/*===========================================================================*/
/*   (SawBbv/bbv-debug.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawBbv/bbv-debug.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_SAW_BBVzd2DEBUGzd2_TYPE_DEFINITIONS
#define BGL_BgL_SAW_BBVzd2DEBUGzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_patternsz00;
		obj_t BgL_labelsz00;
	}                    *BgL_rtl_switchz00_bglt;

	typedef struct BgL_rtl_ifnez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_thenz00;
	}                  *BgL_rtl_ifnez00_bglt;

	typedef struct BgL_rtl_goz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_toz00;
	}                *BgL_rtl_goz00_bglt;

	typedef struct BgL_rtl_loadiz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_atomz00_bgl *BgL_constantz00;
	}                   *BgL_rtl_loadiz00_bglt;

	typedef struct BgL_rtl_callz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                  *BgL_rtl_callz00_bglt;

	typedef struct BgL_rtl_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                    *BgL_rtl_pragmaz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_rtl_regzf2razf2_bgl
	{
		int BgL_numz00;
		obj_t BgL_colorz00;
		obj_t BgL_coalescez00;
		int BgL_occurrencesz00;
		obj_t BgL_interferez00;
		obj_t BgL_interfere2z00;
	}                      *BgL_rtl_regzf2razf2_bglt;

	typedef struct BgL_regsetz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_lengthz00;
		int BgL_msiza7eza7;
		obj_t BgL_regvz00;
		obj_t BgL_reglz00;
		obj_t BgL_stringz00;
	}                *BgL_regsetz00_bglt;

	typedef struct BgL_rtl_inszf2bbvzf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_outz00;
		obj_t BgL_inz00;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		obj_t BgL_z52hashz52;
	}                       *BgL_rtl_inszf2bbvzf2_bglt;

	typedef struct BgL_blockvz00_bgl
	{
		obj_t BgL_versionsz00;
		obj_t BgL_genericz00;
		long BgL_z52markz52;
		obj_t BgL_mergez00;
	}                *BgL_blockvz00_bglt;

	typedef struct BgL_blocksz00_bgl
	{
		long BgL_z52markz52;
		obj_t BgL_z52hashz52;
		obj_t BgL_z52blacklistz52;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		struct BgL_blockz00_bgl *BgL_parentz00;
		long BgL_gccntz00;
		long BgL_gcmarkz00;
		obj_t BgL_mblockz00;
		obj_t BgL_creatorz00;
		obj_t BgL_mergesz00;
		bool_t BgL_asleepz00;
	}                *BgL_blocksz00_bglt;

	typedef struct BgL_bbvzd2ctxzd2_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_idz00;
		obj_t BgL_entriesz00;
	}                   *BgL_bbvzd2ctxzd2_bglt;

	typedef struct BgL_bbvzd2ctxentryzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_rtl_regz00_bgl *BgL_regz00;
		obj_t BgL_typesz00;
		bool_t BgL_polarityz00;
		long BgL_countz00;
		obj_t BgL_valuez00;
		obj_t BgL_aliasesz00;
		obj_t BgL_initvalz00;
	}                        *BgL_bbvzd2ctxentryzd2_bglt;


#endif													// BGL_BgL_SAW_BBVzd2DEBUGzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2debugzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	static obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2debugzd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2debugzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2symbolza2z00zztype_cachez00;
	static obj_t BGl_z62logzd2blockszd2historyz62zzsaw_bbvzd2debugzd2(obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_stringzd2replacezd2zz__r4_strings_6_7z00(obj_t,
		unsigned char, unsigned char);
	extern obj_t BGl_za2destza2z00zzengine_paramz00;
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	extern obj_t BGl_rtl_pragmaz00zzsaw_defsz00;
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_bbvzd2debugzd2 = BUNSPEC;
	static obj_t BGl_paddingrze70ze7zzsaw_bbvzd2debugzd2(obj_t, long, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt);
	extern obj_t BGl_za2vectorza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt);
	extern obj_t BGl_funz00zzast_varz00;
	extern bool_t
		BGl_rtl_inszd2ifeqzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	extern obj_t BGl_za2bboolza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static long BGl_assertzd2cntzd2zzsaw_bbvzd2debugzd2 = 0L;
	static obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2debugzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2debugzd2(int, int, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2debugzd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2debugzd2(BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, int, obj_t, obj_t, int, obj_t, obj_t);
	static obj_t BGl_z62dumpzd2jsonzd2cfgz62zzsaw_bbvzd2debugzd2(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2debugzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_shapezd2ctxzd2entryz00zzsaw_bbvzd2debugzd2(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2debugzd2(void);
	BGL_IMPORT obj_t BGl_commandzd2linezd2zz__osz00(void);
	extern obj_t BGl_za2outputzd2portza2zd2zztype_cachez00;
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_getenvz00zz__osz00(obj_t);
	BGL_IMPORT obj_t bigloo_module_mangle(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	extern obj_t
		BGl_rtl_inszd2switchzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_genericzd2initzd2zzsaw_bbvzd2debugzd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2debugzd2(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32113ze3ze5zzsaw_bbvzd2debugzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_objectzd2initzd2zzsaw_bbvzd2debugzd2(void);
	static obj_t BGl_z62zc3z04anonymousza32597ze3ze5zzsaw_bbvzd2debugzd2(obj_t,
		obj_t);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_IMPORT obj_t string_append_3(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt,
		obj_t);
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	extern bool_t
		BGl_rtl_inszd2ifnezf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	extern obj_t BGl_regsetz00zzsaw_regsetz00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	BGL_IMPORT obj_t
		BGl_callzd2withzd2outputzd2filezd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_regzd2debugnamezd2zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt);
	extern obj_t BGl_za2brealza2z00zztype_cachez00;
	extern obj_t BGl_za2stringza2z00zztype_cachez00;
	extern obj_t BGl_za2bstringza2z00zztype_cachez00;
	static obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	static obj_t BGl_sortzd2blockszd2zzsaw_bbvzd2debugzd2(obj_t);
	BGL_IMPORT obj_t BGl_fprintfz00zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_basenamez00zz__osz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2;
	BGL_EXPORTED_DECL obj_t
		BGl_assertzd2contextz12zc0zzsaw_bbvzd2debugzd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(BgL_blockz00_bglt, obj_t);
	static obj_t BGl_z62assertzd2contextz12za2zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	static obj_t BGl_compilezd2simpleze70z35zzsaw_bbvzd2debugzd2(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2debugzd2(obj_t,
		obj_t);
	static obj_t BGl_appendzd221011zd2zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_bbvzd2debugzd2(void);
	static long BGl_debugcntz00zzsaw_bbvzd2debugzd2 = 0L;
	static obj_t BGl_bbsetzd2conszd2zzsaw_bbvzd2debugzd2(BgL_blockz00_bglt,
		obj_t);
	BGL_IMPORT obj_t bgl_typeof(obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2debugzd2(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2debugzd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_logzd2labelze70z35zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2debugzd2(void);
	static BgL_typez00_bglt
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	extern bool_t BGl_blockzd2livezf3z21zzsaw_bbvzd2typeszd2(BgL_blockz00_bglt);
	static obj_t BGl_z62dumpzd2cfg2998zb0zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	static obj_t BGl_z62dumpzd2cfg2999zb0zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	extern obj_t BGl_dumpz00zzsaw_defsz00(obj_t, obj_t, int);
	extern obj_t BGl_za2realza2z00zztype_cachez00;
	static obj_t BGl_z62dumpzd2cfgzb0zzsaw_bbvzd2debugzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_logzd2entryze70z35zzsaw_bbvzd2debugzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt);
	extern bool_t BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_logzd2blockszd2historyz00zzsaw_bbvzd2debugzd2(BgL_globalz00_bglt, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	static obj_t BGl_z62rtlzd2assertzd2exprzd2typezb0zzsaw_bbvzd2debugzd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rtlzd2assertzd2fxcmpz00zzsaw_bbvzd2debugzd2(obj_t,
		obj_t, obj_t, bool_t, BgL_bbvzd2ctxzd2_bglt, obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2debugzd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2debugzd2(obj_t,
		obj_t);
	extern obj_t BGl_za2pairza2z00zztype_cachez00;
	BGL_IMPORT obj_t
		BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_callz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t
		BGl_dumpzd2jsonzd2cfgz00zzsaw_bbvzd2debugzd2(BgL_globalz00_bglt, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtlzd2assertzd2exprzd2typezd2zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt,
		BgL_typez00_bglt, obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2debugzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2debugzd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62assertzd2blockszb0zzsaw_bbvzd2debugzd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2debugzd2(BgL_regsetz00_bglt,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32148ze3ze5zzsaw_bbvzd2debugzd2(obj_t,
		obj_t);
	extern obj_t BGl_dumpzd2inszd2rhsz00zzsaw_defsz00(BgL_rtl_insz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_getzd2bbzd2markz00zzsaw_bbvzd2typeszd2(void);
	BGL_EXPORTED_DECL obj_t BGl_gendebugidz00zzsaw_bbvzd2debugzd2(void);
	static obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2debugzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2specializa7ez75(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32190ze3ze5zzsaw_bbvzd2debugzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_logzd2blockszd2zzsaw_bbvzd2debugzd2(BgL_globalz00_bglt, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2debugzd2(obj_t);
	BGL_IMPORT obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	static obj_t BGl_z62gendebugidz62zzsaw_bbvzd2debugzd2(obj_t);
	extern obj_t BGl_rtl_loadiz00zzsaw_defsz00;
	static obj_t BGl_typezd2predicatezd2zzsaw_bbvzd2debugzd2(obj_t);
	BGL_IMPORT obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62rtlzd2assertzd2fxcmpz62zzsaw_bbvzd2debugzd2(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2debugzd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_z62zc3z04anonymousza32370ze3ze5zzsaw_bbvzd2debugzd2(obj_t,
		obj_t);
	static obj_t BGl_paddinglze70ze7zzsaw_bbvzd2debugzd2(obj_t, long, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2debugzd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2debugzd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2debugzd2(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2debugzd2(void);
	BGL_IMPORT obj_t BGl_prefixz00zz__osz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2debugzd2(void);
	static obj_t BGl_z62rtlzd2assertzd2regzd2typezb0zzsaw_bbvzd2debugzd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62logzd2blockszb0zzsaw_bbvzd2debugzd2(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32185ze3ze5zzsaw_bbvzd2debugzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_dumpzd2cfgzd2zzsaw_bbvzd2debugzd2(BgL_globalz00_bglt, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t bigloo_mangle(obj_t);
	BGL_IMPORT obj_t bgl_display_fixnum(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2debugzd2
		(BgL_rtl_regz00_bglt, int);
	static obj_t BGl_makezd2emptyzd2bbsetz00zzsaw_bbvzd2debugzd2(void);
	extern obj_t BGl_za2bnilza2z00zztype_cachez00;
	extern obj_t BGl_za2bintzd2ze3longza2z31zzsaw_bbvzd2cachezd2;
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62assertzd2blockzb0zzsaw_bbvzd2debugzd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2debugzd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_assertzd2failurezd2zzsaw_bbvzd2debugzd2(obj_t,
		BgL_rtl_regz00_bglt, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_regsetzf3zf3zzsaw_bbvzd2debugzd2(obj_t);
	static bool_t BGl_dumpzd2blockszd2errorz00zzsaw_bbvzd2debugzd2;
	static obj_t
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2debugzd2(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2debugzd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2debugzd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2debugzd2(BgL_regsetz00_bglt);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2debugzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2bintza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2debugzd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2debugzd2
		(BgL_rtl_regz00_bglt, obj_t);
	extern obj_t BGl_za2bcharza2z00zztype_cachez00;
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2debugzd2(void);
	static obj_t BGl_lblze70ze7zzsaw_bbvzd2debugzd2(obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2debugzd2(obj_t,
		obj_t);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	extern obj_t BGl_rtl_regzf2razf2zzsaw_regsetz00;
	static obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2procedurezd2elza2zd2zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_assertzd2blockszd2zzsaw_bbvzd2debugzd2(BgL_blockz00_bglt, obj_t);
	static obj_t BGl_sortzd2blocksze70z35zzsaw_bbvzd2debugzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2debugzd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2debugzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2debugzd2(BgL_regsetz00_bglt);
	extern obj_t BGl_za2bbvzd2assertza2zd2zzsaw_bbvzd2configzd2;
	static obj_t
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2debugzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32394ze3ze5zzsaw_bbvzd2debugzd2(obj_t,
		obj_t);
	static BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2debugzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt);
	extern obj_t BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2;
	extern obj_t BGl_blockz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtlzd2assertzd2regzd2typezd2zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt,
		BgL_typez00_bglt, bool_t, BgL_bbvzd2ctxzd2_bglt, obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2debugzd2(BgL_regsetz00_bglt,
		int);
	static obj_t BGl_z62zc3z04anonymousza32379ze3ze5zzsaw_bbvzd2debugzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_dumpzd2marginzd2zzsaw_defsz00(obj_t, int);
	static obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2debugzd2(obj_t,
		obj_t);
	extern obj_t BGl_za2charza2z00zztype_cachez00;
	static obj_t __cnst[9];


	   
		 
		DEFINE_STRING(BGl_string3010z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3010za700za7za7s3115za7, "parent block: ", 14);
	      DEFINE_STRING(BGl_string3011z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3011za700za7za7s3116za7,
		"instruction target not in succs of #~a", 38);
	      DEFINE_STRING(BGl_string3012z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3012za700za7za7s3117za7, "./a.out", 7);
	      DEFINE_STRING(BGl_string3013z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3013za700za7za7s3118za7, "~a-~a~a", 7);
	      DEFINE_STRING(BGl_string3014z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3014za700za7za7s3119za7, ";; -*- mode: bee -*-", 20);
	      DEFINE_STRING(BGl_string3015z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3015za700za7za7s3120za7, ";; *** ", 7);
	      DEFINE_STRING(BGl_string3016z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3016za700za7za7s3121za7, "BIGLOOBBVVLENGTH", 16);
	      DEFINE_STRING(BGl_string3017z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3017za700za7za7s3122za7, "false", 5);
	      DEFINE_STRING(BGl_string3018z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3018za700za7za7s3123za7, "BIGLOOBBVVERSIONLIMIT", 21);
	      DEFINE_STRING(BGl_string3019z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3019za700za7za7s3124za7, "4", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2namezd2envzf2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73125za7,
		BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3100z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3100za700za7za7s3126za7,
		"/* rtl-assert-expr-type */ ($1 || ~a)", 37);
	      DEFINE_STRING(BGl_string3101z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3101za700za7za7s3127za7,
		"(fprintf(stderr, \"*** ~a[~a]:%s:%d\\n%s: %s:%d\\n\", __FILE__, __LINE__, \"~a\", \"~a\", ~a), exit(127), 0)",
		100);
	      DEFINE_STRING(BGl_string3020z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3020za700za7za7s3128za7, "BIGLOOBBVSTRATEGY", 17);
	      DEFINE_STRING(BGl_string3102z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3102za700za7za7s3129za7,
		"(fprintf(stderr, \"*** ~a[~a]:%s:%d\\n%s\\n\", __FILE__, __LINE__, \"~a\"), exit(127), 0)",
		83);
	      DEFINE_STRING(BGl_string3021z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3021za700za7za7s3130za7, "size", 4);
	      DEFINE_STRING(BGl_string3103z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3103za700za7za7s3131za7, "==", 2);
	      DEFINE_STRING(BGl_string3022z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3022za700za7za7s3132za7,
		";; BIGLOOBBVVLENGTH=\"~a\" BIGLOOBBVVERSIONLIMIT=\"~a\" BIGLOOBBVSTRATEGY=\"~a\" ~( )\n",
		80);
	      DEFINE_STRING(BGl_string3104z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3104za700za7za7s3133za7,
		"/* rtl-assert-fxcmp */ (~a((~a) ~a (~a)) || ~a)", 47);
	      DEFINE_STRING(BGl_string3023z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3023za700za7za7s3134za7,
		";; bglcfg '~a' > '~a.dot' && dot '~a.dot' -Tpdf > '~a.pdf'\n", 59);
	      DEFINE_STRING(BGl_string3105z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3105za700za7za7s3135za7, "CINT(~a)", 8);
	      DEFINE_STRING(BGl_string3024z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3024za700za7za7s3136za7, "~a:~a", 5);
	      DEFINE_STRING(BGl_string3106z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3106za700za7za7s3137za7, "x=", 2);
	      DEFINE_STRING(BGl_string3025z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3025za700za7za7s3138za7, "!~a", 3);
	      DEFINE_STRING(BGl_string3107z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3107za700za7za7s3139za7, "y=", 2);
	      DEFINE_STRING(BGl_string3026z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3026za700za7za7s3140za7, "{", 1);
	      DEFINE_STRING(BGl_string3108z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3108za700za7za7s3141za7, "z=", 2);
	      DEFINE_STRING(BGl_string3027z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3027za700za7za7s3142za7, "\"id\": ", 6);
	      DEFINE_STRING(BGl_string3109z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3109za700za7za7s3143za7, "w=", 2);
	      DEFINE_STRING(BGl_string3028z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3028za700za7za7s3144za7, "\"origin\": ", 10);
	      DEFINE_STRING(BGl_string3029z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3029za700za7za7s3145za7, "\"bbs\": \"~a\",\n", 13);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_dumpzd2cfgzd2envz00zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762dumpza7d2cfgza7b03146za7,
		BGl_z62dumpzd2cfgzb0zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3110za700za7za7s3147za7, "saw_bbv-debug", 13);
	      DEFINE_STRING(BGl_string3111z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3111za700za7za7s3148za7,
		"= #() bigloo-c bbv #(\"\" \" \" \"  \" \"   \" \"    \" \"     \" \"      \" \"       \" \"        \" \"         \" \"          \" \"           \" \"            \" \"             \" \"              \" \"               \" \"                \" \"                 \" \"                  \" \"                   \" \"                    \" \"                     \" \"                      \" \"                       \" \"                        \" \"                         \" \"                          \" \"                           \" \"                            \" \"                             \" \"                              \" \"                               \" \"                                \" \"                                 \" \"                                  \" \"                                   \" \"                                    \" \"                                     \" \"                                      \" \"                                       \") location _ root bbset ",
		944);
	      DEFINE_STRING(BGl_string3030z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3030za700za7za7s3149za7, "\"loc\": \"~a\",\n", 13);
	      DEFINE_STRING(BGl_string3031z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3031za700za7za7s3150za7, "\"usage\": ", 9);
	      DEFINE_STRING(BGl_string3032z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3032za700za7za7s3151za7, "\"context\": ~s,\n", 15);
	      DEFINE_STRING(BGl_string3033z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3033za700za7za7s3152za7, "\"predecessors\": [", 17);
	      DEFINE_STRING(BGl_string3034z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3034za700za7za7s3153za7, "~(, )", 5);
	      DEFINE_STRING(BGl_string3035z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3035za700za7za7s3154za7, "],", 2);
	      DEFINE_STRING(BGl_string3036z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3036za700za7za7s3155za7, "\"successors\": [", 15);
	      DEFINE_STRING(BGl_string3037z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3037za700za7za7s3156za7, "\"details\": ~s\n", 14);
	      DEFINE_STRING(BGl_string3038z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3038za700za7za7s3157za7, "}", 1);
	      DEFINE_STRING(BGl_string3039z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3039za700za7za7s3158za7, "  \"compiler\": \"bigloo\",", 23);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2nilzd2envz00zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762regsetza7d2nilza73159za7,
		BGl_z62regsetzd2nilzb0zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3040z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3040za700za7za7s3160za7, "  \"version-limit\": ~a,\n", 23);
	      DEFINE_STRING(BGl_string3041z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3041za700za7za7s3161za7, "  \"vector-length\": \"~a\",\n",
		25);
	      DEFINE_STRING(BGl_string3042z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3042za700za7za7s3162za7, "  \"merge-strategy\": \"~a\",\n",
		26);
	      DEFINE_STRING(BGl_string3043z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3043za700za7za7s3163za7, "  \"specializedCFG\": [", 21);
	      DEFINE_STRING(BGl_string3044z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3044za700za7za7s3164za7, "    null", 8);
	      DEFINE_STRING(BGl_string3045z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3045za700za7za7s3165za7, "  ],", 4);
	      DEFINE_STRING(BGl_string3046z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3046za700za7za7s3166za7, "  \"history\": ", 13);
	      DEFINE_STRING(BGl_string3047z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3047za700za7za7s3167za7, "  ]", 3);
	      DEFINE_STRING(BGl_string3048z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3048za700za7za7s3168za7, "(", 1);
	      DEFINE_STRING(BGl_string3049z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3049za700za7za7s3169za7, " <- ", 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2setz12zd2envz32zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73170za7,
		BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3050z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3050za700za7za7s3171za7, ")", 1);
	      DEFINE_STRING(BGl_string3051z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3051za700za7za7s3172za7, " ", 1);
	      DEFINE_STRING(BGl_string3053z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3053za700za7za7s3173za7,
		"-----------------------------------------------------------", 59);
	      DEFINE_STRING(BGl_string3054z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3054za700za7za7s3174za7, "~>", 2);
	      DEFINE_STRING(BGl_string3055z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3055za700za7za7s3175za7, "~( )", 4);
	      DEFINE_STRING(BGl_string3056z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3056za700za7za7s3176za7, "~a+~a", 5);
	      DEFINE_STRING(BGl_string3057z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3057za700za7za7s3177za7, "<- ~a+~a", 8);
	      DEFINE_STRING(BGl_string3058z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3058za700za7za7s3178za7, "<- ~a", 5);
	      DEFINE_STRING(BGl_string3059z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3059za700za7za7s3179za7, "==== ", 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2envzf2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73180za7,
		BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3052z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762za7c3za704anonymo3181za7,
		BGl_z62zc3z04anonymousza32185ze3ze5zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3060z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3060za700za7za7s3182za7, "", 0);
	      DEFINE_STRING(BGl_string3061z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3061za700za7za7s3183za7, " ~a", 3);
	      DEFINE_STRING(BGl_string3062z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3062za700za7za7s3184za7, "~a:[~( )~a]", 11);
	      DEFINE_STRING(BGl_string3063z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3063za700za7za7s3185za7, "~a:[~( )~a ~( )]", 16);
	      DEFINE_STRING(BGl_string3064z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3064za700za7za7s3186za7, "\"event\": \"create\",", 18);
	      DEFINE_STRING(BGl_string3065z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3065za700za7za7s3187za7, "\"from\": ~a\n", 11);
	      DEFINE_STRING(BGl_string3066z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3066za700za7za7s3188za7, "\"from\": \"~a\"\n", 13);
	      DEFINE_STRING(BGl_string3067z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3067za700za7za7s3189za7, "\"event\": \"mergeCreate\",", 23);
	      DEFINE_STRING(BGl_string3068z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3068za700za7za7s3190za7, "\"merged\": [~a, ~a]\n", 19);
	      DEFINE_STRING(BGl_string3069z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3069za700za7za7s3191za7, "missing block creator (~a)", 26);
	      DEFINE_STRING(BGl_string3070z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3070za700za7za7s3192za7, "log-blocks-history", 18);
	      DEFINE_STRING(BGl_string3071z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3071za700za7za7s3193za7, "},", 2);
	      DEFINE_STRING(BGl_string3072z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3072za700za7za7s3194za7, "\"event\": \"merge\",", 17);
	      DEFINE_STRING(BGl_string3073z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3073za700za7za7s3195za7, "[", 1);
	      DEFINE_STRING(BGl_string3074z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3074za700za7za7s3196za7, "    null\n", 9);
	      DEFINE_STRING(BGl_string3076z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3076za700za7za7s3197za7, "_", 1);
	      DEFINE_STRING(BGl_string3077z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3077za700za7za7s3198za7, "INTEGERP", 8);
	      DEFINE_STRING(BGl_string3078z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3078za700za7za7s3199za7, "FLONUMP", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2setz12zd2envzc1zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73200za7,
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2debugzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3079z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3079za700za7za7s3201za7, "VECTORP", 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2setz12zd2envz32zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73202za7,
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2debugzd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2envzf2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73203za7,
		BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3080z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3080za700za7za7s3204za7, "PAIRP", 5);
	      DEFINE_STRING(BGl_string3081z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3081za700za7za7s3205za7, "STRINGP", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3075z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762za7c3za704anonymo3206za7,
		BGl_z62zc3z04anonymousza32379ze3ze5zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_logzd2blockszd2envz00zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762logza7d2blocksza73207za7,
		BGl_z62logzd2blockszb0zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string3082z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3082za700za7za7s3208za7, "CHARP", 5);
	      DEFINE_STRING(BGl_string3083z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3083za700za7za7s3209za7, "NULLP", 5);
	      DEFINE_STRING(BGl_string3084z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3084za700za7za7s3210za7, "PROCEDUREP", 10);
	      DEFINE_STRING(BGl_string3085z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3085za700za7za7s3211za7, "OUTPUT_PORTP", 12);
	      DEFINE_STRING(BGl_string3086z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3086za700za7za7s3212za7, "BOOLEANP", 8);
	      DEFINE_STRING(BGl_string3087z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3087za700za7za7s3213za7, "SYMBOLP", 7);
	      DEFINE_STRING(BGl_string3088z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3088za700za7za7s3214za7, "BBV-ASSERT-FAILURE", 18);
	      DEFINE_STRING(BGl_string3089z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3089za700za7za7s3215za7, "!", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2setz12zd2envz32zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73216za7,
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2debugzd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2regvzd2envz00zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762regsetza7d2regv3217z00,
		BGl_z62regsetzd2regvzb0zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3090z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3090za700za7za7s3218za7,
		"(fprintf(stderr, \"*** ~a[~a]:%s:%d\\n%s: %s::~a%s (%s:%d)\\n\", __FILE__, __LINE__, \"~a\", \"~a\", BSTRING_TO_STRING(bgl_typeof(~a)), \"~a\", ~a), exit(127), 0)",
		152);
	      DEFINE_STRING(BGl_string3091z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3091za700za7za7s3219za7,
		"(fprintf(stderr, \"*** ~a[~a]:%s:%d\\n%s: %s::~a%s\\n\", __FILE__, __LINE__, \"~a\", \"~a\", BSTRING_TO_STRING(bgl_typeof(~a))), exit(127), 0)",
		134);
	      DEFINE_STRING(BGl_string3092z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3092za700za7za7s3220za7, "~( \n         && )", 17);
	      DEFINE_STRING(BGl_string3093z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3093za700za7za7s3221za7, "before assert!", 14);
	      DEFINE_STRING(BGl_string3094z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3094za700za7za7s3222za7, "assert-context!", 15);
	      DEFINE_STRING(BGl_string3095z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3095za700za7za7s3223za7, "(~a~a(~a) || ~a)", 16);
	      DEFINE_STRING(BGl_string3096z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3096za700za7za7s3224za7, "~( && )", 7);
	      DEFINE_STRING(BGl_string3097z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3097za700za7za7s3225za7,
		"/* rtl-assert-reg-type */ (~a~a(~a) || ~a)", 42);
	      DEFINE_STRING(BGl_string3098z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3098za700za7za7s3226za7, "PAS DE TYPE PRED ", 17);
	      DEFINE_STRING(BGl_string3099z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3099za700za7za7s3227za7, "\077\077?", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtlzd2assertzd2regzd2typezd2envz00zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtlza7d2assertza73228za7,
		BGl_z62rtlzd2assertzd2regzd2typezb0zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 6);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_gendebugidzd2envzd2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762gendebugidza7623229z00,
		BGl_z62gendebugidz62zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2setz12zd2envzc0zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762regsetza7d2leng3230z00,
		BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2msiza7ezd2envza7zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762regsetza7d2msiza73231za7,
		BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2envzf2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73232za7,
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2setz12zd2envz32zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73233za7,
		BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2envzf2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73234za7,
		BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2nilzd2envzf2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73235za7,
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2envzf2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73236za7,
		BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2numzd2envzf2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73237za7,
		BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_assertzd2blockszd2envz00zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762assertza7d2bloc3238z00,
		BGl_z62assertzd2blockszb0zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2setz12zd2envzc0zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762regsetza7d2stri3239z00,
		BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2setz12zd2envz32zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73240za7,
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2debugzd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2envz01zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73241za7,
		BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_assertzd2blockzd2envz00zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762assertza7d2bloc3242z00,
		BGl_z62assertzd2blockzb0zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2envz00zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762regsetza7d2stri3243z00,
		BGl_z62regsetzd2stringzb0zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2setz12zd2envz32zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73244za7,
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2debugzd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2rtl_regzf2razd2envzf2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762makeza7d2rtl_re3245z00,
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razf3zd2envzd3zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73246za7,
		BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2envz00zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762regsetza7d2leng3247z00,
		BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2envzf2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73248za7,
		BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2envzf2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73249za7,
		BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2hardwarezd2envzf2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73250za7,
		BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2regsetzd2envz00zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762makeza7d2regset3251z00,
		BGl_z62makezd2regsetzb0zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_dumpzd2jsonzd2cfgzd2envzd2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762dumpza7d2jsonza7d3252za7,
		BGl_z62dumpzd2jsonzd2cfgz62zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzf3zd2envz21zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762regsetza7f3za791za73253z00,
		BGl_z62regsetzf3z91zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_assertzd2contextz12zd2envz12zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762assertza7d2cont3254z00,
		BGl_z62assertzd2contextz12za2zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2keyzd2envzf2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73255za7,
		BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_logzd2blockszd2historyzd2envzd2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762logza7d2blocksza73256za7,
		BGl_z62logzd2blockszd2historyz62zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtlzd2assertzd2fxcmpzd2envzd2zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtlza7d2assertza73257za7,
		BGl_z62rtlzd2assertzd2fxcmpz62zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2setz12zd2envz32zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtl_regza7f2raza73258za7,
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2debugzd2, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string3001z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3001za700za7za7s3259za7, ":", 1);
	      DEFINE_STRING(BGl_string3002z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3002za700za7za7s3260za7, ",", 1);
	      DEFINE_STRING(BGl_string3003z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3003za700za7za7s3261za7, "SawBbv/bbv-debug.scm", 20);
	      DEFINE_STRING(BGl_string3004z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3004za700za7za7s3262za7,
		"predecessors and cnt not in sync #~a", 36);
	      DEFINE_STRING(BGl_string3005z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3005za700za7za7s3263za7, "preds.len=~a gccnt=~a", 21);
	      DEFINE_STRING(BGl_string3006z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3006za700za7za7s3264za7, "predecessors not pointing to #~a",
		32);
	      DEFINE_STRING(BGl_string3007z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3007za700za7za7s3265za7, "#~a", 3);
	      DEFINE_STRING(BGl_string3008z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3008za700za7za7s3266za7, "successors not pointing to #~a",
		30);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtlzd2assertzd2exprzd2typezd2envz00zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762rtlza7d2assertza73267za7,
		BGl_z62rtlzd2assertzd2exprzd2typezb0zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 6);
	      DEFINE_STRING(BGl_string3009z00zzsaw_bbvzd2debugzd2,
		BgL_bgl_string3009za700za7za7s3268za7, "wrong block: ", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2reglzd2envz00zzsaw_bbvzd2debugzd2,
		BgL_bgl_za762regsetza7d2regl3269z00,
		BGl_z62regsetzd2reglzb0zzsaw_bbvzd2debugzd2, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_bbvzd2debugzd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2debugzd2(long
		BgL_checksumz00_5752, char *BgL_fromz00_5753)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_bbvzd2debugzd2))
				{
					BGl_requirezd2initializa7ationz75zzsaw_bbvzd2debugzd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_bbvzd2debugzd2();
					BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2debugzd2();
					BGl_cnstzd2initzd2zzsaw_bbvzd2debugzd2();
					BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2debugzd2();
					return BGl_toplevelzd2initzd2zzsaw_bbvzd2debugzd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2debugzd2(void)
	{
		{	/* SawBbv/bbv-debug.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_bbv-debug");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_bbv-debug");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"saw_bbv-debug");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "saw_bbv-debug");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "saw_bbv-debug");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"saw_bbv-debug");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_bbv-debug");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_bbv-debug");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_bbv-debug");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_bbv-debug");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_bbv-debug");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_bbv-debug");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"saw_bbv-debug");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_bbv-debug");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "saw_bbv-debug");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2debugzd2(void)
	{
		{	/* SawBbv/bbv-debug.scm 15 */
			{	/* SawBbv/bbv-debug.scm 15 */
				obj_t BgL_cportz00_5332;

				{	/* SawBbv/bbv-debug.scm 15 */
					obj_t BgL_stringz00_5339;

					BgL_stringz00_5339 = BGl_string3111z00zzsaw_bbvzd2debugzd2;
					{	/* SawBbv/bbv-debug.scm 15 */
						obj_t BgL_startz00_5340;

						BgL_startz00_5340 = BINT(0L);
						{	/* SawBbv/bbv-debug.scm 15 */
							obj_t BgL_endz00_5341;

							BgL_endz00_5341 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_5339)));
							{	/* SawBbv/bbv-debug.scm 15 */

								BgL_cportz00_5332 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_5339, BgL_startz00_5340, BgL_endz00_5341);
				}}}}
				{
					long BgL_iz00_5333;

					BgL_iz00_5333 = 8L;
				BgL_loopz00_5334:
					if ((BgL_iz00_5333 == -1L))
						{	/* SawBbv/bbv-debug.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* SawBbv/bbv-debug.scm 15 */
							{	/* SawBbv/bbv-debug.scm 15 */
								obj_t BgL_arg3114z00_5335;

								{	/* SawBbv/bbv-debug.scm 15 */

									{	/* SawBbv/bbv-debug.scm 15 */
										obj_t BgL_locationz00_5337;

										BgL_locationz00_5337 = BBOOL(((bool_t) 0));
										{	/* SawBbv/bbv-debug.scm 15 */

											BgL_arg3114z00_5335 =
												BGl_readz00zz__readerz00(BgL_cportz00_5332,
												BgL_locationz00_5337);
										}
									}
								}
								{	/* SawBbv/bbv-debug.scm 15 */
									int BgL_tmpz00_5786;

									BgL_tmpz00_5786 = (int) (BgL_iz00_5333);
									CNST_TABLE_SET(BgL_tmpz00_5786, BgL_arg3114z00_5335);
							}}
							{	/* SawBbv/bbv-debug.scm 15 */
								int BgL_auxz00_5338;

								BgL_auxz00_5338 = (int) ((BgL_iz00_5333 - 1L));
								{
									long BgL_iz00_5791;

									BgL_iz00_5791 = (long) (BgL_auxz00_5338);
									BgL_iz00_5333 = BgL_iz00_5791;
									goto BgL_loopz00_5334;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2debugzd2(void)
	{
		{	/* SawBbv/bbv-debug.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2debugzd2(void)
	{
		{	/* SawBbv/bbv-debug.scm 15 */
			BGl_dumpzd2blockszd2errorz00zzsaw_bbvzd2debugzd2 = ((bool_t) 0);
			BGl_debugcntz00zzsaw_bbvzd2debugzd2 = 0L;
			return (BGl_assertzd2cntzd2zzsaw_bbvzd2debugzd2 = 0L, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzsaw_bbvzd2debugzd2(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_2209;

				BgL_headz00_2209 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2210;
					obj_t BgL_tailz00_2211;

					BgL_prevz00_2210 = BgL_headz00_2209;
					BgL_tailz00_2211 = BgL_l1z00_1;
				BgL_loopz00_2212:
					if (PAIRP(BgL_tailz00_2211))
						{
							obj_t BgL_newzd2prevzd2_2214;

							BgL_newzd2prevzd2_2214 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2211), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2210, BgL_newzd2prevzd2_2214);
							{
								obj_t BgL_tailz00_5801;
								obj_t BgL_prevz00_5800;

								BgL_prevz00_5800 = BgL_newzd2prevzd2_2214;
								BgL_tailz00_5801 = CDR(BgL_tailz00_2211);
								BgL_tailz00_2211 = BgL_tailz00_5801;
								BgL_prevz00_2210 = BgL_prevz00_5800;
								goto BgL_loopz00_2212;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_2209);
				}
			}
		}

	}



/* make-rtl_reg/ra */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2debugzd2(BgL_typez00_bglt
		BgL_type1179z00_17, obj_t BgL_var1180z00_18, obj_t BgL_onexprzf31181zf3_19,
		obj_t BgL_name1182z00_20, obj_t BgL_key1183z00_21,
		obj_t BgL_hardware1184z00_22, int BgL_num1185z00_23,
		obj_t BgL_color1186z00_24, obj_t BgL_coalesce1187z00_25,
		int BgL_occurrences1188z00_26, obj_t BgL_interfere1189z00_27,
		obj_t BgL_interfere21190z00_28)
	{
		{	/* SawMill/regset.sch 55 */
			{	/* SawMill/regset.sch 55 */
				BgL_rtl_regz00_bglt BgL_new1166z00_5343;

				{	/* SawMill/regset.sch 55 */
					BgL_rtl_regz00_bglt BgL_tmp1164z00_5344;
					BgL_rtl_regzf2razf2_bglt BgL_wide1165z00_5345;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_5804;

						{	/* SawMill/regset.sch 55 */
							BgL_rtl_regz00_bglt BgL_new1163z00_5346;

							BgL_new1163z00_5346 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/regset.sch 55 */
								long BgL_arg1703z00_5347;

								BgL_arg1703z00_5347 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1163z00_5346),
									BgL_arg1703z00_5347);
							}
							{	/* SawMill/regset.sch 55 */
								BgL_objectz00_bglt BgL_tmpz00_5809;

								BgL_tmpz00_5809 = ((BgL_objectz00_bglt) BgL_new1163z00_5346);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5809, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1163z00_5346);
							BgL_auxz00_5804 = BgL_new1163z00_5346;
						}
						BgL_tmp1164z00_5344 = ((BgL_rtl_regz00_bglt) BgL_auxz00_5804);
					}
					BgL_wide1165z00_5345 =
						((BgL_rtl_regzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regzf2razf2_bgl))));
					{	/* SawMill/regset.sch 55 */
						obj_t BgL_auxz00_5817;
						BgL_objectz00_bglt BgL_tmpz00_5815;

						BgL_auxz00_5817 = ((obj_t) BgL_wide1165z00_5345);
						BgL_tmpz00_5815 = ((BgL_objectz00_bglt) BgL_tmp1164z00_5344);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5815, BgL_auxz00_5817);
					}
					((BgL_objectz00_bglt) BgL_tmp1164z00_5344);
					{	/* SawMill/regset.sch 55 */
						long BgL_arg1702z00_5348;

						BgL_arg1702z00_5348 =
							BGL_CLASS_NUM(BGl_rtl_regzf2razf2zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1164z00_5344), BgL_arg1702z00_5348);
					}
					BgL_new1166z00_5343 = ((BgL_rtl_regz00_bglt) BgL_tmp1164z00_5344);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1166z00_5343)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1179z00_17), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_5343)))->BgL_varz00) =
					((obj_t) BgL_var1180z00_18), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_5343)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31181zf3_19), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_5343)))->BgL_namez00) =
					((obj_t) BgL_name1182z00_20), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_5343)))->BgL_keyz00) =
					((obj_t) BgL_key1183z00_21), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_5343)))->BgL_debugnamez00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_5343)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1184z00_22), BUNSPEC);
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_5839;

					{
						obj_t BgL_auxz00_5840;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_5841;

							BgL_tmpz00_5841 = ((BgL_objectz00_bglt) BgL_new1166z00_5343);
							BgL_auxz00_5840 = BGL_OBJECT_WIDENING(BgL_tmpz00_5841);
						}
						BgL_auxz00_5839 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5840);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5839))->BgL_numz00) =
						((int) BgL_num1185z00_23), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_5846;

					{
						obj_t BgL_auxz00_5847;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_5848;

							BgL_tmpz00_5848 = ((BgL_objectz00_bglt) BgL_new1166z00_5343);
							BgL_auxz00_5847 = BGL_OBJECT_WIDENING(BgL_tmpz00_5848);
						}
						BgL_auxz00_5846 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5847);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5846))->
							BgL_colorz00) = ((obj_t) BgL_color1186z00_24), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_5853;

					{
						obj_t BgL_auxz00_5854;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_5855;

							BgL_tmpz00_5855 = ((BgL_objectz00_bglt) BgL_new1166z00_5343);
							BgL_auxz00_5854 = BGL_OBJECT_WIDENING(BgL_tmpz00_5855);
						}
						BgL_auxz00_5853 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5854);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5853))->
							BgL_coalescez00) = ((obj_t) BgL_coalesce1187z00_25), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_5860;

					{
						obj_t BgL_auxz00_5861;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_5862;

							BgL_tmpz00_5862 = ((BgL_objectz00_bglt) BgL_new1166z00_5343);
							BgL_auxz00_5861 = BGL_OBJECT_WIDENING(BgL_tmpz00_5862);
						}
						BgL_auxz00_5860 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5861);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5860))->
							BgL_occurrencesz00) = ((int) BgL_occurrences1188z00_26), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_5867;

					{
						obj_t BgL_auxz00_5868;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_5869;

							BgL_tmpz00_5869 = ((BgL_objectz00_bglt) BgL_new1166z00_5343);
							BgL_auxz00_5868 = BGL_OBJECT_WIDENING(BgL_tmpz00_5869);
						}
						BgL_auxz00_5867 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5868);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5867))->
							BgL_interferez00) = ((obj_t) BgL_interfere1189z00_27), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_5874;

					{
						obj_t BgL_auxz00_5875;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_5876;

							BgL_tmpz00_5876 = ((BgL_objectz00_bglt) BgL_new1166z00_5343);
							BgL_auxz00_5875 = BGL_OBJECT_WIDENING(BgL_tmpz00_5876);
						}
						BgL_auxz00_5874 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5875);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5874))->
							BgL_interfere2z00) = ((obj_t) BgL_interfere21190z00_28), BUNSPEC);
				}
				return BgL_new1166z00_5343;
			}
		}

	}



/* &make-rtl_reg/ra */
	BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5137, obj_t BgL_type1179z00_5138, obj_t BgL_var1180z00_5139,
		obj_t BgL_onexprzf31181zf3_5140, obj_t BgL_name1182z00_5141,
		obj_t BgL_key1183z00_5142, obj_t BgL_hardware1184z00_5143,
		obj_t BgL_num1185z00_5144, obj_t BgL_color1186z00_5145,
		obj_t BgL_coalesce1187z00_5146, obj_t BgL_occurrences1188z00_5147,
		obj_t BgL_interfere1189z00_5148, obj_t BgL_interfere21190z00_5149)
	{
		{	/* SawMill/regset.sch 55 */
			return
				BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2debugzd2(
				((BgL_typez00_bglt) BgL_type1179z00_5138), BgL_var1180z00_5139,
				BgL_onexprzf31181zf3_5140, BgL_name1182z00_5141, BgL_key1183z00_5142,
				BgL_hardware1184z00_5143, CINT(BgL_num1185z00_5144),
				BgL_color1186z00_5145, BgL_coalesce1187z00_5146,
				CINT(BgL_occurrences1188z00_5147), BgL_interfere1189z00_5148,
				BgL_interfere21190z00_5149);
		}

	}



/* rtl_reg/ra? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2debugzd2(obj_t
		BgL_objz00_29)
	{
		{	/* SawMill/regset.sch 56 */
			{	/* SawMill/regset.sch 56 */
				obj_t BgL_classz00_5349;

				BgL_classz00_5349 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_29))
					{	/* SawMill/regset.sch 56 */
						BgL_objectz00_bglt BgL_arg1807z00_5350;

						BgL_arg1807z00_5350 = (BgL_objectz00_bglt) (BgL_objz00_29);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 56 */
								long BgL_idxz00_5351;

								BgL_idxz00_5351 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5350);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5351 + 2L)) == BgL_classz00_5349);
							}
						else
							{	/* SawMill/regset.sch 56 */
								bool_t BgL_res2977z00_5354;

								{	/* SawMill/regset.sch 56 */
									obj_t BgL_oclassz00_5355;

									{	/* SawMill/regset.sch 56 */
										obj_t BgL_arg1815z00_5356;
										long BgL_arg1816z00_5357;

										BgL_arg1815z00_5356 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 56 */
											long BgL_arg1817z00_5358;

											BgL_arg1817z00_5358 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5350);
											BgL_arg1816z00_5357 = (BgL_arg1817z00_5358 - OBJECT_TYPE);
										}
										BgL_oclassz00_5355 =
											VECTOR_REF(BgL_arg1815z00_5356, BgL_arg1816z00_5357);
									}
									{	/* SawMill/regset.sch 56 */
										bool_t BgL__ortest_1115z00_5359;

										BgL__ortest_1115z00_5359 =
											(BgL_classz00_5349 == BgL_oclassz00_5355);
										if (BgL__ortest_1115z00_5359)
											{	/* SawMill/regset.sch 56 */
												BgL_res2977z00_5354 = BgL__ortest_1115z00_5359;
											}
										else
											{	/* SawMill/regset.sch 56 */
												long BgL_odepthz00_5360;

												{	/* SawMill/regset.sch 56 */
													obj_t BgL_arg1804z00_5361;

													BgL_arg1804z00_5361 = (BgL_oclassz00_5355);
													BgL_odepthz00_5360 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5361);
												}
												if ((2L < BgL_odepthz00_5360))
													{	/* SawMill/regset.sch 56 */
														obj_t BgL_arg1802z00_5362;

														{	/* SawMill/regset.sch 56 */
															obj_t BgL_arg1803z00_5363;

															BgL_arg1803z00_5363 = (BgL_oclassz00_5355);
															BgL_arg1802z00_5362 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5363,
																2L);
														}
														BgL_res2977z00_5354 =
															(BgL_arg1802z00_5362 == BgL_classz00_5349);
													}
												else
													{	/* SawMill/regset.sch 56 */
														BgL_res2977z00_5354 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2977z00_5354;
							}
					}
				else
					{	/* SawMill/regset.sch 56 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_reg/ra? */
	obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5150,
		obj_t BgL_objz00_5151)
	{
		{	/* SawMill/regset.sch 56 */
			return BBOOL(BGl_rtl_regzf2razf3z01zzsaw_bbvzd2debugzd2(BgL_objz00_5151));
		}

	}



/* rtl_reg/ra-nil */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2debugzd2(void)
	{
		{	/* SawMill/regset.sch 57 */
			{	/* SawMill/regset.sch 57 */
				obj_t BgL_classz00_4100;

				BgL_classz00_4100 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				{	/* SawMill/regset.sch 57 */
					obj_t BgL__ortest_1117z00_4101;

					BgL__ortest_1117z00_4101 = BGL_CLASS_NIL(BgL_classz00_4100);
					if (CBOOL(BgL__ortest_1117z00_4101))
						{	/* SawMill/regset.sch 57 */
							return ((BgL_rtl_regz00_bglt) BgL__ortest_1117z00_4101);
						}
					else
						{	/* SawMill/regset.sch 57 */
							return
								((BgL_rtl_regz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4100));
						}
				}
			}
		}

	}



/* &rtl_reg/ra-nil */
	BgL_rtl_regz00_bglt BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5152)
	{
		{	/* SawMill/regset.sch 57 */
			return BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2debugzd2();
		}

	}



/* rtl_reg/ra-interfere2 */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_oz00_30)
	{
		{	/* SawMill/regset.sch 58 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_5916;

				{
					obj_t BgL_auxz00_5917;

					{	/* SawMill/regset.sch 58 */
						BgL_objectz00_bglt BgL_tmpz00_5918;

						BgL_tmpz00_5918 = ((BgL_objectz00_bglt) BgL_oz00_30);
						BgL_auxz00_5917 = BGL_OBJECT_WIDENING(BgL_tmpz00_5918);
					}
					BgL_auxz00_5916 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5917);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5916))->
					BgL_interfere2z00);
			}
		}

	}



/* &rtl_reg/ra-interfere2 */
	obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5153, obj_t BgL_oz00_5154)
	{
		{	/* SawMill/regset.sch 58 */
			return
				BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5154));
		}

	}



/* rtl_reg/ra-interfere2-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2debugzd2
		(BgL_rtl_regz00_bglt BgL_oz00_31, obj_t BgL_vz00_32)
	{
		{	/* SawMill/regset.sch 59 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_5925;

				{
					obj_t BgL_auxz00_5926;

					{	/* SawMill/regset.sch 59 */
						BgL_objectz00_bglt BgL_tmpz00_5927;

						BgL_tmpz00_5927 = ((BgL_objectz00_bglt) BgL_oz00_31);
						BgL_auxz00_5926 = BGL_OBJECT_WIDENING(BgL_tmpz00_5927);
					}
					BgL_auxz00_5925 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5926);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5925))->
						BgL_interfere2z00) = ((obj_t) BgL_vz00_32), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere2-set! */
	obj_t BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5155, obj_t BgL_oz00_5156, obj_t BgL_vz00_5157)
	{
		{	/* SawMill/regset.sch 59 */
			return
				BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5156), BgL_vz00_5157);
		}

	}



/* rtl_reg/ra-interfere */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_oz00_33)
	{
		{	/* SawMill/regset.sch 60 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_5934;

				{
					obj_t BgL_auxz00_5935;

					{	/* SawMill/regset.sch 60 */
						BgL_objectz00_bglt BgL_tmpz00_5936;

						BgL_tmpz00_5936 = ((BgL_objectz00_bglt) BgL_oz00_33);
						BgL_auxz00_5935 = BGL_OBJECT_WIDENING(BgL_tmpz00_5936);
					}
					BgL_auxz00_5934 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5935);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5934))->
					BgL_interferez00);
			}
		}

	}



/* &rtl_reg/ra-interfere */
	obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5158, obj_t BgL_oz00_5159)
	{
		{	/* SawMill/regset.sch 60 */
			return
				BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5159));
		}

	}



/* rtl_reg/ra-interfere-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2debugzd2
		(BgL_rtl_regz00_bglt BgL_oz00_34, obj_t BgL_vz00_35)
	{
		{	/* SawMill/regset.sch 61 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_5943;

				{
					obj_t BgL_auxz00_5944;

					{	/* SawMill/regset.sch 61 */
						BgL_objectz00_bglt BgL_tmpz00_5945;

						BgL_tmpz00_5945 = ((BgL_objectz00_bglt) BgL_oz00_34);
						BgL_auxz00_5944 = BGL_OBJECT_WIDENING(BgL_tmpz00_5945);
					}
					BgL_auxz00_5943 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5944);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5943))->
						BgL_interferez00) = ((obj_t) BgL_vz00_35), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere-set! */
	obj_t BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5160, obj_t BgL_oz00_5161, obj_t BgL_vz00_5162)
	{
		{	/* SawMill/regset.sch 61 */
			return
				BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5161), BgL_vz00_5162);
		}

	}



/* rtl_reg/ra-occurrences */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_oz00_36)
	{
		{	/* SawMill/regset.sch 62 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_5952;

				{
					obj_t BgL_auxz00_5953;

					{	/* SawMill/regset.sch 62 */
						BgL_objectz00_bglt BgL_tmpz00_5954;

						BgL_tmpz00_5954 = ((BgL_objectz00_bglt) BgL_oz00_36);
						BgL_auxz00_5953 = BGL_OBJECT_WIDENING(BgL_tmpz00_5954);
					}
					BgL_auxz00_5952 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5953);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5952))->
					BgL_occurrencesz00);
			}
		}

	}



/* &rtl_reg/ra-occurrences */
	obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5163, obj_t BgL_oz00_5164)
	{
		{	/* SawMill/regset.sch 62 */
			return
				BINT(BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2debugzd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_5164)));
		}

	}



/* rtl_reg/ra-occurrences-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2debugzd2
		(BgL_rtl_regz00_bglt BgL_oz00_37, int BgL_vz00_38)
	{
		{	/* SawMill/regset.sch 63 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_5962;

				{
					obj_t BgL_auxz00_5963;

					{	/* SawMill/regset.sch 63 */
						BgL_objectz00_bglt BgL_tmpz00_5964;

						BgL_tmpz00_5964 = ((BgL_objectz00_bglt) BgL_oz00_37);
						BgL_auxz00_5963 = BGL_OBJECT_WIDENING(BgL_tmpz00_5964);
					}
					BgL_auxz00_5962 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5963);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5962))->
						BgL_occurrencesz00) = ((int) BgL_vz00_38), BUNSPEC);
		}}

	}



/* &rtl_reg/ra-occurrences-set! */
	obj_t BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5165, obj_t BgL_oz00_5166, obj_t BgL_vz00_5167)
	{
		{	/* SawMill/regset.sch 63 */
			return
				BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5166), CINT(BgL_vz00_5167));
		}

	}



/* rtl_reg/ra-coalesce */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_oz00_39)
	{
		{	/* SawMill/regset.sch 64 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_5972;

				{
					obj_t BgL_auxz00_5973;

					{	/* SawMill/regset.sch 64 */
						BgL_objectz00_bglt BgL_tmpz00_5974;

						BgL_tmpz00_5974 = ((BgL_objectz00_bglt) BgL_oz00_39);
						BgL_auxz00_5973 = BGL_OBJECT_WIDENING(BgL_tmpz00_5974);
					}
					BgL_auxz00_5972 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5973);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5972))->
					BgL_coalescez00);
			}
		}

	}



/* &rtl_reg/ra-coalesce */
	obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5168, obj_t BgL_oz00_5169)
	{
		{	/* SawMill/regset.sch 64 */
			return
				BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5169));
		}

	}



/* rtl_reg/ra-coalesce-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2debugzd2
		(BgL_rtl_regz00_bglt BgL_oz00_40, obj_t BgL_vz00_41)
	{
		{	/* SawMill/regset.sch 65 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_5981;

				{
					obj_t BgL_auxz00_5982;

					{	/* SawMill/regset.sch 65 */
						BgL_objectz00_bglt BgL_tmpz00_5983;

						BgL_tmpz00_5983 = ((BgL_objectz00_bglt) BgL_oz00_40);
						BgL_auxz00_5982 = BGL_OBJECT_WIDENING(BgL_tmpz00_5983);
					}
					BgL_auxz00_5981 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5982);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5981))->
						BgL_coalescez00) = ((obj_t) BgL_vz00_41), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-coalesce-set! */
	obj_t BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5170, obj_t BgL_oz00_5171, obj_t BgL_vz00_5172)
	{
		{	/* SawMill/regset.sch 65 */
			return
				BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5171), BgL_vz00_5172);
		}

	}



/* rtl_reg/ra-color */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_oz00_42)
	{
		{	/* SawMill/regset.sch 66 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_5990;

				{
					obj_t BgL_auxz00_5991;

					{	/* SawMill/regset.sch 66 */
						BgL_objectz00_bglt BgL_tmpz00_5992;

						BgL_tmpz00_5992 = ((BgL_objectz00_bglt) BgL_oz00_42);
						BgL_auxz00_5991 = BGL_OBJECT_WIDENING(BgL_tmpz00_5992);
					}
					BgL_auxz00_5990 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5991);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5990))->BgL_colorz00);
			}
		}

	}



/* &rtl_reg/ra-color */
	obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5173, obj_t BgL_oz00_5174)
	{
		{	/* SawMill/regset.sch 66 */
			return
				BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5174));
		}

	}



/* rtl_reg/ra-color-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_oz00_43, obj_t BgL_vz00_44)
	{
		{	/* SawMill/regset.sch 67 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_5999;

				{
					obj_t BgL_auxz00_6000;

					{	/* SawMill/regset.sch 67 */
						BgL_objectz00_bglt BgL_tmpz00_6001;

						BgL_tmpz00_6001 = ((BgL_objectz00_bglt) BgL_oz00_43);
						BgL_auxz00_6000 = BGL_OBJECT_WIDENING(BgL_tmpz00_6001);
					}
					BgL_auxz00_5999 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6000);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5999))->
						BgL_colorz00) = ((obj_t) BgL_vz00_44), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-color-set! */
	obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5175, obj_t BgL_oz00_5176, obj_t BgL_vz00_5177)
	{
		{	/* SawMill/regset.sch 67 */
			return
				BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5176), BgL_vz00_5177);
		}

	}



/* rtl_reg/ra-num */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_oz00_45)
	{
		{	/* SawMill/regset.sch 68 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_6008;

				{
					obj_t BgL_auxz00_6009;

					{	/* SawMill/regset.sch 68 */
						BgL_objectz00_bglt BgL_tmpz00_6010;

						BgL_tmpz00_6010 = ((BgL_objectz00_bglt) BgL_oz00_45);
						BgL_auxz00_6009 = BGL_OBJECT_WIDENING(BgL_tmpz00_6010);
					}
					BgL_auxz00_6008 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6009);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6008))->BgL_numz00);
			}
		}

	}



/* &rtl_reg/ra-num */
	obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5178,
		obj_t BgL_oz00_5179)
	{
		{	/* SawMill/regset.sch 68 */
			return
				BINT(BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2debugzd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_5179)));
		}

	}



/* rtl_reg/ra-hardware */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_oz00_48)
	{
		{	/* SawMill/regset.sch 70 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_48)))->BgL_hardwarez00);
		}

	}



/* &rtl_reg/ra-hardware */
	obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5180, obj_t BgL_oz00_5181)
	{
		{	/* SawMill/regset.sch 70 */
			return
				BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5181));
		}

	}



/* rtl_reg/ra-key */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_oz00_51)
	{
		{	/* SawMill/regset.sch 72 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_51)))->BgL_keyz00);
		}

	}



/* &rtl_reg/ra-key */
	obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5182,
		obj_t BgL_oz00_5183)
	{
		{	/* SawMill/regset.sch 72 */
			return
				BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5183));
		}

	}



/* rtl_reg/ra-name */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_oz00_54)
	{
		{	/* SawMill/regset.sch 74 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_54)))->BgL_namez00);
		}

	}



/* &rtl_reg/ra-name */
	obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5184,
		obj_t BgL_oz00_5185)
	{
		{	/* SawMill/regset.sch 74 */
			return
				BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5185));
		}

	}



/* rtl_reg/ra-onexpr? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_oz00_57)
	{
		{	/* SawMill/regset.sch 76 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_57)))->BgL_onexprzf3zf3);
		}

	}



/* &rtl_reg/ra-onexpr? */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5186, obj_t BgL_oz00_5187)
	{
		{	/* SawMill/regset.sch 76 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5187));
		}

	}



/* rtl_reg/ra-onexpr?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2debugzd2
		(BgL_rtl_regz00_bglt BgL_oz00_58, obj_t BgL_vz00_59)
	{
		{	/* SawMill/regset.sch 77 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_58)))->BgL_onexprzf3zf3) =
				((obj_t) BgL_vz00_59), BUNSPEC);
		}

	}



/* &rtl_reg/ra-onexpr?-set! */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5188, obj_t BgL_oz00_5189, obj_t BgL_vz00_5190)
	{
		{	/* SawMill/regset.sch 77 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5189), BgL_vz00_5190);
		}

	}



/* rtl_reg/ra-var */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_oz00_60)
	{
		{	/* SawMill/regset.sch 78 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_60)))->BgL_varz00);
		}

	}



/* &rtl_reg/ra-var */
	obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5191,
		obj_t BgL_oz00_5192)
	{
		{	/* SawMill/regset.sch 78 */
			return
				BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5192));
		}

	}



/* rtl_reg/ra-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_oz00_61, obj_t BgL_vz00_62)
	{
		{	/* SawMill/regset.sch 79 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_61)))->BgL_varz00) =
				((obj_t) BgL_vz00_62), BUNSPEC);
		}

	}



/* &rtl_reg/ra-var-set! */
	obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5193, obj_t BgL_oz00_5194, obj_t BgL_vz00_5195)
	{
		{	/* SawMill/regset.sch 79 */
			return
				BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5194), BgL_vz00_5195);
		}

	}



/* rtl_reg/ra-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_oz00_63)
	{
		{	/* SawMill/regset.sch 80 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_63)))->BgL_typez00);
		}

	}



/* &rtl_reg/ra-type */
	BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5196, obj_t BgL_oz00_5197)
	{
		{	/* SawMill/regset.sch 80 */
			return
				BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5197));
		}

	}



/* rtl_reg/ra-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_oz00_64, BgL_typez00_bglt BgL_vz00_65)
	{
		{	/* SawMill/regset.sch 81 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_64)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_65), BUNSPEC);
		}

	}



/* &rtl_reg/ra-type-set! */
	obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5198, obj_t BgL_oz00_5199, obj_t BgL_vz00_5200)
	{
		{	/* SawMill/regset.sch 81 */
			return
				BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_5199),
				((BgL_typez00_bglt) BgL_vz00_5200));
		}

	}



/* make-regset */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2debugzd2(int BgL_length1172z00_66,
		int BgL_msiza7e1173za7_67, obj_t BgL_regv1174z00_68,
		obj_t BgL_regl1175z00_69, obj_t BgL_string1176z00_70)
	{
		{	/* SawMill/regset.sch 84 */
			{	/* SawMill/regset.sch 84 */
				BgL_regsetz00_bglt BgL_new1168z00_5364;

				{	/* SawMill/regset.sch 84 */
					BgL_regsetz00_bglt BgL_new1167z00_5365;

					BgL_new1167z00_5365 =
						((BgL_regsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_regsetz00_bgl))));
					{	/* SawMill/regset.sch 84 */
						long BgL_arg1705z00_5366;

						BgL_arg1705z00_5366 = BGL_CLASS_NUM(BGl_regsetz00zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1167z00_5365), BgL_arg1705z00_5366);
					}
					BgL_new1168z00_5364 = BgL_new1167z00_5365;
				}
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_5364))->BgL_lengthz00) =
					((int) BgL_length1172z00_66), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_5364))->BgL_msiza7eza7) =
					((int) BgL_msiza7e1173za7_67), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_5364))->BgL_regvz00) =
					((obj_t) BgL_regv1174z00_68), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_5364))->BgL_reglz00) =
					((obj_t) BgL_regl1175z00_69), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_5364))->BgL_stringz00) =
					((obj_t) BgL_string1176z00_70), BUNSPEC);
				return BgL_new1168z00_5364;
			}
		}

	}



/* &make-regset */
	BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5201, obj_t BgL_length1172z00_5202,
		obj_t BgL_msiza7e1173za7_5203, obj_t BgL_regv1174z00_5204,
		obj_t BgL_regl1175z00_5205, obj_t BgL_string1176z00_5206)
	{
		{	/* SawMill/regset.sch 84 */
			return
				BGl_makezd2regsetzd2zzsaw_bbvzd2debugzd2(CINT(BgL_length1172z00_5202),
				CINT(BgL_msiza7e1173za7_5203), BgL_regv1174z00_5204,
				BgL_regl1175z00_5205, BgL_string1176z00_5206);
		}

	}



/* regset? */
	BGL_EXPORTED_DEF bool_t BGl_regsetzf3zf3zzsaw_bbvzd2debugzd2(obj_t
		BgL_objz00_71)
	{
		{	/* SawMill/regset.sch 85 */
			{	/* SawMill/regset.sch 85 */
				obj_t BgL_classz00_5367;

				BgL_classz00_5367 = BGl_regsetz00zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_71))
					{	/* SawMill/regset.sch 85 */
						BgL_objectz00_bglt BgL_arg1807z00_5368;

						BgL_arg1807z00_5368 = (BgL_objectz00_bglt) (BgL_objz00_71);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 85 */
								long BgL_idxz00_5369;

								BgL_idxz00_5369 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5368);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_5369 + 1L)) == BgL_classz00_5367);
							}
						else
							{	/* SawMill/regset.sch 85 */
								bool_t BgL_res2978z00_5372;

								{	/* SawMill/regset.sch 85 */
									obj_t BgL_oclassz00_5373;

									{	/* SawMill/regset.sch 85 */
										obj_t BgL_arg1815z00_5374;
										long BgL_arg1816z00_5375;

										BgL_arg1815z00_5374 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 85 */
											long BgL_arg1817z00_5376;

											BgL_arg1817z00_5376 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5368);
											BgL_arg1816z00_5375 = (BgL_arg1817z00_5376 - OBJECT_TYPE);
										}
										BgL_oclassz00_5373 =
											VECTOR_REF(BgL_arg1815z00_5374, BgL_arg1816z00_5375);
									}
									{	/* SawMill/regset.sch 85 */
										bool_t BgL__ortest_1115z00_5377;

										BgL__ortest_1115z00_5377 =
											(BgL_classz00_5367 == BgL_oclassz00_5373);
										if (BgL__ortest_1115z00_5377)
											{	/* SawMill/regset.sch 85 */
												BgL_res2978z00_5372 = BgL__ortest_1115z00_5377;
											}
										else
											{	/* SawMill/regset.sch 85 */
												long BgL_odepthz00_5378;

												{	/* SawMill/regset.sch 85 */
													obj_t BgL_arg1804z00_5379;

													BgL_arg1804z00_5379 = (BgL_oclassz00_5373);
													BgL_odepthz00_5378 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_5379);
												}
												if ((1L < BgL_odepthz00_5378))
													{	/* SawMill/regset.sch 85 */
														obj_t BgL_arg1802z00_5380;

														{	/* SawMill/regset.sch 85 */
															obj_t BgL_arg1803z00_5381;

															BgL_arg1803z00_5381 = (BgL_oclassz00_5373);
															BgL_arg1802z00_5380 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5381,
																1L);
														}
														BgL_res2978z00_5372 =
															(BgL_arg1802z00_5380 == BgL_classz00_5367);
													}
												else
													{	/* SawMill/regset.sch 85 */
														BgL_res2978z00_5372 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2978z00_5372;
							}
					}
				else
					{	/* SawMill/regset.sch 85 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &regset? */
	obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5207,
		obj_t BgL_objz00_5208)
	{
		{	/* SawMill/regset.sch 85 */
			return BBOOL(BGl_regsetzf3zf3zzsaw_bbvzd2debugzd2(BgL_objz00_5208));
		}

	}



/* regset-nil */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2debugzd2(void)
	{
		{	/* SawMill/regset.sch 86 */
			{	/* SawMill/regset.sch 86 */
				obj_t BgL_classz00_4153;

				BgL_classz00_4153 = BGl_regsetz00zzsaw_regsetz00;
				{	/* SawMill/regset.sch 86 */
					obj_t BgL__ortest_1117z00_4154;

					BgL__ortest_1117z00_4154 = BGL_CLASS_NIL(BgL_classz00_4153);
					if (CBOOL(BgL__ortest_1117z00_4154))
						{	/* SawMill/regset.sch 86 */
							return ((BgL_regsetz00_bglt) BgL__ortest_1117z00_4154);
						}
					else
						{	/* SawMill/regset.sch 86 */
							return
								((BgL_regsetz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4153));
						}
				}
			}
		}

	}



/* &regset-nil */
	BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5209)
	{
		{	/* SawMill/regset.sch 86 */
			return BGl_regsetzd2nilzd2zzsaw_bbvzd2debugzd2();
		}

	}



/* regset-string */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2debugzd2(BgL_regsetz00_bglt BgL_oz00_72)
	{
		{	/* SawMill/regset.sch 87 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_72))->BgL_stringz00);
		}

	}



/* &regset-string */
	obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5210,
		obj_t BgL_oz00_5211)
	{
		{	/* SawMill/regset.sch 87 */
			return
				BGl_regsetzd2stringzd2zzsaw_bbvzd2debugzd2(
				((BgL_regsetz00_bglt) BgL_oz00_5211));
		}

	}



/* regset-string-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2debugzd2(BgL_regsetz00_bglt
		BgL_oz00_73, obj_t BgL_vz00_74)
	{
		{	/* SawMill/regset.sch 88 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_73))->BgL_stringz00) =
				((obj_t) BgL_vz00_74), BUNSPEC);
		}

	}



/* &regset-string-set! */
	obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5212, obj_t BgL_oz00_5213, obj_t BgL_vz00_5214)
	{
		{	/* SawMill/regset.sch 88 */
			return
				BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2debugzd2(
				((BgL_regsetz00_bglt) BgL_oz00_5213), BgL_vz00_5214);
		}

	}



/* regset-regl */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2debugzd2(BgL_regsetz00_bglt BgL_oz00_75)
	{
		{	/* SawMill/regset.sch 89 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_75))->BgL_reglz00);
		}

	}



/* &regset-regl */
	obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5215,
		obj_t BgL_oz00_5216)
	{
		{	/* SawMill/regset.sch 89 */
			return
				BGl_regsetzd2reglzd2zzsaw_bbvzd2debugzd2(
				((BgL_regsetz00_bglt) BgL_oz00_5216));
		}

	}



/* regset-regv */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2debugzd2(BgL_regsetz00_bglt BgL_oz00_78)
	{
		{	/* SawMill/regset.sch 91 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_78))->BgL_regvz00);
		}

	}



/* &regset-regv */
	obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5217,
		obj_t BgL_oz00_5218)
	{
		{	/* SawMill/regset.sch 91 */
			return
				BGl_regsetzd2regvzd2zzsaw_bbvzd2debugzd2(
				((BgL_regsetz00_bglt) BgL_oz00_5218));
		}

	}



/* regset-msize */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2debugzd2(BgL_regsetz00_bglt BgL_oz00_81)
	{
		{	/* SawMill/regset.sch 93 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_81))->BgL_msiza7eza7);
		}

	}



/* &regset-msize */
	obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5219,
		obj_t BgL_oz00_5220)
	{
		{	/* SawMill/regset.sch 93 */
			return
				BINT(BGl_regsetzd2msiza7ez75zzsaw_bbvzd2debugzd2(
					((BgL_regsetz00_bglt) BgL_oz00_5220)));
		}

	}



/* regset-length */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2debugzd2(BgL_regsetz00_bglt BgL_oz00_84)
	{
		{	/* SawMill/regset.sch 95 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_84))->BgL_lengthz00);
		}

	}



/* &regset-length */
	obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5221,
		obj_t BgL_oz00_5222)
	{
		{	/* SawMill/regset.sch 95 */
			return
				BINT(BGl_regsetzd2lengthzd2zzsaw_bbvzd2debugzd2(
					((BgL_regsetz00_bglt) BgL_oz00_5222)));
		}

	}



/* regset-length-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2debugzd2(BgL_regsetz00_bglt
		BgL_oz00_85, int BgL_vz00_86)
	{
		{	/* SawMill/regset.sch 96 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_85))->BgL_lengthz00) =
				((int) BgL_vz00_86), BUNSPEC);
		}

	}



/* &regset-length-set! */
	obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5223, obj_t BgL_oz00_5224, obj_t BgL_vz00_5225)
	{
		{	/* SawMill/regset.sch 96 */
			return
				BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2debugzd2(
				((BgL_regsetz00_bglt) BgL_oz00_5224), CINT(BgL_vz00_5225));
		}

	}



/* make-empty-bbset */
	obj_t BGl_makezd2emptyzd2bbsetz00zzsaw_bbvzd2debugzd2(void)
	{
		{	/* SawMill/bbset.sch 20 */
			{	/* SawMill/bbset.sch 21 */
				obj_t BgL_arg1722z00_2249;

				BgL_arg1722z00_2249 = BGl_getzd2bbzd2markz00zzsaw_bbvzd2typeszd2();
				{	/* SawMill/bbset.sch 15 */
					obj_t BgL_newz00_4162;

					BgL_newz00_4162 = create_struct(CNST_TABLE_REF(0), (int) (2L));
					{	/* SawMill/bbset.sch 15 */
						int BgL_tmpz00_6126;

						BgL_tmpz00_6126 = (int) (1L);
						STRUCT_SET(BgL_newz00_4162, BgL_tmpz00_6126, BNIL);
					}
					{	/* SawMill/bbset.sch 15 */
						int BgL_tmpz00_6129;

						BgL_tmpz00_6129 = (int) (0L);
						STRUCT_SET(BgL_newz00_4162, BgL_tmpz00_6129, BgL_arg1722z00_2249);
					}
					return BgL_newz00_4162;
				}
			}
		}

	}



/* bbset-cons */
	obj_t BGl_bbsetzd2conszd2zzsaw_bbvzd2debugzd2(BgL_blockz00_bglt BgL_bz00_99,
		obj_t BgL_setz00_100)
	{
		{	/* SawMill/bbset.sch 33 */
			{	/* SawMill/bbset.sch 34 */
				obj_t BgL_mz00_2253;

				BgL_mz00_2253 = STRUCT_REF(BgL_setz00_100, (int) (0L));
				{
					BgL_blocksz00_bglt BgL_auxz00_6134;

					{
						obj_t BgL_auxz00_6135;

						{	/* SawMill/bbset.sch 36 */
							BgL_objectz00_bglt BgL_tmpz00_6136;

							BgL_tmpz00_6136 = ((BgL_objectz00_bglt) BgL_bz00_99);
							BgL_auxz00_6135 = BGL_OBJECT_WIDENING(BgL_tmpz00_6136);
						}
						BgL_auxz00_6134 = ((BgL_blocksz00_bglt) BgL_auxz00_6135);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_6134))->BgL_z52markz52) =
						((long) (long) CINT(BgL_mz00_2253)), BUNSPEC);
				}
				{	/* SawMill/bbset.sch 37 */
					obj_t BgL_arg1734z00_2255;

					{	/* SawMill/bbset.sch 37 */
						obj_t BgL_arg1735z00_2256;

						BgL_arg1735z00_2256 = STRUCT_REF(BgL_setz00_100, (int) (1L));
						BgL_arg1734z00_2255 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_99), BgL_arg1735z00_2256);
					}
					{	/* SawMill/bbset.sch 15 */
						int BgL_tmpz00_6146;

						BgL_tmpz00_6146 = (int) (1L);
						STRUCT_SET(BgL_setz00_100, BgL_tmpz00_6146, BgL_arg1734z00_2255);
				}}
				return BgL_setz00_100;
			}
		}

	}



/* gendebugid */
	BGL_EXPORTED_DEF obj_t BGl_gendebugidz00zzsaw_bbvzd2debugzd2(void)
	{
		{	/* SawBbv/bbv-debug.scm 67 */
			BGl_debugcntz00zzsaw_bbvzd2debugzd2 =
				(BGl_debugcntz00zzsaw_bbvzd2debugzd2 + 1L);
			return BINT(BGl_debugcntz00zzsaw_bbvzd2debugzd2);
		}

	}



/* &gendebugid */
	obj_t BGl_z62gendebugidz62zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5226)
	{
		{	/* SawBbv/bbv-debug.scm 67 */
			return BGl_gendebugidz00zzsaw_bbvzd2debugzd2();
		}

	}



/* assert-block */
	BGL_EXPORTED_DEF obj_t
		BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(BgL_blockz00_bglt BgL_bz00_105,
		obj_t BgL_stagez00_106)
	{
		{	/* SawBbv/bbv-debug.scm 77 */
			{	/* SawBbv/bbv-debug.scm 79 */
				bool_t BgL_test3283z00_6152;

				{
					BgL_blocksz00_bglt BgL_auxz00_6153;

					{
						obj_t BgL_auxz00_6154;

						{	/* SawBbv/bbv-debug.scm 79 */
							BgL_objectz00_bglt BgL_tmpz00_6155;

							BgL_tmpz00_6155 = ((BgL_objectz00_bglt) BgL_bz00_105);
							BgL_auxz00_6154 = BGL_OBJECT_WIDENING(BgL_tmpz00_6155);
						}
						BgL_auxz00_6153 = ((BgL_blocksz00_bglt) BgL_auxz00_6154);
					}
					BgL_test3283z00_6152 =
						(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_6153))->BgL_asleepz00);
				}
				if (BgL_test3283z00_6152)
					{	/* SawBbv/bbv-debug.scm 79 */
						return BFALSE;
					}
				else
					{	/* SawBbv/bbv-debug.scm 79 */
						{	/* SawBbv/bbv-debug.scm 81 */
							bool_t BgL_test3284z00_6160;

							{	/* SawBbv/bbv-debug.scm 81 */
								bool_t BgL_test3285z00_6161;

								{	/* SawBbv/bbv-debug.scm 81 */
									long BgL_arg1840z00_2310;
									long BgL_arg1842z00_2311;

									{
										BgL_blocksz00_bglt BgL_auxz00_6162;

										{
											obj_t BgL_auxz00_6163;

											{	/* SawBbv/bbv-debug.scm 81 */
												BgL_objectz00_bglt BgL_tmpz00_6164;

												BgL_tmpz00_6164 = ((BgL_objectz00_bglt) BgL_bz00_105);
												BgL_auxz00_6163 = BGL_OBJECT_WIDENING(BgL_tmpz00_6164);
											}
											BgL_auxz00_6162 = ((BgL_blocksz00_bglt) BgL_auxz00_6163);
										}
										BgL_arg1840z00_2310 =
											(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_6162))->
											BgL_gccntz00);
									}
									BgL_arg1842z00_2311 =
										bgl_list_length(
										(((BgL_blockz00_bglt) COBJECT(
													((BgL_blockz00_bglt) BgL_bz00_105)))->BgL_predsz00));
									BgL_test3285z00_6161 =
										(BgL_arg1840z00_2310 == BgL_arg1842z00_2311);
								}
								if (BgL_test3285z00_6161)
									{	/* SawBbv/bbv-debug.scm 81 */
										BgL_test3284z00_6160 = ((bool_t) 1);
									}
								else
									{	/* SawBbv/bbv-debug.scm 81 */
										obj_t BgL_arg1839z00_2309;

										{
											BgL_blocksz00_bglt BgL_auxz00_6173;

											{
												obj_t BgL_auxz00_6174;

												{	/* SawBbv/bbv-debug.scm 81 */
													BgL_objectz00_bglt BgL_tmpz00_6175;

													BgL_tmpz00_6175 = ((BgL_objectz00_bglt) BgL_bz00_105);
													BgL_auxz00_6174 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_6175);
												}
												BgL_auxz00_6173 =
													((BgL_blocksz00_bglt) BgL_auxz00_6174);
											}
											BgL_arg1839z00_2309 =
												(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_6173))->
												BgL_creatorz00);
										}
										BgL_test3284z00_6160 =
											(BgL_arg1839z00_2309 == CNST_TABLE_REF(1));
									}
							}
							if (BgL_test3284z00_6160)
								{	/* SawBbv/bbv-debug.scm 81 */
									BFALSE;
								}
							else
								{	/* SawBbv/bbv-debug.scm 81 */
									{	/* SawBbv/bbv-debug.scm 82 */
										obj_t BgL_arg1805z00_2292;
										obj_t BgL_arg1806z00_2293;

										{	/* SawBbv/bbv-debug.scm 82 */
											obj_t BgL_tmpz00_6182;

											BgL_tmpz00_6182 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_arg1805z00_2292 =
												BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6182);
										}
										BgL_arg1806z00_2293 =
											BGl_shapez00zztools_shapez00(((obj_t) BgL_bz00_105));
										{	/* SawBbv/bbv-debug.scm 82 */
											obj_t BgL_list1807z00_2294;

											{	/* SawBbv/bbv-debug.scm 82 */
												obj_t BgL_arg1808z00_2295;

												{	/* SawBbv/bbv-debug.scm 82 */
													obj_t BgL_arg1812z00_2296;

													{	/* SawBbv/bbv-debug.scm 82 */
														obj_t BgL_arg1820z00_2297;

														{	/* SawBbv/bbv-debug.scm 82 */
															obj_t BgL_arg1822z00_2298;

															BgL_arg1822z00_2298 =
																MAKE_YOUNG_PAIR(BgL_arg1806z00_2293, BNIL);
															BgL_arg1820z00_2297 =
																MAKE_YOUNG_PAIR
																(BGl_string3001z00zzsaw_bbvzd2debugzd2,
																BgL_arg1822z00_2298);
														}
														BgL_arg1812z00_2296 =
															MAKE_YOUNG_PAIR(BINT(82L), BgL_arg1820z00_2297);
													}
													BgL_arg1808z00_2295 =
														MAKE_YOUNG_PAIR
														(BGl_string3002z00zzsaw_bbvzd2debugzd2,
														BgL_arg1812z00_2296);
												}
												BgL_list1807z00_2294 =
													MAKE_YOUNG_PAIR(BGl_string3003z00zzsaw_bbvzd2debugzd2,
													BgL_arg1808z00_2295);
											}
											BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1805z00_2292,
												BgL_list1807z00_2294);
										}
									}
									{	/* SawBbv/bbv-debug.scm 84 */
										obj_t BgL_arg1823z00_2299;
										obj_t BgL_arg1831z00_2300;

										{	/* SawBbv/bbv-debug.scm 84 */
											int BgL_arg1832z00_2301;

											BgL_arg1832z00_2301 =
												(((BgL_blockz00_bglt) COBJECT(
														((BgL_blockz00_bglt) BgL_bz00_105)))->BgL_labelz00);
											{	/* SawBbv/bbv-debug.scm 84 */
												obj_t BgL_list1833z00_2302;

												BgL_list1833z00_2302 =
													MAKE_YOUNG_PAIR(BINT(BgL_arg1832z00_2301), BNIL);
												BgL_arg1823z00_2299 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string3004z00zzsaw_bbvzd2debugzd2,
													BgL_list1833z00_2302);
										}}
										{	/* SawBbv/bbv-debug.scm 85 */
											long BgL_arg1834z00_2303;
											long BgL_arg1835z00_2304;

											BgL_arg1834z00_2303 =
												bgl_list_length(
												(((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt) BgL_bz00_105)))->
													BgL_predsz00));
											{
												BgL_blocksz00_bglt BgL_auxz00_6202;

												{
													obj_t BgL_auxz00_6203;

													{	/* SawBbv/bbv-debug.scm 85 */
														BgL_objectz00_bglt BgL_tmpz00_6204;

														BgL_tmpz00_6204 =
															((BgL_objectz00_bglt) BgL_bz00_105);
														BgL_auxz00_6203 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_6204);
													}
													BgL_auxz00_6202 =
														((BgL_blocksz00_bglt) BgL_auxz00_6203);
												}
												BgL_arg1835z00_2304 =
													(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_6202))->
													BgL_gccntz00);
											}
											{	/* SawBbv/bbv-debug.scm 85 */
												obj_t BgL_list1836z00_2305;

												{	/* SawBbv/bbv-debug.scm 85 */
													obj_t BgL_arg1837z00_2306;

													BgL_arg1837z00_2306 =
														MAKE_YOUNG_PAIR(BINT(BgL_arg1835z00_2304), BNIL);
													BgL_list1836z00_2305 =
														MAKE_YOUNG_PAIR(BINT(BgL_arg1834z00_2303),
														BgL_arg1837z00_2306);
												}
												BgL_arg1831z00_2300 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string3005z00zzsaw_bbvzd2debugzd2,
													BgL_list1836z00_2305);
										}}
										BGl_errorz00zz__errorz00(BgL_stagez00_106,
											BgL_arg1823z00_2299, BgL_arg1831z00_2300);
						}}}
						{	/* SawBbv/bbv-debug.scm 87 */
							obj_t BgL_lz00_2313;

							{	/* SawBbv/bbv-debug.scm 87 */
								obj_t BgL_hook1480z00_2355;

								BgL_hook1480z00_2355 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
								{	/* SawBbv/bbv-debug.scm 87 */
									obj_t BgL_g1481z00_2356;

									BgL_g1481z00_2356 =
										(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt) BgL_bz00_105)))->BgL_predsz00);
									{
										obj_t BgL_l1477z00_2358;
										obj_t BgL_h1478z00_2359;

										BgL_l1477z00_2358 = BgL_g1481z00_2356;
										BgL_h1478z00_2359 = BgL_hook1480z00_2355;
									BgL_zc3z04anonymousza31877ze3z87_2360:
										if (NULLP(BgL_l1477z00_2358))
											{	/* SawBbv/bbv-debug.scm 87 */
												BgL_lz00_2313 = CDR(BgL_hook1480z00_2355);
											}
										else
											{	/* SawBbv/bbv-debug.scm 87 */
												bool_t BgL_test3287z00_6221;

												{	/* SawBbv/bbv-debug.scm 88 */
													BgL_blockz00_bglt BgL_i1173z00_2374;

													BgL_i1173z00_2374 =
														((BgL_blockz00_bglt)
														CAR(((obj_t) BgL_l1477z00_2358)));
													{	/* SawBbv/bbv-debug.scm 89 */
														bool_t BgL_test3288z00_6225;

														{
															BgL_blocksz00_bglt BgL_auxz00_6226;

															{
																obj_t BgL_auxz00_6227;

																{	/* SawBbv/bbv-debug.scm 89 */
																	BgL_objectz00_bglt BgL_tmpz00_6228;

																	BgL_tmpz00_6228 =
																		((BgL_objectz00_bglt) BgL_i1173z00_2374);
																	BgL_auxz00_6227 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_6228);
																}
																BgL_auxz00_6226 =
																	((BgL_blocksz00_bglt) BgL_auxz00_6227);
															}
															BgL_test3288z00_6225 =
																(((BgL_blocksz00_bglt)
																	COBJECT(BgL_auxz00_6226))->BgL_asleepz00);
														}
														if (BgL_test3288z00_6225)
															{	/* SawBbv/bbv-debug.scm 89 */
																BgL_test3287z00_6221 = ((bool_t) 0);
															}
														else
															{	/* SawBbv/bbv-debug.scm 89 */
																if (CBOOL
																	(BGl_memqz00zz__r4_pairs_and_lists_6_3z00((
																				(obj_t) BgL_bz00_105),
																			(((BgL_blockz00_bglt)
																					COBJECT(((BgL_blockz00_bglt)
																							BgL_i1173z00_2374)))->
																				BgL_succsz00))))
																	{	/* SawBbv/bbv-debug.scm 89 */
																		BgL_test3287z00_6221 = ((bool_t) 0);
																	}
																else
																	{	/* SawBbv/bbv-debug.scm 89 */
																		BgL_test3287z00_6221 = ((bool_t) 1);
																	}
															}
													}
												}
												if (BgL_test3287z00_6221)
													{	/* SawBbv/bbv-debug.scm 87 */
														obj_t BgL_nh1479z00_2369;

														{	/* SawBbv/bbv-debug.scm 87 */
															obj_t BgL_arg1885z00_2371;

															BgL_arg1885z00_2371 =
																CAR(((obj_t) BgL_l1477z00_2358));
															BgL_nh1479z00_2369 =
																MAKE_YOUNG_PAIR(BgL_arg1885z00_2371, BNIL);
														}
														SET_CDR(BgL_h1478z00_2359, BgL_nh1479z00_2369);
														{	/* SawBbv/bbv-debug.scm 87 */
															obj_t BgL_arg1884z00_2370;

															BgL_arg1884z00_2370 =
																CDR(((obj_t) BgL_l1477z00_2358));
															{
																obj_t BgL_h1478z00_6246;
																obj_t BgL_l1477z00_6245;

																BgL_l1477z00_6245 = BgL_arg1884z00_2370;
																BgL_h1478z00_6246 = BgL_nh1479z00_2369;
																BgL_h1478z00_2359 = BgL_h1478z00_6246;
																BgL_l1477z00_2358 = BgL_l1477z00_6245;
																goto BgL_zc3z04anonymousza31877ze3z87_2360;
															}
														}
													}
												else
													{	/* SawBbv/bbv-debug.scm 87 */
														obj_t BgL_arg1887z00_2372;

														BgL_arg1887z00_2372 =
															CDR(((obj_t) BgL_l1477z00_2358));
														{
															obj_t BgL_l1477z00_6249;

															BgL_l1477z00_6249 = BgL_arg1887z00_2372;
															BgL_l1477z00_2358 = BgL_l1477z00_6249;
															goto BgL_zc3z04anonymousza31877ze3z87_2360;
														}
													}
											}
									}
								}
							}
							if (PAIRP(BgL_lz00_2313))
								{	/* SawBbv/bbv-debug.scm 91 */
									{	/* SawBbv/bbv-debug.scm 92 */
										obj_t BgL_arg1845z00_2315;
										obj_t BgL_arg1846z00_2316;

										{	/* SawBbv/bbv-debug.scm 92 */
											obj_t BgL_tmpz00_6252;

											BgL_tmpz00_6252 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_arg1845z00_2315 =
												BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6252);
										}
										BgL_arg1846z00_2316 =
											BGl_shapez00zztools_shapez00(((obj_t) BgL_bz00_105));
										{	/* SawBbv/bbv-debug.scm 92 */
											obj_t BgL_list1847z00_2317;

											{	/* SawBbv/bbv-debug.scm 92 */
												obj_t BgL_arg1848z00_2318;

												{	/* SawBbv/bbv-debug.scm 92 */
													obj_t BgL_arg1849z00_2319;

													{	/* SawBbv/bbv-debug.scm 92 */
														obj_t BgL_arg1850z00_2320;

														{	/* SawBbv/bbv-debug.scm 92 */
															obj_t BgL_arg1851z00_2321;

															BgL_arg1851z00_2321 =
																MAKE_YOUNG_PAIR(BgL_arg1846z00_2316, BNIL);
															BgL_arg1850z00_2320 =
																MAKE_YOUNG_PAIR
																(BGl_string3001z00zzsaw_bbvzd2debugzd2,
																BgL_arg1851z00_2321);
														}
														BgL_arg1849z00_2319 =
															MAKE_YOUNG_PAIR(BINT(92L), BgL_arg1850z00_2320);
													}
													BgL_arg1848z00_2318 =
														MAKE_YOUNG_PAIR
														(BGl_string3002z00zzsaw_bbvzd2debugzd2,
														BgL_arg1849z00_2319);
												}
												BgL_list1847z00_2317 =
													MAKE_YOUNG_PAIR(BGl_string3003z00zzsaw_bbvzd2debugzd2,
													BgL_arg1848z00_2318);
											}
											BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1845z00_2315,
												BgL_list1847z00_2317);
										}
									}
									{
										obj_t BgL_l1482z00_2323;

										BgL_l1482z00_2323 = BgL_lz00_2313;
									BgL_zc3z04anonymousza31852ze3z87_2324:
										if (PAIRP(BgL_l1482z00_2323))
											{	/* SawBbv/bbv-debug.scm 93 */
												{	/* SawBbv/bbv-debug.scm 93 */
													obj_t BgL_bz00_2326;

													BgL_bz00_2326 = CAR(BgL_l1482z00_2323);
													{	/* SawBbv/bbv-debug.scm 93 */
														obj_t BgL_arg1854z00_2327;
														obj_t BgL_arg1856z00_2328;

														{	/* SawBbv/bbv-debug.scm 93 */
															obj_t BgL_tmpz00_6267;

															BgL_tmpz00_6267 = BGL_CURRENT_DYNAMIC_ENV();
															BgL_arg1854z00_2327 =
																BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6267);
														}
														BgL_arg1856z00_2328 =
															BGl_shapez00zztools_shapez00(BgL_bz00_2326);
														{	/* SawBbv/bbv-debug.scm 93 */
															obj_t BgL_list1857z00_2329;

															{	/* SawBbv/bbv-debug.scm 93 */
																obj_t BgL_arg1858z00_2330;

																{	/* SawBbv/bbv-debug.scm 93 */
																	obj_t BgL_arg1859z00_2331;

																	{	/* SawBbv/bbv-debug.scm 93 */
																		obj_t BgL_arg1860z00_2332;

																		{	/* SawBbv/bbv-debug.scm 93 */
																			obj_t BgL_arg1862z00_2333;

																			BgL_arg1862z00_2333 =
																				MAKE_YOUNG_PAIR(BgL_arg1856z00_2328,
																				BNIL);
																			BgL_arg1860z00_2332 =
																				MAKE_YOUNG_PAIR
																				(BGl_string3001z00zzsaw_bbvzd2debugzd2,
																				BgL_arg1862z00_2333);
																		}
																		BgL_arg1859z00_2331 =
																			MAKE_YOUNG_PAIR(BINT(93L),
																			BgL_arg1860z00_2332);
																	}
																	BgL_arg1858z00_2330 =
																		MAKE_YOUNG_PAIR
																		(BGl_string3002z00zzsaw_bbvzd2debugzd2,
																		BgL_arg1859z00_2331);
																}
																BgL_list1857z00_2329 =
																	MAKE_YOUNG_PAIR
																	(BGl_string3003z00zzsaw_bbvzd2debugzd2,
																	BgL_arg1858z00_2330);
															}
															BGl_tprintz00zz__r4_output_6_10_3z00
																(BgL_arg1854z00_2327, BgL_list1857z00_2329);
														}
													}
												}
												{
													obj_t BgL_l1482z00_6278;

													BgL_l1482z00_6278 = CDR(BgL_l1482z00_2323);
													BgL_l1482z00_2323 = BgL_l1482z00_6278;
													goto BgL_zc3z04anonymousza31852ze3z87_2324;
												}
											}
										else
											{	/* SawBbv/bbv-debug.scm 93 */
												((bool_t) 1);
											}
									}
									{	/* SawBbv/bbv-debug.scm 95 */
										obj_t BgL_arg1864z00_2336;
										obj_t BgL_arg1866z00_2337;

										{	/* SawBbv/bbv-debug.scm 95 */
											int BgL_arg1868z00_2338;

											BgL_arg1868z00_2338 =
												(((BgL_blockz00_bglt) COBJECT(
														((BgL_blockz00_bglt) BgL_bz00_105)))->BgL_labelz00);
											{	/* SawBbv/bbv-debug.scm 95 */
												obj_t BgL_list1869z00_2339;

												BgL_list1869z00_2339 =
													MAKE_YOUNG_PAIR(BINT(BgL_arg1868z00_2338), BNIL);
												BgL_arg1864z00_2336 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string3006z00zzsaw_bbvzd2debugzd2,
													BgL_list1869z00_2339);
										}}
										{	/* SawBbv/bbv-debug.scm 96 */
											obj_t BgL_head1486z00_2342;

											BgL_head1486z00_2342 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											{
												obj_t BgL_l1484z00_2344;
												obj_t BgL_tail1487z00_2345;

												BgL_l1484z00_2344 = BgL_lz00_2313;
												BgL_tail1487z00_2345 = BgL_head1486z00_2342;
											BgL_zc3z04anonymousza31871ze3z87_2346:
												if (NULLP(BgL_l1484z00_2344))
													{	/* SawBbv/bbv-debug.scm 96 */
														BgL_arg1866z00_2337 = CDR(BgL_head1486z00_2342);
													}
												else
													{	/* SawBbv/bbv-debug.scm 96 */
														obj_t BgL_newtail1488z00_2348;

														{	/* SawBbv/bbv-debug.scm 96 */
															obj_t BgL_arg1874z00_2350;

															{	/* SawBbv/bbv-debug.scm 96 */
																obj_t BgL_bz00_2351;

																BgL_bz00_2351 =
																	CAR(((obj_t) BgL_l1484z00_2344));
																{	/* SawBbv/bbv-debug.scm 96 */
																	int BgL_arg1875z00_2352;

																	BgL_arg1875z00_2352 =
																		(((BgL_blockz00_bglt) COBJECT(
																				((BgL_blockz00_bglt) BgL_bz00_2351)))->
																		BgL_labelz00);
																	{	/* SawBbv/bbv-debug.scm 96 */
																		obj_t BgL_list1876z00_2353;

																		BgL_list1876z00_2353 =
																			MAKE_YOUNG_PAIR(BINT(BgL_arg1875z00_2352),
																			BNIL);
																		BgL_arg1874z00_2350 =
																			BGl_formatz00zz__r4_output_6_10_3z00
																			(BGl_string3007z00zzsaw_bbvzd2debugzd2,
																			BgL_list1876z00_2353);
															}}}
															BgL_newtail1488z00_2348 =
																MAKE_YOUNG_PAIR(BgL_arg1874z00_2350, BNIL);
														}
														SET_CDR(BgL_tail1487z00_2345,
															BgL_newtail1488z00_2348);
														{	/* SawBbv/bbv-debug.scm 96 */
															obj_t BgL_arg1873z00_2349;

															BgL_arg1873z00_2349 =
																CDR(((obj_t) BgL_l1484z00_2344));
															{
																obj_t BgL_tail1487z00_6301;
																obj_t BgL_l1484z00_6300;

																BgL_l1484z00_6300 = BgL_arg1873z00_2349;
																BgL_tail1487z00_6301 = BgL_newtail1488z00_2348;
																BgL_tail1487z00_2345 = BgL_tail1487z00_6301;
																BgL_l1484z00_2344 = BgL_l1484z00_6300;
																goto BgL_zc3z04anonymousza31871ze3z87_2346;
															}
														}
													}
											}
										}
										BGl_errorz00zz__errorz00(BgL_stagez00_106,
											BgL_arg1864z00_2336, BgL_arg1866z00_2337);
									}
								}
							else
								{	/* SawBbv/bbv-debug.scm 91 */
									BFALSE;
								}
						}
						{	/* SawBbv/bbv-debug.scm 98 */
							obj_t BgL_lz00_2379;

							{	/* SawBbv/bbv-debug.scm 98 */
								obj_t BgL_hook1493z00_2421;

								BgL_hook1493z00_2421 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
								{	/* SawBbv/bbv-debug.scm 98 */
									obj_t BgL_g1494z00_2422;

									BgL_g1494z00_2422 =
										(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt) BgL_bz00_105)))->BgL_succsz00);
									{
										obj_t BgL_l1490z00_2424;
										obj_t BgL_h1491z00_2425;

										BgL_l1490z00_2424 = BgL_g1494z00_2422;
										BgL_h1491z00_2425 = BgL_hook1493z00_2421;
									BgL_zc3z04anonymousza31927ze3z87_2426:
										if (NULLP(BgL_l1490z00_2424))
											{	/* SawBbv/bbv-debug.scm 98 */
												BgL_lz00_2379 = CDR(BgL_hook1493z00_2421);
											}
										else
											{	/* SawBbv/bbv-debug.scm 98 */
												bool_t BgL_test3294z00_6309;

												{	/* SawBbv/bbv-debug.scm 99 */
													BgL_blockz00_bglt BgL_i1175z00_2440;

													BgL_i1175z00_2440 =
														((BgL_blockz00_bglt)
														CAR(((obj_t) BgL_l1490z00_2424)));
													{	/* SawBbv/bbv-debug.scm 100 */
														bool_t BgL_test3295z00_6313;

														{
															BgL_blocksz00_bglt BgL_auxz00_6314;

															{
																obj_t BgL_auxz00_6315;

																{	/* SawBbv/bbv-debug.scm 100 */
																	BgL_objectz00_bglt BgL_tmpz00_6316;

																	BgL_tmpz00_6316 =
																		((BgL_objectz00_bglt) BgL_i1175z00_2440);
																	BgL_auxz00_6315 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_6316);
																}
																BgL_auxz00_6314 =
																	((BgL_blocksz00_bglt) BgL_auxz00_6315);
															}
															BgL_test3295z00_6313 =
																(((BgL_blocksz00_bglt)
																	COBJECT(BgL_auxz00_6314))->BgL_asleepz00);
														}
														if (BgL_test3295z00_6313)
															{	/* SawBbv/bbv-debug.scm 100 */
																BgL_test3294z00_6309 = ((bool_t) 0);
															}
														else
															{	/* SawBbv/bbv-debug.scm 100 */
																if (CBOOL
																	(BGl_memqz00zz__r4_pairs_and_lists_6_3z00((
																				(obj_t) BgL_bz00_105),
																			(((BgL_blockz00_bglt)
																					COBJECT(((BgL_blockz00_bglt)
																							BgL_i1175z00_2440)))->
																				BgL_predsz00))))
																	{	/* SawBbv/bbv-debug.scm 100 */
																		BgL_test3294z00_6309 = ((bool_t) 0);
																	}
																else
																	{	/* SawBbv/bbv-debug.scm 100 */
																		BgL_test3294z00_6309 = ((bool_t) 1);
																	}
															}
													}
												}
												if (BgL_test3294z00_6309)
													{	/* SawBbv/bbv-debug.scm 98 */
														obj_t BgL_nh1492z00_2435;

														{	/* SawBbv/bbv-debug.scm 98 */
															obj_t BgL_arg1934z00_2437;

															BgL_arg1934z00_2437 =
																CAR(((obj_t) BgL_l1490z00_2424));
															BgL_nh1492z00_2435 =
																MAKE_YOUNG_PAIR(BgL_arg1934z00_2437, BNIL);
														}
														SET_CDR(BgL_h1491z00_2425, BgL_nh1492z00_2435);
														{	/* SawBbv/bbv-debug.scm 98 */
															obj_t BgL_arg1933z00_2436;

															BgL_arg1933z00_2436 =
																CDR(((obj_t) BgL_l1490z00_2424));
															{
																obj_t BgL_h1491z00_6334;
																obj_t BgL_l1490z00_6333;

																BgL_l1490z00_6333 = BgL_arg1933z00_2436;
																BgL_h1491z00_6334 = BgL_nh1492z00_2435;
																BgL_h1491z00_2425 = BgL_h1491z00_6334;
																BgL_l1490z00_2424 = BgL_l1490z00_6333;
																goto BgL_zc3z04anonymousza31927ze3z87_2426;
															}
														}
													}
												else
													{	/* SawBbv/bbv-debug.scm 98 */
														obj_t BgL_arg1935z00_2438;

														BgL_arg1935z00_2438 =
															CDR(((obj_t) BgL_l1490z00_2424));
														{
															obj_t BgL_l1490z00_6337;

															BgL_l1490z00_6337 = BgL_arg1935z00_2438;
															BgL_l1490z00_2424 = BgL_l1490z00_6337;
															goto BgL_zc3z04anonymousza31927ze3z87_2426;
														}
													}
											}
									}
								}
							}
							if (PAIRP(BgL_lz00_2379))
								{	/* SawBbv/bbv-debug.scm 102 */
									{	/* SawBbv/bbv-debug.scm 103 */
										obj_t BgL_arg1890z00_2381;
										obj_t BgL_arg1891z00_2382;

										{	/* SawBbv/bbv-debug.scm 103 */
											obj_t BgL_tmpz00_6340;

											BgL_tmpz00_6340 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_arg1890z00_2381 =
												BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6340);
										}
										BgL_arg1891z00_2382 =
											BGl_shapez00zztools_shapez00(((obj_t) BgL_bz00_105));
										{	/* SawBbv/bbv-debug.scm 103 */
											obj_t BgL_list1892z00_2383;

											{	/* SawBbv/bbv-debug.scm 103 */
												obj_t BgL_arg1893z00_2384;

												{	/* SawBbv/bbv-debug.scm 103 */
													obj_t BgL_arg1894z00_2385;

													{	/* SawBbv/bbv-debug.scm 103 */
														obj_t BgL_arg1896z00_2386;

														{	/* SawBbv/bbv-debug.scm 103 */
															obj_t BgL_arg1897z00_2387;

															BgL_arg1897z00_2387 =
																MAKE_YOUNG_PAIR(BgL_arg1891z00_2382, BNIL);
															BgL_arg1896z00_2386 =
																MAKE_YOUNG_PAIR
																(BGl_string3001z00zzsaw_bbvzd2debugzd2,
																BgL_arg1897z00_2387);
														}
														BgL_arg1894z00_2385 =
															MAKE_YOUNG_PAIR(BINT(103L), BgL_arg1896z00_2386);
													}
													BgL_arg1893z00_2384 =
														MAKE_YOUNG_PAIR
														(BGl_string3002z00zzsaw_bbvzd2debugzd2,
														BgL_arg1894z00_2385);
												}
												BgL_list1892z00_2383 =
													MAKE_YOUNG_PAIR(BGl_string3003z00zzsaw_bbvzd2debugzd2,
													BgL_arg1893z00_2384);
											}
											BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1890z00_2381,
												BgL_list1892z00_2383);
										}
									}
									{
										obj_t BgL_l1495z00_2389;

										BgL_l1495z00_2389 = BgL_lz00_2379;
									BgL_zc3z04anonymousza31898ze3z87_2390:
										if (PAIRP(BgL_l1495z00_2389))
											{	/* SawBbv/bbv-debug.scm 104 */
												{	/* SawBbv/bbv-debug.scm 104 */
													obj_t BgL_bz00_2392;

													BgL_bz00_2392 = CAR(BgL_l1495z00_2389);
													{	/* SawBbv/bbv-debug.scm 104 */
														obj_t BgL_arg1901z00_2393;
														obj_t BgL_arg1902z00_2394;

														{	/* SawBbv/bbv-debug.scm 104 */
															obj_t BgL_tmpz00_6355;

															BgL_tmpz00_6355 = BGL_CURRENT_DYNAMIC_ENV();
															BgL_arg1901z00_2393 =
																BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6355);
														}
														BgL_arg1902z00_2394 =
															BGl_shapez00zztools_shapez00(BgL_bz00_2392);
														{	/* SawBbv/bbv-debug.scm 104 */
															obj_t BgL_list1903z00_2395;

															{	/* SawBbv/bbv-debug.scm 104 */
																obj_t BgL_arg1904z00_2396;

																{	/* SawBbv/bbv-debug.scm 104 */
																	obj_t BgL_arg1906z00_2397;

																	{	/* SawBbv/bbv-debug.scm 104 */
																		obj_t BgL_arg1910z00_2398;

																		{	/* SawBbv/bbv-debug.scm 104 */
																			obj_t BgL_arg1911z00_2399;

																			BgL_arg1911z00_2399 =
																				MAKE_YOUNG_PAIR(BgL_arg1902z00_2394,
																				BNIL);
																			BgL_arg1910z00_2398 =
																				MAKE_YOUNG_PAIR
																				(BGl_string3001z00zzsaw_bbvzd2debugzd2,
																				BgL_arg1911z00_2399);
																		}
																		BgL_arg1906z00_2397 =
																			MAKE_YOUNG_PAIR(BINT(104L),
																			BgL_arg1910z00_2398);
																	}
																	BgL_arg1904z00_2396 =
																		MAKE_YOUNG_PAIR
																		(BGl_string3002z00zzsaw_bbvzd2debugzd2,
																		BgL_arg1906z00_2397);
																}
																BgL_list1903z00_2395 =
																	MAKE_YOUNG_PAIR
																	(BGl_string3003z00zzsaw_bbvzd2debugzd2,
																	BgL_arg1904z00_2396);
															}
															BGl_tprintz00zz__r4_output_6_10_3z00
																(BgL_arg1901z00_2393, BgL_list1903z00_2395);
														}
													}
												}
												{
													obj_t BgL_l1495z00_6366;

													BgL_l1495z00_6366 = CDR(BgL_l1495z00_2389);
													BgL_l1495z00_2389 = BgL_l1495z00_6366;
													goto BgL_zc3z04anonymousza31898ze3z87_2390;
												}
											}
										else
											{	/* SawBbv/bbv-debug.scm 104 */
												((bool_t) 1);
											}
									}
									{	/* SawBbv/bbv-debug.scm 106 */
										obj_t BgL_arg1913z00_2402;
										obj_t BgL_arg1914z00_2403;

										{	/* SawBbv/bbv-debug.scm 106 */
											int BgL_arg1916z00_2404;

											BgL_arg1916z00_2404 =
												(((BgL_blockz00_bglt) COBJECT(
														((BgL_blockz00_bglt) BgL_bz00_105)))->BgL_labelz00);
											{	/* SawBbv/bbv-debug.scm 106 */
												obj_t BgL_list1917z00_2405;

												BgL_list1917z00_2405 =
													MAKE_YOUNG_PAIR(BINT(BgL_arg1916z00_2404), BNIL);
												BgL_arg1913z00_2402 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string3008z00zzsaw_bbvzd2debugzd2,
													BgL_list1917z00_2405);
										}}
										{	/* SawBbv/bbv-debug.scm 107 */
											obj_t BgL_head1499z00_2408;

											BgL_head1499z00_2408 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											{
												obj_t BgL_l1497z00_2410;
												obj_t BgL_tail1500z00_2411;

												BgL_l1497z00_2410 = BgL_lz00_2379;
												BgL_tail1500z00_2411 = BgL_head1499z00_2408;
											BgL_zc3z04anonymousza31919ze3z87_2412:
												if (NULLP(BgL_l1497z00_2410))
													{	/* SawBbv/bbv-debug.scm 107 */
														BgL_arg1914z00_2403 = CDR(BgL_head1499z00_2408);
													}
												else
													{	/* SawBbv/bbv-debug.scm 107 */
														obj_t BgL_newtail1501z00_2414;

														{	/* SawBbv/bbv-debug.scm 107 */
															obj_t BgL_arg1924z00_2416;

															{	/* SawBbv/bbv-debug.scm 107 */
																obj_t BgL_bz00_2417;

																BgL_bz00_2417 =
																	CAR(((obj_t) BgL_l1497z00_2410));
																{	/* SawBbv/bbv-debug.scm 107 */
																	int BgL_arg1925z00_2418;

																	BgL_arg1925z00_2418 =
																		(((BgL_blockz00_bglt) COBJECT(
																				((BgL_blockz00_bglt) BgL_bz00_2417)))->
																		BgL_labelz00);
																	{	/* SawBbv/bbv-debug.scm 107 */
																		obj_t BgL_list1926z00_2419;

																		BgL_list1926z00_2419 =
																			MAKE_YOUNG_PAIR(BINT(BgL_arg1925z00_2418),
																			BNIL);
																		BgL_arg1924z00_2416 =
																			BGl_formatz00zz__r4_output_6_10_3z00
																			(BGl_string3007z00zzsaw_bbvzd2debugzd2,
																			BgL_list1926z00_2419);
															}}}
															BgL_newtail1501z00_2414 =
																MAKE_YOUNG_PAIR(BgL_arg1924z00_2416, BNIL);
														}
														SET_CDR(BgL_tail1500z00_2411,
															BgL_newtail1501z00_2414);
														{	/* SawBbv/bbv-debug.scm 107 */
															obj_t BgL_arg1923z00_2415;

															BgL_arg1923z00_2415 =
																CDR(((obj_t) BgL_l1497z00_2410));
															{
																obj_t BgL_tail1500z00_6389;
																obj_t BgL_l1497z00_6388;

																BgL_l1497z00_6388 = BgL_arg1923z00_2415;
																BgL_tail1500z00_6389 = BgL_newtail1501z00_2414;
																BgL_tail1500z00_2411 = BgL_tail1500z00_6389;
																BgL_l1497z00_2410 = BgL_l1497z00_6388;
																goto BgL_zc3z04anonymousza31919ze3z87_2412;
															}
														}
													}
											}
										}
										BGl_errorz00zz__errorz00(BgL_stagez00_106,
											BgL_arg1913z00_2402, BgL_arg1914z00_2403);
									}
								}
							else
								{	/* SawBbv/bbv-debug.scm 102 */
									BFALSE;
								}
						}
						{	/* SawBbv/bbv-debug.scm 109 */
							obj_t BgL_lz00_2445;

							BgL_lz00_2445 = BNIL;
							if (BGl_blockzd2livezf3z21zzsaw_bbvzd2typeszd2(BgL_bz00_105))
								{	/* SawBbv/bbv-debug.scm 110 */
									{	/* SawBbv/bbv-debug.scm 111 */
										obj_t BgL_g1507z00_2447;

										BgL_g1507z00_2447 =
											(((BgL_blockz00_bglt) COBJECT(
													((BgL_blockz00_bglt) BgL_bz00_105)))->BgL_firstz00);
										{
											obj_t BgL_l1505z00_2449;

											BgL_l1505z00_2449 = BgL_g1507z00_2447;
										BgL_zc3z04anonymousza31938ze3z87_2450:
											if (PAIRP(BgL_l1505z00_2449))
												{	/* SawBbv/bbv-debug.scm 111 */
													{	/* SawBbv/bbv-debug.scm 113 */
														obj_t BgL_insz00_2452;

														BgL_insz00_2452 = CAR(BgL_l1505z00_2449);
														if (BGl_rtl_inszd2ifeqzf3z21zzsaw_bbvzd2typeszd2(
																((BgL_rtl_insz00_bglt) BgL_insz00_2452)))
															{	/* SawBbv/bbv-debug.scm 113 */
																BgL_lz00_2445 =
																	MAKE_YOUNG_PAIR(BgL_insz00_2452,
																	BgL_lz00_2445);
															}
														else
															{	/* SawBbv/bbv-debug.scm 113 */
																if (BGl_rtl_inszd2ifnezf3z21zzsaw_bbvzd2typeszd2
																	(((BgL_rtl_insz00_bglt) BgL_insz00_2452)))
																	{	/* SawBbv/bbv-debug.scm 117 */
																		BgL_rtl_ifnez00_bglt BgL_i1178z00_2456;

																		BgL_i1178z00_2456 =
																			((BgL_rtl_ifnez00_bglt)
																			(((BgL_rtl_insz00_bglt) COBJECT(
																						((BgL_rtl_insz00_bglt)
																							BgL_insz00_2452)))->BgL_funz00));
																		if (CBOOL
																			(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
																					((obj_t) (((BgL_rtl_ifnez00_bglt)
																								COBJECT(BgL_i1178z00_2456))->
																							BgL_thenz00)),
																					(((BgL_blockz00_bglt)
																							COBJECT(((BgL_blockz00_bglt)
																									BgL_bz00_105)))->
																						BgL_succsz00))))
																			{	/* SawBbv/bbv-debug.scm 118 */
																				BFALSE;
																			}
																		else
																			{	/* SawBbv/bbv-debug.scm 118 */
																				BgL_lz00_2445 =
																					MAKE_YOUNG_PAIR(BgL_insz00_2452,
																					BgL_lz00_2445);
																			}
																	}
																else
																	{	/* SawBbv/bbv-debug.scm 115 */
																		if (BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2(((BgL_rtl_insz00_bglt) BgL_insz00_2452)))
																			{	/* SawBbv/bbv-debug.scm 122 */
																				BgL_rtl_goz00_bglt BgL_i1180z00_2464;

																				BgL_i1180z00_2464 =
																					((BgL_rtl_goz00_bglt)
																					(((BgL_rtl_insz00_bglt) COBJECT(
																								((BgL_rtl_insz00_bglt)
																									BgL_insz00_2452)))->
																						BgL_funz00));
																				if (CBOOL
																					(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																						(((obj_t) (((BgL_rtl_goz00_bglt)
																										COBJECT
																										(BgL_i1180z00_2464))->
																									BgL_toz00)),
																							(((BgL_blockz00_bglt)
																									COBJECT(((BgL_blockz00_bglt)
																											BgL_bz00_105)))->
																								BgL_succsz00))))
																					{	/* SawBbv/bbv-debug.scm 123 */
																						BFALSE;
																					}
																				else
																					{	/* SawBbv/bbv-debug.scm 123 */
																						BgL_lz00_2445 =
																							MAKE_YOUNG_PAIR(BgL_insz00_2452,
																							BgL_lz00_2445);
																					}
																			}
																		else
																			{	/* SawBbv/bbv-debug.scm 120 */
																				if (CBOOL
																					(BGl_rtl_inszd2switchzf3z21zzsaw_bbvzd2typeszd2
																						(((BgL_rtl_insz00_bglt)
																								BgL_insz00_2452))))
																					{	/* SawBbv/bbv-debug.scm 127 */
																						BgL_rtl_switchz00_bglt
																							BgL_i1182z00_2472;
																						BgL_i1182z00_2472 =
																							((BgL_rtl_switchz00_bglt) ((
																									(BgL_rtl_insz00_bglt)
																									COBJECT(((BgL_rtl_insz00_bglt)
																											BgL_insz00_2452)))->
																								BgL_funz00));
																						{	/* SawBbv/bbv-debug.scm 128 */
																							obj_t BgL_g1504z00_2473;

																							BgL_g1504z00_2473 =
																								(((BgL_rtl_switchz00_bglt)
																									COBJECT(BgL_i1182z00_2472))->
																								BgL_labelsz00);
																							{
																								obj_t BgL_l1502z00_2475;

																								{	/* SawBbv/bbv-debug.scm 128 */
																									bool_t BgL_tmpz00_6438;

																									BgL_l1502z00_2475 =
																										BgL_g1504z00_2473;
																								BgL_zc3z04anonymousza31954ze3z87_2476:
																									if (PAIRP
																										(BgL_l1502z00_2475))
																										{	/* SawBbv/bbv-debug.scm 128 */
																											{	/* SawBbv/bbv-debug.scm 129 */
																												obj_t BgL_lblz00_2478;

																												BgL_lblz00_2478 =
																													CAR
																													(BgL_l1502z00_2475);
																												if (CBOOL
																													(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																														(BgL_lblz00_2478,
																															(((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) BgL_bz00_105)))->BgL_succsz00))))
																													{	/* SawBbv/bbv-debug.scm 129 */
																														BFALSE;
																													}
																												else
																													{	/* SawBbv/bbv-debug.scm 129 */
																														BgL_lz00_2445 =
																															MAKE_YOUNG_PAIR
																															(BgL_insz00_2452,
																															BgL_lz00_2445);
																													}
																											}
																											{
																												obj_t BgL_l1502z00_6448;

																												BgL_l1502z00_6448 =
																													CDR
																													(BgL_l1502z00_2475);
																												BgL_l1502z00_2475 =
																													BgL_l1502z00_6448;
																												goto
																													BgL_zc3z04anonymousza31954ze3z87_2476;
																											}
																										}
																									else
																										{	/* SawBbv/bbv-debug.scm 128 */
																											BgL_tmpz00_6438 =
																												((bool_t) 1);
																										}
																									BBOOL(BgL_tmpz00_6438);
																								}
																							}
																						}
																					}
																				else
																					{	/* SawBbv/bbv-debug.scm 125 */
																						BFALSE;
																					}
																			}
																	}
															}
													}
													{
														obj_t BgL_l1505z00_6451;

														BgL_l1505z00_6451 = CDR(BgL_l1505z00_2449);
														BgL_l1505z00_2449 = BgL_l1505z00_6451;
														goto BgL_zc3z04anonymousza31938ze3z87_2450;
													}
												}
											else
												{	/* SawBbv/bbv-debug.scm 111 */
													((bool_t) 1);
												}
										}
									}
									if (NULLP(BgL_lz00_2445))
										{	/* SawBbv/bbv-debug.scm 133 */
											return BFALSE;
										}
									else
										{	/* SawBbv/bbv-debug.scm 133 */
											{	/* SawBbv/bbv-debug.scm 134 */
												obj_t BgL_arg1962z00_2487;
												obj_t BgL_arg1963z00_2488;

												{	/* SawBbv/bbv-debug.scm 134 */
													obj_t BgL_tmpz00_6455;

													BgL_tmpz00_6455 = BGL_CURRENT_DYNAMIC_ENV();
													BgL_arg1962z00_2487 =
														BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6455);
												}
												BgL_arg1963z00_2488 =
													BGl_shapez00zztools_shapez00(((obj_t) BgL_bz00_105));
												{	/* SawBbv/bbv-debug.scm 134 */
													obj_t BgL_list1964z00_2489;

													{	/* SawBbv/bbv-debug.scm 134 */
														obj_t BgL_arg1965z00_2490;

														{	/* SawBbv/bbv-debug.scm 134 */
															obj_t BgL_arg1966z00_2491;

															{	/* SawBbv/bbv-debug.scm 134 */
																obj_t BgL_arg1967z00_2492;

																{	/* SawBbv/bbv-debug.scm 134 */
																	obj_t BgL_arg1968z00_2493;

																	{	/* SawBbv/bbv-debug.scm 134 */
																		obj_t BgL_arg1969z00_2494;

																		BgL_arg1969z00_2494 =
																			MAKE_YOUNG_PAIR(BgL_arg1963z00_2488,
																			BNIL);
																		BgL_arg1968z00_2493 =
																			MAKE_YOUNG_PAIR
																			(BGl_string3009z00zzsaw_bbvzd2debugzd2,
																			BgL_arg1969z00_2494);
																	}
																	BgL_arg1967z00_2492 =
																		MAKE_YOUNG_PAIR
																		(BGl_string3001z00zzsaw_bbvzd2debugzd2,
																		BgL_arg1968z00_2493);
																}
																BgL_arg1966z00_2491 =
																	MAKE_YOUNG_PAIR(BINT(134L),
																	BgL_arg1967z00_2492);
															}
															BgL_arg1965z00_2490 =
																MAKE_YOUNG_PAIR
																(BGl_string3002z00zzsaw_bbvzd2debugzd2,
																BgL_arg1966z00_2491);
														}
														BgL_list1964z00_2489 =
															MAKE_YOUNG_PAIR
															(BGl_string3003z00zzsaw_bbvzd2debugzd2,
															BgL_arg1965z00_2490);
													}
													BGl_tprintz00zz__r4_output_6_10_3z00
														(BgL_arg1962z00_2487, BgL_list1964z00_2489);
												}
											}
											{	/* SawBbv/bbv-debug.scm 135 */
												obj_t BgL_arg1970z00_2495;
												obj_t BgL_arg1971z00_2496;

												{	/* SawBbv/bbv-debug.scm 135 */
													obj_t BgL_tmpz00_6468;

													BgL_tmpz00_6468 = BGL_CURRENT_DYNAMIC_ENV();
													BgL_arg1970z00_2495 =
														BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_6468);
												}
												{	/* SawBbv/bbv-debug.scm 135 */
													BgL_blockz00_bglt BgL_arg1978z00_2503;

													{
														BgL_blocksz00_bglt BgL_auxz00_6471;

														{
															obj_t BgL_auxz00_6472;

															{	/* SawBbv/bbv-debug.scm 135 */
																BgL_objectz00_bglt BgL_tmpz00_6473;

																BgL_tmpz00_6473 =
																	((BgL_objectz00_bglt) BgL_bz00_105);
																BgL_auxz00_6472 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_6473);
															}
															BgL_auxz00_6471 =
																((BgL_blocksz00_bglt) BgL_auxz00_6472);
														}
														BgL_arg1978z00_2503 =
															(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_6471))->
															BgL_parentz00);
													}
													BgL_arg1971z00_2496 =
														BGl_shapez00zztools_shapez00(
														((obj_t) BgL_arg1978z00_2503));
												}
												{	/* SawBbv/bbv-debug.scm 135 */
													obj_t BgL_list1972z00_2497;

													{	/* SawBbv/bbv-debug.scm 135 */
														obj_t BgL_arg1973z00_2498;

														{	/* SawBbv/bbv-debug.scm 135 */
															obj_t BgL_arg1974z00_2499;

															{	/* SawBbv/bbv-debug.scm 135 */
																obj_t BgL_arg1975z00_2500;

																{	/* SawBbv/bbv-debug.scm 135 */
																	obj_t BgL_arg1976z00_2501;

																	{	/* SawBbv/bbv-debug.scm 135 */
																		obj_t BgL_arg1977z00_2502;

																		BgL_arg1977z00_2502 =
																			MAKE_YOUNG_PAIR(BgL_arg1971z00_2496,
																			BNIL);
																		BgL_arg1976z00_2501 =
																			MAKE_YOUNG_PAIR
																			(BGl_string3010z00zzsaw_bbvzd2debugzd2,
																			BgL_arg1977z00_2502);
																	}
																	BgL_arg1975z00_2500 =
																		MAKE_YOUNG_PAIR
																		(BGl_string3001z00zzsaw_bbvzd2debugzd2,
																		BgL_arg1976z00_2501);
																}
																BgL_arg1974z00_2499 =
																	MAKE_YOUNG_PAIR(BINT(135L),
																	BgL_arg1975z00_2500);
															}
															BgL_arg1973z00_2498 =
																MAKE_YOUNG_PAIR
																(BGl_string3002z00zzsaw_bbvzd2debugzd2,
																BgL_arg1974z00_2499);
														}
														BgL_list1972z00_2497 =
															MAKE_YOUNG_PAIR
															(BGl_string3003z00zzsaw_bbvzd2debugzd2,
															BgL_arg1973z00_2498);
													}
													BGl_tprintz00zz__r4_output_6_10_3z00
														(BgL_arg1970z00_2495, BgL_list1972z00_2497);
												}
											}
											{	/* SawBbv/bbv-debug.scm 137 */
												obj_t BgL_arg1979z00_2504;
												obj_t BgL_arg1980z00_2505;

												{	/* SawBbv/bbv-debug.scm 137 */
													int BgL_arg1981z00_2506;

													BgL_arg1981z00_2506 =
														(((BgL_blockz00_bglt) COBJECT(
																((BgL_blockz00_bglt) BgL_bz00_105)))->
														BgL_labelz00);
													{	/* SawBbv/bbv-debug.scm 137 */
														obj_t BgL_list1982z00_2507;

														BgL_list1982z00_2507 =
															MAKE_YOUNG_PAIR(BINT(BgL_arg1981z00_2506), BNIL);
														BgL_arg1979z00_2504 =
															BGl_formatz00zz__r4_output_6_10_3z00
															(BGl_string3011z00zzsaw_bbvzd2debugzd2,
															BgL_list1982z00_2507);
												}}
												{	/* SawBbv/bbv-debug.scm 138 */
													obj_t BgL_l1508z00_2508;

													BgL_l1508z00_2508 = BgL_lz00_2445;
													{	/* SawBbv/bbv-debug.scm 138 */
														obj_t BgL_head1510z00_2510;

														BgL_head1510z00_2510 =
															MAKE_YOUNG_PAIR(BGl_shapez00zztools_shapez00(CAR
																(BgL_l1508z00_2508)), BNIL);
														{	/* SawBbv/bbv-debug.scm 138 */
															obj_t BgL_g1513z00_2511;

															BgL_g1513z00_2511 = CDR(BgL_l1508z00_2508);
															{
																obj_t BgL_l1508z00_2513;
																obj_t BgL_tail1511z00_2514;

																BgL_l1508z00_2513 = BgL_g1513z00_2511;
																BgL_tail1511z00_2514 = BgL_head1510z00_2510;
															BgL_zc3z04anonymousza31984ze3z87_2515:
																if (NULLP(BgL_l1508z00_2513))
																	{	/* SawBbv/bbv-debug.scm 138 */
																		BgL_arg1980z00_2505 = BgL_head1510z00_2510;
																	}
																else
																	{	/* SawBbv/bbv-debug.scm 138 */
																		obj_t BgL_newtail1512z00_2517;

																		{	/* SawBbv/bbv-debug.scm 138 */
																			obj_t BgL_arg1987z00_2519;

																			{	/* SawBbv/bbv-debug.scm 138 */
																				obj_t BgL_arg1988z00_2520;

																				BgL_arg1988z00_2520 =
																					CAR(((obj_t) BgL_l1508z00_2513));
																				BgL_arg1987z00_2519 =
																					BGl_shapez00zztools_shapez00
																					(BgL_arg1988z00_2520);
																			}
																			BgL_newtail1512z00_2517 =
																				MAKE_YOUNG_PAIR(BgL_arg1987z00_2519,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1511z00_2514,
																			BgL_newtail1512z00_2517);
																		{	/* SawBbv/bbv-debug.scm 138 */
																			obj_t BgL_arg1986z00_2518;

																			BgL_arg1986z00_2518 =
																				CDR(((obj_t) BgL_l1508z00_2513));
																			{
																				obj_t BgL_tail1511z00_6507;
																				obj_t BgL_l1508z00_6506;

																				BgL_l1508z00_6506 = BgL_arg1986z00_2518;
																				BgL_tail1511z00_6507 =
																					BgL_newtail1512z00_2517;
																				BgL_tail1511z00_2514 =
																					BgL_tail1511z00_6507;
																				BgL_l1508z00_2513 = BgL_l1508z00_6506;
																				goto
																					BgL_zc3z04anonymousza31984ze3z87_2515;
																			}
																		}
																	}
															}
														}
													}
												}
												return
													BGl_errorz00zz__errorz00(BgL_stagez00_106,
													BgL_arg1979z00_2504, BgL_arg1980z00_2505);
											}
										}
								}
							else
								{	/* SawBbv/bbv-debug.scm 110 */
									return BFALSE;
								}
						}
					}
			}
		}

	}



/* &assert-block */
	obj_t BGl_z62assertzd2blockzb0zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5227,
		obj_t BgL_bz00_5228, obj_t BgL_stagez00_5229)
	{
		{	/* SawBbv/bbv-debug.scm 77 */
			return
				BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(
				((BgL_blockz00_bglt) BgL_bz00_5228), BgL_stagez00_5229);
		}

	}



/* dump-cfg */
	BGL_EXPORTED_DEF obj_t
		BGl_dumpzd2cfgzd2zzsaw_bbvzd2debugzd2(BgL_globalz00_bglt BgL_globalz00_107,
		obj_t BgL_paramsz00_108, obj_t BgL_blocksz00_109, obj_t BgL_suffixz00_110)
	{
		{	/* SawBbv/bbv-debug.scm 143 */
			{	/* SawBbv/bbv-debug.scm 146 */
				obj_t BgL_onamez00_2524;

				if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
					{	/* SawBbv/bbv-debug.scm 146 */
						BgL_onamez00_2524 = BGl_za2destza2z00zzengine_paramz00;
					}
				else
					{	/* SawBbv/bbv-debug.scm 148 */
						bool_t BgL_test3313z00_6513;

						if (PAIRP(BGl_za2srczd2filesza2zd2zzengine_paramz00))
							{	/* SawBbv/bbv-debug.scm 148 */
								obj_t BgL_tmpz00_6516;

								BgL_tmpz00_6516 =
									CAR(BGl_za2srczd2filesza2zd2zzengine_paramz00);
								BgL_test3313z00_6513 = STRINGP(BgL_tmpz00_6516);
							}
						else
							{	/* SawBbv/bbv-debug.scm 148 */
								BgL_test3313z00_6513 = ((bool_t) 0);
							}
						if (BgL_test3313z00_6513)
							{	/* SawBbv/bbv-debug.scm 148 */
								BgL_onamez00_2524 =
									BGl_prefixz00zz__osz00(CAR
									(BGl_za2srczd2filesza2zd2zzengine_paramz00));
							}
						else
							{	/* SawBbv/bbv-debug.scm 148 */
								BgL_onamez00_2524 = BGl_string3012z00zzsaw_bbvzd2debugzd2;
							}
					}
				{	/* SawBbv/bbv-debug.scm 153 */
					obj_t BgL_filenamez00_2525;

					{	/* SawBbv/bbv-debug.scm 153 */
						obj_t BgL_arg2008z00_2557;

						{	/* SawBbv/bbv-debug.scm 153 */
							obj_t BgL_arg2009z00_2558;

							BgL_arg2009z00_2558 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_globalz00_107)))->BgL_idz00);
							{	/* SawBbv/bbv-debug.scm 153 */
								obj_t BgL_list2010z00_2559;

								{	/* SawBbv/bbv-debug.scm 153 */
									obj_t BgL_arg2011z00_2560;

									{	/* SawBbv/bbv-debug.scm 153 */
										obj_t BgL_arg2012z00_2561;

										BgL_arg2012z00_2561 =
											MAKE_YOUNG_PAIR(BgL_suffixz00_110, BNIL);
										BgL_arg2011z00_2560 =
											MAKE_YOUNG_PAIR(BgL_arg2009z00_2558, BgL_arg2012z00_2561);
									}
									BgL_list2010z00_2559 =
										MAKE_YOUNG_PAIR(BgL_onamez00_2524, BgL_arg2011z00_2560);
								}
								BgL_arg2008z00_2557 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string3013z00zzsaw_bbvzd2debugzd2, BgL_list2010z00_2559);
							}
						}
						BgL_filenamez00_2525 =
							BGl_stringzd2replacezd2zz__r4_strings_6_7z00(BgL_arg2008z00_2557,
							(char) (((unsigned char) '/')), (char) (((unsigned char) '_')));
					}
					{	/* SawBbv/bbv-debug.scm 156 */
						obj_t BgL_namez00_2526;

						BgL_namez00_2526 = BGl_prefixz00zz__osz00(BgL_filenamez00_2525);
						{	/* SawBbv/bbv-debug.scm 159 */
							obj_t BgL_dumpzd2cfgzd2_5230;

							BgL_dumpzd2cfgzd2_5230 =
								MAKE_FX_PROCEDURE(BGl_z62dumpzd2cfg2998zb0zzsaw_bbvzd2debugzd2,
								(int) (1L), (int) (4L));
							PROCEDURE_SET(BgL_dumpzd2cfgzd2_5230,
								(int) (0L), ((obj_t) BgL_globalz00_107));
							PROCEDURE_SET(BgL_dumpzd2cfgzd2_5230,
								(int) (1L), BgL_namez00_2526);
							PROCEDURE_SET(BgL_dumpzd2cfgzd2_5230,
								(int) (2L), BgL_filenamez00_2525);
							PROCEDURE_SET(BgL_dumpzd2cfgzd2_5230,
								(int) (3L), BgL_blocksz00_109);
							return
								BGl_callzd2withzd2outputzd2filezd2zz__r4_ports_6_10_1z00
								(BgL_filenamez00_2525, BgL_dumpzd2cfgzd2_5230);
						}
					}
				}
			}
		}

	}



/* &dump-cfg */
	obj_t BGl_z62dumpzd2cfgzb0zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5231,
		obj_t BgL_globalz00_5232, obj_t BgL_paramsz00_5233,
		obj_t BgL_blocksz00_5234, obj_t BgL_suffixz00_5235)
	{
		{	/* SawBbv/bbv-debug.scm 143 */
			return
				BGl_dumpzd2cfgzd2zzsaw_bbvzd2debugzd2(
				((BgL_globalz00_bglt) BgL_globalz00_5232), BgL_paramsz00_5233,
				BgL_blocksz00_5234, BgL_suffixz00_5235);
		}

	}



/* &dump-cfg2998 */
	obj_t BGl_z62dumpzd2cfg2998zb0zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5236,
		obj_t BgL_portz00_5241)
	{
		{	/* SawBbv/bbv-debug.scm 173 */
			{	/* SawBbv/bbv-debug.scm 159 */
				BgL_globalz00_bglt BgL_globalz00_5237;
				obj_t BgL_namez00_5238;
				obj_t BgL_filenamez00_5239;
				obj_t BgL_blocksz00_5240;

				BgL_globalz00_5237 =
					((BgL_globalz00_bglt) PROCEDURE_REF(BgL_envz00_5236, (int) (0L)));
				BgL_namez00_5238 = ((obj_t) PROCEDURE_REF(BgL_envz00_5236, (int) (1L)));
				BgL_filenamez00_5239 = PROCEDURE_REF(BgL_envz00_5236, (int) (2L));
				BgL_blocksz00_5240 =
					((obj_t) PROCEDURE_REF(BgL_envz00_5236, (int) (3L)));
				{	/* SawBbv/bbv-debug.scm 159 */
					obj_t BgL_idz00_5382;

					BgL_idz00_5382 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_globalz00_5237)))->BgL_idz00);
					{	/* SawBbv/bbv-debug.scm 159 */

						{	/* SawBbv/bbv-debug.scm 160 */
							obj_t BgL_tmpz00_6559;

							BgL_tmpz00_6559 = ((obj_t) BgL_portz00_5241);
							bgl_display_string(BGl_string3014z00zzsaw_bbvzd2debugzd2,
								BgL_tmpz00_6559);
						}
						{	/* SawBbv/bbv-debug.scm 160 */
							obj_t BgL_tmpz00_6562;

							BgL_tmpz00_6562 = ((obj_t) BgL_portz00_5241);
							bgl_display_char(((unsigned char) 10), BgL_tmpz00_6562);
						}
						{	/* SawBbv/bbv-debug.scm 161 */
							obj_t BgL_tmpz00_6565;

							BgL_tmpz00_6565 = ((obj_t) BgL_portz00_5241);
							bgl_display_string(BGl_string3015z00zzsaw_bbvzd2debugzd2,
								BgL_tmpz00_6565);
						}
						bgl_display_obj(BgL_idz00_5382, BgL_portz00_5241);
						{	/* SawBbv/bbv-debug.scm 161 */
							obj_t BgL_tmpz00_6569;

							BgL_tmpz00_6569 = ((obj_t) BgL_portz00_5241);
							bgl_display_string(BGl_string3001z00zzsaw_bbvzd2debugzd2,
								BgL_tmpz00_6569);
						}
						{	/* SawBbv/bbv-debug.scm 161 */
							obj_t BgL_tmpz00_6572;

							BgL_tmpz00_6572 = ((obj_t) BgL_portz00_5241);
							bgl_display_char(((unsigned char) 10), BgL_tmpz00_6572);
						}
						{	/* SawBbv/bbv-debug.scm 163 */
							obj_t BgL_arg1993z00_5383;
							obj_t BgL_arg1994z00_5384;
							obj_t BgL_arg1995z00_5385;
							obj_t BgL_arg1996z00_5386;

							{	/* SawBbv/bbv-debug.scm 163 */
								obj_t BgL__ortest_1183z00_5387;

								BgL__ortest_1183z00_5387 =
									BGl_getenvz00zz__osz00(BGl_string3016z00zzsaw_bbvzd2debugzd2);
								if (CBOOL(BgL__ortest_1183z00_5387))
									{	/* SawBbv/bbv-debug.scm 163 */
										BgL_arg1993z00_5383 = BgL__ortest_1183z00_5387;
									}
								else
									{	/* SawBbv/bbv-debug.scm 163 */
										BgL_arg1993z00_5383 = BGl_string3017z00zzsaw_bbvzd2debugzd2;
									}
							}
							{	/* SawBbv/bbv-debug.scm 164 */
								obj_t BgL__ortest_1184z00_5388;

								BgL__ortest_1184z00_5388 =
									BGl_getenvz00zz__osz00(BGl_string3018z00zzsaw_bbvzd2debugzd2);
								if (CBOOL(BgL__ortest_1184z00_5388))
									{	/* SawBbv/bbv-debug.scm 164 */
										BgL_arg1994z00_5384 = BgL__ortest_1184z00_5388;
									}
								else
									{	/* SawBbv/bbv-debug.scm 164 */
										BgL_arg1994z00_5384 = BGl_string3019z00zzsaw_bbvzd2debugzd2;
									}
							}
							{	/* SawBbv/bbv-debug.scm 165 */
								obj_t BgL__ortest_1185z00_5389;

								BgL__ortest_1185z00_5389 =
									BGl_getenvz00zz__osz00(BGl_string3020z00zzsaw_bbvzd2debugzd2);
								if (CBOOL(BgL__ortest_1185z00_5389))
									{	/* SawBbv/bbv-debug.scm 165 */
										BgL_arg1995z00_5385 = BgL__ortest_1185z00_5389;
									}
								else
									{	/* SawBbv/bbv-debug.scm 165 */
										BgL_arg1995z00_5385 = BGl_string3021z00zzsaw_bbvzd2debugzd2;
									}
							}
							BgL_arg1996z00_5386 = BGl_commandzd2linezd2zz__osz00();
							{	/* SawBbv/bbv-debug.scm 162 */
								obj_t BgL_list1997z00_5390;

								{	/* SawBbv/bbv-debug.scm 162 */
									obj_t BgL_arg1998z00_5391;

									{	/* SawBbv/bbv-debug.scm 162 */
										obj_t BgL_arg1999z00_5392;

										{	/* SawBbv/bbv-debug.scm 162 */
											obj_t BgL_arg2000z00_5393;

											BgL_arg2000z00_5393 =
												MAKE_YOUNG_PAIR(BgL_arg1996z00_5386, BNIL);
											BgL_arg1999z00_5392 =
												MAKE_YOUNG_PAIR(BgL_arg1995z00_5385,
												BgL_arg2000z00_5393);
										}
										BgL_arg1998z00_5391 =
											MAKE_YOUNG_PAIR(BgL_arg1994z00_5384, BgL_arg1999z00_5392);
									}
									BgL_list1997z00_5390 =
										MAKE_YOUNG_PAIR(BgL_arg1993z00_5383, BgL_arg1998z00_5391);
								}
								BGl_fprintfz00zz__r4_output_6_10_3z00(BgL_portz00_5241,
									BGl_string3022z00zzsaw_bbvzd2debugzd2, BgL_list1997z00_5390);
							}
						}
						{	/* SawBbv/bbv-debug.scm 167 */
							obj_t BgL_list2001z00_5394;

							{	/* SawBbv/bbv-debug.scm 167 */
								obj_t BgL_arg2002z00_5395;

								{	/* SawBbv/bbv-debug.scm 167 */
									obj_t BgL_arg2003z00_5396;

									{	/* SawBbv/bbv-debug.scm 167 */
										obj_t BgL_arg2004z00_5397;

										BgL_arg2004z00_5397 =
											MAKE_YOUNG_PAIR(BgL_namez00_5238, BNIL);
										BgL_arg2003z00_5396 =
											MAKE_YOUNG_PAIR(BgL_namez00_5238, BgL_arg2004z00_5397);
									}
									BgL_arg2002z00_5395 =
										MAKE_YOUNG_PAIR(BgL_namez00_5238, BgL_arg2003z00_5396);
								}
								BgL_list2001z00_5394 =
									MAKE_YOUNG_PAIR(BgL_filenamez00_5239, BgL_arg2002z00_5395);
							}
							BGl_fprintfz00zz__r4_output_6_10_3z00(BgL_portz00_5241,
								BGl_string3023z00zzsaw_bbvzd2debugzd2, BgL_list2001z00_5394);
						}
						{
							obj_t BgL_l1516z00_5399;

							BgL_l1516z00_5399 = BgL_blocksz00_5240;
						BgL_zc3z04anonymousza32005ze3z87_5398:
							if (PAIRP(BgL_l1516z00_5399))
								{	/* SawBbv/bbv-debug.scm 169 */
									{	/* SawBbv/bbv-debug.scm 170 */
										obj_t BgL_bz00_5400;

										BgL_bz00_5400 = CAR(BgL_l1516z00_5399);
										BGl_dumpz00zzsaw_defsz00(BgL_bz00_5400, BgL_portz00_5241,
											(int) (0L));
										{	/* SawBbv/bbv-debug.scm 171 */
											obj_t BgL_tmpz00_6600;

											BgL_tmpz00_6600 = ((obj_t) BgL_portz00_5241);
											bgl_display_char(((unsigned char) 10), BgL_tmpz00_6600);
									}}
									{
										obj_t BgL_l1516z00_6603;

										BgL_l1516z00_6603 = CDR(BgL_l1516z00_5399);
										BgL_l1516z00_5399 = BgL_l1516z00_6603;
										goto BgL_zc3z04anonymousza32005ze3z87_5398;
									}
								}
							else
								{	/* SawBbv/bbv-debug.scm 169 */
									((bool_t) 1);
								}
						}
						return BgL_idz00_5382;
					}
				}
			}
		}

	}



/* shape-ctx-entry */
	obj_t BGl_shapezd2ctxzd2entryz00zzsaw_bbvzd2debugzd2(obj_t BgL_ez00_111)
	{
		{	/* SawBbv/bbv-debug.scm 182 */
			{	/* SawBbv/bbv-debug.scm 184 */
				bool_t BgL_test3319z00_6605;

				if (
					(bgl_list_length(
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_111)))->
								BgL_typesz00)) == 1L))
					{	/* SawBbv/bbv-debug.scm 185 */
						bool_t BgL_test3321z00_6611;

						{	/* SawBbv/bbv-debug.scm 185 */
							obj_t BgL_arg2076z00_2637;

							BgL_arg2076z00_2637 =
								CAR(
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
											((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_111)))->
									BgL_typesz00));
							BgL_test3321z00_6611 =
								(BgL_arg2076z00_2637 == BGl_za2objza2z00zztype_cachez00);
						}
						if (BgL_test3321z00_6611)
							{	/* SawBbv/bbv-debug.scm 185 */
								if (
									((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
													((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_111)))->
											BgL_valuez00) == CNST_TABLE_REF(2)))
									{	/* SawBbv/bbv-debug.scm 186 */
										BgL_test3319z00_6605 =
											NULLP(
											(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
														((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_111)))->
												BgL_aliasesz00));
									}
								else
									{	/* SawBbv/bbv-debug.scm 186 */
										BgL_test3319z00_6605 = ((bool_t) 0);
									}
							}
						else
							{	/* SawBbv/bbv-debug.scm 185 */
								BgL_test3319z00_6605 = ((bool_t) 0);
							}
					}
				else
					{	/* SawBbv/bbv-debug.scm 184 */
						BgL_test3319z00_6605 = ((bool_t) 0);
					}
				if (BgL_test3319z00_6605)
					{	/* SawBbv/bbv-debug.scm 188 */
						BgL_rtl_regz00_bglt BgL_arg2038z00_2585;

						BgL_arg2038z00_2585 =
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_111)))->BgL_regz00);
						BGL_TAIL return
							BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2038z00_2585));
					}
				else
					{	/* SawBbv/bbv-debug.scm 189 */
						obj_t BgL_arg2039z00_2586;
						obj_t BgL_arg2040z00_2587;

						{	/* SawBbv/bbv-debug.scm 189 */
							BgL_rtl_regz00_bglt BgL_arg2041z00_2588;

							BgL_arg2041z00_2588 =
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_111)))->BgL_regz00);
							BgL_arg2039z00_2586 =
								BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2041z00_2588));
						}
						if (
							((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
											((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_111)))->
									BgL_valuez00) == CNST_TABLE_REF(2)))
							{	/* SawBbv/bbv-debug.scm 191 */
								if (
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
												((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_111)))->
										BgL_polarityz00))
									{	/* SawBbv/bbv-debug.scm 195 */
										obj_t BgL_l1518z00_2592;

										BgL_l1518z00_2592 =
											(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
													((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_111)))->
											BgL_typesz00);
										{	/* SawBbv/bbv-debug.scm 195 */
											obj_t BgL_head1520z00_2594;

											BgL_head1520z00_2594 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											{
												obj_t BgL_l1518z00_2596;
												obj_t BgL_tail1521z00_2597;

												BgL_l1518z00_2596 = BgL_l1518z00_2592;
												BgL_tail1521z00_2597 = BgL_head1520z00_2594;
											BgL_zc3z04anonymousza32046ze3z87_2598:
												if (NULLP(BgL_l1518z00_2596))
													{	/* SawBbv/bbv-debug.scm 195 */
														BgL_arg2040z00_2587 = CDR(BgL_head1520z00_2594);
													}
												else
													{	/* SawBbv/bbv-debug.scm 195 */
														obj_t BgL_newtail1522z00_2600;

														{	/* SawBbv/bbv-debug.scm 195 */
															obj_t BgL_arg2049z00_2602;

															{	/* SawBbv/bbv-debug.scm 195 */
																obj_t BgL_tz00_2603;

																BgL_tz00_2603 =
																	CAR(((obj_t) BgL_l1518z00_2596));
																{	/* SawBbv/bbv-debug.scm 195 */
																	obj_t BgL_arg2050z00_2604;
																	long BgL_arg2051z00_2605;

																	BgL_arg2050z00_2604 =
																		BGl_shapez00zztools_shapez00(BgL_tz00_2603);
																	BgL_arg2051z00_2605 =
																		(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
																				((BgL_bbvzd2ctxentryzd2_bglt)
																					BgL_ez00_111)))->BgL_countz00);
																	{	/* SawBbv/bbv-debug.scm 195 */
																		obj_t BgL_list2052z00_2606;

																		{	/* SawBbv/bbv-debug.scm 195 */
																			obj_t BgL_arg2055z00_2607;

																			BgL_arg2055z00_2607 =
																				MAKE_YOUNG_PAIR(BINT
																				(BgL_arg2051z00_2605), BNIL);
																			BgL_list2052z00_2606 =
																				MAKE_YOUNG_PAIR(BgL_arg2050z00_2604,
																				BgL_arg2055z00_2607);
																		}
																		BgL_arg2049z00_2602 =
																			BGl_formatz00zz__r4_output_6_10_3z00
																			(BGl_string3024z00zzsaw_bbvzd2debugzd2,
																			BgL_list2052z00_2606);
															}}}
															BgL_newtail1522z00_2600 =
																MAKE_YOUNG_PAIR(BgL_arg2049z00_2602, BNIL);
														}
														SET_CDR(BgL_tail1521z00_2597,
															BgL_newtail1522z00_2600);
														{	/* SawBbv/bbv-debug.scm 195 */
															obj_t BgL_arg2048z00_2601;

															BgL_arg2048z00_2601 =
																CDR(((obj_t) BgL_l1518z00_2596));
															{
																obj_t BgL_tail1521z00_6660;
																obj_t BgL_l1518z00_6659;

																BgL_l1518z00_6659 = BgL_arg2048z00_2601;
																BgL_tail1521z00_6660 = BgL_newtail1522z00_2600;
																BgL_tail1521z00_2597 = BgL_tail1521z00_6660;
																BgL_l1518z00_2596 = BgL_l1518z00_6659;
																goto BgL_zc3z04anonymousza32046ze3z87_2598;
															}
														}
													}
											}
										}
									}
								else
									{	/* SawBbv/bbv-debug.scm 196 */
										obj_t BgL_l1523z00_2609;

										BgL_l1523z00_2609 =
											(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
													((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_111)))->
											BgL_typesz00);
										{	/* SawBbv/bbv-debug.scm 196 */
											obj_t BgL_head1525z00_2611;

											BgL_head1525z00_2611 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											{
												obj_t BgL_l1523z00_2613;
												obj_t BgL_tail1526z00_2614;

												BgL_l1523z00_2613 = BgL_l1523z00_2609;
												BgL_tail1526z00_2614 = BgL_head1525z00_2611;
											BgL_zc3z04anonymousza32057ze3z87_2615:
												if (NULLP(BgL_l1523z00_2613))
													{	/* SawBbv/bbv-debug.scm 196 */
														BgL_arg2040z00_2587 = CDR(BgL_head1525z00_2611);
													}
												else
													{	/* SawBbv/bbv-debug.scm 196 */
														obj_t BgL_newtail1527z00_2617;

														{	/* SawBbv/bbv-debug.scm 196 */
															obj_t BgL_arg2060z00_2619;

															{	/* SawBbv/bbv-debug.scm 196 */
																obj_t BgL_tz00_2620;

																BgL_tz00_2620 =
																	CAR(((obj_t) BgL_l1523z00_2613));
																{	/* SawBbv/bbv-debug.scm 196 */
																	obj_t BgL_arg2061z00_2621;

																	BgL_arg2061z00_2621 =
																		BGl_shapez00zztools_shapez00(BgL_tz00_2620);
																	{	/* SawBbv/bbv-debug.scm 196 */
																		obj_t BgL_list2062z00_2622;

																		BgL_list2062z00_2622 =
																			MAKE_YOUNG_PAIR(BgL_arg2061z00_2621,
																			BNIL);
																		BgL_arg2060z00_2619 =
																			BGl_formatz00zz__r4_output_6_10_3z00
																			(BGl_string3025z00zzsaw_bbvzd2debugzd2,
																			BgL_list2062z00_2622);
																	}
																}
															}
															BgL_newtail1527z00_2617 =
																MAKE_YOUNG_PAIR(BgL_arg2060z00_2619, BNIL);
														}
														SET_CDR(BgL_tail1526z00_2614,
															BgL_newtail1527z00_2617);
														{	/* SawBbv/bbv-debug.scm 196 */
															obj_t BgL_arg2059z00_2618;

															BgL_arg2059z00_2618 =
																CDR(((obj_t) BgL_l1523z00_2613));
															{
																obj_t BgL_tail1526z00_6677;
																obj_t BgL_l1523z00_6676;

																BgL_l1523z00_6676 = BgL_arg2059z00_2618;
																BgL_tail1526z00_6677 = BgL_newtail1527z00_2617;
																BgL_tail1526z00_2614 = BgL_tail1526z00_6677;
																BgL_l1523z00_2613 = BgL_l1523z00_6676;
																goto BgL_zc3z04anonymousza32057ze3z87_2615;
															}
														}
													}
											}
										}
									}
							}
						else
							{	/* SawBbv/bbv-debug.scm 192 */
								obj_t BgL_arg2063z00_2624;

								{	/* SawBbv/bbv-debug.scm 192 */
									obj_t BgL_arg2065z00_2626;
									long BgL_arg2067z00_2627;

									{	/* SawBbv/bbv-debug.scm 192 */
										obj_t BgL_arg2070z00_2630;

										BgL_arg2070z00_2630 =
											(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
													((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_111)))->
											BgL_valuez00);
										BgL_arg2065z00_2626 =
											BGl_shapez00zztools_shapez00(BgL_arg2070z00_2630);
									}
									BgL_arg2067z00_2627 =
										(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
												((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_111)))->
										BgL_countz00);
									{	/* SawBbv/bbv-debug.scm 192 */
										obj_t BgL_list2068z00_2628;

										{	/* SawBbv/bbv-debug.scm 192 */
											obj_t BgL_arg2069z00_2629;

											BgL_arg2069z00_2629 =
												MAKE_YOUNG_PAIR(BINT(BgL_arg2067z00_2627), BNIL);
											BgL_list2068z00_2628 =
												MAKE_YOUNG_PAIR(BgL_arg2065z00_2626,
												BgL_arg2069z00_2629);
										}
										BgL_arg2063z00_2624 =
											BGl_formatz00zz__r4_output_6_10_3z00
											(BGl_string3024z00zzsaw_bbvzd2debugzd2,
											BgL_list2068z00_2628);
								}}
								{	/* SawBbv/bbv-debug.scm 192 */
									obj_t BgL_list2064z00_2625;

									BgL_list2064z00_2625 =
										MAKE_YOUNG_PAIR(BgL_arg2063z00_2624, BNIL);
									BgL_arg2040z00_2587 = BgL_list2064z00_2625;
							}}
						return MAKE_YOUNG_PAIR(BgL_arg2039z00_2586, BgL_arg2040z00_2587);
					}
			}
		}

	}



/* dump-json-cfg */
	BGL_EXPORTED_DEF obj_t
		BGl_dumpzd2jsonzd2cfgz00zzsaw_bbvzd2debugzd2(BgL_globalz00_bglt
		BgL_globalz00_112, obj_t BgL_paramsz00_113, obj_t BgL_historyz00_114,
		obj_t BgL_blocksz00_115, obj_t BgL_suffixz00_116)
	{
		{	/* SawBbv/bbv-debug.scm 201 */
			{	/* SawBbv/bbv-debug.scm 204 */
				obj_t BgL_onamez00_2641;

				if (STRINGP(BGl_za2destza2z00zzengine_paramz00))
					{	/* SawBbv/bbv-debug.scm 204 */
						BgL_onamez00_2641 = BGl_za2destza2z00zzengine_paramz00;
					}
				else
					{	/* SawBbv/bbv-debug.scm 206 */
						bool_t BgL_test3328z00_6691;

						if (PAIRP(BGl_za2srczd2filesza2zd2zzengine_paramz00))
							{	/* SawBbv/bbv-debug.scm 206 */
								obj_t BgL_tmpz00_6694;

								BgL_tmpz00_6694 =
									CAR(BGl_za2srczd2filesza2zd2zzengine_paramz00);
								BgL_test3328z00_6691 = STRINGP(BgL_tmpz00_6694);
							}
						else
							{	/* SawBbv/bbv-debug.scm 206 */
								BgL_test3328z00_6691 = ((bool_t) 0);
							}
						if (BgL_test3328z00_6691)
							{	/* SawBbv/bbv-debug.scm 206 */
								BgL_onamez00_2641 =
									BGl_prefixz00zz__osz00(CAR
									(BGl_za2srczd2filesza2zd2zzengine_paramz00));
							}
						else
							{	/* SawBbv/bbv-debug.scm 206 */
								BgL_onamez00_2641 = BGl_string3012z00zzsaw_bbvzd2debugzd2;
							}
					}
				{	/* SawBbv/bbv-debug.scm 211 */
					obj_t BgL_filenamez00_2642;

					{	/* SawBbv/bbv-debug.scm 211 */
						obj_t BgL_arg2173z00_2822;

						{	/* SawBbv/bbv-debug.scm 211 */
							obj_t BgL_arg2174z00_2823;

							BgL_arg2174z00_2823 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_globalz00_112)))->BgL_idz00);
							{	/* SawBbv/bbv-debug.scm 211 */
								obj_t BgL_list2175z00_2824;

								{	/* SawBbv/bbv-debug.scm 211 */
									obj_t BgL_arg2176z00_2825;

									{	/* SawBbv/bbv-debug.scm 211 */
										obj_t BgL_arg2177z00_2826;

										BgL_arg2177z00_2826 =
											MAKE_YOUNG_PAIR(BgL_suffixz00_116, BNIL);
										BgL_arg2176z00_2825 =
											MAKE_YOUNG_PAIR(BgL_arg2174z00_2823, BgL_arg2177z00_2826);
									}
									BgL_list2175z00_2824 =
										MAKE_YOUNG_PAIR(BgL_onamez00_2641, BgL_arg2176z00_2825);
								}
								BgL_arg2173z00_2822 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string3013z00zzsaw_bbvzd2debugzd2, BgL_list2175z00_2824);
							}
						}
						BgL_filenamez00_2642 =
							BGl_stringzd2replacezd2zz__r4_strings_6_7z00(BgL_arg2173z00_2822,
							(char) (((unsigned char) '/')), (char) (((unsigned char) '_')));
					}
					{	/* SawBbv/bbv-debug.scm 214 */
						obj_t BgL_namez00_2643;

						BgL_namez00_2643 = BGl_prefixz00zz__osz00(BgL_filenamez00_2642);
						{	/* SawBbv/bbv-debug.scm 276 */
							obj_t BgL_dumpzd2cfgzd2_5244;

							BgL_dumpzd2cfgzd2_5244 =
								MAKE_FX_PROCEDURE(BGl_z62dumpzd2cfg2999zb0zzsaw_bbvzd2debugzd2,
								(int) (1L), (int) (3L));
							PROCEDURE_SET(BgL_dumpzd2cfgzd2_5244,
								(int) (0L), ((obj_t) BgL_globalz00_112));
							PROCEDURE_SET(BgL_dumpzd2cfgzd2_5244,
								(int) (1L), BgL_blocksz00_115);
							PROCEDURE_SET(BgL_dumpzd2cfgzd2_5244,
								(int) (2L), BgL_historyz00_114);
							return
								BGl_callzd2withzd2outputzd2filezd2zz__r4_ports_6_10_1z00
								(BgL_filenamez00_2642, BgL_dumpzd2cfgzd2_5244);
						}
					}
				}
			}
		}

	}



/* &dump-json-cfg */
	obj_t BGl_z62dumpzd2jsonzd2cfgz62zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5245,
		obj_t BgL_globalz00_5246, obj_t BgL_paramsz00_5247,
		obj_t BgL_historyz00_5248, obj_t BgL_blocksz00_5249,
		obj_t BgL_suffixz00_5250)
	{
		{	/* SawBbv/bbv-debug.scm 201 */
			return
				BGl_dumpzd2jsonzd2cfgz00zzsaw_bbvzd2debugzd2(
				((BgL_globalz00_bglt) BgL_globalz00_5246), BgL_paramsz00_5247,
				BgL_historyz00_5248, BgL_blocksz00_5249, BgL_suffixz00_5250);
		}

	}



/* &dump-cfg2999 */
	obj_t BGl_z62dumpzd2cfg2999zb0zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5251,
		obj_t BgL_portz00_5255)
	{
		{	/* SawBbv/bbv-debug.scm 299 */
			{	/* SawBbv/bbv-debug.scm 276 */
				BgL_globalz00_bglt BgL_globalz00_5252;
				obj_t BgL_blocksz00_5253;
				obj_t BgL_historyz00_5254;

				BgL_globalz00_5252 =
					((BgL_globalz00_bglt) PROCEDURE_REF(BgL_envz00_5251, (int) (0L)));
				BgL_blocksz00_5253 =
					((obj_t) PROCEDURE_REF(BgL_envz00_5251, (int) (1L)));
				BgL_historyz00_5254 = PROCEDURE_REF(BgL_envz00_5251, (int) (2L));
				{
					obj_t BgL_firstz00_5456;
					BgL_blockz00_bglt BgL_oz00_5403;
					obj_t BgL_pz00_5404;
					long BgL_mz00_5405;
					obj_t BgL_sepz00_5406;

					{	/* SawBbv/bbv-debug.scm 276 */
						obj_t BgL_idz00_5468;

						BgL_idz00_5468 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_globalz00_5252)))->BgL_idz00);
						{	/* SawBbv/bbv-debug.scm 276 */

							{	/* SawBbv/bbv-debug.scm 277 */
								obj_t BgL_tmpz00_6732;

								BgL_tmpz00_6732 = ((obj_t) BgL_portz00_5255);
								bgl_display_string(BGl_string3026z00zzsaw_bbvzd2debugzd2,
									BgL_tmpz00_6732);
							}
							{	/* SawBbv/bbv-debug.scm 277 */
								obj_t BgL_tmpz00_6735;

								BgL_tmpz00_6735 = ((obj_t) BgL_portz00_5255);
								bgl_display_char(((unsigned char) 10), BgL_tmpz00_6735);
							}
							{	/* SawBbv/bbv-debug.scm 278 */
								obj_t BgL_tmpz00_6738;

								BgL_tmpz00_6738 = ((obj_t) BgL_portz00_5255);
								bgl_display_string(BGl_string3039z00zzsaw_bbvzd2debugzd2,
									BgL_tmpz00_6738);
							}
							{	/* SawBbv/bbv-debug.scm 278 */
								obj_t BgL_tmpz00_6741;

								BgL_tmpz00_6741 = ((obj_t) BgL_portz00_5255);
								bgl_display_char(((unsigned char) 10), BgL_tmpz00_6741);
							}
							{	/* SawBbv/bbv-debug.scm 280 */
								obj_t BgL_arg2082z00_5469;

								{	/* SawBbv/bbv-debug.scm 280 */
									obj_t BgL__ortest_1192z00_5470;

									BgL__ortest_1192z00_5470 =
										BGl_getenvz00zz__osz00
										(BGl_string3018z00zzsaw_bbvzd2debugzd2);
									if (CBOOL(BgL__ortest_1192z00_5470))
										{	/* SawBbv/bbv-debug.scm 280 */
											BgL_arg2082z00_5469 = BgL__ortest_1192z00_5470;
										}
									else
										{	/* SawBbv/bbv-debug.scm 280 */
											BgL_arg2082z00_5469 =
												BGl_string3019z00zzsaw_bbvzd2debugzd2;
										}
								}
								{	/* SawBbv/bbv-debug.scm 279 */
									obj_t BgL_list2083z00_5471;

									BgL_list2083z00_5471 =
										MAKE_YOUNG_PAIR(BgL_arg2082z00_5469, BNIL);
									BGl_fprintfz00zz__r4_output_6_10_3z00(BgL_portz00_5255,
										BGl_string3040z00zzsaw_bbvzd2debugzd2,
										BgL_list2083z00_5471);
								}
							}
							{	/* SawBbv/bbv-debug.scm 282 */
								obj_t BgL_arg2084z00_5472;

								{	/* SawBbv/bbv-debug.scm 282 */
									obj_t BgL__ortest_1193z00_5473;

									BgL__ortest_1193z00_5473 =
										BGl_getenvz00zz__osz00
										(BGl_string3016z00zzsaw_bbvzd2debugzd2);
									if (CBOOL(BgL__ortest_1193z00_5473))
										{	/* SawBbv/bbv-debug.scm 282 */
											BgL_arg2084z00_5472 = BgL__ortest_1193z00_5473;
										}
									else
										{	/* SawBbv/bbv-debug.scm 282 */
											BgL_arg2084z00_5472 =
												BGl_string3017z00zzsaw_bbvzd2debugzd2;
										}
								}
								{	/* SawBbv/bbv-debug.scm 281 */
									obj_t BgL_list2085z00_5474;

									BgL_list2085z00_5474 =
										MAKE_YOUNG_PAIR(BgL_arg2084z00_5472, BNIL);
									BGl_fprintfz00zz__r4_output_6_10_3z00(BgL_portz00_5255,
										BGl_string3041z00zzsaw_bbvzd2debugzd2,
										BgL_list2085z00_5474);
								}
							}
							{	/* SawBbv/bbv-debug.scm 284 */
								obj_t BgL_arg2086z00_5475;

								{	/* SawBbv/bbv-debug.scm 284 */
									obj_t BgL__ortest_1194z00_5476;

									BgL__ortest_1194z00_5476 =
										BGl_getenvz00zz__osz00
										(BGl_string3020z00zzsaw_bbvzd2debugzd2);
									if (CBOOL(BgL__ortest_1194z00_5476))
										{	/* SawBbv/bbv-debug.scm 284 */
											BgL_arg2086z00_5475 = BgL__ortest_1194z00_5476;
										}
									else
										{	/* SawBbv/bbv-debug.scm 284 */
											BgL_arg2086z00_5475 =
												BGl_string3021z00zzsaw_bbvzd2debugzd2;
										}
								}
								{	/* SawBbv/bbv-debug.scm 283 */
									obj_t BgL_list2087z00_5477;

									BgL_list2087z00_5477 =
										MAKE_YOUNG_PAIR(BgL_arg2086z00_5475, BNIL);
									BGl_fprintfz00zz__r4_output_6_10_3z00(BgL_portz00_5255,
										BGl_string3042z00zzsaw_bbvzd2debugzd2,
										BgL_list2087z00_5477);
								}
							}
							{	/* SawBbv/bbv-debug.scm 285 */
								obj_t BgL_tmpz00_6759;

								BgL_tmpz00_6759 = ((obj_t) BgL_portz00_5255);
								bgl_display_string(BGl_string3043z00zzsaw_bbvzd2debugzd2,
									BgL_tmpz00_6759);
							}
							{	/* SawBbv/bbv-debug.scm 285 */
								obj_t BgL_tmpz00_6762;

								BgL_tmpz00_6762 = ((obj_t) BgL_portz00_5255);
								bgl_display_char(((unsigned char) 10), BgL_tmpz00_6762);
							}
							{	/* SawBbv/bbv-debug.scm 288 */
								obj_t BgL_arg2088z00_5478;

								{	/* SawBbv/bbv-debug.scm 288 */
									obj_t BgL_l1558z00_5479;

									BgL_l1558z00_5479 =
										BGl_sortzd2blockszd2zzsaw_bbvzd2debugzd2
										(BgL_blocksz00_5253);
									if (NULLP(BgL_l1558z00_5479))
										{	/* SawBbv/bbv-debug.scm 288 */
											BgL_arg2088z00_5478 = BNIL;
										}
									else
										{	/* SawBbv/bbv-debug.scm 288 */
											obj_t BgL_head1560z00_5480;

											BgL_head1560z00_5480 = MAKE_YOUNG_PAIR(BNIL, BNIL);
											{
												obj_t BgL_l1558z00_5482;
												obj_t BgL_tail1561z00_5483;

												BgL_l1558z00_5482 = BgL_l1558z00_5479;
												BgL_tail1561z00_5483 = BgL_head1560z00_5480;
											BgL_zc3z04anonymousza32091ze3z87_5481:
												if (NULLP(BgL_l1558z00_5482))
													{	/* SawBbv/bbv-debug.scm 288 */
														BgL_arg2088z00_5478 = CDR(BgL_head1560z00_5480);
													}
												else
													{	/* SawBbv/bbv-debug.scm 288 */
														obj_t BgL_newtail1562z00_5484;

														{	/* SawBbv/bbv-debug.scm 288 */
															obj_t BgL_arg2094z00_5485;

															{	/* SawBbv/bbv-debug.scm 288 */
																obj_t BgL_bz00_5486;

																BgL_bz00_5486 =
																	CAR(((obj_t) BgL_l1558z00_5482));
																BgL_oz00_5403 =
																	((BgL_blockz00_bglt) BgL_bz00_5486);
																BgL_pz00_5404 = BgL_portz00_5255;
																BgL_mz00_5405 = 4L;
																BgL_sepz00_5406 =
																	BGl_string3002z00zzsaw_bbvzd2debugzd2;
																BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_5404,
																	(int) (BgL_mz00_5405));
																{	/* SawBbv/bbv-debug.scm 243 */
																	obj_t BgL_tmpz00_6776;

																	BgL_tmpz00_6776 = ((obj_t) BgL_pz00_5404);
																	bgl_display_string
																		(BGl_string3026z00zzsaw_bbvzd2debugzd2,
																		BgL_tmpz00_6776);
																}
																{	/* SawBbv/bbv-debug.scm 243 */
																	obj_t BgL_tmpz00_6779;

																	BgL_tmpz00_6779 = ((obj_t) BgL_pz00_5404);
																	bgl_display_char(((unsigned char) 10),
																		BgL_tmpz00_6779);
																}
																{	/* SawBbv/bbv-debug.scm 244 */
																	long BgL_arg2096z00_5407;

																	BgL_arg2096z00_5407 = (BgL_mz00_5405 + 2L);
																	BGl_dumpzd2marginzd2zzsaw_defsz00
																		(BgL_pz00_5404,
																		(int) (BgL_arg2096z00_5407));
																}
																{	/* SawBbv/bbv-debug.scm 245 */
																	obj_t BgL_tmpz00_6785;

																	BgL_tmpz00_6785 = ((obj_t) BgL_pz00_5404);
																	bgl_display_string
																		(BGl_string3027z00zzsaw_bbvzd2debugzd2,
																		BgL_tmpz00_6785);
																}
																{	/* SawBbv/bbv-debug.scm 245 */
																	int BgL_arg2097z00_5408;

																	BgL_arg2097z00_5408 =
																		(((BgL_blockz00_bglt) COBJECT(
																				((BgL_blockz00_bglt) BgL_oz00_5403)))->
																		BgL_labelz00);
																	bgl_display_obj(BINT(BgL_arg2097z00_5408),
																		BgL_pz00_5404);
																}
																{	/* SawBbv/bbv-debug.scm 245 */
																	obj_t BgL_tmpz00_6792;

																	BgL_tmpz00_6792 = ((obj_t) BgL_pz00_5404);
																	bgl_display_string
																		(BGl_string3002z00zzsaw_bbvzd2debugzd2,
																		BgL_tmpz00_6792);
																}
																{	/* SawBbv/bbv-debug.scm 245 */
																	obj_t BgL_tmpz00_6795;

																	BgL_tmpz00_6795 = ((obj_t) BgL_pz00_5404);
																	bgl_display_char(((unsigned char) 10),
																		BgL_tmpz00_6795);
																}
																{	/* SawBbv/bbv-debug.scm 246 */
																	long BgL_arg2098z00_5409;

																	BgL_arg2098z00_5409 = (BgL_mz00_5405 + 2L);
																	BGl_dumpzd2marginzd2zzsaw_defsz00
																		(BgL_pz00_5404,
																		(int) (BgL_arg2098z00_5409));
																}
																{	/* SawBbv/bbv-debug.scm 247 */
																	obj_t BgL_tmpz00_6801;

																	BgL_tmpz00_6801 = ((obj_t) BgL_pz00_5404);
																	bgl_display_string
																		(BGl_string3028z00zzsaw_bbvzd2debugzd2,
																		BgL_tmpz00_6801);
																}
																{	/* SawBbv/bbv-debug.scm 247 */
																	int BgL_arg2099z00_5410;

																	{	/* SawBbv/bbv-debug.scm 247 */
																		BgL_blockz00_bglt BgL_arg2100z00_5411;

																		{
																			BgL_blocksz00_bglt BgL_auxz00_6804;

																			{
																				obj_t BgL_auxz00_6805;

																				{	/* SawBbv/bbv-debug.scm 247 */
																					BgL_objectz00_bglt BgL_tmpz00_6806;

																					BgL_tmpz00_6806 =
																						((BgL_objectz00_bglt)
																						BgL_oz00_5403);
																					BgL_auxz00_6805 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_6806);
																				}
																				BgL_auxz00_6804 =
																					((BgL_blocksz00_bglt)
																					BgL_auxz00_6805);
																			}
																			BgL_arg2100z00_5411 =
																				(((BgL_blocksz00_bglt)
																					COBJECT(BgL_auxz00_6804))->
																				BgL_parentz00);
																		}
																		BgL_arg2099z00_5410 =
																			(((BgL_blockz00_bglt) COBJECT(
																					((BgL_blockz00_bglt)
																						BgL_arg2100z00_5411)))->
																			BgL_labelz00);
																	}
																	bgl_display_obj(BINT(BgL_arg2099z00_5410),
																		BgL_pz00_5404);
																}
																{	/* SawBbv/bbv-debug.scm 247 */
																	obj_t BgL_tmpz00_6815;

																	BgL_tmpz00_6815 = ((obj_t) BgL_pz00_5404);
																	bgl_display_string
																		(BGl_string3002z00zzsaw_bbvzd2debugzd2,
																		BgL_tmpz00_6815);
																}
																{	/* SawBbv/bbv-debug.scm 247 */
																	obj_t BgL_tmpz00_6818;

																	BgL_tmpz00_6818 = ((obj_t) BgL_pz00_5404);
																	bgl_display_char(((unsigned char) 10),
																		BgL_tmpz00_6818);
																}
																{	/* SawBbv/bbv-debug.scm 248 */
																	long BgL_arg2101z00_5412;

																	BgL_arg2101z00_5412 = (BgL_mz00_5405 + 2L);
																	BGl_dumpzd2marginzd2zzsaw_defsz00
																		(BgL_pz00_5404,
																		(int) (BgL_arg2101z00_5412));
																}
																{	/* SawBbv/bbv-debug.scm 249 */
																	obj_t BgL_arg2102z00_5413;

																	BgL_arg2102z00_5413 =
																		(((BgL_variablez00_bglt) COBJECT(
																				((BgL_variablez00_bglt)
																					BgL_globalz00_5252)))->BgL_idz00);
																	{	/* SawBbv/bbv-debug.scm 249 */
																		obj_t BgL_list2103z00_5414;

																		BgL_list2103z00_5414 =
																			MAKE_YOUNG_PAIR(BgL_arg2102z00_5413,
																			BNIL);
																		BGl_fprintfz00zz__r4_output_6_10_3z00
																			(BgL_pz00_5404,
																			BGl_string3029z00zzsaw_bbvzd2debugzd2,
																			BgL_list2103z00_5414);
																}}
																{	/* SawBbv/bbv-debug.scm 250 */
																	long BgL_arg2104z00_5415;

																	BgL_arg2104z00_5415 = (BgL_mz00_5405 + 2L);
																	BGl_dumpzd2marginzd2zzsaw_defsz00
																		(BgL_pz00_5404,
																		(int) (BgL_arg2104z00_5415));
																}
																{	/* SawBbv/bbv-debug.scm 251 */
																	obj_t BgL_arg2105z00_5416;

																	{	/* SawBbv/bbv-debug.scm 251 */
																		obj_t BgL_arg2107z00_5417;

																		BgL_arg2107z00_5417 =
																			(((BgL_blockz00_bglt) COBJECT(
																					((BgL_blockz00_bglt)
																						BgL_oz00_5403)))->BgL_firstz00);
																		BgL_firstz00_5456 = BgL_arg2107z00_5417;
																		{	/* SawBbv/bbv-debug.scm 217 */
																			obj_t BgL_iz00_5457;

																			{
																				obj_t BgL_list1529z00_5459;

																				BgL_list1529z00_5459 =
																					BgL_firstz00_5456;
																			BgL_zc3z04anonymousza32169ze3z87_5458:
																				if (PAIRP(BgL_list1529z00_5459))
																					{	/* SawBbv/bbv-debug.scm 217 */
																						if (CBOOL(
																								(((BgL_rtl_insz00_bglt) COBJECT(
																											((BgL_rtl_insz00_bglt)
																												CAR
																												(BgL_list1529z00_5459))))->
																									BgL_locz00)))
																							{	/* SawBbv/bbv-debug.scm 217 */
																								BgL_iz00_5457 =
																									CAR(BgL_list1529z00_5459);
																							}
																						else
																							{
																								obj_t BgL_list1529z00_6841;

																								BgL_list1529z00_6841 =
																									CDR(BgL_list1529z00_5459);
																								BgL_list1529z00_5459 =
																									BgL_list1529z00_6841;
																								goto
																									BgL_zc3z04anonymousza32169ze3z87_5458;
																							}
																					}
																				else
																					{	/* SawBbv/bbv-debug.scm 217 */
																						BgL_iz00_5457 = BFALSE;
																					}
																			}
																			if (CBOOL(BgL_iz00_5457))
																				{	/* SawBbv/bbv-debug.scm 222 */
																					bool_t BgL_test3338z00_6845;

																					{	/* SawBbv/bbv-debug.scm 222 */
																						obj_t BgL_arg2168z00_5460;

																						BgL_arg2168z00_5460 =
																							(((BgL_rtl_insz00_bglt) COBJECT(
																									((BgL_rtl_insz00_bglt)
																										BgL_iz00_5457)))->
																							BgL_locz00);
																						if (STRUCTP(BgL_arg2168z00_5460))
																							{	/* SawBbv/bbv-debug.scm 222 */
																								BgL_test3338z00_6845 =
																									(STRUCT_KEY
																									(BgL_arg2168z00_5460) ==
																									CNST_TABLE_REF(3));
																							}
																						else
																							{	/* SawBbv/bbv-debug.scm 222 */
																								BgL_test3338z00_6845 =
																									((bool_t) 0);
																							}
																					}
																					if (BgL_test3338z00_6845)
																						{	/* SawBbv/bbv-debug.scm 223 */
																							obj_t BgL_arg2161z00_5461;
																							obj_t BgL_arg2162z00_5462;

																							{	/* SawBbv/bbv-debug.scm 223 */
																								obj_t BgL_arg2165z00_5463;

																								{	/* SawBbv/bbv-debug.scm 223 */
																									obj_t BgL_sz00_5464;

																									BgL_sz00_5464 =
																										(((BgL_rtl_insz00_bglt)
																											COBJECT((
																													(BgL_rtl_insz00_bglt)
																													BgL_iz00_5457)))->
																										BgL_locz00);
																									BgL_arg2165z00_5463 =
																										STRUCT_REF(BgL_sz00_5464,
																										(int) (0L));
																								}
																								BgL_arg2161z00_5461 =
																									BGl_basenamez00zz__osz00
																									(BgL_arg2165z00_5463);
																							}
																							{	/* SawBbv/bbv-debug.scm 223 */
																								obj_t BgL_sz00_5465;

																								BgL_sz00_5465 =
																									(((BgL_rtl_insz00_bglt)
																										COBJECT((
																												(BgL_rtl_insz00_bglt)
																												BgL_iz00_5457)))->
																									BgL_locz00);
																								BgL_arg2162z00_5462 =
																									STRUCT_REF(BgL_sz00_5465,
																									(int) (2L));
																							}
																							{	/* SawBbv/bbv-debug.scm 223 */
																								obj_t BgL_list2163z00_5466;

																								{	/* SawBbv/bbv-debug.scm 223 */
																									obj_t BgL_arg2164z00_5467;

																									BgL_arg2164z00_5467 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2162z00_5462, BNIL);
																									BgL_list2163z00_5466 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg2161z00_5461,
																										BgL_arg2164z00_5467);
																								}
																								BgL_arg2105z00_5416 =
																									BGl_formatz00zz__r4_output_6_10_3z00
																									(BGl_string3024z00zzsaw_bbvzd2debugzd2,
																									BgL_list2163z00_5466);
																						}}
																					else
																						{	/* SawBbv/bbv-debug.scm 222 */
																							BgL_arg2105z00_5416 = BFALSE;
																						}
																				}
																			else
																				{	/* SawBbv/bbv-debug.scm 220 */
																					BgL_arg2105z00_5416 = BFALSE;
																				}
																		}
																	}
																	{	/* SawBbv/bbv-debug.scm 251 */
																		obj_t BgL_list2106z00_5418;

																		BgL_list2106z00_5418 =
																			MAKE_YOUNG_PAIR(BgL_arg2105z00_5416,
																			BNIL);
																		BGl_fprintfz00zz__r4_output_6_10_3z00
																			(BgL_pz00_5404,
																			BGl_string3030z00zzsaw_bbvzd2debugzd2,
																			BgL_list2106z00_5418);
																	}
																}
																{	/* SawBbv/bbv-debug.scm 252 */
																	long BgL_arg2108z00_5419;

																	BgL_arg2108z00_5419 = (BgL_mz00_5405 + 2L);
																	BGl_dumpzd2marginzd2zzsaw_defsz00
																		(BgL_pz00_5404,
																		(int) (BgL_arg2108z00_5419));
																}
																{	/* SawBbv/bbv-debug.scm 253 */
																	obj_t BgL_tmpz00_6870;

																	BgL_tmpz00_6870 = ((obj_t) BgL_pz00_5404);
																	bgl_display_string
																		(BGl_string3031z00zzsaw_bbvzd2debugzd2,
																		BgL_tmpz00_6870);
																}
																bgl_display_fixnum(BINT(0L),
																	((obj_t) BgL_pz00_5404));
																{	/* SawBbv/bbv-debug.scm 253 */
																	obj_t BgL_tmpz00_6876;

																	BgL_tmpz00_6876 = ((obj_t) BgL_pz00_5404);
																	bgl_display_string
																		(BGl_string3002z00zzsaw_bbvzd2debugzd2,
																		BgL_tmpz00_6876);
																}
																{	/* SawBbv/bbv-debug.scm 253 */
																	obj_t BgL_tmpz00_6879;

																	BgL_tmpz00_6879 = ((obj_t) BgL_pz00_5404);
																	bgl_display_char(((unsigned char) 10),
																		BgL_tmpz00_6879);
																}
																{	/* SawBbv/bbv-debug.scm 254 */
																	long BgL_arg2109z00_5420;

																	BgL_arg2109z00_5420 = (BgL_mz00_5405 + 2L);
																	BGl_dumpzd2marginzd2zzsaw_defsz00
																		(BgL_pz00_5404,
																		(int) (BgL_arg2109z00_5420));
																}
																{	/* SawBbv/bbv-debug.scm 258 */
																	obj_t BgL_arg2110z00_5421;

																	{	/* SawBbv/bbv-debug.scm 258 */
																		obj_t BgL_zc3z04anonymousza32113ze3z87_5422;

																		BgL_zc3z04anonymousza32113ze3z87_5422 =
																			MAKE_FX_PROCEDURE
																			(BGl_z62zc3z04anonymousza32113ze3ze5zzsaw_bbvzd2debugzd2,
																			(int) (1L), (int) (1L));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza32113ze3z87_5422,
																			(int) (0L), ((obj_t) BgL_oz00_5403));
																		BgL_arg2110z00_5421 =
																			BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00
																			(BgL_zc3z04anonymousza32113ze3z87_5422);
																	}
																	{	/* SawBbv/bbv-debug.scm 255 */
																		obj_t BgL_list2111z00_5423;

																		BgL_list2111z00_5423 =
																			MAKE_YOUNG_PAIR(BgL_arg2110z00_5421,
																			BNIL);
																		BGl_fprintfz00zz__r4_output_6_10_3z00
																			(BgL_pz00_5404,
																			BGl_string3032z00zzsaw_bbvzd2debugzd2,
																			BgL_list2111z00_5423);
																}}
																{	/* SawBbv/bbv-debug.scm 263 */
																	long BgL_arg2118z00_5424;

																	BgL_arg2118z00_5424 = (BgL_mz00_5405 + 2L);
																	BGl_dumpzd2marginzd2zzsaw_defsz00
																		(BgL_pz00_5404,
																		(int) (BgL_arg2118z00_5424));
																}
																{	/* SawBbv/bbv-debug.scm 264 */
																	obj_t BgL_tmpz00_6897;

																	BgL_tmpz00_6897 = ((obj_t) BgL_pz00_5404);
																	bgl_display_string
																		(BGl_string3033z00zzsaw_bbvzd2debugzd2,
																		BgL_tmpz00_6897);
																}
																{	/* SawBbv/bbv-debug.scm 264 */
																	obj_t BgL_arg2119z00_5425;

																	{	/* SawBbv/bbv-debug.scm 264 */
																		obj_t BgL_arg2120z00_5426;

																		{	/* SawBbv/bbv-debug.scm 264 */
																			obj_t BgL_l1537z00_5427;

																			BgL_l1537z00_5427 =
																				(((BgL_blockz00_bglt) COBJECT(
																						((BgL_blockz00_bglt)
																							BgL_oz00_5403)))->BgL_predsz00);
																			if (NULLP(BgL_l1537z00_5427))
																				{	/* SawBbv/bbv-debug.scm 264 */
																					BgL_arg2120z00_5426 = BNIL;
																				}
																			else
																				{	/* SawBbv/bbv-debug.scm 264 */
																					obj_t BgL_head1539z00_5428;

																					BgL_head1539z00_5428 =
																						MAKE_YOUNG_PAIR
																						(BGl_lblze70ze7zzsaw_bbvzd2debugzd2
																						(CAR(BgL_l1537z00_5427)), BNIL);
																					{	/* SawBbv/bbv-debug.scm 264 */
																						obj_t BgL_g1542z00_5429;

																						BgL_g1542z00_5429 =
																							CDR(BgL_l1537z00_5427);
																						{
																							obj_t BgL_l1537z00_5431;
																							obj_t BgL_tail1540z00_5432;

																							BgL_l1537z00_5431 =
																								BgL_g1542z00_5429;
																							BgL_tail1540z00_5432 =
																								BgL_head1539z00_5428;
																						BgL_zc3z04anonymousza32123ze3z87_5430:
																							if (NULLP
																								(BgL_l1537z00_5431))
																								{	/* SawBbv/bbv-debug.scm 264 */
																									BgL_arg2120z00_5426 =
																										BgL_head1539z00_5428;
																								}
																							else
																								{	/* SawBbv/bbv-debug.scm 264 */
																									obj_t BgL_newtail1541z00_5433;

																									{	/* SawBbv/bbv-debug.scm 264 */
																										obj_t BgL_arg2126z00_5434;

																										{	/* SawBbv/bbv-debug.scm 264 */
																											obj_t BgL_arg2127z00_5435;

																											BgL_arg2127z00_5435 =
																												CAR(
																												((obj_t)
																													BgL_l1537z00_5431));
																											BgL_arg2126z00_5434 =
																												BGl_lblze70ze7zzsaw_bbvzd2debugzd2
																												(BgL_arg2127z00_5435);
																										}
																										BgL_newtail1541z00_5433 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2126z00_5434,
																											BNIL);
																									}
																									SET_CDR(BgL_tail1540z00_5432,
																										BgL_newtail1541z00_5433);
																									{	/* SawBbv/bbv-debug.scm 264 */
																										obj_t BgL_arg2125z00_5436;

																										BgL_arg2125z00_5436 =
																											CDR(
																											((obj_t)
																												BgL_l1537z00_5431));
																										{
																											obj_t
																												BgL_tail1540z00_6918;
																											obj_t BgL_l1537z00_6917;

																											BgL_l1537z00_6917 =
																												BgL_arg2125z00_5436;
																											BgL_tail1540z00_6918 =
																												BgL_newtail1541z00_5433;
																											BgL_tail1540z00_5432 =
																												BgL_tail1540z00_6918;
																											BgL_l1537z00_5431 =
																												BgL_l1537z00_6917;
																											goto
																												BgL_zc3z04anonymousza32123ze3z87_5430;
																										}
																									}
																								}
																						}
																					}
																				}
																		}
																		{	/* SawBbv/bbv-debug.scm 264 */
																			obj_t BgL_list2121z00_5437;

																			BgL_list2121z00_5437 =
																				MAKE_YOUNG_PAIR(BgL_arg2120z00_5426,
																				BNIL);
																			BgL_arg2119z00_5425 =
																				BGl_formatz00zz__r4_output_6_10_3z00
																				(BGl_string3034z00zzsaw_bbvzd2debugzd2,
																				BgL_list2121z00_5437);
																		}
																	}
																	bgl_display_obj(BgL_arg2119z00_5425,
																		BgL_pz00_5404);
																}
																{	/* SawBbv/bbv-debug.scm 264 */
																	obj_t BgL_tmpz00_6922;

																	BgL_tmpz00_6922 = ((obj_t) BgL_pz00_5404);
																	bgl_display_string
																		(BGl_string3035z00zzsaw_bbvzd2debugzd2,
																		BgL_tmpz00_6922);
																}
																{	/* SawBbv/bbv-debug.scm 264 */
																	obj_t BgL_tmpz00_6925;

																	BgL_tmpz00_6925 = ((obj_t) BgL_pz00_5404);
																	bgl_display_char(((unsigned char) 10),
																		BgL_tmpz00_6925);
																}
																{	/* SawBbv/bbv-debug.scm 265 */
																	long BgL_arg2131z00_5438;

																	BgL_arg2131z00_5438 = (BgL_mz00_5405 + 2L);
																	BGl_dumpzd2marginzd2zzsaw_defsz00
																		(BgL_pz00_5404,
																		(int) (BgL_arg2131z00_5438));
																}
																{	/* SawBbv/bbv-debug.scm 266 */
																	obj_t BgL_tmpz00_6931;

																	BgL_tmpz00_6931 = ((obj_t) BgL_pz00_5404);
																	bgl_display_string
																		(BGl_string3036z00zzsaw_bbvzd2debugzd2,
																		BgL_tmpz00_6931);
																}
																{	/* SawBbv/bbv-debug.scm 266 */
																	obj_t BgL_arg2132z00_5439;

																	{	/* SawBbv/bbv-debug.scm 266 */
																		obj_t BgL_arg2133z00_5440;

																		{	/* SawBbv/bbv-debug.scm 266 */
																			obj_t BgL_l1544z00_5441;

																			BgL_l1544z00_5441 =
																				(((BgL_blockz00_bglt) COBJECT(
																						((BgL_blockz00_bglt)
																							BgL_oz00_5403)))->BgL_succsz00);
																			if (NULLP(BgL_l1544z00_5441))
																				{	/* SawBbv/bbv-debug.scm 266 */
																					BgL_arg2133z00_5440 = BNIL;
																				}
																			else
																				{	/* SawBbv/bbv-debug.scm 266 */
																					obj_t BgL_head1546z00_5442;

																					BgL_head1546z00_5442 =
																						MAKE_YOUNG_PAIR
																						(BGl_lblze70ze7zzsaw_bbvzd2debugzd2
																						(CAR(BgL_l1544z00_5441)), BNIL);
																					{	/* SawBbv/bbv-debug.scm 266 */
																						obj_t BgL_g1549z00_5443;

																						BgL_g1549z00_5443 =
																							CDR(BgL_l1544z00_5441);
																						{
																							obj_t BgL_l1544z00_5445;
																							obj_t BgL_tail1547z00_5446;

																							BgL_l1544z00_5445 =
																								BgL_g1549z00_5443;
																							BgL_tail1547z00_5446 =
																								BgL_head1546z00_5442;
																						BgL_zc3z04anonymousza32136ze3z87_5444:
																							if (NULLP
																								(BgL_l1544z00_5445))
																								{	/* SawBbv/bbv-debug.scm 266 */
																									BgL_arg2133z00_5440 =
																										BgL_head1546z00_5442;
																								}
																							else
																								{	/* SawBbv/bbv-debug.scm 266 */
																									obj_t BgL_newtail1548z00_5447;

																									{	/* SawBbv/bbv-debug.scm 266 */
																										obj_t BgL_arg2139z00_5448;

																										{	/* SawBbv/bbv-debug.scm 266 */
																											obj_t BgL_arg2141z00_5449;

																											BgL_arg2141z00_5449 =
																												CAR(
																												((obj_t)
																													BgL_l1544z00_5445));
																											BgL_arg2139z00_5448 =
																												BGl_lblze70ze7zzsaw_bbvzd2debugzd2
																												(BgL_arg2141z00_5449);
																										}
																										BgL_newtail1548z00_5447 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2139z00_5448,
																											BNIL);
																									}
																									SET_CDR(BgL_tail1547z00_5446,
																										BgL_newtail1548z00_5447);
																									{	/* SawBbv/bbv-debug.scm 266 */
																										obj_t BgL_arg2138z00_5450;

																										BgL_arg2138z00_5450 =
																											CDR(
																											((obj_t)
																												BgL_l1544z00_5445));
																										{
																											obj_t
																												BgL_tail1547z00_6952;
																											obj_t BgL_l1544z00_6951;

																											BgL_l1544z00_6951 =
																												BgL_arg2138z00_5450;
																											BgL_tail1547z00_6952 =
																												BgL_newtail1548z00_5447;
																											BgL_tail1547z00_5446 =
																												BgL_tail1547z00_6952;
																											BgL_l1544z00_5445 =
																												BgL_l1544z00_6951;
																											goto
																												BgL_zc3z04anonymousza32136ze3z87_5444;
																										}
																									}
																								}
																						}
																					}
																				}
																		}
																		{	/* SawBbv/bbv-debug.scm 266 */
																			obj_t BgL_list2134z00_5451;

																			BgL_list2134z00_5451 =
																				MAKE_YOUNG_PAIR(BgL_arg2133z00_5440,
																				BNIL);
																			BgL_arg2132z00_5439 =
																				BGl_formatz00zz__r4_output_6_10_3z00
																				(BGl_string3034z00zzsaw_bbvzd2debugzd2,
																				BgL_list2134z00_5451);
																		}
																	}
																	bgl_display_obj(BgL_arg2132z00_5439,
																		BgL_pz00_5404);
																}
																{	/* SawBbv/bbv-debug.scm 266 */
																	obj_t BgL_tmpz00_6956;

																	BgL_tmpz00_6956 = ((obj_t) BgL_pz00_5404);
																	bgl_display_string
																		(BGl_string3035z00zzsaw_bbvzd2debugzd2,
																		BgL_tmpz00_6956);
																}
																{	/* SawBbv/bbv-debug.scm 266 */
																	obj_t BgL_tmpz00_6959;

																	BgL_tmpz00_6959 = ((obj_t) BgL_pz00_5404);
																	bgl_display_char(((unsigned char) 10),
																		BgL_tmpz00_6959);
																}
																{	/* SawBbv/bbv-debug.scm 267 */
																	long BgL_arg2144z00_5452;

																	BgL_arg2144z00_5452 = (BgL_mz00_5405 + 2L);
																	BGl_dumpzd2marginzd2zzsaw_defsz00
																		(BgL_pz00_5404,
																		(int) (BgL_arg2144z00_5452));
																}
																{	/* SawBbv/bbv-debug.scm 271 */
																	obj_t BgL_arg2145z00_5453;

																	{	/* SawBbv/bbv-debug.scm 271 */
																		obj_t BgL_zc3z04anonymousza32148ze3z87_5454;

																		BgL_zc3z04anonymousza32148ze3z87_5454 =
																			MAKE_FX_PROCEDURE
																			(BGl_z62zc3z04anonymousza32148ze3ze5zzsaw_bbvzd2debugzd2,
																			(int) (1L), (int) (1L));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza32148ze3z87_5454,
																			(int) (0L), ((obj_t) BgL_oz00_5403));
																		BgL_arg2145z00_5453 =
																			BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00
																			(BgL_zc3z04anonymousza32148ze3z87_5454);
																	}
																	{	/* SawBbv/bbv-debug.scm 268 */
																		obj_t BgL_list2146z00_5455;

																		BgL_list2146z00_5455 =
																			MAKE_YOUNG_PAIR(BgL_arg2145z00_5453,
																			BNIL);
																		BGl_fprintfz00zz__r4_output_6_10_3z00
																			(BgL_pz00_5404,
																			BGl_string3037z00zzsaw_bbvzd2debugzd2,
																			BgL_list2146z00_5455);
																}}
																BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_5404,
																	(int) (BgL_mz00_5405));
																{	/* SawBbv/bbv-debug.scm 273 */
																	obj_t BgL_tmpz00_6976;

																	BgL_tmpz00_6976 = ((obj_t) BgL_pz00_5404);
																	bgl_display_string
																		(BGl_string3038z00zzsaw_bbvzd2debugzd2,
																		BgL_tmpz00_6976);
																}
																bgl_display_obj(BgL_sepz00_5406, BgL_pz00_5404);
																{	/* SawBbv/bbv-debug.scm 273 */
																	obj_t BgL_tmpz00_6980;

																	BgL_tmpz00_6980 = ((obj_t) BgL_pz00_5404);
																	BgL_arg2094z00_5485 =
																		bgl_display_char(((unsigned char) 10),
																		BgL_tmpz00_6980);
															}}
															BgL_newtail1562z00_5484 =
																MAKE_YOUNG_PAIR(BgL_arg2094z00_5485, BNIL);
														}
														SET_CDR(BgL_tail1561z00_5483,
															BgL_newtail1562z00_5484);
														{	/* SawBbv/bbv-debug.scm 288 */
															obj_t BgL_arg2093z00_5487;

															BgL_arg2093z00_5487 =
																CDR(((obj_t) BgL_l1558z00_5482));
															{
																obj_t BgL_tail1561z00_6989;
																obj_t BgL_l1558z00_6988;

																BgL_l1558z00_6988 = BgL_arg2093z00_5487;
																BgL_tail1561z00_6989 = BgL_newtail1562z00_5484;
																BgL_tail1561z00_5483 = BgL_tail1561z00_6989;
																BgL_l1558z00_5482 = BgL_l1558z00_6988;
																goto BgL_zc3z04anonymousza32091ze3z87_5481;
															}
														}
													}
											}
										}
								}
								{	/* SawBbv/bbv-debug.scm 287 */
									obj_t BgL_list2089z00_5488;

									BgL_list2089z00_5488 =
										MAKE_YOUNG_PAIR(BgL_arg2088z00_5478, BNIL);
									BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string3034z00zzsaw_bbvzd2debugzd2,
										BgL_list2089z00_5488);
								}
							}
							{	/* SawBbv/bbv-debug.scm 291 */
								obj_t BgL_tmpz00_6992;

								BgL_tmpz00_6992 = ((obj_t) BgL_portz00_5255);
								bgl_display_string(BGl_string3044z00zzsaw_bbvzd2debugzd2,
									BgL_tmpz00_6992);
							}
							if (CBOOL(BgL_historyz00_5254))
								{	/* SawBbv/bbv-debug.scm 292 */
									{	/* SawBbv/bbv-debug.scm 294 */
										obj_t BgL_tmpz00_6997;

										BgL_tmpz00_6997 = ((obj_t) BgL_portz00_5255);
										bgl_display_string(BGl_string3045z00zzsaw_bbvzd2debugzd2,
											BgL_tmpz00_6997);
									}
									{	/* SawBbv/bbv-debug.scm 294 */
										obj_t BgL_tmpz00_7000;

										BgL_tmpz00_7000 = ((obj_t) BgL_portz00_5255);
										bgl_display_char(((unsigned char) 10), BgL_tmpz00_7000);
									}
									{	/* SawBbv/bbv-debug.scm 295 */
										obj_t BgL_tmpz00_7003;

										BgL_tmpz00_7003 = ((obj_t) BgL_portz00_5255);
										bgl_display_string(BGl_string3046z00zzsaw_bbvzd2debugzd2,
											BgL_tmpz00_7003);
									}
									bgl_display_obj(BgL_historyz00_5254, BgL_portz00_5255);
									{	/* SawBbv/bbv-debug.scm 295 */
										obj_t BgL_tmpz00_7007;

										BgL_tmpz00_7007 = ((obj_t) BgL_portz00_5255);
										bgl_display_char(((unsigned char) 10), BgL_tmpz00_7007);
								}}
							else
								{	/* SawBbv/bbv-debug.scm 292 */
									{	/* SawBbv/bbv-debug.scm 296 */
										obj_t BgL_tmpz00_7010;

										BgL_tmpz00_7010 = ((obj_t) BgL_portz00_5255);
										bgl_display_string(BGl_string3047z00zzsaw_bbvzd2debugzd2,
											BgL_tmpz00_7010);
									}
									{	/* SawBbv/bbv-debug.scm 296 */
										obj_t BgL_tmpz00_7013;

										BgL_tmpz00_7013 = ((obj_t) BgL_portz00_5255);
										bgl_display_char(((unsigned char) 10), BgL_tmpz00_7013);
								}}
							{	/* SawBbv/bbv-debug.scm 298 */
								obj_t BgL_tmpz00_7016;

								BgL_tmpz00_7016 = ((obj_t) BgL_portz00_5255);
								bgl_display_string(BGl_string3038z00zzsaw_bbvzd2debugzd2,
									BgL_tmpz00_7016);
							}
							{	/* SawBbv/bbv-debug.scm 298 */
								obj_t BgL_tmpz00_7019;

								BgL_tmpz00_7019 = ((obj_t) BgL_portz00_5255);
								bgl_display_char(((unsigned char) 10), BgL_tmpz00_7019);
							}
							return BgL_idz00_5468;
						}
					}
				}
			}
		}

	}



/* lbl~0 */
	obj_t BGl_lblze70ze7zzsaw_bbvzd2debugzd2(obj_t BgL_nz00_2783)
	{
		{	/* SawBbv/bbv-debug.scm 239 */
			{	/* SawBbv/bbv-debug.scm 239 */
				bool_t BgL_test3345z00_7022;

				{	/* SawBbv/bbv-debug.scm 239 */
					obj_t BgL_classz00_4277;

					BgL_classz00_4277 = BGl_blockz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_nz00_2783))
						{	/* SawBbv/bbv-debug.scm 239 */
							BgL_objectz00_bglt BgL_arg1807z00_4279;

							BgL_arg1807z00_4279 = (BgL_objectz00_bglt) (BgL_nz00_2783);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-debug.scm 239 */
									long BgL_idxz00_4285;

									BgL_idxz00_4285 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4279);
									BgL_test3345z00_7022 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4285 + 1L)) == BgL_classz00_4277);
								}
							else
								{	/* SawBbv/bbv-debug.scm 239 */
									bool_t BgL_res2981z00_4310;

									{	/* SawBbv/bbv-debug.scm 239 */
										obj_t BgL_oclassz00_4293;

										{	/* SawBbv/bbv-debug.scm 239 */
											obj_t BgL_arg1815z00_4301;
											long BgL_arg1816z00_4302;

											BgL_arg1815z00_4301 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-debug.scm 239 */
												long BgL_arg1817z00_4303;

												BgL_arg1817z00_4303 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4279);
												BgL_arg1816z00_4302 =
													(BgL_arg1817z00_4303 - OBJECT_TYPE);
											}
											BgL_oclassz00_4293 =
												VECTOR_REF(BgL_arg1815z00_4301, BgL_arg1816z00_4302);
										}
										{	/* SawBbv/bbv-debug.scm 239 */
											bool_t BgL__ortest_1115z00_4294;

											BgL__ortest_1115z00_4294 =
												(BgL_classz00_4277 == BgL_oclassz00_4293);
											if (BgL__ortest_1115z00_4294)
												{	/* SawBbv/bbv-debug.scm 239 */
													BgL_res2981z00_4310 = BgL__ortest_1115z00_4294;
												}
											else
												{	/* SawBbv/bbv-debug.scm 239 */
													long BgL_odepthz00_4295;

													{	/* SawBbv/bbv-debug.scm 239 */
														obj_t BgL_arg1804z00_4296;

														BgL_arg1804z00_4296 = (BgL_oclassz00_4293);
														BgL_odepthz00_4295 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4296);
													}
													if ((1L < BgL_odepthz00_4295))
														{	/* SawBbv/bbv-debug.scm 239 */
															obj_t BgL_arg1802z00_4298;

															{	/* SawBbv/bbv-debug.scm 239 */
																obj_t BgL_arg1803z00_4299;

																BgL_arg1803z00_4299 = (BgL_oclassz00_4293);
																BgL_arg1802z00_4298 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4299,
																	1L);
															}
															BgL_res2981z00_4310 =
																(BgL_arg1802z00_4298 == BgL_classz00_4277);
														}
													else
														{	/* SawBbv/bbv-debug.scm 239 */
															BgL_res2981z00_4310 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test3345z00_7022 = BgL_res2981z00_4310;
								}
						}
					else
						{	/* SawBbv/bbv-debug.scm 239 */
							BgL_test3345z00_7022 = ((bool_t) 0);
						}
				}
				if (BgL_test3345z00_7022)
					{	/* SawBbv/bbv-debug.scm 239 */
						return
							BINT(
							(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_nz00_2783)))->BgL_labelz00));
					}
				else
					{	/* SawBbv/bbv-debug.scm 239 */
						BGL_TAIL return bgl_typeof(BgL_nz00_2783);
					}
			}
		}

	}



/* &<@anonymous:2148> */
	obj_t BGl_z62zc3z04anonymousza32148ze3ze5zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5256, obj_t BgL_opz00_5258)
	{
		{	/* SawBbv/bbv-debug.scm 270 */
			{	/* SawBbv/bbv-debug.scm 271 */
				BgL_blockz00_bglt BgL_i1190z00_5257;

				BgL_i1190z00_5257 =
					((BgL_blockz00_bglt) PROCEDURE_REF(BgL_envz00_5256, (int) (0L)));
				{	/* SawBbv/bbv-debug.scm 271 */
					bool_t BgL_tmpz00_7052;

					{
						BgL_rtl_insz00_bglt BgL_oz00_5490;
						obj_t BgL_pz00_5491;

						{	/* SawBbv/bbv-debug.scm 271 */
							obj_t BgL_g1553z00_5493;

							BgL_g1553z00_5493 =
								(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_i1190z00_5257)))->BgL_firstz00);
							{
								obj_t BgL_l1551z00_5495;

								BgL_l1551z00_5495 = BgL_g1553z00_5493;
							BgL_zc3z04anonymousza32149ze3z87_5494:
								if (PAIRP(BgL_l1551z00_5495))
									{	/* SawBbv/bbv-debug.scm 271 */
										{	/* SawBbv/bbv-debug.scm 271 */
											obj_t BgL_iz00_5496;

											BgL_iz00_5496 = CAR(BgL_l1551z00_5495);
											BgL_oz00_5490 = ((BgL_rtl_insz00_bglt) BgL_iz00_5496);
											BgL_pz00_5491 = BgL_opz00_5258;
											if (CBOOL(
													(((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt) BgL_oz00_5490)))->
														BgL_destz00)))
												{	/* SawBbv/bbv-debug.scm 227 */
													{	/* SawBbv/bbv-debug.scm 228 */
														obj_t BgL_tmpz00_7062;

														BgL_tmpz00_7062 = ((obj_t) BgL_pz00_5491);
														bgl_display_string
															(BGl_string3048z00zzsaw_bbvzd2debugzd2,
															BgL_tmpz00_7062);
													}
													{	/* SawBbv/bbv-debug.scm 229 */
														obj_t BgL_arg2156z00_5492;

														BgL_arg2156z00_5492 =
															(((BgL_rtl_insz00_bglt) COBJECT(
																	((BgL_rtl_insz00_bglt) BgL_oz00_5490)))->
															BgL_destz00);
														BGl_dumpz00zzsaw_defsz00(BgL_arg2156z00_5492,
															BgL_pz00_5491, (int) (0L));
													}
													{	/* SawBbv/bbv-debug.scm 230 */
														obj_t BgL_tmpz00_7069;

														BgL_tmpz00_7069 = ((obj_t) BgL_pz00_5491);
														bgl_display_string
															(BGl_string3049z00zzsaw_bbvzd2debugzd2,
															BgL_tmpz00_7069);
												}}
											else
												{	/* SawBbv/bbv-debug.scm 227 */
													BFALSE;
												}
											BGl_dumpzd2inszd2rhsz00zzsaw_defsz00(
												((BgL_rtl_insz00_bglt) BgL_oz00_5490), BgL_pz00_5491,
												BINT(0L));
											if (CBOOL(
													(((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt) BgL_oz00_5490)))->
														BgL_destz00)))
												{	/* SawBbv/bbv-debug.scm 233 */
													obj_t BgL_tmpz00_7079;

													BgL_tmpz00_7079 = ((obj_t) BgL_pz00_5491);
													bgl_display_string
														(BGl_string3050z00zzsaw_bbvzd2debugzd2,
														BgL_tmpz00_7079);
												}
											else
												{	/* SawBbv/bbv-debug.scm 232 */
													BFALSE;
												}
											{	/* SawBbv/bbv-debug.scm 234 */
												obj_t BgL_tmpz00_7082;

												BgL_tmpz00_7082 = ((obj_t) BgL_pz00_5491);
												bgl_display_char(((unsigned char) 10), BgL_tmpz00_7082);
										}}
										{
											obj_t BgL_l1551z00_7086;

											BgL_l1551z00_7086 = CDR(BgL_l1551z00_5495);
											BgL_l1551z00_5495 = BgL_l1551z00_7086;
											goto BgL_zc3z04anonymousza32149ze3z87_5494;
										}
									}
								else
									{	/* SawBbv/bbv-debug.scm 271 */
										BgL_tmpz00_7052 = ((bool_t) 1);
									}
							}
						}
					}
					return BBOOL(BgL_tmpz00_7052);
				}
			}
		}

	}



/* &<@anonymous:2113> */
	obj_t BGl_z62zc3z04anonymousza32113ze3ze5zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5259, obj_t BgL_opz00_5261)
	{
		{	/* SawBbv/bbv-debug.scm 257 */
			{	/* SawBbv/bbv-debug.scm 258 */
				BgL_blockz00_bglt BgL_i1190z00_5260;

				BgL_i1190z00_5260 =
					((BgL_blockz00_bglt) PROCEDURE_REF(BgL_envz00_5259, (int) (0L)));
				{	/* SawBbv/bbv-debug.scm 258 */
					bool_t BgL_tmpz00_7092;

					{	/* SawBbv/bbv-debug.scm 258 */
						obj_t BgL_g1536z00_5497;

						{	/* SawBbv/bbv-debug.scm 261 */
							BgL_bbvzd2ctxzd2_bglt BgL_i1191z00_5498;

							{
								BgL_blocksz00_bglt BgL_auxz00_7093;

								{
									obj_t BgL_auxz00_7094;

									{	/* SawBbv/bbv-debug.scm 261 */
										BgL_objectz00_bglt BgL_tmpz00_7095;

										BgL_tmpz00_7095 = ((BgL_objectz00_bglt) BgL_i1190z00_5260);
										BgL_auxz00_7094 = BGL_OBJECT_WIDENING(BgL_tmpz00_7095);
									}
									BgL_auxz00_7093 = ((BgL_blocksz00_bglt) BgL_auxz00_7094);
								}
								BgL_i1191z00_5498 =
									(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7093))->BgL_ctxz00);
							}
							BgL_g1536z00_5497 =
								(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_i1191z00_5498))->
								BgL_entriesz00);
						}
						{
							obj_t BgL_l1534z00_5500;

							BgL_l1534z00_5500 = BgL_g1536z00_5497;
						BgL_zc3z04anonymousza32114ze3z87_5499:
							if (PAIRP(BgL_l1534z00_5500))
								{	/* SawBbv/bbv-debug.scm 261 */
									{	/* SawBbv/bbv-debug.scm 259 */
										obj_t BgL_ez00_5501;

										BgL_ez00_5501 = CAR(BgL_l1534z00_5500);
										bgl_display_obj
											(BGl_shapezd2ctxzd2entryz00zzsaw_bbvzd2debugzd2
											(BgL_ez00_5501), BgL_opz00_5261);
										{	/* SawBbv/bbv-debug.scm 260 */
											obj_t BgL_tmpz00_7106;

											BgL_tmpz00_7106 = ((obj_t) BgL_opz00_5261);
											bgl_display_string(BGl_string3051z00zzsaw_bbvzd2debugzd2,
												BgL_tmpz00_7106);
										}
									}
									{
										obj_t BgL_l1534z00_7109;

										BgL_l1534z00_7109 = CDR(BgL_l1534z00_5500);
										BgL_l1534z00_5500 = BgL_l1534z00_7109;
										goto BgL_zc3z04anonymousza32114ze3z87_5499;
									}
								}
							else
								{	/* SawBbv/bbv-debug.scm 261 */
									BgL_tmpz00_7092 = ((bool_t) 1);
								}
						}
					}
					return BBOOL(BgL_tmpz00_7092);
				}
			}
		}

	}



/* sort-blocks */
	obj_t BGl_sortzd2blockszd2zzsaw_bbvzd2debugzd2(obj_t BgL_bz00_117)
	{
		{	/* SawBbv/bbv-debug.scm 315 */
			return
				BGl_sortz00zz__r4_vectors_6_8z00(BGl_proc3052z00zzsaw_bbvzd2debugzd2,
				BgL_bz00_117);
		}

	}



/* &<@anonymous:2185> */
	obj_t BGl_z62zc3z04anonymousza32185ze3ze5zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5263, obj_t BgL_xz00_5264, obj_t BgL_yz00_5265)
	{
		{	/* SawBbv/bbv-debug.scm 316 */
			return
				BBOOL(
				((long) (
						(((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_xz00_5264)))->BgL_labelz00)) <=
					(long) (
						(((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_yz00_5265)))->BgL_labelz00))));
		}

	}



/* log-blocks */
	BGL_EXPORTED_DEF obj_t
		BGl_logzd2blockszd2zzsaw_bbvzd2debugzd2(BgL_globalz00_bglt
		BgL_globalz00_118, obj_t BgL_paramsz00_119, obj_t BgL_bvz00_120)
	{
		{	/* SawBbv/bbv-debug.scm 321 */
			{	/* SawBbv/bbv-debug.scm 326 */
				obj_t BgL_marginsz00_2841;

				BgL_marginsz00_2841 = CNST_TABLE_REF(4);
				{	/* SawBbv/bbv-debug.scm 404 */
					obj_t BgL_arg2188z00_2851;

					{	/* SawBbv/bbv-debug.scm 404 */
						obj_t BgL_tmpz00_7122;

						BgL_tmpz00_7122 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_arg2188z00_2851 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_7122);
					}
					{	/* SawBbv/bbv-debug.scm 406 */
						obj_t BgL_zc3z04anonymousza32190ze3z87_5266;

						BgL_zc3z04anonymousza32190ze3z87_5266 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza32190ze3ze5zzsaw_bbvzd2debugzd2,
							(int) (0L), (int) (4L));
						PROCEDURE_SET(BgL_zc3z04anonymousza32190ze3z87_5266, (int) (0L),
							BgL_marginsz00_2841);
						PROCEDURE_SET(BgL_zc3z04anonymousza32190ze3z87_5266, (int) (1L),
							((obj_t) BgL_globalz00_118));
						PROCEDURE_SET(BgL_zc3z04anonymousza32190ze3z87_5266, (int) (2L),
							BgL_paramsz00_119);
						PROCEDURE_SET(BgL_zc3z04anonymousza32190ze3z87_5266, (int) (3L),
							BgL_bvz00_120);
						return
							BGl_withzd2outputzd2tozd2portzd2zz__r4_ports_6_10_1z00
							(BgL_arg2188z00_2851, BgL_zc3z04anonymousza32190ze3z87_5266);
					}
				}
			}
		}

	}



/* &log-blocks */
	obj_t BGl_z62logzd2blockszb0zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5267,
		obj_t BgL_globalz00_5268, obj_t BgL_paramsz00_5269, obj_t BgL_bvz00_5270)
	{
		{	/* SawBbv/bbv-debug.scm 321 */
			return
				BGl_logzd2blockszd2zzsaw_bbvzd2debugzd2(
				((BgL_globalz00_bglt) BgL_globalz00_5268), BgL_paramsz00_5269,
				BgL_bvz00_5270);
		}

	}



/* &<@anonymous:2190> */
	obj_t BGl_z62zc3z04anonymousza32190ze3ze5zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5271)
	{
		{	/* SawBbv/bbv-debug.scm 405 */
			{	/* SawBbv/bbv-debug.scm 406 */
				obj_t BgL_marginsz00_5272;
				BgL_globalz00_bglt BgL_globalz00_5273;
				obj_t BgL_paramsz00_5274;
				obj_t BgL_bvz00_5275;

				BgL_marginsz00_5272 =
					((obj_t) PROCEDURE_REF(BgL_envz00_5271, (int) (0L)));
				BgL_globalz00_5273 =
					((BgL_globalz00_bglt) PROCEDURE_REF(BgL_envz00_5271, (int) (1L)));
				BgL_paramsz00_5274 =
					((obj_t) PROCEDURE_REF(BgL_envz00_5271, (int) (2L)));
				BgL_bvz00_5275 = ((obj_t) PROCEDURE_REF(BgL_envz00_5271, (int) (3L)));
				{
					obj_t BgL_mblockz00_5563;
					obj_t BgL_creatorz00_5564;
					obj_t BgL_mblockz00_5547;
					obj_t BgL_mergesz00_5548;
					BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_5534;
					BgL_blockz00_bglt BgL_bz00_5516;
					BgL_blockz00_bglt BgL_bz00_5507;

					{	/* SawBbv/bbv-debug.scm 406 */
						obj_t BgL_port1618z00_5587;

						{	/* SawBbv/bbv-debug.scm 406 */
							obj_t BgL_tmpz00_7152;

							BgL_tmpz00_7152 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1618z00_5587 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7152);
						}
						bgl_display_string(BGl_string3059z00zzsaw_bbvzd2debugzd2,
							BgL_port1618z00_5587);
						{	/* SawBbv/bbv-debug.scm 406 */
							obj_t BgL_arg2191z00_5588;

							BgL_arg2191z00_5588 =
								BGl_shapez00zztools_shapez00(((obj_t) BgL_globalz00_5273));
							bgl_display_obj(BgL_arg2191z00_5588, BgL_port1618z00_5587);
						}
						bgl_display_string(BGl_string3051z00zzsaw_bbvzd2debugzd2,
							BgL_port1618z00_5587);
						{	/* SawBbv/bbv-debug.scm 406 */
							obj_t BgL_arg2192z00_5589;

							if (NULLP(BgL_paramsz00_5274))
								{	/* SawBbv/bbv-debug.scm 406 */
									BgL_arg2192z00_5589 = BNIL;
								}
							else
								{	/* SawBbv/bbv-debug.scm 406 */
									obj_t BgL_head1614z00_5590;

									{	/* SawBbv/bbv-debug.scm 406 */
										obj_t BgL_arg2199z00_5591;

										{	/* SawBbv/bbv-debug.scm 406 */
											obj_t BgL_arg2200z00_5592;

											BgL_arg2200z00_5592 = CAR(((obj_t) BgL_paramsz00_5274));
											BgL_arg2199z00_5591 =
												BGl_shapez00zztools_shapez00(BgL_arg2200z00_5592);
										}
										BgL_head1614z00_5590 =
											MAKE_YOUNG_PAIR(BgL_arg2199z00_5591, BNIL);
									}
									{	/* SawBbv/bbv-debug.scm 406 */
										obj_t BgL_g1617z00_5593;

										BgL_g1617z00_5593 = CDR(((obj_t) BgL_paramsz00_5274));
										{
											obj_t BgL_l1612z00_5595;
											obj_t BgL_tail1615z00_5596;

											BgL_l1612z00_5595 = BgL_g1617z00_5593;
											BgL_tail1615z00_5596 = BgL_head1614z00_5590;
										BgL_zc3z04anonymousza32194ze3z87_5594:
											if (NULLP(BgL_l1612z00_5595))
												{	/* SawBbv/bbv-debug.scm 406 */
													BgL_arg2192z00_5589 = BgL_head1614z00_5590;
												}
											else
												{	/* SawBbv/bbv-debug.scm 406 */
													obj_t BgL_newtail1616z00_5597;

													{	/* SawBbv/bbv-debug.scm 406 */
														obj_t BgL_arg2197z00_5598;

														{	/* SawBbv/bbv-debug.scm 406 */
															obj_t BgL_arg2198z00_5599;

															BgL_arg2198z00_5599 =
																CAR(((obj_t) BgL_l1612z00_5595));
															BgL_arg2197z00_5598 =
																BGl_shapez00zztools_shapez00
																(BgL_arg2198z00_5599);
														}
														BgL_newtail1616z00_5597 =
															MAKE_YOUNG_PAIR(BgL_arg2197z00_5598, BNIL);
													}
													SET_CDR(BgL_tail1615z00_5596,
														BgL_newtail1616z00_5597);
													{	/* SawBbv/bbv-debug.scm 406 */
														obj_t BgL_arg2196z00_5600;

														BgL_arg2196z00_5600 =
															CDR(((obj_t) BgL_l1612z00_5595));
														{
															obj_t BgL_tail1615z00_7178;
															obj_t BgL_l1612z00_7177;

															BgL_l1612z00_7177 = BgL_arg2196z00_5600;
															BgL_tail1615z00_7178 = BgL_newtail1616z00_5597;
															BgL_tail1615z00_5596 = BgL_tail1615z00_7178;
															BgL_l1612z00_5595 = BgL_l1612z00_7177;
															goto BgL_zc3z04anonymousza32194ze3z87_5594;
														}
													}
												}
										}
									}
								}
							bgl_display_obj(BgL_arg2192z00_5589, BgL_port1618z00_5587);
						}
						bgl_display_char(((unsigned char) 10), BgL_port1618z00_5587);
					}
					{	/* SawBbv/bbv-debug.scm 407 */
						obj_t BgL_g1621z00_5601;

						BgL_g1621z00_5601 =
							BGl_sortzd2blockszd2zzsaw_bbvzd2debugzd2(BgL_bvz00_5275);
						{
							obj_t BgL_l1619z00_5603;

							BgL_l1619z00_5603 = BgL_g1621z00_5601;
						BgL_zc3z04anonymousza32201ze3z87_5602:
							if (PAIRP(BgL_l1619z00_5603))
								{	/* SawBbv/bbv-debug.scm 407 */
									{	/* SawBbv/bbv-debug.scm 407 */
										obj_t BgL_arg2203z00_5604;

										BgL_arg2203z00_5604 = CAR(BgL_l1619z00_5603);
										BgL_bz00_5507 = ((BgL_blockz00_bglt) BgL_arg2203z00_5604);
										{	/* SawBbv/bbv-debug.scm 399 */
											obj_t BgL_port1607z00_5508;

											{	/* SawBbv/bbv-debug.scm 399 */
												obj_t BgL_tmpz00_7185;

												BgL_tmpz00_7185 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_port1607z00_5508 =
													BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7185);
											}
											bgl_display_string(BGl_string3053z00zzsaw_bbvzd2debugzd2,
												BgL_port1607z00_5508);
											bgl_display_char(((unsigned char) 10),
												BgL_port1607z00_5508);
										}
										{	/* SawBbv/bbv-debug.scm 400 */
											obj_t BgL_port1608z00_5509;

											{	/* SawBbv/bbv-debug.scm 400 */
												obj_t BgL_tmpz00_7190;

												BgL_tmpz00_7190 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_port1608z00_5509 =
													BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7190);
											}
											bgl_display_string(BGl_string3026z00zzsaw_bbvzd2debugzd2,
												BgL_port1608z00_5509);
											{	/* SawBbv/bbv-debug.scm 400 */
												int BgL_arg2361z00_5510;

												BgL_arg2361z00_5510 =
													(((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt) BgL_bz00_5507)))->
													BgL_labelz00);
												bgl_display_obj(BINT(BgL_arg2361z00_5510),
													BgL_port1608z00_5509);
											}
											bgl_display_string(BGl_string3038z00zzsaw_bbvzd2debugzd2,
												BgL_port1608z00_5509);
											bgl_display_char(((unsigned char) 10),
												BgL_port1608z00_5509);
										}
										{	/* SawBbv/bbv-debug.scm 401 */
											obj_t BgL_g1611z00_5511;

											{	/* SawBbv/bbv-debug.scm 401 */
												obj_t BgL_arg2366z00_5512;

												{
													BgL_blockvz00_bglt BgL_auxz00_7200;

													{
														obj_t BgL_auxz00_7201;

														{	/* SawBbv/bbv-debug.scm 401 */
															BgL_objectz00_bglt BgL_tmpz00_7202;

															BgL_tmpz00_7202 =
																((BgL_objectz00_bglt) BgL_bz00_5507);
															BgL_auxz00_7201 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_7202);
														}
														BgL_auxz00_7200 =
															((BgL_blockvz00_bglt) BgL_auxz00_7201);
													}
													BgL_arg2366z00_5512 =
														(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_7200))->
														BgL_versionsz00);
												}
												BgL_g1611z00_5511 =
													BGl_sortzd2blockszd2zzsaw_bbvzd2debugzd2
													(BgL_arg2366z00_5512);
											}
											{
												obj_t BgL_l1609z00_5514;

												BgL_l1609z00_5514 = BgL_g1611z00_5511;
											BgL_zc3z04anonymousza32362ze3z87_5513:
												if (PAIRP(BgL_l1609z00_5514))
													{	/* SawBbv/bbv-debug.scm 401 */
														{	/* SawBbv/bbv-debug.scm 401 */
															obj_t BgL_arg2364z00_5515;

															BgL_arg2364z00_5515 = CAR(BgL_l1609z00_5514);
															BgL_bz00_5516 =
																((BgL_blockz00_bglt) BgL_arg2364z00_5515);
															{	/* SawBbv/bbv-debug.scm 390 */
																obj_t BgL_port1606z00_5517;

																{	/* SawBbv/bbv-debug.scm 390 */
																	obj_t BgL_tmpz00_7211;

																	BgL_tmpz00_7211 = BGL_CURRENT_DYNAMIC_ENV();
																	BgL_port1606z00_5517 =
																		BGL_ENV_CURRENT_OUTPUT_PORT
																		(BgL_tmpz00_7211);
																}
																bgl_display_string
																	(BGl_string3051z00zzsaw_bbvzd2debugzd2,
																	BgL_port1606z00_5517);
																{	/* SawBbv/bbv-debug.scm 391 */
																	obj_t BgL_arg2341z00_5518;

																	{	/* SawBbv/bbv-debug.scm 391 */
																		obj_t BgL_arg2342z00_5519;

																		BgL_arg2342z00_5519 =
																			BGl_logzd2labelze70z35zzsaw_bbvzd2debugzd2
																			(((obj_t) BgL_bz00_5516), BNIL);
																		BgL_arg2341z00_5518 =
																			BGl_paddinglze70ze7zzsaw_bbvzd2debugzd2
																			(BgL_marginsz00_5272, 4L,
																			BgL_arg2342z00_5519);
																	}
																	bgl_display_obj(BgL_arg2341z00_5518,
																		BgL_port1606z00_5517);
																}
																{	/* SawBbv/bbv-debug.scm 392 */
																	obj_t BgL_arg2345z00_5520;

																	{	/* SawBbv/bbv-debug.scm 392 */
																		obj_t BgL_arg2346z00_5521;

																		{	/* SawBbv/bbv-debug.scm 392 */
																			bool_t BgL_test3358z00_7219;

																			{	/* SawBbv/bbv-debug.scm 392 */
																				obj_t BgL_tmpz00_7220;

																				{
																					BgL_blocksz00_bglt BgL_auxz00_7221;

																					{
																						obj_t BgL_auxz00_7222;

																						{	/* SawBbv/bbv-debug.scm 392 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_7223;
																							BgL_tmpz00_7223 =
																								((BgL_objectz00_bglt)
																								BgL_bz00_5516);
																							BgL_auxz00_7222 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_7223);
																						}
																						BgL_auxz00_7221 =
																							((BgL_blocksz00_bglt)
																							BgL_auxz00_7222);
																					}
																					BgL_tmpz00_7220 =
																						(((BgL_blocksz00_bglt)
																							COBJECT(BgL_auxz00_7221))->
																						BgL_mblockz00);
																				}
																				BgL_test3358z00_7219 =
																					CBOOL(BgL_tmpz00_7220);
																			}
																			if (BgL_test3358z00_7219)
																				{	/* SawBbv/bbv-debug.scm 392 */
																					obj_t BgL_arg2348z00_5522;

																					{
																						BgL_blocksz00_bglt BgL_auxz00_7229;

																						{
																							obj_t BgL_auxz00_7230;

																							{	/* SawBbv/bbv-debug.scm 392 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_7231;
																								BgL_tmpz00_7231 =
																									((BgL_objectz00_bglt)
																									BgL_bz00_5516);
																								BgL_auxz00_7230 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_7231);
																							}
																							BgL_auxz00_7229 =
																								((BgL_blocksz00_bglt)
																								BgL_auxz00_7230);
																						}
																						BgL_arg2348z00_5522 =
																							(((BgL_blocksz00_bglt)
																								COBJECT(BgL_auxz00_7229))->
																							BgL_mblockz00);
																					}
																					{	/* SawBbv/bbv-debug.scm 392 */
																						obj_t BgL_list2368z00_5523;

																						BgL_list2368z00_5523 =
																							MAKE_YOUNG_PAIR
																							(BGl_string3054z00zzsaw_bbvzd2debugzd2,
																							BNIL);
																						BgL_arg2346z00_5521 =
																							BGl_logzd2labelze70z35zzsaw_bbvzd2debugzd2
																							(BgL_arg2348z00_5522,
																							BgL_list2368z00_5523);
																					}
																				}
																			else
																				{	/* SawBbv/bbv-debug.scm 392 */
																					BgL_arg2346z00_5521 = BFALSE;
																				}
																		}
																		BgL_arg2345z00_5520 =
																			BGl_paddingrze70ze7zzsaw_bbvzd2debugzd2
																			(BgL_marginsz00_5272, 6L,
																			BgL_arg2346z00_5521);
																	}
																	bgl_display_obj(BgL_arg2345z00_5520,
																		BgL_port1606z00_5517);
																}
																{	/* SawBbv/bbv-debug.scm 393 */
																	obj_t BgL_arg2349z00_5524;

																	{	/* SawBbv/bbv-debug.scm 393 */
																		obj_t BgL_arg2350z00_5525;

																		{	/* SawBbv/bbv-debug.scm 393 */
																			obj_t BgL_arg2351z00_5526;
																			obj_t BgL_arg2352z00_5527;

																			{
																				BgL_blocksz00_bglt BgL_auxz00_7240;

																				{
																					obj_t BgL_auxz00_7241;

																					{	/* SawBbv/bbv-debug.scm 393 */
																						BgL_objectz00_bglt BgL_tmpz00_7242;

																						BgL_tmpz00_7242 =
																							((BgL_objectz00_bglt)
																							BgL_bz00_5516);
																						BgL_auxz00_7241 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_7242);
																					}
																					BgL_auxz00_7240 =
																						((BgL_blocksz00_bglt)
																						BgL_auxz00_7241);
																				}
																				BgL_arg2351z00_5526 =
																					(((BgL_blocksz00_bglt)
																						COBJECT(BgL_auxz00_7240))->
																					BgL_mblockz00);
																			}
																			{
																				BgL_blocksz00_bglt BgL_auxz00_7247;

																				{
																					obj_t BgL_auxz00_7248;

																					{	/* SawBbv/bbv-debug.scm 393 */
																						BgL_objectz00_bglt BgL_tmpz00_7249;

																						BgL_tmpz00_7249 =
																							((BgL_objectz00_bglt)
																							BgL_bz00_5516);
																						BgL_auxz00_7248 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_7249);
																					}
																					BgL_auxz00_7247 =
																						((BgL_blocksz00_bglt)
																						BgL_auxz00_7248);
																				}
																				BgL_arg2352z00_5527 =
																					(((BgL_blocksz00_bglt)
																						COBJECT(BgL_auxz00_7247))->
																					BgL_creatorz00);
																			}
																			BgL_mblockz00_5563 = BgL_arg2351z00_5526;
																			BgL_creatorz00_5564 = BgL_arg2352z00_5527;
																			if (PAIRP(BgL_creatorz00_5564))
																				{	/* SawBbv/bbv-debug.scm 375 */
																					int BgL_arg2313z00_5565;
																					int BgL_arg2314z00_5566;

																					BgL_arg2313z00_5565 =
																						(((BgL_blockz00_bglt) COBJECT(
																								((BgL_blockz00_bglt)
																									CAR(BgL_creatorz00_5564))))->
																						BgL_labelz00);
																					BgL_arg2314z00_5566 =
																						(((BgL_blockz00_bglt)
																							COBJECT(((BgL_blockz00_bglt)
																									CDR(BgL_creatorz00_5564))))->
																						BgL_labelz00);
																					{	/* SawBbv/bbv-debug.scm 374 */
																						obj_t BgL_list2315z00_5567;

																						{	/* SawBbv/bbv-debug.scm 374 */
																							obj_t BgL_arg2316z00_5568;

																							BgL_arg2316z00_5568 =
																								MAKE_YOUNG_PAIR(BINT
																								(BgL_arg2314z00_5566), BNIL);
																							BgL_list2315z00_5567 =
																								MAKE_YOUNG_PAIR(BINT
																								(BgL_arg2313z00_5565),
																								BgL_arg2316z00_5568);
																						}
																						BgL_arg2350z00_5525 =
																							BGl_formatz00zz__r4_output_6_10_3z00
																							(BGl_string3057z00zzsaw_bbvzd2debugzd2,
																							BgL_list2315z00_5567);
																				}}
																			else
																				{	/* SawBbv/bbv-debug.scm 377 */
																					bool_t BgL_test3360z00_7267;

																					{	/* SawBbv/bbv-debug.scm 377 */
																						obj_t BgL_classz00_5569;

																						BgL_classz00_5569 =
																							BGl_blockz00zzsaw_defsz00;
																						if (BGL_OBJECTP
																							(BgL_creatorz00_5564))
																							{	/* SawBbv/bbv-debug.scm 377 */
																								BgL_objectz00_bglt
																									BgL_arg1807z00_5570;
																								BgL_arg1807z00_5570 =
																									(BgL_objectz00_bglt)
																									(BgL_creatorz00_5564);
																								if (BGL_CONDEXPAND_ISA_ARCH64())
																									{	/* SawBbv/bbv-debug.scm 377 */
																										long BgL_idxz00_5571;

																										BgL_idxz00_5571 =
																											BGL_OBJECT_INHERITANCE_NUM
																											(BgL_arg1807z00_5570);
																										BgL_test3360z00_7267 =
																											(VECTOR_REF
																											(BGl_za2inheritancesza2z00zz__objectz00,
																												(BgL_idxz00_5571 +
																													1L)) ==
																											BgL_classz00_5569);
																									}
																								else
																									{	/* SawBbv/bbv-debug.scm 377 */
																										bool_t BgL_res2982z00_5574;

																										{	/* SawBbv/bbv-debug.scm 377 */
																											obj_t BgL_oclassz00_5575;

																											{	/* SawBbv/bbv-debug.scm 377 */
																												obj_t
																													BgL_arg1815z00_5576;
																												long
																													BgL_arg1816z00_5577;
																												BgL_arg1815z00_5576 =
																													(BGl_za2classesza2z00zz__objectz00);
																												{	/* SawBbv/bbv-debug.scm 377 */
																													long
																														BgL_arg1817z00_5578;
																													BgL_arg1817z00_5578 =
																														BGL_OBJECT_CLASS_NUM
																														(BgL_arg1807z00_5570);
																													BgL_arg1816z00_5577 =
																														(BgL_arg1817z00_5578
																														- OBJECT_TYPE);
																												}
																												BgL_oclassz00_5575 =
																													VECTOR_REF
																													(BgL_arg1815z00_5576,
																													BgL_arg1816z00_5577);
																											}
																											{	/* SawBbv/bbv-debug.scm 377 */
																												bool_t
																													BgL__ortest_1115z00_5579;
																												BgL__ortest_1115z00_5579
																													=
																													(BgL_classz00_5569 ==
																													BgL_oclassz00_5575);
																												if (BgL__ortest_1115z00_5579)
																													{	/* SawBbv/bbv-debug.scm 377 */
																														BgL_res2982z00_5574
																															=
																															BgL__ortest_1115z00_5579;
																													}
																												else
																													{	/* SawBbv/bbv-debug.scm 377 */
																														long
																															BgL_odepthz00_5580;
																														{	/* SawBbv/bbv-debug.scm 377 */
																															obj_t
																																BgL_arg1804z00_5581;
																															BgL_arg1804z00_5581
																																=
																																(BgL_oclassz00_5575);
																															BgL_odepthz00_5580
																																=
																																BGL_CLASS_DEPTH
																																(BgL_arg1804z00_5581);
																														}
																														if (
																															(1L <
																																BgL_odepthz00_5580))
																															{	/* SawBbv/bbv-debug.scm 377 */
																																obj_t
																																	BgL_arg1802z00_5582;
																																{	/* SawBbv/bbv-debug.scm 377 */
																																	obj_t
																																		BgL_arg1803z00_5583;
																																	BgL_arg1803z00_5583
																																		=
																																		(BgL_oclassz00_5575);
																																	BgL_arg1802z00_5582
																																		=
																																		BGL_CLASS_ANCESTORS_REF
																																		(BgL_arg1803z00_5583,
																																		1L);
																																}
																																BgL_res2982z00_5574
																																	=
																																	(BgL_arg1802z00_5582
																																	==
																																	BgL_classz00_5569);
																															}
																														else
																															{	/* SawBbv/bbv-debug.scm 377 */
																																BgL_res2982z00_5574
																																	=
																																	((bool_t) 0);
																															}
																													}
																											}
																										}
																										BgL_test3360z00_7267 =
																											BgL_res2982z00_5574;
																									}
																							}
																						else
																							{	/* SawBbv/bbv-debug.scm 377 */
																								BgL_test3360z00_7267 =
																									((bool_t) 0);
																							}
																					}
																					if (BgL_test3360z00_7267)
																						{	/* SawBbv/bbv-debug.scm 378 */
																							int BgL_arg2320z00_5584;

																							BgL_arg2320z00_5584 =
																								(((BgL_blockz00_bglt) COBJECT(
																										((BgL_blockz00_bglt)
																											BgL_creatorz00_5564)))->
																								BgL_labelz00);
																							{	/* SawBbv/bbv-debug.scm 378 */
																								obj_t BgL_list2321z00_5585;

																								BgL_list2321z00_5585 =
																									MAKE_YOUNG_PAIR(BINT
																									(BgL_arg2320z00_5584), BNIL);
																								BgL_arg2350z00_5525 =
																									BGl_formatz00zz__r4_output_6_10_3z00
																									(BGl_string3058z00zzsaw_bbvzd2debugzd2,
																									BgL_list2321z00_5585);
																						}}
																					else
																						{	/* SawBbv/bbv-debug.scm 380 */
																							obj_t BgL_list2322z00_5586;

																							BgL_list2322z00_5586 =
																								MAKE_YOUNG_PAIR
																								(BgL_creatorz00_5564, BNIL);
																							BgL_arg2350z00_5525 =
																								BGl_formatz00zz__r4_output_6_10_3z00
																								(BGl_string3058z00zzsaw_bbvzd2debugzd2,
																								BgL_list2322z00_5586);
																						}
																				}
																		}
																		BgL_arg2349z00_5524 =
																			BGl_paddingrze70ze7zzsaw_bbvzd2debugzd2
																			(BgL_marginsz00_5272, 12L,
																			BgL_arg2350z00_5525);
																	}
																	bgl_display_obj(BgL_arg2349z00_5524,
																		BgL_port1606z00_5517);
																}
																bgl_display_string
																	(BGl_string3051z00zzsaw_bbvzd2debugzd2,
																	BgL_port1606z00_5517);
																{	/* SawBbv/bbv-debug.scm 394 */
																	obj_t BgL_arg2353z00_5528;

																	{	/* SawBbv/bbv-debug.scm 394 */
																		obj_t BgL_arg2354z00_5529;

																		{	/* SawBbv/bbv-debug.scm 394 */
																			obj_t BgL_arg2355z00_5530;
																			obj_t BgL_arg2356z00_5531;

																			{
																				BgL_blocksz00_bglt BgL_auxz00_7300;

																				{
																					obj_t BgL_auxz00_7301;

																					{	/* SawBbv/bbv-debug.scm 394 */
																						BgL_objectz00_bglt BgL_tmpz00_7302;

																						BgL_tmpz00_7302 =
																							((BgL_objectz00_bglt)
																							BgL_bz00_5516);
																						BgL_auxz00_7301 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_7302);
																					}
																					BgL_auxz00_7300 =
																						((BgL_blocksz00_bglt)
																						BgL_auxz00_7301);
																				}
																				BgL_arg2355z00_5530 =
																					(((BgL_blocksz00_bglt)
																						COBJECT(BgL_auxz00_7300))->
																					BgL_mblockz00);
																			}
																			{
																				BgL_blocksz00_bglt BgL_auxz00_7307;

																				{
																					obj_t BgL_auxz00_7308;

																					{	/* SawBbv/bbv-debug.scm 394 */
																						BgL_objectz00_bglt BgL_tmpz00_7309;

																						BgL_tmpz00_7309 =
																							((BgL_objectz00_bglt)
																							BgL_bz00_5516);
																						BgL_auxz00_7308 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_7309);
																					}
																					BgL_auxz00_7307 =
																						((BgL_blocksz00_bglt)
																						BgL_auxz00_7308);
																				}
																				BgL_arg2356z00_5531 =
																					(((BgL_blocksz00_bglt)
																						COBJECT(BgL_auxz00_7307))->
																					BgL_mergesz00);
																			}
																			BgL_mblockz00_5547 = BgL_arg2355z00_5530;
																			BgL_mergesz00_5548 = BgL_arg2356z00_5531;
																			{	/* SawBbv/bbv-debug.scm 384 */
																				obj_t BgL_arg2324z00_5549;

																				if (NULLP(BgL_mergesz00_5548))
																					{	/* SawBbv/bbv-debug.scm 384 */
																						BgL_arg2324z00_5549 = BNIL;
																					}
																				else
																					{	/* SawBbv/bbv-debug.scm 384 */
																						obj_t BgL_head1603z00_5550;

																						BgL_head1603z00_5550 =
																							MAKE_YOUNG_PAIR(BNIL, BNIL);
																						{
																							obj_t BgL_l1601z00_5552;
																							obj_t BgL_tail1604z00_5553;

																							BgL_l1601z00_5552 =
																								BgL_mergesz00_5548;
																							BgL_tail1604z00_5553 =
																								BgL_head1603z00_5550;
																						BgL_zc3z04anonymousza32327ze3z87_5551:
																							if (NULLP
																								(BgL_l1601z00_5552))
																								{	/* SawBbv/bbv-debug.scm 384 */
																									BgL_arg2324z00_5549 =
																										CDR(BgL_head1603z00_5550);
																								}
																							else
																								{	/* SawBbv/bbv-debug.scm 384 */
																									obj_t BgL_newtail1605z00_5554;

																									{	/* SawBbv/bbv-debug.scm 384 */
																										obj_t BgL_arg2331z00_5555;

																										{	/* SawBbv/bbv-debug.scm 384 */
																											obj_t BgL_mz00_5556;

																											BgL_mz00_5556 =
																												CAR(
																												((obj_t)
																													BgL_l1601z00_5552));
																											{	/* SawBbv/bbv-debug.scm 385 */
																												int BgL_arg2333z00_5557;
																												int BgL_arg2335z00_5558;

																												BgL_arg2333z00_5557 =
																													(((BgL_blockz00_bglt)
																														COBJECT((
																																(BgL_blockz00_bglt)
																																CAR(((obj_t)
																																		BgL_mz00_5556)))))->
																													BgL_labelz00);
																												BgL_arg2335z00_5558 =
																													(((BgL_blockz00_bglt)
																														COBJECT((
																																(BgL_blockz00_bglt)
																																CDR(((obj_t)
																																		BgL_mz00_5556)))))->
																													BgL_labelz00);
																												{	/* SawBbv/bbv-debug.scm 385 */
																													obj_t
																														BgL_list2336z00_5559;
																													{	/* SawBbv/bbv-debug.scm 385 */
																														obj_t
																															BgL_arg2337z00_5560;
																														BgL_arg2337z00_5560
																															=
																															MAKE_YOUNG_PAIR
																															(BINT
																															(BgL_arg2335z00_5558),
																															BNIL);
																														BgL_list2336z00_5559
																															=
																															MAKE_YOUNG_PAIR
																															(BINT
																															(BgL_arg2333z00_5557),
																															BgL_arg2337z00_5560);
																													}
																													BgL_arg2331z00_5555 =
																														BGl_formatz00zz__r4_output_6_10_3z00
																														(BGl_string3056z00zzsaw_bbvzd2debugzd2,
																														BgL_list2336z00_5559);
																										}}}
																										BgL_newtail1605z00_5554 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2331z00_5555,
																											BNIL);
																									}
																									SET_CDR(BgL_tail1604z00_5553,
																										BgL_newtail1605z00_5554);
																									{	/* SawBbv/bbv-debug.scm 384 */
																										obj_t BgL_arg2330z00_5561;

																										BgL_arg2330z00_5561 =
																											CDR(
																											((obj_t)
																												BgL_l1601z00_5552));
																										{
																											obj_t
																												BgL_tail1604z00_7340;
																											obj_t BgL_l1601z00_7339;

																											BgL_l1601z00_7339 =
																												BgL_arg2330z00_5561;
																											BgL_tail1604z00_7340 =
																												BgL_newtail1605z00_5554;
																											BgL_tail1604z00_5553 =
																												BgL_tail1604z00_7340;
																											BgL_l1601z00_5552 =
																												BgL_l1601z00_7339;
																											goto
																												BgL_zc3z04anonymousza32327ze3z87_5551;
																										}
																									}
																								}
																						}
																					}
																				{	/* SawBbv/bbv-debug.scm 383 */
																					obj_t BgL_list2325z00_5562;

																					BgL_list2325z00_5562 =
																						MAKE_YOUNG_PAIR(BgL_arg2324z00_5549,
																						BNIL);
																					BgL_arg2354z00_5529 =
																						BGl_formatz00zz__r4_output_6_10_3z00
																						(BGl_string3034z00zzsaw_bbvzd2debugzd2,
																						BgL_list2325z00_5562);
																				}
																			}
																		}
																		BgL_arg2353z00_5528 =
																			BGl_paddinglze70ze7zzsaw_bbvzd2debugzd2
																			(BgL_marginsz00_5272, 30L,
																			BgL_arg2354z00_5529);
																	}
																	bgl_display_obj(BgL_arg2353z00_5528,
																		BgL_port1606z00_5517);
																}
																bgl_display_string
																	(BGl_string3051z00zzsaw_bbvzd2debugzd2,
																	BgL_port1606z00_5517);
																{	/* SawBbv/bbv-debug.scm 395 */
																	obj_t BgL_arg2357z00_5532;

																	{	/* SawBbv/bbv-debug.scm 395 */
																		BgL_bbvzd2ctxzd2_bglt BgL_arg2358z00_5533;

																		{
																			BgL_blocksz00_bglt BgL_auxz00_7346;

																			{
																				obj_t BgL_auxz00_7347;

																				{	/* SawBbv/bbv-debug.scm 395 */
																					BgL_objectz00_bglt BgL_tmpz00_7348;

																					BgL_tmpz00_7348 =
																						((BgL_objectz00_bglt)
																						BgL_bz00_5516);
																					BgL_auxz00_7347 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_7348);
																				}
																				BgL_auxz00_7346 =
																					((BgL_blocksz00_bglt)
																					BgL_auxz00_7347);
																			}
																			BgL_arg2358z00_5533 =
																				(((BgL_blocksz00_bglt)
																					COBJECT(BgL_auxz00_7346))->
																				BgL_ctxz00);
																		}
																		BgL_ctxz00_5534 = BgL_arg2358z00_5533;
																		{	/* SawBbv/bbv-debug.scm 369 */
																			obj_t BgL_arg2301z00_5535;

																			{	/* SawBbv/bbv-debug.scm 369 */
																				obj_t BgL_l1595z00_5536;

																				BgL_l1595z00_5536 =
																					(((BgL_bbvzd2ctxzd2_bglt)
																						COBJECT(BgL_ctxz00_5534))->
																					BgL_entriesz00);
																				if (NULLP(BgL_l1595z00_5536))
																					{	/* SawBbv/bbv-debug.scm 369 */
																						BgL_arg2301z00_5535 = BNIL;
																					}
																				else
																					{	/* SawBbv/bbv-debug.scm 369 */
																						obj_t BgL_head1597z00_5537;

																						BgL_head1597z00_5537 =
																							MAKE_YOUNG_PAIR
																							(BGl_logzd2entryze70z35zzsaw_bbvzd2debugzd2
																							(CAR(BgL_l1595z00_5536)), BNIL);
																						{	/* SawBbv/bbv-debug.scm 369 */
																							obj_t BgL_g1600z00_5538;

																							BgL_g1600z00_5538 =
																								CDR(BgL_l1595z00_5536);
																							{
																								obj_t BgL_l1595z00_5540;
																								obj_t BgL_tail1598z00_5541;

																								BgL_l1595z00_5540 =
																									BgL_g1600z00_5538;
																								BgL_tail1598z00_5541 =
																									BgL_head1597z00_5537;
																							BgL_zc3z04anonymousza32304ze3z87_5539:
																								if (NULLP
																									(BgL_l1595z00_5540))
																									{	/* SawBbv/bbv-debug.scm 369 */
																										BgL_arg2301z00_5535 =
																											BgL_head1597z00_5537;
																									}
																								else
																									{	/* SawBbv/bbv-debug.scm 369 */
																										obj_t
																											BgL_newtail1599z00_5542;
																										{	/* SawBbv/bbv-debug.scm 369 */
																											obj_t BgL_arg2307z00_5543;

																											{	/* SawBbv/bbv-debug.scm 369 */
																												obj_t
																													BgL_arg2308z00_5544;
																												BgL_arg2308z00_5544 =
																													CAR(((obj_t)
																														BgL_l1595z00_5540));
																												BgL_arg2307z00_5543 =
																													BGl_logzd2entryze70z35zzsaw_bbvzd2debugzd2
																													(BgL_arg2308z00_5544);
																											}
																											BgL_newtail1599z00_5542 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2307z00_5543,
																												BNIL);
																										}
																										SET_CDR
																											(BgL_tail1598z00_5541,
																											BgL_newtail1599z00_5542);
																										{	/* SawBbv/bbv-debug.scm 369 */
																											obj_t BgL_arg2306z00_5545;

																											BgL_arg2306z00_5545 =
																												CDR(
																												((obj_t)
																													BgL_l1595z00_5540));
																											{
																												obj_t
																													BgL_tail1598z00_7370;
																												obj_t BgL_l1595z00_7369;

																												BgL_l1595z00_7369 =
																													BgL_arg2306z00_5545;
																												BgL_tail1598z00_7370 =
																													BgL_newtail1599z00_5542;
																												BgL_tail1598z00_5541 =
																													BgL_tail1598z00_7370;
																												BgL_l1595z00_5540 =
																													BgL_l1595z00_7369;
																												goto
																													BgL_zc3z04anonymousza32304ze3z87_5539;
																											}
																										}
																									}
																							}
																						}
																					}
																			}
																			{	/* SawBbv/bbv-debug.scm 369 */
																				obj_t BgL_list2302z00_5546;

																				BgL_list2302z00_5546 =
																					MAKE_YOUNG_PAIR(BgL_arg2301z00_5535,
																					BNIL);
																				BgL_arg2357z00_5532 =
																					BGl_formatz00zz__r4_output_6_10_3z00
																					(BGl_string3055z00zzsaw_bbvzd2debugzd2,
																					BgL_list2302z00_5546);
																			}
																		}
																	}
																	bgl_display_obj(BgL_arg2357z00_5532,
																		BgL_port1606z00_5517);
																}
																bgl_display_char(((unsigned char) 10),
																	BgL_port1606z00_5517);
														}}
														{
															obj_t BgL_l1609z00_7376;

															BgL_l1609z00_7376 = CDR(BgL_l1609z00_5514);
															BgL_l1609z00_5514 = BgL_l1609z00_7376;
															goto BgL_zc3z04anonymousza32362ze3z87_5513;
														}
													}
												else
													{	/* SawBbv/bbv-debug.scm 401 */
														((bool_t) 1);
													}
											}
										}
									}
									{
										obj_t BgL_l1619z00_7379;

										BgL_l1619z00_7379 = CDR(BgL_l1619z00_5603);
										BgL_l1619z00_5603 = BgL_l1619z00_7379;
										goto BgL_zc3z04anonymousza32201ze3z87_5602;
									}
								}
							else
								{	/* SawBbv/bbv-debug.scm 407 */
									((bool_t) 1);
								}
						}
					}
					{	/* SawBbv/bbv-debug.scm 408 */
						obj_t BgL_arg2205z00_5605;

						{	/* SawBbv/bbv-debug.scm 408 */
							obj_t BgL_tmpz00_7381;

							BgL_tmpz00_7381 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg2205z00_5605 =
								BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_7381);
						}
						return bgl_display_char(((unsigned char) 10), BgL_arg2205z00_5605);
		}}}}

	}



/* log-entry~0 */
	obj_t BGl_logzd2entryze70z35zzsaw_bbvzd2debugzd2(obj_t BgL_ez00_2915)
	{
		{	/* SawBbv/bbv-debug.scm 364 */
			if (NULLP(
					(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
								((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2915)))->
						BgL_aliasesz00)))
				{	/* SawBbv/bbv-debug.scm 360 */
					obj_t BgL_arg2267z00_2984;
					obj_t BgL_arg2268z00_2985;
					obj_t BgL_arg2269z00_2986;

					{	/* SawBbv/bbv-debug.scm 360 */
						BgL_rtl_regz00_bglt BgL_arg2273z00_2990;

						BgL_arg2273z00_2990 =
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2915)))->BgL_regz00);
						BgL_arg2267z00_2984 =
							BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2273z00_2990));
					}
					if (
						(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2915)))->
							BgL_polarityz00))
						{	/* SawBbv/bbv-debug.scm 362 */
							obj_t BgL_l1584z00_2992;

							BgL_l1584z00_2992 =
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2915)))->
								BgL_typesz00);
							{	/* SawBbv/bbv-debug.scm 362 */
								obj_t BgL_head1586z00_2994;

								BgL_head1586z00_2994 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1584z00_2996;
									obj_t BgL_tail1587z00_2997;

									BgL_l1584z00_2996 = BgL_l1584z00_2992;
									BgL_tail1587z00_2997 = BgL_head1586z00_2994;
								BgL_zc3z04anonymousza32276ze3z87_2998:
									if (NULLP(BgL_l1584z00_2996))
										{	/* SawBbv/bbv-debug.scm 362 */
											BgL_arg2268z00_2985 = CDR(BgL_head1586z00_2994);
										}
									else
										{	/* SawBbv/bbv-debug.scm 362 */
											obj_t BgL_newtail1588z00_3000;

											{	/* SawBbv/bbv-debug.scm 362 */
												obj_t BgL_arg2280z00_3002;

												{	/* SawBbv/bbv-debug.scm 362 */
													obj_t BgL_tz00_3003;

													BgL_tz00_3003 = CAR(((obj_t) BgL_l1584z00_2996));
													{	/* SawBbv/bbv-debug.scm 362 */
														obj_t BgL_arg2281z00_3004;
														long BgL_arg2282z00_3005;

														BgL_arg2281z00_3004 =
															BGl_shapez00zztools_shapez00(BgL_tz00_3003);
														BgL_arg2282z00_3005 =
															(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
																	((BgL_bbvzd2ctxentryzd2_bglt)
																		BgL_ez00_2915)))->BgL_countz00);
														{	/* SawBbv/bbv-debug.scm 362 */
															obj_t BgL_list2283z00_3006;

															{	/* SawBbv/bbv-debug.scm 362 */
																obj_t BgL_arg2284z00_3007;

																BgL_arg2284z00_3007 =
																	MAKE_YOUNG_PAIR(BINT(BgL_arg2282z00_3005),
																	BNIL);
																BgL_list2283z00_3006 =
																	MAKE_YOUNG_PAIR(BgL_arg2281z00_3004,
																	BgL_arg2284z00_3007);
															}
															BgL_arg2280z00_3002 =
																BGl_formatz00zz__r4_output_6_10_3z00
																(BGl_string3024z00zzsaw_bbvzd2debugzd2,
																BgL_list2283z00_3006);
												}}}
												BgL_newtail1588z00_3000 =
													MAKE_YOUNG_PAIR(BgL_arg2280z00_3002, BNIL);
											}
											SET_CDR(BgL_tail1587z00_2997, BgL_newtail1588z00_3000);
											{	/* SawBbv/bbv-debug.scm 362 */
												obj_t BgL_arg2279z00_3001;

												BgL_arg2279z00_3001 = CDR(((obj_t) BgL_l1584z00_2996));
												{
													obj_t BgL_tail1587z00_7416;
													obj_t BgL_l1584z00_7415;

													BgL_l1584z00_7415 = BgL_arg2279z00_3001;
													BgL_tail1587z00_7416 = BgL_newtail1588z00_3000;
													BgL_tail1587z00_2997 = BgL_tail1587z00_7416;
													BgL_l1584z00_2996 = BgL_l1584z00_7415;
													goto BgL_zc3z04anonymousza32276ze3z87_2998;
												}
											}
										}
								}
							}
						}
					else
						{	/* SawBbv/bbv-debug.scm 363 */
							obj_t BgL_l1590z00_3009;

							BgL_l1590z00_3009 =
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2915)))->
								BgL_typesz00);
							{	/* SawBbv/bbv-debug.scm 363 */
								obj_t BgL_head1592z00_3011;

								BgL_head1592z00_3011 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1590z00_3013;
									obj_t BgL_tail1593z00_3014;

									BgL_l1590z00_3013 = BgL_l1590z00_3009;
									BgL_tail1593z00_3014 = BgL_head1592z00_3011;
								BgL_zc3z04anonymousza32286ze3z87_3015:
									if (NULLP(BgL_l1590z00_3013))
										{	/* SawBbv/bbv-debug.scm 363 */
											BgL_arg2268z00_2985 = CDR(BgL_head1592z00_3011);
										}
									else
										{	/* SawBbv/bbv-debug.scm 363 */
											obj_t BgL_newtail1594z00_3017;

											{	/* SawBbv/bbv-debug.scm 363 */
												obj_t BgL_arg2289z00_3019;

												{	/* SawBbv/bbv-debug.scm 363 */
													obj_t BgL_tz00_3020;

													BgL_tz00_3020 = CAR(((obj_t) BgL_l1590z00_3013));
													{	/* SawBbv/bbv-debug.scm 363 */
														obj_t BgL_arg2290z00_3021;

														BgL_arg2290z00_3021 =
															BGl_shapez00zztools_shapez00(BgL_tz00_3020);
														{	/* SawBbv/bbv-debug.scm 363 */
															obj_t BgL_list2291z00_3022;

															BgL_list2291z00_3022 =
																MAKE_YOUNG_PAIR(BgL_arg2290z00_3021, BNIL);
															BgL_arg2289z00_3019 =
																BGl_formatz00zz__r4_output_6_10_3z00
																(BGl_string3025z00zzsaw_bbvzd2debugzd2,
																BgL_list2291z00_3022);
														}
													}
												}
												BgL_newtail1594z00_3017 =
													MAKE_YOUNG_PAIR(BgL_arg2289z00_3019, BNIL);
											}
											SET_CDR(BgL_tail1593z00_3014, BgL_newtail1594z00_3017);
											{	/* SawBbv/bbv-debug.scm 363 */
												obj_t BgL_arg2288z00_3018;

												BgL_arg2288z00_3018 = CDR(((obj_t) BgL_l1590z00_3013));
												{
													obj_t BgL_tail1593z00_7433;
													obj_t BgL_l1590z00_7432;

													BgL_l1590z00_7432 = BgL_arg2288z00_3018;
													BgL_tail1593z00_7433 = BgL_newtail1594z00_3017;
													BgL_tail1593z00_3014 = BgL_tail1593z00_7433;
													BgL_l1590z00_3013 = BgL_l1590z00_7432;
													goto BgL_zc3z04anonymousza32286ze3z87_3015;
												}
											}
										}
								}
							}
						}
					if (
						((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2915)))->
								BgL_valuez00) == CNST_TABLE_REF(2)))
						{	/* SawBbv/bbv-debug.scm 364 */
							BgL_arg2269z00_2986 = BGl_string3060z00zzsaw_bbvzd2debugzd2;
						}
					else
						{	/* SawBbv/bbv-debug.scm 364 */
							obj_t BgL_arg2294z00_3026;

							{	/* SawBbv/bbv-debug.scm 364 */
								obj_t BgL_arg2296z00_3028;

								BgL_arg2296z00_3028 =
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
											((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2915)))->
									BgL_valuez00);
								BgL_arg2294z00_3026 =
									BGl_shapez00zztools_shapez00(BgL_arg2296z00_3028);
							}
							{	/* SawBbv/bbv-debug.scm 364 */
								obj_t BgL_list2295z00_3027;

								BgL_list2295z00_3027 =
									MAKE_YOUNG_PAIR(BgL_arg2294z00_3026, BNIL);
								BgL_arg2269z00_2986 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string3061z00zzsaw_bbvzd2debugzd2, BgL_list2295z00_3027);
							}
						}
					{	/* SawBbv/bbv-debug.scm 359 */
						obj_t BgL_list2270z00_2987;

						{	/* SawBbv/bbv-debug.scm 359 */
							obj_t BgL_arg2271z00_2988;

							{	/* SawBbv/bbv-debug.scm 359 */
								obj_t BgL_arg2272z00_2989;

								BgL_arg2272z00_2989 =
									MAKE_YOUNG_PAIR(BgL_arg2269z00_2986, BNIL);
								BgL_arg2271z00_2988 =
									MAKE_YOUNG_PAIR(BgL_arg2268z00_2985, BgL_arg2272z00_2989);
							}
							BgL_list2270z00_2987 =
								MAKE_YOUNG_PAIR(BgL_arg2267z00_2984, BgL_arg2271z00_2988);
						}
						return
							BGl_formatz00zz__r4_output_6_10_3z00
							(BGl_string3062z00zzsaw_bbvzd2debugzd2, BgL_list2270z00_2987);
					}
				}
			else
				{	/* SawBbv/bbv-debug.scm 353 */
					obj_t BgL_arg2227z00_2920;
					obj_t BgL_arg2228z00_2921;
					obj_t BgL_arg2229z00_2922;
					obj_t BgL_arg2230z00_2923;

					{	/* SawBbv/bbv-debug.scm 353 */
						BgL_rtl_regz00_bglt BgL_arg2235z00_2928;

						BgL_arg2235z00_2928 =
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2915)))->BgL_regz00);
						BgL_arg2227z00_2920 =
							BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2235z00_2928));
					}
					if (
						(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2915)))->
							BgL_polarityz00))
						{	/* SawBbv/bbv-debug.scm 355 */
							obj_t BgL_l1568z00_2930;

							BgL_l1568z00_2930 =
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2915)))->
								BgL_typesz00);
							{	/* SawBbv/bbv-debug.scm 355 */
								obj_t BgL_head1570z00_2932;

								BgL_head1570z00_2932 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1568z00_2934;
									obj_t BgL_tail1571z00_2935;

									BgL_l1568z00_2934 = BgL_l1568z00_2930;
									BgL_tail1571z00_2935 = BgL_head1570z00_2932;
								BgL_zc3z04anonymousza32238ze3z87_2936:
									if (NULLP(BgL_l1568z00_2934))
										{	/* SawBbv/bbv-debug.scm 355 */
											BgL_arg2228z00_2921 = CDR(BgL_head1570z00_2932);
										}
									else
										{	/* SawBbv/bbv-debug.scm 355 */
											obj_t BgL_newtail1572z00_2938;

											{	/* SawBbv/bbv-debug.scm 355 */
												obj_t BgL_arg2241z00_2940;

												{	/* SawBbv/bbv-debug.scm 355 */
													obj_t BgL_tz00_2941;

													BgL_tz00_2941 = CAR(((obj_t) BgL_l1568z00_2934));
													{	/* SawBbv/bbv-debug.scm 355 */
														obj_t BgL_arg2242z00_2942;
														long BgL_arg2243z00_2943;

														BgL_arg2242z00_2942 =
															BGl_shapez00zztools_shapez00(BgL_tz00_2941);
														BgL_arg2243z00_2943 =
															(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
																	((BgL_bbvzd2ctxentryzd2_bglt)
																		BgL_ez00_2915)))->BgL_countz00);
														{	/* SawBbv/bbv-debug.scm 355 */
															obj_t BgL_list2244z00_2944;

															{	/* SawBbv/bbv-debug.scm 355 */
																obj_t BgL_arg2245z00_2945;

																BgL_arg2245z00_2945 =
																	MAKE_YOUNG_PAIR(BINT(BgL_arg2243z00_2943),
																	BNIL);
																BgL_list2244z00_2944 =
																	MAKE_YOUNG_PAIR(BgL_arg2242z00_2942,
																	BgL_arg2245z00_2945);
															}
															BgL_arg2241z00_2940 =
																BGl_formatz00zz__r4_output_6_10_3z00
																(BGl_string3024z00zzsaw_bbvzd2debugzd2,
																BgL_list2244z00_2944);
												}}}
												BgL_newtail1572z00_2938 =
													MAKE_YOUNG_PAIR(BgL_arg2241z00_2940, BNIL);
											}
											SET_CDR(BgL_tail1571z00_2935, BgL_newtail1572z00_2938);
											{	/* SawBbv/bbv-debug.scm 355 */
												obj_t BgL_arg2240z00_2939;

												BgL_arg2240z00_2939 = CDR(((obj_t) BgL_l1568z00_2934));
												{
													obj_t BgL_tail1571z00_7475;
													obj_t BgL_l1568z00_7474;

													BgL_l1568z00_7474 = BgL_arg2240z00_2939;
													BgL_tail1571z00_7475 = BgL_newtail1572z00_2938;
													BgL_tail1571z00_2935 = BgL_tail1571z00_7475;
													BgL_l1568z00_2934 = BgL_l1568z00_7474;
													goto BgL_zc3z04anonymousza32238ze3z87_2936;
												}
											}
										}
								}
							}
						}
					else
						{	/* SawBbv/bbv-debug.scm 356 */
							obj_t BgL_l1573z00_2947;

							BgL_l1573z00_2947 =
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2915)))->
								BgL_typesz00);
							{	/* SawBbv/bbv-debug.scm 356 */
								obj_t BgL_head1575z00_2949;

								BgL_head1575z00_2949 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1573z00_2951;
									obj_t BgL_tail1576z00_2952;

									BgL_l1573z00_2951 = BgL_l1573z00_2947;
									BgL_tail1576z00_2952 = BgL_head1575z00_2949;
								BgL_zc3z04anonymousza32247ze3z87_2953:
									if (NULLP(BgL_l1573z00_2951))
										{	/* SawBbv/bbv-debug.scm 356 */
											BgL_arg2228z00_2921 = CDR(BgL_head1575z00_2949);
										}
									else
										{	/* SawBbv/bbv-debug.scm 356 */
											obj_t BgL_newtail1577z00_2955;

											{	/* SawBbv/bbv-debug.scm 356 */
												obj_t BgL_arg2250z00_2957;

												{	/* SawBbv/bbv-debug.scm 356 */
													obj_t BgL_tz00_2958;

													BgL_tz00_2958 = CAR(((obj_t) BgL_l1573z00_2951));
													{	/* SawBbv/bbv-debug.scm 356 */
														obj_t BgL_arg2251z00_2959;

														BgL_arg2251z00_2959 =
															BGl_shapez00zztools_shapez00(BgL_tz00_2958);
														{	/* SawBbv/bbv-debug.scm 356 */
															obj_t BgL_list2252z00_2960;

															BgL_list2252z00_2960 =
																MAKE_YOUNG_PAIR(BgL_arg2251z00_2959, BNIL);
															BgL_arg2250z00_2957 =
																BGl_formatz00zz__r4_output_6_10_3z00
																(BGl_string3025z00zzsaw_bbvzd2debugzd2,
																BgL_list2252z00_2960);
														}
													}
												}
												BgL_newtail1577z00_2955 =
													MAKE_YOUNG_PAIR(BgL_arg2250z00_2957, BNIL);
											}
											SET_CDR(BgL_tail1576z00_2952, BgL_newtail1577z00_2955);
											{	/* SawBbv/bbv-debug.scm 356 */
												obj_t BgL_arg2249z00_2956;

												BgL_arg2249z00_2956 = CDR(((obj_t) BgL_l1573z00_2951));
												{
													obj_t BgL_tail1576z00_7492;
													obj_t BgL_l1573z00_7491;

													BgL_l1573z00_7491 = BgL_arg2249z00_2956;
													BgL_tail1576z00_7492 = BgL_newtail1577z00_2955;
													BgL_tail1576z00_2952 = BgL_tail1576z00_7492;
													BgL_l1573z00_2951 = BgL_l1573z00_7491;
													goto BgL_zc3z04anonymousza32247ze3z87_2953;
												}
											}
										}
								}
							}
						}
					if (
						((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2915)))->
								BgL_valuez00) == CNST_TABLE_REF(2)))
						{	/* SawBbv/bbv-debug.scm 357 */
							BgL_arg2229z00_2922 = BGl_string3060z00zzsaw_bbvzd2debugzd2;
						}
					else
						{	/* SawBbv/bbv-debug.scm 357 */
							obj_t BgL_arg2255z00_2964;

							{	/* SawBbv/bbv-debug.scm 357 */
								obj_t BgL_arg2257z00_2966;

								BgL_arg2257z00_2966 =
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
											((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2915)))->
									BgL_valuez00);
								BgL_arg2255z00_2964 =
									BGl_shapez00zztools_shapez00(BgL_arg2257z00_2966);
							}
							{	/* SawBbv/bbv-debug.scm 357 */
								obj_t BgL_list2256z00_2965;

								BgL_list2256z00_2965 =
									MAKE_YOUNG_PAIR(BgL_arg2255z00_2964, BNIL);
								BgL_arg2229z00_2922 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string3061z00zzsaw_bbvzd2debugzd2, BgL_list2256z00_2965);
							}
						}
					{	/* SawBbv/bbv-debug.scm 358 */
						obj_t BgL_l1578z00_2968;

						BgL_l1578z00_2968 =
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2915)))->
							BgL_aliasesz00);
						if (NULLP(BgL_l1578z00_2968))
							{	/* SawBbv/bbv-debug.scm 358 */
								BgL_arg2230z00_2923 = BNIL;
							}
						else
							{	/* SawBbv/bbv-debug.scm 358 */
								obj_t BgL_head1580z00_2970;

								BgL_head1580z00_2970 =
									MAKE_YOUNG_PAIR(BGl_shapez00zztools_shapez00(CAR
										(BgL_l1578z00_2968)), BNIL);
								{	/* SawBbv/bbv-debug.scm 358 */
									obj_t BgL_g1583z00_2971;

									BgL_g1583z00_2971 = CDR(BgL_l1578z00_2968);
									{
										obj_t BgL_l1578z00_2973;
										obj_t BgL_tail1581z00_2974;

										BgL_l1578z00_2973 = BgL_g1583z00_2971;
										BgL_tail1581z00_2974 = BgL_head1580z00_2970;
									BgL_zc3z04anonymousza32260ze3z87_2975:
										if (NULLP(BgL_l1578z00_2973))
											{	/* SawBbv/bbv-debug.scm 358 */
												BgL_arg2230z00_2923 = BgL_head1580z00_2970;
											}
										else
											{	/* SawBbv/bbv-debug.scm 358 */
												obj_t BgL_newtail1582z00_2977;

												{	/* SawBbv/bbv-debug.scm 358 */
													obj_t BgL_arg2263z00_2979;

													{	/* SawBbv/bbv-debug.scm 358 */
														obj_t BgL_arg2264z00_2980;

														BgL_arg2264z00_2980 =
															CAR(((obj_t) BgL_l1578z00_2973));
														BgL_arg2263z00_2979 =
															BGl_shapez00zztools_shapez00(BgL_arg2264z00_2980);
													}
													BgL_newtail1582z00_2977 =
														MAKE_YOUNG_PAIR(BgL_arg2263z00_2979, BNIL);
												}
												SET_CDR(BgL_tail1581z00_2974, BgL_newtail1582z00_2977);
												{	/* SawBbv/bbv-debug.scm 358 */
													obj_t BgL_arg2262z00_2978;

													BgL_arg2262z00_2978 =
														CDR(((obj_t) BgL_l1578z00_2973));
													{
														obj_t BgL_tail1581z00_7521;
														obj_t BgL_l1578z00_7520;

														BgL_l1578z00_7520 = BgL_arg2262z00_2978;
														BgL_tail1581z00_7521 = BgL_newtail1582z00_2977;
														BgL_tail1581z00_2974 = BgL_tail1581z00_7521;
														BgL_l1578z00_2973 = BgL_l1578z00_7520;
														goto BgL_zc3z04anonymousza32260ze3z87_2975;
													}
												}
											}
									}
								}
							}
					}
					{	/* SawBbv/bbv-debug.scm 352 */
						obj_t BgL_list2231z00_2924;

						{	/* SawBbv/bbv-debug.scm 352 */
							obj_t BgL_arg2232z00_2925;

							{	/* SawBbv/bbv-debug.scm 352 */
								obj_t BgL_arg2233z00_2926;

								{	/* SawBbv/bbv-debug.scm 352 */
									obj_t BgL_arg2234z00_2927;

									BgL_arg2234z00_2927 =
										MAKE_YOUNG_PAIR(BgL_arg2230z00_2923, BNIL);
									BgL_arg2233z00_2926 =
										MAKE_YOUNG_PAIR(BgL_arg2229z00_2922, BgL_arg2234z00_2927);
								}
								BgL_arg2232z00_2925 =
									MAKE_YOUNG_PAIR(BgL_arg2228z00_2921, BgL_arg2233z00_2926);
							}
							BgL_list2231z00_2924 =
								MAKE_YOUNG_PAIR(BgL_arg2227z00_2920, BgL_arg2232z00_2925);
						}
						return
							BGl_formatz00zz__r4_output_6_10_3z00
							(BGl_string3063z00zzsaw_bbvzd2debugzd2, BgL_list2231z00_2924);
					}
				}
		}

	}



/* log-label~0 */
	obj_t BGl_logzd2labelze70z35zzsaw_bbvzd2debugzd2(obj_t BgL_blockz00_2883,
		obj_t BgL_prefixz00_2884)
	{
		{	/* SawBbv/bbv-debug.scm 331 */
			{	/* SawBbv/bbv-debug.scm 331 */
				obj_t BgL_dsssl2207z00_2886;

				BgL_dsssl2207z00_2886 = BgL_prefixz00_2884;
				{	/* SawBbv/bbv-debug.scm 329 */
					obj_t BgL_prefixz00_2887;

					{	/* SawBbv/bbv-debug.scm 329 */
						bool_t BgL_test3380z00_7527;

						if (NULLP(BgL_dsssl2207z00_2886))
							{	/* SawBbv/bbv-debug.scm 329 */
								BgL_test3380z00_7527 = ((bool_t) 1);
							}
						else
							{	/* SawBbv/bbv-debug.scm 329 */
								BgL_test3380z00_7527 =
									CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR
										(BgL_dsssl2207z00_2886), BNIL));
							}
						if (BgL_test3380z00_7527)
							{	/* SawBbv/bbv-debug.scm 329 */
								BgL_prefixz00_2887 = BFALSE;
							}
						else
							{	/* SawBbv/bbv-debug.scm 329 */
								obj_t BgL_tmp2208z00_2898;

								BgL_tmp2208z00_2898 = CAR(BgL_dsssl2207z00_2886);
								BgL_dsssl2207z00_2886 = CDR(BgL_dsssl2207z00_2886);
								BgL_prefixz00_2887 = BgL_tmp2208z00_2898;
							}
					}
					if (CBOOL(BgL_prefixz00_2887))
						{	/* SawBbv/bbv-debug.scm 330 */
							obj_t BgL_arg2209z00_2888;

							{	/* SawBbv/bbv-debug.scm 330 */

								BgL_arg2209z00_2888 =
									BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
									(long) (
										(((BgL_blockz00_bglt) COBJECT(
													((BgL_blockz00_bglt) BgL_blockz00_2883)))->
											BgL_labelz00)), 10L);
							}
							return string_append(BgL_prefixz00_2887, BgL_arg2209z00_2888);
						}
					else
						{	/* SawBbv/bbv-debug.scm 331 */

							return
								BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
								(long) (
									(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt) BgL_blockz00_2883)))->
										BgL_labelz00)), 10L);
		}}}}

	}



/* paddingr~0 */
	obj_t BGl_paddingrze70ze7zzsaw_bbvzd2debugzd2(obj_t BgL_marginsz00_5325,
		long BgL_nz00_2908, obj_t BgL_strz00_2909)
	{
		{	/* SawBbv/bbv-debug.scm 347 */
			if (CBOOL(BgL_strz00_2909))
				{	/* SawBbv/bbv-debug.scm 343 */
					long BgL_lenz00_2911;

					BgL_lenz00_2911 = STRING_LENGTH(((obj_t) BgL_strz00_2909));
					if ((BgL_lenz00_2911 < BgL_nz00_2908))
						{	/* SawBbv/bbv-debug.scm 345 */
							obj_t BgL_arg2222z00_2913;

							{	/* SawBbv/bbv-debug.scm 345 */
								long BgL_arg2223z00_2914;

								BgL_arg2223z00_2914 = (BgL_nz00_2908 - BgL_lenz00_2911);
								BgL_arg2222z00_2913 =
									VECTOR_REF(
									((obj_t) BgL_marginsz00_5325), BgL_arg2223z00_2914);
							}
							return string_append(BgL_strz00_2909, BgL_arg2222z00_2913);
						}
					else
						{	/* SawBbv/bbv-debug.scm 344 */
							return BgL_strz00_2909;
						}
				}
			else
				{	/* SawBbv/bbv-debug.scm 342 */
					return VECTOR_REF(((obj_t) BgL_marginsz00_5325), BgL_nz00_2908);
				}
		}

	}



/* paddingl~0 */
	obj_t BGl_paddinglze70ze7zzsaw_bbvzd2debugzd2(obj_t BgL_marginsz00_5326,
		long BgL_nz00_2901, obj_t BgL_strz00_2902)
	{
		{	/* SawBbv/bbv-debug.scm 339 */
			{	/* SawBbv/bbv-debug.scm 335 */
				long BgL_lenz00_2904;

				BgL_lenz00_2904 = STRING_LENGTH(BgL_strz00_2902);
				if ((BgL_lenz00_2904 < BgL_nz00_2901))
					{	/* SawBbv/bbv-debug.scm 337 */
						obj_t BgL_arg2218z00_2906;

						{	/* SawBbv/bbv-debug.scm 337 */
							long BgL_arg2219z00_2907;

							BgL_arg2219z00_2907 = (BgL_nz00_2901 - BgL_lenz00_2904);
							BgL_arg2218z00_2906 =
								VECTOR_REF(((obj_t) BgL_marginsz00_5326), BgL_arg2219z00_2907);
						}
						return string_append(BgL_arg2218z00_2906, BgL_strz00_2902);
					}
				else
					{	/* SawBbv/bbv-debug.scm 336 */
						return BgL_strz00_2902;
					}
			}
		}

	}



/* log-blocks-history */
	BGL_EXPORTED_DEF obj_t
		BGl_logzd2blockszd2historyz00zzsaw_bbvzd2debugzd2(BgL_globalz00_bglt
		BgL_globalz00_121, obj_t BgL_paramsz00_122, obj_t BgL_blocksz00_123)
	{
		{	/* SawBbv/bbv-debug.scm 413 */
			{	/* SawBbv/bbv-debug.scm 488 */
				obj_t BgL_zc3z04anonymousza32370ze3z87_5278;

				BgL_zc3z04anonymousza32370ze3z87_5278 =
					MAKE_FX_PROCEDURE
					(BGl_z62zc3z04anonymousza32370ze3ze5zzsaw_bbvzd2debugzd2, (int) (1L),
					(int) (2L));
				PROCEDURE_SET(BgL_zc3z04anonymousza32370ze3z87_5278, (int) (0L),
					((obj_t) BgL_globalz00_121));
				PROCEDURE_SET(BgL_zc3z04anonymousza32370ze3z87_5278, (int) (1L),
					BgL_blocksz00_123);
				return
					BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00
					(BgL_zc3z04anonymousza32370ze3z87_5278);
			}
		}

	}



/* &log-blocks-history */
	obj_t BGl_z62logzd2blockszd2historyz62zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5279, obj_t BgL_globalz00_5280, obj_t BgL_paramsz00_5281,
		obj_t BgL_blocksz00_5282)
	{
		{	/* SawBbv/bbv-debug.scm 413 */
			return
				BGl_logzd2blockszd2historyz00zzsaw_bbvzd2debugzd2(
				((BgL_globalz00_bglt) BgL_globalz00_5280), BgL_paramsz00_5281,
				BgL_blocksz00_5282);
		}

	}



/* &<@anonymous:2370> */
	obj_t BGl_z62zc3z04anonymousza32370ze3ze5zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5283, obj_t BgL_portz00_5286)
	{
		{	/* SawBbv/bbv-debug.scm 487 */
			{	/* SawBbv/bbv-debug.scm 488 */
				BgL_globalz00_bglt BgL_globalz00_5284;
				obj_t BgL_blocksz00_5285;

				BgL_globalz00_5284 =
					((BgL_globalz00_bglt) PROCEDURE_REF(BgL_envz00_5283, (int) (0L)));
				BgL_blocksz00_5285 =
					((obj_t) PROCEDURE_REF(BgL_envz00_5283, (int) (1L)));
				{
					BgL_blockz00_bglt BgL_bz00_5680;
					obj_t BgL_pz00_5681;
					long BgL_mz00_5682;
					BgL_blockz00_bglt BgL_bz00_5672;
					obj_t BgL_pz00_5673;
					long BgL_mz00_5674;
					BgL_blockz00_bglt BgL_bz00_5618;
					obj_t BgL_pz00_5619;
					long BgL_mz00_5620;
					BgL_blockz00_bglt BgL_bz00_5610;
					obj_t BgL_pz00_5611;
					long BgL_mz00_5612;

					{	/* SawBbv/bbv-debug.scm 488 */
						obj_t BgL_tmpz00_7582;

						BgL_tmpz00_7582 = ((obj_t) BgL_portz00_5286);
						bgl_display_string(BGl_string3073z00zzsaw_bbvzd2debugzd2,
							BgL_tmpz00_7582);
					}
					{	/* SawBbv/bbv-debug.scm 488 */
						obj_t BgL_tmpz00_7585;

						BgL_tmpz00_7585 = ((obj_t) BgL_portz00_5286);
						bgl_display_char(((unsigned char) 10), BgL_tmpz00_7585);
					}
					{	/* SawBbv/bbv-debug.scm 489 */
						obj_t BgL_g1650z00_5695;

						BgL_g1650z00_5695 =
							BGl_sortzd2blocksze70z35zzsaw_bbvzd2debugzd2(BgL_blocksz00_5285);
						{
							obj_t BgL_l1648z00_5697;

							BgL_l1648z00_5697 = BgL_g1650z00_5695;
						BgL_zc3z04anonymousza32371ze3z87_5696:
							if (PAIRP(BgL_l1648z00_5697))
								{	/* SawBbv/bbv-debug.scm 490 */
									{	/* SawBbv/bbv-debug.scm 489 */
										obj_t BgL_bz00_5698;

										BgL_bz00_5698 = CAR(BgL_l1648z00_5697);
										BgL_bz00_5610 = ((BgL_blockz00_bglt) BgL_bz00_5698);
										BgL_pz00_5611 = BgL_portz00_5286;
										BgL_mz00_5612 = 4L;
										{	/* SawBbv/bbv-debug.scm 478 */
											obj_t BgL_g1643z00_5613;

											{	/* SawBbv/bbv-debug.scm 479 */
												obj_t BgL_arg2457z00_5614;

												{
													BgL_blockvz00_bglt BgL_auxz00_7592;

													{
														obj_t BgL_auxz00_7593;

														{	/* SawBbv/bbv-debug.scm 479 */
															BgL_objectz00_bglt BgL_tmpz00_7594;

															BgL_tmpz00_7594 =
																((BgL_objectz00_bglt) BgL_bz00_5610);
															BgL_auxz00_7593 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_7594);
														}
														BgL_auxz00_7592 =
															((BgL_blockvz00_bglt) BgL_auxz00_7593);
													}
													BgL_arg2457z00_5614 =
														(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_7592))->
														BgL_versionsz00);
												}
												BgL_g1643z00_5613 =
													BGl_sortzd2blocksze70z35zzsaw_bbvzd2debugzd2
													(BgL_arg2457z00_5614);
											}
											{
												obj_t BgL_l1641z00_5616;

												BgL_l1641z00_5616 = BgL_g1643z00_5613;
											BgL_zc3z04anonymousza32454ze3z87_5615:
												if (PAIRP(BgL_l1641z00_5616))
													{	/* SawBbv/bbv-debug.scm 479 */
														{	/* SawBbv/bbv-debug.scm 478 */
															obj_t BgL_bz00_5617;

															BgL_bz00_5617 = CAR(BgL_l1641z00_5616);
															BgL_bz00_5618 =
																((BgL_blockz00_bglt) BgL_bz00_5617);
															BgL_pz00_5619 = BgL_pz00_5611;
															BgL_mz00_5620 = BgL_mz00_5612;
															BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_5619,
																(int) (BgL_mz00_5620));
															{	/* SawBbv/bbv-debug.scm 421 */
																obj_t BgL_tmpz00_7605;

																BgL_tmpz00_7605 = ((obj_t) BgL_pz00_5619);
																bgl_display_string
																	(BGl_string3026z00zzsaw_bbvzd2debugzd2,
																	BgL_tmpz00_7605);
															}
															{	/* SawBbv/bbv-debug.scm 421 */
																obj_t BgL_tmpz00_7608;

																BgL_tmpz00_7608 = ((obj_t) BgL_pz00_5619);
																bgl_display_char(((unsigned char) 10),
																	BgL_tmpz00_7608);
															}
															{	/* SawBbv/bbv-debug.scm 422 */
																long BgL_arg2382z00_5621;

																BgL_arg2382z00_5621 = (BgL_mz00_5620 + 2L);
																BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_5619,
																	(int) (BgL_arg2382z00_5621));
															}
															{	/* SawBbv/bbv-debug.scm 423 */
																obj_t BgL_tmpz00_7614;

																BgL_tmpz00_7614 = ((obj_t) BgL_pz00_5619);
																bgl_display_string
																	(BGl_string3027z00zzsaw_bbvzd2debugzd2,
																	BgL_tmpz00_7614);
															}
															{	/* SawBbv/bbv-debug.scm 423 */
																int BgL_arg2383z00_5622;

																BgL_arg2383z00_5622 =
																	(((BgL_blockz00_bglt) COBJECT(
																			((BgL_blockz00_bglt) BgL_bz00_5618)))->
																	BgL_labelz00);
																bgl_display_obj(BINT(BgL_arg2383z00_5622),
																	BgL_pz00_5619);
															}
															{	/* SawBbv/bbv-debug.scm 423 */
																obj_t BgL_tmpz00_7621;

																BgL_tmpz00_7621 = ((obj_t) BgL_pz00_5619);
																bgl_display_string
																	(BGl_string3002z00zzsaw_bbvzd2debugzd2,
																	BgL_tmpz00_7621);
															}
															{	/* SawBbv/bbv-debug.scm 423 */
																obj_t BgL_tmpz00_7624;

																BgL_tmpz00_7624 = ((obj_t) BgL_pz00_5619);
																bgl_display_char(((unsigned char) 10),
																	BgL_tmpz00_7624);
															}
															{	/* SawBbv/bbv-debug.scm 424 */
																long BgL_arg2384z00_5623;

																BgL_arg2384z00_5623 = (BgL_mz00_5620 + 2L);
																BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_5619,
																	(int) (BgL_arg2384z00_5623));
															}
															{	/* SawBbv/bbv-debug.scm 425 */
																obj_t BgL_tmpz00_7630;

																BgL_tmpz00_7630 = ((obj_t) BgL_pz00_5619);
																bgl_display_string
																	(BGl_string3028z00zzsaw_bbvzd2debugzd2,
																	BgL_tmpz00_7630);
															}
															{	/* SawBbv/bbv-debug.scm 425 */
																int BgL_arg2385z00_5624;

																{	/* SawBbv/bbv-debug.scm 425 */
																	BgL_blockz00_bglt BgL_arg2386z00_5625;

																	{
																		BgL_blocksz00_bglt BgL_auxz00_7633;

																		{
																			obj_t BgL_auxz00_7634;

																			{	/* SawBbv/bbv-debug.scm 425 */
																				BgL_objectz00_bglt BgL_tmpz00_7635;

																				BgL_tmpz00_7635 =
																					((BgL_objectz00_bglt) BgL_bz00_5618);
																				BgL_auxz00_7634 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_7635);
																			}
																			BgL_auxz00_7633 =
																				((BgL_blocksz00_bglt) BgL_auxz00_7634);
																		}
																		BgL_arg2386z00_5625 =
																			(((BgL_blocksz00_bglt)
																				COBJECT(BgL_auxz00_7633))->
																			BgL_parentz00);
																	}
																	BgL_arg2385z00_5624 =
																		(((BgL_blockz00_bglt) COBJECT(
																				((BgL_blockz00_bglt)
																					BgL_arg2386z00_5625)))->BgL_labelz00);
																}
																bgl_display_obj(BINT(BgL_arg2385z00_5624),
																	BgL_pz00_5619);
															}
															{	/* SawBbv/bbv-debug.scm 425 */
																obj_t BgL_tmpz00_7644;

																BgL_tmpz00_7644 = ((obj_t) BgL_pz00_5619);
																bgl_display_string
																	(BGl_string3002z00zzsaw_bbvzd2debugzd2,
																	BgL_tmpz00_7644);
															}
															{	/* SawBbv/bbv-debug.scm 425 */
																obj_t BgL_tmpz00_7647;

																BgL_tmpz00_7647 = ((obj_t) BgL_pz00_5619);
																bgl_display_char(((unsigned char) 10),
																	BgL_tmpz00_7647);
															}
															{	/* SawBbv/bbv-debug.scm 426 */
																long BgL_arg2387z00_5626;

																BgL_arg2387z00_5626 = (BgL_mz00_5620 + 2L);
																BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_5619,
																	(int) (BgL_arg2387z00_5626));
															}
															{	/* SawBbv/bbv-debug.scm 427 */
																obj_t BgL_arg2388z00_5627;

																BgL_arg2388z00_5627 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_globalz00_5284)))->BgL_idz00);
																{	/* SawBbv/bbv-debug.scm 427 */
																	obj_t BgL_list2389z00_5628;

																	BgL_list2389z00_5628 =
																		MAKE_YOUNG_PAIR(BgL_arg2388z00_5627, BNIL);
																	BGl_fprintfz00zz__r4_output_6_10_3z00
																		(BgL_pz00_5619,
																		BGl_string3029z00zzsaw_bbvzd2debugzd2,
																		BgL_list2389z00_5628);
															}}
															{	/* SawBbv/bbv-debug.scm 428 */
																long BgL_arg2390z00_5629;

																BgL_arg2390z00_5629 = (BgL_mz00_5620 + 2L);
																BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_5619,
																	(int) (BgL_arg2390z00_5629));
															}
															{	/* SawBbv/bbv-debug.scm 432 */
																obj_t BgL_arg2391z00_5630;

																{	/* SawBbv/bbv-debug.scm 432 */
																	obj_t BgL_zc3z04anonymousza32394ze3z87_5631;

																	BgL_zc3z04anonymousza32394ze3z87_5631 =
																		MAKE_FX_PROCEDURE
																		(BGl_z62zc3z04anonymousza32394ze3ze5zzsaw_bbvzd2debugzd2,
																		(int) (1L), (int) (1L));
																	PROCEDURE_SET
																		(BgL_zc3z04anonymousza32394ze3z87_5631,
																		(int) (0L), ((obj_t) BgL_bz00_5618));
																	BgL_arg2391z00_5630 =
																		BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00
																		(BgL_zc3z04anonymousza32394ze3z87_5631);
																}
																{	/* SawBbv/bbv-debug.scm 429 */
																	obj_t BgL_list2392z00_5632;

																	BgL_list2392z00_5632 =
																		MAKE_YOUNG_PAIR(BgL_arg2391z00_5630, BNIL);
																	BGl_fprintfz00zz__r4_output_6_10_3z00
																		(BgL_pz00_5619,
																		BGl_string3032z00zzsaw_bbvzd2debugzd2,
																		BgL_list2392z00_5632);
															}}
															{	/* SawBbv/bbv-debug.scm 437 */
																long BgL_arg2399z00_5633;

																BgL_arg2399z00_5633 = (BgL_mz00_5620 + 2L);
																BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_5619,
																	(int) (BgL_arg2399z00_5633));
															}
															{	/* SawBbv/bbv-debug.scm 439 */
																bool_t BgL_test3388z00_7672;

																{	/* SawBbv/bbv-debug.scm 439 */
																	obj_t BgL_arg2435z00_5634;

																	{
																		BgL_blocksz00_bglt BgL_auxz00_7673;

																		{
																			obj_t BgL_auxz00_7674;

																			{	/* SawBbv/bbv-debug.scm 439 */
																				BgL_objectz00_bglt BgL_tmpz00_7675;

																				BgL_tmpz00_7675 =
																					((BgL_objectz00_bglt) BgL_bz00_5618);
																				BgL_auxz00_7674 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_7675);
																			}
																			BgL_auxz00_7673 =
																				((BgL_blocksz00_bglt) BgL_auxz00_7674);
																		}
																		BgL_arg2435z00_5634 =
																			(((BgL_blocksz00_bglt)
																				COBJECT(BgL_auxz00_7673))->
																			BgL_creatorz00);
																	}
																	{	/* SawBbv/bbv-debug.scm 439 */
																		obj_t BgL_classz00_5635;

																		BgL_classz00_5635 =
																			BGl_blockz00zzsaw_defsz00;
																		if (BGL_OBJECTP(BgL_arg2435z00_5634))
																			{	/* SawBbv/bbv-debug.scm 439 */
																				BgL_objectz00_bglt BgL_arg1807z00_5636;

																				BgL_arg1807z00_5636 =
																					(BgL_objectz00_bglt)
																					(BgL_arg2435z00_5634);
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* SawBbv/bbv-debug.scm 439 */
																						long BgL_idxz00_5637;

																						BgL_idxz00_5637 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_5636);
																						BgL_test3388z00_7672 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_5637 + 1L)) ==
																							BgL_classz00_5635);
																					}
																				else
																					{	/* SawBbv/bbv-debug.scm 439 */
																						bool_t BgL_res2983z00_5640;

																						{	/* SawBbv/bbv-debug.scm 439 */
																							obj_t BgL_oclassz00_5641;

																							{	/* SawBbv/bbv-debug.scm 439 */
																								obj_t BgL_arg1815z00_5642;
																								long BgL_arg1816z00_5643;

																								BgL_arg1815z00_5642 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* SawBbv/bbv-debug.scm 439 */
																									long BgL_arg1817z00_5644;

																									BgL_arg1817z00_5644 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_5636);
																									BgL_arg1816z00_5643 =
																										(BgL_arg1817z00_5644 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_5641 =
																									VECTOR_REF
																									(BgL_arg1815z00_5642,
																									BgL_arg1816z00_5643);
																							}
																							{	/* SawBbv/bbv-debug.scm 439 */
																								bool_t BgL__ortest_1115z00_5645;

																								BgL__ortest_1115z00_5645 =
																									(BgL_classz00_5635 ==
																									BgL_oclassz00_5641);
																								if (BgL__ortest_1115z00_5645)
																									{	/* SawBbv/bbv-debug.scm 439 */
																										BgL_res2983z00_5640 =
																											BgL__ortest_1115z00_5645;
																									}
																								else
																									{	/* SawBbv/bbv-debug.scm 439 */
																										long BgL_odepthz00_5646;

																										{	/* SawBbv/bbv-debug.scm 439 */
																											obj_t BgL_arg1804z00_5647;

																											BgL_arg1804z00_5647 =
																												(BgL_oclassz00_5641);
																											BgL_odepthz00_5646 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_5647);
																										}
																										if (
																											(1L < BgL_odepthz00_5646))
																											{	/* SawBbv/bbv-debug.scm 439 */
																												obj_t
																													BgL_arg1802z00_5648;
																												{	/* SawBbv/bbv-debug.scm 439 */
																													obj_t
																														BgL_arg1803z00_5649;
																													BgL_arg1803z00_5649 =
																														(BgL_oclassz00_5641);
																													BgL_arg1802z00_5648 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_5649,
																														1L);
																												}
																												BgL_res2983z00_5640 =
																													(BgL_arg1802z00_5648
																													== BgL_classz00_5635);
																											}
																										else
																											{	/* SawBbv/bbv-debug.scm 439 */
																												BgL_res2983z00_5640 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test3388z00_7672 =
																							BgL_res2983z00_5640;
																					}
																			}
																		else
																			{	/* SawBbv/bbv-debug.scm 439 */
																				BgL_test3388z00_7672 = ((bool_t) 0);
																			}
																	}
																}
																if (BgL_test3388z00_7672)
																	{	/* SawBbv/bbv-debug.scm 439 */
																		{	/* SawBbv/bbv-debug.scm 440 */
																			obj_t BgL_tmpz00_7702;

																			BgL_tmpz00_7702 = ((obj_t) BgL_pz00_5619);
																			bgl_display_string
																				(BGl_string3064z00zzsaw_bbvzd2debugzd2,
																				BgL_tmpz00_7702);
																		}
																		{	/* SawBbv/bbv-debug.scm 440 */
																			obj_t BgL_tmpz00_7705;

																			BgL_tmpz00_7705 = ((obj_t) BgL_pz00_5619);
																			bgl_display_char(((unsigned char) 10),
																				BgL_tmpz00_7705);
																		}
																		{	/* SawBbv/bbv-debug.scm 441 */
																			long BgL_arg2403z00_5650;

																			BgL_arg2403z00_5650 =
																				(BgL_mz00_5620 + 2L);
																			BGl_dumpzd2marginzd2zzsaw_defsz00
																				(BgL_pz00_5619,
																				(int) (BgL_arg2403z00_5650));
																		}
																		{	/* SawBbv/bbv-debug.scm 442 */
																			int BgL_arg2404z00_5651;

																			{	/* SawBbv/bbv-debug.scm 442 */
																				obj_t BgL_arg2407z00_5652;

																				{
																					BgL_blocksz00_bglt BgL_auxz00_7711;

																					{
																						obj_t BgL_auxz00_7712;

																						{	/* SawBbv/bbv-debug.scm 442 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_7713;
																							BgL_tmpz00_7713 =
																								((BgL_objectz00_bglt)
																								BgL_bz00_5618);
																							BgL_auxz00_7712 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_7713);
																						}
																						BgL_auxz00_7711 =
																							((BgL_blocksz00_bglt)
																							BgL_auxz00_7712);
																					}
																					BgL_arg2407z00_5652 =
																						(((BgL_blocksz00_bglt)
																							COBJECT(BgL_auxz00_7711))->
																						BgL_creatorz00);
																				}
																				BgL_arg2404z00_5651 =
																					(((BgL_blockz00_bglt) COBJECT(
																							((BgL_blockz00_bglt)
																								BgL_arg2407z00_5652)))->
																					BgL_labelz00);
																			}
																			{	/* SawBbv/bbv-debug.scm 442 */
																				obj_t BgL_list2405z00_5653;

																				BgL_list2405z00_5653 =
																					MAKE_YOUNG_PAIR(BINT
																					(BgL_arg2404z00_5651), BNIL);
																				BGl_fprintfz00zz__r4_output_6_10_3z00
																					(BgL_pz00_5619,
																					BGl_string3065z00zzsaw_bbvzd2debugzd2,
																					BgL_list2405z00_5653);
																	}}}
																else
																	{	/* SawBbv/bbv-debug.scm 443 */
																		bool_t BgL_test3393z00_7723;

																		{	/* SawBbv/bbv-debug.scm 443 */
																			obj_t BgL_arg2434z00_5654;

																			{
																				BgL_blocksz00_bglt BgL_auxz00_7724;

																				{
																					obj_t BgL_auxz00_7725;

																					{	/* SawBbv/bbv-debug.scm 443 */
																						BgL_objectz00_bglt BgL_tmpz00_7726;

																						BgL_tmpz00_7726 =
																							((BgL_objectz00_bglt)
																							BgL_bz00_5618);
																						BgL_auxz00_7725 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_7726);
																					}
																					BgL_auxz00_7724 =
																						((BgL_blocksz00_bglt)
																						BgL_auxz00_7725);
																				}
																				BgL_arg2434z00_5654 =
																					(((BgL_blocksz00_bglt)
																						COBJECT(BgL_auxz00_7724))->
																					BgL_creatorz00);
																			}
																			BgL_test3393z00_7723 =
																				SYMBOLP(BgL_arg2434z00_5654);
																		}
																		if (BgL_test3393z00_7723)
																			{	/* SawBbv/bbv-debug.scm 443 */
																				{	/* SawBbv/bbv-debug.scm 444 */
																					obj_t BgL_tmpz00_7732;

																					BgL_tmpz00_7732 =
																						((obj_t) BgL_pz00_5619);
																					bgl_display_string
																						(BGl_string3064z00zzsaw_bbvzd2debugzd2,
																						BgL_tmpz00_7732);
																				}
																				{	/* SawBbv/bbv-debug.scm 444 */
																					obj_t BgL_tmpz00_7735;

																					BgL_tmpz00_7735 =
																						((obj_t) BgL_pz00_5619);
																					bgl_display_char(((unsigned char) 10),
																						BgL_tmpz00_7735);
																				}
																				{	/* SawBbv/bbv-debug.scm 445 */
																					long BgL_arg2410z00_5655;

																					BgL_arg2410z00_5655 =
																						(BgL_mz00_5620 + 2L);
																					BGl_dumpzd2marginzd2zzsaw_defsz00
																						(BgL_pz00_5619,
																						(int) (BgL_arg2410z00_5655));
																				}
																				{	/* SawBbv/bbv-debug.scm 446 */
																					obj_t BgL_arg2411z00_5656;

																					{
																						BgL_blocksz00_bglt BgL_auxz00_7741;

																						{
																							obj_t BgL_auxz00_7742;

																							{	/* SawBbv/bbv-debug.scm 446 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_7743;
																								BgL_tmpz00_7743 =
																									((BgL_objectz00_bglt)
																									BgL_bz00_5618);
																								BgL_auxz00_7742 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_7743);
																							}
																							BgL_auxz00_7741 =
																								((BgL_blocksz00_bglt)
																								BgL_auxz00_7742);
																						}
																						BgL_arg2411z00_5656 =
																							(((BgL_blocksz00_bglt)
																								COBJECT(BgL_auxz00_7741))->
																							BgL_creatorz00);
																					}
																					{	/* SawBbv/bbv-debug.scm 446 */
																						obj_t BgL_list2412z00_5657;

																						BgL_list2412z00_5657 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2411z00_5656, BNIL);
																						BGl_fprintfz00zz__r4_output_6_10_3z00
																							(BgL_pz00_5619,
																							BGl_string3066z00zzsaw_bbvzd2debugzd2,
																							BgL_list2412z00_5657);
																			}}}
																		else
																			{	/* SawBbv/bbv-debug.scm 447 */
																				bool_t BgL_test3394z00_7750;

																				{	/* SawBbv/bbv-debug.scm 447 */
																					obj_t BgL_arg2432z00_5658;

																					{
																						BgL_blocksz00_bglt BgL_auxz00_7751;

																						{
																							obj_t BgL_auxz00_7752;

																							{	/* SawBbv/bbv-debug.scm 447 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_7753;
																								BgL_tmpz00_7753 =
																									((BgL_objectz00_bglt)
																									BgL_bz00_5618);
																								BgL_auxz00_7752 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_7753);
																							}
																							BgL_auxz00_7751 =
																								((BgL_blocksz00_bglt)
																								BgL_auxz00_7752);
																						}
																						BgL_arg2432z00_5658 =
																							(((BgL_blocksz00_bglt)
																								COBJECT(BgL_auxz00_7751))->
																							BgL_creatorz00);
																					}
																					BgL_test3394z00_7750 =
																						PAIRP(BgL_arg2432z00_5658);
																				}
																				if (BgL_test3394z00_7750)
																					{	/* SawBbv/bbv-debug.scm 447 */
																						{	/* SawBbv/bbv-debug.scm 448 */
																							obj_t BgL_tmpz00_7759;

																							BgL_tmpz00_7759 =
																								((obj_t) BgL_pz00_5619);
																							bgl_display_string
																								(BGl_string3067z00zzsaw_bbvzd2debugzd2,
																								BgL_tmpz00_7759);
																						}
																						{	/* SawBbv/bbv-debug.scm 448 */
																							obj_t BgL_tmpz00_7762;

																							BgL_tmpz00_7762 =
																								((obj_t) BgL_pz00_5619);
																							bgl_display_char(((unsigned char)
																									10), BgL_tmpz00_7762);
																						}
																						{	/* SawBbv/bbv-debug.scm 449 */
																							long BgL_arg2417z00_5659;

																							BgL_arg2417z00_5659 =
																								(BgL_mz00_5620 + 2L);
																							BGl_dumpzd2marginzd2zzsaw_defsz00
																								(BgL_pz00_5619,
																								(int) (BgL_arg2417z00_5659));
																						}
																						{	/* SawBbv/bbv-debug.scm 451 */
																							int BgL_arg2418z00_5660;
																							int BgL_arg2419z00_5661;

																							{	/* SawBbv/bbv-debug.scm 451 */
																								obj_t BgL_arg2423z00_5662;

																								{	/* SawBbv/bbv-debug.scm 451 */
																									obj_t BgL_arg2424z00_5663;

																									{
																										BgL_blocksz00_bglt
																											BgL_auxz00_7768;
																										{
																											obj_t BgL_auxz00_7769;

																											{	/* SawBbv/bbv-debug.scm 451 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_7770;
																												BgL_tmpz00_7770 =
																													((BgL_objectz00_bglt)
																													BgL_bz00_5618);
																												BgL_auxz00_7769 =
																													BGL_OBJECT_WIDENING
																													(BgL_tmpz00_7770);
																											}
																											BgL_auxz00_7768 =
																												((BgL_blocksz00_bglt)
																												BgL_auxz00_7769);
																										}
																										BgL_arg2424z00_5663 =
																											(((BgL_blocksz00_bglt)
																												COBJECT
																												(BgL_auxz00_7768))->
																											BgL_creatorz00);
																									}
																									BgL_arg2423z00_5662 =
																										CAR(
																										((obj_t)
																											BgL_arg2424z00_5663));
																								}
																								BgL_arg2418z00_5660 =
																									(((BgL_blockz00_bglt) COBJECT(
																											((BgL_blockz00_bglt)
																												BgL_arg2423z00_5662)))->
																									BgL_labelz00);
																							}
																							{	/* SawBbv/bbv-debug.scm 452 */
																								obj_t BgL_arg2425z00_5664;

																								{	/* SawBbv/bbv-debug.scm 452 */
																									obj_t BgL_arg2426z00_5665;

																									{
																										BgL_blocksz00_bglt
																											BgL_auxz00_7779;
																										{
																											obj_t BgL_auxz00_7780;

																											{	/* SawBbv/bbv-debug.scm 452 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_7781;
																												BgL_tmpz00_7781 =
																													((BgL_objectz00_bglt)
																													BgL_bz00_5618);
																												BgL_auxz00_7780 =
																													BGL_OBJECT_WIDENING
																													(BgL_tmpz00_7781);
																											}
																											BgL_auxz00_7779 =
																												((BgL_blocksz00_bglt)
																												BgL_auxz00_7780);
																										}
																										BgL_arg2426z00_5665 =
																											(((BgL_blocksz00_bglt)
																												COBJECT
																												(BgL_auxz00_7779))->
																											BgL_creatorz00);
																									}
																									BgL_arg2425z00_5664 =
																										CDR(
																										((obj_t)
																											BgL_arg2426z00_5665));
																								}
																								BgL_arg2419z00_5661 =
																									(((BgL_blockz00_bglt) COBJECT(
																											((BgL_blockz00_bglt)
																												BgL_arg2425z00_5664)))->
																									BgL_labelz00);
																							}
																							{	/* SawBbv/bbv-debug.scm 450 */
																								obj_t BgL_list2420z00_5666;

																								{	/* SawBbv/bbv-debug.scm 450 */
																									obj_t BgL_arg2421z00_5667;

																									BgL_arg2421z00_5667 =
																										MAKE_YOUNG_PAIR(BINT
																										(BgL_arg2419z00_5661),
																										BNIL);
																									BgL_list2420z00_5666 =
																										MAKE_YOUNG_PAIR(BINT
																										(BgL_arg2418z00_5660),
																										BgL_arg2421z00_5667);
																								}
																								BGl_fprintfz00zz__r4_output_6_10_3z00
																									(BgL_pz00_5619,
																									BGl_string3068z00zzsaw_bbvzd2debugzd2,
																									BgL_list2420z00_5666);
																					}}}
																				else
																					{	/* SawBbv/bbv-debug.scm 455 */
																						obj_t BgL_arg2428z00_5668;
																						obj_t BgL_arg2429z00_5669;

																						{	/* SawBbv/bbv-debug.scm 455 */
																							int BgL_arg2430z00_5670;

																							BgL_arg2430z00_5670 =
																								(((BgL_blockz00_bglt) COBJECT(
																										((BgL_blockz00_bglt)
																											BgL_bz00_5618)))->
																								BgL_labelz00);
																							{	/* SawBbv/bbv-debug.scm 455 */
																								obj_t BgL_list2431z00_5671;

																								BgL_list2431z00_5671 =
																									MAKE_YOUNG_PAIR(BINT
																									(BgL_arg2430z00_5670), BNIL);
																								BgL_arg2428z00_5668 =
																									BGl_formatz00zz__r4_output_6_10_3z00
																									(BGl_string3069z00zzsaw_bbvzd2debugzd2,
																									BgL_list2431z00_5671);
																						}}
																						{
																							BgL_blocksz00_bglt
																								BgL_auxz00_7800;
																							{
																								obj_t BgL_auxz00_7801;

																								{	/* SawBbv/bbv-debug.scm 454 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_7802;
																									BgL_tmpz00_7802 =
																										((BgL_objectz00_bglt)
																										BgL_bz00_5618);
																									BgL_auxz00_7801 =
																										BGL_OBJECT_WIDENING
																										(BgL_tmpz00_7802);
																								}
																								BgL_auxz00_7800 =
																									((BgL_blocksz00_bglt)
																									BgL_auxz00_7801);
																							}
																							BgL_arg2429z00_5669 =
																								(((BgL_blocksz00_bglt)
																									COBJECT(BgL_auxz00_7800))->
																								BgL_creatorz00);
																						}
																						BGl_errorz00zz__errorz00
																							(BGl_string3070z00zzsaw_bbvzd2debugzd2,
																							BgL_arg2428z00_5668,
																							BgL_arg2429z00_5669);
															}}}}
															BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_5619,
																(int) (BgL_mz00_5620));
															{	/* SawBbv/bbv-debug.scm 457 */
																obj_t BgL_tmpz00_7810;

																BgL_tmpz00_7810 = ((obj_t) BgL_pz00_5619);
																bgl_display_string
																	(BGl_string3071z00zzsaw_bbvzd2debugzd2,
																	BgL_tmpz00_7810);
															}
															{	/* SawBbv/bbv-debug.scm 457 */
																obj_t BgL_tmpz00_7813;

																BgL_tmpz00_7813 = ((obj_t) BgL_pz00_5619);
																bgl_display_char(((unsigned char) 10),
																	BgL_tmpz00_7813);
														}}
														{
															obj_t BgL_l1641z00_7817;

															BgL_l1641z00_7817 = CDR(BgL_l1641z00_5616);
															BgL_l1641z00_5616 = BgL_l1641z00_7817;
															goto BgL_zc3z04anonymousza32454ze3z87_5615;
														}
													}
												else
													{	/* SawBbv/bbv-debug.scm 479 */
														((bool_t) 1);
													}
											}
										}
									}
									{
										obj_t BgL_l1648z00_7820;

										BgL_l1648z00_7820 = CDR(BgL_l1648z00_5697);
										BgL_l1648z00_5697 = BgL_l1648z00_7820;
										goto BgL_zc3z04anonymousza32371ze3z87_5696;
									}
								}
							else
								{	/* SawBbv/bbv-debug.scm 490 */
									((bool_t) 1);
								}
						}
					}
					{	/* SawBbv/bbv-debug.scm 491 */
						obj_t BgL_g1653z00_5699;

						BgL_g1653z00_5699 =
							BGl_sortzd2blocksze70z35zzsaw_bbvzd2debugzd2(BgL_blocksz00_5285);
						{
							obj_t BgL_l1651z00_5701;

							BgL_l1651z00_5701 = BgL_g1653z00_5699;
						BgL_zc3z04anonymousza32374ze3z87_5700:
							if (PAIRP(BgL_l1651z00_5701))
								{	/* SawBbv/bbv-debug.scm 492 */
									{	/* SawBbv/bbv-debug.scm 491 */
										obj_t BgL_bz00_5702;

										BgL_bz00_5702 = CAR(BgL_l1651z00_5701);
										BgL_bz00_5672 = ((BgL_blockz00_bglt) BgL_bz00_5702);
										BgL_pz00_5673 = BgL_portz00_5286;
										BgL_mz00_5674 = 4L;
										{	/* SawBbv/bbv-debug.scm 483 */
											obj_t BgL_g1646z00_5675;

											{	/* SawBbv/bbv-debug.scm 484 */
												obj_t BgL_arg2462z00_5676;

												{
													BgL_blockvz00_bglt BgL_auxz00_7826;

													{
														obj_t BgL_auxz00_7827;

														{	/* SawBbv/bbv-debug.scm 484 */
															BgL_objectz00_bglt BgL_tmpz00_7828;

															BgL_tmpz00_7828 =
																((BgL_objectz00_bglt) BgL_bz00_5672);
															BgL_auxz00_7827 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_7828);
														}
														BgL_auxz00_7826 =
															((BgL_blockvz00_bglt) BgL_auxz00_7827);
													}
													BgL_arg2462z00_5676 =
														(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_7826))->
														BgL_versionsz00);
												}
												BgL_g1646z00_5675 =
													BGl_sortzd2blocksze70z35zzsaw_bbvzd2debugzd2
													(BgL_arg2462z00_5676);
											}
											{
												obj_t BgL_l1644z00_5678;

												BgL_l1644z00_5678 = BgL_g1646z00_5675;
											BgL_zc3z04anonymousza32459ze3z87_5677:
												if (PAIRP(BgL_l1644z00_5678))
													{	/* SawBbv/bbv-debug.scm 484 */
														{	/* SawBbv/bbv-debug.scm 483 */
															obj_t BgL_bz00_5679;

															BgL_bz00_5679 = CAR(BgL_l1644z00_5678);
															BgL_bz00_5680 =
																((BgL_blockz00_bglt) BgL_bz00_5679);
															BgL_pz00_5681 = BgL_pz00_5673;
															BgL_mz00_5682 = BgL_mz00_5674;
															{	/* SawBbv/bbv-debug.scm 461 */
																obj_t BgL_g1640z00_5683;

																{
																	BgL_blocksz00_bglt BgL_auxz00_7837;

																	{
																		obj_t BgL_auxz00_7838;

																		{	/* SawBbv/bbv-debug.scm 461 */
																			BgL_objectz00_bglt BgL_tmpz00_7839;

																			BgL_tmpz00_7839 =
																				((BgL_objectz00_bglt) BgL_bz00_5680);
																			BgL_auxz00_7838 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_7839);
																		}
																		BgL_auxz00_7837 =
																			((BgL_blocksz00_bglt) BgL_auxz00_7838);
																	}
																	BgL_g1640z00_5683 =
																		(((BgL_blocksz00_bglt)
																			COBJECT(BgL_auxz00_7837))->BgL_mergesz00);
																}
																{
																	obj_t BgL_l1638z00_5685;

																	BgL_l1638z00_5685 = BgL_g1640z00_5683;
																BgL_zc3z04anonymousza32437ze3z87_5684:
																	if (PAIRP(BgL_l1638z00_5685))
																		{	/* SawBbv/bbv-debug.scm 461 */
																			{	/* SawBbv/bbv-debug.scm 462 */
																				obj_t BgL_ez00_5686;

																				BgL_ez00_5686 = CAR(BgL_l1638z00_5685);
																				BGl_dumpzd2marginzd2zzsaw_defsz00
																					(BgL_pz00_5681,
																					(int) (BgL_mz00_5682));
																				{	/* SawBbv/bbv-debug.scm 463 */
																					obj_t BgL_tmpz00_7849;

																					BgL_tmpz00_7849 =
																						((obj_t) BgL_pz00_5681);
																					bgl_display_string
																						(BGl_string3026z00zzsaw_bbvzd2debugzd2,
																						BgL_tmpz00_7849);
																				}
																				{	/* SawBbv/bbv-debug.scm 463 */
																					obj_t BgL_tmpz00_7852;

																					BgL_tmpz00_7852 =
																						((obj_t) BgL_pz00_5681);
																					bgl_display_char(((unsigned char) 10),
																						BgL_tmpz00_7852);
																				}
																				{	/* SawBbv/bbv-debug.scm 464 */
																					long BgL_arg2439z00_5687;

																					BgL_arg2439z00_5687 =
																						(BgL_mz00_5682 + 2L);
																					BGl_dumpzd2marginzd2zzsaw_defsz00
																						(BgL_pz00_5681,
																						(int) (BgL_arg2439z00_5687));
																				}
																				{	/* SawBbv/bbv-debug.scm 465 */
																					obj_t BgL_tmpz00_7858;

																					BgL_tmpz00_7858 =
																						((obj_t) BgL_pz00_5681);
																					bgl_display_string
																						(BGl_string3027z00zzsaw_bbvzd2debugzd2,
																						BgL_tmpz00_7858);
																				}
																				{	/* SawBbv/bbv-debug.scm 465 */
																					int BgL_arg2442z00_5688;

																					BgL_arg2442z00_5688 =
																						(((BgL_blockz00_bglt) COBJECT(
																								((BgL_blockz00_bglt)
																									BgL_bz00_5680)))->
																						BgL_labelz00);
																					bgl_display_obj(BINT
																						(BgL_arg2442z00_5688),
																						BgL_pz00_5681);
																				}
																				{	/* SawBbv/bbv-debug.scm 465 */
																					obj_t BgL_tmpz00_7865;

																					BgL_tmpz00_7865 =
																						((obj_t) BgL_pz00_5681);
																					bgl_display_string
																						(BGl_string3002z00zzsaw_bbvzd2debugzd2,
																						BgL_tmpz00_7865);
																				}
																				{	/* SawBbv/bbv-debug.scm 465 */
																					obj_t BgL_tmpz00_7868;

																					BgL_tmpz00_7868 =
																						((obj_t) BgL_pz00_5681);
																					bgl_display_char(((unsigned char) 10),
																						BgL_tmpz00_7868);
																				}
																				{	/* SawBbv/bbv-debug.scm 466 */
																					long BgL_arg2443z00_5689;

																					BgL_arg2443z00_5689 =
																						(BgL_mz00_5682 + 2L);
																					BGl_dumpzd2marginzd2zzsaw_defsz00
																						(BgL_pz00_5681,
																						(int) (BgL_arg2443z00_5689));
																				}
																				{	/* SawBbv/bbv-debug.scm 467 */
																					obj_t BgL_tmpz00_7874;

																					BgL_tmpz00_7874 =
																						((obj_t) BgL_pz00_5681);
																					bgl_display_string
																						(BGl_string3072z00zzsaw_bbvzd2debugzd2,
																						BgL_tmpz00_7874);
																				}
																				{	/* SawBbv/bbv-debug.scm 467 */
																					obj_t BgL_tmpz00_7877;

																					BgL_tmpz00_7877 =
																						((obj_t) BgL_pz00_5681);
																					bgl_display_char(((unsigned char) 10),
																						BgL_tmpz00_7877);
																				}
																				{	/* SawBbv/bbv-debug.scm 468 */
																					long BgL_arg2444z00_5690;

																					BgL_arg2444z00_5690 =
																						(BgL_mz00_5682 + 2L);
																					BGl_dumpzd2marginzd2zzsaw_defsz00
																						(BgL_pz00_5681,
																						(int) (BgL_arg2444z00_5690));
																				}
																				{	/* SawBbv/bbv-debug.scm 470 */
																					int BgL_arg2445z00_5691;
																					int BgL_arg2446z00_5692;

																					BgL_arg2445z00_5691 =
																						(((BgL_blockz00_bglt) COBJECT(
																								((BgL_blockz00_bglt)
																									CAR(
																										((obj_t)
																											BgL_ez00_5686)))))->
																						BgL_labelz00);
																					BgL_arg2446z00_5692 =
																						(((BgL_blockz00_bglt)
																							COBJECT(((BgL_blockz00_bglt)
																									CDR(((obj_t)
																											BgL_ez00_5686)))))->
																						BgL_labelz00);
																					{	/* SawBbv/bbv-debug.scm 469 */
																						obj_t BgL_list2447z00_5693;

																						{	/* SawBbv/bbv-debug.scm 469 */
																							obj_t BgL_arg2449z00_5694;

																							BgL_arg2449z00_5694 =
																								MAKE_YOUNG_PAIR(BINT
																								(BgL_arg2446z00_5692), BNIL);
																							BgL_list2447z00_5693 =
																								MAKE_YOUNG_PAIR(BINT
																								(BgL_arg2445z00_5691),
																								BgL_arg2449z00_5694);
																						}
																						BGl_fprintfz00zz__r4_output_6_10_3z00
																							(BgL_pz00_5681,
																							BGl_string3068z00zzsaw_bbvzd2debugzd2,
																							BgL_list2447z00_5693);
																				}}
																				BGl_dumpzd2marginzd2zzsaw_defsz00
																					(BgL_pz00_5681,
																					(int) (BgL_mz00_5682));
																				{	/* SawBbv/bbv-debug.scm 473 */
																					obj_t BgL_tmpz00_7898;

																					BgL_tmpz00_7898 =
																						((obj_t) BgL_pz00_5681);
																					bgl_display_string
																						(BGl_string3071z00zzsaw_bbvzd2debugzd2,
																						BgL_tmpz00_7898);
																				}
																				{	/* SawBbv/bbv-debug.scm 473 */
																					obj_t BgL_tmpz00_7901;

																					BgL_tmpz00_7901 =
																						((obj_t) BgL_pz00_5681);
																					bgl_display_char(((unsigned char) 10),
																						BgL_tmpz00_7901);
																			}}
																			{
																				obj_t BgL_l1638z00_7904;

																				BgL_l1638z00_7904 =
																					CDR(BgL_l1638z00_5685);
																				BgL_l1638z00_5685 = BgL_l1638z00_7904;
																				goto
																					BgL_zc3z04anonymousza32437ze3z87_5684;
																			}
																		}
																	else
																		{	/* SawBbv/bbv-debug.scm 461 */
																			((bool_t) 1);
																		}
																}
															}
														}
														{
															obj_t BgL_l1644z00_7907;

															BgL_l1644z00_7907 = CDR(BgL_l1644z00_5678);
															BgL_l1644z00_5678 = BgL_l1644z00_7907;
															goto BgL_zc3z04anonymousza32459ze3z87_5677;
														}
													}
												else
													{	/* SawBbv/bbv-debug.scm 484 */
														((bool_t) 1);
													}
											}
										}
									}
									{
										obj_t BgL_l1651z00_7910;

										BgL_l1651z00_7910 = CDR(BgL_l1651z00_5701);
										BgL_l1651z00_5701 = BgL_l1651z00_7910;
										goto BgL_zc3z04anonymousza32374ze3z87_5700;
									}
								}
							else
								{	/* SawBbv/bbv-debug.scm 492 */
									((bool_t) 1);
								}
						}
					}
					{	/* SawBbv/bbv-debug.scm 493 */
						obj_t BgL_tmpz00_7912;

						BgL_tmpz00_7912 = ((obj_t) BgL_portz00_5286);
						bgl_display_string(BGl_string3074z00zzsaw_bbvzd2debugzd2,
							BgL_tmpz00_7912);
					}
					{	/* SawBbv/bbv-debug.scm 494 */
						obj_t BgL_tmpz00_7915;

						BgL_tmpz00_7915 = ((obj_t) BgL_portz00_5286);
						bgl_display_string(BGl_string3047z00zzsaw_bbvzd2debugzd2,
							BgL_tmpz00_7915);
					}
					{	/* SawBbv/bbv-debug.scm 494 */
						obj_t BgL_tmpz00_7918;

						BgL_tmpz00_7918 = ((obj_t) BgL_portz00_5286);
						return bgl_display_char(((unsigned char) 10), BgL_tmpz00_7918);
		}}}}

	}



/* sort-blocks~0 */
	obj_t BGl_sortzd2blocksze70z35zzsaw_bbvzd2debugzd2(obj_t BgL_bz00_3163)
	{
		{	/* SawBbv/bbv-debug.scm 416 */
			return
				BGl_sortz00zz__r4_vectors_6_8z00(BGl_proc3075z00zzsaw_bbvzd2debugzd2,
				BgL_bz00_3163);
		}

	}



/* &<@anonymous:2394> */
	obj_t BGl_z62zc3z04anonymousza32394ze3ze5zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5287, obj_t BgL_opz00_5289)
	{
		{	/* SawBbv/bbv-debug.scm 431 */
			{	/* SawBbv/bbv-debug.scm 432 */
				BgL_blockz00_bglt BgL_i1203z00_5288;

				BgL_i1203z00_5288 =
					((BgL_blockz00_bglt) PROCEDURE_REF(BgL_envz00_5287, (int) (0L)));
				{	/* SawBbv/bbv-debug.scm 432 */
					bool_t BgL_tmpz00_7925;

					{	/* SawBbv/bbv-debug.scm 432 */
						obj_t BgL_g1629z00_5703;

						{	/* SawBbv/bbv-debug.scm 435 */
							BgL_bbvzd2ctxzd2_bglt BgL_i1204z00_5704;

							{
								BgL_blocksz00_bglt BgL_auxz00_7926;

								{
									obj_t BgL_auxz00_7927;

									{	/* SawBbv/bbv-debug.scm 435 */
										BgL_objectz00_bglt BgL_tmpz00_7928;

										BgL_tmpz00_7928 = ((BgL_objectz00_bglt) BgL_i1203z00_5288);
										BgL_auxz00_7927 = BGL_OBJECT_WIDENING(BgL_tmpz00_7928);
									}
									BgL_auxz00_7926 = ((BgL_blocksz00_bglt) BgL_auxz00_7927);
								}
								BgL_i1204z00_5704 =
									(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7926))->BgL_ctxz00);
							}
							BgL_g1629z00_5703 =
								(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_i1204z00_5704))->
								BgL_entriesz00);
						}
						{
							obj_t BgL_l1627z00_5706;

							BgL_l1627z00_5706 = BgL_g1629z00_5703;
						BgL_zc3z04anonymousza32395ze3z87_5705:
							if (PAIRP(BgL_l1627z00_5706))
								{	/* SawBbv/bbv-debug.scm 435 */
									{	/* SawBbv/bbv-debug.scm 433 */
										obj_t BgL_ez00_5707;

										BgL_ez00_5707 = CAR(BgL_l1627z00_5706);
										bgl_display_obj
											(BGl_shapezd2ctxzd2entryz00zzsaw_bbvzd2debugzd2
											(BgL_ez00_5707), BgL_opz00_5289);
										{	/* SawBbv/bbv-debug.scm 434 */
											obj_t BgL_tmpz00_7939;

											BgL_tmpz00_7939 = ((obj_t) BgL_opz00_5289);
											bgl_display_string(BGl_string3051z00zzsaw_bbvzd2debugzd2,
												BgL_tmpz00_7939);
										}
									}
									{
										obj_t BgL_l1627z00_7942;

										BgL_l1627z00_7942 = CDR(BgL_l1627z00_5706);
										BgL_l1627z00_5706 = BgL_l1627z00_7942;
										goto BgL_zc3z04anonymousza32395ze3z87_5705;
									}
								}
							else
								{	/* SawBbv/bbv-debug.scm 435 */
									BgL_tmpz00_7925 = ((bool_t) 1);
								}
						}
					}
					return BBOOL(BgL_tmpz00_7925);
				}
			}
		}

	}



/* &<@anonymous:2379> */
	obj_t BGl_z62zc3z04anonymousza32379ze3ze5zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5290, obj_t BgL_xz00_5291, obj_t BgL_yz00_5292)
	{
		{	/* SawBbv/bbv-debug.scm 416 */
			{	/* SawBbv/bbv-debug.scm 416 */
				bool_t BgL_tmpz00_7945;

				{	/* SawBbv/bbv-debug.scm 416 */

					BgL_tmpz00_7945 =
						(
						(long) (
							(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_xz00_5291)))->BgL_labelz00)) <=
						(long) (
							(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_yz00_5292)))->BgL_labelz00)));
				}
				return BBOOL(BgL_tmpz00_7945);
			}
		}

	}



/* assert-blocks */
	BGL_EXPORTED_DEF obj_t
		BGl_assertzd2blockszd2zzsaw_bbvzd2debugzd2(BgL_blockz00_bglt BgL_bz00_124,
		obj_t BgL_lblz00_125)
	{
		{	/* SawBbv/bbv-debug.scm 503 */
			if (CBOOL(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2))
				{	/* SawBbv/bbv-debug.scm 505 */
					obj_t BgL_g1208z00_3300;
					obj_t BgL_g1209z00_3301;

					{	/* SawBbv/bbv-debug.scm 505 */
						obj_t BgL_list2480z00_3319;

						BgL_list2480z00_3319 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_124), BNIL);
						BgL_g1208z00_3300 = BgL_list2480z00_3319;
					}
					BgL_g1209z00_3301 = BGl_makezd2emptyzd2bbsetz00zzsaw_bbvzd2debugzd2();
					{
						obj_t BgL_bsz00_3303;
						obj_t BgL_setz00_3304;

						BgL_bsz00_3303 = BgL_g1208z00_3300;
						BgL_setz00_3304 = BgL_g1209z00_3301;
					BgL_zc3z04anonymousza32463ze3z87_3305:
						if (NULLP(BgL_bsz00_3303))
							{	/* SawBbv/bbv-debug.scm 508 */
								return BgL_setz00_3304;
							}
						else
							{	/* SawBbv/bbv-debug.scm 510 */
								bool_t BgL_test3401z00_7961;

								{	/* SawBbv/bbv-debug.scm 510 */
									BgL_blockz00_bglt BgL_blockz00_4687;

									BgL_blockz00_4687 =
										((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_3303)));
									{	/* SawBbv/bbv-debug.scm 510 */
										long BgL_arg1724z00_4690;
										obj_t BgL_arg1733z00_4691;

										{
											BgL_blocksz00_bglt BgL_auxz00_7965;

											{
												obj_t BgL_auxz00_7966;

												{	/* SawBbv/bbv-debug.scm 510 */
													BgL_objectz00_bglt BgL_tmpz00_7967;

													BgL_tmpz00_7967 =
														((BgL_objectz00_bglt) BgL_blockz00_4687);
													BgL_auxz00_7966 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_7967);
												}
												BgL_auxz00_7965 =
													((BgL_blocksz00_bglt) BgL_auxz00_7966);
											}
											BgL_arg1724z00_4690 =
												(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7965))->
												BgL_z52markz52);
										}
										BgL_arg1733z00_4691 =
											STRUCT_REF(BgL_setz00_3304, (int) (0L));
										BgL_test3401z00_7961 =
											(BINT(BgL_arg1724z00_4690) == BgL_arg1733z00_4691);
								}}
								if (BgL_test3401z00_7961)
									{	/* SawBbv/bbv-debug.scm 511 */
										obj_t BgL_arg2469z00_3309;

										BgL_arg2469z00_3309 = CDR(((obj_t) BgL_bsz00_3303));
										{
											obj_t BgL_bsz00_7978;

											BgL_bsz00_7978 = BgL_arg2469z00_3309;
											BgL_bsz00_3303 = BgL_bsz00_7978;
											goto BgL_zc3z04anonymousza32463ze3z87_3305;
										}
									}
								else
									{	/* SawBbv/bbv-debug.scm 513 */
										BgL_blockz00_bglt BgL_i1210z00_3310;

										BgL_i1210z00_3310 =
											((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_3303)));
										{	/* SawBbv/bbv-debug.scm 514 */
											obj_t BgL_arg2470z00_3311;

											BgL_arg2470z00_3311 = CAR(((obj_t) BgL_bsz00_3303));
											BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(
												((BgL_blockz00_bglt) BgL_arg2470z00_3311),
												BgL_lblz00_125);
										}
										{	/* SawBbv/bbv-debug.scm 515 */
											obj_t BgL_arg2471z00_3312;
											obj_t BgL_arg2473z00_3313;

											{	/* SawBbv/bbv-debug.scm 515 */
												obj_t BgL_arg2474z00_3314;
												obj_t BgL_arg2475z00_3315;

												BgL_arg2474z00_3314 =
													(((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt) BgL_i1210z00_3310)))->
													BgL_succsz00);
												BgL_arg2475z00_3315 = CDR(((obj_t) BgL_bsz00_3303));
												BgL_arg2471z00_3312 =
													BGl_appendzd221011zd2zzsaw_bbvzd2debugzd2
													(BgL_arg2474z00_3314, BgL_arg2475z00_3315);
											}
											{	/* SawBbv/bbv-debug.scm 516 */
												obj_t BgL_arg2476z00_3316;

												BgL_arg2476z00_3316 = CAR(((obj_t) BgL_bsz00_3303));
												BgL_arg2473z00_3313 =
													BGl_bbsetzd2conszd2zzsaw_bbvzd2debugzd2(
													((BgL_blockz00_bglt) BgL_arg2476z00_3316),
													BgL_setz00_3304);
											}
											{
												obj_t BgL_setz00_7996;
												obj_t BgL_bsz00_7995;

												BgL_bsz00_7995 = BgL_arg2471z00_3312;
												BgL_setz00_7996 = BgL_arg2473z00_3313;
												BgL_setz00_3304 = BgL_setz00_7996;
												BgL_bsz00_3303 = BgL_bsz00_7995;
												goto BgL_zc3z04anonymousza32463ze3z87_3305;
											}
										}
									}
							}
					}
				}
			else
				{	/* SawBbv/bbv-debug.scm 504 */
					return BFALSE;
				}
		}

	}



/* &assert-blocks */
	obj_t BGl_z62assertzd2blockszb0zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5293,
		obj_t BgL_bz00_5294, obj_t BgL_lblz00_5295)
	{
		{	/* SawBbv/bbv-debug.scm 503 */
			return
				BGl_assertzd2blockszd2zzsaw_bbvzd2debugzd2(
				((BgL_blockz00_bglt) BgL_bz00_5294), BgL_lblz00_5295);
		}

	}



/* reg-debugname */
	obj_t BGl_regzd2debugnamezd2zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_regz00_126)
	{
		{	/* SawBbv/bbv-debug.scm 521 */
			if (CBOOL(
					(((BgL_rtl_regz00_bglt) COBJECT(BgL_regz00_126))->BgL_debugnamez00)))
				{	/* SawBbv/bbv-debug.scm 524 */
					return
						(((BgL_rtl_regz00_bglt) COBJECT(BgL_regz00_126))->BgL_debugnamez00);
				}
			else
				{	/* SawBbv/bbv-debug.scm 526 */
					bool_t BgL_test3403z00_8003;

					{	/* SawBbv/bbv-debug.scm 526 */
						obj_t BgL_arg2503z00_3340;

						BgL_arg2503z00_3340 =
							(((BgL_rtl_regz00_bglt) COBJECT(BgL_regz00_126))->BgL_varz00);
						{	/* SawBbv/bbv-debug.scm 526 */
							obj_t BgL_classz00_4699;

							BgL_classz00_4699 = BGl_globalz00zzast_varz00;
							if (BGL_OBJECTP(BgL_arg2503z00_3340))
								{	/* SawBbv/bbv-debug.scm 526 */
									BgL_objectz00_bglt BgL_arg1807z00_4701;

									BgL_arg1807z00_4701 =
										(BgL_objectz00_bglt) (BgL_arg2503z00_3340);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawBbv/bbv-debug.scm 526 */
											long BgL_idxz00_4707;

											BgL_idxz00_4707 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4701);
											BgL_test3403z00_8003 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4707 + 2L)) == BgL_classz00_4699);
										}
									else
										{	/* SawBbv/bbv-debug.scm 526 */
											bool_t BgL_res2985z00_4732;

											{	/* SawBbv/bbv-debug.scm 526 */
												obj_t BgL_oclassz00_4715;

												{	/* SawBbv/bbv-debug.scm 526 */
													obj_t BgL_arg1815z00_4723;
													long BgL_arg1816z00_4724;

													BgL_arg1815z00_4723 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawBbv/bbv-debug.scm 526 */
														long BgL_arg1817z00_4725;

														BgL_arg1817z00_4725 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4701);
														BgL_arg1816z00_4724 =
															(BgL_arg1817z00_4725 - OBJECT_TYPE);
													}
													BgL_oclassz00_4715 =
														VECTOR_REF(BgL_arg1815z00_4723,
														BgL_arg1816z00_4724);
												}
												{	/* SawBbv/bbv-debug.scm 526 */
													bool_t BgL__ortest_1115z00_4716;

													BgL__ortest_1115z00_4716 =
														(BgL_classz00_4699 == BgL_oclassz00_4715);
													if (BgL__ortest_1115z00_4716)
														{	/* SawBbv/bbv-debug.scm 526 */
															BgL_res2985z00_4732 = BgL__ortest_1115z00_4716;
														}
													else
														{	/* SawBbv/bbv-debug.scm 526 */
															long BgL_odepthz00_4717;

															{	/* SawBbv/bbv-debug.scm 526 */
																obj_t BgL_arg1804z00_4718;

																BgL_arg1804z00_4718 = (BgL_oclassz00_4715);
																BgL_odepthz00_4717 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4718);
															}
															if ((2L < BgL_odepthz00_4717))
																{	/* SawBbv/bbv-debug.scm 526 */
																	obj_t BgL_arg1802z00_4720;

																	{	/* SawBbv/bbv-debug.scm 526 */
																		obj_t BgL_arg1803z00_4721;

																		BgL_arg1803z00_4721 = (BgL_oclassz00_4715);
																		BgL_arg1802z00_4720 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4721, 2L);
																	}
																	BgL_res2985z00_4732 =
																		(BgL_arg1802z00_4720 == BgL_classz00_4699);
																}
															else
																{	/* SawBbv/bbv-debug.scm 526 */
																	BgL_res2985z00_4732 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3403z00_8003 = BgL_res2985z00_4732;
										}
								}
							else
								{	/* SawBbv/bbv-debug.scm 526 */
									BgL_test3403z00_8003 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test3403z00_8003)
						{	/* SawBbv/bbv-debug.scm 527 */
							BgL_globalz00_bglt BgL_i1212z00_3324;

							BgL_i1212z00_3324 =
								((BgL_globalz00_bglt)
								(((BgL_rtl_regz00_bglt) COBJECT(BgL_regz00_126))->BgL_varz00));
							{
								obj_t BgL_auxz00_8029;

								{	/* SawBbv/bbv-debug.scm 529 */
									obj_t BgL_arg2484z00_3325;
									obj_t BgL_arg2486z00_3326;

									{	/* SawBbv/bbv-debug.scm 529 */
										obj_t BgL_arg2487z00_3327;

										{	/* SawBbv/bbv-debug.scm 529 */
											obj_t BgL__ortest_1213z00_3328;

											BgL__ortest_1213z00_3328 =
												(((BgL_globalz00_bglt) COBJECT(BgL_i1212z00_3324))->
												BgL_aliasz00);
											if (CBOOL(BgL__ortest_1213z00_3328))
												{	/* SawBbv/bbv-debug.scm 529 */
													BgL_arg2487z00_3327 = BgL__ortest_1213z00_3328;
												}
											else
												{	/* SawBbv/bbv-debug.scm 529 */
													BgL_arg2487z00_3327 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt) BgL_i1212z00_3324)))->
														BgL_idz00);
												}
										}
										{	/* SawBbv/bbv-debug.scm 529 */
											obj_t BgL_arg1455z00_4734;

											BgL_arg1455z00_4734 =
												SYMBOL_TO_STRING(((obj_t) BgL_arg2487z00_3327));
											BgL_arg2484z00_3325 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_4734);
										}
									}
									{	/* SawBbv/bbv-debug.scm 530 */
										obj_t BgL_arg2488z00_3329;

										BgL_arg2488z00_3329 =
											(((BgL_globalz00_bglt) COBJECT(BgL_i1212z00_3324))->
											BgL_modulez00);
										{	/* SawBbv/bbv-debug.scm 530 */
											obj_t BgL_arg1455z00_4736;

											BgL_arg1455z00_4736 =
												SYMBOL_TO_STRING(BgL_arg2488z00_3329);
											BgL_arg2486z00_3326 =
												BGl_stringzd2copyzd2zz__r4_strings_6_7z00
												(BgL_arg1455z00_4736);
										}
									}
									BgL_auxz00_8029 =
										bigloo_module_mangle(BgL_arg2484z00_3325,
										BgL_arg2486z00_3326);
								}
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_regz00_126))->
										BgL_debugnamez00) = ((obj_t) BgL_auxz00_8029), BUNSPEC);
							}
							return
								(((BgL_rtl_regz00_bglt) COBJECT(BgL_regz00_126))->
								BgL_debugnamez00);
						}
					else
						{	/* SawBbv/bbv-debug.scm 532 */
							bool_t BgL_test3409z00_8044;

							{	/* SawBbv/bbv-debug.scm 532 */
								obj_t BgL_arg2502z00_3339;

								BgL_arg2502z00_3339 =
									(((BgL_rtl_regz00_bglt) COBJECT(BgL_regz00_126))->BgL_varz00);
								{	/* SawBbv/bbv-debug.scm 532 */
									obj_t BgL_classz00_4737;

									BgL_classz00_4737 = BGl_localz00zzast_varz00;
									if (BGL_OBJECTP(BgL_arg2502z00_3339))
										{	/* SawBbv/bbv-debug.scm 532 */
											BgL_objectz00_bglt BgL_arg1807z00_4739;

											BgL_arg1807z00_4739 =
												(BgL_objectz00_bglt) (BgL_arg2502z00_3339);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawBbv/bbv-debug.scm 532 */
													long BgL_idxz00_4745;

													BgL_idxz00_4745 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4739);
													BgL_test3409z00_8044 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_4745 + 2L)) == BgL_classz00_4737);
												}
											else
												{	/* SawBbv/bbv-debug.scm 532 */
													bool_t BgL_res2986z00_4770;

													{	/* SawBbv/bbv-debug.scm 532 */
														obj_t BgL_oclassz00_4753;

														{	/* SawBbv/bbv-debug.scm 532 */
															obj_t BgL_arg1815z00_4761;
															long BgL_arg1816z00_4762;

															BgL_arg1815z00_4761 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawBbv/bbv-debug.scm 532 */
																long BgL_arg1817z00_4763;

																BgL_arg1817z00_4763 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4739);
																BgL_arg1816z00_4762 =
																	(BgL_arg1817z00_4763 - OBJECT_TYPE);
															}
															BgL_oclassz00_4753 =
																VECTOR_REF(BgL_arg1815z00_4761,
																BgL_arg1816z00_4762);
														}
														{	/* SawBbv/bbv-debug.scm 532 */
															bool_t BgL__ortest_1115z00_4754;

															BgL__ortest_1115z00_4754 =
																(BgL_classz00_4737 == BgL_oclassz00_4753);
															if (BgL__ortest_1115z00_4754)
																{	/* SawBbv/bbv-debug.scm 532 */
																	BgL_res2986z00_4770 =
																		BgL__ortest_1115z00_4754;
																}
															else
																{	/* SawBbv/bbv-debug.scm 532 */
																	long BgL_odepthz00_4755;

																	{	/* SawBbv/bbv-debug.scm 532 */
																		obj_t BgL_arg1804z00_4756;

																		BgL_arg1804z00_4756 = (BgL_oclassz00_4753);
																		BgL_odepthz00_4755 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_4756);
																	}
																	if ((2L < BgL_odepthz00_4755))
																		{	/* SawBbv/bbv-debug.scm 532 */
																			obj_t BgL_arg1802z00_4758;

																			{	/* SawBbv/bbv-debug.scm 532 */
																				obj_t BgL_arg1803z00_4759;

																				BgL_arg1803z00_4759 =
																					(BgL_oclassz00_4753);
																				BgL_arg1802z00_4758 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_4759, 2L);
																			}
																			BgL_res2986z00_4770 =
																				(BgL_arg1802z00_4758 ==
																				BgL_classz00_4737);
																		}
																	else
																		{	/* SawBbv/bbv-debug.scm 532 */
																			BgL_res2986z00_4770 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test3409z00_8044 = BgL_res2986z00_4770;
												}
										}
									else
										{	/* SawBbv/bbv-debug.scm 532 */
											BgL_test3409z00_8044 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test3409z00_8044)
								{	/* SawBbv/bbv-debug.scm 533 */
									BgL_localz00_bglt BgL_i1214z00_3332;

									BgL_i1214z00_3332 =
										((BgL_localz00_bglt)
										(((BgL_rtl_regz00_bglt) COBJECT(BgL_regz00_126))->
											BgL_varz00));
									{
										obj_t BgL_auxz00_8070;

										{	/* SawBbv/bbv-debug.scm 537 */
											obj_t BgL_arg2492z00_3333;

											{	/* SawBbv/bbv-debug.scm 537 */
												obj_t BgL_arg2493z00_3334;
												obj_t BgL_arg2495z00_3335;

												{	/* SawBbv/bbv-debug.scm 537 */
													obj_t BgL_arg2497z00_3336;

													BgL_arg2497z00_3336 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt) BgL_i1214z00_3332)))->
														BgL_idz00);
													{	/* SawBbv/bbv-debug.scm 537 */
														obj_t BgL_arg1455z00_4772;

														BgL_arg1455z00_4772 =
															SYMBOL_TO_STRING(BgL_arg2497z00_3336);
														BgL_arg2493z00_3334 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_4772);
													}
												}
												{	/* SawBbv/bbv-debug.scm 537 */
													obj_t BgL_arg2500z00_3337;

													BgL_arg2500z00_3337 =
														(((BgL_rtl_regz00_bglt) COBJECT(BgL_regz00_126))->
														BgL_keyz00);
													{	/* SawBbv/bbv-debug.scm 537 */
														obj_t BgL_arg1455z00_4774;

														BgL_arg1455z00_4774 =
															SYMBOL_TO_STRING(((obj_t) BgL_arg2500z00_3337));
														BgL_arg2495z00_3335 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_4774);
													}
												}
												BgL_arg2492z00_3333 =
													string_append_3(BgL_arg2493z00_3334,
													BGl_string3076z00zzsaw_bbvzd2debugzd2,
													BgL_arg2495z00_3335);
											}
											BgL_auxz00_8070 = bigloo_mangle(BgL_arg2492z00_3333);
										}
										((((BgL_rtl_regz00_bglt) COBJECT(BgL_regz00_126))->
												BgL_debugnamez00) = ((obj_t) BgL_auxz00_8070), BUNSPEC);
									}
									return
										(((BgL_rtl_regz00_bglt) COBJECT(BgL_regz00_126))->
										BgL_debugnamez00);
								}
							else
								{	/* SawBbv/bbv-debug.scm 532 */
									{
										obj_t BgL_auxz00_8083;

										{	/* SawBbv/bbv-debug.scm 540 */
											obj_t BgL_arg2501z00_3338;

											BgL_arg2501z00_3338 =
												BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(5));
											{	/* SawBbv/bbv-debug.scm 540 */
												obj_t BgL_arg1455z00_4776;

												BgL_arg1455z00_4776 =
													SYMBOL_TO_STRING(BgL_arg2501z00_3338);
												BgL_auxz00_8083 =
													BGl_stringzd2copyzd2zz__r4_strings_6_7z00
													(BgL_arg1455z00_4776);
											}
										}
										((((BgL_rtl_regz00_bglt) COBJECT(BgL_regz00_126))->
												BgL_debugnamez00) = ((obj_t) BgL_auxz00_8083), BUNSPEC);
									}
									return
										(((BgL_rtl_regz00_bglt) COBJECT(BgL_regz00_126))->
										BgL_debugnamez00);
								}
						}
				}
		}

	}



/* type-predicate */
	obj_t BGl_typezd2predicatezd2zzsaw_bbvzd2debugzd2(obj_t BgL_typez00_127)
	{
		{	/* SawBbv/bbv-debug.scm 546 */
			if ((BgL_typez00_127 == BGl_za2longza2z00zztype_cachez00))
				{	/* SawBbv/bbv-debug.scm 548 */
					return BGl_string3077z00zzsaw_bbvzd2debugzd2;
				}
			else
				{	/* SawBbv/bbv-debug.scm 548 */
					if ((BgL_typez00_127 == BGl_za2bintza2z00zztype_cachez00))
						{	/* SawBbv/bbv-debug.scm 549 */
							return BGl_string3077z00zzsaw_bbvzd2debugzd2;
						}
					else
						{	/* SawBbv/bbv-debug.scm 549 */
							if ((BgL_typez00_127 == BGl_za2realza2z00zztype_cachez00))
								{	/* SawBbv/bbv-debug.scm 550 */
									return BGl_string3078z00zzsaw_bbvzd2debugzd2;
								}
							else
								{	/* SawBbv/bbv-debug.scm 550 */
									if ((BgL_typez00_127 == BGl_za2brealza2z00zztype_cachez00))
										{	/* SawBbv/bbv-debug.scm 551 */
											return BGl_string3078z00zzsaw_bbvzd2debugzd2;
										}
									else
										{	/* SawBbv/bbv-debug.scm 551 */
											if (
												(BgL_typez00_127 == BGl_za2vectorza2z00zztype_cachez00))
												{	/* SawBbv/bbv-debug.scm 552 */
													return BGl_string3079z00zzsaw_bbvzd2debugzd2;
												}
											else
												{	/* SawBbv/bbv-debug.scm 552 */
													if (
														(BgL_typez00_127 ==
															BGl_za2pairza2z00zztype_cachez00))
														{	/* SawBbv/bbv-debug.scm 553 */
															return BGl_string3080z00zzsaw_bbvzd2debugzd2;
														}
													else
														{	/* SawBbv/bbv-debug.scm 553 */
															if (
																(BgL_typez00_127 ==
																	BGl_za2stringza2z00zztype_cachez00))
																{	/* SawBbv/bbv-debug.scm 554 */
																	return BGl_string3081z00zzsaw_bbvzd2debugzd2;
																}
															else
																{	/* SawBbv/bbv-debug.scm 554 */
																	if (
																		(BgL_typez00_127 ==
																			BGl_za2bstringza2z00zztype_cachez00))
																		{	/* SawBbv/bbv-debug.scm 555 */
																			return
																				BGl_string3081z00zzsaw_bbvzd2debugzd2;
																		}
																	else
																		{	/* SawBbv/bbv-debug.scm 555 */
																			if (
																				(BgL_typez00_127 ==
																					BGl_za2charza2z00zztype_cachez00))
																				{	/* SawBbv/bbv-debug.scm 556 */
																					return
																						BGl_string3082z00zzsaw_bbvzd2debugzd2;
																				}
																			else
																				{	/* SawBbv/bbv-debug.scm 556 */
																					if (
																						(BgL_typez00_127 ==
																							BGl_za2bcharza2z00zztype_cachez00))
																						{	/* SawBbv/bbv-debug.scm 557 */
																							return
																								BGl_string3082z00zzsaw_bbvzd2debugzd2;
																						}
																					else
																						{	/* SawBbv/bbv-debug.scm 557 */
																							if (
																								(BgL_typez00_127 ==
																									BGl_za2bnilza2z00zztype_cachez00))
																								{	/* SawBbv/bbv-debug.scm 558 */
																									return
																										BGl_string3083z00zzsaw_bbvzd2debugzd2;
																								}
																							else
																								{	/* SawBbv/bbv-debug.scm 558 */
																									if (
																										(BgL_typez00_127 ==
																											BGl_za2procedureza2z00zztype_cachez00))
																										{	/* SawBbv/bbv-debug.scm 559 */
																											return
																												BGl_string3084z00zzsaw_bbvzd2debugzd2;
																										}
																									else
																										{	/* SawBbv/bbv-debug.scm 559 */
																											if (
																												(BgL_typez00_127 ==
																													BGl_za2outputzd2portza2zd2zztype_cachez00))
																												{	/* SawBbv/bbv-debug.scm 560 */
																													return
																														BGl_string3085z00zzsaw_bbvzd2debugzd2;
																												}
																											else
																												{	/* SawBbv/bbv-debug.scm 560 */
																													if (
																														(BgL_typez00_127 ==
																															BGl_za2boolza2z00zztype_cachez00))
																														{	/* SawBbv/bbv-debug.scm 561 */
																															return
																																BGl_string3086z00zzsaw_bbvzd2debugzd2;
																														}
																													else
																														{	/* SawBbv/bbv-debug.scm 561 */
																															if (
																																(BgL_typez00_127
																																	==
																																	BGl_za2bboolza2z00zztype_cachez00))
																																{	/* SawBbv/bbv-debug.scm 562 */
																																	return
																																		BGl_string3086z00zzsaw_bbvzd2debugzd2;
																																}
																															else
																																{	/* SawBbv/bbv-debug.scm 562 */
																																	if (
																																		(BgL_typez00_127
																																			==
																																			BGl_za2symbolza2z00zztype_cachez00))
																																		{	/* SawBbv/bbv-debug.scm 563 */
																																			return
																																				BGl_string3087z00zzsaw_bbvzd2debugzd2;
																																		}
																																	else
																																		{	/* SawBbv/bbv-debug.scm 563 */
																																			return
																																				BFALSE;
																																		}
																																}
																														}
																												}
																										}
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* assert-failure */
	obj_t BGl_assertzd2failurezd2zzsaw_bbvzd2debugzd2(obj_t BgL_predz00_128,
		BgL_rtl_regz00_bglt BgL_regz00_129, obj_t BgL_polarityz00_130,
		obj_t BgL_locz00_131, obj_t BgL_tagz00_132)
	{
		{	/* SawBbv/bbv-debug.scm 574 */
			{	/* SawBbv/bbv-debug.scm 575 */
				obj_t BgL_debugnamez00_3349;

				BgL_debugnamez00_3349 =
					BGl_regzd2debugnamezd2zzsaw_bbvzd2debugzd2(BgL_regz00_129);
				BGl_assertzd2cntzd2zzsaw_bbvzd2debugzd2 =
					(BGl_assertzd2cntzd2zzsaw_bbvzd2debugzd2 + 1L);
				{	/* SawBbv/bbv-debug.scm 577 */
					bool_t BgL_test3430z00_8124;

					if (STRUCTP(BgL_locz00_131))
						{	/* SawBbv/bbv-debug.scm 577 */
							BgL_test3430z00_8124 =
								(STRUCT_KEY(BgL_locz00_131) == CNST_TABLE_REF(3));
						}
					else
						{	/* SawBbv/bbv-debug.scm 577 */
							BgL_test3430z00_8124 = ((bool_t) 0);
						}
					if (BgL_test3430z00_8124)
						{	/* SawBbv/bbv-debug.scm 579 */
							obj_t BgL_arg2505z00_3351;
							obj_t BgL_arg2506z00_3352;
							obj_t BgL_arg2508z00_3353;

							if (CBOOL(BgL_polarityz00_130))
								{	/* SawBbv/bbv-debug.scm 579 */
									BgL_arg2505z00_3351 = BGl_string3060z00zzsaw_bbvzd2debugzd2;
								}
							else
								{	/* SawBbv/bbv-debug.scm 579 */
									BgL_arg2505z00_3351 = BGl_string3089z00zzsaw_bbvzd2debugzd2;
								}
							BgL_arg2506z00_3352 = STRUCT_REF(BgL_locz00_131, (int) (0L));
							BgL_arg2508z00_3353 = STRUCT_REF(BgL_locz00_131, (int) (1L));
							{	/* SawBbv/bbv-debug.scm 578 */
								obj_t BgL_list2509z00_3354;

								{	/* SawBbv/bbv-debug.scm 578 */
									obj_t BgL_arg2510z00_3355;

									{	/* SawBbv/bbv-debug.scm 578 */
										obj_t BgL_arg2511z00_3356;

										{	/* SawBbv/bbv-debug.scm 578 */
											obj_t BgL_arg2512z00_3357;

											{	/* SawBbv/bbv-debug.scm 578 */
												obj_t BgL_arg2513z00_3358;

												{	/* SawBbv/bbv-debug.scm 578 */
													obj_t BgL_arg2514z00_3359;

													{	/* SawBbv/bbv-debug.scm 578 */
														obj_t BgL_arg2515z00_3360;

														{	/* SawBbv/bbv-debug.scm 578 */
															obj_t BgL_arg2516z00_3361;

															BgL_arg2516z00_3361 =
																MAKE_YOUNG_PAIR(BgL_arg2508z00_3353, BNIL);
															BgL_arg2515z00_3360 =
																MAKE_YOUNG_PAIR(BgL_arg2506z00_3352,
																BgL_arg2516z00_3361);
														}
														BgL_arg2514z00_3359 =
															MAKE_YOUNG_PAIR(BgL_debugnamez00_3349,
															BgL_arg2515z00_3360);
													}
													BgL_arg2513z00_3358 =
														MAKE_YOUNG_PAIR(BgL_debugnamez00_3349,
														BgL_arg2514z00_3359);
												}
												BgL_arg2512z00_3357 =
													MAKE_YOUNG_PAIR(BgL_predz00_128, BgL_arg2513z00_3358);
											}
											BgL_arg2511z00_3356 =
												MAKE_YOUNG_PAIR(BgL_arg2505z00_3351,
												BgL_arg2512z00_3357);
										}
										BgL_arg2510z00_3355 =
											MAKE_YOUNG_PAIR(BINT
											(BGl_assertzd2cntzd2zzsaw_bbvzd2debugzd2),
											BgL_arg2511z00_3356);
									}
									BgL_list2509z00_3354 =
										MAKE_YOUNG_PAIR(BgL_tagz00_132, BgL_arg2510z00_3355);
								}
								return
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string3090z00zzsaw_bbvzd2debugzd2, BgL_list2509z00_3354);
							}
						}
					else
						{	/* SawBbv/bbv-debug.scm 581 */
							obj_t BgL_arg2518z00_3362;

							if (CBOOL(BgL_polarityz00_130))
								{	/* SawBbv/bbv-debug.scm 581 */
									BgL_arg2518z00_3362 = BGl_string3060z00zzsaw_bbvzd2debugzd2;
								}
							else
								{	/* SawBbv/bbv-debug.scm 581 */
									BgL_arg2518z00_3362 = BGl_string3089z00zzsaw_bbvzd2debugzd2;
								}
							{	/* SawBbv/bbv-debug.scm 580 */
								obj_t BgL_list2519z00_3363;

								{	/* SawBbv/bbv-debug.scm 580 */
									obj_t BgL_arg2521z00_3364;

									{	/* SawBbv/bbv-debug.scm 580 */
										obj_t BgL_arg2524z00_3365;

										{	/* SawBbv/bbv-debug.scm 580 */
											obj_t BgL_arg2525z00_3366;

											{	/* SawBbv/bbv-debug.scm 580 */
												obj_t BgL_arg2526z00_3367;

												{	/* SawBbv/bbv-debug.scm 580 */
													obj_t BgL_arg2527z00_3368;

													BgL_arg2527z00_3368 =
														MAKE_YOUNG_PAIR(BgL_debugnamez00_3349, BNIL);
													BgL_arg2526z00_3367 =
														MAKE_YOUNG_PAIR(BgL_debugnamez00_3349,
														BgL_arg2527z00_3368);
												}
												BgL_arg2525z00_3366 =
													MAKE_YOUNG_PAIR(BgL_predz00_128, BgL_arg2526z00_3367);
											}
											BgL_arg2524z00_3365 =
												MAKE_YOUNG_PAIR(BgL_arg2518z00_3362,
												BgL_arg2525z00_3366);
										}
										BgL_arg2521z00_3364 =
											MAKE_YOUNG_PAIR(BINT
											(BGl_assertzd2cntzd2zzsaw_bbvzd2debugzd2),
											BgL_arg2524z00_3365);
									}
									BgL_list2519z00_3363 =
										MAKE_YOUNG_PAIR(BgL_tagz00_132, BgL_arg2521z00_3364);
								}
								return
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string3091z00zzsaw_bbvzd2debugzd2, BgL_list2519z00_3363);
							}
						}
				}
			}
		}

	}



/* assert-context! */
	BGL_EXPORTED_DEF obj_t
		BGl_assertzd2contextz12zc0zzsaw_bbvzd2debugzd2(BgL_blockz00_bglt
		BgL_bz00_135)
	{
		{	/* SawBbv/bbv-debug.scm 589 */
			{
				BgL_rtl_insz00_bglt BgL_iz00_3528;
				bool_t BgL_firstz00_3529;
				BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_3463;
				obj_t BgL_locz00_3464;
				obj_t BgL_cexprz00_3411;
				BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_3412;
				obj_t BgL_locz00_3413;

				BGl_assertzd2blockszd2zzsaw_bbvzd2debugzd2(BgL_bz00_135,
					BGl_string3093z00zzsaw_bbvzd2debugzd2);
				if (((long) CINT(BGl_za2bbvzd2assertza2zd2zzsaw_bbvzd2configzd2) >= 1L))
					{	/* SawBbv/bbv-debug.scm 642 */
						obj_t BgL_g1229z00_3374;
						obj_t BgL_g1230z00_3375;

						{	/* SawBbv/bbv-debug.scm 642 */
							obj_t BgL_list2557z00_3410;

							BgL_list2557z00_3410 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_135), BNIL);
							BgL_g1229z00_3374 = BgL_list2557z00_3410;
						}
						BgL_g1230z00_3375 =
							BGl_makezd2emptyzd2bbsetz00zzsaw_bbvzd2debugzd2();
						{
							obj_t BgL_bsz00_3377;
							obj_t BgL_accz00_3378;

							{
								BgL_blockz00_bglt BgL_auxz00_8163;

								BgL_bsz00_3377 = BgL_g1229z00_3374;
								BgL_accz00_3378 = BgL_g1230z00_3375;
							BgL_zc3z04anonymousza32529ze3z87_3379:
								if (NULLP(BgL_bsz00_3377))
									{	/* SawBbv/bbv-debug.scm 645 */
										BgL_auxz00_8163 = BgL_bz00_135;
									}
								else
									{	/* SawBbv/bbv-debug.scm 647 */
										bool_t BgL_test3436z00_8166;

										{	/* SawBbv/bbv-debug.scm 647 */
											BgL_blockz00_bglt BgL_blockz00_4824;

											BgL_blockz00_4824 =
												((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_3377)));
											{	/* SawBbv/bbv-debug.scm 647 */
												long BgL_arg1724z00_4827;
												obj_t BgL_arg1733z00_4828;

												{
													BgL_blocksz00_bglt BgL_auxz00_8170;

													{
														obj_t BgL_auxz00_8171;

														{	/* SawBbv/bbv-debug.scm 647 */
															BgL_objectz00_bglt BgL_tmpz00_8172;

															BgL_tmpz00_8172 =
																((BgL_objectz00_bglt) BgL_blockz00_4824);
															BgL_auxz00_8171 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_8172);
														}
														BgL_auxz00_8170 =
															((BgL_blocksz00_bglt) BgL_auxz00_8171);
													}
													BgL_arg1724z00_4827 =
														(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_8170))->
														BgL_z52markz52);
												}
												BgL_arg1733z00_4828 =
													STRUCT_REF(BgL_accz00_3378, (int) (0L));
												BgL_test3436z00_8166 =
													(BINT(BgL_arg1724z00_4827) == BgL_arg1733z00_4828);
										}}
										if (BgL_test3436z00_8166)
											{	/* SawBbv/bbv-debug.scm 648 */
												obj_t BgL_arg2536z00_3383;

												BgL_arg2536z00_3383 = CDR(((obj_t) BgL_bsz00_3377));
												{
													obj_t BgL_bsz00_8183;

													BgL_bsz00_8183 = BgL_arg2536z00_3383;
													BgL_bsz00_3377 = BgL_bsz00_8183;
													goto BgL_zc3z04anonymousza32529ze3z87_3379;
												}
											}
										else
											{	/* SawBbv/bbv-debug.scm 650 */
												BgL_blockz00_bglt BgL_i1231z00_3384;

												BgL_i1231z00_3384 =
													((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_3377)));
												if (CBOOL
													(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2))
													{	/* SawBbv/bbv-debug.scm 651 */
														obj_t BgL_arg2537z00_3385;

														BgL_arg2537z00_3385 = CAR(((obj_t) BgL_bsz00_3377));
														BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(
															((BgL_blockz00_bglt) BgL_arg2537z00_3385),
															BGl_string3094z00zzsaw_bbvzd2debugzd2);
													}
												else
													{	/* SawBbv/bbv-debug.scm 651 */
														BFALSE;
													}
												{
													obj_t BgL_firstz00_3389;
													obj_t BgL_accz00_3390;

													{	/* SawBbv/bbv-debug.scm 652 */
														obj_t BgL_arg2538z00_3388;

														BgL_arg2538z00_3388 =
															(((BgL_blockz00_bglt) COBJECT(
																	((BgL_blockz00_bglt) BgL_i1231z00_3384)))->
															BgL_firstz00);
														BgL_firstz00_3389 = BgL_arg2538z00_3388;
														BgL_accz00_3390 = BNIL;
													BgL_zc3z04anonymousza32539ze3z87_3391:
														if (NULLP(BgL_firstz00_3389))
															{	/* SawBbv/bbv-debug.scm 655 */
																obj_t BgL_arg2542z00_3393;
																obj_t BgL_arg2543z00_3394;

																BgL_arg2542z00_3393 =
																	CAR(((obj_t) BgL_bsz00_3377));
																BgL_arg2543z00_3394 =
																	bgl_reverse_bang(BgL_accz00_3390);
																((((BgL_blockz00_bglt) COBJECT(
																				((BgL_blockz00_bglt)
																					BgL_arg2542z00_3393)))->
																		BgL_firstz00) =
																	((obj_t) BgL_arg2543z00_3394), BUNSPEC);
															}
														else
															{	/* SawBbv/bbv-debug.scm 656 */
																obj_t BgL_insz00_3395;

																BgL_insz00_3395 =
																	CAR(((obj_t) BgL_firstz00_3389));
																{	/* SawBbv/bbv-debug.scm 656 */
																	obj_t BgL_assertz00_3396;

																	{	/* SawBbv/bbv-debug.scm 657 */
																		bool_t BgL_arg2549z00_3401;

																		BgL_arg2549z00_3401 =
																			NULLP(BgL_accz00_3390);
																		BgL_iz00_3528 =
																			((BgL_rtl_insz00_bglt) BgL_insz00_3395);
																		BgL_firstz00_3529 = BgL_arg2549z00_3401;
																		{	/* SawBbv/bbv-debug.scm 633 */
																			bool_t BgL_test3439z00_8205;

																			if (BgL_firstz00_3529)
																				{	/* SawBbv/bbv-debug.scm 633 */
																					BgL_test3439z00_8205 = ((bool_t) 1);
																				}
																			else
																				{	/* SawBbv/bbv-debug.scm 633 */
																					BgL_test3439z00_8205 =
																						(
																						(long)
																						CINT
																						(BGl_za2bbvzd2assertza2zd2zzsaw_bbvzd2configzd2)
																						>= 2L);
																				}
																			if (BgL_test3439z00_8205)
																				{	/* SawBbv/bbv-debug.scm 635 */
																					obj_t BgL_assertz00_3533;

																					{	/* SawBbv/bbv-debug.scm 635 */
																						BgL_bbvzd2ctxzd2_bglt
																							BgL_arg2641z00_3538;
																						obj_t BgL_arg2642z00_3539;

																						{
																							BgL_rtl_inszf2bbvzf2_bglt
																								BgL_auxz00_8209;
																							{
																								obj_t BgL_auxz00_8210;

																								{	/* SawBbv/bbv-debug.scm 635 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_8211;
																									BgL_tmpz00_8211 =
																										((BgL_objectz00_bglt)
																										BgL_iz00_3528);
																									BgL_auxz00_8210 =
																										BGL_OBJECT_WIDENING
																										(BgL_tmpz00_8211);
																								}
																								BgL_auxz00_8209 =
																									((BgL_rtl_inszf2bbvzf2_bglt)
																									BgL_auxz00_8210);
																							}
																							BgL_arg2641z00_3538 =
																								(((BgL_rtl_inszf2bbvzf2_bglt)
																									COBJECT(BgL_auxz00_8209))->
																								BgL_ctxz00);
																						}
																						BgL_arg2642z00_3539 =
																							(((BgL_rtl_insz00_bglt) COBJECT(
																									((BgL_rtl_insz00_bglt)
																										BgL_iz00_3528)))->
																							BgL_locz00);
																						BgL_ctxz00_3463 =
																							BgL_arg2641z00_3538;
																						BgL_locz00_3464 =
																							BgL_arg2642z00_3539;
																						{	/* SawBbv/bbv-debug.scm 611 */
																							obj_t BgL_checksz00_3467;

																							{	/* SawBbv/bbv-debug.scm 611 */
																								obj_t BgL_arg2595z00_3471;

																								BgL_arg2595z00_3471 =
																									(((BgL_bbvzd2ctxzd2_bglt)
																										COBJECT(BgL_ctxz00_3463))->
																									BgL_entriesz00);
																								{	/* SawBbv/bbv-debug.scm 612 */
																									obj_t
																										BgL_zc3z04anonymousza32597ze3z87_5296;
																									BgL_zc3z04anonymousza32597ze3z87_5296
																										=
																										MAKE_FX_PROCEDURE
																										(BGl_z62zc3z04anonymousza32597ze3ze5zzsaw_bbvzd2debugzd2,
																										(int) (1L), (int) (1L));
																									PROCEDURE_SET
																										(BgL_zc3z04anonymousza32597ze3z87_5296,
																										(int) (0L),
																										BgL_locz00_3464);
																									{	/* SawBbv/bbv-debug.scm 611 */
																										obj_t BgL_list2596z00_3472;

																										BgL_list2596z00_3472 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2595z00_3471,
																											BNIL);
																										BgL_checksz00_3467 =
																											BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
																											(BgL_zc3z04anonymousza32597ze3z87_5296,
																											BgL_list2596z00_3472);
																							}}}
																							if (NULLP(BgL_checksz00_3467))
																								{	/* SawBbv/bbv-debug.scm 628 */
																									BgL_assertz00_3533 = BFALSE;
																								}
																							else
																								{	/* SawBbv/bbv-debug.scm 629 */
																									obj_t BgL_list2592z00_3469;

																									BgL_list2592z00_3469 =
																										MAKE_YOUNG_PAIR
																										(BgL_checksz00_3467, BNIL);
																									BgL_assertz00_3533 =
																										BGl_formatz00zz__r4_output_6_10_3z00
																										(BGl_string3092z00zzsaw_bbvzd2debugzd2,
																										BgL_list2592z00_3469);
																								}
																						}
																					}
																					if (STRINGP(BgL_assertz00_3533))
																						{	/* SawBbv/bbv-debug.scm 638 */
																							BgL_bbvzd2ctxzd2_bglt
																								BgL_arg2639z00_3536;
																							obj_t BgL_arg2640z00_3537;

																							{
																								BgL_rtl_inszf2bbvzf2_bglt
																									BgL_auxz00_8232;
																								{
																									obj_t BgL_auxz00_8233;

																									{	/* SawBbv/bbv-debug.scm 638 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_8234;
																										BgL_tmpz00_8234 =
																											((BgL_objectz00_bglt)
																											BgL_iz00_3528);
																										BgL_auxz00_8233 =
																											BGL_OBJECT_WIDENING
																											(BgL_tmpz00_8234);
																									}
																									BgL_auxz00_8232 =
																										((BgL_rtl_inszf2bbvzf2_bglt)
																										BgL_auxz00_8233);
																								}
																								BgL_arg2639z00_3536 =
																									(((BgL_rtl_inszf2bbvzf2_bglt)
																										COBJECT(BgL_auxz00_8232))->
																									BgL_ctxz00);
																							}
																							BgL_arg2640z00_3537 =
																								(((BgL_rtl_insz00_bglt) COBJECT(
																										((BgL_rtl_insz00_bglt)
																											BgL_iz00_3528)))->
																								BgL_locz00);
																							{
																								BgL_rtl_insz00_bglt
																									BgL_auxz00_8241;
																								BgL_cexprz00_3411 =
																									BgL_assertz00_3533;
																								BgL_ctxz00_3412 =
																									BgL_arg2639z00_3536;
																								BgL_locz00_3413 =
																									BgL_arg2640z00_3537;
																								{	/* SawBbv/bbv-debug.scm 592 */
																									BgL_rtl_pragmaz00_bglt
																										BgL_prz00_3415;
																									BgL_regsetz00_bglt
																										BgL_esz00_3416;
																									{	/* SawBbv/bbv-debug.scm 592 */
																										BgL_rtl_pragmaz00_bglt
																											BgL_new1217z00_3423;
																										{	/* SawBbv/bbv-debug.scm 592 */
																											BgL_rtl_pragmaz00_bglt
																												BgL_new1216z00_3424;
																											BgL_new1216z00_3424 =
																												(
																												(BgL_rtl_pragmaz00_bglt)
																												BOBJECT(GC_MALLOC(sizeof
																														(struct
																															BgL_rtl_pragmaz00_bgl))));
																											{	/* SawBbv/bbv-debug.scm 592 */
																												long
																													BgL_arg2562z00_3425;
																												BgL_arg2562z00_3425 =
																													BGL_CLASS_NUM
																													(BGl_rtl_pragmaz00zzsaw_defsz00);
																												BGL_OBJECT_CLASS_NUM_SET
																													(((BgL_objectz00_bglt)
																														BgL_new1216z00_3424),
																													BgL_arg2562z00_3425);
																											}
																											BgL_new1217z00_3423 =
																												BgL_new1216z00_3424;
																										}
																										((((BgL_rtl_funz00_bglt)
																													COBJECT((
																															(BgL_rtl_funz00_bglt)
																															BgL_new1217z00_3423)))->
																												BgL_locz00) =
																											((obj_t) BFALSE),
																											BUNSPEC);
																										((((BgL_rtl_pragmaz00_bglt)
																													COBJECT
																													(BgL_new1217z00_3423))->
																												BgL_formatz00) =
																											((obj_t)
																												BgL_cexprz00_3411),
																											BUNSPEC);
																										((((BgL_rtl_pragmaz00_bglt)
																													COBJECT
																													(BgL_new1217z00_3423))->
																												BgL_srfi0z00) =
																											((obj_t)
																												CNST_TABLE_REF(6)),
																											BUNSPEC);
																										BgL_prz00_3415 =
																											BgL_new1217z00_3423;
																									}
																									{	/* SawBbv/bbv-debug.scm 595 */
																										BgL_regsetz00_bglt
																											BgL_new1219z00_3426;
																										{	/* SawBbv/bbv-debug.scm 595 */
																											BgL_regsetz00_bglt
																												BgL_new1218z00_3427;
																											BgL_new1218z00_3427 =
																												((BgL_regsetz00_bglt)
																												BOBJECT(GC_MALLOC(sizeof
																														(struct
																															BgL_regsetz00_bgl))));
																											{	/* SawBbv/bbv-debug.scm 595 */
																												long
																													BgL_arg2563z00_3428;
																												BgL_arg2563z00_3428 =
																													BGL_CLASS_NUM
																													(BGl_regsetz00zzsaw_regsetz00);
																												BGL_OBJECT_CLASS_NUM_SET
																													(((BgL_objectz00_bglt)
																														BgL_new1218z00_3427),
																													BgL_arg2563z00_3428);
																											}
																											BgL_new1219z00_3426 =
																												BgL_new1218z00_3427;
																										}
																										((((BgL_regsetz00_bglt)
																													COBJECT
																													(BgL_new1219z00_3426))->
																												BgL_lengthz00) =
																											((int) (int) (0L)),
																											BUNSPEC);
																										((((BgL_regsetz00_bglt)
																													COBJECT
																													(BgL_new1219z00_3426))->
																												BgL_msiza7eza7) =
																											((int) (int) (0L)),
																											BUNSPEC);
																										((((BgL_regsetz00_bglt)
																													COBJECT
																													(BgL_new1219z00_3426))->
																												BgL_regvz00) =
																											((obj_t)
																												CNST_TABLE_REF(7)),
																											BUNSPEC);
																										((((BgL_regsetz00_bglt)
																													COBJECT
																													(BgL_new1219z00_3426))->
																												BgL_reglz00) =
																											((obj_t) BNIL), BUNSPEC);
																										((((BgL_regsetz00_bglt)
																													COBJECT
																													(BgL_new1219z00_3426))->
																												BgL_stringz00) =
																											((obj_t)
																												BGl_string3060z00zzsaw_bbvzd2debugzd2),
																											BUNSPEC);
																										BgL_esz00_3416 =
																											BgL_new1219z00_3426;
																									}
																									{	/* SawBbv/bbv-debug.scm 596 */
																										BgL_rtl_insz00_bglt
																											BgL_new1223z00_3417;
																										{	/* SawBbv/bbv-debug.scm 597 */
																											BgL_rtl_insz00_bglt
																												BgL_tmp1221z00_3418;
																											BgL_rtl_inszf2bbvzf2_bglt
																												BgL_wide1222z00_3419;
																											{
																												BgL_rtl_insz00_bglt
																													BgL_auxz00_8263;
																												{	/* SawBbv/bbv-debug.scm 597 */
																													BgL_rtl_insz00_bglt
																														BgL_new1220z00_3421;
																													BgL_new1220z00_3421 =
																														(
																														(BgL_rtl_insz00_bglt)
																														BOBJECT(GC_MALLOC
																															(sizeof(struct
																																	BgL_rtl_insz00_bgl))));
																													{	/* SawBbv/bbv-debug.scm 597 */
																														long
																															BgL_arg2561z00_3422;
																														BgL_arg2561z00_3422
																															=
																															BGL_CLASS_NUM
																															(BGl_rtl_insz00zzsaw_defsz00);
																														BGL_OBJECT_CLASS_NUM_SET
																															(((BgL_objectz00_bglt) BgL_new1220z00_3421), BgL_arg2561z00_3422);
																													}
																													{	/* SawBbv/bbv-debug.scm 597 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_8268;
																														BgL_tmpz00_8268 =
																															(
																															(BgL_objectz00_bglt)
																															BgL_new1220z00_3421);
																														BGL_OBJECT_WIDENING_SET
																															(BgL_tmpz00_8268,
																															BFALSE);
																													}
																													((BgL_objectz00_bglt)
																														BgL_new1220z00_3421);
																													BgL_auxz00_8263 =
																														BgL_new1220z00_3421;
																												}
																												BgL_tmp1221z00_3418 =
																													((BgL_rtl_insz00_bglt)
																													BgL_auxz00_8263);
																											}
																											BgL_wide1222z00_3419 =
																												(
																												(BgL_rtl_inszf2bbvzf2_bglt)
																												BOBJECT(GC_MALLOC(sizeof
																														(struct
																															BgL_rtl_inszf2bbvzf2_bgl))));
																											{	/* SawBbv/bbv-debug.scm 597 */
																												obj_t BgL_auxz00_8276;
																												BgL_objectz00_bglt
																													BgL_tmpz00_8274;
																												BgL_auxz00_8276 =
																													((obj_t)
																													BgL_wide1222z00_3419);
																												BgL_tmpz00_8274 =
																													((BgL_objectz00_bglt)
																													BgL_tmp1221z00_3418);
																												BGL_OBJECT_WIDENING_SET
																													(BgL_tmpz00_8274,
																													BgL_auxz00_8276);
																											}
																											((BgL_objectz00_bglt)
																												BgL_tmp1221z00_3418);
																											{	/* SawBbv/bbv-debug.scm 597 */
																												long
																													BgL_arg2560z00_3420;
																												BgL_arg2560z00_3420 =
																													BGL_CLASS_NUM
																													(BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2);
																												BGL_OBJECT_CLASS_NUM_SET
																													(((BgL_objectz00_bglt)
																														BgL_tmp1221z00_3418),
																													BgL_arg2560z00_3420);
																											}
																											BgL_new1223z00_3417 =
																												((BgL_rtl_insz00_bglt)
																												BgL_tmp1221z00_3418);
																										}
																										((((BgL_rtl_insz00_bglt)
																													COBJECT((
																															(BgL_rtl_insz00_bglt)
																															BgL_new1223z00_3417)))->
																												BgL_locz00) =
																											((obj_t) BgL_locz00_3413),
																											BUNSPEC);
																										((((BgL_rtl_insz00_bglt)
																													COBJECT((
																															(BgL_rtl_insz00_bglt)
																															BgL_new1223z00_3417)))->
																												BgL_z52spillz52) =
																											((obj_t) BNIL), BUNSPEC);
																										((((BgL_rtl_insz00_bglt)
																													COBJECT((
																															(BgL_rtl_insz00_bglt)
																															BgL_new1223z00_3417)))->
																												BgL_destz00) =
																											((obj_t) BFALSE),
																											BUNSPEC);
																										((((BgL_rtl_insz00_bglt)
																													COBJECT((
																															(BgL_rtl_insz00_bglt)
																															BgL_new1223z00_3417)))->
																												BgL_funz00) =
																											((BgL_rtl_funz00_bglt) (
																													(BgL_rtl_funz00_bglt)
																													BgL_prz00_3415)),
																											BUNSPEC);
																										((((BgL_rtl_insz00_bglt)
																													COBJECT((
																															(BgL_rtl_insz00_bglt)
																															BgL_new1223z00_3417)))->
																												BgL_argsz00) =
																											((obj_t) BNIL), BUNSPEC);
																										{
																											BgL_rtl_inszf2bbvzf2_bglt
																												BgL_auxz00_8295;
																											{
																												obj_t BgL_auxz00_8296;

																												{	/* SawBbv/bbv-debug.scm 602 */
																													BgL_objectz00_bglt
																														BgL_tmpz00_8297;
																													BgL_tmpz00_8297 =
																														(
																														(BgL_objectz00_bglt)
																														BgL_new1223z00_3417);
																													BgL_auxz00_8296 =
																														BGL_OBJECT_WIDENING
																														(BgL_tmpz00_8297);
																												}
																												BgL_auxz00_8295 =
																													(
																													(BgL_rtl_inszf2bbvzf2_bglt)
																													BgL_auxz00_8296);
																											}
																											((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_8295))->BgL_defz00) = ((obj_t) ((obj_t) BgL_esz00_3416)), BUNSPEC);
																										}
																										{
																											BgL_rtl_inszf2bbvzf2_bglt
																												BgL_auxz00_8303;
																											{
																												obj_t BgL_auxz00_8304;

																												{	/* SawBbv/bbv-debug.scm 601 */
																													BgL_objectz00_bglt
																														BgL_tmpz00_8305;
																													BgL_tmpz00_8305 =
																														(
																														(BgL_objectz00_bglt)
																														BgL_new1223z00_3417);
																													BgL_auxz00_8304 =
																														BGL_OBJECT_WIDENING
																														(BgL_tmpz00_8305);
																												}
																												BgL_auxz00_8303 =
																													(
																													(BgL_rtl_inszf2bbvzf2_bglt)
																													BgL_auxz00_8304);
																											}
																											((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_8303))->BgL_outz00) = ((obj_t) ((obj_t) BgL_esz00_3416)), BUNSPEC);
																										}
																										{
																											BgL_rtl_inszf2bbvzf2_bglt
																												BgL_auxz00_8311;
																											{
																												obj_t BgL_auxz00_8312;

																												{	/* SawBbv/bbv-debug.scm 600 */
																													BgL_objectz00_bglt
																														BgL_tmpz00_8313;
																													BgL_tmpz00_8313 =
																														(
																														(BgL_objectz00_bglt)
																														BgL_new1223z00_3417);
																													BgL_auxz00_8312 =
																														BGL_OBJECT_WIDENING
																														(BgL_tmpz00_8313);
																												}
																												BgL_auxz00_8311 =
																													(
																													(BgL_rtl_inszf2bbvzf2_bglt)
																													BgL_auxz00_8312);
																											}
																											((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_8311))->BgL_inz00) = ((obj_t) ((obj_t) BgL_esz00_3416)), BUNSPEC);
																										}
																										{
																											BgL_rtl_inszf2bbvzf2_bglt
																												BgL_auxz00_8319;
																											{
																												obj_t BgL_auxz00_8320;

																												{	/* SawBbv/bbv-debug.scm 599 */
																													BgL_objectz00_bglt
																														BgL_tmpz00_8321;
																													BgL_tmpz00_8321 =
																														(
																														(BgL_objectz00_bglt)
																														BgL_new1223z00_3417);
																													BgL_auxz00_8320 =
																														BGL_OBJECT_WIDENING
																														(BgL_tmpz00_8321);
																												}
																												BgL_auxz00_8319 =
																													(
																													(BgL_rtl_inszf2bbvzf2_bglt)
																													BgL_auxz00_8320);
																											}
																											((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_8319))->BgL_ctxz00) = ((BgL_bbvzd2ctxzd2_bglt) BgL_ctxz00_3412), BUNSPEC);
																										}
																										{
																											BgL_rtl_inszf2bbvzf2_bglt
																												BgL_auxz00_8326;
																											{
																												obj_t BgL_auxz00_8327;

																												{	/* SawBbv/bbv-debug.scm 599 */
																													BgL_objectz00_bglt
																														BgL_tmpz00_8328;
																													BgL_tmpz00_8328 =
																														(
																														(BgL_objectz00_bglt)
																														BgL_new1223z00_3417);
																													BgL_auxz00_8327 =
																														BGL_OBJECT_WIDENING
																														(BgL_tmpz00_8328);
																												}
																												BgL_auxz00_8326 =
																													(
																													(BgL_rtl_inszf2bbvzf2_bglt)
																													BgL_auxz00_8327);
																											}
																											((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_8326))->BgL_z52hashz52) = ((obj_t) BFALSE), BUNSPEC);
																										}
																										BgL_auxz00_8241 =
																											BgL_new1223z00_3417;
																								}}
																								BgL_assertz00_3396 =
																									((obj_t) BgL_auxz00_8241);
																						}}
																					else
																						{	/* SawBbv/bbv-debug.scm 636 */
																							BgL_assertz00_3396 = BFALSE;
																						}
																				}
																			else
																				{	/* SawBbv/bbv-debug.scm 633 */
																					BgL_assertz00_3396 = BFALSE;
																				}
																		}
																	}
																	{	/* SawBbv/bbv-debug.scm 657 */

																		{	/* SawBbv/bbv-debug.scm 659 */
																			obj_t BgL_arg2545z00_3398;
																			obj_t BgL_arg2546z00_3399;

																			BgL_arg2545z00_3398 =
																				CDR(((obj_t) BgL_firstz00_3389));
																			if (CBOOL(BgL_assertz00_3396))
																				{	/* SawBbv/bbv-debug.scm 661 */
																					obj_t BgL_arg2548z00_3400;

																					BgL_arg2548z00_3400 =
																						MAKE_YOUNG_PAIR(BgL_assertz00_3396,
																						BgL_accz00_3390);
																					BgL_arg2546z00_3399 =
																						MAKE_YOUNG_PAIR(BgL_insz00_3395,
																						BgL_arg2548z00_3400);
																				}
																			else
																				{	/* SawBbv/bbv-debug.scm 660 */
																					BgL_arg2546z00_3399 =
																						MAKE_YOUNG_PAIR(BgL_insz00_3395,
																						BgL_accz00_3390);
																				}
																			{
																				obj_t BgL_accz00_8343;
																				obj_t BgL_firstz00_8342;

																				BgL_firstz00_8342 = BgL_arg2545z00_3398;
																				BgL_accz00_8343 = BgL_arg2546z00_3399;
																				BgL_accz00_3390 = BgL_accz00_8343;
																				BgL_firstz00_3389 = BgL_firstz00_8342;
																				goto
																					BgL_zc3z04anonymousza32539ze3z87_3391;
																			}
																		}
																	}
																}
															}
													}
												}
												{	/* SawBbv/bbv-debug.scm 663 */
													obj_t BgL_arg2551z00_3403;
													obj_t BgL_arg2552z00_3404;

													{	/* SawBbv/bbv-debug.scm 663 */
														obj_t BgL_arg2553z00_3405;
														obj_t BgL_arg2554z00_3406;

														BgL_arg2553z00_3405 =
															(((BgL_blockz00_bglt) COBJECT(
																	((BgL_blockz00_bglt) BgL_i1231z00_3384)))->
															BgL_succsz00);
														BgL_arg2554z00_3406 = CDR(((obj_t) BgL_bsz00_3377));
														BgL_arg2551z00_3403 =
															BGl_appendzd221011zd2zzsaw_bbvzd2debugzd2
															(BgL_arg2553z00_3405, BgL_arg2554z00_3406);
													}
													{	/* SawBbv/bbv-debug.scm 663 */
														obj_t BgL_arg2555z00_3407;

														BgL_arg2555z00_3407 = CAR(((obj_t) BgL_bsz00_3377));
														BgL_arg2552z00_3404 =
															BGl_bbsetzd2conszd2zzsaw_bbvzd2debugzd2(
															((BgL_blockz00_bglt) BgL_arg2555z00_3407),
															BgL_accz00_3378);
													}
													{
														obj_t BgL_accz00_8354;
														obj_t BgL_bsz00_8353;

														BgL_bsz00_8353 = BgL_arg2551z00_3403;
														BgL_accz00_8354 = BgL_arg2552z00_3404;
														BgL_accz00_3378 = BgL_accz00_8354;
														BgL_bsz00_3377 = BgL_bsz00_8353;
														goto BgL_zc3z04anonymousza32529ze3z87_3379;
													}
												}
											}
									}
								return ((obj_t) BgL_auxz00_8163);
							}
						}
					}
				else
					{	/* SawBbv/bbv-debug.scm 641 */
						return ((obj_t) BgL_bz00_135);
					}
			}
		}

	}



/* &assert-context! */
	obj_t BGl_z62assertzd2contextz12za2zzsaw_bbvzd2debugzd2(obj_t BgL_envz00_5297,
		obj_t BgL_bz00_5298)
	{
		{	/* SawBbv/bbv-debug.scm 589 */
			return
				BGl_assertzd2contextz12zc0zzsaw_bbvzd2debugzd2(
				((BgL_blockz00_bglt) BgL_bz00_5298));
		}

	}



/* &<@anonymous:2597> */
	obj_t BGl_z62zc3z04anonymousza32597ze3ze5zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5299, obj_t BgL_ez00_5301)
	{
		{	/* SawBbv/bbv-debug.scm 611 */
			{	/* SawBbv/bbv-debug.scm 612 */
				obj_t BgL_locz00_5300;

				BgL_locz00_5300 = PROCEDURE_REF(BgL_envz00_5299, (int) (0L));
				{
					obj_t BgL_typesz00_5709;

					{	/* SawBbv/bbv-debug.scm 613 */
						BgL_rtl_regz00_bglt BgL_i1226z00_5727;

						BgL_i1226z00_5727 =
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_5301)))->BgL_regz00);
						{	/* SawBbv/bbv-debug.scm 614 */
							bool_t BgL_test3444z00_8363;

							if (BGl_bigloozd2typezf3z21zztype_typez00(
									(((BgL_rtl_regz00_bglt) COBJECT(BgL_i1226z00_5727))->
										BgL_typez00)))
								{	/* SawBbv/bbv-debug.scm 615 */
									bool_t BgL_test3446z00_8367;

									{	/* SawBbv/bbv-debug.scm 615 */
										BgL_typez00_bglt BgL_arg2633z00_5728;

										BgL_arg2633z00_5728 =
											(((BgL_rtl_regz00_bglt) COBJECT(BgL_i1226z00_5727))->
											BgL_typez00);
										BgL_test3446z00_8367 =
											(((obj_t) BgL_arg2633z00_5728) ==
											BGl_za2procedurezd2elza2zd2zztype_cachez00);
									}
									if (BgL_test3446z00_8367)
										{	/* SawBbv/bbv-debug.scm 615 */
											BgL_test3444z00_8363 = ((bool_t) 0);
										}
									else
										{	/* SawBbv/bbv-debug.scm 616 */
											bool_t BgL_test3447z00_8371;

											{	/* SawBbv/bbv-debug.scm 616 */
												BgL_typez00_bglt BgL_arg2632z00_5729;

												BgL_arg2632z00_5729 =
													(((BgL_rtl_regz00_bglt) COBJECT(BgL_i1226z00_5727))->
													BgL_typez00);
												BgL_test3447z00_8371 =
													(((obj_t) BgL_arg2632z00_5729) ==
													BGl_za2procedureza2z00zztype_cachez00);
											}
											if (BgL_test3447z00_8371)
												{	/* SawBbv/bbv-debug.scm 616 */
													BgL_test3444z00_8363 = ((bool_t) 0);
												}
											else
												{	/* SawBbv/bbv-debug.scm 616 */
													BgL_test3444z00_8363 = ((bool_t) 1);
												}
										}
								}
							else
								{	/* SawBbv/bbv-debug.scm 614 */
									BgL_test3444z00_8363 = ((bool_t) 0);
								}
							if (BgL_test3444z00_8363)
								{	/* SawBbv/bbv-debug.scm 617 */
									obj_t BgL_predsz00_5730;

									{	/* SawBbv/bbv-debug.scm 617 */
										obj_t BgL_arg2631z00_5731;

										BgL_arg2631z00_5731 =
											(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
													((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_5301)))->
											BgL_typesz00);
										BgL_typesz00_5709 = BgL_arg2631z00_5731;
										{	/* SawBbv/bbv-debug.scm 607 */
											obj_t BgL_hook1666z00_5710;

											BgL_hook1666z00_5710 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
											{	/* SawBbv/bbv-debug.scm 607 */
												obj_t BgL_g1667z00_5711;

												{	/* SawBbv/bbv-debug.scm 607 */
													obj_t BgL_head1658z00_5712;

													BgL_head1658z00_5712 =
														MAKE_YOUNG_PAIR
														(BGl_typezd2predicatezd2zzsaw_bbvzd2debugzd2(CAR
															(BgL_typesz00_5709)), BNIL);
													{	/* SawBbv/bbv-debug.scm 607 */
														obj_t BgL_g1661z00_5713;

														BgL_g1661z00_5713 = CDR(BgL_typesz00_5709);
														{
															obj_t BgL_l1656z00_5715;
															obj_t BgL_tail1659z00_5716;

															BgL_l1656z00_5715 = BgL_g1661z00_5713;
															BgL_tail1659z00_5716 = BgL_head1658z00_5712;
														BgL_zc3z04anonymousza32581ze3z87_5714:
															if (NULLP(BgL_l1656z00_5715))
																{	/* SawBbv/bbv-debug.scm 607 */
																	BgL_g1667z00_5711 = BgL_head1658z00_5712;
																}
															else
																{	/* SawBbv/bbv-debug.scm 607 */
																	obj_t BgL_newtail1660z00_5717;

																	{	/* SawBbv/bbv-debug.scm 607 */
																		obj_t BgL_arg2585z00_5718;

																		BgL_arg2585z00_5718 =
																			BGl_typezd2predicatezd2zzsaw_bbvzd2debugzd2
																			(CAR(((obj_t) BgL_l1656z00_5715)));
																		BgL_newtail1660z00_5717 =
																			MAKE_YOUNG_PAIR(BgL_arg2585z00_5718,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1659z00_5716,
																		BgL_newtail1660z00_5717);
																	{	/* SawBbv/bbv-debug.scm 607 */
																		obj_t BgL_arg2584z00_5719;

																		BgL_arg2584z00_5719 =
																			CDR(((obj_t) BgL_l1656z00_5715));
																		{
																			obj_t BgL_tail1659z00_8392;
																			obj_t BgL_l1656z00_8391;

																			BgL_l1656z00_8391 = BgL_arg2584z00_5719;
																			BgL_tail1659z00_8392 =
																				BgL_newtail1660z00_5717;
																			BgL_tail1659z00_5716 =
																				BgL_tail1659z00_8392;
																			BgL_l1656z00_5715 = BgL_l1656z00_8391;
																			goto
																				BgL_zc3z04anonymousza32581ze3z87_5714;
																		}
																	}
																}
														}
													}
												}
												{
													obj_t BgL_l1663z00_5721;
													obj_t BgL_h1664z00_5722;

													BgL_l1663z00_5721 = BgL_g1667z00_5711;
													BgL_h1664z00_5722 = BgL_hook1666z00_5710;
												BgL_zc3z04anonymousza32566ze3z87_5720:
													if (NULLP(BgL_l1663z00_5721))
														{	/* SawBbv/bbv-debug.scm 607 */
															BgL_predsz00_5730 = CDR(BgL_hook1666z00_5710);
														}
													else
														{	/* SawBbv/bbv-debug.scm 607 */
															if (CBOOL(CAR(((obj_t) BgL_l1663z00_5721))))
																{	/* SawBbv/bbv-debug.scm 607 */
																	obj_t BgL_nh1665z00_5723;

																	{	/* SawBbv/bbv-debug.scm 607 */
																		obj_t BgL_arg2578z00_5724;

																		BgL_arg2578z00_5724 =
																			CAR(((obj_t) BgL_l1663z00_5721));
																		BgL_nh1665z00_5723 =
																			MAKE_YOUNG_PAIR(BgL_arg2578z00_5724,
																			BNIL);
																	}
																	SET_CDR(BgL_h1664z00_5722,
																		BgL_nh1665z00_5723);
																	{	/* SawBbv/bbv-debug.scm 607 */
																		obj_t BgL_arg2572z00_5725;

																		BgL_arg2572z00_5725 =
																			CDR(((obj_t) BgL_l1663z00_5721));
																		{
																			obj_t BgL_h1664z00_8407;
																			obj_t BgL_l1663z00_8406;

																			BgL_l1663z00_8406 = BgL_arg2572z00_5725;
																			BgL_h1664z00_8407 = BgL_nh1665z00_5723;
																			BgL_h1664z00_5722 = BgL_h1664z00_8407;
																			BgL_l1663z00_5721 = BgL_l1663z00_8406;
																			goto
																				BgL_zc3z04anonymousza32566ze3z87_5720;
																		}
																	}
																}
															else
																{	/* SawBbv/bbv-debug.scm 607 */
																	obj_t BgL_arg2579z00_5726;

																	BgL_arg2579z00_5726 =
																		CDR(((obj_t) BgL_l1663z00_5721));
																	{
																		obj_t BgL_l1663z00_8410;

																		BgL_l1663z00_8410 = BgL_arg2579z00_5726;
																		BgL_l1663z00_5721 = BgL_l1663z00_8410;
																		goto BgL_zc3z04anonymousza32566ze3z87_5720;
																	}
																}
														}
												}
											}
										}
									}
									if (PAIRP(BgL_predsz00_5730))
										{	/* SawBbv/bbv-debug.scm 620 */
											obj_t BgL_arg2612z00_5732;

											{	/* SawBbv/bbv-debug.scm 620 */
												obj_t BgL_head1670z00_5733;

												BgL_head1670z00_5733 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												{
													obj_t BgL_l1668z00_5735;
													obj_t BgL_tail1671z00_5736;

													BgL_l1668z00_5735 = BgL_predsz00_5730;
													BgL_tail1671z00_5736 = BgL_head1670z00_5733;
												BgL_zc3z04anonymousza32615ze3z87_5734:
													if (NULLP(BgL_l1668z00_5735))
														{	/* SawBbv/bbv-debug.scm 620 */
															BgL_arg2612z00_5732 = CDR(BgL_head1670z00_5733);
														}
													else
														{	/* SawBbv/bbv-debug.scm 620 */
															obj_t BgL_newtail1672z00_5737;

															{	/* SawBbv/bbv-debug.scm 620 */
																obj_t BgL_arg2618z00_5738;

																{	/* SawBbv/bbv-debug.scm 620 */
																	obj_t BgL_pz00_5739;

																	BgL_pz00_5739 =
																		CAR(((obj_t) BgL_l1668z00_5735));
																	{	/* SawBbv/bbv-debug.scm 622 */
																		obj_t BgL_arg2619z00_5740;
																		obj_t BgL_arg2620z00_5741;
																		obj_t BgL_arg2621z00_5742;

																		if (
																			(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
																						((BgL_bbvzd2ctxentryzd2_bglt)
																							BgL_ez00_5301)))->
																				BgL_polarityz00))
																			{	/* SawBbv/bbv-debug.scm 622 */
																				BgL_arg2619z00_5740 =
																					BGl_string3060z00zzsaw_bbvzd2debugzd2;
																			}
																		else
																			{	/* SawBbv/bbv-debug.scm 622 */
																				BgL_arg2619z00_5740 =
																					BGl_string3089z00zzsaw_bbvzd2debugzd2;
																			}
																		{	/* SawBbv/bbv-debug.scm 624 */
																			BgL_rtl_regz00_bglt BgL_arg2628z00_5743;

																			BgL_arg2628z00_5743 =
																				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
																						((BgL_bbvzd2ctxentryzd2_bglt)
																							BgL_ez00_5301)))->BgL_regz00);
																			BgL_arg2620z00_5741 =
																				BGl_regzd2debugnamezd2zzsaw_bbvzd2debugzd2
																				(BgL_arg2628z00_5743);
																		}
																		{	/* SawBbv/bbv-debug.scm 625 */
																			BgL_rtl_regz00_bglt BgL_arg2629z00_5744;
																			bool_t BgL_arg2630z00_5745;

																			BgL_arg2629z00_5744 =
																				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
																						((BgL_bbvzd2ctxentryzd2_bglt)
																							BgL_ez00_5301)))->BgL_regz00);
																			BgL_arg2630z00_5745 =
																				(((BgL_bbvzd2ctxentryzd2_bglt)
																					COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																							BgL_ez00_5301)))->
																				BgL_polarityz00);
																			{	/* SawBbv/bbv-debug.scm 625 */

																				BgL_arg2621z00_5742 =
																					BGl_assertzd2failurezd2zzsaw_bbvzd2debugzd2
																					(BgL_pz00_5739, BgL_arg2629z00_5744,
																					BBOOL(BgL_arg2630z00_5745),
																					BgL_locz00_5300,
																					BGl_string3088z00zzsaw_bbvzd2debugzd2);
																			}
																		}
																		{	/* SawBbv/bbv-debug.scm 621 */
																			obj_t BgL_list2622z00_5746;

																			{	/* SawBbv/bbv-debug.scm 621 */
																				obj_t BgL_arg2623z00_5747;

																				{	/* SawBbv/bbv-debug.scm 621 */
																					obj_t BgL_arg2624z00_5748;

																					{	/* SawBbv/bbv-debug.scm 621 */
																						obj_t BgL_arg2626z00_5749;

																						BgL_arg2626z00_5749 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2621z00_5742, BNIL);
																						BgL_arg2624z00_5748 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2620z00_5741,
																							BgL_arg2626z00_5749);
																					}
																					BgL_arg2623z00_5747 =
																						MAKE_YOUNG_PAIR(BgL_pz00_5739,
																						BgL_arg2624z00_5748);
																				}
																				BgL_list2622z00_5746 =
																					MAKE_YOUNG_PAIR(BgL_arg2619z00_5740,
																					BgL_arg2623z00_5747);
																			}
																			BgL_arg2618z00_5738 =
																				BGl_formatz00zz__r4_output_6_10_3z00
																				(BGl_string3095z00zzsaw_bbvzd2debugzd2,
																				BgL_list2622z00_5746);
																		}
																	}
																}
																BgL_newtail1672z00_5737 =
																	MAKE_YOUNG_PAIR(BgL_arg2618z00_5738, BNIL);
															}
															SET_CDR(BgL_tail1671z00_5736,
																BgL_newtail1672z00_5737);
															{	/* SawBbv/bbv-debug.scm 620 */
																obj_t BgL_arg2617z00_5750;

																BgL_arg2617z00_5750 =
																	CDR(((obj_t) BgL_l1668z00_5735));
																{
																	obj_t BgL_tail1671z00_8441;
																	obj_t BgL_l1668z00_8440;

																	BgL_l1668z00_8440 = BgL_arg2617z00_5750;
																	BgL_tail1671z00_8441 =
																		BgL_newtail1672z00_5737;
																	BgL_tail1671z00_5736 = BgL_tail1671z00_8441;
																	BgL_l1668z00_5735 = BgL_l1668z00_8440;
																	goto BgL_zc3z04anonymousza32615ze3z87_5734;
																}
															}
														}
												}
											}
											{	/* SawBbv/bbv-debug.scm 619 */
												obj_t BgL_list2613z00_5751;

												BgL_list2613z00_5751 =
													MAKE_YOUNG_PAIR(BgL_arg2612z00_5732, BNIL);
												return
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string3096z00zzsaw_bbvzd2debugzd2,
													BgL_list2613z00_5751);
											}
										}
									else
										{	/* SawBbv/bbv-debug.scm 618 */
											return BFALSE;
										}
								}
							else
								{	/* SawBbv/bbv-debug.scm 614 */
									return BFALSE;
								}
						}
					}
				}
			}
		}

	}



/* rtl-assert-reg-type */
	BGL_EXPORTED_DEF obj_t
		BGl_rtlzd2assertzd2regzd2typezd2zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_regz00_136, BgL_typez00_bglt BgL_typez00_137,
		bool_t BgL_polarityz00_138, BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_139,
		obj_t BgL_locz00_140, obj_t BgL_tagz00_141)
	{
		{	/* SawBbv/bbv-debug.scm 669 */
			{	/* SawBbv/bbv-debug.scm 670 */
				obj_t BgL_predz00_3544;

				BgL_predz00_3544 =
					BGl_typezd2predicatezd2zzsaw_bbvzd2debugzd2(
					((obj_t) BgL_typez00_137));
				if (CBOOL(BgL_predz00_3544))
					{	/* SawBbv/bbv-debug.scm 672 */
						obj_t BgL_debugnamez00_3545;

						BgL_debugnamez00_3545 =
							BGl_regzd2debugnamezd2zzsaw_bbvzd2debugzd2(BgL_regz00_136);
						{	/* SawBbv/bbv-debug.scm 673 */
							obj_t BgL_cexprz00_3546;

							{	/* SawBbv/bbv-debug.scm 674 */
								obj_t BgL_arg2645z00_3551;

								{	/* SawBbv/bbv-debug.scm 677 */

									BgL_arg2645z00_3551 =
										BGl_assertzd2failurezd2zzsaw_bbvzd2debugzd2
										(BgL_predz00_3544, BgL_regz00_136, BgL_locz00_140,
										BgL_tagz00_141, BGl_string3088z00zzsaw_bbvzd2debugzd2);
								}
								{	/* SawBbv/bbv-debug.scm 673 */
									obj_t BgL_list2646z00_3552;

									{	/* SawBbv/bbv-debug.scm 673 */
										obj_t BgL_arg2647z00_3553;

										{	/* SawBbv/bbv-debug.scm 673 */
											obj_t BgL_arg2648z00_3554;

											{	/* SawBbv/bbv-debug.scm 673 */
												obj_t BgL_arg2649z00_3555;

												BgL_arg2649z00_3555 =
													MAKE_YOUNG_PAIR(BgL_arg2645z00_3551, BNIL);
												BgL_arg2648z00_3554 =
													MAKE_YOUNG_PAIR(BgL_debugnamez00_3545,
													BgL_arg2649z00_3555);
											}
											BgL_arg2647z00_3553 =
												MAKE_YOUNG_PAIR(BgL_predz00_3544, BgL_arg2648z00_3554);
										}
										{	/* SawBbv/bbv-debug.scm 673 */
											obj_t BgL_tmpz00_8453;

											if (BgL_polarityz00_138)
												{	/* SawBbv/bbv-debug.scm 674 */
													BgL_tmpz00_8453 =
														BGl_string3060z00zzsaw_bbvzd2debugzd2;
												}
											else
												{	/* SawBbv/bbv-debug.scm 674 */
													BgL_tmpz00_8453 =
														BGl_string3089z00zzsaw_bbvzd2debugzd2;
												}
											BgL_list2646z00_3552 =
												MAKE_YOUNG_PAIR(BgL_tmpz00_8453, BgL_arg2647z00_3553);
										}
									}
									BgL_cexprz00_3546 =
										BGl_formatz00zz__r4_output_6_10_3z00
										(BGl_string3097z00zzsaw_bbvzd2debugzd2,
										BgL_list2646z00_3552);
								}
							}
							{	/* SawBbv/bbv-debug.scm 678 */
								BgL_rtl_pragmaz00_bglt BgL_new1235z00_3547;

								{	/* SawBbv/bbv-debug.scm 678 */
									BgL_rtl_pragmaz00_bglt BgL_new1234z00_3548;

									BgL_new1234z00_3548 =
										((BgL_rtl_pragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_rtl_pragmaz00_bgl))));
									{	/* SawBbv/bbv-debug.scm 678 */
										long BgL_arg2643z00_3549;

										BgL_arg2643z00_3549 =
											BGL_CLASS_NUM(BGl_rtl_pragmaz00zzsaw_defsz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1234z00_3548),
											BgL_arg2643z00_3549);
									}
									BgL_new1235z00_3547 = BgL_new1234z00_3548;
								}
								((((BgL_rtl_funz00_bglt) COBJECT(
												((BgL_rtl_funz00_bglt) BgL_new1235z00_3547)))->
										BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_new1235z00_3547))->
										BgL_formatz00) = ((obj_t) BgL_cexprz00_3546), BUNSPEC);
								((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_new1235z00_3547))->
										BgL_srfi0z00) = ((obj_t) CNST_TABLE_REF(6)), BUNSPEC);
								return ((obj_t) BgL_new1235z00_3547);
							}
						}
					}
				else
					{	/* SawBbv/bbv-debug.scm 671 */
						{	/* SawBbv/bbv-debug.scm 682 */
							obj_t BgL_arg2650z00_3561;
							obj_t BgL_arg2651z00_3562;
							bool_t BgL_arg2653z00_3563;

							{	/* SawBbv/bbv-debug.scm 682 */
								obj_t BgL_tmpz00_8467;

								BgL_tmpz00_8467 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_arg2650z00_3561 =
									BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_8467);
							}
							BgL_arg2651z00_3562 =
								BGl_shapez00zztools_shapez00(((obj_t) BgL_typez00_137));
							BgL_arg2653z00_3563 =
								(((obj_t) BgL_typez00_137) == BGl_za2bnilza2z00zztype_cachez00);
							{	/* SawBbv/bbv-debug.scm 682 */
								obj_t BgL_list2654z00_3564;

								{	/* SawBbv/bbv-debug.scm 682 */
									obj_t BgL_arg2656z00_3565;

									{	/* SawBbv/bbv-debug.scm 682 */
										obj_t BgL_arg2657z00_3566;

										{	/* SawBbv/bbv-debug.scm 682 */
											obj_t BgL_arg2658z00_3567;

											{	/* SawBbv/bbv-debug.scm 682 */
												obj_t BgL_arg2659z00_3568;

												{	/* SawBbv/bbv-debug.scm 682 */
													obj_t BgL_arg2660z00_3569;

													{	/* SawBbv/bbv-debug.scm 682 */
														obj_t BgL_arg2662z00_3570;

														{	/* SawBbv/bbv-debug.scm 682 */
															obj_t BgL_arg2664z00_3571;

															BgL_arg2664z00_3571 =
																MAKE_YOUNG_PAIR(BBOOL(BgL_arg2653z00_3563),
																BNIL);
															BgL_arg2662z00_3570 =
																MAKE_YOUNG_PAIR
																(BGl_string3051z00zzsaw_bbvzd2debugzd2,
																BgL_arg2664z00_3571);
														}
														BgL_arg2660z00_3569 =
															MAKE_YOUNG_PAIR(BgL_arg2651z00_3562,
															BgL_arg2662z00_3570);
													}
													BgL_arg2659z00_3568 =
														MAKE_YOUNG_PAIR
														(BGl_string3098z00zzsaw_bbvzd2debugzd2,
														BgL_arg2660z00_3569);
												}
												BgL_arg2658z00_3567 =
													MAKE_YOUNG_PAIR(BGl_string3001z00zzsaw_bbvzd2debugzd2,
													BgL_arg2659z00_3568);
											}
											BgL_arg2657z00_3566 =
												MAKE_YOUNG_PAIR(BINT(682L), BgL_arg2658z00_3567);
										}
										BgL_arg2656z00_3565 =
											MAKE_YOUNG_PAIR(BGl_string3002z00zzsaw_bbvzd2debugzd2,
											BgL_arg2657z00_3566);
									}
									BgL_list2654z00_3564 =
										MAKE_YOUNG_PAIR(BGl_string3003z00zzsaw_bbvzd2debugzd2,
										BgL_arg2656z00_3565);
								}
								BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg2650z00_3561,
									BgL_list2654z00_3564);
							}
						}
						return BFALSE;
					}
			}
		}

	}



/* &rtl-assert-reg-type */
	obj_t BGl_z62rtlzd2assertzd2regzd2typezb0zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5302, obj_t BgL_regz00_5303, obj_t BgL_typez00_5304,
		obj_t BgL_polarityz00_5305, obj_t BgL_ctxz00_5306, obj_t BgL_locz00_5307,
		obj_t BgL_tagz00_5308)
	{
		{	/* SawBbv/bbv-debug.scm 669 */
			return
				BGl_rtlzd2assertzd2regzd2typezd2zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_regz00_5303),
				((BgL_typez00_bglt) BgL_typez00_5304),
				CBOOL(BgL_polarityz00_5305),
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctxz00_5306), BgL_locz00_5307,
				BgL_tagz00_5308);
		}

	}



/* rtl-assert-expr-type */
	BGL_EXPORTED_DEF obj_t
		BGl_rtlzd2assertzd2exprzd2typezd2zzsaw_bbvzd2debugzd2(BgL_rtl_regz00_bglt
		BgL_regz00_142, BgL_typez00_bglt BgL_typez00_143, obj_t BgL_polarityz00_144,
		obj_t BgL_ctxz00_145, obj_t BgL_locz00_146, obj_t BgL_tagz00_147)
	{
		{	/* SawBbv/bbv-debug.scm 688 */
			{	/* SawBbv/bbv-debug.scm 689 */
				obj_t BgL_debugnamez00_3572;

				BgL_debugnamez00_3572 =
					BGl_regzd2debugnamezd2zzsaw_bbvzd2debugzd2(BgL_regz00_142);
				{	/* SawBbv/bbv-debug.scm 690 */
					obj_t BgL_cexprz00_3573;

					{	/* SawBbv/bbv-debug.scm 691 */
						obj_t BgL_arg2666z00_3577;

						{	/* SawBbv/bbv-debug.scm 691 */
							obj_t BgL_arg2669z00_3579;

							{	/* SawBbv/bbv-debug.scm 691 */
								obj_t BgL__ortest_1236z00_3585;

								BgL__ortest_1236z00_3585 =
									BGl_typezd2predicatezd2zzsaw_bbvzd2debugzd2(
									((obj_t) BgL_typez00_143));
								if (CBOOL(BgL__ortest_1236z00_3585))
									{	/* SawBbv/bbv-debug.scm 691 */
										BgL_arg2669z00_3579 = BgL__ortest_1236z00_3585;
									}
								else
									{	/* SawBbv/bbv-debug.scm 691 */
										BgL_arg2669z00_3579 = BGl_string3099z00zzsaw_bbvzd2debugzd2;
									}
							}
							{	/* SawBbv/bbv-debug.scm 691 */

								BgL_arg2666z00_3577 =
									BGl_assertzd2failurezd2zzsaw_bbvzd2debugzd2
									(BgL_arg2669z00_3579, BgL_regz00_142, BgL_locz00_146,
									BgL_tagz00_147, BGl_string3088z00zzsaw_bbvzd2debugzd2);
							}
						}
						{	/* SawBbv/bbv-debug.scm 690 */
							obj_t BgL_list2667z00_3578;

							BgL_list2667z00_3578 = MAKE_YOUNG_PAIR(BgL_arg2666z00_3577, BNIL);
							BgL_cexprz00_3573 =
								BGl_formatz00zz__r4_output_6_10_3z00
								(BGl_string3100z00zzsaw_bbvzd2debugzd2, BgL_list2667z00_3578);
						}
					}
					{	/* SawBbv/bbv-debug.scm 692 */
						BgL_rtl_pragmaz00_bglt BgL_new1238z00_3574;

						{	/* SawBbv/bbv-debug.scm 692 */
							BgL_rtl_pragmaz00_bglt BgL_new1237z00_3575;

							BgL_new1237z00_3575 =
								((BgL_rtl_pragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_pragmaz00_bgl))));
							{	/* SawBbv/bbv-debug.scm 692 */
								long BgL_arg2665z00_3576;

								BgL_arg2665z00_3576 =
									BGL_CLASS_NUM(BGl_rtl_pragmaz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1237z00_3575),
									BgL_arg2665z00_3576);
							}
							BgL_new1238z00_3574 = BgL_new1237z00_3575;
						}
						((((BgL_rtl_funz00_bglt) COBJECT(
										((BgL_rtl_funz00_bglt) BgL_new1238z00_3574)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_new1238z00_3574))->
								BgL_formatz00) = ((obj_t) BgL_cexprz00_3573), BUNSPEC);
						((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_new1238z00_3574))->
								BgL_srfi0z00) = ((obj_t) CNST_TABLE_REF(6)), BUNSPEC);
						return ((obj_t) BgL_new1238z00_3574);
					}
				}
			}
		}

	}



/* &rtl-assert-expr-type */
	obj_t BGl_z62rtlzd2assertzd2exprzd2typezb0zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5309, obj_t BgL_regz00_5310, obj_t BgL_typez00_5311,
		obj_t BgL_polarityz00_5312, obj_t BgL_ctxz00_5313, obj_t BgL_locz00_5314,
		obj_t BgL_tagz00_5315)
	{
		{	/* SawBbv/bbv-debug.scm 688 */
			return
				BGl_rtlzd2assertzd2exprzd2typezd2zzsaw_bbvzd2debugzd2(
				((BgL_rtl_regz00_bglt) BgL_regz00_5310),
				((BgL_typez00_bglt) BgL_typez00_5311), BgL_polarityz00_5312,
				BgL_ctxz00_5313, BgL_locz00_5314, BgL_tagz00_5315);
		}

	}



/* rtl-assert-fxcmp */
	BGL_EXPORTED_DEF obj_t BGl_rtlzd2assertzd2fxcmpz00zzsaw_bbvzd2debugzd2(obj_t
		BgL_opz00_148, obj_t BgL_xz00_149, obj_t BgL_yz00_150,
		bool_t BgL_polarityz00_151, BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_152,
		obj_t BgL_locz00_153, obj_t BgL_tagz00_154)
	{
		{	/* SawBbv/bbv-debug.scm 699 */
			{
				obj_t BgL_locz00_3603;
				obj_t BgL_tagz00_3604;

				{	/* SawBbv/bbv-debug.scm 736 */
					obj_t BgL_cxz00_3588;
					obj_t BgL_cyz00_3589;

					BgL_cxz00_3588 =
						BGl_compilezd2simpleze70z35zzsaw_bbvzd2debugzd2(BgL_xz00_149);
					BgL_cyz00_3589 =
						BGl_compilezd2simpleze70z35zzsaw_bbvzd2debugzd2(BgL_yz00_150);
					{	/* SawBbv/bbv-debug.scm 738 */
						bool_t BgL_test3457z00_8513;

						if (CBOOL(BgL_cxz00_3588))
							{	/* SawBbv/bbv-debug.scm 738 */
								BgL_test3457z00_8513 = CBOOL(BgL_cyz00_3589);
							}
						else
							{	/* SawBbv/bbv-debug.scm 738 */
								BgL_test3457z00_8513 = ((bool_t) 0);
							}
						if (BgL_test3457z00_8513)
							{	/* SawBbv/bbv-debug.scm 739 */
								obj_t BgL_cexprz00_3591;

								{	/* SawBbv/bbv-debug.scm 740 */
									obj_t BgL_arg2674z00_3596;
									obj_t BgL_arg2675z00_3597;

									if ((BgL_opz00_148 == CNST_TABLE_REF(8)))
										{	/* SawBbv/bbv-debug.scm 741 */
											BgL_arg2674z00_3596 =
												BGl_string3103z00zzsaw_bbvzd2debugzd2;
										}
									else
										{	/* SawBbv/bbv-debug.scm 741 */
											BgL_arg2674z00_3596 = BgL_opz00_148;
										}
									BgL_locz00_3603 = BgL_locz00_153;
									BgL_tagz00_3604 = BgL_tagz00_154;
									BGl_assertzd2cntzd2zzsaw_bbvzd2debugzd2 =
										(BGl_assertzd2cntzd2zzsaw_bbvzd2debugzd2 + 1L);
									{	/* SawBbv/bbv-debug.scm 703 */
										bool_t BgL_test3460z00_8521;

										if (STRUCTP(BgL_locz00_3603))
											{	/* SawBbv/bbv-debug.scm 703 */
												BgL_test3460z00_8521 =
													(STRUCT_KEY(BgL_locz00_3603) == CNST_TABLE_REF(3));
											}
										else
											{	/* SawBbv/bbv-debug.scm 703 */
												BgL_test3460z00_8521 = ((bool_t) 0);
											}
										if (BgL_test3460z00_8521)
											{	/* SawBbv/bbv-debug.scm 705 */
												obj_t BgL_arg2685z00_3607;
												obj_t BgL_arg2686z00_3608;

												BgL_arg2685z00_3607 =
													STRUCT_REF(BgL_locz00_3603, (int) (0L));
												BgL_arg2686z00_3608 =
													STRUCT_REF(BgL_locz00_3603, (int) (1L));
												{	/* SawBbv/bbv-debug.scm 704 */
													obj_t BgL_list2687z00_3609;

													{	/* SawBbv/bbv-debug.scm 704 */
														obj_t BgL_arg2688z00_3610;

														{	/* SawBbv/bbv-debug.scm 704 */
															obj_t BgL_arg2689z00_3611;

															{	/* SawBbv/bbv-debug.scm 704 */
																obj_t BgL_arg2690z00_3612;

																{	/* SawBbv/bbv-debug.scm 704 */
																	obj_t BgL_arg2691z00_3613;

																	BgL_arg2691z00_3613 =
																		MAKE_YOUNG_PAIR(BgL_arg2686z00_3608, BNIL);
																	BgL_arg2690z00_3612 =
																		MAKE_YOUNG_PAIR(BgL_arg2685z00_3607,
																		BgL_arg2691z00_3613);
																}
																BgL_arg2689z00_3611 =
																	MAKE_YOUNG_PAIR(BgL_opz00_148,
																	BgL_arg2690z00_3612);
															}
															BgL_arg2688z00_3610 =
																MAKE_YOUNG_PAIR(BINT
																(BGl_assertzd2cntzd2zzsaw_bbvzd2debugzd2),
																BgL_arg2689z00_3611);
														}
														BgL_list2687z00_3609 =
															MAKE_YOUNG_PAIR(BgL_tagz00_3604,
															BgL_arg2688z00_3610);
													}
													BgL_arg2675z00_3597 =
														BGl_formatz00zz__r4_output_6_10_3z00
														(BGl_string3101z00zzsaw_bbvzd2debugzd2,
														BgL_list2687z00_3609);
											}}
										else
											{	/* SawBbv/bbv-debug.scm 706 */
												obj_t BgL_list2692z00_3614;

												{	/* SawBbv/bbv-debug.scm 706 */
													obj_t BgL_arg2693z00_3615;

													{	/* SawBbv/bbv-debug.scm 706 */
														obj_t BgL_arg2694z00_3616;

														BgL_arg2694z00_3616 =
															MAKE_YOUNG_PAIR(BgL_opz00_148, BNIL);
														BgL_arg2693z00_3615 =
															MAKE_YOUNG_PAIR(BINT
															(BGl_assertzd2cntzd2zzsaw_bbvzd2debugzd2),
															BgL_arg2694z00_3616);
													}
													BgL_list2692z00_3614 =
														MAKE_YOUNG_PAIR(BgL_tagz00_3604,
														BgL_arg2693z00_3615);
												}
												BgL_arg2675z00_3597 =
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string3102z00zzsaw_bbvzd2debugzd2,
													BgL_list2692z00_3614);
											}
									}
									{	/* SawBbv/bbv-debug.scm 739 */
										obj_t BgL_list2676z00_3598;

										{	/* SawBbv/bbv-debug.scm 739 */
											obj_t BgL_arg2678z00_3599;

											{	/* SawBbv/bbv-debug.scm 739 */
												obj_t BgL_arg2680z00_3600;

												{	/* SawBbv/bbv-debug.scm 739 */
													obj_t BgL_arg2681z00_3601;

													{	/* SawBbv/bbv-debug.scm 739 */
														obj_t BgL_arg2682z00_3602;

														BgL_arg2682z00_3602 =
															MAKE_YOUNG_PAIR(BgL_arg2675z00_3597, BNIL);
														BgL_arg2681z00_3601 =
															MAKE_YOUNG_PAIR(BgL_cyz00_3589,
															BgL_arg2682z00_3602);
													}
													BgL_arg2680z00_3600 =
														MAKE_YOUNG_PAIR(BgL_arg2674z00_3596,
														BgL_arg2681z00_3601);
												}
												BgL_arg2678z00_3599 =
													MAKE_YOUNG_PAIR(BgL_cxz00_3588, BgL_arg2680z00_3600);
											}
											{	/* SawBbv/bbv-debug.scm 739 */
												obj_t BgL_tmpz00_8547;

												if (BgL_polarityz00_151)
													{	/* SawBbv/bbv-debug.scm 740 */
														BgL_tmpz00_8547 =
															BGl_string3060z00zzsaw_bbvzd2debugzd2;
													}
												else
													{	/* SawBbv/bbv-debug.scm 740 */
														BgL_tmpz00_8547 =
															BGl_string3089z00zzsaw_bbvzd2debugzd2;
													}
												BgL_list2676z00_3598 =
													MAKE_YOUNG_PAIR(BgL_tmpz00_8547, BgL_arg2678z00_3599);
											}
										}
										BgL_cexprz00_3591 =
											BGl_formatz00zz__r4_output_6_10_3z00
											(BGl_string3104z00zzsaw_bbvzd2debugzd2,
											BgL_list2676z00_3598);
									}
								}
								{	/* SawBbv/bbv-debug.scm 742 */
									BgL_rtl_pragmaz00_bglt BgL_new1245z00_3592;

									{	/* SawBbv/bbv-debug.scm 742 */
										BgL_rtl_pragmaz00_bglt BgL_new1244z00_3593;

										BgL_new1244z00_3593 =
											((BgL_rtl_pragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_rtl_pragmaz00_bgl))));
										{	/* SawBbv/bbv-debug.scm 742 */
											long BgL_arg2672z00_3594;

											BgL_arg2672z00_3594 =
												BGL_CLASS_NUM(BGl_rtl_pragmaz00zzsaw_defsz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1244z00_3593),
												BgL_arg2672z00_3594);
										}
										BgL_new1245z00_3592 = BgL_new1244z00_3593;
									}
									((((BgL_rtl_funz00_bglt) COBJECT(
													((BgL_rtl_funz00_bglt) BgL_new1245z00_3592)))->
											BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
									((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_new1245z00_3592))->
											BgL_formatz00) = ((obj_t) BgL_cexprz00_3591), BUNSPEC);
									((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_new1245z00_3592))->
											BgL_srfi0z00) = ((obj_t) CNST_TABLE_REF(6)), BUNSPEC);
									return ((obj_t) BgL_new1245z00_3592);
								}
							}
						else
							{	/* SawBbv/bbv-debug.scm 738 */
								return BFALSE;
							}
					}
				}
			}
		}

	}



/* compile-simple~0 */
	obj_t BGl_compilezd2simpleze70z35zzsaw_bbvzd2debugzd2(obj_t BgL_xz00_3617)
	{
		{	/* SawBbv/bbv-debug.scm 734 */
			{	/* SawBbv/bbv-debug.scm 711 */
				bool_t BgL_test3463z00_8561;

				{	/* SawBbv/bbv-debug.scm 711 */
					obj_t BgL_classz00_4855;

					BgL_classz00_4855 = BGl_rtl_insz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_xz00_3617))
						{	/* SawBbv/bbv-debug.scm 711 */
							BgL_objectz00_bglt BgL_arg1807z00_4857;

							BgL_arg1807z00_4857 = (BgL_objectz00_bglt) (BgL_xz00_3617);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-debug.scm 711 */
									long BgL_idxz00_4863;

									BgL_idxz00_4863 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4857);
									BgL_test3463z00_8561 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4863 + 1L)) == BgL_classz00_4855);
								}
							else
								{	/* SawBbv/bbv-debug.scm 711 */
									bool_t BgL_res2990z00_4888;

									{	/* SawBbv/bbv-debug.scm 711 */
										obj_t BgL_oclassz00_4871;

										{	/* SawBbv/bbv-debug.scm 711 */
											obj_t BgL_arg1815z00_4879;
											long BgL_arg1816z00_4880;

											BgL_arg1815z00_4879 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-debug.scm 711 */
												long BgL_arg1817z00_4881;

												BgL_arg1817z00_4881 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4857);
												BgL_arg1816z00_4880 =
													(BgL_arg1817z00_4881 - OBJECT_TYPE);
											}
											BgL_oclassz00_4871 =
												VECTOR_REF(BgL_arg1815z00_4879, BgL_arg1816z00_4880);
										}
										{	/* SawBbv/bbv-debug.scm 711 */
											bool_t BgL__ortest_1115z00_4872;

											BgL__ortest_1115z00_4872 =
												(BgL_classz00_4855 == BgL_oclassz00_4871);
											if (BgL__ortest_1115z00_4872)
												{	/* SawBbv/bbv-debug.scm 711 */
													BgL_res2990z00_4888 = BgL__ortest_1115z00_4872;
												}
											else
												{	/* SawBbv/bbv-debug.scm 711 */
													long BgL_odepthz00_4873;

													{	/* SawBbv/bbv-debug.scm 711 */
														obj_t BgL_arg1804z00_4874;

														BgL_arg1804z00_4874 = (BgL_oclassz00_4871);
														BgL_odepthz00_4873 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4874);
													}
													if ((1L < BgL_odepthz00_4873))
														{	/* SawBbv/bbv-debug.scm 711 */
															obj_t BgL_arg1802z00_4876;

															{	/* SawBbv/bbv-debug.scm 711 */
																obj_t BgL_arg1803z00_4877;

																BgL_arg1803z00_4877 = (BgL_oclassz00_4871);
																BgL_arg1802z00_4876 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4877,
																	1L);
															}
															BgL_res2990z00_4888 =
																(BgL_arg1802z00_4876 == BgL_classz00_4855);
														}
													else
														{	/* SawBbv/bbv-debug.scm 711 */
															BgL_res2990z00_4888 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test3463z00_8561 = BgL_res2990z00_4888;
								}
						}
					else
						{	/* SawBbv/bbv-debug.scm 711 */
							BgL_test3463z00_8561 = ((bool_t) 0);
						}
				}
				if (BgL_test3463z00_8561)
					{	/* SawBbv/bbv-debug.scm 714 */
						bool_t BgL_test3468z00_8584;

						{	/* SawBbv/bbv-debug.scm 714 */
							BgL_rtl_funz00_bglt BgL_arg2751z00_3672;

							BgL_arg2751z00_3672 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_xz00_3617)))->BgL_funz00);
							{	/* SawBbv/bbv-debug.scm 714 */
								obj_t BgL_classz00_4889;

								BgL_classz00_4889 = BGl_rtl_loadiz00zzsaw_defsz00;
								{	/* SawBbv/bbv-debug.scm 714 */
									BgL_objectz00_bglt BgL_arg1807z00_4891;

									{	/* SawBbv/bbv-debug.scm 714 */
										obj_t BgL_tmpz00_8587;

										BgL_tmpz00_8587 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg2751z00_3672));
										BgL_arg1807z00_4891 =
											(BgL_objectz00_bglt) (BgL_tmpz00_8587);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawBbv/bbv-debug.scm 714 */
											long BgL_idxz00_4897;

											BgL_idxz00_4897 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4891);
											BgL_test3468z00_8584 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4897 + 3L)) == BgL_classz00_4889);
										}
									else
										{	/* SawBbv/bbv-debug.scm 714 */
											bool_t BgL_res2991z00_4922;

											{	/* SawBbv/bbv-debug.scm 714 */
												obj_t BgL_oclassz00_4905;

												{	/* SawBbv/bbv-debug.scm 714 */
													obj_t BgL_arg1815z00_4913;
													long BgL_arg1816z00_4914;

													BgL_arg1815z00_4913 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawBbv/bbv-debug.scm 714 */
														long BgL_arg1817z00_4915;

														BgL_arg1817z00_4915 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4891);
														BgL_arg1816z00_4914 =
															(BgL_arg1817z00_4915 - OBJECT_TYPE);
													}
													BgL_oclassz00_4905 =
														VECTOR_REF(BgL_arg1815z00_4913,
														BgL_arg1816z00_4914);
												}
												{	/* SawBbv/bbv-debug.scm 714 */
													bool_t BgL__ortest_1115z00_4906;

													BgL__ortest_1115z00_4906 =
														(BgL_classz00_4889 == BgL_oclassz00_4905);
													if (BgL__ortest_1115z00_4906)
														{	/* SawBbv/bbv-debug.scm 714 */
															BgL_res2991z00_4922 = BgL__ortest_1115z00_4906;
														}
													else
														{	/* SawBbv/bbv-debug.scm 714 */
															long BgL_odepthz00_4907;

															{	/* SawBbv/bbv-debug.scm 714 */
																obj_t BgL_arg1804z00_4908;

																BgL_arg1804z00_4908 = (BgL_oclassz00_4905);
																BgL_odepthz00_4907 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4908);
															}
															if ((3L < BgL_odepthz00_4907))
																{	/* SawBbv/bbv-debug.scm 714 */
																	obj_t BgL_arg1802z00_4910;

																	{	/* SawBbv/bbv-debug.scm 714 */
																		obj_t BgL_arg1803z00_4911;

																		BgL_arg1803z00_4911 = (BgL_oclassz00_4905);
																		BgL_arg1802z00_4910 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4911, 3L);
																	}
																	BgL_res2991z00_4922 =
																		(BgL_arg1802z00_4910 == BgL_classz00_4889);
																}
															else
																{	/* SawBbv/bbv-debug.scm 714 */
																	BgL_res2991z00_4922 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3468z00_8584 = BgL_res2991z00_4922;
										}
								}
							}
						}
						if (BgL_test3468z00_8584)
							{	/* SawBbv/bbv-debug.scm 715 */
								BgL_rtl_loadiz00_bglt BgL_i1240z00_3623;

								BgL_i1240z00_3623 =
									((BgL_rtl_loadiz00_bglt)
									(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_xz00_3617)))->BgL_funz00));
								{	/* SawBbv/bbv-debug.scm 716 */
									BgL_atomz00_bglt BgL_i1241z00_3624;

									BgL_i1241z00_3624 =
										(((BgL_rtl_loadiz00_bglt) COBJECT(BgL_i1240z00_3623))->
										BgL_constantz00);
									{	/* SawBbv/bbv-debug.scm 717 */
										obj_t BgL_arg2699z00_3625;

										BgL_arg2699z00_3625 =
											(((BgL_atomz00_bglt) COBJECT(BgL_i1241z00_3624))->
											BgL_valuez00);
										{	/* SawBbv/bbv-debug.scm 717 */
											obj_t BgL_list2700z00_3626;

											BgL_list2700z00_3626 =
												MAKE_YOUNG_PAIR(BgL_arg2699z00_3625, BNIL);
											return
												BGl_formatz00zz__r4_output_6_10_3z00
												(BGl_string3105z00zzsaw_bbvzd2debugzd2,
												BgL_list2700z00_3626);
										}
									}
								}
							}
						else
							{	/* SawBbv/bbv-debug.scm 718 */
								bool_t BgL_test3472z00_8617;

								{	/* SawBbv/bbv-debug.scm 718 */
									BgL_rtl_funz00_bglt BgL_arg2750z00_3671;

									BgL_arg2750z00_3671 =
										(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_xz00_3617)))->BgL_funz00);
									{	/* SawBbv/bbv-debug.scm 718 */
										obj_t BgL_classz00_4923;

										BgL_classz00_4923 = BGl_rtl_callz00zzsaw_defsz00;
										{	/* SawBbv/bbv-debug.scm 718 */
											BgL_objectz00_bglt BgL_arg1807z00_4925;

											{	/* SawBbv/bbv-debug.scm 718 */
												obj_t BgL_tmpz00_8620;

												BgL_tmpz00_8620 =
													((obj_t) ((BgL_objectz00_bglt) BgL_arg2750z00_3671));
												BgL_arg1807z00_4925 =
													(BgL_objectz00_bglt) (BgL_tmpz00_8620);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawBbv/bbv-debug.scm 718 */
													long BgL_idxz00_4931;

													BgL_idxz00_4931 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4925);
													BgL_test3472z00_8617 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_4931 + 2L)) == BgL_classz00_4923);
												}
											else
												{	/* SawBbv/bbv-debug.scm 718 */
													bool_t BgL_res2992z00_4956;

													{	/* SawBbv/bbv-debug.scm 718 */
														obj_t BgL_oclassz00_4939;

														{	/* SawBbv/bbv-debug.scm 718 */
															obj_t BgL_arg1815z00_4947;
															long BgL_arg1816z00_4948;

															BgL_arg1815z00_4947 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawBbv/bbv-debug.scm 718 */
																long BgL_arg1817z00_4949;

																BgL_arg1817z00_4949 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4925);
																BgL_arg1816z00_4948 =
																	(BgL_arg1817z00_4949 - OBJECT_TYPE);
															}
															BgL_oclassz00_4939 =
																VECTOR_REF(BgL_arg1815z00_4947,
																BgL_arg1816z00_4948);
														}
														{	/* SawBbv/bbv-debug.scm 718 */
															bool_t BgL__ortest_1115z00_4940;

															BgL__ortest_1115z00_4940 =
																(BgL_classz00_4923 == BgL_oclassz00_4939);
															if (BgL__ortest_1115z00_4940)
																{	/* SawBbv/bbv-debug.scm 718 */
																	BgL_res2992z00_4956 =
																		BgL__ortest_1115z00_4940;
																}
															else
																{	/* SawBbv/bbv-debug.scm 718 */
																	long BgL_odepthz00_4941;

																	{	/* SawBbv/bbv-debug.scm 718 */
																		obj_t BgL_arg1804z00_4942;

																		BgL_arg1804z00_4942 = (BgL_oclassz00_4939);
																		BgL_odepthz00_4941 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_4942);
																	}
																	if ((2L < BgL_odepthz00_4941))
																		{	/* SawBbv/bbv-debug.scm 718 */
																			obj_t BgL_arg1802z00_4944;

																			{	/* SawBbv/bbv-debug.scm 718 */
																				obj_t BgL_arg1803z00_4945;

																				BgL_arg1803z00_4945 =
																					(BgL_oclassz00_4939);
																				BgL_arg1802z00_4944 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_4945, 2L);
																			}
																			BgL_res2992z00_4956 =
																				(BgL_arg1802z00_4944 ==
																				BgL_classz00_4923);
																		}
																	else
																		{	/* SawBbv/bbv-debug.scm 718 */
																			BgL_res2992z00_4956 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test3472z00_8617 = BgL_res2992z00_4956;
												}
										}
									}
								}
								if (BgL_test3472z00_8617)
									{	/* SawBbv/bbv-debug.scm 718 */
										{	/* SawBbv/bbv-debug.scm 719 */
											BgL_rtl_callz00_bglt BgL_i1242z00_3629;

											BgL_i1242z00_3629 =
												((BgL_rtl_callz00_bglt)
												(((BgL_rtl_insz00_bglt) COBJECT(
															((BgL_rtl_insz00_bglt) BgL_xz00_3617)))->
													BgL_funz00));
											{	/* SawBbv/bbv-debug.scm 721 */
												bool_t BgL_test3476z00_8646;

												{	/* SawBbv/bbv-debug.scm 721 */
													BgL_globalz00_bglt BgL_arg2736z00_3658;

													BgL_arg2736z00_3658 =
														(((BgL_rtl_callz00_bglt)
															COBJECT(BgL_i1242z00_3629))->BgL_varz00);
													BgL_test3476z00_8646 =
														(((obj_t) BgL_arg2736z00_3658) ==
														BGl_za2bintzd2ze3longza2z31zzsaw_bbvzd2cachezd2);
												}
												if (BgL_test3476z00_8646)
													{	/* SawBbv/bbv-debug.scm 722 */
														bool_t BgL_test3477z00_8650;

														{	/* SawBbv/bbv-debug.scm 722 */
															obj_t BgL_arg2723z00_3648;

															{	/* SawBbv/bbv-debug.scm 722 */
																obj_t BgL_pairz00_4957;

																BgL_pairz00_4957 =
																	(((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt) BgL_xz00_3617)))->
																	BgL_argsz00);
																BgL_arg2723z00_3648 = CAR(BgL_pairz00_4957);
															}
															{	/* SawBbv/bbv-debug.scm 722 */
																obj_t BgL_classz00_4958;

																BgL_classz00_4958 = BGl_rtl_regz00zzsaw_defsz00;
																if (BGL_OBJECTP(BgL_arg2723z00_3648))
																	{	/* SawBbv/bbv-debug.scm 722 */
																		BgL_objectz00_bglt BgL_arg1807z00_4960;

																		BgL_arg1807z00_4960 =
																			(BgL_objectz00_bglt)
																			(BgL_arg2723z00_3648);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* SawBbv/bbv-debug.scm 722 */
																				long BgL_idxz00_4966;

																				BgL_idxz00_4966 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_4960);
																				BgL_test3477z00_8650 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_4966 + 1L)) ==
																					BgL_classz00_4958);
																			}
																		else
																			{	/* SawBbv/bbv-debug.scm 722 */
																				bool_t BgL_res2993z00_4991;

																				{	/* SawBbv/bbv-debug.scm 722 */
																					obj_t BgL_oclassz00_4974;

																					{	/* SawBbv/bbv-debug.scm 722 */
																						obj_t BgL_arg1815z00_4982;
																						long BgL_arg1816z00_4983;

																						BgL_arg1815z00_4982 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* SawBbv/bbv-debug.scm 722 */
																							long BgL_arg1817z00_4984;

																							BgL_arg1817z00_4984 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_4960);
																							BgL_arg1816z00_4983 =
																								(BgL_arg1817z00_4984 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_4974 =
																							VECTOR_REF(BgL_arg1815z00_4982,
																							BgL_arg1816z00_4983);
																					}
																					{	/* SawBbv/bbv-debug.scm 722 */
																						bool_t BgL__ortest_1115z00_4975;

																						BgL__ortest_1115z00_4975 =
																							(BgL_classz00_4958 ==
																							BgL_oclassz00_4974);
																						if (BgL__ortest_1115z00_4975)
																							{	/* SawBbv/bbv-debug.scm 722 */
																								BgL_res2993z00_4991 =
																									BgL__ortest_1115z00_4975;
																							}
																						else
																							{	/* SawBbv/bbv-debug.scm 722 */
																								long BgL_odepthz00_4976;

																								{	/* SawBbv/bbv-debug.scm 722 */
																									obj_t BgL_arg1804z00_4977;

																									BgL_arg1804z00_4977 =
																										(BgL_oclassz00_4974);
																									BgL_odepthz00_4976 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_4977);
																								}
																								if ((1L < BgL_odepthz00_4976))
																									{	/* SawBbv/bbv-debug.scm 722 */
																										obj_t BgL_arg1802z00_4979;

																										{	/* SawBbv/bbv-debug.scm 722 */
																											obj_t BgL_arg1803z00_4980;

																											BgL_arg1803z00_4980 =
																												(BgL_oclassz00_4974);
																											BgL_arg1802z00_4979 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_4980,
																												1L);
																										}
																										BgL_res2993z00_4991 =
																											(BgL_arg1802z00_4979 ==
																											BgL_classz00_4958);
																									}
																								else
																									{	/* SawBbv/bbv-debug.scm 722 */
																										BgL_res2993z00_4991 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test3477z00_8650 =
																					BgL_res2993z00_4991;
																			}
																	}
																else
																	{	/* SawBbv/bbv-debug.scm 722 */
																		BgL_test3477z00_8650 = ((bool_t) 0);
																	}
															}
														}
														if (BgL_test3477z00_8650)
															{	/* SawBbv/bbv-debug.scm 723 */
																obj_t BgL_arg2708z00_3636;

																{	/* SawBbv/bbv-debug.scm 723 */
																	obj_t BgL_arg2710z00_3638;

																	{	/* SawBbv/bbv-debug.scm 723 */
																		obj_t BgL_pairz00_4992;

																		BgL_pairz00_4992 =
																			(((BgL_rtl_insz00_bglt) COBJECT(
																					((BgL_rtl_insz00_bglt)
																						BgL_xz00_3617)))->BgL_argsz00);
																		BgL_arg2710z00_3638 = CAR(BgL_pairz00_4992);
																	}
																	BgL_arg2708z00_3636 =
																		BGl_compilezd2simpleze70z35zzsaw_bbvzd2debugzd2
																		(BgL_arg2710z00_3638);
																}
																{	/* SawBbv/bbv-debug.scm 723 */
																	obj_t BgL_list2709z00_3637;

																	BgL_list2709z00_3637 =
																		MAKE_YOUNG_PAIR(BgL_arg2708z00_3636, BNIL);
																	BGl_formatz00zz__r4_output_6_10_3z00
																		(BGl_string3105z00zzsaw_bbvzd2debugzd2,
																		BgL_list2709z00_3637);
																}
															}
														else
															{	/* SawBbv/bbv-debug.scm 724 */
																obj_t BgL_arg2712z00_3640;
																obj_t BgL_arg2714z00_3641;

																{	/* SawBbv/bbv-debug.scm 724 */
																	obj_t BgL_tmpz00_8682;

																	BgL_tmpz00_8682 = BGL_CURRENT_DYNAMIC_ENV();
																	BgL_arg2712z00_3640 =
																		BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_8682);
																}
																BgL_arg2714z00_3641 =
																	BGl_shapez00zztools_shapez00(BgL_xz00_3617);
																{	/* SawBbv/bbv-debug.scm 724 */
																	obj_t BgL_list2715z00_3642;

																	{	/* SawBbv/bbv-debug.scm 724 */
																		obj_t BgL_arg2716z00_3643;

																		{	/* SawBbv/bbv-debug.scm 724 */
																			obj_t BgL_arg2717z00_3644;

																			{	/* SawBbv/bbv-debug.scm 724 */
																				obj_t BgL_arg2719z00_3645;

																				{	/* SawBbv/bbv-debug.scm 724 */
																					obj_t BgL_arg2721z00_3646;

																					{	/* SawBbv/bbv-debug.scm 724 */
																						obj_t BgL_arg2722z00_3647;

																						BgL_arg2722z00_3647 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2714z00_3641, BNIL);
																						BgL_arg2721z00_3646 =
																							MAKE_YOUNG_PAIR
																							(BGl_string3106z00zzsaw_bbvzd2debugzd2,
																							BgL_arg2722z00_3647);
																					}
																					BgL_arg2719z00_3645 =
																						MAKE_YOUNG_PAIR
																						(BGl_string3001z00zzsaw_bbvzd2debugzd2,
																						BgL_arg2721z00_3646);
																				}
																				BgL_arg2717z00_3644 =
																					MAKE_YOUNG_PAIR(BINT(724L),
																					BgL_arg2719z00_3645);
																			}
																			BgL_arg2716z00_3643 =
																				MAKE_YOUNG_PAIR
																				(BGl_string3002z00zzsaw_bbvzd2debugzd2,
																				BgL_arg2717z00_3644);
																		}
																		BgL_list2715z00_3642 =
																			MAKE_YOUNG_PAIR
																			(BGl_string3003z00zzsaw_bbvzd2debugzd2,
																			BgL_arg2716z00_3643);
																	}
																	BGl_tprintz00zz__r4_output_6_10_3z00
																		(BgL_arg2712z00_3640, BgL_list2715z00_3642);
																}
															}
													}
												else
													{	/* SawBbv/bbv-debug.scm 725 */
														obj_t BgL_arg2725z00_3650;
														obj_t BgL_arg2726z00_3651;

														{	/* SawBbv/bbv-debug.scm 725 */
															obj_t BgL_tmpz00_8694;

															BgL_tmpz00_8694 = BGL_CURRENT_DYNAMIC_ENV();
															BgL_arg2725z00_3650 =
																BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_8694);
														}
														BgL_arg2726z00_3651 =
															BGl_shapez00zztools_shapez00(BgL_xz00_3617);
														{	/* SawBbv/bbv-debug.scm 725 */
															obj_t BgL_list2727z00_3652;

															{	/* SawBbv/bbv-debug.scm 725 */
																obj_t BgL_arg2728z00_3653;

																{	/* SawBbv/bbv-debug.scm 725 */
																	obj_t BgL_arg2729z00_3654;

																	{	/* SawBbv/bbv-debug.scm 725 */
																		obj_t BgL_arg2731z00_3655;

																		{	/* SawBbv/bbv-debug.scm 725 */
																			obj_t BgL_arg2733z00_3656;

																			{	/* SawBbv/bbv-debug.scm 725 */
																				obj_t BgL_arg2734z00_3657;

																				BgL_arg2734z00_3657 =
																					MAKE_YOUNG_PAIR(BgL_arg2726z00_3651,
																					BNIL);
																				BgL_arg2733z00_3656 =
																					MAKE_YOUNG_PAIR
																					(BGl_string3107z00zzsaw_bbvzd2debugzd2,
																					BgL_arg2734z00_3657);
																			}
																			BgL_arg2731z00_3655 =
																				MAKE_YOUNG_PAIR
																				(BGl_string3001z00zzsaw_bbvzd2debugzd2,
																				BgL_arg2733z00_3656);
																		}
																		BgL_arg2729z00_3654 =
																			MAKE_YOUNG_PAIR(BINT(725L),
																			BgL_arg2731z00_3655);
																	}
																	BgL_arg2728z00_3653 =
																		MAKE_YOUNG_PAIR
																		(BGl_string3002z00zzsaw_bbvzd2debugzd2,
																		BgL_arg2729z00_3654);
																}
																BgL_list2727z00_3652 =
																	MAKE_YOUNG_PAIR
																	(BGl_string3003z00zzsaw_bbvzd2debugzd2,
																	BgL_arg2728z00_3653);
															}
															BGl_tprintz00zz__r4_output_6_10_3z00
																(BgL_arg2725z00_3650, BgL_list2727z00_3652);
														}
													}
											}
										}
										return BFALSE;
									}
								else
									{	/* SawBbv/bbv-debug.scm 718 */
										{	/* SawBbv/bbv-debug.scm 728 */
											obj_t BgL_arg2737z00_3659;
											obj_t BgL_arg2738z00_3660;
											obj_t BgL_arg2739z00_3661;

											{	/* SawBbv/bbv-debug.scm 728 */
												obj_t BgL_tmpz00_8706;

												BgL_tmpz00_8706 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_arg2737z00_3659 =
													BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_8706);
											}
											BgL_arg2738z00_3660 =
												BGl_shapez00zztools_shapez00(BgL_xz00_3617);
											{	/* SawBbv/bbv-debug.scm 728 */
												BgL_rtl_funz00_bglt BgL_arg2749z00_3670;

												BgL_arg2749z00_3670 =
													(((BgL_rtl_insz00_bglt) COBJECT(
															((BgL_rtl_insz00_bglt) BgL_xz00_3617)))->
													BgL_funz00);
												BgL_arg2739z00_3661 =
													bgl_typeof(((obj_t) BgL_arg2749z00_3670));
											}
											{	/* SawBbv/bbv-debug.scm 728 */
												obj_t BgL_list2740z00_3662;

												{	/* SawBbv/bbv-debug.scm 728 */
													obj_t BgL_arg2741z00_3663;

													{	/* SawBbv/bbv-debug.scm 728 */
														obj_t BgL_arg2742z00_3664;

														{	/* SawBbv/bbv-debug.scm 728 */
															obj_t BgL_arg2743z00_3665;

															{	/* SawBbv/bbv-debug.scm 728 */
																obj_t BgL_arg2744z00_3666;

																{	/* SawBbv/bbv-debug.scm 728 */
																	obj_t BgL_arg2746z00_3667;

																	{	/* SawBbv/bbv-debug.scm 728 */
																		obj_t BgL_arg2747z00_3668;

																		{	/* SawBbv/bbv-debug.scm 728 */
																			obj_t BgL_arg2748z00_3669;

																			BgL_arg2748z00_3669 =
																				MAKE_YOUNG_PAIR(BgL_arg2739z00_3661,
																				BNIL);
																			BgL_arg2747z00_3668 =
																				MAKE_YOUNG_PAIR
																				(BGl_string3051z00zzsaw_bbvzd2debugzd2,
																				BgL_arg2748z00_3669);
																		}
																		BgL_arg2746z00_3667 =
																			MAKE_YOUNG_PAIR(BgL_arg2738z00_3660,
																			BgL_arg2747z00_3668);
																	}
																	BgL_arg2744z00_3666 =
																		MAKE_YOUNG_PAIR
																		(BGl_string3108z00zzsaw_bbvzd2debugzd2,
																		BgL_arg2746z00_3667);
																}
																BgL_arg2743z00_3665 =
																	MAKE_YOUNG_PAIR
																	(BGl_string3001z00zzsaw_bbvzd2debugzd2,
																	BgL_arg2744z00_3666);
															}
															BgL_arg2742z00_3664 =
																MAKE_YOUNG_PAIR(BINT(728L),
																BgL_arg2743z00_3665);
														}
														BgL_arg2741z00_3663 =
															MAKE_YOUNG_PAIR
															(BGl_string3002z00zzsaw_bbvzd2debugzd2,
															BgL_arg2742z00_3664);
													}
													BgL_list2740z00_3662 =
														MAKE_YOUNG_PAIR
														(BGl_string3003z00zzsaw_bbvzd2debugzd2,
														BgL_arg2741z00_3663);
												}
												BGl_tprintz00zz__r4_output_6_10_3z00
													(BgL_arg2737z00_3659, BgL_list2740z00_3662);
											}
										}
										return BFALSE;
									}
							}
					}
				else
					{	/* SawBbv/bbv-debug.scm 730 */
						bool_t BgL_test3482z00_8724;

						{	/* SawBbv/bbv-debug.scm 730 */
							obj_t BgL_classz00_4996;

							BgL_classz00_4996 = BGl_rtl_regz00zzsaw_defsz00;
							if (BGL_OBJECTP(BgL_xz00_3617))
								{	/* SawBbv/bbv-debug.scm 730 */
									BgL_objectz00_bglt BgL_arg1807z00_4998;

									BgL_arg1807z00_4998 = (BgL_objectz00_bglt) (BgL_xz00_3617);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawBbv/bbv-debug.scm 730 */
											long BgL_idxz00_5004;

											BgL_idxz00_5004 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4998);
											BgL_test3482z00_8724 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_5004 + 1L)) == BgL_classz00_4996);
										}
									else
										{	/* SawBbv/bbv-debug.scm 730 */
											bool_t BgL_res2994z00_5029;

											{	/* SawBbv/bbv-debug.scm 730 */
												obj_t BgL_oclassz00_5012;

												{	/* SawBbv/bbv-debug.scm 730 */
													obj_t BgL_arg1815z00_5020;
													long BgL_arg1816z00_5021;

													BgL_arg1815z00_5020 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawBbv/bbv-debug.scm 730 */
														long BgL_arg1817z00_5022;

														BgL_arg1817z00_5022 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4998);
														BgL_arg1816z00_5021 =
															(BgL_arg1817z00_5022 - OBJECT_TYPE);
													}
													BgL_oclassz00_5012 =
														VECTOR_REF(BgL_arg1815z00_5020,
														BgL_arg1816z00_5021);
												}
												{	/* SawBbv/bbv-debug.scm 730 */
													bool_t BgL__ortest_1115z00_5013;

													BgL__ortest_1115z00_5013 =
														(BgL_classz00_4996 == BgL_oclassz00_5012);
													if (BgL__ortest_1115z00_5013)
														{	/* SawBbv/bbv-debug.scm 730 */
															BgL_res2994z00_5029 = BgL__ortest_1115z00_5013;
														}
													else
														{	/* SawBbv/bbv-debug.scm 730 */
															long BgL_odepthz00_5014;

															{	/* SawBbv/bbv-debug.scm 730 */
																obj_t BgL_arg1804z00_5015;

																BgL_arg1804z00_5015 = (BgL_oclassz00_5012);
																BgL_odepthz00_5014 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_5015);
															}
															if ((1L < BgL_odepthz00_5014))
																{	/* SawBbv/bbv-debug.scm 730 */
																	obj_t BgL_arg1802z00_5017;

																	{	/* SawBbv/bbv-debug.scm 730 */
																		obj_t BgL_arg1803z00_5018;

																		BgL_arg1803z00_5018 = (BgL_oclassz00_5012);
																		BgL_arg1802z00_5017 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_5018, 1L);
																	}
																	BgL_res2994z00_5029 =
																		(BgL_arg1802z00_5017 == BgL_classz00_4996);
																}
															else
																{	/* SawBbv/bbv-debug.scm 730 */
																	BgL_res2994z00_5029 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3482z00_8724 = BgL_res2994z00_5029;
										}
								}
							else
								{	/* SawBbv/bbv-debug.scm 730 */
									BgL_test3482z00_8724 = ((bool_t) 0);
								}
						}
						if (BgL_test3482z00_8724)
							{	/* SawBbv/bbv-debug.scm 730 */
								return
									BGl_regzd2debugnamezd2zzsaw_bbvzd2debugzd2(
									((BgL_rtl_regz00_bglt) BgL_xz00_3617));
							}
						else
							{	/* SawBbv/bbv-debug.scm 730 */
								{	/* SawBbv/bbv-debug.scm 733 */
									obj_t BgL_arg2753z00_3674;
									obj_t BgL_arg2754z00_3675;
									obj_t BgL_arg2755z00_3676;

									{	/* SawBbv/bbv-debug.scm 733 */
										obj_t BgL_tmpz00_8749;

										BgL_tmpz00_8749 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_arg2753z00_3674 =
											BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_8749);
									}
									BgL_arg2754z00_3675 =
										BGl_shapez00zztools_shapez00(BgL_xz00_3617);
									BgL_arg2755z00_3676 = bgl_typeof(BGl_funz00zzast_varz00);
									{	/* SawBbv/bbv-debug.scm 733 */
										obj_t BgL_list2756z00_3677;

										{	/* SawBbv/bbv-debug.scm 733 */
											obj_t BgL_arg2757z00_3678;

											{	/* SawBbv/bbv-debug.scm 733 */
												obj_t BgL_arg2758z00_3679;

												{	/* SawBbv/bbv-debug.scm 733 */
													obj_t BgL_arg2759z00_3680;

													{	/* SawBbv/bbv-debug.scm 733 */
														obj_t BgL_arg2760z00_3681;

														{	/* SawBbv/bbv-debug.scm 733 */
															obj_t BgL_arg2761z00_3682;

															{	/* SawBbv/bbv-debug.scm 733 */
																obj_t BgL_arg2762z00_3683;

																{	/* SawBbv/bbv-debug.scm 733 */
																	obj_t BgL_arg2764z00_3684;

																	BgL_arg2764z00_3684 =
																		MAKE_YOUNG_PAIR(BgL_arg2755z00_3676, BNIL);
																	BgL_arg2762z00_3683 =
																		MAKE_YOUNG_PAIR
																		(BGl_string3051z00zzsaw_bbvzd2debugzd2,
																		BgL_arg2764z00_3684);
																}
																BgL_arg2761z00_3682 =
																	MAKE_YOUNG_PAIR(BgL_arg2754z00_3675,
																	BgL_arg2762z00_3683);
															}
															BgL_arg2760z00_3681 =
																MAKE_YOUNG_PAIR
																(BGl_string3109z00zzsaw_bbvzd2debugzd2,
																BgL_arg2761z00_3682);
														}
														BgL_arg2759z00_3680 =
															MAKE_YOUNG_PAIR
															(BGl_string3001z00zzsaw_bbvzd2debugzd2,
															BgL_arg2760z00_3681);
													}
													BgL_arg2758z00_3679 =
														MAKE_YOUNG_PAIR(BINT(733L), BgL_arg2759z00_3680);
												}
												BgL_arg2757z00_3678 =
													MAKE_YOUNG_PAIR(BGl_string3002z00zzsaw_bbvzd2debugzd2,
													BgL_arg2758z00_3679);
											}
											BgL_list2756z00_3677 =
												MAKE_YOUNG_PAIR(BGl_string3003z00zzsaw_bbvzd2debugzd2,
												BgL_arg2757z00_3678);
										}
										BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg2753z00_3674,
											BgL_list2756z00_3677);
									}
								}
								return BFALSE;
							}
					}
			}
		}

	}



/* &rtl-assert-fxcmp */
	obj_t BGl_z62rtlzd2assertzd2fxcmpz62zzsaw_bbvzd2debugzd2(obj_t
		BgL_envz00_5316, obj_t BgL_opz00_5317, obj_t BgL_xz00_5318,
		obj_t BgL_yz00_5319, obj_t BgL_polarityz00_5320, obj_t BgL_ctxz00_5321,
		obj_t BgL_locz00_5322, obj_t BgL_tagz00_5323)
	{
		{	/* SawBbv/bbv-debug.scm 699 */
			return
				BGl_rtlzd2assertzd2fxcmpz00zzsaw_bbvzd2debugzd2(BgL_opz00_5317,
				BgL_xz00_5318, BgL_yz00_5319, CBOOL(BgL_polarityz00_5320),
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctxz00_5321), BgL_locz00_5322,
				BgL_tagz00_5323);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_bbvzd2debugzd2(void)
	{
		{	/* SawBbv/bbv-debug.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_bbvzd2debugzd2(void)
	{
		{	/* SawBbv/bbv-debug.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_bbvzd2debugzd2(void)
	{
		{	/* SawBbv/bbv-debug.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2debugzd2(void)
	{
		{	/* SawBbv/bbv-debug.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zzsaw_regsetz00(358079690L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(237915200L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(288263219L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(425659118L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2specializa7ez75(506937389L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(242097300L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
			return
				BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(481635416L,
				BSTRING_TO_STRING(BGl_string3110z00zzsaw_bbvzd2debugzd2));
		}

	}

#ifdef __cplusplus
}
#endif
