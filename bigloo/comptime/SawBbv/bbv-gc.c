/*===========================================================================*/
/*   (SawBbv/bbv-gc.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawBbv/bbv-gc.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_SAW_BBVzd2GCzd2_TYPE_DEFINITIONS
#define BGL_BgL_SAW_BBVzd2GCzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_rtl_regzf2razf2_bgl
	{
		int BgL_numz00;
		obj_t BgL_colorz00;
		obj_t BgL_coalescez00;
		int BgL_occurrencesz00;
		obj_t BgL_interferez00;
		obj_t BgL_interfere2z00;
	}                      *BgL_rtl_regzf2razf2_bglt;

	typedef struct BgL_regsetz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_lengthz00;
		int BgL_msiza7eza7;
		obj_t BgL_regvz00;
		obj_t BgL_reglz00;
		obj_t BgL_stringz00;
	}                *BgL_regsetz00_bglt;

	typedef struct BgL_blockvz00_bgl
	{
		obj_t BgL_versionsz00;
		obj_t BgL_genericz00;
		long BgL_z52markz52;
		obj_t BgL_mergez00;
	}                *BgL_blockvz00_bglt;

	typedef struct BgL_blocksz00_bgl
	{
		long BgL_z52markz52;
		obj_t BgL_z52hashz52;
		obj_t BgL_z52blacklistz52;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		struct BgL_blockz00_bgl *BgL_parentz00;
		long BgL_gccntz00;
		long BgL_gcmarkz00;
		obj_t BgL_mblockz00;
		obj_t BgL_creatorz00;
		obj_t BgL_mergesz00;
		bool_t BgL_asleepz00;
	}                *BgL_blocksz00_bglt;

	typedef struct BgL_bbvzd2ctxzd2_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_idz00;
		obj_t BgL_entriesz00;
	}                   *BgL_bbvzd2ctxzd2_bglt;


#endif													// BGL_BgL_SAW_BBVzd2GCzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2gczd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_za2gczd2graphza2zd2zzsaw_bbvzd2gczd2 = BUNSPEC;
	static obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2gczd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2gczd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bbvzd2gczd2disconnectz12z70zzsaw_bbvzd2gczd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31676ze3ze5zzsaw_bbvzd2gczd2(obj_t,
		BgL_blockz00_bglt);
	static long BGl_za2gczd2blockzd2lenza2z00zzsaw_bbvzd2gczd2 = 0L;
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_bbvzd2gczd2 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t
		BGl_blockVzd2livezd2versionsz00zzsaw_bbvzd2typeszd2(BgL_blockz00_bglt);
	static obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2gczd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2gczd2redirectz12z12zzsaw_bbvzd2gczd2(BgL_blockz00_bglt,
		BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2gcz12zc0zzsaw_bbvzd2gczd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2gczd2(int, int, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2gczd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_z62bbvzd2gczd2initz12z70zzsaw_bbvzd2gczd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2gczd2(BgL_typez00_bglt, obj_t, obj_t,
		obj_t, obj_t, obj_t, int, obj_t, obj_t, int, obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2gczd2(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2gczd2(void);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2gczd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2gczd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2gczd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_ssrzd2removezd2edgez12z12zz__ssrz00(obj_t, long, long,
		obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_bbvzd2gczd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2gczd2(obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2gczd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt);
	static long BGl_za2gczd2markza2zd2zzsaw_bbvzd2gczd2 = 0L;
	static obj_t BGl_objectzd2initzd2zzsaw_bbvzd2gczd2(void);
	static obj_t BGl_z62bbvzd2gczd2ondisconnectz12z70zzsaw_bbvzd2gczd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt,
		obj_t);
	extern obj_t BGl_regsetz00zzsaw_regsetz00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2gczd2addzd2blockz12zc0zzsaw_bbvzd2gczd2(BgL_blockz00_bglt);
	BGL_IMPORT obj_t BGl_ssrzd2makezd2graphz00zz__ssrz00(obj_t);
	static obj_t BGl_za2gczd2blocksza2zd2zzsaw_bbvzd2gczd2 = BUNSPEC;
	static obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2gczd2(obj_t, obj_t);
	static obj_t BGl_z62bbvzd2gczd2redirectz12z70zzsaw_bbvzd2gczd2(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2;
	BGL_IMPORT obj_t make_vector(long, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2gczd2connectz12z12zzsaw_bbvzd2gczd2(BgL_blockz00_bglt,
		BgL_blockz00_bglt);
	extern obj_t BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t BGl_z62bbvzd2gczd2connectz12z70zzsaw_bbvzd2gczd2(obj_t, obj_t,
		obj_t);
	static long BGl_getzd2gczd2markz12z12zzsaw_bbvzd2gczd2(void);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2gczd2(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_bbvzd2gczd2(void);
	static obj_t
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2gczd2(obj_t, obj_t,
		obj_t);
	static BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2gczd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2gczd2(void);
	static BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2gczd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2gczd2disconnectz12z12zzsaw_bbvzd2gczd2(BgL_blockz00_bglt,
		BgL_blockz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2gczd2(obj_t, obj_t);
	static obj_t BGl_z62bbvzd2gczd2blockzd2reachablezf3z43zzsaw_bbvzd2gczd2(obj_t,
		obj_t);
	static obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2gczd2(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2gczd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2gczd2(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31603ze3ze5zzsaw_bbvzd2gczd2(obj_t,
		BgL_blockz00_bglt);
	BGL_IMPORT obj_t BGl_vectorzd2copyz12zc0zz__r4_vectors_6_8z00(obj_t, long,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2gczd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2gczd2(BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2gczd2(BgL_regsetz00_bglt, obj_t);
	static bool_t BGl_z62zc3z04anonymousza31702ze3ze5zzsaw_bbvzd2gczd2(obj_t,
		BgL_blockz00_bglt);
	BGL_IMPORT obj_t bgl_remq_bang(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2gczd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2gczd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2debugzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2specializa7ez75(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__ssrz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_z62bbvzd2gcz12za2zzsaw_bbvzd2gczd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(obj_t, obj_t,
		long, long);
	BGL_EXPORTED_DECL bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2gczd2(obj_t);
	BGL_IMPORT obj_t BGl_ssrzd2connectedzf3z21zz__ssrz00(obj_t, long);
	static obj_t
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2gczd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2gczd2addzd2blockz12za2zzsaw_bbvzd2gczd2(obj_t,
		obj_t);
	static bool_t BGl_z62zc3z04anonymousza31617ze3ze5zzsaw_bbvzd2gczd2(obj_t,
		BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2gczd2initz12z12zzsaw_bbvzd2gczd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2gczd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2gczd2(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2gczd2(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2gczd2(void);
	extern obj_t BGl_za2bbvzd2blockszd2gcza2z00zzsaw_bbvzd2configzd2;
	static obj_t BGl_z62zc3z04anonymousza31715ze3ze5zzsaw_bbvzd2gczd2(obj_t,
		BgL_blockz00_bglt);
	static bool_t BGl_walkbsz12z12zzsaw_bbvzd2gczd2(BgL_blockz00_bglt, long,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31651ze3ze5zzsaw_bbvzd2gczd2(obj_t,
		BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2gczd2
		(BgL_rtl_regz00_bglt, int);
	BGL_IMPORT obj_t BGl_ssrzd2redirectz12zc0zz__ssrz00(obj_t, long, long, obj_t,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2gczd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_regsetzf3zf3zzsaw_bbvzd2gczd2(obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2gczd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2gczd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2gczd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2gczd2(obj_t, obj_t);
	static bool_t BGl_walkbvz12z12zzsaw_bbvzd2gczd2(BgL_blockz00_bglt, long,
		obj_t);
	static obj_t BGl_z62bbvzd2gczd2onconnectz12z70zzsaw_bbvzd2gczd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2gczd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2gczd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2gczd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_ssrzd2addzd2edgez12z12zz__ssrz00(obj_t, long, long,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2gczd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2gczd2(void);
	static obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2gczd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2gczd2(obj_t, obj_t);
	extern obj_t BGl_rtl_regzf2razf2zzsaw_regsetz00;
	static obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2gczd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2gczd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2gczd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2gczd2(BgL_regsetz00_bglt);
	static obj_t
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2gczd2(obj_t, obj_t,
		obj_t);
	static BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2gczd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2gczd2blockzd2reachablezf3z21zzsaw_bbvzd2gczd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2gczd2(BgL_regsetz00_bglt, int);
	static obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2gczd2(obj_t, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_bbvzd2gczd2ondisconnectz12zd2envzc0zzsaw_bbvzd2gczd2,
		BgL_bgl_za762bbvza7d2gcza7d2on1986za7,
		BGl_z62bbvzd2gczd2ondisconnectz12z70zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2gczd2initz12zd2envzc0zzsaw_bbvzd2gczd2,
		BgL_bgl_za762bbvza7d2gcza7d2in1987za7,
		BGl_z62bbvzd2gczd2initz12z70zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2namezd2envzf2zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza71988za7,
		BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2nilzd2envz00zzsaw_bbvzd2gczd2,
		BgL_bgl_za762regsetza7d2nilza71989za7,
		BGl_z62regsetzd2nilzb0zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2setz12zd2envz32zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza71990za7,
		BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2envzf2zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza71991za7,
		BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_bbvzd2gczd2onconnectz12zd2envzc0zzsaw_bbvzd2gczd2,
		BgL_bgl_za762bbvza7d2gcza7d2on1992za7,
		BGl_z62bbvzd2gczd2onconnectz12z70zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2gczd2redirectz12zd2envzc0zzsaw_bbvzd2gczd2,
		BgL_bgl_za762bbvza7d2gcza7d2re1993za7,
		BGl_z62bbvzd2gczd2redirectz12z70zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2setz12zd2envzc1zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza71994za7,
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2gczd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2setz12zd2envz32zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza71995za7,
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2gczd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2envzf2zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza71996za7,
		BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2setz12zd2envz32zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza71997za7,
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2gczd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2gczd2addzd2blockz12zd2envz12zzsaw_bbvzd2gczd2,
		BgL_bgl_za762bbvza7d2gcza7d2ad1998za7,
		BGl_z62bbvzd2gczd2addzd2blockz12za2zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2regvzd2envz00zzsaw_bbvzd2gczd2,
		BgL_bgl_za762regsetza7d2regv1999z00,
		BGl_z62regsetzd2regvzb0zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2setz12zd2envzc0zzsaw_bbvzd2gczd2,
		BgL_bgl_za762regsetza7d2leng2000z00,
		BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2msiza7ezd2envza7zzsaw_bbvzd2gczd2,
		BgL_bgl_za762regsetza7d2msiza72001za7,
		BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bbvzd2gcz12zd2envz12zzsaw_bbvzd2gczd2,
		BgL_bgl_za762bbvza7d2gcza712za7a2002z00,
		BGl_z62bbvzd2gcz12za2zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2envzf2zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza72003za7,
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2setz12zd2envz32zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza72004za7,
		BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2envzf2zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza72005za7,
		BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2gczd2connectz12zd2envzc0zzsaw_bbvzd2gczd2,
		BgL_bgl_za762bbvza7d2gcza7d2co2006za7,
		BGl_z62bbvzd2gczd2connectz12z70zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2gczd2blockzd2reachablezf3zd2envzf3zzsaw_bbvzd2gczd2,
		BgL_bgl_za762bbvza7d2gcza7d2bl2007za7,
		BGl_z62bbvzd2gczd2blockzd2reachablezf3z43zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2nilzd2envzf2zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza72008za7,
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2envzf2zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza72009za7,
		BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2numzd2envzf2zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza72010za7,
		BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2setz12zd2envzc0zzsaw_bbvzd2gczd2,
		BgL_bgl_za762regsetza7d2stri2011z00,
		BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2setz12zd2envz32zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza72012za7,
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2envz01zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza72013za7,
		BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2gczd2disconnectz12zd2envzc0zzsaw_bbvzd2gczd2,
		BgL_bgl_za762bbvza7d2gcza7d2di2014za7,
		BGl_z62bbvzd2gczd2disconnectz12z70zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2stringzd2envz00zzsaw_bbvzd2gczd2,
		BgL_bgl_za762regsetza7d2stri2015z00,
		BGl_z62regsetzd2stringzb0zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2setz12zd2envz32zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza72016za7,
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2gczd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2rtl_regzf2razd2envzf2zzsaw_bbvzd2gczd2,
		BgL_bgl_za762makeza7d2rtl_re2017z00,
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razf3zd2envzd3zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza72018za7,
		BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1980z00zzsaw_bbvzd2gczd2,
		BgL_bgl_string1980za700za7za7s2019za7, "#~a[~a]", 7);
	      DEFINE_STRING(BGl_string1981z00zzsaw_bbvzd2gczd2,
		BgL_bgl_string1981za700za7za7s2020za7, "bbv-gc!<", 8);
	      DEFINE_STRING(BGl_string1982z00zzsaw_bbvzd2gczd2,
		BgL_bgl_string1982za700za7za7s2021za7, "bbv-gc!>", 8);
	      DEFINE_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2,
		BgL_bgl_string1983za700za7za7s2022za7, "saw_bbv-gc", 10);
	      DEFINE_STRING(BGl_string1984z00zzsaw_bbvzd2gczd2,
		BgL_bgl_string1984za700za7za7s2023za7, "ssr root cnt ", 13);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc1978z00zzsaw_bbvzd2gczd2,
		BgL_bgl_za762za7c3za704anonymo2024za7,
		BGl_z62zc3z04anonymousza31617ze3ze5zzsaw_bbvzd2gczd2);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc1979z00zzsaw_bbvzd2gczd2,
		BgL_bgl_za762za7c3za704anonymo2025za7,
		BGl_z62zc3z04anonymousza31702ze3ze5zzsaw_bbvzd2gczd2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2lengthzd2envz00zzsaw_bbvzd2gczd2,
		BgL_bgl_za762regsetza7d2leng2026z00,
		BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2envzf2zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza72027za7,
		BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2envzf2zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza72028za7,
		BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2hardwarezd2envzf2zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza72029za7,
		BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2regsetzd2envz00zzsaw_bbvzd2gczd2,
		BgL_bgl_za762makeza7d2regset2030z00,
		BGl_z62makezd2regsetzb0zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzf3zd2envz21zzsaw_bbvzd2gczd2,
		BgL_bgl_za762regsetza7f3za791za72031z00,
		BGl_z62regsetzf3z91zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2keyzd2envzf2zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza72032za7,
		BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2setz12zd2envz32zzsaw_bbvzd2gczd2,
		BgL_bgl_za762rtl_regza7f2raza72033za7,
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2gczd2, 0L, BUNSPEC,
		2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2reglzd2envz00zzsaw_bbvzd2gczd2,
		BgL_bgl_za762regsetza7d2regl2034z00,
		BGl_z62regsetzd2reglzb0zzsaw_bbvzd2gczd2, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2gczd2graphza2zd2zzsaw_bbvzd2gczd2));
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_bbvzd2gczd2));
		     ADD_ROOT((void *) (&BGl_za2gczd2blocksza2zd2zzsaw_bbvzd2gczd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2gczd2(long
		BgL_checksumz00_3212, char *BgL_fromz00_3213)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_bbvzd2gczd2))
				{
					BGl_requirezd2initializa7ationz75zzsaw_bbvzd2gczd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_bbvzd2gczd2();
					BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2gczd2();
					BGl_cnstzd2initzd2zzsaw_bbvzd2gczd2();
					BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2gczd2();
					return BGl_toplevelzd2initzd2zzsaw_bbvzd2gczd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2gczd2(void)
	{
		{	/* SawBbv/bbv-gc.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_bbv-gc");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "saw_bbv-gc");
			BGl_modulezd2initializa7ationz75zz__ssrz00(0L, "saw_bbv-gc");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_bbv-gc");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "saw_bbv-gc");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_bbv-gc");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_bbv-gc");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "saw_bbv-gc");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_bbv-gc");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_bbv-gc");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "saw_bbv-gc");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2gczd2(void)
	{
		{	/* SawBbv/bbv-gc.scm 15 */
			{	/* SawBbv/bbv-gc.scm 15 */
				obj_t BgL_cportz00_3105;

				{	/* SawBbv/bbv-gc.scm 15 */
					obj_t BgL_stringz00_3112;

					BgL_stringz00_3112 = BGl_string1984z00zzsaw_bbvzd2gczd2;
					{	/* SawBbv/bbv-gc.scm 15 */
						obj_t BgL_startz00_3113;

						BgL_startz00_3113 = BINT(0L);
						{	/* SawBbv/bbv-gc.scm 15 */
							obj_t BgL_endz00_3114;

							BgL_endz00_3114 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3112)));
							{	/* SawBbv/bbv-gc.scm 15 */

								BgL_cportz00_3105 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3112, BgL_startz00_3113, BgL_endz00_3114);
				}}}}
				{
					long BgL_iz00_3106;

					BgL_iz00_3106 = 2L;
				BgL_loopz00_3107:
					if ((BgL_iz00_3106 == -1L))
						{	/* SawBbv/bbv-gc.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* SawBbv/bbv-gc.scm 15 */
							{	/* SawBbv/bbv-gc.scm 15 */
								obj_t BgL_arg1985z00_3108;

								{	/* SawBbv/bbv-gc.scm 15 */

									{	/* SawBbv/bbv-gc.scm 15 */
										obj_t BgL_locationz00_3110;

										BgL_locationz00_3110 = BBOOL(((bool_t) 0));
										{	/* SawBbv/bbv-gc.scm 15 */

											BgL_arg1985z00_3108 =
												BGl_readz00zz__readerz00(BgL_cportz00_3105,
												BgL_locationz00_3110);
										}
									}
								}
								{	/* SawBbv/bbv-gc.scm 15 */
									int BgL_tmpz00_3242;

									BgL_tmpz00_3242 = (int) (BgL_iz00_3106);
									CNST_TABLE_SET(BgL_tmpz00_3242, BgL_arg1985z00_3108);
							}}
							{	/* SawBbv/bbv-gc.scm 15 */
								int BgL_auxz00_3111;

								BgL_auxz00_3111 = (int) ((BgL_iz00_3106 - 1L));
								{
									long BgL_iz00_3247;

									BgL_iz00_3247 = (long) (BgL_auxz00_3111);
									BgL_iz00_3106 = BgL_iz00_3247;
									goto BgL_loopz00_3107;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2gczd2(void)
	{
		{	/* SawBbv/bbv-gc.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2gczd2(void)
	{
		{	/* SawBbv/bbv-gc.scm 15 */
			BGl_za2gczd2markza2zd2zzsaw_bbvzd2gczd2 = 0L;
			BGl_za2gczd2graphza2zd2zzsaw_bbvzd2gczd2 = BFALSE;
			BGl_za2gczd2blocksza2zd2zzsaw_bbvzd2gczd2 = make_vector(16L, BUNSPEC);
			return (BGl_za2gczd2blockzd2lenza2z00zzsaw_bbvzd2gczd2 = 0L, BUNSPEC);
		}

	}



/* make-rtl_reg/ra */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2gczd2(BgL_typez00_bglt
		BgL_type1179z00_3, obj_t BgL_var1180z00_4, obj_t BgL_onexprzf31181zf3_5,
		obj_t BgL_name1182z00_6, obj_t BgL_key1183z00_7,
		obj_t BgL_hardware1184z00_8, int BgL_num1185z00_9,
		obj_t BgL_color1186z00_10, obj_t BgL_coalesce1187z00_11,
		int BgL_occurrences1188z00_12, obj_t BgL_interfere1189z00_13,
		obj_t BgL_interfere21190z00_14)
	{
		{	/* SawMill/regset.sch 55 */
			{	/* SawMill/regset.sch 55 */
				BgL_rtl_regz00_bglt BgL_new1166z00_3116;

				{	/* SawMill/regset.sch 55 */
					BgL_rtl_regz00_bglt BgL_tmp1164z00_3117;
					BgL_rtl_regzf2razf2_bglt BgL_wide1165z00_3118;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_3251;

						{	/* SawMill/regset.sch 55 */
							BgL_rtl_regz00_bglt BgL_new1163z00_3119;

							BgL_new1163z00_3119 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/regset.sch 55 */
								long BgL_arg1489z00_3120;

								BgL_arg1489z00_3120 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1163z00_3119),
									BgL_arg1489z00_3120);
							}
							{	/* SawMill/regset.sch 55 */
								BgL_objectz00_bglt BgL_tmpz00_3256;

								BgL_tmpz00_3256 = ((BgL_objectz00_bglt) BgL_new1163z00_3119);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3256, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1163z00_3119);
							BgL_auxz00_3251 = BgL_new1163z00_3119;
						}
						BgL_tmp1164z00_3117 = ((BgL_rtl_regz00_bglt) BgL_auxz00_3251);
					}
					BgL_wide1165z00_3118 =
						((BgL_rtl_regzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regzf2razf2_bgl))));
					{	/* SawMill/regset.sch 55 */
						obj_t BgL_auxz00_3264;
						BgL_objectz00_bglt BgL_tmpz00_3262;

						BgL_auxz00_3264 = ((obj_t) BgL_wide1165z00_3118);
						BgL_tmpz00_3262 = ((BgL_objectz00_bglt) BgL_tmp1164z00_3117);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3262, BgL_auxz00_3264);
					}
					((BgL_objectz00_bglt) BgL_tmp1164z00_3117);
					{	/* SawMill/regset.sch 55 */
						long BgL_arg1485z00_3121;

						BgL_arg1485z00_3121 =
							BGL_CLASS_NUM(BGl_rtl_regzf2razf2zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1164z00_3117), BgL_arg1485z00_3121);
					}
					BgL_new1166z00_3116 = ((BgL_rtl_regz00_bglt) BgL_tmp1164z00_3117);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1166z00_3116)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1179z00_3), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3116)))->BgL_varz00) =
					((obj_t) BgL_var1180z00_4), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3116)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31181zf3_5), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3116)))->BgL_namez00) =
					((obj_t) BgL_name1182z00_6), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3116)))->BgL_keyz00) =
					((obj_t) BgL_key1183z00_7), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3116)))->BgL_debugnamez00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3116)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1184z00_8), BUNSPEC);
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3286;

					{
						obj_t BgL_auxz00_3287;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3288;

							BgL_tmpz00_3288 = ((BgL_objectz00_bglt) BgL_new1166z00_3116);
							BgL_auxz00_3287 = BGL_OBJECT_WIDENING(BgL_tmpz00_3288);
						}
						BgL_auxz00_3286 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3287);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3286))->BgL_numz00) =
						((int) BgL_num1185z00_9), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3293;

					{
						obj_t BgL_auxz00_3294;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3295;

							BgL_tmpz00_3295 = ((BgL_objectz00_bglt) BgL_new1166z00_3116);
							BgL_auxz00_3294 = BGL_OBJECT_WIDENING(BgL_tmpz00_3295);
						}
						BgL_auxz00_3293 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3294);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3293))->
							BgL_colorz00) = ((obj_t) BgL_color1186z00_10), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3300;

					{
						obj_t BgL_auxz00_3301;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3302;

							BgL_tmpz00_3302 = ((BgL_objectz00_bglt) BgL_new1166z00_3116);
							BgL_auxz00_3301 = BGL_OBJECT_WIDENING(BgL_tmpz00_3302);
						}
						BgL_auxz00_3300 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3301);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3300))->
							BgL_coalescez00) = ((obj_t) BgL_coalesce1187z00_11), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3307;

					{
						obj_t BgL_auxz00_3308;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3309;

							BgL_tmpz00_3309 = ((BgL_objectz00_bglt) BgL_new1166z00_3116);
							BgL_auxz00_3308 = BGL_OBJECT_WIDENING(BgL_tmpz00_3309);
						}
						BgL_auxz00_3307 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3308);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3307))->
							BgL_occurrencesz00) = ((int) BgL_occurrences1188z00_12), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3314;

					{
						obj_t BgL_auxz00_3315;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3316;

							BgL_tmpz00_3316 = ((BgL_objectz00_bglt) BgL_new1166z00_3116);
							BgL_auxz00_3315 = BGL_OBJECT_WIDENING(BgL_tmpz00_3316);
						}
						BgL_auxz00_3314 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3315);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3314))->
							BgL_interferez00) = ((obj_t) BgL_interfere1189z00_13), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3321;

					{
						obj_t BgL_auxz00_3322;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3323;

							BgL_tmpz00_3323 = ((BgL_objectz00_bglt) BgL_new1166z00_3116);
							BgL_auxz00_3322 = BGL_OBJECT_WIDENING(BgL_tmpz00_3323);
						}
						BgL_auxz00_3321 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3322);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3321))->
							BgL_interfere2z00) = ((obj_t) BgL_interfere21190z00_14), BUNSPEC);
				}
				return BgL_new1166z00_3116;
			}
		}

	}



/* &make-rtl_reg/ra */
	BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_2964, obj_t BgL_type1179z00_2965, obj_t BgL_var1180z00_2966,
		obj_t BgL_onexprzf31181zf3_2967, obj_t BgL_name1182z00_2968,
		obj_t BgL_key1183z00_2969, obj_t BgL_hardware1184z00_2970,
		obj_t BgL_num1185z00_2971, obj_t BgL_color1186z00_2972,
		obj_t BgL_coalesce1187z00_2973, obj_t BgL_occurrences1188z00_2974,
		obj_t BgL_interfere1189z00_2975, obj_t BgL_interfere21190z00_2976)
	{
		{	/* SawMill/regset.sch 55 */
			return
				BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2gczd2(
				((BgL_typez00_bglt) BgL_type1179z00_2965), BgL_var1180z00_2966,
				BgL_onexprzf31181zf3_2967, BgL_name1182z00_2968, BgL_key1183z00_2969,
				BgL_hardware1184z00_2970, CINT(BgL_num1185z00_2971),
				BgL_color1186z00_2972, BgL_coalesce1187z00_2973,
				CINT(BgL_occurrences1188z00_2974), BgL_interfere1189z00_2975,
				BgL_interfere21190z00_2976);
		}

	}



/* rtl_reg/ra? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2gczd2(obj_t
		BgL_objz00_15)
	{
		{	/* SawMill/regset.sch 56 */
			{	/* SawMill/regset.sch 56 */
				obj_t BgL_classz00_3122;

				BgL_classz00_3122 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_15))
					{	/* SawMill/regset.sch 56 */
						BgL_objectz00_bglt BgL_arg1807z00_3123;

						BgL_arg1807z00_3123 = (BgL_objectz00_bglt) (BgL_objz00_15);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 56 */
								long BgL_idxz00_3124;

								BgL_idxz00_3124 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3123);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3124 + 2L)) == BgL_classz00_3122);
							}
						else
							{	/* SawMill/regset.sch 56 */
								bool_t BgL_res1975z00_3127;

								{	/* SawMill/regset.sch 56 */
									obj_t BgL_oclassz00_3128;

									{	/* SawMill/regset.sch 56 */
										obj_t BgL_arg1815z00_3129;
										long BgL_arg1816z00_3130;

										BgL_arg1815z00_3129 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 56 */
											long BgL_arg1817z00_3131;

											BgL_arg1817z00_3131 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3123);
											BgL_arg1816z00_3130 = (BgL_arg1817z00_3131 - OBJECT_TYPE);
										}
										BgL_oclassz00_3128 =
											VECTOR_REF(BgL_arg1815z00_3129, BgL_arg1816z00_3130);
									}
									{	/* SawMill/regset.sch 56 */
										bool_t BgL__ortest_1115z00_3132;

										BgL__ortest_1115z00_3132 =
											(BgL_classz00_3122 == BgL_oclassz00_3128);
										if (BgL__ortest_1115z00_3132)
											{	/* SawMill/regset.sch 56 */
												BgL_res1975z00_3127 = BgL__ortest_1115z00_3132;
											}
										else
											{	/* SawMill/regset.sch 56 */
												long BgL_odepthz00_3133;

												{	/* SawMill/regset.sch 56 */
													obj_t BgL_arg1804z00_3134;

													BgL_arg1804z00_3134 = (BgL_oclassz00_3128);
													BgL_odepthz00_3133 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3134);
												}
												if ((2L < BgL_odepthz00_3133))
													{	/* SawMill/regset.sch 56 */
														obj_t BgL_arg1802z00_3135;

														{	/* SawMill/regset.sch 56 */
															obj_t BgL_arg1803z00_3136;

															BgL_arg1803z00_3136 = (BgL_oclassz00_3128);
															BgL_arg1802z00_3135 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3136,
																2L);
														}
														BgL_res1975z00_3127 =
															(BgL_arg1802z00_3135 == BgL_classz00_3122);
													}
												else
													{	/* SawMill/regset.sch 56 */
														BgL_res1975z00_3127 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1975z00_3127;
							}
					}
				else
					{	/* SawMill/regset.sch 56 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_reg/ra? */
	obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2gczd2(obj_t BgL_envz00_2977,
		obj_t BgL_objz00_2978)
	{
		{	/* SawMill/regset.sch 56 */
			return BBOOL(BGl_rtl_regzf2razf3z01zzsaw_bbvzd2gczd2(BgL_objz00_2978));
		}

	}



/* rtl_reg/ra-nil */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2gczd2(void)
	{
		{	/* SawMill/regset.sch 57 */
			{	/* SawMill/regset.sch 57 */
				obj_t BgL_classz00_2831;

				BgL_classz00_2831 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				{	/* SawMill/regset.sch 57 */
					obj_t BgL__ortest_1117z00_2832;

					BgL__ortest_1117z00_2832 = BGL_CLASS_NIL(BgL_classz00_2831);
					if (CBOOL(BgL__ortest_1117z00_2832))
						{	/* SawMill/regset.sch 57 */
							return ((BgL_rtl_regz00_bglt) BgL__ortest_1117z00_2832);
						}
					else
						{	/* SawMill/regset.sch 57 */
							return
								((BgL_rtl_regz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2831));
						}
				}
			}
		}

	}



/* &rtl_reg/ra-nil */
	BgL_rtl_regz00_bglt BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_2979)
	{
		{	/* SawMill/regset.sch 57 */
			return BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2gczd2();
		}

	}



/* rtl_reg/ra-interfere2 */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt
		BgL_oz00_16)
	{
		{	/* SawMill/regset.sch 58 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3363;

				{
					obj_t BgL_auxz00_3364;

					{	/* SawMill/regset.sch 58 */
						BgL_objectz00_bglt BgL_tmpz00_3365;

						BgL_tmpz00_3365 = ((BgL_objectz00_bglt) BgL_oz00_16);
						BgL_auxz00_3364 = BGL_OBJECT_WIDENING(BgL_tmpz00_3365);
					}
					BgL_auxz00_3363 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3364);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3363))->
					BgL_interfere2z00);
			}
		}

	}



/* &rtl_reg/ra-interfere2 */
	obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_2980, obj_t BgL_oz00_2981)
	{
		{	/* SawMill/regset.sch 58 */
			return
				BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2981));
		}

	}



/* rtl_reg/ra-interfere2-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2gczd2
		(BgL_rtl_regz00_bglt BgL_oz00_17, obj_t BgL_vz00_18)
	{
		{	/* SawMill/regset.sch 59 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3372;

				{
					obj_t BgL_auxz00_3373;

					{	/* SawMill/regset.sch 59 */
						BgL_objectz00_bglt BgL_tmpz00_3374;

						BgL_tmpz00_3374 = ((BgL_objectz00_bglt) BgL_oz00_17);
						BgL_auxz00_3373 = BGL_OBJECT_WIDENING(BgL_tmpz00_3374);
					}
					BgL_auxz00_3372 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3373);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3372))->
						BgL_interfere2z00) = ((obj_t) BgL_vz00_18), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere2-set! */
	obj_t BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_2982, obj_t BgL_oz00_2983, obj_t BgL_vz00_2984)
	{
		{	/* SawMill/regset.sch 59 */
			return
				BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2983), BgL_vz00_2984);
		}

	}



/* rtl_reg/ra-interfere */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt
		BgL_oz00_19)
	{
		{	/* SawMill/regset.sch 60 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3381;

				{
					obj_t BgL_auxz00_3382;

					{	/* SawMill/regset.sch 60 */
						BgL_objectz00_bglt BgL_tmpz00_3383;

						BgL_tmpz00_3383 = ((BgL_objectz00_bglt) BgL_oz00_19);
						BgL_auxz00_3382 = BGL_OBJECT_WIDENING(BgL_tmpz00_3383);
					}
					BgL_auxz00_3381 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3382);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3381))->
					BgL_interferez00);
			}
		}

	}



/* &rtl_reg/ra-interfere */
	obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_2985, obj_t BgL_oz00_2986)
	{
		{	/* SawMill/regset.sch 60 */
			return
				BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2986));
		}

	}



/* rtl_reg/ra-interfere-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2gczd2
		(BgL_rtl_regz00_bglt BgL_oz00_20, obj_t BgL_vz00_21)
	{
		{	/* SawMill/regset.sch 61 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3390;

				{
					obj_t BgL_auxz00_3391;

					{	/* SawMill/regset.sch 61 */
						BgL_objectz00_bglt BgL_tmpz00_3392;

						BgL_tmpz00_3392 = ((BgL_objectz00_bglt) BgL_oz00_20);
						BgL_auxz00_3391 = BGL_OBJECT_WIDENING(BgL_tmpz00_3392);
					}
					BgL_auxz00_3390 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3391);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3390))->
						BgL_interferez00) = ((obj_t) BgL_vz00_21), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere-set! */
	obj_t BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_2987, obj_t BgL_oz00_2988, obj_t BgL_vz00_2989)
	{
		{	/* SawMill/regset.sch 61 */
			return
				BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2988), BgL_vz00_2989);
		}

	}



/* rtl_reg/ra-occurrences */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt
		BgL_oz00_22)
	{
		{	/* SawMill/regset.sch 62 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3399;

				{
					obj_t BgL_auxz00_3400;

					{	/* SawMill/regset.sch 62 */
						BgL_objectz00_bglt BgL_tmpz00_3401;

						BgL_tmpz00_3401 = ((BgL_objectz00_bglt) BgL_oz00_22);
						BgL_auxz00_3400 = BGL_OBJECT_WIDENING(BgL_tmpz00_3401);
					}
					BgL_auxz00_3399 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3400);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3399))->
					BgL_occurrencesz00);
			}
		}

	}



/* &rtl_reg/ra-occurrences */
	obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_2990, obj_t BgL_oz00_2991)
	{
		{	/* SawMill/regset.sch 62 */
			return
				BINT(BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2gczd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_2991)));
		}

	}



/* rtl_reg/ra-occurrences-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2gczd2
		(BgL_rtl_regz00_bglt BgL_oz00_23, int BgL_vz00_24)
	{
		{	/* SawMill/regset.sch 63 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3409;

				{
					obj_t BgL_auxz00_3410;

					{	/* SawMill/regset.sch 63 */
						BgL_objectz00_bglt BgL_tmpz00_3411;

						BgL_tmpz00_3411 = ((BgL_objectz00_bglt) BgL_oz00_23);
						BgL_auxz00_3410 = BGL_OBJECT_WIDENING(BgL_tmpz00_3411);
					}
					BgL_auxz00_3409 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3410);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3409))->
						BgL_occurrencesz00) = ((int) BgL_vz00_24), BUNSPEC);
		}}

	}



/* &rtl_reg/ra-occurrences-set! */
	obj_t BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_2992, obj_t BgL_oz00_2993, obj_t BgL_vz00_2994)
	{
		{	/* SawMill/regset.sch 63 */
			return
				BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2993), CINT(BgL_vz00_2994));
		}

	}



/* rtl_reg/ra-coalesce */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt
		BgL_oz00_25)
	{
		{	/* SawMill/regset.sch 64 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3419;

				{
					obj_t BgL_auxz00_3420;

					{	/* SawMill/regset.sch 64 */
						BgL_objectz00_bglt BgL_tmpz00_3421;

						BgL_tmpz00_3421 = ((BgL_objectz00_bglt) BgL_oz00_25);
						BgL_auxz00_3420 = BGL_OBJECT_WIDENING(BgL_tmpz00_3421);
					}
					BgL_auxz00_3419 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3420);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3419))->
					BgL_coalescez00);
			}
		}

	}



/* &rtl_reg/ra-coalesce */
	obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_2995, obj_t BgL_oz00_2996)
	{
		{	/* SawMill/regset.sch 64 */
			return
				BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2996));
		}

	}



/* rtl_reg/ra-coalesce-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt
		BgL_oz00_26, obj_t BgL_vz00_27)
	{
		{	/* SawMill/regset.sch 65 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3428;

				{
					obj_t BgL_auxz00_3429;

					{	/* SawMill/regset.sch 65 */
						BgL_objectz00_bglt BgL_tmpz00_3430;

						BgL_tmpz00_3430 = ((BgL_objectz00_bglt) BgL_oz00_26);
						BgL_auxz00_3429 = BGL_OBJECT_WIDENING(BgL_tmpz00_3430);
					}
					BgL_auxz00_3428 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3429);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3428))->
						BgL_coalescez00) = ((obj_t) BgL_vz00_27), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-coalesce-set! */
	obj_t BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_2997, obj_t BgL_oz00_2998, obj_t BgL_vz00_2999)
	{
		{	/* SawMill/regset.sch 65 */
			return
				BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2998), BgL_vz00_2999);
		}

	}



/* rtl_reg/ra-color */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt
		BgL_oz00_28)
	{
		{	/* SawMill/regset.sch 66 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3437;

				{
					obj_t BgL_auxz00_3438;

					{	/* SawMill/regset.sch 66 */
						BgL_objectz00_bglt BgL_tmpz00_3439;

						BgL_tmpz00_3439 = ((BgL_objectz00_bglt) BgL_oz00_28);
						BgL_auxz00_3438 = BGL_OBJECT_WIDENING(BgL_tmpz00_3439);
					}
					BgL_auxz00_3437 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3438);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3437))->BgL_colorz00);
			}
		}

	}



/* &rtl_reg/ra-color */
	obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2gczd2(obj_t BgL_envz00_3000,
		obj_t BgL_oz00_3001)
	{
		{	/* SawMill/regset.sch 66 */
			return
				BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3001));
		}

	}



/* rtl_reg/ra-color-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt
		BgL_oz00_29, obj_t BgL_vz00_30)
	{
		{	/* SawMill/regset.sch 67 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3446;

				{
					obj_t BgL_auxz00_3447;

					{	/* SawMill/regset.sch 67 */
						BgL_objectz00_bglt BgL_tmpz00_3448;

						BgL_tmpz00_3448 = ((BgL_objectz00_bglt) BgL_oz00_29);
						BgL_auxz00_3447 = BGL_OBJECT_WIDENING(BgL_tmpz00_3448);
					}
					BgL_auxz00_3446 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3447);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3446))->
						BgL_colorz00) = ((obj_t) BgL_vz00_30), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-color-set! */
	obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3002, obj_t BgL_oz00_3003, obj_t BgL_vz00_3004)
	{
		{	/* SawMill/regset.sch 67 */
			return
				BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3003), BgL_vz00_3004);
		}

	}



/* rtl_reg/ra-num */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt BgL_oz00_31)
	{
		{	/* SawMill/regset.sch 68 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3455;

				{
					obj_t BgL_auxz00_3456;

					{	/* SawMill/regset.sch 68 */
						BgL_objectz00_bglt BgL_tmpz00_3457;

						BgL_tmpz00_3457 = ((BgL_objectz00_bglt) BgL_oz00_31);
						BgL_auxz00_3456 = BGL_OBJECT_WIDENING(BgL_tmpz00_3457);
					}
					BgL_auxz00_3455 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3456);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3455))->BgL_numz00);
			}
		}

	}



/* &rtl_reg/ra-num */
	obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2gczd2(obj_t BgL_envz00_3005,
		obj_t BgL_oz00_3006)
	{
		{	/* SawMill/regset.sch 68 */
			return
				BINT(BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2gczd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_3006)));
		}

	}



/* rtl_reg/ra-hardware */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt
		BgL_oz00_34)
	{
		{	/* SawMill/regset.sch 70 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_34)))->BgL_hardwarez00);
		}

	}



/* &rtl_reg/ra-hardware */
	obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3007, obj_t BgL_oz00_3008)
	{
		{	/* SawMill/regset.sch 70 */
			return
				BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3008));
		}

	}



/* rtl_reg/ra-key */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt BgL_oz00_37)
	{
		{	/* SawMill/regset.sch 72 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_37)))->BgL_keyz00);
		}

	}



/* &rtl_reg/ra-key */
	obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2gczd2(obj_t BgL_envz00_3009,
		obj_t BgL_oz00_3010)
	{
		{	/* SawMill/regset.sch 72 */
			return
				BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3010));
		}

	}



/* rtl_reg/ra-name */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt BgL_oz00_40)
	{
		{	/* SawMill/regset.sch 74 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_40)))->BgL_namez00);
		}

	}



/* &rtl_reg/ra-name */
	obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2gczd2(obj_t BgL_envz00_3011,
		obj_t BgL_oz00_3012)
	{
		{	/* SawMill/regset.sch 74 */
			return
				BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3012));
		}

	}



/* rtl_reg/ra-onexpr? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt
		BgL_oz00_43)
	{
		{	/* SawMill/regset.sch 76 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_43)))->BgL_onexprzf3zf3);
		}

	}



/* &rtl_reg/ra-onexpr? */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3013, obj_t BgL_oz00_3014)
	{
		{	/* SawMill/regset.sch 76 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3014));
		}

	}



/* rtl_reg/ra-onexpr?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2gczd2
		(BgL_rtl_regz00_bglt BgL_oz00_44, obj_t BgL_vz00_45)
	{
		{	/* SawMill/regset.sch 77 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_44)))->BgL_onexprzf3zf3) =
				((obj_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &rtl_reg/ra-onexpr?-set! */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3015, obj_t BgL_oz00_3016, obj_t BgL_vz00_3017)
	{
		{	/* SawMill/regset.sch 77 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3016), BgL_vz00_3017);
		}

	}



/* rtl_reg/ra-var */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt BgL_oz00_46)
	{
		{	/* SawMill/regset.sch 78 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_46)))->BgL_varz00);
		}

	}



/* &rtl_reg/ra-var */
	obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2gczd2(obj_t BgL_envz00_3018,
		obj_t BgL_oz00_3019)
	{
		{	/* SawMill/regset.sch 78 */
			return
				BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3019));
		}

	}



/* rtl_reg/ra-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt
		BgL_oz00_47, obj_t BgL_vz00_48)
	{
		{	/* SawMill/regset.sch 79 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_47)))->BgL_varz00) =
				((obj_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &rtl_reg/ra-var-set! */
	obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3020, obj_t BgL_oz00_3021, obj_t BgL_vz00_3022)
	{
		{	/* SawMill/regset.sch 79 */
			return
				BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3021), BgL_vz00_3022);
		}

	}



/* rtl_reg/ra-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt BgL_oz00_49)
	{
		{	/* SawMill/regset.sch 80 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_49)))->BgL_typez00);
		}

	}



/* &rtl_reg/ra-type */
	BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3023, obj_t BgL_oz00_3024)
	{
		{	/* SawMill/regset.sch 80 */
			return
				BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3024));
		}

	}



/* rtl_reg/ra-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2gczd2(BgL_rtl_regz00_bglt
		BgL_oz00_50, BgL_typez00_bglt BgL_vz00_51)
	{
		{	/* SawMill/regset.sch 81 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_50)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_51), BUNSPEC);
		}

	}



/* &rtl_reg/ra-type-set! */
	obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3025, obj_t BgL_oz00_3026, obj_t BgL_vz00_3027)
	{
		{	/* SawMill/regset.sch 81 */
			return
				BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2gczd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3026),
				((BgL_typez00_bglt) BgL_vz00_3027));
		}

	}



/* make-regset */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt BGl_makezd2regsetzd2zzsaw_bbvzd2gczd2(int
		BgL_length1172z00_52, int BgL_msiza7e1173za7_53, obj_t BgL_regv1174z00_54,
		obj_t BgL_regl1175z00_55, obj_t BgL_string1176z00_56)
	{
		{	/* SawMill/regset.sch 84 */
			{	/* SawMill/regset.sch 84 */
				BgL_regsetz00_bglt BgL_new1168z00_3137;

				{	/* SawMill/regset.sch 84 */
					BgL_regsetz00_bglt BgL_new1167z00_3138;

					BgL_new1167z00_3138 =
						((BgL_regsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_regsetz00_bgl))));
					{	/* SawMill/regset.sch 84 */
						long BgL_arg1502z00_3139;

						BgL_arg1502z00_3139 = BGL_CLASS_NUM(BGl_regsetz00zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1167z00_3138), BgL_arg1502z00_3139);
					}
					BgL_new1168z00_3137 = BgL_new1167z00_3138;
				}
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3137))->BgL_lengthz00) =
					((int) BgL_length1172z00_52), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3137))->BgL_msiza7eza7) =
					((int) BgL_msiza7e1173za7_53), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3137))->BgL_regvz00) =
					((obj_t) BgL_regv1174z00_54), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3137))->BgL_reglz00) =
					((obj_t) BgL_regl1175z00_55), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3137))->BgL_stringz00) =
					((obj_t) BgL_string1176z00_56), BUNSPEC);
				return BgL_new1168z00_3137;
			}
		}

	}



/* &make-regset */
	BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3028, obj_t BgL_length1172z00_3029,
		obj_t BgL_msiza7e1173za7_3030, obj_t BgL_regv1174z00_3031,
		obj_t BgL_regl1175z00_3032, obj_t BgL_string1176z00_3033)
	{
		{	/* SawMill/regset.sch 84 */
			return
				BGl_makezd2regsetzd2zzsaw_bbvzd2gczd2(CINT(BgL_length1172z00_3029),
				CINT(BgL_msiza7e1173za7_3030), BgL_regv1174z00_3031,
				BgL_regl1175z00_3032, BgL_string1176z00_3033);
		}

	}



/* regset? */
	BGL_EXPORTED_DEF bool_t BGl_regsetzf3zf3zzsaw_bbvzd2gczd2(obj_t BgL_objz00_57)
	{
		{	/* SawMill/regset.sch 85 */
			{	/* SawMill/regset.sch 85 */
				obj_t BgL_classz00_3140;

				BgL_classz00_3140 = BGl_regsetz00zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_57))
					{	/* SawMill/regset.sch 85 */
						BgL_objectz00_bglt BgL_arg1807z00_3141;

						BgL_arg1807z00_3141 = (BgL_objectz00_bglt) (BgL_objz00_57);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 85 */
								long BgL_idxz00_3142;

								BgL_idxz00_3142 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3141);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3142 + 1L)) == BgL_classz00_3140);
							}
						else
							{	/* SawMill/regset.sch 85 */
								bool_t BgL_res1976z00_3145;

								{	/* SawMill/regset.sch 85 */
									obj_t BgL_oclassz00_3146;

									{	/* SawMill/regset.sch 85 */
										obj_t BgL_arg1815z00_3147;
										long BgL_arg1816z00_3148;

										BgL_arg1815z00_3147 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 85 */
											long BgL_arg1817z00_3149;

											BgL_arg1817z00_3149 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3141);
											BgL_arg1816z00_3148 = (BgL_arg1817z00_3149 - OBJECT_TYPE);
										}
										BgL_oclassz00_3146 =
											VECTOR_REF(BgL_arg1815z00_3147, BgL_arg1816z00_3148);
									}
									{	/* SawMill/regset.sch 85 */
										bool_t BgL__ortest_1115z00_3150;

										BgL__ortest_1115z00_3150 =
											(BgL_classz00_3140 == BgL_oclassz00_3146);
										if (BgL__ortest_1115z00_3150)
											{	/* SawMill/regset.sch 85 */
												BgL_res1976z00_3145 = BgL__ortest_1115z00_3150;
											}
										else
											{	/* SawMill/regset.sch 85 */
												long BgL_odepthz00_3151;

												{	/* SawMill/regset.sch 85 */
													obj_t BgL_arg1804z00_3152;

													BgL_arg1804z00_3152 = (BgL_oclassz00_3146);
													BgL_odepthz00_3151 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3152);
												}
												if ((1L < BgL_odepthz00_3151))
													{	/* SawMill/regset.sch 85 */
														obj_t BgL_arg1802z00_3153;

														{	/* SawMill/regset.sch 85 */
															obj_t BgL_arg1803z00_3154;

															BgL_arg1803z00_3154 = (BgL_oclassz00_3146);
															BgL_arg1802z00_3153 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3154,
																1L);
														}
														BgL_res1976z00_3145 =
															(BgL_arg1802z00_3153 == BgL_classz00_3140);
													}
												else
													{	/* SawMill/regset.sch 85 */
														BgL_res1976z00_3145 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1976z00_3145;
							}
					}
				else
					{	/* SawMill/regset.sch 85 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &regset? */
	obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2gczd2(obj_t BgL_envz00_3034,
		obj_t BgL_objz00_3035)
	{
		{	/* SawMill/regset.sch 85 */
			return BBOOL(BGl_regsetzf3zf3zzsaw_bbvzd2gczd2(BgL_objz00_3035));
		}

	}



/* regset-nil */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt BGl_regsetzd2nilzd2zzsaw_bbvzd2gczd2(void)
	{
		{	/* SawMill/regset.sch 86 */
			{	/* SawMill/regset.sch 86 */
				obj_t BgL_classz00_2884;

				BgL_classz00_2884 = BGl_regsetz00zzsaw_regsetz00;
				{	/* SawMill/regset.sch 86 */
					obj_t BgL__ortest_1117z00_2885;

					BgL__ortest_1117z00_2885 = BGL_CLASS_NIL(BgL_classz00_2884);
					if (CBOOL(BgL__ortest_1117z00_2885))
						{	/* SawMill/regset.sch 86 */
							return ((BgL_regsetz00_bglt) BgL__ortest_1117z00_2885);
						}
					else
						{	/* SawMill/regset.sch 86 */
							return
								((BgL_regsetz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2884));
						}
				}
			}
		}

	}



/* &regset-nil */
	BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3036)
	{
		{	/* SawMill/regset.sch 86 */
			return BGl_regsetzd2nilzd2zzsaw_bbvzd2gczd2();
		}

	}



/* regset-string */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2gczd2(BgL_regsetz00_bglt BgL_oz00_58)
	{
		{	/* SawMill/regset.sch 87 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_58))->BgL_stringz00);
		}

	}



/* &regset-string */
	obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2gczd2(obj_t BgL_envz00_3037,
		obj_t BgL_oz00_3038)
	{
		{	/* SawMill/regset.sch 87 */
			return
				BGl_regsetzd2stringzd2zzsaw_bbvzd2gczd2(
				((BgL_regsetz00_bglt) BgL_oz00_3038));
		}

	}



/* regset-string-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2gczd2(BgL_regsetz00_bglt
		BgL_oz00_59, obj_t BgL_vz00_60)
	{
		{	/* SawMill/regset.sch 88 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_59))->BgL_stringz00) =
				((obj_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &regset-string-set! */
	obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3039, obj_t BgL_oz00_3040, obj_t BgL_vz00_3041)
	{
		{	/* SawMill/regset.sch 88 */
			return
				BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2gczd2(
				((BgL_regsetz00_bglt) BgL_oz00_3040), BgL_vz00_3041);
		}

	}



/* regset-regl */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2gczd2(BgL_regsetz00_bglt BgL_oz00_61)
	{
		{	/* SawMill/regset.sch 89 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_61))->BgL_reglz00);
		}

	}



/* &regset-regl */
	obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2gczd2(obj_t BgL_envz00_3042,
		obj_t BgL_oz00_3043)
	{
		{	/* SawMill/regset.sch 89 */
			return
				BGl_regsetzd2reglzd2zzsaw_bbvzd2gczd2(
				((BgL_regsetz00_bglt) BgL_oz00_3043));
		}

	}



/* regset-regv */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2gczd2(BgL_regsetz00_bglt BgL_oz00_64)
	{
		{	/* SawMill/regset.sch 91 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_64))->BgL_regvz00);
		}

	}



/* &regset-regv */
	obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2gczd2(obj_t BgL_envz00_3044,
		obj_t BgL_oz00_3045)
	{
		{	/* SawMill/regset.sch 91 */
			return
				BGl_regsetzd2regvzd2zzsaw_bbvzd2gczd2(
				((BgL_regsetz00_bglt) BgL_oz00_3045));
		}

	}



/* regset-msize */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2gczd2(BgL_regsetz00_bglt BgL_oz00_67)
	{
		{	/* SawMill/regset.sch 93 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_67))->BgL_msiza7eza7);
		}

	}



/* &regset-msize */
	obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2gczd2(obj_t BgL_envz00_3046,
		obj_t BgL_oz00_3047)
	{
		{	/* SawMill/regset.sch 93 */
			return
				BINT(BGl_regsetzd2msiza7ez75zzsaw_bbvzd2gczd2(
					((BgL_regsetz00_bglt) BgL_oz00_3047)));
		}

	}



/* regset-length */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2gczd2(BgL_regsetz00_bglt BgL_oz00_70)
	{
		{	/* SawMill/regset.sch 95 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_70))->BgL_lengthz00);
		}

	}



/* &regset-length */
	obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2gczd2(obj_t BgL_envz00_3048,
		obj_t BgL_oz00_3049)
	{
		{	/* SawMill/regset.sch 95 */
			return
				BINT(BGl_regsetzd2lengthzd2zzsaw_bbvzd2gczd2(
					((BgL_regsetz00_bglt) BgL_oz00_3049)));
		}

	}



/* regset-length-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2gczd2(BgL_regsetz00_bglt
		BgL_oz00_71, int BgL_vz00_72)
	{
		{	/* SawMill/regset.sch 96 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_71))->BgL_lengthz00) =
				((int) BgL_vz00_72), BUNSPEC);
		}

	}



/* &regset-length-set! */
	obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3050, obj_t BgL_oz00_3051, obj_t BgL_vz00_3052)
	{
		{	/* SawMill/regset.sch 96 */
			return
				BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2gczd2(
				((BgL_regsetz00_bglt) BgL_oz00_3051), CINT(BgL_vz00_3052));
		}

	}



/* bbv-gc! */
	BGL_EXPORTED_DEF obj_t BGl_bbvzd2gcz12zc0zzsaw_bbvzd2gczd2(BgL_blockz00_bglt
		BgL_bvz00_91)
	{
		{	/* SawBbv/bbv-gc.scm 52 */
			if (
				(BGl_za2bbvzd2blockszd2gcza2z00zzsaw_bbvzd2configzd2 ==
					CNST_TABLE_REF(0)))
				{	/* SawBbv/bbv-gc.scm 57 */
					long BgL_m0z00_2225;
					obj_t BgL_lz00_2226;

					BgL_m0z00_2225 = BGl_getzd2gczd2markz12z12zzsaw_bbvzd2gczd2();
					BgL_lz00_2226 =
						BGl_blockVzd2livezd2versionsz00zzsaw_bbvzd2typeszd2(BgL_bvz00_91);
					if (CBOOL(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2))
						{	/* SawBbv/bbv-gc.scm 62 */
							long BgL_mz00_2227;
							obj_t BgL_dz00_3079;

							BgL_mz00_2227 = BGl_getzd2gczd2markz12z12zzsaw_bbvzd2gczd2();
							BgL_dz00_3079 = MAKE_CELL(BNIL);
							{
								obj_t BgL_l1420z00_2230;

								BgL_l1420z00_2230 = BgL_lz00_2226;
							BgL_zc3z04anonymousza31594ze3z87_2231:
								if (PAIRP(BgL_l1420z00_2230))
									{	/* SawBbv/bbv-gc.scm 64 */
										{	/* SawBbv/bbv-gc.scm 65 */
											obj_t BgL_bz00_2233;

											BgL_bz00_2233 = CAR(BgL_l1420z00_2230);
											{	/* SawBbv/bbv-gc.scm 67 */
												obj_t BgL_zc3z04anonymousza31603ze3z87_3058;

												{
													int BgL_tmpz00_3580;

													BgL_tmpz00_3580 = (int) (1L);
													BgL_zc3z04anonymousza31603ze3z87_3058 =
														MAKE_L_PROCEDURE
														(BGl_z62zc3z04anonymousza31603ze3ze5zzsaw_bbvzd2gczd2,
														BgL_tmpz00_3580);
												}
												PROCEDURE_L_SET(BgL_zc3z04anonymousza31603ze3z87_3058,
													(int) (0L), ((obj_t) BgL_dz00_3079));
												BGl_walkbsz12z12zzsaw_bbvzd2gczd2(
													((BgL_blockz00_bglt) BgL_bz00_2233), BgL_mz00_2227,
													BgL_zc3z04anonymousza31603ze3z87_3058);
										}}
										{
											obj_t BgL_l1420z00_3588;

											BgL_l1420z00_3588 = CDR(BgL_l1420z00_2230);
											BgL_l1420z00_2230 = BgL_l1420z00_3588;
											goto BgL_zc3z04anonymousza31594ze3z87_2231;
										}
									}
								else
									{	/* SawBbv/bbv-gc.scm 64 */
										((bool_t) 1);
									}
							}
							BUNSPEC;
						}
					else
						{	/* SawBbv/bbv-gc.scm 60 */
							BFALSE;
						}
					BGl_walkbvz12z12zzsaw_bbvzd2gczd2(BgL_bvz00_91,
						BGl_getzd2gczd2markz12z12zzsaw_bbvzd2gczd2(),
						BGl_proc1978z00zzsaw_bbvzd2gczd2);
					{
						obj_t BgL_l1425z00_2261;

						BgL_l1425z00_2261 = BgL_lz00_2226;
					BgL_zc3z04anonymousza31626ze3z87_2262:
						if (PAIRP(BgL_l1425z00_2261))
							{	/* SawBbv/bbv-gc.scm 86 */
								{	/* SawBbv/bbv-gc.scm 87 */
									obj_t BgL_bz00_2264;

									BgL_bz00_2264 = CAR(BgL_l1425z00_2261);
									{	/* SawBbv/bbv-gc.scm 88 */
										bool_t BgL_test2051z00_3595;

										{	/* SawBbv/bbv-gc.scm 88 */
											obj_t BgL_arg1642z00_2268;

											{
												BgL_blocksz00_bglt BgL_auxz00_3596;

												{
													obj_t BgL_auxz00_3597;

													{	/* SawBbv/bbv-gc.scm 88 */
														BgL_objectz00_bglt BgL_tmpz00_3598;

														BgL_tmpz00_3598 =
															((BgL_objectz00_bglt)
															((BgL_blockz00_bglt) BgL_bz00_2264));
														BgL_auxz00_3597 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3598);
													}
													BgL_auxz00_3596 =
														((BgL_blocksz00_bglt) BgL_auxz00_3597);
												}
												BgL_arg1642z00_2268 =
													(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_3596))->
													BgL_creatorz00);
											}
											BgL_test2051z00_3595 =
												(BgL_arg1642z00_2268 == CNST_TABLE_REF(1));
										}
										if (BgL_test2051z00_3595)
											{
												BgL_blocksz00_bglt BgL_auxz00_3606;

												{
													obj_t BgL_auxz00_3607;

													{	/* SawBbv/bbv-gc.scm 89 */
														BgL_objectz00_bglt BgL_tmpz00_3608;

														BgL_tmpz00_3608 =
															((BgL_objectz00_bglt)
															((BgL_blockz00_bglt) BgL_bz00_2264));
														BgL_auxz00_3607 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3608);
													}
													BgL_auxz00_3606 =
														((BgL_blocksz00_bglt) BgL_auxz00_3607);
												}
												((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_3606))->
														BgL_gccntz00) = ((long) 1L), BUNSPEC);
											}
										else
											{	/* SawBbv/bbv-gc.scm 88 */
												BFALSE;
											}
									}
								}
								{
									obj_t BgL_l1425z00_3614;

									BgL_l1425z00_3614 = CDR(BgL_l1425z00_2261);
									BgL_l1425z00_2261 = BgL_l1425z00_3614;
									goto BgL_zc3z04anonymousza31626ze3z87_2262;
								}
							}
						else
							{	/* SawBbv/bbv-gc.scm 86 */
								((bool_t) 1);
							}
					}
					{	/* SawBbv/bbv-gc.scm 92 */
						long BgL_mz00_2271;

						BgL_mz00_2271 = BGl_getzd2gczd2markz12z12zzsaw_bbvzd2gczd2();
						{
							obj_t BgL_l1427z00_2273;

							BgL_l1427z00_2273 = BgL_lz00_2226;
						BgL_zc3z04anonymousza31647ze3z87_2274:
							if (PAIRP(BgL_l1427z00_2273))
								{	/* SawBbv/bbv-gc.scm 93 */
									{	/* SawBbv/bbv-gc.scm 94 */
										obj_t BgL_bz00_2276;

										BgL_bz00_2276 = CAR(BgL_l1427z00_2273);
										{	/* SawBbv/bbv-gc.scm 96 */
											obj_t BgL_zc3z04anonymousza31651ze3z87_3056;

											{
												int BgL_tmpz00_3620;

												BgL_tmpz00_3620 = (int) (1L);
												BgL_zc3z04anonymousza31651ze3z87_3056 =
													MAKE_L_PROCEDURE
													(BGl_z62zc3z04anonymousza31651ze3ze5zzsaw_bbvzd2gczd2,
													BgL_tmpz00_3620);
											}
											PROCEDURE_L_SET(BgL_zc3z04anonymousza31651ze3z87_3056,
												(int) (0L), BINT(BgL_m0z00_2225));
											BGl_walkbsz12z12zzsaw_bbvzd2gczd2(
												((BgL_blockz00_bglt) BgL_bz00_2276), BgL_mz00_2271,
												BgL_zc3z04anonymousza31651ze3z87_3056);
									}}
									{
										obj_t BgL_l1427z00_3628;

										BgL_l1427z00_3628 = CDR(BgL_l1427z00_2273);
										BgL_l1427z00_2273 = BgL_l1427z00_3628;
										goto BgL_zc3z04anonymousza31647ze3z87_2274;
									}
								}
							else
								{	/* SawBbv/bbv-gc.scm 93 */
									((bool_t) 1);
								}
						}
					}
					{	/* SawBbv/bbv-gc.scm 101 */
						long BgL_mz00_2285;

						BgL_mz00_2285 = BGl_getzd2gczd2markz12z12zzsaw_bbvzd2gczd2();
						{
							obj_t BgL_l1435z00_2287;

							BgL_l1435z00_2287 = BgL_lz00_2226;
						BgL_zc3z04anonymousza31662ze3z87_2288:
							if (PAIRP(BgL_l1435z00_2287))
								{	/* SawBbv/bbv-gc.scm 102 */
									{	/* SawBbv/bbv-gc.scm 103 */
										obj_t BgL_bz00_2290;

										BgL_bz00_2290 = CAR(BgL_l1435z00_2287);
										{	/* SawBbv/bbv-gc.scm 105 */
											obj_t BgL_zc3z04anonymousza31676ze3z87_3055;

											{
												int BgL_tmpz00_3634;

												BgL_tmpz00_3634 = (int) (1L);
												BgL_zc3z04anonymousza31676ze3z87_3055 =
													MAKE_L_PROCEDURE
													(BGl_z62zc3z04anonymousza31676ze3ze5zzsaw_bbvzd2gczd2,
													BgL_tmpz00_3634);
											}
											PROCEDURE_L_SET(BgL_zc3z04anonymousza31676ze3z87_3055,
												(int) (0L), BINT(BgL_m0z00_2225));
											BGl_walkbsz12z12zzsaw_bbvzd2gczd2(
												((BgL_blockz00_bglt) BgL_bz00_2290), BgL_mz00_2285,
												BgL_zc3z04anonymousza31676ze3z87_3055);
									}}
									{
										obj_t BgL_l1435z00_3642;

										BgL_l1435z00_3642 = CDR(BgL_l1435z00_2287);
										BgL_l1435z00_2287 = BgL_l1435z00_3642;
										goto BgL_zc3z04anonymousza31662ze3z87_2288;
									}
								}
							else
								{	/* SawBbv/bbv-gc.scm 102 */
									((bool_t) 1);
								}
						}
					}
					BGl_walkbvz12z12zzsaw_bbvzd2gczd2(BgL_bvz00_91,
						BGl_getzd2gczd2markz12z12zzsaw_bbvzd2gczd2(),
						BGl_proc1979z00zzsaw_bbvzd2gczd2);
					if (CBOOL(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2))
						{	/* SawBbv/bbv-gc.scm 126 */
							long BgL_mz00_2335;
							obj_t BgL_dz00_3081;

							BgL_mz00_2335 = BGl_getzd2gczd2markz12z12zzsaw_bbvzd2gczd2();
							BgL_dz00_3081 = MAKE_CELL(BNIL);
							{
								obj_t BgL_l1440z00_2338;

								BgL_l1440z00_2338 = BgL_lz00_2226;
							BgL_zc3z04anonymousza31710ze3z87_2339:
								if (PAIRP(BgL_l1440z00_2338))
									{	/* SawBbv/bbv-gc.scm 128 */
										{	/* SawBbv/bbv-gc.scm 129 */
											obj_t BgL_bz00_2341;

											BgL_bz00_2341 = CAR(BgL_l1440z00_2338);
											{	/* SawBbv/bbv-gc.scm 131 */
												obj_t BgL_zc3z04anonymousza31715ze3z87_3053;

												{
													int BgL_tmpz00_3652;

													BgL_tmpz00_3652 = (int) (1L);
													BgL_zc3z04anonymousza31715ze3z87_3053 =
														MAKE_L_PROCEDURE
														(BGl_z62zc3z04anonymousza31715ze3ze5zzsaw_bbvzd2gczd2,
														BgL_tmpz00_3652);
												}
												PROCEDURE_L_SET(BgL_zc3z04anonymousza31715ze3z87_3053,
													(int) (0L), ((obj_t) BgL_dz00_3081));
												BGl_walkbsz12z12zzsaw_bbvzd2gczd2(
													((BgL_blockz00_bglt) BgL_bz00_2341), BgL_mz00_2335,
													BgL_zc3z04anonymousza31715ze3z87_3053);
										}}
										{
											obj_t BgL_l1440z00_3660;

											BgL_l1440z00_3660 = CDR(BgL_l1440z00_2338);
											BgL_l1440z00_2338 = BgL_l1440z00_3660;
											goto BgL_zc3z04anonymousza31710ze3z87_2339;
										}
									}
								else
									{	/* SawBbv/bbv-gc.scm 128 */
										((bool_t) 1);
									}
							}
							return BUNSPEC;
						}
					else
						{	/* SawBbv/bbv-gc.scm 124 */
							return BFALSE;
						}
				}
			else
				{	/* SawBbv/bbv-gc.scm 55 */
					return BFALSE;
				}
		}

	}



/* &bbv-gc! */
	obj_t BGl_z62bbvzd2gcz12za2zzsaw_bbvzd2gczd2(obj_t BgL_envz00_3059,
		obj_t BgL_bvz00_3060)
	{
		{	/* SawBbv/bbv-gc.scm 52 */
			return
				BGl_bbvzd2gcz12zc0zzsaw_bbvzd2gczd2(
				((BgL_blockz00_bglt) BgL_bvz00_3060));
		}

	}



/* &<@anonymous:1715> */
	obj_t BGl_z62zc3z04anonymousza31715ze3ze5zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3061, BgL_blockz00_bglt BgL_bz00_3063)
	{
		{	/* SawBbv/bbv-gc.scm 130 */
			{	/* SawBbv/bbv-gc.scm 131 */
				obj_t BgL_dz00_3062;

				BgL_dz00_3062 = PROCEDURE_L_REF(BgL_envz00_3061, (int) (0L));
				{	/* SawBbv/bbv-gc.scm 131 */
					obj_t BgL_auxz00_3156;

					{	/* SawBbv/bbv-gc.scm 132 */
						obj_t BgL_arg1717z00_3157;

						{	/* SawBbv/bbv-gc.scm 132 */
							int BgL_arg1718z00_3158;
							long BgL_arg1720z00_3159;

							BgL_arg1718z00_3158 =
								(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_bz00_3063)))->BgL_labelz00);
							{
								BgL_blocksz00_bglt BgL_auxz00_3668;

								{
									obj_t BgL_auxz00_3669;

									{	/* SawBbv/bbv-gc.scm 133 */
										BgL_objectz00_bglt BgL_tmpz00_3670;

										BgL_tmpz00_3670 = ((BgL_objectz00_bglt) BgL_bz00_3063);
										BgL_auxz00_3669 = BGL_OBJECT_WIDENING(BgL_tmpz00_3670);
									}
									BgL_auxz00_3668 = ((BgL_blocksz00_bglt) BgL_auxz00_3669);
								}
								BgL_arg1720z00_3159 =
									(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_3668))->
									BgL_gccntz00);
							}
							{	/* SawBbv/bbv-gc.scm 131 */
								obj_t BgL_list1721z00_3160;

								{	/* SawBbv/bbv-gc.scm 131 */
									obj_t BgL_arg1722z00_3161;

									BgL_arg1722z00_3161 =
										MAKE_YOUNG_PAIR(BINT(BgL_arg1720z00_3159), BNIL);
									BgL_list1721z00_3160 =
										MAKE_YOUNG_PAIR(BINT(BgL_arg1718z00_3158),
										BgL_arg1722z00_3161);
								}
								BgL_arg1717z00_3157 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string1980z00zzsaw_bbvzd2gczd2, BgL_list1721z00_3160);
						}}
						BgL_auxz00_3156 =
							MAKE_YOUNG_PAIR(BgL_arg1717z00_3157, CELL_REF(BgL_dz00_3062));
					}
					CELL_SET(BgL_dz00_3062, BgL_auxz00_3156);
				}
				return
					BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(BgL_bz00_3063,
					BGl_string1981z00zzsaw_bbvzd2gczd2);
			}
		}

	}



/* &<@anonymous:1702> */
	bool_t BGl_z62zc3z04anonymousza31702ze3ze5zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3065, BgL_blockz00_bglt BgL_bz00_3066)
	{
		{	/* SawBbv/bbv-gc.scm 114 */
			{	/* SawBbv/bbv-gc.scm 116 */
				obj_t BgL_g1439z00_3163;

				{
					BgL_blockvz00_bglt BgL_auxz00_3682;

					{
						obj_t BgL_auxz00_3683;

						{	/* SawBbv/bbv-gc.scm 116 */
							BgL_objectz00_bglt BgL_tmpz00_3684;

							BgL_tmpz00_3684 = ((BgL_objectz00_bglt) BgL_bz00_3066);
							BgL_auxz00_3683 = BGL_OBJECT_WIDENING(BgL_tmpz00_3684);
						}
						BgL_auxz00_3682 = ((BgL_blockvz00_bglt) BgL_auxz00_3683);
					}
					BgL_g1439z00_3163 =
						(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_3682))->BgL_versionsz00);
				}
				{
					obj_t BgL_l1437z00_3165;

					BgL_l1437z00_3165 = BgL_g1439z00_3163;
				BgL_zc3z04anonymousza31703ze3z87_3164:
					if (PAIRP(BgL_l1437z00_3165))
						{	/* SawBbv/bbv-gc.scm 116 */
							{	/* SawBbv/bbv-gc.scm 117 */
								obj_t BgL_bz00_3166;

								BgL_bz00_3166 = CAR(BgL_l1437z00_3165);
								{	/* SawBbv/bbv-gc.scm 118 */
									bool_t BgL_test2057z00_3692;

									{	/* SawBbv/bbv-gc.scm 118 */
										long BgL_arg1708z00_3167;

										{
											BgL_blocksz00_bglt BgL_auxz00_3693;

											{
												obj_t BgL_auxz00_3694;

												{	/* SawBbv/bbv-gc.scm 118 */
													BgL_objectz00_bglt BgL_tmpz00_3695;

													BgL_tmpz00_3695 =
														((BgL_objectz00_bglt)
														((BgL_blockz00_bglt) BgL_bz00_3166));
													BgL_auxz00_3694 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3695);
												}
												BgL_auxz00_3693 =
													((BgL_blocksz00_bglt) BgL_auxz00_3694);
											}
											BgL_arg1708z00_3167 =
												(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_3693))->
												BgL_gccntz00);
										}
										BgL_test2057z00_3692 = (BgL_arg1708z00_3167 == 0L);
									}
									if (BgL_test2057z00_3692)
										{	/* SawBbv/bbv-gc.scm 118 */
											{
												BgL_blocksz00_bglt BgL_auxz00_3702;

												{
													obj_t BgL_auxz00_3703;

													{	/* SawBbv/bbv-gc.scm 119 */
														BgL_objectz00_bglt BgL_tmpz00_3704;

														BgL_tmpz00_3704 =
															((BgL_objectz00_bglt)
															((BgL_blockz00_bglt) BgL_bz00_3166));
														BgL_auxz00_3703 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_3704);
													}
													BgL_auxz00_3702 =
														((BgL_blocksz00_bglt) BgL_auxz00_3703);
												}
												((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_3702))->
														BgL_asleepz00) = ((bool_t) ((bool_t) 1)), BUNSPEC);
											}
											((((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt)
																((BgL_blockz00_bglt) BgL_bz00_3166))))->
													BgL_succsz00) = ((obj_t) BNIL), BUNSPEC);
											((((BgL_blockz00_bglt)
														COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
																	BgL_bz00_3166))))->BgL_predsz00) =
												((obj_t) BNIL), BUNSPEC);
										}
									else
										{	/* SawBbv/bbv-gc.scm 118 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1437z00_3716;

								BgL_l1437z00_3716 = CDR(BgL_l1437z00_3165);
								BgL_l1437z00_3165 = BgL_l1437z00_3716;
								goto BgL_zc3z04anonymousza31703ze3z87_3164;
							}
						}
					else
						{	/* SawBbv/bbv-gc.scm 116 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* &<@anonymous:1676> */
	obj_t BGl_z62zc3z04anonymousza31676ze3ze5zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3067, BgL_blockz00_bglt BgL_bz00_3069)
	{
		{	/* SawBbv/bbv-gc.scm 104 */
			{	/* SawBbv/bbv-gc.scm 105 */
				long BgL_m0z00_3068;

				BgL_m0z00_3068 =
					(long) CINT(PROCEDURE_L_REF(BgL_envz00_3067, (int) (0L)));
				{
					obj_t BgL_auxz00_3721;

					{	/* SawBbv/bbv-gc.scm 107 */
						obj_t BgL_hook1433z00_3171;

						BgL_hook1433z00_3171 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
						{	/* SawBbv/bbv-gc.scm 107 */
							obj_t BgL_g1434z00_3172;

							BgL_g1434z00_3172 =
								(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_bz00_3069)))->BgL_predsz00);
							{
								obj_t BgL_l1430z00_3174;
								obj_t BgL_h1431z00_3175;

								BgL_l1430z00_3174 = BgL_g1434z00_3172;
								BgL_h1431z00_3175 = BgL_hook1433z00_3171;
							BgL_zc3z04anonymousza31677ze3z87_3173:
								if (NULLP(BgL_l1430z00_3174))
									{	/* SawBbv/bbv-gc.scm 107 */
										BgL_auxz00_3721 = CDR(BgL_hook1433z00_3171);
									}
								else
									{	/* SawBbv/bbv-gc.scm 107 */
										bool_t BgL_test2059z00_3729;

										{	/* SawBbv/bbv-gc.scm 108 */
											BgL_blockz00_bglt BgL_i1178z00_3176;

											BgL_i1178z00_3176 =
												((BgL_blockz00_bglt) CAR(((obj_t) BgL_l1430z00_3174)));
											{	/* SawBbv/bbv-gc.scm 109 */
												long BgL_arg1692z00_3177;

												{
													BgL_blocksz00_bglt BgL_auxz00_3733;

													{
														obj_t BgL_auxz00_3734;

														{	/* SawBbv/bbv-gc.scm 109 */
															BgL_objectz00_bglt BgL_tmpz00_3735;

															BgL_tmpz00_3735 =
																((BgL_objectz00_bglt) BgL_i1178z00_3176);
															BgL_auxz00_3734 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_3735);
														}
														BgL_auxz00_3733 =
															((BgL_blocksz00_bglt) BgL_auxz00_3734);
													}
													BgL_arg1692z00_3177 =
														(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_3733))->
														BgL_gcmarkz00);
												}
												BgL_test2059z00_3729 =
													(BgL_arg1692z00_3177 == BgL_m0z00_3068);
										}}
										if (BgL_test2059z00_3729)
											{	/* SawBbv/bbv-gc.scm 107 */
												obj_t BgL_nh1432z00_3180;

												{	/* SawBbv/bbv-gc.scm 107 */
													obj_t BgL_arg1689z00_3181;

													BgL_arg1689z00_3181 =
														CAR(((obj_t) BgL_l1430z00_3174));
													BgL_nh1432z00_3180 =
														MAKE_YOUNG_PAIR(BgL_arg1689z00_3181, BNIL);
												}
												SET_CDR(BgL_h1431z00_3175, BgL_nh1432z00_3180);
												{	/* SawBbv/bbv-gc.scm 107 */
													obj_t BgL_arg1688z00_3182;

													BgL_arg1688z00_3182 =
														CDR(((obj_t) BgL_l1430z00_3174));
													{
														obj_t BgL_h1431z00_3748;
														obj_t BgL_l1430z00_3747;

														BgL_l1430z00_3747 = BgL_arg1688z00_3182;
														BgL_h1431z00_3748 = BgL_nh1432z00_3180;
														BgL_h1431z00_3175 = BgL_h1431z00_3748;
														BgL_l1430z00_3174 = BgL_l1430z00_3747;
														goto BgL_zc3z04anonymousza31677ze3z87_3173;
													}
												}
											}
										else
											{	/* SawBbv/bbv-gc.scm 107 */
												obj_t BgL_arg1691z00_3183;

												BgL_arg1691z00_3183 = CDR(((obj_t) BgL_l1430z00_3174));
												{
													obj_t BgL_l1430z00_3751;

													BgL_l1430z00_3751 = BgL_arg1691z00_3183;
													BgL_l1430z00_3174 = BgL_l1430z00_3751;
													goto BgL_zc3z04anonymousza31677ze3z87_3173;
												}
											}
									}
							}
						}
					}
					return
						((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_bz00_3069)))->BgL_predsz00) =
						((obj_t) BgL_auxz00_3721), BUNSPEC);
				}
			}
		}

	}



/* &<@anonymous:1651> */
	obj_t BGl_z62zc3z04anonymousza31651ze3ze5zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3070, BgL_blockz00_bglt BgL_bz00_3072)
	{
		{	/* SawBbv/bbv-gc.scm 95 */
			{	/* SawBbv/bbv-gc.scm 96 */
				long BgL_m0z00_3071;

				BgL_m0z00_3071 =
					(long) CINT(PROCEDURE_L_REF(BgL_envz00_3070, (int) (0L)));
				{
					BgL_blocksz00_bglt BgL_auxz00_3756;

					{
						obj_t BgL_auxz00_3757;

						{	/* SawBbv/bbv-gc.scm 97 */
							BgL_objectz00_bglt BgL_tmpz00_3758;

							BgL_tmpz00_3758 = ((BgL_objectz00_bglt) BgL_bz00_3072);
							BgL_auxz00_3757 = BGL_OBJECT_WIDENING(BgL_tmpz00_3758);
						}
						BgL_auxz00_3756 = ((BgL_blocksz00_bglt) BgL_auxz00_3757);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_3756))->BgL_gcmarkz00) =
						((long) BgL_m0z00_3071), BUNSPEC);
				}
				{
					long BgL_auxz00_3769;
					BgL_blocksz00_bglt BgL_auxz00_3763;

					{	/* SawBbv/bbv-gc.scm 98 */
						long BgL_arg1654z00_3185;

						{
							BgL_blocksz00_bglt BgL_auxz00_3770;

							{
								obj_t BgL_auxz00_3771;

								{	/* SawBbv/bbv-gc.scm 98 */
									BgL_objectz00_bglt BgL_tmpz00_3772;

									BgL_tmpz00_3772 = ((BgL_objectz00_bglt) BgL_bz00_3072);
									BgL_auxz00_3771 = BGL_OBJECT_WIDENING(BgL_tmpz00_3772);
								}
								BgL_auxz00_3770 = ((BgL_blocksz00_bglt) BgL_auxz00_3771);
							}
							BgL_arg1654z00_3185 =
								(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_3770))->BgL_gccntz00);
						}
						BgL_auxz00_3769 = (BgL_arg1654z00_3185 + 1L);
					}
					{
						obj_t BgL_auxz00_3764;

						{	/* SawBbv/bbv-gc.scm 98 */
							BgL_objectz00_bglt BgL_tmpz00_3765;

							BgL_tmpz00_3765 = ((BgL_objectz00_bglt) BgL_bz00_3072);
							BgL_auxz00_3764 = BGL_OBJECT_WIDENING(BgL_tmpz00_3765);
						}
						BgL_auxz00_3763 = ((BgL_blocksz00_bglt) BgL_auxz00_3764);
					}
					return
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_3763))->BgL_gccntz00) =
						((long) BgL_auxz00_3769), BUNSPEC);
		}}}

	}



/* &<@anonymous:1617> */
	bool_t BGl_z62zc3z04anonymousza31617ze3ze5zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3073, BgL_blockz00_bglt BgL_bz00_3074)
	{
		{	/* SawBbv/bbv-gc.scm 79 */
			{	/* SawBbv/bbv-gc.scm 81 */
				obj_t BgL_g1424z00_3187;

				{
					BgL_blockvz00_bglt BgL_auxz00_3779;

					{
						obj_t BgL_auxz00_3780;

						{	/* SawBbv/bbv-gc.scm 81 */
							BgL_objectz00_bglt BgL_tmpz00_3781;

							BgL_tmpz00_3781 = ((BgL_objectz00_bglt) BgL_bz00_3074);
							BgL_auxz00_3780 = BGL_OBJECT_WIDENING(BgL_tmpz00_3781);
						}
						BgL_auxz00_3779 = ((BgL_blockvz00_bglt) BgL_auxz00_3780);
					}
					BgL_g1424z00_3187 =
						(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_3779))->BgL_versionsz00);
				}
				{
					obj_t BgL_l1422z00_3189;

					BgL_l1422z00_3189 = BgL_g1424z00_3187;
				BgL_zc3z04anonymousza31618ze3z87_3188:
					if (PAIRP(BgL_l1422z00_3189))
						{	/* SawBbv/bbv-gc.scm 81 */
							{	/* SawBbv/bbv-gc.scm 82 */
								obj_t BgL_bz00_3190;

								BgL_bz00_3190 = CAR(BgL_l1422z00_3189);
								{
									BgL_blocksz00_bglt BgL_auxz00_3789;

									{
										obj_t BgL_auxz00_3790;

										{	/* SawBbv/bbv-gc.scm 83 */
											BgL_objectz00_bglt BgL_tmpz00_3791;

											BgL_tmpz00_3791 =
												((BgL_objectz00_bglt)
												((BgL_blockz00_bglt) BgL_bz00_3190));
											BgL_auxz00_3790 = BGL_OBJECT_WIDENING(BgL_tmpz00_3791);
										}
										BgL_auxz00_3789 = ((BgL_blocksz00_bglt) BgL_auxz00_3790);
									}
									((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_3789))->
											BgL_gccntz00) = ((long) 0L), BUNSPEC);
							}}
							{
								obj_t BgL_l1422z00_3797;

								BgL_l1422z00_3797 = CDR(BgL_l1422z00_3189);
								BgL_l1422z00_3189 = BgL_l1422z00_3797;
								goto BgL_zc3z04anonymousza31618ze3z87_3188;
							}
						}
					else
						{	/* SawBbv/bbv-gc.scm 81 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* &<@anonymous:1603> */
	obj_t BGl_z62zc3z04anonymousza31603ze3ze5zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3075, BgL_blockz00_bglt BgL_bz00_3077)
	{
		{	/* SawBbv/bbv-gc.scm 66 */
			{	/* SawBbv/bbv-gc.scm 67 */
				obj_t BgL_dz00_3076;

				BgL_dz00_3076 = PROCEDURE_L_REF(BgL_envz00_3075, (int) (0L));
				{	/* SawBbv/bbv-gc.scm 67 */
					obj_t BgL_auxz00_3192;

					{	/* SawBbv/bbv-gc.scm 68 */
						obj_t BgL_arg1605z00_3193;

						{	/* SawBbv/bbv-gc.scm 68 */
							int BgL_arg1606z00_3194;
							long BgL_arg1609z00_3195;

							BgL_arg1606z00_3194 =
								(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_bz00_3077)))->BgL_labelz00);
							{
								BgL_blocksz00_bglt BgL_auxz00_3803;

								{
									obj_t BgL_auxz00_3804;

									{	/* SawBbv/bbv-gc.scm 69 */
										BgL_objectz00_bglt BgL_tmpz00_3805;

										BgL_tmpz00_3805 = ((BgL_objectz00_bglt) BgL_bz00_3077);
										BgL_auxz00_3804 = BGL_OBJECT_WIDENING(BgL_tmpz00_3805);
									}
									BgL_auxz00_3803 = ((BgL_blocksz00_bglt) BgL_auxz00_3804);
								}
								BgL_arg1609z00_3195 =
									(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_3803))->
									BgL_gccntz00);
							}
							{	/* SawBbv/bbv-gc.scm 67 */
								obj_t BgL_list1610z00_3196;

								{	/* SawBbv/bbv-gc.scm 67 */
									obj_t BgL_arg1611z00_3197;

									BgL_arg1611z00_3197 =
										MAKE_YOUNG_PAIR(BINT(BgL_arg1609z00_3195), BNIL);
									BgL_list1610z00_3196 =
										MAKE_YOUNG_PAIR(BINT(BgL_arg1606z00_3194),
										BgL_arg1611z00_3197);
								}
								BgL_arg1605z00_3193 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string1980z00zzsaw_bbvzd2gczd2, BgL_list1610z00_3196);
						}}
						BgL_auxz00_3192 =
							MAKE_YOUNG_PAIR(BgL_arg1605z00_3193, CELL_REF(BgL_dz00_3076));
					}
					CELL_SET(BgL_dz00_3076, BgL_auxz00_3192);
				}
				return
					BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(BgL_bz00_3077,
					BGl_string1982z00zzsaw_bbvzd2gczd2);
			}
		}

	}



/* get-gc-mark! */
	long BGl_getzd2gczd2markz12z12zzsaw_bbvzd2gczd2(void)
	{
		{	/* SawBbv/bbv-gc.scm 147 */
			BGl_za2gczd2markza2zd2zzsaw_bbvzd2gczd2 =
				(1L + BGl_za2gczd2markza2zd2zzsaw_bbvzd2gczd2);
			return BGl_za2gczd2markza2zd2zzsaw_bbvzd2gczd2;
		}

	}



/* walkbs! */
	bool_t BGl_walkbsz12z12zzsaw_bbvzd2gczd2(BgL_blockz00_bglt BgL_bz00_92,
		long BgL_mz00_93, obj_t BgL_procz00_94)
	{
		{	/* SawBbv/bbv-gc.scm 154 */
			((obj_t(*)(obj_t,
						BgL_blockz00_bglt))
				PROCEDURE_L_ENTRY(BgL_procz00_94)) (BgL_procz00_94, BgL_bz00_92);
			{	/* SawBbv/bbv-gc.scm 157 */
				bool_t BgL_test2061z00_3822;

				{	/* SawBbv/bbv-gc.scm 157 */
					long BgL_arg1738z00_2364;

					{
						BgL_blocksz00_bglt BgL_auxz00_3823;

						{
							obj_t BgL_auxz00_3824;

							{	/* SawBbv/bbv-gc.scm 157 */
								BgL_objectz00_bglt BgL_tmpz00_3825;

								BgL_tmpz00_3825 = ((BgL_objectz00_bglt) BgL_bz00_92);
								BgL_auxz00_3824 = BGL_OBJECT_WIDENING(BgL_tmpz00_3825);
							}
							BgL_auxz00_3823 = ((BgL_blocksz00_bglt) BgL_auxz00_3824);
						}
						BgL_arg1738z00_2364 =
							(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_3823))->BgL_z52markz52);
					}
					BgL_test2061z00_3822 = (BgL_arg1738z00_2364 == BgL_mz00_93);
				}
				if (BgL_test2061z00_3822)
					{	/* SawBbv/bbv-gc.scm 157 */
						return ((bool_t) 0);
					}
				else
					{	/* SawBbv/bbv-gc.scm 157 */
						{
							BgL_blocksz00_bglt BgL_auxz00_3831;

							{
								obj_t BgL_auxz00_3832;

								{	/* SawBbv/bbv-gc.scm 158 */
									BgL_objectz00_bglt BgL_tmpz00_3833;

									BgL_tmpz00_3833 = ((BgL_objectz00_bglt) BgL_bz00_92);
									BgL_auxz00_3832 = BGL_OBJECT_WIDENING(BgL_tmpz00_3833);
								}
								BgL_auxz00_3831 = ((BgL_blocksz00_bglt) BgL_auxz00_3832);
							}
							((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_3831))->
									BgL_z52markz52) = ((long) BgL_mz00_93), BUNSPEC);
						}
						{	/* SawBbv/bbv-gc.scm 159 */
							obj_t BgL_g1444z00_2356;

							BgL_g1444z00_2356 =
								(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_bz00_92)))->BgL_succsz00);
							{
								obj_t BgL_l1442z00_2358;

								BgL_l1442z00_2358 = BgL_g1444z00_2356;
							BgL_zc3z04anonymousza31735ze3z87_2359:
								if (PAIRP(BgL_l1442z00_2358))
									{	/* SawBbv/bbv-gc.scm 159 */
										{	/* SawBbv/bbv-gc.scm 159 */
											obj_t BgL_sz00_2361;

											BgL_sz00_2361 = CAR(BgL_l1442z00_2358);
											BGl_walkbsz12z12zzsaw_bbvzd2gczd2(
												((BgL_blockz00_bglt) BgL_sz00_2361), BgL_mz00_93,
												BgL_procz00_94);
										}
										{
											obj_t BgL_l1442z00_3845;

											BgL_l1442z00_3845 = CDR(BgL_l1442z00_2358);
											BgL_l1442z00_2358 = BgL_l1442z00_3845;
											goto BgL_zc3z04anonymousza31735ze3z87_2359;
										}
									}
								else
									{	/* SawBbv/bbv-gc.scm 159 */
										return ((bool_t) 1);
									}
							}
						}
					}
			}
		}

	}



/* walkbv! */
	bool_t BGl_walkbvz12z12zzsaw_bbvzd2gczd2(BgL_blockz00_bglt BgL_bz00_95,
		long BgL_mz00_96, obj_t BgL_procz00_97)
	{
		{	/* SawBbv/bbv-gc.scm 164 */
			{	/* SawBbv/bbv-gc.scm 166 */
				bool_t BgL_test2063z00_3847;

				{	/* SawBbv/bbv-gc.scm 166 */
					long BgL_arg1747z00_2376;

					{
						BgL_blockvz00_bglt BgL_auxz00_3848;

						{
							obj_t BgL_auxz00_3849;

							{	/* SawBbv/bbv-gc.scm 166 */
								BgL_objectz00_bglt BgL_tmpz00_3850;

								BgL_tmpz00_3850 = ((BgL_objectz00_bglt) BgL_bz00_95);
								BgL_auxz00_3849 = BGL_OBJECT_WIDENING(BgL_tmpz00_3850);
							}
							BgL_auxz00_3848 = ((BgL_blockvz00_bglt) BgL_auxz00_3849);
						}
						BgL_arg1747z00_2376 =
							(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_3848))->BgL_z52markz52);
					}
					BgL_test2063z00_3847 = (BgL_arg1747z00_2376 == BgL_mz00_96);
				}
				if (BgL_test2063z00_3847)
					{	/* SawBbv/bbv-gc.scm 166 */
						return ((bool_t) 0);
					}
				else
					{	/* SawBbv/bbv-gc.scm 166 */
						{
							BgL_blockvz00_bglt BgL_auxz00_3856;

							{
								obj_t BgL_auxz00_3857;

								{	/* SawBbv/bbv-gc.scm 167 */
									BgL_objectz00_bglt BgL_tmpz00_3858;

									BgL_tmpz00_3858 = ((BgL_objectz00_bglt) BgL_bz00_95);
									BgL_auxz00_3857 = BGL_OBJECT_WIDENING(BgL_tmpz00_3858);
								}
								BgL_auxz00_3856 = ((BgL_blockvz00_bglt) BgL_auxz00_3857);
							}
							((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_3856))->
									BgL_z52markz52) = ((long) BgL_mz00_96), BUNSPEC);
						}
						((bool_t(*)(obj_t,
									BgL_blockz00_bglt))
							PROCEDURE_L_ENTRY(BgL_procz00_97)) (BgL_procz00_97, BgL_bz00_95);
						{	/* SawBbv/bbv-gc.scm 169 */
							obj_t BgL_g1447z00_2368;

							BgL_g1447z00_2368 =
								(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_bz00_95)))->BgL_succsz00);
							{
								obj_t BgL_l1445z00_2370;

								BgL_l1445z00_2370 = BgL_g1447z00_2368;
							BgL_zc3z04anonymousza31742ze3z87_2371:
								if (PAIRP(BgL_l1445z00_2370))
									{	/* SawBbv/bbv-gc.scm 169 */
										{	/* SawBbv/bbv-gc.scm 169 */
											obj_t BgL_sz00_2373;

											BgL_sz00_2373 = CAR(BgL_l1445z00_2370);
											BGl_walkbvz12z12zzsaw_bbvzd2gczd2(
												((BgL_blockz00_bglt) BgL_sz00_2373), BgL_mz00_96,
												BgL_procz00_97);
										}
										{
											obj_t BgL_l1445z00_3874;

											BgL_l1445z00_3874 = CDR(BgL_l1445z00_2370);
											BgL_l1445z00_2370 = BgL_l1445z00_3874;
											goto BgL_zc3z04anonymousza31742ze3z87_2371;
										}
									}
								else
									{	/* SawBbv/bbv-gc.scm 169 */
										return ((bool_t) 1);
									}
							}
						}
					}
			}
		}

	}



/* bbv-gc-init! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2gczd2initz12z12zzsaw_bbvzd2gczd2(BgL_blockz00_bglt BgL_rootz00_98)
	{
		{	/* SawBbv/bbv-gc.scm 181 */
			{	/* SawBbv/bbv-gc.scm 182 */
				int BgL_arg1748z00_2377;

				BgL_arg1748z00_2377 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_rootz00_98)))->BgL_labelz00);
				BGl_za2gczd2graphza2zd2zzsaw_bbvzd2gczd2 =
					BGl_ssrzd2makezd2graphz00zz__ssrz00(BINT(BgL_arg1748z00_2377));
			}
			{	/* SawBbv/bbv-gc.scm 183 */
				obj_t BgL_vecz00_2379;

				BgL_vecz00_2379 = BGl_za2gczd2blocksza2zd2zzsaw_bbvzd2gczd2;
				{	/* SawBbv/bbv-gc.scm 183 */

					BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(BgL_vecz00_2379, BFALSE,
						0L, VECTOR_LENGTH(BgL_vecz00_2379));
			}}
			return (BGl_za2gczd2blockzd2lenza2z00zzsaw_bbvzd2gczd2 = 0L, BUNSPEC);
		}

	}



/* &bbv-gc-init! */
	obj_t BGl_z62bbvzd2gczd2initz12z70zzsaw_bbvzd2gczd2(obj_t BgL_envz00_3083,
		obj_t BgL_rootz00_3084)
	{
		{	/* SawBbv/bbv-gc.scm 181 */
			return
				BGl_bbvzd2gczd2initz12z12zzsaw_bbvzd2gczd2(
				((BgL_blockz00_bglt) BgL_rootz00_3084));
		}

	}



/* bbv-gc-add-block! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2gczd2addzd2blockz12zc0zzsaw_bbvzd2gczd2(BgL_blockz00_bglt
		BgL_bz00_99)
	{
		{	/* SawBbv/bbv-gc.scm 189 */
			{	/* SawBbv/bbv-gc.scm 191 */
				bool_t BgL_test2065z00_3884;

				{	/* SawBbv/bbv-gc.scm 191 */
					long BgL_arg1761z00_2397;

					BgL_arg1761z00_2397 =
						VECTOR_LENGTH(BGl_za2gczd2blocksza2zd2zzsaw_bbvzd2gczd2);
					BgL_test2065z00_3884 =
						(
						(long) (
							(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_bz00_99)))->BgL_labelz00)) >=
						BgL_arg1761z00_2397);
				}
				if (BgL_test2065z00_3884)
					{	/* SawBbv/bbv-gc.scm 192 */
						obj_t BgL_nvz00_2387;

						{	/* SawBbv/bbv-gc.scm 192 */
							long BgL_arg1752z00_2393;

							BgL_arg1752z00_2393 =
								(
								(long) (
									(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt) BgL_bz00_99)))->BgL_labelz00)) +
								1024L);
							BgL_nvz00_2387 = make_vector(BgL_arg1752z00_2393, BUNSPEC);
						}
						{	/* SawBbv/bbv-gc.scm 193 */
							obj_t BgL_sourcez00_2390;

							BgL_sourcez00_2390 = BGl_za2gczd2blocksza2zd2zzsaw_bbvzd2gczd2;
							{	/* SawBbv/bbv-gc.scm 193 */

								BGl_vectorzd2copyz12zc0zz__r4_vectors_6_8z00(BgL_nvz00_2387, 0L,
									BgL_sourcez00_2390, BINT(0L),
									BINT(VECTOR_LENGTH(BgL_sourcez00_2390)));
						}}
						BGl_za2gczd2blocksza2zd2zzsaw_bbvzd2gczd2 = BgL_nvz00_2387;
					}
				else
					{	/* SawBbv/bbv-gc.scm 191 */
						BFALSE;
					}
			}
			{	/* SawBbv/bbv-gc.scm 195 */
				int BgL_arg1762z00_2398;

				BgL_arg1762z00_2398 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_bz00_99)))->BgL_labelz00);
				{	/* SawBbv/bbv-gc.scm 195 */
					obj_t BgL_vectorz00_2942;
					long BgL_kz00_2943;

					BgL_vectorz00_2942 = BGl_za2gczd2blocksza2zd2zzsaw_bbvzd2gczd2;
					BgL_kz00_2943 = (long) (BgL_arg1762z00_2398);
					VECTOR_SET(BgL_vectorz00_2942, BgL_kz00_2943, ((obj_t) BgL_bz00_99));
			}}
			{	/* SawBbv/bbv-gc.scm 196 */
				bool_t BgL_test2066z00_3904;

				{	/* SawBbv/bbv-gc.scm 196 */
					long BgL_n2z00_2945;

					BgL_n2z00_2945 = BGl_za2gczd2blockzd2lenza2z00zzsaw_bbvzd2gczd2;
					BgL_test2066z00_3904 =
						(
						(long) (
							(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_bz00_99)))->BgL_labelz00)) >=
						BgL_n2z00_2945);
				}
				if (BgL_test2066z00_3904)
					{	/* SawBbv/bbv-gc.scm 196 */
						return (BGl_za2gczd2blockzd2lenza2z00zzsaw_bbvzd2gczd2 =
							((long) (
									(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt) BgL_bz00_99)))->BgL_labelz00)) +
								1L), BUNSPEC);
					}
				else
					{	/* SawBbv/bbv-gc.scm 196 */
						return BFALSE;
					}
			}
		}

	}



/* &bbv-gc-add-block! */
	obj_t BGl_z62bbvzd2gczd2addzd2blockz12za2zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3085, obj_t BgL_bz00_3086)
	{
		{	/* SawBbv/bbv-gc.scm 189 */
			return
				BGl_bbvzd2gczd2addzd2blockz12zc0zzsaw_bbvzd2gczd2(
				((BgL_blockz00_bglt) BgL_bz00_3086));
		}

	}



/* bbv-gc-block-reachable? */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2gczd2blockzd2reachablezf3z21zzsaw_bbvzd2gczd2(BgL_blockz00_bglt
		BgL_bz00_100)
	{
		{	/* SawBbv/bbv-gc.scm 202 */
			{	/* SawBbv/bbv-gc.scm 203 */
				int BgL_arg1771z00_2947;

				BgL_arg1771z00_2947 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_bz00_100)))->BgL_labelz00);
				return
					BGl_ssrzd2connectedzf3z21zz__ssrz00
					(BGl_za2gczd2graphza2zd2zzsaw_bbvzd2gczd2,
					(long) (BgL_arg1771z00_2947));
		}}

	}



/* &bbv-gc-block-reachable? */
	obj_t BGl_z62bbvzd2gczd2blockzd2reachablezf3z43zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3087, obj_t BgL_bz00_3088)
	{
		{	/* SawBbv/bbv-gc.scm 202 */
			return
				BGl_bbvzd2gczd2blockzd2reachablezf3z21zzsaw_bbvzd2gczd2(
				((BgL_blockz00_bglt) BgL_bz00_3088));
		}

	}



/* &bbv-gc-ondisconnect! */
	obj_t BGl_z62bbvzd2gczd2ondisconnectz12z70zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3089, obj_t BgL_nz00_3090)
	{
		{	/* SawBbv/bbv-gc.scm 208 */
			{	/* SawBbv/bbv-gc.scm 211 */
				long BgL_nz00_3202;

				BgL_nz00_3202 = (long) CINT(BgL_nz00_3090);
				{	/* SawBbv/bbv-gc.scm 211 */
					obj_t BgL_bz00_3203;

					BgL_bz00_3203 =
						VECTOR_REF(BGl_za2gczd2blocksza2zd2zzsaw_bbvzd2gczd2,
						BgL_nz00_3202);
					{	/* SawBbv/bbv-gc.scm 223 */
						obj_t BgL_g1450z00_3204;

						BgL_g1450z00_3204 =
							(((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt)
										((BgL_blockz00_bglt) BgL_bz00_3203))))->BgL_succsz00);
						{
							obj_t BgL_l1448z00_3206;

							BgL_l1448z00_3206 = BgL_g1450z00_3204;
						BgL_zc3z04anonymousza31772ze3z87_3205:
							if (PAIRP(BgL_l1448z00_3206))
								{	/* SawBbv/bbv-gc.scm 223 */
									{	/* SawBbv/bbv-gc.scm 224 */
										obj_t BgL_sz00_3207;

										BgL_sz00_3207 = CAR(BgL_l1448z00_3206);
										{	/* SawBbv/bbv-gc.scm 224 */
											int BgL_arg1775z00_3208;

											BgL_arg1775z00_3208 =
												(((BgL_blockz00_bglt) COBJECT(
														((BgL_blockz00_bglt) BgL_sz00_3207)))->
												BgL_labelz00);
											BGl_ssrzd2removezd2edgez12z12zz__ssrz00
												(BGl_za2gczd2graphza2zd2zzsaw_bbvzd2gczd2,
												BgL_nz00_3202, (long) (BgL_arg1775z00_3208), BFALSE);
										}
										{
											obj_t BgL_auxz00_3933;

											{	/* SawBbv/bbv-gc.scm 226 */
												obj_t BgL_arg1798z00_3209;

												BgL_arg1798z00_3209 =
													(((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt)
																((BgL_blockz00_bglt) BgL_sz00_3207))))->
													BgL_predsz00);
												BgL_auxz00_3933 =
													bgl_remq_bang(BgL_bz00_3203, BgL_arg1798z00_3209);
											}
											((((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt)
																((BgL_blockz00_bglt) BgL_sz00_3207))))->
													BgL_predsz00) = ((obj_t) BgL_auxz00_3933), BUNSPEC);
									}}
									{
										obj_t BgL_l1448z00_3941;

										BgL_l1448z00_3941 = CDR(BgL_l1448z00_3206);
										BgL_l1448z00_3206 = BgL_l1448z00_3941;
										goto BgL_zc3z04anonymousza31772ze3z87_3205;
									}
								}
							else
								{	/* SawBbv/bbv-gc.scm 223 */
									((bool_t) 1);
								}
						}
					}
					return
						((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt)
										((BgL_blockz00_bglt) BgL_bz00_3203))))->BgL_succsz00) =
						((obj_t) BNIL), BUNSPEC);
				}
			}
		}

	}



/* &bbv-gc-onconnect! */
	obj_t BGl_z62bbvzd2gczd2onconnectz12z70zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3091, obj_t BgL_nz00_3092)
	{
		{	/* SawBbv/bbv-gc.scm 233 */
			{	/* SawBbv/bbv-gc.scm 236 */
				long BgL_nz00_3210;

				BgL_nz00_3210 = (long) CINT(BgL_nz00_3092);
				{	/* SawBbv/bbv-gc.scm 236 */
					obj_t BgL_bz00_3211;

					BgL_bz00_3211 =
						VECTOR_REF(BGl_za2gczd2blocksza2zd2zzsaw_bbvzd2gczd2,
						BgL_nz00_3210);
					return BUNSPEC;
				}
			}
		}

	}



/* bbv-gc-connect! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2gczd2connectz12z12zzsaw_bbvzd2gczd2(BgL_blockz00_bglt
		BgL_fromz00_103, BgL_blockz00_bglt BgL_toz00_104)
	{
		{	/* SawBbv/bbv-gc.scm 251 */
			if (
				(BGl_za2bbvzd2blockszd2gcza2z00zzsaw_bbvzd2configzd2 ==
					CNST_TABLE_REF(2)))
				{	/* SawBbv/bbv-gc.scm 261 */
					int BgL_arg1805z00_2419;
					int BgL_arg1806z00_2420;

					BgL_arg1805z00_2419 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_fromz00_103)))->BgL_labelz00);
					BgL_arg1806z00_2420 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_toz00_104)))->BgL_labelz00);
					return
						BGl_ssrzd2addzd2edgez12z12zz__ssrz00
						(BGl_za2gczd2graphza2zd2zzsaw_bbvzd2gczd2,
						(long) (BgL_arg1805z00_2419), (long) (BgL_arg1806z00_2420),
						BGl_bbvzd2gczd2onconnectz12zd2envzc0zzsaw_bbvzd2gczd2);
				}
			else
				{	/* SawBbv/bbv-gc.scm 260 */
					return BFALSE;
				}
		}

	}



/* &bbv-gc-connect! */
	obj_t BGl_z62bbvzd2gczd2connectz12z70zzsaw_bbvzd2gczd2(obj_t BgL_envz00_3093,
		obj_t BgL_fromz00_3094, obj_t BgL_toz00_3095)
	{
		{	/* SawBbv/bbv-gc.scm 251 */
			return
				BGl_bbvzd2gczd2connectz12z12zzsaw_bbvzd2gczd2(
				((BgL_blockz00_bglt) BgL_fromz00_3094),
				((BgL_blockz00_bglt) BgL_toz00_3095));
		}

	}



/* bbv-gc-disconnect! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2gczd2disconnectz12z12zzsaw_bbvzd2gczd2(BgL_blockz00_bglt
		BgL_fromz00_105, BgL_blockz00_bglt BgL_toz00_106)
	{
		{	/* SawBbv/bbv-gc.scm 267 */
			if (
				(BGl_za2bbvzd2blockszd2gcza2z00zzsaw_bbvzd2configzd2 ==
					CNST_TABLE_REF(2)))
				{	/* SawBbv/bbv-gc.scm 277 */
					int BgL_arg1808z00_2422;
					int BgL_arg1812z00_2423;

					BgL_arg1808z00_2422 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_fromz00_105)))->BgL_labelz00);
					BgL_arg1812z00_2423 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_toz00_106)))->BgL_labelz00);
					return
						BGl_ssrzd2removezd2edgez12z12zz__ssrz00
						(BGl_za2gczd2graphza2zd2zzsaw_bbvzd2gczd2,
						(long) (BgL_arg1808z00_2422), (long) (BgL_arg1812z00_2423),
						BGl_bbvzd2gczd2ondisconnectz12zd2envzc0zzsaw_bbvzd2gczd2);
				}
			else
				{	/* SawBbv/bbv-gc.scm 276 */
					return BFALSE;
				}
		}

	}



/* &bbv-gc-disconnect! */
	obj_t BGl_z62bbvzd2gczd2disconnectz12z70zzsaw_bbvzd2gczd2(obj_t
		BgL_envz00_3096, obj_t BgL_fromz00_3097, obj_t BgL_toz00_3098)
	{
		{	/* SawBbv/bbv-gc.scm 267 */
			return
				BGl_bbvzd2gczd2disconnectz12z12zzsaw_bbvzd2gczd2(
				((BgL_blockz00_bglt) BgL_fromz00_3097),
				((BgL_blockz00_bglt) BgL_toz00_3098));
		}

	}



/* bbv-gc-redirect! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2gczd2redirectz12z12zzsaw_bbvzd2gczd2(BgL_blockz00_bglt
		BgL_oldz00_107, BgL_blockz00_bglt BgL_newz00_108)
	{
		{	/* SawBbv/bbv-gc.scm 283 */
			if (
				(BGl_za2bbvzd2blockszd2gcza2z00zzsaw_bbvzd2configzd2 ==
					CNST_TABLE_REF(2)))
				{	/* SawBbv/bbv-gc.scm 293 */
					int BgL_arg1820z00_2425;
					int BgL_arg1822z00_2426;

					BgL_arg1820z00_2425 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_oldz00_107)))->BgL_labelz00);
					BgL_arg1822z00_2426 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_newz00_108)))->BgL_labelz00);
					return
						BGl_ssrzd2redirectz12zc0zz__ssrz00
						(BGl_za2gczd2graphza2zd2zzsaw_bbvzd2gczd2,
						(long) (BgL_arg1820z00_2425), (long) (BgL_arg1822z00_2426),
						BGl_bbvzd2gczd2onconnectz12zd2envzc0zzsaw_bbvzd2gczd2,
						BGl_bbvzd2gczd2ondisconnectz12zd2envzc0zzsaw_bbvzd2gczd2);
				}
			else
				{	/* SawBbv/bbv-gc.scm 292 */
					return BFALSE;
				}
		}

	}



/* &bbv-gc-redirect! */
	obj_t BGl_z62bbvzd2gczd2redirectz12z70zzsaw_bbvzd2gczd2(obj_t BgL_envz00_3099,
		obj_t BgL_oldz00_3100, obj_t BgL_newz00_3101)
	{
		{	/* SawBbv/bbv-gc.scm 283 */
			return
				BGl_bbvzd2gczd2redirectz12z12zzsaw_bbvzd2gczd2(
				((BgL_blockz00_bglt) BgL_oldz00_3100),
				((BgL_blockz00_bglt) BgL_newz00_3101));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_bbvzd2gczd2(void)
	{
		{	/* SawBbv/bbv-gc.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_bbvzd2gczd2(void)
	{
		{	/* SawBbv/bbv-gc.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_bbvzd2gczd2(void)
	{
		{	/* SawBbv/bbv-gc.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2gczd2(void)
	{
		{	/* SawBbv/bbv-gc.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zzsaw_regsetz00(358079690L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(237915200L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(288263219L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(425659118L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2specializa7ez75(506937389L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(242097300L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(481635416L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
			return
				BGl_modulezd2initializa7ationz75zzsaw_bbvzd2debugzd2(120981929L,
				BSTRING_TO_STRING(BGl_string1983z00zzsaw_bbvzd2gczd2));
		}

	}

#ifdef __cplusplus
}
#endif
