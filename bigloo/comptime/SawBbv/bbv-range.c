/*===========================================================================*/
/*   (SawBbv/bbv-range.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawBbv/bbv-range.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_SAW_BBVzd2RANGEzd2_TYPE_DEFINITIONS
#define BGL_BgL_SAW_BBVzd2RANGEzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_loadiz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_atomz00_bgl *BgL_constantz00;
	}                   *BgL_rtl_loadiz00_bglt;

	typedef struct BgL_rtl_callz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                  *BgL_rtl_callz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_bbvzd2ctxzd2_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_idz00;
		obj_t BgL_entriesz00;
	}                   *BgL_bbvzd2ctxzd2_bglt;

	typedef struct BgL_bbvzd2ctxentryzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_rtl_regz00_bgl *BgL_regz00;
		obj_t BgL_typesz00;
		bool_t BgL_polarityz00;
		long BgL_countz00;
		obj_t BgL_valuez00;
		obj_t BgL_aliasesz00;
		obj_t BgL_initvalz00;
	}                        *BgL_bbvzd2ctxentryzd2_bglt;

	typedef struct BgL_bbvzd2rangezd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_loz00;
		obj_t BgL_upz00;
	}                     *BgL_bbvzd2rangezd2_bglt;

	typedef struct BgL_bbvzd2vlenzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_vecz00;
		int BgL_offsetz00;
	}                    *BgL_bbvzd2vlenzd2_bglt;


#endif													// BGL_BgL_SAW_BBVzd2RANGEzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62rtlzd2rangezb0zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	extern obj_t BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt,
		BgL_rtl_regz00_bglt);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_bbvzd2rangezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2 = BUNSPEC;
	static obj_t BGl_z62bbvzd2minzd2fixnumz62zzsaw_bbvzd2rangezd2(obj_t);
	static obj_t BGl_zd2zd2z00zzsaw_bbvzd2rangezd2(obj_t);
	static BgL_bbvzd2rangezd2_bglt
		BGl_z62bbvzd2rangezd2eqz62zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_typezd2errorzd2zz__errorz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2vlenzf3z43zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_za2bbvzd2optimzd2vlengthza2z00zzsaw_bbvzd2configzd2;
	BGL_EXPORTED_DEF obj_t BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2 = BUNSPEC;
	static BgL_bbvzd2rangezd2_bglt
		BGl_z62bbvzd2rangezd2gtz62zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	extern bool_t
		BGl_rtl_inszd2callzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2ltez00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt,
		BgL_bbvzd2rangezd2_bglt);
	static obj_t BGl_z62zd3rvzb1zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rtlzd2rangezd2zzsaw_bbvzd2rangezd2(obj_t,
		BgL_bbvzd2ctxzd2_bglt);
	static obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2rangezd2(void);
	static BgL_bbvzd2rangezd2_bglt
		BGl_vlenzd2ze3rangez31zzsaw_bbvzd2rangezd2(obj_t, BgL_bbvzd2ctxzd2_bglt);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2rangezd2_bglt
		BGl_vlenzd2rangezd2zzsaw_bbvzd2rangezd2(void);
	static obj_t BGl_za2rvza2zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	extern bool_t
		BGl_rtl_inszd2vlenzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_genericzd2initzd2zzsaw_bbvzd2rangezd2(void);
	static obj_t BGl_z62rangezd2typezf3z43zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzsaw_bbvzd2rangezd2(void);
	BGL_EXPORTED_DECL bool_t BGl_bbvzd2vlenzf3z21zzsaw_bbvzd2rangezd2(obj_t);
	BGL_IMPORT obj_t BGl_2za2za2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_2zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_bignum_mul(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2zc3zc3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2mulz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt,
		BgL_bbvzd2rangezd2_bglt);
	BGL_EXPORTED_DECL long BGl_bbvzd2minzd2fixnumz00zzsaw_bbvzd2rangezd2(void);
	BGL_IMPORT bool_t BGl_2zd3zd3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	static BgL_bbvzd2rangezd2_bglt
		BGl_z62bbvzd2rangezd2subz62zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_zb2rvzb2zzsaw_bbvzd2rangezd2(obj_t, obj_t, double);
	static BgL_bbvzd2rangezd2_bglt
		BGl_z62vlenzd2rangezb0zzsaw_bbvzd2rangezd2(obj_t);
	static obj_t BGl_z62bbvzd2maxzd2fixnumz62zzsaw_bbvzd2rangezd2(obj_t);
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	static obj_t
		BGl_z62objectzd2printzd2bbvzd2vle1536zb0zzsaw_bbvzd2rangezd2(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_bbvzd2rangezd2_bglt
		BGl_z62bbvzd2rangezd2ltz62zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2intersectionz00zzsaw_bbvzd2rangezd2
		(BgL_bbvzd2rangezd2_bglt, BgL_bbvzd2rangezd2_bglt);
	extern obj_t BGl_za2longzd2ze3bintza2z31zzsaw_bbvzd2cachezd2;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt);
	BGL_EXPORTED_DECL BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2unionz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt,
		BgL_bbvzd2rangezd2_bglt);
	static BgL_bbvzd2rangezd2_bglt
		BGl_z62bbvzd2rangezd2unionz62zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_bbvzd2rangezd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2vlenzd2vecz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2vlenzd2_bglt);
	BGL_EXPORTED_DECL obj_t BGl_maxrvz00zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	static obj_t BGl_zd2rvzd2zzsaw_bbvzd2rangezd2(obj_t, obj_t, double);
	BGL_EXPORTED_DECL obj_t BGl_zd3rvzd3zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	static BgL_bbvzd2rangezd2_bglt
		BGl_z62fixnumzd2rangezb0zzsaw_bbvzd2rangezd2(obj_t);
	extern bool_t
		BGl_rtl_inszd2strlenzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_ze3zd3rvz30zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	static BgL_bbvzd2rangezd2_bglt
		BGl_z62bbvzd2rangezd2neqz62zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_bignum_sub(obj_t, obj_t);
	extern bool_t
		BGl_rtl_inszd2loadizf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL long BGl_bbvzd2maxzd2fixnumz00zzsaw_bbvzd2rangezd2(void);
	static obj_t BGl_ze3rvze3zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	static BgL_bbvzd2rangezd2_bglt
		BGl_z62bbvzd2rangezd2gtez62zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	static obj_t BGl__maxrvz00zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_zd2zd2zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_bbvzd2vlenzd2offsetz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2vlenzd2_bglt);
	static obj_t BGl_z62bbvzd2vlenzd2offsetz62zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_bbvzd2rangezd2fixnumzf3zf3zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt);
	static BgL_bbvzd2rangezd2_bglt BGl_za2vlenzd2rangeza2zd2zzsaw_bbvzd2rangezd2;
	BGL_EXPORTED_DECL obj_t BGl_minrvz00zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	static obj_t BGl_zd2ze3rangez31zzsaw_bbvzd2rangezd2(obj_t);
	static obj_t BGl_z62bbvzd2singletonzf3z43zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	static BgL_bbvzd2rangezd2_bglt
		BGl_z62bbvzd2rangezd2addz62zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2intzd2ze3longza2z31zzsaw_bbvzd2cachezd2;
	static obj_t BGl__minrvz00zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	static obj_t BGl_maxrvzd2lozd2zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2debugzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long,
		char *);
	static obj_t BGl_z62shapezd2bbvzd2range1558z62zzsaw_bbvzd2rangezd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2subz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt,
		BgL_bbvzd2rangezd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2rangezc3zd3zc2zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt,
		BgL_bbvzd2rangezd2_bglt);
	static BgL_bbvzd2rangezd2_bglt
		BGl_z62fixnumzd2ze3rangez53zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2eqz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt,
		BgL_bbvzd2rangezd2_bglt);
	static BgL_bbvzd2rangezd2_bglt
		BGl_z62bbvzd2rangezd2intersectionz62zzsaw_bbvzd2rangezd2(obj_t, obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62lambda2303z62zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2304z62zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2308z62zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2309z62zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2rangeze3zd3ze2zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt,
		BgL_bbvzd2rangezd2_bglt);
	static obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2rangezd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2rangezd2(void);
	BGL_EXPORTED_DECL BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2gtz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt,
		BgL_bbvzd2rangezd2_bglt);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2rangezd2(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2rangezd2(void);
	BGL_EXPORTED_DECL bool_t BGl_bbvzd2singletonzf3z21zzsaw_bbvzd2rangezd2(obj_t);
	BGL_IMPORT obj_t BGl_objectz00zz__objectz00;
	extern obj_t BGl_za2bintzd2ze3longza2z31zzsaw_bbvzd2cachezd2;
	BGL_IMPORT obj_t BGl_bigloozd2configzd2zz__configurez00(obj_t);
	static BgL_bbvzd2rangezd2_bglt
		BGl_z62bbvzd2rangezd2ltez62zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62emptyzd2rangezf3z43zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32276ze3ze5zzsaw_bbvzd2rangezd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2neqz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt,
		BgL_bbvzd2rangezd2_bglt);
	static obj_t BGl_z62bbvzd2vlenzd2vecz62zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	static BgL_bbvzd2rangezd2_bglt BGl_z62lambda2272z62zzsaw_bbvzd2rangezd2(obj_t,
		obj_t, obj_t);
	static BgL_bbvzd2rangezd2_bglt
		BGl_z62lambda2274z62zzsaw_bbvzd2rangezd2(obj_t);
	static obj_t BGl_minrvzd2upzd2zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2gtez00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt,
		BgL_bbvzd2rangezd2_bglt);
	static obj_t BGl_z62lambda2281z62zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2282z62zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2287z62zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2288z62zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62bbvzd2rangezd2fixnumzf3z91zzsaw_bbvzd2rangezd2(obj_t,
		obj_t);
	extern obj_t BGl_za2bintza2z00zztype_cachez00;
	BGL_EXPORTED_DECL BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2ltz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt,
		BgL_bbvzd2rangezd2_bglt);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_strze70ze7zzsaw_bbvzd2rangezd2(obj_t);
	static obj_t BGl_z62bbvzd2rangezc3zd3za0zzsaw_bbvzd2rangezd2(obj_t, obj_t,
		obj_t);
	static BgL_bbvzd2rangezd2_bglt
		BGl_z62bbvzd2rangezd2mulz62zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	static BgL_bbvzd2vlenzd2_bglt BGl_z62lambda2294z62zzsaw_bbvzd2rangezd2(obj_t,
		obj_t, obj_t);
	static BgL_bbvzd2vlenzd2_bglt BGl_z62lambda2296z62zzsaw_bbvzd2rangezd2(obj_t);
	BGL_IMPORT bool_t BGl_2zc3zd3z10zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2rangezc3z11zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt,
		BgL_bbvzd2rangezd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2rangezd3z01zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt,
		BgL_bbvzd2rangezd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2rangeze3z31zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt,
		BgL_bbvzd2rangezd2_bglt);
	BGL_EXPORTED_DECL bool_t BGl_bbvzd2rangezf3z21zzsaw_bbvzd2rangezd2(obj_t);
	extern bool_t
		BGl_rtl_inszd2movzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2addz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt,
		BgL_bbvzd2rangezd2_bglt);
	static obj_t BGl_z62bbvzd2rangeze3zd3z80zzsaw_bbvzd2rangezd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rangezd2typezf3z21zzsaw_bbvzd2rangezd2(obj_t);
	static BgL_bbvzd2rangezd2_bglt
		BGl_za2fixnumzd2rangeza2zd2zzsaw_bbvzd2rangezd2;
	BGL_IMPORT bool_t BGl_2ze3zd3z30zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2rangezd2_bglt
		BGl_fixnumzd2rangezd2zzsaw_bbvzd2rangezd2(void);
	static obj_t BGl_z62bbvzd2rangezc3z73zzsaw_bbvzd2rangezd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2rangezd3z63zzsaw_bbvzd2rangezd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32298ze3ze5zzsaw_bbvzd2rangezd2(obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2rangeze3z53zzsaw_bbvzd2rangezd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2rangezf3z43zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2rangezd2_bglt
		BGl_fixnumzd2ze3rangez31zzsaw_bbvzd2rangezd2(long);
	static obj_t BGl_zb2zb2z00zzsaw_bbvzd2rangezd2(obj_t);
	static obj_t __cnst[11];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vlenzd2rangezd2envz00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762vlenza7d2rangeza72637za7,
		BGl_z62vlenzd2rangezb0zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_fixnumzd2rangezd2envz00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762fixnumza7d2rang2638z00,
		BGl_z62fixnumzd2rangezb0zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_maxrvzd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl__maxrvza700za7za7saw_b2639za7, opt_generic_entry,
		BGl__maxrvz00zzsaw_bbvzd2rangezd2, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2rangezd2mulzd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7d2640za7,
		BGl_z62bbvzd2rangezd2mulz62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2minzd2fixnumzd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2minza7d2f2641za7,
		BGl_z62bbvzd2minzd2fixnumz62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bbvzd2rangeze3zd2envze3zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7e2642za7,
		BGl_z62bbvzd2rangeze3z53zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2rangezd2addzd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7d2643za7,
		BGl_z62bbvzd2rangezd2addz62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2rangezd2gtzd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7d2644za7,
		BGl_z62bbvzd2rangezd2gtz62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2vlenzd2veczd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2vlenza7d22645za7,
		BGl_z62bbvzd2vlenzd2vecz62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2rangezc3zd3zd2envz10zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7c2646za7,
		BGl_z62bbvzd2rangezc3zd3za0zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
#define BGl_real2600z00zzsaw_bbvzd2rangezd2 bigloo_infinity
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bbvzd2rangezf3zd2envzf3zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7f2647za7,
		BGl_z62bbvzd2rangezf3z43zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2601z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2601za700za7za7s2648za7,
		"/tmp/bigloo/comptime/SawBbv/bbv-range.scm", 41);
	      DEFINE_STRING(BGl_string2602z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2602za700za7za7s2649za7, "*rv", 3);
	      DEFINE_STRING(BGl_string2603z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2603za700za7za7s2650za7, "bbv-vlen", 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2rangezd2intersectionzd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7d2651za7,
		BGl_z62bbvzd2rangezd2intersectionz62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rangezd2typezf3zd2envzf3zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762rangeza7d2typeza72652za7,
		BGl_z62rangezd2typezf3z43zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_emptyzd2rangezf3zd2envzf3zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762emptyza7d2range2653z00,
		BGl_z62emptyzd2rangezf3z43zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2604z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762lambda2282za7622654z00,
		BGl_z62lambda2282z62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2605z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762lambda2281za7622655z00,
		BGl_z62lambda2281z62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2606z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762lambda2288za7622656z00,
		BGl_z62lambda2288z62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2607z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762lambda2287za7622657z00,
		BGl_z62lambda2287z62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2608z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762za7c3za704anonymo2658za7,
		BGl_z62zc3z04anonymousza32276ze3ze5zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2609z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762lambda2274za7622659z00,
		BGl_z62lambda2274z62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2619z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2619za700za7za7s2660za7, "object-print", 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2rangezd2fixnumzf3zd2envz21zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7d2661za7,
		BGl_z62bbvzd2rangezd2fixnumzf3z91zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2610z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762lambda2272za7622662z00,
		BGl_z62lambda2272z62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2611z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762lambda2304za7622663z00,
		BGl_z62lambda2304z62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2612z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762lambda2303za7622664z00,
		BGl_z62lambda2303z62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2613z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762lambda2309za7622665z00,
		BGl_z62lambda2309z62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2614z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762lambda2308za7622666z00,
		BGl_z62lambda2308z62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2621z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2621za700za7za7s2667za7, "shape", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2615z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762za7c3za704anonymo2668za7,
		BGl_z62zc3z04anonymousza32298ze3ze5zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2622z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2622za700za7za7s2669za7, "[empty]", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2616z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762lambda2296za7622670z00,
		BGl_z62lambda2296z62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2623z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2623za700za7za7s2671za7, "[~a..~a]", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2617z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762lambda2294za7622672z00,
		BGl_z62lambda2294z62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2624z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2624za700za7za7s2673za7, "#(~a ~a)", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2618z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762objectza7d2prin2674z00,
		BGl_z62objectzd2printzd2bbvzd2vle1536zb0zzsaw_bbvzd2rangezd2, 0L, BUNSPEC,
		3);
	      DEFINE_STRING(BGl_string2625z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2625za700za7za7s2675za7, "upfx", 4);
	      DEFINE_STRING(BGl_string2626z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2626za700za7za7s2676za7, "upfx-1", 6);
	extern obj_t BGl_shapezd2envzd2zztools_shapez00;
	   
		 
		DEFINE_STRING(BGl_string2627z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2627za700za7za7s2677za7, "upfx-~a", 7);
	      DEFINE_STRING(BGl_string2628z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2628za700za7za7s2678za7, "lofx", 4);
	      DEFINE_STRING(BGl_string2629z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2629za700za7za7s2679za7, "lofx+1", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2620z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762shapeza7d2bbvza7d2680za7,
		BGl_z62shapezd2bbvzd2range1558z62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2630z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2630za700za7za7s2681za7, "lofx+2", 6);
	      DEFINE_STRING(BGl_string2631z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2631za700za7za7s2682za7, "lofx+~a", 7);
	      DEFINE_STRING(BGl_string2632z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2632za700za7za7s2683za7, "-inf", 4);
	      DEFINE_STRING(BGl_string2633z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2633za700za7za7s2684za7, "+inf", 4);
	      DEFINE_STRING(BGl_string2634z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2634za700za7za7s2685za7, "saw_bbv-range", 13);
	      DEFINE_STRING(BGl_string2635z00zzsaw_bbvzd2rangezd2,
		BgL_bgl_string2635za700za7za7s2686za7,
		"bbv-vlen int offset vec saw_bbv-range bbv-range up obj lo #z1 int-size ",
		71);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2rangezd2neqzd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7d2687za7,
		BGl_z62bbvzd2rangezd2neqz62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	BGL_IMPORT obj_t BGl_objectzd2printzd2envz00zz__objectz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2vlenzd2offsetzd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2vlenza7d22688za7,
		BGl_z62bbvzd2vlenzd2offsetz62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
#define BGl_real2599z00zzsaw_bbvzd2rangezd2 bigloo_minfinity
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2rangezd2eqzd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7d2689za7,
		BGl_z62bbvzd2rangezd2eqz62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2rangeze3zd3zd2envz30zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7e2690za7,
		BGl_z62bbvzd2rangeze3zd3z80zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2maxzd2fixnumzd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2maxza7d2f2691za7,
		BGl_z62bbvzd2maxzd2fixnumz62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2rangezd2unionzd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7d2692za7,
		BGl_z62bbvzd2rangezd2unionz62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bbvzd2vlenzf3zd2envzf3zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2vlenza7f32693za7,
		BGl_z62bbvzd2vlenzf3z43zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtlzd2rangezd2envz00zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762rtlza7d2rangeza7b2694za7,
		BGl_z62rtlzd2rangezb0zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2singletonzf3zd2envzf3zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2singlet2695z00,
		BGl_z62bbvzd2singletonzf3z43zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_fixnumzd2ze3rangezd2envze3zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762fixnumza7d2za7e3r2696za7,
		BGl_z62fixnumzd2ze3rangez53zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2rangezd2ltezd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7d2697za7,
		BGl_z62bbvzd2rangezd2ltez62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bbvzd2rangezc3zd2envzc3zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7c2698za7,
		BGl_z62bbvzd2rangezc3z73zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2rangezd2gtezd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7d2699za7,
		BGl_z62bbvzd2rangezd2gtez62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2rangezd2ltzd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7d2700za7,
		BGl_z62bbvzd2rangezd2ltz62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2rangezd2subzd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7d2701za7,
		BGl_z62bbvzd2rangezd2subz62zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_minrvzd2envzd2zzsaw_bbvzd2rangezd2,
		BgL_bgl__minrvza700za7za7saw_b2702za7, opt_generic_entry,
		BGl__minrvz00zzsaw_bbvzd2rangezd2, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_zd3rvzd2envz01zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762za7d3rvza7b1za7za7saw2703za7,
		BGl_z62zd3rvzb1zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bbvzd2rangezd3zd2envzd3zzsaw_bbvzd2rangezd2,
		BgL_bgl_za762bbvza7d2rangeza7d2704za7,
		BGl_z62bbvzd2rangezd3z63zzsaw_bbvzd2rangezd2, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_bbvzd2rangezd2));
		     ADD_ROOT((void *) (&BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2));
		     ADD_ROOT((void *) (&BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2));
		     ADD_ROOT((void *) (&BGl_za2vlenzd2rangeza2zd2zzsaw_bbvzd2rangezd2));
		     ADD_ROOT((void *) (&BGl_za2fixnumzd2rangeza2zd2zzsaw_bbvzd2rangezd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(long
		BgL_checksumz00_6455, char *BgL_fromz00_6456)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_bbvzd2rangezd2))
				{
					BGl_requirezd2initializa7ationz75zzsaw_bbvzd2rangezd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_bbvzd2rangezd2();
					BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2rangezd2();
					BGl_cnstzd2initzd2zzsaw_bbvzd2rangezd2();
					BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2rangezd2();
					BGl_objectzd2initzd2zzsaw_bbvzd2rangezd2();
					BGl_methodzd2initzd2zzsaw_bbvzd2rangezd2();
					return BGl_toplevelzd2initzd2zzsaw_bbvzd2rangezd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2rangezd2(void)
	{
		{	/* SawBbv/bbv-range.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"saw_bbv-range");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_bbv-range");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_bbv-range");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_bbv-range");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"saw_bbv-range");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_bbv-range");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_bbv-range");
			BGl_modulezd2initializa7ationz75zz__configurez00(0L, "saw_bbv-range");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_bbv-range");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"saw_bbv-range");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_bbv-range");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "saw_bbv-range");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_bbv-range");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_bbv-range");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_bbv-range");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2rangezd2(void)
	{
		{	/* SawBbv/bbv-range.scm 15 */
			{	/* SawBbv/bbv-range.scm 15 */
				obj_t BgL_cportz00_6369;

				{	/* SawBbv/bbv-range.scm 15 */
					obj_t BgL_stringz00_6376;

					BgL_stringz00_6376 = BGl_string2635z00zzsaw_bbvzd2rangezd2;
					{	/* SawBbv/bbv-range.scm 15 */
						obj_t BgL_startz00_6377;

						BgL_startz00_6377 = BINT(0L);
						{	/* SawBbv/bbv-range.scm 15 */
							obj_t BgL_endz00_6378;

							BgL_endz00_6378 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_6376)));
							{	/* SawBbv/bbv-range.scm 15 */

								BgL_cportz00_6369 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_6376, BgL_startz00_6377, BgL_endz00_6378);
				}}}}
				{
					long BgL_iz00_6370;

					BgL_iz00_6370 = 10L;
				BgL_loopz00_6371:
					if ((BgL_iz00_6370 == -1L))
						{	/* SawBbv/bbv-range.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* SawBbv/bbv-range.scm 15 */
							{	/* SawBbv/bbv-range.scm 15 */
								obj_t BgL_arg2636z00_6372;

								{	/* SawBbv/bbv-range.scm 15 */

									{	/* SawBbv/bbv-range.scm 15 */
										obj_t BgL_locationz00_6374;

										BgL_locationz00_6374 = BBOOL(((bool_t) 0));
										{	/* SawBbv/bbv-range.scm 15 */

											BgL_arg2636z00_6372 =
												BGl_readz00zz__readerz00(BgL_cportz00_6369,
												BgL_locationz00_6374);
										}
									}
								}
								{	/* SawBbv/bbv-range.scm 15 */
									int BgL_tmpz00_6491;

									BgL_tmpz00_6491 = (int) (BgL_iz00_6370);
									CNST_TABLE_SET(BgL_tmpz00_6491, BgL_arg2636z00_6372);
							}}
							{	/* SawBbv/bbv-range.scm 15 */
								int BgL_auxz00_6375;

								BgL_auxz00_6375 = (int) ((BgL_iz00_6370 - 1L));
								{
									long BgL_iz00_6496;

									BgL_iz00_6496 = (long) (BgL_auxz00_6375);
									BgL_iz00_6370 = BgL_iz00_6496;
									goto BgL_loopz00_6371;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2rangezd2(void)
	{
		{	/* SawBbv/bbv-range.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2rangezd2(void)
	{
		{	/* SawBbv/bbv-range.scm 15 */
			{	/* SawBbv/bbv-range.scm 358 */
				BgL_bbvzd2rangezd2_bglt BgL_new1209z00_1942;

				{	/* SawBbv/bbv-range.scm 359 */
					BgL_bbvzd2rangezd2_bglt BgL_new1208z00_1943;

					BgL_new1208z00_1943 =
						((BgL_bbvzd2rangezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2rangezd2_bgl))));
					{	/* SawBbv/bbv-range.scm 359 */
						long BgL_arg1559z00_1944;

						BgL_arg1559z00_1944 =
							BGL_CLASS_NUM(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1208z00_1943), BgL_arg1559z00_1944);
					}
					BgL_new1209z00_1942 = BgL_new1208z00_1943;
				}
				{
					obj_t BgL_auxz00_6503;

					{	/* SawBbv/bbv-range.scm 245 */
						long BgL_arg1820z00_3304;

						{	/* SawBbv/bbv-range.scm 245 */
							long BgL_arg1822z00_3305;

							{	/* SawBbv/bbv-range.scm 243 */
								long BgL_arg1808z00_3306;

								{	/* SawBbv/bbv-range.scm 243 */
									obj_t BgL_arg1812z00_3307;

									BgL_arg1812z00_3307 =
										BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0));
									BgL_arg1808z00_3306 = ((long) CINT(BgL_arg1812z00_3307) - 2L);
								}
								BgL_arg1822z00_3305 = (1L << (int) (BgL_arg1808z00_3306));
							}
							BgL_arg1820z00_3304 = NEG(BgL_arg1822z00_3305);
						}
						BgL_auxz00_6503 = BINT((BgL_arg1820z00_3304 - 1L));
					}
					((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1209z00_1942))->
							BgL_loz00) = ((obj_t) BgL_auxz00_6503), BUNSPEC);
				}
				{
					obj_t BgL_auxz00_6514;

					{	/* SawBbv/bbv-range.scm 243 */
						long BgL_arg1808z00_3312;

						{	/* SawBbv/bbv-range.scm 243 */
							obj_t BgL_arg1812z00_3313;

							BgL_arg1812z00_3313 =
								BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0));
							BgL_arg1808z00_3312 = ((long) CINT(BgL_arg1812z00_3313) - 2L);
						}
						BgL_auxz00_6514 = BINT((1L << (int) (BgL_arg1808z00_3312)));
					}
					((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1209z00_1942))->
							BgL_upz00) = ((obj_t) BgL_auxz00_6514), BUNSPEC);
				}
				BGl_za2fixnumzd2rangeza2zd2zzsaw_bbvzd2rangezd2 = BgL_new1209z00_1942;
			}
			{	/* SawBbv/bbv-range.scm 380 */
				BgL_bbvzd2rangezd2_bglt BgL_new1213z00_1945;

				{	/* SawBbv/bbv-range.scm 381 */
					BgL_bbvzd2rangezd2_bglt BgL_new1212z00_1946;

					BgL_new1212z00_1946 =
						((BgL_bbvzd2rangezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2rangezd2_bgl))));
					{	/* SawBbv/bbv-range.scm 381 */
						long BgL_arg1561z00_1947;

						BgL_arg1561z00_1947 =
							BGL_CLASS_NUM(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1212z00_1946), BgL_arg1561z00_1947);
					}
					BgL_new1213z00_1945 = BgL_new1212z00_1946;
				}
				((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1213z00_1945))->BgL_loz00) =
					((obj_t) BINT(0L)), BUNSPEC);
				{
					obj_t BgL_auxz00_6529;

					{	/* SawBbv/bbv-range.scm 243 */
						long BgL_arg1808z00_3319;

						{	/* SawBbv/bbv-range.scm 243 */
							obj_t BgL_arg1812z00_3320;

							BgL_arg1812z00_3320 =
								BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0));
							BgL_arg1808z00_3319 = ((long) CINT(BgL_arg1812z00_3320) - 2L);
						}
						BgL_auxz00_6529 = BINT((1L << (int) (BgL_arg1808z00_3319)));
					}
					((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1213z00_1945))->
							BgL_upz00) = ((obj_t) BgL_auxz00_6529), BUNSPEC);
				}
				return (BGl_za2vlenzd2rangeza2zd2zzsaw_bbvzd2rangezd2 =
					BgL_new1213z00_1945, BUNSPEC);
			}
		}

	}



/* bbv-vlen? */
	BGL_EXPORTED_DEF bool_t BGl_bbvzd2vlenzf3z21zzsaw_bbvzd2rangezd2(obj_t
		BgL_oz00_6)
	{
		{	/* SawBbv/bbv-range.scm 84 */
			{	/* SawBbv/bbv-range.scm 85 */
				obj_t BgL_classz00_6380;

				BgL_classz00_6380 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
				if (BGL_OBJECTP(BgL_oz00_6))
					{	/* SawBbv/bbv-range.scm 85 */
						BgL_objectz00_bglt BgL_arg1807z00_6381;

						BgL_arg1807z00_6381 = (BgL_objectz00_bglt) (BgL_oz00_6);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-range.scm 85 */
								long BgL_idxz00_6382;

								BgL_idxz00_6382 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6381);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6382 + 1L)) == BgL_classz00_6380);
							}
						else
							{	/* SawBbv/bbv-range.scm 85 */
								bool_t BgL_res2528z00_6385;

								{	/* SawBbv/bbv-range.scm 85 */
									obj_t BgL_oclassz00_6386;

									{	/* SawBbv/bbv-range.scm 85 */
										obj_t BgL_arg1815z00_6387;
										long BgL_arg1816z00_6388;

										BgL_arg1815z00_6387 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-range.scm 85 */
											long BgL_arg1817z00_6389;

											BgL_arg1817z00_6389 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6381);
											BgL_arg1816z00_6388 = (BgL_arg1817z00_6389 - OBJECT_TYPE);
										}
										BgL_oclassz00_6386 =
											VECTOR_REF(BgL_arg1815z00_6387, BgL_arg1816z00_6388);
									}
									{	/* SawBbv/bbv-range.scm 85 */
										bool_t BgL__ortest_1115z00_6390;

										BgL__ortest_1115z00_6390 =
											(BgL_classz00_6380 == BgL_oclassz00_6386);
										if (BgL__ortest_1115z00_6390)
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_res2528z00_6385 = BgL__ortest_1115z00_6390;
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												long BgL_odepthz00_6391;

												{	/* SawBbv/bbv-range.scm 85 */
													obj_t BgL_arg1804z00_6392;

													BgL_arg1804z00_6392 = (BgL_oclassz00_6386);
													BgL_odepthz00_6391 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6392);
												}
												if ((1L < BgL_odepthz00_6391))
													{	/* SawBbv/bbv-range.scm 85 */
														obj_t BgL_arg1802z00_6393;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_arg1803z00_6394;

															BgL_arg1803z00_6394 = (BgL_oclassz00_6386);
															BgL_arg1802z00_6393 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6394,
																1L);
														}
														BgL_res2528z00_6385 =
															(BgL_arg1802z00_6393 == BgL_classz00_6380);
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_res2528z00_6385 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2528z00_6385;
							}
					}
				else
					{	/* SawBbv/bbv-range.scm 85 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bbv-vlen? */
	obj_t BGl_z62bbvzd2vlenzf3z43zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6232,
		obj_t BgL_oz00_6233)
	{
		{	/* SawBbv/bbv-range.scm 84 */
			return BBOOL(BGl_bbvzd2vlenzf3z21zzsaw_bbvzd2rangezd2(BgL_oz00_6233));
		}

	}



/* bbv-vlen-vec */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2vlenzd2vecz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2vlenzd2_bglt
		BgL_oz00_7)
	{
		{	/* SawBbv/bbv-range.scm 87 */
			return (((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_oz00_7))->BgL_vecz00);
		}

	}



/* &bbv-vlen-vec */
	obj_t BGl_z62bbvzd2vlenzd2vecz62zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6234,
		obj_t BgL_oz00_6235)
	{
		{	/* SawBbv/bbv-range.scm 87 */
			return
				BGl_bbvzd2vlenzd2vecz00zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2vlenzd2_bglt) BgL_oz00_6235));
		}

	}



/* bbv-vlen-offset */
	BGL_EXPORTED_DEF int
		BGl_bbvzd2vlenzd2offsetz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2vlenzd2_bglt
		BgL_oz00_8)
	{
		{	/* SawBbv/bbv-range.scm 90 */
			return (((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_oz00_8))->BgL_offsetz00);
		}

	}



/* &bbv-vlen-offset */
	obj_t BGl_z62bbvzd2vlenzd2offsetz62zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6236,
		obj_t BgL_oz00_6237)
	{
		{	/* SawBbv/bbv-range.scm 90 */
			return
				BINT(BGl_bbvzd2vlenzd2offsetz00zzsaw_bbvzd2rangezd2(
					((BgL_bbvzd2vlenzd2_bglt) BgL_oz00_6237)));
		}

	}



/* =rv */
	BGL_EXPORTED_DEF obj_t BGl_zd3rvzd3zzsaw_bbvzd2rangezd2(obj_t BgL_xz00_11,
		obj_t BgL_yz00_12)
	{
		{	/* SawBbv/bbv-range.scm 99 */
			{	/* SawBbv/bbv-range.scm 101 */
				bool_t BgL_test2711z00_6569;

				if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_xz00_11))
					{	/* SawBbv/bbv-range.scm 101 */
						BgL_test2711z00_6569 =
							BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_yz00_12);
					}
				else
					{	/* SawBbv/bbv-range.scm 101 */
						BgL_test2711z00_6569 = ((bool_t) 0);
					}
				if (BgL_test2711z00_6569)
					{	/* SawBbv/bbv-range.scm 102 */
						bool_t BgL_test2713z00_6573;

						if (INTEGERP(BgL_xz00_11))
							{	/* SawBbv/bbv-range.scm 102 */
								BgL_test2713z00_6573 = INTEGERP(BgL_yz00_12);
							}
						else
							{	/* SawBbv/bbv-range.scm 102 */
								BgL_test2713z00_6573 = ((bool_t) 0);
							}
						if (BgL_test2713z00_6573)
							{	/* SawBbv/bbv-range.scm 102 */
								return
									BBOOL(((long) CINT(BgL_xz00_11) == (long) CINT(BgL_yz00_12)));
							}
						else
							{	/* SawBbv/bbv-range.scm 102 */
								return
									BBOOL(BGl_2zd3zd3zz__r4_numbers_6_5z00(BgL_xz00_11,
										BgL_yz00_12));
							}
					}
				else
					{	/* SawBbv/bbv-range.scm 103 */
						bool_t BgL_test2715z00_6583;

						{	/* SawBbv/bbv-range.scm 94 */
							bool_t BgL_test2716z00_6584;

							{	/* SawBbv/bbv-range.scm 85 */
								obj_t BgL_classz00_3441;

								BgL_classz00_3441 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
								if (BGL_OBJECTP(BgL_xz00_11))
									{	/* SawBbv/bbv-range.scm 85 */
										BgL_objectz00_bglt BgL_arg1807z00_3443;

										BgL_arg1807z00_3443 = (BgL_objectz00_bglt) (BgL_xz00_11);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawBbv/bbv-range.scm 85 */
												long BgL_idxz00_3449;

												BgL_idxz00_3449 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3443);
												BgL_test2716z00_6584 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3449 + 1L)) == BgL_classz00_3441);
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												bool_t BgL_res2531z00_3474;

												{	/* SawBbv/bbv-range.scm 85 */
													obj_t BgL_oclassz00_3457;

													{	/* SawBbv/bbv-range.scm 85 */
														obj_t BgL_arg1815z00_3465;
														long BgL_arg1816z00_3466;

														BgL_arg1815z00_3465 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawBbv/bbv-range.scm 85 */
															long BgL_arg1817z00_3467;

															BgL_arg1817z00_3467 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3443);
															BgL_arg1816z00_3466 =
																(BgL_arg1817z00_3467 - OBJECT_TYPE);
														}
														BgL_oclassz00_3457 =
															VECTOR_REF(BgL_arg1815z00_3465,
															BgL_arg1816z00_3466);
													}
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL__ortest_1115z00_3458;

														BgL__ortest_1115z00_3458 =
															(BgL_classz00_3441 == BgL_oclassz00_3457);
														if (BgL__ortest_1115z00_3458)
															{	/* SawBbv/bbv-range.scm 85 */
																BgL_res2531z00_3474 = BgL__ortest_1115z00_3458;
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_odepthz00_3459;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1804z00_3460;

																	BgL_arg1804z00_3460 = (BgL_oclassz00_3457);
																	BgL_odepthz00_3459 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3460);
																}
																if ((1L < BgL_odepthz00_3459))
																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1802z00_3462;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1803z00_3463;

																			BgL_arg1803z00_3463 =
																				(BgL_oclassz00_3457);
																			BgL_arg1802z00_3462 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3463, 1L);
																		}
																		BgL_res2531z00_3474 =
																			(BgL_arg1802z00_3462 ==
																			BgL_classz00_3441);
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2531z00_3474 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2716z00_6584 = BgL_res2531z00_3474;
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 85 */
										BgL_test2716z00_6584 = ((bool_t) 0);
									}
							}
							if (BgL_test2716z00_6584)
								{	/* SawBbv/bbv-range.scm 94 */
									bool_t BgL_test2721z00_6607;

									{	/* SawBbv/bbv-range.scm 85 */
										obj_t BgL_classz00_3475;

										BgL_classz00_3475 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
										if (BGL_OBJECTP(BgL_yz00_12))
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_objectz00_bglt BgL_arg1807z00_3477;

												BgL_arg1807z00_3477 =
													(BgL_objectz00_bglt) (BgL_yz00_12);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_idxz00_3483;

														BgL_idxz00_3483 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3477);
														BgL_test2721z00_6607 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3483 + 1L)) == BgL_classz00_3475);
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL_res2532z00_3508;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_oclassz00_3491;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1815z00_3499;
																long BgL_arg1816z00_3500;

																BgL_arg1815z00_3499 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_arg1817z00_3501;

																	BgL_arg1817z00_3501 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3477);
																	BgL_arg1816z00_3500 =
																		(BgL_arg1817z00_3501 - OBJECT_TYPE);
																}
																BgL_oclassz00_3491 =
																	VECTOR_REF(BgL_arg1815z00_3499,
																	BgL_arg1816z00_3500);
															}
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL__ortest_1115z00_3492;

																BgL__ortest_1115z00_3492 =
																	(BgL_classz00_3475 == BgL_oclassz00_3491);
																if (BgL__ortest_1115z00_3492)
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2532z00_3508 =
																			BgL__ortest_1115z00_3492;
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		long BgL_odepthz00_3493;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1804z00_3494;

																			BgL_arg1804z00_3494 =
																				(BgL_oclassz00_3491);
																			BgL_odepthz00_3493 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3494);
																		}
																		if ((1L < BgL_odepthz00_3493))
																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1802z00_3496;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1803z00_3497;

																					BgL_arg1803z00_3497 =
																						(BgL_oclassz00_3491);
																					BgL_arg1802z00_3496 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3497, 1L);
																				}
																				BgL_res2532z00_3508 =
																					(BgL_arg1802z00_3496 ==
																					BgL_classz00_3475);
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2532z00_3508 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2721z00_6607 = BgL_res2532z00_3508;
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_test2721z00_6607 = ((bool_t) 0);
											}
									}
									if (BgL_test2721z00_6607)
										{	/* SawBbv/bbv-range.scm 94 */
											BgL_test2715z00_6583 =
												(
												(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
															((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_11)))->
													BgL_vecz00) ==
												(((BgL_bbvzd2vlenzd2_bglt)
														COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_yz00_12)))->
													BgL_vecz00));
										}
									else
										{	/* SawBbv/bbv-range.scm 94 */
											BgL_test2715z00_6583 = ((bool_t) 0);
										}
								}
							else
								{	/* SawBbv/bbv-range.scm 94 */
									BgL_test2715z00_6583 = ((bool_t) 0);
								}
						}
						if (BgL_test2715z00_6583)
							{	/* SawBbv/bbv-range.scm 103 */
								return
									BBOOL(
									((long) (
											(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
														((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_11)))->
												BgL_offsetz00)) ==
										(long) ((((BgL_bbvzd2vlenzd2_bglt)
													COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_yz00_12)))->
												BgL_offsetz00))));
							}
						else
							{	/* SawBbv/bbv-range.scm 105 */
								bool_t BgL_test2726z00_6643;

								if (INTEGERP(BgL_xz00_11))
									{	/* SawBbv/bbv-range.scm 85 */
										obj_t BgL_classz00_3519;

										BgL_classz00_3519 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
										if (BGL_OBJECTP(BgL_yz00_12))
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_objectz00_bglt BgL_arg1807z00_3521;

												BgL_arg1807z00_3521 =
													(BgL_objectz00_bglt) (BgL_yz00_12);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_idxz00_3527;

														BgL_idxz00_3527 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3521);
														BgL_test2726z00_6643 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3527 + 1L)) == BgL_classz00_3519);
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL_res2533z00_3552;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_oclassz00_3535;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1815z00_3543;
																long BgL_arg1816z00_3544;

																BgL_arg1815z00_3543 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_arg1817z00_3545;

																	BgL_arg1817z00_3545 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3521);
																	BgL_arg1816z00_3544 =
																		(BgL_arg1817z00_3545 - OBJECT_TYPE);
																}
																BgL_oclassz00_3535 =
																	VECTOR_REF(BgL_arg1815z00_3543,
																	BgL_arg1816z00_3544);
															}
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL__ortest_1115z00_3536;

																BgL__ortest_1115z00_3536 =
																	(BgL_classz00_3519 == BgL_oclassz00_3535);
																if (BgL__ortest_1115z00_3536)
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2533z00_3552 =
																			BgL__ortest_1115z00_3536;
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		long BgL_odepthz00_3537;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1804z00_3538;

																			BgL_arg1804z00_3538 =
																				(BgL_oclassz00_3535);
																			BgL_odepthz00_3537 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3538);
																		}
																		if ((1L < BgL_odepthz00_3537))
																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1802z00_3540;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1803z00_3541;

																					BgL_arg1803z00_3541 =
																						(BgL_oclassz00_3535);
																					BgL_arg1802z00_3540 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3541, 1L);
																				}
																				BgL_res2533z00_3552 =
																					(BgL_arg1802z00_3540 ==
																					BgL_classz00_3519);
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2533z00_3552 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2726z00_6643 = BgL_res2533z00_3552;
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_test2726z00_6643 = ((bool_t) 0);
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 105 */
										BgL_test2726z00_6643 = ((bool_t) 0);
									}
								if (BgL_test2726z00_6643)
									{	/* SawBbv/bbv-range.scm 105 */
										if (
											((long) CINT(BgL_xz00_11) <
												(long) (
													(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																((BgL_bbvzd2vlenzd2_bglt) BgL_yz00_12)))->
														BgL_offsetz00))))
											{	/* SawBbv/bbv-range.scm 106 */
												return BFALSE;
											}
										else
											{	/* SawBbv/bbv-range.scm 106 */
												return BUNSPEC;
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 107 */
										bool_t BgL_test2733z00_6674;

										{	/* SawBbv/bbv-range.scm 107 */
											bool_t BgL_test2734z00_6675;

											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_classz00_3557;

												BgL_classz00_3557 =
													BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
												if (BGL_OBJECTP(BgL_xz00_11))
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_objectz00_bglt BgL_arg1807z00_3559;

														BgL_arg1807z00_3559 =
															(BgL_objectz00_bglt) (BgL_xz00_11);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_idxz00_3565;

																BgL_idxz00_3565 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3559);
																BgL_test2734z00_6675 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3565 + 1L)) ==
																	BgL_classz00_3557);
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL_res2534z00_3590;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_oclassz00_3573;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1815z00_3581;
																		long BgL_arg1816z00_3582;

																		BgL_arg1815z00_3581 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_arg1817z00_3583;

																			BgL_arg1817z00_3583 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3559);
																			BgL_arg1816z00_3582 =
																				(BgL_arg1817z00_3583 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3573 =
																			VECTOR_REF(BgL_arg1815z00_3581,
																			BgL_arg1816z00_3582);
																	}
																	{	/* SawBbv/bbv-range.scm 85 */
																		bool_t BgL__ortest_1115z00_3574;

																		BgL__ortest_1115z00_3574 =
																			(BgL_classz00_3557 == BgL_oclassz00_3573);
																		if (BgL__ortest_1115z00_3574)
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2534z00_3590 =
																					BgL__ortest_1115z00_3574;
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				long BgL_odepthz00_3575;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1804z00_3576;

																					BgL_arg1804z00_3576 =
																						(BgL_oclassz00_3573);
																					BgL_odepthz00_3575 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3576);
																				}
																				if ((1L < BgL_odepthz00_3575))
																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1802z00_3578;

																						{	/* SawBbv/bbv-range.scm 85 */
																							obj_t BgL_arg1803z00_3579;

																							BgL_arg1803z00_3579 =
																								(BgL_oclassz00_3573);
																							BgL_arg1802z00_3578 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3579, 1L);
																						}
																						BgL_res2534z00_3590 =
																							(BgL_arg1802z00_3578 ==
																							BgL_classz00_3557);
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 85 */
																						BgL_res2534z00_3590 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2734z00_6675 = BgL_res2534z00_3590;
															}
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_test2734z00_6675 = ((bool_t) 0);
													}
											}
											if (BgL_test2734z00_6675)
												{	/* SawBbv/bbv-range.scm 107 */
													BgL_test2733z00_6674 = INTEGERP(BgL_yz00_12);
												}
											else
												{	/* SawBbv/bbv-range.scm 107 */
													BgL_test2733z00_6674 = ((bool_t) 0);
												}
										}
										if (BgL_test2733z00_6674)
											{	/* SawBbv/bbv-range.scm 107 */
												if (
													((long) (
															(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																		((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_11)))->
																BgL_offsetz00)) > (long) CINT(BgL_yz00_12)))
													{	/* SawBbv/bbv-range.scm 108 */
														return BFALSE;
													}
												else
													{	/* SawBbv/bbv-range.scm 108 */
														return BUNSPEC;
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 107 */
												return BUNSPEC;
											}
									}
							}
					}
			}
		}

	}



/* &=rv */
	obj_t BGl_z62zd3rvzb1zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6238,
		obj_t BgL_xz00_6239, obj_t BgL_yz00_6240)
	{
		{	/* SawBbv/bbv-range.scm 99 */
			return BGl_zd3rvzd3zzsaw_bbvzd2rangezd2(BgL_xz00_6239, BgL_yz00_6240);
		}

	}



/* >rv */
	obj_t BGl_ze3rvze3zzsaw_bbvzd2rangezd2(obj_t BgL_xz00_13, obj_t BgL_yz00_14)
	{
		{	/* SawBbv/bbv-range.scm 115 */
			{	/* SawBbv/bbv-range.scm 117 */
				bool_t BgL_test2740z00_6706;

				if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_xz00_13))
					{	/* SawBbv/bbv-range.scm 117 */
						BgL_test2740z00_6706 =
							BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_yz00_14);
					}
				else
					{	/* SawBbv/bbv-range.scm 117 */
						BgL_test2740z00_6706 = ((bool_t) 0);
					}
				if (BgL_test2740z00_6706)
					{	/* SawBbv/bbv-range.scm 118 */
						bool_t BgL_test2742z00_6710;

						if (INTEGERP(BgL_xz00_13))
							{	/* SawBbv/bbv-range.scm 118 */
								BgL_test2742z00_6710 = INTEGERP(BgL_yz00_14);
							}
						else
							{	/* SawBbv/bbv-range.scm 118 */
								BgL_test2742z00_6710 = ((bool_t) 0);
							}
						if (BgL_test2742z00_6710)
							{	/* SawBbv/bbv-range.scm 118 */
								return
									BBOOL(((long) CINT(BgL_xz00_13) > (long) CINT(BgL_yz00_14)));
							}
						else
							{	/* SawBbv/bbv-range.scm 118 */
								return
									BBOOL(BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_xz00_13,
										BgL_yz00_14));
							}
					}
				else
					{	/* SawBbv/bbv-range.scm 119 */
						bool_t BgL_test2744z00_6720;

						{	/* SawBbv/bbv-range.scm 94 */
							bool_t BgL_test2745z00_6721;

							{	/* SawBbv/bbv-range.scm 85 */
								obj_t BgL_classz00_3601;

								BgL_classz00_3601 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
								if (BGL_OBJECTP(BgL_xz00_13))
									{	/* SawBbv/bbv-range.scm 85 */
										BgL_objectz00_bglt BgL_arg1807z00_3603;

										BgL_arg1807z00_3603 = (BgL_objectz00_bglt) (BgL_xz00_13);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawBbv/bbv-range.scm 85 */
												long BgL_idxz00_3609;

												BgL_idxz00_3609 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3603);
												BgL_test2745z00_6721 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3609 + 1L)) == BgL_classz00_3601);
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												bool_t BgL_res2535z00_3634;

												{	/* SawBbv/bbv-range.scm 85 */
													obj_t BgL_oclassz00_3617;

													{	/* SawBbv/bbv-range.scm 85 */
														obj_t BgL_arg1815z00_3625;
														long BgL_arg1816z00_3626;

														BgL_arg1815z00_3625 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawBbv/bbv-range.scm 85 */
															long BgL_arg1817z00_3627;

															BgL_arg1817z00_3627 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3603);
															BgL_arg1816z00_3626 =
																(BgL_arg1817z00_3627 - OBJECT_TYPE);
														}
														BgL_oclassz00_3617 =
															VECTOR_REF(BgL_arg1815z00_3625,
															BgL_arg1816z00_3626);
													}
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL__ortest_1115z00_3618;

														BgL__ortest_1115z00_3618 =
															(BgL_classz00_3601 == BgL_oclassz00_3617);
														if (BgL__ortest_1115z00_3618)
															{	/* SawBbv/bbv-range.scm 85 */
																BgL_res2535z00_3634 = BgL__ortest_1115z00_3618;
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_odepthz00_3619;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1804z00_3620;

																	BgL_arg1804z00_3620 = (BgL_oclassz00_3617);
																	BgL_odepthz00_3619 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3620);
																}
																if ((1L < BgL_odepthz00_3619))
																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1802z00_3622;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1803z00_3623;

																			BgL_arg1803z00_3623 =
																				(BgL_oclassz00_3617);
																			BgL_arg1802z00_3622 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3623, 1L);
																		}
																		BgL_res2535z00_3634 =
																			(BgL_arg1802z00_3622 ==
																			BgL_classz00_3601);
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2535z00_3634 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2745z00_6721 = BgL_res2535z00_3634;
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 85 */
										BgL_test2745z00_6721 = ((bool_t) 0);
									}
							}
							if (BgL_test2745z00_6721)
								{	/* SawBbv/bbv-range.scm 94 */
									bool_t BgL_test2750z00_6744;

									{	/* SawBbv/bbv-range.scm 85 */
										obj_t BgL_classz00_3635;

										BgL_classz00_3635 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
										if (BGL_OBJECTP(BgL_yz00_14))
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_objectz00_bglt BgL_arg1807z00_3637;

												BgL_arg1807z00_3637 =
													(BgL_objectz00_bglt) (BgL_yz00_14);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_idxz00_3643;

														BgL_idxz00_3643 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3637);
														BgL_test2750z00_6744 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3643 + 1L)) == BgL_classz00_3635);
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL_res2536z00_3668;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_oclassz00_3651;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1815z00_3659;
																long BgL_arg1816z00_3660;

																BgL_arg1815z00_3659 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_arg1817z00_3661;

																	BgL_arg1817z00_3661 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3637);
																	BgL_arg1816z00_3660 =
																		(BgL_arg1817z00_3661 - OBJECT_TYPE);
																}
																BgL_oclassz00_3651 =
																	VECTOR_REF(BgL_arg1815z00_3659,
																	BgL_arg1816z00_3660);
															}
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL__ortest_1115z00_3652;

																BgL__ortest_1115z00_3652 =
																	(BgL_classz00_3635 == BgL_oclassz00_3651);
																if (BgL__ortest_1115z00_3652)
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2536z00_3668 =
																			BgL__ortest_1115z00_3652;
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		long BgL_odepthz00_3653;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1804z00_3654;

																			BgL_arg1804z00_3654 =
																				(BgL_oclassz00_3651);
																			BgL_odepthz00_3653 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3654);
																		}
																		if ((1L < BgL_odepthz00_3653))
																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1802z00_3656;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1803z00_3657;

																					BgL_arg1803z00_3657 =
																						(BgL_oclassz00_3651);
																					BgL_arg1802z00_3656 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3657, 1L);
																				}
																				BgL_res2536z00_3668 =
																					(BgL_arg1802z00_3656 ==
																					BgL_classz00_3635);
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2536z00_3668 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2750z00_6744 = BgL_res2536z00_3668;
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_test2750z00_6744 = ((bool_t) 0);
											}
									}
									if (BgL_test2750z00_6744)
										{	/* SawBbv/bbv-range.scm 94 */
											BgL_test2744z00_6720 =
												(
												(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
															((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_13)))->
													BgL_vecz00) ==
												(((BgL_bbvzd2vlenzd2_bglt)
														COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_yz00_14)))->
													BgL_vecz00));
										}
									else
										{	/* SawBbv/bbv-range.scm 94 */
											BgL_test2744z00_6720 = ((bool_t) 0);
										}
								}
							else
								{	/* SawBbv/bbv-range.scm 94 */
									BgL_test2744z00_6720 = ((bool_t) 0);
								}
						}
						if (BgL_test2744z00_6720)
							{	/* SawBbv/bbv-range.scm 119 */
								return
									BBOOL(
									((long) (
											(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
														((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_13)))->
												BgL_offsetz00)) >
										(long) ((((BgL_bbvzd2vlenzd2_bglt)
													COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_yz00_14)))->
												BgL_offsetz00))));
							}
						else
							{	/* SawBbv/bbv-range.scm 121 */
								bool_t BgL_test2755z00_6780;

								{	/* SawBbv/bbv-range.scm 121 */
									bool_t BgL_test2756z00_6781;

									{	/* SawBbv/bbv-range.scm 85 */
										obj_t BgL_classz00_3679;

										BgL_classz00_3679 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
										if (BGL_OBJECTP(BgL_xz00_13))
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_objectz00_bglt BgL_arg1807z00_3681;

												BgL_arg1807z00_3681 =
													(BgL_objectz00_bglt) (BgL_xz00_13);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_idxz00_3687;

														BgL_idxz00_3687 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3681);
														BgL_test2756z00_6781 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3687 + 1L)) == BgL_classz00_3679);
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL_res2537z00_3712;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_oclassz00_3695;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1815z00_3703;
																long BgL_arg1816z00_3704;

																BgL_arg1815z00_3703 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_arg1817z00_3705;

																	BgL_arg1817z00_3705 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3681);
																	BgL_arg1816z00_3704 =
																		(BgL_arg1817z00_3705 - OBJECT_TYPE);
																}
																BgL_oclassz00_3695 =
																	VECTOR_REF(BgL_arg1815z00_3703,
																	BgL_arg1816z00_3704);
															}
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL__ortest_1115z00_3696;

																BgL__ortest_1115z00_3696 =
																	(BgL_classz00_3679 == BgL_oclassz00_3695);
																if (BgL__ortest_1115z00_3696)
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2537z00_3712 =
																			BgL__ortest_1115z00_3696;
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		long BgL_odepthz00_3697;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1804z00_3698;

																			BgL_arg1804z00_3698 =
																				(BgL_oclassz00_3695);
																			BgL_odepthz00_3697 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3698);
																		}
																		if ((1L < BgL_odepthz00_3697))
																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1802z00_3700;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1803z00_3701;

																					BgL_arg1803z00_3701 =
																						(BgL_oclassz00_3695);
																					BgL_arg1802z00_3700 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3701, 1L);
																				}
																				BgL_res2537z00_3712 =
																					(BgL_arg1802z00_3700 ==
																					BgL_classz00_3679);
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2537z00_3712 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2756z00_6781 = BgL_res2537z00_3712;
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_test2756z00_6781 = ((bool_t) 0);
											}
									}
									if (BgL_test2756z00_6781)
										{	/* SawBbv/bbv-range.scm 121 */
											BgL_test2755z00_6780 = INTEGERP(BgL_yz00_14);
										}
									else
										{	/* SawBbv/bbv-range.scm 121 */
											BgL_test2755z00_6780 = ((bool_t) 0);
										}
								}
								if (BgL_test2755z00_6780)
									{	/* SawBbv/bbv-range.scm 121 */
										if (
											((long) (
													(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_13)))->
														BgL_offsetz00)) > (long) CINT(BgL_yz00_14)))
											{	/* SawBbv/bbv-range.scm 122 */
												return BTRUE;
											}
										else
											{	/* SawBbv/bbv-range.scm 124 */
												bool_t BgL_test2762z00_6811;

												{	/* SawBbv/bbv-range.scm 124 */
													obj_t BgL_a1507z00_1997;

													{	/* SawBbv/bbv-range.scm 124 */
														obj_t BgL_arg1629z00_1999;

														{	/* SawBbv/bbv-range.scm 124 */
															long BgL_arg1630z00_2000;
															int BgL_arg1642z00_2001;

															{	/* SawBbv/bbv-range.scm 243 */
																long BgL_arg1808z00_3717;

																{	/* SawBbv/bbv-range.scm 243 */
																	obj_t BgL_arg1812z00_3718;

																	BgL_arg1812z00_3718 =
																		BGl_bigloozd2configzd2zz__configurez00
																		(CNST_TABLE_REF(0));
																	BgL_arg1808z00_3717 =
																		((long) CINT(BgL_arg1812z00_3718) - 2L);
																}
																BgL_arg1630z00_2000 =
																	(1L << (int) (BgL_arg1808z00_3717));
															}
															BgL_arg1642z00_2001 =
																(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																		((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_13)))->
																BgL_offsetz00);
															{	/* SawBbv/bbv-range.scm 124 */
																obj_t BgL_za71za7_3723;
																obj_t BgL_za72za7_3724;

																BgL_za71za7_3723 = BINT(BgL_arg1630z00_2000);
																BgL_za72za7_3724 = BINT(BgL_arg1642z00_2001);
																{	/* SawBbv/bbv-range.scm 124 */
																	obj_t BgL_tmpz00_3725;

																	BgL_tmpz00_3725 = BINT(0L);
																	if (BGL_ADDFX_OV(BgL_za71za7_3723,
																			BgL_za72za7_3724, BgL_tmpz00_3725))
																		{	/* SawBbv/bbv-range.scm 124 */
																			BgL_arg1629z00_1999 =
																				bgl_bignum_add(bgl_long_to_bignum(
																					(long) CINT(BgL_za71za7_3723)),
																				bgl_long_to_bignum(
																					(long) CINT(BgL_za72za7_3724)));
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 124 */
																			BgL_arg1629z00_1999 = BgL_tmpz00_3725;
																		}
																}
															}
														}
														BgL_a1507z00_1997 =
															BGl_zd2ze3rangez31zzsaw_bbvzd2rangezd2
															(BgL_arg1629z00_1999);
													}
													if (INTEGERP(BgL_a1507z00_1997))
														{	/* SawBbv/bbv-range.scm 124 */
															BgL_test2762z00_6811 =
																(
																(long) CINT(BgL_a1507z00_1997) <=
																(long) CINT(BgL_yz00_14));
														}
													else
														{	/* SawBbv/bbv-range.scm 124 */
															BgL_test2762z00_6811 =
																BGl_2zc3zd3z10zz__r4_numbers_6_5z00
																(BgL_a1507z00_1997, BgL_yz00_14);
														}
												}
												if (BgL_test2762z00_6811)
													{	/* SawBbv/bbv-range.scm 124 */
														return BFALSE;
													}
												else
													{	/* SawBbv/bbv-range.scm 124 */
														return BUNSPEC;
													}
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 127 */
										bool_t BgL_test2765z00_6837;

										if (INTEGERP(BgL_xz00_13))
											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_classz00_3735;

												BgL_classz00_3735 =
													BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
												if (BGL_OBJECTP(BgL_yz00_14))
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_objectz00_bglt BgL_arg1807z00_3737;

														BgL_arg1807z00_3737 =
															(BgL_objectz00_bglt) (BgL_yz00_14);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_idxz00_3743;

																BgL_idxz00_3743 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3737);
																BgL_test2765z00_6837 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3743 + 1L)) ==
																	BgL_classz00_3735);
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL_res2538z00_3768;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_oclassz00_3751;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1815z00_3759;
																		long BgL_arg1816z00_3760;

																		BgL_arg1815z00_3759 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_arg1817z00_3761;

																			BgL_arg1817z00_3761 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3737);
																			BgL_arg1816z00_3760 =
																				(BgL_arg1817z00_3761 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3751 =
																			VECTOR_REF(BgL_arg1815z00_3759,
																			BgL_arg1816z00_3760);
																	}
																	{	/* SawBbv/bbv-range.scm 85 */
																		bool_t BgL__ortest_1115z00_3752;

																		BgL__ortest_1115z00_3752 =
																			(BgL_classz00_3735 == BgL_oclassz00_3751);
																		if (BgL__ortest_1115z00_3752)
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2538z00_3768 =
																					BgL__ortest_1115z00_3752;
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				long BgL_odepthz00_3753;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1804z00_3754;

																					BgL_arg1804z00_3754 =
																						(BgL_oclassz00_3751);
																					BgL_odepthz00_3753 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3754);
																				}
																				if ((1L < BgL_odepthz00_3753))
																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1802z00_3756;

																						{	/* SawBbv/bbv-range.scm 85 */
																							obj_t BgL_arg1803z00_3757;

																							BgL_arg1803z00_3757 =
																								(BgL_oclassz00_3751);
																							BgL_arg1802z00_3756 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3757, 1L);
																						}
																						BgL_res2538z00_3768 =
																							(BgL_arg1802z00_3756 ==
																							BgL_classz00_3735);
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 85 */
																						BgL_res2538z00_3768 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2765z00_6837 = BgL_res2538z00_3768;
															}
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_test2765z00_6837 = ((bool_t) 0);
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 127 */
												BgL_test2765z00_6837 = ((bool_t) 0);
											}
										if (BgL_test2765z00_6837)
											{	/* SawBbv/bbv-range.scm 128 */
												bool_t BgL_test2771z00_6862;

												{	/* SawBbv/bbv-range.scm 128 */
													obj_t BgL_b1508z00_2014;

													{	/* SawBbv/bbv-range.scm 128 */
														long BgL_arg1678z00_2016;

														{	/* SawBbv/bbv-range.scm 128 */
															long BgL_arg1681z00_2017;
															int BgL_arg1688z00_2018;

															{	/* SawBbv/bbv-range.scm 243 */
																long BgL_arg1808z00_3769;

																{	/* SawBbv/bbv-range.scm 243 */
																	obj_t BgL_arg1812z00_3770;

																	BgL_arg1812z00_3770 =
																		BGl_bigloozd2configzd2zz__configurez00
																		(CNST_TABLE_REF(0));
																	BgL_arg1808z00_3769 =
																		((long) CINT(BgL_arg1812z00_3770) - 2L);
																}
																BgL_arg1681z00_2017 =
																	(1L << (int) (BgL_arg1808z00_3769));
															}
															BgL_arg1688z00_2018 =
																(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																		((BgL_bbvzd2vlenzd2_bglt) BgL_yz00_14)))->
																BgL_offsetz00);
															BgL_arg1678z00_2016 =
																(BgL_arg1681z00_2017 +
																(long) (BgL_arg1688z00_2018));
														}
														BgL_b1508z00_2014 =
															BGl_zd2ze3rangez31zzsaw_bbvzd2rangezd2(BINT
															(BgL_arg1678z00_2016));
													}
													if (INTEGERP(BgL_b1508z00_2014))
														{	/* SawBbv/bbv-range.scm 128 */
															BgL_test2771z00_6862 =
																(
																(long) CINT(BgL_xz00_13) >
																(long) CINT(BgL_b1508z00_2014));
														}
													else
														{	/* SawBbv/bbv-range.scm 128 */
															BgL_test2771z00_6862 =
																BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_xz00_13,
																BgL_b1508z00_2014);
														}
												}
												if (BgL_test2771z00_6862)
													{	/* SawBbv/bbv-range.scm 128 */
														return BTRUE;
													}
												else
													{	/* SawBbv/bbv-range.scm 128 */
														if (
															((long) CINT(BgL_xz00_13) <=
																(long) (
																	(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																				((BgL_bbvzd2vlenzd2_bglt)
																					BgL_yz00_14)))->BgL_offsetz00))))
															{	/* SawBbv/bbv-range.scm 130 */
																return BFALSE;
															}
														else
															{	/* SawBbv/bbv-range.scm 130 */
																return BUNSPEC;
															}
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 127 */
												return BUNSPEC;
											}
									}
							}
					}
			}
		}

	}



/* >=rv */
	obj_t BGl_ze3zd3rvz30zzsaw_bbvzd2rangezd2(obj_t BgL_xz00_15,
		obj_t BgL_yz00_16)
	{
		{	/* SawBbv/bbv-range.scm 139 */
			{	/* SawBbv/bbv-range.scm 141 */
				bool_t BgL_test2774z00_6887;

				if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_xz00_15))
					{	/* SawBbv/bbv-range.scm 141 */
						BgL_test2774z00_6887 =
							BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_yz00_16);
					}
				else
					{	/* SawBbv/bbv-range.scm 141 */
						BgL_test2774z00_6887 = ((bool_t) 0);
					}
				if (BgL_test2774z00_6887)
					{	/* SawBbv/bbv-range.scm 142 */
						bool_t BgL_test2776z00_6891;

						if (INTEGERP(BgL_xz00_15))
							{	/* SawBbv/bbv-range.scm 142 */
								BgL_test2776z00_6891 = INTEGERP(BgL_yz00_16);
							}
						else
							{	/* SawBbv/bbv-range.scm 142 */
								BgL_test2776z00_6891 = ((bool_t) 0);
							}
						if (BgL_test2776z00_6891)
							{	/* SawBbv/bbv-range.scm 142 */
								return
									BBOOL(((long) CINT(BgL_xz00_15) >= (long) CINT(BgL_yz00_16)));
							}
						else
							{	/* SawBbv/bbv-range.scm 142 */
								return
									BBOOL(BGl_2ze3zd3z30zz__r4_numbers_6_5z00(BgL_xz00_15,
										BgL_yz00_16));
							}
					}
				else
					{	/* SawBbv/bbv-range.scm 143 */
						bool_t BgL_test2778z00_6901;

						{	/* SawBbv/bbv-range.scm 94 */
							bool_t BgL_test2779z00_6902;

							{	/* SawBbv/bbv-range.scm 85 */
								obj_t BgL_classz00_3789;

								BgL_classz00_3789 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
								if (BGL_OBJECTP(BgL_xz00_15))
									{	/* SawBbv/bbv-range.scm 85 */
										BgL_objectz00_bglt BgL_arg1807z00_3791;

										BgL_arg1807z00_3791 = (BgL_objectz00_bglt) (BgL_xz00_15);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawBbv/bbv-range.scm 85 */
												long BgL_idxz00_3797;

												BgL_idxz00_3797 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3791);
												BgL_test2779z00_6902 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3797 + 1L)) == BgL_classz00_3789);
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												bool_t BgL_res2539z00_3822;

												{	/* SawBbv/bbv-range.scm 85 */
													obj_t BgL_oclassz00_3805;

													{	/* SawBbv/bbv-range.scm 85 */
														obj_t BgL_arg1815z00_3813;
														long BgL_arg1816z00_3814;

														BgL_arg1815z00_3813 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawBbv/bbv-range.scm 85 */
															long BgL_arg1817z00_3815;

															BgL_arg1817z00_3815 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3791);
															BgL_arg1816z00_3814 =
																(BgL_arg1817z00_3815 - OBJECT_TYPE);
														}
														BgL_oclassz00_3805 =
															VECTOR_REF(BgL_arg1815z00_3813,
															BgL_arg1816z00_3814);
													}
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL__ortest_1115z00_3806;

														BgL__ortest_1115z00_3806 =
															(BgL_classz00_3789 == BgL_oclassz00_3805);
														if (BgL__ortest_1115z00_3806)
															{	/* SawBbv/bbv-range.scm 85 */
																BgL_res2539z00_3822 = BgL__ortest_1115z00_3806;
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_odepthz00_3807;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1804z00_3808;

																	BgL_arg1804z00_3808 = (BgL_oclassz00_3805);
																	BgL_odepthz00_3807 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3808);
																}
																if ((1L < BgL_odepthz00_3807))
																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1802z00_3810;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1803z00_3811;

																			BgL_arg1803z00_3811 =
																				(BgL_oclassz00_3805);
																			BgL_arg1802z00_3810 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3811, 1L);
																		}
																		BgL_res2539z00_3822 =
																			(BgL_arg1802z00_3810 ==
																			BgL_classz00_3789);
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2539z00_3822 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2779z00_6902 = BgL_res2539z00_3822;
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 85 */
										BgL_test2779z00_6902 = ((bool_t) 0);
									}
							}
							if (BgL_test2779z00_6902)
								{	/* SawBbv/bbv-range.scm 94 */
									bool_t BgL_test2784z00_6925;

									{	/* SawBbv/bbv-range.scm 85 */
										obj_t BgL_classz00_3823;

										BgL_classz00_3823 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
										if (BGL_OBJECTP(BgL_yz00_16))
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_objectz00_bglt BgL_arg1807z00_3825;

												BgL_arg1807z00_3825 =
													(BgL_objectz00_bglt) (BgL_yz00_16);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_idxz00_3831;

														BgL_idxz00_3831 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3825);
														BgL_test2784z00_6925 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3831 + 1L)) == BgL_classz00_3823);
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL_res2540z00_3856;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_oclassz00_3839;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1815z00_3847;
																long BgL_arg1816z00_3848;

																BgL_arg1815z00_3847 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_arg1817z00_3849;

																	BgL_arg1817z00_3849 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3825);
																	BgL_arg1816z00_3848 =
																		(BgL_arg1817z00_3849 - OBJECT_TYPE);
																}
																BgL_oclassz00_3839 =
																	VECTOR_REF(BgL_arg1815z00_3847,
																	BgL_arg1816z00_3848);
															}
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL__ortest_1115z00_3840;

																BgL__ortest_1115z00_3840 =
																	(BgL_classz00_3823 == BgL_oclassz00_3839);
																if (BgL__ortest_1115z00_3840)
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2540z00_3856 =
																			BgL__ortest_1115z00_3840;
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		long BgL_odepthz00_3841;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1804z00_3842;

																			BgL_arg1804z00_3842 =
																				(BgL_oclassz00_3839);
																			BgL_odepthz00_3841 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3842);
																		}
																		if ((1L < BgL_odepthz00_3841))
																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1802z00_3844;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1803z00_3845;

																					BgL_arg1803z00_3845 =
																						(BgL_oclassz00_3839);
																					BgL_arg1802z00_3844 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3845, 1L);
																				}
																				BgL_res2540z00_3856 =
																					(BgL_arg1802z00_3844 ==
																					BgL_classz00_3823);
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2540z00_3856 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2784z00_6925 = BgL_res2540z00_3856;
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_test2784z00_6925 = ((bool_t) 0);
											}
									}
									if (BgL_test2784z00_6925)
										{	/* SawBbv/bbv-range.scm 94 */
											BgL_test2778z00_6901 =
												(
												(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
															((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_15)))->
													BgL_vecz00) ==
												(((BgL_bbvzd2vlenzd2_bglt)
														COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_yz00_16)))->
													BgL_vecz00));
										}
									else
										{	/* SawBbv/bbv-range.scm 94 */
											BgL_test2778z00_6901 = ((bool_t) 0);
										}
								}
							else
								{	/* SawBbv/bbv-range.scm 94 */
									BgL_test2778z00_6901 = ((bool_t) 0);
								}
						}
						if (BgL_test2778z00_6901)
							{	/* SawBbv/bbv-range.scm 143 */
								return
									BBOOL(
									((long) (
											(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
														((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_15)))->
												BgL_offsetz00)) >=
										(long) ((((BgL_bbvzd2vlenzd2_bglt)
													COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_yz00_16)))->
												BgL_offsetz00))));
							}
						else
							{	/* SawBbv/bbv-range.scm 145 */
								bool_t BgL_test2789z00_6961;

								{	/* SawBbv/bbv-range.scm 145 */
									bool_t BgL_test2790z00_6962;

									{	/* SawBbv/bbv-range.scm 85 */
										obj_t BgL_classz00_3867;

										BgL_classz00_3867 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
										if (BGL_OBJECTP(BgL_xz00_15))
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_objectz00_bglt BgL_arg1807z00_3869;

												BgL_arg1807z00_3869 =
													(BgL_objectz00_bglt) (BgL_xz00_15);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_idxz00_3875;

														BgL_idxz00_3875 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3869);
														BgL_test2790z00_6962 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3875 + 1L)) == BgL_classz00_3867);
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL_res2541z00_3900;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_oclassz00_3883;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1815z00_3891;
																long BgL_arg1816z00_3892;

																BgL_arg1815z00_3891 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_arg1817z00_3893;

																	BgL_arg1817z00_3893 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3869);
																	BgL_arg1816z00_3892 =
																		(BgL_arg1817z00_3893 - OBJECT_TYPE);
																}
																BgL_oclassz00_3883 =
																	VECTOR_REF(BgL_arg1815z00_3891,
																	BgL_arg1816z00_3892);
															}
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL__ortest_1115z00_3884;

																BgL__ortest_1115z00_3884 =
																	(BgL_classz00_3867 == BgL_oclassz00_3883);
																if (BgL__ortest_1115z00_3884)
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2541z00_3900 =
																			BgL__ortest_1115z00_3884;
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		long BgL_odepthz00_3885;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1804z00_3886;

																			BgL_arg1804z00_3886 =
																				(BgL_oclassz00_3883);
																			BgL_odepthz00_3885 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3886);
																		}
																		if ((1L < BgL_odepthz00_3885))
																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1802z00_3888;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1803z00_3889;

																					BgL_arg1803z00_3889 =
																						(BgL_oclassz00_3883);
																					BgL_arg1802z00_3888 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3889, 1L);
																				}
																				BgL_res2541z00_3900 =
																					(BgL_arg1802z00_3888 ==
																					BgL_classz00_3867);
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2541z00_3900 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2790z00_6962 = BgL_res2541z00_3900;
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_test2790z00_6962 = ((bool_t) 0);
											}
									}
									if (BgL_test2790z00_6962)
										{	/* SawBbv/bbv-range.scm 145 */
											BgL_test2789z00_6961 = INTEGERP(BgL_yz00_16);
										}
									else
										{	/* SawBbv/bbv-range.scm 145 */
											BgL_test2789z00_6961 = ((bool_t) 0);
										}
								}
								if (BgL_test2789z00_6961)
									{	/* SawBbv/bbv-range.scm 145 */
										if (
											((long) (
													(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_15)))->
														BgL_offsetz00)) >= (long) CINT(BgL_yz00_16)))
											{	/* SawBbv/bbv-range.scm 146 */
												return BTRUE;
											}
										else
											{	/* SawBbv/bbv-range.scm 148 */
												bool_t BgL_test2796z00_6992;

												{	/* SawBbv/bbv-range.scm 148 */
													obj_t BgL_a1509z00_2038;

													{	/* SawBbv/bbv-range.scm 148 */
														obj_t BgL_arg1714z00_2040;

														{	/* SawBbv/bbv-range.scm 148 */
															long BgL_arg1717z00_2041;
															int BgL_arg1718z00_2042;

															{	/* SawBbv/bbv-range.scm 243 */
																long BgL_arg1808z00_3905;

																{	/* SawBbv/bbv-range.scm 243 */
																	obj_t BgL_arg1812z00_3906;

																	BgL_arg1812z00_3906 =
																		BGl_bigloozd2configzd2zz__configurez00
																		(CNST_TABLE_REF(0));
																	BgL_arg1808z00_3905 =
																		((long) CINT(BgL_arg1812z00_3906) - 2L);
																}
																BgL_arg1717z00_2041 =
																	(1L << (int) (BgL_arg1808z00_3905));
															}
															BgL_arg1718z00_2042 =
																(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																		((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_15)))->
																BgL_offsetz00);
															{	/* SawBbv/bbv-range.scm 148 */
																obj_t BgL_za71za7_3911;
																obj_t BgL_za72za7_3912;

																BgL_za71za7_3911 = BINT(BgL_arg1717z00_2041);
																BgL_za72za7_3912 = BINT(BgL_arg1718z00_2042);
																{	/* SawBbv/bbv-range.scm 148 */
																	obj_t BgL_tmpz00_3913;

																	BgL_tmpz00_3913 = BINT(0L);
																	if (BGL_ADDFX_OV(BgL_za71za7_3911,
																			BgL_za72za7_3912, BgL_tmpz00_3913))
																		{	/* SawBbv/bbv-range.scm 148 */
																			BgL_arg1714z00_2040 =
																				bgl_bignum_add(bgl_long_to_bignum(
																					(long) CINT(BgL_za71za7_3911)),
																				bgl_long_to_bignum(
																					(long) CINT(BgL_za72za7_3912)));
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 148 */
																			BgL_arg1714z00_2040 = BgL_tmpz00_3913;
																		}
																}
															}
														}
														BgL_a1509z00_2038 =
															BGl_zd2ze3rangez31zzsaw_bbvzd2rangezd2
															(BgL_arg1714z00_2040);
													}
													if (INTEGERP(BgL_a1509z00_2038))
														{	/* SawBbv/bbv-range.scm 148 */
															BgL_test2796z00_6992 =
																(
																(long) CINT(BgL_a1509z00_2038) <
																(long) CINT(BgL_yz00_16));
														}
													else
														{	/* SawBbv/bbv-range.scm 148 */
															BgL_test2796z00_6992 =
																BGl_2zc3zc3zz__r4_numbers_6_5z00
																(BgL_a1509z00_2038, BgL_yz00_16);
														}
												}
												if (BgL_test2796z00_6992)
													{	/* SawBbv/bbv-range.scm 148 */
														return BFALSE;
													}
												else
													{	/* SawBbv/bbv-range.scm 148 */
														return BUNSPEC;
													}
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 151 */
										bool_t BgL_test2799z00_7018;

										if (INTEGERP(BgL_xz00_15))
											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_classz00_3923;

												BgL_classz00_3923 =
													BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
												if (BGL_OBJECTP(BgL_yz00_16))
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_objectz00_bglt BgL_arg1807z00_3925;

														BgL_arg1807z00_3925 =
															(BgL_objectz00_bglt) (BgL_yz00_16);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_idxz00_3931;

																BgL_idxz00_3931 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3925);
																BgL_test2799z00_7018 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3931 + 1L)) ==
																	BgL_classz00_3923);
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL_res2542z00_3956;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_oclassz00_3939;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1815z00_3947;
																		long BgL_arg1816z00_3948;

																		BgL_arg1815z00_3947 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_arg1817z00_3949;

																			BgL_arg1817z00_3949 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3925);
																			BgL_arg1816z00_3948 =
																				(BgL_arg1817z00_3949 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3939 =
																			VECTOR_REF(BgL_arg1815z00_3947,
																			BgL_arg1816z00_3948);
																	}
																	{	/* SawBbv/bbv-range.scm 85 */
																		bool_t BgL__ortest_1115z00_3940;

																		BgL__ortest_1115z00_3940 =
																			(BgL_classz00_3923 == BgL_oclassz00_3939);
																		if (BgL__ortest_1115z00_3940)
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2542z00_3956 =
																					BgL__ortest_1115z00_3940;
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				long BgL_odepthz00_3941;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1804z00_3942;

																					BgL_arg1804z00_3942 =
																						(BgL_oclassz00_3939);
																					BgL_odepthz00_3941 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3942);
																				}
																				if ((1L < BgL_odepthz00_3941))
																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1802z00_3944;

																						{	/* SawBbv/bbv-range.scm 85 */
																							obj_t BgL_arg1803z00_3945;

																							BgL_arg1803z00_3945 =
																								(BgL_oclassz00_3939);
																							BgL_arg1802z00_3944 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3945, 1L);
																						}
																						BgL_res2542z00_3956 =
																							(BgL_arg1802z00_3944 ==
																							BgL_classz00_3923);
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 85 */
																						BgL_res2542z00_3956 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2799z00_7018 = BgL_res2542z00_3956;
															}
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_test2799z00_7018 = ((bool_t) 0);
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 151 */
												BgL_test2799z00_7018 = ((bool_t) 0);
											}
										if (BgL_test2799z00_7018)
											{	/* SawBbv/bbv-range.scm 152 */
												bool_t BgL_test2805z00_7043;

												{	/* SawBbv/bbv-range.scm 152 */
													obj_t BgL_b1510z00_2055;

													{	/* SawBbv/bbv-range.scm 152 */
														long BgL_arg1740z00_2057;

														{	/* SawBbv/bbv-range.scm 152 */
															long BgL_arg1746z00_2058;
															int BgL_arg1747z00_2059;

															{	/* SawBbv/bbv-range.scm 243 */
																long BgL_arg1808z00_3957;

																{	/* SawBbv/bbv-range.scm 243 */
																	obj_t BgL_arg1812z00_3958;

																	BgL_arg1812z00_3958 =
																		BGl_bigloozd2configzd2zz__configurez00
																		(CNST_TABLE_REF(0));
																	BgL_arg1808z00_3957 =
																		((long) CINT(BgL_arg1812z00_3958) - 2L);
																}
																BgL_arg1746z00_2058 =
																	(1L << (int) (BgL_arg1808z00_3957));
															}
															BgL_arg1747z00_2059 =
																(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																		((BgL_bbvzd2vlenzd2_bglt) BgL_yz00_16)))->
																BgL_offsetz00);
															BgL_arg1740z00_2057 =
																(BgL_arg1746z00_2058 +
																(long) (BgL_arg1747z00_2059));
														}
														BgL_b1510z00_2055 =
															BGl_zd2ze3rangez31zzsaw_bbvzd2rangezd2(BINT
															(BgL_arg1740z00_2057));
													}
													if (INTEGERP(BgL_b1510z00_2055))
														{	/* SawBbv/bbv-range.scm 152 */
															BgL_test2805z00_7043 =
																(
																(long) CINT(BgL_xz00_15) >
																(long) CINT(BgL_b1510z00_2055));
														}
													else
														{	/* SawBbv/bbv-range.scm 152 */
															BgL_test2805z00_7043 =
																BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_xz00_15,
																BgL_b1510z00_2055);
														}
												}
												if (BgL_test2805z00_7043)
													{	/* SawBbv/bbv-range.scm 152 */
														return BTRUE;
													}
												else
													{	/* SawBbv/bbv-range.scm 152 */
														if (
															((long) CINT(BgL_xz00_15) <
																(long) (
																	(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																				((BgL_bbvzd2vlenzd2_bglt)
																					BgL_yz00_16)))->BgL_offsetz00))))
															{	/* SawBbv/bbv-range.scm 154 */
																return BFALSE;
															}
														else
															{	/* SawBbv/bbv-range.scm 154 */
																return BUNSPEC;
															}
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 151 */
												return BUNSPEC;
											}
									}
							}
					}
			}
		}

	}



/* _minrv */
	obj_t BGl__minrvz00zzsaw_bbvzd2rangezd2(obj_t BgL_env1538z00_25,
		obj_t BgL_opt1537z00_24)
	{
		{	/* SawBbv/bbv-range.scm 175 */
			{	/* SawBbv/bbv-range.scm 175 */
				obj_t BgL_g1539z00_2063;
				obj_t BgL_g1540z00_2064;

				BgL_g1539z00_2063 = VECTOR_REF(BgL_opt1537z00_24, 0L);
				BgL_g1540z00_2064 = VECTOR_REF(BgL_opt1537z00_24, 1L);
				switch (VECTOR_LENGTH(BgL_opt1537z00_24))
					{
					case 2L:

						{	/* SawBbv/bbv-range.scm 175 */

							return
								BGl_minrvz00zzsaw_bbvzd2rangezd2(BgL_g1539z00_2063,
								BgL_g1540z00_2064, BUNSPEC);
						}
						break;
					case 3L:

						{	/* SawBbv/bbv-range.scm 175 */
							obj_t BgL_unspecifiedz00_2068;

							BgL_unspecifiedz00_2068 = VECTOR_REF(BgL_opt1537z00_24, 2L);
							{	/* SawBbv/bbv-range.scm 175 */

								return
									BGl_minrvz00zzsaw_bbvzd2rangezd2(BgL_g1539z00_2063,
									BgL_g1540z00_2064, BgL_unspecifiedz00_2068);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* minrv */
	BGL_EXPORTED_DEF obj_t BGl_minrvz00zzsaw_bbvzd2rangezd2(obj_t BgL_xz00_21,
		obj_t BgL_yz00_22, obj_t BgL_unspecifiedz00_23)
	{
		{	/* SawBbv/bbv-range.scm 175 */
			{	/* SawBbv/bbv-range.scm 176 */
				long BgL_aux1173z00_2070;

				{	/* SawBbv/bbv-range.scm 176 */
					obj_t BgL_aux1171z00_2071;

					BgL_aux1171z00_2071 =
						BGl_ze3zd3rvz30zzsaw_bbvzd2rangezd2(BgL_yz00_22, BgL_xz00_21);
					if (CNSTP(BgL_aux1171z00_2071))
						{	/* SawBbv/bbv-range.scm 176 */
							BgL_aux1173z00_2070 = CCNST(BgL_aux1171z00_2071);
						}
					else
						{	/* SawBbv/bbv-range.scm 176 */
							BgL_aux1173z00_2070 = -1L;
						}
				}
				switch (BgL_aux1173z00_2070)
					{
					case 4L:

						return BgL_xz00_21;
						break;
					case 2L:

						return BgL_yz00_22;
						break;
					default:
						return BgL_unspecifiedz00_23;
					}
			}
		}

	}



/* minrv-up */
	obj_t BGl_minrvzd2upzd2zzsaw_bbvzd2rangezd2(obj_t BgL_xz00_26,
		obj_t BgL_yz00_27, obj_t BgL_unspecifiedz00_28)
	{
		{	/* SawBbv/bbv-range.scm 187 */
			{	/* SawBbv/bbv-range.scm 188 */
				obj_t BgL_mz00_2079;

				{	/* SawBbv/bbv-range.scm 45 */

					BgL_mz00_2079 =
						BGl_minrvz00zzsaw_bbvzd2rangezd2(BgL_xz00_26, BgL_yz00_27, BUNSPEC);
				}
				if ((BgL_mz00_2079 == BUNSPEC))
					{
						obj_t BgL_xz00_2081;
						obj_t BgL_yz00_2082;

						BgL_xz00_2081 = BgL_xz00_26;
						BgL_yz00_2082 = BgL_yz00_27;
					BgL_zc3z04anonymousza31749ze3z87_2083:
						{	/* SawBbv/bbv-range.scm 194 */
							bool_t BgL_test2810z00_7083;

							{	/* SawBbv/bbv-range.scm 194 */
								bool_t BgL_test2811z00_7084;

								{	/* SawBbv/bbv-range.scm 85 */
									obj_t BgL_classz00_3971;

									BgL_classz00_3971 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
									if (BGL_OBJECTP(BgL_xz00_2081))
										{	/* SawBbv/bbv-range.scm 85 */
											BgL_objectz00_bglt BgL_arg1807z00_3973;

											BgL_arg1807z00_3973 =
												(BgL_objectz00_bglt) (BgL_xz00_2081);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawBbv/bbv-range.scm 85 */
													long BgL_idxz00_3979;

													BgL_idxz00_3979 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3973);
													BgL_test2811z00_7084 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_3979 + 1L)) == BgL_classz00_3971);
												}
											else
												{	/* SawBbv/bbv-range.scm 85 */
													bool_t BgL_res2543z00_4004;

													{	/* SawBbv/bbv-range.scm 85 */
														obj_t BgL_oclassz00_3987;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_arg1815z00_3995;
															long BgL_arg1816z00_3996;

															BgL_arg1815z00_3995 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_arg1817z00_3997;

																BgL_arg1817z00_3997 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3973);
																BgL_arg1816z00_3996 =
																	(BgL_arg1817z00_3997 - OBJECT_TYPE);
															}
															BgL_oclassz00_3987 =
																VECTOR_REF(BgL_arg1815z00_3995,
																BgL_arg1816z00_3996);
														}
														{	/* SawBbv/bbv-range.scm 85 */
															bool_t BgL__ortest_1115z00_3988;

															BgL__ortest_1115z00_3988 =
																(BgL_classz00_3971 == BgL_oclassz00_3987);
															if (BgL__ortest_1115z00_3988)
																{	/* SawBbv/bbv-range.scm 85 */
																	BgL_res2543z00_4004 =
																		BgL__ortest_1115z00_3988;
																}
															else
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_odepthz00_3989;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1804z00_3990;

																		BgL_arg1804z00_3990 = (BgL_oclassz00_3987);
																		BgL_odepthz00_3989 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_3990);
																	}
																	if ((1L < BgL_odepthz00_3989))
																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1802z00_3992;

																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1803z00_3993;

																				BgL_arg1803z00_3993 =
																					(BgL_oclassz00_3987);
																				BgL_arg1802z00_3992 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_3993, 1L);
																			}
																			BgL_res2543z00_4004 =
																				(BgL_arg1802z00_3992 ==
																				BgL_classz00_3971);
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 85 */
																			BgL_res2543z00_4004 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2811z00_7084 = BgL_res2543z00_4004;
												}
										}
									else
										{	/* SawBbv/bbv-range.scm 85 */
											BgL_test2811z00_7084 = ((bool_t) 0);
										}
								}
								if (BgL_test2811z00_7084)
									{	/* SawBbv/bbv-range.scm 194 */
										BgL_test2810z00_7083 = INTEGERP(BgL_yz00_2082);
									}
								else
									{	/* SawBbv/bbv-range.scm 194 */
										BgL_test2810z00_7083 = ((bool_t) 0);
									}
							}
							if (BgL_test2810z00_7083)
								{	/* SawBbv/bbv-range.scm 195 */
									BgL_bbvzd2vlenzd2_bglt BgL_new1175z00_2087;

									{	/* SawBbv/bbv-range.scm 195 */
										BgL_bbvzd2vlenzd2_bglt BgL_new1178z00_2091;

										BgL_new1178z00_2091 =
											((BgL_bbvzd2vlenzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_bbvzd2vlenzd2_bgl))));
										{	/* SawBbv/bbv-range.scm 195 */
											long BgL_arg1753z00_2092;

											BgL_arg1753z00_2092 =
												BGL_CLASS_NUM(BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1178z00_2091),
												BgL_arg1753z00_2092);
										}
										BgL_new1175z00_2087 = BgL_new1178z00_2091;
									}
									((((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_new1175z00_2087))->
											BgL_vecz00) =
										((obj_t) (((BgL_bbvzd2vlenzd2_bglt)
													COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_2081)))->
												BgL_vecz00)), BUNSPEC);
									{
										int BgL_auxz00_7115;

										{	/* SawBbv/bbv-range.scm 196 */
											int BgL_az00_2088;
											obj_t BgL_bz00_2089;

											BgL_az00_2088 =
												(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
														((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_2081)))->
												BgL_offsetz00);
											BgL_bz00_2089 = BgL_yz00_2082;
											if (((long) (BgL_az00_2088) < (long) CINT(BgL_bz00_2089)))
												{	/* SawBbv/bbv-range.scm 196 */
													BgL_auxz00_7115 = BgL_az00_2088;
												}
											else
												{	/* SawBbv/bbv-range.scm 196 */
													BgL_auxz00_7115 = CINT(BgL_bz00_2089);
												}
										}
										((((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_new1175z00_2087))->
												BgL_offsetz00) = ((int) BgL_auxz00_7115), BUNSPEC);
									}
									return ((obj_t) BgL_new1175z00_2087);
								}
							else
								{	/* SawBbv/bbv-range.scm 197 */
									bool_t BgL_test2817z00_7125;

									{	/* SawBbv/bbv-range.scm 197 */
										bool_t BgL_test2818z00_7126;

										{	/* SawBbv/bbv-range.scm 85 */
											obj_t BgL_classz00_4012;

											BgL_classz00_4012 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
											if (BGL_OBJECTP(BgL_xz00_2081))
												{	/* SawBbv/bbv-range.scm 85 */
													BgL_objectz00_bglt BgL_arg1807z00_4014;

													BgL_arg1807z00_4014 =
														(BgL_objectz00_bglt) (BgL_xz00_2081);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* SawBbv/bbv-range.scm 85 */
															long BgL_idxz00_4020;

															BgL_idxz00_4020 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4014);
															BgL_test2818z00_7126 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_4020 + 1L)) == BgL_classz00_4012);
														}
													else
														{	/* SawBbv/bbv-range.scm 85 */
															bool_t BgL_res2544z00_4045;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_oclassz00_4028;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1815z00_4036;
																	long BgL_arg1816z00_4037;

																	BgL_arg1815z00_4036 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* SawBbv/bbv-range.scm 85 */
																		long BgL_arg1817z00_4038;

																		BgL_arg1817z00_4038 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4014);
																		BgL_arg1816z00_4037 =
																			(BgL_arg1817z00_4038 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_4028 =
																		VECTOR_REF(BgL_arg1815z00_4036,
																		BgL_arg1816z00_4037);
																}
																{	/* SawBbv/bbv-range.scm 85 */
																	bool_t BgL__ortest_1115z00_4029;

																	BgL__ortest_1115z00_4029 =
																		(BgL_classz00_4012 == BgL_oclassz00_4028);
																	if (BgL__ortest_1115z00_4029)
																		{	/* SawBbv/bbv-range.scm 85 */
																			BgL_res2544z00_4045 =
																				BgL__ortest_1115z00_4029;
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_odepthz00_4030;

																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1804z00_4031;

																				BgL_arg1804z00_4031 =
																					(BgL_oclassz00_4028);
																				BgL_odepthz00_4030 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_4031);
																			}
																			if ((1L < BgL_odepthz00_4030))
																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1802z00_4033;

																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1803z00_4034;

																						BgL_arg1803z00_4034 =
																							(BgL_oclassz00_4028);
																						BgL_arg1802z00_4033 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_4034, 1L);
																					}
																					BgL_res2544z00_4045 =
																						(BgL_arg1802z00_4033 ==
																						BgL_classz00_4012);
																				}
																			else
																				{	/* SawBbv/bbv-range.scm 85 */
																					BgL_res2544z00_4045 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2818z00_7126 = BgL_res2544z00_4045;
														}
												}
											else
												{	/* SawBbv/bbv-range.scm 85 */
													BgL_test2818z00_7126 = ((bool_t) 0);
												}
										}
										if (BgL_test2818z00_7126)
											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_classz00_4046;

												BgL_classz00_4046 =
													BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
												if (BGL_OBJECTP(BgL_yz00_2082))
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_objectz00_bglt BgL_arg1807z00_4048;

														BgL_arg1807z00_4048 =
															(BgL_objectz00_bglt) (BgL_yz00_2082);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_idxz00_4054;

																BgL_idxz00_4054 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_4048);
																BgL_test2817z00_7125 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_4054 + 1L)) ==
																	BgL_classz00_4046);
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL_res2545z00_4079;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_oclassz00_4062;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1815z00_4070;
																		long BgL_arg1816z00_4071;

																		BgL_arg1815z00_4070 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_arg1817z00_4072;

																			BgL_arg1817z00_4072 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_4048);
																			BgL_arg1816z00_4071 =
																				(BgL_arg1817z00_4072 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_4062 =
																			VECTOR_REF(BgL_arg1815z00_4070,
																			BgL_arg1816z00_4071);
																	}
																	{	/* SawBbv/bbv-range.scm 85 */
																		bool_t BgL__ortest_1115z00_4063;

																		BgL__ortest_1115z00_4063 =
																			(BgL_classz00_4046 == BgL_oclassz00_4062);
																		if (BgL__ortest_1115z00_4063)
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2545z00_4079 =
																					BgL__ortest_1115z00_4063;
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				long BgL_odepthz00_4064;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1804z00_4065;

																					BgL_arg1804z00_4065 =
																						(BgL_oclassz00_4062);
																					BgL_odepthz00_4064 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_4065);
																				}
																				if ((1L < BgL_odepthz00_4064))
																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1802z00_4067;

																						{	/* SawBbv/bbv-range.scm 85 */
																							obj_t BgL_arg1803z00_4068;

																							BgL_arg1803z00_4068 =
																								(BgL_oclassz00_4062);
																							BgL_arg1802z00_4067 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_4068, 1L);
																						}
																						BgL_res2545z00_4079 =
																							(BgL_arg1802z00_4067 ==
																							BgL_classz00_4046);
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 85 */
																						BgL_res2545z00_4079 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2817z00_7125 = BgL_res2545z00_4079;
															}
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_test2817z00_7125 = ((bool_t) 0);
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 197 */
												BgL_test2817z00_7125 = ((bool_t) 0);
											}
									}
									if (BgL_test2817z00_7125)
										{	/* SawBbv/bbv-range.scm 197 */
											if (
												((long) (
														(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																	((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_2081)))->
															BgL_offsetz00)) <
													(long) ((((BgL_bbvzd2vlenzd2_bglt)
																COBJECT(((BgL_bbvzd2vlenzd2_bglt)
																		BgL_yz00_2082)))->BgL_offsetz00))))
												{	/* SawBbv/bbv-range.scm 198 */
													return BgL_xz00_2081;
												}
											else
												{	/* SawBbv/bbv-range.scm 198 */
													return BgL_yz00_2082;
												}
										}
									else
										{	/* SawBbv/bbv-range.scm 200 */
											bool_t BgL_test2828z00_7179;

											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_classz00_4086;

												BgL_classz00_4086 =
													BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
												if (BGL_OBJECTP(BgL_yz00_2082))
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_objectz00_bglt BgL_arg1807z00_4088;

														BgL_arg1807z00_4088 =
															(BgL_objectz00_bglt) (BgL_yz00_2082);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_idxz00_4094;

																BgL_idxz00_4094 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_4088);
																BgL_test2828z00_7179 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_4094 + 1L)) ==
																	BgL_classz00_4086);
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL_res2546z00_4119;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_oclassz00_4102;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1815z00_4110;
																		long BgL_arg1816z00_4111;

																		BgL_arg1815z00_4110 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_arg1817z00_4112;

																			BgL_arg1817z00_4112 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_4088);
																			BgL_arg1816z00_4111 =
																				(BgL_arg1817z00_4112 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_4102 =
																			VECTOR_REF(BgL_arg1815z00_4110,
																			BgL_arg1816z00_4111);
																	}
																	{	/* SawBbv/bbv-range.scm 85 */
																		bool_t BgL__ortest_1115z00_4103;

																		BgL__ortest_1115z00_4103 =
																			(BgL_classz00_4086 == BgL_oclassz00_4102);
																		if (BgL__ortest_1115z00_4103)
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2546z00_4119 =
																					BgL__ortest_1115z00_4103;
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				long BgL_odepthz00_4104;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1804z00_4105;

																					BgL_arg1804z00_4105 =
																						(BgL_oclassz00_4102);
																					BgL_odepthz00_4104 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_4105);
																				}
																				if ((1L < BgL_odepthz00_4104))
																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1802z00_4107;

																						{	/* SawBbv/bbv-range.scm 85 */
																							obj_t BgL_arg1803z00_4108;

																							BgL_arg1803z00_4108 =
																								(BgL_oclassz00_4102);
																							BgL_arg1802z00_4107 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_4108, 1L);
																						}
																						BgL_res2546z00_4119 =
																							(BgL_arg1802z00_4107 ==
																							BgL_classz00_4086);
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 85 */
																						BgL_res2546z00_4119 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2828z00_7179 = BgL_res2546z00_4119;
															}
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_test2828z00_7179 = ((bool_t) 0);
													}
											}
											if (BgL_test2828z00_7179)
												{
													obj_t BgL_yz00_7203;
													obj_t BgL_xz00_7202;

													BgL_xz00_7202 = BgL_yz00_2082;
													BgL_yz00_7203 = BgL_xz00_2081;
													BgL_yz00_2082 = BgL_yz00_7203;
													BgL_xz00_2081 = BgL_xz00_7202;
													goto BgL_zc3z04anonymousza31749ze3z87_2083;
												}
											else
												{	/* SawBbv/bbv-range.scm 200 */
													return BgL_unspecifiedz00_28;
												}
										}
								}
						}
					}
				else
					{	/* SawBbv/bbv-range.scm 189 */
						return BgL_mz00_2079;
					}
			}
		}

	}



/* _maxrv */
	obj_t BGl__maxrvz00zzsaw_bbvzd2rangezd2(obj_t BgL_env1548z00_35,
		obj_t BgL_opt1547z00_34)
	{
		{	/* SawBbv/bbv-range.scm 208 */
			{	/* SawBbv/bbv-range.scm 208 */
				obj_t BgL_g1549z00_2107;
				obj_t BgL_g1550z00_2108;

				BgL_g1549z00_2107 = VECTOR_REF(BgL_opt1547z00_34, 0L);
				BgL_g1550z00_2108 = VECTOR_REF(BgL_opt1547z00_34, 1L);
				switch (VECTOR_LENGTH(BgL_opt1547z00_34))
					{
					case 2L:

						{	/* SawBbv/bbv-range.scm 208 */

							return
								BGl_maxrvz00zzsaw_bbvzd2rangezd2(BgL_g1549z00_2107,
								BgL_g1550z00_2108, BUNSPEC);
						}
						break;
					case 3L:

						{	/* SawBbv/bbv-range.scm 208 */
							obj_t BgL_unspecifiedz00_2112;

							BgL_unspecifiedz00_2112 = VECTOR_REF(BgL_opt1547z00_34, 2L);
							{	/* SawBbv/bbv-range.scm 208 */

								return
									BGl_maxrvz00zzsaw_bbvzd2rangezd2(BgL_g1549z00_2107,
									BgL_g1550z00_2108, BgL_unspecifiedz00_2112);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* maxrv */
	BGL_EXPORTED_DEF obj_t BGl_maxrvz00zzsaw_bbvzd2rangezd2(obj_t BgL_xz00_31,
		obj_t BgL_yz00_32, obj_t BgL_unspecifiedz00_33)
	{
		{	/* SawBbv/bbv-range.scm 208 */
			{	/* SawBbv/bbv-range.scm 209 */
				long BgL_aux1182z00_2114;

				{	/* SawBbv/bbv-range.scm 209 */
					obj_t BgL_aux1180z00_2115;

					BgL_aux1180z00_2115 =
						BGl_ze3zd3rvz30zzsaw_bbvzd2rangezd2(BgL_xz00_31, BgL_yz00_32);
					if (CNSTP(BgL_aux1180z00_2115))
						{	/* SawBbv/bbv-range.scm 209 */
							BgL_aux1182z00_2114 = CCNST(BgL_aux1180z00_2115);
						}
					else
						{	/* SawBbv/bbv-range.scm 209 */
							BgL_aux1182z00_2114 = -1L;
						}
				}
				switch (BgL_aux1182z00_2114)
					{
					case 4L:

						return BgL_xz00_31;
						break;
					case 2L:

						return BgL_yz00_32;
						break;
					default:
						return BgL_unspecifiedz00_33;
					}
			}
		}

	}



/* maxrv-lo */
	obj_t BGl_maxrvzd2lozd2zzsaw_bbvzd2rangezd2(obj_t BgL_xz00_36,
		obj_t BgL_yz00_37, obj_t BgL_unspecifiedz00_38)
	{
		{	/* SawBbv/bbv-range.scm 220 */
			{	/* SawBbv/bbv-range.scm 221 */
				obj_t BgL_mz00_2123;

				{	/* SawBbv/bbv-range.scm 46 */

					BgL_mz00_2123 =
						BGl_maxrvz00zzsaw_bbvzd2rangezd2(BgL_xz00_36, BgL_yz00_37, BUNSPEC);
				}
				if ((BgL_mz00_2123 == BUNSPEC))
					{
						obj_t BgL_xz00_2125;
						obj_t BgL_yz00_2126;

						BgL_xz00_2125 = BgL_xz00_36;
						BgL_yz00_2126 = BgL_yz00_37;
					BgL_zc3z04anonymousza31770ze3z87_2127:
						{	/* SawBbv/bbv-range.scm 227 */
							bool_t BgL_test2835z00_7219;

							{	/* SawBbv/bbv-range.scm 227 */
								bool_t BgL_test2836z00_7220;

								{	/* SawBbv/bbv-range.scm 85 */
									obj_t BgL_classz00_4120;

									BgL_classz00_4120 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
									if (BGL_OBJECTP(BgL_xz00_2125))
										{	/* SawBbv/bbv-range.scm 85 */
											BgL_objectz00_bglt BgL_arg1807z00_4122;

											BgL_arg1807z00_4122 =
												(BgL_objectz00_bglt) (BgL_xz00_2125);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawBbv/bbv-range.scm 85 */
													long BgL_idxz00_4128;

													BgL_idxz00_4128 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4122);
													BgL_test2836z00_7220 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_4128 + 1L)) == BgL_classz00_4120);
												}
											else
												{	/* SawBbv/bbv-range.scm 85 */
													bool_t BgL_res2547z00_4153;

													{	/* SawBbv/bbv-range.scm 85 */
														obj_t BgL_oclassz00_4136;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_arg1815z00_4144;
															long BgL_arg1816z00_4145;

															BgL_arg1815z00_4144 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_arg1817z00_4146;

																BgL_arg1817z00_4146 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4122);
																BgL_arg1816z00_4145 =
																	(BgL_arg1817z00_4146 - OBJECT_TYPE);
															}
															BgL_oclassz00_4136 =
																VECTOR_REF(BgL_arg1815z00_4144,
																BgL_arg1816z00_4145);
														}
														{	/* SawBbv/bbv-range.scm 85 */
															bool_t BgL__ortest_1115z00_4137;

															BgL__ortest_1115z00_4137 =
																(BgL_classz00_4120 == BgL_oclassz00_4136);
															if (BgL__ortest_1115z00_4137)
																{	/* SawBbv/bbv-range.scm 85 */
																	BgL_res2547z00_4153 =
																		BgL__ortest_1115z00_4137;
																}
															else
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_odepthz00_4138;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1804z00_4139;

																		BgL_arg1804z00_4139 = (BgL_oclassz00_4136);
																		BgL_odepthz00_4138 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_4139);
																	}
																	if ((1L < BgL_odepthz00_4138))
																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1802z00_4141;

																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1803z00_4142;

																				BgL_arg1803z00_4142 =
																					(BgL_oclassz00_4136);
																				BgL_arg1802z00_4141 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_4142, 1L);
																			}
																			BgL_res2547z00_4153 =
																				(BgL_arg1802z00_4141 ==
																				BgL_classz00_4120);
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 85 */
																			BgL_res2547z00_4153 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2836z00_7220 = BgL_res2547z00_4153;
												}
										}
									else
										{	/* SawBbv/bbv-range.scm 85 */
											BgL_test2836z00_7220 = ((bool_t) 0);
										}
								}
								if (BgL_test2836z00_7220)
									{	/* SawBbv/bbv-range.scm 227 */
										BgL_test2835z00_7219 = INTEGERP(BgL_yz00_2126);
									}
								else
									{	/* SawBbv/bbv-range.scm 227 */
										BgL_test2835z00_7219 = ((bool_t) 0);
									}
							}
							if (BgL_test2835z00_7219)
								{	/* SawBbv/bbv-range.scm 228 */
									BgL_bbvzd2vlenzd2_bglt BgL_new1184z00_2131;

									{	/* SawBbv/bbv-range.scm 228 */
										BgL_bbvzd2vlenzd2_bglt BgL_new1187z00_2135;

										BgL_new1187z00_2135 =
											((BgL_bbvzd2vlenzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_bbvzd2vlenzd2_bgl))));
										{	/* SawBbv/bbv-range.scm 228 */
											long BgL_arg1775z00_2136;

											BgL_arg1775z00_2136 =
												BGL_CLASS_NUM(BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1187z00_2135),
												BgL_arg1775z00_2136);
										}
										BgL_new1184z00_2131 = BgL_new1187z00_2135;
									}
									((((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_new1184z00_2131))->
											BgL_vecz00) =
										((obj_t) (((BgL_bbvzd2vlenzd2_bglt)
													COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_2125)))->
												BgL_vecz00)), BUNSPEC);
									{
										int BgL_auxz00_7251;

										{	/* SawBbv/bbv-range.scm 229 */
											int BgL_az00_2132;
											obj_t BgL_bz00_2133;

											BgL_az00_2132 =
												(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
														((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_2125)))->
												BgL_offsetz00);
											BgL_bz00_2133 = BgL_yz00_2126;
											if (((long) (BgL_az00_2132) > (long) CINT(BgL_bz00_2133)))
												{	/* SawBbv/bbv-range.scm 229 */
													BgL_auxz00_7251 = BgL_az00_2132;
												}
											else
												{	/* SawBbv/bbv-range.scm 229 */
													BgL_auxz00_7251 = CINT(BgL_bz00_2133);
												}
										}
										((((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_new1184z00_2131))->
												BgL_offsetz00) = ((int) BgL_auxz00_7251), BUNSPEC);
									}
									return ((obj_t) BgL_new1184z00_2131);
								}
							else
								{	/* SawBbv/bbv-range.scm 230 */
									bool_t BgL_test2842z00_7261;

									{	/* SawBbv/bbv-range.scm 230 */
										bool_t BgL_test2843z00_7262;

										{	/* SawBbv/bbv-range.scm 85 */
											obj_t BgL_classz00_4161;

											BgL_classz00_4161 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
											if (BGL_OBJECTP(BgL_xz00_2125))
												{	/* SawBbv/bbv-range.scm 85 */
													BgL_objectz00_bglt BgL_arg1807z00_4163;

													BgL_arg1807z00_4163 =
														(BgL_objectz00_bglt) (BgL_xz00_2125);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* SawBbv/bbv-range.scm 85 */
															long BgL_idxz00_4169;

															BgL_idxz00_4169 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4163);
															BgL_test2843z00_7262 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_4169 + 1L)) == BgL_classz00_4161);
														}
													else
														{	/* SawBbv/bbv-range.scm 85 */
															bool_t BgL_res2548z00_4194;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_oclassz00_4177;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1815z00_4185;
																	long BgL_arg1816z00_4186;

																	BgL_arg1815z00_4185 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* SawBbv/bbv-range.scm 85 */
																		long BgL_arg1817z00_4187;

																		BgL_arg1817z00_4187 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4163);
																		BgL_arg1816z00_4186 =
																			(BgL_arg1817z00_4187 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_4177 =
																		VECTOR_REF(BgL_arg1815z00_4185,
																		BgL_arg1816z00_4186);
																}
																{	/* SawBbv/bbv-range.scm 85 */
																	bool_t BgL__ortest_1115z00_4178;

																	BgL__ortest_1115z00_4178 =
																		(BgL_classz00_4161 == BgL_oclassz00_4177);
																	if (BgL__ortest_1115z00_4178)
																		{	/* SawBbv/bbv-range.scm 85 */
																			BgL_res2548z00_4194 =
																				BgL__ortest_1115z00_4178;
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_odepthz00_4179;

																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1804z00_4180;

																				BgL_arg1804z00_4180 =
																					(BgL_oclassz00_4177);
																				BgL_odepthz00_4179 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_4180);
																			}
																			if ((1L < BgL_odepthz00_4179))
																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1802z00_4182;

																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1803z00_4183;

																						BgL_arg1803z00_4183 =
																							(BgL_oclassz00_4177);
																						BgL_arg1802z00_4182 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_4183, 1L);
																					}
																					BgL_res2548z00_4194 =
																						(BgL_arg1802z00_4182 ==
																						BgL_classz00_4161);
																				}
																			else
																				{	/* SawBbv/bbv-range.scm 85 */
																					BgL_res2548z00_4194 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2843z00_7262 = BgL_res2548z00_4194;
														}
												}
											else
												{	/* SawBbv/bbv-range.scm 85 */
													BgL_test2843z00_7262 = ((bool_t) 0);
												}
										}
										if (BgL_test2843z00_7262)
											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_classz00_4195;

												BgL_classz00_4195 =
													BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
												if (BGL_OBJECTP(BgL_yz00_2126))
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_objectz00_bglt BgL_arg1807z00_4197;

														BgL_arg1807z00_4197 =
															(BgL_objectz00_bglt) (BgL_yz00_2126);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_idxz00_4203;

																BgL_idxz00_4203 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_4197);
																BgL_test2842z00_7261 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_4203 + 1L)) ==
																	BgL_classz00_4195);
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL_res2549z00_4228;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_oclassz00_4211;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1815z00_4219;
																		long BgL_arg1816z00_4220;

																		BgL_arg1815z00_4219 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_arg1817z00_4221;

																			BgL_arg1817z00_4221 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_4197);
																			BgL_arg1816z00_4220 =
																				(BgL_arg1817z00_4221 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_4211 =
																			VECTOR_REF(BgL_arg1815z00_4219,
																			BgL_arg1816z00_4220);
																	}
																	{	/* SawBbv/bbv-range.scm 85 */
																		bool_t BgL__ortest_1115z00_4212;

																		BgL__ortest_1115z00_4212 =
																			(BgL_classz00_4195 == BgL_oclassz00_4211);
																		if (BgL__ortest_1115z00_4212)
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2549z00_4228 =
																					BgL__ortest_1115z00_4212;
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				long BgL_odepthz00_4213;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1804z00_4214;

																					BgL_arg1804z00_4214 =
																						(BgL_oclassz00_4211);
																					BgL_odepthz00_4213 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_4214);
																				}
																				if ((1L < BgL_odepthz00_4213))
																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1802z00_4216;

																						{	/* SawBbv/bbv-range.scm 85 */
																							obj_t BgL_arg1803z00_4217;

																							BgL_arg1803z00_4217 =
																								(BgL_oclassz00_4211);
																							BgL_arg1802z00_4216 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_4217, 1L);
																						}
																						BgL_res2549z00_4228 =
																							(BgL_arg1802z00_4216 ==
																							BgL_classz00_4195);
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 85 */
																						BgL_res2549z00_4228 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2842z00_7261 = BgL_res2549z00_4228;
															}
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_test2842z00_7261 = ((bool_t) 0);
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 230 */
												BgL_test2842z00_7261 = ((bool_t) 0);
											}
									}
									if (BgL_test2842z00_7261)
										{	/* SawBbv/bbv-range.scm 230 */
											if (
												((long) (
														(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																	((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_2125)))->
															BgL_offsetz00)) >
													(long) ((((BgL_bbvzd2vlenzd2_bglt)
																COBJECT(((BgL_bbvzd2vlenzd2_bglt)
																		BgL_yz00_2126)))->BgL_offsetz00))))
												{	/* SawBbv/bbv-range.scm 231 */
													return BgL_xz00_2125;
												}
											else
												{	/* SawBbv/bbv-range.scm 231 */
													return BgL_yz00_2126;
												}
										}
									else
										{	/* SawBbv/bbv-range.scm 234 */
											bool_t BgL_test2853z00_7315;

											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_classz00_4235;

												BgL_classz00_4235 =
													BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
												if (BGL_OBJECTP(BgL_yz00_2126))
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_objectz00_bglt BgL_arg1807z00_4237;

														BgL_arg1807z00_4237 =
															(BgL_objectz00_bglt) (BgL_yz00_2126);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_idxz00_4243;

																BgL_idxz00_4243 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_4237);
																BgL_test2853z00_7315 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_4243 + 1L)) ==
																	BgL_classz00_4235);
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL_res2550z00_4268;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_oclassz00_4251;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1815z00_4259;
																		long BgL_arg1816z00_4260;

																		BgL_arg1815z00_4259 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_arg1817z00_4261;

																			BgL_arg1817z00_4261 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_4237);
																			BgL_arg1816z00_4260 =
																				(BgL_arg1817z00_4261 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_4251 =
																			VECTOR_REF(BgL_arg1815z00_4259,
																			BgL_arg1816z00_4260);
																	}
																	{	/* SawBbv/bbv-range.scm 85 */
																		bool_t BgL__ortest_1115z00_4252;

																		BgL__ortest_1115z00_4252 =
																			(BgL_classz00_4235 == BgL_oclassz00_4251);
																		if (BgL__ortest_1115z00_4252)
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2550z00_4268 =
																					BgL__ortest_1115z00_4252;
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				long BgL_odepthz00_4253;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1804z00_4254;

																					BgL_arg1804z00_4254 =
																						(BgL_oclassz00_4251);
																					BgL_odepthz00_4253 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_4254);
																				}
																				if ((1L < BgL_odepthz00_4253))
																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1802z00_4256;

																						{	/* SawBbv/bbv-range.scm 85 */
																							obj_t BgL_arg1803z00_4257;

																							BgL_arg1803z00_4257 =
																								(BgL_oclassz00_4251);
																							BgL_arg1802z00_4256 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_4257, 1L);
																						}
																						BgL_res2550z00_4268 =
																							(BgL_arg1802z00_4256 ==
																							BgL_classz00_4235);
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 85 */
																						BgL_res2550z00_4268 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2853z00_7315 = BgL_res2550z00_4268;
															}
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_test2853z00_7315 = ((bool_t) 0);
													}
											}
											if (BgL_test2853z00_7315)
												{
													obj_t BgL_yz00_7339;
													obj_t BgL_xz00_7338;

													BgL_xz00_7338 = BgL_yz00_2126;
													BgL_yz00_7339 = BgL_xz00_2125;
													BgL_yz00_2126 = BgL_yz00_7339;
													BgL_xz00_2125 = BgL_xz00_7338;
													goto BgL_zc3z04anonymousza31770ze3z87_2127;
												}
											else
												{	/* SawBbv/bbv-range.scm 234 */
													return BgL_unspecifiedz00_38;
												}
										}
								}
						}
					}
				else
					{	/* SawBbv/bbv-range.scm 222 */
						return BgL_mz00_2123;
					}
			}
		}

	}



/* bbv-max-fixnum */
	BGL_EXPORTED_DEF long BGl_bbvzd2maxzd2fixnumz00zzsaw_bbvzd2rangezd2(void)
	{
		{	/* SawBbv/bbv-range.scm 242 */
			{	/* SawBbv/bbv-range.scm 243 */
				long BgL_arg1808z00_6395;

				{	/* SawBbv/bbv-range.scm 243 */
					obj_t BgL_arg1812z00_6396;

					BgL_arg1812z00_6396 =
						BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0));
					BgL_arg1808z00_6395 = ((long) CINT(BgL_arg1812z00_6396) - 2L);
				}
				return (1L << (int) (BgL_arg1808z00_6395));
		}}

	}



/* &bbv-max-fixnum */
	obj_t BGl_z62bbvzd2maxzd2fixnumz62zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6241)
	{
		{	/* SawBbv/bbv-range.scm 242 */
			{	/* SawBbv/bbv-range.scm 243 */
				long BgL_tmpz00_7346;

				{	/* SawBbv/bbv-range.scm 243 */
					long BgL_arg1808z00_6397;

					{	/* SawBbv/bbv-range.scm 243 */
						obj_t BgL_arg1812z00_6398;

						BgL_arg1812z00_6398 =
							BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0));
						BgL_arg1808z00_6397 = ((long) CINT(BgL_arg1812z00_6398) - 2L);
					}
					BgL_tmpz00_7346 = (1L << (int) (BgL_arg1808z00_6397));
				}
				return BINT(BgL_tmpz00_7346);
			}
		}

	}



/* bbv-min-fixnum */
	BGL_EXPORTED_DEF long BGl_bbvzd2minzd2fixnumz00zzsaw_bbvzd2rangezd2(void)
	{
		{	/* SawBbv/bbv-range.scm 244 */
			{	/* SawBbv/bbv-range.scm 245 */
				long BgL_arg1820z00_6399;

				{	/* SawBbv/bbv-range.scm 245 */
					long BgL_arg1822z00_6400;

					{	/* SawBbv/bbv-range.scm 243 */
						long BgL_arg1808z00_6401;

						{	/* SawBbv/bbv-range.scm 243 */
							obj_t BgL_arg1812z00_6402;

							BgL_arg1812z00_6402 =
								BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0));
							BgL_arg1808z00_6401 = ((long) CINT(BgL_arg1812z00_6402) - 2L);
						}
						BgL_arg1822z00_6400 = (1L << (int) (BgL_arg1808z00_6401));
					}
					BgL_arg1820z00_6399 = NEG(BgL_arg1822z00_6400);
				}
				return (BgL_arg1820z00_6399 - 1L);
			}
		}

	}



/* &bbv-min-fixnum */
	obj_t BGl_z62bbvzd2minzd2fixnumz62zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6242)
	{
		{	/* SawBbv/bbv-range.scm 244 */
			{	/* SawBbv/bbv-range.scm 245 */
				long BgL_tmpz00_7362;

				{	/* SawBbv/bbv-range.scm 245 */
					long BgL_arg1820z00_6403;

					{	/* SawBbv/bbv-range.scm 245 */
						long BgL_arg1822z00_6404;

						{	/* SawBbv/bbv-range.scm 243 */
							long BgL_arg1808z00_6405;

							{	/* SawBbv/bbv-range.scm 243 */
								obj_t BgL_arg1812z00_6406;

								BgL_arg1812z00_6406 =
									BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0));
								BgL_arg1808z00_6405 = ((long) CINT(BgL_arg1812z00_6406) - 2L);
							}
							BgL_arg1822z00_6404 = (1L << (int) (BgL_arg1808z00_6405));
						}
						BgL_arg1820z00_6403 = NEG(BgL_arg1822z00_6404);
					}
					BgL_tmpz00_7362 = (BgL_arg1820z00_6403 - 1L);
				}
				return BINT(BgL_tmpz00_7362);
			}
		}

	}



/* ++ */
	obj_t BGl_zb2zb2z00zzsaw_bbvzd2rangezd2(obj_t BgL_iz00_41)
	{
		{	/* SawBbv/bbv-range.scm 249 */
			if ((BgL_iz00_41 == BGl_bbvzd2maxzd2fixnumzd2envzd2zzsaw_bbvzd2rangezd2))
				{	/* SawBbv/bbv-range.scm 251 */
					return BGl_bbvzd2maxzd2fixnumzd2envzd2zzsaw_bbvzd2rangezd2;
				}
			else
				{	/* SawBbv/bbv-range.scm 253 */
					bool_t BgL_test2859z00_7374;

					{	/* SawBbv/bbv-range.scm 85 */
						obj_t BgL_classz00_4281;

						BgL_classz00_4281 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
						if (BGL_OBJECTP(BgL_iz00_41))
							{	/* SawBbv/bbv-range.scm 85 */
								BgL_objectz00_bglt BgL_arg1807z00_4283;

								BgL_arg1807z00_4283 = (BgL_objectz00_bglt) (BgL_iz00_41);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-range.scm 85 */
										long BgL_idxz00_4289;

										BgL_idxz00_4289 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4283);
										BgL_test2859z00_7374 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4289 + 1L)) == BgL_classz00_4281);
									}
								else
									{	/* SawBbv/bbv-range.scm 85 */
										bool_t BgL_res2551z00_4314;

										{	/* SawBbv/bbv-range.scm 85 */
											obj_t BgL_oclassz00_4297;

											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_arg1815z00_4305;
												long BgL_arg1816z00_4306;

												BgL_arg1815z00_4305 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-range.scm 85 */
													long BgL_arg1817z00_4307;

													BgL_arg1817z00_4307 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4283);
													BgL_arg1816z00_4306 =
														(BgL_arg1817z00_4307 - OBJECT_TYPE);
												}
												BgL_oclassz00_4297 =
													VECTOR_REF(BgL_arg1815z00_4305, BgL_arg1816z00_4306);
											}
											{	/* SawBbv/bbv-range.scm 85 */
												bool_t BgL__ortest_1115z00_4298;

												BgL__ortest_1115z00_4298 =
													(BgL_classz00_4281 == BgL_oclassz00_4297);
												if (BgL__ortest_1115z00_4298)
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_res2551z00_4314 = BgL__ortest_1115z00_4298;
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_odepthz00_4299;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_arg1804z00_4300;

															BgL_arg1804z00_4300 = (BgL_oclassz00_4297);
															BgL_odepthz00_4299 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4300);
														}
														if ((1L < BgL_odepthz00_4299))
															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1802z00_4302;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1803z00_4303;

																	BgL_arg1803z00_4303 = (BgL_oclassz00_4297);
																	BgL_arg1802z00_4302 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4303,
																		1L);
																}
																BgL_res2551z00_4314 =
																	(BgL_arg1802z00_4302 == BgL_classz00_4281);
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																BgL_res2551z00_4314 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2859z00_7374 = BgL_res2551z00_4314;
									}
							}
						else
							{	/* SawBbv/bbv-range.scm 85 */
								BgL_test2859z00_7374 = ((bool_t) 0);
							}
					}
					if (BgL_test2859z00_7374)
						{	/* SawBbv/bbv-range.scm 254 */
							BgL_bbvzd2vlenzd2_bglt BgL_new1189z00_2157;

							{	/* SawBbv/bbv-range.scm 254 */
								BgL_bbvzd2vlenzd2_bglt BgL_new1192z00_2159;

								BgL_new1192z00_2159 =
									((BgL_bbvzd2vlenzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_bbvzd2vlenzd2_bgl))));
								{	/* SawBbv/bbv-range.scm 254 */
									long BgL_arg1832z00_2160;

									BgL_arg1832z00_2160 =
										BGL_CLASS_NUM(BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1192z00_2159),
										BgL_arg1832z00_2160);
								}
								BgL_new1189z00_2157 = BgL_new1192z00_2159;
							}
							((((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_new1189z00_2157))->
									BgL_vecz00) =
								((obj_t) (((BgL_bbvzd2vlenzd2_bglt)
											COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_iz00_41)))->
										BgL_vecz00)), BUNSPEC);
							((((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_new1189z00_2157))->
									BgL_offsetz00) =
								((int) (int) (((long) ((((BgL_bbvzd2vlenzd2_bglt)
														COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_iz00_41)))->
													BgL_offsetz00)) + 1L))), BUNSPEC);
							return ((obj_t) BgL_new1189z00_2157);
						}
					else
						{	/* SawBbv/bbv-range.scm 253 */
							if (INTEGERP(BgL_iz00_41))
								{	/* SawBbv/bbv-range.scm 257 */
									obj_t BgL_tmpz00_4323;

									BgL_tmpz00_4323 = BINT(0L);
									{	/* SawBbv/bbv-range.scm 257 */
										bool_t BgL_test2865z00_7414;

										{	/* SawBbv/bbv-range.scm 257 */
											obj_t BgL_tmpz00_7415;

											BgL_tmpz00_7415 = BINT(1L);
											BgL_test2865z00_7414 =
												BGL_ADDFX_OV(BgL_iz00_41, BgL_tmpz00_7415,
												BgL_tmpz00_4323);
										}
										if (BgL_test2865z00_7414)
											{	/* SawBbv/bbv-range.scm 257 */
												return
													bgl_bignum_add(bgl_long_to_bignum(
														(long) CINT(BgL_iz00_41)), CNST_TABLE_REF(1));
											}
										else
											{	/* SawBbv/bbv-range.scm 257 */
												return BgL_tmpz00_4323;
											}
									}
								}
							else
								{	/* SawBbv/bbv-range.scm 257 */
									return
										BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_iz00_41, BINT(1L));
								}
						}
				}
		}

	}



/* -- */
	obj_t BGl_zd2zd2z00zzsaw_bbvzd2rangezd2(obj_t BgL_iz00_42)
	{
		{	/* SawBbv/bbv-range.scm 259 */
			if ((BgL_iz00_42 == BGl_bbvzd2minzd2fixnumzd2envzd2zzsaw_bbvzd2rangezd2))
				{	/* SawBbv/bbv-range.scm 261 */
					return BGl_bbvzd2minzd2fixnumzd2envzd2zzsaw_bbvzd2rangezd2;
				}
			else
				{	/* SawBbv/bbv-range.scm 263 */
					bool_t BgL_test2867z00_7426;

					{	/* SawBbv/bbv-range.scm 85 */
						obj_t BgL_classz00_4331;

						BgL_classz00_4331 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
						if (BGL_OBJECTP(BgL_iz00_42))
							{	/* SawBbv/bbv-range.scm 85 */
								BgL_objectz00_bglt BgL_arg1807z00_4333;

								BgL_arg1807z00_4333 = (BgL_objectz00_bglt) (BgL_iz00_42);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-range.scm 85 */
										long BgL_idxz00_4339;

										BgL_idxz00_4339 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4333);
										BgL_test2867z00_7426 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4339 + 1L)) == BgL_classz00_4331);
									}
								else
									{	/* SawBbv/bbv-range.scm 85 */
										bool_t BgL_res2552z00_4364;

										{	/* SawBbv/bbv-range.scm 85 */
											obj_t BgL_oclassz00_4347;

											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_arg1815z00_4355;
												long BgL_arg1816z00_4356;

												BgL_arg1815z00_4355 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-range.scm 85 */
													long BgL_arg1817z00_4357;

													BgL_arg1817z00_4357 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4333);
													BgL_arg1816z00_4356 =
														(BgL_arg1817z00_4357 - OBJECT_TYPE);
												}
												BgL_oclassz00_4347 =
													VECTOR_REF(BgL_arg1815z00_4355, BgL_arg1816z00_4356);
											}
											{	/* SawBbv/bbv-range.scm 85 */
												bool_t BgL__ortest_1115z00_4348;

												BgL__ortest_1115z00_4348 =
													(BgL_classz00_4331 == BgL_oclassz00_4347);
												if (BgL__ortest_1115z00_4348)
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_res2552z00_4364 = BgL__ortest_1115z00_4348;
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_odepthz00_4349;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_arg1804z00_4350;

															BgL_arg1804z00_4350 = (BgL_oclassz00_4347);
															BgL_odepthz00_4349 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4350);
														}
														if ((1L < BgL_odepthz00_4349))
															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1802z00_4352;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1803z00_4353;

																	BgL_arg1803z00_4353 = (BgL_oclassz00_4347);
																	BgL_arg1802z00_4352 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4353,
																		1L);
																}
																BgL_res2552z00_4364 =
																	(BgL_arg1802z00_4352 == BgL_classz00_4331);
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																BgL_res2552z00_4364 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2867z00_7426 = BgL_res2552z00_4364;
									}
							}
						else
							{	/* SawBbv/bbv-range.scm 85 */
								BgL_test2867z00_7426 = ((bool_t) 0);
							}
					}
					if (BgL_test2867z00_7426)
						{	/* SawBbv/bbv-range.scm 264 */
							BgL_bbvzd2vlenzd2_bglt BgL_new1193z00_2163;

							{	/* SawBbv/bbv-range.scm 264 */
								BgL_bbvzd2vlenzd2_bglt BgL_new1196z00_2165;

								BgL_new1196z00_2165 =
									((BgL_bbvzd2vlenzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_bbvzd2vlenzd2_bgl))));
								{	/* SawBbv/bbv-range.scm 264 */
									long BgL_arg1835z00_2166;

									BgL_arg1835z00_2166 =
										BGL_CLASS_NUM(BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1196z00_2165),
										BgL_arg1835z00_2166);
								}
								BgL_new1193z00_2163 = BgL_new1196z00_2165;
							}
							((((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_new1193z00_2163))->
									BgL_vecz00) =
								((obj_t) (((BgL_bbvzd2vlenzd2_bglt)
											COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_iz00_42)))->
										BgL_vecz00)), BUNSPEC);
							((((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_new1193z00_2163))->
									BgL_offsetz00) =
								((int) (int) (((long) ((((BgL_bbvzd2vlenzd2_bglt)
														COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_iz00_42)))->
													BgL_offsetz00)) - 1L))), BUNSPEC);
							return ((obj_t) BgL_new1193z00_2163);
						}
					else
						{	/* SawBbv/bbv-range.scm 263 */
							if (INTEGERP(BgL_iz00_42))
								{	/* SawBbv/bbv-range.scm 267 */
									obj_t BgL_tmpz00_4373;

									BgL_tmpz00_4373 = BINT(0L);
									{	/* SawBbv/bbv-range.scm 267 */
										bool_t BgL_test2873z00_7466;

										{	/* SawBbv/bbv-range.scm 267 */
											obj_t BgL_tmpz00_7467;

											BgL_tmpz00_7467 = BINT(1L);
											BgL_test2873z00_7466 =
												BGL_SUBFX_OV(BgL_iz00_42, BgL_tmpz00_7467,
												BgL_tmpz00_4373);
										}
										if (BgL_test2873z00_7466)
											{	/* SawBbv/bbv-range.scm 267 */
												return
													bgl_bignum_sub(bgl_long_to_bignum(
														(long) CINT(BgL_iz00_42)), CNST_TABLE_REF(1));
											}
										else
											{	/* SawBbv/bbv-range.scm 267 */
												return BgL_tmpz00_4373;
											}
									}
								}
							else
								{	/* SawBbv/bbv-range.scm 267 */
									return
										BGl_2zd2zd2zz__r4_numbers_6_5z00(BgL_iz00_42, BINT(1L));
								}
						}
				}
		}

	}



/* bbv-range? */
	BGL_EXPORTED_DEF bool_t BGl_bbvzd2rangezf3z21zzsaw_bbvzd2rangezd2(obj_t
		BgL_oz00_43)
	{
		{	/* SawBbv/bbv-range.scm 272 */
			{	/* SawBbv/bbv-range.scm 273 */
				obj_t BgL_classz00_6407;

				BgL_classz00_6407 = BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2;
				if (BGL_OBJECTP(BgL_oz00_43))
					{	/* SawBbv/bbv-range.scm 273 */
						BgL_objectz00_bglt BgL_arg1807z00_6408;

						BgL_arg1807z00_6408 = (BgL_objectz00_bglt) (BgL_oz00_43);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-range.scm 273 */
								long BgL_idxz00_6409;

								BgL_idxz00_6409 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6408);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6409 + 1L)) == BgL_classz00_6407);
							}
						else
							{	/* SawBbv/bbv-range.scm 273 */
								bool_t BgL_res2553z00_6412;

								{	/* SawBbv/bbv-range.scm 273 */
									obj_t BgL_oclassz00_6413;

									{	/* SawBbv/bbv-range.scm 273 */
										obj_t BgL_arg1815z00_6414;
										long BgL_arg1816z00_6415;

										BgL_arg1815z00_6414 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-range.scm 273 */
											long BgL_arg1817z00_6416;

											BgL_arg1817z00_6416 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6408);
											BgL_arg1816z00_6415 = (BgL_arg1817z00_6416 - OBJECT_TYPE);
										}
										BgL_oclassz00_6413 =
											VECTOR_REF(BgL_arg1815z00_6414, BgL_arg1816z00_6415);
									}
									{	/* SawBbv/bbv-range.scm 273 */
										bool_t BgL__ortest_1115z00_6417;

										BgL__ortest_1115z00_6417 =
											(BgL_classz00_6407 == BgL_oclassz00_6413);
										if (BgL__ortest_1115z00_6417)
											{	/* SawBbv/bbv-range.scm 273 */
												BgL_res2553z00_6412 = BgL__ortest_1115z00_6417;
											}
										else
											{	/* SawBbv/bbv-range.scm 273 */
												long BgL_odepthz00_6418;

												{	/* SawBbv/bbv-range.scm 273 */
													obj_t BgL_arg1804z00_6419;

													BgL_arg1804z00_6419 = (BgL_oclassz00_6413);
													BgL_odepthz00_6418 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6419);
												}
												if ((1L < BgL_odepthz00_6418))
													{	/* SawBbv/bbv-range.scm 273 */
														obj_t BgL_arg1802z00_6420;

														{	/* SawBbv/bbv-range.scm 273 */
															obj_t BgL_arg1803z00_6421;

															BgL_arg1803z00_6421 = (BgL_oclassz00_6413);
															BgL_arg1802z00_6420 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6421,
																1L);
														}
														BgL_res2553z00_6412 =
															(BgL_arg1802z00_6420 == BgL_classz00_6407);
													}
												else
													{	/* SawBbv/bbv-range.scm 273 */
														BgL_res2553z00_6412 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2553z00_6412;
							}
					}
				else
					{	/* SawBbv/bbv-range.scm 273 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bbv-range? */
	obj_t BGl_z62bbvzd2rangezf3z43zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6243,
		obj_t BgL_oz00_6244)
	{
		{	/* SawBbv/bbv-range.scm 272 */
			return BBOOL(BGl_bbvzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_oz00_6244));
		}

	}



/* bbv-range-fixnum? */
	BGL_EXPORTED_DEF bool_t
		BGl_bbvzd2rangezd2fixnumzf3zf3zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_oz00_44)
	{
		{	/* SawBbv/bbv-range.scm 278 */
			{	/* SawBbv/bbv-range.scm 280 */
				bool_t BgL_test2878z00_7500;

				{	/* SawBbv/bbv-range.scm 280 */
					bool_t BgL__ortest_1199z00_2175;

					{	/* SawBbv/bbv-range.scm 280 */
						obj_t BgL_arg1844z00_2180;

						BgL_arg1844z00_2180 =
							(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_oz00_44))->BgL_loz00);
						{	/* SawBbv/bbv-range.scm 85 */
							obj_t BgL_classz00_4415;

							BgL_classz00_4415 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
							if (BGL_OBJECTP(BgL_arg1844z00_2180))
								{	/* SawBbv/bbv-range.scm 85 */
									BgL_objectz00_bglt BgL_arg1807z00_4417;

									BgL_arg1807z00_4417 =
										(BgL_objectz00_bglt) (BgL_arg1844z00_2180);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawBbv/bbv-range.scm 85 */
											long BgL_idxz00_4423;

											BgL_idxz00_4423 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4417);
											BgL__ortest_1199z00_2175 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4423 + 1L)) == BgL_classz00_4415);
										}
									else
										{	/* SawBbv/bbv-range.scm 85 */
											bool_t BgL_res2554z00_4448;

											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_oclassz00_4431;

												{	/* SawBbv/bbv-range.scm 85 */
													obj_t BgL_arg1815z00_4439;
													long BgL_arg1816z00_4440;

													BgL_arg1815z00_4439 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_arg1817z00_4441;

														BgL_arg1817z00_4441 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4417);
														BgL_arg1816z00_4440 =
															(BgL_arg1817z00_4441 - OBJECT_TYPE);
													}
													BgL_oclassz00_4431 =
														VECTOR_REF(BgL_arg1815z00_4439,
														BgL_arg1816z00_4440);
												}
												{	/* SawBbv/bbv-range.scm 85 */
													bool_t BgL__ortest_1115z00_4432;

													BgL__ortest_1115z00_4432 =
														(BgL_classz00_4415 == BgL_oclassz00_4431);
													if (BgL__ortest_1115z00_4432)
														{	/* SawBbv/bbv-range.scm 85 */
															BgL_res2554z00_4448 = BgL__ortest_1115z00_4432;
														}
													else
														{	/* SawBbv/bbv-range.scm 85 */
															long BgL_odepthz00_4433;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1804z00_4434;

																BgL_arg1804z00_4434 = (BgL_oclassz00_4431);
																BgL_odepthz00_4433 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4434);
															}
															if ((1L < BgL_odepthz00_4433))
																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1802z00_4436;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1803z00_4437;

																		BgL_arg1803z00_4437 = (BgL_oclassz00_4431);
																		BgL_arg1802z00_4436 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4437, 1L);
																	}
																	BgL_res2554z00_4448 =
																		(BgL_arg1802z00_4436 == BgL_classz00_4415);
																}
															else
																{	/* SawBbv/bbv-range.scm 85 */
																	BgL_res2554z00_4448 = ((bool_t) 0);
																}
														}
												}
											}
											BgL__ortest_1199z00_2175 = BgL_res2554z00_4448;
										}
								}
							else
								{	/* SawBbv/bbv-range.scm 85 */
									BgL__ortest_1199z00_2175 = ((bool_t) 0);
								}
						}
					}
					if (BgL__ortest_1199z00_2175)
						{	/* SawBbv/bbv-range.scm 280 */
							BgL_test2878z00_7500 = BgL__ortest_1199z00_2175;
						}
					else
						{	/* SawBbv/bbv-range.scm 280 */
							if (INTEGERP(
									(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_oz00_44))->
										BgL_loz00)))
								{	/* SawBbv/bbv-range.scm 280 */
									obj_t BgL_arg1840z00_2177;
									long BgL_arg1842z00_2178;

									BgL_arg1840z00_2177 =
										(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_oz00_44))->
										BgL_loz00);
									{	/* SawBbv/bbv-range.scm 245 */
										long BgL_arg1820z00_4449;

										{	/* SawBbv/bbv-range.scm 245 */
											long BgL_arg1822z00_4450;

											{	/* SawBbv/bbv-range.scm 243 */
												long BgL_arg1808z00_4451;

												{	/* SawBbv/bbv-range.scm 243 */
													obj_t BgL_arg1812z00_4452;

													BgL_arg1812z00_4452 =
														BGl_bigloozd2configzd2zz__configurez00
														(CNST_TABLE_REF(0));
													BgL_arg1808z00_4451 =
														((long) CINT(BgL_arg1812z00_4452) - 2L);
												}
												BgL_arg1822z00_4450 =
													(1L << (int) (BgL_arg1808z00_4451));
											}
											BgL_arg1820z00_4449 = NEG(BgL_arg1822z00_4450);
										}
										BgL_arg1842z00_2178 = (BgL_arg1820z00_4449 - 1L);
									}
									BgL_test2878z00_7500 =
										((long) CINT(BgL_arg1840z00_2177) >= BgL_arg1842z00_2178);
								}
							else
								{	/* SawBbv/bbv-range.scm 280 */
									BgL_test2878z00_7500 = ((bool_t) 0);
								}
						}
				}
				if (BgL_test2878z00_7500)
					{	/* SawBbv/bbv-range.scm 281 */
						bool_t BgL__ortest_1201z00_2169;

						{	/* SawBbv/bbv-range.scm 281 */
							obj_t BgL_arg1839z00_2174;

							BgL_arg1839z00_2174 =
								(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_oz00_44))->BgL_upz00);
							{	/* SawBbv/bbv-range.scm 85 */
								obj_t BgL_classz00_4459;

								BgL_classz00_4459 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
								if (BGL_OBJECTP(BgL_arg1839z00_2174))
									{	/* SawBbv/bbv-range.scm 85 */
										BgL_objectz00_bglt BgL_arg1807z00_4461;

										BgL_arg1807z00_4461 =
											(BgL_objectz00_bglt) (BgL_arg1839z00_2174);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawBbv/bbv-range.scm 85 */
												long BgL_idxz00_4467;

												BgL_idxz00_4467 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4461);
												BgL__ortest_1201z00_2169 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4467 + 1L)) == BgL_classz00_4459);
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												bool_t BgL_res2555z00_4492;

												{	/* SawBbv/bbv-range.scm 85 */
													obj_t BgL_oclassz00_4475;

													{	/* SawBbv/bbv-range.scm 85 */
														obj_t BgL_arg1815z00_4483;
														long BgL_arg1816z00_4484;

														BgL_arg1815z00_4483 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawBbv/bbv-range.scm 85 */
															long BgL_arg1817z00_4485;

															BgL_arg1817z00_4485 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4461);
															BgL_arg1816z00_4484 =
																(BgL_arg1817z00_4485 - OBJECT_TYPE);
														}
														BgL_oclassz00_4475 =
															VECTOR_REF(BgL_arg1815z00_4483,
															BgL_arg1816z00_4484);
													}
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL__ortest_1115z00_4476;

														BgL__ortest_1115z00_4476 =
															(BgL_classz00_4459 == BgL_oclassz00_4475);
														if (BgL__ortest_1115z00_4476)
															{	/* SawBbv/bbv-range.scm 85 */
																BgL_res2555z00_4492 = BgL__ortest_1115z00_4476;
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_odepthz00_4477;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1804z00_4478;

																	BgL_arg1804z00_4478 = (BgL_oclassz00_4475);
																	BgL_odepthz00_4477 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4478);
																}
																if ((1L < BgL_odepthz00_4477))
																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1802z00_4480;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1803z00_4481;

																			BgL_arg1803z00_4481 =
																				(BgL_oclassz00_4475);
																			BgL_arg1802z00_4480 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4481, 1L);
																		}
																		BgL_res2555z00_4492 =
																			(BgL_arg1802z00_4480 ==
																			BgL_classz00_4459);
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2555z00_4492 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL__ortest_1201z00_2169 = BgL_res2555z00_4492;
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 85 */
										BgL__ortest_1201z00_2169 = ((bool_t) 0);
									}
							}
						}
						if (BgL__ortest_1201z00_2169)
							{	/* SawBbv/bbv-range.scm 281 */
								return BgL__ortest_1201z00_2169;
							}
						else
							{	/* SawBbv/bbv-range.scm 281 */
								if (INTEGERP(
										(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_oz00_44))->
											BgL_upz00)))
									{	/* SawBbv/bbv-range.scm 281 */
										obj_t BgL_arg1836z00_2171;
										long BgL_arg1837z00_2172;

										BgL_arg1836z00_2171 =
											(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_oz00_44))->
											BgL_upz00);
										{	/* SawBbv/bbv-range.scm 243 */
											long BgL_arg1808z00_4493;

											{	/* SawBbv/bbv-range.scm 243 */
												obj_t BgL_arg1812z00_4494;

												BgL_arg1812z00_4494 =
													BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
													(0));
												BgL_arg1808z00_4493 =
													((long) CINT(BgL_arg1812z00_4494) - 2L);
											}
											BgL_arg1837z00_2172 = (1L << (int) (BgL_arg1808z00_4493));
										}
										return
											((long) CINT(BgL_arg1836z00_2171) <= BgL_arg1837z00_2172);
									}
								else
									{	/* SawBbv/bbv-range.scm 281 */
										return ((bool_t) 0);
									}
							}
					}
				else
					{	/* SawBbv/bbv-range.scm 280 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bbv-range-fixnum? */
	obj_t BGl_z62bbvzd2rangezd2fixnumzf3z91zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6245, obj_t BgL_oz00_6246)
	{
		{	/* SawBbv/bbv-range.scm 278 */
			return
				BBOOL(BGl_bbvzd2rangezd2fixnumzf3zf3zzsaw_bbvzd2rangezd2(
					((BgL_bbvzd2rangezd2_bglt) BgL_oz00_6246)));
		}

	}



/* empty-range? */
	BGL_EXPORTED_DEF bool_t
		BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_rz00_50)
	{
		{	/* SawBbv/bbv-range.scm 350 */
			{	/* SawBbv/bbv-range.scm 351 */
				bool_t BgL_test2891z00_7578;

				if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(
						(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_rz00_50))->BgL_loz00)))
					{	/* SawBbv/bbv-range.scm 351 */
						BgL_test2891z00_7578 =
							BGl_numberzf3zf3zz__r4_numbers_6_5z00(
							(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_rz00_50))->BgL_upz00));
					}
				else
					{	/* SawBbv/bbv-range.scm 351 */
						BgL_test2891z00_7578 = ((bool_t) 0);
					}
				if (BgL_test2891z00_7578)
					{	/* SawBbv/bbv-range.scm 352 */
						obj_t BgL_a1515z00_2195;

						BgL_a1515z00_2195 =
							(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_rz00_50))->BgL_loz00);
						{	/* SawBbv/bbv-range.scm 352 */
							obj_t BgL_b1516z00_2196;

							BgL_b1516z00_2196 =
								(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_rz00_50))->BgL_upz00);
							{	/* SawBbv/bbv-range.scm 352 */

								{	/* SawBbv/bbv-range.scm 352 */
									bool_t BgL_test2893z00_7586;

									if (INTEGERP(BgL_a1515z00_2195))
										{	/* SawBbv/bbv-range.scm 352 */
											BgL_test2893z00_7586 = INTEGERP(BgL_b1516z00_2196);
										}
									else
										{	/* SawBbv/bbv-range.scm 352 */
											BgL_test2893z00_7586 = ((bool_t) 0);
										}
									if (BgL_test2893z00_7586)
										{	/* SawBbv/bbv-range.scm 352 */
											return
												(
												(long) CINT(BgL_a1515z00_2195) >
												(long) CINT(BgL_b1516z00_2196));
										}
									else
										{	/* SawBbv/bbv-range.scm 352 */
											return
												BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_a1515z00_2195,
												BgL_b1516z00_2196);
										}
								}
							}
						}
					}
				else
					{	/* SawBbv/bbv-range.scm 351 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &empty-range? */
	obj_t BGl_z62emptyzd2rangezf3z43zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6247,
		obj_t BgL_rz00_6248)
	{
		{	/* SawBbv/bbv-range.scm 350 */
			return
				BBOOL(BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(
					((BgL_bbvzd2rangezd2_bglt) BgL_rz00_6248)));
		}

	}



/* fixnum-range */
	BGL_EXPORTED_DEF BgL_bbvzd2rangezd2_bglt
		BGl_fixnumzd2rangezd2zzsaw_bbvzd2rangezd2(void)
	{
		{	/* SawBbv/bbv-range.scm 365 */
			return BGl_za2fixnumzd2rangeza2zd2zzsaw_bbvzd2rangezd2;
		}

	}



/* &fixnum-range */
	BgL_bbvzd2rangezd2_bglt BGl_z62fixnumzd2rangezb0zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6249)
	{
		{	/* SawBbv/bbv-range.scm 365 */
			return BGl_fixnumzd2rangezd2zzsaw_bbvzd2rangezd2();
		}

	}



/* fixnum->range */
	BGL_EXPORTED_DEF BgL_bbvzd2rangezd2_bglt
		BGl_fixnumzd2ze3rangez31zzsaw_bbvzd2rangezd2(long BgL_nz00_51)
	{
		{	/* SawBbv/bbv-range.scm 371 */
			{	/* SawBbv/bbv-range.scm 372 */
				BgL_bbvzd2rangezd2_bglt BgL_new1211z00_2201;

				{	/* SawBbv/bbv-range.scm 373 */
					BgL_bbvzd2rangezd2_bglt BgL_new1210z00_2202;

					BgL_new1210z00_2202 =
						((BgL_bbvzd2rangezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2rangezd2_bgl))));
					{	/* SawBbv/bbv-range.scm 373 */
						long BgL_arg1858z00_2203;

						BgL_arg1858z00_2203 =
							BGL_CLASS_NUM(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1210z00_2202), BgL_arg1858z00_2203);
					}
					BgL_new1211z00_2201 = BgL_new1210z00_2202;
				}
				((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1211z00_2201))->BgL_loz00) =
					((obj_t) BINT(BgL_nz00_51)), BUNSPEC);
				((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1211z00_2201))->BgL_upz00) =
					((obj_t) BINT(BgL_nz00_51)), BUNSPEC);
				return BgL_new1211z00_2201;
			}
		}

	}



/* &fixnum->range */
	BgL_bbvzd2rangezd2_bglt BGl_z62fixnumzd2ze3rangez53zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6250, obj_t BgL_nz00_6251)
	{
		{	/* SawBbv/bbv-range.scm 371 */
			return
				BGl_fixnumzd2ze3rangez31zzsaw_bbvzd2rangezd2(
				(long) CINT(BgL_nz00_6251));
		}

	}



/* vlen-range */
	BGL_EXPORTED_DEF BgL_bbvzd2rangezd2_bglt
		BGl_vlenzd2rangezd2zzsaw_bbvzd2rangezd2(void)
	{
		{	/* SawBbv/bbv-range.scm 387 */
			return BGl_za2vlenzd2rangeza2zd2zzsaw_bbvzd2rangezd2;
		}

	}



/* &vlen-range */
	BgL_bbvzd2rangezd2_bglt BGl_z62vlenzd2rangezb0zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6252)
	{
		{	/* SawBbv/bbv-range.scm 387 */
			return BGl_vlenzd2rangezd2zzsaw_bbvzd2rangezd2();
		}

	}



/* vlen->range */
	BgL_bbvzd2rangezd2_bglt BGl_vlenzd2ze3rangez31zzsaw_bbvzd2rangezd2(obj_t
		BgL_regz00_52, BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_53)
	{
		{	/* SawBbv/bbv-range.scm 393 */
			{
				obj_t BgL_regz00_2212;
				BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_2213;

				if (CBOOL(BGl_za2bbvzd2optimzd2vlengthza2z00zzsaw_bbvzd2configzd2))
					{	/* SawBbv/bbv-range.scm 414 */
						BgL_bbvzd2vlenzd2_bglt BgL_valz00_2205;

						{	/* SawBbv/bbv-range.scm 414 */
							BgL_bbvzd2vlenzd2_bglt BgL_new1219z00_2209;

							{	/* SawBbv/bbv-range.scm 415 */
								BgL_bbvzd2vlenzd2_bglt BgL_new1218z00_2210;

								BgL_new1218z00_2210 =
									((BgL_bbvzd2vlenzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_bbvzd2vlenzd2_bgl))));
								{	/* SawBbv/bbv-range.scm 415 */
									long BgL_arg1860z00_2211;

									BgL_arg1860z00_2211 =
										BGL_CLASS_NUM(BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1218z00_2210),
										BgL_arg1860z00_2211);
								}
								BgL_new1219z00_2209 = BgL_new1218z00_2210;
							}
							((((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_new1219z00_2209))->
									BgL_vecz00) = ((obj_t) BgL_regz00_52), BUNSPEC);
							((((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_new1219z00_2209))->
									BgL_offsetz00) = ((int) (int) (0L)), BUNSPEC);
							BgL_valz00_2205 = BgL_new1219z00_2209;
						}
						{	/* SawBbv/bbv-range.scm 417 */
							BgL_bbvzd2rangezd2_bglt BgL_new1221z00_2206;

							{	/* SawBbv/bbv-range.scm 418 */
								BgL_bbvzd2rangezd2_bglt BgL_new1220z00_2207;

								BgL_new1220z00_2207 =
									((BgL_bbvzd2rangezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_bbvzd2rangezd2_bgl))));
								{	/* SawBbv/bbv-range.scm 418 */
									long BgL_arg1859z00_2208;

									BgL_arg1859z00_2208 =
										BGL_CLASS_NUM(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1220z00_2207),
										BgL_arg1859z00_2208);
								}
								BgL_new1221z00_2206 = BgL_new1220z00_2207;
							}
							{
								obj_t BgL_auxz00_7622;

								{	/* SawBbv/bbv-range.scm 418 */
									long BgL_tmpz00_7623;

									BgL_regz00_2212 = BgL_regz00_52;
									BgL_ctxz00_2213 = BgL_ctxz00_53;
									{
										long BgL_lowz00_2218;
										obj_t BgL_entriesz00_2219;

										BgL_lowz00_2218 = 0L;
										BgL_entriesz00_2219 =
											(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_2213))->
											BgL_entriesz00);
									BgL_zc3z04anonymousza31863ze3z87_2220:
										if (NULLP(BgL_entriesz00_2219))
											{	/* SawBbv/bbv-range.scm 399 */
												BgL_tmpz00_7623 = BgL_lowz00_2218;
											}
										else
											{	/* SawBbv/bbv-range.scm 401 */
												BgL_bbvzd2ctxentryzd2_bglt BgL_i1215z00_2222;

												BgL_i1215z00_2222 =
													((BgL_bbvzd2ctxentryzd2_bglt)
													CAR(((obj_t) BgL_entriesz00_2219)));
												{	/* SawBbv/bbv-range.scm 402 */
													bool_t BgL_test2897z00_7629;

													{	/* SawBbv/bbv-range.scm 402 */
														obj_t BgL_arg1883z00_2244;

														BgL_arg1883z00_2244 =
															(((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_i1215z00_2222))->BgL_valuez00);
														{	/* SawBbv/bbv-range.scm 402 */
															obj_t BgL_classz00_4591;

															BgL_classz00_4591 =
																BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2;
															if (BGL_OBJECTP(BgL_arg1883z00_2244))
																{	/* SawBbv/bbv-range.scm 402 */
																	BgL_objectz00_bglt BgL_arg1807z00_4593;

																	BgL_arg1807z00_4593 =
																		(BgL_objectz00_bglt) (BgL_arg1883z00_2244);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* SawBbv/bbv-range.scm 402 */
																			long BgL_idxz00_4599;

																			BgL_idxz00_4599 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_4593);
																			BgL_test2897z00_7629 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_4599 + 1L)) ==
																				BgL_classz00_4591);
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 402 */
																			bool_t BgL_res2558z00_4624;

																			{	/* SawBbv/bbv-range.scm 402 */
																				obj_t BgL_oclassz00_4607;

																				{	/* SawBbv/bbv-range.scm 402 */
																					obj_t BgL_arg1815z00_4615;
																					long BgL_arg1816z00_4616;

																					BgL_arg1815z00_4615 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* SawBbv/bbv-range.scm 402 */
																						long BgL_arg1817z00_4617;

																						BgL_arg1817z00_4617 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_4593);
																						BgL_arg1816z00_4616 =
																							(BgL_arg1817z00_4617 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_4607 =
																						VECTOR_REF(BgL_arg1815z00_4615,
																						BgL_arg1816z00_4616);
																				}
																				{	/* SawBbv/bbv-range.scm 402 */
																					bool_t BgL__ortest_1115z00_4608;

																					BgL__ortest_1115z00_4608 =
																						(BgL_classz00_4591 ==
																						BgL_oclassz00_4607);
																					if (BgL__ortest_1115z00_4608)
																						{	/* SawBbv/bbv-range.scm 402 */
																							BgL_res2558z00_4624 =
																								BgL__ortest_1115z00_4608;
																						}
																					else
																						{	/* SawBbv/bbv-range.scm 402 */
																							long BgL_odepthz00_4609;

																							{	/* SawBbv/bbv-range.scm 402 */
																								obj_t BgL_arg1804z00_4610;

																								BgL_arg1804z00_4610 =
																									(BgL_oclassz00_4607);
																								BgL_odepthz00_4609 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_4610);
																							}
																							if ((1L < BgL_odepthz00_4609))
																								{	/* SawBbv/bbv-range.scm 402 */
																									obj_t BgL_arg1802z00_4612;

																									{	/* SawBbv/bbv-range.scm 402 */
																										obj_t BgL_arg1803z00_4613;

																										BgL_arg1803z00_4613 =
																											(BgL_oclassz00_4607);
																										BgL_arg1802z00_4612 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_4613, 1L);
																									}
																									BgL_res2558z00_4624 =
																										(BgL_arg1802z00_4612 ==
																										BgL_classz00_4591);
																								}
																							else
																								{	/* SawBbv/bbv-range.scm 402 */
																									BgL_res2558z00_4624 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2897z00_7629 =
																				BgL_res2558z00_4624;
																		}
																}
															else
																{	/* SawBbv/bbv-range.scm 402 */
																	BgL_test2897z00_7629 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test2897z00_7629)
														{	/* SawBbv/bbv-range.scm 403 */
															BgL_bbvzd2rangezd2_bglt BgL_i1216z00_2225;

															BgL_i1216z00_2225 =
																((BgL_bbvzd2rangezd2_bglt)
																(((BgL_bbvzd2ctxentryzd2_bglt)
																		COBJECT(BgL_i1215z00_2222))->BgL_valuez00));
															{	/* SawBbv/bbv-range.scm 404 */
																bool_t BgL_test2902z00_7655;

																{	/* SawBbv/bbv-range.scm 404 */
																	obj_t BgL_arg1880z00_2242;

																	BgL_arg1880z00_2242 =
																		(((BgL_bbvzd2rangezd2_bglt)
																			COBJECT(BgL_i1216z00_2225))->BgL_upz00);
																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_classz00_4625;

																		BgL_classz00_4625 =
																			BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
																		if (BGL_OBJECTP(BgL_arg1880z00_2242))
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_objectz00_bglt BgL_arg1807z00_4627;

																				BgL_arg1807z00_4627 =
																					(BgL_objectz00_bglt)
																					(BgL_arg1880z00_2242);
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* SawBbv/bbv-range.scm 85 */
																						long BgL_idxz00_4633;

																						BgL_idxz00_4633 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_4627);
																						BgL_test2902z00_7655 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_4633 + 1L)) ==
																							BgL_classz00_4625);
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 85 */
																						bool_t BgL_res2559z00_4658;

																						{	/* SawBbv/bbv-range.scm 85 */
																							obj_t BgL_oclassz00_4641;

																							{	/* SawBbv/bbv-range.scm 85 */
																								obj_t BgL_arg1815z00_4649;
																								long BgL_arg1816z00_4650;

																								BgL_arg1815z00_4649 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* SawBbv/bbv-range.scm 85 */
																									long BgL_arg1817z00_4651;

																									BgL_arg1817z00_4651 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_4627);
																									BgL_arg1816z00_4650 =
																										(BgL_arg1817z00_4651 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_4641 =
																									VECTOR_REF
																									(BgL_arg1815z00_4649,
																									BgL_arg1816z00_4650);
																							}
																							{	/* SawBbv/bbv-range.scm 85 */
																								bool_t BgL__ortest_1115z00_4642;

																								BgL__ortest_1115z00_4642 =
																									(BgL_classz00_4625 ==
																									BgL_oclassz00_4641);
																								if (BgL__ortest_1115z00_4642)
																									{	/* SawBbv/bbv-range.scm 85 */
																										BgL_res2559z00_4658 =
																											BgL__ortest_1115z00_4642;
																									}
																								else
																									{	/* SawBbv/bbv-range.scm 85 */
																										long BgL_odepthz00_4643;

																										{	/* SawBbv/bbv-range.scm 85 */
																											obj_t BgL_arg1804z00_4644;

																											BgL_arg1804z00_4644 =
																												(BgL_oclassz00_4641);
																											BgL_odepthz00_4643 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_4644);
																										}
																										if (
																											(1L < BgL_odepthz00_4643))
																											{	/* SawBbv/bbv-range.scm 85 */
																												obj_t
																													BgL_arg1802z00_4646;
																												{	/* SawBbv/bbv-range.scm 85 */
																													obj_t
																														BgL_arg1803z00_4647;
																													BgL_arg1803z00_4647 =
																														(BgL_oclassz00_4641);
																													BgL_arg1802z00_4646 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_4647,
																														1L);
																												}
																												BgL_res2559z00_4658 =
																													(BgL_arg1802z00_4646
																													== BgL_classz00_4625);
																											}
																										else
																											{	/* SawBbv/bbv-range.scm 85 */
																												BgL_res2559z00_4658 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test2902z00_7655 =
																							BgL_res2559z00_4658;
																					}
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_test2902z00_7655 = ((bool_t) 0);
																			}
																	}
																}
																if (BgL_test2902z00_7655)
																	{	/* SawBbv/bbv-range.scm 405 */
																		BgL_bbvzd2vlenzd2_bglt BgL_i1217z00_2228;

																		BgL_i1217z00_2228 =
																			((BgL_bbvzd2vlenzd2_bglt)
																			(((BgL_bbvzd2rangezd2_bglt)
																					COBJECT(BgL_i1216z00_2225))->
																				BgL_upz00));
																		if (((((BgL_bbvzd2vlenzd2_bglt)
																						COBJECT(BgL_i1217z00_2228))->
																					BgL_vecz00) == BgL_regz00_2212))
																			{
																				obj_t BgL_entriesz00_7692;
																				long BgL_lowz00_7684;

																				{	/* SawBbv/bbv-range.scm 408 */
																					long BgL_bz00_2235;

																					BgL_bz00_2235 =
																						(
																						(long) CINT(
																							(((BgL_bbvzd2rangezd2_bglt)
																									COBJECT(BgL_i1216z00_2225))->
																								BgL_loz00)) -
																						(long) ((((BgL_bbvzd2vlenzd2_bglt)
																									COBJECT(BgL_i1217z00_2228))->
																								BgL_offsetz00)));
																					if ((BgL_lowz00_2218 > BgL_bz00_2235))
																						{	/* SawBbv/bbv-range.scm 408 */
																							BgL_lowz00_7684 = BgL_lowz00_2218;
																						}
																					else
																						{	/* SawBbv/bbv-range.scm 408 */
																							BgL_lowz00_7684 = BgL_bz00_2235;
																						}
																				}
																				BgL_entriesz00_7692 =
																					CDR(((obj_t) BgL_entriesz00_2219));
																				BgL_entriesz00_2219 =
																					BgL_entriesz00_7692;
																				BgL_lowz00_2218 = BgL_lowz00_7684;
																				goto
																					BgL_zc3z04anonymousza31863ze3z87_2220;
																			}
																		else
																			{
																				obj_t BgL_entriesz00_7695;

																				BgL_entriesz00_7695 =
																					CDR(((obj_t) BgL_entriesz00_2219));
																				BgL_entriesz00_2219 =
																					BgL_entriesz00_7695;
																				goto
																					BgL_zc3z04anonymousza31863ze3z87_2220;
																			}
																	}
																else
																	{
																		obj_t BgL_entriesz00_7698;

																		BgL_entriesz00_7698 =
																			CDR(((obj_t) BgL_entriesz00_2219));
																		BgL_entriesz00_2219 = BgL_entriesz00_7698;
																		goto BgL_zc3z04anonymousza31863ze3z87_2220;
																	}
															}
														}
													else
														{
															obj_t BgL_entriesz00_7701;

															BgL_entriesz00_7701 =
																CDR(((obj_t) BgL_entriesz00_2219));
															BgL_entriesz00_2219 = BgL_entriesz00_7701;
															goto BgL_zc3z04anonymousza31863ze3z87_2220;
														}
												}
											}
									}
									BgL_auxz00_7622 = BINT(BgL_tmpz00_7623);
								}
								((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1221z00_2206))->
										BgL_loz00) = ((obj_t) BgL_auxz00_7622), BUNSPEC);
							}
							((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1221z00_2206))->
									BgL_upz00) = ((obj_t) ((obj_t) BgL_valz00_2205)), BUNSPEC);
							return BgL_new1221z00_2206;
						}
					}
				else
					{	/* SawBbv/bbv-range.scm 413 */
						return BGl_za2vlenzd2rangeza2zd2zzsaw_bbvzd2rangezd2;
					}
			}
		}

	}



/* range-type? */
	BGL_EXPORTED_DEF bool_t BGl_rangezd2typezf3z21zzsaw_bbvzd2rangezd2(obj_t
		BgL_tyz00_54)
	{
		{	/* SawBbv/bbv-range.scm 425 */
			{	/* SawBbv/bbv-range.scm 426 */
				bool_t BgL__ortest_1222z00_2247;

				BgL__ortest_1222z00_2247 =
					(BgL_tyz00_54 == BGl_za2bintza2z00zztype_cachez00);
				if (BgL__ortest_1222z00_2247)
					{	/* SawBbv/bbv-range.scm 426 */
						return BgL__ortest_1222z00_2247;
					}
				else
					{	/* SawBbv/bbv-range.scm 426 */
						return (BgL_tyz00_54 == BGl_za2longza2z00zztype_cachez00);
					}
			}
		}

	}



/* &range-type? */
	obj_t BGl_z62rangezd2typezf3z43zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6253,
		obj_t BgL_tyz00_6254)
	{
		{	/* SawBbv/bbv-range.scm 425 */
			return BBOOL(BGl_rangezd2typezf3z21zzsaw_bbvzd2rangezd2(BgL_tyz00_6254));
		}

	}



/* rtl-range */
	BGL_EXPORTED_DEF obj_t BGl_rtlzd2rangezd2zzsaw_bbvzd2rangezd2(obj_t
		BgL_iz00_55, BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_56)
	{
		{	/* SawBbv/bbv-range.scm 431 */
		BGl_rtlzd2rangezd2zzsaw_bbvzd2rangezd2:
			{	/* SawBbv/bbv-range.scm 436 */
				bool_t BgL_test2910z00_7714;

				{	/* SawBbv/bbv-range.scm 436 */
					obj_t BgL_classz00_4673;

					BgL_classz00_4673 = BGl_rtl_regz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_iz00_55))
						{	/* SawBbv/bbv-range.scm 436 */
							BgL_objectz00_bglt BgL_arg1807z00_4675;

							BgL_arg1807z00_4675 = (BgL_objectz00_bglt) (BgL_iz00_55);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-range.scm 436 */
									long BgL_idxz00_4681;

									BgL_idxz00_4681 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4675);
									BgL_test2910z00_7714 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4681 + 1L)) == BgL_classz00_4673);
								}
							else
								{	/* SawBbv/bbv-range.scm 436 */
									bool_t BgL_res2560z00_4706;

									{	/* SawBbv/bbv-range.scm 436 */
										obj_t BgL_oclassz00_4689;

										{	/* SawBbv/bbv-range.scm 436 */
											obj_t BgL_arg1815z00_4697;
											long BgL_arg1816z00_4698;

											BgL_arg1815z00_4697 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-range.scm 436 */
												long BgL_arg1817z00_4699;

												BgL_arg1817z00_4699 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4675);
												BgL_arg1816z00_4698 =
													(BgL_arg1817z00_4699 - OBJECT_TYPE);
											}
											BgL_oclassz00_4689 =
												VECTOR_REF(BgL_arg1815z00_4697, BgL_arg1816z00_4698);
										}
										{	/* SawBbv/bbv-range.scm 436 */
											bool_t BgL__ortest_1115z00_4690;

											BgL__ortest_1115z00_4690 =
												(BgL_classz00_4673 == BgL_oclassz00_4689);
											if (BgL__ortest_1115z00_4690)
												{	/* SawBbv/bbv-range.scm 436 */
													BgL_res2560z00_4706 = BgL__ortest_1115z00_4690;
												}
											else
												{	/* SawBbv/bbv-range.scm 436 */
													long BgL_odepthz00_4691;

													{	/* SawBbv/bbv-range.scm 436 */
														obj_t BgL_arg1804z00_4692;

														BgL_arg1804z00_4692 = (BgL_oclassz00_4689);
														BgL_odepthz00_4691 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4692);
													}
													if ((1L < BgL_odepthz00_4691))
														{	/* SawBbv/bbv-range.scm 436 */
															obj_t BgL_arg1802z00_4694;

															{	/* SawBbv/bbv-range.scm 436 */
																obj_t BgL_arg1803z00_4695;

																BgL_arg1803z00_4695 = (BgL_oclassz00_4689);
																BgL_arg1802z00_4694 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4695,
																	1L);
															}
															BgL_res2560z00_4706 =
																(BgL_arg1802z00_4694 == BgL_classz00_4673);
														}
													else
														{	/* SawBbv/bbv-range.scm 436 */
															BgL_res2560z00_4706 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2910z00_7714 = BgL_res2560z00_4706;
								}
						}
					else
						{	/* SawBbv/bbv-range.scm 436 */
							BgL_test2910z00_7714 = ((bool_t) 0);
						}
				}
				if (BgL_test2910z00_7714)
					{	/* SawBbv/bbv-range.scm 439 */
						obj_t BgL_ez00_2249;

						BgL_ez00_2249 =
							BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2(BgL_ctxz00_56,
							((BgL_rtl_regz00_bglt) BgL_iz00_55));
						if (CBOOL(BgL_ez00_2249))
							{	/* SawBbv/bbv-range.scm 445 */
								bool_t BgL_test2916z00_7741;

								if (
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
												((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2249)))->
										BgL_polarityz00))
									{
										obj_t BgL_l1517z00_2268;

										BgL_l1517z00_2268 =
											(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
													((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2249)))->
											BgL_typesz00);
									BgL_zc3z04anonymousza31894ze3z87_2269:
										if (NULLP(BgL_l1517z00_2268))
											{	/* SawBbv/bbv-range.scm 445 */
												BgL_test2916z00_7741 = ((bool_t) 0);
											}
										else
											{	/* SawBbv/bbv-range.scm 445 */
												bool_t BgL__ortest_1520z00_2271;

												BgL__ortest_1520z00_2271 =
													BGl_rangezd2typezf3z21zzsaw_bbvzd2rangezd2(CAR(
														((obj_t) BgL_l1517z00_2268)));
												if (BgL__ortest_1520z00_2271)
													{	/* SawBbv/bbv-range.scm 445 */
														BgL_test2916z00_7741 = BgL__ortest_1520z00_2271;
													}
												else
													{
														obj_t BgL_l1517z00_7751;

														BgL_l1517z00_7751 =
															CDR(((obj_t) BgL_l1517z00_2268));
														BgL_l1517z00_2268 = BgL_l1517z00_7751;
														goto BgL_zc3z04anonymousza31894ze3z87_2269;
													}
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 445 */
										BgL_test2916z00_7741 = ((bool_t) 0);
									}
								if (BgL_test2916z00_7741)
									{	/* SawBbv/bbv-range.scm 446 */
										bool_t BgL_test2920z00_7756;

										{	/* SawBbv/bbv-range.scm 446 */
											obj_t BgL_arg1893z00_2264;

											BgL_arg1893z00_2264 =
												(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
														((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2249)))->
												BgL_valuez00);
											{	/* SawBbv/bbv-range.scm 273 */
												obj_t BgL_classz00_4709;

												BgL_classz00_4709 =
													BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2;
												if (BGL_OBJECTP(BgL_arg1893z00_2264))
													{	/* SawBbv/bbv-range.scm 273 */
														BgL_objectz00_bglt BgL_arg1807z00_4711;

														BgL_arg1807z00_4711 =
															(BgL_objectz00_bglt) (BgL_arg1893z00_2264);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawBbv/bbv-range.scm 273 */
																long BgL_idxz00_4717;

																BgL_idxz00_4717 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_4711);
																BgL_test2920z00_7756 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_4717 + 1L)) ==
																	BgL_classz00_4709);
															}
														else
															{	/* SawBbv/bbv-range.scm 273 */
																bool_t BgL_res2561z00_4742;

																{	/* SawBbv/bbv-range.scm 273 */
																	obj_t BgL_oclassz00_4725;

																	{	/* SawBbv/bbv-range.scm 273 */
																		obj_t BgL_arg1815z00_4733;
																		long BgL_arg1816z00_4734;

																		BgL_arg1815z00_4733 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawBbv/bbv-range.scm 273 */
																			long BgL_arg1817z00_4735;

																			BgL_arg1817z00_4735 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_4711);
																			BgL_arg1816z00_4734 =
																				(BgL_arg1817z00_4735 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_4725 =
																			VECTOR_REF(BgL_arg1815z00_4733,
																			BgL_arg1816z00_4734);
																	}
																	{	/* SawBbv/bbv-range.scm 273 */
																		bool_t BgL__ortest_1115z00_4726;

																		BgL__ortest_1115z00_4726 =
																			(BgL_classz00_4709 == BgL_oclassz00_4725);
																		if (BgL__ortest_1115z00_4726)
																			{	/* SawBbv/bbv-range.scm 273 */
																				BgL_res2561z00_4742 =
																					BgL__ortest_1115z00_4726;
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 273 */
																				long BgL_odepthz00_4727;

																				{	/* SawBbv/bbv-range.scm 273 */
																					obj_t BgL_arg1804z00_4728;

																					BgL_arg1804z00_4728 =
																						(BgL_oclassz00_4725);
																					BgL_odepthz00_4727 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_4728);
																				}
																				if ((1L < BgL_odepthz00_4727))
																					{	/* SawBbv/bbv-range.scm 273 */
																						obj_t BgL_arg1802z00_4730;

																						{	/* SawBbv/bbv-range.scm 273 */
																							obj_t BgL_arg1803z00_4731;

																							BgL_arg1803z00_4731 =
																								(BgL_oclassz00_4725);
																							BgL_arg1802z00_4730 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_4731, 1L);
																						}
																						BgL_res2561z00_4742 =
																							(BgL_arg1802z00_4730 ==
																							BgL_classz00_4709);
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 273 */
																						BgL_res2561z00_4742 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2920z00_7756 = BgL_res2561z00_4742;
															}
													}
												else
													{	/* SawBbv/bbv-range.scm 273 */
														BgL_test2920z00_7756 = ((bool_t) 0);
													}
											}
										}
										if (BgL_test2920z00_7756)
											{	/* SawBbv/bbv-range.scm 446 */
												return
													(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
															((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_2249)))->
													BgL_valuez00);
											}
										else
											{	/* SawBbv/bbv-range.scm 446 */
												return BFALSE;
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 445 */
										return BFALSE;
									}
							}
						else
							{	/* SawBbv/bbv-range.scm 440 */
								return BFALSE;
							}
					}
				else
					{	/* SawBbv/bbv-range.scm 436 */
						if (BGl_rtl_inszd2movzf3z21zzsaw_bbvzd2typeszd2(
								((BgL_rtl_insz00_bglt) BgL_iz00_55)))
							{	/* SawBbv/bbv-range.scm 450 */
								obj_t BgL_arg1899z00_2276;

								{	/* SawBbv/bbv-range.scm 450 */
									obj_t BgL_pairz00_4744;

									BgL_pairz00_4744 =
										(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_iz00_55)))->BgL_argsz00);
									BgL_arg1899z00_2276 = CAR(BgL_pairz00_4744);
								}
								{
									obj_t BgL_iz00_7789;

									BgL_iz00_7789 = BgL_arg1899z00_2276;
									BgL_iz00_55 = BgL_iz00_7789;
									goto BGl_rtlzd2rangezd2zzsaw_bbvzd2rangezd2;
								}
							}
						else
							{	/* SawBbv/bbv-range.scm 448 */
								if (BGl_rtl_inszd2strlenzf3z21zzsaw_bbvzd2typeszd2(
										((BgL_rtl_insz00_bglt) BgL_iz00_55)))
									{	/* SawBbv/bbv-range.scm 456 */
										bool_t BgL_test2927z00_7793;

										{	/* SawBbv/bbv-range.scm 456 */
											obj_t BgL_arg1913z00_2290;

											{	/* SawBbv/bbv-range.scm 456 */
												obj_t BgL_pairz00_4745;

												BgL_pairz00_4745 =
													(((BgL_rtl_insz00_bglt) COBJECT(
															((BgL_rtl_insz00_bglt) BgL_iz00_55)))->
													BgL_argsz00);
												BgL_arg1913z00_2290 = CAR(BgL_pairz00_4745);
											}
											{	/* SawBbv/bbv-range.scm 456 */
												obj_t BgL_classz00_4746;

												BgL_classz00_4746 = BGl_rtl_regz00zzsaw_defsz00;
												if (BGL_OBJECTP(BgL_arg1913z00_2290))
													{	/* SawBbv/bbv-range.scm 456 */
														BgL_objectz00_bglt BgL_arg1807z00_4748;

														BgL_arg1807z00_4748 =
															(BgL_objectz00_bglt) (BgL_arg1913z00_2290);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawBbv/bbv-range.scm 456 */
																long BgL_idxz00_4754;

																BgL_idxz00_4754 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_4748);
																BgL_test2927z00_7793 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_4754 + 1L)) ==
																	BgL_classz00_4746);
															}
														else
															{	/* SawBbv/bbv-range.scm 456 */
																bool_t BgL_res2562z00_4779;

																{	/* SawBbv/bbv-range.scm 456 */
																	obj_t BgL_oclassz00_4762;

																	{	/* SawBbv/bbv-range.scm 456 */
																		obj_t BgL_arg1815z00_4770;
																		long BgL_arg1816z00_4771;

																		BgL_arg1815z00_4770 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawBbv/bbv-range.scm 456 */
																			long BgL_arg1817z00_4772;

																			BgL_arg1817z00_4772 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_4748);
																			BgL_arg1816z00_4771 =
																				(BgL_arg1817z00_4772 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_4762 =
																			VECTOR_REF(BgL_arg1815z00_4770,
																			BgL_arg1816z00_4771);
																	}
																	{	/* SawBbv/bbv-range.scm 456 */
																		bool_t BgL__ortest_1115z00_4763;

																		BgL__ortest_1115z00_4763 =
																			(BgL_classz00_4746 == BgL_oclassz00_4762);
																		if (BgL__ortest_1115z00_4763)
																			{	/* SawBbv/bbv-range.scm 456 */
																				BgL_res2562z00_4779 =
																					BgL__ortest_1115z00_4763;
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 456 */
																				long BgL_odepthz00_4764;

																				{	/* SawBbv/bbv-range.scm 456 */
																					obj_t BgL_arg1804z00_4765;

																					BgL_arg1804z00_4765 =
																						(BgL_oclassz00_4762);
																					BgL_odepthz00_4764 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_4765);
																				}
																				if ((1L < BgL_odepthz00_4764))
																					{	/* SawBbv/bbv-range.scm 456 */
																						obj_t BgL_arg1802z00_4767;

																						{	/* SawBbv/bbv-range.scm 456 */
																							obj_t BgL_arg1803z00_4768;

																							BgL_arg1803z00_4768 =
																								(BgL_oclassz00_4762);
																							BgL_arg1802z00_4767 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_4768, 1L);
																						}
																						BgL_res2562z00_4779 =
																							(BgL_arg1802z00_4767 ==
																							BgL_classz00_4746);
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 456 */
																						BgL_res2562z00_4779 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2927z00_7793 = BgL_res2562z00_4779;
															}
													}
												else
													{	/* SawBbv/bbv-range.scm 456 */
														BgL_test2927z00_7793 = ((bool_t) 0);
													}
											}
										}
										if (BgL_test2927z00_7793)
											{	/* SawBbv/bbv-range.scm 458 */
												obj_t BgL_g1225z00_2283;

												{	/* SawBbv/bbv-range.scm 458 */
													obj_t BgL_arg1911z00_2288;

													{	/* SawBbv/bbv-range.scm 458 */
														obj_t BgL_pairz00_4780;

														BgL_pairz00_4780 =
															(((BgL_rtl_insz00_bglt) COBJECT(
																	((BgL_rtl_insz00_bglt) BgL_iz00_55)))->
															BgL_argsz00);
														BgL_arg1911z00_2288 = CAR(BgL_pairz00_4780);
													}
													BgL_g1225z00_2283 =
														BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2
														(BgL_ctxz00_56,
														((BgL_rtl_regz00_bglt) BgL_arg1911z00_2288));
												}
												if (CBOOL(BgL_g1225z00_2283))
													{	/* SawBbv/bbv-range.scm 461 */
														obj_t BgL_arg1906z00_2286;

														{	/* SawBbv/bbv-range.scm 461 */
															obj_t BgL_pairz00_4781;

															BgL_pairz00_4781 =
																(((BgL_rtl_insz00_bglt) COBJECT(
																		((BgL_rtl_insz00_bglt) BgL_iz00_55)))->
																BgL_argsz00);
															BgL_arg1906z00_2286 = CAR(BgL_pairz00_4781);
														}
														return
															((obj_t)
															BGl_vlenzd2ze3rangez31zzsaw_bbvzd2rangezd2
															(BgL_arg1906z00_2286, BgL_ctxz00_56));
													}
												else
													{	/* SawBbv/bbv-range.scm 463 */
														BgL_bbvzd2rangezd2_bglt BgL_res2563z00_4782;

														BgL_res2563z00_4782 =
															BGl_za2vlenzd2rangeza2zd2zzsaw_bbvzd2rangezd2;
														return ((obj_t) BgL_res2563z00_4782);
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 457 */
												BgL_bbvzd2rangezd2_bglt BgL_res2564z00_4783;

												BgL_res2564z00_4783 =
													BGl_za2vlenzd2rangeza2zd2zzsaw_bbvzd2rangezd2;
												return ((obj_t) BgL_res2564z00_4783);
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 451 */
										if (BGl_rtl_inszd2vlenzf3z21zzsaw_bbvzd2typeszd2(
												((BgL_rtl_insz00_bglt) BgL_iz00_55)))
											{	/* SawBbv/bbv-range.scm 468 */
												bool_t BgL_test2934z00_7836;

												{	/* SawBbv/bbv-range.scm 468 */
													obj_t BgL_arg1925z00_2304;

													{	/* SawBbv/bbv-range.scm 468 */
														obj_t BgL_pairz00_4784;

														BgL_pairz00_4784 =
															(((BgL_rtl_insz00_bglt) COBJECT(
																	((BgL_rtl_insz00_bglt) BgL_iz00_55)))->
															BgL_argsz00);
														BgL_arg1925z00_2304 = CAR(BgL_pairz00_4784);
													}
													{	/* SawBbv/bbv-range.scm 468 */
														obj_t BgL_classz00_4785;

														BgL_classz00_4785 = BGl_rtl_regz00zzsaw_defsz00;
														if (BGL_OBJECTP(BgL_arg1925z00_2304))
															{	/* SawBbv/bbv-range.scm 468 */
																BgL_objectz00_bglt BgL_arg1807z00_4787;

																BgL_arg1807z00_4787 =
																	(BgL_objectz00_bglt) (BgL_arg1925z00_2304);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* SawBbv/bbv-range.scm 468 */
																		long BgL_idxz00_4793;

																		BgL_idxz00_4793 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_4787);
																		BgL_test2934z00_7836 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_4793 + 1L)) ==
																			BgL_classz00_4785);
																	}
																else
																	{	/* SawBbv/bbv-range.scm 468 */
																		bool_t BgL_res2565z00_4818;

																		{	/* SawBbv/bbv-range.scm 468 */
																			obj_t BgL_oclassz00_4801;

																			{	/* SawBbv/bbv-range.scm 468 */
																				obj_t BgL_arg1815z00_4809;
																				long BgL_arg1816z00_4810;

																				BgL_arg1815z00_4809 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* SawBbv/bbv-range.scm 468 */
																					long BgL_arg1817z00_4811;

																					BgL_arg1817z00_4811 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_4787);
																					BgL_arg1816z00_4810 =
																						(BgL_arg1817z00_4811 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_4801 =
																					VECTOR_REF(BgL_arg1815z00_4809,
																					BgL_arg1816z00_4810);
																			}
																			{	/* SawBbv/bbv-range.scm 468 */
																				bool_t BgL__ortest_1115z00_4802;

																				BgL__ortest_1115z00_4802 =
																					(BgL_classz00_4785 ==
																					BgL_oclassz00_4801);
																				if (BgL__ortest_1115z00_4802)
																					{	/* SawBbv/bbv-range.scm 468 */
																						BgL_res2565z00_4818 =
																							BgL__ortest_1115z00_4802;
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 468 */
																						long BgL_odepthz00_4803;

																						{	/* SawBbv/bbv-range.scm 468 */
																							obj_t BgL_arg1804z00_4804;

																							BgL_arg1804z00_4804 =
																								(BgL_oclassz00_4801);
																							BgL_odepthz00_4803 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_4804);
																						}
																						if ((1L < BgL_odepthz00_4803))
																							{	/* SawBbv/bbv-range.scm 468 */
																								obj_t BgL_arg1802z00_4806;

																								{	/* SawBbv/bbv-range.scm 468 */
																									obj_t BgL_arg1803z00_4807;

																									BgL_arg1803z00_4807 =
																										(BgL_oclassz00_4801);
																									BgL_arg1802z00_4806 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_4807, 1L);
																								}
																								BgL_res2565z00_4818 =
																									(BgL_arg1802z00_4806 ==
																									BgL_classz00_4785);
																							}
																						else
																							{	/* SawBbv/bbv-range.scm 468 */
																								BgL_res2565z00_4818 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2934z00_7836 = BgL_res2565z00_4818;
																	}
															}
														else
															{	/* SawBbv/bbv-range.scm 468 */
																BgL_test2934z00_7836 = ((bool_t) 0);
															}
													}
												}
												if (BgL_test2934z00_7836)
													{	/* SawBbv/bbv-range.scm 470 */
														obj_t BgL_g1229z00_2297;

														{	/* SawBbv/bbv-range.scm 470 */
															obj_t BgL_arg1923z00_2302;

															{	/* SawBbv/bbv-range.scm 470 */
																obj_t BgL_pairz00_4819;

																BgL_pairz00_4819 =
																	(((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt) BgL_iz00_55)))->
																	BgL_argsz00);
																BgL_arg1923z00_2302 = CAR(BgL_pairz00_4819);
															}
															BgL_g1229z00_2297 =
																BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2
																(BgL_ctxz00_56,
																((BgL_rtl_regz00_bglt) BgL_arg1923z00_2302));
														}
														if (CBOOL(BgL_g1229z00_2297))
															{	/* SawBbv/bbv-range.scm 473 */
																obj_t BgL_arg1919z00_2300;

																{	/* SawBbv/bbv-range.scm 473 */
																	obj_t BgL_pairz00_4820;

																	BgL_pairz00_4820 =
																		(((BgL_rtl_insz00_bglt) COBJECT(
																				((BgL_rtl_insz00_bglt) BgL_iz00_55)))->
																		BgL_argsz00);
																	BgL_arg1919z00_2300 = CAR(BgL_pairz00_4820);
																}
																return
																	((obj_t)
																	BGl_vlenzd2ze3rangez31zzsaw_bbvzd2rangezd2
																	(BgL_arg1919z00_2300, BgL_ctxz00_56));
															}
														else
															{	/* SawBbv/bbv-range.scm 475 */
																BgL_bbvzd2rangezd2_bglt BgL_res2566z00_4821;

																BgL_res2566z00_4821 =
																	BGl_za2vlenzd2rangeza2zd2zzsaw_bbvzd2rangezd2;
																return ((obj_t) BgL_res2566z00_4821);
															}
													}
												else
													{	/* SawBbv/bbv-range.scm 469 */
														BgL_bbvzd2rangezd2_bglt BgL_res2567z00_4822;

														BgL_res2567z00_4822 =
															BGl_za2vlenzd2rangeza2zd2zzsaw_bbvzd2rangezd2;
														return ((obj_t) BgL_res2567z00_4822);
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 464 */
												if (BGl_rtl_inszd2callzf3z21zzsaw_bbvzd2typeszd2(
														((BgL_rtl_insz00_bglt) BgL_iz00_55)))
													{	/* SawBbv/bbv-range.scm 479 */
														BgL_rtl_callz00_bglt BgL_i1232z00_2308;

														BgL_i1232z00_2308 =
															((BgL_rtl_callz00_bglt)
															(((BgL_rtl_insz00_bglt) COBJECT(
																		((BgL_rtl_insz00_bglt) BgL_iz00_55)))->
																BgL_funz00));
														{	/* SawBbv/bbv-range.scm 481 */
															bool_t BgL_test2941z00_7882;

															{	/* SawBbv/bbv-range.scm 481 */
																BgL_globalz00_bglt BgL_arg1942z00_2323;

																BgL_arg1942z00_2323 =
																	(((BgL_rtl_callz00_bglt)
																		COBJECT(BgL_i1232z00_2308))->BgL_varz00);
																BgL_test2941z00_7882 =
																	(((obj_t) BgL_arg1942z00_2323) ==
																	BGl_za2intzd2ze3longza2z31zzsaw_bbvzd2cachezd2);
															}
															if (BgL_test2941z00_7882)
																{	/* SawBbv/bbv-range.scm 482 */
																	obj_t BgL_arg1930z00_2311;

																	{	/* SawBbv/bbv-range.scm 482 */
																		obj_t BgL_pairz00_4824;

																		BgL_pairz00_4824 =
																			(((BgL_rtl_insz00_bglt) COBJECT(
																					((BgL_rtl_insz00_bglt)
																						BgL_iz00_55)))->BgL_argsz00);
																		BgL_arg1930z00_2311 = CAR(BgL_pairz00_4824);
																	}
																	{
																		obj_t BgL_iz00_7889;

																		BgL_iz00_7889 = BgL_arg1930z00_2311;
																		BgL_iz00_55 = BgL_iz00_7889;
																		goto BGl_rtlzd2rangezd2zzsaw_bbvzd2rangezd2;
																	}
																}
															else
																{	/* SawBbv/bbv-range.scm 483 */
																	bool_t BgL_test2942z00_7890;

																	{	/* SawBbv/bbv-range.scm 483 */
																		BgL_globalz00_bglt BgL_arg1941z00_2322;

																		BgL_arg1941z00_2322 =
																			(((BgL_rtl_callz00_bglt)
																				COBJECT(BgL_i1232z00_2308))->
																			BgL_varz00);
																		BgL_test2942z00_7890 =
																			(((obj_t) BgL_arg1941z00_2322) ==
																			BGl_za2longzd2ze3bintza2z31zzsaw_bbvzd2cachezd2);
																	}
																	if (BgL_test2942z00_7890)
																		{	/* SawBbv/bbv-range.scm 484 */
																			obj_t BgL_arg1934z00_2315;

																			{	/* SawBbv/bbv-range.scm 484 */
																				obj_t BgL_pairz00_4826;

																				BgL_pairz00_4826 =
																					(((BgL_rtl_insz00_bglt) COBJECT(
																							((BgL_rtl_insz00_bglt)
																								BgL_iz00_55)))->BgL_argsz00);
																				BgL_arg1934z00_2315 =
																					CAR(BgL_pairz00_4826);
																			}
																			{
																				obj_t BgL_iz00_7897;

																				BgL_iz00_7897 = BgL_arg1934z00_2315;
																				BgL_iz00_55 = BgL_iz00_7897;
																				goto
																					BGl_rtlzd2rangezd2zzsaw_bbvzd2rangezd2;
																			}
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 485 */
																			bool_t BgL_test2943z00_7898;

																			{	/* SawBbv/bbv-range.scm 485 */
																				BgL_globalz00_bglt BgL_arg1940z00_2321;

																				BgL_arg1940z00_2321 =
																					(((BgL_rtl_callz00_bglt)
																						COBJECT(BgL_i1232z00_2308))->
																					BgL_varz00);
																				BgL_test2943z00_7898 =
																					(((obj_t) BgL_arg1940z00_2321) ==
																					BGl_za2bintzd2ze3longza2z31zzsaw_bbvzd2cachezd2);
																			}
																			if (BgL_test2943z00_7898)
																				{	/* SawBbv/bbv-range.scm 486 */
																					obj_t BgL_arg1938z00_2319;

																					{	/* SawBbv/bbv-range.scm 486 */
																						obj_t BgL_pairz00_4828;

																						BgL_pairz00_4828 =
																							(((BgL_rtl_insz00_bglt) COBJECT(
																									((BgL_rtl_insz00_bglt)
																										BgL_iz00_55)))->
																							BgL_argsz00);
																						BgL_arg1938z00_2319 =
																							CAR(BgL_pairz00_4828);
																					}
																					{
																						obj_t BgL_iz00_7905;

																						BgL_iz00_7905 = BgL_arg1938z00_2319;
																						BgL_iz00_55 = BgL_iz00_7905;
																						goto
																							BGl_rtlzd2rangezd2zzsaw_bbvzd2rangezd2;
																					}
																				}
																			else
																				{	/* SawBbv/bbv-range.scm 485 */
																					return BFALSE;
																				}
																		}
																}
														}
													}
												else
													{	/* SawBbv/bbv-range.scm 476 */
														if (BGl_rtl_inszd2loadizf3z21zzsaw_bbvzd2typeszd2(
																((BgL_rtl_insz00_bglt) BgL_iz00_55)))
															{	/* SawBbv/bbv-range.scm 492 */
																BgL_rtl_loadiz00_bglt BgL_i1234z00_2326;

																BgL_i1234z00_2326 =
																	((BgL_rtl_loadiz00_bglt)
																	(((BgL_rtl_insz00_bglt) COBJECT(
																				((BgL_rtl_insz00_bglt) BgL_iz00_55)))->
																		BgL_funz00));
																{	/* SawBbv/bbv-range.scm 493 */
																	BgL_atomz00_bglt BgL_i1235z00_2327;

																	BgL_i1235z00_2327 =
																		(((BgL_rtl_loadiz00_bglt)
																			COBJECT(BgL_i1234z00_2326))->
																		BgL_constantz00);
																	if (INTEGERP((((BgL_atomz00_bglt)
																					COBJECT(BgL_i1235z00_2327))->
																				BgL_valuez00)))
																		{	/* SawBbv/bbv-range.scm 495 */
																			obj_t BgL_arg1946z00_2330;

																			BgL_arg1946z00_2330 =
																				(((BgL_atomz00_bglt)
																					COBJECT(BgL_i1235z00_2327))->
																				BgL_valuez00);
																			return ((obj_t)
																				BGl_fixnumzd2ze3rangez31zzsaw_bbvzd2rangezd2
																				((long) CINT(BgL_arg1946z00_2330)));
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 494 */
																			return BFALSE;
																		}
																}
															}
														else
															{	/* SawBbv/bbv-range.scm 489 */
																return BFALSE;
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* &rtl-range */
	obj_t BGl_z62rtlzd2rangezb0zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6255,
		obj_t BgL_iz00_6256, obj_t BgL_ctxz00_6257)
	{
		{	/* SawBbv/bbv-range.scm 431 */
			return
				BGl_rtlzd2rangezd2zzsaw_bbvzd2rangezd2(BgL_iz00_6256,
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctxz00_6257));
		}

	}



/* bbv-singleton? */
	BGL_EXPORTED_DEF bool_t BGl_bbvzd2singletonzf3z21zzsaw_bbvzd2rangezd2(obj_t
		BgL_rngz00_57)
	{
		{	/* SawBbv/bbv-range.scm 502 */
			{	/* SawBbv/bbv-range.scm 503 */
				bool_t BgL_test2946z00_7922;

				{	/* SawBbv/bbv-range.scm 503 */
					obj_t BgL_classz00_4829;

					BgL_classz00_4829 = BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2;
					if (BGL_OBJECTP(BgL_rngz00_57))
						{	/* SawBbv/bbv-range.scm 503 */
							BgL_objectz00_bglt BgL_arg1807z00_4831;

							BgL_arg1807z00_4831 = (BgL_objectz00_bglt) (BgL_rngz00_57);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-range.scm 503 */
									long BgL_idxz00_4837;

									BgL_idxz00_4837 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4831);
									BgL_test2946z00_7922 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4837 + 1L)) == BgL_classz00_4829);
								}
							else
								{	/* SawBbv/bbv-range.scm 503 */
									bool_t BgL_res2568z00_4862;

									{	/* SawBbv/bbv-range.scm 503 */
										obj_t BgL_oclassz00_4845;

										{	/* SawBbv/bbv-range.scm 503 */
											obj_t BgL_arg1815z00_4853;
											long BgL_arg1816z00_4854;

											BgL_arg1815z00_4853 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-range.scm 503 */
												long BgL_arg1817z00_4855;

												BgL_arg1817z00_4855 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4831);
												BgL_arg1816z00_4854 =
													(BgL_arg1817z00_4855 - OBJECT_TYPE);
											}
											BgL_oclassz00_4845 =
												VECTOR_REF(BgL_arg1815z00_4853, BgL_arg1816z00_4854);
										}
										{	/* SawBbv/bbv-range.scm 503 */
											bool_t BgL__ortest_1115z00_4846;

											BgL__ortest_1115z00_4846 =
												(BgL_classz00_4829 == BgL_oclassz00_4845);
											if (BgL__ortest_1115z00_4846)
												{	/* SawBbv/bbv-range.scm 503 */
													BgL_res2568z00_4862 = BgL__ortest_1115z00_4846;
												}
											else
												{	/* SawBbv/bbv-range.scm 503 */
													long BgL_odepthz00_4847;

													{	/* SawBbv/bbv-range.scm 503 */
														obj_t BgL_arg1804z00_4848;

														BgL_arg1804z00_4848 = (BgL_oclassz00_4845);
														BgL_odepthz00_4847 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4848);
													}
													if ((1L < BgL_odepthz00_4847))
														{	/* SawBbv/bbv-range.scm 503 */
															obj_t BgL_arg1802z00_4850;

															{	/* SawBbv/bbv-range.scm 503 */
																obj_t BgL_arg1803z00_4851;

																BgL_arg1803z00_4851 = (BgL_oclassz00_4845);
																BgL_arg1802z00_4850 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4851,
																	1L);
															}
															BgL_res2568z00_4862 =
																(BgL_arg1802z00_4850 == BgL_classz00_4829);
														}
													else
														{	/* SawBbv/bbv-range.scm 503 */
															BgL_res2568z00_4862 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2946z00_7922 = BgL_res2568z00_4862;
								}
						}
					else
						{	/* SawBbv/bbv-range.scm 503 */
							BgL_test2946z00_7922 = ((bool_t) 0);
						}
				}
				if (BgL_test2946z00_7922)
					{	/* SawBbv/bbv-range.scm 504 */
						bool_t BgL_test2951z00_7945;

						{	/* SawBbv/bbv-range.scm 504 */
							obj_t BgL_arg1954z00_2345;

							{	/* SawBbv/bbv-range.scm 504 */
								obj_t BgL_arg1955z00_2346;
								obj_t BgL_arg1956z00_2347;

								BgL_arg1955z00_2346 =
									(((BgL_bbvzd2rangezd2_bglt) COBJECT(
											((BgL_bbvzd2rangezd2_bglt) BgL_rngz00_57)))->BgL_upz00);
								BgL_arg1956z00_2347 =
									(((BgL_bbvzd2rangezd2_bglt) COBJECT(
											((BgL_bbvzd2rangezd2_bglt) BgL_rngz00_57)))->BgL_loz00);
								BgL_arg1954z00_2345 =
									BGl_zd3rvzd3zzsaw_bbvzd2rangezd2(BgL_arg1955z00_2346,
									BgL_arg1956z00_2347);
							}
							BgL_test2951z00_7945 = (BgL_arg1954z00_2345 == BTRUE);
						}
						if (BgL_test2951z00_7945)
							{	/* SawBbv/bbv-range.scm 505 */
								obj_t BgL__andtest_1238z00_2334;

								{	/* SawBbv/bbv-range.scm 505 */
									obj_t BgL_arg1951z00_2340;
									obj_t BgL_arg1952z00_2341;

									BgL_arg1951z00_2340 =
										(((BgL_bbvzd2rangezd2_bglt) COBJECT(
												((BgL_bbvzd2rangezd2_bglt) BgL_rngz00_57)))->BgL_upz00);
									{	/* SawBbv/bbv-range.scm 505 */
										long BgL_a1521z00_2342;

										{	/* SawBbv/bbv-range.scm 245 */
											long BgL_arg1820z00_4866;

											{	/* SawBbv/bbv-range.scm 245 */
												long BgL_arg1822z00_4867;

												{	/* SawBbv/bbv-range.scm 243 */
													long BgL_arg1808z00_4868;

													{	/* SawBbv/bbv-range.scm 243 */
														obj_t BgL_arg1812z00_4869;

														BgL_arg1812z00_4869 =
															BGl_bigloozd2configzd2zz__configurez00
															(CNST_TABLE_REF(0));
														BgL_arg1808z00_4868 =
															((long) CINT(BgL_arg1812z00_4869) - 2L);
													}
													BgL_arg1822z00_4867 =
														(1L << (int) (BgL_arg1808z00_4868));
												}
												BgL_arg1820z00_4866 = NEG(BgL_arg1822z00_4867);
											}
											BgL_a1521z00_2342 = (BgL_arg1820z00_4866 - 1L);
										}
										{	/* SawBbv/bbv-range.scm 505 */

											{	/* SawBbv/bbv-range.scm 505 */
												obj_t BgL_za71za7_4874;
												obj_t BgL_za72za7_4875;

												BgL_za71za7_4874 = BINT(BgL_a1521z00_2342);
												BgL_za72za7_4875 = BINT(1L);
												{	/* SawBbv/bbv-range.scm 505 */
													obj_t BgL_tmpz00_4876;

													BgL_tmpz00_4876 = BINT(0L);
													if (BGL_ADDFX_OV(BgL_za71za7_4874, BgL_za72za7_4875,
															BgL_tmpz00_4876))
														{	/* SawBbv/bbv-range.scm 505 */
															BgL_arg1952z00_2341 =
																bgl_bignum_add(bgl_long_to_bignum(
																	(long) CINT(BgL_za71za7_4874)),
																bgl_long_to_bignum(
																	(long) CINT(BgL_za72za7_4875)));
														}
													else
														{	/* SawBbv/bbv-range.scm 505 */
															BgL_arg1952z00_2341 = BgL_tmpz00_4876;
														}
												}
											}
										}
									}
									BgL__andtest_1238z00_2334 =
										BGl_ze3rvze3zzsaw_bbvzd2rangezd2(BgL_arg1951z00_2340,
										BgL_arg1952z00_2341);
								}
								if (CBOOL(BgL__andtest_1238z00_2334))
									{	/* SawBbv/bbv-range.scm 506 */
										obj_t BgL_arg1948z00_2335;
										obj_t BgL_arg1949z00_2336;

										BgL_arg1948z00_2335 =
											(((BgL_bbvzd2rangezd2_bglt) COBJECT(
													((BgL_bbvzd2rangezd2_bglt) BgL_rngz00_57)))->
											BgL_upz00);
										{	/* SawBbv/bbv-range.scm 506 */
											long BgL_a1523z00_2337;

											{	/* SawBbv/bbv-range.scm 243 */
												long BgL_arg1808z00_4885;

												{	/* SawBbv/bbv-range.scm 243 */
													obj_t BgL_arg1812z00_4886;

													BgL_arg1812z00_4886 =
														BGl_bigloozd2configzd2zz__configurez00
														(CNST_TABLE_REF(0));
													BgL_arg1808z00_4885 =
														((long) CINT(BgL_arg1812z00_4886) - 2L);
												}
												BgL_a1523z00_2337 = (1L << (int) (BgL_arg1808z00_4885));
											}
											{	/* SawBbv/bbv-range.scm 506 */

												{	/* SawBbv/bbv-range.scm 506 */
													obj_t BgL_za71za7_4889;
													obj_t BgL_za72za7_4890;

													BgL_za71za7_4889 = BINT(BgL_a1523z00_2337);
													BgL_za72za7_4890 = BINT(1L);
													{	/* SawBbv/bbv-range.scm 506 */
														obj_t BgL_tmpz00_4891;

														BgL_tmpz00_4891 = BINT(0L);
														if (BGL_SUBFX_OV(BgL_za71za7_4889, BgL_za72za7_4890,
																BgL_tmpz00_4891))
															{	/* SawBbv/bbv-range.scm 506 */
																BgL_arg1949z00_2336 =
																	bgl_bignum_sub(bgl_long_to_bignum(
																		(long) CINT(BgL_za71za7_4889)),
																	bgl_long_to_bignum(
																		(long) CINT(BgL_za72za7_4890)));
															}
														else
															{	/* SawBbv/bbv-range.scm 506 */
																BgL_arg1949z00_2336 = BgL_tmpz00_4891;
															}
													}
												}
											}
										}
										return
											CBOOL(BGl_ze3rvze3zzsaw_bbvzd2rangezd2
											(BgL_arg1949z00_2336, BgL_arg1948z00_2335));
									}
								else
									{	/* SawBbv/bbv-range.scm 505 */
										return ((bool_t) 0);
									}
							}
						else
							{	/* SawBbv/bbv-range.scm 504 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* SawBbv/bbv-range.scm 503 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bbv-singleton? */
	obj_t BGl_z62bbvzd2singletonzf3z43zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6258,
		obj_t BgL_rngz00_6259)
	{
		{	/* SawBbv/bbv-range.scm 502 */
			return
				BBOOL(BGl_bbvzd2singletonzf3z21zzsaw_bbvzd2rangezd2(BgL_rngz00_6259));
		}

	}



/* bbv-range< */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2rangezc3z11zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_xz00_59, BgL_bbvzd2rangezd2_bglt BgL_yz00_60)
	{
		{	/* SawBbv/bbv-range.scm 517 */
			{
				BgL_bbvzd2rangezd2_bglt BgL_xz00_2350;
				BgL_bbvzd2rangezd2_bglt BgL_yz00_2351;

				BgL_xz00_2350 = BgL_xz00_59;
				BgL_yz00_2351 = BgL_yz00_60;
				if (BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_xz00_2350))
					{	/* SawBbv/bbv-range.scm 521 */
						if (BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_yz00_2351))
							{	/* SawBbv/bbv-range.scm 521 */
								return BUNSPEC;
							}
						else
							{	/* SawBbv/bbv-range.scm 521 */
								return BTRUE;
							}
					}
				else
					{	/* SawBbv/bbv-range.scm 521 */
						if (BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_yz00_2351))
							{	/* SawBbv/bbv-range.scm 522 */
								return BTRUE;
							}
						else
							{	/* SawBbv/bbv-range.scm 523 */
								bool_t BgL_test2958z00_8003;

								{	/* SawBbv/bbv-range.scm 523 */
									obj_t BgL_arg1991z00_2385;
									obj_t BgL_arg1992z00_2386;

									BgL_arg1991z00_2385 =
										(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_2350))->
										BgL_upz00);
									BgL_arg1992z00_2386 =
										(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_2351))->
										BgL_upz00);
									{	/* SawBbv/bbv-range.scm 94 */
										bool_t BgL_test2959z00_8006;

										{	/* SawBbv/bbv-range.scm 85 */
											obj_t BgL_classz00_4909;

											BgL_classz00_4909 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
											if (BGL_OBJECTP(BgL_arg1991z00_2385))
												{	/* SawBbv/bbv-range.scm 85 */
													BgL_objectz00_bglt BgL_arg1807z00_4911;

													BgL_arg1807z00_4911 =
														(BgL_objectz00_bglt) (BgL_arg1991z00_2385);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* SawBbv/bbv-range.scm 85 */
															long BgL_idxz00_4917;

															BgL_idxz00_4917 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4911);
															BgL_test2959z00_8006 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_4917 + 1L)) == BgL_classz00_4909);
														}
													else
														{	/* SawBbv/bbv-range.scm 85 */
															bool_t BgL_res2569z00_4942;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_oclassz00_4925;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1815z00_4933;
																	long BgL_arg1816z00_4934;

																	BgL_arg1815z00_4933 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* SawBbv/bbv-range.scm 85 */
																		long BgL_arg1817z00_4935;

																		BgL_arg1817z00_4935 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4911);
																		BgL_arg1816z00_4934 =
																			(BgL_arg1817z00_4935 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_4925 =
																		VECTOR_REF(BgL_arg1815z00_4933,
																		BgL_arg1816z00_4934);
																}
																{	/* SawBbv/bbv-range.scm 85 */
																	bool_t BgL__ortest_1115z00_4926;

																	BgL__ortest_1115z00_4926 =
																		(BgL_classz00_4909 == BgL_oclassz00_4925);
																	if (BgL__ortest_1115z00_4926)
																		{	/* SawBbv/bbv-range.scm 85 */
																			BgL_res2569z00_4942 =
																				BgL__ortest_1115z00_4926;
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_odepthz00_4927;

																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1804z00_4928;

																				BgL_arg1804z00_4928 =
																					(BgL_oclassz00_4925);
																				BgL_odepthz00_4927 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_4928);
																			}
																			if ((1L < BgL_odepthz00_4927))
																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1802z00_4930;

																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1803z00_4931;

																						BgL_arg1803z00_4931 =
																							(BgL_oclassz00_4925);
																						BgL_arg1802z00_4930 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_4931, 1L);
																					}
																					BgL_res2569z00_4942 =
																						(BgL_arg1802z00_4930 ==
																						BgL_classz00_4909);
																				}
																			else
																				{	/* SawBbv/bbv-range.scm 85 */
																					BgL_res2569z00_4942 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2959z00_8006 = BgL_res2569z00_4942;
														}
												}
											else
												{	/* SawBbv/bbv-range.scm 85 */
													BgL_test2959z00_8006 = ((bool_t) 0);
												}
										}
										if (BgL_test2959z00_8006)
											{	/* SawBbv/bbv-range.scm 94 */
												bool_t BgL_test2964z00_8029;

												{	/* SawBbv/bbv-range.scm 85 */
													obj_t BgL_classz00_4943;

													BgL_classz00_4943 =
														BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
													if (BGL_OBJECTP(BgL_arg1992z00_2386))
														{	/* SawBbv/bbv-range.scm 85 */
															BgL_objectz00_bglt BgL_arg1807z00_4945;

															BgL_arg1807z00_4945 =
																(BgL_objectz00_bglt) (BgL_arg1992z00_2386);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_idxz00_4951;

																	BgL_idxz00_4951 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_4945);
																	BgL_test2964z00_8029 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_4951 + 1L)) ==
																		BgL_classz00_4943);
																}
															else
																{	/* SawBbv/bbv-range.scm 85 */
																	bool_t BgL_res2570z00_4976;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_oclassz00_4959;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1815z00_4967;
																			long BgL_arg1816z00_4968;

																			BgL_arg1815z00_4967 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* SawBbv/bbv-range.scm 85 */
																				long BgL_arg1817z00_4969;

																				BgL_arg1817z00_4969 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_4945);
																				BgL_arg1816z00_4968 =
																					(BgL_arg1817z00_4969 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_4959 =
																				VECTOR_REF(BgL_arg1815z00_4967,
																				BgL_arg1816z00_4968);
																		}
																		{	/* SawBbv/bbv-range.scm 85 */
																			bool_t BgL__ortest_1115z00_4960;

																			BgL__ortest_1115z00_4960 =
																				(BgL_classz00_4943 ==
																				BgL_oclassz00_4959);
																			if (BgL__ortest_1115z00_4960)
																				{	/* SawBbv/bbv-range.scm 85 */
																					BgL_res2570z00_4976 =
																						BgL__ortest_1115z00_4960;
																				}
																			else
																				{	/* SawBbv/bbv-range.scm 85 */
																					long BgL_odepthz00_4961;

																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1804z00_4962;

																						BgL_arg1804z00_4962 =
																							(BgL_oclassz00_4959);
																						BgL_odepthz00_4961 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_4962);
																					}
																					if ((1L < BgL_odepthz00_4961))
																						{	/* SawBbv/bbv-range.scm 85 */
																							obj_t BgL_arg1802z00_4964;

																							{	/* SawBbv/bbv-range.scm 85 */
																								obj_t BgL_arg1803z00_4965;

																								BgL_arg1803z00_4965 =
																									(BgL_oclassz00_4959);
																								BgL_arg1802z00_4964 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_4965, 1L);
																							}
																							BgL_res2570z00_4976 =
																								(BgL_arg1802z00_4964 ==
																								BgL_classz00_4943);
																						}
																					else
																						{	/* SawBbv/bbv-range.scm 85 */
																							BgL_res2570z00_4976 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2964z00_8029 = BgL_res2570z00_4976;
																}
														}
													else
														{	/* SawBbv/bbv-range.scm 85 */
															BgL_test2964z00_8029 = ((bool_t) 0);
														}
												}
												if (BgL_test2964z00_8029)
													{	/* SawBbv/bbv-range.scm 94 */
														BgL_test2958z00_8003 =
															(
															(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																		((BgL_bbvzd2vlenzd2_bglt)
																			BgL_arg1991z00_2385)))->BgL_vecz00) ==
															(((BgL_bbvzd2vlenzd2_bglt)
																	COBJECT(((BgL_bbvzd2vlenzd2_bglt)
																			BgL_arg1992z00_2386)))->BgL_vecz00));
													}
												else
													{	/* SawBbv/bbv-range.scm 94 */
														BgL_test2958z00_8003 = ((bool_t) 0);
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 94 */
												BgL_test2958z00_8003 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2958z00_8003)
									{	/* SawBbv/bbv-range.scm 523 */
										if (
											((long) (
													(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																((BgL_bbvzd2vlenzd2_bglt)
																	(((BgL_bbvzd2rangezd2_bglt)
																			COBJECT(BgL_xz00_2350))->BgL_upz00))))->
														BgL_offsetz00)) <
												(long) ((((BgL_bbvzd2vlenzd2_bglt)
															COBJECT(((BgL_bbvzd2vlenzd2_bglt) ((
																			(BgL_bbvzd2rangezd2_bglt)
																			COBJECT(BgL_yz00_2351))->BgL_upz00))))->
														BgL_offsetz00))))
											{	/* SawBbv/bbv-range.scm 524 */
												if (
													(BGl_ze3zd3rvz30zzsaw_bbvzd2rangezd2(
															(((BgL_bbvzd2rangezd2_bglt)
																	COBJECT(BgL_yz00_2351))->BgL_loz00),
															(((BgL_bbvzd2rangezd2_bglt)
																	COBJECT(BgL_xz00_2350))->BgL_loz00)) ==
														BTRUE))
													{	/* SawBbv/bbv-range.scm 525 */
														return BTRUE;
													}
												else
													{	/* SawBbv/bbv-range.scm 525 */
														return BUNSPEC;
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 524 */
												return BUNSPEC;
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 523 */
										if (
											(BGl_ze3rvze3zzsaw_bbvzd2rangezd2(
													(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_2351))->
														BgL_loz00),
													(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_2350))->
														BgL_upz00)) == BTRUE))
											{	/* SawBbv/bbv-range.scm 529 */
												return BTRUE;
											}
										else
											{	/* SawBbv/bbv-range.scm 529 */
												if (
													(BGl_ze3zd3rvz30zzsaw_bbvzd2rangezd2(
															(((BgL_bbvzd2rangezd2_bglt)
																	COBJECT(BgL_xz00_2350))->BgL_loz00),
															(((BgL_bbvzd2rangezd2_bglt)
																	COBJECT(BgL_yz00_2351))->BgL_upz00)) ==
														BTRUE))
													{	/* SawBbv/bbv-range.scm 530 */
														return BFALSE;
													}
												else
													{	/* SawBbv/bbv-range.scm 530 */
														return BUNSPEC;
													}
											}
									}
							}
					}
			}
		}

	}



/* &bbv-range< */
	obj_t BGl_z62bbvzd2rangezc3z73zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6260,
		obj_t BgL_xz00_6261, obj_t BgL_yz00_6262)
	{
		{	/* SawBbv/bbv-range.scm 517 */
			return
				BGl_bbvzd2rangezc3z11zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6261),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6262));
		}

	}



/* bbv-range<= */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2rangezc3zd3zc2zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_xz00_61, BgL_bbvzd2rangezd2_bglt BgL_yz00_62)
	{
		{	/* SawBbv/bbv-range.scm 542 */
			if (BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_xz00_61))
				{	/* SawBbv/bbv-range.scm 544 */
					if (BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_yz00_62))
						{	/* SawBbv/bbv-range.scm 544 */
							return BUNSPEC;
						}
					else
						{	/* SawBbv/bbv-range.scm 544 */
							return BTRUE;
						}
				}
			else
				{	/* SawBbv/bbv-range.scm 544 */
					if (BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_yz00_62))
						{	/* SawBbv/bbv-range.scm 545 */
							return BTRUE;
						}
					else
						{	/* SawBbv/bbv-range.scm 546 */
							bool_t BgL_test2976z00_8091;

							{	/* SawBbv/bbv-range.scm 546 */
								obj_t BgL_arg2026z00_2420;
								obj_t BgL_arg2027z00_2421;

								BgL_arg2026z00_2420 =
									(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_61))->BgL_upz00);
								BgL_arg2027z00_2421 =
									(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_62))->BgL_upz00);
								{	/* SawBbv/bbv-range.scm 94 */
									bool_t BgL_test2977z00_8094;

									{	/* SawBbv/bbv-range.scm 85 */
										obj_t BgL_classz00_5003;

										BgL_classz00_5003 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
										if (BGL_OBJECTP(BgL_arg2026z00_2420))
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_objectz00_bglt BgL_arg1807z00_5005;

												BgL_arg1807z00_5005 =
													(BgL_objectz00_bglt) (BgL_arg2026z00_2420);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_idxz00_5011;

														BgL_idxz00_5011 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5005);
														BgL_test2977z00_8094 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_5011 + 1L)) == BgL_classz00_5003);
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL_res2571z00_5036;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_oclassz00_5019;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1815z00_5027;
																long BgL_arg1816z00_5028;

																BgL_arg1815z00_5027 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_arg1817z00_5029;

																	BgL_arg1817z00_5029 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5005);
																	BgL_arg1816z00_5028 =
																		(BgL_arg1817z00_5029 - OBJECT_TYPE);
																}
																BgL_oclassz00_5019 =
																	VECTOR_REF(BgL_arg1815z00_5027,
																	BgL_arg1816z00_5028);
															}
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL__ortest_1115z00_5020;

																BgL__ortest_1115z00_5020 =
																	(BgL_classz00_5003 == BgL_oclassz00_5019);
																if (BgL__ortest_1115z00_5020)
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2571z00_5036 =
																			BgL__ortest_1115z00_5020;
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		long BgL_odepthz00_5021;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1804z00_5022;

																			BgL_arg1804z00_5022 =
																				(BgL_oclassz00_5019);
																			BgL_odepthz00_5021 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_5022);
																		}
																		if ((1L < BgL_odepthz00_5021))
																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1802z00_5024;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1803z00_5025;

																					BgL_arg1803z00_5025 =
																						(BgL_oclassz00_5019);
																					BgL_arg1802z00_5024 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_5025, 1L);
																				}
																				BgL_res2571z00_5036 =
																					(BgL_arg1802z00_5024 ==
																					BgL_classz00_5003);
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2571z00_5036 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2977z00_8094 = BgL_res2571z00_5036;
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_test2977z00_8094 = ((bool_t) 0);
											}
									}
									if (BgL_test2977z00_8094)
										{	/* SawBbv/bbv-range.scm 94 */
											bool_t BgL_test2982z00_8117;

											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_classz00_5037;

												BgL_classz00_5037 =
													BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
												if (BGL_OBJECTP(BgL_arg2027z00_2421))
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_objectz00_bglt BgL_arg1807z00_5039;

														BgL_arg1807z00_5039 =
															(BgL_objectz00_bglt) (BgL_arg2027z00_2421);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_idxz00_5045;

																BgL_idxz00_5045 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_5039);
																BgL_test2982z00_8117 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_5045 + 1L)) ==
																	BgL_classz00_5037);
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL_res2572z00_5070;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_oclassz00_5053;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1815z00_5061;
																		long BgL_arg1816z00_5062;

																		BgL_arg1815z00_5061 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_arg1817z00_5063;

																			BgL_arg1817z00_5063 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_5039);
																			BgL_arg1816z00_5062 =
																				(BgL_arg1817z00_5063 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_5053 =
																			VECTOR_REF(BgL_arg1815z00_5061,
																			BgL_arg1816z00_5062);
																	}
																	{	/* SawBbv/bbv-range.scm 85 */
																		bool_t BgL__ortest_1115z00_5054;

																		BgL__ortest_1115z00_5054 =
																			(BgL_classz00_5037 == BgL_oclassz00_5053);
																		if (BgL__ortest_1115z00_5054)
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2572z00_5070 =
																					BgL__ortest_1115z00_5054;
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				long BgL_odepthz00_5055;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1804z00_5056;

																					BgL_arg1804z00_5056 =
																						(BgL_oclassz00_5053);
																					BgL_odepthz00_5055 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_5056);
																				}
																				if ((1L < BgL_odepthz00_5055))
																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1802z00_5058;

																						{	/* SawBbv/bbv-range.scm 85 */
																							obj_t BgL_arg1803z00_5059;

																							BgL_arg1803z00_5059 =
																								(BgL_oclassz00_5053);
																							BgL_arg1802z00_5058 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_5059, 1L);
																						}
																						BgL_res2572z00_5070 =
																							(BgL_arg1802z00_5058 ==
																							BgL_classz00_5037);
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 85 */
																						BgL_res2572z00_5070 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2982z00_8117 = BgL_res2572z00_5070;
															}
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_test2982z00_8117 = ((bool_t) 0);
													}
											}
											if (BgL_test2982z00_8117)
												{	/* SawBbv/bbv-range.scm 94 */
													BgL_test2976z00_8091 =
														(
														(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																	((BgL_bbvzd2vlenzd2_bglt)
																		BgL_arg2026z00_2420)))->BgL_vecz00) ==
														(((BgL_bbvzd2vlenzd2_bglt)
																COBJECT(((BgL_bbvzd2vlenzd2_bglt)
																		BgL_arg2027z00_2421)))->BgL_vecz00));
												}
											else
												{	/* SawBbv/bbv-range.scm 94 */
													BgL_test2976z00_8091 = ((bool_t) 0);
												}
										}
									else
										{	/* SawBbv/bbv-range.scm 94 */
											BgL_test2976z00_8091 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test2976z00_8091)
								{	/* SawBbv/bbv-range.scm 546 */
									if (
										((long) (
												(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
															((BgL_bbvzd2vlenzd2_bglt)
																(((BgL_bbvzd2rangezd2_bglt)
																		COBJECT(BgL_xz00_61))->BgL_upz00))))->
													BgL_offsetz00)) <=
											(long) ((((BgL_bbvzd2vlenzd2_bglt)
														COBJECT(((BgL_bbvzd2vlenzd2_bglt) ((
																		(BgL_bbvzd2rangezd2_bglt)
																		COBJECT(BgL_yz00_62))->BgL_upz00))))->
													BgL_offsetz00))))
										{	/* SawBbv/bbv-range.scm 547 */
											if (
												(BGl_ze3zd3rvz30zzsaw_bbvzd2rangezd2(
														(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_62))->
															BgL_loz00),
														(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_61))->
															BgL_loz00)) == BTRUE))
												{	/* SawBbv/bbv-range.scm 548 */
													return BTRUE;
												}
											else
												{	/* SawBbv/bbv-range.scm 548 */
													return BUNSPEC;
												}
										}
									else
										{	/* SawBbv/bbv-range.scm 547 */
											return BUNSPEC;
										}
								}
							else
								{	/* SawBbv/bbv-range.scm 546 */
									if (
										(BGl_ze3zd3rvz30zzsaw_bbvzd2rangezd2(
												(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_62))->
													BgL_loz00),
												(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_61))->
													BgL_upz00)) == BTRUE))
										{	/* SawBbv/bbv-range.scm 552 */
											return BTRUE;
										}
									else
										{	/* SawBbv/bbv-range.scm 552 */
											if (
												(BGl_ze3rvze3zzsaw_bbvzd2rangezd2(
														(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_61))->
															BgL_loz00),
														(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_62))->
															BgL_upz00)) == BTRUE))
												{	/* SawBbv/bbv-range.scm 553 */
													return BFALSE;
												}
											else
												{	/* SawBbv/bbv-range.scm 553 */
													return BUNSPEC;
												}
										}
								}
						}
				}
		}

	}



/* &bbv-range<= */
	obj_t BGl_z62bbvzd2rangezc3zd3za0zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6263,
		obj_t BgL_xz00_6264, obj_t BgL_yz00_6265)
	{
		{	/* SawBbv/bbv-range.scm 542 */
			return
				BGl_bbvzd2rangezc3zd3zc2zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6264),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6265));
		}

	}



/* bbv-range> */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2rangeze3z31zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_xz00_63, BgL_bbvzd2rangezd2_bglt BgL_yz00_64)
	{
		{	/* SawBbv/bbv-range.scm 559 */
			{
				BgL_bbvzd2rangezd2_bglt BgL_xz00_2423;
				BgL_bbvzd2rangezd2_bglt BgL_yz00_2424;

				BgL_xz00_2423 = BgL_xz00_63;
				BgL_yz00_2424 = BgL_yz00_64;
				if (BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_xz00_2423))
					{	/* SawBbv/bbv-range.scm 563 */
						if (BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_yz00_2424))
							{	/* SawBbv/bbv-range.scm 563 */
								return BUNSPEC;
							}
						else
							{	/* SawBbv/bbv-range.scm 563 */
								return BTRUE;
							}
					}
				else
					{	/* SawBbv/bbv-range.scm 563 */
						if (BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_yz00_2424))
							{	/* SawBbv/bbv-range.scm 564 */
								return BTRUE;
							}
						else
							{	/* SawBbv/bbv-range.scm 565 */
								bool_t BgL_test2994z00_8179;

								{	/* SawBbv/bbv-range.scm 565 */
									obj_t BgL_arg2061z00_2460;
									obj_t BgL_arg2062z00_2461;

									BgL_arg2061z00_2460 =
										(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_2423))->
										BgL_upz00);
									BgL_arg2062z00_2461 =
										(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_2424))->
										BgL_upz00);
									{	/* SawBbv/bbv-range.scm 94 */
										bool_t BgL_test2995z00_8182;

										{	/* SawBbv/bbv-range.scm 85 */
											obj_t BgL_classz00_5097;

											BgL_classz00_5097 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
											if (BGL_OBJECTP(BgL_arg2061z00_2460))
												{	/* SawBbv/bbv-range.scm 85 */
													BgL_objectz00_bglt BgL_arg1807z00_5099;

													BgL_arg1807z00_5099 =
														(BgL_objectz00_bglt) (BgL_arg2061z00_2460);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* SawBbv/bbv-range.scm 85 */
															long BgL_idxz00_5105;

															BgL_idxz00_5105 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5099);
															BgL_test2995z00_8182 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_5105 + 1L)) == BgL_classz00_5097);
														}
													else
														{	/* SawBbv/bbv-range.scm 85 */
															bool_t BgL_res2573z00_5130;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_oclassz00_5113;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1815z00_5121;
																	long BgL_arg1816z00_5122;

																	BgL_arg1815z00_5121 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* SawBbv/bbv-range.scm 85 */
																		long BgL_arg1817z00_5123;

																		BgL_arg1817z00_5123 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5099);
																		BgL_arg1816z00_5122 =
																			(BgL_arg1817z00_5123 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_5113 =
																		VECTOR_REF(BgL_arg1815z00_5121,
																		BgL_arg1816z00_5122);
																}
																{	/* SawBbv/bbv-range.scm 85 */
																	bool_t BgL__ortest_1115z00_5114;

																	BgL__ortest_1115z00_5114 =
																		(BgL_classz00_5097 == BgL_oclassz00_5113);
																	if (BgL__ortest_1115z00_5114)
																		{	/* SawBbv/bbv-range.scm 85 */
																			BgL_res2573z00_5130 =
																				BgL__ortest_1115z00_5114;
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_odepthz00_5115;

																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1804z00_5116;

																				BgL_arg1804z00_5116 =
																					(BgL_oclassz00_5113);
																				BgL_odepthz00_5115 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_5116);
																			}
																			if ((1L < BgL_odepthz00_5115))
																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1802z00_5118;

																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1803z00_5119;

																						BgL_arg1803z00_5119 =
																							(BgL_oclassz00_5113);
																						BgL_arg1802z00_5118 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_5119, 1L);
																					}
																					BgL_res2573z00_5130 =
																						(BgL_arg1802z00_5118 ==
																						BgL_classz00_5097);
																				}
																			else
																				{	/* SawBbv/bbv-range.scm 85 */
																					BgL_res2573z00_5130 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2995z00_8182 = BgL_res2573z00_5130;
														}
												}
											else
												{	/* SawBbv/bbv-range.scm 85 */
													BgL_test2995z00_8182 = ((bool_t) 0);
												}
										}
										if (BgL_test2995z00_8182)
											{	/* SawBbv/bbv-range.scm 94 */
												bool_t BgL_test3000z00_8205;

												{	/* SawBbv/bbv-range.scm 85 */
													obj_t BgL_classz00_5131;

													BgL_classz00_5131 =
														BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
													if (BGL_OBJECTP(BgL_arg2062z00_2461))
														{	/* SawBbv/bbv-range.scm 85 */
															BgL_objectz00_bglt BgL_arg1807z00_5133;

															BgL_arg1807z00_5133 =
																(BgL_objectz00_bglt) (BgL_arg2062z00_2461);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_idxz00_5139;

																	BgL_idxz00_5139 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_5133);
																	BgL_test3000z00_8205 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_5139 + 1L)) ==
																		BgL_classz00_5131);
																}
															else
																{	/* SawBbv/bbv-range.scm 85 */
																	bool_t BgL_res2574z00_5164;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_oclassz00_5147;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1815z00_5155;
																			long BgL_arg1816z00_5156;

																			BgL_arg1815z00_5155 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* SawBbv/bbv-range.scm 85 */
																				long BgL_arg1817z00_5157;

																				BgL_arg1817z00_5157 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_5133);
																				BgL_arg1816z00_5156 =
																					(BgL_arg1817z00_5157 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_5147 =
																				VECTOR_REF(BgL_arg1815z00_5155,
																				BgL_arg1816z00_5156);
																		}
																		{	/* SawBbv/bbv-range.scm 85 */
																			bool_t BgL__ortest_1115z00_5148;

																			BgL__ortest_1115z00_5148 =
																				(BgL_classz00_5131 ==
																				BgL_oclassz00_5147);
																			if (BgL__ortest_1115z00_5148)
																				{	/* SawBbv/bbv-range.scm 85 */
																					BgL_res2574z00_5164 =
																						BgL__ortest_1115z00_5148;
																				}
																			else
																				{	/* SawBbv/bbv-range.scm 85 */
																					long BgL_odepthz00_5149;

																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1804z00_5150;

																						BgL_arg1804z00_5150 =
																							(BgL_oclassz00_5147);
																						BgL_odepthz00_5149 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_5150);
																					}
																					if ((1L < BgL_odepthz00_5149))
																						{	/* SawBbv/bbv-range.scm 85 */
																							obj_t BgL_arg1802z00_5152;

																							{	/* SawBbv/bbv-range.scm 85 */
																								obj_t BgL_arg1803z00_5153;

																								BgL_arg1803z00_5153 =
																									(BgL_oclassz00_5147);
																								BgL_arg1802z00_5152 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_5153, 1L);
																							}
																							BgL_res2574z00_5164 =
																								(BgL_arg1802z00_5152 ==
																								BgL_classz00_5131);
																						}
																					else
																						{	/* SawBbv/bbv-range.scm 85 */
																							BgL_res2574z00_5164 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test3000z00_8205 = BgL_res2574z00_5164;
																}
														}
													else
														{	/* SawBbv/bbv-range.scm 85 */
															BgL_test3000z00_8205 = ((bool_t) 0);
														}
												}
												if (BgL_test3000z00_8205)
													{	/* SawBbv/bbv-range.scm 94 */
														BgL_test2994z00_8179 =
															(
															(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																		((BgL_bbvzd2vlenzd2_bglt)
																			BgL_arg2061z00_2460)))->BgL_vecz00) ==
															(((BgL_bbvzd2vlenzd2_bglt)
																	COBJECT(((BgL_bbvzd2vlenzd2_bglt)
																			BgL_arg2062z00_2461)))->BgL_vecz00));
													}
												else
													{	/* SawBbv/bbv-range.scm 94 */
														BgL_test2994z00_8179 = ((bool_t) 0);
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 94 */
												BgL_test2994z00_8179 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2994z00_8179)
									{	/* SawBbv/bbv-range.scm 565 */
										if (
											((long) (
													(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																((BgL_bbvzd2vlenzd2_bglt)
																	(((BgL_bbvzd2rangezd2_bglt)
																			COBJECT(BgL_xz00_2423))->BgL_upz00))))->
														BgL_offsetz00)) >
												(long) ((((BgL_bbvzd2vlenzd2_bglt)
															COBJECT(((BgL_bbvzd2vlenzd2_bglt) ((
																			(BgL_bbvzd2rangezd2_bglt)
																			COBJECT(BgL_yz00_2424))->BgL_upz00))))->
														BgL_offsetz00))))
											{	/* SawBbv/bbv-range.scm 567 */
												bool_t BgL_test3006z00_8243;

												{	/* SawBbv/bbv-range.scm 567 */
													obj_t BgL_tmpz00_8244;

													{	/* SawBbv/bbv-range.scm 567 */
														obj_t BgL_a1525z00_2441;

														BgL_a1525z00_2441 =
															(((BgL_bbvzd2rangezd2_bglt)
																COBJECT(BgL_xz00_2423))->BgL_loz00);
														{	/* SawBbv/bbv-range.scm 567 */
															obj_t BgL_b1526z00_2442;

															BgL_b1526z00_2442 =
																(((BgL_bbvzd2rangezd2_bglt)
																	COBJECT(BgL_yz00_2424))->BgL_loz00);
															{	/* SawBbv/bbv-range.scm 567 */

																{	/* SawBbv/bbv-range.scm 567 */
																	bool_t BgL_test3007z00_8247;

																	if (INTEGERP(BgL_a1525z00_2441))
																		{	/* SawBbv/bbv-range.scm 567 */
																			BgL_test3007z00_8247 =
																				INTEGERP(BgL_b1526z00_2442);
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 567 */
																			BgL_test3007z00_8247 = ((bool_t) 0);
																		}
																	if (BgL_test3007z00_8247)
																		{	/* SawBbv/bbv-range.scm 567 */
																			BgL_tmpz00_8244 =
																				BBOOL(
																				((long) CINT(BgL_a1525z00_2441) >=
																					(long) CINT(BgL_b1526z00_2442)));
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 567 */
																			BgL_tmpz00_8244 =
																				BBOOL
																				(BGl_2ze3zd3z30zz__r4_numbers_6_5z00
																				(BgL_a1525z00_2441, BgL_b1526z00_2442));
																		}
																}
															}
														}
													}
													BgL_test3006z00_8243 = (BgL_tmpz00_8244 == BTRUE);
												}
												if (BgL_test3006z00_8243)
													{	/* SawBbv/bbv-range.scm 567 */
														return BTRUE;
													}
												else
													{	/* SawBbv/bbv-range.scm 567 */
														return BUNSPEC;
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 566 */
												return BFALSE;
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 565 */
										if (
											(BGl_ze3rvze3zzsaw_bbvzd2rangezd2(
													(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_2423))->
														BgL_loz00),
													(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_2424))->
														BgL_upz00)) == BTRUE))
											{	/* SawBbv/bbv-range.scm 570 */
												return BTRUE;
											}
										else
											{	/* SawBbv/bbv-range.scm 570 */
												if (
													(BGl_ze3zd3rvz30zzsaw_bbvzd2rangezd2(
															(((BgL_bbvzd2rangezd2_bglt)
																	COBJECT(BgL_yz00_2424))->BgL_loz00),
															(((BgL_bbvzd2rangezd2_bglt)
																	COBJECT(BgL_xz00_2423))->BgL_upz00)) ==
														BTRUE))
													{	/* SawBbv/bbv-range.scm 571 */
														return BFALSE;
													}
												else
													{	/* SawBbv/bbv-range.scm 571 */
														return BUNSPEC;
													}
											}
									}
							}
					}
			}
		}

	}



/* &bbv-range> */
	obj_t BGl_z62bbvzd2rangeze3z53zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6266,
		obj_t BgL_xz00_6267, obj_t BgL_yz00_6268)
	{
		{	/* SawBbv/bbv-range.scm 559 */
			return
				BGl_bbvzd2rangeze3z31zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6267),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6268));
		}

	}



/* bbv-range>= */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2rangeze3zd3ze2zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_xz00_65, BgL_bbvzd2rangezd2_bglt BgL_yz00_66)
	{
		{	/* SawBbv/bbv-range.scm 583 */
			if (BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_xz00_65))
				{	/* SawBbv/bbv-range.scm 585 */
					if (BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_yz00_66))
						{	/* SawBbv/bbv-range.scm 585 */
							return BUNSPEC;
						}
					else
						{	/* SawBbv/bbv-range.scm 585 */
							return BTRUE;
						}
				}
			else
				{	/* SawBbv/bbv-range.scm 585 */
					if (BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_yz00_66))
						{	/* SawBbv/bbv-range.scm 586 */
							return BTRUE;
						}
					else
						{	/* SawBbv/bbv-range.scm 587 */
							bool_t BgL_test3014z00_8277;

							{	/* SawBbv/bbv-range.scm 587 */
								obj_t BgL_arg2096z00_2497;
								obj_t BgL_arg2097z00_2498;

								BgL_arg2096z00_2497 =
									(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_65))->BgL_upz00);
								BgL_arg2097z00_2498 =
									(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_66))->BgL_upz00);
								{	/* SawBbv/bbv-range.scm 94 */
									bool_t BgL_test3015z00_8280;

									{	/* SawBbv/bbv-range.scm 85 */
										obj_t BgL_classz00_5193;

										BgL_classz00_5193 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
										if (BGL_OBJECTP(BgL_arg2096z00_2497))
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_objectz00_bglt BgL_arg1807z00_5195;

												BgL_arg1807z00_5195 =
													(BgL_objectz00_bglt) (BgL_arg2096z00_2497);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_idxz00_5201;

														BgL_idxz00_5201 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5195);
														BgL_test3015z00_8280 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_5201 + 1L)) == BgL_classz00_5193);
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL_res2575z00_5226;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_oclassz00_5209;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1815z00_5217;
																long BgL_arg1816z00_5218;

																BgL_arg1815z00_5217 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_arg1817z00_5219;

																	BgL_arg1817z00_5219 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5195);
																	BgL_arg1816z00_5218 =
																		(BgL_arg1817z00_5219 - OBJECT_TYPE);
																}
																BgL_oclassz00_5209 =
																	VECTOR_REF(BgL_arg1815z00_5217,
																	BgL_arg1816z00_5218);
															}
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL__ortest_1115z00_5210;

																BgL__ortest_1115z00_5210 =
																	(BgL_classz00_5193 == BgL_oclassz00_5209);
																if (BgL__ortest_1115z00_5210)
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2575z00_5226 =
																			BgL__ortest_1115z00_5210;
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		long BgL_odepthz00_5211;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1804z00_5212;

																			BgL_arg1804z00_5212 =
																				(BgL_oclassz00_5209);
																			BgL_odepthz00_5211 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_5212);
																		}
																		if ((1L < BgL_odepthz00_5211))
																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1802z00_5214;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1803z00_5215;

																					BgL_arg1803z00_5215 =
																						(BgL_oclassz00_5209);
																					BgL_arg1802z00_5214 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_5215, 1L);
																				}
																				BgL_res2575z00_5226 =
																					(BgL_arg1802z00_5214 ==
																					BgL_classz00_5193);
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2575z00_5226 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test3015z00_8280 = BgL_res2575z00_5226;
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_test3015z00_8280 = ((bool_t) 0);
											}
									}
									if (BgL_test3015z00_8280)
										{	/* SawBbv/bbv-range.scm 94 */
											bool_t BgL_test3020z00_8303;

											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_classz00_5227;

												BgL_classz00_5227 =
													BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
												if (BGL_OBJECTP(BgL_arg2097z00_2498))
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_objectz00_bglt BgL_arg1807z00_5229;

														BgL_arg1807z00_5229 =
															(BgL_objectz00_bglt) (BgL_arg2097z00_2498);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_idxz00_5235;

																BgL_idxz00_5235 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_5229);
																BgL_test3020z00_8303 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_5235 + 1L)) ==
																	BgL_classz00_5227);
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL_res2576z00_5260;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_oclassz00_5243;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1815z00_5251;
																		long BgL_arg1816z00_5252;

																		BgL_arg1815z00_5251 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_arg1817z00_5253;

																			BgL_arg1817z00_5253 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_5229);
																			BgL_arg1816z00_5252 =
																				(BgL_arg1817z00_5253 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_5243 =
																			VECTOR_REF(BgL_arg1815z00_5251,
																			BgL_arg1816z00_5252);
																	}
																	{	/* SawBbv/bbv-range.scm 85 */
																		bool_t BgL__ortest_1115z00_5244;

																		BgL__ortest_1115z00_5244 =
																			(BgL_classz00_5227 == BgL_oclassz00_5243);
																		if (BgL__ortest_1115z00_5244)
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2576z00_5260 =
																					BgL__ortest_1115z00_5244;
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				long BgL_odepthz00_5245;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1804z00_5246;

																					BgL_arg1804z00_5246 =
																						(BgL_oclassz00_5243);
																					BgL_odepthz00_5245 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_5246);
																				}
																				if ((1L < BgL_odepthz00_5245))
																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1802z00_5248;

																						{	/* SawBbv/bbv-range.scm 85 */
																							obj_t BgL_arg1803z00_5249;

																							BgL_arg1803z00_5249 =
																								(BgL_oclassz00_5243);
																							BgL_arg1802z00_5248 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_5249, 1L);
																						}
																						BgL_res2576z00_5260 =
																							(BgL_arg1802z00_5248 ==
																							BgL_classz00_5227);
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 85 */
																						BgL_res2576z00_5260 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test3020z00_8303 = BgL_res2576z00_5260;
															}
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_test3020z00_8303 = ((bool_t) 0);
													}
											}
											if (BgL_test3020z00_8303)
												{	/* SawBbv/bbv-range.scm 94 */
													BgL_test3014z00_8277 =
														(
														(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																	((BgL_bbvzd2vlenzd2_bglt)
																		BgL_arg2096z00_2497)))->BgL_vecz00) ==
														(((BgL_bbvzd2vlenzd2_bglt)
																COBJECT(((BgL_bbvzd2vlenzd2_bglt)
																		BgL_arg2097z00_2498)))->BgL_vecz00));
												}
											else
												{	/* SawBbv/bbv-range.scm 94 */
													BgL_test3014z00_8277 = ((bool_t) 0);
												}
										}
									else
										{	/* SawBbv/bbv-range.scm 94 */
											BgL_test3014z00_8277 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test3014z00_8277)
								{	/* SawBbv/bbv-range.scm 587 */
									if (
										((long) (
												(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
															((BgL_bbvzd2vlenzd2_bglt)
																(((BgL_bbvzd2rangezd2_bglt)
																		COBJECT(BgL_xz00_65))->BgL_upz00))))->
													BgL_offsetz00)) >=
											(long) ((((BgL_bbvzd2vlenzd2_bglt)
														COBJECT(((BgL_bbvzd2vlenzd2_bglt) ((
																		(BgL_bbvzd2rangezd2_bglt)
																		COBJECT(BgL_yz00_66))->BgL_upz00))))->
													BgL_offsetz00))))
										{	/* SawBbv/bbv-range.scm 589 */
											bool_t BgL_test3026z00_8341;

											{	/* SawBbv/bbv-range.scm 589 */
												obj_t BgL_tmpz00_8342;

												{	/* SawBbv/bbv-range.scm 589 */
													obj_t BgL_a1527z00_2478;

													BgL_a1527z00_2478 =
														(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_65))->
														BgL_loz00);
													{	/* SawBbv/bbv-range.scm 589 */
														obj_t BgL_b1528z00_2479;

														BgL_b1528z00_2479 =
															(((BgL_bbvzd2rangezd2_bglt)
																COBJECT(BgL_yz00_66))->BgL_loz00);
														{	/* SawBbv/bbv-range.scm 589 */

															{	/* SawBbv/bbv-range.scm 589 */
																bool_t BgL_test3027z00_8345;

																if (INTEGERP(BgL_a1527z00_2478))
																	{	/* SawBbv/bbv-range.scm 589 */
																		BgL_test3027z00_8345 =
																			INTEGERP(BgL_b1528z00_2479);
																	}
																else
																	{	/* SawBbv/bbv-range.scm 589 */
																		BgL_test3027z00_8345 = ((bool_t) 0);
																	}
																if (BgL_test3027z00_8345)
																	{	/* SawBbv/bbv-range.scm 589 */
																		BgL_tmpz00_8342 =
																			BBOOL(
																			((long) CINT(BgL_a1527z00_2478) >=
																				(long) CINT(BgL_b1528z00_2479)));
																	}
																else
																	{	/* SawBbv/bbv-range.scm 589 */
																		BgL_tmpz00_8342 =
																			BBOOL(BGl_2ze3zd3z30zz__r4_numbers_6_5z00
																			(BgL_a1527z00_2478, BgL_b1528z00_2479));
																	}
															}
														}
													}
												}
												BgL_test3026z00_8341 = (BgL_tmpz00_8342 == BTRUE);
											}
											if (BgL_test3026z00_8341)
												{	/* SawBbv/bbv-range.scm 589 */
													return BTRUE;
												}
											else
												{	/* SawBbv/bbv-range.scm 589 */
													return BUNSPEC;
												}
										}
									else
										{	/* SawBbv/bbv-range.scm 588 */
											return BFALSE;
										}
								}
							else
								{	/* SawBbv/bbv-range.scm 587 */
									if (
										(BGl_ze3zd3rvz30zzsaw_bbvzd2rangezd2(
												(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_65))->
													BgL_loz00),
												(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_66))->
													BgL_upz00)) == BTRUE))
										{	/* SawBbv/bbv-range.scm 592 */
											return BTRUE;
										}
									else
										{	/* SawBbv/bbv-range.scm 592 */
											if (
												(BGl_ze3rvze3zzsaw_bbvzd2rangezd2(
														(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_66))->
															BgL_loz00),
														(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_65))->
															BgL_upz00)) == BTRUE))
												{	/* SawBbv/bbv-range.scm 593 */
													return BFALSE;
												}
											else
												{	/* SawBbv/bbv-range.scm 593 */
													return BUNSPEC;
												}
										}
								}
						}
				}
		}

	}



/* &bbv-range>= */
	obj_t BGl_z62bbvzd2rangeze3zd3z80zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6269,
		obj_t BgL_xz00_6270, obj_t BgL_yz00_6271)
	{
		{	/* SawBbv/bbv-range.scm 583 */
			return
				BGl_bbvzd2rangeze3zd3ze2zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6270),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6271));
		}

	}



/* bbv-range= */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2rangezd3z01zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_xz00_67, BgL_bbvzd2rangezd2_bglt BgL_yz00_68)
	{
		{	/* SawBbv/bbv-range.scm 599 */
			{	/* SawBbv/bbv-range.scm 601 */
				bool_t BgL_test3031z00_8369;

				if (BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_xz00_67))
					{	/* SawBbv/bbv-range.scm 601 */
						BgL_test3031z00_8369 = ((bool_t) 1);
					}
				else
					{	/* SawBbv/bbv-range.scm 601 */
						BgL_test3031z00_8369 =
							BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(BgL_yz00_68);
					}
				if (BgL_test3031z00_8369)
					{	/* SawBbv/bbv-range.scm 601 */
						return BUNSPEC;
					}
				else
					{	/* SawBbv/bbv-range.scm 603 */
						bool_t BgL_test3033z00_8373;

						if (
							(BGl_zd3rvzd3zzsaw_bbvzd2rangezd2(
									(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_67))->BgL_loz00),
									(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_67))->
										BgL_upz00)) == BTRUE))
							{	/* SawBbv/bbv-range.scm 603 */
								if (
									(BGl_zd3rvzd3zzsaw_bbvzd2rangezd2(
											(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_68))->
												BgL_loz00),
											(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_68))->
												BgL_upz00)) == BTRUE))
									{	/* SawBbv/bbv-range.scm 604 */
										BgL_test3033z00_8373 =
											(BGl_zd3rvzd3zzsaw_bbvzd2rangezd2(
												(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_67))->
													BgL_loz00),
												(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_68))->
													BgL_upz00)) == BTRUE);
									}
								else
									{	/* SawBbv/bbv-range.scm 604 */
										BgL_test3033z00_8373 = ((bool_t) 0);
									}
							}
						else
							{	/* SawBbv/bbv-range.scm 603 */
								BgL_test3033z00_8373 = ((bool_t) 0);
							}
						if (BgL_test3033z00_8373)
							{	/* SawBbv/bbv-range.scm 603 */
								return BTRUE;
							}
						else
							{	/* SawBbv/bbv-range.scm 607 */
								bool_t BgL_test3036z00_8388;

								if (
									(BGl_zd3rvzd3zzsaw_bbvzd2rangezd2(
											(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_67))->
												BgL_loz00),
											(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_67))->
												BgL_upz00)) == BTRUE))
									{	/* SawBbv/bbv-range.scm 607 */
										if (
											(BGl_zd3rvzd3zzsaw_bbvzd2rangezd2(
													(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_68))->
														BgL_loz00),
													(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_68))->
														BgL_upz00)) == BTRUE))
											{	/* SawBbv/bbv-range.scm 608 */
												BgL_test3036z00_8388 =
													(BGl_zd3rvzd3zzsaw_bbvzd2rangezd2(
														(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_67))->
															BgL_loz00),
														(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_68))->
															BgL_upz00)) == BFALSE);
											}
										else
											{	/* SawBbv/bbv-range.scm 608 */
												BgL_test3036z00_8388 = ((bool_t) 0);
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 607 */
										BgL_test3036z00_8388 = ((bool_t) 0);
									}
								if (BgL_test3036z00_8388)
									{	/* SawBbv/bbv-range.scm 607 */
										return BFALSE;
									}
								else
									{	/* SawBbv/bbv-range.scm 607 */
										return BUNSPEC;
									}
							}
					}
			}
		}

	}



/* &bbv-range= */
	obj_t BGl_z62bbvzd2rangezd3z63zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6272,
		obj_t BgL_xz00_6273, obj_t BgL_yz00_6274)
	{
		{	/* SawBbv/bbv-range.scm 599 */
			return
				BGl_bbvzd2rangezd3z01zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6273),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6274));
		}

	}



/* bbv-range-lt */
	BGL_EXPORTED_DEF BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2ltz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_xz00_69, BgL_bbvzd2rangezd2_bglt BgL_yz00_70)
	{
		{	/* SawBbv/bbv-range.scm 620 */
			{	/* SawBbv/bbv-range.scm 622 */
				obj_t BgL_xlz00_2560;
				obj_t BgL_xuz00_2561;
				obj_t BgL_yuz00_2563;

				BgL_xlz00_2560 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_69))->BgL_loz00);
				BgL_xuz00_2561 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_69))->BgL_upz00);
				BgL_yuz00_2563 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_70))->BgL_upz00);
				{	/* SawBbv/bbv-range.scm 627 */
					BgL_bbvzd2rangezd2_bglt BgL_resz00_2564;

					{	/* SawBbv/bbv-range.scm 627 */
						BgL_bbvzd2rangezd2_bglt BgL_new1240z00_2565;

						{	/* SawBbv/bbv-range.scm 628 */
							BgL_bbvzd2rangezd2_bglt BgL_new1239z00_2567;

							BgL_new1239z00_2567 =
								((BgL_bbvzd2rangezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_bbvzd2rangezd2_bgl))));
							{	/* SawBbv/bbv-range.scm 628 */
								long BgL_arg2158z00_2568;

								BgL_arg2158z00_2568 =
									BGL_CLASS_NUM(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1239z00_2567),
									BgL_arg2158z00_2568);
							}
							BgL_new1240z00_2565 = BgL_new1239z00_2567;
						}
						((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1240z00_2565))->
								BgL_loz00) = ((obj_t) BgL_xlz00_2560), BUNSPEC);
						{
							obj_t BgL_auxz00_8414;

							{	/* SawBbv/bbv-range.scm 629 */
								obj_t BgL_arg2157z00_2566;

								BgL_arg2157z00_2566 =
									BGl_zd2zd2z00zzsaw_bbvzd2rangezd2(BgL_yuz00_2563);
								BgL_auxz00_8414 =
									BGl_minrvzd2upzd2zzsaw_bbvzd2rangezd2(BgL_xuz00_2561,
									BgL_arg2157z00_2566, BgL_xuz00_2561);
							}
							((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1240z00_2565))->
									BgL_upz00) = ((obj_t) BgL_auxz00_8414), BUNSPEC);
						}
						BgL_resz00_2564 = BgL_new1240z00_2565;
					}
					return BgL_resz00_2564;
				}
			}
		}

	}



/* &bbv-range-lt */
	BgL_bbvzd2rangezd2_bglt BGl_z62bbvzd2rangezd2ltz62zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6275, obj_t BgL_xz00_6276, obj_t BgL_yz00_6277)
	{
		{	/* SawBbv/bbv-range.scm 620 */
			return
				BGl_bbvzd2rangezd2ltz00zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6276),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6277));
		}

	}



/* bbv-range-lte */
	BGL_EXPORTED_DEF BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2ltez00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_xz00_71, BgL_bbvzd2rangezd2_bglt BgL_yz00_72)
	{
		{	/* SawBbv/bbv-range.scm 642 */
			{	/* SawBbv/bbv-range.scm 643 */
				obj_t BgL_xlz00_2569;
				obj_t BgL_xuz00_2570;
				obj_t BgL_yuz00_2572;

				BgL_xlz00_2569 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_71))->BgL_loz00);
				BgL_xuz00_2570 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_71))->BgL_upz00);
				BgL_yuz00_2572 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_72))->BgL_upz00);
				{	/* SawBbv/bbv-range.scm 648 */
					BgL_bbvzd2rangezd2_bglt BgL_resz00_2573;

					{	/* SawBbv/bbv-range.scm 648 */
						BgL_bbvzd2rangezd2_bglt BgL_new1243z00_2574;

						{	/* SawBbv/bbv-range.scm 649 */
							BgL_bbvzd2rangezd2_bglt BgL_new1242z00_2575;

							BgL_new1242z00_2575 =
								((BgL_bbvzd2rangezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_bbvzd2rangezd2_bgl))));
							{	/* SawBbv/bbv-range.scm 649 */
								long BgL_arg2159z00_2576;

								BgL_arg2159z00_2576 =
									BGL_CLASS_NUM(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1242z00_2575),
									BgL_arg2159z00_2576);
							}
							BgL_new1243z00_2574 = BgL_new1242z00_2575;
						}
						((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1243z00_2574))->
								BgL_loz00) = ((obj_t) BgL_xlz00_2569), BUNSPEC);
						((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1243z00_2574))->
								BgL_upz00) =
							((obj_t) BGl_minrvzd2upzd2zzsaw_bbvzd2rangezd2(BgL_xuz00_2570,
									BgL_yuz00_2572, BgL_xuz00_2570)), BUNSPEC);
						BgL_resz00_2573 = BgL_new1243z00_2574;
					}
					return BgL_resz00_2573;
				}
			}
		}

	}



/* &bbv-range-lte */
	BgL_bbvzd2rangezd2_bglt BGl_z62bbvzd2rangezd2ltez62zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6278, obj_t BgL_xz00_6279, obj_t BgL_yz00_6280)
	{
		{	/* SawBbv/bbv-range.scm 642 */
			return
				BGl_bbvzd2rangezd2ltez00zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6279),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6280));
		}

	}



/* bbv-range-gt */
	BGL_EXPORTED_DEF BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2gtz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_xz00_73, BgL_bbvzd2rangezd2_bglt BgL_yz00_74)
	{
		{	/* SawBbv/bbv-range.scm 656 */
			{	/* SawBbv/bbv-range.scm 657 */
				obj_t BgL_xlz00_2577;
				obj_t BgL_xuz00_2578;
				obj_t BgL_ylz00_2579;

				BgL_xlz00_2577 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_73))->BgL_loz00);
				BgL_xuz00_2578 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_73))->BgL_upz00);
				BgL_ylz00_2579 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_74))->BgL_loz00);
				{	/* SawBbv/bbv-range.scm 662 */
					BgL_bbvzd2rangezd2_bglt BgL_resz00_2581;

					{	/* SawBbv/bbv-range.scm 662 */
						BgL_bbvzd2rangezd2_bglt BgL_new1245z00_2582;

						{	/* SawBbv/bbv-range.scm 663 */
							BgL_bbvzd2rangezd2_bglt BgL_new1244z00_2584;

							BgL_new1244z00_2584 =
								((BgL_bbvzd2rangezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_bbvzd2rangezd2_bgl))));
							{	/* SawBbv/bbv-range.scm 663 */
								long BgL_arg2161z00_2585;

								BgL_arg2161z00_2585 =
									BGL_CLASS_NUM(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1244z00_2584),
									BgL_arg2161z00_2585);
							}
							BgL_new1245z00_2582 = BgL_new1244z00_2584;
						}
						{
							obj_t BgL_auxz00_8441;

							{	/* SawBbv/bbv-range.scm 663 */
								obj_t BgL_arg2160z00_2583;

								BgL_arg2160z00_2583 =
									BGl_zb2zb2z00zzsaw_bbvzd2rangezd2(BgL_ylz00_2579);
								BgL_auxz00_8441 =
									BGl_maxrvzd2lozd2zzsaw_bbvzd2rangezd2(BgL_xlz00_2577,
									BgL_arg2160z00_2583, BgL_xlz00_2577);
							}
							((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1245z00_2582))->
									BgL_loz00) = ((obj_t) BgL_auxz00_8441), BUNSPEC);
						}
						((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1245z00_2582))->
								BgL_upz00) = ((obj_t) BgL_xuz00_2578), BUNSPEC);
						BgL_resz00_2581 = BgL_new1245z00_2582;
					}
					return BgL_resz00_2581;
				}
			}
		}

	}



/* &bbv-range-gt */
	BgL_bbvzd2rangezd2_bglt BGl_z62bbvzd2rangezd2gtz62zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6281, obj_t BgL_xz00_6282, obj_t BgL_yz00_6283)
	{
		{	/* SawBbv/bbv-range.scm 656 */
			return
				BGl_bbvzd2rangezd2gtz00zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6282),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6283));
		}

	}



/* bbv-range-gte */
	BGL_EXPORTED_DEF BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2gtez00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_xz00_75, BgL_bbvzd2rangezd2_bglt BgL_yz00_76)
	{
		{	/* SawBbv/bbv-range.scm 670 */
			{	/* SawBbv/bbv-range.scm 672 */
				obj_t BgL_xlz00_2586;
				obj_t BgL_xuz00_2587;
				obj_t BgL_ylz00_2588;

				BgL_xlz00_2586 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_75))->BgL_loz00);
				BgL_xuz00_2587 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_75))->BgL_upz00);
				BgL_ylz00_2588 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_76))->BgL_loz00);
				{	/* SawBbv/bbv-range.scm 677 */
					BgL_bbvzd2rangezd2_bglt BgL_resz00_2590;

					{	/* SawBbv/bbv-range.scm 677 */
						BgL_bbvzd2rangezd2_bglt BgL_new1247z00_2591;

						{	/* SawBbv/bbv-range.scm 678 */
							BgL_bbvzd2rangezd2_bglt BgL_new1246z00_2592;

							BgL_new1246z00_2592 =
								((BgL_bbvzd2rangezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_bbvzd2rangezd2_bgl))));
							{	/* SawBbv/bbv-range.scm 678 */
								long BgL_arg2162z00_2593;

								BgL_arg2162z00_2593 =
									BGL_CLASS_NUM(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1246z00_2592),
									BgL_arg2162z00_2593);
							}
							BgL_new1247z00_2591 = BgL_new1246z00_2592;
						}
						((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1247z00_2591))->
								BgL_loz00) =
							((obj_t) BGl_maxrvzd2lozd2zzsaw_bbvzd2rangezd2(BgL_xlz00_2586,
									BgL_ylz00_2588, BgL_xlz00_2586)), BUNSPEC);
						((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1247z00_2591))->
								BgL_upz00) = ((obj_t) BgL_xuz00_2587), BUNSPEC);
						BgL_resz00_2590 = BgL_new1247z00_2591;
					}
					return BgL_resz00_2590;
				}
			}
		}

	}



/* &bbv-range-gte */
	BgL_bbvzd2rangezd2_bglt BGl_z62bbvzd2rangezd2gtez62zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6284, obj_t BgL_xz00_6285, obj_t BgL_yz00_6286)
	{
		{	/* SawBbv/bbv-range.scm 670 */
			return
				BGl_bbvzd2rangezd2gtez00zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6285),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6286));
		}

	}



/* bbv-range-eq */
	BGL_EXPORTED_DEF BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2eqz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_xz00_77, BgL_bbvzd2rangezd2_bglt BgL_yz00_78)
	{
		{	/* SawBbv/bbv-range.scm 691 */
			BGL_TAIL return
				BGl_bbvzd2rangezd2intersectionz00zzsaw_bbvzd2rangezd2(BgL_xz00_77,
				BgL_yz00_78);
		}

	}



/* &bbv-range-eq */
	BgL_bbvzd2rangezd2_bglt BGl_z62bbvzd2rangezd2eqz62zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6287, obj_t BgL_xz00_6288, obj_t BgL_yz00_6289)
	{
		{	/* SawBbv/bbv-range.scm 691 */
			return
				BGl_bbvzd2rangezd2eqz00zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6288),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6289));
		}

	}



/* bbv-range-neq */
	BGL_EXPORTED_DEF BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2neqz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_xz00_79, BgL_bbvzd2rangezd2_bglt BgL_yz00_80)
	{
		{	/* SawBbv/bbv-range.scm 700 */
			if (BGl_bbvzd2singletonzf3z21zzsaw_bbvzd2rangezd2(((obj_t) BgL_xz00_79)))
				{	/* SawBbv/bbv-range.scm 703 */
					return BgL_xz00_79;
				}
			else
				{	/* SawBbv/bbv-range.scm 703 */
					if (BGl_bbvzd2singletonzf3z21zzsaw_bbvzd2rangezd2(
							((obj_t) BgL_yz00_80)))
						{	/* SawBbv/bbv-range.scm 706 */
							obj_t BgL_yvz00_2596;

							BgL_yvz00_2596 =
								(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_80))->BgL_loz00);
							if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_yvz00_2596,
									(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_79))->
										BgL_loz00)))
								{	/* SawBbv/bbv-range.scm 709 */
									BgL_bbvzd2rangezd2_bglt BgL_new1248z00_2600;

									{	/* SawBbv/bbv-range.scm 710 */
										BgL_bbvzd2rangezd2_bglt BgL_new1251z00_2602;

										BgL_new1251z00_2602 =
											((BgL_bbvzd2rangezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_bbvzd2rangezd2_bgl))));
										{	/* SawBbv/bbv-range.scm 710 */
											long BgL_arg2168z00_2603;

											BgL_arg2168z00_2603 =
												BGL_CLASS_NUM(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1251z00_2602),
												BgL_arg2168z00_2603);
										}
										BgL_new1248z00_2600 = BgL_new1251z00_2602;
									}
									{
										obj_t BgL_auxz00_8480;

										{	/* SawBbv/bbv-range.scm 710 */
											obj_t BgL_arg2167z00_2601;

											BgL_arg2167z00_2601 =
												(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_79))->
												BgL_loz00);
											BgL_auxz00_8480 =
												BGl_zb2zb2z00zzsaw_bbvzd2rangezd2(BgL_arg2167z00_2601);
										}
										((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1248z00_2600))->
												BgL_loz00) = ((obj_t) BgL_auxz00_8480), BUNSPEC);
									}
									((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1248z00_2600))->
											BgL_upz00) =
										((obj_t) (((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_79))->
												BgL_upz00)), BUNSPEC);
									return BgL_new1248z00_2600;
								}
							else
								{	/* SawBbv/bbv-range.scm 708 */
									if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00(BgL_yvz00_2596,
											(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_79))->
												BgL_upz00)))
										{	/* SawBbv/bbv-range.scm 712 */
											BgL_bbvzd2rangezd2_bglt BgL_new1252z00_2607;

											{	/* SawBbv/bbv-range.scm 712 */
												BgL_bbvzd2rangezd2_bglt BgL_new1255z00_2609;

												BgL_new1255z00_2609 =
													((BgL_bbvzd2rangezd2_bglt)
													BOBJECT(GC_MALLOC(sizeof(struct
																BgL_bbvzd2rangezd2_bgl))));
												{	/* SawBbv/bbv-range.scm 712 */
													long BgL_arg2172z00_2610;

													BgL_arg2172z00_2610 =
														BGL_CLASS_NUM
														(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
													BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
															BgL_new1255z00_2609), BgL_arg2172z00_2610);
												}
												BgL_new1252z00_2607 = BgL_new1255z00_2609;
											}
											((((BgL_bbvzd2rangezd2_bglt)
														COBJECT(BgL_new1252z00_2607))->BgL_loz00) =
												((obj_t) (((BgL_bbvzd2rangezd2_bglt)
															COBJECT(BgL_xz00_79))->BgL_loz00)), BUNSPEC);
											{
												obj_t BgL_auxz00_8495;

												{	/* SawBbv/bbv-range.scm 713 */
													obj_t BgL_arg2171z00_2608;

													BgL_arg2171z00_2608 =
														(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_79))->
														BgL_upz00);
													BgL_auxz00_8495 =
														BGl_zd2zd2z00zzsaw_bbvzd2rangezd2
														(BgL_arg2171z00_2608);
												}
												((((BgL_bbvzd2rangezd2_bglt)
															COBJECT(BgL_new1252z00_2607))->BgL_upz00) =
													((obj_t) BgL_auxz00_8495), BUNSPEC);
											}
											return BgL_new1252z00_2607;
										}
									else
										{	/* SawBbv/bbv-range.scm 711 */
											return BgL_xz00_79;
										}
								}
						}
					else
						{	/* SawBbv/bbv-range.scm 705 */
							return BgL_xz00_79;
						}
				}
		}

	}



/* &bbv-range-neq */
	BgL_bbvzd2rangezd2_bglt BGl_z62bbvzd2rangezd2neqz62zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6290, obj_t BgL_xz00_6291, obj_t BgL_yz00_6292)
	{
		{	/* SawBbv/bbv-range.scm 700 */
			return
				BGl_bbvzd2rangezd2neqz00zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6291),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6292));
		}

	}



/* ->range */
	obj_t BGl_zd2ze3rangez31zzsaw_bbvzd2rangezd2(obj_t BgL_nz00_81)
	{
		{	/* SawBbv/bbv-range.scm 722 */
			if (INTEGERP(BgL_nz00_81))
				{	/* SawBbv/bbv-range.scm 726 */
					bool_t BgL_test3044z00_8504;

					{	/* SawBbv/bbv-range.scm 726 */
						long BgL_b1529z00_2622;

						{	/* SawBbv/bbv-range.scm 245 */
							long BgL_arg1820z00_5334;

							{	/* SawBbv/bbv-range.scm 245 */
								long BgL_arg1822z00_5335;

								{	/* SawBbv/bbv-range.scm 243 */
									long BgL_arg1808z00_5336;

									{	/* SawBbv/bbv-range.scm 243 */
										obj_t BgL_arg1812z00_5337;

										BgL_arg1812z00_5337 =
											BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0));
										BgL_arg1808z00_5336 =
											((long) CINT(BgL_arg1812z00_5337) - 2L);
									}
									BgL_arg1822z00_5335 = (1L << (int) (BgL_arg1808z00_5336));
								}
								BgL_arg1820z00_5334 = NEG(BgL_arg1822z00_5335);
							}
							BgL_b1529z00_2622 = (BgL_arg1820z00_5334 - 1L);
						}
						BgL_test3044z00_8504 =
							((long) CINT(BgL_nz00_81) < BgL_b1529z00_2622);
					}
					if (BgL_test3044z00_8504)
						{	/* SawBbv/bbv-range.scm 726 */
							return BGL_REAL_CNST(BGl_real2599z00zzsaw_bbvzd2rangezd2);
						}
					else
						{	/* SawBbv/bbv-range.scm 727 */
							bool_t BgL_test3045z00_8515;

							{	/* SawBbv/bbv-range.scm 727 */
								long BgL_b1530z00_2620;

								{	/* SawBbv/bbv-range.scm 243 */
									long BgL_arg1808z00_5344;

									{	/* SawBbv/bbv-range.scm 243 */
										obj_t BgL_arg1812z00_5345;

										BgL_arg1812z00_5345 =
											BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0));
										BgL_arg1808z00_5344 =
											((long) CINT(BgL_arg1812z00_5345) - 2L);
									}
									BgL_b1530z00_2620 = (1L << (int) (BgL_arg1808z00_5344));
								}
								BgL_test3045z00_8515 =
									((long) CINT(BgL_nz00_81) > BgL_b1530z00_2620);
							}
							if (BgL_test3045z00_8515)
								{	/* SawBbv/bbv-range.scm 727 */
									return BGL_REAL_CNST(BGl_real2600z00zzsaw_bbvzd2rangezd2);
								}
							else
								{	/* SawBbv/bbv-range.scm 727 */
									return BgL_nz00_81;
								}
						}
				}
			else
				{	/* SawBbv/bbv-range.scm 724 */
					if (BIGNUMP(BgL_nz00_81))
						{	/* SawBbv/bbv-range.scm 729 */
							if (BGl_2ze3ze3zz__r4_numbers_6_5z00(BgL_nz00_81, BINT(0L)))
								{	/* SawBbv/bbv-range.scm 730 */
									return BGL_REAL_CNST(BGl_real2600z00zzsaw_bbvzd2rangezd2);
								}
							else
								{	/* SawBbv/bbv-range.scm 730 */
									return BGL_REAL_CNST(BGl_real2599z00zzsaw_bbvzd2rangezd2);
								}
						}
					else
						{	/* SawBbv/bbv-range.scm 729 */
							return BgL_nz00_81;
						}
				}
		}

	}



/* +rv */
	obj_t BGl_zb2rvzb2zzsaw_bbvzd2rangezd2(obj_t BgL_xz00_82, obj_t BgL_yz00_83,
		double BgL_defz00_84)
	{
		{	/* SawBbv/bbv-range.scm 737 */
		BGl_zb2rvzb2zzsaw_bbvzd2rangezd2:
			{	/* SawBbv/bbv-range.scm 739 */
				bool_t BgL_test3048z00_8529;

				if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_xz00_82))
					{	/* SawBbv/bbv-range.scm 739 */
						BgL_test3048z00_8529 =
							BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_yz00_83);
					}
				else
					{	/* SawBbv/bbv-range.scm 739 */
						BgL_test3048z00_8529 = ((bool_t) 0);
					}
				if (BgL_test3048z00_8529)
					{	/* SawBbv/bbv-range.scm 740 */
						obj_t BgL_arg2184z00_2628;

						{	/* SawBbv/bbv-range.scm 740 */
							bool_t BgL_test3050z00_8533;

							if (INTEGERP(BgL_xz00_82))
								{	/* SawBbv/bbv-range.scm 740 */
									BgL_test3050z00_8533 = INTEGERP(BgL_yz00_83);
								}
							else
								{	/* SawBbv/bbv-range.scm 740 */
									BgL_test3050z00_8533 = ((bool_t) 0);
								}
							if (BgL_test3050z00_8533)
								{	/* SawBbv/bbv-range.scm 740 */
									obj_t BgL_tmpz00_5353;

									BgL_tmpz00_5353 = BINT(0L);
									if (BGL_ADDFX_OV(BgL_xz00_82, BgL_yz00_83, BgL_tmpz00_5353))
										{	/* SawBbv/bbv-range.scm 740 */
											BgL_arg2184z00_2628 =
												bgl_bignum_add(bgl_long_to_bignum(
													(long) CINT(BgL_xz00_82)),
												bgl_long_to_bignum((long) CINT(BgL_yz00_83)));
										}
									else
										{	/* SawBbv/bbv-range.scm 740 */
											BgL_arg2184z00_2628 = BgL_tmpz00_5353;
										}
								}
							else
								{	/* SawBbv/bbv-range.scm 740 */
									BgL_arg2184z00_2628 =
										BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_xz00_82, BgL_yz00_83);
								}
						}
						return BGl_zd2ze3rangez31zzsaw_bbvzd2rangezd2(BgL_arg2184z00_2628);
					}
				else
					{	/* SawBbv/bbv-range.scm 741 */
						bool_t BgL_test3053z00_8547;

						{	/* SawBbv/bbv-range.scm 741 */
							bool_t BgL_test3054z00_8548;

							{	/* SawBbv/bbv-range.scm 85 */
								obj_t BgL_classz00_5361;

								BgL_classz00_5361 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
								if (BGL_OBJECTP(BgL_xz00_82))
									{	/* SawBbv/bbv-range.scm 85 */
										BgL_objectz00_bglt BgL_arg1807z00_5363;

										BgL_arg1807z00_5363 = (BgL_objectz00_bglt) (BgL_xz00_82);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawBbv/bbv-range.scm 85 */
												long BgL_idxz00_5369;

												BgL_idxz00_5369 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5363);
												BgL_test3054z00_8548 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5369 + 1L)) == BgL_classz00_5361);
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												bool_t BgL_res2577z00_5394;

												{	/* SawBbv/bbv-range.scm 85 */
													obj_t BgL_oclassz00_5377;

													{	/* SawBbv/bbv-range.scm 85 */
														obj_t BgL_arg1815z00_5385;
														long BgL_arg1816z00_5386;

														BgL_arg1815z00_5385 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawBbv/bbv-range.scm 85 */
															long BgL_arg1817z00_5387;

															BgL_arg1817z00_5387 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5363);
															BgL_arg1816z00_5386 =
																(BgL_arg1817z00_5387 - OBJECT_TYPE);
														}
														BgL_oclassz00_5377 =
															VECTOR_REF(BgL_arg1815z00_5385,
															BgL_arg1816z00_5386);
													}
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL__ortest_1115z00_5378;

														BgL__ortest_1115z00_5378 =
															(BgL_classz00_5361 == BgL_oclassz00_5377);
														if (BgL__ortest_1115z00_5378)
															{	/* SawBbv/bbv-range.scm 85 */
																BgL_res2577z00_5394 = BgL__ortest_1115z00_5378;
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_odepthz00_5379;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1804z00_5380;

																	BgL_arg1804z00_5380 = (BgL_oclassz00_5377);
																	BgL_odepthz00_5379 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_5380);
																}
																if ((1L < BgL_odepthz00_5379))
																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1802z00_5382;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1803z00_5383;

																			BgL_arg1803z00_5383 =
																				(BgL_oclassz00_5377);
																			BgL_arg1802z00_5382 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_5383, 1L);
																		}
																		BgL_res2577z00_5394 =
																			(BgL_arg1802z00_5382 ==
																			BgL_classz00_5361);
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2577z00_5394 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test3054z00_8548 = BgL_res2577z00_5394;
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 85 */
										BgL_test3054z00_8548 = ((bool_t) 0);
									}
							}
							if (BgL_test3054z00_8548)
								{	/* SawBbv/bbv-range.scm 85 */
									obj_t BgL_classz00_5395;

									BgL_classz00_5395 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
									if (BGL_OBJECTP(BgL_yz00_83))
										{	/* SawBbv/bbv-range.scm 85 */
											BgL_objectz00_bglt BgL_arg1807z00_5397;

											BgL_arg1807z00_5397 = (BgL_objectz00_bglt) (BgL_yz00_83);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawBbv/bbv-range.scm 85 */
													long BgL_idxz00_5403;

													BgL_idxz00_5403 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5397);
													BgL_test3053z00_8547 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_5403 + 1L)) == BgL_classz00_5395);
												}
											else
												{	/* SawBbv/bbv-range.scm 85 */
													bool_t BgL_res2578z00_5428;

													{	/* SawBbv/bbv-range.scm 85 */
														obj_t BgL_oclassz00_5411;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_arg1815z00_5419;
															long BgL_arg1816z00_5420;

															BgL_arg1815z00_5419 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_arg1817z00_5421;

																BgL_arg1817z00_5421 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5397);
																BgL_arg1816z00_5420 =
																	(BgL_arg1817z00_5421 - OBJECT_TYPE);
															}
															BgL_oclassz00_5411 =
																VECTOR_REF(BgL_arg1815z00_5419,
																BgL_arg1816z00_5420);
														}
														{	/* SawBbv/bbv-range.scm 85 */
															bool_t BgL__ortest_1115z00_5412;

															BgL__ortest_1115z00_5412 =
																(BgL_classz00_5395 == BgL_oclassz00_5411);
															if (BgL__ortest_1115z00_5412)
																{	/* SawBbv/bbv-range.scm 85 */
																	BgL_res2578z00_5428 =
																		BgL__ortest_1115z00_5412;
																}
															else
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_odepthz00_5413;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1804z00_5414;

																		BgL_arg1804z00_5414 = (BgL_oclassz00_5411);
																		BgL_odepthz00_5413 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_5414);
																	}
																	if ((1L < BgL_odepthz00_5413))
																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1802z00_5416;

																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1803z00_5417;

																				BgL_arg1803z00_5417 =
																					(BgL_oclassz00_5411);
																				BgL_arg1802z00_5416 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_5417, 1L);
																			}
																			BgL_res2578z00_5428 =
																				(BgL_arg1802z00_5416 ==
																				BgL_classz00_5395);
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 85 */
																			BgL_res2578z00_5428 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test3053z00_8547 = BgL_res2578z00_5428;
												}
										}
									else
										{	/* SawBbv/bbv-range.scm 85 */
											BgL_test3053z00_8547 = ((bool_t) 0);
										}
								}
							else
								{	/* SawBbv/bbv-range.scm 741 */
									BgL_test3053z00_8547 = ((bool_t) 0);
								}
						}
						if (BgL_test3053z00_8547)
							{	/* SawBbv/bbv-range.scm 741 */
								if (
									((((BgL_bbvzd2vlenzd2_bglt) COBJECT(
													((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_82)))->
											BgL_vecz00) ==
										(((BgL_bbvzd2vlenzd2_bglt) COBJECT(((BgL_bbvzd2vlenzd2_bglt)
														BgL_yz00_83)))->BgL_vecz00)))
									{	/* SawBbv/bbv-range.scm 743 */
										long BgL_noz00_2635;

										BgL_noz00_2635 =
											(
											(long) (
												(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
															((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_82)))->
													BgL_offsetz00)) +
											(long) ((((BgL_bbvzd2vlenzd2_bglt)
														COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_yz00_83)))->
													BgL_offsetz00)));
										{	/* SawBbv/bbv-range.scm 744 */
											BgL_bbvzd2vlenzd2_bglt BgL_new1256z00_2637;

											{	/* SawBbv/bbv-range.scm 744 */
												BgL_bbvzd2vlenzd2_bglt BgL_new1260z00_2638;

												BgL_new1260z00_2638 =
													((BgL_bbvzd2vlenzd2_bglt)
													BOBJECT(GC_MALLOC(sizeof(struct
																BgL_bbvzd2vlenzd2_bgl))));
												{	/* SawBbv/bbv-range.scm 744 */
													long BgL_arg2191z00_2639;

													BgL_arg2191z00_2639 =
														BGL_CLASS_NUM
														(BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2);
													BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
															BgL_new1260z00_2638), BgL_arg2191z00_2639);
												}
												BgL_new1256z00_2637 = BgL_new1260z00_2638;
											}
											((((BgL_bbvzd2vlenzd2_bglt)
														COBJECT(BgL_new1256z00_2637))->BgL_vecz00) =
												((obj_t) (((BgL_bbvzd2vlenzd2_bglt)
															COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_82)))->
														BgL_vecz00)), BUNSPEC);
											((((BgL_bbvzd2vlenzd2_bglt)
														COBJECT(BgL_new1256z00_2637))->BgL_offsetz00) =
												((int) (int) (BgL_noz00_2635)), BUNSPEC);
											return ((obj_t) BgL_new1256z00_2637);
										}
									}
								else
									{	/* SawBbv/bbv-range.scm 243 */
										long BgL_arg1808z00_5442;

										{	/* SawBbv/bbv-range.scm 243 */
											obj_t BgL_arg1812z00_5443;

											BgL_arg1812z00_5443 =
												BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
												(0));
											BgL_arg1808z00_5442 =
												((long) CINT(BgL_arg1812z00_5443) - 2L);
										}
										return BINT((1L << (int) (BgL_arg1808z00_5442)));
							}}
						else
							{	/* SawBbv/bbv-range.scm 747 */
								bool_t BgL_test3064z00_8623;

								{	/* SawBbv/bbv-range.scm 747 */
									bool_t BgL_test3065z00_8624;

									{	/* SawBbv/bbv-range.scm 85 */
										obj_t BgL_classz00_5446;

										BgL_classz00_5446 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
										if (BGL_OBJECTP(BgL_xz00_82))
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_objectz00_bglt BgL_arg1807z00_5448;

												BgL_arg1807z00_5448 =
													(BgL_objectz00_bglt) (BgL_xz00_82);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_idxz00_5454;

														BgL_idxz00_5454 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5448);
														BgL_test3065z00_8624 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_5454 + 1L)) == BgL_classz00_5446);
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL_res2579z00_5479;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_oclassz00_5462;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1815z00_5470;
																long BgL_arg1816z00_5471;

																BgL_arg1815z00_5470 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_arg1817z00_5472;

																	BgL_arg1817z00_5472 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5448);
																	BgL_arg1816z00_5471 =
																		(BgL_arg1817z00_5472 - OBJECT_TYPE);
																}
																BgL_oclassz00_5462 =
																	VECTOR_REF(BgL_arg1815z00_5470,
																	BgL_arg1816z00_5471);
															}
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL__ortest_1115z00_5463;

																BgL__ortest_1115z00_5463 =
																	(BgL_classz00_5446 == BgL_oclassz00_5462);
																if (BgL__ortest_1115z00_5463)
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2579z00_5479 =
																			BgL__ortest_1115z00_5463;
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		long BgL_odepthz00_5464;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1804z00_5465;

																			BgL_arg1804z00_5465 =
																				(BgL_oclassz00_5462);
																			BgL_odepthz00_5464 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_5465);
																		}
																		if ((1L < BgL_odepthz00_5464))
																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1802z00_5467;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1803z00_5468;

																					BgL_arg1803z00_5468 =
																						(BgL_oclassz00_5462);
																					BgL_arg1802z00_5467 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_5468, 1L);
																				}
																				BgL_res2579z00_5479 =
																					(BgL_arg1802z00_5467 ==
																					BgL_classz00_5446);
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2579z00_5479 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test3065z00_8624 = BgL_res2579z00_5479;
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_test3065z00_8624 = ((bool_t) 0);
											}
									}
									if (BgL_test3065z00_8624)
										{	/* SawBbv/bbv-range.scm 747 */
											BgL_test3064z00_8623 = INTEGERP(BgL_yz00_83);
										}
									else
										{	/* SawBbv/bbv-range.scm 747 */
											BgL_test3064z00_8623 = ((bool_t) 0);
										}
								}
								if (BgL_test3064z00_8623)
									{	/* SawBbv/bbv-range.scm 748 */
										long BgL_noz00_2646;

										BgL_noz00_2646 =
											(
											(long) (
												(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
															((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_82)))->
													BgL_offsetz00)) + (long) CINT(BgL_yz00_83));
										if ((BgL_noz00_2646 <= 0L))
											{	/* SawBbv/bbv-range.scm 750 */
												BgL_bbvzd2vlenzd2_bglt BgL_new1261z00_2649;

												{	/* SawBbv/bbv-range.scm 750 */
													BgL_bbvzd2vlenzd2_bglt BgL_new1264z00_2650;

													BgL_new1264z00_2650 =
														((BgL_bbvzd2vlenzd2_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_bbvzd2vlenzd2_bgl))));
													{	/* SawBbv/bbv-range.scm 750 */
														long BgL_arg2200z00_2651;

														BgL_arg2200z00_2651 =
															BGL_CLASS_NUM
															(BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2);
														BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																BgL_new1264z00_2650), BgL_arg2200z00_2651);
													}
													BgL_new1261z00_2649 = BgL_new1264z00_2650;
												}
												((((BgL_bbvzd2vlenzd2_bglt)
															COBJECT(BgL_new1261z00_2649))->BgL_vecz00) =
													((obj_t) (((BgL_bbvzd2vlenzd2_bglt)
																COBJECT(((BgL_bbvzd2vlenzd2_bglt)
																		BgL_xz00_82)))->BgL_vecz00)), BUNSPEC);
												((((BgL_bbvzd2vlenzd2_bglt)
															COBJECT(BgL_new1261z00_2649))->BgL_offsetz00) =
													((int) (int) (BgL_noz00_2646)), BUNSPEC);
												return ((obj_t) BgL_new1261z00_2649);
											}
										else
											{	/* SawBbv/bbv-range.scm 243 */
												long BgL_arg1808z00_5488;

												{	/* SawBbv/bbv-range.scm 243 */
													obj_t BgL_arg1812z00_5489;

													BgL_arg1812z00_5489 =
														BGl_bigloozd2configzd2zz__configurez00
														(CNST_TABLE_REF(0));
													BgL_arg1808z00_5488 =
														((long) CINT(BgL_arg1812z00_5489) - 2L);
												}
												return BINT((1L << (int) (BgL_arg1808z00_5488)));
									}}
								else
									{	/* SawBbv/bbv-range.scm 753 */
										bool_t BgL_test3071z00_8672;

										if (INTEGERP(BgL_xz00_82))
											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_classz00_5492;

												BgL_classz00_5492 =
													BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
												if (BGL_OBJECTP(BgL_yz00_83))
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_objectz00_bglt BgL_arg1807z00_5494;

														BgL_arg1807z00_5494 =
															(BgL_objectz00_bglt) (BgL_yz00_83);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_idxz00_5500;

																BgL_idxz00_5500 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_5494);
																BgL_test3071z00_8672 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_5500 + 1L)) ==
																	BgL_classz00_5492);
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL_res2580z00_5525;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_oclassz00_5508;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1815z00_5516;
																		long BgL_arg1816z00_5517;

																		BgL_arg1815z00_5516 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_arg1817z00_5518;

																			BgL_arg1817z00_5518 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_5494);
																			BgL_arg1816z00_5517 =
																				(BgL_arg1817z00_5518 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_5508 =
																			VECTOR_REF(BgL_arg1815z00_5516,
																			BgL_arg1816z00_5517);
																	}
																	{	/* SawBbv/bbv-range.scm 85 */
																		bool_t BgL__ortest_1115z00_5509;

																		BgL__ortest_1115z00_5509 =
																			(BgL_classz00_5492 == BgL_oclassz00_5508);
																		if (BgL__ortest_1115z00_5509)
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2580z00_5525 =
																					BgL__ortest_1115z00_5509;
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				long BgL_odepthz00_5510;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1804z00_5511;

																					BgL_arg1804z00_5511 =
																						(BgL_oclassz00_5508);
																					BgL_odepthz00_5510 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_5511);
																				}
																				if ((1L < BgL_odepthz00_5510))
																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1802z00_5513;

																						{	/* SawBbv/bbv-range.scm 85 */
																							obj_t BgL_arg1803z00_5514;

																							BgL_arg1803z00_5514 =
																								(BgL_oclassz00_5508);
																							BgL_arg1802z00_5513 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_5514, 1L);
																						}
																						BgL_res2580z00_5525 =
																							(BgL_arg1802z00_5513 ==
																							BgL_classz00_5492);
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 85 */
																						BgL_res2580z00_5525 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test3071z00_8672 = BgL_res2580z00_5525;
															}
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_test3071z00_8672 = ((bool_t) 0);
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 753 */
												BgL_test3071z00_8672 = ((bool_t) 0);
											}
										if (BgL_test3071z00_8672)
											{
												obj_t BgL_yz00_8698;
												obj_t BgL_xz00_8697;

												BgL_xz00_8697 = BgL_yz00_83;
												BgL_yz00_8698 = BgL_xz00_82;
												BgL_yz00_83 = BgL_yz00_8698;
												BgL_xz00_82 = BgL_xz00_8697;
												goto BGl_zb2rvzb2zzsaw_bbvzd2rangezd2;
											}
										else
											{	/* SawBbv/bbv-range.scm 753 */
												return DOUBLE_TO_REAL(BgL_defz00_84);
											}
									}
							}
					}
			}
		}

	}



/* -rv */
	obj_t BGl_zd2rvzd2zzsaw_bbvzd2rangezd2(obj_t BgL_xz00_85, obj_t BgL_yz00_86,
		double BgL_defz00_87)
	{
		{	/* SawBbv/bbv-range.scm 761 */
			{	/* SawBbv/bbv-range.scm 763 */
				bool_t BgL_test3077z00_8700;

				if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_yz00_86))
					{	/* SawBbv/bbv-range.scm 763 */
						BgL_test3077z00_8700 =
							BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_yz00_86);
					}
				else
					{	/* SawBbv/bbv-range.scm 763 */
						BgL_test3077z00_8700 = ((bool_t) 0);
					}
				if (BgL_test3077z00_8700)
					{	/* SawBbv/bbv-range.scm 763 */
						BGL_TAIL return
							BGl_zb2rvzb2zzsaw_bbvzd2rangezd2(BgL_xz00_85,
							BGl_zd2zd2zz__r4_numbers_6_5z00(BgL_yz00_86, BNIL),
							BgL_defz00_87);
					}
				else
					{	/* SawBbv/bbv-range.scm 765 */
						bool_t BgL_test3079z00_8706;

						{	/* SawBbv/bbv-range.scm 765 */
							bool_t BgL_test3080z00_8707;

							{	/* SawBbv/bbv-range.scm 85 */
								obj_t BgL_classz00_5526;

								BgL_classz00_5526 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
								if (BGL_OBJECTP(BgL_xz00_85))
									{	/* SawBbv/bbv-range.scm 85 */
										BgL_objectz00_bglt BgL_arg1807z00_5528;

										BgL_arg1807z00_5528 = (BgL_objectz00_bglt) (BgL_xz00_85);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawBbv/bbv-range.scm 85 */
												long BgL_idxz00_5534;

												BgL_idxz00_5534 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5528);
												BgL_test3080z00_8707 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5534 + 1L)) == BgL_classz00_5526);
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												bool_t BgL_res2581z00_5559;

												{	/* SawBbv/bbv-range.scm 85 */
													obj_t BgL_oclassz00_5542;

													{	/* SawBbv/bbv-range.scm 85 */
														obj_t BgL_arg1815z00_5550;
														long BgL_arg1816z00_5551;

														BgL_arg1815z00_5550 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawBbv/bbv-range.scm 85 */
															long BgL_arg1817z00_5552;

															BgL_arg1817z00_5552 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5528);
															BgL_arg1816z00_5551 =
																(BgL_arg1817z00_5552 - OBJECT_TYPE);
														}
														BgL_oclassz00_5542 =
															VECTOR_REF(BgL_arg1815z00_5550,
															BgL_arg1816z00_5551);
													}
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL__ortest_1115z00_5543;

														BgL__ortest_1115z00_5543 =
															(BgL_classz00_5526 == BgL_oclassz00_5542);
														if (BgL__ortest_1115z00_5543)
															{	/* SawBbv/bbv-range.scm 85 */
																BgL_res2581z00_5559 = BgL__ortest_1115z00_5543;
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_odepthz00_5544;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1804z00_5545;

																	BgL_arg1804z00_5545 = (BgL_oclassz00_5542);
																	BgL_odepthz00_5544 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_5545);
																}
																if ((1L < BgL_odepthz00_5544))
																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1802z00_5547;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1803z00_5548;

																			BgL_arg1803z00_5548 =
																				(BgL_oclassz00_5542);
																			BgL_arg1802z00_5547 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_5548, 1L);
																		}
																		BgL_res2581z00_5559 =
																			(BgL_arg1802z00_5547 ==
																			BgL_classz00_5526);
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2581z00_5559 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test3080z00_8707 = BgL_res2581z00_5559;
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 85 */
										BgL_test3080z00_8707 = ((bool_t) 0);
									}
							}
							if (BgL_test3080z00_8707)
								{	/* SawBbv/bbv-range.scm 85 */
									obj_t BgL_classz00_5560;

									BgL_classz00_5560 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
									if (BGL_OBJECTP(BgL_yz00_86))
										{	/* SawBbv/bbv-range.scm 85 */
											BgL_objectz00_bglt BgL_arg1807z00_5562;

											BgL_arg1807z00_5562 = (BgL_objectz00_bglt) (BgL_yz00_86);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawBbv/bbv-range.scm 85 */
													long BgL_idxz00_5568;

													BgL_idxz00_5568 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5562);
													BgL_test3079z00_8706 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_5568 + 1L)) == BgL_classz00_5560);
												}
											else
												{	/* SawBbv/bbv-range.scm 85 */
													bool_t BgL_res2582z00_5593;

													{	/* SawBbv/bbv-range.scm 85 */
														obj_t BgL_oclassz00_5576;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_arg1815z00_5584;
															long BgL_arg1816z00_5585;

															BgL_arg1815z00_5584 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_arg1817z00_5586;

																BgL_arg1817z00_5586 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5562);
																BgL_arg1816z00_5585 =
																	(BgL_arg1817z00_5586 - OBJECT_TYPE);
															}
															BgL_oclassz00_5576 =
																VECTOR_REF(BgL_arg1815z00_5584,
																BgL_arg1816z00_5585);
														}
														{	/* SawBbv/bbv-range.scm 85 */
															bool_t BgL__ortest_1115z00_5577;

															BgL__ortest_1115z00_5577 =
																(BgL_classz00_5560 == BgL_oclassz00_5576);
															if (BgL__ortest_1115z00_5577)
																{	/* SawBbv/bbv-range.scm 85 */
																	BgL_res2582z00_5593 =
																		BgL__ortest_1115z00_5577;
																}
															else
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_odepthz00_5578;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1804z00_5579;

																		BgL_arg1804z00_5579 = (BgL_oclassz00_5576);
																		BgL_odepthz00_5578 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_5579);
																	}
																	if ((1L < BgL_odepthz00_5578))
																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1802z00_5581;

																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1803z00_5582;

																				BgL_arg1803z00_5582 =
																					(BgL_oclassz00_5576);
																				BgL_arg1802z00_5581 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_5582, 1L);
																			}
																			BgL_res2582z00_5593 =
																				(BgL_arg1802z00_5581 ==
																				BgL_classz00_5560);
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 85 */
																			BgL_res2582z00_5593 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test3079z00_8706 = BgL_res2582z00_5593;
												}
										}
									else
										{	/* SawBbv/bbv-range.scm 85 */
											BgL_test3079z00_8706 = ((bool_t) 0);
										}
								}
							else
								{	/* SawBbv/bbv-range.scm 765 */
									BgL_test3079z00_8706 = ((bool_t) 0);
								}
						}
						if (BgL_test3079z00_8706)
							{	/* SawBbv/bbv-range.scm 765 */
								if (
									((((BgL_bbvzd2vlenzd2_bglt) COBJECT(
													((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_85)))->
											BgL_vecz00) ==
										(((BgL_bbvzd2vlenzd2_bglt) COBJECT(((BgL_bbvzd2vlenzd2_bglt)
														BgL_yz00_86)))->BgL_vecz00)))
									{	/* SawBbv/bbv-range.scm 767 */
										long BgL_noz00_2668;

										BgL_noz00_2668 =
											(
											(long) (
												(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
															((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_85)))->
													BgL_offsetz00)) -
											(long) ((((BgL_bbvzd2vlenzd2_bglt)
														COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_yz00_86)))->
													BgL_offsetz00)));
										{	/* SawBbv/bbv-range.scm 768 */
											BgL_bbvzd2vlenzd2_bglt BgL_new1265z00_2670;

											{	/* SawBbv/bbv-range.scm 768 */
												BgL_bbvzd2vlenzd2_bglt BgL_new1268z00_2671;

												BgL_new1268z00_2671 =
													((BgL_bbvzd2vlenzd2_bglt)
													BOBJECT(GC_MALLOC(sizeof(struct
																BgL_bbvzd2vlenzd2_bgl))));
												{	/* SawBbv/bbv-range.scm 768 */
													long BgL_arg2213z00_2672;

													BgL_arg2213z00_2672 =
														BGL_CLASS_NUM
														(BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2);
													BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
															BgL_new1268z00_2671), BgL_arg2213z00_2672);
												}
												BgL_new1265z00_2670 = BgL_new1268z00_2671;
											}
											((((BgL_bbvzd2vlenzd2_bglt)
														COBJECT(BgL_new1265z00_2670))->BgL_vecz00) =
												((obj_t) (((BgL_bbvzd2vlenzd2_bglt)
															COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_85)))->
														BgL_vecz00)), BUNSPEC);
											((((BgL_bbvzd2vlenzd2_bglt)
														COBJECT(BgL_new1265z00_2670))->BgL_offsetz00) =
												((int) (int) (BgL_noz00_2668)), BUNSPEC);
											return ((obj_t) BgL_new1265z00_2670);
										}
									}
								else
									{	/* SawBbv/bbv-range.scm 243 */
										long BgL_arg1808z00_5607;

										{	/* SawBbv/bbv-range.scm 243 */
											obj_t BgL_arg1812z00_5608;

											BgL_arg1812z00_5608 =
												BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
												(0));
											BgL_arg1808z00_5607 =
												((long) CINT(BgL_arg1812z00_5608) - 2L);
										}
										return BINT((1L << (int) (BgL_arg1808z00_5607)));
							}}
						else
							{	/* SawBbv/bbv-range.scm 771 */
								bool_t BgL_test3090z00_8782;

								{	/* SawBbv/bbv-range.scm 771 */
									bool_t BgL_test3091z00_8783;

									{	/* SawBbv/bbv-range.scm 85 */
										obj_t BgL_classz00_5611;

										BgL_classz00_5611 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
										if (BGL_OBJECTP(BgL_xz00_85))
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_objectz00_bglt BgL_arg1807z00_5613;

												BgL_arg1807z00_5613 =
													(BgL_objectz00_bglt) (BgL_xz00_85);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_idxz00_5619;

														BgL_idxz00_5619 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5613);
														BgL_test3091z00_8783 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_5619 + 1L)) == BgL_classz00_5611);
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL_res2583z00_5644;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_oclassz00_5627;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1815z00_5635;
																long BgL_arg1816z00_5636;

																BgL_arg1815z00_5635 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_arg1817z00_5637;

																	BgL_arg1817z00_5637 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5613);
																	BgL_arg1816z00_5636 =
																		(BgL_arg1817z00_5637 - OBJECT_TYPE);
																}
																BgL_oclassz00_5627 =
																	VECTOR_REF(BgL_arg1815z00_5635,
																	BgL_arg1816z00_5636);
															}
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL__ortest_1115z00_5628;

																BgL__ortest_1115z00_5628 =
																	(BgL_classz00_5611 == BgL_oclassz00_5627);
																if (BgL__ortest_1115z00_5628)
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2583z00_5644 =
																			BgL__ortest_1115z00_5628;
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		long BgL_odepthz00_5629;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1804z00_5630;

																			BgL_arg1804z00_5630 =
																				(BgL_oclassz00_5627);
																			BgL_odepthz00_5629 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_5630);
																		}
																		if ((1L < BgL_odepthz00_5629))
																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1802z00_5632;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1803z00_5633;

																					BgL_arg1803z00_5633 =
																						(BgL_oclassz00_5627);
																					BgL_arg1802z00_5632 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_5633, 1L);
																				}
																				BgL_res2583z00_5644 =
																					(BgL_arg1802z00_5632 ==
																					BgL_classz00_5611);
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2583z00_5644 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test3091z00_8783 = BgL_res2583z00_5644;
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_test3091z00_8783 = ((bool_t) 0);
											}
									}
									if (BgL_test3091z00_8783)
										{	/* SawBbv/bbv-range.scm 771 */
											BgL_test3090z00_8782 = INTEGERP(BgL_yz00_86);
										}
									else
										{	/* SawBbv/bbv-range.scm 771 */
											BgL_test3090z00_8782 = ((bool_t) 0);
										}
								}
								if (BgL_test3090z00_8782)
									{	/* SawBbv/bbv-range.scm 772 */
										long BgL_noz00_2679;

										BgL_noz00_2679 =
											(
											(long) (
												(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
															((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_85)))->
													BgL_offsetz00)) - (long) CINT(BgL_yz00_86));
										if ((BgL_noz00_2679 <= 0L))
											{	/* SawBbv/bbv-range.scm 774 */
												BgL_bbvzd2vlenzd2_bglt BgL_new1269z00_2682;

												{	/* SawBbv/bbv-range.scm 774 */
													BgL_bbvzd2vlenzd2_bglt BgL_new1272z00_2683;

													BgL_new1272z00_2683 =
														((BgL_bbvzd2vlenzd2_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_bbvzd2vlenzd2_bgl))));
													{	/* SawBbv/bbv-range.scm 774 */
														long BgL_arg2221z00_2684;

														BgL_arg2221z00_2684 =
															BGL_CLASS_NUM
															(BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2);
														BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																BgL_new1272z00_2683), BgL_arg2221z00_2684);
													}
													BgL_new1269z00_2682 = BgL_new1272z00_2683;
												}
												((((BgL_bbvzd2vlenzd2_bglt)
															COBJECT(BgL_new1269z00_2682))->BgL_vecz00) =
													((obj_t) (((BgL_bbvzd2vlenzd2_bglt)
																COBJECT(((BgL_bbvzd2vlenzd2_bglt)
																		BgL_xz00_85)))->BgL_vecz00)), BUNSPEC);
												((((BgL_bbvzd2vlenzd2_bglt)
															COBJECT(BgL_new1269z00_2682))->BgL_offsetz00) =
													((int) (int) (BgL_noz00_2679)), BUNSPEC);
												return ((obj_t) BgL_new1269z00_2682);
											}
										else
											{	/* SawBbv/bbv-range.scm 776 */
												obj_t BgL_arg2222z00_2685;

												{	/* SawBbv/bbv-range.scm 776 */
													long BgL_arg2223z00_2686;

													{	/* SawBbv/bbv-range.scm 243 */
														long BgL_arg1808z00_5653;

														{	/* SawBbv/bbv-range.scm 243 */
															obj_t BgL_arg1812z00_5654;

															BgL_arg1812z00_5654 =
																BGl_bigloozd2configzd2zz__configurez00
																(CNST_TABLE_REF(0));
															BgL_arg1808z00_5653 =
																((long) CINT(BgL_arg1812z00_5654) - 2L);
														}
														BgL_arg2223z00_2686 =
															(1L << (int) (BgL_arg1808z00_5653));
													}
													{	/* SawBbv/bbv-range.scm 776 */
														obj_t BgL_za71za7_5657;

														BgL_za71za7_5657 = BINT(BgL_arg2223z00_2686);
														{	/* SawBbv/bbv-range.scm 776 */
															obj_t BgL_tmpz00_5659;

															BgL_tmpz00_5659 = BINT(0L);
															if (BGL_SUBFX_OV(BgL_za71za7_5657, BgL_yz00_86,
																	BgL_tmpz00_5659))
																{	/* SawBbv/bbv-range.scm 776 */
																	BgL_arg2222z00_2685 =
																		bgl_bignum_sub(bgl_long_to_bignum(
																			(long) CINT(BgL_za71za7_5657)),
																		bgl_long_to_bignum(
																			(long) CINT(BgL_yz00_86)));
																}
															else
																{	/* SawBbv/bbv-range.scm 776 */
																	BgL_arg2222z00_2685 = BgL_tmpz00_5659;
																}
														}
													}
												}
												return
													BGl_zd2ze3rangez31zzsaw_bbvzd2rangezd2
													(BgL_arg2222z00_2685);
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 777 */
										bool_t BgL_test3098z00_8840;

										if (INTEGERP(BgL_xz00_85))
											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_classz00_5667;

												BgL_classz00_5667 =
													BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
												if (BGL_OBJECTP(BgL_yz00_86))
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_objectz00_bglt BgL_arg1807z00_5669;

														BgL_arg1807z00_5669 =
															(BgL_objectz00_bglt) (BgL_yz00_86);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_idxz00_5675;

																BgL_idxz00_5675 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_5669);
																BgL_test3098z00_8840 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_5675 + 1L)) ==
																	BgL_classz00_5667);
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL_res2584z00_5700;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_oclassz00_5683;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1815z00_5691;
																		long BgL_arg1816z00_5692;

																		BgL_arg1815z00_5691 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_arg1817z00_5693;

																			BgL_arg1817z00_5693 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_5669);
																			BgL_arg1816z00_5692 =
																				(BgL_arg1817z00_5693 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_5683 =
																			VECTOR_REF(BgL_arg1815z00_5691,
																			BgL_arg1816z00_5692);
																	}
																	{	/* SawBbv/bbv-range.scm 85 */
																		bool_t BgL__ortest_1115z00_5684;

																		BgL__ortest_1115z00_5684 =
																			(BgL_classz00_5667 == BgL_oclassz00_5683);
																		if (BgL__ortest_1115z00_5684)
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2584z00_5700 =
																					BgL__ortest_1115z00_5684;
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				long BgL_odepthz00_5685;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1804z00_5686;

																					BgL_arg1804z00_5686 =
																						(BgL_oclassz00_5683);
																					BgL_odepthz00_5685 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_5686);
																				}
																				if ((1L < BgL_odepthz00_5685))
																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1802z00_5688;

																						{	/* SawBbv/bbv-range.scm 85 */
																							obj_t BgL_arg1803z00_5689;

																							BgL_arg1803z00_5689 =
																								(BgL_oclassz00_5683);
																							BgL_arg1802z00_5688 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_5689, 1L);
																						}
																						BgL_res2584z00_5700 =
																							(BgL_arg1802z00_5688 ==
																							BgL_classz00_5667);
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 85 */
																						BgL_res2584z00_5700 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test3098z00_8840 = BgL_res2584z00_5700;
															}
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_test3098z00_8840 = ((bool_t) 0);
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 777 */
												BgL_test3098z00_8840 = ((bool_t) 0);
											}
										if (BgL_test3098z00_8840)
											{	/* SawBbv/bbv-range.scm 779 */
												obj_t BgL_arg2227z00_2691;

												{	/* SawBbv/bbv-range.scm 779 */
													long BgL_arg2228z00_2692;

													{	/* SawBbv/bbv-range.scm 243 */
														long BgL_arg1808z00_5705;

														{	/* SawBbv/bbv-range.scm 243 */
															obj_t BgL_arg1812z00_5706;

															BgL_arg1812z00_5706 =
																BGl_bigloozd2configzd2zz__configurez00
																(CNST_TABLE_REF(0));
															BgL_arg1808z00_5705 =
																((long) CINT(BgL_arg1812z00_5706) - 2L);
														}
														BgL_arg2228z00_2692 =
															(1L << (int) (BgL_arg1808z00_5705));
													}
													{	/* SawBbv/bbv-range.scm 779 */
														obj_t BgL_za72za7_5710;

														BgL_za72za7_5710 = BINT(BgL_arg2228z00_2692);
														{	/* SawBbv/bbv-range.scm 779 */
															obj_t BgL_tmpz00_5711;

															BgL_tmpz00_5711 = BINT(0L);
															if (BGL_SUBFX_OV(BgL_xz00_85, BgL_za72za7_5710,
																	BgL_tmpz00_5711))
																{	/* SawBbv/bbv-range.scm 779 */
																	BgL_arg2227z00_2691 =
																		bgl_bignum_sub(bgl_long_to_bignum(
																			(long) CINT(BgL_xz00_85)),
																		bgl_long_to_bignum(
																			(long) CINT(BgL_za72za7_5710)));
																}
															else
																{	/* SawBbv/bbv-range.scm 779 */
																	BgL_arg2227z00_2691 = BgL_tmpz00_5711;
																}
														}
													}
												}
												return
													BGl_zd2ze3rangez31zzsaw_bbvzd2rangezd2
													(BgL_arg2227z00_2691);
											}
										else
											{	/* SawBbv/bbv-range.scm 777 */
												return DOUBLE_TO_REAL(BgL_defz00_87);
											}
									}
							}
					}
			}
		}

	}



/* *rv */
	obj_t BGl_za2rvza2zzsaw_bbvzd2rangezd2(obj_t BgL_xz00_88, obj_t BgL_yz00_89,
		obj_t BgL_defz00_90)
	{
		{	/* SawBbv/bbv-range.scm 786 */
		BGl_za2rvza2zzsaw_bbvzd2rangezd2:
			{	/* SawBbv/bbv-range.scm 788 */
				bool_t BgL_test3105z00_8882;

				if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_xz00_88))
					{	/* SawBbv/bbv-range.scm 788 */
						BgL_test3105z00_8882 =
							BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_yz00_89);
					}
				else
					{	/* SawBbv/bbv-range.scm 788 */
						BgL_test3105z00_8882 = ((bool_t) 0);
					}
				if (BgL_test3105z00_8882)
					{	/* SawBbv/bbv-range.scm 789 */
						obj_t BgL_arg2232z00_2700;

						{	/* SawBbv/bbv-range.scm 789 */
							bool_t BgL_test3107z00_8886;

							if (INTEGERP(BgL_xz00_88))
								{	/* SawBbv/bbv-range.scm 789 */
									BgL_test3107z00_8886 = INTEGERP(BgL_yz00_89);
								}
							else
								{	/* SawBbv/bbv-range.scm 789 */
									BgL_test3107z00_8886 = ((bool_t) 0);
								}
							if (BgL_test3107z00_8886)
								{	/* SawBbv/bbv-range.scm 789 */
									obj_t BgL_tmpz00_5721;

									BgL_tmpz00_5721 = BINT(0L);
									{	/* SawBbv/bbv-range.scm 789 */
										bool_t BgL_test3109z00_8891;

										{	/* SawBbv/bbv-range.scm 789 */
											long BgL_tmpz00_8892;

											BgL_tmpz00_8892 = (long) CINT(BgL_yz00_89);
											BgL_test3109z00_8891 =
												BGL_MULFX_OV(BgL_xz00_88, BgL_tmpz00_8892,
												BgL_tmpz00_5721);
										}
										if (BgL_test3109z00_8891)
											{	/* SawBbv/bbv-range.scm 789 */
												BgL_arg2232z00_2700 =
													bgl_bignum_mul(bgl_long_to_bignum(
														(long) CINT(BgL_xz00_88)),
													bgl_long_to_bignum((long) CINT(BgL_yz00_89)));
											}
										else
											{	/* SawBbv/bbv-range.scm 789 */
												BgL_arg2232z00_2700 = BgL_tmpz00_5721;
											}
									}
								}
							else
								{	/* SawBbv/bbv-range.scm 789 */
									BgL_arg2232z00_2700 =
										BGl_2za2za2zz__r4_numbers_6_5z00(BgL_xz00_88, BgL_yz00_89);
								}
						}
						return BGl_zd2ze3rangez31zzsaw_bbvzd2rangezd2(BgL_arg2232z00_2700);
					}
				else
					{	/* SawBbv/bbv-range.scm 790 */
						bool_t BgL_test3110z00_8902;

						{	/* SawBbv/bbv-range.scm 790 */
							bool_t BgL_test3111z00_8903;

							{	/* SawBbv/bbv-range.scm 85 */
								obj_t BgL_classz00_5729;

								BgL_classz00_5729 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
								if (BGL_OBJECTP(BgL_xz00_88))
									{	/* SawBbv/bbv-range.scm 85 */
										BgL_objectz00_bglt BgL_arg1807z00_5731;

										BgL_arg1807z00_5731 = (BgL_objectz00_bglt) (BgL_xz00_88);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawBbv/bbv-range.scm 85 */
												long BgL_idxz00_5737;

												BgL_idxz00_5737 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5731);
												BgL_test3111z00_8903 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5737 + 1L)) == BgL_classz00_5729);
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												bool_t BgL_res2585z00_5762;

												{	/* SawBbv/bbv-range.scm 85 */
													obj_t BgL_oclassz00_5745;

													{	/* SawBbv/bbv-range.scm 85 */
														obj_t BgL_arg1815z00_5753;
														long BgL_arg1816z00_5754;

														BgL_arg1815z00_5753 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawBbv/bbv-range.scm 85 */
															long BgL_arg1817z00_5755;

															BgL_arg1817z00_5755 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5731);
															BgL_arg1816z00_5754 =
																(BgL_arg1817z00_5755 - OBJECT_TYPE);
														}
														BgL_oclassz00_5745 =
															VECTOR_REF(BgL_arg1815z00_5753,
															BgL_arg1816z00_5754);
													}
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL__ortest_1115z00_5746;

														BgL__ortest_1115z00_5746 =
															(BgL_classz00_5729 == BgL_oclassz00_5745);
														if (BgL__ortest_1115z00_5746)
															{	/* SawBbv/bbv-range.scm 85 */
																BgL_res2585z00_5762 = BgL__ortest_1115z00_5746;
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_odepthz00_5747;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1804z00_5748;

																	BgL_arg1804z00_5748 = (BgL_oclassz00_5745);
																	BgL_odepthz00_5747 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_5748);
																}
																if ((1L < BgL_odepthz00_5747))
																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1802z00_5750;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1803z00_5751;

																			BgL_arg1803z00_5751 =
																				(BgL_oclassz00_5745);
																			BgL_arg1802z00_5750 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_5751, 1L);
																		}
																		BgL_res2585z00_5762 =
																			(BgL_arg1802z00_5750 ==
																			BgL_classz00_5729);
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2585z00_5762 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test3111z00_8903 = BgL_res2585z00_5762;
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 85 */
										BgL_test3111z00_8903 = ((bool_t) 0);
									}
							}
							if (BgL_test3111z00_8903)
								{	/* SawBbv/bbv-range.scm 790 */
									BgL_test3110z00_8902 = INTEGERP(BgL_yz00_89);
								}
							else
								{	/* SawBbv/bbv-range.scm 790 */
									BgL_test3110z00_8902 = ((bool_t) 0);
								}
						}
						if (BgL_test3110z00_8902)
							{	/* SawBbv/bbv-range.scm 791 */
								long BgL_noz00_2704;

								BgL_noz00_2704 =
									(
									(long) (
										(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
													((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_88)))->
											BgL_offsetz00)) * (long) CINT(BgL_yz00_89));
								if ((BgL_noz00_2704 < 0L))
									{	/* SawBbv/bbv-range.scm 793 */
										BgL_bbvzd2vlenzd2_bglt BgL_new1273z00_2707;

										{	/* SawBbv/bbv-range.scm 793 */
											BgL_bbvzd2vlenzd2_bglt BgL_new1276z00_2708;

											BgL_new1276z00_2708 =
												((BgL_bbvzd2vlenzd2_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_bbvzd2vlenzd2_bgl))));
											{	/* SawBbv/bbv-range.scm 793 */
												long BgL_arg2237z00_2709;

												BgL_arg2237z00_2709 =
													BGL_CLASS_NUM(BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1276z00_2708),
													BgL_arg2237z00_2709);
											}
											BgL_new1273z00_2707 = BgL_new1276z00_2708;
										}
										((((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_new1273z00_2707))->
												BgL_vecz00) =
											((obj_t) (((BgL_bbvzd2vlenzd2_bglt)
														COBJECT(((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_88)))->
													BgL_vecz00)), BUNSPEC);
										((((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_new1273z00_2707))->
												BgL_offsetz00) =
											((int) (int) (BgL_noz00_2704)), BUNSPEC);
										return ((obj_t) BgL_new1273z00_2707);
									}
								else
									{	/* SawBbv/bbv-range.scm 243 */
										long BgL_arg1808z00_5771;

										{	/* SawBbv/bbv-range.scm 243 */
											obj_t BgL_arg1812z00_5772;

											BgL_arg1812z00_5772 =
												BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
												(0));
											BgL_arg1808z00_5771 =
												((long) CINT(BgL_arg1812z00_5772) - 2L);
										}
										return BINT((1L << (int) (BgL_arg1808z00_5771)));
							}}
						else
							{	/* SawBbv/bbv-range.scm 796 */
								bool_t BgL_test3117z00_8951;

								{	/* SawBbv/bbv-range.scm 796 */
									bool_t BgL_test3118z00_8952;

									{	/* SawBbv/bbv-range.scm 85 */
										obj_t BgL_classz00_5775;

										BgL_classz00_5775 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
										if (BGL_OBJECTP(BgL_xz00_88))
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_objectz00_bglt BgL_arg1807z00_5777;

												BgL_arg1807z00_5777 =
													(BgL_objectz00_bglt) (BgL_xz00_88);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_idxz00_5783;

														BgL_idxz00_5783 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5777);
														BgL_test3118z00_8952 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_5783 + 1L)) == BgL_classz00_5775);
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL_res2586z00_5808;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_oclassz00_5791;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1815z00_5799;
																long BgL_arg1816z00_5800;

																BgL_arg1815z00_5799 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_arg1817z00_5801;

																	BgL_arg1817z00_5801 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5777);
																	BgL_arg1816z00_5800 =
																		(BgL_arg1817z00_5801 - OBJECT_TYPE);
																}
																BgL_oclassz00_5791 =
																	VECTOR_REF(BgL_arg1815z00_5799,
																	BgL_arg1816z00_5800);
															}
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL__ortest_1115z00_5792;

																BgL__ortest_1115z00_5792 =
																	(BgL_classz00_5775 == BgL_oclassz00_5791);
																if (BgL__ortest_1115z00_5792)
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2586z00_5808 =
																			BgL__ortest_1115z00_5792;
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		long BgL_odepthz00_5793;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1804z00_5794;

																			BgL_arg1804z00_5794 =
																				(BgL_oclassz00_5791);
																			BgL_odepthz00_5793 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_5794);
																		}
																		if ((1L < BgL_odepthz00_5793))
																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1802z00_5796;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1803z00_5797;

																					BgL_arg1803z00_5797 =
																						(BgL_oclassz00_5791);
																					BgL_arg1802z00_5796 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_5797, 1L);
																				}
																				BgL_res2586z00_5808 =
																					(BgL_arg1802z00_5796 ==
																					BgL_classz00_5775);
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2586z00_5808 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test3118z00_8952 = BgL_res2586z00_5808;
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												BgL_test3118z00_8952 = ((bool_t) 0);
											}
									}
									if (BgL_test3118z00_8952)
										{	/* SawBbv/bbv-range.scm 796 */
											BgL_test3117z00_8951 = INTEGERP(BgL_yz00_89);
										}
									else
										{	/* SawBbv/bbv-range.scm 796 */
											BgL_test3117z00_8951 = ((bool_t) 0);
										}
								}
								if (BgL_test3117z00_8951)
									{	/* SawBbv/bbv-range.scm 797 */
										long BgL_noz00_2713;

										BgL_noz00_2713 =
											(
											(long) (
												(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
															((BgL_bbvzd2vlenzd2_bglt) BgL_xz00_88)))->
													BgL_offsetz00)) * (long) CINT(BgL_yz00_89));
										if ((BgL_noz00_2713 < 0L))
											{	/* SawBbv/bbv-range.scm 799 */
												BgL_bbvzd2vlenzd2_bglt BgL_duplicated1279z00_2715;
												BgL_bbvzd2vlenzd2_bglt BgL_new1277z00_2716;

												{	/* SawBbv/bbv-range.scm 799 */
													obj_t BgL_aux2598z00_6368;

													BgL_aux2598z00_6368 = BgL_yz00_89;
													{
														obj_t BgL_auxz00_8983;

														BgL_auxz00_8983 =
															BGl_typezd2errorzd2zz__errorz00
															(BGl_string2601z00zzsaw_bbvzd2rangezd2,
															BINT(29917L),
															BGl_string2602z00zzsaw_bbvzd2rangezd2,
															BGl_string2603z00zzsaw_bbvzd2rangezd2,
															BgL_aux2598z00_6368);
														FAILURE(BgL_auxz00_8983, BFALSE, BFALSE);
													}
												}
												{	/* SawBbv/bbv-range.scm 799 */
													BgL_bbvzd2vlenzd2_bglt BgL_new1280z00_2717;

													BgL_new1280z00_2717 =
														((BgL_bbvzd2vlenzd2_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_bbvzd2vlenzd2_bgl))));
													{	/* SawBbv/bbv-range.scm 799 */
														long BgL_arg2242z00_2718;

														BgL_arg2242z00_2718 =
															BGL_CLASS_NUM
															(BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2);
														BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																BgL_new1280z00_2717), BgL_arg2242z00_2718);
													}
													BgL_new1277z00_2716 = BgL_new1280z00_2717;
												}
												((((BgL_bbvzd2vlenzd2_bglt)
															COBJECT(BgL_new1277z00_2716))->BgL_vecz00) =
													((obj_t) (((BgL_bbvzd2vlenzd2_bglt)
																COBJECT(BgL_duplicated1279z00_2715))->
															BgL_vecz00)), BUNSPEC);
												((((BgL_bbvzd2vlenzd2_bglt)
															COBJECT(BgL_new1277z00_2716))->BgL_offsetz00) =
													((int) (int) (BgL_noz00_2713)), BUNSPEC);
												return ((obj_t) BgL_new1277z00_2716);
											}
										else
											{	/* SawBbv/bbv-range.scm 798 */
												return
													BGL_REAL_CNST(BGl_real2600z00zzsaw_bbvzd2rangezd2);
											}
									}
								else
									{	/* SawBbv/bbv-range.scm 802 */
										bool_t BgL_test3124z00_8996;

										if (INTEGERP(BgL_xz00_88))
											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_classz00_5817;

												BgL_classz00_5817 =
													BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
												if (BGL_OBJECTP(BgL_yz00_89))
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_objectz00_bglt BgL_arg1807z00_5819;

														BgL_arg1807z00_5819 =
															(BgL_objectz00_bglt) (BgL_yz00_89);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_idxz00_5825;

																BgL_idxz00_5825 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_5819);
																BgL_test3124z00_8996 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_5825 + 1L)) ==
																	BgL_classz00_5817);
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																bool_t BgL_res2587z00_5850;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_oclassz00_5833;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1815z00_5841;
																		long BgL_arg1816z00_5842;

																		BgL_arg1815z00_5841 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawBbv/bbv-range.scm 85 */
																			long BgL_arg1817z00_5843;

																			BgL_arg1817z00_5843 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_5819);
																			BgL_arg1816z00_5842 =
																				(BgL_arg1817z00_5843 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_5833 =
																			VECTOR_REF(BgL_arg1815z00_5841,
																			BgL_arg1816z00_5842);
																	}
																	{	/* SawBbv/bbv-range.scm 85 */
																		bool_t BgL__ortest_1115z00_5834;

																		BgL__ortest_1115z00_5834 =
																			(BgL_classz00_5817 == BgL_oclassz00_5833);
																		if (BgL__ortest_1115z00_5834)
																			{	/* SawBbv/bbv-range.scm 85 */
																				BgL_res2587z00_5850 =
																					BgL__ortest_1115z00_5834;
																			}
																		else
																			{	/* SawBbv/bbv-range.scm 85 */
																				long BgL_odepthz00_5835;

																				{	/* SawBbv/bbv-range.scm 85 */
																					obj_t BgL_arg1804z00_5836;

																					BgL_arg1804z00_5836 =
																						(BgL_oclassz00_5833);
																					BgL_odepthz00_5835 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_5836);
																				}
																				if ((1L < BgL_odepthz00_5835))
																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_arg1802z00_5838;

																						{	/* SawBbv/bbv-range.scm 85 */
																							obj_t BgL_arg1803z00_5839;

																							BgL_arg1803z00_5839 =
																								(BgL_oclassz00_5833);
																							BgL_arg1802z00_5838 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_5839, 1L);
																						}
																						BgL_res2587z00_5850 =
																							(BgL_arg1802z00_5838 ==
																							BgL_classz00_5817);
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 85 */
																						BgL_res2587z00_5850 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test3124z00_8996 = BgL_res2587z00_5850;
															}
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_test3124z00_8996 = ((bool_t) 0);
													}
											}
										else
											{	/* SawBbv/bbv-range.scm 802 */
												BgL_test3124z00_8996 = ((bool_t) 0);
											}
										if (BgL_test3124z00_8996)
											{
												obj_t BgL_yz00_9022;
												obj_t BgL_xz00_9021;

												BgL_xz00_9021 = BgL_yz00_89;
												BgL_yz00_9022 = BgL_xz00_88;
												BgL_yz00_89 = BgL_yz00_9022;
												BgL_xz00_88 = BgL_xz00_9021;
												goto BGl_za2rvza2zzsaw_bbvzd2rangezd2;
											}
										else
											{	/* SawBbv/bbv-range.scm 802 */
												return BgL_defz00_90;
											}
									}
							}
					}
			}
		}

	}



/* bbv-range-add */
	BGL_EXPORTED_DEF BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2addz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_xz00_91, BgL_bbvzd2rangezd2_bglt BgL_yz00_92)
	{
		{	/* SawBbv/bbv-range.scm 810 */
			{	/* SawBbv/bbv-range.scm 811 */
				obj_t BgL_ylz00_2726;
				obj_t BgL_yuz00_2727;
				obj_t BgL_xlz00_2728;
				obj_t BgL_xuz00_2729;

				BgL_ylz00_2726 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_92))->BgL_loz00);
				BgL_yuz00_2727 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_92))->BgL_upz00);
				BgL_xlz00_2728 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_91))->BgL_loz00);
				BgL_xuz00_2729 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_91))->BgL_upz00);
				{	/* SawBbv/bbv-range.scm 815 */
					BgL_bbvzd2rangezd2_bglt BgL_new1282z00_2730;

					{	/* SawBbv/bbv-range.scm 816 */
						BgL_bbvzd2rangezd2_bglt BgL_new1281z00_2731;

						BgL_new1281z00_2731 =
							((BgL_bbvzd2rangezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_bbvzd2rangezd2_bgl))));
						{	/* SawBbv/bbv-range.scm 816 */
							long BgL_arg2246z00_2732;

							BgL_arg2246z00_2732 =
								BGL_CLASS_NUM(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1281z00_2731),
								BgL_arg2246z00_2732);
						}
						BgL_new1282z00_2730 = BgL_new1281z00_2731;
					}
					((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1282z00_2730))->
							BgL_loz00) =
						((obj_t) BGl_zb2rvzb2zzsaw_bbvzd2rangezd2(BgL_xlz00_2728,
								BgL_ylz00_2726, (-BGL_INFINITY))), BUNSPEC);
					((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1282z00_2730))->
							BgL_upz00) =
						((obj_t) BGl_zb2rvzb2zzsaw_bbvzd2rangezd2(BgL_xuz00_2729,
								BgL_yuz00_2727, BGL_INFINITY)), BUNSPEC);
					return BgL_new1282z00_2730;
				}
			}
		}

	}



/* &bbv-range-add */
	BgL_bbvzd2rangezd2_bglt BGl_z62bbvzd2rangezd2addz62zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6293, obj_t BgL_xz00_6294, obj_t BgL_yz00_6295)
	{
		{	/* SawBbv/bbv-range.scm 810 */
			return
				BGl_bbvzd2rangezd2addz00zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6294),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6295));
		}

	}



/* bbv-range-sub */
	BGL_EXPORTED_DEF BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2subz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_xz00_93, BgL_bbvzd2rangezd2_bglt BgL_yz00_94)
	{
		{	/* SawBbv/bbv-range.scm 822 */
			{	/* SawBbv/bbv-range.scm 823 */
				obj_t BgL_ylz00_2733;
				obj_t BgL_yuz00_2734;
				obj_t BgL_xlz00_2735;
				obj_t BgL_xuz00_2736;

				BgL_ylz00_2733 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_94))->BgL_loz00);
				BgL_yuz00_2734 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_94))->BgL_upz00);
				BgL_xlz00_2735 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_93))->BgL_loz00);
				BgL_xuz00_2736 =
					(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_93))->BgL_upz00);
				{	/* SawBbv/bbv-range.scm 827 */
					BgL_bbvzd2rangezd2_bglt BgL_new1284z00_2737;

					{	/* SawBbv/bbv-range.scm 828 */
						BgL_bbvzd2rangezd2_bglt BgL_new1283z00_2738;

						BgL_new1283z00_2738 =
							((BgL_bbvzd2rangezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_bbvzd2rangezd2_bgl))));
						{	/* SawBbv/bbv-range.scm 828 */
							long BgL_arg2247z00_2739;

							BgL_arg2247z00_2739 =
								BGL_CLASS_NUM(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1283z00_2738),
								BgL_arg2247z00_2739);
						}
						BgL_new1284z00_2737 = BgL_new1283z00_2738;
					}
					((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1284z00_2737))->
							BgL_loz00) =
						((obj_t) BGl_zd2rvzd2zzsaw_bbvzd2rangezd2(BgL_xlz00_2735,
								BgL_yuz00_2734, (-BGL_INFINITY))), BUNSPEC);
					((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1284z00_2737))->
							BgL_upz00) =
						((obj_t) BGl_zd2rvzd2zzsaw_bbvzd2rangezd2(BgL_xuz00_2736,
								BgL_ylz00_2733, BGL_INFINITY)), BUNSPEC);
					return BgL_new1284z00_2737;
				}
			}
		}

	}



/* &bbv-range-sub */
	BgL_bbvzd2rangezd2_bglt BGl_z62bbvzd2rangezd2subz62zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6296, obj_t BgL_xz00_6297, obj_t BgL_yz00_6298)
	{
		{	/* SawBbv/bbv-range.scm 822 */
			return
				BGl_bbvzd2rangezd2subz00zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6297),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6298));
		}

	}



/* bbv-range-mul */
	BGL_EXPORTED_DEF BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2mulz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_xz00_95, BgL_bbvzd2rangezd2_bglt BgL_yz00_96)
	{
		{	/* SawBbv/bbv-range.scm 834 */
			{	/* SawBbv/bbv-range.scm 835 */
				obj_t BgL_ylz00_2740;

				{	/* SawBbv/bbv-range.scm 295 */
					bool_t BgL_test3130z00_9053;

					{	/* SawBbv/bbv-range.scm 295 */
						obj_t BgL_arg1848z00_5867;

						BgL_arg1848z00_5867 =
							(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_96))->BgL_loz00);
						{	/* SawBbv/bbv-range.scm 85 */
							obj_t BgL_classz00_5869;

							BgL_classz00_5869 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
							if (BGL_OBJECTP(BgL_arg1848z00_5867))
								{	/* SawBbv/bbv-range.scm 85 */
									BgL_objectz00_bglt BgL_arg1807z00_5871;

									BgL_arg1807z00_5871 =
										(BgL_objectz00_bglt) (BgL_arg1848z00_5867);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawBbv/bbv-range.scm 85 */
											long BgL_idxz00_5877;

											BgL_idxz00_5877 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5871);
											BgL_test3130z00_9053 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_5877 + 1L)) == BgL_classz00_5869);
										}
									else
										{	/* SawBbv/bbv-range.scm 85 */
											bool_t BgL_res2588z00_5902;

											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_oclassz00_5885;

												{	/* SawBbv/bbv-range.scm 85 */
													obj_t BgL_arg1815z00_5893;
													long BgL_arg1816z00_5894;

													BgL_arg1815z00_5893 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_arg1817z00_5895;

														BgL_arg1817z00_5895 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5871);
														BgL_arg1816z00_5894 =
															(BgL_arg1817z00_5895 - OBJECT_TYPE);
													}
													BgL_oclassz00_5885 =
														VECTOR_REF(BgL_arg1815z00_5893,
														BgL_arg1816z00_5894);
												}
												{	/* SawBbv/bbv-range.scm 85 */
													bool_t BgL__ortest_1115z00_5886;

													BgL__ortest_1115z00_5886 =
														(BgL_classz00_5869 == BgL_oclassz00_5885);
													if (BgL__ortest_1115z00_5886)
														{	/* SawBbv/bbv-range.scm 85 */
															BgL_res2588z00_5902 = BgL__ortest_1115z00_5886;
														}
													else
														{	/* SawBbv/bbv-range.scm 85 */
															long BgL_odepthz00_5887;

															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1804z00_5888;

																BgL_arg1804z00_5888 = (BgL_oclassz00_5885);
																BgL_odepthz00_5887 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_5888);
															}
															if ((1L < BgL_odepthz00_5887))
																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1802z00_5890;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1803z00_5891;

																		BgL_arg1803z00_5891 = (BgL_oclassz00_5885);
																		BgL_arg1802z00_5890 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_5891, 1L);
																	}
																	BgL_res2588z00_5902 =
																		(BgL_arg1802z00_5890 == BgL_classz00_5869);
																}
															else
																{	/* SawBbv/bbv-range.scm 85 */
																	BgL_res2588z00_5902 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3130z00_9053 = BgL_res2588z00_5902;
										}
								}
							else
								{	/* SawBbv/bbv-range.scm 85 */
									BgL_test3130z00_9053 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test3130z00_9053)
						{	/* SawBbv/bbv-range.scm 295 */
							BgL_ylz00_2740 =
								BINT(
								(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
											((BgL_bbvzd2vlenzd2_bglt)
												(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_96))->
													BgL_loz00))))->BgL_offsetz00));
						}
					else
						{	/* SawBbv/bbv-range.scm 295 */
							BgL_ylz00_2740 =
								(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_96))->BgL_loz00);
						}
				}
				{	/* SawBbv/bbv-range.scm 835 */
					obj_t BgL_yuz00_2741;

					{	/* SawBbv/bbv-range.scm 311 */
						bool_t BgL_test3135z00_9082;

						{	/* SawBbv/bbv-range.scm 85 */
							obj_t BgL_classz00_5907;

							BgL_classz00_5907 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
							{	/* SawBbv/bbv-range.scm 85 */
								BgL_objectz00_bglt BgL_arg1807z00_5909;

								{	/* SawBbv/bbv-range.scm 85 */
									obj_t BgL_tmpz00_9083;

									BgL_tmpz00_9083 = ((obj_t) BgL_yz00_96);
									BgL_arg1807z00_5909 = (BgL_objectz00_bglt) (BgL_tmpz00_9083);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawBbv/bbv-range.scm 85 */
										long BgL_idxz00_5915;

										BgL_idxz00_5915 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5909);
										BgL_test3135z00_9082 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_5915 + 1L)) == BgL_classz00_5907);
									}
								else
									{	/* SawBbv/bbv-range.scm 85 */
										bool_t BgL_res2589z00_5940;

										{	/* SawBbv/bbv-range.scm 85 */
											obj_t BgL_oclassz00_5923;

											{	/* SawBbv/bbv-range.scm 85 */
												obj_t BgL_arg1815z00_5931;
												long BgL_arg1816z00_5932;

												BgL_arg1815z00_5931 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawBbv/bbv-range.scm 85 */
													long BgL_arg1817z00_5933;

													BgL_arg1817z00_5933 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5909);
													BgL_arg1816z00_5932 =
														(BgL_arg1817z00_5933 - OBJECT_TYPE);
												}
												BgL_oclassz00_5923 =
													VECTOR_REF(BgL_arg1815z00_5931, BgL_arg1816z00_5932);
											}
											{	/* SawBbv/bbv-range.scm 85 */
												bool_t BgL__ortest_1115z00_5924;

												BgL__ortest_1115z00_5924 =
													(BgL_classz00_5907 == BgL_oclassz00_5923);
												if (BgL__ortest_1115z00_5924)
													{	/* SawBbv/bbv-range.scm 85 */
														BgL_res2589z00_5940 = BgL__ortest_1115z00_5924;
													}
												else
													{	/* SawBbv/bbv-range.scm 85 */
														long BgL_odepthz00_5925;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_arg1804z00_5926;

															BgL_arg1804z00_5926 = (BgL_oclassz00_5923);
															BgL_odepthz00_5925 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_5926);
														}
														if ((1L < BgL_odepthz00_5925))
															{	/* SawBbv/bbv-range.scm 85 */
																obj_t BgL_arg1802z00_5928;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1803z00_5929;

																	BgL_arg1803z00_5929 = (BgL_oclassz00_5923);
																	BgL_arg1802z00_5928 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5929,
																		1L);
																}
																BgL_res2589z00_5940 =
																	(BgL_arg1802z00_5928 == BgL_classz00_5907);
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																BgL_res2589z00_5940 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3135z00_9082 = BgL_res2589z00_5940;
									}
							}
						}
						if (BgL_test3135z00_9082)
							{	/* SawBbv/bbv-range.scm 243 */
								long BgL_arg1808z00_5941;

								{	/* SawBbv/bbv-range.scm 243 */
									obj_t BgL_arg1812z00_5942;

									BgL_arg1812z00_5942 =
										BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0));
									BgL_arg1808z00_5941 = ((long) CINT(BgL_arg1812z00_5942) - 2L);
								}
								BgL_yuz00_2741 = BINT((1L << (int) (BgL_arg1808z00_5941)));
							}
						else
							{	/* SawBbv/bbv-range.scm 311 */
								BgL_yuz00_2741 =
									(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_96))->BgL_upz00);
							}
					}
					{	/* SawBbv/bbv-range.scm 836 */
						obj_t BgL_xlz00_2742;

						{	/* SawBbv/bbv-range.scm 295 */
							bool_t BgL_test3139z00_9113;

							{	/* SawBbv/bbv-range.scm 295 */
								obj_t BgL_arg1848z00_5947;

								BgL_arg1848z00_5947 =
									(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_95))->BgL_loz00);
								{	/* SawBbv/bbv-range.scm 85 */
									obj_t BgL_classz00_5949;

									BgL_classz00_5949 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
									if (BGL_OBJECTP(BgL_arg1848z00_5947))
										{	/* SawBbv/bbv-range.scm 85 */
											BgL_objectz00_bglt BgL_arg1807z00_5951;

											BgL_arg1807z00_5951 =
												(BgL_objectz00_bglt) (BgL_arg1848z00_5947);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawBbv/bbv-range.scm 85 */
													long BgL_idxz00_5957;

													BgL_idxz00_5957 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5951);
													BgL_test3139z00_9113 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_5957 + 1L)) == BgL_classz00_5949);
												}
											else
												{	/* SawBbv/bbv-range.scm 85 */
													bool_t BgL_res2590z00_5982;

													{	/* SawBbv/bbv-range.scm 85 */
														obj_t BgL_oclassz00_5965;

														{	/* SawBbv/bbv-range.scm 85 */
															obj_t BgL_arg1815z00_5973;
															long BgL_arg1816z00_5974;

															BgL_arg1815z00_5973 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_arg1817z00_5975;

																BgL_arg1817z00_5975 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5951);
																BgL_arg1816z00_5974 =
																	(BgL_arg1817z00_5975 - OBJECT_TYPE);
															}
															BgL_oclassz00_5965 =
																VECTOR_REF(BgL_arg1815z00_5973,
																BgL_arg1816z00_5974);
														}
														{	/* SawBbv/bbv-range.scm 85 */
															bool_t BgL__ortest_1115z00_5966;

															BgL__ortest_1115z00_5966 =
																(BgL_classz00_5949 == BgL_oclassz00_5965);
															if (BgL__ortest_1115z00_5966)
																{	/* SawBbv/bbv-range.scm 85 */
																	BgL_res2590z00_5982 =
																		BgL__ortest_1115z00_5966;
																}
															else
																{	/* SawBbv/bbv-range.scm 85 */
																	long BgL_odepthz00_5967;

																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1804z00_5968;

																		BgL_arg1804z00_5968 = (BgL_oclassz00_5965);
																		BgL_odepthz00_5967 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_5968);
																	}
																	if ((1L < BgL_odepthz00_5967))
																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1802z00_5970;

																			{	/* SawBbv/bbv-range.scm 85 */
																				obj_t BgL_arg1803z00_5971;

																				BgL_arg1803z00_5971 =
																					(BgL_oclassz00_5965);
																				BgL_arg1802z00_5970 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_5971, 1L);
																			}
																			BgL_res2590z00_5982 =
																				(BgL_arg1802z00_5970 ==
																				BgL_classz00_5949);
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 85 */
																			BgL_res2590z00_5982 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test3139z00_9113 = BgL_res2590z00_5982;
												}
										}
									else
										{	/* SawBbv/bbv-range.scm 85 */
											BgL_test3139z00_9113 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test3139z00_9113)
								{	/* SawBbv/bbv-range.scm 295 */
									BgL_xlz00_2742 =
										BINT(
										(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
													((BgL_bbvzd2vlenzd2_bglt)
														(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_95))->
															BgL_loz00))))->BgL_offsetz00));
								}
							else
								{	/* SawBbv/bbv-range.scm 295 */
									BgL_xlz00_2742 =
										(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_95))->
										BgL_loz00);
								}
						}
						{	/* SawBbv/bbv-range.scm 837 */
							obj_t BgL_xuz00_2743;

							{	/* SawBbv/bbv-range.scm 311 */
								bool_t BgL_test3144z00_9142;

								{	/* SawBbv/bbv-range.scm 85 */
									obj_t BgL_classz00_5987;

									BgL_classz00_5987 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
									{	/* SawBbv/bbv-range.scm 85 */
										BgL_objectz00_bglt BgL_arg1807z00_5989;

										{	/* SawBbv/bbv-range.scm 85 */
											obj_t BgL_tmpz00_9143;

											BgL_tmpz00_9143 = ((obj_t) BgL_xz00_95);
											BgL_arg1807z00_5989 =
												(BgL_objectz00_bglt) (BgL_tmpz00_9143);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawBbv/bbv-range.scm 85 */
												long BgL_idxz00_5995;

												BgL_idxz00_5995 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5989);
												BgL_test3144z00_9142 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5995 + 1L)) == BgL_classz00_5987);
											}
										else
											{	/* SawBbv/bbv-range.scm 85 */
												bool_t BgL_res2591z00_6020;

												{	/* SawBbv/bbv-range.scm 85 */
													obj_t BgL_oclassz00_6003;

													{	/* SawBbv/bbv-range.scm 85 */
														obj_t BgL_arg1815z00_6011;
														long BgL_arg1816z00_6012;

														BgL_arg1815z00_6011 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawBbv/bbv-range.scm 85 */
															long BgL_arg1817z00_6013;

															BgL_arg1817z00_6013 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5989);
															BgL_arg1816z00_6012 =
																(BgL_arg1817z00_6013 - OBJECT_TYPE);
														}
														BgL_oclassz00_6003 =
															VECTOR_REF(BgL_arg1815z00_6011,
															BgL_arg1816z00_6012);
													}
													{	/* SawBbv/bbv-range.scm 85 */
														bool_t BgL__ortest_1115z00_6004;

														BgL__ortest_1115z00_6004 =
															(BgL_classz00_5987 == BgL_oclassz00_6003);
														if (BgL__ortest_1115z00_6004)
															{	/* SawBbv/bbv-range.scm 85 */
																BgL_res2591z00_6020 = BgL__ortest_1115z00_6004;
															}
														else
															{	/* SawBbv/bbv-range.scm 85 */
																long BgL_odepthz00_6005;

																{	/* SawBbv/bbv-range.scm 85 */
																	obj_t BgL_arg1804z00_6006;

																	BgL_arg1804z00_6006 = (BgL_oclassz00_6003);
																	BgL_odepthz00_6005 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_6006);
																}
																if ((1L < BgL_odepthz00_6005))
																	{	/* SawBbv/bbv-range.scm 85 */
																		obj_t BgL_arg1802z00_6008;

																		{	/* SawBbv/bbv-range.scm 85 */
																			obj_t BgL_arg1803z00_6009;

																			BgL_arg1803z00_6009 =
																				(BgL_oclassz00_6003);
																			BgL_arg1802z00_6008 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_6009, 1L);
																		}
																		BgL_res2591z00_6020 =
																			(BgL_arg1802z00_6008 ==
																			BgL_classz00_5987);
																	}
																else
																	{	/* SawBbv/bbv-range.scm 85 */
																		BgL_res2591z00_6020 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test3144z00_9142 = BgL_res2591z00_6020;
											}
									}
								}
								if (BgL_test3144z00_9142)
									{	/* SawBbv/bbv-range.scm 243 */
										long BgL_arg1808z00_6021;

										{	/* SawBbv/bbv-range.scm 243 */
											obj_t BgL_arg1812z00_6022;

											BgL_arg1812z00_6022 =
												BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
												(0));
											BgL_arg1808z00_6021 =
												((long) CINT(BgL_arg1812z00_6022) - 2L);
										}
										BgL_xuz00_2743 = BINT((1L << (int) (BgL_arg1808z00_6021)));
									}
								else
									{	/* SawBbv/bbv-range.scm 311 */
										BgL_xuz00_2743 =
											(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_95))->
											BgL_upz00);
									}
							}
							{	/* SawBbv/bbv-range.scm 838 */
								obj_t BgL_v0z00_2744;

								BgL_v0z00_2744 =
									BGl_za2rvza2zzsaw_bbvzd2rangezd2(BgL_xlz00_2742,
									BgL_ylz00_2740, BUNSPEC);
								{	/* SawBbv/bbv-range.scm 839 */
									obj_t BgL_v1z00_2745;

									BgL_v1z00_2745 =
										BGl_za2rvza2zzsaw_bbvzd2rangezd2(BgL_xlz00_2742,
										BgL_yuz00_2741, BUNSPEC);
									{	/* SawBbv/bbv-range.scm 840 */
										obj_t BgL_v2z00_2746;

										BgL_v2z00_2746 =
											BGl_za2rvza2zzsaw_bbvzd2rangezd2(BgL_xuz00_2743,
											BgL_yuz00_2741, BUNSPEC);
										{	/* SawBbv/bbv-range.scm 841 */
											obj_t BgL_v3z00_2747;

											BgL_v3z00_2747 =
												BGl_za2rvza2zzsaw_bbvzd2rangezd2(BgL_yuz00_2741,
												BgL_ylz00_2740, BUNSPEC);
											{	/* SawBbv/bbv-range.scm 842 */
												obj_t BgL_mi0z00_2748;

												{	/* SawBbv/bbv-range.scm 45 */

													BgL_mi0z00_2748 =
														BGl_minrvz00zzsaw_bbvzd2rangezd2(BgL_v0z00_2744,
														BgL_v1z00_2745, BUNSPEC);
												}
												{	/* SawBbv/bbv-range.scm 843 */
													obj_t BgL_mi1z00_2749;

													{	/* SawBbv/bbv-range.scm 45 */

														BgL_mi1z00_2749 =
															BGl_minrvz00zzsaw_bbvzd2rangezd2(BgL_v2z00_2746,
															BgL_v3z00_2747, BUNSPEC);
													}
													{	/* SawBbv/bbv-range.scm 844 */
														obj_t BgL_ma0z00_2750;

														{	/* SawBbv/bbv-range.scm 46 */

															BgL_ma0z00_2750 =
																BGl_maxrvz00zzsaw_bbvzd2rangezd2(BgL_v0z00_2744,
																BgL_v1z00_2745, BUNSPEC);
														}
														{	/* SawBbv/bbv-range.scm 845 */
															obj_t BgL_ma1z00_2751;

															{	/* SawBbv/bbv-range.scm 46 */

																BgL_ma1z00_2751 =
																	BGl_maxrvz00zzsaw_bbvzd2rangezd2
																	(BgL_v2z00_2746, BgL_v3z00_2747, BUNSPEC);
															}
															{	/* SawBbv/bbv-range.scm 846 */
																obj_t BgL_nloz00_2752;

																{	/* SawBbv/bbv-range.scm 847 */
																	obj_t BgL_arg2251z00_2768;
																	obj_t BgL_arg2252z00_2769;

																	{	/* SawBbv/bbv-range.scm 45 */

																		BgL_arg2251z00_2768 =
																			BGl_minrvz00zzsaw_bbvzd2rangezd2
																			(BgL_mi0z00_2748, BgL_mi1z00_2749,
																			BUNSPEC);
																	}
																	{	/* SawBbv/bbv-range.scm 45 */

																		BgL_arg2252z00_2769 =
																			BGl_minrvz00zzsaw_bbvzd2rangezd2
																			(BgL_xlz00_2742, BgL_ylz00_2740, BUNSPEC);
																	}
																	{	/* SawBbv/bbv-range.scm 45 */

																		BgL_nloz00_2752 =
																			BGl_minrvz00zzsaw_bbvzd2rangezd2
																			(BgL_arg2251z00_2768, BgL_arg2252z00_2769,
																			BUNSPEC);
																	}
																}
																{	/* SawBbv/bbv-range.scm 847 */
																	obj_t BgL_nupz00_2753;

																	{	/* SawBbv/bbv-range.scm 848 */
																		obj_t BgL_arg2249z00_2757;
																		obj_t BgL_arg2250z00_2758;

																		{	/* SawBbv/bbv-range.scm 46 */

																			BgL_arg2249z00_2757 =
																				BGl_maxrvz00zzsaw_bbvzd2rangezd2
																				(BgL_ma0z00_2750, BgL_ma1z00_2751,
																				BUNSPEC);
																		}
																		{	/* SawBbv/bbv-range.scm 46 */

																			BgL_arg2250z00_2758 =
																				BGl_maxrvz00zzsaw_bbvzd2rangezd2
																				(BgL_xuz00_2743, BgL_yuz00_2741,
																				BUNSPEC);
																		}
																		{	/* SawBbv/bbv-range.scm 46 */

																			BgL_nupz00_2753 =
																				BGl_maxrvz00zzsaw_bbvzd2rangezd2
																				(BgL_arg2249z00_2757,
																				BgL_arg2250z00_2758, BUNSPEC);
																		}
																	}
																	{	/* SawBbv/bbv-range.scm 848 */

																		{	/* SawBbv/bbv-range.scm 849 */
																			BgL_bbvzd2rangezd2_bglt
																				BgL_new1286z00_2754;
																			{	/* SawBbv/bbv-range.scm 850 */
																				BgL_bbvzd2rangezd2_bglt
																					BgL_new1285z00_2755;
																				BgL_new1285z00_2755 =
																					((BgL_bbvzd2rangezd2_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_bbvzd2rangezd2_bgl))));
																				{	/* SawBbv/bbv-range.scm 850 */
																					long BgL_arg2248z00_2756;

																					BgL_arg2248z00_2756 =
																						BGL_CLASS_NUM
																						(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
																					BGL_OBJECT_CLASS_NUM_SET((
																							(BgL_objectz00_bglt)
																							BgL_new1285z00_2755),
																						BgL_arg2248z00_2756);
																				}
																				BgL_new1286z00_2754 =
																					BgL_new1285z00_2755;
																			}
																			{
																				obj_t BgL_auxz00_9191;

																				if ((BgL_nloz00_2752 == BUNSPEC))
																					{	/* SawBbv/bbv-range.scm 850 */
																						BgL_auxz00_9191 =
																							BGl_bbvzd2minzd2fixnumzd2envzd2zzsaw_bbvzd2rangezd2;
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 850 */
																						BgL_auxz00_9191 = BgL_nloz00_2752;
																					}
																				((((BgL_bbvzd2rangezd2_bglt)
																							COBJECT(BgL_new1286z00_2754))->
																						BgL_loz00) =
																					((obj_t) BgL_auxz00_9191), BUNSPEC);
																			}
																			{
																				obj_t BgL_auxz00_9195;

																				if ((BgL_nupz00_2753 == BUNSPEC))
																					{	/* SawBbv/bbv-range.scm 851 */
																						BgL_auxz00_9195 =
																							BGl_bbvzd2maxzd2fixnumzd2envzd2zzsaw_bbvzd2rangezd2;
																					}
																				else
																					{	/* SawBbv/bbv-range.scm 851 */
																						BgL_auxz00_9195 = BgL_nupz00_2753;
																					}
																				((((BgL_bbvzd2rangezd2_bglt)
																							COBJECT(BgL_new1286z00_2754))->
																						BgL_upz00) =
																					((obj_t) BgL_auxz00_9195), BUNSPEC);
																			}
																			return BgL_new1286z00_2754;
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* &bbv-range-mul */
	BgL_bbvzd2rangezd2_bglt BGl_z62bbvzd2rangezd2mulz62zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6299, obj_t BgL_xz00_6300, obj_t BgL_yz00_6301)
	{
		{	/* SawBbv/bbv-range.scm 834 */
			return
				BGl_bbvzd2rangezd2mulz00zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6300),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6301));
		}

	}



/* bbv-range-union */
	BGL_EXPORTED_DEF BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2unionz00zzsaw_bbvzd2rangezd2(BgL_bbvzd2rangezd2_bglt
		BgL_xz00_97, BgL_bbvzd2rangezd2_bglt BgL_yz00_98)
	{
		{	/* SawBbv/bbv-range.scm 856 */
			{	/* SawBbv/bbv-range.scm 858 */
				BgL_bbvzd2rangezd2_bglt BgL_resz00_2791;

				{	/* SawBbv/bbv-range.scm 858 */
					BgL_bbvzd2rangezd2_bglt BgL_new1288z00_2792;

					{	/* SawBbv/bbv-range.scm 859 */
						BgL_bbvzd2rangezd2_bglt BgL_new1287z00_2799;

						BgL_new1287z00_2799 =
							((BgL_bbvzd2rangezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_bbvzd2rangezd2_bgl))));
						{	/* SawBbv/bbv-range.scm 859 */
							long BgL_arg2259z00_2800;

							BgL_arg2259z00_2800 =
								BGL_CLASS_NUM(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1287z00_2799),
								BgL_arg2259z00_2800);
						}
						BgL_new1288z00_2792 = BgL_new1287z00_2799;
					}
					{
						obj_t BgL_auxz00_9206;

						{	/* SawBbv/bbv-range.scm 859 */
							obj_t BgL_arg2253z00_2793;
							obj_t BgL_arg2254z00_2794;
							long BgL_arg2255z00_2795;

							BgL_arg2253z00_2793 =
								(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_97))->BgL_loz00);
							BgL_arg2254z00_2794 =
								(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_98))->BgL_loz00);
							{	/* SawBbv/bbv-range.scm 245 */
								long BgL_arg1820z00_6033;

								{	/* SawBbv/bbv-range.scm 245 */
									long BgL_arg1822z00_6034;

									{	/* SawBbv/bbv-range.scm 243 */
										long BgL_arg1808z00_6035;

										{	/* SawBbv/bbv-range.scm 243 */
											obj_t BgL_arg1812z00_6036;

											BgL_arg1812z00_6036 =
												BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
												(0));
											BgL_arg1808z00_6035 =
												((long) CINT(BgL_arg1812z00_6036) - 2L);
										}
										BgL_arg1822z00_6034 = (1L << (int) (BgL_arg1808z00_6035));
									}
									BgL_arg1820z00_6033 = NEG(BgL_arg1822z00_6034);
								}
								BgL_arg2255z00_2795 = (BgL_arg1820z00_6033 - 1L);
							}
							BgL_auxz00_9206 =
								BGl_minrvzd2upzd2zzsaw_bbvzd2rangezd2(BgL_arg2253z00_2793,
								BgL_arg2254z00_2794, BINT(BgL_arg2255z00_2795));
						}
						((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1288z00_2792))->
								BgL_loz00) = ((obj_t) BgL_auxz00_9206), BUNSPEC);
					}
					{
						obj_t BgL_auxz00_9220;

						{	/* SawBbv/bbv-range.scm 860 */
							obj_t BgL_arg2256z00_2796;
							obj_t BgL_arg2257z00_2797;
							long BgL_arg2258z00_2798;

							BgL_arg2256z00_2796 =
								(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_97))->BgL_upz00);
							BgL_arg2257z00_2797 =
								(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_98))->BgL_upz00);
							{	/* SawBbv/bbv-range.scm 243 */
								long BgL_arg1808z00_6043;

								{	/* SawBbv/bbv-range.scm 243 */
									obj_t BgL_arg1812z00_6044;

									BgL_arg1812z00_6044 =
										BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0));
									BgL_arg1808z00_6043 = ((long) CINT(BgL_arg1812z00_6044) - 2L);
								}
								BgL_arg2258z00_2798 = (1L << (int) (BgL_arg1808z00_6043));
							}
							BgL_auxz00_9220 =
								BGl_maxrvzd2lozd2zzsaw_bbvzd2rangezd2(BgL_arg2256z00_2796,
								BgL_arg2257z00_2797, BINT(BgL_arg2258z00_2798));
						}
						((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1288z00_2792))->
								BgL_upz00) = ((obj_t) BgL_auxz00_9220), BUNSPEC);
					}
					BgL_resz00_2791 = BgL_new1288z00_2792;
				}
				return BgL_resz00_2791;
			}
		}

	}



/* &bbv-range-union */
	BgL_bbvzd2rangezd2_bglt
		BGl_z62bbvzd2rangezd2unionz62zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6302,
		obj_t BgL_xz00_6303, obj_t BgL_yz00_6304)
	{
		{	/* SawBbv/bbv-range.scm 856 */
			return
				BGl_bbvzd2rangezd2unionz00zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6303),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6304));
		}

	}



/* bbv-range-intersection */
	BGL_EXPORTED_DEF BgL_bbvzd2rangezd2_bglt
		BGl_bbvzd2rangezd2intersectionz00zzsaw_bbvzd2rangezd2
		(BgL_bbvzd2rangezd2_bglt BgL_xz00_99, BgL_bbvzd2rangezd2_bglt BgL_yz00_100)
	{
		{	/* SawBbv/bbv-range.scm 869 */
			{	/* SawBbv/bbv-range.scm 871 */
				BgL_bbvzd2rangezd2_bglt BgL_resz00_2801;

				{	/* SawBbv/bbv-range.scm 871 */
					BgL_bbvzd2rangezd2_bglt BgL_new1290z00_2802;

					{	/* SawBbv/bbv-range.scm 872 */
						BgL_bbvzd2rangezd2_bglt BgL_new1289z00_2809;

						BgL_new1289z00_2809 =
							((BgL_bbvzd2rangezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_bbvzd2rangezd2_bgl))));
						{	/* SawBbv/bbv-range.scm 872 */
							long BgL_arg2266z00_2810;

							BgL_arg2266z00_2810 =
								BGL_CLASS_NUM(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1289z00_2809),
								BgL_arg2266z00_2810);
						}
						BgL_new1290z00_2802 = BgL_new1289z00_2809;
					}
					{
						obj_t BgL_auxz00_9239;

						{	/* SawBbv/bbv-range.scm 872 */
							obj_t BgL_arg2260z00_2803;
							obj_t BgL_arg2261z00_2804;
							long BgL_arg2262z00_2805;

							BgL_arg2260z00_2803 =
								(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_99))->BgL_loz00);
							BgL_arg2261z00_2804 =
								(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_100))->BgL_loz00);
							{	/* SawBbv/bbv-range.scm 245 */
								long BgL_arg1820z00_6052;

								{	/* SawBbv/bbv-range.scm 245 */
									long BgL_arg1822z00_6053;

									{	/* SawBbv/bbv-range.scm 243 */
										long BgL_arg1808z00_6054;

										{	/* SawBbv/bbv-range.scm 243 */
											obj_t BgL_arg1812z00_6055;

											BgL_arg1812z00_6055 =
												BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
												(0));
											BgL_arg1808z00_6054 =
												((long) CINT(BgL_arg1812z00_6055) - 2L);
										}
										BgL_arg1822z00_6053 = (1L << (int) (BgL_arg1808z00_6054));
									}
									BgL_arg1820z00_6052 = NEG(BgL_arg1822z00_6053);
								}
								BgL_arg2262z00_2805 = (BgL_arg1820z00_6052 - 1L);
							}
							BgL_auxz00_9239 =
								BGl_maxrvzd2lozd2zzsaw_bbvzd2rangezd2(BgL_arg2260z00_2803,
								BgL_arg2261z00_2804, BINT(BgL_arg2262z00_2805));
						}
						((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1290z00_2802))->
								BgL_loz00) = ((obj_t) BgL_auxz00_9239), BUNSPEC);
					}
					{
						obj_t BgL_auxz00_9253;

						{	/* SawBbv/bbv-range.scm 873 */
							obj_t BgL_arg2263z00_2806;
							obj_t BgL_arg2264z00_2807;
							long BgL_arg2265z00_2808;

							BgL_arg2263z00_2806 =
								(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_xz00_99))->BgL_upz00);
							BgL_arg2264z00_2807 =
								(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_yz00_100))->BgL_upz00);
							{	/* SawBbv/bbv-range.scm 243 */
								long BgL_arg1808z00_6062;

								{	/* SawBbv/bbv-range.scm 243 */
									obj_t BgL_arg1812z00_6063;

									BgL_arg1812z00_6063 =
										BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0));
									BgL_arg1808z00_6062 = ((long) CINT(BgL_arg1812z00_6063) - 2L);
								}
								BgL_arg2265z00_2808 = (1L << (int) (BgL_arg1808z00_6062));
							}
							BgL_auxz00_9253 =
								BGl_minrvzd2upzd2zzsaw_bbvzd2rangezd2(BgL_arg2263z00_2806,
								BgL_arg2264z00_2807, BINT(BgL_arg2265z00_2808));
						}
						((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1290z00_2802))->
								BgL_upz00) = ((obj_t) BgL_auxz00_9253), BUNSPEC);
					}
					BgL_resz00_2801 = BgL_new1290z00_2802;
				}
				return BgL_resz00_2801;
			}
		}

	}



/* &bbv-range-intersection */
	BgL_bbvzd2rangezd2_bglt
		BGl_z62bbvzd2rangezd2intersectionz62zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6305, obj_t BgL_xz00_6306, obj_t BgL_yz00_6307)
	{
		{	/* SawBbv/bbv-range.scm 869 */
			return
				BGl_bbvzd2rangezd2intersectionz00zzsaw_bbvzd2rangezd2(
				((BgL_bbvzd2rangezd2_bglt) BgL_xz00_6306),
				((BgL_bbvzd2rangezd2_bglt) BgL_yz00_6307));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_bbvzd2rangezd2(void)
	{
		{	/* SawBbv/bbv-range.scm 15 */
			{	/* SawBbv/bbv-range.scm 30 */
				obj_t BgL_arg2270z00_2814;
				obj_t BgL_arg2271z00_2815;

				{	/* SawBbv/bbv-range.scm 30 */
					obj_t BgL_v1531z00_2827;

					BgL_v1531z00_2827 = create_vector(2L);
					{	/* SawBbv/bbv-range.scm 30 */
						obj_t BgL_arg2277z00_2828;

						BgL_arg2277z00_2828 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc2605z00zzsaw_bbvzd2rangezd2,
							BGl_proc2604z00zzsaw_bbvzd2rangezd2, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1531z00_2827, 0L, BgL_arg2277z00_2828);
					}
					{	/* SawBbv/bbv-range.scm 30 */
						obj_t BgL_arg2283z00_2838;

						BgL_arg2283z00_2838 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc2607z00zzsaw_bbvzd2rangezd2,
							BGl_proc2606z00zzsaw_bbvzd2rangezd2, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1531z00_2827, 1L, BgL_arg2283z00_2838);
					}
					BgL_arg2270z00_2814 = BgL_v1531z00_2827;
				}
				{	/* SawBbv/bbv-range.scm 30 */
					obj_t BgL_v1532z00_2848;

					BgL_v1532z00_2848 = create_vector(0L);
					BgL_arg2271z00_2815 = BgL_v1532z00_2848;
				}
				BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(5),
					CNST_TABLE_REF(6), BGl_objectz00zz__objectz00, 13605L,
					BGl_proc2610z00zzsaw_bbvzd2rangezd2,
					BGl_proc2609z00zzsaw_bbvzd2rangezd2, BFALSE,
					BGl_proc2608z00zzsaw_bbvzd2rangezd2, BFALSE, BgL_arg2270z00_2814,
					BgL_arg2271z00_2815);
			}
			{	/* SawBbv/bbv-range.scm 34 */
				obj_t BgL_arg2292z00_2855;
				obj_t BgL_arg2293z00_2856;

				{	/* SawBbv/bbv-range.scm 34 */
					obj_t BgL_v1533z00_2868;

					BgL_v1533z00_2868 = create_vector(2L);
					{	/* SawBbv/bbv-range.scm 34 */
						obj_t BgL_arg2299z00_2869;

						BgL_arg2299z00_2869 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc2612z00zzsaw_bbvzd2rangezd2,
							BGl_proc2611z00zzsaw_bbvzd2rangezd2, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1533z00_2868, 0L, BgL_arg2299z00_2869);
					}
					{	/* SawBbv/bbv-range.scm 34 */
						obj_t BgL_arg2305z00_2879;

						BgL_arg2305z00_2879 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc2614z00zzsaw_bbvzd2rangezd2,
							BGl_proc2613z00zzsaw_bbvzd2rangezd2, ((bool_t) 1), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(9));
						VECTOR_SET(BgL_v1533z00_2868, 1L, BgL_arg2305z00_2879);
					}
					BgL_arg2292z00_2855 = BgL_v1533z00_2868;
				}
				{	/* SawBbv/bbv-range.scm 34 */
					obj_t BgL_v1534z00_2889;

					BgL_v1534z00_2889 = create_vector(0L);
					BgL_arg2293z00_2856 = BgL_v1534z00_2889;
				}
				return (BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(10),
						CNST_TABLE_REF(6), BGl_objectz00zz__objectz00, 11775L,
						BGl_proc2617z00zzsaw_bbvzd2rangezd2,
						BGl_proc2616z00zzsaw_bbvzd2rangezd2, BFALSE,
						BGl_proc2615z00zzsaw_bbvzd2rangezd2, BFALSE, BgL_arg2292z00_2855,
						BgL_arg2293z00_2856), BUNSPEC);
			}
		}

	}



/* &<@anonymous:2298> */
	obj_t BGl_z62zc3z04anonymousza32298ze3ze5zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6322, obj_t BgL_new1165z00_6323)
	{
		{	/* SawBbv/bbv-range.scm 34 */
			{
				BgL_bbvzd2vlenzd2_bglt BgL_auxz00_9294;

				((((BgL_bbvzd2vlenzd2_bglt) COBJECT(
								((BgL_bbvzd2vlenzd2_bglt) BgL_new1165z00_6323)))->BgL_vecz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_bbvzd2vlenzd2_bglt) COBJECT(((BgL_bbvzd2vlenzd2_bglt)
									BgL_new1165z00_6323)))->BgL_offsetz00) =
					((int) (int) (0L)), BUNSPEC);
				BgL_auxz00_9294 = ((BgL_bbvzd2vlenzd2_bglt) BgL_new1165z00_6323);
				return ((obj_t) BgL_auxz00_9294);
			}
		}

	}



/* &lambda2296 */
	BgL_bbvzd2vlenzd2_bglt BGl_z62lambda2296z62zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6324)
	{
		{	/* SawBbv/bbv-range.scm 34 */
			{	/* SawBbv/bbv-range.scm 34 */
				BgL_bbvzd2vlenzd2_bglt BgL_new1164z00_6423;

				BgL_new1164z00_6423 =
					((BgL_bbvzd2vlenzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_bbvzd2vlenzd2_bgl))));
				{	/* SawBbv/bbv-range.scm 34 */
					long BgL_arg2297z00_6424;

					BgL_arg2297z00_6424 =
						BGL_CLASS_NUM(BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1164z00_6423), BgL_arg2297z00_6424);
				}
				return BgL_new1164z00_6423;
			}
		}

	}



/* &lambda2294 */
	BgL_bbvzd2vlenzd2_bglt BGl_z62lambda2294z62zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6325, obj_t BgL_vec1162z00_6326, obj_t BgL_offset1163z00_6327)
	{
		{	/* SawBbv/bbv-range.scm 34 */
			{	/* SawBbv/bbv-range.scm 34 */
				int BgL_offset1163z00_6425;

				BgL_offset1163z00_6425 = CINT(BgL_offset1163z00_6327);
				{	/* SawBbv/bbv-range.scm 34 */
					BgL_bbvzd2vlenzd2_bglt BgL_new1294z00_6426;

					{	/* SawBbv/bbv-range.scm 34 */
						BgL_bbvzd2vlenzd2_bglt BgL_new1293z00_6427;

						BgL_new1293z00_6427 =
							((BgL_bbvzd2vlenzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_bbvzd2vlenzd2_bgl))));
						{	/* SawBbv/bbv-range.scm 34 */
							long BgL_arg2295z00_6428;

							BgL_arg2295z00_6428 =
								BGL_CLASS_NUM(BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1293z00_6427),
								BgL_arg2295z00_6428);
						}
						BgL_new1294z00_6426 = BgL_new1293z00_6427;
					}
					((((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_new1294z00_6426))->
							BgL_vecz00) = ((obj_t) BgL_vec1162z00_6326), BUNSPEC);
					((((BgL_bbvzd2vlenzd2_bglt) COBJECT(BgL_new1294z00_6426))->
							BgL_offsetz00) = ((int) BgL_offset1163z00_6425), BUNSPEC);
					return BgL_new1294z00_6426;
				}
			}
		}

	}



/* &lambda2309 */
	obj_t BGl_z62lambda2309z62zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6328,
		obj_t BgL_oz00_6329, obj_t BgL_vz00_6330)
	{
		{	/* SawBbv/bbv-range.scm 34 */
			{	/* SawBbv/bbv-range.scm 34 */
				int BgL_vz00_6430;

				BgL_vz00_6430 = CINT(BgL_vz00_6330);
				return
					((((BgL_bbvzd2vlenzd2_bglt) COBJECT(
								((BgL_bbvzd2vlenzd2_bglt) BgL_oz00_6329)))->BgL_offsetz00) =
					((int) BgL_vz00_6430), BUNSPEC);
		}}

	}



/* &lambda2308 */
	obj_t BGl_z62lambda2308z62zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6331,
		obj_t BgL_oz00_6332)
	{
		{	/* SawBbv/bbv-range.scm 34 */
			return
				BINT(
				(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
							((BgL_bbvzd2vlenzd2_bglt) BgL_oz00_6332)))->BgL_offsetz00));
		}

	}



/* &lambda2304 */
	obj_t BGl_z62lambda2304z62zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6333,
		obj_t BgL_oz00_6334, obj_t BgL_vz00_6335)
	{
		{	/* SawBbv/bbv-range.scm 34 */
			return
				((((BgL_bbvzd2vlenzd2_bglt) COBJECT(
							((BgL_bbvzd2vlenzd2_bglt) BgL_oz00_6334)))->BgL_vecz00) =
				((obj_t) BgL_vz00_6335), BUNSPEC);
		}

	}



/* &lambda2303 */
	obj_t BGl_z62lambda2303z62zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6336,
		obj_t BgL_oz00_6337)
	{
		{	/* SawBbv/bbv-range.scm 34 */
			return
				(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
						((BgL_bbvzd2vlenzd2_bglt) BgL_oz00_6337)))->BgL_vecz00);
		}

	}



/* &<@anonymous:2276> */
	obj_t BGl_z62zc3z04anonymousza32276ze3ze5zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6338, obj_t BgL_new1160z00_6339)
	{
		{	/* SawBbv/bbv-range.scm 30 */
			{
				BgL_bbvzd2rangezd2_bglt BgL_auxz00_9323;

				((((BgL_bbvzd2rangezd2_bglt) COBJECT(
								((BgL_bbvzd2rangezd2_bglt) BgL_new1160z00_6339)))->BgL_loz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_bbvzd2rangezd2_bglt) COBJECT(((BgL_bbvzd2rangezd2_bglt)
									BgL_new1160z00_6339)))->BgL_upz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_9323 = ((BgL_bbvzd2rangezd2_bglt) BgL_new1160z00_6339);
				return ((obj_t) BgL_auxz00_9323);
			}
		}

	}



/* &lambda2274 */
	BgL_bbvzd2rangezd2_bglt BGl_z62lambda2274z62zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6340)
	{
		{	/* SawBbv/bbv-range.scm 30 */
			{	/* SawBbv/bbv-range.scm 30 */
				BgL_bbvzd2rangezd2_bglt BgL_new1159z00_6435;

				BgL_new1159z00_6435 =
					((BgL_bbvzd2rangezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_bbvzd2rangezd2_bgl))));
				{	/* SawBbv/bbv-range.scm 30 */
					long BgL_arg2275z00_6436;

					BgL_arg2275z00_6436 =
						BGL_CLASS_NUM(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1159z00_6435), BgL_arg2275z00_6436);
				}
				return BgL_new1159z00_6435;
			}
		}

	}



/* &lambda2272 */
	BgL_bbvzd2rangezd2_bglt BGl_z62lambda2272z62zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6341, obj_t BgL_lo1157z00_6342, obj_t BgL_up1158z00_6343)
	{
		{	/* SawBbv/bbv-range.scm 30 */
			{	/* SawBbv/bbv-range.scm 30 */
				BgL_bbvzd2rangezd2_bglt BgL_new1292z00_6437;

				{	/* SawBbv/bbv-range.scm 30 */
					BgL_bbvzd2rangezd2_bglt BgL_new1291z00_6438;

					BgL_new1291z00_6438 =
						((BgL_bbvzd2rangezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2rangezd2_bgl))));
					{	/* SawBbv/bbv-range.scm 30 */
						long BgL_arg2273z00_6439;

						BgL_arg2273z00_6439 =
							BGL_CLASS_NUM(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1291z00_6438), BgL_arg2273z00_6439);
					}
					BgL_new1292z00_6437 = BgL_new1291z00_6438;
				}
				((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1292z00_6437))->BgL_loz00) =
					((obj_t) BgL_lo1157z00_6342), BUNSPEC);
				((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_new1292z00_6437))->BgL_upz00) =
					((obj_t) BgL_up1158z00_6343), BUNSPEC);
				return BgL_new1292z00_6437;
			}
		}

	}



/* &lambda2288 */
	obj_t BGl_z62lambda2288z62zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6344,
		obj_t BgL_oz00_6345, obj_t BgL_vz00_6346)
	{
		{	/* SawBbv/bbv-range.scm 30 */
			return
				((((BgL_bbvzd2rangezd2_bglt) COBJECT(
							((BgL_bbvzd2rangezd2_bglt) BgL_oz00_6345)))->BgL_upz00) =
				((obj_t) BgL_vz00_6346), BUNSPEC);
		}

	}



/* &lambda2287 */
	obj_t BGl_z62lambda2287z62zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6347,
		obj_t BgL_oz00_6348)
	{
		{	/* SawBbv/bbv-range.scm 30 */
			return
				(((BgL_bbvzd2rangezd2_bglt) COBJECT(
						((BgL_bbvzd2rangezd2_bglt) BgL_oz00_6348)))->BgL_upz00);
		}

	}



/* &lambda2282 */
	obj_t BGl_z62lambda2282z62zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6349,
		obj_t BgL_oz00_6350, obj_t BgL_vz00_6351)
	{
		{	/* SawBbv/bbv-range.scm 30 */
			return
				((((BgL_bbvzd2rangezd2_bglt) COBJECT(
							((BgL_bbvzd2rangezd2_bglt) BgL_oz00_6350)))->BgL_loz00) =
				((obj_t) BgL_vz00_6351), BUNSPEC);
		}

	}



/* &lambda2281 */
	obj_t BGl_z62lambda2281z62zzsaw_bbvzd2rangezd2(obj_t BgL_envz00_6352,
		obj_t BgL_oz00_6353)
	{
		{	/* SawBbv/bbv-range.scm 30 */
			return
				(((BgL_bbvzd2rangezd2_bglt) COBJECT(
						((BgL_bbvzd2rangezd2_bglt) BgL_oz00_6353)))->BgL_loz00);
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_bbvzd2rangezd2(void)
	{
		{	/* SawBbv/bbv-range.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_bbvzd2rangezd2(void)
	{
		{	/* SawBbv/bbv-range.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_objectzd2printzd2envz00zz__objectz00,
				BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2,
				BGl_proc2618z00zzsaw_bbvzd2rangezd2,
				BGl_string2619z00zzsaw_bbvzd2rangezd2);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00,
				BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2,
				BGl_proc2620z00zzsaw_bbvzd2rangezd2,
				BGl_string2621z00zzsaw_bbvzd2rangezd2);
		}

	}



/* &shape-bbv-range1558 */
	obj_t BGl_z62shapezd2bbvzd2range1558z62zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6356, obj_t BgL_ez00_6357)
	{
		{	/* SawBbv/bbv-range.scm 318 */
			if (BGl_emptyzd2rangezf3z21zzsaw_bbvzd2rangezd2(
					((BgL_bbvzd2rangezd2_bglt) BgL_ez00_6357)))
				{	/* SawBbv/bbv-range.scm 342 */
					return BGl_string2622z00zzsaw_bbvzd2rangezd2;
				}
			else
				{	/* SawBbv/bbv-range.scm 345 */
					obj_t BgL_arg2316z00_6445;
					obj_t BgL_arg2317z00_6446;

					BgL_arg2316z00_6445 =
						BGl_strze70ze7zzsaw_bbvzd2rangezd2(
						(((BgL_bbvzd2rangezd2_bglt) COBJECT(
									((BgL_bbvzd2rangezd2_bglt) BgL_ez00_6357)))->BgL_loz00));
					BgL_arg2317z00_6446 =
						BGl_strze70ze7zzsaw_bbvzd2rangezd2(
						(((BgL_bbvzd2rangezd2_bglt) COBJECT(
									((BgL_bbvzd2rangezd2_bglt) BgL_ez00_6357)))->BgL_upz00));
					{	/* SawBbv/bbv-range.scm 345 */
						obj_t BgL_list2318z00_6447;

						{	/* SawBbv/bbv-range.scm 345 */
							obj_t BgL_arg2319z00_6448;

							BgL_arg2319z00_6448 = MAKE_YOUNG_PAIR(BgL_arg2317z00_6446, BNIL);
							BgL_list2318z00_6447 =
								MAKE_YOUNG_PAIR(BgL_arg2316z00_6445, BgL_arg2319z00_6448);
						}
						return
							BGl_formatz00zz__r4_output_6_10_3z00
							(BGl_string2623z00zzsaw_bbvzd2rangezd2, BgL_list2318z00_6447);
					}
				}
		}

	}



/* str~0 */
	obj_t BGl_strze70ze7zzsaw_bbvzd2rangezd2(obj_t BgL_nz00_2925)
	{
		{	/* SawBbv/bbv-range.scm 340 */
			{
				obj_t BgL_nz00_2918;

				{	/* SawBbv/bbv-range.scm 327 */
					bool_t BgL_test3151z00_9362;

					{	/* SawBbv/bbv-range.scm 327 */
						long BgL_arg2381z00_2976;

						{	/* SawBbv/bbv-range.scm 243 */
							long BgL_arg1808z00_6090;

							{	/* SawBbv/bbv-range.scm 243 */
								obj_t BgL_arg1812z00_6091;

								BgL_arg1812z00_6091 =
									BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF(0));
								BgL_arg1808z00_6090 = ((long) CINT(BgL_arg1812z00_6091) - 2L);
							}
							BgL_arg2381z00_2976 = (1L << (int) (BgL_arg1808z00_6090));
						}
						BgL_test3151z00_9362 = (BgL_nz00_2925 == BINT(BgL_arg2381z00_2976));
					}
					if (BgL_test3151z00_9362)
						{	/* SawBbv/bbv-range.scm 327 */
							return BGl_string2625z00zzsaw_bbvzd2rangezd2;
						}
					else
						{	/* SawBbv/bbv-range.scm 328 */
							bool_t BgL_test3152z00_9371;

							{	/* SawBbv/bbv-range.scm 328 */
								long BgL_arg2379z00_2974;

								{	/* SawBbv/bbv-range.scm 328 */
									long BgL_arg2380z00_2975;

									{	/* SawBbv/bbv-range.scm 243 */
										long BgL_arg1808z00_6094;

										{	/* SawBbv/bbv-range.scm 243 */
											obj_t BgL_arg1812z00_6095;

											BgL_arg1812z00_6095 =
												BGl_bigloozd2configzd2zz__configurez00(CNST_TABLE_REF
												(0));
											BgL_arg1808z00_6094 =
												((long) CINT(BgL_arg1812z00_6095) - 2L);
										}
										BgL_arg2380z00_2975 = (1L << (int) (BgL_arg1808z00_6094));
									}
									BgL_arg2379z00_2974 = (BgL_arg2380z00_2975 - 1L);
								}
								BgL_test3152z00_9371 =
									(BgL_nz00_2925 == BINT(BgL_arg2379z00_2974));
							}
							if (BgL_test3152z00_9371)
								{	/* SawBbv/bbv-range.scm 328 */
									return BGl_string2626z00zzsaw_bbvzd2rangezd2;
								}
							else
								{	/* SawBbv/bbv-range.scm 329 */
									bool_t BgL_test3153z00_9381;

									if (INTEGERP(BgL_nz00_2925))
										{	/* SawBbv/bbv-range.scm 329 */
											if (((long) CINT(BgL_nz00_2925) > 0L))
												{	/* SawBbv/bbv-range.scm 329 */
													long BgL_arg2377z00_2972;

													{	/* SawBbv/bbv-range.scm 329 */
														long BgL_arg2378z00_2973;

														{	/* SawBbv/bbv-range.scm 243 */
															long BgL_arg1808z00_6100;

															{	/* SawBbv/bbv-range.scm 243 */
																obj_t BgL_arg1812z00_6101;

																BgL_arg1812z00_6101 =
																	BGl_bigloozd2configzd2zz__configurez00
																	(CNST_TABLE_REF(0));
																BgL_arg1808z00_6100 =
																	((long) CINT(BgL_arg1812z00_6101) - 2L);
															}
															BgL_arg2378z00_2973 =
																(1L << (int) (BgL_arg1808z00_6100));
														}
														BgL_arg2377z00_2972 =
															(BgL_arg2378z00_2973 -
															(long) CINT(BgL_nz00_2925));
													}
													BgL_test3153z00_9381 = (BgL_arg2377z00_2972 < 20L);
												}
											else
												{	/* SawBbv/bbv-range.scm 329 */
													BgL_test3153z00_9381 = ((bool_t) 0);
												}
										}
									else
										{	/* SawBbv/bbv-range.scm 329 */
											BgL_test3153z00_9381 = ((bool_t) 0);
										}
									if (BgL_test3153z00_9381)
										{	/* SawBbv/bbv-range.scm 330 */
											long BgL_arg2342z00_2937;

											{	/* SawBbv/bbv-range.scm 330 */
												long BgL_arg2345z00_2939;

												{	/* SawBbv/bbv-range.scm 243 */
													long BgL_arg1808z00_6107;

													{	/* SawBbv/bbv-range.scm 243 */
														obj_t BgL_arg1812z00_6108;

														BgL_arg1812z00_6108 =
															BGl_bigloozd2configzd2zz__configurez00
															(CNST_TABLE_REF(0));
														BgL_arg1808z00_6107 =
															((long) CINT(BgL_arg1812z00_6108) - 2L);
													}
													BgL_arg2345z00_2939 =
														(1L << (int) (BgL_arg1808z00_6107));
												}
												BgL_arg2342z00_2937 =
													(BgL_arg2345z00_2939 - (long) CINT(BgL_nz00_2925));
											}
											{	/* SawBbv/bbv-range.scm 330 */
												obj_t BgL_list2343z00_2938;

												BgL_list2343z00_2938 =
													MAKE_YOUNG_PAIR(BINT(BgL_arg2342z00_2937), BNIL);
												return
													BGl_formatz00zz__r4_output_6_10_3z00
													(BGl_string2627z00zzsaw_bbvzd2rangezd2,
													BgL_list2343z00_2938);
											}
										}
									else
										{	/* SawBbv/bbv-range.scm 331 */
											bool_t BgL_test3156z00_9407;

											{	/* SawBbv/bbv-range.scm 331 */
												long BgL_arg2376z00_2969;

												{	/* SawBbv/bbv-range.scm 245 */
													long BgL_arg1820z00_6113;

													{	/* SawBbv/bbv-range.scm 245 */
														long BgL_arg1822z00_6114;

														{	/* SawBbv/bbv-range.scm 243 */
															long BgL_arg1808z00_6115;

															{	/* SawBbv/bbv-range.scm 243 */
																obj_t BgL_arg1812z00_6116;

																BgL_arg1812z00_6116 =
																	BGl_bigloozd2configzd2zz__configurez00
																	(CNST_TABLE_REF(0));
																BgL_arg1808z00_6115 =
																	((long) CINT(BgL_arg1812z00_6116) - 2L);
															}
															BgL_arg1822z00_6114 =
																(1L << (int) (BgL_arg1808z00_6115));
														}
														BgL_arg1820z00_6113 = NEG(BgL_arg1822z00_6114);
													}
													BgL_arg2376z00_2969 = (BgL_arg1820z00_6113 - 1L);
												}
												BgL_test3156z00_9407 =
													(BgL_nz00_2925 == BINT(BgL_arg2376z00_2969));
											}
											if (BgL_test3156z00_9407)
												{	/* SawBbv/bbv-range.scm 331 */
													return BGl_string2628z00zzsaw_bbvzd2rangezd2;
												}
											else
												{	/* SawBbv/bbv-range.scm 332 */
													bool_t BgL_test3157z00_9418;

													{	/* SawBbv/bbv-range.scm 332 */
														long BgL_arg2374z00_2967;

														{	/* SawBbv/bbv-range.scm 332 */
															long BgL_arg2375z00_2968;

															{	/* SawBbv/bbv-range.scm 245 */
																long BgL_arg1820z00_6121;

																{	/* SawBbv/bbv-range.scm 245 */
																	long BgL_arg1822z00_6122;

																	{	/* SawBbv/bbv-range.scm 243 */
																		long BgL_arg1808z00_6123;

																		{	/* SawBbv/bbv-range.scm 243 */
																			obj_t BgL_arg1812z00_6124;

																			BgL_arg1812z00_6124 =
																				BGl_bigloozd2configzd2zz__configurez00
																				(CNST_TABLE_REF(0));
																			BgL_arg1808z00_6123 =
																				((long) CINT(BgL_arg1812z00_6124) - 2L);
																		}
																		BgL_arg1822z00_6122 =
																			(1L << (int) (BgL_arg1808z00_6123));
																	}
																	BgL_arg1820z00_6121 =
																		NEG(BgL_arg1822z00_6122);
																}
																BgL_arg2375z00_2968 =
																	(BgL_arg1820z00_6121 - 1L);
															}
															BgL_arg2374z00_2967 = (BgL_arg2375z00_2968 + 1L);
														}
														BgL_test3157z00_9418 =
															(BgL_nz00_2925 == BINT(BgL_arg2374z00_2967));
													}
													if (BgL_test3157z00_9418)
														{	/* SawBbv/bbv-range.scm 332 */
															return BGl_string2629z00zzsaw_bbvzd2rangezd2;
														}
													else
														{	/* SawBbv/bbv-range.scm 333 */
															bool_t BgL_test3158z00_9430;

															{	/* SawBbv/bbv-range.scm 333 */
																long BgL_arg2371z00_2965;

																{	/* SawBbv/bbv-range.scm 333 */
																	long BgL_arg2373z00_2966;

																	{	/* SawBbv/bbv-range.scm 245 */
																		long BgL_arg1820z00_6130;

																		{	/* SawBbv/bbv-range.scm 245 */
																			long BgL_arg1822z00_6131;

																			{	/* SawBbv/bbv-range.scm 243 */
																				long BgL_arg1808z00_6132;

																				{	/* SawBbv/bbv-range.scm 243 */
																					obj_t BgL_arg1812z00_6133;

																					BgL_arg1812z00_6133 =
																						BGl_bigloozd2configzd2zz__configurez00
																						(CNST_TABLE_REF(0));
																					BgL_arg1808z00_6132 =
																						((long) CINT(BgL_arg1812z00_6133) -
																						2L);
																				}
																				BgL_arg1822z00_6131 =
																					(1L << (int) (BgL_arg1808z00_6132));
																			}
																			BgL_arg1820z00_6130 =
																				NEG(BgL_arg1822z00_6131);
																		}
																		BgL_arg2373z00_2966 =
																			(BgL_arg1820z00_6130 - 1L);
																	}
																	BgL_arg2371z00_2965 =
																		(BgL_arg2373z00_2966 + 2L);
																}
																BgL_test3158z00_9430 =
																	(BgL_nz00_2925 == BINT(BgL_arg2371z00_2965));
															}
															if (BgL_test3158z00_9430)
																{	/* SawBbv/bbv-range.scm 333 */
																	return BGl_string2630z00zzsaw_bbvzd2rangezd2;
																}
															else
																{	/* SawBbv/bbv-range.scm 334 */
																	bool_t BgL_test3159z00_9442;

																	if (INTEGERP(BgL_nz00_2925))
																		{	/* SawBbv/bbv-range.scm 334 */
																			if (((long) CINT(BgL_nz00_2925) < 0L))
																				{	/* SawBbv/bbv-range.scm 334 */
																					long BgL_arg2369z00_2963;

																					{	/* SawBbv/bbv-range.scm 334 */
																						long BgL_arg2370z00_2964;

																						{	/* SawBbv/bbv-range.scm 245 */
																							long BgL_arg1820z00_6140;

																							{	/* SawBbv/bbv-range.scm 245 */
																								long BgL_arg1822z00_6141;

																								{	/* SawBbv/bbv-range.scm 243 */
																									long BgL_arg1808z00_6142;

																									{	/* SawBbv/bbv-range.scm 243 */
																										obj_t BgL_arg1812z00_6143;

																										BgL_arg1812z00_6143 =
																											BGl_bigloozd2configzd2zz__configurez00
																											(CNST_TABLE_REF(0));
																										BgL_arg1808z00_6142 =
																											((long)
																											CINT(BgL_arg1812z00_6143)
																											- 2L);
																									}
																									BgL_arg1822z00_6141 =
																										(1L <<
																										(int)
																										(BgL_arg1808z00_6142));
																								}
																								BgL_arg1820z00_6140 =
																									NEG(BgL_arg1822z00_6141);
																							}
																							BgL_arg2370z00_2964 =
																								(BgL_arg1820z00_6140 - 1L);
																						}
																						BgL_arg2369z00_2963 =
																							(BgL_arg2370z00_2964 +
																							(long) CINT(BgL_nz00_2925));
																					}
																					BgL_test3159z00_9442 =
																						(BgL_arg2369z00_2963 > 20L);
																				}
																			else
																				{	/* SawBbv/bbv-range.scm 334 */
																					BgL_test3159z00_9442 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 334 */
																			BgL_test3159z00_9442 = ((bool_t) 0);
																		}
																	if (BgL_test3159z00_9442)
																		{	/* SawBbv/bbv-range.scm 335 */
																			long BgL_arg2361z00_2953;

																			{	/* SawBbv/bbv-range.scm 335 */
																				long BgL_arg2363z00_2955;

																				{	/* SawBbv/bbv-range.scm 245 */
																					long BgL_arg1820z00_6151;

																					{	/* SawBbv/bbv-range.scm 245 */
																						long BgL_arg1822z00_6152;

																						{	/* SawBbv/bbv-range.scm 243 */
																							long BgL_arg1808z00_6153;

																							{	/* SawBbv/bbv-range.scm 243 */
																								obj_t BgL_arg1812z00_6154;

																								BgL_arg1812z00_6154 =
																									BGl_bigloozd2configzd2zz__configurez00
																									(CNST_TABLE_REF(0));
																								BgL_arg1808z00_6153 =
																									((long)
																									CINT(BgL_arg1812z00_6154) -
																									2L);
																							}
																							BgL_arg1822z00_6152 =
																								(1L <<
																								(int) (BgL_arg1808z00_6153));
																						}
																						BgL_arg1820z00_6151 =
																							NEG(BgL_arg1822z00_6152);
																					}
																					BgL_arg2363z00_2955 =
																						(BgL_arg1820z00_6151 - 1L);
																				}
																				BgL_arg2361z00_2953 =
																					(BgL_arg2363z00_2955 +
																					(long) CINT(BgL_nz00_2925));
																			}
																			{	/* SawBbv/bbv-range.scm 335 */
																				obj_t BgL_list2362z00_2954;

																				BgL_list2362z00_2954 =
																					MAKE_YOUNG_PAIR(BINT
																					(BgL_arg2361z00_2953), BNIL);
																				return
																					BGl_formatz00zz__r4_output_6_10_3z00
																					(BGl_string2631z00zzsaw_bbvzd2rangezd2,
																					BgL_list2362z00_2954);
																			}
																		}
																	else
																		{	/* SawBbv/bbv-range.scm 334 */
																			if (INTEGERP(BgL_nz00_2925))
																				{	/* SawBbv/bbv-range.scm 336 */
																					return BgL_nz00_2925;
																				}
																			else
																				{	/* SawBbv/bbv-range.scm 337 */
																					bool_t BgL_test3163z00_9474;

																					{	/* SawBbv/bbv-range.scm 85 */
																						obj_t BgL_classz00_6161;

																						BgL_classz00_6161 =
																							BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
																						if (BGL_OBJECTP(BgL_nz00_2925))
																							{	/* SawBbv/bbv-range.scm 85 */
																								BgL_objectz00_bglt
																									BgL_arg1807z00_6163;
																								BgL_arg1807z00_6163 =
																									(BgL_objectz00_bglt)
																									(BgL_nz00_2925);
																								if (BGL_CONDEXPAND_ISA_ARCH64())
																									{	/* SawBbv/bbv-range.scm 85 */
																										long BgL_idxz00_6169;

																										BgL_idxz00_6169 =
																											BGL_OBJECT_INHERITANCE_NUM
																											(BgL_arg1807z00_6163);
																										BgL_test3163z00_9474 =
																											(VECTOR_REF
																											(BGl_za2inheritancesza2z00zz__objectz00,
																												(BgL_idxz00_6169 +
																													1L)) ==
																											BgL_classz00_6161);
																									}
																								else
																									{	/* SawBbv/bbv-range.scm 85 */
																										bool_t BgL_res2592z00_6194;

																										{	/* SawBbv/bbv-range.scm 85 */
																											obj_t BgL_oclassz00_6177;

																											{	/* SawBbv/bbv-range.scm 85 */
																												obj_t
																													BgL_arg1815z00_6185;
																												long
																													BgL_arg1816z00_6186;
																												BgL_arg1815z00_6185 =
																													(BGl_za2classesza2z00zz__objectz00);
																												{	/* SawBbv/bbv-range.scm 85 */
																													long
																														BgL_arg1817z00_6187;
																													BgL_arg1817z00_6187 =
																														BGL_OBJECT_CLASS_NUM
																														(BgL_arg1807z00_6163);
																													BgL_arg1816z00_6186 =
																														(BgL_arg1817z00_6187
																														- OBJECT_TYPE);
																												}
																												BgL_oclassz00_6177 =
																													VECTOR_REF
																													(BgL_arg1815z00_6185,
																													BgL_arg1816z00_6186);
																											}
																											{	/* SawBbv/bbv-range.scm 85 */
																												bool_t
																													BgL__ortest_1115z00_6178;
																												BgL__ortest_1115z00_6178
																													=
																													(BgL_classz00_6161 ==
																													BgL_oclassz00_6177);
																												if (BgL__ortest_1115z00_6178)
																													{	/* SawBbv/bbv-range.scm 85 */
																														BgL_res2592z00_6194
																															=
																															BgL__ortest_1115z00_6178;
																													}
																												else
																													{	/* SawBbv/bbv-range.scm 85 */
																														long
																															BgL_odepthz00_6179;
																														{	/* SawBbv/bbv-range.scm 85 */
																															obj_t
																																BgL_arg1804z00_6180;
																															BgL_arg1804z00_6180
																																=
																																(BgL_oclassz00_6177);
																															BgL_odepthz00_6179
																																=
																																BGL_CLASS_DEPTH
																																(BgL_arg1804z00_6180);
																														}
																														if (
																															(1L <
																																BgL_odepthz00_6179))
																															{	/* SawBbv/bbv-range.scm 85 */
																																obj_t
																																	BgL_arg1802z00_6182;
																																{	/* SawBbv/bbv-range.scm 85 */
																																	obj_t
																																		BgL_arg1803z00_6183;
																																	BgL_arg1803z00_6183
																																		=
																																		(BgL_oclassz00_6177);
																																	BgL_arg1802z00_6182
																																		=
																																		BGL_CLASS_ANCESTORS_REF
																																		(BgL_arg1803z00_6183,
																																		1L);
																																}
																																BgL_res2592z00_6194
																																	=
																																	(BgL_arg1802z00_6182
																																	==
																																	BgL_classz00_6161);
																															}
																														else
																															{	/* SawBbv/bbv-range.scm 85 */
																																BgL_res2592z00_6194
																																	=
																																	((bool_t) 0);
																															}
																													}
																											}
																										}
																										BgL_test3163z00_9474 =
																											BgL_res2592z00_6194;
																									}
																							}
																						else
																							{	/* SawBbv/bbv-range.scm 85 */
																								BgL_test3163z00_9474 =
																									((bool_t) 0);
																							}
																					}
																					if (BgL_test3163z00_9474)
																						{	/* SawBbv/bbv-range.scm 337 */
																							BgL_nz00_2918 = BgL_nz00_2925;
																							{	/* SawBbv/bbv-range.scm 322 */
																								obj_t BgL_arg2323z00_2920;
																								int BgL_arg2324z00_2921;

																								{	/* SawBbv/bbv-range.scm 322 */
																									obj_t BgL_arg2327z00_2924;

																									BgL_arg2327z00_2924 =
																										(((BgL_bbvzd2vlenzd2_bglt)
																											COBJECT((
																													(BgL_bbvzd2vlenzd2_bglt)
																													BgL_nz00_2918)))->
																										BgL_vecz00);
																									BgL_arg2323z00_2920 =
																										BGl_shapez00zztools_shapez00
																										(BgL_arg2327z00_2924);
																								}
																								BgL_arg2324z00_2921 =
																									(((BgL_bbvzd2vlenzd2_bglt)
																										COBJECT((
																												(BgL_bbvzd2vlenzd2_bglt)
																												BgL_nz00_2918)))->
																									BgL_offsetz00);
																								{	/* SawBbv/bbv-range.scm 321 */
																									obj_t BgL_list2325z00_2922;

																									{	/* SawBbv/bbv-range.scm 321 */
																										obj_t BgL_arg2326z00_2923;

																										BgL_arg2326z00_2923 =
																											MAKE_YOUNG_PAIR(BINT
																											(BgL_arg2324z00_2921),
																											BNIL);
																										BgL_list2325z00_2922 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2323z00_2920,
																											BgL_arg2326z00_2923);
																									}
																									return
																										BGl_formatz00zz__r4_output_6_10_3z00
																										(BGl_string2624z00zzsaw_bbvzd2rangezd2,
																										BgL_list2325z00_2922);
																								}
																							}
																						}
																					else
																						{	/* SawBbv/bbv-range.scm 337 */
																							if (REALP(BgL_nz00_2925))
																								{	/* SawBbv/bbv-range.scm 338 */
																									if (BGL_IS_INF(REAL_TO_DOUBLE
																											(BgL_nz00_2925)))
																										{	/* SawBbv/bbv-range.scm 339 */
																											if (
																												(REAL_TO_DOUBLE
																													(BgL_nz00_2925) <
																													((double) 0.0)))
																												{	/* SawBbv/bbv-range.scm 339 */
																													return
																														BGl_string2632z00zzsaw_bbvzd2rangezd2;
																												}
																											else
																												{	/* SawBbv/bbv-range.scm 339 */
																													return
																														BGl_string2633z00zzsaw_bbvzd2rangezd2;
																												}
																										}
																									else
																										{	/* SawBbv/bbv-range.scm 339 */
																											return BgL_nz00_2925;
																										}
																								}
																							else
																								{	/* SawBbv/bbv-range.scm 338 */
																									BGL_TAIL return
																										BGl_shapez00zztools_shapez00
																										(BgL_nz00_2925);
																								}
																						}
																				}
																		}
																}
														}
												}
										}
								}
						}
				}
			}
		}

	}



/* &object-print-bbv-vle1536 */
	obj_t BGl_z62objectzd2printzd2bbvzd2vle1536zb0zzsaw_bbvzd2rangezd2(obj_t
		BgL_envz00_6358, obj_t BgL_oz00_6359, obj_t BgL_pz00_6360,
		obj_t BgL_procz00_6361)
	{
		{	/* SawBbv/bbv-range.scm 75 */
			{	/* SawBbv/bbv-range.scm 76 */
				obj_t BgL_strz00_6450;

				{	/* SawBbv/bbv-range.scm 77 */
					obj_t BgL_arg2310z00_6451;
					int BgL_arg2311z00_6452;

					BgL_arg2310z00_6451 =
						BGl_shapez00zztools_shapez00(
						(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
									((BgL_bbvzd2vlenzd2_bglt) BgL_oz00_6359)))->BgL_vecz00));
					BgL_arg2311z00_6452 =
						(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
								((BgL_bbvzd2vlenzd2_bglt) BgL_oz00_6359)))->BgL_offsetz00);
					{	/* SawBbv/bbv-range.scm 76 */
						obj_t BgL_list2312z00_6453;

						{	/* SawBbv/bbv-range.scm 76 */
							obj_t BgL_arg2313z00_6454;

							BgL_arg2313z00_6454 =
								MAKE_YOUNG_PAIR(BINT(BgL_arg2311z00_6452), BNIL);
							BgL_list2312z00_6453 =
								MAKE_YOUNG_PAIR(BgL_arg2310z00_6451, BgL_arg2313z00_6454);
						}
						BgL_strz00_6450 =
							BGl_formatz00zz__r4_output_6_10_3z00
							(BGl_string2624z00zzsaw_bbvzd2rangezd2, BgL_list2312z00_6453);
				}}
				return bgl_display_obj(BgL_strz00_6450, BgL_pz00_6360);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2rangezd2(void)
	{
		{	/* SawBbv/bbv-range.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2634z00zzsaw_bbvzd2rangezd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2634z00zzsaw_bbvzd2rangezd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2634z00zzsaw_bbvzd2rangezd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2634z00zzsaw_bbvzd2rangezd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2634z00zzsaw_bbvzd2rangezd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2634z00zzsaw_bbvzd2rangezd2));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string2634z00zzsaw_bbvzd2rangezd2));
			BGl_modulezd2initializa7ationz75zzsaw_regsetz00(358079690L,
				BSTRING_TO_STRING(BGl_string2634z00zzsaw_bbvzd2rangezd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2debugzd2(120981929L,
				BSTRING_TO_STRING(BGl_string2634z00zzsaw_bbvzd2rangezd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(288263219L,
				BSTRING_TO_STRING(BGl_string2634z00zzsaw_bbvzd2rangezd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(242097300L,
				BSTRING_TO_STRING(BGl_string2634z00zzsaw_bbvzd2rangezd2));
			return
				BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(425659118L,
				BSTRING_TO_STRING(BGl_string2634z00zzsaw_bbvzd2rangezd2));
		}

	}

#ifdef __cplusplus
}
#endif
