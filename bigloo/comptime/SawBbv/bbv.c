/*===========================================================================*/
/*   (SawBbv/bbv.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawBbv/bbv.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_BBV_TYPE_DEFINITIONS
#define BGL_SAW_BBV_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_ifeqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_thenz00;
	}                  *BgL_rtl_ifeqz00_bglt;

	typedef struct BgL_rtl_ifnez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_thenz00;
	}                  *BgL_rtl_ifnez00_bglt;

	typedef struct BgL_rtl_goz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_toz00;
	}                *BgL_rtl_goz00_bglt;

	typedef struct BgL_rtl_nopz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_nopz00_bglt;

	typedef struct BgL_rtl_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                    *BgL_rtl_pragmaz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_rtl_regzf2razf2_bgl
	{
		int BgL_numz00;
		obj_t BgL_colorz00;
		obj_t BgL_coalescez00;
		int BgL_occurrencesz00;
		obj_t BgL_interferez00;
		obj_t BgL_interfere2z00;
	}                      *BgL_rtl_regzf2razf2_bglt;

	typedef struct BgL_regsetz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_lengthz00;
		int BgL_msiza7eza7;
		obj_t BgL_regvz00;
		obj_t BgL_reglz00;
		obj_t BgL_stringz00;
	}                *BgL_regsetz00_bglt;

	typedef struct BgL_blockvz00_bgl
	{
		obj_t BgL_versionsz00;
		obj_t BgL_genericz00;
		long BgL_z52markz52;
		obj_t BgL_mergez00;
	}                *BgL_blockvz00_bglt;

	typedef struct BgL_blocksz00_bgl
	{
		long BgL_z52markz52;
		obj_t BgL_z52hashz52;
		obj_t BgL_z52blacklistz52;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		struct BgL_blockz00_bgl *BgL_parentz00;
		long BgL_gccntz00;
		long BgL_gcmarkz00;
		obj_t BgL_mblockz00;
		obj_t BgL_creatorz00;
		obj_t BgL_mergesz00;
		bool_t BgL_asleepz00;
	}                *BgL_blocksz00_bglt;

	typedef struct BgL_bbvzd2ctxzd2_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_idz00;
		obj_t BgL_entriesz00;
	}                   *BgL_bbvzd2ctxzd2_bglt;


#endif													// BGL_SAW_BBV_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	static obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	extern obj_t BGl_rtl_pragmaz00zzsaw_defsz00;
	static BgL_rtl_insz00_bglt
		BGl_removezd2inszd2tempsz12ze70zf5zzsaw_bbvz00(BgL_rtl_insz00_bglt);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_bbvz00 = BUNSPEC;
	extern obj_t BGl_za2bbvzd2logza2zd2zzsaw_bbvzd2configzd2;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvz00(BgL_rtl_regz00_bglt);
	extern bool_t
		BGl_rtl_inszd2ifeqzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	extern obj_t BGl_za2maxzd2blockzd2mergezd2versionsza2zd2zzsaw_bbvzd2configzd2;
	static obj_t BGl_normaliza7ezd2gotoz12z67zzsaw_bbvz00(BgL_blockz00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_za2bbvzd2optimzd2vlengthza2z00zzsaw_bbvzd2configzd2;
	static obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt BGl_makezd2regsetzd2zzsaw_bbvz00(int,
		int, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvz00(BgL_rtl_regz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvz00(BgL_typez00_bglt, obj_t, obj_t,
		obj_t, obj_t, obj_t, int, obj_t, obj_t, int, obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern bool_t
		BGl_rtl_inszd2callzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_toplevelzd2initzd2zzsaw_bbvz00(void);
	extern obj_t BGl_za2bbvzd2dumpzd2jsonza2z00zzsaw_bbvzd2configzd2;
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvz00(obj_t, obj_t);
	static BgL_blockz00_bglt BGl_dumpzd2blockszd2zzsaw_bbvz00(long, obj_t,
		BgL_globalz00_bglt, obj_t, obj_t, obj_t, BgL_blockz00_bglt);
	static bool_t BGl_reorderzd2succsz12zc0zzsaw_bbvz00(obj_t);
	static obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvz00(obj_t, obj_t);
	extern obj_t
		BGl_rtl_inszd2switchzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_bbvz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvz00(obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvz00(obj_t);
	extern obj_t
		BGl_simplifyzd2branchz12zc0zzsaw_bbvzd2optimzd2(BgL_globalz00_bglt,
		BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvz00(BgL_rtl_regz00_bglt);
	extern obj_t BGl_removezd2gotoz12zc0zzsaw_bbvzd2optimzd2(BgL_globalz00_bglt,
		BgL_blockz00_bglt);
	static obj_t BGl_objectzd2initzd2zzsaw_bbvz00(void);
	BGL_EXPORTED_DECL obj_t BGl_bbvz00zzsaw_bbvz00(BgL_backendz00_bglt,
		BgL_globalz00_bglt, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvz00(BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_failz12z12zzsaw_bbvz00(BgL_blockz00_bglt);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvz00(BgL_rtl_regz00_bglt, obj_t);
	extern BgL_blockz00_bglt
		BGl_bbvzd2rootzd2blockz00zzsaw_bbvzd2specializa7ez75(BgL_blockz00_bglt,
		BgL_bbvzd2ctxzd2_bglt);
	BGL_IMPORT obj_t BGl_stringzd2containszd2zz__r4_strings_6_7z00(obj_t, obj_t,
		int);
	extern bool_t
		BGl_rtl_inszd2ifnezf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	extern obj_t BGl_regsetz00zzsaw_regsetz00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	static obj_t BGl_z62regsetzf3z91zzsaw_bbvz00(obj_t, obj_t);
	extern bool_t
		BGl_rtl_inszd2errorzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2;
	extern bool_t
		BGl_rtl_inszd2failzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	extern obj_t
		BGl_assertzd2contextz12zc0zzsaw_bbvzd2debugzd2(BgL_blockz00_bglt);
	extern obj_t BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(BgL_blockz00_bglt,
		obj_t);
	extern obj_t
		BGl_rtl_callzd2predicatezd2zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvz00(obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzsaw_bbvz00(obj_t, obj_t);
	extern obj_t BGl_rtl_nopz00zzsaw_defsz00;
	extern obj_t BGl_coalescez12z12zzsaw_bbvzd2optimzd2(BgL_globalz00_bglt,
		BgL_blockz00_bglt);
	static obj_t BGl_methodzd2initzd2zzsaw_bbvz00(void);
	static obj_t BGl_bbsetzd2conszd2zzsaw_bbvz00(BgL_blockz00_bglt, obj_t);
	extern obj_t
		BGl_bbvzd2livenessz12zc0zzsaw_bbvzd2livenesszd2(BgL_backendz00_bglt, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvz00(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_inszd2argsza2z70zzsaw_defsz00(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvz00(void);
	static BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_bbvz00(obj_t,
		obj_t);
	extern obj_t BGl_rtl_goz00zzsaw_defsz00;
	extern obj_t BGl_rtl_ifnez00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvz00(BgL_rtl_regz00_bglt);
	extern bool_t BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	extern obj_t
		BGl_logzd2blockszd2historyz00zzsaw_bbvzd2debugzd2(BgL_globalz00_bglt, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvz00(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvz00(obj_t, obj_t);
	BGL_EXPORTED_DECL int BGl_regsetzd2msiza7ez75zzsaw_bbvz00(BgL_regsetz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvz00(obj_t, obj_t);
	extern obj_t BGl_za2compilerzd2debugza2zd2zzengine_paramz00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static BgL_blockz00_bglt BGl_gcz12z12zzsaw_bbvz00(BgL_blockz00_bglt);
	extern obj_t BGl_dumpzd2jsonzd2cfgz00zzsaw_bbvzd2debugzd2(BgL_globalz00_bglt,
		obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_za2tracezd2levelza2zd2zzengine_paramz00;
	static obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL int BGl_regsetzd2lengthzd2zzsaw_bbvz00(BgL_regsetz00_bglt);
	extern obj_t BGl_za2bbvzd2optimzd2aliasza2z00zzsaw_bbvzd2configzd2;
	extern obj_t BGl_za2sawzd2bbvzf3za2z21zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvz00(BgL_regsetz00_bglt, obj_t);
	extern obj_t BGl_za2sawzd2bbvzd2functionsza2z00zzengine_paramz00;
	BGL_IMPORT obj_t bgl_remq_bang(obj_t, obj_t);
	extern obj_t BGl_getzd2bbzd2markz00zzsaw_bbvzd2typeszd2(void);
	static obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2profilezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2debugzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2optimzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2livenesszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2mergezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2utilszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2specializa7ez75(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_normaliza7ezd2ifeqz12z67zzsaw_bbvz00(BgL_blockz00_bglt);
	static obj_t BGl_markzd2mergez12zc0zzsaw_bbvz00(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvz00(BgL_rtl_regz00_bglt);
	extern obj_t BGl_logzd2blockszd2zzsaw_bbvzd2debugzd2(BgL_globalz00_bglt,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvz00(obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvz00(BgL_rtl_regz00_bglt,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_bbvz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvz00(void);
	extern BgL_bbvzd2ctxzd2_bglt
		BGl_paramszd2ze3ctxz31zzsaw_bbvzd2typeszd2(obj_t);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvz00(void);
	extern obj_t BGl_setzd2maxzd2labelz12z12zzsaw_bbvzd2utilszd2(obj_t);
	extern obj_t BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2;
	static obj_t BGl_gczd2rootszd2initz00zzsaw_bbvz00(void);
	extern obj_t BGl_dumpzd2cfgzd2zzsaw_bbvzd2debugzd2(BgL_globalz00_bglt, obj_t,
		obj_t, obj_t);
	static obj_t BGl_normaliza7ezd2movz12z67zzsaw_bbvz00(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvz00(BgL_rtl_regz00_bglt,
		int);
	static obj_t BGl_makezd2emptyzd2bbsetz00zzsaw_bbvz00(void);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvz00(BgL_rtl_regz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_regsetzf3zf3zzsaw_bbvz00(obj_t);
	extern obj_t BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2;
	extern obj_t BGl_startzd2bbvzd2cachez12z12zzsaw_bbvzd2cachezd2(void);
	static obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvz00(BgL_regsetz00_bglt);
	static obj_t BGl_normaliza7ezd2typecheckz12z67zzsaw_bbvz00(BgL_blockz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_removezd2innerzd2movz12ze70zf5zzsaw_bbvz00(obj_t);
	extern obj_t BGl_removezd2nopz12zc0zzsaw_bbvzd2optimzd2(BgL_globalz00_bglt,
		BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvz00(BgL_rtl_regz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvz00(BgL_rtl_regz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt BGl_regsetzd2nilzd2zzsaw_bbvz00(void);
	static BgL_blockz00_bglt
		BGl_removezd2tempsz12zc0zzsaw_bbvz00(BgL_blockz00_bglt);
	static obj_t BGl_loopze70ze7zzsaw_bbvz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvz00(obj_t, obj_t);
	extern obj_t BGl_rtl_regzf2razf2zzsaw_regsetz00;
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31753ze3ze5zzsaw_bbvz00(obj_t);
	static obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvz00(obj_t, obj_t);
	extern bool_t
		BGl_rtl_inszd2movzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvz00(BgL_rtl_regz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_regsetzd2reglzd2zzsaw_bbvz00(BgL_regsetz00_bglt);
	static obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvz00(obj_t, obj_t);
	extern obj_t BGl_blockzd2ze3blockzd2listze3zzsaw_bbvzd2utilszd2(obj_t,
		BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_regsetzd2regvzd2zzsaw_bbvz00(BgL_regsetz00_bglt);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvz00(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvz00(BgL_regsetz00_bglt, int);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static bool_t BGl_sidezd2effectzd2freezf3ze70z14zzsaw_bbvz00(obj_t);
	static obj_t BGl_z62bbvz62zzsaw_bbvz00(obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvz00(obj_t, obj_t);
	extern obj_t BGl_profilez12z12zzsaw_bbvzd2profilezd2(BgL_blockz00_bglt);
	static obj_t __cnst[2];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razd2namezd2envzf2zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72382za7,
		BGl_z62rtl_regzf2razd2namez42zzsaw_bbvz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2nilzd2envz00zzsaw_bbvz00,
		BgL_bgl_za762regsetza7d2nilza72383za7, BGl_z62regsetzd2nilzb0zzsaw_bbvz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2setz12zd2envz32zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72384za7,
		BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2envzf2zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72385za7,
		BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2344z00zzsaw_bbvz00,
		BgL_bgl_string2344za700za7za7s2386za7, " vlen", 5);
	      DEFINE_STRING(BGl_string2345z00zzsaw_bbvz00,
		BgL_bgl_string2345za700za7za7s2387za7, "", 0);
	      DEFINE_STRING(BGl_string2346z00zzsaw_bbvz00,
		BgL_bgl_string2346za700za7za7s2388za7, " alias", 6);
	      DEFINE_STRING(BGl_string2347z00zzsaw_bbvz00,
		BgL_bgl_string2347za700za7za7s2389za7, " cleanup", 8);
	      DEFINE_STRING(BGl_string2348z00zzsaw_bbvz00,
		BgL_bgl_string2348za700za7za7s2390za7, ")", 1);
	      DEFINE_STRING(BGl_string2349z00zzsaw_bbvz00,
		BgL_bgl_string2349za700za7za7s2391za7, " (", 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2setz12zd2envzc1zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72392za7,
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2setz12zd2envz32zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72393za7,
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razd2colorzd2envzf2zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72394za7,
		BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2350z00zzsaw_bbvz00,
		BgL_bgl_string2350za700za7za7s2395za7, "        bbv ", 12);
	      DEFINE_STRING(BGl_string2351z00zzsaw_bbvz00,
		BgL_bgl_string2351za700za7za7s2396za7, ".plain.cfg", 10);
	      DEFINE_STRING(BGl_string2352z00zzsaw_bbvz00,
		BgL_bgl_string2352za700za7za7s2397za7, ".reorder.cfg", 12);
	      DEFINE_STRING(BGl_string2353z00zzsaw_bbvz00,
		BgL_bgl_string2353za700za7za7s2398za7, ".failure.cfg", 12);
	      DEFINE_STRING(BGl_string2354z00zzsaw_bbvz00,
		BgL_bgl_string2354za700za7za7s2399za7, ".typecheck.cfg", 14);
	      DEFINE_STRING(BGl_string2355z00zzsaw_bbvz00,
		BgL_bgl_string2355za700za7za7s2400za7, ".goto.cfg", 9);
	      DEFINE_STRING(BGl_string2356z00zzsaw_bbvz00,
		BgL_bgl_string2356za700za7za7s2401za7, ".ifeq.cfg", 9);
	      DEFINE_STRING(BGl_string2357z00zzsaw_bbvz00,
		BgL_bgl_string2357za700za7za7s2402za7, ".mov.cfg", 8);
	      DEFINE_STRING(BGl_string2358z00zzsaw_bbvz00,
		BgL_bgl_string2358za700za7za7s2403za7, ".liveness.cfg", 13);
	      DEFINE_STRING(BGl_string2359z00zzsaw_bbvz00,
		BgL_bgl_string2359za700za7za7s2404za7, "specialize", 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2setz12zd2envz32zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72405za7,
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2regvzd2envz00zzsaw_bbvz00,
		BgL_bgl_za762regsetza7d2regv2406z00, BGl_z62regsetzd2regvzb0zzsaw_bbvz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2360z00zzsaw_bbvz00,
		BgL_bgl_string2360za700za7za7s2407za7, "profile", 7);
	      DEFINE_STRING(BGl_string2361z00zzsaw_bbvz00,
		BgL_bgl_string2361za700za7za7s2408za7, "assert", 6);
	      DEFINE_STRING(BGl_string2362z00zzsaw_bbvz00,
		BgL_bgl_string2362za700za7za7s2409za7, "coalesce", 8);
	      DEFINE_STRING(BGl_string2363z00zzsaw_bbvz00,
		BgL_bgl_string2363za700za7za7s2410za7, "simplify1", 9);
	      DEFINE_STRING(BGl_string2364z00zzsaw_bbvz00,
		BgL_bgl_string2364za700za7za7s2411za7, "goto", 4);
	      DEFINE_STRING(BGl_string2365z00zzsaw_bbvz00,
		BgL_bgl_string2365za700za7za7s2412za7, "nop", 3);
	      DEFINE_STRING(BGl_string2366z00zzsaw_bbvz00,
		BgL_bgl_string2366za700za7za7s2413za7, "simplify2", 9);
	      DEFINE_STRING(BGl_string2367z00zzsaw_bbvz00,
		BgL_bgl_string2367za700za7za7s2414za7, " -> ", 4);
	      DEFINE_STRING(BGl_string2368z00zzsaw_bbvz00,
		BgL_bgl_string2368za700za7za7s2415za7, " ", 1);
	      DEFINE_STRING(BGl_string2369z00zzsaw_bbvz00,
		BgL_bgl_string2369za700za7za7s2416za7, "\n", 1);
	      DEFINE_STRING(BGl_string2370z00zzsaw_bbvz00,
		BgL_bgl_string2370za700za7za7s2417za7, "bbv", 3);
	      DEFINE_STRING(BGl_string2371z00zzsaw_bbvz00,
		BgL_bgl_string2371za700za7za7s2418za7, ".~a.cfg", 7);
	      DEFINE_STRING(BGl_string2372z00zzsaw_bbvz00,
		BgL_bgl_string2372za700za7za7s2419za7, ".~a.json", 8);
	      DEFINE_STRING(BGl_string2373z00zzsaw_bbvz00,
		BgL_bgl_string2373za700za7za7s2420za7, "gc!", 3);
	      DEFINE_STRING(BGl_string2374z00zzsaw_bbvz00,
		BgL_bgl_string2374za700za7za7s2421za7, "gc", 2);
	      DEFINE_STRING(BGl_string2375z00zzsaw_bbvz00,
		BgL_bgl_string2375za700za7za7s2422za7, "normalize-ifeq!", 15);
	      DEFINE_STRING(BGl_string2376z00zzsaw_bbvz00,
		BgL_bgl_string2376za700za7za7s2423za7, "bad block (no instruction)", 26);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2setz12zd2envzc0zzsaw_bbvz00,
		BgL_bgl_za762regsetza7d2leng2424z00,
		BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2377z00zzsaw_bbvz00,
		BgL_bgl_string2377za700za7za7s2425za7, "bad block", 9);
	      DEFINE_STRING(BGl_string2378z00zzsaw_bbvz00,
		BgL_bgl_string2378za700za7za7s2426za7, "exit(0)", 7);
	      DEFINE_STRING(BGl_string2379z00zzsaw_bbvz00,
		BgL_bgl_string2379za700za7za7s2427za7, "saw_bbv", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2msiza7ezd2envza7zzsaw_bbvz00,
		BgL_bgl_za762regsetza7d2msiza72428za7,
		BGl_z62regsetzd2msiza7ez17zzsaw_bbvz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2380z00zzsaw_bbvz00,
		BgL_bgl_string2380za700za7za7s2429za7, "bigloo-c bbset ", 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razd2typezd2envzf2zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72430za7,
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2setz12zd2envz32zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72431za7,
		BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2envzf2zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72432za7,
		BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bbvzd2envzd2zzsaw_bbvz00,
		BgL_bgl_za762bbvza762za7za7saw_b2433z00, BGl_z62bbvz62zzsaw_bbvz00, 0L,
		BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razd2nilzd2envzf2zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72434za7,
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razd2varzd2envzf2zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72435za7,
		BGl_z62rtl_regzf2razd2varz42zzsaw_bbvz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razd2numzd2envzf2zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72436za7,
		BGl_z62rtl_regzf2razd2numz42zzsaw_bbvz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2setz12zd2envzc0zzsaw_bbvz00,
		BgL_bgl_za762regsetza7d2stri2437z00,
		BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2setz12zd2envz32zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72438za7,
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2envz01zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72439za7,
		BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2stringzd2envz00zzsaw_bbvz00,
		BgL_bgl_za762regsetza7d2stri2440z00, BGl_z62regsetzd2stringzb0zzsaw_bbvz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2setz12zd2envz32zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72441za7,
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_regzf2razd2envzf2zzsaw_bbvz00,
		BgL_bgl_za762makeza7d2rtl_re2442z00,
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvz00, 0L, BUNSPEC, 12);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razf3zd2envzd3zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72443za7,
		BGl_z62rtl_regzf2razf3z63zzsaw_bbvz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2lengthzd2envz00zzsaw_bbvz00,
		BgL_bgl_za762regsetza7d2leng2444z00, BGl_z62regsetzd2lengthzb0zzsaw_bbvz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2envzf2zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72445za7,
		BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2envzf2zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72446za7,
		BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2hardwarezd2envzf2zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72447za7,
		BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2regsetzd2envz00zzsaw_bbvz00,
		BgL_bgl_za762makeza7d2regset2448z00, BGl_z62makezd2regsetzb0zzsaw_bbvz00,
		0L, BUNSPEC, 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzf3zd2envz21zzsaw_bbvz00,
		BgL_bgl_za762regsetza7f3za791za72449z00, BGl_z62regsetzf3z91zzsaw_bbvz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razd2keyzd2envzf2zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72450za7,
		BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2setz12zd2envz32zzsaw_bbvz00,
		BgL_bgl_za762rtl_regza7f2raza72451za7,
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2reglzd2envz00zzsaw_bbvz00,
		BgL_bgl_za762regsetza7d2regl2452z00, BGl_z62regsetzd2reglzb0zzsaw_bbvz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_bbvz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvz00(long
		BgL_checksumz00_4140, char *BgL_fromz00_4141)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_bbvz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_bbvz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_bbvz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_bbvz00();
					BGl_cnstzd2initzd2zzsaw_bbvz00();
					BGl_importedzd2moduleszd2initz00zzsaw_bbvz00();
					return BGl_toplevelzd2initzd2zzsaw_bbvz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvz00(void)
	{
		{	/* SawBbv/bbv.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_bbv");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "saw_bbv");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_bbv");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "saw_bbv");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_bbv");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "saw_bbv");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_bbv");
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "saw_bbv");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "saw_bbv");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "saw_bbv");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_bbv");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_bbv");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "saw_bbv");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_bbvz00(void)
	{
		{	/* SawBbv/bbv.scm 15 */
			{	/* SawBbv/bbv.scm 15 */
				obj_t BgL_cportz00_4081;

				{	/* SawBbv/bbv.scm 15 */
					obj_t BgL_stringz00_4088;

					BgL_stringz00_4088 = BGl_string2380z00zzsaw_bbvz00;
					{	/* SawBbv/bbv.scm 15 */
						obj_t BgL_startz00_4089;

						BgL_startz00_4089 = BINT(0L);
						{	/* SawBbv/bbv.scm 15 */
							obj_t BgL_endz00_4090;

							BgL_endz00_4090 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4088)));
							{	/* SawBbv/bbv.scm 15 */

								BgL_cportz00_4081 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4088, BgL_startz00_4089, BgL_endz00_4090);
				}}}}
				{
					long BgL_iz00_4082;

					BgL_iz00_4082 = 1L;
				BgL_loopz00_4083:
					if ((BgL_iz00_4082 == -1L))
						{	/* SawBbv/bbv.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* SawBbv/bbv.scm 15 */
							{	/* SawBbv/bbv.scm 15 */
								obj_t BgL_arg2381z00_4084;

								{	/* SawBbv/bbv.scm 15 */

									{	/* SawBbv/bbv.scm 15 */
										obj_t BgL_locationz00_4086;

										BgL_locationz00_4086 = BBOOL(((bool_t) 0));
										{	/* SawBbv/bbv.scm 15 */

											BgL_arg2381z00_4084 =
												BGl_readz00zz__readerz00(BgL_cportz00_4081,
												BgL_locationz00_4086);
										}
									}
								}
								{	/* SawBbv/bbv.scm 15 */
									int BgL_tmpz00_4172;

									BgL_tmpz00_4172 = (int) (BgL_iz00_4082);
									CNST_TABLE_SET(BgL_tmpz00_4172, BgL_arg2381z00_4084);
							}}
							{	/* SawBbv/bbv.scm 15 */
								int BgL_auxz00_4087;

								BgL_auxz00_4087 = (int) ((BgL_iz00_4082 - 1L));
								{
									long BgL_iz00_4177;

									BgL_iz00_4177 = (long) (BgL_auxz00_4087);
									BgL_iz00_4082 = BgL_iz00_4177;
									goto BgL_loopz00_4083;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_bbvz00(void)
	{
		{	/* SawBbv/bbv.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_bbvz00(void)
	{
		{	/* SawBbv/bbv.scm 15 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzsaw_bbvz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_2161;

				BgL_headz00_2161 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2162;
					obj_t BgL_tailz00_2163;

					BgL_prevz00_2162 = BgL_headz00_2161;
					BgL_tailz00_2163 = BgL_l1z00_1;
				BgL_loopz00_2164:
					if (PAIRP(BgL_tailz00_2163))
						{
							obj_t BgL_newzd2prevzd2_2166;

							BgL_newzd2prevzd2_2166 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2163), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2162, BgL_newzd2prevzd2_2166);
							{
								obj_t BgL_tailz00_4187;
								obj_t BgL_prevz00_4186;

								BgL_prevz00_4186 = BgL_newzd2prevzd2_2166;
								BgL_tailz00_4187 = CDR(BgL_tailz00_2163);
								BgL_tailz00_2163 = BgL_tailz00_4187;
								BgL_prevz00_2162 = BgL_prevz00_4186;
								goto BgL_loopz00_2164;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_2161);
				}
			}
		}

	}



/* make-rtl_reg/ra */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvz00(BgL_typez00_bglt BgL_type1179z00_3,
		obj_t BgL_var1180z00_4, obj_t BgL_onexprzf31181zf3_5,
		obj_t BgL_name1182z00_6, obj_t BgL_key1183z00_7,
		obj_t BgL_hardware1184z00_8, int BgL_num1185z00_9,
		obj_t BgL_color1186z00_10, obj_t BgL_coalesce1187z00_11,
		int BgL_occurrences1188z00_12, obj_t BgL_interfere1189z00_13,
		obj_t BgL_interfere21190z00_14)
	{
		{	/* SawMill/regset.sch 55 */
			{	/* SawMill/regset.sch 55 */
				BgL_rtl_regz00_bglt BgL_new1166z00_4092;

				{	/* SawMill/regset.sch 55 */
					BgL_rtl_regz00_bglt BgL_tmp1164z00_4093;
					BgL_rtl_regzf2razf2_bglt BgL_wide1165z00_4094;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_4190;

						{	/* SawMill/regset.sch 55 */
							BgL_rtl_regz00_bglt BgL_new1163z00_4095;

							BgL_new1163z00_4095 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/regset.sch 55 */
								long BgL_arg1546z00_4096;

								BgL_arg1546z00_4096 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1163z00_4095),
									BgL_arg1546z00_4096);
							}
							{	/* SawMill/regset.sch 55 */
								BgL_objectz00_bglt BgL_tmpz00_4195;

								BgL_tmpz00_4195 = ((BgL_objectz00_bglt) BgL_new1163z00_4095);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4195, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1163z00_4095);
							BgL_auxz00_4190 = BgL_new1163z00_4095;
						}
						BgL_tmp1164z00_4093 = ((BgL_rtl_regz00_bglt) BgL_auxz00_4190);
					}
					BgL_wide1165z00_4094 =
						((BgL_rtl_regzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regzf2razf2_bgl))));
					{	/* SawMill/regset.sch 55 */
						obj_t BgL_auxz00_4203;
						BgL_objectz00_bglt BgL_tmpz00_4201;

						BgL_auxz00_4203 = ((obj_t) BgL_wide1165z00_4094);
						BgL_tmpz00_4201 = ((BgL_objectz00_bglt) BgL_tmp1164z00_4093);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4201, BgL_auxz00_4203);
					}
					((BgL_objectz00_bglt) BgL_tmp1164z00_4093);
					{	/* SawMill/regset.sch 55 */
						long BgL_arg1544z00_4097;

						BgL_arg1544z00_4097 =
							BGL_CLASS_NUM(BGl_rtl_regzf2razf2zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1164z00_4093), BgL_arg1544z00_4097);
					}
					BgL_new1166z00_4092 = ((BgL_rtl_regz00_bglt) BgL_tmp1164z00_4093);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1166z00_4092)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1179z00_3), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_4092)))->BgL_varz00) =
					((obj_t) BgL_var1180z00_4), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_4092)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31181zf3_5), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_4092)))->BgL_namez00) =
					((obj_t) BgL_name1182z00_6), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_4092)))->BgL_keyz00) =
					((obj_t) BgL_key1183z00_7), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_4092)))->BgL_debugnamez00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_4092)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1184z00_8), BUNSPEC);
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4225;

					{
						obj_t BgL_auxz00_4226;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_4227;

							BgL_tmpz00_4227 = ((BgL_objectz00_bglt) BgL_new1166z00_4092);
							BgL_auxz00_4226 = BGL_OBJECT_WIDENING(BgL_tmpz00_4227);
						}
						BgL_auxz00_4225 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4226);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4225))->BgL_numz00) =
						((int) BgL_num1185z00_9), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4232;

					{
						obj_t BgL_auxz00_4233;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_4234;

							BgL_tmpz00_4234 = ((BgL_objectz00_bglt) BgL_new1166z00_4092);
							BgL_auxz00_4233 = BGL_OBJECT_WIDENING(BgL_tmpz00_4234);
						}
						BgL_auxz00_4232 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4233);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4232))->
							BgL_colorz00) = ((obj_t) BgL_color1186z00_10), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4239;

					{
						obj_t BgL_auxz00_4240;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_4241;

							BgL_tmpz00_4241 = ((BgL_objectz00_bglt) BgL_new1166z00_4092);
							BgL_auxz00_4240 = BGL_OBJECT_WIDENING(BgL_tmpz00_4241);
						}
						BgL_auxz00_4239 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4240);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4239))->
							BgL_coalescez00) = ((obj_t) BgL_coalesce1187z00_11), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4246;

					{
						obj_t BgL_auxz00_4247;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_4248;

							BgL_tmpz00_4248 = ((BgL_objectz00_bglt) BgL_new1166z00_4092);
							BgL_auxz00_4247 = BGL_OBJECT_WIDENING(BgL_tmpz00_4248);
						}
						BgL_auxz00_4246 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4247);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4246))->
							BgL_occurrencesz00) = ((int) BgL_occurrences1188z00_12), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4253;

					{
						obj_t BgL_auxz00_4254;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_4255;

							BgL_tmpz00_4255 = ((BgL_objectz00_bglt) BgL_new1166z00_4092);
							BgL_auxz00_4254 = BGL_OBJECT_WIDENING(BgL_tmpz00_4255);
						}
						BgL_auxz00_4253 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4254);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4253))->
							BgL_interferez00) = ((obj_t) BgL_interfere1189z00_13), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4260;

					{
						obj_t BgL_auxz00_4261;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_4262;

							BgL_tmpz00_4262 = ((BgL_objectz00_bglt) BgL_new1166z00_4092);
							BgL_auxz00_4261 = BGL_OBJECT_WIDENING(BgL_tmpz00_4262);
						}
						BgL_auxz00_4260 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4261);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4260))->
							BgL_interfere2z00) = ((obj_t) BgL_interfere21190z00_14), BUNSPEC);
				}
				return BgL_new1166z00_4092;
			}
		}

	}



/* &make-rtl_reg/ra */
	BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvz00(obj_t
		BgL_envz00_3983, obj_t BgL_type1179z00_3984, obj_t BgL_var1180z00_3985,
		obj_t BgL_onexprzf31181zf3_3986, obj_t BgL_name1182z00_3987,
		obj_t BgL_key1183z00_3988, obj_t BgL_hardware1184z00_3989,
		obj_t BgL_num1185z00_3990, obj_t BgL_color1186z00_3991,
		obj_t BgL_coalesce1187z00_3992, obj_t BgL_occurrences1188z00_3993,
		obj_t BgL_interfere1189z00_3994, obj_t BgL_interfere21190z00_3995)
	{
		{	/* SawMill/regset.sch 55 */
			return
				BGl_makezd2rtl_regzf2raz20zzsaw_bbvz00(
				((BgL_typez00_bglt) BgL_type1179z00_3984), BgL_var1180z00_3985,
				BgL_onexprzf31181zf3_3986, BgL_name1182z00_3987, BgL_key1183z00_3988,
				BgL_hardware1184z00_3989, CINT(BgL_num1185z00_3990),
				BgL_color1186z00_3991, BgL_coalesce1187z00_3992,
				CINT(BgL_occurrences1188z00_3993), BgL_interfere1189z00_3994,
				BgL_interfere21190z00_3995);
		}

	}



/* rtl_reg/ra? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvz00(obj_t
		BgL_objz00_15)
	{
		{	/* SawMill/regset.sch 56 */
			{	/* SawMill/regset.sch 56 */
				obj_t BgL_classz00_4098;

				BgL_classz00_4098 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_15))
					{	/* SawMill/regset.sch 56 */
						BgL_objectz00_bglt BgL_arg1807z00_4099;

						BgL_arg1807z00_4099 = (BgL_objectz00_bglt) (BgL_objz00_15);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 56 */
								long BgL_idxz00_4100;

								BgL_idxz00_4100 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4099);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4100 + 2L)) == BgL_classz00_4098);
							}
						else
							{	/* SawMill/regset.sch 56 */
								bool_t BgL_res2322z00_4103;

								{	/* SawMill/regset.sch 56 */
									obj_t BgL_oclassz00_4104;

									{	/* SawMill/regset.sch 56 */
										obj_t BgL_arg1815z00_4105;
										long BgL_arg1816z00_4106;

										BgL_arg1815z00_4105 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 56 */
											long BgL_arg1817z00_4107;

											BgL_arg1817z00_4107 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4099);
											BgL_arg1816z00_4106 = (BgL_arg1817z00_4107 - OBJECT_TYPE);
										}
										BgL_oclassz00_4104 =
											VECTOR_REF(BgL_arg1815z00_4105, BgL_arg1816z00_4106);
									}
									{	/* SawMill/regset.sch 56 */
										bool_t BgL__ortest_1115z00_4108;

										BgL__ortest_1115z00_4108 =
											(BgL_classz00_4098 == BgL_oclassz00_4104);
										if (BgL__ortest_1115z00_4108)
											{	/* SawMill/regset.sch 56 */
												BgL_res2322z00_4103 = BgL__ortest_1115z00_4108;
											}
										else
											{	/* SawMill/regset.sch 56 */
												long BgL_odepthz00_4109;

												{	/* SawMill/regset.sch 56 */
													obj_t BgL_arg1804z00_4110;

													BgL_arg1804z00_4110 = (BgL_oclassz00_4104);
													BgL_odepthz00_4109 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4110);
												}
												if ((2L < BgL_odepthz00_4109))
													{	/* SawMill/regset.sch 56 */
														obj_t BgL_arg1802z00_4111;

														{	/* SawMill/regset.sch 56 */
															obj_t BgL_arg1803z00_4112;

															BgL_arg1803z00_4112 = (BgL_oclassz00_4104);
															BgL_arg1802z00_4111 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4112,
																2L);
														}
														BgL_res2322z00_4103 =
															(BgL_arg1802z00_4111 == BgL_classz00_4098);
													}
												else
													{	/* SawMill/regset.sch 56 */
														BgL_res2322z00_4103 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2322z00_4103;
							}
					}
				else
					{	/* SawMill/regset.sch 56 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_reg/ra? */
	obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvz00(obj_t BgL_envz00_3996,
		obj_t BgL_objz00_3997)
	{
		{	/* SawMill/regset.sch 56 */
			return BBOOL(BGl_rtl_regzf2razf3z01zzsaw_bbvz00(BgL_objz00_3997));
		}

	}



/* rtl_reg/ra-nil */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvz00(void)
	{
		{	/* SawMill/regset.sch 57 */
			{	/* SawMill/regset.sch 57 */
				obj_t BgL_classz00_3319;

				BgL_classz00_3319 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				{	/* SawMill/regset.sch 57 */
					obj_t BgL__ortest_1117z00_3320;

					BgL__ortest_1117z00_3320 = BGL_CLASS_NIL(BgL_classz00_3319);
					if (CBOOL(BgL__ortest_1117z00_3320))
						{	/* SawMill/regset.sch 57 */
							return ((BgL_rtl_regz00_bglt) BgL__ortest_1117z00_3320);
						}
					else
						{	/* SawMill/regset.sch 57 */
							return
								((BgL_rtl_regz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3319));
						}
				}
			}
		}

	}



/* &rtl_reg/ra-nil */
	BgL_rtl_regz00_bglt BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvz00(obj_t
		BgL_envz00_3998)
	{
		{	/* SawMill/regset.sch 57 */
			return BGl_rtl_regzf2razd2nilz20zzsaw_bbvz00();
		}

	}



/* rtl_reg/ra-interfere2 */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvz00(BgL_rtl_regz00_bglt
		BgL_oz00_16)
	{
		{	/* SawMill/regset.sch 58 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4302;

				{
					obj_t BgL_auxz00_4303;

					{	/* SawMill/regset.sch 58 */
						BgL_objectz00_bglt BgL_tmpz00_4304;

						BgL_tmpz00_4304 = ((BgL_objectz00_bglt) BgL_oz00_16);
						BgL_auxz00_4303 = BGL_OBJECT_WIDENING(BgL_tmpz00_4304);
					}
					BgL_auxz00_4302 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4303);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4302))->
					BgL_interfere2z00);
			}
		}

	}



/* &rtl_reg/ra-interfere2 */
	obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvz00(obj_t BgL_envz00_3999,
		obj_t BgL_oz00_4000)
	{
		{	/* SawMill/regset.sch 58 */
			return
				BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4000));
		}

	}



/* rtl_reg/ra-interfere2-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvz00(BgL_rtl_regz00_bglt
		BgL_oz00_17, obj_t BgL_vz00_18)
	{
		{	/* SawMill/regset.sch 59 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4311;

				{
					obj_t BgL_auxz00_4312;

					{	/* SawMill/regset.sch 59 */
						BgL_objectz00_bglt BgL_tmpz00_4313;

						BgL_tmpz00_4313 = ((BgL_objectz00_bglt) BgL_oz00_17);
						BgL_auxz00_4312 = BGL_OBJECT_WIDENING(BgL_tmpz00_4313);
					}
					BgL_auxz00_4311 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4312);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4311))->
						BgL_interfere2z00) = ((obj_t) BgL_vz00_18), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere2-set! */
	obj_t BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvz00(obj_t
		BgL_envz00_4001, obj_t BgL_oz00_4002, obj_t BgL_vz00_4003)
	{
		{	/* SawMill/regset.sch 59 */
			return
				BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4002), BgL_vz00_4003);
		}

	}



/* rtl_reg/ra-interfere */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvz00(BgL_rtl_regz00_bglt BgL_oz00_19)
	{
		{	/* SawMill/regset.sch 60 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4320;

				{
					obj_t BgL_auxz00_4321;

					{	/* SawMill/regset.sch 60 */
						BgL_objectz00_bglt BgL_tmpz00_4322;

						BgL_tmpz00_4322 = ((BgL_objectz00_bglt) BgL_oz00_19);
						BgL_auxz00_4321 = BGL_OBJECT_WIDENING(BgL_tmpz00_4322);
					}
					BgL_auxz00_4320 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4321);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4320))->
					BgL_interferez00);
			}
		}

	}



/* &rtl_reg/ra-interfere */
	obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvz00(obj_t BgL_envz00_4004,
		obj_t BgL_oz00_4005)
	{
		{	/* SawMill/regset.sch 60 */
			return
				BGl_rtl_regzf2razd2interferez20zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4005));
		}

	}



/* rtl_reg/ra-interfere-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvz00(BgL_rtl_regz00_bglt
		BgL_oz00_20, obj_t BgL_vz00_21)
	{
		{	/* SawMill/regset.sch 61 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4329;

				{
					obj_t BgL_auxz00_4330;

					{	/* SawMill/regset.sch 61 */
						BgL_objectz00_bglt BgL_tmpz00_4331;

						BgL_tmpz00_4331 = ((BgL_objectz00_bglt) BgL_oz00_20);
						BgL_auxz00_4330 = BGL_OBJECT_WIDENING(BgL_tmpz00_4331);
					}
					BgL_auxz00_4329 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4330);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4329))->
						BgL_interferez00) = ((obj_t) BgL_vz00_21), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere-set! */
	obj_t BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvz00(obj_t
		BgL_envz00_4006, obj_t BgL_oz00_4007, obj_t BgL_vz00_4008)
	{
		{	/* SawMill/regset.sch 61 */
			return
				BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4007), BgL_vz00_4008);
		}

	}



/* rtl_reg/ra-occurrences */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvz00(BgL_rtl_regz00_bglt
		BgL_oz00_22)
	{
		{	/* SawMill/regset.sch 62 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4338;

				{
					obj_t BgL_auxz00_4339;

					{	/* SawMill/regset.sch 62 */
						BgL_objectz00_bglt BgL_tmpz00_4340;

						BgL_tmpz00_4340 = ((BgL_objectz00_bglt) BgL_oz00_22);
						BgL_auxz00_4339 = BGL_OBJECT_WIDENING(BgL_tmpz00_4340);
					}
					BgL_auxz00_4338 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4339);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4338))->
					BgL_occurrencesz00);
			}
		}

	}



/* &rtl_reg/ra-occurrences */
	obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvz00(obj_t BgL_envz00_4009,
		obj_t BgL_oz00_4010)
	{
		{	/* SawMill/regset.sch 62 */
			return
				BINT(BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvz00(
					((BgL_rtl_regz00_bglt) BgL_oz00_4010)));
		}

	}



/* rtl_reg/ra-occurrences-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvz00(BgL_rtl_regz00_bglt
		BgL_oz00_23, int BgL_vz00_24)
	{
		{	/* SawMill/regset.sch 63 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4348;

				{
					obj_t BgL_auxz00_4349;

					{	/* SawMill/regset.sch 63 */
						BgL_objectz00_bglt BgL_tmpz00_4350;

						BgL_tmpz00_4350 = ((BgL_objectz00_bglt) BgL_oz00_23);
						BgL_auxz00_4349 = BGL_OBJECT_WIDENING(BgL_tmpz00_4350);
					}
					BgL_auxz00_4348 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4349);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4348))->
						BgL_occurrencesz00) = ((int) BgL_vz00_24), BUNSPEC);
		}}

	}



/* &rtl_reg/ra-occurrences-set! */
	obj_t BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvz00(obj_t
		BgL_envz00_4011, obj_t BgL_oz00_4012, obj_t BgL_vz00_4013)
	{
		{	/* SawMill/regset.sch 63 */
			return
				BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4012), CINT(BgL_vz00_4013));
		}

	}



/* rtl_reg/ra-coalesce */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvz00(BgL_rtl_regz00_bglt BgL_oz00_25)
	{
		{	/* SawMill/regset.sch 64 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4358;

				{
					obj_t BgL_auxz00_4359;

					{	/* SawMill/regset.sch 64 */
						BgL_objectz00_bglt BgL_tmpz00_4360;

						BgL_tmpz00_4360 = ((BgL_objectz00_bglt) BgL_oz00_25);
						BgL_auxz00_4359 = BGL_OBJECT_WIDENING(BgL_tmpz00_4360);
					}
					BgL_auxz00_4358 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4359);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4358))->
					BgL_coalescez00);
			}
		}

	}



/* &rtl_reg/ra-coalesce */
	obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvz00(obj_t BgL_envz00_4014,
		obj_t BgL_oz00_4015)
	{
		{	/* SawMill/regset.sch 64 */
			return
				BGl_rtl_regzf2razd2coalescez20zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4015));
		}

	}



/* rtl_reg/ra-coalesce-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvz00(BgL_rtl_regz00_bglt
		BgL_oz00_26, obj_t BgL_vz00_27)
	{
		{	/* SawMill/regset.sch 65 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4367;

				{
					obj_t BgL_auxz00_4368;

					{	/* SawMill/regset.sch 65 */
						BgL_objectz00_bglt BgL_tmpz00_4369;

						BgL_tmpz00_4369 = ((BgL_objectz00_bglt) BgL_oz00_26);
						BgL_auxz00_4368 = BGL_OBJECT_WIDENING(BgL_tmpz00_4369);
					}
					BgL_auxz00_4367 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4368);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4367))->
						BgL_coalescez00) = ((obj_t) BgL_vz00_27), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-coalesce-set! */
	obj_t BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvz00(obj_t
		BgL_envz00_4016, obj_t BgL_oz00_4017, obj_t BgL_vz00_4018)
	{
		{	/* SawMill/regset.sch 65 */
			return
				BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4017), BgL_vz00_4018);
		}

	}



/* rtl_reg/ra-color */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvz00(BgL_rtl_regz00_bglt BgL_oz00_28)
	{
		{	/* SawMill/regset.sch 66 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4376;

				{
					obj_t BgL_auxz00_4377;

					{	/* SawMill/regset.sch 66 */
						BgL_objectz00_bglt BgL_tmpz00_4378;

						BgL_tmpz00_4378 = ((BgL_objectz00_bglt) BgL_oz00_28);
						BgL_auxz00_4377 = BGL_OBJECT_WIDENING(BgL_tmpz00_4378);
					}
					BgL_auxz00_4376 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4377);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4376))->BgL_colorz00);
			}
		}

	}



/* &rtl_reg/ra-color */
	obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvz00(obj_t BgL_envz00_4019,
		obj_t BgL_oz00_4020)
	{
		{	/* SawMill/regset.sch 66 */
			return
				BGl_rtl_regzf2razd2colorz20zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4020));
		}

	}



/* rtl_reg/ra-color-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvz00(BgL_rtl_regz00_bglt
		BgL_oz00_29, obj_t BgL_vz00_30)
	{
		{	/* SawMill/regset.sch 67 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4385;

				{
					obj_t BgL_auxz00_4386;

					{	/* SawMill/regset.sch 67 */
						BgL_objectz00_bglt BgL_tmpz00_4387;

						BgL_tmpz00_4387 = ((BgL_objectz00_bglt) BgL_oz00_29);
						BgL_auxz00_4386 = BGL_OBJECT_WIDENING(BgL_tmpz00_4387);
					}
					BgL_auxz00_4385 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4386);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4385))->
						BgL_colorz00) = ((obj_t) BgL_vz00_30), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-color-set! */
	obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvz00(obj_t
		BgL_envz00_4021, obj_t BgL_oz00_4022, obj_t BgL_vz00_4023)
	{
		{	/* SawMill/regset.sch 67 */
			return
				BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4022), BgL_vz00_4023);
		}

	}



/* rtl_reg/ra-num */
	BGL_EXPORTED_DEF int BGl_rtl_regzf2razd2numz20zzsaw_bbvz00(BgL_rtl_regz00_bglt
		BgL_oz00_31)
	{
		{	/* SawMill/regset.sch 68 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4394;

				{
					obj_t BgL_auxz00_4395;

					{	/* SawMill/regset.sch 68 */
						BgL_objectz00_bglt BgL_tmpz00_4396;

						BgL_tmpz00_4396 = ((BgL_objectz00_bglt) BgL_oz00_31);
						BgL_auxz00_4395 = BGL_OBJECT_WIDENING(BgL_tmpz00_4396);
					}
					BgL_auxz00_4394 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4395);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4394))->BgL_numz00);
			}
		}

	}



/* &rtl_reg/ra-num */
	obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvz00(obj_t BgL_envz00_4024,
		obj_t BgL_oz00_4025)
	{
		{	/* SawMill/regset.sch 68 */
			return
				BINT(BGl_rtl_regzf2razd2numz20zzsaw_bbvz00(
					((BgL_rtl_regz00_bglt) BgL_oz00_4025)));
		}

	}



/* rtl_reg/ra-hardware */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvz00(BgL_rtl_regz00_bglt BgL_oz00_34)
	{
		{	/* SawMill/regset.sch 70 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_34)))->BgL_hardwarez00);
		}

	}



/* &rtl_reg/ra-hardware */
	obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvz00(obj_t BgL_envz00_4026,
		obj_t BgL_oz00_4027)
	{
		{	/* SawMill/regset.sch 70 */
			return
				BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4027));
		}

	}



/* rtl_reg/ra-key */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvz00(BgL_rtl_regz00_bglt BgL_oz00_37)
	{
		{	/* SawMill/regset.sch 72 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_37)))->BgL_keyz00);
		}

	}



/* &rtl_reg/ra-key */
	obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvz00(obj_t BgL_envz00_4028,
		obj_t BgL_oz00_4029)
	{
		{	/* SawMill/regset.sch 72 */
			return
				BGl_rtl_regzf2razd2keyz20zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4029));
		}

	}



/* rtl_reg/ra-name */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvz00(BgL_rtl_regz00_bglt BgL_oz00_40)
	{
		{	/* SawMill/regset.sch 74 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_40)))->BgL_namez00);
		}

	}



/* &rtl_reg/ra-name */
	obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvz00(obj_t BgL_envz00_4030,
		obj_t BgL_oz00_4031)
	{
		{	/* SawMill/regset.sch 74 */
			return
				BGl_rtl_regzf2razd2namez20zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4031));
		}

	}



/* rtl_reg/ra-onexpr? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvz00(BgL_rtl_regz00_bglt BgL_oz00_43)
	{
		{	/* SawMill/regset.sch 76 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_43)))->BgL_onexprzf3zf3);
		}

	}



/* &rtl_reg/ra-onexpr? */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvz00(obj_t BgL_envz00_4032,
		obj_t BgL_oz00_4033)
	{
		{	/* SawMill/regset.sch 76 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4033));
		}

	}



/* rtl_reg/ra-onexpr?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvz00(BgL_rtl_regz00_bglt
		BgL_oz00_44, obj_t BgL_vz00_45)
	{
		{	/* SawMill/regset.sch 77 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_44)))->BgL_onexprzf3zf3) =
				((obj_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &rtl_reg/ra-onexpr?-set! */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvz00(obj_t
		BgL_envz00_4034, obj_t BgL_oz00_4035, obj_t BgL_vz00_4036)
	{
		{	/* SawMill/regset.sch 77 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4035), BgL_vz00_4036);
		}

	}



/* rtl_reg/ra-var */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvz00(BgL_rtl_regz00_bglt BgL_oz00_46)
	{
		{	/* SawMill/regset.sch 78 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_46)))->BgL_varz00);
		}

	}



/* &rtl_reg/ra-var */
	obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvz00(obj_t BgL_envz00_4037,
		obj_t BgL_oz00_4038)
	{
		{	/* SawMill/regset.sch 78 */
			return
				BGl_rtl_regzf2razd2varz20zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4038));
		}

	}



/* rtl_reg/ra-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvz00(BgL_rtl_regz00_bglt
		BgL_oz00_47, obj_t BgL_vz00_48)
	{
		{	/* SawMill/regset.sch 79 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_47)))->BgL_varz00) =
				((obj_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &rtl_reg/ra-var-set! */
	obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvz00(obj_t BgL_envz00_4039,
		obj_t BgL_oz00_4040, obj_t BgL_vz00_4041)
	{
		{	/* SawMill/regset.sch 79 */
			return
				BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4040), BgL_vz00_4041);
		}

	}



/* rtl_reg/ra-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvz00(BgL_rtl_regz00_bglt BgL_oz00_49)
	{
		{	/* SawMill/regset.sch 80 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_49)))->BgL_typez00);
		}

	}



/* &rtl_reg/ra-type */
	BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_bbvz00(obj_t
		BgL_envz00_4042, obj_t BgL_oz00_4043)
	{
		{	/* SawMill/regset.sch 80 */
			return
				BGl_rtl_regzf2razd2typez20zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4043));
		}

	}



/* rtl_reg/ra-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvz00(BgL_rtl_regz00_bglt
		BgL_oz00_50, BgL_typez00_bglt BgL_vz00_51)
	{
		{	/* SawMill/regset.sch 81 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_50)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_51), BUNSPEC);
		}

	}



/* &rtl_reg/ra-type-set! */
	obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvz00(obj_t
		BgL_envz00_4044, obj_t BgL_oz00_4045, obj_t BgL_vz00_4046)
	{
		{	/* SawMill/regset.sch 81 */
			return
				BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_4045),
				((BgL_typez00_bglt) BgL_vz00_4046));
		}

	}



/* make-regset */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt BGl_makezd2regsetzd2zzsaw_bbvz00(int
		BgL_length1172z00_52, int BgL_msiza7e1173za7_53, obj_t BgL_regv1174z00_54,
		obj_t BgL_regl1175z00_55, obj_t BgL_string1176z00_56)
	{
		{	/* SawMill/regset.sch 84 */
			{	/* SawMill/regset.sch 84 */
				BgL_regsetz00_bglt BgL_new1168z00_4113;

				{	/* SawMill/regset.sch 84 */
					BgL_regsetz00_bglt BgL_new1167z00_4114;

					BgL_new1167z00_4114 =
						((BgL_regsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_regsetz00_bgl))));
					{	/* SawMill/regset.sch 84 */
						long BgL_arg1552z00_4115;

						BgL_arg1552z00_4115 = BGL_CLASS_NUM(BGl_regsetz00zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1167z00_4114), BgL_arg1552z00_4115);
					}
					BgL_new1168z00_4113 = BgL_new1167z00_4114;
				}
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_4113))->BgL_lengthz00) =
					((int) BgL_length1172z00_52), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_4113))->BgL_msiza7eza7) =
					((int) BgL_msiza7e1173za7_53), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_4113))->BgL_regvz00) =
					((obj_t) BgL_regv1174z00_54), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_4113))->BgL_reglz00) =
					((obj_t) BgL_regl1175z00_55), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_4113))->BgL_stringz00) =
					((obj_t) BgL_string1176z00_56), BUNSPEC);
				return BgL_new1168z00_4113;
			}
		}

	}



/* &make-regset */
	BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvz00(obj_t BgL_envz00_4047,
		obj_t BgL_length1172z00_4048, obj_t BgL_msiza7e1173za7_4049,
		obj_t BgL_regv1174z00_4050, obj_t BgL_regl1175z00_4051,
		obj_t BgL_string1176z00_4052)
	{
		{	/* SawMill/regset.sch 84 */
			return
				BGl_makezd2regsetzd2zzsaw_bbvz00(CINT(BgL_length1172z00_4048),
				CINT(BgL_msiza7e1173za7_4049), BgL_regv1174z00_4050,
				BgL_regl1175z00_4051, BgL_string1176z00_4052);
		}

	}



/* regset? */
	BGL_EXPORTED_DEF bool_t BGl_regsetzf3zf3zzsaw_bbvz00(obj_t BgL_objz00_57)
	{
		{	/* SawMill/regset.sch 85 */
			{	/* SawMill/regset.sch 85 */
				obj_t BgL_classz00_4116;

				BgL_classz00_4116 = BGl_regsetz00zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_57))
					{	/* SawMill/regset.sch 85 */
						BgL_objectz00_bglt BgL_arg1807z00_4117;

						BgL_arg1807z00_4117 = (BgL_objectz00_bglt) (BgL_objz00_57);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 85 */
								long BgL_idxz00_4118;

								BgL_idxz00_4118 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4117);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4118 + 1L)) == BgL_classz00_4116);
							}
						else
							{	/* SawMill/regset.sch 85 */
								bool_t BgL_res2323z00_4121;

								{	/* SawMill/regset.sch 85 */
									obj_t BgL_oclassz00_4122;

									{	/* SawMill/regset.sch 85 */
										obj_t BgL_arg1815z00_4123;
										long BgL_arg1816z00_4124;

										BgL_arg1815z00_4123 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 85 */
											long BgL_arg1817z00_4125;

											BgL_arg1817z00_4125 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4117);
											BgL_arg1816z00_4124 = (BgL_arg1817z00_4125 - OBJECT_TYPE);
										}
										BgL_oclassz00_4122 =
											VECTOR_REF(BgL_arg1815z00_4123, BgL_arg1816z00_4124);
									}
									{	/* SawMill/regset.sch 85 */
										bool_t BgL__ortest_1115z00_4126;

										BgL__ortest_1115z00_4126 =
											(BgL_classz00_4116 == BgL_oclassz00_4122);
										if (BgL__ortest_1115z00_4126)
											{	/* SawMill/regset.sch 85 */
												BgL_res2323z00_4121 = BgL__ortest_1115z00_4126;
											}
										else
											{	/* SawMill/regset.sch 85 */
												long BgL_odepthz00_4127;

												{	/* SawMill/regset.sch 85 */
													obj_t BgL_arg1804z00_4128;

													BgL_arg1804z00_4128 = (BgL_oclassz00_4122);
													BgL_odepthz00_4127 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4128);
												}
												if ((1L < BgL_odepthz00_4127))
													{	/* SawMill/regset.sch 85 */
														obj_t BgL_arg1802z00_4129;

														{	/* SawMill/regset.sch 85 */
															obj_t BgL_arg1803z00_4130;

															BgL_arg1803z00_4130 = (BgL_oclassz00_4122);
															BgL_arg1802z00_4129 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4130,
																1L);
														}
														BgL_res2323z00_4121 =
															(BgL_arg1802z00_4129 == BgL_classz00_4116);
													}
												else
													{	/* SawMill/regset.sch 85 */
														BgL_res2323z00_4121 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2323z00_4121;
							}
					}
				else
					{	/* SawMill/regset.sch 85 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &regset? */
	obj_t BGl_z62regsetzf3z91zzsaw_bbvz00(obj_t BgL_envz00_4053,
		obj_t BgL_objz00_4054)
	{
		{	/* SawMill/regset.sch 85 */
			return BBOOL(BGl_regsetzf3zf3zzsaw_bbvz00(BgL_objz00_4054));
		}

	}



/* regset-nil */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt BGl_regsetzd2nilzd2zzsaw_bbvz00(void)
	{
		{	/* SawMill/regset.sch 86 */
			{	/* SawMill/regset.sch 86 */
				obj_t BgL_classz00_3372;

				BgL_classz00_3372 = BGl_regsetz00zzsaw_regsetz00;
				{	/* SawMill/regset.sch 86 */
					obj_t BgL__ortest_1117z00_3373;

					BgL__ortest_1117z00_3373 = BGL_CLASS_NIL(BgL_classz00_3372);
					if (CBOOL(BgL__ortest_1117z00_3373))
						{	/* SawMill/regset.sch 86 */
							return ((BgL_regsetz00_bglt) BgL__ortest_1117z00_3373);
						}
					else
						{	/* SawMill/regset.sch 86 */
							return
								((BgL_regsetz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3372));
						}
				}
			}
		}

	}



/* &regset-nil */
	BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvz00(obj_t BgL_envz00_4055)
	{
		{	/* SawMill/regset.sch 86 */
			return BGl_regsetzd2nilzd2zzsaw_bbvz00();
		}

	}



/* regset-string */
	BGL_EXPORTED_DEF obj_t BGl_regsetzd2stringzd2zzsaw_bbvz00(BgL_regsetz00_bglt
		BgL_oz00_58)
	{
		{	/* SawMill/regset.sch 87 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_58))->BgL_stringz00);
		}

	}



/* &regset-string */
	obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvz00(obj_t BgL_envz00_4056,
		obj_t BgL_oz00_4057)
	{
		{	/* SawMill/regset.sch 87 */
			return
				BGl_regsetzd2stringzd2zzsaw_bbvz00(
				((BgL_regsetz00_bglt) BgL_oz00_4057));
		}

	}



/* regset-string-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvz00(BgL_regsetz00_bglt BgL_oz00_59,
		obj_t BgL_vz00_60)
	{
		{	/* SawMill/regset.sch 88 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_59))->BgL_stringz00) =
				((obj_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &regset-string-set! */
	obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvz00(obj_t BgL_envz00_4058,
		obj_t BgL_oz00_4059, obj_t BgL_vz00_4060)
	{
		{	/* SawMill/regset.sch 88 */
			return
				BGl_regsetzd2stringzd2setz12z12zzsaw_bbvz00(
				((BgL_regsetz00_bglt) BgL_oz00_4059), BgL_vz00_4060);
		}

	}



/* regset-regl */
	BGL_EXPORTED_DEF obj_t BGl_regsetzd2reglzd2zzsaw_bbvz00(BgL_regsetz00_bglt
		BgL_oz00_61)
	{
		{	/* SawMill/regset.sch 89 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_61))->BgL_reglz00);
		}

	}



/* &regset-regl */
	obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvz00(obj_t BgL_envz00_4061,
		obj_t BgL_oz00_4062)
	{
		{	/* SawMill/regset.sch 89 */
			return
				BGl_regsetzd2reglzd2zzsaw_bbvz00(((BgL_regsetz00_bglt) BgL_oz00_4062));
		}

	}



/* regset-regv */
	BGL_EXPORTED_DEF obj_t BGl_regsetzd2regvzd2zzsaw_bbvz00(BgL_regsetz00_bglt
		BgL_oz00_64)
	{
		{	/* SawMill/regset.sch 91 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_64))->BgL_regvz00);
		}

	}



/* &regset-regv */
	obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvz00(obj_t BgL_envz00_4063,
		obj_t BgL_oz00_4064)
	{
		{	/* SawMill/regset.sch 91 */
			return
				BGl_regsetzd2regvzd2zzsaw_bbvz00(((BgL_regsetz00_bglt) BgL_oz00_4064));
		}

	}



/* regset-msize */
	BGL_EXPORTED_DEF int BGl_regsetzd2msiza7ez75zzsaw_bbvz00(BgL_regsetz00_bglt
		BgL_oz00_67)
	{
		{	/* SawMill/regset.sch 93 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_67))->BgL_msiza7eza7);
		}

	}



/* &regset-msize */
	obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvz00(obj_t BgL_envz00_4065,
		obj_t BgL_oz00_4066)
	{
		{	/* SawMill/regset.sch 93 */
			return
				BINT(BGl_regsetzd2msiza7ez75zzsaw_bbvz00(
					((BgL_regsetz00_bglt) BgL_oz00_4066)));
		}

	}



/* regset-length */
	BGL_EXPORTED_DEF int BGl_regsetzd2lengthzd2zzsaw_bbvz00(BgL_regsetz00_bglt
		BgL_oz00_70)
	{
		{	/* SawMill/regset.sch 95 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_70))->BgL_lengthz00);
		}

	}



/* &regset-length */
	obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvz00(obj_t BgL_envz00_4067,
		obj_t BgL_oz00_4068)
	{
		{	/* SawMill/regset.sch 95 */
			return
				BINT(BGl_regsetzd2lengthzd2zzsaw_bbvz00(
					((BgL_regsetz00_bglt) BgL_oz00_4068)));
		}

	}



/* regset-length-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvz00(BgL_regsetz00_bglt BgL_oz00_71,
		int BgL_vz00_72)
	{
		{	/* SawMill/regset.sch 96 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_71))->BgL_lengthz00) =
				((int) BgL_vz00_72), BUNSPEC);
		}

	}



/* &regset-length-set! */
	obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvz00(obj_t BgL_envz00_4069,
		obj_t BgL_oz00_4070, obj_t BgL_vz00_4071)
	{
		{	/* SawMill/regset.sch 96 */
			return
				BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvz00(
				((BgL_regsetz00_bglt) BgL_oz00_4070), CINT(BgL_vz00_4071));
		}

	}



/* make-empty-bbset */
	obj_t BGl_makezd2emptyzd2bbsetz00zzsaw_bbvz00(void)
	{
		{	/* SawMill/bbset.sch 20 */
			{	/* SawMill/bbset.sch 21 */
				obj_t BgL_arg1576z00_2189;

				BgL_arg1576z00_2189 = BGl_getzd2bbzd2markz00zzsaw_bbvzd2typeszd2();
				{	/* SawMill/bbset.sch 15 */
					obj_t BgL_newz00_3381;

					BgL_newz00_3381 = create_struct(CNST_TABLE_REF(0), (int) (2L));
					{	/* SawMill/bbset.sch 15 */
						int BgL_tmpz00_4512;

						BgL_tmpz00_4512 = (int) (1L);
						STRUCT_SET(BgL_newz00_3381, BgL_tmpz00_4512, BNIL);
					}
					{	/* SawMill/bbset.sch 15 */
						int BgL_tmpz00_4515;

						BgL_tmpz00_4515 = (int) (0L);
						STRUCT_SET(BgL_newz00_3381, BgL_tmpz00_4515, BgL_arg1576z00_2189);
					}
					return BgL_newz00_3381;
				}
			}
		}

	}



/* bbset-cons */
	obj_t BGl_bbsetzd2conszd2zzsaw_bbvz00(BgL_blockz00_bglt BgL_bz00_85,
		obj_t BgL_setz00_86)
	{
		{	/* SawMill/bbset.sch 33 */
			{	/* SawMill/bbset.sch 34 */
				obj_t BgL_mz00_2193;

				BgL_mz00_2193 = STRUCT_REF(BgL_setz00_86, (int) (0L));
				{
					BgL_blocksz00_bglt BgL_auxz00_4520;

					{
						obj_t BgL_auxz00_4521;

						{	/* SawMill/bbset.sch 36 */
							BgL_objectz00_bglt BgL_tmpz00_4522;

							BgL_tmpz00_4522 = ((BgL_objectz00_bglt) BgL_bz00_85);
							BgL_auxz00_4521 = BGL_OBJECT_WIDENING(BgL_tmpz00_4522);
						}
						BgL_auxz00_4520 = ((BgL_blocksz00_bglt) BgL_auxz00_4521);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4520))->BgL_z52markz52) =
						((long) (long) CINT(BgL_mz00_2193)), BUNSPEC);
				}
				{	/* SawMill/bbset.sch 37 */
					obj_t BgL_arg1589z00_2195;

					{	/* SawMill/bbset.sch 37 */
						obj_t BgL_arg1591z00_2196;

						BgL_arg1591z00_2196 = STRUCT_REF(BgL_setz00_86, (int) (1L));
						BgL_arg1589z00_2195 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_85), BgL_arg1591z00_2196);
					}
					{	/* SawMill/bbset.sch 15 */
						int BgL_tmpz00_4532;

						BgL_tmpz00_4532 = (int) (1L);
						STRUCT_SET(BgL_setz00_86, BgL_tmpz00_4532, BgL_arg1589z00_2195);
				}}
				return BgL_setz00_86;
			}
		}

	}



/* bbv */
	BGL_EXPORTED_DEF obj_t BGl_bbvz00zzsaw_bbvz00(BgL_backendz00_bglt
		BgL_backz00_91, BgL_globalz00_bglt BgL_globalz00_92, obj_t BgL_paramsz00_93,
		obj_t BgL_blocksz00_94)
	{
		{	/* SawBbv/bbv.scm 49 */
			{	/* SawBbv/bbv.scm 50 */
				bool_t BgL_test2466z00_4535;

				if (CBOOL(BGl_za2sawzd2bbvzf3za2z21zzengine_paramz00))
					{	/* SawBbv/bbv.scm 51 */
						bool_t BgL_test2468z00_4538;

						if (NULLP(BGl_za2sawzd2bbvzd2functionsza2z00zzengine_paramz00))
							{	/* SawBbv/bbv.scm 51 */
								BgL_test2468z00_4538 = ((bool_t) 1);
							}
						else
							{	/* SawBbv/bbv.scm 52 */
								obj_t BgL_arg1798z00_2314;

								BgL_arg1798z00_2314 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_globalz00_92)))->BgL_idz00);
								BgL_test2468z00_4538 =
									CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1798z00_2314,
										BGl_za2sawzd2bbvzd2functionsza2z00zzengine_paramz00));
							}
						if (BgL_test2468z00_4538)
							{	/* SawBbv/bbv.scm 51 */
								BgL_test2466z00_4535 =
									(
									(long)
									CINT
									(BGl_za2maxzd2blockzd2mergezd2versionsza2zd2zzsaw_bbvzd2configzd2)
									>= 1L);
							}
						else
							{	/* SawBbv/bbv.scm 51 */
								BgL_test2466z00_4535 = ((bool_t) 0);
							}
					}
				else
					{	/* SawBbv/bbv.scm 50 */
						BgL_test2466z00_4535 = ((bool_t) 0);
					}
				if (BgL_test2466z00_4535)
					{	/* SawBbv/bbv.scm 50 */
						BGl_startzd2bbvzd2cachez12z12zzsaw_bbvzd2cachezd2();
						{	/* SawBbv/bbv.scm 56 */
							obj_t BgL_arg1646z00_2227;
							obj_t BgL_arg1650z00_2228;
							obj_t BgL_arg1651z00_2229;
							obj_t BgL_arg1654z00_2230;

							BgL_arg1646z00_2227 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_globalz00_92)))->BgL_idz00);
							if (CBOOL
								(BGl_za2bbvzd2optimzd2vlengthza2z00zzsaw_bbvzd2configzd2))
								{	/* SawBbv/bbv.scm 58 */
									BgL_arg1650z00_2228 = BGl_string2344z00zzsaw_bbvz00;
								}
							else
								{	/* SawBbv/bbv.scm 58 */
									BgL_arg1650z00_2228 = BGl_string2345z00zzsaw_bbvz00;
								}
							if (CBOOL(BGl_za2bbvzd2optimzd2aliasza2z00zzsaw_bbvzd2configzd2))
								{	/* SawBbv/bbv.scm 59 */
									BgL_arg1651z00_2229 = BGl_string2346z00zzsaw_bbvz00;
								}
							else
								{	/* SawBbv/bbv.scm 59 */
									BgL_arg1651z00_2229 = BGl_string2345z00zzsaw_bbvz00;
								}
							if (CBOOL
								(BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2))
								{	/* SawBbv/bbv.scm 60 */
									BgL_arg1654z00_2230 = BGl_string2347z00zzsaw_bbvz00;
								}
							else
								{	/* SawBbv/bbv.scm 60 */
									BgL_arg1654z00_2230 = BGl_string2345z00zzsaw_bbvz00;
								}
							{	/* SawBbv/bbv.scm 56 */
								obj_t BgL_list1655z00_2231;

								{	/* SawBbv/bbv.scm 56 */
									obj_t BgL_arg1661z00_2232;

									{	/* SawBbv/bbv.scm 56 */
										obj_t BgL_arg1663z00_2233;

										{	/* SawBbv/bbv.scm 56 */
											obj_t BgL_arg1675z00_2234;

											{	/* SawBbv/bbv.scm 56 */
												obj_t BgL_arg1678z00_2235;

												{	/* SawBbv/bbv.scm 56 */
													obj_t BgL_arg1681z00_2236;

													{	/* SawBbv/bbv.scm 56 */
														obj_t BgL_arg1688z00_2237;

														{	/* SawBbv/bbv.scm 56 */
															obj_t BgL_arg1689z00_2238;

															BgL_arg1689z00_2238 =
																MAKE_YOUNG_PAIR(BGl_string2348z00zzsaw_bbvz00,
																BNIL);
															BgL_arg1688z00_2237 =
																MAKE_YOUNG_PAIR(BgL_arg1654z00_2230,
																BgL_arg1689z00_2238);
														}
														BgL_arg1681z00_2236 =
															MAKE_YOUNG_PAIR(BgL_arg1651z00_2229,
															BgL_arg1688z00_2237);
													}
													BgL_arg1678z00_2235 =
														MAKE_YOUNG_PAIR(BgL_arg1650z00_2228,
														BgL_arg1681z00_2236);
												}
												BgL_arg1675z00_2234 =
													MAKE_YOUNG_PAIR
													(BGl_za2maxzd2blockzd2mergezd2versionsza2zd2zzsaw_bbvzd2configzd2,
													BgL_arg1678z00_2235);
											}
											BgL_arg1663z00_2233 =
												MAKE_YOUNG_PAIR(BGl_string2349z00zzsaw_bbvz00,
												BgL_arg1675z00_2234);
										}
										BgL_arg1661z00_2232 =
											MAKE_YOUNG_PAIR(BgL_arg1646z00_2227, BgL_arg1663z00_2233);
									}
									BgL_list1655z00_2231 =
										MAKE_YOUNG_PAIR(BGl_string2350z00zzsaw_bbvz00,
										BgL_arg1661z00_2232);
								}
								BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1655z00_2231);
							}
						}
						if (CBOOL(BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2))
							{	/* SawBbv/bbv.scm 62 */
								BGl_dumpzd2cfgzd2zzsaw_bbvzd2debugzd2(BgL_globalz00_92,
									BgL_paramsz00_93, BgL_blocksz00_94,
									BGl_string2351z00zzsaw_bbvz00);
							}
						else
							{	/* SawBbv/bbv.scm 62 */
								BFALSE;
							}
						BGl_setzd2maxzd2labelz12z12zzsaw_bbvzd2utilszd2(BgL_blocksz00_94);
						BGl_reorderzd2succsz12zc0zzsaw_bbvz00(BgL_blocksz00_94);
						if (CBOOL(BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2))
							{	/* SawBbv/bbv.scm 70 */
								BGl_dumpzd2cfgzd2zzsaw_bbvzd2debugzd2(BgL_globalz00_92,
									BgL_paramsz00_93, BgL_blocksz00_94,
									BGl_string2352z00zzsaw_bbvz00);
							}
						else
							{	/* SawBbv/bbv.scm 70 */
								BFALSE;
							}
						{	/* SawBbv/bbv.scm 73 */
							obj_t BgL_arg1691z00_2239;

							BgL_arg1691z00_2239 = CAR(((obj_t) BgL_blocksz00_94));
							BGl_failz12z12zzsaw_bbvz00(
								((BgL_blockz00_bglt) BgL_arg1691z00_2239));
						}
						if (CBOOL(BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2))
							{	/* SawBbv/bbv.scm 74 */
								BGl_dumpzd2cfgzd2zzsaw_bbvzd2debugzd2(BgL_globalz00_92,
									BgL_paramsz00_93, BgL_blocksz00_94,
									BGl_string2353z00zzsaw_bbvz00);
							}
						else
							{	/* SawBbv/bbv.scm 74 */
								BFALSE;
							}
						{	/* SawBbv/bbv.scm 78 */
							obj_t BgL_arg1692z00_2240;

							BgL_arg1692z00_2240 = CAR(((obj_t) BgL_blocksz00_94));
							BGl_normaliza7ezd2typecheckz12z67zzsaw_bbvz00(
								((BgL_blockz00_bglt) BgL_arg1692z00_2240));
						}
						if (CBOOL(BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2))
							{	/* SawBbv/bbv.scm 79 */
								BGl_dumpzd2cfgzd2zzsaw_bbvzd2debugzd2(BgL_globalz00_92,
									BgL_paramsz00_93, BgL_blocksz00_94,
									BGl_string2354z00zzsaw_bbvz00);
							}
						else
							{	/* SawBbv/bbv.scm 79 */
								BFALSE;
							}
						{	/* SawBbv/bbv.scm 81 */
							obj_t BgL_blocksz00_2241;

							{	/* SawBbv/bbv.scm 81 */
								BgL_blockz00_bglt BgL_arg1773z00_2310;

								{	/* SawBbv/bbv.scm 81 */
									obj_t BgL_arg1775z00_2311;

									BgL_arg1775z00_2311 = CAR(((obj_t) BgL_blocksz00_94));
									BgL_arg1773z00_2310 =
										BGl_removezd2tempsz12zc0zzsaw_bbvz00(
										((BgL_blockz00_bglt) BgL_arg1775z00_2311));
								}
								BgL_blocksz00_2241 =
									BGl_normaliza7ezd2gotoz12z67zzsaw_bbvz00(BgL_arg1773z00_2310);
							}
							if (CBOOL(BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2))
								{	/* SawBbv/bbv.scm 82 */
									BGl_dumpzd2cfgzd2zzsaw_bbvzd2debugzd2(BgL_globalz00_92,
										BgL_paramsz00_93, BgL_blocksz00_2241,
										BGl_string2355z00zzsaw_bbvz00);
								}
							else
								{	/* SawBbv/bbv.scm 82 */
									BFALSE;
								}
							{	/* SawBbv/bbv.scm 84 */
								obj_t BgL_blocksz00_2242;

								{	/* SawBbv/bbv.scm 84 */
									obj_t BgL_arg1771z00_2309;

									BgL_arg1771z00_2309 = CAR(BgL_blocksz00_2241);
									BgL_blocksz00_2242 =
										BGl_normaliza7ezd2ifeqz12z67zzsaw_bbvz00(
										((BgL_blockz00_bglt) BgL_arg1771z00_2309));
								}
								if (CBOOL(BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2))
									{	/* SawBbv/bbv.scm 85 */
										BGl_dumpzd2cfgzd2zzsaw_bbvzd2debugzd2(BgL_globalz00_92,
											BgL_paramsz00_93, BgL_blocksz00_2242,
											BGl_string2356z00zzsaw_bbvz00);
									}
								else
									{	/* SawBbv/bbv.scm 85 */
										BFALSE;
									}
								{	/* SawBbv/bbv.scm 87 */
									obj_t BgL_blocksz00_2243;

									{	/* SawBbv/bbv.scm 87 */
										obj_t BgL_arg1770z00_2308;

										BgL_arg1770z00_2308 = CAR(BgL_blocksz00_2242);
										BgL_blocksz00_2243 =
											BGl_normaliza7ezd2movz12z67zzsaw_bbvz00(
											((BgL_blockz00_bglt) BgL_arg1770z00_2308));
									}
									if (CBOOL(BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2))
										{	/* SawBbv/bbv.scm 88 */
											BGl_dumpzd2cfgzd2zzsaw_bbvzd2debugzd2(BgL_globalz00_92,
												BgL_paramsz00_93, BgL_blocksz00_2243,
												BGl_string2357z00zzsaw_bbvz00);
										}
									else
										{	/* SawBbv/bbv.scm 88 */
											BFALSE;
										}
									{	/* SawBbv/bbv.scm 90 */
										obj_t BgL_regsz00_2244;

										BgL_regsz00_2244 =
											BGl_bbvzd2livenessz12zc0zzsaw_bbvzd2livenesszd2
											(BgL_backz00_91, BgL_blocksz00_2243, BgL_paramsz00_93);
										{	/* SawBbv/bbv.scm 92 */
											obj_t BgL_arg1699z00_2245;

											BgL_arg1699z00_2245 = CAR(BgL_blocksz00_2243);
											BGl_markzd2mergez12zc0zzsaw_bbvz00(
												((BgL_blockz00_bglt) BgL_arg1699z00_2245));
										}
										if (CBOOL
											(BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2))
											{	/* SawBbv/bbv.scm 93 */
												BGl_dumpzd2cfgzd2zzsaw_bbvzd2debugzd2(BgL_globalz00_92,
													BgL_paramsz00_93, BgL_blocksz00_2243,
													BGl_string2358z00zzsaw_bbvz00);
											}
										else
											{	/* SawBbv/bbv.scm 93 */
												BFALSE;
											}
										{	/* SawBbv/bbv.scm 95 */
											obj_t BgL_exitd1172z00_2246;

											BgL_exitd1172z00_2246 = BGL_EXITD_TOP_AS_OBJ();
											{	/* SawBbv/bbv.scm 131 */
												obj_t BgL_zc3z04anonymousza31753ze3z87_4072;

												BgL_zc3z04anonymousza31753ze3z87_4072 =
													MAKE_FX_PROCEDURE
													(BGl_z62zc3z04anonymousza31753ze3ze5zzsaw_bbvz00,
													(int) (0L), (int) (1L));
												PROCEDURE_SET(BgL_zc3z04anonymousza31753ze3z87_4072,
													(int) (0L), BgL_regsz00_2244);
												{	/* SawBbv/bbv.scm 95 */
													obj_t BgL_arg1828z00_3420;

													{	/* SawBbv/bbv.scm 95 */
														obj_t BgL_arg1829z00_3421;

														BgL_arg1829z00_3421 =
															BGL_EXITD_PROTECT(BgL_exitd1172z00_2246);
														BgL_arg1828z00_3420 =
															MAKE_YOUNG_PAIR
															(BgL_zc3z04anonymousza31753ze3z87_4072,
															BgL_arg1829z00_3421);
													}
													BGL_EXITD_PROTECT_SET(BgL_exitd1172z00_2246,
														BgL_arg1828z00_3420);
													BUNSPEC;
												}
												{	/* SawBbv/bbv.scm 96 */
													obj_t BgL_tmp1174z00_2248;

													{	/* SawBbv/bbv.scm 98 */
														BgL_blockz00_bglt BgL_sz00_2250;

														{	/* SawBbv/bbv.scm 99 */
															BgL_blockz00_bglt BgL_arg1750z00_2289;

															{	/* SawBbv/bbv.scm 99 */
																obj_t BgL_arg1751z00_2290;
																BgL_bbvzd2ctxzd2_bglt BgL_arg1752z00_2291;

																BgL_arg1751z00_2290 = CAR(BgL_blocksz00_2243);
																BgL_arg1752z00_2291 =
																	BGl_paramszd2ze3ctxz31zzsaw_bbvzd2typeszd2
																	(BgL_paramsz00_93);
																BgL_arg1750z00_2289 =
																	BGl_bbvzd2rootzd2blockz00zzsaw_bbvzd2specializa7ez75
																	(((BgL_blockz00_bglt) BgL_arg1751z00_2290),
																	BgL_arg1752z00_2291);
															}
															BgL_sz00_2250 =
																BGl_dumpzd2blockszd2zzsaw_bbvz00(1L,
																BGl_string2359z00zzsaw_bbvz00, BgL_globalz00_92,
																BgL_paramsz00_93, BgL_regsz00_2244, BFALSE,
																BgL_arg1750z00_2289);
														}
														{	/* SawBbv/bbv.scm 98 */
															obj_t BgL___az00_2251;

															if (CBOOL
																(BGl_za2bbvzd2logza2zd2zzsaw_bbvzd2configzd2))
																{	/* SawBbv/bbv.scm 101 */
																	BgL___az00_2251 =
																		BGl_logzd2blockszd2zzsaw_bbvzd2debugzd2
																		(BgL_globalz00_92, BgL_paramsz00_93,
																		BgL_blocksz00_2243);
																}
															else
																{	/* SawBbv/bbv.scm 101 */
																	BgL___az00_2251 = BFALSE;
																}
															{	/* SawBbv/bbv.scm 101 */
																obj_t BgL_historyz00_2252;

																if (CBOOL
																	(BGl_za2bbvzd2dumpzd2jsonza2z00zzsaw_bbvzd2configzd2))
																	{	/* SawBbv/bbv.scm 103 */
																		BgL_historyz00_2252 =
																			BGl_logzd2blockszd2historyz00zzsaw_bbvzd2debugzd2
																			(BgL_globalz00_92, BgL_paramsz00_93,
																			BgL_blocksz00_2243);
																	}
																else
																	{	/* SawBbv/bbv.scm 103 */
																		BgL_historyz00_2252 = BFALSE;
																	}
																{	/* SawBbv/bbv.scm 103 */
																	BgL_blockz00_bglt BgL_prz00_2253;

																	{	/* SawBbv/bbv.scm 106 */
																		obj_t BgL_arg1749z00_2288;

																		BgL_arg1749z00_2288 =
																			BGl_profilez12z12zzsaw_bbvzd2profilezd2
																			(BgL_sz00_2250);
																		BgL_prz00_2253 =
																			BGl_dumpzd2blockszd2zzsaw_bbvz00(1L,
																			BGl_string2360z00zzsaw_bbvz00,
																			BgL_globalz00_92, BgL_paramsz00_93,
																			BgL_regsz00_2244, BgL_historyz00_2252,
																			((BgL_blockz00_bglt)
																				BgL_arg1749z00_2288));
																	}
																	{	/* SawBbv/bbv.scm 105 */
																		BgL_blockz00_bglt BgL_asz00_2254;

																		{	/* SawBbv/bbv.scm 108 */
																			obj_t BgL_arg1748z00_2287;

																			BgL_arg1748z00_2287 =
																				BGl_assertzd2contextz12zc0zzsaw_bbvzd2debugzd2
																				(BgL_prz00_2253);
																			BgL_asz00_2254 =
																				BGl_dumpzd2blockszd2zzsaw_bbvz00(1L,
																				BGl_string2361z00zzsaw_bbvz00,
																				BgL_globalz00_92, BgL_paramsz00_93,
																				BgL_regsz00_2244, BgL_historyz00_2252,
																				((BgL_blockz00_bglt)
																					BgL_arg1748z00_2287));
																		}
																		{	/* SawBbv/bbv.scm 107 */
																			obj_t BgL_bz00_2255;

																			{	/* SawBbv/bbv.scm 110 */
																				BgL_blockz00_bglt BgL_arg1724z00_2276;

																				if (CBOOL
																					(BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2))
																					{	/* SawBbv/bbv.scm 121 */
																						obj_t BgL_arg1733z00_2277;

																						{	/* SawBbv/bbv.scm 121 */
																							BgL_blockz00_bglt
																								BgL_arg1734z00_2278;
																							{	/* SawBbv/bbv.scm 121 */
																								obj_t BgL_arg1735z00_2279;

																								{	/* SawBbv/bbv.scm 121 */
																									BgL_blockz00_bglt
																										BgL_arg1736z00_2280;
																									{	/* SawBbv/bbv.scm 121 */
																										obj_t BgL_arg1737z00_2281;

																										{	/* SawBbv/bbv.scm 121 */
																											BgL_blockz00_bglt
																												BgL_arg1738z00_2282;
																											{	/* SawBbv/bbv.scm 121 */
																												obj_t
																													BgL_arg1739z00_2283;
																												{	/* SawBbv/bbv.scm 121 */
																													BgL_blockz00_bglt
																														BgL_arg1740z00_2284;
																													{	/* SawBbv/bbv.scm 121 */
																														obj_t
																															BgL_arg1746z00_2285;
																														BgL_arg1746z00_2285
																															=
																															BGl_coalescez12z12zzsaw_bbvzd2optimzd2
																															(BgL_globalz00_92,
																															BGl_gcz12z12zzsaw_bbvz00
																															(BgL_asz00_2254));
																														BgL_arg1740z00_2284
																															=
																															BGl_dumpzd2blockszd2zzsaw_bbvz00
																															(1L,
																															BGl_string2362z00zzsaw_bbvz00,
																															BgL_globalz00_92,
																															BgL_paramsz00_93,
																															BgL_regsz00_2244,
																															BgL_historyz00_2252,
																															((BgL_blockz00_bglt) BgL_arg1746z00_2285));
																													}
																													BgL_arg1739z00_2283 =
																														BGl_simplifyzd2branchz12zc0zzsaw_bbvzd2optimzd2
																														(BgL_globalz00_92,
																														((BgL_blockz00_bglt)
																															BgL_arg1740z00_2284));
																												}
																												BgL_arg1738z00_2282 =
																													BGl_dumpzd2blockszd2zzsaw_bbvz00
																													(1L,
																													BGl_string2363z00zzsaw_bbvz00,
																													BgL_globalz00_92,
																													BgL_paramsz00_93,
																													BgL_regsz00_2244,
																													BgL_historyz00_2252,
																													((BgL_blockz00_bglt)
																														BgL_arg1739z00_2283));
																											}
																											BgL_arg1737z00_2281 =
																												BGl_removezd2gotoz12zc0zzsaw_bbvzd2optimzd2
																												(BgL_globalz00_92,
																												((BgL_blockz00_bglt)
																													BgL_arg1738z00_2282));
																										}
																										BgL_arg1736z00_2280 =
																											BGl_dumpzd2blockszd2zzsaw_bbvz00
																											(1L,
																											BGl_string2364z00zzsaw_bbvz00,
																											BgL_globalz00_92,
																											BgL_paramsz00_93,
																											BgL_regsz00_2244,
																											BgL_historyz00_2252,
																											((BgL_blockz00_bglt)
																												BgL_arg1737z00_2281));
																									}
																									BgL_arg1735z00_2279 =
																										BGl_removezd2nopz12zc0zzsaw_bbvzd2optimzd2
																										(BgL_globalz00_92,
																										((BgL_blockz00_bglt)
																											BgL_arg1736z00_2280));
																								}
																								BgL_arg1734z00_2278 =
																									BGl_dumpzd2blockszd2zzsaw_bbvz00
																									(1L,
																									BGl_string2365z00zzsaw_bbvz00,
																									BgL_globalz00_92,
																									BgL_paramsz00_93,
																									BgL_regsz00_2244,
																									BgL_historyz00_2252,
																									((BgL_blockz00_bglt)
																										BgL_arg1735z00_2279));
																							}
																							BgL_arg1733z00_2277 =
																								BGl_simplifyzd2branchz12zc0zzsaw_bbvzd2optimzd2
																								(BgL_globalz00_92,
																								((BgL_blockz00_bglt)
																									BgL_arg1734z00_2278));
																						}
																						BgL_arg1724z00_2276 =
																							BGl_dumpzd2blockszd2zzsaw_bbvz00
																							(1L,
																							BGl_string2366z00zzsaw_bbvz00,
																							BgL_globalz00_92,
																							BgL_paramsz00_93,
																							BgL_regsz00_2244,
																							BgL_historyz00_2252,
																							((BgL_blockz00_bglt)
																								BgL_arg1733z00_2277));
																					}
																				else
																					{	/* SawBbv/bbv.scm 110 */
																						BgL_arg1724z00_2276 =
																							BgL_asz00_2254;
																					}
																				BgL_bz00_2255 =
																					BGl_blockzd2ze3blockzd2listze3zzsaw_bbvzd2utilszd2
																					(BgL_regsz00_2244,
																					((BgL_blockz00_bglt)
																						BgL_arg1724z00_2276));
																			}
																			{	/* SawBbv/bbv.scm 109 */

																				{	/* SawBbv/bbv.scm 124 */
																					long BgL_arg1701z00_2256;
																					long BgL_arg1702z00_2257;

																					BgL_arg1701z00_2256 =
																						bgl_list_length(BgL_blocksz00_2243);
																					BgL_arg1702z00_2257 =
																						bgl_list_length(BgL_bz00_2255);
																					{	/* SawBbv/bbv.scm 123 */
																						obj_t BgL_list1703z00_2258;

																						{	/* SawBbv/bbv.scm 123 */
																							obj_t BgL_arg1705z00_2259;

																							{	/* SawBbv/bbv.scm 123 */
																								obj_t BgL_arg1708z00_2260;

																								{	/* SawBbv/bbv.scm 123 */
																									obj_t BgL_arg1709z00_2261;

																									BgL_arg1709z00_2261 =
																										MAKE_YOUNG_PAIR(BINT
																										(BgL_arg1702z00_2257),
																										BNIL);
																									BgL_arg1708z00_2260 =
																										MAKE_YOUNG_PAIR
																										(BGl_string2367z00zzsaw_bbvz00,
																										BgL_arg1709z00_2261);
																								}
																								BgL_arg1705z00_2259 =
																									MAKE_YOUNG_PAIR(BINT
																									(BgL_arg1701z00_2256),
																									BgL_arg1708z00_2260);
																							}
																							BgL_list1703z00_2258 =
																								MAKE_YOUNG_PAIR
																								(BGl_string2368z00zzsaw_bbvz00,
																								BgL_arg1705z00_2259);
																						}
																						BGl_verbosez00zztools_speekz00(BINT
																							(3L), BgL_list1703z00_2258);
																				}}
																				{	/* SawBbv/bbv.scm 125 */
																					obj_t BgL_list1710z00_2262;

																					BgL_list1710z00_2262 =
																						MAKE_YOUNG_PAIR
																						(BGl_string2369z00zzsaw_bbvz00,
																						BNIL);
																					BGl_verbosez00zztools_speekz00(BINT
																						(2L), BgL_list1710z00_2262);
																				}
																				BGl_dumpzd2blockszd2zzsaw_bbvz00(1L,
																					BGl_string2370z00zzsaw_bbvz00,
																					BgL_globalz00_92, BgL_paramsz00_93,
																					BgL_regsz00_2244, BgL_historyz00_2252,
																					BgL_sz00_2250);
																				{
																					obj_t BgL_l1484z00_2265;

																					BgL_l1484z00_2265 = BgL_bz00_2255;
																				BgL_zc3z04anonymousza31711ze3z87_2266:
																					if (NULLP(BgL_l1484z00_2265))
																						{	/* SawBbv/bbv.scm 127 */
																							BgL_bz00_2255;
																						}
																					else
																						{	/* SawBbv/bbv.scm 127 */
																							{	/* SawBbv/bbv.scm 127 */
																								obj_t BgL_arg1714z00_2268;

																								{	/* SawBbv/bbv.scm 127 */
																									obj_t BgL_bz00_2269;

																									BgL_bz00_2269 =
																										CAR(
																										((obj_t)
																											BgL_l1484z00_2265));
																									{	/* SawBbv/bbv.scm 127 */
																										long BgL_arg1717z00_2271;

																										{	/* SawBbv/bbv.scm 127 */
																											obj_t BgL_arg1718z00_2272;

																											{	/* SawBbv/bbv.scm 127 */
																												obj_t
																													BgL_arg1720z00_2273;
																												{	/* SawBbv/bbv.scm 127 */
																													obj_t
																														BgL_arg1815z00_3425;
																													long
																														BgL_arg1816z00_3426;
																													BgL_arg1815z00_3425 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* SawBbv/bbv.scm 127 */
																														long
																															BgL_arg1817z00_3427;
																														BgL_arg1817z00_3427
																															=
																															BGL_OBJECT_CLASS_NUM
																															(((BgL_objectz00_bglt) BgL_bz00_2269));
																														BgL_arg1816z00_3426
																															=
																															(BgL_arg1817z00_3427
																															- OBJECT_TYPE);
																													}
																													BgL_arg1720z00_2273 =
																														VECTOR_REF
																														(BgL_arg1815z00_3425,
																														BgL_arg1816z00_3426);
																												}
																												BgL_arg1718z00_2272 =
																													BGl_classzd2superzd2zz__objectz00
																													(BgL_arg1720z00_2273);
																											}
																											{	/* SawBbv/bbv.scm 127 */
																												obj_t BgL_tmpz00_4689;

																												BgL_tmpz00_4689 =
																													((obj_t)
																													BgL_arg1718z00_2272);
																												BgL_arg1717z00_2271 =
																													BGL_CLASS_NUM
																													(BgL_tmpz00_4689);
																										}}
																										BGL_OBJECT_CLASS_NUM_SET(
																											((BgL_objectz00_bglt)
																												BgL_bz00_2269),
																											BgL_arg1717z00_2271);
																									}
																									{	/* SawBbv/bbv.scm 127 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_4694;
																										BgL_tmpz00_4694 =
																											((BgL_objectz00_bglt)
																											BgL_bz00_2269);
																										BGL_OBJECT_WIDENING_SET
																											(BgL_tmpz00_4694, BFALSE);
																									}
																									((BgL_objectz00_bglt)
																										BgL_bz00_2269);
																									BgL_arg1714z00_2268 =
																										BgL_bz00_2269;
																								}
																								{	/* SawBbv/bbv.scm 127 */
																									obj_t BgL_tmpz00_4698;

																									BgL_tmpz00_4698 =
																										((obj_t) BgL_l1484z00_2265);
																									SET_CAR(BgL_tmpz00_4698,
																										BgL_arg1714z00_2268);
																							}}
																							{	/* SawBbv/bbv.scm 127 */
																								obj_t BgL_arg1722z00_2274;

																								BgL_arg1722z00_2274 =
																									CDR(
																									((obj_t) BgL_l1484z00_2265));
																								{
																									obj_t BgL_l1484z00_4703;

																									BgL_l1484z00_4703 =
																										BgL_arg1722z00_2274;
																									BgL_l1484z00_2265 =
																										BgL_l1484z00_4703;
																									goto
																										BgL_zc3z04anonymousza31711ze3z87_2266;
																								}
																							}
																						}
																				}
																				BgL_tmp1174z00_2248 = BgL_bz00_2255;
																			}
																		}
																	}
																}
															}
														}
													}
													{	/* SawBbv/bbv.scm 95 */
														bool_t BgL_test2485z00_4704;

														{	/* SawBbv/bbv.scm 95 */
															obj_t BgL_arg1827z00_3440;

															BgL_arg1827z00_3440 =
																BGL_EXITD_PROTECT(BgL_exitd1172z00_2246);
															BgL_test2485z00_4704 = PAIRP(BgL_arg1827z00_3440);
														}
														if (BgL_test2485z00_4704)
															{	/* SawBbv/bbv.scm 95 */
																obj_t BgL_arg1825z00_3441;

																{	/* SawBbv/bbv.scm 95 */
																	obj_t BgL_arg1826z00_3442;

																	BgL_arg1826z00_3442 =
																		BGL_EXITD_PROTECT(BgL_exitd1172z00_2246);
																	BgL_arg1825z00_3441 =
																		CDR(((obj_t) BgL_arg1826z00_3442));
																}
																BGL_EXITD_PROTECT_SET(BgL_exitd1172z00_2246,
																	BgL_arg1825z00_3441);
																BUNSPEC;
															}
														else
															{	/* SawBbv/bbv.scm 95 */
																BFALSE;
															}
													}
													BGl_z62zc3z04anonymousza31753ze3ze5zzsaw_bbvz00
														(BgL_zc3z04anonymousza31753ze3z87_4072);
													return BgL_tmp1174z00_2248;
												}
											}
										}
									}
								}
							}
						}
					}
				else
					{	/* SawBbv/bbv.scm 50 */
						return BgL_blocksz00_94;
					}
			}
		}

	}



/* &bbv */
	obj_t BGl_z62bbvz62zzsaw_bbvz00(obj_t BgL_envz00_4073, obj_t BgL_backz00_4074,
		obj_t BgL_globalz00_4075, obj_t BgL_paramsz00_4076,
		obj_t BgL_blocksz00_4077)
	{
		{	/* SawBbv/bbv.scm 49 */
			return
				BGl_bbvz00zzsaw_bbvz00(
				((BgL_backendz00_bglt) BgL_backz00_4074),
				((BgL_globalz00_bglt) BgL_globalz00_4075), BgL_paramsz00_4076,
				BgL_blocksz00_4077);
		}

	}



/* &<@anonymous:1753> */
	obj_t BGl_z62zc3z04anonymousza31753ze3ze5zzsaw_bbvz00(obj_t BgL_envz00_4078)
	{
		{	/* SawBbv/bbv.scm 95 */
			{	/* SawBbv/bbv.scm 131 */
				obj_t BgL_regsz00_4079;

				BgL_regsz00_4079 = ((obj_t) PROCEDURE_REF(BgL_envz00_4078, (int) (0L)));
				{	/* SawBbv/bbv.scm 131 */
					bool_t BgL_tmpz00_4718;

					{	/* SawBbv/bbv.scm 131 */
						bool_t BgL_test2486z00_4719;

						if (
							((long) CINT(BGl_za2compilerzd2debugza2zd2zzengine_paramz00) >=
								1L))
							{	/* SawBbv/bbv.scm 131 */
								BgL_test2486z00_4719 = ((bool_t) 1);
							}
						else
							{	/* SawBbv/bbv.scm 131 */
								BgL_test2486z00_4719 =
									(
									(long) CINT(BGl_za2tracezd2levelza2zd2zzengine_paramz00) >=
									1L);
							}
						if (BgL_test2486z00_4719)
							{	/* SawBbv/bbv.scm 131 */
								BgL_tmpz00_4718 = ((bool_t) 0);
							}
						else
							{
								obj_t BgL_l1481z00_4132;

								BgL_l1481z00_4132 = BgL_regsz00_4079;
							BgL_zc3z04anonymousza31757ze3z87_4131:
								if (PAIRP(BgL_l1481z00_4132))
									{	/* SawBbv/bbv.scm 133 */
										{	/* SawBbv/bbv.scm 133 */
											obj_t BgL_rz00_4133;

											BgL_rz00_4133 = CAR(BgL_l1481z00_4132);
											{	/* SawBbv/bbv.scm 133 */
												long BgL_arg1761z00_4134;

												{	/* SawBbv/bbv.scm 133 */
													obj_t BgL_arg1762z00_4135;

													{	/* SawBbv/bbv.scm 133 */
														obj_t BgL_arg1765z00_4136;

														{	/* SawBbv/bbv.scm 133 */
															obj_t BgL_arg1815z00_4137;
															long BgL_arg1816z00_4138;

															BgL_arg1815z00_4137 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawBbv/bbv.scm 133 */
																long BgL_arg1817z00_4139;

																BgL_arg1817z00_4139 =
																	BGL_OBJECT_CLASS_NUM(
																	((BgL_objectz00_bglt) BgL_rz00_4133));
																BgL_arg1816z00_4138 =
																	(BgL_arg1817z00_4139 - OBJECT_TYPE);
															}
															BgL_arg1765z00_4136 =
																VECTOR_REF(BgL_arg1815z00_4137,
																BgL_arg1816z00_4138);
														}
														BgL_arg1762z00_4135 =
															BGl_classzd2superzd2zz__objectz00
															(BgL_arg1765z00_4136);
													}
													{	/* SawBbv/bbv.scm 133 */
														obj_t BgL_tmpz00_4734;

														BgL_tmpz00_4734 = ((obj_t) BgL_arg1762z00_4135);
														BgL_arg1761z00_4134 =
															BGL_CLASS_NUM(BgL_tmpz00_4734);
												}}
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_rz00_4133),
													BgL_arg1761z00_4134);
											}
											{	/* SawBbv/bbv.scm 133 */
												BgL_objectz00_bglt BgL_tmpz00_4739;

												BgL_tmpz00_4739 = ((BgL_objectz00_bglt) BgL_rz00_4133);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4739, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_rz00_4133);
											BgL_rz00_4133;
										}
										{
											obj_t BgL_l1481z00_4743;

											BgL_l1481z00_4743 = CDR(BgL_l1481z00_4132);
											BgL_l1481z00_4132 = BgL_l1481z00_4743;
											goto BgL_zc3z04anonymousza31757ze3z87_4131;
										}
									}
								else
									{	/* SawBbv/bbv.scm 133 */
										BgL_tmpz00_4718 = ((bool_t) 1);
									}
							}
					}
					return BBOOL(BgL_tmpz00_4718);
				}
			}
		}

	}



/* dump-blocks */
	BgL_blockz00_bglt BGl_dumpzd2blockszd2zzsaw_bbvz00(long BgL_levelz00_95,
		obj_t BgL_namez00_96, BgL_globalz00_bglt BgL_globalz00_97,
		obj_t BgL_paramsz00_98, obj_t BgL_regsz00_99, obj_t BgL_historyz00_100,
		BgL_blockz00_bglt BgL_bz00_101)
	{
		{	/* SawBbv/bbv.scm 139 */
			{	/* SawBbv/bbv.scm 140 */
				bool_t BgL_test2489z00_4746;

				if (CBOOL(BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2))
					{	/* SawBbv/bbv.scm 140 */
						bool_t BgL__ortest_1178z00_2320;

						BgL__ortest_1178z00_2320 =
							(
							(long) CINT(BGl_za2tracezd2levelza2zd2zzengine_paramz00) >=
							BgL_levelz00_95);
						if (BgL__ortest_1178z00_2320)
							{	/* SawBbv/bbv.scm 140 */
								BgL_test2489z00_4746 = BgL__ortest_1178z00_2320;
							}
						else
							{	/* SawBbv/bbv.scm 140 */
								BgL_test2489z00_4746 = (BgL_levelz00_95 == 1L);
							}
					}
				else
					{	/* SawBbv/bbv.scm 140 */
						BgL_test2489z00_4746 = ((bool_t) 0);
					}
				if (BgL_test2489z00_4746)
					{	/* SawBbv/bbv.scm 142 */
						obj_t BgL_arg1805z00_2317;
						obj_t BgL_arg1806z00_2318;

						BgL_arg1805z00_2317 =
							BGl_blockzd2ze3blockzd2listze3zzsaw_bbvzd2utilszd2(BgL_regsz00_99,
							((BgL_blockz00_bglt) BgL_bz00_101));
						{	/* SawBbv/bbv.scm 142 */
							obj_t BgL_list1807z00_2319;

							BgL_list1807z00_2319 = MAKE_YOUNG_PAIR(BgL_namez00_96, BNIL);
							BgL_arg1806z00_2318 =
								BGl_formatz00zz__r4_output_6_10_3z00
								(BGl_string2371z00zzsaw_bbvz00, BgL_list1807z00_2319);
						}
						BGl_dumpzd2cfgzd2zzsaw_bbvzd2debugzd2(BgL_globalz00_97,
							BgL_paramsz00_98, BgL_arg1805z00_2317, BgL_arg1806z00_2318);
					}
				else
					{	/* SawBbv/bbv.scm 140 */
						BFALSE;
					}
			}
			{	/* SawBbv/bbv.scm 143 */
				bool_t BgL_test2492z00_4758;

				if (CBOOL(BGl_za2bbvzd2dumpzd2jsonza2z00zzsaw_bbvzd2configzd2))
					{	/* SawBbv/bbv.scm 143 */
						bool_t BgL__ortest_1179z00_2326;

						BgL__ortest_1179z00_2326 =
							(
							(long) CINT(BGl_za2tracezd2levelza2zd2zzengine_paramz00) >=
							BgL_levelz00_95);
						if (BgL__ortest_1179z00_2326)
							{	/* SawBbv/bbv.scm 143 */
								BgL_test2492z00_4758 = BgL__ortest_1179z00_2326;
							}
						else
							{	/* SawBbv/bbv.scm 143 */
								BgL_test2492z00_4758 = (BgL_levelz00_95 == 1L);
							}
					}
				else
					{	/* SawBbv/bbv.scm 143 */
						BgL_test2492z00_4758 = ((bool_t) 0);
					}
				if (BgL_test2492z00_4758)
					{	/* SawBbv/bbv.scm 145 */
						obj_t BgL_arg1812z00_2323;
						obj_t BgL_arg1820z00_2324;

						BgL_arg1812z00_2323 =
							BGl_blockzd2ze3blockzd2listze3zzsaw_bbvzd2utilszd2(BgL_regsz00_99,
							((BgL_blockz00_bglt) BgL_bz00_101));
						{	/* SawBbv/bbv.scm 145 */
							obj_t BgL_list1821z00_2325;

							BgL_list1821z00_2325 = MAKE_YOUNG_PAIR(BgL_namez00_96, BNIL);
							BgL_arg1820z00_2324 =
								BGl_formatz00zz__r4_output_6_10_3z00
								(BGl_string2372z00zzsaw_bbvz00, BgL_list1821z00_2325);
						}
						BGl_dumpzd2jsonzd2cfgz00zzsaw_bbvzd2debugzd2(BgL_globalz00_97,
							BgL_paramsz00_98, BgL_historyz00_100, BgL_arg1812z00_2323,
							BgL_arg1820z00_2324);
					}
				else
					{	/* SawBbv/bbv.scm 143 */
						BFALSE;
					}
			}
			return BgL_bz00_101;
		}

	}



/* mark-merge! */
	obj_t BGl_markzd2mergez12zc0zzsaw_bbvz00(BgL_blockz00_bglt BgL_blockz00_102)
	{
		{	/* SawBbv/bbv.scm 154 */
			return BGl_loopze70ze7zzsaw_bbvz00(((obj_t) BgL_blockz00_102), BNIL);
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzsaw_bbvz00(obj_t BgL_blockz00_2329,
		obj_t BgL_stackz00_2330)
	{
		{	/* SawBbv/bbv.scm 156 */
			{	/* SawBbv/bbv.scm 160 */
				bool_t BgL_test2495z00_4772;

				{	/* SawBbv/bbv.scm 160 */
					obj_t BgL_arg1835z00_2348;

					{
						BgL_blockvz00_bglt BgL_auxz00_4773;

						{
							obj_t BgL_auxz00_4774;

							{	/* SawBbv/bbv.scm 160 */
								BgL_objectz00_bglt BgL_tmpz00_4775;

								BgL_tmpz00_4775 =
									((BgL_objectz00_bglt)
									((BgL_blockz00_bglt) BgL_blockz00_2329));
								BgL_auxz00_4774 = BGL_OBJECT_WIDENING(BgL_tmpz00_4775);
							}
							BgL_auxz00_4773 = ((BgL_blockvz00_bglt) BgL_auxz00_4774);
						}
						BgL_arg1835z00_2348 =
							(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4773))->BgL_mergez00);
					}
					BgL_test2495z00_4772 = (BgL_arg1835z00_2348 == BUNSPEC);
				}
				if (BgL_test2495z00_4772)
					{	/* SawBbv/bbv.scm 160 */
						if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
								(BgL_blockz00_2329, BgL_stackz00_2330)))
							{
								BgL_blockvz00_bglt BgL_auxz00_4785;

								{
									obj_t BgL_auxz00_4786;

									{	/* SawBbv/bbv.scm 164 */
										BgL_objectz00_bglt BgL_tmpz00_4787;

										BgL_tmpz00_4787 =
											((BgL_objectz00_bglt)
											((BgL_blockz00_bglt) BgL_blockz00_2329));
										BgL_auxz00_4786 = BGL_OBJECT_WIDENING(BgL_tmpz00_4787);
									}
									BgL_auxz00_4785 = ((BgL_blockvz00_bglt) BgL_auxz00_4786);
								}
								return
									((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4785))->
										BgL_mergez00) = ((obj_t) BTRUE), BUNSPEC);
							}
						else
							{	/* SawBbv/bbv.scm 166 */
								obj_t BgL_nstackz00_2336;

								BgL_nstackz00_2336 =
									MAKE_YOUNG_PAIR(BgL_blockz00_2329, BgL_stackz00_2330);
								{	/* SawBbv/bbv.scm 167 */
									obj_t BgL_g1488z00_2337;

									BgL_g1488z00_2337 =
										(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt)
													((BgL_blockz00_bglt) BgL_blockz00_2329))))->
										BgL_succsz00);
									{
										obj_t BgL_l1486z00_2339;

										BgL_l1486z00_2339 = BgL_g1488z00_2337;
									BgL_zc3z04anonymousza31826ze3z87_2340:
										if (PAIRP(BgL_l1486z00_2339))
											{	/* SawBbv/bbv.scm 167 */
												BGl_loopze70ze7zzsaw_bbvz00(CAR(BgL_l1486z00_2339),
													BgL_nstackz00_2336);
												{
													obj_t BgL_l1486z00_4801;

													BgL_l1486z00_4801 = CDR(BgL_l1486z00_2339);
													BgL_l1486z00_2339 = BgL_l1486z00_4801;
													goto BgL_zc3z04anonymousza31826ze3z87_2340;
												}
											}
										else
											{	/* SawBbv/bbv.scm 167 */
												((bool_t) 1);
											}
									}
								}
								{	/* SawBbv/bbv.scm 170 */
									bool_t BgL_test2498z00_4803;

									{	/* SawBbv/bbv.scm 170 */
										obj_t BgL_arg1834z00_2347;

										{
											BgL_blockvz00_bglt BgL_auxz00_4804;

											{
												obj_t BgL_auxz00_4805;

												{	/* SawBbv/bbv.scm 170 */
													BgL_objectz00_bglt BgL_tmpz00_4806;

													BgL_tmpz00_4806 =
														((BgL_objectz00_bglt)
														((BgL_blockz00_bglt) BgL_blockz00_2329));
													BgL_auxz00_4805 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4806);
												}
												BgL_auxz00_4804 =
													((BgL_blockvz00_bglt) BgL_auxz00_4805);
											}
											BgL_arg1834z00_2347 =
												(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4804))->
												BgL_mergez00);
										}
										BgL_test2498z00_4803 = (BgL_arg1834z00_2347 == BTRUE);
									}
									if (BgL_test2498z00_4803)
										{	/* SawBbv/bbv.scm 170 */
											return BFALSE;
										}
									else
										{
											BgL_blockvz00_bglt BgL_auxz00_4813;

											{
												obj_t BgL_auxz00_4814;

												{	/* SawBbv/bbv.scm 171 */
													BgL_objectz00_bglt BgL_tmpz00_4815;

													BgL_tmpz00_4815 =
														((BgL_objectz00_bglt)
														((BgL_blockz00_bglt) BgL_blockz00_2329));
													BgL_auxz00_4814 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4815);
												}
												BgL_auxz00_4813 =
													((BgL_blockvz00_bglt) BgL_auxz00_4814);
											}
											return
												((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4813))->
													BgL_mergez00) = ((obj_t) BFALSE), BUNSPEC);
										}
								}
							}
					}
				else
					{	/* SawBbv/bbv.scm 160 */
						return BUNSPEC;
					}
			}
		}

	}



/* reorder-succs! */
	bool_t BGl_reorderzd2succsz12zc0zzsaw_bbvz00(obj_t BgL_blocksz00_103)
	{
		{	/* SawBbv/bbv.scm 176 */
			{	/* SawBbv/bbv.scm 178 */
				bool_t BgL_test2499z00_4821;

				if (NULLP(BgL_blocksz00_103))
					{	/* SawBbv/bbv.scm 178 */
						BgL_test2499z00_4821 = ((bool_t) 0);
					}
				else
					{	/* SawBbv/bbv.scm 178 */
						obj_t BgL_tmpz00_4824;

						BgL_tmpz00_4824 = CDR(BgL_blocksz00_103);
						BgL_test2499z00_4821 = PAIRP(BgL_tmpz00_4824);
					}
				if (BgL_test2499z00_4821)
					{
						obj_t BgL_bsz00_2354;

						BgL_bsz00_2354 = BgL_blocksz00_103;
					BgL_zc3z04anonymousza31839ze3z87_2355:
						{	/* SawBbv/bbv.scm 180 */
							bool_t BgL_test2501z00_4827;

							{	/* SawBbv/bbv.scm 180 */
								obj_t BgL_tmpz00_4828;

								BgL_tmpz00_4828 = CDR(((obj_t) BgL_bsz00_2354));
								BgL_test2501z00_4827 = PAIRP(BgL_tmpz00_4828);
							}
							if (BgL_test2501z00_4827)
								{	/* SawBbv/bbv.scm 181 */
									obj_t BgL_bz00_2358;
									obj_t BgL_nz00_2359;

									BgL_bz00_2358 = CAR(((obj_t) BgL_bsz00_2354));
									{	/* SawBbv/bbv.scm 182 */
										obj_t BgL_pairz00_3462;

										BgL_pairz00_3462 = CDR(((obj_t) BgL_bsz00_2354));
										BgL_nz00_2359 = CAR(BgL_pairz00_3462);
									}
									if (NULLP(
											(((BgL_blockz00_bglt) COBJECT(
														((BgL_blockz00_bglt) BgL_bz00_2358)))->
												BgL_succsz00)))
										{	/* SawBbv/bbv.scm 184 */
											BFALSE;
										}
									else
										{	/* SawBbv/bbv.scm 185 */
											bool_t BgL_test2503z00_4841;

											{	/* SawBbv/bbv.scm 185 */
												bool_t BgL_test2504z00_4842;

												{	/* SawBbv/bbv.scm 185 */
													obj_t BgL_arg1862z00_2380;

													BgL_arg1862z00_2380 =
														CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
															(((BgL_blockz00_bglt) COBJECT(
																		((BgL_blockz00_bglt) BgL_bz00_2358)))->
																BgL_firstz00)));
													BgL_test2504z00_4842 =
														BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2((
															(BgL_rtl_insz00_bglt) BgL_arg1862z00_2380));
												}
												if (BgL_test2504z00_4842)
													{	/* SawBbv/bbv.scm 185 */
														BgL_test2503z00_4841 = ((bool_t) 1);
													}
												else
													{	/* SawBbv/bbv.scm 186 */
														obj_t BgL_arg1858z00_2377;

														BgL_arg1858z00_2377 =
															CAR
															(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00((
																	((BgL_blockz00_bglt)
																		COBJECT(((BgL_blockz00_bglt)
																				BgL_bz00_2358)))->BgL_firstz00)));
														BgL_test2503z00_4841 =
															CBOOL
															(BGl_rtl_inszd2switchzf3z21zzsaw_bbvzd2typeszd2((
																	(BgL_rtl_insz00_bglt) BgL_arg1858z00_2377)));
													}
											}
											if (BgL_test2503z00_4841)
												{	/* SawBbv/bbv.scm 185 */
													BFALSE;
												}
											else
												{
													obj_t BgL_auxz00_4856;

													{	/* SawBbv/bbv.scm 187 */
														obj_t BgL_arg1856z00_2374;

														{	/* SawBbv/bbv.scm 187 */
															obj_t BgL_arg1857z00_2375;

															BgL_arg1857z00_2375 =
																(((BgL_blockz00_bglt) COBJECT(
																		((BgL_blockz00_bglt) BgL_bz00_2358)))->
																BgL_succsz00);
															BgL_arg1856z00_2374 =
																bgl_remq_bang(BgL_nz00_2359,
																BgL_arg1857z00_2375);
														}
														BgL_auxz00_4856 =
															MAKE_YOUNG_PAIR(BgL_nz00_2359,
															BgL_arg1856z00_2374);
													}
													((((BgL_blockz00_bglt) COBJECT(
																	((BgL_blockz00_bglt) BgL_bz00_2358)))->
															BgL_succsz00) =
														((obj_t) BgL_auxz00_4856), BUNSPEC);
												}
										}
									{	/* SawBbv/bbv.scm 188 */
										obj_t BgL_arg1868z00_2384;

										BgL_arg1868z00_2384 = CDR(((obj_t) BgL_bsz00_2354));
										{
											obj_t BgL_bsz00_4865;

											BgL_bsz00_4865 = BgL_arg1868z00_2384;
											BgL_bsz00_2354 = BgL_bsz00_4865;
											goto BgL_zc3z04anonymousza31839ze3z87_2355;
										}
									}
								}
							else
								{	/* SawBbv/bbv.scm 180 */
									return ((bool_t) 0);
								}
						}
					}
				else
					{	/* SawBbv/bbv.scm 178 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* gc! */
	BgL_blockz00_bglt BGl_gcz12z12zzsaw_bbvz00(BgL_blockz00_bglt BgL_bz00_104)
	{
		{	/* SawBbv/bbv.scm 197 */
			{
				BgL_blockz00_bglt BgL_bz00_2431;
				obj_t BgL_setmz00_2432;
				BgL_blockz00_bglt BgL_bz00_2409;

				{	/* SawBbv/bbv.scm 231 */
					bool_t BgL_test2505z00_4866;

					if (
						(BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2 == BTRUE))
						{	/* SawBbv/bbv.scm 231 */
							BgL_test2505z00_4866 = ((bool_t) 1);
						}
					else
						{	/* SawBbv/bbv.scm 231 */
							if (STRINGP
								(BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2))
								{	/* SawBbv/bbv.scm 233 */
									obj_t BgL__ortest_1191z00_2402;

									{	/* SawBbv/bbv.scm 233 */
										obj_t BgL_g1868z00_2406;

										BgL_g1868z00_2406 =
											BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2;
										{	/* SawBbv/bbv.scm 233 */

											BgL__ortest_1191z00_2402 =
												BGl_stringzd2containszd2zz__r4_strings_6_7z00
												(BgL_g1868z00_2406, BGl_string2362z00zzsaw_bbvz00,
												(int) (0L));
									}}
									if (CBOOL(BgL__ortest_1191z00_2402))
										{	/* SawBbv/bbv.scm 233 */
											BgL_test2505z00_4866 = CBOOL(BgL__ortest_1191z00_2402);
										}
									else
										{	/* SawBbv/bbv.scm 234 */
											obj_t BgL_g1868z00_2403;

											BgL_g1868z00_2403 =
												BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2;
											{	/* SawBbv/bbv.scm 234 */

												BgL_test2505z00_4866 =
													CBOOL(BGl_stringzd2containszd2zz__r4_strings_6_7z00
													(BgL_g1868z00_2403, BGl_string2374z00zzsaw_bbvz00,
														(int) (0L)));
								}}}
							else
								{	/* SawBbv/bbv.scm 232 */
									BgL_test2505z00_4866 = ((bool_t) 0);
								}
						}
					if (BgL_test2505z00_4866)
						{	/* SawBbv/bbv.scm 231 */
							BgL_bz00_2431 = BgL_bz00_104;
							BgL_bz00_2409 = BgL_bz00_104;
							{	/* SawBbv/bbv.scm 200 */
								obj_t BgL_g1183z00_2411;
								obj_t BgL_g1184z00_2412;

								{	/* SawBbv/bbv.scm 200 */
									obj_t BgL_list1888z00_2430;

									BgL_list1888z00_2430 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_2409), BNIL);
									BgL_g1183z00_2411 = BgL_list1888z00_2430;
								}
								BgL_g1184z00_2412 = BGl_makezd2emptyzd2bbsetz00zzsaw_bbvz00();
								{
									obj_t BgL_bsz00_2414;
									obj_t BgL_setz00_2415;

									BgL_bsz00_2414 = BgL_g1183z00_2411;
									BgL_setz00_2415 = BgL_g1184z00_2412;
								BgL_zc3z04anonymousza31874ze3z87_2416:
									if (NULLP(BgL_bsz00_2414))
										{	/* SawBbv/bbv.scm 203 */
											BgL_setmz00_2432 = BgL_setz00_2415;
										}
									else
										{	/* SawBbv/bbv.scm 205 */
											bool_t BgL_test2516z00_4996;

											{	/* SawBbv/bbv.scm 205 */
												BgL_blockz00_bglt BgL_blockz00_3468;

												BgL_blockz00_3468 =
													((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2414)));
												{	/* SawBbv/bbv.scm 205 */
													long BgL_arg1584z00_3471;
													obj_t BgL_arg1585z00_3472;

													{
														BgL_blocksz00_bglt BgL_auxz00_5000;

														{
															obj_t BgL_auxz00_5001;

															{	/* SawBbv/bbv.scm 205 */
																BgL_objectz00_bglt BgL_tmpz00_5002;

																BgL_tmpz00_5002 =
																	((BgL_objectz00_bglt) BgL_blockz00_3468);
																BgL_auxz00_5001 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5002);
															}
															BgL_auxz00_5000 =
																((BgL_blocksz00_bglt) BgL_auxz00_5001);
														}
														BgL_arg1584z00_3471 =
															(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5000))->
															BgL_z52markz52);
													}
													BgL_arg1585z00_3472 =
														STRUCT_REF(BgL_setz00_2415, (int) (0L));
													BgL_test2516z00_4996 =
														(BINT(BgL_arg1584z00_3471) == BgL_arg1585z00_3472);
											}}
											if (BgL_test2516z00_4996)
												{	/* SawBbv/bbv.scm 206 */
													obj_t BgL_arg1878z00_2420;

													BgL_arg1878z00_2420 = CDR(((obj_t) BgL_bsz00_2414));
													{
														obj_t BgL_bsz00_5013;

														BgL_bsz00_5013 = BgL_arg1878z00_2420;
														BgL_bsz00_2414 = BgL_bsz00_5013;
														goto BgL_zc3z04anonymousza31874ze3z87_2416;
													}
												}
											else
												{	/* SawBbv/bbv.scm 208 */
													BgL_blockz00_bglt BgL_i1185z00_2421;

													BgL_i1185z00_2421 =
														((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2414)));
													if (CBOOL
														(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2))
														{	/* SawBbv/bbv.scm 210 */
															obj_t BgL_arg1879z00_2422;

															BgL_arg1879z00_2422 =
																CAR(((obj_t) BgL_bsz00_2414));
															BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(
																((BgL_blockz00_bglt) BgL_arg1879z00_2422),
																BGl_string2373z00zzsaw_bbvz00);
														}
													else
														{	/* SawBbv/bbv.scm 209 */
															BFALSE;
														}
													{	/* SawBbv/bbv.scm 211 */
														obj_t BgL_arg1880z00_2423;
														obj_t BgL_arg1882z00_2424;

														{	/* SawBbv/bbv.scm 211 */
															obj_t BgL_arg1883z00_2425;
															obj_t BgL_arg1884z00_2426;

															BgL_arg1883z00_2425 =
																(((BgL_blockz00_bglt) COBJECT(
																		((BgL_blockz00_bglt) BgL_i1185z00_2421)))->
																BgL_succsz00);
															BgL_arg1884z00_2426 =
																CDR(((obj_t) BgL_bsz00_2414));
															BgL_arg1880z00_2423 =
																BGl_appendzd221011zd2zzsaw_bbvz00
																(BgL_arg1883z00_2425, BgL_arg1884z00_2426);
														}
														{	/* SawBbv/bbv.scm 212 */
															obj_t BgL_arg1885z00_2427;

															BgL_arg1885z00_2427 =
																CAR(((obj_t) BgL_bsz00_2414));
															BgL_arg1882z00_2424 =
																BGl_bbsetzd2conszd2zzsaw_bbvz00(
																((BgL_blockz00_bglt) BgL_arg1885z00_2427),
																BgL_setz00_2415);
														}
														{
															obj_t BgL_setz00_5033;
															obj_t BgL_bsz00_5032;

															BgL_bsz00_5032 = BgL_arg1880z00_2423;
															BgL_setz00_5033 = BgL_arg1882z00_2424;
															BgL_setz00_2415 = BgL_setz00_5033;
															BgL_bsz00_2414 = BgL_bsz00_5032;
															goto BgL_zc3z04anonymousza31874ze3z87_2416;
														}
													}
												}
										}
								}
							}
							{	/* SawBbv/bbv.scm 215 */
								obj_t BgL_g1186z00_2434;

								{	/* SawBbv/bbv.scm 215 */
									obj_t BgL_list1919z00_2483;

									BgL_list1919z00_2483 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_2431), BNIL);
									BgL_g1186z00_2434 = BgL_list1919z00_2483;
								}
								{
									obj_t BgL_bsz00_2437;
									obj_t BgL_setz00_2438;

									BgL_bsz00_2437 = BgL_g1186z00_2434;
									BgL_setz00_2438 = BNIL;
								BgL_zc3z04anonymousza31890ze3z87_2439:
									if (NULLP(BgL_bsz00_2437))
										{	/* SawBbv/bbv.scm 218 */
											BgL_setz00_2438;
										}
									else
										{	/* SawBbv/bbv.scm 218 */
											if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
															((obj_t) BgL_bsz00_2437)), BgL_setz00_2438)))
												{	/* SawBbv/bbv.scm 221 */
													obj_t BgL_arg1894z00_2443;

													BgL_arg1894z00_2443 = CDR(((obj_t) BgL_bsz00_2437));
													{
														obj_t BgL_bsz00_4890;

														BgL_bsz00_4890 = BgL_arg1894z00_2443;
														BgL_bsz00_2437 = BgL_bsz00_4890;
														goto BgL_zc3z04anonymousza31890ze3z87_2439;
													}
												}
											else
												{	/* SawBbv/bbv.scm 223 */
													BgL_blockz00_bglt BgL_i1188z00_2444;

													BgL_i1188z00_2444 =
														((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2437)));
													{
														obj_t BgL_auxz00_4894;

														{	/* SawBbv/bbv.scm 224 */
															obj_t BgL_hook1493z00_2445;

															BgL_hook1493z00_2445 =
																MAKE_YOUNG_PAIR(BFALSE, BNIL);
															{	/* SawBbv/bbv.scm 224 */
																obj_t BgL_g1494z00_2446;

																BgL_g1494z00_2446 =
																	(((BgL_blockz00_bglt) COBJECT(
																			((BgL_blockz00_bglt)
																				BgL_i1188z00_2444)))->BgL_predsz00);
																{
																	obj_t BgL_l1490z00_2448;
																	obj_t BgL_h1491z00_2449;

																	BgL_l1490z00_2448 = BgL_g1494z00_2446;
																	BgL_h1491z00_2449 = BgL_hook1493z00_2445;
																BgL_zc3z04anonymousza31895ze3z87_2450:
																	if (NULLP(BgL_l1490z00_2448))
																		{	/* SawBbv/bbv.scm 224 */
																			BgL_auxz00_4894 =
																				CDR(BgL_hook1493z00_2445);
																		}
																	else
																		{	/* SawBbv/bbv.scm 224 */
																			bool_t BgL_test2512z00_4902;

																			{	/* SawBbv/bbv.scm 224 */
																				BgL_blockz00_bglt BgL_blockz00_3486;

																				BgL_blockz00_3486 =
																					((BgL_blockz00_bglt)
																					CAR(((obj_t) BgL_l1490z00_2448)));
																				{	/* SawBbv/bbv.scm 224 */
																					long BgL_arg1584z00_3489;
																					obj_t BgL_arg1585z00_3490;

																					{
																						BgL_blocksz00_bglt BgL_auxz00_4906;

																						{
																							obj_t BgL_auxz00_4907;

																							{	/* SawBbv/bbv.scm 224 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_4908;
																								BgL_tmpz00_4908 =
																									((BgL_objectz00_bglt)
																									BgL_blockz00_3486);
																								BgL_auxz00_4907 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_4908);
																							}
																							BgL_auxz00_4906 =
																								((BgL_blocksz00_bglt)
																								BgL_auxz00_4907);
																						}
																						BgL_arg1584z00_3489 =
																							(((BgL_blocksz00_bglt)
																								COBJECT(BgL_auxz00_4906))->
																							BgL_z52markz52);
																					}
																					BgL_arg1585z00_3490 =
																						STRUCT_REF(BgL_setmz00_2432,
																						(int) (0L));
																					BgL_test2512z00_4902 =
																						(BINT(BgL_arg1584z00_3489) ==
																						BgL_arg1585z00_3490);
																			}}
																			if (BgL_test2512z00_4902)
																				{	/* SawBbv/bbv.scm 224 */
																					obj_t BgL_nh1492z00_2454;

																					{	/* SawBbv/bbv.scm 224 */
																						obj_t BgL_arg1899z00_2456;

																						BgL_arg1899z00_2456 =
																							CAR(((obj_t) BgL_l1490z00_2448));
																						BgL_nh1492z00_2454 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1899z00_2456, BNIL);
																					}
																					SET_CDR(BgL_h1491z00_2449,
																						BgL_nh1492z00_2454);
																					{	/* SawBbv/bbv.scm 224 */
																						obj_t BgL_arg1898z00_2455;

																						BgL_arg1898z00_2455 =
																							CDR(((obj_t) BgL_l1490z00_2448));
																						{
																							obj_t BgL_h1491z00_4924;
																							obj_t BgL_l1490z00_4923;

																							BgL_l1490z00_4923 =
																								BgL_arg1898z00_2455;
																							BgL_h1491z00_4924 =
																								BgL_nh1492z00_2454;
																							BgL_h1491z00_2449 =
																								BgL_h1491z00_4924;
																							BgL_l1490z00_2448 =
																								BgL_l1490z00_4923;
																							goto
																								BgL_zc3z04anonymousza31895ze3z87_2450;
																						}
																					}
																				}
																			else
																				{	/* SawBbv/bbv.scm 224 */
																					obj_t BgL_arg1901z00_2457;

																					BgL_arg1901z00_2457 =
																						CDR(((obj_t) BgL_l1490z00_2448));
																					{
																						obj_t BgL_l1490z00_4927;

																						BgL_l1490z00_4927 =
																							BgL_arg1901z00_2457;
																						BgL_l1490z00_2448 =
																							BgL_l1490z00_4927;
																						goto
																							BgL_zc3z04anonymousza31895ze3z87_2450;
																					}
																				}
																		}
																}
															}
														}
														((((BgL_blockz00_bglt) COBJECT(
																		((BgL_blockz00_bglt) BgL_i1188z00_2444)))->
																BgL_predsz00) =
															((obj_t) BgL_auxz00_4894), BUNSPEC);
													}
													{	/* SawBbv/bbv.scm 225 */
														BgL_blockz00_bglt BgL_i1189z00_2460;

														{
															BgL_blocksz00_bglt BgL_auxz00_4929;

															{
																obj_t BgL_auxz00_4930;

																{	/* SawBbv/bbv.scm 226 */
																	BgL_objectz00_bglt BgL_tmpz00_4931;

																	BgL_tmpz00_4931 =
																		((BgL_objectz00_bglt) BgL_i1188z00_2444);
																	BgL_auxz00_4930 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_4931);
																}
																BgL_auxz00_4929 =
																	((BgL_blocksz00_bglt) BgL_auxz00_4930);
															}
															BgL_i1189z00_2460 =
																(((BgL_blocksz00_bglt)
																	COBJECT(BgL_auxz00_4929))->BgL_parentz00);
														}
														{
															obj_t BgL_auxz00_4942;
															BgL_blockvz00_bglt BgL_auxz00_4936;

															{	/* SawBbv/bbv.scm 227 */
																obj_t BgL_hook1499z00_2461;

																BgL_hook1499z00_2461 =
																	MAKE_YOUNG_PAIR(BFALSE, BNIL);
																{	/* SawBbv/bbv.scm 227 */
																	obj_t BgL_g1500z00_2462;

																	{
																		BgL_blockvz00_bglt BgL_auxz00_4944;

																		{
																			obj_t BgL_auxz00_4945;

																			{	/* SawBbv/bbv.scm 227 */
																				BgL_objectz00_bglt BgL_tmpz00_4946;

																				BgL_tmpz00_4946 =
																					((BgL_objectz00_bglt)
																					BgL_i1189z00_2460);
																				BgL_auxz00_4945 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_4946);
																			}
																			BgL_auxz00_4944 =
																				((BgL_blockvz00_bglt) BgL_auxz00_4945);
																		}
																		BgL_g1500z00_2462 =
																			(((BgL_blockvz00_bglt)
																				COBJECT(BgL_auxz00_4944))->
																			BgL_versionsz00);
																	}
																	{
																		obj_t BgL_l1496z00_2464;
																		obj_t BgL_h1497z00_2465;

																		BgL_l1496z00_2464 = BgL_g1500z00_2462;
																		BgL_h1497z00_2465 = BgL_hook1499z00_2461;
																	BgL_zc3z04anonymousza31902ze3z87_2466:
																		if (NULLP(BgL_l1496z00_2464))
																			{	/* SawBbv/bbv.scm 227 */
																				BgL_auxz00_4942 =
																					CDR(BgL_hook1499z00_2461);
																			}
																		else
																			{	/* SawBbv/bbv.scm 227 */
																				bool_t BgL_test2514z00_4954;

																				{	/* SawBbv/bbv.scm 227 */
																					BgL_blockz00_bglt BgL_blockz00_3502;

																					BgL_blockz00_3502 =
																						((BgL_blockz00_bglt)
																						CAR(((obj_t) BgL_l1496z00_2464)));
																					{	/* SawBbv/bbv.scm 227 */
																						long BgL_arg1584z00_3505;
																						obj_t BgL_arg1585z00_3506;

																						{
																							BgL_blocksz00_bglt
																								BgL_auxz00_4958;
																							{
																								obj_t BgL_auxz00_4959;

																								{	/* SawBbv/bbv.scm 227 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_4960;
																									BgL_tmpz00_4960 =
																										((BgL_objectz00_bglt)
																										BgL_blockz00_3502);
																									BgL_auxz00_4959 =
																										BGL_OBJECT_WIDENING
																										(BgL_tmpz00_4960);
																								}
																								BgL_auxz00_4958 =
																									((BgL_blocksz00_bglt)
																									BgL_auxz00_4959);
																							}
																							BgL_arg1584z00_3505 =
																								(((BgL_blocksz00_bglt)
																									COBJECT(BgL_auxz00_4958))->
																								BgL_z52markz52);
																						}
																						BgL_arg1585z00_3506 =
																							STRUCT_REF(BgL_setmz00_2432,
																							(int) (0L));
																						BgL_test2514z00_4954 =
																							(BINT(BgL_arg1584z00_3505) ==
																							BgL_arg1585z00_3506);
																				}}
																				if (BgL_test2514z00_4954)
																					{	/* SawBbv/bbv.scm 227 */
																						obj_t BgL_nh1498z00_2470;

																						{	/* SawBbv/bbv.scm 227 */
																							obj_t BgL_arg1910z00_2472;

																							BgL_arg1910z00_2472 =
																								CAR(
																								((obj_t) BgL_l1496z00_2464));
																							BgL_nh1498z00_2470 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1910z00_2472, BNIL);
																						}
																						SET_CDR(BgL_h1497z00_2465,
																							BgL_nh1498z00_2470);
																						{	/* SawBbv/bbv.scm 227 */
																							obj_t BgL_arg1906z00_2471;

																							BgL_arg1906z00_2471 =
																								CDR(
																								((obj_t) BgL_l1496z00_2464));
																							{
																								obj_t BgL_h1497z00_4976;
																								obj_t BgL_l1496z00_4975;

																								BgL_l1496z00_4975 =
																									BgL_arg1906z00_2471;
																								BgL_h1497z00_4976 =
																									BgL_nh1498z00_2470;
																								BgL_h1497z00_2465 =
																									BgL_h1497z00_4976;
																								BgL_l1496z00_2464 =
																									BgL_l1496z00_4975;
																								goto
																									BgL_zc3z04anonymousza31902ze3z87_2466;
																							}
																						}
																					}
																				else
																					{	/* SawBbv/bbv.scm 227 */
																						obj_t BgL_arg1911z00_2473;

																						BgL_arg1911z00_2473 =
																							CDR(((obj_t) BgL_l1496z00_2464));
																						{
																							obj_t BgL_l1496z00_4979;

																							BgL_l1496z00_4979 =
																								BgL_arg1911z00_2473;
																							BgL_l1496z00_2464 =
																								BgL_l1496z00_4979;
																							goto
																								BgL_zc3z04anonymousza31902ze3z87_2466;
																						}
																					}
																			}
																	}
																}
															}
															{
																obj_t BgL_auxz00_4937;

																{	/* SawBbv/bbv.scm 226 */
																	BgL_objectz00_bglt BgL_tmpz00_4938;

																	BgL_tmpz00_4938 =
																		((BgL_objectz00_bglt) BgL_i1189z00_2460);
																	BgL_auxz00_4937 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_4938);
																}
																BgL_auxz00_4936 =
																	((BgL_blockvz00_bglt) BgL_auxz00_4937);
															}
															((((BgL_blockvz00_bglt)
																		COBJECT(BgL_auxz00_4936))->
																	BgL_versionsz00) =
																((obj_t) BgL_auxz00_4942), BUNSPEC);
														}
													}
													{	/* SawBbv/bbv.scm 228 */
														obj_t BgL_arg1912z00_2476;
														obj_t BgL_arg1913z00_2477;

														{	/* SawBbv/bbv.scm 228 */
															obj_t BgL_arg1914z00_2478;
															obj_t BgL_arg1916z00_2479;

															BgL_arg1914z00_2478 =
																(((BgL_blockz00_bglt) COBJECT(
																		((BgL_blockz00_bglt) BgL_i1188z00_2444)))->
																BgL_succsz00);
															BgL_arg1916z00_2479 =
																CDR(((obj_t) BgL_bsz00_2437));
															BgL_arg1912z00_2476 =
																BGl_appendzd221011zd2zzsaw_bbvz00
																(BgL_arg1914z00_2478, BgL_arg1916z00_2479);
														}
														{	/* SawBbv/bbv.scm 229 */
															obj_t BgL_arg1917z00_2480;

															BgL_arg1917z00_2480 =
																CAR(((obj_t) BgL_bsz00_2437));
															BgL_arg1913z00_2477 =
																MAKE_YOUNG_PAIR(BgL_arg1917z00_2480,
																BgL_setz00_2438);
														}
														{
															obj_t BgL_setz00_4990;
															obj_t BgL_bsz00_4989;

															BgL_bsz00_4989 = BgL_arg1912z00_2476;
															BgL_setz00_4990 = BgL_arg1913z00_2477;
															BgL_setz00_2438 = BgL_setz00_4990;
															BgL_bsz00_2437 = BgL_bsz00_4989;
															goto BgL_zc3z04anonymousza31890ze3z87_2439;
														}
													}
												}
										}
								}
							}
						}
					else
						{	/* SawBbv/bbv.scm 231 */
							BFALSE;
						}
				}
				return BgL_bz00_104;
			}
		}

	}



/* normalize-ifeq! */
	obj_t BGl_normaliza7ezd2ifeqz12z67zzsaw_bbvz00(BgL_blockz00_bglt BgL_bz00_105)
	{
		{	/* SawBbv/bbv.scm 244 */
			{
				BgL_blockz00_bglt BgL_bz00_2534;

				{	/* SawBbv/bbv.scm 292 */
					obj_t BgL_g1208z00_2488;

					{	/* SawBbv/bbv.scm 292 */
						obj_t BgL_list1933z00_2507;

						BgL_list1933z00_2507 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_105), BNIL);
						BgL_g1208z00_2488 = BgL_list1933z00_2507;
					}
					{
						obj_t BgL_bsz00_2491;
						obj_t BgL_accz00_2492;

						BgL_bsz00_2491 = BgL_g1208z00_2488;
						BgL_accz00_2492 = BNIL;
					BgL_zc3z04anonymousza31920ze3z87_2493:
						if (NULLP(BgL_bsz00_2491))
							{	/* SawBbv/bbv.scm 295 */
								return bgl_reverse(BgL_accz00_2492);
							}
						else
							{	/* SawBbv/bbv.scm 295 */
								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
												((obj_t) BgL_bsz00_2491)), BgL_accz00_2492)))
									{	/* SawBbv/bbv.scm 298 */
										obj_t BgL_arg1925z00_2497;

										BgL_arg1925z00_2497 = CDR(((obj_t) BgL_bsz00_2491));
										{
											obj_t BgL_bsz00_5046;

											BgL_bsz00_5046 = BgL_arg1925z00_2497;
											BgL_bsz00_2491 = BgL_bsz00_5046;
											goto BgL_zc3z04anonymousza31920ze3z87_2493;
										}
									}
								else
									{	/* SawBbv/bbv.scm 297 */
										{	/* SawBbv/bbv.scm 300 */
											obj_t BgL_arg1926z00_2498;

											BgL_arg1926z00_2498 = CAR(((obj_t) BgL_bsz00_2491));
											BgL_bz00_2534 = ((BgL_blockz00_bglt) BgL_arg1926z00_2498);
											if (NULLP(
													(((BgL_blockz00_bglt) COBJECT(BgL_bz00_2534))->
														BgL_succsz00)))
												{	/* SawBbv/bbv.scm 255 */
													BFALSE;
												}
											else
												{
													obj_t BgL_insz00_2541;

													BgL_insz00_2541 =
														(((BgL_blockz00_bglt) COBJECT(BgL_bz00_2534))->
														BgL_firstz00);
												BgL_zc3z04anonymousza31958ze3z87_2542:
													if (NULLP(BgL_insz00_2541))
														{	/* SawBbv/bbv.scm 259 */
															if (NULLP(
																	(((BgL_blockz00_bglt)
																			COBJECT(BgL_bz00_2534))->BgL_firstz00)))
																{	/* SawBbv/bbv.scm 262 */
																	obj_t BgL_arg1962z00_2546;

																	BgL_arg1962z00_2546 =
																		BGl_shapez00zztools_shapez00(
																		((obj_t) BgL_bz00_2534));
																	BGl_errorz00zz__errorz00
																		(BGl_string2375z00zzsaw_bbvz00,
																		BGl_string2376z00zzsaw_bbvz00,
																		BgL_arg1962z00_2546);
																}
															else
																{	/* SawBbv/bbv.scm 260 */
																	BFALSE;
																}
														}
													else
														{	/* SawBbv/bbv.scm 263 */
															bool_t BgL_test2523z00_5060;

															{	/* SawBbv/bbv.scm 263 */
																obj_t BgL_arg2004z00_2603;

																BgL_arg2004z00_2603 =
																	CAR(((obj_t) BgL_insz00_2541));
																BgL_test2523z00_5060 =
																	BGl_rtl_inszd2ifnezf3z21zzsaw_bbvzd2typeszd2(
																	((BgL_rtl_insz00_bglt) BgL_arg2004z00_2603));
															}
															if (BgL_test2523z00_5060)
																{	/* SawBbv/bbv.scm 264 */
																	bool_t BgL_test2524z00_5065;

																	if (NULLP(CDR(
																				(((BgL_blockz00_bglt)
																						COBJECT(BgL_bz00_2534))->
																					BgL_succsz00))))
																		{	/* SawBbv/bbv.scm 264 */
																			obj_t BgL_arg1974z00_2562;

																			BgL_arg1974z00_2562 =
																				CAR(((obj_t) BgL_insz00_2541));
																			BgL_test2524z00_5065 =
																				BGl_sidezd2effectzd2freezf3ze70z14zzsaw_bbvz00
																				(BgL_arg1974z00_2562);
																		}
																	else
																		{	/* SawBbv/bbv.scm 264 */
																			BgL_test2524z00_5065 = ((bool_t) 0);
																		}
																	if (BgL_test2524z00_5065)
																		{	/* SawBbv/bbv.scm 265 */
																			BgL_rtl_insz00_bglt BgL_i1196z00_2557;

																			BgL_i1196z00_2557 =
																				((BgL_rtl_insz00_bglt)
																				CAR(((obj_t) BgL_insz00_2541)));
																			{
																				BgL_rtl_funz00_bglt BgL_auxz00_5076;

																				{	/* SawBbv/bbv.scm 266 */
																					BgL_rtl_nopz00_bglt
																						BgL_new1198z00_2558;
																					{	/* SawBbv/bbv.scm 266 */
																						BgL_rtl_nopz00_bglt
																							BgL_new1197z00_2559;
																						BgL_new1197z00_2559 =
																							((BgL_rtl_nopz00_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_rtl_nopz00_bgl))));
																						{	/* SawBbv/bbv.scm 266 */
																							long BgL_arg1973z00_2560;

																							{	/* SawBbv/bbv.scm 266 */
																								obj_t BgL_classz00_3593;

																								BgL_classz00_3593 =
																									BGl_rtl_nopz00zzsaw_defsz00;
																								BgL_arg1973z00_2560 =
																									BGL_CLASS_NUM
																									(BgL_classz00_3593);
																							}
																							BGL_OBJECT_CLASS_NUM_SET(
																								((BgL_objectz00_bglt)
																									BgL_new1197z00_2559),
																								BgL_arg1973z00_2560);
																						}
																						BgL_new1198z00_2558 =
																							BgL_new1197z00_2559;
																					}
																					((((BgL_rtl_funz00_bglt) COBJECT(
																									((BgL_rtl_funz00_bglt)
																										BgL_new1198z00_2558)))->
																							BgL_locz00) =
																						((obj_t) BFALSE), BUNSPEC);
																					BgL_auxz00_5076 =
																						((BgL_rtl_funz00_bglt)
																						BgL_new1198z00_2558);
																				}
																				((((BgL_rtl_insz00_bglt)
																							COBJECT(BgL_i1196z00_2557))->
																						BgL_funz00) =
																					((BgL_rtl_funz00_bglt)
																						BgL_auxz00_5076), BUNSPEC);
																			}
																			((((BgL_rtl_insz00_bglt)
																						COBJECT(BgL_i1196z00_2557))->
																					BgL_argsz00) =
																				((obj_t) BNIL), BUNSPEC);
																		}
																	else
																		{	/* SawBbv/bbv.scm 264 */
																			BUNSPEC;
																		}
																}
															else
																{	/* SawBbv/bbv.scm 269 */
																	bool_t BgL_test2526z00_5086;

																	{	/* SawBbv/bbv.scm 269 */
																		obj_t BgL_arg2003z00_2602;

																		BgL_arg2003z00_2602 =
																			CAR(((obj_t) BgL_insz00_2541));
																		BgL_test2526z00_5086 =
																			BGl_rtl_inszd2ifeqzf3z21zzsaw_bbvzd2typeszd2
																			(((BgL_rtl_insz00_bglt)
																				BgL_arg2003z00_2602));
																	}
																	if (BgL_test2526z00_5086)
																		{	/* SawBbv/bbv.scm 271 */
																			BgL_rtl_insz00_bglt BgL_i1199z00_2567;

																			BgL_i1199z00_2567 =
																				((BgL_rtl_insz00_bglt)
																				CAR(((obj_t) BgL_insz00_2541)));
																			{	/* SawBbv/bbv.scm 273 */
																				bool_t BgL_test2527z00_5094;

																				if (NULLP(CDR(
																							((obj_t) BgL_insz00_2541))))
																					{	/* SawBbv/bbv.scm 273 */
																						BgL_test2527z00_5094 = ((bool_t) 1);
																					}
																				else
																					{	/* SawBbv/bbv.scm 273 */
																						bool_t BgL_test2529z00_5099;

																						{	/* SawBbv/bbv.scm 273 */
																							obj_t BgL_arg2000z00_2599;

																							{	/* SawBbv/bbv.scm 273 */
																								obj_t BgL_pairz00_3602;

																								BgL_pairz00_3602 =
																									CDR(
																									((obj_t) BgL_insz00_2541));
																								BgL_arg2000z00_2599 =
																									CAR(BgL_pairz00_3602);
																							}
																							BgL_test2529z00_5099 =
																								BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2
																								(((BgL_rtl_insz00_bglt)
																									BgL_arg2000z00_2599));
																						}
																						if (BgL_test2529z00_5099)
																							{	/* SawBbv/bbv.scm 273 */
																								BgL_test2527z00_5094 =
																									((bool_t) 0);
																							}
																						else
																							{	/* SawBbv/bbv.scm 273 */
																								BgL_test2527z00_5094 =
																									((bool_t) 1);
																							}
																					}
																				if (BgL_test2527z00_5094)
																					{	/* SawBbv/bbv.scm 274 */
																						obj_t BgL_arg1986z00_2575;

																						BgL_arg1986z00_2575 =
																							BGl_shapez00zztools_shapez00(
																							((obj_t) BgL_bz00_2534));
																						BGl_errorz00zz__errorz00
																							(BGl_string2375z00zzsaw_bbvz00,
																							BGl_string2377z00zzsaw_bbvz00,
																							BgL_arg1986z00_2575);
																					}
																				else
																					{	/* SawBbv/bbv.scm 275 */
																						bool_t BgL_test2530z00_5108;

																						if (NULLP(CDR(
																									(((BgL_blockz00_bglt)
																											COBJECT(BgL_bz00_2534))->
																										BgL_succsz00))))
																							{	/* SawBbv/bbv.scm 275 */
																								obj_t BgL_arg1997z00_2594;

																								BgL_arg1997z00_2594 =
																									CAR(
																									((obj_t) BgL_insz00_2541));
																								BgL_test2530z00_5108 =
																									BGl_sidezd2effectzd2freezf3ze70z14zzsaw_bbvz00
																									(BgL_arg1997z00_2594);
																							}
																						else
																							{	/* SawBbv/bbv.scm 275 */
																								BgL_test2530z00_5108 =
																									((bool_t) 0);
																							}
																						if (BgL_test2530z00_5108)
																							{	/* SawBbv/bbv.scm 275 */
																								{
																									BgL_rtl_funz00_bglt
																										BgL_auxz00_5116;
																									{	/* SawBbv/bbv.scm 276 */
																										BgL_rtl_nopz00_bglt
																											BgL_new1201z00_2583;
																										{	/* SawBbv/bbv.scm 276 */
																											BgL_rtl_nopz00_bglt
																												BgL_new1200z00_2584;
																											BgL_new1200z00_2584 =
																												((BgL_rtl_nopz00_bglt)
																												BOBJECT(GC_MALLOC(sizeof
																														(struct
																															BgL_rtl_nopz00_bgl))));
																											{	/* SawBbv/bbv.scm 276 */
																												long
																													BgL_arg1994z00_2585;
																												{	/* SawBbv/bbv.scm 276 */
																													obj_t
																														BgL_classz00_3605;
																													BgL_classz00_3605 =
																														BGl_rtl_nopz00zzsaw_defsz00;
																													BgL_arg1994z00_2585 =
																														BGL_CLASS_NUM
																														(BgL_classz00_3605);
																												}
																												BGL_OBJECT_CLASS_NUM_SET
																													(((BgL_objectz00_bglt)
																														BgL_new1200z00_2584),
																													BgL_arg1994z00_2585);
																											}
																											BgL_new1201z00_2583 =
																												BgL_new1200z00_2584;
																										}
																										((((BgL_rtl_funz00_bglt)
																													COBJECT((
																															(BgL_rtl_funz00_bglt)
																															BgL_new1201z00_2583)))->
																												BgL_locz00) =
																											((obj_t) BFALSE),
																											BUNSPEC);
																										BgL_auxz00_5116 =
																											((BgL_rtl_funz00_bglt)
																											BgL_new1201z00_2583);
																									}
																									((((BgL_rtl_insz00_bglt)
																												COBJECT
																												(BgL_i1199z00_2567))->
																											BgL_funz00) =
																										((BgL_rtl_funz00_bglt)
																											BgL_auxz00_5116),
																										BUNSPEC);
																								}
																								((((BgL_rtl_insz00_bglt)
																											COBJECT
																											(BgL_i1199z00_2567))->
																										BgL_argsz00) =
																									((obj_t) BNIL), BUNSPEC);
																							}
																						else
																							{	/* SawBbv/bbv.scm 279 */
																								BgL_rtl_ifeqz00_bglt
																									BgL_i1202z00_2586;
																								BgL_i1202z00_2586 =
																									((BgL_rtl_ifeqz00_bglt) ((
																											(BgL_rtl_insz00_bglt)
																											COBJECT
																											(BgL_i1199z00_2567))->
																										BgL_funz00));
																								{	/* SawBbv/bbv.scm 280 */
																									BgL_rtl_insz00_bglt
																										BgL_i1203z00_2587;
																									{	/* SawBbv/bbv.scm 280 */
																										obj_t BgL_pairz00_3611;

																										BgL_pairz00_3611 =
																											CDR(
																											((obj_t)
																												BgL_insz00_2541));
																										BgL_i1203z00_2587 =
																											((BgL_rtl_insz00_bglt)
																											CAR(BgL_pairz00_3611));
																									}
																									{	/* SawBbv/bbv.scm 281 */
																										BgL_rtl_goz00_bglt
																											BgL_i1204z00_2588;
																										BgL_i1204z00_2588 =
																											((BgL_rtl_goz00_bglt) ((
																													(BgL_rtl_insz00_bglt)
																													COBJECT
																													(BgL_i1203z00_2587))->
																												BgL_funz00));
																										{
																											BgL_rtl_funz00_bglt
																												BgL_auxz00_5134;
																											{	/* SawBbv/bbv.scm 283 */
																												BgL_rtl_ifnez00_bglt
																													BgL_new1206z00_2589;
																												{	/* SawBbv/bbv.scm 285 */
																													BgL_rtl_ifnez00_bglt
																														BgL_new1205z00_2590;
																													BgL_new1205z00_2590 =
																														(
																														(BgL_rtl_ifnez00_bglt)
																														BOBJECT(GC_MALLOC
																															(sizeof(struct
																																	BgL_rtl_ifnez00_bgl))));
																													{	/* SawBbv/bbv.scm 285 */
																														long
																															BgL_arg1995z00_2591;
																														{	/* SawBbv/bbv.scm 285 */
																															obj_t
																																BgL_classz00_3612;
																															BgL_classz00_3612
																																=
																																BGl_rtl_ifnez00zzsaw_defsz00;
																															BgL_arg1995z00_2591
																																=
																																BGL_CLASS_NUM
																																(BgL_classz00_3612);
																														}
																														BGL_OBJECT_CLASS_NUM_SET
																															(((BgL_objectz00_bglt) BgL_new1205z00_2590), BgL_arg1995z00_2591);
																													}
																													BgL_new1206z00_2589 =
																														BgL_new1205z00_2590;
																												}
																												((((BgL_rtl_funz00_bglt)
																															COBJECT((
																																	(BgL_rtl_funz00_bglt)
																																	BgL_new1206z00_2589)))->
																														BgL_locz00) =
																													((obj_t) ((
																																(BgL_rtl_funz00_bglt)
																																COBJECT((
																																		(BgL_rtl_funz00_bglt)
																																		BgL_i1202z00_2586)))->
																															BgL_locz00)),
																													BUNSPEC);
																												((((BgL_rtl_ifnez00_bglt) COBJECT(BgL_new1206z00_2589))->BgL_thenz00) = ((BgL_blockz00_bglt) (((BgL_rtl_goz00_bglt) COBJECT(BgL_i1204z00_2588))->BgL_toz00)), BUNSPEC);
																												BgL_auxz00_5134 =
																													((BgL_rtl_funz00_bglt)
																													BgL_new1206z00_2589);
																											}
																											((((BgL_rtl_insz00_bglt)
																														COBJECT
																														(BgL_i1199z00_2567))->
																													BgL_funz00) =
																												((BgL_rtl_funz00_bglt)
																													BgL_auxz00_5134),
																												BUNSPEC);
																										}
																										((((BgL_rtl_goz00_bglt)
																													COBJECT
																													(BgL_i1204z00_2588))->
																												BgL_toz00) =
																											((BgL_blockz00_bglt) ((
																														(BgL_rtl_ifeqz00_bglt)
																														COBJECT
																														(BgL_i1202z00_2586))->
																													BgL_thenz00)),
																											BUNSPEC);
																										{
																											obj_t BgL_auxz00_5149;

																											{	/* SawBbv/bbv.scm 287 */
																												obj_t
																													BgL_arg1996z00_2592;
																												BgL_arg1996z00_2592 =
																													(((BgL_blockz00_bglt)
																														COBJECT
																														(BgL_bz00_2534))->
																													BgL_succsz00);
																												BgL_auxz00_5149 =
																													bgl_reverse_bang
																													(BgL_arg1996z00_2592);
																											}
																											((((BgL_blockz00_bglt)
																														COBJECT
																														(BgL_bz00_2534))->
																													BgL_succsz00) =
																												((obj_t)
																													BgL_auxz00_5149),
																												BUNSPEC);
																		}}}}}}}
																	else
																		{	/* SawBbv/bbv.scm 289 */
																			obj_t BgL_arg2002z00_2601;

																			BgL_arg2002z00_2601 =
																				CDR(((obj_t) BgL_insz00_2541));
																			{
																				obj_t BgL_insz00_5155;

																				BgL_insz00_5155 = BgL_arg2002z00_2601;
																				BgL_insz00_2541 = BgL_insz00_5155;
																				goto
																					BgL_zc3z04anonymousza31958ze3z87_2542;
																			}
																		}
																}
														}
												}
										}
										{	/* SawBbv/bbv.scm 301 */
											BgL_blockz00_bglt BgL_i1210z00_2499;

											BgL_i1210z00_2499 =
												((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2491)));
											{	/* SawBbv/bbv.scm 302 */
												obj_t BgL_arg1927z00_2500;
												obj_t BgL_arg1928z00_2501;

												{	/* SawBbv/bbv.scm 302 */
													obj_t BgL_arg1929z00_2502;
													obj_t BgL_arg1930z00_2503;

													BgL_arg1929z00_2502 =
														(((BgL_blockz00_bglt) COBJECT(BgL_i1210z00_2499))->
														BgL_succsz00);
													BgL_arg1930z00_2503 = CDR(((obj_t) BgL_bsz00_2491));
													BgL_arg1927z00_2500 =
														BGl_appendzd221011zd2zzsaw_bbvz00
														(BgL_arg1929z00_2502, BgL_arg1930z00_2503);
												}
												{	/* SawBbv/bbv.scm 302 */
													obj_t BgL_arg1931z00_2504;

													BgL_arg1931z00_2504 = CAR(((obj_t) BgL_bsz00_2491));
													BgL_arg1928z00_2501 =
														MAKE_YOUNG_PAIR(BgL_arg1931z00_2504,
														BgL_accz00_2492);
												}
												{
													obj_t BgL_accz00_5169;
													obj_t BgL_bsz00_5168;

													BgL_bsz00_5168 = BgL_arg1927z00_2500;
													BgL_accz00_5169 = BgL_arg1928z00_2501;
													BgL_accz00_2492 = BgL_accz00_5169;
													BgL_bsz00_2491 = BgL_bsz00_5168;
													goto BgL_zc3z04anonymousza31920ze3z87_2493;
												}
											}
										}
									}
							}
					}
				}
			}
		}

	}



/* side-effect-free?~0 */
	bool_t BGl_sidezd2effectzd2freezf3ze70z14zzsaw_bbvz00(obj_t BgL_insz00_2508)
	{
		{	/* SawBbv/bbv.scm 251 */
			{	/* SawBbv/bbv.scm 248 */
				bool_t BgL_test2532z00_5170;

				{	/* SawBbv/bbv.scm 248 */
					bool_t BgL_test2533z00_5171;

					{	/* SawBbv/bbv.scm 248 */
						obj_t BgL_arg1952z00_2532;

						{	/* SawBbv/bbv.scm 248 */
							obj_t BgL_pairz00_3516;

							BgL_pairz00_3516 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_insz00_2508)))->BgL_argsz00);
							BgL_arg1952z00_2532 = CAR(BgL_pairz00_3516);
						}
						{	/* SawBbv/bbv.scm 248 */
							obj_t BgL_classz00_3517;

							BgL_classz00_3517 = BGl_rtl_insz00zzsaw_defsz00;
							if (BGL_OBJECTP(BgL_arg1952z00_2532))
								{	/* SawBbv/bbv.scm 248 */
									BgL_objectz00_bglt BgL_arg1807z00_3519;

									BgL_arg1807z00_3519 =
										(BgL_objectz00_bglt) (BgL_arg1952z00_2532);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawBbv/bbv.scm 248 */
											long BgL_idxz00_3525;

											BgL_idxz00_3525 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3519);
											BgL_test2533z00_5171 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_3525 + 1L)) == BgL_classz00_3517);
										}
									else
										{	/* SawBbv/bbv.scm 248 */
											bool_t BgL_res2326z00_3550;

											{	/* SawBbv/bbv.scm 248 */
												obj_t BgL_oclassz00_3533;

												{	/* SawBbv/bbv.scm 248 */
													obj_t BgL_arg1815z00_3541;
													long BgL_arg1816z00_3542;

													BgL_arg1815z00_3541 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawBbv/bbv.scm 248 */
														long BgL_arg1817z00_3543;

														BgL_arg1817z00_3543 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3519);
														BgL_arg1816z00_3542 =
															(BgL_arg1817z00_3543 - OBJECT_TYPE);
													}
													BgL_oclassz00_3533 =
														VECTOR_REF(BgL_arg1815z00_3541,
														BgL_arg1816z00_3542);
												}
												{	/* SawBbv/bbv.scm 248 */
													bool_t BgL__ortest_1115z00_3534;

													BgL__ortest_1115z00_3534 =
														(BgL_classz00_3517 == BgL_oclassz00_3533);
													if (BgL__ortest_1115z00_3534)
														{	/* SawBbv/bbv.scm 248 */
															BgL_res2326z00_3550 = BgL__ortest_1115z00_3534;
														}
													else
														{	/* SawBbv/bbv.scm 248 */
															long BgL_odepthz00_3535;

															{	/* SawBbv/bbv.scm 248 */
																obj_t BgL_arg1804z00_3536;

																BgL_arg1804z00_3536 = (BgL_oclassz00_3533);
																BgL_odepthz00_3535 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_3536);
															}
															if ((1L < BgL_odepthz00_3535))
																{	/* SawBbv/bbv.scm 248 */
																	obj_t BgL_arg1802z00_3538;

																	{	/* SawBbv/bbv.scm 248 */
																		obj_t BgL_arg1803z00_3539;

																		BgL_arg1803z00_3539 = (BgL_oclassz00_3533);
																		BgL_arg1802z00_3538 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_3539, 1L);
																	}
																	BgL_res2326z00_3550 =
																		(BgL_arg1802z00_3538 == BgL_classz00_3517);
																}
															else
																{	/* SawBbv/bbv.scm 248 */
																	BgL_res2326z00_3550 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2533z00_5171 = BgL_res2326z00_3550;
										}
								}
							else
								{	/* SawBbv/bbv.scm 248 */
									BgL_test2533z00_5171 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2533z00_5171)
						{	/* SawBbv/bbv.scm 248 */
							obj_t BgL_arg1950z00_2530;

							{	/* SawBbv/bbv.scm 248 */
								obj_t BgL_pairz00_3551;

								BgL_pairz00_3551 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_insz00_2508)))->BgL_argsz00);
								BgL_arg1950z00_2530 = CAR(BgL_pairz00_3551);
							}
							BgL_test2532z00_5170 =
								BGl_rtl_inszd2callzf3z21zzsaw_bbvzd2typeszd2(
								((BgL_rtl_insz00_bglt) BgL_arg1950z00_2530));
						}
					else
						{	/* SawBbv/bbv.scm 248 */
							BgL_test2532z00_5170 = ((bool_t) 0);
						}
				}
				if (BgL_test2532z00_5170)
					{	/* SawBbv/bbv.scm 249 */
						bool_t BgL_test2538z00_5202;

						{	/* SawBbv/bbv.scm 249 */
							obj_t BgL_arg1948z00_2527;

							{	/* SawBbv/bbv.scm 249 */
								obj_t BgL_pairz00_3552;

								BgL_pairz00_3552 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_insz00_2508)))->BgL_argsz00);
								BgL_arg1948z00_2527 = CAR(BgL_pairz00_3552);
							}
							BgL_test2538z00_5202 =
								CBOOL(BGl_rtl_callzd2predicatezd2zzsaw_bbvzd2typeszd2(
									((BgL_rtl_insz00_bglt) BgL_arg1948z00_2527)));
						}
						if (BgL_test2538z00_5202)
							{	/* SawBbv/bbv.scm 250 */
								obj_t BgL_argsz00_2522;

								BgL_argsz00_2522 =
									BGl_rtl_inszd2argsza2z70zzsaw_defsz00(
									((BgL_rtl_insz00_bglt) BgL_insz00_2508));
								if (NULLP(BgL_argsz00_2522))
									{	/* SawBbv/bbv.scm 251 */
										return ((bool_t) 0);
									}
								else
									{	/* SawBbv/bbv.scm 251 */
										if (NULLP(CDR(BgL_argsz00_2522)))
											{	/* SawBbv/bbv.scm 251 */
												obj_t BgL_arg1946z00_2525;

												BgL_arg1946z00_2525 = CAR(BgL_argsz00_2522);
												{	/* SawBbv/bbv.scm 251 */
													obj_t BgL_classz00_3555;

													BgL_classz00_3555 = BGl_rtl_regz00zzsaw_defsz00;
													if (BGL_OBJECTP(BgL_arg1946z00_2525))
														{	/* SawBbv/bbv.scm 251 */
															BgL_objectz00_bglt BgL_arg1807z00_3557;

															BgL_arg1807z00_3557 =
																(BgL_objectz00_bglt) (BgL_arg1946z00_2525);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* SawBbv/bbv.scm 251 */
																	long BgL_idxz00_3563;

																	BgL_idxz00_3563 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3557);
																	return (VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3563 + 1L)) ==
																		BgL_classz00_3555);
																}
															else
																{	/* SawBbv/bbv.scm 251 */
																	bool_t BgL_res2327z00_3588;

																	{	/* SawBbv/bbv.scm 251 */
																		obj_t BgL_oclassz00_3571;

																		{	/* SawBbv/bbv.scm 251 */
																			obj_t BgL_arg1815z00_3579;
																			long BgL_arg1816z00_3580;

																			BgL_arg1815z00_3579 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* SawBbv/bbv.scm 251 */
																				long BgL_arg1817z00_3581;

																				BgL_arg1817z00_3581 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3557);
																				BgL_arg1816z00_3580 =
																					(BgL_arg1817z00_3581 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3571 =
																				VECTOR_REF(BgL_arg1815z00_3579,
																				BgL_arg1816z00_3580);
																		}
																		{	/* SawBbv/bbv.scm 251 */
																			bool_t BgL__ortest_1115z00_3572;

																			BgL__ortest_1115z00_3572 =
																				(BgL_classz00_3555 ==
																				BgL_oclassz00_3571);
																			if (BgL__ortest_1115z00_3572)
																				{	/* SawBbv/bbv.scm 251 */
																					BgL_res2327z00_3588 =
																						BgL__ortest_1115z00_3572;
																				}
																			else
																				{	/* SawBbv/bbv.scm 251 */
																					long BgL_odepthz00_3573;

																					{	/* SawBbv/bbv.scm 251 */
																						obj_t BgL_arg1804z00_3574;

																						BgL_arg1804z00_3574 =
																							(BgL_oclassz00_3571);
																						BgL_odepthz00_3573 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3574);
																					}
																					if ((1L < BgL_odepthz00_3573))
																						{	/* SawBbv/bbv.scm 251 */
																							obj_t BgL_arg1802z00_3576;

																							{	/* SawBbv/bbv.scm 251 */
																								obj_t BgL_arg1803z00_3577;

																								BgL_arg1803z00_3577 =
																									(BgL_oclassz00_3571);
																								BgL_arg1802z00_3576 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3577, 1L);
																							}
																							BgL_res2327z00_3588 =
																								(BgL_arg1802z00_3576 ==
																								BgL_classz00_3555);
																						}
																					else
																						{	/* SawBbv/bbv.scm 251 */
																							BgL_res2327z00_3588 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	return BgL_res2327z00_3588;
																}
														}
													else
														{	/* SawBbv/bbv.scm 251 */
															return ((bool_t) 0);
														}
												}
											}
										else
											{	/* SawBbv/bbv.scm 251 */
												return ((bool_t) 0);
											}
									}
							}
						else
							{	/* SawBbv/bbv.scm 249 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* SawBbv/bbv.scm 248 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* normalize-mov! */
	obj_t BGl_normaliza7ezd2movz12z67zzsaw_bbvz00(BgL_blockz00_bglt BgL_bz00_106)
	{
		{	/* SawBbv/bbv.scm 312 */
			{
				BgL_rtl_insz00_bglt BgL_iz00_2648;
				BgL_blockz00_bglt BgL_bz00_2667;

				{	/* SawBbv/bbv.scm 342 */
					obj_t BgL_g1217z00_2611;

					{	/* SawBbv/bbv.scm 342 */
						obj_t BgL_list2019z00_2630;

						BgL_list2019z00_2630 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_106), BNIL);
						BgL_g1217z00_2611 = BgL_list2019z00_2630;
					}
					{
						obj_t BgL_bsz00_2614;
						obj_t BgL_accz00_2615;

						BgL_bsz00_2614 = BgL_g1217z00_2611;
						BgL_accz00_2615 = BNIL;
					BgL_zc3z04anonymousza32007ze3z87_2616:
						if (NULLP(BgL_bsz00_2614))
							{	/* SawBbv/bbv.scm 345 */
								return bgl_reverse(BgL_accz00_2615);
							}
						else
							{	/* SawBbv/bbv.scm 345 */
								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
												((obj_t) BgL_bsz00_2614)), BgL_accz00_2615)))
									{	/* SawBbv/bbv.scm 348 */
										obj_t BgL_arg2011z00_2620;

										BgL_arg2011z00_2620 = CDR(((obj_t) BgL_bsz00_2614));
										{
											obj_t BgL_bsz00_5251;

											BgL_bsz00_5251 = BgL_arg2011z00_2620;
											BgL_bsz00_2614 = BgL_bsz00_5251;
											goto BgL_zc3z04anonymousza32007ze3z87_2616;
										}
									}
								else
									{	/* SawBbv/bbv.scm 347 */
										{	/* SawBbv/bbv.scm 350 */
											obj_t BgL_arg2012z00_2621;

											BgL_arg2012z00_2621 = CAR(((obj_t) BgL_bsz00_2614));
											BgL_bz00_2667 = ((BgL_blockz00_bglt) BgL_arg2012z00_2621);
											{
												obj_t BgL_auxz00_5254;

												{	/* SawBbv/bbv.scm 339 */
													obj_t BgL_l01509z00_2670;

													BgL_l01509z00_2670 =
														(((BgL_blockz00_bglt) COBJECT(BgL_bz00_2667))->
														BgL_firstz00);
													{
														obj_t BgL_l1508z00_2672;

														BgL_l1508z00_2672 = BgL_l01509z00_2670;
													BgL_zc3z04anonymousza32044ze3z87_2673:
														if (NULLP(BgL_l1508z00_2672))
															{	/* SawBbv/bbv.scm 339 */
																BgL_auxz00_5254 = BgL_l01509z00_2670;
															}
														else
															{	/* SawBbv/bbv.scm 339 */
																{	/* SawBbv/bbv.scm 339 */
																	BgL_rtl_insz00_bglt BgL_arg2046z00_2675;

																	{	/* SawBbv/bbv.scm 339 */
																		obj_t BgL_arg2047z00_2676;

																		BgL_arg2047z00_2676 =
																			CAR(((obj_t) BgL_l1508z00_2672));
																		BgL_iz00_2648 =
																			((BgL_rtl_insz00_bglt)
																			BgL_arg2047z00_2676);
																		if (BGl_rtl_inszd2movzf3z21zzsaw_bbvzd2typeszd2(BgL_iz00_2648))
																			{	/* SawBbv/bbv.scm 327 */
																				obj_t BgL_niz00_2652;

																				BgL_niz00_2652 =
																					BGl_removezd2innerzd2movz12ze70zf5zzsaw_bbvz00
																					(CAR((((BgL_rtl_insz00_bglt)
																								COBJECT(BgL_iz00_2648))->
																							BgL_argsz00)));
																				{	/* SawBbv/bbv.scm 328 */
																					bool_t BgL_test2549z00_5265;

																					{	/* SawBbv/bbv.scm 328 */
																						obj_t BgL_classz00_3662;

																						BgL_classz00_3662 =
																							BGl_rtl_insz00zzsaw_defsz00;
																						if (BGL_OBJECTP(BgL_niz00_2652))
																							{	/* SawBbv/bbv.scm 328 */
																								BgL_objectz00_bglt
																									BgL_arg1807z00_3664;
																								BgL_arg1807z00_3664 =
																									(BgL_objectz00_bglt)
																									(BgL_niz00_2652);
																								if (BGL_CONDEXPAND_ISA_ARCH64())
																									{	/* SawBbv/bbv.scm 328 */
																										long BgL_idxz00_3670;

																										BgL_idxz00_3670 =
																											BGL_OBJECT_INHERITANCE_NUM
																											(BgL_arg1807z00_3664);
																										BgL_test2549z00_5265 =
																											(VECTOR_REF
																											(BGl_za2inheritancesza2z00zz__objectz00,
																												(BgL_idxz00_3670 +
																													1L)) ==
																											BgL_classz00_3662);
																									}
																								else
																									{	/* SawBbv/bbv.scm 328 */
																										bool_t BgL_res2330z00_3695;

																										{	/* SawBbv/bbv.scm 328 */
																											obj_t BgL_oclassz00_3678;

																											{	/* SawBbv/bbv.scm 328 */
																												obj_t
																													BgL_arg1815z00_3686;
																												long
																													BgL_arg1816z00_3687;
																												BgL_arg1815z00_3686 =
																													(BGl_za2classesza2z00zz__objectz00);
																												{	/* SawBbv/bbv.scm 328 */
																													long
																														BgL_arg1817z00_3688;
																													BgL_arg1817z00_3688 =
																														BGL_OBJECT_CLASS_NUM
																														(BgL_arg1807z00_3664);
																													BgL_arg1816z00_3687 =
																														(BgL_arg1817z00_3688
																														- OBJECT_TYPE);
																												}
																												BgL_oclassz00_3678 =
																													VECTOR_REF
																													(BgL_arg1815z00_3686,
																													BgL_arg1816z00_3687);
																											}
																											{	/* SawBbv/bbv.scm 328 */
																												bool_t
																													BgL__ortest_1115z00_3679;
																												BgL__ortest_1115z00_3679
																													=
																													(BgL_classz00_3662 ==
																													BgL_oclassz00_3678);
																												if (BgL__ortest_1115z00_3679)
																													{	/* SawBbv/bbv.scm 328 */
																														BgL_res2330z00_3695
																															=
																															BgL__ortest_1115z00_3679;
																													}
																												else
																													{	/* SawBbv/bbv.scm 328 */
																														long
																															BgL_odepthz00_3680;
																														{	/* SawBbv/bbv.scm 328 */
																															obj_t
																																BgL_arg1804z00_3681;
																															BgL_arg1804z00_3681
																																=
																																(BgL_oclassz00_3678);
																															BgL_odepthz00_3680
																																=
																																BGL_CLASS_DEPTH
																																(BgL_arg1804z00_3681);
																														}
																														if (
																															(1L <
																																BgL_odepthz00_3680))
																															{	/* SawBbv/bbv.scm 328 */
																																obj_t
																																	BgL_arg1802z00_3683;
																																{	/* SawBbv/bbv.scm 328 */
																																	obj_t
																																		BgL_arg1803z00_3684;
																																	BgL_arg1803z00_3684
																																		=
																																		(BgL_oclassz00_3678);
																																	BgL_arg1802z00_3683
																																		=
																																		BGL_CLASS_ANCESTORS_REF
																																		(BgL_arg1803z00_3684,
																																		1L);
																																}
																																BgL_res2330z00_3695
																																	=
																																	(BgL_arg1802z00_3683
																																	==
																																	BgL_classz00_3662);
																															}
																														else
																															{	/* SawBbv/bbv.scm 328 */
																																BgL_res2330z00_3695
																																	=
																																	((bool_t) 0);
																															}
																													}
																											}
																										}
																										BgL_test2549z00_5265 =
																											BgL_res2330z00_3695;
																									}
																							}
																						else
																							{	/* SawBbv/bbv.scm 328 */
																								BgL_test2549z00_5265 =
																									((bool_t) 0);
																							}
																					}
																					if (BgL_test2549z00_5265)
																						{	/* SawBbv/bbv.scm 328 */
																							((((BgL_rtl_insz00_bglt)
																										COBJECT(BgL_iz00_2648))->
																									BgL_funz00) =
																								((BgL_rtl_funz00_bglt) ((
																											(BgL_rtl_insz00_bglt)
																											COBJECT((
																													(BgL_rtl_insz00_bglt)
																													BgL_niz00_2652)))->
																										BgL_funz00)), BUNSPEC);
																							((((BgL_rtl_insz00_bglt)
																										COBJECT(BgL_iz00_2648))->
																									BgL_argsz00) =
																								((obj_t) (((BgL_rtl_insz00_bglt)
																											COBJECT((
																													(BgL_rtl_insz00_bglt)
																													BgL_niz00_2652)))->
																										BgL_argsz00)), BUNSPEC);
																						}
																					else
																						{	/* SawBbv/bbv.scm 328 */
																							BFALSE;
																						}
																				}
																				BgL_arg2046z00_2675 = BgL_iz00_2648;
																			}
																		else
																			{	/* SawBbv/bbv.scm 325 */
																				{
																					obj_t BgL_auxz00_5294;

																					{	/* SawBbv/bbv.scm 334 */
																						obj_t BgL_l01506z00_2658;

																						BgL_l01506z00_2658 =
																							(((BgL_rtl_insz00_bglt)
																								COBJECT(BgL_iz00_2648))->
																							BgL_argsz00);
																						{
																							obj_t BgL_l1505z00_2660;

																							BgL_l1505z00_2660 =
																								BgL_l01506z00_2658;
																						BgL_zc3z04anonymousza32038ze3z87_2661:
																							if (NULLP
																								(BgL_l1505z00_2660))
																								{	/* SawBbv/bbv.scm 334 */
																									BgL_auxz00_5294 =
																										BgL_l01506z00_2658;
																								}
																							else
																								{	/* SawBbv/bbv.scm 334 */
																									{	/* SawBbv/bbv.scm 334 */
																										obj_t BgL_arg2040z00_2663;

																										{	/* SawBbv/bbv.scm 334 */
																											obj_t BgL_arg2041z00_2664;

																											BgL_arg2041z00_2664 =
																												CAR(
																												((obj_t)
																													BgL_l1505z00_2660));
																											BgL_arg2040z00_2663 =
																												BGl_removezd2innerzd2movz12ze70zf5zzsaw_bbvz00
																												(BgL_arg2041z00_2664);
																										}
																										{	/* SawBbv/bbv.scm 334 */
																											obj_t BgL_tmpz00_5301;

																											BgL_tmpz00_5301 =
																												((obj_t)
																												BgL_l1505z00_2660);
																											SET_CAR(BgL_tmpz00_5301,
																												BgL_arg2040z00_2663);
																										}
																									}
																									{	/* SawBbv/bbv.scm 334 */
																										obj_t BgL_arg2042z00_2665;

																										BgL_arg2042z00_2665 =
																											CDR(
																											((obj_t)
																												BgL_l1505z00_2660));
																										{
																											obj_t BgL_l1505z00_5306;

																											BgL_l1505z00_5306 =
																												BgL_arg2042z00_2665;
																											BgL_l1505z00_2660 =
																												BgL_l1505z00_5306;
																											goto
																												BgL_zc3z04anonymousza32038ze3z87_2661;
																										}
																									}
																								}
																						}
																					}
																					((((BgL_rtl_insz00_bglt)
																								COBJECT(BgL_iz00_2648))->
																							BgL_argsz00) =
																						((obj_t) BgL_auxz00_5294), BUNSPEC);
																				}
																				BgL_arg2046z00_2675 = BgL_iz00_2648;
																			}
																	}
																	{	/* SawBbv/bbv.scm 339 */
																		obj_t BgL_auxz00_5311;
																		obj_t BgL_tmpz00_5309;

																		BgL_auxz00_5311 =
																			((obj_t) BgL_arg2046z00_2675);
																		BgL_tmpz00_5309 =
																			((obj_t) BgL_l1508z00_2672);
																		SET_CAR(BgL_tmpz00_5309, BgL_auxz00_5311);
																	}
																}
																{	/* SawBbv/bbv.scm 339 */
																	obj_t BgL_arg2048z00_2677;

																	BgL_arg2048z00_2677 =
																		CDR(((obj_t) BgL_l1508z00_2672));
																	{
																		obj_t BgL_l1508z00_5316;

																		BgL_l1508z00_5316 = BgL_arg2048z00_2677;
																		BgL_l1508z00_2672 = BgL_l1508z00_5316;
																		goto BgL_zc3z04anonymousza32044ze3z87_2673;
																	}
																}
															}
													}
												}
												((((BgL_blockz00_bglt) COBJECT(BgL_bz00_2667))->
														BgL_firstz00) = ((obj_t) BgL_auxz00_5254), BUNSPEC);
											}
										}
										{	/* SawBbv/bbv.scm 351 */
											BgL_blockz00_bglt BgL_i1219z00_2622;

											BgL_i1219z00_2622 =
												((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2614)));
											{	/* SawBbv/bbv.scm 352 */
												obj_t BgL_arg2013z00_2623;
												obj_t BgL_arg2014z00_2624;

												{	/* SawBbv/bbv.scm 352 */
													obj_t BgL_arg2015z00_2625;
													obj_t BgL_arg2016z00_2626;

													BgL_arg2015z00_2625 =
														(((BgL_blockz00_bglt) COBJECT(BgL_i1219z00_2622))->
														BgL_succsz00);
													BgL_arg2016z00_2626 = CDR(((obj_t) BgL_bsz00_2614));
													BgL_arg2013z00_2623 =
														BGl_appendzd221011zd2zzsaw_bbvz00
														(BgL_arg2015z00_2625, BgL_arg2016z00_2626);
												}
												{	/* SawBbv/bbv.scm 352 */
													obj_t BgL_arg2017z00_2627;

													BgL_arg2017z00_2627 = CAR(((obj_t) BgL_bsz00_2614));
													BgL_arg2014z00_2624 =
														MAKE_YOUNG_PAIR(BgL_arg2017z00_2627,
														BgL_accz00_2615);
												}
												{
													obj_t BgL_accz00_5330;
													obj_t BgL_bsz00_5329;

													BgL_bsz00_5329 = BgL_arg2013z00_2623;
													BgL_accz00_5330 = BgL_arg2014z00_2624;
													BgL_accz00_2615 = BgL_accz00_5330;
													BgL_bsz00_2614 = BgL_bsz00_5329;
													goto BgL_zc3z04anonymousza32007ze3z87_2616;
												}
											}
										}
									}
							}
					}
				}
			}
		}

	}



/* remove-inner-mov!~0 */
	obj_t BGl_removezd2innerzd2movz12ze70zf5zzsaw_bbvz00(obj_t BgL_oz00_2631)
	{
		{	/* SawBbv/bbv.scm 322 */
		BGl_removezd2innerzd2movz12ze70zf5zzsaw_bbvz00:
			{	/* SawBbv/bbv.scm 315 */
				bool_t BgL_test2555z00_5331;

				{	/* SawBbv/bbv.scm 315 */
					obj_t BgL_classz00_3623;

					BgL_classz00_3623 = BGl_rtl_insz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_oz00_2631))
						{	/* SawBbv/bbv.scm 315 */
							BgL_objectz00_bglt BgL_arg1807z00_3625;

							BgL_arg1807z00_3625 = (BgL_objectz00_bglt) (BgL_oz00_2631);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv.scm 315 */
									long BgL_idxz00_3631;

									BgL_idxz00_3631 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3625);
									BgL_test2555z00_5331 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3631 + 1L)) == BgL_classz00_3623);
								}
							else
								{	/* SawBbv/bbv.scm 315 */
									bool_t BgL_res2329z00_3656;

									{	/* SawBbv/bbv.scm 315 */
										obj_t BgL_oclassz00_3639;

										{	/* SawBbv/bbv.scm 315 */
											obj_t BgL_arg1815z00_3647;
											long BgL_arg1816z00_3648;

											BgL_arg1815z00_3647 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv.scm 315 */
												long BgL_arg1817z00_3649;

												BgL_arg1817z00_3649 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3625);
												BgL_arg1816z00_3648 =
													(BgL_arg1817z00_3649 - OBJECT_TYPE);
											}
											BgL_oclassz00_3639 =
												VECTOR_REF(BgL_arg1815z00_3647, BgL_arg1816z00_3648);
										}
										{	/* SawBbv/bbv.scm 315 */
											bool_t BgL__ortest_1115z00_3640;

											BgL__ortest_1115z00_3640 =
												(BgL_classz00_3623 == BgL_oclassz00_3639);
											if (BgL__ortest_1115z00_3640)
												{	/* SawBbv/bbv.scm 315 */
													BgL_res2329z00_3656 = BgL__ortest_1115z00_3640;
												}
											else
												{	/* SawBbv/bbv.scm 315 */
													long BgL_odepthz00_3641;

													{	/* SawBbv/bbv.scm 315 */
														obj_t BgL_arg1804z00_3642;

														BgL_arg1804z00_3642 = (BgL_oclassz00_3639);
														BgL_odepthz00_3641 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3642);
													}
													if ((1L < BgL_odepthz00_3641))
														{	/* SawBbv/bbv.scm 315 */
															obj_t BgL_arg1802z00_3644;

															{	/* SawBbv/bbv.scm 315 */
																obj_t BgL_arg1803z00_3645;

																BgL_arg1803z00_3645 = (BgL_oclassz00_3639);
																BgL_arg1802z00_3644 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3645,
																	1L);
															}
															BgL_res2329z00_3656 =
																(BgL_arg1802z00_3644 == BgL_classz00_3623);
														}
													else
														{	/* SawBbv/bbv.scm 315 */
															BgL_res2329z00_3656 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2555z00_5331 = BgL_res2329z00_3656;
								}
						}
					else
						{	/* SawBbv/bbv.scm 315 */
							BgL_test2555z00_5331 = ((bool_t) 0);
						}
				}
				if (BgL_test2555z00_5331)
					{	/* SawBbv/bbv.scm 315 */
						if (BGl_rtl_inszd2movzf3z21zzsaw_bbvzd2typeszd2(
								((BgL_rtl_insz00_bglt) BgL_oz00_2631)))
							{	/* SawBbv/bbv.scm 318 */
								obj_t BgL_arg2024z00_2636;

								{	/* SawBbv/bbv.scm 318 */
									obj_t BgL_pairz00_3657;

									BgL_pairz00_3657 =
										(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_oz00_2631)))->BgL_argsz00);
									BgL_arg2024z00_2636 = CAR(BgL_pairz00_3657);
								}
								{
									obj_t BgL_oz00_5360;

									BgL_oz00_5360 = BgL_arg2024z00_2636;
									BgL_oz00_2631 = BgL_oz00_5360;
									goto BGl_removezd2innerzd2movz12ze70zf5zzsaw_bbvz00;
								}
							}
						else
							{	/* SawBbv/bbv.scm 316 */
								{
									obj_t BgL_auxz00_5361;

									{	/* SawBbv/bbv.scm 320 */
										obj_t BgL_l01503z00_2639;

										BgL_l01503z00_2639 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_oz00_2631)))->BgL_argsz00);
										{
											obj_t BgL_l1502z00_2641;

											BgL_l1502z00_2641 = BgL_l01503z00_2639;
										BgL_zc3z04anonymousza32026ze3z87_2642:
											if (NULLP(BgL_l1502z00_2641))
												{	/* SawBbv/bbv.scm 320 */
													BgL_auxz00_5361 = BgL_l01503z00_2639;
												}
											else
												{	/* SawBbv/bbv.scm 320 */
													{	/* SawBbv/bbv.scm 320 */
														obj_t BgL_arg2029z00_2644;

														{	/* SawBbv/bbv.scm 320 */
															obj_t BgL_arg2030z00_2645;

															BgL_arg2030z00_2645 =
																CAR(((obj_t) BgL_l1502z00_2641));
															BgL_arg2029z00_2644 =
																BGl_removezd2innerzd2movz12ze70zf5zzsaw_bbvz00
																(BgL_arg2030z00_2645);
														}
														{	/* SawBbv/bbv.scm 320 */
															obj_t BgL_tmpz00_5370;

															BgL_tmpz00_5370 = ((obj_t) BgL_l1502z00_2641);
															SET_CAR(BgL_tmpz00_5370, BgL_arg2029z00_2644);
														}
													}
													{	/* SawBbv/bbv.scm 320 */
														obj_t BgL_arg2031z00_2646;

														BgL_arg2031z00_2646 =
															CDR(((obj_t) BgL_l1502z00_2641));
														{
															obj_t BgL_l1502z00_5375;

															BgL_l1502z00_5375 = BgL_arg2031z00_2646;
															BgL_l1502z00_2641 = BgL_l1502z00_5375;
															goto BgL_zc3z04anonymousza32026ze3z87_2642;
														}
													}
												}
										}
									}
									((((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_oz00_2631)))->
											BgL_argsz00) = ((obj_t) BgL_auxz00_5361), BUNSPEC);
								}
								return BgL_oz00_2631;
							}
					}
				else
					{	/* SawBbv/bbv.scm 315 */
						return BgL_oz00_2631;
					}
			}
		}

	}



/* fail! */
	obj_t BGl_failz12z12zzsaw_bbvz00(BgL_blockz00_bglt BgL_bz00_107)
	{
		{	/* SawBbv/bbv.scm 360 */
			{
				BgL_blockz00_bglt BgL_bz00_2703;

				{	/* SawBbv/bbv.scm 378 */
					obj_t BgL_g1225z00_2683;

					{	/* SawBbv/bbv.scm 378 */
						obj_t BgL_list2063z00_2702;

						BgL_list2063z00_2702 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_107), BNIL);
						BgL_g1225z00_2683 = BgL_list2063z00_2702;
					}
					{
						obj_t BgL_bsz00_2686;
						obj_t BgL_accz00_2687;

						BgL_bsz00_2686 = BgL_g1225z00_2683;
						BgL_accz00_2687 = BNIL;
					BgL_zc3z04anonymousza32049ze3z87_2688:
						if (NULLP(BgL_bsz00_2686))
							{	/* SawBbv/bbv.scm 381 */
								return bgl_reverse(BgL_accz00_2687);
							}
						else
							{	/* SawBbv/bbv.scm 381 */
								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
												((obj_t) BgL_bsz00_2686)), BgL_accz00_2687)))
									{	/* SawBbv/bbv.scm 384 */
										obj_t BgL_arg2055z00_2692;

										BgL_arg2055z00_2692 = CDR(((obj_t) BgL_bsz00_2686));
										{
											obj_t BgL_bsz00_5389;

											BgL_bsz00_5389 = BgL_arg2055z00_2692;
											BgL_bsz00_2686 = BgL_bsz00_5389;
											goto BgL_zc3z04anonymousza32049ze3z87_2688;
										}
									}
								else
									{	/* SawBbv/bbv.scm 383 */
										{	/* SawBbv/bbv.scm 386 */
											obj_t BgL_arg2056z00_2693;

											BgL_arg2056z00_2693 = CAR(((obj_t) BgL_bsz00_2686));
											BgL_bz00_2703 = ((BgL_blockz00_bglt) BgL_arg2056z00_2693);
											{	/* SawBbv/bbv.scm 364 */
												bool_t BgL_test2564z00_5392;

												if (NULLP(
														(((BgL_blockz00_bglt) COBJECT(BgL_bz00_2703))->
															BgL_succsz00)))
													{	/* SawBbv/bbv.scm 364 */
														BgL_test2564z00_5392 = ((bool_t) 0);
													}
												else
													{	/* SawBbv/bbv.scm 364 */
														obj_t BgL_arg2084z00_2731;

														BgL_arg2084z00_2731 =
															CAR(
															(((BgL_blockz00_bglt) COBJECT(BgL_bz00_2703))->
																BgL_firstz00));
														BgL_test2564z00_5392 =
															BGl_rtl_inszd2errorzf3z21zzsaw_bbvzd2typeszd2((
																(BgL_rtl_insz00_bglt) BgL_arg2084z00_2731));
													}
												if (BgL_test2564z00_5392)
													{	/* SawBbv/bbv.scm 365 */
														obj_t BgL_lpz00_2712;

														BgL_lpz00_2712 =
															BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
															(((BgL_blockz00_bglt) COBJECT(BgL_bz00_2703))->
																BgL_firstz00));
														{	/* SawBbv/bbv.scm 365 */
															obj_t BgL_lastz00_2713;

															BgL_lastz00_2713 = CAR(BgL_lpz00_2712);
															{	/* SawBbv/bbv.scm 366 */

																{	/* SawBbv/bbv.scm 367 */
																	BgL_rtl_insz00_bglt BgL_failz00_2714;

																	{	/* SawBbv/bbv.scm 367 */
																		BgL_rtl_insz00_bglt BgL_new1222z00_2723;

																		{	/* SawBbv/bbv.scm 367 */
																			BgL_rtl_insz00_bglt BgL_new1221z00_2727;

																			BgL_new1221z00_2727 =
																				((BgL_rtl_insz00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_rtl_insz00_bgl))));
																			{	/* SawBbv/bbv.scm 367 */
																				long BgL_arg2082z00_2728;

																				BgL_arg2082z00_2728 =
																					BGL_CLASS_NUM
																					(BGl_rtl_insz00zzsaw_defsz00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1221z00_2727),
																					BgL_arg2082z00_2728);
																			}
																			{	/* SawBbv/bbv.scm 367 */
																				BgL_objectz00_bglt BgL_tmpz00_5407;

																				BgL_tmpz00_5407 =
																					((BgL_objectz00_bglt)
																					BgL_new1221z00_2727);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5407,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1221z00_2727);
																			BgL_new1222z00_2723 = BgL_new1221z00_2727;
																		}
																		((((BgL_rtl_insz00_bglt)
																					COBJECT(BgL_new1222z00_2723))->
																				BgL_locz00) =
																			((obj_t) BFALSE), BUNSPEC);
																		((((BgL_rtl_insz00_bglt)
																					COBJECT(BgL_new1222z00_2723))->
																				BgL_z52spillz52) =
																			((obj_t) BNIL), BUNSPEC);
																		((((BgL_rtl_insz00_bglt)
																					COBJECT(BgL_new1222z00_2723))->
																				BgL_destz00) =
																			((obj_t) BFALSE), BUNSPEC);
																		{
																			BgL_rtl_funz00_bglt BgL_auxz00_5414;

																			{	/* SawBbv/bbv.scm 369 */
																				BgL_rtl_pragmaz00_bglt
																					BgL_new1224z00_2724;
																				{	/* SawBbv/bbv.scm 371 */
																					BgL_rtl_pragmaz00_bglt
																						BgL_new1223z00_2725;
																					BgL_new1223z00_2725 =
																						((BgL_rtl_pragmaz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_rtl_pragmaz00_bgl))));
																					{	/* SawBbv/bbv.scm 371 */
																						long BgL_arg2081z00_2726;

																						{	/* SawBbv/bbv.scm 371 */
																							obj_t BgL_classz00_3715;

																							BgL_classz00_3715 =
																								BGl_rtl_pragmaz00zzsaw_defsz00;
																							BgL_arg2081z00_2726 =
																								BGL_CLASS_NUM
																								(BgL_classz00_3715);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1223z00_2725),
																							BgL_arg2081z00_2726);
																					}
																					BgL_new1224z00_2724 =
																						BgL_new1223z00_2725;
																				}
																				((((BgL_rtl_funz00_bglt) COBJECT(
																								((BgL_rtl_funz00_bglt)
																									BgL_new1224z00_2724)))->
																						BgL_locz00) =
																					((obj_t) (((BgL_rtl_insz00_bglt)
																								COBJECT(((BgL_rtl_insz00_bglt)
																										BgL_lastz00_2713)))->
																							BgL_locz00)), BUNSPEC);
																				((((BgL_rtl_pragmaz00_bglt)
																							COBJECT(BgL_new1224z00_2724))->
																						BgL_formatz00) =
																					((obj_t)
																						BGl_string2378z00zzsaw_bbvz00),
																					BUNSPEC);
																				((((BgL_rtl_pragmaz00_bglt)
																							COBJECT(BgL_new1224z00_2724))->
																						BgL_srfi0z00) =
																					((obj_t) CNST_TABLE_REF(1)), BUNSPEC);
																				BgL_auxz00_5414 =
																					((BgL_rtl_funz00_bglt)
																					BgL_new1224z00_2724);
																			}
																			((((BgL_rtl_insz00_bglt)
																						COBJECT(BgL_new1222z00_2723))->
																					BgL_funz00) =
																				((BgL_rtl_funz00_bglt) BgL_auxz00_5414),
																				BUNSPEC);
																		}
																		((((BgL_rtl_insz00_bglt)
																					COBJECT(BgL_new1222z00_2723))->
																				BgL_argsz00) = ((obj_t) BNIL), BUNSPEC);
																		BgL_failz00_2714 = BgL_new1222z00_2723;
																	}
																	if (NULLP(CDR(
																				(((BgL_blockz00_bglt)
																						COBJECT(BgL_bz00_2703))->
																					BgL_succsz00))))
																		{	/* SawBbv/bbv.scm 374 */
																			((((BgL_blockz00_bglt)
																						COBJECT(BgL_bz00_2703))->
																					BgL_succsz00) =
																				((obj_t) BNIL), BUNSPEC);
																		}
																	else
																		{	/* SawBbv/bbv.scm 374 */
																			BFALSE;
																		}
																	{	/* SawBbv/bbv.scm 376 */
																		obj_t BgL_arg2078z00_2720;
																		obj_t BgL_arg2079z00_2721;

																		BgL_arg2078z00_2720 =
																			(((BgL_blockz00_bglt)
																				COBJECT(BgL_bz00_2703))->BgL_firstz00);
																		{	/* SawBbv/bbv.scm 376 */
																			obj_t BgL_list2080z00_2722;

																			BgL_list2080z00_2722 =
																				MAKE_YOUNG_PAIR(
																				((obj_t) BgL_failz00_2714), BNIL);
																			BgL_arg2079z00_2721 =
																				BgL_list2080z00_2722;
																		}
																		{	/* SawBbv/bbv.scm 376 */
																			obj_t BgL_tmpz00_5437;

																			BgL_tmpz00_5437 =
																				((obj_t) BgL_arg2078z00_2720);
																			SET_CDR(BgL_tmpz00_5437,
																				BgL_arg2079z00_2721);
																		}
																	}
																}
															}
														}
													}
												else
													{	/* SawBbv/bbv.scm 364 */
														BFALSE;
													}
											}
										}
										{	/* SawBbv/bbv.scm 387 */
											BgL_blockz00_bglt BgL_i1228z00_2694;

											BgL_i1228z00_2694 =
												((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2686)));
											{	/* SawBbv/bbv.scm 388 */
												obj_t BgL_arg2057z00_2695;
												obj_t BgL_arg2058z00_2696;

												{	/* SawBbv/bbv.scm 388 */
													obj_t BgL_arg2059z00_2697;
													obj_t BgL_arg2060z00_2698;

													BgL_arg2059z00_2697 =
														(((BgL_blockz00_bglt) COBJECT(BgL_i1228z00_2694))->
														BgL_succsz00);
													BgL_arg2060z00_2698 = CDR(((obj_t) BgL_bsz00_2686));
													BgL_arg2057z00_2695 =
														BGl_appendzd221011zd2zzsaw_bbvz00
														(BgL_arg2059z00_2697, BgL_arg2060z00_2698);
												}
												{	/* SawBbv/bbv.scm 388 */
													obj_t BgL_arg2061z00_2699;

													BgL_arg2061z00_2699 = CAR(((obj_t) BgL_bsz00_2686));
													BgL_arg2058z00_2696 =
														MAKE_YOUNG_PAIR(BgL_arg2061z00_2699,
														BgL_accz00_2687);
												}
												{
													obj_t BgL_accz00_5452;
													obj_t BgL_bsz00_5451;

													BgL_bsz00_5451 = BgL_arg2057z00_2695;
													BgL_accz00_5452 = BgL_arg2058z00_2696;
													BgL_accz00_2687 = BgL_accz00_5452;
													BgL_bsz00_2686 = BgL_bsz00_5451;
													goto BgL_zc3z04anonymousza32049ze3z87_2688;
												}
											}
										}
									}
							}
					}
				}
			}
		}

	}



/* normalize-typecheck! */
	obj_t BGl_normaliza7ezd2typecheckz12z67zzsaw_bbvz00(BgL_blockz00_bglt
		BgL_bz00_108)
	{
		{	/* SawBbv/bbv.scm 396 */
			{
				BgL_blockz00_bglt BgL_bz00_2805;
				BgL_rtl_insz00_bglt BgL_iz00_2799;
				BgL_rtl_insz00_bglt BgL_iz00_2758;

				{	/* SawBbv/bbv.scm 429 */
					obj_t BgL_g1235z00_2738;

					{	/* SawBbv/bbv.scm 429 */
						obj_t BgL_list2101z00_2757;

						BgL_list2101z00_2757 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_108), BNIL);
						BgL_g1235z00_2738 = BgL_list2101z00_2757;
					}
					{
						obj_t BgL_bsz00_2741;
						obj_t BgL_accz00_2742;

						BgL_bsz00_2741 = BgL_g1235z00_2738;
						BgL_accz00_2742 = BNIL;
					BgL_zc3z04anonymousza32088ze3z87_2743:
						if (NULLP(BgL_bsz00_2741))
							{	/* SawBbv/bbv.scm 432 */
								return bgl_reverse(BgL_accz00_2742);
							}
						else
							{	/* SawBbv/bbv.scm 432 */
								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
												((obj_t) BgL_bsz00_2741)), BgL_accz00_2742)))
									{	/* SawBbv/bbv.scm 435 */
										obj_t BgL_arg2093z00_2747;

										BgL_arg2093z00_2747 = CDR(((obj_t) BgL_bsz00_2741));
										{
											obj_t BgL_bsz00_5465;

											BgL_bsz00_5465 = BgL_arg2093z00_2747;
											BgL_bsz00_2741 = BgL_bsz00_5465;
											goto BgL_zc3z04anonymousza32088ze3z87_2743;
										}
									}
								else
									{	/* SawBbv/bbv.scm 434 */
										{	/* SawBbv/bbv.scm 437 */
											obj_t BgL_arg2094z00_2748;

											BgL_arg2094z00_2748 = CAR(((obj_t) BgL_bsz00_2741));
											BgL_bz00_2805 = ((BgL_blockz00_bglt) BgL_arg2094z00_2748);
											{	/* SawBbv/bbv.scm 427 */
												obj_t BgL_g1512z00_2808;

												BgL_g1512z00_2808 =
													(((BgL_blockz00_bglt) COBJECT(BgL_bz00_2805))->
													BgL_firstz00);
												{
													obj_t BgL_l1510z00_2810;

													BgL_l1510z00_2810 = BgL_g1512z00_2808;
												BgL_zc3z04anonymousza32136ze3z87_2811:
													if (PAIRP(BgL_l1510z00_2810))
														{	/* SawBbv/bbv.scm 427 */
															{	/* SawBbv/bbv.scm 427 */
																obj_t BgL_arg2138z00_2813;

																BgL_arg2138z00_2813 = CAR(BgL_l1510z00_2810);
																BgL_iz00_2799 =
																	((BgL_rtl_insz00_bglt) BgL_arg2138z00_2813);
																{	/* SawBbv/bbv.scm 418 */
																	obj_t BgL_callz00_2801;

																	BgL_iz00_2758 = BgL_iz00_2799;
																	if (BGl_rtl_inszd2ifnezf3z21zzsaw_bbvzd2typeszd2(BgL_iz00_2758))
																		{	/* SawBbv/bbv.scm 401 */
																			bool_t BgL_test2571z00_5474;

																			{	/* SawBbv/bbv.scm 401 */
																				bool_t BgL_test2572z00_5475;

																				{	/* SawBbv/bbv.scm 401 */
																					obj_t BgL_arg2131z00_2797;

																					BgL_arg2131z00_2797 =
																						CAR(
																						(((BgL_rtl_insz00_bglt)
																								COBJECT(BgL_iz00_2758))->
																							BgL_argsz00));
																					{	/* SawBbv/bbv.scm 401 */
																						obj_t BgL_classz00_3730;

																						BgL_classz00_3730 =
																							BGl_rtl_insz00zzsaw_defsz00;
																						if (BGL_OBJECTP
																							(BgL_arg2131z00_2797))
																							{	/* SawBbv/bbv.scm 401 */
																								BgL_objectz00_bglt
																									BgL_arg1807z00_3732;
																								BgL_arg1807z00_3732 =
																									(BgL_objectz00_bglt)
																									(BgL_arg2131z00_2797);
																								if (BGL_CONDEXPAND_ISA_ARCH64())
																									{	/* SawBbv/bbv.scm 401 */
																										long BgL_idxz00_3738;

																										BgL_idxz00_3738 =
																											BGL_OBJECT_INHERITANCE_NUM
																											(BgL_arg1807z00_3732);
																										BgL_test2572z00_5475 =
																											(VECTOR_REF
																											(BGl_za2inheritancesza2z00zz__objectz00,
																												(BgL_idxz00_3738 +
																													1L)) ==
																											BgL_classz00_3730);
																									}
																								else
																									{	/* SawBbv/bbv.scm 401 */
																										bool_t BgL_res2334z00_3763;

																										{	/* SawBbv/bbv.scm 401 */
																											obj_t BgL_oclassz00_3746;

																											{	/* SawBbv/bbv.scm 401 */
																												obj_t
																													BgL_arg1815z00_3754;
																												long
																													BgL_arg1816z00_3755;
																												BgL_arg1815z00_3754 =
																													(BGl_za2classesza2z00zz__objectz00);
																												{	/* SawBbv/bbv.scm 401 */
																													long
																														BgL_arg1817z00_3756;
																													BgL_arg1817z00_3756 =
																														BGL_OBJECT_CLASS_NUM
																														(BgL_arg1807z00_3732);
																													BgL_arg1816z00_3755 =
																														(BgL_arg1817z00_3756
																														- OBJECT_TYPE);
																												}
																												BgL_oclassz00_3746 =
																													VECTOR_REF
																													(BgL_arg1815z00_3754,
																													BgL_arg1816z00_3755);
																											}
																											{	/* SawBbv/bbv.scm 401 */
																												bool_t
																													BgL__ortest_1115z00_3747;
																												BgL__ortest_1115z00_3747
																													=
																													(BgL_classz00_3730 ==
																													BgL_oclassz00_3746);
																												if (BgL__ortest_1115z00_3747)
																													{	/* SawBbv/bbv.scm 401 */
																														BgL_res2334z00_3763
																															=
																															BgL__ortest_1115z00_3747;
																													}
																												else
																													{	/* SawBbv/bbv.scm 401 */
																														long
																															BgL_odepthz00_3748;
																														{	/* SawBbv/bbv.scm 401 */
																															obj_t
																																BgL_arg1804z00_3749;
																															BgL_arg1804z00_3749
																																=
																																(BgL_oclassz00_3746);
																															BgL_odepthz00_3748
																																=
																																BGL_CLASS_DEPTH
																																(BgL_arg1804z00_3749);
																														}
																														if (
																															(1L <
																																BgL_odepthz00_3748))
																															{	/* SawBbv/bbv.scm 401 */
																																obj_t
																																	BgL_arg1802z00_3751;
																																{	/* SawBbv/bbv.scm 401 */
																																	obj_t
																																		BgL_arg1803z00_3752;
																																	BgL_arg1803z00_3752
																																		=
																																		(BgL_oclassz00_3746);
																																	BgL_arg1802z00_3751
																																		=
																																		BGL_CLASS_ANCESTORS_REF
																																		(BgL_arg1803z00_3752,
																																		1L);
																																}
																																BgL_res2334z00_3763
																																	=
																																	(BgL_arg1802z00_3751
																																	==
																																	BgL_classz00_3730);
																															}
																														else
																															{	/* SawBbv/bbv.scm 401 */
																																BgL_res2334z00_3763
																																	=
																																	((bool_t) 0);
																															}
																													}
																											}
																										}
																										BgL_test2572z00_5475 =
																											BgL_res2334z00_3763;
																									}
																							}
																						else
																							{	/* SawBbv/bbv.scm 401 */
																								BgL_test2572z00_5475 =
																									((bool_t) 0);
																							}
																					}
																				}
																				if (BgL_test2572z00_5475)
																					{	/* SawBbv/bbv.scm 401 */
																						obj_t BgL_arg2129z00_2795;

																						BgL_arg2129z00_2795 =
																							CAR(
																							(((BgL_rtl_insz00_bglt)
																									COBJECT(BgL_iz00_2758))->
																								BgL_argsz00));
																						BgL_test2571z00_5474 =
																							BGl_rtl_inszd2movzf3z21zzsaw_bbvzd2typeszd2
																							(((BgL_rtl_insz00_bglt)
																								BgL_arg2129z00_2795));
																					}
																				else
																					{	/* SawBbv/bbv.scm 401 */
																						BgL_test2571z00_5474 = ((bool_t) 0);
																					}
																			}
																			if (BgL_test2571z00_5474)
																				{	/* SawBbv/bbv.scm 402 */
																					obj_t BgL_g1230z00_2770;

																					BgL_g1230z00_2770 =
																						CAR(
																						(((BgL_rtl_insz00_bglt)
																								COBJECT(BgL_iz00_2758))->
																							BgL_argsz00));
																					{
																						obj_t BgL_jz00_2772;

																						BgL_jz00_2772 = BgL_g1230z00_2770;
																					BgL_zc3z04anonymousza32112ze3z87_2773:
																						{	/* SawBbv/bbv.scm 404 */
																							bool_t BgL_test2577z00_5506;

																							{	/* SawBbv/bbv.scm 404 */
																								obj_t BgL_classz00_3766;

																								BgL_classz00_3766 =
																									BGl_rtl_insz00zzsaw_defsz00;
																								if (BGL_OBJECTP(BgL_jz00_2772))
																									{	/* SawBbv/bbv.scm 404 */
																										BgL_objectz00_bglt
																											BgL_arg1807z00_3768;
																										BgL_arg1807z00_3768 =
																											(BgL_objectz00_bglt)
																											(BgL_jz00_2772);
																										if (BGL_CONDEXPAND_ISA_ARCH64())
																											{	/* SawBbv/bbv.scm 404 */
																												long BgL_idxz00_3774;

																												BgL_idxz00_3774 =
																													BGL_OBJECT_INHERITANCE_NUM
																													(BgL_arg1807z00_3768);
																												BgL_test2577z00_5506 =
																													(VECTOR_REF
																													(BGl_za2inheritancesza2z00zz__objectz00,
																														(BgL_idxz00_3774 +
																															1L)) ==
																													BgL_classz00_3766);
																											}
																										else
																											{	/* SawBbv/bbv.scm 404 */
																												bool_t
																													BgL_res2335z00_3799;
																												{	/* SawBbv/bbv.scm 404 */
																													obj_t
																														BgL_oclassz00_3782;
																													{	/* SawBbv/bbv.scm 404 */
																														obj_t
																															BgL_arg1815z00_3790;
																														long
																															BgL_arg1816z00_3791;
																														BgL_arg1815z00_3790
																															=
																															(BGl_za2classesza2z00zz__objectz00);
																														{	/* SawBbv/bbv.scm 404 */
																															long
																																BgL_arg1817z00_3792;
																															BgL_arg1817z00_3792
																																=
																																BGL_OBJECT_CLASS_NUM
																																(BgL_arg1807z00_3768);
																															BgL_arg1816z00_3791
																																=
																																(BgL_arg1817z00_3792
																																- OBJECT_TYPE);
																														}
																														BgL_oclassz00_3782 =
																															VECTOR_REF
																															(BgL_arg1815z00_3790,
																															BgL_arg1816z00_3791);
																													}
																													{	/* SawBbv/bbv.scm 404 */
																														bool_t
																															BgL__ortest_1115z00_3783;
																														BgL__ortest_1115z00_3783
																															=
																															(BgL_classz00_3766
																															==
																															BgL_oclassz00_3782);
																														if (BgL__ortest_1115z00_3783)
																															{	/* SawBbv/bbv.scm 404 */
																																BgL_res2335z00_3799
																																	=
																																	BgL__ortest_1115z00_3783;
																															}
																														else
																															{	/* SawBbv/bbv.scm 404 */
																																long
																																	BgL_odepthz00_3784;
																																{	/* SawBbv/bbv.scm 404 */
																																	obj_t
																																		BgL_arg1804z00_3785;
																																	BgL_arg1804z00_3785
																																		=
																																		(BgL_oclassz00_3782);
																																	BgL_odepthz00_3784
																																		=
																																		BGL_CLASS_DEPTH
																																		(BgL_arg1804z00_3785);
																																}
																																if (
																																	(1L <
																																		BgL_odepthz00_3784))
																																	{	/* SawBbv/bbv.scm 404 */
																																		obj_t
																																			BgL_arg1802z00_3787;
																																		{	/* SawBbv/bbv.scm 404 */
																																			obj_t
																																				BgL_arg1803z00_3788;
																																			BgL_arg1803z00_3788
																																				=
																																				(BgL_oclassz00_3782);
																																			BgL_arg1802z00_3787
																																				=
																																				BGL_CLASS_ANCESTORS_REF
																																				(BgL_arg1803z00_3788,
																																				1L);
																																		}
																																		BgL_res2335z00_3799
																																			=
																																			(BgL_arg1802z00_3787
																																			==
																																			BgL_classz00_3766);
																																	}
																																else
																																	{	/* SawBbv/bbv.scm 404 */
																																		BgL_res2335z00_3799
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																													}
																												}
																												BgL_test2577z00_5506 =
																													BgL_res2335z00_3799;
																											}
																									}
																								else
																									{	/* SawBbv/bbv.scm 404 */
																										BgL_test2577z00_5506 =
																											((bool_t) 0);
																									}
																							}
																							if (BgL_test2577z00_5506)
																								{	/* SawBbv/bbv.scm 404 */
																									if (BGl_rtl_inszd2movzf3z21zzsaw_bbvzd2typeszd2(((BgL_rtl_insz00_bglt) BgL_jz00_2772)))
																										{	/* SawBbv/bbv.scm 408 */
																											obj_t BgL_arg2115z00_2777;

																											{	/* SawBbv/bbv.scm 408 */
																												obj_t BgL_pairz00_3800;

																												BgL_pairz00_3800 =
																													(((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt) BgL_jz00_2772)))->BgL_argsz00);
																												BgL_arg2115z00_2777 =
																													CAR(BgL_pairz00_3800);
																											}
																											{
																												obj_t BgL_jz00_5535;

																												BgL_jz00_5535 =
																													BgL_arg2115z00_2777;
																												BgL_jz00_2772 =
																													BgL_jz00_5535;
																												goto
																													BgL_zc3z04anonymousza32112ze3z87_2773;
																											}
																										}
																									else
																										{	/* SawBbv/bbv.scm 406 */
																											if (BGl_rtl_inszd2callzf3z21zzsaw_bbvzd2typeszd2(((BgL_rtl_insz00_bglt) BgL_jz00_2772)))
																												{	/* SawBbv/bbv.scm 409 */
																													if (CBOOL
																														(BGl_rtl_callzd2predicatezd2zzsaw_bbvzd2typeszd2
																															(((BgL_rtl_insz00_bglt) BgL_jz00_2772))))
																														{	/* SawBbv/bbv.scm 411 */
																															obj_t
																																BgL_argsz00_2781;
																															BgL_argsz00_2781 =
																																BGl_rtl_inszd2argsza2z70zzsaw_defsz00
																																(BgL_iz00_2758);
																															{	/* SawBbv/bbv.scm 412 */
																																bool_t
																																	BgL_test2585z00_5544;
																																if (NULLP
																																	(BgL_argsz00_2781))
																																	{	/* SawBbv/bbv.scm 412 */
																																		BgL_test2585z00_5544
																																			=
																																			((bool_t)
																																			0);
																																	}
																																else
																																	{	/* SawBbv/bbv.scm 412 */
																																		if (NULLP
																																			(CDR
																																				(BgL_argsz00_2781)))
																																			{	/* SawBbv/bbv.scm 412 */
																																				obj_t
																																					BgL_arg2125z00_2790;
																																				BgL_arg2125z00_2790
																																					=
																																					CAR
																																					(BgL_argsz00_2781);
																																				{	/* SawBbv/bbv.scm 412 */
																																					obj_t
																																						BgL_classz00_3803;
																																					BgL_classz00_3803
																																						=
																																						BGl_rtl_regz00zzsaw_defsz00;
																																					if (BGL_OBJECTP(BgL_arg2125z00_2790))
																																						{	/* SawBbv/bbv.scm 412 */
																																							BgL_objectz00_bglt
																																								BgL_arg1807z00_3805;
																																							BgL_arg1807z00_3805
																																								=
																																								(BgL_objectz00_bglt)
																																								(BgL_arg2125z00_2790);
																																							if (BGL_CONDEXPAND_ISA_ARCH64())
																																								{	/* SawBbv/bbv.scm 412 */
																																									long
																																										BgL_idxz00_3811;
																																									BgL_idxz00_3811
																																										=
																																										BGL_OBJECT_INHERITANCE_NUM
																																										(BgL_arg1807z00_3805);
																																									BgL_test2585z00_5544
																																										=
																																										(VECTOR_REF
																																										(BGl_za2inheritancesza2z00zz__objectz00,
																																											(BgL_idxz00_3811
																																												+
																																												1L))
																																										==
																																										BgL_classz00_3803);
																																								}
																																							else
																																								{	/* SawBbv/bbv.scm 412 */
																																									bool_t
																																										BgL_res2336z00_3836;
																																									{	/* SawBbv/bbv.scm 412 */
																																										obj_t
																																											BgL_oclassz00_3819;
																																										{	/* SawBbv/bbv.scm 412 */
																																											obj_t
																																												BgL_arg1815z00_3827;
																																											long
																																												BgL_arg1816z00_3828;
																																											BgL_arg1815z00_3827
																																												=
																																												(BGl_za2classesza2z00zz__objectz00);
																																											{	/* SawBbv/bbv.scm 412 */
																																												long
																																													BgL_arg1817z00_3829;
																																												BgL_arg1817z00_3829
																																													=
																																													BGL_OBJECT_CLASS_NUM
																																													(BgL_arg1807z00_3805);
																																												BgL_arg1816z00_3828
																																													=
																																													(BgL_arg1817z00_3829
																																													-
																																													OBJECT_TYPE);
																																											}
																																											BgL_oclassz00_3819
																																												=
																																												VECTOR_REF
																																												(BgL_arg1815z00_3827,
																																												BgL_arg1816z00_3828);
																																										}
																																										{	/* SawBbv/bbv.scm 412 */
																																											bool_t
																																												BgL__ortest_1115z00_3820;
																																											BgL__ortest_1115z00_3820
																																												=
																																												(BgL_classz00_3803
																																												==
																																												BgL_oclassz00_3819);
																																											if (BgL__ortest_1115z00_3820)
																																												{	/* SawBbv/bbv.scm 412 */
																																													BgL_res2336z00_3836
																																														=
																																														BgL__ortest_1115z00_3820;
																																												}
																																											else
																																												{	/* SawBbv/bbv.scm 412 */
																																													long
																																														BgL_odepthz00_3821;
																																													{	/* SawBbv/bbv.scm 412 */
																																														obj_t
																																															BgL_arg1804z00_3822;
																																														BgL_arg1804z00_3822
																																															=
																																															(BgL_oclassz00_3819);
																																														BgL_odepthz00_3821
																																															=
																																															BGL_CLASS_DEPTH
																																															(BgL_arg1804z00_3822);
																																													}
																																													if ((1L < BgL_odepthz00_3821))
																																														{	/* SawBbv/bbv.scm 412 */
																																															obj_t
																																																BgL_arg1802z00_3824;
																																															{	/* SawBbv/bbv.scm 412 */
																																																obj_t
																																																	BgL_arg1803z00_3825;
																																																BgL_arg1803z00_3825
																																																	=
																																																	(BgL_oclassz00_3819);
																																																BgL_arg1802z00_3824
																																																	=
																																																	BGL_CLASS_ANCESTORS_REF
																																																	(BgL_arg1803z00_3825,
																																																	1L);
																																															}
																																															BgL_res2336z00_3836
																																																=
																																																(BgL_arg1802z00_3824
																																																==
																																																BgL_classz00_3803);
																																														}
																																													else
																																														{	/* SawBbv/bbv.scm 412 */
																																															BgL_res2336z00_3836
																																																=
																																																(
																																																(bool_t)
																																																0);
																																														}
																																												}
																																										}
																																									}
																																									BgL_test2585z00_5544
																																										=
																																										BgL_res2336z00_3836;
																																								}
																																						}
																																					else
																																						{	/* SawBbv/bbv.scm 412 */
																																							BgL_test2585z00_5544
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																			}
																																		else
																																			{	/* SawBbv/bbv.scm 412 */
																																				BgL_test2585z00_5544
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																																if (BgL_test2585z00_5544)
																																	{	/* SawBbv/bbv.scm 412 */
																																		BgL_callz00_2801
																																			=
																																			BgL_jz00_2772;
																																	}
																																else
																																	{	/* SawBbv/bbv.scm 412 */
																																		BgL_callz00_2801
																																			= BFALSE;
																																	}
																															}
																														}
																													else
																														{	/* SawBbv/bbv.scm 410 */
																															BgL_callz00_2801 =
																																BFALSE;
																														}
																												}
																											else
																												{	/* SawBbv/bbv.scm 409 */
																													BgL_callz00_2801 =
																														BFALSE;
																												}
																										}
																								}
																							else
																								{	/* SawBbv/bbv.scm 404 */
																									BgL_callz00_2801 = BFALSE;
																								}
																						}
																					}
																				}
																			else
																				{	/* SawBbv/bbv.scm 401 */
																					BgL_callz00_2801 = BFALSE;
																				}
																		}
																	else
																		{	/* SawBbv/bbv.scm 399 */
																			BgL_callz00_2801 = BFALSE;
																		}
																	if (CBOOL(BgL_callz00_2801))
																		{	/* SawBbv/bbv.scm 419 */
																			((((BgL_rtl_insz00_bglt) COBJECT(
																							((BgL_rtl_insz00_bglt)
																								BgL_callz00_2801)))->
																					BgL_destz00) =
																				((obj_t) (((BgL_rtl_insz00_bglt)
																							COBJECT(BgL_iz00_2799))->
																						BgL_destz00)), BUNSPEC);
																			{	/* SawBbv/bbv.scm 423 */
																				obj_t BgL_arg2134z00_2804;

																				BgL_arg2134z00_2804 =
																					(((BgL_rtl_insz00_bglt)
																						COBJECT(BgL_iz00_2799))->
																					BgL_argsz00);
																				{	/* SawBbv/bbv.scm 423 */
																					obj_t BgL_tmpz00_5579;

																					BgL_tmpz00_5579 =
																						((obj_t) BgL_arg2134z00_2804);
																					SET_CAR(BgL_tmpz00_5579,
																						BgL_callz00_2801);
																				}
																			}
																		}
																	else
																		{	/* SawBbv/bbv.scm 419 */
																			BFALSE;
																		}
																}
															}
															{
																obj_t BgL_l1510z00_5583;

																BgL_l1510z00_5583 = CDR(BgL_l1510z00_2810);
																BgL_l1510z00_2810 = BgL_l1510z00_5583;
																goto BgL_zc3z04anonymousza32136ze3z87_2811;
															}
														}
													else
														{	/* SawBbv/bbv.scm 427 */
															((bool_t) 1);
														}
												}
											}
										}
										{	/* SawBbv/bbv.scm 438 */
											BgL_blockz00_bglt BgL_i1237z00_2749;

											BgL_i1237z00_2749 =
												((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2741)));
											{	/* SawBbv/bbv.scm 439 */
												obj_t BgL_arg2095z00_2750;
												obj_t BgL_arg2096z00_2751;

												{	/* SawBbv/bbv.scm 439 */
													obj_t BgL_arg2097z00_2752;
													obj_t BgL_arg2098z00_2753;

													BgL_arg2097z00_2752 =
														(((BgL_blockz00_bglt) COBJECT(BgL_i1237z00_2749))->
														BgL_succsz00);
													BgL_arg2098z00_2753 = CDR(((obj_t) BgL_bsz00_2741));
													BgL_arg2095z00_2750 =
														BGl_appendzd221011zd2zzsaw_bbvz00
														(BgL_arg2097z00_2752, BgL_arg2098z00_2753);
												}
												{	/* SawBbv/bbv.scm 439 */
													obj_t BgL_arg2099z00_2754;

													BgL_arg2099z00_2754 = CAR(((obj_t) BgL_bsz00_2741));
													BgL_arg2096z00_2751 =
														MAKE_YOUNG_PAIR(BgL_arg2099z00_2754,
														BgL_accz00_2742);
												}
												{
													obj_t BgL_accz00_5597;
													obj_t BgL_bsz00_5596;

													BgL_bsz00_5596 = BgL_arg2095z00_2750;
													BgL_accz00_5597 = BgL_arg2096z00_2751;
													BgL_accz00_2742 = BgL_accz00_5597;
													BgL_bsz00_2741 = BgL_bsz00_5596;
													goto BgL_zc3z04anonymousza32088ze3z87_2743;
												}
											}
										}
									}
							}
					}
				}
			}
		}

	}



/* normalize-goto! */
	obj_t BGl_normaliza7ezd2gotoz12z67zzsaw_bbvz00(BgL_blockz00_bglt BgL_bz00_109)
	{
		{	/* SawBbv/bbv.scm 449 */
			{
				BgL_blockz00_bglt BgL_bz00_2840;

				{	/* SawBbv/bbv.scm 465 */
					obj_t BgL_g1244z00_2820;

					{	/* SawBbv/bbv.scm 465 */
						obj_t BgL_list2152z00_2839;

						BgL_list2152z00_2839 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_109), BNIL);
						BgL_g1244z00_2820 = BgL_list2152z00_2839;
					}
					{
						obj_t BgL_bsz00_2823;
						obj_t BgL_accz00_2824;

						BgL_bsz00_2823 = BgL_g1244z00_2820;
						BgL_accz00_2824 = BNIL;
					BgL_zc3z04anonymousza32140ze3z87_2825:
						if (NULLP(BgL_bsz00_2823))
							{	/* SawBbv/bbv.scm 468 */
								return bgl_reverse(BgL_accz00_2824);
							}
						else
							{	/* SawBbv/bbv.scm 468 */
								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
												((obj_t) BgL_bsz00_2823)), BgL_accz00_2824)))
									{	/* SawBbv/bbv.scm 471 */
										obj_t BgL_arg2144z00_2829;

										BgL_arg2144z00_2829 = CDR(((obj_t) BgL_bsz00_2823));
										{
											obj_t BgL_bsz00_5610;

											BgL_bsz00_5610 = BgL_arg2144z00_2829;
											BgL_bsz00_2823 = BgL_bsz00_5610;
											goto BgL_zc3z04anonymousza32140ze3z87_2825;
										}
									}
								else
									{	/* SawBbv/bbv.scm 470 */
										{	/* SawBbv/bbv.scm 473 */
											obj_t BgL_arg2145z00_2830;

											BgL_arg2145z00_2830 = CAR(((obj_t) BgL_bsz00_2823));
											BgL_bz00_2840 = ((BgL_blockz00_bglt) BgL_arg2145z00_2830);
											if (NULLP(
													(((BgL_blockz00_bglt) COBJECT(BgL_bz00_2840))->
														BgL_succsz00)))
												{	/* SawBbv/bbv.scm 453 */
													BFALSE;
												}
											else
												{	/* SawBbv/bbv.scm 454 */
													obj_t BgL_lpz00_2845;

													BgL_lpz00_2845 =
														BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
														(((BgL_blockz00_bglt) COBJECT(BgL_bz00_2840))->
															BgL_firstz00));
													{	/* SawBbv/bbv.scm 454 */
														obj_t BgL_lastz00_2846;

														BgL_lastz00_2846 = CAR(BgL_lpz00_2845);
														{	/* SawBbv/bbv.scm 455 */

															{	/* SawBbv/bbv.scm 456 */
																bool_t BgL_test2596z00_5619;

																if (BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2(
																		((BgL_rtl_insz00_bglt) BgL_lastz00_2846)))
																	{	/* SawBbv/bbv.scm 456 */
																		BgL_test2596z00_5619 = ((bool_t) 1);
																	}
																else
																	{	/* SawBbv/bbv.scm 456 */
																		BgL_test2596z00_5619 =
																			BGl_rtl_inszd2failzf3z21zzsaw_bbvzd2typeszd2
																			(((BgL_rtl_insz00_bglt)
																				BgL_lastz00_2846));
																	}
																if (BgL_test2596z00_5619)
																	{	/* SawBbv/bbv.scm 456 */
																		BFALSE;
																	}
																else
																	{	/* SawBbv/bbv.scm 457 */
																		BgL_rtl_insz00_bglt BgL_goz00_2849;

																		{	/* SawBbv/bbv.scm 457 */
																			BgL_rtl_insz00_bglt BgL_new1240z00_2852;

																			{	/* SawBbv/bbv.scm 457 */
																				BgL_rtl_insz00_bglt BgL_new1239z00_2857;

																				BgL_new1239z00_2857 =
																					((BgL_rtl_insz00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_rtl_insz00_bgl))));
																				{	/* SawBbv/bbv.scm 457 */
																					long BgL_arg2162z00_2858;

																					BgL_arg2162z00_2858 =
																						BGL_CLASS_NUM
																						(BGl_rtl_insz00zzsaw_defsz00);
																					BGL_OBJECT_CLASS_NUM_SET((
																							(BgL_objectz00_bglt)
																							BgL_new1239z00_2857),
																						BgL_arg2162z00_2858);
																				}
																				{	/* SawBbv/bbv.scm 457 */
																					BgL_objectz00_bglt BgL_tmpz00_5629;

																					BgL_tmpz00_5629 =
																						((BgL_objectz00_bglt)
																						BgL_new1239z00_2857);
																					BGL_OBJECT_WIDENING_SET
																						(BgL_tmpz00_5629, BFALSE);
																				}
																				((BgL_objectz00_bglt)
																					BgL_new1239z00_2857);
																				BgL_new1240z00_2852 =
																					BgL_new1239z00_2857;
																			}
																			((((BgL_rtl_insz00_bglt)
																						COBJECT(BgL_new1240z00_2852))->
																					BgL_locz00) =
																				((obj_t) BFALSE), BUNSPEC);
																			((((BgL_rtl_insz00_bglt)
																						COBJECT(BgL_new1240z00_2852))->
																					BgL_z52spillz52) =
																				((obj_t) BNIL), BUNSPEC);
																			((((BgL_rtl_insz00_bglt)
																						COBJECT(BgL_new1240z00_2852))->
																					BgL_destz00) =
																				((obj_t) BFALSE), BUNSPEC);
																			{
																				BgL_rtl_funz00_bglt BgL_auxz00_5636;

																				{	/* SawBbv/bbv.scm 459 */
																					BgL_rtl_goz00_bglt
																						BgL_new1243z00_2853;
																					{	/* SawBbv/bbv.scm 460 */
																						BgL_rtl_goz00_bglt
																							BgL_new1242z00_2855;
																						BgL_new1242z00_2855 =
																							((BgL_rtl_goz00_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_rtl_goz00_bgl))));
																						{	/* SawBbv/bbv.scm 460 */
																							long BgL_arg2161z00_2856;

																							{	/* SawBbv/bbv.scm 460 */
																								obj_t BgL_classz00_3852;

																								BgL_classz00_3852 =
																									BGl_rtl_goz00zzsaw_defsz00;
																								BgL_arg2161z00_2856 =
																									BGL_CLASS_NUM
																									(BgL_classz00_3852);
																							}
																							BGL_OBJECT_CLASS_NUM_SET(
																								((BgL_objectz00_bglt)
																									BgL_new1242z00_2855),
																								BgL_arg2161z00_2856);
																						}
																						BgL_new1243z00_2853 =
																							BgL_new1242z00_2855;
																					}
																					((((BgL_rtl_funz00_bglt) COBJECT(
																									((BgL_rtl_funz00_bglt)
																										BgL_new1243z00_2853)))->
																							BgL_locz00) =
																						((obj_t) (((BgL_rtl_insz00_bglt)
																									COBJECT(((BgL_rtl_insz00_bglt)
																											BgL_lastz00_2846)))->
																								BgL_locz00)), BUNSPEC);
																					{
																						BgL_blockz00_bglt BgL_auxz00_5645;

																						{	/* SawBbv/bbv.scm 461 */
																							obj_t BgL_pairz00_3856;

																							BgL_pairz00_3856 =
																								(((BgL_blockz00_bglt)
																									COBJECT(BgL_bz00_2840))->
																								BgL_succsz00);
																							BgL_auxz00_5645 =
																								((BgL_blockz00_bglt)
																								CAR(BgL_pairz00_3856));
																						}
																						((((BgL_rtl_goz00_bglt)
																									COBJECT
																									(BgL_new1243z00_2853))->
																								BgL_toz00) =
																							((BgL_blockz00_bglt)
																								BgL_auxz00_5645), BUNSPEC);
																					}
																					BgL_auxz00_5636 =
																						((BgL_rtl_funz00_bglt)
																						BgL_new1243z00_2853);
																				}
																				((((BgL_rtl_insz00_bglt)
																							COBJECT(BgL_new1240z00_2852))->
																						BgL_funz00) =
																					((BgL_rtl_funz00_bglt)
																						BgL_auxz00_5636), BUNSPEC);
																			}
																			((((BgL_rtl_insz00_bglt)
																						COBJECT(BgL_new1240z00_2852))->
																					BgL_argsz00) =
																				((obj_t) BNIL), BUNSPEC);
																			BgL_goz00_2849 = BgL_new1240z00_2852;
																		}
																		{	/* SawBbv/bbv.scm 463 */
																			obj_t BgL_arg2158z00_2850;

																			{	/* SawBbv/bbv.scm 463 */
																				obj_t BgL_list2159z00_2851;

																				BgL_list2159z00_2851 =
																					MAKE_YOUNG_PAIR(
																					((obj_t) BgL_goz00_2849), BNIL);
																				BgL_arg2158z00_2850 =
																					BgL_list2159z00_2851;
																			}
																			SET_CDR(BgL_lpz00_2845,
																				BgL_arg2158z00_2850);
										}}}}}}}
										{	/* SawBbv/bbv.scm 474 */
											BgL_blockz00_bglt BgL_i1246z00_2831;

											BgL_i1246z00_2831 =
												((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2823)));
											{	/* SawBbv/bbv.scm 475 */
												obj_t BgL_arg2146z00_2832;
												obj_t BgL_arg2147z00_2833;

												{	/* SawBbv/bbv.scm 475 */
													obj_t BgL_arg2148z00_2834;
													obj_t BgL_arg2149z00_2835;

													BgL_arg2148z00_2834 =
														(((BgL_blockz00_bglt) COBJECT(BgL_i1246z00_2831))->
														BgL_succsz00);
													BgL_arg2149z00_2835 = CDR(((obj_t) BgL_bsz00_2823));
													BgL_arg2146z00_2832 =
														BGl_appendzd221011zd2zzsaw_bbvz00
														(BgL_arg2148z00_2834, BgL_arg2149z00_2835);
												}
												{	/* SawBbv/bbv.scm 475 */
													obj_t BgL_arg2150z00_2836;

													BgL_arg2150z00_2836 = CAR(((obj_t) BgL_bsz00_2823));
													BgL_arg2147z00_2833 =
														MAKE_YOUNG_PAIR(BgL_arg2150z00_2836,
														BgL_accz00_2824);
												}
												{
													obj_t BgL_accz00_5668;
													obj_t BgL_bsz00_5667;

													BgL_bsz00_5667 = BgL_arg2146z00_2832;
													BgL_accz00_5668 = BgL_arg2147z00_2833;
													BgL_accz00_2824 = BgL_accz00_5668;
													BgL_bsz00_2823 = BgL_bsz00_5667;
													goto BgL_zc3z04anonymousza32140ze3z87_2825;
												}
											}
										}
									}
							}
					}
				}
			}
		}

	}



/* remove-temps! */
	BgL_blockz00_bglt BGl_removezd2tempsz12zc0zzsaw_bbvz00(BgL_blockz00_bglt
		BgL_bz00_110)
	{
		{	/* SawBbv/bbv.scm 480 */
			{
				BgL_blockz00_bglt BgL_bz00_2906;

				{	/* SawBbv/bbv.scm 503 */
					obj_t BgL_g1251z00_2865;

					{	/* SawBbv/bbv.scm 503 */
						obj_t BgL_list2177z00_2884;

						BgL_list2177z00_2884 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_110), BNIL);
						BgL_g1251z00_2865 = BgL_list2177z00_2884;
					}
					{
						obj_t BgL_bsz00_2868;
						obj_t BgL_accz00_2869;

						BgL_bsz00_2868 = BgL_g1251z00_2865;
						BgL_accz00_2869 = BNIL;
					BgL_zc3z04anonymousza32165ze3z87_2870:
						if (NULLP(BgL_bsz00_2868))
							{	/* SawBbv/bbv.scm 506 */
								return BgL_bz00_110;
							}
						else
							{	/* SawBbv/bbv.scm 506 */
								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
												((obj_t) BgL_bsz00_2868)), BgL_accz00_2869)))
									{	/* SawBbv/bbv.scm 509 */
										obj_t BgL_arg2169z00_2874;

										BgL_arg2169z00_2874 = CDR(((obj_t) BgL_bsz00_2868));
										{
											obj_t BgL_bsz00_5680;

											BgL_bsz00_5680 = BgL_arg2169z00_2874;
											BgL_bsz00_2868 = BgL_bsz00_5680;
											goto BgL_zc3z04anonymousza32165ze3z87_2870;
										}
									}
								else
									{	/* SawBbv/bbv.scm 511 */
										BgL_blockz00_bglt BgL_i1253z00_2875;

										BgL_i1253z00_2875 =
											((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2868)));
										{	/* SawBbv/bbv.scm 512 */
											obj_t BgL_arg2170z00_2876;

											BgL_arg2170z00_2876 = CAR(((obj_t) BgL_bsz00_2868));
											BgL_bz00_2906 = ((BgL_blockz00_bglt) BgL_arg2170z00_2876);
											{
												obj_t BgL_auxz00_5686;

												{	/* SawBbv/bbv.scm 501 */
													obj_t BgL_l01515z00_2909;

													BgL_l01515z00_2909 =
														(((BgL_blockz00_bglt) COBJECT(BgL_bz00_2906))->
														BgL_firstz00);
													{
														obj_t BgL_l1514z00_2911;

														BgL_l1514z00_2911 = BgL_l01515z00_2909;
													BgL_zc3z04anonymousza32190ze3z87_2912:
														if (NULLP(BgL_l1514z00_2911))
															{	/* SawBbv/bbv.scm 501 */
																BgL_auxz00_5686 = BgL_l01515z00_2909;
															}
														else
															{	/* SawBbv/bbv.scm 501 */
																{	/* SawBbv/bbv.scm 501 */
																	BgL_rtl_insz00_bglt BgL_arg2192z00_2914;

																	{	/* SawBbv/bbv.scm 501 */
																		obj_t BgL_arg2193z00_2915;

																		BgL_arg2193z00_2915 =
																			CAR(((obj_t) BgL_l1514z00_2911));
																		BgL_arg2192z00_2914 =
																			BGl_removezd2inszd2tempsz12ze70zf5zzsaw_bbvz00
																			(((BgL_rtl_insz00_bglt)
																				BgL_arg2193z00_2915));
																	}
																	{	/* SawBbv/bbv.scm 501 */
																		obj_t BgL_auxz00_5696;
																		obj_t BgL_tmpz00_5694;

																		BgL_auxz00_5696 =
																			((obj_t) BgL_arg2192z00_2914);
																		BgL_tmpz00_5694 =
																			((obj_t) BgL_l1514z00_2911);
																		SET_CAR(BgL_tmpz00_5694, BgL_auxz00_5696);
																	}
																}
																{	/* SawBbv/bbv.scm 501 */
																	obj_t BgL_arg2194z00_2916;

																	BgL_arg2194z00_2916 =
																		CDR(((obj_t) BgL_l1514z00_2911));
																	{
																		obj_t BgL_l1514z00_5701;

																		BgL_l1514z00_5701 = BgL_arg2194z00_2916;
																		BgL_l1514z00_2911 = BgL_l1514z00_5701;
																		goto BgL_zc3z04anonymousza32190ze3z87_2912;
																	}
																}
															}
													}
												}
												((((BgL_blockz00_bglt) COBJECT(BgL_bz00_2906))->
														BgL_firstz00) = ((obj_t) BgL_auxz00_5686), BUNSPEC);
											}
										}
										{	/* SawBbv/bbv.scm 513 */
											obj_t BgL_arg2171z00_2877;
											obj_t BgL_arg2172z00_2878;

											{	/* SawBbv/bbv.scm 513 */
												obj_t BgL_arg2173z00_2879;
												obj_t BgL_arg2174z00_2880;

												BgL_arg2173z00_2879 =
													(((BgL_blockz00_bglt) COBJECT(BgL_i1253z00_2875))->
													BgL_succsz00);
												BgL_arg2174z00_2880 = CDR(((obj_t) BgL_bsz00_2868));
												BgL_arg2171z00_2877 =
													BGl_appendzd221011zd2zzsaw_bbvz00(BgL_arg2173z00_2879,
													BgL_arg2174z00_2880);
											}
											{	/* SawBbv/bbv.scm 513 */
												obj_t BgL_arg2175z00_2881;

												BgL_arg2175z00_2881 = CAR(((obj_t) BgL_bsz00_2868));
												BgL_arg2172z00_2878 =
													MAKE_YOUNG_PAIR(BgL_arg2175z00_2881, BgL_accz00_2869);
											}
											{
												obj_t BgL_accz00_5712;
												obj_t BgL_bsz00_5711;

												BgL_bsz00_5711 = BgL_arg2171z00_2877;
												BgL_accz00_5712 = BgL_arg2172z00_2878;
												BgL_accz00_2869 = BgL_accz00_5712;
												BgL_bsz00_2868 = BgL_bsz00_5711;
												goto BgL_zc3z04anonymousza32165ze3z87_2870;
											}
										}
									}
							}
					}
				}
			}
		}

	}



/* remove-ins-temps!~0 */
	BgL_rtl_insz00_bglt
		BGl_removezd2inszd2tempsz12ze70zf5zzsaw_bbvz00(BgL_rtl_insz00_bglt
		BgL_insz00_2885)
	{
		{	/* SawBbv/bbv.scm 497 */
			{
				obj_t BgL_argsz00_2890;

				BgL_argsz00_2890 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_insz00_2885))->BgL_argsz00);
			BgL_zc3z04anonymousza32180ze3z87_2891:
				if (NULLP(BgL_argsz00_2890))
					{	/* SawBbv/bbv.scm 485 */
						return BgL_insz00_2885;
					}
				else
					{	/* SawBbv/bbv.scm 487 */
						obj_t BgL_g1248z00_2893;

						BgL_g1248z00_2893 = CAR(((obj_t) BgL_argsz00_2890));
						{
							obj_t BgL_argz00_2895;

							BgL_argz00_2895 = BgL_g1248z00_2893;
						BgL_zc3z04anonymousza32182ze3z87_2896:
							{	/* SawBbv/bbv.scm 488 */
								bool_t BgL_test2602z00_5717;

								{	/* SawBbv/bbv.scm 488 */
									obj_t BgL_classz00_3867;

									BgL_classz00_3867 = BGl_rtl_insz00zzsaw_defsz00;
									if (BGL_OBJECTP(BgL_argz00_2895))
										{	/* SawBbv/bbv.scm 488 */
											BgL_objectz00_bglt BgL_arg1807z00_3869;

											BgL_arg1807z00_3869 =
												(BgL_objectz00_bglt) (BgL_argz00_2895);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawBbv/bbv.scm 488 */
													long BgL_idxz00_3875;

													BgL_idxz00_3875 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3869);
													BgL_test2602z00_5717 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_3875 + 1L)) == BgL_classz00_3867);
												}
											else
												{	/* SawBbv/bbv.scm 488 */
													bool_t BgL_res2340z00_3900;

													{	/* SawBbv/bbv.scm 488 */
														obj_t BgL_oclassz00_3883;

														{	/* SawBbv/bbv.scm 488 */
															obj_t BgL_arg1815z00_3891;
															long BgL_arg1816z00_3892;

															BgL_arg1815z00_3891 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawBbv/bbv.scm 488 */
																long BgL_arg1817z00_3893;

																BgL_arg1817z00_3893 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3869);
																BgL_arg1816z00_3892 =
																	(BgL_arg1817z00_3893 - OBJECT_TYPE);
															}
															BgL_oclassz00_3883 =
																VECTOR_REF(BgL_arg1815z00_3891,
																BgL_arg1816z00_3892);
														}
														{	/* SawBbv/bbv.scm 488 */
															bool_t BgL__ortest_1115z00_3884;

															BgL__ortest_1115z00_3884 =
																(BgL_classz00_3867 == BgL_oclassz00_3883);
															if (BgL__ortest_1115z00_3884)
																{	/* SawBbv/bbv.scm 488 */
																	BgL_res2340z00_3900 =
																		BgL__ortest_1115z00_3884;
																}
															else
																{	/* SawBbv/bbv.scm 488 */
																	long BgL_odepthz00_3885;

																	{	/* SawBbv/bbv.scm 488 */
																		obj_t BgL_arg1804z00_3886;

																		BgL_arg1804z00_3886 = (BgL_oclassz00_3883);
																		BgL_odepthz00_3885 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_3886);
																	}
																	if ((1L < BgL_odepthz00_3885))
																		{	/* SawBbv/bbv.scm 488 */
																			obj_t BgL_arg1802z00_3888;

																			{	/* SawBbv/bbv.scm 488 */
																				obj_t BgL_arg1803z00_3889;

																				BgL_arg1803z00_3889 =
																					(BgL_oclassz00_3883);
																				BgL_arg1802z00_3888 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_3889, 1L);
																			}
																			BgL_res2340z00_3900 =
																				(BgL_arg1802z00_3888 ==
																				BgL_classz00_3867);
																		}
																	else
																		{	/* SawBbv/bbv.scm 488 */
																			BgL_res2340z00_3900 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2602z00_5717 = BgL_res2340z00_3900;
												}
										}
									else
										{	/* SawBbv/bbv.scm 488 */
											BgL_test2602z00_5717 = ((bool_t) 0);
										}
								}
								if (BgL_test2602z00_5717)
									{	/* SawBbv/bbv.scm 488 */
										if (BGl_rtl_inszd2movzf3z21zzsaw_bbvzd2typeszd2(
												((BgL_rtl_insz00_bglt) BgL_argz00_2895)))
											{	/* SawBbv/bbv.scm 491 */
												obj_t BgL_arg2185z00_2900;

												{	/* SawBbv/bbv.scm 491 */
													obj_t BgL_pairz00_3901;

													BgL_pairz00_3901 =
														(((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt) BgL_argz00_2895)))->
														BgL_argsz00);
													BgL_arg2185z00_2900 = CAR(BgL_pairz00_3901);
												}
												{
													obj_t BgL_argz00_5746;

													BgL_argz00_5746 = BgL_arg2185z00_2900;
													BgL_argz00_2895 = BgL_argz00_5746;
													goto BgL_zc3z04anonymousza32182ze3z87_2896;
												}
											}
										else
											{	/* SawBbv/bbv.scm 489 */
												BGl_removezd2inszd2tempsz12ze70zf5zzsaw_bbvz00(
													((BgL_rtl_insz00_bglt) BgL_argz00_2895));
												{	/* SawBbv/bbv.scm 494 */
													obj_t BgL_arg2187z00_2902;

													BgL_arg2187z00_2902 = CDR(((obj_t) BgL_argsz00_2890));
													{
														obj_t BgL_argsz00_5751;

														BgL_argsz00_5751 = BgL_arg2187z00_2902;
														BgL_argsz00_2890 = BgL_argsz00_5751;
														goto BgL_zc3z04anonymousza32180ze3z87_2891;
													}
												}
											}
									}
								else
									{	/* SawBbv/bbv.scm 488 */
										{	/* SawBbv/bbv.scm 496 */
											obj_t BgL_tmpz00_5752;

											BgL_tmpz00_5752 = ((obj_t) BgL_argsz00_2890);
											SET_CAR(BgL_tmpz00_5752, BgL_argz00_2895);
										}
										{	/* SawBbv/bbv.scm 497 */
											obj_t BgL_arg2188z00_2903;

											BgL_arg2188z00_2903 = CDR(((obj_t) BgL_argsz00_2890));
											{
												obj_t BgL_argsz00_5757;

												BgL_argsz00_5757 = BgL_arg2188z00_2903;
												BgL_argsz00_2890 = BgL_argsz00_5757;
												goto BgL_zc3z04anonymousza32180ze3z87_2891;
											}
										}
									}
							}
						}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_bbvz00(void)
	{
		{	/* SawBbv/bbv.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_bbvz00(void)
	{
		{	/* SawBbv/bbv.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_bbvz00(void)
	{
		{	/* SawBbv/bbv.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvz00(void)
	{
		{	/* SawBbv/bbv.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzsaw_regsetz00(358079690L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(237915200L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(288263219L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(425659118L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2specializa7ez75(506937389L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(242097300L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2utilszd2(221709922L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2mergezd2(376575453L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2livenesszd2(156090797L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2optimzd2(154965736L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2debugzd2(120981929L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_bbvzd2profilezd2(288620400L,
				BSTRING_TO_STRING(BGl_string2379z00zzsaw_bbvz00));
		}

	}

#ifdef __cplusplus
}
#endif
