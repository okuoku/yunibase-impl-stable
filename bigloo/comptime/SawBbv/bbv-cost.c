/*===========================================================================*/
/*   (SawBbv/bbv-cost.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawBbv/bbv-cost.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_SAW_BBVzd2COSTzd2_TYPE_DEFINITIONS
#define BGL_BgL_SAW_BBVzd2COSTzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_rtl_regzf2razf2_bgl
	{
		int BgL_numz00;
		obj_t BgL_colorz00;
		obj_t BgL_coalescez00;
		int BgL_occurrencesz00;
		obj_t BgL_interferez00;
		obj_t BgL_interfere2z00;
	}                      *BgL_rtl_regzf2razf2_bglt;

	typedef struct BgL_regsetz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_lengthz00;
		int BgL_msiza7eza7;
		obj_t BgL_regvz00;
		obj_t BgL_reglz00;
		obj_t BgL_stringz00;
	}                *BgL_regsetz00_bglt;

	typedef struct BgL_rtl_inszf2bbvzf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_outz00;
		obj_t BgL_inz00;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		obj_t BgL_z52hashz52;
	}                       *BgL_rtl_inszf2bbvzf2_bglt;

	typedef struct BgL_blockvz00_bgl
	{
		obj_t BgL_versionsz00;
		obj_t BgL_genericz00;
		long BgL_z52markz52;
		obj_t BgL_mergez00;
	}                *BgL_blockvz00_bglt;

	typedef struct BgL_blocksz00_bgl
	{
		long BgL_z52markz52;
		obj_t BgL_z52hashz52;
		obj_t BgL_z52blacklistz52;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		struct BgL_blockz00_bgl *BgL_parentz00;
		long BgL_gccntz00;
		long BgL_gcmarkz00;
		obj_t BgL_mblockz00;
		obj_t BgL_creatorz00;
		obj_t BgL_mergesz00;
		bool_t BgL_asleepz00;
	}                *BgL_blocksz00_bglt;

	typedef struct BgL_bbvzd2queuezd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_blocksz00;
		obj_t BgL_lastz00;
	}                     *BgL_bbvzd2queuezd2_bglt;

	typedef struct BgL_bbvzd2ctxzd2_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_idz00;
		obj_t BgL_entriesz00;
	}                   *BgL_bbvzd2ctxzd2_bglt;

	typedef struct BgL_bbvzd2ctxentryzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_rtl_regz00_bgl *BgL_regz00;
		obj_t BgL_typesz00;
		bool_t BgL_polarityz00;
		long BgL_countz00;
		obj_t BgL_valuez00;
		obj_t BgL_aliasesz00;
		obj_t BgL_initvalz00;
	}                        *BgL_bbvzd2ctxentryzd2_bglt;


#endif													// BGL_BgL_SAW_BBVzd2COSTzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2ctxzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt,
		BgL_bbvzd2ctxzd2_bglt);
	static BgL_blockz00_bglt BGl_z62blockSzd2parentzb0zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	static obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	extern obj_t BGl_za2zc3flza2zc3zzsaw_bbvzd2cachezd2;
	static obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2;
	BGL_EXPORTED_DECL bool_t
		BGl_bbvzd2ctxentryzd2polarityz00zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_blockVzd2nilzd2zzsaw_bbvzd2costzd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2firstzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	static obj_t BGl_z62blockVzd2firstzb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2defz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62rtl_inszf2bbvzd2defz42zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_z62blockSzd2asleepzb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_bbvzd2costzd2 = BUNSPEC;
	static obj_t BGl_z62bbvzd2queuezd2lastz62zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2gccntzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt, long);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_inszf2bbvzd2argsz42zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2typesz00zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	static BgL_blockz00_bglt BGl_z62blockVzd2nilzb0zzsaw_bbvzd2costzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2argszd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt,
		obj_t);
	static obj_t BGl_z62blockSzd2z52hashze2zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2z52hashzd2setz12zd0zzsaw_bbvzd2costzd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2ctxzf3z43zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_blockSzd2z52markz80zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_za2zd3flza2zd3zzsaw_bbvzd2cachezd2;
	static obj_t BGl_z62blockVzd2z52markze2zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62blockVzd2versionszd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockSzd2succszd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzf3z43zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_blockVzd2labelzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzd2initvalz62zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2costzd2(int, int, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2costzd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2gcmarkzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt, long);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2costzd2(BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, int, obj_t, obj_t, int, obj_t, obj_t);
	extern obj_t BGl_za2zc3zd3flza2z10zzsaw_bbvzd2cachezd2;
	static obj_t BGl_z62rtl_inszf2bbvzd2z52spillz10zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2costzd2(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	extern bool_t
		BGl_rtl_inszd2callzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62blockVzd2mergezb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static BgL_bbvzd2ctxzd2_bglt
		BGl_z62makezd2bbvzd2ctxz62zzsaw_bbvzd2costzd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2inz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62blockSzd2predszd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2outzd2setz12z82zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2costzd2(void);
	static obj_t BGl_z62bbvzd2ctxentryzd2polarityz62zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	static obj_t BGl_z62blockVzd2succszd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2z52spillzd2setz12zb2zzsaw_bbvzd2costzd2
		(BgL_rtl_insz00_bglt, obj_t);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2versionszd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzd2countz62zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	static obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62blockSzd2labelzd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockVzd2predszd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2initvalz00zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	static obj_t BGl_z62blockVzd2genericzb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2z52hashz72zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_genericzd2initzd2zzsaw_bbvzd2costzd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt);
	static BgL_bbvzd2queuezd2_bglt
		BGl_z62bbvzd2queuezd2nilz62zzsaw_bbvzd2costzd2(obj_t);
	static obj_t BGl_z62blockSzf3z91zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2outzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt,
		obj_t);
	extern obj_t BGl_za2ze3flza2ze3zzsaw_bbvzd2cachezd2;
	static obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2costzd2(obj_t);
	static obj_t BGl_z62blockVzd2labelzd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_objectzd2initzd2zzsaw_bbvzd2costzd2(void);
	static obj_t BGl_z62rtl_inszf2bbvzd2funzd2setz12z82zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockSzd2z52markzd2setz12z22zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2argszd2setz12z82zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_funzd2costzb0zzsaw_bbvzd2costzd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62blockSzd2gcmarkzb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2queuezd2lastz00zzsaw_bbvzd2costzd2(BgL_bbvzd2queuezd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2mergeszd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62blockVzd2z52markzd2setz12z22zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2outz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt);
	extern obj_t BGl_regsetz00zzsaw_regsetz00;
	static obj_t BGl_z62blockSzd2predszb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2outz42zzsaw_bbvzd2costzd2(obj_t, obj_t);
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	static obj_t BGl_z62rtl_funzd2cost1444zb0zzsaw_bbvzd2costzd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2funzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt,
		BgL_rtl_funz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2queuezd2blocksz00zzsaw_bbvzd2costzd2(BgL_bbvzd2queuezd2_bglt);
	static obj_t BGl_z62blockVzd2mergezd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockSzd2mblockzd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_inszf2bbvzf3z01zzsaw_bbvzd2costzd2(obj_t);
	static obj_t BGl_z62blockSzd2asleepzd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockVzf3z91zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_bbvzd2queuezf3z21zzsaw_bbvzd2costzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2z52hashzd2setz12z40zzsaw_bbvzd2costzd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2valuez00zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	static BgL_bbvzd2ctxentryzd2_bglt
		BGl_z62makezd2bbvzd2ctxentryz62zzsaw_bbvzd2costzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_blockSzd2ctxzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	static obj_t
		BGl_z62bbvzd2ctxentryzd2countzd2setz12za2zzsaw_bbvzd2costzd2(obj_t, obj_t,
		obj_t);
	extern obj_t
		BGl_rtl_callzd2predicatezd2zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2succszd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	static obj_t BGl_z62blockVzd2succszb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_bbvzd2costzd2(void);
	static obj_t BGl_z62rtl_inszd2costzb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62blockSzd2mblockzb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62blockVzd2genericzd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2genericzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL long
		BGl_bbvzd2ctxzd2idz00zzsaw_bbvzd2costzd2(BgL_bbvzd2ctxzd2_bglt);
	static obj_t BGl_z62bbvzd2queuezf3z43zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_blockSzd2parentzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxentryzd2_bglt
		BGl_bbvzd2ctxentryzd2nilz00zzsaw_bbvzd2costzd2(void);
	static obj_t
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2costzd2(obj_t, obj_t,
		obj_t);
	static BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_bbvzd2ctxzd2_bglt BGl_z62blockSzd2ctxzb0zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt);
	extern obj_t BGl_za2ze3zd3flza2z30zzsaw_bbvzd2cachezd2;
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2costzd2(void);
	static BgL_typez00_bglt
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_makezd2blockSzd2zzsaw_bbvzd2costzd2(int, obj_t, obj_t, obj_t, long,
		obj_t, obj_t, BgL_bbvzd2ctxzd2_bglt, BgL_blockz00_bglt, long, long, obj_t,
		obj_t, obj_t, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxzd2idzd2setz12zc0zzsaw_bbvzd2costzd2(BgL_bbvzd2ctxzd2_bglt,
		long);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2predszd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_makezd2blockVzd2zzsaw_bbvzd2costzd2(int, obj_t, obj_t, obj_t, obj_t,
		obj_t, long, obj_t);
	extern obj_t BGl_za2zc3fxza2zc3zzsaw_bbvzd2cachezd2;
	static obj_t BGl_z62bbvzd2ctxzd2idzd2setz12za2zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2zb2flza2zb2zzsaw_bbvzd2cachezd2;
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2queuezd2blockszd2setz12zc0zzsaw_bbvzd2costzd2
		(BgL_bbvzd2queuezd2_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_bbvzd2ctxzf3z21zzsaw_bbvzd2costzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2destzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt,
		obj_t);
	static BgL_bbvzd2queuezd2_bglt
		BGl_z62makezd2bbvzd2queuez62zzsaw_bbvzd2costzd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2z52blacklistzd2setz12z40zzsaw_bbvzd2costzd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_blockSzd2asleepzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	extern obj_t BGl_rtl_ifnez00zzsaw_defsz00;
	static obj_t BGl_z62bbvzd2ctxzd2idz62zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62blockSzd2gccntzd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzd2aliasesz62zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_zb2zb2zz__r4_numbers_6_5z00(obj_t);
	static obj_t
		BGl_z62bbvzd2ctxentryzd2initvalzd2setz12za2zzsaw_bbvzd2costzd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62blockSzd2firstzb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_blockSzd2gccntzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	static obj_t BGl_z62bbvzd2queuezd2blocksz62zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2costzd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_rtl_inszf2bbvzd2ctxz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt);
	static BgL_bbvzd2ctxzd2_bglt
		BGl_z62rtl_inszf2bbvzd2ctxz42zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_bbvzd2ctxentryzf3z21zzsaw_bbvzd2costzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2z52blacklistz80zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	extern obj_t BGl_za2zd3fxza2zd3zzsaw_bbvzd2cachezd2;
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_bbvzd2ctxentryzd2regz00zzsaw_bbvzd2costzd2(BgL_bbvzd2ctxentryzd2_bglt);
	extern obj_t BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2;
	static obj_t
		BGl_z62bbvzd2ctxentryzd2aliaseszd2setz12za2zzsaw_bbvzd2costzd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2aliasesz00zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	static obj_t BGl_z62rtl_inszf2bbvzd2defzd2setz12z82zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2zc3zd3fxza2z10zzsaw_bbvzd2cachezd2;
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2firstzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt, obj_t);
	static obj_t BGl_z62blockSzd2mergeszd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_insz00_bglt
		BGl_makezd2rtl_inszf2bbvz20zzsaw_bbvzd2costzd2(obj_t, obj_t, obj_t,
		BgL_rtl_funz00_bglt, obj_t, obj_t, obj_t, obj_t, BgL_bbvzd2ctxzd2_bglt,
		obj_t);
	static obj_t BGl_z62bbvzd2queuezd2blockszd2setz12za2zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2costzd2(BgL_regsetz00_bglt);
	extern obj_t BGl_blockSz00zzsaw_bbvzd2typeszd2;
	static obj_t BGl_z62blockSzd2z52markze2zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62blockSzd2creatorzb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2destzd2setz12z82zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_blockVz00zzsaw_bbvzd2typeszd2;
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2versionszd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2costzd2(BgL_regsetz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL int
		BGl_blockSzd2labelzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	static obj_t BGl_z62bbvzd2queuezd2lastzd2setz12za2zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockVzd2versionszb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2firstzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt, obj_t);
	static obj_t BGl_z62blockVzd2labelzb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2z52spillz72zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2defzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2queuezd2_bglt
		BGl_bbvzd2queuezd2nilz00zzsaw_bbvzd2costzd2(void);
	static BgL_bbvzd2ctxentryzd2_bglt
		BGl_z62bbvzd2ctxentryzd2nilz62zzsaw_bbvzd2costzd2(obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2z52hashz10zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2initvalzd2setz12zc0zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxentryzd2_bglt, obj_t);
	extern obj_t BGl_za2ze3fxza2ze3zzsaw_bbvzd2cachezd2;
	extern obj_t BGl_za2zd2flza2zd2zzsaw_bbvzd2cachezd2;
	static obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2firstzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL BgL_bbvzd2queuezd2_bglt
		BGl_makezd2bbvzd2queuez00zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2costzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2utilszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_EXPORTED_DECL long
		BGl_bbvzd2ctxentryzd2countz00zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	BGL_EXPORTED_DECL long
		BGl_blockSzd2gcmarkzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2genericzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_bbvzd2ctxzd2nilz00zzsaw_bbvzd2costzd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2aliaseszd2setz12zc0zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxentryzd2_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_blockSzd2nilzd2zzsaw_bbvzd2costzd2(void);
	static BgL_blockz00_bglt BGl_z62makezd2blockSzb0zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzd2typesz62zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2costzd2(obj_t);
	static BgL_blockz00_bglt BGl_z62makezd2blockVzb0zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2z52hashz80zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2z52spillzd2setz12zd0zzsaw_bbvzd2costzd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL long
		BGl_blockVzd2z52markz80zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	static obj_t
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2costzd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2inzd2setz12z82zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2loczd2setz12z82zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_funzd2costzd2rtl_ifn1448z62zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockSzd2z52hashzd2setz12z22zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt,
		obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62bbvzd2ctxentryzd2regz62zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_funz00_bglt
		BGl_rtl_inszf2bbvzd2funz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2costzd2(void);
	BGL_EXPORTED_DECL bool_t BGl_blockSzf3zf3zzsaw_bbvzd2costzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2mergezd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	static obj_t BGl_z62blockSzd2gcmarkzd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static BgL_rtl_funz00_bglt
		BGl_z62rtl_inszf2bbvzd2funz42zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_makezd2bbvzd2ctxz00zzsaw_bbvzd2costzd2(long, obj_t);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2costzd2(void);
	extern obj_t BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2;
	static obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2costzd2(void);
	static BgL_blockz00_bglt BGl_z62blockSzd2nilzb0zzsaw_bbvzd2costzd2(obj_t);
	static BgL_rtl_insz00_bglt
		BGl_z62makezd2rtl_inszf2bbvz42zzsaw_bbvzd2costzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2creatorzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2mblockzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2asleepzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2destz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2mblockzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2succszd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt, obj_t);
	static obj_t
		BGl_z62blockSzd2z52blacklistzd2setz12z22zzsaw_bbvzd2costzd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzf3z63zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2loczd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2costzd2
		(BgL_rtl_regz00_bglt, int);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2predszd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2succszd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt, obj_t);
	extern obj_t BGl_za2ze3zd3fxza2z30zzsaw_bbvzd2cachezd2;
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62blockSzd2succszb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2z52hashzd2setz12zb2zzsaw_bbvzd2costzd2
		(BgL_rtl_insz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2costzd2
		(BgL_rtl_regz00_bglt, obj_t);
	extern obj_t BGl_za2zb2fxza2zb2zzsaw_bbvzd2cachezd2;
	static obj_t BGl_rtl_funzd2costzd2zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_blockVzf3zf3zzsaw_bbvzd2costzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2labelzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt, int);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2predszd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2queuezd2lastzd2setz12zc0zzsaw_bbvzd2costzd2
		(BgL_bbvzd2queuezd2_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_regsetzf3zf3zzsaw_bbvzd2costzd2(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_insz00_bglt
		BGl_rtl_inszf2bbvzd2nilz20zzsaw_bbvzd2costzd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2countzd2setz12zc0zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxentryzd2_bglt, long);
	static BgL_rtl_insz00_bglt
		BGl_z62rtl_inszf2bbvzd2nilz42zzsaw_bbvzd2costzd2(obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2costzd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2labelzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt, int);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2predszd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2costzd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62blockVzd2predszb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2z52markzd2setz12z40zzsaw_bbvzd2costzd2(BgL_blockz00_bglt,
		long);
	static obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_bbvzd2ctxzd2_bglt
		BGl_z62bbvzd2ctxzd2nilz62zzsaw_bbvzd2costzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2costzd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_z62blockSzd2z52blacklistze2zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2locz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2costzd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_z62blockSzd2mergeszb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2locz42zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzd2valuez62zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2z52markzd2setz12z40zzsaw_bbvzd2costzd2(BgL_blockz00_bglt,
		long);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2costzd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2argsz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2succszd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt);
	extern obj_t BGl_rtl_regzf2razf2zzsaw_regsetz00;
	static obj_t BGl_z62blockSzd2firstzd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2mergezd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62bbvzd2ctxzd2entrieszd2setz12za2zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxzd2entriesz00zzsaw_bbvzd2costzd2(BgL_bbvzd2ctxzd2_bglt);
	static obj_t BGl_z62blockSzd2gccntzb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_rtl_inszd2costzd2zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2costzd2(BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2inzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt,
		obj_t);
	static obj_t BGl_z62bbvzd2ctxzd2entriesz62zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2destz42zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2costzd2(BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxentryzd2_bglt
		BGl_makezd2bbvzd2ctxentryz00zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt, obj_t,
		bool_t, long, obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockVzd2firstzd2setz12z70zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2ctxzd2setz12z82zzsaw_bbvzd2costzd2(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2costzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt);
	extern obj_t BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2;
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2mergeszd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt,
		obj_t);
	extern obj_t BGl_blockz00zzsaw_defsz00;
	static obj_t BGl_z62rtl_inszf2bbvzd2inz42zzsaw_bbvzd2costzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2costzd2(BgL_regsetz00_bglt, int);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxzd2entrieszd2setz12zc0zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxzd2_bglt, obj_t);
	extern obj_t BGl_za2zd2fxza2zd2zzsaw_bbvzd2cachezd2;
	static obj_t BGl_z62blockSzd2labelzb0zzsaw_bbvzd2costzd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2costzd2(obj_t,
		obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2asleepzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2asle1960z00,
		BGl_z62blockSzd2asleepzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52hashzd2setz12zd2envz92zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2za752h1961za7,
		BGl_z62blockSzd2z52hashzd2setz12z22zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2nilzd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2queueza7d1962za7,
		BGl_z62bbvzd2queuezd2nilz62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2namezd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza71963za7,
		BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2argszd2setz12zd2envz32zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv1964z00,
		BGl_z62rtl_inszf2bbvzd2argszd2setz12z82zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2creatorzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2crea1965z00,
		BGl_z62blockSzd2creatorzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2mblockzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2mblo1966z00,
		BGl_z62blockSzd2mblockzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2nilzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762regsetza7d2nilza71967za7,
		BGl_z62regsetzd2nilzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2predszd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2pred1968z00,
		BGl_z62blockSzd2predszd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2inzd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv1969z00,
		BGl_z62rtl_inszf2bbvzd2inz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2polarityzd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxentr1970z00,
		BGl_z62bbvzd2ctxentryzd2polarityz62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2firstzd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2firs1971z00,
		BGl_z62blockSzd2firstzd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockVzd2nilzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2nilza71972za7,
		BGl_z62blockVzd2nilzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52hashzd2envz52zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2za752h1973za7,
		BGl_z62blockSzd2z52hashze2zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2blockszd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2queueza7d1974za7,
		BGl_z62bbvzd2queuezd2blocksz62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2setz12zd2envz32zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza71975za7,
		BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2inzd2setz12zd2envz32zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv1976z00,
		BGl_z62rtl_inszf2bbvzd2inzd2setz12z82zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza71977za7,
		BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2genericzd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2gene1978z00,
		BGl_z62blockVzd2genericzd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2setz12zd2envzc1zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza71979za7,
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2costzd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2setz12zd2envz32zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza71980za7,
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2costzd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2ctxzd2setz12zd2envz32zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv1981z00,
		BGl_z62rtl_inszf2bbvzd2ctxzd2setz12z82zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza71982za7,
		BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2z52hashzd2setz12zd2envz60zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv1983z00,
		BGl_z62rtl_inszf2bbvzd2z52hashzd2setz12zd0zzsaw_bbvzd2costzd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2setz12zd2envz32zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza71984za7,
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2costzd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2versionszd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2vers1985z00,
		BGl_z62blockVzd2versionszd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2regvzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762regsetza7d2regv1986z00,
		BGl_z62regsetzd2regvzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2bbvzd2ctxentryzd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762makeza7d2bbvza7d21987za7,
		BGl_z62makezd2bbvzd2ctxentryz62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2nilzd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv1988z00,
		BGl_z62rtl_inszf2bbvzd2nilz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzf3zd2envz21zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7f3za791za71989z00,
		BGl_z62blockSzf3z91zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2initvalzd2setz12zd2envz12zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxentr1990z00,
		BGl_z62bbvzd2ctxentryzd2initvalzd2setz12za2zzsaw_bbvzd2costzd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2typeszd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxentr1991z00,
		BGl_z62bbvzd2ctxentryzd2typesz62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762regsetza7d2leng1992z00,
		BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockVzd2firstzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2firs1993z00,
		BGl_z62blockVzd2firstzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2mblockzd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2mblo1994z00,
		BGl_z62blockSzd2mblockzd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2countzd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxentr1995z00,
		BGl_z62bbvzd2ctxentryzd2countz62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2msiza7ezd2envza7zzsaw_bbvzd2costzd2,
		BgL_bgl_za762regsetza7d2msiza71996za7,
		BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bbvzd2ctxzd2nilzd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxza7d2n1997za7,
		BGl_z62bbvzd2ctxzd2nilz62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzf3zd2envzf3zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxentr1998z00,
		BGl_z62bbvzd2ctxentryzf3z43zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bbvzd2ctxzd2idzd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxza7d2i1999za7,
		BGl_z62bbvzd2ctxzd2idz62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2aliaseszd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxentr2000z00,
		BGl_z62bbvzd2ctxentryzd2aliasesz62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2nilzd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxentr2001z00,
		BGl_z62bbvzd2ctxentryzd2nilz62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2gcmarkzd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2gcma2002z00,
		BGl_z62blockSzd2gcmarkzd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza72003za7,
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2setz12zd2envz32zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza72004za7,
		BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2outzd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2005z00,
		BGl_z62rtl_inszf2bbvzd2outz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2nilzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2nilza72006za7,
		BGl_z62blockSzd2nilzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2rtl_inszf2bbvzd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762makeza7d2rtl_in2007z00,
		BGl_z62makezd2rtl_inszf2bbvz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2firstzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2firs2008z00,
		BGl_z62blockSzd2firstzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza72009za7,
		BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2z52markzd2envz52zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2za752m2010za7,
		BGl_z62blockVzd2z52markze2zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2labelzd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2labe2011z00,
		BGl_z62blockSzd2labelzd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2versionszd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2vers2012z00,
		BGl_z62blockVzd2versionszb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2z52hashzd2envza0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2013z00,
		BGl_z62rtl_inszf2bbvzd2z52hashz10zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockVzd2mergezd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2merg2014z00,
		BGl_z62blockVzd2mergezb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52markzd2setz12zd2envz92zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2za752m2015za7,
		BGl_z62blockSzd2z52markzd2setz12z22zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2bbvzd2queuezd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762makeza7d2bbvza7d22016za7,
		BGl_z62makezd2bbvzd2queuez62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52markzd2envz52zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2za752m2017za7,
		BGl_z62blockSzd2z52markze2zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2ctxzd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2018z00,
		BGl_z62rtl_inszf2bbvzd2ctxz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2succszd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2succ2019z00,
		BGl_z62blockSzd2succszd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2nilzd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza72020za7,
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza72021za7,
		BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2blockSzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762makeza7d2blocks2022z00,
		BGl_z62makezd2blockSzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2numzd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza72023za7,
		BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2blockszd2setz12zd2envz12zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2queueza7d2024za7,
		BGl_z62bbvzd2queuezd2blockszd2setz12za2zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2entrieszd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxza7d2e2025za7,
		BGl_z62bbvzd2ctxzd2entriesz62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2predszd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2pred2026z00,
		BGl_z62blockVzd2predszd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762regsetza7d2stri2027z00,
		BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2firstzd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2firs2028z00,
		BGl_z62blockVzd2firstzd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2valuezd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxentr2029z00,
		BGl_z62bbvzd2ctxentryzd2valuez62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2gccntzd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2gccn2030z00,
		BGl_z62blockSzd2gccntzd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1956z00zzsaw_bbvzd2costzd2,
		BgL_bgl_string1956za700za7za7s2031za7, "rtl_fun-cost1444", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2setz12zd2envz32zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza72032za7,
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1958z00zzsaw_bbvzd2costzd2,
		BgL_bgl_string1958za700za7za7s2033za7, "rtl_fun-cost", 12);
	      DEFINE_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2,
		BgL_bgl_string1959za700za7za7s2034za7, "saw_bbv-cost", 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2envz01zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza72035za7,
		BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2ctxzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2ctxza72036za7,
		BGl_z62blockSzd2ctxzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2outzd2setz12zd2envz32zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2037z00,
		BGl_z62rtl_inszf2bbvzd2outzd2setz12z82zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2stringzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762regsetza7d2stri2038z00,
		BGl_z62regsetzd2stringzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1955z00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_funza7d2cos2039z00,
		BGl_z62rtl_funzd2cost1444zb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1957z00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_funza7d2cos2040z00,
		BGl_z62rtl_funzd2costzd2rtl_ifn1448z62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2asleepzd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2asle2041z00,
		BGl_z62blockSzd2asleepzd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2gcmarkzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2gcma2042z00,
		BGl_z62blockSzd2gcmarkzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52blacklistzd2envz52zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2za752b2043za7,
		BGl_z62blockSzd2z52blacklistze2zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2setz12zd2envz32zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza72044za7,
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2costzd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2rtl_regzf2razd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762makeza7d2rtl_re2045z00,
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2argszd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2046z00,
		BGl_z62rtl_inszf2bbvzd2argsz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2idzd2setz12zd2envz12zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxza7d2i2047za7,
		BGl_z62bbvzd2ctxzd2idzd2setz12za2zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razf3zd2envzd3zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza72048za7,
		BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2z52spillzd2setz12zd2envz60zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2049z00,
		BGl_z62rtl_inszf2bbvzd2z52spillzd2setz12zd0zzsaw_bbvzd2costzd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2lengthzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762regsetza7d2leng2050z00,
		BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzf3zd2envzd3zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2051z00,
		BGl_z62rtl_inszf2bbvzf3z63zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2lastzd2setz12zd2envz12zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2queueza7d2052za7,
		BGl_z62bbvzd2queuezd2lastzd2setz12za2zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bbvzd2queuezf3zd2envzf3zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2queueza7f2053za7,
		BGl_z62bbvzd2queuezf3z43zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2initvalzd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxentr2054z00,
		BGl_z62bbvzd2ctxentryzd2initvalz62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza72055za7,
		BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza72056za7,
		BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2gccntzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2gccn2057z00,
		BGl_z62blockSzd2gccntzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2genericzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2gene2058z00,
		BGl_z62blockVzd2genericzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2hardwarezd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza72059za7,
		BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2loczd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2060z00,
		BGl_z62rtl_inszf2bbvzd2locz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2aliaseszd2setz12zd2envz12zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxentr2061z00,
		BGl_z62bbvzd2ctxentryzd2aliaseszd2setz12za2zzsaw_bbvzd2costzd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2regsetzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762makeza7d2regset2062z00,
		BGl_z62makezd2regsetzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockVzd2succszd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2succ2063z00,
		BGl_z62blockVzd2succszb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2funzd2setz12zd2envz32zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2064z00,
		BGl_z62rtl_inszf2bbvzd2funzd2setz12z82zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2destzd2setz12zd2envz32zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2065z00,
		BGl_z62rtl_inszf2bbvzd2destzd2setz12z82zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_rtl_funzd2costzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_funza7d2cos2066z00,
		BGl_z62rtl_funzd2costzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockVzf3zd2envz21zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7f3za791za72067z00,
		BGl_z62blockVzf3z91zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52blacklistzd2setz12zd2envz92zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2za752b2068za7,
		BGl_z62blockSzd2z52blacklistzd2setz12z22zzsaw_bbvzd2costzd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2countzd2setz12zd2envz12zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxentr2069z00,
		BGl_z62bbvzd2ctxentryzd2countzd2setz12za2zzsaw_bbvzd2costzd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2z52spillzd2envza0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2070z00,
		BGl_z62rtl_inszf2bbvzd2z52spillz10zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2defzd2setz12zd2envz32zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2071z00,
		BGl_z62rtl_inszf2bbvzd2defzd2setz12z82zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2bbvzd2ctxzd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762makeza7d2bbvza7d22072za7,
		BGl_z62makezd2bbvzd2ctxz62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2labelzd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2labe2073z00,
		BGl_z62blockVzd2labelzd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2succszd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2succ2074z00,
		BGl_z62blockSzd2succszb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2entrieszd2setz12zd2envz12zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxza7d2e2075za7,
		BGl_z62bbvzd2ctxzd2entrieszd2setz12za2zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2destzd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2076z00,
		BGl_z62rtl_inszf2bbvzd2destz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2loczd2setz12zd2envz32zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2077z00,
		BGl_z62rtl_inszf2bbvzd2loczd2setz12z82zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2lastzd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2queueza7d2078za7,
		BGl_z62bbvzd2queuezd2lastz62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockVzd2labelzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2labe2079z00,
		BGl_z62blockVzd2labelzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2regzd2envzd2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxentr2080z00,
		BGl_z62bbvzd2ctxentryzd2regz62zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2z52markzd2setz12zd2envz92zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2za752m2081za7,
		BGl_z62blockVzd2z52markzd2setz12z22zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2succszd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2succ2082z00,
		BGl_z62blockVzd2succszd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockVzd2predszd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2pred2083z00,
		BGl_z62blockVzd2predszb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2blockVzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762makeza7d2blockv2084z00,
		BGl_z62makezd2blockVzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2mergezd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blockvza7d2merg2085z00,
		BGl_z62blockVzd2mergezd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzf3zd2envz21zzsaw_bbvzd2costzd2,
		BgL_bgl_za762regsetza7f3za791za72086z00,
		BGl_z62regsetzf3z91zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2labelzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2labe2087z00,
		BGl_z62blockSzd2labelzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2funzd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2088z00,
		BGl_z62rtl_inszf2bbvzd2funz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2keyzd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza72089za7,
		BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2mergeszd2setz12zd2envzc0zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2merg2090z00,
		BGl_z62blockSzd2mergeszd2setz12z70zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2predszd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2pred2091z00,
		BGl_z62blockSzd2predszb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2mergeszd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2merg2092z00,
		BGl_z62blockSzd2mergeszb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2parentzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762blocksza7d2pare2093z00,
		BGl_z62blockSzd2parentzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2setz12zd2envz32zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_regza7f2raza72094za7,
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2costzd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bbvzd2ctxzf3zd2envzf3zzsaw_bbvzd2costzd2,
		BgL_bgl_za762bbvza7d2ctxza7f3za72095z00,
		BGl_z62bbvzd2ctxzf3z43zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2defzd2envzf2zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7f2bbv2096z00,
		BGl_z62rtl_inszf2bbvzd2defz42zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_inszd2costzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762rtl_insza7d2cos2097z00,
		BGl_z62rtl_inszd2costzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2reglzd2envz00zzsaw_bbvzd2costzd2,
		BgL_bgl_za762regsetza7d2regl2098z00,
		BGl_z62regsetzd2reglzb0zzsaw_bbvzd2costzd2, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_bbvzd2costzd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2costzd2(long
		BgL_checksumz00_3832, char *BgL_fromz00_3833)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_bbvzd2costzd2))
				{
					BGl_requirezd2initializa7ationz75zzsaw_bbvzd2costzd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_bbvzd2costzd2();
					BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2costzd2();
					BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2costzd2();
					BGl_genericzd2initzd2zzsaw_bbvzd2costzd2();
					BGl_methodzd2initzd2zzsaw_bbvzd2costzd2();
					return BGl_toplevelzd2initzd2zzsaw_bbvzd2costzd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2costzd2(void)
	{
		{	/* SawBbv/bbv-cost.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_bbv-cost");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "saw_bbv-cost");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_bbv-cost");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_bbv-cost");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_bbv-cost");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_bbv-cost");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_bbv-cost");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "saw_bbv-cost");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_bbv-cost");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2costzd2(void)
	{
		{	/* SawBbv/bbv-cost.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2costzd2(void)
	{
		{	/* SawBbv/bbv-cost.scm 15 */
			return BUNSPEC;
		}

	}



/* make-rtl_reg/ra */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2costzd2(BgL_typez00_bglt
		BgL_type1179z00_3, obj_t BgL_var1180z00_4, obj_t BgL_onexprzf31181zf3_5,
		obj_t BgL_name1182z00_6, obj_t BgL_key1183z00_7,
		obj_t BgL_hardware1184z00_8, int BgL_num1185z00_9,
		obj_t BgL_color1186z00_10, obj_t BgL_coalesce1187z00_11,
		int BgL_occurrences1188z00_12, obj_t BgL_interfere1189z00_13,
		obj_t BgL_interfere21190z00_14)
	{
		{	/* SawMill/regset.sch 55 */
			{	/* SawMill/regset.sch 55 */
				BgL_rtl_regz00_bglt BgL_new1166z00_3614;

				{	/* SawMill/regset.sch 55 */
					BgL_rtl_regz00_bglt BgL_tmp1164z00_3615;
					BgL_rtl_regzf2razf2_bglt BgL_wide1165z00_3616;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_3853;

						{	/* SawMill/regset.sch 55 */
							BgL_rtl_regz00_bglt BgL_new1163z00_3617;

							BgL_new1163z00_3617 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/regset.sch 55 */
								long BgL_arg1473z00_3618;

								BgL_arg1473z00_3618 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1163z00_3617),
									BgL_arg1473z00_3618);
							}
							{	/* SawMill/regset.sch 55 */
								BgL_objectz00_bglt BgL_tmpz00_3858;

								BgL_tmpz00_3858 = ((BgL_objectz00_bglt) BgL_new1163z00_3617);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3858, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1163z00_3617);
							BgL_auxz00_3853 = BgL_new1163z00_3617;
						}
						BgL_tmp1164z00_3615 = ((BgL_rtl_regz00_bglt) BgL_auxz00_3853);
					}
					BgL_wide1165z00_3616 =
						((BgL_rtl_regzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regzf2razf2_bgl))));
					{	/* SawMill/regset.sch 55 */
						obj_t BgL_auxz00_3866;
						BgL_objectz00_bglt BgL_tmpz00_3864;

						BgL_auxz00_3866 = ((obj_t) BgL_wide1165z00_3616);
						BgL_tmpz00_3864 = ((BgL_objectz00_bglt) BgL_tmp1164z00_3615);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3864, BgL_auxz00_3866);
					}
					((BgL_objectz00_bglt) BgL_tmp1164z00_3615);
					{	/* SawMill/regset.sch 55 */
						long BgL_arg1472z00_3619;

						BgL_arg1472z00_3619 =
							BGL_CLASS_NUM(BGl_rtl_regzf2razf2zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1164z00_3615), BgL_arg1472z00_3619);
					}
					BgL_new1166z00_3614 = ((BgL_rtl_regz00_bglt) BgL_tmp1164z00_3615);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1166z00_3614)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1179z00_3), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3614)))->BgL_varz00) =
					((obj_t) BgL_var1180z00_4), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3614)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31181zf3_5), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3614)))->BgL_namez00) =
					((obj_t) BgL_name1182z00_6), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3614)))->BgL_keyz00) =
					((obj_t) BgL_key1183z00_7), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3614)))->BgL_debugnamez00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3614)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1184z00_8), BUNSPEC);
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3888;

					{
						obj_t BgL_auxz00_3889;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3890;

							BgL_tmpz00_3890 = ((BgL_objectz00_bglt) BgL_new1166z00_3614);
							BgL_auxz00_3889 = BGL_OBJECT_WIDENING(BgL_tmpz00_3890);
						}
						BgL_auxz00_3888 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3889);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3888))->BgL_numz00) =
						((int) BgL_num1185z00_9), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3895;

					{
						obj_t BgL_auxz00_3896;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3897;

							BgL_tmpz00_3897 = ((BgL_objectz00_bglt) BgL_new1166z00_3614);
							BgL_auxz00_3896 = BGL_OBJECT_WIDENING(BgL_tmpz00_3897);
						}
						BgL_auxz00_3895 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3896);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3895))->
							BgL_colorz00) = ((obj_t) BgL_color1186z00_10), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3902;

					{
						obj_t BgL_auxz00_3903;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3904;

							BgL_tmpz00_3904 = ((BgL_objectz00_bglt) BgL_new1166z00_3614);
							BgL_auxz00_3903 = BGL_OBJECT_WIDENING(BgL_tmpz00_3904);
						}
						BgL_auxz00_3902 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3903);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3902))->
							BgL_coalescez00) = ((obj_t) BgL_coalesce1187z00_11), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3909;

					{
						obj_t BgL_auxz00_3910;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3911;

							BgL_tmpz00_3911 = ((BgL_objectz00_bglt) BgL_new1166z00_3614);
							BgL_auxz00_3910 = BGL_OBJECT_WIDENING(BgL_tmpz00_3911);
						}
						BgL_auxz00_3909 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3910);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3909))->
							BgL_occurrencesz00) = ((int) BgL_occurrences1188z00_12), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3916;

					{
						obj_t BgL_auxz00_3917;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3918;

							BgL_tmpz00_3918 = ((BgL_objectz00_bglt) BgL_new1166z00_3614);
							BgL_auxz00_3917 = BGL_OBJECT_WIDENING(BgL_tmpz00_3918);
						}
						BgL_auxz00_3916 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3917);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3916))->
							BgL_interferez00) = ((obj_t) BgL_interfere1189z00_13), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3923;

					{
						obj_t BgL_auxz00_3924;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3925;

							BgL_tmpz00_3925 = ((BgL_objectz00_bglt) BgL_new1166z00_3614);
							BgL_auxz00_3924 = BGL_OBJECT_WIDENING(BgL_tmpz00_3925);
						}
						BgL_auxz00_3923 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3924);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3923))->
							BgL_interfere2z00) = ((obj_t) BgL_interfere21190z00_14), BUNSPEC);
				}
				return BgL_new1166z00_3614;
			}
		}

	}



/* &make-rtl_reg/ra */
	BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3244, obj_t BgL_type1179z00_3245, obj_t BgL_var1180z00_3246,
		obj_t BgL_onexprzf31181zf3_3247, obj_t BgL_name1182z00_3248,
		obj_t BgL_key1183z00_3249, obj_t BgL_hardware1184z00_3250,
		obj_t BgL_num1185z00_3251, obj_t BgL_color1186z00_3252,
		obj_t BgL_coalesce1187z00_3253, obj_t BgL_occurrences1188z00_3254,
		obj_t BgL_interfere1189z00_3255, obj_t BgL_interfere21190z00_3256)
	{
		{	/* SawMill/regset.sch 55 */
			return
				BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2costzd2(
				((BgL_typez00_bglt) BgL_type1179z00_3245), BgL_var1180z00_3246,
				BgL_onexprzf31181zf3_3247, BgL_name1182z00_3248, BgL_key1183z00_3249,
				BgL_hardware1184z00_3250, CINT(BgL_num1185z00_3251),
				BgL_color1186z00_3252, BgL_coalesce1187z00_3253,
				CINT(BgL_occurrences1188z00_3254), BgL_interfere1189z00_3255,
				BgL_interfere21190z00_3256);
		}

	}



/* rtl_reg/ra? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2costzd2(obj_t
		BgL_objz00_15)
	{
		{	/* SawMill/regset.sch 56 */
			{	/* SawMill/regset.sch 56 */
				obj_t BgL_classz00_3620;

				BgL_classz00_3620 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_15))
					{	/* SawMill/regset.sch 56 */
						BgL_objectz00_bglt BgL_arg1807z00_3621;

						BgL_arg1807z00_3621 = (BgL_objectz00_bglt) (BgL_objz00_15);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 56 */
								long BgL_idxz00_3622;

								BgL_idxz00_3622 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3621);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3622 + 2L)) == BgL_classz00_3620);
							}
						else
							{	/* SawMill/regset.sch 56 */
								bool_t BgL_res1940z00_3625;

								{	/* SawMill/regset.sch 56 */
									obj_t BgL_oclassz00_3626;

									{	/* SawMill/regset.sch 56 */
										obj_t BgL_arg1815z00_3627;
										long BgL_arg1816z00_3628;

										BgL_arg1815z00_3627 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 56 */
											long BgL_arg1817z00_3629;

											BgL_arg1817z00_3629 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3621);
											BgL_arg1816z00_3628 = (BgL_arg1817z00_3629 - OBJECT_TYPE);
										}
										BgL_oclassz00_3626 =
											VECTOR_REF(BgL_arg1815z00_3627, BgL_arg1816z00_3628);
									}
									{	/* SawMill/regset.sch 56 */
										bool_t BgL__ortest_1115z00_3630;

										BgL__ortest_1115z00_3630 =
											(BgL_classz00_3620 == BgL_oclassz00_3626);
										if (BgL__ortest_1115z00_3630)
											{	/* SawMill/regset.sch 56 */
												BgL_res1940z00_3625 = BgL__ortest_1115z00_3630;
											}
										else
											{	/* SawMill/regset.sch 56 */
												long BgL_odepthz00_3631;

												{	/* SawMill/regset.sch 56 */
													obj_t BgL_arg1804z00_3632;

													BgL_arg1804z00_3632 = (BgL_oclassz00_3626);
													BgL_odepthz00_3631 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3632);
												}
												if ((2L < BgL_odepthz00_3631))
													{	/* SawMill/regset.sch 56 */
														obj_t BgL_arg1802z00_3633;

														{	/* SawMill/regset.sch 56 */
															obj_t BgL_arg1803z00_3634;

															BgL_arg1803z00_3634 = (BgL_oclassz00_3626);
															BgL_arg1802z00_3633 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3634,
																2L);
														}
														BgL_res1940z00_3625 =
															(BgL_arg1802z00_3633 == BgL_classz00_3620);
													}
												else
													{	/* SawMill/regset.sch 56 */
														BgL_res1940z00_3625 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1940z00_3625;
							}
					}
				else
					{	/* SawMill/regset.sch 56 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_reg/ra? */
	obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3257,
		obj_t BgL_objz00_3258)
	{
		{	/* SawMill/regset.sch 56 */
			return BBOOL(BGl_rtl_regzf2razf3z01zzsaw_bbvzd2costzd2(BgL_objz00_3258));
		}

	}



/* rtl_reg/ra-nil */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2costzd2(void)
	{
		{	/* SawMill/regset.sch 57 */
			{	/* SawMill/regset.sch 57 */
				obj_t BgL_classz00_2769;

				BgL_classz00_2769 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				{	/* SawMill/regset.sch 57 */
					obj_t BgL__ortest_1117z00_2770;

					BgL__ortest_1117z00_2770 = BGL_CLASS_NIL(BgL_classz00_2769);
					if (CBOOL(BgL__ortest_1117z00_2770))
						{	/* SawMill/regset.sch 57 */
							return ((BgL_rtl_regz00_bglt) BgL__ortest_1117z00_2770);
						}
					else
						{	/* SawMill/regset.sch 57 */
							return
								((BgL_rtl_regz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2769));
						}
				}
			}
		}

	}



/* &rtl_reg/ra-nil */
	BgL_rtl_regz00_bglt BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3259)
	{
		{	/* SawMill/regset.sch 57 */
			return BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2costzd2();
		}

	}



/* rtl_reg/ra-interfere2 */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_oz00_16)
	{
		{	/* SawMill/regset.sch 58 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3965;

				{
					obj_t BgL_auxz00_3966;

					{	/* SawMill/regset.sch 58 */
						BgL_objectz00_bglt BgL_tmpz00_3967;

						BgL_tmpz00_3967 = ((BgL_objectz00_bglt) BgL_oz00_16);
						BgL_auxz00_3966 = BGL_OBJECT_WIDENING(BgL_tmpz00_3967);
					}
					BgL_auxz00_3965 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3966);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3965))->
					BgL_interfere2z00);
			}
		}

	}



/* &rtl_reg/ra-interfere2 */
	obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3260, obj_t BgL_oz00_3261)
	{
		{	/* SawMill/regset.sch 58 */
			return
				BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3261));
		}

	}



/* rtl_reg/ra-interfere2-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2costzd2
		(BgL_rtl_regz00_bglt BgL_oz00_17, obj_t BgL_vz00_18)
	{
		{	/* SawMill/regset.sch 59 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3974;

				{
					obj_t BgL_auxz00_3975;

					{	/* SawMill/regset.sch 59 */
						BgL_objectz00_bglt BgL_tmpz00_3976;

						BgL_tmpz00_3976 = ((BgL_objectz00_bglt) BgL_oz00_17);
						BgL_auxz00_3975 = BGL_OBJECT_WIDENING(BgL_tmpz00_3976);
					}
					BgL_auxz00_3974 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3975);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3974))->
						BgL_interfere2z00) = ((obj_t) BgL_vz00_18), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere2-set! */
	obj_t BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3262, obj_t BgL_oz00_3263, obj_t BgL_vz00_3264)
	{
		{	/* SawMill/regset.sch 59 */
			return
				BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3263), BgL_vz00_3264);
		}

	}



/* rtl_reg/ra-interfere */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_oz00_19)
	{
		{	/* SawMill/regset.sch 60 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3983;

				{
					obj_t BgL_auxz00_3984;

					{	/* SawMill/regset.sch 60 */
						BgL_objectz00_bglt BgL_tmpz00_3985;

						BgL_tmpz00_3985 = ((BgL_objectz00_bglt) BgL_oz00_19);
						BgL_auxz00_3984 = BGL_OBJECT_WIDENING(BgL_tmpz00_3985);
					}
					BgL_auxz00_3983 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3984);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3983))->
					BgL_interferez00);
			}
		}

	}



/* &rtl_reg/ra-interfere */
	obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3265, obj_t BgL_oz00_3266)
	{
		{	/* SawMill/regset.sch 60 */
			return
				BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3266));
		}

	}



/* rtl_reg/ra-interfere-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2costzd2
		(BgL_rtl_regz00_bglt BgL_oz00_20, obj_t BgL_vz00_21)
	{
		{	/* SawMill/regset.sch 61 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3992;

				{
					obj_t BgL_auxz00_3993;

					{	/* SawMill/regset.sch 61 */
						BgL_objectz00_bglt BgL_tmpz00_3994;

						BgL_tmpz00_3994 = ((BgL_objectz00_bglt) BgL_oz00_20);
						BgL_auxz00_3993 = BGL_OBJECT_WIDENING(BgL_tmpz00_3994);
					}
					BgL_auxz00_3992 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3993);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3992))->
						BgL_interferez00) = ((obj_t) BgL_vz00_21), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere-set! */
	obj_t BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3267, obj_t BgL_oz00_3268, obj_t BgL_vz00_3269)
	{
		{	/* SawMill/regset.sch 61 */
			return
				BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3268), BgL_vz00_3269);
		}

	}



/* rtl_reg/ra-occurrences */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_oz00_22)
	{
		{	/* SawMill/regset.sch 62 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4001;

				{
					obj_t BgL_auxz00_4002;

					{	/* SawMill/regset.sch 62 */
						BgL_objectz00_bglt BgL_tmpz00_4003;

						BgL_tmpz00_4003 = ((BgL_objectz00_bglt) BgL_oz00_22);
						BgL_auxz00_4002 = BGL_OBJECT_WIDENING(BgL_tmpz00_4003);
					}
					BgL_auxz00_4001 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4002);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4001))->
					BgL_occurrencesz00);
			}
		}

	}



/* &rtl_reg/ra-occurrences */
	obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3270, obj_t BgL_oz00_3271)
	{
		{	/* SawMill/regset.sch 62 */
			return
				BINT(BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2costzd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_3271)));
		}

	}



/* rtl_reg/ra-occurrences-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2costzd2
		(BgL_rtl_regz00_bglt BgL_oz00_23, int BgL_vz00_24)
	{
		{	/* SawMill/regset.sch 63 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4011;

				{
					obj_t BgL_auxz00_4012;

					{	/* SawMill/regset.sch 63 */
						BgL_objectz00_bglt BgL_tmpz00_4013;

						BgL_tmpz00_4013 = ((BgL_objectz00_bglt) BgL_oz00_23);
						BgL_auxz00_4012 = BGL_OBJECT_WIDENING(BgL_tmpz00_4013);
					}
					BgL_auxz00_4011 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4012);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4011))->
						BgL_occurrencesz00) = ((int) BgL_vz00_24), BUNSPEC);
		}}

	}



/* &rtl_reg/ra-occurrences-set! */
	obj_t BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3272, obj_t BgL_oz00_3273, obj_t BgL_vz00_3274)
	{
		{	/* SawMill/regset.sch 63 */
			return
				BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3273), CINT(BgL_vz00_3274));
		}

	}



/* rtl_reg/ra-coalesce */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_oz00_25)
	{
		{	/* SawMill/regset.sch 64 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4021;

				{
					obj_t BgL_auxz00_4022;

					{	/* SawMill/regset.sch 64 */
						BgL_objectz00_bglt BgL_tmpz00_4023;

						BgL_tmpz00_4023 = ((BgL_objectz00_bglt) BgL_oz00_25);
						BgL_auxz00_4022 = BGL_OBJECT_WIDENING(BgL_tmpz00_4023);
					}
					BgL_auxz00_4021 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4022);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4021))->
					BgL_coalescez00);
			}
		}

	}



/* &rtl_reg/ra-coalesce */
	obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3275, obj_t BgL_oz00_3276)
	{
		{	/* SawMill/regset.sch 64 */
			return
				BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3276));
		}

	}



/* rtl_reg/ra-coalesce-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2costzd2
		(BgL_rtl_regz00_bglt BgL_oz00_26, obj_t BgL_vz00_27)
	{
		{	/* SawMill/regset.sch 65 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4030;

				{
					obj_t BgL_auxz00_4031;

					{	/* SawMill/regset.sch 65 */
						BgL_objectz00_bglt BgL_tmpz00_4032;

						BgL_tmpz00_4032 = ((BgL_objectz00_bglt) BgL_oz00_26);
						BgL_auxz00_4031 = BGL_OBJECT_WIDENING(BgL_tmpz00_4032);
					}
					BgL_auxz00_4030 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4031);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4030))->
						BgL_coalescez00) = ((obj_t) BgL_vz00_27), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-coalesce-set! */
	obj_t BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3277, obj_t BgL_oz00_3278, obj_t BgL_vz00_3279)
	{
		{	/* SawMill/regset.sch 65 */
			return
				BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3278), BgL_vz00_3279);
		}

	}



/* rtl_reg/ra-color */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_oz00_28)
	{
		{	/* SawMill/regset.sch 66 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4039;

				{
					obj_t BgL_auxz00_4040;

					{	/* SawMill/regset.sch 66 */
						BgL_objectz00_bglt BgL_tmpz00_4041;

						BgL_tmpz00_4041 = ((BgL_objectz00_bglt) BgL_oz00_28);
						BgL_auxz00_4040 = BGL_OBJECT_WIDENING(BgL_tmpz00_4041);
					}
					BgL_auxz00_4039 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4040);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4039))->BgL_colorz00);
			}
		}

	}



/* &rtl_reg/ra-color */
	obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3280,
		obj_t BgL_oz00_3281)
	{
		{	/* SawMill/regset.sch 66 */
			return
				BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3281));
		}

	}



/* rtl_reg/ra-color-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_oz00_29, obj_t BgL_vz00_30)
	{
		{	/* SawMill/regset.sch 67 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4048;

				{
					obj_t BgL_auxz00_4049;

					{	/* SawMill/regset.sch 67 */
						BgL_objectz00_bglt BgL_tmpz00_4050;

						BgL_tmpz00_4050 = ((BgL_objectz00_bglt) BgL_oz00_29);
						BgL_auxz00_4049 = BGL_OBJECT_WIDENING(BgL_tmpz00_4050);
					}
					BgL_auxz00_4048 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4049);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4048))->
						BgL_colorz00) = ((obj_t) BgL_vz00_30), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-color-set! */
	obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3282, obj_t BgL_oz00_3283, obj_t BgL_vz00_3284)
	{
		{	/* SawMill/regset.sch 67 */
			return
				BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3283), BgL_vz00_3284);
		}

	}



/* rtl_reg/ra-num */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_oz00_31)
	{
		{	/* SawMill/regset.sch 68 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4057;

				{
					obj_t BgL_auxz00_4058;

					{	/* SawMill/regset.sch 68 */
						BgL_objectz00_bglt BgL_tmpz00_4059;

						BgL_tmpz00_4059 = ((BgL_objectz00_bglt) BgL_oz00_31);
						BgL_auxz00_4058 = BGL_OBJECT_WIDENING(BgL_tmpz00_4059);
					}
					BgL_auxz00_4057 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4058);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4057))->BgL_numz00);
			}
		}

	}



/* &rtl_reg/ra-num */
	obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3285,
		obj_t BgL_oz00_3286)
	{
		{	/* SawMill/regset.sch 68 */
			return
				BINT(BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2costzd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_3286)));
		}

	}



/* rtl_reg/ra-hardware */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_oz00_34)
	{
		{	/* SawMill/regset.sch 70 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_34)))->BgL_hardwarez00);
		}

	}



/* &rtl_reg/ra-hardware */
	obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3287, obj_t BgL_oz00_3288)
	{
		{	/* SawMill/regset.sch 70 */
			return
				BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3288));
		}

	}



/* rtl_reg/ra-key */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_oz00_37)
	{
		{	/* SawMill/regset.sch 72 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_37)))->BgL_keyz00);
		}

	}



/* &rtl_reg/ra-key */
	obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3289,
		obj_t BgL_oz00_3290)
	{
		{	/* SawMill/regset.sch 72 */
			return
				BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3290));
		}

	}



/* rtl_reg/ra-name */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_oz00_40)
	{
		{	/* SawMill/regset.sch 74 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_40)))->BgL_namez00);
		}

	}



/* &rtl_reg/ra-name */
	obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3291,
		obj_t BgL_oz00_3292)
	{
		{	/* SawMill/regset.sch 74 */
			return
				BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3292));
		}

	}



/* rtl_reg/ra-onexpr? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_oz00_43)
	{
		{	/* SawMill/regset.sch 76 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_43)))->BgL_onexprzf3zf3);
		}

	}



/* &rtl_reg/ra-onexpr? */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3293, obj_t BgL_oz00_3294)
	{
		{	/* SawMill/regset.sch 76 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3294));
		}

	}



/* rtl_reg/ra-onexpr?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2costzd2
		(BgL_rtl_regz00_bglt BgL_oz00_44, obj_t BgL_vz00_45)
	{
		{	/* SawMill/regset.sch 77 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_44)))->BgL_onexprzf3zf3) =
				((obj_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &rtl_reg/ra-onexpr?-set! */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3295, obj_t BgL_oz00_3296, obj_t BgL_vz00_3297)
	{
		{	/* SawMill/regset.sch 77 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3296), BgL_vz00_3297);
		}

	}



/* rtl_reg/ra-var */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_oz00_46)
	{
		{	/* SawMill/regset.sch 78 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_46)))->BgL_varz00);
		}

	}



/* &rtl_reg/ra-var */
	obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3298,
		obj_t BgL_oz00_3299)
	{
		{	/* SawMill/regset.sch 78 */
			return
				BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3299));
		}

	}



/* rtl_reg/ra-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_oz00_47, obj_t BgL_vz00_48)
	{
		{	/* SawMill/regset.sch 79 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_47)))->BgL_varz00) =
				((obj_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &rtl_reg/ra-var-set! */
	obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3300, obj_t BgL_oz00_3301, obj_t BgL_vz00_3302)
	{
		{	/* SawMill/regset.sch 79 */
			return
				BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3301), BgL_vz00_3302);
		}

	}



/* rtl_reg/ra-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_oz00_49)
	{
		{	/* SawMill/regset.sch 80 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_49)))->BgL_typez00);
		}

	}



/* &rtl_reg/ra-type */
	BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3303, obj_t BgL_oz00_3304)
	{
		{	/* SawMill/regset.sch 80 */
			return
				BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3304));
		}

	}



/* rtl_reg/ra-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_oz00_50, BgL_typez00_bglt BgL_vz00_51)
	{
		{	/* SawMill/regset.sch 81 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_50)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_51), BUNSPEC);
		}

	}



/* &rtl_reg/ra-type-set! */
	obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3305, obj_t BgL_oz00_3306, obj_t BgL_vz00_3307)
	{
		{	/* SawMill/regset.sch 81 */
			return
				BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3306),
				((BgL_typez00_bglt) BgL_vz00_3307));
		}

	}



/* make-regset */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2costzd2(int BgL_length1172z00_52,
		int BgL_msiza7e1173za7_53, obj_t BgL_regv1174z00_54,
		obj_t BgL_regl1175z00_55, obj_t BgL_string1176z00_56)
	{
		{	/* SawMill/regset.sch 84 */
			{	/* SawMill/regset.sch 84 */
				BgL_regsetz00_bglt BgL_new1168z00_3635;

				{	/* SawMill/regset.sch 84 */
					BgL_regsetz00_bglt BgL_new1167z00_3636;

					BgL_new1167z00_3636 =
						((BgL_regsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_regsetz00_bgl))));
					{	/* SawMill/regset.sch 84 */
						long BgL_arg1485z00_3637;

						BgL_arg1485z00_3637 = BGL_CLASS_NUM(BGl_regsetz00zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1167z00_3636), BgL_arg1485z00_3637);
					}
					BgL_new1168z00_3635 = BgL_new1167z00_3636;
				}
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3635))->BgL_lengthz00) =
					((int) BgL_length1172z00_52), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3635))->BgL_msiza7eza7) =
					((int) BgL_msiza7e1173za7_53), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3635))->BgL_regvz00) =
					((obj_t) BgL_regv1174z00_54), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3635))->BgL_reglz00) =
					((obj_t) BgL_regl1175z00_55), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3635))->BgL_stringz00) =
					((obj_t) BgL_string1176z00_56), BUNSPEC);
				return BgL_new1168z00_3635;
			}
		}

	}



/* &make-regset */
	BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3308, obj_t BgL_length1172z00_3309,
		obj_t BgL_msiza7e1173za7_3310, obj_t BgL_regv1174z00_3311,
		obj_t BgL_regl1175z00_3312, obj_t BgL_string1176z00_3313)
	{
		{	/* SawMill/regset.sch 84 */
			return
				BGl_makezd2regsetzd2zzsaw_bbvzd2costzd2(CINT(BgL_length1172z00_3309),
				CINT(BgL_msiza7e1173za7_3310), BgL_regv1174z00_3311,
				BgL_regl1175z00_3312, BgL_string1176z00_3313);
		}

	}



/* regset? */
	BGL_EXPORTED_DEF bool_t BGl_regsetzf3zf3zzsaw_bbvzd2costzd2(obj_t
		BgL_objz00_57)
	{
		{	/* SawMill/regset.sch 85 */
			{	/* SawMill/regset.sch 85 */
				obj_t BgL_classz00_3638;

				BgL_classz00_3638 = BGl_regsetz00zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_57))
					{	/* SawMill/regset.sch 85 */
						BgL_objectz00_bglt BgL_arg1807z00_3639;

						BgL_arg1807z00_3639 = (BgL_objectz00_bglt) (BgL_objz00_57);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 85 */
								long BgL_idxz00_3640;

								BgL_idxz00_3640 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3639);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3640 + 1L)) == BgL_classz00_3638);
							}
						else
							{	/* SawMill/regset.sch 85 */
								bool_t BgL_res1941z00_3643;

								{	/* SawMill/regset.sch 85 */
									obj_t BgL_oclassz00_3644;

									{	/* SawMill/regset.sch 85 */
										obj_t BgL_arg1815z00_3645;
										long BgL_arg1816z00_3646;

										BgL_arg1815z00_3645 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 85 */
											long BgL_arg1817z00_3647;

											BgL_arg1817z00_3647 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3639);
											BgL_arg1816z00_3646 = (BgL_arg1817z00_3647 - OBJECT_TYPE);
										}
										BgL_oclassz00_3644 =
											VECTOR_REF(BgL_arg1815z00_3645, BgL_arg1816z00_3646);
									}
									{	/* SawMill/regset.sch 85 */
										bool_t BgL__ortest_1115z00_3648;

										BgL__ortest_1115z00_3648 =
											(BgL_classz00_3638 == BgL_oclassz00_3644);
										if (BgL__ortest_1115z00_3648)
											{	/* SawMill/regset.sch 85 */
												BgL_res1941z00_3643 = BgL__ortest_1115z00_3648;
											}
										else
											{	/* SawMill/regset.sch 85 */
												long BgL_odepthz00_3649;

												{	/* SawMill/regset.sch 85 */
													obj_t BgL_arg1804z00_3650;

													BgL_arg1804z00_3650 = (BgL_oclassz00_3644);
													BgL_odepthz00_3649 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3650);
												}
												if ((1L < BgL_odepthz00_3649))
													{	/* SawMill/regset.sch 85 */
														obj_t BgL_arg1802z00_3651;

														{	/* SawMill/regset.sch 85 */
															obj_t BgL_arg1803z00_3652;

															BgL_arg1803z00_3652 = (BgL_oclassz00_3644);
															BgL_arg1802z00_3651 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3652,
																1L);
														}
														BgL_res1941z00_3643 =
															(BgL_arg1802z00_3651 == BgL_classz00_3638);
													}
												else
													{	/* SawMill/regset.sch 85 */
														BgL_res1941z00_3643 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1941z00_3643;
							}
					}
				else
					{	/* SawMill/regset.sch 85 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &regset? */
	obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3314,
		obj_t BgL_objz00_3315)
	{
		{	/* SawMill/regset.sch 85 */
			return BBOOL(BGl_regsetzf3zf3zzsaw_bbvzd2costzd2(BgL_objz00_3315));
		}

	}



/* regset-nil */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2costzd2(void)
	{
		{	/* SawMill/regset.sch 86 */
			{	/* SawMill/regset.sch 86 */
				obj_t BgL_classz00_2822;

				BgL_classz00_2822 = BGl_regsetz00zzsaw_regsetz00;
				{	/* SawMill/regset.sch 86 */
					obj_t BgL__ortest_1117z00_2823;

					BgL__ortest_1117z00_2823 = BGL_CLASS_NIL(BgL_classz00_2822);
					if (CBOOL(BgL__ortest_1117z00_2823))
						{	/* SawMill/regset.sch 86 */
							return ((BgL_regsetz00_bglt) BgL__ortest_1117z00_2823);
						}
					else
						{	/* SawMill/regset.sch 86 */
							return
								((BgL_regsetz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2822));
						}
				}
			}
		}

	}



/* &regset-nil */
	BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3316)
	{
		{	/* SawMill/regset.sch 86 */
			return BGl_regsetzd2nilzd2zzsaw_bbvzd2costzd2();
		}

	}



/* regset-string */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2costzd2(BgL_regsetz00_bglt BgL_oz00_58)
	{
		{	/* SawMill/regset.sch 87 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_58))->BgL_stringz00);
		}

	}



/* &regset-string */
	obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3317,
		obj_t BgL_oz00_3318)
	{
		{	/* SawMill/regset.sch 87 */
			return
				BGl_regsetzd2stringzd2zzsaw_bbvzd2costzd2(
				((BgL_regsetz00_bglt) BgL_oz00_3318));
		}

	}



/* regset-string-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2costzd2(BgL_regsetz00_bglt
		BgL_oz00_59, obj_t BgL_vz00_60)
	{
		{	/* SawMill/regset.sch 88 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_59))->BgL_stringz00) =
				((obj_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &regset-string-set! */
	obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3319, obj_t BgL_oz00_3320, obj_t BgL_vz00_3321)
	{
		{	/* SawMill/regset.sch 88 */
			return
				BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_regsetz00_bglt) BgL_oz00_3320), BgL_vz00_3321);
		}

	}



/* regset-regl */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2costzd2(BgL_regsetz00_bglt BgL_oz00_61)
	{
		{	/* SawMill/regset.sch 89 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_61))->BgL_reglz00);
		}

	}



/* &regset-regl */
	obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3322,
		obj_t BgL_oz00_3323)
	{
		{	/* SawMill/regset.sch 89 */
			return
				BGl_regsetzd2reglzd2zzsaw_bbvzd2costzd2(
				((BgL_regsetz00_bglt) BgL_oz00_3323));
		}

	}



/* regset-regv */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2costzd2(BgL_regsetz00_bglt BgL_oz00_64)
	{
		{	/* SawMill/regset.sch 91 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_64))->BgL_regvz00);
		}

	}



/* &regset-regv */
	obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3324,
		obj_t BgL_oz00_3325)
	{
		{	/* SawMill/regset.sch 91 */
			return
				BGl_regsetzd2regvzd2zzsaw_bbvzd2costzd2(
				((BgL_regsetz00_bglt) BgL_oz00_3325));
		}

	}



/* regset-msize */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2costzd2(BgL_regsetz00_bglt BgL_oz00_67)
	{
		{	/* SawMill/regset.sch 93 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_67))->BgL_msiza7eza7);
		}

	}



/* &regset-msize */
	obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3326,
		obj_t BgL_oz00_3327)
	{
		{	/* SawMill/regset.sch 93 */
			return
				BINT(BGl_regsetzd2msiza7ez75zzsaw_bbvzd2costzd2(
					((BgL_regsetz00_bglt) BgL_oz00_3327)));
		}

	}



/* regset-length */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2costzd2(BgL_regsetz00_bglt BgL_oz00_70)
	{
		{	/* SawMill/regset.sch 95 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_70))->BgL_lengthz00);
		}

	}



/* &regset-length */
	obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3328,
		obj_t BgL_oz00_3329)
	{
		{	/* SawMill/regset.sch 95 */
			return
				BINT(BGl_regsetzd2lengthzd2zzsaw_bbvzd2costzd2(
					((BgL_regsetz00_bglt) BgL_oz00_3329)));
		}

	}



/* regset-length-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2costzd2(BgL_regsetz00_bglt
		BgL_oz00_71, int BgL_vz00_72)
	{
		{	/* SawMill/regset.sch 96 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_71))->BgL_lengthz00) =
				((int) BgL_vz00_72), BUNSPEC);
		}

	}



/* &regset-length-set! */
	obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3330, obj_t BgL_oz00_3331, obj_t BgL_vz00_3332)
	{
		{	/* SawMill/regset.sch 96 */
			return
				BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_regsetz00_bglt) BgL_oz00_3331), CINT(BgL_vz00_3332));
		}

	}



/* make-rtl_ins/bbv */
	BGL_EXPORTED_DEF BgL_rtl_insz00_bglt
		BGl_makezd2rtl_inszf2bbvz20zzsaw_bbvzd2costzd2(obj_t BgL_loc1264z00_91,
		obj_t BgL_z52spill1265z52_92, obj_t BgL_dest1266z00_93,
		BgL_rtl_funz00_bglt BgL_fun1267z00_94, obj_t BgL_args1268z00_95,
		obj_t BgL_def1269z00_96, obj_t BgL_out1270z00_97, obj_t BgL_in1271z00_98,
		BgL_bbvzd2ctxzd2_bglt BgL_ctx1272z00_99, obj_t BgL_z52hash1273z52_100)
	{
		{	/* SawBbv/bbv-types.sch 137 */
			{	/* SawBbv/bbv-types.sch 137 */
				BgL_rtl_insz00_bglt BgL_new1175z00_3653;

				{	/* SawBbv/bbv-types.sch 137 */
					BgL_rtl_insz00_bglt BgL_tmp1173z00_3654;
					BgL_rtl_inszf2bbvzf2_bglt BgL_wide1174z00_3655;

					{
						BgL_rtl_insz00_bglt BgL_auxz00_4171;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_rtl_insz00_bglt BgL_new1172z00_3656;

							BgL_new1172z00_3656 =
								((BgL_rtl_insz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_insz00_bgl))));
							{	/* SawBbv/bbv-types.sch 137 */
								long BgL_arg1591z00_3657;

								BgL_arg1591z00_3657 =
									BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1172z00_3656),
									BgL_arg1591z00_3657);
							}
							{	/* SawBbv/bbv-types.sch 137 */
								BgL_objectz00_bglt BgL_tmpz00_4176;

								BgL_tmpz00_4176 = ((BgL_objectz00_bglt) BgL_new1172z00_3656);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4176, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1172z00_3656);
							BgL_auxz00_4171 = BgL_new1172z00_3656;
						}
						BgL_tmp1173z00_3654 = ((BgL_rtl_insz00_bglt) BgL_auxz00_4171);
					}
					BgL_wide1174z00_3655 =
						((BgL_rtl_inszf2bbvzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_inszf2bbvzf2_bgl))));
					{	/* SawBbv/bbv-types.sch 137 */
						obj_t BgL_auxz00_4184;
						BgL_objectz00_bglt BgL_tmpz00_4182;

						BgL_auxz00_4184 = ((obj_t) BgL_wide1174z00_3655);
						BgL_tmpz00_4182 = ((BgL_objectz00_bglt) BgL_tmp1173z00_3654);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4182, BgL_auxz00_4184);
					}
					((BgL_objectz00_bglt) BgL_tmp1173z00_3654);
					{	/* SawBbv/bbv-types.sch 137 */
						long BgL_arg1589z00_3658;

						BgL_arg1589z00_3658 =
							BGL_CLASS_NUM(BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1173z00_3654), BgL_arg1589z00_3658);
					}
					BgL_new1175z00_3653 = ((BgL_rtl_insz00_bglt) BgL_tmp1173z00_3654);
				}
				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_new1175z00_3653)))->BgL_locz00) =
					((obj_t) BgL_loc1264z00_91), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1175z00_3653)))->BgL_z52spillz52) =
					((obj_t) BgL_z52spill1265z52_92), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1175z00_3653)))->BgL_destz00) =
					((obj_t) BgL_dest1266z00_93), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1175z00_3653)))->BgL_funz00) =
					((BgL_rtl_funz00_bglt) BgL_fun1267z00_94), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1175z00_3653)))->BgL_argsz00) =
					((obj_t) BgL_args1268z00_95), BUNSPEC);
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4202;

					{
						obj_t BgL_auxz00_4203;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_objectz00_bglt BgL_tmpz00_4204;

							BgL_tmpz00_4204 = ((BgL_objectz00_bglt) BgL_new1175z00_3653);
							BgL_auxz00_4203 = BGL_OBJECT_WIDENING(BgL_tmpz00_4204);
						}
						BgL_auxz00_4202 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4203);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4202))->
							BgL_defz00) = ((obj_t) BgL_def1269z00_96), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4209;

					{
						obj_t BgL_auxz00_4210;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_objectz00_bglt BgL_tmpz00_4211;

							BgL_tmpz00_4211 = ((BgL_objectz00_bglt) BgL_new1175z00_3653);
							BgL_auxz00_4210 = BGL_OBJECT_WIDENING(BgL_tmpz00_4211);
						}
						BgL_auxz00_4209 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4210);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4209))->
							BgL_outz00) = ((obj_t) BgL_out1270z00_97), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4216;

					{
						obj_t BgL_auxz00_4217;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_objectz00_bglt BgL_tmpz00_4218;

							BgL_tmpz00_4218 = ((BgL_objectz00_bglt) BgL_new1175z00_3653);
							BgL_auxz00_4217 = BGL_OBJECT_WIDENING(BgL_tmpz00_4218);
						}
						BgL_auxz00_4216 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4217);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4216))->BgL_inz00) =
						((obj_t) BgL_in1271z00_98), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4223;

					{
						obj_t BgL_auxz00_4224;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_objectz00_bglt BgL_tmpz00_4225;

							BgL_tmpz00_4225 = ((BgL_objectz00_bglt) BgL_new1175z00_3653);
							BgL_auxz00_4224 = BGL_OBJECT_WIDENING(BgL_tmpz00_4225);
						}
						BgL_auxz00_4223 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4224);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4223))->
							BgL_ctxz00) =
						((BgL_bbvzd2ctxzd2_bglt) BgL_ctx1272z00_99), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4230;

					{
						obj_t BgL_auxz00_4231;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_objectz00_bglt BgL_tmpz00_4232;

							BgL_tmpz00_4232 = ((BgL_objectz00_bglt) BgL_new1175z00_3653);
							BgL_auxz00_4231 = BGL_OBJECT_WIDENING(BgL_tmpz00_4232);
						}
						BgL_auxz00_4230 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4231);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4230))->
							BgL_z52hashz52) = ((obj_t) BgL_z52hash1273z52_100), BUNSPEC);
				}
				return BgL_new1175z00_3653;
			}
		}

	}



/* &make-rtl_ins/bbv */
	BgL_rtl_insz00_bglt BGl_z62makezd2rtl_inszf2bbvz42zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3333, obj_t BgL_loc1264z00_3334, obj_t BgL_z52spill1265z52_3335,
		obj_t BgL_dest1266z00_3336, obj_t BgL_fun1267z00_3337,
		obj_t BgL_args1268z00_3338, obj_t BgL_def1269z00_3339,
		obj_t BgL_out1270z00_3340, obj_t BgL_in1271z00_3341,
		obj_t BgL_ctx1272z00_3342, obj_t BgL_z52hash1273z52_3343)
	{
		{	/* SawBbv/bbv-types.sch 137 */
			return
				BGl_makezd2rtl_inszf2bbvz20zzsaw_bbvzd2costzd2(BgL_loc1264z00_3334,
				BgL_z52spill1265z52_3335, BgL_dest1266z00_3336,
				((BgL_rtl_funz00_bglt) BgL_fun1267z00_3337), BgL_args1268z00_3338,
				BgL_def1269z00_3339, BgL_out1270z00_3340, BgL_in1271z00_3341,
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctx1272z00_3342), BgL_z52hash1273z52_3343);
		}

	}



/* rtl_ins/bbv? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_inszf2bbvzf3z01zzsaw_bbvzd2costzd2(obj_t
		BgL_objz00_101)
	{
		{	/* SawBbv/bbv-types.sch 138 */
			{	/* SawBbv/bbv-types.sch 138 */
				obj_t BgL_classz00_3659;

				BgL_classz00_3659 = BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_101))
					{	/* SawBbv/bbv-types.sch 138 */
						BgL_objectz00_bglt BgL_arg1807z00_3660;

						BgL_arg1807z00_3660 = (BgL_objectz00_bglt) (BgL_objz00_101);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 138 */
								long BgL_idxz00_3661;

								BgL_idxz00_3661 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3660);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3661 + 2L)) == BgL_classz00_3659);
							}
						else
							{	/* SawBbv/bbv-types.sch 138 */
								bool_t BgL_res1942z00_3664;

								{	/* SawBbv/bbv-types.sch 138 */
									obj_t BgL_oclassz00_3665;

									{	/* SawBbv/bbv-types.sch 138 */
										obj_t BgL_arg1815z00_3666;
										long BgL_arg1816z00_3667;

										BgL_arg1815z00_3666 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 138 */
											long BgL_arg1817z00_3668;

											BgL_arg1817z00_3668 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3660);
											BgL_arg1816z00_3667 = (BgL_arg1817z00_3668 - OBJECT_TYPE);
										}
										BgL_oclassz00_3665 =
											VECTOR_REF(BgL_arg1815z00_3666, BgL_arg1816z00_3667);
									}
									{	/* SawBbv/bbv-types.sch 138 */
										bool_t BgL__ortest_1115z00_3669;

										BgL__ortest_1115z00_3669 =
											(BgL_classz00_3659 == BgL_oclassz00_3665);
										if (BgL__ortest_1115z00_3669)
											{	/* SawBbv/bbv-types.sch 138 */
												BgL_res1942z00_3664 = BgL__ortest_1115z00_3669;
											}
										else
											{	/* SawBbv/bbv-types.sch 138 */
												long BgL_odepthz00_3670;

												{	/* SawBbv/bbv-types.sch 138 */
													obj_t BgL_arg1804z00_3671;

													BgL_arg1804z00_3671 = (BgL_oclassz00_3665);
													BgL_odepthz00_3670 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3671);
												}
												if ((2L < BgL_odepthz00_3670))
													{	/* SawBbv/bbv-types.sch 138 */
														obj_t BgL_arg1802z00_3672;

														{	/* SawBbv/bbv-types.sch 138 */
															obj_t BgL_arg1803z00_3673;

															BgL_arg1803z00_3673 = (BgL_oclassz00_3665);
															BgL_arg1802z00_3672 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3673,
																2L);
														}
														BgL_res1942z00_3664 =
															(BgL_arg1802z00_3672 == BgL_classz00_3659);
													}
												else
													{	/* SawBbv/bbv-types.sch 138 */
														BgL_res1942z00_3664 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1942z00_3664;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 138 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_ins/bbv? */
	obj_t BGl_z62rtl_inszf2bbvzf3z63zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3344,
		obj_t BgL_objz00_3345)
	{
		{	/* SawBbv/bbv-types.sch 138 */
			return BBOOL(BGl_rtl_inszf2bbvzf3z01zzsaw_bbvzd2costzd2(BgL_objz00_3345));
		}

	}



/* rtl_ins/bbv-nil */
	BGL_EXPORTED_DEF BgL_rtl_insz00_bglt
		BGl_rtl_inszf2bbvzd2nilz20zzsaw_bbvzd2costzd2(void)
	{
		{	/* SawBbv/bbv-types.sch 139 */
			{	/* SawBbv/bbv-types.sch 139 */
				obj_t BgL_classz00_2877;

				BgL_classz00_2877 = BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 139 */
					obj_t BgL__ortest_1117z00_2878;

					BgL__ortest_1117z00_2878 = BGL_CLASS_NIL(BgL_classz00_2877);
					if (CBOOL(BgL__ortest_1117z00_2878))
						{	/* SawBbv/bbv-types.sch 139 */
							return ((BgL_rtl_insz00_bglt) BgL__ortest_1117z00_2878);
						}
					else
						{	/* SawBbv/bbv-types.sch 139 */
							return
								((BgL_rtl_insz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2877));
						}
				}
			}
		}

	}



/* &rtl_ins/bbv-nil */
	BgL_rtl_insz00_bglt BGl_z62rtl_inszf2bbvzd2nilz42zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3346)
	{
		{	/* SawBbv/bbv-types.sch 139 */
			return BGl_rtl_inszf2bbvzd2nilz20zzsaw_bbvzd2costzd2();
		}

	}



/* rtl_ins/bbv-%hash */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2z52hashz72zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_102)
	{
		{	/* SawBbv/bbv-types.sch 140 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4271;

				{
					obj_t BgL_auxz00_4272;

					{	/* SawBbv/bbv-types.sch 140 */
						BgL_objectz00_bglt BgL_tmpz00_4273;

						BgL_tmpz00_4273 = ((BgL_objectz00_bglt) BgL_oz00_102);
						BgL_auxz00_4272 = BGL_OBJECT_WIDENING(BgL_tmpz00_4273);
					}
					BgL_auxz00_4271 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4272);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4271))->
					BgL_z52hashz52);
			}
		}

	}



/* &rtl_ins/bbv-%hash */
	obj_t BGl_z62rtl_inszf2bbvzd2z52hashz10zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3347, obj_t BgL_oz00_3348)
	{
		{	/* SawBbv/bbv-types.sch 140 */
			return
				BGl_rtl_inszf2bbvzd2z52hashz72zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3348));
		}

	}



/* rtl_ins/bbv-%hash-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2z52hashzd2setz12zb2zzsaw_bbvzd2costzd2
		(BgL_rtl_insz00_bglt BgL_oz00_103, obj_t BgL_vz00_104)
	{
		{	/* SawBbv/bbv-types.sch 141 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4280;

				{
					obj_t BgL_auxz00_4281;

					{	/* SawBbv/bbv-types.sch 141 */
						BgL_objectz00_bglt BgL_tmpz00_4282;

						BgL_tmpz00_4282 = ((BgL_objectz00_bglt) BgL_oz00_103);
						BgL_auxz00_4281 = BGL_OBJECT_WIDENING(BgL_tmpz00_4282);
					}
					BgL_auxz00_4280 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4281);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4280))->
						BgL_z52hashz52) = ((obj_t) BgL_vz00_104), BUNSPEC);
			}
		}

	}



/* &rtl_ins/bbv-%hash-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2z52hashzd2setz12zd0zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3349, obj_t BgL_oz00_3350, obj_t BgL_vz00_3351)
	{
		{	/* SawBbv/bbv-types.sch 141 */
			return
				BGl_rtl_inszf2bbvzd2z52hashzd2setz12zb2zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3350), BgL_vz00_3351);
		}

	}



/* rtl_ins/bbv-ctx */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_rtl_inszf2bbvzd2ctxz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_105)
	{
		{	/* SawBbv/bbv-types.sch 142 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4289;

				{
					obj_t BgL_auxz00_4290;

					{	/* SawBbv/bbv-types.sch 142 */
						BgL_objectz00_bglt BgL_tmpz00_4291;

						BgL_tmpz00_4291 = ((BgL_objectz00_bglt) BgL_oz00_105);
						BgL_auxz00_4290 = BGL_OBJECT_WIDENING(BgL_tmpz00_4291);
					}
					BgL_auxz00_4289 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4290);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4289))->BgL_ctxz00);
			}
		}

	}



/* &rtl_ins/bbv-ctx */
	BgL_bbvzd2ctxzd2_bglt BGl_z62rtl_inszf2bbvzd2ctxz42zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3352, obj_t BgL_oz00_3353)
	{
		{	/* SawBbv/bbv-types.sch 142 */
			return
				BGl_rtl_inszf2bbvzd2ctxz20zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3353));
		}

	}



/* rtl_ins/bbv-ctx-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2ctxzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_106, BgL_bbvzd2ctxzd2_bglt BgL_vz00_107)
	{
		{	/* SawBbv/bbv-types.sch 143 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4298;

				{
					obj_t BgL_auxz00_4299;

					{	/* SawBbv/bbv-types.sch 143 */
						BgL_objectz00_bglt BgL_tmpz00_4300;

						BgL_tmpz00_4300 = ((BgL_objectz00_bglt) BgL_oz00_106);
						BgL_auxz00_4299 = BGL_OBJECT_WIDENING(BgL_tmpz00_4300);
					}
					BgL_auxz00_4298 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4299);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4298))->
						BgL_ctxz00) = ((BgL_bbvzd2ctxzd2_bglt) BgL_vz00_107), BUNSPEC);
			}
		}

	}



/* &rtl_ins/bbv-ctx-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2ctxzd2setz12z82zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3354, obj_t BgL_oz00_3355, obj_t BgL_vz00_3356)
	{
		{	/* SawBbv/bbv-types.sch 143 */
			return
				BGl_rtl_inszf2bbvzd2ctxzd2setz12ze0zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3355),
				((BgL_bbvzd2ctxzd2_bglt) BgL_vz00_3356));
		}

	}



/* rtl_ins/bbv-in */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2inz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_108)
	{
		{	/* SawBbv/bbv-types.sch 144 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4308;

				{
					obj_t BgL_auxz00_4309;

					{	/* SawBbv/bbv-types.sch 144 */
						BgL_objectz00_bglt BgL_tmpz00_4310;

						BgL_tmpz00_4310 = ((BgL_objectz00_bglt) BgL_oz00_108);
						BgL_auxz00_4309 = BGL_OBJECT_WIDENING(BgL_tmpz00_4310);
					}
					BgL_auxz00_4308 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4309);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4308))->BgL_inz00);
			}
		}

	}



/* &rtl_ins/bbv-in */
	obj_t BGl_z62rtl_inszf2bbvzd2inz42zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3357,
		obj_t BgL_oz00_3358)
	{
		{	/* SawBbv/bbv-types.sch 144 */
			return
				BGl_rtl_inszf2bbvzd2inz20zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3358));
		}

	}



/* rtl_ins/bbv-in-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2inzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_109, obj_t BgL_vz00_110)
	{
		{	/* SawBbv/bbv-types.sch 145 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4317;

				{
					obj_t BgL_auxz00_4318;

					{	/* SawBbv/bbv-types.sch 145 */
						BgL_objectz00_bglt BgL_tmpz00_4319;

						BgL_tmpz00_4319 = ((BgL_objectz00_bglt) BgL_oz00_109);
						BgL_auxz00_4318 = BGL_OBJECT_WIDENING(BgL_tmpz00_4319);
					}
					BgL_auxz00_4317 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4318);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4317))->BgL_inz00) =
					((obj_t) BgL_vz00_110), BUNSPEC);
			}
		}

	}



/* &rtl_ins/bbv-in-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2inzd2setz12z82zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3359, obj_t BgL_oz00_3360, obj_t BgL_vz00_3361)
	{
		{	/* SawBbv/bbv-types.sch 145 */
			return
				BGl_rtl_inszf2bbvzd2inzd2setz12ze0zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3360), BgL_vz00_3361);
		}

	}



/* rtl_ins/bbv-out */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2outz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_111)
	{
		{	/* SawBbv/bbv-types.sch 146 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4326;

				{
					obj_t BgL_auxz00_4327;

					{	/* SawBbv/bbv-types.sch 146 */
						BgL_objectz00_bglt BgL_tmpz00_4328;

						BgL_tmpz00_4328 = ((BgL_objectz00_bglt) BgL_oz00_111);
						BgL_auxz00_4327 = BGL_OBJECT_WIDENING(BgL_tmpz00_4328);
					}
					BgL_auxz00_4326 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4327);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4326))->BgL_outz00);
			}
		}

	}



/* &rtl_ins/bbv-out */
	obj_t BGl_z62rtl_inszf2bbvzd2outz42zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3362,
		obj_t BgL_oz00_3363)
	{
		{	/* SawBbv/bbv-types.sch 146 */
			return
				BGl_rtl_inszf2bbvzd2outz20zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3363));
		}

	}



/* rtl_ins/bbv-out-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2outzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_112, obj_t BgL_vz00_113)
	{
		{	/* SawBbv/bbv-types.sch 147 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4335;

				{
					obj_t BgL_auxz00_4336;

					{	/* SawBbv/bbv-types.sch 147 */
						BgL_objectz00_bglt BgL_tmpz00_4337;

						BgL_tmpz00_4337 = ((BgL_objectz00_bglt) BgL_oz00_112);
						BgL_auxz00_4336 = BGL_OBJECT_WIDENING(BgL_tmpz00_4337);
					}
					BgL_auxz00_4335 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4336);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4335))->
						BgL_outz00) = ((obj_t) BgL_vz00_113), BUNSPEC);
			}
		}

	}



/* &rtl_ins/bbv-out-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2outzd2setz12z82zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3364, obj_t BgL_oz00_3365, obj_t BgL_vz00_3366)
	{
		{	/* SawBbv/bbv-types.sch 147 */
			return
				BGl_rtl_inszf2bbvzd2outzd2setz12ze0zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3365), BgL_vz00_3366);
		}

	}



/* rtl_ins/bbv-def */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2defz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_114)
	{
		{	/* SawBbv/bbv-types.sch 148 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4344;

				{
					obj_t BgL_auxz00_4345;

					{	/* SawBbv/bbv-types.sch 148 */
						BgL_objectz00_bglt BgL_tmpz00_4346;

						BgL_tmpz00_4346 = ((BgL_objectz00_bglt) BgL_oz00_114);
						BgL_auxz00_4345 = BGL_OBJECT_WIDENING(BgL_tmpz00_4346);
					}
					BgL_auxz00_4344 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4345);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4344))->BgL_defz00);
			}
		}

	}



/* &rtl_ins/bbv-def */
	obj_t BGl_z62rtl_inszf2bbvzd2defz42zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3367,
		obj_t BgL_oz00_3368)
	{
		{	/* SawBbv/bbv-types.sch 148 */
			return
				BGl_rtl_inszf2bbvzd2defz20zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3368));
		}

	}



/* rtl_ins/bbv-def-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2defzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_115, obj_t BgL_vz00_116)
	{
		{	/* SawBbv/bbv-types.sch 149 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4353;

				{
					obj_t BgL_auxz00_4354;

					{	/* SawBbv/bbv-types.sch 149 */
						BgL_objectz00_bglt BgL_tmpz00_4355;

						BgL_tmpz00_4355 = ((BgL_objectz00_bglt) BgL_oz00_115);
						BgL_auxz00_4354 = BGL_OBJECT_WIDENING(BgL_tmpz00_4355);
					}
					BgL_auxz00_4353 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4354);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4353))->
						BgL_defz00) = ((obj_t) BgL_vz00_116), BUNSPEC);
			}
		}

	}



/* &rtl_ins/bbv-def-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2defzd2setz12z82zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3369, obj_t BgL_oz00_3370, obj_t BgL_vz00_3371)
	{
		{	/* SawBbv/bbv-types.sch 149 */
			return
				BGl_rtl_inszf2bbvzd2defzd2setz12ze0zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3370), BgL_vz00_3371);
		}

	}



/* rtl_ins/bbv-args */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2argsz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_117)
	{
		{	/* SawBbv/bbv-types.sch 150 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_117)))->BgL_argsz00);
		}

	}



/* &rtl_ins/bbv-args */
	obj_t BGl_z62rtl_inszf2bbvzd2argsz42zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3372,
		obj_t BgL_oz00_3373)
	{
		{	/* SawBbv/bbv-types.sch 150 */
			return
				BGl_rtl_inszf2bbvzd2argsz20zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3373));
		}

	}



/* rtl_ins/bbv-args-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2argszd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_118, obj_t BgL_vz00_119)
	{
		{	/* SawBbv/bbv-types.sch 151 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_118)))->BgL_argsz00) =
				((obj_t) BgL_vz00_119), BUNSPEC);
		}

	}



/* &rtl_ins/bbv-args-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2argszd2setz12z82zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3374, obj_t BgL_oz00_3375, obj_t BgL_vz00_3376)
	{
		{	/* SawBbv/bbv-types.sch 151 */
			return
				BGl_rtl_inszf2bbvzd2argszd2setz12ze0zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3375), BgL_vz00_3376);
		}

	}



/* rtl_ins/bbv-fun */
	BGL_EXPORTED_DEF BgL_rtl_funz00_bglt
		BGl_rtl_inszf2bbvzd2funz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_120)
	{
		{	/* SawBbv/bbv-types.sch 152 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_120)))->BgL_funz00);
		}

	}



/* &rtl_ins/bbv-fun */
	BgL_rtl_funz00_bglt BGl_z62rtl_inszf2bbvzd2funz42zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3377, obj_t BgL_oz00_3378)
	{
		{	/* SawBbv/bbv-types.sch 152 */
			return
				BGl_rtl_inszf2bbvzd2funz20zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3378));
		}

	}



/* rtl_ins/bbv-fun-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2funzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_121, BgL_rtl_funz00_bglt BgL_vz00_122)
	{
		{	/* SawBbv/bbv-types.sch 153 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_121)))->BgL_funz00) =
				((BgL_rtl_funz00_bglt) BgL_vz00_122), BUNSPEC);
		}

	}



/* &rtl_ins/bbv-fun-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2funzd2setz12z82zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3379, obj_t BgL_oz00_3380, obj_t BgL_vz00_3381)
	{
		{	/* SawBbv/bbv-types.sch 153 */
			return
				BGl_rtl_inszf2bbvzd2funzd2setz12ze0zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3380),
				((BgL_rtl_funz00_bglt) BgL_vz00_3381));
		}

	}



/* rtl_ins/bbv-dest */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2destz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_123)
	{
		{	/* SawBbv/bbv-types.sch 154 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_123)))->BgL_destz00);
		}

	}



/* &rtl_ins/bbv-dest */
	obj_t BGl_z62rtl_inszf2bbvzd2destz42zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3382,
		obj_t BgL_oz00_3383)
	{
		{	/* SawBbv/bbv-types.sch 154 */
			return
				BGl_rtl_inszf2bbvzd2destz20zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3383));
		}

	}



/* rtl_ins/bbv-dest-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2destzd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_124, obj_t BgL_vz00_125)
	{
		{	/* SawBbv/bbv-types.sch 155 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_124)))->BgL_destz00) =
				((obj_t) BgL_vz00_125), BUNSPEC);
		}

	}



/* &rtl_ins/bbv-dest-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2destzd2setz12z82zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3384, obj_t BgL_oz00_3385, obj_t BgL_vz00_3386)
	{
		{	/* SawBbv/bbv-types.sch 155 */
			return
				BGl_rtl_inszf2bbvzd2destzd2setz12ze0zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3385), BgL_vz00_3386);
		}

	}



/* rtl_ins/bbv-%spill */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2z52spillz72zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_126)
	{
		{	/* SawBbv/bbv-types.sch 156 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_126)))->BgL_z52spillz52);
		}

	}



/* &rtl_ins/bbv-%spill */
	obj_t BGl_z62rtl_inszf2bbvzd2z52spillz10zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3387, obj_t BgL_oz00_3388)
	{
		{	/* SawBbv/bbv-types.sch 156 */
			return
				BGl_rtl_inszf2bbvzd2z52spillz72zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3388));
		}

	}



/* rtl_ins/bbv-%spill-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2z52spillzd2setz12zb2zzsaw_bbvzd2costzd2
		(BgL_rtl_insz00_bglt BgL_oz00_127, obj_t BgL_vz00_128)
	{
		{	/* SawBbv/bbv-types.sch 157 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_127)))->BgL_z52spillz52) =
				((obj_t) BgL_vz00_128), BUNSPEC);
		}

	}



/* &rtl_ins/bbv-%spill-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2z52spillzd2setz12zd0zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3389, obj_t BgL_oz00_3390, obj_t BgL_vz00_3391)
	{
		{	/* SawBbv/bbv-types.sch 157 */
			return
				BGl_rtl_inszf2bbvzd2z52spillzd2setz12zb2zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3390), BgL_vz00_3391);
		}

	}



/* rtl_ins/bbv-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2locz20zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_129)
	{
		{	/* SawBbv/bbv-types.sch 158 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_129)))->BgL_locz00);
		}

	}



/* &rtl_ins/bbv-loc */
	obj_t BGl_z62rtl_inszf2bbvzd2locz42zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3392,
		obj_t BgL_oz00_3393)
	{
		{	/* SawBbv/bbv-types.sch 158 */
			return
				BGl_rtl_inszf2bbvzd2locz20zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3393));
		}

	}



/* rtl_ins/bbv-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2loczd2setz12ze0zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_oz00_130, obj_t BgL_vz00_131)
	{
		{	/* SawBbv/bbv-types.sch 159 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_130)))->BgL_locz00) =
				((obj_t) BgL_vz00_131), BUNSPEC);
		}

	}



/* &rtl_ins/bbv-loc-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2loczd2setz12z82zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3394, obj_t BgL_oz00_3395, obj_t BgL_vz00_3396)
	{
		{	/* SawBbv/bbv-types.sch 159 */
			return
				BGl_rtl_inszf2bbvzd2loczd2setz12ze0zzsaw_bbvzd2costzd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3395), BgL_vz00_3396);
		}

	}



/* make-blockV */
	BGL_EXPORTED_DEF BgL_blockz00_bglt BGl_makezd2blockVzd2zzsaw_bbvzd2costzd2(int
		BgL_label1255z00_132, obj_t BgL_preds1256z00_133,
		obj_t BgL_succs1257z00_134, obj_t BgL_first1258z00_135,
		obj_t BgL_versions1259z00_136, obj_t BgL_generic1260z00_137,
		long BgL_z52mark1261z52_138, obj_t BgL_merge1262z00_139)
	{
		{	/* SawBbv/bbv-types.sch 162 */
			{	/* SawBbv/bbv-types.sch 162 */
				BgL_blockz00_bglt BgL_new1179z00_3674;

				{	/* SawBbv/bbv-types.sch 162 */
					BgL_blockz00_bglt BgL_tmp1177z00_3675;
					BgL_blockvz00_bglt BgL_wide1178z00_3676;

					{
						BgL_blockz00_bglt BgL_auxz00_4403;

						{	/* SawBbv/bbv-types.sch 162 */
							BgL_blockz00_bglt BgL_new1176z00_3677;

							BgL_new1176z00_3677 =
								((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_blockz00_bgl))));
							{	/* SawBbv/bbv-types.sch 162 */
								long BgL_arg1594z00_3678;

								BgL_arg1594z00_3678 = BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1176z00_3677),
									BgL_arg1594z00_3678);
							}
							{	/* SawBbv/bbv-types.sch 162 */
								BgL_objectz00_bglt BgL_tmpz00_4408;

								BgL_tmpz00_4408 = ((BgL_objectz00_bglt) BgL_new1176z00_3677);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4408, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1176z00_3677);
							BgL_auxz00_4403 = BgL_new1176z00_3677;
						}
						BgL_tmp1177z00_3675 = ((BgL_blockz00_bglt) BgL_auxz00_4403);
					}
					BgL_wide1178z00_3676 =
						((BgL_blockvz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_blockvz00_bgl))));
					{	/* SawBbv/bbv-types.sch 162 */
						obj_t BgL_auxz00_4416;
						BgL_objectz00_bglt BgL_tmpz00_4414;

						BgL_auxz00_4416 = ((obj_t) BgL_wide1178z00_3676);
						BgL_tmpz00_4414 = ((BgL_objectz00_bglt) BgL_tmp1177z00_3675);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4414, BgL_auxz00_4416);
					}
					((BgL_objectz00_bglt) BgL_tmp1177z00_3675);
					{	/* SawBbv/bbv-types.sch 162 */
						long BgL_arg1593z00_3679;

						BgL_arg1593z00_3679 =
							BGL_CLASS_NUM(BGl_blockVz00zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1177z00_3675), BgL_arg1593z00_3679);
					}
					BgL_new1179z00_3674 = ((BgL_blockz00_bglt) BgL_tmp1177z00_3675);
				}
				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_new1179z00_3674)))->BgL_labelz00) =
					((int) BgL_label1255z00_132), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1179z00_3674)))->BgL_predsz00) =
					((obj_t) BgL_preds1256z00_133), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1179z00_3674)))->BgL_succsz00) =
					((obj_t) BgL_succs1257z00_134), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1179z00_3674)))->BgL_firstz00) =
					((obj_t) BgL_first1258z00_135), BUNSPEC);
				{
					BgL_blockvz00_bglt BgL_auxz00_4432;

					{
						obj_t BgL_auxz00_4433;

						{	/* SawBbv/bbv-types.sch 162 */
							BgL_objectz00_bglt BgL_tmpz00_4434;

							BgL_tmpz00_4434 = ((BgL_objectz00_bglt) BgL_new1179z00_3674);
							BgL_auxz00_4433 = BGL_OBJECT_WIDENING(BgL_tmpz00_4434);
						}
						BgL_auxz00_4432 = ((BgL_blockvz00_bglt) BgL_auxz00_4433);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4432))->BgL_versionsz00) =
						((obj_t) BgL_versions1259z00_136), BUNSPEC);
				}
				{
					BgL_blockvz00_bglt BgL_auxz00_4439;

					{
						obj_t BgL_auxz00_4440;

						{	/* SawBbv/bbv-types.sch 162 */
							BgL_objectz00_bglt BgL_tmpz00_4441;

							BgL_tmpz00_4441 = ((BgL_objectz00_bglt) BgL_new1179z00_3674);
							BgL_auxz00_4440 = BGL_OBJECT_WIDENING(BgL_tmpz00_4441);
						}
						BgL_auxz00_4439 = ((BgL_blockvz00_bglt) BgL_auxz00_4440);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4439))->BgL_genericz00) =
						((obj_t) BgL_generic1260z00_137), BUNSPEC);
				}
				{
					BgL_blockvz00_bglt BgL_auxz00_4446;

					{
						obj_t BgL_auxz00_4447;

						{	/* SawBbv/bbv-types.sch 162 */
							BgL_objectz00_bglt BgL_tmpz00_4448;

							BgL_tmpz00_4448 = ((BgL_objectz00_bglt) BgL_new1179z00_3674);
							BgL_auxz00_4447 = BGL_OBJECT_WIDENING(BgL_tmpz00_4448);
						}
						BgL_auxz00_4446 = ((BgL_blockvz00_bglt) BgL_auxz00_4447);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4446))->BgL_z52markz52) =
						((long) BgL_z52mark1261z52_138), BUNSPEC);
				}
				{
					BgL_blockvz00_bglt BgL_auxz00_4453;

					{
						obj_t BgL_auxz00_4454;

						{	/* SawBbv/bbv-types.sch 162 */
							BgL_objectz00_bglt BgL_tmpz00_4455;

							BgL_tmpz00_4455 = ((BgL_objectz00_bglt) BgL_new1179z00_3674);
							BgL_auxz00_4454 = BGL_OBJECT_WIDENING(BgL_tmpz00_4455);
						}
						BgL_auxz00_4453 = ((BgL_blockvz00_bglt) BgL_auxz00_4454);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4453))->BgL_mergez00) =
						((obj_t) BgL_merge1262z00_139), BUNSPEC);
				}
				return BgL_new1179z00_3674;
			}
		}

	}



/* &make-blockV */
	BgL_blockz00_bglt BGl_z62makezd2blockVzb0zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3397, obj_t BgL_label1255z00_3398, obj_t BgL_preds1256z00_3399,
		obj_t BgL_succs1257z00_3400, obj_t BgL_first1258z00_3401,
		obj_t BgL_versions1259z00_3402, obj_t BgL_generic1260z00_3403,
		obj_t BgL_z52mark1261z52_3404, obj_t BgL_merge1262z00_3405)
	{
		{	/* SawBbv/bbv-types.sch 162 */
			return
				BGl_makezd2blockVzd2zzsaw_bbvzd2costzd2(CINT(BgL_label1255z00_3398),
				BgL_preds1256z00_3399, BgL_succs1257z00_3400, BgL_first1258z00_3401,
				BgL_versions1259z00_3402, BgL_generic1260z00_3403,
				(long) CINT(BgL_z52mark1261z52_3404), BgL_merge1262z00_3405);
		}

	}



/* blockV? */
	BGL_EXPORTED_DEF bool_t BGl_blockVzf3zf3zzsaw_bbvzd2costzd2(obj_t
		BgL_objz00_140)
	{
		{	/* SawBbv/bbv-types.sch 163 */
			{	/* SawBbv/bbv-types.sch 163 */
				obj_t BgL_classz00_3680;

				BgL_classz00_3680 = BGl_blockVz00zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_140))
					{	/* SawBbv/bbv-types.sch 163 */
						BgL_objectz00_bglt BgL_arg1807z00_3681;

						BgL_arg1807z00_3681 = (BgL_objectz00_bglt) (BgL_objz00_140);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 163 */
								long BgL_idxz00_3682;

								BgL_idxz00_3682 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3681);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3682 + 2L)) == BgL_classz00_3680);
							}
						else
							{	/* SawBbv/bbv-types.sch 163 */
								bool_t BgL_res1943z00_3685;

								{	/* SawBbv/bbv-types.sch 163 */
									obj_t BgL_oclassz00_3686;

									{	/* SawBbv/bbv-types.sch 163 */
										obj_t BgL_arg1815z00_3687;
										long BgL_arg1816z00_3688;

										BgL_arg1815z00_3687 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 163 */
											long BgL_arg1817z00_3689;

											BgL_arg1817z00_3689 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3681);
											BgL_arg1816z00_3688 = (BgL_arg1817z00_3689 - OBJECT_TYPE);
										}
										BgL_oclassz00_3686 =
											VECTOR_REF(BgL_arg1815z00_3687, BgL_arg1816z00_3688);
									}
									{	/* SawBbv/bbv-types.sch 163 */
										bool_t BgL__ortest_1115z00_3690;

										BgL__ortest_1115z00_3690 =
											(BgL_classz00_3680 == BgL_oclassz00_3686);
										if (BgL__ortest_1115z00_3690)
											{	/* SawBbv/bbv-types.sch 163 */
												BgL_res1943z00_3685 = BgL__ortest_1115z00_3690;
											}
										else
											{	/* SawBbv/bbv-types.sch 163 */
												long BgL_odepthz00_3691;

												{	/* SawBbv/bbv-types.sch 163 */
													obj_t BgL_arg1804z00_3692;

													BgL_arg1804z00_3692 = (BgL_oclassz00_3686);
													BgL_odepthz00_3691 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3692);
												}
												if ((2L < BgL_odepthz00_3691))
													{	/* SawBbv/bbv-types.sch 163 */
														obj_t BgL_arg1802z00_3693;

														{	/* SawBbv/bbv-types.sch 163 */
															obj_t BgL_arg1803z00_3694;

															BgL_arg1803z00_3694 = (BgL_oclassz00_3686);
															BgL_arg1802z00_3693 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3694,
																2L);
														}
														BgL_res1943z00_3685 =
															(BgL_arg1802z00_3693 == BgL_classz00_3680);
													}
												else
													{	/* SawBbv/bbv-types.sch 163 */
														BgL_res1943z00_3685 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1943z00_3685;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 163 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &blockV? */
	obj_t BGl_z62blockVzf3z91zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3406,
		obj_t BgL_objz00_3407)
	{
		{	/* SawBbv/bbv-types.sch 163 */
			return BBOOL(BGl_blockVzf3zf3zzsaw_bbvzd2costzd2(BgL_objz00_3407));
		}

	}



/* blockV-nil */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_blockVzd2nilzd2zzsaw_bbvzd2costzd2(void)
	{
		{	/* SawBbv/bbv-types.sch 164 */
			{	/* SawBbv/bbv-types.sch 164 */
				obj_t BgL_classz00_2941;

				BgL_classz00_2941 = BGl_blockVz00zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 164 */
					obj_t BgL__ortest_1117z00_2942;

					BgL__ortest_1117z00_2942 = BGL_CLASS_NIL(BgL_classz00_2941);
					if (CBOOL(BgL__ortest_1117z00_2942))
						{	/* SawBbv/bbv-types.sch 164 */
							return ((BgL_blockz00_bglt) BgL__ortest_1117z00_2942);
						}
					else
						{	/* SawBbv/bbv-types.sch 164 */
							return
								((BgL_blockz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2941));
						}
				}
			}
		}

	}



/* &blockV-nil */
	BgL_blockz00_bglt BGl_z62blockVzd2nilzb0zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3408)
	{
		{	/* SawBbv/bbv-types.sch 164 */
			return BGl_blockVzd2nilzd2zzsaw_bbvzd2costzd2();
		}

	}



/* blockV-merge */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2mergezd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_141)
	{
		{	/* SawBbv/bbv-types.sch 165 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4494;

				{
					obj_t BgL_auxz00_4495;

					{	/* SawBbv/bbv-types.sch 165 */
						BgL_objectz00_bglt BgL_tmpz00_4496;

						BgL_tmpz00_4496 = ((BgL_objectz00_bglt) BgL_oz00_141);
						BgL_auxz00_4495 = BGL_OBJECT_WIDENING(BgL_tmpz00_4496);
					}
					BgL_auxz00_4494 = ((BgL_blockvz00_bglt) BgL_auxz00_4495);
				}
				return (((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4494))->BgL_mergez00);
			}
		}

	}



/* &blockV-merge */
	obj_t BGl_z62blockVzd2mergezb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3409,
		obj_t BgL_oz00_3410)
	{
		{	/* SawBbv/bbv-types.sch 165 */
			return
				BGl_blockVzd2mergezd2zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3410));
		}

	}



/* blockV-merge-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2mergezd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_142, obj_t BgL_vz00_143)
	{
		{	/* SawBbv/bbv-types.sch 166 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4503;

				{
					obj_t BgL_auxz00_4504;

					{	/* SawBbv/bbv-types.sch 166 */
						BgL_objectz00_bglt BgL_tmpz00_4505;

						BgL_tmpz00_4505 = ((BgL_objectz00_bglt) BgL_oz00_142);
						BgL_auxz00_4504 = BGL_OBJECT_WIDENING(BgL_tmpz00_4505);
					}
					BgL_auxz00_4503 = ((BgL_blockvz00_bglt) BgL_auxz00_4504);
				}
				return
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4503))->BgL_mergez00) =
					((obj_t) BgL_vz00_143), BUNSPEC);
			}
		}

	}



/* &blockV-merge-set! */
	obj_t BGl_z62blockVzd2mergezd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3411, obj_t BgL_oz00_3412, obj_t BgL_vz00_3413)
	{
		{	/* SawBbv/bbv-types.sch 166 */
			return
				BGl_blockVzd2mergezd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3412), BgL_vz00_3413);
		}

	}



/* blockV-%mark */
	BGL_EXPORTED_DEF long
		BGl_blockVzd2z52markz80zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_144)
	{
		{	/* SawBbv/bbv-types.sch 167 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4512;

				{
					obj_t BgL_auxz00_4513;

					{	/* SawBbv/bbv-types.sch 167 */
						BgL_objectz00_bglt BgL_tmpz00_4514;

						BgL_tmpz00_4514 = ((BgL_objectz00_bglt) BgL_oz00_144);
						BgL_auxz00_4513 = BGL_OBJECT_WIDENING(BgL_tmpz00_4514);
					}
					BgL_auxz00_4512 = ((BgL_blockvz00_bglt) BgL_auxz00_4513);
				}
				return
					(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4512))->BgL_z52markz52);
			}
		}

	}



/* &blockV-%mark */
	obj_t BGl_z62blockVzd2z52markze2zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3414,
		obj_t BgL_oz00_3415)
	{
		{	/* SawBbv/bbv-types.sch 167 */
			return
				BINT(BGl_blockVzd2z52markz80zzsaw_bbvzd2costzd2(
					((BgL_blockz00_bglt) BgL_oz00_3415)));
		}

	}



/* blockV-%mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2z52markzd2setz12z40zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_145, long BgL_vz00_146)
	{
		{	/* SawBbv/bbv-types.sch 168 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4522;

				{
					obj_t BgL_auxz00_4523;

					{	/* SawBbv/bbv-types.sch 168 */
						BgL_objectz00_bglt BgL_tmpz00_4524;

						BgL_tmpz00_4524 = ((BgL_objectz00_bglt) BgL_oz00_145);
						BgL_auxz00_4523 = BGL_OBJECT_WIDENING(BgL_tmpz00_4524);
					}
					BgL_auxz00_4522 = ((BgL_blockvz00_bglt) BgL_auxz00_4523);
				}
				return
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4522))->BgL_z52markz52) =
					((long) BgL_vz00_146), BUNSPEC);
		}}

	}



/* &blockV-%mark-set! */
	obj_t BGl_z62blockVzd2z52markzd2setz12z22zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3416, obj_t BgL_oz00_3417, obj_t BgL_vz00_3418)
	{
		{	/* SawBbv/bbv-types.sch 168 */
			return
				BGl_blockVzd2z52markzd2setz12z40zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3417), (long) CINT(BgL_vz00_3418));
		}

	}



/* blockV-generic */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2genericzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_147)
	{
		{	/* SawBbv/bbv-types.sch 169 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4532;

				{
					obj_t BgL_auxz00_4533;

					{	/* SawBbv/bbv-types.sch 169 */
						BgL_objectz00_bglt BgL_tmpz00_4534;

						BgL_tmpz00_4534 = ((BgL_objectz00_bglt) BgL_oz00_147);
						BgL_auxz00_4533 = BGL_OBJECT_WIDENING(BgL_tmpz00_4534);
					}
					BgL_auxz00_4532 = ((BgL_blockvz00_bglt) BgL_auxz00_4533);
				}
				return
					(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4532))->BgL_genericz00);
			}
		}

	}



/* &blockV-generic */
	obj_t BGl_z62blockVzd2genericzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3419,
		obj_t BgL_oz00_3420)
	{
		{	/* SawBbv/bbv-types.sch 169 */
			return
				BGl_blockVzd2genericzd2zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3420));
		}

	}



/* blockV-generic-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2genericzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_148, obj_t BgL_vz00_149)
	{
		{	/* SawBbv/bbv-types.sch 170 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4541;

				{
					obj_t BgL_auxz00_4542;

					{	/* SawBbv/bbv-types.sch 170 */
						BgL_objectz00_bglt BgL_tmpz00_4543;

						BgL_tmpz00_4543 = ((BgL_objectz00_bglt) BgL_oz00_148);
						BgL_auxz00_4542 = BGL_OBJECT_WIDENING(BgL_tmpz00_4543);
					}
					BgL_auxz00_4541 = ((BgL_blockvz00_bglt) BgL_auxz00_4542);
				}
				return
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4541))->BgL_genericz00) =
					((obj_t) BgL_vz00_149), BUNSPEC);
			}
		}

	}



/* &blockV-generic-set! */
	obj_t BGl_z62blockVzd2genericzd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3421, obj_t BgL_oz00_3422, obj_t BgL_vz00_3423)
	{
		{	/* SawBbv/bbv-types.sch 170 */
			return
				BGl_blockVzd2genericzd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3422), BgL_vz00_3423);
		}

	}



/* blockV-versions */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2versionszd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_150)
	{
		{	/* SawBbv/bbv-types.sch 171 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4550;

				{
					obj_t BgL_auxz00_4551;

					{	/* SawBbv/bbv-types.sch 171 */
						BgL_objectz00_bglt BgL_tmpz00_4552;

						BgL_tmpz00_4552 = ((BgL_objectz00_bglt) BgL_oz00_150);
						BgL_auxz00_4551 = BGL_OBJECT_WIDENING(BgL_tmpz00_4552);
					}
					BgL_auxz00_4550 = ((BgL_blockvz00_bglt) BgL_auxz00_4551);
				}
				return
					(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4550))->BgL_versionsz00);
			}
		}

	}



/* &blockV-versions */
	obj_t BGl_z62blockVzd2versionszb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3424,
		obj_t BgL_oz00_3425)
	{
		{	/* SawBbv/bbv-types.sch 171 */
			return
				BGl_blockVzd2versionszd2zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3425));
		}

	}



/* blockV-versions-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2versionszd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_151, obj_t BgL_vz00_152)
	{
		{	/* SawBbv/bbv-types.sch 172 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4559;

				{
					obj_t BgL_auxz00_4560;

					{	/* SawBbv/bbv-types.sch 172 */
						BgL_objectz00_bglt BgL_tmpz00_4561;

						BgL_tmpz00_4561 = ((BgL_objectz00_bglt) BgL_oz00_151);
						BgL_auxz00_4560 = BGL_OBJECT_WIDENING(BgL_tmpz00_4561);
					}
					BgL_auxz00_4559 = ((BgL_blockvz00_bglt) BgL_auxz00_4560);
				}
				return
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4559))->BgL_versionsz00) =
					((obj_t) BgL_vz00_152), BUNSPEC);
			}
		}

	}



/* &blockV-versions-set! */
	obj_t BGl_z62blockVzd2versionszd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3426, obj_t BgL_oz00_3427, obj_t BgL_vz00_3428)
	{
		{	/* SawBbv/bbv-types.sch 172 */
			return
				BGl_blockVzd2versionszd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3427), BgL_vz00_3428);
		}

	}



/* blockV-first */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2firstzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_153)
	{
		{	/* SawBbv/bbv-types.sch 173 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_153)))->BgL_firstz00);
		}

	}



/* &blockV-first */
	obj_t BGl_z62blockVzd2firstzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3429,
		obj_t BgL_oz00_3430)
	{
		{	/* SawBbv/bbv-types.sch 173 */
			return
				BGl_blockVzd2firstzd2zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3430));
		}

	}



/* blockV-first-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2firstzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_154, obj_t BgL_vz00_155)
	{
		{	/* SawBbv/bbv-types.sch 174 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_154)))->BgL_firstz00) =
				((obj_t) BgL_vz00_155), BUNSPEC);
		}

	}



/* &blockV-first-set! */
	obj_t BGl_z62blockVzd2firstzd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3431, obj_t BgL_oz00_3432, obj_t BgL_vz00_3433)
	{
		{	/* SawBbv/bbv-types.sch 174 */
			return
				BGl_blockVzd2firstzd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3432), BgL_vz00_3433);
		}

	}



/* blockV-succs */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2succszd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_156)
	{
		{	/* SawBbv/bbv-types.sch 175 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_156)))->BgL_succsz00);
		}

	}



/* &blockV-succs */
	obj_t BGl_z62blockVzd2succszb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3434,
		obj_t BgL_oz00_3435)
	{
		{	/* SawBbv/bbv-types.sch 175 */
			return
				BGl_blockVzd2succszd2zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3435));
		}

	}



/* blockV-succs-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2succszd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_157, obj_t BgL_vz00_158)
	{
		{	/* SawBbv/bbv-types.sch 176 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_157)))->BgL_succsz00) =
				((obj_t) BgL_vz00_158), BUNSPEC);
		}

	}



/* &blockV-succs-set! */
	obj_t BGl_z62blockVzd2succszd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3436, obj_t BgL_oz00_3437, obj_t BgL_vz00_3438)
	{
		{	/* SawBbv/bbv-types.sch 176 */
			return
				BGl_blockVzd2succszd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3437), BgL_vz00_3438);
		}

	}



/* blockV-preds */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2predszd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_159)
	{
		{	/* SawBbv/bbv-types.sch 177 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_159)))->BgL_predsz00);
		}

	}



/* &blockV-preds */
	obj_t BGl_z62blockVzd2predszb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3439,
		obj_t BgL_oz00_3440)
	{
		{	/* SawBbv/bbv-types.sch 177 */
			return
				BGl_blockVzd2predszd2zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3440));
		}

	}



/* blockV-preds-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2predszd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_160, obj_t BgL_vz00_161)
	{
		{	/* SawBbv/bbv-types.sch 178 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_160)))->BgL_predsz00) =
				((obj_t) BgL_vz00_161), BUNSPEC);
		}

	}



/* &blockV-preds-set! */
	obj_t BGl_z62blockVzd2predszd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3441, obj_t BgL_oz00_3442, obj_t BgL_vz00_3443)
	{
		{	/* SawBbv/bbv-types.sch 178 */
			return
				BGl_blockVzd2predszd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3442), BgL_vz00_3443);
		}

	}



/* blockV-label */
	BGL_EXPORTED_DEF int
		BGl_blockVzd2labelzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_162)
	{
		{	/* SawBbv/bbv-types.sch 179 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_162)))->BgL_labelz00);
		}

	}



/* &blockV-label */
	obj_t BGl_z62blockVzd2labelzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3444,
		obj_t BgL_oz00_3445)
	{
		{	/* SawBbv/bbv-types.sch 179 */
			return
				BINT(BGl_blockVzd2labelzd2zzsaw_bbvzd2costzd2(
					((BgL_blockz00_bglt) BgL_oz00_3445)));
		}

	}



/* blockV-label-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2labelzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_163, int BgL_vz00_164)
	{
		{	/* SawBbv/bbv-types.sch 180 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_163)))->BgL_labelz00) =
				((int) BgL_vz00_164), BUNSPEC);
		}

	}



/* &blockV-label-set! */
	obj_t BGl_z62blockVzd2labelzd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3446, obj_t BgL_oz00_3447, obj_t BgL_vz00_3448)
	{
		{	/* SawBbv/bbv-types.sch 180 */
			return
				BGl_blockVzd2labelzd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3447), CINT(BgL_vz00_3448));
		}

	}



/* make-blockS */
	BGL_EXPORTED_DEF BgL_blockz00_bglt BGl_makezd2blockSzd2zzsaw_bbvzd2costzd2(int
		BgL_label1239z00_165, obj_t BgL_preds1240z00_166,
		obj_t BgL_succs1241z00_167, obj_t BgL_first1242z00_168,
		long BgL_z52mark1243z52_169, obj_t BgL_z52hash1244z52_170,
		obj_t BgL_z52blacklist1245z52_171, BgL_bbvzd2ctxzd2_bglt BgL_ctx1246z00_172,
		BgL_blockz00_bglt BgL_parent1247z00_173, long BgL_gccnt1248z00_174,
		long BgL_gcmark1249z00_175, obj_t BgL_mblock1250z00_176,
		obj_t BgL_creator1251z00_177, obj_t BgL_merges1252z00_178,
		bool_t BgL_asleep1253z00_179)
	{
		{	/* SawBbv/bbv-types.sch 183 */
			{	/* SawBbv/bbv-types.sch 183 */
				BgL_blockz00_bglt BgL_new1183z00_3695;

				{	/* SawBbv/bbv-types.sch 183 */
					BgL_blockz00_bglt BgL_tmp1181z00_3696;
					BgL_blocksz00_bglt BgL_wide1182z00_3697;

					{
						BgL_blockz00_bglt BgL_auxz00_4602;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_blockz00_bglt BgL_new1180z00_3698;

							BgL_new1180z00_3698 =
								((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_blockz00_bgl))));
							{	/* SawBbv/bbv-types.sch 183 */
								long BgL_arg1602z00_3699;

								BgL_arg1602z00_3699 = BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1180z00_3698),
									BgL_arg1602z00_3699);
							}
							{	/* SawBbv/bbv-types.sch 183 */
								BgL_objectz00_bglt BgL_tmpz00_4607;

								BgL_tmpz00_4607 = ((BgL_objectz00_bglt) BgL_new1180z00_3698);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4607, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1180z00_3698);
							BgL_auxz00_4602 = BgL_new1180z00_3698;
						}
						BgL_tmp1181z00_3696 = ((BgL_blockz00_bglt) BgL_auxz00_4602);
					}
					BgL_wide1182z00_3697 =
						((BgL_blocksz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_blocksz00_bgl))));
					{	/* SawBbv/bbv-types.sch 183 */
						obj_t BgL_auxz00_4615;
						BgL_objectz00_bglt BgL_tmpz00_4613;

						BgL_auxz00_4615 = ((obj_t) BgL_wide1182z00_3697);
						BgL_tmpz00_4613 = ((BgL_objectz00_bglt) BgL_tmp1181z00_3696);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4613, BgL_auxz00_4615);
					}
					((BgL_objectz00_bglt) BgL_tmp1181z00_3696);
					{	/* SawBbv/bbv-types.sch 183 */
						long BgL_arg1595z00_3700;

						BgL_arg1595z00_3700 =
							BGL_CLASS_NUM(BGl_blockSz00zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1181z00_3696), BgL_arg1595z00_3700);
					}
					BgL_new1183z00_3695 = ((BgL_blockz00_bglt) BgL_tmp1181z00_3696);
				}
				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_new1183z00_3695)))->BgL_labelz00) =
					((int) BgL_label1239z00_165), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1183z00_3695)))->BgL_predsz00) =
					((obj_t) BgL_preds1240z00_166), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1183z00_3695)))->BgL_succsz00) =
					((obj_t) BgL_succs1241z00_167), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1183z00_3695)))->BgL_firstz00) =
					((obj_t) BgL_first1242z00_168), BUNSPEC);
				{
					BgL_blocksz00_bglt BgL_auxz00_4631;

					{
						obj_t BgL_auxz00_4632;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_4633;

							BgL_tmpz00_4633 = ((BgL_objectz00_bglt) BgL_new1183z00_3695);
							BgL_auxz00_4632 = BGL_OBJECT_WIDENING(BgL_tmpz00_4633);
						}
						BgL_auxz00_4631 = ((BgL_blocksz00_bglt) BgL_auxz00_4632);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4631))->BgL_z52markz52) =
						((long) BgL_z52mark1243z52_169), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_4638;

					{
						obj_t BgL_auxz00_4639;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_4640;

							BgL_tmpz00_4640 = ((BgL_objectz00_bglt) BgL_new1183z00_3695);
							BgL_auxz00_4639 = BGL_OBJECT_WIDENING(BgL_tmpz00_4640);
						}
						BgL_auxz00_4638 = ((BgL_blocksz00_bglt) BgL_auxz00_4639);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4638))->BgL_z52hashz52) =
						((obj_t) BgL_z52hash1244z52_170), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_4645;

					{
						obj_t BgL_auxz00_4646;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_4647;

							BgL_tmpz00_4647 = ((BgL_objectz00_bglt) BgL_new1183z00_3695);
							BgL_auxz00_4646 = BGL_OBJECT_WIDENING(BgL_tmpz00_4647);
						}
						BgL_auxz00_4645 = ((BgL_blocksz00_bglt) BgL_auxz00_4646);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4645))->
							BgL_z52blacklistz52) =
						((obj_t) BgL_z52blacklist1245z52_171), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_4652;

					{
						obj_t BgL_auxz00_4653;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_4654;

							BgL_tmpz00_4654 = ((BgL_objectz00_bglt) BgL_new1183z00_3695);
							BgL_auxz00_4653 = BGL_OBJECT_WIDENING(BgL_tmpz00_4654);
						}
						BgL_auxz00_4652 = ((BgL_blocksz00_bglt) BgL_auxz00_4653);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4652))->BgL_ctxz00) =
						((BgL_bbvzd2ctxzd2_bglt) BgL_ctx1246z00_172), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_4659;

					{
						obj_t BgL_auxz00_4660;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_4661;

							BgL_tmpz00_4661 = ((BgL_objectz00_bglt) BgL_new1183z00_3695);
							BgL_auxz00_4660 = BGL_OBJECT_WIDENING(BgL_tmpz00_4661);
						}
						BgL_auxz00_4659 = ((BgL_blocksz00_bglt) BgL_auxz00_4660);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4659))->BgL_parentz00) =
						((BgL_blockz00_bglt) BgL_parent1247z00_173), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_4666;

					{
						obj_t BgL_auxz00_4667;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_4668;

							BgL_tmpz00_4668 = ((BgL_objectz00_bglt) BgL_new1183z00_3695);
							BgL_auxz00_4667 = BGL_OBJECT_WIDENING(BgL_tmpz00_4668);
						}
						BgL_auxz00_4666 = ((BgL_blocksz00_bglt) BgL_auxz00_4667);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4666))->BgL_gccntz00) =
						((long) BgL_gccnt1248z00_174), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_4673;

					{
						obj_t BgL_auxz00_4674;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_4675;

							BgL_tmpz00_4675 = ((BgL_objectz00_bglt) BgL_new1183z00_3695);
							BgL_auxz00_4674 = BGL_OBJECT_WIDENING(BgL_tmpz00_4675);
						}
						BgL_auxz00_4673 = ((BgL_blocksz00_bglt) BgL_auxz00_4674);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4673))->BgL_gcmarkz00) =
						((long) BgL_gcmark1249z00_175), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_4680;

					{
						obj_t BgL_auxz00_4681;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_4682;

							BgL_tmpz00_4682 = ((BgL_objectz00_bglt) BgL_new1183z00_3695);
							BgL_auxz00_4681 = BGL_OBJECT_WIDENING(BgL_tmpz00_4682);
						}
						BgL_auxz00_4680 = ((BgL_blocksz00_bglt) BgL_auxz00_4681);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4680))->BgL_mblockz00) =
						((obj_t) BgL_mblock1250z00_176), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_4687;

					{
						obj_t BgL_auxz00_4688;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_4689;

							BgL_tmpz00_4689 = ((BgL_objectz00_bglt) BgL_new1183z00_3695);
							BgL_auxz00_4688 = BGL_OBJECT_WIDENING(BgL_tmpz00_4689);
						}
						BgL_auxz00_4687 = ((BgL_blocksz00_bglt) BgL_auxz00_4688);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4687))->BgL_creatorz00) =
						((obj_t) BgL_creator1251z00_177), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_4694;

					{
						obj_t BgL_auxz00_4695;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_4696;

							BgL_tmpz00_4696 = ((BgL_objectz00_bglt) BgL_new1183z00_3695);
							BgL_auxz00_4695 = BGL_OBJECT_WIDENING(BgL_tmpz00_4696);
						}
						BgL_auxz00_4694 = ((BgL_blocksz00_bglt) BgL_auxz00_4695);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4694))->BgL_mergesz00) =
						((obj_t) BgL_merges1252z00_178), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_4701;

					{
						obj_t BgL_auxz00_4702;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_4703;

							BgL_tmpz00_4703 = ((BgL_objectz00_bglt) BgL_new1183z00_3695);
							BgL_auxz00_4702 = BGL_OBJECT_WIDENING(BgL_tmpz00_4703);
						}
						BgL_auxz00_4701 = ((BgL_blocksz00_bglt) BgL_auxz00_4702);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4701))->BgL_asleepz00) =
						((bool_t) BgL_asleep1253z00_179), BUNSPEC);
				}
				return BgL_new1183z00_3695;
			}
		}

	}



/* &make-blockS */
	BgL_blockz00_bglt BGl_z62makezd2blockSzb0zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3449, obj_t BgL_label1239z00_3450, obj_t BgL_preds1240z00_3451,
		obj_t BgL_succs1241z00_3452, obj_t BgL_first1242z00_3453,
		obj_t BgL_z52mark1243z52_3454, obj_t BgL_z52hash1244z52_3455,
		obj_t BgL_z52blacklist1245z52_3456, obj_t BgL_ctx1246z00_3457,
		obj_t BgL_parent1247z00_3458, obj_t BgL_gccnt1248z00_3459,
		obj_t BgL_gcmark1249z00_3460, obj_t BgL_mblock1250z00_3461,
		obj_t BgL_creator1251z00_3462, obj_t BgL_merges1252z00_3463,
		obj_t BgL_asleep1253z00_3464)
	{
		{	/* SawBbv/bbv-types.sch 183 */
			return
				BGl_makezd2blockSzd2zzsaw_bbvzd2costzd2(CINT(BgL_label1239z00_3450),
				BgL_preds1240z00_3451, BgL_succs1241z00_3452, BgL_first1242z00_3453,
				(long) CINT(BgL_z52mark1243z52_3454), BgL_z52hash1244z52_3455,
				BgL_z52blacklist1245z52_3456,
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctx1246z00_3457),
				((BgL_blockz00_bglt) BgL_parent1247z00_3458),
				(long) CINT(BgL_gccnt1248z00_3459), (long) CINT(BgL_gcmark1249z00_3460),
				BgL_mblock1250z00_3461, BgL_creator1251z00_3462, BgL_merges1252z00_3463,
				CBOOL(BgL_asleep1253z00_3464));
		}

	}



/* blockS? */
	BGL_EXPORTED_DEF bool_t BGl_blockSzf3zf3zzsaw_bbvzd2costzd2(obj_t
		BgL_objz00_180)
	{
		{	/* SawBbv/bbv-types.sch 184 */
			{	/* SawBbv/bbv-types.sch 184 */
				obj_t BgL_classz00_3701;

				BgL_classz00_3701 = BGl_blockSz00zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_180))
					{	/* SawBbv/bbv-types.sch 184 */
						BgL_objectz00_bglt BgL_arg1807z00_3702;

						BgL_arg1807z00_3702 = (BgL_objectz00_bglt) (BgL_objz00_180);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 184 */
								long BgL_idxz00_3703;

								BgL_idxz00_3703 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3702);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3703 + 2L)) == BgL_classz00_3701);
							}
						else
							{	/* SawBbv/bbv-types.sch 184 */
								bool_t BgL_res1944z00_3706;

								{	/* SawBbv/bbv-types.sch 184 */
									obj_t BgL_oclassz00_3707;

									{	/* SawBbv/bbv-types.sch 184 */
										obj_t BgL_arg1815z00_3708;
										long BgL_arg1816z00_3709;

										BgL_arg1815z00_3708 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 184 */
											long BgL_arg1817z00_3710;

											BgL_arg1817z00_3710 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3702);
											BgL_arg1816z00_3709 = (BgL_arg1817z00_3710 - OBJECT_TYPE);
										}
										BgL_oclassz00_3707 =
											VECTOR_REF(BgL_arg1815z00_3708, BgL_arg1816z00_3709);
									}
									{	/* SawBbv/bbv-types.sch 184 */
										bool_t BgL__ortest_1115z00_3711;

										BgL__ortest_1115z00_3711 =
											(BgL_classz00_3701 == BgL_oclassz00_3707);
										if (BgL__ortest_1115z00_3711)
											{	/* SawBbv/bbv-types.sch 184 */
												BgL_res1944z00_3706 = BgL__ortest_1115z00_3711;
											}
										else
											{	/* SawBbv/bbv-types.sch 184 */
												long BgL_odepthz00_3712;

												{	/* SawBbv/bbv-types.sch 184 */
													obj_t BgL_arg1804z00_3713;

													BgL_arg1804z00_3713 = (BgL_oclassz00_3707);
													BgL_odepthz00_3712 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3713);
												}
												if ((2L < BgL_odepthz00_3712))
													{	/* SawBbv/bbv-types.sch 184 */
														obj_t BgL_arg1802z00_3714;

														{	/* SawBbv/bbv-types.sch 184 */
															obj_t BgL_arg1803z00_3715;

															BgL_arg1803z00_3715 = (BgL_oclassz00_3707);
															BgL_arg1802z00_3714 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3715,
																2L);
														}
														BgL_res1944z00_3706 =
															(BgL_arg1802z00_3714 == BgL_classz00_3701);
													}
												else
													{	/* SawBbv/bbv-types.sch 184 */
														BgL_res1944z00_3706 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1944z00_3706;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 184 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &blockS? */
	obj_t BGl_z62blockSzf3z91zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3465,
		obj_t BgL_objz00_3466)
	{
		{	/* SawBbv/bbv-types.sch 184 */
			return BBOOL(BGl_blockSzf3zf3zzsaw_bbvzd2costzd2(BgL_objz00_3466));
		}

	}



/* blockS-nil */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_blockSzd2nilzd2zzsaw_bbvzd2costzd2(void)
	{
		{	/* SawBbv/bbv-types.sch 185 */
			{	/* SawBbv/bbv-types.sch 185 */
				obj_t BgL_classz00_3010;

				BgL_classz00_3010 = BGl_blockSz00zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 185 */
					obj_t BgL__ortest_1117z00_3011;

					BgL__ortest_1117z00_3011 = BGL_CLASS_NIL(BgL_classz00_3010);
					if (CBOOL(BgL__ortest_1117z00_3011))
						{	/* SawBbv/bbv-types.sch 185 */
							return ((BgL_blockz00_bglt) BgL__ortest_1117z00_3011);
						}
					else
						{	/* SawBbv/bbv-types.sch 185 */
							return
								((BgL_blockz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3010));
						}
				}
			}
		}

	}



/* &blockS-nil */
	BgL_blockz00_bglt BGl_z62blockSzd2nilzb0zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3467)
	{
		{	/* SawBbv/bbv-types.sch 185 */
			return BGl_blockSzd2nilzd2zzsaw_bbvzd2costzd2();
		}

	}



/* blockS-asleep */
	BGL_EXPORTED_DEF bool_t
		BGl_blockSzd2asleepzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_181)
	{
		{	/* SawBbv/bbv-types.sch 186 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4747;

				{
					obj_t BgL_auxz00_4748;

					{	/* SawBbv/bbv-types.sch 186 */
						BgL_objectz00_bglt BgL_tmpz00_4749;

						BgL_tmpz00_4749 = ((BgL_objectz00_bglt) BgL_oz00_181);
						BgL_auxz00_4748 = BGL_OBJECT_WIDENING(BgL_tmpz00_4749);
					}
					BgL_auxz00_4747 = ((BgL_blocksz00_bglt) BgL_auxz00_4748);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4747))->BgL_asleepz00);
			}
		}

	}



/* &blockS-asleep */
	obj_t BGl_z62blockSzd2asleepzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3468,
		obj_t BgL_oz00_3469)
	{
		{	/* SawBbv/bbv-types.sch 186 */
			return
				BBOOL(BGl_blockSzd2asleepzd2zzsaw_bbvzd2costzd2(
					((BgL_blockz00_bglt) BgL_oz00_3469)));
		}

	}



/* blockS-asleep-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2asleepzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_182, bool_t BgL_vz00_183)
	{
		{	/* SawBbv/bbv-types.sch 187 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4757;

				{
					obj_t BgL_auxz00_4758;

					{	/* SawBbv/bbv-types.sch 187 */
						BgL_objectz00_bglt BgL_tmpz00_4759;

						BgL_tmpz00_4759 = ((BgL_objectz00_bglt) BgL_oz00_182);
						BgL_auxz00_4758 = BGL_OBJECT_WIDENING(BgL_tmpz00_4759);
					}
					BgL_auxz00_4757 = ((BgL_blocksz00_bglt) BgL_auxz00_4758);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4757))->BgL_asleepz00) =
					((bool_t) BgL_vz00_183), BUNSPEC);
			}
		}

	}



/* &blockS-asleep-set! */
	obj_t BGl_z62blockSzd2asleepzd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3470, obj_t BgL_oz00_3471, obj_t BgL_vz00_3472)
	{
		{	/* SawBbv/bbv-types.sch 187 */
			return
				BGl_blockSzd2asleepzd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3471), CBOOL(BgL_vz00_3472));
		}

	}



/* blockS-merges */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2mergeszd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_184)
	{
		{	/* SawBbv/bbv-types.sch 188 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4767;

				{
					obj_t BgL_auxz00_4768;

					{	/* SawBbv/bbv-types.sch 188 */
						BgL_objectz00_bglt BgL_tmpz00_4769;

						BgL_tmpz00_4769 = ((BgL_objectz00_bglt) BgL_oz00_184);
						BgL_auxz00_4768 = BGL_OBJECT_WIDENING(BgL_tmpz00_4769);
					}
					BgL_auxz00_4767 = ((BgL_blocksz00_bglt) BgL_auxz00_4768);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4767))->BgL_mergesz00);
			}
		}

	}



/* &blockS-merges */
	obj_t BGl_z62blockSzd2mergeszb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3473,
		obj_t BgL_oz00_3474)
	{
		{	/* SawBbv/bbv-types.sch 188 */
			return
				BGl_blockSzd2mergeszd2zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3474));
		}

	}



/* blockS-merges-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2mergeszd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_185, obj_t BgL_vz00_186)
	{
		{	/* SawBbv/bbv-types.sch 189 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4776;

				{
					obj_t BgL_auxz00_4777;

					{	/* SawBbv/bbv-types.sch 189 */
						BgL_objectz00_bglt BgL_tmpz00_4778;

						BgL_tmpz00_4778 = ((BgL_objectz00_bglt) BgL_oz00_185);
						BgL_auxz00_4777 = BGL_OBJECT_WIDENING(BgL_tmpz00_4778);
					}
					BgL_auxz00_4776 = ((BgL_blocksz00_bglt) BgL_auxz00_4777);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4776))->BgL_mergesz00) =
					((obj_t) BgL_vz00_186), BUNSPEC);
			}
		}

	}



/* &blockS-merges-set! */
	obj_t BGl_z62blockSzd2mergeszd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3475, obj_t BgL_oz00_3476, obj_t BgL_vz00_3477)
	{
		{	/* SawBbv/bbv-types.sch 189 */
			return
				BGl_blockSzd2mergeszd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3476), BgL_vz00_3477);
		}

	}



/* blockS-creator */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2creatorzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_187)
	{
		{	/* SawBbv/bbv-types.sch 190 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4785;

				{
					obj_t BgL_auxz00_4786;

					{	/* SawBbv/bbv-types.sch 190 */
						BgL_objectz00_bglt BgL_tmpz00_4787;

						BgL_tmpz00_4787 = ((BgL_objectz00_bglt) BgL_oz00_187);
						BgL_auxz00_4786 = BGL_OBJECT_WIDENING(BgL_tmpz00_4787);
					}
					BgL_auxz00_4785 = ((BgL_blocksz00_bglt) BgL_auxz00_4786);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4785))->BgL_creatorz00);
			}
		}

	}



/* &blockS-creator */
	obj_t BGl_z62blockSzd2creatorzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3478,
		obj_t BgL_oz00_3479)
	{
		{	/* SawBbv/bbv-types.sch 190 */
			return
				BGl_blockSzd2creatorzd2zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3479));
		}

	}



/* blockS-mblock */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2mblockzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_190)
	{
		{	/* SawBbv/bbv-types.sch 192 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4794;

				{
					obj_t BgL_auxz00_4795;

					{	/* SawBbv/bbv-types.sch 192 */
						BgL_objectz00_bglt BgL_tmpz00_4796;

						BgL_tmpz00_4796 = ((BgL_objectz00_bglt) BgL_oz00_190);
						BgL_auxz00_4795 = BGL_OBJECT_WIDENING(BgL_tmpz00_4796);
					}
					BgL_auxz00_4794 = ((BgL_blocksz00_bglt) BgL_auxz00_4795);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4794))->BgL_mblockz00);
			}
		}

	}



/* &blockS-mblock */
	obj_t BGl_z62blockSzd2mblockzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3480,
		obj_t BgL_oz00_3481)
	{
		{	/* SawBbv/bbv-types.sch 192 */
			return
				BGl_blockSzd2mblockzd2zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3481));
		}

	}



/* blockS-mblock-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2mblockzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_191, obj_t BgL_vz00_192)
	{
		{	/* SawBbv/bbv-types.sch 193 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4803;

				{
					obj_t BgL_auxz00_4804;

					{	/* SawBbv/bbv-types.sch 193 */
						BgL_objectz00_bglt BgL_tmpz00_4805;

						BgL_tmpz00_4805 = ((BgL_objectz00_bglt) BgL_oz00_191);
						BgL_auxz00_4804 = BGL_OBJECT_WIDENING(BgL_tmpz00_4805);
					}
					BgL_auxz00_4803 = ((BgL_blocksz00_bglt) BgL_auxz00_4804);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4803))->BgL_mblockz00) =
					((obj_t) BgL_vz00_192), BUNSPEC);
			}
		}

	}



/* &blockS-mblock-set! */
	obj_t BGl_z62blockSzd2mblockzd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3482, obj_t BgL_oz00_3483, obj_t BgL_vz00_3484)
	{
		{	/* SawBbv/bbv-types.sch 193 */
			return
				BGl_blockSzd2mblockzd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3483), BgL_vz00_3484);
		}

	}



/* blockS-gcmark */
	BGL_EXPORTED_DEF long
		BGl_blockSzd2gcmarkzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_193)
	{
		{	/* SawBbv/bbv-types.sch 194 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4812;

				{
					obj_t BgL_auxz00_4813;

					{	/* SawBbv/bbv-types.sch 194 */
						BgL_objectz00_bglt BgL_tmpz00_4814;

						BgL_tmpz00_4814 = ((BgL_objectz00_bglt) BgL_oz00_193);
						BgL_auxz00_4813 = BGL_OBJECT_WIDENING(BgL_tmpz00_4814);
					}
					BgL_auxz00_4812 = ((BgL_blocksz00_bglt) BgL_auxz00_4813);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4812))->BgL_gcmarkz00);
			}
		}

	}



/* &blockS-gcmark */
	obj_t BGl_z62blockSzd2gcmarkzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3485,
		obj_t BgL_oz00_3486)
	{
		{	/* SawBbv/bbv-types.sch 194 */
			return
				BINT(BGl_blockSzd2gcmarkzd2zzsaw_bbvzd2costzd2(
					((BgL_blockz00_bglt) BgL_oz00_3486)));
		}

	}



/* blockS-gcmark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2gcmarkzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_194, long BgL_vz00_195)
	{
		{	/* SawBbv/bbv-types.sch 195 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4822;

				{
					obj_t BgL_auxz00_4823;

					{	/* SawBbv/bbv-types.sch 195 */
						BgL_objectz00_bglt BgL_tmpz00_4824;

						BgL_tmpz00_4824 = ((BgL_objectz00_bglt) BgL_oz00_194);
						BgL_auxz00_4823 = BGL_OBJECT_WIDENING(BgL_tmpz00_4824);
					}
					BgL_auxz00_4822 = ((BgL_blocksz00_bglt) BgL_auxz00_4823);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4822))->BgL_gcmarkz00) =
					((long) BgL_vz00_195), BUNSPEC);
		}}

	}



/* &blockS-gcmark-set! */
	obj_t BGl_z62blockSzd2gcmarkzd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3487, obj_t BgL_oz00_3488, obj_t BgL_vz00_3489)
	{
		{	/* SawBbv/bbv-types.sch 195 */
			return
				BGl_blockSzd2gcmarkzd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3488), (long) CINT(BgL_vz00_3489));
		}

	}



/* blockS-gccnt */
	BGL_EXPORTED_DEF long
		BGl_blockSzd2gccntzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_196)
	{
		{	/* SawBbv/bbv-types.sch 196 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4832;

				{
					obj_t BgL_auxz00_4833;

					{	/* SawBbv/bbv-types.sch 196 */
						BgL_objectz00_bglt BgL_tmpz00_4834;

						BgL_tmpz00_4834 = ((BgL_objectz00_bglt) BgL_oz00_196);
						BgL_auxz00_4833 = BGL_OBJECT_WIDENING(BgL_tmpz00_4834);
					}
					BgL_auxz00_4832 = ((BgL_blocksz00_bglt) BgL_auxz00_4833);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4832))->BgL_gccntz00);
			}
		}

	}



/* &blockS-gccnt */
	obj_t BGl_z62blockSzd2gccntzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3490,
		obj_t BgL_oz00_3491)
	{
		{	/* SawBbv/bbv-types.sch 196 */
			return
				BINT(BGl_blockSzd2gccntzd2zzsaw_bbvzd2costzd2(
					((BgL_blockz00_bglt) BgL_oz00_3491)));
		}

	}



/* blockS-gccnt-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2gccntzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_197, long BgL_vz00_198)
	{
		{	/* SawBbv/bbv-types.sch 197 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4842;

				{
					obj_t BgL_auxz00_4843;

					{	/* SawBbv/bbv-types.sch 197 */
						BgL_objectz00_bglt BgL_tmpz00_4844;

						BgL_tmpz00_4844 = ((BgL_objectz00_bglt) BgL_oz00_197);
						BgL_auxz00_4843 = BGL_OBJECT_WIDENING(BgL_tmpz00_4844);
					}
					BgL_auxz00_4842 = ((BgL_blocksz00_bglt) BgL_auxz00_4843);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4842))->BgL_gccntz00) =
					((long) BgL_vz00_198), BUNSPEC);
		}}

	}



/* &blockS-gccnt-set! */
	obj_t BGl_z62blockSzd2gccntzd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3492, obj_t BgL_oz00_3493, obj_t BgL_vz00_3494)
	{
		{	/* SawBbv/bbv-types.sch 197 */
			return
				BGl_blockSzd2gccntzd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3493), (long) CINT(BgL_vz00_3494));
		}

	}



/* blockS-parent */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_blockSzd2parentzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_199)
	{
		{	/* SawBbv/bbv-types.sch 198 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4852;

				{
					obj_t BgL_auxz00_4853;

					{	/* SawBbv/bbv-types.sch 198 */
						BgL_objectz00_bglt BgL_tmpz00_4854;

						BgL_tmpz00_4854 = ((BgL_objectz00_bglt) BgL_oz00_199);
						BgL_auxz00_4853 = BGL_OBJECT_WIDENING(BgL_tmpz00_4854);
					}
					BgL_auxz00_4852 = ((BgL_blocksz00_bglt) BgL_auxz00_4853);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4852))->BgL_parentz00);
			}
		}

	}



/* &blockS-parent */
	BgL_blockz00_bglt BGl_z62blockSzd2parentzb0zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3495, obj_t BgL_oz00_3496)
	{
		{	/* SawBbv/bbv-types.sch 198 */
			return
				BGl_blockSzd2parentzd2zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3496));
		}

	}



/* blockS-ctx */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_blockSzd2ctxzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_202)
	{
		{	/* SawBbv/bbv-types.sch 200 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4861;

				{
					obj_t BgL_auxz00_4862;

					{	/* SawBbv/bbv-types.sch 200 */
						BgL_objectz00_bglt BgL_tmpz00_4863;

						BgL_tmpz00_4863 = ((BgL_objectz00_bglt) BgL_oz00_202);
						BgL_auxz00_4862 = BGL_OBJECT_WIDENING(BgL_tmpz00_4863);
					}
					BgL_auxz00_4861 = ((BgL_blocksz00_bglt) BgL_auxz00_4862);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4861))->BgL_ctxz00);
			}
		}

	}



/* &blockS-ctx */
	BgL_bbvzd2ctxzd2_bglt BGl_z62blockSzd2ctxzb0zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3497, obj_t BgL_oz00_3498)
	{
		{	/* SawBbv/bbv-types.sch 200 */
			return
				BGl_blockSzd2ctxzd2zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3498));
		}

	}



/* blockS-%blacklist */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2z52blacklistz80zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_205)
	{
		{	/* SawBbv/bbv-types.sch 202 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4870;

				{
					obj_t BgL_auxz00_4871;

					{	/* SawBbv/bbv-types.sch 202 */
						BgL_objectz00_bglt BgL_tmpz00_4872;

						BgL_tmpz00_4872 = ((BgL_objectz00_bglt) BgL_oz00_205);
						BgL_auxz00_4871 = BGL_OBJECT_WIDENING(BgL_tmpz00_4872);
					}
					BgL_auxz00_4870 = ((BgL_blocksz00_bglt) BgL_auxz00_4871);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4870))->
					BgL_z52blacklistz52);
			}
		}

	}



/* &blockS-%blacklist */
	obj_t BGl_z62blockSzd2z52blacklistze2zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3499, obj_t BgL_oz00_3500)
	{
		{	/* SawBbv/bbv-types.sch 202 */
			return
				BGl_blockSzd2z52blacklistz80zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3500));
		}

	}



/* blockS-%blacklist-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2z52blacklistzd2setz12z40zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_206, obj_t BgL_vz00_207)
	{
		{	/* SawBbv/bbv-types.sch 203 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4879;

				{
					obj_t BgL_auxz00_4880;

					{	/* SawBbv/bbv-types.sch 203 */
						BgL_objectz00_bglt BgL_tmpz00_4881;

						BgL_tmpz00_4881 = ((BgL_objectz00_bglt) BgL_oz00_206);
						BgL_auxz00_4880 = BGL_OBJECT_WIDENING(BgL_tmpz00_4881);
					}
					BgL_auxz00_4879 = ((BgL_blocksz00_bglt) BgL_auxz00_4880);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4879))->
						BgL_z52blacklistz52) = ((obj_t) BgL_vz00_207), BUNSPEC);
			}
		}

	}



/* &blockS-%blacklist-set! */
	obj_t BGl_z62blockSzd2z52blacklistzd2setz12z22zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3501, obj_t BgL_oz00_3502, obj_t BgL_vz00_3503)
	{
		{	/* SawBbv/bbv-types.sch 203 */
			return
				BGl_blockSzd2z52blacklistzd2setz12z40zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3502), BgL_vz00_3503);
		}

	}



/* blockS-%hash */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2z52hashz80zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_208)
	{
		{	/* SawBbv/bbv-types.sch 204 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4888;

				{
					obj_t BgL_auxz00_4889;

					{	/* SawBbv/bbv-types.sch 204 */
						BgL_objectz00_bglt BgL_tmpz00_4890;

						BgL_tmpz00_4890 = ((BgL_objectz00_bglt) BgL_oz00_208);
						BgL_auxz00_4889 = BGL_OBJECT_WIDENING(BgL_tmpz00_4890);
					}
					BgL_auxz00_4888 = ((BgL_blocksz00_bglt) BgL_auxz00_4889);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4888))->BgL_z52hashz52);
			}
		}

	}



/* &blockS-%hash */
	obj_t BGl_z62blockSzd2z52hashze2zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3504,
		obj_t BgL_oz00_3505)
	{
		{	/* SawBbv/bbv-types.sch 204 */
			return
				BGl_blockSzd2z52hashz80zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3505));
		}

	}



/* blockS-%hash-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2z52hashzd2setz12z40zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_209, obj_t BgL_vz00_210)
	{
		{	/* SawBbv/bbv-types.sch 205 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4897;

				{
					obj_t BgL_auxz00_4898;

					{	/* SawBbv/bbv-types.sch 205 */
						BgL_objectz00_bglt BgL_tmpz00_4899;

						BgL_tmpz00_4899 = ((BgL_objectz00_bglt) BgL_oz00_209);
						BgL_auxz00_4898 = BGL_OBJECT_WIDENING(BgL_tmpz00_4899);
					}
					BgL_auxz00_4897 = ((BgL_blocksz00_bglt) BgL_auxz00_4898);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4897))->BgL_z52hashz52) =
					((obj_t) BgL_vz00_210), BUNSPEC);
			}
		}

	}



/* &blockS-%hash-set! */
	obj_t BGl_z62blockSzd2z52hashzd2setz12z22zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3506, obj_t BgL_oz00_3507, obj_t BgL_vz00_3508)
	{
		{	/* SawBbv/bbv-types.sch 205 */
			return
				BGl_blockSzd2z52hashzd2setz12z40zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3507), BgL_vz00_3508);
		}

	}



/* blockS-%mark */
	BGL_EXPORTED_DEF long
		BGl_blockSzd2z52markz80zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_211)
	{
		{	/* SawBbv/bbv-types.sch 206 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4906;

				{
					obj_t BgL_auxz00_4907;

					{	/* SawBbv/bbv-types.sch 206 */
						BgL_objectz00_bglt BgL_tmpz00_4908;

						BgL_tmpz00_4908 = ((BgL_objectz00_bglt) BgL_oz00_211);
						BgL_auxz00_4907 = BGL_OBJECT_WIDENING(BgL_tmpz00_4908);
					}
					BgL_auxz00_4906 = ((BgL_blocksz00_bglt) BgL_auxz00_4907);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4906))->BgL_z52markz52);
			}
		}

	}



/* &blockS-%mark */
	obj_t BGl_z62blockSzd2z52markze2zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3509,
		obj_t BgL_oz00_3510)
	{
		{	/* SawBbv/bbv-types.sch 206 */
			return
				BINT(BGl_blockSzd2z52markz80zzsaw_bbvzd2costzd2(
					((BgL_blockz00_bglt) BgL_oz00_3510)));
		}

	}



/* blockS-%mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2z52markzd2setz12z40zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_212, long BgL_vz00_213)
	{
		{	/* SawBbv/bbv-types.sch 207 */
			{
				BgL_blocksz00_bglt BgL_auxz00_4916;

				{
					obj_t BgL_auxz00_4917;

					{	/* SawBbv/bbv-types.sch 207 */
						BgL_objectz00_bglt BgL_tmpz00_4918;

						BgL_tmpz00_4918 = ((BgL_objectz00_bglt) BgL_oz00_212);
						BgL_auxz00_4917 = BGL_OBJECT_WIDENING(BgL_tmpz00_4918);
					}
					BgL_auxz00_4916 = ((BgL_blocksz00_bglt) BgL_auxz00_4917);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4916))->BgL_z52markz52) =
					((long) BgL_vz00_213), BUNSPEC);
		}}

	}



/* &blockS-%mark-set! */
	obj_t BGl_z62blockSzd2z52markzd2setz12z22zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3511, obj_t BgL_oz00_3512, obj_t BgL_vz00_3513)
	{
		{	/* SawBbv/bbv-types.sch 207 */
			return
				BGl_blockSzd2z52markzd2setz12z40zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3512), (long) CINT(BgL_vz00_3513));
		}

	}



/* blockS-first */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2firstzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_214)
	{
		{	/* SawBbv/bbv-types.sch 208 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_214)))->BgL_firstz00);
		}

	}



/* &blockS-first */
	obj_t BGl_z62blockSzd2firstzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3514,
		obj_t BgL_oz00_3515)
	{
		{	/* SawBbv/bbv-types.sch 208 */
			return
				BGl_blockSzd2firstzd2zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3515));
		}

	}



/* blockS-first-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2firstzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_215, obj_t BgL_vz00_216)
	{
		{	/* SawBbv/bbv-types.sch 209 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_215)))->BgL_firstz00) =
				((obj_t) BgL_vz00_216), BUNSPEC);
		}

	}



/* &blockS-first-set! */
	obj_t BGl_z62blockSzd2firstzd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3516, obj_t BgL_oz00_3517, obj_t BgL_vz00_3518)
	{
		{	/* SawBbv/bbv-types.sch 209 */
			return
				BGl_blockSzd2firstzd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3517), BgL_vz00_3518);
		}

	}



/* blockS-succs */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2succszd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_217)
	{
		{	/* SawBbv/bbv-types.sch 210 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_217)))->BgL_succsz00);
		}

	}



/* &blockS-succs */
	obj_t BGl_z62blockSzd2succszb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3519,
		obj_t BgL_oz00_3520)
	{
		{	/* SawBbv/bbv-types.sch 210 */
			return
				BGl_blockSzd2succszd2zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3520));
		}

	}



/* blockS-succs-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2succszd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_218, obj_t BgL_vz00_219)
	{
		{	/* SawBbv/bbv-types.sch 211 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_218)))->BgL_succsz00) =
				((obj_t) BgL_vz00_219), BUNSPEC);
		}

	}



/* &blockS-succs-set! */
	obj_t BGl_z62blockSzd2succszd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3521, obj_t BgL_oz00_3522, obj_t BgL_vz00_3523)
	{
		{	/* SawBbv/bbv-types.sch 211 */
			return
				BGl_blockSzd2succszd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3522), BgL_vz00_3523);
		}

	}



/* blockS-preds */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2predszd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_220)
	{
		{	/* SawBbv/bbv-types.sch 212 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_220)))->BgL_predsz00);
		}

	}



/* &blockS-preds */
	obj_t BGl_z62blockSzd2predszb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3524,
		obj_t BgL_oz00_3525)
	{
		{	/* SawBbv/bbv-types.sch 212 */
			return
				BGl_blockSzd2predszd2zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3525));
		}

	}



/* blockS-preds-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2predszd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_221, obj_t BgL_vz00_222)
	{
		{	/* SawBbv/bbv-types.sch 213 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_221)))->BgL_predsz00) =
				((obj_t) BgL_vz00_222), BUNSPEC);
		}

	}



/* &blockS-preds-set! */
	obj_t BGl_z62blockSzd2predszd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3526, obj_t BgL_oz00_3527, obj_t BgL_vz00_3528)
	{
		{	/* SawBbv/bbv-types.sch 213 */
			return
				BGl_blockSzd2predszd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3527), BgL_vz00_3528);
		}

	}



/* blockS-label */
	BGL_EXPORTED_DEF int
		BGl_blockSzd2labelzd2zzsaw_bbvzd2costzd2(BgL_blockz00_bglt BgL_oz00_223)
	{
		{	/* SawBbv/bbv-types.sch 214 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_223)))->BgL_labelz00);
		}

	}



/* &blockS-label */
	obj_t BGl_z62blockSzd2labelzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3529,
		obj_t BgL_oz00_3530)
	{
		{	/* SawBbv/bbv-types.sch 214 */
			return
				BINT(BGl_blockSzd2labelzd2zzsaw_bbvzd2costzd2(
					((BgL_blockz00_bglt) BgL_oz00_3530)));
		}

	}



/* blockS-label-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2labelzd2setz12z12zzsaw_bbvzd2costzd2(BgL_blockz00_bglt
		BgL_oz00_224, int BgL_vz00_225)
	{
		{	/* SawBbv/bbv-types.sch 215 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_224)))->BgL_labelz00) =
				((int) BgL_vz00_225), BUNSPEC);
		}

	}



/* &blockS-label-set! */
	obj_t BGl_z62blockSzd2labelzd2setz12z70zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3531, obj_t BgL_oz00_3532, obj_t BgL_vz00_3533)
	{
		{	/* SawBbv/bbv-types.sch 215 */
			return
				BGl_blockSzd2labelzd2setz12z12zzsaw_bbvzd2costzd2(
				((BgL_blockz00_bglt) BgL_oz00_3532), CINT(BgL_vz00_3533));
		}

	}



/* make-bbv-queue */
	BGL_EXPORTED_DEF BgL_bbvzd2queuezd2_bglt
		BGl_makezd2bbvzd2queuez00zzsaw_bbvzd2costzd2(obj_t BgL_blocks1236z00_226,
		obj_t BgL_last1237z00_227)
	{
		{	/* SawBbv/bbv-types.sch 218 */
			{	/* SawBbv/bbv-types.sch 218 */
				BgL_bbvzd2queuezd2_bglt BgL_new1185z00_3716;

				{	/* SawBbv/bbv-types.sch 218 */
					BgL_bbvzd2queuezd2_bglt BgL_new1184z00_3717;

					BgL_new1184z00_3717 =
						((BgL_bbvzd2queuezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2queuezd2_bgl))));
					{	/* SawBbv/bbv-types.sch 218 */
						long BgL_arg1605z00_3718;

						BgL_arg1605z00_3718 =
							BGL_CLASS_NUM(BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1184z00_3717), BgL_arg1605z00_3718);
					}
					BgL_new1185z00_3716 = BgL_new1184z00_3717;
				}
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_new1185z00_3716))->
						BgL_blocksz00) = ((obj_t) BgL_blocks1236z00_226), BUNSPEC);
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_new1185z00_3716))->
						BgL_lastz00) = ((obj_t) BgL_last1237z00_227), BUNSPEC);
				return BgL_new1185z00_3716;
			}
		}

	}



/* &make-bbv-queue */
	BgL_bbvzd2queuezd2_bglt BGl_z62makezd2bbvzd2queuez62zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3534, obj_t BgL_blocks1236z00_3535, obj_t BgL_last1237z00_3536)
	{
		{	/* SawBbv/bbv-types.sch 218 */
			return
				BGl_makezd2bbvzd2queuez00zzsaw_bbvzd2costzd2(BgL_blocks1236z00_3535,
				BgL_last1237z00_3536);
		}

	}



/* bbv-queue? */
	BGL_EXPORTED_DEF bool_t BGl_bbvzd2queuezf3z21zzsaw_bbvzd2costzd2(obj_t
		BgL_objz00_228)
	{
		{	/* SawBbv/bbv-types.sch 219 */
			{	/* SawBbv/bbv-types.sch 219 */
				obj_t BgL_classz00_3719;

				BgL_classz00_3719 = BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_228))
					{	/* SawBbv/bbv-types.sch 219 */
						BgL_objectz00_bglt BgL_arg1807z00_3720;

						BgL_arg1807z00_3720 = (BgL_objectz00_bglt) (BgL_objz00_228);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 219 */
								long BgL_idxz00_3721;

								BgL_idxz00_3721 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3720);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3721 + 1L)) == BgL_classz00_3719);
							}
						else
							{	/* SawBbv/bbv-types.sch 219 */
								bool_t BgL_res1945z00_3724;

								{	/* SawBbv/bbv-types.sch 219 */
									obj_t BgL_oclassz00_3725;

									{	/* SawBbv/bbv-types.sch 219 */
										obj_t BgL_arg1815z00_3726;
										long BgL_arg1816z00_3727;

										BgL_arg1815z00_3726 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 219 */
											long BgL_arg1817z00_3728;

											BgL_arg1817z00_3728 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3720);
											BgL_arg1816z00_3727 = (BgL_arg1817z00_3728 - OBJECT_TYPE);
										}
										BgL_oclassz00_3725 =
											VECTOR_REF(BgL_arg1815z00_3726, BgL_arg1816z00_3727);
									}
									{	/* SawBbv/bbv-types.sch 219 */
										bool_t BgL__ortest_1115z00_3729;

										BgL__ortest_1115z00_3729 =
											(BgL_classz00_3719 == BgL_oclassz00_3725);
										if (BgL__ortest_1115z00_3729)
											{	/* SawBbv/bbv-types.sch 219 */
												BgL_res1945z00_3724 = BgL__ortest_1115z00_3729;
											}
										else
											{	/* SawBbv/bbv-types.sch 219 */
												long BgL_odepthz00_3730;

												{	/* SawBbv/bbv-types.sch 219 */
													obj_t BgL_arg1804z00_3731;

													BgL_arg1804z00_3731 = (BgL_oclassz00_3725);
													BgL_odepthz00_3730 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3731);
												}
												if ((1L < BgL_odepthz00_3730))
													{	/* SawBbv/bbv-types.sch 219 */
														obj_t BgL_arg1802z00_3732;

														{	/* SawBbv/bbv-types.sch 219 */
															obj_t BgL_arg1803z00_3733;

															BgL_arg1803z00_3733 = (BgL_oclassz00_3725);
															BgL_arg1802z00_3732 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3733,
																1L);
														}
														BgL_res1945z00_3724 =
															(BgL_arg1802z00_3732 == BgL_classz00_3719);
													}
												else
													{	/* SawBbv/bbv-types.sch 219 */
														BgL_res1945z00_3724 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1945z00_3724;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 219 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bbv-queue? */
	obj_t BGl_z62bbvzd2queuezf3z43zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3537,
		obj_t BgL_objz00_3538)
	{
		{	/* SawBbv/bbv-types.sch 219 */
			return BBOOL(BGl_bbvzd2queuezf3z21zzsaw_bbvzd2costzd2(BgL_objz00_3538));
		}

	}



/* bbv-queue-nil */
	BGL_EXPORTED_DEF BgL_bbvzd2queuezd2_bglt
		BGl_bbvzd2queuezd2nilz00zzsaw_bbvzd2costzd2(void)
	{
		{	/* SawBbv/bbv-types.sch 220 */
			{	/* SawBbv/bbv-types.sch 220 */
				obj_t BgL_classz00_3071;

				BgL_classz00_3071 = BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 220 */
					obj_t BgL__ortest_1117z00_3072;

					BgL__ortest_1117z00_3072 = BGL_CLASS_NIL(BgL_classz00_3071);
					if (CBOOL(BgL__ortest_1117z00_3072))
						{	/* SawBbv/bbv-types.sch 220 */
							return ((BgL_bbvzd2queuezd2_bglt) BgL__ortest_1117z00_3072);
						}
					else
						{	/* SawBbv/bbv-types.sch 220 */
							return
								((BgL_bbvzd2queuezd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3071));
						}
				}
			}
		}

	}



/* &bbv-queue-nil */
	BgL_bbvzd2queuezd2_bglt BGl_z62bbvzd2queuezd2nilz62zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3539)
	{
		{	/* SawBbv/bbv-types.sch 220 */
			return BGl_bbvzd2queuezd2nilz00zzsaw_bbvzd2costzd2();
		}

	}



/* bbv-queue-last */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2queuezd2lastz00zzsaw_bbvzd2costzd2(BgL_bbvzd2queuezd2_bglt
		BgL_oz00_229)
	{
		{	/* SawBbv/bbv-types.sch 221 */
			return (((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_oz00_229))->BgL_lastz00);
		}

	}



/* &bbv-queue-last */
	obj_t BGl_z62bbvzd2queuezd2lastz62zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3540,
		obj_t BgL_oz00_3541)
	{
		{	/* SawBbv/bbv-types.sch 221 */
			return
				BGl_bbvzd2queuezd2lastz00zzsaw_bbvzd2costzd2(
				((BgL_bbvzd2queuezd2_bglt) BgL_oz00_3541));
		}

	}



/* bbv-queue-last-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2queuezd2lastzd2setz12zc0zzsaw_bbvzd2costzd2
		(BgL_bbvzd2queuezd2_bglt BgL_oz00_230, obj_t BgL_vz00_231)
	{
		{	/* SawBbv/bbv-types.sch 222 */
			return
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_oz00_230))->BgL_lastz00) =
				((obj_t) BgL_vz00_231), BUNSPEC);
		}

	}



/* &bbv-queue-last-set! */
	obj_t BGl_z62bbvzd2queuezd2lastzd2setz12za2zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3542, obj_t BgL_oz00_3543, obj_t BgL_vz00_3544)
	{
		{	/* SawBbv/bbv-types.sch 222 */
			return
				BGl_bbvzd2queuezd2lastzd2setz12zc0zzsaw_bbvzd2costzd2(
				((BgL_bbvzd2queuezd2_bglt) BgL_oz00_3543), BgL_vz00_3544);
		}

	}



/* bbv-queue-blocks */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2queuezd2blocksz00zzsaw_bbvzd2costzd2(BgL_bbvzd2queuezd2_bglt
		BgL_oz00_232)
	{
		{	/* SawBbv/bbv-types.sch 223 */
			return (((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_oz00_232))->BgL_blocksz00);
		}

	}



/* &bbv-queue-blocks */
	obj_t BGl_z62bbvzd2queuezd2blocksz62zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3545,
		obj_t BgL_oz00_3546)
	{
		{	/* SawBbv/bbv-types.sch 223 */
			return
				BGl_bbvzd2queuezd2blocksz00zzsaw_bbvzd2costzd2(
				((BgL_bbvzd2queuezd2_bglt) BgL_oz00_3546));
		}

	}



/* bbv-queue-blocks-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2queuezd2blockszd2setz12zc0zzsaw_bbvzd2costzd2
		(BgL_bbvzd2queuezd2_bglt BgL_oz00_233, obj_t BgL_vz00_234)
	{
		{	/* SawBbv/bbv-types.sch 224 */
			return
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_oz00_233))->BgL_blocksz00) =
				((obj_t) BgL_vz00_234), BUNSPEC);
		}

	}



/* &bbv-queue-blocks-set! */
	obj_t BGl_z62bbvzd2queuezd2blockszd2setz12za2zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3547, obj_t BgL_oz00_3548, obj_t BgL_vz00_3549)
	{
		{	/* SawBbv/bbv-types.sch 224 */
			return
				BGl_bbvzd2queuezd2blockszd2setz12zc0zzsaw_bbvzd2costzd2(
				((BgL_bbvzd2queuezd2_bglt) BgL_oz00_3548), BgL_vz00_3549);
		}

	}



/* make-bbv-ctx */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_makezd2bbvzd2ctxz00zzsaw_bbvzd2costzd2(long BgL_id1233z00_235,
		obj_t BgL_entries1234z00_236)
	{
		{	/* SawBbv/bbv-types.sch 227 */
			{	/* SawBbv/bbv-types.sch 227 */
				BgL_bbvzd2ctxzd2_bglt BgL_new1187z00_3734;

				{	/* SawBbv/bbv-types.sch 227 */
					BgL_bbvzd2ctxzd2_bglt BgL_new1186z00_3735;

					BgL_new1186z00_3735 =
						((BgL_bbvzd2ctxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2ctxzd2_bgl))));
					{	/* SawBbv/bbv-types.sch 227 */
						long BgL_arg1609z00_3736;

						BgL_arg1609z00_3736 =
							BGL_CLASS_NUM(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1186z00_3735), BgL_arg1609z00_3736);
					}
					BgL_new1187z00_3734 = BgL_new1186z00_3735;
				}
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1187z00_3734))->BgL_idz00) =
					((long) BgL_id1233z00_235), BUNSPEC);
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1187z00_3734))->
						BgL_entriesz00) = ((obj_t) BgL_entries1234z00_236), BUNSPEC);
				{	/* SawBbv/bbv-types.sch 227 */
					obj_t BgL_fun1606z00_3737;

					BgL_fun1606z00_3737 =
						BGl_classzd2constructorzd2zz__objectz00
						(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
					BGL_PROCEDURE_CALL1(BgL_fun1606z00_3737,
						((obj_t) BgL_new1187z00_3734));
				}
				return BgL_new1187z00_3734;
			}
		}

	}



/* &make-bbv-ctx */
	BgL_bbvzd2ctxzd2_bglt BGl_z62makezd2bbvzd2ctxz62zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3550, obj_t BgL_id1233z00_3551, obj_t BgL_entries1234z00_3552)
	{
		{	/* SawBbv/bbv-types.sch 227 */
			return
				BGl_makezd2bbvzd2ctxz00zzsaw_bbvzd2costzd2(
				(long) CINT(BgL_id1233z00_3551), BgL_entries1234z00_3552);
		}

	}



/* bbv-ctx? */
	BGL_EXPORTED_DEF bool_t BGl_bbvzd2ctxzf3z21zzsaw_bbvzd2costzd2(obj_t
		BgL_objz00_237)
	{
		{	/* SawBbv/bbv-types.sch 228 */
			{	/* SawBbv/bbv-types.sch 228 */
				obj_t BgL_classz00_3738;

				BgL_classz00_3738 = BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_237))
					{	/* SawBbv/bbv-types.sch 228 */
						BgL_objectz00_bglt BgL_arg1807z00_3739;

						BgL_arg1807z00_3739 = (BgL_objectz00_bglt) (BgL_objz00_237);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 228 */
								long BgL_idxz00_3740;

								BgL_idxz00_3740 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3739);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3740 + 1L)) == BgL_classz00_3738);
							}
						else
							{	/* SawBbv/bbv-types.sch 228 */
								bool_t BgL_res1946z00_3743;

								{	/* SawBbv/bbv-types.sch 228 */
									obj_t BgL_oclassz00_3744;

									{	/* SawBbv/bbv-types.sch 228 */
										obj_t BgL_arg1815z00_3745;
										long BgL_arg1816z00_3746;

										BgL_arg1815z00_3745 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 228 */
											long BgL_arg1817z00_3747;

											BgL_arg1817z00_3747 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3739);
											BgL_arg1816z00_3746 = (BgL_arg1817z00_3747 - OBJECT_TYPE);
										}
										BgL_oclassz00_3744 =
											VECTOR_REF(BgL_arg1815z00_3745, BgL_arg1816z00_3746);
									}
									{	/* SawBbv/bbv-types.sch 228 */
										bool_t BgL__ortest_1115z00_3748;

										BgL__ortest_1115z00_3748 =
											(BgL_classz00_3738 == BgL_oclassz00_3744);
										if (BgL__ortest_1115z00_3748)
											{	/* SawBbv/bbv-types.sch 228 */
												BgL_res1946z00_3743 = BgL__ortest_1115z00_3748;
											}
										else
											{	/* SawBbv/bbv-types.sch 228 */
												long BgL_odepthz00_3749;

												{	/* SawBbv/bbv-types.sch 228 */
													obj_t BgL_arg1804z00_3750;

													BgL_arg1804z00_3750 = (BgL_oclassz00_3744);
													BgL_odepthz00_3749 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3750);
												}
												if ((1L < BgL_odepthz00_3749))
													{	/* SawBbv/bbv-types.sch 228 */
														obj_t BgL_arg1802z00_3751;

														{	/* SawBbv/bbv-types.sch 228 */
															obj_t BgL_arg1803z00_3752;

															BgL_arg1803z00_3752 = (BgL_oclassz00_3744);
															BgL_arg1802z00_3751 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3752,
																1L);
														}
														BgL_res1946z00_3743 =
															(BgL_arg1802z00_3751 == BgL_classz00_3738);
													}
												else
													{	/* SawBbv/bbv-types.sch 228 */
														BgL_res1946z00_3743 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1946z00_3743;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 228 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bbv-ctx? */
	obj_t BGl_z62bbvzd2ctxzf3z43zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3553,
		obj_t BgL_objz00_3554)
	{
		{	/* SawBbv/bbv-types.sch 228 */
			return BBOOL(BGl_bbvzd2ctxzf3z21zzsaw_bbvzd2costzd2(BgL_objz00_3554));
		}

	}



/* bbv-ctx-nil */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_bbvzd2ctxzd2nilz00zzsaw_bbvzd2costzd2(void)
	{
		{	/* SawBbv/bbv-types.sch 229 */
			{	/* SawBbv/bbv-types.sch 229 */
				obj_t BgL_classz00_3114;

				BgL_classz00_3114 = BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 229 */
					obj_t BgL__ortest_1117z00_3115;

					BgL__ortest_1117z00_3115 = BGL_CLASS_NIL(BgL_classz00_3114);
					if (CBOOL(BgL__ortest_1117z00_3115))
						{	/* SawBbv/bbv-types.sch 229 */
							return ((BgL_bbvzd2ctxzd2_bglt) BgL__ortest_1117z00_3115);
						}
					else
						{	/* SawBbv/bbv-types.sch 229 */
							return
								((BgL_bbvzd2ctxzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3114));
						}
				}
			}
		}

	}



/* &bbv-ctx-nil */
	BgL_bbvzd2ctxzd2_bglt BGl_z62bbvzd2ctxzd2nilz62zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3555)
	{
		{	/* SawBbv/bbv-types.sch 229 */
			return BGl_bbvzd2ctxzd2nilz00zzsaw_bbvzd2costzd2();
		}

	}



/* bbv-ctx-entries */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxzd2entriesz00zzsaw_bbvzd2costzd2(BgL_bbvzd2ctxzd2_bglt
		BgL_oz00_238)
	{
		{	/* SawBbv/bbv-types.sch 230 */
			return (((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_oz00_238))->BgL_entriesz00);
		}

	}



/* &bbv-ctx-entries */
	obj_t BGl_z62bbvzd2ctxzd2entriesz62zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3556,
		obj_t BgL_oz00_3557)
	{
		{	/* SawBbv/bbv-types.sch 230 */
			return
				BGl_bbvzd2ctxzd2entriesz00zzsaw_bbvzd2costzd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_3557));
		}

	}



/* bbv-ctx-entries-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxzd2entrieszd2setz12zc0zzsaw_bbvzd2costzd2(BgL_bbvzd2ctxzd2_bglt
		BgL_oz00_239, obj_t BgL_vz00_240)
	{
		{	/* SawBbv/bbv-types.sch 231 */
			return
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_oz00_239))->BgL_entriesz00) =
				((obj_t) BgL_vz00_240), BUNSPEC);
		}

	}



/* &bbv-ctx-entries-set! */
	obj_t BGl_z62bbvzd2ctxzd2entrieszd2setz12za2zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3558, obj_t BgL_oz00_3559, obj_t BgL_vz00_3560)
	{
		{	/* SawBbv/bbv-types.sch 231 */
			return
				BGl_bbvzd2ctxzd2entrieszd2setz12zc0zzsaw_bbvzd2costzd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_3559), BgL_vz00_3560);
		}

	}



/* bbv-ctx-id */
	BGL_EXPORTED_DEF long
		BGl_bbvzd2ctxzd2idz00zzsaw_bbvzd2costzd2(BgL_bbvzd2ctxzd2_bglt BgL_oz00_241)
	{
		{	/* SawBbv/bbv-types.sch 232 */
			return (((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_oz00_241))->BgL_idz00);
		}

	}



/* &bbv-ctx-id */
	obj_t BGl_z62bbvzd2ctxzd2idz62zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3561,
		obj_t BgL_oz00_3562)
	{
		{	/* SawBbv/bbv-types.sch 232 */
			return
				BINT(BGl_bbvzd2ctxzd2idz00zzsaw_bbvzd2costzd2(
					((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_3562)));
		}

	}



/* bbv-ctx-id-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxzd2idzd2setz12zc0zzsaw_bbvzd2costzd2(BgL_bbvzd2ctxzd2_bglt
		BgL_oz00_242, long BgL_vz00_243)
	{
		{	/* SawBbv/bbv-types.sch 233 */
			return
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_oz00_242))->BgL_idz00) =
				((long) BgL_vz00_243), BUNSPEC);
		}

	}



/* &bbv-ctx-id-set! */
	obj_t BGl_z62bbvzd2ctxzd2idzd2setz12za2zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3563, obj_t BgL_oz00_3564, obj_t BgL_vz00_3565)
	{
		{	/* SawBbv/bbv-types.sch 233 */
			return
				BGl_bbvzd2ctxzd2idzd2setz12zc0zzsaw_bbvzd2costzd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_3564), (long) CINT(BgL_vz00_3565));
		}

	}



/* make-bbv-ctxentry */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxentryzd2_bglt
		BGl_makezd2bbvzd2ctxentryz00zzsaw_bbvzd2costzd2(BgL_rtl_regz00_bglt
		BgL_reg1225z00_244, obj_t BgL_types1226z00_245,
		bool_t BgL_polarity1227z00_246, long BgL_count1228z00_247,
		obj_t BgL_value1229z00_248, obj_t BgL_aliases1230z00_249,
		obj_t BgL_initval1231z00_250)
	{
		{	/* SawBbv/bbv-types.sch 236 */
			{	/* SawBbv/bbv-types.sch 236 */
				BgL_bbvzd2ctxentryzd2_bglt BgL_new1190z00_3753;

				{	/* SawBbv/bbv-types.sch 236 */
					BgL_bbvzd2ctxentryzd2_bglt BgL_new1189z00_3754;

					BgL_new1189z00_3754 =
						((BgL_bbvzd2ctxentryzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2ctxentryzd2_bgl))));
					{	/* SawBbv/bbv-types.sch 236 */
						long BgL_arg1611z00_3755;

						BgL_arg1611z00_3755 =
							BGL_CLASS_NUM(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1189z00_3754), BgL_arg1611z00_3755);
					}
					BgL_new1190z00_3753 = BgL_new1189z00_3754;
				}
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1190z00_3753))->
						BgL_regz00) = ((BgL_rtl_regz00_bglt) BgL_reg1225z00_244), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1190z00_3753))->
						BgL_typesz00) = ((obj_t) BgL_types1226z00_245), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1190z00_3753))->
						BgL_polarityz00) = ((bool_t) BgL_polarity1227z00_246), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1190z00_3753))->
						BgL_countz00) = ((long) BgL_count1228z00_247), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1190z00_3753))->
						BgL_valuez00) = ((obj_t) BgL_value1229z00_248), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1190z00_3753))->
						BgL_aliasesz00) = ((obj_t) BgL_aliases1230z00_249), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1190z00_3753))->
						BgL_initvalz00) = ((obj_t) BgL_initval1231z00_250), BUNSPEC);
				return BgL_new1190z00_3753;
			}
		}

	}



/* &make-bbv-ctxentry */
	BgL_bbvzd2ctxentryzd2_bglt
		BGl_z62makezd2bbvzd2ctxentryz62zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3566,
		obj_t BgL_reg1225z00_3567, obj_t BgL_types1226z00_3568,
		obj_t BgL_polarity1227z00_3569, obj_t BgL_count1228z00_3570,
		obj_t BgL_value1229z00_3571, obj_t BgL_aliases1230z00_3572,
		obj_t BgL_initval1231z00_3573)
	{
		{	/* SawBbv/bbv-types.sch 236 */
			return
				BGl_makezd2bbvzd2ctxentryz00zzsaw_bbvzd2costzd2(
				((BgL_rtl_regz00_bglt) BgL_reg1225z00_3567), BgL_types1226z00_3568,
				CBOOL(BgL_polarity1227z00_3569),
				(long) CINT(BgL_count1228z00_3570), BgL_value1229z00_3571,
				BgL_aliases1230z00_3572, BgL_initval1231z00_3573);
		}

	}



/* bbv-ctxentry? */
	BGL_EXPORTED_DEF bool_t BGl_bbvzd2ctxentryzf3z21zzsaw_bbvzd2costzd2(obj_t
		BgL_objz00_251)
	{
		{	/* SawBbv/bbv-types.sch 237 */
			{	/* SawBbv/bbv-types.sch 237 */
				obj_t BgL_classz00_3756;

				BgL_classz00_3756 = BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_251))
					{	/* SawBbv/bbv-types.sch 237 */
						BgL_objectz00_bglt BgL_arg1807z00_3757;

						BgL_arg1807z00_3757 = (BgL_objectz00_bglt) (BgL_objz00_251);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 237 */
								long BgL_idxz00_3758;

								BgL_idxz00_3758 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3757);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3758 + 1L)) == BgL_classz00_3756);
							}
						else
							{	/* SawBbv/bbv-types.sch 237 */
								bool_t BgL_res1947z00_3761;

								{	/* SawBbv/bbv-types.sch 237 */
									obj_t BgL_oclassz00_3762;

									{	/* SawBbv/bbv-types.sch 237 */
										obj_t BgL_arg1815z00_3763;
										long BgL_arg1816z00_3764;

										BgL_arg1815z00_3763 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 237 */
											long BgL_arg1817z00_3765;

											BgL_arg1817z00_3765 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3757);
											BgL_arg1816z00_3764 = (BgL_arg1817z00_3765 - OBJECT_TYPE);
										}
										BgL_oclassz00_3762 =
											VECTOR_REF(BgL_arg1815z00_3763, BgL_arg1816z00_3764);
									}
									{	/* SawBbv/bbv-types.sch 237 */
										bool_t BgL__ortest_1115z00_3766;

										BgL__ortest_1115z00_3766 =
											(BgL_classz00_3756 == BgL_oclassz00_3762);
										if (BgL__ortest_1115z00_3766)
											{	/* SawBbv/bbv-types.sch 237 */
												BgL_res1947z00_3761 = BgL__ortest_1115z00_3766;
											}
										else
											{	/* SawBbv/bbv-types.sch 237 */
												long BgL_odepthz00_3767;

												{	/* SawBbv/bbv-types.sch 237 */
													obj_t BgL_arg1804z00_3768;

													BgL_arg1804z00_3768 = (BgL_oclassz00_3762);
													BgL_odepthz00_3767 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3768);
												}
												if ((1L < BgL_odepthz00_3767))
													{	/* SawBbv/bbv-types.sch 237 */
														obj_t BgL_arg1802z00_3769;

														{	/* SawBbv/bbv-types.sch 237 */
															obj_t BgL_arg1803z00_3770;

															BgL_arg1803z00_3770 = (BgL_oclassz00_3762);
															BgL_arg1802z00_3769 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3770,
																1L);
														}
														BgL_res1947z00_3761 =
															(BgL_arg1802z00_3769 == BgL_classz00_3756);
													}
												else
													{	/* SawBbv/bbv-types.sch 237 */
														BgL_res1947z00_3761 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1947z00_3761;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 237 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bbv-ctxentry? */
	obj_t BGl_z62bbvzd2ctxentryzf3z43zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3574,
		obj_t BgL_objz00_3575)
	{
		{	/* SawBbv/bbv-types.sch 237 */
			return
				BBOOL(BGl_bbvzd2ctxentryzf3z21zzsaw_bbvzd2costzd2(BgL_objz00_3575));
		}

	}



/* bbv-ctxentry-nil */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxentryzd2_bglt
		BGl_bbvzd2ctxentryzd2nilz00zzsaw_bbvzd2costzd2(void)
	{
		{	/* SawBbv/bbv-types.sch 238 */
			{	/* SawBbv/bbv-types.sch 238 */
				obj_t BgL_classz00_3156;

				BgL_classz00_3156 = BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 238 */
					obj_t BgL__ortest_1117z00_3157;

					BgL__ortest_1117z00_3157 = BGL_CLASS_NIL(BgL_classz00_3156);
					if (CBOOL(BgL__ortest_1117z00_3157))
						{	/* SawBbv/bbv-types.sch 238 */
							return ((BgL_bbvzd2ctxentryzd2_bglt) BgL__ortest_1117z00_3157);
						}
					else
						{	/* SawBbv/bbv-types.sch 238 */
							return
								((BgL_bbvzd2ctxentryzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3156));
						}
				}
			}
		}

	}



/* &bbv-ctxentry-nil */
	BgL_bbvzd2ctxentryzd2_bglt
		BGl_z62bbvzd2ctxentryzd2nilz62zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3576)
	{
		{	/* SawBbv/bbv-types.sch 238 */
			return BGl_bbvzd2ctxentryzd2nilz00zzsaw_bbvzd2costzd2();
		}

	}



/* bbv-ctxentry-initval */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2initvalz00zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_252)
	{
		{	/* SawBbv/bbv-types.sch 239 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_252))->BgL_initvalz00);
		}

	}



/* &bbv-ctxentry-initval */
	obj_t BGl_z62bbvzd2ctxentryzd2initvalz62zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3577, obj_t BgL_oz00_3578)
	{
		{	/* SawBbv/bbv-types.sch 239 */
			return
				BGl_bbvzd2ctxentryzd2initvalz00zzsaw_bbvzd2costzd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3578));
		}

	}



/* bbv-ctxentry-initval-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2initvalzd2setz12zc0zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_253, obj_t BgL_vz00_254)
	{
		{	/* SawBbv/bbv-types.sch 240 */
			return
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_253))->
					BgL_initvalz00) = ((obj_t) BgL_vz00_254), BUNSPEC);
		}

	}



/* &bbv-ctxentry-initval-set! */
	obj_t BGl_z62bbvzd2ctxentryzd2initvalzd2setz12za2zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3579, obj_t BgL_oz00_3580, obj_t BgL_vz00_3581)
	{
		{	/* SawBbv/bbv-types.sch 240 */
			return
				BGl_bbvzd2ctxentryzd2initvalzd2setz12zc0zzsaw_bbvzd2costzd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3580), BgL_vz00_3581);
		}

	}



/* bbv-ctxentry-aliases */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2aliasesz00zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_255)
	{
		{	/* SawBbv/bbv-types.sch 241 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_255))->BgL_aliasesz00);
		}

	}



/* &bbv-ctxentry-aliases */
	obj_t BGl_z62bbvzd2ctxentryzd2aliasesz62zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3582, obj_t BgL_oz00_3583)
	{
		{	/* SawBbv/bbv-types.sch 241 */
			return
				BGl_bbvzd2ctxentryzd2aliasesz00zzsaw_bbvzd2costzd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3583));
		}

	}



/* bbv-ctxentry-aliases-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2aliaseszd2setz12zc0zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_256, obj_t BgL_vz00_257)
	{
		{	/* SawBbv/bbv-types.sch 242 */
			return
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_256))->
					BgL_aliasesz00) = ((obj_t) BgL_vz00_257), BUNSPEC);
		}

	}



/* &bbv-ctxentry-aliases-set! */
	obj_t BGl_z62bbvzd2ctxentryzd2aliaseszd2setz12za2zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3584, obj_t BgL_oz00_3585, obj_t BgL_vz00_3586)
	{
		{	/* SawBbv/bbv-types.sch 242 */
			return
				BGl_bbvzd2ctxentryzd2aliaseszd2setz12zc0zzsaw_bbvzd2costzd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3585), BgL_vz00_3586);
		}

	}



/* bbv-ctxentry-value */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2valuez00zzsaw_bbvzd2costzd2(BgL_bbvzd2ctxentryzd2_bglt
		BgL_oz00_258)
	{
		{	/* SawBbv/bbv-types.sch 243 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_258))->BgL_valuez00);
		}

	}



/* &bbv-ctxentry-value */
	obj_t BGl_z62bbvzd2ctxentryzd2valuez62zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3587, obj_t BgL_oz00_3588)
	{
		{	/* SawBbv/bbv-types.sch 243 */
			return
				BGl_bbvzd2ctxentryzd2valuez00zzsaw_bbvzd2costzd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3588));
		}

	}



/* bbv-ctxentry-count */
	BGL_EXPORTED_DEF long
		BGl_bbvzd2ctxentryzd2countz00zzsaw_bbvzd2costzd2(BgL_bbvzd2ctxentryzd2_bglt
		BgL_oz00_261)
	{
		{	/* SawBbv/bbv-types.sch 245 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_261))->BgL_countz00);
		}

	}



/* &bbv-ctxentry-count */
	obj_t BGl_z62bbvzd2ctxentryzd2countz62zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3589, obj_t BgL_oz00_3590)
	{
		{	/* SawBbv/bbv-types.sch 245 */
			return
				BINT(BGl_bbvzd2ctxentryzd2countz00zzsaw_bbvzd2costzd2(
					((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3590)));
		}

	}



/* bbv-ctxentry-count-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2countzd2setz12zc0zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_262, long BgL_vz00_263)
	{
		{	/* SawBbv/bbv-types.sch 246 */
			return
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_262))->BgL_countz00) =
				((long) BgL_vz00_263), BUNSPEC);
		}

	}



/* &bbv-ctxentry-count-set! */
	obj_t BGl_z62bbvzd2ctxentryzd2countzd2setz12za2zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3591, obj_t BgL_oz00_3592, obj_t BgL_vz00_3593)
	{
		{	/* SawBbv/bbv-types.sch 246 */
			return
				BGl_bbvzd2ctxentryzd2countzd2setz12zc0zzsaw_bbvzd2costzd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3592),
				(long) CINT(BgL_vz00_3593));
		}

	}



/* bbv-ctxentry-polarity */
	BGL_EXPORTED_DEF bool_t
		BGl_bbvzd2ctxentryzd2polarityz00zzsaw_bbvzd2costzd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_264)
	{
		{	/* SawBbv/bbv-types.sch 247 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_264))->BgL_polarityz00);
		}

	}



/* &bbv-ctxentry-polarity */
	obj_t BGl_z62bbvzd2ctxentryzd2polarityz62zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3594, obj_t BgL_oz00_3595)
	{
		{	/* SawBbv/bbv-types.sch 247 */
			return
				BBOOL(BGl_bbvzd2ctxentryzd2polarityz00zzsaw_bbvzd2costzd2(
					((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3595)));
		}

	}



/* bbv-ctxentry-types */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2typesz00zzsaw_bbvzd2costzd2(BgL_bbvzd2ctxentryzd2_bglt
		BgL_oz00_267)
	{
		{	/* SawBbv/bbv-types.sch 249 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_267))->BgL_typesz00);
		}

	}



/* &bbv-ctxentry-types */
	obj_t BGl_z62bbvzd2ctxentryzd2typesz62zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3596, obj_t BgL_oz00_3597)
	{
		{	/* SawBbv/bbv-types.sch 249 */
			return
				BGl_bbvzd2ctxentryzd2typesz00zzsaw_bbvzd2costzd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3597));
		}

	}



/* bbv-ctxentry-reg */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_bbvzd2ctxentryzd2regz00zzsaw_bbvzd2costzd2(BgL_bbvzd2ctxentryzd2_bglt
		BgL_oz00_270)
	{
		{	/* SawBbv/bbv-types.sch 251 */
			return (((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_270))->BgL_regz00);
		}

	}



/* &bbv-ctxentry-reg */
	BgL_rtl_regz00_bglt BGl_z62bbvzd2ctxentryzd2regz62zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3598, obj_t BgL_oz00_3599)
	{
		{	/* SawBbv/bbv-types.sch 251 */
			return
				BGl_bbvzd2ctxentryzd2regz00zzsaw_bbvzd2costzd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3599));
		}

	}



/* rtl_ins-cost */
	BGL_EXPORTED_DEF long
		BGl_rtl_inszd2costzd2zzsaw_bbvzd2costzd2(BgL_rtl_insz00_bglt
		BgL_thisz00_277)
	{
		{	/* SawBbv/bbv-cost.scm 75 */
			{	/* SawBbv/bbv-cost.scm 77 */
				BgL_rtl_funz00_bglt BgL_arg1613z00_2251;
				obj_t BgL_arg1615z00_2252;

				BgL_arg1613z00_2251 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_thisz00_277))->BgL_funz00);
				BgL_arg1615z00_2252 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_thisz00_277))->BgL_argsz00);
				return
					(long) CINT(BGl_rtl_funzd2costzd2zzsaw_bbvzd2costzd2(
						((obj_t) BgL_arg1613z00_2251), BgL_arg1615z00_2252));
		}}

	}



/* &rtl_ins-cost */
	obj_t BGl_z62rtl_inszd2costzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3600,
		obj_t BgL_thisz00_3601)
	{
		{	/* SawBbv/bbv-cost.scm 75 */
			return
				BINT(BGl_rtl_inszd2costzd2zzsaw_bbvzd2costzd2(
					((BgL_rtl_insz00_bglt) BgL_thisz00_3601)));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_bbvzd2costzd2(void)
	{
		{	/* SawBbv/bbv-cost.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_bbvzd2costzd2(void)
	{
		{	/* SawBbv/bbv-cost.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_rtl_funzd2costzd2envz00zzsaw_bbvzd2costzd2,
				BGl_proc1955z00zzsaw_bbvzd2costzd2, BFALSE,
				BGl_string1956z00zzsaw_bbvzd2costzd2);
		}

	}



/* &rtl_fun-cost1444 */
	obj_t BGl_z62rtl_funzd2cost1444zb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3603,
		obj_t BgL_thisz00_3604, obj_t BgL_argsz00_3605)
	{
		{	/* SawBbv/bbv-cost.scm 43 */
			return BINT(0L);
		}

	}



/* rtl_fun-cost */
	obj_t BGl_rtl_funzd2costzd2zzsaw_bbvzd2costzd2(obj_t BgL_thisz00_273,
		obj_t BgL_argsz00_274)
	{
		{	/* SawBbv/bbv-cost.scm 43 */
			if (BGL_OBJECTP(BgL_thisz00_273))
				{	/* SawBbv/bbv-cost.scm 43 */
					obj_t BgL_method1446z00_2301;

					{	/* SawBbv/bbv-cost.scm 43 */
						obj_t BgL_res1952z00_3188;

						{	/* SawBbv/bbv-cost.scm 43 */
							long BgL_objzd2classzd2numz00_3159;

							BgL_objzd2classzd2numz00_3159 =
								BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_thisz00_273));
							{	/* SawBbv/bbv-cost.scm 43 */
								obj_t BgL_arg1811z00_3160;

								BgL_arg1811z00_3160 =
									PROCEDURE_REF(BGl_rtl_funzd2costzd2envz00zzsaw_bbvzd2costzd2,
									(int) (1L));
								{	/* SawBbv/bbv-cost.scm 43 */
									int BgL_offsetz00_3163;

									BgL_offsetz00_3163 = (int) (BgL_objzd2classzd2numz00_3159);
									{	/* SawBbv/bbv-cost.scm 43 */
										long BgL_offsetz00_3164;

										BgL_offsetz00_3164 =
											((long) (BgL_offsetz00_3163) - OBJECT_TYPE);
										{	/* SawBbv/bbv-cost.scm 43 */
											long BgL_modz00_3165;

											BgL_modz00_3165 =
												(BgL_offsetz00_3164 >> (int) ((long) ((int) (4L))));
											{	/* SawBbv/bbv-cost.scm 43 */
												long BgL_restz00_3167;

												BgL_restz00_3167 =
													(BgL_offsetz00_3164 &
													(long) (
														(int) (
															((long) (
																	(int) (
																		(1L <<
																			(int) ((long) ((int) (4L)))))) - 1L))));
												{	/* SawBbv/bbv-cost.scm 43 */

													{	/* SawBbv/bbv-cost.scm 43 */
														obj_t BgL_bucketz00_3169;

														BgL_bucketz00_3169 =
															VECTOR_REF(
															((obj_t) BgL_arg1811z00_3160), BgL_modz00_3165);
														BgL_res1952z00_3188 =
															VECTOR_REF(
															((obj_t) BgL_bucketz00_3169), BgL_restz00_3167);
						}}}}}}}}
						BgL_method1446z00_2301 = BgL_res1952z00_3188;
					}
					return
						BGL_PROCEDURE_CALL2(BgL_method1446z00_2301, BgL_thisz00_273,
						BgL_argsz00_274);
				}
			else
				{	/* SawBbv/bbv-cost.scm 43 */
					obj_t BgL_fun1690z00_2302;

					BgL_fun1690z00_2302 =
						PROCEDURE_REF(BGl_rtl_funzd2costzd2envz00zzsaw_bbvzd2costzd2,
						(int) (0L));
					return
						BGL_PROCEDURE_CALL2(BgL_fun1690z00_2302, BgL_thisz00_273,
						BgL_argsz00_274);
				}
		}

	}



/* &rtl_fun-cost */
	obj_t BGl_z62rtl_funzd2costzb0zzsaw_bbvzd2costzd2(obj_t BgL_envz00_3606,
		obj_t BgL_thisz00_3607, obj_t BgL_argsz00_3608)
	{
		{	/* SawBbv/bbv-cost.scm 43 */
			return
				BGl_rtl_funzd2costzd2zzsaw_bbvzd2costzd2(BgL_thisz00_3607,
				BgL_argsz00_3608);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_bbvzd2costzd2(void)
	{
		{	/* SawBbv/bbv-cost.scm 15 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_rtl_funzd2costzd2envz00zzsaw_bbvzd2costzd2,
				BGl_rtl_ifnez00zzsaw_defsz00, BGl_proc1957z00zzsaw_bbvzd2costzd2,
				BGl_string1958z00zzsaw_bbvzd2costzd2);
		}

	}



/* &rtl_fun-cost-rtl_ifn1448 */
	obj_t BGl_z62rtl_funzd2costzd2rtl_ifn1448z62zzsaw_bbvzd2costzd2(obj_t
		BgL_envz00_3610, obj_t BgL_thisz00_3611, obj_t BgL_argsz00_3612)
	{
		{	/* SawBbv/bbv-cost.scm 48 */
			{	/* SawBbv/bbv-cost.scm 50 */
				long BgL_tmpz00_5199;

				{	/* SawBbv/bbv-cost.scm 50 */
					bool_t BgL_test2141z00_5200;

					{	/* SawBbv/bbv-cost.scm 50 */
						bool_t BgL_test2142z00_5201;

						{	/* SawBbv/bbv-cost.scm 50 */
							obj_t BgL_arg1753z00_3772;

							BgL_arg1753z00_3772 = CAR(((obj_t) BgL_argsz00_3612));
							{	/* SawBbv/bbv-cost.scm 50 */
								obj_t BgL_classz00_3773;

								BgL_classz00_3773 = BGl_rtl_insz00zzsaw_defsz00;
								if (BGL_OBJECTP(BgL_arg1753z00_3772))
									{	/* SawBbv/bbv-cost.scm 50 */
										BgL_objectz00_bglt BgL_arg1807z00_3774;

										BgL_arg1807z00_3774 =
											(BgL_objectz00_bglt) (BgL_arg1753z00_3772);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawBbv/bbv-cost.scm 50 */
												long BgL_idxz00_3775;

												BgL_idxz00_3775 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3774);
												BgL_test2142z00_5201 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3775 + 1L)) == BgL_classz00_3773);
											}
										else
											{	/* SawBbv/bbv-cost.scm 50 */
												bool_t BgL_res1954z00_3778;

												{	/* SawBbv/bbv-cost.scm 50 */
													obj_t BgL_oclassz00_3779;

													{	/* SawBbv/bbv-cost.scm 50 */
														obj_t BgL_arg1815z00_3780;
														long BgL_arg1816z00_3781;

														BgL_arg1815z00_3780 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawBbv/bbv-cost.scm 50 */
															long BgL_arg1817z00_3782;

															BgL_arg1817z00_3782 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3774);
															BgL_arg1816z00_3781 =
																(BgL_arg1817z00_3782 - OBJECT_TYPE);
														}
														BgL_oclassz00_3779 =
															VECTOR_REF(BgL_arg1815z00_3780,
															BgL_arg1816z00_3781);
													}
													{	/* SawBbv/bbv-cost.scm 50 */
														bool_t BgL__ortest_1115z00_3783;

														BgL__ortest_1115z00_3783 =
															(BgL_classz00_3773 == BgL_oclassz00_3779);
														if (BgL__ortest_1115z00_3783)
															{	/* SawBbv/bbv-cost.scm 50 */
																BgL_res1954z00_3778 = BgL__ortest_1115z00_3783;
															}
														else
															{	/* SawBbv/bbv-cost.scm 50 */
																long BgL_odepthz00_3784;

																{	/* SawBbv/bbv-cost.scm 50 */
																	obj_t BgL_arg1804z00_3785;

																	BgL_arg1804z00_3785 = (BgL_oclassz00_3779);
																	BgL_odepthz00_3784 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3785);
																}
																if ((1L < BgL_odepthz00_3784))
																	{	/* SawBbv/bbv-cost.scm 50 */
																		obj_t BgL_arg1802z00_3786;

																		{	/* SawBbv/bbv-cost.scm 50 */
																			obj_t BgL_arg1803z00_3787;

																			BgL_arg1803z00_3787 =
																				(BgL_oclassz00_3779);
																			BgL_arg1802z00_3786 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3787, 1L);
																		}
																		BgL_res1954z00_3778 =
																			(BgL_arg1802z00_3786 ==
																			BgL_classz00_3773);
																	}
																else
																	{	/* SawBbv/bbv-cost.scm 50 */
																		BgL_res1954z00_3778 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2142z00_5201 = BgL_res1954z00_3778;
											}
									}
								else
									{	/* SawBbv/bbv-cost.scm 50 */
										BgL_test2142z00_5201 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test2142z00_5201)
							{	/* SawBbv/bbv-cost.scm 51 */
								bool_t BgL_test2147z00_5226;

								{	/* SawBbv/bbv-cost.scm 51 */
									obj_t BgL_arg1752z00_3788;

									BgL_arg1752z00_3788 = CAR(((obj_t) BgL_argsz00_3612));
									BgL_test2147z00_5226 =
										BGl_rtl_inszd2callzf3z21zzsaw_bbvzd2typeszd2(
										((BgL_rtl_insz00_bglt) BgL_arg1752z00_3788));
								}
								if (BgL_test2147z00_5226)
									{	/* SawBbv/bbv-cost.scm 52 */
										obj_t BgL_arg1751z00_3789;

										BgL_arg1751z00_3789 = CAR(((obj_t) BgL_argsz00_3612));
										BgL_test2141z00_5200 =
											CBOOL(BGl_rtl_callzd2predicatezd2zzsaw_bbvzd2typeszd2(
												((BgL_rtl_insz00_bglt) BgL_arg1751z00_3789)));
									}
								else
									{	/* SawBbv/bbv-cost.scm 51 */
										BgL_test2141z00_5200 = ((bool_t) 0);
									}
							}
						else
							{	/* SawBbv/bbv-cost.scm 50 */
								BgL_test2141z00_5200 = ((bool_t) 0);
							}
					}
					if (BgL_test2141z00_5200)
						{	/* SawBbv/bbv-cost.scm 54 */
							obj_t BgL_arg1705z00_3790;

							{	/* SawBbv/bbv-cost.scm 54 */
								obj_t BgL_runner1718z00_3791;

								if (NULLP(BgL_argsz00_3612))
									{	/* SawBbv/bbv-cost.scm 54 */
										BgL_runner1718z00_3791 = BNIL;
									}
								else
									{	/* SawBbv/bbv-cost.scm 54 */
										obj_t BgL_head1424z00_3792;

										BgL_head1424z00_3792 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1422z00_3794;
											obj_t BgL_tail1425z00_3795;

											BgL_l1422z00_3794 = BgL_argsz00_3612;
											BgL_tail1425z00_3795 = BgL_head1424z00_3792;
										BgL_zc3z04anonymousza31707ze3z87_3793:
											if (NULLP(BgL_l1422z00_3794))
												{	/* SawBbv/bbv-cost.scm 54 */
													BgL_runner1718z00_3791 = CDR(BgL_head1424z00_3792);
												}
											else
												{	/* SawBbv/bbv-cost.scm 54 */
													obj_t BgL_newtail1426z00_3796;

													{	/* SawBbv/bbv-cost.scm 54 */
														obj_t BgL_arg1717z00_3797;

														{	/* SawBbv/bbv-cost.scm 54 */
															obj_t BgL_az00_3798;

															BgL_az00_3798 = CAR(((obj_t) BgL_l1422z00_3794));
															BgL_arg1717z00_3797 =
																BGl_rtl_funzd2costzd2zzsaw_bbvzd2costzd2
																(BgL_az00_3798, BNIL);
														}
														BgL_newtail1426z00_3796 =
															MAKE_YOUNG_PAIR(BgL_arg1717z00_3797, BNIL);
													}
													SET_CDR(BgL_tail1425z00_3795,
														BgL_newtail1426z00_3796);
													{	/* SawBbv/bbv-cost.scm 54 */
														obj_t BgL_arg1714z00_3799;

														BgL_arg1714z00_3799 =
															CDR(((obj_t) BgL_l1422z00_3794));
														{
															obj_t BgL_tail1425z00_5250;
															obj_t BgL_l1422z00_5249;

															BgL_l1422z00_5249 = BgL_arg1714z00_3799;
															BgL_tail1425z00_5250 = BgL_newtail1426z00_3796;
															BgL_tail1425z00_3795 = BgL_tail1425z00_5250;
															BgL_l1422z00_3794 = BgL_l1422z00_5249;
															goto BgL_zc3z04anonymousza31707ze3z87_3793;
														}
													}
												}
										}
									}
								BgL_arg1705z00_3790 =
									BGl_zb2zb2zz__r4_numbers_6_5z00(BgL_runner1718z00_3791);
							}
							BgL_tmpz00_5199 = (1L + (long) CINT(BgL_arg1705z00_3790));
						}
					else
						{	/* SawBbv/bbv-cost.scm 55 */
							bool_t BgL_test2150z00_5254;

							if ((bgl_list_length(BgL_argsz00_3612) == 2L))
								{	/* SawBbv/bbv-cost.scm 56 */
									bool_t BgL__ortest_1191z00_3800;

									BgL__ortest_1191z00_3800 =
										(BGl_varz00zzast_nodez00 ==
										BGl_za2zc3fxza2zc3zzsaw_bbvzd2cachezd2);
									if (BgL__ortest_1191z00_3800)
										{	/* SawBbv/bbv-cost.scm 56 */
											BgL_test2150z00_5254 = BgL__ortest_1191z00_3800;
										}
									else
										{	/* SawBbv/bbv-cost.scm 56 */
											bool_t BgL__ortest_1192z00_3801;

											BgL__ortest_1192z00_3801 =
												(BGl_varz00zzast_nodez00 ==
												BGl_za2zc3zd3fxza2z10zzsaw_bbvzd2cachezd2);
											if (BgL__ortest_1192z00_3801)
												{	/* SawBbv/bbv-cost.scm 56 */
													BgL_test2150z00_5254 = BgL__ortest_1192z00_3801;
												}
											else
												{	/* SawBbv/bbv-cost.scm 57 */
													bool_t BgL__ortest_1193z00_3802;

													BgL__ortest_1193z00_3802 =
														(BGl_varz00zzast_nodez00 ==
														BGl_za2ze3fxza2ze3zzsaw_bbvzd2cachezd2);
													if (BgL__ortest_1193z00_3802)
														{	/* SawBbv/bbv-cost.scm 57 */
															BgL_test2150z00_5254 = BgL__ortest_1193z00_3802;
														}
													else
														{	/* SawBbv/bbv-cost.scm 57 */
															bool_t BgL__ortest_1194z00_3803;

															BgL__ortest_1194z00_3803 =
																(BGl_varz00zzast_nodez00 ==
																BGl_za2ze3zd3fxza2z30zzsaw_bbvzd2cachezd2);
															if (BgL__ortest_1194z00_3803)
																{	/* SawBbv/bbv-cost.scm 57 */
																	BgL_test2150z00_5254 =
																		BgL__ortest_1194z00_3803;
																}
															else
																{	/* SawBbv/bbv-cost.scm 58 */
																	bool_t BgL__ortest_1195z00_3804;

																	BgL__ortest_1195z00_3804 =
																		(BGl_varz00zzast_nodez00 ==
																		BGl_za2zd3fxza2zd3zzsaw_bbvzd2cachezd2);
																	if (BgL__ortest_1195z00_3804)
																		{	/* SawBbv/bbv-cost.scm 58 */
																			BgL_test2150z00_5254 =
																				BgL__ortest_1195z00_3804;
																		}
																	else
																		{	/* SawBbv/bbv-cost.scm 59 */
																			bool_t BgL__ortest_1196z00_3805;

																			BgL__ortest_1196z00_3805 =
																				(BGl_varz00zzast_nodez00 ==
																				BGl_za2zb2fxza2zb2zzsaw_bbvzd2cachezd2);
																			if (BgL__ortest_1196z00_3805)
																				{	/* SawBbv/bbv-cost.scm 59 */
																					BgL_test2150z00_5254 =
																						BgL__ortest_1196z00_3805;
																				}
																			else
																				{	/* SawBbv/bbv-cost.scm 59 */
																					BgL_test2150z00_5254 =
																						(BGl_varz00zzast_nodez00 ==
																						BGl_za2zd2fxza2zd2zzsaw_bbvzd2cachezd2);
																				}
																		}
																}
														}
												}
										}
								}
							else
								{	/* SawBbv/bbv-cost.scm 55 */
									BgL_test2150z00_5254 = ((bool_t) 0);
								}
							if (BgL_test2150z00_5254)
								{	/* SawBbv/bbv-cost.scm 61 */
									obj_t BgL_arg1724z00_3806;

									{	/* SawBbv/bbv-cost.scm 61 */
										obj_t BgL_runner1735z00_3807;

										if (NULLP(BgL_argsz00_3612))
											{	/* SawBbv/bbv-cost.scm 61 */
												BgL_runner1735z00_3807 = BNIL;
											}
										else
											{	/* SawBbv/bbv-cost.scm 61 */
												obj_t BgL_head1429z00_3808;

												BgL_head1429z00_3808 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												{
													obj_t BgL_l1427z00_3810;
													obj_t BgL_tail1430z00_3811;

													BgL_l1427z00_3810 = BgL_argsz00_3612;
													BgL_tail1430z00_3811 = BgL_head1429z00_3808;
												BgL_zc3z04anonymousza31726ze3z87_3809:
													if (NULLP(BgL_l1427z00_3810))
														{	/* SawBbv/bbv-cost.scm 61 */
															BgL_runner1735z00_3807 =
																CDR(BgL_head1429z00_3808);
														}
													else
														{	/* SawBbv/bbv-cost.scm 61 */
															obj_t BgL_newtail1431z00_3812;

															{	/* SawBbv/bbv-cost.scm 61 */
																obj_t BgL_arg1734z00_3813;

																{	/* SawBbv/bbv-cost.scm 61 */
																	obj_t BgL_az00_3814;

																	BgL_az00_3814 =
																		CAR(((obj_t) BgL_l1427z00_3810));
																	BgL_arg1734z00_3813 =
																		BGl_rtl_funzd2costzd2zzsaw_bbvzd2costzd2
																		(BgL_az00_3814, BNIL);
																}
																BgL_newtail1431z00_3812 =
																	MAKE_YOUNG_PAIR(BgL_arg1734z00_3813, BNIL);
															}
															SET_CDR(BgL_tail1430z00_3811,
																BgL_newtail1431z00_3812);
															{	/* SawBbv/bbv-cost.scm 61 */
																obj_t BgL_arg1733z00_3815;

																BgL_arg1733z00_3815 =
																	CDR(((obj_t) BgL_l1427z00_3810));
																{
																	obj_t BgL_tail1430z00_5285;
																	obj_t BgL_l1427z00_5284;

																	BgL_l1427z00_5284 = BgL_arg1733z00_3815;
																	BgL_tail1430z00_5285 =
																		BgL_newtail1431z00_3812;
																	BgL_tail1430z00_3811 = BgL_tail1430z00_5285;
																	BgL_l1427z00_3810 = BgL_l1427z00_5284;
																	goto BgL_zc3z04anonymousza31726ze3z87_3809;
																}
															}
														}
												}
											}
										BgL_arg1724z00_3806 =
											BGl_zb2zb2zz__r4_numbers_6_5z00(BgL_runner1735z00_3807);
									}
									BgL_tmpz00_5199 = (1L + (long) CINT(BgL_arg1724z00_3806));
								}
							else
								{	/* SawBbv/bbv-cost.scm 62 */
									bool_t BgL_test2160z00_5289;

									if ((bgl_list_length(BgL_argsz00_3612) == 2L))
										{	/* SawBbv/bbv-cost.scm 63 */
											bool_t BgL__ortest_1197z00_3816;

											BgL__ortest_1197z00_3816 =
												(BGl_varz00zzast_nodez00 ==
												BGl_za2zc3flza2zc3zzsaw_bbvzd2cachezd2);
											if (BgL__ortest_1197z00_3816)
												{	/* SawBbv/bbv-cost.scm 63 */
													BgL_test2160z00_5289 = BgL__ortest_1197z00_3816;
												}
											else
												{	/* SawBbv/bbv-cost.scm 63 */
													bool_t BgL__ortest_1198z00_3817;

													BgL__ortest_1198z00_3817 =
														(BGl_varz00zzast_nodez00 ==
														BGl_za2zc3zd3flza2z10zzsaw_bbvzd2cachezd2);
													if (BgL__ortest_1198z00_3817)
														{	/* SawBbv/bbv-cost.scm 63 */
															BgL_test2160z00_5289 = BgL__ortest_1198z00_3817;
														}
													else
														{	/* SawBbv/bbv-cost.scm 64 */
															bool_t BgL__ortest_1199z00_3818;

															BgL__ortest_1199z00_3818 =
																(BGl_varz00zzast_nodez00 ==
																BGl_za2ze3flza2ze3zzsaw_bbvzd2cachezd2);
															if (BgL__ortest_1199z00_3818)
																{	/* SawBbv/bbv-cost.scm 64 */
																	BgL_test2160z00_5289 =
																		BgL__ortest_1199z00_3818;
																}
															else
																{	/* SawBbv/bbv-cost.scm 64 */
																	bool_t BgL__ortest_1200z00_3819;

																	BgL__ortest_1200z00_3819 =
																		(BGl_varz00zzast_nodez00 ==
																		BGl_za2ze3zd3flza2z30zzsaw_bbvzd2cachezd2);
																	if (BgL__ortest_1200z00_3819)
																		{	/* SawBbv/bbv-cost.scm 64 */
																			BgL_test2160z00_5289 =
																				BgL__ortest_1200z00_3819;
																		}
																	else
																		{	/* SawBbv/bbv-cost.scm 65 */
																			bool_t BgL__ortest_1201z00_3820;

																			BgL__ortest_1201z00_3820 =
																				(BGl_varz00zzast_nodez00 ==
																				BGl_za2zd3flza2zd3zzsaw_bbvzd2cachezd2);
																			if (BgL__ortest_1201z00_3820)
																				{	/* SawBbv/bbv-cost.scm 65 */
																					BgL_test2160z00_5289 =
																						BgL__ortest_1201z00_3820;
																				}
																			else
																				{	/* SawBbv/bbv-cost.scm 66 */
																					bool_t BgL__ortest_1202z00_3821;

																					BgL__ortest_1202z00_3821 =
																						(BGl_varz00zzast_nodez00 ==
																						BGl_za2zb2flza2zb2zzsaw_bbvzd2cachezd2);
																					if (BgL__ortest_1202z00_3821)
																						{	/* SawBbv/bbv-cost.scm 66 */
																							BgL_test2160z00_5289 =
																								BgL__ortest_1202z00_3821;
																						}
																					else
																						{	/* SawBbv/bbv-cost.scm 66 */
																							BgL_test2160z00_5289 =
																								(BGl_varz00zzast_nodez00 ==
																								BGl_za2zd2flza2zd2zzsaw_bbvzd2cachezd2);
																						}
																				}
																		}
																}
														}
												}
										}
									else
										{	/* SawBbv/bbv-cost.scm 62 */
											BgL_test2160z00_5289 = ((bool_t) 0);
										}
									if (BgL_test2160z00_5289)
										{	/* SawBbv/bbv-cost.scm 68 */
											obj_t BgL_arg1740z00_3822;

											{	/* SawBbv/bbv-cost.scm 68 */
												obj_t BgL_runner1748z00_3823;

												if (NULLP(BgL_argsz00_3612))
													{	/* SawBbv/bbv-cost.scm 68 */
														BgL_runner1748z00_3823 = BNIL;
													}
												else
													{	/* SawBbv/bbv-cost.scm 68 */
														obj_t BgL_head1434z00_3824;

														BgL_head1434z00_3824 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1432z00_3826;
															obj_t BgL_tail1435z00_3827;

															BgL_l1432z00_3826 = BgL_argsz00_3612;
															BgL_tail1435z00_3827 = BgL_head1434z00_3824;
														BgL_zc3z04anonymousza31742ze3z87_3825:
															if (NULLP(BgL_l1432z00_3826))
																{	/* SawBbv/bbv-cost.scm 68 */
																	BgL_runner1748z00_3823 =
																		CDR(BgL_head1434z00_3824);
																}
															else
																{	/* SawBbv/bbv-cost.scm 68 */
																	obj_t BgL_newtail1436z00_3828;

																	{	/* SawBbv/bbv-cost.scm 68 */
																		obj_t BgL_arg1747z00_3829;

																		{	/* SawBbv/bbv-cost.scm 68 */
																			obj_t BgL_az00_3830;

																			BgL_az00_3830 =
																				CAR(((obj_t) BgL_l1432z00_3826));
																			BgL_arg1747z00_3829 =
																				BGl_rtl_funzd2costzd2zzsaw_bbvzd2costzd2
																				(BgL_az00_3830, BNIL);
																		}
																		BgL_newtail1436z00_3828 =
																			MAKE_YOUNG_PAIR(BgL_arg1747z00_3829,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1435z00_3827,
																		BgL_newtail1436z00_3828);
																	{	/* SawBbv/bbv-cost.scm 68 */
																		obj_t BgL_arg1746z00_3831;

																		BgL_arg1746z00_3831 =
																			CDR(((obj_t) BgL_l1432z00_3826));
																		{
																			obj_t BgL_tail1435z00_5320;
																			obj_t BgL_l1432z00_5319;

																			BgL_l1432z00_5319 = BgL_arg1746z00_3831;
																			BgL_tail1435z00_5320 =
																				BgL_newtail1436z00_3828;
																			BgL_tail1435z00_3827 =
																				BgL_tail1435z00_5320;
																			BgL_l1432z00_3826 = BgL_l1432z00_5319;
																			goto
																				BgL_zc3z04anonymousza31742ze3z87_3825;
																		}
																	}
																}
														}
													}
												BgL_arg1740z00_3822 =
													BGl_zb2zb2zz__r4_numbers_6_5z00
													(BgL_runner1748z00_3823);
											}
											BgL_tmpz00_5199 = (2L + (long) CINT(BgL_arg1740z00_3822));
										}
									else
										{	/* SawBbv/bbv-cost.scm 62 */
											BgL_tmpz00_5199 = 0L;
										}
								}
						}
				}
				return BINT(BgL_tmpz00_5199);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2costzd2(void)
	{
		{	/* SawBbv/bbv-cost.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
			BGl_modulezd2initializa7ationz75zzsaw_regsetz00(358079690L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
			BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(237915200L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(425659118L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(242097300L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
			return
				BGl_modulezd2initializa7ationz75zzsaw_bbvzd2utilszd2(221709922L,
				BSTRING_TO_STRING(BGl_string1959z00zzsaw_bbvzd2costzd2));
		}

	}

#ifdef __cplusplus
}
#endif
