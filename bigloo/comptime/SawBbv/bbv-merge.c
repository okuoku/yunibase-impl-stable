/*===========================================================================*/
/*   (SawBbv/bbv-merge.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawBbv/bbv-merge.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_SAW_BBVzd2MERGEzd2_TYPE_DEFINITIONS
#define BGL_BgL_SAW_BBVzd2MERGEzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_rtl_regzf2razf2_bgl
	{
		int BgL_numz00;
		obj_t BgL_colorz00;
		obj_t BgL_coalescez00;
		int BgL_occurrencesz00;
		obj_t BgL_interferez00;
		obj_t BgL_interfere2z00;
	}                      *BgL_rtl_regzf2razf2_bglt;

	typedef struct BgL_regsetz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_lengthz00;
		int BgL_msiza7eza7;
		obj_t BgL_regvz00;
		obj_t BgL_reglz00;
		obj_t BgL_stringz00;
	}                *BgL_regsetz00_bglt;

	typedef struct BgL_rtl_inszf2bbvzf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_outz00;
		obj_t BgL_inz00;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		obj_t BgL_z52hashz52;
	}                       *BgL_rtl_inszf2bbvzf2_bglt;

	typedef struct BgL_blockvz00_bgl
	{
		obj_t BgL_versionsz00;
		obj_t BgL_genericz00;
		long BgL_z52markz52;
		obj_t BgL_mergez00;
	}                *BgL_blockvz00_bglt;

	typedef struct BgL_blocksz00_bgl
	{
		long BgL_z52markz52;
		obj_t BgL_z52hashz52;
		obj_t BgL_z52blacklistz52;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		struct BgL_blockz00_bgl *BgL_parentz00;
		long BgL_gccntz00;
		long BgL_gcmarkz00;
		obj_t BgL_mblockz00;
		obj_t BgL_creatorz00;
		obj_t BgL_mergesz00;
		bool_t BgL_asleepz00;
	}                *BgL_blocksz00_bglt;

	typedef struct BgL_bbvzd2queuezd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_blocksz00;
		obj_t BgL_lastz00;
	}                     *BgL_bbvzd2queuezd2_bglt;

	typedef struct BgL_bbvzd2ctxzd2_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_idz00;
		obj_t BgL_entriesz00;
	}                   *BgL_bbvzd2ctxzd2_bglt;

	typedef struct BgL_bbvzd2ctxentryzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_rtl_regz00_bgl *BgL_regz00;
		obj_t BgL_typesz00;
		bool_t BgL_polarityz00;
		long BgL_countz00;
		obj_t BgL_valuez00;
		obj_t BgL_aliasesz00;
		obj_t BgL_initvalz00;
	}                        *BgL_bbvzd2ctxentryzd2_bglt;

	typedef struct BgL_bbvzd2rangezd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_loz00;
		obj_t BgL_upz00;
	}                     *BgL_bbvzd2rangezd2_bglt;

	typedef struct BgL_bbvzd2vlenzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_vecz00;
		int BgL_offsetz00;
	}                    *BgL_bbvzd2vlenzd2_bglt;


#endif													// BGL_BgL_SAW_BBVzd2MERGEzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2ctxzd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt,
		BgL_bbvzd2ctxzd2_bglt);
	static BgL_blockz00_bglt BGl_z62blockSzd2parentzb0zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	static obj_t BGl_zc3z04anonymousza32557ze3ze70z60zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t
		BGl_callzd2withzd2inputzd2stringzd2zz__r4_ports_6_10_1z00(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2;
	BGL_EXPORTED_DECL bool_t
		BGl_bbvzd2ctxentryzd2polarityz00zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	static BgL_bbvzd2ctxzd2_bglt
		BGl_mergezd2ctxzd2zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxzd2_bglt,
		BgL_bbvzd2ctxzd2_bglt);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_blockVzd2nilzd2zzsaw_bbvzd2mergezd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2firstzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	static obj_t BGl_z62blockVzd2firstzb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2defz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt);
	extern obj_t BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt,
		BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_inszf2bbvzd2defz42zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32003ze3ze5zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_z62blockSzd2asleepzb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_bbvzd2mergezd2 = BUNSPEC;
	static obj_t BGl_z62bbvzd2queuezd2lastz62zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	extern obj_t BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2;
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2gccntzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt, long);
	static obj_t BGl_z62zc3z04anonymousza32568ze3ze5zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_inszf2bbvzd2argsz42zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2siza7ez75zzsaw_bbvzd2mergezd2
		(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2typesz00zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	extern obj_t BGl_za2bignumza2z00zztype_cachez00;
	static BgL_blockz00_bglt BGl_z62blockVzd2nilzb0zzsaw_bbvzd2mergezd2(obj_t);
	BGL_IMPORT bool_t BGl_equalzf3zf3zz__r4_equivalence_6_2z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2argszd2setz12ze0zzsaw_bbvzd2mergezd2
		(BgL_rtl_insz00_bglt, obj_t);
	static obj_t BGl_z62blockSzd2z52hashze2zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2z52hashzd2setz12zd0zzsaw_bbvzd2mergezd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2ctxzf3z43zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_blockSzd2z52markz80zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62blockVzd2z52markze2zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62blockVzd2versionszd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockSzd2succszd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzf3z43zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_blockVzd2labelzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	static obj_t BGl_z62bbvzd2ctxentryzd2initvalz62zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2mergezd2(int, int, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2mergezd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_za2ADNsza2z00zzsaw_bbvzd2mergezd2 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2gcmarkzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		long);
	BGL_IMPORT obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	extern obj_t BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2mergezd2(BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, int, obj_t, obj_t, int, obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2z52spillz10zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2mergezd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62blockVzd2mergezb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static BgL_bbvzd2ctxzd2_bglt
		BGl_z62makezd2bbvzd2ctxz62zzsaw_bbvzd2mergezd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2inz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62blockSzd2predszd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2outzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_everyz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2mergezd2(void);
	static obj_t BGl_z62bbvzd2ctxentryzd2polarityz62zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	static obj_t BGl_z62blockVzd2succszd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2distancezd2zzsaw_bbvzd2mergezd2
		(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2z52spillzd2setz12zb2zzsaw_bbvzd2mergezd2
		(BgL_rtl_insz00_bglt, obj_t);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_za2ADNtza2z00zzsaw_bbvzd2mergezd2 = BUNSPEC;
	static obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2versionszd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzd2countz62zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	static obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_getenvz00zz__osz00(obj_t);
	static obj_t BGl_z62blockSzd2labelzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockVzd2predszd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2initvalz00zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	static obj_t BGl_z62blockVzd2genericzb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static long
		BGl_entryzd2siza7ez75zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2z52hashz72zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_genericzd2initzd2zzsaw_bbvzd2mergezd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt);
	static BgL_bbvzd2queuezd2_bglt
		BGl_z62bbvzd2queuezd2nilz62zzsaw_bbvzd2mergezd2(obj_t);
	static obj_t BGl_z62blockSzf3z91zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2outzd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2mergezd2(obj_t);
	static obj_t BGl_z62blockVzd2labelzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_objectzd2initzd2zzsaw_bbvzd2mergezd2(void);
	static obj_t BGl_z62rtl_inszf2bbvzd2funzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockSzd2z52markzd2setz12z22zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_2zb2zb2zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2argszd2setz12z82zzsaw_bbvzd2mergezd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62blockSzd2gcmarkzb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_typeszd2intersectionze70z35zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_bignum_mul(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32203ze3ze5zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2queuezd2lastz00zzsaw_bbvzd2mergezd2(BgL_bbvzd2queuezd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2mergeszd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_blockzd2distze70z35zzsaw_bbvzd2mergezd2(obj_t);
	static obj_t BGl_z62blockVzd2z52markzd2setz12z22zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt,
		obj_t);
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2outz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt);
	extern obj_t BGl_regsetz00zzsaw_regsetz00;
	static obj_t BGl_z62blockSzd2predszb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2outz42zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2funzd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt,
		BgL_rtl_funz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2queuezd2blocksz00zzsaw_bbvzd2mergezd2(BgL_bbvzd2queuezd2_bglt);
	static obj_t BGl_z62blockVzd2mergezd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockSzd2mblockzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_inszf2bbvzf3z01zzsaw_bbvzd2mergezd2(obj_t);
	static obj_t BGl_z62blockSzd2asleepzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_ctxzd2scoreze70z35zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxzd2_bglt);
	static obj_t BGl_z62blockVzf3z91zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t
		BGl_ctxzd2scoreze71z35zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxzd2_bglt);
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_bbvzd2queuezf3z21zzsaw_bbvzd2mergezd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2z52hashzd2setz12z40zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2valuez00zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	static BgL_bbvzd2ctxentryzd2_bglt
		BGl_z62makezd2bbvzd2ctxentryz62zzsaw_bbvzd2mergezd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_blockSzd2ctxzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t
		BGl_z62bbvzd2ctxentryzd2countzd2setz12za2zzsaw_bbvzd2mergezd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2succszd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32125ze3ze5zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockVzd2succszb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	static obj_t BGl_appendzd221011zd2zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_bbvzd2mergezd2(void);
	static obj_t BGl_z62blockSzd2mblockzb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62blockVzd2genericzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2genericzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	extern obj_t BGl_maxrvz00zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bbvzd2blockzd2mergez00zzsaw_bbvzd2mergezd2(obj_t);
	BGL_EXPORTED_DECL long
		BGl_bbvzd2ctxzd2idz00zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxzd2_bglt);
	static obj_t BGl_z62bbvzd2queuezf3z43zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62bbvzd2blockzd2mergez62zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_blockSzd2parentzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxentryzd2_bglt
		BGl_bbvzd2ctxentryzd2nilz00zzsaw_bbvzd2mergezd2(void);
	static obj_t
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_bbvzd2ctxzd2_bglt BGl_z62blockSzd2ctxzb0zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2mergezd2(void);
	extern obj_t BGl_zd3rvzd3zzsaw_bbvzd2rangezd2(obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_makezd2blockSzd2zzsaw_bbvzd2mergezd2(int, obj_t, obj_t, obj_t, long,
		obj_t, obj_t, BgL_bbvzd2ctxzd2_bglt, BgL_blockz00_bglt, long, long, obj_t,
		obj_t, obj_t, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxzd2idzd2setz12zc0zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxzd2_bglt,
		long);
	extern obj_t BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2;
	static obj_t BGl_listzd2eqzf3ze70zc6zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2predszd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_makezd2blockVzd2zzsaw_bbvzd2mergezd2(int, obj_t, obj_t, obj_t, obj_t,
		obj_t, long, obj_t);
	static obj_t BGl_z62bbvzd2ctxzd2idzd2setz12za2zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2queuezd2blockszd2setz12zc0zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2queuezd2_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_bbvzd2ctxzf3z21zzsaw_bbvzd2mergezd2(obj_t);
	extern obj_t BGl_za2realza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2destzd2setz12ze0zzsaw_bbvzd2mergezd2
		(BgL_rtl_insz00_bglt, obj_t);
	static BgL_bbvzd2queuezd2_bglt
		BGl_z62makezd2bbvzd2queuez62zzsaw_bbvzd2mergezd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2z52blacklistzd2setz12z40zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_blockSzd2asleepzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	static obj_t BGl_z62bbvzd2ctxzd2idz62zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62blockSzd2gccntzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzd2aliasesz62zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	static obj_t BGl_allze70ze7zzsaw_bbvzd2mergezd2(obj_t);
	static obj_t BGl_allze71ze7zzsaw_bbvzd2mergezd2(obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	static obj_t BGl_allze72ze7zzsaw_bbvzd2mergezd2(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32420ze3ze5zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_zb2zb2zz__r4_numbers_6_5z00(obj_t);
	static obj_t
		BGl_z62bbvzd2ctxentryzd2initvalzd2setz12za2zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_rangezd2wideningzd2zzsaw_bbvzd2mergezd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62blockSzd2firstzb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_blockSzd2gccntzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	static obj_t BGl_z62bbvzd2queuezd2blocksz62zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2mergezd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_rtl_inszf2bbvzd2ctxz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt);
	static BgL_bbvzd2ctxzd2_bglt
		BGl_z62rtl_inszf2bbvzd2ctxz42zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_bbvzd2ctxentryzf3z21zzsaw_bbvzd2mergezd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2z52blacklistz80zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_minrvz00zzsaw_bbvzd2rangezd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_bbvzd2ctxentryzd2regz00zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt);
	extern obj_t BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2;
	static obj_t
		BGl_z62bbvzd2ctxentryzd2aliaseszd2setz12za2zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2aliasesz00zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	static obj_t BGl_z62rtl_inszf2bbvzd2defzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_entryzd2scoreze70z35zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt);
	static obj_t
		BGl_entryzd2scoreze71z35zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt);
	static long
		BGl_entryzd2scoreze72z35zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt);
	static long
		BGl_entryzd2scoreze73z35zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt);
	static obj_t BGl_z62zc3z04anonymousza32511ze3ze5zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_entryzd2scoreze74z35zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2firstzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t BGl_z62blockSzd2mergeszd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_vectorzd2copy3zd2zz__r4_vectors_6_8z00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT long BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(long, long);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_EXPORTED_DECL BgL_rtl_insz00_bglt
		BGl_makezd2rtl_inszf2bbvz20zzsaw_bbvzd2mergezd2(obj_t, obj_t, obj_t,
		BgL_rtl_funz00_bglt, obj_t, obj_t, obj_t, obj_t, BgL_bbvzd2ctxzd2_bglt,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32317ze3ze5zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62bbvzd2queuezd2blockszd2setz12za2zzsaw_bbvzd2mergezd2(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_append2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2mergezd2(BgL_regsetz00_bglt);
	static obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2adnzd2zzsaw_bbvzd2mergezd2
		(obj_t);
	extern obj_t BGl_blockSz00zzsaw_bbvzd2typeszd2;
	static obj_t BGl_z62blockSzd2z52markze2zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62blockSzd2creatorzb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2destzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_blockVz00zzsaw_bbvzd2typeszd2;
	static obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2samepositivezd2zzsaw_bbvzd2mergezd2
		(obj_t);
	static obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2anynegativezd2zzsaw_bbvzd2mergezd2
		(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2versionszd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_regsetz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL int
		BGl_blockSzd2labelzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	static obj_t BGl_z62bbvzd2queuezd2lastzd2setz12za2zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockVzd2versionszb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2firstzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t BGl_z62blockVzd2labelzb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2z52spillz72zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2defzd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2queuezd2_bglt
		BGl_bbvzd2queuezd2nilz00zzsaw_bbvzd2mergezd2(void);
	static BgL_bbvzd2ctxentryzd2_bglt
		BGl_z62bbvzd2ctxentryzd2nilz62zzsaw_bbvzd2mergezd2(obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2z52hashz10zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2initvalzd2setz12zc0zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxentryzd2_bglt, obj_t);
	BGL_IMPORT obj_t bgl_list_ref(obj_t, long);
	static obj_t BGl_z62zc3z04anonymousza32432ze3ze5zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2firstzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL BgL_bbvzd2queuezd2_bglt
		BGl_makezd2bbvzd2queuez00zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2mergezd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2specializa7ez75(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2costzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2utilszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__configurez00(long,
		char *);
	BGL_EXPORTED_DECL long
		BGl_bbvzd2ctxentryzd2countz00zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	BGL_EXPORTED_DECL long
		BGl_blockSzd2gcmarkzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2genericzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_bbvzd2ctxzd2nilz00zzsaw_bbvzd2mergezd2(void);
	BGL_IMPORT obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t, obj_t);
	static obj_t BGl_za2ADNcza2z00zzsaw_bbvzd2mergezd2 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2aliaseszd2setz12zc0zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxentryzd2_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_blockSzd2nilzd2zzsaw_bbvzd2mergezd2(void);
	static BgL_blockz00_bglt BGl_z62makezd2blockSzb0zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzd2typesz62zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2mergezd2(obj_t);
	static BgL_blockz00_bglt BGl_z62makezd2blockVzb0zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2z52hashz80zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	static long BGl_typezd2siza7eze70z92zzsaw_bbvzd2mergezd2(obj_t);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2z52spillzd2setz12zd0zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_blockVzd2z52markz80zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	static obj_t
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2inzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2loczd2setz12z82zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockSzd2z52hashzd2setz12z22zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2mergezd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2mergezd2(void);
	static BgL_rtl_regz00_bglt
		BGl_z62bbvzd2ctxentryzd2regz62zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_funz00_bglt
		BGl_rtl_inszf2bbvzd2funz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2mergezd2(void);
	BGL_EXPORTED_DECL bool_t BGl_blockSzf3zf3zzsaw_bbvzd2mergezd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2mergezd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	static obj_t BGl_z62blockSzd2gcmarkzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static BgL_rtl_funz00_bglt
		BGl_z62rtl_inszf2bbvzd2funz42zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_makezd2bbvzd2ctxz00zzsaw_bbvzd2mergezd2(long, obj_t);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2mergezd2(void);
	extern obj_t BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2;
	static obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2mergezd2(void);
	static obj_t BGl_z62zc3z04anonymousza32290ze3ze5zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_blockzd2siza7ez75zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	static BgL_blockz00_bglt BGl_z62blockSzd2nilzb0zzsaw_bbvzd2mergezd2(obj_t);
	static BgL_rtl_insz00_bglt
		BGl_z62makezd2rtl_inszf2bbvz42zzsaw_bbvzd2mergezd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2creatorzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2mblockzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2asleepzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2destz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt);
	static obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2nearnegativezd2zzsaw_bbvzd2mergezd2
		(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2mblockzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2succszd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t
		BGl_z62blockSzd2z52blacklistzd2setz12z22zzsaw_bbvzd2mergezd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzf3z63zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2loczd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt,
		obj_t);
	static obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2nearobjzd2zzsaw_bbvzd2mergezd2
		(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2mergezd2
		(BgL_rtl_regz00_bglt, int);
	static obj_t BGl_z62zc3z04anonymousza32259ze3ze5zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_bigloozd2configzd2zz__configurez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2predszd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2succszd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt);
	static obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2firstzd2zzsaw_bbvzd2mergezd2
		(obj_t);
	static obj_t BGl_z62blockSzd2succszb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2z52hashzd2setz12zb2zzsaw_bbvzd2mergezd2
		(BgL_rtl_insz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32349ze3ze5zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	extern bool_t
		BGl_bbvzd2ctxzd2equalzf3zf3zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt,
		BgL_bbvzd2ctxzd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2mergezd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_blockVzf3zf3zzsaw_bbvzd2mergezd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2labelzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt, int);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2predszd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2queuezd2lastzd2setz12zc0zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2queuezd2_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_regsetzf3zf3zzsaw_bbvzd2mergezd2(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_insz00_bglt
		BGl_rtl_inszf2bbvzd2nilz20zzsaw_bbvzd2mergezd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2countzd2setz12zc0zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxentryzd2_bglt, long);
	static BgL_rtl_insz00_bglt
		BGl_z62rtl_inszf2bbvzd2nilz42zzsaw_bbvzd2mergezd2(obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2mergezd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2labelzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt, int);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2predszd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2mergezd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62blockVzd2predszb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2z52markzd2setz12z40zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		long);
	static obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2bintza2z00zztype_cachez00;
	static BgL_bbvzd2ctxzd2_bglt
		BGl_z62bbvzd2ctxzd2nilz62zzsaw_bbvzd2mergezd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2mergezd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_z62blockSzd2z52blacklistze2zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2locz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2mergezd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_z62blockSzd2mergeszb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2locz42zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzd2valuez62zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2z52markzd2setz12z40zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		long);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2mergezd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2argsz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_2zc3zd3z10zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2succszd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt);
	extern obj_t BGl_rtl_regzf2razf2zzsaw_regsetz00;
	static obj_t BGl_z62blockSzd2firstzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32474ze3ze5zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2mergezd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62bbvzd2ctxzd2entrieszd2setz12za2zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxzd2entriesz00zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxzd2_bglt);
	static obj_t BGl_z62blockSzd2gccntzb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2mergezd2(BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2inzd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt,
		obj_t);
	static obj_t BGl_z62bbvzd2ctxzd2entriesz62zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2destz42zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2mergezd2(BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxentryzd2_bglt
		BGl_makezd2bbvzd2ctxentryz00zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt, obj_t,
		bool_t, long, obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockVzd2firstzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2ctxzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2mergezd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt);
	extern obj_t BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2;
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2mergeszd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2randomzd2zzsaw_bbvzd2mergezd2
		(obj_t);
	extern obj_t BGl_blockz00zzsaw_defsz00;
	BGL_IMPORT bool_t BGl_2ze3zd3z30zz__r4_numbers_6_5z00(obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2inz42zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2scoreza2z70zzsaw_bbvzd2mergezd2
		(obj_t);
	static obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2scorezb2z60zzsaw_bbvzd2mergezd2
		(obj_t);
	extern BgL_bbvzd2rangezd2_bglt
		BGl_fixnumzd2rangezd2zzsaw_bbvzd2rangezd2(void);
	static obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2scorezd2z00zzsaw_bbvzd2mergezd2
		(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_regsetz00_bglt,
		int);
	static obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2score2zd2zzsaw_bbvzd2mergezd2
		(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxzd2entrieszd2setz12zc0zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxzd2_bglt, obj_t);
	static obj_t BGl_bbvzd2blockzd2mergezd2selectzd2zzsaw_bbvzd2mergezd2(obj_t);
	static BgL_bbvzd2ctxentryzd2_bglt
		BGl_mergezd2ctxentryzd2zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt,
		BgL_bbvzd2ctxentryzd2_bglt);
	static obj_t BGl_z62blockSzd2labelzb0zzsaw_bbvzd2mergezd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2mergezd2(obj_t,
		obj_t);
	static obj_t __cnst[14];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2asleepzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2asle3303z00,
		BGl_z62blockSzd2asleepzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52hashzd2setz12zd2envz92zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2za752h3304za7,
		BGl_z62blockSzd2z52hashzd2setz12z22zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2nilzd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2queueza7d3305za7,
		BGl_z62bbvzd2queuezd2nilz62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2namezd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73306za7,
		BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2argszd2setz12zd2envz32zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3307z00,
		BGl_z62rtl_inszf2bbvzd2argszd2setz12z82zzsaw_bbvzd2mergezd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2creatorzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2crea3308z00,
		BGl_z62blockSzd2creatorzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2mblockzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2mblo3309z00,
		BGl_z62blockSzd2mblockzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2nilzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762regsetza7d2nilza73310za7,
		BGl_z62regsetzd2nilzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2predszd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2pred3311z00,
		BGl_z62blockSzd2predszd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2inzd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3312z00,
		BGl_z62rtl_inszf2bbvzd2inz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2polarityzd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxentr3313z00,
		BGl_z62bbvzd2ctxentryzd2polarityz62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2firstzd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2firs3314z00,
		BGl_z62blockSzd2firstzd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockVzd2nilzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2nilza73315za7,
		BGl_z62blockVzd2nilzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52hashzd2envz52zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2za752h3316za7,
		BGl_z62blockSzd2z52hashze2zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2blockszd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2queueza7d3317za7,
		BGl_z62bbvzd2queuezd2blocksz62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	BGL_IMPORT obj_t BGl_portzd2ze3sexpzd2listzd2envz31zz__readerz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2setz12zd2envz32zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73318za7,
		BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2inzd2setz12zd2envz32zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3319z00,
		BGl_z62rtl_inszf2bbvzd2inzd2setz12z82zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73320za7,
		BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_string3300za700za7za7s3321za7, "saw_bbv-merge", 13);
	      DEFINE_STRING(BGl_string3301z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_string3301za700za7za7s3322za7,
		"_ int-size first random distance size anynegative nearnegative nearobj score2 score- score+ score* adn ",
		103);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2genericzd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2gene3323z00,
		BGl_z62blockVzd2genericzd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2blockzd2mergezd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2blockza7d3324za7,
		BGl_z62bbvzd2blockzd2mergez62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2setz12zd2envzc1zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73325za7,
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2mergezd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2setz12zd2envz32zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73326za7,
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2mergezd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2ctxzd2setz12zd2envz32zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3327z00,
		BGl_z62rtl_inszf2bbvzd2ctxzd2setz12z82zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73328za7,
		BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2z52hashzd2setz12zd2envz60zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3329z00,
		BGl_z62rtl_inszf2bbvzd2z52hashzd2setz12zd0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2setz12zd2envz32zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73330za7,
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2mergezd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2versionszd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2vers3331z00,
		BGl_z62blockVzd2versionszd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2regvzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762regsetza7d2regv3332z00,
		BGl_z62regsetzd2regvzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2bbvzd2ctxentryzd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762makeza7d2bbvza7d23333za7,
		BGl_z62makezd2bbvzd2ctxentryz62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2nilzd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3334z00,
		BGl_z62rtl_inszf2bbvzd2nilz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzf3zd2envz21zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7f3za791za73335z00,
		BGl_z62blockSzf3z91zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2initvalzd2setz12zd2envz12zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxentr3336z00,
		BGl_z62bbvzd2ctxentryzd2initvalzd2setz12za2zzsaw_bbvzd2mergezd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2typeszd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxentr3337z00,
		BGl_z62bbvzd2ctxentryzd2typesz62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762regsetza7d2leng3338z00,
		BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockVzd2firstzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2firs3339z00,
		BGl_z62blockVzd2firstzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2mblockzd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2mblo3340z00,
		BGl_z62blockSzd2mblockzd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2countzd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxentr3341z00,
		BGl_z62bbvzd2ctxentryzd2countz62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2msiza7ezd2envza7zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762regsetza7d2msiza73342za7,
		BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2nilzd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxza7d2n3343za7,
		BGl_z62bbvzd2ctxzd2nilz62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzf3zd2envzf3zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxentr3344z00,
		BGl_z62bbvzd2ctxentryzf3z43zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bbvzd2ctxzd2idzd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxza7d2i3345za7,
		BGl_z62bbvzd2ctxzd2idz62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2aliaseszd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxentr3346z00,
		BGl_z62bbvzd2ctxentryzd2aliasesz62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2nilzd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxentr3347z00,
		BGl_z62bbvzd2ctxentryzd2nilz62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2gcmarkzd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2gcma3348z00,
		BGl_z62blockSzd2gcmarkzd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73349za7,
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2setz12zd2envz32zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73350za7,
		BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2outzd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3351z00,
		BGl_z62rtl_inszf2bbvzd2outz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2nilzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2nilza73352za7,
		BGl_z62blockSzd2nilzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2rtl_inszf2bbvzd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762makeza7d2rtl_in3353z00,
		BGl_z62makezd2rtl_inszf2bbvz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 10);
	      DEFINE_STRING(BGl_string3282z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_string3282za700za7za7s3354za7, "bbv-block-merge-select", 22);
	      DEFINE_STRING(BGl_string3283z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_string3283za700za7za7s3355za7, "strategy not implemented", 24);
	      DEFINE_STRING(BGl_string3284z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_string3284za700za7za7s3356za7, "BIGLOOBBVADN", 12);
	      DEFINE_STRING(BGl_string3285z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_string3285za700za7za7s3357za7,
		"bbv-block-merge-select-strategy-adn", 35);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2firstzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2firs3358z00,
		BGl_z62blockSzd2firstzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3286z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_string3286za700za7za7s3359za7, "wrong adn", 9);
	      DEFINE_STRING(BGl_string3287z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_string3287za700za7za7s3360za7, "not adn provided", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73361za7,
		BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2z52markzd2envz52zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2za752m3362za7,
		BGl_z62blockVzd2z52markze2zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2labelzd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2labe3363z00,
		BGl_z62blockSzd2labelzd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2versionszd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2vers3364z00,
		BGl_z62blockVzd2versionszb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3288z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762za7c3za704anonymo3365za7,
		BGl_z62zc3z04anonymousza32003ze3ze5zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3289z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762za7c3za704anonymo3366za7,
		BGl_z62zc3z04anonymousza32125ze3ze5zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2z52hashzd2envza0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3367z00,
		BGl_z62rtl_inszf2bbvzd2z52hashz10zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockVzd2mergezd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2merg3368z00,
		BGl_z62blockVzd2mergezb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3290z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762za7c3za704anonymo3369za7,
		BGl_z62zc3z04anonymousza32203ze3ze5zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3291z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762za7c3za704anonymo3370za7,
		BGl_z62zc3z04anonymousza32290ze3ze5zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3292z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762za7c3za704anonymo3371za7,
		BGl_z62zc3z04anonymousza32259ze3ze5zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3293z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762za7c3za704anonymo3372za7,
		BGl_z62zc3z04anonymousza32349ze3ze5zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3294z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762za7c3za704anonymo3373za7,
		BGl_z62zc3z04anonymousza32317ze3ze5zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3295z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762za7c3za704anonymo3374za7,
		BGl_z62zc3z04anonymousza32420ze3ze5zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3296z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762za7c3za704anonymo3375za7,
		BGl_z62zc3z04anonymousza32432ze3ze5zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3297z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762za7c3za704anonymo3376za7,
		BGl_z62zc3z04anonymousza32474ze3ze5zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3298z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762za7c3za704anonymo3377za7,
		BGl_z62zc3z04anonymousza32511ze3ze5zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3299z00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762za7c3za704anonymo3378za7,
		BGl_z62zc3z04anonymousza32568ze3ze5zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	BGL_IMPORT obj_t BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52markzd2setz12zd2envz92zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2za752m3379za7,
		BGl_z62blockSzd2z52markzd2setz12z22zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2bbvzd2queuezd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762makeza7d2bbvza7d23380za7,
		BGl_z62makezd2bbvzd2queuez62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52markzd2envz52zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2za752m3381za7,
		BGl_z62blockSzd2z52markze2zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2ctxzd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3382z00,
		BGl_z62rtl_inszf2bbvzd2ctxz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2succszd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2succ3383z00,
		BGl_z62blockSzd2succszd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2nilzd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73384za7,
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73385za7,
		BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2blockSzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762makeza7d2blocks3386z00,
		BGl_z62makezd2blockSzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2numzd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73387za7,
		BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2blockszd2setz12zd2envz12zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2queueza7d3388za7,
		BGl_z62bbvzd2queuezd2blockszd2setz12za2zzsaw_bbvzd2mergezd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2entrieszd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxza7d2e3389za7,
		BGl_z62bbvzd2ctxzd2entriesz62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2predszd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2pred3390z00,
		BGl_z62blockVzd2predszd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762regsetza7d2stri3391z00,
		BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2firstzd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2firs3392z00,
		BGl_z62blockVzd2firstzd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2valuezd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxentr3393z00,
		BGl_z62bbvzd2ctxentryzd2valuez62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2gccntzd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2gccn3394z00,
		BGl_z62blockSzd2gccntzd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2setz12zd2envz32zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73395za7,
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2mergezd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2envz01zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73396za7,
		BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2ctxzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2ctxza73397za7,
		BGl_z62blockSzd2ctxzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2outzd2setz12zd2envz32zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3398z00,
		BGl_z62rtl_inszf2bbvzd2outzd2setz12z82zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762regsetza7d2stri3399z00,
		BGl_z62regsetzd2stringzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2asleepzd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2asle3400z00,
		BGl_z62blockSzd2asleepzd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2gcmarkzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2gcma3401z00,
		BGl_z62blockSzd2gcmarkzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52blacklistzd2envz52zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2za752b3402za7,
		BGl_z62blockSzd2z52blacklistze2zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2setz12zd2envz32zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73403za7,
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2mergezd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2rtl_regzf2razd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762makeza7d2rtl_re3404z00,
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2argszd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3405z00,
		BGl_z62rtl_inszf2bbvzd2argsz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2idzd2setz12zd2envz12zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxza7d2i3406za7,
		BGl_z62bbvzd2ctxzd2idzd2setz12za2zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razf3zd2envzd3zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73407za7,
		BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2z52spillzd2setz12zd2envz60zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3408z00,
		BGl_z62rtl_inszf2bbvzd2z52spillzd2setz12zd0zzsaw_bbvzd2mergezd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762regsetza7d2leng3409z00,
		BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzf3zd2envzd3zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3410z00,
		BGl_z62rtl_inszf2bbvzf3z63zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2lastzd2setz12zd2envz12zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2queueza7d3411za7,
		BGl_z62bbvzd2queuezd2lastzd2setz12za2zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bbvzd2queuezf3zd2envzf3zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2queueza7f3412za7,
		BGl_z62bbvzd2queuezf3z43zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2initvalzd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxentr3413z00,
		BGl_z62bbvzd2ctxentryzd2initvalz62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73414za7,
		BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73415za7,
		BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2gccntzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2gccn3416z00,
		BGl_z62blockSzd2gccntzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2genericzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2gene3417z00,
		BGl_z62blockVzd2genericzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2hardwarezd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73418za7,
		BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2loczd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3419z00,
		BGl_z62rtl_inszf2bbvzd2locz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2aliaseszd2setz12zd2envz12zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxentr3420z00,
		BGl_z62bbvzd2ctxentryzd2aliaseszd2setz12za2zzsaw_bbvzd2mergezd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2regsetzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762makeza7d2regset3421z00,
		BGl_z62makezd2regsetzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockVzd2succszd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2succ3422z00,
		BGl_z62blockVzd2succszb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2funzd2setz12zd2envz32zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3423z00,
		BGl_z62rtl_inszf2bbvzd2funzd2setz12z82zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2destzd2setz12zd2envz32zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3424z00,
		BGl_z62rtl_inszf2bbvzd2destzd2setz12z82zzsaw_bbvzd2mergezd2, 0L, BUNSPEC,
		2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockVzf3zd2envz21zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7f3za791za73425z00,
		BGl_z62blockVzf3z91zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52blacklistzd2setz12zd2envz92zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2za752b3426za7,
		BGl_z62blockSzd2z52blacklistzd2setz12z22zzsaw_bbvzd2mergezd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2countzd2setz12zd2envz12zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxentr3427z00,
		BGl_z62bbvzd2ctxentryzd2countzd2setz12za2zzsaw_bbvzd2mergezd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2z52spillzd2envza0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3428z00,
		BGl_z62rtl_inszf2bbvzd2z52spillz10zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2defzd2setz12zd2envz32zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3429z00,
		BGl_z62rtl_inszf2bbvzd2defzd2setz12z82zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2bbvzd2ctxzd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762makeza7d2bbvza7d23430za7,
		BGl_z62makezd2bbvzd2ctxz62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2labelzd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2labe3431z00,
		BGl_z62blockVzd2labelzd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2succszd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2succ3432z00,
		BGl_z62blockSzd2succszb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2entrieszd2setz12zd2envz12zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxza7d2e3433za7,
		BGl_z62bbvzd2ctxzd2entrieszd2setz12za2zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2destzd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3434z00,
		BGl_z62rtl_inszf2bbvzd2destz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2loczd2setz12zd2envz32zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3435z00,
		BGl_z62rtl_inszf2bbvzd2loczd2setz12z82zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2lastzd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2queueza7d3436za7,
		BGl_z62bbvzd2queuezd2lastz62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockVzd2labelzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2labe3437z00,
		BGl_z62blockVzd2labelzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2regzd2envzd2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxentr3438z00,
		BGl_z62bbvzd2ctxentryzd2regz62zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2z52markzd2setz12zd2envz92zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2za752m3439za7,
		BGl_z62blockVzd2z52markzd2setz12z22zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2succszd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2succ3440z00,
		BGl_z62blockVzd2succszd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockVzd2predszd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2pred3441z00,
		BGl_z62blockVzd2predszb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2blockVzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762makeza7d2blockv3442z00,
		BGl_z62makezd2blockVzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2mergezd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blockvza7d2merg3443z00,
		BGl_z62blockVzd2mergezd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzf3zd2envz21zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762regsetza7f3za791za73444z00,
		BGl_z62regsetzf3z91zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2labelzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2labe3445z00,
		BGl_z62blockSzd2labelzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2funzd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3446z00,
		BGl_z62rtl_inszf2bbvzd2funz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2keyzd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73447za7,
		BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2mergeszd2setz12zd2envzc0zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2merg3448z00,
		BGl_z62blockSzd2mergeszd2setz12z70zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzd2predszd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2pred3449z00,
		BGl_z62blockSzd2predszb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2mergeszd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2merg3450z00,
		BGl_z62blockSzd2mergeszb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2parentzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762blocksza7d2pare3451z00,
		BGl_z62blockSzd2parentzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2setz12zd2envz32zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_regza7f2raza73452za7,
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2mergezd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_bbvzd2ctxzf3zd2envzf3zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762bbvza7d2ctxza7f3za73453z00,
		BGl_z62bbvzd2ctxzf3z43zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2defzd2envzf2zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762rtl_insza7f2bbv3454z00,
		BGl_z62rtl_inszf2bbvzd2defz42zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2reglzd2envz00zzsaw_bbvzd2mergezd2,
		BgL_bgl_za762regsetza7d2regl3455z00,
		BGl_z62regsetzd2reglzb0zzsaw_bbvzd2mergezd2, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_bbvzd2mergezd2));
		     ADD_ROOT((void *) (&BGl_za2ADNsza2z00zzsaw_bbvzd2mergezd2));
		     ADD_ROOT((void *) (&BGl_za2ADNtza2z00zzsaw_bbvzd2mergezd2));
		     ADD_ROOT((void *) (&BGl_za2ADNcza2z00zzsaw_bbvzd2mergezd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2mergezd2(long
		BgL_checksumz00_6714, char *BgL_fromz00_6715)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_bbvzd2mergezd2))
				{
					BGl_requirezd2initializa7ationz75zzsaw_bbvzd2mergezd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_bbvzd2mergezd2();
					BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2mergezd2();
					BGl_cnstzd2initzd2zzsaw_bbvzd2mergezd2();
					BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2mergezd2();
					return BGl_toplevelzd2initzd2zzsaw_bbvzd2mergezd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawBbv/bbv-merge.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_bbv-merge");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_bbv-merge");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_bbv-merge");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_bbv-merge");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_bbv-merge");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_bbv-merge");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "saw_bbv-merge");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"saw_bbv-merge");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_bbv-merge");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_bbv-merge");
			BGl_modulezd2initializa7ationz75zz__configurez00(0L, "saw_bbv-merge");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_bbv-merge");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_bbv-merge");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"saw_bbv-merge");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"saw_bbv-merge");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawBbv/bbv-merge.scm 15 */
			{	/* SawBbv/bbv-merge.scm 15 */
				obj_t BgL_cportz00_6526;

				{	/* SawBbv/bbv-merge.scm 15 */
					obj_t BgL_stringz00_6533;

					BgL_stringz00_6533 = BGl_string3301z00zzsaw_bbvzd2mergezd2;
					{	/* SawBbv/bbv-merge.scm 15 */
						obj_t BgL_startz00_6534;

						BgL_startz00_6534 = BINT(0L);
						{	/* SawBbv/bbv-merge.scm 15 */
							obj_t BgL_endz00_6535;

							BgL_endz00_6535 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_6533)));
							{	/* SawBbv/bbv-merge.scm 15 */

								BgL_cportz00_6526 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_6533, BgL_startz00_6534, BgL_endz00_6535);
				}}}}
				{
					long BgL_iz00_6527;

					BgL_iz00_6527 = 13L;
				BgL_loopz00_6528:
					if ((BgL_iz00_6527 == -1L))
						{	/* SawBbv/bbv-merge.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* SawBbv/bbv-merge.scm 15 */
							{	/* SawBbv/bbv-merge.scm 15 */
								obj_t BgL_arg3302z00_6529;

								{	/* SawBbv/bbv-merge.scm 15 */

									{	/* SawBbv/bbv-merge.scm 15 */
										obj_t BgL_locationz00_6531;

										BgL_locationz00_6531 = BBOOL(((bool_t) 0));
										{	/* SawBbv/bbv-merge.scm 15 */

											BgL_arg3302z00_6529 =
												BGl_readz00zz__readerz00(BgL_cportz00_6526,
												BgL_locationz00_6531);
										}
									}
								}
								{	/* SawBbv/bbv-merge.scm 15 */
									int BgL_tmpz00_6748;

									BgL_tmpz00_6748 = (int) (BgL_iz00_6527);
									CNST_TABLE_SET(BgL_tmpz00_6748, BgL_arg3302z00_6529);
							}}
							{	/* SawBbv/bbv-merge.scm 15 */
								int BgL_auxz00_6532;

								BgL_auxz00_6532 = (int) ((BgL_iz00_6527 - 1L));
								{
									long BgL_iz00_6753;

									BgL_iz00_6753 = (long) (BgL_auxz00_6532);
									BgL_iz00_6527 = BgL_iz00_6753;
									goto BgL_loopz00_6528;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawBbv/bbv-merge.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawBbv/bbv-merge.scm 15 */
			BGl_za2ADNcza2z00zzsaw_bbvzd2mergezd2 = BFALSE;
			BGl_za2ADNtza2z00zzsaw_bbvzd2mergezd2 = BFALSE;
			return (BGl_za2ADNsza2z00zzsaw_bbvzd2mergezd2 = BFALSE, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzsaw_bbvzd2mergezd2(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_2165;

				BgL_headz00_2165 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2166;
					obj_t BgL_tailz00_2167;

					BgL_prevz00_2166 = BgL_headz00_2165;
					BgL_tailz00_2167 = BgL_l1z00_1;
				BgL_loopz00_2168:
					if (PAIRP(BgL_tailz00_2167))
						{
							obj_t BgL_newzd2prevzd2_2170;

							BgL_newzd2prevzd2_2170 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2167), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2166, BgL_newzd2prevzd2_2170);
							{
								obj_t BgL_tailz00_6763;
								obj_t BgL_prevz00_6762;

								BgL_prevz00_6762 = BgL_newzd2prevzd2_2170;
								BgL_tailz00_6763 = CDR(BgL_tailz00_2167);
								BgL_tailz00_2167 = BgL_tailz00_6763;
								BgL_prevz00_2166 = BgL_prevz00_6762;
								goto BgL_loopz00_2168;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_2165);
				}
			}
		}

	}



/* make-rtl_reg/ra */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2mergezd2(BgL_typez00_bglt
		BgL_type1179z00_3, obj_t BgL_var1180z00_4, obj_t BgL_onexprzf31181zf3_5,
		obj_t BgL_name1182z00_6, obj_t BgL_key1183z00_7,
		obj_t BgL_hardware1184z00_8, int BgL_num1185z00_9,
		obj_t BgL_color1186z00_10, obj_t BgL_coalesce1187z00_11,
		int BgL_occurrences1188z00_12, obj_t BgL_interfere1189z00_13,
		obj_t BgL_interfere21190z00_14)
	{
		{	/* SawMill/regset.sch 55 */
			{	/* SawMill/regset.sch 55 */
				BgL_rtl_regz00_bglt BgL_new1166z00_6537;

				{	/* SawMill/regset.sch 55 */
					BgL_rtl_regz00_bglt BgL_tmp1164z00_6538;
					BgL_rtl_regzf2razf2_bglt BgL_wide1165z00_6539;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_6766;

						{	/* SawMill/regset.sch 55 */
							BgL_rtl_regz00_bglt BgL_new1163z00_6540;

							BgL_new1163z00_6540 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/regset.sch 55 */
								long BgL_arg1965z00_6541;

								BgL_arg1965z00_6541 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1163z00_6540),
									BgL_arg1965z00_6541);
							}
							{	/* SawMill/regset.sch 55 */
								BgL_objectz00_bglt BgL_tmpz00_6771;

								BgL_tmpz00_6771 = ((BgL_objectz00_bglt) BgL_new1163z00_6540);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6771, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1163z00_6540);
							BgL_auxz00_6766 = BgL_new1163z00_6540;
						}
						BgL_tmp1164z00_6538 = ((BgL_rtl_regz00_bglt) BgL_auxz00_6766);
					}
					BgL_wide1165z00_6539 =
						((BgL_rtl_regzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regzf2razf2_bgl))));
					{	/* SawMill/regset.sch 55 */
						obj_t BgL_auxz00_6779;
						BgL_objectz00_bglt BgL_tmpz00_6777;

						BgL_auxz00_6779 = ((obj_t) BgL_wide1165z00_6539);
						BgL_tmpz00_6777 = ((BgL_objectz00_bglt) BgL_tmp1164z00_6538);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6777, BgL_auxz00_6779);
					}
					((BgL_objectz00_bglt) BgL_tmp1164z00_6538);
					{	/* SawMill/regset.sch 55 */
						long BgL_arg1964z00_6542;

						BgL_arg1964z00_6542 =
							BGL_CLASS_NUM(BGl_rtl_regzf2razf2zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1164z00_6538), BgL_arg1964z00_6542);
					}
					BgL_new1166z00_6537 = ((BgL_rtl_regz00_bglt) BgL_tmp1164z00_6538);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1166z00_6537)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1179z00_3), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_6537)))->BgL_varz00) =
					((obj_t) BgL_var1180z00_4), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_6537)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31181zf3_5), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_6537)))->BgL_namez00) =
					((obj_t) BgL_name1182z00_6), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_6537)))->BgL_keyz00) =
					((obj_t) BgL_key1183z00_7), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_6537)))->BgL_debugnamez00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_6537)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1184z00_8), BUNSPEC);
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_6801;

					{
						obj_t BgL_auxz00_6802;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_6803;

							BgL_tmpz00_6803 = ((BgL_objectz00_bglt) BgL_new1166z00_6537);
							BgL_auxz00_6802 = BGL_OBJECT_WIDENING(BgL_tmpz00_6803);
						}
						BgL_auxz00_6801 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6802);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6801))->BgL_numz00) =
						((int) BgL_num1185z00_9), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_6808;

					{
						obj_t BgL_auxz00_6809;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_6810;

							BgL_tmpz00_6810 = ((BgL_objectz00_bglt) BgL_new1166z00_6537);
							BgL_auxz00_6809 = BGL_OBJECT_WIDENING(BgL_tmpz00_6810);
						}
						BgL_auxz00_6808 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6809);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6808))->
							BgL_colorz00) = ((obj_t) BgL_color1186z00_10), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_6815;

					{
						obj_t BgL_auxz00_6816;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_6817;

							BgL_tmpz00_6817 = ((BgL_objectz00_bglt) BgL_new1166z00_6537);
							BgL_auxz00_6816 = BGL_OBJECT_WIDENING(BgL_tmpz00_6817);
						}
						BgL_auxz00_6815 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6816);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6815))->
							BgL_coalescez00) = ((obj_t) BgL_coalesce1187z00_11), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_6822;

					{
						obj_t BgL_auxz00_6823;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_6824;

							BgL_tmpz00_6824 = ((BgL_objectz00_bglt) BgL_new1166z00_6537);
							BgL_auxz00_6823 = BGL_OBJECT_WIDENING(BgL_tmpz00_6824);
						}
						BgL_auxz00_6822 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6823);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6822))->
							BgL_occurrencesz00) = ((int) BgL_occurrences1188z00_12), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_6829;

					{
						obj_t BgL_auxz00_6830;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_6831;

							BgL_tmpz00_6831 = ((BgL_objectz00_bglt) BgL_new1166z00_6537);
							BgL_auxz00_6830 = BGL_OBJECT_WIDENING(BgL_tmpz00_6831);
						}
						BgL_auxz00_6829 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6830);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6829))->
							BgL_interferez00) = ((obj_t) BgL_interfere1189z00_13), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_6836;

					{
						obj_t BgL_auxz00_6837;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_6838;

							BgL_tmpz00_6838 = ((BgL_objectz00_bglt) BgL_new1166z00_6537);
							BgL_auxz00_6837 = BGL_OBJECT_WIDENING(BgL_tmpz00_6838);
						}
						BgL_auxz00_6836 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6837);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6836))->
							BgL_interfere2z00) = ((obj_t) BgL_interfere21190z00_14), BUNSPEC);
				}
				return BgL_new1166z00_6537;
			}
		}

	}



/* &make-rtl_reg/ra */
	BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6113, obj_t BgL_type1179z00_6114, obj_t BgL_var1180z00_6115,
		obj_t BgL_onexprzf31181zf3_6116, obj_t BgL_name1182z00_6117,
		obj_t BgL_key1183z00_6118, obj_t BgL_hardware1184z00_6119,
		obj_t BgL_num1185z00_6120, obj_t BgL_color1186z00_6121,
		obj_t BgL_coalesce1187z00_6122, obj_t BgL_occurrences1188z00_6123,
		obj_t BgL_interfere1189z00_6124, obj_t BgL_interfere21190z00_6125)
	{
		{	/* SawMill/regset.sch 55 */
			return
				BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2mergezd2(
				((BgL_typez00_bglt) BgL_type1179z00_6114), BgL_var1180z00_6115,
				BgL_onexprzf31181zf3_6116, BgL_name1182z00_6117, BgL_key1183z00_6118,
				BgL_hardware1184z00_6119, CINT(BgL_num1185z00_6120),
				BgL_color1186z00_6121, BgL_coalesce1187z00_6122,
				CINT(BgL_occurrences1188z00_6123), BgL_interfere1189z00_6124,
				BgL_interfere21190z00_6125);
		}

	}



/* rtl_reg/ra? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2mergezd2(obj_t
		BgL_objz00_15)
	{
		{	/* SawMill/regset.sch 56 */
			{	/* SawMill/regset.sch 56 */
				obj_t BgL_classz00_6543;

				BgL_classz00_6543 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_15))
					{	/* SawMill/regset.sch 56 */
						BgL_objectz00_bglt BgL_arg1807z00_6544;

						BgL_arg1807z00_6544 = (BgL_objectz00_bglt) (BgL_objz00_15);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 56 */
								long BgL_idxz00_6545;

								BgL_idxz00_6545 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6544);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6545 + 2L)) == BgL_classz00_6543);
							}
						else
							{	/* SawMill/regset.sch 56 */
								bool_t BgL_res3255z00_6548;

								{	/* SawMill/regset.sch 56 */
									obj_t BgL_oclassz00_6549;

									{	/* SawMill/regset.sch 56 */
										obj_t BgL_arg1815z00_6550;
										long BgL_arg1816z00_6551;

										BgL_arg1815z00_6550 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 56 */
											long BgL_arg1817z00_6552;

											BgL_arg1817z00_6552 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6544);
											BgL_arg1816z00_6551 = (BgL_arg1817z00_6552 - OBJECT_TYPE);
										}
										BgL_oclassz00_6549 =
											VECTOR_REF(BgL_arg1815z00_6550, BgL_arg1816z00_6551);
									}
									{	/* SawMill/regset.sch 56 */
										bool_t BgL__ortest_1115z00_6553;

										BgL__ortest_1115z00_6553 =
											(BgL_classz00_6543 == BgL_oclassz00_6549);
										if (BgL__ortest_1115z00_6553)
											{	/* SawMill/regset.sch 56 */
												BgL_res3255z00_6548 = BgL__ortest_1115z00_6553;
											}
										else
											{	/* SawMill/regset.sch 56 */
												long BgL_odepthz00_6554;

												{	/* SawMill/regset.sch 56 */
													obj_t BgL_arg1804z00_6555;

													BgL_arg1804z00_6555 = (BgL_oclassz00_6549);
													BgL_odepthz00_6554 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6555);
												}
												if ((2L < BgL_odepthz00_6554))
													{	/* SawMill/regset.sch 56 */
														obj_t BgL_arg1802z00_6556;

														{	/* SawMill/regset.sch 56 */
															obj_t BgL_arg1803z00_6557;

															BgL_arg1803z00_6557 = (BgL_oclassz00_6549);
															BgL_arg1802z00_6556 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6557,
																2L);
														}
														BgL_res3255z00_6548 =
															(BgL_arg1802z00_6556 == BgL_classz00_6543);
													}
												else
													{	/* SawMill/regset.sch 56 */
														BgL_res3255z00_6548 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3255z00_6548;
							}
					}
				else
					{	/* SawMill/regset.sch 56 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_reg/ra? */
	obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6126,
		obj_t BgL_objz00_6127)
	{
		{	/* SawMill/regset.sch 56 */
			return BBOOL(BGl_rtl_regzf2razf3z01zzsaw_bbvzd2mergezd2(BgL_objz00_6127));
		}

	}



/* rtl_reg/ra-nil */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawMill/regset.sch 57 */
			{	/* SawMill/regset.sch 57 */
				obj_t BgL_classz00_4458;

				BgL_classz00_4458 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				{	/* SawMill/regset.sch 57 */
					obj_t BgL__ortest_1117z00_4459;

					BgL__ortest_1117z00_4459 = BGL_CLASS_NIL(BgL_classz00_4458);
					if (CBOOL(BgL__ortest_1117z00_4459))
						{	/* SawMill/regset.sch 57 */
							return ((BgL_rtl_regz00_bglt) BgL__ortest_1117z00_4459);
						}
					else
						{	/* SawMill/regset.sch 57 */
							return
								((BgL_rtl_regz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4458));
						}
				}
			}
		}

	}



/* &rtl_reg/ra-nil */
	BgL_rtl_regz00_bglt BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6128)
	{
		{	/* SawMill/regset.sch 57 */
			return BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2mergezd2();
		}

	}



/* rtl_reg/ra-interfere2 */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_oz00_16)
	{
		{	/* SawMill/regset.sch 58 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_6878;

				{
					obj_t BgL_auxz00_6879;

					{	/* SawMill/regset.sch 58 */
						BgL_objectz00_bglt BgL_tmpz00_6880;

						BgL_tmpz00_6880 = ((BgL_objectz00_bglt) BgL_oz00_16);
						BgL_auxz00_6879 = BGL_OBJECT_WIDENING(BgL_tmpz00_6880);
					}
					BgL_auxz00_6878 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6879);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6878))->
					BgL_interfere2z00);
			}
		}

	}



/* &rtl_reg/ra-interfere2 */
	obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6129, obj_t BgL_oz00_6130)
	{
		{	/* SawMill/regset.sch 58 */
			return
				BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6130));
		}

	}



/* rtl_reg/ra-interfere2-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2mergezd2
		(BgL_rtl_regz00_bglt BgL_oz00_17, obj_t BgL_vz00_18)
	{
		{	/* SawMill/regset.sch 59 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_6887;

				{
					obj_t BgL_auxz00_6888;

					{	/* SawMill/regset.sch 59 */
						BgL_objectz00_bglt BgL_tmpz00_6889;

						BgL_tmpz00_6889 = ((BgL_objectz00_bglt) BgL_oz00_17);
						BgL_auxz00_6888 = BGL_OBJECT_WIDENING(BgL_tmpz00_6889);
					}
					BgL_auxz00_6887 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6888);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6887))->
						BgL_interfere2z00) = ((obj_t) BgL_vz00_18), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere2-set! */
	obj_t BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6131, obj_t BgL_oz00_6132, obj_t BgL_vz00_6133)
	{
		{	/* SawMill/regset.sch 59 */
			return
				BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6132), BgL_vz00_6133);
		}

	}



/* rtl_reg/ra-interfere */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_oz00_19)
	{
		{	/* SawMill/regset.sch 60 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_6896;

				{
					obj_t BgL_auxz00_6897;

					{	/* SawMill/regset.sch 60 */
						BgL_objectz00_bglt BgL_tmpz00_6898;

						BgL_tmpz00_6898 = ((BgL_objectz00_bglt) BgL_oz00_19);
						BgL_auxz00_6897 = BGL_OBJECT_WIDENING(BgL_tmpz00_6898);
					}
					BgL_auxz00_6896 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6897);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6896))->
					BgL_interferez00);
			}
		}

	}



/* &rtl_reg/ra-interfere */
	obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6134, obj_t BgL_oz00_6135)
	{
		{	/* SawMill/regset.sch 60 */
			return
				BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6135));
		}

	}



/* rtl_reg/ra-interfere-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2mergezd2
		(BgL_rtl_regz00_bglt BgL_oz00_20, obj_t BgL_vz00_21)
	{
		{	/* SawMill/regset.sch 61 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_6905;

				{
					obj_t BgL_auxz00_6906;

					{	/* SawMill/regset.sch 61 */
						BgL_objectz00_bglt BgL_tmpz00_6907;

						BgL_tmpz00_6907 = ((BgL_objectz00_bglt) BgL_oz00_20);
						BgL_auxz00_6906 = BGL_OBJECT_WIDENING(BgL_tmpz00_6907);
					}
					BgL_auxz00_6905 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6906);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6905))->
						BgL_interferez00) = ((obj_t) BgL_vz00_21), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere-set! */
	obj_t BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6136, obj_t BgL_oz00_6137, obj_t BgL_vz00_6138)
	{
		{	/* SawMill/regset.sch 61 */
			return
				BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6137), BgL_vz00_6138);
		}

	}



/* rtl_reg/ra-occurrences */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_oz00_22)
	{
		{	/* SawMill/regset.sch 62 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_6914;

				{
					obj_t BgL_auxz00_6915;

					{	/* SawMill/regset.sch 62 */
						BgL_objectz00_bglt BgL_tmpz00_6916;

						BgL_tmpz00_6916 = ((BgL_objectz00_bglt) BgL_oz00_22);
						BgL_auxz00_6915 = BGL_OBJECT_WIDENING(BgL_tmpz00_6916);
					}
					BgL_auxz00_6914 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6915);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6914))->
					BgL_occurrencesz00);
			}
		}

	}



/* &rtl_reg/ra-occurrences */
	obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6139, obj_t BgL_oz00_6140)
	{
		{	/* SawMill/regset.sch 62 */
			return
				BINT(BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2mergezd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_6140)));
		}

	}



/* rtl_reg/ra-occurrences-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2mergezd2
		(BgL_rtl_regz00_bglt BgL_oz00_23, int BgL_vz00_24)
	{
		{	/* SawMill/regset.sch 63 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_6924;

				{
					obj_t BgL_auxz00_6925;

					{	/* SawMill/regset.sch 63 */
						BgL_objectz00_bglt BgL_tmpz00_6926;

						BgL_tmpz00_6926 = ((BgL_objectz00_bglt) BgL_oz00_23);
						BgL_auxz00_6925 = BGL_OBJECT_WIDENING(BgL_tmpz00_6926);
					}
					BgL_auxz00_6924 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6925);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6924))->
						BgL_occurrencesz00) = ((int) BgL_vz00_24), BUNSPEC);
		}}

	}



/* &rtl_reg/ra-occurrences-set! */
	obj_t BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6141, obj_t BgL_oz00_6142, obj_t BgL_vz00_6143)
	{
		{	/* SawMill/regset.sch 63 */
			return
				BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6142), CINT(BgL_vz00_6143));
		}

	}



/* rtl_reg/ra-coalesce */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_oz00_25)
	{
		{	/* SawMill/regset.sch 64 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_6934;

				{
					obj_t BgL_auxz00_6935;

					{	/* SawMill/regset.sch 64 */
						BgL_objectz00_bglt BgL_tmpz00_6936;

						BgL_tmpz00_6936 = ((BgL_objectz00_bglt) BgL_oz00_25);
						BgL_auxz00_6935 = BGL_OBJECT_WIDENING(BgL_tmpz00_6936);
					}
					BgL_auxz00_6934 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6935);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6934))->
					BgL_coalescez00);
			}
		}

	}



/* &rtl_reg/ra-coalesce */
	obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6144, obj_t BgL_oz00_6145)
	{
		{	/* SawMill/regset.sch 64 */
			return
				BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6145));
		}

	}



/* rtl_reg/ra-coalesce-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2mergezd2
		(BgL_rtl_regz00_bglt BgL_oz00_26, obj_t BgL_vz00_27)
	{
		{	/* SawMill/regset.sch 65 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_6943;

				{
					obj_t BgL_auxz00_6944;

					{	/* SawMill/regset.sch 65 */
						BgL_objectz00_bglt BgL_tmpz00_6945;

						BgL_tmpz00_6945 = ((BgL_objectz00_bglt) BgL_oz00_26);
						BgL_auxz00_6944 = BGL_OBJECT_WIDENING(BgL_tmpz00_6945);
					}
					BgL_auxz00_6943 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6944);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6943))->
						BgL_coalescez00) = ((obj_t) BgL_vz00_27), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-coalesce-set! */
	obj_t BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6146, obj_t BgL_oz00_6147, obj_t BgL_vz00_6148)
	{
		{	/* SawMill/regset.sch 65 */
			return
				BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6147), BgL_vz00_6148);
		}

	}



/* rtl_reg/ra-color */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_oz00_28)
	{
		{	/* SawMill/regset.sch 66 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_6952;

				{
					obj_t BgL_auxz00_6953;

					{	/* SawMill/regset.sch 66 */
						BgL_objectz00_bglt BgL_tmpz00_6954;

						BgL_tmpz00_6954 = ((BgL_objectz00_bglt) BgL_oz00_28);
						BgL_auxz00_6953 = BGL_OBJECT_WIDENING(BgL_tmpz00_6954);
					}
					BgL_auxz00_6952 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6953);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6952))->BgL_colorz00);
			}
		}

	}



/* &rtl_reg/ra-color */
	obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6149, obj_t BgL_oz00_6150)
	{
		{	/* SawMill/regset.sch 66 */
			return
				BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6150));
		}

	}



/* rtl_reg/ra-color-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_oz00_29, obj_t BgL_vz00_30)
	{
		{	/* SawMill/regset.sch 67 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_6961;

				{
					obj_t BgL_auxz00_6962;

					{	/* SawMill/regset.sch 67 */
						BgL_objectz00_bglt BgL_tmpz00_6963;

						BgL_tmpz00_6963 = ((BgL_objectz00_bglt) BgL_oz00_29);
						BgL_auxz00_6962 = BGL_OBJECT_WIDENING(BgL_tmpz00_6963);
					}
					BgL_auxz00_6961 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6962);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6961))->
						BgL_colorz00) = ((obj_t) BgL_vz00_30), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-color-set! */
	obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6151, obj_t BgL_oz00_6152, obj_t BgL_vz00_6153)
	{
		{	/* SawMill/regset.sch 67 */
			return
				BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6152), BgL_vz00_6153);
		}

	}



/* rtl_reg/ra-num */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_oz00_31)
	{
		{	/* SawMill/regset.sch 68 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_6970;

				{
					obj_t BgL_auxz00_6971;

					{	/* SawMill/regset.sch 68 */
						BgL_objectz00_bglt BgL_tmpz00_6972;

						BgL_tmpz00_6972 = ((BgL_objectz00_bglt) BgL_oz00_31);
						BgL_auxz00_6971 = BGL_OBJECT_WIDENING(BgL_tmpz00_6972);
					}
					BgL_auxz00_6970 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6971);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_6970))->BgL_numz00);
			}
		}

	}



/* &rtl_reg/ra-num */
	obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6154,
		obj_t BgL_oz00_6155)
	{
		{	/* SawMill/regset.sch 68 */
			return
				BINT(BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2mergezd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_6155)));
		}

	}



/* rtl_reg/ra-hardware */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_oz00_34)
	{
		{	/* SawMill/regset.sch 70 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_34)))->BgL_hardwarez00);
		}

	}



/* &rtl_reg/ra-hardware */
	obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6156, obj_t BgL_oz00_6157)
	{
		{	/* SawMill/regset.sch 70 */
			return
				BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6157));
		}

	}



/* rtl_reg/ra-key */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_oz00_37)
	{
		{	/* SawMill/regset.sch 72 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_37)))->BgL_keyz00);
		}

	}



/* &rtl_reg/ra-key */
	obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6158,
		obj_t BgL_oz00_6159)
	{
		{	/* SawMill/regset.sch 72 */
			return
				BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6159));
		}

	}



/* rtl_reg/ra-name */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_oz00_40)
	{
		{	/* SawMill/regset.sch 74 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_40)))->BgL_namez00);
		}

	}



/* &rtl_reg/ra-name */
	obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6160,
		obj_t BgL_oz00_6161)
	{
		{	/* SawMill/regset.sch 74 */
			return
				BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6161));
		}

	}



/* rtl_reg/ra-onexpr? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_oz00_43)
	{
		{	/* SawMill/regset.sch 76 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_43)))->BgL_onexprzf3zf3);
		}

	}



/* &rtl_reg/ra-onexpr? */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6162, obj_t BgL_oz00_6163)
	{
		{	/* SawMill/regset.sch 76 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6163));
		}

	}



/* rtl_reg/ra-onexpr?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2mergezd2
		(BgL_rtl_regz00_bglt BgL_oz00_44, obj_t BgL_vz00_45)
	{
		{	/* SawMill/regset.sch 77 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_44)))->BgL_onexprzf3zf3) =
				((obj_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &rtl_reg/ra-onexpr?-set! */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6164, obj_t BgL_oz00_6165, obj_t BgL_vz00_6166)
	{
		{	/* SawMill/regset.sch 77 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6165), BgL_vz00_6166);
		}

	}



/* rtl_reg/ra-var */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_oz00_46)
	{
		{	/* SawMill/regset.sch 78 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_46)))->BgL_varz00);
		}

	}



/* &rtl_reg/ra-var */
	obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6167,
		obj_t BgL_oz00_6168)
	{
		{	/* SawMill/regset.sch 78 */
			return
				BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6168));
		}

	}



/* rtl_reg/ra-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_oz00_47, obj_t BgL_vz00_48)
	{
		{	/* SawMill/regset.sch 79 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_47)))->BgL_varz00) =
				((obj_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &rtl_reg/ra-var-set! */
	obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6169, obj_t BgL_oz00_6170, obj_t BgL_vz00_6171)
	{
		{	/* SawMill/regset.sch 79 */
			return
				BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6170), BgL_vz00_6171);
		}

	}



/* rtl_reg/ra-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_oz00_49)
	{
		{	/* SawMill/regset.sch 80 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_49)))->BgL_typez00);
		}

	}



/* &rtl_reg/ra-type */
	BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6172, obj_t BgL_oz00_6173)
	{
		{	/* SawMill/regset.sch 80 */
			return
				BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6173));
		}

	}



/* rtl_reg/ra-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_oz00_50, BgL_typez00_bglt BgL_vz00_51)
	{
		{	/* SawMill/regset.sch 81 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_50)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_51), BUNSPEC);
		}

	}



/* &rtl_reg/ra-type-set! */
	obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6174, obj_t BgL_oz00_6175, obj_t BgL_vz00_6176)
	{
		{	/* SawMill/regset.sch 81 */
			return
				BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_6175),
				((BgL_typez00_bglt) BgL_vz00_6176));
		}

	}



/* make-regset */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2mergezd2(int BgL_length1172z00_52,
		int BgL_msiza7e1173za7_53, obj_t BgL_regv1174z00_54,
		obj_t BgL_regl1175z00_55, obj_t BgL_string1176z00_56)
	{
		{	/* SawMill/regset.sch 84 */
			{	/* SawMill/regset.sch 84 */
				BgL_regsetz00_bglt BgL_new1168z00_6558;

				{	/* SawMill/regset.sch 84 */
					BgL_regsetz00_bglt BgL_new1167z00_6559;

					BgL_new1167z00_6559 =
						((BgL_regsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_regsetz00_bgl))));
					{	/* SawMill/regset.sch 84 */
						long BgL_arg1966z00_6560;

						BgL_arg1966z00_6560 = BGL_CLASS_NUM(BGl_regsetz00zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1167z00_6559), BgL_arg1966z00_6560);
					}
					BgL_new1168z00_6558 = BgL_new1167z00_6559;
				}
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_6558))->BgL_lengthz00) =
					((int) BgL_length1172z00_52), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_6558))->BgL_msiza7eza7) =
					((int) BgL_msiza7e1173za7_53), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_6558))->BgL_regvz00) =
					((obj_t) BgL_regv1174z00_54), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_6558))->BgL_reglz00) =
					((obj_t) BgL_regl1175z00_55), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_6558))->BgL_stringz00) =
					((obj_t) BgL_string1176z00_56), BUNSPEC);
				return BgL_new1168z00_6558;
			}
		}

	}



/* &make-regset */
	BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6177, obj_t BgL_length1172z00_6178,
		obj_t BgL_msiza7e1173za7_6179, obj_t BgL_regv1174z00_6180,
		obj_t BgL_regl1175z00_6181, obj_t BgL_string1176z00_6182)
	{
		{	/* SawMill/regset.sch 84 */
			return
				BGl_makezd2regsetzd2zzsaw_bbvzd2mergezd2(CINT(BgL_length1172z00_6178),
				CINT(BgL_msiza7e1173za7_6179), BgL_regv1174z00_6180,
				BgL_regl1175z00_6181, BgL_string1176z00_6182);
		}

	}



/* regset? */
	BGL_EXPORTED_DEF bool_t BGl_regsetzf3zf3zzsaw_bbvzd2mergezd2(obj_t
		BgL_objz00_57)
	{
		{	/* SawMill/regset.sch 85 */
			{	/* SawMill/regset.sch 85 */
				obj_t BgL_classz00_6561;

				BgL_classz00_6561 = BGl_regsetz00zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_57))
					{	/* SawMill/regset.sch 85 */
						BgL_objectz00_bglt BgL_arg1807z00_6562;

						BgL_arg1807z00_6562 = (BgL_objectz00_bglt) (BgL_objz00_57);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 85 */
								long BgL_idxz00_6563;

								BgL_idxz00_6563 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6562);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6563 + 1L)) == BgL_classz00_6561);
							}
						else
							{	/* SawMill/regset.sch 85 */
								bool_t BgL_res3256z00_6566;

								{	/* SawMill/regset.sch 85 */
									obj_t BgL_oclassz00_6567;

									{	/* SawMill/regset.sch 85 */
										obj_t BgL_arg1815z00_6568;
										long BgL_arg1816z00_6569;

										BgL_arg1815z00_6568 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 85 */
											long BgL_arg1817z00_6570;

											BgL_arg1817z00_6570 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6562);
											BgL_arg1816z00_6569 = (BgL_arg1817z00_6570 - OBJECT_TYPE);
										}
										BgL_oclassz00_6567 =
											VECTOR_REF(BgL_arg1815z00_6568, BgL_arg1816z00_6569);
									}
									{	/* SawMill/regset.sch 85 */
										bool_t BgL__ortest_1115z00_6571;

										BgL__ortest_1115z00_6571 =
											(BgL_classz00_6561 == BgL_oclassz00_6567);
										if (BgL__ortest_1115z00_6571)
											{	/* SawMill/regset.sch 85 */
												BgL_res3256z00_6566 = BgL__ortest_1115z00_6571;
											}
										else
											{	/* SawMill/regset.sch 85 */
												long BgL_odepthz00_6572;

												{	/* SawMill/regset.sch 85 */
													obj_t BgL_arg1804z00_6573;

													BgL_arg1804z00_6573 = (BgL_oclassz00_6567);
													BgL_odepthz00_6572 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6573);
												}
												if ((1L < BgL_odepthz00_6572))
													{	/* SawMill/regset.sch 85 */
														obj_t BgL_arg1802z00_6574;

														{	/* SawMill/regset.sch 85 */
															obj_t BgL_arg1803z00_6575;

															BgL_arg1803z00_6575 = (BgL_oclassz00_6567);
															BgL_arg1802z00_6574 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6575,
																1L);
														}
														BgL_res3256z00_6566 =
															(BgL_arg1802z00_6574 == BgL_classz00_6561);
													}
												else
													{	/* SawMill/regset.sch 85 */
														BgL_res3256z00_6566 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3256z00_6566;
							}
					}
				else
					{	/* SawMill/regset.sch 85 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &regset? */
	obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6183,
		obj_t BgL_objz00_6184)
	{
		{	/* SawMill/regset.sch 85 */
			return BBOOL(BGl_regsetzf3zf3zzsaw_bbvzd2mergezd2(BgL_objz00_6184));
		}

	}



/* regset-nil */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawMill/regset.sch 86 */
			{	/* SawMill/regset.sch 86 */
				obj_t BgL_classz00_4511;

				BgL_classz00_4511 = BGl_regsetz00zzsaw_regsetz00;
				{	/* SawMill/regset.sch 86 */
					obj_t BgL__ortest_1117z00_4512;

					BgL__ortest_1117z00_4512 = BGL_CLASS_NIL(BgL_classz00_4511);
					if (CBOOL(BgL__ortest_1117z00_4512))
						{	/* SawMill/regset.sch 86 */
							return ((BgL_regsetz00_bglt) BgL__ortest_1117z00_4512);
						}
					else
						{	/* SawMill/regset.sch 86 */
							return
								((BgL_regsetz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4511));
						}
				}
			}
		}

	}



/* &regset-nil */
	BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6185)
	{
		{	/* SawMill/regset.sch 86 */
			return BGl_regsetzd2nilzd2zzsaw_bbvzd2mergezd2();
		}

	}



/* regset-string */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2mergezd2(BgL_regsetz00_bglt BgL_oz00_58)
	{
		{	/* SawMill/regset.sch 87 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_58))->BgL_stringz00);
		}

	}



/* &regset-string */
	obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6186,
		obj_t BgL_oz00_6187)
	{
		{	/* SawMill/regset.sch 87 */
			return
				BGl_regsetzd2stringzd2zzsaw_bbvzd2mergezd2(
				((BgL_regsetz00_bglt) BgL_oz00_6187));
		}

	}



/* regset-string-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_regsetz00_bglt
		BgL_oz00_59, obj_t BgL_vz00_60)
	{
		{	/* SawMill/regset.sch 88 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_59))->BgL_stringz00) =
				((obj_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &regset-string-set! */
	obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6188, obj_t BgL_oz00_6189, obj_t BgL_vz00_6190)
	{
		{	/* SawMill/regset.sch 88 */
			return
				BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_regsetz00_bglt) BgL_oz00_6189), BgL_vz00_6190);
		}

	}



/* regset-regl */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2mergezd2(BgL_regsetz00_bglt BgL_oz00_61)
	{
		{	/* SawMill/regset.sch 89 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_61))->BgL_reglz00);
		}

	}



/* &regset-regl */
	obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6191,
		obj_t BgL_oz00_6192)
	{
		{	/* SawMill/regset.sch 89 */
			return
				BGl_regsetzd2reglzd2zzsaw_bbvzd2mergezd2(
				((BgL_regsetz00_bglt) BgL_oz00_6192));
		}

	}



/* regset-regv */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2mergezd2(BgL_regsetz00_bglt BgL_oz00_64)
	{
		{	/* SawMill/regset.sch 91 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_64))->BgL_regvz00);
		}

	}



/* &regset-regv */
	obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6193,
		obj_t BgL_oz00_6194)
	{
		{	/* SawMill/regset.sch 91 */
			return
				BGl_regsetzd2regvzd2zzsaw_bbvzd2mergezd2(
				((BgL_regsetz00_bglt) BgL_oz00_6194));
		}

	}



/* regset-msize */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2mergezd2(BgL_regsetz00_bglt BgL_oz00_67)
	{
		{	/* SawMill/regset.sch 93 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_67))->BgL_msiza7eza7);
		}

	}



/* &regset-msize */
	obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6195,
		obj_t BgL_oz00_6196)
	{
		{	/* SawMill/regset.sch 93 */
			return
				BINT(BGl_regsetzd2msiza7ez75zzsaw_bbvzd2mergezd2(
					((BgL_regsetz00_bglt) BgL_oz00_6196)));
		}

	}



/* regset-length */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2mergezd2(BgL_regsetz00_bglt BgL_oz00_70)
	{
		{	/* SawMill/regset.sch 95 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_70))->BgL_lengthz00);
		}

	}



/* &regset-length */
	obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6197,
		obj_t BgL_oz00_6198)
	{
		{	/* SawMill/regset.sch 95 */
			return
				BINT(BGl_regsetzd2lengthzd2zzsaw_bbvzd2mergezd2(
					((BgL_regsetz00_bglt) BgL_oz00_6198)));
		}

	}



/* regset-length-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_regsetz00_bglt
		BgL_oz00_71, int BgL_vz00_72)
	{
		{	/* SawMill/regset.sch 96 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_71))->BgL_lengthz00) =
				((int) BgL_vz00_72), BUNSPEC);
		}

	}



/* &regset-length-set! */
	obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6199, obj_t BgL_oz00_6200, obj_t BgL_vz00_6201)
	{
		{	/* SawMill/regset.sch 96 */
			return
				BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_regsetz00_bglt) BgL_oz00_6200), CINT(BgL_vz00_6201));
		}

	}



/* make-rtl_ins/bbv */
	BGL_EXPORTED_DEF BgL_rtl_insz00_bglt
		BGl_makezd2rtl_inszf2bbvz20zzsaw_bbvzd2mergezd2(obj_t BgL_loc1264z00_73,
		obj_t BgL_z52spill1265z52_74, obj_t BgL_dest1266z00_75,
		BgL_rtl_funz00_bglt BgL_fun1267z00_76, obj_t BgL_args1268z00_77,
		obj_t BgL_def1269z00_78, obj_t BgL_out1270z00_79, obj_t BgL_in1271z00_80,
		BgL_bbvzd2ctxzd2_bglt BgL_ctx1272z00_81, obj_t BgL_z52hash1273z52_82)
	{
		{	/* SawBbv/bbv-types.sch 137 */
			{	/* SawBbv/bbv-types.sch 137 */
				BgL_rtl_insz00_bglt BgL_new1172z00_6576;

				{	/* SawBbv/bbv-types.sch 137 */
					BgL_rtl_insz00_bglt BgL_tmp1170z00_6577;
					BgL_rtl_inszf2bbvzf2_bglt BgL_wide1171z00_6578;

					{
						BgL_rtl_insz00_bglt BgL_auxz00_7084;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_rtl_insz00_bglt BgL_new1169z00_6579;

							BgL_new1169z00_6579 =
								((BgL_rtl_insz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_insz00_bgl))));
							{	/* SawBbv/bbv-types.sch 137 */
								long BgL_arg1968z00_6580;

								BgL_arg1968z00_6580 =
									BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1169z00_6579),
									BgL_arg1968z00_6580);
							}
							{	/* SawBbv/bbv-types.sch 137 */
								BgL_objectz00_bglt BgL_tmpz00_7089;

								BgL_tmpz00_7089 = ((BgL_objectz00_bglt) BgL_new1169z00_6579);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7089, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1169z00_6579);
							BgL_auxz00_7084 = BgL_new1169z00_6579;
						}
						BgL_tmp1170z00_6577 = ((BgL_rtl_insz00_bglt) BgL_auxz00_7084);
					}
					BgL_wide1171z00_6578 =
						((BgL_rtl_inszf2bbvzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_inszf2bbvzf2_bgl))));
					{	/* SawBbv/bbv-types.sch 137 */
						obj_t BgL_auxz00_7097;
						BgL_objectz00_bglt BgL_tmpz00_7095;

						BgL_auxz00_7097 = ((obj_t) BgL_wide1171z00_6578);
						BgL_tmpz00_7095 = ((BgL_objectz00_bglt) BgL_tmp1170z00_6577);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7095, BgL_auxz00_7097);
					}
					((BgL_objectz00_bglt) BgL_tmp1170z00_6577);
					{	/* SawBbv/bbv-types.sch 137 */
						long BgL_arg1967z00_6581;

						BgL_arg1967z00_6581 =
							BGL_CLASS_NUM(BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1170z00_6577), BgL_arg1967z00_6581);
					}
					BgL_new1172z00_6576 = ((BgL_rtl_insz00_bglt) BgL_tmp1170z00_6577);
				}
				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_new1172z00_6576)))->BgL_locz00) =
					((obj_t) BgL_loc1264z00_73), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1172z00_6576)))->BgL_z52spillz52) =
					((obj_t) BgL_z52spill1265z52_74), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1172z00_6576)))->BgL_destz00) =
					((obj_t) BgL_dest1266z00_75), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1172z00_6576)))->BgL_funz00) =
					((BgL_rtl_funz00_bglt) BgL_fun1267z00_76), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1172z00_6576)))->BgL_argsz00) =
					((obj_t) BgL_args1268z00_77), BUNSPEC);
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_7115;

					{
						obj_t BgL_auxz00_7116;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_objectz00_bglt BgL_tmpz00_7117;

							BgL_tmpz00_7117 = ((BgL_objectz00_bglt) BgL_new1172z00_6576);
							BgL_auxz00_7116 = BGL_OBJECT_WIDENING(BgL_tmpz00_7117);
						}
						BgL_auxz00_7115 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_7116);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_7115))->
							BgL_defz00) = ((obj_t) BgL_def1269z00_78), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_7122;

					{
						obj_t BgL_auxz00_7123;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_objectz00_bglt BgL_tmpz00_7124;

							BgL_tmpz00_7124 = ((BgL_objectz00_bglt) BgL_new1172z00_6576);
							BgL_auxz00_7123 = BGL_OBJECT_WIDENING(BgL_tmpz00_7124);
						}
						BgL_auxz00_7122 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_7123);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_7122))->
							BgL_outz00) = ((obj_t) BgL_out1270z00_79), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_7129;

					{
						obj_t BgL_auxz00_7130;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_objectz00_bglt BgL_tmpz00_7131;

							BgL_tmpz00_7131 = ((BgL_objectz00_bglt) BgL_new1172z00_6576);
							BgL_auxz00_7130 = BGL_OBJECT_WIDENING(BgL_tmpz00_7131);
						}
						BgL_auxz00_7129 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_7130);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_7129))->BgL_inz00) =
						((obj_t) BgL_in1271z00_80), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_7136;

					{
						obj_t BgL_auxz00_7137;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_objectz00_bglt BgL_tmpz00_7138;

							BgL_tmpz00_7138 = ((BgL_objectz00_bglt) BgL_new1172z00_6576);
							BgL_auxz00_7137 = BGL_OBJECT_WIDENING(BgL_tmpz00_7138);
						}
						BgL_auxz00_7136 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_7137);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_7136))->
							BgL_ctxz00) =
						((BgL_bbvzd2ctxzd2_bglt) BgL_ctx1272z00_81), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_7143;

					{
						obj_t BgL_auxz00_7144;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_objectz00_bglt BgL_tmpz00_7145;

							BgL_tmpz00_7145 = ((BgL_objectz00_bglt) BgL_new1172z00_6576);
							BgL_auxz00_7144 = BGL_OBJECT_WIDENING(BgL_tmpz00_7145);
						}
						BgL_auxz00_7143 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_7144);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_7143))->
							BgL_z52hashz52) = ((obj_t) BgL_z52hash1273z52_82), BUNSPEC);
				}
				return BgL_new1172z00_6576;
			}
		}

	}



/* &make-rtl_ins/bbv */
	BgL_rtl_insz00_bglt BGl_z62makezd2rtl_inszf2bbvz42zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6202, obj_t BgL_loc1264z00_6203, obj_t BgL_z52spill1265z52_6204,
		obj_t BgL_dest1266z00_6205, obj_t BgL_fun1267z00_6206,
		obj_t BgL_args1268z00_6207, obj_t BgL_def1269z00_6208,
		obj_t BgL_out1270z00_6209, obj_t BgL_in1271z00_6210,
		obj_t BgL_ctx1272z00_6211, obj_t BgL_z52hash1273z52_6212)
	{
		{	/* SawBbv/bbv-types.sch 137 */
			return
				BGl_makezd2rtl_inszf2bbvz20zzsaw_bbvzd2mergezd2(BgL_loc1264z00_6203,
				BgL_z52spill1265z52_6204, BgL_dest1266z00_6205,
				((BgL_rtl_funz00_bglt) BgL_fun1267z00_6206), BgL_args1268z00_6207,
				BgL_def1269z00_6208, BgL_out1270z00_6209, BgL_in1271z00_6210,
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctx1272z00_6211), BgL_z52hash1273z52_6212);
		}

	}



/* rtl_ins/bbv? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_inszf2bbvzf3z01zzsaw_bbvzd2mergezd2(obj_t
		BgL_objz00_83)
	{
		{	/* SawBbv/bbv-types.sch 138 */
			{	/* SawBbv/bbv-types.sch 138 */
				obj_t BgL_classz00_6582;

				BgL_classz00_6582 = BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_83))
					{	/* SawBbv/bbv-types.sch 138 */
						BgL_objectz00_bglt BgL_arg1807z00_6583;

						BgL_arg1807z00_6583 = (BgL_objectz00_bglt) (BgL_objz00_83);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 138 */
								long BgL_idxz00_6584;

								BgL_idxz00_6584 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6583);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6584 + 2L)) == BgL_classz00_6582);
							}
						else
							{	/* SawBbv/bbv-types.sch 138 */
								bool_t BgL_res3257z00_6587;

								{	/* SawBbv/bbv-types.sch 138 */
									obj_t BgL_oclassz00_6588;

									{	/* SawBbv/bbv-types.sch 138 */
										obj_t BgL_arg1815z00_6589;
										long BgL_arg1816z00_6590;

										BgL_arg1815z00_6589 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 138 */
											long BgL_arg1817z00_6591;

											BgL_arg1817z00_6591 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6583);
											BgL_arg1816z00_6590 = (BgL_arg1817z00_6591 - OBJECT_TYPE);
										}
										BgL_oclassz00_6588 =
											VECTOR_REF(BgL_arg1815z00_6589, BgL_arg1816z00_6590);
									}
									{	/* SawBbv/bbv-types.sch 138 */
										bool_t BgL__ortest_1115z00_6592;

										BgL__ortest_1115z00_6592 =
											(BgL_classz00_6582 == BgL_oclassz00_6588);
										if (BgL__ortest_1115z00_6592)
											{	/* SawBbv/bbv-types.sch 138 */
												BgL_res3257z00_6587 = BgL__ortest_1115z00_6592;
											}
										else
											{	/* SawBbv/bbv-types.sch 138 */
												long BgL_odepthz00_6593;

												{	/* SawBbv/bbv-types.sch 138 */
													obj_t BgL_arg1804z00_6594;

													BgL_arg1804z00_6594 = (BgL_oclassz00_6588);
													BgL_odepthz00_6593 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6594);
												}
												if ((2L < BgL_odepthz00_6593))
													{	/* SawBbv/bbv-types.sch 138 */
														obj_t BgL_arg1802z00_6595;

														{	/* SawBbv/bbv-types.sch 138 */
															obj_t BgL_arg1803z00_6596;

															BgL_arg1803z00_6596 = (BgL_oclassz00_6588);
															BgL_arg1802z00_6595 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6596,
																2L);
														}
														BgL_res3257z00_6587 =
															(BgL_arg1802z00_6595 == BgL_classz00_6582);
													}
												else
													{	/* SawBbv/bbv-types.sch 138 */
														BgL_res3257z00_6587 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3257z00_6587;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 138 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_ins/bbv? */
	obj_t BGl_z62rtl_inszf2bbvzf3z63zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6213,
		obj_t BgL_objz00_6214)
	{
		{	/* SawBbv/bbv-types.sch 138 */
			return
				BBOOL(BGl_rtl_inszf2bbvzf3z01zzsaw_bbvzd2mergezd2(BgL_objz00_6214));
		}

	}



/* rtl_ins/bbv-nil */
	BGL_EXPORTED_DEF BgL_rtl_insz00_bglt
		BGl_rtl_inszf2bbvzd2nilz20zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawBbv/bbv-types.sch 139 */
			{	/* SawBbv/bbv-types.sch 139 */
				obj_t BgL_classz00_4566;

				BgL_classz00_4566 = BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 139 */
					obj_t BgL__ortest_1117z00_4567;

					BgL__ortest_1117z00_4567 = BGL_CLASS_NIL(BgL_classz00_4566);
					if (CBOOL(BgL__ortest_1117z00_4567))
						{	/* SawBbv/bbv-types.sch 139 */
							return ((BgL_rtl_insz00_bglt) BgL__ortest_1117z00_4567);
						}
					else
						{	/* SawBbv/bbv-types.sch 139 */
							return
								((BgL_rtl_insz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4566));
						}
				}
			}
		}

	}



/* &rtl_ins/bbv-nil */
	BgL_rtl_insz00_bglt BGl_z62rtl_inszf2bbvzd2nilz42zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6215)
	{
		{	/* SawBbv/bbv-types.sch 139 */
			return BGl_rtl_inszf2bbvzd2nilz20zzsaw_bbvzd2mergezd2();
		}

	}



/* rtl_ins/bbv-%hash */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2z52hashz72zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_84)
	{
		{	/* SawBbv/bbv-types.sch 140 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_7184;

				{
					obj_t BgL_auxz00_7185;

					{	/* SawBbv/bbv-types.sch 140 */
						BgL_objectz00_bglt BgL_tmpz00_7186;

						BgL_tmpz00_7186 = ((BgL_objectz00_bglt) BgL_oz00_84);
						BgL_auxz00_7185 = BGL_OBJECT_WIDENING(BgL_tmpz00_7186);
					}
					BgL_auxz00_7184 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_7185);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_7184))->
					BgL_z52hashz52);
			}
		}

	}



/* &rtl_ins/bbv-%hash */
	obj_t BGl_z62rtl_inszf2bbvzd2z52hashz10zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6216, obj_t BgL_oz00_6217)
	{
		{	/* SawBbv/bbv-types.sch 140 */
			return
				BGl_rtl_inszf2bbvzd2z52hashz72zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6217));
		}

	}



/* rtl_ins/bbv-%hash-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2z52hashzd2setz12zb2zzsaw_bbvzd2mergezd2
		(BgL_rtl_insz00_bglt BgL_oz00_85, obj_t BgL_vz00_86)
	{
		{	/* SawBbv/bbv-types.sch 141 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_7193;

				{
					obj_t BgL_auxz00_7194;

					{	/* SawBbv/bbv-types.sch 141 */
						BgL_objectz00_bglt BgL_tmpz00_7195;

						BgL_tmpz00_7195 = ((BgL_objectz00_bglt) BgL_oz00_85);
						BgL_auxz00_7194 = BGL_OBJECT_WIDENING(BgL_tmpz00_7195);
					}
					BgL_auxz00_7193 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_7194);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_7193))->
						BgL_z52hashz52) = ((obj_t) BgL_vz00_86), BUNSPEC);
			}
		}

	}



/* &rtl_ins/bbv-%hash-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2z52hashzd2setz12zd0zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6218, obj_t BgL_oz00_6219, obj_t BgL_vz00_6220)
	{
		{	/* SawBbv/bbv-types.sch 141 */
			return
				BGl_rtl_inszf2bbvzd2z52hashzd2setz12zb2zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6219), BgL_vz00_6220);
		}

	}



/* rtl_ins/bbv-ctx */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_rtl_inszf2bbvzd2ctxz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_87)
	{
		{	/* SawBbv/bbv-types.sch 142 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_7202;

				{
					obj_t BgL_auxz00_7203;

					{	/* SawBbv/bbv-types.sch 142 */
						BgL_objectz00_bglt BgL_tmpz00_7204;

						BgL_tmpz00_7204 = ((BgL_objectz00_bglt) BgL_oz00_87);
						BgL_auxz00_7203 = BGL_OBJECT_WIDENING(BgL_tmpz00_7204);
					}
					BgL_auxz00_7202 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_7203);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_7202))->BgL_ctxz00);
			}
		}

	}



/* &rtl_ins/bbv-ctx */
	BgL_bbvzd2ctxzd2_bglt BGl_z62rtl_inszf2bbvzd2ctxz42zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6221, obj_t BgL_oz00_6222)
	{
		{	/* SawBbv/bbv-types.sch 142 */
			return
				BGl_rtl_inszf2bbvzd2ctxz20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6222));
		}

	}



/* rtl_ins/bbv-ctx-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2ctxzd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_88, BgL_bbvzd2ctxzd2_bglt BgL_vz00_89)
	{
		{	/* SawBbv/bbv-types.sch 143 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_7211;

				{
					obj_t BgL_auxz00_7212;

					{	/* SawBbv/bbv-types.sch 143 */
						BgL_objectz00_bglt BgL_tmpz00_7213;

						BgL_tmpz00_7213 = ((BgL_objectz00_bglt) BgL_oz00_88);
						BgL_auxz00_7212 = BGL_OBJECT_WIDENING(BgL_tmpz00_7213);
					}
					BgL_auxz00_7211 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_7212);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_7211))->
						BgL_ctxz00) = ((BgL_bbvzd2ctxzd2_bglt) BgL_vz00_89), BUNSPEC);
			}
		}

	}



/* &rtl_ins/bbv-ctx-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2ctxzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6223, obj_t BgL_oz00_6224, obj_t BgL_vz00_6225)
	{
		{	/* SawBbv/bbv-types.sch 143 */
			return
				BGl_rtl_inszf2bbvzd2ctxzd2setz12ze0zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6224),
				((BgL_bbvzd2ctxzd2_bglt) BgL_vz00_6225));
		}

	}



/* rtl_ins/bbv-in */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2inz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_90)
	{
		{	/* SawBbv/bbv-types.sch 144 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_7221;

				{
					obj_t BgL_auxz00_7222;

					{	/* SawBbv/bbv-types.sch 144 */
						BgL_objectz00_bglt BgL_tmpz00_7223;

						BgL_tmpz00_7223 = ((BgL_objectz00_bglt) BgL_oz00_90);
						BgL_auxz00_7222 = BGL_OBJECT_WIDENING(BgL_tmpz00_7223);
					}
					BgL_auxz00_7221 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_7222);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_7221))->BgL_inz00);
			}
		}

	}



/* &rtl_ins/bbv-in */
	obj_t BGl_z62rtl_inszf2bbvzd2inz42zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6226,
		obj_t BgL_oz00_6227)
	{
		{	/* SawBbv/bbv-types.sch 144 */
			return
				BGl_rtl_inszf2bbvzd2inz20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6227));
		}

	}



/* rtl_ins/bbv-in-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2inzd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_91, obj_t BgL_vz00_92)
	{
		{	/* SawBbv/bbv-types.sch 145 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_7230;

				{
					obj_t BgL_auxz00_7231;

					{	/* SawBbv/bbv-types.sch 145 */
						BgL_objectz00_bglt BgL_tmpz00_7232;

						BgL_tmpz00_7232 = ((BgL_objectz00_bglt) BgL_oz00_91);
						BgL_auxz00_7231 = BGL_OBJECT_WIDENING(BgL_tmpz00_7232);
					}
					BgL_auxz00_7230 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_7231);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_7230))->BgL_inz00) =
					((obj_t) BgL_vz00_92), BUNSPEC);
			}
		}

	}



/* &rtl_ins/bbv-in-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2inzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6228, obj_t BgL_oz00_6229, obj_t BgL_vz00_6230)
	{
		{	/* SawBbv/bbv-types.sch 145 */
			return
				BGl_rtl_inszf2bbvzd2inzd2setz12ze0zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6229), BgL_vz00_6230);
		}

	}



/* rtl_ins/bbv-out */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2outz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_93)
	{
		{	/* SawBbv/bbv-types.sch 146 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_7239;

				{
					obj_t BgL_auxz00_7240;

					{	/* SawBbv/bbv-types.sch 146 */
						BgL_objectz00_bglt BgL_tmpz00_7241;

						BgL_tmpz00_7241 = ((BgL_objectz00_bglt) BgL_oz00_93);
						BgL_auxz00_7240 = BGL_OBJECT_WIDENING(BgL_tmpz00_7241);
					}
					BgL_auxz00_7239 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_7240);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_7239))->BgL_outz00);
			}
		}

	}



/* &rtl_ins/bbv-out */
	obj_t BGl_z62rtl_inszf2bbvzd2outz42zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6231,
		obj_t BgL_oz00_6232)
	{
		{	/* SawBbv/bbv-types.sch 146 */
			return
				BGl_rtl_inszf2bbvzd2outz20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6232));
		}

	}



/* rtl_ins/bbv-out-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2outzd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_94, obj_t BgL_vz00_95)
	{
		{	/* SawBbv/bbv-types.sch 147 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_7248;

				{
					obj_t BgL_auxz00_7249;

					{	/* SawBbv/bbv-types.sch 147 */
						BgL_objectz00_bglt BgL_tmpz00_7250;

						BgL_tmpz00_7250 = ((BgL_objectz00_bglt) BgL_oz00_94);
						BgL_auxz00_7249 = BGL_OBJECT_WIDENING(BgL_tmpz00_7250);
					}
					BgL_auxz00_7248 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_7249);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_7248))->
						BgL_outz00) = ((obj_t) BgL_vz00_95), BUNSPEC);
			}
		}

	}



/* &rtl_ins/bbv-out-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2outzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6233, obj_t BgL_oz00_6234, obj_t BgL_vz00_6235)
	{
		{	/* SawBbv/bbv-types.sch 147 */
			return
				BGl_rtl_inszf2bbvzd2outzd2setz12ze0zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6234), BgL_vz00_6235);
		}

	}



/* rtl_ins/bbv-def */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2defz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_96)
	{
		{	/* SawBbv/bbv-types.sch 148 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_7257;

				{
					obj_t BgL_auxz00_7258;

					{	/* SawBbv/bbv-types.sch 148 */
						BgL_objectz00_bglt BgL_tmpz00_7259;

						BgL_tmpz00_7259 = ((BgL_objectz00_bglt) BgL_oz00_96);
						BgL_auxz00_7258 = BGL_OBJECT_WIDENING(BgL_tmpz00_7259);
					}
					BgL_auxz00_7257 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_7258);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_7257))->BgL_defz00);
			}
		}

	}



/* &rtl_ins/bbv-def */
	obj_t BGl_z62rtl_inszf2bbvzd2defz42zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6236,
		obj_t BgL_oz00_6237)
	{
		{	/* SawBbv/bbv-types.sch 148 */
			return
				BGl_rtl_inszf2bbvzd2defz20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6237));
		}

	}



/* rtl_ins/bbv-def-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2defzd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_97, obj_t BgL_vz00_98)
	{
		{	/* SawBbv/bbv-types.sch 149 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_7266;

				{
					obj_t BgL_auxz00_7267;

					{	/* SawBbv/bbv-types.sch 149 */
						BgL_objectz00_bglt BgL_tmpz00_7268;

						BgL_tmpz00_7268 = ((BgL_objectz00_bglt) BgL_oz00_97);
						BgL_auxz00_7267 = BGL_OBJECT_WIDENING(BgL_tmpz00_7268);
					}
					BgL_auxz00_7266 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_7267);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_7266))->
						BgL_defz00) = ((obj_t) BgL_vz00_98), BUNSPEC);
			}
		}

	}



/* &rtl_ins/bbv-def-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2defzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6238, obj_t BgL_oz00_6239, obj_t BgL_vz00_6240)
	{
		{	/* SawBbv/bbv-types.sch 149 */
			return
				BGl_rtl_inszf2bbvzd2defzd2setz12ze0zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6239), BgL_vz00_6240);
		}

	}



/* rtl_ins/bbv-args */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2argsz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_99)
	{
		{	/* SawBbv/bbv-types.sch 150 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_99)))->BgL_argsz00);
		}

	}



/* &rtl_ins/bbv-args */
	obj_t BGl_z62rtl_inszf2bbvzd2argsz42zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6241, obj_t BgL_oz00_6242)
	{
		{	/* SawBbv/bbv-types.sch 150 */
			return
				BGl_rtl_inszf2bbvzd2argsz20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6242));
		}

	}



/* rtl_ins/bbv-args-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2argszd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_100, obj_t BgL_vz00_101)
	{
		{	/* SawBbv/bbv-types.sch 151 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_100)))->BgL_argsz00) =
				((obj_t) BgL_vz00_101), BUNSPEC);
		}

	}



/* &rtl_ins/bbv-args-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2argszd2setz12z82zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6243, obj_t BgL_oz00_6244, obj_t BgL_vz00_6245)
	{
		{	/* SawBbv/bbv-types.sch 151 */
			return
				BGl_rtl_inszf2bbvzd2argszd2setz12ze0zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6244), BgL_vz00_6245);
		}

	}



/* rtl_ins/bbv-fun */
	BGL_EXPORTED_DEF BgL_rtl_funz00_bglt
		BGl_rtl_inszf2bbvzd2funz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_102)
	{
		{	/* SawBbv/bbv-types.sch 152 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_102)))->BgL_funz00);
		}

	}



/* &rtl_ins/bbv-fun */
	BgL_rtl_funz00_bglt BGl_z62rtl_inszf2bbvzd2funz42zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6246, obj_t BgL_oz00_6247)
	{
		{	/* SawBbv/bbv-types.sch 152 */
			return
				BGl_rtl_inszf2bbvzd2funz20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6247));
		}

	}



/* rtl_ins/bbv-fun-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2funzd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_103, BgL_rtl_funz00_bglt BgL_vz00_104)
	{
		{	/* SawBbv/bbv-types.sch 153 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_103)))->BgL_funz00) =
				((BgL_rtl_funz00_bglt) BgL_vz00_104), BUNSPEC);
		}

	}



/* &rtl_ins/bbv-fun-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2funzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6248, obj_t BgL_oz00_6249, obj_t BgL_vz00_6250)
	{
		{	/* SawBbv/bbv-types.sch 153 */
			return
				BGl_rtl_inszf2bbvzd2funzd2setz12ze0zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6249),
				((BgL_rtl_funz00_bglt) BgL_vz00_6250));
		}

	}



/* rtl_ins/bbv-dest */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2destz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_105)
	{
		{	/* SawBbv/bbv-types.sch 154 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_105)))->BgL_destz00);
		}

	}



/* &rtl_ins/bbv-dest */
	obj_t BGl_z62rtl_inszf2bbvzd2destz42zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6251, obj_t BgL_oz00_6252)
	{
		{	/* SawBbv/bbv-types.sch 154 */
			return
				BGl_rtl_inszf2bbvzd2destz20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6252));
		}

	}



/* rtl_ins/bbv-dest-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2destzd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_106, obj_t BgL_vz00_107)
	{
		{	/* SawBbv/bbv-types.sch 155 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_106)))->BgL_destz00) =
				((obj_t) BgL_vz00_107), BUNSPEC);
		}

	}



/* &rtl_ins/bbv-dest-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2destzd2setz12z82zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6253, obj_t BgL_oz00_6254, obj_t BgL_vz00_6255)
	{
		{	/* SawBbv/bbv-types.sch 155 */
			return
				BGl_rtl_inszf2bbvzd2destzd2setz12ze0zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6254), BgL_vz00_6255);
		}

	}



/* rtl_ins/bbv-%spill */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2z52spillz72zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_108)
	{
		{	/* SawBbv/bbv-types.sch 156 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_108)))->BgL_z52spillz52);
		}

	}



/* &rtl_ins/bbv-%spill */
	obj_t BGl_z62rtl_inszf2bbvzd2z52spillz10zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6256, obj_t BgL_oz00_6257)
	{
		{	/* SawBbv/bbv-types.sch 156 */
			return
				BGl_rtl_inszf2bbvzd2z52spillz72zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6257));
		}

	}



/* rtl_ins/bbv-%spill-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2z52spillzd2setz12zb2zzsaw_bbvzd2mergezd2
		(BgL_rtl_insz00_bglt BgL_oz00_109, obj_t BgL_vz00_110)
	{
		{	/* SawBbv/bbv-types.sch 157 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_109)))->BgL_z52spillz52) =
				((obj_t) BgL_vz00_110), BUNSPEC);
		}

	}



/* &rtl_ins/bbv-%spill-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2z52spillzd2setz12zd0zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6258, obj_t BgL_oz00_6259, obj_t BgL_vz00_6260)
	{
		{	/* SawBbv/bbv-types.sch 157 */
			return
				BGl_rtl_inszf2bbvzd2z52spillzd2setz12zb2zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6259), BgL_vz00_6260);
		}

	}



/* rtl_ins/bbv-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2locz20zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_111)
	{
		{	/* SawBbv/bbv-types.sch 158 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_111)))->BgL_locz00);
		}

	}



/* &rtl_ins/bbv-loc */
	obj_t BGl_z62rtl_inszf2bbvzd2locz42zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6261,
		obj_t BgL_oz00_6262)
	{
		{	/* SawBbv/bbv-types.sch 158 */
			return
				BGl_rtl_inszf2bbvzd2locz20zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6262));
		}

	}



/* rtl_ins/bbv-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2loczd2setz12ze0zzsaw_bbvzd2mergezd2(BgL_rtl_insz00_bglt
		BgL_oz00_112, obj_t BgL_vz00_113)
	{
		{	/* SawBbv/bbv-types.sch 159 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_112)))->BgL_locz00) =
				((obj_t) BgL_vz00_113), BUNSPEC);
		}

	}



/* &rtl_ins/bbv-loc-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2loczd2setz12z82zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6263, obj_t BgL_oz00_6264, obj_t BgL_vz00_6265)
	{
		{	/* SawBbv/bbv-types.sch 159 */
			return
				BGl_rtl_inszf2bbvzd2loczd2setz12ze0zzsaw_bbvzd2mergezd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_6264), BgL_vz00_6265);
		}

	}



/* make-blockV */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_makezd2blockVzd2zzsaw_bbvzd2mergezd2(int BgL_label1255z00_114,
		obj_t BgL_preds1256z00_115, obj_t BgL_succs1257z00_116,
		obj_t BgL_first1258z00_117, obj_t BgL_versions1259z00_118,
		obj_t BgL_generic1260z00_119, long BgL_z52mark1261z52_120,
		obj_t BgL_merge1262z00_121)
	{
		{	/* SawBbv/bbv-types.sch 162 */
			{	/* SawBbv/bbv-types.sch 162 */
				BgL_blockz00_bglt BgL_new1176z00_6597;

				{	/* SawBbv/bbv-types.sch 162 */
					BgL_blockz00_bglt BgL_tmp1174z00_6598;
					BgL_blockvz00_bglt BgL_wide1175z00_6599;

					{
						BgL_blockz00_bglt BgL_auxz00_7316;

						{	/* SawBbv/bbv-types.sch 162 */
							BgL_blockz00_bglt BgL_new1173z00_6600;

							BgL_new1173z00_6600 =
								((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_blockz00_bgl))));
							{	/* SawBbv/bbv-types.sch 162 */
								long BgL_arg1970z00_6601;

								BgL_arg1970z00_6601 = BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1173z00_6600),
									BgL_arg1970z00_6601);
							}
							{	/* SawBbv/bbv-types.sch 162 */
								BgL_objectz00_bglt BgL_tmpz00_7321;

								BgL_tmpz00_7321 = ((BgL_objectz00_bglt) BgL_new1173z00_6600);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7321, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1173z00_6600);
							BgL_auxz00_7316 = BgL_new1173z00_6600;
						}
						BgL_tmp1174z00_6598 = ((BgL_blockz00_bglt) BgL_auxz00_7316);
					}
					BgL_wide1175z00_6599 =
						((BgL_blockvz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_blockvz00_bgl))));
					{	/* SawBbv/bbv-types.sch 162 */
						obj_t BgL_auxz00_7329;
						BgL_objectz00_bglt BgL_tmpz00_7327;

						BgL_auxz00_7329 = ((obj_t) BgL_wide1175z00_6599);
						BgL_tmpz00_7327 = ((BgL_objectz00_bglt) BgL_tmp1174z00_6598);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7327, BgL_auxz00_7329);
					}
					((BgL_objectz00_bglt) BgL_tmp1174z00_6598);
					{	/* SawBbv/bbv-types.sch 162 */
						long BgL_arg1969z00_6602;

						BgL_arg1969z00_6602 =
							BGL_CLASS_NUM(BGl_blockVz00zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1174z00_6598), BgL_arg1969z00_6602);
					}
					BgL_new1176z00_6597 = ((BgL_blockz00_bglt) BgL_tmp1174z00_6598);
				}
				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_new1176z00_6597)))->BgL_labelz00) =
					((int) BgL_label1255z00_114), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1176z00_6597)))->BgL_predsz00) =
					((obj_t) BgL_preds1256z00_115), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1176z00_6597)))->BgL_succsz00) =
					((obj_t) BgL_succs1257z00_116), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1176z00_6597)))->BgL_firstz00) =
					((obj_t) BgL_first1258z00_117), BUNSPEC);
				{
					BgL_blockvz00_bglt BgL_auxz00_7345;

					{
						obj_t BgL_auxz00_7346;

						{	/* SawBbv/bbv-types.sch 162 */
							BgL_objectz00_bglt BgL_tmpz00_7347;

							BgL_tmpz00_7347 = ((BgL_objectz00_bglt) BgL_new1176z00_6597);
							BgL_auxz00_7346 = BGL_OBJECT_WIDENING(BgL_tmpz00_7347);
						}
						BgL_auxz00_7345 = ((BgL_blockvz00_bglt) BgL_auxz00_7346);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_7345))->BgL_versionsz00) =
						((obj_t) BgL_versions1259z00_118), BUNSPEC);
				}
				{
					BgL_blockvz00_bglt BgL_auxz00_7352;

					{
						obj_t BgL_auxz00_7353;

						{	/* SawBbv/bbv-types.sch 162 */
							BgL_objectz00_bglt BgL_tmpz00_7354;

							BgL_tmpz00_7354 = ((BgL_objectz00_bglt) BgL_new1176z00_6597);
							BgL_auxz00_7353 = BGL_OBJECT_WIDENING(BgL_tmpz00_7354);
						}
						BgL_auxz00_7352 = ((BgL_blockvz00_bglt) BgL_auxz00_7353);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_7352))->BgL_genericz00) =
						((obj_t) BgL_generic1260z00_119), BUNSPEC);
				}
				{
					BgL_blockvz00_bglt BgL_auxz00_7359;

					{
						obj_t BgL_auxz00_7360;

						{	/* SawBbv/bbv-types.sch 162 */
							BgL_objectz00_bglt BgL_tmpz00_7361;

							BgL_tmpz00_7361 = ((BgL_objectz00_bglt) BgL_new1176z00_6597);
							BgL_auxz00_7360 = BGL_OBJECT_WIDENING(BgL_tmpz00_7361);
						}
						BgL_auxz00_7359 = ((BgL_blockvz00_bglt) BgL_auxz00_7360);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_7359))->BgL_z52markz52) =
						((long) BgL_z52mark1261z52_120), BUNSPEC);
				}
				{
					BgL_blockvz00_bglt BgL_auxz00_7366;

					{
						obj_t BgL_auxz00_7367;

						{	/* SawBbv/bbv-types.sch 162 */
							BgL_objectz00_bglt BgL_tmpz00_7368;

							BgL_tmpz00_7368 = ((BgL_objectz00_bglt) BgL_new1176z00_6597);
							BgL_auxz00_7367 = BGL_OBJECT_WIDENING(BgL_tmpz00_7368);
						}
						BgL_auxz00_7366 = ((BgL_blockvz00_bglt) BgL_auxz00_7367);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_7366))->BgL_mergez00) =
						((obj_t) BgL_merge1262z00_121), BUNSPEC);
				}
				return BgL_new1176z00_6597;
			}
		}

	}



/* &make-blockV */
	BgL_blockz00_bglt BGl_z62makezd2blockVzb0zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6266, obj_t BgL_label1255z00_6267, obj_t BgL_preds1256z00_6268,
		obj_t BgL_succs1257z00_6269, obj_t BgL_first1258z00_6270,
		obj_t BgL_versions1259z00_6271, obj_t BgL_generic1260z00_6272,
		obj_t BgL_z52mark1261z52_6273, obj_t BgL_merge1262z00_6274)
	{
		{	/* SawBbv/bbv-types.sch 162 */
			return
				BGl_makezd2blockVzd2zzsaw_bbvzd2mergezd2(CINT(BgL_label1255z00_6267),
				BgL_preds1256z00_6268, BgL_succs1257z00_6269, BgL_first1258z00_6270,
				BgL_versions1259z00_6271, BgL_generic1260z00_6272,
				(long) CINT(BgL_z52mark1261z52_6273), BgL_merge1262z00_6274);
		}

	}



/* blockV? */
	BGL_EXPORTED_DEF bool_t BGl_blockVzf3zf3zzsaw_bbvzd2mergezd2(obj_t
		BgL_objz00_122)
	{
		{	/* SawBbv/bbv-types.sch 163 */
			{	/* SawBbv/bbv-types.sch 163 */
				obj_t BgL_classz00_6603;

				BgL_classz00_6603 = BGl_blockVz00zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_122))
					{	/* SawBbv/bbv-types.sch 163 */
						BgL_objectz00_bglt BgL_arg1807z00_6604;

						BgL_arg1807z00_6604 = (BgL_objectz00_bglt) (BgL_objz00_122);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 163 */
								long BgL_idxz00_6605;

								BgL_idxz00_6605 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6604);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6605 + 2L)) == BgL_classz00_6603);
							}
						else
							{	/* SawBbv/bbv-types.sch 163 */
								bool_t BgL_res3258z00_6608;

								{	/* SawBbv/bbv-types.sch 163 */
									obj_t BgL_oclassz00_6609;

									{	/* SawBbv/bbv-types.sch 163 */
										obj_t BgL_arg1815z00_6610;
										long BgL_arg1816z00_6611;

										BgL_arg1815z00_6610 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 163 */
											long BgL_arg1817z00_6612;

											BgL_arg1817z00_6612 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6604);
											BgL_arg1816z00_6611 = (BgL_arg1817z00_6612 - OBJECT_TYPE);
										}
										BgL_oclassz00_6609 =
											VECTOR_REF(BgL_arg1815z00_6610, BgL_arg1816z00_6611);
									}
									{	/* SawBbv/bbv-types.sch 163 */
										bool_t BgL__ortest_1115z00_6613;

										BgL__ortest_1115z00_6613 =
											(BgL_classz00_6603 == BgL_oclassz00_6609);
										if (BgL__ortest_1115z00_6613)
											{	/* SawBbv/bbv-types.sch 163 */
												BgL_res3258z00_6608 = BgL__ortest_1115z00_6613;
											}
										else
											{	/* SawBbv/bbv-types.sch 163 */
												long BgL_odepthz00_6614;

												{	/* SawBbv/bbv-types.sch 163 */
													obj_t BgL_arg1804z00_6615;

													BgL_arg1804z00_6615 = (BgL_oclassz00_6609);
													BgL_odepthz00_6614 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6615);
												}
												if ((2L < BgL_odepthz00_6614))
													{	/* SawBbv/bbv-types.sch 163 */
														obj_t BgL_arg1802z00_6616;

														{	/* SawBbv/bbv-types.sch 163 */
															obj_t BgL_arg1803z00_6617;

															BgL_arg1803z00_6617 = (BgL_oclassz00_6609);
															BgL_arg1802z00_6616 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6617,
																2L);
														}
														BgL_res3258z00_6608 =
															(BgL_arg1802z00_6616 == BgL_classz00_6603);
													}
												else
													{	/* SawBbv/bbv-types.sch 163 */
														BgL_res3258z00_6608 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3258z00_6608;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 163 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &blockV? */
	obj_t BGl_z62blockVzf3z91zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6275,
		obj_t BgL_objz00_6276)
	{
		{	/* SawBbv/bbv-types.sch 163 */
			return BBOOL(BGl_blockVzf3zf3zzsaw_bbvzd2mergezd2(BgL_objz00_6276));
		}

	}



/* blockV-nil */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_blockVzd2nilzd2zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawBbv/bbv-types.sch 164 */
			{	/* SawBbv/bbv-types.sch 164 */
				obj_t BgL_classz00_4630;

				BgL_classz00_4630 = BGl_blockVz00zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 164 */
					obj_t BgL__ortest_1117z00_4631;

					BgL__ortest_1117z00_4631 = BGL_CLASS_NIL(BgL_classz00_4630);
					if (CBOOL(BgL__ortest_1117z00_4631))
						{	/* SawBbv/bbv-types.sch 164 */
							return ((BgL_blockz00_bglt) BgL__ortest_1117z00_4631);
						}
					else
						{	/* SawBbv/bbv-types.sch 164 */
							return
								((BgL_blockz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4630));
						}
				}
			}
		}

	}



/* &blockV-nil */
	BgL_blockz00_bglt BGl_z62blockVzd2nilzb0zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6277)
	{
		{	/* SawBbv/bbv-types.sch 164 */
			return BGl_blockVzd2nilzd2zzsaw_bbvzd2mergezd2();
		}

	}



/* blockV-merge */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2mergezd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_123)
	{
		{	/* SawBbv/bbv-types.sch 165 */
			{
				BgL_blockvz00_bglt BgL_auxz00_7407;

				{
					obj_t BgL_auxz00_7408;

					{	/* SawBbv/bbv-types.sch 165 */
						BgL_objectz00_bglt BgL_tmpz00_7409;

						BgL_tmpz00_7409 = ((BgL_objectz00_bglt) BgL_oz00_123);
						BgL_auxz00_7408 = BGL_OBJECT_WIDENING(BgL_tmpz00_7409);
					}
					BgL_auxz00_7407 = ((BgL_blockvz00_bglt) BgL_auxz00_7408);
				}
				return (((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_7407))->BgL_mergez00);
			}
		}

	}



/* &blockV-merge */
	obj_t BGl_z62blockVzd2mergezb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6278,
		obj_t BgL_oz00_6279)
	{
		{	/* SawBbv/bbv-types.sch 165 */
			return
				BGl_blockVzd2mergezd2zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6279));
		}

	}



/* blockV-merge-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2mergezd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_124, obj_t BgL_vz00_125)
	{
		{	/* SawBbv/bbv-types.sch 166 */
			{
				BgL_blockvz00_bglt BgL_auxz00_7416;

				{
					obj_t BgL_auxz00_7417;

					{	/* SawBbv/bbv-types.sch 166 */
						BgL_objectz00_bglt BgL_tmpz00_7418;

						BgL_tmpz00_7418 = ((BgL_objectz00_bglt) BgL_oz00_124);
						BgL_auxz00_7417 = BGL_OBJECT_WIDENING(BgL_tmpz00_7418);
					}
					BgL_auxz00_7416 = ((BgL_blockvz00_bglt) BgL_auxz00_7417);
				}
				return
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_7416))->BgL_mergez00) =
					((obj_t) BgL_vz00_125), BUNSPEC);
			}
		}

	}



/* &blockV-merge-set! */
	obj_t BGl_z62blockVzd2mergezd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6280, obj_t BgL_oz00_6281, obj_t BgL_vz00_6282)
	{
		{	/* SawBbv/bbv-types.sch 166 */
			return
				BGl_blockVzd2mergezd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6281), BgL_vz00_6282);
		}

	}



/* blockV-%mark */
	BGL_EXPORTED_DEF long
		BGl_blockVzd2z52markz80zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_126)
	{
		{	/* SawBbv/bbv-types.sch 167 */
			{
				BgL_blockvz00_bglt BgL_auxz00_7425;

				{
					obj_t BgL_auxz00_7426;

					{	/* SawBbv/bbv-types.sch 167 */
						BgL_objectz00_bglt BgL_tmpz00_7427;

						BgL_tmpz00_7427 = ((BgL_objectz00_bglt) BgL_oz00_126);
						BgL_auxz00_7426 = BGL_OBJECT_WIDENING(BgL_tmpz00_7427);
					}
					BgL_auxz00_7425 = ((BgL_blockvz00_bglt) BgL_auxz00_7426);
				}
				return
					(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_7425))->BgL_z52markz52);
			}
		}

	}



/* &blockV-%mark */
	obj_t BGl_z62blockVzd2z52markze2zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6283,
		obj_t BgL_oz00_6284)
	{
		{	/* SawBbv/bbv-types.sch 167 */
			return
				BINT(BGl_blockVzd2z52markz80zzsaw_bbvzd2mergezd2(
					((BgL_blockz00_bglt) BgL_oz00_6284)));
		}

	}



/* blockV-%mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2z52markzd2setz12z40zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_127, long BgL_vz00_128)
	{
		{	/* SawBbv/bbv-types.sch 168 */
			{
				BgL_blockvz00_bglt BgL_auxz00_7435;

				{
					obj_t BgL_auxz00_7436;

					{	/* SawBbv/bbv-types.sch 168 */
						BgL_objectz00_bglt BgL_tmpz00_7437;

						BgL_tmpz00_7437 = ((BgL_objectz00_bglt) BgL_oz00_127);
						BgL_auxz00_7436 = BGL_OBJECT_WIDENING(BgL_tmpz00_7437);
					}
					BgL_auxz00_7435 = ((BgL_blockvz00_bglt) BgL_auxz00_7436);
				}
				return
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_7435))->BgL_z52markz52) =
					((long) BgL_vz00_128), BUNSPEC);
		}}

	}



/* &blockV-%mark-set! */
	obj_t BGl_z62blockVzd2z52markzd2setz12z22zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6285, obj_t BgL_oz00_6286, obj_t BgL_vz00_6287)
	{
		{	/* SawBbv/bbv-types.sch 168 */
			return
				BGl_blockVzd2z52markzd2setz12z40zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6286), (long) CINT(BgL_vz00_6287));
		}

	}



/* blockV-generic */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2genericzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_129)
	{
		{	/* SawBbv/bbv-types.sch 169 */
			{
				BgL_blockvz00_bglt BgL_auxz00_7445;

				{
					obj_t BgL_auxz00_7446;

					{	/* SawBbv/bbv-types.sch 169 */
						BgL_objectz00_bglt BgL_tmpz00_7447;

						BgL_tmpz00_7447 = ((BgL_objectz00_bglt) BgL_oz00_129);
						BgL_auxz00_7446 = BGL_OBJECT_WIDENING(BgL_tmpz00_7447);
					}
					BgL_auxz00_7445 = ((BgL_blockvz00_bglt) BgL_auxz00_7446);
				}
				return
					(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_7445))->BgL_genericz00);
			}
		}

	}



/* &blockV-generic */
	obj_t BGl_z62blockVzd2genericzb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6288,
		obj_t BgL_oz00_6289)
	{
		{	/* SawBbv/bbv-types.sch 169 */
			return
				BGl_blockVzd2genericzd2zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6289));
		}

	}



/* blockV-generic-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2genericzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_130, obj_t BgL_vz00_131)
	{
		{	/* SawBbv/bbv-types.sch 170 */
			{
				BgL_blockvz00_bglt BgL_auxz00_7454;

				{
					obj_t BgL_auxz00_7455;

					{	/* SawBbv/bbv-types.sch 170 */
						BgL_objectz00_bglt BgL_tmpz00_7456;

						BgL_tmpz00_7456 = ((BgL_objectz00_bglt) BgL_oz00_130);
						BgL_auxz00_7455 = BGL_OBJECT_WIDENING(BgL_tmpz00_7456);
					}
					BgL_auxz00_7454 = ((BgL_blockvz00_bglt) BgL_auxz00_7455);
				}
				return
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_7454))->BgL_genericz00) =
					((obj_t) BgL_vz00_131), BUNSPEC);
			}
		}

	}



/* &blockV-generic-set! */
	obj_t BGl_z62blockVzd2genericzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6290, obj_t BgL_oz00_6291, obj_t BgL_vz00_6292)
	{
		{	/* SawBbv/bbv-types.sch 170 */
			return
				BGl_blockVzd2genericzd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6291), BgL_vz00_6292);
		}

	}



/* blockV-versions */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2versionszd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_132)
	{
		{	/* SawBbv/bbv-types.sch 171 */
			{
				BgL_blockvz00_bglt BgL_auxz00_7463;

				{
					obj_t BgL_auxz00_7464;

					{	/* SawBbv/bbv-types.sch 171 */
						BgL_objectz00_bglt BgL_tmpz00_7465;

						BgL_tmpz00_7465 = ((BgL_objectz00_bglt) BgL_oz00_132);
						BgL_auxz00_7464 = BGL_OBJECT_WIDENING(BgL_tmpz00_7465);
					}
					BgL_auxz00_7463 = ((BgL_blockvz00_bglt) BgL_auxz00_7464);
				}
				return
					(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_7463))->BgL_versionsz00);
			}
		}

	}



/* &blockV-versions */
	obj_t BGl_z62blockVzd2versionszb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6293,
		obj_t BgL_oz00_6294)
	{
		{	/* SawBbv/bbv-types.sch 171 */
			return
				BGl_blockVzd2versionszd2zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6294));
		}

	}



/* blockV-versions-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2versionszd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_133, obj_t BgL_vz00_134)
	{
		{	/* SawBbv/bbv-types.sch 172 */
			{
				BgL_blockvz00_bglt BgL_auxz00_7472;

				{
					obj_t BgL_auxz00_7473;

					{	/* SawBbv/bbv-types.sch 172 */
						BgL_objectz00_bglt BgL_tmpz00_7474;

						BgL_tmpz00_7474 = ((BgL_objectz00_bglt) BgL_oz00_133);
						BgL_auxz00_7473 = BGL_OBJECT_WIDENING(BgL_tmpz00_7474);
					}
					BgL_auxz00_7472 = ((BgL_blockvz00_bglt) BgL_auxz00_7473);
				}
				return
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_7472))->BgL_versionsz00) =
					((obj_t) BgL_vz00_134), BUNSPEC);
			}
		}

	}



/* &blockV-versions-set! */
	obj_t BGl_z62blockVzd2versionszd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6295, obj_t BgL_oz00_6296, obj_t BgL_vz00_6297)
	{
		{	/* SawBbv/bbv-types.sch 172 */
			return
				BGl_blockVzd2versionszd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6296), BgL_vz00_6297);
		}

	}



/* blockV-first */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2firstzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_135)
	{
		{	/* SawBbv/bbv-types.sch 173 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_135)))->BgL_firstz00);
		}

	}



/* &blockV-first */
	obj_t BGl_z62blockVzd2firstzb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6298,
		obj_t BgL_oz00_6299)
	{
		{	/* SawBbv/bbv-types.sch 173 */
			return
				BGl_blockVzd2firstzd2zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6299));
		}

	}



/* blockV-first-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2firstzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_136, obj_t BgL_vz00_137)
	{
		{	/* SawBbv/bbv-types.sch 174 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_136)))->BgL_firstz00) =
				((obj_t) BgL_vz00_137), BUNSPEC);
		}

	}



/* &blockV-first-set! */
	obj_t BGl_z62blockVzd2firstzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6300, obj_t BgL_oz00_6301, obj_t BgL_vz00_6302)
	{
		{	/* SawBbv/bbv-types.sch 174 */
			return
				BGl_blockVzd2firstzd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6301), BgL_vz00_6302);
		}

	}



/* blockV-succs */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2succszd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_138)
	{
		{	/* SawBbv/bbv-types.sch 175 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_138)))->BgL_succsz00);
		}

	}



/* &blockV-succs */
	obj_t BGl_z62blockVzd2succszb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6303,
		obj_t BgL_oz00_6304)
	{
		{	/* SawBbv/bbv-types.sch 175 */
			return
				BGl_blockVzd2succszd2zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6304));
		}

	}



/* blockV-succs-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2succszd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_139, obj_t BgL_vz00_140)
	{
		{	/* SawBbv/bbv-types.sch 176 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_139)))->BgL_succsz00) =
				((obj_t) BgL_vz00_140), BUNSPEC);
		}

	}



/* &blockV-succs-set! */
	obj_t BGl_z62blockVzd2succszd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6305, obj_t BgL_oz00_6306, obj_t BgL_vz00_6307)
	{
		{	/* SawBbv/bbv-types.sch 176 */
			return
				BGl_blockVzd2succszd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6306), BgL_vz00_6307);
		}

	}



/* blockV-preds */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2predszd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_141)
	{
		{	/* SawBbv/bbv-types.sch 177 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_141)))->BgL_predsz00);
		}

	}



/* &blockV-preds */
	obj_t BGl_z62blockVzd2predszb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6308,
		obj_t BgL_oz00_6309)
	{
		{	/* SawBbv/bbv-types.sch 177 */
			return
				BGl_blockVzd2predszd2zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6309));
		}

	}



/* blockV-preds-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2predszd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_142, obj_t BgL_vz00_143)
	{
		{	/* SawBbv/bbv-types.sch 178 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_142)))->BgL_predsz00) =
				((obj_t) BgL_vz00_143), BUNSPEC);
		}

	}



/* &blockV-preds-set! */
	obj_t BGl_z62blockVzd2predszd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6310, obj_t BgL_oz00_6311, obj_t BgL_vz00_6312)
	{
		{	/* SawBbv/bbv-types.sch 178 */
			return
				BGl_blockVzd2predszd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6311), BgL_vz00_6312);
		}

	}



/* blockV-label */
	BGL_EXPORTED_DEF int
		BGl_blockVzd2labelzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_144)
	{
		{	/* SawBbv/bbv-types.sch 179 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_144)))->BgL_labelz00);
		}

	}



/* &blockV-label */
	obj_t BGl_z62blockVzd2labelzb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6313,
		obj_t BgL_oz00_6314)
	{
		{	/* SawBbv/bbv-types.sch 179 */
			return
				BINT(BGl_blockVzd2labelzd2zzsaw_bbvzd2mergezd2(
					((BgL_blockz00_bglt) BgL_oz00_6314)));
		}

	}



/* blockV-label-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2labelzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_145, int BgL_vz00_146)
	{
		{	/* SawBbv/bbv-types.sch 180 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_145)))->BgL_labelz00) =
				((int) BgL_vz00_146), BUNSPEC);
		}

	}



/* &blockV-label-set! */
	obj_t BGl_z62blockVzd2labelzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6315, obj_t BgL_oz00_6316, obj_t BgL_vz00_6317)
	{
		{	/* SawBbv/bbv-types.sch 180 */
			return
				BGl_blockVzd2labelzd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6316), CINT(BgL_vz00_6317));
		}

	}



/* make-blockS */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_makezd2blockSzd2zzsaw_bbvzd2mergezd2(int BgL_label1239z00_147,
		obj_t BgL_preds1240z00_148, obj_t BgL_succs1241z00_149,
		obj_t BgL_first1242z00_150, long BgL_z52mark1243z52_151,
		obj_t BgL_z52hash1244z52_152, obj_t BgL_z52blacklist1245z52_153,
		BgL_bbvzd2ctxzd2_bglt BgL_ctx1246z00_154,
		BgL_blockz00_bglt BgL_parent1247z00_155, long BgL_gccnt1248z00_156,
		long BgL_gcmark1249z00_157, obj_t BgL_mblock1250z00_158,
		obj_t BgL_creator1251z00_159, obj_t BgL_merges1252z00_160,
		bool_t BgL_asleep1253z00_161)
	{
		{	/* SawBbv/bbv-types.sch 183 */
			{	/* SawBbv/bbv-types.sch 183 */
				BgL_blockz00_bglt BgL_new1180z00_6618;

				{	/* SawBbv/bbv-types.sch 183 */
					BgL_blockz00_bglt BgL_tmp1178z00_6619;
					BgL_blocksz00_bglt BgL_wide1179z00_6620;

					{
						BgL_blockz00_bglt BgL_auxz00_7515;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_blockz00_bglt BgL_new1177z00_6621;

							BgL_new1177z00_6621 =
								((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_blockz00_bgl))));
							{	/* SawBbv/bbv-types.sch 183 */
								long BgL_arg1972z00_6622;

								BgL_arg1972z00_6622 = BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1177z00_6621),
									BgL_arg1972z00_6622);
							}
							{	/* SawBbv/bbv-types.sch 183 */
								BgL_objectz00_bglt BgL_tmpz00_7520;

								BgL_tmpz00_7520 = ((BgL_objectz00_bglt) BgL_new1177z00_6621);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7520, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1177z00_6621);
							BgL_auxz00_7515 = BgL_new1177z00_6621;
						}
						BgL_tmp1178z00_6619 = ((BgL_blockz00_bglt) BgL_auxz00_7515);
					}
					BgL_wide1179z00_6620 =
						((BgL_blocksz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_blocksz00_bgl))));
					{	/* SawBbv/bbv-types.sch 183 */
						obj_t BgL_auxz00_7528;
						BgL_objectz00_bglt BgL_tmpz00_7526;

						BgL_auxz00_7528 = ((obj_t) BgL_wide1179z00_6620);
						BgL_tmpz00_7526 = ((BgL_objectz00_bglt) BgL_tmp1178z00_6619);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7526, BgL_auxz00_7528);
					}
					((BgL_objectz00_bglt) BgL_tmp1178z00_6619);
					{	/* SawBbv/bbv-types.sch 183 */
						long BgL_arg1971z00_6623;

						BgL_arg1971z00_6623 =
							BGL_CLASS_NUM(BGl_blockSz00zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1178z00_6619), BgL_arg1971z00_6623);
					}
					BgL_new1180z00_6618 = ((BgL_blockz00_bglt) BgL_tmp1178z00_6619);
				}
				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_new1180z00_6618)))->BgL_labelz00) =
					((int) BgL_label1239z00_147), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1180z00_6618)))->BgL_predsz00) =
					((obj_t) BgL_preds1240z00_148), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1180z00_6618)))->BgL_succsz00) =
					((obj_t) BgL_succs1241z00_149), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1180z00_6618)))->BgL_firstz00) =
					((obj_t) BgL_first1242z00_150), BUNSPEC);
				{
					BgL_blocksz00_bglt BgL_auxz00_7544;

					{
						obj_t BgL_auxz00_7545;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_7546;

							BgL_tmpz00_7546 = ((BgL_objectz00_bglt) BgL_new1180z00_6618);
							BgL_auxz00_7545 = BGL_OBJECT_WIDENING(BgL_tmpz00_7546);
						}
						BgL_auxz00_7544 = ((BgL_blocksz00_bglt) BgL_auxz00_7545);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7544))->BgL_z52markz52) =
						((long) BgL_z52mark1243z52_151), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_7551;

					{
						obj_t BgL_auxz00_7552;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_7553;

							BgL_tmpz00_7553 = ((BgL_objectz00_bglt) BgL_new1180z00_6618);
							BgL_auxz00_7552 = BGL_OBJECT_WIDENING(BgL_tmpz00_7553);
						}
						BgL_auxz00_7551 = ((BgL_blocksz00_bglt) BgL_auxz00_7552);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7551))->BgL_z52hashz52) =
						((obj_t) BgL_z52hash1244z52_152), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_7558;

					{
						obj_t BgL_auxz00_7559;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_7560;

							BgL_tmpz00_7560 = ((BgL_objectz00_bglt) BgL_new1180z00_6618);
							BgL_auxz00_7559 = BGL_OBJECT_WIDENING(BgL_tmpz00_7560);
						}
						BgL_auxz00_7558 = ((BgL_blocksz00_bglt) BgL_auxz00_7559);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7558))->
							BgL_z52blacklistz52) =
						((obj_t) BgL_z52blacklist1245z52_153), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_7565;

					{
						obj_t BgL_auxz00_7566;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_7567;

							BgL_tmpz00_7567 = ((BgL_objectz00_bglt) BgL_new1180z00_6618);
							BgL_auxz00_7566 = BGL_OBJECT_WIDENING(BgL_tmpz00_7567);
						}
						BgL_auxz00_7565 = ((BgL_blocksz00_bglt) BgL_auxz00_7566);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7565))->BgL_ctxz00) =
						((BgL_bbvzd2ctxzd2_bglt) BgL_ctx1246z00_154), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_7572;

					{
						obj_t BgL_auxz00_7573;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_7574;

							BgL_tmpz00_7574 = ((BgL_objectz00_bglt) BgL_new1180z00_6618);
							BgL_auxz00_7573 = BGL_OBJECT_WIDENING(BgL_tmpz00_7574);
						}
						BgL_auxz00_7572 = ((BgL_blocksz00_bglt) BgL_auxz00_7573);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7572))->BgL_parentz00) =
						((BgL_blockz00_bglt) BgL_parent1247z00_155), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_7579;

					{
						obj_t BgL_auxz00_7580;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_7581;

							BgL_tmpz00_7581 = ((BgL_objectz00_bglt) BgL_new1180z00_6618);
							BgL_auxz00_7580 = BGL_OBJECT_WIDENING(BgL_tmpz00_7581);
						}
						BgL_auxz00_7579 = ((BgL_blocksz00_bglt) BgL_auxz00_7580);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7579))->BgL_gccntz00) =
						((long) BgL_gccnt1248z00_156), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_7586;

					{
						obj_t BgL_auxz00_7587;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_7588;

							BgL_tmpz00_7588 = ((BgL_objectz00_bglt) BgL_new1180z00_6618);
							BgL_auxz00_7587 = BGL_OBJECT_WIDENING(BgL_tmpz00_7588);
						}
						BgL_auxz00_7586 = ((BgL_blocksz00_bglt) BgL_auxz00_7587);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7586))->BgL_gcmarkz00) =
						((long) BgL_gcmark1249z00_157), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_7593;

					{
						obj_t BgL_auxz00_7594;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_7595;

							BgL_tmpz00_7595 = ((BgL_objectz00_bglt) BgL_new1180z00_6618);
							BgL_auxz00_7594 = BGL_OBJECT_WIDENING(BgL_tmpz00_7595);
						}
						BgL_auxz00_7593 = ((BgL_blocksz00_bglt) BgL_auxz00_7594);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7593))->BgL_mblockz00) =
						((obj_t) BgL_mblock1250z00_158), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_7600;

					{
						obj_t BgL_auxz00_7601;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_7602;

							BgL_tmpz00_7602 = ((BgL_objectz00_bglt) BgL_new1180z00_6618);
							BgL_auxz00_7601 = BGL_OBJECT_WIDENING(BgL_tmpz00_7602);
						}
						BgL_auxz00_7600 = ((BgL_blocksz00_bglt) BgL_auxz00_7601);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7600))->BgL_creatorz00) =
						((obj_t) BgL_creator1251z00_159), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_7607;

					{
						obj_t BgL_auxz00_7608;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_7609;

							BgL_tmpz00_7609 = ((BgL_objectz00_bglt) BgL_new1180z00_6618);
							BgL_auxz00_7608 = BGL_OBJECT_WIDENING(BgL_tmpz00_7609);
						}
						BgL_auxz00_7607 = ((BgL_blocksz00_bglt) BgL_auxz00_7608);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7607))->BgL_mergesz00) =
						((obj_t) BgL_merges1252z00_160), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_7614;

					{
						obj_t BgL_auxz00_7615;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_7616;

							BgL_tmpz00_7616 = ((BgL_objectz00_bglt) BgL_new1180z00_6618);
							BgL_auxz00_7615 = BGL_OBJECT_WIDENING(BgL_tmpz00_7616);
						}
						BgL_auxz00_7614 = ((BgL_blocksz00_bglt) BgL_auxz00_7615);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7614))->BgL_asleepz00) =
						((bool_t) BgL_asleep1253z00_161), BUNSPEC);
				}
				return BgL_new1180z00_6618;
			}
		}

	}



/* &make-blockS */
	BgL_blockz00_bglt BGl_z62makezd2blockSzb0zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6318, obj_t BgL_label1239z00_6319, obj_t BgL_preds1240z00_6320,
		obj_t BgL_succs1241z00_6321, obj_t BgL_first1242z00_6322,
		obj_t BgL_z52mark1243z52_6323, obj_t BgL_z52hash1244z52_6324,
		obj_t BgL_z52blacklist1245z52_6325, obj_t BgL_ctx1246z00_6326,
		obj_t BgL_parent1247z00_6327, obj_t BgL_gccnt1248z00_6328,
		obj_t BgL_gcmark1249z00_6329, obj_t BgL_mblock1250z00_6330,
		obj_t BgL_creator1251z00_6331, obj_t BgL_merges1252z00_6332,
		obj_t BgL_asleep1253z00_6333)
	{
		{	/* SawBbv/bbv-types.sch 183 */
			return
				BGl_makezd2blockSzd2zzsaw_bbvzd2mergezd2(CINT(BgL_label1239z00_6319),
				BgL_preds1240z00_6320, BgL_succs1241z00_6321, BgL_first1242z00_6322,
				(long) CINT(BgL_z52mark1243z52_6323), BgL_z52hash1244z52_6324,
				BgL_z52blacklist1245z52_6325,
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctx1246z00_6326),
				((BgL_blockz00_bglt) BgL_parent1247z00_6327),
				(long) CINT(BgL_gccnt1248z00_6328), (long) CINT(BgL_gcmark1249z00_6329),
				BgL_mblock1250z00_6330, BgL_creator1251z00_6331, BgL_merges1252z00_6332,
				CBOOL(BgL_asleep1253z00_6333));
		}

	}



/* blockS? */
	BGL_EXPORTED_DEF bool_t BGl_blockSzf3zf3zzsaw_bbvzd2mergezd2(obj_t
		BgL_objz00_162)
	{
		{	/* SawBbv/bbv-types.sch 184 */
			{	/* SawBbv/bbv-types.sch 184 */
				obj_t BgL_classz00_6624;

				BgL_classz00_6624 = BGl_blockSz00zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_162))
					{	/* SawBbv/bbv-types.sch 184 */
						BgL_objectz00_bglt BgL_arg1807z00_6625;

						BgL_arg1807z00_6625 = (BgL_objectz00_bglt) (BgL_objz00_162);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 184 */
								long BgL_idxz00_6626;

								BgL_idxz00_6626 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6625);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6626 + 2L)) == BgL_classz00_6624);
							}
						else
							{	/* SawBbv/bbv-types.sch 184 */
								bool_t BgL_res3259z00_6629;

								{	/* SawBbv/bbv-types.sch 184 */
									obj_t BgL_oclassz00_6630;

									{	/* SawBbv/bbv-types.sch 184 */
										obj_t BgL_arg1815z00_6631;
										long BgL_arg1816z00_6632;

										BgL_arg1815z00_6631 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 184 */
											long BgL_arg1817z00_6633;

											BgL_arg1817z00_6633 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6625);
											BgL_arg1816z00_6632 = (BgL_arg1817z00_6633 - OBJECT_TYPE);
										}
										BgL_oclassz00_6630 =
											VECTOR_REF(BgL_arg1815z00_6631, BgL_arg1816z00_6632);
									}
									{	/* SawBbv/bbv-types.sch 184 */
										bool_t BgL__ortest_1115z00_6634;

										BgL__ortest_1115z00_6634 =
											(BgL_classz00_6624 == BgL_oclassz00_6630);
										if (BgL__ortest_1115z00_6634)
											{	/* SawBbv/bbv-types.sch 184 */
												BgL_res3259z00_6629 = BgL__ortest_1115z00_6634;
											}
										else
											{	/* SawBbv/bbv-types.sch 184 */
												long BgL_odepthz00_6635;

												{	/* SawBbv/bbv-types.sch 184 */
													obj_t BgL_arg1804z00_6636;

													BgL_arg1804z00_6636 = (BgL_oclassz00_6630);
													BgL_odepthz00_6635 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6636);
												}
												if ((2L < BgL_odepthz00_6635))
													{	/* SawBbv/bbv-types.sch 184 */
														obj_t BgL_arg1802z00_6637;

														{	/* SawBbv/bbv-types.sch 184 */
															obj_t BgL_arg1803z00_6638;

															BgL_arg1803z00_6638 = (BgL_oclassz00_6630);
															BgL_arg1802z00_6637 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6638,
																2L);
														}
														BgL_res3259z00_6629 =
															(BgL_arg1802z00_6637 == BgL_classz00_6624);
													}
												else
													{	/* SawBbv/bbv-types.sch 184 */
														BgL_res3259z00_6629 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3259z00_6629;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 184 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &blockS? */
	obj_t BGl_z62blockSzf3z91zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6334,
		obj_t BgL_objz00_6335)
	{
		{	/* SawBbv/bbv-types.sch 184 */
			return BBOOL(BGl_blockSzf3zf3zzsaw_bbvzd2mergezd2(BgL_objz00_6335));
		}

	}



/* blockS-nil */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_blockSzd2nilzd2zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawBbv/bbv-types.sch 185 */
			{	/* SawBbv/bbv-types.sch 185 */
				obj_t BgL_classz00_4699;

				BgL_classz00_4699 = BGl_blockSz00zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 185 */
					obj_t BgL__ortest_1117z00_4700;

					BgL__ortest_1117z00_4700 = BGL_CLASS_NIL(BgL_classz00_4699);
					if (CBOOL(BgL__ortest_1117z00_4700))
						{	/* SawBbv/bbv-types.sch 185 */
							return ((BgL_blockz00_bglt) BgL__ortest_1117z00_4700);
						}
					else
						{	/* SawBbv/bbv-types.sch 185 */
							return
								((BgL_blockz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4699));
						}
				}
			}
		}

	}



/* &blockS-nil */
	BgL_blockz00_bglt BGl_z62blockSzd2nilzb0zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6336)
	{
		{	/* SawBbv/bbv-types.sch 185 */
			return BGl_blockSzd2nilzd2zzsaw_bbvzd2mergezd2();
		}

	}



/* blockS-asleep */
	BGL_EXPORTED_DEF bool_t
		BGl_blockSzd2asleepzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_163)
	{
		{	/* SawBbv/bbv-types.sch 186 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7660;

				{
					obj_t BgL_auxz00_7661;

					{	/* SawBbv/bbv-types.sch 186 */
						BgL_objectz00_bglt BgL_tmpz00_7662;

						BgL_tmpz00_7662 = ((BgL_objectz00_bglt) BgL_oz00_163);
						BgL_auxz00_7661 = BGL_OBJECT_WIDENING(BgL_tmpz00_7662);
					}
					BgL_auxz00_7660 = ((BgL_blocksz00_bglt) BgL_auxz00_7661);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7660))->BgL_asleepz00);
			}
		}

	}



/* &blockS-asleep */
	obj_t BGl_z62blockSzd2asleepzb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6337,
		obj_t BgL_oz00_6338)
	{
		{	/* SawBbv/bbv-types.sch 186 */
			return
				BBOOL(BGl_blockSzd2asleepzd2zzsaw_bbvzd2mergezd2(
					((BgL_blockz00_bglt) BgL_oz00_6338)));
		}

	}



/* blockS-asleep-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2asleepzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_164, bool_t BgL_vz00_165)
	{
		{	/* SawBbv/bbv-types.sch 187 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7670;

				{
					obj_t BgL_auxz00_7671;

					{	/* SawBbv/bbv-types.sch 187 */
						BgL_objectz00_bglt BgL_tmpz00_7672;

						BgL_tmpz00_7672 = ((BgL_objectz00_bglt) BgL_oz00_164);
						BgL_auxz00_7671 = BGL_OBJECT_WIDENING(BgL_tmpz00_7672);
					}
					BgL_auxz00_7670 = ((BgL_blocksz00_bglt) BgL_auxz00_7671);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7670))->BgL_asleepz00) =
					((bool_t) BgL_vz00_165), BUNSPEC);
			}
		}

	}



/* &blockS-asleep-set! */
	obj_t BGl_z62blockSzd2asleepzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6339, obj_t BgL_oz00_6340, obj_t BgL_vz00_6341)
	{
		{	/* SawBbv/bbv-types.sch 187 */
			return
				BGl_blockSzd2asleepzd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6340), CBOOL(BgL_vz00_6341));
		}

	}



/* blockS-merges */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2mergeszd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_166)
	{
		{	/* SawBbv/bbv-types.sch 188 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7680;

				{
					obj_t BgL_auxz00_7681;

					{	/* SawBbv/bbv-types.sch 188 */
						BgL_objectz00_bglt BgL_tmpz00_7682;

						BgL_tmpz00_7682 = ((BgL_objectz00_bglt) BgL_oz00_166);
						BgL_auxz00_7681 = BGL_OBJECT_WIDENING(BgL_tmpz00_7682);
					}
					BgL_auxz00_7680 = ((BgL_blocksz00_bglt) BgL_auxz00_7681);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7680))->BgL_mergesz00);
			}
		}

	}



/* &blockS-merges */
	obj_t BGl_z62blockSzd2mergeszb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6342,
		obj_t BgL_oz00_6343)
	{
		{	/* SawBbv/bbv-types.sch 188 */
			return
				BGl_blockSzd2mergeszd2zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6343));
		}

	}



/* blockS-merges-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2mergeszd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_167, obj_t BgL_vz00_168)
	{
		{	/* SawBbv/bbv-types.sch 189 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7689;

				{
					obj_t BgL_auxz00_7690;

					{	/* SawBbv/bbv-types.sch 189 */
						BgL_objectz00_bglt BgL_tmpz00_7691;

						BgL_tmpz00_7691 = ((BgL_objectz00_bglt) BgL_oz00_167);
						BgL_auxz00_7690 = BGL_OBJECT_WIDENING(BgL_tmpz00_7691);
					}
					BgL_auxz00_7689 = ((BgL_blocksz00_bglt) BgL_auxz00_7690);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7689))->BgL_mergesz00) =
					((obj_t) BgL_vz00_168), BUNSPEC);
			}
		}

	}



/* &blockS-merges-set! */
	obj_t BGl_z62blockSzd2mergeszd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6344, obj_t BgL_oz00_6345, obj_t BgL_vz00_6346)
	{
		{	/* SawBbv/bbv-types.sch 189 */
			return
				BGl_blockSzd2mergeszd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6345), BgL_vz00_6346);
		}

	}



/* blockS-creator */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2creatorzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_169)
	{
		{	/* SawBbv/bbv-types.sch 190 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7698;

				{
					obj_t BgL_auxz00_7699;

					{	/* SawBbv/bbv-types.sch 190 */
						BgL_objectz00_bglt BgL_tmpz00_7700;

						BgL_tmpz00_7700 = ((BgL_objectz00_bglt) BgL_oz00_169);
						BgL_auxz00_7699 = BGL_OBJECT_WIDENING(BgL_tmpz00_7700);
					}
					BgL_auxz00_7698 = ((BgL_blocksz00_bglt) BgL_auxz00_7699);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7698))->BgL_creatorz00);
			}
		}

	}



/* &blockS-creator */
	obj_t BGl_z62blockSzd2creatorzb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6347,
		obj_t BgL_oz00_6348)
	{
		{	/* SawBbv/bbv-types.sch 190 */
			return
				BGl_blockSzd2creatorzd2zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6348));
		}

	}



/* blockS-mblock */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2mblockzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_172)
	{
		{	/* SawBbv/bbv-types.sch 192 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7707;

				{
					obj_t BgL_auxz00_7708;

					{	/* SawBbv/bbv-types.sch 192 */
						BgL_objectz00_bglt BgL_tmpz00_7709;

						BgL_tmpz00_7709 = ((BgL_objectz00_bglt) BgL_oz00_172);
						BgL_auxz00_7708 = BGL_OBJECT_WIDENING(BgL_tmpz00_7709);
					}
					BgL_auxz00_7707 = ((BgL_blocksz00_bglt) BgL_auxz00_7708);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7707))->BgL_mblockz00);
			}
		}

	}



/* &blockS-mblock */
	obj_t BGl_z62blockSzd2mblockzb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6349,
		obj_t BgL_oz00_6350)
	{
		{	/* SawBbv/bbv-types.sch 192 */
			return
				BGl_blockSzd2mblockzd2zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6350));
		}

	}



/* blockS-mblock-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2mblockzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_173, obj_t BgL_vz00_174)
	{
		{	/* SawBbv/bbv-types.sch 193 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7716;

				{
					obj_t BgL_auxz00_7717;

					{	/* SawBbv/bbv-types.sch 193 */
						BgL_objectz00_bglt BgL_tmpz00_7718;

						BgL_tmpz00_7718 = ((BgL_objectz00_bglt) BgL_oz00_173);
						BgL_auxz00_7717 = BGL_OBJECT_WIDENING(BgL_tmpz00_7718);
					}
					BgL_auxz00_7716 = ((BgL_blocksz00_bglt) BgL_auxz00_7717);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7716))->BgL_mblockz00) =
					((obj_t) BgL_vz00_174), BUNSPEC);
			}
		}

	}



/* &blockS-mblock-set! */
	obj_t BGl_z62blockSzd2mblockzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6351, obj_t BgL_oz00_6352, obj_t BgL_vz00_6353)
	{
		{	/* SawBbv/bbv-types.sch 193 */
			return
				BGl_blockSzd2mblockzd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6352), BgL_vz00_6353);
		}

	}



/* blockS-gcmark */
	BGL_EXPORTED_DEF long
		BGl_blockSzd2gcmarkzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_175)
	{
		{	/* SawBbv/bbv-types.sch 194 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7725;

				{
					obj_t BgL_auxz00_7726;

					{	/* SawBbv/bbv-types.sch 194 */
						BgL_objectz00_bglt BgL_tmpz00_7727;

						BgL_tmpz00_7727 = ((BgL_objectz00_bglt) BgL_oz00_175);
						BgL_auxz00_7726 = BGL_OBJECT_WIDENING(BgL_tmpz00_7727);
					}
					BgL_auxz00_7725 = ((BgL_blocksz00_bglt) BgL_auxz00_7726);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7725))->BgL_gcmarkz00);
			}
		}

	}



/* &blockS-gcmark */
	obj_t BGl_z62blockSzd2gcmarkzb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6354,
		obj_t BgL_oz00_6355)
	{
		{	/* SawBbv/bbv-types.sch 194 */
			return
				BINT(BGl_blockSzd2gcmarkzd2zzsaw_bbvzd2mergezd2(
					((BgL_blockz00_bglt) BgL_oz00_6355)));
		}

	}



/* blockS-gcmark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2gcmarkzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_176, long BgL_vz00_177)
	{
		{	/* SawBbv/bbv-types.sch 195 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7735;

				{
					obj_t BgL_auxz00_7736;

					{	/* SawBbv/bbv-types.sch 195 */
						BgL_objectz00_bglt BgL_tmpz00_7737;

						BgL_tmpz00_7737 = ((BgL_objectz00_bglt) BgL_oz00_176);
						BgL_auxz00_7736 = BGL_OBJECT_WIDENING(BgL_tmpz00_7737);
					}
					BgL_auxz00_7735 = ((BgL_blocksz00_bglt) BgL_auxz00_7736);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7735))->BgL_gcmarkz00) =
					((long) BgL_vz00_177), BUNSPEC);
		}}

	}



/* &blockS-gcmark-set! */
	obj_t BGl_z62blockSzd2gcmarkzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6356, obj_t BgL_oz00_6357, obj_t BgL_vz00_6358)
	{
		{	/* SawBbv/bbv-types.sch 195 */
			return
				BGl_blockSzd2gcmarkzd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6357), (long) CINT(BgL_vz00_6358));
		}

	}



/* blockS-gccnt */
	BGL_EXPORTED_DEF long
		BGl_blockSzd2gccntzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_178)
	{
		{	/* SawBbv/bbv-types.sch 196 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7745;

				{
					obj_t BgL_auxz00_7746;

					{	/* SawBbv/bbv-types.sch 196 */
						BgL_objectz00_bglt BgL_tmpz00_7747;

						BgL_tmpz00_7747 = ((BgL_objectz00_bglt) BgL_oz00_178);
						BgL_auxz00_7746 = BGL_OBJECT_WIDENING(BgL_tmpz00_7747);
					}
					BgL_auxz00_7745 = ((BgL_blocksz00_bglt) BgL_auxz00_7746);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7745))->BgL_gccntz00);
			}
		}

	}



/* &blockS-gccnt */
	obj_t BGl_z62blockSzd2gccntzb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6359,
		obj_t BgL_oz00_6360)
	{
		{	/* SawBbv/bbv-types.sch 196 */
			return
				BINT(BGl_blockSzd2gccntzd2zzsaw_bbvzd2mergezd2(
					((BgL_blockz00_bglt) BgL_oz00_6360)));
		}

	}



/* blockS-gccnt-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2gccntzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_179, long BgL_vz00_180)
	{
		{	/* SawBbv/bbv-types.sch 197 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7755;

				{
					obj_t BgL_auxz00_7756;

					{	/* SawBbv/bbv-types.sch 197 */
						BgL_objectz00_bglt BgL_tmpz00_7757;

						BgL_tmpz00_7757 = ((BgL_objectz00_bglt) BgL_oz00_179);
						BgL_auxz00_7756 = BGL_OBJECT_WIDENING(BgL_tmpz00_7757);
					}
					BgL_auxz00_7755 = ((BgL_blocksz00_bglt) BgL_auxz00_7756);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7755))->BgL_gccntz00) =
					((long) BgL_vz00_180), BUNSPEC);
		}}

	}



/* &blockS-gccnt-set! */
	obj_t BGl_z62blockSzd2gccntzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6361, obj_t BgL_oz00_6362, obj_t BgL_vz00_6363)
	{
		{	/* SawBbv/bbv-types.sch 197 */
			return
				BGl_blockSzd2gccntzd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6362), (long) CINT(BgL_vz00_6363));
		}

	}



/* blockS-parent */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_blockSzd2parentzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_181)
	{
		{	/* SawBbv/bbv-types.sch 198 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7765;

				{
					obj_t BgL_auxz00_7766;

					{	/* SawBbv/bbv-types.sch 198 */
						BgL_objectz00_bglt BgL_tmpz00_7767;

						BgL_tmpz00_7767 = ((BgL_objectz00_bglt) BgL_oz00_181);
						BgL_auxz00_7766 = BGL_OBJECT_WIDENING(BgL_tmpz00_7767);
					}
					BgL_auxz00_7765 = ((BgL_blocksz00_bglt) BgL_auxz00_7766);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7765))->BgL_parentz00);
			}
		}

	}



/* &blockS-parent */
	BgL_blockz00_bglt BGl_z62blockSzd2parentzb0zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6364, obj_t BgL_oz00_6365)
	{
		{	/* SawBbv/bbv-types.sch 198 */
			return
				BGl_blockSzd2parentzd2zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6365));
		}

	}



/* blockS-ctx */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_blockSzd2ctxzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_184)
	{
		{	/* SawBbv/bbv-types.sch 200 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7774;

				{
					obj_t BgL_auxz00_7775;

					{	/* SawBbv/bbv-types.sch 200 */
						BgL_objectz00_bglt BgL_tmpz00_7776;

						BgL_tmpz00_7776 = ((BgL_objectz00_bglt) BgL_oz00_184);
						BgL_auxz00_7775 = BGL_OBJECT_WIDENING(BgL_tmpz00_7776);
					}
					BgL_auxz00_7774 = ((BgL_blocksz00_bglt) BgL_auxz00_7775);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7774))->BgL_ctxz00);
			}
		}

	}



/* &blockS-ctx */
	BgL_bbvzd2ctxzd2_bglt BGl_z62blockSzd2ctxzb0zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6366, obj_t BgL_oz00_6367)
	{
		{	/* SawBbv/bbv-types.sch 200 */
			return
				BGl_blockSzd2ctxzd2zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6367));
		}

	}



/* blockS-%blacklist */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2z52blacklistz80zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_187)
	{
		{	/* SawBbv/bbv-types.sch 202 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7783;

				{
					obj_t BgL_auxz00_7784;

					{	/* SawBbv/bbv-types.sch 202 */
						BgL_objectz00_bglt BgL_tmpz00_7785;

						BgL_tmpz00_7785 = ((BgL_objectz00_bglt) BgL_oz00_187);
						BgL_auxz00_7784 = BGL_OBJECT_WIDENING(BgL_tmpz00_7785);
					}
					BgL_auxz00_7783 = ((BgL_blocksz00_bglt) BgL_auxz00_7784);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7783))->
					BgL_z52blacklistz52);
			}
		}

	}



/* &blockS-%blacklist */
	obj_t BGl_z62blockSzd2z52blacklistze2zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6368, obj_t BgL_oz00_6369)
	{
		{	/* SawBbv/bbv-types.sch 202 */
			return
				BGl_blockSzd2z52blacklistz80zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6369));
		}

	}



/* blockS-%blacklist-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2z52blacklistzd2setz12z40zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_188, obj_t BgL_vz00_189)
	{
		{	/* SawBbv/bbv-types.sch 203 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7792;

				{
					obj_t BgL_auxz00_7793;

					{	/* SawBbv/bbv-types.sch 203 */
						BgL_objectz00_bglt BgL_tmpz00_7794;

						BgL_tmpz00_7794 = ((BgL_objectz00_bglt) BgL_oz00_188);
						BgL_auxz00_7793 = BGL_OBJECT_WIDENING(BgL_tmpz00_7794);
					}
					BgL_auxz00_7792 = ((BgL_blocksz00_bglt) BgL_auxz00_7793);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7792))->
						BgL_z52blacklistz52) = ((obj_t) BgL_vz00_189), BUNSPEC);
			}
		}

	}



/* &blockS-%blacklist-set! */
	obj_t BGl_z62blockSzd2z52blacklistzd2setz12z22zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6370, obj_t BgL_oz00_6371, obj_t BgL_vz00_6372)
	{
		{	/* SawBbv/bbv-types.sch 203 */
			return
				BGl_blockSzd2z52blacklistzd2setz12z40zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6371), BgL_vz00_6372);
		}

	}



/* blockS-%hash */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2z52hashz80zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_190)
	{
		{	/* SawBbv/bbv-types.sch 204 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7801;

				{
					obj_t BgL_auxz00_7802;

					{	/* SawBbv/bbv-types.sch 204 */
						BgL_objectz00_bglt BgL_tmpz00_7803;

						BgL_tmpz00_7803 = ((BgL_objectz00_bglt) BgL_oz00_190);
						BgL_auxz00_7802 = BGL_OBJECT_WIDENING(BgL_tmpz00_7803);
					}
					BgL_auxz00_7801 = ((BgL_blocksz00_bglt) BgL_auxz00_7802);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7801))->BgL_z52hashz52);
			}
		}

	}



/* &blockS-%hash */
	obj_t BGl_z62blockSzd2z52hashze2zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6373,
		obj_t BgL_oz00_6374)
	{
		{	/* SawBbv/bbv-types.sch 204 */
			return
				BGl_blockSzd2z52hashz80zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6374));
		}

	}



/* blockS-%hash-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2z52hashzd2setz12z40zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_191, obj_t BgL_vz00_192)
	{
		{	/* SawBbv/bbv-types.sch 205 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7810;

				{
					obj_t BgL_auxz00_7811;

					{	/* SawBbv/bbv-types.sch 205 */
						BgL_objectz00_bglt BgL_tmpz00_7812;

						BgL_tmpz00_7812 = ((BgL_objectz00_bglt) BgL_oz00_191);
						BgL_auxz00_7811 = BGL_OBJECT_WIDENING(BgL_tmpz00_7812);
					}
					BgL_auxz00_7810 = ((BgL_blocksz00_bglt) BgL_auxz00_7811);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7810))->BgL_z52hashz52) =
					((obj_t) BgL_vz00_192), BUNSPEC);
			}
		}

	}



/* &blockS-%hash-set! */
	obj_t BGl_z62blockSzd2z52hashzd2setz12z22zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6375, obj_t BgL_oz00_6376, obj_t BgL_vz00_6377)
	{
		{	/* SawBbv/bbv-types.sch 205 */
			return
				BGl_blockSzd2z52hashzd2setz12z40zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6376), BgL_vz00_6377);
		}

	}



/* blockS-%mark */
	BGL_EXPORTED_DEF long
		BGl_blockSzd2z52markz80zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_193)
	{
		{	/* SawBbv/bbv-types.sch 206 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7819;

				{
					obj_t BgL_auxz00_7820;

					{	/* SawBbv/bbv-types.sch 206 */
						BgL_objectz00_bglt BgL_tmpz00_7821;

						BgL_tmpz00_7821 = ((BgL_objectz00_bglt) BgL_oz00_193);
						BgL_auxz00_7820 = BGL_OBJECT_WIDENING(BgL_tmpz00_7821);
					}
					BgL_auxz00_7819 = ((BgL_blocksz00_bglt) BgL_auxz00_7820);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7819))->BgL_z52markz52);
			}
		}

	}



/* &blockS-%mark */
	obj_t BGl_z62blockSzd2z52markze2zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6378,
		obj_t BgL_oz00_6379)
	{
		{	/* SawBbv/bbv-types.sch 206 */
			return
				BINT(BGl_blockSzd2z52markz80zzsaw_bbvzd2mergezd2(
					((BgL_blockz00_bglt) BgL_oz00_6379)));
		}

	}



/* blockS-%mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2z52markzd2setz12z40zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_194, long BgL_vz00_195)
	{
		{	/* SawBbv/bbv-types.sch 207 */
			{
				BgL_blocksz00_bglt BgL_auxz00_7829;

				{
					obj_t BgL_auxz00_7830;

					{	/* SawBbv/bbv-types.sch 207 */
						BgL_objectz00_bglt BgL_tmpz00_7831;

						BgL_tmpz00_7831 = ((BgL_objectz00_bglt) BgL_oz00_194);
						BgL_auxz00_7830 = BGL_OBJECT_WIDENING(BgL_tmpz00_7831);
					}
					BgL_auxz00_7829 = ((BgL_blocksz00_bglt) BgL_auxz00_7830);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_7829))->BgL_z52markz52) =
					((long) BgL_vz00_195), BUNSPEC);
		}}

	}



/* &blockS-%mark-set! */
	obj_t BGl_z62blockSzd2z52markzd2setz12z22zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6380, obj_t BgL_oz00_6381, obj_t BgL_vz00_6382)
	{
		{	/* SawBbv/bbv-types.sch 207 */
			return
				BGl_blockSzd2z52markzd2setz12z40zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6381), (long) CINT(BgL_vz00_6382));
		}

	}



/* blockS-first */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2firstzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_196)
	{
		{	/* SawBbv/bbv-types.sch 208 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_196)))->BgL_firstz00);
		}

	}



/* &blockS-first */
	obj_t BGl_z62blockSzd2firstzb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6383,
		obj_t BgL_oz00_6384)
	{
		{	/* SawBbv/bbv-types.sch 208 */
			return
				BGl_blockSzd2firstzd2zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6384));
		}

	}



/* blockS-first-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2firstzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_197, obj_t BgL_vz00_198)
	{
		{	/* SawBbv/bbv-types.sch 209 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_197)))->BgL_firstz00) =
				((obj_t) BgL_vz00_198), BUNSPEC);
		}

	}



/* &blockS-first-set! */
	obj_t BGl_z62blockSzd2firstzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6385, obj_t BgL_oz00_6386, obj_t BgL_vz00_6387)
	{
		{	/* SawBbv/bbv-types.sch 209 */
			return
				BGl_blockSzd2firstzd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6386), BgL_vz00_6387);
		}

	}



/* blockS-succs */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2succszd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_199)
	{
		{	/* SawBbv/bbv-types.sch 210 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_199)))->BgL_succsz00);
		}

	}



/* &blockS-succs */
	obj_t BGl_z62blockSzd2succszb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6388,
		obj_t BgL_oz00_6389)
	{
		{	/* SawBbv/bbv-types.sch 210 */
			return
				BGl_blockSzd2succszd2zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6389));
		}

	}



/* blockS-succs-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2succszd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_200, obj_t BgL_vz00_201)
	{
		{	/* SawBbv/bbv-types.sch 211 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_200)))->BgL_succsz00) =
				((obj_t) BgL_vz00_201), BUNSPEC);
		}

	}



/* &blockS-succs-set! */
	obj_t BGl_z62blockSzd2succszd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6390, obj_t BgL_oz00_6391, obj_t BgL_vz00_6392)
	{
		{	/* SawBbv/bbv-types.sch 211 */
			return
				BGl_blockSzd2succszd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6391), BgL_vz00_6392);
		}

	}



/* blockS-preds */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2predszd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_202)
	{
		{	/* SawBbv/bbv-types.sch 212 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_202)))->BgL_predsz00);
		}

	}



/* &blockS-preds */
	obj_t BGl_z62blockSzd2predszb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6393,
		obj_t BgL_oz00_6394)
	{
		{	/* SawBbv/bbv-types.sch 212 */
			return
				BGl_blockSzd2predszd2zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6394));
		}

	}



/* blockS-preds-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2predszd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_203, obj_t BgL_vz00_204)
	{
		{	/* SawBbv/bbv-types.sch 213 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_203)))->BgL_predsz00) =
				((obj_t) BgL_vz00_204), BUNSPEC);
		}

	}



/* &blockS-preds-set! */
	obj_t BGl_z62blockSzd2predszd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6395, obj_t BgL_oz00_6396, obj_t BgL_vz00_6397)
	{
		{	/* SawBbv/bbv-types.sch 213 */
			return
				BGl_blockSzd2predszd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6396), BgL_vz00_6397);
		}

	}



/* blockS-label */
	BGL_EXPORTED_DEF int
		BGl_blockSzd2labelzd2zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt BgL_oz00_205)
	{
		{	/* SawBbv/bbv-types.sch 214 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_205)))->BgL_labelz00);
		}

	}



/* &blockS-label */
	obj_t BGl_z62blockSzd2labelzb0zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6398,
		obj_t BgL_oz00_6399)
	{
		{	/* SawBbv/bbv-types.sch 214 */
			return
				BINT(BGl_blockSzd2labelzd2zzsaw_bbvzd2mergezd2(
					((BgL_blockz00_bglt) BgL_oz00_6399)));
		}

	}



/* blockS-label-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2labelzd2setz12z12zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_oz00_206, int BgL_vz00_207)
	{
		{	/* SawBbv/bbv-types.sch 215 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_206)))->BgL_labelz00) =
				((int) BgL_vz00_207), BUNSPEC);
		}

	}



/* &blockS-label-set! */
	obj_t BGl_z62blockSzd2labelzd2setz12z70zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6400, obj_t BgL_oz00_6401, obj_t BgL_vz00_6402)
	{
		{	/* SawBbv/bbv-types.sch 215 */
			return
				BGl_blockSzd2labelzd2setz12z12zzsaw_bbvzd2mergezd2(
				((BgL_blockz00_bglt) BgL_oz00_6401), CINT(BgL_vz00_6402));
		}

	}



/* make-bbv-queue */
	BGL_EXPORTED_DEF BgL_bbvzd2queuezd2_bglt
		BGl_makezd2bbvzd2queuez00zzsaw_bbvzd2mergezd2(obj_t BgL_blocks1236z00_208,
		obj_t BgL_last1237z00_209)
	{
		{	/* SawBbv/bbv-types.sch 218 */
			{	/* SawBbv/bbv-types.sch 218 */
				BgL_bbvzd2queuezd2_bglt BgL_new1183z00_6639;

				{	/* SawBbv/bbv-types.sch 218 */
					BgL_bbvzd2queuezd2_bglt BgL_new1182z00_6640;

					BgL_new1182z00_6640 =
						((BgL_bbvzd2queuezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2queuezd2_bgl))));
					{	/* SawBbv/bbv-types.sch 218 */
						long BgL_arg1973z00_6641;

						BgL_arg1973z00_6641 =
							BGL_CLASS_NUM(BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1182z00_6640), BgL_arg1973z00_6641);
					}
					BgL_new1183z00_6639 = BgL_new1182z00_6640;
				}
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_new1183z00_6639))->
						BgL_blocksz00) = ((obj_t) BgL_blocks1236z00_208), BUNSPEC);
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_new1183z00_6639))->
						BgL_lastz00) = ((obj_t) BgL_last1237z00_209), BUNSPEC);
				return BgL_new1183z00_6639;
			}
		}

	}



/* &make-bbv-queue */
	BgL_bbvzd2queuezd2_bglt BGl_z62makezd2bbvzd2queuez62zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6403, obj_t BgL_blocks1236z00_6404, obj_t BgL_last1237z00_6405)
	{
		{	/* SawBbv/bbv-types.sch 218 */
			return
				BGl_makezd2bbvzd2queuez00zzsaw_bbvzd2mergezd2(BgL_blocks1236z00_6404,
				BgL_last1237z00_6405);
		}

	}



/* bbv-queue? */
	BGL_EXPORTED_DEF bool_t BGl_bbvzd2queuezf3z21zzsaw_bbvzd2mergezd2(obj_t
		BgL_objz00_210)
	{
		{	/* SawBbv/bbv-types.sch 219 */
			{	/* SawBbv/bbv-types.sch 219 */
				obj_t BgL_classz00_6642;

				BgL_classz00_6642 = BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_210))
					{	/* SawBbv/bbv-types.sch 219 */
						BgL_objectz00_bglt BgL_arg1807z00_6643;

						BgL_arg1807z00_6643 = (BgL_objectz00_bglt) (BgL_objz00_210);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 219 */
								long BgL_idxz00_6644;

								BgL_idxz00_6644 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6643);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6644 + 1L)) == BgL_classz00_6642);
							}
						else
							{	/* SawBbv/bbv-types.sch 219 */
								bool_t BgL_res3260z00_6647;

								{	/* SawBbv/bbv-types.sch 219 */
									obj_t BgL_oclassz00_6648;

									{	/* SawBbv/bbv-types.sch 219 */
										obj_t BgL_arg1815z00_6649;
										long BgL_arg1816z00_6650;

										BgL_arg1815z00_6649 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 219 */
											long BgL_arg1817z00_6651;

											BgL_arg1817z00_6651 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6643);
											BgL_arg1816z00_6650 = (BgL_arg1817z00_6651 - OBJECT_TYPE);
										}
										BgL_oclassz00_6648 =
											VECTOR_REF(BgL_arg1815z00_6649, BgL_arg1816z00_6650);
									}
									{	/* SawBbv/bbv-types.sch 219 */
										bool_t BgL__ortest_1115z00_6652;

										BgL__ortest_1115z00_6652 =
											(BgL_classz00_6642 == BgL_oclassz00_6648);
										if (BgL__ortest_1115z00_6652)
											{	/* SawBbv/bbv-types.sch 219 */
												BgL_res3260z00_6647 = BgL__ortest_1115z00_6652;
											}
										else
											{	/* SawBbv/bbv-types.sch 219 */
												long BgL_odepthz00_6653;

												{	/* SawBbv/bbv-types.sch 219 */
													obj_t BgL_arg1804z00_6654;

													BgL_arg1804z00_6654 = (BgL_oclassz00_6648);
													BgL_odepthz00_6653 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6654);
												}
												if ((1L < BgL_odepthz00_6653))
													{	/* SawBbv/bbv-types.sch 219 */
														obj_t BgL_arg1802z00_6655;

														{	/* SawBbv/bbv-types.sch 219 */
															obj_t BgL_arg1803z00_6656;

															BgL_arg1803z00_6656 = (BgL_oclassz00_6648);
															BgL_arg1802z00_6655 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6656,
																1L);
														}
														BgL_res3260z00_6647 =
															(BgL_arg1802z00_6655 == BgL_classz00_6642);
													}
												else
													{	/* SawBbv/bbv-types.sch 219 */
														BgL_res3260z00_6647 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3260z00_6647;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 219 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bbv-queue? */
	obj_t BGl_z62bbvzd2queuezf3z43zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6406,
		obj_t BgL_objz00_6407)
	{
		{	/* SawBbv/bbv-types.sch 219 */
			return BBOOL(BGl_bbvzd2queuezf3z21zzsaw_bbvzd2mergezd2(BgL_objz00_6407));
		}

	}



/* bbv-queue-nil */
	BGL_EXPORTED_DEF BgL_bbvzd2queuezd2_bglt
		BGl_bbvzd2queuezd2nilz00zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawBbv/bbv-types.sch 220 */
			{	/* SawBbv/bbv-types.sch 220 */
				obj_t BgL_classz00_4760;

				BgL_classz00_4760 = BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 220 */
					obj_t BgL__ortest_1117z00_4761;

					BgL__ortest_1117z00_4761 = BGL_CLASS_NIL(BgL_classz00_4760);
					if (CBOOL(BgL__ortest_1117z00_4761))
						{	/* SawBbv/bbv-types.sch 220 */
							return ((BgL_bbvzd2queuezd2_bglt) BgL__ortest_1117z00_4761);
						}
					else
						{	/* SawBbv/bbv-types.sch 220 */
							return
								((BgL_bbvzd2queuezd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4760));
						}
				}
			}
		}

	}



/* &bbv-queue-nil */
	BgL_bbvzd2queuezd2_bglt BGl_z62bbvzd2queuezd2nilz62zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6408)
	{
		{	/* SawBbv/bbv-types.sch 220 */
			return BGl_bbvzd2queuezd2nilz00zzsaw_bbvzd2mergezd2();
		}

	}



/* bbv-queue-last */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2queuezd2lastz00zzsaw_bbvzd2mergezd2(BgL_bbvzd2queuezd2_bglt
		BgL_oz00_211)
	{
		{	/* SawBbv/bbv-types.sch 221 */
			return (((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_oz00_211))->BgL_lastz00);
		}

	}



/* &bbv-queue-last */
	obj_t BGl_z62bbvzd2queuezd2lastz62zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6409,
		obj_t BgL_oz00_6410)
	{
		{	/* SawBbv/bbv-types.sch 221 */
			return
				BGl_bbvzd2queuezd2lastz00zzsaw_bbvzd2mergezd2(
				((BgL_bbvzd2queuezd2_bglt) BgL_oz00_6410));
		}

	}



/* bbv-queue-last-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2queuezd2lastzd2setz12zc0zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2queuezd2_bglt BgL_oz00_212, obj_t BgL_vz00_213)
	{
		{	/* SawBbv/bbv-types.sch 222 */
			return
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_oz00_212))->BgL_lastz00) =
				((obj_t) BgL_vz00_213), BUNSPEC);
		}

	}



/* &bbv-queue-last-set! */
	obj_t BGl_z62bbvzd2queuezd2lastzd2setz12za2zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6411, obj_t BgL_oz00_6412, obj_t BgL_vz00_6413)
	{
		{	/* SawBbv/bbv-types.sch 222 */
			return
				BGl_bbvzd2queuezd2lastzd2setz12zc0zzsaw_bbvzd2mergezd2(
				((BgL_bbvzd2queuezd2_bglt) BgL_oz00_6412), BgL_vz00_6413);
		}

	}



/* bbv-queue-blocks */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2queuezd2blocksz00zzsaw_bbvzd2mergezd2(BgL_bbvzd2queuezd2_bglt
		BgL_oz00_214)
	{
		{	/* SawBbv/bbv-types.sch 223 */
			return (((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_oz00_214))->BgL_blocksz00);
		}

	}



/* &bbv-queue-blocks */
	obj_t BGl_z62bbvzd2queuezd2blocksz62zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6414, obj_t BgL_oz00_6415)
	{
		{	/* SawBbv/bbv-types.sch 223 */
			return
				BGl_bbvzd2queuezd2blocksz00zzsaw_bbvzd2mergezd2(
				((BgL_bbvzd2queuezd2_bglt) BgL_oz00_6415));
		}

	}



/* bbv-queue-blocks-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2queuezd2blockszd2setz12zc0zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2queuezd2_bglt BgL_oz00_215, obj_t BgL_vz00_216)
	{
		{	/* SawBbv/bbv-types.sch 224 */
			return
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_oz00_215))->BgL_blocksz00) =
				((obj_t) BgL_vz00_216), BUNSPEC);
		}

	}



/* &bbv-queue-blocks-set! */
	obj_t BGl_z62bbvzd2queuezd2blockszd2setz12za2zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6416, obj_t BgL_oz00_6417, obj_t BgL_vz00_6418)
	{
		{	/* SawBbv/bbv-types.sch 224 */
			return
				BGl_bbvzd2queuezd2blockszd2setz12zc0zzsaw_bbvzd2mergezd2(
				((BgL_bbvzd2queuezd2_bglt) BgL_oz00_6417), BgL_vz00_6418);
		}

	}



/* make-bbv-ctx */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_makezd2bbvzd2ctxz00zzsaw_bbvzd2mergezd2(long BgL_id1233z00_217,
		obj_t BgL_entries1234z00_218)
	{
		{	/* SawBbv/bbv-types.sch 227 */
			{	/* SawBbv/bbv-types.sch 227 */
				BgL_bbvzd2ctxzd2_bglt BgL_new1185z00_6657;

				{	/* SawBbv/bbv-types.sch 227 */
					BgL_bbvzd2ctxzd2_bglt BgL_new1184z00_6658;

					BgL_new1184z00_6658 =
						((BgL_bbvzd2ctxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2ctxzd2_bgl))));
					{	/* SawBbv/bbv-types.sch 227 */
						long BgL_arg1975z00_6659;

						BgL_arg1975z00_6659 =
							BGL_CLASS_NUM(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1184z00_6658), BgL_arg1975z00_6659);
					}
					BgL_new1185z00_6657 = BgL_new1184z00_6658;
				}
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1185z00_6657))->BgL_idz00) =
					((long) BgL_id1233z00_217), BUNSPEC);
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1185z00_6657))->
						BgL_entriesz00) = ((obj_t) BgL_entries1234z00_218), BUNSPEC);
				{	/* SawBbv/bbv-types.sch 227 */
					obj_t BgL_fun1974z00_6660;

					BgL_fun1974z00_6660 =
						BGl_classzd2constructorzd2zz__objectz00
						(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
					BGL_PROCEDURE_CALL1(BgL_fun1974z00_6660,
						((obj_t) BgL_new1185z00_6657));
				}
				return BgL_new1185z00_6657;
			}
		}

	}



/* &make-bbv-ctx */
	BgL_bbvzd2ctxzd2_bglt BGl_z62makezd2bbvzd2ctxz62zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6419, obj_t BgL_id1233z00_6420, obj_t BgL_entries1234z00_6421)
	{
		{	/* SawBbv/bbv-types.sch 227 */
			return
				BGl_makezd2bbvzd2ctxz00zzsaw_bbvzd2mergezd2(
				(long) CINT(BgL_id1233z00_6420), BgL_entries1234z00_6421);
		}

	}



/* bbv-ctx? */
	BGL_EXPORTED_DEF bool_t BGl_bbvzd2ctxzf3z21zzsaw_bbvzd2mergezd2(obj_t
		BgL_objz00_219)
	{
		{	/* SawBbv/bbv-types.sch 228 */
			{	/* SawBbv/bbv-types.sch 228 */
				obj_t BgL_classz00_6661;

				BgL_classz00_6661 = BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_219))
					{	/* SawBbv/bbv-types.sch 228 */
						BgL_objectz00_bglt BgL_arg1807z00_6662;

						BgL_arg1807z00_6662 = (BgL_objectz00_bglt) (BgL_objz00_219);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 228 */
								long BgL_idxz00_6663;

								BgL_idxz00_6663 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6662);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6663 + 1L)) == BgL_classz00_6661);
							}
						else
							{	/* SawBbv/bbv-types.sch 228 */
								bool_t BgL_res3261z00_6666;

								{	/* SawBbv/bbv-types.sch 228 */
									obj_t BgL_oclassz00_6667;

									{	/* SawBbv/bbv-types.sch 228 */
										obj_t BgL_arg1815z00_6668;
										long BgL_arg1816z00_6669;

										BgL_arg1815z00_6668 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 228 */
											long BgL_arg1817z00_6670;

											BgL_arg1817z00_6670 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6662);
											BgL_arg1816z00_6669 = (BgL_arg1817z00_6670 - OBJECT_TYPE);
										}
										BgL_oclassz00_6667 =
											VECTOR_REF(BgL_arg1815z00_6668, BgL_arg1816z00_6669);
									}
									{	/* SawBbv/bbv-types.sch 228 */
										bool_t BgL__ortest_1115z00_6671;

										BgL__ortest_1115z00_6671 =
											(BgL_classz00_6661 == BgL_oclassz00_6667);
										if (BgL__ortest_1115z00_6671)
											{	/* SawBbv/bbv-types.sch 228 */
												BgL_res3261z00_6666 = BgL__ortest_1115z00_6671;
											}
										else
											{	/* SawBbv/bbv-types.sch 228 */
												long BgL_odepthz00_6672;

												{	/* SawBbv/bbv-types.sch 228 */
													obj_t BgL_arg1804z00_6673;

													BgL_arg1804z00_6673 = (BgL_oclassz00_6667);
													BgL_odepthz00_6672 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6673);
												}
												if ((1L < BgL_odepthz00_6672))
													{	/* SawBbv/bbv-types.sch 228 */
														obj_t BgL_arg1802z00_6674;

														{	/* SawBbv/bbv-types.sch 228 */
															obj_t BgL_arg1803z00_6675;

															BgL_arg1803z00_6675 = (BgL_oclassz00_6667);
															BgL_arg1802z00_6674 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6675,
																1L);
														}
														BgL_res3261z00_6666 =
															(BgL_arg1802z00_6674 == BgL_classz00_6661);
													}
												else
													{	/* SawBbv/bbv-types.sch 228 */
														BgL_res3261z00_6666 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3261z00_6666;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 228 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bbv-ctx? */
	obj_t BGl_z62bbvzd2ctxzf3z43zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6422,
		obj_t BgL_objz00_6423)
	{
		{	/* SawBbv/bbv-types.sch 228 */
			return BBOOL(BGl_bbvzd2ctxzf3z21zzsaw_bbvzd2mergezd2(BgL_objz00_6423));
		}

	}



/* bbv-ctx-nil */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_bbvzd2ctxzd2nilz00zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawBbv/bbv-types.sch 229 */
			{	/* SawBbv/bbv-types.sch 229 */
				obj_t BgL_classz00_4803;

				BgL_classz00_4803 = BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 229 */
					obj_t BgL__ortest_1117z00_4804;

					BgL__ortest_1117z00_4804 = BGL_CLASS_NIL(BgL_classz00_4803);
					if (CBOOL(BgL__ortest_1117z00_4804))
						{	/* SawBbv/bbv-types.sch 229 */
							return ((BgL_bbvzd2ctxzd2_bglt) BgL__ortest_1117z00_4804);
						}
					else
						{	/* SawBbv/bbv-types.sch 229 */
							return
								((BgL_bbvzd2ctxzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4803));
						}
				}
			}
		}

	}



/* &bbv-ctx-nil */
	BgL_bbvzd2ctxzd2_bglt BGl_z62bbvzd2ctxzd2nilz62zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6424)
	{
		{	/* SawBbv/bbv-types.sch 229 */
			return BGl_bbvzd2ctxzd2nilz00zzsaw_bbvzd2mergezd2();
		}

	}



/* bbv-ctx-entries */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxzd2entriesz00zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxzd2_bglt
		BgL_oz00_220)
	{
		{	/* SawBbv/bbv-types.sch 230 */
			return (((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_oz00_220))->BgL_entriesz00);
		}

	}



/* &bbv-ctx-entries */
	obj_t BGl_z62bbvzd2ctxzd2entriesz62zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6425,
		obj_t BgL_oz00_6426)
	{
		{	/* SawBbv/bbv-types.sch 230 */
			return
				BGl_bbvzd2ctxzd2entriesz00zzsaw_bbvzd2mergezd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_6426));
		}

	}



/* bbv-ctx-entries-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxzd2entrieszd2setz12zc0zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxzd2_bglt BgL_oz00_221, obj_t BgL_vz00_222)
	{
		{	/* SawBbv/bbv-types.sch 231 */
			return
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_oz00_221))->BgL_entriesz00) =
				((obj_t) BgL_vz00_222), BUNSPEC);
		}

	}



/* &bbv-ctx-entries-set! */
	obj_t BGl_z62bbvzd2ctxzd2entrieszd2setz12za2zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6427, obj_t BgL_oz00_6428, obj_t BgL_vz00_6429)
	{
		{	/* SawBbv/bbv-types.sch 231 */
			return
				BGl_bbvzd2ctxzd2entrieszd2setz12zc0zzsaw_bbvzd2mergezd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_6428), BgL_vz00_6429);
		}

	}



/* bbv-ctx-id */
	BGL_EXPORTED_DEF long
		BGl_bbvzd2ctxzd2idz00zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxzd2_bglt
		BgL_oz00_223)
	{
		{	/* SawBbv/bbv-types.sch 232 */
			return (((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_oz00_223))->BgL_idz00);
		}

	}



/* &bbv-ctx-id */
	obj_t BGl_z62bbvzd2ctxzd2idz62zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6430,
		obj_t BgL_oz00_6431)
	{
		{	/* SawBbv/bbv-types.sch 232 */
			return
				BINT(BGl_bbvzd2ctxzd2idz00zzsaw_bbvzd2mergezd2(
					((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_6431)));
		}

	}



/* bbv-ctx-id-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxzd2idzd2setz12zc0zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxzd2_bglt
		BgL_oz00_224, long BgL_vz00_225)
	{
		{	/* SawBbv/bbv-types.sch 233 */
			return
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_oz00_224))->BgL_idz00) =
				((long) BgL_vz00_225), BUNSPEC);
		}

	}



/* &bbv-ctx-id-set! */
	obj_t BGl_z62bbvzd2ctxzd2idzd2setz12za2zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6432, obj_t BgL_oz00_6433, obj_t BgL_vz00_6434)
	{
		{	/* SawBbv/bbv-types.sch 233 */
			return
				BGl_bbvzd2ctxzd2idzd2setz12zc0zzsaw_bbvzd2mergezd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_6433), (long) CINT(BgL_vz00_6434));
		}

	}



/* make-bbv-ctxentry */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxentryzd2_bglt
		BGl_makezd2bbvzd2ctxentryz00zzsaw_bbvzd2mergezd2(BgL_rtl_regz00_bglt
		BgL_reg1225z00_226, obj_t BgL_types1226z00_227,
		bool_t BgL_polarity1227z00_228, long BgL_count1228z00_229,
		obj_t BgL_value1229z00_230, obj_t BgL_aliases1230z00_231,
		obj_t BgL_initval1231z00_232)
	{
		{	/* SawBbv/bbv-types.sch 236 */
			{	/* SawBbv/bbv-types.sch 236 */
				BgL_bbvzd2ctxentryzd2_bglt BgL_new1187z00_6676;

				{	/* SawBbv/bbv-types.sch 236 */
					BgL_bbvzd2ctxentryzd2_bglt BgL_new1186z00_6677;

					BgL_new1186z00_6677 =
						((BgL_bbvzd2ctxentryzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2ctxentryzd2_bgl))));
					{	/* SawBbv/bbv-types.sch 236 */
						long BgL_arg1976z00_6678;

						BgL_arg1976z00_6678 =
							BGL_CLASS_NUM(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1186z00_6677), BgL_arg1976z00_6678);
					}
					BgL_new1187z00_6676 = BgL_new1186z00_6677;
				}
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1187z00_6676))->
						BgL_regz00) = ((BgL_rtl_regz00_bglt) BgL_reg1225z00_226), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1187z00_6676))->
						BgL_typesz00) = ((obj_t) BgL_types1226z00_227), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1187z00_6676))->
						BgL_polarityz00) = ((bool_t) BgL_polarity1227z00_228), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1187z00_6676))->
						BgL_countz00) = ((long) BgL_count1228z00_229), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1187z00_6676))->
						BgL_valuez00) = ((obj_t) BgL_value1229z00_230), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1187z00_6676))->
						BgL_aliasesz00) = ((obj_t) BgL_aliases1230z00_231), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1187z00_6676))->
						BgL_initvalz00) = ((obj_t) BgL_initval1231z00_232), BUNSPEC);
				return BgL_new1187z00_6676;
			}
		}

	}



/* &make-bbv-ctxentry */
	BgL_bbvzd2ctxentryzd2_bglt
		BGl_z62makezd2bbvzd2ctxentryz62zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6435,
		obj_t BgL_reg1225z00_6436, obj_t BgL_types1226z00_6437,
		obj_t BgL_polarity1227z00_6438, obj_t BgL_count1228z00_6439,
		obj_t BgL_value1229z00_6440, obj_t BgL_aliases1230z00_6441,
		obj_t BgL_initval1231z00_6442)
	{
		{	/* SawBbv/bbv-types.sch 236 */
			return
				BGl_makezd2bbvzd2ctxentryz00zzsaw_bbvzd2mergezd2(
				((BgL_rtl_regz00_bglt) BgL_reg1225z00_6436), BgL_types1226z00_6437,
				CBOOL(BgL_polarity1227z00_6438),
				(long) CINT(BgL_count1228z00_6439), BgL_value1229z00_6440,
				BgL_aliases1230z00_6441, BgL_initval1231z00_6442);
		}

	}



/* bbv-ctxentry? */
	BGL_EXPORTED_DEF bool_t BGl_bbvzd2ctxentryzf3z21zzsaw_bbvzd2mergezd2(obj_t
		BgL_objz00_233)
	{
		{	/* SawBbv/bbv-types.sch 237 */
			{	/* SawBbv/bbv-types.sch 237 */
				obj_t BgL_classz00_6679;

				BgL_classz00_6679 = BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_233))
					{	/* SawBbv/bbv-types.sch 237 */
						BgL_objectz00_bglt BgL_arg1807z00_6680;

						BgL_arg1807z00_6680 = (BgL_objectz00_bglt) (BgL_objz00_233);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 237 */
								long BgL_idxz00_6681;

								BgL_idxz00_6681 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6680);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_6681 + 1L)) == BgL_classz00_6679);
							}
						else
							{	/* SawBbv/bbv-types.sch 237 */
								bool_t BgL_res3262z00_6684;

								{	/* SawBbv/bbv-types.sch 237 */
									obj_t BgL_oclassz00_6685;

									{	/* SawBbv/bbv-types.sch 237 */
										obj_t BgL_arg1815z00_6686;
										long BgL_arg1816z00_6687;

										BgL_arg1815z00_6686 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 237 */
											long BgL_arg1817z00_6688;

											BgL_arg1817z00_6688 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6680);
											BgL_arg1816z00_6687 = (BgL_arg1817z00_6688 - OBJECT_TYPE);
										}
										BgL_oclassz00_6685 =
											VECTOR_REF(BgL_arg1815z00_6686, BgL_arg1816z00_6687);
									}
									{	/* SawBbv/bbv-types.sch 237 */
										bool_t BgL__ortest_1115z00_6689;

										BgL__ortest_1115z00_6689 =
											(BgL_classz00_6679 == BgL_oclassz00_6685);
										if (BgL__ortest_1115z00_6689)
											{	/* SawBbv/bbv-types.sch 237 */
												BgL_res3262z00_6684 = BgL__ortest_1115z00_6689;
											}
										else
											{	/* SawBbv/bbv-types.sch 237 */
												long BgL_odepthz00_6690;

												{	/* SawBbv/bbv-types.sch 237 */
													obj_t BgL_arg1804z00_6691;

													BgL_arg1804z00_6691 = (BgL_oclassz00_6685);
													BgL_odepthz00_6690 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_6691);
												}
												if ((1L < BgL_odepthz00_6690))
													{	/* SawBbv/bbv-types.sch 237 */
														obj_t BgL_arg1802z00_6692;

														{	/* SawBbv/bbv-types.sch 237 */
															obj_t BgL_arg1803z00_6693;

															BgL_arg1803z00_6693 = (BgL_oclassz00_6685);
															BgL_arg1802z00_6692 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6693,
																1L);
														}
														BgL_res3262z00_6684 =
															(BgL_arg1802z00_6692 == BgL_classz00_6679);
													}
												else
													{	/* SawBbv/bbv-types.sch 237 */
														BgL_res3262z00_6684 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3262z00_6684;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 237 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bbv-ctxentry? */
	obj_t BGl_z62bbvzd2ctxentryzf3z43zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6443,
		obj_t BgL_objz00_6444)
	{
		{	/* SawBbv/bbv-types.sch 237 */
			return
				BBOOL(BGl_bbvzd2ctxentryzf3z21zzsaw_bbvzd2mergezd2(BgL_objz00_6444));
		}

	}



/* bbv-ctxentry-nil */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxentryzd2_bglt
		BGl_bbvzd2ctxentryzd2nilz00zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawBbv/bbv-types.sch 238 */
			{	/* SawBbv/bbv-types.sch 238 */
				obj_t BgL_classz00_4845;

				BgL_classz00_4845 = BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 238 */
					obj_t BgL__ortest_1117z00_4846;

					BgL__ortest_1117z00_4846 = BGL_CLASS_NIL(BgL_classz00_4845);
					if (CBOOL(BgL__ortest_1117z00_4846))
						{	/* SawBbv/bbv-types.sch 238 */
							return ((BgL_bbvzd2ctxentryzd2_bglt) BgL__ortest_1117z00_4846);
						}
					else
						{	/* SawBbv/bbv-types.sch 238 */
							return
								((BgL_bbvzd2ctxentryzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4845));
						}
				}
			}
		}

	}



/* &bbv-ctxentry-nil */
	BgL_bbvzd2ctxentryzd2_bglt
		BGl_z62bbvzd2ctxentryzd2nilz62zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6445)
	{
		{	/* SawBbv/bbv-types.sch 238 */
			return BGl_bbvzd2ctxentryzd2nilz00zzsaw_bbvzd2mergezd2();
		}

	}



/* bbv-ctxentry-initval */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2initvalz00zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_234)
	{
		{	/* SawBbv/bbv-types.sch 239 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_234))->BgL_initvalz00);
		}

	}



/* &bbv-ctxentry-initval */
	obj_t BGl_z62bbvzd2ctxentryzd2initvalz62zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6446, obj_t BgL_oz00_6447)
	{
		{	/* SawBbv/bbv-types.sch 239 */
			return
				BGl_bbvzd2ctxentryzd2initvalz00zzsaw_bbvzd2mergezd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_6447));
		}

	}



/* bbv-ctxentry-initval-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2initvalzd2setz12zc0zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_235, obj_t BgL_vz00_236)
	{
		{	/* SawBbv/bbv-types.sch 240 */
			return
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_235))->
					BgL_initvalz00) = ((obj_t) BgL_vz00_236), BUNSPEC);
		}

	}



/* &bbv-ctxentry-initval-set! */
	obj_t BGl_z62bbvzd2ctxentryzd2initvalzd2setz12za2zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6448, obj_t BgL_oz00_6449, obj_t BgL_vz00_6450)
	{
		{	/* SawBbv/bbv-types.sch 240 */
			return
				BGl_bbvzd2ctxentryzd2initvalzd2setz12zc0zzsaw_bbvzd2mergezd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_6449), BgL_vz00_6450);
		}

	}



/* bbv-ctxentry-aliases */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2aliasesz00zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_237)
	{
		{	/* SawBbv/bbv-types.sch 241 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_237))->BgL_aliasesz00);
		}

	}



/* &bbv-ctxentry-aliases */
	obj_t BGl_z62bbvzd2ctxentryzd2aliasesz62zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6451, obj_t BgL_oz00_6452)
	{
		{	/* SawBbv/bbv-types.sch 241 */
			return
				BGl_bbvzd2ctxentryzd2aliasesz00zzsaw_bbvzd2mergezd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_6452));
		}

	}



/* bbv-ctxentry-aliases-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2aliaseszd2setz12zc0zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_238, obj_t BgL_vz00_239)
	{
		{	/* SawBbv/bbv-types.sch 242 */
			return
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_238))->
					BgL_aliasesz00) = ((obj_t) BgL_vz00_239), BUNSPEC);
		}

	}



/* &bbv-ctxentry-aliases-set! */
	obj_t BGl_z62bbvzd2ctxentryzd2aliaseszd2setz12za2zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6453, obj_t BgL_oz00_6454, obj_t BgL_vz00_6455)
	{
		{	/* SawBbv/bbv-types.sch 242 */
			return
				BGl_bbvzd2ctxentryzd2aliaseszd2setz12zc0zzsaw_bbvzd2mergezd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_6454), BgL_vz00_6455);
		}

	}



/* bbv-ctxentry-value */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2valuez00zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt
		BgL_oz00_240)
	{
		{	/* SawBbv/bbv-types.sch 243 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_240))->BgL_valuez00);
		}

	}



/* &bbv-ctxentry-value */
	obj_t BGl_z62bbvzd2ctxentryzd2valuez62zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6456, obj_t BgL_oz00_6457)
	{
		{	/* SawBbv/bbv-types.sch 243 */
			return
				BGl_bbvzd2ctxentryzd2valuez00zzsaw_bbvzd2mergezd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_6457));
		}

	}



/* bbv-ctxentry-count */
	BGL_EXPORTED_DEF long
		BGl_bbvzd2ctxentryzd2countz00zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt
		BgL_oz00_243)
	{
		{	/* SawBbv/bbv-types.sch 245 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_243))->BgL_countz00);
		}

	}



/* &bbv-ctxentry-count */
	obj_t BGl_z62bbvzd2ctxentryzd2countz62zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6458, obj_t BgL_oz00_6459)
	{
		{	/* SawBbv/bbv-types.sch 245 */
			return
				BINT(BGl_bbvzd2ctxentryzd2countz00zzsaw_bbvzd2mergezd2(
					((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_6459)));
		}

	}



/* bbv-ctxentry-count-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2countzd2setz12zc0zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_244, long BgL_vz00_245)
	{
		{	/* SawBbv/bbv-types.sch 246 */
			return
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_244))->BgL_countz00) =
				((long) BgL_vz00_245), BUNSPEC);
		}

	}



/* &bbv-ctxentry-count-set! */
	obj_t BGl_z62bbvzd2ctxentryzd2countzd2setz12za2zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6460, obj_t BgL_oz00_6461, obj_t BgL_vz00_6462)
	{
		{	/* SawBbv/bbv-types.sch 246 */
			return
				BGl_bbvzd2ctxentryzd2countzd2setz12zc0zzsaw_bbvzd2mergezd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_6461),
				(long) CINT(BgL_vz00_6462));
		}

	}



/* bbv-ctxentry-polarity */
	BGL_EXPORTED_DEF bool_t
		BGl_bbvzd2ctxentryzd2polarityz00zzsaw_bbvzd2mergezd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_246)
	{
		{	/* SawBbv/bbv-types.sch 247 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_246))->BgL_polarityz00);
		}

	}



/* &bbv-ctxentry-polarity */
	obj_t BGl_z62bbvzd2ctxentryzd2polarityz62zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6463, obj_t BgL_oz00_6464)
	{
		{	/* SawBbv/bbv-types.sch 247 */
			return
				BBOOL(BGl_bbvzd2ctxentryzd2polarityz00zzsaw_bbvzd2mergezd2(
					((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_6464)));
		}

	}



/* bbv-ctxentry-types */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2typesz00zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt
		BgL_oz00_249)
	{
		{	/* SawBbv/bbv-types.sch 249 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_249))->BgL_typesz00);
		}

	}



/* &bbv-ctxentry-types */
	obj_t BGl_z62bbvzd2ctxentryzd2typesz62zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6465, obj_t BgL_oz00_6466)
	{
		{	/* SawBbv/bbv-types.sch 249 */
			return
				BGl_bbvzd2ctxentryzd2typesz00zzsaw_bbvzd2mergezd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_6466));
		}

	}



/* bbv-ctxentry-reg */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_bbvzd2ctxentryzd2regz00zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt
		BgL_oz00_252)
	{
		{	/* SawBbv/bbv-types.sch 251 */
			return (((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_252))->BgL_regz00);
		}

	}



/* &bbv-ctxentry-reg */
	BgL_rtl_regz00_bglt BGl_z62bbvzd2ctxentryzd2regz62zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6467, obj_t BgL_oz00_6468)
	{
		{	/* SawBbv/bbv-types.sch 251 */
			return
				BGl_bbvzd2ctxentryzd2regz00zzsaw_bbvzd2mergezd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_6468));
		}

	}



/* bbv-block-merge */
	BGL_EXPORTED_DEF obj_t BGl_bbvzd2blockzd2mergez00zzsaw_bbvzd2mergezd2(obj_t
		BgL_bsz00_255)
	{
		{	/* SawBbv/bbv-merge.scm 48 */
			{	/* SawBbv/bbv-merge.scm 56 */
				obj_t BgL_bs1z00_2210;

				BgL_bs1z00_2210 =
					BGl_bbvzd2blockzd2mergezd2selectzd2zzsaw_bbvzd2mergezd2
					(BgL_bsz00_255);
				{	/* SawBbv/bbv-merge.scm 57 */
					obj_t BgL_bs2z00_2211;

					{	/* SawBbv/bbv-merge.scm 58 */
						obj_t BgL_tmpz00_4847;

						{	/* SawBbv/bbv-merge.scm 58 */
							int BgL_tmpz00_8062;

							BgL_tmpz00_8062 = (int) (1L);
							BgL_tmpz00_4847 = BGL_MVALUES_VAL(BgL_tmpz00_8062);
						}
						{	/* SawBbv/bbv-merge.scm 58 */
							int BgL_tmpz00_8065;

							BgL_tmpz00_8065 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_8065, BUNSPEC);
						}
						BgL_bs2z00_2211 = BgL_tmpz00_4847;
					}
					{	/* SawBbv/bbv-merge.scm 62 */
						BgL_bbvzd2ctxzd2_bglt BgL_mctxz00_2214;

						{	/* SawBbv/bbv-merge.scm 62 */
							BgL_bbvzd2ctxzd2_bglt BgL_arg1977z00_2218;
							BgL_bbvzd2ctxzd2_bglt BgL_arg1978z00_2219;

							{
								BgL_blocksz00_bglt BgL_auxz00_8068;

								{
									obj_t BgL_auxz00_8069;

									{	/* SawBbv/bbv-merge.scm 62 */
										BgL_objectz00_bglt BgL_tmpz00_8070;

										BgL_tmpz00_8070 =
											((BgL_objectz00_bglt)
											((BgL_blockz00_bglt) BgL_bs1z00_2210));
										BgL_auxz00_8069 = BGL_OBJECT_WIDENING(BgL_tmpz00_8070);
									}
									BgL_auxz00_8068 = ((BgL_blocksz00_bglt) BgL_auxz00_8069);
								}
								BgL_arg1977z00_2218 =
									(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_8068))->BgL_ctxz00);
							}
							{
								BgL_blocksz00_bglt BgL_auxz00_8076;

								{
									obj_t BgL_auxz00_8077;

									{	/* SawBbv/bbv-merge.scm 62 */
										BgL_objectz00_bglt BgL_tmpz00_8078;

										BgL_tmpz00_8078 =
											((BgL_objectz00_bglt)
											((BgL_blockz00_bglt) BgL_bs2z00_2211));
										BgL_auxz00_8077 = BGL_OBJECT_WIDENING(BgL_tmpz00_8078);
									}
									BgL_auxz00_8076 = ((BgL_blocksz00_bglt) BgL_auxz00_8077);
								}
								BgL_arg1978z00_2219 =
									(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_8076))->BgL_ctxz00);
							}
							BgL_mctxz00_2214 =
								BGl_mergezd2ctxzd2zzsaw_bbvzd2mergezd2(BgL_arg1977z00_2218,
								BgL_arg1978z00_2219);
						}
						{	/* SawBbv/bbv-merge.scm 64 */
							int BgL_tmpz00_8085;

							BgL_tmpz00_8085 = (int) (3L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8085);
						}
						{	/* SawBbv/bbv-merge.scm 64 */
							int BgL_tmpz00_8088;

							BgL_tmpz00_8088 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_8088, BgL_bs2z00_2211);
						}
						{	/* SawBbv/bbv-merge.scm 64 */
							obj_t BgL_auxz00_8093;
							int BgL_tmpz00_8091;

							BgL_auxz00_8093 = ((obj_t) BgL_mctxz00_2214);
							BgL_tmpz00_8091 = (int) (2L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_8091, BgL_auxz00_8093);
						}
						return BgL_bs1z00_2210;
					}
				}
			}
		}

	}



/* &bbv-block-merge */
	obj_t BGl_z62bbvzd2blockzd2mergez62zzsaw_bbvzd2mergezd2(obj_t BgL_envz00_6469,
		obj_t BgL_bsz00_6470)
	{
		{	/* SawBbv/bbv-merge.scm 48 */
			return BGl_bbvzd2blockzd2mergez00zzsaw_bbvzd2mergezd2(BgL_bsz00_6470);
		}

	}



/* bbv-block-merge-select */
	obj_t BGl_bbvzd2blockzd2mergezd2selectzd2zzsaw_bbvzd2mergezd2(obj_t
		BgL_bsz00_256)
	{
		{	/* SawBbv/bbv-merge.scm 71 */
			{	/* SawBbv/bbv-merge.scm 72 */
				bool_t BgL_test3499z00_8097;

				if (NULLP(BgL_bsz00_256))
					{	/* SawBbv/bbv-merge.scm 72 */
						BgL_test3499z00_8097 = ((bool_t) 0);
					}
				else
					{	/* SawBbv/bbv-merge.scm 72 */
						bool_t BgL_test3501z00_8100;

						{	/* SawBbv/bbv-merge.scm 72 */
							obj_t BgL_tmpz00_8101;

							BgL_tmpz00_8101 = CDR(BgL_bsz00_256);
							BgL_test3501z00_8100 = PAIRP(BgL_tmpz00_8101);
						}
						if (BgL_test3501z00_8100)
							{	/* SawBbv/bbv-merge.scm 72 */
								BgL_test3499z00_8097 = NULLP(CDR(CDR(BgL_bsz00_256)));
							}
						else
							{	/* SawBbv/bbv-merge.scm 72 */
								BgL_test3499z00_8097 = ((bool_t) 0);
							}
					}
				if (BgL_test3499z00_8097)
					{	/* SawBbv/bbv-merge.scm 75 */
						obj_t BgL_val0_1524z00_2226;
						obj_t BgL_val1_1525z00_2227;

						BgL_val0_1524z00_2226 = CAR(((obj_t) BgL_bsz00_256));
						{	/* SawBbv/bbv-merge.scm 75 */
							obj_t BgL_pairz00_4859;

							BgL_pairz00_4859 = CDR(((obj_t) BgL_bsz00_256));
							BgL_val1_1525z00_2227 = CAR(BgL_pairz00_4859);
						}
						{	/* SawBbv/bbv-merge.scm 75 */
							int BgL_tmpz00_8112;

							BgL_tmpz00_8112 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8112);
						}
						{	/* SawBbv/bbv-merge.scm 75 */
							int BgL_tmpz00_8115;

							BgL_tmpz00_8115 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_8115, BgL_val1_1525z00_2227);
						}
						return BgL_val0_1524z00_2226;
					}
				else
					{	/* SawBbv/bbv-merge.scm 76 */
						obj_t BgL_casezd2valuezd2_2228;

						BgL_casezd2valuezd2_2228 =
							BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2;
						if ((BgL_casezd2valuezd2_2228 == CNST_TABLE_REF(0)))
							{	/* SawBbv/bbv-merge.scm 76 */
								return
									BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2adnzd2zzsaw_bbvzd2mergezd2
									(BgL_bsz00_256);
							}
						else
							{	/* SawBbv/bbv-merge.scm 76 */
								if ((BgL_casezd2valuezd2_2228 == CNST_TABLE_REF(1)))
									{	/* SawBbv/bbv-merge.scm 76 */
										return
											BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2scoreza2z70zzsaw_bbvzd2mergezd2
											(BgL_bsz00_256);
									}
								else
									{	/* SawBbv/bbv-merge.scm 76 */
										if ((BgL_casezd2valuezd2_2228 == CNST_TABLE_REF(2)))
											{	/* SawBbv/bbv-merge.scm 76 */
												return
													BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2scorezb2z60zzsaw_bbvzd2mergezd2
													(BgL_bsz00_256);
											}
										else
											{	/* SawBbv/bbv-merge.scm 76 */
												if ((BgL_casezd2valuezd2_2228 == CNST_TABLE_REF(3)))
													{	/* SawBbv/bbv-merge.scm 76 */
														return
															BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2scorezd2z00zzsaw_bbvzd2mergezd2
															(BgL_bsz00_256);
													}
												else
													{	/* SawBbv/bbv-merge.scm 76 */
														if ((BgL_casezd2valuezd2_2228 == CNST_TABLE_REF(4)))
															{	/* SawBbv/bbv-merge.scm 76 */
																return
																	BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2score2zd2zzsaw_bbvzd2mergezd2
																	(BgL_bsz00_256);
															}
														else
															{	/* SawBbv/bbv-merge.scm 76 */
																if (
																	(BgL_casezd2valuezd2_2228 ==
																		CNST_TABLE_REF(5)))
																	{	/* SawBbv/bbv-merge.scm 76 */
																		return
																			BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2nearobjzd2zzsaw_bbvzd2mergezd2
																			(BgL_bsz00_256);
																	}
																else
																	{	/* SawBbv/bbv-merge.scm 76 */
																		if (
																			(BgL_casezd2valuezd2_2228 ==
																				CNST_TABLE_REF(6)))
																			{	/* SawBbv/bbv-merge.scm 76 */
																				return
																					BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2nearnegativezd2zzsaw_bbvzd2mergezd2
																					(BgL_bsz00_256);
																			}
																		else
																			{	/* SawBbv/bbv-merge.scm 76 */
																				if (
																					(BgL_casezd2valuezd2_2228 ==
																						CNST_TABLE_REF(7)))
																					{	/* SawBbv/bbv-merge.scm 76 */
																						return
																							BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2anynegativezd2zzsaw_bbvzd2mergezd2
																							(BgL_bsz00_256);
																					}
																				else
																					{	/* SawBbv/bbv-merge.scm 76 */
																						if (
																							(BgL_casezd2valuezd2_2228 ==
																								CNST_TABLE_REF(8)))
																							{	/* SawBbv/bbv-merge.scm 76 */
																								return
																									BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2siza7ez75zzsaw_bbvzd2mergezd2
																									(BgL_bsz00_256);
																							}
																						else
																							{	/* SawBbv/bbv-merge.scm 76 */
																								if (
																									(BgL_casezd2valuezd2_2228 ==
																										CNST_TABLE_REF(9)))
																									{	/* SawBbv/bbv-merge.scm 76 */
																										return
																											BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2distancezd2zzsaw_bbvzd2mergezd2
																											(BgL_bsz00_256);
																									}
																								else
																									{	/* SawBbv/bbv-merge.scm 76 */
																										if (
																											(BgL_casezd2valuezd2_2228
																												== CNST_TABLE_REF(10)))
																											{	/* SawBbv/bbv-merge.scm 76 */
																												return
																													BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2randomzd2zzsaw_bbvzd2mergezd2
																													(BgL_bsz00_256);
																											}
																										else
																											{	/* SawBbv/bbv-merge.scm 76 */
																												if (
																													(BgL_casezd2valuezd2_2228
																														==
																														CNST_TABLE_REF(11)))
																													{	/* SawBbv/bbv-merge.scm 76 */
																														return
																															BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2firstzd2zzsaw_bbvzd2mergezd2
																															(BgL_bsz00_256);
																													}
																												else
																													{	/* SawBbv/bbv-merge.scm 76 */
																														return
																															BGl_errorz00zz__errorz00
																															(BGl_string3282z00zzsaw_bbvzd2mergezd2,
																															BGl_string3283z00zzsaw_bbvzd2mergezd2,
																															BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2);
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* bbv-block-merge-select-strategy-adn */
	obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2adnzd2zzsaw_bbvzd2mergezd2
		(obj_t BgL_bsz00_257)
	{
		{	/* SawBbv/bbv-merge.scm 101 */
			{

				if (CBOOL(BGl_za2ADNcza2z00zzsaw_bbvzd2mergezd2))
					{	/* SawBbv/bbv-merge.scm 135 */
						BFALSE;
					}
				else
					{	/* SawBbv/bbv-merge.scm 136 */
						obj_t BgL_sz00_2439;

						BgL_sz00_2439 =
							BGl_getenvz00zz__osz00(BGl_string3284z00zzsaw_bbvzd2mergezd2);
						if (STRINGP(BgL_sz00_2439))
							{	/* SawBbv/bbv-merge.scm 138 */
								obj_t BgL_vz00_2441;

								{	/* SawBbv/bbv-merge.scm 138 */
									obj_t BgL_arg2113z00_2457;

									{	/* SawBbv/bbv-merge.scm 138 */
										obj_t BgL_l1538z00_2458;

										BgL_l1538z00_2458 =
											BGl_callzd2withzd2inputzd2stringzd2zz__r4_ports_6_10_1z00
											(BgL_sz00_2439,
											BGl_portzd2ze3sexpzd2listzd2envz31zz__readerz00);
										if (NULLP(BgL_l1538z00_2458))
											{	/* SawBbv/bbv-merge.scm 138 */
												BgL_arg2113z00_2457 = BNIL;
											}
										else
											{	/* SawBbv/bbv-merge.scm 138 */
												obj_t BgL_head1540z00_2460;

												BgL_head1540z00_2460 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												{
													obj_t BgL_l1538z00_2462;
													obj_t BgL_tail1541z00_2463;

													BgL_l1538z00_2462 = BgL_l1538z00_2458;
													BgL_tail1541z00_2463 = BgL_head1540z00_2460;
												BgL_zc3z04anonymousza32115ze3z87_2464:
													if (NULLP(BgL_l1538z00_2462))
														{	/* SawBbv/bbv-merge.scm 138 */
															BgL_arg2113z00_2457 = CDR(BgL_head1540z00_2460);
														}
													else
														{	/* SawBbv/bbv-merge.scm 138 */
															obj_t BgL_newtail1542z00_2466;

															{	/* SawBbv/bbv-merge.scm 138 */
																obj_t BgL_arg2118z00_2468;

																{	/* SawBbv/bbv-merge.scm 138 */
																	obj_t BgL_xz00_2469;

																	BgL_xz00_2469 =
																		CAR(((obj_t) BgL_l1538z00_2462));
																	if (INTEGERP(BgL_xz00_2469))
																		{	/* SawBbv/bbv-merge.scm 140 */
																			BgL_arg2118z00_2468 =
																				DOUBLE_TO_REAL(
																				(double) ((long) CINT(BgL_xz00_2469)));
																		}
																	else
																		{	/* SawBbv/bbv-merge.scm 140 */
																			if (REALP(BgL_xz00_2469))
																				{	/* SawBbv/bbv-merge.scm 141 */
																					BgL_arg2118z00_2468 = BgL_xz00_2469;
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 141 */
																					BgL_arg2118z00_2468 =
																						BGl_errorz00zz__errorz00
																						(BGl_string3285z00zzsaw_bbvzd2mergezd2,
																						BGl_string3286z00zzsaw_bbvzd2mergezd2,
																						BgL_sz00_2439);
																				}
																		}
																}
																BgL_newtail1542z00_2466 =
																	MAKE_YOUNG_PAIR(BgL_arg2118z00_2468, BNIL);
															}
															SET_CDR(BgL_tail1541z00_2463,
																BgL_newtail1542z00_2466);
															{	/* SawBbv/bbv-merge.scm 138 */
																obj_t BgL_arg2117z00_2467;

																BgL_arg2117z00_2467 =
																	CDR(((obj_t) BgL_l1538z00_2462));
																{
																	obj_t BgL_tail1541z00_8194;
																	obj_t BgL_l1538z00_8193;

																	BgL_l1538z00_8193 = BgL_arg2117z00_2467;
																	BgL_tail1541z00_8194 =
																		BgL_newtail1542z00_2466;
																	BgL_tail1541z00_2463 = BgL_tail1541z00_8194;
																	BgL_l1538z00_2462 = BgL_l1538z00_8193;
																	goto BgL_zc3z04anonymousza32115ze3z87_2464;
																}
															}
														}
												}
											}
									}
									BgL_vz00_2441 =
										BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
										(BgL_arg2113z00_2457);
								}
								{	/* SawBbv/bbv-merge.scm 146 */

									{	/* SawBbv/bbv-merge.scm 147 */
										bool_t BgL_test3520z00_8196;

										if ((VECTOR_LENGTH(BgL_vz00_2441) < 6L))
											{	/* SawBbv/bbv-merge.scm 147 */
												BgL_test3520z00_8196 = ((bool_t) 1);
											}
										else
											{	/* SawBbv/bbv-merge.scm 147 */
												bool_t BgL_test3522z00_8200;

												{	/* SawBbv/bbv-merge.scm 147 */
													long BgL_arg2112z00_2456;

													{	/* SawBbv/bbv-merge.scm 147 */
														long BgL_n1z00_4947;
														long BgL_n2z00_4948;

														BgL_n1z00_4947 = VECTOR_LENGTH(BgL_vz00_2441);
														BgL_n2z00_4948 = 3L;
														{	/* SawBbv/bbv-merge.scm 147 */
															bool_t BgL_test3523z00_8202;

															{	/* SawBbv/bbv-merge.scm 147 */
																long BgL_arg1338z00_4950;

																BgL_arg1338z00_4950 =
																	(((BgL_n1z00_4947) | (BgL_n2z00_4948)) &
																	-2147483648);
																BgL_test3523z00_8202 =
																	(BgL_arg1338z00_4950 == 0L);
															}
															if (BgL_test3523z00_8202)
																{	/* SawBbv/bbv-merge.scm 147 */
																	int32_t BgL_arg1334z00_4951;

																	{	/* SawBbv/bbv-merge.scm 147 */
																		int32_t BgL_arg1336z00_4952;
																		int32_t BgL_arg1337z00_4953;

																		BgL_arg1336z00_4952 =
																			(int32_t) (BgL_n1z00_4947);
																		BgL_arg1337z00_4953 =
																			(int32_t) (BgL_n2z00_4948);
																		BgL_arg1334z00_4951 =
																			(BgL_arg1336z00_4952 %
																			BgL_arg1337z00_4953);
																	}
																	{	/* SawBbv/bbv-merge.scm 147 */
																		long BgL_arg1446z00_4958;

																		BgL_arg1446z00_4958 =
																			(long) (BgL_arg1334z00_4951);
																		BgL_arg2112z00_2456 =
																			(long) (BgL_arg1446z00_4958);
																}}
															else
																{	/* SawBbv/bbv-merge.scm 147 */
																	BgL_arg2112z00_2456 =
																		(BgL_n1z00_4947 % BgL_n2z00_4948);
																}
														}
													}
													BgL_test3522z00_8200 = (BgL_arg2112z00_2456 == 0L);
												}
												if (BgL_test3522z00_8200)
													{	/* SawBbv/bbv-merge.scm 147 */
														BgL_test3520z00_8196 = ((bool_t) 0);
													}
												else
													{	/* SawBbv/bbv-merge.scm 147 */
														BgL_test3520z00_8196 = ((bool_t) 1);
													}
											}
										if (BgL_test3520z00_8196)
											{	/* SawBbv/bbv-merge.scm 147 */
												BGl_errorz00zz__errorz00
													(BGl_string3285z00zzsaw_bbvzd2mergezd2,
													BGl_string3286z00zzsaw_bbvzd2mergezd2, BgL_sz00_2439);
											}
										else
											{	/* SawBbv/bbv-merge.scm 147 */
												{	/* SawBbv/bbv-merge.scm 151 */
													long BgL_arg2106z00_2448;

													BgL_arg2106z00_2448 =
														(VECTOR_LENGTH(BgL_vz00_2441) / 3L);
													BGl_za2ADNcza2z00zzsaw_bbvzd2mergezd2 =
														BGl_vectorzd2copy3zd2zz__r4_vectors_6_8z00
														(BgL_vz00_2441, BINT(0L),
														BINT(BgL_arg2106z00_2448));
												}
												{	/* SawBbv/bbv-merge.scm 152 */
													long BgL_arg2107z00_2449;
													long BgL_arg2108z00_2450;

													BgL_arg2107z00_2449 =
														(VECTOR_LENGTH(BgL_vz00_2441) / 3L);
													BgL_arg2108z00_2450 =
														(2L * (VECTOR_LENGTH(BgL_vz00_2441) / 3L));
													BGl_za2ADNtza2z00zzsaw_bbvzd2mergezd2 =
														BGl_vectorzd2copy3zd2zz__r4_vectors_6_8z00
														(BgL_vz00_2441, BINT(BgL_arg2107z00_2449),
														BINT(BgL_arg2108z00_2450));
												}
												{	/* SawBbv/bbv-merge.scm 153 */
													long BgL_arg2110z00_2452;

													BgL_arg2110z00_2452 =
														(2L * (VECTOR_LENGTH(BgL_vz00_2441) / 3L));
													BGl_za2ADNsza2z00zzsaw_bbvzd2mergezd2 =
														BGl_vectorzd2copy3zd2zz__r4_vectors_6_8z00
														(BgL_vz00_2441, BINT(BgL_arg2110z00_2452),
														BINT(VECTOR_LENGTH(BgL_vz00_2441)));
												} BUNSPEC;
							}}}}
						else
							{	/* SawBbv/bbv-merge.scm 137 */
								BGl_errorz00zz__errorz00(BGl_string3285z00zzsaw_bbvzd2mergezd2,
									BGl_string3287z00zzsaw_bbvzd2mergezd2,
									BGl_string3284z00zzsaw_bbvzd2mergezd2);
							}
					}
				{	/* SawBbv/bbv-merge.scm 172 */
					obj_t BgL_cxz00_2258;

					{	/* SawBbv/bbv-merge.scm 172 */
						obj_t BgL_head1547z00_2340;

						{	/* SawBbv/bbv-merge.scm 172 */
							BgL_bbvzd2ctxzd2_bglt BgL_arg2048z00_2352;

							{	/* SawBbv/bbv-merge.scm 172 */
								BgL_blockz00_bglt BgL_oz00_4970;

								BgL_oz00_4970 = ((BgL_blockz00_bglt) CAR(BgL_bsz00_257));
								{
									BgL_blocksz00_bglt BgL_auxz00_8236;

									{
										obj_t BgL_auxz00_8237;

										{	/* SawBbv/bbv-merge.scm 172 */
											BgL_objectz00_bglt BgL_tmpz00_8238;

											BgL_tmpz00_8238 = ((BgL_objectz00_bglt) BgL_oz00_4970);
											BgL_auxz00_8237 = BGL_OBJECT_WIDENING(BgL_tmpz00_8238);
										}
										BgL_auxz00_8236 = ((BgL_blocksz00_bglt) BgL_auxz00_8237);
									}
									BgL_arg2048z00_2352 =
										(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_8236))->
										BgL_ctxz00);
								}
							}
							BgL_head1547z00_2340 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg2048z00_2352), BNIL);
						}
						{	/* SawBbv/bbv-merge.scm 172 */
							obj_t BgL_g1550z00_2341;

							BgL_g1550z00_2341 = CDR(BgL_bsz00_257);
							{
								obj_t BgL_l1545z00_2343;
								obj_t BgL_tail1548z00_2344;

								BgL_l1545z00_2343 = BgL_g1550z00_2341;
								BgL_tail1548z00_2344 = BgL_head1547z00_2340;
							BgL_zc3z04anonymousza32043ze3z87_2345:
								if (NULLP(BgL_l1545z00_2343))
									{	/* SawBbv/bbv-merge.scm 172 */
										BgL_cxz00_2258 = BgL_head1547z00_2340;
									}
								else
									{	/* SawBbv/bbv-merge.scm 172 */
										obj_t BgL_newtail1549z00_2347;

										{	/* SawBbv/bbv-merge.scm 172 */
											BgL_bbvzd2ctxzd2_bglt BgL_arg2046z00_2349;

											{	/* SawBbv/bbv-merge.scm 172 */
												BgL_blockz00_bglt BgL_oz00_4974;

												BgL_oz00_4974 =
													((BgL_blockz00_bglt)
													CAR(((obj_t) BgL_l1545z00_2343)));
												{
													BgL_blocksz00_bglt BgL_auxz00_8251;

													{
														obj_t BgL_auxz00_8252;

														{	/* SawBbv/bbv-merge.scm 172 */
															BgL_objectz00_bglt BgL_tmpz00_8253;

															BgL_tmpz00_8253 =
																((BgL_objectz00_bglt) BgL_oz00_4974);
															BgL_auxz00_8252 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_8253);
														}
														BgL_auxz00_8251 =
															((BgL_blocksz00_bglt) BgL_auxz00_8252);
													}
													BgL_arg2046z00_2349 =
														(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_8251))->
														BgL_ctxz00);
												}
											}
											BgL_newtail1549z00_2347 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg2046z00_2349), BNIL);
										}
										SET_CDR(BgL_tail1548z00_2344, BgL_newtail1549z00_2347);
										{	/* SawBbv/bbv-merge.scm 172 */
											obj_t BgL_arg2045z00_2348;

											BgL_arg2045z00_2348 = CDR(((obj_t) BgL_l1545z00_2343));
											{
												obj_t BgL_tail1548z00_8264;
												obj_t BgL_l1545z00_8263;

												BgL_l1545z00_8263 = BgL_arg2045z00_2348;
												BgL_tail1548z00_8264 = BgL_newtail1549z00_2347;
												BgL_tail1548z00_2344 = BgL_tail1548z00_8264;
												BgL_l1545z00_2343 = BgL_l1545z00_8263;
												goto BgL_zc3z04anonymousza32043ze3z87_2345;
											}
										}
									}
							}
						}
					}
					{	/* SawBbv/bbv-merge.scm 172 */
						obj_t BgL_ccz00_2259;

						BgL_ccz00_2259 = BGl_allze72ze7zzsaw_bbvzd2mergezd2(BgL_bsz00_257);
						{	/* SawBbv/bbv-merge.scm 173 */
							obj_t BgL_ccsz00_2260;

							if (NULLP(BgL_ccz00_2259))
								{	/* SawBbv/bbv-merge.scm 174 */
									BgL_ccsz00_2260 = BNIL;
								}
							else
								{	/* SawBbv/bbv-merge.scm 174 */
									obj_t BgL_head1564z00_2274;

									BgL_head1564z00_2274 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{
										obj_t BgL_l1562z00_2276;
										obj_t BgL_tail1565z00_2277;

										BgL_l1562z00_2276 = BgL_ccz00_2259;
										BgL_tail1565z00_2277 = BgL_head1564z00_2274;
									BgL_zc3z04anonymousza32008ze3z87_2278:
										if (NULLP(BgL_l1562z00_2276))
											{	/* SawBbv/bbv-merge.scm 174 */
												BgL_ccsz00_2260 = CDR(BgL_head1564z00_2274);
											}
										else
											{	/* SawBbv/bbv-merge.scm 174 */
												obj_t BgL_newtail1566z00_2280;

												{	/* SawBbv/bbv-merge.scm 174 */
													obj_t BgL_arg2011z00_2282;

													{	/* SawBbv/bbv-merge.scm 174 */
														obj_t BgL_cz00_2283;

														BgL_cz00_2283 = CAR(((obj_t) BgL_l1562z00_2276));
														{	/* SawBbv/bbv-merge.scm 175 */
															BgL_blockz00_bglt BgL_i1197z00_2284;

															BgL_i1197z00_2284 =
																((BgL_blockz00_bglt)
																CAR(((obj_t) BgL_cz00_2283)));
															{	/* SawBbv/bbv-merge.scm 176 */
																BgL_blockz00_bglt BgL_i1198z00_2285;

																BgL_i1198z00_2285 =
																	((BgL_blockz00_bglt)
																	CDR(((obj_t) BgL_cz00_2283)));
																{	/* SawBbv/bbv-merge.scm 177 */
																	BgL_bbvzd2ctxzd2_bglt BgL_nxz00_2286;

																	{	/* SawBbv/bbv-merge.scm 177 */
																		BgL_bbvzd2ctxzd2_bglt BgL_arg2040z00_2335;
																		BgL_bbvzd2ctxzd2_bglt BgL_arg2041z00_2336;

																		{
																			BgL_blocksz00_bglt BgL_auxz00_8280;

																			{
																				obj_t BgL_auxz00_8281;

																				{	/* SawBbv/bbv-merge.scm 177 */
																					BgL_objectz00_bglt BgL_tmpz00_8282;

																					BgL_tmpz00_8282 =
																						((BgL_objectz00_bglt)
																						BgL_i1197z00_2284);
																					BgL_auxz00_8281 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_8282);
																				}
																				BgL_auxz00_8280 =
																					((BgL_blocksz00_bglt)
																					BgL_auxz00_8281);
																			}
																			BgL_arg2040z00_2335 =
																				(((BgL_blocksz00_bglt)
																					COBJECT(BgL_auxz00_8280))->
																				BgL_ctxz00);
																		}
																		{
																			BgL_blocksz00_bglt BgL_auxz00_8287;

																			{
																				obj_t BgL_auxz00_8288;

																				{	/* SawBbv/bbv-merge.scm 177 */
																					BgL_objectz00_bglt BgL_tmpz00_8289;

																					BgL_tmpz00_8289 =
																						((BgL_objectz00_bglt)
																						BgL_i1198z00_2285);
																					BgL_auxz00_8288 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_8289);
																				}
																				BgL_auxz00_8287 =
																					((BgL_blocksz00_bglt)
																					BgL_auxz00_8288);
																			}
																			BgL_arg2041z00_2336 =
																				(((BgL_blocksz00_bglt)
																					COBJECT(BgL_auxz00_8287))->
																				BgL_ctxz00);
																		}
																		BgL_nxz00_2286 =
																			BGl_mergezd2ctxzd2zzsaw_bbvzd2mergezd2
																			(BgL_arg2040z00_2335,
																			BgL_arg2041z00_2336);
																	}
																	{	/* SawBbv/bbv-merge.scm 177 */
																		obj_t BgL_scorez00_2287;

																		{	/* SawBbv/bbv-merge.scm 178 */
																			obj_t BgL_runner2039z00_2334;

																			{	/* SawBbv/bbv-merge.scm 179 */
																				obj_t BgL_l1556z00_2288;

																				{	/* SawBbv/bbv-merge.scm 180 */
																					obj_t BgL_hook1555z00_2304;

																					BgL_hook1555z00_2304 =
																						MAKE_YOUNG_PAIR(BFALSE, BNIL);
																					{
																						obj_t BgL_l1552z00_2306;
																						obj_t BgL_h1553z00_2307;

																						BgL_l1552z00_2306 = BgL_cxz00_2258;
																						BgL_h1553z00_2307 =
																							BgL_hook1555z00_2304;
																					BgL_zc3z04anonymousza32020ze3z87_2308:
																						if (NULLP
																							(BgL_l1552z00_2306))
																							{	/* SawBbv/bbv-merge.scm 180 */
																								BgL_l1556z00_2288 =
																									CDR(BgL_hook1555z00_2304);
																							}
																						else
																							{	/* SawBbv/bbv-merge.scm 180 */
																								bool_t BgL_test3528z00_8299;

																								{	/* SawBbv/bbv-merge.scm 181 */
																									obj_t BgL_xz00_2327;

																									BgL_xz00_2327 =
																										CAR(
																										((obj_t)
																											BgL_l1552z00_2306));
																									{	/* SawBbv/bbv-merge.scm 181 */
																										bool_t BgL_test3529z00_8302;

																										{	/* SawBbv/bbv-merge.scm 181 */
																											bool_t
																												BgL_test3530z00_8303;
																											{	/* SawBbv/bbv-merge.scm 181 */
																												BgL_bbvzd2ctxzd2_bglt
																													BgL_arg2038z00_2332;
																												{
																													BgL_blocksz00_bglt
																														BgL_auxz00_8304;
																													{
																														obj_t
																															BgL_auxz00_8305;
																														{	/* SawBbv/bbv-merge.scm 181 */
																															BgL_objectz00_bglt
																																BgL_tmpz00_8306;
																															BgL_tmpz00_8306 =
																																(
																																(BgL_objectz00_bglt)
																																BgL_i1197z00_2284);
																															BgL_auxz00_8305 =
																																BGL_OBJECT_WIDENING
																																(BgL_tmpz00_8306);
																														}
																														BgL_auxz00_8304 =
																															(
																															(BgL_blocksz00_bglt)
																															BgL_auxz00_8305);
																													}
																													BgL_arg2038z00_2332 =
																														(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_8304))->BgL_ctxz00);
																												}
																												BgL_test3530z00_8303 =
																													(BgL_xz00_2327 ==
																													((obj_t)
																														BgL_arg2038z00_2332));
																											}
																											if (BgL_test3530z00_8303)
																												{	/* SawBbv/bbv-merge.scm 181 */
																													BgL_test3529z00_8302 =
																														((bool_t) 1);
																												}
																											else
																												{	/* SawBbv/bbv-merge.scm 182 */
																													bool_t
																														BgL_test3531z00_8313;
																													{	/* SawBbv/bbv-merge.scm 182 */
																														BgL_bbvzd2ctxzd2_bglt
																															BgL_arg2037z00_2331;
																														{
																															BgL_blocksz00_bglt
																																BgL_auxz00_8314;
																															{
																																obj_t
																																	BgL_auxz00_8315;
																																{	/* SawBbv/bbv-merge.scm 182 */
																																	BgL_objectz00_bglt
																																		BgL_tmpz00_8316;
																																	BgL_tmpz00_8316
																																		=
																																		(
																																		(BgL_objectz00_bglt)
																																		BgL_i1198z00_2285);
																																	BgL_auxz00_8315
																																		=
																																		BGL_OBJECT_WIDENING
																																		(BgL_tmpz00_8316);
																																}
																																BgL_auxz00_8314
																																	=
																																	(
																																	(BgL_blocksz00_bglt)
																																	BgL_auxz00_8315);
																															}
																															BgL_arg2037z00_2331
																																=
																																(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_8314))->BgL_ctxz00);
																														}
																														BgL_test3531z00_8313
																															=
																															(BgL_xz00_2327 ==
																															((obj_t)
																																BgL_arg2037z00_2331));
																													}
																													if (BgL_test3531z00_8313)
																														{	/* SawBbv/bbv-merge.scm 182 */
																															BgL_test3529z00_8302
																																= ((bool_t) 1);
																														}
																													else
																														{	/* SawBbv/bbv-merge.scm 182 */
																															BgL_test3529z00_8302
																																=
																																BGl_bbvzd2ctxzd2equalzf3zf3zzsaw_bbvzd2typeszd2
																																(((BgL_bbvzd2ctxzd2_bglt) BgL_xz00_2327), BgL_nxz00_2286);
																														}
																												}
																										}
																										if (BgL_test3529z00_8302)
																											{	/* SawBbv/bbv-merge.scm 181 */
																												BgL_test3528z00_8299 =
																													((bool_t) 0);
																											}
																										else
																											{	/* SawBbv/bbv-merge.scm 181 */
																												BgL_test3528z00_8299 =
																													((bool_t) 1);
																											}
																									}
																								}
																								if (BgL_test3528z00_8299)
																									{	/* SawBbv/bbv-merge.scm 180 */
																										obj_t BgL_nh1554z00_2323;

																										{	/* SawBbv/bbv-merge.scm 180 */
																											obj_t BgL_arg2034z00_2325;

																											BgL_arg2034z00_2325 =
																												CAR(
																												((obj_t)
																													BgL_l1552z00_2306));
																											BgL_nh1554z00_2323 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2034z00_2325,
																												BNIL);
																										}
																										SET_CDR(BgL_h1553z00_2307,
																											BgL_nh1554z00_2323);
																										{	/* SawBbv/bbv-merge.scm 180 */
																											obj_t BgL_arg2033z00_2324;

																											BgL_arg2033z00_2324 =
																												CDR(
																												((obj_t)
																													BgL_l1552z00_2306));
																											{
																												obj_t BgL_h1553z00_8332;
																												obj_t BgL_l1552z00_8331;

																												BgL_l1552z00_8331 =
																													BgL_arg2033z00_2324;
																												BgL_h1553z00_8332 =
																													BgL_nh1554z00_2323;
																												BgL_h1553z00_2307 =
																													BgL_h1553z00_8332;
																												BgL_l1552z00_2306 =
																													BgL_l1552z00_8331;
																												goto
																													BgL_zc3z04anonymousza32020ze3z87_2308;
																											}
																										}
																									}
																								else
																									{	/* SawBbv/bbv-merge.scm 180 */
																										obj_t BgL_arg2036z00_2326;

																										BgL_arg2036z00_2326 =
																											CDR(
																											((obj_t)
																												BgL_l1552z00_2306));
																										{
																											obj_t BgL_l1552z00_8335;

																											BgL_l1552z00_8335 =
																												BgL_arg2036z00_2326;
																											BgL_l1552z00_2306 =
																												BgL_l1552z00_8335;
																											goto
																												BgL_zc3z04anonymousza32020ze3z87_2308;
																										}
																									}
																							}
																					}
																				}
																				if (NULLP(BgL_l1556z00_2288))
																					{	/* SawBbv/bbv-merge.scm 179 */
																						BgL_runner2039z00_2334 = BNIL;
																					}
																				else
																					{	/* SawBbv/bbv-merge.scm 179 */
																						obj_t BgL_head1558z00_2290;

																						{	/* SawBbv/bbv-merge.scm 179 */
																							obj_t BgL_arg2018z00_2302;

																							{	/* SawBbv/bbv-merge.scm 179 */
																								obj_t BgL_arg2019z00_2303;

																								BgL_arg2019z00_2303 =
																									CAR(
																									((obj_t) BgL_l1556z00_2288));
																								BgL_arg2018z00_2302 =
																									BGl_ctxzd2scoreze71z35zzsaw_bbvzd2mergezd2
																									(((BgL_bbvzd2ctxzd2_bglt)
																										BgL_arg2019z00_2303));
																							}
																							BgL_head1558z00_2290 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2018z00_2302, BNIL);
																						}
																						{	/* SawBbv/bbv-merge.scm 179 */
																							obj_t BgL_g1561z00_2291;

																							BgL_g1561z00_2291 =
																								CDR(
																								((obj_t) BgL_l1556z00_2288));
																							{
																								obj_t BgL_l1556z00_2293;
																								obj_t BgL_tail1559z00_2294;

																								BgL_l1556z00_2293 =
																									BgL_g1561z00_2291;
																								BgL_tail1559z00_2294 =
																									BgL_head1558z00_2290;
																							BgL_zc3z04anonymousza32013ze3z87_2295:
																								if (NULLP
																									(BgL_l1556z00_2293))
																									{	/* SawBbv/bbv-merge.scm 179 */
																										BgL_runner2039z00_2334 =
																											BgL_head1558z00_2290;
																									}
																								else
																									{	/* SawBbv/bbv-merge.scm 179 */
																										obj_t
																											BgL_newtail1560z00_2297;
																										{	/* SawBbv/bbv-merge.scm 179 */
																											obj_t BgL_arg2016z00_2299;

																											{	/* SawBbv/bbv-merge.scm 179 */
																												obj_t
																													BgL_arg2017z00_2300;
																												BgL_arg2017z00_2300 =
																													CAR(((obj_t)
																														BgL_l1556z00_2293));
																												BgL_arg2016z00_2299 =
																													BGl_ctxzd2scoreze71z35zzsaw_bbvzd2mergezd2
																													(((BgL_bbvzd2ctxzd2_bglt) BgL_arg2017z00_2300));
																											}
																											BgL_newtail1560z00_2297 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg2016z00_2299,
																												BNIL);
																										}
																										SET_CDR
																											(BgL_tail1559z00_2294,
																											BgL_newtail1560z00_2297);
																										{	/* SawBbv/bbv-merge.scm 179 */
																											obj_t BgL_arg2015z00_2298;

																											BgL_arg2015z00_2298 =
																												CDR(
																												((obj_t)
																													BgL_l1556z00_2293));
																											{
																												obj_t
																													BgL_tail1559z00_8356;
																												obj_t BgL_l1556z00_8355;

																												BgL_l1556z00_8355 =
																													BgL_arg2015z00_2298;
																												BgL_tail1559z00_8356 =
																													BgL_newtail1560z00_2297;
																												BgL_tail1559z00_2294 =
																													BgL_tail1559z00_8356;
																												BgL_l1556z00_2293 =
																													BgL_l1556z00_8355;
																												goto
																													BgL_zc3z04anonymousza32013ze3z87_2295;
																											}
																										}
																									}
																							}
																						}
																					}
																			}
																			BgL_scorez00_2287 =
																				BGl_zb2zb2zz__r4_numbers_6_5z00
																				(BgL_runner2039z00_2334);
																		}
																		{	/* SawBbv/bbv-merge.scm 178 */

																			BgL_arg2011z00_2282 =
																				MAKE_YOUNG_PAIR(BgL_scorez00_2287,
																				BgL_cz00_2283);
																		}
																	}
																}
															}
														}
													}
													BgL_newtail1566z00_2280 =
														MAKE_YOUNG_PAIR(BgL_arg2011z00_2282, BNIL);
												}
												SET_CDR(BgL_tail1565z00_2277, BgL_newtail1566z00_2280);
												{	/* SawBbv/bbv-merge.scm 174 */
													obj_t BgL_arg2010z00_2281;

													BgL_arg2010z00_2281 =
														CDR(((obj_t) BgL_l1562z00_2276));
													{
														obj_t BgL_tail1565z00_8364;
														obj_t BgL_l1562z00_8363;

														BgL_l1562z00_8363 = BgL_arg2010z00_2281;
														BgL_tail1565z00_8364 = BgL_newtail1566z00_2280;
														BgL_tail1565z00_2277 = BgL_tail1565z00_8364;
														BgL_l1562z00_2276 = BgL_l1562z00_8363;
														goto BgL_zc3z04anonymousza32008ze3z87_2278;
													}
												}
											}
									}
								}
							{	/* SawBbv/bbv-merge.scm 174 */
								obj_t BgL_sccsz00_2261;

								BgL_sccsz00_2261 =
									BGl_sortz00zz__r4_vectors_6_8z00
									(BGl_proc3288z00zzsaw_bbvzd2mergezd2, BgL_ccsz00_2260);
								{	/* SawBbv/bbv-merge.scm 187 */
									obj_t BgL_pairz00_2262;

									{	/* SawBbv/bbv-merge.scm 190 */
										obj_t BgL_pairz00_5008;

										BgL_pairz00_5008 = CAR(((obj_t) BgL_sccsz00_2261));
										BgL_pairz00_2262 = CDR(BgL_pairz00_5008);
									}
									{	/* SawBbv/bbv-merge.scm 190 */

										{	/* SawBbv/bbv-merge.scm 194 */
											obj_t BgL_val0_1567z00_2263;
											obj_t BgL_val1_1568z00_2264;

											BgL_val0_1567z00_2263 = CAR(((obj_t) BgL_pairz00_2262));
											BgL_val1_1568z00_2264 = CDR(((obj_t) BgL_pairz00_2262));
											{	/* SawBbv/bbv-merge.scm 194 */
												int BgL_tmpz00_8373;

												BgL_tmpz00_8373 = (int) (2L);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8373);
											}
											{	/* SawBbv/bbv-merge.scm 194 */
												int BgL_tmpz00_8376;

												BgL_tmpz00_8376 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_8376,
													BgL_val1_1568z00_2264);
											}
											return BgL_val0_1567z00_2263;
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* entry-score~4 */
	obj_t BGl_entryzd2scoreze74z35zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt
		BgL_ez00_2354)
	{
		{	/* SawBbv/bbv-merge.scm 118 */
			{	/* SawBbv/bbv-merge.scm 105 */
				double BgL_tz00_2357;
				double BgL_cz00_2358;
				double BgL_sz00_2359;
				long BgL_lz00_2360;

				if (
					(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2354))->
						BgL_polarityz00))
					{	/* SawBbv/bbv-merge.scm 105 */
						BgL_tz00_2357 = ((double) 10.0);
					}
				else
					{	/* SawBbv/bbv-merge.scm 105 */
						BgL_tz00_2357 = ((double) 1.0);
					}
				BgL_cz00_2358 =
					(double) (
					(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2354))->
						BgL_countz00));
				BgL_sz00_2359 =
					(double) (BGl_entryzd2siza7ez75zzsaw_bbvzd2mergezd2(BgL_ez00_2354));
				BgL_lz00_2360 = VECTOR_LENGTH(BGl_za2ADNcza2z00zzsaw_bbvzd2mergezd2);
				{	/* SawBbv/bbv-merge.scm 109 */
					obj_t BgL_g1191z00_2361;
					obj_t BgL_g1192z00_2362;
					obj_t BgL_g1193z00_2363;

					BgL_g1191z00_2361 =
						VECTOR_REF(BGl_za2ADNcza2z00zzsaw_bbvzd2mergezd2, 0L);
					BgL_g1192z00_2362 =
						VECTOR_REF(BGl_za2ADNtza2z00zzsaw_bbvzd2mergezd2, 0L);
					BgL_g1193z00_2363 =
						VECTOR_REF(BGl_za2ADNsza2z00zzsaw_bbvzd2mergezd2, 0L);
					{
						long BgL_iz00_2365;
						obj_t BgL_csz00_2366;
						obj_t BgL_tsz00_2367;
						obj_t BgL_ssz00_2368;

						BgL_iz00_2365 = 1L;
						BgL_csz00_2366 = BgL_g1191z00_2361;
						BgL_tsz00_2367 = BgL_g1192z00_2362;
						BgL_ssz00_2368 = BgL_g1193z00_2363;
					BgL_zc3z04anonymousza32051ze3z87_2369:
						if ((BgL_iz00_2365 == BgL_lz00_2360))
							{	/* SawBbv/bbv-merge.scm 114 */
								obj_t BgL_b1526z00_2371;

								{	/* SawBbv/bbv-merge.scm 114 */
									bool_t BgL_test3536z00_8391;

									if (INTEGERP(BgL_tsz00_2367))
										{	/* SawBbv/bbv-merge.scm 114 */
											BgL_test3536z00_8391 = INTEGERP(BgL_ssz00_2368);
										}
									else
										{	/* SawBbv/bbv-merge.scm 114 */
											BgL_test3536z00_8391 = ((bool_t) 0);
										}
									if (BgL_test3536z00_8391)
										{	/* SawBbv/bbv-merge.scm 114 */
											obj_t BgL_tmpz00_4882;

											BgL_tmpz00_4882 = BINT(0L);
											if (BGL_ADDFX_OV(BgL_tsz00_2367, BgL_ssz00_2368,
													BgL_tmpz00_4882))
												{	/* SawBbv/bbv-merge.scm 114 */
													BgL_b1526z00_2371 =
														bgl_bignum_add(bgl_long_to_bignum(
															(long) CINT(BgL_tsz00_2367)),
														bgl_long_to_bignum((long) CINT(BgL_ssz00_2368)));
												}
											else
												{	/* SawBbv/bbv-merge.scm 114 */
													BgL_b1526z00_2371 = BgL_tmpz00_4882;
												}
										}
									else
										{	/* SawBbv/bbv-merge.scm 114 */
											BgL_b1526z00_2371 =
												BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_tsz00_2367,
												BgL_ssz00_2368);
										}
								}
								{	/* SawBbv/bbv-merge.scm 114 */
									bool_t BgL_test3539z00_8404;

									if (INTEGERP(BgL_csz00_2366))
										{	/* SawBbv/bbv-merge.scm 114 */
											BgL_test3539z00_8404 = INTEGERP(BgL_b1526z00_2371);
										}
									else
										{	/* SawBbv/bbv-merge.scm 114 */
											BgL_test3539z00_8404 = ((bool_t) 0);
										}
									if (BgL_test3539z00_8404)
										{	/* SawBbv/bbv-merge.scm 114 */
											obj_t BgL_tmpz00_4892;

											BgL_tmpz00_4892 = BINT(0L);
											if (BGL_ADDFX_OV(BgL_csz00_2366, BgL_b1526z00_2371,
													BgL_tmpz00_4892))
												{	/* SawBbv/bbv-merge.scm 114 */
													return
														bgl_bignum_add(bgl_long_to_bignum(
															(long) CINT(BgL_csz00_2366)),
														bgl_long_to_bignum((long) CINT(BgL_b1526z00_2371)));
												}
											else
												{	/* SawBbv/bbv-merge.scm 114 */
													return BgL_tmpz00_4892;
												}
										}
									else
										{	/* SawBbv/bbv-merge.scm 114 */
											return
												BGl_2zb2zb2zz__r4_numbers_6_5z00(BgL_csz00_2366,
												BgL_b1526z00_2371);
										}
								}
							}
						else
							{	/* SawBbv/bbv-merge.scm 115 */
								long BgL_arg2055z00_2374;
								double BgL_arg2056z00_2375;
								double BgL_arg2057z00_2376;
								double BgL_arg2058z00_2377;

								BgL_arg2055z00_2374 = (BgL_iz00_2365 + 1L);
								{	/* SawBbv/bbv-merge.scm 116 */
									double BgL_arg2059z00_2378;

									{	/* SawBbv/bbv-merge.scm 116 */
										double BgL_arg2060z00_2379;
										obj_t BgL_arg2061z00_2380;

										{	/* SawBbv/bbv-merge.scm 116 */
											double BgL_arg2062z00_2381;

											BgL_arg2062z00_2381 = (double) (BgL_iz00_2365);
											BgL_arg2060z00_2379 =
												pow(BgL_cz00_2358, BgL_arg2062z00_2381);
										}
										BgL_arg2061z00_2380 =
											VECTOR_REF(BGl_za2ADNcza2z00zzsaw_bbvzd2mergezd2,
											BgL_iz00_2365);
										BgL_arg2059z00_2378 =
											(BgL_arg2060z00_2379 *
											REAL_TO_DOUBLE(BgL_arg2061z00_2380));
									}
									BgL_arg2056z00_2375 =
										(BgL_arg2059z00_2378 + REAL_TO_DOUBLE(BgL_csz00_2366));
								}
								{	/* SawBbv/bbv-merge.scm 117 */
									double BgL_arg2063z00_2382;

									{	/* SawBbv/bbv-merge.scm 117 */
										double BgL_arg2064z00_2383;
										obj_t BgL_arg2065z00_2384;

										{	/* SawBbv/bbv-merge.scm 117 */
											double BgL_arg2067z00_2385;

											BgL_arg2067z00_2385 = (double) (BgL_iz00_2365);
											BgL_arg2064z00_2383 =
												pow(BgL_tz00_2357, BgL_arg2067z00_2385);
										}
										BgL_arg2065z00_2384 =
											VECTOR_REF(BGl_za2ADNtza2z00zzsaw_bbvzd2mergezd2,
											BgL_iz00_2365);
										BgL_arg2063z00_2382 =
											(BgL_arg2064z00_2383 *
											REAL_TO_DOUBLE(BgL_arg2065z00_2384));
									}
									BgL_arg2057z00_2376 =
										(BgL_arg2063z00_2382 + REAL_TO_DOUBLE(BgL_tsz00_2367));
								}
								{	/* SawBbv/bbv-merge.scm 118 */
									double BgL_arg2068z00_2386;

									{	/* SawBbv/bbv-merge.scm 118 */
										double BgL_arg2069z00_2387;
										obj_t BgL_arg2070z00_2388;

										{	/* SawBbv/bbv-merge.scm 118 */
											double BgL_arg2072z00_2389;

											BgL_arg2072z00_2389 = (double) (BgL_iz00_2365);
											BgL_arg2069z00_2387 =
												pow(BgL_sz00_2359, BgL_arg2072z00_2389);
										}
										BgL_arg2070z00_2388 =
											VECTOR_REF(BGl_za2ADNsza2z00zzsaw_bbvzd2mergezd2,
											BgL_iz00_2365);
										BgL_arg2068z00_2386 =
											(BgL_arg2069z00_2387 *
											REAL_TO_DOUBLE(BgL_arg2070z00_2388));
									}
									BgL_arg2058z00_2377 =
										(BgL_arg2068z00_2386 + REAL_TO_DOUBLE(BgL_ssz00_2368));
								}
								{
									obj_t BgL_ssz00_8444;
									obj_t BgL_tsz00_8442;
									obj_t BgL_csz00_8440;
									long BgL_iz00_8439;

									BgL_iz00_8439 = BgL_arg2055z00_2374;
									BgL_csz00_8440 = DOUBLE_TO_REAL(BgL_arg2056z00_2375);
									BgL_tsz00_8442 = DOUBLE_TO_REAL(BgL_arg2057z00_2376);
									BgL_ssz00_8444 = DOUBLE_TO_REAL(BgL_arg2058z00_2377);
									BgL_ssz00_2368 = BgL_ssz00_8444;
									BgL_tsz00_2367 = BgL_tsz00_8442;
									BgL_csz00_2366 = BgL_csz00_8440;
									BgL_iz00_2365 = BgL_iz00_8439;
									goto BgL_zc3z04anonymousza32051ze3z87_2369;
								}
							}
					}
				}
			}
		}

	}



/* all~2 */
	obj_t BGl_allze72ze7zzsaw_bbvzd2mergezd2(obj_t BgL_lz00_2418)
	{
		{	/* SawBbv/bbv-merge.scm 132 */
			if (NULLP(BgL_lz00_2418))
				{	/* SawBbv/bbv-merge.scm 129 */
					return BNIL;
				}
			else
				{	/* SawBbv/bbv-merge.scm 131 */
					obj_t BgL_arg2090z00_2421;
					obj_t BgL_arg2091z00_2422;

					{	/* SawBbv/bbv-merge.scm 131 */
						obj_t BgL_l1533z00_2423;

						BgL_l1533z00_2423 = CDR(((obj_t) BgL_lz00_2418));
						if (NULLP(BgL_l1533z00_2423))
							{	/* SawBbv/bbv-merge.scm 131 */
								BgL_arg2090z00_2421 = BNIL;
							}
						else
							{	/* SawBbv/bbv-merge.scm 131 */
								obj_t BgL_head1535z00_2425;

								BgL_head1535z00_2425 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1533z00_2427;
									obj_t BgL_tail1536z00_2428;

									BgL_l1533z00_2427 = BgL_l1533z00_2423;
									BgL_tail1536z00_2428 = BgL_head1535z00_2425;
								BgL_zc3z04anonymousza32093ze3z87_2429:
									if (NULLP(BgL_l1533z00_2427))
										{	/* SawBbv/bbv-merge.scm 131 */
											BgL_arg2090z00_2421 = CDR(BgL_head1535z00_2425);
										}
									else
										{	/* SawBbv/bbv-merge.scm 131 */
											obj_t BgL_newtail1537z00_2431;

											{	/* SawBbv/bbv-merge.scm 131 */
												obj_t BgL_arg2096z00_2433;

												{	/* SawBbv/bbv-merge.scm 131 */
													obj_t BgL_ez00_2434;

													BgL_ez00_2434 = CAR(((obj_t) BgL_l1533z00_2427));
													{	/* SawBbv/bbv-merge.scm 131 */
														obj_t BgL_arg2097z00_2435;

														BgL_arg2097z00_2435 = CAR(((obj_t) BgL_lz00_2418));
														BgL_arg2096z00_2433 =
															MAKE_YOUNG_PAIR(BgL_arg2097z00_2435,
															BgL_ez00_2434);
													}
												}
												BgL_newtail1537z00_2431 =
													MAKE_YOUNG_PAIR(BgL_arg2096z00_2433, BNIL);
											}
											SET_CDR(BgL_tail1536z00_2428, BgL_newtail1537z00_2431);
											{	/* SawBbv/bbv-merge.scm 131 */
												obj_t BgL_arg2095z00_2432;

												BgL_arg2095z00_2432 = CDR(((obj_t) BgL_l1533z00_2427));
												{
													obj_t BgL_tail1536z00_8466;
													obj_t BgL_l1533z00_8465;

													BgL_l1533z00_8465 = BgL_arg2095z00_2432;
													BgL_tail1536z00_8466 = BgL_newtail1537z00_2431;
													BgL_tail1536z00_2428 = BgL_tail1536z00_8466;
													BgL_l1533z00_2427 = BgL_l1533z00_8465;
													goto BgL_zc3z04anonymousza32093ze3z87_2429;
												}
											}
										}
								}
							}
					}
					{	/* SawBbv/bbv-merge.scm 132 */
						obj_t BgL_arg2098z00_2437;

						BgL_arg2098z00_2437 = CDR(((obj_t) BgL_lz00_2418));
						BgL_arg2091z00_2422 =
							BGl_allze72ze7zzsaw_bbvzd2mergezd2(BgL_arg2098z00_2437);
					}
					return
						BGl_appendzd221011zd2zzsaw_bbvzd2mergezd2(BgL_arg2090z00_2421,
						BgL_arg2091z00_2422);
				}
		}

	}



/* ctx-score~1 */
	obj_t BGl_ctxzd2scoreze71z35zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxzd2_bglt
		BgL_ctxz00_2394)
	{
		{	/* SawBbv/bbv-merge.scm 122 */
			{	/* SawBbv/bbv-merge.scm 122 */
				obj_t BgL_runner2085z00_2413;

				{	/* SawBbv/bbv-merge.scm 122 */
					obj_t BgL_l1527z00_2397;

					BgL_l1527z00_2397 =
						(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_2394))->
						BgL_entriesz00);
					if (NULLP(BgL_l1527z00_2397))
						{	/* SawBbv/bbv-merge.scm 122 */
							BgL_runner2085z00_2413 = BNIL;
						}
					else
						{	/* SawBbv/bbv-merge.scm 122 */
							obj_t BgL_head1529z00_2399;

							{	/* SawBbv/bbv-merge.scm 122 */
								obj_t BgL_arg2083z00_2411;

								{	/* SawBbv/bbv-merge.scm 122 */
									obj_t BgL_arg2084z00_2412;

									BgL_arg2084z00_2412 = CAR(BgL_l1527z00_2397);
									BgL_arg2083z00_2411 =
										BGl_entryzd2scoreze74z35zzsaw_bbvzd2mergezd2(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_arg2084z00_2412));
								}
								BgL_head1529z00_2399 =
									MAKE_YOUNG_PAIR(BgL_arg2083z00_2411, BNIL);
							}
							{	/* SawBbv/bbv-merge.scm 122 */
								obj_t BgL_g1532z00_2400;

								BgL_g1532z00_2400 = CDR(BgL_l1527z00_2397);
								{
									obj_t BgL_l1527z00_2402;
									obj_t BgL_tail1530z00_2403;

									BgL_l1527z00_2402 = BgL_g1532z00_2400;
									BgL_tail1530z00_2403 = BgL_head1529z00_2399;
								BgL_zc3z04anonymousza32078ze3z87_2404:
									if (NULLP(BgL_l1527z00_2402))
										{	/* SawBbv/bbv-merge.scm 122 */
											BgL_runner2085z00_2413 = BgL_head1529z00_2399;
										}
									else
										{	/* SawBbv/bbv-merge.scm 122 */
											obj_t BgL_newtail1531z00_2406;

											{	/* SawBbv/bbv-merge.scm 122 */
												obj_t BgL_arg2081z00_2408;

												{	/* SawBbv/bbv-merge.scm 122 */
													obj_t BgL_arg2082z00_2409;

													BgL_arg2082z00_2409 =
														CAR(((obj_t) BgL_l1527z00_2402));
													BgL_arg2081z00_2408 =
														BGl_entryzd2scoreze74z35zzsaw_bbvzd2mergezd2(
														((BgL_bbvzd2ctxentryzd2_bglt) BgL_arg2082z00_2409));
												}
												BgL_newtail1531z00_2406 =
													MAKE_YOUNG_PAIR(BgL_arg2081z00_2408, BNIL);
											}
											SET_CDR(BgL_tail1530z00_2403, BgL_newtail1531z00_2406);
											{	/* SawBbv/bbv-merge.scm 122 */
												obj_t BgL_arg2080z00_2407;

												BgL_arg2080z00_2407 = CDR(((obj_t) BgL_l1527z00_2402));
												{
													obj_t BgL_tail1530z00_8490;
													obj_t BgL_l1527z00_8489;

													BgL_l1527z00_8489 = BgL_arg2080z00_2407;
													BgL_tail1530z00_8490 = BgL_newtail1531z00_2406;
													BgL_tail1530z00_2403 = BgL_tail1530z00_8490;
													BgL_l1527z00_2402 = BgL_l1527z00_8489;
													goto BgL_zc3z04anonymousza32078ze3z87_2404;
												}
											}
										}
								}
							}
						}
				}
				return BGl_zb2zb2zz__r4_numbers_6_5z00(BgL_runner2085z00_2413);
			}
		}

	}



/* &<@anonymous:2003> */
	obj_t BGl_z62zc3z04anonymousza32003ze3ze5zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6472, obj_t BgL_xz00_6473, obj_t BgL_yz00_6474)
	{
		{	/* SawBbv/bbv-merge.scm 187 */
			return
				BBOOL(
				(REAL_TO_DOUBLE(CAR(
							((obj_t) BgL_xz00_6473))) >=
					REAL_TO_DOUBLE(CAR(((obj_t) BgL_yz00_6474)))));
		}

	}



/* bbv-block-merge-select-strategy-score* */
	obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2scoreza2z70zzsaw_bbvzd2mergezd2
		(obj_t BgL_bsz00_258)
	{
		{	/* SawBbv/bbv-merge.scm 202 */
			{	/* SawBbv/bbv-merge.scm 232 */
				obj_t BgL_ccz00_2490;

				BgL_ccz00_2490 = BGl_allze71ze7zzsaw_bbvzd2mergezd2(BgL_bsz00_258);
				{	/* SawBbv/bbv-merge.scm 232 */
					obj_t BgL_cxz00_2491;

					{	/* SawBbv/bbv-merge.scm 233 */
						obj_t BgL_head1584z00_2572;

						{	/* SawBbv/bbv-merge.scm 233 */
							BgL_bbvzd2ctxzd2_bglt BgL_arg2168z00_2584;

							{	/* SawBbv/bbv-merge.scm 233 */
								BgL_blockz00_bglt BgL_oz00_5026;

								BgL_oz00_5026 = ((BgL_blockz00_bglt) CAR(BgL_bsz00_258));
								{
									BgL_blocksz00_bglt BgL_auxz00_8503;

									{
										obj_t BgL_auxz00_8504;

										{	/* SawBbv/bbv-merge.scm 233 */
											BgL_objectz00_bglt BgL_tmpz00_8505;

											BgL_tmpz00_8505 = ((BgL_objectz00_bglt) BgL_oz00_5026);
											BgL_auxz00_8504 = BGL_OBJECT_WIDENING(BgL_tmpz00_8505);
										}
										BgL_auxz00_8503 = ((BgL_blocksz00_bglt) BgL_auxz00_8504);
									}
									BgL_arg2168z00_2584 =
										(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_8503))->
										BgL_ctxz00);
								}
							}
							BgL_head1584z00_2572 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg2168z00_2584), BNIL);
						}
						{	/* SawBbv/bbv-merge.scm 233 */
							obj_t BgL_g1587z00_2573;

							BgL_g1587z00_2573 = CDR(BgL_bsz00_258);
							{
								obj_t BgL_l1582z00_2575;
								obj_t BgL_tail1585z00_2576;

								BgL_l1582z00_2575 = BgL_g1587z00_2573;
								BgL_tail1585z00_2576 = BgL_head1584z00_2572;
							BgL_zc3z04anonymousza32163ze3z87_2577:
								if (NULLP(BgL_l1582z00_2575))
									{	/* SawBbv/bbv-merge.scm 233 */
										BgL_cxz00_2491 = BgL_head1584z00_2572;
									}
								else
									{	/* SawBbv/bbv-merge.scm 233 */
										obj_t BgL_newtail1586z00_2579;

										{	/* SawBbv/bbv-merge.scm 233 */
											BgL_bbvzd2ctxzd2_bglt BgL_arg2166z00_2581;

											{	/* SawBbv/bbv-merge.scm 233 */
												BgL_blockz00_bglt BgL_oz00_5030;

												BgL_oz00_5030 =
													((BgL_blockz00_bglt)
													CAR(((obj_t) BgL_l1582z00_2575)));
												{
													BgL_blocksz00_bglt BgL_auxz00_8518;

													{
														obj_t BgL_auxz00_8519;

														{	/* SawBbv/bbv-merge.scm 233 */
															BgL_objectz00_bglt BgL_tmpz00_8520;

															BgL_tmpz00_8520 =
																((BgL_objectz00_bglt) BgL_oz00_5030);
															BgL_auxz00_8519 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_8520);
														}
														BgL_auxz00_8518 =
															((BgL_blocksz00_bglt) BgL_auxz00_8519);
													}
													BgL_arg2166z00_2581 =
														(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_8518))->
														BgL_ctxz00);
												}
											}
											BgL_newtail1586z00_2579 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg2166z00_2581), BNIL);
										}
										SET_CDR(BgL_tail1585z00_2576, BgL_newtail1586z00_2579);
										{	/* SawBbv/bbv-merge.scm 233 */
											obj_t BgL_arg2165z00_2580;

											BgL_arg2165z00_2580 = CDR(((obj_t) BgL_l1582z00_2575));
											{
												obj_t BgL_tail1585z00_8531;
												obj_t BgL_l1582z00_8530;

												BgL_l1582z00_8530 = BgL_arg2165z00_2580;
												BgL_tail1585z00_8531 = BgL_newtail1586z00_2579;
												BgL_tail1585z00_2576 = BgL_tail1585z00_8531;
												BgL_l1582z00_2575 = BgL_l1582z00_8530;
												goto BgL_zc3z04anonymousza32163ze3z87_2577;
											}
										}
									}
							}
						}
					}
					{	/* SawBbv/bbv-merge.scm 233 */
						obj_t BgL_ccsz00_2492;

						if (NULLP(BgL_ccz00_2490))
							{	/* SawBbv/bbv-merge.scm 234 */
								BgL_ccsz00_2492 = BNIL;
							}
						else
							{	/* SawBbv/bbv-merge.scm 234 */
								obj_t BgL_head1602z00_2506;

								BgL_head1602z00_2506 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1600z00_2508;
									obj_t BgL_tail1603z00_2509;

									BgL_l1600z00_2508 = BgL_ccz00_2490;
									BgL_tail1603z00_2509 = BgL_head1602z00_2506;
								BgL_zc3z04anonymousza32129ze3z87_2510:
									if (NULLP(BgL_l1600z00_2508))
										{	/* SawBbv/bbv-merge.scm 234 */
											BgL_ccsz00_2492 = CDR(BgL_head1602z00_2506);
										}
									else
										{	/* SawBbv/bbv-merge.scm 234 */
											obj_t BgL_newtail1604z00_2512;

											{	/* SawBbv/bbv-merge.scm 234 */
												obj_t BgL_arg2132z00_2514;

												{	/* SawBbv/bbv-merge.scm 234 */
													obj_t BgL_cz00_2515;

													BgL_cz00_2515 = CAR(((obj_t) BgL_l1600z00_2508));
													{	/* SawBbv/bbv-merge.scm 235 */
														BgL_blockz00_bglt BgL_i1203z00_2516;

														BgL_i1203z00_2516 =
															((BgL_blockz00_bglt)
															CAR(((obj_t) BgL_cz00_2515)));
														{	/* SawBbv/bbv-merge.scm 236 */
															BgL_blockz00_bglt BgL_i1204z00_2517;

															BgL_i1204z00_2517 =
																((BgL_blockz00_bglt)
																CDR(((obj_t) BgL_cz00_2515)));
															{	/* SawBbv/bbv-merge.scm 237 */
																BgL_bbvzd2ctxzd2_bglt BgL_nxz00_2518;

																{	/* SawBbv/bbv-merge.scm 237 */
																	BgL_bbvzd2ctxzd2_bglt BgL_arg2160z00_2567;
																	BgL_bbvzd2ctxzd2_bglt BgL_arg2161z00_2568;

																	{
																		BgL_blocksz00_bglt BgL_auxz00_8546;

																		{
																			obj_t BgL_auxz00_8547;

																			{	/* SawBbv/bbv-merge.scm 237 */
																				BgL_objectz00_bglt BgL_tmpz00_8548;

																				BgL_tmpz00_8548 =
																					((BgL_objectz00_bglt)
																					BgL_i1203z00_2516);
																				BgL_auxz00_8547 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_8548);
																			}
																			BgL_auxz00_8546 =
																				((BgL_blocksz00_bglt) BgL_auxz00_8547);
																		}
																		BgL_arg2160z00_2567 =
																			(((BgL_blocksz00_bglt)
																				COBJECT(BgL_auxz00_8546))->BgL_ctxz00);
																	}
																	{
																		BgL_blocksz00_bglt BgL_auxz00_8553;

																		{
																			obj_t BgL_auxz00_8554;

																			{	/* SawBbv/bbv-merge.scm 237 */
																				BgL_objectz00_bglt BgL_tmpz00_8555;

																				BgL_tmpz00_8555 =
																					((BgL_objectz00_bglt)
																					BgL_i1204z00_2517);
																				BgL_auxz00_8554 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_8555);
																			}
																			BgL_auxz00_8553 =
																				((BgL_blocksz00_bglt) BgL_auxz00_8554);
																		}
																		BgL_arg2161z00_2568 =
																			(((BgL_blocksz00_bglt)
																				COBJECT(BgL_auxz00_8553))->BgL_ctxz00);
																	}
																	BgL_nxz00_2518 =
																		BGl_mergezd2ctxzd2zzsaw_bbvzd2mergezd2
																		(BgL_arg2160z00_2567, BgL_arg2161z00_2568);
																}
																{	/* SawBbv/bbv-merge.scm 237 */
																	obj_t BgL_scorez00_2519;

																	{	/* SawBbv/bbv-merge.scm 238 */
																		obj_t BgL_runner2159z00_2566;

																		{	/* SawBbv/bbv-merge.scm 239 */
																			obj_t BgL_l1594z00_2520;

																			{	/* SawBbv/bbv-merge.scm 240 */
																				obj_t BgL_hook1593z00_2536;

																				BgL_hook1593z00_2536 =
																					MAKE_YOUNG_PAIR(BFALSE, BNIL);
																				{
																					obj_t BgL_l1590z00_2538;
																					obj_t BgL_h1591z00_2539;

																					BgL_l1590z00_2538 = BgL_cxz00_2491;
																					BgL_h1591z00_2539 =
																						BgL_hook1593z00_2536;
																				BgL_zc3z04anonymousza32142ze3z87_2540:
																					if (NULLP(BgL_l1590z00_2538))
																						{	/* SawBbv/bbv-merge.scm 240 */
																							BgL_l1594z00_2520 =
																								CDR(BgL_hook1593z00_2536);
																						}
																					else
																						{	/* SawBbv/bbv-merge.scm 240 */
																							bool_t BgL_test3551z00_8565;

																							{	/* SawBbv/bbv-merge.scm 241 */
																								obj_t BgL_xz00_2559;

																								BgL_xz00_2559 =
																									CAR(
																									((obj_t) BgL_l1590z00_2538));
																								{	/* SawBbv/bbv-merge.scm 241 */
																									bool_t BgL_test3552z00_8568;

																									{	/* SawBbv/bbv-merge.scm 241 */
																										bool_t BgL_test3553z00_8569;

																										{	/* SawBbv/bbv-merge.scm 241 */
																											BgL_bbvzd2ctxzd2_bglt
																												BgL_arg2158z00_2564;
																											{
																												BgL_blocksz00_bglt
																													BgL_auxz00_8570;
																												{
																													obj_t BgL_auxz00_8571;

																													{	/* SawBbv/bbv-merge.scm 241 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_8572;
																														BgL_tmpz00_8572 =
																															(
																															(BgL_objectz00_bglt)
																															BgL_i1203z00_2516);
																														BgL_auxz00_8571 =
																															BGL_OBJECT_WIDENING
																															(BgL_tmpz00_8572);
																													}
																													BgL_auxz00_8570 =
																														(
																														(BgL_blocksz00_bglt)
																														BgL_auxz00_8571);
																												}
																												BgL_arg2158z00_2564 =
																													(((BgL_blocksz00_bglt)
																														COBJECT
																														(BgL_auxz00_8570))->
																													BgL_ctxz00);
																											}
																											BgL_test3553z00_8569 =
																												(BgL_xz00_2559 ==
																												((obj_t)
																													BgL_arg2158z00_2564));
																										}
																										if (BgL_test3553z00_8569)
																											{	/* SawBbv/bbv-merge.scm 241 */
																												BgL_test3552z00_8568 =
																													((bool_t) 1);
																											}
																										else
																											{	/* SawBbv/bbv-merge.scm 242 */
																												bool_t
																													BgL_test3554z00_8579;
																												{	/* SawBbv/bbv-merge.scm 242 */
																													BgL_bbvzd2ctxzd2_bglt
																														BgL_arg2157z00_2563;
																													{
																														BgL_blocksz00_bglt
																															BgL_auxz00_8580;
																														{
																															obj_t
																																BgL_auxz00_8581;
																															{	/* SawBbv/bbv-merge.scm 242 */
																																BgL_objectz00_bglt
																																	BgL_tmpz00_8582;
																																BgL_tmpz00_8582
																																	=
																																	(
																																	(BgL_objectz00_bglt)
																																	BgL_i1204z00_2517);
																																BgL_auxz00_8581
																																	=
																																	BGL_OBJECT_WIDENING
																																	(BgL_tmpz00_8582);
																															}
																															BgL_auxz00_8580 =
																																(
																																(BgL_blocksz00_bglt)
																																BgL_auxz00_8581);
																														}
																														BgL_arg2157z00_2563
																															=
																															(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_8580))->BgL_ctxz00);
																													}
																													BgL_test3554z00_8579 =
																														(BgL_xz00_2559 ==
																														((obj_t)
																															BgL_arg2157z00_2563));
																												}
																												if (BgL_test3554z00_8579)
																													{	/* SawBbv/bbv-merge.scm 242 */
																														BgL_test3552z00_8568
																															= ((bool_t) 1);
																													}
																												else
																													{	/* SawBbv/bbv-merge.scm 242 */
																														BgL_test3552z00_8568
																															=
																															BGl_bbvzd2ctxzd2equalzf3zf3zzsaw_bbvzd2typeszd2
																															(((BgL_bbvzd2ctxzd2_bglt) BgL_xz00_2559), BgL_nxz00_2518);
																													}
																											}
																									}
																									if (BgL_test3552z00_8568)
																										{	/* SawBbv/bbv-merge.scm 241 */
																											BgL_test3551z00_8565 =
																												((bool_t) 0);
																										}
																									else
																										{	/* SawBbv/bbv-merge.scm 241 */
																											BgL_test3551z00_8565 =
																												((bool_t) 1);
																										}
																								}
																							}
																							if (BgL_test3551z00_8565)
																								{	/* SawBbv/bbv-merge.scm 240 */
																									obj_t BgL_nh1592z00_2555;

																									{	/* SawBbv/bbv-merge.scm 240 */
																										obj_t BgL_arg2155z00_2557;

																										BgL_arg2155z00_2557 =
																											CAR(
																											((obj_t)
																												BgL_l1590z00_2538));
																										BgL_nh1592z00_2555 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2155z00_2557,
																											BNIL);
																									}
																									SET_CDR(BgL_h1591z00_2539,
																										BgL_nh1592z00_2555);
																									{	/* SawBbv/bbv-merge.scm 240 */
																										obj_t BgL_arg2154z00_2556;

																										BgL_arg2154z00_2556 =
																											CDR(
																											((obj_t)
																												BgL_l1590z00_2538));
																										{
																											obj_t BgL_h1591z00_8598;
																											obj_t BgL_l1590z00_8597;

																											BgL_l1590z00_8597 =
																												BgL_arg2154z00_2556;
																											BgL_h1591z00_8598 =
																												BgL_nh1592z00_2555;
																											BgL_h1591z00_2539 =
																												BgL_h1591z00_8598;
																											BgL_l1590z00_2538 =
																												BgL_l1590z00_8597;
																											goto
																												BgL_zc3z04anonymousza32142ze3z87_2540;
																										}
																									}
																								}
																							else
																								{	/* SawBbv/bbv-merge.scm 240 */
																									obj_t BgL_arg2156z00_2558;

																									BgL_arg2156z00_2558 =
																										CDR(
																										((obj_t)
																											BgL_l1590z00_2538));
																									{
																										obj_t BgL_l1590z00_8601;

																										BgL_l1590z00_8601 =
																											BgL_arg2156z00_2558;
																										BgL_l1590z00_2538 =
																											BgL_l1590z00_8601;
																										goto
																											BgL_zc3z04anonymousza32142ze3z87_2540;
																									}
																								}
																						}
																				}
																			}
																			if (NULLP(BgL_l1594z00_2520))
																				{	/* SawBbv/bbv-merge.scm 239 */
																					BgL_runner2159z00_2566 = BNIL;
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 239 */
																					obj_t BgL_head1596z00_2522;

																					{	/* SawBbv/bbv-merge.scm 239 */
																						obj_t BgL_arg2139z00_2534;

																						{	/* SawBbv/bbv-merge.scm 239 */
																							obj_t BgL_arg2141z00_2535;

																							BgL_arg2141z00_2535 =
																								CAR(
																								((obj_t) BgL_l1594z00_2520));
																							BgL_arg2139z00_2534 =
																								BGl_ctxzd2scoreze70z35zzsaw_bbvzd2mergezd2
																								(((BgL_bbvzd2ctxzd2_bglt)
																									BgL_arg2141z00_2535));
																						}
																						BgL_head1596z00_2522 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2139z00_2534, BNIL);
																					}
																					{	/* SawBbv/bbv-merge.scm 239 */
																						obj_t BgL_g1599z00_2523;

																						BgL_g1599z00_2523 =
																							CDR(((obj_t) BgL_l1594z00_2520));
																						{
																							obj_t BgL_l1594z00_2525;
																							obj_t BgL_tail1597z00_2526;

																							BgL_l1594z00_2525 =
																								BgL_g1599z00_2523;
																							BgL_tail1597z00_2526 =
																								BgL_head1596z00_2522;
																						BgL_zc3z04anonymousza32134ze3z87_2527:
																							if (NULLP
																								(BgL_l1594z00_2525))
																								{	/* SawBbv/bbv-merge.scm 239 */
																									BgL_runner2159z00_2566 =
																										BgL_head1596z00_2522;
																								}
																							else
																								{	/* SawBbv/bbv-merge.scm 239 */
																									obj_t BgL_newtail1598z00_2529;

																									{	/* SawBbv/bbv-merge.scm 239 */
																										obj_t BgL_arg2137z00_2531;

																										{	/* SawBbv/bbv-merge.scm 239 */
																											obj_t BgL_arg2138z00_2532;

																											BgL_arg2138z00_2532 =
																												CAR(
																												((obj_t)
																													BgL_l1594z00_2525));
																											BgL_arg2137z00_2531 =
																												BGl_ctxzd2scoreze70z35zzsaw_bbvzd2mergezd2
																												(((BgL_bbvzd2ctxzd2_bglt) BgL_arg2138z00_2532));
																										}
																										BgL_newtail1598z00_2529 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2137z00_2531,
																											BNIL);
																									}
																									SET_CDR(BgL_tail1597z00_2526,
																										BgL_newtail1598z00_2529);
																									{	/* SawBbv/bbv-merge.scm 239 */
																										obj_t BgL_arg2136z00_2530;

																										BgL_arg2136z00_2530 =
																											CDR(
																											((obj_t)
																												BgL_l1594z00_2525));
																										{
																											obj_t
																												BgL_tail1597z00_8622;
																											obj_t BgL_l1594z00_8621;

																											BgL_l1594z00_8621 =
																												BgL_arg2136z00_2530;
																											BgL_tail1597z00_8622 =
																												BgL_newtail1598z00_2529;
																											BgL_tail1597z00_2526 =
																												BgL_tail1597z00_8622;
																											BgL_l1594z00_2525 =
																												BgL_l1594z00_8621;
																											goto
																												BgL_zc3z04anonymousza32134ze3z87_2527;
																										}
																									}
																								}
																						}
																					}
																				}
																		}
																		BgL_scorez00_2519 =
																			BGl_zb2zb2zz__r4_numbers_6_5z00
																			(BgL_runner2159z00_2566);
																	}
																	{	/* SawBbv/bbv-merge.scm 238 */

																		BgL_arg2132z00_2514 =
																			MAKE_YOUNG_PAIR(BgL_scorez00_2519,
																			BgL_cz00_2515);
																	}
																}
															}
														}
													}
												}
												BgL_newtail1604z00_2512 =
													MAKE_YOUNG_PAIR(BgL_arg2132z00_2514, BNIL);
											}
											SET_CDR(BgL_tail1603z00_2509, BgL_newtail1604z00_2512);
											{	/* SawBbv/bbv-merge.scm 234 */
												obj_t BgL_arg2131z00_2513;

												BgL_arg2131z00_2513 = CDR(((obj_t) BgL_l1600z00_2508));
												{
													obj_t BgL_tail1603z00_8630;
													obj_t BgL_l1600z00_8629;

													BgL_l1600z00_8629 = BgL_arg2131z00_2513;
													BgL_tail1603z00_8630 = BgL_newtail1604z00_2512;
													BgL_tail1603z00_2509 = BgL_tail1603z00_8630;
													BgL_l1600z00_2508 = BgL_l1600z00_8629;
													goto BgL_zc3z04anonymousza32129ze3z87_2510;
												}
											}
										}
								}
							}
						{	/* SawBbv/bbv-merge.scm 234 */
							obj_t BgL_sccsz00_2493;

							BgL_sccsz00_2493 =
								BGl_sortz00zz__r4_vectors_6_8z00
								(BGl_proc3289z00zzsaw_bbvzd2mergezd2, BgL_ccsz00_2492);
							{	/* SawBbv/bbv-merge.scm 247 */
								obj_t BgL_pairz00_2494;

								{	/* SawBbv/bbv-merge.scm 250 */
									obj_t BgL_pairz00_5064;

									BgL_pairz00_5064 = CAR(((obj_t) BgL_sccsz00_2493));
									BgL_pairz00_2494 = CDR(BgL_pairz00_5064);
								}
								{	/* SawBbv/bbv-merge.scm 250 */

									{	/* SawBbv/bbv-merge.scm 254 */
										obj_t BgL_val0_1605z00_2495;
										obj_t BgL_val1_1606z00_2496;

										BgL_val0_1605z00_2495 = CAR(((obj_t) BgL_pairz00_2494));
										BgL_val1_1606z00_2496 = CDR(((obj_t) BgL_pairz00_2494));
										{	/* SawBbv/bbv-merge.scm 254 */
											int BgL_tmpz00_8639;

											BgL_tmpz00_8639 = (int) (2L);
											BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8639);
										}
										{	/* SawBbv/bbv-merge.scm 254 */
											int BgL_tmpz00_8642;

											BgL_tmpz00_8642 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_8642,
												BgL_val1_1606z00_2496);
										}
										return BgL_val0_1605z00_2495;
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* entry-score~3 */
	long BGl_entryzd2scoreze73z35zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt
		BgL_ez00_2586)
	{
		{	/* SawBbv/bbv-merge.scm 209 */
			if (
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2586))->
					BgL_polarityz00))
				{	/* SawBbv/bbv-merge.scm 208 */
					bool_t BgL_test3558z00_8647;

					{	/* SawBbv/bbv-merge.scm 208 */
						obj_t BgL_arg2174z00_2592;

						BgL_arg2174z00_2592 =
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2586))->
							BgL_typesz00);
						BgL_test3558z00_8647 =
							CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
							(BGl_za2objza2z00zztype_cachez00, BgL_arg2174z00_2592));
					}
					if (BgL_test3558z00_8647)
						{	/* SawBbv/bbv-merge.scm 208 */
							return 0L;
						}
					else
						{	/* SawBbv/bbv-merge.scm 208 */
							return
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2586))->
								BgL_countz00);
						}
				}
			else
				{	/* SawBbv/bbv-merge.scm 207 */
					return 1L;
				}
		}

	}



/* all~1 */
	obj_t BGl_allze71ze7zzsaw_bbvzd2mergezd2(obj_t BgL_lz00_2617)
	{
		{	/* SawBbv/bbv-merge.scm 223 */
			if (NULLP(BgL_lz00_2617))
				{	/* SawBbv/bbv-merge.scm 220 */
					return BNIL;
				}
			else
				{	/* SawBbv/bbv-merge.scm 222 */
					obj_t BgL_arg2189z00_2620;
					obj_t BgL_arg2190z00_2621;

					{	/* SawBbv/bbv-merge.scm 222 */
						obj_t BgL_l1575z00_2622;

						BgL_l1575z00_2622 = CDR(((obj_t) BgL_lz00_2617));
						if (NULLP(BgL_l1575z00_2622))
							{	/* SawBbv/bbv-merge.scm 222 */
								BgL_arg2189z00_2620 = BNIL;
							}
						else
							{	/* SawBbv/bbv-merge.scm 222 */
								obj_t BgL_head1577z00_2624;

								BgL_head1577z00_2624 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1575z00_2626;
									obj_t BgL_tail1578z00_2627;

									BgL_l1575z00_2626 = BgL_l1575z00_2622;
									BgL_tail1578z00_2627 = BgL_head1577z00_2624;
								BgL_zc3z04anonymousza32192ze3z87_2628:
									if (NULLP(BgL_l1575z00_2626))
										{	/* SawBbv/bbv-merge.scm 222 */
											BgL_arg2189z00_2620 = CDR(BgL_head1577z00_2624);
										}
									else
										{	/* SawBbv/bbv-merge.scm 222 */
											obj_t BgL_newtail1579z00_2630;

											{	/* SawBbv/bbv-merge.scm 222 */
												obj_t BgL_arg2196z00_2632;

												{	/* SawBbv/bbv-merge.scm 222 */
													obj_t BgL_ez00_2633;

													BgL_ez00_2633 = CAR(((obj_t) BgL_l1575z00_2626));
													{	/* SawBbv/bbv-merge.scm 222 */
														obj_t BgL_arg2197z00_2634;

														BgL_arg2197z00_2634 = CAR(((obj_t) BgL_lz00_2617));
														BgL_arg2196z00_2632 =
															MAKE_YOUNG_PAIR(BgL_arg2197z00_2634,
															BgL_ez00_2633);
													}
												}
												BgL_newtail1579z00_2630 =
													MAKE_YOUNG_PAIR(BgL_arg2196z00_2632, BNIL);
											}
											SET_CDR(BgL_tail1578z00_2627, BgL_newtail1579z00_2630);
											{	/* SawBbv/bbv-merge.scm 222 */
												obj_t BgL_arg2194z00_2631;

												BgL_arg2194z00_2631 = CDR(((obj_t) BgL_l1575z00_2626));
												{
													obj_t BgL_tail1578z00_8672;
													obj_t BgL_l1575z00_8671;

													BgL_l1575z00_8671 = BgL_arg2194z00_2631;
													BgL_tail1578z00_8672 = BgL_newtail1579z00_2630;
													BgL_tail1578z00_2627 = BgL_tail1578z00_8672;
													BgL_l1575z00_2626 = BgL_l1575z00_8671;
													goto BgL_zc3z04anonymousza32192ze3z87_2628;
												}
											}
										}
								}
							}
					}
					{	/* SawBbv/bbv-merge.scm 223 */
						obj_t BgL_arg2198z00_2636;

						BgL_arg2198z00_2636 = CDR(((obj_t) BgL_lz00_2617));
						BgL_arg2190z00_2621 =
							BGl_allze71ze7zzsaw_bbvzd2mergezd2(BgL_arg2198z00_2636);
					}
					return
						BGl_appendzd221011zd2zzsaw_bbvzd2mergezd2(BgL_arg2189z00_2620,
						BgL_arg2190z00_2621);
				}
		}

	}



/* ctx-score~0 */
	obj_t BGl_ctxzd2scoreze70z35zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxzd2_bglt
		BgL_ctxz00_2593)
	{
		{	/* SawBbv/bbv-merge.scm 213 */
			{	/* SawBbv/bbv-merge.scm 213 */
				obj_t BgL_runner2184z00_2612;

				{	/* SawBbv/bbv-merge.scm 213 */
					obj_t BgL_l1569z00_2596;

					BgL_l1569z00_2596 =
						(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_2593))->
						BgL_entriesz00);
					if (NULLP(BgL_l1569z00_2596))
						{	/* SawBbv/bbv-merge.scm 213 */
							BgL_runner2184z00_2612 = BNIL;
						}
					else
						{	/* SawBbv/bbv-merge.scm 213 */
							obj_t BgL_head1571z00_2598;

							{	/* SawBbv/bbv-merge.scm 213 */
								long BgL_arg2182z00_2610;

								BgL_arg2182z00_2610 =
									BGl_entryzd2scoreze73z35zzsaw_bbvzd2mergezd2(
									((BgL_bbvzd2ctxentryzd2_bglt) CAR(BgL_l1569z00_2596)));
								BgL_head1571z00_2598 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg2182z00_2610), BNIL);
							}
							{	/* SawBbv/bbv-merge.scm 213 */
								obj_t BgL_g1574z00_2599;

								BgL_g1574z00_2599 = CDR(BgL_l1569z00_2596);
								{
									obj_t BgL_l1569z00_2601;
									obj_t BgL_tail1572z00_2602;

									BgL_l1569z00_2601 = BgL_g1574z00_2599;
									BgL_tail1572z00_2602 = BgL_head1571z00_2598;
								BgL_zc3z04anonymousza32177ze3z87_2603:
									if (NULLP(BgL_l1569z00_2601))
										{	/* SawBbv/bbv-merge.scm 213 */
											BgL_runner2184z00_2612 = BgL_head1571z00_2598;
										}
									else
										{	/* SawBbv/bbv-merge.scm 213 */
											obj_t BgL_newtail1573z00_2605;

											{	/* SawBbv/bbv-merge.scm 213 */
												long BgL_arg2180z00_2607;

												BgL_arg2180z00_2607 =
													BGl_entryzd2scoreze73z35zzsaw_bbvzd2mergezd2(
													((BgL_bbvzd2ctxentryzd2_bglt)
														CAR(((obj_t) BgL_l1569z00_2601))));
												BgL_newtail1573z00_2605 =
													MAKE_YOUNG_PAIR(BINT(BgL_arg2180z00_2607), BNIL);
											}
											SET_CDR(BgL_tail1572z00_2602, BgL_newtail1573z00_2605);
											{	/* SawBbv/bbv-merge.scm 213 */
												obj_t BgL_arg2179z00_2606;

												BgL_arg2179z00_2606 = CDR(((obj_t) BgL_l1569z00_2601));
												{
													obj_t BgL_tail1572z00_8698;
													obj_t BgL_l1569z00_8697;

													BgL_l1569z00_8697 = BgL_arg2179z00_2606;
													BgL_tail1572z00_8698 = BgL_newtail1573z00_2605;
													BgL_tail1572z00_2602 = BgL_tail1572z00_8698;
													BgL_l1569z00_2601 = BgL_l1569z00_8697;
													goto BgL_zc3z04anonymousza32177ze3z87_2603;
												}
											}
										}
								}
							}
						}
				}
				return BGl_zb2zb2zz__r4_numbers_6_5z00(BgL_runner2184z00_2612);
			}
		}

	}



/* &<@anonymous:2125> */
	obj_t BGl_z62zc3z04anonymousza32125ze3ze5zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6479, obj_t BgL_xz00_6480, obj_t BgL_yz00_6481)
	{
		{	/* SawBbv/bbv-merge.scm 247 */
			return
				BBOOL(
				((long) CINT(CAR(
							((obj_t) BgL_xz00_6480))) >=
					(long) CINT(CAR(((obj_t) BgL_yz00_6481)))));
		}

	}



/* bbv-block-merge-select-strategy-score+ */
	obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2scorezb2z60zzsaw_bbvzd2mergezd2
		(obj_t BgL_bsz00_259)
	{
		{	/* SawBbv/bbv-merge.scm 261 */
			{
				BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_2692;

				{	/* SawBbv/bbv-merge.scm 291 */
					obj_t BgL_ccz00_2653;

					BgL_ccz00_2653 = BGl_allze70ze7zzsaw_bbvzd2mergezd2(BgL_bsz00_259);
					{	/* SawBbv/bbv-merge.scm 291 */
						obj_t BgL_ccsz00_2654;

						if (NULLP(BgL_ccz00_2653))
							{	/* SawBbv/bbv-merge.scm 292 */
								BgL_ccsz00_2654 = BNIL;
							}
						else
							{	/* SawBbv/bbv-merge.scm 292 */
								obj_t BgL_head1622z00_2668;

								BgL_head1622z00_2668 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1620z00_2670;
									obj_t BgL_tail1623z00_2671;

									BgL_l1620z00_2670 = BgL_ccz00_2653;
									BgL_tail1623z00_2671 = BgL_head1622z00_2668;
								BgL_zc3z04anonymousza32207ze3z87_2672:
									if (NULLP(BgL_l1620z00_2670))
										{	/* SawBbv/bbv-merge.scm 292 */
											BgL_ccsz00_2654 = CDR(BgL_head1622z00_2668);
										}
									else
										{	/* SawBbv/bbv-merge.scm 292 */
											obj_t BgL_newtail1624z00_2674;

											{	/* SawBbv/bbv-merge.scm 292 */
												obj_t BgL_arg2210z00_2676;

												{	/* SawBbv/bbv-merge.scm 292 */
													obj_t BgL_cz00_2677;

													BgL_cz00_2677 = CAR(((obj_t) BgL_l1620z00_2670));
													{	/* SawBbv/bbv-merge.scm 293 */
														BgL_blockz00_bglt BgL_i1209z00_2678;

														BgL_i1209z00_2678 =
															((BgL_blockz00_bglt)
															CAR(((obj_t) BgL_cz00_2677)));
														{	/* SawBbv/bbv-merge.scm 294 */
															BgL_blockz00_bglt BgL_i1210z00_2679;

															BgL_i1210z00_2679 =
																((BgL_blockz00_bglt)
																CDR(((obj_t) BgL_cz00_2677)));
															{	/* SawBbv/bbv-merge.scm 295 */
																BgL_bbvzd2ctxzd2_bglt BgL_nxz00_2680;

																{	/* SawBbv/bbv-merge.scm 295 */
																	BgL_bbvzd2ctxzd2_bglt BgL_arg2211z00_2682;
																	BgL_bbvzd2ctxzd2_bglt BgL_arg2212z00_2683;

																	{
																		BgL_blocksz00_bglt BgL_auxz00_8723;

																		{
																			obj_t BgL_auxz00_8724;

																			{	/* SawBbv/bbv-merge.scm 295 */
																				BgL_objectz00_bglt BgL_tmpz00_8725;

																				BgL_tmpz00_8725 =
																					((BgL_objectz00_bglt)
																					BgL_i1209z00_2678);
																				BgL_auxz00_8724 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_8725);
																			}
																			BgL_auxz00_8723 =
																				((BgL_blocksz00_bglt) BgL_auxz00_8724);
																		}
																		BgL_arg2211z00_2682 =
																			(((BgL_blocksz00_bglt)
																				COBJECT(BgL_auxz00_8723))->BgL_ctxz00);
																	}
																	{
																		BgL_blocksz00_bglt BgL_auxz00_8730;

																		{
																			obj_t BgL_auxz00_8731;

																			{	/* SawBbv/bbv-merge.scm 295 */
																				BgL_objectz00_bglt BgL_tmpz00_8732;

																				BgL_tmpz00_8732 =
																					((BgL_objectz00_bglt)
																					BgL_i1210z00_2679);
																				BgL_auxz00_8731 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_8732);
																			}
																			BgL_auxz00_8730 =
																				((BgL_blocksz00_bglt) BgL_auxz00_8731);
																		}
																		BgL_arg2212z00_2683 =
																			(((BgL_blocksz00_bglt)
																				COBJECT(BgL_auxz00_8730))->BgL_ctxz00);
																	}
																	BgL_nxz00_2680 =
																		BGl_mergezd2ctxzd2zzsaw_bbvzd2mergezd2
																		(BgL_arg2211z00_2682, BgL_arg2212z00_2683);
																}
																{	/* SawBbv/bbv-merge.scm 295 */
																	obj_t BgL_scorez00_2681;

																	BgL_ctxz00_2692 = BgL_nxz00_2680;
																	{	/* SawBbv/bbv-merge.scm 272 */
																		obj_t BgL_runner2227z00_2711;

																		{	/* SawBbv/bbv-merge.scm 272 */
																			obj_t BgL_l1607z00_2695;

																			BgL_l1607z00_2695 =
																				(((BgL_bbvzd2ctxzd2_bglt)
																					COBJECT(BgL_ctxz00_2692))->
																				BgL_entriesz00);
																			if (NULLP(BgL_l1607z00_2695))
																				{	/* SawBbv/bbv-merge.scm 272 */
																					BgL_runner2227z00_2711 = BNIL;
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 272 */
																					obj_t BgL_head1609z00_2697;

																					{	/* SawBbv/bbv-merge.scm 272 */
																						long BgL_arg2225z00_2709;

																						BgL_arg2225z00_2709 =
																							BGl_entryzd2scoreze72z35zzsaw_bbvzd2mergezd2
																							(((BgL_bbvzd2ctxentryzd2_bglt)
																								CAR(BgL_l1607z00_2695)));
																						BgL_head1609z00_2697 =
																							MAKE_YOUNG_PAIR(BINT
																							(BgL_arg2225z00_2709), BNIL);
																					}
																					{	/* SawBbv/bbv-merge.scm 272 */
																						obj_t BgL_g1612z00_2698;

																						BgL_g1612z00_2698 =
																							CDR(BgL_l1607z00_2695);
																						{
																							obj_t BgL_l1607z00_2700;
																							obj_t BgL_tail1610z00_2701;

																							BgL_l1607z00_2700 =
																								BgL_g1612z00_2698;
																							BgL_tail1610z00_2701 =
																								BgL_head1609z00_2697;
																						BgL_zc3z04anonymousza32220ze3z87_2702:
																							if (NULLP
																								(BgL_l1607z00_2700))
																								{	/* SawBbv/bbv-merge.scm 272 */
																									BgL_runner2227z00_2711 =
																										BgL_head1609z00_2697;
																								}
																							else
																								{	/* SawBbv/bbv-merge.scm 272 */
																									obj_t BgL_newtail1611z00_2704;

																									{	/* SawBbv/bbv-merge.scm 272 */
																										long BgL_arg2223z00_2706;

																										BgL_arg2223z00_2706 =
																											BGl_entryzd2scoreze72z35zzsaw_bbvzd2mergezd2
																											(((BgL_bbvzd2ctxentryzd2_bglt) CAR(((obj_t) BgL_l1607z00_2700))));
																										BgL_newtail1611z00_2704 =
																											MAKE_YOUNG_PAIR(BINT
																											(BgL_arg2223z00_2706),
																											BNIL);
																									}
																									SET_CDR(BgL_tail1610z00_2701,
																										BgL_newtail1611z00_2704);
																									{	/* SawBbv/bbv-merge.scm 272 */
																										obj_t BgL_arg2222z00_2705;

																										BgL_arg2222z00_2705 =
																											CDR(
																											((obj_t)
																												BgL_l1607z00_2700));
																										{
																											obj_t
																												BgL_tail1610z00_8759;
																											obj_t BgL_l1607z00_8758;

																											BgL_l1607z00_8758 =
																												BgL_arg2222z00_2705;
																											BgL_tail1610z00_8759 =
																												BgL_newtail1611z00_2704;
																											BgL_tail1610z00_2701 =
																												BgL_tail1610z00_8759;
																											BgL_l1607z00_2700 =
																												BgL_l1607z00_8758;
																											goto
																												BgL_zc3z04anonymousza32220ze3z87_2702;
																										}
																									}
																								}
																						}
																					}
																				}
																		}
																		BgL_scorez00_2681 =
																			BGl_zb2zb2zz__r4_numbers_6_5z00
																			(BgL_runner2227z00_2711);
																	}
																	{	/* SawBbv/bbv-merge.scm 296 */

																		BgL_arg2210z00_2676 =
																			MAKE_YOUNG_PAIR(BgL_scorez00_2681,
																			BgL_cz00_2677);
																	}
																}
															}
														}
													}
												}
												BgL_newtail1624z00_2674 =
													MAKE_YOUNG_PAIR(BgL_arg2210z00_2676, BNIL);
											}
											SET_CDR(BgL_tail1623z00_2671, BgL_newtail1624z00_2674);
											{	/* SawBbv/bbv-merge.scm 292 */
												obj_t BgL_arg2209z00_2675;

												BgL_arg2209z00_2675 = CDR(((obj_t) BgL_l1620z00_2670));
												{
													obj_t BgL_tail1623z00_8767;
													obj_t BgL_l1620z00_8766;

													BgL_l1620z00_8766 = BgL_arg2209z00_2675;
													BgL_tail1623z00_8767 = BgL_newtail1624z00_2674;
													BgL_tail1623z00_2671 = BgL_tail1623z00_8767;
													BgL_l1620z00_2670 = BgL_l1620z00_8766;
													goto BgL_zc3z04anonymousza32207ze3z87_2672;
												}
											}
										}
								}
							}
						{	/* SawBbv/bbv-merge.scm 292 */
							obj_t BgL_sccsz00_2655;

							BgL_sccsz00_2655 =
								BGl_sortz00zz__r4_vectors_6_8z00
								(BGl_proc3290z00zzsaw_bbvzd2mergezd2, BgL_ccsz00_2654);
							{	/* SawBbv/bbv-merge.scm 299 */
								obj_t BgL_pairz00_2656;

								{	/* SawBbv/bbv-merge.scm 302 */
									obj_t BgL_pairz00_5098;

									BgL_pairz00_5098 = CAR(((obj_t) BgL_sccsz00_2655));
									BgL_pairz00_2656 = CDR(BgL_pairz00_5098);
								}
								{	/* SawBbv/bbv-merge.scm 302 */

									{	/* SawBbv/bbv-merge.scm 306 */
										obj_t BgL_val0_1625z00_2657;
										obj_t BgL_val1_1626z00_2658;

										BgL_val0_1625z00_2657 = CAR(((obj_t) BgL_pairz00_2656));
										BgL_val1_1626z00_2658 = CDR(((obj_t) BgL_pairz00_2656));
										{	/* SawBbv/bbv-merge.scm 306 */
											int BgL_tmpz00_8776;

											BgL_tmpz00_8776 = (int) (2L);
											BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8776);
										}
										{	/* SawBbv/bbv-merge.scm 306 */
											int BgL_tmpz00_8779;

											BgL_tmpz00_8779 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_8779,
												BgL_val1_1626z00_2658);
										}
										return BgL_val0_1625z00_2657;
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* entry-score~2 */
	long BGl_entryzd2scoreze72z35zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt
		BgL_ez00_2685)
	{
		{	/* SawBbv/bbv-merge.scm 268 */
			if (
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2685))->
					BgL_polarityz00))
				{	/* SawBbv/bbv-merge.scm 267 */
					bool_t BgL_test3569z00_8784;

					{	/* SawBbv/bbv-merge.scm 267 */
						obj_t BgL_arg2217z00_2691;

						BgL_arg2217z00_2691 =
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2685))->
							BgL_typesz00);
						BgL_test3569z00_8784 =
							CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
							(BGl_za2objza2z00zztype_cachez00, BgL_arg2217z00_2691));
					}
					if (BgL_test3569z00_8784)
						{	/* SawBbv/bbv-merge.scm 267 */
							return 0L;
						}
					else
						{	/* SawBbv/bbv-merge.scm 267 */
							return
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2685))->
								BgL_countz00);
						}
				}
			else
				{	/* SawBbv/bbv-merge.scm 266 */
					return 1L;
				}
		}

	}



/* all~0 */
	obj_t BGl_allze70ze7zzsaw_bbvzd2mergezd2(obj_t BgL_lz00_2716)
	{
		{	/* SawBbv/bbv-merge.scm 282 */
			if (NULLP(BgL_lz00_2716))
				{	/* SawBbv/bbv-merge.scm 279 */
					return BNIL;
				}
			else
				{	/* SawBbv/bbv-merge.scm 281 */
					obj_t BgL_arg2232z00_2719;
					obj_t BgL_arg2233z00_2720;

					{	/* SawBbv/bbv-merge.scm 281 */
						obj_t BgL_l1613z00_2721;

						BgL_l1613z00_2721 = CDR(((obj_t) BgL_lz00_2716));
						if (NULLP(BgL_l1613z00_2721))
							{	/* SawBbv/bbv-merge.scm 281 */
								BgL_arg2232z00_2719 = BNIL;
							}
						else
							{	/* SawBbv/bbv-merge.scm 281 */
								obj_t BgL_head1615z00_2723;

								BgL_head1615z00_2723 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1613z00_2725;
									obj_t BgL_tail1616z00_2726;

									BgL_l1613z00_2725 = BgL_l1613z00_2721;
									BgL_tail1616z00_2726 = BgL_head1615z00_2723;
								BgL_zc3z04anonymousza32235ze3z87_2727:
									if (NULLP(BgL_l1613z00_2725))
										{	/* SawBbv/bbv-merge.scm 281 */
											BgL_arg2232z00_2719 = CDR(BgL_head1615z00_2723);
										}
									else
										{	/* SawBbv/bbv-merge.scm 281 */
											obj_t BgL_newtail1617z00_2729;

											{	/* SawBbv/bbv-merge.scm 281 */
												obj_t BgL_arg2238z00_2731;

												{	/* SawBbv/bbv-merge.scm 281 */
													obj_t BgL_ez00_2732;

													BgL_ez00_2732 = CAR(((obj_t) BgL_l1613z00_2725));
													{	/* SawBbv/bbv-merge.scm 281 */
														obj_t BgL_arg2239z00_2733;

														BgL_arg2239z00_2733 = CAR(((obj_t) BgL_lz00_2716));
														BgL_arg2238z00_2731 =
															MAKE_YOUNG_PAIR(BgL_arg2239z00_2733,
															BgL_ez00_2732);
													}
												}
												BgL_newtail1617z00_2729 =
													MAKE_YOUNG_PAIR(BgL_arg2238z00_2731, BNIL);
											}
											SET_CDR(BgL_tail1616z00_2726, BgL_newtail1617z00_2729);
											{	/* SawBbv/bbv-merge.scm 281 */
												obj_t BgL_arg2237z00_2730;

												BgL_arg2237z00_2730 = CDR(((obj_t) BgL_l1613z00_2725));
												{
													obj_t BgL_tail1616z00_8809;
													obj_t BgL_l1613z00_8808;

													BgL_l1613z00_8808 = BgL_arg2237z00_2730;
													BgL_tail1616z00_8809 = BgL_newtail1617z00_2729;
													BgL_tail1616z00_2726 = BgL_tail1616z00_8809;
													BgL_l1613z00_2725 = BgL_l1613z00_8808;
													goto BgL_zc3z04anonymousza32235ze3z87_2727;
												}
											}
										}
								}
							}
					}
					{	/* SawBbv/bbv-merge.scm 282 */
						obj_t BgL_arg2240z00_2735;

						BgL_arg2240z00_2735 = CDR(((obj_t) BgL_lz00_2716));
						BgL_arg2233z00_2720 =
							BGl_allze70ze7zzsaw_bbvzd2mergezd2(BgL_arg2240z00_2735);
					}
					return
						BGl_appendzd221011zd2zzsaw_bbvzd2mergezd2(BgL_arg2232z00_2719,
						BgL_arg2233z00_2720);
				}
		}

	}



/* &<@anonymous:2203> */
	obj_t BGl_z62zc3z04anonymousza32203ze3ze5zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6483, obj_t BgL_xz00_6484, obj_t BgL_yz00_6485)
	{
		{	/* SawBbv/bbv-merge.scm 299 */
			return
				BBOOL(
				((long) CINT(CAR(
							((obj_t) BgL_xz00_6484))) >=
					(long) CINT(CAR(((obj_t) BgL_yz00_6485)))));
		}

	}



/* bbv-block-merge-select-strategy-score- */
	obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2scorezd2z00zzsaw_bbvzd2mergezd2
		(obj_t BgL_bsz00_260)
	{
		{	/* SawBbv/bbv-merge.scm 311 */
			{
				obj_t BgL_xz00_2772;
				obj_t BgL_yz00_2773;
				BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_2805;
				obj_t BgL_bsz00_2829;

				{
					obj_t BgL_pbsz00_2747;

					BgL_pbsz00_2747 = BgL_bsz00_260;
				BgL_zc3z04anonymousza32241ze3z87_2748:
					if (NULLP(BgL_pbsz00_2747))
						{	/* SawBbv/bbv-merge.scm 363 */
							BgL_bsz00_2829 = BgL_bsz00_260;
							{	/* SawBbv/bbv-merge.scm 348 */
								obj_t BgL_bsz00_2831;

								{	/* SawBbv/bbv-merge.scm 348 */
									obj_t BgL_arg2289z00_2843;

									{	/* SawBbv/bbv-merge.scm 350 */
										obj_t BgL_head1639z00_2852;

										BgL_head1639z00_2852 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1637z00_2854;
											obj_t BgL_tail1640z00_2855;

											BgL_l1637z00_2854 = BgL_bsz00_2829;
											BgL_tail1640z00_2855 = BgL_head1639z00_2852;
										BgL_zc3z04anonymousza32293ze3z87_2856:
											if (NULLP(BgL_l1637z00_2854))
												{	/* SawBbv/bbv-merge.scm 350 */
													BgL_arg2289z00_2843 = CDR(BgL_head1639z00_2852);
												}
											else
												{	/* SawBbv/bbv-merge.scm 350 */
													obj_t BgL_newtail1641z00_2858;

													{	/* SawBbv/bbv-merge.scm 350 */
														obj_t BgL_arg2296z00_2860;

														{	/* SawBbv/bbv-merge.scm 350 */
															obj_t BgL_bz00_2861;

															BgL_bz00_2861 = CAR(((obj_t) BgL_l1637z00_2854));
															{	/* SawBbv/bbv-merge.scm 350 */
																obj_t BgL_arg2297z00_2862;

																{	/* SawBbv/bbv-merge.scm 345 */
																	BgL_bbvzd2ctxzd2_bglt BgL_arg2283z00_5129;

																	{
																		BgL_blocksz00_bglt BgL_auxz00_8830;

																		{
																			obj_t BgL_auxz00_8831;

																			{	/* SawBbv/bbv-merge.scm 345 */
																				BgL_objectz00_bglt BgL_tmpz00_8832;

																				BgL_tmpz00_8832 =
																					((BgL_objectz00_bglt)
																					((BgL_blockz00_bglt) BgL_bz00_2861));
																				BgL_auxz00_8831 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_8832);
																			}
																			BgL_auxz00_8830 =
																				((BgL_blocksz00_bglt) BgL_auxz00_8831);
																		}
																		BgL_arg2283z00_5129 =
																			(((BgL_blocksz00_bglt)
																				COBJECT(BgL_auxz00_8830))->BgL_ctxz00);
																	}
																	BgL_ctxz00_2805 = BgL_arg2283z00_5129;
																	{	/* SawBbv/bbv-merge.scm 341 */
																		obj_t BgL_runner2281z00_2824;

																		{	/* SawBbv/bbv-merge.scm 341 */
																			obj_t BgL_l1629z00_2808;

																			BgL_l1629z00_2808 =
																				(((BgL_bbvzd2ctxzd2_bglt)
																					COBJECT(BgL_ctxz00_2805))->
																				BgL_entriesz00);
																			if (NULLP(BgL_l1629z00_2808))
																				{	/* SawBbv/bbv-merge.scm 341 */
																					BgL_runner2281z00_2824 = BNIL;
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 341 */
																					obj_t BgL_head1631z00_2810;

																					{	/* SawBbv/bbv-merge.scm 341 */
																						obj_t BgL_arg2279z00_2822;

																						{	/* SawBbv/bbv-merge.scm 341 */
																							obj_t BgL_arg2280z00_2823;

																							BgL_arg2280z00_2823 =
																								CAR(BgL_l1629z00_2808);
																							BgL_arg2279z00_2822 =
																								BGl_entryzd2scoreze71z35zzsaw_bbvzd2mergezd2
																								(((BgL_bbvzd2ctxentryzd2_bglt)
																									BgL_arg2280z00_2823));
																						}
																						BgL_head1631z00_2810 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2279z00_2822, BNIL);
																					}
																					{	/* SawBbv/bbv-merge.scm 341 */
																						obj_t BgL_g1634z00_2811;

																						BgL_g1634z00_2811 =
																							CDR(BgL_l1629z00_2808);
																						{
																							obj_t BgL_l1629z00_2813;
																							obj_t BgL_tail1632z00_2814;

																							BgL_l1629z00_2813 =
																								BgL_g1634z00_2811;
																							BgL_tail1632z00_2814 =
																								BgL_head1631z00_2810;
																						BgL_zc3z04anonymousza32273ze3z87_2815:
																							if (NULLP
																								(BgL_l1629z00_2813))
																								{	/* SawBbv/bbv-merge.scm 341 */
																									BgL_runner2281z00_2824 =
																										BgL_head1631z00_2810;
																								}
																							else
																								{	/* SawBbv/bbv-merge.scm 341 */
																									obj_t BgL_newtail1633z00_2817;

																									{	/* SawBbv/bbv-merge.scm 341 */
																										obj_t BgL_arg2276z00_2819;

																										{	/* SawBbv/bbv-merge.scm 341 */
																											obj_t BgL_arg2277z00_2820;

																											BgL_arg2277z00_2820 =
																												CAR(
																												((obj_t)
																													BgL_l1629z00_2813));
																											BgL_arg2276z00_2819 =
																												BGl_entryzd2scoreze71z35zzsaw_bbvzd2mergezd2
																												(((BgL_bbvzd2ctxentryzd2_bglt) BgL_arg2277z00_2820));
																										}
																										BgL_newtail1633z00_2817 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2276z00_2819,
																											BNIL);
																									}
																									SET_CDR(BgL_tail1632z00_2814,
																										BgL_newtail1633z00_2817);
																									{	/* SawBbv/bbv-merge.scm 341 */
																										obj_t BgL_arg2275z00_2818;

																										BgL_arg2275z00_2818 =
																											CDR(
																											((obj_t)
																												BgL_l1629z00_2813));
																										{
																											obj_t
																												BgL_tail1632z00_8857;
																											obj_t BgL_l1629z00_8856;

																											BgL_l1629z00_8856 =
																												BgL_arg2275z00_2818;
																											BgL_tail1632z00_8857 =
																												BgL_newtail1633z00_2817;
																											BgL_tail1632z00_2814 =
																												BgL_tail1632z00_8857;
																											BgL_l1629z00_2813 =
																												BgL_l1629z00_8856;
																											goto
																												BgL_zc3z04anonymousza32273ze3z87_2815;
																										}
																									}
																								}
																						}
																					}
																				}
																		}
																		BgL_arg2297z00_2862 =
																			BGl_zb2zb2zz__r4_numbers_6_5z00
																			(BgL_runner2281z00_2824);
																	}
																}
																BgL_arg2296z00_2860 =
																	MAKE_YOUNG_PAIR(BgL_arg2297z00_2862,
																	BgL_bz00_2861);
															}
														}
														BgL_newtail1641z00_2858 =
															MAKE_YOUNG_PAIR(BgL_arg2296z00_2860, BNIL);
													}
													SET_CDR(BgL_tail1640z00_2855,
														BgL_newtail1641z00_2858);
													{	/* SawBbv/bbv-merge.scm 350 */
														obj_t BgL_arg2295z00_2859;

														BgL_arg2295z00_2859 =
															CDR(((obj_t) BgL_l1637z00_2854));
														{
															obj_t BgL_tail1640z00_8865;
															obj_t BgL_l1637z00_8864;

															BgL_l1637z00_8864 = BgL_arg2295z00_2859;
															BgL_tail1640z00_8865 = BgL_newtail1641z00_2858;
															BgL_tail1640z00_2855 = BgL_tail1640z00_8865;
															BgL_l1637z00_2854 = BgL_l1637z00_8864;
															goto BgL_zc3z04anonymousza32293ze3z87_2856;
														}
													}
												}
										}
									}
									BgL_bsz00_2831 =
										BGl_sortz00zz__r4_vectors_6_8z00
										(BGl_proc3291z00zzsaw_bbvzd2mergezd2, BgL_arg2289z00_2843);
								}
								{	/* SawBbv/bbv-merge.scm 359 */
									obj_t BgL_val0_1644z00_2840;
									obj_t BgL_val1_1645z00_2841;

									{	/* SawBbv/bbv-merge.scm 359 */
										obj_t BgL_pairz00_5143;

										BgL_pairz00_5143 = CAR(((obj_t) BgL_bsz00_2831));
										BgL_val0_1644z00_2840 = CDR(BgL_pairz00_5143);
									}
									{	/* SawBbv/bbv-merge.scm 359 */
										obj_t BgL_pairz00_5149;

										{	/* SawBbv/bbv-merge.scm 359 */
											obj_t BgL_pairz00_5148;

											BgL_pairz00_5148 = CDR(((obj_t) BgL_bsz00_2831));
											BgL_pairz00_5149 = CAR(BgL_pairz00_5148);
										}
										BgL_val1_1645z00_2841 = CDR(BgL_pairz00_5149);
									}
									{	/* SawBbv/bbv-merge.scm 359 */
										int BgL_tmpz00_8874;

										BgL_tmpz00_8874 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8874);
									}
									{	/* SawBbv/bbv-merge.scm 359 */
										int BgL_tmpz00_8877;

										BgL_tmpz00_8877 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_8877, BgL_val1_1645z00_2841);
									}
									return BgL_val0_1644z00_2840;
								}
							}
						}
					else
						{	/* SawBbv/bbv-merge.scm 365 */
							obj_t BgL_sbz00_2750;

							{	/* SawBbv/bbv-merge.scm 365 */
								obj_t BgL_g1648z00_2754;

								BgL_g1648z00_2754 = CDR(((obj_t) BgL_pbsz00_2747));
								{
									obj_t BgL_list1647z00_2756;

									BgL_list1647z00_2756 = BgL_g1648z00_2754;
								BgL_zc3z04anonymousza32244ze3z87_2757:
									if (PAIRP(BgL_list1647z00_2756))
										{	/* SawBbv/bbv-merge.scm 367 */
											bool_t BgL_test3578z00_8884;

											{	/* SawBbv/bbv-merge.scm 366 */
												obj_t BgL_bz00_2763;

												BgL_bz00_2763 = CAR(BgL_list1647z00_2756);
												{	/* SawBbv/bbv-merge.scm 366 */
													obj_t BgL_arg2249z00_2764;

													BgL_arg2249z00_2764 = CAR(((obj_t) BgL_pbsz00_2747));
													{	/* SawBbv/bbv-merge.scm 366 */
														obj_t BgL_tmpz00_8888;

														BgL_xz00_2772 = BgL_arg2249z00_2764;
														BgL_yz00_2773 = BgL_bz00_2763;
														{	/* SawBbv/bbv-merge.scm 319 */
															BgL_bbvzd2ctxzd2_bglt BgL_i1213z00_2777;

															{
																BgL_blocksz00_bglt BgL_auxz00_8889;

																{
																	obj_t BgL_auxz00_8890;

																	{	/* SawBbv/bbv-merge.scm 320 */
																		BgL_objectz00_bglt BgL_tmpz00_8891;

																		BgL_tmpz00_8891 =
																			((BgL_objectz00_bglt)
																			((BgL_blockz00_bglt) BgL_xz00_2772));
																		BgL_auxz00_8890 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_8891);
																	}
																	BgL_auxz00_8889 =
																		((BgL_blocksz00_bglt) BgL_auxz00_8890);
																}
																BgL_i1213z00_2777 =
																	(((BgL_blocksz00_bglt)
																		COBJECT(BgL_auxz00_8889))->BgL_ctxz00);
															}
															{	/* SawBbv/bbv-merge.scm 320 */
																BgL_bbvzd2ctxzd2_bglt BgL_i1214z00_2778;

																{
																	BgL_blocksz00_bglt BgL_auxz00_8897;

																	{
																		obj_t BgL_auxz00_8898;

																		{	/* SawBbv/bbv-merge.scm 321 */
																			BgL_objectz00_bglt BgL_tmpz00_8899;

																			BgL_tmpz00_8899 =
																				((BgL_objectz00_bglt)
																				((BgL_blockz00_bglt) BgL_yz00_2773));
																			BgL_auxz00_8898 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_8899);
																		}
																		BgL_auxz00_8897 =
																			((BgL_blocksz00_bglt) BgL_auxz00_8898);
																	}
																	BgL_i1214z00_2778 =
																		(((BgL_blocksz00_bglt)
																			COBJECT(BgL_auxz00_8897))->BgL_ctxz00);
																}
																{	/* SawBbv/bbv-merge.scm 321 */
																	obj_t BgL_arg2255z00_2780;
																	obj_t BgL_arg2256z00_2781;

																	BgL_arg2255z00_2780 =
																		(((BgL_bbvzd2ctxzd2_bglt)
																			COBJECT(BgL_i1213z00_2777))->
																		BgL_entriesz00);
																	BgL_arg2256z00_2781 =
																		(((BgL_bbvzd2ctxzd2_bglt)
																			COBJECT(BgL_i1214z00_2778))->
																		BgL_entriesz00);
																	{	/* SawBbv/bbv-merge.scm 321 */
																		obj_t BgL_list2257z00_2782;

																		{	/* SawBbv/bbv-merge.scm 321 */
																			obj_t BgL_arg2258z00_2783;

																			BgL_arg2258z00_2783 =
																				MAKE_YOUNG_PAIR(BgL_arg2256z00_2781,
																				BNIL);
																			BgL_list2257z00_2782 =
																				MAKE_YOUNG_PAIR(BgL_arg2255z00_2780,
																				BgL_arg2258z00_2783);
																		}
																		BgL_tmpz00_8888 =
																			BGl_everyz00zz__r4_pairs_and_lists_6_3z00
																			(BGl_proc3292z00zzsaw_bbvzd2mergezd2,
																			BgL_list2257z00_2782);
																	}
																}
															}
														}
														BgL_test3578z00_8884 = CBOOL(BgL_tmpz00_8888);
													}
												}
											}
											if (BgL_test3578z00_8884)
												{	/* SawBbv/bbv-merge.scm 367 */
													BgL_sbz00_2750 = CAR(BgL_list1647z00_2756);
												}
											else
												{
													obj_t BgL_list1647z00_8912;

													BgL_list1647z00_8912 = CDR(BgL_list1647z00_2756);
													BgL_list1647z00_2756 = BgL_list1647z00_8912;
													goto BgL_zc3z04anonymousza32244ze3z87_2757;
												}
										}
									else
										{	/* SawBbv/bbv-merge.scm 367 */
											BgL_sbz00_2750 = BFALSE;
										}
								}
							}
							if (CBOOL(BgL_sbz00_2750))
								{	/* SawBbv/bbv-merge.scm 373 */
									obj_t BgL_val0_1649z00_2751;

									BgL_val0_1649z00_2751 = CAR(((obj_t) BgL_pbsz00_2747));
									{	/* SawBbv/bbv-merge.scm 373 */
										int BgL_tmpz00_8918;

										BgL_tmpz00_8918 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_8918);
									}
									{	/* SawBbv/bbv-merge.scm 373 */
										int BgL_tmpz00_8921;

										BgL_tmpz00_8921 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_8921, BgL_sbz00_2750);
									}
									return BgL_val0_1649z00_2751;
								}
							else
								{	/* SawBbv/bbv-merge.scm 374 */
									obj_t BgL_arg2243z00_2753;

									BgL_arg2243z00_2753 = CDR(((obj_t) BgL_pbsz00_2747));
									{
										obj_t BgL_pbsz00_8926;

										BgL_pbsz00_8926 = BgL_arg2243z00_2753;
										BgL_pbsz00_2747 = BgL_pbsz00_8926;
										goto BgL_zc3z04anonymousza32241ze3z87_2748;
									}
								}
						}
				}
			}
		}

	}



/* entry-score~1 */
	obj_t BGl_entryzd2scoreze71z35zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt
		BgL_ez00_2795)
	{
		{	/* SawBbv/bbv-merge.scm 337 */
			if (
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2795))->
					BgL_polarityz00))
				{	/* SawBbv/bbv-merge.scm 336 */
					bool_t BgL_test3581z00_8929;

					{	/* SawBbv/bbv-merge.scm 336 */
						obj_t BgL_arg2270z00_2804;

						BgL_arg2270z00_2804 =
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2795))->
							BgL_typesz00);
						BgL_test3581z00_8929 =
							CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
							(BGl_za2objza2z00zztype_cachez00, BgL_arg2270z00_2804));
					}
					if (BgL_test3581z00_8929)
						{	/* SawBbv/bbv-merge.scm 336 */
							return BINT(0L);
						}
					else
						{	/* SawBbv/bbv-merge.scm 337 */
							long BgL_a1627z00_2801;

							BgL_a1627z00_2801 =
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2795))->
								BgL_countz00);
							{	/* SawBbv/bbv-merge.scm 337 */
								long BgL_b1628z00_2802;

								BgL_b1628z00_2802 =
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2795))->
									BgL_countz00);
								{	/* SawBbv/bbv-merge.scm 337 */

									{	/* SawBbv/bbv-merge.scm 337 */
										obj_t BgL_za71za7_5107;
										obj_t BgL_za72za7_5108;

										BgL_za71za7_5107 = BINT(BgL_a1627z00_2801);
										BgL_za72za7_5108 = BINT(BgL_b1628z00_2802);
										{	/* SawBbv/bbv-merge.scm 337 */
											obj_t BgL_tmpz00_5109;

											BgL_tmpz00_5109 = BINT(0L);
											{	/* SawBbv/bbv-merge.scm 337 */
												bool_t BgL_test3582z00_8939;

												{	/* SawBbv/bbv-merge.scm 337 */
													long BgL_tmpz00_8940;

													BgL_tmpz00_8940 = (long) CINT(BgL_za72za7_5108);
													BgL_test3582z00_8939 =
														BGL_MULFX_OV(BgL_za71za7_5107, BgL_tmpz00_8940,
														BgL_tmpz00_5109);
												}
												if (BgL_test3582z00_8939)
													{	/* SawBbv/bbv-merge.scm 337 */
														return
															bgl_bignum_mul(bgl_long_to_bignum(
																(long) CINT(BgL_za71za7_5107)),
															bgl_long_to_bignum(
																(long) CINT(BgL_za72za7_5108)));
													}
												else
													{	/* SawBbv/bbv-merge.scm 337 */
														return BgL_tmpz00_5109;
													}
											}
										}
									}
								}
							}
						}
				}
			else
				{	/* SawBbv/bbv-merge.scm 335 */
					return BINT(1L);
				}
		}

	}



/* &<@anonymous:2290> */
	obj_t BGl_z62zc3z04anonymousza32290ze3ze5zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6491, obj_t BgL_xz00_6492, obj_t BgL_yz00_6493)
	{
		{	/* SawBbv/bbv-merge.scm 348 */
			{	/* SawBbv/bbv-merge.scm 349 */
				bool_t BgL_tmpz00_8949;

				{	/* SawBbv/bbv-merge.scm 349 */
					obj_t BgL_a1635z00_6694;

					BgL_a1635z00_6694 = CAR(((obj_t) BgL_xz00_6492));
					{	/* SawBbv/bbv-merge.scm 349 */
						obj_t BgL_b1636z00_6695;

						BgL_b1636z00_6695 = CAR(((obj_t) BgL_yz00_6493));
						{	/* SawBbv/bbv-merge.scm 349 */

							{	/* SawBbv/bbv-merge.scm 349 */
								bool_t BgL_test3583z00_8954;

								if (INTEGERP(BgL_a1635z00_6694))
									{	/* SawBbv/bbv-merge.scm 349 */
										BgL_test3583z00_8954 = INTEGERP(BgL_b1636z00_6695);
									}
								else
									{	/* SawBbv/bbv-merge.scm 349 */
										BgL_test3583z00_8954 = ((bool_t) 0);
									}
								if (BgL_test3583z00_8954)
									{	/* SawBbv/bbv-merge.scm 349 */
										BgL_tmpz00_8949 =
											(
											(long) CINT(BgL_a1635z00_6694) <=
											(long) CINT(BgL_b1636z00_6695));
									}
								else
									{	/* SawBbv/bbv-merge.scm 349 */
										BgL_tmpz00_8949 =
											BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BgL_a1635z00_6694,
											BgL_b1636z00_6695);
									}
							}
						}
					}
				}
				return BBOOL(BgL_tmpz00_8949);
			}
		}

	}



/* &<@anonymous:2259> */
	obj_t BGl_z62zc3z04anonymousza32259ze3ze5zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6494, obj_t BgL_exz00_6495, obj_t BgL_eyz00_6496)
	{
		{	/* SawBbv/bbv-merge.scm 321 */
			if (
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
							((BgL_bbvzd2ctxentryzd2_bglt) BgL_exz00_6495)))->BgL_polarityz00))
				{	/* SawBbv/bbv-merge.scm 327 */
					if (
						(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_eyz00_6496)))->
							BgL_polarityz00))
						{	/* SawBbv/bbv-merge.scm 329 */
							obj_t BgL_arg2262z00_6696;
							obj_t BgL_arg2263z00_6697;

							BgL_arg2262z00_6696 =
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_exz00_6495)))->
								BgL_typesz00);
							BgL_arg2263z00_6697 =
								(((BgL_bbvzd2ctxentryzd2_bglt)
									COBJECT(((BgL_bbvzd2ctxentryzd2_bglt) BgL_eyz00_6496)))->
								BgL_typesz00);
							{	/* SawBbv/bbv-merge.scm 314 */
								obj_t BgL_list2251z00_6698;

								{	/* SawBbv/bbv-merge.scm 314 */
									obj_t BgL_arg2252z00_6699;

									BgL_arg2252z00_6699 =
										MAKE_YOUNG_PAIR(BgL_arg2263z00_6697, BNIL);
									BgL_list2251z00_6698 =
										MAKE_YOUNG_PAIR(BgL_arg2262z00_6696, BgL_arg2252z00_6699);
								}
								return
									BGl_everyz00zz__r4_pairs_and_lists_6_3z00
									(BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00,
									BgL_list2251z00_6698);
							}
						}
					else
						{	/* SawBbv/bbv-merge.scm 328 */
							return BFALSE;
						}
				}
			else
				{	/* SawBbv/bbv-merge.scm 327 */
					if (
						(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_eyz00_6496)))->
							BgL_polarityz00))
						{	/* SawBbv/bbv-merge.scm 327 */
							return BFALSE;
						}
					else
						{	/* SawBbv/bbv-merge.scm 327 */
							return BTRUE;
						}
				}
		}

	}



/* bbv-block-merge-select-strategy-score2 */
	obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2score2zd2zzsaw_bbvzd2mergezd2
		(obj_t BgL_bsz00_261)
	{
		{	/* SawBbv/bbv-merge.scm 379 */
			{
				obj_t BgL_xz00_2903;
				obj_t BgL_yz00_2904;
				BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_2936;
				obj_t BgL_bsz00_2960;

				{
					obj_t BgL_pbsz00_2878;

					BgL_pbsz00_2878 = BgL_bsz00_261;
				BgL_zc3z04anonymousza32298ze3z87_2879:
					if (NULLP(BgL_pbsz00_2878))
						{	/* SawBbv/bbv-merge.scm 431 */
							BgL_bsz00_2960 = BgL_bsz00_261;
							{	/* SawBbv/bbv-merge.scm 416 */
								obj_t BgL_bsz00_2962;

								{	/* SawBbv/bbv-merge.scm 416 */
									obj_t BgL_arg2348z00_2974;

									{	/* SawBbv/bbv-merge.scm 418 */
										obj_t BgL_head1663z00_2983;

										BgL_head1663z00_2983 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1661z00_2985;
											obj_t BgL_tail1664z00_2986;

											BgL_l1661z00_2985 = BgL_bsz00_2960;
											BgL_tail1664z00_2986 = BgL_head1663z00_2983;
										BgL_zc3z04anonymousza32352ze3z87_2987:
											if (NULLP(BgL_l1661z00_2985))
												{	/* SawBbv/bbv-merge.scm 418 */
													BgL_arg2348z00_2974 = CDR(BgL_head1663z00_2983);
												}
											else
												{	/* SawBbv/bbv-merge.scm 418 */
													obj_t BgL_newtail1665z00_2989;

													{	/* SawBbv/bbv-merge.scm 418 */
														obj_t BgL_arg2355z00_2991;

														{	/* SawBbv/bbv-merge.scm 418 */
															obj_t BgL_bz00_2992;

															BgL_bz00_2992 = CAR(((obj_t) BgL_l1661z00_2985));
															{	/* SawBbv/bbv-merge.scm 418 */
																obj_t BgL_arg2356z00_2993;

																{	/* SawBbv/bbv-merge.scm 413 */
																	BgL_bbvzd2ctxzd2_bglt BgL_arg2341z00_5185;

																	{
																		BgL_blocksz00_bglt BgL_auxz00_8987;

																		{
																			obj_t BgL_auxz00_8988;

																			{	/* SawBbv/bbv-merge.scm 413 */
																				BgL_objectz00_bglt BgL_tmpz00_8989;

																				BgL_tmpz00_8989 =
																					((BgL_objectz00_bglt)
																					((BgL_blockz00_bglt) BgL_bz00_2992));
																				BgL_auxz00_8988 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_8989);
																			}
																			BgL_auxz00_8987 =
																				((BgL_blocksz00_bglt) BgL_auxz00_8988);
																		}
																		BgL_arg2341z00_5185 =
																			(((BgL_blocksz00_bglt)
																				COBJECT(BgL_auxz00_8987))->BgL_ctxz00);
																	}
																	BgL_ctxz00_2936 = BgL_arg2341z00_5185;
																	{	/* SawBbv/bbv-merge.scm 409 */
																		obj_t BgL_runner2339z00_2955;

																		{	/* SawBbv/bbv-merge.scm 409 */
																			obj_t BgL_l1653z00_2939;

																			BgL_l1653z00_2939 =
																				(((BgL_bbvzd2ctxzd2_bglt)
																					COBJECT(BgL_ctxz00_2936))->
																				BgL_entriesz00);
																			if (NULLP(BgL_l1653z00_2939))
																				{	/* SawBbv/bbv-merge.scm 409 */
																					BgL_runner2339z00_2955 = BNIL;
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 409 */
																					obj_t BgL_head1655z00_2941;

																					{	/* SawBbv/bbv-merge.scm 409 */
																						obj_t BgL_arg2337z00_2953;

																						{	/* SawBbv/bbv-merge.scm 409 */
																							obj_t BgL_arg2338z00_2954;

																							BgL_arg2338z00_2954 =
																								CAR(BgL_l1653z00_2939);
																							BgL_arg2337z00_2953 =
																								BGl_entryzd2scoreze70z35zzsaw_bbvzd2mergezd2
																								(((BgL_bbvzd2ctxentryzd2_bglt)
																									BgL_arg2338z00_2954));
																						}
																						BgL_head1655z00_2941 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2337z00_2953, BNIL);
																					}
																					{	/* SawBbv/bbv-merge.scm 409 */
																						obj_t BgL_g1658z00_2942;

																						BgL_g1658z00_2942 =
																							CDR(BgL_l1653z00_2939);
																						{
																							obj_t BgL_l1653z00_2944;
																							obj_t BgL_tail1656z00_2945;

																							BgL_l1653z00_2944 =
																								BgL_g1658z00_2942;
																							BgL_tail1656z00_2945 =
																								BgL_head1655z00_2941;
																						BgL_zc3z04anonymousza32331ze3z87_2946:
																							if (NULLP
																								(BgL_l1653z00_2944))
																								{	/* SawBbv/bbv-merge.scm 409 */
																									BgL_runner2339z00_2955 =
																										BgL_head1655z00_2941;
																								}
																							else
																								{	/* SawBbv/bbv-merge.scm 409 */
																									obj_t BgL_newtail1657z00_2948;

																									{	/* SawBbv/bbv-merge.scm 409 */
																										obj_t BgL_arg2335z00_2950;

																										{	/* SawBbv/bbv-merge.scm 409 */
																											obj_t BgL_arg2336z00_2951;

																											BgL_arg2336z00_2951 =
																												CAR(
																												((obj_t)
																													BgL_l1653z00_2944));
																											BgL_arg2335z00_2950 =
																												BGl_entryzd2scoreze70z35zzsaw_bbvzd2mergezd2
																												(((BgL_bbvzd2ctxentryzd2_bglt) BgL_arg2336z00_2951));
																										}
																										BgL_newtail1657z00_2948 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg2335z00_2950,
																											BNIL);
																									}
																									SET_CDR(BgL_tail1656z00_2945,
																										BgL_newtail1657z00_2948);
																									{	/* SawBbv/bbv-merge.scm 409 */
																										obj_t BgL_arg2333z00_2949;

																										BgL_arg2333z00_2949 =
																											CDR(
																											((obj_t)
																												BgL_l1653z00_2944));
																										{
																											obj_t
																												BgL_tail1656z00_9014;
																											obj_t BgL_l1653z00_9013;

																											BgL_l1653z00_9013 =
																												BgL_arg2333z00_2949;
																											BgL_tail1656z00_9014 =
																												BgL_newtail1657z00_2948;
																											BgL_tail1656z00_2945 =
																												BgL_tail1656z00_9014;
																											BgL_l1653z00_2944 =
																												BgL_l1653z00_9013;
																											goto
																												BgL_zc3z04anonymousza32331ze3z87_2946;
																										}
																									}
																								}
																						}
																					}
																				}
																		}
																		BgL_arg2356z00_2993 =
																			BGl_zb2zb2zz__r4_numbers_6_5z00
																			(BgL_runner2339z00_2955);
																	}
																}
																BgL_arg2355z00_2991 =
																	MAKE_YOUNG_PAIR(BgL_arg2356z00_2993,
																	BgL_bz00_2992);
															}
														}
														BgL_newtail1665z00_2989 =
															MAKE_YOUNG_PAIR(BgL_arg2355z00_2991, BNIL);
													}
													SET_CDR(BgL_tail1664z00_2986,
														BgL_newtail1665z00_2989);
													{	/* SawBbv/bbv-merge.scm 418 */
														obj_t BgL_arg2354z00_2990;

														BgL_arg2354z00_2990 =
															CDR(((obj_t) BgL_l1661z00_2985));
														{
															obj_t BgL_tail1664z00_9022;
															obj_t BgL_l1661z00_9021;

															BgL_l1661z00_9021 = BgL_arg2354z00_2990;
															BgL_tail1664z00_9022 = BgL_newtail1665z00_2989;
															BgL_tail1664z00_2986 = BgL_tail1664z00_9022;
															BgL_l1661z00_2985 = BgL_l1661z00_9021;
															goto BgL_zc3z04anonymousza32352ze3z87_2987;
														}
													}
												}
										}
									}
									BgL_bsz00_2962 =
										BGl_sortz00zz__r4_vectors_6_8z00
										(BGl_proc3293z00zzsaw_bbvzd2mergezd2, BgL_arg2348z00_2974);
								}
								{	/* SawBbv/bbv-merge.scm 427 */
									obj_t BgL_val0_1668z00_2971;
									obj_t BgL_val1_1669z00_2972;

									{	/* SawBbv/bbv-merge.scm 427 */
										obj_t BgL_pairz00_5199;

										BgL_pairz00_5199 = CAR(((obj_t) BgL_bsz00_2962));
										BgL_val0_1668z00_2971 = CDR(BgL_pairz00_5199);
									}
									{	/* SawBbv/bbv-merge.scm 427 */
										obj_t BgL_pairz00_5205;

										{	/* SawBbv/bbv-merge.scm 427 */
											obj_t BgL_pairz00_5204;

											BgL_pairz00_5204 = CDR(((obj_t) BgL_bsz00_2962));
											BgL_pairz00_5205 = CAR(BgL_pairz00_5204);
										}
										BgL_val1_1669z00_2972 = CDR(BgL_pairz00_5205);
									}
									{	/* SawBbv/bbv-merge.scm 427 */
										int BgL_tmpz00_9031;

										BgL_tmpz00_9031 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9031);
									}
									{	/* SawBbv/bbv-merge.scm 427 */
										int BgL_tmpz00_9034;

										BgL_tmpz00_9034 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_9034, BgL_val1_1669z00_2972);
									}
									return BgL_val0_1668z00_2971;
								}
							}
						}
					else
						{	/* SawBbv/bbv-merge.scm 433 */
							obj_t BgL_sbz00_2881;

							{	/* SawBbv/bbv-merge.scm 433 */
								obj_t BgL_g1673z00_2885;

								BgL_g1673z00_2885 = CDR(((obj_t) BgL_pbsz00_2878));
								{
									obj_t BgL_list1672z00_2887;

									BgL_list1672z00_2887 = BgL_g1673z00_2885;
								BgL_zc3z04anonymousza32302ze3z87_2888:
									if (PAIRP(BgL_list1672z00_2887))
										{	/* SawBbv/bbv-merge.scm 435 */
											bool_t BgL_test3593z00_9041;

											{	/* SawBbv/bbv-merge.scm 434 */
												obj_t BgL_bz00_2894;

												BgL_bz00_2894 = CAR(BgL_list1672z00_2887);
												{	/* SawBbv/bbv-merge.scm 434 */
													obj_t BgL_arg2307z00_2895;

													BgL_arg2307z00_2895 = CAR(((obj_t) BgL_pbsz00_2878));
													{	/* SawBbv/bbv-merge.scm 434 */
														obj_t BgL_tmpz00_9045;

														BgL_xz00_2903 = BgL_arg2307z00_2895;
														BgL_yz00_2904 = BgL_bz00_2894;
														{	/* SawBbv/bbv-merge.scm 387 */
															BgL_bbvzd2ctxzd2_bglt BgL_i1223z00_2908;

															{
																BgL_blocksz00_bglt BgL_auxz00_9046;

																{
																	obj_t BgL_auxz00_9047;

																	{	/* SawBbv/bbv-merge.scm 388 */
																		BgL_objectz00_bglt BgL_tmpz00_9048;

																		BgL_tmpz00_9048 =
																			((BgL_objectz00_bglt)
																			((BgL_blockz00_bglt) BgL_xz00_2903));
																		BgL_auxz00_9047 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_9048);
																	}
																	BgL_auxz00_9046 =
																		((BgL_blocksz00_bglt) BgL_auxz00_9047);
																}
																BgL_i1223z00_2908 =
																	(((BgL_blocksz00_bglt)
																		COBJECT(BgL_auxz00_9046))->BgL_ctxz00);
															}
															{	/* SawBbv/bbv-merge.scm 388 */
																BgL_bbvzd2ctxzd2_bglt BgL_i1224z00_2909;

																{
																	BgL_blocksz00_bglt BgL_auxz00_9054;

																	{
																		obj_t BgL_auxz00_9055;

																		{	/* SawBbv/bbv-merge.scm 389 */
																			BgL_objectz00_bglt BgL_tmpz00_9056;

																			BgL_tmpz00_9056 =
																				((BgL_objectz00_bglt)
																				((BgL_blockz00_bglt) BgL_yz00_2904));
																			BgL_auxz00_9055 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_9056);
																		}
																		BgL_auxz00_9054 =
																			((BgL_blocksz00_bglt) BgL_auxz00_9055);
																	}
																	BgL_i1224z00_2909 =
																		(((BgL_blocksz00_bglt)
																			COBJECT(BgL_auxz00_9054))->BgL_ctxz00);
																}
																{	/* SawBbv/bbv-merge.scm 389 */
																	obj_t BgL_arg2313z00_2911;
																	obj_t BgL_arg2314z00_2912;

																	BgL_arg2313z00_2911 =
																		(((BgL_bbvzd2ctxzd2_bglt)
																			COBJECT(BgL_i1223z00_2908))->
																		BgL_entriesz00);
																	BgL_arg2314z00_2912 =
																		(((BgL_bbvzd2ctxzd2_bglt)
																			COBJECT(BgL_i1224z00_2909))->
																		BgL_entriesz00);
																	{	/* SawBbv/bbv-merge.scm 389 */
																		obj_t BgL_list2315z00_2913;

																		{	/* SawBbv/bbv-merge.scm 389 */
																			obj_t BgL_arg2316z00_2914;

																			BgL_arg2316z00_2914 =
																				MAKE_YOUNG_PAIR(BgL_arg2314z00_2912,
																				BNIL);
																			BgL_list2315z00_2913 =
																				MAKE_YOUNG_PAIR(BgL_arg2313z00_2911,
																				BgL_arg2316z00_2914);
																		}
																		BgL_tmpz00_9045 =
																			BGl_everyz00zz__r4_pairs_and_lists_6_3z00
																			(BGl_proc3294z00zzsaw_bbvzd2mergezd2,
																			BgL_list2315z00_2913);
																	}
																}
															}
														}
														BgL_test3593z00_9041 = CBOOL(BgL_tmpz00_9045);
													}
												}
											}
											if (BgL_test3593z00_9041)
												{	/* SawBbv/bbv-merge.scm 435 */
													BgL_sbz00_2881 = CAR(BgL_list1672z00_2887);
												}
											else
												{
													obj_t BgL_list1672z00_9069;

													BgL_list1672z00_9069 = CDR(BgL_list1672z00_2887);
													BgL_list1672z00_2887 = BgL_list1672z00_9069;
													goto BgL_zc3z04anonymousza32302ze3z87_2888;
												}
										}
									else
										{	/* SawBbv/bbv-merge.scm 435 */
											BgL_sbz00_2881 = BFALSE;
										}
								}
							}
							if (CBOOL(BgL_sbz00_2881))
								{	/* SawBbv/bbv-merge.scm 441 */
									obj_t BgL_val0_1674z00_2882;

									BgL_val0_1674z00_2882 = CAR(((obj_t) BgL_pbsz00_2878));
									{	/* SawBbv/bbv-merge.scm 441 */
										int BgL_tmpz00_9075;

										BgL_tmpz00_9075 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9075);
									}
									{	/* SawBbv/bbv-merge.scm 441 */
										int BgL_tmpz00_9078;

										BgL_tmpz00_9078 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_9078, BgL_sbz00_2881);
									}
									return BgL_val0_1674z00_2882;
								}
							else
								{	/* SawBbv/bbv-merge.scm 442 */
									obj_t BgL_arg2301z00_2884;

									BgL_arg2301z00_2884 = CDR(((obj_t) BgL_pbsz00_2878));
									{
										obj_t BgL_pbsz00_9083;

										BgL_pbsz00_9083 = BgL_arg2301z00_2884;
										BgL_pbsz00_2878 = BgL_pbsz00_9083;
										goto BgL_zc3z04anonymousza32298ze3z87_2879;
									}
								}
						}
				}
			}
		}

	}



/* entry-score~0 */
	obj_t BGl_entryzd2scoreze70z35zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt
		BgL_ez00_2926)
	{
		{	/* SawBbv/bbv-merge.scm 405 */
			if (
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2926))->
					BgL_polarityz00))
				{	/* SawBbv/bbv-merge.scm 404 */
					bool_t BgL_test3596z00_9086;

					{	/* SawBbv/bbv-merge.scm 404 */
						obj_t BgL_arg2328z00_2935;

						BgL_arg2328z00_2935 =
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2926))->
							BgL_typesz00);
						BgL_test3596z00_9086 =
							CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
							(BGl_za2objza2z00zztype_cachez00, BgL_arg2328z00_2935));
					}
					if (BgL_test3596z00_9086)
						{	/* SawBbv/bbv-merge.scm 404 */
							return BINT(0L);
						}
					else
						{	/* SawBbv/bbv-merge.scm 405 */
							long BgL_a1651z00_2932;

							BgL_a1651z00_2932 =
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2926))->
								BgL_countz00);
							{	/* SawBbv/bbv-merge.scm 405 */
								long BgL_b1652z00_2933;

								BgL_b1652z00_2933 =
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_2926))->
									BgL_countz00);
								{	/* SawBbv/bbv-merge.scm 405 */

									{	/* SawBbv/bbv-merge.scm 405 */
										obj_t BgL_za71za7_5163;
										obj_t BgL_za72za7_5164;

										BgL_za71za7_5163 = BINT(BgL_a1651z00_2932);
										BgL_za72za7_5164 = BINT(BgL_b1652z00_2933);
										{	/* SawBbv/bbv-merge.scm 405 */
											obj_t BgL_tmpz00_5165;

											BgL_tmpz00_5165 = BINT(0L);
											{	/* SawBbv/bbv-merge.scm 405 */
												bool_t BgL_test3597z00_9096;

												{	/* SawBbv/bbv-merge.scm 405 */
													long BgL_tmpz00_9097;

													BgL_tmpz00_9097 = (long) CINT(BgL_za72za7_5164);
													BgL_test3597z00_9096 =
														BGL_MULFX_OV(BgL_za71za7_5163, BgL_tmpz00_9097,
														BgL_tmpz00_5165);
												}
												if (BgL_test3597z00_9096)
													{	/* SawBbv/bbv-merge.scm 405 */
														return
															bgl_bignum_mul(bgl_long_to_bignum(
																(long) CINT(BgL_za71za7_5163)),
															bgl_long_to_bignum(
																(long) CINT(BgL_za72za7_5164)));
													}
												else
													{	/* SawBbv/bbv-merge.scm 405 */
														return BgL_tmpz00_5165;
													}
											}
										}
									}
								}
							}
						}
				}
			else
				{	/* SawBbv/bbv-merge.scm 403 */
					return BINT(1L);
				}
		}

	}



/* &<@anonymous:2349> */
	obj_t BGl_z62zc3z04anonymousza32349ze3ze5zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6499, obj_t BgL_xz00_6500, obj_t BgL_yz00_6501)
	{
		{	/* SawBbv/bbv-merge.scm 416 */
			{	/* SawBbv/bbv-merge.scm 417 */
				bool_t BgL_tmpz00_9106;

				{	/* SawBbv/bbv-merge.scm 417 */
					obj_t BgL_a1659z00_6700;

					BgL_a1659z00_6700 = CAR(((obj_t) BgL_xz00_6500));
					{	/* SawBbv/bbv-merge.scm 417 */
						obj_t BgL_b1660z00_6701;

						BgL_b1660z00_6701 = CAR(((obj_t) BgL_yz00_6501));
						{	/* SawBbv/bbv-merge.scm 417 */

							{	/* SawBbv/bbv-merge.scm 417 */
								bool_t BgL_test3598z00_9111;

								if (INTEGERP(BgL_a1659z00_6700))
									{	/* SawBbv/bbv-merge.scm 417 */
										BgL_test3598z00_9111 = INTEGERP(BgL_b1660z00_6701);
									}
								else
									{	/* SawBbv/bbv-merge.scm 417 */
										BgL_test3598z00_9111 = ((bool_t) 0);
									}
								if (BgL_test3598z00_9111)
									{	/* SawBbv/bbv-merge.scm 417 */
										BgL_tmpz00_9106 =
											(
											(long) CINT(BgL_a1659z00_6700) <=
											(long) CINT(BgL_b1660z00_6701));
									}
								else
									{	/* SawBbv/bbv-merge.scm 417 */
										BgL_tmpz00_9106 =
											BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BgL_a1659z00_6700,
											BgL_b1660z00_6701);
									}
							}
						}
					}
				}
				return BBOOL(BgL_tmpz00_9106);
			}
		}

	}



/* &<@anonymous:2317> */
	obj_t BGl_z62zc3z04anonymousza32317ze3ze5zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6502, obj_t BgL_exz00_6503, obj_t BgL_eyz00_6504)
	{
		{	/* SawBbv/bbv-merge.scm 389 */
			if (
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
							((BgL_bbvzd2ctxentryzd2_bglt) BgL_exz00_6503)))->BgL_polarityz00))
				{	/* SawBbv/bbv-merge.scm 395 */
					if (
						(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_eyz00_6504)))->
							BgL_polarityz00))
						{	/* SawBbv/bbv-merge.scm 397 */
							obj_t BgL_arg2320z00_6702;
							obj_t BgL_arg2321z00_6703;

							BgL_arg2320z00_6702 =
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_exz00_6503)))->
								BgL_typesz00);
							BgL_arg2321z00_6703 =
								(((BgL_bbvzd2ctxentryzd2_bglt)
									COBJECT(((BgL_bbvzd2ctxentryzd2_bglt) BgL_eyz00_6504)))->
								BgL_typesz00);
							{	/* SawBbv/bbv-merge.scm 382 */
								obj_t BgL_list2309z00_6704;

								{	/* SawBbv/bbv-merge.scm 382 */
									obj_t BgL_arg2310z00_6705;

									BgL_arg2310z00_6705 =
										MAKE_YOUNG_PAIR(BgL_arg2321z00_6703, BNIL);
									BgL_list2309z00_6704 =
										MAKE_YOUNG_PAIR(BgL_arg2320z00_6702, BgL_arg2310z00_6705);
								}
								return
									BGl_everyz00zz__r4_pairs_and_lists_6_3z00
									(BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00,
									BgL_list2309z00_6704);
							}
						}
					else
						{	/* SawBbv/bbv-merge.scm 396 */
							return BFALSE;
						}
				}
			else
				{	/* SawBbv/bbv-merge.scm 395 */
					if (
						(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_eyz00_6504)))->
							BgL_polarityz00))
						{	/* SawBbv/bbv-merge.scm 395 */
							return BFALSE;
						}
					else
						{	/* SawBbv/bbv-merge.scm 395 */
							return BTRUE;
						}
				}
		}

	}



/* bbv-block-merge-select-strategy-nearobj */
	obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2nearobjzd2zzsaw_bbvzd2mergezd2
		(obj_t BgL_bsz00_262)
	{
		{	/* SawBbv/bbv-merge.scm 451 */
			{
				BgL_blockz00_bglt BgL_bz00_3077;
				BgL_blockz00_bglt BgL_bz00_3058;

				{	/* SawBbv/bbv-merge.scm 476 */
					obj_t BgL_bnbz00_3011;

					{	/* SawBbv/bbv-merge.scm 476 */
						obj_t BgL_hook1690z00_3044;

						BgL_hook1690z00_3044 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
						{
							obj_t BgL_l1687z00_3046;
							obj_t BgL_h1688z00_3047;

							BgL_l1687z00_3046 = BgL_bsz00_262;
							BgL_h1688z00_3047 = BgL_hook1690z00_3044;
						BgL_zc3z04anonymousza32379ze3z87_3048:
							if (NULLP(BgL_l1687z00_3046))
								{	/* SawBbv/bbv-merge.scm 476 */
									BgL_bnbz00_3011 = CDR(BgL_hook1690z00_3044);
								}
							else
								{	/* SawBbv/bbv-merge.scm 476 */
									bool_t BgL_test3604z00_9140;

									BgL_bz00_3058 =
										((BgL_blockz00_bglt) CAR(((obj_t) BgL_l1687z00_3046)));
									{	/* SawBbv/bbv-merge.scm 455 */
										BgL_bbvzd2ctxzd2_bglt BgL_i1232z00_3061;

										{
											BgL_blocksz00_bglt BgL_auxz00_9141;

											{
												obj_t BgL_auxz00_9142;

												{	/* SawBbv/bbv-merge.scm 456 */
													BgL_objectz00_bglt BgL_tmpz00_9143;

													BgL_tmpz00_9143 =
														((BgL_objectz00_bglt) BgL_bz00_3058);
													BgL_auxz00_9142 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_9143);
												}
												BgL_auxz00_9141 =
													((BgL_blocksz00_bglt) BgL_auxz00_9142);
											}
											BgL_i1232z00_3061 =
												(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_9141))->
												BgL_ctxz00);
										}
										{
											obj_t BgL_l1676z00_3064;

											BgL_l1676z00_3064 =
												(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_i1232z00_3061))->
												BgL_entriesz00);
										BgL_zc3z04anonymousza32388ze3z87_3065:
											if (NULLP(BgL_l1676z00_3064))
												{	/* SawBbv/bbv-merge.scm 456 */
													BgL_test3604z00_9140 = ((bool_t) 1);
												}
											else
												{	/* SawBbv/bbv-merge.scm 456 */
													bool_t BgL_test3606z00_9150;

													{	/* SawBbv/bbv-merge.scm 457 */
														BgL_bbvzd2ctxentryzd2_bglt BgL_ez00_3070;

														BgL_ez00_3070 =
															((BgL_bbvzd2ctxentryzd2_bglt)
															CAR(((obj_t) BgL_l1676z00_3064)));
														{	/* SawBbv/bbv-merge.scm 458 */
															bool_t BgL__ortest_1234z00_3072;

															{	/* SawBbv/bbv-merge.scm 458 */
																obj_t BgL_arg2392z00_3074;

																BgL_arg2392z00_3074 =
																	CAR(
																	(((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_ez00_3070))->BgL_typesz00));
																BgL__ortest_1234z00_3072 =
																	(BgL_arg2392z00_3074 ==
																	BGl_za2objza2z00zztype_cachez00);
															}
															if (BgL__ortest_1234z00_3072)
																{	/* SawBbv/bbv-merge.scm 458 */
																	BgL_test3606z00_9150 =
																		BgL__ortest_1234z00_3072;
																}
															else
																{	/* SawBbv/bbv-merge.scm 458 */
																	if (
																		(((BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(BgL_ez00_3070))->
																			BgL_polarityz00))
																		{	/* SawBbv/bbv-merge.scm 458 */
																			BgL_test3606z00_9150 = ((bool_t) 0);
																		}
																	else
																		{	/* SawBbv/bbv-merge.scm 458 */
																			BgL_test3606z00_9150 = ((bool_t) 1);
																		}
																}
														}
													}
													if (BgL_test3606z00_9150)
														{
															obj_t BgL_l1676z00_9160;

															BgL_l1676z00_9160 =
																CDR(((obj_t) BgL_l1676z00_3064));
															BgL_l1676z00_3064 = BgL_l1676z00_9160;
															goto BgL_zc3z04anonymousza32388ze3z87_3065;
														}
													else
														{	/* SawBbv/bbv-merge.scm 456 */
															BgL_test3604z00_9140 = ((bool_t) 0);
														}
												}
										}
									}
									if (BgL_test3604z00_9140)
										{	/* SawBbv/bbv-merge.scm 476 */
											obj_t BgL_nh1689z00_3052;

											{	/* SawBbv/bbv-merge.scm 476 */
												obj_t BgL_arg2384z00_3054;

												BgL_arg2384z00_3054 = CAR(((obj_t) BgL_l1687z00_3046));
												BgL_nh1689z00_3052 =
													MAKE_YOUNG_PAIR(BgL_arg2384z00_3054, BNIL);
											}
											SET_CDR(BgL_h1688z00_3047, BgL_nh1689z00_3052);
											{	/* SawBbv/bbv-merge.scm 476 */
												obj_t BgL_arg2383z00_3053;

												BgL_arg2383z00_3053 = CDR(((obj_t) BgL_l1687z00_3046));
												{
													obj_t BgL_h1688z00_9174;
													obj_t BgL_l1687z00_9173;

													BgL_l1687z00_9173 = BgL_arg2383z00_3053;
													BgL_h1688z00_9174 = BgL_nh1689z00_3052;
													BgL_h1688z00_3047 = BgL_h1688z00_9174;
													BgL_l1687z00_3046 = BgL_l1687z00_9173;
													goto BgL_zc3z04anonymousza32379ze3z87_3048;
												}
											}
										}
									else
										{	/* SawBbv/bbv-merge.scm 476 */
											obj_t BgL_arg2385z00_3055;

											BgL_arg2385z00_3055 = CDR(((obj_t) BgL_l1687z00_3046));
											{
												obj_t BgL_l1687z00_9177;

												BgL_l1687z00_9177 = BgL_arg2385z00_3055;
												BgL_l1687z00_3046 = BgL_l1687z00_9177;
												goto BgL_zc3z04anonymousza32379ze3z87_3048;
											}
										}
								}
						}
					}
					{	/* SawBbv/bbv-merge.scm 477 */
						bool_t BgL_test3609z00_9178;

						if (PAIRP(BgL_bnbz00_3011))
							{	/* SawBbv/bbv-merge.scm 477 */
								obj_t BgL_tmpz00_9181;

								BgL_tmpz00_9181 = CDR(BgL_bnbz00_3011);
								BgL_test3609z00_9178 = PAIRP(BgL_tmpz00_9181);
							}
						else
							{	/* SawBbv/bbv-merge.scm 477 */
								BgL_test3609z00_9178 = ((bool_t) 0);
							}
						if (BgL_test3609z00_9178)
							{	/* SawBbv/bbv-merge.scm 478 */
								obj_t BgL_fbz00_3015;

								{
									obj_t BgL_list1692z00_3034;

									BgL_list1692z00_3034 = BgL_bsz00_262;
								BgL_zc3z04anonymousza32372ze3z87_3035:
									if (PAIRP(BgL_list1692z00_3034))
										{	/* SawBbv/bbv-merge.scm 478 */
											bool_t BgL_test3612z00_9186;

											BgL_bz00_3077 =
												((BgL_blockz00_bglt) CAR(BgL_list1692z00_3034));
											{	/* SawBbv/bbv-merge.scm 463 */
												BgL_bbvzd2ctxzd2_bglt BgL_i1236z00_3080;

												{
													BgL_blocksz00_bglt BgL_auxz00_9187;

													{
														obj_t BgL_auxz00_9188;

														{	/* SawBbv/bbv-merge.scm 464 */
															BgL_objectz00_bglt BgL_tmpz00_9189;

															BgL_tmpz00_9189 =
																((BgL_objectz00_bglt) BgL_bz00_3077);
															BgL_auxz00_9188 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_9189);
														}
														BgL_auxz00_9187 =
															((BgL_blocksz00_bglt) BgL_auxz00_9188);
													}
													BgL_i1236z00_3080 =
														(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_9187))->
														BgL_ctxz00);
												}
												{
													obj_t BgL_l1680z00_3083;

													BgL_l1680z00_3083 =
														(((BgL_bbvzd2ctxzd2_bglt)
															COBJECT(BgL_i1236z00_3080))->BgL_entriesz00);
												BgL_zc3z04anonymousza32395ze3z87_3084:
													if (NULLP(BgL_l1680z00_3083))
														{	/* SawBbv/bbv-merge.scm 464 */
															BgL_test3612z00_9186 = ((bool_t) 1);
														}
													else
														{	/* SawBbv/bbv-merge.scm 464 */
															bool_t BgL_test3614z00_9196;

															{	/* SawBbv/bbv-merge.scm 465 */
																BgL_bbvzd2ctxentryzd2_bglt BgL_ez00_3089;

																BgL_ez00_3089 =
																	((BgL_bbvzd2ctxentryzd2_bglt)
																	CAR(((obj_t) BgL_l1680z00_3083)));
																{	/* SawBbv/bbv-merge.scm 466 */
																	obj_t BgL_arg2398z00_3091;

																	BgL_arg2398z00_3091 =
																		CAR(
																		(((BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(BgL_ez00_3089))->BgL_typesz00));
																	BgL_test3614z00_9196 =
																		(BgL_arg2398z00_3091 ==
																		BGl_za2objza2z00zztype_cachez00);
																}
															}
															if (BgL_test3614z00_9196)
																{
																	obj_t BgL_l1680z00_9203;

																	BgL_l1680z00_9203 =
																		CDR(((obj_t) BgL_l1680z00_3083));
																	BgL_l1680z00_3083 = BgL_l1680z00_9203;
																	goto BgL_zc3z04anonymousza32395ze3z87_3084;
																}
															else
																{	/* SawBbv/bbv-merge.scm 464 */
																	BgL_test3612z00_9186 = ((bool_t) 0);
																}
														}
												}
											}
											if (BgL_test3612z00_9186)
												{	/* SawBbv/bbv-merge.scm 478 */
													BgL_fbz00_3015 = CAR(BgL_list1692z00_3034);
												}
											else
												{
													obj_t BgL_list1692z00_9210;

													BgL_list1692z00_9210 = CDR(BgL_list1692z00_3034);
													BgL_list1692z00_3034 = BgL_list1692z00_9210;
													goto BgL_zc3z04anonymousza32372ze3z87_3035;
												}
										}
									else
										{	/* SawBbv/bbv-merge.scm 478 */
											BgL_fbz00_3015 = BFALSE;
										}
								}
								if (CBOOL(BgL_fbz00_3015))
									{	/* SawBbv/bbv-merge.scm 481 */
										obj_t BgL_nfbz00_3016;

										{
											obj_t BgL_list1694z00_3020;

											BgL_list1694z00_3020 = BgL_bnbz00_3011;
										BgL_zc3z04anonymousza32365ze3z87_3021:
											if (PAIRP(BgL_list1694z00_3020))
												{	/* SawBbv/bbv-merge.scm 481 */
													bool_t BgL_test3617z00_9216;

													if ((CAR(BgL_list1694z00_3020) == BgL_fbz00_3015))
														{	/* SawBbv/bbv-merge.scm 481 */
															BgL_test3617z00_9216 = ((bool_t) 0);
														}
													else
														{	/* SawBbv/bbv-merge.scm 481 */
															BgL_test3617z00_9216 = ((bool_t) 1);
														}
													if (BgL_test3617z00_9216)
														{	/* SawBbv/bbv-merge.scm 481 */
															BgL_nfbz00_3016 = CAR(BgL_list1694z00_3020);
														}
													else
														{
															obj_t BgL_list1694z00_9221;

															BgL_list1694z00_9221 = CDR(BgL_list1694z00_3020);
															BgL_list1694z00_3020 = BgL_list1694z00_9221;
															goto BgL_zc3z04anonymousza32365ze3z87_3021;
														}
												}
											else
												{	/* SawBbv/bbv-merge.scm 481 */
													BgL_nfbz00_3016 = BFALSE;
												}
										}
										{	/* SawBbv/bbv-merge.scm 485 */
											int BgL_tmpz00_9223;

											BgL_tmpz00_9223 = (int) (2L);
											BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9223);
										}
										{	/* SawBbv/bbv-merge.scm 485 */
											int BgL_tmpz00_9226;

											BgL_tmpz00_9226 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_9226, BgL_fbz00_3015);
										}
										return BgL_nfbz00_3016;
									}
								else
									{	/* SawBbv/bbv-merge.scm 486 */
										bool_t BgL_test3619z00_9229;

										{	/* SawBbv/bbv-merge.scm 486 */
											obj_t BgL_tmpz00_9230;

											BgL_tmpz00_9230 = CDR(BgL_bnbz00_3011);
											BgL_test3619z00_9229 = PAIRP(BgL_tmpz00_9230);
										}
										if (BgL_test3619z00_9229)
											{	/* SawBbv/bbv-merge.scm 490 */
												obj_t BgL_val0_1697z00_3030;
												obj_t BgL_val1_1698z00_3031;

												BgL_val0_1697z00_3030 = CAR(BgL_bnbz00_3011);
												BgL_val1_1698z00_3031 = CAR(CDR(BgL_bnbz00_3011));
												{	/* SawBbv/bbv-merge.scm 490 */
													int BgL_tmpz00_9236;

													BgL_tmpz00_9236 = (int) (2L);
													BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9236);
												}
												{	/* SawBbv/bbv-merge.scm 490 */
													int BgL_tmpz00_9239;

													BgL_tmpz00_9239 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_9239,
														BgL_val1_1698z00_3031);
												}
												return BgL_val0_1697z00_3030;
											}
										else
											{	/* SawBbv/bbv-merge.scm 486 */
												BGL_TAIL return
													BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2samepositivezd2zzsaw_bbvzd2mergezd2
													(BgL_bsz00_262);
											}
									}
							}
						else
							{	/* SawBbv/bbv-merge.scm 477 */
								BGL_TAIL return
									BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2samepositivezd2zzsaw_bbvzd2mergezd2
									(BgL_bsz00_262);
							}
					}
				}
			}
		}

	}



/* bbv-block-merge-select-strategy-samepositive */
	obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2samepositivezd2zzsaw_bbvzd2mergezd2
		(obj_t BgL_bsz00_263)
	{
		{	/* SawBbv/bbv-merge.scm 501 */
			{
				obj_t BgL_xz00_3124;
				obj_t BgL_yz00_3125;

				{
					obj_t BgL_pbsz00_3099;

					BgL_pbsz00_3099 = BgL_bsz00_263;
				BgL_zc3z04anonymousza32400ze3z87_3100:
					if (NULLP(BgL_pbsz00_3099))
						{	/* SawBbv/bbv-merge.scm 524 */
							BGL_TAIL return
								BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2nearnegativezd2zzsaw_bbvzd2mergezd2
								(BgL_bsz00_263);
						}
					else
						{	/* SawBbv/bbv-merge.scm 526 */
							obj_t BgL_sbz00_3102;

							{	/* SawBbv/bbv-merge.scm 526 */
								obj_t BgL_g1701z00_3106;

								BgL_g1701z00_3106 = CDR(((obj_t) BgL_pbsz00_3099));
								{
									obj_t BgL_list1700z00_3108;

									BgL_list1700z00_3108 = BgL_g1701z00_3106;
								BgL_zc3z04anonymousza32403ze3z87_3109:
									if (PAIRP(BgL_list1700z00_3108))
										{	/* SawBbv/bbv-merge.scm 528 */
											bool_t BgL_test3622z00_9251;

											{	/* SawBbv/bbv-merge.scm 527 */
												obj_t BgL_bz00_3115;

												BgL_bz00_3115 = CAR(BgL_list1700z00_3108);
												{	/* SawBbv/bbv-merge.scm 527 */
													obj_t BgL_arg2408z00_3116;

													BgL_arg2408z00_3116 = CAR(((obj_t) BgL_pbsz00_3099));
													{	/* SawBbv/bbv-merge.scm 527 */
														obj_t BgL_tmpz00_9255;

														BgL_xz00_3124 = BgL_arg2408z00_3116;
														BgL_yz00_3125 = BgL_bz00_3115;
														{	/* SawBbv/bbv-merge.scm 509 */
															BgL_bbvzd2ctxzd2_bglt BgL_i1240z00_3129;

															{
																BgL_blocksz00_bglt BgL_auxz00_9256;

																{
																	obj_t BgL_auxz00_9257;

																	{	/* SawBbv/bbv-merge.scm 510 */
																		BgL_objectz00_bglt BgL_tmpz00_9258;

																		BgL_tmpz00_9258 =
																			((BgL_objectz00_bglt)
																			((BgL_blockz00_bglt) BgL_xz00_3124));
																		BgL_auxz00_9257 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_9258);
																	}
																	BgL_auxz00_9256 =
																		((BgL_blocksz00_bglt) BgL_auxz00_9257);
																}
																BgL_i1240z00_3129 =
																	(((BgL_blocksz00_bglt)
																		COBJECT(BgL_auxz00_9256))->BgL_ctxz00);
															}
															{	/* SawBbv/bbv-merge.scm 510 */
																BgL_bbvzd2ctxzd2_bglt BgL_i1241z00_3130;

																{
																	BgL_blocksz00_bglt BgL_auxz00_9264;

																	{
																		obj_t BgL_auxz00_9265;

																		{	/* SawBbv/bbv-merge.scm 511 */
																			BgL_objectz00_bglt BgL_tmpz00_9266;

																			BgL_tmpz00_9266 =
																				((BgL_objectz00_bglt)
																				((BgL_blockz00_bglt) BgL_yz00_3125));
																			BgL_auxz00_9265 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_9266);
																		}
																		BgL_auxz00_9264 =
																			((BgL_blocksz00_bglt) BgL_auxz00_9265);
																	}
																	BgL_i1241z00_3130 =
																		(((BgL_blocksz00_bglt)
																			COBJECT(BgL_auxz00_9264))->BgL_ctxz00);
																}
																{	/* SawBbv/bbv-merge.scm 511 */
																	obj_t BgL_arg2415z00_3132;
																	obj_t BgL_arg2417z00_3133;

																	BgL_arg2415z00_3132 =
																		(((BgL_bbvzd2ctxzd2_bglt)
																			COBJECT(BgL_i1240z00_3129))->
																		BgL_entriesz00);
																	BgL_arg2417z00_3133 =
																		(((BgL_bbvzd2ctxzd2_bglt)
																			COBJECT(BgL_i1241z00_3130))->
																		BgL_entriesz00);
																	{	/* SawBbv/bbv-merge.scm 511 */
																		obj_t BgL_list2418z00_3134;

																		{	/* SawBbv/bbv-merge.scm 511 */
																			obj_t BgL_arg2419z00_3135;

																			BgL_arg2419z00_3135 =
																				MAKE_YOUNG_PAIR(BgL_arg2417z00_3133,
																				BNIL);
																			BgL_list2418z00_3134 =
																				MAKE_YOUNG_PAIR(BgL_arg2415z00_3132,
																				BgL_arg2419z00_3135);
																		}
																		BgL_tmpz00_9255 =
																			BGl_everyz00zz__r4_pairs_and_lists_6_3z00
																			(BGl_proc3295z00zzsaw_bbvzd2mergezd2,
																			BgL_list2418z00_3134);
																	}
																}
															}
														}
														BgL_test3622z00_9251 = CBOOL(BgL_tmpz00_9255);
													}
												}
											}
											if (BgL_test3622z00_9251)
												{	/* SawBbv/bbv-merge.scm 528 */
													BgL_sbz00_3102 = CAR(BgL_list1700z00_3108);
												}
											else
												{
													obj_t BgL_list1700z00_9279;

													BgL_list1700z00_9279 = CDR(BgL_list1700z00_3108);
													BgL_list1700z00_3108 = BgL_list1700z00_9279;
													goto BgL_zc3z04anonymousza32403ze3z87_3109;
												}
										}
									else
										{	/* SawBbv/bbv-merge.scm 528 */
											BgL_sbz00_3102 = BFALSE;
										}
								}
							}
							if (CBOOL(BgL_sbz00_3102))
								{	/* SawBbv/bbv-merge.scm 534 */
									obj_t BgL_val0_1702z00_3103;

									BgL_val0_1702z00_3103 = CAR(((obj_t) BgL_pbsz00_3099));
									{	/* SawBbv/bbv-merge.scm 534 */
										int BgL_tmpz00_9285;

										BgL_tmpz00_9285 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9285);
									}
									{	/* SawBbv/bbv-merge.scm 534 */
										int BgL_tmpz00_9288;

										BgL_tmpz00_9288 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_9288, BgL_sbz00_3102);
									}
									return BgL_val0_1702z00_3103;
								}
							else
								{	/* SawBbv/bbv-merge.scm 535 */
									obj_t BgL_arg2402z00_3105;

									BgL_arg2402z00_3105 = CDR(((obj_t) BgL_pbsz00_3099));
									{
										obj_t BgL_pbsz00_9293;

										BgL_pbsz00_9293 = BgL_arg2402z00_3105;
										BgL_pbsz00_3099 = BgL_pbsz00_9293;
										goto BgL_zc3z04anonymousza32400ze3z87_3100;
									}
								}
						}
				}
			}
		}

	}



/* &<@anonymous:2420> */
	obj_t BGl_z62zc3z04anonymousza32420ze3ze5zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6506, obj_t BgL_exz00_6507, obj_t BgL_eyz00_6508)
	{
		{	/* SawBbv/bbv-merge.scm 511 */
			if (
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
							((BgL_bbvzd2ctxentryzd2_bglt) BgL_exz00_6507)))->BgL_polarityz00))
				{	/* SawBbv/bbv-merge.scm 517 */
					if (
						(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_eyz00_6508)))->
							BgL_polarityz00))
						{	/* SawBbv/bbv-merge.scm 519 */
							obj_t BgL_arg2423z00_6706;
							obj_t BgL_arg2424z00_6707;

							BgL_arg2423z00_6706 =
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
										((BgL_bbvzd2ctxentryzd2_bglt) BgL_exz00_6507)))->
								BgL_typesz00);
							BgL_arg2424z00_6707 =
								(((BgL_bbvzd2ctxentryzd2_bglt)
									COBJECT(((BgL_bbvzd2ctxentryzd2_bglt) BgL_eyz00_6508)))->
								BgL_typesz00);
							{	/* SawBbv/bbv-merge.scm 504 */
								obj_t BgL_list2410z00_6708;

								{	/* SawBbv/bbv-merge.scm 504 */
									obj_t BgL_arg2411z00_6709;

									BgL_arg2411z00_6709 =
										MAKE_YOUNG_PAIR(BgL_arg2424z00_6707, BNIL);
									BgL_list2410z00_6708 =
										MAKE_YOUNG_PAIR(BgL_arg2423z00_6706, BgL_arg2411z00_6709);
								}
								return
									BGl_everyz00zz__r4_pairs_and_lists_6_3z00
									(BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00,
									BgL_list2410z00_6708);
							}
						}
					else
						{	/* SawBbv/bbv-merge.scm 518 */
							return BFALSE;
						}
				}
			else
				{	/* SawBbv/bbv-merge.scm 517 */
					if (
						(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_eyz00_6508)))->
							BgL_polarityz00))
						{	/* SawBbv/bbv-merge.scm 517 */
							return BFALSE;
						}
					else
						{	/* SawBbv/bbv-merge.scm 517 */
							return BTRUE;
						}
				}
		}

	}



/* bbv-block-merge-select-strategy-nearnegative */
	obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2nearnegativezd2zzsaw_bbvzd2mergezd2
		(obj_t BgL_bsz00_264)
	{
		{	/* SawBbv/bbv-merge.scm 544 */
			{
				BgL_blockz00_bglt BgL_bz00_3197;

				{	/* SawBbv/bbv-merge.scm 563 */
					obj_t BgL_bnbz00_3151;

					{	/* SawBbv/bbv-merge.scm 563 */
						obj_t BgL_hook1716z00_3183;

						BgL_hook1716z00_3183 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
						{
							obj_t BgL_l1713z00_3185;
							obj_t BgL_h1714z00_3186;

							BgL_l1713z00_3185 = BgL_bsz00_264;
							BgL_h1714z00_3186 = BgL_hook1716z00_3183;
						BgL_zc3z04anonymousza32443ze3z87_3187:
							if (NULLP(BgL_l1713z00_3185))
								{	/* SawBbv/bbv-merge.scm 563 */
									BgL_bnbz00_3151 = CDR(BgL_hook1716z00_3183);
								}
							else
								{	/* SawBbv/bbv-merge.scm 563 */
									bool_t BgL_test3628z00_9314;

									BgL_bz00_3197 =
										((BgL_blockz00_bglt) CAR(((obj_t) BgL_l1713z00_3185)));
									{	/* SawBbv/bbv-merge.scm 548 */
										BgL_bbvzd2ctxzd2_bglt BgL_i1245z00_3200;

										{
											BgL_blocksz00_bglt BgL_auxz00_9315;

											{
												obj_t BgL_auxz00_9316;

												{	/* SawBbv/bbv-merge.scm 549 */
													BgL_objectz00_bglt BgL_tmpz00_9317;

													BgL_tmpz00_9317 =
														((BgL_objectz00_bglt) BgL_bz00_3197);
													BgL_auxz00_9316 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_9317);
												}
												BgL_auxz00_9315 =
													((BgL_blocksz00_bglt) BgL_auxz00_9316);
											}
											BgL_i1245z00_3200 =
												(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_9315))->
												BgL_ctxz00);
										}
										{
											obj_t BgL_l1704z00_3203;

											BgL_l1704z00_3203 =
												(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_i1245z00_3200))->
												BgL_entriesz00);
										BgL_zc3z04anonymousza32453ze3z87_3204:
											if (NULLP(BgL_l1704z00_3203))
												{	/* SawBbv/bbv-merge.scm 549 */
													BgL_test3628z00_9314 = ((bool_t) 1);
												}
											else
												{	/* SawBbv/bbv-merge.scm 549 */
													bool_t BgL_test3630z00_9324;

													{	/* SawBbv/bbv-merge.scm 550 */
														BgL_bbvzd2ctxentryzd2_bglt BgL_ez00_3209;

														BgL_ez00_3209 =
															((BgL_bbvzd2ctxentryzd2_bglt)
															CAR(((obj_t) BgL_l1704z00_3203)));
														{	/* SawBbv/bbv-merge.scm 551 */
															bool_t BgL__ortest_1247z00_3211;

															{	/* SawBbv/bbv-merge.scm 551 */
																obj_t BgL_arg2457z00_3213;

																BgL_arg2457z00_3213 =
																	CAR(
																	(((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_ez00_3209))->BgL_typesz00));
																BgL__ortest_1247z00_3211 =
																	(BgL_arg2457z00_3213 ==
																	BGl_za2objza2z00zztype_cachez00);
															}
															if (BgL__ortest_1247z00_3211)
																{	/* SawBbv/bbv-merge.scm 551 */
																	BgL_test3630z00_9324 =
																		BgL__ortest_1247z00_3211;
																}
															else
																{	/* SawBbv/bbv-merge.scm 551 */
																	if (
																		(((BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(BgL_ez00_3209))->
																			BgL_polarityz00))
																		{	/* SawBbv/bbv-merge.scm 551 */
																			BgL_test3630z00_9324 = ((bool_t) 0);
																		}
																	else
																		{	/* SawBbv/bbv-merge.scm 551 */
																			BgL_test3630z00_9324 = ((bool_t) 1);
																		}
																}
														}
													}
													if (BgL_test3630z00_9324)
														{
															obj_t BgL_l1704z00_9334;

															BgL_l1704z00_9334 =
																CDR(((obj_t) BgL_l1704z00_3203));
															BgL_l1704z00_3203 = BgL_l1704z00_9334;
															goto BgL_zc3z04anonymousza32453ze3z87_3204;
														}
													else
														{	/* SawBbv/bbv-merge.scm 549 */
															BgL_test3628z00_9314 = ((bool_t) 0);
														}
												}
										}
									}
									if (BgL_test3628z00_9314)
										{	/* SawBbv/bbv-merge.scm 563 */
											obj_t BgL_nh1715z00_3191;

											{	/* SawBbv/bbv-merge.scm 563 */
												obj_t BgL_arg2449z00_3193;

												BgL_arg2449z00_3193 = CAR(((obj_t) BgL_l1713z00_3185));
												BgL_nh1715z00_3191 =
													MAKE_YOUNG_PAIR(BgL_arg2449z00_3193, BNIL);
											}
											SET_CDR(BgL_h1714z00_3186, BgL_nh1715z00_3191);
											{	/* SawBbv/bbv-merge.scm 563 */
												obj_t BgL_arg2447z00_3192;

												BgL_arg2447z00_3192 = CDR(((obj_t) BgL_l1713z00_3185));
												{
													obj_t BgL_h1714z00_9348;
													obj_t BgL_l1713z00_9347;

													BgL_l1713z00_9347 = BgL_arg2447z00_3192;
													BgL_h1714z00_9348 = BgL_nh1715z00_3191;
													BgL_h1714z00_3186 = BgL_h1714z00_9348;
													BgL_l1713z00_3185 = BgL_l1713z00_9347;
													goto BgL_zc3z04anonymousza32443ze3z87_3187;
												}
											}
										}
									else
										{	/* SawBbv/bbv-merge.scm 563 */
											obj_t BgL_arg2450z00_3194;

											BgL_arg2450z00_3194 = CDR(((obj_t) BgL_l1713z00_3185));
											{
												obj_t BgL_l1713z00_9351;

												BgL_l1713z00_9351 = BgL_arg2450z00_3194;
												BgL_l1713z00_3185 = BgL_l1713z00_9351;
												goto BgL_zc3z04anonymousza32443ze3z87_3187;
											}
										}
								}
						}
					}
					{	/* SawBbv/bbv-merge.scm 564 */
						bool_t BgL_test3633z00_9352;

						if (PAIRP(BgL_bnbz00_3151))
							{	/* SawBbv/bbv-merge.scm 564 */
								obj_t BgL_tmpz00_9355;

								BgL_tmpz00_9355 = CDR(BgL_bnbz00_3151);
								BgL_test3633z00_9352 = PAIRP(BgL_tmpz00_9355);
							}
						else
							{	/* SawBbv/bbv-merge.scm 564 */
								BgL_test3633z00_9352 = ((bool_t) 0);
							}
						if (BgL_test3633z00_9352)
							{	/* SawBbv/bbv-merge.scm 565 */
								obj_t BgL_lstz00_3155;

								{	/* SawBbv/bbv-merge.scm 565 */
									obj_t BgL_arg2431z00_3159;

									{	/* SawBbv/bbv-merge.scm 567 */
										obj_t BgL_head1721z00_3168;

										BgL_head1721z00_3168 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1719z00_3170;
											obj_t BgL_tail1722z00_3171;

											BgL_l1719z00_3170 = BgL_bnbz00_3151;
											BgL_tail1722z00_3171 = BgL_head1721z00_3168;
										BgL_zc3z04anonymousza32435ze3z87_3172:
											if (NULLP(BgL_l1719z00_3170))
												{	/* SawBbv/bbv-merge.scm 567 */
													BgL_arg2431z00_3159 = CDR(BgL_head1721z00_3168);
												}
											else
												{	/* SawBbv/bbv-merge.scm 567 */
													obj_t BgL_newtail1723z00_3174;

													{	/* SawBbv/bbv-merge.scm 567 */
														obj_t BgL_arg2438z00_3176;

														{	/* SawBbv/bbv-merge.scm 567 */
															obj_t BgL_bz00_3177;

															BgL_bz00_3177 = CAR(((obj_t) BgL_l1719z00_3170));
															{	/* SawBbv/bbv-merge.scm 567 */
																obj_t BgL_arg2439z00_3178;

																BgL_arg2439z00_3178 =
																	BGl_blockzd2siza7ez75zzsaw_bbvzd2mergezd2(
																	((BgL_blockz00_bglt) BgL_bz00_3177));
																BgL_arg2438z00_3176 =
																	MAKE_YOUNG_PAIR(BgL_arg2439z00_3178,
																	BgL_bz00_3177);
															}
														}
														BgL_newtail1723z00_3174 =
															MAKE_YOUNG_PAIR(BgL_arg2438z00_3176, BNIL);
													}
													SET_CDR(BgL_tail1722z00_3171,
														BgL_newtail1723z00_3174);
													{	/* SawBbv/bbv-merge.scm 567 */
														obj_t BgL_arg2437z00_3175;

														BgL_arg2437z00_3175 =
															CDR(((obj_t) BgL_l1719z00_3170));
														{
															obj_t BgL_tail1722z00_9372;
															obj_t BgL_l1719z00_9371;

															BgL_l1719z00_9371 = BgL_arg2437z00_3175;
															BgL_tail1722z00_9372 = BgL_newtail1723z00_3174;
															BgL_tail1722z00_3171 = BgL_tail1722z00_9372;
															BgL_l1719z00_3170 = BgL_l1719z00_9371;
															goto BgL_zc3z04anonymousza32435ze3z87_3172;
														}
													}
												}
										}
									}
									BgL_lstz00_3155 =
										BGl_sortz00zz__r4_vectors_6_8z00
										(BGl_proc3296z00zzsaw_bbvzd2mergezd2, BgL_arg2431z00_3159);
								}
								{	/* SawBbv/bbv-merge.scm 571 */
									obj_t BgL_val0_1724z00_3156;
									obj_t BgL_val1_1725z00_3157;

									{	/* SawBbv/bbv-merge.scm 571 */
										obj_t BgL_pairz00_5277;

										BgL_pairz00_5277 = CAR(((obj_t) BgL_lstz00_3155));
										BgL_val0_1724z00_3156 = CDR(BgL_pairz00_5277);
									}
									{	/* SawBbv/bbv-merge.scm 571 */
										obj_t BgL_pairz00_5283;

										{	/* SawBbv/bbv-merge.scm 571 */
											obj_t BgL_pairz00_5282;

											BgL_pairz00_5282 = CDR(((obj_t) BgL_lstz00_3155));
											BgL_pairz00_5283 = CAR(BgL_pairz00_5282);
										}
										BgL_val1_1725z00_3157 = CDR(BgL_pairz00_5283);
									}
									{	/* SawBbv/bbv-merge.scm 571 */
										int BgL_tmpz00_9381;

										BgL_tmpz00_9381 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9381);
									}
									{	/* SawBbv/bbv-merge.scm 571 */
										int BgL_tmpz00_9384;

										BgL_tmpz00_9384 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_9384, BgL_val1_1725z00_3157);
									}
									return BgL_val0_1724z00_3156;
								}
							}
						else
							{	/* SawBbv/bbv-merge.scm 564 */
								BGL_TAIL return
									BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2anynegativezd2zzsaw_bbvzd2mergezd2
									(BgL_bsz00_264);
							}
					}
				}
			}
		}

	}



/* &<@anonymous:2432> */
	obj_t BGl_z62zc3z04anonymousza32432ze3ze5zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6510, obj_t BgL_xz00_6511, obj_t BgL_yz00_6512)
	{
		{	/* SawBbv/bbv-merge.scm 565 */
			{	/* SawBbv/bbv-merge.scm 566 */
				bool_t BgL_tmpz00_9388;

				{	/* SawBbv/bbv-merge.scm 566 */
					obj_t BgL_a1717z00_6710;

					BgL_a1717z00_6710 = CAR(((obj_t) BgL_xz00_6511));
					{	/* SawBbv/bbv-merge.scm 566 */
						obj_t BgL_b1718z00_6711;

						BgL_b1718z00_6711 = CAR(((obj_t) BgL_yz00_6512));
						{	/* SawBbv/bbv-merge.scm 566 */

							{	/* SawBbv/bbv-merge.scm 566 */
								bool_t BgL_test3636z00_9393;

								if (INTEGERP(BgL_a1717z00_6710))
									{	/* SawBbv/bbv-merge.scm 566 */
										BgL_test3636z00_9393 = INTEGERP(BgL_b1718z00_6711);
									}
								else
									{	/* SawBbv/bbv-merge.scm 566 */
										BgL_test3636z00_9393 = ((bool_t) 0);
									}
								if (BgL_test3636z00_9393)
									{	/* SawBbv/bbv-merge.scm 566 */
										BgL_tmpz00_9388 =
											(
											(long) CINT(BgL_a1717z00_6710) >=
											(long) CINT(BgL_b1718z00_6711));
									}
								else
									{	/* SawBbv/bbv-merge.scm 566 */
										BgL_tmpz00_9388 =
											BGl_2ze3zd3z30zz__r4_numbers_6_5z00(BgL_a1717z00_6710,
											BgL_b1718z00_6711);
									}
							}
						}
					}
				}
				return BBOOL(BgL_tmpz00_9388);
			}
		}

	}



/* bbv-block-merge-select-strategy-anynegative */
	obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2anynegativezd2zzsaw_bbvzd2mergezd2
		(obj_t BgL_bsz00_265)
	{
		{	/* SawBbv/bbv-merge.scm 579 */
			{
				BgL_blockz00_bglt BgL_bz00_3284;

				{	/* SawBbv/bbv-merge.scm 590 */
					obj_t BgL_bnbz00_3238;

					{	/* SawBbv/bbv-merge.scm 590 */
						obj_t BgL_hook1734z00_3270;

						BgL_hook1734z00_3270 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
						{
							obj_t BgL_l1731z00_3272;
							obj_t BgL_h1732z00_3273;

							BgL_l1731z00_3272 = BgL_bsz00_265;
							BgL_h1732z00_3273 = BgL_hook1734z00_3270;
						BgL_zc3z04anonymousza32483ze3z87_3274:
							if (NULLP(BgL_l1731z00_3272))
								{	/* SawBbv/bbv-merge.scm 590 */
									BgL_bnbz00_3238 = CDR(BgL_hook1734z00_3270);
								}
							else
								{	/* SawBbv/bbv-merge.scm 590 */
									bool_t BgL_test3639z00_9406;

									BgL_bz00_3284 =
										((BgL_blockz00_bglt) CAR(((obj_t) BgL_l1731z00_3272)));
									{	/* SawBbv/bbv-merge.scm 583 */
										BgL_bbvzd2ctxzd2_bglt BgL_i1253z00_3287;

										{
											BgL_blocksz00_bglt BgL_auxz00_9407;

											{
												obj_t BgL_auxz00_9408;

												{	/* SawBbv/bbv-merge.scm 584 */
													BgL_objectz00_bglt BgL_tmpz00_9409;

													BgL_tmpz00_9409 =
														((BgL_objectz00_bglt) BgL_bz00_3284);
													BgL_auxz00_9408 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_9409);
												}
												BgL_auxz00_9407 =
													((BgL_blocksz00_bglt) BgL_auxz00_9408);
											}
											BgL_i1253z00_3287 =
												(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_9407))->
												BgL_ctxz00);
										}
										{
											obj_t BgL_l1726z00_3290;

											BgL_l1726z00_3290 =
												(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_i1253z00_3287))->
												BgL_entriesz00);
										BgL_zc3z04anonymousza32494ze3z87_3291:
											if (NULLP(BgL_l1726z00_3290))
												{	/* SawBbv/bbv-merge.scm 584 */
													BgL_test3639z00_9406 = ((bool_t) 0);
												}
											else
												{	/* SawBbv/bbv-merge.scm 585 */
													bool_t BgL__ortest_1729z00_3293;

													if (
														(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
																	((BgL_bbvzd2ctxentryzd2_bglt)
																		CAR(
																			((obj_t) BgL_l1726z00_3290)))))->
															BgL_polarityz00))
														{	/* SawBbv/bbv-merge.scm 586 */
															BgL__ortest_1729z00_3293 = ((bool_t) 0);
														}
													else
														{	/* SawBbv/bbv-merge.scm 586 */
															BgL__ortest_1729z00_3293 = ((bool_t) 1);
														}
													if (BgL__ortest_1729z00_3293)
														{	/* SawBbv/bbv-merge.scm 584 */
															BgL_test3639z00_9406 = BgL__ortest_1729z00_3293;
														}
													else
														{
															obj_t BgL_l1726z00_9422;

															BgL_l1726z00_9422 =
																CDR(((obj_t) BgL_l1726z00_3290));
															BgL_l1726z00_3290 = BgL_l1726z00_9422;
															goto BgL_zc3z04anonymousza32494ze3z87_3291;
														}
												}
										}
									}
									if (BgL_test3639z00_9406)
										{	/* SawBbv/bbv-merge.scm 590 */
											obj_t BgL_nh1733z00_3278;

											{	/* SawBbv/bbv-merge.scm 590 */
												obj_t BgL_arg2490z00_3280;

												BgL_arg2490z00_3280 = CAR(((obj_t) BgL_l1731z00_3272));
												BgL_nh1733z00_3278 =
													MAKE_YOUNG_PAIR(BgL_arg2490z00_3280, BNIL);
											}
											SET_CDR(BgL_h1732z00_3273, BgL_nh1733z00_3278);
											{	/* SawBbv/bbv-merge.scm 590 */
												obj_t BgL_arg2488z00_3279;

												BgL_arg2488z00_3279 = CDR(((obj_t) BgL_l1731z00_3272));
												{
													obj_t BgL_h1732z00_9436;
													obj_t BgL_l1731z00_9435;

													BgL_l1731z00_9435 = BgL_arg2488z00_3279;
													BgL_h1732z00_9436 = BgL_nh1733z00_3278;
													BgL_h1732z00_3273 = BgL_h1732z00_9436;
													BgL_l1731z00_3272 = BgL_l1731z00_9435;
													goto BgL_zc3z04anonymousza32483ze3z87_3274;
												}
											}
										}
									else
										{	/* SawBbv/bbv-merge.scm 590 */
											obj_t BgL_arg2491z00_3281;

											BgL_arg2491z00_3281 = CDR(((obj_t) BgL_l1731z00_3272));
											{
												obj_t BgL_l1731z00_9439;

												BgL_l1731z00_9439 = BgL_arg2491z00_3281;
												BgL_l1731z00_3272 = BgL_l1731z00_9439;
												goto BgL_zc3z04anonymousza32483ze3z87_3274;
											}
										}
								}
						}
					}
					{	/* SawBbv/bbv-merge.scm 591 */
						bool_t BgL_test3643z00_9440;

						if (PAIRP(BgL_bnbz00_3238))
							{	/* SawBbv/bbv-merge.scm 591 */
								obj_t BgL_tmpz00_9443;

								BgL_tmpz00_9443 = CDR(BgL_bnbz00_3238);
								BgL_test3643z00_9440 = PAIRP(BgL_tmpz00_9443);
							}
						else
							{	/* SawBbv/bbv-merge.scm 591 */
								BgL_test3643z00_9440 = ((bool_t) 0);
							}
						if (BgL_test3643z00_9440)
							{	/* SawBbv/bbv-merge.scm 592 */
								obj_t BgL_lstz00_3242;

								{	/* SawBbv/bbv-merge.scm 592 */
									obj_t BgL_arg2473z00_3246;

									{	/* SawBbv/bbv-merge.scm 594 */
										obj_t BgL_head1739z00_3255;

										BgL_head1739z00_3255 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1737z00_3257;
											obj_t BgL_tail1740z00_3258;

											BgL_l1737z00_3257 = BgL_bnbz00_3238;
											BgL_tail1740z00_3258 = BgL_head1739z00_3255;
										BgL_zc3z04anonymousza32477ze3z87_3259:
											if (NULLP(BgL_l1737z00_3257))
												{	/* SawBbv/bbv-merge.scm 594 */
													BgL_arg2473z00_3246 = CDR(BgL_head1739z00_3255);
												}
											else
												{	/* SawBbv/bbv-merge.scm 594 */
													obj_t BgL_newtail1741z00_3261;

													{	/* SawBbv/bbv-merge.scm 594 */
														obj_t BgL_arg2480z00_3263;

														{	/* SawBbv/bbv-merge.scm 594 */
															obj_t BgL_bz00_3264;

															BgL_bz00_3264 = CAR(((obj_t) BgL_l1737z00_3257));
															{	/* SawBbv/bbv-merge.scm 594 */
																obj_t BgL_arg2481z00_3265;

																BgL_arg2481z00_3265 =
																	BGl_blockzd2siza7ez75zzsaw_bbvzd2mergezd2(
																	((BgL_blockz00_bglt) BgL_bz00_3264));
																BgL_arg2480z00_3263 =
																	MAKE_YOUNG_PAIR(BgL_arg2481z00_3265,
																	BgL_bz00_3264);
															}
														}
														BgL_newtail1741z00_3261 =
															MAKE_YOUNG_PAIR(BgL_arg2480z00_3263, BNIL);
													}
													SET_CDR(BgL_tail1740z00_3258,
														BgL_newtail1741z00_3261);
													{	/* SawBbv/bbv-merge.scm 594 */
														obj_t BgL_arg2479z00_3262;

														BgL_arg2479z00_3262 =
															CDR(((obj_t) BgL_l1737z00_3257));
														{
															obj_t BgL_tail1740z00_9460;
															obj_t BgL_l1737z00_9459;

															BgL_l1737z00_9459 = BgL_arg2479z00_3262;
															BgL_tail1740z00_9460 = BgL_newtail1741z00_3261;
															BgL_tail1740z00_3258 = BgL_tail1740z00_9460;
															BgL_l1737z00_3257 = BgL_l1737z00_9459;
															goto BgL_zc3z04anonymousza32477ze3z87_3259;
														}
													}
												}
										}
									}
									BgL_lstz00_3242 =
										BGl_sortz00zz__r4_vectors_6_8z00
										(BGl_proc3297z00zzsaw_bbvzd2mergezd2, BgL_arg2473z00_3246);
								}
								{	/* SawBbv/bbv-merge.scm 598 */
									obj_t BgL_val0_1742z00_3243;
									obj_t BgL_val1_1743z00_3244;

									{	/* SawBbv/bbv-merge.scm 598 */
										obj_t BgL_pairz00_5305;

										BgL_pairz00_5305 = CAR(((obj_t) BgL_lstz00_3242));
										BgL_val0_1742z00_3243 = CDR(BgL_pairz00_5305);
									}
									{	/* SawBbv/bbv-merge.scm 598 */
										obj_t BgL_pairz00_5311;

										{	/* SawBbv/bbv-merge.scm 598 */
											obj_t BgL_pairz00_5310;

											BgL_pairz00_5310 = CDR(((obj_t) BgL_lstz00_3242));
											BgL_pairz00_5311 = CAR(BgL_pairz00_5310);
										}
										BgL_val1_1743z00_3244 = CDR(BgL_pairz00_5311);
									}
									{	/* SawBbv/bbv-merge.scm 598 */
										int BgL_tmpz00_9469;

										BgL_tmpz00_9469 = (int) (2L);
										BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9469);
									}
									{	/* SawBbv/bbv-merge.scm 598 */
										int BgL_tmpz00_9472;

										BgL_tmpz00_9472 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_9472, BgL_val1_1743z00_3244);
									}
									return BgL_val0_1742z00_3243;
								}
							}
						else
							{	/* SawBbv/bbv-merge.scm 591 */
								BGL_TAIL return
									BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2siza7ez75zzsaw_bbvzd2mergezd2
									(BgL_bsz00_265);
							}
					}
				}
			}
		}

	}



/* &<@anonymous:2474> */
	obj_t BGl_z62zc3z04anonymousza32474ze3ze5zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6514, obj_t BgL_xz00_6515, obj_t BgL_yz00_6516)
	{
		{	/* SawBbv/bbv-merge.scm 592 */
			{	/* SawBbv/bbv-merge.scm 593 */
				bool_t BgL_tmpz00_9476;

				{	/* SawBbv/bbv-merge.scm 593 */
					obj_t BgL_a1735z00_6712;

					BgL_a1735z00_6712 = CAR(((obj_t) BgL_xz00_6515));
					{	/* SawBbv/bbv-merge.scm 593 */
						obj_t BgL_b1736z00_6713;

						BgL_b1736z00_6713 = CAR(((obj_t) BgL_yz00_6516));
						{	/* SawBbv/bbv-merge.scm 593 */

							{	/* SawBbv/bbv-merge.scm 593 */
								bool_t BgL_test3646z00_9481;

								if (INTEGERP(BgL_a1735z00_6712))
									{	/* SawBbv/bbv-merge.scm 593 */
										BgL_test3646z00_9481 = INTEGERP(BgL_b1736z00_6713);
									}
								else
									{	/* SawBbv/bbv-merge.scm 593 */
										BgL_test3646z00_9481 = ((bool_t) 0);
									}
								if (BgL_test3646z00_9481)
									{	/* SawBbv/bbv-merge.scm 593 */
										BgL_tmpz00_9476 =
											(
											(long) CINT(BgL_a1735z00_6712) >=
											(long) CINT(BgL_b1736z00_6713));
									}
								else
									{	/* SawBbv/bbv-merge.scm 593 */
										BgL_tmpz00_9476 =
											BGl_2ze3zd3z30zz__r4_numbers_6_5z00(BgL_a1735z00_6712,
											BgL_b1736z00_6713);
									}
							}
						}
					}
				}
				return BBOOL(BgL_tmpz00_9476);
			}
		}

	}



/* bbv-block-merge-select-strategy-size */
	obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2siza7ez75zzsaw_bbvzd2mergezd2
		(obj_t BgL_bsz00_266)
	{
		{	/* SawBbv/bbv-merge.scm 606 */
			{
				BgL_blockz00_bglt BgL_bz00_3363;

				{	/* SawBbv/bbv-merge.scm 619 */
					long BgL_maxsz00_3308;

					BgL_bz00_3363 = ((BgL_blockz00_bglt) CAR(BgL_bsz00_266));
					{	/* SawBbv/bbv-merge.scm 610 */
						BgL_bbvzd2ctxzd2_bglt BgL_i1256z00_3366;

						{
							BgL_blocksz00_bglt BgL_auxz00_9490;

							{
								obj_t BgL_auxz00_9491;

								{	/* SawBbv/bbv-merge.scm 611 */
									BgL_objectz00_bglt BgL_tmpz00_9492;

									BgL_tmpz00_9492 = ((BgL_objectz00_bglt) BgL_bz00_3363);
									BgL_auxz00_9491 = BGL_OBJECT_WIDENING(BgL_tmpz00_9492);
								}
								BgL_auxz00_9490 = ((BgL_blocksz00_bglt) BgL_auxz00_9491);
							}
							BgL_i1256z00_3366 =
								(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_9490))->BgL_ctxz00);
						}
						BgL_maxsz00_3308 =
							(100L *
							bgl_list_length(
								(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_i1256z00_3366))->
									BgL_entriesz00)));
					}
					{	/* SawBbv/bbv-merge.scm 619 */
						obj_t BgL_bsza7za7_3309;

						{	/* SawBbv/bbv-merge.scm 620 */
							obj_t BgL_head1748z00_3350;

							BgL_head1748z00_3350 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1746z00_3352;
								obj_t BgL_tail1749z00_3353;

								BgL_l1746z00_3352 = BgL_bsz00_266;
								BgL_tail1749z00_3353 = BgL_head1748z00_3350;
							BgL_zc3z04anonymousza32528ze3z87_3354:
								if (NULLP(BgL_l1746z00_3352))
									{	/* SawBbv/bbv-merge.scm 620 */
										BgL_bsza7za7_3309 = CDR(BgL_head1748z00_3350);
									}
								else
									{	/* SawBbv/bbv-merge.scm 620 */
										obj_t BgL_newtail1750z00_3356;

										{	/* SawBbv/bbv-merge.scm 620 */
											obj_t BgL_arg2536z00_3358;

											{	/* SawBbv/bbv-merge.scm 620 */
												obj_t BgL_bz00_3359;

												BgL_bz00_3359 = CAR(((obj_t) BgL_l1746z00_3352));
												{	/* SawBbv/bbv-merge.scm 620 */
													obj_t BgL_arg2537z00_3360;

													BgL_arg2537z00_3360 =
														BGl_blockzd2siza7ez75zzsaw_bbvzd2mergezd2(
														((BgL_blockz00_bglt) BgL_bz00_3359));
													BgL_arg2536z00_3358 =
														MAKE_YOUNG_PAIR(BgL_arg2537z00_3360, BgL_bz00_3359);
												}
											}
											BgL_newtail1750z00_3356 =
												MAKE_YOUNG_PAIR(BgL_arg2536z00_3358, BNIL);
										}
										SET_CDR(BgL_tail1749z00_3353, BgL_newtail1750z00_3356);
										{	/* SawBbv/bbv-merge.scm 620 */
											obj_t BgL_arg2534z00_3357;

											BgL_arg2534z00_3357 = CDR(((obj_t) BgL_l1746z00_3352));
											{
												obj_t BgL_tail1749z00_9516;
												obj_t BgL_l1746z00_9515;

												BgL_l1746z00_9515 = BgL_arg2534z00_3357;
												BgL_tail1749z00_9516 = BgL_newtail1750z00_3356;
												BgL_tail1749z00_3353 = BgL_tail1749z00_9516;
												BgL_l1746z00_3352 = BgL_l1746z00_9515;
												goto BgL_zc3z04anonymousza32528ze3z87_3354;
											}
										}
									}
							}
						}
						{	/* SawBbv/bbv-merge.scm 620 */
							obj_t BgL_mbsza7za7_3310;

							{	/* SawBbv/bbv-merge.scm 621 */
								obj_t BgL_hook1755z00_3332;

								BgL_hook1755z00_3332 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
								{
									obj_t BgL_l1752z00_3334;
									obj_t BgL_h1753z00_3335;

									BgL_l1752z00_3334 = BgL_bsza7za7_3309;
									BgL_h1753z00_3335 = BgL_hook1755z00_3332;
								BgL_zc3z04anonymousza32515ze3z87_3336:
									if (NULLP(BgL_l1752z00_3334))
										{	/* SawBbv/bbv-merge.scm 621 */
											BgL_mbsza7za7_3310 = CDR(BgL_hook1755z00_3332);
										}
									else
										{	/* SawBbv/bbv-merge.scm 621 */
											bool_t BgL_test3650z00_9521;

											{	/* SawBbv/bbv-merge.scm 621 */
												long BgL_tmpz00_9522;

												{	/* SawBbv/bbv-merge.scm 621 */
													obj_t BgL_pairz00_5323;

													BgL_pairz00_5323 = CAR(((obj_t) BgL_l1752z00_3334));
													BgL_tmpz00_9522 = (long) CINT(CAR(BgL_pairz00_5323));
												}
												BgL_test3650z00_9521 =
													(BgL_tmpz00_9522 == BgL_maxsz00_3308);
											}
											if (BgL_test3650z00_9521)
												{	/* SawBbv/bbv-merge.scm 621 */
													obj_t BgL_nh1754z00_3341;

													{	/* SawBbv/bbv-merge.scm 621 */
														obj_t BgL_arg2524z00_3343;

														BgL_arg2524z00_3343 =
															CAR(((obj_t) BgL_l1752z00_3334));
														BgL_nh1754z00_3341 =
															MAKE_YOUNG_PAIR(BgL_arg2524z00_3343, BNIL);
													}
													SET_CDR(BgL_h1753z00_3335, BgL_nh1754z00_3341);
													{	/* SawBbv/bbv-merge.scm 621 */
														obj_t BgL_arg2521z00_3342;

														BgL_arg2521z00_3342 =
															CDR(((obj_t) BgL_l1752z00_3334));
														{
															obj_t BgL_h1753z00_9535;
															obj_t BgL_l1752z00_9534;

															BgL_l1752z00_9534 = BgL_arg2521z00_3342;
															BgL_h1753z00_9535 = BgL_nh1754z00_3341;
															BgL_h1753z00_3335 = BgL_h1753z00_9535;
															BgL_l1752z00_3334 = BgL_l1752z00_9534;
															goto BgL_zc3z04anonymousza32515ze3z87_3336;
														}
													}
												}
											else
												{	/* SawBbv/bbv-merge.scm 621 */
													obj_t BgL_arg2525z00_3344;

													BgL_arg2525z00_3344 =
														CDR(((obj_t) BgL_l1752z00_3334));
													{
														obj_t BgL_l1752z00_9538;

														BgL_l1752z00_9538 = BgL_arg2525z00_3344;
														BgL_l1752z00_3334 = BgL_l1752z00_9538;
														goto BgL_zc3z04anonymousza32515ze3z87_3336;
													}
												}
										}
								}
							}
							{	/* SawBbv/bbv-merge.scm 621 */

								{	/* SawBbv/bbv-merge.scm 622 */
									bool_t BgL_test3651z00_9539;

									if (PAIRP(BgL_mbsza7za7_3310))
										{	/* SawBbv/bbv-merge.scm 622 */
											obj_t BgL_tmpz00_9542;

											BgL_tmpz00_9542 = CDR(BgL_mbsza7za7_3310);
											BgL_test3651z00_9539 = PAIRP(BgL_tmpz00_9542);
										}
									else
										{	/* SawBbv/bbv-merge.scm 622 */
											BgL_test3651z00_9539 = ((bool_t) 0);
										}
									if (BgL_test3651z00_9539)
										{	/* SawBbv/bbv-merge.scm 628 */
											obj_t BgL_val0_1756z00_3314;
											obj_t BgL_val1_1757z00_3315;

											BgL_val0_1756z00_3314 = CDR(CAR(BgL_mbsza7za7_3310));
											BgL_val1_1757z00_3315 = CDR(CAR(CDR(BgL_mbsza7za7_3310)));
											{	/* SawBbv/bbv-merge.scm 628 */
												int BgL_tmpz00_9550;

												BgL_tmpz00_9550 = (int) (2L);
												BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9550);
											}
											{	/* SawBbv/bbv-merge.scm 628 */
												int BgL_tmpz00_9553;

												BgL_tmpz00_9553 = (int) (1L);
												BGL_MVALUES_VAL_SET(BgL_tmpz00_9553,
													BgL_val1_1757z00_3315);
											}
											return BgL_val0_1756z00_3314;
										}
									else
										{	/* SawBbv/bbv-merge.scm 630 */
											obj_t BgL_lz00_3318;

											BgL_lz00_3318 =
												BGl_sortz00zz__r4_vectors_6_8z00(BgL_bsza7za7_3309,
												BGl_proc3298z00zzsaw_bbvzd2mergezd2);
											{	/* SawBbv/bbv-merge.scm 634 */
												obj_t BgL_val0_1758z00_3319;
												obj_t BgL_val1_1759z00_3320;

												{	/* SawBbv/bbv-merge.scm 634 */
													obj_t BgL_pairz00_5345;

													BgL_pairz00_5345 = CAR(((obj_t) BgL_lz00_3318));
													BgL_val0_1758z00_3319 = CDR(BgL_pairz00_5345);
												}
												{	/* SawBbv/bbv-merge.scm 634 */
													obj_t BgL_pairz00_5350;

													{	/* SawBbv/bbv-merge.scm 634 */
														obj_t BgL_pairz00_5349;

														BgL_pairz00_5349 = CDR(((obj_t) BgL_lz00_3318));
														BgL_pairz00_5350 = CAR(BgL_pairz00_5349);
													}
													BgL_val1_1759z00_3320 = CDR(BgL_pairz00_5350);
												}
												{	/* SawBbv/bbv-merge.scm 634 */
													int BgL_tmpz00_9564;

													BgL_tmpz00_9564 = (int) (2L);
													BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9564);
												}
												{	/* SawBbv/bbv-merge.scm 634 */
													int BgL_tmpz00_9567;

													BgL_tmpz00_9567 = (int) (1L);
													BGL_MVALUES_VAL_SET(BgL_tmpz00_9567,
														BgL_val1_1759z00_3320);
												}
												return BgL_val0_1758z00_3319;
											}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:2511> */
	obj_t BGl_z62zc3z04anonymousza32511ze3ze5zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6518, obj_t BgL_xz00_6519, obj_t BgL_yz00_6520)
	{
		{	/* SawBbv/bbv-merge.scm 630 */
			return
				BBOOL(
				((long) CINT(CAR(
							((obj_t) BgL_xz00_6519))) <=
					(long) CINT(CAR(((obj_t) BgL_yz00_6520)))));
		}

	}



/* bbv-block-merge-select-strategy-distance */
	obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2distancezd2zzsaw_bbvzd2mergezd2
		(obj_t BgL_bsz00_267)
	{
		{	/* SawBbv/bbv-merge.scm 641 */
			{	/* SawBbv/bbv-merge.scm 693 */
				obj_t BgL_lz00_3373;

				{	/* SawBbv/bbv-merge.scm 693 */
					obj_t BgL_arg2546z00_3378;

					{	/* SawBbv/bbv-merge.scm 693 */
						obj_t BgL_l1780z00_3380;

						BgL_l1780z00_3380 =
							BGl_zc3z04anonymousza32557ze3ze70z60zzsaw_bbvzd2mergezd2
							(BgL_bsz00_267, BgL_bsz00_267);
						if (NULLP(BgL_l1780z00_3380))
							{	/* SawBbv/bbv-merge.scm 693 */
								BgL_arg2546z00_3378 = BNIL;
							}
						else
							{	/* SawBbv/bbv-merge.scm 693 */
								obj_t BgL_head1782z00_3382;

								{	/* SawBbv/bbv-merge.scm 693 */
									obj_t BgL_arg2555z00_3394;

									{	/* SawBbv/bbv-merge.scm 693 */
										obj_t BgL_arg2556z00_3395;

										BgL_arg2556z00_3395 = CAR(((obj_t) BgL_l1780z00_3380));
										BgL_arg2555z00_3394 =
											BGl_blockzd2distze70z35zzsaw_bbvzd2mergezd2
											(BgL_arg2556z00_3395);
									}
									BgL_head1782z00_3382 =
										MAKE_YOUNG_PAIR(BgL_arg2555z00_3394, BNIL);
								}
								{	/* SawBbv/bbv-merge.scm 693 */
									obj_t BgL_g1785z00_3383;

									BgL_g1785z00_3383 = CDR(((obj_t) BgL_l1780z00_3380));
									{
										obj_t BgL_l1780z00_3385;
										obj_t BgL_tail1783z00_3386;

										BgL_l1780z00_3385 = BgL_g1785z00_3383;
										BgL_tail1783z00_3386 = BgL_head1782z00_3382;
									BgL_zc3z04anonymousza32550ze3z87_3387:
										if (NULLP(BgL_l1780z00_3385))
											{	/* SawBbv/bbv-merge.scm 693 */
												BgL_arg2546z00_3378 = BgL_head1782z00_3382;
											}
										else
											{	/* SawBbv/bbv-merge.scm 693 */
												obj_t BgL_newtail1784z00_3389;

												{	/* SawBbv/bbv-merge.scm 693 */
													obj_t BgL_arg2553z00_3391;

													{	/* SawBbv/bbv-merge.scm 693 */
														obj_t BgL_arg2554z00_3392;

														BgL_arg2554z00_3392 =
															CAR(((obj_t) BgL_l1780z00_3385));
														BgL_arg2553z00_3391 =
															BGl_blockzd2distze70z35zzsaw_bbvzd2mergezd2
															(BgL_arg2554z00_3392);
													}
													BgL_newtail1784z00_3389 =
														MAKE_YOUNG_PAIR(BgL_arg2553z00_3391, BNIL);
												}
												SET_CDR(BgL_tail1783z00_3386, BgL_newtail1784z00_3389);
												{	/* SawBbv/bbv-merge.scm 693 */
													obj_t BgL_arg2552z00_3390;

													BgL_arg2552z00_3390 =
														CDR(((obj_t) BgL_l1780z00_3385));
													{
														obj_t BgL_tail1783z00_9597;
														obj_t BgL_l1780z00_9596;

														BgL_l1780z00_9596 = BgL_arg2552z00_3390;
														BgL_tail1783z00_9597 = BgL_newtail1784z00_3389;
														BgL_tail1783z00_3386 = BgL_tail1783z00_9597;
														BgL_l1780z00_3385 = BgL_l1780z00_9596;
														goto BgL_zc3z04anonymousza32550ze3z87_3387;
													}
												}
											}
									}
								}
							}
					}
					BgL_lz00_3373 =
						BGl_sortz00zz__r4_vectors_6_8z00(BgL_arg2546z00_3378,
						BGl_proc3299z00zzsaw_bbvzd2mergezd2);
				}
				{	/* SawBbv/bbv-merge.scm 701 */
					obj_t BgL_val0_1786z00_3374;
					obj_t BgL_val1_1787z00_3375;

					{	/* SawBbv/bbv-merge.scm 701 */
						obj_t BgL_pairz00_5524;

						{	/* SawBbv/bbv-merge.scm 701 */
							obj_t BgL_pairz00_5523;

							BgL_pairz00_5523 = CAR(((obj_t) BgL_lz00_3373));
							BgL_pairz00_5524 = CDR(BgL_pairz00_5523);
						}
						BgL_val0_1786z00_3374 = CAR(BgL_pairz00_5524);
					}
					{	/* SawBbv/bbv-merge.scm 701 */
						obj_t BgL_pairz00_5529;

						{	/* SawBbv/bbv-merge.scm 701 */
							obj_t BgL_pairz00_5528;

							BgL_pairz00_5528 = CAR(((obj_t) BgL_lz00_3373));
							BgL_pairz00_5529 = CDR(BgL_pairz00_5528);
						}
						BgL_val1_1787z00_3375 = CDR(BgL_pairz00_5529);
					}
					{	/* SawBbv/bbv-merge.scm 701 */
						int BgL_tmpz00_9607;

						BgL_tmpz00_9607 = (int) (2L);
						BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9607);
					}
					{	/* SawBbv/bbv-merge.scm 701 */
						int BgL_tmpz00_9610;

						BgL_tmpz00_9610 = (int) (1L);
						BGL_MVALUES_VAL_SET(BgL_tmpz00_9610, BgL_val1_1787z00_3375);
					}
					return BgL_val0_1786z00_3374;
				}
			}
		}

	}



/* <@anonymous:2557>~0 */
	obj_t BGl_zc3z04anonymousza32557ze3ze70z60zzsaw_bbvzd2mergezd2(obj_t
		BgL_bsz00_6525, obj_t BgL_l1779z00_3397)
	{
		{	/* SawBbv/bbv-merge.scm 694 */
			if (NULLP(BgL_l1779z00_3397))
				{	/* SawBbv/bbv-merge.scm 694 */
					return BNIL;
				}
			else
				{	/* SawBbv/bbv-merge.scm 695 */
					obj_t BgL_arg2560z00_3400;
					obj_t BgL_arg2561z00_3401;

					{	/* SawBbv/bbv-merge.scm 695 */
						obj_t BgL_xz00_3402;

						BgL_xz00_3402 = CAR(((obj_t) BgL_l1779z00_3397));
						{	/* SawBbv/bbv-merge.scm 695 */
							obj_t BgL_head1775z00_3405;

							BgL_head1775z00_3405 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1773z00_3407;
								obj_t BgL_tail1776z00_3408;

								BgL_l1773z00_3407 = BgL_bsz00_6525;
								BgL_tail1776z00_3408 = BgL_head1775z00_3405;
							BgL_zc3z04anonymousza32563ze3z87_3409:
								if (NULLP(BgL_l1773z00_3407))
									{	/* SawBbv/bbv-merge.scm 695 */
										BgL_arg2560z00_3400 = CDR(BgL_head1775z00_3405);
									}
								else
									{	/* SawBbv/bbv-merge.scm 695 */
										obj_t BgL_newtail1777z00_3411;

										{	/* SawBbv/bbv-merge.scm 695 */
											obj_t BgL_arg2566z00_3413;

											{	/* SawBbv/bbv-merge.scm 695 */
												obj_t BgL_yz00_3414;

												BgL_yz00_3414 = CAR(((obj_t) BgL_l1773z00_3407));
												BgL_arg2566z00_3413 =
													MAKE_YOUNG_PAIR(BgL_xz00_3402, BgL_yz00_3414);
											}
											BgL_newtail1777z00_3411 =
												MAKE_YOUNG_PAIR(BgL_arg2566z00_3413, BNIL);
										}
										SET_CDR(BgL_tail1776z00_3408, BgL_newtail1777z00_3411);
										{	/* SawBbv/bbv-merge.scm 695 */
											obj_t BgL_arg2565z00_3412;

											BgL_arg2565z00_3412 = CDR(((obj_t) BgL_l1773z00_3407));
											{
												obj_t BgL_tail1776z00_9629;
												obj_t BgL_l1773z00_9628;

												BgL_l1773z00_9628 = BgL_arg2565z00_3412;
												BgL_tail1776z00_9629 = BgL_newtail1777z00_3411;
												BgL_tail1776z00_3408 = BgL_tail1776z00_9629;
												BgL_l1773z00_3407 = BgL_l1773z00_9628;
												goto BgL_zc3z04anonymousza32563ze3z87_3409;
											}
										}
									}
							}
						}
					}
					{	/* SawBbv/bbv-merge.scm 694 */
						obj_t BgL_arg2567z00_3416;

						BgL_arg2567z00_3416 = CDR(((obj_t) BgL_l1779z00_3397));
						BgL_arg2561z00_3401 =
							BGl_zc3z04anonymousza32557ze3ze70z60zzsaw_bbvzd2mergezd2
							(BgL_bsz00_6525, BgL_arg2567z00_3416);
					}
					return bgl_append2(BgL_arg2560z00_3400, BgL_arg2561z00_3401);
				}
		}

	}



/* block-dist~0 */
	obj_t BGl_blockzd2distze70z35zzsaw_bbvzd2mergezd2(obj_t BgL_pz00_3566)
	{
		{	/* SawBbv/bbv-merge.scm 690 */
			{
				BgL_bbvzd2ctxentryzd2_bglt BgL_xz00_3424;
				BgL_bbvzd2ctxentryzd2_bglt BgL_yz00_3425;
				BgL_bbvzd2ctxzd2_bglt BgL_xctxz00_3545;
				BgL_bbvzd2ctxzd2_bglt BgL_yctxz00_3546;

				{	/* SawBbv/bbv-merge.scm 684 */
					obj_t BgL_xz00_3568;
					obj_t BgL_yz00_3569;

					BgL_xz00_3568 = CAR(BgL_pz00_3566);
					BgL_yz00_3569 = CDR(BgL_pz00_3566);
					if ((BgL_xz00_3568 == BgL_yz00_3569))
						{	/* SawBbv/bbv-merge.scm 686 */
							return MAKE_YOUNG_PAIR(BINT(BGL_LONG_MAX), BgL_pz00_3566);
						}
					else
						{	/* SawBbv/bbv-merge.scm 690 */
							obj_t BgL_arg2680z00_3573;

							{	/* SawBbv/bbv-merge.scm 690 */
								BgL_bbvzd2ctxzd2_bglt BgL_arg2681z00_3574;
								BgL_bbvzd2ctxzd2_bglt BgL_arg2682z00_3575;

								{
									BgL_blocksz00_bglt BgL_auxz00_9640;

									{
										obj_t BgL_auxz00_9641;

										{	/* SawBbv/bbv-merge.scm 690 */
											BgL_objectz00_bglt BgL_tmpz00_9642;

											BgL_tmpz00_9642 =
												((BgL_objectz00_bglt)
												((BgL_blockz00_bglt) BgL_xz00_3568));
											BgL_auxz00_9641 = BGL_OBJECT_WIDENING(BgL_tmpz00_9642);
										}
										BgL_auxz00_9640 = ((BgL_blocksz00_bglt) BgL_auxz00_9641);
									}
									BgL_arg2681z00_3574 =
										(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_9640))->
										BgL_ctxz00);
								}
								{
									BgL_blocksz00_bglt BgL_auxz00_9648;

									{
										obj_t BgL_auxz00_9649;

										{	/* SawBbv/bbv-merge.scm 690 */
											BgL_objectz00_bglt BgL_tmpz00_9650;

											BgL_tmpz00_9650 =
												((BgL_objectz00_bglt)
												((BgL_blockz00_bglt) BgL_yz00_3569));
											BgL_auxz00_9649 = BGL_OBJECT_WIDENING(BgL_tmpz00_9650);
										}
										BgL_auxz00_9648 = ((BgL_blocksz00_bglt) BgL_auxz00_9649);
									}
									BgL_arg2682z00_3575 =
										(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_9648))->
										BgL_ctxz00);
								}
								BgL_xctxz00_3545 = BgL_arg2681z00_3574;
								BgL_yctxz00_3546 = BgL_arg2682z00_3575;
								{	/* SawBbv/bbv-merge.scm 677 */
									obj_t BgL_runner2675z00_3565;

									{	/* SawBbv/bbv-merge.scm 678 */
										obj_t BgL_l1768z00_3549;

										BgL_l1768z00_3549 =
											(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_xctxz00_3545))->
											BgL_entriesz00);
										if (NULLP(BgL_l1768z00_3549))
											{	/* SawBbv/bbv-merge.scm 678 */
												BgL_runner2675z00_3565 = BNIL;
											}
										else
											{	/* SawBbv/bbv-merge.scm 678 */
												obj_t BgL_head1770z00_3551;

												BgL_head1770z00_3551 = MAKE_YOUNG_PAIR(BNIL, BNIL);
												{
													obj_t BgL_l1768z00_3553;
													obj_t BgL_tail1771z00_3554;

													BgL_l1768z00_3553 = BgL_l1768z00_3549;
													BgL_tail1771z00_3554 = BgL_head1770z00_3551;
												BgL_zc3z04anonymousza32668ze3z87_3555:
													if (NULLP(BgL_l1768z00_3553))
														{	/* SawBbv/bbv-merge.scm 678 */
															BgL_runner2675z00_3565 =
																CDR(BgL_head1770z00_3551);
														}
													else
														{	/* SawBbv/bbv-merge.scm 678 */
															obj_t BgL_newtail1772z00_3557;

															{	/* SawBbv/bbv-merge.scm 678 */
																long BgL_arg2672z00_3559;

																{	/* SawBbv/bbv-merge.scm 678 */
																	obj_t BgL_ez00_3560;

																	BgL_ez00_3560 =
																		CAR(((obj_t) BgL_l1768z00_3553));
																	{	/* SawBbv/bbv-merge.scm 680 */
																		obj_t BgL_arg2673z00_3562;

																		{	/* SawBbv/bbv-merge.scm 680 */
																			BgL_rtl_regz00_bglt BgL_arg2674z00_3563;

																			BgL_arg2674z00_3563 =
																				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
																						((BgL_bbvzd2ctxentryzd2_bglt)
																							BgL_ez00_3560)))->BgL_regz00);
																			BgL_arg2673z00_3562 =
																				BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2
																				(BgL_yctxz00_3546, BgL_arg2674z00_3563);
																		}
																		BgL_xz00_3424 =
																			((BgL_bbvzd2ctxentryzd2_bglt)
																			BgL_ez00_3560);
																		BgL_yz00_3425 =
																			((BgL_bbvzd2ctxentryzd2_bglt)
																			BgL_arg2673z00_3562);
																		{	/* SawBbv/bbv-merge.scm 651 */
																			bool_t BgL_test3660z00_9668;

																			if (BGl_equalzf3zf3zz__r4_equivalence_6_2z00((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_xz00_3424))->BgL_typesz00), (((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_yz00_3425))->BgL_typesz00)))
																				{	/* SawBbv/bbv-merge.scm 651 */
																					BgL_test3660z00_9668 =
																						(BBOOL(
																							(((BgL_bbvzd2ctxentryzd2_bglt)
																									COBJECT(BgL_xz00_3424))->
																								BgL_polarityz00)) ==
																						BBOOL((((BgL_bbvzd2ctxentryzd2_bglt)
																									COBJECT(BgL_yz00_3425))->
																								BgL_polarityz00)));
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 651 */
																					BgL_test3660z00_9668 = ((bool_t) 0);
																				}
																			if (BgL_test3660z00_9668)
																				{	/* SawBbv/bbv-merge.scm 653 */
																					bool_t BgL_test3662z00_9678;

																					{	/* SawBbv/bbv-merge.scm 653 */
																						bool_t BgL_test3663z00_9679;

																						{	/* SawBbv/bbv-merge.scm 653 */
																							obj_t BgL_arg2623z00_3467;

																							BgL_arg2623z00_3467 =
																								(((BgL_bbvzd2ctxentryzd2_bglt)
																									COBJECT(BgL_xz00_3424))->
																								BgL_valuez00);
																							{	/* SawBbv/bbv-merge.scm 653 */
																								obj_t BgL_classz00_5351;

																								BgL_classz00_5351 =
																									BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2;
																								if (BGL_OBJECTP
																									(BgL_arg2623z00_3467))
																									{	/* SawBbv/bbv-merge.scm 653 */
																										BgL_objectz00_bglt
																											BgL_arg1807z00_5353;
																										BgL_arg1807z00_5353 =
																											(BgL_objectz00_bglt)
																											(BgL_arg2623z00_3467);
																										if (BGL_CONDEXPAND_ISA_ARCH64())
																											{	/* SawBbv/bbv-merge.scm 653 */
																												long BgL_idxz00_5359;

																												BgL_idxz00_5359 =
																													BGL_OBJECT_INHERITANCE_NUM
																													(BgL_arg1807z00_5353);
																												BgL_test3663z00_9679 =
																													(VECTOR_REF
																													(BGl_za2inheritancesza2z00zz__objectz00,
																														(BgL_idxz00_5359 +
																															1L)) ==
																													BgL_classz00_5351);
																											}
																										else
																											{	/* SawBbv/bbv-merge.scm 653 */
																												bool_t
																													BgL_res3263z00_5384;
																												{	/* SawBbv/bbv-merge.scm 653 */
																													obj_t
																														BgL_oclassz00_5367;
																													{	/* SawBbv/bbv-merge.scm 653 */
																														obj_t
																															BgL_arg1815z00_5375;
																														long
																															BgL_arg1816z00_5376;
																														BgL_arg1815z00_5375
																															=
																															(BGl_za2classesza2z00zz__objectz00);
																														{	/* SawBbv/bbv-merge.scm 653 */
																															long
																																BgL_arg1817z00_5377;
																															BgL_arg1817z00_5377
																																=
																																BGL_OBJECT_CLASS_NUM
																																(BgL_arg1807z00_5353);
																															BgL_arg1816z00_5376
																																=
																																(BgL_arg1817z00_5377
																																- OBJECT_TYPE);
																														}
																														BgL_oclassz00_5367 =
																															VECTOR_REF
																															(BgL_arg1815z00_5375,
																															BgL_arg1816z00_5376);
																													}
																													{	/* SawBbv/bbv-merge.scm 653 */
																														bool_t
																															BgL__ortest_1115z00_5368;
																														BgL__ortest_1115z00_5368
																															=
																															(BgL_classz00_5351
																															==
																															BgL_oclassz00_5367);
																														if (BgL__ortest_1115z00_5368)
																															{	/* SawBbv/bbv-merge.scm 653 */
																																BgL_res3263z00_5384
																																	=
																																	BgL__ortest_1115z00_5368;
																															}
																														else
																															{	/* SawBbv/bbv-merge.scm 653 */
																																long
																																	BgL_odepthz00_5369;
																																{	/* SawBbv/bbv-merge.scm 653 */
																																	obj_t
																																		BgL_arg1804z00_5370;
																																	BgL_arg1804z00_5370
																																		=
																																		(BgL_oclassz00_5367);
																																	BgL_odepthz00_5369
																																		=
																																		BGL_CLASS_DEPTH
																																		(BgL_arg1804z00_5370);
																																}
																																if (
																																	(1L <
																																		BgL_odepthz00_5369))
																																	{	/* SawBbv/bbv-merge.scm 653 */
																																		obj_t
																																			BgL_arg1802z00_5372;
																																		{	/* SawBbv/bbv-merge.scm 653 */
																																			obj_t
																																				BgL_arg1803z00_5373;
																																			BgL_arg1803z00_5373
																																				=
																																				(BgL_oclassz00_5367);
																																			BgL_arg1802z00_5372
																																				=
																																				BGL_CLASS_ANCESTORS_REF
																																				(BgL_arg1803z00_5373,
																																				1L);
																																		}
																																		BgL_res3263z00_5384
																																			=
																																			(BgL_arg1802z00_5372
																																			==
																																			BgL_classz00_5351);
																																	}
																																else
																																	{	/* SawBbv/bbv-merge.scm 653 */
																																		BgL_res3263z00_5384
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																													}
																												}
																												BgL_test3663z00_9679 =
																													BgL_res3263z00_5384;
																											}
																									}
																								else
																									{	/* SawBbv/bbv-merge.scm 653 */
																										BgL_test3663z00_9679 =
																											((bool_t) 0);
																									}
																							}
																						}
																						if (BgL_test3663z00_9679)
																							{	/* SawBbv/bbv-merge.scm 653 */
																								obj_t BgL_arg2622z00_3466;

																								BgL_arg2622z00_3466 =
																									(((BgL_bbvzd2ctxentryzd2_bglt)
																										COBJECT(BgL_yz00_3425))->
																									BgL_valuez00);
																								{	/* SawBbv/bbv-merge.scm 653 */
																									obj_t BgL_classz00_5385;

																									BgL_classz00_5385 =
																										BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2;
																									if (BGL_OBJECTP
																										(BgL_arg2622z00_3466))
																										{	/* SawBbv/bbv-merge.scm 653 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_5387;
																											BgL_arg1807z00_5387 =
																												(BgL_objectz00_bglt)
																												(BgL_arg2622z00_3466);
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* SawBbv/bbv-merge.scm 653 */
																													long BgL_idxz00_5393;

																													BgL_idxz00_5393 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_5387);
																													BgL_test3662z00_9678 =
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_5393 +
																																1L)) ==
																														BgL_classz00_5385);
																												}
																											else
																												{	/* SawBbv/bbv-merge.scm 653 */
																													bool_t
																														BgL_res3264z00_5418;
																													{	/* SawBbv/bbv-merge.scm 653 */
																														obj_t
																															BgL_oclassz00_5401;
																														{	/* SawBbv/bbv-merge.scm 653 */
																															obj_t
																																BgL_arg1815z00_5409;
																															long
																																BgL_arg1816z00_5410;
																															BgL_arg1815z00_5409
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* SawBbv/bbv-merge.scm 653 */
																																long
																																	BgL_arg1817z00_5411;
																																BgL_arg1817z00_5411
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_5387);
																																BgL_arg1816z00_5410
																																	=
																																	(BgL_arg1817z00_5411
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_5401
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_5409,
																																BgL_arg1816z00_5410);
																														}
																														{	/* SawBbv/bbv-merge.scm 653 */
																															bool_t
																																BgL__ortest_1115z00_5402;
																															BgL__ortest_1115z00_5402
																																=
																																(BgL_classz00_5385
																																==
																																BgL_oclassz00_5401);
																															if (BgL__ortest_1115z00_5402)
																																{	/* SawBbv/bbv-merge.scm 653 */
																																	BgL_res3264z00_5418
																																		=
																																		BgL__ortest_1115z00_5402;
																																}
																															else
																																{	/* SawBbv/bbv-merge.scm 653 */
																																	long
																																		BgL_odepthz00_5403;
																																	{	/* SawBbv/bbv-merge.scm 653 */
																																		obj_t
																																			BgL_arg1804z00_5404;
																																		BgL_arg1804z00_5404
																																			=
																																			(BgL_oclassz00_5401);
																																		BgL_odepthz00_5403
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_5404);
																																	}
																																	if (
																																		(1L <
																																			BgL_odepthz00_5403))
																																		{	/* SawBbv/bbv-merge.scm 653 */
																																			obj_t
																																				BgL_arg1802z00_5406;
																																			{	/* SawBbv/bbv-merge.scm 653 */
																																				obj_t
																																					BgL_arg1803z00_5407;
																																				BgL_arg1803z00_5407
																																					=
																																					(BgL_oclassz00_5401);
																																				BgL_arg1802z00_5406
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_5407,
																																					1L);
																																			}
																																			BgL_res3264z00_5418
																																				=
																																				(BgL_arg1802z00_5406
																																				==
																																				BgL_classz00_5385);
																																		}
																																	else
																																		{	/* SawBbv/bbv-merge.scm 653 */
																																			BgL_res3264z00_5418
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test3662z00_9678 =
																														BgL_res3264z00_5418;
																												}
																										}
																									else
																										{	/* SawBbv/bbv-merge.scm 653 */
																											BgL_test3662z00_9678 =
																												((bool_t) 0);
																										}
																								}
																							}
																						else
																							{	/* SawBbv/bbv-merge.scm 653 */
																								BgL_test3662z00_9678 =
																									((bool_t) 0);
																							}
																					}
																					if (BgL_test3662z00_9678)
																						{	/* SawBbv/bbv-merge.scm 654 */
																							BgL_bbvzd2rangezd2_bglt
																								BgL_i1259z00_3442;
																							BgL_i1259z00_3442 =
																								((BgL_bbvzd2rangezd2_bglt) ((
																										(BgL_bbvzd2ctxentryzd2_bglt)
																										COBJECT(BgL_xz00_3424))->
																									BgL_valuez00));
																							{	/* SawBbv/bbv-merge.scm 655 */
																								BgL_bbvzd2rangezd2_bglt
																									BgL_i1260z00_3443;
																								BgL_i1260z00_3443 =
																									((BgL_bbvzd2rangezd2_bglt) ((
																											(BgL_bbvzd2ctxentryzd2_bglt)
																											COBJECT(BgL_yz00_3425))->
																										BgL_valuez00));
																								{	/* SawBbv/bbv-merge.scm 656 */
																									bool_t BgL_test3672z00_9730;

																									if (
																										((long) CINT(
																												(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_i1259z00_3442))->BgL_loz00)) == (long) CINT((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_i1260z00_3443))->BgL_loz00))))
																										{	/* SawBbv/bbv-merge.scm 656 */
																											BgL_test3672z00_9730 =
																												(
																												(long) CINT(
																													(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_i1259z00_3442))->BgL_upz00)) == (long) CINT((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_i1260z00_3443))->BgL_upz00)));
																										}
																									else
																										{	/* SawBbv/bbv-merge.scm 656 */
																											BgL_test3672z00_9730 =
																												((bool_t) 0);
																										}
																									if (BgL_test3672z00_9730)
																										{	/* SawBbv/bbv-merge.scm 656 */
																											BgL_arg2672z00_3559 = 0L;
																										}
																									else
																										{	/* SawBbv/bbv-merge.scm 656 */
																											BgL_arg2672z00_3559 = 1L;
																										}
																								}
																							}
																						}
																					else
																						{	/* SawBbv/bbv-merge.scm 659 */
																							bool_t BgL_test3674z00_9742;

																							{	/* SawBbv/bbv-merge.scm 659 */
																								bool_t BgL_test3675z00_9743;

																								{	/* SawBbv/bbv-merge.scm 659 */
																									obj_t BgL_arg2621z00_3464;

																									BgL_arg2621z00_3464 =
																										(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_xz00_3424))->BgL_valuez00);
																									{	/* SawBbv/bbv-merge.scm 659 */
																										obj_t BgL_classz00_5423;

																										BgL_classz00_5423 =
																											BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2;
																										if (BGL_OBJECTP
																											(BgL_arg2621z00_3464))
																											{	/* SawBbv/bbv-merge.scm 659 */
																												BgL_objectz00_bglt
																													BgL_arg1807z00_5425;
																												BgL_arg1807z00_5425 =
																													(BgL_objectz00_bglt)
																													(BgL_arg2621z00_3464);
																												if (BGL_CONDEXPAND_ISA_ARCH64())
																													{	/* SawBbv/bbv-merge.scm 659 */
																														long
																															BgL_idxz00_5431;
																														BgL_idxz00_5431 =
																															BGL_OBJECT_INHERITANCE_NUM
																															(BgL_arg1807z00_5425);
																														BgL_test3675z00_9743
																															=
																															(VECTOR_REF
																															(BGl_za2inheritancesza2z00zz__objectz00,
																																(BgL_idxz00_5431
																																	+ 1L)) ==
																															BgL_classz00_5423);
																													}
																												else
																													{	/* SawBbv/bbv-merge.scm 659 */
																														bool_t
																															BgL_res3265z00_5456;
																														{	/* SawBbv/bbv-merge.scm 659 */
																															obj_t
																																BgL_oclassz00_5439;
																															{	/* SawBbv/bbv-merge.scm 659 */
																																obj_t
																																	BgL_arg1815z00_5447;
																																long
																																	BgL_arg1816z00_5448;
																																BgL_arg1815z00_5447
																																	=
																																	(BGl_za2classesza2z00zz__objectz00);
																																{	/* SawBbv/bbv-merge.scm 659 */
																																	long
																																		BgL_arg1817z00_5449;
																																	BgL_arg1817z00_5449
																																		=
																																		BGL_OBJECT_CLASS_NUM
																																		(BgL_arg1807z00_5425);
																																	BgL_arg1816z00_5448
																																		=
																																		(BgL_arg1817z00_5449
																																		-
																																		OBJECT_TYPE);
																																}
																																BgL_oclassz00_5439
																																	=
																																	VECTOR_REF
																																	(BgL_arg1815z00_5447,
																																	BgL_arg1816z00_5448);
																															}
																															{	/* SawBbv/bbv-merge.scm 659 */
																																bool_t
																																	BgL__ortest_1115z00_5440;
																																BgL__ortest_1115z00_5440
																																	=
																																	(BgL_classz00_5423
																																	==
																																	BgL_oclassz00_5439);
																																if (BgL__ortest_1115z00_5440)
																																	{	/* SawBbv/bbv-merge.scm 659 */
																																		BgL_res3265z00_5456
																																			=
																																			BgL__ortest_1115z00_5440;
																																	}
																																else
																																	{	/* SawBbv/bbv-merge.scm 659 */
																																		long
																																			BgL_odepthz00_5441;
																																		{	/* SawBbv/bbv-merge.scm 659 */
																																			obj_t
																																				BgL_arg1804z00_5442;
																																			BgL_arg1804z00_5442
																																				=
																																				(BgL_oclassz00_5439);
																																			BgL_odepthz00_5441
																																				=
																																				BGL_CLASS_DEPTH
																																				(BgL_arg1804z00_5442);
																																		}
																																		if (
																																			(1L <
																																				BgL_odepthz00_5441))
																																			{	/* SawBbv/bbv-merge.scm 659 */
																																				obj_t
																																					BgL_arg1802z00_5444;
																																				{	/* SawBbv/bbv-merge.scm 659 */
																																					obj_t
																																						BgL_arg1803z00_5445;
																																					BgL_arg1803z00_5445
																																						=
																																						(BgL_oclassz00_5439);
																																					BgL_arg1802z00_5444
																																						=
																																						BGL_CLASS_ANCESTORS_REF
																																						(BgL_arg1803z00_5445,
																																						1L);
																																				}
																																				BgL_res3265z00_5456
																																					=
																																					(BgL_arg1802z00_5444
																																					==
																																					BgL_classz00_5423);
																																			}
																																		else
																																			{	/* SawBbv/bbv-merge.scm 659 */
																																				BgL_res3265z00_5456
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																															}
																														}
																														BgL_test3675z00_9743
																															=
																															BgL_res3265z00_5456;
																													}
																											}
																										else
																											{	/* SawBbv/bbv-merge.scm 659 */
																												BgL_test3675z00_9743 =
																													((bool_t) 0);
																											}
																									}
																								}
																								if (BgL_test3675z00_9743)
																									{	/* SawBbv/bbv-merge.scm 659 */
																										BgL_test3674z00_9742 =
																											((bool_t) 1);
																									}
																								else
																									{	/* SawBbv/bbv-merge.scm 659 */
																										obj_t BgL_arg2620z00_3463;

																										BgL_arg2620z00_3463 =
																											(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_yz00_3425))->BgL_valuez00);
																										{	/* SawBbv/bbv-merge.scm 659 */
																											obj_t BgL_classz00_5457;

																											BgL_classz00_5457 =
																												BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2;
																											if (BGL_OBJECTP
																												(BgL_arg2620z00_3463))
																												{	/* SawBbv/bbv-merge.scm 659 */
																													BgL_objectz00_bglt
																														BgL_arg1807z00_5459;
																													BgL_arg1807z00_5459 =
																														(BgL_objectz00_bglt)
																														(BgL_arg2620z00_3463);
																													if (BGL_CONDEXPAND_ISA_ARCH64())
																														{	/* SawBbv/bbv-merge.scm 659 */
																															long
																																BgL_idxz00_5465;
																															BgL_idxz00_5465 =
																																BGL_OBJECT_INHERITANCE_NUM
																																(BgL_arg1807z00_5459);
																															BgL_test3674z00_9742
																																=
																																(VECTOR_REF
																																(BGl_za2inheritancesza2z00zz__objectz00,
																																	(BgL_idxz00_5465
																																		+ 1L)) ==
																																BgL_classz00_5457);
																														}
																													else
																														{	/* SawBbv/bbv-merge.scm 659 */
																															bool_t
																																BgL_res3266z00_5490;
																															{	/* SawBbv/bbv-merge.scm 659 */
																																obj_t
																																	BgL_oclassz00_5473;
																																{	/* SawBbv/bbv-merge.scm 659 */
																																	obj_t
																																		BgL_arg1815z00_5481;
																																	long
																																		BgL_arg1816z00_5482;
																																	BgL_arg1815z00_5481
																																		=
																																		(BGl_za2classesza2z00zz__objectz00);
																																	{	/* SawBbv/bbv-merge.scm 659 */
																																		long
																																			BgL_arg1817z00_5483;
																																		BgL_arg1817z00_5483
																																			=
																																			BGL_OBJECT_CLASS_NUM
																																			(BgL_arg1807z00_5459);
																																		BgL_arg1816z00_5482
																																			=
																																			(BgL_arg1817z00_5483
																																			-
																																			OBJECT_TYPE);
																																	}
																																	BgL_oclassz00_5473
																																		=
																																		VECTOR_REF
																																		(BgL_arg1815z00_5481,
																																		BgL_arg1816z00_5482);
																																}
																																{	/* SawBbv/bbv-merge.scm 659 */
																																	bool_t
																																		BgL__ortest_1115z00_5474;
																																	BgL__ortest_1115z00_5474
																																		=
																																		(BgL_classz00_5457
																																		==
																																		BgL_oclassz00_5473);
																																	if (BgL__ortest_1115z00_5474)
																																		{	/* SawBbv/bbv-merge.scm 659 */
																																			BgL_res3266z00_5490
																																				=
																																				BgL__ortest_1115z00_5474;
																																		}
																																	else
																																		{	/* SawBbv/bbv-merge.scm 659 */
																																			long
																																				BgL_odepthz00_5475;
																																			{	/* SawBbv/bbv-merge.scm 659 */
																																				obj_t
																																					BgL_arg1804z00_5476;
																																				BgL_arg1804z00_5476
																																					=
																																					(BgL_oclassz00_5473);
																																				BgL_odepthz00_5475
																																					=
																																					BGL_CLASS_DEPTH
																																					(BgL_arg1804z00_5476);
																																			}
																																			if (
																																				(1L <
																																					BgL_odepthz00_5475))
																																				{	/* SawBbv/bbv-merge.scm 659 */
																																					obj_t
																																						BgL_arg1802z00_5478;
																																					{	/* SawBbv/bbv-merge.scm 659 */
																																						obj_t
																																							BgL_arg1803z00_5479;
																																						BgL_arg1803z00_5479
																																							=
																																							(BgL_oclassz00_5473);
																																						BgL_arg1802z00_5478
																																							=
																																							BGL_CLASS_ANCESTORS_REF
																																							(BgL_arg1803z00_5479,
																																							1L);
																																					}
																																					BgL_res3266z00_5490
																																						=
																																						(BgL_arg1802z00_5478
																																						==
																																						BgL_classz00_5457);
																																				}
																																			else
																																				{	/* SawBbv/bbv-merge.scm 659 */
																																					BgL_res3266z00_5490
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																															}
																															BgL_test3674z00_9742
																																=
																																BgL_res3266z00_5490;
																														}
																												}
																											else
																												{	/* SawBbv/bbv-merge.scm 659 */
																													BgL_test3674z00_9742 =
																														((bool_t) 0);
																												}
																										}
																									}
																							}
																							if (BgL_test3674z00_9742)
																								{	/* SawBbv/bbv-merge.scm 659 */
																									BgL_arg2672z00_3559 = 3L;
																								}
																							else
																								{	/* SawBbv/bbv-merge.scm 659 */
																									BgL_arg2672z00_3559 = 2L;
																								}
																						}
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 651 */
																					if (
																						(BBOOL(
																								(((BgL_bbvzd2ctxentryzd2_bglt)
																										COBJECT(BgL_xz00_3424))->
																									BgL_polarityz00)) ==
																							BBOOL(((
																										(BgL_bbvzd2ctxentryzd2_bglt)
																										COBJECT(BgL_yz00_3425))->
																									BgL_polarityz00))))
																						{	/* SawBbv/bbv-merge.scm 663 */
																							if (
																								(((BgL_bbvzd2ctxentryzd2_bglt)
																										COBJECT(BgL_xz00_3424))->
																									BgL_polarityz00))
																								{	/* SawBbv/bbv-merge.scm 670 */
																									bool_t BgL_test3686z00_9798;

																									{	/* SawBbv/bbv-merge.scm 670 */
																										bool_t BgL_test3687z00_9799;

																										{	/* SawBbv/bbv-merge.scm 670 */
																											obj_t BgL_arg2636z00_3479;

																											BgL_arg2636z00_3479 =
																												(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_xz00_3424))->BgL_typesz00);
																											BgL_test3687z00_9799 =
																												CBOOL
																												(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																												(BGl_za2objza2z00zztype_cachez00,
																													BgL_arg2636z00_3479));
																										}
																										if (BgL_test3687z00_9799)
																											{	/* SawBbv/bbv-merge.scm 670 */
																												BgL_test3686z00_9798 =
																													((bool_t) 1);
																											}
																										else
																											{	/* SawBbv/bbv-merge.scm 670 */
																												obj_t
																													BgL_arg2635z00_3478;
																												BgL_arg2635z00_3478 =
																													(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_yz00_3425))->BgL_typesz00);
																												BgL_test3686z00_9798 =
																													CBOOL
																													(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																													(BGl_za2objza2z00zztype_cachez00,
																														BgL_arg2635z00_3478));
																											}
																									}
																									if (BgL_test3686z00_9798)
																										{	/* SawBbv/bbv-merge.scm 670 */
																											BgL_arg2672z00_3559 = 4L;
																										}
																									else
																										{	/* SawBbv/bbv-merge.scm 670 */
																											BgL_arg2672z00_3559 = 11L;
																										}
																								}
																							else
																								{	/* SawBbv/bbv-merge.scm 666 */
																									bool_t BgL_test3688z00_9806;

																									{	/* SawBbv/bbv-merge.scm 666 */
																										bool_t BgL_test3689z00_9807;

																										{
																											obj_t BgL_l1760z00_3529;

																											BgL_l1760z00_3529 =
																												(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_xz00_3424))->BgL_typesz00);
																										BgL_zc3z04anonymousza32654ze3z87_3530:
																											if (NULLP
																												(BgL_l1760z00_3529))
																												{	/* SawBbv/bbv-merge.scm 666 */
																													BgL_test3689z00_9807 =
																														((bool_t) 1);
																												}
																											else
																												{	/* SawBbv/bbv-merge.scm 666 */
																													if (CBOOL
																														(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																															(CAR(((obj_t)
																																		BgL_l1760z00_3529)),
																																(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_yz00_3425))->BgL_typesz00))))
																														{
																															obj_t
																																BgL_l1760z00_9816;
																															BgL_l1760z00_9816
																																=
																																CDR(((obj_t)
																																	BgL_l1760z00_3529));
																															BgL_l1760z00_3529
																																=
																																BgL_l1760z00_9816;
																															goto
																																BgL_zc3z04anonymousza32654ze3z87_3530;
																														}
																													else
																														{	/* SawBbv/bbv-merge.scm 666 */
																															BgL_test3689z00_9807
																																= ((bool_t) 0);
																														}
																												}
																										}
																										if (BgL_test3689z00_9807)
																											{	/* SawBbv/bbv-merge.scm 666 */
																												BgL_test3688z00_9806 =
																													((bool_t) 1);
																											}
																										else
																											{
																												obj_t BgL_l1764z00_3518;

																												BgL_l1764z00_3518 =
																													(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_yz00_3425))->BgL_typesz00);
																											BgL_zc3z04anonymousza32650ze3z87_3519:
																												if (NULLP
																													(BgL_l1764z00_3518))
																													{	/* SawBbv/bbv-merge.scm 667 */
																														BgL_test3688z00_9806
																															= ((bool_t) 1);
																													}
																												else
																													{	/* SawBbv/bbv-merge.scm 667 */
																														if (CBOOL
																															(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																																(CAR(((obj_t)
																																			BgL_l1764z00_3518)),
																																	(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_xz00_3424))->BgL_typesz00))))
																															{
																																obj_t
																																	BgL_l1764z00_9828;
																																BgL_l1764z00_9828
																																	=
																																	CDR(((obj_t)
																																		BgL_l1764z00_3518));
																																BgL_l1764z00_3518
																																	=
																																	BgL_l1764z00_9828;
																																goto
																																	BgL_zc3z04anonymousza32650ze3z87_3519;
																															}
																														else
																															{	/* SawBbv/bbv-merge.scm 667 */
																																BgL_test3688z00_9806
																																	=
																																	((bool_t) 0);
																															}
																													}
																											}
																									}
																									if (BgL_test3688z00_9806)
																										{	/* SawBbv/bbv-merge.scm 666 */
																											BgL_arg2672z00_3559 = 4L;
																										}
																									else
																										{	/* SawBbv/bbv-merge.scm 666 */
																											BgL_arg2672z00_3559 = 5L;
																										}
																								}
																						}
																					else
																						{	/* SawBbv/bbv-merge.scm 663 */
																							BgL_arg2672z00_3559 = 10L;
																						}
																				}
																		}
																	}
																}
																BgL_newtail1772z00_3557 =
																	MAKE_YOUNG_PAIR(BINT(BgL_arg2672z00_3559),
																	BNIL);
															}
															SET_CDR(BgL_tail1771z00_3554,
																BgL_newtail1772z00_3557);
															{	/* SawBbv/bbv-merge.scm 678 */
																obj_t BgL_arg2670z00_3558;

																BgL_arg2670z00_3558 =
																	CDR(((obj_t) BgL_l1768z00_3553));
																{
																	obj_t BgL_tail1771z00_9840;
																	obj_t BgL_l1768z00_9839;

																	BgL_l1768z00_9839 = BgL_arg2670z00_3558;
																	BgL_tail1771z00_9840 =
																		BgL_newtail1772z00_3557;
																	BgL_tail1771z00_3554 = BgL_tail1771z00_9840;
																	BgL_l1768z00_3553 = BgL_l1768z00_9839;
																	goto BgL_zc3z04anonymousza32668ze3z87_3555;
																}
															}
														}
												}
											}
									}
									BgL_arg2680z00_3573 =
										BGl_zb2zb2zz__r4_numbers_6_5z00(BgL_runner2675z00_3565);
								}
							}
							return MAKE_YOUNG_PAIR(BgL_arg2680z00_3573, BgL_pz00_3566);
						}
				}
			}
		}

	}



/* &<@anonymous:2568> */
	obj_t BGl_z62zc3z04anonymousza32568ze3ze5zzsaw_bbvzd2mergezd2(obj_t
		BgL_envz00_6522, obj_t BgL_xz00_6523, obj_t BgL_yz00_6524)
	{
		{	/* SawBbv/bbv-merge.scm 697 */
			return
				BBOOL(
				((long) CINT(CAR(
							((obj_t) BgL_xz00_6523))) <=
					(long) CINT(CAR(((obj_t) BgL_yz00_6524)))));
		}

	}



/* bbv-block-merge-select-strategy-random */
	obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2randomzd2zzsaw_bbvzd2mergezd2
		(obj_t BgL_bsz00_268)
	{
		{	/* SawBbv/bbv-merge.scm 708 */
			{	/* SawBbv/bbv-merge.scm 710 */
				long BgL_lenz00_3579;

				BgL_lenz00_3579 = bgl_list_length(BgL_bsz00_268);
				{	/* SawBbv/bbv-merge.scm 710 */
					long BgL_xz00_3580;

					if ((BgL_lenz00_3579 == 0L))
						{	/* SawBbv/bbv-merge.scm 711 */
							BgL_xz00_3580 = 0L;
						}
					else
						{	/* SawBbv/bbv-merge.scm 711 */
							int BgL_arg1330z00_5532;

							BgL_arg1330z00_5532 = rand();
							BgL_xz00_3580 =
								BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
								(long) (BgL_arg1330z00_5532), BgL_lenz00_3579);
						}
					{	/* SawBbv/bbv-merge.scm 711 */

						{

						BgL_zc3z04anonymousza32683ze3z87_3582:
							{	/* SawBbv/bbv-merge.scm 713 */
								long BgL_yz00_3583;

								if ((BgL_lenz00_3579 == 0L))
									{	/* SawBbv/bbv-merge.scm 713 */
										BgL_yz00_3583 = 0L;
									}
								else
									{	/* SawBbv/bbv-merge.scm 713 */
										int BgL_arg1330z00_5536;

										BgL_arg1330z00_5536 = rand();
										BgL_yz00_3583 =
											BGl_modulofxz00zz__r4_numbers_6_5_fixnumz00(
											(long) (BgL_arg1330z00_5536), BgL_lenz00_3579);
									}
								if ((BgL_xz00_3580 == BgL_yz00_3583))
									{	/* SawBbv/bbv-merge.scm 714 */
										goto BgL_zc3z04anonymousza32683ze3z87_3582;
									}
								else
									{	/* SawBbv/bbv-merge.scm 720 */
										obj_t BgL_val0_1788z00_3585;
										obj_t BgL_val1_1789z00_3586;

										BgL_val0_1788z00_3585 =
											bgl_list_ref(BgL_bsz00_268, BgL_xz00_3580);
										BgL_val1_1789z00_3586 =
											bgl_list_ref(BgL_bsz00_268, BgL_yz00_3583);
										{	/* SawBbv/bbv-merge.scm 720 */
											int BgL_tmpz00_9866;

											BgL_tmpz00_9866 = (int) (2L);
											BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9866);
										}
										{	/* SawBbv/bbv-merge.scm 720 */
											int BgL_tmpz00_9869;

											BgL_tmpz00_9869 = (int) (1L);
											BGL_MVALUES_VAL_SET(BgL_tmpz00_9869,
												BgL_val1_1789z00_3586);
										}
										return BgL_val0_1788z00_3585;
									}
							}
						}
					}
				}
			}
		}

	}



/* bbv-block-merge-select-strategy-first */
	obj_t
		BGl_bbvzd2blockzd2mergezd2selectzd2strategyzd2firstzd2zzsaw_bbvzd2mergezd2
		(obj_t BgL_bsz00_269)
	{
		{	/* SawBbv/bbv-merge.scm 727 */
			{	/* SawBbv/bbv-merge.scm 732 */
				obj_t BgL_val0_1790z00_3588;
				obj_t BgL_val1_1791z00_3589;

				BgL_val0_1790z00_3588 = CAR(BgL_bsz00_269);
				BgL_val1_1791z00_3589 = CAR(CDR(BgL_bsz00_269));
				{	/* SawBbv/bbv-merge.scm 732 */
					int BgL_tmpz00_9875;

					BgL_tmpz00_9875 = (int) (2L);
					BGL_MVALUES_NUMBER_SET(BgL_tmpz00_9875);
				}
				{	/* SawBbv/bbv-merge.scm 732 */
					int BgL_tmpz00_9878;

					BgL_tmpz00_9878 = (int) (1L);
					BGL_MVALUES_VAL_SET(BgL_tmpz00_9878, BgL_val1_1791z00_3589);
				}
				return BgL_val0_1790z00_3588;
			}
		}

	}



/* merge-ctx */
	BgL_bbvzd2ctxzd2_bglt
		BGl_mergezd2ctxzd2zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxzd2_bglt
		BgL_ctx1z00_270, BgL_bbvzd2ctxzd2_bglt BgL_ctx2z00_271)
	{
		{	/* SawBbv/bbv-merge.scm 743 */
			{	/* SawBbv/bbv-merge.scm 745 */
				BgL_bbvzd2ctxzd2_bglt BgL_new1267z00_3591;

				{	/* SawBbv/bbv-merge.scm 745 */
					BgL_bbvzd2ctxzd2_bglt BgL_new1266z00_3609;

					BgL_new1266z00_3609 =
						((BgL_bbvzd2ctxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2ctxzd2_bgl))));
					{	/* SawBbv/bbv-merge.scm 745 */
						long BgL_arg2693z00_3610;

						BgL_arg2693z00_3610 =
							BGL_CLASS_NUM(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1266z00_3609), BgL_arg2693z00_3610);
					}
					BgL_new1267z00_3591 = BgL_new1266z00_3609;
				}
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1267z00_3591))->BgL_idz00) =
					((long) -1L), BUNSPEC);
				{
					obj_t BgL_auxz00_9886;

					{	/* SawBbv/bbv-merge.scm 746 */
						obj_t BgL_l1792z00_3592;

						BgL_l1792z00_3592 =
							(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctx1z00_270))->
							BgL_entriesz00);
						if (NULLP(BgL_l1792z00_3592))
							{	/* SawBbv/bbv-merge.scm 746 */
								BgL_auxz00_9886 = BNIL;
							}
						else
							{	/* SawBbv/bbv-merge.scm 746 */
								obj_t BgL_head1794z00_3594;

								BgL_head1794z00_3594 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1792z00_3596;
									obj_t BgL_tail1795z00_3597;

									BgL_l1792z00_3596 = BgL_l1792z00_3592;
									BgL_tail1795z00_3597 = BgL_head1794z00_3594;
								BgL_zc3z04anonymousza32686ze3z87_3598:
									if (NULLP(BgL_l1792z00_3596))
										{	/* SawBbv/bbv-merge.scm 746 */
											BgL_auxz00_9886 = CDR(BgL_head1794z00_3594);
										}
									else
										{	/* SawBbv/bbv-merge.scm 746 */
											obj_t BgL_newtail1796z00_3600;

											{	/* SawBbv/bbv-merge.scm 746 */
												BgL_bbvzd2ctxentryzd2_bglt BgL_arg2689z00_3602;

												{	/* SawBbv/bbv-merge.scm 746 */
													obj_t BgL_ez00_3603;

													BgL_ez00_3603 = CAR(((obj_t) BgL_l1792z00_3596));
													{	/* SawBbv/bbv-merge.scm 748 */
														obj_t BgL_arg2690z00_3605;

														{	/* SawBbv/bbv-merge.scm 748 */
															BgL_rtl_regz00_bglt BgL_arg2691z00_3606;

															BgL_arg2691z00_3606 =
																(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
																		((BgL_bbvzd2ctxentryzd2_bglt)
																			BgL_ez00_3603)))->BgL_regz00);
															BgL_arg2690z00_3605 =
																BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2
																(BgL_ctx2z00_271, BgL_arg2691z00_3606);
														}
														BgL_arg2689z00_3602 =
															BGl_mergezd2ctxentryzd2zzsaw_bbvzd2mergezd2(
															((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_3603),
															((BgL_bbvzd2ctxentryzd2_bglt)
																BgL_arg2690z00_3605));
													}
												}
												BgL_newtail1796z00_3600 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg2689z00_3602), BNIL);
											}
											SET_CDR(BgL_tail1795z00_3597, BgL_newtail1796z00_3600);
											{	/* SawBbv/bbv-merge.scm 746 */
												obj_t BgL_arg2688z00_3601;

												BgL_arg2688z00_3601 = CDR(((obj_t) BgL_l1792z00_3596));
												{
													obj_t BgL_tail1795z00_9908;
													obj_t BgL_l1792z00_9907;

													BgL_l1792z00_9907 = BgL_arg2688z00_3601;
													BgL_tail1795z00_9908 = BgL_newtail1796z00_3600;
													BgL_tail1795z00_3597 = BgL_tail1795z00_9908;
													BgL_l1792z00_3596 = BgL_l1792z00_9907;
													goto BgL_zc3z04anonymousza32686ze3z87_3598;
												}
											}
										}
								}
							}
					}
					((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1267z00_3591))->
							BgL_entriesz00) = ((obj_t) BgL_auxz00_9886), BUNSPEC);
				}
				{	/* SawBbv/bbv-merge.scm 745 */
					obj_t BgL_fun2692z00_3608;

					BgL_fun2692z00_3608 =
						BGl_classzd2constructorzd2zz__objectz00
						(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
					BGL_PROCEDURE_CALL1(BgL_fun2692z00_3608,
						((obj_t) BgL_new1267z00_3591));
				}
				return BgL_new1267z00_3591;
			}
		}

	}



/* merge-ctxentry */
	BgL_bbvzd2ctxentryzd2_bglt
		BGl_mergezd2ctxentryzd2zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt
		BgL_e1z00_272, BgL_bbvzd2ctxentryzd2_bglt BgL_e2z00_273)
	{
		{	/* SawBbv/bbv-merge.scm 754 */
			{
				obj_t BgL_xz00_3698;
				obj_t BgL_yz00_3699;
				obj_t BgL_types1z00_3705;
				obj_t BgL_types2z00_3706;
				obj_t BgL_range1z00_3737;
				obj_t BgL_range2z00_3738;

				if (
					(BBOOL(
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_e1z00_272))->
								BgL_polarityz00)) ==
						BBOOL((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_e2z00_273))->
								BgL_polarityz00))))
					{	/* SawBbv/bbv-merge.scm 806 */
						if (
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_e1z00_272))->
								BgL_polarityz00))
							{	/* SawBbv/bbv-merge.scm 820 */
								bool_t BgL_test3701z00_9924;

								BgL_types1z00_3705 =
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_e1z00_272))->
									BgL_typesz00);
								BgL_types2z00_3706 =
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_e2z00_273))->
									BgL_typesz00);
								if ((bgl_list_length(BgL_types1z00_3705) ==
										bgl_list_length(BgL_types2z00_3706)))
									{
										obj_t BgL_l1797z00_3712;

										BgL_l1797z00_3712 = BgL_types1z00_3705;
									BgL_zc3z04anonymousza32761ze3z87_3713:
										if (NULLP(BgL_l1797z00_3712))
											{	/* SawBbv/bbv-merge.scm 763 */
												BgL_test3701z00_9924 = ((bool_t) 1);
											}
										else
											{	/* SawBbv/bbv-merge.scm 763 */
												bool_t BgL_test3704z00_9931;

												{	/* SawBbv/bbv-merge.scm 764 */
													obj_t BgL_tz00_3718;

													BgL_tz00_3718 = CAR(((obj_t) BgL_l1797z00_3712));
													{
														obj_t BgL_types2z00_3720;

														BgL_types2z00_3720 = BgL_types2z00_3706;
													BgL_zc3z04anonymousza32765ze3z87_3721:
														if (NULLP(BgL_types2z00_3720))
															{	/* SawBbv/bbv-merge.scm 766 */
																BgL_test3704z00_9931 = ((bool_t) 0);
															}
														else
															{	/* SawBbv/bbv-merge.scm 767 */
																bool_t BgL_test3706z00_9936;

																BgL_xz00_3698 = BgL_tz00_3718;
																BgL_yz00_3699 =
																	CAR(((obj_t) BgL_types2z00_3720));
																{	/* SawBbv/bbv-merge.scm 757 */
																	bool_t BgL__ortest_1269z00_3701;

																	BgL__ortest_1269z00_3701 =
																		(BgL_xz00_3698 == BgL_yz00_3699);
																	if (BgL__ortest_1269z00_3701)
																		{	/* SawBbv/bbv-merge.scm 757 */
																			BgL_test3706z00_9936 =
																				BgL__ortest_1269z00_3701;
																		}
																	else
																		{	/* SawBbv/bbv-merge.scm 758 */
																			bool_t BgL__ortest_1270z00_3702;

																			if (
																				(BgL_xz00_3698 ==
																					BGl_za2longza2z00zztype_cachez00))
																				{	/* SawBbv/bbv-merge.scm 758 */
																					BgL__ortest_1270z00_3702 =
																						(BgL_yz00_3699 ==
																						BGl_za2bintza2z00zztype_cachez00);
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 758 */
																					BgL__ortest_1270z00_3702 =
																						((bool_t) 0);
																				}
																			if (BgL__ortest_1270z00_3702)
																				{	/* SawBbv/bbv-merge.scm 758 */
																					BgL_test3706z00_9936 =
																						BgL__ortest_1270z00_3702;
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 758 */
																					if (
																						(BgL_xz00_3698 ==
																							BGl_za2bintza2z00zztype_cachez00))
																						{	/* SawBbv/bbv-merge.scm 759 */
																							BgL_test3706z00_9936 =
																								(BgL_yz00_3699 ==
																								BGl_za2longza2z00zztype_cachez00);
																						}
																					else
																						{	/* SawBbv/bbv-merge.scm 759 */
																							BgL_test3706z00_9936 =
																								((bool_t) 0);
																						}
																				}
																		}
																}
																if (BgL_test3706z00_9936)
																	{	/* SawBbv/bbv-merge.scm 767 */
																		BgL_test3704z00_9931 = ((bool_t) 1);
																	}
																else
																	{
																		obj_t BgL_types2z00_9948;

																		BgL_types2z00_9948 =
																			CDR(((obj_t) BgL_types2z00_3720));
																		BgL_types2z00_3720 = BgL_types2z00_9948;
																		goto BgL_zc3z04anonymousza32765ze3z87_3721;
																	}
															}
													}
												}
												if (BgL_test3704z00_9931)
													{
														obj_t BgL_l1797z00_9951;

														BgL_l1797z00_9951 =
															CDR(((obj_t) BgL_l1797z00_3712));
														BgL_l1797z00_3712 = BgL_l1797z00_9951;
														goto BgL_zc3z04anonymousza32761ze3z87_3713;
													}
												else
													{	/* SawBbv/bbv-merge.scm 763 */
														BgL_test3701z00_9924 = ((bool_t) 0);
													}
											}
									}
								else
									{	/* SawBbv/bbv-merge.scm 762 */
										BgL_test3701z00_9924 = ((bool_t) 0);
									}
								if (BgL_test3701z00_9924)
									{	/* SawBbv/bbv-merge.scm 831 */
										bool_t BgL_test3711z00_9956;

										{	/* SawBbv/bbv-merge.scm 831 */
											bool_t BgL_test3712z00_9957;

											{	/* SawBbv/bbv-merge.scm 831 */
												obj_t BgL_arg2727z00_3664;

												BgL_arg2727z00_3664 =
													(((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(BgL_e1z00_272))->BgL_valuez00);
												{	/* SawBbv/bbv-merge.scm 831 */
													obj_t BgL_classz00_5583;

													BgL_classz00_5583 =
														BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2;
													if (BGL_OBJECTP(BgL_arg2727z00_3664))
														{	/* SawBbv/bbv-merge.scm 831 */
															BgL_objectz00_bglt BgL_arg1807z00_5585;

															BgL_arg1807z00_5585 =
																(BgL_objectz00_bglt) (BgL_arg2727z00_3664);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* SawBbv/bbv-merge.scm 831 */
																	long BgL_idxz00_5591;

																	BgL_idxz00_5591 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_5585);
																	BgL_test3712z00_9957 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_5591 + 1L)) ==
																		BgL_classz00_5583);
																}
															else
																{	/* SawBbv/bbv-merge.scm 831 */
																	bool_t BgL_res3269z00_5616;

																	{	/* SawBbv/bbv-merge.scm 831 */
																		obj_t BgL_oclassz00_5599;

																		{	/* SawBbv/bbv-merge.scm 831 */
																			obj_t BgL_arg1815z00_5607;
																			long BgL_arg1816z00_5608;

																			BgL_arg1815z00_5607 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* SawBbv/bbv-merge.scm 831 */
																				long BgL_arg1817z00_5609;

																				BgL_arg1817z00_5609 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_5585);
																				BgL_arg1816z00_5608 =
																					(BgL_arg1817z00_5609 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_5599 =
																				VECTOR_REF(BgL_arg1815z00_5607,
																				BgL_arg1816z00_5608);
																		}
																		{	/* SawBbv/bbv-merge.scm 831 */
																			bool_t BgL__ortest_1115z00_5600;

																			BgL__ortest_1115z00_5600 =
																				(BgL_classz00_5583 ==
																				BgL_oclassz00_5599);
																			if (BgL__ortest_1115z00_5600)
																				{	/* SawBbv/bbv-merge.scm 831 */
																					BgL_res3269z00_5616 =
																						BgL__ortest_1115z00_5600;
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 831 */
																					long BgL_odepthz00_5601;

																					{	/* SawBbv/bbv-merge.scm 831 */
																						obj_t BgL_arg1804z00_5602;

																						BgL_arg1804z00_5602 =
																							(BgL_oclassz00_5599);
																						BgL_odepthz00_5601 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_5602);
																					}
																					if ((1L < BgL_odepthz00_5601))
																						{	/* SawBbv/bbv-merge.scm 831 */
																							obj_t BgL_arg1802z00_5604;

																							{	/* SawBbv/bbv-merge.scm 831 */
																								obj_t BgL_arg1803z00_5605;

																								BgL_arg1803z00_5605 =
																									(BgL_oclassz00_5599);
																								BgL_arg1802z00_5604 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_5605, 1L);
																							}
																							BgL_res3269z00_5616 =
																								(BgL_arg1802z00_5604 ==
																								BgL_classz00_5583);
																						}
																					else
																						{	/* SawBbv/bbv-merge.scm 831 */
																							BgL_res3269z00_5616 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test3712z00_9957 = BgL_res3269z00_5616;
																}
														}
													else
														{	/* SawBbv/bbv-merge.scm 831 */
															BgL_test3712z00_9957 = ((bool_t) 0);
														}
												}
											}
											if (BgL_test3712z00_9957)
												{	/* SawBbv/bbv-merge.scm 831 */
													bool_t BgL_test3717z00_9981;

													{	/* SawBbv/bbv-merge.scm 831 */
														obj_t BgL_arg2726z00_3663;

														BgL_arg2726z00_3663 =
															(((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_e2z00_273))->BgL_valuez00);
														{	/* SawBbv/bbv-merge.scm 831 */
															obj_t BgL_classz00_5617;

															BgL_classz00_5617 =
																BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2;
															if (BGL_OBJECTP(BgL_arg2726z00_3663))
																{	/* SawBbv/bbv-merge.scm 831 */
																	BgL_objectz00_bglt BgL_arg1807z00_5619;

																	BgL_arg1807z00_5619 =
																		(BgL_objectz00_bglt) (BgL_arg2726z00_3663);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* SawBbv/bbv-merge.scm 831 */
																			long BgL_idxz00_5625;

																			BgL_idxz00_5625 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_5619);
																			BgL_test3717z00_9981 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_5625 + 1L)) ==
																				BgL_classz00_5617);
																		}
																	else
																		{	/* SawBbv/bbv-merge.scm 831 */
																			bool_t BgL_res3270z00_5650;

																			{	/* SawBbv/bbv-merge.scm 831 */
																				obj_t BgL_oclassz00_5633;

																				{	/* SawBbv/bbv-merge.scm 831 */
																					obj_t BgL_arg1815z00_5641;
																					long BgL_arg1816z00_5642;

																					BgL_arg1815z00_5641 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* SawBbv/bbv-merge.scm 831 */
																						long BgL_arg1817z00_5643;

																						BgL_arg1817z00_5643 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_5619);
																						BgL_arg1816z00_5642 =
																							(BgL_arg1817z00_5643 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_5633 =
																						VECTOR_REF(BgL_arg1815z00_5641,
																						BgL_arg1816z00_5642);
																				}
																				{	/* SawBbv/bbv-merge.scm 831 */
																					bool_t BgL__ortest_1115z00_5634;

																					BgL__ortest_1115z00_5634 =
																						(BgL_classz00_5617 ==
																						BgL_oclassz00_5633);
																					if (BgL__ortest_1115z00_5634)
																						{	/* SawBbv/bbv-merge.scm 831 */
																							BgL_res3270z00_5650 =
																								BgL__ortest_1115z00_5634;
																						}
																					else
																						{	/* SawBbv/bbv-merge.scm 831 */
																							long BgL_odepthz00_5635;

																							{	/* SawBbv/bbv-merge.scm 831 */
																								obj_t BgL_arg1804z00_5636;

																								BgL_arg1804z00_5636 =
																									(BgL_oclassz00_5633);
																								BgL_odepthz00_5635 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_5636);
																							}
																							if ((1L < BgL_odepthz00_5635))
																								{	/* SawBbv/bbv-merge.scm 831 */
																									obj_t BgL_arg1802z00_5638;

																									{	/* SawBbv/bbv-merge.scm 831 */
																										obj_t BgL_arg1803z00_5639;

																										BgL_arg1803z00_5639 =
																											(BgL_oclassz00_5633);
																										BgL_arg1802z00_5638 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_5639, 1L);
																									}
																									BgL_res3270z00_5650 =
																										(BgL_arg1802z00_5638 ==
																										BgL_classz00_5617);
																								}
																							else
																								{	/* SawBbv/bbv-merge.scm 831 */
																									BgL_res3270z00_5650 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test3717z00_9981 =
																				BgL_res3270z00_5650;
																		}
																}
															else
																{	/* SawBbv/bbv-merge.scm 831 */
																	BgL_test3717z00_9981 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test3717z00_9981)
														{	/* SawBbv/bbv-merge.scm 831 */
															BgL_test3711z00_9956 = ((bool_t) 0);
														}
													else
														{	/* SawBbv/bbv-merge.scm 831 */
															BgL_test3711z00_9956 = ((bool_t) 1);
														}
												}
											else
												{	/* SawBbv/bbv-merge.scm 831 */
													BgL_test3711z00_9956 = ((bool_t) 1);
												}
										}
										if (BgL_test3711z00_9956)
											{	/* SawBbv/bbv-merge.scm 834 */
												BgL_bbvzd2ctxentryzd2_bglt BgL_new1289z00_3634;

												{	/* SawBbv/bbv-merge.scm 834 */
													BgL_bbvzd2ctxentryzd2_bglt BgL_new1292z00_3643;

													BgL_new1292z00_3643 =
														((BgL_bbvzd2ctxentryzd2_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_bbvzd2ctxentryzd2_bgl))));
													{	/* SawBbv/bbv-merge.scm 834 */
														long BgL_arg2715z00_3644;

														BgL_arg2715z00_3644 =
															BGL_CLASS_NUM
															(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
														BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																BgL_new1292z00_3643), BgL_arg2715z00_3644);
													}
													BgL_new1289z00_3634 = BgL_new1292z00_3643;
												}
												((((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(BgL_new1289z00_3634))->BgL_regz00) =
													((BgL_rtl_regz00_bglt) (((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_e1z00_272))->BgL_regz00)), BUNSPEC);
												((((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(BgL_new1289z00_3634))->BgL_typesz00) =
													((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_e1z00_272))->BgL_typesz00)),
													BUNSPEC);
												((((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(BgL_new1289z00_3634))->BgL_polarityz00) =
													((bool_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_e1z00_272))->BgL_polarityz00)),
													BUNSPEC);
												{
													long BgL_auxz00_10015;

													{	/* SawBbv/bbv-merge.scm 836 */
														long BgL_az00_3635;
														long BgL_bz00_3636;

														BgL_az00_3635 =
															(((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_e1z00_272))->BgL_countz00);
														BgL_bz00_3636 =
															(((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_e2z00_273))->BgL_countz00);
														if ((BgL_az00_3635 < BgL_bz00_3636))
															{	/* SawBbv/bbv-merge.scm 836 */
																BgL_auxz00_10015 = BgL_az00_3635;
															}
														else
															{	/* SawBbv/bbv-merge.scm 836 */
																BgL_auxz00_10015 = BgL_bz00_3636;
															}
													}
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1289z00_3634))->BgL_countz00) =
														((long) BgL_auxz00_10015), BUNSPEC);
												}
												((((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(BgL_new1289z00_3634))->BgL_valuez00) =
													((obj_t) CNST_TABLE_REF(13)), BUNSPEC);
												{
													obj_t BgL_auxz00_10023;

													{	/* SawBbv/bbv-merge.scm 835 */
														bool_t BgL_test3723z00_10024;

														{	/* SawBbv/bbv-merge.scm 835 */
															obj_t BgL_arg2712z00_3641;
															obj_t BgL_arg2714z00_3642;

															BgL_arg2712z00_3641 =
																(((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_e1z00_272))->BgL_aliasesz00);
															BgL_arg2714z00_3642 =
																(((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_e2z00_273))->BgL_aliasesz00);
															BgL_test3723z00_10024 =
																CBOOL
																(BGl_listzd2eqzf3ze70zc6zzsaw_bbvzd2mergezd2
																(BgL_arg2712z00_3641, BgL_arg2714z00_3642));
														}
														if (BgL_test3723z00_10024)
															{	/* SawBbv/bbv-merge.scm 835 */
																BgL_auxz00_10023 =
																	(((BgL_bbvzd2ctxentryzd2_bglt)
																		COBJECT(BgL_e1z00_272))->BgL_aliasesz00);
															}
														else
															{	/* SawBbv/bbv-merge.scm 835 */
																BgL_auxz00_10023 = BNIL;
															}
													}
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1289z00_3634))->BgL_aliasesz00) =
														((obj_t) BgL_auxz00_10023), BUNSPEC);
												}
												((((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(BgL_new1289z00_3634))->BgL_initvalz00) =
													((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_e1z00_272))->BgL_initvalz00)),
													BUNSPEC);
												return BgL_new1289z00_3634;
											}
										else
											{	/* SawBbv/bbv-merge.scm 839 */
												obj_t BgL_rangez00_3645;

												BgL_range1z00_3737 =
													(((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(BgL_e1z00_272))->BgL_valuez00);
												BgL_range2z00_3738 =
													(((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(BgL_e2z00_273))->BgL_valuez00);
												{	/* SawBbv/bbv-merge.scm 782 */
													obj_t BgL_arg2780z00_3742;
													obj_t BgL_arg2781z00_3743;
													obj_t BgL_arg2783z00_3744;

													{	/* SawBbv/bbv-merge.scm 782 */
														obj_t BgL_arg2784z00_3745;
														obj_t BgL_arg2786z00_3746;
														long BgL_arg2787z00_3747;

														BgL_arg2784z00_3745 =
															(((BgL_bbvzd2rangezd2_bglt) COBJECT(
																	((BgL_bbvzd2rangezd2_bglt)
																		BgL_range1z00_3737)))->BgL_loz00);
														BgL_arg2786z00_3746 =
															(((BgL_bbvzd2rangezd2_bglt)
																COBJECT(((BgL_bbvzd2rangezd2_bglt)
																		BgL_range2z00_3738)))->BgL_loz00);
														{	/* SawBbv/bbv-merge.scm 782 */
															long BgL_arg3066z00_5562;

															{	/* SawBbv/bbv-merge.scm 782 */
																long BgL_arg3067z00_5563;

																{	/* SawBbv/bbv-merge.scm 782 */
																	long BgL_arg3070z00_5564;

																	{	/* SawBbv/bbv-merge.scm 782 */
																		obj_t BgL_arg3071z00_5565;

																		BgL_arg3071z00_5565 =
																			BGl_bigloozd2configzd2zz__configurez00
																			(CNST_TABLE_REF(12));
																		BgL_arg3070z00_5564 =
																			((long) CINT(BgL_arg3071z00_5565) - 2L);
																	}
																	BgL_arg3067z00_5563 =
																		(1L << (int) (BgL_arg3070z00_5564));
																}
																BgL_arg3066z00_5562 = NEG(BgL_arg3067z00_5563);
															}
															BgL_arg2787z00_3747 = (BgL_arg3066z00_5562 - 1L);
														}
														BgL_arg2780z00_3742 =
															BGl_minrvz00zzsaw_bbvzd2rangezd2
															(BgL_arg2784z00_3745, BgL_arg2786z00_3746,
															BINT(BgL_arg2787z00_3747));
													}
													{	/* SawBbv/bbv-merge.scm 783 */
														obj_t BgL_arg2789z00_3748;
														obj_t BgL_arg2793z00_3749;
														long BgL_arg2794z00_3750;

														BgL_arg2789z00_3748 =
															(((BgL_bbvzd2rangezd2_bglt) COBJECT(
																	((BgL_bbvzd2rangezd2_bglt)
																		BgL_range1z00_3737)))->BgL_upz00);
														BgL_arg2793z00_3749 =
															(((BgL_bbvzd2rangezd2_bglt)
																COBJECT(((BgL_bbvzd2rangezd2_bglt)
																		BgL_range2z00_3738)))->BgL_upz00);
														{	/* SawBbv/bbv-merge.scm 783 */
															long BgL_arg3070z00_5570;

															{	/* SawBbv/bbv-merge.scm 783 */
																obj_t BgL_arg3071z00_5571;

																BgL_arg3071z00_5571 =
																	BGl_bigloozd2configzd2zz__configurez00
																	(CNST_TABLE_REF(12));
																BgL_arg3070z00_5570 =
																	((long) CINT(BgL_arg3071z00_5571) - 2L);
															}
															BgL_arg2794z00_3750 =
																(1L << (int) (BgL_arg3070z00_5570));
														}
														BgL_arg2781z00_3743 =
															BGl_maxrvz00zzsaw_bbvzd2rangezd2
															(BgL_arg2789z00_3748, BgL_arg2793z00_3749,
															BINT(BgL_arg2794z00_3750));
													}
													{	/* SawBbv/bbv-merge.scm 784 */
														obj_t BgL_list2795z00_3751;

														{	/* SawBbv/bbv-merge.scm 784 */
															obj_t BgL_arg2799z00_3752;

															BgL_arg2799z00_3752 =
																MAKE_YOUNG_PAIR(BgL_range2z00_3738, BNIL);
															BgL_list2795z00_3751 =
																MAKE_YOUNG_PAIR(BgL_range1z00_3737,
																BgL_arg2799z00_3752);
														}
														BgL_arg2783z00_3744 = BgL_list2795z00_3751;
													}
													BgL_rangez00_3645 =
														BGl_rangezd2wideningzd2zzsaw_bbvzd2mergezd2
														(BgL_arg2780z00_3742, BgL_arg2781z00_3743,
														BgL_arg2783z00_3744);
												}
												{	/* SawBbv/bbv-merge.scm 842 */
													BgL_bbvzd2ctxentryzd2_bglt BgL_new1293z00_3647;

													{	/* SawBbv/bbv-merge.scm 842 */
														BgL_bbvzd2ctxentryzd2_bglt BgL_new1296z00_3657;

														BgL_new1296z00_3657 =
															((BgL_bbvzd2ctxentryzd2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_bbvzd2ctxentryzd2_bgl))));
														{	/* SawBbv/bbv-merge.scm 842 */
															long BgL_arg2723z00_3658;

															BgL_arg2723z00_3658 =
																BGL_CLASS_NUM
																(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																	BgL_new1296z00_3657), BgL_arg2723z00_3658);
														}
														BgL_new1293z00_3647 = BgL_new1296z00_3657;
													}
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1293z00_3647))->BgL_regz00) =
														((BgL_rtl_regz00_bglt) ((
																	(BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_e1z00_272))->BgL_regz00)),
														BUNSPEC);
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1293z00_3647))->BgL_typesz00) =
														((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_e1z00_272))->BgL_typesz00)),
														BUNSPEC);
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1293z00_3647))->
															BgL_polarityz00) =
														((bool_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_e1z00_272))->BgL_polarityz00)),
														BUNSPEC);
													{
														long BgL_auxz00_10074;

														{	/* SawBbv/bbv-merge.scm 844 */
															long BgL_az00_3648;
															long BgL_bz00_3649;

															BgL_az00_3648 =
																(((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_e1z00_272))->BgL_countz00);
															BgL_bz00_3649 =
																(((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_e2z00_273))->BgL_countz00);
															if ((BgL_az00_3648 < BgL_bz00_3649))
																{	/* SawBbv/bbv-merge.scm 844 */
																	BgL_auxz00_10074 = BgL_az00_3648;
																}
															else
																{	/* SawBbv/bbv-merge.scm 844 */
																	BgL_auxz00_10074 = BgL_bz00_3649;
																}
														}
														((((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_new1293z00_3647))->BgL_countz00) =
															((long) BgL_auxz00_10074), BUNSPEC);
													}
													{
														obj_t BgL_auxz00_10080;

														if (CBOOL(BgL_rangez00_3645))
															{	/* SawBbv/bbv-merge.scm 845 */
																BgL_auxz00_10080 = BgL_rangez00_3645;
															}
														else
															{	/* SawBbv/bbv-merge.scm 845 */
																BgL_auxz00_10080 =
																	((obj_t)
																	BGl_fixnumzd2rangezd2zzsaw_bbvzd2rangezd2());
															}
														((((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_new1293z00_3647))->BgL_valuez00) =
															((obj_t) BgL_auxz00_10080), BUNSPEC);
													}
													{
														obj_t BgL_auxz00_10086;

														{	/* SawBbv/bbv-merge.scm 843 */
															bool_t BgL_test3726z00_10087;

															{	/* SawBbv/bbv-merge.scm 843 */
																obj_t BgL_arg2721z00_3655;
																obj_t BgL_arg2722z00_3656;

																BgL_arg2721z00_3655 =
																	(((BgL_bbvzd2ctxentryzd2_bglt)
																		COBJECT(BgL_e1z00_272))->BgL_aliasesz00);
																BgL_arg2722z00_3656 =
																	(((BgL_bbvzd2ctxentryzd2_bglt)
																		COBJECT(BgL_e2z00_273))->BgL_aliasesz00);
																BgL_test3726z00_10087 =
																	CBOOL
																	(BGl_listzd2eqzf3ze70zc6zzsaw_bbvzd2mergezd2
																	(BgL_arg2721z00_3655, BgL_arg2722z00_3656));
															}
															if (BgL_test3726z00_10087)
																{	/* SawBbv/bbv-merge.scm 843 */
																	BgL_auxz00_10086 =
																		(((BgL_bbvzd2ctxentryzd2_bglt)
																			COBJECT(BgL_e1z00_272))->BgL_aliasesz00);
																}
															else
																{	/* SawBbv/bbv-merge.scm 843 */
																	BgL_auxz00_10086 = BNIL;
																}
														}
														((((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_new1293z00_3647))->
																BgL_aliasesz00) =
															((obj_t) BgL_auxz00_10086), BUNSPEC);
													}
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1293z00_3647))->BgL_initvalz00) =
														((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_e1z00_272))->BgL_initvalz00)),
														BUNSPEC);
													return BgL_new1293z00_3647;
												}
											}
									}
								else
									{	/* SawBbv/bbv-merge.scm 823 */
										obj_t BgL_tsz00_3665;

										BgL_tsz00_3665 =
											BGl_typeszd2intersectionze70z35zzsaw_bbvzd2mergezd2(
											(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_e1z00_272))->
												BgL_typesz00),
											(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_e2z00_273))->
												BgL_typesz00));
										if (NULLP(BgL_tsz00_3665))
											{	/* SawBbv/bbv-merge.scm 824 */
											BgL_zc3z04anonymousza32775ze3z87_3731:
												{	/* SawBbv/bbv-merge.scm 772 */
													BgL_bbvzd2ctxentryzd2_bglt BgL_new1273z00_3733;

													{	/* SawBbv/bbv-merge.scm 772 */
														BgL_bbvzd2ctxentryzd2_bglt BgL_new1276z00_3735;

														BgL_new1276z00_3735 =
															((BgL_bbvzd2ctxentryzd2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_bbvzd2ctxentryzd2_bgl))));
														{	/* SawBbv/bbv-merge.scm 772 */
															long BgL_arg2777z00_3736;

															BgL_arg2777z00_3736 =
																BGL_CLASS_NUM
																(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																	BgL_new1276z00_3735), BgL_arg2777z00_3736);
														}
														BgL_new1273z00_3733 = BgL_new1276z00_3735;
													}
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1273z00_3733))->BgL_regz00) =
														((BgL_rtl_regz00_bglt) ((
																	(BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_e1z00_272))->BgL_regz00)),
														BUNSPEC);
													{
														obj_t BgL_auxz00_10107;

														{	/* SawBbv/bbv-merge.scm 773 */
															obj_t BgL_list2776z00_3734;

															BgL_list2776z00_3734 =
																MAKE_YOUNG_PAIR(BGl_za2objza2z00zztype_cachez00,
																BNIL);
															BgL_auxz00_10107 = BgL_list2776z00_3734;
														}
														((((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_new1273z00_3733))->BgL_typesz00) =
															((obj_t) BgL_auxz00_10107), BUNSPEC);
													}
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1273z00_3733))->
															BgL_polarityz00) =
														((bool_t) ((bool_t) 1)), BUNSPEC);
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1273z00_3733))->BgL_countz00) =
														((long) (((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_e1z00_272))->BgL_countz00)),
														BUNSPEC);
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1273z00_3733))->BgL_valuez00) =
														((obj_t) CNST_TABLE_REF(13)), BUNSPEC);
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1273z00_3733))->BgL_aliasesz00) =
														((obj_t) BNIL), BUNSPEC);
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1273z00_3733))->BgL_initvalz00) =
														((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_e1z00_272))->BgL_initvalz00)),
														BUNSPEC);
													return BgL_new1273z00_3733;
												}
											}
										else
											{	/* SawBbv/bbv-merge.scm 826 */
												BgL_bbvzd2ctxentryzd2_bglt BgL_new1285z00_3668;

												{	/* SawBbv/bbv-merge.scm 826 */
													BgL_bbvzd2ctxentryzd2_bglt BgL_new1288z00_3677;

													BgL_new1288z00_3677 =
														((BgL_bbvzd2ctxentryzd2_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_bbvzd2ctxentryzd2_bgl))));
													{	/* SawBbv/bbv-merge.scm 826 */
														long BgL_arg2738z00_3678;

														BgL_arg2738z00_3678 =
															BGL_CLASS_NUM
															(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
														BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																BgL_new1288z00_3677), BgL_arg2738z00_3678);
													}
													BgL_new1285z00_3668 = BgL_new1288z00_3677;
												}
												((((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(BgL_new1285z00_3668))->BgL_regz00) =
													((BgL_rtl_regz00_bglt) (((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_e1z00_272))->BgL_regz00)), BUNSPEC);
												((((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(BgL_new1285z00_3668))->BgL_typesz00) =
													((obj_t) BgL_tsz00_3665), BUNSPEC);
												((((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(BgL_new1285z00_3668))->BgL_polarityz00) =
													((bool_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_e1z00_272))->BgL_polarityz00)),
													BUNSPEC);
												{
													long BgL_auxz00_10127;

													{	/* SawBbv/bbv-merge.scm 829 */
														long BgL_az00_3669;
														long BgL_bz00_3670;

														BgL_az00_3669 =
															(((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_e1z00_272))->BgL_countz00);
														BgL_bz00_3670 =
															(((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_e2z00_273))->BgL_countz00);
														if ((BgL_az00_3669 < BgL_bz00_3670))
															{	/* SawBbv/bbv-merge.scm 829 */
																BgL_auxz00_10127 = BgL_az00_3669;
															}
														else
															{	/* SawBbv/bbv-merge.scm 829 */
																BgL_auxz00_10127 = BgL_bz00_3670;
															}
													}
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1285z00_3668))->BgL_countz00) =
														((long) BgL_auxz00_10127), BUNSPEC);
												}
												((((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(BgL_new1285z00_3668))->BgL_valuez00) =
													((obj_t) CNST_TABLE_REF(13)), BUNSPEC);
												{
													obj_t BgL_auxz00_10135;

													{	/* SawBbv/bbv-merge.scm 827 */
														bool_t BgL_test3729z00_10136;

														{	/* SawBbv/bbv-merge.scm 827 */
															obj_t BgL_arg2736z00_3675;
															obj_t BgL_arg2737z00_3676;

															BgL_arg2736z00_3675 =
																(((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_e1z00_272))->BgL_aliasesz00);
															BgL_arg2737z00_3676 =
																(((BgL_bbvzd2ctxentryzd2_bglt)
																	COBJECT(BgL_e2z00_273))->BgL_aliasesz00);
															BgL_test3729z00_10136 =
																CBOOL
																(BGl_listzd2eqzf3ze70zc6zzsaw_bbvzd2mergezd2
																(BgL_arg2736z00_3675, BgL_arg2737z00_3676));
														}
														if (BgL_test3729z00_10136)
															{	/* SawBbv/bbv-merge.scm 827 */
																BgL_auxz00_10135 =
																	(((BgL_bbvzd2ctxentryzd2_bglt)
																		COBJECT(BgL_e1z00_272))->BgL_aliasesz00);
															}
														else
															{	/* SawBbv/bbv-merge.scm 827 */
																BgL_auxz00_10135 = BNIL;
															}
													}
													((((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_new1285z00_3668))->BgL_aliasesz00) =
														((obj_t) BgL_auxz00_10135), BUNSPEC);
												}
												((((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(BgL_new1285z00_3668))->BgL_initvalz00) =
													((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_e1z00_272))->BgL_initvalz00)),
													BUNSPEC);
												return BgL_new1285z00_3668;
											}
									}
							}
						else
							{	/* SawBbv/bbv-merge.scm 811 */
								obj_t BgL_tsz00_3683;

								BgL_tsz00_3683 =
									BGl_typeszd2intersectionze70z35zzsaw_bbvzd2mergezd2(
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_e1z00_272))->
										BgL_typesz00),
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_e2z00_273))->
										BgL_typesz00));
								if (NULLP(BgL_tsz00_3683))
									{	/* SawBbv/bbv-merge.scm 813 */
										goto BgL_zc3z04anonymousza32775ze3z87_3731;
									}
								else
									{	/* SawBbv/bbv-merge.scm 815 */
										BgL_bbvzd2ctxentryzd2_bglt BgL_new1281z00_3686;

										{	/* SawBbv/bbv-merge.scm 815 */
											BgL_bbvzd2ctxentryzd2_bglt BgL_new1284z00_3692;

											BgL_new1284z00_3692 =
												((BgL_bbvzd2ctxentryzd2_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_bbvzd2ctxentryzd2_bgl))));
											{	/* SawBbv/bbv-merge.scm 815 */
												long BgL_arg2750z00_3693;

												BgL_arg2750z00_3693 =
													BGL_CLASS_NUM
													(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
												BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
														BgL_new1284z00_3692), BgL_arg2750z00_3693);
											}
											BgL_new1281z00_3686 = BgL_new1284z00_3692;
										}
										((((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(BgL_new1281z00_3686))->BgL_regz00) =
											((BgL_rtl_regz00_bglt) (((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(BgL_e1z00_272))->BgL_regz00)), BUNSPEC);
										((((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(BgL_new1281z00_3686))->BgL_typesz00) =
											((obj_t) BgL_tsz00_3683), BUNSPEC);
										((((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(BgL_new1281z00_3686))->BgL_polarityz00) =
											((bool_t) (((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(BgL_e1z00_272))->BgL_polarityz00)),
											BUNSPEC);
										((((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(BgL_new1281z00_3686))->BgL_countz00) =
											((long) 1L), BUNSPEC);
										((((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(BgL_new1281z00_3686))->BgL_valuez00) =
											((obj_t) CNST_TABLE_REF(13)), BUNSPEC);
										{
											obj_t BgL_auxz00_10162;

											{	/* SawBbv/bbv-merge.scm 816 */
												bool_t BgL_test3731z00_10163;

												{	/* SawBbv/bbv-merge.scm 816 */
													obj_t BgL_arg2748z00_3690;
													obj_t BgL_arg2749z00_3691;

													BgL_arg2748z00_3690 =
														(((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(BgL_e1z00_272))->BgL_aliasesz00);
													BgL_arg2749z00_3691 =
														(((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(BgL_e2z00_273))->BgL_aliasesz00);
													BgL_test3731z00_10163 =
														CBOOL(BGl_listzd2eqzf3ze70zc6zzsaw_bbvzd2mergezd2
														(BgL_arg2748z00_3690, BgL_arg2749z00_3691));
												}
												if (BgL_test3731z00_10163)
													{	/* SawBbv/bbv-merge.scm 816 */
														BgL_auxz00_10162 =
															(((BgL_bbvzd2ctxentryzd2_bglt)
																COBJECT(BgL_e1z00_272))->BgL_aliasesz00);
													}
												else
													{	/* SawBbv/bbv-merge.scm 816 */
														BgL_auxz00_10162 = BNIL;
													}
											}
											((((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(BgL_new1281z00_3686))->BgL_aliasesz00) =
												((obj_t) BgL_auxz00_10162), BUNSPEC);
										}
										((((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(BgL_new1281z00_3686))->BgL_initvalz00) =
											((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(BgL_e1z00_272))->BgL_initvalz00)), BUNSPEC);
										return BgL_new1281z00_3686;
									}
							}
					}
				else
					{	/* SawBbv/bbv-merge.scm 806 */
						goto BgL_zc3z04anonymousza32775ze3z87_3731;
					}
			}
		}

	}



/* types-intersection~0 */
	obj_t BGl_typeszd2intersectionze70z35zzsaw_bbvzd2mergezd2(obj_t
		BgL_ts1z00_3753, obj_t BgL_ts2z00_3754)
	{
		{	/* SawBbv/bbv-merge.scm 788 */
			{	/* SawBbv/bbv-merge.scm 788 */
				obj_t BgL_hook1804z00_3756;

				BgL_hook1804z00_3756 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
				{
					obj_t BgL_l1801z00_3758;
					obj_t BgL_h1802z00_3759;

					BgL_l1801z00_3758 = BgL_ts1z00_3753;
					BgL_h1802z00_3759 = BgL_hook1804z00_3756;
				BgL_zc3z04anonymousza32801ze3z87_3760:
					if (NULLP(BgL_l1801z00_3758))
						{	/* SawBbv/bbv-merge.scm 788 */
							return CDR(BgL_hook1804z00_3756);
						}
					else
						{	/* SawBbv/bbv-merge.scm 788 */
							if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(CAR(
											((obj_t) BgL_l1801z00_3758)), BgL_ts2z00_3754)))
								{	/* SawBbv/bbv-merge.scm 788 */
									obj_t BgL_nh1803z00_3764;

									{	/* SawBbv/bbv-merge.scm 788 */
										obj_t BgL_arg2805z00_3766;

										BgL_arg2805z00_3766 = CAR(((obj_t) BgL_l1801z00_3758));
										BgL_nh1803z00_3764 =
											MAKE_YOUNG_PAIR(BgL_arg2805z00_3766, BNIL);
									}
									SET_CDR(BgL_h1802z00_3759, BgL_nh1803z00_3764);
									{	/* SawBbv/bbv-merge.scm 788 */
										obj_t BgL_arg2804z00_3765;

										BgL_arg2804z00_3765 = CDR(((obj_t) BgL_l1801z00_3758));
										{
											obj_t BgL_h1802z00_10188;
											obj_t BgL_l1801z00_10187;

											BgL_l1801z00_10187 = BgL_arg2804z00_3765;
											BgL_h1802z00_10188 = BgL_nh1803z00_3764;
											BgL_h1802z00_3759 = BgL_h1802z00_10188;
											BgL_l1801z00_3758 = BgL_l1801z00_10187;
											goto BgL_zc3z04anonymousza32801ze3z87_3760;
										}
									}
								}
							else
								{	/* SawBbv/bbv-merge.scm 788 */
									obj_t BgL_arg2808z00_3767;

									BgL_arg2808z00_3767 = CDR(((obj_t) BgL_l1801z00_3758));
									{
										obj_t BgL_l1801z00_10191;

										BgL_l1801z00_10191 = BgL_arg2808z00_3767;
										BgL_l1801z00_3758 = BgL_l1801z00_10191;
										goto BgL_zc3z04anonymousza32801ze3z87_3760;
									}
								}
						}
				}
			}
		}

	}



/* list-eq?~0 */
	obj_t BGl_listzd2eqzf3ze70zc6zzsaw_bbvzd2mergezd2(obj_t BgL_l1z00_3770,
		obj_t BgL_l2z00_3771)
	{
		{	/* SawBbv/bbv-merge.scm 792 */
			if ((bgl_list_length(BgL_l1z00_3770) == bgl_list_length(BgL_l2z00_3771)))
				{	/* SawBbv/bbv-merge.scm 792 */
					obj_t BgL_list2814z00_3776;

					{	/* SawBbv/bbv-merge.scm 792 */
						obj_t BgL_arg2815z00_3777;

						BgL_arg2815z00_3777 = MAKE_YOUNG_PAIR(BgL_l2z00_3771, BNIL);
						BgL_list2814z00_3776 =
							MAKE_YOUNG_PAIR(BgL_l1z00_3770, BgL_arg2815z00_3777);
					}
					return
						BGl_everyz00zz__r4_pairs_and_lists_6_3z00
						(BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00, BgL_list2814z00_3776);
				}
			else
				{	/* SawBbv/bbv-merge.scm 791 */
					return BFALSE;
				}
		}

	}



/* range-widening */
	obj_t BGl_rangezd2wideningzd2zzsaw_bbvzd2mergezd2(obj_t BgL_lz00_274,
		obj_t BgL_uz00_275, obj_t BgL_rangesz00_276)
	{
		{	/* SawBbv/bbv-merge.scm 850 */
			{	/* SawBbv/bbv-merge.scm 850 */
				bool_t BgL_wideningz00_3786;

				BgL_wideningz00_3786 = ((bool_t) 0);
				{
					obj_t BgL_vz00_3945;
					obj_t BgL_vz00_3921;
					obj_t BgL_vz00_3912;
					obj_t BgL_vz00_3903;

					{	/* SawBbv/bbv-merge.scm 896 */
						obj_t BgL_nlz00_3792;
						obj_t BgL_nuz00_3793;

						{	/* SawBbv/bbv-merge.scm 896 */
							bool_t BgL_test3735z00_10199;

							{
								obj_t BgL_l1811z00_3867;

								BgL_l1811z00_3867 = BgL_rangesz00_276;
							BgL_zc3z04anonymousza32880ze3z87_3868:
								if (NULLP(BgL_l1811z00_3867))
									{	/* SawBbv/bbv-merge.scm 896 */
										BgL_test3735z00_10199 = ((bool_t) 1);
									}
								else
									{	/* SawBbv/bbv-merge.scm 896 */
										bool_t BgL_test3737z00_10202;

										{	/* SawBbv/bbv-merge.scm 897 */
											obj_t BgL_rz00_3873;

											BgL_rz00_3873 = CAR(((obj_t) BgL_l1811z00_3867));
											{	/* SawBbv/bbv-merge.scm 898 */
												obj_t BgL_arg2882z00_3875;

												{	/* SawBbv/bbv-merge.scm 898 */
													obj_t BgL_arg2883z00_3876;

													BgL_arg2883z00_3876 =
														(((BgL_bbvzd2rangezd2_bglt) COBJECT(
																((BgL_bbvzd2rangezd2_bglt) BgL_rz00_3873)))->
														BgL_loz00);
													BgL_arg2882z00_3875 =
														BGl_zd3rvzd3zzsaw_bbvzd2rangezd2(BgL_lz00_274,
														BgL_arg2883z00_3876);
												}
												BgL_test3737z00_10202 = (BgL_arg2882z00_3875 == BTRUE);
											}
										}
										if (BgL_test3737z00_10202)
											{	/* SawBbv/bbv-merge.scm 896 */
												obj_t BgL_arg2881z00_3872;

												BgL_arg2881z00_3872 = CDR(((obj_t) BgL_l1811z00_3867));
												{
													obj_t BgL_l1811z00_10211;

													BgL_l1811z00_10211 = BgL_arg2881z00_3872;
													BgL_l1811z00_3867 = BgL_l1811z00_10211;
													goto BgL_zc3z04anonymousza32880ze3z87_3868;
												}
											}
										else
											{	/* SawBbv/bbv-merge.scm 896 */
												BgL_test3735z00_10199 = ((bool_t) 0);
											}
									}
							}
							if (BgL_test3735z00_10199)
								{	/* SawBbv/bbv-merge.scm 896 */
									BgL_nlz00_3792 = BgL_lz00_274;
								}
							else
								{	/* SawBbv/bbv-merge.scm 896 */
									BgL_vz00_3921 = BgL_lz00_274;
									BgL_wideningz00_3786 = ((bool_t) 1);
									{	/* SawBbv/bbv-merge.scm 867 */
										bool_t BgL_test3738z00_10212;

										{	/* SawBbv/bbv-merge.scm 867 */
											obj_t BgL_classz00_5689;

											BgL_classz00_5689 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
											if (BGL_OBJECTP(BgL_vz00_3921))
												{	/* SawBbv/bbv-merge.scm 867 */
													BgL_objectz00_bglt BgL_arg1807z00_5691;

													BgL_arg1807z00_5691 =
														(BgL_objectz00_bglt) (BgL_vz00_3921);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* SawBbv/bbv-merge.scm 867 */
															long BgL_idxz00_5697;

															BgL_idxz00_5697 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5691);
															BgL_test3738z00_10212 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_5697 + 1L)) == BgL_classz00_5689);
														}
													else
														{	/* SawBbv/bbv-merge.scm 867 */
															bool_t BgL_res3271z00_5722;

															{	/* SawBbv/bbv-merge.scm 867 */
																obj_t BgL_oclassz00_5705;

																{	/* SawBbv/bbv-merge.scm 867 */
																	obj_t BgL_arg1815z00_5713;
																	long BgL_arg1816z00_5714;

																	BgL_arg1815z00_5713 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* SawBbv/bbv-merge.scm 867 */
																		long BgL_arg1817z00_5715;

																		BgL_arg1817z00_5715 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5691);
																		BgL_arg1816z00_5714 =
																			(BgL_arg1817z00_5715 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_5705 =
																		VECTOR_REF(BgL_arg1815z00_5713,
																		BgL_arg1816z00_5714);
																}
																{	/* SawBbv/bbv-merge.scm 867 */
																	bool_t BgL__ortest_1115z00_5706;

																	BgL__ortest_1115z00_5706 =
																		(BgL_classz00_5689 == BgL_oclassz00_5705);
																	if (BgL__ortest_1115z00_5706)
																		{	/* SawBbv/bbv-merge.scm 867 */
																			BgL_res3271z00_5722 =
																				BgL__ortest_1115z00_5706;
																		}
																	else
																		{	/* SawBbv/bbv-merge.scm 867 */
																			long BgL_odepthz00_5707;

																			{	/* SawBbv/bbv-merge.scm 867 */
																				obj_t BgL_arg1804z00_5708;

																				BgL_arg1804z00_5708 =
																					(BgL_oclassz00_5705);
																				BgL_odepthz00_5707 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_5708);
																			}
																			if ((1L < BgL_odepthz00_5707))
																				{	/* SawBbv/bbv-merge.scm 867 */
																					obj_t BgL_arg1802z00_5710;

																					{	/* SawBbv/bbv-merge.scm 867 */
																						obj_t BgL_arg1803z00_5711;

																						BgL_arg1803z00_5711 =
																							(BgL_oclassz00_5705);
																						BgL_arg1802z00_5710 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_5711, 1L);
																					}
																					BgL_res3271z00_5722 =
																						(BgL_arg1802z00_5710 ==
																						BgL_classz00_5689);
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 867 */
																					BgL_res3271z00_5722 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test3738z00_10212 = BgL_res3271z00_5722;
														}
												}
											else
												{	/* SawBbv/bbv-merge.scm 867 */
													BgL_test3738z00_10212 = ((bool_t) 0);
												}
										}
										if (BgL_test3738z00_10212)
											{	/* SawBbv/bbv-merge.scm 867 */
												BgL_vz00_3903 = BgL_vz00_3921;
												{	/* SawBbv/bbv-merge.scm 855 */
													bool_t BgL_test3743z00_10235;

													{	/* SawBbv/bbv-merge.scm 855 */

														BgL_test3743z00_10235 =
															(
															(long) (
																(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																			((BgL_bbvzd2vlenzd2_bglt)
																				BgL_vz00_3903)))->BgL_offsetz00)) <
															-10L);
													}
													if (BgL_test3743z00_10235)
														{	/* SawBbv/bbv-merge.scm 856 */
															long BgL_arg3066z00_5673;

															{	/* SawBbv/bbv-merge.scm 856 */
																long BgL_arg3067z00_5674;

																{	/* SawBbv/bbv-merge.scm 856 */
																	long BgL_arg3070z00_5675;

																	{	/* SawBbv/bbv-merge.scm 856 */
																		obj_t BgL_arg3071z00_5676;

																		BgL_arg3071z00_5676 =
																			BGl_bigloozd2configzd2zz__configurez00
																			(CNST_TABLE_REF(12));
																		BgL_arg3070z00_5675 =
																			((long) CINT(BgL_arg3071z00_5676) - 2L);
																	}
																	BgL_arg3067z00_5674 =
																		(1L << (int) (BgL_arg3070z00_5675));
																}
																BgL_arg3066z00_5673 = NEG(BgL_arg3067z00_5674);
															}
															BgL_nlz00_3792 = BINT((BgL_arg3066z00_5673 - 1L));
														}
													else
														{	/* SawBbv/bbv-merge.scm 855 */
															BgL_nlz00_3792 = BgL_vz00_3903;
														}
												}
											}
										else
											{	/* SawBbv/bbv-merge.scm 867 */
												if (INTEGERP(BgL_vz00_3921))
													{	/* SawBbv/bbv-merge.scm 868 */
														if (((long) CINT(BgL_vz00_3921) > 1L))
															{	/* SawBbv/bbv-merge.scm 869 */
																BgL_nlz00_3792 =
																	BINT(((long) CINT(BgL_vz00_3921) / 2L));
															}
														else
															{	/* SawBbv/bbv-merge.scm 869 */
																if (((long) CINT(BgL_vz00_3921) == 1L))
																	{	/* SawBbv/bbv-merge.scm 870 */
																		BgL_nlz00_3792 = BINT(0L);
																	}
																else
																	{	/* SawBbv/bbv-merge.scm 870 */
																		if (((long) CINT(BgL_vz00_3921) == 0L))
																			{	/* SawBbv/bbv-merge.scm 871 */
																				BgL_nlz00_3792 = BgL_vz00_3921;
																			}
																		else
																			{	/* SawBbv/bbv-merge.scm 871 */
																				if (((long) CINT(BgL_vz00_3921) > -16L))
																					{	/* SawBbv/bbv-merge.scm 872 */
																						BgL_nlz00_3792 =
																							BINT(
																							((long) CINT(BgL_vz00_3921) *
																								2L));
																					}
																				else
																					{	/* SawBbv/bbv-merge.scm 872 */
																						if (
																							((long) CINT(BgL_vz00_3921) >
																								-255L))
																							{	/* SawBbv/bbv-merge.scm 873 */
																								BgL_nlz00_3792 = BINT(-255L);
																							}
																						else
																							{	/* SawBbv/bbv-merge.scm 874 */
																								bool_t BgL_test3750z00_10274;

																								{	/* SawBbv/bbv-merge.scm 874 */
																									long BgL_arg2916z00_3934;

																									{	/* SawBbv/bbv-merge.scm 874 */
																										long BgL_arg2917z00_3935;

																										{	/* SawBbv/bbv-merge.scm 874 */
																											long BgL_arg3066z00_5730;

																											{	/* SawBbv/bbv-merge.scm 874 */
																												long
																													BgL_arg3067z00_5731;
																												{	/* SawBbv/bbv-merge.scm 874 */
																													long
																														BgL_arg3070z00_5732;
																													{	/* SawBbv/bbv-merge.scm 874 */
																														obj_t
																															BgL_arg3071z00_5733;
																														BgL_arg3071z00_5733
																															=
																															BGl_bigloozd2configzd2zz__configurez00
																															(CNST_TABLE_REF
																															(12));
																														BgL_arg3070z00_5732
																															=
																															((long)
																															CINT
																															(BgL_arg3071z00_5733)
																															- 2L);
																													}
																													BgL_arg3067z00_5731 =
																														(1L <<
																														(int)
																														(BgL_arg3070z00_5732));
																												}
																												BgL_arg3066z00_5730 =
																													NEG
																													(BgL_arg3067z00_5731);
																											}
																											BgL_arg2917z00_3935 =
																												(BgL_arg3066z00_5730 -
																												1L);
																										}
																										BgL_arg2916z00_3934 =
																											(BgL_arg2917z00_3935 +
																											1L);
																									}
																									BgL_test3750z00_10274 =
																										(
																										(long) CINT(BgL_vz00_3921)
																										>= BgL_arg2916z00_3934);
																								}
																								if (BgL_test3750z00_10274)
																									{	/* SawBbv/bbv-merge.scm 874 */
																										long BgL_arg2915z00_3933;

																										{	/* SawBbv/bbv-merge.scm 874 */
																											long BgL_arg3066z00_5741;

																											{	/* SawBbv/bbv-merge.scm 874 */
																												long
																													BgL_arg3067z00_5742;
																												{	/* SawBbv/bbv-merge.scm 874 */
																													long
																														BgL_arg3070z00_5743;
																													{	/* SawBbv/bbv-merge.scm 874 */
																														obj_t
																															BgL_arg3071z00_5744;
																														BgL_arg3071z00_5744
																															=
																															BGl_bigloozd2configzd2zz__configurez00
																															(CNST_TABLE_REF
																															(12));
																														BgL_arg3070z00_5743
																															=
																															((long)
																															CINT
																															(BgL_arg3071z00_5744)
																															- 2L);
																													}
																													BgL_arg3067z00_5742 =
																														(1L <<
																														(int)
																														(BgL_arg3070z00_5743));
																												}
																												BgL_arg3066z00_5741 =
																													NEG
																													(BgL_arg3067z00_5742);
																											}
																											BgL_arg2915z00_3933 =
																												(BgL_arg3066z00_5741 -
																												1L);
																										}
																										BgL_nlz00_3792 =
																											BINT(
																											(BgL_arg2915z00_3933 +
																												1L));
																									}
																								else
																									{	/* SawBbv/bbv-merge.scm 875 */
																										long BgL_arg3066z00_5750;

																										{	/* SawBbv/bbv-merge.scm 875 */
																											long BgL_arg3067z00_5751;

																											{	/* SawBbv/bbv-merge.scm 875 */
																												long
																													BgL_arg3070z00_5752;
																												{	/* SawBbv/bbv-merge.scm 875 */
																													obj_t
																														BgL_arg3071z00_5753;
																													BgL_arg3071z00_5753 =
																														BGl_bigloozd2configzd2zz__configurez00
																														(CNST_TABLE_REF
																														(12));
																													BgL_arg3070z00_5752 =
																														((long)
																														CINT
																														(BgL_arg3071z00_5753)
																														- 2L);
																												}
																												BgL_arg3067z00_5751 =
																													(1L <<
																													(int)
																													(BgL_arg3070z00_5752));
																											}
																											BgL_arg3066z00_5750 =
																												NEG
																												(BgL_arg3067z00_5751);
																										}
																										BgL_nlz00_3792 =
																											BINT(
																											(BgL_arg3066z00_5750 -
																												1L));
													}}}}}}}
												else
													{	/* SawBbv/bbv-merge.scm 868 */
														BgL_nlz00_3792 = BgL_vz00_3921;
													}
											}
									}
								}
						}
						{	/* SawBbv/bbv-merge.scm 902 */
							bool_t BgL_test3751z00_10305;

							{
								obj_t BgL_l1814z00_3892;

								BgL_l1814z00_3892 = BgL_rangesz00_276;
							BgL_zc3z04anonymousza32891ze3z87_3893:
								if (NULLP(BgL_l1814z00_3892))
									{	/* SawBbv/bbv-merge.scm 902 */
										BgL_test3751z00_10305 = ((bool_t) 1);
									}
								else
									{	/* SawBbv/bbv-merge.scm 902 */
										bool_t BgL_test3753z00_10308;

										{	/* SawBbv/bbv-merge.scm 903 */
											obj_t BgL_rz00_3898;

											BgL_rz00_3898 = CAR(((obj_t) BgL_l1814z00_3892));
											{	/* SawBbv/bbv-merge.scm 904 */
												obj_t BgL_arg2893z00_3900;

												{	/* SawBbv/bbv-merge.scm 904 */
													obj_t BgL_arg2894z00_3901;

													BgL_arg2894z00_3901 =
														(((BgL_bbvzd2rangezd2_bglt) COBJECT(
																((BgL_bbvzd2rangezd2_bglt) BgL_rz00_3898)))->
														BgL_upz00);
													BgL_arg2893z00_3900 =
														BGl_zd3rvzd3zzsaw_bbvzd2rangezd2(BgL_uz00_275,
														BgL_arg2894z00_3901);
												}
												BgL_test3753z00_10308 = (BgL_arg2893z00_3900 == BTRUE);
											}
										}
										if (BgL_test3753z00_10308)
											{	/* SawBbv/bbv-merge.scm 902 */
												obj_t BgL_arg2892z00_3897;

												BgL_arg2892z00_3897 = CDR(((obj_t) BgL_l1814z00_3892));
												{
													obj_t BgL_l1814z00_10317;

													BgL_l1814z00_10317 = BgL_arg2892z00_3897;
													BgL_l1814z00_3892 = BgL_l1814z00_10317;
													goto BgL_zc3z04anonymousza32891ze3z87_3893;
												}
											}
										else
											{	/* SawBbv/bbv-merge.scm 902 */
												BgL_test3751z00_10305 = ((bool_t) 0);
											}
									}
							}
							if (BgL_test3751z00_10305)
								{	/* SawBbv/bbv-merge.scm 902 */
									BgL_nuz00_3793 = BgL_uz00_275;
								}
							else
								{	/* SawBbv/bbv-merge.scm 902 */
									BgL_vz00_3945 = BgL_uz00_275;
									BgL_wideningz00_3786 = ((bool_t) 1);
									{	/* SawBbv/bbv-merge.scm 885 */
										bool_t BgL_test3754z00_10318;

										{	/* SawBbv/bbv-merge.scm 885 */
											obj_t BgL_classz00_5758;

											BgL_classz00_5758 = BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
											if (BGL_OBJECTP(BgL_vz00_3945))
												{	/* SawBbv/bbv-merge.scm 885 */
													BgL_objectz00_bglt BgL_arg1807z00_5760;

													BgL_arg1807z00_5760 =
														(BgL_objectz00_bglt) (BgL_vz00_3945);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* SawBbv/bbv-merge.scm 885 */
															long BgL_idxz00_5766;

															BgL_idxz00_5766 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5760);
															BgL_test3754z00_10318 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_5766 + 1L)) == BgL_classz00_5758);
														}
													else
														{	/* SawBbv/bbv-merge.scm 885 */
															bool_t BgL_res3272z00_5791;

															{	/* SawBbv/bbv-merge.scm 885 */
																obj_t BgL_oclassz00_5774;

																{	/* SawBbv/bbv-merge.scm 885 */
																	obj_t BgL_arg1815z00_5782;
																	long BgL_arg1816z00_5783;

																	BgL_arg1815z00_5782 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* SawBbv/bbv-merge.scm 885 */
																		long BgL_arg1817z00_5784;

																		BgL_arg1817z00_5784 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5760);
																		BgL_arg1816z00_5783 =
																			(BgL_arg1817z00_5784 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_5774 =
																		VECTOR_REF(BgL_arg1815z00_5782,
																		BgL_arg1816z00_5783);
																}
																{	/* SawBbv/bbv-merge.scm 885 */
																	bool_t BgL__ortest_1115z00_5775;

																	BgL__ortest_1115z00_5775 =
																		(BgL_classz00_5758 == BgL_oclassz00_5774);
																	if (BgL__ortest_1115z00_5775)
																		{	/* SawBbv/bbv-merge.scm 885 */
																			BgL_res3272z00_5791 =
																				BgL__ortest_1115z00_5775;
																		}
																	else
																		{	/* SawBbv/bbv-merge.scm 885 */
																			long BgL_odepthz00_5776;

																			{	/* SawBbv/bbv-merge.scm 885 */
																				obj_t BgL_arg1804z00_5777;

																				BgL_arg1804z00_5777 =
																					(BgL_oclassz00_5774);
																				BgL_odepthz00_5776 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_5777);
																			}
																			if ((1L < BgL_odepthz00_5776))
																				{	/* SawBbv/bbv-merge.scm 885 */
																					obj_t BgL_arg1802z00_5779;

																					{	/* SawBbv/bbv-merge.scm 885 */
																						obj_t BgL_arg1803z00_5780;

																						BgL_arg1803z00_5780 =
																							(BgL_oclassz00_5774);
																						BgL_arg1802z00_5779 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_5780, 1L);
																					}
																					BgL_res3272z00_5791 =
																						(BgL_arg1802z00_5779 ==
																						BgL_classz00_5758);
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 885 */
																					BgL_res3272z00_5791 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test3754z00_10318 = BgL_res3272z00_5791;
														}
												}
											else
												{	/* SawBbv/bbv-merge.scm 885 */
													BgL_test3754z00_10318 = ((bool_t) 0);
												}
										}
										if (BgL_test3754z00_10318)
											{	/* SawBbv/bbv-merge.scm 885 */
												BgL_vz00_3912 = BgL_vz00_3945;
												{	/* SawBbv/bbv-merge.scm 860 */
													bool_t BgL_test3759z00_10341;

													{	/* SawBbv/bbv-merge.scm 860 */

														BgL_test3759z00_10341 =
															(
															(long) (
																(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																			((BgL_bbvzd2vlenzd2_bglt)
																				BgL_vz00_3912)))->BgL_offsetz00)) >
															10L);
													}
													if (BgL_test3759z00_10341)
														{	/* SawBbv/bbv-merge.scm 861 */
															long BgL_arg3070z00_5685;

															{	/* SawBbv/bbv-merge.scm 861 */
																obj_t BgL_arg3071z00_5686;

																BgL_arg3071z00_5686 =
																	BGl_bigloozd2configzd2zz__configurez00
																	(CNST_TABLE_REF(12));
																BgL_arg3070z00_5685 =
																	((long) CINT(BgL_arg3071z00_5686) - 2L);
															}
															BgL_nuz00_3793 =
																BINT((1L << (int) (BgL_arg3070z00_5685)));
														}
													else
														{	/* SawBbv/bbv-merge.scm 860 */
															BgL_nuz00_3793 = BgL_vz00_3912;
														}
												}
											}
										else
											{	/* SawBbv/bbv-merge.scm 885 */
												if (INTEGERP(BgL_vz00_3945))
													{	/* SawBbv/bbv-merge.scm 886 */
														if (((long) CINT(BgL_vz00_3945) < -1L))
															{	/* SawBbv/bbv-merge.scm 887 */
																BgL_nuz00_3793 =
																	BINT(((long) CINT(BgL_vz00_3945) / 2L));
															}
														else
															{	/* SawBbv/bbv-merge.scm 887 */
																if (((long) CINT(BgL_vz00_3945) == -1L))
																	{	/* SawBbv/bbv-merge.scm 888 */
																		BgL_nuz00_3793 = BINT(0L);
																	}
																else
																	{	/* SawBbv/bbv-merge.scm 888 */
																		if (((long) CINT(BgL_vz00_3945) == 0L))
																			{	/* SawBbv/bbv-merge.scm 889 */
																				BgL_nuz00_3793 = BgL_vz00_3945;
																			}
																		else
																			{	/* SawBbv/bbv-merge.scm 889 */
																				if (((long) CINT(BgL_vz00_3945) < 16L))
																					{	/* SawBbv/bbv-merge.scm 890 */
																						BgL_nuz00_3793 =
																							BINT(
																							((long) CINT(BgL_vz00_3945) *
																								2L));
																					}
																				else
																					{	/* SawBbv/bbv-merge.scm 890 */
																						if (
																							((long) CINT(BgL_vz00_3945) <
																								255L))
																							{	/* SawBbv/bbv-merge.scm 891 */
																								BgL_nuz00_3793 = BINT(255L);
																							}
																						else
																							{	/* SawBbv/bbv-merge.scm 891 */
																								if (
																									((long) CINT(BgL_vz00_3945) <
																										65535L))
																									{	/* SawBbv/bbv-merge.scm 892 */
																										BgL_nuz00_3793 =
																											BINT(65535L);
																									}
																								else
																									{	/* SawBbv/bbv-merge.scm 893 */
																										bool_t
																											BgL_test3767z00_10382;
																										{	/* SawBbv/bbv-merge.scm 893 */
																											long BgL_arg2934z00_3959;

																											{	/* SawBbv/bbv-merge.scm 893 */
																												long
																													BgL_arg2935z00_3960;
																												{	/* SawBbv/bbv-merge.scm 893 */
																													long
																														BgL_arg3070z00_5800;
																													{	/* SawBbv/bbv-merge.scm 893 */
																														obj_t
																															BgL_arg3071z00_5801;
																														BgL_arg3071z00_5801
																															=
																															BGl_bigloozd2configzd2zz__configurez00
																															(CNST_TABLE_REF
																															(12));
																														BgL_arg3070z00_5800
																															=
																															((long)
																															CINT
																															(BgL_arg3071z00_5801)
																															- 2L);
																													}
																													BgL_arg2935z00_3960 =
																														(1L <<
																														(int)
																														(BgL_arg3070z00_5800));
																												}
																												BgL_arg2934z00_3959 =
																													(BgL_arg2935z00_3960 -
																													1L);
																											}
																											BgL_test3767z00_10382 =
																												(
																												(long)
																												CINT(BgL_vz00_3945) <=
																												BgL_arg2934z00_3959);
																										}
																										if (BgL_test3767z00_10382)
																											{	/* SawBbv/bbv-merge.scm 893 */
																												long
																													BgL_arg2933z00_3958;
																												{	/* SawBbv/bbv-merge.scm 893 */
																													long
																														BgL_arg3070z00_5807;
																													{	/* SawBbv/bbv-merge.scm 893 */
																														obj_t
																															BgL_arg3071z00_5808;
																														BgL_arg3071z00_5808
																															=
																															BGl_bigloozd2configzd2zz__configurez00
																															(CNST_TABLE_REF
																															(12));
																														BgL_arg3070z00_5807
																															=
																															((long)
																															CINT
																															(BgL_arg3071z00_5808)
																															- 2L);
																													}
																													BgL_arg2933z00_3958 =
																														(1L <<
																														(int)
																														(BgL_arg3070z00_5807));
																												}
																												BgL_nuz00_3793 =
																													BINT(
																													(BgL_arg2933z00_3958 -
																														1L));
																											}
																										else
																											{	/* SawBbv/bbv-merge.scm 894 */
																												long
																													BgL_arg3070z00_5812;
																												{	/* SawBbv/bbv-merge.scm 894 */
																													obj_t
																														BgL_arg3071z00_5813;
																													BgL_arg3071z00_5813 =
																														BGl_bigloozd2configzd2zz__configurez00
																														(CNST_TABLE_REF
																														(12));
																													BgL_arg3070z00_5812 =
																														((long)
																														CINT
																														(BgL_arg3071z00_5813)
																														- 2L);
																												}
																												BgL_nuz00_3793 =
																													BINT(
																													(1L <<
																														(int)
																														(BgL_arg3070z00_5812)));
													}}}}}}}}
												else
													{	/* SawBbv/bbv-merge.scm 886 */
														BgL_nuz00_3793 = BgL_vz00_3945;
													}
											}
									}
								}
						}
						{	/* SawBbv/bbv-merge.scm 909 */
							bool_t BgL_test3768z00_10407;

							if (
								(BGl_zd3rvzd3zzsaw_bbvzd2rangezd2(BgL_nlz00_3792,
										BgL_lz00_274) == BTRUE))
								{	/* SawBbv/bbv-merge.scm 909 */
									if (
										(BGl_zd3rvzd3zzsaw_bbvzd2rangezd2(BgL_nuz00_3793,
												BgL_uz00_275) == BTRUE))
										{	/* SawBbv/bbv-merge.scm 909 */
											if (BgL_wideningz00_3786)
												{	/* SawBbv/bbv-merge.scm 909 */
													BgL_test3768z00_10407 = ((bool_t) 0);
												}
											else
												{	/* SawBbv/bbv-merge.scm 909 */
													BgL_test3768z00_10407 = ((bool_t) 1);
												}
										}
									else
										{	/* SawBbv/bbv-merge.scm 909 */
											BgL_test3768z00_10407 = ((bool_t) 0);
										}
								}
							else
								{	/* SawBbv/bbv-merge.scm 909 */
									BgL_test3768z00_10407 = ((bool_t) 0);
								}
							if (BgL_test3768z00_10407)
								{	/* SawBbv/bbv-merge.scm 909 */
									return CAR(((obj_t) BgL_rangesz00_276));
								}
							else
								{	/* SawBbv/bbv-merge.scm 911 */
									bool_t BgL_test3772z00_10417;

									{
										obj_t BgL_l1817z00_3831;

										BgL_l1817z00_3831 = BgL_rangesz00_276;
									BgL_zc3z04anonymousza32847ze3z87_3832:
										if (NULLP(BgL_l1817z00_3831))
											{	/* SawBbv/bbv-merge.scm 911 */
												BgL_test3772z00_10417 = ((bool_t) 1);
											}
										else
											{	/* SawBbv/bbv-merge.scm 911 */
												bool_t BgL_test3774z00_10420;

												{	/* SawBbv/bbv-merge.scm 912 */
													BgL_bbvzd2rangezd2_bglt BgL_i1300z00_3838;

													BgL_i1300z00_3838 =
														((BgL_bbvzd2rangezd2_bglt)
														CAR(((obj_t) BgL_rangesz00_276)));
													{	/* SawBbv/bbv-merge.scm 913 */
														BgL_bbvzd2rangezd2_bglt BgL_i1301z00_3839;

														BgL_i1301z00_3839 =
															((BgL_bbvzd2rangezd2_bglt)
															CAR(((obj_t) BgL_l1817z00_3831)));
														{	/* SawBbv/bbv-merge.scm 914 */
															bool_t BgL_test3775z00_10427;

															{	/* SawBbv/bbv-merge.scm 914 */
																obj_t BgL_arg2871z00_3847;

																BgL_arg2871z00_3847 =
																	(((BgL_bbvzd2rangezd2_bglt)
																		COBJECT(BgL_i1300z00_3838))->BgL_upz00);
																{	/* SawBbv/bbv-merge.scm 914 */
																	obj_t BgL_classz00_5823;

																	BgL_classz00_5823 =
																		BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
																	if (BGL_OBJECTP(BgL_arg2871z00_3847))
																		{	/* SawBbv/bbv-merge.scm 914 */
																			BgL_objectz00_bglt BgL_arg1807z00_5825;

																			BgL_arg1807z00_5825 =
																				(BgL_objectz00_bglt)
																				(BgL_arg2871z00_3847);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* SawBbv/bbv-merge.scm 914 */
																					long BgL_idxz00_5831;

																					BgL_idxz00_5831 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_5825);
																					BgL_test3775z00_10427 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_5831 + 1L)) ==
																						BgL_classz00_5823);
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 914 */
																					bool_t BgL_res3273z00_5856;

																					{	/* SawBbv/bbv-merge.scm 914 */
																						obj_t BgL_oclassz00_5839;

																						{	/* SawBbv/bbv-merge.scm 914 */
																							obj_t BgL_arg1815z00_5847;
																							long BgL_arg1816z00_5848;

																							BgL_arg1815z00_5847 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* SawBbv/bbv-merge.scm 914 */
																								long BgL_arg1817z00_5849;

																								BgL_arg1817z00_5849 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_5825);
																								BgL_arg1816z00_5848 =
																									(BgL_arg1817z00_5849 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_5839 =
																								VECTOR_REF(BgL_arg1815z00_5847,
																								BgL_arg1816z00_5848);
																						}
																						{	/* SawBbv/bbv-merge.scm 914 */
																							bool_t BgL__ortest_1115z00_5840;

																							BgL__ortest_1115z00_5840 =
																								(BgL_classz00_5823 ==
																								BgL_oclassz00_5839);
																							if (BgL__ortest_1115z00_5840)
																								{	/* SawBbv/bbv-merge.scm 914 */
																									BgL_res3273z00_5856 =
																										BgL__ortest_1115z00_5840;
																								}
																							else
																								{	/* SawBbv/bbv-merge.scm 914 */
																									long BgL_odepthz00_5841;

																									{	/* SawBbv/bbv-merge.scm 914 */
																										obj_t BgL_arg1804z00_5842;

																										BgL_arg1804z00_5842 =
																											(BgL_oclassz00_5839);
																										BgL_odepthz00_5841 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_5842);
																									}
																									if ((1L < BgL_odepthz00_5841))
																										{	/* SawBbv/bbv-merge.scm 914 */
																											obj_t BgL_arg1802z00_5844;

																											{	/* SawBbv/bbv-merge.scm 914 */
																												obj_t
																													BgL_arg1803z00_5845;
																												BgL_arg1803z00_5845 =
																													(BgL_oclassz00_5839);
																												BgL_arg1802z00_5844 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_5845,
																													1L);
																											}
																											BgL_res3273z00_5856 =
																												(BgL_arg1802z00_5844 ==
																												BgL_classz00_5823);
																										}
																									else
																										{	/* SawBbv/bbv-merge.scm 914 */
																											BgL_res3273z00_5856 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test3775z00_10427 =
																						BgL_res3273z00_5856;
																				}
																		}
																	else
																		{	/* SawBbv/bbv-merge.scm 914 */
																			BgL_test3775z00_10427 = ((bool_t) 0);
																		}
																}
															}
															if (BgL_test3775z00_10427)
																{	/* SawBbv/bbv-merge.scm 915 */
																	bool_t BgL_test3780z00_10451;

																	{	/* SawBbv/bbv-merge.scm 915 */
																		obj_t BgL_arg2870z00_3846;

																		BgL_arg2870z00_3846 =
																			(((BgL_bbvzd2rangezd2_bglt)
																				COBJECT(BgL_i1301z00_3839))->BgL_upz00);
																		{	/* SawBbv/bbv-merge.scm 915 */
																			obj_t BgL_classz00_5857;

																			BgL_classz00_5857 =
																				BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
																			if (BGL_OBJECTP(BgL_arg2870z00_3846))
																				{	/* SawBbv/bbv-merge.scm 915 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_5859;
																					BgL_arg1807z00_5859 =
																						(BgL_objectz00_bglt)
																						(BgL_arg2870z00_3846);
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* SawBbv/bbv-merge.scm 915 */
																							long BgL_idxz00_5865;

																							BgL_idxz00_5865 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_5859);
																							BgL_test3780z00_10451 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_5865 + 1L)) ==
																								BgL_classz00_5857);
																						}
																					else
																						{	/* SawBbv/bbv-merge.scm 915 */
																							bool_t BgL_res3274z00_5890;

																							{	/* SawBbv/bbv-merge.scm 915 */
																								obj_t BgL_oclassz00_5873;

																								{	/* SawBbv/bbv-merge.scm 915 */
																									obj_t BgL_arg1815z00_5881;
																									long BgL_arg1816z00_5882;

																									BgL_arg1815z00_5881 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* SawBbv/bbv-merge.scm 915 */
																										long BgL_arg1817z00_5883;

																										BgL_arg1817z00_5883 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_5859);
																										BgL_arg1816z00_5882 =
																											(BgL_arg1817z00_5883 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_5873 =
																										VECTOR_REF
																										(BgL_arg1815z00_5881,
																										BgL_arg1816z00_5882);
																								}
																								{	/* SawBbv/bbv-merge.scm 915 */
																									bool_t
																										BgL__ortest_1115z00_5874;
																									BgL__ortest_1115z00_5874 =
																										(BgL_classz00_5857 ==
																										BgL_oclassz00_5873);
																									if (BgL__ortest_1115z00_5874)
																										{	/* SawBbv/bbv-merge.scm 915 */
																											BgL_res3274z00_5890 =
																												BgL__ortest_1115z00_5874;
																										}
																									else
																										{	/* SawBbv/bbv-merge.scm 915 */
																											long BgL_odepthz00_5875;

																											{	/* SawBbv/bbv-merge.scm 915 */
																												obj_t
																													BgL_arg1804z00_5876;
																												BgL_arg1804z00_5876 =
																													(BgL_oclassz00_5873);
																												BgL_odepthz00_5875 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_5876);
																											}
																											if (
																												(1L <
																													BgL_odepthz00_5875))
																												{	/* SawBbv/bbv-merge.scm 915 */
																													obj_t
																														BgL_arg1802z00_5878;
																													{	/* SawBbv/bbv-merge.scm 915 */
																														obj_t
																															BgL_arg1803z00_5879;
																														BgL_arg1803z00_5879
																															=
																															(BgL_oclassz00_5873);
																														BgL_arg1802z00_5878
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_5879,
																															1L);
																													}
																													BgL_res3274z00_5890 =
																														(BgL_arg1802z00_5878
																														==
																														BgL_classz00_5857);
																												}
																											else
																												{	/* SawBbv/bbv-merge.scm 915 */
																													BgL_res3274z00_5890 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test3780z00_10451 =
																								BgL_res3274z00_5890;
																						}
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 915 */
																					BgL_test3780z00_10451 = ((bool_t) 0);
																				}
																		}
																	}
																	if (BgL_test3780z00_10451)
																		{	/* SawBbv/bbv-merge.scm 915 */
																			BgL_test3774z00_10420 =
																				(
																				(((BgL_bbvzd2vlenzd2_bglt) COBJECT(
																							((BgL_bbvzd2vlenzd2_bglt)
																								(((BgL_bbvzd2rangezd2_bglt)
																										COBJECT
																										(BgL_i1300z00_3838))->
																									BgL_upz00))))->BgL_vecz00) ==
																				(((BgL_bbvzd2vlenzd2_bglt)
																						COBJECT(((BgL_bbvzd2vlenzd2_bglt) ((
																										(BgL_bbvzd2rangezd2_bglt)
																										COBJECT
																										(BgL_i1301z00_3839))->
																									BgL_upz00))))->BgL_vecz00));
																		}
																	else
																		{	/* SawBbv/bbv-merge.scm 915 */
																			BgL_test3774z00_10420 = ((bool_t) 0);
																		}
																}
															else
																{	/* SawBbv/bbv-merge.scm 914 */
																	BgL_test3774z00_10420 = ((bool_t) 0);
																}
														}
													}
												}
												if (BgL_test3774z00_10420)
													{	/* SawBbv/bbv-merge.scm 911 */
														obj_t BgL_arg2848z00_3836;

														BgL_arg2848z00_3836 =
															CDR(((obj_t) BgL_l1817z00_3831));
														{
															obj_t BgL_l1817z00_10484;

															BgL_l1817z00_10484 = BgL_arg2848z00_3836;
															BgL_l1817z00_3831 = BgL_l1817z00_10484;
															goto BgL_zc3z04anonymousza32847ze3z87_3832;
														}
													}
												else
													{	/* SawBbv/bbv-merge.scm 911 */
														BgL_test3772z00_10417 = ((bool_t) 0);
													}
											}
									}
									if (BgL_test3772z00_10417)
										{	/* SawBbv/bbv-merge.scm 911 */
											if ((BgL_nuz00_3793 == BgL_uz00_275))
												{	/* SawBbv/bbv-merge.scm 920 */
													BgL_bbvzd2rangezd2_bglt BgL_new1305z00_3821;

													{	/* SawBbv/bbv-merge.scm 921 */
														BgL_bbvzd2rangezd2_bglt BgL_new1304z00_3822;

														BgL_new1304z00_3822 =
															((BgL_bbvzd2rangezd2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_bbvzd2rangezd2_bgl))));
														{	/* SawBbv/bbv-merge.scm 921 */
															long BgL_arg2839z00_3823;

															BgL_arg2839z00_3823 =
																BGL_CLASS_NUM
																(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																	BgL_new1304z00_3822), BgL_arg2839z00_3823);
														}
														BgL_new1305z00_3821 = BgL_new1304z00_3822;
													}
													((((BgL_bbvzd2rangezd2_bglt)
																COBJECT(BgL_new1305z00_3821))->BgL_loz00) =
														((obj_t) BgL_nlz00_3792), BUNSPEC);
													((((BgL_bbvzd2rangezd2_bglt)
																COBJECT(BgL_new1305z00_3821))->BgL_upz00) =
														((obj_t) BgL_uz00_275), BUNSPEC);
													return ((obj_t) BgL_new1305z00_3821);
												}
											else
												{	/* SawBbv/bbv-merge.scm 923 */
													BgL_bbvzd2rangezd2_bglt BgL_new1308z00_3824;

													{	/* SawBbv/bbv-merge.scm 924 */
														BgL_bbvzd2rangezd2_bglt BgL_new1307z00_3825;

														BgL_new1307z00_3825 =
															((BgL_bbvzd2rangezd2_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_bbvzd2rangezd2_bgl))));
														{	/* SawBbv/bbv-merge.scm 924 */
															long BgL_arg2844z00_3826;

															BgL_arg2844z00_3826 =
																BGL_CLASS_NUM
																(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																	BgL_new1307z00_3825), BgL_arg2844z00_3826);
														}
														BgL_new1308z00_3824 = BgL_new1307z00_3825;
													}
													((((BgL_bbvzd2rangezd2_bglt)
																COBJECT(BgL_new1308z00_3824))->BgL_loz00) =
														((obj_t) BgL_lz00_274), BUNSPEC);
													((((BgL_bbvzd2rangezd2_bglt)
																COBJECT(BgL_new1308z00_3824))->BgL_upz00) =
														((obj_t) BgL_nuz00_3793), BUNSPEC);
													return ((obj_t) BgL_new1308z00_3824);
												}
										}
									else
										{	/* SawBbv/bbv-merge.scm 927 */
											BgL_bbvzd2rangezd2_bglt BgL_new1310z00_3827;

											{	/* SawBbv/bbv-merge.scm 928 */
												BgL_bbvzd2rangezd2_bglt BgL_new1309z00_3828;

												BgL_new1309z00_3828 =
													((BgL_bbvzd2rangezd2_bglt)
													BOBJECT(GC_MALLOC(sizeof(struct
																BgL_bbvzd2rangezd2_bgl))));
												{	/* SawBbv/bbv-merge.scm 928 */
													long BgL_arg2846z00_3829;

													BgL_arg2846z00_3829 =
														BGL_CLASS_NUM
														(BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2);
													BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
															BgL_new1309z00_3828), BgL_arg2846z00_3829);
												}
												BgL_new1310z00_3827 = BgL_new1309z00_3828;
											}
											((((BgL_bbvzd2rangezd2_bglt)
														COBJECT(BgL_new1310z00_3827))->BgL_loz00) =
												((obj_t) BgL_nlz00_3792), BUNSPEC);
											((((BgL_bbvzd2rangezd2_bglt)
														COBJECT(BgL_new1310z00_3827))->BgL_upz00) =
												((obj_t) BgL_nuz00_3793), BUNSPEC);
											return ((obj_t) BgL_new1310z00_3827);
										}
								}
						}
					}
				}
			}
		}

	}



/* entry-size */
	long BGl_entryzd2siza7ez75zzsaw_bbvzd2mergezd2(BgL_bbvzd2ctxentryzd2_bglt
		BgL_ez00_277)
	{
		{	/* SawBbv/bbv-merge.scm 934 */
			{
				BgL_bbvzd2rangezd2_bglt BgL_rz00_4005;

				if (
					(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_277))->
						BgL_polarityz00))
					{	/* SawBbv/bbv-merge.scm 956 */
						bool_t BgL_test3787z00_10510;

						{	/* SawBbv/bbv-merge.scm 956 */
							obj_t BgL_arg2980z00_3998;

							BgL_arg2980z00_3998 =
								CAR(
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_277))->
									BgL_typesz00));
							BgL_test3787z00_10510 =
								(BgL_arg2980z00_3998 == BGl_za2objza2z00zztype_cachez00);
						}
						if (BgL_test3787z00_10510)
							{	/* SawBbv/bbv-merge.scm 956 */
								return 100L;
							}
						else
							{	/* SawBbv/bbv-merge.scm 957 */
								bool_t BgL_test3788z00_10514;

								{	/* SawBbv/bbv-merge.scm 957 */
									obj_t BgL_arg2979z00_3997;

									BgL_arg2979z00_3997 =
										(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_277))->
										BgL_valuez00);
									{	/* SawBbv/bbv-merge.scm 957 */
										obj_t BgL_classz00_5982;

										BgL_classz00_5982 = BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2;
										if (BGL_OBJECTP(BgL_arg2979z00_3997))
											{	/* SawBbv/bbv-merge.scm 957 */
												BgL_objectz00_bglt BgL_arg1807z00_5984;

												BgL_arg1807z00_5984 =
													(BgL_objectz00_bglt) (BgL_arg2979z00_3997);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawBbv/bbv-merge.scm 957 */
														long BgL_idxz00_5990;

														BgL_idxz00_5990 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5984);
														BgL_test3788z00_10514 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_5990 + 1L)) == BgL_classz00_5982);
													}
												else
													{	/* SawBbv/bbv-merge.scm 957 */
														bool_t BgL_res3277z00_6015;

														{	/* SawBbv/bbv-merge.scm 957 */
															obj_t BgL_oclassz00_5998;

															{	/* SawBbv/bbv-merge.scm 957 */
																obj_t BgL_arg1815z00_6006;
																long BgL_arg1816z00_6007;

																BgL_arg1815z00_6006 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawBbv/bbv-merge.scm 957 */
																	long BgL_arg1817z00_6008;

																	BgL_arg1817z00_6008 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5984);
																	BgL_arg1816z00_6007 =
																		(BgL_arg1817z00_6008 - OBJECT_TYPE);
																}
																BgL_oclassz00_5998 =
																	VECTOR_REF(BgL_arg1815z00_6006,
																	BgL_arg1816z00_6007);
															}
															{	/* SawBbv/bbv-merge.scm 957 */
																bool_t BgL__ortest_1115z00_5999;

																BgL__ortest_1115z00_5999 =
																	(BgL_classz00_5982 == BgL_oclassz00_5998);
																if (BgL__ortest_1115z00_5999)
																	{	/* SawBbv/bbv-merge.scm 957 */
																		BgL_res3277z00_6015 =
																			BgL__ortest_1115z00_5999;
																	}
																else
																	{	/* SawBbv/bbv-merge.scm 957 */
																		long BgL_odepthz00_6000;

																		{	/* SawBbv/bbv-merge.scm 957 */
																			obj_t BgL_arg1804z00_6001;

																			BgL_arg1804z00_6001 =
																				(BgL_oclassz00_5998);
																			BgL_odepthz00_6000 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_6001);
																		}
																		if ((1L < BgL_odepthz00_6000))
																			{	/* SawBbv/bbv-merge.scm 957 */
																				obj_t BgL_arg1802z00_6003;

																				{	/* SawBbv/bbv-merge.scm 957 */
																					obj_t BgL_arg1803z00_6004;

																					BgL_arg1803z00_6004 =
																						(BgL_oclassz00_5998);
																					BgL_arg1802z00_6003 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_6004, 1L);
																				}
																				BgL_res3277z00_6015 =
																					(BgL_arg1802z00_6003 ==
																					BgL_classz00_5982);
																			}
																		else
																			{	/* SawBbv/bbv-merge.scm 957 */
																				BgL_res3277z00_6015 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test3788z00_10514 = BgL_res3277z00_6015;
													}
											}
										else
											{	/* SawBbv/bbv-merge.scm 957 */
												BgL_test3788z00_10514 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test3788z00_10514)
									{	/* SawBbv/bbv-merge.scm 957 */
										BgL_rz00_4005 =
											((BgL_bbvzd2rangezd2_bglt)
											(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_277))->
												BgL_valuez00));
										{	/* SawBbv/bbv-merge.scm 939 */
											bool_t BgL_test3793z00_10538;

											{	/* SawBbv/bbv-merge.scm 939 */
												bool_t BgL_test3794z00_10539;

												{	/* SawBbv/bbv-merge.scm 939 */
													obj_t BgL_arg3050z00_4058;

													BgL_arg3050z00_4058 =
														(((BgL_bbvzd2rangezd2_bglt)
															COBJECT(BgL_rz00_4005))->BgL_loz00);
													{	/* SawBbv/bbv-merge.scm 939 */
														obj_t BgL_classz00_5905;

														BgL_classz00_5905 =
															BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
														if (BGL_OBJECTP(BgL_arg3050z00_4058))
															{	/* SawBbv/bbv-merge.scm 939 */
																BgL_objectz00_bglt BgL_arg1807z00_5907;

																BgL_arg1807z00_5907 =
																	(BgL_objectz00_bglt) (BgL_arg3050z00_4058);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* SawBbv/bbv-merge.scm 939 */
																		long BgL_idxz00_5913;

																		BgL_idxz00_5913 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_5907);
																		BgL_test3794z00_10539 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_5913 + 1L)) ==
																			BgL_classz00_5905);
																	}
																else
																	{	/* SawBbv/bbv-merge.scm 939 */
																		bool_t BgL_res3275z00_5938;

																		{	/* SawBbv/bbv-merge.scm 939 */
																			obj_t BgL_oclassz00_5921;

																			{	/* SawBbv/bbv-merge.scm 939 */
																				obj_t BgL_arg1815z00_5929;
																				long BgL_arg1816z00_5930;

																				BgL_arg1815z00_5929 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* SawBbv/bbv-merge.scm 939 */
																					long BgL_arg1817z00_5931;

																					BgL_arg1817z00_5931 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_5907);
																					BgL_arg1816z00_5930 =
																						(BgL_arg1817z00_5931 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_5921 =
																					VECTOR_REF(BgL_arg1815z00_5929,
																					BgL_arg1816z00_5930);
																			}
																			{	/* SawBbv/bbv-merge.scm 939 */
																				bool_t BgL__ortest_1115z00_5922;

																				BgL__ortest_1115z00_5922 =
																					(BgL_classz00_5905 ==
																					BgL_oclassz00_5921);
																				if (BgL__ortest_1115z00_5922)
																					{	/* SawBbv/bbv-merge.scm 939 */
																						BgL_res3275z00_5938 =
																							BgL__ortest_1115z00_5922;
																					}
																				else
																					{	/* SawBbv/bbv-merge.scm 939 */
																						long BgL_odepthz00_5923;

																						{	/* SawBbv/bbv-merge.scm 939 */
																							obj_t BgL_arg1804z00_5924;

																							BgL_arg1804z00_5924 =
																								(BgL_oclassz00_5921);
																							BgL_odepthz00_5923 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_5924);
																						}
																						if ((1L < BgL_odepthz00_5923))
																							{	/* SawBbv/bbv-merge.scm 939 */
																								obj_t BgL_arg1802z00_5926;

																								{	/* SawBbv/bbv-merge.scm 939 */
																									obj_t BgL_arg1803z00_5927;

																									BgL_arg1803z00_5927 =
																										(BgL_oclassz00_5921);
																									BgL_arg1802z00_5926 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_5927, 1L);
																								}
																								BgL_res3275z00_5938 =
																									(BgL_arg1802z00_5926 ==
																									BgL_classz00_5905);
																							}
																						else
																							{	/* SawBbv/bbv-merge.scm 939 */
																								BgL_res3275z00_5938 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test3794z00_10539 = BgL_res3275z00_5938;
																	}
															}
														else
															{	/* SawBbv/bbv-merge.scm 939 */
																BgL_test3794z00_10539 = ((bool_t) 0);
															}
													}
												}
												if (BgL_test3794z00_10539)
													{	/* SawBbv/bbv-merge.scm 939 */
														obj_t BgL_arg3049z00_4057;

														BgL_arg3049z00_4057 =
															(((BgL_bbvzd2rangezd2_bglt)
																COBJECT(BgL_rz00_4005))->BgL_upz00);
														{	/* SawBbv/bbv-merge.scm 939 */
															obj_t BgL_classz00_5939;

															BgL_classz00_5939 =
																BGl_bbvzd2vlenzd2zzsaw_bbvzd2rangezd2;
															if (BGL_OBJECTP(BgL_arg3049z00_4057))
																{	/* SawBbv/bbv-merge.scm 939 */
																	BgL_objectz00_bglt BgL_arg1807z00_5941;

																	BgL_arg1807z00_5941 =
																		(BgL_objectz00_bglt) (BgL_arg3049z00_4057);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* SawBbv/bbv-merge.scm 939 */
																			long BgL_idxz00_5947;

																			BgL_idxz00_5947 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_5941);
																			BgL_test3793z00_10538 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_5947 + 1L)) ==
																				BgL_classz00_5939);
																		}
																	else
																		{	/* SawBbv/bbv-merge.scm 939 */
																			bool_t BgL_res3276z00_5972;

																			{	/* SawBbv/bbv-merge.scm 939 */
																				obj_t BgL_oclassz00_5955;

																				{	/* SawBbv/bbv-merge.scm 939 */
																					obj_t BgL_arg1815z00_5963;
																					long BgL_arg1816z00_5964;

																					BgL_arg1815z00_5963 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* SawBbv/bbv-merge.scm 939 */
																						long BgL_arg1817z00_5965;

																						BgL_arg1817z00_5965 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_5941);
																						BgL_arg1816z00_5964 =
																							(BgL_arg1817z00_5965 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_5955 =
																						VECTOR_REF(BgL_arg1815z00_5963,
																						BgL_arg1816z00_5964);
																				}
																				{	/* SawBbv/bbv-merge.scm 939 */
																					bool_t BgL__ortest_1115z00_5956;

																					BgL__ortest_1115z00_5956 =
																						(BgL_classz00_5939 ==
																						BgL_oclassz00_5955);
																					if (BgL__ortest_1115z00_5956)
																						{	/* SawBbv/bbv-merge.scm 939 */
																							BgL_res3276z00_5972 =
																								BgL__ortest_1115z00_5956;
																						}
																					else
																						{	/* SawBbv/bbv-merge.scm 939 */
																							long BgL_odepthz00_5957;

																							{	/* SawBbv/bbv-merge.scm 939 */
																								obj_t BgL_arg1804z00_5958;

																								BgL_arg1804z00_5958 =
																									(BgL_oclassz00_5955);
																								BgL_odepthz00_5957 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_5958);
																							}
																							if ((1L < BgL_odepthz00_5957))
																								{	/* SawBbv/bbv-merge.scm 939 */
																									obj_t BgL_arg1802z00_5960;

																									{	/* SawBbv/bbv-merge.scm 939 */
																										obj_t BgL_arg1803z00_5961;

																										BgL_arg1803z00_5961 =
																											(BgL_oclassz00_5955);
																										BgL_arg1802z00_5960 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_5961, 1L);
																									}
																									BgL_res3276z00_5972 =
																										(BgL_arg1802z00_5960 ==
																										BgL_classz00_5939);
																								}
																							else
																								{	/* SawBbv/bbv-merge.scm 939 */
																									BgL_res3276z00_5972 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test3793z00_10538 =
																				BgL_res3276z00_5972;
																		}
																}
															else
																{	/* SawBbv/bbv-merge.scm 939 */
																	BgL_test3793z00_10538 = ((bool_t) 0);
																}
														}
													}
												else
													{	/* SawBbv/bbv-merge.scm 939 */
														BgL_test3793z00_10538 = ((bool_t) 0);
													}
											}
											if (BgL_test3793z00_10538)
												{	/* SawBbv/bbv-merge.scm 939 */
													return 4L;
												}
											else
												{	/* SawBbv/bbv-merge.scm 940 */
													bool_t BgL_test3803z00_10586;

													if (INTEGERP(
															(((BgL_bbvzd2rangezd2_bglt)
																	COBJECT(BgL_rz00_4005))->BgL_loz00)))
														{	/* SawBbv/bbv-merge.scm 940 */
															if (INTEGERP(
																	(((BgL_bbvzd2rangezd2_bglt)
																			COBJECT(BgL_rz00_4005))->BgL_upz00)))
																{	/* SawBbv/bbv-merge.scm 940 */
																	BgL_test3803z00_10586 = ((bool_t) 0);
																}
															else
																{	/* SawBbv/bbv-merge.scm 940 */
																	BgL_test3803z00_10586 = ((bool_t) 1);
																}
														}
													else
														{	/* SawBbv/bbv-merge.scm 940 */
															BgL_test3803z00_10586 = ((bool_t) 1);
														}
													if (BgL_test3803z00_10586)
														{	/* SawBbv/bbv-merge.scm 940 */
															return 6L;
														}
													else
														{	/* SawBbv/bbv-merge.scm 941 */
															bool_t BgL_test3806z00_10593;

															if (
																((long) CINT(
																		(((BgL_bbvzd2rangezd2_bglt)
																				COBJECT(BgL_rz00_4005))->BgL_loz00)) >=
																	-128L))
																{	/* SawBbv/bbv-merge.scm 941 */
																	BgL_test3806z00_10593 =
																		(
																		(long) CINT(
																			(((BgL_bbvzd2rangezd2_bglt)
																					COBJECT(BgL_rz00_4005))->
																				BgL_upz00)) <= 127L);
																}
															else
																{	/* SawBbv/bbv-merge.scm 941 */
																	BgL_test3806z00_10593 = ((bool_t) 0);
																}
															if (BgL_test3806z00_10593)
																{	/* SawBbv/bbv-merge.scm 941 */
																	return 1L;
																}
															else
																{	/* SawBbv/bbv-merge.scm 942 */
																	bool_t BgL_test3808z00_10601;

																	if (
																		((long) CINT(
																				(((BgL_bbvzd2rangezd2_bglt)
																						COBJECT(BgL_rz00_4005))->
																					BgL_loz00)) >= 0L))
																		{	/* SawBbv/bbv-merge.scm 942 */
																			BgL_test3808z00_10601 =
																				(
																				(long) CINT(
																					(((BgL_bbvzd2rangezd2_bglt)
																							COBJECT(BgL_rz00_4005))->
																						BgL_upz00)) <= 255L);
																		}
																	else
																		{	/* SawBbv/bbv-merge.scm 942 */
																			BgL_test3808z00_10601 = ((bool_t) 0);
																		}
																	if (BgL_test3808z00_10601)
																		{	/* SawBbv/bbv-merge.scm 942 */
																			return 1L;
																		}
																	else
																		{	/* SawBbv/bbv-merge.scm 943 */
																			bool_t BgL_test3810z00_10609;

																			if (
																				((long) CINT(
																						(((BgL_bbvzd2rangezd2_bglt)
																								COBJECT(BgL_rz00_4005))->
																							BgL_loz00)) >= -65536L))
																				{	/* SawBbv/bbv-merge.scm 943 */
																					BgL_test3810z00_10609 =
																						(
																						(long) CINT(
																							(((BgL_bbvzd2rangezd2_bglt)
																									COBJECT(BgL_rz00_4005))->
																								BgL_upz00)) <= -65535L);
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 943 */
																					BgL_test3810z00_10609 = ((bool_t) 0);
																				}
																			if (BgL_test3810z00_10609)
																				{	/* SawBbv/bbv-merge.scm 943 */
																					return 2L;
																				}
																			else
																				{	/* SawBbv/bbv-merge.scm 944 */
																					bool_t BgL_test3812z00_10617;

																					if (
																						((long) CINT(
																								(((BgL_bbvzd2rangezd2_bglt)
																										COBJECT(BgL_rz00_4005))->
																									BgL_loz00)) >= 0L))
																						{	/* SawBbv/bbv-merge.scm 944 */
																							BgL_test3812z00_10617 =
																								(
																								(long) CINT(
																									(((BgL_bbvzd2rangezd2_bglt)
																											COBJECT(BgL_rz00_4005))->
																										BgL_upz00)) <= 536870912L);
																						}
																					else
																						{	/* SawBbv/bbv-merge.scm 944 */
																							BgL_test3812z00_10617 =
																								((bool_t) 0);
																						}
																					if (BgL_test3812z00_10617)
																						{	/* SawBbv/bbv-merge.scm 944 */
																							return 3L;
																						}
																					else
																						{	/* SawBbv/bbv-merge.scm 944 */
																							return 5L;
																						}
																				}
																		}
																}
														}
												}
										}
									}
								else
									{	/* SawBbv/bbv-merge.scm 959 */
										obj_t BgL_arg2966z00_3978;

										{	/* SawBbv/bbv-merge.scm 959 */
											obj_t BgL_runner2977z00_3995;

											{	/* SawBbv/bbv-merge.scm 959 */
												obj_t BgL_l1820z00_3979;

												BgL_l1820z00_3979 =
													(((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(BgL_ez00_277))->BgL_typesz00);
												{	/* SawBbv/bbv-merge.scm 959 */
													obj_t BgL_head1822z00_3981;

													{	/* SawBbv/bbv-merge.scm 959 */
														long BgL_arg2975z00_3993;

														BgL_arg2975z00_3993 =
															BGl_typezd2siza7eze70z92zzsaw_bbvzd2mergezd2(CAR
															(BgL_l1820z00_3979));
														BgL_head1822z00_3981 =
															MAKE_YOUNG_PAIR(BINT(BgL_arg2975z00_3993), BNIL);
													}
													{	/* SawBbv/bbv-merge.scm 959 */
														obj_t BgL_g1825z00_3982;

														BgL_g1825z00_3982 = CDR(BgL_l1820z00_3979);
														{
															obj_t BgL_l1820z00_3984;
															obj_t BgL_tail1823z00_3985;

															BgL_l1820z00_3984 = BgL_g1825z00_3982;
															BgL_tail1823z00_3985 = BgL_head1822z00_3981;
														BgL_zc3z04anonymousza32968ze3z87_3986:
															if (NULLP(BgL_l1820z00_3984))
																{	/* SawBbv/bbv-merge.scm 959 */
																	BgL_runner2977z00_3995 = BgL_head1822z00_3981;
																}
															else
																{	/* SawBbv/bbv-merge.scm 959 */
																	obj_t BgL_newtail1824z00_3988;

																	{	/* SawBbv/bbv-merge.scm 959 */
																		long BgL_arg2973z00_3990;

																		BgL_arg2973z00_3990 =
																			BGl_typezd2siza7eze70z92zzsaw_bbvzd2mergezd2
																			(CAR(((obj_t) BgL_l1820z00_3984)));
																		BgL_newtail1824z00_3988 =
																			MAKE_YOUNG_PAIR(BINT(BgL_arg2973z00_3990),
																			BNIL);
																	}
																	SET_CDR(BgL_tail1823z00_3985,
																		BgL_newtail1824z00_3988);
																	{	/* SawBbv/bbv-merge.scm 959 */
																		obj_t BgL_arg2972z00_3989;

																		BgL_arg2972z00_3989 =
																			CDR(((obj_t) BgL_l1820z00_3984));
																		{
																			obj_t BgL_tail1823z00_10644;
																			obj_t BgL_l1820z00_10643;

																			BgL_l1820z00_10643 = BgL_arg2972z00_3989;
																			BgL_tail1823z00_10644 =
																				BgL_newtail1824z00_3988;
																			BgL_tail1823z00_3985 =
																				BgL_tail1823z00_10644;
																			BgL_l1820z00_3984 = BgL_l1820z00_10643;
																			goto
																				BgL_zc3z04anonymousza32968ze3z87_3986;
																		}
																	}
																}
														}
													}
												}
											}
											BgL_arg2966z00_3978 =
												BGl_zb2zb2zz__r4_numbers_6_5z00(BgL_runner2977z00_3995);
										}
										return (10L * (long) CINT(BgL_arg2966z00_3978));
					}}}
				else
					{	/* SawBbv/bbv-merge.scm 955 */
						long BgL_bz00_4001;

						BgL_bz00_4001 =
							(10L *
							bgl_list_length(
								(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_ez00_277))->
									BgL_typesz00)));
						if ((10L < BgL_bz00_4001))
							{	/* SawBbv/bbv-merge.scm 955 */
								return 10L;
							}
						else
							{	/* SawBbv/bbv-merge.scm 955 */
								return BgL_bz00_4001;
							}
					}
			}
		}

	}



/* type-size~0 */
	long BGl_typezd2siza7eze70z92zzsaw_bbvzd2mergezd2(obj_t BgL_typez00_4059)
	{
		{	/* SawBbv/bbv-merge.scm 951 */
			if ((BgL_typez00_4059 == BGl_za2bignumza2z00zztype_cachez00))
				{	/* SawBbv/bbv-merge.scm 949 */
					return 1L;
				}
			else
				{	/* SawBbv/bbv-merge.scm 949 */
					if ((BgL_typez00_4059 == BGl_za2realza2z00zztype_cachez00))
						{	/* SawBbv/bbv-merge.scm 950 */
							return 2L;
						}
					else
						{	/* SawBbv/bbv-merge.scm 950 */
							return 3L;
						}
				}
		}

	}



/* block-size */
	obj_t BGl_blockzd2siza7ez75zzsaw_bbvzd2mergezd2(BgL_blockz00_bglt
		BgL_bz00_278)
	{
		{	/* SawBbv/bbv-merge.scm 964 */
			{	/* SawBbv/bbv-merge.scm 966 */
				BgL_bbvzd2ctxzd2_bglt BgL_i1314z00_4064;

				{
					BgL_blocksz00_bglt BgL_auxz00_10657;

					{
						obj_t BgL_auxz00_10658;

						{	/* SawBbv/bbv-merge.scm 967 */
							BgL_objectz00_bglt BgL_tmpz00_10659;

							BgL_tmpz00_10659 = ((BgL_objectz00_bglt) BgL_bz00_278);
							BgL_auxz00_10658 = BGL_OBJECT_WIDENING(BgL_tmpz00_10659);
						}
						BgL_auxz00_10657 = ((BgL_blocksz00_bglt) BgL_auxz00_10658);
					}
					BgL_i1314z00_4064 =
						(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_10657))->BgL_ctxz00);
				}
				{	/* SawBbv/bbv-merge.scm 967 */
					obj_t BgL_runner3063z00_4081;

					{	/* SawBbv/bbv-merge.scm 967 */
						obj_t BgL_l1826z00_4065;

						BgL_l1826z00_4065 =
							(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_i1314z00_4064))->
							BgL_entriesz00);
						if (NULLP(BgL_l1826z00_4065))
							{	/* SawBbv/bbv-merge.scm 967 */
								BgL_runner3063z00_4081 = BNIL;
							}
						else
							{	/* SawBbv/bbv-merge.scm 967 */
								obj_t BgL_head1828z00_4067;

								{	/* SawBbv/bbv-merge.scm 967 */
									long BgL_arg3061z00_4079;

									{	/* SawBbv/bbv-merge.scm 967 */
										obj_t BgL_arg3062z00_4080;

										BgL_arg3062z00_4080 = CAR(BgL_l1826z00_4065);
										BgL_arg3061z00_4079 =
											BGl_entryzd2siza7ez75zzsaw_bbvzd2mergezd2(
											((BgL_bbvzd2ctxentryzd2_bglt) BgL_arg3062z00_4080));
									}
									BgL_head1828z00_4067 =
										MAKE_YOUNG_PAIR(BINT(BgL_arg3061z00_4079), BNIL);
								}
								{	/* SawBbv/bbv-merge.scm 967 */
									obj_t BgL_g1960z00_4068;

									BgL_g1960z00_4068 = CDR(BgL_l1826z00_4065);
									{
										obj_t BgL_l1826z00_4070;
										obj_t BgL_tail1829z00_4071;

										BgL_l1826z00_4070 = BgL_g1960z00_4068;
										BgL_tail1829z00_4071 = BgL_head1828z00_4067;
									BgL_zc3z04anonymousza33053ze3z87_4072:
										if (NULLP(BgL_l1826z00_4070))
											{	/* SawBbv/bbv-merge.scm 967 */
												BgL_runner3063z00_4081 = BgL_head1828z00_4067;
											}
										else
											{	/* SawBbv/bbv-merge.scm 967 */
												obj_t BgL_newtail1830z00_4074;

												{	/* SawBbv/bbv-merge.scm 967 */
													long BgL_arg3058z00_4076;

													{	/* SawBbv/bbv-merge.scm 967 */
														obj_t BgL_arg3059z00_4077;

														BgL_arg3059z00_4077 =
															CAR(((obj_t) BgL_l1826z00_4070));
														BgL_arg3058z00_4076 =
															BGl_entryzd2siza7ez75zzsaw_bbvzd2mergezd2(
															((BgL_bbvzd2ctxentryzd2_bglt)
																BgL_arg3059z00_4077));
													}
													BgL_newtail1830z00_4074 =
														MAKE_YOUNG_PAIR(BINT(BgL_arg3058z00_4076), BNIL);
												}
												SET_CDR(BgL_tail1829z00_4071, BgL_newtail1830z00_4074);
												{	/* SawBbv/bbv-merge.scm 967 */
													obj_t BgL_arg3055z00_4075;

													BgL_arg3055z00_4075 =
														CDR(((obj_t) BgL_l1826z00_4070));
													{
														obj_t BgL_tail1829z00_10685;
														obj_t BgL_l1826z00_10684;

														BgL_l1826z00_10684 = BgL_arg3055z00_4075;
														BgL_tail1829z00_10685 = BgL_newtail1830z00_4074;
														BgL_tail1829z00_4071 = BgL_tail1829z00_10685;
														BgL_l1826z00_4070 = BgL_l1826z00_10684;
														goto BgL_zc3z04anonymousza33053ze3z87_4072;
													}
												}
											}
									}
								}
							}
					}
					return BGl_zb2zb2zz__r4_numbers_6_5z00(BgL_runner3063z00_4081);
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawBbv/bbv-merge.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawBbv/bbv-merge.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawBbv/bbv-merge.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2mergezd2(void)
	{
		{	/* SawBbv/bbv-merge.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zzsaw_regsetz00(358079690L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(425659118L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2utilszd2(221709922L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(481635416L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2costzd2(414627252L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(288263219L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
			return
				BGl_modulezd2initializa7ationz75zzsaw_bbvzd2specializa7ez75(506937389L,
				BSTRING_TO_STRING(BGl_string3300z00zzsaw_bbvzd2mergezd2));
		}

	}

#ifdef __cplusplus
}
#endif
