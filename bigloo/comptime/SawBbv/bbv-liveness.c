/*===========================================================================*/
/*   (SawBbv/bbv-liveness.scm)                                               */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawBbv/bbv-liveness.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_SAW_BBVzd2LIVENESSzd2_TYPE_DEFINITIONS
#define BGL_BgL_SAW_BBVzd2LIVENESSzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_rtl_regzf2razf2_bgl
	{
		int BgL_numz00;
		obj_t BgL_colorz00;
		obj_t BgL_coalescez00;
		int BgL_occurrencesz00;
		obj_t BgL_interferez00;
		obj_t BgL_interfere2z00;
	}                      *BgL_rtl_regzf2razf2_bglt;

	typedef struct BgL_regsetz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_lengthz00;
		int BgL_msiza7eza7;
		obj_t BgL_regvz00;
		obj_t BgL_reglz00;
		obj_t BgL_stringz00;
	}                *BgL_regsetz00_bglt;

	typedef struct BgL_rtl_inszf2bbvzf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_outz00;
		obj_t BgL_inz00;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		obj_t BgL_z52hashz52;
	}                       *BgL_rtl_inszf2bbvzf2_bglt;

	typedef struct BgL_blockvz00_bgl
	{
		obj_t BgL_versionsz00;
		obj_t BgL_genericz00;
		long BgL_z52markz52;
		obj_t BgL_mergez00;
	}                *BgL_blockvz00_bglt;

	typedef struct BgL_blocksz00_bgl
	{
		long BgL_z52markz52;
		obj_t BgL_z52hashz52;
		obj_t BgL_z52blacklistz52;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		struct BgL_blockz00_bgl *BgL_parentz00;
		long BgL_gccntz00;
		long BgL_gcmarkz00;
		obj_t BgL_mblockz00;
		obj_t BgL_creatorz00;
		obj_t BgL_mergesz00;
		bool_t BgL_asleepz00;
	}                *BgL_blocksz00_bglt;

	typedef struct BgL_bbvzd2queuezd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_blocksz00;
		obj_t BgL_lastz00;
	}                     *BgL_bbvzd2queuezd2_bglt;

	typedef struct BgL_bbvzd2ctxzd2_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_idz00;
		obj_t BgL_entriesz00;
	}                   *BgL_bbvzd2ctxzd2_bglt;

	typedef struct BgL_bbvzd2ctxentryzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_rtl_regz00_bgl *BgL_regz00;
		obj_t BgL_typesz00;
		bool_t BgL_polarityz00;
		long BgL_countz00;
		obj_t BgL_valuez00;
		obj_t BgL_aliasesz00;
		obj_t BgL_initvalz00;
	}                        *BgL_bbvzd2ctxentryzd2_bglt;


#endif													// BGL_BgL_SAW_BBVzd2LIVENESSzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2ctxzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt, BgL_bbvzd2ctxzd2_bglt);
	static BgL_blockz00_bglt
		BGl_z62blockSzd2parentzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	static obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2;
	BGL_EXPORTED_DECL bool_t
		BGl_bbvzd2ctxentryzd2polarityz00zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_blockVzd2nilzd2zzsaw_bbvzd2livenesszd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2firstzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	static obj_t BGl_z62blockVzd2firstzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2defz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62rtl_inszf2bbvzd2defz42zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_z62blockSzd2asleepzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_bbvzd2livenesszd2 =
		BUNSPEC;
	static obj_t BGl_z62bbvzd2queuezd2lastz62zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2gccntzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		long);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt);
	static bool_t BGl_widenzd2bbvz12zc0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	extern bool_t BGl_regsetzd2addz12zc0zzsaw_regsetz00(BgL_regsetz00_bglt,
		BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_inszf2bbvzd2argsz42zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2typesz00zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	extern obj_t BGl_collectzd2registersz12zc0zzsaw_regutilsz00(obj_t);
	static BgL_blockz00_bglt BGl_z62blockVzd2nilzb0zzsaw_bbvzd2livenesszd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2argszd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt, obj_t);
	static obj_t BGl_z62blockSzd2z52hashze2zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2z52hashzd2setz12zd0zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bbvzd2ctxzf3z43zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_blockSzd2z52markz80zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62blockVzd2z52markze2zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t
		BGl_z62blockVzd2versionszd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62blockSzd2succszd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzf3z43zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_blockVzd2labelzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	static obj_t BGl_z62bbvzd2ctxentryzd2initvalz62zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2livenesszd2(int, int, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2gcmarkzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		long);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2livenesszd2(BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, int, obj_t, obj_t, int, obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2z52spillz10zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2livenesszd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62blockVzd2mergezb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static BgL_bbvzd2ctxzd2_bglt
		BGl_z62makezd2bbvzd2ctxz62zzsaw_bbvzd2livenesszd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2inz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62blockSzd2predszd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2outzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzd2polarityz62zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	static obj_t BGl_z62blockVzd2succszd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2z52spillzd2setz12zb2zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt, obj_t);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2versionszd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzd2countz62zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	static obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t BGl_z62blockSzd2labelzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockVzd2predszd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2initvalz00zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	static obj_t BGl_z62blockVzd2genericzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2z52hashz72zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_bbvzd2livenesszd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt);
	static BgL_bbvzd2queuezd2_bglt
		BGl_z62bbvzd2queuezd2nilz62zzsaw_bbvzd2livenesszd2(obj_t);
	static obj_t BGl_z62blockSzf3z91zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2outzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2livenesszd2(obj_t);
	static obj_t BGl_z62blockVzd2labelzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt);
	static obj_t BGl_objectzd2initzd2zzsaw_bbvzd2livenesszd2(void);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2funzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62blockSzd2z52markzd2setz12z22zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2argszd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62blockSzd2gcmarkzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2queuezd2lastz00zzsaw_bbvzd2livenesszd2(BgL_bbvzd2queuezd2_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2mergeszd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62blockVzd2z52markzd2setz12z22zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt, obj_t);
	static bool_t BGl_argszd2widenzd2bbvz12ze70zf5zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2outz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt);
	extern obj_t BGl_regsetz00zzsaw_regsetz00;
	static obj_t BGl_z62blockSzd2predszb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2outz42zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2funzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt, BgL_rtl_funz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2queuezd2blocksz00zzsaw_bbvzd2livenesszd2(BgL_bbvzd2queuezd2_bglt);
	static obj_t BGl_z62blockVzd2mergezd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockSzd2mblockzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_rtl_inszf2bbvzf3z01zzsaw_bbvzd2livenesszd2(obj_t);
	static obj_t BGl_z62blockSzd2asleepzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockVzf3z91zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_bbvzd2queuezf3z21zzsaw_bbvzd2livenesszd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2z52hashzd2setz12z40zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2valuez00zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	static BgL_bbvzd2ctxentryzd2_bglt
		BGl_z62makezd2bbvzd2ctxentryz62zzsaw_bbvzd2livenesszd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_blockSzd2ctxzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	static obj_t
		BGl_z62bbvzd2ctxentryzd2countzd2setz12za2zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2succszd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	static obj_t BGl_z62blockVzd2succszb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	static obj_t BGl_appendzd221011zd2zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_bbvzd2livenesszd2(void);
	static obj_t BGl_z62blockSzd2mblockzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t BGl_z62blockVzd2genericzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	extern bool_t BGl_regsetzd2unionza2z12z62zzsaw_regsetz00(BgL_regsetz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2genericzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL long
		BGl_bbvzd2ctxzd2idz00zzsaw_bbvzd2livenesszd2(BgL_bbvzd2ctxzd2_bglt);
	static obj_t BGl_z62bbvzd2queuezf3z43zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2livenessz12zc0zzsaw_bbvzd2livenesszd2(BgL_backendz00_bglt, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_blockSzd2parentzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxentryzd2_bglt
		BGl_bbvzd2ctxentryzd2nilz00zzsaw_bbvzd2livenesszd2(void);
	static obj_t
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt
		BGl_z62makezd2regsetzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_inszd2argsza2z70zzsaw_defsz00(BgL_rtl_insz00_bglt);
	static BgL_bbvzd2ctxzd2_bglt
		BGl_z62blockSzd2ctxzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2livenesszd2(void);
	static BgL_typez00_bglt
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_makezd2blockSzd2zzsaw_bbvzd2livenesszd2(int, obj_t, obj_t, obj_t, long,
		obj_t, obj_t, BgL_bbvzd2ctxzd2_bglt, BgL_blockz00_bglt, long, long, obj_t,
		obj_t, obj_t, bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxzd2idzd2setz12zc0zzsaw_bbvzd2livenesszd2(BgL_bbvzd2ctxzd2_bglt,
		long);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2predszd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_makezd2blockVzd2zzsaw_bbvzd2livenesszd2(int, obj_t, obj_t, obj_t, obj_t,
		obj_t, long, obj_t);
	static obj_t BGl_z62bbvzd2ctxzd2idzd2setz12za2zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2queuezd2blockszd2setz12zc0zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2queuezd2_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_bbvzd2ctxzf3z21zzsaw_bbvzd2livenesszd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2destzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt, obj_t);
	static BgL_bbvzd2queuezd2_bglt
		BGl_z62makezd2bbvzd2queuez62zzsaw_bbvzd2livenesszd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2z52blacklistzd2setz12z40zzsaw_bbvzd2livenesszd2
		(BgL_blockz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_blockSzd2asleepzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	static obj_t BGl_z62bbvzd2ctxzd2idz62zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62blockSzd2gccntzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzd2aliasesz62zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62bbvzd2ctxentryzd2initvalzd2setz12za2zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	static obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t BGl_z62blockSzd2firstzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_blockSzd2gccntzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	static obj_t BGl_z62bbvzd2queuezd2blocksz62zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2livenesszd2(BgL_regsetz00_bglt);
	static obj_t
		BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_rtl_inszf2bbvzd2ctxz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt);
	static BgL_bbvzd2ctxzd2_bglt
		BGl_z62rtl_inszf2bbvzd2ctxz42zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_bbvzd2ctxentryzf3z21zzsaw_bbvzd2livenesszd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2z52blacklistz80zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	extern BgL_regsetz00_bglt BGl_listzd2ze3regsetz31zzsaw_regsetz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_bbvzd2ctxentryzd2regz00zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	extern obj_t BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2;
	static obj_t
		BGl_z62bbvzd2ctxentryzd2aliaseszd2setz12za2zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2aliasesz00zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2defzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2firstzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t BGl_z62blockSzd2mergeszd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_insz00_bglt
		BGl_makezd2rtl_inszf2bbvz20zzsaw_bbvzd2livenesszd2(obj_t, obj_t, obj_t,
		BgL_rtl_funz00_bglt, obj_t, obj_t, obj_t, obj_t, BgL_bbvzd2ctxzd2_bglt,
		obj_t);
	static obj_t
		BGl_z62bbvzd2queuezd2blockszd2setz12za2zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2livenesszd2(BgL_regsetz00_bglt);
	extern obj_t BGl_blockSz00zzsaw_bbvzd2typeszd2;
	static obj_t BGl_z62blockSzd2z52markze2zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t BGl_z62blockSzd2creatorzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2destzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_blockVz00zzsaw_bbvzd2typeszd2;
	BGL_IMPORT obj_t BGl_appendzd2mapz12zc0zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2versionszd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_regsetz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL int
		BGl_blockSzd2labelzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	static obj_t
		BGl_z62bbvzd2queuezd2lastzd2setz12za2zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62blockVzd2versionszb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2firstzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t BGl_z62blockVzd2labelzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2z52spillz72zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2defzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2queuezd2_bglt
		BGl_bbvzd2queuezd2nilz00zzsaw_bbvzd2livenesszd2(void);
	static BgL_bbvzd2ctxentryzd2_bglt
		BGl_z62bbvzd2ctxentryzd2nilz62zzsaw_bbvzd2livenesszd2(obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2z52hashz10zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2initvalzd2setz12zc0zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2firstzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL BgL_bbvzd2queuezd2_bglt
		BGl_makezd2bbvzd2queuez00zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2livenesszd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2costzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2mergezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2utilszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_EXPORTED_DECL long
		BGl_bbvzd2ctxentryzd2countz00zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt);
	BGL_EXPORTED_DECL long
		BGl_blockSzd2gcmarkzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2genericzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_bbvzd2ctxzd2nilz00zzsaw_bbvzd2livenesszd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2aliaseszd2setz12zc0zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_blockSzd2nilzd2zzsaw_bbvzd2livenesszd2(void);
	static BgL_blockz00_bglt BGl_z62makezd2blockSzb0zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzd2typesz62zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2livenesszd2(obj_t);
	static BgL_blockz00_bglt BGl_z62makezd2blockVzb0zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2z52hashz80zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2z52spillzd2setz12zd0zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL long
		BGl_blockVzd2z52markz80zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	static obj_t
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2inzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2loczd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31617ze3ze5zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	static obj_t BGl_z62blockSzd2z52hashzd2setz12z22zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62bbvzd2ctxentryzd2regz62zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_funz00_bglt
		BGl_rtl_inszf2bbvzd2funz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2livenesszd2(void);
	BGL_EXPORTED_DECL bool_t BGl_blockSzf3zf3zzsaw_bbvzd2livenesszd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2mergezd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	static obj_t BGl_z62blockSzd2gcmarkzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static BgL_rtl_funz00_bglt
		BGl_z62rtl_inszf2bbvzd2funz42zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_makezd2bbvzd2ctxz00zzsaw_bbvzd2livenesszd2(long, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2livenesszd2(void);
	extern obj_t BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2;
	static obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2livenesszd2(void);
	static BgL_blockz00_bglt BGl_z62blockSzd2nilzb0zzsaw_bbvzd2livenesszd2(obj_t);
	static BgL_rtl_insz00_bglt
		BGl_z62makezd2rtl_inszf2bbvz42zzsaw_bbvzd2livenesszd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2creatorzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	extern obj_t BGl_regsetzd2forzd2eachz00zzsaw_regsetz00(obj_t,
		BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2mblockzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2asleepzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		bool_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2destz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2mblockzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2succszd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t
		BGl_z62blockSzd2z52blacklistzd2setz12z22zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzf3z63zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2loczd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt, int);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2predszd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2succszd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62blockSzd2succszb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2z52hashzd2setz12zb2zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_blockVzf3zf3zzsaw_bbvzd2livenesszd2(obj_t);
	BGL_IMPORT obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2labelzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		int);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2predszd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2queuezd2lastzd2setz12zc0zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2queuezd2_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_regsetzf3zf3zzsaw_bbvzd2livenesszd2(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_insz00_bglt
		BGl_rtl_inszf2bbvzd2nilz20zzsaw_bbvzd2livenesszd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxentryzd2countzd2setz12zc0zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt, long);
	static BgL_rtl_insz00_bglt
		BGl_z62rtl_inszf2bbvzd2nilz42zzsaw_bbvzd2livenesszd2(obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2labelzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		int);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2predszd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2livenesszd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62blockVzd2predszb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2z52markzd2setz12z40zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		long);
	static obj_t
		BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t);
	extern bool_t BGl_regsetzd2unionz12zc0zzsaw_regsetz00(BgL_regsetz00_bglt,
		BgL_regsetz00_bglt);
	extern bool_t
		BGl_rtl_inszd2fxovopzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static BgL_bbvzd2ctxzd2_bglt
		BGl_z62bbvzd2ctxzd2nilz62zzsaw_bbvzd2livenesszd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_z62blockSzd2z52blacklistze2zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2locz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_z62blockSzd2mergeszb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2locz42zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2ctxentryzd2valuez62zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2z52markzd2setz12z40zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		long);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2livenesszd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2argsz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2succszd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt);
	extern BgL_regsetz00_bglt BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00(obj_t);
	extern obj_t BGl_rtl_regzf2razf2zzsaw_regsetz00;
	static obj_t BGl_z62blockSzd2firstzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockVzd2mergezd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt, BgL_typez00_bglt);
	static obj_t
		BGl_z62bbvzd2ctxzd2entrieszd2setz12za2zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxzd2entriesz00zzsaw_bbvzd2livenesszd2(BgL_bbvzd2ctxzd2_bglt);
	static obj_t BGl_z62blockSzd2gccntzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2livenesszd2(BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszf2bbvzd2inzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt, obj_t);
	static obj_t BGl_z62bbvzd2ctxzd2entriesz62zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	static obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_inszf2bbvzd2destz42zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2livenesszd2(BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxentryzd2_bglt
		BGl_makezd2bbvzd2ctxentryz00zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt,
		obj_t, bool_t, long, obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62blockVzd2firstzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62rtl_inszf2bbvzd2ctxzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t);
	static BgL_regsetz00_bglt
		BGl_z62regsetzd2nilzb0zzsaw_bbvzd2livenesszd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt);
	extern obj_t BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2;
	BGL_EXPORTED_DECL obj_t
		BGl_blockSzd2mergeszd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt,
		obj_t);
	extern obj_t BGl_blockz00zzsaw_defsz00;
	static obj_t BGl_z62rtl_inszf2bbvzd2inz42zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	static obj_t BGl_livenesszd2insz12ze70z27zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_regsetz00_bglt,
		int);
	BGL_EXPORTED_DECL obj_t
		BGl_bbvzd2ctxzd2entrieszd2setz12zc0zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxzd2_bglt, obj_t);
	static obj_t BGl_getzd2argsze70z35zzsaw_bbvzd2livenesszd2(obj_t);
	static obj_t BGl_z62blockSzd2labelzb0zzsaw_bbvzd2livenesszd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2livenesszd2(obj_t,
		obj_t);
	static obj_t BGl_z62bbvzd2livenessz12za2zzsaw_bbvzd2livenesszd2(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2asleepzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2asle1995z00,
		BGl_z62blockSzd2asleepzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52hashzd2setz12zd2envz92zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2za752h1996za7,
		BGl_z62blockSzd2z52hashzd2setz12z22zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2nilzd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2queueza7d1997za7,
		BGl_z62bbvzd2queuezd2nilz62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2namezd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza71998za7,
		BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2argszd2setz12zd2envz32zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv1999z00,
		BGl_z62rtl_inszf2bbvzd2argszd2setz12z82zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2creatorzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2crea2000z00,
		BGl_z62blockSzd2creatorzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2mblockzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2mblo2001z00,
		BGl_z62blockSzd2mblockzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2nilzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762regsetza7d2nilza72002za7,
		BGl_z62regsetzd2nilzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2predszd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2pred2003z00,
		BGl_z62blockSzd2predszd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2inzd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2004z00,
		BGl_z62rtl_inszf2bbvzd2inz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2polarityzd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxentr2005z00,
		BGl_z62bbvzd2ctxentryzd2polarityz62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2firstzd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2firs2006z00,
		BGl_z62blockSzd2firstzd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2nilzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2nilza72007za7,
		BGl_z62blockVzd2nilzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52hashzd2envz52zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2za752h2008za7,
		BGl_z62blockSzd2z52hashze2zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2blockszd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2queueza7d2009za7,
		BGl_z62bbvzd2queuezd2blocksz62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2setz12zd2envz32zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72010za7,
		BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2inzd2setz12zd2envz32zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2011z00,
		BGl_z62rtl_inszf2bbvzd2inzd2setz12z82zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72012za7,
		BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2genericzd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2gene2013z00,
		BGl_z62blockVzd2genericzd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2setz12zd2envzc1zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72014za7,
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2livenesszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2setz12zd2envz32zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72015za7,
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2livenesszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2ctxzd2setz12zd2envz32zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2016z00,
		BGl_z62rtl_inszf2bbvzd2ctxzd2setz12z82zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72017za7,
		BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2z52hashzd2setz12zd2envz60zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2018z00,
		BGl_z62rtl_inszf2bbvzd2z52hashzd2setz12zd0zzsaw_bbvzd2livenesszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2setz12zd2envz32zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72019za7,
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2livenesszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2versionszd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2vers2020z00,
		BGl_z62blockVzd2versionszd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2regvzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762regsetza7d2regv2021z00,
		BGl_z62regsetzd2regvzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2bbvzd2ctxentryzd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762makeza7d2bbvza7d22022za7,
		BGl_z62makezd2bbvzd2ctxentryz62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2nilzd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2023z00,
		BGl_z62rtl_inszf2bbvzd2nilz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockSzf3zd2envz21zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7f3za791za72024z00,
		BGl_z62blockSzf3z91zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2initvalzd2setz12zd2envz12zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxentr2025z00,
		BGl_z62bbvzd2ctxentryzd2initvalzd2setz12za2zzsaw_bbvzd2livenesszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2typeszd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxentr2026z00,
		BGl_z62bbvzd2ctxentryzd2typesz62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762regsetza7d2leng2027z00,
		BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2firstzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2firs2028z00,
		BGl_z62blockVzd2firstzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2mblockzd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2mblo2029z00,
		BGl_z62blockSzd2mblockzd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2countzd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxentr2030z00,
		BGl_z62bbvzd2ctxentryzd2countz62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2msiza7ezd2envza7zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762regsetza7d2msiza72031za7,
		BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2nilzd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxza7d2n2032za7,
		BGl_z62bbvzd2ctxzd2nilz62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzf3zd2envzf3zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxentr2033z00,
		BGl_z62bbvzd2ctxentryzf3z43zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2idzd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxza7d2i2034za7,
		BGl_z62bbvzd2ctxzd2idz62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2aliaseszd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxentr2035z00,
		BGl_z62bbvzd2ctxentryzd2aliasesz62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2nilzd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxentr2036z00,
		BGl_z62bbvzd2ctxentryzd2nilz62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2gcmarkzd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2gcma2037z00,
		BGl_z62blockSzd2gcmarkzd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72038za7,
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2setz12zd2envz32zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72039za7,
		BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2outzd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2040z00,
		BGl_z62rtl_inszf2bbvzd2outz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2nilzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2nilza72041za7,
		BGl_z62blockSzd2nilzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2rtl_inszf2bbvzd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762makeza7d2rtl_in2042z00,
		BGl_z62makezd2rtl_inszf2bbvz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 10);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2firstzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2firs2043z00,
		BGl_z62blockSzd2firstzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72044za7,
		BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2z52markzd2envz52zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2za752m2045za7,
		BGl_z62blockVzd2z52markze2zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2labelzd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2labe2046z00,
		BGl_z62blockSzd2labelzd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2versionszd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2vers2047z00,
		BGl_z62blockVzd2versionszb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2z52hashzd2envza0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2048z00,
		BGl_z62rtl_inszf2bbvzd2z52hashz10zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2mergezd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2merg2049z00,
		BGl_z62blockVzd2mergezb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52markzd2setz12zd2envz92zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2za752m2050za7,
		BGl_z62blockSzd2z52markzd2setz12z22zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2bbvzd2queuezd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762makeza7d2bbvza7d22051za7,
		BGl_z62makezd2bbvzd2queuez62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52markzd2envz52zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2za752m2052za7,
		BGl_z62blockSzd2z52markze2zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2ctxzd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2053z00,
		BGl_z62rtl_inszf2bbvzd2ctxz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2succszd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2succ2054z00,
		BGl_z62blockSzd2succszd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2livenessz12zd2envz12zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2livenes2055z00,
		BGl_z62bbvzd2livenessz12za2zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2nilzd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72056za7,
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72057za7,
		BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2blockSzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762makeza7d2blocks2058z00,
		BGl_z62makezd2blockSzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 15);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2numzd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72059za7,
		BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2blockszd2setz12zd2envz12zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2queueza7d2060za7,
		BGl_z62bbvzd2queuezd2blockszd2setz12za2zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2entrieszd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxza7d2e2061za7,
		BGl_z62bbvzd2ctxzd2entriesz62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2predszd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2pred2062z00,
		BGl_z62blockVzd2predszd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762regsetza7d2stri2063z00,
		BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2firstzd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2firs2064z00,
		BGl_z62blockVzd2firstzd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2valuezd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxentr2065z00,
		BGl_z62bbvzd2ctxentryzd2valuez62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2gccntzd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2gccn2066z00,
		BGl_z62blockSzd2gccntzd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2setz12zd2envz32zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72067za7,
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2envz01zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72068za7,
		BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2ctxzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2ctxza72069za7,
		BGl_z62blockSzd2ctxzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2outzd2setz12zd2envz32zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2070z00,
		BGl_z62rtl_inszf2bbvzd2outzd2setz12z82zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762regsetza7d2stri2071z00,
		BGl_z62regsetzd2stringzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	extern obj_t BGl_collectzd2registerz12zd2envz12zzsaw_regutilsz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2asleepzd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2asle2072z00,
		BGl_z62blockSzd2asleepzd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2gcmarkzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2gcma2073z00,
		BGl_z62blockSzd2gcmarkzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52blacklistzd2envz52zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2za752b2074za7,
		BGl_z62blockSzd2z52blacklistze2zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2setz12zd2envz32zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72075za7,
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2livenesszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2rtl_regzf2razd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762makeza7d2rtl_re2076z00,
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2argszd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2077z00,
		BGl_z62rtl_inszf2bbvzd2argsz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2idzd2setz12zd2envz12zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxza7d2i2078za7,
		BGl_z62bbvzd2ctxzd2idzd2setz12za2zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razf3zd2envzd3zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72079za7,
		BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2z52spillzd2setz12zd2envz60zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2080z00,
		BGl_z62rtl_inszf2bbvzd2z52spillzd2setz12zd0zzsaw_bbvzd2livenesszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762regsetza7d2leng2081z00,
		BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzf3zd2envzd3zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2082z00,
		BGl_z62rtl_inszf2bbvzf3z63zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2lastzd2setz12zd2envz12zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2queueza7d2083za7,
		BGl_z62bbvzd2queuezd2lastzd2setz12za2zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezf3zd2envzf3zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2queueza7f2084za7,
		BGl_z62bbvzd2queuezf3z43zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2initvalzd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxentr2085z00,
		BGl_z62bbvzd2ctxentryzd2initvalz62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_string1994za700za7za7s2086za7, "saw_bbv-liveness", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72087za7,
		BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72088za7,
		BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2gccntzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2gccn2089z00,
		BGl_z62blockSzd2gccntzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2genericzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2gene2090z00,
		BGl_z62blockVzd2genericzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2hardwarezd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72091za7,
		BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2loczd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2092z00,
		BGl_z62rtl_inszf2bbvzd2locz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2aliaseszd2setz12zd2envz12zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxentr2093z00,
		BGl_z62bbvzd2ctxentryzd2aliaseszd2setz12za2zzsaw_bbvzd2livenesszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2regsetzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762makeza7d2regset2094z00,
		BGl_z62makezd2regsetzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2succszd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2succ2095z00,
		BGl_z62blockVzd2succszb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2funzd2setz12zd2envz32zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2096z00,
		BGl_z62rtl_inszf2bbvzd2funzd2setz12z82zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2destzd2setz12zd2envz32zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2097z00,
		BGl_z62rtl_inszf2bbvzd2destzd2setz12z82zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockVzf3zd2envz21zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7f3za791za72098z00,
		BGl_z62blockVzf3z91zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2z52blacklistzd2setz12zd2envz92zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2za752b2099za7,
		BGl_z62blockSzd2z52blacklistzd2setz12z22zzsaw_bbvzd2livenesszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2countzd2setz12zd2envz12zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxentr2100z00,
		BGl_z62bbvzd2ctxentryzd2countzd2setz12za2zzsaw_bbvzd2livenesszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2z52spillzd2envza0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2101z00,
		BGl_z62rtl_inszf2bbvzd2z52spillz10zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2defzd2setz12zd2envz32zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2102z00,
		BGl_z62rtl_inszf2bbvzd2defzd2setz12z82zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2bbvzd2ctxzd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762makeza7d2bbvza7d22103za7,
		BGl_z62makezd2bbvzd2ctxz62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2labelzd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2labe2104z00,
		BGl_z62blockVzd2labelzd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2succszd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2succ2105z00,
		BGl_z62blockSzd2succszb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2entrieszd2setz12zd2envz12zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxza7d2e2106za7,
		BGl_z62bbvzd2ctxzd2entrieszd2setz12za2zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2destzd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2107z00,
		BGl_z62rtl_inszf2bbvzd2destz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2loczd2setz12zd2envz32zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2108z00,
		BGl_z62rtl_inszf2bbvzd2loczd2setz12z82zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2queuezd2lastzd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2queueza7d2109za7,
		BGl_z62bbvzd2queuezd2lastz62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2labelzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2labe2110z00,
		BGl_z62blockVzd2labelzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxentryzd2regzd2envzd2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxentr2111z00,
		BGl_z62bbvzd2ctxentryzd2regz62zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2z52markzd2setz12zd2envz92zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2za752m2112za7,
		BGl_z62blockVzd2z52markzd2setz12z22zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2succszd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2succ2113z00,
		BGl_z62blockVzd2succszd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2predszd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2pred2114z00,
		BGl_z62blockVzd2predszb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2blockVzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762makeza7d2blockv2115z00,
		BGl_z62makezd2blockVzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockVzd2mergezd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blockvza7d2merg2116z00,
		BGl_z62blockVzd2mergezd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzf3zd2envz21zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762regsetza7f3za791za72117z00,
		BGl_z62regsetzf3z91zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2labelzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2labe2118z00,
		BGl_z62blockSzd2labelzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2funzd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2119z00,
		BGl_z62rtl_inszf2bbvzd2funz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2keyzd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72120za7,
		BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2mergeszd2setz12zd2envzc0zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2merg2121z00,
		BGl_z62blockSzd2mergeszd2setz12z70zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2predszd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2pred2122z00,
		BGl_z62blockSzd2predszb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2mergeszd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2merg2123z00,
		BGl_z62blockSzd2mergeszb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockSzd2parentzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762blocksza7d2pare2124z00,
		BGl_z62blockSzd2parentzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2setz12zd2envz32zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_regza7f2raza72125za7,
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2livenesszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzf3zd2envzf3zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762bbvza7d2ctxza7f3za72126z00,
		BGl_z62bbvzd2ctxzf3z43zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszf2bbvzd2defzd2envzf2zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762rtl_insza7f2bbv2127z00,
		BGl_z62rtl_inszf2bbvzd2defz42zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2reglzd2envz00zzsaw_bbvzd2livenesszd2,
		BgL_bgl_za762regsetza7d2regl2128z00,
		BGl_z62regsetzd2reglzb0zzsaw_bbvzd2livenesszd2, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_bbvzd2livenesszd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2livenesszd2(long
		BgL_checksumz00_4166, char *BgL_fromz00_4167)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_bbvzd2livenesszd2))
				{
					BGl_requirezd2initializa7ationz75zzsaw_bbvzd2livenesszd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_bbvzd2livenesszd2();
					BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2livenesszd2();
					BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2livenesszd2();
					return BGl_methodzd2initzd2zzsaw_bbvzd2livenesszd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2livenesszd2(void)
	{
		{	/* SawBbv/bbv-liveness.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_bbv-liveness");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_bbv-liveness");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"saw_bbv-liveness");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_bbv-liveness");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"saw_bbv-liveness");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_bbv-liveness");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_bbv-liveness");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_bbv-liveness");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_bbv-liveness");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_bbv-liveness");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2livenesszd2(void)
	{
		{	/* SawBbv/bbv-liveness.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzsaw_bbvzd2livenesszd2(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_2146;

				BgL_headz00_2146 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2147;
					obj_t BgL_tailz00_2148;

					BgL_prevz00_2147 = BgL_headz00_2146;
					BgL_tailz00_2148 = BgL_l1z00_1;
				BgL_loopz00_2149:
					if (PAIRP(BgL_tailz00_2148))
						{
							obj_t BgL_newzd2prevzd2_2151;

							BgL_newzd2prevzd2_2151 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2148), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2147, BgL_newzd2prevzd2_2151);
							{
								obj_t BgL_tailz00_4193;
								obj_t BgL_prevz00_4192;

								BgL_prevz00_4192 = BgL_newzd2prevzd2_2151;
								BgL_tailz00_4193 = CDR(BgL_tailz00_2148);
								BgL_tailz00_2148 = BgL_tailz00_4193;
								BgL_prevz00_2147 = BgL_prevz00_4192;
								goto BgL_loopz00_2149;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_2146);
				}
			}
		}

	}



/* make-rtl_reg/ra */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2livenesszd2(BgL_typez00_bglt
		BgL_type1179z00_3, obj_t BgL_var1180z00_4, obj_t BgL_onexprzf31181zf3_5,
		obj_t BgL_name1182z00_6, obj_t BgL_key1183z00_7,
		obj_t BgL_hardware1184z00_8, int BgL_num1185z00_9,
		obj_t BgL_color1186z00_10, obj_t BgL_coalesce1187z00_11,
		int BgL_occurrences1188z00_12, obj_t BgL_interfere1189z00_13,
		obj_t BgL_interfere21190z00_14)
	{
		{	/* SawMill/regset.sch 55 */
			{	/* SawMill/regset.sch 55 */
				BgL_rtl_regz00_bglt BgL_new1166z00_3994;

				{	/* SawMill/regset.sch 55 */
					BgL_rtl_regz00_bglt BgL_tmp1164z00_3995;
					BgL_rtl_regzf2razf2_bglt BgL_wide1165z00_3996;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_4196;

						{	/* SawMill/regset.sch 55 */
							BgL_rtl_regz00_bglt BgL_new1163z00_3997;

							BgL_new1163z00_3997 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/regset.sch 55 */
								long BgL_arg1509z00_3998;

								BgL_arg1509z00_3998 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1163z00_3997),
									BgL_arg1509z00_3998);
							}
							{	/* SawMill/regset.sch 55 */
								BgL_objectz00_bglt BgL_tmpz00_4201;

								BgL_tmpz00_4201 = ((BgL_objectz00_bglt) BgL_new1163z00_3997);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4201, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1163z00_3997);
							BgL_auxz00_4196 = BgL_new1163z00_3997;
						}
						BgL_tmp1164z00_3995 = ((BgL_rtl_regz00_bglt) BgL_auxz00_4196);
					}
					BgL_wide1165z00_3996 =
						((BgL_rtl_regzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regzf2razf2_bgl))));
					{	/* SawMill/regset.sch 55 */
						obj_t BgL_auxz00_4209;
						BgL_objectz00_bglt BgL_tmpz00_4207;

						BgL_auxz00_4209 = ((obj_t) BgL_wide1165z00_3996);
						BgL_tmpz00_4207 = ((BgL_objectz00_bglt) BgL_tmp1164z00_3995);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4207, BgL_auxz00_4209);
					}
					((BgL_objectz00_bglt) BgL_tmp1164z00_3995);
					{	/* SawMill/regset.sch 55 */
						long BgL_arg1502z00_3999;

						BgL_arg1502z00_3999 =
							BGL_CLASS_NUM(BGl_rtl_regzf2razf2zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1164z00_3995), BgL_arg1502z00_3999);
					}
					BgL_new1166z00_3994 = ((BgL_rtl_regz00_bglt) BgL_tmp1164z00_3995);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1166z00_3994)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1179z00_3), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3994)))->BgL_varz00) =
					((obj_t) BgL_var1180z00_4), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3994)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31181zf3_5), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3994)))->BgL_namez00) =
					((obj_t) BgL_name1182z00_6), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3994)))->BgL_keyz00) =
					((obj_t) BgL_key1183z00_7), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3994)))->BgL_debugnamez00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3994)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1184z00_8), BUNSPEC);
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4231;

					{
						obj_t BgL_auxz00_4232;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_4233;

							BgL_tmpz00_4233 = ((BgL_objectz00_bglt) BgL_new1166z00_3994);
							BgL_auxz00_4232 = BGL_OBJECT_WIDENING(BgL_tmpz00_4233);
						}
						BgL_auxz00_4231 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4232);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4231))->BgL_numz00) =
						((int) BgL_num1185z00_9), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4238;

					{
						obj_t BgL_auxz00_4239;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_4240;

							BgL_tmpz00_4240 = ((BgL_objectz00_bglt) BgL_new1166z00_3994);
							BgL_auxz00_4239 = BGL_OBJECT_WIDENING(BgL_tmpz00_4240);
						}
						BgL_auxz00_4238 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4239);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4238))->
							BgL_colorz00) = ((obj_t) BgL_color1186z00_10), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4245;

					{
						obj_t BgL_auxz00_4246;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_4247;

							BgL_tmpz00_4247 = ((BgL_objectz00_bglt) BgL_new1166z00_3994);
							BgL_auxz00_4246 = BGL_OBJECT_WIDENING(BgL_tmpz00_4247);
						}
						BgL_auxz00_4245 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4246);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4245))->
							BgL_coalescez00) = ((obj_t) BgL_coalesce1187z00_11), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4252;

					{
						obj_t BgL_auxz00_4253;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_4254;

							BgL_tmpz00_4254 = ((BgL_objectz00_bglt) BgL_new1166z00_3994);
							BgL_auxz00_4253 = BGL_OBJECT_WIDENING(BgL_tmpz00_4254);
						}
						BgL_auxz00_4252 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4253);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4252))->
							BgL_occurrencesz00) = ((int) BgL_occurrences1188z00_12), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4259;

					{
						obj_t BgL_auxz00_4260;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_4261;

							BgL_tmpz00_4261 = ((BgL_objectz00_bglt) BgL_new1166z00_3994);
							BgL_auxz00_4260 = BGL_OBJECT_WIDENING(BgL_tmpz00_4261);
						}
						BgL_auxz00_4259 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4260);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4259))->
							BgL_interferez00) = ((obj_t) BgL_interfere1189z00_13), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4266;

					{
						obj_t BgL_auxz00_4267;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_4268;

							BgL_tmpz00_4268 = ((BgL_objectz00_bglt) BgL_new1166z00_3994);
							BgL_auxz00_4267 = BGL_OBJECT_WIDENING(BgL_tmpz00_4268);
						}
						BgL_auxz00_4266 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4267);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4266))->
							BgL_interfere2z00) = ((obj_t) BgL_interfere21190z00_14), BUNSPEC);
				}
				return BgL_new1166z00_3994;
			}
		}

	}



/* &make-rtl_reg/ra */
	BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3623, obj_t BgL_type1179z00_3624, obj_t BgL_var1180z00_3625,
		obj_t BgL_onexprzf31181zf3_3626, obj_t BgL_name1182z00_3627,
		obj_t BgL_key1183z00_3628, obj_t BgL_hardware1184z00_3629,
		obj_t BgL_num1185z00_3630, obj_t BgL_color1186z00_3631,
		obj_t BgL_coalesce1187z00_3632, obj_t BgL_occurrences1188z00_3633,
		obj_t BgL_interfere1189z00_3634, obj_t BgL_interfere21190z00_3635)
	{
		{	/* SawMill/regset.sch 55 */
			return
				BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2livenesszd2(
				((BgL_typez00_bglt) BgL_type1179z00_3624), BgL_var1180z00_3625,
				BgL_onexprzf31181zf3_3626, BgL_name1182z00_3627, BgL_key1183z00_3628,
				BgL_hardware1184z00_3629, CINT(BgL_num1185z00_3630),
				BgL_color1186z00_3631, BgL_coalesce1187z00_3632,
				CINT(BgL_occurrences1188z00_3633), BgL_interfere1189z00_3634,
				BgL_interfere21190z00_3635);
		}

	}



/* rtl_reg/ra? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2livenesszd2(obj_t
		BgL_objz00_15)
	{
		{	/* SawMill/regset.sch 56 */
			{	/* SawMill/regset.sch 56 */
				obj_t BgL_classz00_4000;

				BgL_classz00_4000 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_15))
					{	/* SawMill/regset.sch 56 */
						BgL_objectz00_bglt BgL_arg1807z00_4001;

						BgL_arg1807z00_4001 = (BgL_objectz00_bglt) (BgL_objz00_15);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 56 */
								long BgL_idxz00_4002;

								BgL_idxz00_4002 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4001);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4002 + 2L)) == BgL_classz00_4000);
							}
						else
							{	/* SawMill/regset.sch 56 */
								bool_t BgL_res1976z00_4005;

								{	/* SawMill/regset.sch 56 */
									obj_t BgL_oclassz00_4006;

									{	/* SawMill/regset.sch 56 */
										obj_t BgL_arg1815z00_4007;
										long BgL_arg1816z00_4008;

										BgL_arg1815z00_4007 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 56 */
											long BgL_arg1817z00_4009;

											BgL_arg1817z00_4009 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4001);
											BgL_arg1816z00_4008 = (BgL_arg1817z00_4009 - OBJECT_TYPE);
										}
										BgL_oclassz00_4006 =
											VECTOR_REF(BgL_arg1815z00_4007, BgL_arg1816z00_4008);
									}
									{	/* SawMill/regset.sch 56 */
										bool_t BgL__ortest_1115z00_4010;

										BgL__ortest_1115z00_4010 =
											(BgL_classz00_4000 == BgL_oclassz00_4006);
										if (BgL__ortest_1115z00_4010)
											{	/* SawMill/regset.sch 56 */
												BgL_res1976z00_4005 = BgL__ortest_1115z00_4010;
											}
										else
											{	/* SawMill/regset.sch 56 */
												long BgL_odepthz00_4011;

												{	/* SawMill/regset.sch 56 */
													obj_t BgL_arg1804z00_4012;

													BgL_arg1804z00_4012 = (BgL_oclassz00_4006);
													BgL_odepthz00_4011 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4012);
												}
												if ((2L < BgL_odepthz00_4011))
													{	/* SawMill/regset.sch 56 */
														obj_t BgL_arg1802z00_4013;

														{	/* SawMill/regset.sch 56 */
															obj_t BgL_arg1803z00_4014;

															BgL_arg1803z00_4014 = (BgL_oclassz00_4006);
															BgL_arg1802z00_4013 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4014,
																2L);
														}
														BgL_res1976z00_4005 =
															(BgL_arg1802z00_4013 == BgL_classz00_4000);
													}
												else
													{	/* SawMill/regset.sch 56 */
														BgL_res1976z00_4005 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1976z00_4005;
							}
					}
				else
					{	/* SawMill/regset.sch 56 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_reg/ra? */
	obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3636,
		obj_t BgL_objz00_3637)
	{
		{	/* SawMill/regset.sch 56 */
			return
				BBOOL(BGl_rtl_regzf2razf3z01zzsaw_bbvzd2livenesszd2(BgL_objz00_3637));
		}

	}



/* rtl_reg/ra-nil */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2livenesszd2(void)
	{
		{	/* SawMill/regset.sch 57 */
			{	/* SawMill/regset.sch 57 */
				obj_t BgL_classz00_2835;

				BgL_classz00_2835 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				{	/* SawMill/regset.sch 57 */
					obj_t BgL__ortest_1117z00_2836;

					BgL__ortest_1117z00_2836 = BGL_CLASS_NIL(BgL_classz00_2835);
					if (CBOOL(BgL__ortest_1117z00_2836))
						{	/* SawMill/regset.sch 57 */
							return ((BgL_rtl_regz00_bglt) BgL__ortest_1117z00_2836);
						}
					else
						{	/* SawMill/regset.sch 57 */
							return
								((BgL_rtl_regz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2835));
						}
				}
			}
		}

	}



/* &rtl_reg/ra-nil */
	BgL_rtl_regz00_bglt BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3638)
	{
		{	/* SawMill/regset.sch 57 */
			return BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2livenesszd2();
		}

	}



/* rtl_reg/ra-interfere2 */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt
		BgL_oz00_16)
	{
		{	/* SawMill/regset.sch 58 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4308;

				{
					obj_t BgL_auxz00_4309;

					{	/* SawMill/regset.sch 58 */
						BgL_objectz00_bglt BgL_tmpz00_4310;

						BgL_tmpz00_4310 = ((BgL_objectz00_bglt) BgL_oz00_16);
						BgL_auxz00_4309 = BGL_OBJECT_WIDENING(BgL_tmpz00_4310);
					}
					BgL_auxz00_4308 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4309);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4308))->
					BgL_interfere2z00);
			}
		}

	}



/* &rtl_reg/ra-interfere2 */
	obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3639, obj_t BgL_oz00_3640)
	{
		{	/* SawMill/regset.sch 58 */
			return
				BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3640));
		}

	}



/* rtl_reg/ra-interfere2-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt BgL_oz00_17, obj_t BgL_vz00_18)
	{
		{	/* SawMill/regset.sch 59 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4317;

				{
					obj_t BgL_auxz00_4318;

					{	/* SawMill/regset.sch 59 */
						BgL_objectz00_bglt BgL_tmpz00_4319;

						BgL_tmpz00_4319 = ((BgL_objectz00_bglt) BgL_oz00_17);
						BgL_auxz00_4318 = BGL_OBJECT_WIDENING(BgL_tmpz00_4319);
					}
					BgL_auxz00_4317 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4318);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4317))->
						BgL_interfere2z00) = ((obj_t) BgL_vz00_18), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere2-set! */
	obj_t
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3641, obj_t BgL_oz00_3642, obj_t BgL_vz00_3643)
	{
		{	/* SawMill/regset.sch 59 */
			return
				BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3642), BgL_vz00_3643);
		}

	}



/* rtl_reg/ra-interfere */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt
		BgL_oz00_19)
	{
		{	/* SawMill/regset.sch 60 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4326;

				{
					obj_t BgL_auxz00_4327;

					{	/* SawMill/regset.sch 60 */
						BgL_objectz00_bglt BgL_tmpz00_4328;

						BgL_tmpz00_4328 = ((BgL_objectz00_bglt) BgL_oz00_19);
						BgL_auxz00_4327 = BGL_OBJECT_WIDENING(BgL_tmpz00_4328);
					}
					BgL_auxz00_4326 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4327);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4326))->
					BgL_interferez00);
			}
		}

	}



/* &rtl_reg/ra-interfere */
	obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3644, obj_t BgL_oz00_3645)
	{
		{	/* SawMill/regset.sch 60 */
			return
				BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3645));
		}

	}



/* rtl_reg/ra-interfere-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt BgL_oz00_20, obj_t BgL_vz00_21)
	{
		{	/* SawMill/regset.sch 61 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4335;

				{
					obj_t BgL_auxz00_4336;

					{	/* SawMill/regset.sch 61 */
						BgL_objectz00_bglt BgL_tmpz00_4337;

						BgL_tmpz00_4337 = ((BgL_objectz00_bglt) BgL_oz00_20);
						BgL_auxz00_4336 = BGL_OBJECT_WIDENING(BgL_tmpz00_4337);
					}
					BgL_auxz00_4335 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4336);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4335))->
						BgL_interferez00) = ((obj_t) BgL_vz00_21), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere-set! */
	obj_t BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3646, obj_t BgL_oz00_3647, obj_t BgL_vz00_3648)
	{
		{	/* SawMill/regset.sch 61 */
			return
				BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3647), BgL_vz00_3648);
		}

	}



/* rtl_reg/ra-occurrences */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt
		BgL_oz00_22)
	{
		{	/* SawMill/regset.sch 62 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4344;

				{
					obj_t BgL_auxz00_4345;

					{	/* SawMill/regset.sch 62 */
						BgL_objectz00_bglt BgL_tmpz00_4346;

						BgL_tmpz00_4346 = ((BgL_objectz00_bglt) BgL_oz00_22);
						BgL_auxz00_4345 = BGL_OBJECT_WIDENING(BgL_tmpz00_4346);
					}
					BgL_auxz00_4344 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4345);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4344))->
					BgL_occurrencesz00);
			}
		}

	}



/* &rtl_reg/ra-occurrences */
	obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3649, obj_t BgL_oz00_3650)
	{
		{	/* SawMill/regset.sch 62 */
			return
				BINT(BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2livenesszd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_3650)));
		}

	}



/* rtl_reg/ra-occurrences-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt BgL_oz00_23, int BgL_vz00_24)
	{
		{	/* SawMill/regset.sch 63 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4354;

				{
					obj_t BgL_auxz00_4355;

					{	/* SawMill/regset.sch 63 */
						BgL_objectz00_bglt BgL_tmpz00_4356;

						BgL_tmpz00_4356 = ((BgL_objectz00_bglt) BgL_oz00_23);
						BgL_auxz00_4355 = BGL_OBJECT_WIDENING(BgL_tmpz00_4356);
					}
					BgL_auxz00_4354 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4355);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4354))->
						BgL_occurrencesz00) = ((int) BgL_vz00_24), BUNSPEC);
		}}

	}



/* &rtl_reg/ra-occurrences-set! */
	obj_t
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3651, obj_t BgL_oz00_3652, obj_t BgL_vz00_3653)
	{
		{	/* SawMill/regset.sch 63 */
			return
				BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3652), CINT(BgL_vz00_3653));
		}

	}



/* rtl_reg/ra-coalesce */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt
		BgL_oz00_25)
	{
		{	/* SawMill/regset.sch 64 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4364;

				{
					obj_t BgL_auxz00_4365;

					{	/* SawMill/regset.sch 64 */
						BgL_objectz00_bglt BgL_tmpz00_4366;

						BgL_tmpz00_4366 = ((BgL_objectz00_bglt) BgL_oz00_25);
						BgL_auxz00_4365 = BGL_OBJECT_WIDENING(BgL_tmpz00_4366);
					}
					BgL_auxz00_4364 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4365);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4364))->
					BgL_coalescez00);
			}
		}

	}



/* &rtl_reg/ra-coalesce */
	obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3654, obj_t BgL_oz00_3655)
	{
		{	/* SawMill/regset.sch 64 */
			return
				BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3655));
		}

	}



/* rtl_reg/ra-coalesce-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt BgL_oz00_26, obj_t BgL_vz00_27)
	{
		{	/* SawMill/regset.sch 65 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4373;

				{
					obj_t BgL_auxz00_4374;

					{	/* SawMill/regset.sch 65 */
						BgL_objectz00_bglt BgL_tmpz00_4375;

						BgL_tmpz00_4375 = ((BgL_objectz00_bglt) BgL_oz00_26);
						BgL_auxz00_4374 = BGL_OBJECT_WIDENING(BgL_tmpz00_4375);
					}
					BgL_auxz00_4373 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4374);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4373))->
						BgL_coalescez00) = ((obj_t) BgL_vz00_27), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-coalesce-set! */
	obj_t BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3656, obj_t BgL_oz00_3657, obj_t BgL_vz00_3658)
	{
		{	/* SawMill/regset.sch 65 */
			return
				BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3657), BgL_vz00_3658);
		}

	}



/* rtl_reg/ra-color */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt
		BgL_oz00_28)
	{
		{	/* SawMill/regset.sch 66 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4382;

				{
					obj_t BgL_auxz00_4383;

					{	/* SawMill/regset.sch 66 */
						BgL_objectz00_bglt BgL_tmpz00_4384;

						BgL_tmpz00_4384 = ((BgL_objectz00_bglt) BgL_oz00_28);
						BgL_auxz00_4383 = BGL_OBJECT_WIDENING(BgL_tmpz00_4384);
					}
					BgL_auxz00_4382 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4383);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4382))->BgL_colorz00);
			}
		}

	}



/* &rtl_reg/ra-color */
	obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3659, obj_t BgL_oz00_3660)
	{
		{	/* SawMill/regset.sch 66 */
			return
				BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3660));
		}

	}



/* rtl_reg/ra-color-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt BgL_oz00_29, obj_t BgL_vz00_30)
	{
		{	/* SawMill/regset.sch 67 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4391;

				{
					obj_t BgL_auxz00_4392;

					{	/* SawMill/regset.sch 67 */
						BgL_objectz00_bglt BgL_tmpz00_4393;

						BgL_tmpz00_4393 = ((BgL_objectz00_bglt) BgL_oz00_29);
						BgL_auxz00_4392 = BGL_OBJECT_WIDENING(BgL_tmpz00_4393);
					}
					BgL_auxz00_4391 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4392);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4391))->
						BgL_colorz00) = ((obj_t) BgL_vz00_30), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-color-set! */
	obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3661, obj_t BgL_oz00_3662, obj_t BgL_vz00_3663)
	{
		{	/* SawMill/regset.sch 67 */
			return
				BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3662), BgL_vz00_3663);
		}

	}



/* rtl_reg/ra-num */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt
		BgL_oz00_31)
	{
		{	/* SawMill/regset.sch 68 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4400;

				{
					obj_t BgL_auxz00_4401;

					{	/* SawMill/regset.sch 68 */
						BgL_objectz00_bglt BgL_tmpz00_4402;

						BgL_tmpz00_4402 = ((BgL_objectz00_bglt) BgL_oz00_31);
						BgL_auxz00_4401 = BGL_OBJECT_WIDENING(BgL_tmpz00_4402);
					}
					BgL_auxz00_4400 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4401);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4400))->BgL_numz00);
			}
		}

	}



/* &rtl_reg/ra-num */
	obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3664, obj_t BgL_oz00_3665)
	{
		{	/* SawMill/regset.sch 68 */
			return
				BINT(BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2livenesszd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_3665)));
		}

	}



/* rtl_reg/ra-hardware */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt
		BgL_oz00_34)
	{
		{	/* SawMill/regset.sch 70 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_34)))->BgL_hardwarez00);
		}

	}



/* &rtl_reg/ra-hardware */
	obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3666, obj_t BgL_oz00_3667)
	{
		{	/* SawMill/regset.sch 70 */
			return
				BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3667));
		}

	}



/* rtl_reg/ra-key */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt
		BgL_oz00_37)
	{
		{	/* SawMill/regset.sch 72 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_37)))->BgL_keyz00);
		}

	}



/* &rtl_reg/ra-key */
	obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3668, obj_t BgL_oz00_3669)
	{
		{	/* SawMill/regset.sch 72 */
			return
				BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3669));
		}

	}



/* rtl_reg/ra-name */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt
		BgL_oz00_40)
	{
		{	/* SawMill/regset.sch 74 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_40)))->BgL_namez00);
		}

	}



/* &rtl_reg/ra-name */
	obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3670, obj_t BgL_oz00_3671)
	{
		{	/* SawMill/regset.sch 74 */
			return
				BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3671));
		}

	}



/* rtl_reg/ra-onexpr? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt
		BgL_oz00_43)
	{
		{	/* SawMill/regset.sch 76 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_43)))->BgL_onexprzf3zf3);
		}

	}



/* &rtl_reg/ra-onexpr? */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3672, obj_t BgL_oz00_3673)
	{
		{	/* SawMill/regset.sch 76 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3673));
		}

	}



/* rtl_reg/ra-onexpr?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt BgL_oz00_44, obj_t BgL_vz00_45)
	{
		{	/* SawMill/regset.sch 77 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_44)))->BgL_onexprzf3zf3) =
				((obj_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &rtl_reg/ra-onexpr?-set! */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3674, obj_t BgL_oz00_3675, obj_t BgL_vz00_3676)
	{
		{	/* SawMill/regset.sch 77 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3675), BgL_vz00_3676);
		}

	}



/* rtl_reg/ra-var */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt
		BgL_oz00_46)
	{
		{	/* SawMill/regset.sch 78 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_46)))->BgL_varz00);
		}

	}



/* &rtl_reg/ra-var */
	obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3677, obj_t BgL_oz00_3678)
	{
		{	/* SawMill/regset.sch 78 */
			return
				BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3678));
		}

	}



/* rtl_reg/ra-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt BgL_oz00_47, obj_t BgL_vz00_48)
	{
		{	/* SawMill/regset.sch 79 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_47)))->BgL_varz00) =
				((obj_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &rtl_reg/ra-var-set! */
	obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3679, obj_t BgL_oz00_3680, obj_t BgL_vz00_3681)
	{
		{	/* SawMill/regset.sch 79 */
			return
				BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3680), BgL_vz00_3681);
		}

	}



/* rtl_reg/ra-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt
		BgL_oz00_49)
	{
		{	/* SawMill/regset.sch 80 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_49)))->BgL_typez00);
		}

	}



/* &rtl_reg/ra-type */
	BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3682, obj_t BgL_oz00_3683)
	{
		{	/* SawMill/regset.sch 80 */
			return
				BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3683));
		}

	}



/* rtl_reg/ra-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_regz00_bglt BgL_oz00_50, BgL_typez00_bglt BgL_vz00_51)
	{
		{	/* SawMill/regset.sch 81 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_50)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_51), BUNSPEC);
		}

	}



/* &rtl_reg/ra-type-set! */
	obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3684, obj_t BgL_oz00_3685, obj_t BgL_vz00_3686)
	{
		{	/* SawMill/regset.sch 81 */
			return
				BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3685),
				((BgL_typez00_bglt) BgL_vz00_3686));
		}

	}



/* make-regset */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2livenesszd2(int BgL_length1172z00_52,
		int BgL_msiza7e1173za7_53, obj_t BgL_regv1174z00_54,
		obj_t BgL_regl1175z00_55, obj_t BgL_string1176z00_56)
	{
		{	/* SawMill/regset.sch 84 */
			{	/* SawMill/regset.sch 84 */
				BgL_regsetz00_bglt BgL_new1168z00_4015;

				{	/* SawMill/regset.sch 84 */
					BgL_regsetz00_bglt BgL_new1167z00_4016;

					BgL_new1167z00_4016 =
						((BgL_regsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_regsetz00_bgl))));
					{	/* SawMill/regset.sch 84 */
						long BgL_arg1513z00_4017;

						BgL_arg1513z00_4017 = BGL_CLASS_NUM(BGl_regsetz00zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1167z00_4016), BgL_arg1513z00_4017);
					}
					BgL_new1168z00_4015 = BgL_new1167z00_4016;
				}
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_4015))->BgL_lengthz00) =
					((int) BgL_length1172z00_52), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_4015))->BgL_msiza7eza7) =
					((int) BgL_msiza7e1173za7_53), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_4015))->BgL_regvz00) =
					((obj_t) BgL_regv1174z00_54), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_4015))->BgL_reglz00) =
					((obj_t) BgL_regl1175z00_55), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_4015))->BgL_stringz00) =
					((obj_t) BgL_string1176z00_56), BUNSPEC);
				return BgL_new1168z00_4015;
			}
		}

	}



/* &make-regset */
	BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3687, obj_t BgL_length1172z00_3688,
		obj_t BgL_msiza7e1173za7_3689, obj_t BgL_regv1174z00_3690,
		obj_t BgL_regl1175z00_3691, obj_t BgL_string1176z00_3692)
	{
		{	/* SawMill/regset.sch 84 */
			return
				BGl_makezd2regsetzd2zzsaw_bbvzd2livenesszd2(CINT
				(BgL_length1172z00_3688), CINT(BgL_msiza7e1173za7_3689),
				BgL_regv1174z00_3690, BgL_regl1175z00_3691, BgL_string1176z00_3692);
		}

	}



/* regset? */
	BGL_EXPORTED_DEF bool_t BGl_regsetzf3zf3zzsaw_bbvzd2livenesszd2(obj_t
		BgL_objz00_57)
	{
		{	/* SawMill/regset.sch 85 */
			{	/* SawMill/regset.sch 85 */
				obj_t BgL_classz00_4018;

				BgL_classz00_4018 = BGl_regsetz00zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_57))
					{	/* SawMill/regset.sch 85 */
						BgL_objectz00_bglt BgL_arg1807z00_4019;

						BgL_arg1807z00_4019 = (BgL_objectz00_bglt) (BgL_objz00_57);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 85 */
								long BgL_idxz00_4020;

								BgL_idxz00_4020 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4019);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4020 + 1L)) == BgL_classz00_4018);
							}
						else
							{	/* SawMill/regset.sch 85 */
								bool_t BgL_res1977z00_4023;

								{	/* SawMill/regset.sch 85 */
									obj_t BgL_oclassz00_4024;

									{	/* SawMill/regset.sch 85 */
										obj_t BgL_arg1815z00_4025;
										long BgL_arg1816z00_4026;

										BgL_arg1815z00_4025 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 85 */
											long BgL_arg1817z00_4027;

											BgL_arg1817z00_4027 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4019);
											BgL_arg1816z00_4026 = (BgL_arg1817z00_4027 - OBJECT_TYPE);
										}
										BgL_oclassz00_4024 =
											VECTOR_REF(BgL_arg1815z00_4025, BgL_arg1816z00_4026);
									}
									{	/* SawMill/regset.sch 85 */
										bool_t BgL__ortest_1115z00_4028;

										BgL__ortest_1115z00_4028 =
											(BgL_classz00_4018 == BgL_oclassz00_4024);
										if (BgL__ortest_1115z00_4028)
											{	/* SawMill/regset.sch 85 */
												BgL_res1977z00_4023 = BgL__ortest_1115z00_4028;
											}
										else
											{	/* SawMill/regset.sch 85 */
												long BgL_odepthz00_4029;

												{	/* SawMill/regset.sch 85 */
													obj_t BgL_arg1804z00_4030;

													BgL_arg1804z00_4030 = (BgL_oclassz00_4024);
													BgL_odepthz00_4029 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4030);
												}
												if ((1L < BgL_odepthz00_4029))
													{	/* SawMill/regset.sch 85 */
														obj_t BgL_arg1802z00_4031;

														{	/* SawMill/regset.sch 85 */
															obj_t BgL_arg1803z00_4032;

															BgL_arg1803z00_4032 = (BgL_oclassz00_4024);
															BgL_arg1802z00_4031 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4032,
																1L);
														}
														BgL_res1977z00_4023 =
															(BgL_arg1802z00_4031 == BgL_classz00_4018);
													}
												else
													{	/* SawMill/regset.sch 85 */
														BgL_res1977z00_4023 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1977z00_4023;
							}
					}
				else
					{	/* SawMill/regset.sch 85 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &regset? */
	obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3693,
		obj_t BgL_objz00_3694)
	{
		{	/* SawMill/regset.sch 85 */
			return BBOOL(BGl_regsetzf3zf3zzsaw_bbvzd2livenesszd2(BgL_objz00_3694));
		}

	}



/* regset-nil */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2livenesszd2(void)
	{
		{	/* SawMill/regset.sch 86 */
			{	/* SawMill/regset.sch 86 */
				obj_t BgL_classz00_2888;

				BgL_classz00_2888 = BGl_regsetz00zzsaw_regsetz00;
				{	/* SawMill/regset.sch 86 */
					obj_t BgL__ortest_1117z00_2889;

					BgL__ortest_1117z00_2889 = BGL_CLASS_NIL(BgL_classz00_2888);
					if (CBOOL(BgL__ortest_1117z00_2889))
						{	/* SawMill/regset.sch 86 */
							return ((BgL_regsetz00_bglt) BgL__ortest_1117z00_2889);
						}
					else
						{	/* SawMill/regset.sch 86 */
							return
								((BgL_regsetz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2888));
						}
				}
			}
		}

	}



/* &regset-nil */
	BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3695)
	{
		{	/* SawMill/regset.sch 86 */
			return BGl_regsetzd2nilzd2zzsaw_bbvzd2livenesszd2();
		}

	}



/* regset-string */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2livenesszd2(BgL_regsetz00_bglt
		BgL_oz00_58)
	{
		{	/* SawMill/regset.sch 87 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_58))->BgL_stringz00);
		}

	}



/* &regset-string */
	obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3696,
		obj_t BgL_oz00_3697)
	{
		{	/* SawMill/regset.sch 87 */
			return
				BGl_regsetzd2stringzd2zzsaw_bbvzd2livenesszd2(
				((BgL_regsetz00_bglt) BgL_oz00_3697));
		}

	}



/* regset-string-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_regsetz00_bglt
		BgL_oz00_59, obj_t BgL_vz00_60)
	{
		{	/* SawMill/regset.sch 88 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_59))->BgL_stringz00) =
				((obj_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &regset-string-set! */
	obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3698, obj_t BgL_oz00_3699, obj_t BgL_vz00_3700)
	{
		{	/* SawMill/regset.sch 88 */
			return
				BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_regsetz00_bglt) BgL_oz00_3699), BgL_vz00_3700);
		}

	}



/* regset-regl */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2livenesszd2(BgL_regsetz00_bglt BgL_oz00_61)
	{
		{	/* SawMill/regset.sch 89 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_61))->BgL_reglz00);
		}

	}



/* &regset-regl */
	obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3701,
		obj_t BgL_oz00_3702)
	{
		{	/* SawMill/regset.sch 89 */
			return
				BGl_regsetzd2reglzd2zzsaw_bbvzd2livenesszd2(
				((BgL_regsetz00_bglt) BgL_oz00_3702));
		}

	}



/* regset-regv */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2livenesszd2(BgL_regsetz00_bglt BgL_oz00_64)
	{
		{	/* SawMill/regset.sch 91 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_64))->BgL_regvz00);
		}

	}



/* &regset-regv */
	obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3703,
		obj_t BgL_oz00_3704)
	{
		{	/* SawMill/regset.sch 91 */
			return
				BGl_regsetzd2regvzd2zzsaw_bbvzd2livenesszd2(
				((BgL_regsetz00_bglt) BgL_oz00_3704));
		}

	}



/* regset-msize */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2livenesszd2(BgL_regsetz00_bglt
		BgL_oz00_67)
	{
		{	/* SawMill/regset.sch 93 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_67))->BgL_msiza7eza7);
		}

	}



/* &regset-msize */
	obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3705,
		obj_t BgL_oz00_3706)
	{
		{	/* SawMill/regset.sch 93 */
			return
				BINT(BGl_regsetzd2msiza7ez75zzsaw_bbvzd2livenesszd2(
					((BgL_regsetz00_bglt) BgL_oz00_3706)));
		}

	}



/* regset-length */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2livenesszd2(BgL_regsetz00_bglt
		BgL_oz00_70)
	{
		{	/* SawMill/regset.sch 95 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_70))->BgL_lengthz00);
		}

	}



/* &regset-length */
	obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3707,
		obj_t BgL_oz00_3708)
	{
		{	/* SawMill/regset.sch 95 */
			return
				BINT(BGl_regsetzd2lengthzd2zzsaw_bbvzd2livenesszd2(
					((BgL_regsetz00_bglt) BgL_oz00_3708)));
		}

	}



/* regset-length-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_regsetz00_bglt
		BgL_oz00_71, int BgL_vz00_72)
	{
		{	/* SawMill/regset.sch 96 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_71))->BgL_lengthz00) =
				((int) BgL_vz00_72), BUNSPEC);
		}

	}



/* &regset-length-set! */
	obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3709, obj_t BgL_oz00_3710, obj_t BgL_vz00_3711)
	{
		{	/* SawMill/regset.sch 96 */
			return
				BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_regsetz00_bglt) BgL_oz00_3710), CINT(BgL_vz00_3711));
		}

	}



/* make-rtl_ins/bbv */
	BGL_EXPORTED_DEF BgL_rtl_insz00_bglt
		BGl_makezd2rtl_inszf2bbvz20zzsaw_bbvzd2livenesszd2(obj_t BgL_loc1264z00_73,
		obj_t BgL_z52spill1265z52_74, obj_t BgL_dest1266z00_75,
		BgL_rtl_funz00_bglt BgL_fun1267z00_76, obj_t BgL_args1268z00_77,
		obj_t BgL_def1269z00_78, obj_t BgL_out1270z00_79, obj_t BgL_in1271z00_80,
		BgL_bbvzd2ctxzd2_bglt BgL_ctx1272z00_81, obj_t BgL_z52hash1273z52_82)
	{
		{	/* SawBbv/bbv-types.sch 137 */
			{	/* SawBbv/bbv-types.sch 137 */
				BgL_rtl_insz00_bglt BgL_new1172z00_4033;

				{	/* SawBbv/bbv-types.sch 137 */
					BgL_rtl_insz00_bglt BgL_tmp1170z00_4034;
					BgL_rtl_inszf2bbvzf2_bglt BgL_wide1171z00_4035;

					{
						BgL_rtl_insz00_bglt BgL_auxz00_4514;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_rtl_insz00_bglt BgL_new1169z00_4036;

							BgL_new1169z00_4036 =
								((BgL_rtl_insz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_insz00_bgl))));
							{	/* SawBbv/bbv-types.sch 137 */
								long BgL_arg1516z00_4037;

								BgL_arg1516z00_4037 =
									BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1169z00_4036),
									BgL_arg1516z00_4037);
							}
							{	/* SawBbv/bbv-types.sch 137 */
								BgL_objectz00_bglt BgL_tmpz00_4519;

								BgL_tmpz00_4519 = ((BgL_objectz00_bglt) BgL_new1169z00_4036);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4519, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1169z00_4036);
							BgL_auxz00_4514 = BgL_new1169z00_4036;
						}
						BgL_tmp1170z00_4034 = ((BgL_rtl_insz00_bglt) BgL_auxz00_4514);
					}
					BgL_wide1171z00_4035 =
						((BgL_rtl_inszf2bbvzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_inszf2bbvzf2_bgl))));
					{	/* SawBbv/bbv-types.sch 137 */
						obj_t BgL_auxz00_4527;
						BgL_objectz00_bglt BgL_tmpz00_4525;

						BgL_auxz00_4527 = ((obj_t) BgL_wide1171z00_4035);
						BgL_tmpz00_4525 = ((BgL_objectz00_bglt) BgL_tmp1170z00_4034);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4525, BgL_auxz00_4527);
					}
					((BgL_objectz00_bglt) BgL_tmp1170z00_4034);
					{	/* SawBbv/bbv-types.sch 137 */
						long BgL_arg1514z00_4038;

						BgL_arg1514z00_4038 =
							BGL_CLASS_NUM(BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1170z00_4034), BgL_arg1514z00_4038);
					}
					BgL_new1172z00_4033 = ((BgL_rtl_insz00_bglt) BgL_tmp1170z00_4034);
				}
				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_new1172z00_4033)))->BgL_locz00) =
					((obj_t) BgL_loc1264z00_73), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1172z00_4033)))->BgL_z52spillz52) =
					((obj_t) BgL_z52spill1265z52_74), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1172z00_4033)))->BgL_destz00) =
					((obj_t) BgL_dest1266z00_75), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1172z00_4033)))->BgL_funz00) =
					((BgL_rtl_funz00_bglt) BgL_fun1267z00_76), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1172z00_4033)))->BgL_argsz00) =
					((obj_t) BgL_args1268z00_77), BUNSPEC);
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4545;

					{
						obj_t BgL_auxz00_4546;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_objectz00_bglt BgL_tmpz00_4547;

							BgL_tmpz00_4547 = ((BgL_objectz00_bglt) BgL_new1172z00_4033);
							BgL_auxz00_4546 = BGL_OBJECT_WIDENING(BgL_tmpz00_4547);
						}
						BgL_auxz00_4545 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4546);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4545))->
							BgL_defz00) = ((obj_t) BgL_def1269z00_78), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4552;

					{
						obj_t BgL_auxz00_4553;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_objectz00_bglt BgL_tmpz00_4554;

							BgL_tmpz00_4554 = ((BgL_objectz00_bglt) BgL_new1172z00_4033);
							BgL_auxz00_4553 = BGL_OBJECT_WIDENING(BgL_tmpz00_4554);
						}
						BgL_auxz00_4552 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4553);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4552))->
							BgL_outz00) = ((obj_t) BgL_out1270z00_79), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4559;

					{
						obj_t BgL_auxz00_4560;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_objectz00_bglt BgL_tmpz00_4561;

							BgL_tmpz00_4561 = ((BgL_objectz00_bglt) BgL_new1172z00_4033);
							BgL_auxz00_4560 = BGL_OBJECT_WIDENING(BgL_tmpz00_4561);
						}
						BgL_auxz00_4559 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4560);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4559))->BgL_inz00) =
						((obj_t) BgL_in1271z00_80), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4566;

					{
						obj_t BgL_auxz00_4567;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_objectz00_bglt BgL_tmpz00_4568;

							BgL_tmpz00_4568 = ((BgL_objectz00_bglt) BgL_new1172z00_4033);
							BgL_auxz00_4567 = BGL_OBJECT_WIDENING(BgL_tmpz00_4568);
						}
						BgL_auxz00_4566 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4567);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4566))->
							BgL_ctxz00) =
						((BgL_bbvzd2ctxzd2_bglt) BgL_ctx1272z00_81), BUNSPEC);
				}
				{
					BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4573;

					{
						obj_t BgL_auxz00_4574;

						{	/* SawBbv/bbv-types.sch 137 */
							BgL_objectz00_bglt BgL_tmpz00_4575;

							BgL_tmpz00_4575 = ((BgL_objectz00_bglt) BgL_new1172z00_4033);
							BgL_auxz00_4574 = BGL_OBJECT_WIDENING(BgL_tmpz00_4575);
						}
						BgL_auxz00_4573 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4574);
					}
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4573))->
							BgL_z52hashz52) = ((obj_t) BgL_z52hash1273z52_82), BUNSPEC);
				}
				return BgL_new1172z00_4033;
			}
		}

	}



/* &make-rtl_ins/bbv */
	BgL_rtl_insz00_bglt
		BGl_z62makezd2rtl_inszf2bbvz42zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3712,
		obj_t BgL_loc1264z00_3713, obj_t BgL_z52spill1265z52_3714,
		obj_t BgL_dest1266z00_3715, obj_t BgL_fun1267z00_3716,
		obj_t BgL_args1268z00_3717, obj_t BgL_def1269z00_3718,
		obj_t BgL_out1270z00_3719, obj_t BgL_in1271z00_3720,
		obj_t BgL_ctx1272z00_3721, obj_t BgL_z52hash1273z52_3722)
	{
		{	/* SawBbv/bbv-types.sch 137 */
			return
				BGl_makezd2rtl_inszf2bbvz20zzsaw_bbvzd2livenesszd2(BgL_loc1264z00_3713,
				BgL_z52spill1265z52_3714, BgL_dest1266z00_3715,
				((BgL_rtl_funz00_bglt) BgL_fun1267z00_3716), BgL_args1268z00_3717,
				BgL_def1269z00_3718, BgL_out1270z00_3719, BgL_in1271z00_3720,
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctx1272z00_3721), BgL_z52hash1273z52_3722);
		}

	}



/* rtl_ins/bbv? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_inszf2bbvzf3z01zzsaw_bbvzd2livenesszd2(obj_t
		BgL_objz00_83)
	{
		{	/* SawBbv/bbv-types.sch 138 */
			{	/* SawBbv/bbv-types.sch 138 */
				obj_t BgL_classz00_4039;

				BgL_classz00_4039 = BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_83))
					{	/* SawBbv/bbv-types.sch 138 */
						BgL_objectz00_bglt BgL_arg1807z00_4040;

						BgL_arg1807z00_4040 = (BgL_objectz00_bglt) (BgL_objz00_83);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 138 */
								long BgL_idxz00_4041;

								BgL_idxz00_4041 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4040);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4041 + 2L)) == BgL_classz00_4039);
							}
						else
							{	/* SawBbv/bbv-types.sch 138 */
								bool_t BgL_res1978z00_4044;

								{	/* SawBbv/bbv-types.sch 138 */
									obj_t BgL_oclassz00_4045;

									{	/* SawBbv/bbv-types.sch 138 */
										obj_t BgL_arg1815z00_4046;
										long BgL_arg1816z00_4047;

										BgL_arg1815z00_4046 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 138 */
											long BgL_arg1817z00_4048;

											BgL_arg1817z00_4048 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4040);
											BgL_arg1816z00_4047 = (BgL_arg1817z00_4048 - OBJECT_TYPE);
										}
										BgL_oclassz00_4045 =
											VECTOR_REF(BgL_arg1815z00_4046, BgL_arg1816z00_4047);
									}
									{	/* SawBbv/bbv-types.sch 138 */
										bool_t BgL__ortest_1115z00_4049;

										BgL__ortest_1115z00_4049 =
											(BgL_classz00_4039 == BgL_oclassz00_4045);
										if (BgL__ortest_1115z00_4049)
											{	/* SawBbv/bbv-types.sch 138 */
												BgL_res1978z00_4044 = BgL__ortest_1115z00_4049;
											}
										else
											{	/* SawBbv/bbv-types.sch 138 */
												long BgL_odepthz00_4050;

												{	/* SawBbv/bbv-types.sch 138 */
													obj_t BgL_arg1804z00_4051;

													BgL_arg1804z00_4051 = (BgL_oclassz00_4045);
													BgL_odepthz00_4050 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4051);
												}
												if ((2L < BgL_odepthz00_4050))
													{	/* SawBbv/bbv-types.sch 138 */
														obj_t BgL_arg1802z00_4052;

														{	/* SawBbv/bbv-types.sch 138 */
															obj_t BgL_arg1803z00_4053;

															BgL_arg1803z00_4053 = (BgL_oclassz00_4045);
															BgL_arg1802z00_4052 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4053,
																2L);
														}
														BgL_res1978z00_4044 =
															(BgL_arg1802z00_4052 == BgL_classz00_4039);
													}
												else
													{	/* SawBbv/bbv-types.sch 138 */
														BgL_res1978z00_4044 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1978z00_4044;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 138 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_ins/bbv? */
	obj_t BGl_z62rtl_inszf2bbvzf3z63zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3723,
		obj_t BgL_objz00_3724)
	{
		{	/* SawBbv/bbv-types.sch 138 */
			return
				BBOOL(BGl_rtl_inszf2bbvzf3z01zzsaw_bbvzd2livenesszd2(BgL_objz00_3724));
		}

	}



/* rtl_ins/bbv-nil */
	BGL_EXPORTED_DEF BgL_rtl_insz00_bglt
		BGl_rtl_inszf2bbvzd2nilz20zzsaw_bbvzd2livenesszd2(void)
	{
		{	/* SawBbv/bbv-types.sch 139 */
			{	/* SawBbv/bbv-types.sch 139 */
				obj_t BgL_classz00_2943;

				BgL_classz00_2943 = BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 139 */
					obj_t BgL__ortest_1117z00_2944;

					BgL__ortest_1117z00_2944 = BGL_CLASS_NIL(BgL_classz00_2943);
					if (CBOOL(BgL__ortest_1117z00_2944))
						{	/* SawBbv/bbv-types.sch 139 */
							return ((BgL_rtl_insz00_bglt) BgL__ortest_1117z00_2944);
						}
					else
						{	/* SawBbv/bbv-types.sch 139 */
							return
								((BgL_rtl_insz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2943));
						}
				}
			}
		}

	}



/* &rtl_ins/bbv-nil */
	BgL_rtl_insz00_bglt BGl_z62rtl_inszf2bbvzd2nilz42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3725)
	{
		{	/* SawBbv/bbv-types.sch 139 */
			return BGl_rtl_inszf2bbvzd2nilz20zzsaw_bbvzd2livenesszd2();
		}

	}



/* rtl_ins/bbv-%hash */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2z52hashz72zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt
		BgL_oz00_84)
	{
		{	/* SawBbv/bbv-types.sch 140 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4614;

				{
					obj_t BgL_auxz00_4615;

					{	/* SawBbv/bbv-types.sch 140 */
						BgL_objectz00_bglt BgL_tmpz00_4616;

						BgL_tmpz00_4616 = ((BgL_objectz00_bglt) BgL_oz00_84);
						BgL_auxz00_4615 = BGL_OBJECT_WIDENING(BgL_tmpz00_4616);
					}
					BgL_auxz00_4614 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4615);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4614))->
					BgL_z52hashz52);
			}
		}

	}



/* &rtl_ins/bbv-%hash */
	obj_t BGl_z62rtl_inszf2bbvzd2z52hashz10zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3726, obj_t BgL_oz00_3727)
	{
		{	/* SawBbv/bbv-types.sch 140 */
			return
				BGl_rtl_inszf2bbvzd2z52hashz72zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3727));
		}

	}



/* rtl_ins/bbv-%hash-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2z52hashzd2setz12zb2zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt BgL_oz00_85, obj_t BgL_vz00_86)
	{
		{	/* SawBbv/bbv-types.sch 141 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4623;

				{
					obj_t BgL_auxz00_4624;

					{	/* SawBbv/bbv-types.sch 141 */
						BgL_objectz00_bglt BgL_tmpz00_4625;

						BgL_tmpz00_4625 = ((BgL_objectz00_bglt) BgL_oz00_85);
						BgL_auxz00_4624 = BGL_OBJECT_WIDENING(BgL_tmpz00_4625);
					}
					BgL_auxz00_4623 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4624);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4623))->
						BgL_z52hashz52) = ((obj_t) BgL_vz00_86), BUNSPEC);
			}
		}

	}



/* &rtl_ins/bbv-%hash-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2z52hashzd2setz12zd0zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3728, obj_t BgL_oz00_3729, obj_t BgL_vz00_3730)
	{
		{	/* SawBbv/bbv-types.sch 141 */
			return
				BGl_rtl_inszf2bbvzd2z52hashzd2setz12zb2zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3729), BgL_vz00_3730);
		}

	}



/* rtl_ins/bbv-ctx */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_rtl_inszf2bbvzd2ctxz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt
		BgL_oz00_87)
	{
		{	/* SawBbv/bbv-types.sch 142 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4632;

				{
					obj_t BgL_auxz00_4633;

					{	/* SawBbv/bbv-types.sch 142 */
						BgL_objectz00_bglt BgL_tmpz00_4634;

						BgL_tmpz00_4634 = ((BgL_objectz00_bglt) BgL_oz00_87);
						BgL_auxz00_4633 = BGL_OBJECT_WIDENING(BgL_tmpz00_4634);
					}
					BgL_auxz00_4632 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4633);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4632))->BgL_ctxz00);
			}
		}

	}



/* &rtl_ins/bbv-ctx */
	BgL_bbvzd2ctxzd2_bglt
		BGl_z62rtl_inszf2bbvzd2ctxz42zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3731,
		obj_t BgL_oz00_3732)
	{
		{	/* SawBbv/bbv-types.sch 142 */
			return
				BGl_rtl_inszf2bbvzd2ctxz20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3732));
		}

	}



/* rtl_ins/bbv-ctx-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2ctxzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt BgL_oz00_88, BgL_bbvzd2ctxzd2_bglt BgL_vz00_89)
	{
		{	/* SawBbv/bbv-types.sch 143 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4641;

				{
					obj_t BgL_auxz00_4642;

					{	/* SawBbv/bbv-types.sch 143 */
						BgL_objectz00_bglt BgL_tmpz00_4643;

						BgL_tmpz00_4643 = ((BgL_objectz00_bglt) BgL_oz00_88);
						BgL_auxz00_4642 = BGL_OBJECT_WIDENING(BgL_tmpz00_4643);
					}
					BgL_auxz00_4641 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4642);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4641))->
						BgL_ctxz00) = ((BgL_bbvzd2ctxzd2_bglt) BgL_vz00_89), BUNSPEC);
			}
		}

	}



/* &rtl_ins/bbv-ctx-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2ctxzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3733, obj_t BgL_oz00_3734, obj_t BgL_vz00_3735)
	{
		{	/* SawBbv/bbv-types.sch 143 */
			return
				BGl_rtl_inszf2bbvzd2ctxzd2setz12ze0zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3734),
				((BgL_bbvzd2ctxzd2_bglt) BgL_vz00_3735));
		}

	}



/* rtl_ins/bbv-in */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2inz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt
		BgL_oz00_90)
	{
		{	/* SawBbv/bbv-types.sch 144 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4651;

				{
					obj_t BgL_auxz00_4652;

					{	/* SawBbv/bbv-types.sch 144 */
						BgL_objectz00_bglt BgL_tmpz00_4653;

						BgL_tmpz00_4653 = ((BgL_objectz00_bglt) BgL_oz00_90);
						BgL_auxz00_4652 = BGL_OBJECT_WIDENING(BgL_tmpz00_4653);
					}
					BgL_auxz00_4651 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4652);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4651))->BgL_inz00);
			}
		}

	}



/* &rtl_ins/bbv-in */
	obj_t BGl_z62rtl_inszf2bbvzd2inz42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3736, obj_t BgL_oz00_3737)
	{
		{	/* SawBbv/bbv-types.sch 144 */
			return
				BGl_rtl_inszf2bbvzd2inz20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3737));
		}

	}



/* rtl_ins/bbv-in-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2inzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt BgL_oz00_91, obj_t BgL_vz00_92)
	{
		{	/* SawBbv/bbv-types.sch 145 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4660;

				{
					obj_t BgL_auxz00_4661;

					{	/* SawBbv/bbv-types.sch 145 */
						BgL_objectz00_bglt BgL_tmpz00_4662;

						BgL_tmpz00_4662 = ((BgL_objectz00_bglt) BgL_oz00_91);
						BgL_auxz00_4661 = BGL_OBJECT_WIDENING(BgL_tmpz00_4662);
					}
					BgL_auxz00_4660 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4661);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4660))->BgL_inz00) =
					((obj_t) BgL_vz00_92), BUNSPEC);
			}
		}

	}



/* &rtl_ins/bbv-in-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2inzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3738, obj_t BgL_oz00_3739, obj_t BgL_vz00_3740)
	{
		{	/* SawBbv/bbv-types.sch 145 */
			return
				BGl_rtl_inszf2bbvzd2inzd2setz12ze0zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3739), BgL_vz00_3740);
		}

	}



/* rtl_ins/bbv-out */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2outz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt
		BgL_oz00_93)
	{
		{	/* SawBbv/bbv-types.sch 146 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4669;

				{
					obj_t BgL_auxz00_4670;

					{	/* SawBbv/bbv-types.sch 146 */
						BgL_objectz00_bglt BgL_tmpz00_4671;

						BgL_tmpz00_4671 = ((BgL_objectz00_bglt) BgL_oz00_93);
						BgL_auxz00_4670 = BGL_OBJECT_WIDENING(BgL_tmpz00_4671);
					}
					BgL_auxz00_4669 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4670);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4669))->BgL_outz00);
			}
		}

	}



/* &rtl_ins/bbv-out */
	obj_t BGl_z62rtl_inszf2bbvzd2outz42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3741, obj_t BgL_oz00_3742)
	{
		{	/* SawBbv/bbv-types.sch 146 */
			return
				BGl_rtl_inszf2bbvzd2outz20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3742));
		}

	}



/* rtl_ins/bbv-out-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2outzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt BgL_oz00_94, obj_t BgL_vz00_95)
	{
		{	/* SawBbv/bbv-types.sch 147 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4678;

				{
					obj_t BgL_auxz00_4679;

					{	/* SawBbv/bbv-types.sch 147 */
						BgL_objectz00_bglt BgL_tmpz00_4680;

						BgL_tmpz00_4680 = ((BgL_objectz00_bglt) BgL_oz00_94);
						BgL_auxz00_4679 = BGL_OBJECT_WIDENING(BgL_tmpz00_4680);
					}
					BgL_auxz00_4678 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4679);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4678))->
						BgL_outz00) = ((obj_t) BgL_vz00_95), BUNSPEC);
			}
		}

	}



/* &rtl_ins/bbv-out-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2outzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3743, obj_t BgL_oz00_3744, obj_t BgL_vz00_3745)
	{
		{	/* SawBbv/bbv-types.sch 147 */
			return
				BGl_rtl_inszf2bbvzd2outzd2setz12ze0zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3744), BgL_vz00_3745);
		}

	}



/* rtl_ins/bbv-def */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2defz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt
		BgL_oz00_96)
	{
		{	/* SawBbv/bbv-types.sch 148 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4687;

				{
					obj_t BgL_auxz00_4688;

					{	/* SawBbv/bbv-types.sch 148 */
						BgL_objectz00_bglt BgL_tmpz00_4689;

						BgL_tmpz00_4689 = ((BgL_objectz00_bglt) BgL_oz00_96);
						BgL_auxz00_4688 = BGL_OBJECT_WIDENING(BgL_tmpz00_4689);
					}
					BgL_auxz00_4687 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4688);
				}
				return
					(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4687))->BgL_defz00);
			}
		}

	}



/* &rtl_ins/bbv-def */
	obj_t BGl_z62rtl_inszf2bbvzd2defz42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3746, obj_t BgL_oz00_3747)
	{
		{	/* SawBbv/bbv-types.sch 148 */
			return
				BGl_rtl_inszf2bbvzd2defz20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3747));
		}

	}



/* rtl_ins/bbv-def-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2defzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt BgL_oz00_97, obj_t BgL_vz00_98)
	{
		{	/* SawBbv/bbv-types.sch 149 */
			{
				BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4696;

				{
					obj_t BgL_auxz00_4697;

					{	/* SawBbv/bbv-types.sch 149 */
						BgL_objectz00_bglt BgL_tmpz00_4698;

						BgL_tmpz00_4698 = ((BgL_objectz00_bglt) BgL_oz00_97);
						BgL_auxz00_4697 = BGL_OBJECT_WIDENING(BgL_tmpz00_4698);
					}
					BgL_auxz00_4696 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4697);
				}
				return
					((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4696))->
						BgL_defz00) = ((obj_t) BgL_vz00_98), BUNSPEC);
			}
		}

	}



/* &rtl_ins/bbv-def-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2defzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3748, obj_t BgL_oz00_3749, obj_t BgL_vz00_3750)
	{
		{	/* SawBbv/bbv-types.sch 149 */
			return
				BGl_rtl_inszf2bbvzd2defzd2setz12ze0zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3749), BgL_vz00_3750);
		}

	}



/* rtl_ins/bbv-args */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2argsz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt
		BgL_oz00_99)
	{
		{	/* SawBbv/bbv-types.sch 150 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_99)))->BgL_argsz00);
		}

	}



/* &rtl_ins/bbv-args */
	obj_t BGl_z62rtl_inszf2bbvzd2argsz42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3751, obj_t BgL_oz00_3752)
	{
		{	/* SawBbv/bbv-types.sch 150 */
			return
				BGl_rtl_inszf2bbvzd2argsz20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3752));
		}

	}



/* rtl_ins/bbv-args-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2argszd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt BgL_oz00_100, obj_t BgL_vz00_101)
	{
		{	/* SawBbv/bbv-types.sch 151 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_100)))->BgL_argsz00) =
				((obj_t) BgL_vz00_101), BUNSPEC);
		}

	}



/* &rtl_ins/bbv-args-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2argszd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3753, obj_t BgL_oz00_3754, obj_t BgL_vz00_3755)
	{
		{	/* SawBbv/bbv-types.sch 151 */
			return
				BGl_rtl_inszf2bbvzd2argszd2setz12ze0zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3754), BgL_vz00_3755);
		}

	}



/* rtl_ins/bbv-fun */
	BGL_EXPORTED_DEF BgL_rtl_funz00_bglt
		BGl_rtl_inszf2bbvzd2funz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt
		BgL_oz00_102)
	{
		{	/* SawBbv/bbv-types.sch 152 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_102)))->BgL_funz00);
		}

	}



/* &rtl_ins/bbv-fun */
	BgL_rtl_funz00_bglt BGl_z62rtl_inszf2bbvzd2funz42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3756, obj_t BgL_oz00_3757)
	{
		{	/* SawBbv/bbv-types.sch 152 */
			return
				BGl_rtl_inszf2bbvzd2funz20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3757));
		}

	}



/* rtl_ins/bbv-fun-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2funzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt BgL_oz00_103, BgL_rtl_funz00_bglt BgL_vz00_104)
	{
		{	/* SawBbv/bbv-types.sch 153 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_103)))->BgL_funz00) =
				((BgL_rtl_funz00_bglt) BgL_vz00_104), BUNSPEC);
		}

	}



/* &rtl_ins/bbv-fun-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2funzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3758, obj_t BgL_oz00_3759, obj_t BgL_vz00_3760)
	{
		{	/* SawBbv/bbv-types.sch 153 */
			return
				BGl_rtl_inszf2bbvzd2funzd2setz12ze0zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3759),
				((BgL_rtl_funz00_bglt) BgL_vz00_3760));
		}

	}



/* rtl_ins/bbv-dest */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2destz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt
		BgL_oz00_105)
	{
		{	/* SawBbv/bbv-types.sch 154 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_105)))->BgL_destz00);
		}

	}



/* &rtl_ins/bbv-dest */
	obj_t BGl_z62rtl_inszf2bbvzd2destz42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3761, obj_t BgL_oz00_3762)
	{
		{	/* SawBbv/bbv-types.sch 154 */
			return
				BGl_rtl_inszf2bbvzd2destz20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3762));
		}

	}



/* rtl_ins/bbv-dest-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2destzd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt BgL_oz00_106, obj_t BgL_vz00_107)
	{
		{	/* SawBbv/bbv-types.sch 155 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_106)))->BgL_destz00) =
				((obj_t) BgL_vz00_107), BUNSPEC);
		}

	}



/* &rtl_ins/bbv-dest-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2destzd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3763, obj_t BgL_oz00_3764, obj_t BgL_vz00_3765)
	{
		{	/* SawBbv/bbv-types.sch 155 */
			return
				BGl_rtl_inszf2bbvzd2destzd2setz12ze0zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3764), BgL_vz00_3765);
		}

	}



/* rtl_ins/bbv-%spill */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2z52spillz72zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt
		BgL_oz00_108)
	{
		{	/* SawBbv/bbv-types.sch 156 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_108)))->BgL_z52spillz52);
		}

	}



/* &rtl_ins/bbv-%spill */
	obj_t BGl_z62rtl_inszf2bbvzd2z52spillz10zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3766, obj_t BgL_oz00_3767)
	{
		{	/* SawBbv/bbv-types.sch 156 */
			return
				BGl_rtl_inszf2bbvzd2z52spillz72zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3767));
		}

	}



/* rtl_ins/bbv-%spill-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2z52spillzd2setz12zb2zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt BgL_oz00_109, obj_t BgL_vz00_110)
	{
		{	/* SawBbv/bbv-types.sch 157 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_109)))->BgL_z52spillz52) =
				((obj_t) BgL_vz00_110), BUNSPEC);
		}

	}



/* &rtl_ins/bbv-%spill-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2z52spillzd2setz12zd0zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3768, obj_t BgL_oz00_3769, obj_t BgL_vz00_3770)
	{
		{	/* SawBbv/bbv-types.sch 157 */
			return
				BGl_rtl_inszf2bbvzd2z52spillzd2setz12zb2zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3769), BgL_vz00_3770);
		}

	}



/* rtl_ins/bbv-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2locz20zzsaw_bbvzd2livenesszd2(BgL_rtl_insz00_bglt
		BgL_oz00_111)
	{
		{	/* SawBbv/bbv-types.sch 158 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_111)))->BgL_locz00);
		}

	}



/* &rtl_ins/bbv-loc */
	obj_t BGl_z62rtl_inszf2bbvzd2locz42zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3771, obj_t BgL_oz00_3772)
	{
		{	/* SawBbv/bbv-types.sch 158 */
			return
				BGl_rtl_inszf2bbvzd2locz20zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3772));
		}

	}



/* rtl_ins/bbv-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszf2bbvzd2loczd2setz12ze0zzsaw_bbvzd2livenesszd2
		(BgL_rtl_insz00_bglt BgL_oz00_112, obj_t BgL_vz00_113)
	{
		{	/* SawBbv/bbv-types.sch 159 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_112)))->BgL_locz00) =
				((obj_t) BgL_vz00_113), BUNSPEC);
		}

	}



/* &rtl_ins/bbv-loc-set! */
	obj_t BGl_z62rtl_inszf2bbvzd2loczd2setz12z82zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3773, obj_t BgL_oz00_3774, obj_t BgL_vz00_3775)
	{
		{	/* SawBbv/bbv-types.sch 159 */
			return
				BGl_rtl_inszf2bbvzd2loczd2setz12ze0zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_insz00_bglt) BgL_oz00_3774), BgL_vz00_3775);
		}

	}



/* make-blockV */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_makezd2blockVzd2zzsaw_bbvzd2livenesszd2(int BgL_label1255z00_114,
		obj_t BgL_preds1256z00_115, obj_t BgL_succs1257z00_116,
		obj_t BgL_first1258z00_117, obj_t BgL_versions1259z00_118,
		obj_t BgL_generic1260z00_119, long BgL_z52mark1261z52_120,
		obj_t BgL_merge1262z00_121)
	{
		{	/* SawBbv/bbv-types.sch 162 */
			{	/* SawBbv/bbv-types.sch 162 */
				BgL_blockz00_bglt BgL_new1176z00_4054;

				{	/* SawBbv/bbv-types.sch 162 */
					BgL_blockz00_bglt BgL_tmp1174z00_4055;
					BgL_blockvz00_bglt BgL_wide1175z00_4056;

					{
						BgL_blockz00_bglt BgL_auxz00_4746;

						{	/* SawBbv/bbv-types.sch 162 */
							BgL_blockz00_bglt BgL_new1173z00_4057;

							BgL_new1173z00_4057 =
								((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_blockz00_bgl))));
							{	/* SawBbv/bbv-types.sch 162 */
								long BgL_arg1540z00_4058;

								BgL_arg1540z00_4058 = BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1173z00_4057),
									BgL_arg1540z00_4058);
							}
							{	/* SawBbv/bbv-types.sch 162 */
								BgL_objectz00_bglt BgL_tmpz00_4751;

								BgL_tmpz00_4751 = ((BgL_objectz00_bglt) BgL_new1173z00_4057);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4751, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1173z00_4057);
							BgL_auxz00_4746 = BgL_new1173z00_4057;
						}
						BgL_tmp1174z00_4055 = ((BgL_blockz00_bglt) BgL_auxz00_4746);
					}
					BgL_wide1175z00_4056 =
						((BgL_blockvz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_blockvz00_bgl))));
					{	/* SawBbv/bbv-types.sch 162 */
						obj_t BgL_auxz00_4759;
						BgL_objectz00_bglt BgL_tmpz00_4757;

						BgL_auxz00_4759 = ((obj_t) BgL_wide1175z00_4056);
						BgL_tmpz00_4757 = ((BgL_objectz00_bglt) BgL_tmp1174z00_4055);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4757, BgL_auxz00_4759);
					}
					((BgL_objectz00_bglt) BgL_tmp1174z00_4055);
					{	/* SawBbv/bbv-types.sch 162 */
						long BgL_arg1535z00_4059;

						BgL_arg1535z00_4059 =
							BGL_CLASS_NUM(BGl_blockVz00zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1174z00_4055), BgL_arg1535z00_4059);
					}
					BgL_new1176z00_4054 = ((BgL_blockz00_bglt) BgL_tmp1174z00_4055);
				}
				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_new1176z00_4054)))->BgL_labelz00) =
					((int) BgL_label1255z00_114), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1176z00_4054)))->BgL_predsz00) =
					((obj_t) BgL_preds1256z00_115), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1176z00_4054)))->BgL_succsz00) =
					((obj_t) BgL_succs1257z00_116), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1176z00_4054)))->BgL_firstz00) =
					((obj_t) BgL_first1258z00_117), BUNSPEC);
				{
					BgL_blockvz00_bglt BgL_auxz00_4775;

					{
						obj_t BgL_auxz00_4776;

						{	/* SawBbv/bbv-types.sch 162 */
							BgL_objectz00_bglt BgL_tmpz00_4777;

							BgL_tmpz00_4777 = ((BgL_objectz00_bglt) BgL_new1176z00_4054);
							BgL_auxz00_4776 = BGL_OBJECT_WIDENING(BgL_tmpz00_4777);
						}
						BgL_auxz00_4775 = ((BgL_blockvz00_bglt) BgL_auxz00_4776);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4775))->BgL_versionsz00) =
						((obj_t) BgL_versions1259z00_118), BUNSPEC);
				}
				{
					BgL_blockvz00_bglt BgL_auxz00_4782;

					{
						obj_t BgL_auxz00_4783;

						{	/* SawBbv/bbv-types.sch 162 */
							BgL_objectz00_bglt BgL_tmpz00_4784;

							BgL_tmpz00_4784 = ((BgL_objectz00_bglt) BgL_new1176z00_4054);
							BgL_auxz00_4783 = BGL_OBJECT_WIDENING(BgL_tmpz00_4784);
						}
						BgL_auxz00_4782 = ((BgL_blockvz00_bglt) BgL_auxz00_4783);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4782))->BgL_genericz00) =
						((obj_t) BgL_generic1260z00_119), BUNSPEC);
				}
				{
					BgL_blockvz00_bglt BgL_auxz00_4789;

					{
						obj_t BgL_auxz00_4790;

						{	/* SawBbv/bbv-types.sch 162 */
							BgL_objectz00_bglt BgL_tmpz00_4791;

							BgL_tmpz00_4791 = ((BgL_objectz00_bglt) BgL_new1176z00_4054);
							BgL_auxz00_4790 = BGL_OBJECT_WIDENING(BgL_tmpz00_4791);
						}
						BgL_auxz00_4789 = ((BgL_blockvz00_bglt) BgL_auxz00_4790);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4789))->BgL_z52markz52) =
						((long) BgL_z52mark1261z52_120), BUNSPEC);
				}
				{
					BgL_blockvz00_bglt BgL_auxz00_4796;

					{
						obj_t BgL_auxz00_4797;

						{	/* SawBbv/bbv-types.sch 162 */
							BgL_objectz00_bglt BgL_tmpz00_4798;

							BgL_tmpz00_4798 = ((BgL_objectz00_bglt) BgL_new1176z00_4054);
							BgL_auxz00_4797 = BGL_OBJECT_WIDENING(BgL_tmpz00_4798);
						}
						BgL_auxz00_4796 = ((BgL_blockvz00_bglt) BgL_auxz00_4797);
					}
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4796))->BgL_mergez00) =
						((obj_t) BgL_merge1262z00_121), BUNSPEC);
				}
				return BgL_new1176z00_4054;
			}
		}

	}



/* &make-blockV */
	BgL_blockz00_bglt BGl_z62makezd2blockVzb0zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3776, obj_t BgL_label1255z00_3777, obj_t BgL_preds1256z00_3778,
		obj_t BgL_succs1257z00_3779, obj_t BgL_first1258z00_3780,
		obj_t BgL_versions1259z00_3781, obj_t BgL_generic1260z00_3782,
		obj_t BgL_z52mark1261z52_3783, obj_t BgL_merge1262z00_3784)
	{
		{	/* SawBbv/bbv-types.sch 162 */
			return
				BGl_makezd2blockVzd2zzsaw_bbvzd2livenesszd2(CINT(BgL_label1255z00_3777),
				BgL_preds1256z00_3778, BgL_succs1257z00_3779, BgL_first1258z00_3780,
				BgL_versions1259z00_3781, BgL_generic1260z00_3782,
				(long) CINT(BgL_z52mark1261z52_3783), BgL_merge1262z00_3784);
		}

	}



/* blockV? */
	BGL_EXPORTED_DEF bool_t BGl_blockVzf3zf3zzsaw_bbvzd2livenesszd2(obj_t
		BgL_objz00_122)
	{
		{	/* SawBbv/bbv-types.sch 163 */
			{	/* SawBbv/bbv-types.sch 163 */
				obj_t BgL_classz00_4060;

				BgL_classz00_4060 = BGl_blockVz00zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_122))
					{	/* SawBbv/bbv-types.sch 163 */
						BgL_objectz00_bglt BgL_arg1807z00_4061;

						BgL_arg1807z00_4061 = (BgL_objectz00_bglt) (BgL_objz00_122);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 163 */
								long BgL_idxz00_4062;

								BgL_idxz00_4062 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4061);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4062 + 2L)) == BgL_classz00_4060);
							}
						else
							{	/* SawBbv/bbv-types.sch 163 */
								bool_t BgL_res1979z00_4065;

								{	/* SawBbv/bbv-types.sch 163 */
									obj_t BgL_oclassz00_4066;

									{	/* SawBbv/bbv-types.sch 163 */
										obj_t BgL_arg1815z00_4067;
										long BgL_arg1816z00_4068;

										BgL_arg1815z00_4067 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 163 */
											long BgL_arg1817z00_4069;

											BgL_arg1817z00_4069 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4061);
											BgL_arg1816z00_4068 = (BgL_arg1817z00_4069 - OBJECT_TYPE);
										}
										BgL_oclassz00_4066 =
											VECTOR_REF(BgL_arg1815z00_4067, BgL_arg1816z00_4068);
									}
									{	/* SawBbv/bbv-types.sch 163 */
										bool_t BgL__ortest_1115z00_4070;

										BgL__ortest_1115z00_4070 =
											(BgL_classz00_4060 == BgL_oclassz00_4066);
										if (BgL__ortest_1115z00_4070)
											{	/* SawBbv/bbv-types.sch 163 */
												BgL_res1979z00_4065 = BgL__ortest_1115z00_4070;
											}
										else
											{	/* SawBbv/bbv-types.sch 163 */
												long BgL_odepthz00_4071;

												{	/* SawBbv/bbv-types.sch 163 */
													obj_t BgL_arg1804z00_4072;

													BgL_arg1804z00_4072 = (BgL_oclassz00_4066);
													BgL_odepthz00_4071 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4072);
												}
												if ((2L < BgL_odepthz00_4071))
													{	/* SawBbv/bbv-types.sch 163 */
														obj_t BgL_arg1802z00_4073;

														{	/* SawBbv/bbv-types.sch 163 */
															obj_t BgL_arg1803z00_4074;

															BgL_arg1803z00_4074 = (BgL_oclassz00_4066);
															BgL_arg1802z00_4073 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4074,
																2L);
														}
														BgL_res1979z00_4065 =
															(BgL_arg1802z00_4073 == BgL_classz00_4060);
													}
												else
													{	/* SawBbv/bbv-types.sch 163 */
														BgL_res1979z00_4065 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1979z00_4065;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 163 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &blockV? */
	obj_t BGl_z62blockVzf3z91zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3785,
		obj_t BgL_objz00_3786)
	{
		{	/* SawBbv/bbv-types.sch 163 */
			return BBOOL(BGl_blockVzf3zf3zzsaw_bbvzd2livenesszd2(BgL_objz00_3786));
		}

	}



/* blockV-nil */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_blockVzd2nilzd2zzsaw_bbvzd2livenesszd2(void)
	{
		{	/* SawBbv/bbv-types.sch 164 */
			{	/* SawBbv/bbv-types.sch 164 */
				obj_t BgL_classz00_3007;

				BgL_classz00_3007 = BGl_blockVz00zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 164 */
					obj_t BgL__ortest_1117z00_3008;

					BgL__ortest_1117z00_3008 = BGL_CLASS_NIL(BgL_classz00_3007);
					if (CBOOL(BgL__ortest_1117z00_3008))
						{	/* SawBbv/bbv-types.sch 164 */
							return ((BgL_blockz00_bglt) BgL__ortest_1117z00_3008);
						}
					else
						{	/* SawBbv/bbv-types.sch 164 */
							return
								((BgL_blockz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3007));
						}
				}
			}
		}

	}



/* &blockV-nil */
	BgL_blockz00_bglt BGl_z62blockVzd2nilzb0zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3787)
	{
		{	/* SawBbv/bbv-types.sch 164 */
			return BGl_blockVzd2nilzd2zzsaw_bbvzd2livenesszd2();
		}

	}



/* blockV-merge */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2mergezd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt BgL_oz00_123)
	{
		{	/* SawBbv/bbv-types.sch 165 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4837;

				{
					obj_t BgL_auxz00_4838;

					{	/* SawBbv/bbv-types.sch 165 */
						BgL_objectz00_bglt BgL_tmpz00_4839;

						BgL_tmpz00_4839 = ((BgL_objectz00_bglt) BgL_oz00_123);
						BgL_auxz00_4838 = BGL_OBJECT_WIDENING(BgL_tmpz00_4839);
					}
					BgL_auxz00_4837 = ((BgL_blockvz00_bglt) BgL_auxz00_4838);
				}
				return (((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4837))->BgL_mergez00);
			}
		}

	}



/* &blockV-merge */
	obj_t BGl_z62blockVzd2mergezb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3788,
		obj_t BgL_oz00_3789)
	{
		{	/* SawBbv/bbv-types.sch 165 */
			return
				BGl_blockVzd2mergezd2zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3789));
		}

	}



/* blockV-merge-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2mergezd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_124, obj_t BgL_vz00_125)
	{
		{	/* SawBbv/bbv-types.sch 166 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4846;

				{
					obj_t BgL_auxz00_4847;

					{	/* SawBbv/bbv-types.sch 166 */
						BgL_objectz00_bglt BgL_tmpz00_4848;

						BgL_tmpz00_4848 = ((BgL_objectz00_bglt) BgL_oz00_124);
						BgL_auxz00_4847 = BGL_OBJECT_WIDENING(BgL_tmpz00_4848);
					}
					BgL_auxz00_4846 = ((BgL_blockvz00_bglt) BgL_auxz00_4847);
				}
				return
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4846))->BgL_mergez00) =
					((obj_t) BgL_vz00_125), BUNSPEC);
			}
		}

	}



/* &blockV-merge-set! */
	obj_t BGl_z62blockVzd2mergezd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3790, obj_t BgL_oz00_3791, obj_t BgL_vz00_3792)
	{
		{	/* SawBbv/bbv-types.sch 166 */
			return
				BGl_blockVzd2mergezd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3791), BgL_vz00_3792);
		}

	}



/* blockV-%mark */
	BGL_EXPORTED_DEF long
		BGl_blockVzd2z52markz80zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_126)
	{
		{	/* SawBbv/bbv-types.sch 167 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4855;

				{
					obj_t BgL_auxz00_4856;

					{	/* SawBbv/bbv-types.sch 167 */
						BgL_objectz00_bglt BgL_tmpz00_4857;

						BgL_tmpz00_4857 = ((BgL_objectz00_bglt) BgL_oz00_126);
						BgL_auxz00_4856 = BGL_OBJECT_WIDENING(BgL_tmpz00_4857);
					}
					BgL_auxz00_4855 = ((BgL_blockvz00_bglt) BgL_auxz00_4856);
				}
				return
					(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4855))->BgL_z52markz52);
			}
		}

	}



/* &blockV-%mark */
	obj_t BGl_z62blockVzd2z52markze2zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3793,
		obj_t BgL_oz00_3794)
	{
		{	/* SawBbv/bbv-types.sch 167 */
			return
				BINT(BGl_blockVzd2z52markz80zzsaw_bbvzd2livenesszd2(
					((BgL_blockz00_bglt) BgL_oz00_3794)));
		}

	}



/* blockV-%mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2z52markzd2setz12z40zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_127, long BgL_vz00_128)
	{
		{	/* SawBbv/bbv-types.sch 168 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4865;

				{
					obj_t BgL_auxz00_4866;

					{	/* SawBbv/bbv-types.sch 168 */
						BgL_objectz00_bglt BgL_tmpz00_4867;

						BgL_tmpz00_4867 = ((BgL_objectz00_bglt) BgL_oz00_127);
						BgL_auxz00_4866 = BGL_OBJECT_WIDENING(BgL_tmpz00_4867);
					}
					BgL_auxz00_4865 = ((BgL_blockvz00_bglt) BgL_auxz00_4866);
				}
				return
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4865))->BgL_z52markz52) =
					((long) BgL_vz00_128), BUNSPEC);
		}}

	}



/* &blockV-%mark-set! */
	obj_t BGl_z62blockVzd2z52markzd2setz12z22zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3795, obj_t BgL_oz00_3796, obj_t BgL_vz00_3797)
	{
		{	/* SawBbv/bbv-types.sch 168 */
			return
				BGl_blockVzd2z52markzd2setz12z40zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3796), (long) CINT(BgL_vz00_3797));
		}

	}



/* blockV-generic */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2genericzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_129)
	{
		{	/* SawBbv/bbv-types.sch 169 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4875;

				{
					obj_t BgL_auxz00_4876;

					{	/* SawBbv/bbv-types.sch 169 */
						BgL_objectz00_bglt BgL_tmpz00_4877;

						BgL_tmpz00_4877 = ((BgL_objectz00_bglt) BgL_oz00_129);
						BgL_auxz00_4876 = BGL_OBJECT_WIDENING(BgL_tmpz00_4877);
					}
					BgL_auxz00_4875 = ((BgL_blockvz00_bglt) BgL_auxz00_4876);
				}
				return
					(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4875))->BgL_genericz00);
			}
		}

	}



/* &blockV-generic */
	obj_t BGl_z62blockVzd2genericzb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3798,
		obj_t BgL_oz00_3799)
	{
		{	/* SawBbv/bbv-types.sch 169 */
			return
				BGl_blockVzd2genericzd2zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3799));
		}

	}



/* blockV-generic-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2genericzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_130, obj_t BgL_vz00_131)
	{
		{	/* SawBbv/bbv-types.sch 170 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4884;

				{
					obj_t BgL_auxz00_4885;

					{	/* SawBbv/bbv-types.sch 170 */
						BgL_objectz00_bglt BgL_tmpz00_4886;

						BgL_tmpz00_4886 = ((BgL_objectz00_bglt) BgL_oz00_130);
						BgL_auxz00_4885 = BGL_OBJECT_WIDENING(BgL_tmpz00_4886);
					}
					BgL_auxz00_4884 = ((BgL_blockvz00_bglt) BgL_auxz00_4885);
				}
				return
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4884))->BgL_genericz00) =
					((obj_t) BgL_vz00_131), BUNSPEC);
			}
		}

	}



/* &blockV-generic-set! */
	obj_t BGl_z62blockVzd2genericzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3800, obj_t BgL_oz00_3801, obj_t BgL_vz00_3802)
	{
		{	/* SawBbv/bbv-types.sch 170 */
			return
				BGl_blockVzd2genericzd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3801), BgL_vz00_3802);
		}

	}



/* blockV-versions */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2versionszd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_132)
	{
		{	/* SawBbv/bbv-types.sch 171 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4893;

				{
					obj_t BgL_auxz00_4894;

					{	/* SawBbv/bbv-types.sch 171 */
						BgL_objectz00_bglt BgL_tmpz00_4895;

						BgL_tmpz00_4895 = ((BgL_objectz00_bglt) BgL_oz00_132);
						BgL_auxz00_4894 = BGL_OBJECT_WIDENING(BgL_tmpz00_4895);
					}
					BgL_auxz00_4893 = ((BgL_blockvz00_bglt) BgL_auxz00_4894);
				}
				return
					(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4893))->BgL_versionsz00);
			}
		}

	}



/* &blockV-versions */
	obj_t BGl_z62blockVzd2versionszb0zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3803, obj_t BgL_oz00_3804)
	{
		{	/* SawBbv/bbv-types.sch 171 */
			return
				BGl_blockVzd2versionszd2zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3804));
		}

	}



/* blockV-versions-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2versionszd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_133, obj_t BgL_vz00_134)
	{
		{	/* SawBbv/bbv-types.sch 172 */
			{
				BgL_blockvz00_bglt BgL_auxz00_4902;

				{
					obj_t BgL_auxz00_4903;

					{	/* SawBbv/bbv-types.sch 172 */
						BgL_objectz00_bglt BgL_tmpz00_4904;

						BgL_tmpz00_4904 = ((BgL_objectz00_bglt) BgL_oz00_133);
						BgL_auxz00_4903 = BGL_OBJECT_WIDENING(BgL_tmpz00_4904);
					}
					BgL_auxz00_4902 = ((BgL_blockvz00_bglt) BgL_auxz00_4903);
				}
				return
					((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4902))->BgL_versionsz00) =
					((obj_t) BgL_vz00_134), BUNSPEC);
			}
		}

	}



/* &blockV-versions-set! */
	obj_t BGl_z62blockVzd2versionszd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3805, obj_t BgL_oz00_3806, obj_t BgL_vz00_3807)
	{
		{	/* SawBbv/bbv-types.sch 172 */
			return
				BGl_blockVzd2versionszd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3806), BgL_vz00_3807);
		}

	}



/* blockV-first */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2firstzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt BgL_oz00_135)
	{
		{	/* SawBbv/bbv-types.sch 173 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_135)))->BgL_firstz00);
		}

	}



/* &blockV-first */
	obj_t BGl_z62blockVzd2firstzb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3808,
		obj_t BgL_oz00_3809)
	{
		{	/* SawBbv/bbv-types.sch 173 */
			return
				BGl_blockVzd2firstzd2zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3809));
		}

	}



/* blockV-first-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2firstzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_136, obj_t BgL_vz00_137)
	{
		{	/* SawBbv/bbv-types.sch 174 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_136)))->BgL_firstz00) =
				((obj_t) BgL_vz00_137), BUNSPEC);
		}

	}



/* &blockV-first-set! */
	obj_t BGl_z62blockVzd2firstzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3810, obj_t BgL_oz00_3811, obj_t BgL_vz00_3812)
	{
		{	/* SawBbv/bbv-types.sch 174 */
			return
				BGl_blockVzd2firstzd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3811), BgL_vz00_3812);
		}

	}



/* blockV-succs */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2succszd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt BgL_oz00_138)
	{
		{	/* SawBbv/bbv-types.sch 175 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_138)))->BgL_succsz00);
		}

	}



/* &blockV-succs */
	obj_t BGl_z62blockVzd2succszb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3813,
		obj_t BgL_oz00_3814)
	{
		{	/* SawBbv/bbv-types.sch 175 */
			return
				BGl_blockVzd2succszd2zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3814));
		}

	}



/* blockV-succs-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2succszd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_139, obj_t BgL_vz00_140)
	{
		{	/* SawBbv/bbv-types.sch 176 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_139)))->BgL_succsz00) =
				((obj_t) BgL_vz00_140), BUNSPEC);
		}

	}



/* &blockV-succs-set! */
	obj_t BGl_z62blockVzd2succszd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3815, obj_t BgL_oz00_3816, obj_t BgL_vz00_3817)
	{
		{	/* SawBbv/bbv-types.sch 176 */
			return
				BGl_blockVzd2succszd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3816), BgL_vz00_3817);
		}

	}



/* blockV-preds */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2predszd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt BgL_oz00_141)
	{
		{	/* SawBbv/bbv-types.sch 177 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_141)))->BgL_predsz00);
		}

	}



/* &blockV-preds */
	obj_t BGl_z62blockVzd2predszb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3818,
		obj_t BgL_oz00_3819)
	{
		{	/* SawBbv/bbv-types.sch 177 */
			return
				BGl_blockVzd2predszd2zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3819));
		}

	}



/* blockV-preds-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2predszd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_142, obj_t BgL_vz00_143)
	{
		{	/* SawBbv/bbv-types.sch 178 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_142)))->BgL_predsz00) =
				((obj_t) BgL_vz00_143), BUNSPEC);
		}

	}



/* &blockV-preds-set! */
	obj_t BGl_z62blockVzd2predszd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3820, obj_t BgL_oz00_3821, obj_t BgL_vz00_3822)
	{
		{	/* SawBbv/bbv-types.sch 178 */
			return
				BGl_blockVzd2predszd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3821), BgL_vz00_3822);
		}

	}



/* blockV-label */
	BGL_EXPORTED_DEF int
		BGl_blockVzd2labelzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt BgL_oz00_144)
	{
		{	/* SawBbv/bbv-types.sch 179 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_144)))->BgL_labelz00);
		}

	}



/* &blockV-label */
	obj_t BGl_z62blockVzd2labelzb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3823,
		obj_t BgL_oz00_3824)
	{
		{	/* SawBbv/bbv-types.sch 179 */
			return
				BINT(BGl_blockVzd2labelzd2zzsaw_bbvzd2livenesszd2(
					((BgL_blockz00_bglt) BgL_oz00_3824)));
		}

	}



/* blockV-label-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockVzd2labelzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_145, int BgL_vz00_146)
	{
		{	/* SawBbv/bbv-types.sch 180 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_145)))->BgL_labelz00) =
				((int) BgL_vz00_146), BUNSPEC);
		}

	}



/* &blockV-label-set! */
	obj_t BGl_z62blockVzd2labelzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3825, obj_t BgL_oz00_3826, obj_t BgL_vz00_3827)
	{
		{	/* SawBbv/bbv-types.sch 180 */
			return
				BGl_blockVzd2labelzd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3826), CINT(BgL_vz00_3827));
		}

	}



/* make-blockS */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_makezd2blockSzd2zzsaw_bbvzd2livenesszd2(int BgL_label1239z00_147,
		obj_t BgL_preds1240z00_148, obj_t BgL_succs1241z00_149,
		obj_t BgL_first1242z00_150, long BgL_z52mark1243z52_151,
		obj_t BgL_z52hash1244z52_152, obj_t BgL_z52blacklist1245z52_153,
		BgL_bbvzd2ctxzd2_bglt BgL_ctx1246z00_154,
		BgL_blockz00_bglt BgL_parent1247z00_155, long BgL_gccnt1248z00_156,
		long BgL_gcmark1249z00_157, obj_t BgL_mblock1250z00_158,
		obj_t BgL_creator1251z00_159, obj_t BgL_merges1252z00_160,
		bool_t BgL_asleep1253z00_161)
	{
		{	/* SawBbv/bbv-types.sch 183 */
			{	/* SawBbv/bbv-types.sch 183 */
				BgL_blockz00_bglt BgL_new1180z00_4075;

				{	/* SawBbv/bbv-types.sch 183 */
					BgL_blockz00_bglt BgL_tmp1178z00_4076;
					BgL_blocksz00_bglt BgL_wide1179z00_4077;

					{
						BgL_blockz00_bglt BgL_auxz00_4945;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_blockz00_bglt BgL_new1177z00_4078;

							BgL_new1177z00_4078 =
								((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_blockz00_bgl))));
							{	/* SawBbv/bbv-types.sch 183 */
								long BgL_arg1546z00_4079;

								BgL_arg1546z00_4079 = BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1177z00_4078),
									BgL_arg1546z00_4079);
							}
							{	/* SawBbv/bbv-types.sch 183 */
								BgL_objectz00_bglt BgL_tmpz00_4950;

								BgL_tmpz00_4950 = ((BgL_objectz00_bglt) BgL_new1177z00_4078);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4950, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1177z00_4078);
							BgL_auxz00_4945 = BgL_new1177z00_4078;
						}
						BgL_tmp1178z00_4076 = ((BgL_blockz00_bglt) BgL_auxz00_4945);
					}
					BgL_wide1179z00_4077 =
						((BgL_blocksz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_blocksz00_bgl))));
					{	/* SawBbv/bbv-types.sch 183 */
						obj_t BgL_auxz00_4958;
						BgL_objectz00_bglt BgL_tmpz00_4956;

						BgL_auxz00_4958 = ((obj_t) BgL_wide1179z00_4077);
						BgL_tmpz00_4956 = ((BgL_objectz00_bglt) BgL_tmp1178z00_4076);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4956, BgL_auxz00_4958);
					}
					((BgL_objectz00_bglt) BgL_tmp1178z00_4076);
					{	/* SawBbv/bbv-types.sch 183 */
						long BgL_arg1544z00_4080;

						BgL_arg1544z00_4080 =
							BGL_CLASS_NUM(BGl_blockSz00zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1178z00_4076), BgL_arg1544z00_4080);
					}
					BgL_new1180z00_4075 = ((BgL_blockz00_bglt) BgL_tmp1178z00_4076);
				}
				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_new1180z00_4075)))->BgL_labelz00) =
					((int) BgL_label1239z00_147), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1180z00_4075)))->BgL_predsz00) =
					((obj_t) BgL_preds1240z00_148), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1180z00_4075)))->BgL_succsz00) =
					((obj_t) BgL_succs1241z00_149), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1180z00_4075)))->BgL_firstz00) =
					((obj_t) BgL_first1242z00_150), BUNSPEC);
				{
					BgL_blocksz00_bglt BgL_auxz00_4974;

					{
						obj_t BgL_auxz00_4975;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_4976;

							BgL_tmpz00_4976 = ((BgL_objectz00_bglt) BgL_new1180z00_4075);
							BgL_auxz00_4975 = BGL_OBJECT_WIDENING(BgL_tmpz00_4976);
						}
						BgL_auxz00_4974 = ((BgL_blocksz00_bglt) BgL_auxz00_4975);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4974))->BgL_z52markz52) =
						((long) BgL_z52mark1243z52_151), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_4981;

					{
						obj_t BgL_auxz00_4982;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_4983;

							BgL_tmpz00_4983 = ((BgL_objectz00_bglt) BgL_new1180z00_4075);
							BgL_auxz00_4982 = BGL_OBJECT_WIDENING(BgL_tmpz00_4983);
						}
						BgL_auxz00_4981 = ((BgL_blocksz00_bglt) BgL_auxz00_4982);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4981))->BgL_z52hashz52) =
						((obj_t) BgL_z52hash1244z52_152), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_4988;

					{
						obj_t BgL_auxz00_4989;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_4990;

							BgL_tmpz00_4990 = ((BgL_objectz00_bglt) BgL_new1180z00_4075);
							BgL_auxz00_4989 = BGL_OBJECT_WIDENING(BgL_tmpz00_4990);
						}
						BgL_auxz00_4988 = ((BgL_blocksz00_bglt) BgL_auxz00_4989);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4988))->
							BgL_z52blacklistz52) =
						((obj_t) BgL_z52blacklist1245z52_153), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_4995;

					{
						obj_t BgL_auxz00_4996;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_4997;

							BgL_tmpz00_4997 = ((BgL_objectz00_bglt) BgL_new1180z00_4075);
							BgL_auxz00_4996 = BGL_OBJECT_WIDENING(BgL_tmpz00_4997);
						}
						BgL_auxz00_4995 = ((BgL_blocksz00_bglt) BgL_auxz00_4996);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4995))->BgL_ctxz00) =
						((BgL_bbvzd2ctxzd2_bglt) BgL_ctx1246z00_154), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_5002;

					{
						obj_t BgL_auxz00_5003;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_5004;

							BgL_tmpz00_5004 = ((BgL_objectz00_bglt) BgL_new1180z00_4075);
							BgL_auxz00_5003 = BGL_OBJECT_WIDENING(BgL_tmpz00_5004);
						}
						BgL_auxz00_5002 = ((BgL_blocksz00_bglt) BgL_auxz00_5003);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5002))->BgL_parentz00) =
						((BgL_blockz00_bglt) BgL_parent1247z00_155), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_5009;

					{
						obj_t BgL_auxz00_5010;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_5011;

							BgL_tmpz00_5011 = ((BgL_objectz00_bglt) BgL_new1180z00_4075);
							BgL_auxz00_5010 = BGL_OBJECT_WIDENING(BgL_tmpz00_5011);
						}
						BgL_auxz00_5009 = ((BgL_blocksz00_bglt) BgL_auxz00_5010);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5009))->BgL_gccntz00) =
						((long) BgL_gccnt1248z00_156), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_5016;

					{
						obj_t BgL_auxz00_5017;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_5018;

							BgL_tmpz00_5018 = ((BgL_objectz00_bglt) BgL_new1180z00_4075);
							BgL_auxz00_5017 = BGL_OBJECT_WIDENING(BgL_tmpz00_5018);
						}
						BgL_auxz00_5016 = ((BgL_blocksz00_bglt) BgL_auxz00_5017);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5016))->BgL_gcmarkz00) =
						((long) BgL_gcmark1249z00_157), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_5023;

					{
						obj_t BgL_auxz00_5024;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_5025;

							BgL_tmpz00_5025 = ((BgL_objectz00_bglt) BgL_new1180z00_4075);
							BgL_auxz00_5024 = BGL_OBJECT_WIDENING(BgL_tmpz00_5025);
						}
						BgL_auxz00_5023 = ((BgL_blocksz00_bglt) BgL_auxz00_5024);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5023))->BgL_mblockz00) =
						((obj_t) BgL_mblock1250z00_158), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_5030;

					{
						obj_t BgL_auxz00_5031;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_5032;

							BgL_tmpz00_5032 = ((BgL_objectz00_bglt) BgL_new1180z00_4075);
							BgL_auxz00_5031 = BGL_OBJECT_WIDENING(BgL_tmpz00_5032);
						}
						BgL_auxz00_5030 = ((BgL_blocksz00_bglt) BgL_auxz00_5031);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5030))->BgL_creatorz00) =
						((obj_t) BgL_creator1251z00_159), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_5037;

					{
						obj_t BgL_auxz00_5038;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_5039;

							BgL_tmpz00_5039 = ((BgL_objectz00_bglt) BgL_new1180z00_4075);
							BgL_auxz00_5038 = BGL_OBJECT_WIDENING(BgL_tmpz00_5039);
						}
						BgL_auxz00_5037 = ((BgL_blocksz00_bglt) BgL_auxz00_5038);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5037))->BgL_mergesz00) =
						((obj_t) BgL_merges1252z00_160), BUNSPEC);
				}
				{
					BgL_blocksz00_bglt BgL_auxz00_5044;

					{
						obj_t BgL_auxz00_5045;

						{	/* SawBbv/bbv-types.sch 183 */
							BgL_objectz00_bglt BgL_tmpz00_5046;

							BgL_tmpz00_5046 = ((BgL_objectz00_bglt) BgL_new1180z00_4075);
							BgL_auxz00_5045 = BGL_OBJECT_WIDENING(BgL_tmpz00_5046);
						}
						BgL_auxz00_5044 = ((BgL_blocksz00_bglt) BgL_auxz00_5045);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5044))->BgL_asleepz00) =
						((bool_t) BgL_asleep1253z00_161), BUNSPEC);
				}
				return BgL_new1180z00_4075;
			}
		}

	}



/* &make-blockS */
	BgL_blockz00_bglt BGl_z62makezd2blockSzb0zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3828, obj_t BgL_label1239z00_3829, obj_t BgL_preds1240z00_3830,
		obj_t BgL_succs1241z00_3831, obj_t BgL_first1242z00_3832,
		obj_t BgL_z52mark1243z52_3833, obj_t BgL_z52hash1244z52_3834,
		obj_t BgL_z52blacklist1245z52_3835, obj_t BgL_ctx1246z00_3836,
		obj_t BgL_parent1247z00_3837, obj_t BgL_gccnt1248z00_3838,
		obj_t BgL_gcmark1249z00_3839, obj_t BgL_mblock1250z00_3840,
		obj_t BgL_creator1251z00_3841, obj_t BgL_merges1252z00_3842,
		obj_t BgL_asleep1253z00_3843)
	{
		{	/* SawBbv/bbv-types.sch 183 */
			return
				BGl_makezd2blockSzd2zzsaw_bbvzd2livenesszd2(CINT(BgL_label1239z00_3829),
				BgL_preds1240z00_3830, BgL_succs1241z00_3831, BgL_first1242z00_3832,
				(long) CINT(BgL_z52mark1243z52_3833), BgL_z52hash1244z52_3834,
				BgL_z52blacklist1245z52_3835,
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctx1246z00_3836),
				((BgL_blockz00_bglt) BgL_parent1247z00_3837),
				(long) CINT(BgL_gccnt1248z00_3838), (long) CINT(BgL_gcmark1249z00_3839),
				BgL_mblock1250z00_3840, BgL_creator1251z00_3841, BgL_merges1252z00_3842,
				CBOOL(BgL_asleep1253z00_3843));
		}

	}



/* blockS? */
	BGL_EXPORTED_DEF bool_t BGl_blockSzf3zf3zzsaw_bbvzd2livenesszd2(obj_t
		BgL_objz00_162)
	{
		{	/* SawBbv/bbv-types.sch 184 */
			{	/* SawBbv/bbv-types.sch 184 */
				obj_t BgL_classz00_4081;

				BgL_classz00_4081 = BGl_blockSz00zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_162))
					{	/* SawBbv/bbv-types.sch 184 */
						BgL_objectz00_bglt BgL_arg1807z00_4082;

						BgL_arg1807z00_4082 = (BgL_objectz00_bglt) (BgL_objz00_162);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 184 */
								long BgL_idxz00_4083;

								BgL_idxz00_4083 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4082);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4083 + 2L)) == BgL_classz00_4081);
							}
						else
							{	/* SawBbv/bbv-types.sch 184 */
								bool_t BgL_res1980z00_4086;

								{	/* SawBbv/bbv-types.sch 184 */
									obj_t BgL_oclassz00_4087;

									{	/* SawBbv/bbv-types.sch 184 */
										obj_t BgL_arg1815z00_4088;
										long BgL_arg1816z00_4089;

										BgL_arg1815z00_4088 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 184 */
											long BgL_arg1817z00_4090;

											BgL_arg1817z00_4090 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4082);
											BgL_arg1816z00_4089 = (BgL_arg1817z00_4090 - OBJECT_TYPE);
										}
										BgL_oclassz00_4087 =
											VECTOR_REF(BgL_arg1815z00_4088, BgL_arg1816z00_4089);
									}
									{	/* SawBbv/bbv-types.sch 184 */
										bool_t BgL__ortest_1115z00_4091;

										BgL__ortest_1115z00_4091 =
											(BgL_classz00_4081 == BgL_oclassz00_4087);
										if (BgL__ortest_1115z00_4091)
											{	/* SawBbv/bbv-types.sch 184 */
												BgL_res1980z00_4086 = BgL__ortest_1115z00_4091;
											}
										else
											{	/* SawBbv/bbv-types.sch 184 */
												long BgL_odepthz00_4092;

												{	/* SawBbv/bbv-types.sch 184 */
													obj_t BgL_arg1804z00_4093;

													BgL_arg1804z00_4093 = (BgL_oclassz00_4087);
													BgL_odepthz00_4092 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4093);
												}
												if ((2L < BgL_odepthz00_4092))
													{	/* SawBbv/bbv-types.sch 184 */
														obj_t BgL_arg1802z00_4094;

														{	/* SawBbv/bbv-types.sch 184 */
															obj_t BgL_arg1803z00_4095;

															BgL_arg1803z00_4095 = (BgL_oclassz00_4087);
															BgL_arg1802z00_4094 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4095,
																2L);
														}
														BgL_res1980z00_4086 =
															(BgL_arg1802z00_4094 == BgL_classz00_4081);
													}
												else
													{	/* SawBbv/bbv-types.sch 184 */
														BgL_res1980z00_4086 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1980z00_4086;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 184 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &blockS? */
	obj_t BGl_z62blockSzf3z91zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3844,
		obj_t BgL_objz00_3845)
	{
		{	/* SawBbv/bbv-types.sch 184 */
			return BBOOL(BGl_blockSzf3zf3zzsaw_bbvzd2livenesszd2(BgL_objz00_3845));
		}

	}



/* blockS-nil */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_blockSzd2nilzd2zzsaw_bbvzd2livenesszd2(void)
	{
		{	/* SawBbv/bbv-types.sch 185 */
			{	/* SawBbv/bbv-types.sch 185 */
				obj_t BgL_classz00_3076;

				BgL_classz00_3076 = BGl_blockSz00zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 185 */
					obj_t BgL__ortest_1117z00_3077;

					BgL__ortest_1117z00_3077 = BGL_CLASS_NIL(BgL_classz00_3076);
					if (CBOOL(BgL__ortest_1117z00_3077))
						{	/* SawBbv/bbv-types.sch 185 */
							return ((BgL_blockz00_bglt) BgL__ortest_1117z00_3077);
						}
					else
						{	/* SawBbv/bbv-types.sch 185 */
							return
								((BgL_blockz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3076));
						}
				}
			}
		}

	}



/* &blockS-nil */
	BgL_blockz00_bglt BGl_z62blockSzd2nilzb0zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3846)
	{
		{	/* SawBbv/bbv-types.sch 185 */
			return BGl_blockSzd2nilzd2zzsaw_bbvzd2livenesszd2();
		}

	}



/* blockS-asleep */
	BGL_EXPORTED_DEF bool_t
		BGl_blockSzd2asleepzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_163)
	{
		{	/* SawBbv/bbv-types.sch 186 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5090;

				{
					obj_t BgL_auxz00_5091;

					{	/* SawBbv/bbv-types.sch 186 */
						BgL_objectz00_bglt BgL_tmpz00_5092;

						BgL_tmpz00_5092 = ((BgL_objectz00_bglt) BgL_oz00_163);
						BgL_auxz00_5091 = BGL_OBJECT_WIDENING(BgL_tmpz00_5092);
					}
					BgL_auxz00_5090 = ((BgL_blocksz00_bglt) BgL_auxz00_5091);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5090))->BgL_asleepz00);
			}
		}

	}



/* &blockS-asleep */
	obj_t BGl_z62blockSzd2asleepzb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3847,
		obj_t BgL_oz00_3848)
	{
		{	/* SawBbv/bbv-types.sch 186 */
			return
				BBOOL(BGl_blockSzd2asleepzd2zzsaw_bbvzd2livenesszd2(
					((BgL_blockz00_bglt) BgL_oz00_3848)));
		}

	}



/* blockS-asleep-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2asleepzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_164, bool_t BgL_vz00_165)
	{
		{	/* SawBbv/bbv-types.sch 187 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5100;

				{
					obj_t BgL_auxz00_5101;

					{	/* SawBbv/bbv-types.sch 187 */
						BgL_objectz00_bglt BgL_tmpz00_5102;

						BgL_tmpz00_5102 = ((BgL_objectz00_bglt) BgL_oz00_164);
						BgL_auxz00_5101 = BGL_OBJECT_WIDENING(BgL_tmpz00_5102);
					}
					BgL_auxz00_5100 = ((BgL_blocksz00_bglt) BgL_auxz00_5101);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5100))->BgL_asleepz00) =
					((bool_t) BgL_vz00_165), BUNSPEC);
			}
		}

	}



/* &blockS-asleep-set! */
	obj_t BGl_z62blockSzd2asleepzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3849, obj_t BgL_oz00_3850, obj_t BgL_vz00_3851)
	{
		{	/* SawBbv/bbv-types.sch 187 */
			return
				BGl_blockSzd2asleepzd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3850), CBOOL(BgL_vz00_3851));
		}

	}



/* blockS-merges */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2mergeszd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_166)
	{
		{	/* SawBbv/bbv-types.sch 188 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5110;

				{
					obj_t BgL_auxz00_5111;

					{	/* SawBbv/bbv-types.sch 188 */
						BgL_objectz00_bglt BgL_tmpz00_5112;

						BgL_tmpz00_5112 = ((BgL_objectz00_bglt) BgL_oz00_166);
						BgL_auxz00_5111 = BGL_OBJECT_WIDENING(BgL_tmpz00_5112);
					}
					BgL_auxz00_5110 = ((BgL_blocksz00_bglt) BgL_auxz00_5111);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5110))->BgL_mergesz00);
			}
		}

	}



/* &blockS-merges */
	obj_t BGl_z62blockSzd2mergeszb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3852,
		obj_t BgL_oz00_3853)
	{
		{	/* SawBbv/bbv-types.sch 188 */
			return
				BGl_blockSzd2mergeszd2zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3853));
		}

	}



/* blockS-merges-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2mergeszd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_167, obj_t BgL_vz00_168)
	{
		{	/* SawBbv/bbv-types.sch 189 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5119;

				{
					obj_t BgL_auxz00_5120;

					{	/* SawBbv/bbv-types.sch 189 */
						BgL_objectz00_bglt BgL_tmpz00_5121;

						BgL_tmpz00_5121 = ((BgL_objectz00_bglt) BgL_oz00_167);
						BgL_auxz00_5120 = BGL_OBJECT_WIDENING(BgL_tmpz00_5121);
					}
					BgL_auxz00_5119 = ((BgL_blocksz00_bglt) BgL_auxz00_5120);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5119))->BgL_mergesz00) =
					((obj_t) BgL_vz00_168), BUNSPEC);
			}
		}

	}



/* &blockS-merges-set! */
	obj_t BGl_z62blockSzd2mergeszd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3854, obj_t BgL_oz00_3855, obj_t BgL_vz00_3856)
	{
		{	/* SawBbv/bbv-types.sch 189 */
			return
				BGl_blockSzd2mergeszd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3855), BgL_vz00_3856);
		}

	}



/* blockS-creator */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2creatorzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_169)
	{
		{	/* SawBbv/bbv-types.sch 190 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5128;

				{
					obj_t BgL_auxz00_5129;

					{	/* SawBbv/bbv-types.sch 190 */
						BgL_objectz00_bglt BgL_tmpz00_5130;

						BgL_tmpz00_5130 = ((BgL_objectz00_bglt) BgL_oz00_169);
						BgL_auxz00_5129 = BGL_OBJECT_WIDENING(BgL_tmpz00_5130);
					}
					BgL_auxz00_5128 = ((BgL_blocksz00_bglt) BgL_auxz00_5129);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5128))->BgL_creatorz00);
			}
		}

	}



/* &blockS-creator */
	obj_t BGl_z62blockSzd2creatorzb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3857,
		obj_t BgL_oz00_3858)
	{
		{	/* SawBbv/bbv-types.sch 190 */
			return
				BGl_blockSzd2creatorzd2zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3858));
		}

	}



/* blockS-mblock */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2mblockzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_172)
	{
		{	/* SawBbv/bbv-types.sch 192 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5137;

				{
					obj_t BgL_auxz00_5138;

					{	/* SawBbv/bbv-types.sch 192 */
						BgL_objectz00_bglt BgL_tmpz00_5139;

						BgL_tmpz00_5139 = ((BgL_objectz00_bglt) BgL_oz00_172);
						BgL_auxz00_5138 = BGL_OBJECT_WIDENING(BgL_tmpz00_5139);
					}
					BgL_auxz00_5137 = ((BgL_blocksz00_bglt) BgL_auxz00_5138);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5137))->BgL_mblockz00);
			}
		}

	}



/* &blockS-mblock */
	obj_t BGl_z62blockSzd2mblockzb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3859,
		obj_t BgL_oz00_3860)
	{
		{	/* SawBbv/bbv-types.sch 192 */
			return
				BGl_blockSzd2mblockzd2zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3860));
		}

	}



/* blockS-mblock-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2mblockzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_173, obj_t BgL_vz00_174)
	{
		{	/* SawBbv/bbv-types.sch 193 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5146;

				{
					obj_t BgL_auxz00_5147;

					{	/* SawBbv/bbv-types.sch 193 */
						BgL_objectz00_bglt BgL_tmpz00_5148;

						BgL_tmpz00_5148 = ((BgL_objectz00_bglt) BgL_oz00_173);
						BgL_auxz00_5147 = BGL_OBJECT_WIDENING(BgL_tmpz00_5148);
					}
					BgL_auxz00_5146 = ((BgL_blocksz00_bglt) BgL_auxz00_5147);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5146))->BgL_mblockz00) =
					((obj_t) BgL_vz00_174), BUNSPEC);
			}
		}

	}



/* &blockS-mblock-set! */
	obj_t BGl_z62blockSzd2mblockzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3861, obj_t BgL_oz00_3862, obj_t BgL_vz00_3863)
	{
		{	/* SawBbv/bbv-types.sch 193 */
			return
				BGl_blockSzd2mblockzd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3862), BgL_vz00_3863);
		}

	}



/* blockS-gcmark */
	BGL_EXPORTED_DEF long
		BGl_blockSzd2gcmarkzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_175)
	{
		{	/* SawBbv/bbv-types.sch 194 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5155;

				{
					obj_t BgL_auxz00_5156;

					{	/* SawBbv/bbv-types.sch 194 */
						BgL_objectz00_bglt BgL_tmpz00_5157;

						BgL_tmpz00_5157 = ((BgL_objectz00_bglt) BgL_oz00_175);
						BgL_auxz00_5156 = BGL_OBJECT_WIDENING(BgL_tmpz00_5157);
					}
					BgL_auxz00_5155 = ((BgL_blocksz00_bglt) BgL_auxz00_5156);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5155))->BgL_gcmarkz00);
			}
		}

	}



/* &blockS-gcmark */
	obj_t BGl_z62blockSzd2gcmarkzb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3864,
		obj_t BgL_oz00_3865)
	{
		{	/* SawBbv/bbv-types.sch 194 */
			return
				BINT(BGl_blockSzd2gcmarkzd2zzsaw_bbvzd2livenesszd2(
					((BgL_blockz00_bglt) BgL_oz00_3865)));
		}

	}



/* blockS-gcmark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2gcmarkzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_176, long BgL_vz00_177)
	{
		{	/* SawBbv/bbv-types.sch 195 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5165;

				{
					obj_t BgL_auxz00_5166;

					{	/* SawBbv/bbv-types.sch 195 */
						BgL_objectz00_bglt BgL_tmpz00_5167;

						BgL_tmpz00_5167 = ((BgL_objectz00_bglt) BgL_oz00_176);
						BgL_auxz00_5166 = BGL_OBJECT_WIDENING(BgL_tmpz00_5167);
					}
					BgL_auxz00_5165 = ((BgL_blocksz00_bglt) BgL_auxz00_5166);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5165))->BgL_gcmarkz00) =
					((long) BgL_vz00_177), BUNSPEC);
		}}

	}



/* &blockS-gcmark-set! */
	obj_t BGl_z62blockSzd2gcmarkzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3866, obj_t BgL_oz00_3867, obj_t BgL_vz00_3868)
	{
		{	/* SawBbv/bbv-types.sch 195 */
			return
				BGl_blockSzd2gcmarkzd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3867), (long) CINT(BgL_vz00_3868));
		}

	}



/* blockS-gccnt */
	BGL_EXPORTED_DEF long
		BGl_blockSzd2gccntzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt BgL_oz00_178)
	{
		{	/* SawBbv/bbv-types.sch 196 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5175;

				{
					obj_t BgL_auxz00_5176;

					{	/* SawBbv/bbv-types.sch 196 */
						BgL_objectz00_bglt BgL_tmpz00_5177;

						BgL_tmpz00_5177 = ((BgL_objectz00_bglt) BgL_oz00_178);
						BgL_auxz00_5176 = BGL_OBJECT_WIDENING(BgL_tmpz00_5177);
					}
					BgL_auxz00_5175 = ((BgL_blocksz00_bglt) BgL_auxz00_5176);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5175))->BgL_gccntz00);
			}
		}

	}



/* &blockS-gccnt */
	obj_t BGl_z62blockSzd2gccntzb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3869,
		obj_t BgL_oz00_3870)
	{
		{	/* SawBbv/bbv-types.sch 196 */
			return
				BINT(BGl_blockSzd2gccntzd2zzsaw_bbvzd2livenesszd2(
					((BgL_blockz00_bglt) BgL_oz00_3870)));
		}

	}



/* blockS-gccnt-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2gccntzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_179, long BgL_vz00_180)
	{
		{	/* SawBbv/bbv-types.sch 197 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5185;

				{
					obj_t BgL_auxz00_5186;

					{	/* SawBbv/bbv-types.sch 197 */
						BgL_objectz00_bglt BgL_tmpz00_5187;

						BgL_tmpz00_5187 = ((BgL_objectz00_bglt) BgL_oz00_179);
						BgL_auxz00_5186 = BGL_OBJECT_WIDENING(BgL_tmpz00_5187);
					}
					BgL_auxz00_5185 = ((BgL_blocksz00_bglt) BgL_auxz00_5186);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5185))->BgL_gccntz00) =
					((long) BgL_vz00_180), BUNSPEC);
		}}

	}



/* &blockS-gccnt-set! */
	obj_t BGl_z62blockSzd2gccntzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3871, obj_t BgL_oz00_3872, obj_t BgL_vz00_3873)
	{
		{	/* SawBbv/bbv-types.sch 197 */
			return
				BGl_blockSzd2gccntzd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3872), (long) CINT(BgL_vz00_3873));
		}

	}



/* blockS-parent */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_blockSzd2parentzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_181)
	{
		{	/* SawBbv/bbv-types.sch 198 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5195;

				{
					obj_t BgL_auxz00_5196;

					{	/* SawBbv/bbv-types.sch 198 */
						BgL_objectz00_bglt BgL_tmpz00_5197;

						BgL_tmpz00_5197 = ((BgL_objectz00_bglt) BgL_oz00_181);
						BgL_auxz00_5196 = BGL_OBJECT_WIDENING(BgL_tmpz00_5197);
					}
					BgL_auxz00_5195 = ((BgL_blocksz00_bglt) BgL_auxz00_5196);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5195))->BgL_parentz00);
			}
		}

	}



/* &blockS-parent */
	BgL_blockz00_bglt BGl_z62blockSzd2parentzb0zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3874, obj_t BgL_oz00_3875)
	{
		{	/* SawBbv/bbv-types.sch 198 */
			return
				BGl_blockSzd2parentzd2zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3875));
		}

	}



/* blockS-ctx */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_blockSzd2ctxzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt BgL_oz00_184)
	{
		{	/* SawBbv/bbv-types.sch 200 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5204;

				{
					obj_t BgL_auxz00_5205;

					{	/* SawBbv/bbv-types.sch 200 */
						BgL_objectz00_bglt BgL_tmpz00_5206;

						BgL_tmpz00_5206 = ((BgL_objectz00_bglt) BgL_oz00_184);
						BgL_auxz00_5205 = BGL_OBJECT_WIDENING(BgL_tmpz00_5206);
					}
					BgL_auxz00_5204 = ((BgL_blocksz00_bglt) BgL_auxz00_5205);
				}
				return (((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5204))->BgL_ctxz00);
			}
		}

	}



/* &blockS-ctx */
	BgL_bbvzd2ctxzd2_bglt BGl_z62blockSzd2ctxzb0zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3876, obj_t BgL_oz00_3877)
	{
		{	/* SawBbv/bbv-types.sch 200 */
			return
				BGl_blockSzd2ctxzd2zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3877));
		}

	}



/* blockS-%blacklist */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2z52blacklistz80zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_187)
	{
		{	/* SawBbv/bbv-types.sch 202 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5213;

				{
					obj_t BgL_auxz00_5214;

					{	/* SawBbv/bbv-types.sch 202 */
						BgL_objectz00_bglt BgL_tmpz00_5215;

						BgL_tmpz00_5215 = ((BgL_objectz00_bglt) BgL_oz00_187);
						BgL_auxz00_5214 = BGL_OBJECT_WIDENING(BgL_tmpz00_5215);
					}
					BgL_auxz00_5213 = ((BgL_blocksz00_bglt) BgL_auxz00_5214);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5213))->
					BgL_z52blacklistz52);
			}
		}

	}



/* &blockS-%blacklist */
	obj_t BGl_z62blockSzd2z52blacklistze2zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3878, obj_t BgL_oz00_3879)
	{
		{	/* SawBbv/bbv-types.sch 202 */
			return
				BGl_blockSzd2z52blacklistz80zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3879));
		}

	}



/* blockS-%blacklist-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2z52blacklistzd2setz12z40zzsaw_bbvzd2livenesszd2
		(BgL_blockz00_bglt BgL_oz00_188, obj_t BgL_vz00_189)
	{
		{	/* SawBbv/bbv-types.sch 203 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5222;

				{
					obj_t BgL_auxz00_5223;

					{	/* SawBbv/bbv-types.sch 203 */
						BgL_objectz00_bglt BgL_tmpz00_5224;

						BgL_tmpz00_5224 = ((BgL_objectz00_bglt) BgL_oz00_188);
						BgL_auxz00_5223 = BGL_OBJECT_WIDENING(BgL_tmpz00_5224);
					}
					BgL_auxz00_5222 = ((BgL_blocksz00_bglt) BgL_auxz00_5223);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5222))->
						BgL_z52blacklistz52) = ((obj_t) BgL_vz00_189), BUNSPEC);
			}
		}

	}



/* &blockS-%blacklist-set! */
	obj_t BGl_z62blockSzd2z52blacklistzd2setz12z22zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3880, obj_t BgL_oz00_3881, obj_t BgL_vz00_3882)
	{
		{	/* SawBbv/bbv-types.sch 203 */
			return
				BGl_blockSzd2z52blacklistzd2setz12z40zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3881), BgL_vz00_3882);
		}

	}



/* blockS-%hash */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2z52hashz80zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_190)
	{
		{	/* SawBbv/bbv-types.sch 204 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5231;

				{
					obj_t BgL_auxz00_5232;

					{	/* SawBbv/bbv-types.sch 204 */
						BgL_objectz00_bglt BgL_tmpz00_5233;

						BgL_tmpz00_5233 = ((BgL_objectz00_bglt) BgL_oz00_190);
						BgL_auxz00_5232 = BGL_OBJECT_WIDENING(BgL_tmpz00_5233);
					}
					BgL_auxz00_5231 = ((BgL_blocksz00_bglt) BgL_auxz00_5232);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5231))->BgL_z52hashz52);
			}
		}

	}



/* &blockS-%hash */
	obj_t BGl_z62blockSzd2z52hashze2zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3883,
		obj_t BgL_oz00_3884)
	{
		{	/* SawBbv/bbv-types.sch 204 */
			return
				BGl_blockSzd2z52hashz80zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3884));
		}

	}



/* blockS-%hash-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2z52hashzd2setz12z40zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_191, obj_t BgL_vz00_192)
	{
		{	/* SawBbv/bbv-types.sch 205 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5240;

				{
					obj_t BgL_auxz00_5241;

					{	/* SawBbv/bbv-types.sch 205 */
						BgL_objectz00_bglt BgL_tmpz00_5242;

						BgL_tmpz00_5242 = ((BgL_objectz00_bglt) BgL_oz00_191);
						BgL_auxz00_5241 = BGL_OBJECT_WIDENING(BgL_tmpz00_5242);
					}
					BgL_auxz00_5240 = ((BgL_blocksz00_bglt) BgL_auxz00_5241);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5240))->BgL_z52hashz52) =
					((obj_t) BgL_vz00_192), BUNSPEC);
			}
		}

	}



/* &blockS-%hash-set! */
	obj_t BGl_z62blockSzd2z52hashzd2setz12z22zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3885, obj_t BgL_oz00_3886, obj_t BgL_vz00_3887)
	{
		{	/* SawBbv/bbv-types.sch 205 */
			return
				BGl_blockSzd2z52hashzd2setz12z40zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3886), BgL_vz00_3887);
		}

	}



/* blockS-%mark */
	BGL_EXPORTED_DEF long
		BGl_blockSzd2z52markz80zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_193)
	{
		{	/* SawBbv/bbv-types.sch 206 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5249;

				{
					obj_t BgL_auxz00_5250;

					{	/* SawBbv/bbv-types.sch 206 */
						BgL_objectz00_bglt BgL_tmpz00_5251;

						BgL_tmpz00_5251 = ((BgL_objectz00_bglt) BgL_oz00_193);
						BgL_auxz00_5250 = BGL_OBJECT_WIDENING(BgL_tmpz00_5251);
					}
					BgL_auxz00_5249 = ((BgL_blocksz00_bglt) BgL_auxz00_5250);
				}
				return
					(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5249))->BgL_z52markz52);
			}
		}

	}



/* &blockS-%mark */
	obj_t BGl_z62blockSzd2z52markze2zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3888,
		obj_t BgL_oz00_3889)
	{
		{	/* SawBbv/bbv-types.sch 206 */
			return
				BINT(BGl_blockSzd2z52markz80zzsaw_bbvzd2livenesszd2(
					((BgL_blockz00_bglt) BgL_oz00_3889)));
		}

	}



/* blockS-%mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2z52markzd2setz12z40zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_194, long BgL_vz00_195)
	{
		{	/* SawBbv/bbv-types.sch 207 */
			{
				BgL_blocksz00_bglt BgL_auxz00_5259;

				{
					obj_t BgL_auxz00_5260;

					{	/* SawBbv/bbv-types.sch 207 */
						BgL_objectz00_bglt BgL_tmpz00_5261;

						BgL_tmpz00_5261 = ((BgL_objectz00_bglt) BgL_oz00_194);
						BgL_auxz00_5260 = BGL_OBJECT_WIDENING(BgL_tmpz00_5261);
					}
					BgL_auxz00_5259 = ((BgL_blocksz00_bglt) BgL_auxz00_5260);
				}
				return
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_5259))->BgL_z52markz52) =
					((long) BgL_vz00_195), BUNSPEC);
		}}

	}



/* &blockS-%mark-set! */
	obj_t BGl_z62blockSzd2z52markzd2setz12z22zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3890, obj_t BgL_oz00_3891, obj_t BgL_vz00_3892)
	{
		{	/* SawBbv/bbv-types.sch 207 */
			return
				BGl_blockSzd2z52markzd2setz12z40zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3891), (long) CINT(BgL_vz00_3892));
		}

	}



/* blockS-first */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2firstzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt BgL_oz00_196)
	{
		{	/* SawBbv/bbv-types.sch 208 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_196)))->BgL_firstz00);
		}

	}



/* &blockS-first */
	obj_t BGl_z62blockSzd2firstzb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3893,
		obj_t BgL_oz00_3894)
	{
		{	/* SawBbv/bbv-types.sch 208 */
			return
				BGl_blockSzd2firstzd2zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3894));
		}

	}



/* blockS-first-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2firstzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_197, obj_t BgL_vz00_198)
	{
		{	/* SawBbv/bbv-types.sch 209 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_197)))->BgL_firstz00) =
				((obj_t) BgL_vz00_198), BUNSPEC);
		}

	}



/* &blockS-first-set! */
	obj_t BGl_z62blockSzd2firstzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3895, obj_t BgL_oz00_3896, obj_t BgL_vz00_3897)
	{
		{	/* SawBbv/bbv-types.sch 209 */
			return
				BGl_blockSzd2firstzd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3896), BgL_vz00_3897);
		}

	}



/* blockS-succs */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2succszd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt BgL_oz00_199)
	{
		{	/* SawBbv/bbv-types.sch 210 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_199)))->BgL_succsz00);
		}

	}



/* &blockS-succs */
	obj_t BGl_z62blockSzd2succszb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3898,
		obj_t BgL_oz00_3899)
	{
		{	/* SawBbv/bbv-types.sch 210 */
			return
				BGl_blockSzd2succszd2zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3899));
		}

	}



/* blockS-succs-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2succszd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_200, obj_t BgL_vz00_201)
	{
		{	/* SawBbv/bbv-types.sch 211 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_200)))->BgL_succsz00) =
				((obj_t) BgL_vz00_201), BUNSPEC);
		}

	}



/* &blockS-succs-set! */
	obj_t BGl_z62blockSzd2succszd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3900, obj_t BgL_oz00_3901, obj_t BgL_vz00_3902)
	{
		{	/* SawBbv/bbv-types.sch 211 */
			return
				BGl_blockSzd2succszd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3901), BgL_vz00_3902);
		}

	}



/* blockS-preds */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2predszd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt BgL_oz00_202)
	{
		{	/* SawBbv/bbv-types.sch 212 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_202)))->BgL_predsz00);
		}

	}



/* &blockS-preds */
	obj_t BGl_z62blockSzd2predszb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3903,
		obj_t BgL_oz00_3904)
	{
		{	/* SawBbv/bbv-types.sch 212 */
			return
				BGl_blockSzd2predszd2zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3904));
		}

	}



/* blockS-preds-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2predszd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_203, obj_t BgL_vz00_204)
	{
		{	/* SawBbv/bbv-types.sch 213 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_203)))->BgL_predsz00) =
				((obj_t) BgL_vz00_204), BUNSPEC);
		}

	}



/* &blockS-preds-set! */
	obj_t BGl_z62blockSzd2predszd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3905, obj_t BgL_oz00_3906, obj_t BgL_vz00_3907)
	{
		{	/* SawBbv/bbv-types.sch 213 */
			return
				BGl_blockSzd2predszd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3906), BgL_vz00_3907);
		}

	}



/* blockS-label */
	BGL_EXPORTED_DEF int
		BGl_blockSzd2labelzd2zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt BgL_oz00_205)
	{
		{	/* SawBbv/bbv-types.sch 214 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_205)))->BgL_labelz00);
		}

	}



/* &blockS-label */
	obj_t BGl_z62blockSzd2labelzb0zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3908,
		obj_t BgL_oz00_3909)
	{
		{	/* SawBbv/bbv-types.sch 214 */
			return
				BINT(BGl_blockSzd2labelzd2zzsaw_bbvzd2livenesszd2(
					((BgL_blockz00_bglt) BgL_oz00_3909)));
		}

	}



/* blockS-label-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockSzd2labelzd2setz12z12zzsaw_bbvzd2livenesszd2(BgL_blockz00_bglt
		BgL_oz00_206, int BgL_vz00_207)
	{
		{	/* SawBbv/bbv-types.sch 215 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_206)))->BgL_labelz00) =
				((int) BgL_vz00_207), BUNSPEC);
		}

	}



/* &blockS-label-set! */
	obj_t BGl_z62blockSzd2labelzd2setz12z70zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3910, obj_t BgL_oz00_3911, obj_t BgL_vz00_3912)
	{
		{	/* SawBbv/bbv-types.sch 215 */
			return
				BGl_blockSzd2labelzd2setz12z12zzsaw_bbvzd2livenesszd2(
				((BgL_blockz00_bglt) BgL_oz00_3911), CINT(BgL_vz00_3912));
		}

	}



/* make-bbv-queue */
	BGL_EXPORTED_DEF BgL_bbvzd2queuezd2_bglt
		BGl_makezd2bbvzd2queuez00zzsaw_bbvzd2livenesszd2(obj_t
		BgL_blocks1236z00_208, obj_t BgL_last1237z00_209)
	{
		{	/* SawBbv/bbv-types.sch 218 */
			{	/* SawBbv/bbv-types.sch 218 */
				BgL_bbvzd2queuezd2_bglt BgL_new1183z00_4096;

				{	/* SawBbv/bbv-types.sch 218 */
					BgL_bbvzd2queuezd2_bglt BgL_new1182z00_4097;

					BgL_new1182z00_4097 =
						((BgL_bbvzd2queuezd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2queuezd2_bgl))));
					{	/* SawBbv/bbv-types.sch 218 */
						long BgL_arg1552z00_4098;

						BgL_arg1552z00_4098 =
							BGL_CLASS_NUM(BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1182z00_4097), BgL_arg1552z00_4098);
					}
					BgL_new1183z00_4096 = BgL_new1182z00_4097;
				}
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_new1183z00_4096))->
						BgL_blocksz00) = ((obj_t) BgL_blocks1236z00_208), BUNSPEC);
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_new1183z00_4096))->
						BgL_lastz00) = ((obj_t) BgL_last1237z00_209), BUNSPEC);
				return BgL_new1183z00_4096;
			}
		}

	}



/* &make-bbv-queue */
	BgL_bbvzd2queuezd2_bglt
		BGl_z62makezd2bbvzd2queuez62zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3913,
		obj_t BgL_blocks1236z00_3914, obj_t BgL_last1237z00_3915)
	{
		{	/* SawBbv/bbv-types.sch 218 */
			return
				BGl_makezd2bbvzd2queuez00zzsaw_bbvzd2livenesszd2(BgL_blocks1236z00_3914,
				BgL_last1237z00_3915);
		}

	}



/* bbv-queue? */
	BGL_EXPORTED_DEF bool_t BGl_bbvzd2queuezf3z21zzsaw_bbvzd2livenesszd2(obj_t
		BgL_objz00_210)
	{
		{	/* SawBbv/bbv-types.sch 219 */
			{	/* SawBbv/bbv-types.sch 219 */
				obj_t BgL_classz00_4099;

				BgL_classz00_4099 = BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_210))
					{	/* SawBbv/bbv-types.sch 219 */
						BgL_objectz00_bglt BgL_arg1807z00_4100;

						BgL_arg1807z00_4100 = (BgL_objectz00_bglt) (BgL_objz00_210);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 219 */
								long BgL_idxz00_4101;

								BgL_idxz00_4101 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4100);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4101 + 1L)) == BgL_classz00_4099);
							}
						else
							{	/* SawBbv/bbv-types.sch 219 */
								bool_t BgL_res1981z00_4104;

								{	/* SawBbv/bbv-types.sch 219 */
									obj_t BgL_oclassz00_4105;

									{	/* SawBbv/bbv-types.sch 219 */
										obj_t BgL_arg1815z00_4106;
										long BgL_arg1816z00_4107;

										BgL_arg1815z00_4106 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 219 */
											long BgL_arg1817z00_4108;

											BgL_arg1817z00_4108 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4100);
											BgL_arg1816z00_4107 = (BgL_arg1817z00_4108 - OBJECT_TYPE);
										}
										BgL_oclassz00_4105 =
											VECTOR_REF(BgL_arg1815z00_4106, BgL_arg1816z00_4107);
									}
									{	/* SawBbv/bbv-types.sch 219 */
										bool_t BgL__ortest_1115z00_4109;

										BgL__ortest_1115z00_4109 =
											(BgL_classz00_4099 == BgL_oclassz00_4105);
										if (BgL__ortest_1115z00_4109)
											{	/* SawBbv/bbv-types.sch 219 */
												BgL_res1981z00_4104 = BgL__ortest_1115z00_4109;
											}
										else
											{	/* SawBbv/bbv-types.sch 219 */
												long BgL_odepthz00_4110;

												{	/* SawBbv/bbv-types.sch 219 */
													obj_t BgL_arg1804z00_4111;

													BgL_arg1804z00_4111 = (BgL_oclassz00_4105);
													BgL_odepthz00_4110 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4111);
												}
												if ((1L < BgL_odepthz00_4110))
													{	/* SawBbv/bbv-types.sch 219 */
														obj_t BgL_arg1802z00_4112;

														{	/* SawBbv/bbv-types.sch 219 */
															obj_t BgL_arg1803z00_4113;

															BgL_arg1803z00_4113 = (BgL_oclassz00_4105);
															BgL_arg1802z00_4112 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4113,
																1L);
														}
														BgL_res1981z00_4104 =
															(BgL_arg1802z00_4112 == BgL_classz00_4099);
													}
												else
													{	/* SawBbv/bbv-types.sch 219 */
														BgL_res1981z00_4104 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1981z00_4104;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 219 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bbv-queue? */
	obj_t BGl_z62bbvzd2queuezf3z43zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3916,
		obj_t BgL_objz00_3917)
	{
		{	/* SawBbv/bbv-types.sch 219 */
			return
				BBOOL(BGl_bbvzd2queuezf3z21zzsaw_bbvzd2livenesszd2(BgL_objz00_3917));
		}

	}



/* bbv-queue-nil */
	BGL_EXPORTED_DEF BgL_bbvzd2queuezd2_bglt
		BGl_bbvzd2queuezd2nilz00zzsaw_bbvzd2livenesszd2(void)
	{
		{	/* SawBbv/bbv-types.sch 220 */
			{	/* SawBbv/bbv-types.sch 220 */
				obj_t BgL_classz00_3137;

				BgL_classz00_3137 = BGl_bbvzd2queuezd2zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 220 */
					obj_t BgL__ortest_1117z00_3138;

					BgL__ortest_1117z00_3138 = BGL_CLASS_NIL(BgL_classz00_3137);
					if (CBOOL(BgL__ortest_1117z00_3138))
						{	/* SawBbv/bbv-types.sch 220 */
							return ((BgL_bbvzd2queuezd2_bglt) BgL__ortest_1117z00_3138);
						}
					else
						{	/* SawBbv/bbv-types.sch 220 */
							return
								((BgL_bbvzd2queuezd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3137));
						}
				}
			}
		}

	}



/* &bbv-queue-nil */
	BgL_bbvzd2queuezd2_bglt
		BGl_z62bbvzd2queuezd2nilz62zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3918)
	{
		{	/* SawBbv/bbv-types.sch 220 */
			return BGl_bbvzd2queuezd2nilz00zzsaw_bbvzd2livenesszd2();
		}

	}



/* bbv-queue-last */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2queuezd2lastz00zzsaw_bbvzd2livenesszd2(BgL_bbvzd2queuezd2_bglt
		BgL_oz00_211)
	{
		{	/* SawBbv/bbv-types.sch 221 */
			return (((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_oz00_211))->BgL_lastz00);
		}

	}



/* &bbv-queue-last */
	obj_t BGl_z62bbvzd2queuezd2lastz62zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3919, obj_t BgL_oz00_3920)
	{
		{	/* SawBbv/bbv-types.sch 221 */
			return
				BGl_bbvzd2queuezd2lastz00zzsaw_bbvzd2livenesszd2(
				((BgL_bbvzd2queuezd2_bglt) BgL_oz00_3920));
		}

	}



/* bbv-queue-last-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2queuezd2lastzd2setz12zc0zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2queuezd2_bglt BgL_oz00_212, obj_t BgL_vz00_213)
	{
		{	/* SawBbv/bbv-types.sch 222 */
			return
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_oz00_212))->BgL_lastz00) =
				((obj_t) BgL_vz00_213), BUNSPEC);
		}

	}



/* &bbv-queue-last-set! */
	obj_t BGl_z62bbvzd2queuezd2lastzd2setz12za2zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3921, obj_t BgL_oz00_3922, obj_t BgL_vz00_3923)
	{
		{	/* SawBbv/bbv-types.sch 222 */
			return
				BGl_bbvzd2queuezd2lastzd2setz12zc0zzsaw_bbvzd2livenesszd2(
				((BgL_bbvzd2queuezd2_bglt) BgL_oz00_3922), BgL_vz00_3923);
		}

	}



/* bbv-queue-blocks */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2queuezd2blocksz00zzsaw_bbvzd2livenesszd2(BgL_bbvzd2queuezd2_bglt
		BgL_oz00_214)
	{
		{	/* SawBbv/bbv-types.sch 223 */
			return (((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_oz00_214))->BgL_blocksz00);
		}

	}



/* &bbv-queue-blocks */
	obj_t BGl_z62bbvzd2queuezd2blocksz62zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3924, obj_t BgL_oz00_3925)
	{
		{	/* SawBbv/bbv-types.sch 223 */
			return
				BGl_bbvzd2queuezd2blocksz00zzsaw_bbvzd2livenesszd2(
				((BgL_bbvzd2queuezd2_bglt) BgL_oz00_3925));
		}

	}



/* bbv-queue-blocks-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2queuezd2blockszd2setz12zc0zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2queuezd2_bglt BgL_oz00_215, obj_t BgL_vz00_216)
	{
		{	/* SawBbv/bbv-types.sch 224 */
			return
				((((BgL_bbvzd2queuezd2_bglt) COBJECT(BgL_oz00_215))->BgL_blocksz00) =
				((obj_t) BgL_vz00_216), BUNSPEC);
		}

	}



/* &bbv-queue-blocks-set! */
	obj_t BGl_z62bbvzd2queuezd2blockszd2setz12za2zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3926, obj_t BgL_oz00_3927, obj_t BgL_vz00_3928)
	{
		{	/* SawBbv/bbv-types.sch 224 */
			return
				BGl_bbvzd2queuezd2blockszd2setz12zc0zzsaw_bbvzd2livenesszd2(
				((BgL_bbvzd2queuezd2_bglt) BgL_oz00_3927), BgL_vz00_3928);
		}

	}



/* make-bbv-ctx */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_makezd2bbvzd2ctxz00zzsaw_bbvzd2livenesszd2(long BgL_id1233z00_217,
		obj_t BgL_entries1234z00_218)
	{
		{	/* SawBbv/bbv-types.sch 227 */
			{	/* SawBbv/bbv-types.sch 227 */
				BgL_bbvzd2ctxzd2_bglt BgL_new1185z00_4114;

				{	/* SawBbv/bbv-types.sch 227 */
					BgL_bbvzd2ctxzd2_bglt BgL_new1184z00_4115;

					BgL_new1184z00_4115 =
						((BgL_bbvzd2ctxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2ctxzd2_bgl))));
					{	/* SawBbv/bbv-types.sch 227 */
						long BgL_arg1559z00_4116;

						BgL_arg1559z00_4116 =
							BGL_CLASS_NUM(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1184z00_4115), BgL_arg1559z00_4116);
					}
					BgL_new1185z00_4114 = BgL_new1184z00_4115;
				}
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1185z00_4114))->BgL_idz00) =
					((long) BgL_id1233z00_217), BUNSPEC);
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1185z00_4114))->
						BgL_entriesz00) = ((obj_t) BgL_entries1234z00_218), BUNSPEC);
				{	/* SawBbv/bbv-types.sch 227 */
					obj_t BgL_fun1553z00_4117;

					BgL_fun1553z00_4117 =
						BGl_classzd2constructorzd2zz__objectz00
						(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
					BGL_PROCEDURE_CALL1(BgL_fun1553z00_4117,
						((obj_t) BgL_new1185z00_4114));
				}
				return BgL_new1185z00_4114;
			}
		}

	}



/* &make-bbv-ctx */
	BgL_bbvzd2ctxzd2_bglt BGl_z62makezd2bbvzd2ctxz62zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3929, obj_t BgL_id1233z00_3930, obj_t BgL_entries1234z00_3931)
	{
		{	/* SawBbv/bbv-types.sch 227 */
			return
				BGl_makezd2bbvzd2ctxz00zzsaw_bbvzd2livenesszd2(
				(long) CINT(BgL_id1233z00_3930), BgL_entries1234z00_3931);
		}

	}



/* bbv-ctx? */
	BGL_EXPORTED_DEF bool_t BGl_bbvzd2ctxzf3z21zzsaw_bbvzd2livenesszd2(obj_t
		BgL_objz00_219)
	{
		{	/* SawBbv/bbv-types.sch 228 */
			{	/* SawBbv/bbv-types.sch 228 */
				obj_t BgL_classz00_4118;

				BgL_classz00_4118 = BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_219))
					{	/* SawBbv/bbv-types.sch 228 */
						BgL_objectz00_bglt BgL_arg1807z00_4119;

						BgL_arg1807z00_4119 = (BgL_objectz00_bglt) (BgL_objz00_219);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 228 */
								long BgL_idxz00_4120;

								BgL_idxz00_4120 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4119);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4120 + 1L)) == BgL_classz00_4118);
							}
						else
							{	/* SawBbv/bbv-types.sch 228 */
								bool_t BgL_res1982z00_4123;

								{	/* SawBbv/bbv-types.sch 228 */
									obj_t BgL_oclassz00_4124;

									{	/* SawBbv/bbv-types.sch 228 */
										obj_t BgL_arg1815z00_4125;
										long BgL_arg1816z00_4126;

										BgL_arg1815z00_4125 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 228 */
											long BgL_arg1817z00_4127;

											BgL_arg1817z00_4127 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4119);
											BgL_arg1816z00_4126 = (BgL_arg1817z00_4127 - OBJECT_TYPE);
										}
										BgL_oclassz00_4124 =
											VECTOR_REF(BgL_arg1815z00_4125, BgL_arg1816z00_4126);
									}
									{	/* SawBbv/bbv-types.sch 228 */
										bool_t BgL__ortest_1115z00_4128;

										BgL__ortest_1115z00_4128 =
											(BgL_classz00_4118 == BgL_oclassz00_4124);
										if (BgL__ortest_1115z00_4128)
											{	/* SawBbv/bbv-types.sch 228 */
												BgL_res1982z00_4123 = BgL__ortest_1115z00_4128;
											}
										else
											{	/* SawBbv/bbv-types.sch 228 */
												long BgL_odepthz00_4129;

												{	/* SawBbv/bbv-types.sch 228 */
													obj_t BgL_arg1804z00_4130;

													BgL_arg1804z00_4130 = (BgL_oclassz00_4124);
													BgL_odepthz00_4129 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4130);
												}
												if ((1L < BgL_odepthz00_4129))
													{	/* SawBbv/bbv-types.sch 228 */
														obj_t BgL_arg1802z00_4131;

														{	/* SawBbv/bbv-types.sch 228 */
															obj_t BgL_arg1803z00_4132;

															BgL_arg1803z00_4132 = (BgL_oclassz00_4124);
															BgL_arg1802z00_4131 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4132,
																1L);
														}
														BgL_res1982z00_4123 =
															(BgL_arg1802z00_4131 == BgL_classz00_4118);
													}
												else
													{	/* SawBbv/bbv-types.sch 228 */
														BgL_res1982z00_4123 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1982z00_4123;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 228 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bbv-ctx? */
	obj_t BGl_z62bbvzd2ctxzf3z43zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3932,
		obj_t BgL_objz00_3933)
	{
		{	/* SawBbv/bbv-types.sch 228 */
			return BBOOL(BGl_bbvzd2ctxzf3z21zzsaw_bbvzd2livenesszd2(BgL_objz00_3933));
		}

	}



/* bbv-ctx-nil */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_bbvzd2ctxzd2nilz00zzsaw_bbvzd2livenesszd2(void)
	{
		{	/* SawBbv/bbv-types.sch 229 */
			{	/* SawBbv/bbv-types.sch 229 */
				obj_t BgL_classz00_3180;

				BgL_classz00_3180 = BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 229 */
					obj_t BgL__ortest_1117z00_3181;

					BgL__ortest_1117z00_3181 = BGL_CLASS_NIL(BgL_classz00_3180);
					if (CBOOL(BgL__ortest_1117z00_3181))
						{	/* SawBbv/bbv-types.sch 229 */
							return ((BgL_bbvzd2ctxzd2_bglt) BgL__ortest_1117z00_3181);
						}
					else
						{	/* SawBbv/bbv-types.sch 229 */
							return
								((BgL_bbvzd2ctxzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3180));
						}
				}
			}
		}

	}



/* &bbv-ctx-nil */
	BgL_bbvzd2ctxzd2_bglt BGl_z62bbvzd2ctxzd2nilz62zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3934)
	{
		{	/* SawBbv/bbv-types.sch 229 */
			return BGl_bbvzd2ctxzd2nilz00zzsaw_bbvzd2livenesszd2();
		}

	}



/* bbv-ctx-entries */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxzd2entriesz00zzsaw_bbvzd2livenesszd2(BgL_bbvzd2ctxzd2_bglt
		BgL_oz00_220)
	{
		{	/* SawBbv/bbv-types.sch 230 */
			return (((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_oz00_220))->BgL_entriesz00);
		}

	}



/* &bbv-ctx-entries */
	obj_t BGl_z62bbvzd2ctxzd2entriesz62zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3935, obj_t BgL_oz00_3936)
	{
		{	/* SawBbv/bbv-types.sch 230 */
			return
				BGl_bbvzd2ctxzd2entriesz00zzsaw_bbvzd2livenesszd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_3936));
		}

	}



/* bbv-ctx-entries-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxzd2entrieszd2setz12zc0zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxzd2_bglt BgL_oz00_221, obj_t BgL_vz00_222)
	{
		{	/* SawBbv/bbv-types.sch 231 */
			return
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_oz00_221))->BgL_entriesz00) =
				((obj_t) BgL_vz00_222), BUNSPEC);
		}

	}



/* &bbv-ctx-entries-set! */
	obj_t BGl_z62bbvzd2ctxzd2entrieszd2setz12za2zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3937, obj_t BgL_oz00_3938, obj_t BgL_vz00_3939)
	{
		{	/* SawBbv/bbv-types.sch 231 */
			return
				BGl_bbvzd2ctxzd2entrieszd2setz12zc0zzsaw_bbvzd2livenesszd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_3938), BgL_vz00_3939);
		}

	}



/* bbv-ctx-id */
	BGL_EXPORTED_DEF long
		BGl_bbvzd2ctxzd2idz00zzsaw_bbvzd2livenesszd2(BgL_bbvzd2ctxzd2_bglt
		BgL_oz00_223)
	{
		{	/* SawBbv/bbv-types.sch 232 */
			return (((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_oz00_223))->BgL_idz00);
		}

	}



/* &bbv-ctx-id */
	obj_t BGl_z62bbvzd2ctxzd2idz62zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3940,
		obj_t BgL_oz00_3941)
	{
		{	/* SawBbv/bbv-types.sch 232 */
			return
				BINT(BGl_bbvzd2ctxzd2idz00zzsaw_bbvzd2livenesszd2(
					((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_3941)));
		}

	}



/* bbv-ctx-id-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxzd2idzd2setz12zc0zzsaw_bbvzd2livenesszd2(BgL_bbvzd2ctxzd2_bglt
		BgL_oz00_224, long BgL_vz00_225)
	{
		{	/* SawBbv/bbv-types.sch 233 */
			return
				((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_oz00_224))->BgL_idz00) =
				((long) BgL_vz00_225), BUNSPEC);
		}

	}



/* &bbv-ctx-id-set! */
	obj_t BGl_z62bbvzd2ctxzd2idzd2setz12za2zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3942, obj_t BgL_oz00_3943, obj_t BgL_vz00_3944)
	{
		{	/* SawBbv/bbv-types.sch 233 */
			return
				BGl_bbvzd2ctxzd2idzd2setz12zc0zzsaw_bbvzd2livenesszd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_oz00_3943), (long) CINT(BgL_vz00_3944));
		}

	}



/* make-bbv-ctxentry */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxentryzd2_bglt
		BGl_makezd2bbvzd2ctxentryz00zzsaw_bbvzd2livenesszd2(BgL_rtl_regz00_bglt
		BgL_reg1225z00_226, obj_t BgL_types1226z00_227,
		bool_t BgL_polarity1227z00_228, long BgL_count1228z00_229,
		obj_t BgL_value1229z00_230, obj_t BgL_aliases1230z00_231,
		obj_t BgL_initval1231z00_232)
	{
		{	/* SawBbv/bbv-types.sch 236 */
			{	/* SawBbv/bbv-types.sch 236 */
				BgL_bbvzd2ctxentryzd2_bglt BgL_new1187z00_4133;

				{	/* SawBbv/bbv-types.sch 236 */
					BgL_bbvzd2ctxentryzd2_bglt BgL_new1186z00_4134;

					BgL_new1186z00_4134 =
						((BgL_bbvzd2ctxentryzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_bbvzd2ctxentryzd2_bgl))));
					{	/* SawBbv/bbv-types.sch 236 */
						long BgL_arg1561z00_4135;

						BgL_arg1561z00_4135 =
							BGL_CLASS_NUM(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1186z00_4134), BgL_arg1561z00_4135);
					}
					BgL_new1187z00_4133 = BgL_new1186z00_4134;
				}
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1187z00_4133))->
						BgL_regz00) = ((BgL_rtl_regz00_bglt) BgL_reg1225z00_226), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1187z00_4133))->
						BgL_typesz00) = ((obj_t) BgL_types1226z00_227), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1187z00_4133))->
						BgL_polarityz00) = ((bool_t) BgL_polarity1227z00_228), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1187z00_4133))->
						BgL_countz00) = ((long) BgL_count1228z00_229), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1187z00_4133))->
						BgL_valuez00) = ((obj_t) BgL_value1229z00_230), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1187z00_4133))->
						BgL_aliasesz00) = ((obj_t) BgL_aliases1230z00_231), BUNSPEC);
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_new1187z00_4133))->
						BgL_initvalz00) = ((obj_t) BgL_initval1231z00_232), BUNSPEC);
				return BgL_new1187z00_4133;
			}
		}

	}



/* &make-bbv-ctxentry */
	BgL_bbvzd2ctxentryzd2_bglt
		BGl_z62makezd2bbvzd2ctxentryz62zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3945, obj_t BgL_reg1225z00_3946, obj_t BgL_types1226z00_3947,
		obj_t BgL_polarity1227z00_3948, obj_t BgL_count1228z00_3949,
		obj_t BgL_value1229z00_3950, obj_t BgL_aliases1230z00_3951,
		obj_t BgL_initval1231z00_3952)
	{
		{	/* SawBbv/bbv-types.sch 236 */
			return
				BGl_makezd2bbvzd2ctxentryz00zzsaw_bbvzd2livenesszd2(
				((BgL_rtl_regz00_bglt) BgL_reg1225z00_3946), BgL_types1226z00_3947,
				CBOOL(BgL_polarity1227z00_3948),
				(long) CINT(BgL_count1228z00_3949), BgL_value1229z00_3950,
				BgL_aliases1230z00_3951, BgL_initval1231z00_3952);
		}

	}



/* bbv-ctxentry? */
	BGL_EXPORTED_DEF bool_t BGl_bbvzd2ctxentryzf3z21zzsaw_bbvzd2livenesszd2(obj_t
		BgL_objz00_233)
	{
		{	/* SawBbv/bbv-types.sch 237 */
			{	/* SawBbv/bbv-types.sch 237 */
				obj_t BgL_classz00_4136;

				BgL_classz00_4136 = BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2;
				if (BGL_OBJECTP(BgL_objz00_233))
					{	/* SawBbv/bbv-types.sch 237 */
						BgL_objectz00_bglt BgL_arg1807z00_4137;

						BgL_arg1807z00_4137 = (BgL_objectz00_bglt) (BgL_objz00_233);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawBbv/bbv-types.sch 237 */
								long BgL_idxz00_4138;

								BgL_idxz00_4138 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4137);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4138 + 1L)) == BgL_classz00_4136);
							}
						else
							{	/* SawBbv/bbv-types.sch 237 */
								bool_t BgL_res1983z00_4141;

								{	/* SawBbv/bbv-types.sch 237 */
									obj_t BgL_oclassz00_4142;

									{	/* SawBbv/bbv-types.sch 237 */
										obj_t BgL_arg1815z00_4143;
										long BgL_arg1816z00_4144;

										BgL_arg1815z00_4143 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawBbv/bbv-types.sch 237 */
											long BgL_arg1817z00_4145;

											BgL_arg1817z00_4145 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4137);
											BgL_arg1816z00_4144 = (BgL_arg1817z00_4145 - OBJECT_TYPE);
										}
										BgL_oclassz00_4142 =
											VECTOR_REF(BgL_arg1815z00_4143, BgL_arg1816z00_4144);
									}
									{	/* SawBbv/bbv-types.sch 237 */
										bool_t BgL__ortest_1115z00_4146;

										BgL__ortest_1115z00_4146 =
											(BgL_classz00_4136 == BgL_oclassz00_4142);
										if (BgL__ortest_1115z00_4146)
											{	/* SawBbv/bbv-types.sch 237 */
												BgL_res1983z00_4141 = BgL__ortest_1115z00_4146;
											}
										else
											{	/* SawBbv/bbv-types.sch 237 */
												long BgL_odepthz00_4147;

												{	/* SawBbv/bbv-types.sch 237 */
													obj_t BgL_arg1804z00_4148;

													BgL_arg1804z00_4148 = (BgL_oclassz00_4142);
													BgL_odepthz00_4147 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4148);
												}
												if ((1L < BgL_odepthz00_4147))
													{	/* SawBbv/bbv-types.sch 237 */
														obj_t BgL_arg1802z00_4149;

														{	/* SawBbv/bbv-types.sch 237 */
															obj_t BgL_arg1803z00_4150;

															BgL_arg1803z00_4150 = (BgL_oclassz00_4142);
															BgL_arg1802z00_4149 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4150,
																1L);
														}
														BgL_res1983z00_4141 =
															(BgL_arg1802z00_4149 == BgL_classz00_4136);
													}
												else
													{	/* SawBbv/bbv-types.sch 237 */
														BgL_res1983z00_4141 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1983z00_4141;
							}
					}
				else
					{	/* SawBbv/bbv-types.sch 237 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &bbv-ctxentry? */
	obj_t BGl_z62bbvzd2ctxentryzf3z43zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3953, obj_t BgL_objz00_3954)
	{
		{	/* SawBbv/bbv-types.sch 237 */
			return
				BBOOL(BGl_bbvzd2ctxentryzf3z21zzsaw_bbvzd2livenesszd2(BgL_objz00_3954));
		}

	}



/* bbv-ctxentry-nil */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxentryzd2_bglt
		BGl_bbvzd2ctxentryzd2nilz00zzsaw_bbvzd2livenesszd2(void)
	{
		{	/* SawBbv/bbv-types.sch 238 */
			{	/* SawBbv/bbv-types.sch 238 */
				obj_t BgL_classz00_3222;

				BgL_classz00_3222 = BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2;
				{	/* SawBbv/bbv-types.sch 238 */
					obj_t BgL__ortest_1117z00_3223;

					BgL__ortest_1117z00_3223 = BGL_CLASS_NIL(BgL_classz00_3222);
					if (CBOOL(BgL__ortest_1117z00_3223))
						{	/* SawBbv/bbv-types.sch 238 */
							return ((BgL_bbvzd2ctxentryzd2_bglt) BgL__ortest_1117z00_3223);
						}
					else
						{	/* SawBbv/bbv-types.sch 238 */
							return
								((BgL_bbvzd2ctxentryzd2_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3222));
						}
				}
			}
		}

	}



/* &bbv-ctxentry-nil */
	BgL_bbvzd2ctxentryzd2_bglt
		BGl_z62bbvzd2ctxentryzd2nilz62zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3955)
	{
		{	/* SawBbv/bbv-types.sch 238 */
			return BGl_bbvzd2ctxentryzd2nilz00zzsaw_bbvzd2livenesszd2();
		}

	}



/* bbv-ctxentry-initval */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2initvalz00zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_234)
	{
		{	/* SawBbv/bbv-types.sch 239 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_234))->BgL_initvalz00);
		}

	}



/* &bbv-ctxentry-initval */
	obj_t BGl_z62bbvzd2ctxentryzd2initvalz62zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3956, obj_t BgL_oz00_3957)
	{
		{	/* SawBbv/bbv-types.sch 239 */
			return
				BGl_bbvzd2ctxentryzd2initvalz00zzsaw_bbvzd2livenesszd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3957));
		}

	}



/* bbv-ctxentry-initval-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2initvalzd2setz12zc0zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_235, obj_t BgL_vz00_236)
	{
		{	/* SawBbv/bbv-types.sch 240 */
			return
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_235))->
					BgL_initvalz00) = ((obj_t) BgL_vz00_236), BUNSPEC);
		}

	}



/* &bbv-ctxentry-initval-set! */
	obj_t BGl_z62bbvzd2ctxentryzd2initvalzd2setz12za2zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3958, obj_t BgL_oz00_3959, obj_t BgL_vz00_3960)
	{
		{	/* SawBbv/bbv-types.sch 240 */
			return
				BGl_bbvzd2ctxentryzd2initvalzd2setz12zc0zzsaw_bbvzd2livenesszd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3959), BgL_vz00_3960);
		}

	}



/* bbv-ctxentry-aliases */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2aliasesz00zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_237)
	{
		{	/* SawBbv/bbv-types.sch 241 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_237))->BgL_aliasesz00);
		}

	}



/* &bbv-ctxentry-aliases */
	obj_t BGl_z62bbvzd2ctxentryzd2aliasesz62zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3961, obj_t BgL_oz00_3962)
	{
		{	/* SawBbv/bbv-types.sch 241 */
			return
				BGl_bbvzd2ctxentryzd2aliasesz00zzsaw_bbvzd2livenesszd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3962));
		}

	}



/* bbv-ctxentry-aliases-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2aliaseszd2setz12zc0zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_238, obj_t BgL_vz00_239)
	{
		{	/* SawBbv/bbv-types.sch 242 */
			return
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_238))->
					BgL_aliasesz00) = ((obj_t) BgL_vz00_239), BUNSPEC);
		}

	}



/* &bbv-ctxentry-aliases-set! */
	obj_t BGl_z62bbvzd2ctxentryzd2aliaseszd2setz12za2zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3963, obj_t BgL_oz00_3964, obj_t BgL_vz00_3965)
	{
		{	/* SawBbv/bbv-types.sch 242 */
			return
				BGl_bbvzd2ctxentryzd2aliaseszd2setz12zc0zzsaw_bbvzd2livenesszd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3964), BgL_vz00_3965);
		}

	}



/* bbv-ctxentry-value */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2valuez00zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_240)
	{
		{	/* SawBbv/bbv-types.sch 243 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_240))->BgL_valuez00);
		}

	}



/* &bbv-ctxentry-value */
	obj_t BGl_z62bbvzd2ctxentryzd2valuez62zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3966, obj_t BgL_oz00_3967)
	{
		{	/* SawBbv/bbv-types.sch 243 */
			return
				BGl_bbvzd2ctxentryzd2valuez00zzsaw_bbvzd2livenesszd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3967));
		}

	}



/* bbv-ctxentry-count */
	BGL_EXPORTED_DEF long
		BGl_bbvzd2ctxentryzd2countz00zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_243)
	{
		{	/* SawBbv/bbv-types.sch 245 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_243))->BgL_countz00);
		}

	}



/* &bbv-ctxentry-count */
	obj_t BGl_z62bbvzd2ctxentryzd2countz62zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3968, obj_t BgL_oz00_3969)
	{
		{	/* SawBbv/bbv-types.sch 245 */
			return
				BINT(BGl_bbvzd2ctxentryzd2countz00zzsaw_bbvzd2livenesszd2(
					((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3969)));
		}

	}



/* bbv-ctxentry-count-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2countzd2setz12zc0zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_244, long BgL_vz00_245)
	{
		{	/* SawBbv/bbv-types.sch 246 */
			return
				((((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_244))->BgL_countz00) =
				((long) BgL_vz00_245), BUNSPEC);
		}

	}



/* &bbv-ctxentry-count-set! */
	obj_t BGl_z62bbvzd2ctxentryzd2countzd2setz12za2zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3970, obj_t BgL_oz00_3971, obj_t BgL_vz00_3972)
	{
		{	/* SawBbv/bbv-types.sch 246 */
			return
				BGl_bbvzd2ctxentryzd2countzd2setz12zc0zzsaw_bbvzd2livenesszd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3971),
				(long) CINT(BgL_vz00_3972));
		}

	}



/* bbv-ctxentry-polarity */
	BGL_EXPORTED_DEF bool_t
		BGl_bbvzd2ctxentryzd2polarityz00zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_246)
	{
		{	/* SawBbv/bbv-types.sch 247 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_246))->BgL_polarityz00);
		}

	}



/* &bbv-ctxentry-polarity */
	obj_t BGl_z62bbvzd2ctxentryzd2polarityz62zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3973, obj_t BgL_oz00_3974)
	{
		{	/* SawBbv/bbv-types.sch 247 */
			return
				BBOOL(BGl_bbvzd2ctxentryzd2polarityz00zzsaw_bbvzd2livenesszd2(
					((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3974)));
		}

	}



/* bbv-ctxentry-types */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2ctxentryzd2typesz00zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_249)
	{
		{	/* SawBbv/bbv-types.sch 249 */
			return
				(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_249))->BgL_typesz00);
		}

	}



/* &bbv-ctxentry-types */
	obj_t BGl_z62bbvzd2ctxentryzd2typesz62zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3975, obj_t BgL_oz00_3976)
	{
		{	/* SawBbv/bbv-types.sch 249 */
			return
				BGl_bbvzd2ctxentryzd2typesz00zzsaw_bbvzd2livenesszd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3976));
		}

	}



/* bbv-ctxentry-reg */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_bbvzd2ctxentryzd2regz00zzsaw_bbvzd2livenesszd2
		(BgL_bbvzd2ctxentryzd2_bglt BgL_oz00_252)
	{
		{	/* SawBbv/bbv-types.sch 251 */
			return (((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(BgL_oz00_252))->BgL_regz00);
		}

	}



/* &bbv-ctxentry-reg */
	BgL_rtl_regz00_bglt
		BGl_z62bbvzd2ctxentryzd2regz62zzsaw_bbvzd2livenesszd2(obj_t BgL_envz00_3977,
		obj_t BgL_oz00_3978)
	{
		{	/* SawBbv/bbv-types.sch 251 */
			return
				BGl_bbvzd2ctxentryzd2regz00zzsaw_bbvzd2livenesszd2(
				((BgL_bbvzd2ctxentryzd2_bglt) BgL_oz00_3978));
		}

	}



/* bbv-liveness! */
	BGL_EXPORTED_DEF obj_t
		BGl_bbvzd2livenessz12zc0zzsaw_bbvzd2livenesszd2(BgL_backendz00_bglt
		BgL_backz00_255, obj_t BgL_blocksz00_256, obj_t BgL_paramsz00_257)
	{
		{	/* SawBbv/bbv-liveness.scm 54 */
			{
				obj_t BgL_blockz00_2307;

				{	/* SawBbv/bbv-liveness.scm 91 */
					obj_t BgL_cregsz00_2194;

					BgL_cregsz00_2194 =
						BGl_collectzd2registersz12zc0zzsaw_regutilsz00(BgL_blocksz00_256);
					{	/* SawBbv/bbv-liveness.scm 91 */
						obj_t BgL_hregsz00_2195;

						{	/* SawBbv/bbv-liveness.scm 92 */
							obj_t BgL_arg1611z00_2241;

							BgL_arg1611z00_2241 =
								(((BgL_backendz00_bglt) COBJECT(BgL_backz00_255))->
								BgL_registersz00);
							{	/* SawBbv/bbv-liveness.scm 92 */
								obj_t BgL_list1612z00_2242;

								BgL_list1612z00_2242 =
									MAKE_YOUNG_PAIR(BgL_arg1611z00_2241, BNIL);
								BgL_hregsz00_2195 =
									BGl_appendzd2mapz12zc0zz__r4_control_features_6_9z00
									(BGl_collectzd2registerz12zd2envz12zzsaw_regutilsz00,
									BgL_list1612z00_2242);
							}
						}
						{	/* SawBbv/bbv-liveness.scm 92 */
							obj_t BgL_pregsz00_2196;

							{	/* SawBbv/bbv-liveness.scm 93 */
								obj_t BgL_hook1454z00_2227;

								BgL_hook1454z00_2227 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
								{
									obj_t BgL_l1451z00_2229;
									obj_t BgL_h1452z00_2230;

									BgL_l1451z00_2229 = BgL_paramsz00_257;
									BgL_h1452z00_2230 = BgL_hook1454z00_2227;
								BgL_zc3z04anonymousza31592ze3z87_2231:
									if (NULLP(BgL_l1451z00_2229))
										{	/* SawBbv/bbv-liveness.scm 93 */
											BgL_pregsz00_2196 = CDR(BgL_hook1454z00_2227);
										}
									else
										{	/* SawBbv/bbv-liveness.scm 93 */
											bool_t BgL_test2172z00_5499;

											{	/* SawBbv/bbv-liveness.scm 93 */
												obj_t BgL_arg1609z00_2239;

												BgL_arg1609z00_2239 = CAR(((obj_t) BgL_l1451z00_2229));
												{	/* SawBbv/bbv-liveness.scm 93 */
													obj_t BgL_classz00_3371;

													BgL_classz00_3371 =
														BGl_rtl_regzf2razf2zzsaw_regsetz00;
													if (BGL_OBJECTP(BgL_arg1609z00_2239))
														{	/* SawBbv/bbv-liveness.scm 93 */
															BgL_objectz00_bglt BgL_arg1807z00_3373;

															BgL_arg1807z00_3373 =
																(BgL_objectz00_bglt) (BgL_arg1609z00_2239);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* SawBbv/bbv-liveness.scm 93 */
																	long BgL_idxz00_3379;

																	BgL_idxz00_3379 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3373);
																	BgL_test2172z00_5499 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3379 + 2L)) ==
																		BgL_classz00_3371);
																}
															else
																{	/* SawBbv/bbv-liveness.scm 93 */
																	bool_t BgL_res1986z00_3404;

																	{	/* SawBbv/bbv-liveness.scm 93 */
																		obj_t BgL_oclassz00_3387;

																		{	/* SawBbv/bbv-liveness.scm 93 */
																			obj_t BgL_arg1815z00_3395;
																			long BgL_arg1816z00_3396;

																			BgL_arg1815z00_3395 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* SawBbv/bbv-liveness.scm 93 */
																				long BgL_arg1817z00_3397;

																				BgL_arg1817z00_3397 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3373);
																				BgL_arg1816z00_3396 =
																					(BgL_arg1817z00_3397 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3387 =
																				VECTOR_REF(BgL_arg1815z00_3395,
																				BgL_arg1816z00_3396);
																		}
																		{	/* SawBbv/bbv-liveness.scm 93 */
																			bool_t BgL__ortest_1115z00_3388;

																			BgL__ortest_1115z00_3388 =
																				(BgL_classz00_3371 ==
																				BgL_oclassz00_3387);
																			if (BgL__ortest_1115z00_3388)
																				{	/* SawBbv/bbv-liveness.scm 93 */
																					BgL_res1986z00_3404 =
																						BgL__ortest_1115z00_3388;
																				}
																			else
																				{	/* SawBbv/bbv-liveness.scm 93 */
																					long BgL_odepthz00_3389;

																					{	/* SawBbv/bbv-liveness.scm 93 */
																						obj_t BgL_arg1804z00_3390;

																						BgL_arg1804z00_3390 =
																							(BgL_oclassz00_3387);
																						BgL_odepthz00_3389 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3390);
																					}
																					if ((2L < BgL_odepthz00_3389))
																						{	/* SawBbv/bbv-liveness.scm 93 */
																							obj_t BgL_arg1802z00_3392;

																							{	/* SawBbv/bbv-liveness.scm 93 */
																								obj_t BgL_arg1803z00_3393;

																								BgL_arg1803z00_3393 =
																									(BgL_oclassz00_3387);
																								BgL_arg1802z00_3392 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3393, 2L);
																							}
																							BgL_res1986z00_3404 =
																								(BgL_arg1802z00_3392 ==
																								BgL_classz00_3371);
																						}
																					else
																						{	/* SawBbv/bbv-liveness.scm 93 */
																							BgL_res1986z00_3404 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2172z00_5499 = BgL_res1986z00_3404;
																}
														}
													else
														{	/* SawBbv/bbv-liveness.scm 93 */
															BgL_test2172z00_5499 = ((bool_t) 0);
														}
												}
											}
											if (BgL_test2172z00_5499)
												{	/* SawBbv/bbv-liveness.scm 93 */
													obj_t BgL_nh1453z00_2235;

													{	/* SawBbv/bbv-liveness.scm 93 */
														obj_t BgL_arg1605z00_2237;

														BgL_arg1605z00_2237 =
															CAR(((obj_t) BgL_l1451z00_2229));
														BgL_nh1453z00_2235 =
															MAKE_YOUNG_PAIR(BgL_arg1605z00_2237, BNIL);
													}
													SET_CDR(BgL_h1452z00_2230, BgL_nh1453z00_2235);
													{	/* SawBbv/bbv-liveness.scm 93 */
														obj_t BgL_arg1602z00_2236;

														BgL_arg1602z00_2236 =
															CDR(((obj_t) BgL_l1451z00_2229));
														{
															obj_t BgL_h1452z00_5531;
															obj_t BgL_l1451z00_5530;

															BgL_l1451z00_5530 = BgL_arg1602z00_2236;
															BgL_h1452z00_5531 = BgL_nh1453z00_2235;
															BgL_h1452z00_2230 = BgL_h1452z00_5531;
															BgL_l1451z00_2229 = BgL_l1451z00_5530;
															goto BgL_zc3z04anonymousza31592ze3z87_2231;
														}
													}
												}
											else
												{	/* SawBbv/bbv-liveness.scm 93 */
													obj_t BgL_arg1606z00_2238;

													BgL_arg1606z00_2238 =
														CDR(((obj_t) BgL_l1451z00_2229));
													{
														obj_t BgL_l1451z00_5534;

														BgL_l1451z00_5534 = BgL_arg1606z00_2238;
														BgL_l1451z00_2229 = BgL_l1451z00_5534;
														goto BgL_zc3z04anonymousza31592ze3z87_2231;
													}
												}
										}
								}
							}
							{	/* SawBbv/bbv-liveness.scm 93 */
								obj_t BgL_regsz00_2197;

								BgL_regsz00_2197 =
									BGl_appendzd221011zd2zzsaw_bbvzd2livenesszd2
									(BgL_hregsz00_2195, BgL_cregsz00_2194);
								{	/* SawBbv/bbv-liveness.scm 94 */

									BGl_widenzd2bbvz12zc0zzsaw_bbvzd2livenesszd2
										(BgL_blocksz00_256, BgL_regsz00_2197);
									if (NULLP(BgL_blocksz00_256))
										{	/* SawBbv/bbv-liveness.scm 99 */
											((bool_t) 0);
										}
									else
										{	/* SawBbv/bbv-liveness.scm 100 */
											obj_t BgL_inssz00_2199;

											BgL_inssz00_2199 =
												(((BgL_blockz00_bglt) COBJECT(
														((BgL_blockz00_bglt)
															CAR(BgL_blocksz00_256))))->BgL_firstz00);
											if (NULLP(BgL_inssz00_2199))
												{	/* SawBbv/bbv-liveness.scm 101 */
													((bool_t) 0);
												}
											else
												{	/* SawBbv/bbv-liveness.scm 102 */
													obj_t BgL_insz00_2201;

													BgL_insz00_2201 = CAR(BgL_inssz00_2199);
													{
														obj_t BgL_l1455z00_2204;

														BgL_l1455z00_2204 = BgL_pregsz00_2196;
													BgL_zc3z04anonymousza31564ze3z87_2205:
														if (PAIRP(BgL_l1455z00_2204))
															{	/* SawBbv/bbv-liveness.scm 104 */
																{	/* SawBbv/bbv-liveness.scm 104 */
																	obj_t BgL_az00_2207;

																	BgL_az00_2207 = CAR(BgL_l1455z00_2204);
																	{	/* SawBbv/bbv-liveness.scm 104 */
																		obj_t BgL_arg1571z00_2208;

																		{
																			BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5548;

																			{
																				obj_t BgL_auxz00_5549;

																				{	/* SawBbv/bbv-liveness.scm 104 */
																					BgL_objectz00_bglt BgL_tmpz00_5550;

																					BgL_tmpz00_5550 =
																						((BgL_objectz00_bglt)
																						((BgL_rtl_insz00_bglt)
																							BgL_insz00_2201));
																					BgL_auxz00_5549 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_5550);
																				}
																				BgL_auxz00_5548 =
																					((BgL_rtl_inszf2bbvzf2_bglt)
																					BgL_auxz00_5549);
																			}
																			BgL_arg1571z00_2208 =
																				(((BgL_rtl_inszf2bbvzf2_bglt)
																					COBJECT(BgL_auxz00_5548))->BgL_inz00);
																		}
																		BGl_regsetzd2addz12zc0zzsaw_regsetz00(
																			((BgL_regsetz00_bglt)
																				BgL_arg1571z00_2208),
																			((BgL_rtl_regz00_bglt) BgL_az00_2207));
																	}
																}
																{
																	obj_t BgL_l1455z00_5559;

																	BgL_l1455z00_5559 = CDR(BgL_l1455z00_2204);
																	BgL_l1455z00_2204 = BgL_l1455z00_5559;
																	goto BgL_zc3z04anonymousza31564ze3z87_2205;
																}
															}
														else
															{	/* SawBbv/bbv-liveness.scm 104 */
																((bool_t) 1);
															}
													}
												}
										}
									{
										long BgL_iz00_2213;

										BgL_iz00_2213 = 0L;
									BgL_zc3z04anonymousza31576ze3z87_2214:
										{
											obj_t BgL_bsz00_2216;
											obj_t BgL_tz00_2217;

											BgL_bsz00_2216 = BgL_blocksz00_256;
											BgL_tz00_2217 = BFALSE;
										BgL_zc3z04anonymousza31577ze3z87_2218:
											if (NULLP(BgL_bsz00_2216))
												{	/* SawBbv/bbv-liveness.scm 109 */
													if (CBOOL(BgL_tz00_2217))
														{
															long BgL_iz00_5565;

															BgL_iz00_5565 = (BgL_iz00_2213 + 1L);
															BgL_iz00_2213 = BgL_iz00_5565;
															goto BgL_zc3z04anonymousza31576ze3z87_2214;
														}
													else
														{	/* SawBbv/bbv-liveness.scm 110 */
															return BgL_regsz00_2197;
														}
												}
											else
												{	/* SawBbv/bbv-liveness.scm 113 */
													obj_t BgL_arg1585z00_2221;
													obj_t BgL_arg1589z00_2222;

													BgL_arg1585z00_2221 = CDR(((obj_t) BgL_bsz00_2216));
													{	/* SawBbv/bbv-liveness.scm 113 */
														obj_t BgL__ortest_1197z00_2223;

														{	/* SawBbv/bbv-liveness.scm 113 */
															obj_t BgL_arg1591z00_2224;

															BgL_arg1591z00_2224 =
																CAR(((obj_t) BgL_bsz00_2216));
															BgL_blockz00_2307 = BgL_arg1591z00_2224;
															{	/* SawBbv/bbv-liveness.scm 82 */
																obj_t BgL_g1193z00_2310;
																obj_t BgL_g1194z00_2311;

																BgL_g1193z00_2310 =
																	bgl_reverse(
																	(((BgL_blockz00_bglt) COBJECT(
																				((BgL_blockz00_bglt)
																					BgL_blockz00_2307)))->BgL_firstz00));
																{	/* SawBbv/bbv-liveness.scm 83 */
																	obj_t BgL_l1445z00_2326;

																	BgL_l1445z00_2326 =
																		(((BgL_blockz00_bglt) COBJECT(
																				((BgL_blockz00_bglt)
																					BgL_blockz00_2307)))->BgL_succsz00);
																	if (NULLP(BgL_l1445z00_2326))
																		{	/* SawBbv/bbv-liveness.scm 83 */
																			BgL_g1194z00_2311 = BNIL;
																		}
																	else
																		{	/* SawBbv/bbv-liveness.scm 83 */
																			obj_t BgL_head1447z00_2328;

																			BgL_head1447z00_2328 =
																				MAKE_YOUNG_PAIR(BNIL, BNIL);
																			{
																				obj_t BgL_l1445z00_2330;
																				obj_t BgL_tail1448z00_2331;

																				BgL_l1445z00_2330 = BgL_l1445z00_2326;
																				BgL_tail1448z00_2331 =
																					BgL_head1447z00_2328;
																			BgL_zc3z04anonymousza31713ze3z87_2332:
																				if (NULLP(BgL_l1445z00_2330))
																					{	/* SawBbv/bbv-liveness.scm 83 */
																						BgL_g1194z00_2311 =
																							CDR(BgL_head1447z00_2328);
																					}
																				else
																					{	/* SawBbv/bbv-liveness.scm 83 */
																						obj_t BgL_newtail1449z00_2334;

																						{	/* SawBbv/bbv-liveness.scm 83 */
																							obj_t BgL_arg1718z00_2336;

																							{	/* SawBbv/bbv-liveness.scm 83 */
																								obj_t BgL_pairz00_3362;

																								BgL_pairz00_3362 =
																									(((BgL_blockz00_bglt) COBJECT(
																											((BgL_blockz00_bglt)
																												CAR(
																													((obj_t)
																														BgL_l1445z00_2330)))))->
																									BgL_firstz00);
																								BgL_arg1718z00_2336 =
																									CAR(BgL_pairz00_3362);
																							}
																							BgL_newtail1449z00_2334 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1718z00_2336, BNIL);
																						}
																						SET_CDR(BgL_tail1448z00_2331,
																							BgL_newtail1449z00_2334);
																						{	/* SawBbv/bbv-liveness.scm 83 */
																							obj_t BgL_arg1717z00_2335;

																							BgL_arg1717z00_2335 =
																								CDR(
																								((obj_t) BgL_l1445z00_2330));
																							{
																								obj_t BgL_tail1448z00_5592;
																								obj_t BgL_l1445z00_5591;

																								BgL_l1445z00_5591 =
																									BgL_arg1717z00_2335;
																								BgL_tail1448z00_5592 =
																									BgL_newtail1449z00_2334;
																								BgL_tail1448z00_2331 =
																									BgL_tail1448z00_5592;
																								BgL_l1445z00_2330 =
																									BgL_l1445z00_5591;
																								goto
																									BgL_zc3z04anonymousza31713ze3z87_2332;
																							}
																						}
																					}
																			}
																		}
																}
																{
																	obj_t BgL_inssz00_2313;
																	obj_t BgL_succz00_2314;
																	obj_t BgL_tz00_2315;

																	BgL_inssz00_2313 = BgL_g1193z00_2310;
																	BgL_succz00_2314 = BgL_g1194z00_2311;
																	BgL_tz00_2315 = BFALSE;
																BgL_zc3z04anonymousza31703ze3z87_2316:
																	if (PAIRP(BgL_inssz00_2313))
																		{	/* SawBbv/bbv-liveness.scm 86 */
																			obj_t BgL_uz00_2318;

																			BgL_uz00_2318 =
																				BGl_livenesszd2insz12ze70z27zzsaw_bbvzd2livenesszd2
																				(CAR(BgL_inssz00_2313),
																				BgL_succz00_2314);
																			{	/* SawBbv/bbv-liveness.scm 87 */
																				obj_t BgL_arg1705z00_2319;
																				obj_t BgL_arg1708z00_2320;
																				obj_t BgL_arg1709z00_2321;

																				BgL_arg1705z00_2319 =
																					CDR(BgL_inssz00_2313);
																				BgL_arg1708z00_2320 =
																					CAR(BgL_inssz00_2313);
																				if (CBOOL(BgL_tz00_2315))
																					{	/* SawBbv/bbv-liveness.scm 87 */
																						BgL_arg1709z00_2321 = BgL_tz00_2315;
																					}
																				else
																					{	/* SawBbv/bbv-liveness.scm 87 */
																						BgL_arg1709z00_2321 = BgL_uz00_2318;
																					}
																				{
																					obj_t BgL_tz00_5603;
																					obj_t BgL_succz00_5602;
																					obj_t BgL_inssz00_5601;

																					BgL_inssz00_5601 =
																						BgL_arg1705z00_2319;
																					BgL_succz00_5602 =
																						BgL_arg1708z00_2320;
																					BgL_tz00_5603 = BgL_arg1709z00_2321;
																					BgL_tz00_2315 = BgL_tz00_5603;
																					BgL_succz00_2314 = BgL_succz00_5602;
																					BgL_inssz00_2313 = BgL_inssz00_5601;
																					goto
																						BgL_zc3z04anonymousza31703ze3z87_2316;
																				}
																			}
																		}
																	else
																		{	/* SawBbv/bbv-liveness.scm 85 */
																			BgL__ortest_1197z00_2223 = BgL_tz00_2315;
																		}
																}
															}
														}
														if (CBOOL(BgL__ortest_1197z00_2223))
															{	/* SawBbv/bbv-liveness.scm 113 */
																BgL_arg1589z00_2222 = BgL__ortest_1197z00_2223;
															}
														else
															{	/* SawBbv/bbv-liveness.scm 113 */
																BgL_arg1589z00_2222 = BgL_tz00_2217;
															}
													}
													{
														obj_t BgL_tz00_5607;
														obj_t BgL_bsz00_5606;

														BgL_bsz00_5606 = BgL_arg1585z00_2221;
														BgL_tz00_5607 = BgL_arg1589z00_2222;
														BgL_tz00_2217 = BgL_tz00_5607;
														BgL_bsz00_2216 = BgL_bsz00_5606;
														goto BgL_zc3z04anonymousza31577ze3z87_2218;
													}
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* liveness-ins!~0 */
	obj_t BGl_livenesszd2insz12ze70z27zzsaw_bbvzd2livenesszd2(obj_t
		BgL_insz00_2246, obj_t BgL_succz00_2247)
	{
		{	/* SawBbv/bbv-liveness.scm 78 */
			{	/* SawBbv/bbv-liveness.scm 62 */
				obj_t BgL_subz00_2250;

				{	/* SawBbv/bbv-liveness.scm 62 */
					obj_t BgL_l1431z00_2293;

					BgL_l1431z00_2293 =
						(((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt)
									((BgL_rtl_insz00_bglt) BgL_insz00_2246))))->BgL_argsz00);
					if (NULLP(BgL_l1431z00_2293))
						{	/* SawBbv/bbv-liveness.scm 62 */
							BgL_subz00_2250 = BNIL;
						}
					else
						{	/* SawBbv/bbv-liveness.scm 62 */
							obj_t BgL_head1433z00_2295;

							BgL_head1433z00_2295 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1431z00_2297;
								obj_t BgL_tail1434z00_2298;

								BgL_l1431z00_2297 = BgL_l1431z00_2293;
								BgL_tail1434z00_2298 = BgL_head1433z00_2295;
							BgL_zc3z04anonymousza31691ze3z87_2299:
								if (NULLP(BgL_l1431z00_2297))
									{	/* SawBbv/bbv-liveness.scm 62 */
										BgL_subz00_2250 = CDR(BgL_head1433z00_2295);
									}
								else
									{	/* SawBbv/bbv-liveness.scm 62 */
										obj_t BgL_newtail1435z00_2301;

										{	/* SawBbv/bbv-liveness.scm 62 */
											obj_t BgL_arg1700z00_2303;

											{	/* SawBbv/bbv-liveness.scm 62 */
												obj_t BgL_az00_2304;

												BgL_az00_2304 = CAR(((obj_t) BgL_l1431z00_2297));
												{	/* SawBbv/bbv-liveness.scm 63 */
													bool_t BgL_test2189z00_5619;

													{	/* SawBbv/bbv-liveness.scm 63 */
														obj_t BgL_classz00_3228;

														BgL_classz00_3228 = BGl_rtl_insz00zzsaw_defsz00;
														if (BGL_OBJECTP(BgL_az00_2304))
															{	/* SawBbv/bbv-liveness.scm 63 */
																BgL_objectz00_bglt BgL_arg1807z00_3230;

																BgL_arg1807z00_3230 =
																	(BgL_objectz00_bglt) (BgL_az00_2304);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* SawBbv/bbv-liveness.scm 63 */
																		long BgL_idxz00_3236;

																		BgL_idxz00_3236 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_3230);
																		BgL_test2189z00_5619 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_3236 + 1L)) ==
																			BgL_classz00_3228);
																	}
																else
																	{	/* SawBbv/bbv-liveness.scm 63 */
																		bool_t BgL_res1984z00_3261;

																		{	/* SawBbv/bbv-liveness.scm 63 */
																			obj_t BgL_oclassz00_3244;

																			{	/* SawBbv/bbv-liveness.scm 63 */
																				obj_t BgL_arg1815z00_3252;
																				long BgL_arg1816z00_3253;

																				BgL_arg1815z00_3252 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* SawBbv/bbv-liveness.scm 63 */
																					long BgL_arg1817z00_3254;

																					BgL_arg1817z00_3254 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_3230);
																					BgL_arg1816z00_3253 =
																						(BgL_arg1817z00_3254 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_3244 =
																					VECTOR_REF(BgL_arg1815z00_3252,
																					BgL_arg1816z00_3253);
																			}
																			{	/* SawBbv/bbv-liveness.scm 63 */
																				bool_t BgL__ortest_1115z00_3245;

																				BgL__ortest_1115z00_3245 =
																					(BgL_classz00_3228 ==
																					BgL_oclassz00_3244);
																				if (BgL__ortest_1115z00_3245)
																					{	/* SawBbv/bbv-liveness.scm 63 */
																						BgL_res1984z00_3261 =
																							BgL__ortest_1115z00_3245;
																					}
																				else
																					{	/* SawBbv/bbv-liveness.scm 63 */
																						long BgL_odepthz00_3246;

																						{	/* SawBbv/bbv-liveness.scm 63 */
																							obj_t BgL_arg1804z00_3247;

																							BgL_arg1804z00_3247 =
																								(BgL_oclassz00_3244);
																							BgL_odepthz00_3246 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_3247);
																						}
																						if ((1L < BgL_odepthz00_3246))
																							{	/* SawBbv/bbv-liveness.scm 63 */
																								obj_t BgL_arg1802z00_3249;

																								{	/* SawBbv/bbv-liveness.scm 63 */
																									obj_t BgL_arg1803z00_3250;

																									BgL_arg1803z00_3250 =
																										(BgL_oclassz00_3244);
																									BgL_arg1802z00_3249 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_3250, 1L);
																								}
																								BgL_res1984z00_3261 =
																									(BgL_arg1802z00_3249 ==
																									BgL_classz00_3228);
																							}
																						else
																							{	/* SawBbv/bbv-liveness.scm 63 */
																								BgL_res1984z00_3261 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2189z00_5619 = BgL_res1984z00_3261;
																	}
															}
														else
															{	/* SawBbv/bbv-liveness.scm 63 */
																BgL_test2189z00_5619 = ((bool_t) 0);
															}
													}
													if (BgL_test2189z00_5619)
														{	/* SawBbv/bbv-liveness.scm 63 */
															BgL_arg1700z00_2303 =
																BGl_livenesszd2insz12ze70z27zzsaw_bbvzd2livenesszd2
																(BgL_az00_2304, BgL_insz00_2246);
														}
													else
														{	/* SawBbv/bbv-liveness.scm 63 */
															BgL_arg1700z00_2303 = BFALSE;
														}
												}
											}
											BgL_newtail1435z00_2301 =
												MAKE_YOUNG_PAIR(BgL_arg1700z00_2303, BNIL);
										}
										SET_CDR(BgL_tail1434z00_2298, BgL_newtail1435z00_2301);
										{	/* SawBbv/bbv-liveness.scm 62 */
											obj_t BgL_arg1699z00_2302;

											BgL_arg1699z00_2302 = CDR(((obj_t) BgL_l1431z00_2297));
											{
												obj_t BgL_tail1434z00_5648;
												obj_t BgL_l1431z00_5647;

												BgL_l1431z00_5647 = BgL_arg1699z00_2302;
												BgL_tail1434z00_5648 = BgL_newtail1435z00_2301;
												BgL_tail1434z00_2298 = BgL_tail1434z00_5648;
												BgL_l1431z00_2297 = BgL_l1431z00_5647;
												goto BgL_zc3z04anonymousza31691ze3z87_2299;
											}
										}
									}
							}
						}
				}
				{	/* SawBbv/bbv-liveness.scm 66 */
					obj_t BgL_uz00_3991;

					{	/* SawBbv/bbv-liveness.scm 67 */
						obj_t BgL_cellvalz00_5649;

						{	/* SawBbv/bbv-liveness.scm 67 */
							bool_t BgL_test2194z00_5650;

							{	/* SawBbv/bbv-liveness.scm 67 */
								obj_t BgL_classz00_3264;

								BgL_classz00_3264 = BGl_rtl_insz00zzsaw_defsz00;
								if (BGL_OBJECTP(BgL_succz00_2247))
									{	/* SawBbv/bbv-liveness.scm 67 */
										BgL_objectz00_bglt BgL_arg1807z00_3266;

										BgL_arg1807z00_3266 =
											(BgL_objectz00_bglt) (BgL_succz00_2247);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawBbv/bbv-liveness.scm 67 */
												long BgL_idxz00_3272;

												BgL_idxz00_3272 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3266);
												BgL_test2194z00_5650 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3272 + 1L)) == BgL_classz00_3264);
											}
										else
											{	/* SawBbv/bbv-liveness.scm 67 */
												bool_t BgL_res1985z00_3297;

												{	/* SawBbv/bbv-liveness.scm 67 */
													obj_t BgL_oclassz00_3280;

													{	/* SawBbv/bbv-liveness.scm 67 */
														obj_t BgL_arg1815z00_3288;
														long BgL_arg1816z00_3289;

														BgL_arg1815z00_3288 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawBbv/bbv-liveness.scm 67 */
															long BgL_arg1817z00_3290;

															BgL_arg1817z00_3290 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3266);
															BgL_arg1816z00_3289 =
																(BgL_arg1817z00_3290 - OBJECT_TYPE);
														}
														BgL_oclassz00_3280 =
															VECTOR_REF(BgL_arg1815z00_3288,
															BgL_arg1816z00_3289);
													}
													{	/* SawBbv/bbv-liveness.scm 67 */
														bool_t BgL__ortest_1115z00_3281;

														BgL__ortest_1115z00_3281 =
															(BgL_classz00_3264 == BgL_oclassz00_3280);
														if (BgL__ortest_1115z00_3281)
															{	/* SawBbv/bbv-liveness.scm 67 */
																BgL_res1985z00_3297 = BgL__ortest_1115z00_3281;
															}
														else
															{	/* SawBbv/bbv-liveness.scm 67 */
																long BgL_odepthz00_3282;

																{	/* SawBbv/bbv-liveness.scm 67 */
																	obj_t BgL_arg1804z00_3283;

																	BgL_arg1804z00_3283 = (BgL_oclassz00_3280);
																	BgL_odepthz00_3282 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3283);
																}
																if ((1L < BgL_odepthz00_3282))
																	{	/* SawBbv/bbv-liveness.scm 67 */
																		obj_t BgL_arg1802z00_3285;

																		{	/* SawBbv/bbv-liveness.scm 67 */
																			obj_t BgL_arg1803z00_3286;

																			BgL_arg1803z00_3286 =
																				(BgL_oclassz00_3280);
																			BgL_arg1802z00_3285 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3286, 1L);
																		}
																		BgL_res1985z00_3297 =
																			(BgL_arg1802z00_3285 ==
																			BgL_classz00_3264);
																	}
																else
																	{	/* SawBbv/bbv-liveness.scm 67 */
																		BgL_res1985z00_3297 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2194z00_5650 = BgL_res1985z00_3297;
											}
									}
								else
									{	/* SawBbv/bbv-liveness.scm 67 */
										BgL_test2194z00_5650 = ((bool_t) 0);
									}
							}
							if (BgL_test2194z00_5650)
								{	/* SawBbv/bbv-liveness.scm 68 */
									obj_t BgL_arg1646z00_2272;
									obj_t BgL_arg1650z00_2273;

									{
										BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5673;

										{
											obj_t BgL_auxz00_5674;

											{	/* SawBbv/bbv-liveness.scm 68 */
												BgL_objectz00_bglt BgL_tmpz00_5675;

												BgL_tmpz00_5675 =
													((BgL_objectz00_bglt)
													((BgL_rtl_insz00_bglt) BgL_insz00_2246));
												BgL_auxz00_5674 = BGL_OBJECT_WIDENING(BgL_tmpz00_5675);
											}
											BgL_auxz00_5673 =
												((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_5674);
										}
										BgL_arg1646z00_2272 =
											(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_5673))->
											BgL_outz00);
									}
									{
										BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5681;

										{
											obj_t BgL_auxz00_5682;

											{	/* SawBbv/bbv-liveness.scm 57 */
												BgL_objectz00_bglt BgL_tmpz00_5683;

												BgL_tmpz00_5683 =
													((BgL_objectz00_bglt)
													((BgL_rtl_insz00_bglt) BgL_succz00_2247));
												BgL_auxz00_5682 = BGL_OBJECT_WIDENING(BgL_tmpz00_5683);
											}
											BgL_auxz00_5681 =
												((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_5682);
										}
										BgL_arg1650z00_2273 =
											(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_5681))->
											BgL_inz00);
									}
									BgL_cellvalz00_5649 =
										BBOOL(BGl_regsetzd2unionz12zc0zzsaw_regsetz00(
											((BgL_regsetz00_bglt) BgL_arg1646z00_2272),
											((BgL_regsetz00_bglt) BgL_arg1650z00_2273)));
								}
							else
								{	/* SawBbv/bbv-liveness.scm 67 */
									if (PAIRP(BgL_succz00_2247))
										{	/* SawBbv/bbv-liveness.scm 70 */
											obj_t BgL_arg1654z00_2275;
											obj_t BgL_arg1661z00_2276;

											{
												BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5695;

												{
													obj_t BgL_auxz00_5696;

													{	/* SawBbv/bbv-liveness.scm 70 */
														BgL_objectz00_bglt BgL_tmpz00_5697;

														BgL_tmpz00_5697 =
															((BgL_objectz00_bglt)
															((BgL_rtl_insz00_bglt) BgL_insz00_2246));
														BgL_auxz00_5696 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5697);
													}
													BgL_auxz00_5695 =
														((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_5696);
												}
												BgL_arg1654z00_2275 =
													(((BgL_rtl_inszf2bbvzf2_bglt)
														COBJECT(BgL_auxz00_5695))->BgL_outz00);
											}
											{	/* SawBbv/bbv-liveness.scm 70 */
												obj_t BgL_head1438z00_2279;

												{	/* SawBbv/bbv-liveness.scm 70 */
													obj_t BgL_arg1688z00_2291;

													{	/* SawBbv/bbv-liveness.scm 57 */
														BgL_rtl_insz00_bglt BgL_i1188z00_3303;

														BgL_i1188z00_3303 =
															((BgL_rtl_insz00_bglt) CAR(BgL_succz00_2247));
														{
															BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5705;

															{
																obj_t BgL_auxz00_5706;

																{	/* SawBbv/bbv-liveness.scm 57 */
																	BgL_objectz00_bglt BgL_tmpz00_5707;

																	BgL_tmpz00_5707 =
																		((BgL_objectz00_bglt) BgL_i1188z00_3303);
																	BgL_auxz00_5706 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_5707);
																}
																BgL_auxz00_5705 =
																	((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_5706);
															}
															BgL_arg1688z00_2291 =
																(((BgL_rtl_inszf2bbvzf2_bglt)
																	COBJECT(BgL_auxz00_5705))->BgL_inz00);
														}
													}
													BgL_head1438z00_2279 =
														MAKE_YOUNG_PAIR(BgL_arg1688z00_2291, BNIL);
												}
												{	/* SawBbv/bbv-liveness.scm 70 */
													obj_t BgL_g1441z00_2280;

													BgL_g1441z00_2280 = CDR(BgL_succz00_2247);
													{
														obj_t BgL_l1436z00_2282;
														obj_t BgL_tail1439z00_2283;

														BgL_l1436z00_2282 = BgL_g1441z00_2280;
														BgL_tail1439z00_2283 = BgL_head1438z00_2279;
													BgL_zc3z04anonymousza31664ze3z87_2284:
														if (NULLP(BgL_l1436z00_2282))
															{	/* SawBbv/bbv-liveness.scm 70 */
																BgL_arg1661z00_2276 = BgL_head1438z00_2279;
															}
														else
															{	/* SawBbv/bbv-liveness.scm 70 */
																obj_t BgL_newtail1440z00_2286;

																{	/* SawBbv/bbv-liveness.scm 70 */
																	obj_t BgL_arg1678z00_2288;

																	{	/* SawBbv/bbv-liveness.scm 57 */
																		BgL_rtl_insz00_bglt BgL_i1188z00_3307;

																		BgL_i1188z00_3307 =
																			((BgL_rtl_insz00_bglt)
																			CAR(((obj_t) BgL_l1436z00_2282)));
																		{
																			BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5719;

																			{
																				obj_t BgL_auxz00_5720;

																				{	/* SawBbv/bbv-liveness.scm 57 */
																					BgL_objectz00_bglt BgL_tmpz00_5721;

																					BgL_tmpz00_5721 =
																						((BgL_objectz00_bglt)
																						BgL_i1188z00_3307);
																					BgL_auxz00_5720 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_5721);
																				}
																				BgL_auxz00_5719 =
																					((BgL_rtl_inszf2bbvzf2_bglt)
																					BgL_auxz00_5720);
																			}
																			BgL_arg1678z00_2288 =
																				(((BgL_rtl_inszf2bbvzf2_bglt)
																					COBJECT(BgL_auxz00_5719))->BgL_inz00);
																		}
																	}
																	BgL_newtail1440z00_2286 =
																		MAKE_YOUNG_PAIR(BgL_arg1678z00_2288, BNIL);
																}
																SET_CDR(BgL_tail1439z00_2283,
																	BgL_newtail1440z00_2286);
																{	/* SawBbv/bbv-liveness.scm 70 */
																	obj_t BgL_arg1675z00_2287;

																	BgL_arg1675z00_2287 =
																		CDR(((obj_t) BgL_l1436z00_2282));
																	{
																		obj_t BgL_tail1439z00_5731;
																		obj_t BgL_l1436z00_5730;

																		BgL_l1436z00_5730 = BgL_arg1675z00_2287;
																		BgL_tail1439z00_5731 =
																			BgL_newtail1440z00_2286;
																		BgL_tail1439z00_2283 = BgL_tail1439z00_5731;
																		BgL_l1436z00_2282 = BgL_l1436z00_5730;
																		goto BgL_zc3z04anonymousza31664ze3z87_2284;
																	}
																}
															}
													}
												}
											}
											BgL_cellvalz00_5649 =
												BBOOL(BGl_regsetzd2unionza2z12z62zzsaw_regsetz00(
													((BgL_regsetz00_bglt) BgL_arg1654z00_2275),
													BgL_arg1661z00_2276));
										}
									else
										{	/* SawBbv/bbv-liveness.scm 69 */
											BgL_cellvalz00_5649 = BFALSE;
										}
								}
						}
						BgL_uz00_3991 = MAKE_CELL(BgL_cellvalz00_5649);
					}
					{	/* SawBbv/bbv-liveness.scm 74 */
						obj_t BgL_arg1616z00_2253;

						{
							BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5735;

							{
								obj_t BgL_auxz00_5736;

								{	/* SawBbv/bbv-liveness.scm 73 */
									BgL_objectz00_bglt BgL_tmpz00_5737;

									BgL_tmpz00_5737 =
										((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt) BgL_insz00_2246));
									BgL_auxz00_5736 = BGL_OBJECT_WIDENING(BgL_tmpz00_5737);
								}
								BgL_auxz00_5735 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_5736);
							}
							BgL_arg1616z00_2253 =
								(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_5735))->
								BgL_outz00);
						}
						{	/* SawBbv/bbv-liveness.scm 75 */
							obj_t BgL_zc3z04anonymousza31617ze3z87_3979;

							BgL_zc3z04anonymousza31617ze3z87_3979 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31617ze3ze5zzsaw_bbvzd2livenesszd2,
								(int) (1L), (int) (2L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31617ze3z87_3979, (int) (0L),
								((obj_t) ((BgL_rtl_insz00_bglt) BgL_insz00_2246)));
							PROCEDURE_SET(BgL_zc3z04anonymousza31617ze3z87_3979, (int) (1L),
								((obj_t) BgL_uz00_3991));
							BGl_regsetzd2forzd2eachz00zzsaw_regsetz00
								(BgL_zc3z04anonymousza31617ze3z87_3979,
								((BgL_regsetz00_bglt) BgL_arg1616z00_2253));
					}}
					{	/* SawBbv/bbv-liveness.scm 78 */
						obj_t BgL__ortest_1191z00_2262;

						BgL__ortest_1191z00_2262 = CELL_REF(BgL_uz00_3991);
						if (CBOOL(BgL__ortest_1191z00_2262))
							{	/* SawBbv/bbv-liveness.scm 78 */
								return BgL__ortest_1191z00_2262;
							}
						else
							{
								obj_t BgL_l1442z00_2264;

								BgL_l1442z00_2264 = BgL_subz00_2250;
							BgL_zc3z04anonymousza31630ze3z87_2265:
								if (NULLP(BgL_l1442z00_2264))
									{	/* SawBbv/bbv-liveness.scm 78 */
										return BFALSE;
									}
								else
									{	/* SawBbv/bbv-liveness.scm 78 */
										obj_t BgL__ortest_1444z00_2267;

										BgL__ortest_1444z00_2267 = CAR(((obj_t) BgL_l1442z00_2264));
										if (CBOOL(BgL__ortest_1444z00_2267))
											{	/* SawBbv/bbv-liveness.scm 78 */
												return BgL__ortest_1444z00_2267;
											}
										else
											{
												obj_t BgL_l1442z00_5763;

												BgL_l1442z00_5763 = CDR(((obj_t) BgL_l1442z00_2264));
												BgL_l1442z00_2264 = BgL_l1442z00_5763;
												goto BgL_zc3z04anonymousza31630ze3z87_2265;
											}
									}
							}
					}
				}
			}
		}

	}



/* &bbv-liveness! */
	obj_t BGl_z62bbvzd2livenessz12za2zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3980, obj_t BgL_backz00_3981, obj_t BgL_blocksz00_3982,
		obj_t BgL_paramsz00_3983)
	{
		{	/* SawBbv/bbv-liveness.scm 54 */
			return
				BGl_bbvzd2livenessz12zc0zzsaw_bbvzd2livenesszd2(
				((BgL_backendz00_bglt) BgL_backz00_3981), BgL_blocksz00_3982,
				BgL_paramsz00_3983);
		}

	}



/* &<@anonymous:1617> */
	obj_t BGl_z62zc3z04anonymousza31617ze3ze5zzsaw_bbvzd2livenesszd2(obj_t
		BgL_envz00_3984, obj_t BgL_rz00_3987)
	{
		{	/* SawBbv/bbv-liveness.scm 74 */
			{	/* SawBbv/bbv-liveness.scm 75 */
				BgL_rtl_insz00_bglt BgL_i1189z00_3985;
				obj_t BgL_uz00_3986;

				BgL_i1189z00_3985 =
					((BgL_rtl_insz00_bglt) PROCEDURE_REF(BgL_envz00_3984, (int) (0L)));
				BgL_uz00_3986 = PROCEDURE_REF(BgL_envz00_3984, (int) (1L));
				{	/* SawBbv/bbv-liveness.scm 75 */
					bool_t BgL_test2204z00_5773;

					{	/* SawBbv/bbv-liveness.scm 75 */
						obj_t BgL_arg1629z00_4151;

						{
							BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5774;

							{
								obj_t BgL_auxz00_5775;

								{	/* SawBbv/bbv-liveness.scm 75 */
									BgL_objectz00_bglt BgL_tmpz00_5776;

									BgL_tmpz00_5776 = ((BgL_objectz00_bglt) BgL_i1189z00_3985);
									BgL_auxz00_5775 = BGL_OBJECT_WIDENING(BgL_tmpz00_5776);
								}
								BgL_auxz00_5774 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_5775);
							}
							BgL_arg1629z00_4151 =
								(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_5774))->
								BgL_defz00);
						}
						{	/* SawBbv/bbv-liveness.scm 75 */
							long BgL_basez00_4152;
							long BgL_bitz00_4153;

							{	/* SawBbv/bbv-liveness.scm 75 */
								int BgL_arg1858z00_4154;

								{
									BgL_rtl_regzf2razf2_bglt BgL_auxz00_5781;

									{
										obj_t BgL_auxz00_5782;

										{	/* SawBbv/bbv-liveness.scm 75 */
											BgL_objectz00_bglt BgL_tmpz00_5783;

											BgL_tmpz00_5783 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_rz00_3987));
											BgL_auxz00_5782 = BGL_OBJECT_WIDENING(BgL_tmpz00_5783);
										}
										BgL_auxz00_5781 =
											((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5782);
									}
									BgL_arg1858z00_4154 =
										(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5781))->
										BgL_numz00);
								}
								BgL_basez00_4152 = ((long) (BgL_arg1858z00_4154) / 8L);
							}
							{	/* SawBbv/bbv-liveness.scm 75 */
								int BgL_arg1859z00_4155;

								{
									BgL_rtl_regzf2razf2_bglt BgL_auxz00_5791;

									{
										obj_t BgL_auxz00_5792;

										{	/* SawBbv/bbv-liveness.scm 75 */
											BgL_objectz00_bglt BgL_tmpz00_5793;

											BgL_tmpz00_5793 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_rz00_3987));
											BgL_auxz00_5792 = BGL_OBJECT_WIDENING(BgL_tmpz00_5793);
										}
										BgL_auxz00_5791 =
											((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5792);
									}
									BgL_arg1859z00_4155 =
										(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5791))->
										BgL_numz00);
								}
								{	/* SawBbv/bbv-liveness.scm 75 */
									long BgL_n1z00_4156;
									long BgL_n2z00_4157;

									BgL_n1z00_4156 = (long) (BgL_arg1859z00_4155);
									BgL_n2z00_4157 = 8L;
									{	/* SawBbv/bbv-liveness.scm 75 */
										bool_t BgL_test2205z00_5800;

										{	/* SawBbv/bbv-liveness.scm 75 */
											long BgL_arg1338z00_4158;

											BgL_arg1338z00_4158 =
												(((BgL_n1z00_4156) | (BgL_n2z00_4157)) & -2147483648);
											BgL_test2205z00_5800 = (BgL_arg1338z00_4158 == 0L);
										}
										if (BgL_test2205z00_5800)
											{	/* SawBbv/bbv-liveness.scm 75 */
												int32_t BgL_arg1334z00_4159;

												{	/* SawBbv/bbv-liveness.scm 75 */
													int32_t BgL_arg1336z00_4160;
													int32_t BgL_arg1337z00_4161;

													BgL_arg1336z00_4160 = (int32_t) (BgL_n1z00_4156);
													BgL_arg1337z00_4161 = (int32_t) (BgL_n2z00_4157);
													BgL_arg1334z00_4159 =
														(BgL_arg1336z00_4160 % BgL_arg1337z00_4161);
												}
												{	/* SawBbv/bbv-liveness.scm 75 */
													long BgL_arg1446z00_4162;

													BgL_arg1446z00_4162 = (long) (BgL_arg1334z00_4159);
													BgL_bitz00_4153 = (long) (BgL_arg1446z00_4162);
											}}
										else
											{	/* SawBbv/bbv-liveness.scm 75 */
												BgL_bitz00_4153 = (BgL_n1z00_4156 % BgL_n2z00_4157);
											}
									}
								}
							}
							if (
								(BgL_basez00_4152 <
									STRING_LENGTH(
										(((BgL_regsetz00_bglt) COBJECT(
													((BgL_regsetz00_bglt) BgL_arg1629z00_4151)))->
											BgL_stringz00))))
								{	/* SawBbv/bbv-liveness.scm 75 */
									BgL_test2204z00_5773 =
										(
										((STRING_REF(
													(((BgL_regsetz00_bglt) COBJECT(
																((BgL_regsetz00_bglt) BgL_arg1629z00_4151)))->
														BgL_stringz00),
													BgL_basez00_4152)) & (1L << (int) (BgL_bitz00_4153)))
										> 0L);
								}
							else
								{	/* SawBbv/bbv-liveness.scm 75 */
									BgL_test2204z00_5773 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2204z00_5773)
						{	/* SawBbv/bbv-liveness.scm 75 */
							return BFALSE;
						}
					else
						{	/* SawBbv/bbv-liveness.scm 76 */
							obj_t BgL_auxz00_4163;

							{	/* SawBbv/bbv-liveness.scm 76 */
								bool_t BgL__ortest_1190z00_4164;

								{	/* SawBbv/bbv-liveness.scm 76 */
									obj_t BgL_arg1627z00_4165;

									{
										BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5822;

										{
											obj_t BgL_auxz00_5823;

											{	/* SawBbv/bbv-liveness.scm 76 */
												BgL_objectz00_bglt BgL_tmpz00_5824;

												BgL_tmpz00_5824 =
													((BgL_objectz00_bglt) BgL_i1189z00_3985);
												BgL_auxz00_5823 = BGL_OBJECT_WIDENING(BgL_tmpz00_5824);
											}
											BgL_auxz00_5822 =
												((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_5823);
										}
										BgL_arg1627z00_4165 =
											(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_5822))->
											BgL_inz00);
									}
									BgL__ortest_1190z00_4164 =
										BGl_regsetzd2addz12zc0zzsaw_regsetz00(
										((BgL_regsetz00_bglt) BgL_arg1627z00_4165),
										((BgL_rtl_regz00_bglt) BgL_rz00_3987));
								}
								if (BgL__ortest_1190z00_4164)
									{	/* SawBbv/bbv-liveness.scm 76 */
										BgL_auxz00_4163 = BBOOL(BgL__ortest_1190z00_4164);
									}
								else
									{	/* SawBbv/bbv-liveness.scm 76 */
										BgL_auxz00_4163 = CELL_REF(BgL_uz00_3986);
									}
							}
							return CELL_SET(BgL_uz00_3986, BgL_auxz00_4163);
						}
				}
			}
		}

	}



/* widen-bbv! */
	bool_t BGl_widenzd2bbvz12zc0zzsaw_bbvzd2livenesszd2(obj_t BgL_oz00_258,
		obj_t BgL_regsz00_259)
	{
		{	/* SawBbv/bbv-liveness.scm 121 */
			{
				obj_t BgL_oz00_2396;
				obj_t BgL_oz00_2406;
				obj_t BgL_oz00_2438;

				{
					obj_t BgL_l1472z00_2349;

					BgL_l1472z00_2349 = BgL_oz00_258;
				BgL_zc3z04anonymousza31721ze3z87_2350:
					if (PAIRP(BgL_l1472z00_2349))
						{	/* SawBbv/bbv-liveness.scm 169 */
							BgL_oz00_2438 = CAR(BgL_l1472z00_2349);
							{	/* SawBbv/bbv-liveness.scm 165 */
								BgL_blockvz00_bglt BgL_wide1217z00_2442;

								BgL_wide1217z00_2442 =
									((BgL_blockvz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockvz00_bgl))));
								{	/* SawBbv/bbv-liveness.scm 165 */
									obj_t BgL_auxz00_5841;
									BgL_objectz00_bglt BgL_tmpz00_5837;

									BgL_auxz00_5841 = ((obj_t) BgL_wide1217z00_2442);
									BgL_tmpz00_5837 =
										((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_2438)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5837, BgL_auxz00_5841);
								}
								((BgL_objectz00_bglt)
									((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_2438)));
								{	/* SawBbv/bbv-liveness.scm 165 */
									long BgL_arg1840z00_2443;

									BgL_arg1840z00_2443 =
										BGL_CLASS_NUM(BGl_blockVz00zzsaw_bbvzd2typeszd2);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_blockz00_bglt)
												((BgL_blockz00_bglt) BgL_oz00_2438))),
										BgL_arg1840z00_2443);
								}
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_2438)));
							}
							{
								BgL_blockvz00_bglt BgL_auxz00_5855;

								{
									obj_t BgL_auxz00_5856;

									{	/* SawBbv/bbv-types.scm 47 */
										BgL_objectz00_bglt BgL_tmpz00_5857;

										BgL_tmpz00_5857 =
											((BgL_objectz00_bglt)
											((BgL_blockz00_bglt)
												((BgL_blockz00_bglt) BgL_oz00_2438)));
										BgL_auxz00_5856 = BGL_OBJECT_WIDENING(BgL_tmpz00_5857);
									}
									BgL_auxz00_5855 = ((BgL_blockvz00_bglt) BgL_auxz00_5856);
								}
								((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_5855))->
										BgL_versionsz00) = ((obj_t) BNIL), BUNSPEC);
							}
							{
								BgL_blockvz00_bglt BgL_auxz00_5864;

								{
									obj_t BgL_auxz00_5865;

									{	/* SawBbv/bbv-types.scm 47 */
										BgL_objectz00_bglt BgL_tmpz00_5866;

										BgL_tmpz00_5866 =
											((BgL_objectz00_bglt)
											((BgL_blockz00_bglt)
												((BgL_blockz00_bglt) BgL_oz00_2438)));
										BgL_auxz00_5865 = BGL_OBJECT_WIDENING(BgL_tmpz00_5866);
									}
									BgL_auxz00_5864 = ((BgL_blockvz00_bglt) BgL_auxz00_5865);
								}
								((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_5864))->
										BgL_genericz00) = ((obj_t) BUNSPEC), BUNSPEC);
							}
							{
								BgL_blockvz00_bglt BgL_auxz00_5873;

								{
									obj_t BgL_auxz00_5874;

									{	/* SawBbv/bbv-types.scm 47 */
										BgL_objectz00_bglt BgL_tmpz00_5875;

										BgL_tmpz00_5875 =
											((BgL_objectz00_bglt)
											((BgL_blockz00_bglt)
												((BgL_blockz00_bglt) BgL_oz00_2438)));
										BgL_auxz00_5874 = BGL_OBJECT_WIDENING(BgL_tmpz00_5875);
									}
									BgL_auxz00_5873 = ((BgL_blockvz00_bglt) BgL_auxz00_5874);
								}
								((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_5873))->
										BgL_z52markz52) = ((long) -1L), BUNSPEC);
							}
							{
								BgL_blockvz00_bglt BgL_auxz00_5882;

								{
									obj_t BgL_auxz00_5883;

									{	/* SawBbv/bbv-types.scm 47 */
										BgL_objectz00_bglt BgL_tmpz00_5884;

										BgL_tmpz00_5884 =
											((BgL_objectz00_bglt)
											((BgL_blockz00_bglt)
												((BgL_blockz00_bglt) BgL_oz00_2438)));
										BgL_auxz00_5883 = BGL_OBJECT_WIDENING(BgL_tmpz00_5884);
									}
									BgL_auxz00_5882 = ((BgL_blockvz00_bglt) BgL_auxz00_5883);
								}
								((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_5882))->
										BgL_mergez00) = ((obj_t) BUNSPEC), BUNSPEC);
							}
							((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_2438));
							{	/* SawBbv/bbv-liveness.scm 167 */
								obj_t BgL_g1471z00_2446;

								BgL_g1471z00_2446 =
									(((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt) BgL_oz00_2438)))->BgL_firstz00);
								{
									obj_t BgL_l1469z00_2448;

									BgL_l1469z00_2448 = BgL_g1471z00_2446;
								BgL_zc3z04anonymousza31841ze3z87_2449:
									if (PAIRP(BgL_l1469z00_2448))
										{	/* SawBbv/bbv-liveness.scm 167 */
											BgL_oz00_2406 = CAR(BgL_l1469z00_2448);
											{	/* SawBbv/bbv-liveness.scm 150 */
												BgL_rtl_inszf2bbvzf2_bglt BgL_wide1209z00_2411;

												BgL_wide1209z00_2411 =
													((BgL_rtl_inszf2bbvzf2_bglt)
													BOBJECT(GC_MALLOC(sizeof(struct
																BgL_rtl_inszf2bbvzf2_bgl))));
												{	/* SawBbv/bbv-liveness.scm 150 */
													obj_t BgL_auxz00_5902;
													BgL_objectz00_bglt BgL_tmpz00_5898;

													BgL_auxz00_5902 = ((obj_t) BgL_wide1209z00_2411);
													BgL_tmpz00_5898 =
														((BgL_objectz00_bglt)
														((BgL_rtl_insz00_bglt)
															((BgL_rtl_insz00_bglt) BgL_oz00_2406)));
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5898,
														BgL_auxz00_5902);
												}
												((BgL_objectz00_bglt)
													((BgL_rtl_insz00_bglt)
														((BgL_rtl_insz00_bglt) BgL_oz00_2406)));
												{	/* SawBbv/bbv-liveness.scm 150 */
													long BgL_arg1805z00_2412;

													BgL_arg1805z00_2412 =
														BGL_CLASS_NUM
														(BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2);
													BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																(BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt)
																	BgL_oz00_2406))), BgL_arg1805z00_2412);
												}
												((BgL_rtl_insz00_bglt)
													((BgL_rtl_insz00_bglt)
														((BgL_rtl_insz00_bglt) BgL_oz00_2406)));
											}
											{
												obj_t BgL_auxz00_5924;
												BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5916;

												{	/* SawBbv/bbv-liveness.scm 152 */
													obj_t BgL_g1211z00_2414;

													BgL_oz00_2396 = BgL_oz00_2406;
													if (BGl_rtl_inszd2fxovopzf3z21zzsaw_bbvzd2typeszd2(
															((BgL_rtl_insz00_bglt) BgL_oz00_2396)))
														{	/* SawBbv/bbv-liveness.scm 146 */
															obj_t BgL_pairz00_3508;

															{
																BgL_rtl_insz00_bglt BgL_auxz00_5928;

																{	/* SawBbv/bbv-liveness.scm 145 */
																	obj_t BgL_pairz00_3507;

																	BgL_pairz00_3507 =
																		(((BgL_rtl_insz00_bglt) COBJECT(
																				((BgL_rtl_insz00_bglt)
																					BgL_oz00_2396)))->BgL_argsz00);
																	BgL_auxz00_5928 =
																		((BgL_rtl_insz00_bglt)
																		CAR(BgL_pairz00_3507));
																}
																BgL_pairz00_3508 =
																	(((BgL_rtl_insz00_bglt)
																		COBJECT(BgL_auxz00_5928))->BgL_argsz00);
															}
															BgL_g1211z00_2414 =
																CAR(CDR(CDR(BgL_pairz00_3508)));
														}
													else
														{	/* SawBbv/bbv-liveness.scm 139 */
															BgL_g1211z00_2414 = BFALSE;
														}
													if (CBOOL(BgL_g1211z00_2414))
														{	/* SawBbv/bbv-liveness.scm 155 */
															obj_t BgL_arg1806z00_2417;

															{	/* SawBbv/bbv-liveness.scm 155 */
																obj_t BgL_list1807z00_2418;

																BgL_list1807z00_2418 =
																	MAKE_YOUNG_PAIR(BgL_g1211z00_2414, BNIL);
																BgL_arg1806z00_2417 = BgL_list1807z00_2418;
															}
															BgL_auxz00_5924 =
																((obj_t)
																BGl_listzd2ze3regsetz31zzsaw_regsetz00
																(BgL_arg1806z00_2417, BgL_regsz00_259));
														}
													else
														{	/* SawBbv/bbv-liveness.scm 156 */
															bool_t BgL_test2212z00_5942;

															if (CBOOL(
																	(((BgL_rtl_insz00_bglt) COBJECT(
																				((BgL_rtl_insz00_bglt)
																					BgL_oz00_2406)))->BgL_destz00)))
																{	/* SawBbv/bbv-liveness.scm 156 */
																	BgL_test2212z00_5942 =
																		CBOOL(
																		(((BgL_rtl_regz00_bglt) COBJECT(
																					((BgL_rtl_regz00_bglt)
																						(((BgL_rtl_insz00_bglt) COBJECT(
																									((BgL_rtl_insz00_bglt)
																										BgL_oz00_2406)))->
																							BgL_destz00))))->
																			BgL_onexprzf3zf3));
																}
															else
																{	/* SawBbv/bbv-liveness.scm 156 */
																	BgL_test2212z00_5942 = ((bool_t) 1);
																}
															if (BgL_test2212z00_5942)
																{	/* SawBbv/bbv-liveness.scm 156 */
																	BgL_auxz00_5924 =
																		((obj_t)
																		BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00
																		(BgL_regsz00_259));
																}
															else
																{	/* SawBbv/bbv-liveness.scm 159 */
																	obj_t BgL_arg1820z00_2422;

																	{	/* SawBbv/bbv-liveness.scm 159 */
																		obj_t BgL_arg1822z00_2423;

																		BgL_arg1822z00_2423 =
																			(((BgL_rtl_insz00_bglt) COBJECT(
																					((BgL_rtl_insz00_bglt)
																						BgL_oz00_2406)))->BgL_destz00);
																		{	/* SawBbv/bbv-liveness.scm 159 */
																			obj_t BgL_list1823z00_2424;

																			BgL_list1823z00_2424 =
																				MAKE_YOUNG_PAIR(BgL_arg1822z00_2423,
																				BNIL);
																			BgL_arg1820z00_2422 =
																				BgL_list1823z00_2424;
																		}
																	}
																	BgL_auxz00_5924 =
																		((obj_t)
																		BGl_listzd2ze3regsetz31zzsaw_regsetz00
																		(BgL_arg1820z00_2422, BgL_regsz00_259));
																}
														}
												}
												{
													obj_t BgL_auxz00_5917;

													{	/* SawBbv/bbv-liveness.scm 151 */
														BgL_objectz00_bglt BgL_tmpz00_5918;

														BgL_tmpz00_5918 =
															((BgL_objectz00_bglt)
															((BgL_rtl_insz00_bglt)
																((BgL_rtl_insz00_bglt) BgL_oz00_2406)));
														BgL_auxz00_5917 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5918);
													}
													BgL_auxz00_5916 =
														((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_5917);
												}
												((((BgL_rtl_inszf2bbvzf2_bglt)
															COBJECT(BgL_auxz00_5916))->BgL_defz00) =
													((obj_t) BgL_auxz00_5924), BUNSPEC);
											}
											{
												BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5960;

												{
													obj_t BgL_auxz00_5961;

													{	/* SawBbv/bbv-liveness.scm 161 */
														BgL_objectz00_bglt BgL_tmpz00_5962;

														BgL_tmpz00_5962 =
															((BgL_objectz00_bglt)
															((BgL_rtl_insz00_bglt)
																((BgL_rtl_insz00_bglt) BgL_oz00_2406)));
														BgL_auxz00_5961 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5962);
													}
													BgL_auxz00_5960 =
														((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_5961);
												}
												((((BgL_rtl_inszf2bbvzf2_bglt)
															COBJECT(BgL_auxz00_5960))->BgL_outz00) =
													((obj_t) ((obj_t)
															BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00
															(BgL_regsz00_259))), BUNSPEC);
											}
											{
												obj_t BgL_auxz00_5979;
												BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5971;

												{	/* SawBbv/bbv-liveness.scm 160 */
													obj_t BgL_arg1832z00_2427;

													BgL_arg1832z00_2427 =
														BGl_getzd2argsze70z35zzsaw_bbvzd2livenesszd2
														(BgL_oz00_2406);
													BgL_auxz00_5979 =
														((obj_t)
														BGl_listzd2ze3regsetz31zzsaw_regsetz00
														(BgL_arg1832z00_2427, BgL_regsz00_259));
												}
												{
													obj_t BgL_auxz00_5972;

													{	/* SawBbv/bbv-liveness.scm 160 */
														BgL_objectz00_bglt BgL_tmpz00_5973;

														BgL_tmpz00_5973 =
															((BgL_objectz00_bglt)
															((BgL_rtl_insz00_bglt)
																((BgL_rtl_insz00_bglt) BgL_oz00_2406)));
														BgL_auxz00_5972 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5973);
													}
													BgL_auxz00_5971 =
														((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_5972);
												}
												((((BgL_rtl_inszf2bbvzf2_bglt)
															COBJECT(BgL_auxz00_5971))->BgL_inz00) =
													((obj_t) BgL_auxz00_5979), BUNSPEC);
											}
											{
												BgL_bbvzd2ctxzd2_bglt BgL_auxz00_5992;
												BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5984;

												{	/* SawBbv/bbv-liveness.scm 160 */
													obj_t BgL_arg1833z00_2428;

													{	/* SawBbv/bbv-liveness.scm 160 */
														obj_t BgL_arg1834z00_2429;

														{	/* SawBbv/bbv-liveness.scm 160 */
															obj_t BgL_classz00_3525;

															BgL_classz00_3525 =
																BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2;
															BgL_arg1834z00_2429 =
																BGL_CLASS_ALL_FIELDS(BgL_classz00_3525);
														}
														BgL_arg1833z00_2428 =
															VECTOR_REF(BgL_arg1834z00_2429, 8L);
													}
													BgL_auxz00_5992 =
														((BgL_bbvzd2ctxzd2_bglt)
														BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
														(BgL_arg1833z00_2428));
												}
												{
													obj_t BgL_auxz00_5985;

													{	/* SawBbv/bbv-liveness.scm 160 */
														BgL_objectz00_bglt BgL_tmpz00_5986;

														BgL_tmpz00_5986 =
															((BgL_objectz00_bglt)
															((BgL_rtl_insz00_bglt)
																((BgL_rtl_insz00_bglt) BgL_oz00_2406)));
														BgL_auxz00_5985 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5986);
													}
													BgL_auxz00_5984 =
														((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_5985);
												}
												((((BgL_rtl_inszf2bbvzf2_bglt)
															COBJECT(BgL_auxz00_5984))->BgL_ctxz00) =
													((BgL_bbvzd2ctxzd2_bglt) BgL_auxz00_5992), BUNSPEC);
											}
											{
												BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5998;

												{
													obj_t BgL_auxz00_5999;

													{	/* SawBbv/bbv-liveness.scm 160 */
														BgL_objectz00_bglt BgL_tmpz00_6000;

														BgL_tmpz00_6000 =
															((BgL_objectz00_bglt)
															((BgL_rtl_insz00_bglt)
																((BgL_rtl_insz00_bglt) BgL_oz00_2406)));
														BgL_auxz00_5999 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_6000);
													}
													BgL_auxz00_5998 =
														((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_5999);
												}
												((((BgL_rtl_inszf2bbvzf2_bglt)
															COBJECT(BgL_auxz00_5998))->BgL_z52hashz52) =
													((obj_t) BFALSE), BUNSPEC);
											}
											((BgL_rtl_insz00_bglt)
												((BgL_rtl_insz00_bglt) BgL_oz00_2406));
											{	/* SawBbv/bbv-liveness.scm 162 */
												obj_t BgL_g1468z00_2430;

												BgL_g1468z00_2430 =
													(((BgL_rtl_insz00_bglt) COBJECT(
															((BgL_rtl_insz00_bglt) BgL_oz00_2406)))->
													BgL_argsz00);
												{
													obj_t BgL_l1466z00_2432;

													BgL_l1466z00_2432 = BgL_g1468z00_2430;
												BgL_zc3z04anonymousza31835ze3z87_2433:
													if (PAIRP(BgL_l1466z00_2432))
														{	/* SawBbv/bbv-liveness.scm 162 */
															BGl_argszd2widenzd2bbvz12ze70zf5zzsaw_bbvzd2livenesszd2
																(BgL_regsz00_259, CAR(BgL_l1466z00_2432));
															{
																obj_t BgL_l1466z00_6015;

																BgL_l1466z00_6015 = CDR(BgL_l1466z00_2432);
																BgL_l1466z00_2432 = BgL_l1466z00_6015;
																goto BgL_zc3z04anonymousza31835ze3z87_2433;
															}
														}
													else
														{	/* SawBbv/bbv-liveness.scm 162 */
															((bool_t) 1);
														}
												}
											}
											{
												obj_t BgL_l1469z00_6018;

												BgL_l1469z00_6018 = CDR(BgL_l1469z00_2448);
												BgL_l1469z00_2448 = BgL_l1469z00_6018;
												goto BgL_zc3z04anonymousza31841ze3z87_2449;
											}
										}
									else
										{	/* SawBbv/bbv-liveness.scm 167 */
											((bool_t) 1);
										}
								}
							}
							{
								obj_t BgL_l1472z00_6021;

								BgL_l1472z00_6021 = CDR(BgL_l1472z00_2349);
								BgL_l1472z00_2349 = BgL_l1472z00_6021;
								goto BgL_zc3z04anonymousza31721ze3z87_2350;
							}
						}
					else
						{	/* SawBbv/bbv-liveness.scm 169 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* get-args~0 */
	obj_t BGl_getzd2argsze70z35zzsaw_bbvzd2livenesszd2(obj_t BgL_oz00_2355)
	{
		{	/* SawBbv/bbv-liveness.scm 124 */
			{	/* SawBbv/bbv-liveness.scm 124 */
				obj_t BgL_hook1461z00_2357;

				BgL_hook1461z00_2357 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
				{	/* SawBbv/bbv-liveness.scm 124 */
					obj_t BgL_g1462z00_2358;

					BgL_g1462z00_2358 =
						BGl_rtl_inszd2argsza2z70zzsaw_defsz00(
						((BgL_rtl_insz00_bglt) BgL_oz00_2355));
					{
						obj_t BgL_l1458z00_2360;
						obj_t BgL_h1459z00_2361;

						BgL_l1458z00_2360 = BgL_g1462z00_2358;
						BgL_h1459z00_2361 = BgL_hook1461z00_2357;
					BgL_zc3z04anonymousza31735ze3z87_2362:
						if (NULLP(BgL_l1458z00_2360))
							{	/* SawBbv/bbv-liveness.scm 124 */
								return CDR(BgL_hook1461z00_2357);
							}
						else
							{	/* SawBbv/bbv-liveness.scm 124 */
								bool_t BgL_test2216z00_6029;

								{	/* SawBbv/bbv-liveness.scm 124 */
									obj_t BgL_arg1747z00_2370;

									BgL_arg1747z00_2370 = CAR(((obj_t) BgL_l1458z00_2360));
									{	/* SawBbv/bbv-liveness.scm 124 */
										obj_t BgL_classz00_3420;

										BgL_classz00_3420 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
										if (BGL_OBJECTP(BgL_arg1747z00_2370))
											{	/* SawBbv/bbv-liveness.scm 124 */
												BgL_objectz00_bglt BgL_arg1807z00_3422;

												BgL_arg1807z00_3422 =
													(BgL_objectz00_bglt) (BgL_arg1747z00_2370);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawBbv/bbv-liveness.scm 124 */
														long BgL_idxz00_3428;

														BgL_idxz00_3428 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3422);
														BgL_test2216z00_6029 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3428 + 2L)) == BgL_classz00_3420);
													}
												else
													{	/* SawBbv/bbv-liveness.scm 124 */
														bool_t BgL_res1987z00_3453;

														{	/* SawBbv/bbv-liveness.scm 124 */
															obj_t BgL_oclassz00_3436;

															{	/* SawBbv/bbv-liveness.scm 124 */
																obj_t BgL_arg1815z00_3444;
																long BgL_arg1816z00_3445;

																BgL_arg1815z00_3444 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawBbv/bbv-liveness.scm 124 */
																	long BgL_arg1817z00_3446;

																	BgL_arg1817z00_3446 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3422);
																	BgL_arg1816z00_3445 =
																		(BgL_arg1817z00_3446 - OBJECT_TYPE);
																}
																BgL_oclassz00_3436 =
																	VECTOR_REF(BgL_arg1815z00_3444,
																	BgL_arg1816z00_3445);
															}
															{	/* SawBbv/bbv-liveness.scm 124 */
																bool_t BgL__ortest_1115z00_3437;

																BgL__ortest_1115z00_3437 =
																	(BgL_classz00_3420 == BgL_oclassz00_3436);
																if (BgL__ortest_1115z00_3437)
																	{	/* SawBbv/bbv-liveness.scm 124 */
																		BgL_res1987z00_3453 =
																			BgL__ortest_1115z00_3437;
																	}
																else
																	{	/* SawBbv/bbv-liveness.scm 124 */
																		long BgL_odepthz00_3438;

																		{	/* SawBbv/bbv-liveness.scm 124 */
																			obj_t BgL_arg1804z00_3439;

																			BgL_arg1804z00_3439 =
																				(BgL_oclassz00_3436);
																			BgL_odepthz00_3438 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3439);
																		}
																		if ((2L < BgL_odepthz00_3438))
																			{	/* SawBbv/bbv-liveness.scm 124 */
																				obj_t BgL_arg1802z00_3441;

																				{	/* SawBbv/bbv-liveness.scm 124 */
																					obj_t BgL_arg1803z00_3442;

																					BgL_arg1803z00_3442 =
																						(BgL_oclassz00_3436);
																					BgL_arg1802z00_3441 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3442, 2L);
																				}
																				BgL_res1987z00_3453 =
																					(BgL_arg1802z00_3441 ==
																					BgL_classz00_3420);
																			}
																		else
																			{	/* SawBbv/bbv-liveness.scm 124 */
																				BgL_res1987z00_3453 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2216z00_6029 = BgL_res1987z00_3453;
													}
											}
										else
											{	/* SawBbv/bbv-liveness.scm 124 */
												BgL_test2216z00_6029 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2216z00_6029)
									{	/* SawBbv/bbv-liveness.scm 124 */
										obj_t BgL_nh1460z00_2366;

										{	/* SawBbv/bbv-liveness.scm 124 */
											obj_t BgL_arg1740z00_2368;

											BgL_arg1740z00_2368 = CAR(((obj_t) BgL_l1458z00_2360));
											BgL_nh1460z00_2366 =
												MAKE_YOUNG_PAIR(BgL_arg1740z00_2368, BNIL);
										}
										SET_CDR(BgL_h1459z00_2361, BgL_nh1460z00_2366);
										{	/* SawBbv/bbv-liveness.scm 124 */
											obj_t BgL_arg1739z00_2367;

											BgL_arg1739z00_2367 = CDR(((obj_t) BgL_l1458z00_2360));
											{
												obj_t BgL_h1459z00_6061;
												obj_t BgL_l1458z00_6060;

												BgL_l1458z00_6060 = BgL_arg1739z00_2367;
												BgL_h1459z00_6061 = BgL_nh1460z00_2366;
												BgL_h1459z00_2361 = BgL_h1459z00_6061;
												BgL_l1458z00_2360 = BgL_l1458z00_6060;
												goto BgL_zc3z04anonymousza31735ze3z87_2362;
											}
										}
									}
								else
									{	/* SawBbv/bbv-liveness.scm 124 */
										obj_t BgL_arg1746z00_2369;

										BgL_arg1746z00_2369 = CDR(((obj_t) BgL_l1458z00_2360));
										{
											obj_t BgL_l1458z00_6064;

											BgL_l1458z00_6064 = BgL_arg1746z00_2369;
											BgL_l1458z00_2360 = BgL_l1458z00_6064;
											goto BgL_zc3z04anonymousza31735ze3z87_2362;
										}
									}
							}
					}
				}
			}
		}

	}



/* args-widen-bbv!~0 */
	bool_t BGl_argszd2widenzd2bbvz12ze70zf5zzsaw_bbvzd2livenesszd2(obj_t
		BgL_regsz00_3993, obj_t BgL_oz00_2372)
	{
		{	/* SawBbv/bbv-liveness.scm 135 */
			{	/* SawBbv/bbv-liveness.scm 127 */
				bool_t BgL_test2221z00_6065;

				{	/* SawBbv/bbv-liveness.scm 127 */
					obj_t BgL_classz00_3458;

					BgL_classz00_3458 = BGl_rtl_insz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_oz00_2372))
						{	/* SawBbv/bbv-liveness.scm 127 */
							BgL_objectz00_bglt BgL_arg1807z00_3460;

							BgL_arg1807z00_3460 = (BgL_objectz00_bglt) (BgL_oz00_2372);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawBbv/bbv-liveness.scm 127 */
									long BgL_idxz00_3466;

									BgL_idxz00_3466 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3460);
									BgL_test2221z00_6065 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3466 + 1L)) == BgL_classz00_3458);
								}
							else
								{	/* SawBbv/bbv-liveness.scm 127 */
									bool_t BgL_res1988z00_3491;

									{	/* SawBbv/bbv-liveness.scm 127 */
										obj_t BgL_oclassz00_3474;

										{	/* SawBbv/bbv-liveness.scm 127 */
											obj_t BgL_arg1815z00_3482;
											long BgL_arg1816z00_3483;

											BgL_arg1815z00_3482 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawBbv/bbv-liveness.scm 127 */
												long BgL_arg1817z00_3484;

												BgL_arg1817z00_3484 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3460);
												BgL_arg1816z00_3483 =
													(BgL_arg1817z00_3484 - OBJECT_TYPE);
											}
											BgL_oclassz00_3474 =
												VECTOR_REF(BgL_arg1815z00_3482, BgL_arg1816z00_3483);
										}
										{	/* SawBbv/bbv-liveness.scm 127 */
											bool_t BgL__ortest_1115z00_3475;

											BgL__ortest_1115z00_3475 =
												(BgL_classz00_3458 == BgL_oclassz00_3474);
											if (BgL__ortest_1115z00_3475)
												{	/* SawBbv/bbv-liveness.scm 127 */
													BgL_res1988z00_3491 = BgL__ortest_1115z00_3475;
												}
											else
												{	/* SawBbv/bbv-liveness.scm 127 */
													long BgL_odepthz00_3476;

													{	/* SawBbv/bbv-liveness.scm 127 */
														obj_t BgL_arg1804z00_3477;

														BgL_arg1804z00_3477 = (BgL_oclassz00_3474);
														BgL_odepthz00_3476 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3477);
													}
													if ((1L < BgL_odepthz00_3476))
														{	/* SawBbv/bbv-liveness.scm 127 */
															obj_t BgL_arg1802z00_3479;

															{	/* SawBbv/bbv-liveness.scm 127 */
																obj_t BgL_arg1803z00_3480;

																BgL_arg1803z00_3480 = (BgL_oclassz00_3474);
																BgL_arg1802z00_3479 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3480,
																	1L);
															}
															BgL_res1988z00_3491 =
																(BgL_arg1802z00_3479 == BgL_classz00_3458);
														}
													else
														{	/* SawBbv/bbv-liveness.scm 127 */
															BgL_res1988z00_3491 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2221z00_6065 = BgL_res1988z00_3491;
								}
						}
					else
						{	/* SawBbv/bbv-liveness.scm 127 */
							BgL_test2221z00_6065 = ((bool_t) 0);
						}
				}
				if (BgL_test2221z00_6065)
					{	/* SawBbv/bbv-liveness.scm 127 */
						{	/* SawBbv/bbv-liveness.scm 129 */
							BgL_rtl_inszf2bbvzf2_bglt BgL_wide1201z00_2378;

							BgL_wide1201z00_2378 =
								((BgL_rtl_inszf2bbvzf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_inszf2bbvzf2_bgl))));
							{	/* SawBbv/bbv-liveness.scm 129 */
								obj_t BgL_auxz00_6093;
								BgL_objectz00_bglt BgL_tmpz00_6089;

								BgL_auxz00_6093 = ((obj_t) BgL_wide1201z00_2378);
								BgL_tmpz00_6089 =
									((BgL_objectz00_bglt)
									((BgL_rtl_insz00_bglt)
										((BgL_rtl_insz00_bglt) BgL_oz00_2372)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6089, BgL_auxz00_6093);
							}
							((BgL_objectz00_bglt)
								((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_2372)));
							{	/* SawBbv/bbv-liveness.scm 129 */
								long BgL_arg1751z00_2379;

								BgL_arg1751z00_2379 =
									BGL_CLASS_NUM(BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt)
											((BgL_rtl_insz00_bglt) BgL_oz00_2372))),
									BgL_arg1751z00_2379);
							}
							((BgL_rtl_insz00_bglt)
								((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_2372)));
						}
						{
							BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_6107;

							{
								obj_t BgL_auxz00_6108;

								{	/* SawBbv/bbv-liveness.scm 130 */
									BgL_objectz00_bglt BgL_tmpz00_6109;

									BgL_tmpz00_6109 =
										((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt)
											((BgL_rtl_insz00_bglt) BgL_oz00_2372)));
									BgL_auxz00_6108 = BGL_OBJECT_WIDENING(BgL_tmpz00_6109);
								}
								BgL_auxz00_6107 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_6108);
							}
							((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_6107))->
									BgL_defz00) =
								((obj_t) ((obj_t)
										BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00
										(BgL_regsz00_3993))), BUNSPEC);
						}
						{
							obj_t BgL_auxz00_6126;
							BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_6118;

							if (CBOOL(
									(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_oz00_2372)))->BgL_destz00)))
								{	/* SawBbv/bbv-liveness.scm 133 */
									obj_t BgL_arg1753z00_2382;

									{	/* SawBbv/bbv-liveness.scm 133 */
										obj_t BgL_arg1754z00_2383;

										BgL_arg1754z00_2383 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_oz00_2372)))->BgL_destz00);
										{	/* SawBbv/bbv-liveness.scm 133 */
											obj_t BgL_list1755z00_2384;

											BgL_list1755z00_2384 =
												MAKE_YOUNG_PAIR(BgL_arg1754z00_2383, BNIL);
											BgL_arg1753z00_2382 = BgL_list1755z00_2384;
										}
									}
									BgL_auxz00_6126 =
										((obj_t)
										BGl_listzd2ze3regsetz31zzsaw_regsetz00(BgL_arg1753z00_2382,
											BgL_regsz00_3993));
								}
							else
								{	/* SawBbv/bbv-liveness.scm 132 */
									BgL_auxz00_6126 =
										((obj_t)
										BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00
										(BgL_regsz00_3993));
								}
							{
								obj_t BgL_auxz00_6119;

								{	/* SawBbv/bbv-liveness.scm 132 */
									BgL_objectz00_bglt BgL_tmpz00_6120;

									BgL_tmpz00_6120 =
										((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt)
											((BgL_rtl_insz00_bglt) BgL_oz00_2372)));
									BgL_auxz00_6119 = BGL_OBJECT_WIDENING(BgL_tmpz00_6120);
								}
								BgL_auxz00_6118 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_6119);
							}
							((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_6118))->
									BgL_outz00) = ((obj_t) BgL_auxz00_6126), BUNSPEC);
						}
						{
							obj_t BgL_auxz00_6147;
							BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_6139;

							{	/* SawBbv/bbv-liveness.scm 131 */
								obj_t BgL_arg1761z00_2385;

								BgL_arg1761z00_2385 =
									BGl_getzd2argsze70z35zzsaw_bbvzd2livenesszd2(BgL_oz00_2372);
								BgL_auxz00_6147 =
									((obj_t)
									BGl_listzd2ze3regsetz31zzsaw_regsetz00(BgL_arg1761z00_2385,
										BgL_regsz00_3993));
							}
							{
								obj_t BgL_auxz00_6140;

								{	/* SawBbv/bbv-liveness.scm 131 */
									BgL_objectz00_bglt BgL_tmpz00_6141;

									BgL_tmpz00_6141 =
										((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt)
											((BgL_rtl_insz00_bglt) BgL_oz00_2372)));
									BgL_auxz00_6140 = BGL_OBJECT_WIDENING(BgL_tmpz00_6141);
								}
								BgL_auxz00_6139 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_6140);
							}
							((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_6139))->
									BgL_inz00) = ((obj_t) BgL_auxz00_6147), BUNSPEC);
						}
						{
							BgL_bbvzd2ctxzd2_bglt BgL_auxz00_6160;
							BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_6152;

							{	/* SawBbv/bbv-liveness.scm 131 */
								obj_t BgL_arg1762z00_2386;

								{	/* SawBbv/bbv-liveness.scm 131 */
									obj_t BgL_arg1765z00_2387;

									{	/* SawBbv/bbv-liveness.scm 131 */
										obj_t BgL_classz00_3501;

										BgL_classz00_3501 =
											BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2;
										BgL_arg1765z00_2387 =
											BGL_CLASS_ALL_FIELDS(BgL_classz00_3501);
									}
									BgL_arg1762z00_2386 = VECTOR_REF(BgL_arg1765z00_2387, 8L);
								}
								BgL_auxz00_6160 =
									((BgL_bbvzd2ctxzd2_bglt)
									BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
									(BgL_arg1762z00_2386));
							}
							{
								obj_t BgL_auxz00_6153;

								{	/* SawBbv/bbv-liveness.scm 131 */
									BgL_objectz00_bglt BgL_tmpz00_6154;

									BgL_tmpz00_6154 =
										((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt)
											((BgL_rtl_insz00_bglt) BgL_oz00_2372)));
									BgL_auxz00_6153 = BGL_OBJECT_WIDENING(BgL_tmpz00_6154);
								}
								BgL_auxz00_6152 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_6153);
							}
							((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_6152))->
									BgL_ctxz00) =
								((BgL_bbvzd2ctxzd2_bglt) BgL_auxz00_6160), BUNSPEC);
						}
						{
							BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_6166;

							{
								obj_t BgL_auxz00_6167;

								{	/* SawBbv/bbv-liveness.scm 131 */
									BgL_objectz00_bglt BgL_tmpz00_6168;

									BgL_tmpz00_6168 =
										((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt)
											((BgL_rtl_insz00_bglt) BgL_oz00_2372)));
									BgL_auxz00_6167 = BGL_OBJECT_WIDENING(BgL_tmpz00_6168);
								}
								BgL_auxz00_6166 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_6167);
							}
							((((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_6166))->
									BgL_z52hashz52) = ((obj_t) BFALSE), BUNSPEC);
						}
						((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_2372));
						{	/* SawBbv/bbv-liveness.scm 135 */
							obj_t BgL_g1465z00_2388;

							BgL_g1465z00_2388 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_oz00_2372)))->BgL_argsz00);
							{
								obj_t BgL_l1463z00_2390;

								BgL_l1463z00_2390 = BgL_g1465z00_2388;
							BgL_zc3z04anonymousza31766ze3z87_2391:
								if (PAIRP(BgL_l1463z00_2390))
									{	/* SawBbv/bbv-liveness.scm 135 */
										BGl_argszd2widenzd2bbvz12ze70zf5zzsaw_bbvzd2livenesszd2
											(BgL_regsz00_3993, CAR(BgL_l1463z00_2390));
										{
											obj_t BgL_l1463z00_6183;

											BgL_l1463z00_6183 = CDR(BgL_l1463z00_2390);
											BgL_l1463z00_2390 = BgL_l1463z00_6183;
											goto BgL_zc3z04anonymousza31766ze3z87_2391;
										}
									}
								else
									{	/* SawBbv/bbv-liveness.scm 135 */
										return ((bool_t) 1);
									}
							}
						}
					}
				else
					{	/* SawBbv/bbv-liveness.scm 127 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_bbvzd2livenesszd2(void)
	{
		{	/* SawBbv/bbv-liveness.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_bbvzd2livenesszd2(void)
	{
		{	/* SawBbv/bbv-liveness.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_bbvzd2livenesszd2(void)
	{
		{	/* SawBbv/bbv-liveness.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2livenesszd2(void)
	{
		{	/* SawBbv/bbv-liveness.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zzsaw_regsetz00(358079690L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(237915200L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(425659118L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(242097300L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2utilszd2(221709922L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(481635416L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2mergezd2(376575453L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(288263219L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
			return
				BGl_modulezd2initializa7ationz75zzsaw_bbvzd2costzd2(414627252L,
				BSTRING_TO_STRING(BGl_string1994z00zzsaw_bbvzd2livenesszd2));
		}

	}

#ifdef __cplusplus
}
#endif
