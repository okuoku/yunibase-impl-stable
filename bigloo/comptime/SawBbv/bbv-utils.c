/*===========================================================================*/
/*   (SawBbv/bbv-utils.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawBbv/bbv-utils.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_SAW_BBVzd2UTILSzd2_TYPE_DEFINITIONS
#define BGL_BgL_SAW_BBVzd2UTILSzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_patternsz00;
		obj_t BgL_labelsz00;
	}                    *BgL_rtl_switchz00_bglt;

	typedef struct BgL_rtl_ifeqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_thenz00;
	}                  *BgL_rtl_ifeqz00_bglt;

	typedef struct BgL_rtl_ifnez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_thenz00;
	}                  *BgL_rtl_ifnez00_bglt;

	typedef struct BgL_rtl_goz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_toz00;
	}                *BgL_rtl_goz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_rtl_regzf2razf2_bgl
	{
		int BgL_numz00;
		obj_t BgL_colorz00;
		obj_t BgL_coalescez00;
		int BgL_occurrencesz00;
		obj_t BgL_interferez00;
		obj_t BgL_interfere2z00;
	}                      *BgL_rtl_regzf2razf2_bglt;

	typedef struct BgL_regsetz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_lengthz00;
		int BgL_msiza7eza7;
		obj_t BgL_regvz00;
		obj_t BgL_reglz00;
		obj_t BgL_stringz00;
	}                *BgL_regsetz00_bglt;

	typedef struct BgL_rtl_inszf2bbvzf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_outz00;
		obj_t BgL_inz00;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		obj_t BgL_z52hashz52;
	}                       *BgL_rtl_inszf2bbvzf2_bglt;

	typedef struct BgL_blockvz00_bgl
	{
		obj_t BgL_versionsz00;
		obj_t BgL_genericz00;
		long BgL_z52markz52;
		obj_t BgL_mergez00;
	}                *BgL_blockvz00_bglt;

	typedef struct BgL_blocksz00_bgl
	{
		long BgL_z52markz52;
		obj_t BgL_z52hashz52;
		obj_t BgL_z52blacklistz52;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		struct BgL_blockz00_bgl *BgL_parentz00;
		long BgL_gccntz00;
		long BgL_gcmarkz00;
		obj_t BgL_mblockz00;
		obj_t BgL_creatorz00;
		obj_t BgL_mergesz00;
		bool_t BgL_asleepz00;
	}                *BgL_blocksz00_bglt;

	typedef struct BgL_bbvzd2ctxzd2_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_idz00;
		obj_t BgL_entriesz00;
	}                   *BgL_bbvzd2ctxzd2_bglt;

	typedef struct BgL_bbvzd2ctxentryzd2_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_rtl_regz00_bgl *BgL_regz00;
		obj_t BgL_typesz00;
		bool_t BgL_polarityz00;
		long BgL_countz00;
		obj_t BgL_valuez00;
		obj_t BgL_aliasesz00;
		obj_t BgL_initvalz00;
	}                        *BgL_bbvzd2ctxentryzd2_bglt;

	typedef struct BgL_bbvzd2rangezd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_loz00;
		obj_t BgL_upz00;
	}                     *BgL_bbvzd2rangezd2_bglt;


#endif													// BGL_BgL_SAW_BBVzd2UTILSzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2utilszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2utilszd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2utilszd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2;
	extern obj_t BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt,
		BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static BgL_bbvzd2ctxzd2_bglt
		BGl_z62bbvzd2ctxzd2filterzd2livezd2inzd2regszb0zzsaw_bbvzd2utilszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_bbvzd2utilszd2 = BUNSPEC;
	extern obj_t BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt);
	extern bool_t
		BGl_rtl_inszd2ifeqzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	extern obj_t BGl_za2pairzd2nilza2zd2zztype_cachez00;
	static obj_t BGl_z62zc3z04anonymousza31864ze3ze5zzsaw_bbvzd2utilszd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2utilszd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_bbvzd2gczd2redirectz12z12zzsaw_bbvzd2gczd2(BgL_blockz00_bglt,
		BgL_blockz00_bglt);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2utilszd2(int, int, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2utilszd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2utilszd2(BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, int, obj_t, obj_t, int, obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2utilszd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2utilszd2(void);
	static obj_t BGl_z62zc3z04anonymousza31971ze3ze5zzsaw_bbvzd2utilszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31948ze3ze5zzsaw_bbvzd2utilszd2(obj_t,
		obj_t);
	extern obj_t
		BGl_rtl_inszd2switchzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_genericzd2initzd2zzsaw_bbvzd2utilszd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2utilszd2(obj_t);
	BGL_IMPORT obj_t BGl_filterzd2mapzd2zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31981ze3ze5zzsaw_bbvzd2utilszd2(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzsaw_bbvzd2utilszd2(void);
	static obj_t BGl_z62zc3zd3tyz72zzsaw_bbvzd2utilszd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt,
		obj_t);
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	extern bool_t
		BGl_rtl_inszd2ifnezf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	extern obj_t BGl_regsetz00zzsaw_regsetz00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	static BgL_bbvzd2ctxzd2_bglt
		BGl_z62bbvzd2ctxzd2extendzd2livezd2outzd2regszb0zzsaw_bbvzd2utilszd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2brealza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t BGl_zc3zd3tyz10zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	static obj_t BGl_z62blockzd2ze3blockzd2listz81zzsaw_bbvzd2utilszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl__replacezd2blockz12zc0zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2;
	extern obj_t BGl_bbvzd2gczd2connectz12z12zzsaw_bbvzd2gczd2(BgL_blockz00_bglt,
		BgL_blockz00_bglt);
	extern obj_t BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t BGl_z62listzd2replacezb0zzsaw_bbvzd2utilszd2(obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2utilszd2(obj_t,
		obj_t);
	static obj_t BGl_appendzd221011zd2zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_bbvzd2utilszd2(void);
	static obj_t BGl_bbsetzd2conszd2zzsaw_bbvzd2utilszd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2utilszd2(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2utilszd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2utilszd2(void);
	static BgL_typez00_bglt
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	extern obj_t BGl_za2realza2z00zztype_cachez00;
	extern obj_t
		BGl_blockzd2predszd2updatez12z12zzsaw_bbvzd2typeszd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt);
	extern bool_t BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2utilszd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2utilszd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_replacezd2blockz12zc0zzsaw_bbvzd2utilszd2(BgL_blockz00_bglt,
		BgL_blockz00_bglt, obj_t);
	extern obj_t BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t BGl_z62setzd2maxzd2labelz12z70zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2utilszd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2utilszd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62genlabelz62zzsaw_bbvzd2utilszd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2utilszd2(BgL_regsetz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2constructorzd2zz__objectz00(obj_t);
	BGL_IMPORT obj_t bgl_remq_bang(obj_t, obj_t);
	extern BgL_bbvzd2ctxzd2_bglt
		BGl_extendzd2ctxz12zc0zzsaw_bbvzd2typeszd2(BgL_bbvzd2ctxzd2_bglt,
		BgL_rtl_regz00_bglt, obj_t, bool_t, obj_t, obj_t);
	extern obj_t BGl_getzd2bbzd2markz00zzsaw_bbvzd2typeszd2(void);
	static obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2utilszd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2gczd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2debugzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2specializa7ez75(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2utilszd2(obj_t);
	BGL_IMPORT obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2utilszd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static long BGl_za2labelza2z00zzsaw_bbvzd2utilszd2 = 0L;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2utilszd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2utilszd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2utilszd2(void);
	static bool_t BGl_ctxze3zd3zf3zc3zzsaw_bbvzd2utilszd2(BgL_bbvzd2ctxzd2_bglt,
		BgL_bbvzd2ctxzd2_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2utilszd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2maxzd2labelz12z12zzsaw_bbvzd2utilszd2(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2utilszd2(void);
	extern obj_t BGl_regsetzd2forzd2eachz00zzsaw_regsetz00(obj_t,
		BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2utilszd2
		(BgL_rtl_regz00_bglt, int);
	static obj_t BGl_makezd2emptyzd2bbsetz00zzsaw_bbvzd2utilszd2(void);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2utilszd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_filterz12z12zz__r4_control_features_6_9z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_bbvzd2ctxzd2filterzd2livezd2inzd2regszd2zzsaw_bbvzd2utilszd2
		(BgL_bbvzd2ctxzd2_bglt, BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_regsetzf3zf3zzsaw_bbvzd2utilszd2(obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2utilszd2(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2utilszd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2utilszd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_bbvzd2ctxzd2_bglt
		BGl_bbvzd2ctxzd2extendzd2livezd2outzd2regszd2zzsaw_bbvzd2utilszd2
		(BgL_bbvzd2ctxzd2_bglt, BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2utilszd2(BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_genlabelz00zzsaw_bbvzd2utilszd2(void);
	static obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2utilszd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2bintza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_redirectzd2blockz12zc0zzsaw_bbvzd2utilszd2(BgL_blockz00_bglt,
		BgL_blockz00_bglt, BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2utilszd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_z62redirectzd2blockz12za2zzsaw_bbvzd2utilszd2(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2utilszd2
		(BgL_rtl_regz00_bglt, obj_t);
	extern obj_t BGl_iszd2subtypezf3z21zztype_typeofz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2utilszd2(void);
	BGL_EXPORTED_DECL obj_t BGl_listzd2replacezd2zzsaw_bbvzd2utilszd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_loopze70ze7zzsaw_bbvzd2utilszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2utilszd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	extern obj_t BGl_rtl_regzf2razf2zzsaw_regsetz00;
	static obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2utilszd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2utilszd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockzd2ze3blockzd2listze3zzsaw_bbvzd2utilszd2(obj_t,
		BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2utilszd2(BgL_regsetz00_bglt);
	BGL_IMPORT obj_t
		BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2utilszd2(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2utilszd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt);
	BGL_IMPORT bool_t BGl_2ze3zd3z30zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2utilszd2(BgL_regsetz00_bglt,
		int);
	static obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2utilszd2(obj_t,
		obj_t);
	static obj_t __cnst[4];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_zc3zd3tyzd2envzc2zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762za7c3za7d3tyza772za7za72163z00,
		BGl_z62zc3zd3tyz72zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2namezd2envzf2zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72164za7,
		BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_genlabelzd2envzd2zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762genlabelza762za7za72165z00,
		BGl_z62genlabelz62zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2145z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2145za700za7za7s2166za7, "replace-block!.old>", 19);
	      DEFINE_STRING(BGl_string2146z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2146za700za7za7s2167za7, "replace-block!.new>", 19);
	      DEFINE_STRING(BGl_string2147z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2147za700za7za7s2168za7, " -> ", 4);
	      DEFINE_STRING(BGl_string2148z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2148za700za7za7s2169za7, "REPLACE-BLOCK-ERROR! ", 21);
	      DEFINE_STRING(BGl_string2149z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2149za700za7za7s2170za7, ":", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_replacezd2blockz12zd2envz12zzsaw_bbvzd2utilszd2,
		BgL_bgl__replaceza7d2block2171za7, opt_generic_entry,
		BGl__replacezd2blockz12zc0zzsaw_bbvzd2utilszd2, BFALSE, -1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2nilzd2envz00zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762regsetza7d2nilza72172za7,
		BGl_z62regsetzd2nilzb0zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2150z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2150za700za7za7s2173za7, ",", 1);
	      DEFINE_STRING(BGl_string2151z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2151za700za7za7s2174za7, "SawBbv/bbv-utils.scm", 20);
	      DEFINE_STRING(BGl_string2152z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2152za700za7za7s2175za7, " octx=", 6);
	      DEFINE_STRING(BGl_string2153z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2153za700za7za7s2176za7, " nctx=", 6);
	      DEFINE_STRING(BGl_string2154z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2154za700za7za7s2177za7, "Wrong block replacement ~a", 26);
	      DEFINE_STRING(BGl_string2155z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2155za700za7za7s2178za7, "replace-block!", 14);
	      DEFINE_STRING(BGl_string2156z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2156za700za7za7s2179za7, "replace-block!.old<", 19);
	      DEFINE_STRING(BGl_string2157z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2157za700za7za7s2180za7, "replace-block!.new<", 19);
	      DEFINE_STRING(BGl_string2158z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2158za700za7za7s2181za7, " ", 1);
	      DEFINE_STRING(BGl_string2159z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2159za700za7za7s2182za7, "ERROR ctx>=? ", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2setz12zd2envz32zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72183za7,
		BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2160za700za7za7s2184za7, "saw_bbv-utils", 13);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockzd2ze3blockzd2listzd2envz31zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762blockza7d2za7e3bl2185za7,
		BGl_z62blockzd2ze3blockzd2listz81zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2161z00zzsaw_bbvzd2utilszd2,
		BgL_bgl_string2161za700za7za7s2186za7, "_ :debug number bbset ", 22);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2envzf2zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72187za7,
		BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2extendzd2livezd2outzd2regszd2envz00zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762bbvza7d2ctxza7d2e2188za7,
		BGl_z62bbvzd2ctxzd2extendzd2livezd2outzd2regszb0zzsaw_bbvzd2utilszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_redirectzd2blockz12zd2envz12zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762redirectza7d2bl2189z00,
		BGl_z62redirectzd2blockz12za2zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2setz12zd2envzc1zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72190za7,
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2utilszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2setz12zd2envz32zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72191za7,
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2utilszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2envzf2zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72192za7,
		BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2setz12zd2envz32zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72193za7,
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2utilszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2regvzd2envz00zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762regsetza7d2regv2194z00,
		BGl_z62regsetzd2regvzb0zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2maxzd2labelz12zd2envzc0zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762setza7d2maxza7d2l2195za7,
		BGl_z62setzd2maxzd2labelz12z70zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2setz12zd2envzc0zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762regsetza7d2leng2196z00,
		BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2msiza7ezd2envza7zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762regsetza7d2msiza72197za7,
		BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2envzf2zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72198za7,
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2setz12zd2envz32zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72199za7,
		BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2envzf2zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72200za7,
		BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	BGL_IMPORT obj_t BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2nilzd2envzf2zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72201za7,
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2replacezd2envz00zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762listza7d2replac2202z00,
		BGl_z62listzd2replacezb0zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2envzf2zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72203za7,
		BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2numzd2envzf2zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72204za7,
		BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2setz12zd2envzc0zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762regsetza7d2stri2205z00,
		BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2setz12zd2envz32zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72206za7,
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2utilszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2envz01zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72207za7,
		BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2envz00zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762regsetza7d2stri2208z00,
		BGl_z62regsetzd2stringzb0zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2setz12zd2envz32zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72209za7,
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2utilszd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2rtl_regzf2razd2envzf2zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762makeza7d2rtl_re2210z00,
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razf3zd2envzd3zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72211za7,
		BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2envz00zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762regsetza7d2leng2212z00,
		BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2envzf2zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72213za7,
		BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2envzf2zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72214za7,
		BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2hardwarezd2envzf2zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72215za7,
		BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2regsetzd2envz00zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762makeza7d2regset2216z00,
		BGl_z62makezd2regsetzb0zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bbvzd2ctxzd2filterzd2livezd2inzd2regszd2envz00zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762bbvza7d2ctxza7d2f2217za7,
		BGl_z62bbvzd2ctxzd2filterzd2livezd2inzd2regszb0zzsaw_bbvzd2utilszd2, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzf3zd2envz21zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762regsetza7f3za791za72218z00,
		BGl_z62regsetzf3z91zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2keyzd2envzf2zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72219za7,
		BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2setz12zd2envz32zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762rtl_regza7f2raza72220za7,
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2utilszd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2reglzd2envz00zzsaw_bbvzd2utilszd2,
		BgL_bgl_za762regsetza7d2regl2221z00,
		BGl_z62regsetzd2reglzb0zzsaw_bbvzd2utilszd2, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_bbvzd2utilszd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2utilszd2(long
		BgL_checksumz00_3856, char *BgL_fromz00_3857)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_bbvzd2utilszd2))
				{
					BGl_requirezd2initializa7ationz75zzsaw_bbvzd2utilszd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_bbvzd2utilszd2();
					BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2utilszd2();
					BGl_cnstzd2initzd2zzsaw_bbvzd2utilszd2();
					BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2utilszd2();
					return BGl_toplevelzd2initzd2zzsaw_bbvzd2utilszd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2utilszd2(void)
	{
		{	/* SawBbv/bbv-utils.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_bbv-utils");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"saw_bbv-utils");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"saw_bbv-utils");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_bbv-utils");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_bbv-utils");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_bbv-utils");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_bbv-utils");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_bbv-utils");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"saw_bbv-utils");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_bbv-utils");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"saw_bbv-utils");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_bbv-utils");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "saw_bbv-utils");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_bbv-utils");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_bbv-utils");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "saw_bbv-utils");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2utilszd2(void)
	{
		{	/* SawBbv/bbv-utils.scm 15 */
			{	/* SawBbv/bbv-utils.scm 15 */
				obj_t BgL_cportz00_3692;

				{	/* SawBbv/bbv-utils.scm 15 */
					obj_t BgL_stringz00_3699;

					BgL_stringz00_3699 = BGl_string2161z00zzsaw_bbvzd2utilszd2;
					{	/* SawBbv/bbv-utils.scm 15 */
						obj_t BgL_startz00_3700;

						BgL_startz00_3700 = BINT(0L);
						{	/* SawBbv/bbv-utils.scm 15 */
							obj_t BgL_endz00_3701;

							BgL_endz00_3701 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3699)));
							{	/* SawBbv/bbv-utils.scm 15 */

								BgL_cportz00_3692 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3699, BgL_startz00_3700, BgL_endz00_3701);
				}}}}
				{
					long BgL_iz00_3693;

					BgL_iz00_3693 = 3L;
				BgL_loopz00_3694:
					if ((BgL_iz00_3693 == -1L))
						{	/* SawBbv/bbv-utils.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* SawBbv/bbv-utils.scm 15 */
							{	/* SawBbv/bbv-utils.scm 15 */
								obj_t BgL_arg2162z00_3695;

								{	/* SawBbv/bbv-utils.scm 15 */

									{	/* SawBbv/bbv-utils.scm 15 */
										obj_t BgL_locationz00_3697;

										BgL_locationz00_3697 = BBOOL(((bool_t) 0));
										{	/* SawBbv/bbv-utils.scm 15 */

											BgL_arg2162z00_3695 =
												BGl_readz00zz__readerz00(BgL_cportz00_3692,
												BgL_locationz00_3697);
										}
									}
								}
								{	/* SawBbv/bbv-utils.scm 15 */
									int BgL_tmpz00_3891;

									BgL_tmpz00_3891 = (int) (BgL_iz00_3693);
									CNST_TABLE_SET(BgL_tmpz00_3891, BgL_arg2162z00_3695);
							}}
							{	/* SawBbv/bbv-utils.scm 15 */
								int BgL_auxz00_3698;

								BgL_auxz00_3698 = (int) ((BgL_iz00_3693 - 1L));
								{
									long BgL_iz00_3896;

									BgL_iz00_3896 = (long) (BgL_auxz00_3698);
									BgL_iz00_3693 = BgL_iz00_3896;
									goto BgL_loopz00_3694;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2utilszd2(void)
	{
		{	/* SawBbv/bbv-utils.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2utilszd2(void)
	{
		{	/* SawBbv/bbv-utils.scm 15 */
			return (BGl_za2labelza2z00zzsaw_bbvzd2utilszd2 = 0L, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzsaw_bbvzd2utilszd2(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_2167;

				BgL_headz00_2167 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2168;
					obj_t BgL_tailz00_2169;

					BgL_prevz00_2168 = BgL_headz00_2167;
					BgL_tailz00_2169 = BgL_l1z00_1;
				BgL_loopz00_2170:
					if (PAIRP(BgL_tailz00_2169))
						{
							obj_t BgL_newzd2prevzd2_2172;

							BgL_newzd2prevzd2_2172 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2169), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2168, BgL_newzd2prevzd2_2172);
							{
								obj_t BgL_tailz00_3906;
								obj_t BgL_prevz00_3905;

								BgL_prevz00_3905 = BgL_newzd2prevzd2_2172;
								BgL_tailz00_3906 = CDR(BgL_tailz00_2169);
								BgL_tailz00_2169 = BgL_tailz00_3906;
								BgL_prevz00_2168 = BgL_prevz00_3905;
								goto BgL_loopz00_2170;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_2167);
				}
			}
		}

	}



/* make-rtl_reg/ra */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2utilszd2(BgL_typez00_bglt
		BgL_type1179z00_3, obj_t BgL_var1180z00_4, obj_t BgL_onexprzf31181zf3_5,
		obj_t BgL_name1182z00_6, obj_t BgL_key1183z00_7,
		obj_t BgL_hardware1184z00_8, int BgL_num1185z00_9,
		obj_t BgL_color1186z00_10, obj_t BgL_coalesce1187z00_11,
		int BgL_occurrences1188z00_12, obj_t BgL_interfere1189z00_13,
		obj_t BgL_interfere21190z00_14)
	{
		{	/* SawMill/regset.sch 55 */
			{	/* SawMill/regset.sch 55 */
				BgL_rtl_regz00_bglt BgL_new1166z00_3703;

				{	/* SawMill/regset.sch 55 */
					BgL_rtl_regz00_bglt BgL_tmp1164z00_3704;
					BgL_rtl_regzf2razf2_bglt BgL_wide1165z00_3705;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_3909;

						{	/* SawMill/regset.sch 55 */
							BgL_rtl_regz00_bglt BgL_new1163z00_3706;

							BgL_new1163z00_3706 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/regset.sch 55 */
								long BgL_arg1544z00_3707;

								BgL_arg1544z00_3707 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1163z00_3706),
									BgL_arg1544z00_3707);
							}
							{	/* SawMill/regset.sch 55 */
								BgL_objectz00_bglt BgL_tmpz00_3914;

								BgL_tmpz00_3914 = ((BgL_objectz00_bglt) BgL_new1163z00_3706);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3914, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1163z00_3706);
							BgL_auxz00_3909 = BgL_new1163z00_3706;
						}
						BgL_tmp1164z00_3704 = ((BgL_rtl_regz00_bglt) BgL_auxz00_3909);
					}
					BgL_wide1165z00_3705 =
						((BgL_rtl_regzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regzf2razf2_bgl))));
					{	/* SawMill/regset.sch 55 */
						obj_t BgL_auxz00_3922;
						BgL_objectz00_bglt BgL_tmpz00_3920;

						BgL_auxz00_3922 = ((obj_t) BgL_wide1165z00_3705);
						BgL_tmpz00_3920 = ((BgL_objectz00_bglt) BgL_tmp1164z00_3704);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3920, BgL_auxz00_3922);
					}
					((BgL_objectz00_bglt) BgL_tmp1164z00_3704);
					{	/* SawMill/regset.sch 55 */
						long BgL_arg1540z00_3708;

						BgL_arg1540z00_3708 =
							BGL_CLASS_NUM(BGl_rtl_regzf2razf2zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1164z00_3704), BgL_arg1540z00_3708);
					}
					BgL_new1166z00_3703 = ((BgL_rtl_regz00_bglt) BgL_tmp1164z00_3704);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1166z00_3703)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1179z00_3), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3703)))->BgL_varz00) =
					((obj_t) BgL_var1180z00_4), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3703)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31181zf3_5), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3703)))->BgL_namez00) =
					((obj_t) BgL_name1182z00_6), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3703)))->BgL_keyz00) =
					((obj_t) BgL_key1183z00_7), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3703)))->BgL_debugnamez00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3703)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1184z00_8), BUNSPEC);
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3944;

					{
						obj_t BgL_auxz00_3945;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3946;

							BgL_tmpz00_3946 = ((BgL_objectz00_bglt) BgL_new1166z00_3703);
							BgL_auxz00_3945 = BGL_OBJECT_WIDENING(BgL_tmpz00_3946);
						}
						BgL_auxz00_3944 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3945);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3944))->BgL_numz00) =
						((int) BgL_num1185z00_9), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3951;

					{
						obj_t BgL_auxz00_3952;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3953;

							BgL_tmpz00_3953 = ((BgL_objectz00_bglt) BgL_new1166z00_3703);
							BgL_auxz00_3952 = BGL_OBJECT_WIDENING(BgL_tmpz00_3953);
						}
						BgL_auxz00_3951 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3952);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3951))->
							BgL_colorz00) = ((obj_t) BgL_color1186z00_10), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3958;

					{
						obj_t BgL_auxz00_3959;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3960;

							BgL_tmpz00_3960 = ((BgL_objectz00_bglt) BgL_new1166z00_3703);
							BgL_auxz00_3959 = BGL_OBJECT_WIDENING(BgL_tmpz00_3960);
						}
						BgL_auxz00_3958 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3959);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3958))->
							BgL_coalescez00) = ((obj_t) BgL_coalesce1187z00_11), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3965;

					{
						obj_t BgL_auxz00_3966;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3967;

							BgL_tmpz00_3967 = ((BgL_objectz00_bglt) BgL_new1166z00_3703);
							BgL_auxz00_3966 = BGL_OBJECT_WIDENING(BgL_tmpz00_3967);
						}
						BgL_auxz00_3965 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3966);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3965))->
							BgL_occurrencesz00) = ((int) BgL_occurrences1188z00_12), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3972;

					{
						obj_t BgL_auxz00_3973;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3974;

							BgL_tmpz00_3974 = ((BgL_objectz00_bglt) BgL_new1166z00_3703);
							BgL_auxz00_3973 = BGL_OBJECT_WIDENING(BgL_tmpz00_3974);
						}
						BgL_auxz00_3972 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3973);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3972))->
							BgL_interferez00) = ((obj_t) BgL_interfere1189z00_13), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3979;

					{
						obj_t BgL_auxz00_3980;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3981;

							BgL_tmpz00_3981 = ((BgL_objectz00_bglt) BgL_new1166z00_3703);
							BgL_auxz00_3980 = BGL_OBJECT_WIDENING(BgL_tmpz00_3981);
						}
						BgL_auxz00_3979 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3980);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3979))->
							BgL_interfere2z00) = ((obj_t) BgL_interfere21190z00_14), BUNSPEC);
				}
				return BgL_new1166z00_3703;
			}
		}

	}



/* &make-rtl_reg/ra */
	BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3553, obj_t BgL_type1179z00_3554, obj_t BgL_var1180z00_3555,
		obj_t BgL_onexprzf31181zf3_3556, obj_t BgL_name1182z00_3557,
		obj_t BgL_key1183z00_3558, obj_t BgL_hardware1184z00_3559,
		obj_t BgL_num1185z00_3560, obj_t BgL_color1186z00_3561,
		obj_t BgL_coalesce1187z00_3562, obj_t BgL_occurrences1188z00_3563,
		obj_t BgL_interfere1189z00_3564, obj_t BgL_interfere21190z00_3565)
	{
		{	/* SawMill/regset.sch 55 */
			return
				BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2utilszd2(
				((BgL_typez00_bglt) BgL_type1179z00_3554), BgL_var1180z00_3555,
				BgL_onexprzf31181zf3_3556, BgL_name1182z00_3557, BgL_key1183z00_3558,
				BgL_hardware1184z00_3559, CINT(BgL_num1185z00_3560),
				BgL_color1186z00_3561, BgL_coalesce1187z00_3562,
				CINT(BgL_occurrences1188z00_3563), BgL_interfere1189z00_3564,
				BgL_interfere21190z00_3565);
		}

	}



/* rtl_reg/ra? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2utilszd2(obj_t
		BgL_objz00_15)
	{
		{	/* SawMill/regset.sch 56 */
			{	/* SawMill/regset.sch 56 */
				obj_t BgL_classz00_3709;

				BgL_classz00_3709 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_15))
					{	/* SawMill/regset.sch 56 */
						BgL_objectz00_bglt BgL_arg1807z00_3710;

						BgL_arg1807z00_3710 = (BgL_objectz00_bglt) (BgL_objz00_15);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 56 */
								long BgL_idxz00_3711;

								BgL_idxz00_3711 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3710);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3711 + 2L)) == BgL_classz00_3709);
							}
						else
							{	/* SawMill/regset.sch 56 */
								bool_t BgL_res2139z00_3714;

								{	/* SawMill/regset.sch 56 */
									obj_t BgL_oclassz00_3715;

									{	/* SawMill/regset.sch 56 */
										obj_t BgL_arg1815z00_3716;
										long BgL_arg1816z00_3717;

										BgL_arg1815z00_3716 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 56 */
											long BgL_arg1817z00_3718;

											BgL_arg1817z00_3718 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3710);
											BgL_arg1816z00_3717 = (BgL_arg1817z00_3718 - OBJECT_TYPE);
										}
										BgL_oclassz00_3715 =
											VECTOR_REF(BgL_arg1815z00_3716, BgL_arg1816z00_3717);
									}
									{	/* SawMill/regset.sch 56 */
										bool_t BgL__ortest_1115z00_3719;

										BgL__ortest_1115z00_3719 =
											(BgL_classz00_3709 == BgL_oclassz00_3715);
										if (BgL__ortest_1115z00_3719)
											{	/* SawMill/regset.sch 56 */
												BgL_res2139z00_3714 = BgL__ortest_1115z00_3719;
											}
										else
											{	/* SawMill/regset.sch 56 */
												long BgL_odepthz00_3720;

												{	/* SawMill/regset.sch 56 */
													obj_t BgL_arg1804z00_3721;

													BgL_arg1804z00_3721 = (BgL_oclassz00_3715);
													BgL_odepthz00_3720 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3721);
												}
												if ((2L < BgL_odepthz00_3720))
													{	/* SawMill/regset.sch 56 */
														obj_t BgL_arg1802z00_3722;

														{	/* SawMill/regset.sch 56 */
															obj_t BgL_arg1803z00_3723;

															BgL_arg1803z00_3723 = (BgL_oclassz00_3715);
															BgL_arg1802z00_3722 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3723,
																2L);
														}
														BgL_res2139z00_3714 =
															(BgL_arg1802z00_3722 == BgL_classz00_3709);
													}
												else
													{	/* SawMill/regset.sch 56 */
														BgL_res2139z00_3714 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2139z00_3714;
							}
					}
				else
					{	/* SawMill/regset.sch 56 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_reg/ra? */
	obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2utilszd2(obj_t BgL_envz00_3566,
		obj_t BgL_objz00_3567)
	{
		{	/* SawMill/regset.sch 56 */
			return BBOOL(BGl_rtl_regzf2razf3z01zzsaw_bbvzd2utilszd2(BgL_objz00_3567));
		}

	}



/* rtl_reg/ra-nil */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2utilszd2(void)
	{
		{	/* SawMill/regset.sch 57 */
			{	/* SawMill/regset.sch 57 */
				obj_t BgL_classz00_3049;

				BgL_classz00_3049 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				{	/* SawMill/regset.sch 57 */
					obj_t BgL__ortest_1117z00_3050;

					BgL__ortest_1117z00_3050 = BGL_CLASS_NIL(BgL_classz00_3049);
					if (CBOOL(BgL__ortest_1117z00_3050))
						{	/* SawMill/regset.sch 57 */
							return ((BgL_rtl_regz00_bglt) BgL__ortest_1117z00_3050);
						}
					else
						{	/* SawMill/regset.sch 57 */
							return
								((BgL_rtl_regz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3049));
						}
				}
			}
		}

	}



/* &rtl_reg/ra-nil */
	BgL_rtl_regz00_bglt BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3568)
	{
		{	/* SawMill/regset.sch 57 */
			return BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2utilszd2();
		}

	}



/* rtl_reg/ra-interfere2 */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt
		BgL_oz00_16)
	{
		{	/* SawMill/regset.sch 58 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4021;

				{
					obj_t BgL_auxz00_4022;

					{	/* SawMill/regset.sch 58 */
						BgL_objectz00_bglt BgL_tmpz00_4023;

						BgL_tmpz00_4023 = ((BgL_objectz00_bglt) BgL_oz00_16);
						BgL_auxz00_4022 = BGL_OBJECT_WIDENING(BgL_tmpz00_4023);
					}
					BgL_auxz00_4021 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4022);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4021))->
					BgL_interfere2z00);
			}
		}

	}



/* &rtl_reg/ra-interfere2 */
	obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3569, obj_t BgL_oz00_3570)
	{
		{	/* SawMill/regset.sch 58 */
			return
				BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3570));
		}

	}



/* rtl_reg/ra-interfere2-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2utilszd2
		(BgL_rtl_regz00_bglt BgL_oz00_17, obj_t BgL_vz00_18)
	{
		{	/* SawMill/regset.sch 59 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4030;

				{
					obj_t BgL_auxz00_4031;

					{	/* SawMill/regset.sch 59 */
						BgL_objectz00_bglt BgL_tmpz00_4032;

						BgL_tmpz00_4032 = ((BgL_objectz00_bglt) BgL_oz00_17);
						BgL_auxz00_4031 = BGL_OBJECT_WIDENING(BgL_tmpz00_4032);
					}
					BgL_auxz00_4030 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4031);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4030))->
						BgL_interfere2z00) = ((obj_t) BgL_vz00_18), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere2-set! */
	obj_t BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3571, obj_t BgL_oz00_3572, obj_t BgL_vz00_3573)
	{
		{	/* SawMill/regset.sch 59 */
			return
				BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3572), BgL_vz00_3573);
		}

	}



/* rtl_reg/ra-interfere */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt
		BgL_oz00_19)
	{
		{	/* SawMill/regset.sch 60 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4039;

				{
					obj_t BgL_auxz00_4040;

					{	/* SawMill/regset.sch 60 */
						BgL_objectz00_bglt BgL_tmpz00_4041;

						BgL_tmpz00_4041 = ((BgL_objectz00_bglt) BgL_oz00_19);
						BgL_auxz00_4040 = BGL_OBJECT_WIDENING(BgL_tmpz00_4041);
					}
					BgL_auxz00_4039 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4040);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4039))->
					BgL_interferez00);
			}
		}

	}



/* &rtl_reg/ra-interfere */
	obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3574, obj_t BgL_oz00_3575)
	{
		{	/* SawMill/regset.sch 60 */
			return
				BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3575));
		}

	}



/* rtl_reg/ra-interfere-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2utilszd2
		(BgL_rtl_regz00_bglt BgL_oz00_20, obj_t BgL_vz00_21)
	{
		{	/* SawMill/regset.sch 61 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4048;

				{
					obj_t BgL_auxz00_4049;

					{	/* SawMill/regset.sch 61 */
						BgL_objectz00_bglt BgL_tmpz00_4050;

						BgL_tmpz00_4050 = ((BgL_objectz00_bglt) BgL_oz00_20);
						BgL_auxz00_4049 = BGL_OBJECT_WIDENING(BgL_tmpz00_4050);
					}
					BgL_auxz00_4048 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4049);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4048))->
						BgL_interferez00) = ((obj_t) BgL_vz00_21), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere-set! */
	obj_t BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3576, obj_t BgL_oz00_3577, obj_t BgL_vz00_3578)
	{
		{	/* SawMill/regset.sch 61 */
			return
				BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3577), BgL_vz00_3578);
		}

	}



/* rtl_reg/ra-occurrences */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt
		BgL_oz00_22)
	{
		{	/* SawMill/regset.sch 62 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4057;

				{
					obj_t BgL_auxz00_4058;

					{	/* SawMill/regset.sch 62 */
						BgL_objectz00_bglt BgL_tmpz00_4059;

						BgL_tmpz00_4059 = ((BgL_objectz00_bglt) BgL_oz00_22);
						BgL_auxz00_4058 = BGL_OBJECT_WIDENING(BgL_tmpz00_4059);
					}
					BgL_auxz00_4057 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4058);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4057))->
					BgL_occurrencesz00);
			}
		}

	}



/* &rtl_reg/ra-occurrences */
	obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3579, obj_t BgL_oz00_3580)
	{
		{	/* SawMill/regset.sch 62 */
			return
				BINT(BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2utilszd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_3580)));
		}

	}



/* rtl_reg/ra-occurrences-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2utilszd2
		(BgL_rtl_regz00_bglt BgL_oz00_23, int BgL_vz00_24)
	{
		{	/* SawMill/regset.sch 63 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4067;

				{
					obj_t BgL_auxz00_4068;

					{	/* SawMill/regset.sch 63 */
						BgL_objectz00_bglt BgL_tmpz00_4069;

						BgL_tmpz00_4069 = ((BgL_objectz00_bglt) BgL_oz00_23);
						BgL_auxz00_4068 = BGL_OBJECT_WIDENING(BgL_tmpz00_4069);
					}
					BgL_auxz00_4067 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4068);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4067))->
						BgL_occurrencesz00) = ((int) BgL_vz00_24), BUNSPEC);
		}}

	}



/* &rtl_reg/ra-occurrences-set! */
	obj_t BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3581, obj_t BgL_oz00_3582, obj_t BgL_vz00_3583)
	{
		{	/* SawMill/regset.sch 63 */
			return
				BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3582), CINT(BgL_vz00_3583));
		}

	}



/* rtl_reg/ra-coalesce */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt
		BgL_oz00_25)
	{
		{	/* SawMill/regset.sch 64 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4077;

				{
					obj_t BgL_auxz00_4078;

					{	/* SawMill/regset.sch 64 */
						BgL_objectz00_bglt BgL_tmpz00_4079;

						BgL_tmpz00_4079 = ((BgL_objectz00_bglt) BgL_oz00_25);
						BgL_auxz00_4078 = BGL_OBJECT_WIDENING(BgL_tmpz00_4079);
					}
					BgL_auxz00_4077 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4078);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4077))->
					BgL_coalescez00);
			}
		}

	}



/* &rtl_reg/ra-coalesce */
	obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3584, obj_t BgL_oz00_3585)
	{
		{	/* SawMill/regset.sch 64 */
			return
				BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3585));
		}

	}



/* rtl_reg/ra-coalesce-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2utilszd2
		(BgL_rtl_regz00_bglt BgL_oz00_26, obj_t BgL_vz00_27)
	{
		{	/* SawMill/regset.sch 65 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4086;

				{
					obj_t BgL_auxz00_4087;

					{	/* SawMill/regset.sch 65 */
						BgL_objectz00_bglt BgL_tmpz00_4088;

						BgL_tmpz00_4088 = ((BgL_objectz00_bglt) BgL_oz00_26);
						BgL_auxz00_4087 = BGL_OBJECT_WIDENING(BgL_tmpz00_4088);
					}
					BgL_auxz00_4086 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4087);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4086))->
						BgL_coalescez00) = ((obj_t) BgL_vz00_27), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-coalesce-set! */
	obj_t BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3586, obj_t BgL_oz00_3587, obj_t BgL_vz00_3588)
	{
		{	/* SawMill/regset.sch 65 */
			return
				BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3587), BgL_vz00_3588);
		}

	}



/* rtl_reg/ra-color */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt
		BgL_oz00_28)
	{
		{	/* SawMill/regset.sch 66 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4095;

				{
					obj_t BgL_auxz00_4096;

					{	/* SawMill/regset.sch 66 */
						BgL_objectz00_bglt BgL_tmpz00_4097;

						BgL_tmpz00_4097 = ((BgL_objectz00_bglt) BgL_oz00_28);
						BgL_auxz00_4096 = BGL_OBJECT_WIDENING(BgL_tmpz00_4097);
					}
					BgL_auxz00_4095 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4096);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4095))->BgL_colorz00);
			}
		}

	}



/* &rtl_reg/ra-color */
	obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3589, obj_t BgL_oz00_3590)
	{
		{	/* SawMill/regset.sch 66 */
			return
				BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3590));
		}

	}



/* rtl_reg/ra-color-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt
		BgL_oz00_29, obj_t BgL_vz00_30)
	{
		{	/* SawMill/regset.sch 67 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4104;

				{
					obj_t BgL_auxz00_4105;

					{	/* SawMill/regset.sch 67 */
						BgL_objectz00_bglt BgL_tmpz00_4106;

						BgL_tmpz00_4106 = ((BgL_objectz00_bglt) BgL_oz00_29);
						BgL_auxz00_4105 = BGL_OBJECT_WIDENING(BgL_tmpz00_4106);
					}
					BgL_auxz00_4104 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4105);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4104))->
						BgL_colorz00) = ((obj_t) BgL_vz00_30), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-color-set! */
	obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3591, obj_t BgL_oz00_3592, obj_t BgL_vz00_3593)
	{
		{	/* SawMill/regset.sch 67 */
			return
				BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3592), BgL_vz00_3593);
		}

	}



/* rtl_reg/ra-num */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt
		BgL_oz00_31)
	{
		{	/* SawMill/regset.sch 68 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4113;

				{
					obj_t BgL_auxz00_4114;

					{	/* SawMill/regset.sch 68 */
						BgL_objectz00_bglt BgL_tmpz00_4115;

						BgL_tmpz00_4115 = ((BgL_objectz00_bglt) BgL_oz00_31);
						BgL_auxz00_4114 = BGL_OBJECT_WIDENING(BgL_tmpz00_4115);
					}
					BgL_auxz00_4113 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4114);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4113))->BgL_numz00);
			}
		}

	}



/* &rtl_reg/ra-num */
	obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2utilszd2(obj_t BgL_envz00_3594,
		obj_t BgL_oz00_3595)
	{
		{	/* SawMill/regset.sch 68 */
			return
				BINT(BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2utilszd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_3595)));
		}

	}



/* rtl_reg/ra-hardware */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt
		BgL_oz00_34)
	{
		{	/* SawMill/regset.sch 70 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_34)))->BgL_hardwarez00);
		}

	}



/* &rtl_reg/ra-hardware */
	obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3596, obj_t BgL_oz00_3597)
	{
		{	/* SawMill/regset.sch 70 */
			return
				BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3597));
		}

	}



/* rtl_reg/ra-key */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt
		BgL_oz00_37)
	{
		{	/* SawMill/regset.sch 72 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_37)))->BgL_keyz00);
		}

	}



/* &rtl_reg/ra-key */
	obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2utilszd2(obj_t BgL_envz00_3598,
		obj_t BgL_oz00_3599)
	{
		{	/* SawMill/regset.sch 72 */
			return
				BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3599));
		}

	}



/* rtl_reg/ra-name */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt
		BgL_oz00_40)
	{
		{	/* SawMill/regset.sch 74 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_40)))->BgL_namez00);
		}

	}



/* &rtl_reg/ra-name */
	obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2utilszd2(obj_t BgL_envz00_3600,
		obj_t BgL_oz00_3601)
	{
		{	/* SawMill/regset.sch 74 */
			return
				BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3601));
		}

	}



/* rtl_reg/ra-onexpr? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt
		BgL_oz00_43)
	{
		{	/* SawMill/regset.sch 76 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_43)))->BgL_onexprzf3zf3);
		}

	}



/* &rtl_reg/ra-onexpr? */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3602, obj_t BgL_oz00_3603)
	{
		{	/* SawMill/regset.sch 76 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3603));
		}

	}



/* rtl_reg/ra-onexpr?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2utilszd2
		(BgL_rtl_regz00_bglt BgL_oz00_44, obj_t BgL_vz00_45)
	{
		{	/* SawMill/regset.sch 77 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_44)))->BgL_onexprzf3zf3) =
				((obj_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &rtl_reg/ra-onexpr?-set! */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3604, obj_t BgL_oz00_3605, obj_t BgL_vz00_3606)
	{
		{	/* SawMill/regset.sch 77 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3605), BgL_vz00_3606);
		}

	}



/* rtl_reg/ra-var */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt
		BgL_oz00_46)
	{
		{	/* SawMill/regset.sch 78 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_46)))->BgL_varz00);
		}

	}



/* &rtl_reg/ra-var */
	obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2utilszd2(obj_t BgL_envz00_3607,
		obj_t BgL_oz00_3608)
	{
		{	/* SawMill/regset.sch 78 */
			return
				BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3608));
		}

	}



/* rtl_reg/ra-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt
		BgL_oz00_47, obj_t BgL_vz00_48)
	{
		{	/* SawMill/regset.sch 79 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_47)))->BgL_varz00) =
				((obj_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &rtl_reg/ra-var-set! */
	obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3609, obj_t BgL_oz00_3610, obj_t BgL_vz00_3611)
	{
		{	/* SawMill/regset.sch 79 */
			return
				BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3610), BgL_vz00_3611);
		}

	}



/* rtl_reg/ra-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt
		BgL_oz00_49)
	{
		{	/* SawMill/regset.sch 80 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_49)))->BgL_typez00);
		}

	}



/* &rtl_reg/ra-type */
	BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3612, obj_t BgL_oz00_3613)
	{
		{	/* SawMill/regset.sch 80 */
			return
				BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3613));
		}

	}



/* rtl_reg/ra-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2utilszd2(BgL_rtl_regz00_bglt
		BgL_oz00_50, BgL_typez00_bglt BgL_vz00_51)
	{
		{	/* SawMill/regset.sch 81 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_50)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_51), BUNSPEC);
		}

	}



/* &rtl_reg/ra-type-set! */
	obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3614, obj_t BgL_oz00_3615, obj_t BgL_vz00_3616)
	{
		{	/* SawMill/regset.sch 81 */
			return
				BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2utilszd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3615),
				((BgL_typez00_bglt) BgL_vz00_3616));
		}

	}



/* make-regset */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2utilszd2(int BgL_length1172z00_52,
		int BgL_msiza7e1173za7_53, obj_t BgL_regv1174z00_54,
		obj_t BgL_regl1175z00_55, obj_t BgL_string1176z00_56)
	{
		{	/* SawMill/regset.sch 84 */
			{	/* SawMill/regset.sch 84 */
				BgL_regsetz00_bglt BgL_new1168z00_3724;

				{	/* SawMill/regset.sch 84 */
					BgL_regsetz00_bglt BgL_new1167z00_3725;

					BgL_new1167z00_3725 =
						((BgL_regsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_regsetz00_bgl))));
					{	/* SawMill/regset.sch 84 */
						long BgL_arg1546z00_3726;

						BgL_arg1546z00_3726 = BGL_CLASS_NUM(BGl_regsetz00zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1167z00_3725), BgL_arg1546z00_3726);
					}
					BgL_new1168z00_3724 = BgL_new1167z00_3725;
				}
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3724))->BgL_lengthz00) =
					((int) BgL_length1172z00_52), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3724))->BgL_msiza7eza7) =
					((int) BgL_msiza7e1173za7_53), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3724))->BgL_regvz00) =
					((obj_t) BgL_regv1174z00_54), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3724))->BgL_reglz00) =
					((obj_t) BgL_regl1175z00_55), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3724))->BgL_stringz00) =
					((obj_t) BgL_string1176z00_56), BUNSPEC);
				return BgL_new1168z00_3724;
			}
		}

	}



/* &make-regset */
	BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3617, obj_t BgL_length1172z00_3618,
		obj_t BgL_msiza7e1173za7_3619, obj_t BgL_regv1174z00_3620,
		obj_t BgL_regl1175z00_3621, obj_t BgL_string1176z00_3622)
	{
		{	/* SawMill/regset.sch 84 */
			return
				BGl_makezd2regsetzd2zzsaw_bbvzd2utilszd2(CINT(BgL_length1172z00_3618),
				CINT(BgL_msiza7e1173za7_3619), BgL_regv1174z00_3620,
				BgL_regl1175z00_3621, BgL_string1176z00_3622);
		}

	}



/* regset? */
	BGL_EXPORTED_DEF bool_t BGl_regsetzf3zf3zzsaw_bbvzd2utilszd2(obj_t
		BgL_objz00_57)
	{
		{	/* SawMill/regset.sch 85 */
			{	/* SawMill/regset.sch 85 */
				obj_t BgL_classz00_3727;

				BgL_classz00_3727 = BGl_regsetz00zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_57))
					{	/* SawMill/regset.sch 85 */
						BgL_objectz00_bglt BgL_arg1807z00_3728;

						BgL_arg1807z00_3728 = (BgL_objectz00_bglt) (BgL_objz00_57);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 85 */
								long BgL_idxz00_3729;

								BgL_idxz00_3729 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3728);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3729 + 1L)) == BgL_classz00_3727);
							}
						else
							{	/* SawMill/regset.sch 85 */
								bool_t BgL_res2140z00_3732;

								{	/* SawMill/regset.sch 85 */
									obj_t BgL_oclassz00_3733;

									{	/* SawMill/regset.sch 85 */
										obj_t BgL_arg1815z00_3734;
										long BgL_arg1816z00_3735;

										BgL_arg1815z00_3734 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 85 */
											long BgL_arg1817z00_3736;

											BgL_arg1817z00_3736 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3728);
											BgL_arg1816z00_3735 = (BgL_arg1817z00_3736 - OBJECT_TYPE);
										}
										BgL_oclassz00_3733 =
											VECTOR_REF(BgL_arg1815z00_3734, BgL_arg1816z00_3735);
									}
									{	/* SawMill/regset.sch 85 */
										bool_t BgL__ortest_1115z00_3737;

										BgL__ortest_1115z00_3737 =
											(BgL_classz00_3727 == BgL_oclassz00_3733);
										if (BgL__ortest_1115z00_3737)
											{	/* SawMill/regset.sch 85 */
												BgL_res2140z00_3732 = BgL__ortest_1115z00_3737;
											}
										else
											{	/* SawMill/regset.sch 85 */
												long BgL_odepthz00_3738;

												{	/* SawMill/regset.sch 85 */
													obj_t BgL_arg1804z00_3739;

													BgL_arg1804z00_3739 = (BgL_oclassz00_3733);
													BgL_odepthz00_3738 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3739);
												}
												if ((1L < BgL_odepthz00_3738))
													{	/* SawMill/regset.sch 85 */
														obj_t BgL_arg1802z00_3740;

														{	/* SawMill/regset.sch 85 */
															obj_t BgL_arg1803z00_3741;

															BgL_arg1803z00_3741 = (BgL_oclassz00_3733);
															BgL_arg1802z00_3740 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3741,
																1L);
														}
														BgL_res2140z00_3732 =
															(BgL_arg1802z00_3740 == BgL_classz00_3727);
													}
												else
													{	/* SawMill/regset.sch 85 */
														BgL_res2140z00_3732 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2140z00_3732;
							}
					}
				else
					{	/* SawMill/regset.sch 85 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &regset? */
	obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2utilszd2(obj_t BgL_envz00_3623,
		obj_t BgL_objz00_3624)
	{
		{	/* SawMill/regset.sch 85 */
			return BBOOL(BGl_regsetzf3zf3zzsaw_bbvzd2utilszd2(BgL_objz00_3624));
		}

	}



/* regset-nil */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2utilszd2(void)
	{
		{	/* SawMill/regset.sch 86 */
			{	/* SawMill/regset.sch 86 */
				obj_t BgL_classz00_3102;

				BgL_classz00_3102 = BGl_regsetz00zzsaw_regsetz00;
				{	/* SawMill/regset.sch 86 */
					obj_t BgL__ortest_1117z00_3103;

					BgL__ortest_1117z00_3103 = BGL_CLASS_NIL(BgL_classz00_3102);
					if (CBOOL(BgL__ortest_1117z00_3103))
						{	/* SawMill/regset.sch 86 */
							return ((BgL_regsetz00_bglt) BgL__ortest_1117z00_3103);
						}
					else
						{	/* SawMill/regset.sch 86 */
							return
								((BgL_regsetz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3102));
						}
				}
			}
		}

	}



/* &regset-nil */
	BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3625)
	{
		{	/* SawMill/regset.sch 86 */
			return BGl_regsetzd2nilzd2zzsaw_bbvzd2utilszd2();
		}

	}



/* regset-string */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2utilszd2(BgL_regsetz00_bglt BgL_oz00_58)
	{
		{	/* SawMill/regset.sch 87 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_58))->BgL_stringz00);
		}

	}



/* &regset-string */
	obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2utilszd2(obj_t BgL_envz00_3626,
		obj_t BgL_oz00_3627)
	{
		{	/* SawMill/regset.sch 87 */
			return
				BGl_regsetzd2stringzd2zzsaw_bbvzd2utilszd2(
				((BgL_regsetz00_bglt) BgL_oz00_3627));
		}

	}



/* regset-string-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2utilszd2(BgL_regsetz00_bglt
		BgL_oz00_59, obj_t BgL_vz00_60)
	{
		{	/* SawMill/regset.sch 88 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_59))->BgL_stringz00) =
				((obj_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &regset-string-set! */
	obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3628, obj_t BgL_oz00_3629, obj_t BgL_vz00_3630)
	{
		{	/* SawMill/regset.sch 88 */
			return
				BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2utilszd2(
				((BgL_regsetz00_bglt) BgL_oz00_3629), BgL_vz00_3630);
		}

	}



/* regset-regl */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2utilszd2(BgL_regsetz00_bglt BgL_oz00_61)
	{
		{	/* SawMill/regset.sch 89 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_61))->BgL_reglz00);
		}

	}



/* &regset-regl */
	obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2utilszd2(obj_t BgL_envz00_3631,
		obj_t BgL_oz00_3632)
	{
		{	/* SawMill/regset.sch 89 */
			return
				BGl_regsetzd2reglzd2zzsaw_bbvzd2utilszd2(
				((BgL_regsetz00_bglt) BgL_oz00_3632));
		}

	}



/* regset-regv */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2utilszd2(BgL_regsetz00_bglt BgL_oz00_64)
	{
		{	/* SawMill/regset.sch 91 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_64))->BgL_regvz00);
		}

	}



/* &regset-regv */
	obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2utilszd2(obj_t BgL_envz00_3633,
		obj_t BgL_oz00_3634)
	{
		{	/* SawMill/regset.sch 91 */
			return
				BGl_regsetzd2regvzd2zzsaw_bbvzd2utilszd2(
				((BgL_regsetz00_bglt) BgL_oz00_3634));
		}

	}



/* regset-msize */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2utilszd2(BgL_regsetz00_bglt BgL_oz00_67)
	{
		{	/* SawMill/regset.sch 93 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_67))->BgL_msiza7eza7);
		}

	}



/* &regset-msize */
	obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2utilszd2(obj_t BgL_envz00_3635,
		obj_t BgL_oz00_3636)
	{
		{	/* SawMill/regset.sch 93 */
			return
				BINT(BGl_regsetzd2msiza7ez75zzsaw_bbvzd2utilszd2(
					((BgL_regsetz00_bglt) BgL_oz00_3636)));
		}

	}



/* regset-length */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2utilszd2(BgL_regsetz00_bglt BgL_oz00_70)
	{
		{	/* SawMill/regset.sch 95 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_70))->BgL_lengthz00);
		}

	}



/* &regset-length */
	obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2utilszd2(obj_t BgL_envz00_3637,
		obj_t BgL_oz00_3638)
	{
		{	/* SawMill/regset.sch 95 */
			return
				BINT(BGl_regsetzd2lengthzd2zzsaw_bbvzd2utilszd2(
					((BgL_regsetz00_bglt) BgL_oz00_3638)));
		}

	}



/* regset-length-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2utilszd2(BgL_regsetz00_bglt
		BgL_oz00_71, int BgL_vz00_72)
	{
		{	/* SawMill/regset.sch 96 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_71))->BgL_lengthz00) =
				((int) BgL_vz00_72), BUNSPEC);
		}

	}



/* &regset-length-set! */
	obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3639, obj_t BgL_oz00_3640, obj_t BgL_vz00_3641)
	{
		{	/* SawMill/regset.sch 96 */
			return
				BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2utilszd2(
				((BgL_regsetz00_bglt) BgL_oz00_3640), CINT(BgL_vz00_3641));
		}

	}



/* make-empty-bbset */
	obj_t BGl_makezd2emptyzd2bbsetz00zzsaw_bbvzd2utilszd2(void)
	{
		{	/* SawMill/bbset.sch 20 */
			{	/* SawMill/bbset.sch 21 */
				obj_t BgL_arg1573z00_2195;

				BgL_arg1573z00_2195 = BGl_getzd2bbzd2markz00zzsaw_bbvzd2typeszd2();
				{	/* SawMill/bbset.sch 15 */
					obj_t BgL_newz00_3111;

					BgL_newz00_3111 = create_struct(CNST_TABLE_REF(0), (int) (2L));
					{	/* SawMill/bbset.sch 15 */
						int BgL_tmpz00_4231;

						BgL_tmpz00_4231 = (int) (1L);
						STRUCT_SET(BgL_newz00_3111, BgL_tmpz00_4231, BNIL);
					}
					{	/* SawMill/bbset.sch 15 */
						int BgL_tmpz00_4234;

						BgL_tmpz00_4234 = (int) (0L);
						STRUCT_SET(BgL_newz00_3111, BgL_tmpz00_4234, BgL_arg1573z00_2195);
					}
					return BgL_newz00_3111;
				}
			}
		}

	}



/* bbset-cons */
	obj_t BGl_bbsetzd2conszd2zzsaw_bbvzd2utilszd2(BgL_blockz00_bglt BgL_bz00_85,
		obj_t BgL_setz00_86)
	{
		{	/* SawMill/bbset.sch 33 */
			{	/* SawMill/bbset.sch 34 */
				obj_t BgL_mz00_2199;

				BgL_mz00_2199 = STRUCT_REF(BgL_setz00_86, (int) (0L));
				{
					BgL_blocksz00_bglt BgL_auxz00_4239;

					{
						obj_t BgL_auxz00_4240;

						{	/* SawMill/bbset.sch 36 */
							BgL_objectz00_bglt BgL_tmpz00_4241;

							BgL_tmpz00_4241 = ((BgL_objectz00_bglt) BgL_bz00_85);
							BgL_auxz00_4240 = BGL_OBJECT_WIDENING(BgL_tmpz00_4241);
						}
						BgL_auxz00_4239 = ((BgL_blocksz00_bglt) BgL_auxz00_4240);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4239))->BgL_z52markz52) =
						((long) (long) CINT(BgL_mz00_2199)), BUNSPEC);
				}
				{	/* SawMill/bbset.sch 37 */
					obj_t BgL_arg1584z00_2201;

					{	/* SawMill/bbset.sch 37 */
						obj_t BgL_arg1585z00_2202;

						BgL_arg1585z00_2202 = STRUCT_REF(BgL_setz00_86, (int) (1L));
						BgL_arg1584z00_2201 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_85), BgL_arg1585z00_2202);
					}
					{	/* SawMill/bbset.sch 15 */
						int BgL_tmpz00_4251;

						BgL_tmpz00_4251 = (int) (1L);
						STRUCT_SET(BgL_setz00_86, BgL_tmpz00_4251, BgL_arg1584z00_2201);
				}}
				return BgL_setz00_86;
			}
		}

	}



/* <=ty */
	BGL_EXPORTED_DEF obj_t BGl_zc3zd3tyz10zzsaw_bbvzd2utilszd2(obj_t BgL_xz00_91,
		obj_t BgL_yz00_92)
	{
		{	/* SawBbv/bbv-utils.scm 55 */
			if ((BgL_yz00_92 == BGl_za2objza2z00zztype_cachez00))
				{	/* SawBbv/bbv-utils.scm 57 */
					return BTRUE;
				}
			else
				{	/* SawBbv/bbv-utils.scm 57 */
					if ((BgL_xz00_91 == BgL_yz00_92))
						{	/* SawBbv/bbv-utils.scm 58 */
							return BTRUE;
						}
					else
						{	/* SawBbv/bbv-utils.scm 58 */
							if ((BgL_xz00_91 == BGl_za2bintza2z00zztype_cachez00))
								{	/* SawBbv/bbv-utils.scm 59 */
									bool_t BgL__ortest_1172z00_2227;

									BgL__ortest_1172z00_2227 =
										(BgL_yz00_92 == BGl_za2longza2z00zztype_cachez00);
									if (BgL__ortest_1172z00_2227)
										{	/* SawBbv/bbv-utils.scm 59 */
											return BBOOL(BgL__ortest_1172z00_2227);
										}
									else
										{	/* SawBbv/bbv-utils.scm 59 */
											return BBOOL((BgL_yz00_92 == CNST_TABLE_REF(1)));
										}
								}
							else
								{	/* SawBbv/bbv-utils.scm 59 */
									if ((BgL_xz00_91 == BGl_za2longza2z00zztype_cachez00))
										{	/* SawBbv/bbv-utils.scm 60 */
											bool_t BgL__ortest_1173z00_2228;

											BgL__ortest_1173z00_2228 =
												(BgL_yz00_92 == BGl_za2bintza2z00zztype_cachez00);
											if (BgL__ortest_1173z00_2228)
												{	/* SawBbv/bbv-utils.scm 60 */
													return BBOOL(BgL__ortest_1173z00_2228);
												}
											else
												{	/* SawBbv/bbv-utils.scm 60 */
													return BBOOL((BgL_yz00_92 == CNST_TABLE_REF(1)));
												}
										}
									else
										{	/* SawBbv/bbv-utils.scm 60 */
											if ((BgL_xz00_91 == BGl_za2realza2z00zztype_cachez00))
												{	/* SawBbv/bbv-utils.scm 61 */
													bool_t BgL__ortest_1174z00_2229;

													BgL__ortest_1174z00_2229 =
														(BgL_yz00_92 == BGl_za2brealza2z00zztype_cachez00);
													if (BgL__ortest_1174z00_2229)
														{	/* SawBbv/bbv-utils.scm 61 */
															return BBOOL(BgL__ortest_1174z00_2229);
														}
													else
														{	/* SawBbv/bbv-utils.scm 61 */
															return BBOOL((BgL_yz00_92 == CNST_TABLE_REF(1)));
														}
												}
											else
												{	/* SawBbv/bbv-utils.scm 61 */
													if (
														(BgL_xz00_91 == BGl_za2brealza2z00zztype_cachez00))
														{	/* SawBbv/bbv-utils.scm 62 */
															bool_t BgL__ortest_1175z00_2230;

															BgL__ortest_1175z00_2230 =
																(BgL_yz00_92 ==
																BGl_za2realza2z00zztype_cachez00);
															if (BgL__ortest_1175z00_2230)
																{	/* SawBbv/bbv-utils.scm 62 */
																	return BBOOL(BgL__ortest_1175z00_2230);
																}
															else
																{	/* SawBbv/bbv-utils.scm 62 */
																	return
																		BBOOL((BgL_yz00_92 == CNST_TABLE_REF(1)));
																}
														}
													else
														{	/* SawBbv/bbv-utils.scm 63 */
															bool_t BgL_test2245z00_4290;

															if ((BgL_xz00_91 == CNST_TABLE_REF(1)))
																{	/* SawBbv/bbv-utils.scm 63 */
																	BgL_test2245z00_4290 = ((bool_t) 1);
																}
															else
																{	/* SawBbv/bbv-utils.scm 63 */
																	BgL_test2245z00_4290 =
																		(BgL_yz00_92 == CNST_TABLE_REF(1));
																}
															if (BgL_test2245z00_4290)
																{	/* SawBbv/bbv-utils.scm 63 */
																	return BFALSE;
																}
															else
																{	/* SawBbv/bbv-utils.scm 63 */
																	if (
																		(BgL_yz00_92 ==
																			BGl_za2pairzd2nilza2zd2zztype_cachez00))
																		{	/* SawBbv/bbv-utils.scm 64 */
																			return BFALSE;
																		}
																	else
																		{	/* SawBbv/bbv-utils.scm 64 */
																			BGL_TAIL return
																				BGl_iszd2subtypezf3z21zztype_typeofz00
																				(BgL_xz00_91, BgL_yz00_92);
																		}
																}
														}
												}
										}
								}
						}
				}
		}

	}



/* &<=ty */
	obj_t BGl_z62zc3zd3tyz72zzsaw_bbvzd2utilszd2(obj_t BgL_envz00_3642,
		obj_t BgL_xz00_3643, obj_t BgL_yz00_3644)
	{
		{	/* SawBbv/bbv-utils.scm 55 */
			return BGl_zc3zd3tyz10zzsaw_bbvzd2utilszd2(BgL_xz00_3643, BgL_yz00_3644);
		}

	}



/* genlabel */
	BGL_EXPORTED_DEF obj_t BGl_genlabelz00zzsaw_bbvzd2utilszd2(void)
	{
		{	/* SawBbv/bbv-utils.scm 71 */
			BGl_za2labelza2z00zzsaw_bbvzd2utilszd2 =
				(1L + BGl_za2labelza2z00zzsaw_bbvzd2utilszd2);
			return BINT(BGl_za2labelza2z00zzsaw_bbvzd2utilszd2);
		}

	}



/* &genlabel */
	obj_t BGl_z62genlabelz62zzsaw_bbvzd2utilszd2(obj_t BgL_envz00_3645)
	{
		{	/* SawBbv/bbv-utils.scm 71 */
			return BGl_genlabelz00zzsaw_bbvzd2utilszd2();
		}

	}



/* set-max-label! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2maxzd2labelz12z12zzsaw_bbvzd2utilszd2(obj_t
		BgL_blocksz00_93)
	{
		{	/* SawBbv/bbv-utils.scm 78 */
			BGl_za2labelza2z00zzsaw_bbvzd2utilszd2 = 0L;
			{
				obj_t BgL_l1465z00_2233;

				{	/* SawBbv/bbv-utils.scm 80 */
					bool_t BgL_tmpz00_4303;

					BgL_l1465z00_2233 = BgL_blocksz00_93;
				BgL_zc3z04anonymousza31629ze3z87_2234:
					if (PAIRP(BgL_l1465z00_2233))
						{	/* SawBbv/bbv-utils.scm 80 */
							{	/* SawBbv/bbv-utils.scm 81 */
								obj_t BgL_bz00_2236;

								BgL_bz00_2236 = CAR(BgL_l1465z00_2233);
								{	/* SawBbv/bbv-utils.scm 82 */
									bool_t BgL_test2249z00_4307;

									{	/* SawBbv/bbv-utils.scm 82 */
										long BgL_n2z00_3126;

										BgL_n2z00_3126 = BGl_za2labelza2z00zzsaw_bbvzd2utilszd2;
										BgL_test2249z00_4307 =
											(
											(long) (
												(((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt) BgL_bz00_2236)))->
													BgL_labelz00)) > BgL_n2z00_3126);
									}
									if (BgL_test2249z00_4307)
										{	/* SawBbv/bbv-utils.scm 82 */
											BGl_za2labelza2z00zzsaw_bbvzd2utilszd2 =
												(long) (
												(((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt) BgL_bz00_2236)))->
													BgL_labelz00));
										}
									else
										{	/* SawBbv/bbv-utils.scm 82 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1465z00_4315;

								BgL_l1465z00_4315 = CDR(BgL_l1465z00_2233);
								BgL_l1465z00_2233 = BgL_l1465z00_4315;
								goto BgL_zc3z04anonymousza31629ze3z87_2234;
							}
						}
					else
						{	/* SawBbv/bbv-utils.scm 80 */
							BgL_tmpz00_4303 = ((bool_t) 1);
						}
					return BBOOL(BgL_tmpz00_4303);
				}
			}
		}

	}



/* &set-max-label! */
	obj_t BGl_z62setzd2maxzd2labelz12z70zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3646, obj_t BgL_blocksz00_3647)
	{
		{	/* SawBbv/bbv-utils.scm 78 */
			return
				BGl_setzd2maxzd2labelz12z12zzsaw_bbvzd2utilszd2(BgL_blocksz00_3647);
		}

	}



/* list-replace */
	BGL_EXPORTED_DEF obj_t BGl_listzd2replacezd2zzsaw_bbvzd2utilszd2(obj_t
		BgL_lstz00_94, obj_t BgL_oldz00_95, obj_t BgL_newz00_96)
	{
		{	/* SawBbv/bbv-utils.scm 89 */
			return
				BGl_loopze70ze7zzsaw_bbvzd2utilszd2(BgL_oldz00_95, BgL_newz00_96,
				BgL_lstz00_94);
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzsaw_bbvzd2utilszd2(obj_t BgL_oldz00_3690,
		obj_t BgL_newz00_3689, obj_t BgL_lz00_2244)
	{
		{	/* SawBbv/bbv-utils.scm 90 */
			if (NULLP(BgL_lz00_2244))
				{	/* SawBbv/bbv-utils.scm 92 */
					return BgL_lz00_2244;
				}
			else
				{	/* SawBbv/bbv-utils.scm 92 */
					if ((CAR(((obj_t) BgL_lz00_2244)) == BgL_oldz00_3690))
						{	/* SawBbv/bbv-utils.scm 93 */
							obj_t BgL_arg1661z00_2249;

							BgL_arg1661z00_2249 = CDR(((obj_t) BgL_lz00_2244));
							return MAKE_YOUNG_PAIR(BgL_newz00_3689, BgL_arg1661z00_2249);
						}
					else
						{	/* SawBbv/bbv-utils.scm 94 */
							obj_t BgL_arg1663z00_2250;
							obj_t BgL_arg1675z00_2251;

							BgL_arg1663z00_2250 = CAR(((obj_t) BgL_lz00_2244));
							{	/* SawBbv/bbv-utils.scm 94 */
								obj_t BgL_arg1678z00_2252;

								BgL_arg1678z00_2252 = CDR(((obj_t) BgL_lz00_2244));
								BgL_arg1675z00_2251 =
									BGl_loopze70ze7zzsaw_bbvzd2utilszd2(BgL_oldz00_3690,
									BgL_newz00_3689, BgL_arg1678z00_2252);
							}
							return MAKE_YOUNG_PAIR(BgL_arg1663z00_2250, BgL_arg1675z00_2251);
						}
				}
		}

	}



/* &list-replace */
	obj_t BGl_z62listzd2replacezb0zzsaw_bbvzd2utilszd2(obj_t BgL_envz00_3648,
		obj_t BgL_lstz00_3649, obj_t BgL_oldz00_3650, obj_t BgL_newz00_3651)
	{
		{	/* SawBbv/bbv-utils.scm 89 */
			return
				BGl_listzd2replacezd2zzsaw_bbvzd2utilszd2(BgL_lstz00_3649,
				BgL_oldz00_3650, BgL_newz00_3651);
		}

	}



/* block->block-list */
	BGL_EXPORTED_DEF obj_t
		BGl_blockzd2ze3blockzd2listze3zzsaw_bbvzd2utilszd2(obj_t BgL_regsz00_97,
		BgL_blockz00_bglt BgL_bz00_98)
	{
		{	/* SawBbv/bbv-utils.scm 99 */
			{	/* SawBbv/bbv-utils.scm 100 */
				obj_t BgL_g1177z00_2255;
				obj_t BgL_g1178z00_2256;

				{	/* SawBbv/bbv-utils.scm 100 */
					obj_t BgL_list1712z00_2280;

					BgL_list1712z00_2280 = MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_98), BNIL);
					BgL_g1177z00_2255 = BgL_list1712z00_2280;
				}
				BgL_g1178z00_2256 = BGl_makezd2emptyzd2bbsetz00zzsaw_bbvzd2utilszd2();
				{
					obj_t BgL_bsz00_2258;
					obj_t BgL_accz00_2259;

					BgL_bsz00_2258 = BgL_g1177z00_2255;
					BgL_accz00_2259 = BgL_g1178z00_2256;
				BgL_zc3z04anonymousza31682ze3z87_2260:
					if (NULLP(BgL_bsz00_2258))
						{	/* SawBbv/bbv-utils.scm 103 */
							return bgl_reverse_bang(STRUCT_REF(BgL_accz00_2259, (int) (1L)));
						}
					else
						{	/* SawBbv/bbv-utils.scm 105 */
							bool_t BgL_test2253z00_4344;

							{	/* SawBbv/bbv-utils.scm 105 */
								BgL_blockz00_bglt BgL_blockz00_3135;

								BgL_blockz00_3135 =
									((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2258)));
								{	/* SawBbv/bbv-utils.scm 105 */
									long BgL_arg1575z00_3138;
									obj_t BgL_arg1576z00_3139;

									{
										BgL_blocksz00_bglt BgL_auxz00_4348;

										{
											obj_t BgL_auxz00_4349;

											{	/* SawBbv/bbv-utils.scm 105 */
												BgL_objectz00_bglt BgL_tmpz00_4350;

												BgL_tmpz00_4350 =
													((BgL_objectz00_bglt) BgL_blockz00_3135);
												BgL_auxz00_4349 = BGL_OBJECT_WIDENING(BgL_tmpz00_4350);
											}
											BgL_auxz00_4348 = ((BgL_blocksz00_bglt) BgL_auxz00_4349);
										}
										BgL_arg1575z00_3138 =
											(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4348))->
											BgL_z52markz52);
									}
									BgL_arg1576z00_3139 = STRUCT_REF(BgL_accz00_2259, (int) (0L));
									BgL_test2253z00_4344 =
										(BINT(BgL_arg1575z00_3138) == BgL_arg1576z00_3139);
							}}
							if (BgL_test2253z00_4344)
								{	/* SawBbv/bbv-utils.scm 106 */
									obj_t BgL_arg1691z00_2265;

									BgL_arg1691z00_2265 = CDR(((obj_t) BgL_bsz00_2258));
									{
										obj_t BgL_bsz00_4361;

										BgL_bsz00_4361 = BgL_arg1691z00_2265;
										BgL_bsz00_2258 = BgL_bsz00_4361;
										goto BgL_zc3z04anonymousza31682ze3z87_2260;
									}
								}
							else
								{	/* SawBbv/bbv-utils.scm 108 */
									BgL_blockz00_bglt BgL_i1179z00_2266;

									BgL_i1179z00_2266 =
										((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2258)));
									if (NULLP(
											(((BgL_blockz00_bglt) COBJECT(BgL_i1179z00_2266))->
												BgL_succsz00)))
										{	/* SawBbv/bbv-utils.scm 111 */
											obj_t BgL_arg1699z00_2269;
											obj_t BgL_arg1700z00_2270;

											BgL_arg1699z00_2269 = CDR(((obj_t) BgL_bsz00_2258));
											{	/* SawBbv/bbv-utils.scm 111 */
												obj_t BgL_arg1701z00_2271;

												BgL_arg1701z00_2271 = CAR(((obj_t) BgL_bsz00_2258));
												BgL_arg1700z00_2270 =
													BGl_bbsetzd2conszd2zzsaw_bbvzd2utilszd2(
													((BgL_blockz00_bglt) BgL_arg1701z00_2271),
													BgL_accz00_2259);
											}
											{
												obj_t BgL_accz00_4375;
												obj_t BgL_bsz00_4374;

												BgL_bsz00_4374 = BgL_arg1699z00_2269;
												BgL_accz00_4375 = BgL_arg1700z00_2270;
												BgL_accz00_2259 = BgL_accz00_4375;
												BgL_bsz00_2258 = BgL_bsz00_4374;
												goto BgL_zc3z04anonymousza31682ze3z87_2260;
											}
										}
									else
										{	/* SawBbv/bbv-utils.scm 113 */
											obj_t BgL_arg1702z00_2272;
											obj_t BgL_arg1703z00_2273;

											{	/* SawBbv/bbv-utils.scm 113 */
												obj_t BgL_arg1705z00_2274;
												obj_t BgL_arg1708z00_2275;

												BgL_arg1705z00_2274 =
													(((BgL_blockz00_bglt) COBJECT(BgL_i1179z00_2266))->
													BgL_succsz00);
												BgL_arg1708z00_2275 = CDR(((obj_t) BgL_bsz00_2258));
												BgL_arg1702z00_2272 =
													BGl_appendzd221011zd2zzsaw_bbvzd2utilszd2
													(BgL_arg1705z00_2274, BgL_arg1708z00_2275);
											}
											{	/* SawBbv/bbv-utils.scm 114 */
												obj_t BgL_arg1709z00_2276;

												BgL_arg1709z00_2276 = CAR(((obj_t) BgL_bsz00_2258));
												BgL_arg1703z00_2273 =
													BGl_bbsetzd2conszd2zzsaw_bbvzd2utilszd2(
													((BgL_blockz00_bglt) BgL_arg1709z00_2276),
													BgL_accz00_2259);
											}
											{
												obj_t BgL_accz00_4385;
												obj_t BgL_bsz00_4384;

												BgL_bsz00_4384 = BgL_arg1702z00_2272;
												BgL_accz00_4385 = BgL_arg1703z00_2273;
												BgL_accz00_2259 = BgL_accz00_4385;
												BgL_bsz00_2258 = BgL_bsz00_4384;
												goto BgL_zc3z04anonymousza31682ze3z87_2260;
											}
										}
								}
						}
				}
			}
		}

	}



/* &block->block-list */
	obj_t BGl_z62blockzd2ze3blockzd2listz81zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3652, obj_t BgL_regsz00_3653, obj_t BgL_bz00_3654)
	{
		{	/* SawBbv/bbv-utils.scm 99 */
			return
				BGl_blockzd2ze3blockzd2listze3zzsaw_bbvzd2utilszd2(BgL_regsz00_3653,
				((BgL_blockz00_bglt) BgL_bz00_3654));
		}

	}



/* redirect-block! */
	BGL_EXPORTED_DEF obj_t
		BGl_redirectzd2blockz12zc0zzsaw_bbvzd2utilszd2(BgL_blockz00_bglt
		BgL_bz00_99, BgL_blockz00_bglt BgL_oldz00_100,
		BgL_blockz00_bglt BgL_newz00_101)
	{
		{	/* SawBbv/bbv-utils.scm 119 */
			{
				obj_t BgL_auxz00_4388;

				{	/* SawBbv/bbv-utils.scm 127 */
					obj_t BgL_arg1714z00_2282;

					BgL_arg1714z00_2282 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_bz00_99)))->BgL_succsz00);
					BgL_auxz00_4388 =
						BGl_loopze70ze7zzsaw_bbvzd2utilszd2(
						((obj_t) BgL_oldz00_100),
						((obj_t) BgL_newz00_101), BgL_arg1714z00_2282);
				}
				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_bz00_99)))->BgL_succsz00) =
					((obj_t) BgL_auxz00_4388), BUNSPEC);
			}
			{	/* SawBbv/bbv-utils.scm 129 */
				obj_t BgL_arg1717z00_2284;

				{	/* SawBbv/bbv-utils.scm 129 */
					obj_t BgL_arg1718z00_2285;

					BgL_arg1718z00_2285 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_oldz00_100)))->BgL_predsz00);
					BgL_arg1717z00_2284 =
						bgl_remq_bang(((obj_t) BgL_bz00_99), BgL_arg1718z00_2285);
				}
				BGl_blockzd2predszd2updatez12z12zzsaw_bbvzd2typeszd2(
					((BgL_blockz00_bglt) BgL_oldz00_100), BgL_arg1717z00_2284);
			}
			if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
						((obj_t) BgL_bz00_99),
						(((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_newz00_101)))->BgL_predsz00))))
				{	/* SawBbv/bbv-utils.scm 131 */
					BFALSE;
				}
			else
				{	/* SawBbv/bbv-utils.scm 132 */
					obj_t BgL_arg1722z00_2289;

					{	/* SawBbv/bbv-utils.scm 132 */
						obj_t BgL_arg1724z00_2290;

						BgL_arg1724z00_2290 =
							(((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_newz00_101)))->BgL_predsz00);
						BgL_arg1722z00_2289 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_99), BgL_arg1724z00_2290);
					}
					BGl_blockzd2predszd2updatez12z12zzsaw_bbvzd2typeszd2(
						((BgL_blockz00_bglt) BgL_newz00_101), BgL_arg1722z00_2289);
				}
			{	/* SawBbv/bbv-utils.scm 133 */
				obj_t BgL_g1469z00_2292;

				BgL_g1469z00_2292 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_bz00_99)))->BgL_firstz00);
				{
					obj_t BgL_l1467z00_2294;

					BgL_l1467z00_2294 = BgL_g1469z00_2292;
				BgL_zc3z04anonymousza31734ze3z87_2295:
					if (PAIRP(BgL_l1467z00_2294))
						{	/* SawBbv/bbv-utils.scm 133 */
							{	/* SawBbv/bbv-utils.scm 135 */
								obj_t BgL_insz00_2297;

								BgL_insz00_2297 = CAR(BgL_l1467z00_2294);
								if (BGl_rtl_inszd2ifeqzf3z21zzsaw_bbvzd2typeszd2(
										((BgL_rtl_insz00_bglt) BgL_insz00_2297)))
									{	/* SawBbv/bbv-utils.scm 137 */
										BgL_rtl_ifeqz00_bglt BgL_i1184z00_2300;

										BgL_i1184z00_2300 =
											((BgL_rtl_ifeqz00_bglt)
											(((BgL_rtl_insz00_bglt) COBJECT(
														((BgL_rtl_insz00_bglt) BgL_insz00_2297)))->
												BgL_funz00));
										if ((((obj_t) (((BgL_rtl_ifeqz00_bglt)
															COBJECT(BgL_i1184z00_2300))->BgL_thenz00)) ==
												((obj_t) BgL_oldz00_100)))
											{	/* SawBbv/bbv-utils.scm 138 */
												((((BgL_rtl_ifeqz00_bglt) COBJECT(BgL_i1184z00_2300))->
														BgL_thenz00) =
													((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
															BgL_newz00_101)), BUNSPEC);
											}
										else
											{	/* SawBbv/bbv-utils.scm 138 */
												BFALSE;
											}
									}
								else
									{	/* SawBbv/bbv-utils.scm 135 */
										if (BGl_rtl_inszd2ifnezf3z21zzsaw_bbvzd2typeszd2(
												((BgL_rtl_insz00_bglt) BgL_insz00_2297)))
											{	/* SawBbv/bbv-utils.scm 142 */
												BgL_rtl_ifnez00_bglt BgL_i1186z00_2306;

												BgL_i1186z00_2306 =
													((BgL_rtl_ifnez00_bglt)
													(((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt) BgL_insz00_2297)))->
														BgL_funz00));
												if ((((obj_t) (((BgL_rtl_ifnez00_bglt)
																	COBJECT(BgL_i1186z00_2306))->BgL_thenz00)) ==
														((obj_t) BgL_oldz00_100)))
													{	/* SawBbv/bbv-utils.scm 143 */
														((((BgL_rtl_ifnez00_bglt)
																	COBJECT(BgL_i1186z00_2306))->BgL_thenz00) =
															((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
																	BgL_newz00_101)), BUNSPEC);
													}
												else
													{	/* SawBbv/bbv-utils.scm 143 */
														BFALSE;
													}
											}
										else
											{	/* SawBbv/bbv-utils.scm 140 */
												if (BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2(
														((BgL_rtl_insz00_bglt) BgL_insz00_2297)))
													{	/* SawBbv/bbv-utils.scm 147 */
														BgL_rtl_goz00_bglt BgL_i1188z00_2312;

														BgL_i1188z00_2312 =
															((BgL_rtl_goz00_bglt)
															(((BgL_rtl_insz00_bglt) COBJECT(
																		((BgL_rtl_insz00_bglt) BgL_insz00_2297)))->
																BgL_funz00));
														if ((((obj_t) (((BgL_rtl_goz00_bglt)
																			COBJECT(BgL_i1188z00_2312))->
																		BgL_toz00)) == ((obj_t) BgL_oldz00_100)))
															{	/* SawBbv/bbv-utils.scm 148 */
																((((BgL_rtl_goz00_bglt)
																			COBJECT(BgL_i1188z00_2312))->BgL_toz00) =
																	((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
																			BgL_newz00_101)), BUNSPEC);
															}
														else
															{	/* SawBbv/bbv-utils.scm 148 */
																BFALSE;
															}
													}
												else
													{	/* SawBbv/bbv-utils.scm 145 */
														if (CBOOL
															(BGl_rtl_inszd2switchzf3z21zzsaw_bbvzd2typeszd2((
																		(BgL_rtl_insz00_bglt) BgL_insz00_2297))))
															{	/* SawBbv/bbv-utils.scm 152 */
																BgL_rtl_switchz00_bglt BgL_i1190z00_2318;

																BgL_i1190z00_2318 =
																	((BgL_rtl_switchz00_bglt)
																	(((BgL_rtl_insz00_bglt) COBJECT(
																				((BgL_rtl_insz00_bglt)
																					BgL_insz00_2297)))->BgL_funz00));
																{
																	obj_t BgL_auxz00_4465;

																	{	/* SawBbv/bbv-utils.scm 153 */
																		obj_t BgL_arg1755z00_2319;

																		BgL_arg1755z00_2319 =
																			(((BgL_rtl_switchz00_bglt)
																				COBJECT(BgL_i1190z00_2318))->
																			BgL_labelsz00);
																		BgL_auxz00_4465 =
																			BGl_loopze70ze7zzsaw_bbvzd2utilszd2((
																				(obj_t) BgL_oldz00_100),
																			((obj_t) BgL_newz00_101),
																			((obj_t) BgL_arg1755z00_2319));
																	}
																	((((BgL_rtl_switchz00_bglt)
																				COBJECT(BgL_i1190z00_2318))->
																			BgL_labelsz00) =
																		((obj_t) BgL_auxz00_4465), BUNSPEC);
																}
															}
														else
															{	/* SawBbv/bbv-utils.scm 150 */
																BFALSE;
															}
													}
											}
									}
							}
							{
								obj_t BgL_l1467z00_4472;

								BgL_l1467z00_4472 = CDR(BgL_l1467z00_2294);
								BgL_l1467z00_2294 = BgL_l1467z00_4472;
								goto BgL_zc3z04anonymousza31734ze3z87_2295;
							}
						}
					else
						{	/* SawBbv/bbv-utils.scm 133 */
							((bool_t) 1);
						}
				}
			}
			return ((obj_t) BgL_bz00_99);
		}

	}



/* &redirect-block! */
	obj_t BGl_z62redirectzd2blockz12za2zzsaw_bbvzd2utilszd2(obj_t BgL_envz00_3655,
		obj_t BgL_bz00_3656, obj_t BgL_oldz00_3657, obj_t BgL_newz00_3658)
	{
		{	/* SawBbv/bbv-utils.scm 119 */
			return
				BGl_redirectzd2blockz12zc0zzsaw_bbvzd2utilszd2(
				((BgL_blockz00_bglt) BgL_bz00_3656),
				((BgL_blockz00_bglt) BgL_oldz00_3657),
				((BgL_blockz00_bglt) BgL_newz00_3658));
		}

	}



/* _replace-block! */
	obj_t BGl__replacezd2blockz12zc0zzsaw_bbvzd2utilszd2(obj_t BgL_env1507z00_106,
		obj_t BgL_opt1506z00_105)
	{
		{	/* SawBbv/bbv-utils.scm 163 */
			{
				obj_t BgL_k1z00_2323;
				long BgL_iz00_2324;

				{	/* SawBbv/bbv-utils.scm 163 */
					obj_t BgL_g1513z00_2326;
					obj_t BgL_g1514z00_2327;

					BgL_g1513z00_2326 = VECTOR_REF(BgL_opt1506z00_105, 0L);
					BgL_g1514z00_2327 = VECTOR_REF(BgL_opt1506z00_105, 1L);
					{	/* SawBbv/bbv-utils.scm 163 */
						obj_t BgL_debugz00_2328;

						BgL_debugz00_2328 = BFALSE;
						{	/* SawBbv/bbv-utils.scm 163 */

							{	/* SawBbv/bbv-utils.scm 163 */
								long BgL_index1512z00_2329;

								BgL_k1z00_2323 = CNST_TABLE_REF(2);
								BgL_iz00_2324 = 2L;
							BgL_search1509z00_2325:
								if ((BgL_iz00_2324 == VECTOR_LENGTH(BgL_opt1506z00_105)))
									{	/* SawBbv/bbv-utils.scm 163 */
										BgL_index1512z00_2329 = -1L;
									}
								else
									{	/* SawBbv/bbv-utils.scm 163 */
										obj_t BgL_vz00_2335;

										BgL_vz00_2335 =
											VECTOR_REF(BgL_opt1506z00_105, BgL_iz00_2324);
										if ((BgL_vz00_2335 == BgL_k1z00_2323))
											{	/* SawBbv/bbv-utils.scm 163 */
												BgL_index1512z00_2329 = (BgL_iz00_2324 + 1L);
											}
										else
											{
												long BgL_iz00_4488;

												BgL_iz00_4488 = (BgL_iz00_2324 + 2L);
												BgL_iz00_2324 = BgL_iz00_4488;
												goto BgL_search1509z00_2325;
											}
									}
								if ((BgL_index1512z00_2329 >= 0L))
									{	/* SawBbv/bbv-utils.scm 163 */
										BgL_debugz00_2328 =
											VECTOR_REF(BgL_opt1506z00_105, BgL_index1512z00_2329);
									}
								else
									{	/* SawBbv/bbv-utils.scm 163 */
										BFALSE;
									}
							}
							{	/* SawBbv/bbv-utils.scm 163 */
								obj_t BgL_arg1765z00_2331;
								obj_t BgL_arg1767z00_2332;

								BgL_arg1765z00_2331 = VECTOR_REF(BgL_opt1506z00_105, 0L);
								BgL_arg1767z00_2332 = VECTOR_REF(BgL_opt1506z00_105, 1L);
								{	/* SawBbv/bbv-utils.scm 163 */
									obj_t BgL_debugz00_2333;

									BgL_debugz00_2333 = BgL_debugz00_2328;
									return
										BGl_replacezd2blockz12zc0zzsaw_bbvzd2utilszd2(
										((BgL_blockz00_bglt) BgL_arg1765z00_2331),
										((BgL_blockz00_bglt) BgL_arg1767z00_2332),
										BgL_debugz00_2333);
								}
							}
						}
					}
				}
			}
		}

	}



/* replace-block! */
	BGL_EXPORTED_DEF obj_t
		BGl_replacezd2blockz12zc0zzsaw_bbvzd2utilszd2(BgL_blockz00_bglt
		BgL_oldz00_102, BgL_blockz00_bglt BgL_newz00_103, obj_t BgL_debugz00_104)
	{
		{	/* SawBbv/bbv-utils.scm 163 */
			if (CBOOL(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2))
				{	/* SawBbv/bbv-utils.scm 183 */
					BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(BgL_oldz00_102,
						BGl_string2145z00zzsaw_bbvzd2utilszd2);
					BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(BgL_newz00_103,
						BGl_string2146z00zzsaw_bbvzd2utilszd2);
				}
			else
				{	/* SawBbv/bbv-utils.scm 183 */
					BFALSE;
				}
			if ((((obj_t) BgL_oldz00_102) == ((obj_t) BgL_newz00_103)))
				{	/* SawBbv/bbv-utils.scm 186 */
					return BFALSE;
				}
			else
				{	/* SawBbv/bbv-utils.scm 186 */
					{	/* SawBbv/bbv-utils.scm 188 */
						bool_t BgL_test2269z00_4507;

						if (CBOOL(BgL_debugz00_104))
							{	/* SawBbv/bbv-utils.scm 188 */
								BgL_test2269z00_4507 =
									CBOOL(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2);
							}
						else
							{	/* SawBbv/bbv-utils.scm 188 */
								BgL_test2269z00_4507 = ((bool_t) 0);
							}
						if (BgL_test2269z00_4507)
							{	/* SawBbv/bbv-utils.scm 191 */
								bool_t BgL_test2271z00_4511;

								{	/* SawBbv/bbv-utils.scm 191 */
									BgL_bbvzd2ctxzd2_bglt BgL_arg1856z00_2376;
									BgL_bbvzd2ctxzd2_bglt BgL_arg1857z00_2377;

									{
										BgL_blocksz00_bglt BgL_auxz00_4512;

										{
											obj_t BgL_auxz00_4513;

											{	/* SawBbv/bbv-utils.scm 191 */
												BgL_objectz00_bglt BgL_tmpz00_4514;

												BgL_tmpz00_4514 = ((BgL_objectz00_bglt) BgL_newz00_103);
												BgL_auxz00_4513 = BGL_OBJECT_WIDENING(BgL_tmpz00_4514);
											}
											BgL_auxz00_4512 = ((BgL_blocksz00_bglt) BgL_auxz00_4513);
										}
										BgL_arg1856z00_2376 =
											(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4512))->
											BgL_ctxz00);
									}
									{
										BgL_blocksz00_bglt BgL_auxz00_4519;

										{
											obj_t BgL_auxz00_4520;

											{	/* SawBbv/bbv-utils.scm 191 */
												BgL_objectz00_bglt BgL_tmpz00_4521;

												BgL_tmpz00_4521 = ((BgL_objectz00_bglt) BgL_oldz00_102);
												BgL_auxz00_4520 = BGL_OBJECT_WIDENING(BgL_tmpz00_4521);
											}
											BgL_auxz00_4519 = ((BgL_blocksz00_bglt) BgL_auxz00_4520);
										}
										BgL_arg1857z00_2377 =
											(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4519))->
											BgL_ctxz00);
									}
									BgL_test2271z00_4511 =
										BGl_ctxze3zd3zf3zc3zzsaw_bbvzd2utilszd2(BgL_arg1856z00_2376,
										BgL_arg1857z00_2377);
								}
								if (BgL_test2271z00_4511)
									{	/* SawBbv/bbv-utils.scm 191 */
										BFALSE;
									}
								else
									{	/* SawBbv/bbv-utils.scm 191 */
										{	/* SawBbv/bbv-utils.scm 192 */
											obj_t BgL_arg1798z00_2343;
											int BgL_arg1799z00_2344;
											int BgL_arg1805z00_2345;

											{	/* SawBbv/bbv-utils.scm 192 */
												obj_t BgL_tmpz00_4527;

												BgL_tmpz00_4527 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_arg1798z00_2343 =
													BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_4527);
											}
											BgL_arg1799z00_2344 =
												(((BgL_blockz00_bglt) COBJECT(
														((BgL_blockz00_bglt) BgL_oldz00_102)))->
												BgL_labelz00);
											BgL_arg1805z00_2345 =
												(((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
															BgL_newz00_103)))->BgL_labelz00);
											{	/* SawBbv/bbv-utils.scm 192 */
												obj_t BgL_list1806z00_2346;

												{	/* SawBbv/bbv-utils.scm 192 */
													obj_t BgL_arg1808z00_2347;

													{	/* SawBbv/bbv-utils.scm 192 */
														obj_t BgL_arg1812z00_2348;

														{	/* SawBbv/bbv-utils.scm 192 */
															obj_t BgL_arg1820z00_2349;

															{	/* SawBbv/bbv-utils.scm 192 */
																obj_t BgL_arg1822z00_2350;

																{	/* SawBbv/bbv-utils.scm 192 */
																	obj_t BgL_arg1823z00_2351;

																	{	/* SawBbv/bbv-utils.scm 192 */
																		obj_t BgL_arg1831z00_2352;

																		{	/* SawBbv/bbv-utils.scm 192 */
																			obj_t BgL_arg1832z00_2353;

																			BgL_arg1832z00_2353 =
																				MAKE_YOUNG_PAIR(BINT
																				(BgL_arg1805z00_2345), BNIL);
																			BgL_arg1831z00_2352 =
																				MAKE_YOUNG_PAIR
																				(BGl_string2147z00zzsaw_bbvzd2utilszd2,
																				BgL_arg1832z00_2353);
																		}
																		BgL_arg1823z00_2351 =
																			MAKE_YOUNG_PAIR(BINT(BgL_arg1799z00_2344),
																			BgL_arg1831z00_2352);
																	}
																	BgL_arg1822z00_2350 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2148z00zzsaw_bbvzd2utilszd2,
																		BgL_arg1823z00_2351);
																}
																BgL_arg1820z00_2349 =
																	MAKE_YOUNG_PAIR
																	(BGl_string2149z00zzsaw_bbvzd2utilszd2,
																	BgL_arg1822z00_2350);
															}
															BgL_arg1812z00_2348 =
																MAKE_YOUNG_PAIR(BINT(192L),
																BgL_arg1820z00_2349);
														}
														BgL_arg1808z00_2347 =
															MAKE_YOUNG_PAIR
															(BGl_string2150z00zzsaw_bbvzd2utilszd2,
															BgL_arg1812z00_2348);
													}
													BgL_list1806z00_2346 =
														MAKE_YOUNG_PAIR
														(BGl_string2151z00zzsaw_bbvzd2utilszd2,
														BgL_arg1808z00_2347);
												}
												BGl_tprintz00zz__r4_output_6_10_3z00
													(BgL_arg1798z00_2343, BgL_list1806z00_2346);
										}}
										{	/* SawBbv/bbv-utils.scm 194 */
											obj_t BgL_arg1833z00_2354;
											obj_t BgL_arg1834z00_2355;

											{	/* SawBbv/bbv-utils.scm 194 */
												obj_t BgL_tmpz00_4546;

												BgL_tmpz00_4546 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_arg1833z00_2354 =
													BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_4546);
											}
											{	/* SawBbv/bbv-utils.scm 194 */
												BgL_bbvzd2ctxzd2_bglt BgL_arg1842z00_2362;

												{
													BgL_blocksz00_bglt BgL_auxz00_4549;

													{
														obj_t BgL_auxz00_4550;

														{	/* SawBbv/bbv-utils.scm 194 */
															BgL_objectz00_bglt BgL_tmpz00_4551;

															BgL_tmpz00_4551 =
																((BgL_objectz00_bglt) BgL_oldz00_102);
															BgL_auxz00_4550 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_4551);
														}
														BgL_auxz00_4549 =
															((BgL_blocksz00_bglt) BgL_auxz00_4550);
													}
													BgL_arg1842z00_2362 =
														(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4549))->
														BgL_ctxz00);
												}
												BgL_arg1834z00_2355 =
													BGl_shapez00zztools_shapez00(
													((obj_t) BgL_arg1842z00_2362));
											}
											{	/* SawBbv/bbv-utils.scm 194 */
												obj_t BgL_list1835z00_2356;

												{	/* SawBbv/bbv-utils.scm 194 */
													obj_t BgL_arg1836z00_2357;

													{	/* SawBbv/bbv-utils.scm 194 */
														obj_t BgL_arg1837z00_2358;

														{	/* SawBbv/bbv-utils.scm 194 */
															obj_t BgL_arg1838z00_2359;

															{	/* SawBbv/bbv-utils.scm 194 */
																obj_t BgL_arg1839z00_2360;

																{	/* SawBbv/bbv-utils.scm 194 */
																	obj_t BgL_arg1840z00_2361;

																	BgL_arg1840z00_2361 =
																		MAKE_YOUNG_PAIR(BgL_arg1834z00_2355, BNIL);
																	BgL_arg1839z00_2360 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2152z00zzsaw_bbvzd2utilszd2,
																		BgL_arg1840z00_2361);
																}
																BgL_arg1838z00_2359 =
																	MAKE_YOUNG_PAIR
																	(BGl_string2149z00zzsaw_bbvzd2utilszd2,
																	BgL_arg1839z00_2360);
															}
															BgL_arg1837z00_2358 =
																MAKE_YOUNG_PAIR(BINT(194L),
																BgL_arg1838z00_2359);
														}
														BgL_arg1836z00_2357 =
															MAKE_YOUNG_PAIR
															(BGl_string2150z00zzsaw_bbvzd2utilszd2,
															BgL_arg1837z00_2358);
													}
													BgL_list1835z00_2356 =
														MAKE_YOUNG_PAIR
														(BGl_string2151z00zzsaw_bbvzd2utilszd2,
														BgL_arg1836z00_2357);
												}
												BGl_tprintz00zz__r4_output_6_10_3z00
													(BgL_arg1833z00_2354, BgL_list1835z00_2356);
										}}
										{	/* SawBbv/bbv-utils.scm 195 */
											obj_t BgL_arg1843z00_2363;
											obj_t BgL_arg1844z00_2364;

											{	/* SawBbv/bbv-utils.scm 195 */
												obj_t BgL_tmpz00_4566;

												BgL_tmpz00_4566 = BGL_CURRENT_DYNAMIC_ENV();
												BgL_arg1843z00_2363 =
													BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_4566);
											}
											{	/* SawBbv/bbv-utils.scm 195 */
												BgL_bbvzd2ctxzd2_bglt BgL_arg1851z00_2371;

												{
													BgL_blocksz00_bglt BgL_auxz00_4569;

													{
														obj_t BgL_auxz00_4570;

														{	/* SawBbv/bbv-utils.scm 195 */
															BgL_objectz00_bglt BgL_tmpz00_4571;

															BgL_tmpz00_4571 =
																((BgL_objectz00_bglt) BgL_newz00_103);
															BgL_auxz00_4570 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_4571);
														}
														BgL_auxz00_4569 =
															((BgL_blocksz00_bglt) BgL_auxz00_4570);
													}
													BgL_arg1851z00_2371 =
														(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4569))->
														BgL_ctxz00);
												}
												BgL_arg1844z00_2364 =
													BGl_shapez00zztools_shapez00(
													((obj_t) BgL_arg1851z00_2371));
											}
											{	/* SawBbv/bbv-utils.scm 195 */
												obj_t BgL_list1845z00_2365;

												{	/* SawBbv/bbv-utils.scm 195 */
													obj_t BgL_arg1846z00_2366;

													{	/* SawBbv/bbv-utils.scm 195 */
														obj_t BgL_arg1847z00_2367;

														{	/* SawBbv/bbv-utils.scm 195 */
															obj_t BgL_arg1848z00_2368;

															{	/* SawBbv/bbv-utils.scm 195 */
																obj_t BgL_arg1849z00_2369;

																{	/* SawBbv/bbv-utils.scm 195 */
																	obj_t BgL_arg1850z00_2370;

																	BgL_arg1850z00_2370 =
																		MAKE_YOUNG_PAIR(BgL_arg1844z00_2364, BNIL);
																	BgL_arg1849z00_2369 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2153z00zzsaw_bbvzd2utilszd2,
																		BgL_arg1850z00_2370);
																}
																BgL_arg1848z00_2368 =
																	MAKE_YOUNG_PAIR
																	(BGl_string2149z00zzsaw_bbvzd2utilszd2,
																	BgL_arg1849z00_2369);
															}
															BgL_arg1847z00_2367 =
																MAKE_YOUNG_PAIR(BINT(195L),
																BgL_arg1848z00_2368);
														}
														BgL_arg1846z00_2366 =
															MAKE_YOUNG_PAIR
															(BGl_string2150z00zzsaw_bbvzd2utilszd2,
															BgL_arg1847z00_2367);
													}
													BgL_list1845z00_2365 =
														MAKE_YOUNG_PAIR
														(BGl_string2151z00zzsaw_bbvzd2utilszd2,
														BgL_arg1846z00_2366);
												}
												BGl_tprintz00zz__r4_output_6_10_3z00
													(BgL_arg1843z00_2363, BgL_list1845z00_2365);
										}}
										{	/* SawBbv/bbv-utils.scm 197 */
											obj_t BgL_arg1852z00_2372;
											int BgL_arg1853z00_2373;

											{	/* SawBbv/bbv-utils.scm 197 */
												int BgL_arg1854z00_2374;

												BgL_arg1854z00_2374 =
													(((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt) BgL_oldz00_102)))->
													BgL_labelz00);
												{	/* SawBbv/bbv-utils.scm 197 */
													obj_t BgL_list1855z00_2375;

													BgL_list1855z00_2375 =
														MAKE_YOUNG_PAIR(BINT(BgL_arg1854z00_2374), BNIL);
													BgL_arg1852z00_2372 =
														BGl_formatz00zz__r4_output_6_10_3z00
														(BGl_string2154z00zzsaw_bbvzd2utilszd2,
														BgL_list1855z00_2375);
											}}
											BgL_arg1853z00_2373 =
												(((BgL_blockz00_bglt) COBJECT(
														((BgL_blockz00_bglt) BgL_newz00_103)))->
												BgL_labelz00);
											BGl_errorz00zz__errorz00
												(BGl_string2155z00zzsaw_bbvzd2utilszd2,
												BgL_arg1852z00_2372, BINT(BgL_arg1853z00_2373));
							}}}
						else
							{	/* SawBbv/bbv-utils.scm 188 */
								BFALSE;
							}
					}
					{	/* SawBbv/bbv-utils.scm 201 */
						obj_t BgL_g1473z00_2379;

						BgL_g1473z00_2379 =
							(((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_oldz00_102)))->BgL_succsz00);
						{
							obj_t BgL_l1471z00_2381;

							BgL_l1471z00_2381 = BgL_g1473z00_2379;
						BgL_zc3z04anonymousza31858ze3z87_2382:
							if (PAIRP(BgL_l1471z00_2381))
								{	/* SawBbv/bbv-utils.scm 201 */
									{	/* SawBbv/bbv-utils.scm 202 */
										obj_t BgL_bz00_2384;

										BgL_bz00_2384 = CAR(BgL_l1471z00_2381);
										{	/* SawBbv/bbv-utils.scm 207 */
											obj_t BgL_arg1860z00_2386;

											{	/* SawBbv/bbv-utils.scm 207 */
												obj_t BgL_arg1863z00_2388;

												BgL_arg1863z00_2388 =
													(((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt)
																((BgL_blockz00_bglt) BgL_bz00_2384))))->
													BgL_predsz00);
												{	/* SawBbv/bbv-utils.scm 207 */
													obj_t BgL_zc3z04anonymousza31864ze3z87_3659;

													BgL_zc3z04anonymousza31864ze3z87_3659 =
														MAKE_FX_PROCEDURE
														(BGl_z62zc3z04anonymousza31864ze3ze5zzsaw_bbvzd2utilszd2,
														(int) (1L), (int) (1L));
													PROCEDURE_SET(BgL_zc3z04anonymousza31864ze3z87_3659,
														(int) (0L), ((obj_t) BgL_oldz00_102));
													BgL_arg1860z00_2386 =
														BGl_filterz12z12zz__r4_control_features_6_9z00
														(BgL_zc3z04anonymousza31864ze3z87_3659,
														BgL_arg1863z00_2388);
											}}
											BGl_blockzd2predszd2updatez12z12zzsaw_bbvzd2typeszd2(
												((BgL_blockz00_bglt) BgL_bz00_2384),
												BgL_arg1860z00_2386);
									}}
									{
										obj_t BgL_l1471z00_4612;

										BgL_l1471z00_4612 = CDR(BgL_l1471z00_2381);
										BgL_l1471z00_2381 = BgL_l1471z00_4612;
										goto BgL_zc3z04anonymousza31858ze3z87_2382;
									}
								}
							else
								{	/* SawBbv/bbv-utils.scm 201 */
									((bool_t) 1);
								}
						}
					}
					{	/* SawBbv/bbv-utils.scm 209 */
						obj_t BgL_g1479z00_2394;

						BgL_g1479z00_2394 =
							(((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_oldz00_102)))->BgL_predsz00);
						{
							obj_t BgL_l1477z00_2396;

							BgL_l1477z00_2396 = BgL_g1479z00_2394;
						BgL_zc3z04anonymousza31867ze3z87_2397:
							if (PAIRP(BgL_l1477z00_2396))
								{	/* SawBbv/bbv-utils.scm 209 */
									{	/* SawBbv/bbv-utils.scm 210 */
										obj_t BgL_bz00_2399;

										BgL_bz00_2399 = CAR(BgL_l1477z00_2396);
										{
											obj_t BgL_auxz00_4619;

											{	/* SawBbv/bbv-utils.scm 211 */
												obj_t BgL_arg1869z00_2401;

												BgL_arg1869z00_2401 =
													(((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt)
																((BgL_blockz00_bglt) BgL_bz00_2399))))->
													BgL_succsz00);
												BgL_auxz00_4619 =
													BGl_loopze70ze7zzsaw_bbvzd2utilszd2(((obj_t)
														BgL_oldz00_102), ((obj_t) BgL_newz00_103),
													BgL_arg1869z00_2401);
											}
											((((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt)
																((BgL_blockz00_bglt) BgL_bz00_2399))))->
													BgL_succsz00) = ((obj_t) BgL_auxz00_4619), BUNSPEC);
										}
										BGl_bbvzd2gczd2connectz12z12zzsaw_bbvzd2gczd2(
											((BgL_blockz00_bglt) BgL_bz00_2399), BgL_newz00_103);
										{	/* SawBbv/bbv-utils.scm 213 */
											obj_t BgL_g1476z00_2402;

											BgL_g1476z00_2402 =
												(((BgL_blockz00_bglt) COBJECT(
														((BgL_blockz00_bglt)
															((BgL_blockz00_bglt) BgL_bz00_2399))))->
												BgL_firstz00);
											{
												obj_t BgL_l1474z00_2404;

												BgL_l1474z00_2404 = BgL_g1476z00_2402;
											BgL_zc3z04anonymousza31870ze3z87_2405:
												if (PAIRP(BgL_l1474z00_2404))
													{	/* SawBbv/bbv-utils.scm 213 */
														{	/* SawBbv/bbv-utils.scm 215 */
															obj_t BgL_insz00_2407;

															BgL_insz00_2407 = CAR(BgL_l1474z00_2404);
															if (BGl_rtl_inszd2ifeqzf3z21zzsaw_bbvzd2typeszd2(
																	((BgL_rtl_insz00_bglt) BgL_insz00_2407)))
																{	/* SawBbv/bbv-utils.scm 217 */
																	BgL_rtl_ifeqz00_bglt BgL_i1198z00_2410;

																	BgL_i1198z00_2410 =
																		((BgL_rtl_ifeqz00_bglt)
																		(((BgL_rtl_insz00_bglt) COBJECT(
																					((BgL_rtl_insz00_bglt)
																						BgL_insz00_2407)))->BgL_funz00));
																	if ((((obj_t) (((BgL_rtl_ifeqz00_bglt)
																						COBJECT(BgL_i1198z00_2410))->
																					BgL_thenz00)) ==
																			((obj_t) BgL_oldz00_102)))
																		{	/* SawBbv/bbv-utils.scm 218 */
																			((((BgL_rtl_ifeqz00_bglt)
																						COBJECT(BgL_i1198z00_2410))->
																					BgL_thenz00) =
																				((BgL_blockz00_bglt) (
																						(BgL_blockz00_bglt)
																						BgL_newz00_103)), BUNSPEC);
																		}
																	else
																		{	/* SawBbv/bbv-utils.scm 218 */
																			BFALSE;
																		}
																}
															else
																{	/* SawBbv/bbv-utils.scm 215 */
																	if (BGl_rtl_inszd2ifnezf3z21zzsaw_bbvzd2typeszd2(((BgL_rtl_insz00_bglt) BgL_insz00_2407)))
																		{	/* SawBbv/bbv-utils.scm 222 */
																			BgL_rtl_ifnez00_bglt BgL_i1200z00_2416;

																			BgL_i1200z00_2416 =
																				((BgL_rtl_ifnez00_bglt)
																				(((BgL_rtl_insz00_bglt) COBJECT(
																							((BgL_rtl_insz00_bglt)
																								BgL_insz00_2407)))->
																					BgL_funz00));
																			if ((((obj_t) (((BgL_rtl_ifnez00_bglt)
																								COBJECT(BgL_i1200z00_2416))->
																							BgL_thenz00)) ==
																					((obj_t) BgL_oldz00_102)))
																				{	/* SawBbv/bbv-utils.scm 223 */
																					((((BgL_rtl_ifnez00_bglt)
																								COBJECT(BgL_i1200z00_2416))->
																							BgL_thenz00) =
																						((BgL_blockz00_bglt) (
																								(BgL_blockz00_bglt)
																								BgL_newz00_103)), BUNSPEC);
																				}
																			else
																				{	/* SawBbv/bbv-utils.scm 223 */
																					BFALSE;
																				}
																		}
																	else
																		{	/* SawBbv/bbv-utils.scm 220 */
																			if (BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2(((BgL_rtl_insz00_bglt) BgL_insz00_2407)))
																				{	/* SawBbv/bbv-utils.scm 227 */
																					BgL_rtl_goz00_bglt BgL_i1202z00_2422;

																					BgL_i1202z00_2422 =
																						((BgL_rtl_goz00_bglt)
																						(((BgL_rtl_insz00_bglt) COBJECT(
																									((BgL_rtl_insz00_bglt)
																										BgL_insz00_2407)))->
																							BgL_funz00));
																					if ((((obj_t) (((BgL_rtl_goz00_bglt)
																										COBJECT
																										(BgL_i1202z00_2422))->
																									BgL_toz00)) ==
																							((obj_t) BgL_oldz00_102)))
																						{	/* SawBbv/bbv-utils.scm 228 */
																							((((BgL_rtl_goz00_bglt)
																										COBJECT
																										(BgL_i1202z00_2422))->
																									BgL_toz00) =
																								((BgL_blockz00_bglt) (
																										(BgL_blockz00_bglt)
																										BgL_newz00_103)), BUNSPEC);
																						}
																					else
																						{	/* SawBbv/bbv-utils.scm 228 */
																							BFALSE;
																						}
																				}
																			else
																				{	/* SawBbv/bbv-utils.scm 225 */
																					if (CBOOL
																						(BGl_rtl_inszd2switchzf3z21zzsaw_bbvzd2typeszd2
																							(((BgL_rtl_insz00_bglt)
																									BgL_insz00_2407))))
																						{	/* SawBbv/bbv-utils.scm 232 */
																							BgL_rtl_switchz00_bglt
																								BgL_i1204z00_2428;
																							BgL_i1204z00_2428 =
																								((BgL_rtl_switchz00_bglt) ((
																										(BgL_rtl_insz00_bglt)
																										COBJECT((
																												(BgL_rtl_insz00_bglt)
																												BgL_insz00_2407)))->
																									BgL_funz00));
																							{
																								obj_t BgL_auxz00_4683;

																								{	/* SawBbv/bbv-utils.scm 233 */
																									obj_t BgL_arg1887z00_2429;

																									BgL_arg1887z00_2429 =
																										(((BgL_rtl_switchz00_bglt)
																											COBJECT
																											(BgL_i1204z00_2428))->
																										BgL_labelsz00);
																									BgL_auxz00_4683 =
																										BGl_loopze70ze7zzsaw_bbvzd2utilszd2
																										(((obj_t) BgL_oldz00_102),
																										((obj_t) BgL_newz00_103),
																										((obj_t)
																											BgL_arg1887z00_2429));
																								}
																								((((BgL_rtl_switchz00_bglt)
																											COBJECT
																											(BgL_i1204z00_2428))->
																										BgL_labelsz00) =
																									((obj_t) BgL_auxz00_4683),
																									BUNSPEC);
																							}
																						}
																					else
																						{	/* SawBbv/bbv-utils.scm 230 */
																							BFALSE;
																						}
																				}
																		}
																}
														}
														{
															obj_t BgL_l1474z00_4690;

															BgL_l1474z00_4690 = CDR(BgL_l1474z00_2404);
															BgL_l1474z00_2404 = BgL_l1474z00_4690;
															goto BgL_zc3z04anonymousza31870ze3z87_2405;
														}
													}
												else
													{	/* SawBbv/bbv-utils.scm 213 */
														((bool_t) 1);
													}
											}
										}
									}
									{
										obj_t BgL_l1477z00_4692;

										BgL_l1477z00_4692 = CDR(BgL_l1477z00_2396);
										BgL_l1477z00_2396 = BgL_l1477z00_4692;
										goto BgL_zc3z04anonymousza31867ze3z87_2397;
									}
								}
							else
								{	/* SawBbv/bbv-utils.scm 209 */
									((bool_t) 1);
								}
						}
					}
					{	/* SawBbv/bbv-utils.scm 238 */
						obj_t BgL_arg1890z00_2435;

						{	/* SawBbv/bbv-utils.scm 238 */
							obj_t BgL_arg1891z00_2436;

							{	/* SawBbv/bbv-utils.scm 238 */
								obj_t BgL_arg1892z00_2437;
								obj_t BgL_arg1893z00_2438;

								BgL_arg1892z00_2437 =
									(((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt) BgL_oldz00_102)))->BgL_predsz00);
								BgL_arg1893z00_2438 =
									(((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt) BgL_newz00_103)))->BgL_predsz00);
								BgL_arg1891z00_2436 =
									BGl_appendzd221011zd2zzsaw_bbvzd2utilszd2(BgL_arg1892z00_2437,
									BgL_arg1893z00_2438);
							}
							BgL_arg1890z00_2435 =
								BGl_deletezd2duplicateszd2zz__r4_pairs_and_lists_6_3z00
								(BgL_arg1891z00_2436,
								BGl_eqzf3zd2envz21zz__r4_equivalence_6_2z00);
						}
						BGl_blockzd2predszd2updatez12z12zzsaw_bbvzd2typeszd2(
							((BgL_blockz00_bglt) BgL_newz00_103), BgL_arg1890z00_2435);
					}
					{
						BgL_blocksz00_bglt BgL_auxz00_4702;

						{
							obj_t BgL_auxz00_4703;

							{	/* SawBbv/bbv-utils.scm 240 */
								BgL_objectz00_bglt BgL_tmpz00_4704;

								BgL_tmpz00_4704 = ((BgL_objectz00_bglt) BgL_oldz00_102);
								BgL_auxz00_4703 = BGL_OBJECT_WIDENING(BgL_tmpz00_4704);
							}
							BgL_auxz00_4702 = ((BgL_blocksz00_bglt) BgL_auxz00_4703);
						}
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4702))->BgL_mblockz00) =
							((obj_t) ((obj_t) BgL_newz00_103)), BUNSPEC);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_oldz00_102)))->BgL_predsz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_oldz00_102)))->BgL_succsz00) = ((obj_t) BNIL), BUNSPEC);
					{
						BgL_blocksz00_bglt BgL_auxz00_4714;

						{
							obj_t BgL_auxz00_4715;

							{	/* SawBbv/bbv-utils.scm 243 */
								BgL_objectz00_bglt BgL_tmpz00_4716;

								BgL_tmpz00_4716 = ((BgL_objectz00_bglt) BgL_oldz00_102);
								BgL_auxz00_4715 = BGL_OBJECT_WIDENING(BgL_tmpz00_4716);
							}
							BgL_auxz00_4714 = ((BgL_blocksz00_bglt) BgL_auxz00_4715);
						}
						((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4714))->BgL_gccntz00) =
							((long) 0L), BUNSPEC);
					}
					BGl_bbvzd2gczd2redirectz12z12zzsaw_bbvzd2gczd2(BgL_oldz00_102,
						BgL_newz00_103);
					if (CBOOL(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2))
						{	/* SawBbv/bbv-utils.scm 245 */
							BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(BgL_oldz00_102,
								BGl_string2156z00zzsaw_bbvzd2utilszd2);
							BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(BgL_newz00_103,
								BGl_string2157z00zzsaw_bbvzd2utilszd2);
						}
					else
						{	/* SawBbv/bbv-utils.scm 245 */
							BFALSE;
						}
					return ((obj_t) BgL_newz00_103);
				}
		}

	}



/* &<@anonymous:1864> */
	obj_t BGl_z62zc3z04anonymousza31864ze3ze5zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3660, obj_t BgL_nz00_3662)
	{
		{	/* SawBbv/bbv-utils.scm 207 */
			{	/* SawBbv/bbv-utils.scm 207 */
				bool_t BgL_tmpz00_4727;

				if (
					(BgL_nz00_3662 ==
						((obj_t)
							((BgL_blockz00_bglt)
								PROCEDURE_REF(BgL_envz00_3660, (int) (0L))))))
					{	/* SawBbv/bbv-utils.scm 207 */
						BgL_tmpz00_4727 = ((bool_t) 0);
					}
				else
					{	/* SawBbv/bbv-utils.scm 207 */
						BgL_tmpz00_4727 = ((bool_t) 1);
					}
				return BBOOL(BgL_tmpz00_4727);
			}
		}

	}



/* ctx>=? */
	bool_t BGl_ctxze3zd3zf3zc3zzsaw_bbvzd2utilszd2(BgL_bbvzd2ctxzd2_bglt
		BgL_xz00_107, BgL_bbvzd2ctxzd2_bglt BgL_yz00_108)
	{
		{	/* SawBbv/bbv-utils.scm 262 */
			{
				obj_t BgL_xz00_2454;
				obj_t BgL_yz00_2455;

				{	/* SawBbv/bbv-utils.scm 291 */
					obj_t BgL_g1488z00_2441;

					BgL_g1488z00_2441 =
						(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_xz00_107))->BgL_entriesz00);
					{
						obj_t BgL_l1486z00_2443;

						BgL_l1486z00_2443 = BgL_g1488z00_2441;
					BgL_zc3z04anonymousza31894ze3z87_2444:
						if (NULLP(BgL_l1486z00_2443))
							{	/* SawBbv/bbv-utils.scm 291 */
								return ((bool_t) 1);
							}
						else
							{	/* SawBbv/bbv-utils.scm 291 */
								bool_t BgL_test2285z00_4738;

								{	/* SawBbv/bbv-utils.scm 292 */
									obj_t BgL_exz00_2449;

									BgL_exz00_2449 = CAR(((obj_t) BgL_l1486z00_2443));
									{	/* SawBbv/bbv-utils.scm 293 */
										obj_t BgL_eyz00_2451;

										{	/* SawBbv/bbv-utils.scm 293 */
											BgL_rtl_regz00_bglt BgL_arg1897z00_2452;

											BgL_arg1897z00_2452 =
												(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
														((BgL_bbvzd2ctxentryzd2_bglt) BgL_exz00_2449)))->
												BgL_regz00);
											BgL_eyz00_2451 =
												BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2(BgL_yz00_108,
												BgL_arg1897z00_2452);
										}
										BgL_xz00_2454 = BgL_exz00_2449;
										BgL_yz00_2455 = BgL_eyz00_2451;
										{	/* SawBbv/bbv-utils.scm 272 */
											bool_t BgL_test2286z00_4744;

											{	/* SawBbv/bbv-utils.scm 272 */
												obj_t BgL_arg1941z00_2518;

												BgL_arg1941z00_2518 =
													(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
															((BgL_bbvzd2ctxentryzd2_bglt) BgL_xz00_2454)))->
													BgL_typesz00);
												BgL_test2286z00_4744 =
													CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
													(BGl_za2objza2z00zztype_cachez00,
														BgL_arg1941z00_2518));
											}
											if (BgL_test2286z00_4744)
												{	/* SawBbv/bbv-utils.scm 272 */
													BgL_test2285z00_4738 = ((bool_t) 1);
												}
											else
												{	/* SawBbv/bbv-utils.scm 274 */
													bool_t BgL_test2287z00_4749;

													{	/* SawBbv/bbv-utils.scm 274 */
														obj_t BgL_arg1940z00_2517;

														BgL_arg1940z00_2517 =
															(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
																	((BgL_bbvzd2ctxentryzd2_bglt)
																		BgL_yz00_2455)))->BgL_typesz00);
														BgL_test2287z00_4749 =
															CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
															(BGl_za2objza2z00zztype_cachez00,
																BgL_arg1940z00_2517));
													}
													if (BgL_test2287z00_4749)
														{	/* SawBbv/bbv-utils.scm 274 */
															BgL_test2285z00_4738 = ((bool_t) 0);
														}
													else
														{	/* SawBbv/bbv-utils.scm 274 */
															if (
																(BBOOL(
																		(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
																					((BgL_bbvzd2ctxentryzd2_bglt)
																						BgL_xz00_2454)))->
																			BgL_polarityz00)) ==
																	BBOOL((((BgL_bbvzd2ctxentryzd2_bglt)
																				COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																						BgL_yz00_2455)))->
																			BgL_polarityz00))))
																{	/* SawBbv/bbv-utils.scm 278 */
																	bool_t BgL_test2289z00_4762;

																	{
																		obj_t BgL_l1480z00_2506;

																		BgL_l1480z00_2506 =
																			(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
																					((BgL_bbvzd2ctxentryzd2_bglt)
																						BgL_xz00_2454)))->BgL_typesz00);
																	BgL_zc3z04anonymousza31935ze3z87_2507:
																		if (NULLP(BgL_l1480z00_2506))
																			{	/* SawBbv/bbv-utils.scm 278 */
																				BgL_test2289z00_4762 = ((bool_t) 1);
																			}
																		else
																			{	/* SawBbv/bbv-utils.scm 278 */
																				if (CBOOL
																					(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																						(CAR(((obj_t) BgL_l1480z00_2506)),
																							(((BgL_bbvzd2ctxentryzd2_bglt)
																									COBJECT((
																											(BgL_bbvzd2ctxentryzd2_bglt)
																											BgL_yz00_2455)))->
																								BgL_typesz00))))
																					{
																						obj_t BgL_l1480z00_4772;

																						BgL_l1480z00_4772 =
																							CDR(((obj_t) BgL_l1480z00_2506));
																						BgL_l1480z00_2506 =
																							BgL_l1480z00_4772;
																						goto
																							BgL_zc3z04anonymousza31935ze3z87_2507;
																					}
																				else
																					{	/* SawBbv/bbv-utils.scm 278 */
																						BgL_test2289z00_4762 = ((bool_t) 0);
																					}
																			}
																	}
																	if (BgL_test2289z00_4762)
																		{	/* SawBbv/bbv-utils.scm 280 */
																			bool_t BgL_test2292z00_4777;

																			{	/* SawBbv/bbv-utils.scm 280 */
																				obj_t BgL_arg1934z00_2503;

																				BgL_arg1934z00_2503 =
																					(((BgL_bbvzd2ctxentryzd2_bglt)
																						COBJECT((
																								(BgL_bbvzd2ctxentryzd2_bglt)
																								BgL_xz00_2454)))->BgL_valuez00);
																				{	/* SawBbv/bbv-utils.scm 280 */
																					obj_t BgL_classz00_3177;

																					BgL_classz00_3177 =
																						BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2;
																					if (BGL_OBJECTP(BgL_arg1934z00_2503))
																						{	/* SawBbv/bbv-utils.scm 280 */
																							BgL_objectz00_bglt
																								BgL_arg1807z00_3179;
																							BgL_arg1807z00_3179 =
																								(BgL_objectz00_bglt)
																								(BgL_arg1934z00_2503);
																							if (BGL_CONDEXPAND_ISA_ARCH64())
																								{	/* SawBbv/bbv-utils.scm 280 */
																									long BgL_idxz00_3185;

																									BgL_idxz00_3185 =
																										BGL_OBJECT_INHERITANCE_NUM
																										(BgL_arg1807z00_3179);
																									BgL_test2292z00_4777 =
																										(VECTOR_REF
																										(BGl_za2inheritancesza2z00zz__objectz00,
																											(BgL_idxz00_3185 + 1L)) ==
																										BgL_classz00_3177);
																								}
																							else
																								{	/* SawBbv/bbv-utils.scm 280 */
																									bool_t BgL_res2142z00_3210;

																									{	/* SawBbv/bbv-utils.scm 280 */
																										obj_t BgL_oclassz00_3193;

																										{	/* SawBbv/bbv-utils.scm 280 */
																											obj_t BgL_arg1815z00_3201;
																											long BgL_arg1816z00_3202;

																											BgL_arg1815z00_3201 =
																												(BGl_za2classesza2z00zz__objectz00);
																											{	/* SawBbv/bbv-utils.scm 280 */
																												long
																													BgL_arg1817z00_3203;
																												BgL_arg1817z00_3203 =
																													BGL_OBJECT_CLASS_NUM
																													(BgL_arg1807z00_3179);
																												BgL_arg1816z00_3202 =
																													(BgL_arg1817z00_3203 -
																													OBJECT_TYPE);
																											}
																											BgL_oclassz00_3193 =
																												VECTOR_REF
																												(BgL_arg1815z00_3201,
																												BgL_arg1816z00_3202);
																										}
																										{	/* SawBbv/bbv-utils.scm 280 */
																											bool_t
																												BgL__ortest_1115z00_3194;
																											BgL__ortest_1115z00_3194 =
																												(BgL_classz00_3177 ==
																												BgL_oclassz00_3193);
																											if (BgL__ortest_1115z00_3194)
																												{	/* SawBbv/bbv-utils.scm 280 */
																													BgL_res2142z00_3210 =
																														BgL__ortest_1115z00_3194;
																												}
																											else
																												{	/* SawBbv/bbv-utils.scm 280 */
																													long
																														BgL_odepthz00_3195;
																													{	/* SawBbv/bbv-utils.scm 280 */
																														obj_t
																															BgL_arg1804z00_3196;
																														BgL_arg1804z00_3196
																															=
																															(BgL_oclassz00_3193);
																														BgL_odepthz00_3195 =
																															BGL_CLASS_DEPTH
																															(BgL_arg1804z00_3196);
																													}
																													if (
																														(1L <
																															BgL_odepthz00_3195))
																														{	/* SawBbv/bbv-utils.scm 280 */
																															obj_t
																																BgL_arg1802z00_3198;
																															{	/* SawBbv/bbv-utils.scm 280 */
																																obj_t
																																	BgL_arg1803z00_3199;
																																BgL_arg1803z00_3199
																																	=
																																	(BgL_oclassz00_3193);
																																BgL_arg1802z00_3198
																																	=
																																	BGL_CLASS_ANCESTORS_REF
																																	(BgL_arg1803z00_3199,
																																	1L);
																															}
																															BgL_res2142z00_3210
																																=
																																(BgL_arg1802z00_3198
																																==
																																BgL_classz00_3177);
																														}
																													else
																														{	/* SawBbv/bbv-utils.scm 280 */
																															BgL_res2142z00_3210
																																= ((bool_t) 0);
																														}
																												}
																										}
																									}
																									BgL_test2292z00_4777 =
																										BgL_res2142z00_3210;
																								}
																						}
																					else
																						{	/* SawBbv/bbv-utils.scm 280 */
																							BgL_test2292z00_4777 =
																								((bool_t) 0);
																						}
																				}
																			}
																			if (BgL_test2292z00_4777)
																				{	/* SawBbv/bbv-utils.scm 281 */
																					bool_t BgL_test2297z00_4802;

																					{	/* SawBbv/bbv-utils.scm 281 */
																						obj_t BgL_arg1933z00_2502;

																						BgL_arg1933z00_2502 =
																							(((BgL_bbvzd2ctxentryzd2_bglt)
																								COBJECT((
																										(BgL_bbvzd2ctxentryzd2_bglt)
																										BgL_yz00_2455)))->
																							BgL_valuez00);
																						{	/* SawBbv/bbv-utils.scm 281 */
																							obj_t BgL_classz00_3211;

																							BgL_classz00_3211 =
																								BGl_bbvzd2rangezd2zzsaw_bbvzd2rangezd2;
																							if (BGL_OBJECTP
																								(BgL_arg1933z00_2502))
																								{	/* SawBbv/bbv-utils.scm 281 */
																									BgL_objectz00_bglt
																										BgL_arg1807z00_3213;
																									BgL_arg1807z00_3213 =
																										(BgL_objectz00_bglt)
																										(BgL_arg1933z00_2502);
																									if (BGL_CONDEXPAND_ISA_ARCH64
																										())
																										{	/* SawBbv/bbv-utils.scm 281 */
																											long BgL_idxz00_3219;

																											BgL_idxz00_3219 =
																												BGL_OBJECT_INHERITANCE_NUM
																												(BgL_arg1807z00_3213);
																											BgL_test2297z00_4802 =
																												(VECTOR_REF
																												(BGl_za2inheritancesza2z00zz__objectz00,
																													(BgL_idxz00_3219 +
																														1L)) ==
																												BgL_classz00_3211);
																										}
																									else
																										{	/* SawBbv/bbv-utils.scm 281 */
																											bool_t
																												BgL_res2143z00_3244;
																											{	/* SawBbv/bbv-utils.scm 281 */
																												obj_t
																													BgL_oclassz00_3227;
																												{	/* SawBbv/bbv-utils.scm 281 */
																													obj_t
																														BgL_arg1815z00_3235;
																													long
																														BgL_arg1816z00_3236;
																													BgL_arg1815z00_3235 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* SawBbv/bbv-utils.scm 281 */
																														long
																															BgL_arg1817z00_3237;
																														BgL_arg1817z00_3237
																															=
																															BGL_OBJECT_CLASS_NUM
																															(BgL_arg1807z00_3213);
																														BgL_arg1816z00_3236
																															=
																															(BgL_arg1817z00_3237
																															- OBJECT_TYPE);
																													}
																													BgL_oclassz00_3227 =
																														VECTOR_REF
																														(BgL_arg1815z00_3235,
																														BgL_arg1816z00_3236);
																												}
																												{	/* SawBbv/bbv-utils.scm 281 */
																													bool_t
																														BgL__ortest_1115z00_3228;
																													BgL__ortest_1115z00_3228
																														=
																														(BgL_classz00_3211
																														==
																														BgL_oclassz00_3227);
																													if (BgL__ortest_1115z00_3228)
																														{	/* SawBbv/bbv-utils.scm 281 */
																															BgL_res2143z00_3244
																																=
																																BgL__ortest_1115z00_3228;
																														}
																													else
																														{	/* SawBbv/bbv-utils.scm 281 */
																															long
																																BgL_odepthz00_3229;
																															{	/* SawBbv/bbv-utils.scm 281 */
																																obj_t
																																	BgL_arg1804z00_3230;
																																BgL_arg1804z00_3230
																																	=
																																	(BgL_oclassz00_3227);
																																BgL_odepthz00_3229
																																	=
																																	BGL_CLASS_DEPTH
																																	(BgL_arg1804z00_3230);
																															}
																															if (
																																(1L <
																																	BgL_odepthz00_3229))
																																{	/* SawBbv/bbv-utils.scm 281 */
																																	obj_t
																																		BgL_arg1802z00_3232;
																																	{	/* SawBbv/bbv-utils.scm 281 */
																																		obj_t
																																			BgL_arg1803z00_3233;
																																		BgL_arg1803z00_3233
																																			=
																																			(BgL_oclassz00_3227);
																																		BgL_arg1802z00_3232
																																			=
																																			BGL_CLASS_ANCESTORS_REF
																																			(BgL_arg1803z00_3233,
																																			1L);
																																	}
																																	BgL_res2143z00_3244
																																		=
																																		(BgL_arg1802z00_3232
																																		==
																																		BgL_classz00_3211);
																																}
																															else
																																{	/* SawBbv/bbv-utils.scm 281 */
																																	BgL_res2143z00_3244
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																											}
																											BgL_test2297z00_4802 =
																												BgL_res2143z00_3244;
																										}
																								}
																							else
																								{	/* SawBbv/bbv-utils.scm 281 */
																									BgL_test2297z00_4802 =
																										((bool_t) 0);
																								}
																						}
																					}
																					if (BgL_test2297z00_4802)
																						{	/* SawBbv/bbv-utils.scm 282 */
																							BgL_bbvzd2rangezd2_bglt
																								BgL_i1208z00_2482;
																							BgL_i1208z00_2482 =
																								((BgL_bbvzd2rangezd2_bglt) ((
																										(BgL_bbvzd2ctxentryzd2_bglt)
																										COBJECT((
																												(BgL_bbvzd2ctxentryzd2_bglt)
																												BgL_xz00_2454)))->
																									BgL_valuez00));
																							{	/* SawBbv/bbv-utils.scm 283 */
																								BgL_bbvzd2rangezd2_bglt
																									BgL_i1209z00_2483;
																								BgL_i1209z00_2483 =
																									((BgL_bbvzd2rangezd2_bglt) ((
																											(BgL_bbvzd2ctxentryzd2_bglt)
																											COBJECT((
																													(BgL_bbvzd2ctxentryzd2_bglt)
																													BgL_yz00_2455)))->
																										BgL_valuez00));
																								{	/* SawBbv/bbv-utils.scm 284 */
																									bool_t BgL_rz00_2484;

																									if (
																										((long) CINT(
																												(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_i1208z00_2482))->BgL_loz00)) <= (long) CINT((((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_i1209z00_2483))->BgL_loz00))))
																										{	/* SawBbv/bbv-utils.scm 284 */
																											obj_t BgL_a1484z00_2497;

																											BgL_a1484z00_2497 =
																												(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_i1208z00_2482))->BgL_upz00);
																											{	/* SawBbv/bbv-utils.scm 284 */
																												obj_t BgL_b1485z00_2498;

																												BgL_b1485z00_2498 =
																													(((BgL_bbvzd2rangezd2_bglt) COBJECT(BgL_i1209z00_2483))->BgL_upz00);
																												{	/* SawBbv/bbv-utils.scm 284 */

																													{	/* SawBbv/bbv-utils.scm 284 */
																														bool_t
																															BgL_test2303z00_4841;
																														if (INTEGERP
																															(BgL_a1484z00_2497))
																															{	/* SawBbv/bbv-utils.scm 284 */
																																BgL_test2303z00_4841
																																	=
																																	INTEGERP
																																	(BgL_b1485z00_2498);
																															}
																														else
																															{	/* SawBbv/bbv-utils.scm 284 */
																																BgL_test2303z00_4841
																																	=
																																	((bool_t) 0);
																															}
																														if (BgL_test2303z00_4841)
																															{	/* SawBbv/bbv-utils.scm 284 */
																																BgL_rz00_2484 =
																																	(
																																	(long)
																																	CINT
																																	(BgL_a1484z00_2497)
																																	>=
																																	(long)
																																	CINT
																																	(BgL_b1485z00_2498));
																															}
																														else
																															{	/* SawBbv/bbv-utils.scm 284 */
																																BgL_rz00_2484 =
																																	BGl_2ze3zd3z30zz__r4_numbers_6_5z00
																																	(BgL_a1484z00_2497,
																																	BgL_b1485z00_2498);
																															}
																													}
																												}
																											}
																										}
																									else
																										{	/* SawBbv/bbv-utils.scm 284 */
																											BgL_rz00_2484 =
																												((bool_t) 0);
																										}
																									if (BgL_rz00_2484)
																										{	/* SawBbv/bbv-utils.scm 285 */
																											BFALSE;
																										}
																									else
																										{	/* SawBbv/bbv-utils.scm 286 */
																											obj_t BgL_arg1918z00_2485;
																											obj_t BgL_arg1919z00_2486;
																											obj_t BgL_arg1920z00_2487;

																											{	/* SawBbv/bbv-utils.scm 286 */
																												obj_t BgL_tmpz00_4850;

																												BgL_tmpz00_4850 =
																													BGL_CURRENT_DYNAMIC_ENV
																													();
																												BgL_arg1918z00_2485 =
																													BGL_ENV_CURRENT_ERROR_PORT
																													(BgL_tmpz00_4850);
																											}
																											BgL_arg1919z00_2486 =
																												BGl_shapez00zztools_shapez00
																												(BgL_xz00_2454);
																											BgL_arg1920z00_2487 =
																												BGl_shapez00zztools_shapez00
																												(BgL_yz00_2455);
																											{	/* SawBbv/bbv-utils.scm 286 */
																												obj_t
																													BgL_list1921z00_2488;
																												{	/* SawBbv/bbv-utils.scm 286 */
																													obj_t
																														BgL_arg1923z00_2489;
																													{	/* SawBbv/bbv-utils.scm 286 */
																														obj_t
																															BgL_arg1924z00_2490;
																														{	/* SawBbv/bbv-utils.scm 286 */
																															obj_t
																																BgL_arg1925z00_2491;
																															{	/* SawBbv/bbv-utils.scm 286 */
																																obj_t
																																	BgL_arg1926z00_2492;
																																{	/* SawBbv/bbv-utils.scm 286 */
																																	obj_t
																																		BgL_arg1927z00_2493;
																																	{	/* SawBbv/bbv-utils.scm 286 */
																																		obj_t
																																			BgL_arg1928z00_2494;
																																		{	/* SawBbv/bbv-utils.scm 286 */
																																			obj_t
																																				BgL_arg1929z00_2495;
																																			BgL_arg1929z00_2495
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_arg1920z00_2487,
																																				BNIL);
																																			BgL_arg1928z00_2494
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BGl_string2158z00zzsaw_bbvzd2utilszd2,
																																				BgL_arg1929z00_2495);
																																		}
																																		BgL_arg1927z00_2493
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_arg1919z00_2486,
																																			BgL_arg1928z00_2494);
																																	}
																																	BgL_arg1926z00_2492
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BGl_string2159z00zzsaw_bbvzd2utilszd2,
																																		BgL_arg1927z00_2493);
																																}
																																BgL_arg1925z00_2491
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BGl_string2149z00zzsaw_bbvzd2utilszd2,
																																	BgL_arg1926z00_2492);
																															}
																															BgL_arg1924z00_2490
																																=
																																MAKE_YOUNG_PAIR
																																(BINT(286L),
																																BgL_arg1925z00_2491);
																														}
																														BgL_arg1923z00_2489
																															=
																															MAKE_YOUNG_PAIR
																															(BGl_string2150z00zzsaw_bbvzd2utilszd2,
																															BgL_arg1924z00_2490);
																													}
																													BgL_list1921z00_2488 =
																														MAKE_YOUNG_PAIR
																														(BGl_string2151z00zzsaw_bbvzd2utilszd2,
																														BgL_arg1923z00_2489);
																												}
																												BGl_tprintz00zz__r4_output_6_10_3z00
																													(BgL_arg1918z00_2485,
																													BgL_list1921z00_2488);
																											}
																										}
																									BgL_test2285z00_4738 =
																										BgL_rz00_2484;
																								}
																							}
																						}
																					else
																						{	/* SawBbv/bbv-utils.scm 281 */
																							BgL_test2285z00_4738 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* SawBbv/bbv-utils.scm 280 */
																					BgL_test2285z00_4738 = ((bool_t) 1);
																				}
																		}
																	else
																		{	/* SawBbv/bbv-utils.scm 278 */
																			BgL_test2285z00_4738 = ((bool_t) 0);
																		}
																}
															else
																{	/* SawBbv/bbv-utils.scm 276 */
																	BgL_test2285z00_4738 = ((bool_t) 0);
																}
														}
												}
										}
									}
								}
								if (BgL_test2285z00_4738)
									{	/* SawBbv/bbv-utils.scm 291 */
										obj_t BgL_arg1896z00_2448;

										BgL_arg1896z00_2448 = CDR(((obj_t) BgL_l1486z00_2443));
										{
											obj_t BgL_l1486z00_4867;

											BgL_l1486z00_4867 = BgL_arg1896z00_2448;
											BgL_l1486z00_2443 = BgL_l1486z00_4867;
											goto BgL_zc3z04anonymousza31894ze3z87_2444;
										}
									}
								else
									{	/* SawBbv/bbv-utils.scm 291 */
										return ((bool_t) 0);
									}
							}
					}
				}
			}
		}

	}



/* bbv-ctx-filter-live-in-regs */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_bbvzd2ctxzd2filterzd2livezd2inzd2regszd2zzsaw_bbvzd2utilszd2
		(BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_109, BgL_rtl_insz00_bglt BgL_insz00_110)
	{
		{	/* SawBbv/bbv-utils.scm 302 */
			{	/* SawBbv/bbv-utils.scm 302 */
				obj_t BgL_filteredz00_3676;

				BgL_filteredz00_3676 = MAKE_CELL(BFALSE);
				{
					BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_2528;
					BgL_rtl_insz00_bglt BgL_insz00_2529;

					{	/* SawBbv/bbv-utils.scm 327 */
						obj_t BgL_entriesz00_2522;

						BgL_ctxz00_2528 = BgL_ctxz00_109;
						BgL_insz00_2529 = BgL_insz00_110;
						{	/* SawBbv/bbv-utils.scm 308 */
							obj_t BgL_arg1946z00_2533;

							BgL_arg1946z00_2533 =
								(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_2528))->
								BgL_entriesz00);
							{	/* SawBbv/bbv-utils.scm 309 */
								obj_t BgL_zc3z04anonymousza31948ze3z87_3666;

								BgL_zc3z04anonymousza31948ze3z87_3666 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31948ze3ze5zzsaw_bbvzd2utilszd2,
									(int) (1L), (int) (2L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31948ze3z87_3666, (int) (0L),
									((obj_t) BgL_filteredz00_3676));
								PROCEDURE_SET(BgL_zc3z04anonymousza31948ze3z87_3666, (int) (1L),
									((obj_t) BgL_insz00_2529));
								{	/* SawBbv/bbv-utils.scm 308 */
									obj_t BgL_list1947z00_2534;

									BgL_list1947z00_2534 =
										MAKE_YOUNG_PAIR(BgL_arg1946z00_2533, BNIL);
									BgL_entriesz00_2522 =
										BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
										(BgL_zc3z04anonymousza31948ze3z87_3666,
										BgL_list1947z00_2534);
						}}}
						if (CBOOL(CELL_REF(BgL_filteredz00_3676)))
							{	/* SawBbv/bbv-utils.scm 329 */
								BgL_bbvzd2ctxzd2_bglt BgL_new1218z00_2524;

								{	/* SawBbv/bbv-utils.scm 329 */
									BgL_bbvzd2ctxzd2_bglt BgL_new1221z00_2526;

									BgL_new1221z00_2526 =
										((BgL_bbvzd2ctxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_bbvzd2ctxzd2_bgl))));
									{	/* SawBbv/bbv-utils.scm 329 */
										long BgL_arg1943z00_2527;

										BgL_arg1943z00_2527 =
											BGL_CLASS_NUM(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1221z00_2526),
											BgL_arg1943z00_2527);
									}
									BgL_new1218z00_2524 = BgL_new1221z00_2526;
								}
								((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1218z00_2524))->
										BgL_idz00) =
									((long) (((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_109))->
											BgL_idz00)), BUNSPEC);
								((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1218z00_2524))->
										BgL_entriesz00) = ((obj_t) BgL_entriesz00_2522), BUNSPEC);
								{	/* SawBbv/bbv-utils.scm 330 */
									obj_t BgL_fun1942z00_2525;

									BgL_fun1942z00_2525 =
										BGl_classzd2constructorzd2zz__objectz00
										(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
									BGL_PROCEDURE_CALL1(BgL_fun1942z00_2525,
										((obj_t) BgL_new1218z00_2524));
								}
								return BgL_new1218z00_2524;
							}
						else
							{	/* SawBbv/bbv-utils.scm 328 */
								return BgL_ctxz00_109;
							}
					}
				}
			}
		}

	}



/* &bbv-ctx-filter-live-in-regs */
	BgL_bbvzd2ctxzd2_bglt
		BGl_z62bbvzd2ctxzd2filterzd2livezd2inzd2regszb0zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3667, obj_t BgL_ctxz00_3668, obj_t BgL_insz00_3669)
	{
		{	/* SawBbv/bbv-utils.scm 302 */
			return
				BGl_bbvzd2ctxzd2filterzd2livezd2inzd2regszd2zzsaw_bbvzd2utilszd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctxz00_3668),
				((BgL_rtl_insz00_bglt) BgL_insz00_3669));
		}

	}



/* &<@anonymous:1948> */
	obj_t BGl_z62zc3z04anonymousza31948ze3ze5zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3670, obj_t BgL_ez00_3673)
	{
		{	/* SawBbv/bbv-utils.scm 308 */
			{	/* SawBbv/bbv-utils.scm 309 */
				obj_t BgL_filteredz00_3671;
				BgL_rtl_insz00_bglt BgL_i1213z00_3672;

				BgL_filteredz00_3671 = PROCEDURE_REF(BgL_envz00_3670, (int) (0L));
				BgL_i1213z00_3672 =
					((BgL_rtl_insz00_bglt) PROCEDURE_REF(BgL_envz00_3670, (int) (1L)));
				{	/* SawBbv/bbv-utils.scm 309 */
					BgL_rtl_regz00_bglt BgL_regz00_3754;

					BgL_regz00_3754 =
						(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
								((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_3673)))->BgL_regz00);
					{	/* SawBbv/bbv-utils.scm 311 */
						bool_t BgL_test2307z00_4905;

						{	/* SawBbv/bbv-utils.scm 311 */
							obj_t BgL_arg1968z00_3755;

							{
								BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4906;

								{
									obj_t BgL_auxz00_4907;

									{	/* SawBbv/bbv-utils.scm 311 */
										BgL_objectz00_bglt BgL_tmpz00_4908;

										BgL_tmpz00_4908 = ((BgL_objectz00_bglt) BgL_i1213z00_3672);
										BgL_auxz00_4907 = BGL_OBJECT_WIDENING(BgL_tmpz00_4908);
									}
									BgL_auxz00_4906 =
										((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4907);
								}
								BgL_arg1968z00_3755 =
									(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_4906))->
									BgL_inz00);
							}
							{	/* SawBbv/bbv-utils.scm 311 */
								long BgL_basez00_3756;
								long BgL_bitz00_3757;

								{	/* SawBbv/bbv-utils.scm 311 */
									int BgL_arg2022z00_3758;

									{
										BgL_rtl_regzf2razf2_bglt BgL_auxz00_4913;

										{
											obj_t BgL_auxz00_4914;

											{	/* SawBbv/bbv-utils.scm 311 */
												BgL_objectz00_bglt BgL_tmpz00_4915;

												BgL_tmpz00_4915 =
													((BgL_objectz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_regz00_3754));
												BgL_auxz00_4914 = BGL_OBJECT_WIDENING(BgL_tmpz00_4915);
											}
											BgL_auxz00_4913 =
												((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4914);
										}
										BgL_arg2022z00_3758 =
											(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4913))->
											BgL_numz00);
									}
									BgL_basez00_3756 = ((long) (BgL_arg2022z00_3758) / 8L);
								}
								{	/* SawBbv/bbv-utils.scm 311 */
									int BgL_arg2024z00_3759;

									{
										BgL_rtl_regzf2razf2_bglt BgL_auxz00_4923;

										{
											obj_t BgL_auxz00_4924;

											{	/* SawBbv/bbv-utils.scm 311 */
												BgL_objectz00_bglt BgL_tmpz00_4925;

												BgL_tmpz00_4925 =
													((BgL_objectz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_regz00_3754));
												BgL_auxz00_4924 = BGL_OBJECT_WIDENING(BgL_tmpz00_4925);
											}
											BgL_auxz00_4923 =
												((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4924);
										}
										BgL_arg2024z00_3759 =
											(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4923))->
											BgL_numz00);
									}
									{	/* SawBbv/bbv-utils.scm 311 */
										long BgL_n1z00_3760;
										long BgL_n2z00_3761;

										BgL_n1z00_3760 = (long) (BgL_arg2024z00_3759);
										BgL_n2z00_3761 = 8L;
										{	/* SawBbv/bbv-utils.scm 311 */
											bool_t BgL_test2308z00_4932;

											{	/* SawBbv/bbv-utils.scm 311 */
												long BgL_arg1338z00_3762;

												BgL_arg1338z00_3762 =
													(((BgL_n1z00_3760) | (BgL_n2z00_3761)) & -2147483648);
												BgL_test2308z00_4932 = (BgL_arg1338z00_3762 == 0L);
											}
											if (BgL_test2308z00_4932)
												{	/* SawBbv/bbv-utils.scm 311 */
													int32_t BgL_arg1334z00_3763;

													{	/* SawBbv/bbv-utils.scm 311 */
														int32_t BgL_arg1336z00_3764;
														int32_t BgL_arg1337z00_3765;

														BgL_arg1336z00_3764 = (int32_t) (BgL_n1z00_3760);
														BgL_arg1337z00_3765 = (int32_t) (BgL_n2z00_3761);
														BgL_arg1334z00_3763 =
															(BgL_arg1336z00_3764 % BgL_arg1337z00_3765);
													}
													{	/* SawBbv/bbv-utils.scm 311 */
														long BgL_arg1446z00_3766;

														BgL_arg1446z00_3766 = (long) (BgL_arg1334z00_3763);
														BgL_bitz00_3757 = (long) (BgL_arg1446z00_3766);
												}}
											else
												{	/* SawBbv/bbv-utils.scm 311 */
													BgL_bitz00_3757 = (BgL_n1z00_3760 % BgL_n2z00_3761);
												}
										}
									}
								}
								if (
									(BgL_basez00_3756 <
										STRING_LENGTH(
											(((BgL_regsetz00_bglt) COBJECT(
														((BgL_regsetz00_bglt) BgL_arg1968z00_3755)))->
												BgL_stringz00))))
									{	/* SawBbv/bbv-utils.scm 311 */
										BgL_test2307z00_4905 =
											(
											((STRING_REF(
														(((BgL_regsetz00_bglt) COBJECT(
																	((BgL_regsetz00_bglt) BgL_arg1968z00_3755)))->
															BgL_stringz00),
														BgL_basez00_3756)) & (1L <<
													(int) (BgL_bitz00_3757))) > 0L);
									}
								else
									{	/* SawBbv/bbv-utils.scm 311 */
										BgL_test2307z00_4905 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test2307z00_4905)
							{	/* SawBbv/bbv-utils.scm 312 */
								bool_t BgL_test2310z00_4954;

								{	/* SawBbv/bbv-utils.scm 312 */
									obj_t BgL_g1492z00_3767;

									BgL_g1492z00_3767 =
										(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
												((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_3673)))->
										BgL_aliasesz00);
									{
										obj_t BgL_l1490z00_3769;

										BgL_l1490z00_3769 = BgL_g1492z00_3767;
									BgL_zc3z04anonymousza31965ze3z87_3768:
										if (NULLP(BgL_l1490z00_3769))
											{	/* SawBbv/bbv-utils.scm 314 */
												BgL_test2310z00_4954 = ((bool_t) 1);
											}
										else
											{	/* SawBbv/bbv-utils.scm 314 */
												bool_t BgL_test2312z00_4959;

												{	/* SawBbv/bbv-utils.scm 313 */
													obj_t BgL_regz00_3770;

													BgL_regz00_3770 = CAR(((obj_t) BgL_l1490z00_3769));
													{	/* SawBbv/bbv-utils.scm 313 */
														obj_t BgL_arg1967z00_3771;

														{
															BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_4962;

															{
																obj_t BgL_auxz00_4963;

																{	/* SawBbv/bbv-utils.scm 313 */
																	BgL_objectz00_bglt BgL_tmpz00_4964;

																	BgL_tmpz00_4964 =
																		((BgL_objectz00_bglt) BgL_i1213z00_3672);
																	BgL_auxz00_4963 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_4964);
																}
																BgL_auxz00_4962 =
																	((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_4963);
															}
															BgL_arg1967z00_3771 =
																(((BgL_rtl_inszf2bbvzf2_bglt)
																	COBJECT(BgL_auxz00_4962))->BgL_inz00);
														}
														{	/* SawBbv/bbv-utils.scm 313 */
															long BgL_basez00_3772;
															long BgL_bitz00_3773;

															{	/* SawBbv/bbv-utils.scm 313 */
																int BgL_arg2022z00_3774;

																{
																	BgL_rtl_regzf2razf2_bglt BgL_auxz00_4969;

																	{
																		obj_t BgL_auxz00_4970;

																		{	/* SawBbv/bbv-utils.scm 313 */
																			BgL_objectz00_bglt BgL_tmpz00_4971;

																			BgL_tmpz00_4971 =
																				((BgL_objectz00_bglt)
																				((BgL_rtl_regz00_bglt)
																					BgL_regz00_3770));
																			BgL_auxz00_4970 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_4971);
																		}
																		BgL_auxz00_4969 =
																			((BgL_rtl_regzf2razf2_bglt)
																			BgL_auxz00_4970);
																	}
																	BgL_arg2022z00_3774 =
																		(((BgL_rtl_regzf2razf2_bglt)
																			COBJECT(BgL_auxz00_4969))->BgL_numz00);
																}
																BgL_basez00_3772 =
																	((long) (BgL_arg2022z00_3774) / 8L);
															}
															{	/* SawBbv/bbv-utils.scm 313 */
																int BgL_arg2024z00_3775;

																{
																	BgL_rtl_regzf2razf2_bglt BgL_auxz00_4979;

																	{
																		obj_t BgL_auxz00_4980;

																		{	/* SawBbv/bbv-utils.scm 313 */
																			BgL_objectz00_bglt BgL_tmpz00_4981;

																			BgL_tmpz00_4981 =
																				((BgL_objectz00_bglt)
																				((BgL_rtl_regz00_bglt)
																					BgL_regz00_3770));
																			BgL_auxz00_4980 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_4981);
																		}
																		BgL_auxz00_4979 =
																			((BgL_rtl_regzf2razf2_bglt)
																			BgL_auxz00_4980);
																	}
																	BgL_arg2024z00_3775 =
																		(((BgL_rtl_regzf2razf2_bglt)
																			COBJECT(BgL_auxz00_4979))->BgL_numz00);
																}
																{	/* SawBbv/bbv-utils.scm 313 */
																	long BgL_n1z00_3776;
																	long BgL_n2z00_3777;

																	BgL_n1z00_3776 = (long) (BgL_arg2024z00_3775);
																	BgL_n2z00_3777 = 8L;
																	{	/* SawBbv/bbv-utils.scm 313 */
																		bool_t BgL_test2313z00_4988;

																		{	/* SawBbv/bbv-utils.scm 313 */
																			long BgL_arg1338z00_3778;

																			BgL_arg1338z00_3778 =
																				(((BgL_n1z00_3776) | (BgL_n2z00_3777)) &
																				-2147483648);
																			BgL_test2313z00_4988 =
																				(BgL_arg1338z00_3778 == 0L);
																		}
																		if (BgL_test2313z00_4988)
																			{	/* SawBbv/bbv-utils.scm 313 */
																				int32_t BgL_arg1334z00_3779;

																				{	/* SawBbv/bbv-utils.scm 313 */
																					int32_t BgL_arg1336z00_3780;
																					int32_t BgL_arg1337z00_3781;

																					BgL_arg1336z00_3780 =
																						(int32_t) (BgL_n1z00_3776);
																					BgL_arg1337z00_3781 =
																						(int32_t) (BgL_n2z00_3777);
																					BgL_arg1334z00_3779 =
																						(BgL_arg1336z00_3780 %
																						BgL_arg1337z00_3781);
																				}
																				{	/* SawBbv/bbv-utils.scm 313 */
																					long BgL_arg1446z00_3782;

																					BgL_arg1446z00_3782 =
																						(long) (BgL_arg1334z00_3779);
																					BgL_bitz00_3773 =
																						(long) (BgL_arg1446z00_3782);
																			}}
																		else
																			{	/* SawBbv/bbv-utils.scm 313 */
																				BgL_bitz00_3773 =
																					(BgL_n1z00_3776 % BgL_n2z00_3777);
																			}
																	}
																}
															}
															if (
																(BgL_basez00_3772 <
																	STRING_LENGTH(
																		(((BgL_regsetz00_bglt) COBJECT(
																					((BgL_regsetz00_bglt)
																						BgL_arg1967z00_3771)))->
																			BgL_stringz00))))
																{	/* SawBbv/bbv-utils.scm 313 */
																	BgL_test2312z00_4959 =
																		(
																		((STRING_REF(
																					(((BgL_regsetz00_bglt) COBJECT(
																								((BgL_regsetz00_bglt)
																									BgL_arg1967z00_3771)))->
																						BgL_stringz00),
																					BgL_basez00_3772)) & (1L <<
																				(int) (BgL_bitz00_3773))) > 0L);
																}
															else
																{	/* SawBbv/bbv-utils.scm 313 */
																	BgL_test2312z00_4959 = ((bool_t) 0);
																}
														}
													}
												}
												if (BgL_test2312z00_4959)
													{	/* SawBbv/bbv-utils.scm 314 */
														obj_t BgL_arg1966z00_3783;

														BgL_arg1966z00_3783 =
															CDR(((obj_t) BgL_l1490z00_3769));
														{
															obj_t BgL_l1490z00_5012;

															BgL_l1490z00_5012 = BgL_arg1966z00_3783;
															BgL_l1490z00_3769 = BgL_l1490z00_5012;
															goto BgL_zc3z04anonymousza31965ze3z87_3768;
														}
													}
												else
													{	/* SawBbv/bbv-utils.scm 314 */
														BgL_test2310z00_4954 = ((bool_t) 0);
													}
											}
									}
								}
								if (BgL_test2310z00_4954)
									{	/* SawBbv/bbv-utils.scm 312 */
										return BgL_ez00_3673;
									}
								else
									{	/* SawBbv/bbv-utils.scm 316 */
										obj_t BgL_aliasesz00_3784;

										{	/* SawBbv/bbv-utils.scm 316 */
											obj_t BgL_hook1498z00_3785;

											BgL_hook1498z00_3785 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
											{	/* SawBbv/bbv-utils.scm 318 */
												obj_t BgL_g1499z00_3786;

												BgL_g1499z00_3786 =
													(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
															((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_3673)))->
													BgL_aliasesz00);
												{
													obj_t BgL_l1495z00_3788;
													obj_t BgL_h1496z00_3789;

													BgL_l1495z00_3788 = BgL_g1499z00_3786;
													BgL_h1496z00_3789 = BgL_hook1498z00_3785;
												BgL_zc3z04anonymousza31957ze3z87_3787:
													if (NULLP(BgL_l1495z00_3788))
														{	/* SawBbv/bbv-utils.scm 318 */
															BgL_aliasesz00_3784 = CDR(BgL_hook1498z00_3785);
														}
													else
														{	/* SawBbv/bbv-utils.scm 318 */
															bool_t BgL_test2316z00_5019;

															{	/* SawBbv/bbv-utils.scm 317 */
																obj_t BgL_regz00_3790;

																BgL_regz00_3790 =
																	CAR(((obj_t) BgL_l1495z00_3788));
																{	/* SawBbv/bbv-utils.scm 317 */
																	obj_t BgL_arg1964z00_3791;

																	{
																		BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5022;

																		{
																			obj_t BgL_auxz00_5023;

																			{	/* SawBbv/bbv-utils.scm 317 */
																				BgL_objectz00_bglt BgL_tmpz00_5024;

																				BgL_tmpz00_5024 =
																					((BgL_objectz00_bglt)
																					BgL_i1213z00_3672);
																				BgL_auxz00_5023 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_5024);
																			}
																			BgL_auxz00_5022 =
																				((BgL_rtl_inszf2bbvzf2_bglt)
																				BgL_auxz00_5023);
																		}
																		BgL_arg1964z00_3791 =
																			(((BgL_rtl_inszf2bbvzf2_bglt)
																				COBJECT(BgL_auxz00_5022))->BgL_inz00);
																	}
																	{	/* SawBbv/bbv-utils.scm 317 */
																		long BgL_basez00_3792;
																		long BgL_bitz00_3793;

																		{	/* SawBbv/bbv-utils.scm 317 */
																			int BgL_arg2022z00_3794;

																			{
																				BgL_rtl_regzf2razf2_bglt
																					BgL_auxz00_5029;
																				{
																					obj_t BgL_auxz00_5030;

																					{	/* SawBbv/bbv-utils.scm 317 */
																						BgL_objectz00_bglt BgL_tmpz00_5031;

																						BgL_tmpz00_5031 =
																							((BgL_objectz00_bglt)
																							((BgL_rtl_regz00_bglt)
																								BgL_regz00_3790));
																						BgL_auxz00_5030 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_5031);
																					}
																					BgL_auxz00_5029 =
																						((BgL_rtl_regzf2razf2_bglt)
																						BgL_auxz00_5030);
																				}
																				BgL_arg2022z00_3794 =
																					(((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_5029))->
																					BgL_numz00);
																			}
																			BgL_basez00_3792 =
																				((long) (BgL_arg2022z00_3794) / 8L);
																		}
																		{	/* SawBbv/bbv-utils.scm 317 */
																			int BgL_arg2024z00_3795;

																			{
																				BgL_rtl_regzf2razf2_bglt
																					BgL_auxz00_5039;
																				{
																					obj_t BgL_auxz00_5040;

																					{	/* SawBbv/bbv-utils.scm 317 */
																						BgL_objectz00_bglt BgL_tmpz00_5041;

																						BgL_tmpz00_5041 =
																							((BgL_objectz00_bglt)
																							((BgL_rtl_regz00_bglt)
																								BgL_regz00_3790));
																						BgL_auxz00_5040 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_5041);
																					}
																					BgL_auxz00_5039 =
																						((BgL_rtl_regzf2razf2_bglt)
																						BgL_auxz00_5040);
																				}
																				BgL_arg2024z00_3795 =
																					(((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_5039))->
																					BgL_numz00);
																			}
																			{	/* SawBbv/bbv-utils.scm 317 */
																				long BgL_n1z00_3796;
																				long BgL_n2z00_3797;

																				BgL_n1z00_3796 =
																					(long) (BgL_arg2024z00_3795);
																				BgL_n2z00_3797 = 8L;
																				{	/* SawBbv/bbv-utils.scm 317 */
																					bool_t BgL_test2317z00_5048;

																					{	/* SawBbv/bbv-utils.scm 317 */
																						long BgL_arg1338z00_3798;

																						BgL_arg1338z00_3798 =
																							(((BgL_n1z00_3796) |
																								(BgL_n2z00_3797)) &
																							-2147483648);
																						BgL_test2317z00_5048 =
																							(BgL_arg1338z00_3798 == 0L);
																					}
																					if (BgL_test2317z00_5048)
																						{	/* SawBbv/bbv-utils.scm 317 */
																							int32_t BgL_arg1334z00_3799;

																							{	/* SawBbv/bbv-utils.scm 317 */
																								int32_t BgL_arg1336z00_3800;
																								int32_t BgL_arg1337z00_3801;

																								BgL_arg1336z00_3800 =
																									(int32_t) (BgL_n1z00_3796);
																								BgL_arg1337z00_3801 =
																									(int32_t) (BgL_n2z00_3797);
																								BgL_arg1334z00_3799 =
																									(BgL_arg1336z00_3800 %
																									BgL_arg1337z00_3801);
																							}
																							{	/* SawBbv/bbv-utils.scm 317 */
																								long BgL_arg1446z00_3802;

																								BgL_arg1446z00_3802 =
																									(long) (BgL_arg1334z00_3799);
																								BgL_bitz00_3793 =
																									(long) (BgL_arg1446z00_3802);
																						}}
																					else
																						{	/* SawBbv/bbv-utils.scm 317 */
																							BgL_bitz00_3793 =
																								(BgL_n1z00_3796 %
																								BgL_n2z00_3797);
																						}
																				}
																			}
																		}
																		if (
																			(BgL_basez00_3792 <
																				STRING_LENGTH(
																					(((BgL_regsetz00_bglt) COBJECT(
																								((BgL_regsetz00_bglt)
																									BgL_arg1964z00_3791)))->
																						BgL_stringz00))))
																			{	/* SawBbv/bbv-utils.scm 317 */
																				BgL_test2316z00_5019 =
																					(
																					((STRING_REF(
																								(((BgL_regsetz00_bglt) COBJECT(
																											((BgL_regsetz00_bglt)
																												BgL_arg1964z00_3791)))->
																									BgL_stringz00),
																								BgL_basez00_3792)) & (1L <<
																							(int) (BgL_bitz00_3793))) > 0L);
																			}
																		else
																			{	/* SawBbv/bbv-utils.scm 317 */
																				BgL_test2316z00_5019 = ((bool_t) 0);
																			}
																	}
																}
															}
															if (BgL_test2316z00_5019)
																{	/* SawBbv/bbv-utils.scm 318 */
																	obj_t BgL_nh1497z00_3803;

																	{	/* SawBbv/bbv-utils.scm 318 */
																		obj_t BgL_arg1962z00_3804;

																		BgL_arg1962z00_3804 =
																			CAR(((obj_t) BgL_l1495z00_3788));
																		BgL_nh1497z00_3803 =
																			MAKE_YOUNG_PAIR(BgL_arg1962z00_3804,
																			BNIL);
																	}
																	SET_CDR(BgL_h1496z00_3789,
																		BgL_nh1497z00_3803);
																	{	/* SawBbv/bbv-utils.scm 318 */
																		obj_t BgL_arg1961z00_3805;

																		BgL_arg1961z00_3805 =
																			CDR(((obj_t) BgL_l1495z00_3788));
																		{
																			obj_t BgL_h1496z00_5077;
																			obj_t BgL_l1495z00_5076;

																			BgL_l1495z00_5076 = BgL_arg1961z00_3805;
																			BgL_h1496z00_5077 = BgL_nh1497z00_3803;
																			BgL_h1496z00_3789 = BgL_h1496z00_5077;
																			BgL_l1495z00_3788 = BgL_l1495z00_5076;
																			goto
																				BgL_zc3z04anonymousza31957ze3z87_3787;
																		}
																	}
																}
															else
																{	/* SawBbv/bbv-utils.scm 318 */
																	obj_t BgL_arg1963z00_3806;

																	BgL_arg1963z00_3806 =
																		CDR(((obj_t) BgL_l1495z00_3788));
																	{
																		obj_t BgL_l1495z00_5080;

																		BgL_l1495z00_5080 = BgL_arg1963z00_3806;
																		BgL_l1495z00_3788 = BgL_l1495z00_5080;
																		goto BgL_zc3z04anonymousza31957ze3z87_3787;
																	}
																}
														}
												}
											}
										}
										{	/* SawBbv/bbv-utils.scm 319 */
											obj_t BgL_auxz00_3807;

											BgL_auxz00_3807 = BTRUE;
											CELL_SET(BgL_filteredz00_3671, BgL_auxz00_3807);
										}
										{	/* SawBbv/bbv-utils.scm 320 */
											BgL_bbvzd2ctxentryzd2_bglt BgL_new1214z00_3808;

											{	/* SawBbv/bbv-utils.scm 320 */
												BgL_bbvzd2ctxentryzd2_bglt BgL_new1217z00_3809;

												BgL_new1217z00_3809 =
													((BgL_bbvzd2ctxentryzd2_bglt)
													BOBJECT(GC_MALLOC(sizeof(struct
																BgL_bbvzd2ctxentryzd2_bgl))));
												{	/* SawBbv/bbv-utils.scm 320 */
													long BgL_arg1956z00_3810;

													BgL_arg1956z00_3810 =
														BGL_CLASS_NUM
														(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
													BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
															BgL_new1217z00_3809), BgL_arg1956z00_3810);
												}
												BgL_new1214z00_3808 = BgL_new1217z00_3809;
											}
											((((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(BgL_new1214z00_3808))->BgL_regz00) =
												((BgL_rtl_regz00_bglt) (((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																	BgL_ez00_3673)))->BgL_regz00)), BUNSPEC);
											((((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(BgL_new1214z00_3808))->BgL_typesz00) =
												((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																	BgL_ez00_3673)))->BgL_typesz00)), BUNSPEC);
											((((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(BgL_new1214z00_3808))->BgL_polarityz00) =
												((bool_t) (((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																	BgL_ez00_3673)))->BgL_polarityz00)), BUNSPEC);
											((((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(BgL_new1214z00_3808))->BgL_countz00) =
												((long) (((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																	BgL_ez00_3673)))->BgL_countz00)), BUNSPEC);
											((((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(BgL_new1214z00_3808))->BgL_valuez00) =
												((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																	BgL_ez00_3673)))->BgL_valuez00)), BUNSPEC);
											((((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(BgL_new1214z00_3808))->BgL_aliasesz00) =
												((obj_t) BgL_aliasesz00_3784), BUNSPEC);
											((((BgL_bbvzd2ctxentryzd2_bglt)
														COBJECT(BgL_new1214z00_3808))->BgL_initvalz00) =
												((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
															COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
																	BgL_ez00_3673)))->BgL_initvalz00)), BUNSPEC);
											return ((obj_t) BgL_new1214z00_3808);
										}
									}
							}
						else
							{	/* SawBbv/bbv-utils.scm 311 */
								{	/* SawBbv/bbv-utils.scm 323 */
									obj_t BgL_auxz00_3811;

									BgL_auxz00_3811 = BTRUE;
									CELL_SET(BgL_filteredz00_3671, BgL_auxz00_3811);
								}
								return BFALSE;
							}
					}
				}
			}
		}

	}



/* bbv-ctx-extend-live-out-regs */
	BGL_EXPORTED_DEF BgL_bbvzd2ctxzd2_bglt
		BGl_bbvzd2ctxzd2extendzd2livezd2outzd2regszd2zzsaw_bbvzd2utilszd2
		(BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_111, BgL_rtl_insz00_bglt BgL_insz00_112)
	{
		{	/* SawBbv/bbv-utils.scm 336 */
			{	/* SawBbv/bbv-utils.scm 338 */
				obj_t BgL_entriesz00_2589;

				{	/* SawBbv/bbv-utils.scm 338 */
					obj_t BgL_arg1979z00_2609;

					BgL_arg1979z00_2609 =
						(((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_111))->BgL_entriesz00);
					{	/* SawBbv/bbv-utils.scm 339 */
						obj_t BgL_zc3z04anonymousza31981ze3z87_3679;

						BgL_zc3z04anonymousza31981ze3z87_3679 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31981ze3ze5zzsaw_bbvzd2utilszd2,
							(int) (1L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31981ze3z87_3679, (int) (0L),
							((obj_t) BgL_insz00_112));
						{	/* SawBbv/bbv-utils.scm 338 */
							obj_t BgL_list1980z00_2610;

							BgL_list1980z00_2610 = MAKE_YOUNG_PAIR(BgL_arg1979z00_2609, BNIL);
							BgL_entriesz00_2589 =
								BGl_filterzd2mapzd2zz__r4_control_features_6_9z00
								(BgL_zc3z04anonymousza31981ze3z87_3679, BgL_list1980z00_2610);
				}}}
				{	/* SawBbv/bbv-utils.scm 346 */
					BgL_bbvzd2ctxzd2_bglt BgL_nctxz00_2590;

					{	/* SawBbv/bbv-utils.scm 346 */
						BgL_bbvzd2ctxzd2_bglt BgL_new1229z00_2604;

						{	/* SawBbv/bbv-utils.scm 346 */
							BgL_bbvzd2ctxzd2_bglt BgL_new1232z00_2606;

							BgL_new1232z00_2606 =
								((BgL_bbvzd2ctxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_bbvzd2ctxzd2_bgl))));
							{	/* SawBbv/bbv-utils.scm 346 */
								long BgL_arg1977z00_2607;

								BgL_arg1977z00_2607 =
									BGL_CLASS_NUM(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1232z00_2606),
									BgL_arg1977z00_2607);
							}
							BgL_new1229z00_2604 = BgL_new1232z00_2606;
						}
						((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1229z00_2604))->
								BgL_idz00) =
							((long) (((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_ctxz00_111))->
									BgL_idz00)), BUNSPEC);
						((((BgL_bbvzd2ctxzd2_bglt) COBJECT(BgL_new1229z00_2604))->
								BgL_entriesz00) = ((obj_t) BgL_entriesz00_2589), BUNSPEC);
						{	/* SawBbv/bbv-utils.scm 347 */
							obj_t BgL_fun1976z00_2605;

							BgL_fun1976z00_2605 =
								BGl_classzd2constructorzd2zz__objectz00
								(BGl_bbvzd2ctxzd2zzsaw_bbvzd2typeszd2);
							BGL_PROCEDURE_CALL1(BgL_fun1976z00_2605,
								((obj_t) BgL_new1229z00_2604));
						}
						BgL_nctxz00_2590 = BgL_new1229z00_2604;
					}
					{	/* SawBbv/bbv-utils.scm 348 */
						obj_t BgL_arg1970z00_2592;

						{
							BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5127;

							{
								obj_t BgL_auxz00_5128;

								{	/* SawBbv/bbv-utils.scm 348 */
									BgL_objectz00_bglt BgL_tmpz00_5129;

									BgL_tmpz00_5129 = ((BgL_objectz00_bglt) BgL_insz00_112);
									BgL_auxz00_5128 = BGL_OBJECT_WIDENING(BgL_tmpz00_5129);
								}
								BgL_auxz00_5127 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_5128);
							}
							BgL_arg1970z00_2592 =
								(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_5127))->
								BgL_outz00);
						}
						{	/* SawBbv/bbv-utils.scm 350 */
							obj_t BgL_zc3z04anonymousza31971ze3z87_3678;

							BgL_zc3z04anonymousza31971ze3z87_3678 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31971ze3ze5zzsaw_bbvzd2utilszd2,
								(int) (1L), (int) (1L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31971ze3z87_3678, (int) (0L),
								((obj_t) BgL_nctxz00_2590));
							BGl_regsetzd2forzd2eachz00zzsaw_regsetz00
								(BgL_zc3z04anonymousza31971ze3z87_3678,
								((BgL_regsetz00_bglt) BgL_arg1970z00_2592));
					}}
					return BgL_nctxz00_2590;
				}
			}
		}

	}



/* &bbv-ctx-extend-live-out-regs */
	BgL_bbvzd2ctxzd2_bglt
		BGl_z62bbvzd2ctxzd2extendzd2livezd2outzd2regszb0zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3680, obj_t BgL_ctxz00_3681, obj_t BgL_insz00_3682)
	{
		{	/* SawBbv/bbv-utils.scm 336 */
			return
				BGl_bbvzd2ctxzd2extendzd2livezd2outzd2regszd2zzsaw_bbvzd2utilszd2(
				((BgL_bbvzd2ctxzd2_bglt) BgL_ctxz00_3681),
				((BgL_rtl_insz00_bglt) BgL_insz00_3682));
		}

	}



/* &<@anonymous:1971> */
	obj_t BGl_z62zc3z04anonymousza31971ze3ze5zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3683, obj_t BgL_regz00_3685)
	{
		{	/* SawBbv/bbv-utils.scm 348 */
			{	/* SawBbv/bbv-utils.scm 350 */
				BgL_bbvzd2ctxzd2_bglt BgL_nctxz00_3684;

				BgL_nctxz00_3684 =
					((BgL_bbvzd2ctxzd2_bglt) PROCEDURE_REF(BgL_envz00_3683, (int) (0L)));
				if (CBOOL(BGl_bbvzd2ctxzd2getz00zzsaw_bbvzd2typeszd2(BgL_nctxz00_3684,
							((BgL_rtl_regz00_bglt) BgL_regz00_3685))))
					{	/* SawBbv/bbv-utils.scm 350 */
						return BFALSE;
					}
				else
					{	/* SawBbv/bbv-utils.scm 352 */
						obj_t BgL_arg1973z00_3812;

						{	/* SawBbv/bbv-utils.scm 352 */
							BgL_typez00_bglt BgL_arg1974z00_3813;

							BgL_arg1974z00_3813 =
								(((BgL_rtl_regz00_bglt) COBJECT(
										((BgL_rtl_regz00_bglt) BgL_regz00_3685)))->BgL_typez00);
							{	/* SawBbv/bbv-utils.scm 352 */
								obj_t BgL_list1975z00_3814;

								BgL_list1975z00_3814 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_arg1974z00_3813), BNIL);
								BgL_arg1973z00_3812 = BgL_list1975z00_3814;
							}
						}
						{	/* SawBbv/bbv-utils.scm 352 */
							obj_t BgL_valuez00_3815;

							BgL_valuez00_3815 = CNST_TABLE_REF(3);
							return
								((obj_t)
								BGl_extendzd2ctxz12zc0zzsaw_bbvzd2typeszd2(BgL_nctxz00_3684,
									((BgL_rtl_regz00_bglt) BgL_regz00_3685), BgL_arg1973z00_3812,
									((bool_t) 1), BFALSE, BgL_valuez00_3815));
						}
					}
			}
		}

	}



/* &<@anonymous:1981> */
	obj_t BGl_z62zc3z04anonymousza31981ze3ze5zzsaw_bbvzd2utilszd2(obj_t
		BgL_envz00_3686, obj_t BgL_ez00_3688)
	{
		{	/* SawBbv/bbv-utils.scm 338 */
			{	/* SawBbv/bbv-utils.scm 339 */
				BgL_rtl_insz00_bglt BgL_i1222z00_3687;

				BgL_i1222z00_3687 =
					((BgL_rtl_insz00_bglt) PROCEDURE_REF(BgL_envz00_3686, (int) (0L)));
				{	/* SawBbv/bbv-utils.scm 339 */
					bool_t BgL_test2320z00_5163;

					{	/* SawBbv/bbv-utils.scm 339 */
						BgL_rtl_regz00_bglt BgL_arg1997z00_3816;
						obj_t BgL_arg1998z00_3817;

						BgL_arg1997z00_3816 =
							(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
									((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_3688)))->BgL_regz00);
						{
							BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5166;

							{
								obj_t BgL_auxz00_5167;

								{	/* SawBbv/bbv-utils.scm 339 */
									BgL_objectz00_bglt BgL_tmpz00_5168;

									BgL_tmpz00_5168 = ((BgL_objectz00_bglt) BgL_i1222z00_3687);
									BgL_auxz00_5167 = BGL_OBJECT_WIDENING(BgL_tmpz00_5168);
								}
								BgL_auxz00_5166 = ((BgL_rtl_inszf2bbvzf2_bglt) BgL_auxz00_5167);
							}
							BgL_arg1998z00_3817 =
								(((BgL_rtl_inszf2bbvzf2_bglt) COBJECT(BgL_auxz00_5166))->
								BgL_outz00);
						}
						{	/* SawBbv/bbv-utils.scm 339 */
							long BgL_basez00_3818;
							long BgL_bitz00_3819;

							{	/* SawBbv/bbv-utils.scm 339 */
								int BgL_arg2022z00_3820;

								{
									BgL_rtl_regzf2razf2_bglt BgL_auxz00_5173;

									{
										obj_t BgL_auxz00_5174;

										{	/* SawBbv/bbv-utils.scm 339 */
											BgL_objectz00_bglt BgL_tmpz00_5175;

											BgL_tmpz00_5175 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_arg1997z00_3816));
											BgL_auxz00_5174 = BGL_OBJECT_WIDENING(BgL_tmpz00_5175);
										}
										BgL_auxz00_5173 =
											((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5174);
									}
									BgL_arg2022z00_3820 =
										(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5173))->
										BgL_numz00);
								}
								BgL_basez00_3818 = ((long) (BgL_arg2022z00_3820) / 8L);
							}
							{	/* SawBbv/bbv-utils.scm 339 */
								int BgL_arg2024z00_3821;

								{
									BgL_rtl_regzf2razf2_bglt BgL_auxz00_5183;

									{
										obj_t BgL_auxz00_5184;

										{	/* SawBbv/bbv-utils.scm 339 */
											BgL_objectz00_bglt BgL_tmpz00_5185;

											BgL_tmpz00_5185 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_arg1997z00_3816));
											BgL_auxz00_5184 = BGL_OBJECT_WIDENING(BgL_tmpz00_5185);
										}
										BgL_auxz00_5183 =
											((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5184);
									}
									BgL_arg2024z00_3821 =
										(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5183))->
										BgL_numz00);
								}
								{	/* SawBbv/bbv-utils.scm 339 */
									long BgL_n1z00_3822;
									long BgL_n2z00_3823;

									BgL_n1z00_3822 = (long) (BgL_arg2024z00_3821);
									BgL_n2z00_3823 = 8L;
									{	/* SawBbv/bbv-utils.scm 339 */
										bool_t BgL_test2321z00_5192;

										{	/* SawBbv/bbv-utils.scm 339 */
											long BgL_arg1338z00_3824;

											BgL_arg1338z00_3824 =
												(((BgL_n1z00_3822) | (BgL_n2z00_3823)) & -2147483648);
											BgL_test2321z00_5192 = (BgL_arg1338z00_3824 == 0L);
										}
										if (BgL_test2321z00_5192)
											{	/* SawBbv/bbv-utils.scm 339 */
												int32_t BgL_arg1334z00_3825;

												{	/* SawBbv/bbv-utils.scm 339 */
													int32_t BgL_arg1336z00_3826;
													int32_t BgL_arg1337z00_3827;

													BgL_arg1336z00_3826 = (int32_t) (BgL_n1z00_3822);
													BgL_arg1337z00_3827 = (int32_t) (BgL_n2z00_3823);
													BgL_arg1334z00_3825 =
														(BgL_arg1336z00_3826 % BgL_arg1337z00_3827);
												}
												{	/* SawBbv/bbv-utils.scm 339 */
													long BgL_arg1446z00_3828;

													BgL_arg1446z00_3828 = (long) (BgL_arg1334z00_3825);
													BgL_bitz00_3819 = (long) (BgL_arg1446z00_3828);
											}}
										else
											{	/* SawBbv/bbv-utils.scm 339 */
												BgL_bitz00_3819 = (BgL_n1z00_3822 % BgL_n2z00_3823);
											}
									}
								}
							}
							if (
								(BgL_basez00_3818 <
									STRING_LENGTH(
										(((BgL_regsetz00_bglt) COBJECT(
													((BgL_regsetz00_bglt) BgL_arg1998z00_3817)))->
											BgL_stringz00))))
								{	/* SawBbv/bbv-utils.scm 339 */
									BgL_test2320z00_5163 =
										(
										((STRING_REF(
													(((BgL_regsetz00_bglt) COBJECT(
																((BgL_regsetz00_bglt) BgL_arg1998z00_3817)))->
														BgL_stringz00),
													BgL_basez00_3818)) & (1L << (int) (BgL_bitz00_3819)))
										> 0L);
								}
							else
								{	/* SawBbv/bbv-utils.scm 339 */
									BgL_test2320z00_5163 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2320z00_5163)
						{	/* SawBbv/bbv-utils.scm 339 */
							if (NULLP(
									(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
												((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_3688)))->
										BgL_aliasesz00)))
								{	/* SawBbv/bbv-utils.scm 341 */
									return BgL_ez00_3688;
								}
							else
								{	/* SawBbv/bbv-utils.scm 343 */
									BgL_bbvzd2ctxentryzd2_bglt BgL_new1224z00_3829;

									{	/* SawBbv/bbv-utils.scm 343 */
										BgL_bbvzd2ctxentryzd2_bglt BgL_new1228z00_3830;

										BgL_new1228z00_3830 =
											((BgL_bbvzd2ctxentryzd2_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_bbvzd2ctxentryzd2_bgl))));
										{	/* SawBbv/bbv-utils.scm 343 */
											long BgL_arg1995z00_3831;

											BgL_arg1995z00_3831 =
												BGL_CLASS_NUM
												(BGl_bbvzd2ctxentryzd2zzsaw_bbvzd2typeszd2);
											BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
													BgL_new1228z00_3830), BgL_arg1995z00_3831);
										}
										BgL_new1224z00_3829 = BgL_new1228z00_3830;
									}
									((((BgL_bbvzd2ctxentryzd2_bglt)
												COBJECT(BgL_new1224z00_3829))->BgL_regz00) =
										((BgL_rtl_regz00_bglt) (((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
															BgL_ez00_3688)))->BgL_regz00)), BUNSPEC);
									((((BgL_bbvzd2ctxentryzd2_bglt)
												COBJECT(BgL_new1224z00_3829))->BgL_typesz00) =
										((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
															BgL_ez00_3688)))->BgL_typesz00)), BUNSPEC);
									((((BgL_bbvzd2ctxentryzd2_bglt)
												COBJECT(BgL_new1224z00_3829))->BgL_polarityz00) =
										((bool_t) (((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
															BgL_ez00_3688)))->BgL_polarityz00)), BUNSPEC);
									((((BgL_bbvzd2ctxentryzd2_bglt)
												COBJECT(BgL_new1224z00_3829))->BgL_countz00) =
										((long) (((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
															BgL_ez00_3688)))->BgL_countz00)), BUNSPEC);
									((((BgL_bbvzd2ctxentryzd2_bglt)
												COBJECT(BgL_new1224z00_3829))->BgL_valuez00) =
										((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
															BgL_ez00_3688)))->BgL_valuez00)), BUNSPEC);
									{
										obj_t BgL_auxz00_5237;

										{	/* SawBbv/bbv-utils.scm 344 */
											obj_t BgL_hook1504z00_3832;

											BgL_hook1504z00_3832 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
											{	/* SawBbv/bbv-utils.scm 344 */
												obj_t BgL_g1505z00_3833;

												BgL_g1505z00_3833 =
													(((BgL_bbvzd2ctxentryzd2_bglt) COBJECT(
															((BgL_bbvzd2ctxentryzd2_bglt) BgL_ez00_3688)))->
													BgL_aliasesz00);
												{
													obj_t BgL_l1501z00_3835;
													obj_t BgL_h1502z00_3836;

													BgL_l1501z00_3835 = BgL_g1505z00_3833;
													BgL_h1502z00_3836 = BgL_hook1504z00_3832;
												BgL_zc3z04anonymousza31987ze3z87_3834:
													if (NULLP(BgL_l1501z00_3835))
														{	/* SawBbv/bbv-utils.scm 344 */
															BgL_auxz00_5237 = CDR(BgL_hook1504z00_3832);
														}
													else
														{	/* SawBbv/bbv-utils.scm 344 */
															bool_t BgL_test2325z00_5244;

															{	/* SawBbv/bbv-utils.scm 344 */
																obj_t BgL_rz00_3837;

																BgL_rz00_3837 =
																	CAR(((obj_t) BgL_l1501z00_3835));
																{	/* SawBbv/bbv-utils.scm 344 */
																	obj_t BgL_arg1994z00_3838;

																	{
																		BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_5247;

																		{
																			obj_t BgL_auxz00_5248;

																			{	/* SawBbv/bbv-utils.scm 344 */
																				BgL_objectz00_bglt BgL_tmpz00_5249;

																				BgL_tmpz00_5249 =
																					((BgL_objectz00_bglt)
																					BgL_i1222z00_3687);
																				BgL_auxz00_5248 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_5249);
																			}
																			BgL_auxz00_5247 =
																				((BgL_rtl_inszf2bbvzf2_bglt)
																				BgL_auxz00_5248);
																		}
																		BgL_arg1994z00_3838 =
																			(((BgL_rtl_inszf2bbvzf2_bglt)
																				COBJECT(BgL_auxz00_5247))->BgL_outz00);
																	}
																	{	/* SawBbv/bbv-utils.scm 344 */
																		long BgL_basez00_3839;
																		long BgL_bitz00_3840;

																		{	/* SawBbv/bbv-utils.scm 344 */
																			int BgL_arg2022z00_3841;

																			{
																				BgL_rtl_regzf2razf2_bglt
																					BgL_auxz00_5254;
																				{
																					obj_t BgL_auxz00_5255;

																					{	/* SawBbv/bbv-utils.scm 344 */
																						BgL_objectz00_bglt BgL_tmpz00_5256;

																						BgL_tmpz00_5256 =
																							((BgL_objectz00_bglt)
																							((BgL_rtl_regz00_bglt)
																								BgL_rz00_3837));
																						BgL_auxz00_5255 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_5256);
																					}
																					BgL_auxz00_5254 =
																						((BgL_rtl_regzf2razf2_bglt)
																						BgL_auxz00_5255);
																				}
																				BgL_arg2022z00_3841 =
																					(((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_5254))->
																					BgL_numz00);
																			}
																			BgL_basez00_3839 =
																				((long) (BgL_arg2022z00_3841) / 8L);
																		}
																		{	/* SawBbv/bbv-utils.scm 344 */
																			int BgL_arg2024z00_3842;

																			{
																				BgL_rtl_regzf2razf2_bglt
																					BgL_auxz00_5264;
																				{
																					obj_t BgL_auxz00_5265;

																					{	/* SawBbv/bbv-utils.scm 344 */
																						BgL_objectz00_bglt BgL_tmpz00_5266;

																						BgL_tmpz00_5266 =
																							((BgL_objectz00_bglt)
																							((BgL_rtl_regz00_bglt)
																								BgL_rz00_3837));
																						BgL_auxz00_5265 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_5266);
																					}
																					BgL_auxz00_5264 =
																						((BgL_rtl_regzf2razf2_bglt)
																						BgL_auxz00_5265);
																				}
																				BgL_arg2024z00_3842 =
																					(((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_5264))->
																					BgL_numz00);
																			}
																			{	/* SawBbv/bbv-utils.scm 344 */
																				long BgL_n1z00_3843;
																				long BgL_n2z00_3844;

																				BgL_n1z00_3843 =
																					(long) (BgL_arg2024z00_3842);
																				BgL_n2z00_3844 = 8L;
																				{	/* SawBbv/bbv-utils.scm 344 */
																					bool_t BgL_test2326z00_5273;

																					{	/* SawBbv/bbv-utils.scm 344 */
																						long BgL_arg1338z00_3845;

																						BgL_arg1338z00_3845 =
																							(((BgL_n1z00_3843) |
																								(BgL_n2z00_3844)) &
																							-2147483648);
																						BgL_test2326z00_5273 =
																							(BgL_arg1338z00_3845 == 0L);
																					}
																					if (BgL_test2326z00_5273)
																						{	/* SawBbv/bbv-utils.scm 344 */
																							int32_t BgL_arg1334z00_3846;

																							{	/* SawBbv/bbv-utils.scm 344 */
																								int32_t BgL_arg1336z00_3847;
																								int32_t BgL_arg1337z00_3848;

																								{	/* SawBbv/bbv-utils.scm 344 */
																									long BgL_xz00_3849;

																									BgL_xz00_3849 =
																										BgL_n1z00_3843;
																									BgL_arg1336z00_3847 =
																										(int32_t) (BgL_xz00_3849);
																								}
																								{	/* SawBbv/bbv-utils.scm 344 */
																									long BgL_xz00_3850;

																									BgL_xz00_3850 =
																										BgL_n2z00_3844;
																									BgL_arg1337z00_3848 =
																										(int32_t) (BgL_xz00_3850);
																								}
																								BgL_arg1334z00_3846 =
																									(BgL_arg1336z00_3847 %
																									BgL_arg1337z00_3848);
																							}
																							{	/* SawBbv/bbv-utils.scm 344 */
																								long BgL_arg1446z00_3851;

																								BgL_arg1446z00_3851 =
																									(long) (BgL_arg1334z00_3846);
																								BgL_bitz00_3840 =
																									(long) (BgL_arg1446z00_3851);
																						}}
																					else
																						{	/* SawBbv/bbv-utils.scm 344 */
																							BgL_bitz00_3840 =
																								(BgL_n1z00_3843 %
																								BgL_n2z00_3844);
																						}
																				}
																			}
																		}
																		if (
																			(BgL_basez00_3839 <
																				STRING_LENGTH(
																					(((BgL_regsetz00_bglt) COBJECT(
																								((BgL_regsetz00_bglt)
																									BgL_arg1994z00_3838)))->
																						BgL_stringz00))))
																			{	/* SawBbv/bbv-utils.scm 344 */
																				BgL_test2325z00_5244 =
																					(
																					((STRING_REF(
																								(((BgL_regsetz00_bglt) COBJECT(
																											((BgL_regsetz00_bglt)
																												BgL_arg1994z00_3838)))->
																									BgL_stringz00),
																								BgL_basez00_3839)) & (1L <<
																							(int) (BgL_bitz00_3840))) > 0L);
																			}
																		else
																			{	/* SawBbv/bbv-utils.scm 344 */
																				BgL_test2325z00_5244 = ((bool_t) 0);
																			}
																	}
																}
															}
															if (BgL_test2325z00_5244)
																{	/* SawBbv/bbv-utils.scm 344 */
																	obj_t BgL_nh1503z00_3852;

																	{	/* SawBbv/bbv-utils.scm 344 */
																		obj_t BgL_arg1992z00_3853;

																		BgL_arg1992z00_3853 =
																			CAR(((obj_t) BgL_l1501z00_3835));
																		BgL_nh1503z00_3852 =
																			MAKE_YOUNG_PAIR(BgL_arg1992z00_3853,
																			BNIL);
																	}
																	SET_CDR(BgL_h1502z00_3836,
																		BgL_nh1503z00_3852);
																	{	/* SawBbv/bbv-utils.scm 344 */
																		obj_t BgL_arg1991z00_3854;

																		BgL_arg1991z00_3854 =
																			CDR(((obj_t) BgL_l1501z00_3835));
																		{
																			obj_t BgL_h1502z00_5302;
																			obj_t BgL_l1501z00_5301;

																			BgL_l1501z00_5301 = BgL_arg1991z00_3854;
																			BgL_h1502z00_5302 = BgL_nh1503z00_3852;
																			BgL_h1502z00_3836 = BgL_h1502z00_5302;
																			BgL_l1501z00_3835 = BgL_l1501z00_5301;
																			goto
																				BgL_zc3z04anonymousza31987ze3z87_3834;
																		}
																	}
																}
															else
																{	/* SawBbv/bbv-utils.scm 344 */
																	obj_t BgL_arg1993z00_3855;

																	BgL_arg1993z00_3855 =
																		CDR(((obj_t) BgL_l1501z00_3835));
																	{
																		obj_t BgL_l1501z00_5305;

																		BgL_l1501z00_5305 = BgL_arg1993z00_3855;
																		BgL_l1501z00_3835 = BgL_l1501z00_5305;
																		goto BgL_zc3z04anonymousza31987ze3z87_3834;
																	}
																}
														}
												}
											}
										}
										((((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(BgL_new1224z00_3829))->BgL_aliasesz00) =
											((obj_t) BgL_auxz00_5237), BUNSPEC);
									}
									((((BgL_bbvzd2ctxentryzd2_bglt)
												COBJECT(BgL_new1224z00_3829))->BgL_initvalz00) =
										((obj_t) (((BgL_bbvzd2ctxentryzd2_bglt)
													COBJECT(((BgL_bbvzd2ctxentryzd2_bglt)
															BgL_ez00_3688)))->BgL_initvalz00)), BUNSPEC);
									return ((obj_t) BgL_new1224z00_3829);
								}
						}
					else
						{	/* SawBbv/bbv-utils.scm 339 */
							return BFALSE;
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_bbvzd2utilszd2(void)
	{
		{	/* SawBbv/bbv-utils.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_bbvzd2utilszd2(void)
	{
		{	/* SawBbv/bbv-utils.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_bbvzd2utilszd2(void)
	{
		{	/* SawBbv/bbv-utils.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2utilszd2(void)
	{
		{	/* SawBbv/bbv-utils.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zzsaw_regsetz00(358079690L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(237915200L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(288263219L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(425659118L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2specializa7ez75(506937389L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(242097300L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(481635416L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2debugzd2(120981929L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
			return
				BGl_modulezd2initializa7ationz75zzsaw_bbvzd2gczd2(137439381L,
				BSTRING_TO_STRING(BGl_string2160z00zzsaw_bbvzd2utilszd2));
		}

	}

#ifdef __cplusplus
}
#endif
