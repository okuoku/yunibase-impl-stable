/*===========================================================================*/
/*   (SawBbv/bbv-optim.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawBbv/bbv-optim.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_SAW_BBVzd2OPTIMzd2_TYPE_DEFINITIONS
#define BGL_BgL_SAW_BBVzd2OPTIMzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_goz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_toz00;
	}                *BgL_rtl_goz00_bglt;

	typedef struct BgL_rtl_nopz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_nopz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_rtl_regzf2razf2_bgl
	{
		int BgL_numz00;
		obj_t BgL_colorz00;
		obj_t BgL_coalescez00;
		int BgL_occurrencesz00;
		obj_t BgL_interferez00;
		obj_t BgL_interfere2z00;
	}                      *BgL_rtl_regzf2razf2_bglt;

	typedef struct BgL_regsetz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_lengthz00;
		int BgL_msiza7eza7;
		obj_t BgL_regvz00;
		obj_t BgL_reglz00;
		obj_t BgL_stringz00;
	}                *BgL_regsetz00_bglt;

	typedef struct BgL_blockvz00_bgl
	{
		obj_t BgL_versionsz00;
		obj_t BgL_genericz00;
		long BgL_z52markz52;
		obj_t BgL_mergez00;
	}                *BgL_blockvz00_bglt;

	typedef struct BgL_blocksz00_bgl
	{
		long BgL_z52markz52;
		obj_t BgL_z52hashz52;
		obj_t BgL_z52blacklistz52;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		struct BgL_blockz00_bgl *BgL_parentz00;
		long BgL_gccntz00;
		long BgL_gcmarkz00;
		obj_t BgL_mblockz00;
		obj_t BgL_creatorz00;
		obj_t BgL_mergesz00;
		bool_t BgL_asleepz00;
	}                *BgL_blocksz00_bglt;

	typedef struct BgL_bbvzd2ctxzd2_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_idz00;
		obj_t BgL_entriesz00;
	}                   *BgL_bbvzd2ctxzd2_bglt;


#endif													// BGL_BgL_SAW_BBVzd2OPTIMzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2optimzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	static obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2optimzd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2optimzd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_bbvzd2optimzd2 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t
		BGl_blockVzd2livezd2versionsz00zzsaw_bbvzd2typeszd2(BgL_blockz00_bglt);
	static obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2optimzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62removezd2nopz12za2zzsaw_bbvzd2optimzd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2optimzd2(int, int, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2optimzd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2optimzd2(BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, int, obj_t, obj_t, int, obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2optimzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_everyz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2optimzd2(void);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2optimzd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2optimzd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2optimzd2(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_bbvzd2optimzd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2optimzd2(obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2optimzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_simplifyzd2branchz12zc0zzsaw_bbvzd2optimzd2(BgL_globalz00_bglt,
		BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_removezd2gotoz12zc0zzsaw_bbvzd2optimzd2(BgL_globalz00_bglt,
		BgL_blockz00_bglt);
	static obj_t BGl_objectzd2initzd2zzsaw_bbvzd2optimzd2(void);
	static bool_t BGl_coz12ze70zf5zzsaw_bbvzd2optimzd2(obj_t, obj_t);
	static obj_t BGl_z62coalescez12z70zzsaw_bbvzd2optimzd2(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_stringzd2containszd2zz__r4_strings_6_7z00(obj_t, obj_t,
		int);
	extern obj_t BGl_regsetz00zzsaw_regsetz00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	static obj_t BGl_z62zc3z04anonymousza32019ze3ze5zzsaw_bbvzd2optimzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2optimzd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2;
	extern obj_t BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(BgL_blockz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2optimzd2(obj_t,
		obj_t);
	static obj_t BGl_appendzd221011zd2zzsaw_bbvzd2optimzd2(obj_t, obj_t);
	extern obj_t BGl_rtl_nopz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t
		BGl_coalescez12z12zzsaw_bbvzd2optimzd2(BgL_globalz00_bglt,
		BgL_blockz00_bglt);
	static obj_t BGl_methodzd2initzd2zzsaw_bbvzd2optimzd2(void);
	static obj_t BGl_bbsetzd2conszd2zzsaw_bbvzd2optimzd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2optimzd2(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2optimzd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_coalescezf3zf3zzsaw_bbvzd2optimzd2(BgL_blockz00_bglt,
		BgL_blockz00_bglt, long);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2optimzd2(void);
	static BgL_typez00_bglt
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2optimzd2(obj_t, obj_t);
	static obj_t BGl_z62simplifyzd2branchz12za2zzsaw_bbvzd2optimzd2(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt);
	extern bool_t BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2optimzd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2optimzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2optimzd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2optimzd2(obj_t,
		obj_t);
	static obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2optimzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2optimzd2(BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2optimzd2(BgL_regsetz00_bglt,
		obj_t);
	extern obj_t BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(obj_t);
	BGL_IMPORT obj_t BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t BGl_getzd2bbzd2markz00zzsaw_bbvzd2typeszd2(void);
	static obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2optimzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2optimzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2debugzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2livenesszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2mergezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2utilszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2specializa7ez75(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2optimzd2(obj_t);
	static obj_t BGl_z62removezd2gotoz12za2zzsaw_bbvzd2optimzd2(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2optimzd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2optimzd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2optimzd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2optimzd2(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2optimzd2(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2optimzd2(void);
	BGL_IMPORT obj_t bgl_remq(obj_t, obj_t);
	static obj_t BGl_gotozd2targetzd2zzsaw_bbvzd2optimzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2optimzd2
		(BgL_rtl_regz00_bglt, int);
	static obj_t BGl_makezd2emptyzd2bbsetz00zzsaw_bbvzd2optimzd2(void);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2optimzd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_filterz12z12zz__r4_control_features_6_9z00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_regsetzf3zf3zzsaw_bbvzd2optimzd2(obj_t);
	extern obj_t BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2;
	extern bool_t
		BGl_rtl_inszd2nopzf3z21zzsaw_bbvzd2typeszd2(BgL_rtl_insz00_bglt);
	static obj_t
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2optimzd2(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2optimzd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2optimzd2(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2optimzd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2optimzd2(BgL_regsetz00_bglt);
	static obj_t BGl_coalescezd2blockz12zc0zzsaw_bbvzd2optimzd2(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2optimzd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_removezd2nopz12zc0zzsaw_bbvzd2optimzd2(BgL_globalz00_bglt,
		BgL_blockz00_bglt);
	extern obj_t BGl_redirectzd2blockz12zc0zzsaw_bbvzd2utilszd2(BgL_blockz00_bglt,
		BgL_blockz00_bglt, BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2optimzd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2optimzd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31647ze3ze5zzsaw_bbvzd2optimzd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2optimzd2(void);
	extern obj_t BGl_listzd2replacezd2zzsaw_bbvzd2utilszd2(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2optimzd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2optimzd2(obj_t, obj_t);
	static bool_t BGl_gotozd2blockzf3z21zzsaw_bbvzd2optimzd2(obj_t);
	extern obj_t BGl_rtl_regzf2razf2zzsaw_regsetz00;
	static obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2optimzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_assertzd2blockszd2zzsaw_bbvzd2debugzd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2optimzd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2optimzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2optimzd2(BgL_regsetz00_bglt);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2optimzd2(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2optimzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2optimzd2(BgL_regsetz00_bglt,
		int);
	static obj_t BGl_z62zc3z04anonymousza31950ze3ze5zzsaw_bbvzd2optimzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2optimzd2(obj_t,
		obj_t);
	static obj_t __cnst[2];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2namezd2envzf2zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72287za7,
		BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2nilzd2envz00zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762regsetza7d2nilza72288za7,
		BGl_z62regsetzd2nilzb0zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2setz12zd2envz32zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72289za7,
		BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2envzf2zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72290za7,
		BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2266z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2266za700za7za7s2291za7, "remove-nop!", 11);
	      DEFINE_STRING(BGl_string2268z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2268za700za7za7s2292za7, "before nop!", 11);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2setz12zd2envzc1zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72293za7,
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2optimzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2269z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2269za700za7za7s2294za7, "nop", 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2setz12zd2envz32zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72295za7,
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2optimzd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2envzf2zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72296za7,
		BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2270z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2270za700za7za7s2297za7, "remove-goto!", 12);
	      DEFINE_STRING(BGl_string2271z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2271za700za7za7s2298za7, "before goto!", 12);
	      DEFINE_STRING(BGl_string2272z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2272za700za7za7s2299za7, "goto", 4);
	      DEFINE_STRING(BGl_string2273z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2273za700za7za7s2300za7, "simplify-branch!", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2267z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762za7c3za704anonymo2301za7,
		BGl_z62zc3z04anonymousza31647ze3ze5zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2274z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2274za700za7za7s2302za7, "simplify-branch!.1", 18);
	      DEFINE_STRING(BGl_string2275z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2275za700za7za7s2303za7, "before simplify-branch!.2a", 26);
	      DEFINE_STRING(BGl_string2276z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2276za700za7za7s2304za7, "before simplify-branch!.2b", 26);
	      DEFINE_STRING(BGl_string2277z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2277za700za7za7s2305za7, "before simplify-branch!", 23);
	      DEFINE_STRING(BGl_string2278z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2278za700za7za7s2306za7, "simplify", 8);
	      DEFINE_STRING(BGl_string2279z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2279za700za7za7s2307za7, "before coalesce!", 16);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2setz12zd2envz32zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72308za7,
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2optimzd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2regvzd2envz00zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762regsetza7d2regv2309z00,
		BGl_z62regsetzd2regvzb0zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2280z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2280za700za7za7s2310za7, "coalesce", 8);
	      DEFINE_STRING(BGl_string2282z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2282za700za7za7s2311za7, "coalesce!", 9);
	      DEFINE_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2283za700za7za7s2312za7, "saw_bbv-optim", 13);
	      DEFINE_STRING(BGl_string2284z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_string2284za700za7za7s2313za7, "* bbset ", 8);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_simplifyzd2branchz12zd2envz12zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762simplifyza7d2br2314z00,
		BGl_z62simplifyzd2branchz12za2zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2281z00zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762za7c3za704anonymo2315za7,
		BGl_z62zc3z04anonymousza31950ze3ze5zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2setz12zd2envzc0zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762regsetza7d2leng2316z00,
		BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2msiza7ezd2envza7zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762regsetza7d2msiza72317za7,
		BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_coalescez12zd2envzc0zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762coalesceza712za772318za7,
		BGl_z62coalescez12z70zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2envzf2zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72319za7,
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2setz12zd2envz32zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72320za7,
		BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2envzf2zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72321za7,
		BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2nilzd2envzf2zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72322za7,
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2envzf2zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72323za7,
		BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2numzd2envzf2zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72324za7,
		BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2setz12zd2envzc0zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762regsetza7d2stri2325z00,
		BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2setz12zd2envz32zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72326za7,
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2optimzd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2envz01zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72327za7,
		BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2envz00zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762regsetza7d2stri2328z00,
		BGl_z62regsetzd2stringzb0zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2setz12zd2envz32zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72329za7,
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2optimzd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2rtl_regzf2razd2envzf2zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762makeza7d2rtl_re2330z00,
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razf3zd2envzd3zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72331za7,
		BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2envz00zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762regsetza7d2leng2332z00,
		BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2envzf2zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72333za7,
		BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2envzf2zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72334za7,
		BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2hardwarezd2envzf2zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72335za7,
		BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2regsetzd2envz00zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762makeza7d2regset2336z00,
		BGl_z62makezd2regsetzb0zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_removezd2gotoz12zd2envz12zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762removeza7d2goto2337z00,
		BGl_z62removezd2gotoz12za2zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzf3zd2envz21zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762regsetza7f3za791za72338z00,
		BGl_z62regsetzf3z91zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2keyzd2envzf2zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72339za7,
		BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_removezd2nopz12zd2envz12zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762removeza7d2nopza72340za7,
		BGl_z62removezd2nopz12za2zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2setz12zd2envz32zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762rtl_regza7f2raza72341za7,
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2optimzd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2reglzd2envz00zzsaw_bbvzd2optimzd2,
		BgL_bgl_za762regsetza7d2regl2342z00,
		BGl_z62regsetzd2reglzb0zzsaw_bbvzd2optimzd2, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_bbvzd2optimzd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2optimzd2(long
		BgL_checksumz00_3638, char *BgL_fromz00_3639)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_bbvzd2optimzd2))
				{
					BGl_requirezd2initializa7ationz75zzsaw_bbvzd2optimzd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_bbvzd2optimzd2();
					BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2optimzd2();
					BGl_cnstzd2initzd2zzsaw_bbvzd2optimzd2();
					BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2optimzd2();
					return BGl_toplevelzd2initzd2zzsaw_bbvzd2optimzd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2optimzd2(void)
	{
		{	/* SawBbv/bbv-optim.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_bbv-optim");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_bbv-optim");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_bbv-optim");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_bbv-optim");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_bbv-optim");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_bbv-optim");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"saw_bbv-optim");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "saw_bbv-optim");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_bbv-optim");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_bbv-optim");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "saw_bbv-optim");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2optimzd2(void)
	{
		{	/* SawBbv/bbv-optim.scm 15 */
			{	/* SawBbv/bbv-optim.scm 15 */
				obj_t BgL_cportz00_3587;

				{	/* SawBbv/bbv-optim.scm 15 */
					obj_t BgL_stringz00_3594;

					BgL_stringz00_3594 = BGl_string2284z00zzsaw_bbvzd2optimzd2;
					{	/* SawBbv/bbv-optim.scm 15 */
						obj_t BgL_startz00_3595;

						BgL_startz00_3595 = BINT(0L);
						{	/* SawBbv/bbv-optim.scm 15 */
							obj_t BgL_endz00_3596;

							BgL_endz00_3596 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3594)));
							{	/* SawBbv/bbv-optim.scm 15 */

								BgL_cportz00_3587 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3594, BgL_startz00_3595, BgL_endz00_3596);
				}}}}
				{
					long BgL_iz00_3588;

					BgL_iz00_3588 = 1L;
				BgL_loopz00_3589:
					if ((BgL_iz00_3588 == -1L))
						{	/* SawBbv/bbv-optim.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* SawBbv/bbv-optim.scm 15 */
							{	/* SawBbv/bbv-optim.scm 15 */
								obj_t BgL_arg2286z00_3590;

								{	/* SawBbv/bbv-optim.scm 15 */

									{	/* SawBbv/bbv-optim.scm 15 */
										obj_t BgL_locationz00_3592;

										BgL_locationz00_3592 = BBOOL(((bool_t) 0));
										{	/* SawBbv/bbv-optim.scm 15 */

											BgL_arg2286z00_3590 =
												BGl_readz00zz__readerz00(BgL_cportz00_3587,
												BgL_locationz00_3592);
										}
									}
								}
								{	/* SawBbv/bbv-optim.scm 15 */
									int BgL_tmpz00_3668;

									BgL_tmpz00_3668 = (int) (BgL_iz00_3588);
									CNST_TABLE_SET(BgL_tmpz00_3668, BgL_arg2286z00_3590);
							}}
							{	/* SawBbv/bbv-optim.scm 15 */
								int BgL_auxz00_3593;

								BgL_auxz00_3593 = (int) ((BgL_iz00_3588 - 1L));
								{
									long BgL_iz00_3673;

									BgL_iz00_3673 = (long) (BgL_auxz00_3593);
									BgL_iz00_3588 = BgL_iz00_3673;
									goto BgL_loopz00_3589;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2optimzd2(void)
	{
		{	/* SawBbv/bbv-optim.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2optimzd2(void)
	{
		{	/* SawBbv/bbv-optim.scm 15 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzsaw_bbvzd2optimzd2(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_2160;

				BgL_headz00_2160 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2161;
					obj_t BgL_tailz00_2162;

					BgL_prevz00_2161 = BgL_headz00_2160;
					BgL_tailz00_2162 = BgL_l1z00_1;
				BgL_loopz00_2163:
					if (PAIRP(BgL_tailz00_2162))
						{
							obj_t BgL_newzd2prevzd2_2165;

							BgL_newzd2prevzd2_2165 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2162), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2161, BgL_newzd2prevzd2_2165);
							{
								obj_t BgL_tailz00_3683;
								obj_t BgL_prevz00_3682;

								BgL_prevz00_3682 = BgL_newzd2prevzd2_2165;
								BgL_tailz00_3683 = CDR(BgL_tailz00_2162);
								BgL_tailz00_2162 = BgL_tailz00_3683;
								BgL_prevz00_2161 = BgL_prevz00_3682;
								goto BgL_loopz00_2163;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_2160);
				}
			}
		}

	}



/* make-rtl_reg/ra */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2optimzd2(BgL_typez00_bglt
		BgL_type1179z00_3, obj_t BgL_var1180z00_4, obj_t BgL_onexprzf31181zf3_5,
		obj_t BgL_name1182z00_6, obj_t BgL_key1183z00_7,
		obj_t BgL_hardware1184z00_8, int BgL_num1185z00_9,
		obj_t BgL_color1186z00_10, obj_t BgL_coalesce1187z00_11,
		int BgL_occurrences1188z00_12, obj_t BgL_interfere1189z00_13,
		obj_t BgL_interfere21190z00_14)
	{
		{	/* SawMill/regset.sch 55 */
			{	/* SawMill/regset.sch 55 */
				BgL_rtl_regz00_bglt BgL_new1166z00_3598;

				{	/* SawMill/regset.sch 55 */
					BgL_rtl_regz00_bglt BgL_tmp1164z00_3599;
					BgL_rtl_regzf2razf2_bglt BgL_wide1165z00_3600;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_3686;

						{	/* SawMill/regset.sch 55 */
							BgL_rtl_regz00_bglt BgL_new1163z00_3601;

							BgL_new1163z00_3601 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/regset.sch 55 */
								long BgL_arg1489z00_3602;

								BgL_arg1489z00_3602 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1163z00_3601),
									BgL_arg1489z00_3602);
							}
							{	/* SawMill/regset.sch 55 */
								BgL_objectz00_bglt BgL_tmpz00_3691;

								BgL_tmpz00_3691 = ((BgL_objectz00_bglt) BgL_new1163z00_3601);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3691, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1163z00_3601);
							BgL_auxz00_3686 = BgL_new1163z00_3601;
						}
						BgL_tmp1164z00_3599 = ((BgL_rtl_regz00_bglt) BgL_auxz00_3686);
					}
					BgL_wide1165z00_3600 =
						((BgL_rtl_regzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regzf2razf2_bgl))));
					{	/* SawMill/regset.sch 55 */
						obj_t BgL_auxz00_3699;
						BgL_objectz00_bglt BgL_tmpz00_3697;

						BgL_auxz00_3699 = ((obj_t) BgL_wide1165z00_3600);
						BgL_tmpz00_3697 = ((BgL_objectz00_bglt) BgL_tmp1164z00_3599);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3697, BgL_auxz00_3699);
					}
					((BgL_objectz00_bglt) BgL_tmp1164z00_3599);
					{	/* SawMill/regset.sch 55 */
						long BgL_arg1485z00_3603;

						BgL_arg1485z00_3603 =
							BGL_CLASS_NUM(BGl_rtl_regzf2razf2zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1164z00_3599), BgL_arg1485z00_3603);
					}
					BgL_new1166z00_3598 = ((BgL_rtl_regz00_bglt) BgL_tmp1164z00_3599);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1166z00_3598)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1179z00_3), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3598)))->BgL_varz00) =
					((obj_t) BgL_var1180z00_4), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3598)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31181zf3_5), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3598)))->BgL_namez00) =
					((obj_t) BgL_name1182z00_6), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3598)))->BgL_keyz00) =
					((obj_t) BgL_key1183z00_7), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3598)))->BgL_debugnamez00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_3598)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1184z00_8), BUNSPEC);
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3721;

					{
						obj_t BgL_auxz00_3722;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3723;

							BgL_tmpz00_3723 = ((BgL_objectz00_bglt) BgL_new1166z00_3598);
							BgL_auxz00_3722 = BGL_OBJECT_WIDENING(BgL_tmpz00_3723);
						}
						BgL_auxz00_3721 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3722);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3721))->BgL_numz00) =
						((int) BgL_num1185z00_9), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3728;

					{
						obj_t BgL_auxz00_3729;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3730;

							BgL_tmpz00_3730 = ((BgL_objectz00_bglt) BgL_new1166z00_3598);
							BgL_auxz00_3729 = BGL_OBJECT_WIDENING(BgL_tmpz00_3730);
						}
						BgL_auxz00_3728 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3729);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3728))->
							BgL_colorz00) = ((obj_t) BgL_color1186z00_10), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3735;

					{
						obj_t BgL_auxz00_3736;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3737;

							BgL_tmpz00_3737 = ((BgL_objectz00_bglt) BgL_new1166z00_3598);
							BgL_auxz00_3736 = BGL_OBJECT_WIDENING(BgL_tmpz00_3737);
						}
						BgL_auxz00_3735 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3736);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3735))->
							BgL_coalescez00) = ((obj_t) BgL_coalesce1187z00_11), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3742;

					{
						obj_t BgL_auxz00_3743;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3744;

							BgL_tmpz00_3744 = ((BgL_objectz00_bglt) BgL_new1166z00_3598);
							BgL_auxz00_3743 = BGL_OBJECT_WIDENING(BgL_tmpz00_3744);
						}
						BgL_auxz00_3742 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3743);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3742))->
							BgL_occurrencesz00) = ((int) BgL_occurrences1188z00_12), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3749;

					{
						obj_t BgL_auxz00_3750;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3751;

							BgL_tmpz00_3751 = ((BgL_objectz00_bglt) BgL_new1166z00_3598);
							BgL_auxz00_3750 = BGL_OBJECT_WIDENING(BgL_tmpz00_3751);
						}
						BgL_auxz00_3749 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3750);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3749))->
							BgL_interferez00) = ((obj_t) BgL_interfere1189z00_13), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3756;

					{
						obj_t BgL_auxz00_3757;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3758;

							BgL_tmpz00_3758 = ((BgL_objectz00_bglt) BgL_new1166z00_3598);
							BgL_auxz00_3757 = BGL_OBJECT_WIDENING(BgL_tmpz00_3758);
						}
						BgL_auxz00_3756 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3757);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3756))->
							BgL_interfere2z00) = ((obj_t) BgL_interfere21190z00_14), BUNSPEC);
				}
				return BgL_new1166z00_3598;
			}
		}

	}



/* &make-rtl_reg/ra */
	BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3473, obj_t BgL_type1179z00_3474, obj_t BgL_var1180z00_3475,
		obj_t BgL_onexprzf31181zf3_3476, obj_t BgL_name1182z00_3477,
		obj_t BgL_key1183z00_3478, obj_t BgL_hardware1184z00_3479,
		obj_t BgL_num1185z00_3480, obj_t BgL_color1186z00_3481,
		obj_t BgL_coalesce1187z00_3482, obj_t BgL_occurrences1188z00_3483,
		obj_t BgL_interfere1189z00_3484, obj_t BgL_interfere21190z00_3485)
	{
		{	/* SawMill/regset.sch 55 */
			return
				BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2optimzd2(
				((BgL_typez00_bglt) BgL_type1179z00_3474), BgL_var1180z00_3475,
				BgL_onexprzf31181zf3_3476, BgL_name1182z00_3477, BgL_key1183z00_3478,
				BgL_hardware1184z00_3479, CINT(BgL_num1185z00_3480),
				BgL_color1186z00_3481, BgL_coalesce1187z00_3482,
				CINT(BgL_occurrences1188z00_3483), BgL_interfere1189z00_3484,
				BgL_interfere21190z00_3485);
		}

	}



/* rtl_reg/ra? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2optimzd2(obj_t
		BgL_objz00_15)
	{
		{	/* SawMill/regset.sch 56 */
			{	/* SawMill/regset.sch 56 */
				obj_t BgL_classz00_3604;

				BgL_classz00_3604 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_15))
					{	/* SawMill/regset.sch 56 */
						BgL_objectz00_bglt BgL_arg1807z00_3605;

						BgL_arg1807z00_3605 = (BgL_objectz00_bglt) (BgL_objz00_15);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 56 */
								long BgL_idxz00_3606;

								BgL_idxz00_3606 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3605);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3606 + 2L)) == BgL_classz00_3604);
							}
						else
							{	/* SawMill/regset.sch 56 */
								bool_t BgL_res2258z00_3609;

								{	/* SawMill/regset.sch 56 */
									obj_t BgL_oclassz00_3610;

									{	/* SawMill/regset.sch 56 */
										obj_t BgL_arg1815z00_3611;
										long BgL_arg1816z00_3612;

										BgL_arg1815z00_3611 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 56 */
											long BgL_arg1817z00_3613;

											BgL_arg1817z00_3613 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3605);
											BgL_arg1816z00_3612 = (BgL_arg1817z00_3613 - OBJECT_TYPE);
										}
										BgL_oclassz00_3610 =
											VECTOR_REF(BgL_arg1815z00_3611, BgL_arg1816z00_3612);
									}
									{	/* SawMill/regset.sch 56 */
										bool_t BgL__ortest_1115z00_3614;

										BgL__ortest_1115z00_3614 =
											(BgL_classz00_3604 == BgL_oclassz00_3610);
										if (BgL__ortest_1115z00_3614)
											{	/* SawMill/regset.sch 56 */
												BgL_res2258z00_3609 = BgL__ortest_1115z00_3614;
											}
										else
											{	/* SawMill/regset.sch 56 */
												long BgL_odepthz00_3615;

												{	/* SawMill/regset.sch 56 */
													obj_t BgL_arg1804z00_3616;

													BgL_arg1804z00_3616 = (BgL_oclassz00_3610);
													BgL_odepthz00_3615 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3616);
												}
												if ((2L < BgL_odepthz00_3615))
													{	/* SawMill/regset.sch 56 */
														obj_t BgL_arg1802z00_3617;

														{	/* SawMill/regset.sch 56 */
															obj_t BgL_arg1803z00_3618;

															BgL_arg1803z00_3618 = (BgL_oclassz00_3610);
															BgL_arg1802z00_3617 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3618,
																2L);
														}
														BgL_res2258z00_3609 =
															(BgL_arg1802z00_3617 == BgL_classz00_3604);
													}
												else
													{	/* SawMill/regset.sch 56 */
														BgL_res2258z00_3609 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2258z00_3609;
							}
					}
				else
					{	/* SawMill/regset.sch 56 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_reg/ra? */
	obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2optimzd2(obj_t BgL_envz00_3486,
		obj_t BgL_objz00_3487)
	{
		{	/* SawMill/regset.sch 56 */
			return BBOOL(BGl_rtl_regzf2razf3z01zzsaw_bbvzd2optimzd2(BgL_objz00_3487));
		}

	}



/* rtl_reg/ra-nil */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2optimzd2(void)
	{
		{	/* SawMill/regset.sch 57 */
			{	/* SawMill/regset.sch 57 */
				obj_t BgL_classz00_3187;

				BgL_classz00_3187 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				{	/* SawMill/regset.sch 57 */
					obj_t BgL__ortest_1117z00_3188;

					BgL__ortest_1117z00_3188 = BGL_CLASS_NIL(BgL_classz00_3187);
					if (CBOOL(BgL__ortest_1117z00_3188))
						{	/* SawMill/regset.sch 57 */
							return ((BgL_rtl_regz00_bglt) BgL__ortest_1117z00_3188);
						}
					else
						{	/* SawMill/regset.sch 57 */
							return
								((BgL_rtl_regz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3187));
						}
				}
			}
		}

	}



/* &rtl_reg/ra-nil */
	BgL_rtl_regz00_bglt BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3488)
	{
		{	/* SawMill/regset.sch 57 */
			return BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2optimzd2();
		}

	}



/* rtl_reg/ra-interfere2 */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt
		BgL_oz00_16)
	{
		{	/* SawMill/regset.sch 58 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3798;

				{
					obj_t BgL_auxz00_3799;

					{	/* SawMill/regset.sch 58 */
						BgL_objectz00_bglt BgL_tmpz00_3800;

						BgL_tmpz00_3800 = ((BgL_objectz00_bglt) BgL_oz00_16);
						BgL_auxz00_3799 = BGL_OBJECT_WIDENING(BgL_tmpz00_3800);
					}
					BgL_auxz00_3798 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3799);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3798))->
					BgL_interfere2z00);
			}
		}

	}



/* &rtl_reg/ra-interfere2 */
	obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3489, obj_t BgL_oz00_3490)
	{
		{	/* SawMill/regset.sch 58 */
			return
				BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3490));
		}

	}



/* rtl_reg/ra-interfere2-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2optimzd2
		(BgL_rtl_regz00_bglt BgL_oz00_17, obj_t BgL_vz00_18)
	{
		{	/* SawMill/regset.sch 59 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3807;

				{
					obj_t BgL_auxz00_3808;

					{	/* SawMill/regset.sch 59 */
						BgL_objectz00_bglt BgL_tmpz00_3809;

						BgL_tmpz00_3809 = ((BgL_objectz00_bglt) BgL_oz00_17);
						BgL_auxz00_3808 = BGL_OBJECT_WIDENING(BgL_tmpz00_3809);
					}
					BgL_auxz00_3807 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3808);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3807))->
						BgL_interfere2z00) = ((obj_t) BgL_vz00_18), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere2-set! */
	obj_t BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3491, obj_t BgL_oz00_3492, obj_t BgL_vz00_3493)
	{
		{	/* SawMill/regset.sch 59 */
			return
				BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3492), BgL_vz00_3493);
		}

	}



/* rtl_reg/ra-interfere */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt
		BgL_oz00_19)
	{
		{	/* SawMill/regset.sch 60 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3816;

				{
					obj_t BgL_auxz00_3817;

					{	/* SawMill/regset.sch 60 */
						BgL_objectz00_bglt BgL_tmpz00_3818;

						BgL_tmpz00_3818 = ((BgL_objectz00_bglt) BgL_oz00_19);
						BgL_auxz00_3817 = BGL_OBJECT_WIDENING(BgL_tmpz00_3818);
					}
					BgL_auxz00_3816 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3817);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3816))->
					BgL_interferez00);
			}
		}

	}



/* &rtl_reg/ra-interfere */
	obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3494, obj_t BgL_oz00_3495)
	{
		{	/* SawMill/regset.sch 60 */
			return
				BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3495));
		}

	}



/* rtl_reg/ra-interfere-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2optimzd2
		(BgL_rtl_regz00_bglt BgL_oz00_20, obj_t BgL_vz00_21)
	{
		{	/* SawMill/regset.sch 61 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3825;

				{
					obj_t BgL_auxz00_3826;

					{	/* SawMill/regset.sch 61 */
						BgL_objectz00_bglt BgL_tmpz00_3827;

						BgL_tmpz00_3827 = ((BgL_objectz00_bglt) BgL_oz00_20);
						BgL_auxz00_3826 = BGL_OBJECT_WIDENING(BgL_tmpz00_3827);
					}
					BgL_auxz00_3825 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3826);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3825))->
						BgL_interferez00) = ((obj_t) BgL_vz00_21), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere-set! */
	obj_t BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3496, obj_t BgL_oz00_3497, obj_t BgL_vz00_3498)
	{
		{	/* SawMill/regset.sch 61 */
			return
				BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3497), BgL_vz00_3498);
		}

	}



/* rtl_reg/ra-occurrences */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt
		BgL_oz00_22)
	{
		{	/* SawMill/regset.sch 62 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3834;

				{
					obj_t BgL_auxz00_3835;

					{	/* SawMill/regset.sch 62 */
						BgL_objectz00_bglt BgL_tmpz00_3836;

						BgL_tmpz00_3836 = ((BgL_objectz00_bglt) BgL_oz00_22);
						BgL_auxz00_3835 = BGL_OBJECT_WIDENING(BgL_tmpz00_3836);
					}
					BgL_auxz00_3834 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3835);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3834))->
					BgL_occurrencesz00);
			}
		}

	}



/* &rtl_reg/ra-occurrences */
	obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3499, obj_t BgL_oz00_3500)
	{
		{	/* SawMill/regset.sch 62 */
			return
				BINT(BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2optimzd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_3500)));
		}

	}



/* rtl_reg/ra-occurrences-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2optimzd2
		(BgL_rtl_regz00_bglt BgL_oz00_23, int BgL_vz00_24)
	{
		{	/* SawMill/regset.sch 63 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3844;

				{
					obj_t BgL_auxz00_3845;

					{	/* SawMill/regset.sch 63 */
						BgL_objectz00_bglt BgL_tmpz00_3846;

						BgL_tmpz00_3846 = ((BgL_objectz00_bglt) BgL_oz00_23);
						BgL_auxz00_3845 = BGL_OBJECT_WIDENING(BgL_tmpz00_3846);
					}
					BgL_auxz00_3844 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3845);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3844))->
						BgL_occurrencesz00) = ((int) BgL_vz00_24), BUNSPEC);
		}}

	}



/* &rtl_reg/ra-occurrences-set! */
	obj_t BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3501, obj_t BgL_oz00_3502, obj_t BgL_vz00_3503)
	{
		{	/* SawMill/regset.sch 63 */
			return
				BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3502), CINT(BgL_vz00_3503));
		}

	}



/* rtl_reg/ra-coalesce */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt
		BgL_oz00_25)
	{
		{	/* SawMill/regset.sch 64 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3854;

				{
					obj_t BgL_auxz00_3855;

					{	/* SawMill/regset.sch 64 */
						BgL_objectz00_bglt BgL_tmpz00_3856;

						BgL_tmpz00_3856 = ((BgL_objectz00_bglt) BgL_oz00_25);
						BgL_auxz00_3855 = BGL_OBJECT_WIDENING(BgL_tmpz00_3856);
					}
					BgL_auxz00_3854 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3855);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3854))->
					BgL_coalescez00);
			}
		}

	}



/* &rtl_reg/ra-coalesce */
	obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3504, obj_t BgL_oz00_3505)
	{
		{	/* SawMill/regset.sch 64 */
			return
				BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3505));
		}

	}



/* rtl_reg/ra-coalesce-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2optimzd2
		(BgL_rtl_regz00_bglt BgL_oz00_26, obj_t BgL_vz00_27)
	{
		{	/* SawMill/regset.sch 65 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3863;

				{
					obj_t BgL_auxz00_3864;

					{	/* SawMill/regset.sch 65 */
						BgL_objectz00_bglt BgL_tmpz00_3865;

						BgL_tmpz00_3865 = ((BgL_objectz00_bglt) BgL_oz00_26);
						BgL_auxz00_3864 = BGL_OBJECT_WIDENING(BgL_tmpz00_3865);
					}
					BgL_auxz00_3863 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3864);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3863))->
						BgL_coalescez00) = ((obj_t) BgL_vz00_27), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-coalesce-set! */
	obj_t BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3506, obj_t BgL_oz00_3507, obj_t BgL_vz00_3508)
	{
		{	/* SawMill/regset.sch 65 */
			return
				BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3507), BgL_vz00_3508);
		}

	}



/* rtl_reg/ra-color */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt
		BgL_oz00_28)
	{
		{	/* SawMill/regset.sch 66 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3872;

				{
					obj_t BgL_auxz00_3873;

					{	/* SawMill/regset.sch 66 */
						BgL_objectz00_bglt BgL_tmpz00_3874;

						BgL_tmpz00_3874 = ((BgL_objectz00_bglt) BgL_oz00_28);
						BgL_auxz00_3873 = BGL_OBJECT_WIDENING(BgL_tmpz00_3874);
					}
					BgL_auxz00_3872 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3873);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3872))->BgL_colorz00);
			}
		}

	}



/* &rtl_reg/ra-color */
	obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3509, obj_t BgL_oz00_3510)
	{
		{	/* SawMill/regset.sch 66 */
			return
				BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3510));
		}

	}



/* rtl_reg/ra-color-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt
		BgL_oz00_29, obj_t BgL_vz00_30)
	{
		{	/* SawMill/regset.sch 67 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3881;

				{
					obj_t BgL_auxz00_3882;

					{	/* SawMill/regset.sch 67 */
						BgL_objectz00_bglt BgL_tmpz00_3883;

						BgL_tmpz00_3883 = ((BgL_objectz00_bglt) BgL_oz00_29);
						BgL_auxz00_3882 = BGL_OBJECT_WIDENING(BgL_tmpz00_3883);
					}
					BgL_auxz00_3881 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3882);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3881))->
						BgL_colorz00) = ((obj_t) BgL_vz00_30), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-color-set! */
	obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3511, obj_t BgL_oz00_3512, obj_t BgL_vz00_3513)
	{
		{	/* SawMill/regset.sch 67 */
			return
				BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3512), BgL_vz00_3513);
		}

	}



/* rtl_reg/ra-num */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt
		BgL_oz00_31)
	{
		{	/* SawMill/regset.sch 68 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3890;

				{
					obj_t BgL_auxz00_3891;

					{	/* SawMill/regset.sch 68 */
						BgL_objectz00_bglt BgL_tmpz00_3892;

						BgL_tmpz00_3892 = ((BgL_objectz00_bglt) BgL_oz00_31);
						BgL_auxz00_3891 = BGL_OBJECT_WIDENING(BgL_tmpz00_3892);
					}
					BgL_auxz00_3890 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3891);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3890))->BgL_numz00);
			}
		}

	}



/* &rtl_reg/ra-num */
	obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2optimzd2(obj_t BgL_envz00_3514,
		obj_t BgL_oz00_3515)
	{
		{	/* SawMill/regset.sch 68 */
			return
				BINT(BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2optimzd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_3515)));
		}

	}



/* rtl_reg/ra-hardware */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt
		BgL_oz00_34)
	{
		{	/* SawMill/regset.sch 70 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_34)))->BgL_hardwarez00);
		}

	}



/* &rtl_reg/ra-hardware */
	obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3516, obj_t BgL_oz00_3517)
	{
		{	/* SawMill/regset.sch 70 */
			return
				BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3517));
		}

	}



/* rtl_reg/ra-key */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt
		BgL_oz00_37)
	{
		{	/* SawMill/regset.sch 72 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_37)))->BgL_keyz00);
		}

	}



/* &rtl_reg/ra-key */
	obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2optimzd2(obj_t BgL_envz00_3518,
		obj_t BgL_oz00_3519)
	{
		{	/* SawMill/regset.sch 72 */
			return
				BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3519));
		}

	}



/* rtl_reg/ra-name */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt
		BgL_oz00_40)
	{
		{	/* SawMill/regset.sch 74 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_40)))->BgL_namez00);
		}

	}



/* &rtl_reg/ra-name */
	obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2optimzd2(obj_t BgL_envz00_3520,
		obj_t BgL_oz00_3521)
	{
		{	/* SawMill/regset.sch 74 */
			return
				BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3521));
		}

	}



/* rtl_reg/ra-onexpr? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt
		BgL_oz00_43)
	{
		{	/* SawMill/regset.sch 76 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_43)))->BgL_onexprzf3zf3);
		}

	}



/* &rtl_reg/ra-onexpr? */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3522, obj_t BgL_oz00_3523)
	{
		{	/* SawMill/regset.sch 76 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3523));
		}

	}



/* rtl_reg/ra-onexpr?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2optimzd2
		(BgL_rtl_regz00_bglt BgL_oz00_44, obj_t BgL_vz00_45)
	{
		{	/* SawMill/regset.sch 77 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_44)))->BgL_onexprzf3zf3) =
				((obj_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &rtl_reg/ra-onexpr?-set! */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3524, obj_t BgL_oz00_3525, obj_t BgL_vz00_3526)
	{
		{	/* SawMill/regset.sch 77 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3525), BgL_vz00_3526);
		}

	}



/* rtl_reg/ra-var */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt
		BgL_oz00_46)
	{
		{	/* SawMill/regset.sch 78 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_46)))->BgL_varz00);
		}

	}



/* &rtl_reg/ra-var */
	obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2optimzd2(obj_t BgL_envz00_3527,
		obj_t BgL_oz00_3528)
	{
		{	/* SawMill/regset.sch 78 */
			return
				BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3528));
		}

	}



/* rtl_reg/ra-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt
		BgL_oz00_47, obj_t BgL_vz00_48)
	{
		{	/* SawMill/regset.sch 79 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_47)))->BgL_varz00) =
				((obj_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &rtl_reg/ra-var-set! */
	obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3529, obj_t BgL_oz00_3530, obj_t BgL_vz00_3531)
	{
		{	/* SawMill/regset.sch 79 */
			return
				BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3530), BgL_vz00_3531);
		}

	}



/* rtl_reg/ra-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt
		BgL_oz00_49)
	{
		{	/* SawMill/regset.sch 80 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_49)))->BgL_typez00);
		}

	}



/* &rtl_reg/ra-type */
	BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3532, obj_t BgL_oz00_3533)
	{
		{	/* SawMill/regset.sch 80 */
			return
				BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3533));
		}

	}



/* rtl_reg/ra-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2optimzd2(BgL_rtl_regz00_bglt
		BgL_oz00_50, BgL_typez00_bglt BgL_vz00_51)
	{
		{	/* SawMill/regset.sch 81 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_50)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_51), BUNSPEC);
		}

	}



/* &rtl_reg/ra-type-set! */
	obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3534, obj_t BgL_oz00_3535, obj_t BgL_vz00_3536)
	{
		{	/* SawMill/regset.sch 81 */
			return
				BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2optimzd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_3535),
				((BgL_typez00_bglt) BgL_vz00_3536));
		}

	}



/* make-regset */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2optimzd2(int BgL_length1172z00_52,
		int BgL_msiza7e1173za7_53, obj_t BgL_regv1174z00_54,
		obj_t BgL_regl1175z00_55, obj_t BgL_string1176z00_56)
	{
		{	/* SawMill/regset.sch 84 */
			{	/* SawMill/regset.sch 84 */
				BgL_regsetz00_bglt BgL_new1168z00_3619;

				{	/* SawMill/regset.sch 84 */
					BgL_regsetz00_bglt BgL_new1167z00_3620;

					BgL_new1167z00_3620 =
						((BgL_regsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_regsetz00_bgl))));
					{	/* SawMill/regset.sch 84 */
						long BgL_arg1502z00_3621;

						BgL_arg1502z00_3621 = BGL_CLASS_NUM(BGl_regsetz00zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1167z00_3620), BgL_arg1502z00_3621);
					}
					BgL_new1168z00_3619 = BgL_new1167z00_3620;
				}
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3619))->BgL_lengthz00) =
					((int) BgL_length1172z00_52), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3619))->BgL_msiza7eza7) =
					((int) BgL_msiza7e1173za7_53), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3619))->BgL_regvz00) =
					((obj_t) BgL_regv1174z00_54), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3619))->BgL_reglz00) =
					((obj_t) BgL_regl1175z00_55), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_3619))->BgL_stringz00) =
					((obj_t) BgL_string1176z00_56), BUNSPEC);
				return BgL_new1168z00_3619;
			}
		}

	}



/* &make-regset */
	BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3537, obj_t BgL_length1172z00_3538,
		obj_t BgL_msiza7e1173za7_3539, obj_t BgL_regv1174z00_3540,
		obj_t BgL_regl1175z00_3541, obj_t BgL_string1176z00_3542)
	{
		{	/* SawMill/regset.sch 84 */
			return
				BGl_makezd2regsetzd2zzsaw_bbvzd2optimzd2(CINT(BgL_length1172z00_3538),
				CINT(BgL_msiza7e1173za7_3539), BgL_regv1174z00_3540,
				BgL_regl1175z00_3541, BgL_string1176z00_3542);
		}

	}



/* regset? */
	BGL_EXPORTED_DEF bool_t BGl_regsetzf3zf3zzsaw_bbvzd2optimzd2(obj_t
		BgL_objz00_57)
	{
		{	/* SawMill/regset.sch 85 */
			{	/* SawMill/regset.sch 85 */
				obj_t BgL_classz00_3622;

				BgL_classz00_3622 = BGl_regsetz00zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_57))
					{	/* SawMill/regset.sch 85 */
						BgL_objectz00_bglt BgL_arg1807z00_3623;

						BgL_arg1807z00_3623 = (BgL_objectz00_bglt) (BgL_objz00_57);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 85 */
								long BgL_idxz00_3624;

								BgL_idxz00_3624 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3623);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3624 + 1L)) == BgL_classz00_3622);
							}
						else
							{	/* SawMill/regset.sch 85 */
								bool_t BgL_res2259z00_3627;

								{	/* SawMill/regset.sch 85 */
									obj_t BgL_oclassz00_3628;

									{	/* SawMill/regset.sch 85 */
										obj_t BgL_arg1815z00_3629;
										long BgL_arg1816z00_3630;

										BgL_arg1815z00_3629 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 85 */
											long BgL_arg1817z00_3631;

											BgL_arg1817z00_3631 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3623);
											BgL_arg1816z00_3630 = (BgL_arg1817z00_3631 - OBJECT_TYPE);
										}
										BgL_oclassz00_3628 =
											VECTOR_REF(BgL_arg1815z00_3629, BgL_arg1816z00_3630);
									}
									{	/* SawMill/regset.sch 85 */
										bool_t BgL__ortest_1115z00_3632;

										BgL__ortest_1115z00_3632 =
											(BgL_classz00_3622 == BgL_oclassz00_3628);
										if (BgL__ortest_1115z00_3632)
											{	/* SawMill/regset.sch 85 */
												BgL_res2259z00_3627 = BgL__ortest_1115z00_3632;
											}
										else
											{	/* SawMill/regset.sch 85 */
												long BgL_odepthz00_3633;

												{	/* SawMill/regset.sch 85 */
													obj_t BgL_arg1804z00_3634;

													BgL_arg1804z00_3634 = (BgL_oclassz00_3628);
													BgL_odepthz00_3633 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3634);
												}
												if ((1L < BgL_odepthz00_3633))
													{	/* SawMill/regset.sch 85 */
														obj_t BgL_arg1802z00_3635;

														{	/* SawMill/regset.sch 85 */
															obj_t BgL_arg1803z00_3636;

															BgL_arg1803z00_3636 = (BgL_oclassz00_3628);
															BgL_arg1802z00_3635 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3636,
																1L);
														}
														BgL_res2259z00_3627 =
															(BgL_arg1802z00_3635 == BgL_classz00_3622);
													}
												else
													{	/* SawMill/regset.sch 85 */
														BgL_res2259z00_3627 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2259z00_3627;
							}
					}
				else
					{	/* SawMill/regset.sch 85 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &regset? */
	obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2optimzd2(obj_t BgL_envz00_3543,
		obj_t BgL_objz00_3544)
	{
		{	/* SawMill/regset.sch 85 */
			return BBOOL(BGl_regsetzf3zf3zzsaw_bbvzd2optimzd2(BgL_objz00_3544));
		}

	}



/* regset-nil */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2optimzd2(void)
	{
		{	/* SawMill/regset.sch 86 */
			{	/* SawMill/regset.sch 86 */
				obj_t BgL_classz00_3240;

				BgL_classz00_3240 = BGl_regsetz00zzsaw_regsetz00;
				{	/* SawMill/regset.sch 86 */
					obj_t BgL__ortest_1117z00_3241;

					BgL__ortest_1117z00_3241 = BGL_CLASS_NIL(BgL_classz00_3240);
					if (CBOOL(BgL__ortest_1117z00_3241))
						{	/* SawMill/regset.sch 86 */
							return ((BgL_regsetz00_bglt) BgL__ortest_1117z00_3241);
						}
					else
						{	/* SawMill/regset.sch 86 */
							return
								((BgL_regsetz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_3240));
						}
				}
			}
		}

	}



/* &regset-nil */
	BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3545)
	{
		{	/* SawMill/regset.sch 86 */
			return BGl_regsetzd2nilzd2zzsaw_bbvzd2optimzd2();
		}

	}



/* regset-string */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2optimzd2(BgL_regsetz00_bglt BgL_oz00_58)
	{
		{	/* SawMill/regset.sch 87 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_58))->BgL_stringz00);
		}

	}



/* &regset-string */
	obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2optimzd2(obj_t BgL_envz00_3546,
		obj_t BgL_oz00_3547)
	{
		{	/* SawMill/regset.sch 87 */
			return
				BGl_regsetzd2stringzd2zzsaw_bbvzd2optimzd2(
				((BgL_regsetz00_bglt) BgL_oz00_3547));
		}

	}



/* regset-string-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2optimzd2(BgL_regsetz00_bglt
		BgL_oz00_59, obj_t BgL_vz00_60)
	{
		{	/* SawMill/regset.sch 88 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_59))->BgL_stringz00) =
				((obj_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &regset-string-set! */
	obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3548, obj_t BgL_oz00_3549, obj_t BgL_vz00_3550)
	{
		{	/* SawMill/regset.sch 88 */
			return
				BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2optimzd2(
				((BgL_regsetz00_bglt) BgL_oz00_3549), BgL_vz00_3550);
		}

	}



/* regset-regl */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2optimzd2(BgL_regsetz00_bglt BgL_oz00_61)
	{
		{	/* SawMill/regset.sch 89 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_61))->BgL_reglz00);
		}

	}



/* &regset-regl */
	obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2optimzd2(obj_t BgL_envz00_3551,
		obj_t BgL_oz00_3552)
	{
		{	/* SawMill/regset.sch 89 */
			return
				BGl_regsetzd2reglzd2zzsaw_bbvzd2optimzd2(
				((BgL_regsetz00_bglt) BgL_oz00_3552));
		}

	}



/* regset-regv */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2optimzd2(BgL_regsetz00_bglt BgL_oz00_64)
	{
		{	/* SawMill/regset.sch 91 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_64))->BgL_regvz00);
		}

	}



/* &regset-regv */
	obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2optimzd2(obj_t BgL_envz00_3553,
		obj_t BgL_oz00_3554)
	{
		{	/* SawMill/regset.sch 91 */
			return
				BGl_regsetzd2regvzd2zzsaw_bbvzd2optimzd2(
				((BgL_regsetz00_bglt) BgL_oz00_3554));
		}

	}



/* regset-msize */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2optimzd2(BgL_regsetz00_bglt BgL_oz00_67)
	{
		{	/* SawMill/regset.sch 93 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_67))->BgL_msiza7eza7);
		}

	}



/* &regset-msize */
	obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2optimzd2(obj_t BgL_envz00_3555,
		obj_t BgL_oz00_3556)
	{
		{	/* SawMill/regset.sch 93 */
			return
				BINT(BGl_regsetzd2msiza7ez75zzsaw_bbvzd2optimzd2(
					((BgL_regsetz00_bglt) BgL_oz00_3556)));
		}

	}



/* regset-length */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2optimzd2(BgL_regsetz00_bglt BgL_oz00_70)
	{
		{	/* SawMill/regset.sch 95 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_70))->BgL_lengthz00);
		}

	}



/* &regset-length */
	obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2optimzd2(obj_t BgL_envz00_3557,
		obj_t BgL_oz00_3558)
	{
		{	/* SawMill/regset.sch 95 */
			return
				BINT(BGl_regsetzd2lengthzd2zzsaw_bbvzd2optimzd2(
					((BgL_regsetz00_bglt) BgL_oz00_3558)));
		}

	}



/* regset-length-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2optimzd2(BgL_regsetz00_bglt
		BgL_oz00_71, int BgL_vz00_72)
	{
		{	/* SawMill/regset.sch 96 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_71))->BgL_lengthz00) =
				((int) BgL_vz00_72), BUNSPEC);
		}

	}



/* &regset-length-set! */
	obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3559, obj_t BgL_oz00_3560, obj_t BgL_vz00_3561)
	{
		{	/* SawMill/regset.sch 96 */
			return
				BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2optimzd2(
				((BgL_regsetz00_bglt) BgL_oz00_3560), CINT(BgL_vz00_3561));
		}

	}



/* make-empty-bbset */
	obj_t BGl_makezd2emptyzd2bbsetz00zzsaw_bbvzd2optimzd2(void)
	{
		{	/* SawMill/bbset.sch 20 */
			{	/* SawMill/bbset.sch 21 */
				obj_t BgL_arg1544z00_2188;

				BgL_arg1544z00_2188 = BGl_getzd2bbzd2markz00zzsaw_bbvzd2typeszd2();
				{	/* SawMill/bbset.sch 15 */
					obj_t BgL_newz00_3249;

					BgL_newz00_3249 = create_struct(CNST_TABLE_REF(0), (int) (2L));
					{	/* SawMill/bbset.sch 15 */
						int BgL_tmpz00_4008;

						BgL_tmpz00_4008 = (int) (1L);
						STRUCT_SET(BgL_newz00_3249, BgL_tmpz00_4008, BNIL);
					}
					{	/* SawMill/bbset.sch 15 */
						int BgL_tmpz00_4011;

						BgL_tmpz00_4011 = (int) (0L);
						STRUCT_SET(BgL_newz00_3249, BgL_tmpz00_4011, BgL_arg1544z00_2188);
					}
					return BgL_newz00_3249;
				}
			}
		}

	}



/* bbset-cons */
	obj_t BGl_bbsetzd2conszd2zzsaw_bbvzd2optimzd2(BgL_blockz00_bglt BgL_bz00_85,
		obj_t BgL_setz00_86)
	{
		{	/* SawMill/bbset.sch 33 */
			{	/* SawMill/bbset.sch 34 */
				obj_t BgL_mz00_2192;

				BgL_mz00_2192 = STRUCT_REF(BgL_setz00_86, (int) (0L));
				{
					BgL_blocksz00_bglt BgL_auxz00_4016;

					{
						obj_t BgL_auxz00_4017;

						{	/* SawMill/bbset.sch 36 */
							BgL_objectz00_bglt BgL_tmpz00_4018;

							BgL_tmpz00_4018 = ((BgL_objectz00_bglt) BgL_bz00_85);
							BgL_auxz00_4017 = BGL_OBJECT_WIDENING(BgL_tmpz00_4018);
						}
						BgL_auxz00_4016 = ((BgL_blocksz00_bglt) BgL_auxz00_4017);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4016))->BgL_z52markz52) =
						((long) (long) CINT(BgL_mz00_2192)), BUNSPEC);
				}
				{	/* SawMill/bbset.sch 37 */
					obj_t BgL_arg1553z00_2194;

					{	/* SawMill/bbset.sch 37 */
						obj_t BgL_arg1559z00_2195;

						BgL_arg1559z00_2195 = STRUCT_REF(BgL_setz00_86, (int) (1L));
						BgL_arg1553z00_2194 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_85), BgL_arg1559z00_2195);
					}
					{	/* SawMill/bbset.sch 15 */
						int BgL_tmpz00_4028;

						BgL_tmpz00_4028 = (int) (1L);
						STRUCT_SET(BgL_setz00_86, BgL_tmpz00_4028, BgL_arg1553z00_2194);
				}}
				return BgL_setz00_86;
			}
		}

	}



/* remove-nop! */
	BGL_EXPORTED_DEF obj_t
		BGl_removezd2nopz12zc0zzsaw_bbvzd2optimzd2(BgL_globalz00_bglt
		BgL_globalz00_91, BgL_blockz00_bglt BgL_bz00_92)
	{
		{	/* SawBbv/bbv-optim.scm 50 */
			{
				BgL_blockz00_bglt BgL_bz00_2230;

				BGl_assertzd2blockszd2zzsaw_bbvzd2debugzd2(
					((BgL_blockz00_bglt) BgL_bz00_92),
					BGl_string2268z00zzsaw_bbvzd2optimzd2);
				{	/* SawBbv/bbv-optim.scm 82 */
					bool_t BgL_test2356z00_4033;

					if (
						(BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2 == BTRUE))
						{	/* SawBbv/bbv-optim.scm 82 */
							BgL_test2356z00_4033 = ((bool_t) 1);
						}
					else
						{	/* SawBbv/bbv-optim.scm 82 */
							if (STRINGP
								(BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2))
								{	/* SawBbv/bbv-optim.scm 84 */
									obj_t BgL_g1868z00_2227;

									BgL_g1868z00_2227 =
										BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2;
									{	/* SawBbv/bbv-optim.scm 84 */

										BgL_test2356z00_4033 =
											CBOOL(BGl_stringzd2containszd2zz__r4_strings_6_7z00
											(BgL_g1868z00_2227, BGl_string2269z00zzsaw_bbvzd2optimzd2,
												(int) (0L)));
								}}
							else
								{	/* SawBbv/bbv-optim.scm 83 */
									BgL_test2356z00_4033 = ((bool_t) 0);
								}
						}
					if (BgL_test2356z00_4033)
						{
							BgL_blockz00_bglt BgL_auxz00_4041;

							BgL_bz00_2230 = BgL_bz00_92;
							{	/* SawBbv/bbv-optim.scm 54 */
								obj_t BgL_g1172z00_2232;
								obj_t BgL_g1173z00_2233;

								{	/* SawBbv/bbv-optim.scm 54 */
									obj_t BgL_list1676z00_2281;

									BgL_list1676z00_2281 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_2230), BNIL);
									BgL_g1172z00_2232 = BgL_list1676z00_2281;
								}
								BgL_g1173z00_2233 =
									BGl_makezd2emptyzd2bbsetz00zzsaw_bbvzd2optimzd2();
								{
									obj_t BgL_bsz00_2235;
									obj_t BgL_accz00_2236;

									BgL_bsz00_2235 = BgL_g1172z00_2232;
									BgL_accz00_2236 = BgL_g1173z00_2233;
								BgL_zc3z04anonymousza31596ze3z87_2237:
									if (NULLP(BgL_bsz00_2235))
										{	/* SawBbv/bbv-optim.scm 57 */
											BgL_auxz00_4041 = BgL_bz00_2230;
										}
									else
										{	/* SawBbv/bbv-optim.scm 59 */
											bool_t BgL_test2360z00_4047;

											{	/* SawBbv/bbv-optim.scm 59 */
												BgL_blockz00_bglt BgL_blockz00_3263;

												BgL_blockz00_3263 =
													((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2235)));
												{	/* SawBbv/bbv-optim.scm 59 */
													long BgL_arg1546z00_3266;
													obj_t BgL_arg1552z00_3267;

													{
														BgL_blocksz00_bglt BgL_auxz00_4051;

														{
															obj_t BgL_auxz00_4052;

															{	/* SawBbv/bbv-optim.scm 59 */
																BgL_objectz00_bglt BgL_tmpz00_4053;

																BgL_tmpz00_4053 =
																	((BgL_objectz00_bglt) BgL_blockz00_3263);
																BgL_auxz00_4052 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_4053);
															}
															BgL_auxz00_4051 =
																((BgL_blocksz00_bglt) BgL_auxz00_4052);
														}
														BgL_arg1546z00_3266 =
															(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4051))->
															BgL_z52markz52);
													}
													BgL_arg1552z00_3267 =
														STRUCT_REF(BgL_accz00_2236, (int) (0L));
													BgL_test2360z00_4047 =
														(BINT(BgL_arg1546z00_3266) == BgL_arg1552z00_3267);
											}}
											if (BgL_test2360z00_4047)
												{	/* SawBbv/bbv-optim.scm 60 */
													obj_t BgL_arg1605z00_2241;

													BgL_arg1605z00_2241 = CDR(((obj_t) BgL_bsz00_2235));
													{
														obj_t BgL_bsz00_4064;

														BgL_bsz00_4064 = BgL_arg1605z00_2241;
														BgL_bsz00_2235 = BgL_bsz00_4064;
														goto BgL_zc3z04anonymousza31596ze3z87_2237;
													}
												}
											else
												{	/* SawBbv/bbv-optim.scm 62 */
													BgL_blockz00_bglt BgL_i1174z00_2242;

													BgL_i1174z00_2242 =
														((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2235)));
													if (CBOOL
														(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2))
														{	/* SawBbv/bbv-optim.scm 63 */
															obj_t BgL_arg1606z00_2243;

															BgL_arg1606z00_2243 =
																CAR(((obj_t) BgL_bsz00_2235));
															BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(
																((BgL_blockz00_bglt) BgL_arg1606z00_2243),
																BGl_string2266z00zzsaw_bbvzd2optimzd2);
														}
													else
														{	/* SawBbv/bbv-optim.scm 63 */
															BFALSE;
														}
													{	/* SawBbv/bbv-optim.scm 64 */
														obj_t BgL_fz00_2244;

														BgL_fz00_2244 =
															BGl_filterz12z12zz__r4_control_features_6_9z00
															(BGl_proc2267z00zzsaw_bbvzd2optimzd2,
															(((BgL_blockz00_bglt)
																	COBJECT(BgL_i1174z00_2242))->BgL_firstz00));
														if (NULLP(BgL_fz00_2244))
															{	/* SawBbv/bbv-optim.scm 66 */
																if (NULLP(
																		(((BgL_blockz00_bglt)
																				COBJECT(BgL_i1174z00_2242))->
																			BgL_succsz00)))
																	{
																		obj_t BgL_auxz00_4081;

																		{	/* SawBbv/bbv-optim.scm 77 */
																			BgL_rtl_insz00_bglt BgL_arg1625z00_2259;

																			{	/* SawBbv/bbv-optim.scm 77 */
																				BgL_rtl_insz00_bglt BgL_new1176z00_2261;

																				{	/* SawBbv/bbv-optim.scm 78 */
																					BgL_rtl_insz00_bglt
																						BgL_new1175z00_2265;
																					BgL_new1175z00_2265 =
																						((BgL_rtl_insz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_rtl_insz00_bgl))));
																					{	/* SawBbv/bbv-optim.scm 78 */
																						long BgL_arg1629z00_2266;

																						{	/* SawBbv/bbv-optim.scm 78 */
																							obj_t BgL_classz00_3278;

																							BgL_classz00_3278 =
																								BGl_rtl_insz00zzsaw_defsz00;
																							BgL_arg1629z00_2266 =
																								BGL_CLASS_NUM
																								(BgL_classz00_3278);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1175z00_2265),
																							BgL_arg1629z00_2266);
																					}
																					{	/* SawBbv/bbv-optim.scm 78 */
																						BgL_objectz00_bglt BgL_tmpz00_4086;

																						BgL_tmpz00_4086 =
																							((BgL_objectz00_bglt)
																							BgL_new1175z00_2265);
																						BGL_OBJECT_WIDENING_SET
																							(BgL_tmpz00_4086, BFALSE);
																					}
																					((BgL_objectz00_bglt)
																						BgL_new1175z00_2265);
																					BgL_new1176z00_2261 =
																						BgL_new1175z00_2265;
																				}
																				((((BgL_rtl_insz00_bglt)
																							COBJECT(BgL_new1176z00_2261))->
																						BgL_locz00) =
																					((obj_t) BFALSE), BUNSPEC);
																				((((BgL_rtl_insz00_bglt)
																							COBJECT(BgL_new1176z00_2261))->
																						BgL_z52spillz52) =
																					((obj_t) BNIL), BUNSPEC);
																				((((BgL_rtl_insz00_bglt)
																							COBJECT(BgL_new1176z00_2261))->
																						BgL_destz00) =
																					((obj_t) BFALSE), BUNSPEC);
																				{
																					BgL_rtl_funz00_bglt BgL_auxz00_4093;

																					{	/* SawBbv/bbv-optim.scm 79 */
																						BgL_rtl_nopz00_bglt
																							BgL_new1178z00_2262;
																						{	/* SawBbv/bbv-optim.scm 79 */
																							BgL_rtl_nopz00_bglt
																								BgL_new1177z00_2263;
																							BgL_new1177z00_2263 =
																								((BgL_rtl_nopz00_bglt)
																								BOBJECT(GC_MALLOC(sizeof(struct
																											BgL_rtl_nopz00_bgl))));
																							{	/* SawBbv/bbv-optim.scm 79 */
																								long BgL_arg1627z00_2264;

																								{	/* SawBbv/bbv-optim.scm 79 */
																									obj_t BgL_classz00_3282;

																									BgL_classz00_3282 =
																										BGl_rtl_nopz00zzsaw_defsz00;
																									BgL_arg1627z00_2264 =
																										BGL_CLASS_NUM
																										(BgL_classz00_3282);
																								}
																								BGL_OBJECT_CLASS_NUM_SET(
																									((BgL_objectz00_bglt)
																										BgL_new1177z00_2263),
																									BgL_arg1627z00_2264);
																							}
																							BgL_new1178z00_2262 =
																								BgL_new1177z00_2263;
																						}
																						((((BgL_rtl_funz00_bglt) COBJECT(
																										((BgL_rtl_funz00_bglt)
																											BgL_new1178z00_2262)))->
																								BgL_locz00) =
																							((obj_t) BFALSE), BUNSPEC);
																						BgL_auxz00_4093 =
																							((BgL_rtl_funz00_bglt)
																							BgL_new1178z00_2262);
																					}
																					((((BgL_rtl_insz00_bglt)
																								COBJECT(BgL_new1176z00_2261))->
																							BgL_funz00) =
																						((BgL_rtl_funz00_bglt)
																							BgL_auxz00_4093), BUNSPEC);
																				}
																				((((BgL_rtl_insz00_bglt)
																							COBJECT(BgL_new1176z00_2261))->
																						BgL_argsz00) =
																					((obj_t) BNIL), BUNSPEC);
																				BgL_arg1625z00_2259 =
																					BgL_new1176z00_2261;
																			}
																			{	/* SawBbv/bbv-optim.scm 77 */
																				obj_t BgL_list1626z00_2260;

																				BgL_list1626z00_2260 =
																					MAKE_YOUNG_PAIR(
																					((obj_t) BgL_arg1625z00_2259), BNIL);
																				BgL_auxz00_4081 = BgL_list1626z00_2260;
																		}}
																		((((BgL_blockz00_bglt)
																					COBJECT(BgL_i1174z00_2242))->
																				BgL_firstz00) =
																			((obj_t) BgL_auxz00_4081), BUNSPEC);
																	}
																else
																	{	/* SawBbv/bbv-optim.scm 71 */
																		obj_t BgL_succz00_2248;

																		BgL_succz00_2248 =
																			CAR(
																			(((BgL_blockz00_bglt)
																					COBJECT(BgL_i1174z00_2242))->
																				BgL_succsz00));
																		{	/* SawBbv/bbv-optim.scm 72 */
																			obj_t BgL_g1449z00_2249;

																			BgL_g1449z00_2249 =
																				(((BgL_blockz00_bglt)
																					COBJECT(BgL_i1174z00_2242))->
																				BgL_predsz00);
																			{
																				obj_t BgL_l1447z00_2251;

																				{	/* SawBbv/bbv-optim.scm 72 */
																					bool_t BgL_tmpz00_4109;

																					BgL_l1447z00_2251 = BgL_g1449z00_2249;
																				BgL_zc3z04anonymousza31611ze3z87_2252:
																					if (PAIRP(BgL_l1447z00_2251))
																						{	/* SawBbv/bbv-optim.scm 72 */
																							{	/* SawBbv/bbv-optim.scm 73 */
																								obj_t BgL_pz00_2254;

																								BgL_pz00_2254 =
																									CAR(BgL_l1447z00_2251);
																								{	/* SawBbv/bbv-optim.scm 73 */
																									obj_t BgL_arg1613z00_2255;

																									BgL_arg1613z00_2255 =
																										CAR(
																										((obj_t) BgL_bsz00_2235));
																									BGl_redirectzd2blockz12zc0zzsaw_bbvzd2utilszd2
																										(((BgL_blockz00_bglt)
																											BgL_pz00_2254),
																										((BgL_blockz00_bglt)
																											BgL_arg1613z00_2255),
																										((BgL_blockz00_bglt)
																											BgL_succz00_2248));
																								}
																							}
																							{
																								obj_t BgL_l1447z00_4119;

																								BgL_l1447z00_4119 =
																									CDR(BgL_l1447z00_2251);
																								BgL_l1447z00_2251 =
																									BgL_l1447z00_4119;
																								goto
																									BgL_zc3z04anonymousza31611ze3z87_2252;
																							}
																						}
																					else
																						{	/* SawBbv/bbv-optim.scm 72 */
																							BgL_tmpz00_4109 = ((bool_t) 1);
																						}
																					BBOOL(BgL_tmpz00_4109);
																				}
																			}
																		}
																	}
															}
														else
															{	/* SawBbv/bbv-optim.scm 66 */
																((((BgL_blockz00_bglt)
																			COBJECT(BgL_i1174z00_2242))->
																		BgL_firstz00) =
																	((obj_t) BgL_fz00_2244), BUNSPEC);
															}
													}
													{	/* SawBbv/bbv-optim.scm 80 */
														obj_t BgL_arg1650z00_2274;
														obj_t BgL_arg1651z00_2275;

														{	/* SawBbv/bbv-optim.scm 80 */
															obj_t BgL_arg1654z00_2276;
															obj_t BgL_arg1661z00_2277;

															BgL_arg1654z00_2276 =
																(((BgL_blockz00_bglt)
																	COBJECT(BgL_i1174z00_2242))->BgL_succsz00);
															BgL_arg1661z00_2277 =
																CDR(((obj_t) BgL_bsz00_2235));
															BgL_arg1650z00_2274 =
																BGl_appendzd221011zd2zzsaw_bbvzd2optimzd2
																(BgL_arg1654z00_2276, BgL_arg1661z00_2277);
														}
														{	/* SawBbv/bbv-optim.scm 80 */
															obj_t BgL_arg1663z00_2278;

															BgL_arg1663z00_2278 =
																CAR(((obj_t) BgL_bsz00_2235));
															BgL_arg1651z00_2275 =
																BGl_bbsetzd2conszd2zzsaw_bbvzd2optimzd2(
																((BgL_blockz00_bglt) BgL_arg1663z00_2278),
																BgL_accz00_2236);
														}
														{
															obj_t BgL_accz00_4132;
															obj_t BgL_bsz00_4131;

															BgL_bsz00_4131 = BgL_arg1650z00_2274;
															BgL_accz00_4132 = BgL_arg1651z00_2275;
															BgL_accz00_2236 = BgL_accz00_4132;
															BgL_bsz00_2235 = BgL_bsz00_4131;
															goto BgL_zc3z04anonymousza31596ze3z87_2237;
														}
													}
												}
										}
								}
							}
							return ((obj_t) BgL_auxz00_4041);
						}
					else
						{	/* SawBbv/bbv-optim.scm 82 */
							return ((obj_t) BgL_bz00_92);
						}
				}
			}
		}

	}



/* &remove-nop! */
	obj_t BGl_z62removezd2nopz12za2zzsaw_bbvzd2optimzd2(obj_t BgL_envz00_3563,
		obj_t BgL_globalz00_3564, obj_t BgL_bz00_3565)
	{
		{	/* SawBbv/bbv-optim.scm 50 */
			return
				BGl_removezd2nopz12zc0zzsaw_bbvzd2optimzd2(
				((BgL_globalz00_bglt) BgL_globalz00_3564),
				((BgL_blockz00_bglt) BgL_bz00_3565));
		}

	}



/* &<@anonymous:1647> */
	obj_t BGl_z62zc3z04anonymousza31647ze3ze5zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3566, obj_t BgL_iz00_3567)
	{
		{	/* SawBbv/bbv-optim.scm 64 */
			{	/* SawBbv/bbv-optim.scm 64 */
				bool_t BgL_tmpz00_4138;

				if (BGl_rtl_inszd2nopzf3z21zzsaw_bbvzd2typeszd2(
						((BgL_rtl_insz00_bglt) BgL_iz00_3567)))
					{	/* SawBbv/bbv-optim.scm 64 */
						BgL_tmpz00_4138 = ((bool_t) 0);
					}
				else
					{	/* SawBbv/bbv-optim.scm 64 */
						BgL_tmpz00_4138 = ((bool_t) 1);
					}
				return BBOOL(BgL_tmpz00_4138);
			}
		}

	}



/* remove-goto! */
	BGL_EXPORTED_DEF obj_t
		BGl_removezd2gotoz12zc0zzsaw_bbvzd2optimzd2(BgL_globalz00_bglt
		BgL_globalz00_93, BgL_blockz00_bglt BgL_bz00_94)
	{
		{	/* SawBbv/bbv-optim.scm 95 */
			{
				BgL_blockz00_bglt BgL_bz00_2315;
				obj_t BgL_bz00_2294;
				obj_t BgL_succsz00_2295;
				obj_t BgL_nextz00_2296;

				BGl_assertzd2blockszd2zzsaw_bbvzd2debugzd2(
					((BgL_blockz00_bglt) BgL_bz00_94),
					BGl_string2271z00zzsaw_bbvzd2optimzd2);
				{	/* SawBbv/bbv-optim.scm 131 */
					bool_t BgL_test2366z00_4145;

					if (
						(BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2 == BTRUE))
						{	/* SawBbv/bbv-optim.scm 131 */
							BgL_test2366z00_4145 = ((bool_t) 1);
						}
					else
						{	/* SawBbv/bbv-optim.scm 131 */
							if (STRINGP
								(BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2))
								{	/* SawBbv/bbv-optim.scm 133 */
									obj_t BgL_g1868z00_2291;

									BgL_g1868z00_2291 =
										BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2;
									{	/* SawBbv/bbv-optim.scm 133 */

										BgL_test2366z00_4145 =
											CBOOL(BGl_stringzd2containszd2zz__r4_strings_6_7z00
											(BgL_g1868z00_2291, BGl_string2272z00zzsaw_bbvzd2optimzd2,
												(int) (0L)));
								}}
							else
								{	/* SawBbv/bbv-optim.scm 132 */
									BgL_test2366z00_4145 = ((bool_t) 0);
								}
						}
					if (BgL_test2366z00_4145)
						{
							BgL_blockz00_bglt BgL_auxz00_4153;

							BgL_bz00_2315 = BgL_bz00_94;
							{	/* SawBbv/bbv-optim.scm 112 */
								obj_t BgL_g1183z00_2317;
								obj_t BgL_g1184z00_2318;

								{	/* SawBbv/bbv-optim.scm 112 */
									obj_t BgL_list1800z00_2370;

									BgL_list1800z00_2370 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_2315), BNIL);
									BgL_g1183z00_2317 = BgL_list1800z00_2370;
								}
								BgL_g1184z00_2318 =
									BGl_makezd2emptyzd2bbsetz00zzsaw_bbvzd2optimzd2();
								{
									obj_t BgL_bsz00_2320;
									obj_t BgL_accz00_2321;

									BgL_bsz00_2320 = BgL_g1183z00_2317;
									BgL_accz00_2321 = BgL_g1184z00_2318;
								BgL_zc3z04anonymousza31704ze3z87_2322:
									if (NULLP(BgL_bsz00_2320))
										{	/* SawBbv/bbv-optim.scm 115 */
											BgL_auxz00_4153 = BgL_bz00_2315;
										}
									else
										{	/* SawBbv/bbv-optim.scm 117 */
											bool_t BgL_test2370z00_4159;

											{	/* SawBbv/bbv-optim.scm 117 */
												BgL_blockz00_bglt BgL_blockz00_3293;

												BgL_blockz00_3293 =
													((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2320)));
												{	/* SawBbv/bbv-optim.scm 117 */
													long BgL_arg1546z00_3296;
													obj_t BgL_arg1552z00_3297;

													{
														BgL_blocksz00_bglt BgL_auxz00_4163;

														{
															obj_t BgL_auxz00_4164;

															{	/* SawBbv/bbv-optim.scm 117 */
																BgL_objectz00_bglt BgL_tmpz00_4165;

																BgL_tmpz00_4165 =
																	((BgL_objectz00_bglt) BgL_blockz00_3293);
																BgL_auxz00_4164 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_4165);
															}
															BgL_auxz00_4163 =
																((BgL_blocksz00_bglt) BgL_auxz00_4164);
														}
														BgL_arg1546z00_3296 =
															(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4163))->
															BgL_z52markz52);
													}
													BgL_arg1552z00_3297 =
														STRUCT_REF(BgL_accz00_2321, (int) (0L));
													BgL_test2370z00_4159 =
														(BINT(BgL_arg1546z00_3296) == BgL_arg1552z00_3297);
											}}
											if (BgL_test2370z00_4159)
												{	/* SawBbv/bbv-optim.scm 118 */
													obj_t BgL_arg1714z00_2326;

													BgL_arg1714z00_2326 = CDR(((obj_t) BgL_bsz00_2320));
													{
														obj_t BgL_bsz00_4176;

														BgL_bsz00_4176 = BgL_arg1714z00_2326;
														BgL_bsz00_2320 = BgL_bsz00_4176;
														goto BgL_zc3z04anonymousza31704ze3z87_2322;
													}
												}
											else
												{	/* SawBbv/bbv-optim.scm 117 */
													if (CBOOL
														(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2))
														{	/* SawBbv/bbv-optim.scm 120 */
															obj_t BgL_arg1717z00_2327;

															BgL_arg1717z00_2327 =
																CAR(((obj_t) BgL_bsz00_2320));
															BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(
																((BgL_blockz00_bglt) BgL_arg1717z00_2327),
																BGl_string2270z00zzsaw_bbvzd2optimzd2);
														}
													else
														{	/* SawBbv/bbv-optim.scm 120 */
															BFALSE;
														}
													{	/* SawBbv/bbv-optim.scm 121 */
														BgL_blockz00_bglt BgL_i1185z00_2328;

														BgL_i1185z00_2328 =
															((BgL_blockz00_bglt)
															CAR(((obj_t) BgL_bsz00_2320)));
														{	/* SawBbv/bbv-optim.scm 122 */
															bool_t BgL_test2372z00_4186;

															{	/* SawBbv/bbv-optim.scm 122 */
																bool_t BgL_test2373z00_4187;

																{	/* SawBbv/bbv-optim.scm 122 */
																	bool_t BgL_test2374z00_4188;

																	{	/* SawBbv/bbv-optim.scm 122 */
																		obj_t BgL_tmpz00_4189;

																		BgL_tmpz00_4189 =
																			CDR(((obj_t) BgL_bsz00_2320));
																		BgL_test2374z00_4188 =
																			PAIRP(BgL_tmpz00_4189);
																	}
																	if (BgL_test2374z00_4188)
																		{	/* SawBbv/bbv-optim.scm 123 */
																			obj_t BgL_arg1761z00_2359;
																			obj_t BgL_arg1762z00_2360;
																			obj_t BgL_arg1765z00_2361;

																			BgL_arg1761z00_2359 =
																				CAR(((obj_t) BgL_bsz00_2320));
																			BgL_arg1762z00_2360 =
																				(((BgL_blockz00_bglt)
																					COBJECT(BgL_i1185z00_2328))->
																				BgL_succsz00);
																			{	/* SawBbv/bbv-optim.scm 123 */
																				obj_t BgL_pairz00_3308;

																				BgL_pairz00_3308 =
																					CDR(((obj_t) BgL_bsz00_2320));
																				BgL_arg1765z00_2361 =
																					CAR(BgL_pairz00_3308);
																			}
																			BgL_bz00_2294 = BgL_arg1761z00_2359;
																			BgL_succsz00_2295 = BgL_arg1762z00_2360;
																			BgL_nextz00_2296 = BgL_arg1765z00_2361;
																			if (NULLP(BgL_succsz00_2295))
																				{	/* SawBbv/bbv-optim.scm 99 */
																					BgL_test2373z00_4187 = ((bool_t) 0);
																				}
																			else
																				{	/* SawBbv/bbv-optim.scm 99 */
																					if (NULLP(
																							(((BgL_blockz00_bglt) COBJECT(
																										((BgL_blockz00_bglt)
																											BgL_bz00_2294)))->
																								BgL_firstz00)))
																						{	/* SawBbv/bbv-optim.scm 101 */
																							BgL_test2373z00_4187 =
																								((bool_t) 0);
																						}
																					else
																						{	/* SawBbv/bbv-optim.scm 102 */
																							obj_t BgL_iz00_2302;

																							BgL_iz00_2302 =
																								CAR
																								(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
																								((((BgL_blockz00_bglt)
																											COBJECT((
																													(BgL_blockz00_bglt)
																													BgL_bz00_2294)))->
																										BgL_firstz00)));
																							if (BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2(((BgL_rtl_insz00_bglt) BgL_iz00_2302)))
																								{	/* SawBbv/bbv-optim.scm 105 */
																									BgL_rtl_goz00_bglt
																										BgL_i1182z00_2305;
																									BgL_i1182z00_2305 =
																										((BgL_rtl_goz00_bglt) ((
																												(BgL_rtl_insz00_bglt)
																												COBJECT((
																														(BgL_rtl_insz00_bglt)
																														BgL_iz00_2302)))->
																											BgL_funz00));
																									if ((((obj_t) ((
																														(BgL_rtl_goz00_bglt)
																														COBJECT
																														(BgL_i1182z00_2305))->
																													BgL_toz00)) ==
																											CAR(BgL_succsz00_2295)))
																										{	/* SawBbv/bbv-optim.scm 106 */
																											BgL_test2373z00_4187 =
																												(
																												((obj_t)
																													(((BgL_rtl_goz00_bglt)
																															COBJECT
																															(BgL_i1182z00_2305))->
																														BgL_toz00)) ==
																												BgL_nextz00_2296);
																										}
																									else
																										{	/* SawBbv/bbv-optim.scm 106 */
																											BgL_test2373z00_4187 =
																												((bool_t) 0);
																										}
																								}
																							else
																								{	/* SawBbv/bbv-optim.scm 103 */
																									BgL_test2373z00_4187 =
																										((bool_t) 0);
																								}
																						}
																				}
																		}
																	else
																		{	/* SawBbv/bbv-optim.scm 122 */
																			BgL_test2373z00_4187 = ((bool_t) 0);
																		}
																}
																if (BgL_test2373z00_4187)
																	{	/* SawBbv/bbv-optim.scm 124 */
																		bool_t BgL_test2379z00_4223;

																		{	/* SawBbv/bbv-optim.scm 124 */
																			BgL_blockz00_bglt BgL_blockz00_3310;

																			{	/* SawBbv/bbv-optim.scm 124 */
																				obj_t BgL_pairz00_3309;

																				BgL_pairz00_3309 =
																					(((BgL_blockz00_bglt)
																						COBJECT(BgL_i1185z00_2328))->
																					BgL_succsz00);
																				BgL_blockz00_3310 =
																					((BgL_blockz00_bglt)
																					CAR(BgL_pairz00_3309));
																			}
																			{	/* SawBbv/bbv-optim.scm 124 */
																				long BgL_arg1546z00_3313;
																				obj_t BgL_arg1552z00_3314;

																				{
																					BgL_blocksz00_bglt BgL_auxz00_4227;

																					{
																						obj_t BgL_auxz00_4228;

																						{	/* SawBbv/bbv-optim.scm 124 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_4229;
																							BgL_tmpz00_4229 =
																								((BgL_objectz00_bglt)
																								BgL_blockz00_3310);
																							BgL_auxz00_4228 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_4229);
																						}
																						BgL_auxz00_4227 =
																							((BgL_blocksz00_bglt)
																							BgL_auxz00_4228);
																					}
																					BgL_arg1546z00_3313 =
																						(((BgL_blocksz00_bglt)
																							COBJECT(BgL_auxz00_4227))->
																						BgL_z52markz52);
																				}
																				BgL_arg1552z00_3314 =
																					STRUCT_REF(BgL_accz00_2321,
																					(int) (0L));
																				BgL_test2379z00_4223 =
																					(BINT(BgL_arg1546z00_3313) ==
																					BgL_arg1552z00_3314);
																		}}
																		if (BgL_test2379z00_4223)
																			{	/* SawBbv/bbv-optim.scm 124 */
																				BgL_test2372z00_4186 = ((bool_t) 0);
																			}
																		else
																			{	/* SawBbv/bbv-optim.scm 124 */
																				BgL_test2372z00_4186 = ((bool_t) 1);
																			}
																	}
																else
																	{	/* SawBbv/bbv-optim.scm 122 */
																		BgL_test2372z00_4186 = ((bool_t) 0);
																	}
															}
															if (BgL_test2372z00_4186)
																{	/* SawBbv/bbv-optim.scm 125 */
																	BgL_blockz00_bglt BgL_i1186z00_2347;

																	BgL_i1186z00_2347 =
																		((BgL_blockz00_bglt)
																		CAR(((obj_t) BgL_bsz00_2320)));
																	{	/* SawBbv/bbv-optim.scm 126 */
																		BgL_rtl_insz00_bglt BgL_i1187z00_2348;

																		BgL_i1187z00_2348 =
																			((BgL_rtl_insz00_bglt)
																			CAR
																			(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
																				((((BgL_blockz00_bglt)
																							COBJECT(BgL_i1186z00_2347))->
																						BgL_firstz00))));
																		{
																			BgL_rtl_funz00_bglt BgL_auxz00_4245;

																			{	/* SawBbv/bbv-optim.scm 127 */
																				BgL_rtl_nopz00_bglt BgL_new1190z00_2349;

																				{	/* SawBbv/bbv-optim.scm 127 */
																					BgL_rtl_nopz00_bglt
																						BgL_new1189z00_2350;
																					BgL_new1189z00_2350 =
																						((BgL_rtl_nopz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_rtl_nopz00_bgl))));
																					{	/* SawBbv/bbv-optim.scm 127 */
																						long BgL_arg1751z00_2351;

																						{	/* SawBbv/bbv-optim.scm 127 */
																							obj_t BgL_classz00_3319;

																							BgL_classz00_3319 =
																								BGl_rtl_nopz00zzsaw_defsz00;
																							BgL_arg1751z00_2351 =
																								BGL_CLASS_NUM
																								(BgL_classz00_3319);
																						}
																						BGL_OBJECT_CLASS_NUM_SET(
																							((BgL_objectz00_bglt)
																								BgL_new1189z00_2350),
																							BgL_arg1751z00_2351);
																					}
																					BgL_new1190z00_2349 =
																						BgL_new1189z00_2350;
																				}
																				((((BgL_rtl_funz00_bglt) COBJECT(
																								((BgL_rtl_funz00_bglt)
																									BgL_new1190z00_2349)))->
																						BgL_locz00) =
																					((obj_t) BFALSE), BUNSPEC);
																				BgL_auxz00_4245 =
																					((BgL_rtl_funz00_bglt)
																					BgL_new1190z00_2349);
																			}
																			((((BgL_rtl_insz00_bglt)
																						COBJECT(BgL_i1187z00_2348))->
																					BgL_funz00) =
																				((BgL_rtl_funz00_bglt) BgL_auxz00_4245),
																				BUNSPEC);
																}}}
															else
																{	/* SawBbv/bbv-optim.scm 122 */
																	BFALSE;
																}
														}
														{	/* SawBbv/bbv-optim.scm 128 */
															obj_t BgL_arg1770z00_2363;
															obj_t BgL_arg1771z00_2364;

															{	/* SawBbv/bbv-optim.scm 128 */
																obj_t BgL_arg1773z00_2365;
																obj_t BgL_arg1775z00_2366;

																BgL_arg1773z00_2365 =
																	(((BgL_blockz00_bglt)
																		COBJECT(BgL_i1185z00_2328))->BgL_succsz00);
																BgL_arg1775z00_2366 =
																	CDR(((obj_t) BgL_bsz00_2320));
																BgL_arg1770z00_2363 =
																	BGl_appendzd221011zd2zzsaw_bbvzd2optimzd2
																	(BgL_arg1773z00_2365, BgL_arg1775z00_2366);
															}
															{	/* SawBbv/bbv-optim.scm 129 */
																obj_t BgL_arg1798z00_2367;

																BgL_arg1798z00_2367 =
																	CAR(((obj_t) BgL_bsz00_2320));
																BgL_arg1771z00_2364 =
																	BGl_bbsetzd2conszd2zzsaw_bbvzd2optimzd2(
																	((BgL_blockz00_bglt) BgL_arg1798z00_2367),
																	BgL_accz00_2321);
															}
															{
																obj_t BgL_accz00_4263;
																obj_t BgL_bsz00_4262;

																BgL_bsz00_4262 = BgL_arg1770z00_2363;
																BgL_accz00_4263 = BgL_arg1771z00_2364;
																BgL_accz00_2321 = BgL_accz00_4263;
																BgL_bsz00_2320 = BgL_bsz00_4262;
																goto BgL_zc3z04anonymousza31704ze3z87_2322;
															}
														}
													}
												}
										}
								}
							}
							return ((obj_t) BgL_auxz00_4153);
						}
					else
						{	/* SawBbv/bbv-optim.scm 131 */
							return ((obj_t) BgL_bz00_94);
						}
				}
			}
		}

	}



/* &remove-goto! */
	obj_t BGl_z62removezd2gotoz12za2zzsaw_bbvzd2optimzd2(obj_t BgL_envz00_3568,
		obj_t BgL_globalz00_3569, obj_t BgL_bz00_3570)
	{
		{	/* SawBbv/bbv-optim.scm 95 */
			return
				BGl_removezd2gotoz12zc0zzsaw_bbvzd2optimzd2(
				((BgL_globalz00_bglt) BgL_globalz00_3569),
				((BgL_blockz00_bglt) BgL_bz00_3570));
		}

	}



/* simplify-branch! */
	BGL_EXPORTED_DEF obj_t
		BGl_simplifyzd2branchz12zc0zzsaw_bbvzd2optimzd2(BgL_globalz00_bglt
		BgL_globalz00_95, BgL_blockz00_bglt BgL_bz00_96)
	{
		{	/* SawBbv/bbv-optim.scm 142 */
			{
				BgL_blockz00_bglt BgL_bz00_2383;

				if (CBOOL(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2))
					{	/* SawBbv/bbv-optim.scm 144 */
						BGl_assertzd2blockszd2zzsaw_bbvzd2debugzd2(
							((BgL_blockz00_bglt) BgL_bz00_96),
							BGl_string2277z00zzsaw_bbvzd2optimzd2);
					}
				else
					{	/* SawBbv/bbv-optim.scm 144 */
						BFALSE;
					}
				{	/* SawBbv/bbv-optim.scm 204 */
					bool_t BgL_test2381z00_4273;

					if (
						(BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2 == BTRUE))
						{	/* SawBbv/bbv-optim.scm 204 */
							BgL_test2381z00_4273 = ((bool_t) 1);
						}
					else
						{	/* SawBbv/bbv-optim.scm 204 */
							if (STRINGP
								(BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2))
								{	/* SawBbv/bbv-optim.scm 206 */
									obj_t BgL_g1868z00_2380;

									BgL_g1868z00_2380 =
										BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2;
									{	/* SawBbv/bbv-optim.scm 206 */

										BgL_test2381z00_4273 =
											CBOOL(BGl_stringzd2containszd2zz__r4_strings_6_7z00
											(BgL_g1868z00_2380, BGl_string2278z00zzsaw_bbvzd2optimzd2,
												(int) (0L)));
								}}
							else
								{	/* SawBbv/bbv-optim.scm 205 */
									BgL_test2381z00_4273 = ((bool_t) 0);
								}
						}
					if (BgL_test2381z00_4273)
						{
							BgL_blockz00_bglt BgL_auxz00_4281;

							BgL_bz00_2383 = BgL_bz00_96;
							{	/* SawBbv/bbv-optim.scm 148 */
								obj_t BgL_g1192z00_2385;
								obj_t BgL_g1193z00_2386;

								{	/* SawBbv/bbv-optim.scm 148 */
									obj_t BgL_list1889z00_2467;

									BgL_list1889z00_2467 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_2383), BNIL);
									BgL_g1192z00_2385 = BgL_list1889z00_2467;
								}
								BgL_g1193z00_2386 =
									BGl_makezd2emptyzd2bbsetz00zzsaw_bbvzd2optimzd2();
								{
									obj_t BgL_bsz00_2388;
									obj_t BgL_accz00_2389;

									BgL_bsz00_2388 = BgL_g1192z00_2385;
									BgL_accz00_2389 = BgL_g1193z00_2386;
								BgL_zc3z04anonymousza31803ze3z87_2390:
									if (NULLP(BgL_bsz00_2388))
										{	/* SawBbv/bbv-optim.scm 151 */
											BgL_auxz00_4281 = BgL_bz00_2383;
										}
									else
										{	/* SawBbv/bbv-optim.scm 153 */
											bool_t BgL_test2385z00_4287;

											{	/* SawBbv/bbv-optim.scm 153 */
												BgL_blockz00_bglt BgL_blockz00_3327;

												BgL_blockz00_3327 =
													((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2388)));
												{	/* SawBbv/bbv-optim.scm 153 */
													long BgL_arg1546z00_3330;
													obj_t BgL_arg1552z00_3331;

													{
														BgL_blocksz00_bglt BgL_auxz00_4291;

														{
															obj_t BgL_auxz00_4292;

															{	/* SawBbv/bbv-optim.scm 153 */
																BgL_objectz00_bglt BgL_tmpz00_4293;

																BgL_tmpz00_4293 =
																	((BgL_objectz00_bglt) BgL_blockz00_3327);
																BgL_auxz00_4292 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_4293);
															}
															BgL_auxz00_4291 =
																((BgL_blocksz00_bglt) BgL_auxz00_4292);
														}
														BgL_arg1546z00_3330 =
															(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4291))->
															BgL_z52markz52);
													}
													BgL_arg1552z00_3331 =
														STRUCT_REF(BgL_accz00_2389, (int) (0L));
													BgL_test2385z00_4287 =
														(BINT(BgL_arg1546z00_3330) == BgL_arg1552z00_3331);
											}}
											if (BgL_test2385z00_4287)
												{	/* SawBbv/bbv-optim.scm 154 */
													obj_t BgL_arg1808z00_2394;

													BgL_arg1808z00_2394 = CDR(((obj_t) BgL_bsz00_2388));
													{
														obj_t BgL_bsz00_4304;

														BgL_bsz00_4304 = BgL_arg1808z00_2394;
														BgL_bsz00_2388 = BgL_bsz00_4304;
														goto BgL_zc3z04anonymousza31803ze3z87_2390;
													}
												}
											else
												{	/* SawBbv/bbv-optim.scm 156 */
													BgL_blockz00_bglt BgL_i1194z00_2395;

													BgL_i1194z00_2395 =
														((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2388)));
													if (CBOOL
														(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2))
														{	/* SawBbv/bbv-optim.scm 158 */
															obj_t BgL_arg1812z00_2396;

															BgL_arg1812z00_2396 =
																CAR(((obj_t) BgL_bsz00_2388));
															BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(
																((BgL_blockz00_bglt) BgL_arg1812z00_2396),
																BGl_string2273z00zzsaw_bbvzd2optimzd2);
														}
													else
														{	/* SawBbv/bbv-optim.scm 157 */
															BFALSE;
														}
													if (
														(bgl_list_length(
																(((BgL_blockz00_bglt) COBJECT(
																			((BgL_blockz00_bglt)
																				BgL_i1194z00_2395)))->BgL_succsz00)) ==
															1L))
														{	/* SawBbv/bbv-optim.scm 160 */
															bool_t BgL_test2388z00_4319;

															{	/* SawBbv/bbv-optim.scm 160 */
																long BgL_tmpz00_4320;

																{	/* SawBbv/bbv-optim.scm 160 */
																	obj_t BgL_auxz00_4321;

																	{
																		BgL_blockz00_bglt BgL_auxz00_4322;

																		{	/* SawBbv/bbv-optim.scm 160 */
																			obj_t BgL_pairz00_3338;

																			BgL_pairz00_3338 =
																				(((BgL_blockz00_bglt) COBJECT(
																						((BgL_blockz00_bglt)
																							BgL_i1194z00_2395)))->
																				BgL_succsz00);
																			BgL_auxz00_4322 =
																				((BgL_blockz00_bglt)
																				CAR(BgL_pairz00_3338));
																		}
																		BgL_auxz00_4321 =
																			(((BgL_blockz00_bglt)
																				COBJECT(BgL_auxz00_4322))->
																			BgL_predsz00);
																	}
																	BgL_tmpz00_4320 =
																		bgl_list_length(BgL_auxz00_4321);
																}
																BgL_test2388z00_4319 = (BgL_tmpz00_4320 == 1L);
															}
															if (BgL_test2388z00_4319)
																{	/* SawBbv/bbv-optim.scm 162 */
																	obj_t BgL_sz00_2405;

																	{	/* SawBbv/bbv-optim.scm 162 */
																		obj_t BgL_pairz00_3341;

																		BgL_pairz00_3341 =
																			(((BgL_blockz00_bglt) COBJECT(
																					((BgL_blockz00_bglt)
																						BgL_i1194z00_2395)))->BgL_succsz00);
																		BgL_sz00_2405 = CAR(BgL_pairz00_3341);
																	}
																	{	/* SawBbv/bbv-optim.scm 164 */
																		obj_t BgL_g1452z00_2406;

																		BgL_g1452z00_2406 =
																			(((BgL_blockz00_bglt) COBJECT(
																					((BgL_blockz00_bglt)
																						BgL_sz00_2405)))->BgL_succsz00);
																		{
																			obj_t BgL_l1450z00_2408;

																			BgL_l1450z00_2408 = BgL_g1452z00_2406;
																		BgL_zc3z04anonymousza31836ze3z87_2409:
																			if (PAIRP(BgL_l1450z00_2408))
																				{	/* SawBbv/bbv-optim.scm 168 */
																					{	/* SawBbv/bbv-optim.scm 165 */
																						obj_t BgL_nsz00_2411;

																						BgL_nsz00_2411 =
																							CAR(BgL_l1450z00_2408);
																						{	/* SawBbv/bbv-optim.scm 166 */
																							obj_t BgL_arg1838z00_2412;

																							{	/* SawBbv/bbv-optim.scm 166 */
																								obj_t BgL_arg1839z00_2413;
																								obj_t BgL_arg1840z00_2414;

																								BgL_arg1839z00_2413 =
																									(((BgL_blockz00_bglt) COBJECT(
																											((BgL_blockz00_bglt)
																												BgL_nsz00_2411)))->
																									BgL_predsz00);
																								BgL_arg1840z00_2414 =
																									CAR(((obj_t) BgL_bsz00_2388));
																								BgL_arg1838z00_2412 =
																									BGl_listzd2replacezd2zzsaw_bbvzd2utilszd2
																									(BgL_arg1839z00_2413,
																									BgL_sz00_2405,
																									BgL_arg1840z00_2414);
																							}
																							((((BgL_blockz00_bglt) COBJECT(
																											((BgL_blockz00_bglt)
																												BgL_nsz00_2411)))->
																									BgL_predsz00) =
																								((obj_t) ((obj_t)
																										BgL_arg1838z00_2412)),
																								BUNSPEC);
																						}
																					}
																					{
																						obj_t BgL_l1450z00_4346;

																						BgL_l1450z00_4346 =
																							CDR(BgL_l1450z00_2408);
																						BgL_l1450z00_2408 =
																							BgL_l1450z00_4346;
																						goto
																							BgL_zc3z04anonymousza31836ze3z87_2409;
																					}
																				}
																			else
																				{	/* SawBbv/bbv-optim.scm 168 */
																					((bool_t) 1);
																				}
																		}
																	}
																	((((BgL_blockz00_bglt) COBJECT(
																					((BgL_blockz00_bglt)
																						BgL_i1194z00_2395)))->
																			BgL_succsz00) =
																		((obj_t) (((BgL_blockz00_bglt)
																					COBJECT(((BgL_blockz00_bglt)
																							BgL_sz00_2405)))->BgL_succsz00)),
																		BUNSPEC);
																	{	/* SawBbv/bbv-optim.scm 170 */
																		obj_t BgL_lpz00_2417;

																		BgL_lpz00_2417 =
																			BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
																			((((BgL_blockz00_bglt)
																					COBJECT(((BgL_blockz00_bglt)
																							BgL_i1194z00_2395)))->
																				BgL_firstz00));
																		{	/* SawBbv/bbv-optim.scm 171 */
																			bool_t BgL_test2390z00_4355;

																			{	/* SawBbv/bbv-optim.scm 171 */
																				obj_t BgL_arg1852z00_2428;

																				BgL_arg1852z00_2428 =
																					CAR(BgL_lpz00_2417);
																				BgL_test2390z00_4355 =
																					BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2
																					(((BgL_rtl_insz00_bglt)
																						BgL_arg1852z00_2428));
																			}
																			if (BgL_test2390z00_4355)
																				{	/* SawBbv/bbv-optim.scm 172 */
																					obj_t BgL_revz00_2420;

																					{	/* SawBbv/bbv-optim.scm 172 */
																						obj_t BgL_arg1846z00_2422;

																						{	/* SawBbv/bbv-optim.scm 172 */
																							obj_t BgL_pairz00_3351;

																							BgL_pairz00_3351 =
																								bgl_reverse(
																								(((BgL_blockz00_bglt) COBJECT(
																											((BgL_blockz00_bglt)
																												BgL_i1194z00_2395)))->
																									BgL_firstz00));
																							BgL_arg1846z00_2422 =
																								CDR(BgL_pairz00_3351);
																						}
																						BgL_revz00_2420 =
																							bgl_reverse_bang
																							(BgL_arg1846z00_2422);
																					}
																					{
																						obj_t BgL_auxz00_4364;

																						{	/* SawBbv/bbv-optim.scm 173 */
																							obj_t BgL_arg1845z00_2421;

																							BgL_arg1845z00_2421 =
																								(((BgL_blockz00_bglt) COBJECT(
																										((BgL_blockz00_bglt)
																											BgL_sz00_2405)))->
																								BgL_firstz00);
																							BgL_auxz00_4364 =
																								BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00
																								(BgL_revz00_2420,
																								BgL_arg1845z00_2421);
																						}
																						((((BgL_blockz00_bglt) COBJECT(
																										((BgL_blockz00_bglt)
																											BgL_i1194z00_2395)))->
																								BgL_firstz00) =
																							((obj_t) BgL_auxz00_4364),
																							BUNSPEC);
																					}
																				}
																			else
																				{
																					obj_t BgL_auxz00_4370;

																					{	/* SawBbv/bbv-optim.scm 175 */
																						obj_t BgL_arg1849z00_2425;
																						obj_t BgL_arg1850z00_2426;

																						BgL_arg1849z00_2425 =
																							(((BgL_blockz00_bglt) COBJECT(
																									((BgL_blockz00_bglt)
																										BgL_i1194z00_2395)))->
																							BgL_firstz00);
																						{	/* SawBbv/bbv-optim.scm 176 */
																							obj_t BgL_arg1851z00_2427;

																							BgL_arg1851z00_2427 =
																								(((BgL_blockz00_bglt) COBJECT(
																										((BgL_blockz00_bglt)
																											BgL_sz00_2405)))->
																								BgL_firstz00);
																							BgL_arg1850z00_2426 =
																								BGl_listzd2copyzd2zz__r4_pairs_and_lists_6_3z00
																								(BgL_arg1851z00_2427);
																						}
																						BgL_auxz00_4370 =
																							BGl_appendzd221011zd2zzsaw_bbvzd2optimzd2
																							(BgL_arg1849z00_2425,
																							BgL_arg1850z00_2426);
																					}
																					((((BgL_blockz00_bglt) COBJECT(
																									((BgL_blockz00_bglt)
																										BgL_i1194z00_2395)))->
																							BgL_firstz00) =
																						((obj_t) BgL_auxz00_4370), BUNSPEC);
																				}
																		}
																	}
																	if (CBOOL
																		(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2))
																		{	/* SawBbv/bbv-optim.scm 177 */
																			BGl_assertzd2blockszd2zzsaw_bbvzd2debugzd2
																				(((BgL_blockz00_bglt) BgL_sz00_2405),
																				BGl_string2274z00zzsaw_bbvzd2optimzd2);
																		}
																	else
																		{	/* SawBbv/bbv-optim.scm 177 */
																			BFALSE;
																		}
																	{

																		goto BgL_zc3z04anonymousza31803ze3z87_2390;
																	}
																}
															else
																{	/* SawBbv/bbv-optim.scm 180 */
																	obj_t BgL_arg1856z00_2431;
																	obj_t BgL_arg1857z00_2432;

																	{	/* SawBbv/bbv-optim.scm 180 */
																		obj_t BgL_arg1858z00_2433;
																		obj_t BgL_arg1859z00_2434;

																		{	/* SawBbv/bbv-optim.scm 180 */
																			obj_t BgL_pairz00_3354;

																			BgL_pairz00_3354 =
																				(((BgL_blockz00_bglt) COBJECT(
																						((BgL_blockz00_bglt)
																							BgL_i1194z00_2395)))->
																				BgL_succsz00);
																			BgL_arg1858z00_2433 =
																				CAR(BgL_pairz00_3354);
																		}
																		BgL_arg1859z00_2434 =
																			CDR(((obj_t) BgL_bsz00_2388));
																		BgL_arg1856z00_2431 =
																			MAKE_YOUNG_PAIR(BgL_arg1858z00_2433,
																			BgL_arg1859z00_2434);
																	}
																	{	/* SawBbv/bbv-optim.scm 181 */
																		obj_t BgL_arg1862z00_2436;

																		BgL_arg1862z00_2436 =
																			CAR(((obj_t) BgL_bsz00_2388));
																		BgL_arg1857z00_2432 =
																			BGl_bbsetzd2conszd2zzsaw_bbvzd2optimzd2(
																			((BgL_blockz00_bglt) BgL_arg1862z00_2436),
																			BgL_accz00_2389);
																	}
																	{
																		obj_t BgL_accz00_4394;
																		obj_t BgL_bsz00_4393;

																		BgL_bsz00_4393 = BgL_arg1856z00_2431;
																		BgL_accz00_4394 = BgL_arg1857z00_2432;
																		BgL_accz00_2389 = BgL_accz00_4394;
																		BgL_bsz00_2388 = BgL_bsz00_4393;
																		goto BgL_zc3z04anonymousza31803ze3z87_2390;
																	}
																}
														}
													else
														{
															obj_t BgL_ssz00_2444;
															obj_t BgL_nsuccsz00_2445;

															{	/* SawBbv/bbv-optim.scm 182 */
																obj_t BgL_arg1869z00_2443;

																BgL_arg1869z00_2443 =
																	(((BgL_blockz00_bglt) COBJECT(
																			((BgL_blockz00_bglt)
																				BgL_i1194z00_2395)))->BgL_succsz00);
																BgL_ssz00_2444 = BgL_arg1869z00_2443;
																BgL_nsuccsz00_2445 = BNIL;
															BgL_zc3z04anonymousza31870ze3z87_2446:
																if (NULLP(BgL_ssz00_2444))
																	{	/* SawBbv/bbv-optim.scm 185 */
																		obj_t BgL_arg1872z00_2448;
																		obj_t BgL_arg1873z00_2449;

																		{	/* SawBbv/bbv-optim.scm 185 */
																			obj_t BgL_arg1874z00_2450;
																			obj_t BgL_arg1875z00_2451;

																			BgL_arg1874z00_2450 =
																				bgl_reverse(BgL_nsuccsz00_2445);
																			BgL_arg1875z00_2451 =
																				CDR(((obj_t) BgL_bsz00_2388));
																			BgL_arg1872z00_2448 =
																				BGl_appendzd221011zd2zzsaw_bbvzd2optimzd2
																				(BgL_arg1874z00_2450,
																				BgL_arg1875z00_2451);
																		}
																		{	/* SawBbv/bbv-optim.scm 186 */
																			obj_t BgL_arg1876z00_2452;

																			BgL_arg1876z00_2452 =
																				CAR(((obj_t) BgL_bsz00_2388));
																			BgL_arg1873z00_2449 =
																				BGl_bbsetzd2conszd2zzsaw_bbvzd2optimzd2(
																				((BgL_blockz00_bglt)
																					BgL_arg1876z00_2452),
																				BgL_accz00_2389);
																		}
																		{
																			obj_t BgL_accz00_4408;
																			obj_t BgL_bsz00_4407;

																			BgL_bsz00_4407 = BgL_arg1872z00_2448;
																			BgL_accz00_4408 = BgL_arg1873z00_2449;
																			BgL_accz00_2389 = BgL_accz00_4408;
																			BgL_bsz00_2388 = BgL_bsz00_4407;
																			goto
																				BgL_zc3z04anonymousza31803ze3z87_2390;
																		}
																	}
																else
																	{	/* SawBbv/bbv-optim.scm 187 */
																		obj_t BgL_sz00_2453;

																		BgL_sz00_2453 =
																			CAR(((obj_t) BgL_ssz00_2444));
																		if (BGl_gotozd2blockzf3z21zzsaw_bbvzd2optimzd2(BgL_sz00_2453))
																			{	/* SawBbv/bbv-optim.scm 189 */
																				obj_t BgL_tz00_2455;

																				BgL_tz00_2455 =
																					BGl_gotozd2targetzd2zzsaw_bbvzd2optimzd2
																					(BgL_sz00_2453);
																				{	/* SawBbv/bbv-optim.scm 195 */
																					obj_t BgL_arg1878z00_2456;

																					BgL_arg1878z00_2456 =
																						CAR(((obj_t) BgL_bsz00_2388));
																					BGl_redirectzd2blockz12zc0zzsaw_bbvzd2utilszd2
																						(((BgL_blockz00_bglt)
																							BgL_arg1878z00_2456),
																						((BgL_blockz00_bglt) BgL_sz00_2453),
																						((BgL_blockz00_bglt)
																							BgL_tz00_2455));
																				}
																				{	/* SawBbv/bbv-optim.scm 197 */
																					obj_t BgL_arg1879z00_2457;

																					BgL_arg1879z00_2457 =
																						bgl_remq(BgL_sz00_2453,
																						(((BgL_blockz00_bglt) COBJECT(
																									((BgL_blockz00_bglt)
																										BgL_tz00_2455)))->
																							BgL_predsz00));
																					((((BgL_blockz00_bglt)
																								COBJECT(((BgL_blockz00_bglt)
																										BgL_tz00_2455)))->
																							BgL_predsz00) =
																						((obj_t) BgL_arg1879z00_2457),
																						BUNSPEC);
																				}
																				if (CBOOL
																					(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2))
																					{	/* SawBbv/bbv-optim.scm 198 */
																						BGl_assertzd2blockszd2zzsaw_bbvzd2debugzd2
																							(((BgL_blockz00_bglt)
																								BgL_sz00_2453),
																							BGl_string2275z00zzsaw_bbvzd2optimzd2);
																						BGl_assertzd2blockszd2zzsaw_bbvzd2debugzd2
																							(((BgL_blockz00_bglt)
																								BgL_tz00_2455),
																							BGl_string2276z00zzsaw_bbvzd2optimzd2);
																					}
																				else
																					{	/* SawBbv/bbv-optim.scm 198 */
																						BFALSE;
																					}
																				{	/* SawBbv/bbv-optim.scm 201 */
																					obj_t BgL_arg1882z00_2459;

																					BgL_arg1882z00_2459 =
																						CDR(((obj_t) BgL_ssz00_2444));
																					{
																						obj_t BgL_ssz00_4433;

																						BgL_ssz00_4433 =
																							BgL_arg1882z00_2459;
																						BgL_ssz00_2444 = BgL_ssz00_4433;
																						goto
																							BgL_zc3z04anonymousza31870ze3z87_2446;
																					}
																				}
																			}
																		else
																			{	/* SawBbv/bbv-optim.scm 202 */
																				obj_t BgL_arg1883z00_2460;
																				obj_t BgL_arg1884z00_2461;

																				BgL_arg1883z00_2460 =
																					CDR(((obj_t) BgL_ssz00_2444));
																				BgL_arg1884z00_2461 =
																					MAKE_YOUNG_PAIR(BgL_sz00_2453,
																					BgL_nsuccsz00_2445);
																				{
																					obj_t BgL_nsuccsz00_4438;
																					obj_t BgL_ssz00_4437;

																					BgL_ssz00_4437 = BgL_arg1883z00_2460;
																					BgL_nsuccsz00_4438 =
																						BgL_arg1884z00_2461;
																					BgL_nsuccsz00_2445 =
																						BgL_nsuccsz00_4438;
																					BgL_ssz00_2444 = BgL_ssz00_4437;
																					goto
																						BgL_zc3z04anonymousza31870ze3z87_2446;
																				}
																			}
																	}
															}
														}
												}
										}
								}
							}
							return ((obj_t) BgL_auxz00_4281);
						}
					else
						{	/* SawBbv/bbv-optim.scm 204 */
							return ((obj_t) BgL_bz00_96);
						}
				}
			}
		}

	}



/* &simplify-branch! */
	obj_t BGl_z62simplifyzd2branchz12za2zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3571, obj_t BgL_globalz00_3572, obj_t BgL_bz00_3573)
	{
		{	/* SawBbv/bbv-optim.scm 142 */
			return
				BGl_simplifyzd2branchz12zc0zzsaw_bbvzd2optimzd2(
				((BgL_globalz00_bglt) BgL_globalz00_3572),
				((BgL_blockz00_bglt) BgL_bz00_3573));
		}

	}



/* coalesce! */
	BGL_EXPORTED_DEF obj_t
		BGl_coalescez12z12zzsaw_bbvzd2optimzd2(BgL_globalz00_bglt BgL_globalz00_97,
		BgL_blockz00_bglt BgL_bz00_98)
	{
		{	/* SawBbv/bbv-optim.scm 217 */
			BGl_assertzd2blockszd2zzsaw_bbvzd2debugzd2(BgL_bz00_98,
				BGl_string2279z00zzsaw_bbvzd2optimzd2);
			{	/* SawBbv/bbv-optim.scm 267 */
				bool_t BgL_test2395z00_4445;

				if ((BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2 == BTRUE))
					{	/* SawBbv/bbv-optim.scm 267 */
						BgL_test2395z00_4445 = ((bool_t) 1);
					}
				else
					{	/* SawBbv/bbv-optim.scm 267 */
						if (STRINGP
							(BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2))
							{	/* SawBbv/bbv-optim.scm 269 */
								obj_t BgL_g1868z00_2477;

								BgL_g1868z00_2477 =
									BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2;
								{	/* SawBbv/bbv-optim.scm 269 */

									BgL_test2395z00_4445 =
										CBOOL(BGl_stringzd2containszd2zz__r4_strings_6_7z00
										(BgL_g1868z00_2477, BGl_string2280z00zzsaw_bbvzd2optimzd2,
											(int) (0L)));
							}}
						else
							{	/* SawBbv/bbv-optim.scm 268 */
								BgL_test2395z00_4445 = ((bool_t) 0);
							}
					}
				if (BgL_test2395z00_4445)
					{	/* SawBbv/bbv-optim.scm 272 */
						obj_t BgL_arg1891z00_2475;

						BgL_arg1891z00_2475 = BGl_getzd2bbzd2markz00zzsaw_bbvzd2typeszd2();
						BGl_coz12ze70zf5zzsaw_bbvzd2optimzd2(BgL_arg1891z00_2475,
							((obj_t) BgL_bz00_98));
					}
				else
					{	/* SawBbv/bbv-optim.scm 267 */
						((bool_t) 0);
					}
			}
			return ((obj_t) BgL_bz00_98);
		}

	}



/* co!~0 */
	bool_t BGl_coz12ze70zf5zzsaw_bbvzd2optimzd2(obj_t BgL_markz00_2480,
		obj_t BgL_bz00_2481)
	{
		{	/* SawBbv/bbv-optim.scm 263 */
			{	/* SawBbv/bbv-optim.scm 226 */
				BgL_blockz00_bglt BgL_i1198z00_2484;

				{
					BgL_blocksz00_bglt BgL_auxz00_4457;

					{
						obj_t BgL_auxz00_4458;

						{	/* SawBbv/bbv-optim.scm 227 */
							BgL_objectz00_bglt BgL_tmpz00_4459;

							BgL_tmpz00_4459 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2481));
							BgL_auxz00_4458 = BGL_OBJECT_WIDENING(BgL_tmpz00_4459);
						}
						BgL_auxz00_4457 = ((BgL_blocksz00_bglt) BgL_auxz00_4458);
					}
					BgL_i1198z00_2484 =
						(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4457))->BgL_parentz00);
				}
				{	/* SawBbv/bbv-optim.scm 227 */
					bool_t BgL_test2398z00_4465;

					{	/* SawBbv/bbv-optim.scm 227 */
						long BgL_arg1961z00_2580;

						{
							BgL_blockvz00_bglt BgL_auxz00_4466;

							{
								obj_t BgL_auxz00_4467;

								{	/* SawBbv/bbv-optim.scm 227 */
									BgL_objectz00_bglt BgL_tmpz00_4468;

									BgL_tmpz00_4468 = ((BgL_objectz00_bglt) BgL_i1198z00_2484);
									BgL_auxz00_4467 = BGL_OBJECT_WIDENING(BgL_tmpz00_4468);
								}
								BgL_auxz00_4466 = ((BgL_blockvz00_bglt) BgL_auxz00_4467);
							}
							BgL_arg1961z00_2580 =
								(((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4466))->
								BgL_z52markz52);
						}
						BgL_test2398z00_4465 =
							((long) CINT(BgL_markz00_2480) == BgL_arg1961z00_2580);
					}
					if (BgL_test2398z00_4465)
						{	/* SawBbv/bbv-optim.scm 227 */
							return ((bool_t) 0);
						}
					else
						{	/* SawBbv/bbv-optim.scm 227 */
							{
								BgL_blockvz00_bglt BgL_auxz00_4475;

								{
									obj_t BgL_auxz00_4476;

									{	/* SawBbv/bbv-optim.scm 228 */
										BgL_objectz00_bglt BgL_tmpz00_4477;

										BgL_tmpz00_4477 = ((BgL_objectz00_bglt) BgL_i1198z00_2484);
										BgL_auxz00_4476 = BGL_OBJECT_WIDENING(BgL_tmpz00_4477);
									}
									BgL_auxz00_4475 = ((BgL_blockvz00_bglt) BgL_auxz00_4476);
								}
								((((BgL_blockvz00_bglt) COBJECT(BgL_auxz00_4475))->
										BgL_z52markz52) =
									((long) (long) CINT(BgL_markz00_2480)), BUNSPEC);
							}
							{	/* SawBbv/bbv-optim.scm 230 */
								obj_t BgL_versionsz00_2487;

								{	/* SawBbv/bbv-optim.scm 230 */
									BgL_blockz00_bglt BgL_arg1960z00_2579;

									{
										BgL_blocksz00_bglt BgL_auxz00_4483;

										{
											obj_t BgL_auxz00_4484;

											{	/* SawBbv/bbv-optim.scm 230 */
												BgL_objectz00_bglt BgL_tmpz00_4485;

												BgL_tmpz00_4485 =
													((BgL_objectz00_bglt)
													((BgL_blockz00_bglt) BgL_bz00_2481));
												BgL_auxz00_4484 = BGL_OBJECT_WIDENING(BgL_tmpz00_4485);
											}
											BgL_auxz00_4483 = ((BgL_blocksz00_bglt) BgL_auxz00_4484);
										}
										BgL_arg1960z00_2579 =
											(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4483))->
											BgL_parentz00);
									}
									BgL_versionsz00_2487 =
										BGl_blockVzd2livezd2versionsz00zzsaw_bbvzd2typeszd2
										(BgL_arg1960z00_2579);
								}
								{	/* SawBbv/bbv-optim.scm 231 */
									bool_t BgL_test2399z00_4492;

									if (NULLP(BgL_versionsz00_2487))
										{	/* SawBbv/bbv-optim.scm 231 */
											BgL_test2399z00_4492 = ((bool_t) 1);
										}
									else
										{	/* SawBbv/bbv-optim.scm 231 */
											BgL_test2399z00_4492 = NULLP(CDR(BgL_versionsz00_2487));
										}
									if (BgL_test2399z00_4492)
										{	/* SawBbv/bbv-optim.scm 232 */
											obj_t BgL_g1455z00_2491;

											BgL_g1455z00_2491 =
												(((BgL_blockz00_bglt) COBJECT(
														((BgL_blockz00_bglt)
															((BgL_blockz00_bglt) BgL_bz00_2481))))->
												BgL_succsz00);
											{
												obj_t BgL_l1453z00_2493;

												BgL_l1453z00_2493 = BgL_g1455z00_2491;
											BgL_zc3z04anonymousza31898ze3z87_2494:
												if (PAIRP(BgL_l1453z00_2493))
													{	/* SawBbv/bbv-optim.scm 232 */
														BGl_coz12ze70zf5zzsaw_bbvzd2optimzd2
															(BgL_markz00_2480, CAR(BgL_l1453z00_2493));
														{
															obj_t BgL_l1453z00_4504;

															BgL_l1453z00_4504 = CDR(BgL_l1453z00_2493);
															BgL_l1453z00_2493 = BgL_l1453z00_4504;
															goto BgL_zc3z04anonymousza31898ze3z87_2494;
														}
													}
												else
													{	/* SawBbv/bbv-optim.scm 232 */
														return ((bool_t) 1);
													}
											}
										}
									else
										{	/* SawBbv/bbv-optim.scm 233 */
											obj_t BgL_ksz00_2499;

											{	/* SawBbv/bbv-optim.scm 233 */
												obj_t BgL_arg1949z00_2556;

												if (NULLP(BgL_versionsz00_2487))
													{	/* SawBbv/bbv-optim.scm 235 */
														BgL_arg1949z00_2556 = BNIL;
													}
												else
													{	/* SawBbv/bbv-optim.scm 235 */
														obj_t BgL_head1458z00_2564;

														BgL_head1458z00_2564 = MAKE_YOUNG_PAIR(BNIL, BNIL);
														{
															obj_t BgL_l1456z00_2566;
															obj_t BgL_tail1459z00_2567;

															BgL_l1456z00_2566 = BgL_versionsz00_2487;
															BgL_tail1459z00_2567 = BgL_head1458z00_2564;
														BgL_zc3z04anonymousza31954ze3z87_2568:
															if (NULLP(BgL_l1456z00_2566))
																{	/* SawBbv/bbv-optim.scm 235 */
																	BgL_arg1949z00_2556 =
																		CDR(BgL_head1458z00_2564);
																}
															else
																{	/* SawBbv/bbv-optim.scm 235 */
																	obj_t BgL_newtail1460z00_2570;

																	{	/* SawBbv/bbv-optim.scm 235 */
																		obj_t BgL_arg1957z00_2572;

																		{	/* SawBbv/bbv-optim.scm 235 */
																			obj_t BgL_vz00_2573;

																			BgL_vz00_2573 =
																				CAR(((obj_t) BgL_l1456z00_2566));
																			BgL_arg1957z00_2572 =
																				MAKE_YOUNG_PAIR
																				(BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2
																				(BgL_vz00_2573), BgL_vz00_2573);
																		}
																		BgL_newtail1460z00_2570 =
																			MAKE_YOUNG_PAIR(BgL_arg1957z00_2572,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1459z00_2567,
																		BgL_newtail1460z00_2570);
																	{	/* SawBbv/bbv-optim.scm 235 */
																		obj_t BgL_arg1956z00_2571;

																		BgL_arg1956z00_2571 =
																			CDR(((obj_t) BgL_l1456z00_2566));
																		{
																			obj_t BgL_tail1459z00_4521;
																			obj_t BgL_l1456z00_4520;

																			BgL_l1456z00_4520 = BgL_arg1956z00_2571;
																			BgL_tail1459z00_4521 =
																				BgL_newtail1460z00_2570;
																			BgL_tail1459z00_2567 =
																				BgL_tail1459z00_4521;
																			BgL_l1456z00_2566 = BgL_l1456z00_4520;
																			goto
																				BgL_zc3z04anonymousza31954ze3z87_2568;
																		}
																	}
																}
														}
													}
												BgL_ksz00_2499 =
													BGl_sortz00zz__r4_vectors_6_8z00
													(BGl_proc2281z00zzsaw_bbvzd2optimzd2,
													BgL_arg1949z00_2556);
											}
											{
												obj_t BgL_ksz00_2501;

												BgL_ksz00_2501 = BgL_ksz00_2499;
											BgL_zc3z04anonymousza31902ze3z87_2502:
												if (NULLP(CDR(((obj_t) BgL_ksz00_2501))))
													{	/* SawBbv/bbv-optim.scm 245 */
														obj_t BgL_g1463z00_2505;

														BgL_g1463z00_2505 =
															(((BgL_blockz00_bglt) COBJECT(
																	((BgL_blockz00_bglt)
																		((BgL_blockz00_bglt) BgL_bz00_2481))))->
															BgL_succsz00);
														{
															obj_t BgL_l1461z00_2507;

															BgL_l1461z00_2507 = BgL_g1463z00_2505;
														BgL_zc3z04anonymousza31905ze3z87_2508:
															if (PAIRP(BgL_l1461z00_2507))
																{	/* SawBbv/bbv-optim.scm 245 */
																	BGl_coz12ze70zf5zzsaw_bbvzd2optimzd2
																		(BgL_markz00_2480, CAR(BgL_l1461z00_2507));
																	{
																		obj_t BgL_l1461z00_4534;

																		BgL_l1461z00_4534 = CDR(BgL_l1461z00_2507);
																		BgL_l1461z00_2507 = BgL_l1461z00_4534;
																		goto BgL_zc3z04anonymousza31905ze3z87_2508;
																	}
																}
															else
																{	/* SawBbv/bbv-optim.scm 245 */
																	return ((bool_t) 1);
																}
														}
													}
												else
													{	/* SawBbv/bbv-optim.scm 246 */
														bool_t BgL_test2406z00_4536;

														{	/* SawBbv/bbv-optim.scm 246 */
															long BgL_arg1945z00_2551;

															{	/* SawBbv/bbv-optim.scm 246 */
																BgL_blockz00_bglt BgL_oz00_3393;

																{	/* SawBbv/bbv-optim.scm 246 */
																	obj_t BgL_pairz00_3392;

																	BgL_pairz00_3392 =
																		CAR(((obj_t) BgL_ksz00_2501));
																	BgL_oz00_3393 =
																		((BgL_blockz00_bglt) CDR(BgL_pairz00_3392));
																}
																{
																	BgL_blocksz00_bglt BgL_auxz00_4541;

																	{
																		obj_t BgL_auxz00_4542;

																		{	/* SawBbv/bbv-optim.scm 246 */
																			BgL_objectz00_bglt BgL_tmpz00_4543;

																			BgL_tmpz00_4543 =
																				((BgL_objectz00_bglt) BgL_oz00_3393);
																			BgL_auxz00_4542 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_4543);
																		}
																		BgL_auxz00_4541 =
																			((BgL_blocksz00_bglt) BgL_auxz00_4542);
																	}
																	BgL_arg1945z00_2551 =
																		(((BgL_blocksz00_bglt)
																			COBJECT(BgL_auxz00_4541))->
																		BgL_z52markz52);
															}}
															BgL_test2406z00_4536 =
																(BgL_arg1945z00_2551 >=
																(long) CINT(BgL_markz00_2480));
														}
														if (BgL_test2406z00_4536)
															{	/* SawBbv/bbv-optim.scm 247 */
																obj_t BgL_arg1914z00_2516;

																BgL_arg1914z00_2516 =
																	CDR(((obj_t) BgL_ksz00_2501));
																{
																	obj_t BgL_ksz00_4552;

																	BgL_ksz00_4552 = BgL_arg1914z00_2516;
																	BgL_ksz00_2501 = BgL_ksz00_4552;
																	goto BgL_zc3z04anonymousza31902ze3z87_2502;
																}
															}
														else
															{	/* SawBbv/bbv-optim.scm 249 */
																obj_t BgL_kz00_2517;

																BgL_kz00_2517 = CAR(((obj_t) BgL_ksz00_2501));
																if (CBOOL
																	(BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2))
																	{	/* SawBbv/bbv-optim.scm 251 */
																		obj_t BgL_arg1916z00_2518;

																		BgL_arg1916z00_2518 =
																			CDR(((obj_t) BgL_kz00_2517));
																		BGl_assertzd2blockzd2zzsaw_bbvzd2debugzd2(
																			((BgL_blockz00_bglt) BgL_arg1916z00_2518),
																			BGl_string2282z00zzsaw_bbvzd2optimzd2);
																	}
																else
																	{	/* SawBbv/bbv-optim.scm 250 */
																		BFALSE;
																	}
																{	/* SawBbv/bbv-optim.scm 252 */
																	obj_t BgL_g1199z00_2519;

																	BgL_g1199z00_2519 =
																		CDR(((obj_t) BgL_ksz00_2501));
																	{
																		obj_t BgL_lsz00_2521;

																		BgL_lsz00_2521 = BgL_g1199z00_2519;
																	BgL_zc3z04anonymousza31917ze3z87_2522:
																		if (NULLP(BgL_lsz00_2521))
																			{	/* SawBbv/bbv-optim.scm 255 */
																				obj_t BgL_arg1919z00_2524;

																				BgL_arg1919z00_2524 =
																					CDR(((obj_t) BgL_ksz00_2501));
																				{
																					obj_t BgL_ksz00_4567;

																					BgL_ksz00_4567 = BgL_arg1919z00_2524;
																					BgL_ksz00_2501 = BgL_ksz00_4567;
																					goto
																						BgL_zc3z04anonymousza31902ze3z87_2502;
																				}
																			}
																		else
																			{	/* SawBbv/bbv-optim.scm 256 */
																				bool_t BgL_test2409z00_4568;

																				{	/* SawBbv/bbv-optim.scm 256 */
																					bool_t BgL_test2410z00_4569;

																					{	/* SawBbv/bbv-optim.scm 256 */
																						long BgL_tmpz00_4570;

																						{	/* SawBbv/bbv-optim.scm 256 */
																							obj_t BgL_pairz00_3406;

																							BgL_pairz00_3406 =
																								CAR(((obj_t) BgL_lsz00_2521));
																							BgL_tmpz00_4570 =
																								(long)
																								CINT(CAR(BgL_pairz00_3406));
																						}
																						BgL_test2410z00_4569 =
																							(
																							(long) CINT(CAR(
																									((obj_t) BgL_kz00_2517))) ==
																							BgL_tmpz00_4570);
																					}
																					if (BgL_test2410z00_4569)
																						{	/* SawBbv/bbv-optim.scm 257 */
																							bool_t BgL_test2411z00_4579;

																							{	/* SawBbv/bbv-optim.scm 257 */
																								obj_t BgL_tmpz00_4580;

																								{	/* SawBbv/bbv-optim.scm 257 */
																									obj_t BgL_pairz00_3413;

																									BgL_pairz00_3413 =
																										CAR(
																										((obj_t) BgL_lsz00_2521));
																									BgL_tmpz00_4580 =
																										CDR(BgL_pairz00_3413);
																								}
																								BgL_test2411z00_4579 =
																									(CDR(
																										((obj_t) BgL_kz00_2517)) ==
																									BgL_tmpz00_4580);
																							}
																							if (BgL_test2411z00_4579)
																								{	/* SawBbv/bbv-optim.scm 257 */
																									BgL_test2409z00_4568 =
																										((bool_t) 0);
																								}
																							else
																								{	/* SawBbv/bbv-optim.scm 259 */
																									obj_t BgL_arg1939z00_2544;
																									obj_t BgL_arg1940z00_2545;

																									BgL_arg1939z00_2544 =
																										CDR(
																										((obj_t) BgL_kz00_2517));
																									{	/* SawBbv/bbv-optim.scm 259 */
																										obj_t BgL_pairz00_3418;

																										BgL_pairz00_3418 =
																											CAR(
																											((obj_t) BgL_lsz00_2521));
																										BgL_arg1940z00_2545 =
																											CDR(BgL_pairz00_3418);
																									}
																									BgL_test2409z00_4568 =
																										CBOOL
																										(BGl_coalescezf3zf3zzsaw_bbvzd2optimzd2
																										(((BgL_blockz00_bglt)
																												BgL_arg1939z00_2544),
																											((BgL_blockz00_bglt)
																												BgL_arg1940z00_2545),
																											0L));
																								}
																						}
																					else
																						{	/* SawBbv/bbv-optim.scm 256 */
																							BgL_test2409z00_4568 =
																								((bool_t) 0);
																						}
																				}
																				if (BgL_test2409z00_4568)
																					{	/* SawBbv/bbv-optim.scm 256 */
																						{	/* SawBbv/bbv-optim.scm 260 */
																							obj_t BgL_arg1935z00_2538;
																							obj_t BgL_arg1936z00_2539;

																							BgL_arg1935z00_2538 =
																								CDR(((obj_t) BgL_kz00_2517));
																							{	/* SawBbv/bbv-optim.scm 260 */
																								obj_t BgL_pairz00_3423;

																								BgL_pairz00_3423 =
																									CAR(((obj_t) BgL_lsz00_2521));
																								BgL_arg1936z00_2539 =
																									CDR(BgL_pairz00_3423);
																							}
																							BGl_coalescezd2blockz12zc0zzsaw_bbvzd2optimzd2
																								(BgL_markz00_2480,
																								BgL_arg1935z00_2538,
																								BgL_arg1936z00_2539);
																						}
																						{	/* SawBbv/bbv-optim.scm 261 */
																							obj_t BgL_arg1937z00_2540;

																							BgL_arg1937z00_2540 =
																								CDR(((obj_t) BgL_lsz00_2521));
																							{
																								obj_t BgL_lsz00_4604;

																								BgL_lsz00_4604 =
																									BgL_arg1937z00_2540;
																								BgL_lsz00_2521 = BgL_lsz00_4604;
																								goto
																									BgL_zc3z04anonymousza31917ze3z87_2522;
																							}
																						}
																					}
																				else
																					{	/* SawBbv/bbv-optim.scm 263 */
																						obj_t BgL_arg1938z00_2541;

																						BgL_arg1938z00_2541 =
																							CDR(((obj_t) BgL_lsz00_2521));
																						{
																							obj_t BgL_lsz00_4607;

																							BgL_lsz00_4607 =
																								BgL_arg1938z00_2541;
																							BgL_lsz00_2521 = BgL_lsz00_4607;
																							goto
																								BgL_zc3z04anonymousza31917ze3z87_2522;
																						}
																					}
																			}
																	}
																}
															}
													}
											}
										}
								}
							}
						}
				}
			}
		}

	}



/* &coalesce! */
	obj_t BGl_z62coalescez12z70zzsaw_bbvzd2optimzd2(obj_t BgL_envz00_3575,
		obj_t BgL_globalz00_3576, obj_t BgL_bz00_3577)
	{
		{	/* SawBbv/bbv-optim.scm 217 */
			return
				BGl_coalescez12z12zzsaw_bbvzd2optimzd2(
				((BgL_globalz00_bglt) BgL_globalz00_3576),
				((BgL_blockz00_bglt) BgL_bz00_3577));
		}

	}



/* &<@anonymous:1950> */
	obj_t BGl_z62zc3z04anonymousza31950ze3ze5zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3578, obj_t BgL_v1z00_3579, obj_t BgL_v2z00_3580)
	{
		{	/* SawBbv/bbv-optim.scm 233 */
			return
				BBOOL(
				((long) CINT(CAR(
							((obj_t) BgL_v1z00_3579))) <=
					(long) CINT(CAR(((obj_t) BgL_v2z00_3580)))));
		}

	}



/* coalesce-block! */
	obj_t BGl_coalescezd2blockz12zc0zzsaw_bbvzd2optimzd2(obj_t BgL_markz00_99,
		obj_t BgL_bz00_100, obj_t BgL_byz00_101)
	{
		{	/* SawBbv/bbv-optim.scm 278 */
			{	/* SawBbv/bbv-optim.scm 282 */
				obj_t BgL_g1201z00_2582;
				obj_t BgL_g1202z00_2583;

				{	/* SawBbv/bbv-optim.scm 282 */
					obj_t BgL_list1984z00_2618;

					BgL_list1984z00_2618 = MAKE_YOUNG_PAIR(BgL_byz00_101, BNIL);
					BgL_g1201z00_2582 = BgL_list1984z00_2618;
				}
				{	/* SawBbv/bbv-optim.scm 283 */
					obj_t BgL_list1985z00_2619;

					BgL_list1985z00_2619 = MAKE_YOUNG_PAIR(BgL_bz00_100, BNIL);
					BgL_g1202z00_2583 = BgL_list1985z00_2619;
				}
				{
					obj_t BgL_byz00_2585;
					obj_t BgL_bxz00_2586;

					BgL_byz00_2585 = BgL_g1201z00_2582;
					BgL_bxz00_2586 = BgL_g1202z00_2583;
				BgL_zc3z04anonymousza31962ze3z87_2587:
					if (NULLP(BgL_byz00_2585))
						{	/* SawBbv/bbv-optim.scm 287 */
							return BUNSPEC;
						}
					else
						{	/* SawBbv/bbv-optim.scm 289 */
							bool_t BgL_test2413z00_4623;

							{	/* SawBbv/bbv-optim.scm 289 */
								long BgL_arg1982z00_2615;

								{	/* SawBbv/bbv-optim.scm 289 */
									BgL_blockz00_bglt BgL_oz00_3430;

									BgL_oz00_3430 = ((BgL_blockz00_bglt) CAR(BgL_byz00_2585));
									{
										BgL_blocksz00_bglt BgL_auxz00_4626;

										{
											obj_t BgL_auxz00_4627;

											{	/* SawBbv/bbv-optim.scm 289 */
												BgL_objectz00_bglt BgL_tmpz00_4628;

												BgL_tmpz00_4628 = ((BgL_objectz00_bglt) BgL_oz00_3430);
												BgL_auxz00_4627 = BGL_OBJECT_WIDENING(BgL_tmpz00_4628);
											}
											BgL_auxz00_4626 = ((BgL_blocksz00_bglt) BgL_auxz00_4627);
										}
										BgL_arg1982z00_2615 =
											(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4626))->
											BgL_z52markz52);
								}}
								BgL_test2413z00_4623 =
									(BgL_arg1982z00_2615 == (long) CINT(BgL_markz00_99));
							}
							if (BgL_test2413z00_4623)
								{	/* SawBbv/bbv-optim.scm 289 */
									return BUNSPEC;
								}
							else
								{	/* SawBbv/bbv-optim.scm 289 */
									if ((CAR(BgL_byz00_2585) == CAR(((obj_t) BgL_bxz00_2586))))
										{	/* SawBbv/bbv-optim.scm 291 */
											return BUNSPEC;
										}
									else
										{	/* SawBbv/bbv-optim.scm 294 */
											BgL_blockz00_bglt BgL_i1203z00_2595;

											BgL_i1203z00_2595 =
												((BgL_blockz00_bglt) CAR(BgL_byz00_2585));
											{
												BgL_blocksz00_bglt BgL_auxz00_4642;

												{
													obj_t BgL_auxz00_4643;

													{	/* SawBbv/bbv-optim.scm 295 */
														BgL_objectz00_bglt BgL_tmpz00_4644;

														BgL_tmpz00_4644 =
															((BgL_objectz00_bglt) BgL_i1203z00_2595);
														BgL_auxz00_4643 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4644);
													}
													BgL_auxz00_4642 =
														((BgL_blocksz00_bglt) BgL_auxz00_4643);
												}
												((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4642))->
														BgL_z52markz52) =
													((long) (long) CINT(BgL_markz00_99)), BUNSPEC);
											}
											{	/* SawBbv/bbv-optim.scm 296 */
												obj_t BgL_g1466z00_2596;

												BgL_g1466z00_2596 =
													(((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt) BgL_i1203z00_2595)))->
													BgL_predsz00);
												{
													obj_t BgL_l1464z00_2598;

													BgL_l1464z00_2598 = BgL_g1466z00_2596;
												BgL_zc3z04anonymousza31970ze3z87_2599:
													if (PAIRP(BgL_l1464z00_2598))
														{	/* SawBbv/bbv-optim.scm 296 */
															{	/* SawBbv/bbv-optim.scm 297 */
																obj_t BgL_dz00_2601;

																BgL_dz00_2601 = CAR(BgL_l1464z00_2598);
																{	/* SawBbv/bbv-optim.scm 298 */
																	bool_t BgL_test2416z00_4655;

																	{	/* SawBbv/bbv-optim.scm 298 */
																		long BgL_arg1976z00_2607;

																		{
																			BgL_blocksz00_bglt BgL_auxz00_4656;

																			{
																				obj_t BgL_auxz00_4657;

																				{	/* SawBbv/bbv-optim.scm 298 */
																					BgL_objectz00_bglt BgL_tmpz00_4658;

																					BgL_tmpz00_4658 =
																						((BgL_objectz00_bglt)
																						((BgL_blockz00_bglt)
																							BgL_dz00_2601));
																					BgL_auxz00_4657 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_4658);
																				}
																				BgL_auxz00_4656 =
																					((BgL_blocksz00_bglt)
																					BgL_auxz00_4657);
																			}
																			BgL_arg1976z00_2607 =
																				(((BgL_blocksz00_bglt)
																					COBJECT(BgL_auxz00_4656))->
																				BgL_z52markz52);
																		}
																		BgL_test2416z00_4655 =
																			(
																			(long) CINT(BgL_markz00_99) ==
																			BgL_arg1976z00_2607);
																	}
																	if (BgL_test2416z00_4655)
																		{	/* SawBbv/bbv-optim.scm 298 */
																			BFALSE;
																		}
																	else
																		{	/* SawBbv/bbv-optim.scm 299 */
																			obj_t BgL_arg1974z00_2605;
																			obj_t BgL_arg1975z00_2606;

																			BgL_arg1974z00_2605 =
																				CAR(((obj_t) BgL_byz00_2585));
																			BgL_arg1975z00_2606 =
																				CAR(((obj_t) BgL_bxz00_2586));
																			BGl_redirectzd2blockz12zc0zzsaw_bbvzd2utilszd2
																				(((BgL_blockz00_bglt) BgL_dz00_2601),
																				((BgL_blockz00_bglt)
																					BgL_arg1974z00_2605),
																				((BgL_blockz00_bglt)
																					BgL_arg1975z00_2606));
																		}
																}
															}
															{
																obj_t BgL_l1464z00_4674;

																BgL_l1464z00_4674 = CDR(BgL_l1464z00_2598);
																BgL_l1464z00_2598 = BgL_l1464z00_4674;
																goto BgL_zc3z04anonymousza31970ze3z87_2599;
															}
														}
													else
														{	/* SawBbv/bbv-optim.scm 296 */
															((bool_t) 1);
														}
												}
											}
											{	/* SawBbv/bbv-optim.scm 301 */
												BgL_blockz00_bglt BgL_i1205z00_2610;

												BgL_i1205z00_2610 =
													((BgL_blockz00_bglt) CAR(((obj_t) BgL_bxz00_2586)));
												{	/* SawBbv/bbv-optim.scm 302 */
													obj_t BgL_arg1978z00_2611;
													obj_t BgL_arg1979z00_2612;

													BgL_arg1978z00_2611 =
														(((BgL_blockz00_bglt) COBJECT(
																((BgL_blockz00_bglt) BgL_i1203z00_2595)))->
														BgL_succsz00);
													BgL_arg1979z00_2612 =
														(((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
																	BgL_i1205z00_2610)))->BgL_succsz00);
													{
														obj_t BgL_bxz00_4684;
														obj_t BgL_byz00_4683;

														BgL_byz00_4683 = BgL_arg1978z00_2611;
														BgL_bxz00_4684 = BgL_arg1979z00_2612;
														BgL_bxz00_2586 = BgL_bxz00_4684;
														BgL_byz00_2585 = BgL_byz00_4683;
														goto BgL_zc3z04anonymousza31962ze3z87_2587;
													}
												}
											}
										}
								}
						}
				}
			}
		}

	}



/* coalesce? */
	obj_t BGl_coalescezf3zf3zzsaw_bbvzd2optimzd2(BgL_blockz00_bglt BgL_bxz00_102,
		BgL_blockz00_bglt BgL_byz00_103, long BgL_depthz00_104)
	{
		{	/* SawBbv/bbv-optim.scm 309 */
			{
				BgL_blockz00_bglt BgL_bxz00_2678;
				BgL_blockz00_bglt BgL_byz00_2679;

				if ((((obj_t) BgL_bxz00_102) == ((obj_t) BgL_byz00_103)))
					{	/* SawBbv/bbv-optim.scm 325 */
						return BTRUE;
					}
				else
					{	/* SawBbv/bbv-optim.scm 325 */
						if ((BgL_depthz00_104 > 40L))
							{	/* SawBbv/bbv-optim.scm 328 */
								bool_t BgL_tmpz00_4691;

								BgL_bxz00_2678 = BgL_bxz00_102;
								BgL_byz00_2679 = BgL_byz00_103;
							BgL_zc3z04anonymousza32039ze3z87_2680:
								{
									obj_t BgL_auxz00_4698;
									BgL_blocksz00_bglt BgL_auxz00_4692;

									{	/* SawBbv/bbv-optim.scm 314 */
										obj_t BgL_arg2040z00_2683;

										{
											BgL_blocksz00_bglt BgL_auxz00_4699;

											{
												obj_t BgL_auxz00_4700;

												{	/* SawBbv/bbv-optim.scm 314 */
													BgL_objectz00_bglt BgL_tmpz00_4701;

													BgL_tmpz00_4701 =
														((BgL_objectz00_bglt) BgL_bxz00_2678);
													BgL_auxz00_4700 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4701);
												}
												BgL_auxz00_4699 =
													((BgL_blocksz00_bglt) BgL_auxz00_4700);
											}
											BgL_arg2040z00_2683 =
												(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4699))->
												BgL_z52blacklistz52);
										}
										BgL_auxz00_4698 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_byz00_2679), BgL_arg2040z00_2683);
									}
									{
										obj_t BgL_auxz00_4693;

										{	/* SawBbv/bbv-optim.scm 314 */
											BgL_objectz00_bglt BgL_tmpz00_4694;

											BgL_tmpz00_4694 = ((BgL_objectz00_bglt) BgL_bxz00_2678);
											BgL_auxz00_4693 = BGL_OBJECT_WIDENING(BgL_tmpz00_4694);
										}
										BgL_auxz00_4692 = ((BgL_blocksz00_bglt) BgL_auxz00_4693);
									}
									((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4692))->
											BgL_z52blacklistz52) =
										((obj_t) BgL_auxz00_4698), BUNSPEC);
								}
								{
									obj_t BgL_auxz00_4715;
									BgL_blocksz00_bglt BgL_auxz00_4709;

									{	/* SawBbv/bbv-optim.scm 315 */
										obj_t BgL_arg2041z00_2684;

										{
											BgL_blocksz00_bglt BgL_auxz00_4716;

											{
												obj_t BgL_auxz00_4717;

												{	/* SawBbv/bbv-optim.scm 315 */
													BgL_objectz00_bglt BgL_tmpz00_4718;

													BgL_tmpz00_4718 =
														((BgL_objectz00_bglt) BgL_byz00_2679);
													BgL_auxz00_4717 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4718);
												}
												BgL_auxz00_4716 =
													((BgL_blocksz00_bglt) BgL_auxz00_4717);
											}
											BgL_arg2041z00_2684 =
												(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4716))->
												BgL_z52blacklistz52);
										}
										BgL_auxz00_4715 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_bxz00_2678), BgL_arg2041z00_2684);
									}
									{
										obj_t BgL_auxz00_4710;

										{	/* SawBbv/bbv-optim.scm 315 */
											BgL_objectz00_bglt BgL_tmpz00_4711;

											BgL_tmpz00_4711 = ((BgL_objectz00_bglt) BgL_byz00_2679);
											BgL_auxz00_4710 = BGL_OBJECT_WIDENING(BgL_tmpz00_4711);
										}
										BgL_auxz00_4709 = ((BgL_blocksz00_bglt) BgL_auxz00_4710);
									}
									((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4709))->
											BgL_z52blacklistz52) =
										((obj_t) BgL_auxz00_4715), BUNSPEC);
								}
								BgL_tmpz00_4691 = ((bool_t) 0);
								return BBOOL(BgL_tmpz00_4691);
							}
						else
							{	/* SawBbv/bbv-optim.scm 329 */
								bool_t BgL_test2419z00_4727;

								{	/* SawBbv/bbv-optim.scm 329 */
									obj_t BgL_arg2037z00_2676;
									obj_t BgL_arg2038z00_2677;

									BgL_arg2037z00_2676 =
										BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
										((obj_t) BgL_bxz00_102));
									BgL_arg2038z00_2677 =
										BGl_bbvzd2hashzd2zzsaw_bbvzd2typeszd2(
										((obj_t) BgL_byz00_103));
									BgL_test2419z00_4727 =
										(
										(long) CINT(BgL_arg2037z00_2676) ==
										(long) CINT(BgL_arg2038z00_2677));
								}
								if (BgL_test2419z00_4727)
									{	/* SawBbv/bbv-optim.scm 331 */
										bool_t BgL_test2420z00_4735;

										{	/* SawBbv/bbv-optim.scm 331 */
											bool_t BgL_test2421z00_4736;

											{	/* SawBbv/bbv-optim.scm 331 */
												obj_t BgL_arg2036z00_2675;

												{
													BgL_blocksz00_bglt BgL_auxz00_4737;

													{
														obj_t BgL_auxz00_4738;

														{	/* SawBbv/bbv-optim.scm 331 */
															BgL_objectz00_bglt BgL_tmpz00_4739;

															BgL_tmpz00_4739 =
																((BgL_objectz00_bglt) BgL_bxz00_102);
															BgL_auxz00_4738 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_4739);
														}
														BgL_auxz00_4737 =
															((BgL_blocksz00_bglt) BgL_auxz00_4738);
													}
													BgL_arg2036z00_2675 =
														(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4737))->
														BgL_z52blacklistz52);
												}
												BgL_test2421z00_4736 =
													(BgL_arg2036z00_2675 == CNST_TABLE_REF(1));
											}
											if (BgL_test2421z00_4736)
												{	/* SawBbv/bbv-optim.scm 331 */
													BgL_test2420z00_4735 = ((bool_t) 1);
												}
											else
												{	/* SawBbv/bbv-optim.scm 331 */
													obj_t BgL_arg2034z00_2674;

													{
														BgL_blocksz00_bglt BgL_auxz00_4746;

														{
															obj_t BgL_auxz00_4747;

															{	/* SawBbv/bbv-optim.scm 331 */
																BgL_objectz00_bglt BgL_tmpz00_4748;

																BgL_tmpz00_4748 =
																	((BgL_objectz00_bglt) BgL_byz00_103);
																BgL_auxz00_4747 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_4748);
															}
															BgL_auxz00_4746 =
																((BgL_blocksz00_bglt) BgL_auxz00_4747);
														}
														BgL_arg2034z00_2674 =
															(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_4746))->
															BgL_z52blacklistz52);
													}
													BgL_test2420z00_4735 =
														(BgL_arg2034z00_2674 == CNST_TABLE_REF(1));
												}
										}
										if (BgL_test2420z00_4735)
											{	/* SawBbv/bbv-optim.scm 332 */
												bool_t BgL_tmpz00_4755;

												{
													BgL_blockz00_bglt BgL_byz00_4757;
													BgL_blockz00_bglt BgL_bxz00_4756;

													BgL_bxz00_4756 = BgL_bxz00_102;
													BgL_byz00_4757 = BgL_byz00_103;
													BgL_byz00_2679 = BgL_byz00_4757;
													BgL_bxz00_2678 = BgL_bxz00_4756;
													goto BgL_zc3z04anonymousza32039ze3z87_2680;
												}
												return BBOOL(BgL_tmpz00_4755);
											}
										else
											{	/* SawBbv/bbv-optim.scm 333 */
												bool_t BgL_test2422z00_4759;

												{	/* SawBbv/bbv-optim.scm 333 */
													bool_t BgL_test2423z00_4760;

													{	/* SawBbv/bbv-optim.scm 333 */
														obj_t BgL_arg2033z00_2672;

														{
															BgL_blocksz00_bglt BgL_auxz00_4761;

															{
																obj_t BgL_auxz00_4762;

																{	/* SawBbv/bbv-optim.scm 333 */
																	BgL_objectz00_bglt BgL_tmpz00_4763;

																	BgL_tmpz00_4763 =
																		((BgL_objectz00_bglt) BgL_byz00_103);
																	BgL_auxz00_4762 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_4763);
																}
																BgL_auxz00_4761 =
																	((BgL_blocksz00_bglt) BgL_auxz00_4762);
															}
															BgL_arg2033z00_2672 =
																(((BgL_blocksz00_bglt)
																	COBJECT(BgL_auxz00_4761))->
																BgL_z52blacklistz52);
														}
														BgL_test2423z00_4760 =
															CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
																((obj_t) BgL_bxz00_102), BgL_arg2033z00_2672));
													}
													if (BgL_test2423z00_4760)
														{	/* SawBbv/bbv-optim.scm 333 */
															BgL_test2422z00_4759 = ((bool_t) 1);
														}
													else
														{	/* SawBbv/bbv-optim.scm 333 */
															obj_t BgL_arg2031z00_2671;

															{
																BgL_blocksz00_bglt BgL_auxz00_4771;

																{
																	obj_t BgL_auxz00_4772;

																	{	/* SawBbv/bbv-optim.scm 333 */
																		BgL_objectz00_bglt BgL_tmpz00_4773;

																		BgL_tmpz00_4773 =
																			((BgL_objectz00_bglt) BgL_bxz00_102);
																		BgL_auxz00_4772 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_4773);
																	}
																	BgL_auxz00_4771 =
																		((BgL_blocksz00_bglt) BgL_auxz00_4772);
																}
																BgL_arg2031z00_2671 =
																	(((BgL_blocksz00_bglt)
																		COBJECT(BgL_auxz00_4771))->
																	BgL_z52blacklistz52);
															}
															BgL_test2422z00_4759 =
																CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
																	((obj_t) BgL_byz00_103),
																	BgL_arg2031z00_2671));
														}
												}
												if (BgL_test2422z00_4759)
													{	/* SawBbv/bbv-optim.scm 333 */
														return BFALSE;
													}
												else
													{	/* SawBbv/bbv-optim.scm 335 */
														bool_t BgL_test2424z00_4781;

														if (
															(bgl_list_length(
																	(((BgL_blockz00_bglt) COBJECT(
																				((BgL_blockz00_bglt) BgL_byz00_103)))->
																		BgL_succsz00)) == 0L))
															{	/* SawBbv/bbv-optim.scm 335 */
																BgL_test2424z00_4781 =
																	(bgl_list_length(
																		(((BgL_blockz00_bglt) COBJECT(
																					((BgL_blockz00_bglt)
																						BgL_byz00_103)))->BgL_firstz00)) ==
																	1L);
															}
														else
															{	/* SawBbv/bbv-optim.scm 335 */
																BgL_test2424z00_4781 = ((bool_t) 0);
															}
														if (BgL_test2424z00_4781)
															{	/* SawBbv/bbv-optim.scm 336 */
																bool_t BgL_tmpz00_4791;

																{
																	BgL_blockz00_bglt BgL_byz00_4793;
																	BgL_blockz00_bglt BgL_bxz00_4792;

																	BgL_bxz00_4792 = BgL_bxz00_102;
																	BgL_byz00_4793 = BgL_byz00_103;
																	BgL_byz00_2679 = BgL_byz00_4793;
																	BgL_bxz00_2678 = BgL_bxz00_4792;
																	goto BgL_zc3z04anonymousza32039ze3z87_2680;
																}
																return BBOOL(BgL_tmpz00_4791);
															}
														else
															{	/* SawBbv/bbv-optim.scm 335 */
																if (
																	(bgl_list_length(
																			(((BgL_blockz00_bglt) COBJECT(
																						((BgL_blockz00_bglt)
																							BgL_byz00_103)))->
																				BgL_succsz00)) ==
																		bgl_list_length((((BgL_blockz00_bglt)
																					COBJECT(((BgL_blockz00_bglt)
																							BgL_bxz00_102)))->BgL_succsz00))))
																	{	/* SawBbv/bbv-optim.scm 338 */
																		obj_t BgL__ortest_1210z00_2650;

																		{	/* SawBbv/bbv-optim.scm 338 */
																			obj_t BgL_arg2015z00_2652;
																			obj_t BgL_arg2016z00_2653;

																			BgL_arg2015z00_2652 =
																				(((BgL_blockz00_bglt) COBJECT(
																						((BgL_blockz00_bglt)
																							BgL_bxz00_102)))->BgL_succsz00);
																			BgL_arg2016z00_2653 =
																				(((BgL_blockz00_bglt)
																					COBJECT(((BgL_blockz00_bglt)
																							BgL_byz00_103)))->BgL_succsz00);
																			{	/* SawBbv/bbv-optim.scm 338 */
																				obj_t
																					BgL_zc3z04anonymousza32019ze3z87_3581;
																				BgL_zc3z04anonymousza32019ze3z87_3581 =
																					MAKE_FX_PROCEDURE
																					(BGl_z62zc3z04anonymousza32019ze3ze5zzsaw_bbvzd2optimzd2,
																					(int) (2L), (int) (1L));
																				PROCEDURE_SET
																					(BgL_zc3z04anonymousza32019ze3z87_3581,
																					(int) (0L), BINT(BgL_depthz00_104));
																				{	/* SawBbv/bbv-optim.scm 338 */
																					obj_t BgL_list2017z00_2654;

																					{	/* SawBbv/bbv-optim.scm 338 */
																						obj_t BgL_arg2018z00_2655;

																						BgL_arg2018z00_2655 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2016z00_2653, BNIL);
																						BgL_list2017z00_2654 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg2015z00_2652,
																							BgL_arg2018z00_2655);
																					}
																					BgL__ortest_1210z00_2650 =
																						BGl_everyz00zz__r4_pairs_and_lists_6_3z00
																						(BgL_zc3z04anonymousza32019ze3z87_3581,
																						BgL_list2017z00_2654);
																		}}}
																		if (CBOOL(BgL__ortest_1210z00_2650))
																			{	/* SawBbv/bbv-optim.scm 338 */
																				return BgL__ortest_1210z00_2650;
																			}
																		else
																			{	/* SawBbv/bbv-optim.scm 339 */
																				bool_t BgL_tmpz00_4818;

																				{
																					BgL_blockz00_bglt BgL_byz00_4820;
																					BgL_blockz00_bglt BgL_bxz00_4819;

																					BgL_bxz00_4819 = BgL_bxz00_102;
																					BgL_byz00_4820 = BgL_byz00_103;
																					BgL_byz00_2679 = BgL_byz00_4820;
																					BgL_bxz00_2678 = BgL_bxz00_4819;
																					goto
																						BgL_zc3z04anonymousza32039ze3z87_2680;
																				}
																				return BBOOL(BgL_tmpz00_4818);
																			}
																	}
																else
																	{	/* SawBbv/bbv-optim.scm 341 */
																		bool_t BgL_tmpz00_4822;

																		{
																			BgL_blockz00_bglt BgL_byz00_4824;
																			BgL_blockz00_bglt BgL_bxz00_4823;

																			BgL_bxz00_4823 = BgL_bxz00_102;
																			BgL_byz00_4824 = BgL_byz00_103;
																			BgL_byz00_2679 = BgL_byz00_4824;
																			BgL_bxz00_2678 = BgL_bxz00_4823;
																			goto
																				BgL_zc3z04anonymousza32039ze3z87_2680;
																		}
																		return BBOOL(BgL_tmpz00_4822);
																	}
															}
													}
											}
									}
								else
									{	/* SawBbv/bbv-optim.scm 330 */
										bool_t BgL_tmpz00_4826;

										{
											BgL_blockz00_bglt BgL_byz00_4828;
											BgL_blockz00_bglt BgL_bxz00_4827;

											BgL_bxz00_4827 = BgL_bxz00_102;
											BgL_byz00_4828 = BgL_byz00_103;
											BgL_byz00_2679 = BgL_byz00_4828;
											BgL_bxz00_2678 = BgL_bxz00_4827;
											goto BgL_zc3z04anonymousza32039ze3z87_2680;
										}
										return BBOOL(BgL_tmpz00_4826);
									}
							}
					}
			}
		}

	}



/* &<@anonymous:2019> */
	obj_t BGl_z62zc3z04anonymousza32019ze3ze5zzsaw_bbvzd2optimzd2(obj_t
		BgL_envz00_3582, obj_t BgL_xz00_3584, obj_t BgL_yz00_3585)
	{
		{	/* SawBbv/bbv-optim.scm 338 */
			{	/* SawBbv/bbv-optim.scm 338 */
				long BgL_depthz00_3583;

				BgL_depthz00_3583 =
					(long) CINT(PROCEDURE_REF(BgL_envz00_3582, (int) (0L)));
				{	/* SawBbv/bbv-optim.scm 338 */
					long BgL_arg2020z00_3637;

					BgL_arg2020z00_3637 = (BgL_depthz00_3583 + 1L);
					return
						BGl_coalescezf3zf3zzsaw_bbvzd2optimzd2(
						((BgL_blockz00_bglt) BgL_xz00_3584),
						((BgL_blockz00_bglt) BgL_yz00_3585), BgL_arg2020z00_3637);
				}
			}
		}

	}



/* goto-block? */
	bool_t BGl_gotozd2blockzf3z21zzsaw_bbvzd2optimzd2(obj_t BgL_bz00_108)
	{
		{	/* SawBbv/bbv-optim.scm 388 */
			{	/* SawBbv/bbv-optim.scm 392 */
				bool_t BgL_test2428z00_4837;

				if (NULLP(
						(((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_bz00_108)))->BgL_succsz00)))
					{	/* SawBbv/bbv-optim.scm 392 */
						BgL_test2428z00_4837 = ((bool_t) 0);
					}
				else
					{	/* SawBbv/bbv-optim.scm 392 */
						obj_t BgL_tmpz00_4842;

						{	/* SawBbv/bbv-optim.scm 392 */
							obj_t BgL_pairz00_3463;

							BgL_pairz00_3463 =
								(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_bz00_108)))->BgL_succsz00);
							BgL_tmpz00_4842 = CDR(BgL_pairz00_3463);
						}
						BgL_test2428z00_4837 = NULLP(BgL_tmpz00_4842);
					}
				if (BgL_test2428z00_4837)
					{
						obj_t BgL_firstz00_2762;

						{	/* SawBbv/bbv-optim.scm 394 */
							obj_t BgL_arg2111z00_2761;

							BgL_arg2111z00_2761 =
								(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_bz00_108)))->BgL_firstz00);
							BgL_firstz00_2762 = BgL_arg2111z00_2761;
						BgL_zc3z04anonymousza32112ze3z87_2763:
							if (NULLP(BgL_firstz00_2762))
								{	/* SawBbv/bbv-optim.scm 396 */
									return ((bool_t) 0);
								}
							else
								{	/* SawBbv/bbv-optim.scm 398 */
									bool_t BgL_test2431z00_4851;

									{	/* SawBbv/bbv-optim.scm 398 */
										obj_t BgL_arg2126z00_2779;

										BgL_arg2126z00_2779 = CAR(((obj_t) BgL_firstz00_2762));
										BgL_test2431z00_4851 =
											BGl_rtl_inszd2gozf3z21zzsaw_bbvzd2typeszd2(
											((BgL_rtl_insz00_bglt) BgL_arg2126z00_2779));
									}
									if (BgL_test2431z00_4851)
										{	/* SawBbv/bbv-optim.scm 398 */
											if (NULLP(CDR(((obj_t) BgL_firstz00_2762))))
												{	/* SawBbv/bbv-optim.scm 402 */
													obj_t BgL_tmpz00_4860;

													{	/* SawBbv/bbv-optim.scm 402 */
														obj_t BgL_pairz00_3467;

														BgL_pairz00_3467 =
															(((BgL_blockz00_bglt) COBJECT(
																	((BgL_blockz00_bglt) BgL_bz00_108)))->
															BgL_succsz00);
														BgL_tmpz00_4860 = CAR(BgL_pairz00_3467);
													}
													return
														(
														((obj_t)
															(((BgL_rtl_goz00_bglt) COBJECT(
																		((BgL_rtl_goz00_bglt)
																			(((BgL_rtl_insz00_bglt) COBJECT(
																						((BgL_rtl_insz00_bglt)
																							CAR(
																								((obj_t)
																									BgL_firstz00_2762)))))->
																				BgL_funz00))))->BgL_toz00)) ==
														BgL_tmpz00_4860);
												}
											else
												{	/* SawBbv/bbv-optim.scm 399 */
													return ((bool_t) 0);
												}
										}
									else
										{	/* SawBbv/bbv-optim.scm 403 */
											bool_t BgL_test2433z00_4872;

											{	/* SawBbv/bbv-optim.scm 403 */
												obj_t BgL_arg2125z00_2778;

												BgL_arg2125z00_2778 = CAR(((obj_t) BgL_firstz00_2762));
												BgL_test2433z00_4872 =
													BGl_rtl_inszd2nopzf3z21zzsaw_bbvzd2typeszd2(
													((BgL_rtl_insz00_bglt) BgL_arg2125z00_2778));
											}
											if (BgL_test2433z00_4872)
												{	/* SawBbv/bbv-optim.scm 404 */
													obj_t BgL_arg2124z00_2777;

													BgL_arg2124z00_2777 =
														CDR(((obj_t) BgL_firstz00_2762));
													{
														obj_t BgL_firstz00_4879;

														BgL_firstz00_4879 = BgL_arg2124z00_2777;
														BgL_firstz00_2762 = BgL_firstz00_4879;
														goto BgL_zc3z04anonymousza32112ze3z87_2763;
													}
												}
											else
												{	/* SawBbv/bbv-optim.scm 403 */
													return ((bool_t) 0);
												}
										}
								}
						}
					}
				else
					{	/* SawBbv/bbv-optim.scm 392 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* goto-target */
	obj_t BGl_gotozd2targetzd2zzsaw_bbvzd2optimzd2(obj_t BgL_bz00_109)
	{
		{	/* SawBbv/bbv-optim.scm 411 */
		BGl_gotozd2targetzd2zzsaw_bbvzd2optimzd2:
			{	/* SawBbv/bbv-optim.scm 412 */
				obj_t BgL_tz00_2785;

				{	/* SawBbv/bbv-optim.scm 412 */
					obj_t BgL_pairz00_3471;

					BgL_pairz00_3471 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_bz00_109)))->BgL_succsz00);
					BgL_tz00_2785 = CAR(BgL_pairz00_3471);
				}
				if (BGl_gotozd2blockzf3z21zzsaw_bbvzd2optimzd2(BgL_tz00_2785))
					{
						obj_t BgL_bz00_4885;

						BgL_bz00_4885 = BgL_tz00_2785;
						BgL_bz00_109 = BgL_bz00_4885;
						goto BGl_gotozd2targetzd2zzsaw_bbvzd2optimzd2;
					}
				else
					{	/* SawBbv/bbv-optim.scm 413 */
						return BgL_tz00_2785;
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_bbvzd2optimzd2(void)
	{
		{	/* SawBbv/bbv-optim.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_bbvzd2optimzd2(void)
	{
		{	/* SawBbv/bbv-optim.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_bbvzd2optimzd2(void)
	{
		{	/* SawBbv/bbv-optim.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2optimzd2(void)
	{
		{	/* SawBbv/bbv-optim.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zzsaw_regsetz00(358079690L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(237915200L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(288263219L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(425659118L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2specializa7ez75(506937389L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(242097300L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2utilszd2(221709922L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2mergezd2(376575453L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2livenesszd2(156090797L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
			return
				BGl_modulezd2initializa7ationz75zzsaw_bbvzd2debugzd2(120981929L,
				BSTRING_TO_STRING(BGl_string2283z00zzsaw_bbvzd2optimzd2));
		}

	}

#ifdef __cplusplus
}
#endif
