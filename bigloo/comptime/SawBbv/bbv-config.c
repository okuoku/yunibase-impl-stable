/*===========================================================================*/
/*   (SawBbv/bbv-config.scm)                                                 */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawBbv/bbv-config.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_SAW_BBVzd2CONFIGzd2_TYPE_DEFINITIONS
#define BGL_BgL_SAW_BBVzd2CONFIGzd2_TYPE_DEFINITIONS
#endif													// BGL_BgL_SAW_BBVzd2CONFIGzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DEF obj_t BGl_za2typezd2callza2zd2zzsaw_bbvzd2configzd2 =
		BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_bbvzd2configzd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bbvzd2logza2zd2zzsaw_bbvzd2configzd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t
		BGl_za2maxzd2blockzd2mergezd2versionsza2zd2zzsaw_bbvzd2configzd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bbvzd2optimzd2vlengthza2z00zzsaw_bbvzd2configzd2
		= BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2configzd2(void);
	BGL_EXPORTED_DEF obj_t BGl_za2bbvzd2dumpzd2jsonza2z00zzsaw_bbvzd2configzd2 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_getenvz00zz__osz00(obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_bbvzd2configzd2(void);
	static obj_t BGl_objectzd2initzd2zzsaw_bbvzd2configzd2(void);
	BGL_EXPORTED_DEF obj_t BGl_za2maxzd2blockzd2limitza2z00zzsaw_bbvzd2configzd2 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_bbvzd2configzd2(void);
	BGL_EXPORTED_DEF obj_t
		BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2 = BUNSPEC;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2bbvzd2verboseza2zd2zzsaw_bbvzd2configzd2 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bbvzd2optimzd2aliasza2z00zzsaw_bbvzd2configzd2 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bbvzd2optimzd2boundza2z00zzsaw_bbvzd2configzd2 =
		BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_EXPORTED_DEF obj_t BGl_za2bbvzd2profileza2zd2zzsaw_bbvzd2configzd2 =
		BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2configzd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2configzd2(void);
	BGL_EXPORTED_DEF obj_t BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2 =
		BUNSPEC;
	static obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2configzd2(void);
	BGL_EXPORTED_DEF obj_t BGl_za2bbvzd2blockszd2gcza2z00zzsaw_bbvzd2configzd2 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2typezd2loadgza2zd2zzsaw_bbvzd2configzd2 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_stringzd2splitzd2zz__r4_strings_6_7z00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t
		BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2typezd2loadiza2zd2zzsaw_bbvzd2configzd2 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bbvzd2assertza2zd2zzsaw_bbvzd2configzd2 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t __cnst[15];


	   
		 
		DEFINE_STRING(BGl_string1070z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1070za700za7za7s1123za7, "false", 5);
	      DEFINE_STRING(BGl_string1071z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1071za700za7za7s1124za7, "true", 4);
	      DEFINE_STRING(BGl_string1072z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1072za700za7za7s1125za7, "bbv-debug", 9);
	      DEFINE_STRING(BGl_string1073z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1073za700za7za7s1126za7, "unknown value", 13);
	      DEFINE_STRING(BGl_string1074z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1074za700za7za7s1127za7, "BIGLOOBBVLOG", 12);
	      DEFINE_STRING(BGl_string1075z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1075za700za7za7s1128za7, "bbv-log", 7);
	      DEFINE_STRING(BGl_string1076z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1076za700za7za7s1129za7, "BIGLOOBBVASSERT", 15);
	      DEFINE_STRING(BGl_string1077z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1077za700za7za7s1130za7, "bbv-assert", 10);
	      DEFINE_STRING(BGl_string1078z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1078za700za7za7s1131za7, "illegal value", 13);
	      DEFINE_STRING(BGl_string1079z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1079za700za7za7s1132za7, "BIGLOOBBVCLEANUP", 16);
	      DEFINE_STRING(BGl_string1080z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1080za700za7za7s1133za7, "", 0);
	      DEFINE_STRING(BGl_string1081z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1081za700za7za7s1134za7, " ", 1);
	      DEFINE_STRING(BGl_string1082z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1082za700za7za7s1135za7, "bbv-blocks-cleanup", 18);
	      DEFINE_STRING(BGl_string1083z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1083za700za7za7s1136za7, "BIGLOOBBVGC", 11);
	      DEFINE_STRING(BGl_string1084z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1084za700za7za7s1137za7, "ssr", 3);
	      DEFINE_STRING(BGl_string1085z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1085za700za7za7s1138za7, "cnt", 3);
	      DEFINE_STRING(BGl_string1086z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1086za700za7za7s1139za7, "bbv-blocks-gc", 13);
	      DEFINE_STRING(BGl_string1087z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1087za700za7za7s1140za7, "BIGLOOBBVSTRATEGY", 17);
	      DEFINE_STRING(BGl_string1088z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1088za700za7za7s1141za7, "adn", 3);
	      DEFINE_STRING(BGl_string1089z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1089za700za7za7s1142za7, "score-", 6);
	      DEFINE_STRING(BGl_string1090z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1090za700za7za7s1143za7, "score2", 6);
	      DEFINE_STRING(BGl_string1091z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1091za700za7za7s1144za7, "score+", 6);
	      DEFINE_STRING(BGl_string1092z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1092za700za7za7s1145za7, "score*", 6);
	      DEFINE_STRING(BGl_string1093z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1093za700za7za7s1146za7, "score", 5);
	      DEFINE_STRING(BGl_string1094z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1094za700za7za7s1147za7, "sametypes", 9);
	      DEFINE_STRING(BGl_string1095z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1095za700za7za7s1148za7, "first", 5);
	      DEFINE_STRING(BGl_string1096z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1096za700za7za7s1149za7, "nearobj", 7);
	      DEFINE_STRING(BGl_string1097z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1097za700za7za7s1150za7, "size", 4);
	      DEFINE_STRING(BGl_string1098z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1098za700za7za7s1151za7, "random", 6);
	      DEFINE_STRING(BGl_string1099z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1099za700za7za7s1152za7, "distance", 8);
	      DEFINE_STRING(BGl_string1100z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1100za700za7za7s1153za7, "bbv-merge-strategy", 18);
	      DEFINE_STRING(BGl_string1101z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1101za700za7za7s1154za7, "unknown strategy", 16);
	      DEFINE_STRING(BGl_string1102z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1102za700za7za7s1155za7, "BIGLOOBBVVLENGTH", 16);
	      DEFINE_STRING(BGl_string1103z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1103za700za7za7s1156za7, "bbv-optim-vlength", 17);
	      DEFINE_STRING(BGl_string1104z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1104za700za7za7s1157za7, "BIGLOOBBVBOUND", 14);
	      DEFINE_STRING(BGl_string1105z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1105za700za7za7s1158za7, "bbv-optim-bound", 15);
	      DEFINE_STRING(BGl_string1106z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1106za700za7za7s1159za7, "BIGLOOBBVALIAS", 14);
	      DEFINE_STRING(BGl_string1107z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1107za700za7za7s1160za7, "bbv-optim-alias", 15);
	      DEFINE_STRING(BGl_string1108z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1108za700za7za7s1161za7, "BIGLOOBBVVERBOSE", 16);
	      DEFINE_STRING(BGl_string1109z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1109za700za7za7s1162za7, "bbv-verbose", 11);
	      DEFINE_STRING(BGl_string1110z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1110za700za7za7s1163za7, "BIGLOOBBVDUMPJSON", 17);
	      DEFINE_STRING(BGl_string1111z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1111za700za7za7s1164za7, "bbv-dump-json", 13);
	      DEFINE_STRING(BGl_string1112z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1112za700za7za7s1165za7, "BIGLOOBBVDUMPCFG", 16);
	      DEFINE_STRING(BGl_string1113z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1113za700za7za7s1166za7, "bbv-dump-cfg", 12);
	      DEFINE_STRING(BGl_string1114z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1114za700za7za7s1167za7, "BIGLOOBBVPROFILE", 16);
	      DEFINE_STRING(BGl_string1115z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1115za700za7za7s1168za7, "bbv-profile", 11);
	      DEFINE_STRING(BGl_string1116z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1116za700za7za7s1169za7, "BIGLOOBBVVERSIONLIMIT", 21);
	      DEFINE_STRING(BGl_string1117z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1117za700za7za7s1170za7,
		"distance random size nearobj first sametypes score score* score+ score2 score- adn cnt ssr (\"gc\" \"coalesce\" \"simplify\" \"goto\" \"nop\") ",
		133);
	      DEFINE_STRING(BGl_string1069z00zzsaw_bbvzd2configzd2,
		BgL_bgl_string1069za700za7za7s1171za7, "BIGLOOBBVDEBUG", 14);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2typezd2callza2zd2zzsaw_bbvzd2configzd2));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_bbvzd2configzd2));
		     ADD_ROOT((void *) (&BGl_za2bbvzd2logza2zd2zzsaw_bbvzd2configzd2));
		   
			 ADD_ROOT((void
				*) (&BGl_za2maxzd2blockzd2mergezd2versionsza2zd2zzsaw_bbvzd2configzd2));
		     ADD_ROOT((void
				*) (&BGl_za2bbvzd2optimzd2vlengthza2z00zzsaw_bbvzd2configzd2));
		     ADD_ROOT((void
				*) (&BGl_za2bbvzd2dumpzd2jsonza2z00zzsaw_bbvzd2configzd2));
		     ADD_ROOT((void
				*) (&BGl_za2maxzd2blockzd2limitza2z00zzsaw_bbvzd2configzd2));
		     ADD_ROOT((void *) (&BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2));
		   
			 ADD_ROOT((void
				*) (&BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2));
		     ADD_ROOT((void *) (&BGl_za2bbvzd2verboseza2zd2zzsaw_bbvzd2configzd2));
		   
			 ADD_ROOT((void
				*) (&BGl_za2bbvzd2optimzd2aliasza2z00zzsaw_bbvzd2configzd2));
		     ADD_ROOT((void
				*) (&BGl_za2bbvzd2optimzd2boundza2z00zzsaw_bbvzd2configzd2));
		     ADD_ROOT((void *) (&BGl_za2bbvzd2profileza2zd2zzsaw_bbvzd2configzd2));
		   
			 ADD_ROOT((void *) (&BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2));
		   
			 ADD_ROOT((void
				*) (&BGl_za2bbvzd2blockszd2gcza2z00zzsaw_bbvzd2configzd2));
		     ADD_ROOT((void *) (&BGl_za2typezd2loadgza2zd2zzsaw_bbvzd2configzd2));
		   
			 ADD_ROOT((void
				*) (&BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2));
		     ADD_ROOT((void *) (&BGl_za2typezd2loadiza2zd2zzsaw_bbvzd2configzd2));
		     ADD_ROOT((void *) (&BGl_za2bbvzd2assertza2zd2zzsaw_bbvzd2configzd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(long
		BgL_checksumz00_502, char *BgL_fromz00_503)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_bbvzd2configzd2))
				{
					BGl_requirezd2initializa7ationz75zzsaw_bbvzd2configzd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_bbvzd2configzd2();
					BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2configzd2();
					BGl_cnstzd2initzd2zzsaw_bbvzd2configzd2();
					return BGl_toplevelzd2initzd2zzsaw_bbvzd2configzd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2configzd2(void)
	{
		{	/* SawBbv/bbv-config.scm 15 */
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "saw_bbv-config");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_bbv-config");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_bbv-config");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_bbv-config");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"saw_bbv-config");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_bbv-config");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_bbv-config");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_bbv-config");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2configzd2(void)
	{
		{	/* SawBbv/bbv-config.scm 15 */
			{	/* SawBbv/bbv-config.scm 15 */
				obj_t BgL_cportz00_491;

				{	/* SawBbv/bbv-config.scm 15 */
					obj_t BgL_stringz00_498;

					BgL_stringz00_498 = BGl_string1117z00zzsaw_bbvzd2configzd2;
					{	/* SawBbv/bbv-config.scm 15 */
						obj_t BgL_startz00_499;

						BgL_startz00_499 = BINT(0L);
						{	/* SawBbv/bbv-config.scm 15 */
							obj_t BgL_endz00_500;

							BgL_endz00_500 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_498)));
							{	/* SawBbv/bbv-config.scm 15 */

								BgL_cportz00_491 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_498, BgL_startz00_499, BgL_endz00_500);
				}}}}
				{
					long BgL_iz00_492;

					BgL_iz00_492 = 14L;
				BgL_loopz00_493:
					if ((BgL_iz00_492 == -1L))
						{	/* SawBbv/bbv-config.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* SawBbv/bbv-config.scm 15 */
							{	/* SawBbv/bbv-config.scm 15 */
								obj_t BgL_arg1122z00_494;

								{	/* SawBbv/bbv-config.scm 15 */

									{	/* SawBbv/bbv-config.scm 15 */
										obj_t BgL_locationz00_496;

										BgL_locationz00_496 = BBOOL(((bool_t) 0));
										{	/* SawBbv/bbv-config.scm 15 */

											BgL_arg1122z00_494 =
												BGl_readz00zz__readerz00(BgL_cportz00_491,
												BgL_locationz00_496);
										}
									}
								}
								{	/* SawBbv/bbv-config.scm 15 */
									int BgL_tmpz00_528;

									BgL_tmpz00_528 = (int) (BgL_iz00_492);
									CNST_TABLE_SET(BgL_tmpz00_528, BgL_arg1122z00_494);
							}}
							{	/* SawBbv/bbv-config.scm 15 */
								int BgL_auxz00_497;

								BgL_auxz00_497 = (int) ((BgL_iz00_492 - 1L));
								{
									long BgL_iz00_533;

									BgL_iz00_533 = (long) (BgL_auxz00_497);
									BgL_iz00_492 = BgL_iz00_533;
									goto BgL_loopz00_493;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2configzd2(void)
	{
		{	/* SawBbv/bbv-config.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2configzd2(void)
	{
		{	/* SawBbv/bbv-config.scm 15 */
			{	/* SawBbv/bbv-config.scm 39 */
				obj_t BgL_ez00_3;

				BgL_ez00_3 =
					BGl_getenvz00zz__osz00(BGl_string1069z00zzsaw_bbvzd2configzd2);
				if (CBOOL(BgL_ez00_3))
					{	/* SawBbv/bbv-config.scm 42 */
						bool_t BgL_test1175z00_539;

						{	/* SawBbv/bbv-config.scm 42 */
							long BgL_l1z00_95;

							BgL_l1z00_95 = STRING_LENGTH(((obj_t) BgL_ez00_3));
							if ((BgL_l1z00_95 == 5L))
								{	/* SawBbv/bbv-config.scm 42 */
									int BgL_arg1282z00_98;

									{	/* SawBbv/bbv-config.scm 42 */
										char *BgL_auxz00_547;
										char *BgL_tmpz00_544;

										BgL_auxz00_547 =
											BSTRING_TO_STRING(BGl_string1070z00zzsaw_bbvzd2configzd2);
										BgL_tmpz00_544 = BSTRING_TO_STRING(((obj_t) BgL_ez00_3));
										BgL_arg1282z00_98 =
											memcmp(BgL_tmpz00_544, BgL_auxz00_547, BgL_l1z00_95);
									}
									BgL_test1175z00_539 = ((long) (BgL_arg1282z00_98) == 0L);
								}
							else
								{	/* SawBbv/bbv-config.scm 42 */
									BgL_test1175z00_539 = ((bool_t) 0);
								}
						}
						if (BgL_test1175z00_539)
							{	/* SawBbv/bbv-config.scm 42 */
								BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2 = BFALSE;
							}
						else
							{	/* SawBbv/bbv-config.scm 43 */
								bool_t BgL_test1177z00_552;

								{	/* SawBbv/bbv-config.scm 43 */
									long BgL_l1z00_106;

									BgL_l1z00_106 = STRING_LENGTH(((obj_t) BgL_ez00_3));
									if ((BgL_l1z00_106 == 4L))
										{	/* SawBbv/bbv-config.scm 43 */
											int BgL_arg1282z00_109;

											{	/* SawBbv/bbv-config.scm 43 */
												char *BgL_auxz00_560;
												char *BgL_tmpz00_557;

												BgL_auxz00_560 =
													BSTRING_TO_STRING
													(BGl_string1071z00zzsaw_bbvzd2configzd2);
												BgL_tmpz00_557 =
													BSTRING_TO_STRING(((obj_t) BgL_ez00_3));
												BgL_arg1282z00_109 =
													memcmp(BgL_tmpz00_557, BgL_auxz00_560, BgL_l1z00_106);
											}
											BgL_test1177z00_552 = ((long) (BgL_arg1282z00_109) == 0L);
										}
									else
										{	/* SawBbv/bbv-config.scm 43 */
											BgL_test1177z00_552 = ((bool_t) 0);
										}
								}
								if (BgL_test1177z00_552)
									{	/* SawBbv/bbv-config.scm 43 */
										BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2 = BTRUE;
									}
								else
									{	/* SawBbv/bbv-config.scm 43 */
										BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2 =
											BGl_errorz00zz__errorz00
											(BGl_string1072z00zzsaw_bbvzd2configzd2,
											BGl_string1073z00zzsaw_bbvzd2configzd2, BgL_ez00_3);
									}
							}
					}
				else
					{	/* SawBbv/bbv-config.scm 41 */
						BGl_za2bbvzd2debugza2zd2zzsaw_bbvzd2configzd2 = BFALSE;
					}
			}
			{	/* SawBbv/bbv-config.scm 50 */
				obj_t BgL_ez00_6;

				BgL_ez00_6 =
					BGl_getenvz00zz__osz00(BGl_string1074z00zzsaw_bbvzd2configzd2);
				if (CBOOL(BgL_ez00_6))
					{	/* SawBbv/bbv-config.scm 53 */
						bool_t BgL_test1180z00_569;

						{	/* SawBbv/bbv-config.scm 53 */
							long BgL_l1z00_117;

							BgL_l1z00_117 = STRING_LENGTH(((obj_t) BgL_ez00_6));
							if ((BgL_l1z00_117 == 5L))
								{	/* SawBbv/bbv-config.scm 53 */
									int BgL_arg1282z00_120;

									{	/* SawBbv/bbv-config.scm 53 */
										char *BgL_auxz00_577;
										char *BgL_tmpz00_574;

										BgL_auxz00_577 =
											BSTRING_TO_STRING(BGl_string1070z00zzsaw_bbvzd2configzd2);
										BgL_tmpz00_574 = BSTRING_TO_STRING(((obj_t) BgL_ez00_6));
										BgL_arg1282z00_120 =
											memcmp(BgL_tmpz00_574, BgL_auxz00_577, BgL_l1z00_117);
									}
									BgL_test1180z00_569 = ((long) (BgL_arg1282z00_120) == 0L);
								}
							else
								{	/* SawBbv/bbv-config.scm 53 */
									BgL_test1180z00_569 = ((bool_t) 0);
								}
						}
						if (BgL_test1180z00_569)
							{	/* SawBbv/bbv-config.scm 53 */
								BGl_za2bbvzd2logza2zd2zzsaw_bbvzd2configzd2 = BFALSE;
							}
						else
							{	/* SawBbv/bbv-config.scm 54 */
								bool_t BgL_test1182z00_582;

								{	/* SawBbv/bbv-config.scm 54 */
									long BgL_l1z00_128;

									BgL_l1z00_128 = STRING_LENGTH(((obj_t) BgL_ez00_6));
									if ((BgL_l1z00_128 == 4L))
										{	/* SawBbv/bbv-config.scm 54 */
											int BgL_arg1282z00_131;

											{	/* SawBbv/bbv-config.scm 54 */
												char *BgL_auxz00_590;
												char *BgL_tmpz00_587;

												BgL_auxz00_590 =
													BSTRING_TO_STRING
													(BGl_string1071z00zzsaw_bbvzd2configzd2);
												BgL_tmpz00_587 =
													BSTRING_TO_STRING(((obj_t) BgL_ez00_6));
												BgL_arg1282z00_131 =
													memcmp(BgL_tmpz00_587, BgL_auxz00_590, BgL_l1z00_128);
											}
											BgL_test1182z00_582 = ((long) (BgL_arg1282z00_131) == 0L);
										}
									else
										{	/* SawBbv/bbv-config.scm 54 */
											BgL_test1182z00_582 = ((bool_t) 0);
										}
								}
								if (BgL_test1182z00_582)
									{	/* SawBbv/bbv-config.scm 54 */
										BGl_za2bbvzd2logza2zd2zzsaw_bbvzd2configzd2 = BTRUE;
									}
								else
									{	/* SawBbv/bbv-config.scm 54 */
										BGl_za2bbvzd2logza2zd2zzsaw_bbvzd2configzd2 =
											BGl_errorz00zz__errorz00
											(BGl_string1075z00zzsaw_bbvzd2configzd2,
											BGl_string1073z00zzsaw_bbvzd2configzd2, BgL_ez00_6);
									}
							}
					}
				else
					{	/* SawBbv/bbv-config.scm 52 */
						BGl_za2bbvzd2logza2zd2zzsaw_bbvzd2configzd2 = BFALSE;
					}
			}
			{	/* SawBbv/bbv-config.scm 61 */
				obj_t BgL_ez00_9;

				BgL_ez00_9 =
					BGl_getenvz00zz__osz00(BGl_string1076z00zzsaw_bbvzd2configzd2);
				if (CBOOL(BgL_ez00_9))
					{	/* SawBbv/bbv-config.scm 64 */
						obj_t BgL__ortest_1012z00_10;

						{	/* SawBbv/bbv-config.scm 64 */

							BgL__ortest_1012z00_10 =
								BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(BgL_ez00_9,
								BINT(10L));
						}
						if (CBOOL(BgL__ortest_1012z00_10))
							{	/* SawBbv/bbv-config.scm 64 */
								BGl_za2bbvzd2assertza2zd2zzsaw_bbvzd2configzd2 =
									BgL__ortest_1012z00_10;
							}
						else
							{	/* SawBbv/bbv-config.scm 64 */
								BGl_za2bbvzd2assertza2zd2zzsaw_bbvzd2configzd2 =
									BGl_errorz00zz__errorz00
									(BGl_string1077z00zzsaw_bbvzd2configzd2,
									BGl_string1078z00zzsaw_bbvzd2configzd2, BgL_ez00_9);
							}
					}
				else
					{	/* SawBbv/bbv-config.scm 62 */
						BGl_za2bbvzd2assertza2zd2zzsaw_bbvzd2configzd2 = BINT(0L);
					}
			}
			{	/* SawBbv/bbv-config.scm 71 */
				obj_t BgL_ez00_13;

				BgL_ez00_13 =
					BGl_getenvz00zz__osz00(BGl_string1079z00zzsaw_bbvzd2configzd2);
				if (CBOOL(BgL_ez00_13))
					{	/* SawBbv/bbv-config.scm 74 */
						bool_t BgL_test1187z00_608;

						{	/* SawBbv/bbv-config.scm 74 */
							long BgL_l1z00_139;

							BgL_l1z00_139 = STRING_LENGTH(((obj_t) BgL_ez00_13));
							if ((BgL_l1z00_139 == 0L))
								{	/* SawBbv/bbv-config.scm 74 */
									int BgL_arg1282z00_142;

									{	/* SawBbv/bbv-config.scm 74 */
										char *BgL_auxz00_616;
										char *BgL_tmpz00_613;

										BgL_auxz00_616 =
											BSTRING_TO_STRING(BGl_string1080z00zzsaw_bbvzd2configzd2);
										BgL_tmpz00_613 = BSTRING_TO_STRING(((obj_t) BgL_ez00_13));
										BgL_arg1282z00_142 =
											memcmp(BgL_tmpz00_613, BgL_auxz00_616, BgL_l1z00_139);
									}
									BgL_test1187z00_608 = ((long) (BgL_arg1282z00_142) == 0L);
								}
							else
								{	/* SawBbv/bbv-config.scm 74 */
									BgL_test1187z00_608 = ((bool_t) 0);
								}
						}
						if (BgL_test1187z00_608)
							{	/* SawBbv/bbv-config.scm 74 */
								BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2 =
									BTRUE;
							}
						else
							{	/* SawBbv/bbv-config.scm 75 */
								bool_t BgL_test1189z00_621;

								{	/* SawBbv/bbv-config.scm 75 */
									long BgL_l1z00_150;

									BgL_l1z00_150 = STRING_LENGTH(((obj_t) BgL_ez00_13));
									if ((BgL_l1z00_150 == 5L))
										{	/* SawBbv/bbv-config.scm 75 */
											int BgL_arg1282z00_153;

											{	/* SawBbv/bbv-config.scm 75 */
												char *BgL_auxz00_629;
												char *BgL_tmpz00_626;

												BgL_auxz00_629 =
													BSTRING_TO_STRING
													(BGl_string1070z00zzsaw_bbvzd2configzd2);
												BgL_tmpz00_626 =
													BSTRING_TO_STRING(((obj_t) BgL_ez00_13));
												BgL_arg1282z00_153 =
													memcmp(BgL_tmpz00_626, BgL_auxz00_629, BgL_l1z00_150);
											}
											BgL_test1189z00_621 = ((long) (BgL_arg1282z00_153) == 0L);
										}
									else
										{	/* SawBbv/bbv-config.scm 75 */
											BgL_test1189z00_621 = ((bool_t) 0);
										}
								}
								if (BgL_test1189z00_621)
									{	/* SawBbv/bbv-config.scm 75 */
										BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2 =
											BFALSE;
									}
								else
									{	/* SawBbv/bbv-config.scm 76 */
										bool_t BgL_test1191z00_634;

										{	/* SawBbv/bbv-config.scm 76 */
											long BgL_l1z00_161;

											BgL_l1z00_161 = STRING_LENGTH(((obj_t) BgL_ez00_13));
											if ((BgL_l1z00_161 == 4L))
												{	/* SawBbv/bbv-config.scm 76 */
													int BgL_arg1282z00_164;

													{	/* SawBbv/bbv-config.scm 76 */
														char *BgL_auxz00_642;
														char *BgL_tmpz00_639;

														BgL_auxz00_642 =
															BSTRING_TO_STRING
															(BGl_string1071z00zzsaw_bbvzd2configzd2);
														BgL_tmpz00_639 =
															BSTRING_TO_STRING(((obj_t) BgL_ez00_13));
														BgL_arg1282z00_164 =
															memcmp(BgL_tmpz00_639, BgL_auxz00_642,
															BgL_l1z00_161);
													}
													BgL_test1191z00_634 =
														((long) (BgL_arg1282z00_164) == 0L);
												}
											else
												{	/* SawBbv/bbv-config.scm 76 */
													BgL_test1191z00_634 = ((bool_t) 0);
												}
										}
										if (BgL_test1191z00_634)
											{	/* SawBbv/bbv-config.scm 76 */
												BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2
													= BTRUE;
											}
										else
											{	/* SawBbv/bbv-config.scm 77 */
												bool_t BgL_test1193z00_647;

												{	/* SawBbv/bbv-config.scm 77 */
													obj_t BgL_g1017z00_30;

													{	/* SawBbv/bbv-config.scm 79 */
														obj_t BgL_list1034z00_40;

														BgL_list1034z00_40 =
															MAKE_YOUNG_PAIR
															(BGl_string1081z00zzsaw_bbvzd2configzd2, BNIL);
														BgL_g1017z00_30 =
															BGl_stringzd2splitzd2zz__r4_strings_6_7z00
															(BgL_ez00_13, BgL_list1034z00_40);
													}
													{
														obj_t BgL_l1015z00_32;

														BgL_l1015z00_32 = BgL_g1017z00_30;
													BgL_zc3z04anonymousza31032ze3z87_33:
														if (NULLP(BgL_l1015z00_32))
															{	/* SawBbv/bbv-config.scm 79 */
																BgL_test1193z00_647 = ((bool_t) 1);
															}
														else
															{	/* SawBbv/bbv-config.scm 79 */
																if (CBOOL
																	(BGl_memberz00zz__r4_pairs_and_lists_6_3z00
																		(CAR(((obj_t) BgL_l1015z00_32)),
																			CNST_TABLE_REF(0))))
																	{
																		obj_t BgL_l1015z00_658;

																		BgL_l1015z00_658 =
																			CDR(((obj_t) BgL_l1015z00_32));
																		BgL_l1015z00_32 = BgL_l1015z00_658;
																		goto BgL_zc3z04anonymousza31032ze3z87_33;
																	}
																else
																	{	/* SawBbv/bbv-config.scm 79 */
																		BgL_test1193z00_647 = ((bool_t) 0);
																	}
															}
													}
												}
												if (BgL_test1193z00_647)
													{	/* SawBbv/bbv-config.scm 77 */
														BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2
															= BgL_ez00_13;
													}
												else
													{	/* SawBbv/bbv-config.scm 81 */
														obj_t BgL_fun1031z00_29;

														BgL_fun1031z00_29 =
															BGl_errorz00zz__errorz00
															(BGl_string1082z00zzsaw_bbvzd2configzd2,
															BGl_string1073z00zzsaw_bbvzd2configzd2,
															BgL_ez00_13);
														BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2
															= BGL_PROCEDURE_CALL0(BgL_fun1031z00_29);
													}
											}
									}
							}
					}
				else
					{	/* SawBbv/bbv-config.scm 73 */
						BGl_za2bbvzd2blockszd2cleanupza2z00zzsaw_bbvzd2configzd2 = BTRUE;
					}
			}
			{	/* SawBbv/bbv-config.scm 87 */
				obj_t BgL_ez00_41;

				BgL_ez00_41 =
					BGl_getenvz00zz__osz00(BGl_string1083z00zzsaw_bbvzd2configzd2);
				if (CBOOL(BgL_ez00_41))
					{	/* SawBbv/bbv-config.scm 90 */
						bool_t BgL_test1197z00_668;

						{	/* SawBbv/bbv-config.scm 90 */
							long BgL_l1z00_174;

							BgL_l1z00_174 = STRING_LENGTH(((obj_t) BgL_ez00_41));
							if ((BgL_l1z00_174 == 5L))
								{	/* SawBbv/bbv-config.scm 90 */
									int BgL_arg1282z00_177;

									{	/* SawBbv/bbv-config.scm 90 */
										char *BgL_auxz00_676;
										char *BgL_tmpz00_673;

										BgL_auxz00_676 =
											BSTRING_TO_STRING(BGl_string1070z00zzsaw_bbvzd2configzd2);
										BgL_tmpz00_673 = BSTRING_TO_STRING(((obj_t) BgL_ez00_41));
										BgL_arg1282z00_177 =
											memcmp(BgL_tmpz00_673, BgL_auxz00_676, BgL_l1z00_174);
									}
									BgL_test1197z00_668 = ((long) (BgL_arg1282z00_177) == 0L);
								}
							else
								{	/* SawBbv/bbv-config.scm 90 */
									BgL_test1197z00_668 = ((bool_t) 0);
								}
						}
						if (BgL_test1197z00_668)
							{	/* SawBbv/bbv-config.scm 90 */
								BGl_za2bbvzd2blockszd2gcza2z00zzsaw_bbvzd2configzd2 = BFALSE;
							}
						else
							{	/* SawBbv/bbv-config.scm 91 */
								bool_t BgL_test1199z00_681;

								{	/* SawBbv/bbv-config.scm 91 */
									long BgL_l1z00_185;

									BgL_l1z00_185 = STRING_LENGTH(((obj_t) BgL_ez00_41));
									if ((BgL_l1z00_185 == 3L))
										{	/* SawBbv/bbv-config.scm 91 */
											int BgL_arg1282z00_188;

											{	/* SawBbv/bbv-config.scm 91 */
												char *BgL_auxz00_689;
												char *BgL_tmpz00_686;

												BgL_auxz00_689 =
													BSTRING_TO_STRING
													(BGl_string1084z00zzsaw_bbvzd2configzd2);
												BgL_tmpz00_686 =
													BSTRING_TO_STRING(((obj_t) BgL_ez00_41));
												BgL_arg1282z00_188 =
													memcmp(BgL_tmpz00_686, BgL_auxz00_689, BgL_l1z00_185);
											}
											BgL_test1199z00_681 = ((long) (BgL_arg1282z00_188) == 0L);
										}
									else
										{	/* SawBbv/bbv-config.scm 91 */
											BgL_test1199z00_681 = ((bool_t) 0);
										}
								}
								if (BgL_test1199z00_681)
									{	/* SawBbv/bbv-config.scm 91 */
										BGl_za2bbvzd2blockszd2gcza2z00zzsaw_bbvzd2configzd2 =
											CNST_TABLE_REF(1);
									}
								else
									{	/* SawBbv/bbv-config.scm 92 */
										bool_t BgL_test1201z00_695;

										{	/* SawBbv/bbv-config.scm 92 */
											long BgL_l1z00_196;

											BgL_l1z00_196 = STRING_LENGTH(((obj_t) BgL_ez00_41));
											if ((BgL_l1z00_196 == 3L))
												{	/* SawBbv/bbv-config.scm 92 */
													int BgL_arg1282z00_199;

													{	/* SawBbv/bbv-config.scm 92 */
														char *BgL_auxz00_703;
														char *BgL_tmpz00_700;

														BgL_auxz00_703 =
															BSTRING_TO_STRING
															(BGl_string1085z00zzsaw_bbvzd2configzd2);
														BgL_tmpz00_700 =
															BSTRING_TO_STRING(((obj_t) BgL_ez00_41));
														BgL_arg1282z00_199 =
															memcmp(BgL_tmpz00_700, BgL_auxz00_703,
															BgL_l1z00_196);
													}
													BgL_test1201z00_695 =
														((long) (BgL_arg1282z00_199) == 0L);
												}
											else
												{	/* SawBbv/bbv-config.scm 92 */
													BgL_test1201z00_695 = ((bool_t) 0);
												}
										}
										if (BgL_test1201z00_695)
											{	/* SawBbv/bbv-config.scm 92 */
												BGl_za2bbvzd2blockszd2gcza2z00zzsaw_bbvzd2configzd2 =
													CNST_TABLE_REF(2);
											}
										else
											{	/* SawBbv/bbv-config.scm 92 */
												BGl_za2bbvzd2blockszd2gcza2z00zzsaw_bbvzd2configzd2 =
													BGl_errorz00zz__errorz00
													(BGl_string1086z00zzsaw_bbvzd2configzd2,
													BGl_string1073z00zzsaw_bbvzd2configzd2, BgL_ez00_41);
											}
									}
							}
					}
				else
					{	/* SawBbv/bbv-config.scm 89 */
						BGl_za2bbvzd2blockszd2gcza2z00zzsaw_bbvzd2configzd2 =
							CNST_TABLE_REF(2);
					}
			}
			{	/* SawBbv/bbv-config.scm 99 */
				obj_t BgL_ez00_45;

				BgL_ez00_45 =
					BGl_getenvz00zz__osz00(BGl_string1087z00zzsaw_bbvzd2configzd2);
				if (CBOOL(BgL_ez00_45))
					{	/* SawBbv/bbv-config.scm 102 */
						bool_t BgL_test1204z00_714;

						{	/* SawBbv/bbv-config.scm 102 */
							long BgL_l1z00_207;

							BgL_l1z00_207 = STRING_LENGTH(((obj_t) BgL_ez00_45));
							if ((BgL_l1z00_207 == 3L))
								{	/* SawBbv/bbv-config.scm 102 */
									int BgL_arg1282z00_210;

									{	/* SawBbv/bbv-config.scm 102 */
										char *BgL_auxz00_722;
										char *BgL_tmpz00_719;

										BgL_auxz00_722 =
											BSTRING_TO_STRING(BGl_string1088z00zzsaw_bbvzd2configzd2);
										BgL_tmpz00_719 = BSTRING_TO_STRING(((obj_t) BgL_ez00_45));
										BgL_arg1282z00_210 =
											memcmp(BgL_tmpz00_719, BgL_auxz00_722, BgL_l1z00_207);
									}
									BgL_test1204z00_714 = ((long) (BgL_arg1282z00_210) == 0L);
								}
							else
								{	/* SawBbv/bbv-config.scm 102 */
									BgL_test1204z00_714 = ((bool_t) 0);
								}
						}
						if (BgL_test1204z00_714)
							{	/* SawBbv/bbv-config.scm 102 */
								BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2 =
									CNST_TABLE_REF(3);
							}
						else
							{	/* SawBbv/bbv-config.scm 103 */
								bool_t BgL_test1206z00_728;

								{	/* SawBbv/bbv-config.scm 103 */
									long BgL_l1z00_218;

									BgL_l1z00_218 = STRING_LENGTH(((obj_t) BgL_ez00_45));
									if ((BgL_l1z00_218 == 6L))
										{	/* SawBbv/bbv-config.scm 103 */
											int BgL_arg1282z00_221;

											{	/* SawBbv/bbv-config.scm 103 */
												char *BgL_auxz00_736;
												char *BgL_tmpz00_733;

												BgL_auxz00_736 =
													BSTRING_TO_STRING
													(BGl_string1089z00zzsaw_bbvzd2configzd2);
												BgL_tmpz00_733 =
													BSTRING_TO_STRING(((obj_t) BgL_ez00_45));
												BgL_arg1282z00_221 =
													memcmp(BgL_tmpz00_733, BgL_auxz00_736, BgL_l1z00_218);
											}
											BgL_test1206z00_728 = ((long) (BgL_arg1282z00_221) == 0L);
										}
									else
										{	/* SawBbv/bbv-config.scm 103 */
											BgL_test1206z00_728 = ((bool_t) 0);
										}
								}
								if (BgL_test1206z00_728)
									{	/* SawBbv/bbv-config.scm 103 */
										BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2 =
											CNST_TABLE_REF(4);
									}
								else
									{	/* SawBbv/bbv-config.scm 104 */
										bool_t BgL_test1208z00_742;

										{	/* SawBbv/bbv-config.scm 104 */
											long BgL_l1z00_229;

											BgL_l1z00_229 = STRING_LENGTH(((obj_t) BgL_ez00_45));
											if ((BgL_l1z00_229 == 6L))
												{	/* SawBbv/bbv-config.scm 104 */
													int BgL_arg1282z00_232;

													{	/* SawBbv/bbv-config.scm 104 */
														char *BgL_auxz00_750;
														char *BgL_tmpz00_747;

														BgL_auxz00_750 =
															BSTRING_TO_STRING
															(BGl_string1090z00zzsaw_bbvzd2configzd2);
														BgL_tmpz00_747 =
															BSTRING_TO_STRING(((obj_t) BgL_ez00_45));
														BgL_arg1282z00_232 =
															memcmp(BgL_tmpz00_747, BgL_auxz00_750,
															BgL_l1z00_229);
													}
													BgL_test1208z00_742 =
														((long) (BgL_arg1282z00_232) == 0L);
												}
											else
												{	/* SawBbv/bbv-config.scm 104 */
													BgL_test1208z00_742 = ((bool_t) 0);
												}
										}
										if (BgL_test1208z00_742)
											{	/* SawBbv/bbv-config.scm 104 */
												BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2
													= CNST_TABLE_REF(5);
											}
										else
											{	/* SawBbv/bbv-config.scm 105 */
												bool_t BgL_test1210z00_756;

												{	/* SawBbv/bbv-config.scm 105 */
													long BgL_l1z00_240;

													BgL_l1z00_240 = STRING_LENGTH(((obj_t) BgL_ez00_45));
													if ((BgL_l1z00_240 == 6L))
														{	/* SawBbv/bbv-config.scm 105 */
															int BgL_arg1282z00_243;

															{	/* SawBbv/bbv-config.scm 105 */
																char *BgL_auxz00_764;
																char *BgL_tmpz00_761;

																BgL_auxz00_764 =
																	BSTRING_TO_STRING
																	(BGl_string1091z00zzsaw_bbvzd2configzd2);
																BgL_tmpz00_761 =
																	BSTRING_TO_STRING(((obj_t) BgL_ez00_45));
																BgL_arg1282z00_243 =
																	memcmp(BgL_tmpz00_761, BgL_auxz00_764,
																	BgL_l1z00_240);
															}
															BgL_test1210z00_756 =
																((long) (BgL_arg1282z00_243) == 0L);
														}
													else
														{	/* SawBbv/bbv-config.scm 105 */
															BgL_test1210z00_756 = ((bool_t) 0);
														}
												}
												if (BgL_test1210z00_756)
													{	/* SawBbv/bbv-config.scm 105 */
														BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2
															= CNST_TABLE_REF(6);
													}
												else
													{	/* SawBbv/bbv-config.scm 106 */
														bool_t BgL_test1212z00_770;

														{	/* SawBbv/bbv-config.scm 106 */
															long BgL_l1z00_251;

															BgL_l1z00_251 =
																STRING_LENGTH(((obj_t) BgL_ez00_45));
															if ((BgL_l1z00_251 == 6L))
																{	/* SawBbv/bbv-config.scm 106 */
																	int BgL_arg1282z00_254;

																	{	/* SawBbv/bbv-config.scm 106 */
																		char *BgL_auxz00_778;
																		char *BgL_tmpz00_775;

																		BgL_auxz00_778 =
																			BSTRING_TO_STRING
																			(BGl_string1092z00zzsaw_bbvzd2configzd2);
																		BgL_tmpz00_775 =
																			BSTRING_TO_STRING(((obj_t) BgL_ez00_45));
																		BgL_arg1282z00_254 =
																			memcmp(BgL_tmpz00_775, BgL_auxz00_778,
																			BgL_l1z00_251);
																	}
																	BgL_test1212z00_770 =
																		((long) (BgL_arg1282z00_254) == 0L);
																}
															else
																{	/* SawBbv/bbv-config.scm 106 */
																	BgL_test1212z00_770 = ((bool_t) 0);
																}
														}
														if (BgL_test1212z00_770)
															{	/* SawBbv/bbv-config.scm 106 */
																BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2
																	= CNST_TABLE_REF(7);
															}
														else
															{	/* SawBbv/bbv-config.scm 107 */
																bool_t BgL_test1214z00_784;

																{	/* SawBbv/bbv-config.scm 107 */
																	long BgL_l1z00_262;

																	BgL_l1z00_262 =
																		STRING_LENGTH(((obj_t) BgL_ez00_45));
																	if ((BgL_l1z00_262 == 5L))
																		{	/* SawBbv/bbv-config.scm 107 */
																			int BgL_arg1282z00_265;

																			{	/* SawBbv/bbv-config.scm 107 */
																				char *BgL_auxz00_792;
																				char *BgL_tmpz00_789;

																				BgL_auxz00_792 =
																					BSTRING_TO_STRING
																					(BGl_string1093z00zzsaw_bbvzd2configzd2);
																				BgL_tmpz00_789 =
																					BSTRING_TO_STRING(((obj_t)
																						BgL_ez00_45));
																				BgL_arg1282z00_265 =
																					memcmp(BgL_tmpz00_789, BgL_auxz00_792,
																					BgL_l1z00_262);
																			}
																			BgL_test1214z00_784 =
																				((long) (BgL_arg1282z00_265) == 0L);
																		}
																	else
																		{	/* SawBbv/bbv-config.scm 107 */
																			BgL_test1214z00_784 = ((bool_t) 0);
																		}
																}
																if (BgL_test1214z00_784)
																	{	/* SawBbv/bbv-config.scm 107 */
																		BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2
																			= CNST_TABLE_REF(8);
																	}
																else
																	{	/* SawBbv/bbv-config.scm 108 */
																		bool_t BgL_test1216z00_798;

																		{	/* SawBbv/bbv-config.scm 108 */
																			long BgL_l1z00_273;

																			BgL_l1z00_273 =
																				STRING_LENGTH(((obj_t) BgL_ez00_45));
																			if ((BgL_l1z00_273 == 9L))
																				{	/* SawBbv/bbv-config.scm 108 */
																					int BgL_arg1282z00_276;

																					{	/* SawBbv/bbv-config.scm 108 */
																						char *BgL_auxz00_806;
																						char *BgL_tmpz00_803;

																						BgL_auxz00_806 =
																							BSTRING_TO_STRING
																							(BGl_string1094z00zzsaw_bbvzd2configzd2);
																						BgL_tmpz00_803 =
																							BSTRING_TO_STRING(((obj_t)
																								BgL_ez00_45));
																						BgL_arg1282z00_276 =
																							memcmp(BgL_tmpz00_803,
																							BgL_auxz00_806, BgL_l1z00_273);
																					}
																					BgL_test1216z00_798 =
																						((long) (BgL_arg1282z00_276) == 0L);
																				}
																			else
																				{	/* SawBbv/bbv-config.scm 108 */
																					BgL_test1216z00_798 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test1216z00_798)
																			{	/* SawBbv/bbv-config.scm 108 */
																				BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2
																					= CNST_TABLE_REF(9);
																			}
																		else
																			{	/* SawBbv/bbv-config.scm 109 */
																				bool_t BgL_test1218z00_812;

																				{	/* SawBbv/bbv-config.scm 109 */
																					long BgL_l1z00_284;

																					BgL_l1z00_284 =
																						STRING_LENGTH(
																						((obj_t) BgL_ez00_45));
																					if ((BgL_l1z00_284 == 5L))
																						{	/* SawBbv/bbv-config.scm 109 */
																							int BgL_arg1282z00_287;

																							{	/* SawBbv/bbv-config.scm 109 */
																								char *BgL_auxz00_820;
																								char *BgL_tmpz00_817;

																								BgL_auxz00_820 =
																									BSTRING_TO_STRING
																									(BGl_string1095z00zzsaw_bbvzd2configzd2);
																								BgL_tmpz00_817 =
																									BSTRING_TO_STRING(((obj_t)
																										BgL_ez00_45));
																								BgL_arg1282z00_287 =
																									memcmp(BgL_tmpz00_817,
																									BgL_auxz00_820,
																									BgL_l1z00_284);
																							}
																							BgL_test1218z00_812 =
																								(
																								(long) (BgL_arg1282z00_287) ==
																								0L);
																						}
																					else
																						{	/* SawBbv/bbv-config.scm 109 */
																							BgL_test1218z00_812 =
																								((bool_t) 0);
																						}
																				}
																				if (BgL_test1218z00_812)
																					{	/* SawBbv/bbv-config.scm 109 */
																						BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2
																							= CNST_TABLE_REF(10);
																					}
																				else
																					{	/* SawBbv/bbv-config.scm 110 */
																						bool_t BgL_test1220z00_826;

																						{	/* SawBbv/bbv-config.scm 110 */
																							long BgL_l1z00_295;

																							BgL_l1z00_295 =
																								STRING_LENGTH(
																								((obj_t) BgL_ez00_45));
																							if ((BgL_l1z00_295 == 7L))
																								{	/* SawBbv/bbv-config.scm 110 */
																									int BgL_arg1282z00_298;

																									{	/* SawBbv/bbv-config.scm 110 */
																										char *BgL_auxz00_834;
																										char *BgL_tmpz00_831;

																										BgL_auxz00_834 =
																											BSTRING_TO_STRING
																											(BGl_string1096z00zzsaw_bbvzd2configzd2);
																										BgL_tmpz00_831 =
																											BSTRING_TO_STRING(((obj_t)
																												BgL_ez00_45));
																										BgL_arg1282z00_298 =
																											memcmp(BgL_tmpz00_831,
																											BgL_auxz00_834,
																											BgL_l1z00_295);
																									}
																									BgL_test1220z00_826 =
																										(
																										(long) (BgL_arg1282z00_298)
																										== 0L);
																								}
																							else
																								{	/* SawBbv/bbv-config.scm 110 */
																									BgL_test1220z00_826 =
																										((bool_t) 0);
																								}
																						}
																						if (BgL_test1220z00_826)
																							{	/* SawBbv/bbv-config.scm 110 */
																								BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2
																									= CNST_TABLE_REF(11);
																							}
																						else
																							{	/* SawBbv/bbv-config.scm 111 */
																								bool_t BgL_test1222z00_840;

																								{	/* SawBbv/bbv-config.scm 111 */
																									long BgL_l1z00_306;

																									BgL_l1z00_306 =
																										STRING_LENGTH(
																										((obj_t) BgL_ez00_45));
																									if ((BgL_l1z00_306 == 4L))
																										{	/* SawBbv/bbv-config.scm 111 */
																											int BgL_arg1282z00_309;

																											{	/* SawBbv/bbv-config.scm 111 */
																												char *BgL_auxz00_848;
																												char *BgL_tmpz00_845;

																												BgL_auxz00_848 =
																													BSTRING_TO_STRING
																													(BGl_string1097z00zzsaw_bbvzd2configzd2);
																												BgL_tmpz00_845 =
																													BSTRING_TO_STRING((
																														(obj_t)
																														BgL_ez00_45));
																												BgL_arg1282z00_309 =
																													memcmp(BgL_tmpz00_845,
																													BgL_auxz00_848,
																													BgL_l1z00_306);
																											}
																											BgL_test1222z00_840 =
																												(
																												(long)
																												(BgL_arg1282z00_309) ==
																												0L);
																										}
																									else
																										{	/* SawBbv/bbv-config.scm 111 */
																											BgL_test1222z00_840 =
																												((bool_t) 0);
																										}
																								}
																								if (BgL_test1222z00_840)
																									{	/* SawBbv/bbv-config.scm 111 */
																										BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2
																											= CNST_TABLE_REF(12);
																									}
																								else
																									{	/* SawBbv/bbv-config.scm 112 */
																										bool_t BgL_test1224z00_854;

																										{	/* SawBbv/bbv-config.scm 112 */
																											long BgL_l1z00_317;

																											BgL_l1z00_317 =
																												STRING_LENGTH(
																												((obj_t) BgL_ez00_45));
																											if ((BgL_l1z00_317 == 6L))
																												{	/* SawBbv/bbv-config.scm 112 */
																													int
																														BgL_arg1282z00_320;
																													{	/* SawBbv/bbv-config.scm 112 */
																														char
																															*BgL_auxz00_862;
																														char
																															*BgL_tmpz00_859;
																														BgL_auxz00_862 =
																															BSTRING_TO_STRING
																															(BGl_string1098z00zzsaw_bbvzd2configzd2);
																														BgL_tmpz00_859 =
																															BSTRING_TO_STRING(
																															((obj_t)
																																BgL_ez00_45));
																														BgL_arg1282z00_320 =
																															memcmp
																															(BgL_tmpz00_859,
																															BgL_auxz00_862,
																															BgL_l1z00_317);
																													}
																													BgL_test1224z00_854 =
																														(
																														(long)
																														(BgL_arg1282z00_320)
																														== 0L);
																												}
																											else
																												{	/* SawBbv/bbv-config.scm 112 */
																													BgL_test1224z00_854 =
																														((bool_t) 0);
																												}
																										}
																										if (BgL_test1224z00_854)
																											{	/* SawBbv/bbv-config.scm 112 */
																												BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2
																													= CNST_TABLE_REF(13);
																											}
																										else
																											{	/* SawBbv/bbv-config.scm 113 */
																												bool_t
																													BgL_test1226z00_868;
																												{	/* SawBbv/bbv-config.scm 113 */
																													long BgL_l1z00_328;

																													BgL_l1z00_328 =
																														STRING_LENGTH(
																														((obj_t)
																															BgL_ez00_45));
																													if ((BgL_l1z00_328 ==
																															8L))
																														{	/* SawBbv/bbv-config.scm 113 */
																															int
																																BgL_arg1282z00_331;
																															{	/* SawBbv/bbv-config.scm 113 */
																																char
																																	*BgL_auxz00_876;
																																char
																																	*BgL_tmpz00_873;
																																BgL_auxz00_876 =
																																	BSTRING_TO_STRING
																																	(BGl_string1099z00zzsaw_bbvzd2configzd2);
																																BgL_tmpz00_873 =
																																	BSTRING_TO_STRING
																																	(((obj_t)
																																		BgL_ez00_45));
																																BgL_arg1282z00_331
																																	=
																																	memcmp
																																	(BgL_tmpz00_873,
																																	BgL_auxz00_876,
																																	BgL_l1z00_328);
																															}
																															BgL_test1226z00_868
																																=
																																((long)
																																(BgL_arg1282z00_331)
																																== 0L);
																														}
																													else
																														{	/* SawBbv/bbv-config.scm 113 */
																															BgL_test1226z00_868
																																= ((bool_t) 0);
																														}
																												}
																												if (BgL_test1226z00_868)
																													{	/* SawBbv/bbv-config.scm 113 */
																														BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2
																															=
																															CNST_TABLE_REF
																															(14);
																													}
																												else
																													{	/* SawBbv/bbv-config.scm 113 */
																														BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2
																															=
																															BGl_errorz00zz__errorz00
																															(BGl_string1100z00zzsaw_bbvzd2configzd2,
																															BGl_string1101z00zzsaw_bbvzd2configzd2,
																															BgL_ez00_45);
																													}
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
					}
				else
					{	/* SawBbv/bbv-config.scm 101 */
						BGl_za2bbvzd2mergezd2strategyza2z00zzsaw_bbvzd2configzd2 =
							CNST_TABLE_REF(4);
					}
			}
			{	/* SawBbv/bbv-config.scm 120 */
				obj_t BgL_ez00_58;

				BgL_ez00_58 =
					BGl_getenvz00zz__osz00(BGl_string1102z00zzsaw_bbvzd2configzd2);
				if (CBOOL(BgL_ez00_58))
					{	/* SawBbv/bbv-config.scm 123 */
						bool_t BgL_test1229z00_887;

						{	/* SawBbv/bbv-config.scm 123 */
							long BgL_l1z00_339;

							BgL_l1z00_339 = STRING_LENGTH(((obj_t) BgL_ez00_58));
							if ((BgL_l1z00_339 == 5L))
								{	/* SawBbv/bbv-config.scm 123 */
									int BgL_arg1282z00_342;

									{	/* SawBbv/bbv-config.scm 123 */
										char *BgL_auxz00_895;
										char *BgL_tmpz00_892;

										BgL_auxz00_895 =
											BSTRING_TO_STRING(BGl_string1070z00zzsaw_bbvzd2configzd2);
										BgL_tmpz00_892 = BSTRING_TO_STRING(((obj_t) BgL_ez00_58));
										BgL_arg1282z00_342 =
											memcmp(BgL_tmpz00_892, BgL_auxz00_895, BgL_l1z00_339);
									}
									BgL_test1229z00_887 = ((long) (BgL_arg1282z00_342) == 0L);
								}
							else
								{	/* SawBbv/bbv-config.scm 123 */
									BgL_test1229z00_887 = ((bool_t) 0);
								}
						}
						if (BgL_test1229z00_887)
							{	/* SawBbv/bbv-config.scm 123 */
								BGl_za2bbvzd2optimzd2vlengthza2z00zzsaw_bbvzd2configzd2 =
									BFALSE;
							}
						else
							{	/* SawBbv/bbv-config.scm 124 */
								bool_t BgL_test1231z00_900;

								{	/* SawBbv/bbv-config.scm 124 */
									long BgL_l1z00_350;

									BgL_l1z00_350 = STRING_LENGTH(((obj_t) BgL_ez00_58));
									if ((BgL_l1z00_350 == 4L))
										{	/* SawBbv/bbv-config.scm 124 */
											int BgL_arg1282z00_353;

											{	/* SawBbv/bbv-config.scm 124 */
												char *BgL_auxz00_908;
												char *BgL_tmpz00_905;

												BgL_auxz00_908 =
													BSTRING_TO_STRING
													(BGl_string1071z00zzsaw_bbvzd2configzd2);
												BgL_tmpz00_905 =
													BSTRING_TO_STRING(((obj_t) BgL_ez00_58));
												BgL_arg1282z00_353 =
													memcmp(BgL_tmpz00_905, BgL_auxz00_908, BgL_l1z00_350);
											}
											BgL_test1231z00_900 = ((long) (BgL_arg1282z00_353) == 0L);
										}
									else
										{	/* SawBbv/bbv-config.scm 124 */
											BgL_test1231z00_900 = ((bool_t) 0);
										}
								}
								if (BgL_test1231z00_900)
									{	/* SawBbv/bbv-config.scm 124 */
										BGl_za2bbvzd2optimzd2vlengthza2z00zzsaw_bbvzd2configzd2 =
											BTRUE;
									}
								else
									{	/* SawBbv/bbv-config.scm 124 */
										BGl_za2bbvzd2optimzd2vlengthza2z00zzsaw_bbvzd2configzd2 =
											BGl_errorz00zz__errorz00
											(BGl_string1103z00zzsaw_bbvzd2configzd2,
											BGl_string1073z00zzsaw_bbvzd2configzd2, BgL_ez00_58);
									}
							}
					}
				else
					{	/* SawBbv/bbv-config.scm 122 */
						BGl_za2bbvzd2optimzd2vlengthza2z00zzsaw_bbvzd2configzd2 = BTRUE;
					}
			}
			{	/* SawBbv/bbv-config.scm 131 */
				obj_t BgL_ez00_61;

				{	/* SawBbv/bbv-config.scm 131 */
					obj_t BgL__ortest_1013z00_64;

					BgL__ortest_1013z00_64 =
						BGl_getenvz00zz__osz00(BGl_string1104z00zzsaw_bbvzd2configzd2);
					if (CBOOL(BgL__ortest_1013z00_64))
						{	/* SawBbv/bbv-config.scm 131 */
							BgL_ez00_61 = BgL__ortest_1013z00_64;
						}
					else
						{	/* SawBbv/bbv-config.scm 131 */
							BgL_ez00_61 =
								BGl_getenvz00zz__osz00(BGl_string1102z00zzsaw_bbvzd2configzd2);
						}
				}
				if (CBOOL(BgL_ez00_61))
					{	/* SawBbv/bbv-config.scm 134 */
						bool_t BgL_test1236z00_920;

						{	/* SawBbv/bbv-config.scm 134 */
							long BgL_l1z00_361;

							BgL_l1z00_361 = STRING_LENGTH(((obj_t) BgL_ez00_61));
							if ((BgL_l1z00_361 == 5L))
								{	/* SawBbv/bbv-config.scm 134 */
									int BgL_arg1282z00_364;

									{	/* SawBbv/bbv-config.scm 134 */
										char *BgL_auxz00_928;
										char *BgL_tmpz00_925;

										BgL_auxz00_928 =
											BSTRING_TO_STRING(BGl_string1070z00zzsaw_bbvzd2configzd2);
										BgL_tmpz00_925 = BSTRING_TO_STRING(((obj_t) BgL_ez00_61));
										BgL_arg1282z00_364 =
											memcmp(BgL_tmpz00_925, BgL_auxz00_928, BgL_l1z00_361);
									}
									BgL_test1236z00_920 = ((long) (BgL_arg1282z00_364) == 0L);
								}
							else
								{	/* SawBbv/bbv-config.scm 134 */
									BgL_test1236z00_920 = ((bool_t) 0);
								}
						}
						if (BgL_test1236z00_920)
							{	/* SawBbv/bbv-config.scm 134 */
								BGl_za2bbvzd2optimzd2boundza2z00zzsaw_bbvzd2configzd2 = BFALSE;
							}
						else
							{	/* SawBbv/bbv-config.scm 135 */
								bool_t BgL_test1239z00_933;

								{	/* SawBbv/bbv-config.scm 135 */
									long BgL_l1z00_372;

									BgL_l1z00_372 = STRING_LENGTH(((obj_t) BgL_ez00_61));
									if ((BgL_l1z00_372 == 4L))
										{	/* SawBbv/bbv-config.scm 135 */
											int BgL_arg1282z00_375;

											{	/* SawBbv/bbv-config.scm 135 */
												char *BgL_auxz00_941;
												char *BgL_tmpz00_938;

												BgL_auxz00_941 =
													BSTRING_TO_STRING
													(BGl_string1071z00zzsaw_bbvzd2configzd2);
												BgL_tmpz00_938 =
													BSTRING_TO_STRING(((obj_t) BgL_ez00_61));
												BgL_arg1282z00_375 =
													memcmp(BgL_tmpz00_938, BgL_auxz00_941, BgL_l1z00_372);
											}
											BgL_test1239z00_933 = ((long) (BgL_arg1282z00_375) == 0L);
										}
									else
										{	/* SawBbv/bbv-config.scm 135 */
											BgL_test1239z00_933 = ((bool_t) 0);
										}
								}
								if (BgL_test1239z00_933)
									{	/* SawBbv/bbv-config.scm 135 */
										BGl_za2bbvzd2optimzd2boundza2z00zzsaw_bbvzd2configzd2 =
											BTRUE;
									}
								else
									{	/* SawBbv/bbv-config.scm 135 */
										BGl_za2bbvzd2optimzd2boundza2z00zzsaw_bbvzd2configzd2 =
											BGl_errorz00zz__errorz00
											(BGl_string1105z00zzsaw_bbvzd2configzd2,
											BGl_string1073z00zzsaw_bbvzd2configzd2, BgL_ez00_61);
									}
							}
					}
				else
					{	/* SawBbv/bbv-config.scm 133 */
						BGl_za2bbvzd2optimzd2boundza2z00zzsaw_bbvzd2configzd2 = BFALSE;
					}
			}
			{	/* SawBbv/bbv-config.scm 142 */
				obj_t BgL_ez00_65;

				BgL_ez00_65 =
					BGl_getenvz00zz__osz00(BGl_string1106z00zzsaw_bbvzd2configzd2);
				if (CBOOL(BgL_ez00_65))
					{	/* SawBbv/bbv-config.scm 145 */
						bool_t BgL_test1242z00_950;

						{	/* SawBbv/bbv-config.scm 145 */
							long BgL_l1z00_383;

							BgL_l1z00_383 = STRING_LENGTH(((obj_t) BgL_ez00_65));
							if ((BgL_l1z00_383 == 5L))
								{	/* SawBbv/bbv-config.scm 145 */
									int BgL_arg1282z00_386;

									{	/* SawBbv/bbv-config.scm 145 */
										char *BgL_auxz00_958;
										char *BgL_tmpz00_955;

										BgL_auxz00_958 =
											BSTRING_TO_STRING(BGl_string1070z00zzsaw_bbvzd2configzd2);
										BgL_tmpz00_955 = BSTRING_TO_STRING(((obj_t) BgL_ez00_65));
										BgL_arg1282z00_386 =
											memcmp(BgL_tmpz00_955, BgL_auxz00_958, BgL_l1z00_383);
									}
									BgL_test1242z00_950 = ((long) (BgL_arg1282z00_386) == 0L);
								}
							else
								{	/* SawBbv/bbv-config.scm 145 */
									BgL_test1242z00_950 = ((bool_t) 0);
								}
						}
						if (BgL_test1242z00_950)
							{	/* SawBbv/bbv-config.scm 145 */
								BGl_za2bbvzd2optimzd2aliasza2z00zzsaw_bbvzd2configzd2 = BFALSE;
							}
						else
							{	/* SawBbv/bbv-config.scm 146 */
								bool_t BgL_test1245z00_963;

								{	/* SawBbv/bbv-config.scm 146 */
									long BgL_l1z00_394;

									BgL_l1z00_394 = STRING_LENGTH(((obj_t) BgL_ez00_65));
									if ((BgL_l1z00_394 == 4L))
										{	/* SawBbv/bbv-config.scm 146 */
											int BgL_arg1282z00_397;

											{	/* SawBbv/bbv-config.scm 146 */
												char *BgL_auxz00_971;
												char *BgL_tmpz00_968;

												BgL_auxz00_971 =
													BSTRING_TO_STRING
													(BGl_string1071z00zzsaw_bbvzd2configzd2);
												BgL_tmpz00_968 =
													BSTRING_TO_STRING(((obj_t) BgL_ez00_65));
												BgL_arg1282z00_397 =
													memcmp(BgL_tmpz00_968, BgL_auxz00_971, BgL_l1z00_394);
											}
											BgL_test1245z00_963 = ((long) (BgL_arg1282z00_397) == 0L);
										}
									else
										{	/* SawBbv/bbv-config.scm 146 */
											BgL_test1245z00_963 = ((bool_t) 0);
										}
								}
								if (BgL_test1245z00_963)
									{	/* SawBbv/bbv-config.scm 146 */
										BGl_za2bbvzd2optimzd2aliasza2z00zzsaw_bbvzd2configzd2 =
											BTRUE;
									}
								else
									{	/* SawBbv/bbv-config.scm 146 */
										BGl_za2bbvzd2optimzd2aliasza2z00zzsaw_bbvzd2configzd2 =
											BGl_errorz00zz__errorz00
											(BGl_string1107z00zzsaw_bbvzd2configzd2,
											BGl_string1073z00zzsaw_bbvzd2configzd2, BgL_ez00_65);
									}
							}
					}
				else
					{	/* SawBbv/bbv-config.scm 144 */
						BGl_za2bbvzd2optimzd2aliasza2z00zzsaw_bbvzd2configzd2 = BTRUE;
					}
			}
			{	/* SawBbv/bbv-config.scm 153 */
				obj_t BgL_ez00_68;

				BgL_ez00_68 =
					BGl_getenvz00zz__osz00(BGl_string1108z00zzsaw_bbvzd2configzd2);
				if (CBOOL(BgL_ez00_68))
					{	/* SawBbv/bbv-config.scm 156 */
						obj_t BgL__ortest_1014z00_69;

						{	/* SawBbv/bbv-config.scm 156 */

							BgL__ortest_1014z00_69 =
								BGl_stringzd2ze3numberz31zz__r4_numbers_6_5z00(BgL_ez00_68,
								BINT(10L));
						}
						if (CBOOL(BgL__ortest_1014z00_69))
							{	/* SawBbv/bbv-config.scm 156 */
								BGl_za2bbvzd2verboseza2zd2zzsaw_bbvzd2configzd2 =
									BgL__ortest_1014z00_69;
							}
						else
							{	/* SawBbv/bbv-config.scm 156 */
								BGl_za2bbvzd2verboseza2zd2zzsaw_bbvzd2configzd2 =
									BGl_errorz00zz__errorz00
									(BGl_string1109z00zzsaw_bbvzd2configzd2,
									BGl_string1078z00zzsaw_bbvzd2configzd2, BgL_ez00_68);
							}
					}
				else
					{	/* SawBbv/bbv-config.scm 154 */
						BGl_za2bbvzd2verboseza2zd2zzsaw_bbvzd2configzd2 = BINT(1L);
					}
			}
			{	/* SawBbv/bbv-config.scm 163 */
				obj_t BgL_ez00_72;

				BgL_ez00_72 =
					BGl_getenvz00zz__osz00(BGl_string1110z00zzsaw_bbvzd2configzd2);
				if (CBOOL(BgL_ez00_72))
					{	/* SawBbv/bbv-config.scm 166 */
						bool_t BgL_test1250z00_989;

						{	/* SawBbv/bbv-config.scm 166 */
							long BgL_l1z00_405;

							BgL_l1z00_405 = STRING_LENGTH(((obj_t) BgL_ez00_72));
							if ((BgL_l1z00_405 == 4L))
								{	/* SawBbv/bbv-config.scm 166 */
									int BgL_arg1282z00_408;

									{	/* SawBbv/bbv-config.scm 166 */
										char *BgL_auxz00_997;
										char *BgL_tmpz00_994;

										BgL_auxz00_997 =
											BSTRING_TO_STRING(BGl_string1071z00zzsaw_bbvzd2configzd2);
										BgL_tmpz00_994 = BSTRING_TO_STRING(((obj_t) BgL_ez00_72));
										BgL_arg1282z00_408 =
											memcmp(BgL_tmpz00_994, BgL_auxz00_997, BgL_l1z00_405);
									}
									BgL_test1250z00_989 = ((long) (BgL_arg1282z00_408) == 0L);
								}
							else
								{	/* SawBbv/bbv-config.scm 166 */
									BgL_test1250z00_989 = ((bool_t) 0);
								}
						}
						if (BgL_test1250z00_989)
							{	/* SawBbv/bbv-config.scm 166 */
								BGl_za2bbvzd2dumpzd2jsonza2z00zzsaw_bbvzd2configzd2 = BTRUE;
							}
						else
							{	/* SawBbv/bbv-config.scm 167 */
								bool_t BgL_test1254z00_1002;

								{	/* SawBbv/bbv-config.scm 167 */
									long BgL_l1z00_416;

									BgL_l1z00_416 = STRING_LENGTH(((obj_t) BgL_ez00_72));
									if ((BgL_l1z00_416 == 5L))
										{	/* SawBbv/bbv-config.scm 167 */
											int BgL_arg1282z00_419;

											{	/* SawBbv/bbv-config.scm 167 */
												char *BgL_auxz00_1010;
												char *BgL_tmpz00_1007;

												BgL_auxz00_1010 =
													BSTRING_TO_STRING
													(BGl_string1070z00zzsaw_bbvzd2configzd2);
												BgL_tmpz00_1007 =
													BSTRING_TO_STRING(((obj_t) BgL_ez00_72));
												BgL_arg1282z00_419 =
													memcmp(BgL_tmpz00_1007, BgL_auxz00_1010,
													BgL_l1z00_416);
											}
											BgL_test1254z00_1002 =
												((long) (BgL_arg1282z00_419) == 0L);
										}
									else
										{	/* SawBbv/bbv-config.scm 167 */
											BgL_test1254z00_1002 = ((bool_t) 0);
										}
								}
								if (BgL_test1254z00_1002)
									{	/* SawBbv/bbv-config.scm 167 */
										BGl_za2bbvzd2dumpzd2jsonza2z00zzsaw_bbvzd2configzd2 =
											BFALSE;
									}
								else
									{	/* SawBbv/bbv-config.scm 168 */
										bool_t BgL_test1256z00_1015;

										{	/* SawBbv/bbv-config.scm 168 */
											long BgL_l1z00_427;

											BgL_l1z00_427 = STRING_LENGTH(((obj_t) BgL_ez00_72));
											if ((BgL_l1z00_427 == 0L))
												{	/* SawBbv/bbv-config.scm 168 */
													int BgL_arg1282z00_430;

													{	/* SawBbv/bbv-config.scm 168 */
														char *BgL_auxz00_1023;
														char *BgL_tmpz00_1020;

														BgL_auxz00_1023 =
															BSTRING_TO_STRING
															(BGl_string1080z00zzsaw_bbvzd2configzd2);
														BgL_tmpz00_1020 =
															BSTRING_TO_STRING(((obj_t) BgL_ez00_72));
														BgL_arg1282z00_430 =
															memcmp(BgL_tmpz00_1020, BgL_auxz00_1023,
															BgL_l1z00_427);
													}
													BgL_test1256z00_1015 =
														((long) (BgL_arg1282z00_430) == 0L);
												}
											else
												{	/* SawBbv/bbv-config.scm 168 */
													BgL_test1256z00_1015 = ((bool_t) 0);
												}
										}
										if (BgL_test1256z00_1015)
											{	/* SawBbv/bbv-config.scm 168 */
												BGl_za2bbvzd2dumpzd2jsonza2z00zzsaw_bbvzd2configzd2 =
													BFALSE;
											}
										else
											{	/* SawBbv/bbv-config.scm 168 */
												BGl_za2bbvzd2dumpzd2jsonza2z00zzsaw_bbvzd2configzd2 =
													BGl_errorz00zz__errorz00
													(BGl_string1111z00zzsaw_bbvzd2configzd2,
													BGl_string1078z00zzsaw_bbvzd2configzd2, BgL_ez00_72);
											}
									}
							}
					}
				else
					{	/* SawBbv/bbv-config.scm 165 */
						BGl_za2bbvzd2dumpzd2jsonza2z00zzsaw_bbvzd2configzd2 = BFALSE;
					}
			}
			{	/* SawBbv/bbv-config.scm 175 */
				obj_t BgL_ez00_76;

				BgL_ez00_76 =
					BGl_getenvz00zz__osz00(BGl_string1112z00zzsaw_bbvzd2configzd2);
				if (CBOOL(BgL_ez00_76))
					{	/* SawBbv/bbv-config.scm 178 */
						bool_t BgL_test1259z00_1032;

						{	/* SawBbv/bbv-config.scm 178 */
							long BgL_l1z00_438;

							BgL_l1z00_438 = STRING_LENGTH(((obj_t) BgL_ez00_76));
							if ((BgL_l1z00_438 == 4L))
								{	/* SawBbv/bbv-config.scm 178 */
									int BgL_arg1282z00_441;

									{	/* SawBbv/bbv-config.scm 178 */
										char *BgL_auxz00_1040;
										char *BgL_tmpz00_1037;

										BgL_auxz00_1040 =
											BSTRING_TO_STRING(BGl_string1071z00zzsaw_bbvzd2configzd2);
										BgL_tmpz00_1037 = BSTRING_TO_STRING(((obj_t) BgL_ez00_76));
										BgL_arg1282z00_441 =
											memcmp(BgL_tmpz00_1037, BgL_auxz00_1040, BgL_l1z00_438);
									}
									BgL_test1259z00_1032 = ((long) (BgL_arg1282z00_441) == 0L);
								}
							else
								{	/* SawBbv/bbv-config.scm 178 */
									BgL_test1259z00_1032 = ((bool_t) 0);
								}
						}
						if (BgL_test1259z00_1032)
							{	/* SawBbv/bbv-config.scm 178 */
								BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2 = BTRUE;
							}
						else
							{	/* SawBbv/bbv-config.scm 179 */
								bool_t BgL_test1261z00_1045;

								{	/* SawBbv/bbv-config.scm 179 */
									long BgL_l1z00_449;

									BgL_l1z00_449 = STRING_LENGTH(((obj_t) BgL_ez00_76));
									if ((BgL_l1z00_449 == 5L))
										{	/* SawBbv/bbv-config.scm 179 */
											int BgL_arg1282z00_452;

											{	/* SawBbv/bbv-config.scm 179 */
												char *BgL_auxz00_1053;
												char *BgL_tmpz00_1050;

												BgL_auxz00_1053 =
													BSTRING_TO_STRING
													(BGl_string1070z00zzsaw_bbvzd2configzd2);
												BgL_tmpz00_1050 =
													BSTRING_TO_STRING(((obj_t) BgL_ez00_76));
												BgL_arg1282z00_452 =
													memcmp(BgL_tmpz00_1050, BgL_auxz00_1053,
													BgL_l1z00_449);
											}
											BgL_test1261z00_1045 =
												((long) (BgL_arg1282z00_452) == 0L);
										}
									else
										{	/* SawBbv/bbv-config.scm 179 */
											BgL_test1261z00_1045 = ((bool_t) 0);
										}
								}
								if (BgL_test1261z00_1045)
									{	/* SawBbv/bbv-config.scm 179 */
										BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2 = BFALSE;
									}
								else
									{	/* SawBbv/bbv-config.scm 180 */
										bool_t BgL_test1269z00_1058;

										{	/* SawBbv/bbv-config.scm 180 */
											long BgL_l1z00_460;

											BgL_l1z00_460 = STRING_LENGTH(((obj_t) BgL_ez00_76));
											if ((BgL_l1z00_460 == 0L))
												{	/* SawBbv/bbv-config.scm 180 */
													int BgL_arg1282z00_463;

													{	/* SawBbv/bbv-config.scm 180 */
														char *BgL_auxz00_1066;
														char *BgL_tmpz00_1063;

														BgL_auxz00_1066 =
															BSTRING_TO_STRING
															(BGl_string1080z00zzsaw_bbvzd2configzd2);
														BgL_tmpz00_1063 =
															BSTRING_TO_STRING(((obj_t) BgL_ez00_76));
														BgL_arg1282z00_463 =
															memcmp(BgL_tmpz00_1063, BgL_auxz00_1066,
															BgL_l1z00_460);
													}
													BgL_test1269z00_1058 =
														((long) (BgL_arg1282z00_463) == 0L);
												}
											else
												{	/* SawBbv/bbv-config.scm 180 */
													BgL_test1269z00_1058 = ((bool_t) 0);
												}
										}
										if (BgL_test1269z00_1058)
											{	/* SawBbv/bbv-config.scm 180 */
												BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2 =
													BFALSE;
											}
										else
											{	/* SawBbv/bbv-config.scm 180 */
												BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2 =
													BGl_errorz00zz__errorz00
													(BGl_string1113z00zzsaw_bbvzd2configzd2,
													BGl_string1078z00zzsaw_bbvzd2configzd2, BgL_ez00_76);
											}
									}
							}
					}
				else
					{	/* SawBbv/bbv-config.scm 177 */
						BGl_za2bbvzd2dumpzd2cfgza2z00zzsaw_bbvzd2configzd2 = BFALSE;
					}
			}
			{	/* SawBbv/bbv-config.scm 187 */
				obj_t BgL_ez00_80;

				BgL_ez00_80 =
					BGl_getenvz00zz__osz00(BGl_string1114z00zzsaw_bbvzd2configzd2);
				if (CBOOL(BgL_ez00_80))
					{	/* SawBbv/bbv-config.scm 190 */
						bool_t BgL_test1272z00_1075;

						{	/* SawBbv/bbv-config.scm 190 */
							long BgL_l1z00_471;

							BgL_l1z00_471 = STRING_LENGTH(((obj_t) BgL_ez00_80));
							if ((BgL_l1z00_471 == 4L))
								{	/* SawBbv/bbv-config.scm 190 */
									int BgL_arg1282z00_474;

									{	/* SawBbv/bbv-config.scm 190 */
										char *BgL_auxz00_1083;
										char *BgL_tmpz00_1080;

										BgL_auxz00_1083 =
											BSTRING_TO_STRING(BGl_string1071z00zzsaw_bbvzd2configzd2);
										BgL_tmpz00_1080 = BSTRING_TO_STRING(((obj_t) BgL_ez00_80));
										BgL_arg1282z00_474 =
											memcmp(BgL_tmpz00_1080, BgL_auxz00_1083, BgL_l1z00_471);
									}
									BgL_test1272z00_1075 = ((long) (BgL_arg1282z00_474) == 0L);
								}
							else
								{	/* SawBbv/bbv-config.scm 190 */
									BgL_test1272z00_1075 = ((bool_t) 0);
								}
						}
						if (BgL_test1272z00_1075)
							{	/* SawBbv/bbv-config.scm 190 */
								BGl_za2bbvzd2profileza2zd2zzsaw_bbvzd2configzd2 = BTRUE;
							}
						else
							{	/* SawBbv/bbv-config.scm 191 */
								bool_t BgL_test1274z00_1088;

								{	/* SawBbv/bbv-config.scm 191 */
									long BgL_l1z00_482;

									BgL_l1z00_482 = STRING_LENGTH(((obj_t) BgL_ez00_80));
									if ((BgL_l1z00_482 == 5L))
										{	/* SawBbv/bbv-config.scm 191 */
											int BgL_arg1282z00_485;

											{	/* SawBbv/bbv-config.scm 191 */
												char *BgL_auxz00_1096;
												char *BgL_tmpz00_1093;

												BgL_auxz00_1096 =
													BSTRING_TO_STRING
													(BGl_string1070z00zzsaw_bbvzd2configzd2);
												BgL_tmpz00_1093 =
													BSTRING_TO_STRING(((obj_t) BgL_ez00_80));
												BgL_arg1282z00_485 =
													memcmp(BgL_tmpz00_1093, BgL_auxz00_1096,
													BgL_l1z00_482);
											}
											BgL_test1274z00_1088 =
												((long) (BgL_arg1282z00_485) == 0L);
										}
									else
										{	/* SawBbv/bbv-config.scm 191 */
											BgL_test1274z00_1088 = ((bool_t) 0);
										}
								}
								if (BgL_test1274z00_1088)
									{	/* SawBbv/bbv-config.scm 191 */
										BGl_za2bbvzd2profileza2zd2zzsaw_bbvzd2configzd2 = BFALSE;
									}
								else
									{	/* SawBbv/bbv-config.scm 191 */
										BGl_za2bbvzd2profileza2zd2zzsaw_bbvzd2configzd2 =
											BGl_errorz00zz__errorz00
											(BGl_string1115z00zzsaw_bbvzd2configzd2,
											BGl_string1078z00zzsaw_bbvzd2configzd2, BgL_ez00_80);
									}
							}
					}
				else
					{	/* SawBbv/bbv-config.scm 189 */
						BGl_za2bbvzd2profileza2zd2zzsaw_bbvzd2configzd2 = BFALSE;
					}
			}
			{	/* SawBbv/bbv-config.scm 199 */
				obj_t BgL_ez00_83;

				BgL_ez00_83 =
					BGl_getenvz00zz__osz00(BGl_string1116z00zzsaw_bbvzd2configzd2);
				if (STRINGP(BgL_ez00_83))
					{	/* SawBbv/bbv-config.scm 201 */
						long BgL_tmpz00_1105;

						{	/* SawBbv/bbv-config.scm 201 */
							char *BgL_tmpz00_1106;

							BgL_tmpz00_1106 = BSTRING_TO_STRING(BgL_ez00_83);
							BgL_tmpz00_1105 = BGL_STRTOL(BgL_tmpz00_1106, 0L, 10L);
						}
						BGl_za2maxzd2blockzd2mergezd2versionsza2zd2zzsaw_bbvzd2configzd2 =
							BINT(BgL_tmpz00_1105);
					}
				else
					{	/* SawBbv/bbv-config.scm 200 */
						BGl_za2maxzd2blockzd2mergezd2versionsza2zd2zzsaw_bbvzd2configzd2 =
							BINT(4L);
					}
			}
			BGl_za2maxzd2blockzd2limitza2z00zzsaw_bbvzd2configzd2 =
				BGl_za2maxzd2blockzd2mergezd2versionsza2zd2zzsaw_bbvzd2configzd2;
			BGl_za2typezd2callza2zd2zzsaw_bbvzd2configzd2 = BTRUE;
			BGl_za2typezd2loadiza2zd2zzsaw_bbvzd2configzd2 = BTRUE;
			return (BGl_za2typezd2loadgza2zd2zzsaw_bbvzd2configzd2 = BTRUE, BUNSPEC);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_bbvzd2configzd2(void)
	{
		{	/* SawBbv/bbv-config.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_bbvzd2configzd2(void)
	{
		{	/* SawBbv/bbv-config.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_bbvzd2configzd2(void)
	{
		{	/* SawBbv/bbv-config.scm 15 */
			return BUNSPEC;
		}

	}

#ifdef __cplusplus
}
#endif
