/*===========================================================================*/
/*   (SawBbv/bbv-cache.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawBbv/bbv-cache.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_SAW_BBVzd2CACHEzd2_TYPE_DEFINITIONS
#define BGL_BgL_SAW_BBVzd2CACHEzd2_TYPE_DEFINITIONS
#endif													// BGL_BgL_SAW_BBVzd2CACHEzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62startzd2bbvzd2cachez12z70zzsaw_bbvzd2cachezd2(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2zc3flza2zc3zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2zb2fxzd2safeza2z60zzsaw_bbvzd2cachezd2 =
		BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2stringzd2lengthza2zd2zzsaw_bbvzd2cachezd2 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2errorza2z00zzsaw_bbvzd2cachezd2 = BUNSPEC;
	extern obj_t BGl_getzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2z42zd2fxzf2ovza2z62zzsaw_bbvzd2cachezd2 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2stringzd2boundzd2checkza2z00zzsaw_bbvzd2cachezd2
		= BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2zd3flza2zd3zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2zc3zd3flza2z10zzsaw_bbvzd2cachezd2 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2cachezd2(void);
	BGL_EXPORTED_DEF obj_t BGl_za2z42zd2fxzf2wzd2ovza2zb0zzsaw_bbvzd2cachezd2 =
		BUNSPEC;
	static obj_t BGl_genericzd2initzd2zzsaw_bbvzd2cachezd2(void);
	BGL_EXPORTED_DEF obj_t BGl_za2ze3flza2ze3zzsaw_bbvzd2cachezd2 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zzsaw_bbvzd2cachezd2(void);
	BGL_EXPORTED_DEF obj_t BGl_za2longzd2ze3bintza2z31zzsaw_bbvzd2cachezd2 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2z42intzd2ze3longza2z73zzsaw_bbvzd2cachezd2 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_bbvzd2cachezd2(void);
	BGL_EXPORTED_DEF obj_t BGl_za2vectorzd2boundzd2checkza2z00zzsaw_bbvzd2cachezd2
		= BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2ze3zd3flza2z30zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2zc3fxza2zc3zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2zb2flza2zb2zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2z42za2fxzf2wzd2ovza2zc0zzsaw_bbvzd2cachezd2 =
		BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_stopzd2bbvzd2cachez12z12zzsaw_bbvzd2cachezd2(void);
	static obj_t BGl_z62stopzd2bbvzd2cachez12z70zzsaw_bbvzd2cachezd2(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za22zb2za2zb2zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2zd3fxza2zd3zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2zd2fxzd2safeza2z00zzsaw_bbvzd2cachezd2 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2zc3zd3fxza2z10zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za22zd2za2zd2zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2z42za2fxzf2ovza2z12zzsaw_bbvzd2cachezd2 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2intzd2ze3longza2z31zzsaw_bbvzd2cachezd2 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2ze3fxza2ze3zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2zd2flza2zd2zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	static bool_t BGl_za2typezd2normsza2zd2zzsaw_bbvzd2cachezd2;
	BGL_EXPORTED_DEF obj_t BGl_za2z42zb2fxzf2wzd2ovza2zd0zzsaw_bbvzd2cachezd2 =
		BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2cachezd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2cachezd2(void);
	BGL_EXPORTED_DEF obj_t BGl_za2za2fxza2za2zzsaw_bbvzd2cachezd2 = BUNSPEC;
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2cachezd2(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2cachezd2(void);
	BGL_EXPORTED_DEF obj_t BGl_za2z42zb2fxzf2ovza2z02zzsaw_bbvzd2cachezd2 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2bintzd2ze3longza2z31zzsaw_bbvzd2cachezd2 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2subfxza2z00zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2za2fxzd2safeza2z70zzsaw_bbvzd2cachezd2 =
		BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2ze3zd3fxza2z30zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2numberzf3za2zf3zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2zb2fxza2zb2zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_startzd2bbvzd2cachez12z12zzsaw_bbvzd2cachezd2(void);
	BGL_EXPORTED_DEF obj_t BGl_za2addfxza2z00zzsaw_bbvzd2cachezd2 = BUNSPEC;
	BGL_EXPORTED_DEF obj_t BGl_za2zd2fxza2zd2zzsaw_bbvzd2cachezd2 = BUNSPEC;
	static bool_t BGl_za2cachezd2startedzf3za2z21zzsaw_bbvzd2cachezd2;
	static obj_t __cnst[39];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stopzd2bbvzd2cachez12zd2envzc0zzsaw_bbvzd2cachezd2,
		BgL_bgl_za762stopza7d2bbvza7d21133za7,
		BGl_z62stopzd2bbvzd2cachez12z70zzsaw_bbvzd2cachezd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_startzd2bbvzd2cachez12zd2envzc0zzsaw_bbvzd2cachezd2,
		BgL_bgl_za762startza7d2bbvza7d1134za7,
		BGl_z62startzd2bbvzd2cachez12z70zzsaw_bbvzd2cachezd2, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1130z00zzsaw_bbvzd2cachezd2,
		BgL_bgl_string1130za700za7za7s1135za7, "saw_bbv-cache", 13);
	      DEFINE_STRING(BGl_string1131z00zzsaw_bbvzd2cachezd2,
		BgL_bgl_string1131za700za7za7s1136za7,
		"number? __error error $string-length $string-bound-check? $vector-bound-check? $long->bint $bint->long $int->long |2+| __r4_numbers_6_5 |2-| $+fl $-fl $=fl $>=fl $>fl $<=fl $<fl $*fx/w-ov $+fx/w-ov $-fx/w-ov $*fx/ov $+fx/ov $-fx/ov *fx-safe +fx-safe -fx-safe $addfx $subfx $*fx $+fx $-fx $=fx $>=fx $>fx $<=fx foreign $<fx ",
		323);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2zc3flza2zc3zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2zb2fxzd2safeza2z60zzsaw_bbvzd2cachezd2));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2stringzd2lengthza2zd2zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2errorza2z00zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2z42zd2fxzf2ovza2z62zzsaw_bbvzd2cachezd2));
		   
			 ADD_ROOT((void
				*) (&BGl_za2stringzd2boundzd2checkza2z00zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2zd3flza2zd3zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2zc3zd3flza2z10zzsaw_bbvzd2cachezd2));
		   
			 ADD_ROOT((void *) (&BGl_za2z42zd2fxzf2wzd2ovza2zb0zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2ze3flza2ze3zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2longzd2ze3bintza2z31zzsaw_bbvzd2cachezd2));
		   
			 ADD_ROOT((void *) (&BGl_za2z42intzd2ze3longza2z73zzsaw_bbvzd2cachezd2));
		   
			 ADD_ROOT((void
				*) (&BGl_za2vectorzd2boundzd2checkza2z00zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2ze3zd3flza2z30zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2zc3fxza2zc3zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2zb2flza2zb2zzsaw_bbvzd2cachezd2));
		   
			 ADD_ROOT((void *) (&BGl_za2z42za2fxzf2wzd2ovza2zc0zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za22zb2za2zb2zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2zd3fxza2zd3zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2zd2fxzd2safeza2z00zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2zc3zd3fxza2z10zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za22zd2za2zd2zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2z42za2fxzf2ovza2z12zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2intzd2ze3longza2z31zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2ze3fxza2ze3zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2zd2flza2zd2zzsaw_bbvzd2cachezd2));
		   
			 ADD_ROOT((void *) (&BGl_za2z42zb2fxzf2wzd2ovza2zd0zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2za2fxza2za2zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2z42zb2fxzf2ovza2z02zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2bintzd2ze3longza2z31zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2subfxza2z00zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2za2fxzd2safeza2z70zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2ze3zd3fxza2z30zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2numberzf3za2zf3zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2zb2fxza2zb2zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2addfxza2z00zzsaw_bbvzd2cachezd2));
		     ADD_ROOT((void *) (&BGl_za2zd2fxza2zd2zzsaw_bbvzd2cachezd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(long
		BgL_checksumz00_494, char *BgL_fromz00_495)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_bbvzd2cachezd2))
				{
					BGl_requirezd2initializa7ationz75zzsaw_bbvzd2cachezd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_bbvzd2cachezd2();
					BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2cachezd2();
					BGl_cnstzd2initzd2zzsaw_bbvzd2cachezd2();
					BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2cachezd2();
					return BGl_toplevelzd2initzd2zzsaw_bbvzd2cachezd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2cachezd2(void)
	{
		{	/* SawBbv/bbv-cache.scm 15 */
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_bbv-cache");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_bbv-cache");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_bbv-cache");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2cachezd2(void)
	{
		{	/* SawBbv/bbv-cache.scm 15 */
			{	/* SawBbv/bbv-cache.scm 15 */
				obj_t BgL_cportz00_483;

				{	/* SawBbv/bbv-cache.scm 15 */
					obj_t BgL_stringz00_490;

					BgL_stringz00_490 = BGl_string1131z00zzsaw_bbvzd2cachezd2;
					{	/* SawBbv/bbv-cache.scm 15 */
						obj_t BgL_startz00_491;

						BgL_startz00_491 = BINT(0L);
						{	/* SawBbv/bbv-cache.scm 15 */
							obj_t BgL_endz00_492;

							BgL_endz00_492 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_490)));
							{	/* SawBbv/bbv-cache.scm 15 */

								BgL_cportz00_483 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_490, BgL_startz00_491, BgL_endz00_492);
				}}}}
				{
					long BgL_iz00_484;

					BgL_iz00_484 = 38L;
				BgL_loopz00_485:
					if ((BgL_iz00_484 == -1L))
						{	/* SawBbv/bbv-cache.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* SawBbv/bbv-cache.scm 15 */
							{	/* SawBbv/bbv-cache.scm 15 */
								obj_t BgL_arg1132z00_486;

								{	/* SawBbv/bbv-cache.scm 15 */

									{	/* SawBbv/bbv-cache.scm 15 */
										obj_t BgL_locationz00_488;

										BgL_locationz00_488 = BBOOL(((bool_t) 0));
										{	/* SawBbv/bbv-cache.scm 15 */

											BgL_arg1132z00_486 =
												BGl_readz00zz__readerz00(BgL_cportz00_483,
												BgL_locationz00_488);
										}
									}
								}
								{	/* SawBbv/bbv-cache.scm 15 */
									int BgL_tmpz00_516;

									BgL_tmpz00_516 = (int) (BgL_iz00_484);
									CNST_TABLE_SET(BgL_tmpz00_516, BgL_arg1132z00_486);
							}}
							{	/* SawBbv/bbv-cache.scm 15 */
								int BgL_auxz00_489;

								BgL_auxz00_489 = (int) ((BgL_iz00_484 - 1L));
								{
									long BgL_iz00_521;

									BgL_iz00_521 = (long) (BgL_auxz00_489);
									BgL_iz00_484 = BgL_iz00_521;
									goto BgL_loopz00_485;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2cachezd2(void)
	{
		{	/* SawBbv/bbv-cache.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2cachezd2(void)
	{
		{	/* SawBbv/bbv-cache.scm 15 */
			BGl_za2cachezd2startedzf3za2z21zzsaw_bbvzd2cachezd2 = ((bool_t) 0);
			BGl_za2zc3fxza2zc3zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zc3zd3fxza2z10zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2ze3zd3fxza2z30zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2ze3fxza2ze3zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zd3fxza2zd3zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zd2fxza2zd2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zb2fxza2zb2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2za2fxza2za2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2subfxza2z00zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2addfxza2z00zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zd2fxzd2safeza2z00zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zb2fxzd2safeza2z60zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2za2fxzd2safeza2z70zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2z42zd2fxzf2ovza2z62zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2z42zb2fxzf2ovza2z02zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2z42za2fxzf2ovza2z12zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2z42zd2fxzf2wzd2ovza2zb0zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2z42zb2fxzf2wzd2ovza2zd0zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2z42za2fxzf2wzd2ovza2zc0zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zc3flza2zc3zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zc3zd3flza2z10zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2ze3zd3flza2z30zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2ze3flza2ze3zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zd3flza2zd3zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zd2flza2zd2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zb2flza2zb2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za22zd2za2zd2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za22zb2za2zb2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2intzd2ze3longza2z31zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2bintzd2ze3longza2z31zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2z42intzd2ze3longza2z73zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2longzd2ze3bintza2z31zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2vectorzd2boundzd2checkza2z00zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2stringzd2boundzd2checkza2z00zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2stringzd2lengthza2zd2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2errorza2z00zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2numberzf3za2zf3zzsaw_bbvzd2cachezd2 = BFALSE;
			return (BGl_za2typezd2normsza2zd2zzsaw_bbvzd2cachezd2 =
				((bool_t) 0), BUNSPEC);
		}

	}



/* start-bbv-cache! */
	BGL_EXPORTED_DEF obj_t BGl_startzd2bbvzd2cachez12z12zzsaw_bbvzd2cachezd2(void)
	{
		{	/* SawBbv/bbv-cache.scm 96 */
			if (BGl_za2cachezd2startedzf3za2z21zzsaw_bbvzd2cachezd2)
				{	/* SawBbv/bbv-cache.scm 97 */
					return BFALSE;
				}
			else
				{	/* SawBbv/bbv-cache.scm 97 */
					BGl_za2cachezd2startedzf3za2z21zzsaw_bbvzd2cachezd2 = ((bool_t) 1);
					BGl_za2zc3fxza2zc3zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(0),
						CNST_TABLE_REF(1));
					BGl_za2zc3zd3fxza2z10zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(2),
						CNST_TABLE_REF(1));
					BGl_za2ze3fxza2ze3zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(3),
						CNST_TABLE_REF(1));
					BGl_za2ze3zd3fxza2z30zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(4),
						CNST_TABLE_REF(1));
					BGl_za2zd3fxza2zd3zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(5),
						CNST_TABLE_REF(1));
					BGl_za2zd2fxza2zd2zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(6),
						CNST_TABLE_REF(1));
					BGl_za2zb2fxza2zb2zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(7),
						CNST_TABLE_REF(1));
					BGl_za2za2fxza2za2zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(8),
						CNST_TABLE_REF(1));
					BGl_za2subfxza2z00zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(9),
						CNST_TABLE_REF(1));
					BGl_za2addfxza2z00zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(10),
						CNST_TABLE_REF(1));
					BGl_za2zd2fxzd2safeza2z00zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(11),
						CNST_TABLE_REF(1));
					BGl_za2zb2fxzd2safeza2z60zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(12),
						CNST_TABLE_REF(1));
					BGl_za2za2fxzd2safeza2z70zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(13),
						CNST_TABLE_REF(1));
					BGl_za2z42zd2fxzf2ovza2z62zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(14),
						CNST_TABLE_REF(1));
					BGl_za2z42zb2fxzf2ovza2z02zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(15),
						CNST_TABLE_REF(1));
					BGl_za2z42za2fxzf2ovza2z12zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(16),
						CNST_TABLE_REF(1));
					BGl_za2z42zd2fxzf2wzd2ovza2zb0zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(17),
						CNST_TABLE_REF(1));
					BGl_za2z42zb2fxzf2wzd2ovza2zd0zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(18),
						CNST_TABLE_REF(1));
					BGl_za2z42za2fxzf2wzd2ovza2zc0zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(19),
						CNST_TABLE_REF(1));
					BGl_za2zc3flza2zc3zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(20),
						CNST_TABLE_REF(1));
					BGl_za2zc3zd3flza2z10zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(21),
						CNST_TABLE_REF(1));
					BGl_za2ze3flza2ze3zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(22),
						CNST_TABLE_REF(1));
					BGl_za2ze3zd3flza2z30zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(23),
						CNST_TABLE_REF(1));
					BGl_za2zd3flza2zd3zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(24),
						CNST_TABLE_REF(1));
					BGl_za2zd2flza2zd2zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(25),
						CNST_TABLE_REF(1));
					BGl_za2zb2flza2zb2zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(26),
						CNST_TABLE_REF(1));
					BGl_za22zd2za2zd2zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(27),
						CNST_TABLE_REF(28));
					BGl_za22zb2za2zb2zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(29),
						CNST_TABLE_REF(28));
					BGl_za2intzd2ze3longza2z31zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(30),
						CNST_TABLE_REF(1));
					BGl_za2bintzd2ze3longza2z31zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(31),
						CNST_TABLE_REF(1));
					BGl_za2z42intzd2ze3longza2z73zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(30),
						CNST_TABLE_REF(1));
					BGl_za2longzd2ze3bintza2z31zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(32),
						CNST_TABLE_REF(1));
					BGl_za2vectorzd2boundzd2checkza2z00zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(33),
						CNST_TABLE_REF(1));
					BGl_za2stringzd2boundzd2checkza2z00zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(34),
						CNST_TABLE_REF(1));
					BGl_za2stringzd2lengthza2zd2zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(35),
						CNST_TABLE_REF(1));
					BGl_za2errorza2z00zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(36),
						CNST_TABLE_REF(37));
					return (BGl_za2numberzf3za2zf3zzsaw_bbvzd2cachezd2 =
						BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(38),
							CNST_TABLE_REF(28)), BUNSPEC);
				}
		}

	}



/* &start-bbv-cache! */
	obj_t BGl_z62startzd2bbvzd2cachez12z70zzsaw_bbvzd2cachezd2(obj_t
		BgL_envz00_481)
	{
		{	/* SawBbv/bbv-cache.scm 96 */
			return BGl_startzd2bbvzd2cachez12z12zzsaw_bbvzd2cachezd2();
		}

	}



/* stop-bbv-cache! */
	BGL_EXPORTED_DEF obj_t BGl_stopzd2bbvzd2cachez12z12zzsaw_bbvzd2cachezd2(void)
	{
		{	/* SawBbv/bbv-cache.scm 140 */
			BGl_za2cachezd2startedzf3za2z21zzsaw_bbvzd2cachezd2 = ((bool_t) 0);
			BGl_za2zc3fxza2zc3zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zc3zd3fxza2z10zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2ze3fxza2ze3zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2ze3zd3fxza2z30zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zd3fxza2zd3zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zd2fxza2zd2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zb2fxza2zb2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2za2fxza2za2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2subfxza2z00zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2addfxza2z00zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zd2fxzd2safeza2z00zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zb2fxzd2safeza2z60zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2za2fxzd2safeza2z70zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2z42zd2fxzf2ovza2z62zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2z42zb2fxzf2ovza2z02zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2z42za2fxzf2ovza2z12zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zc3flza2zc3zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zc3zd3flza2z10zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2ze3flza2ze3zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2ze3zd3flza2z30zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zd3flza2zd3zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zd2flza2zd2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2zb2flza2zb2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za22zd2za2zd2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za22zb2za2zb2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2intzd2ze3longza2z31zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2bintzd2ze3longza2z31zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2z42intzd2ze3longza2z73zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2longzd2ze3bintza2z31zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2vectorzd2boundzd2checkza2z00zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2stringzd2boundzd2checkza2z00zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2stringzd2lengthza2zd2zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2errorza2z00zzsaw_bbvzd2cachezd2 = BFALSE;
			BGl_za2numberzf3za2zf3zzsaw_bbvzd2cachezd2 = BFALSE;
			return (BGl_za2typezd2normsza2zd2zzsaw_bbvzd2cachezd2 =
				((bool_t) 0), BUNSPEC);
		}

	}



/* &stop-bbv-cache! */
	obj_t BGl_z62stopzd2bbvzd2cachez12z70zzsaw_bbvzd2cachezd2(obj_t
		BgL_envz00_482)
	{
		{	/* SawBbv/bbv-cache.scm 140 */
			return BGl_stopzd2bbvzd2cachez12z12zzsaw_bbvzd2cachezd2();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_bbvzd2cachezd2(void)
	{
		{	/* SawBbv/bbv-cache.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_bbvzd2cachezd2(void)
	{
		{	/* SawBbv/bbv-cache.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_bbvzd2cachezd2(void)
	{
		{	/* SawBbv/bbv-cache.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2cachezd2(void)
	{
		{	/* SawBbv/bbv-cache.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1130z00zzsaw_bbvzd2cachezd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1130z00zzsaw_bbvzd2cachezd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1130z00zzsaw_bbvzd2cachezd2));
			return
				BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1130z00zzsaw_bbvzd2cachezd2));
		}

	}

#ifdef __cplusplus
}
#endif
