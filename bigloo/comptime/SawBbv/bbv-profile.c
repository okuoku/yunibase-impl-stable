/*===========================================================================*/
/*   (SawBbv/bbv-profile.scm)                                                */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawBbv/bbv-profile.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_SAW_BBVzd2PROFILEzd2_TYPE_DEFINITIONS
#define BGL_BgL_SAW_BBVzd2PROFILEzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                    *BgL_rtl_pragmaz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_rtl_regzf2razf2_bgl
	{
		int BgL_numz00;
		obj_t BgL_colorz00;
		obj_t BgL_coalescez00;
		int BgL_occurrencesz00;
		obj_t BgL_interferez00;
		obj_t BgL_interfere2z00;
	}                      *BgL_rtl_regzf2razf2_bglt;

	typedef struct BgL_regsetz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_lengthz00;
		int BgL_msiza7eza7;
		obj_t BgL_regvz00;
		obj_t BgL_reglz00;
		obj_t BgL_stringz00;
	}                *BgL_regsetz00_bglt;

	typedef struct BgL_rtl_inszf2bbvzf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_outz00;
		obj_t BgL_inz00;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		obj_t BgL_z52hashz52;
	}                       *BgL_rtl_inszf2bbvzf2_bglt;

	typedef struct BgL_blockvz00_bgl
	{
		obj_t BgL_versionsz00;
		obj_t BgL_genericz00;
		long BgL_z52markz52;
		obj_t BgL_mergez00;
	}                *BgL_blockvz00_bglt;

	typedef struct BgL_blocksz00_bgl
	{
		long BgL_z52markz52;
		obj_t BgL_z52hashz52;
		obj_t BgL_z52blacklistz52;
		struct BgL_bbvzd2ctxzd2_bgl *BgL_ctxz00;
		struct BgL_blockz00_bgl *BgL_parentz00;
		long BgL_gccntz00;
		long BgL_gcmarkz00;
		obj_t BgL_mblockz00;
		obj_t BgL_creatorz00;
		obj_t BgL_mergesz00;
		bool_t BgL_asleepz00;
	}                *BgL_blocksz00_bglt;

	typedef struct BgL_bbvzd2ctxzd2_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_idz00;
		obj_t BgL_entriesz00;
	}                   *BgL_bbvzd2ctxzd2_bglt;


#endif													// BGL_BgL_SAW_BBVzd2PROFILEzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2profilezd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	static obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2profilezd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2profilezd2(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	extern obj_t BGl_rtl_pragmaz00zzsaw_defsz00;
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_bbvzd2profilezd2 =
		BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2profilezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2profilezd2(int, int, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2profilezd2(BgL_typez00_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, int, obj_t, obj_t, int, obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2profilezd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2profilezd2(void);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2profilezd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2profilezd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2profilezd2(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_bbvzd2profilezd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2profilezd2(obj_t,
		obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2profilezd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_objectzd2initzd2zzsaw_bbvzd2profilezd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt, obj_t);
	extern obj_t BGl_regsetz00zzsaw_regsetz00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	static obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2profilezd2(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2profilezd2(obj_t,
		obj_t);
	static obj_t BGl_appendzd221011zd2zzsaw_bbvzd2profilezd2(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_bbvzd2profilezd2(void);
	static obj_t BGl_bbsetzd2conszd2zzsaw_bbvzd2profilezd2(BgL_blockz00_bglt,
		obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2profilezd2(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2profilezd2(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2profilezd2(void);
	static BgL_typez00_bglt
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2profilezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2profilezd2(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2profilezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2profilezd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2profilezd2(obj_t,
		obj_t);
	static obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2profilezd2(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2profilezd2(BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2profilezd2(BgL_regsetz00_bglt,
		obj_t);
	extern obj_t BGl_getzd2bbzd2markz00zzsaw_bbvzd2typeszd2(void);
	static obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2profilezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2profilezd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2debugzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2specializa7ez75(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2profilezd2(obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2profilezd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	extern obj_t BGl_za2bbvzd2profileza2zd2zzsaw_bbvzd2configzd2;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt, obj_t);
	static obj_t BGl_z62profilez12z70zzsaw_bbvzd2profilezd2(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2profilezd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2profilezd2(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2profilezd2(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2profilezd2(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt, int);
	static obj_t BGl_makezd2emptyzd2bbsetz00zzsaw_bbvzd2profilezd2(void);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_regsetzf3zf3zzsaw_bbvzd2profilezd2(obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2profilezd2(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2profilezd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2profilezd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2profilezd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2profilezd2(BgL_regsetz00_bglt);
	static obj_t
		BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2profilezd2(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2profilezd2(void);
	static obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2profilezd2(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2profilezd2(obj_t, obj_t);
	extern obj_t BGl_rtl_regzf2razf2zzsaw_regsetz00;
	static obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2profilezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt, BgL_typez00_bglt);
	extern obj_t BGl_assertzd2blockszd2zzsaw_bbvzd2debugzd2(BgL_blockz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2profilezd2(BgL_regsetz00_bglt);
	static obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2profilezd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2profilezd2(BgL_regsetz00_bglt);
	static obj_t
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2profilezd2(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2profilezd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt);
	extern obj_t BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2;
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2profilezd2(BgL_regsetz00_bglt,
		int);
	static obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2profilezd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_profilez12z12zzsaw_bbvzd2profilezd2(BgL_blockz00_bglt);
	static obj_t __cnst[3];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2namezd2envzf2zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71924za7,
		BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2nilzd2envz00zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762regsetza7d2nilza71925za7,
		BGl_z62regsetzd2nilzb0zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2setz12zd2envz32zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71926za7,
		BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2profilezd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2envzf2zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71927za7,
		BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_profilez12zd2envzc0zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762profileza712za7701928za7,
		BGl_z62profilez12z70zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2setz12zd2envzc1zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71929za7,
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2profilezd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2setz12zd2envz32zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71930za7,
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2profilezd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2envzf2zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71931za7,
		BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2setz12zd2envz32zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71932za7,
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2profilezd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2regvzd2envz00zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762regsetza7d2regv1933z00,
		BGl_z62regsetzd2regvzb0zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2setz12zd2envzc0zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762regsetza7d2leng1934z00,
		BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2msiza7ezd2envza7zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762regsetza7d2msiza71935za7,
		BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2envzf2zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71936za7,
		BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2setz12zd2envz32zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71937za7,
		BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2profilezd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2envzf2zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71938za7,
		BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1918z00zzsaw_bbvzd2profilezd2,
		BgL_bgl_string1918za700za7za7s1939za7, "", 0);
	      DEFINE_STRING(BGl_string1919z00zzsaw_bbvzd2profilezd2,
		BgL_bgl_string1919za700za7za7s1940za7, "BBV_ENTER(~a)", 13);
	      DEFINE_STRING(BGl_string1920z00zzsaw_bbvzd2profilezd2,
		BgL_bgl_string1920za700za7za7s1941za7, "before profile!", 15);
	      DEFINE_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2,
		BgL_bgl_string1921za700za7za7s1942za7, "saw_bbv-profile", 15);
	      DEFINE_STRING(BGl_string1922z00zzsaw_bbvzd2profilezd2,
		BgL_bgl_string1922za700za7za7s1943za7, "#() bigloo-c bbset ", 19);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2nilzd2envzf2zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71944za7,
		BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2envzf2zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71945za7,
		BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2numzd2envzf2zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71946za7,
		BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2setz12zd2envzc0zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762regsetza7d2stri1947z00,
		BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2setz12zd2envz32zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71948za7,
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2profilezd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2envz01zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71949za7,
		BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2envz00zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762regsetza7d2stri1950z00,
		BGl_z62regsetzd2stringzb0zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2setz12zd2envz32zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71951za7,
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2profilezd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2rtl_regzf2razd2envzf2zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762makeza7d2rtl_re1952z00,
		BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razf3zd2envzd3zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71953za7,
		BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2envz00zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762regsetza7d2leng1954z00,
		BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2envzf2zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71955za7,
		BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2envzf2zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71956za7,
		BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2hardwarezd2envzf2zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71957za7,
		BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2regsetzd2envz00zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762makeza7d2regset1958z00,
		BGl_z62makezd2regsetzb0zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzf3zd2envz21zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762regsetza7f3za791za71959z00,
		BGl_z62regsetzf3z91zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2keyzd2envzf2zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71960za7,
		BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2setz12zd2envz32zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762rtl_regza7f2raza71961za7,
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2profilezd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2reglzd2envz00zzsaw_bbvzd2profilezd2,
		BgL_bgl_za762regsetza7d2regl1962z00,
		BGl_z62regsetzd2reglzb0zzsaw_bbvzd2profilezd2, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_bbvzd2profilezd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_bbvzd2profilezd2(long
		BgL_checksumz00_2947, char *BgL_fromz00_2948)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_bbvzd2profilezd2))
				{
					BGl_requirezd2initializa7ationz75zzsaw_bbvzd2profilezd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_bbvzd2profilezd2();
					BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2profilezd2();
					BGl_cnstzd2initzd2zzsaw_bbvzd2profilezd2();
					BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2profilezd2();
					return BGl_toplevelzd2initzd2zzsaw_bbvzd2profilezd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_bbvzd2profilezd2(void)
	{
		{	/* SawBbv/bbv-profile.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_bbv-profile");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"saw_bbv-profile");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_bbv-profile");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_bbv-profile");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_bbv-profile");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "saw_bbv-profile");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_bbv-profile");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_bbv-profile");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_bbv-profile");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_bbv-profile");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "saw_bbv-profile");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_bbvzd2profilezd2(void)
	{
		{	/* SawBbv/bbv-profile.scm 15 */
			{	/* SawBbv/bbv-profile.scm 15 */
				obj_t BgL_cportz00_2897;

				{	/* SawBbv/bbv-profile.scm 15 */
					obj_t BgL_stringz00_2904;

					BgL_stringz00_2904 = BGl_string1922z00zzsaw_bbvzd2profilezd2;
					{	/* SawBbv/bbv-profile.scm 15 */
						obj_t BgL_startz00_2905;

						BgL_startz00_2905 = BINT(0L);
						{	/* SawBbv/bbv-profile.scm 15 */
							obj_t BgL_endz00_2906;

							BgL_endz00_2906 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2904)));
							{	/* SawBbv/bbv-profile.scm 15 */

								BgL_cportz00_2897 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2904, BgL_startz00_2905, BgL_endz00_2906);
				}}}}
				{
					long BgL_iz00_2898;

					BgL_iz00_2898 = 2L;
				BgL_loopz00_2899:
					if ((BgL_iz00_2898 == -1L))
						{	/* SawBbv/bbv-profile.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* SawBbv/bbv-profile.scm 15 */
							{	/* SawBbv/bbv-profile.scm 15 */
								obj_t BgL_arg1923z00_2900;

								{	/* SawBbv/bbv-profile.scm 15 */

									{	/* SawBbv/bbv-profile.scm 15 */
										obj_t BgL_locationz00_2902;

										BgL_locationz00_2902 = BBOOL(((bool_t) 0));
										{	/* SawBbv/bbv-profile.scm 15 */

											BgL_arg1923z00_2900 =
												BGl_readz00zz__readerz00(BgL_cportz00_2897,
												BgL_locationz00_2902);
										}
									}
								}
								{	/* SawBbv/bbv-profile.scm 15 */
									int BgL_tmpz00_2977;

									BgL_tmpz00_2977 = (int) (BgL_iz00_2898);
									CNST_TABLE_SET(BgL_tmpz00_2977, BgL_arg1923z00_2900);
							}}
							{	/* SawBbv/bbv-profile.scm 15 */
								int BgL_auxz00_2903;

								BgL_auxz00_2903 = (int) ((BgL_iz00_2898 - 1L));
								{
									long BgL_iz00_2982;

									BgL_iz00_2982 = (long) (BgL_auxz00_2903);
									BgL_iz00_2898 = BgL_iz00_2982;
									goto BgL_loopz00_2899;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_bbvzd2profilezd2(void)
	{
		{	/* SawBbv/bbv-profile.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_bbvzd2profilezd2(void)
	{
		{	/* SawBbv/bbv-profile.scm 15 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzsaw_bbvzd2profilezd2(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_2160;

				BgL_headz00_2160 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2161;
					obj_t BgL_tailz00_2162;

					BgL_prevz00_2161 = BgL_headz00_2160;
					BgL_tailz00_2162 = BgL_l1z00_1;
				BgL_loopz00_2163:
					if (PAIRP(BgL_tailz00_2162))
						{
							obj_t BgL_newzd2prevzd2_2165;

							BgL_newzd2prevzd2_2165 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2162), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2161, BgL_newzd2prevzd2_2165);
							{
								obj_t BgL_tailz00_2992;
								obj_t BgL_prevz00_2991;

								BgL_prevz00_2991 = BgL_newzd2prevzd2_2165;
								BgL_tailz00_2992 = CDR(BgL_tailz00_2162);
								BgL_tailz00_2162 = BgL_tailz00_2992;
								BgL_prevz00_2161 = BgL_prevz00_2991;
								goto BgL_loopz00_2163;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_2160);
				}
			}
		}

	}



/* make-rtl_reg/ra */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2profilezd2(BgL_typez00_bglt
		BgL_type1179z00_17, obj_t BgL_var1180z00_18, obj_t BgL_onexprzf31181zf3_19,
		obj_t BgL_name1182z00_20, obj_t BgL_key1183z00_21,
		obj_t BgL_hardware1184z00_22, int BgL_num1185z00_23,
		obj_t BgL_color1186z00_24, obj_t BgL_coalesce1187z00_25,
		int BgL_occurrences1188z00_26, obj_t BgL_interfere1189z00_27,
		obj_t BgL_interfere21190z00_28)
	{
		{	/* SawMill/regset.sch 55 */
			{	/* SawMill/regset.sch 55 */
				BgL_rtl_regz00_bglt BgL_new1166z00_2908;

				{	/* SawMill/regset.sch 55 */
					BgL_rtl_regz00_bglt BgL_tmp1164z00_2909;
					BgL_rtl_regzf2razf2_bglt BgL_wide1165z00_2910;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_2995;

						{	/* SawMill/regset.sch 55 */
							BgL_rtl_regz00_bglt BgL_new1163z00_2911;

							BgL_new1163z00_2911 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/regset.sch 55 */
								long BgL_arg1472z00_2912;

								BgL_arg1472z00_2912 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1163z00_2911),
									BgL_arg1472z00_2912);
							}
							{	/* SawMill/regset.sch 55 */
								BgL_objectz00_bglt BgL_tmpz00_3000;

								BgL_tmpz00_3000 = ((BgL_objectz00_bglt) BgL_new1163z00_2911);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3000, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1163z00_2911);
							BgL_auxz00_2995 = BgL_new1163z00_2911;
						}
						BgL_tmp1164z00_2909 = ((BgL_rtl_regz00_bglt) BgL_auxz00_2995);
					}
					BgL_wide1165z00_2910 =
						((BgL_rtl_regzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regzf2razf2_bgl))));
					{	/* SawMill/regset.sch 55 */
						obj_t BgL_auxz00_3008;
						BgL_objectz00_bglt BgL_tmpz00_3006;

						BgL_auxz00_3008 = ((obj_t) BgL_wide1165z00_2910);
						BgL_tmpz00_3006 = ((BgL_objectz00_bglt) BgL_tmp1164z00_2909);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3006, BgL_auxz00_3008);
					}
					((BgL_objectz00_bglt) BgL_tmp1164z00_2909);
					{	/* SawMill/regset.sch 55 */
						long BgL_arg1454z00_2913;

						BgL_arg1454z00_2913 =
							BGL_CLASS_NUM(BGl_rtl_regzf2razf2zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1164z00_2909), BgL_arg1454z00_2913);
					}
					BgL_new1166z00_2908 = ((BgL_rtl_regz00_bglt) BgL_tmp1164z00_2909);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1166z00_2908)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1179z00_17), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_2908)))->BgL_varz00) =
					((obj_t) BgL_var1180z00_18), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_2908)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31181zf3_19), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_2908)))->BgL_namez00) =
					((obj_t) BgL_name1182z00_20), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_2908)))->BgL_keyz00) =
					((obj_t) BgL_key1183z00_21), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_2908)))->BgL_debugnamez00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1166z00_2908)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1184z00_22), BUNSPEC);
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3030;

					{
						obj_t BgL_auxz00_3031;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3032;

							BgL_tmpz00_3032 = ((BgL_objectz00_bglt) BgL_new1166z00_2908);
							BgL_auxz00_3031 = BGL_OBJECT_WIDENING(BgL_tmpz00_3032);
						}
						BgL_auxz00_3030 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3031);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3030))->BgL_numz00) =
						((int) BgL_num1185z00_23), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3037;

					{
						obj_t BgL_auxz00_3038;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3039;

							BgL_tmpz00_3039 = ((BgL_objectz00_bglt) BgL_new1166z00_2908);
							BgL_auxz00_3038 = BGL_OBJECT_WIDENING(BgL_tmpz00_3039);
						}
						BgL_auxz00_3037 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3038);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3037))->
							BgL_colorz00) = ((obj_t) BgL_color1186z00_24), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3044;

					{
						obj_t BgL_auxz00_3045;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3046;

							BgL_tmpz00_3046 = ((BgL_objectz00_bglt) BgL_new1166z00_2908);
							BgL_auxz00_3045 = BGL_OBJECT_WIDENING(BgL_tmpz00_3046);
						}
						BgL_auxz00_3044 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3045);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3044))->
							BgL_coalescez00) = ((obj_t) BgL_coalesce1187z00_25), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3051;

					{
						obj_t BgL_auxz00_3052;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3053;

							BgL_tmpz00_3053 = ((BgL_objectz00_bglt) BgL_new1166z00_2908);
							BgL_auxz00_3052 = BGL_OBJECT_WIDENING(BgL_tmpz00_3053);
						}
						BgL_auxz00_3051 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3052);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3051))->
							BgL_occurrencesz00) = ((int) BgL_occurrences1188z00_26), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3058;

					{
						obj_t BgL_auxz00_3059;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3060;

							BgL_tmpz00_3060 = ((BgL_objectz00_bglt) BgL_new1166z00_2908);
							BgL_auxz00_3059 = BGL_OBJECT_WIDENING(BgL_tmpz00_3060);
						}
						BgL_auxz00_3058 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3059);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3058))->
							BgL_interferez00) = ((obj_t) BgL_interfere1189z00_27), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3065;

					{
						obj_t BgL_auxz00_3066;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3067;

							BgL_tmpz00_3067 = ((BgL_objectz00_bglt) BgL_new1166z00_2908);
							BgL_auxz00_3066 = BGL_OBJECT_WIDENING(BgL_tmpz00_3067);
						}
						BgL_auxz00_3065 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3066);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3065))->
							BgL_interfere2z00) = ((obj_t) BgL_interfere21190z00_28), BUNSPEC);
				}
				return BgL_new1166z00_2908;
			}
		}

	}



/* &make-rtl_reg/ra */
	BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzf2raz42zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2803, obj_t BgL_type1179z00_2804, obj_t BgL_var1180z00_2805,
		obj_t BgL_onexprzf31181zf3_2806, obj_t BgL_name1182z00_2807,
		obj_t BgL_key1183z00_2808, obj_t BgL_hardware1184z00_2809,
		obj_t BgL_num1185z00_2810, obj_t BgL_color1186z00_2811,
		obj_t BgL_coalesce1187z00_2812, obj_t BgL_occurrences1188z00_2813,
		obj_t BgL_interfere1189z00_2814, obj_t BgL_interfere21190z00_2815)
	{
		{	/* SawMill/regset.sch 55 */
			return
				BGl_makezd2rtl_regzf2raz20zzsaw_bbvzd2profilezd2(
				((BgL_typez00_bglt) BgL_type1179z00_2804), BgL_var1180z00_2805,
				BgL_onexprzf31181zf3_2806, BgL_name1182z00_2807, BgL_key1183z00_2808,
				BgL_hardware1184z00_2809, CINT(BgL_num1185z00_2810),
				BgL_color1186z00_2811, BgL_coalesce1187z00_2812,
				CINT(BgL_occurrences1188z00_2813), BgL_interfere1189z00_2814,
				BgL_interfere21190z00_2815);
		}

	}



/* rtl_reg/ra? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_regzf2razf3z01zzsaw_bbvzd2profilezd2(obj_t
		BgL_objz00_29)
	{
		{	/* SawMill/regset.sch 56 */
			{	/* SawMill/regset.sch 56 */
				obj_t BgL_classz00_2914;

				BgL_classz00_2914 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_29))
					{	/* SawMill/regset.sch 56 */
						BgL_objectz00_bglt BgL_arg1807z00_2915;

						BgL_arg1807z00_2915 = (BgL_objectz00_bglt) (BgL_objz00_29);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 56 */
								long BgL_idxz00_2916;

								BgL_idxz00_2916 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2915);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2916 + 2L)) == BgL_classz00_2914);
							}
						else
							{	/* SawMill/regset.sch 56 */
								bool_t BgL_res1914z00_2919;

								{	/* SawMill/regset.sch 56 */
									obj_t BgL_oclassz00_2920;

									{	/* SawMill/regset.sch 56 */
										obj_t BgL_arg1815z00_2921;
										long BgL_arg1816z00_2922;

										BgL_arg1815z00_2921 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 56 */
											long BgL_arg1817z00_2923;

											BgL_arg1817z00_2923 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2915);
											BgL_arg1816z00_2922 = (BgL_arg1817z00_2923 - OBJECT_TYPE);
										}
										BgL_oclassz00_2920 =
											VECTOR_REF(BgL_arg1815z00_2921, BgL_arg1816z00_2922);
									}
									{	/* SawMill/regset.sch 56 */
										bool_t BgL__ortest_1115z00_2924;

										BgL__ortest_1115z00_2924 =
											(BgL_classz00_2914 == BgL_oclassz00_2920);
										if (BgL__ortest_1115z00_2924)
											{	/* SawMill/regset.sch 56 */
												BgL_res1914z00_2919 = BgL__ortest_1115z00_2924;
											}
										else
											{	/* SawMill/regset.sch 56 */
												long BgL_odepthz00_2925;

												{	/* SawMill/regset.sch 56 */
													obj_t BgL_arg1804z00_2926;

													BgL_arg1804z00_2926 = (BgL_oclassz00_2920);
													BgL_odepthz00_2925 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2926);
												}
												if ((2L < BgL_odepthz00_2925))
													{	/* SawMill/regset.sch 56 */
														obj_t BgL_arg1802z00_2927;

														{	/* SawMill/regset.sch 56 */
															obj_t BgL_arg1803z00_2928;

															BgL_arg1803z00_2928 = (BgL_oclassz00_2920);
															BgL_arg1802z00_2927 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2928,
																2L);
														}
														BgL_res1914z00_2919 =
															(BgL_arg1802z00_2927 == BgL_classz00_2914);
													}
												else
													{	/* SawMill/regset.sch 56 */
														BgL_res1914z00_2919 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1914z00_2919;
							}
					}
				else
					{	/* SawMill/regset.sch 56 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_reg/ra? */
	obj_t BGl_z62rtl_regzf2razf3z63zzsaw_bbvzd2profilezd2(obj_t BgL_envz00_2816,
		obj_t BgL_objz00_2817)
	{
		{	/* SawMill/regset.sch 56 */
			return
				BBOOL(BGl_rtl_regzf2razf3z01zzsaw_bbvzd2profilezd2(BgL_objz00_2817));
		}

	}



/* rtl_reg/ra-nil */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2profilezd2(void)
	{
		{	/* SawMill/regset.sch 57 */
			{	/* SawMill/regset.sch 57 */
				obj_t BgL_classz00_2695;

				BgL_classz00_2695 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				{	/* SawMill/regset.sch 57 */
					obj_t BgL__ortest_1117z00_2696;

					BgL__ortest_1117z00_2696 = BGL_CLASS_NIL(BgL_classz00_2695);
					if (CBOOL(BgL__ortest_1117z00_2696))
						{	/* SawMill/regset.sch 57 */
							return ((BgL_rtl_regz00_bglt) BgL__ortest_1117z00_2696);
						}
					else
						{	/* SawMill/regset.sch 57 */
							return
								((BgL_rtl_regz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2695));
						}
				}
			}
		}

	}



/* &rtl_reg/ra-nil */
	BgL_rtl_regz00_bglt BGl_z62rtl_regzf2razd2nilz42zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2818)
	{
		{	/* SawMill/regset.sch 57 */
			return BGl_rtl_regzf2razd2nilz20zzsaw_bbvzd2profilezd2();
		}

	}



/* rtl_reg/ra-interfere2 */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt
		BgL_oz00_30)
	{
		{	/* SawMill/regset.sch 58 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3107;

				{
					obj_t BgL_auxz00_3108;

					{	/* SawMill/regset.sch 58 */
						BgL_objectz00_bglt BgL_tmpz00_3109;

						BgL_tmpz00_3109 = ((BgL_objectz00_bglt) BgL_oz00_30);
						BgL_auxz00_3108 = BGL_OBJECT_WIDENING(BgL_tmpz00_3109);
					}
					BgL_auxz00_3107 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3108);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3107))->
					BgL_interfere2z00);
			}
		}

	}



/* &rtl_reg/ra-interfere2 */
	obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2819, obj_t BgL_oz00_2820)
	{
		{	/* SawMill/regset.sch 58 */
			return
				BGl_rtl_regzf2razd2interfere2z20zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2820));
		}

	}



/* rtl_reg/ra-interfere2-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt BgL_oz00_31, obj_t BgL_vz00_32)
	{
		{	/* SawMill/regset.sch 59 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3116;

				{
					obj_t BgL_auxz00_3117;

					{	/* SawMill/regset.sch 59 */
						BgL_objectz00_bglt BgL_tmpz00_3118;

						BgL_tmpz00_3118 = ((BgL_objectz00_bglt) BgL_oz00_31);
						BgL_auxz00_3117 = BGL_OBJECT_WIDENING(BgL_tmpz00_3118);
					}
					BgL_auxz00_3116 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3117);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3116))->
						BgL_interfere2z00) = ((obj_t) BgL_vz00_32), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere2-set! */
	obj_t BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2821, obj_t BgL_oz00_2822, obj_t BgL_vz00_2823)
	{
		{	/* SawMill/regset.sch 59 */
			return
				BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2822), BgL_vz00_2823);
		}

	}



/* rtl_reg/ra-interfere */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt
		BgL_oz00_33)
	{
		{	/* SawMill/regset.sch 60 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3125;

				{
					obj_t BgL_auxz00_3126;

					{	/* SawMill/regset.sch 60 */
						BgL_objectz00_bglt BgL_tmpz00_3127;

						BgL_tmpz00_3127 = ((BgL_objectz00_bglt) BgL_oz00_33);
						BgL_auxz00_3126 = BGL_OBJECT_WIDENING(BgL_tmpz00_3127);
					}
					BgL_auxz00_3125 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3126);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3125))->
					BgL_interferez00);
			}
		}

	}



/* &rtl_reg/ra-interfere */
	obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2824, obj_t BgL_oz00_2825)
	{
		{	/* SawMill/regset.sch 60 */
			return
				BGl_rtl_regzf2razd2interferez20zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2825));
		}

	}



/* rtl_reg/ra-interfere-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt BgL_oz00_34, obj_t BgL_vz00_35)
	{
		{	/* SawMill/regset.sch 61 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3134;

				{
					obj_t BgL_auxz00_3135;

					{	/* SawMill/regset.sch 61 */
						BgL_objectz00_bglt BgL_tmpz00_3136;

						BgL_tmpz00_3136 = ((BgL_objectz00_bglt) BgL_oz00_34);
						BgL_auxz00_3135 = BGL_OBJECT_WIDENING(BgL_tmpz00_3136);
					}
					BgL_auxz00_3134 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3135);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3134))->
						BgL_interferez00) = ((obj_t) BgL_vz00_35), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere-set! */
	obj_t BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2826, obj_t BgL_oz00_2827, obj_t BgL_vz00_2828)
	{
		{	/* SawMill/regset.sch 61 */
			return
				BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2827), BgL_vz00_2828);
		}

	}



/* rtl_reg/ra-occurrences */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt
		BgL_oz00_36)
	{
		{	/* SawMill/regset.sch 62 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3143;

				{
					obj_t BgL_auxz00_3144;

					{	/* SawMill/regset.sch 62 */
						BgL_objectz00_bglt BgL_tmpz00_3145;

						BgL_tmpz00_3145 = ((BgL_objectz00_bglt) BgL_oz00_36);
						BgL_auxz00_3144 = BGL_OBJECT_WIDENING(BgL_tmpz00_3145);
					}
					BgL_auxz00_3143 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3144);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3143))->
					BgL_occurrencesz00);
			}
		}

	}



/* &rtl_reg/ra-occurrences */
	obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2829, obj_t BgL_oz00_2830)
	{
		{	/* SawMill/regset.sch 62 */
			return
				BINT(BGl_rtl_regzf2razd2occurrencesz20zzsaw_bbvzd2profilezd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_2830)));
		}

	}



/* rtl_reg/ra-occurrences-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt BgL_oz00_37, int BgL_vz00_38)
	{
		{	/* SawMill/regset.sch 63 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3153;

				{
					obj_t BgL_auxz00_3154;

					{	/* SawMill/regset.sch 63 */
						BgL_objectz00_bglt BgL_tmpz00_3155;

						BgL_tmpz00_3155 = ((BgL_objectz00_bglt) BgL_oz00_37);
						BgL_auxz00_3154 = BGL_OBJECT_WIDENING(BgL_tmpz00_3155);
					}
					BgL_auxz00_3153 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3154);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3153))->
						BgL_occurrencesz00) = ((int) BgL_vz00_38), BUNSPEC);
		}}

	}



/* &rtl_reg/ra-occurrences-set! */
	obj_t
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2831, obj_t BgL_oz00_2832, obj_t BgL_vz00_2833)
	{
		{	/* SawMill/regset.sch 63 */
			return
				BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2832), CINT(BgL_vz00_2833));
		}

	}



/* rtl_reg/ra-coalesce */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt
		BgL_oz00_39)
	{
		{	/* SawMill/regset.sch 64 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3163;

				{
					obj_t BgL_auxz00_3164;

					{	/* SawMill/regset.sch 64 */
						BgL_objectz00_bglt BgL_tmpz00_3165;

						BgL_tmpz00_3165 = ((BgL_objectz00_bglt) BgL_oz00_39);
						BgL_auxz00_3164 = BGL_OBJECT_WIDENING(BgL_tmpz00_3165);
					}
					BgL_auxz00_3163 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3164);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3163))->
					BgL_coalescez00);
			}
		}

	}



/* &rtl_reg/ra-coalesce */
	obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2834, obj_t BgL_oz00_2835)
	{
		{	/* SawMill/regset.sch 64 */
			return
				BGl_rtl_regzf2razd2coalescez20zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2835));
		}

	}



/* rtl_reg/ra-coalesce-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt BgL_oz00_40, obj_t BgL_vz00_41)
	{
		{	/* SawMill/regset.sch 65 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3172;

				{
					obj_t BgL_auxz00_3173;

					{	/* SawMill/regset.sch 65 */
						BgL_objectz00_bglt BgL_tmpz00_3174;

						BgL_tmpz00_3174 = ((BgL_objectz00_bglt) BgL_oz00_40);
						BgL_auxz00_3173 = BGL_OBJECT_WIDENING(BgL_tmpz00_3174);
					}
					BgL_auxz00_3172 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3173);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3172))->
						BgL_coalescez00) = ((obj_t) BgL_vz00_41), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-coalesce-set! */
	obj_t BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2836, obj_t BgL_oz00_2837, obj_t BgL_vz00_2838)
	{
		{	/* SawMill/regset.sch 65 */
			return
				BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2837), BgL_vz00_2838);
		}

	}



/* rtl_reg/ra-color */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt
		BgL_oz00_42)
	{
		{	/* SawMill/regset.sch 66 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3181;

				{
					obj_t BgL_auxz00_3182;

					{	/* SawMill/regset.sch 66 */
						BgL_objectz00_bglt BgL_tmpz00_3183;

						BgL_tmpz00_3183 = ((BgL_objectz00_bglt) BgL_oz00_42);
						BgL_auxz00_3182 = BGL_OBJECT_WIDENING(BgL_tmpz00_3183);
					}
					BgL_auxz00_3181 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3182);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3181))->BgL_colorz00);
			}
		}

	}



/* &rtl_reg/ra-color */
	obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2839, obj_t BgL_oz00_2840)
	{
		{	/* SawMill/regset.sch 66 */
			return
				BGl_rtl_regzf2razd2colorz20zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2840));
		}

	}



/* rtl_reg/ra-color-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt BgL_oz00_43, obj_t BgL_vz00_44)
	{
		{	/* SawMill/regset.sch 67 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3190;

				{
					obj_t BgL_auxz00_3191;

					{	/* SawMill/regset.sch 67 */
						BgL_objectz00_bglt BgL_tmpz00_3192;

						BgL_tmpz00_3192 = ((BgL_objectz00_bglt) BgL_oz00_43);
						BgL_auxz00_3191 = BGL_OBJECT_WIDENING(BgL_tmpz00_3192);
					}
					BgL_auxz00_3190 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3191);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3190))->
						BgL_colorz00) = ((obj_t) BgL_vz00_44), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-color-set! */
	obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2841, obj_t BgL_oz00_2842, obj_t BgL_vz00_2843)
	{
		{	/* SawMill/regset.sch 67 */
			return
				BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2842), BgL_vz00_2843);
		}

	}



/* rtl_reg/ra-num */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt
		BgL_oz00_45)
	{
		{	/* SawMill/regset.sch 68 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3199;

				{
					obj_t BgL_auxz00_3200;

					{	/* SawMill/regset.sch 68 */
						BgL_objectz00_bglt BgL_tmpz00_3201;

						BgL_tmpz00_3201 = ((BgL_objectz00_bglt) BgL_oz00_45);
						BgL_auxz00_3200 = BGL_OBJECT_WIDENING(BgL_tmpz00_3201);
					}
					BgL_auxz00_3199 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3200);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3199))->BgL_numz00);
			}
		}

	}



/* &rtl_reg/ra-num */
	obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2844, obj_t BgL_oz00_2845)
	{
		{	/* SawMill/regset.sch 68 */
			return
				BINT(BGl_rtl_regzf2razd2numz20zzsaw_bbvzd2profilezd2(
					((BgL_rtl_regz00_bglt) BgL_oz00_2845)));
		}

	}



/* rtl_reg/ra-hardware */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt
		BgL_oz00_48)
	{
		{	/* SawMill/regset.sch 70 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_48)))->BgL_hardwarez00);
		}

	}



/* &rtl_reg/ra-hardware */
	obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2846, obj_t BgL_oz00_2847)
	{
		{	/* SawMill/regset.sch 70 */
			return
				BGl_rtl_regzf2razd2hardwarez20zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2847));
		}

	}



/* rtl_reg/ra-key */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt
		BgL_oz00_51)
	{
		{	/* SawMill/regset.sch 72 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_51)))->BgL_keyz00);
		}

	}



/* &rtl_reg/ra-key */
	obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2848, obj_t BgL_oz00_2849)
	{
		{	/* SawMill/regset.sch 72 */
			return
				BGl_rtl_regzf2razd2keyz20zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2849));
		}

	}



/* rtl_reg/ra-name */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt
		BgL_oz00_54)
	{
		{	/* SawMill/regset.sch 74 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_54)))->BgL_namez00);
		}

	}



/* &rtl_reg/ra-name */
	obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2850, obj_t BgL_oz00_2851)
	{
		{	/* SawMill/regset.sch 74 */
			return
				BGl_rtl_regzf2razd2namez20zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2851));
		}

	}



/* rtl_reg/ra-onexpr? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt
		BgL_oz00_57)
	{
		{	/* SawMill/regset.sch 76 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_57)))->BgL_onexprzf3zf3);
		}

	}



/* &rtl_reg/ra-onexpr? */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2852, obj_t BgL_oz00_2853)
	{
		{	/* SawMill/regset.sch 76 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2853));
		}

	}



/* rtl_reg/ra-onexpr?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt BgL_oz00_58, obj_t BgL_vz00_59)
	{
		{	/* SawMill/regset.sch 77 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_58)))->BgL_onexprzf3zf3) =
				((obj_t) BgL_vz00_59), BUNSPEC);
		}

	}



/* &rtl_reg/ra-onexpr?-set! */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2854, obj_t BgL_oz00_2855, obj_t BgL_vz00_2856)
	{
		{	/* SawMill/regset.sch 77 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2855), BgL_vz00_2856);
		}

	}



/* rtl_reg/ra-var */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt
		BgL_oz00_60)
	{
		{	/* SawMill/regset.sch 78 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_60)))->BgL_varz00);
		}

	}



/* &rtl_reg/ra-var */
	obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2857, obj_t BgL_oz00_2858)
	{
		{	/* SawMill/regset.sch 78 */
			return
				BGl_rtl_regzf2razd2varz20zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2858));
		}

	}



/* rtl_reg/ra-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt
		BgL_oz00_61, obj_t BgL_vz00_62)
	{
		{	/* SawMill/regset.sch 79 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_61)))->BgL_varz00) =
				((obj_t) BgL_vz00_62), BUNSPEC);
		}

	}



/* &rtl_reg/ra-var-set! */
	obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2859, obj_t BgL_oz00_2860, obj_t BgL_vz00_2861)
	{
		{	/* SawMill/regset.sch 79 */
			return
				BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2860), BgL_vz00_2861);
		}

	}



/* rtl_reg/ra-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2profilezd2(BgL_rtl_regz00_bglt
		BgL_oz00_63)
	{
		{	/* SawMill/regset.sch 80 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_63)))->BgL_typez00);
		}

	}



/* &rtl_reg/ra-type */
	BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2862, obj_t BgL_oz00_2863)
	{
		{	/* SawMill/regset.sch 80 */
			return
				BGl_rtl_regzf2razd2typez20zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2863));
		}

	}



/* rtl_reg/ra-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2profilezd2
		(BgL_rtl_regz00_bglt BgL_oz00_64, BgL_typez00_bglt BgL_vz00_65)
	{
		{	/* SawMill/regset.sch 81 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_64)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_65), BUNSPEC);
		}

	}



/* &rtl_reg/ra-type-set! */
	obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2864, obj_t BgL_oz00_2865, obj_t BgL_vz00_2866)
	{
		{	/* SawMill/regset.sch 81 */
			return
				BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_bbvzd2profilezd2(
				((BgL_rtl_regz00_bglt) BgL_oz00_2865),
				((BgL_typez00_bglt) BgL_vz00_2866));
		}

	}



/* make-regset */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_bbvzd2profilezd2(int BgL_length1172z00_66,
		int BgL_msiza7e1173za7_67, obj_t BgL_regv1174z00_68,
		obj_t BgL_regl1175z00_69, obj_t BgL_string1176z00_70)
	{
		{	/* SawMill/regset.sch 84 */
			{	/* SawMill/regset.sch 84 */
				BgL_regsetz00_bglt BgL_new1168z00_2929;

				{	/* SawMill/regset.sch 84 */
					BgL_regsetz00_bglt BgL_new1167z00_2930;

					BgL_new1167z00_2930 =
						((BgL_regsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_regsetz00_bgl))));
					{	/* SawMill/regset.sch 84 */
						long BgL_arg1473z00_2931;

						BgL_arg1473z00_2931 = BGL_CLASS_NUM(BGl_regsetz00zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1167z00_2930), BgL_arg1473z00_2931);
					}
					BgL_new1168z00_2929 = BgL_new1167z00_2930;
				}
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_2929))->BgL_lengthz00) =
					((int) BgL_length1172z00_66), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_2929))->BgL_msiza7eza7) =
					((int) BgL_msiza7e1173za7_67), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_2929))->BgL_regvz00) =
					((obj_t) BgL_regv1174z00_68), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_2929))->BgL_reglz00) =
					((obj_t) BgL_regl1175z00_69), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1168z00_2929))->BgL_stringz00) =
					((obj_t) BgL_string1176z00_70), BUNSPEC);
				return BgL_new1168z00_2929;
			}
		}

	}



/* &make-regset */
	BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2867, obj_t BgL_length1172z00_2868,
		obj_t BgL_msiza7e1173za7_2869, obj_t BgL_regv1174z00_2870,
		obj_t BgL_regl1175z00_2871, obj_t BgL_string1176z00_2872)
	{
		{	/* SawMill/regset.sch 84 */
			return
				BGl_makezd2regsetzd2zzsaw_bbvzd2profilezd2(CINT(BgL_length1172z00_2868),
				CINT(BgL_msiza7e1173za7_2869), BgL_regv1174z00_2870,
				BgL_regl1175z00_2871, BgL_string1176z00_2872);
		}

	}



/* regset? */
	BGL_EXPORTED_DEF bool_t BGl_regsetzf3zf3zzsaw_bbvzd2profilezd2(obj_t
		BgL_objz00_71)
	{
		{	/* SawMill/regset.sch 85 */
			{	/* SawMill/regset.sch 85 */
				obj_t BgL_classz00_2932;

				BgL_classz00_2932 = BGl_regsetz00zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_71))
					{	/* SawMill/regset.sch 85 */
						BgL_objectz00_bglt BgL_arg1807z00_2933;

						BgL_arg1807z00_2933 = (BgL_objectz00_bglt) (BgL_objz00_71);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 85 */
								long BgL_idxz00_2934;

								BgL_idxz00_2934 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2933);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2934 + 1L)) == BgL_classz00_2932);
							}
						else
							{	/* SawMill/regset.sch 85 */
								bool_t BgL_res1915z00_2937;

								{	/* SawMill/regset.sch 85 */
									obj_t BgL_oclassz00_2938;

									{	/* SawMill/regset.sch 85 */
										obj_t BgL_arg1815z00_2939;
										long BgL_arg1816z00_2940;

										BgL_arg1815z00_2939 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 85 */
											long BgL_arg1817z00_2941;

											BgL_arg1817z00_2941 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2933);
											BgL_arg1816z00_2940 = (BgL_arg1817z00_2941 - OBJECT_TYPE);
										}
										BgL_oclassz00_2938 =
											VECTOR_REF(BgL_arg1815z00_2939, BgL_arg1816z00_2940);
									}
									{	/* SawMill/regset.sch 85 */
										bool_t BgL__ortest_1115z00_2942;

										BgL__ortest_1115z00_2942 =
											(BgL_classz00_2932 == BgL_oclassz00_2938);
										if (BgL__ortest_1115z00_2942)
											{	/* SawMill/regset.sch 85 */
												BgL_res1915z00_2937 = BgL__ortest_1115z00_2942;
											}
										else
											{	/* SawMill/regset.sch 85 */
												long BgL_odepthz00_2943;

												{	/* SawMill/regset.sch 85 */
													obj_t BgL_arg1804z00_2944;

													BgL_arg1804z00_2944 = (BgL_oclassz00_2938);
													BgL_odepthz00_2943 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2944);
												}
												if ((1L < BgL_odepthz00_2943))
													{	/* SawMill/regset.sch 85 */
														obj_t BgL_arg1802z00_2945;

														{	/* SawMill/regset.sch 85 */
															obj_t BgL_arg1803z00_2946;

															BgL_arg1803z00_2946 = (BgL_oclassz00_2938);
															BgL_arg1802z00_2945 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2946,
																1L);
														}
														BgL_res1915z00_2937 =
															(BgL_arg1802z00_2945 == BgL_classz00_2932);
													}
												else
													{	/* SawMill/regset.sch 85 */
														BgL_res1915z00_2937 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1915z00_2937;
							}
					}
				else
					{	/* SawMill/regset.sch 85 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &regset? */
	obj_t BGl_z62regsetzf3z91zzsaw_bbvzd2profilezd2(obj_t BgL_envz00_2873,
		obj_t BgL_objz00_2874)
	{
		{	/* SawMill/regset.sch 85 */
			return BBOOL(BGl_regsetzf3zf3zzsaw_bbvzd2profilezd2(BgL_objz00_2874));
		}

	}



/* regset-nil */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_bbvzd2profilezd2(void)
	{
		{	/* SawMill/regset.sch 86 */
			{	/* SawMill/regset.sch 86 */
				obj_t BgL_classz00_2748;

				BgL_classz00_2748 = BGl_regsetz00zzsaw_regsetz00;
				{	/* SawMill/regset.sch 86 */
					obj_t BgL__ortest_1117z00_2749;

					BgL__ortest_1117z00_2749 = BGL_CLASS_NIL(BgL_classz00_2748);
					if (CBOOL(BgL__ortest_1117z00_2749))
						{	/* SawMill/regset.sch 86 */
							return ((BgL_regsetz00_bglt) BgL__ortest_1117z00_2749);
						}
					else
						{	/* SawMill/regset.sch 86 */
							return
								((BgL_regsetz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2748));
						}
				}
			}
		}

	}



/* &regset-nil */
	BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2875)
	{
		{	/* SawMill/regset.sch 86 */
			return BGl_regsetzd2nilzd2zzsaw_bbvzd2profilezd2();
		}

	}



/* regset-string */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2zzsaw_bbvzd2profilezd2(BgL_regsetz00_bglt BgL_oz00_72)
	{
		{	/* SawMill/regset.sch 87 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_72))->BgL_stringz00);
		}

	}



/* &regset-string */
	obj_t BGl_z62regsetzd2stringzb0zzsaw_bbvzd2profilezd2(obj_t BgL_envz00_2876,
		obj_t BgL_oz00_2877)
	{
		{	/* SawMill/regset.sch 87 */
			return
				BGl_regsetzd2stringzd2zzsaw_bbvzd2profilezd2(
				((BgL_regsetz00_bglt) BgL_oz00_2877));
		}

	}



/* regset-string-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2profilezd2(BgL_regsetz00_bglt
		BgL_oz00_73, obj_t BgL_vz00_74)
	{
		{	/* SawMill/regset.sch 88 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_73))->BgL_stringz00) =
				((obj_t) BgL_vz00_74), BUNSPEC);
		}

	}



/* &regset-string-set! */
	obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2878, obj_t BgL_oz00_2879, obj_t BgL_vz00_2880)
	{
		{	/* SawMill/regset.sch 88 */
			return
				BGl_regsetzd2stringzd2setz12z12zzsaw_bbvzd2profilezd2(
				((BgL_regsetz00_bglt) BgL_oz00_2879), BgL_vz00_2880);
		}

	}



/* regset-regl */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2reglzd2zzsaw_bbvzd2profilezd2(BgL_regsetz00_bglt BgL_oz00_75)
	{
		{	/* SawMill/regset.sch 89 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_75))->BgL_reglz00);
		}

	}



/* &regset-regl */
	obj_t BGl_z62regsetzd2reglzb0zzsaw_bbvzd2profilezd2(obj_t BgL_envz00_2881,
		obj_t BgL_oz00_2882)
	{
		{	/* SawMill/regset.sch 89 */
			return
				BGl_regsetzd2reglzd2zzsaw_bbvzd2profilezd2(
				((BgL_regsetz00_bglt) BgL_oz00_2882));
		}

	}



/* regset-regv */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2regvzd2zzsaw_bbvzd2profilezd2(BgL_regsetz00_bglt BgL_oz00_78)
	{
		{	/* SawMill/regset.sch 91 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_78))->BgL_regvz00);
		}

	}



/* &regset-regv */
	obj_t BGl_z62regsetzd2regvzb0zzsaw_bbvzd2profilezd2(obj_t BgL_envz00_2883,
		obj_t BgL_oz00_2884)
	{
		{	/* SawMill/regset.sch 91 */
			return
				BGl_regsetzd2regvzd2zzsaw_bbvzd2profilezd2(
				((BgL_regsetz00_bglt) BgL_oz00_2884));
		}

	}



/* regset-msize */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2msiza7ez75zzsaw_bbvzd2profilezd2(BgL_regsetz00_bglt
		BgL_oz00_81)
	{
		{	/* SawMill/regset.sch 93 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_81))->BgL_msiza7eza7);
		}

	}



/* &regset-msize */
	obj_t BGl_z62regsetzd2msiza7ez17zzsaw_bbvzd2profilezd2(obj_t BgL_envz00_2885,
		obj_t BgL_oz00_2886)
	{
		{	/* SawMill/regset.sch 93 */
			return
				BINT(BGl_regsetzd2msiza7ez75zzsaw_bbvzd2profilezd2(
					((BgL_regsetz00_bglt) BgL_oz00_2886)));
		}

	}



/* regset-length */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2lengthzd2zzsaw_bbvzd2profilezd2(BgL_regsetz00_bglt BgL_oz00_84)
	{
		{	/* SawMill/regset.sch 95 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_84))->BgL_lengthz00);
		}

	}



/* &regset-length */
	obj_t BGl_z62regsetzd2lengthzb0zzsaw_bbvzd2profilezd2(obj_t BgL_envz00_2887,
		obj_t BgL_oz00_2888)
	{
		{	/* SawMill/regset.sch 95 */
			return
				BINT(BGl_regsetzd2lengthzd2zzsaw_bbvzd2profilezd2(
					((BgL_regsetz00_bglt) BgL_oz00_2888)));
		}

	}



/* regset-length-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2profilezd2(BgL_regsetz00_bglt
		BgL_oz00_85, int BgL_vz00_86)
	{
		{	/* SawMill/regset.sch 96 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_85))->BgL_lengthz00) =
				((int) BgL_vz00_86), BUNSPEC);
		}

	}



/* &regset-length-set! */
	obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_bbvzd2profilezd2(obj_t
		BgL_envz00_2889, obj_t BgL_oz00_2890, obj_t BgL_vz00_2891)
	{
		{	/* SawMill/regset.sch 96 */
			return
				BGl_regsetzd2lengthzd2setz12z12zzsaw_bbvzd2profilezd2(
				((BgL_regsetz00_bglt) BgL_oz00_2890), CINT(BgL_vz00_2891));
		}

	}



/* make-empty-bbset */
	obj_t BGl_makezd2emptyzd2bbsetz00zzsaw_bbvzd2profilezd2(void)
	{
		{	/* SawMill/bbset.sch 20 */
			{	/* SawMill/bbset.sch 21 */
				obj_t BgL_arg1513z00_2200;

				BgL_arg1513z00_2200 = BGl_getzd2bbzd2markz00zzsaw_bbvzd2typeszd2();
				{	/* SawMill/bbset.sch 15 */
					obj_t BgL_newz00_2757;

					BgL_newz00_2757 = create_struct(CNST_TABLE_REF(0), (int) (2L));
					{	/* SawMill/bbset.sch 15 */
						int BgL_tmpz00_3317;

						BgL_tmpz00_3317 = (int) (1L);
						STRUCT_SET(BgL_newz00_2757, BgL_tmpz00_3317, BNIL);
					}
					{	/* SawMill/bbset.sch 15 */
						int BgL_tmpz00_3320;

						BgL_tmpz00_3320 = (int) (0L);
						STRUCT_SET(BgL_newz00_2757, BgL_tmpz00_3320, BgL_arg1513z00_2200);
					}
					return BgL_newz00_2757;
				}
			}
		}

	}



/* bbset-cons */
	obj_t BGl_bbsetzd2conszd2zzsaw_bbvzd2profilezd2(BgL_blockz00_bglt BgL_bz00_99,
		obj_t BgL_setz00_100)
	{
		{	/* SawMill/bbset.sch 33 */
			{	/* SawMill/bbset.sch 34 */
				obj_t BgL_mz00_2204;

				BgL_mz00_2204 = STRUCT_REF(BgL_setz00_100, (int) (0L));
				{
					BgL_blocksz00_bglt BgL_auxz00_3325;

					{
						obj_t BgL_auxz00_3326;

						{	/* SawMill/bbset.sch 36 */
							BgL_objectz00_bglt BgL_tmpz00_3327;

							BgL_tmpz00_3327 = ((BgL_objectz00_bglt) BgL_bz00_99);
							BgL_auxz00_3326 = BGL_OBJECT_WIDENING(BgL_tmpz00_3327);
						}
						BgL_auxz00_3325 = ((BgL_blocksz00_bglt) BgL_auxz00_3326);
					}
					((((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_3325))->BgL_z52markz52) =
						((long) (long) CINT(BgL_mz00_2204)), BUNSPEC);
				}
				{	/* SawMill/bbset.sch 37 */
					obj_t BgL_arg1535z00_2206;

					{	/* SawMill/bbset.sch 37 */
						obj_t BgL_arg1540z00_2207;

						BgL_arg1540z00_2207 = STRUCT_REF(BgL_setz00_100, (int) (1L));
						BgL_arg1535z00_2206 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_99), BgL_arg1540z00_2207);
					}
					{	/* SawMill/bbset.sch 15 */
						int BgL_tmpz00_3337;

						BgL_tmpz00_3337 = (int) (1L);
						STRUCT_SET(BgL_setz00_100, BgL_tmpz00_3337, BgL_arg1535z00_2206);
				}}
				return BgL_setz00_100;
			}
		}

	}



/* profile! */
	BGL_EXPORTED_DEF obj_t
		BGl_profilez12z12zzsaw_bbvzd2profilezd2(BgL_blockz00_bglt BgL_bz00_105)
	{
		{	/* SawBbv/bbv-profile.scm 52 */
			{
				int BgL_labelz00_2279;
				BgL_rtl_insz00_bglt BgL_iz00_2280;
				obj_t BgL_cexprz00_2261;
				BgL_bbvzd2ctxzd2_bglt BgL_ctxz00_2262;
				obj_t BgL_locz00_2263;

				BGl_assertzd2blockszd2zzsaw_bbvzd2debugzd2(BgL_bz00_105,
					BGl_string1920z00zzsaw_bbvzd2profilezd2);
				if (CBOOL(BGl_za2bbvzd2profileza2zd2zzsaw_bbvzd2configzd2))
					{	/* SawBbv/bbv-profile.scm 75 */
						obj_t BgL_g1181z00_2234;
						obj_t BgL_g1182z00_2235;

						{	/* SawBbv/bbv-profile.scm 75 */
							obj_t BgL_list1628z00_2260;

							BgL_list1628z00_2260 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_bz00_105), BNIL);
							BgL_g1181z00_2234 = BgL_list1628z00_2260;
						}
						BgL_g1182z00_2235 =
							BGl_makezd2emptyzd2bbsetz00zzsaw_bbvzd2profilezd2();
						{
							obj_t BgL_bsz00_2237;
							obj_t BgL_accz00_2238;

							{
								BgL_blockz00_bglt BgL_auxz00_3346;

								BgL_bsz00_2237 = BgL_g1181z00_2234;
								BgL_accz00_2238 = BgL_g1182z00_2235;
							BgL_zc3z04anonymousza31586ze3z87_2239:
								if (NULLP(BgL_bsz00_2237))
									{	/* SawBbv/bbv-profile.scm 78 */
										BgL_auxz00_3346 = BgL_bz00_105;
									}
								else
									{	/* SawBbv/bbv-profile.scm 80 */
										bool_t BgL_test1978z00_3349;

										{	/* SawBbv/bbv-profile.scm 80 */
											BgL_blockz00_bglt BgL_blockz00_2791;

											BgL_blockz00_2791 =
												((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2237)));
											{	/* SawBbv/bbv-profile.scm 80 */
												long BgL_arg1514z00_2794;
												obj_t BgL_arg1516z00_2795;

												{
													BgL_blocksz00_bglt BgL_auxz00_3353;

													{
														obj_t BgL_auxz00_3354;

														{	/* SawBbv/bbv-profile.scm 80 */
															BgL_objectz00_bglt BgL_tmpz00_3355;

															BgL_tmpz00_3355 =
																((BgL_objectz00_bglt) BgL_blockz00_2791);
															BgL_auxz00_3354 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_3355);
														}
														BgL_auxz00_3353 =
															((BgL_blocksz00_bglt) BgL_auxz00_3354);
													}
													BgL_arg1514z00_2794 =
														(((BgL_blocksz00_bglt) COBJECT(BgL_auxz00_3353))->
														BgL_z52markz52);
												}
												BgL_arg1516z00_2795 =
													STRUCT_REF(BgL_accz00_2238, (int) (0L));
												BgL_test1978z00_3349 =
													(BINT(BgL_arg1514z00_2794) == BgL_arg1516z00_2795);
										}}
										if (BgL_test1978z00_3349)
											{	/* SawBbv/bbv-profile.scm 81 */
												obj_t BgL_arg1593z00_2243;

												BgL_arg1593z00_2243 = CDR(((obj_t) BgL_bsz00_2237));
												{
													obj_t BgL_bsz00_3366;

													BgL_bsz00_3366 = BgL_arg1593z00_2243;
													BgL_bsz00_2237 = BgL_bsz00_3366;
													goto BgL_zc3z04anonymousza31586ze3z87_2239;
												}
											}
										else
											{	/* SawBbv/bbv-profile.scm 83 */
												BgL_blockz00_bglt BgL_i1183z00_2244;

												BgL_i1183z00_2244 =
													((BgL_blockz00_bglt) CAR(((obj_t) BgL_bsz00_2237)));
												if (NULLP(
														(((BgL_blockz00_bglt) COBJECT(
																	((BgL_blockz00_bglt) BgL_i1183z00_2244)))->
															BgL_firstz00)))
													{	/* SawBbv/bbv-profile.scm 84 */
														BFALSE;
													}
												else
													{	/* SawBbv/bbv-profile.scm 85 */
														BgL_rtl_insz00_bglt BgL_insz00_2247;

														{	/* SawBbv/bbv-profile.scm 85 */
															int BgL_arg1605z00_2249;
															obj_t BgL_arg1606z00_2250;

															BgL_arg1605z00_2249 =
																(((BgL_blockz00_bglt) COBJECT(
																		((BgL_blockz00_bglt) BgL_i1183z00_2244)))->
																BgL_labelz00);
															{	/* SawBbv/bbv-profile.scm 85 */
																obj_t BgL_pairz00_2800;

																BgL_pairz00_2800 =
																	(((BgL_blockz00_bglt) COBJECT(
																			((BgL_blockz00_bglt)
																				BgL_i1183z00_2244)))->BgL_firstz00);
																BgL_arg1606z00_2250 = CAR(BgL_pairz00_2800);
															}
															BgL_labelz00_2279 = BgL_arg1605z00_2249;
															BgL_iz00_2280 =
																((BgL_rtl_insz00_bglt) BgL_arg1606z00_2250);
															{	/* SawBbv/bbv-profile.scm 69 */
																obj_t BgL_cexprz00_2282;

																{	/* SawBbv/bbv-profile.scm 69 */
																	obj_t BgL_list1662z00_2286;

																	BgL_list1662z00_2286 =
																		MAKE_YOUNG_PAIR(BINT(BgL_labelz00_2279),
																		BNIL);
																	BgL_cexprz00_2282 =
																		BGl_formatz00zz__r4_output_6_10_3z00
																		(BGl_string1919z00zzsaw_bbvzd2profilezd2,
																		BgL_list1662z00_2286);
																}
																{	/* SawBbv/bbv-profile.scm 71 */
																	BgL_bbvzd2ctxzd2_bglt BgL_arg1654z00_2284;
																	obj_t BgL_arg1661z00_2285;

																	{
																		BgL_rtl_inszf2bbvzf2_bglt BgL_auxz00_3382;

																		{
																			obj_t BgL_auxz00_3383;

																			{	/* SawBbv/bbv-profile.scm 71 */
																				BgL_objectz00_bglt BgL_tmpz00_3384;

																				BgL_tmpz00_3384 =
																					((BgL_objectz00_bglt) BgL_iz00_2280);
																				BgL_auxz00_3383 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_3384);
																			}
																			BgL_auxz00_3382 =
																				((BgL_rtl_inszf2bbvzf2_bglt)
																				BgL_auxz00_3383);
																		}
																		BgL_arg1654z00_2284 =
																			(((BgL_rtl_inszf2bbvzf2_bglt)
																				COBJECT(BgL_auxz00_3382))->BgL_ctxz00);
																	}
																	BgL_arg1661z00_2285 =
																		(((BgL_rtl_insz00_bglt) COBJECT(
																				((BgL_rtl_insz00_bglt)
																					BgL_iz00_2280)))->BgL_locz00);
																	BgL_cexprz00_2261 = BgL_cexprz00_2282;
																	BgL_ctxz00_2262 = BgL_arg1654z00_2284;
																	BgL_locz00_2263 = BgL_arg1661z00_2285;
																	{	/* SawBbv/bbv-profile.scm 55 */
																		BgL_rtl_pragmaz00_bglt BgL_prz00_2265;
																		BgL_regsetz00_bglt BgL_esz00_2266;

																		{	/* SawBbv/bbv-profile.scm 55 */
																			BgL_rtl_pragmaz00_bglt
																				BgL_new1173z00_2273;
																			{	/* SawBbv/bbv-profile.scm 55 */
																				BgL_rtl_pragmaz00_bglt
																					BgL_new1172z00_2274;
																				BgL_new1172z00_2274 =
																					((BgL_rtl_pragmaz00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_rtl_pragmaz00_bgl))));
																				{	/* SawBbv/bbv-profile.scm 55 */
																					long BgL_arg1646z00_2275;

																					BgL_arg1646z00_2275 =
																						BGL_CLASS_NUM
																						(BGl_rtl_pragmaz00zzsaw_defsz00);
																					BGL_OBJECT_CLASS_NUM_SET((
																							(BgL_objectz00_bglt)
																							BgL_new1172z00_2274),
																						BgL_arg1646z00_2275);
																				}
																				BgL_new1173z00_2273 =
																					BgL_new1172z00_2274;
																			}
																			((((BgL_rtl_funz00_bglt) COBJECT(
																							((BgL_rtl_funz00_bglt)
																								BgL_new1173z00_2273)))->
																					BgL_locz00) =
																				((obj_t) BFALSE), BUNSPEC);
																			((((BgL_rtl_pragmaz00_bglt)
																						COBJECT(BgL_new1173z00_2273))->
																					BgL_formatz00) =
																				((obj_t) BgL_cexprz00_2261), BUNSPEC);
																			((((BgL_rtl_pragmaz00_bglt)
																						COBJECT(BgL_new1173z00_2273))->
																					BgL_srfi0z00) =
																				((obj_t) CNST_TABLE_REF(1)), BUNSPEC);
																			BgL_prz00_2265 = BgL_new1173z00_2273;
																		}
																		{	/* SawBbv/bbv-profile.scm 58 */
																			BgL_regsetz00_bglt BgL_new1175z00_2276;

																			{	/* SawBbv/bbv-profile.scm 58 */
																				BgL_regsetz00_bglt BgL_new1174z00_2277;

																				BgL_new1174z00_2277 =
																					((BgL_regsetz00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_regsetz00_bgl))));
																				{	/* SawBbv/bbv-profile.scm 58 */
																					long BgL_arg1650z00_2278;

																					BgL_arg1650z00_2278 =
																						BGL_CLASS_NUM
																						(BGl_regsetz00zzsaw_regsetz00);
																					BGL_OBJECT_CLASS_NUM_SET((
																							(BgL_objectz00_bglt)
																							BgL_new1174z00_2277),
																						BgL_arg1650z00_2278);
																				}
																				BgL_new1175z00_2276 =
																					BgL_new1174z00_2277;
																			}
																			((((BgL_regsetz00_bglt)
																						COBJECT(BgL_new1175z00_2276))->
																					BgL_lengthz00) =
																				((int) (int) (0L)), BUNSPEC);
																			((((BgL_regsetz00_bglt)
																						COBJECT(BgL_new1175z00_2276))->
																					BgL_msiza7eza7) =
																				((int) (int) (0L)), BUNSPEC);
																			((((BgL_regsetz00_bglt)
																						COBJECT(BgL_new1175z00_2276))->
																					BgL_regvz00) =
																				((obj_t) CNST_TABLE_REF(2)), BUNSPEC);
																			((((BgL_regsetz00_bglt)
																						COBJECT(BgL_new1175z00_2276))->
																					BgL_reglz00) =
																				((obj_t) BNIL), BUNSPEC);
																			((((BgL_regsetz00_bglt)
																						COBJECT(BgL_new1175z00_2276))->
																					BgL_stringz00) =
																				((obj_t)
																					BGl_string1918z00zzsaw_bbvzd2profilezd2),
																				BUNSPEC);
																			BgL_esz00_2266 = BgL_new1175z00_2276;
																		}
																		{	/* SawBbv/bbv-profile.scm 59 */
																			BgL_rtl_insz00_bglt BgL_new1179z00_2267;

																			{	/* SawBbv/bbv-profile.scm 60 */
																				BgL_rtl_insz00_bglt BgL_tmp1177z00_2268;
																				BgL_rtl_inszf2bbvzf2_bglt
																					BgL_wide1178z00_2269;
																				{
																					BgL_rtl_insz00_bglt BgL_auxz00_3412;

																					{	/* SawBbv/bbv-profile.scm 60 */
																						BgL_rtl_insz00_bglt
																							BgL_new1176z00_2271;
																						BgL_new1176z00_2271 =
																							((BgL_rtl_insz00_bglt)
																							BOBJECT(GC_MALLOC(sizeof(struct
																										BgL_rtl_insz00_bgl))));
																						{	/* SawBbv/bbv-profile.scm 60 */
																							long BgL_arg1642z00_2272;

																							BgL_arg1642z00_2272 =
																								BGL_CLASS_NUM
																								(BGl_rtl_insz00zzsaw_defsz00);
																							BGL_OBJECT_CLASS_NUM_SET((
																									(BgL_objectz00_bglt)
																									BgL_new1176z00_2271),
																								BgL_arg1642z00_2272);
																						}
																						{	/* SawBbv/bbv-profile.scm 60 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_3417;
																							BgL_tmpz00_3417 =
																								((BgL_objectz00_bglt)
																								BgL_new1176z00_2271);
																							BGL_OBJECT_WIDENING_SET
																								(BgL_tmpz00_3417, BFALSE);
																						}
																						((BgL_objectz00_bglt)
																							BgL_new1176z00_2271);
																						BgL_auxz00_3412 =
																							BgL_new1176z00_2271;
																					}
																					BgL_tmp1177z00_2268 =
																						((BgL_rtl_insz00_bglt)
																						BgL_auxz00_3412);
																				}
																				BgL_wide1178z00_2269 =
																					((BgL_rtl_inszf2bbvzf2_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_rtl_inszf2bbvzf2_bgl))));
																				{	/* SawBbv/bbv-profile.scm 60 */
																					obj_t BgL_auxz00_3425;
																					BgL_objectz00_bglt BgL_tmpz00_3423;

																					BgL_auxz00_3425 =
																						((obj_t) BgL_wide1178z00_2269);
																					BgL_tmpz00_3423 =
																						((BgL_objectz00_bglt)
																						BgL_tmp1177z00_2268);
																					BGL_OBJECT_WIDENING_SET
																						(BgL_tmpz00_3423, BgL_auxz00_3425);
																				}
																				((BgL_objectz00_bglt)
																					BgL_tmp1177z00_2268);
																				{	/* SawBbv/bbv-profile.scm 60 */
																					long BgL_arg1630z00_2270;

																					BgL_arg1630z00_2270 =
																						BGL_CLASS_NUM
																						(BGl_rtl_inszf2bbvzf2zzsaw_bbvzd2typeszd2);
																					BGL_OBJECT_CLASS_NUM_SET((
																							(BgL_objectz00_bglt)
																							BgL_tmp1177z00_2268),
																						BgL_arg1630z00_2270);
																				}
																				BgL_new1179z00_2267 =
																					((BgL_rtl_insz00_bglt)
																					BgL_tmp1177z00_2268);
																			}
																			((((BgL_rtl_insz00_bglt) COBJECT(
																							((BgL_rtl_insz00_bglt)
																								BgL_new1179z00_2267)))->
																					BgL_locz00) =
																				((obj_t) BgL_locz00_2263), BUNSPEC);
																			((((BgL_rtl_insz00_bglt)
																						COBJECT(((BgL_rtl_insz00_bglt)
																								BgL_new1179z00_2267)))->
																					BgL_z52spillz52) =
																				((obj_t) BNIL), BUNSPEC);
																			((((BgL_rtl_insz00_bglt)
																						COBJECT(((BgL_rtl_insz00_bglt)
																								BgL_new1179z00_2267)))->
																					BgL_destz00) =
																				((obj_t) BFALSE), BUNSPEC);
																			((((BgL_rtl_insz00_bglt)
																						COBJECT(((BgL_rtl_insz00_bglt)
																								BgL_new1179z00_2267)))->
																					BgL_funz00) =
																				((BgL_rtl_funz00_bglt) (
																						(BgL_rtl_funz00_bglt)
																						BgL_prz00_2265)), BUNSPEC);
																			((((BgL_rtl_insz00_bglt)
																						COBJECT(((BgL_rtl_insz00_bglt)
																								BgL_new1179z00_2267)))->
																					BgL_argsz00) =
																				((obj_t) BNIL), BUNSPEC);
																			{
																				BgL_rtl_inszf2bbvzf2_bglt
																					BgL_auxz00_3444;
																				{
																					obj_t BgL_auxz00_3445;

																					{	/* SawBbv/bbv-profile.scm 65 */
																						BgL_objectz00_bglt BgL_tmpz00_3446;

																						BgL_tmpz00_3446 =
																							((BgL_objectz00_bglt)
																							BgL_new1179z00_2267);
																						BgL_auxz00_3445 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_3446);
																					}
																					BgL_auxz00_3444 =
																						((BgL_rtl_inszf2bbvzf2_bglt)
																						BgL_auxz00_3445);
																				}
																				((((BgL_rtl_inszf2bbvzf2_bglt)
																							COBJECT(BgL_auxz00_3444))->
																						BgL_defz00) =
																					((obj_t) ((obj_t) BgL_esz00_2266)),
																					BUNSPEC);
																			}
																			{
																				BgL_rtl_inszf2bbvzf2_bglt
																					BgL_auxz00_3452;
																				{
																					obj_t BgL_auxz00_3453;

																					{	/* SawBbv/bbv-profile.scm 64 */
																						BgL_objectz00_bglt BgL_tmpz00_3454;

																						BgL_tmpz00_3454 =
																							((BgL_objectz00_bglt)
																							BgL_new1179z00_2267);
																						BgL_auxz00_3453 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_3454);
																					}
																					BgL_auxz00_3452 =
																						((BgL_rtl_inszf2bbvzf2_bglt)
																						BgL_auxz00_3453);
																				}
																				((((BgL_rtl_inszf2bbvzf2_bglt)
																							COBJECT(BgL_auxz00_3452))->
																						BgL_outz00) =
																					((obj_t) ((obj_t) BgL_esz00_2266)),
																					BUNSPEC);
																			}
																			{
																				BgL_rtl_inszf2bbvzf2_bglt
																					BgL_auxz00_3460;
																				{
																					obj_t BgL_auxz00_3461;

																					{	/* SawBbv/bbv-profile.scm 63 */
																						BgL_objectz00_bglt BgL_tmpz00_3462;

																						BgL_tmpz00_3462 =
																							((BgL_objectz00_bglt)
																							BgL_new1179z00_2267);
																						BgL_auxz00_3461 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_3462);
																					}
																					BgL_auxz00_3460 =
																						((BgL_rtl_inszf2bbvzf2_bglt)
																						BgL_auxz00_3461);
																				}
																				((((BgL_rtl_inszf2bbvzf2_bglt)
																							COBJECT(BgL_auxz00_3460))->
																						BgL_inz00) =
																					((obj_t) ((obj_t) BgL_esz00_2266)),
																					BUNSPEC);
																			}
																			{
																				BgL_rtl_inszf2bbvzf2_bglt
																					BgL_auxz00_3468;
																				{
																					obj_t BgL_auxz00_3469;

																					{	/* SawBbv/bbv-profile.scm 62 */
																						BgL_objectz00_bglt BgL_tmpz00_3470;

																						BgL_tmpz00_3470 =
																							((BgL_objectz00_bglt)
																							BgL_new1179z00_2267);
																						BgL_auxz00_3469 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_3470);
																					}
																					BgL_auxz00_3468 =
																						((BgL_rtl_inszf2bbvzf2_bglt)
																						BgL_auxz00_3469);
																				}
																				((((BgL_rtl_inszf2bbvzf2_bglt)
																							COBJECT(BgL_auxz00_3468))->
																						BgL_ctxz00) =
																					((BgL_bbvzd2ctxzd2_bglt)
																						BgL_ctxz00_2262), BUNSPEC);
																			}
																			{
																				BgL_rtl_inszf2bbvzf2_bglt
																					BgL_auxz00_3475;
																				{
																					obj_t BgL_auxz00_3476;

																					{	/* SawBbv/bbv-profile.scm 62 */
																						BgL_objectz00_bglt BgL_tmpz00_3477;

																						BgL_tmpz00_3477 =
																							((BgL_objectz00_bglt)
																							BgL_new1179z00_2267);
																						BgL_auxz00_3476 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_3477);
																					}
																					BgL_auxz00_3475 =
																						((BgL_rtl_inszf2bbvzf2_bglt)
																						BgL_auxz00_3476);
																				}
																				((((BgL_rtl_inszf2bbvzf2_bglt)
																							COBJECT(BgL_auxz00_3475))->
																						BgL_z52hashz52) =
																					((obj_t) BFALSE), BUNSPEC);
																			}
																			BgL_insz00_2247 = BgL_new1179z00_2267;
														}}}}}
														{
															obj_t BgL_auxz00_3483;

															{	/* SawBbv/bbv-profile.scm 86 */
																obj_t BgL_arg1602z00_2248;

																BgL_arg1602z00_2248 =
																	(((BgL_blockz00_bglt) COBJECT(
																			((BgL_blockz00_bglt)
																				BgL_i1183z00_2244)))->BgL_firstz00);
																BgL_auxz00_3483 =
																	MAKE_YOUNG_PAIR(((obj_t) BgL_insz00_2247),
																	BgL_arg1602z00_2248);
															}
															((((BgL_blockz00_bglt) COBJECT(
																			((BgL_blockz00_bglt)
																				BgL_i1183z00_2244)))->BgL_firstz00) =
																((obj_t) BgL_auxz00_3483), BUNSPEC);
													}}
												{	/* SawBbv/bbv-profile.scm 87 */
													obj_t BgL_arg1613z00_2253;
													obj_t BgL_arg1615z00_2254;

													{	/* SawBbv/bbv-profile.scm 87 */
														obj_t BgL_arg1616z00_2255;
														obj_t BgL_arg1625z00_2256;

														BgL_arg1616z00_2255 =
															(((BgL_blockz00_bglt) COBJECT(
																	((BgL_blockz00_bglt) BgL_i1183z00_2244)))->
															BgL_succsz00);
														BgL_arg1625z00_2256 = CDR(((obj_t) BgL_bsz00_2237));
														BgL_arg1613z00_2253 =
															BGl_appendzd221011zd2zzsaw_bbvzd2profilezd2
															(BgL_arg1616z00_2255, BgL_arg1625z00_2256);
													}
													{	/* SawBbv/bbv-profile.scm 87 */
														obj_t BgL_arg1626z00_2257;

														BgL_arg1626z00_2257 = CAR(((obj_t) BgL_bsz00_2237));
														BgL_arg1615z00_2254 =
															BGl_bbsetzd2conszd2zzsaw_bbvzd2profilezd2(
															((BgL_blockz00_bglt) BgL_arg1626z00_2257),
															BgL_accz00_2238);
													}
													{
														obj_t BgL_accz00_3500;
														obj_t BgL_bsz00_3499;

														BgL_bsz00_3499 = BgL_arg1613z00_2253;
														BgL_accz00_3500 = BgL_arg1615z00_2254;
														BgL_accz00_2238 = BgL_accz00_3500;
														BgL_bsz00_2237 = BgL_bsz00_3499;
														goto BgL_zc3z04anonymousza31586ze3z87_2239;
													}
												}
											}
									}
								return ((obj_t) BgL_auxz00_3346);
							}
						}
					}
				else
					{	/* SawBbv/bbv-profile.scm 74 */
						return ((obj_t) BgL_bz00_105);
					}
			}
		}

	}



/* &profile! */
	obj_t BGl_z62profilez12z70zzsaw_bbvzd2profilezd2(obj_t BgL_envz00_2892,
		obj_t BgL_bz00_2893)
	{
		{	/* SawBbv/bbv-profile.scm 52 */
			return
				BGl_profilez12z12zzsaw_bbvzd2profilezd2(
				((BgL_blockz00_bglt) BgL_bz00_2893));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_bbvzd2profilezd2(void)
	{
		{	/* SawBbv/bbv-profile.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_bbvzd2profilezd2(void)
	{
		{	/* SawBbv/bbv-profile.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_bbvzd2profilezd2(void)
	{
		{	/* SawBbv/bbv-profile.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_bbvzd2profilezd2(void)
	{
		{	/* SawBbv/bbv-profile.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zzsaw_regsetz00(358079690L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(237915200L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2configzd2(288263219L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2typeszd2(425659118L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2specializa7ez75(506937389L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2cachezd2(242097300L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			BGl_modulezd2initializa7ationz75zzsaw_bbvzd2rangezd2(481635416L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
			return
				BGl_modulezd2initializa7ationz75zzsaw_bbvzd2debugzd2(120981929L,
				BSTRING_TO_STRING(BGl_string1921z00zzsaw_bbvzd2profilezd2));
		}

	}

#ifdef __cplusplus
}
#endif
