/*===========================================================================*/
/*   (Init/extend.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Init/extend.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INIT_EXTEND_TYPE_DEFINITIONS
#define BGL_INIT_EXTEND_TYPE_DEFINITIONS
#endif													// BGL_INIT_EXTEND_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzinit_extendz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzinit_extendz00(void);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzinit_extendz00(void);
	static obj_t BGl_objectzd2initzd2zzinit_extendz00(void);
	static obj_t BGl_z62loadzd2extendzb0zzinit_extendz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzinit_extendz00(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzinit_extendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__evalz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_loadqz00zz__evalz00(obj_t, obj_t);
	extern obj_t BGl_za2libzd2dirza2zd2zzengine_paramz00;
	static obj_t BGl_cnstzd2initzd2zzinit_extendz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzinit_extendz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzinit_extendz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzinit_extendz00(void);
	static obj_t BGl_za2extendzd2tableza2zd2zzinit_extendz00 = BUNSPEC;
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	BGL_IMPORT obj_t BGl_findzd2filezf2pathz20zz__osz00(obj_t, obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_defaultzd2environmentzd2zz__evalz00(void);
	BGL_EXPORTED_DECL obj_t BGl_loadzd2extendzd2zzinit_extendz00(obj_t);
	static obj_t __cnst[1];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_loadzd2extendzd2envz00zzinit_extendz00,
		BgL_bgl_za762loadza7d2extend1063z00,
		BGl_z62loadzd2extendzb0zzinit_extendz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1054z00zzinit_extendz00,
		BgL_bgl_string1054za700za7za7i1064za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1055z00zzinit_extendz00,
		BgL_bgl_string1055za700za7za7i1065za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1056z00zzinit_extendz00,
		BgL_bgl_string1056za700za7za7i1066za7, ".init", 5);
	      DEFINE_STRING(BGl_string1057z00zzinit_extendz00,
		BgL_bgl_string1057za700za7za7i1067za7, "parse-args", 10);
	      DEFINE_STRING(BGl_string1058z00zzinit_extendz00,
		BgL_bgl_string1058za700za7za7i1068za7, "Can't find extend file", 22);
	      DEFINE_STRING(BGl_string1059z00zzinit_extendz00,
		BgL_bgl_string1059za700za7za7i1069za7, "init_extend", 11);
	      DEFINE_STRING(BGl_string1060z00zzinit_extendz00,
		BgL_bgl_string1060za700za7za7i1070za7, "pass-started ", 13);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzinit_extendz00));
		     ADD_ROOT((void *) (&BGl_za2extendzd2tableza2zd2zzinit_extendz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzinit_extendz00(long
		BgL_checksumz00_79, char *BgL_fromz00_80)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzinit_extendz00))
				{
					BGl_requirezd2initializa7ationz75zzinit_extendz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzinit_extendz00();
					BGl_libraryzd2moduleszd2initz00zzinit_extendz00();
					BGl_cnstzd2initzd2zzinit_extendz00();
					BGl_importedzd2moduleszd2initz00zzinit_extendz00();
					return BGl_toplevelzd2initzd2zzinit_extendz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzinit_extendz00(void)
	{
		{	/* Init/extend.scm 16 */
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "init_extend");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "init_extend");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "init_extend");
			BGl_modulezd2initializa7ationz75zz__evalz00(0L, "init_extend");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "init_extend");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"init_extend");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "init_extend");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzinit_extendz00(void)
	{
		{	/* Init/extend.scm 16 */
			{	/* Init/extend.scm 16 */
				obj_t BgL_cportz00_68;

				{	/* Init/extend.scm 16 */
					obj_t BgL_stringz00_75;

					BgL_stringz00_75 = BGl_string1060z00zzinit_extendz00;
					{	/* Init/extend.scm 16 */
						obj_t BgL_startz00_76;

						BgL_startz00_76 = BINT(0L);
						{	/* Init/extend.scm 16 */
							obj_t BgL_endz00_77;

							BgL_endz00_77 = BINT(STRING_LENGTH(((obj_t) BgL_stringz00_75)));
							{	/* Init/extend.scm 16 */

								BgL_cportz00_68 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_75, BgL_startz00_76, BgL_endz00_77);
				}}}}
				{
					long BgL_iz00_69;

					BgL_iz00_69 = 0L;
				BgL_loopz00_70:
					if ((BgL_iz00_69 == -1L))
						{	/* Init/extend.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Init/extend.scm 16 */
							{	/* Init/extend.scm 16 */
								obj_t BgL_arg1062z00_71;

								{	/* Init/extend.scm 16 */

									{	/* Init/extend.scm 16 */
										obj_t BgL_locationz00_73;

										BgL_locationz00_73 = BBOOL(((bool_t) 0));
										{	/* Init/extend.scm 16 */

											BgL_arg1062z00_71 =
												BGl_readz00zz__readerz00(BgL_cportz00_68,
												BgL_locationz00_73);
										}
									}
								}
								{	/* Init/extend.scm 16 */
									int BgL_tmpz00_105;

									BgL_tmpz00_105 = (int) (BgL_iz00_69);
									CNST_TABLE_SET(BgL_tmpz00_105, BgL_arg1062z00_71);
							}}
							{	/* Init/extend.scm 16 */
								int BgL_auxz00_74;

								BgL_auxz00_74 = (int) ((BgL_iz00_69 - 1L));
								{
									long BgL_iz00_110;

									BgL_iz00_110 = (long) (BgL_auxz00_74);
									BgL_iz00_69 = BgL_iz00_110;
									goto BgL_loopz00_70;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzinit_extendz00(void)
	{
		{	/* Init/extend.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzinit_extendz00(void)
	{
		{	/* Init/extend.scm 16 */
			return (BGl_za2extendzd2tableza2zd2zzinit_extendz00 = BNIL, BUNSPEC);
		}

	}



/* load-extend */
	BGL_EXPORTED_DEF obj_t BGl_loadzd2extendzd2zzinit_extendz00(obj_t
		BgL_extendzd2namezd2_3)
	{
		{	/* Init/extend.scm 25 */
			if (CBOOL(BGl_memberz00zz__r4_pairs_and_lists_6_3z00
					(BgL_extendzd2namezd2_3,
						BGl_za2extendzd2tableza2zd2zzinit_extendz00)))
				{	/* Init/extend.scm 26 */
					return BFALSE;
				}
			else
				{	/* Init/extend.scm 26 */
					BGl_za2extendzd2tableza2zd2zzinit_extendz00 =
						MAKE_YOUNG_PAIR(BgL_extendzd2namezd2_3,
						BGl_za2extendzd2tableza2zd2zzinit_extendz00);
					{	/* Init/extend.scm 28 */
						obj_t BgL_fnamez00_13;

						BgL_fnamez00_13 =
							BGl_findzd2filezf2pathz20zz__osz00(BgL_extendzd2namezd2_3,
							BGl_za2libzd2dirza2zd2zzengine_paramz00);
						if (CBOOL(BgL_fnamez00_13))
							{	/* Init/extend.scm 29 */
								{	/* Init/extend.scm 31 */
									obj_t BgL_list1024z00_14;

									{	/* Init/extend.scm 31 */
										obj_t BgL_arg1025z00_15;

										{	/* Init/extend.scm 31 */
											obj_t BgL_arg1026z00_16;

											BgL_arg1026z00_16 =
												MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
											BgL_arg1025z00_15 =
												MAKE_YOUNG_PAIR(BgL_fnamez00_13, BgL_arg1026z00_16);
										}
										BgL_list1024z00_14 =
											MAKE_YOUNG_PAIR(BGl_string1054z00zzinit_extendz00,
											BgL_arg1025z00_15);
									}
									BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1024z00_14);
								}
								BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
								BGl_za2currentzd2passza2zd2zzengine_passz00 = BgL_fnamez00_13;
								{	/* Init/extend.scm 31 */
									obj_t BgL_g1016z00_17;

									BgL_g1016z00_17 = BNIL;
									{
										obj_t BgL_hooksz00_20;
										obj_t BgL_hnamesz00_21;

										BgL_hooksz00_20 = BgL_g1016z00_17;
										BgL_hnamesz00_21 = BNIL;
									BgL_zc3z04anonymousza31027ze3z87_22:
										if (NULLP(BgL_hooksz00_20))
											{	/* Init/extend.scm 31 */
												CNST_TABLE_REF(0);
											}
										else
											{	/* Init/extend.scm 31 */
												bool_t BgL_test1076z00_130;

												{	/* Init/extend.scm 31 */
													obj_t BgL_fun1037z00_29;

													BgL_fun1037z00_29 = CAR(((obj_t) BgL_hooksz00_20));
													BgL_test1076z00_130 =
														CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1037z00_29));
												}
												if (BgL_test1076z00_130)
													{	/* Init/extend.scm 31 */
														obj_t BgL_arg1033z00_26;
														obj_t BgL_arg1035z00_27;

														BgL_arg1033z00_26 = CDR(((obj_t) BgL_hooksz00_20));
														BgL_arg1035z00_27 = CDR(((obj_t) BgL_hnamesz00_21));
														{
															obj_t BgL_hnamesz00_142;
															obj_t BgL_hooksz00_141;

															BgL_hooksz00_141 = BgL_arg1033z00_26;
															BgL_hnamesz00_142 = BgL_arg1035z00_27;
															BgL_hnamesz00_21 = BgL_hnamesz00_142;
															BgL_hooksz00_20 = BgL_hooksz00_141;
															goto BgL_zc3z04anonymousza31027ze3z87_22;
														}
													}
												else
													{	/* Init/extend.scm 31 */
														obj_t BgL_arg1036z00_28;

														BgL_arg1036z00_28 = CAR(((obj_t) BgL_hnamesz00_21));
														BGl_internalzd2errorzd2zztools_errorz00
															(BgL_fnamez00_13,
															BGl_string1055z00zzinit_extendz00,
															BgL_arg1036z00_28);
													}
											}
									}
								}
								{	/* Init/extend.scm 32 */
									obj_t BgL_envz00_33;

									BgL_envz00_33 = BGl_defaultzd2environmentzd2zz__evalz00();
									{	/* Init/extend.scm 32 */

										return
											BGl_loadqz00zz__evalz00(BgL_fnamez00_13, BgL_envz00_33);
									}
								}
							}
						else
							{	/* Init/extend.scm 33 */
								obj_t BgL_fnamez00_34;

								{	/* Init/extend.scm 33 */
									obj_t BgL_arg1051z00_55;

									BgL_arg1051z00_55 =
										string_append(BgL_extendzd2namezd2_3,
										BGl_string1056z00zzinit_extendz00);
									BgL_fnamez00_34 =
										BGl_findzd2filezf2pathz20zz__osz00(BgL_arg1051z00_55,
										BGl_za2libzd2dirza2zd2zzengine_paramz00);
								}
								if (CBOOL(BgL_fnamez00_34))
									{	/* Init/extend.scm 35 */
										{	/* Init/extend.scm 37 */
											obj_t BgL_list1039z00_35;

											{	/* Init/extend.scm 37 */
												obj_t BgL_arg1040z00_36;

												{	/* Init/extend.scm 37 */
													obj_t BgL_arg1041z00_37;

													BgL_arg1041z00_37 =
														MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
													BgL_arg1040z00_36 =
														MAKE_YOUNG_PAIR(BgL_fnamez00_34, BgL_arg1041z00_37);
												}
												BgL_list1039z00_35 =
													MAKE_YOUNG_PAIR(BGl_string1054z00zzinit_extendz00,
													BgL_arg1040z00_36);
											}
											BGl_verbosez00zztools_speekz00(BINT(1L),
												BgL_list1039z00_35);
										}
										BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 =
											BINT(0L);
										BGl_za2currentzd2passza2zd2zzengine_passz00 =
											BgL_fnamez00_34;
										{	/* Init/extend.scm 37 */
											obj_t BgL_g1018z00_38;

											BgL_g1018z00_38 = BNIL;
											{
												obj_t BgL_hooksz00_41;
												obj_t BgL_hnamesz00_42;

												BgL_hooksz00_41 = BgL_g1018z00_38;
												BgL_hnamesz00_42 = BNIL;
											BgL_zc3z04anonymousza31042ze3z87_43:
												if (NULLP(BgL_hooksz00_41))
													{	/* Init/extend.scm 37 */
														CNST_TABLE_REF(0);
													}
												else
													{	/* Init/extend.scm 37 */
														bool_t BgL_test1079z00_162;

														{	/* Init/extend.scm 37 */
															obj_t BgL_fun1049z00_50;

															BgL_fun1049z00_50 =
																CAR(((obj_t) BgL_hooksz00_41));
															BgL_test1079z00_162 =
																CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1049z00_50));
														}
														if (BgL_test1079z00_162)
															{	/* Init/extend.scm 37 */
																obj_t BgL_arg1046z00_47;
																obj_t BgL_arg1047z00_48;

																BgL_arg1046z00_47 =
																	CDR(((obj_t) BgL_hooksz00_41));
																BgL_arg1047z00_48 =
																	CDR(((obj_t) BgL_hnamesz00_42));
																{
																	obj_t BgL_hnamesz00_174;
																	obj_t BgL_hooksz00_173;

																	BgL_hooksz00_173 = BgL_arg1046z00_47;
																	BgL_hnamesz00_174 = BgL_arg1047z00_48;
																	BgL_hnamesz00_42 = BgL_hnamesz00_174;
																	BgL_hooksz00_41 = BgL_hooksz00_173;
																	goto BgL_zc3z04anonymousza31042ze3z87_43;
																}
															}
														else
															{	/* Init/extend.scm 37 */
																obj_t BgL_arg1048z00_49;

																BgL_arg1048z00_49 =
																	CAR(((obj_t) BgL_hnamesz00_42));
																BGl_internalzd2errorzd2zztools_errorz00
																	(BgL_fnamez00_34,
																	BGl_string1055z00zzinit_extendz00,
																	BgL_arg1048z00_49);
															}
													}
											}
										}
										{	/* Init/extend.scm 38 */
											obj_t BgL_envz00_54;

											BgL_envz00_54 = BGl_defaultzd2environmentzd2zz__evalz00();
											{	/* Init/extend.scm 38 */

												return
													BGl_loadqz00zz__evalz00(BgL_fnamez00_34,
													BgL_envz00_54);
											}
										}
									}
								else
									{	/* Init/extend.scm 35 */
										return
											BGl_errorz00zz__errorz00
											(BGl_string1057z00zzinit_extendz00,
											BGl_string1058z00zzinit_extendz00,
											BgL_extendzd2namezd2_3);
									}
							}
					}
				}
		}

	}



/* &load-extend */
	obj_t BGl_z62loadzd2extendzb0zzinit_extendz00(obj_t BgL_envz00_66,
		obj_t BgL_extendzd2namezd2_67)
	{
		{	/* Init/extend.scm 25 */
			return BGl_loadzd2extendzd2zzinit_extendz00(BgL_extendzd2namezd2_67);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzinit_extendz00(void)
	{
		{	/* Init/extend.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzinit_extendz00(void)
	{
		{	/* Init/extend.scm 16 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzinit_extendz00(void)
	{
		{	/* Init/extend.scm 16 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzinit_extendz00(void)
	{
		{	/* Init/extend.scm 16 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1059z00zzinit_extendz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1059z00zzinit_extendz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1059z00zzinit_extendz00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1059z00zzinit_extendz00));
		}

	}

#ifdef __cplusplus
}
#endif
