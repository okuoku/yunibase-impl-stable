/*===========================================================================*/
/*   (Init/main.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Init/main.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INIT_MAIN_TYPE_DEFINITIONS
#define BGL_INIT_MAIN_TYPE_DEFINITIONS
#endif													// BGL_INIT_MAIN_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62zc3z04anonymousza31030ze3ze5zzinit_mainz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzinit_mainz00 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzinit_mainz00(void);
	BGL_IMPORT obj_t unwind_stack_until(obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzinit_mainz00(void);
	static obj_t BGl_objectzd2initzd2zzinit_mainz00(void);
	BGL_EXPORTED_DECL obj_t bigloo_main(obj_t);
	static obj_t BGl_zc3z04exitza31028ze3ze70z60zzinit_mainz00(obj_t);
	static obj_t BGl_methodzd2initzd2zzinit_mainz00(void);
	static obj_t BGl_z62exitz62zzinit_mainz00(obj_t, obj_t);
	extern obj_t BGl_setupzd2defaultzd2valuesz00zzinit_setrcz00(void);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	static obj_t BGl_z62compilerzd2exitzb0zzinit_mainz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzinit_mainz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_enginez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinit_parsezd2argszd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinit_setrcz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bexitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_bigloozd2initializa7edz12z67zz__paramz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzinit_mainz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzinit_mainz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzinit_mainz00(void);
	static obj_t
		BGl_z62zc3z04za2compilerzd2exitza2za31022ze3z37zzinit_mainz00(obj_t, obj_t);
	static obj_t BGl_za2compilerzd2exitza2zd2zzinit_mainz00 = BUNSPEC;
	static obj_t BGl_z62mainz62zzinit_mainz00(obj_t, obj_t);
	extern obj_t BGl_stopzd2tracezd2zztools_tracez00(void);
	BGL_EXPORTED_DECL obj_t BGl_mainz00zzinit_mainz00(obj_t);
	extern obj_t BGl_parsezd2argszd2zzinit_parsezd2argszd2(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_compilerzd2exitzd2zzinit_mainz00(obj_t);
	BGL_IMPORT obj_t BGl_memberz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_enginez00zzengine_enginez00(void);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_compilerzd2exitzd2envz00zzinit_mainz00,
		BgL_bgl_za762compilerza7d2ex1036z00,
		BGl_z62compilerzd2exitzb0zzinit_mainz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_mainzd2envzd2zzinit_mainz00,
		BgL_bgl_za762mainza762za7za7init1037z00, BGl_z62mainz62zzinit_mainz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1032z00zzinit_mainz00,
		BgL_bgl_string1032za700za7za7i1038za7, "-q", 2);
	      DEFINE_STRING(BGl_string1034z00zzinit_mainz00,
		BgL_bgl_string1034za700za7za7i1039za7, "init_main", 9);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc1031z00zzinit_mainz00,
		BgL_bgl_za762za7c3za704za7a2comp1040z00,
		BGl_z62zc3z04za2compilerzd2exitza2za31022ze3z37zzinit_mainz00);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1033z00zzinit_mainz00,
		BgL_bgl_za762za7c3za704anonymo1041za7,
		BGl_z62zc3z04anonymousza31030ze3ze5zzinit_mainz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzinit_mainz00));
		     ADD_ROOT((void *) (&BGl_za2compilerzd2exitza2zd2zzinit_mainz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}


/* Libraries setup */
	static int bigloo_libinit(int argc, char *argv[], char *env[])
	{
		return 0;
	}


	long bigloo_abort(long n)
	{
		return n;
	}

	int BIGLOO_MAIN(int argc, char *argv[], char *env[])
	{
		return _bigloo_main(argc, argv, env, &bigloo_main, &bigloo_libinit, 0);
	}


/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzinit_mainz00(long
		BgL_checksumz00_53, char *BgL_fromz00_54)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzinit_mainz00))
				{
					BGl_requirezd2initializa7ationz75zzinit_mainz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzinit_mainz00();
					BGl_libraryzd2moduleszd2initz00zzinit_mainz00();
					BGl_importedzd2moduleszd2initz00zzinit_mainz00();
					return BGl_toplevelzd2initzd2zzinit_mainz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* bigloo_main */
	BGL_EXPORTED_DEF obj_t bigloo_main(obj_t BgL_argvz00_55)
	{
		{
			BGl_modulezd2initializa7ationz75zzinit_mainz00(0L, "init_main");
			BGl_bigloozd2initializa7edz12z67zz__paramz00();
			{
				obj_t BgL_tmpz00_66;

				BgL_tmpz00_66 = BGl_mainz00zzinit_mainz00(BgL_argvz00_55);
				return BIGLOO_EXIT(BgL_tmpz00_66);
			}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzinit_mainz00(void)
	{
		{	/* Init/main.scm 15 */
			BGl_modulezd2initializa7ationz75zz__bexitz00(0L, "init_main");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "init_main");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"init_main");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "init_main");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzinit_mainz00(void)
	{
		{	/* Init/main.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzinit_mainz00(void)
	{
		{	/* Init/main.scm 15 */
			return (BGl_za2compilerzd2exitza2zd2zzinit_mainz00 =
				BGl_proc1031z00zzinit_mainz00, BUNSPEC);
		}

	}



/* &<@*compiler-exit*:1022> */
	obj_t BGl_z62zc3z04za2compilerzd2exitza2za31022ze3z37zzinit_mainz00(obj_t
		BgL_envz00_39, obj_t BgL_xz00_40)
	{
		{	/* Init/main.scm 29 */
			{	/* Init/main.scm 29 */
				obj_t BgL_list1023z00_52;

				BgL_list1023z00_52 = MAKE_YOUNG_PAIR(BgL_xz00_40, BNIL);
				return BGl_exitz00zz__errorz00(BgL_list1023z00_52);
			}
		}

	}



/* main */
	BGL_EXPORTED_DEF obj_t BGl_mainz00zzinit_mainz00(obj_t BgL_argvz00_3)
	{
		{	/* Init/main.scm 34 */
			if (CBOOL(BGl_memberz00zz__r4_pairs_and_lists_6_3z00
					(BGl_string1032z00zzinit_mainz00, BgL_argvz00_3)))
				{	/* Init/main.scm 38 */
					BFALSE;
				}
			else
				{	/* Init/main.scm 38 */
					BGl_setupzd2defaultzd2valuesz00zzinit_setrcz00();
				}
			BGL_TAIL return
				BGl_zc3z04exitza31028ze3ze70z60zzinit_mainz00(BgL_argvz00_3);
		}

	}



/* <@exit:1028>~0 */
	obj_t BGl_zc3z04exitza31028ze3ze70z60zzinit_mainz00(obj_t BgL_argvz00_51)
	{
		{	/* Init/main.scm 39 */
			jmp_buf_t jmpbuf;
			void *BgL_an_exit1016z00_18;

			if (SET_EXIT(BgL_an_exit1016z00_18))
				{
					return BGL_EXIT_VALUE();
				}
			else
				{
#if( SIGSETJMP_SAVESIGS == 0 )
				 // MS: CARE 5 jan 2021: see runtime/Clib/csystem.c
				 // bgl_restore_signal_handlers();
#endif

					BgL_an_exit1016z00_18 = (void *) jmpbuf;
					{	/* Init/main.scm 39 */
						obj_t BgL_env1020z00_19;

						BgL_env1020z00_19 = BGL_CURRENT_DYNAMIC_ENV();
						PUSH_ENV_EXIT(BgL_env1020z00_19, BgL_an_exit1016z00_18, 1L);
						{	/* Init/main.scm 39 */
							obj_t BgL_an_exitd1017z00_20;

							BgL_an_exitd1017z00_20 =
								BGL_ENV_EXITD_TOP_AS_OBJ(BgL_env1020z00_19);
							{	/* Init/main.scm 39 */
								obj_t BgL_exitz00_42;

								{
									int BgL_tmpz00_86;

									BgL_tmpz00_86 = (int) (1L);
									BgL_exitz00_42 =
										MAKE_L_PROCEDURE(BGl_z62exitz62zzinit_mainz00,
										BgL_tmpz00_86);
								}
								PROCEDURE_L_SET(BgL_exitz00_42,
									(int) (0L), BgL_an_exitd1017z00_20);
								{	/* Init/main.scm 39 */
									obj_t BgL_res1019z00_23;

									BGl_za2compilerzd2exitza2zd2zzinit_mainz00 = BgL_exitz00_42;
									{	/* Init/main.scm 41 */
										obj_t BgL_exitd1012z00_24;

										BgL_exitd1012z00_24 = BGL_EXITD_TOP_AS_OBJ();
										{	/* Init/main.scm 41 */
											obj_t BgL_arg1828z00_31;

											{	/* Init/main.scm 41 */
												obj_t BgL_arg1829z00_32;

												BgL_arg1829z00_32 =
													BGL_EXITD_PROTECT(BgL_exitd1012z00_24);
												BgL_arg1828z00_31 =
													MAKE_YOUNG_PAIR(BGl_proc1033z00zzinit_mainz00,
													BgL_arg1829z00_32);
											}
											BGL_EXITD_PROTECT_SET(BgL_exitd1012z00_24,
												BgL_arg1828z00_31);
											BUNSPEC;
										}
										{	/* Init/main.scm 42 */
											obj_t BgL_tmp1014z00_26;

											if (CBOOL(BGl_parsezd2argszd2zzinit_parsezd2argszd2
													(BgL_argvz00_51)))
												{	/* Init/main.scm 42 */
													BgL_tmp1014z00_26 = BGl_enginez00zzengine_enginez00();
												}
											else
												{	/* Init/main.scm 42 */
													BgL_tmp1014z00_26 = BINT(-1L);
												}
											{	/* Init/main.scm 41 */
												bool_t BgL_test1045z00_100;

												{	/* Init/main.scm 41 */
													obj_t BgL_arg1827z00_34;

													BgL_arg1827z00_34 =
														BGL_EXITD_PROTECT(BgL_exitd1012z00_24);
													BgL_test1045z00_100 = PAIRP(BgL_arg1827z00_34);
												}
												if (BgL_test1045z00_100)
													{	/* Init/main.scm 41 */
														obj_t BgL_arg1825z00_35;

														{	/* Init/main.scm 41 */
															obj_t BgL_arg1826z00_36;

															BgL_arg1826z00_36 =
																BGL_EXITD_PROTECT(BgL_exitd1012z00_24);
															BgL_arg1825z00_35 =
																CDR(((obj_t) BgL_arg1826z00_36));
														}
														BGL_EXITD_PROTECT_SET(BgL_exitd1012z00_24,
															BgL_arg1825z00_35);
														BUNSPEC;
													}
												else
													{	/* Init/main.scm 41 */
														BFALSE;
													}
											}
											BGl_stopzd2tracezd2zztools_tracez00();
											BgL_res1019z00_23 = BgL_tmp1014z00_26;
										}
									}
									POP_ENV_EXIT(BgL_env1020z00_19);
									return BgL_res1019z00_23;
								}
							}
						}
					}
				}
		}

	}



/* &main */
	obj_t BGl_z62mainz62zzinit_mainz00(obj_t BgL_envz00_43, obj_t BgL_argvz00_44)
	{
		{	/* Init/main.scm 34 */
			return BGl_mainz00zzinit_mainz00(BgL_argvz00_44);
		}

	}



/* &<@anonymous:1030> */
	obj_t BGl_z62zc3z04anonymousza31030ze3ze5zzinit_mainz00(obj_t BgL_envz00_45)
	{
		{	/* Init/main.scm 41 */
			return BGl_stopzd2tracezd2zztools_tracez00();
		}

	}



/* &exit */
	obj_t BGl_z62exitz62zzinit_mainz00(obj_t BgL_envz00_46,
		obj_t BgL_val1018z00_48)
	{
		{	/* Init/main.scm 39 */
			return
				unwind_stack_until(PROCEDURE_L_REF(BgL_envz00_46,
					(int) (0L)), BFALSE, BgL_val1018z00_48, BFALSE, BFALSE);
		}

	}



/* compiler-exit */
	BGL_EXPORTED_DEF obj_t BGl_compilerzd2exitzd2zzinit_mainz00(obj_t
		BgL_valuez00_4)
	{
		{	/* Init/main.scm 50 */
			return
				((obj_t(*)(obj_t,
						obj_t))
				PROCEDURE_L_ENTRY(BGl_za2compilerzd2exitza2zd2zzinit_mainz00))
				(BGl_za2compilerzd2exitza2zd2zzinit_mainz00, BgL_valuez00_4);
		}

	}



/* &compiler-exit */
	obj_t BGl_z62compilerzd2exitzb0zzinit_mainz00(obj_t BgL_envz00_49,
		obj_t BgL_valuez00_50)
	{
		{	/* Init/main.scm 50 */
			return BGl_compilerzd2exitzd2zzinit_mainz00(BgL_valuez00_50);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzinit_mainz00(void)
	{
		{	/* Init/main.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzinit_mainz00(void)
	{
		{	/* Init/main.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzinit_mainz00(void)
	{
		{	/* Init/main.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzinit_mainz00(void)
	{
		{	/* Init/main.scm 15 */
			BGl_modulezd2initializa7ationz75zzinit_setrcz00(32737986L,
				BSTRING_TO_STRING(BGl_string1034z00zzinit_mainz00));
			BGl_modulezd2initializa7ationz75zzinit_parsezd2argszd2(421321152L,
				BSTRING_TO_STRING(BGl_string1034z00zzinit_mainz00));
			BGl_modulezd2initializa7ationz75zzengine_enginez00(373986149L,
				BSTRING_TO_STRING(BGl_string1034z00zzinit_mainz00));
			return
				BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1034z00zzinit_mainz00));
		}

	}

#ifdef __cplusplus
}
#endif
