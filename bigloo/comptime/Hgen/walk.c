/*===========================================================================*/
/*   (Hgen/walk.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Hgen/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_HGEN_WALK_TYPE_DEFINITIONS
#define BGL_HGEN_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;


#endif													// BGL_HGEN_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzhgen_walkz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_za2czd2portza2zd2zzbackend_c_emitz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	static obj_t BGl_z62zc3z04anonymousza31310ze3ze5zzhgen_walkz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_stopzd2emissionz12zc0zzbackend_c_emitz00(void);
	static obj_t BGl_genericzd2initzd2zzhgen_walkz00(void);
	static obj_t BGl_objectzd2initzd2zzhgen_walkz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62hgenzd2walkzb0zzhgen_walkz00(obj_t);
	extern obj_t BGl_emitzd2classzd2typesz00zzbackend_c_prototypez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_fprintfz00zz__r4_output_6_10_3z00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_startzd2emissionz12zc0zzbackend_c_emitz00(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzhgen_walkz00(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	BGL_EXPORTED_DECL obj_t BGl_hgenzd2walkzd2zzhgen_walkz00(void);
	BGL_IMPORT obj_t BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(obj_t);
	BGL_IMPORT bool_t BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00(obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzhgen_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzhgen_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzhgen_walkz00(void);
	extern obj_t BGl_emitzd2headerzd2zzbackend_c_emitz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzhgen_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzhgen_walkz00(void);
	BGL_IMPORT obj_t bigloo_mangle(obj_t);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	static obj_t BGl_modulezd2ze3idz31zzhgen_walkz00(obj_t);
	extern obj_t BGl_getzd2classzd2listz00zzobject_classz00(void);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[2];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_hgenzd2walkzd2envz00zzhgen_walkz00,
		BgL_bgl_za762hgenza7d2walkza7b1592za7, BGl_z62hgenzd2walkzb0zzhgen_walkz00,
		0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1580z00zzhgen_walkz00,
		BgL_bgl_string1580za700za7za7h1593za7, "C headers generation", 20);
	      DEFINE_STRING(BGl_string1581z00zzhgen_walkz00,
		BgL_bgl_string1581za700za7za7h1594za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1583z00zzhgen_walkz00,
		BgL_bgl_string1583za700za7za7h1595za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1584z00zzhgen_walkz00,
		BgL_bgl_string1584za700za7za7h1596za7, "#ifndef __BGL_~a_H\n", 19);
	      DEFINE_STRING(BGl_string1585z00zzhgen_walkz00,
		BgL_bgl_string1585za700za7za7h1597za7, "#define __BGL_~a_H\n", 19);
	      DEFINE_STRING(BGl_string1586z00zzhgen_walkz00,
		BgL_bgl_string1586za700za7za7h1598za7, "#endif // __BGL_~a_H\n", 21);
	      DEFINE_STRING(BGl_string1587z00zzhgen_walkz00,
		BgL_bgl_string1587za700za7za7h1599za7, ".h", 2);
	      DEFINE_STRING(BGl_string1588z00zzhgen_walkz00,
		BgL_bgl_string1588za700za7za7h1600za7, "hgen_walk", 9);
	      DEFINE_STRING(BGl_string1589z00zzhgen_walkz00,
		BgL_bgl_string1589za700za7za7h1601za7,
		"pass-started ((lambda () (start-emission! \".h\"))) ", 50);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1582z00zzhgen_walkz00,
		BgL_bgl_za762za7c3za704anonymo1602za7,
		BGl_z62zc3z04anonymousza31310ze3ze5zzhgen_walkz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzhgen_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzhgen_walkz00(long
		BgL_checksumz00_1917, char *BgL_fromz00_1918)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzhgen_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzhgen_walkz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzhgen_walkz00();
					BGl_libraryzd2moduleszd2initz00zzhgen_walkz00();
					BGl_cnstzd2initzd2zzhgen_walkz00();
					BGl_importedzd2moduleszd2initz00zzhgen_walkz00();
					return BGl_methodzd2initzd2zzhgen_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzhgen_walkz00(void)
	{
		{	/* Hgen/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "hgen_walk");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "hgen_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "hgen_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "hgen_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "hgen_walk");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "hgen_walk");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "hgen_walk");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"hgen_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "hgen_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"hgen_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"hgen_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzhgen_walkz00(void)
	{
		{	/* Hgen/walk.scm 15 */
			{	/* Hgen/walk.scm 15 */
				obj_t BgL_cportz00_1906;

				{	/* Hgen/walk.scm 15 */
					obj_t BgL_stringz00_1913;

					BgL_stringz00_1913 = BGl_string1589z00zzhgen_walkz00;
					{	/* Hgen/walk.scm 15 */
						obj_t BgL_startz00_1914;

						BgL_startz00_1914 = BINT(0L);
						{	/* Hgen/walk.scm 15 */
							obj_t BgL_endz00_1915;

							BgL_endz00_1915 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1913)));
							{	/* Hgen/walk.scm 15 */

								BgL_cportz00_1906 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1913, BgL_startz00_1914, BgL_endz00_1915);
				}}}}
				{
					long BgL_iz00_1907;

					BgL_iz00_1907 = 1L;
				BgL_loopz00_1908:
					if ((BgL_iz00_1907 == -1L))
						{	/* Hgen/walk.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Hgen/walk.scm 15 */
							{	/* Hgen/walk.scm 15 */
								obj_t BgL_arg1591z00_1909;

								{	/* Hgen/walk.scm 15 */

									{	/* Hgen/walk.scm 15 */
										obj_t BgL_locationz00_1911;

										BgL_locationz00_1911 = BBOOL(((bool_t) 0));
										{	/* Hgen/walk.scm 15 */

											BgL_arg1591z00_1909 =
												BGl_readz00zz__readerz00(BgL_cportz00_1906,
												BgL_locationz00_1911);
										}
									}
								}
								{	/* Hgen/walk.scm 15 */
									int BgL_tmpz00_1947;

									BgL_tmpz00_1947 = (int) (BgL_iz00_1907);
									CNST_TABLE_SET(BgL_tmpz00_1947, BgL_arg1591z00_1909);
							}}
							{	/* Hgen/walk.scm 15 */
								int BgL_auxz00_1912;

								BgL_auxz00_1912 = (int) ((BgL_iz00_1907 - 1L));
								{
									long BgL_iz00_1952;

									BgL_iz00_1952 = (long) (BgL_auxz00_1912);
									BgL_iz00_1907 = BgL_iz00_1952;
									goto BgL_loopz00_1908;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzhgen_walkz00(void)
	{
		{	/* Hgen/walk.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* hgen-walk */
	BGL_EXPORTED_DEF obj_t BGl_hgenzd2walkzd2zzhgen_walkz00(void)
	{
		{	/* Hgen/walk.scm 30 */
			{	/* Hgen/walk.scm 31 */
				obj_t BgL_list1269z00_1587;

				{	/* Hgen/walk.scm 31 */
					obj_t BgL_arg1272z00_1588;

					{	/* Hgen/walk.scm 31 */
						obj_t BgL_arg1284z00_1589;

						BgL_arg1284z00_1589 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1272z00_1588 =
							MAKE_YOUNG_PAIR(BGl_string1580z00zzhgen_walkz00,
							BgL_arg1284z00_1589);
					}
					BgL_list1269z00_1587 =
						MAKE_YOUNG_PAIR(BGl_string1581z00zzhgen_walkz00,
						BgL_arg1272z00_1588);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1269z00_1587);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1580z00zzhgen_walkz00;
			{	/* Hgen/walk.scm 31 */
				obj_t BgL_g1110z00_1590;
				obj_t BgL_g1111z00_1591;

				{	/* Hgen/walk.scm 31 */
					obj_t BgL_list1309z00_1605;

					BgL_list1309z00_1605 =
						MAKE_YOUNG_PAIR(BGl_proc1582z00zzhgen_walkz00, BNIL);
					BgL_g1110z00_1590 = BgL_list1309z00_1605;
				}
				BgL_g1111z00_1591 = CNST_TABLE_REF(0);
				{
					obj_t BgL_hooksz00_1593;
					obj_t BgL_hnamesz00_1594;

					BgL_hooksz00_1593 = BgL_g1110z00_1590;
					BgL_hnamesz00_1594 = BgL_g1111z00_1591;
				BgL_zc3z04anonymousza31285ze3z87_1595:
					if (NULLP(BgL_hooksz00_1593))
						{	/* Hgen/walk.scm 31 */
							CNST_TABLE_REF(1);
						}
					else
						{	/* Hgen/walk.scm 31 */
							bool_t BgL_test1606z00_1967;

							{	/* Hgen/walk.scm 31 */
								obj_t BgL_fun1307z00_1602;

								BgL_fun1307z00_1602 = CAR(((obj_t) BgL_hooksz00_1593));
								BgL_test1606z00_1967 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1307z00_1602));
							}
							if (BgL_test1606z00_1967)
								{	/* Hgen/walk.scm 31 */
									obj_t BgL_arg1304z00_1599;
									obj_t BgL_arg1305z00_1600;

									BgL_arg1304z00_1599 = CDR(((obj_t) BgL_hooksz00_1593));
									BgL_arg1305z00_1600 = CDR(((obj_t) BgL_hnamesz00_1594));
									{
										obj_t BgL_hnamesz00_1979;
										obj_t BgL_hooksz00_1978;

										BgL_hooksz00_1978 = BgL_arg1304z00_1599;
										BgL_hnamesz00_1979 = BgL_arg1305z00_1600;
										BgL_hnamesz00_1594 = BgL_hnamesz00_1979;
										BgL_hooksz00_1593 = BgL_hooksz00_1978;
										goto BgL_zc3z04anonymousza31285ze3z87_1595;
									}
								}
							else
								{	/* Hgen/walk.scm 31 */
									obj_t BgL_arg1306z00_1601;

									BgL_arg1306z00_1601 = CAR(((obj_t) BgL_hnamesz00_1594));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1580z00zzhgen_walkz00,
										BGl_string1583z00zzhgen_walkz00, BgL_arg1306z00_1601);
								}
						}
				}
			}
			BGl_emitzd2headerzd2zzbackend_c_emitz00();
			{	/* Hgen/walk.scm 36 */
				obj_t BgL_clistz00_1608;

				{	/* Hgen/walk.scm 36 */
					obj_t BgL_hook1249z00_1613;

					BgL_hook1249z00_1613 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
					{	/* Hgen/walk.scm 40 */
						obj_t BgL_g1250z00_1614;

						BgL_g1250z00_1614 = BGl_getzd2classzd2listz00zzobject_classz00();
						{
							obj_t BgL_l1246z00_1616;
							obj_t BgL_h1247z00_1617;

							BgL_l1246z00_1616 = BgL_g1250z00_1614;
							BgL_h1247z00_1617 = BgL_hook1249z00_1613;
						BgL_zc3z04anonymousza31314ze3z87_1618:
							if (NULLP(BgL_l1246z00_1616))
								{	/* Hgen/walk.scm 40 */
									BgL_clistz00_1608 = CDR(BgL_hook1249z00_1613);
								}
							else
								{	/* Hgen/walk.scm 40 */
									bool_t BgL_test1608z00_1989;

									{	/* Hgen/walk.scm 37 */
										obj_t BgL_cz00_1629;

										BgL_cz00_1629 = CAR(((obj_t) BgL_l1246z00_1616));
										{	/* Hgen/walk.scm 37 */
											bool_t BgL_test1609z00_1992;

											{	/* Hgen/walk.scm 37 */
												obj_t BgL_classz00_1824;

												BgL_classz00_1824 = BGl_tclassz00zzobject_classz00;
												if (BGL_OBJECTP(BgL_cz00_1629))
													{	/* Hgen/walk.scm 37 */
														BgL_objectz00_bglt BgL_arg1807z00_1826;

														BgL_arg1807z00_1826 =
															(BgL_objectz00_bglt) (BgL_cz00_1629);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Hgen/walk.scm 37 */
																long BgL_idxz00_1832;

																BgL_idxz00_1832 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_1826);
																BgL_test1609z00_1992 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_1832 + 2L)) ==
																	BgL_classz00_1824);
															}
														else
															{	/* Hgen/walk.scm 37 */
																bool_t BgL_res1578z00_1857;

																{	/* Hgen/walk.scm 37 */
																	obj_t BgL_oclassz00_1840;

																	{	/* Hgen/walk.scm 37 */
																		obj_t BgL_arg1815z00_1848;
																		long BgL_arg1816z00_1849;

																		BgL_arg1815z00_1848 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Hgen/walk.scm 37 */
																			long BgL_arg1817z00_1850;

																			BgL_arg1817z00_1850 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_1826);
																			BgL_arg1816z00_1849 =
																				(BgL_arg1817z00_1850 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_1840 =
																			VECTOR_REF(BgL_arg1815z00_1848,
																			BgL_arg1816z00_1849);
																	}
																	{	/* Hgen/walk.scm 37 */
																		bool_t BgL__ortest_1115z00_1841;

																		BgL__ortest_1115z00_1841 =
																			(BgL_classz00_1824 == BgL_oclassz00_1840);
																		if (BgL__ortest_1115z00_1841)
																			{	/* Hgen/walk.scm 37 */
																				BgL_res1578z00_1857 =
																					BgL__ortest_1115z00_1841;
																			}
																		else
																			{	/* Hgen/walk.scm 37 */
																				long BgL_odepthz00_1842;

																				{	/* Hgen/walk.scm 37 */
																					obj_t BgL_arg1804z00_1843;

																					BgL_arg1804z00_1843 =
																						(BgL_oclassz00_1840);
																					BgL_odepthz00_1842 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_1843);
																				}
																				if ((2L < BgL_odepthz00_1842))
																					{	/* Hgen/walk.scm 37 */
																						obj_t BgL_arg1802z00_1845;

																						{	/* Hgen/walk.scm 37 */
																							obj_t BgL_arg1803z00_1846;

																							BgL_arg1803z00_1846 =
																								(BgL_oclassz00_1840);
																							BgL_arg1802z00_1845 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_1846, 2L);
																						}
																						BgL_res1578z00_1857 =
																							(BgL_arg1802z00_1845 ==
																							BgL_classz00_1824);
																					}
																				else
																					{	/* Hgen/walk.scm 37 */
																						BgL_res1578z00_1857 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test1609z00_1992 = BgL_res1578z00_1857;
															}
													}
												else
													{	/* Hgen/walk.scm 37 */
														BgL_test1609z00_1992 = ((bool_t) 0);
													}
											}
											if (BgL_test1609z00_1992)
												{	/* Hgen/walk.scm 38 */
													obj_t BgL_arg1322z00_1631;

													{	/* Hgen/walk.scm 38 */
														BgL_globalz00_bglt BgL_arg1323z00_1632;

														{
															BgL_tclassz00_bglt BgL_auxz00_2015;

															{
																obj_t BgL_auxz00_2016;

																{	/* Hgen/walk.scm 38 */
																	BgL_objectz00_bglt BgL_tmpz00_2017;

																	BgL_tmpz00_2017 =
																		((BgL_objectz00_bglt)
																		((BgL_typez00_bglt) BgL_cz00_1629));
																	BgL_auxz00_2016 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2017);
																}
																BgL_auxz00_2015 =
																	((BgL_tclassz00_bglt) BgL_auxz00_2016);
															}
															BgL_arg1323z00_1632 =
																(((BgL_tclassz00_bglt)
																	COBJECT(BgL_auxz00_2015))->BgL_holderz00);
														}
														BgL_arg1322z00_1631 =
															(((BgL_globalz00_bglt)
																COBJECT(BgL_arg1323z00_1632))->BgL_modulez00);
													}
													BgL_test1608z00_1989 =
														(BgL_arg1322z00_1631 ==
														BGl_za2moduleza2z00zzmodule_modulez00);
												}
											else
												{	/* Hgen/walk.scm 37 */
													BgL_test1608z00_1989 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test1608z00_1989)
										{	/* Hgen/walk.scm 40 */
											obj_t BgL_nh1248z00_1625;

											{	/* Hgen/walk.scm 40 */
												obj_t BgL_arg1320z00_1627;

												BgL_arg1320z00_1627 = CAR(((obj_t) BgL_l1246z00_1616));
												BgL_nh1248z00_1625 =
													MAKE_YOUNG_PAIR(BgL_arg1320z00_1627, BNIL);
											}
											SET_CDR(BgL_h1247z00_1617, BgL_nh1248z00_1625);
											{	/* Hgen/walk.scm 40 */
												obj_t BgL_arg1319z00_1626;

												BgL_arg1319z00_1626 = CDR(((obj_t) BgL_l1246z00_1616));
												{
													obj_t BgL_h1247z00_2032;
													obj_t BgL_l1246z00_2031;

													BgL_l1246z00_2031 = BgL_arg1319z00_1626;
													BgL_h1247z00_2032 = BgL_nh1248z00_1625;
													BgL_h1247z00_1617 = BgL_h1247z00_2032;
													BgL_l1246z00_1616 = BgL_l1246z00_2031;
													goto BgL_zc3z04anonymousza31314ze3z87_1618;
												}
											}
										}
									else
										{	/* Hgen/walk.scm 40 */
											obj_t BgL_arg1321z00_1628;

											BgL_arg1321z00_1628 = CDR(((obj_t) BgL_l1246z00_1616));
											{
												obj_t BgL_l1246z00_2035;

												BgL_l1246z00_2035 = BgL_arg1321z00_1628;
												BgL_l1246z00_1616 = BgL_l1246z00_2035;
												goto BgL_zc3z04anonymousza31314ze3z87_1618;
											}
										}
								}
						}
					}
				}
				{	/* Hgen/walk.scm 41 */
					obj_t BgL_namez00_1609;

					BgL_namez00_1609 =
						BGl_modulezd2ze3idz31zzhgen_walkz00
						(BGl_za2moduleza2z00zzmodule_modulez00);
					bgl_display_char(((unsigned char) 10),
						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					{	/* Hgen/walk.scm 43 */
						obj_t BgL_list1311z00_1610;

						BgL_list1311z00_1610 = MAKE_YOUNG_PAIR(BgL_namez00_1609, BNIL);
						BGl_fprintfz00zz__r4_output_6_10_3z00
							(BGl_za2czd2portza2zd2zzbackend_c_emitz00,
							BGl_string1584z00zzhgen_walkz00, BgL_list1311z00_1610);
					}
					{	/* Hgen/walk.scm 44 */
						obj_t BgL_list1312z00_1611;

						BgL_list1312z00_1611 = MAKE_YOUNG_PAIR(BgL_namez00_1609, BNIL);
						BGl_fprintfz00zz__r4_output_6_10_3z00
							(BGl_za2czd2portza2zd2zzbackend_c_emitz00,
							BGl_string1585z00zzhgen_walkz00, BgL_list1312z00_1611);
					}
					BGl_emitzd2classzd2typesz00zzbackend_c_prototypez00(BgL_clistz00_1608,
						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					bgl_display_char(((unsigned char) 10),
						BGl_za2czd2portza2zd2zzbackend_c_emitz00);
					{	/* Hgen/walk.scm 47 */
						obj_t BgL_list1313z00_1612;

						BgL_list1313z00_1612 = MAKE_YOUNG_PAIR(BgL_namez00_1609, BNIL);
						BGl_fprintfz00zz__r4_output_6_10_3z00
							(BGl_za2czd2portza2zd2zzbackend_c_emitz00,
							BGl_string1586z00zzhgen_walkz00, BgL_list1313z00_1612);
			}}}
			BGL_TAIL return BGl_stopzd2emissionz12zc0zzbackend_c_emitz00();
		}

	}



/* &hgen-walk */
	obj_t BGl_z62hgenzd2walkzb0zzhgen_walkz00(obj_t BgL_envz00_1904)
	{
		{	/* Hgen/walk.scm 30 */
			return BGl_hgenzd2walkzd2zzhgen_walkz00();
		}

	}



/* &<@anonymous:1310> */
	obj_t BGl_z62zc3z04anonymousza31310ze3ze5zzhgen_walkz00(obj_t BgL_envz00_1905)
	{
		{	/* Hgen/walk.scm 31 */
			return
				BGl_startzd2emissionz12zc0zzbackend_c_emitz00
				(BGl_string1587z00zzhgen_walkz00);
		}

	}



/* module->id */
	obj_t BGl_modulezd2ze3idz31zzhgen_walkz00(obj_t BgL_modz00_3)
	{
		{	/* Hgen/walk.scm 53 */
			{	/* Hgen/walk.scm 54 */
				obj_t BgL_namez00_1634;

				{	/* Hgen/walk.scm 54 */
					obj_t BgL_arg1325z00_1636;

					BgL_arg1325z00_1636 = SYMBOL_TO_STRING(((obj_t) BgL_modz00_3));
					BgL_namez00_1634 =
						BGl_stringzd2upcasezd2zz__r4_strings_6_7z00(BgL_arg1325z00_1636);
				}
				if (BGl_bigloozd2needzd2manglingzf3zf3zz__biglooz00(BgL_namez00_1634))
					{	/* Hgen/walk.scm 55 */
						return bigloo_mangle(BgL_namez00_1634);
					}
				else
					{	/* Hgen/walk.scm 55 */
						return BgL_namez00_1634;
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzhgen_walkz00(void)
	{
		{	/* Hgen/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzhgen_walkz00(void)
	{
		{	/* Hgen/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzhgen_walkz00(void)
	{
		{	/* Hgen/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzhgen_walkz00(void)
	{
		{	/* Hgen/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1588z00zzhgen_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1588z00zzhgen_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1588z00zzhgen_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1588z00zzhgen_walkz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1588z00zzhgen_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1588z00zzhgen_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1588z00zzhgen_walkz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1588z00zzhgen_walkz00));
			BGl_modulezd2initializa7ationz75zzbackend_c_emitz00(474089076L,
				BSTRING_TO_STRING(BGl_string1588z00zzhgen_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(364917963L,
				BSTRING_TO_STRING(BGl_string1588z00zzhgen_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
