/*===========================================================================*/
/*   (SawMill/elsewhere.scm)                                                 */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/elsewhere.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_ELSEWHERE_TYPE_DEFINITIONS
#define BGL_SAW_ELSEWHERE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_scnstz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_nodez00;
		obj_t BgL_classz00;
		obj_t BgL_locz00;
	}               *BgL_scnstz00_bglt;


#endif													// BGL_SAW_ELSEWHERE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_needzd2functionzd2pointerz00zzsaw_elsewherez00(BgL_globalz00_bglt);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_elsewherez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62needzd2functionzd2pointerz62zzsaw_elsewherez00(obj_t,
		obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_elsewherez00(void);
	static obj_t BGl_objectzd2initzd2zzsaw_elsewherez00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_elsewherez00(void);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_elsewherez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzsaw_elsewherez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_elsewherez00(void);
	extern bool_t
		BGl_requirezd2prototypezf3z21zzbackend_c_prototypez00(BgL_globalz00_bglt);
	extern obj_t BGl_scnstz00zzast_varz00;
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_elsewherez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_elsewherez00(void);
	static obj_t __cnst[1];


	   
		 
		DEFINE_STRING(BGl_string1367z00zzsaw_elsewherez00,
		BgL_bgl_string1367za700za7za7s1371za7, "saw_elsewhere", 13);
	      DEFINE_STRING(BGl_string1368z00zzsaw_elsewherez00,
		BgL_bgl_string1368za700za7za7s1372za7, "(sfun sgfun slfun) ", 19);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_needzd2functionzd2pointerzd2envzd2zzsaw_elsewherez00,
		BgL_bgl_za762needza7d2functi1373z00,
		BGl_z62needzd2functionzd2pointerz62zzsaw_elsewherez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_elsewherez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_elsewherez00(long
		BgL_checksumz00_1614, char *BgL_fromz00_1615)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_elsewherez00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_elsewherez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_elsewherez00();
					BGl_libraryzd2moduleszd2initz00zzsaw_elsewherez00();
					BGl_cnstzd2initzd2zzsaw_elsewherez00();
					BGl_importedzd2moduleszd2initz00zzsaw_elsewherez00();
					return BGl_methodzd2initzd2zzsaw_elsewherez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_elsewherez00(void)
	{
		{	/* SawMill/elsewhere.scm 1 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_elsewhere");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_elsewhere");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_elsewhere");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_elsewhere");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_elsewhere");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_elsewhere");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_elsewhere");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_elsewhere");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_elsewherez00(void)
	{
		{	/* SawMill/elsewhere.scm 1 */
			{	/* SawMill/elsewhere.scm 1 */
				obj_t BgL_cportz00_1603;

				{	/* SawMill/elsewhere.scm 1 */
					obj_t BgL_stringz00_1610;

					BgL_stringz00_1610 = BGl_string1368z00zzsaw_elsewherez00;
					{	/* SawMill/elsewhere.scm 1 */
						obj_t BgL_startz00_1611;

						BgL_startz00_1611 = BINT(0L);
						{	/* SawMill/elsewhere.scm 1 */
							obj_t BgL_endz00_1612;

							BgL_endz00_1612 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1610)));
							{	/* SawMill/elsewhere.scm 1 */

								BgL_cportz00_1603 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1610, BgL_startz00_1611, BgL_endz00_1612);
				}}}}
				{
					long BgL_iz00_1604;

					BgL_iz00_1604 = 0L;
				BgL_loopz00_1605:
					if ((BgL_iz00_1604 == -1L))
						{	/* SawMill/elsewhere.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawMill/elsewhere.scm 1 */
							{	/* SawMill/elsewhere.scm 1 */
								obj_t BgL_arg1370z00_1606;

								{	/* SawMill/elsewhere.scm 1 */

									{	/* SawMill/elsewhere.scm 1 */
										obj_t BgL_locationz00_1608;

										BgL_locationz00_1608 = BBOOL(((bool_t) 0));
										{	/* SawMill/elsewhere.scm 1 */

											BgL_arg1370z00_1606 =
												BGl_readz00zz__readerz00(BgL_cportz00_1603,
												BgL_locationz00_1608);
										}
									}
								}
								{	/* SawMill/elsewhere.scm 1 */
									int BgL_tmpz00_1641;

									BgL_tmpz00_1641 = (int) (BgL_iz00_1604);
									CNST_TABLE_SET(BgL_tmpz00_1641, BgL_arg1370z00_1606);
							}}
							{	/* SawMill/elsewhere.scm 1 */
								int BgL_auxz00_1609;

								BgL_auxz00_1609 = (int) ((BgL_iz00_1604 - 1L));
								{
									long BgL_iz00_1646;

									BgL_iz00_1646 = (long) (BgL_auxz00_1609);
									BgL_iz00_1604 = BgL_iz00_1646;
									goto BgL_loopz00_1605;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_elsewherez00(void)
	{
		{	/* SawMill/elsewhere.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* need-function-pointer */
	BGL_EXPORTED_DEF obj_t
		BGl_needzd2functionzd2pointerz00zzsaw_elsewherez00(BgL_globalz00_bglt
		BgL_varz00_3)
	{
		{	/* SawMill/elsewhere.scm 7 */
			{	/* SawMill/elsewhere.scm 8 */
				BgL_valuez00_bglt BgL_valz00_1362;

				BgL_valz00_1362 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_3)))->BgL_valuez00);
				{	/* SawMill/elsewhere.scm 9 */
					bool_t BgL_test1384z00_1651;

					{	/* SawMill/elsewhere.scm 9 */
						obj_t BgL_arg1229z00_1367;

						BgL_arg1229z00_1367 =
							(((BgL_globalz00_bglt) COBJECT(BgL_varz00_3))->BgL_modulez00);
						BgL_test1384z00_1651 =
							(BgL_arg1229z00_1367 == BGl_za2moduleza2z00zzmodule_modulez00);
					}
					if (BgL_test1384z00_1651)
						{	/* SawMill/elsewhere.scm 9 */
							if (BGl_requirezd2prototypezf3z21zzbackend_c_prototypez00
								(BgL_varz00_3))
								{	/* SawMill/elsewhere.scm 11 */
									bool_t BgL_test1386z00_1656;

									{	/* SawMill/elsewhere.scm 11 */
										obj_t BgL_classz00_1532;

										BgL_classz00_1532 = BGl_scnstz00zzast_varz00;
										{	/* SawMill/elsewhere.scm 11 */
											BgL_objectz00_bglt BgL_arg1807z00_1534;

											{	/* SawMill/elsewhere.scm 11 */
												obj_t BgL_tmpz00_1657;

												BgL_tmpz00_1657 =
													((obj_t) ((BgL_objectz00_bglt) BgL_valz00_1362));
												BgL_arg1807z00_1534 =
													(BgL_objectz00_bglt) (BgL_tmpz00_1657);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawMill/elsewhere.scm 11 */
													long BgL_idxz00_1540;

													BgL_idxz00_1540 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1534);
													BgL_test1386z00_1656 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_1540 + 2L)) == BgL_classz00_1532);
												}
											else
												{	/* SawMill/elsewhere.scm 11 */
													bool_t BgL_res1365z00_1565;

													{	/* SawMill/elsewhere.scm 11 */
														obj_t BgL_oclassz00_1548;

														{	/* SawMill/elsewhere.scm 11 */
															obj_t BgL_arg1815z00_1556;
															long BgL_arg1816z00_1557;

															BgL_arg1815z00_1556 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawMill/elsewhere.scm 11 */
																long BgL_arg1817z00_1558;

																BgL_arg1817z00_1558 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1534);
																BgL_arg1816z00_1557 =
																	(BgL_arg1817z00_1558 - OBJECT_TYPE);
															}
															BgL_oclassz00_1548 =
																VECTOR_REF(BgL_arg1815z00_1556,
																BgL_arg1816z00_1557);
														}
														{	/* SawMill/elsewhere.scm 11 */
															bool_t BgL__ortest_1115z00_1549;

															BgL__ortest_1115z00_1549 =
																(BgL_classz00_1532 == BgL_oclassz00_1548);
															if (BgL__ortest_1115z00_1549)
																{	/* SawMill/elsewhere.scm 11 */
																	BgL_res1365z00_1565 =
																		BgL__ortest_1115z00_1549;
																}
															else
																{	/* SawMill/elsewhere.scm 11 */
																	long BgL_odepthz00_1550;

																	{	/* SawMill/elsewhere.scm 11 */
																		obj_t BgL_arg1804z00_1551;

																		BgL_arg1804z00_1551 = (BgL_oclassz00_1548);
																		BgL_odepthz00_1550 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_1551);
																	}
																	if ((2L < BgL_odepthz00_1550))
																		{	/* SawMill/elsewhere.scm 11 */
																			obj_t BgL_arg1802z00_1553;

																			{	/* SawMill/elsewhere.scm 11 */
																				obj_t BgL_arg1803z00_1554;

																				BgL_arg1803z00_1554 =
																					(BgL_oclassz00_1548);
																				BgL_arg1802z00_1553 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_1554, 2L);
																			}
																			BgL_res1365z00_1565 =
																				(BgL_arg1802z00_1553 ==
																				BgL_classz00_1532);
																		}
																	else
																		{	/* SawMill/elsewhere.scm 11 */
																			BgL_res1365z00_1565 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1386z00_1656 = BgL_res1365z00_1565;
												}
										}
									}
									if (BgL_test1386z00_1656)
										{	/* SawMill/elsewhere.scm 11 */
											return
												BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
												(((BgL_scnstz00_bglt) COBJECT(
															((BgL_scnstz00_bglt) BgL_valz00_1362)))->
													BgL_classz00), CNST_TABLE_REF(0));
										}
									else
										{	/* SawMill/elsewhere.scm 11 */
											return BFALSE;
										}
								}
							else
								{	/* SawMill/elsewhere.scm 10 */
									return BFALSE;
								}
						}
					else
						{	/* SawMill/elsewhere.scm 9 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &need-function-pointer */
	obj_t BGl_z62needzd2functionzd2pointerz62zzsaw_elsewherez00(obj_t
		BgL_envz00_1601, obj_t BgL_varz00_1602)
	{
		{	/* SawMill/elsewhere.scm 7 */
			return
				BGl_needzd2functionzd2pointerz00zzsaw_elsewherez00(
				((BgL_globalz00_bglt) BgL_varz00_1602));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_elsewherez00(void)
	{
		{	/* SawMill/elsewhere.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_elsewherez00(void)
	{
		{	/* SawMill/elsewhere.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_elsewherez00(void)
	{
		{	/* SawMill/elsewhere.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_elsewherez00(void)
	{
		{	/* SawMill/elsewhere.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1367z00zzsaw_elsewherez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1367z00zzsaw_elsewherez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1367z00zzsaw_elsewherez00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1367z00zzsaw_elsewherez00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(364917963L,
				BSTRING_TO_STRING(BGl_string1367z00zzsaw_elsewherez00));
		}

	}

#ifdef __cplusplus
}
#endif
