/*===========================================================================*/
/*   (SawMill/lib.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/lib.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_LIB_TYPE_DEFINITIONS
#define BGL_SAW_LIB_TYPE_DEFINITIONS
#endif													// BGL_SAW_LIB_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzsaw_libz00 = BUNSPEC;
	static obj_t BGl_genericzd2initzd2zzsaw_libz00(void);
	static obj_t BGl_objectzd2initzd2zzsaw_libz00(void);
	static obj_t BGl_walkze70ze7zzsaw_libz00(obj_t, obj_t, obj_t);
	static obj_t BGl_walkze71ze7zzsaw_libz00(obj_t, obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzsaw_libz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_libz00(void);
	static obj_t BGl_z62substzd2appendzb0zzsaw_libz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_EXPORTED_DECL obj_t BGl_substz00zzsaw_libz00(obj_t, obj_t, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_libz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_libz00(void);
	static obj_t BGl_z62substz62zzsaw_libz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_substzd2appendzd2zzsaw_libz00(obj_t, obj_t,
		obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_substzd2appendzd2envz00zzsaw_libz00,
		BgL_bgl_za762substza7d2appen1027z00, BGl_z62substzd2appendzb0zzsaw_libz00,
		0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_substzd2envzd2zzsaw_libz00,
		BgL_bgl_za762substza762za7za7saw1028z00, BGl_z62substz62zzsaw_libz00, 0L,
		BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_libz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long
		BgL_checksumz00_59, char *BgL_fromz00_60)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_libz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_libz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_libz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_libz00();
					return BGl_methodzd2initzd2zzsaw_libz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_libz00(void)
	{
		{	/* SawMill/lib.scm 1 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_lib");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_lib");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_libz00(void)
	{
		{	/* SawMill/lib.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzsaw_libz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_9;

				BgL_headz00_9 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_10;
					obj_t BgL_tailz00_11;

					BgL_prevz00_10 = BgL_headz00_9;
					BgL_tailz00_11 = BgL_l1z00_1;
				BgL_loopz00_12:
					if (PAIRP(BgL_tailz00_11))
						{
							obj_t BgL_newzd2prevzd2_14;

							BgL_newzd2prevzd2_14 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_11), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_10, BgL_newzd2prevzd2_14);
							{
								obj_t BgL_tailz00_77;
								obj_t BgL_prevz00_76;

								BgL_prevz00_76 = BgL_newzd2prevzd2_14;
								BgL_tailz00_77 = CDR(BgL_tailz00_11);
								BgL_tailz00_11 = BgL_tailz00_77;
								BgL_prevz00_10 = BgL_prevz00_76;
								goto BgL_loopz00_12;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_9);
				}
			}
		}

	}



/* subst */
	BGL_EXPORTED_DEF obj_t BGl_substz00zzsaw_libz00(obj_t BgL_lz00_3,
		obj_t BgL_xz00_4, obj_t BgL_yz00_5)
	{
		{	/* SawMill/lib.scm 5 */
			return BGl_walkze71ze7zzsaw_libz00(BgL_xz00_4, BgL_yz00_5, BgL_lz00_3);
		}

	}



/* walk~1 */
	obj_t BGl_walkze71ze7zzsaw_libz00(obj_t BgL_xz00_58, obj_t BgL_yz00_57,
		obj_t BgL_lz00_18)
	{
		{	/* SawMill/lib.scm 12 */
			if (NULLP(BgL_lz00_18))
				{	/* SawMill/lib.scm 7 */
					return BgL_lz00_18;
				}
			else
				{	/* SawMill/lib.scm 9 */
					obj_t BgL_oz00_21;

					BgL_oz00_21 = CAR(((obj_t) BgL_lz00_18));
					if ((BgL_oz00_21 == BgL_xz00_58))
						{	/* SawMill/lib.scm 11 */
							obj_t BgL_arg1017z00_22;

							{	/* SawMill/lib.scm 11 */
								obj_t BgL_arg1018z00_23;

								BgL_arg1018z00_23 = CDR(((obj_t) BgL_lz00_18));
								BgL_arg1017z00_22 =
									BGl_walkze71ze7zzsaw_libz00(BgL_xz00_58, BgL_yz00_57,
									BgL_arg1018z00_23);
							}
							return MAKE_YOUNG_PAIR(BgL_yz00_57, BgL_arg1017z00_22);
						}
					else
						{	/* SawMill/lib.scm 12 */
							obj_t BgL_arg1019z00_24;

							{	/* SawMill/lib.scm 12 */
								obj_t BgL_arg1020z00_25;

								BgL_arg1020z00_25 = CDR(((obj_t) BgL_lz00_18));
								BgL_arg1019z00_24 =
									BGl_walkze71ze7zzsaw_libz00(BgL_xz00_58, BgL_yz00_57,
									BgL_arg1020z00_25);
							}
							return MAKE_YOUNG_PAIR(BgL_oz00_21, BgL_arg1019z00_24);
						}
				}
		}

	}



/* &subst */
	obj_t BGl_z62substz62zzsaw_libz00(obj_t BgL_envz00_47, obj_t BgL_lz00_48,
		obj_t BgL_xz00_49, obj_t BgL_yz00_50)
	{
		{	/* SawMill/lib.scm 5 */
			return BGl_substz00zzsaw_libz00(BgL_lz00_48, BgL_xz00_49, BgL_yz00_50);
		}

	}



/* subst-append */
	BGL_EXPORTED_DEF obj_t BGl_substzd2appendzd2zzsaw_libz00(obj_t BgL_lz00_6,
		obj_t BgL_xz00_7, obj_t BgL_yz00_8)
	{
		{	/* SawMill/lib.scm 15 */
			return BGl_walkze70ze7zzsaw_libz00(BgL_xz00_7, BgL_yz00_8, BgL_lz00_6);
		}

	}



/* walk~0 */
	obj_t BGl_walkze70ze7zzsaw_libz00(obj_t BgL_xz00_56, obj_t BgL_yz00_55,
		obj_t BgL_lz00_28)
	{
		{	/* SawMill/lib.scm 22 */
			if (NULLP(BgL_lz00_28))
				{	/* SawMill/lib.scm 17 */
					return BgL_lz00_28;
				}
			else
				{	/* SawMill/lib.scm 19 */
					obj_t BgL_oz00_31;

					BgL_oz00_31 = CAR(((obj_t) BgL_lz00_28));
					if ((BgL_oz00_31 == BgL_xz00_56))
						{	/* SawMill/lib.scm 21 */
							obj_t BgL_arg1023z00_32;

							{	/* SawMill/lib.scm 21 */
								obj_t BgL_arg1024z00_33;

								BgL_arg1024z00_33 = CDR(((obj_t) BgL_lz00_28));
								BgL_arg1023z00_32 =
									BGl_walkze70ze7zzsaw_libz00(BgL_xz00_56, BgL_yz00_55,
									BgL_arg1024z00_33);
							}
							return
								BGl_appendzd221011zd2zzsaw_libz00(BgL_yz00_55,
								BgL_arg1023z00_32);
						}
					else
						{	/* SawMill/lib.scm 22 */
							obj_t BgL_arg1025z00_34;

							{	/* SawMill/lib.scm 22 */
								obj_t BgL_arg1026z00_35;

								BgL_arg1026z00_35 = CDR(((obj_t) BgL_lz00_28));
								BgL_arg1025z00_34 =
									BGl_walkze70ze7zzsaw_libz00(BgL_xz00_56, BgL_yz00_55,
									BgL_arg1026z00_35);
							}
							return MAKE_YOUNG_PAIR(BgL_oz00_31, BgL_arg1025z00_34);
						}
				}
		}

	}



/* &subst-append */
	obj_t BGl_z62substzd2appendzb0zzsaw_libz00(obj_t BgL_envz00_51,
		obj_t BgL_lz00_52, obj_t BgL_xz00_53, obj_t BgL_yz00_54)
	{
		{	/* SawMill/lib.scm 15 */
			return
				BGl_substzd2appendzd2zzsaw_libz00(BgL_lz00_52, BgL_xz00_53,
				BgL_yz00_54);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_libz00(void)
	{
		{	/* SawMill/lib.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_libz00(void)
	{
		{	/* SawMill/lib.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_libz00(void)
	{
		{	/* SawMill/lib.scm 1 */
			return BUNSPEC;
		}

	}

#ifdef __cplusplus
}
#endif
