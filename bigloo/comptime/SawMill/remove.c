/*===========================================================================*/
/*   (SawMill/remove.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/remove.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_REMOVE_TYPE_DEFINITIONS
#define BGL_SAW_REMOVE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_nopz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_nopz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_defcollectz00_bgl
	{
	}                    *BgL_defcollectz00_bglt;

	typedef struct BgL_rcollectz00_bgl
	{
	}                  *BgL_rcollectz00_bglt;

	typedef struct BgL_ucollectz00_bgl
	{
	}                  *BgL_ucollectz00_bglt;

	typedef struct BgL_removedz00_bgl
	{
	}                 *BgL_removedz00_bglt;

	typedef struct BgL_visitedz00_bgl
	{
	}                 *BgL_visitedz00_bglt;

	typedef struct BgL_bremovedz00_bgl
	{
	}                  *BgL_bremovedz00_bglt;

	typedef struct BgL_cregz00_bgl
	{
		obj_t BgL_defsz00;
		obj_t BgL_nbusesz00;
	}              *BgL_cregz00_bglt;


#endif													// BGL_SAW_REMOVE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	static BgL_blockz00_bglt BGl_z62lambda1932z62zzsaw_removez00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1935z62zzsaw_removez00(obj_t, obj_t);
	extern obj_t BGl_rtl_funz00zzsaw_defsz00;
	static BgL_blockz00_bglt BGl_z62lambda1857z62zzsaw_removez00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1938z62zzsaw_removez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_removez00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31863ze3ze5zzsaw_removez00(obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1860z62zzsaw_removez00(obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1864z62zzsaw_removez00(obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1948z62zzsaw_removez00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31953ze3ze5zzsaw_removez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31937ze3ze5zzsaw_removez00(obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1951z62zzsaw_removez00(obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1954z62zzsaw_removez00(obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1877z62zzsaw_removez00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_removez00zzsaw_removez00(BgL_blockz00_bglt);
	static BgL_blockz00_bglt BGl_z62lambda1880z62zzsaw_removez00(obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda1964z62zzsaw_removez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1884z62zzsaw_removez00(obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda1967z62zzsaw_removez00(obj_t, obj_t);
	static bool_t BGl_rcollectze70ze7zzsaw_removez00(obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda1970z62zzsaw_removez00(obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1895z62zzsaw_removez00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1977z62zzsaw_removez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1978z62zzsaw_removez00(obj_t, obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1898z62zzsaw_removez00(obj_t, obj_t);
	extern obj_t BGl_rtl_purez00zzsaw_defsz00;
	static obj_t BGl_z62zc3z04anonymousza31883ze3ze5zzsaw_removez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1982z62zzsaw_removez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1983z62zzsaw_removez00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_removez00(void);
	static bool_t BGl_defcollectze70ze7zzsaw_removez00(obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_rcollectz00zzsaw_removez00 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zzsaw_removez00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_visitedz00zzsaw_removez00 = BUNSPEC;
	extern obj_t BGl_typez00zztype_typez00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31969ze3ze5zzsaw_removez00(obj_t, obj_t);
	extern obj_t BGl_rtl_nopz00zzsaw_defsz00;
	static obj_t BGl_methodzd2initzd2zzsaw_removez00(void);
	static BgL_blockz00_bglt BGl_z62removez62zzsaw_removez00(obj_t, obj_t);
	static bool_t BGl_removablezf3zf3zzsaw_removez00(BgL_rtl_insz00_bglt);
	static obj_t BGl_defcollectz00zzsaw_removez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_bremovedz00zzsaw_removez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzsaw_removez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_substz00zzsaw_libz00(obj_t, obj_t, obj_t);
	static obj_t BGl_getzd2firstzd2unusedz00zzsaw_removez00(BgL_blockz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31900ze3ze5zzsaw_removez00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_removez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_removez00(void);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static bool_t BGl_dfsze70ze7zzsaw_removez00(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_removez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_removez00(void);
	static bool_t BGl_fixzd2removezd2zzsaw_removez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31651ze3ze5zzsaw_removez00(obj_t, obj_t);
	static obj_t BGl_regzd2ze3cregze70zd6zzsaw_removez00(obj_t);
	BGL_IMPORT obj_t BGl_filterz12z12zz__r4_control_features_6_9z00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31920ze3ze5zzsaw_removez00(obj_t, obj_t);
	static obj_t BGl_removedz00zzsaw_removez00 = BUNSPEC;
	static obj_t BGl_cregz00zzsaw_removez00 = BUNSPEC;
	static obj_t BGl_blockzd2removezd2zzsaw_removez00(BgL_blockz00_bglt);
	static obj_t BGl_ucollectz00zzsaw_removez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1901z62zzsaw_removez00(obj_t, obj_t);
	extern obj_t BGl_blockz00zzsaw_defsz00;
	extern obj_t BGl_substzd2appendzd2zzsaw_libz00(obj_t, obj_t, obj_t);
	static BgL_rtl_insz00_bglt BGl_z62lambda1915z62zzsaw_removez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_rtl_insz00_bglt BGl_z62lambda1918z62zzsaw_removez00(obj_t, obj_t);
	static obj_t BGl_getzd2firstzd2removablez00zzsaw_removez00(BgL_blockz00_bglt);
	static bool_t BGl_ucollectze70ze7zzsaw_removez00(obj_t, obj_t);
	static BgL_rtl_insz00_bglt BGl_z62lambda1921z62zzsaw_removez00(obj_t, obj_t);
	static obj_t __cnst[12];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2112z00zzsaw_removez00,
		BgL_bgl_za762lambda1895za7622136z00, BGl_z62lambda1895z62zzsaw_removez00,
		0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2113z00zzsaw_removez00,
		BgL_bgl_za762lambda1921za7622137z00, BGl_z62lambda1921z62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2114z00zzsaw_removez00,
		BgL_bgl_za762za7c3za704anonymo2138za7,
		BGl_z62zc3z04anonymousza31920ze3ze5zzsaw_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2115z00zzsaw_removez00,
		BgL_bgl_za762lambda1918za7622139z00, BGl_z62lambda1918z62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2116z00zzsaw_removez00,
		BgL_bgl_za762lambda1915za7622140z00, BGl_z62lambda1915z62zzsaw_removez00,
		0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2117z00zzsaw_removez00,
		BgL_bgl_za762lambda1938za7622141z00, BGl_z62lambda1938z62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2118z00zzsaw_removez00,
		BgL_bgl_za762za7c3za704anonymo2142za7,
		BGl_z62zc3z04anonymousza31937ze3ze5zzsaw_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2119z00zzsaw_removez00,
		BgL_bgl_za762lambda1935za7622143z00, BGl_z62lambda1935z62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2120z00zzsaw_removez00,
		BgL_bgl_za762lambda1932za7622144z00, BGl_z62lambda1932z62zzsaw_removez00,
		0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2121z00zzsaw_removez00,
		BgL_bgl_za762lambda1954za7622145z00, BGl_z62lambda1954z62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2122z00zzsaw_removez00,
		BgL_bgl_za762za7c3za704anonymo2146za7,
		BGl_z62zc3z04anonymousza31953ze3ze5zzsaw_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2123z00zzsaw_removez00,
		BgL_bgl_za762lambda1951za7622147z00, BGl_z62lambda1951z62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2124z00zzsaw_removez00,
		BgL_bgl_za762lambda1948za7622148z00, BGl_z62lambda1948z62zzsaw_removez00,
		0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2125z00zzsaw_removez00,
		BgL_bgl_za762lambda1978za7622149z00, BGl_z62lambda1978z62zzsaw_removez00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2126z00zzsaw_removez00,
		BgL_bgl_za762lambda1977za7622150z00, BGl_z62lambda1977z62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2133z00zzsaw_removez00,
		BgL_bgl_string2133za700za7za7s2151za7, "saw_remove", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2127z00zzsaw_removez00,
		BgL_bgl_za762lambda1983za7622152z00, BGl_z62lambda1983z62zzsaw_removez00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2134z00zzsaw_removez00,
		BgL_bgl_string2134za700za7za7s2153za7,
		"creg nbuses obj defs bremoved visited removed ucollect rcollect saw_remove defcollect unlink! ",
		94);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2128z00zzsaw_removez00,
		BgL_bgl_za762lambda1982za7622154z00, BGl_z62lambda1982z62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2129z00zzsaw_removez00,
		BgL_bgl_za762lambda1970za7622155z00, BGl_z62lambda1970z62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2130z00zzsaw_removez00,
		BgL_bgl_za762za7c3za704anonymo2156za7,
		BGl_z62zc3z04anonymousza31969ze3ze5zzsaw_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2131z00zzsaw_removez00,
		BgL_bgl_za762lambda1967za7622157z00, BGl_z62lambda1967z62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2132z00zzsaw_removez00,
		BgL_bgl_za762lambda1964za7622158z00, BGl_z62lambda1964z62zzsaw_removez00,
		0L, BUNSPEC, 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2099z00zzsaw_removez00,
		BgL_bgl_za762za7c3za704anonymo2159za7,
		BGl_z62zc3z04anonymousza31651ze3ze5zzsaw_removez00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_removezd2envzd2zzsaw_removez00,
		BgL_bgl_za762removeza762za7za7sa2160z00, BGl_z62removez62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2100z00zzsaw_removez00,
		BgL_bgl_string2100za700za7za7s2161za7, "must have only one successor", 28);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2101z00zzsaw_removez00,
		BgL_bgl_za762lambda1864za7622162z00, BGl_z62lambda1864z62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2102z00zzsaw_removez00,
		BgL_bgl_za762za7c3za704anonymo2163za7,
		BGl_z62zc3z04anonymousza31863ze3ze5zzsaw_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2103z00zzsaw_removez00,
		BgL_bgl_za762lambda1860za7622164z00, BGl_z62lambda1860z62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2104z00zzsaw_removez00,
		BgL_bgl_za762lambda1857za7622165z00, BGl_z62lambda1857z62zzsaw_removez00,
		0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2105z00zzsaw_removez00,
		BgL_bgl_za762lambda1884za7622166z00, BGl_z62lambda1884z62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2106z00zzsaw_removez00,
		BgL_bgl_za762za7c3za704anonymo2167za7,
		BGl_z62zc3z04anonymousza31883ze3ze5zzsaw_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2107z00zzsaw_removez00,
		BgL_bgl_za762lambda1880za7622168z00, BGl_z62lambda1880z62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2108z00zzsaw_removez00,
		BgL_bgl_za762lambda1877za7622169z00, BGl_z62lambda1877z62zzsaw_removez00,
		0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2109z00zzsaw_removez00,
		BgL_bgl_za762lambda1901za7622170z00, BGl_z62lambda1901z62zzsaw_removez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2110z00zzsaw_removez00,
		BgL_bgl_za762za7c3za704anonymo2171za7,
		BGl_z62zc3z04anonymousza31900ze3ze5zzsaw_removez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2111z00zzsaw_removez00,
		BgL_bgl_za762lambda1898za7622172z00, BGl_z62lambda1898z62zzsaw_removez00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_removez00));
		     ADD_ROOT((void *) (&BGl_rcollectz00zzsaw_removez00));
		     ADD_ROOT((void *) (&BGl_visitedz00zzsaw_removez00));
		     ADD_ROOT((void *) (&BGl_defcollectz00zzsaw_removez00));
		     ADD_ROOT((void *) (&BGl_bremovedz00zzsaw_removez00));
		     ADD_ROOT((void *) (&BGl_removedz00zzsaw_removez00));
		     ADD_ROOT((void *) (&BGl_cregz00zzsaw_removez00));
		     ADD_ROOT((void *) (&BGl_ucollectz00zzsaw_removez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_removez00(long
		BgL_checksumz00_3664, char *BgL_fromz00_3665)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_removez00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_removez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_removez00();
					BGl_libraryzd2moduleszd2initz00zzsaw_removez00();
					BGl_cnstzd2initzd2zzsaw_removez00();
					BGl_importedzd2moduleszd2initz00zzsaw_removez00();
					BGl_objectzd2initzd2zzsaw_removez00();
					return BGl_methodzd2initzd2zzsaw_removez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_removez00(void)
	{
		{	/* SawMill/remove.scm 1 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_remove");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_remove");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_remove");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "saw_remove");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_remove");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"saw_remove");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "saw_remove");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_remove");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_remove");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_remove");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_removez00(void)
	{
		{	/* SawMill/remove.scm 1 */
			{	/* SawMill/remove.scm 1 */
				obj_t BgL_cportz00_3496;

				{	/* SawMill/remove.scm 1 */
					obj_t BgL_stringz00_3503;

					BgL_stringz00_3503 = BGl_string2134z00zzsaw_removez00;
					{	/* SawMill/remove.scm 1 */
						obj_t BgL_startz00_3504;

						BgL_startz00_3504 = BINT(0L);
						{	/* SawMill/remove.scm 1 */
							obj_t BgL_endz00_3505;

							BgL_endz00_3505 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3503)));
							{	/* SawMill/remove.scm 1 */

								BgL_cportz00_3496 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3503, BgL_startz00_3504, BgL_endz00_3505);
				}}}}
				{
					long BgL_iz00_3497;

					BgL_iz00_3497 = 11L;
				BgL_loopz00_3498:
					if ((BgL_iz00_3497 == -1L))
						{	/* SawMill/remove.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawMill/remove.scm 1 */
							{	/* SawMill/remove.scm 1 */
								obj_t BgL_arg2135z00_3499;

								{	/* SawMill/remove.scm 1 */

									{	/* SawMill/remove.scm 1 */
										obj_t BgL_locationz00_3501;

										BgL_locationz00_3501 = BBOOL(((bool_t) 0));
										{	/* SawMill/remove.scm 1 */

											BgL_arg2135z00_3499 =
												BGl_readz00zz__readerz00(BgL_cportz00_3496,
												BgL_locationz00_3501);
										}
									}
								}
								{	/* SawMill/remove.scm 1 */
									int BgL_tmpz00_3694;

									BgL_tmpz00_3694 = (int) (BgL_iz00_3497);
									CNST_TABLE_SET(BgL_tmpz00_3694, BgL_arg2135z00_3499);
							}}
							{	/* SawMill/remove.scm 1 */
								int BgL_auxz00_3502;

								BgL_auxz00_3502 = (int) ((BgL_iz00_3497 - 1L));
								{
									long BgL_iz00_3699;

									BgL_iz00_3699 = (long) (BgL_auxz00_3502);
									BgL_iz00_3497 = BgL_iz00_3699;
									goto BgL_loopz00_3498;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_removez00(void)
	{
		{	/* SawMill/remove.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* remove */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_removez00zzsaw_removez00(BgL_blockz00_bglt BgL_bz00_142)
	{
		{	/* SawMill/remove.scm 17 */
			BGl_defcollectze70ze7zzsaw_removez00(((obj_t) BgL_bz00_142));
			BGl_fixzd2removezd2zzsaw_removez00
				(BGl_getzd2firstzd2unusedz00zzsaw_removez00(BgL_bz00_142),
				BGl_getzd2firstzd2removablez00zzsaw_removez00(BgL_bz00_142));
			BGl_dfsze70ze7zzsaw_removez00(((obj_t) BgL_bz00_142));
			{
				obj_t BgL_bz00_2012;

				{
					obj_t BgL_auxz00_3709;

					BgL_bz00_2012 = ((obj_t) BgL_bz00_142);
				BgL_zc3z04anonymousza31662ze3z87_2013:
					{	/* SawMill/remove.scm 30 */
						bool_t BgL_test2175z00_3710;

						{	/* SawMill/remove.scm 30 */
							obj_t BgL_classz00_2943;

							BgL_classz00_2943 = BGl_bremovedz00zzsaw_removez00;
							if (BGL_OBJECTP(BgL_bz00_2012))
								{	/* SawMill/remove.scm 30 */
									obj_t BgL_oclassz00_2945;

									{	/* SawMill/remove.scm 30 */
										obj_t BgL_arg1815z00_2947;
										long BgL_arg1816z00_2948;

										BgL_arg1815z00_2947 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/remove.scm 30 */
											long BgL_arg1817z00_2949;

											BgL_arg1817z00_2949 =
												BGL_OBJECT_CLASS_NUM(
												((BgL_objectz00_bglt) BgL_bz00_2012));
											BgL_arg1816z00_2948 = (BgL_arg1817z00_2949 - OBJECT_TYPE);
										}
										BgL_oclassz00_2945 =
											VECTOR_REF(BgL_arg1815z00_2947, BgL_arg1816z00_2948);
									}
									BgL_test2175z00_3710 =
										(BgL_oclassz00_2945 == BgL_classz00_2943);
								}
							else
								{	/* SawMill/remove.scm 30 */
									BgL_test2175z00_3710 = ((bool_t) 0);
								}
						}
						if (BgL_test2175z00_3710)
							{	/* SawMill/remove.scm 32 */
								obj_t BgL_sz00_2015;

								{	/* SawMill/remove.scm 32 */
									obj_t BgL_pairz00_2956;

									BgL_pairz00_2956 =
										(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt) BgL_bz00_2012)))->BgL_succsz00);
									BgL_sz00_2015 = CAR(BgL_pairz00_2956);
								}
								if ((BgL_bz00_2012 == BgL_sz00_2015))
									{	/* SawMill/remove.scm 33 */
										BgL_auxz00_3709 = BgL_bz00_2012;
									}
								else
									{
										obj_t BgL_bz00_3724;

										BgL_bz00_3724 = BgL_sz00_2015;
										BgL_bz00_2012 = BgL_bz00_3724;
										goto BgL_zc3z04anonymousza31662ze3z87_2013;
									}
							}
						else
							{	/* SawMill/remove.scm 30 */
								BgL_auxz00_3709 = BgL_bz00_2012;
							}
					}
					return ((BgL_blockz00_bglt) BgL_auxz00_3709);
				}
			}
		}

	}



/* dfs~0 */
	bool_t BGl_dfsze70ze7zzsaw_removez00(obj_t BgL_bz00_1984)
	{
		{	/* SawMill/remove.scm 20 */
			{	/* SawMill/remove.scm 21 */
				BgL_visitedz00_bglt BgL_wide1257z00_1988;

				BgL_wide1257z00_1988 =
					((BgL_visitedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_visitedz00_bgl))));
				{	/* SawMill/remove.scm 21 */
					obj_t BgL_auxz00_3732;
					BgL_objectz00_bglt BgL_tmpz00_3728;

					BgL_auxz00_3732 = ((obj_t) BgL_wide1257z00_1988);
					BgL_tmpz00_3728 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1984)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3728, BgL_auxz00_3732);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1984)));
				{	/* SawMill/remove.scm 21 */
					long BgL_arg1646z00_1989;

					BgL_arg1646z00_1989 = BGL_CLASS_NUM(BGl_visitedz00zzsaw_removez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_bz00_1984))), BgL_arg1646z00_1989);
				}
				((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1984)));
			}
			((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1984));
			{	/* SawMill/remove.scm 22 */
				obj_t BgL_lz00_1991;

				BgL_lz00_1991 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_bz00_1984)))->BgL_firstz00);
				{	/* SawMill/remove.scm 23 */
					obj_t BgL_nlz00_1992;

					BgL_nlz00_1992 =
						BGl_filterz12z12zz__r4_control_features_6_9z00
						(BGl_proc2099z00zzsaw_removez00, BgL_lz00_1991);
					if (NULLP(BgL_nlz00_1992))
						{	/* SawMill/remove.scm 24 */
							BGl_blockzd2removezd2zzsaw_removez00(
								((BgL_blockz00_bglt) BgL_bz00_1984));
						}
					else
						{	/* SawMill/remove.scm 26 */
							obj_t BgL_vz00_2915;

							BgL_vz00_2915 = BgL_nlz00_1992;
							((((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt) BgL_bz00_1984)))->BgL_firstz00) =
								((obj_t) BgL_vz00_2915), BUNSPEC);
						}
				}
			}
			{	/* SawMill/remove.scm 27 */
				obj_t BgL_g1538z00_1999;

				BgL_g1538z00_1999 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_bz00_1984)))->BgL_succsz00);
				{
					obj_t BgL_l1536z00_2001;

					BgL_l1536z00_2001 = BgL_g1538z00_1999;
				BgL_zc3z04anonymousza31653ze3z87_2002:
					if (PAIRP(BgL_l1536z00_2001))
						{	/* SawMill/remove.scm 28 */
							{	/* SawMill/remove.scm 27 */
								obj_t BgL_sz00_2004;

								BgL_sz00_2004 = CAR(BgL_l1536z00_2001);
								{	/* SawMill/remove.scm 27 */
									bool_t BgL_test2180z00_3762;

									{	/* SawMill/remove.scm 27 */
										bool_t BgL_test2181z00_3763;

										{	/* SawMill/remove.scm 27 */
											obj_t BgL_classz00_2918;

											BgL_classz00_2918 = BGl_visitedz00zzsaw_removez00;
											if (BGL_OBJECTP(BgL_sz00_2004))
												{	/* SawMill/remove.scm 27 */
													obj_t BgL_oclassz00_2920;

													{	/* SawMill/remove.scm 27 */
														obj_t BgL_arg1815z00_2922;
														long BgL_arg1816z00_2923;

														BgL_arg1815z00_2922 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawMill/remove.scm 27 */
															long BgL_arg1817z00_2924;

															BgL_arg1817z00_2924 =
																BGL_OBJECT_CLASS_NUM(
																((BgL_objectz00_bglt) BgL_sz00_2004));
															BgL_arg1816z00_2923 =
																(BgL_arg1817z00_2924 - OBJECT_TYPE);
														}
														BgL_oclassz00_2920 =
															VECTOR_REF(BgL_arg1815z00_2922,
															BgL_arg1816z00_2923);
													}
													BgL_test2181z00_3763 =
														(BgL_oclassz00_2920 == BgL_classz00_2918);
												}
											else
												{	/* SawMill/remove.scm 27 */
													BgL_test2181z00_3763 = ((bool_t) 0);
												}
										}
										if (BgL_test2181z00_3763)
											{	/* SawMill/remove.scm 27 */
												BgL_test2180z00_3762 = ((bool_t) 1);
											}
										else
											{	/* SawMill/remove.scm 27 */
												obj_t BgL_classz00_2930;

												BgL_classz00_2930 = BGl_bremovedz00zzsaw_removez00;
												if (BGL_OBJECTP(BgL_sz00_2004))
													{	/* SawMill/remove.scm 27 */
														obj_t BgL_oclassz00_2932;

														{	/* SawMill/remove.scm 27 */
															obj_t BgL_arg1815z00_2934;
															long BgL_arg1816z00_2935;

															BgL_arg1815z00_2934 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawMill/remove.scm 27 */
																long BgL_arg1817z00_2936;

																BgL_arg1817z00_2936 =
																	BGL_OBJECT_CLASS_NUM(
																	((BgL_objectz00_bglt) BgL_sz00_2004));
																BgL_arg1816z00_2935 =
																	(BgL_arg1817z00_2936 - OBJECT_TYPE);
															}
															BgL_oclassz00_2932 =
																VECTOR_REF(BgL_arg1815z00_2934,
																BgL_arg1816z00_2935);
														}
														BgL_test2180z00_3762 =
															(BgL_oclassz00_2932 == BgL_classz00_2930);
													}
												else
													{	/* SawMill/remove.scm 27 */
														BgL_test2180z00_3762 = ((bool_t) 0);
													}
											}
									}
									if (BgL_test2180z00_3762)
										{	/* SawMill/remove.scm 27 */
											((bool_t) 0);
										}
									else
										{	/* SawMill/remove.scm 27 */
											BGl_dfsze70ze7zzsaw_removez00(BgL_sz00_2004);
										}
								}
							}
							{
								obj_t BgL_l1536z00_3781;

								BgL_l1536z00_3781 = CDR(BgL_l1536z00_2001);
								BgL_l1536z00_2001 = BgL_l1536z00_3781;
								goto BgL_zc3z04anonymousza31653ze3z87_2002;
							}
						}
					else
						{	/* SawMill/remove.scm 28 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* &remove */
	BgL_blockz00_bglt BGl_z62removez62zzsaw_removez00(obj_t BgL_envz00_3359,
		obj_t BgL_bz00_3360)
	{
		{	/* SawMill/remove.scm 17 */
			return BGl_removez00zzsaw_removez00(((BgL_blockz00_bglt) BgL_bz00_3360));
		}

	}



/* &<@anonymous:1651> */
	obj_t BGl_z62zc3z04anonymousza31651ze3ze5zzsaw_removez00(obj_t
		BgL_envz00_3361, obj_t BgL_insz00_3362)
	{
		{	/* SawMill/remove.scm 23 */
			{	/* SawMill/remove.scm 23 */
				bool_t BgL_tmpz00_3785;

				{	/* SawMill/remove.scm 23 */
					bool_t BgL_test2184z00_3786;

					{	/* SawMill/remove.scm 23 */
						obj_t BgL_classz00_3508;

						BgL_classz00_3508 = BGl_removedz00zzsaw_removez00;
						if (BGL_OBJECTP(BgL_insz00_3362))
							{	/* SawMill/remove.scm 23 */
								obj_t BgL_oclassz00_3509;

								{	/* SawMill/remove.scm 23 */
									obj_t BgL_arg1815z00_3510;
									long BgL_arg1816z00_3511;

									BgL_arg1815z00_3510 = (BGl_za2classesza2z00zz__objectz00);
									{	/* SawMill/remove.scm 23 */
										long BgL_arg1817z00_3512;

										BgL_arg1817z00_3512 =
											BGL_OBJECT_CLASS_NUM(
											((BgL_objectz00_bglt) BgL_insz00_3362));
										BgL_arg1816z00_3511 = (BgL_arg1817z00_3512 - OBJECT_TYPE);
									}
									BgL_oclassz00_3509 =
										VECTOR_REF(BgL_arg1815z00_3510, BgL_arg1816z00_3511);
								}
								BgL_test2184z00_3786 =
									(BgL_oclassz00_3509 == BgL_classz00_3508);
							}
						else
							{	/* SawMill/remove.scm 23 */
								BgL_test2184z00_3786 = ((bool_t) 0);
							}
					}
					if (BgL_test2184z00_3786)
						{	/* SawMill/remove.scm 23 */
							BgL_tmpz00_3785 = ((bool_t) 0);
						}
					else
						{	/* SawMill/remove.scm 23 */
							BgL_tmpz00_3785 = ((bool_t) 1);
						}
				}
				return BBOOL(BgL_tmpz00_3785);
			}
		}

	}



/* block-remove */
	obj_t BGl_blockzd2removezd2zzsaw_removez00(BgL_blockz00_bglt BgL_bz00_143)
	{
		{	/* SawMill/remove.scm 37 */
			{	/* SawMill/remove.scm 39 */
				bool_t BgL_test2186z00_3796;

				if (NULLP((((BgL_blockz00_bglt) COBJECT(BgL_bz00_143))->BgL_succsz00)))
					{	/* SawMill/remove.scm 39 */
						BgL_test2186z00_3796 = ((bool_t) 1);
					}
				else
					{	/* SawMill/remove.scm 39 */
						obj_t BgL_tmpz00_3800;

						BgL_tmpz00_3800 =
							CDR((((BgL_blockz00_bglt) COBJECT(BgL_bz00_143))->BgL_succsz00));
						BgL_test2186z00_3796 = PAIRP(BgL_tmpz00_3800);
					}
				if (BgL_test2186z00_3796)
					{	/* SawMill/remove.scm 39 */
						BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
							BGl_string2100z00zzsaw_removez00, ((obj_t) BgL_bz00_143));
					}
				else
					{	/* SawMill/remove.scm 39 */
						BFALSE;
					}
			}
			{	/* SawMill/remove.scm 41 */
				obj_t BgL_sz00_2029;

				BgL_sz00_2029 =
					CAR((((BgL_blockz00_bglt) COBJECT(BgL_bz00_143))->BgL_succsz00));
				if ((((obj_t) BgL_bz00_143) == BgL_sz00_2029))
					{	/* SawMill/remove.scm 44 */
						obj_t BgL_arg1700z00_2030;

						{	/* SawMill/remove.scm 44 */
							BgL_rtl_insz00_bglt BgL_arg1701z00_2031;

							{	/* SawMill/remove.scm 44 */
								BgL_rtl_insz00_bglt BgL_new1261z00_2033;

								{	/* SawMill/remove.scm 44 */
									BgL_rtl_insz00_bglt BgL_new1260z00_2037;

									BgL_new1260z00_2037 =
										((BgL_rtl_insz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_rtl_insz00_bgl))));
									{	/* SawMill/remove.scm 44 */
										long BgL_arg1705z00_2038;

										BgL_arg1705z00_2038 =
											BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1260z00_2037),
											BgL_arg1705z00_2038);
									}
									{	/* SawMill/remove.scm 44 */
										BgL_objectz00_bglt BgL_tmpz00_3816;

										BgL_tmpz00_3816 =
											((BgL_objectz00_bglt) BgL_new1260z00_2037);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3816, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1260z00_2037);
									BgL_new1261z00_2033 = BgL_new1260z00_2037;
								}
								((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1261z00_2033))->
										BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1261z00_2033))->
										BgL_z52spillz52) = ((obj_t) BNIL), BUNSPEC);
								((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1261z00_2033))->
										BgL_destz00) = ((obj_t) BFALSE), BUNSPEC);
								{
									BgL_rtl_funz00_bglt BgL_auxz00_3823;

									{	/* SawMill/remove.scm 45 */
										BgL_rtl_nopz00_bglt BgL_new1263z00_2034;

										{	/* SawMill/remove.scm 45 */
											BgL_rtl_nopz00_bglt BgL_new1262z00_2035;

											BgL_new1262z00_2035 =
												((BgL_rtl_nopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_rtl_nopz00_bgl))));
											{	/* SawMill/remove.scm 45 */
												long BgL_arg1703z00_2036;

												{	/* SawMill/remove.scm 45 */
													obj_t BgL_classz00_2963;

													BgL_classz00_2963 = BGl_rtl_nopz00zzsaw_defsz00;
													BgL_arg1703z00_2036 =
														BGL_CLASS_NUM(BgL_classz00_2963);
												}
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1262z00_2035),
													BgL_arg1703z00_2036);
											}
											BgL_new1263z00_2034 = BgL_new1262z00_2035;
										}
										((((BgL_rtl_funz00_bglt) COBJECT(
														((BgL_rtl_funz00_bglt) BgL_new1263z00_2034)))->
												BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
										BgL_auxz00_3823 =
											((BgL_rtl_funz00_bglt) BgL_new1263z00_2034);
									}
									((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1261z00_2033))->
											BgL_funz00) =
										((BgL_rtl_funz00_bglt) BgL_auxz00_3823), BUNSPEC);
								}
								((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1261z00_2033))->
										BgL_argsz00) = ((obj_t) BNIL), BUNSPEC);
								BgL_arg1701z00_2031 = BgL_new1261z00_2033;
							}
							{	/* SawMill/remove.scm 44 */
								obj_t BgL_list1702z00_2032;

								BgL_list1702z00_2032 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_arg1701z00_2031), BNIL);
								BgL_arg1700z00_2030 = BgL_list1702z00_2032;
						}}
						return
							((((BgL_blockz00_bglt) COBJECT(BgL_bz00_143))->BgL_firstz00) =
							((obj_t) BgL_arg1700z00_2030), BUNSPEC);
					}
				else
					{	/* SawMill/remove.scm 42 */
						{	/* SawMill/remove.scm 47 */
							BgL_bremovedz00_bglt BgL_wide1266z00_2041;

							BgL_wide1266z00_2041 =
								((BgL_bremovedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_bremovedz00_bgl))));
							{	/* SawMill/remove.scm 47 */
								obj_t BgL_auxz00_3840;
								BgL_objectz00_bglt BgL_tmpz00_3837;

								BgL_auxz00_3840 = ((obj_t) BgL_wide1266z00_2041);
								BgL_tmpz00_3837 =
									((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_143));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3837, BgL_auxz00_3840);
							}
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_143));
							{	/* SawMill/remove.scm 47 */
								long BgL_arg1708z00_2042;

								BgL_arg1708z00_2042 =
									BGL_CLASS_NUM(BGl_bremovedz00zzsaw_removez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_bz00_143)), BgL_arg1708z00_2042);
							}
							((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_143));
						}
						((BgL_blockz00_bglt) BgL_bz00_143);
						{	/* SawMill/remove.scm 48 */
							obj_t BgL_g1541z00_2044;

							BgL_g1541z00_2044 =
								(((BgL_blockz00_bglt) COBJECT(BgL_bz00_143))->BgL_predsz00);
							{
								obj_t BgL_l1539z00_2046;

								BgL_l1539z00_2046 = BgL_g1541z00_2044;
							BgL_zc3z04anonymousza31709ze3z87_2047:
								if (PAIRP(BgL_l1539z00_2046))
									{	/* SawMill/remove.scm 48 */
										{	/* SawMill/remove.scm 48 */
											obj_t BgL_pz00_2049;

											BgL_pz00_2049 = CAR(BgL_l1539z00_2046);
											{
												obj_t BgL_auxz00_3856;

												{	/* SawMill/remove.scm 49 */
													obj_t BgL_arg1714z00_2051;

													BgL_arg1714z00_2051 =
														(((BgL_blockz00_bglt) COBJECT(
																((BgL_blockz00_bglt) BgL_pz00_2049)))->
														BgL_succsz00);
													BgL_auxz00_3856 =
														BGl_substz00zzsaw_libz00(BgL_arg1714z00_2051,
														((obj_t) BgL_bz00_143), BgL_sz00_2029);
												}
												((((BgL_blockz00_bglt) COBJECT(
																((BgL_blockz00_bglt) BgL_pz00_2049)))->
														BgL_succsz00) = ((obj_t) BgL_auxz00_3856), BUNSPEC);
											}
										}
										{
											obj_t BgL_l1539z00_3863;

											BgL_l1539z00_3863 = CDR(BgL_l1539z00_2046);
											BgL_l1539z00_2046 = BgL_l1539z00_3863;
											goto BgL_zc3z04anonymousza31709ze3z87_2047;
										}
									}
								else
									{	/* SawMill/remove.scm 48 */
										((bool_t) 1);
									}
							}
						}
						{	/* SawMill/remove.scm 51 */
							obj_t BgL_npredsz00_2054;

							{	/* SawMill/remove.scm 51 */
								obj_t BgL_arg1718z00_2055;
								obj_t BgL_arg1720z00_2056;

								BgL_arg1718z00_2055 =
									(((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt) BgL_sz00_2029)))->BgL_predsz00);
								BgL_arg1720z00_2056 =
									(((BgL_blockz00_bglt) COBJECT(BgL_bz00_143))->BgL_predsz00);
								BgL_npredsz00_2054 =
									BGl_substzd2appendzd2zzsaw_libz00(BgL_arg1718z00_2055,
									((obj_t) BgL_bz00_143), BgL_arg1720z00_2056);
							}
							return
								((((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt) BgL_sz00_2029)))->BgL_predsz00) =
								((obj_t) BgL_npredsz00_2054), BUNSPEC);
						}
					}
			}
		}

	}



/* reg->creg~0 */
	obj_t BGl_regzd2ze3cregze70zd6zzsaw_removez00(obj_t BgL_rz00_2098)
	{
		{	/* SawMill/remove.scm 57 */
			{	/* SawMill/remove.scm 56 */
				bool_t BgL_test2190z00_3872;

				{	/* SawMill/remove.scm 56 */
					obj_t BgL_classz00_2978;

					BgL_classz00_2978 = BGl_cregz00zzsaw_removez00;
					if (BGL_OBJECTP(BgL_rz00_2098))
						{	/* SawMill/remove.scm 56 */
							obj_t BgL_oclassz00_2980;

							{	/* SawMill/remove.scm 56 */
								obj_t BgL_arg1815z00_2982;
								long BgL_arg1816z00_2983;

								BgL_arg1815z00_2982 = (BGl_za2classesza2z00zz__objectz00);
								{	/* SawMill/remove.scm 56 */
									long BgL_arg1817z00_2984;

									BgL_arg1817z00_2984 =
										BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_rz00_2098));
									BgL_arg1816z00_2983 = (BgL_arg1817z00_2984 - OBJECT_TYPE);
								}
								BgL_oclassz00_2980 =
									VECTOR_REF(BgL_arg1815z00_2982, BgL_arg1816z00_2983);
							}
							BgL_test2190z00_3872 = (BgL_oclassz00_2980 == BgL_classz00_2978);
						}
					else
						{	/* SawMill/remove.scm 56 */
							BgL_test2190z00_3872 = ((bool_t) 0);
						}
				}
				if (BgL_test2190z00_3872)
					{	/* SawMill/remove.scm 56 */
						BFALSE;
					}
				else
					{	/* SawMill/remove.scm 56 */
						{	/* SawMill/remove.scm 57 */
							BgL_cregz00_bglt BgL_wide1271z00_2103;

							BgL_wide1271z00_2103 =
								((BgL_cregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_cregz00_bgl))));
							{	/* SawMill/remove.scm 57 */
								obj_t BgL_auxz00_3886;
								BgL_objectz00_bglt BgL_tmpz00_3882;

								BgL_auxz00_3886 = ((obj_t) BgL_wide1271z00_2103);
								BgL_tmpz00_3882 =
									((BgL_objectz00_bglt)
									((BgL_rtl_regz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_rz00_2098)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3882, BgL_auxz00_3886);
							}
							((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_2098)));
							{	/* SawMill/remove.scm 57 */
								long BgL_arg1749z00_2104;

								BgL_arg1749z00_2104 = BGL_CLASS_NUM(BGl_cregz00zzsaw_removez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_rz00_2098))),
									BgL_arg1749z00_2104);
							}
							((BgL_rtl_regz00_bglt)
								((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_2098)));
						}
						{
							BgL_cregz00_bglt BgL_auxz00_3900;

							{
								obj_t BgL_auxz00_3901;

								{	/* SawMill/remove.scm 57 */
									BgL_objectz00_bglt BgL_tmpz00_3902;

									BgL_tmpz00_3902 =
										((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_rz00_2098)));
									BgL_auxz00_3901 = BGL_OBJECT_WIDENING(BgL_tmpz00_3902);
								}
								BgL_auxz00_3900 = ((BgL_cregz00_bglt) BgL_auxz00_3901);
							}
							((((BgL_cregz00_bglt) COBJECT(BgL_auxz00_3900))->BgL_defsz00) =
								((obj_t) BNIL), BUNSPEC);
						}
						{
							BgL_cregz00_bglt BgL_auxz00_3909;

							{
								obj_t BgL_auxz00_3910;

								{	/* SawMill/remove.scm 57 */
									BgL_objectz00_bglt BgL_tmpz00_3911;

									BgL_tmpz00_3911 =
										((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_rz00_2098)));
									BgL_auxz00_3910 = BGL_OBJECT_WIDENING(BgL_tmpz00_3911);
								}
								BgL_auxz00_3909 = ((BgL_cregz00_bglt) BgL_auxz00_3910);
							}
							((((BgL_cregz00_bglt) COBJECT(BgL_auxz00_3909))->BgL_nbusesz00) =
								((obj_t) BINT(0L)), BUNSPEC);
						}
						((obj_t)
							((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_2098)));
			}}
			return BgL_rz00_2098;
		}

	}



/* defcollect~0 */
	bool_t BGl_defcollectze70ze7zzsaw_removez00(obj_t BgL_bz00_2062)
	{
		{	/* SawMill/remove.scm 65 */
			{
				obj_t BgL_rz00_2106;

				{	/* SawMill/remove.scm 66 */
					BgL_defcollectz00_bglt BgL_wide1277z00_2066;

					BgL_wide1277z00_2066 =
						((BgL_defcollectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_defcollectz00_bgl))));
					{	/* SawMill/remove.scm 66 */
						obj_t BgL_auxz00_3927;
						BgL_objectz00_bglt BgL_tmpz00_3923;

						BgL_auxz00_3927 = ((obj_t) BgL_wide1277z00_2066);
						BgL_tmpz00_3923 =
							((BgL_objectz00_bglt)
							((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2062)));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3923, BgL_auxz00_3927);
					}
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2062)));
					{	/* SawMill/remove.scm 66 */
						long BgL_arg1724z00_2067;

						BgL_arg1724z00_2067 =
							BGL_CLASS_NUM(BGl_defcollectz00zzsaw_removez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_bz00_2062))), BgL_arg1724z00_2067);
					}
					((BgL_blockz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2062)));
				}
				((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2062));
				{	/* SawMill/remove.scm 67 */
					obj_t BgL_g1547z00_2069;

					BgL_g1547z00_2069 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_bz00_2062)))->BgL_firstz00);
					{
						obj_t BgL_l1545z00_2071;

						BgL_l1545z00_2071 = BgL_g1547z00_2069;
					BgL_zc3z04anonymousza31725ze3z87_2072:
						if (PAIRP(BgL_l1545z00_2071))
							{	/* SawMill/remove.scm 71 */
								{	/* SawMill/remove.scm 68 */
									obj_t BgL_insz00_2074;

									BgL_insz00_2074 = CAR(BgL_l1545z00_2071);
									if (CBOOL(
											(((BgL_rtl_insz00_bglt) COBJECT(
														((BgL_rtl_insz00_bglt) BgL_insz00_2074)))->
												BgL_destz00)))
										{	/* SawMill/remove.scm 69 */
											obj_t BgL_arg1733z00_2077;

											BgL_arg1733z00_2077 =
												(((BgL_rtl_insz00_bglt) COBJECT(
														((BgL_rtl_insz00_bglt) BgL_insz00_2074)))->
												BgL_destz00);
											{	/* SawMill/remove.scm 63 */
												BgL_rtl_regz00_bglt BgL_i1274z00_3009;

												BgL_i1274z00_3009 =
													((BgL_rtl_regz00_bglt)
													BGl_regzd2ze3cregze70zd6zzsaw_removez00
													(BgL_arg1733z00_2077));
												{
													obj_t BgL_auxz00_3962;
													BgL_cregz00_bglt BgL_auxz00_3956;

													{	/* SawMill/remove.scm 64 */
														obj_t BgL_arg1753z00_3010;

														{
															BgL_cregz00_bglt BgL_auxz00_3963;

															{
																obj_t BgL_auxz00_3964;

																{	/* SawMill/remove.scm 64 */
																	BgL_objectz00_bglt BgL_tmpz00_3965;

																	BgL_tmpz00_3965 =
																		((BgL_objectz00_bglt) BgL_i1274z00_3009);
																	BgL_auxz00_3964 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_3965);
																}
																BgL_auxz00_3963 =
																	((BgL_cregz00_bglt) BgL_auxz00_3964);
															}
															BgL_arg1753z00_3010 =
																(((BgL_cregz00_bglt) COBJECT(BgL_auxz00_3963))->
																BgL_defsz00);
														}
														BgL_auxz00_3962 =
															MAKE_YOUNG_PAIR(BgL_insz00_2074,
															BgL_arg1753z00_3010);
													}
													{
														obj_t BgL_auxz00_3957;

														{	/* SawMill/remove.scm 64 */
															BgL_objectz00_bglt BgL_tmpz00_3958;

															BgL_tmpz00_3958 =
																((BgL_objectz00_bglt) BgL_i1274z00_3009);
															BgL_auxz00_3957 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_3958);
														}
														BgL_auxz00_3956 =
															((BgL_cregz00_bglt) BgL_auxz00_3957);
													}
													((((BgL_cregz00_bglt) COBJECT(BgL_auxz00_3956))->
															BgL_defsz00) =
														((obj_t) BgL_auxz00_3962), BUNSPEC);
												}
											}
										}
									else
										{	/* SawMill/remove.scm 69 */
											BFALSE;
										}
									{	/* SawMill/remove.scm 70 */
										obj_t BgL_g1544z00_2078;

										BgL_g1544z00_2078 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_insz00_2074)))->
											BgL_argsz00);
										{
											obj_t BgL_l1542z00_2080;

											BgL_l1542z00_2080 = BgL_g1544z00_2078;
										BgL_zc3z04anonymousza31734ze3z87_2081:
											if (PAIRP(BgL_l1542z00_2080))
												{	/* SawMill/remove.scm 70 */
													BgL_rz00_2106 = CAR(BgL_l1542z00_2080);
													{	/* SawMill/remove.scm 60 */
														BgL_rtl_regz00_bglt BgL_i1273z00_2108;

														BgL_i1273z00_2108 =
															((BgL_rtl_regz00_bglt)
															BGl_regzd2ze3cregze70zd6zzsaw_removez00
															(BgL_rz00_2106));
														{
															obj_t BgL_auxz00_3984;
															BgL_cregz00_bglt BgL_auxz00_3978;

															{	/* SawMill/remove.scm 61 */
																obj_t BgL_arg1751z00_2109;

																{
																	BgL_cregz00_bglt BgL_auxz00_3985;

																	{
																		obj_t BgL_auxz00_3986;

																		{	/* SawMill/remove.scm 61 */
																			BgL_objectz00_bglt BgL_tmpz00_3987;

																			BgL_tmpz00_3987 =
																				((BgL_objectz00_bglt)
																				BgL_i1273z00_2108);
																			BgL_auxz00_3986 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_3987);
																		}
																		BgL_auxz00_3985 =
																			((BgL_cregz00_bglt) BgL_auxz00_3986);
																	}
																	BgL_arg1751z00_2109 =
																		(((BgL_cregz00_bglt)
																			COBJECT(BgL_auxz00_3985))->BgL_nbusesz00);
																}
																BgL_auxz00_3984 =
																	ADDFX(BINT(1L), BgL_arg1751z00_2109);
															}
															{
																obj_t BgL_auxz00_3979;

																{	/* SawMill/remove.scm 61 */
																	BgL_objectz00_bglt BgL_tmpz00_3980;

																	BgL_tmpz00_3980 =
																		((BgL_objectz00_bglt) BgL_i1273z00_2108);
																	BgL_auxz00_3979 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_3980);
																}
																BgL_auxz00_3978 =
																	((BgL_cregz00_bglt) BgL_auxz00_3979);
															}
															((((BgL_cregz00_bglt) COBJECT(BgL_auxz00_3978))->
																	BgL_nbusesz00) =
																((obj_t) BgL_auxz00_3984), BUNSPEC);
														}
													}
													{
														obj_t BgL_l1542z00_3996;

														BgL_l1542z00_3996 = CDR(BgL_l1542z00_2080);
														BgL_l1542z00_2080 = BgL_l1542z00_3996;
														goto BgL_zc3z04anonymousza31734ze3z87_2081;
													}
												}
											else
												{	/* SawMill/remove.scm 70 */
													((bool_t) 1);
												}
										}
									}
								}
								{
									obj_t BgL_l1545z00_3998;

									BgL_l1545z00_3998 = CDR(BgL_l1545z00_2071);
									BgL_l1545z00_2071 = BgL_l1545z00_3998;
									goto BgL_zc3z04anonymousza31725ze3z87_2072;
								}
							}
						else
							{	/* SawMill/remove.scm 71 */
								((bool_t) 1);
							}
					}
				}
				{	/* SawMill/remove.scm 72 */
					obj_t BgL_g1550z00_2088;

					BgL_g1550z00_2088 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_bz00_2062)))->BgL_succsz00);
					{
						obj_t BgL_l1548z00_2090;

						BgL_l1548z00_2090 = BgL_g1550z00_2088;
					BgL_zc3z04anonymousza31740ze3z87_2091:
						if (PAIRP(BgL_l1548z00_2090))
							{	/* SawMill/remove.scm 73 */
								{	/* SawMill/remove.scm 72 */
									obj_t BgL_sz00_2093;

									BgL_sz00_2093 = CAR(BgL_l1548z00_2090);
									{	/* SawMill/remove.scm 72 */
										bool_t BgL_test2196z00_4005;

										{	/* SawMill/remove.scm 72 */
											obj_t BgL_classz00_3018;

											BgL_classz00_3018 = BGl_defcollectz00zzsaw_removez00;
											if (BGL_OBJECTP(BgL_sz00_2093))
												{	/* SawMill/remove.scm 72 */
													obj_t BgL_oclassz00_3020;

													{	/* SawMill/remove.scm 72 */
														obj_t BgL_arg1815z00_3022;
														long BgL_arg1816z00_3023;

														BgL_arg1815z00_3022 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawMill/remove.scm 72 */
															long BgL_arg1817z00_3024;

															BgL_arg1817z00_3024 =
																BGL_OBJECT_CLASS_NUM(
																((BgL_objectz00_bglt) BgL_sz00_2093));
															BgL_arg1816z00_3023 =
																(BgL_arg1817z00_3024 - OBJECT_TYPE);
														}
														BgL_oclassz00_3020 =
															VECTOR_REF(BgL_arg1815z00_3022,
															BgL_arg1816z00_3023);
													}
													BgL_test2196z00_4005 =
														(BgL_oclassz00_3020 == BgL_classz00_3018);
												}
											else
												{	/* SawMill/remove.scm 72 */
													BgL_test2196z00_4005 = ((bool_t) 0);
												}
										}
										if (BgL_test2196z00_4005)
											{	/* SawMill/remove.scm 72 */
												((bool_t) 0);
											}
										else
											{	/* SawMill/remove.scm 72 */
												BGl_defcollectze70ze7zzsaw_removez00(BgL_sz00_2093);
											}
									}
								}
								{
									obj_t BgL_l1548z00_4015;

									BgL_l1548z00_4015 = CDR(BgL_l1548z00_2090);
									BgL_l1548z00_2090 = BgL_l1548z00_4015;
									goto BgL_zc3z04anonymousza31740ze3z87_2091;
								}
							}
						else
							{	/* SawMill/remove.scm 73 */
								return ((bool_t) 1);
							}
					}
				}
			}
		}

	}



/* get-first-removable */
	obj_t BGl_getzd2firstzd2removablez00zzsaw_removez00(BgL_blockz00_bglt
		BgL_bz00_145)
	{
		{	/* SawMill/remove.scm 75 */
			{	/* SawMill/remove.scm 76 */
				struct bgl_cell BgL_box2198_3494z00;
				obj_t BgL_rmz00_3494;

				BgL_rmz00_3494 = MAKE_CELL_STACK(BNIL, BgL_box2198_3494z00);
				BGl_rcollectze70ze7zzsaw_removez00(BgL_rmz00_3494,
					((obj_t) BgL_bz00_145));
				return ((obj_t) ((obj_t) CELL_REF(BgL_rmz00_3494)));
			}
		}

	}



/* rcollect~0 */
	bool_t BGl_rcollectze70ze7zzsaw_removez00(obj_t BgL_rmz00_3492,
		obj_t BgL_bz00_2120)
	{
		{	/* SawMill/remove.scm 77 */
			{	/* SawMill/remove.scm 78 */
				BgL_rcollectz00_bglt BgL_wide1283z00_2124;

				BgL_wide1283z00_2124 =
					((BgL_rcollectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rcollectz00_bgl))));
				{	/* SawMill/remove.scm 78 */
					obj_t BgL_auxz00_4025;
					BgL_objectz00_bglt BgL_tmpz00_4021;

					BgL_auxz00_4025 = ((obj_t) BgL_wide1283z00_2124);
					BgL_tmpz00_4021 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2120)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4021, BgL_auxz00_4025);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2120)));
				{	/* SawMill/remove.scm 78 */
					long BgL_arg1755z00_2125;

					BgL_arg1755z00_2125 = BGL_CLASS_NUM(BGl_rcollectz00zzsaw_removez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_bz00_2120))), BgL_arg1755z00_2125);
				}
				((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2120)));
			}
			((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2120));
			{	/* SawMill/remove.scm 79 */
				obj_t BgL_g1553z00_2127;

				BgL_g1553z00_2127 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_bz00_2120)))->BgL_firstz00);
				{
					obj_t BgL_l1551z00_2129;

					BgL_l1551z00_2129 = BgL_g1553z00_2127;
				BgL_zc3z04anonymousza31756ze3z87_2130:
					if (PAIRP(BgL_l1551z00_2129))
						{	/* SawMill/remove.scm 80 */
							{	/* SawMill/remove.scm 79 */
								obj_t BgL_insz00_2132;

								BgL_insz00_2132 = CAR(BgL_l1551z00_2129);
								if (BGl_removablezf3zf3zzsaw_removez00(
										((BgL_rtl_insz00_bglt) BgL_insz00_2132)))
									{	/* SawMill/remove.scm 79 */
										obj_t BgL_auxz00_3493;

										BgL_auxz00_3493 =
											MAKE_YOUNG_PAIR(BgL_insz00_2132,
											((obj_t) ((obj_t) CELL_REF(BgL_rmz00_3492))));
										CELL_SET(BgL_rmz00_3492, BgL_auxz00_3493);
									}
								else
									{	/* SawMill/remove.scm 79 */
										BFALSE;
									}
							}
							{
								obj_t BgL_l1551z00_4051;

								BgL_l1551z00_4051 = CDR(BgL_l1551z00_2129);
								BgL_l1551z00_2129 = BgL_l1551z00_4051;
								goto BgL_zc3z04anonymousza31756ze3z87_2130;
							}
						}
					else
						{	/* SawMill/remove.scm 80 */
							((bool_t) 1);
						}
				}
			}
			{	/* SawMill/remove.scm 81 */
				obj_t BgL_g1556z00_2136;

				BgL_g1556z00_2136 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_bz00_2120)))->BgL_succsz00);
				{
					obj_t BgL_l1554z00_2138;

					BgL_l1554z00_2138 = BgL_g1556z00_2136;
				BgL_zc3z04anonymousza31762ze3z87_2139:
					if (PAIRP(BgL_l1554z00_2138))
						{	/* SawMill/remove.scm 82 */
							{	/* SawMill/remove.scm 81 */
								obj_t BgL_sz00_2141;

								BgL_sz00_2141 = CAR(BgL_l1554z00_2138);
								{	/* SawMill/remove.scm 81 */
									bool_t BgL_test2202z00_4058;

									{	/* SawMill/remove.scm 81 */
										obj_t BgL_classz00_3040;

										BgL_classz00_3040 = BGl_rcollectz00zzsaw_removez00;
										if (BGL_OBJECTP(BgL_sz00_2141))
											{	/* SawMill/remove.scm 81 */
												obj_t BgL_oclassz00_3042;

												{	/* SawMill/remove.scm 81 */
													obj_t BgL_arg1815z00_3044;
													long BgL_arg1816z00_3045;

													BgL_arg1815z00_3044 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/remove.scm 81 */
														long BgL_arg1817z00_3046;

														BgL_arg1817z00_3046 =
															BGL_OBJECT_CLASS_NUM(
															((BgL_objectz00_bglt) BgL_sz00_2141));
														BgL_arg1816z00_3045 =
															(BgL_arg1817z00_3046 - OBJECT_TYPE);
													}
													BgL_oclassz00_3042 =
														VECTOR_REF(BgL_arg1815z00_3044,
														BgL_arg1816z00_3045);
												}
												BgL_test2202z00_4058 =
													(BgL_oclassz00_3042 == BgL_classz00_3040);
											}
										else
											{	/* SawMill/remove.scm 81 */
												BgL_test2202z00_4058 = ((bool_t) 0);
											}
									}
									if (BgL_test2202z00_4058)
										{	/* SawMill/remove.scm 81 */
											((bool_t) 0);
										}
									else
										{	/* SawMill/remove.scm 81 */
											BGl_rcollectze70ze7zzsaw_removez00(BgL_rmz00_3492,
												BgL_sz00_2141);
										}
								}
							}
							{
								obj_t BgL_l1554z00_4068;

								BgL_l1554z00_4068 = CDR(BgL_l1554z00_2138);
								BgL_l1554z00_2138 = BgL_l1554z00_4068;
								goto BgL_zc3z04anonymousza31762ze3z87_2139;
							}
						}
					else
						{	/* SawMill/remove.scm 82 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* get-first-unused */
	obj_t BGl_getzd2firstzd2unusedz00zzsaw_removez00(BgL_blockz00_bglt
		BgL_bz00_146)
	{
		{	/* SawMill/remove.scm 85 */
			{	/* SawMill/remove.scm 86 */
				struct bgl_cell BgL_box2204_3490z00;
				obj_t BgL_unusedz00_3490;

				BgL_unusedz00_3490 = MAKE_CELL_STACK(BNIL, BgL_box2204_3490z00);
				BGl_ucollectze70ze7zzsaw_removez00(BgL_unusedz00_3490,
					((obj_t) BgL_bz00_146));
				return ((obj_t) ((obj_t) CELL_REF(BgL_unusedz00_3490)));
			}
		}

	}



/* ucollect~0 */
	bool_t BGl_ucollectze70ze7zzsaw_removez00(obj_t BgL_unusedz00_3488,
		obj_t BgL_bz00_2148)
	{
		{	/* SawMill/remove.scm 87 */
			{	/* SawMill/remove.scm 88 */
				BgL_ucollectz00_bglt BgL_wide1287z00_2152;

				BgL_wide1287z00_2152 =
					((BgL_ucollectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ucollectz00_bgl))));
				{	/* SawMill/remove.scm 88 */
					obj_t BgL_auxz00_4078;
					BgL_objectz00_bglt BgL_tmpz00_4074;

					BgL_auxz00_4078 = ((obj_t) BgL_wide1287z00_2152);
					BgL_tmpz00_4074 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2148)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4074, BgL_auxz00_4078);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2148)));
				{	/* SawMill/remove.scm 88 */
					long BgL_arg1767z00_2153;

					BgL_arg1767z00_2153 = BGL_CLASS_NUM(BGl_ucollectz00zzsaw_removez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_bz00_2148))), BgL_arg1767z00_2153);
				}
				((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2148)));
			}
			((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2148));
			{	/* SawMill/remove.scm 89 */
				obj_t BgL_g1559z00_2155;

				BgL_g1559z00_2155 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_bz00_2148)))->BgL_firstz00);
				{
					obj_t BgL_l1557z00_2157;

					BgL_l1557z00_2157 = BgL_g1559z00_2155;
				BgL_zc3z04anonymousza31768ze3z87_2158:
					if (PAIRP(BgL_l1557z00_2157))
						{	/* SawMill/remove.scm 96 */
							{	/* SawMill/remove.scm 90 */
								obj_t BgL_insz00_2160;

								BgL_insz00_2160 = CAR(BgL_l1557z00_2157);
								{	/* SawMill/remove.scm 92 */
									bool_t BgL_test2206z00_4099;

									if (CBOOL(
											(((BgL_rtl_insz00_bglt) COBJECT(
														((BgL_rtl_insz00_bglt) BgL_insz00_2160)))->
												BgL_destz00)))
										{	/* SawMill/remove.scm 93 */
											bool_t BgL_test2208z00_4104;

											{	/* SawMill/remove.scm 93 */
												obj_t BgL_arg1820z00_2177;

												{	/* SawMill/remove.scm 93 */
													BgL_rtl_regz00_bglt BgL_oz00_3059;

													BgL_oz00_3059 =
														((BgL_rtl_regz00_bglt)
														(((BgL_rtl_insz00_bglt) COBJECT(
																	((BgL_rtl_insz00_bglt) BgL_insz00_2160)))->
															BgL_destz00));
													{
														BgL_cregz00_bglt BgL_auxz00_4108;

														{
															obj_t BgL_auxz00_4109;

															{	/* SawMill/remove.scm 93 */
																BgL_objectz00_bglt BgL_tmpz00_4110;

																BgL_tmpz00_4110 =
																	((BgL_objectz00_bglt) BgL_oz00_3059);
																BgL_auxz00_4109 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_4110);
															}
															BgL_auxz00_4108 =
																((BgL_cregz00_bglt) BgL_auxz00_4109);
														}
														BgL_arg1820z00_2177 =
															(((BgL_cregz00_bglt) COBJECT(BgL_auxz00_4108))->
															BgL_nbusesz00);
													}
												}
												BgL_test2208z00_4104 =
													((long) CINT(BgL_arg1820z00_2177) == 0L);
											}
											if (BgL_test2208z00_4104)
												{	/* SawMill/remove.scm 94 */
													bool_t BgL_test2209z00_4117;

													{	/* SawMill/remove.scm 94 */
														obj_t BgL_arg1812z00_2176;

														BgL_arg1812z00_2176 =
															(((BgL_rtl_insz00_bglt) COBJECT(
																	((BgL_rtl_insz00_bglt) BgL_insz00_2160)))->
															BgL_destz00);
														BgL_test2209z00_4117 =
															CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
															(BgL_arg1812z00_2176,
																((obj_t) ((obj_t)
																		CELL_REF(BgL_unusedz00_3488)))));
													}
													if (BgL_test2209z00_4117)
														{	/* SawMill/remove.scm 94 */
															BgL_test2206z00_4099 = ((bool_t) 0);
														}
													else
														{	/* SawMill/remove.scm 94 */
															BgL_test2206z00_4099 = ((bool_t) 1);
														}
												}
											else
												{	/* SawMill/remove.scm 93 */
													BgL_test2206z00_4099 = ((bool_t) 0);
												}
										}
									else
										{	/* SawMill/remove.scm 92 */
											BgL_test2206z00_4099 = ((bool_t) 0);
										}
									if (BgL_test2206z00_4099)
										{	/* SawMill/remove.scm 95 */
											obj_t BgL_auxz00_3489;

											{	/* SawMill/remove.scm 95 */
												obj_t BgL_arg1808z00_2172;

												BgL_arg1808z00_2172 =
													(((BgL_rtl_insz00_bglt) COBJECT(
															((BgL_rtl_insz00_bglt) BgL_insz00_2160)))->
													BgL_destz00);
												BgL_auxz00_3489 =
													MAKE_YOUNG_PAIR(BgL_arg1808z00_2172,
													((obj_t) ((obj_t) CELL_REF(BgL_unusedz00_3488))));
											}
											CELL_SET(BgL_unusedz00_3488, BgL_auxz00_3489);
										}
									else
										{	/* SawMill/remove.scm 92 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1557z00_4127;

								BgL_l1557z00_4127 = CDR(BgL_l1557z00_2157);
								BgL_l1557z00_2157 = BgL_l1557z00_4127;
								goto BgL_zc3z04anonymousza31768ze3z87_2158;
							}
						}
					else
						{	/* SawMill/remove.scm 96 */
							((bool_t) 1);
						}
				}
			}
			{	/* SawMill/remove.scm 97 */
				obj_t BgL_g1562z00_2181;

				BgL_g1562z00_2181 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_bz00_2148)))->BgL_succsz00);
				{
					obj_t BgL_l1560z00_2183;

					BgL_l1560z00_2183 = BgL_g1562z00_2181;
				BgL_zc3z04anonymousza31824ze3z87_2184:
					if (PAIRP(BgL_l1560z00_2183))
						{	/* SawMill/remove.scm 98 */
							{	/* SawMill/remove.scm 97 */
								obj_t BgL_sz00_2186;

								BgL_sz00_2186 = CAR(BgL_l1560z00_2183);
								{	/* SawMill/remove.scm 97 */
									bool_t BgL_test2211z00_4134;

									{	/* SawMill/remove.scm 97 */
										obj_t BgL_classz00_3065;

										BgL_classz00_3065 = BGl_ucollectz00zzsaw_removez00;
										if (BGL_OBJECTP(BgL_sz00_2186))
											{	/* SawMill/remove.scm 97 */
												obj_t BgL_oclassz00_3067;

												{	/* SawMill/remove.scm 97 */
													obj_t BgL_arg1815z00_3069;
													long BgL_arg1816z00_3070;

													BgL_arg1815z00_3069 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/remove.scm 97 */
														long BgL_arg1817z00_3071;

														BgL_arg1817z00_3071 =
															BGL_OBJECT_CLASS_NUM(
															((BgL_objectz00_bglt) BgL_sz00_2186));
														BgL_arg1816z00_3070 =
															(BgL_arg1817z00_3071 - OBJECT_TYPE);
													}
													BgL_oclassz00_3067 =
														VECTOR_REF(BgL_arg1815z00_3069,
														BgL_arg1816z00_3070);
												}
												BgL_test2211z00_4134 =
													(BgL_oclassz00_3067 == BgL_classz00_3065);
											}
										else
											{	/* SawMill/remove.scm 97 */
												BgL_test2211z00_4134 = ((bool_t) 0);
											}
									}
									if (BgL_test2211z00_4134)
										{	/* SawMill/remove.scm 97 */
											((bool_t) 0);
										}
									else
										{	/* SawMill/remove.scm 97 */
											BGl_ucollectze70ze7zzsaw_removez00(BgL_unusedz00_3488,
												BgL_sz00_2186);
										}
								}
							}
							{
								obj_t BgL_l1560z00_4144;

								BgL_l1560z00_4144 = CDR(BgL_l1560z00_2183);
								BgL_l1560z00_2183 = BgL_l1560z00_4144;
								goto BgL_zc3z04anonymousza31824ze3z87_2184;
							}
						}
					else
						{	/* SawMill/remove.scm 98 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* fix-remove */
	bool_t BGl_fixzd2removezd2zzsaw_removez00(obj_t BgL_unusedz00_147,
		obj_t BgL_rmz00_148)
	{
		{	/* SawMill/remove.scm 104 */
		BGl_fixzd2removezd2zzsaw_removez00:
			{
				obj_t BgL_l1566z00_2192;

				BgL_l1566z00_2192 = BgL_rmz00_148;
			BgL_zc3z04anonymousza31832ze3z87_2193:
				if (PAIRP(BgL_l1566z00_2192))
					{	/* SawMill/remove.scm 106 */
						{	/* SawMill/remove.scm 107 */
							obj_t BgL_insz00_2195;

							BgL_insz00_2195 = CAR(BgL_l1566z00_2192);
							{	/* SawMill/remove.scm 107 */
								BgL_removedz00_bglt BgL_wide1292z00_2198;

								BgL_wide1292z00_2198 =
									((BgL_removedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_removedz00_bgl))));
								{	/* SawMill/remove.scm 107 */
									obj_t BgL_auxz00_4154;
									BgL_objectz00_bglt BgL_tmpz00_4150;

									BgL_auxz00_4154 = ((obj_t) BgL_wide1292z00_2198);
									BgL_tmpz00_4150 =
										((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt)
											((BgL_rtl_insz00_bglt) BgL_insz00_2195)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4150, BgL_auxz00_4154);
								}
								((BgL_objectz00_bglt)
									((BgL_rtl_insz00_bglt)
										((BgL_rtl_insz00_bglt) BgL_insz00_2195)));
								{	/* SawMill/remove.scm 107 */
									long BgL_arg1834z00_2199;

									BgL_arg1834z00_2199 =
										BGL_CLASS_NUM(BGl_removedz00zzsaw_removez00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_rtl_insz00_bglt)
												((BgL_rtl_insz00_bglt) BgL_insz00_2195))),
										BgL_arg1834z00_2199);
								}
								((BgL_rtl_insz00_bglt)
									((BgL_rtl_insz00_bglt)
										((BgL_rtl_insz00_bglt) BgL_insz00_2195)));
							}
							((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_insz00_2195));
							{	/* SawMill/remove.scm 108 */
								obj_t BgL_g1565z00_2201;

								BgL_g1565z00_2201 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_insz00_2195)))->BgL_argsz00);
								{
									obj_t BgL_l1563z00_2203;

									BgL_l1563z00_2203 = BgL_g1565z00_2201;
								BgL_zc3z04anonymousza31835ze3z87_2204:
									if (PAIRP(BgL_l1563z00_2203))
										{	/* SawMill/remove.scm 113 */
											{	/* SawMill/remove.scm 109 */
												obj_t BgL_rz00_2206;

												BgL_rz00_2206 = CAR(BgL_l1563z00_2203);
												{	/* SawMill/remove.scm 109 */
													obj_t BgL_nz00_2207;

													{
														BgL_cregz00_bglt BgL_auxz00_4175;

														{
															obj_t BgL_auxz00_4176;

															{	/* SawMill/remove.scm 109 */
																BgL_objectz00_bglt BgL_tmpz00_4177;

																BgL_tmpz00_4177 =
																	((BgL_objectz00_bglt)
																	((BgL_rtl_regz00_bglt) BgL_rz00_2206));
																BgL_auxz00_4176 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_4177);
															}
															BgL_auxz00_4175 =
																((BgL_cregz00_bglt) BgL_auxz00_4176);
														}
														BgL_nz00_2207 =
															(((BgL_cregz00_bglt) COBJECT(BgL_auxz00_4175))->
															BgL_nbusesz00);
													}
													{	/* SawMill/remove.scm 110 */
														long BgL_arg1837z00_2208;

														BgL_arg1837z00_2208 =
															((long) CINT(BgL_nz00_2207) - 1L);
														{
															BgL_cregz00_bglt BgL_auxz00_4185;

															{
																obj_t BgL_auxz00_4186;

																{	/* SawMill/remove.scm 110 */
																	BgL_objectz00_bglt BgL_tmpz00_4187;

																	BgL_tmpz00_4187 =
																		((BgL_objectz00_bglt)
																		((BgL_rtl_regz00_bglt) BgL_rz00_2206));
																	BgL_auxz00_4186 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_4187);
																}
																BgL_auxz00_4185 =
																	((BgL_cregz00_bglt) BgL_auxz00_4186);
															}
															((((BgL_cregz00_bglt) COBJECT(BgL_auxz00_4185))->
																	BgL_nbusesz00) =
																((obj_t) BINT(BgL_arg1837z00_2208)), BUNSPEC);
													}}
													if (((long) CINT(BgL_nz00_2207) == 1L))
														{	/* SawMill/remove.scm 111 */
															BgL_unusedz00_147 =
																MAKE_YOUNG_PAIR(BgL_rz00_2206,
																BgL_unusedz00_147);
														}
													else
														{	/* SawMill/remove.scm 111 */
															BFALSE;
														}
												}
											}
											{
												obj_t BgL_l1563z00_4198;

												BgL_l1563z00_4198 = CDR(BgL_l1563z00_2203);
												BgL_l1563z00_2203 = BgL_l1563z00_4198;
												goto BgL_zc3z04anonymousza31835ze3z87_2204;
											}
										}
									else
										{	/* SawMill/remove.scm 113 */
											((bool_t) 1);
										}
								}
							}
						}
						{
							obj_t BgL_l1566z00_4200;

							BgL_l1566z00_4200 = CDR(BgL_l1566z00_2192);
							BgL_l1566z00_2192 = BgL_l1566z00_4200;
							goto BgL_zc3z04anonymousza31832ze3z87_2193;
						}
					}
				else
					{	/* SawMill/remove.scm 106 */
						((bool_t) 1);
					}
			}
			BgL_rmz00_148 = BNIL;
			{
				obj_t BgL_l1571z00_2215;

				BgL_l1571z00_2215 = BgL_unusedz00_147;
			BgL_zc3z04anonymousza31841ze3z87_2216:
				if (PAIRP(BgL_l1571z00_2215))
					{	/* SawMill/remove.scm 117 */
						{	/* SawMill/remove.scm 118 */
							obj_t BgL_rz00_2218;

							BgL_rz00_2218 = CAR(BgL_l1571z00_2215);
							{	/* SawMill/remove.scm 118 */
								obj_t BgL_g1570z00_2219;

								{
									BgL_cregz00_bglt BgL_auxz00_4205;

									{
										obj_t BgL_auxz00_4206;

										{	/* SawMill/remove.scm 121 */
											BgL_objectz00_bglt BgL_tmpz00_4207;

											BgL_tmpz00_4207 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_rz00_2218));
											BgL_auxz00_4206 = BGL_OBJECT_WIDENING(BgL_tmpz00_4207);
										}
										BgL_auxz00_4205 = ((BgL_cregz00_bglt) BgL_auxz00_4206);
									}
									BgL_g1570z00_2219 =
										(((BgL_cregz00_bglt) COBJECT(BgL_auxz00_4205))->
										BgL_defsz00);
								}
								{
									obj_t BgL_l1568z00_2221;

									BgL_l1568z00_2221 = BgL_g1570z00_2219;
								BgL_zc3z04anonymousza31843ze3z87_2222:
									if (PAIRP(BgL_l1568z00_2221))
										{	/* SawMill/remove.scm 121 */
											{	/* SawMill/remove.scm 119 */
												obj_t BgL_insz00_2224;

												BgL_insz00_2224 = CAR(BgL_l1568z00_2221);
												((((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt) BgL_insz00_2224)))->
														BgL_destz00) = ((obj_t) BFALSE), BUNSPEC);
												if (BGl_removablezf3zf3zzsaw_removez00((
															(BgL_rtl_insz00_bglt) BgL_insz00_2224)))
													{	/* SawMill/remove.scm 120 */
														BgL_rmz00_148 =
															MAKE_YOUNG_PAIR(BgL_insz00_2224, BgL_rmz00_148);
													}
												else
													{	/* SawMill/remove.scm 120 */
														BFALSE;
													}
											}
											{
												obj_t BgL_l1568z00_4222;

												BgL_l1568z00_4222 = CDR(BgL_l1568z00_2221);
												BgL_l1568z00_2221 = BgL_l1568z00_4222;
												goto BgL_zc3z04anonymousza31843ze3z87_2222;
											}
										}
									else
										{	/* SawMill/remove.scm 121 */
											((bool_t) 1);
										}
								}
							}
						}
						{
							obj_t BgL_l1571z00_4224;

							BgL_l1571z00_4224 = CDR(BgL_l1571z00_2215);
							BgL_l1571z00_2215 = BgL_l1571z00_4224;
							goto BgL_zc3z04anonymousza31841ze3z87_2216;
						}
					}
				else
					{	/* SawMill/remove.scm 117 */
						((bool_t) 1);
					}
			}
			if (NULLP(BgL_rmz00_148))
				{	/* SawMill/remove.scm 124 */
					return ((bool_t) 0);
				}
			else
				{
					obj_t BgL_unusedz00_4228;

					BgL_unusedz00_4228 = BNIL;
					BgL_unusedz00_147 = BgL_unusedz00_4228;
					goto BGl_fixzd2removezd2zzsaw_removez00;
				}
		}

	}



/* removable? */
	bool_t BGl_removablezf3zf3zzsaw_removez00(BgL_rtl_insz00_bglt BgL_insz00_149)
	{
		{	/* SawMill/remove.scm 129 */
			if (CBOOL((((BgL_rtl_insz00_bglt) COBJECT(BgL_insz00_149))->BgL_destz00)))
				{	/* SawMill/remove.scm 131 */
					return ((bool_t) 0);
				}
			else
				{	/* SawMill/remove.scm 132 */
					BgL_rtl_funz00_bglt BgL_arg1849z00_2233;

					BgL_arg1849z00_2233 =
						(((BgL_rtl_insz00_bglt) COBJECT(BgL_insz00_149))->BgL_funz00);
					{	/* SawMill/remove.scm 132 */
						obj_t BgL_classz00_3101;

						BgL_classz00_3101 = BGl_rtl_purez00zzsaw_defsz00;
						{	/* SawMill/remove.scm 132 */
							BgL_objectz00_bglt BgL_arg1807z00_3103;

							{	/* SawMill/remove.scm 132 */
								obj_t BgL_tmpz00_4233;

								BgL_tmpz00_4233 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg1849z00_2233));
								BgL_arg1807z00_3103 = (BgL_objectz00_bglt) (BgL_tmpz00_4233);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawMill/remove.scm 132 */
									long BgL_idxz00_3109;

									BgL_idxz00_3109 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3103);
									return
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3109 + 2L)) == BgL_classz00_3101);
								}
							else
								{	/* SawMill/remove.scm 132 */
									bool_t BgL_res2097z00_3134;

									{	/* SawMill/remove.scm 132 */
										obj_t BgL_oclassz00_3117;

										{	/* SawMill/remove.scm 132 */
											obj_t BgL_arg1815z00_3125;
											long BgL_arg1816z00_3126;

											BgL_arg1815z00_3125 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawMill/remove.scm 132 */
												long BgL_arg1817z00_3127;

												BgL_arg1817z00_3127 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3103);
												BgL_arg1816z00_3126 =
													(BgL_arg1817z00_3127 - OBJECT_TYPE);
											}
											BgL_oclassz00_3117 =
												VECTOR_REF(BgL_arg1815z00_3125, BgL_arg1816z00_3126);
										}
										{	/* SawMill/remove.scm 132 */
											bool_t BgL__ortest_1115z00_3118;

											BgL__ortest_1115z00_3118 =
												(BgL_classz00_3101 == BgL_oclassz00_3117);
											if (BgL__ortest_1115z00_3118)
												{	/* SawMill/remove.scm 132 */
													BgL_res2097z00_3134 = BgL__ortest_1115z00_3118;
												}
											else
												{	/* SawMill/remove.scm 132 */
													long BgL_odepthz00_3119;

													{	/* SawMill/remove.scm 132 */
														obj_t BgL_arg1804z00_3120;

														BgL_arg1804z00_3120 = (BgL_oclassz00_3117);
														BgL_odepthz00_3119 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3120);
													}
													if ((2L < BgL_odepthz00_3119))
														{	/* SawMill/remove.scm 132 */
															obj_t BgL_arg1802z00_3122;

															{	/* SawMill/remove.scm 132 */
																obj_t BgL_arg1803z00_3123;

																BgL_arg1803z00_3123 = (BgL_oclassz00_3117);
																BgL_arg1802z00_3122 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3123,
																	2L);
															}
															BgL_res2097z00_3134 =
																(BgL_arg1802z00_3122 == BgL_classz00_3101);
														}
													else
														{	/* SawMill/remove.scm 132 */
															BgL_res2097z00_3134 = ((bool_t) 0);
														}
												}
										}
									}
									return BgL_res2097z00_3134;
								}
						}
					}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_removez00(void)
	{
		{	/* SawMill/remove.scm 1 */
			{	/* SawMill/remove.scm 8 */
				obj_t BgL_arg1854z00_2238;
				obj_t BgL_arg1856z00_2239;

				{	/* SawMill/remove.scm 8 */
					obj_t BgL_v1573z00_2264;

					BgL_v1573z00_2264 = create_vector(0L);
					BgL_arg1854z00_2238 = BgL_v1573z00_2264;
				}
				{	/* SawMill/remove.scm 8 */
					obj_t BgL_v1574z00_2265;

					BgL_v1574z00_2265 = create_vector(0L);
					BgL_arg1856z00_2239 = BgL_v1574z00_2265;
				}
				BGl_defcollectz00zzsaw_removez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(1),
					CNST_TABLE_REF(2), BGl_blockz00zzsaw_defsz00, 10433L,
					BGl_proc2104z00zzsaw_removez00, BGl_proc2103z00zzsaw_removez00,
					BFALSE, BGl_proc2102z00zzsaw_removez00,
					BGl_proc2101z00zzsaw_removez00, BgL_arg1854z00_2238,
					BgL_arg1856z00_2239);
			}
			{	/* SawMill/remove.scm 9 */
				obj_t BgL_arg1875z00_2274;
				obj_t BgL_arg1876z00_2275;

				{	/* SawMill/remove.scm 9 */
					obj_t BgL_v1575z00_2300;

					BgL_v1575z00_2300 = create_vector(0L);
					BgL_arg1875z00_2274 = BgL_v1575z00_2300;
				}
				{	/* SawMill/remove.scm 9 */
					obj_t BgL_v1576z00_2301;

					BgL_v1576z00_2301 = create_vector(0L);
					BgL_arg1876z00_2275 = BgL_v1576z00_2301;
				}
				BGl_rcollectz00zzsaw_removez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(3),
					CNST_TABLE_REF(2), BGl_blockz00zzsaw_defsz00, 28326L,
					BGl_proc2108z00zzsaw_removez00, BGl_proc2107z00zzsaw_removez00,
					BFALSE, BGl_proc2106z00zzsaw_removez00,
					BGl_proc2105z00zzsaw_removez00, BgL_arg1875z00_2274,
					BgL_arg1876z00_2275);
			}
			{	/* SawMill/remove.scm 10 */
				obj_t BgL_arg1893z00_2310;
				obj_t BgL_arg1894z00_2311;

				{	/* SawMill/remove.scm 10 */
					obj_t BgL_v1577z00_2336;

					BgL_v1577z00_2336 = create_vector(0L);
					BgL_arg1893z00_2310 = BgL_v1577z00_2336;
				}
				{	/* SawMill/remove.scm 10 */
					obj_t BgL_v1578z00_2337;

					BgL_v1578z00_2337 = create_vector(0L);
					BgL_arg1894z00_2311 = BgL_v1578z00_2337;
				}
				BGl_ucollectz00zzsaw_removez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(4),
					CNST_TABLE_REF(2), BGl_blockz00zzsaw_defsz00, 55259L,
					BGl_proc2112z00zzsaw_removez00, BGl_proc2111z00zzsaw_removez00,
					BFALSE, BGl_proc2110z00zzsaw_removez00,
					BGl_proc2109z00zzsaw_removez00, BgL_arg1893z00_2310,
					BgL_arg1894z00_2311);
			}
			{	/* SawMill/remove.scm 11 */
				obj_t BgL_arg1913z00_2346;
				obj_t BgL_arg1914z00_2347;

				{	/* SawMill/remove.scm 11 */
					obj_t BgL_v1580z00_2373;

					BgL_v1580z00_2373 = create_vector(0L);
					BgL_arg1913z00_2346 = BgL_v1580z00_2373;
				}
				{	/* SawMill/remove.scm 11 */
					obj_t BgL_v1581z00_2374;

					BgL_v1581z00_2374 = create_vector(0L);
					BgL_arg1914z00_2347 = BgL_v1581z00_2374;
				}
				BGl_removedz00zzsaw_removez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(5),
					CNST_TABLE_REF(2), BGl_rtl_insz00zzsaw_defsz00, 19548L,
					BGl_proc2116z00zzsaw_removez00, BGl_proc2115z00zzsaw_removez00,
					BFALSE, BGl_proc2114z00zzsaw_removez00,
					BGl_proc2113z00zzsaw_removez00, BgL_arg1913z00_2346,
					BgL_arg1914z00_2347);
			}
			{	/* SawMill/remove.scm 12 */
				obj_t BgL_arg1930z00_2383;
				obj_t BgL_arg1931z00_2384;

				{	/* SawMill/remove.scm 12 */
					obj_t BgL_v1582z00_2409;

					BgL_v1582z00_2409 = create_vector(0L);
					BgL_arg1930z00_2383 = BgL_v1582z00_2409;
				}
				{	/* SawMill/remove.scm 12 */
					obj_t BgL_v1583z00_2410;

					BgL_v1583z00_2410 = create_vector(0L);
					BgL_arg1931z00_2384 = BgL_v1583z00_2410;
				}
				BGl_visitedz00zzsaw_removez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(6),
					CNST_TABLE_REF(2), BGl_blockz00zzsaw_defsz00, 59014L,
					BGl_proc2120z00zzsaw_removez00, BGl_proc2119z00zzsaw_removez00,
					BFALSE, BGl_proc2118z00zzsaw_removez00,
					BGl_proc2117z00zzsaw_removez00, BgL_arg1930z00_2383,
					BgL_arg1931z00_2384);
			}
			{	/* SawMill/remove.scm 13 */
				obj_t BgL_arg1946z00_2419;
				obj_t BgL_arg1947z00_2420;

				{	/* SawMill/remove.scm 13 */
					obj_t BgL_v1584z00_2445;

					BgL_v1584z00_2445 = create_vector(0L);
					BgL_arg1946z00_2419 = BgL_v1584z00_2445;
				}
				{	/* SawMill/remove.scm 13 */
					obj_t BgL_v1585z00_2446;

					BgL_v1585z00_2446 = create_vector(0L);
					BgL_arg1947z00_2420 = BgL_v1585z00_2446;
				}
				BGl_bremovedz00zzsaw_removez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(7),
					CNST_TABLE_REF(2), BGl_blockz00zzsaw_defsz00, 18178L,
					BGl_proc2124z00zzsaw_removez00, BGl_proc2123z00zzsaw_removez00,
					BFALSE, BGl_proc2122z00zzsaw_removez00,
					BGl_proc2121z00zzsaw_removez00, BgL_arg1946z00_2419,
					BgL_arg1947z00_2420);
			}
			{	/* SawMill/remove.scm 14 */
				obj_t BgL_arg1962z00_2455;
				obj_t BgL_arg1963z00_2456;

				{	/* SawMill/remove.scm 14 */
					obj_t BgL_v1586z00_2486;

					BgL_v1586z00_2486 = create_vector(2L);
					{	/* SawMill/remove.scm 14 */
						obj_t BgL_arg1974z00_2487;

						BgL_arg1974z00_2487 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc2126z00zzsaw_removez00, BGl_proc2125z00zzsaw_removez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(9));
						VECTOR_SET(BgL_v1586z00_2486, 0L, BgL_arg1974z00_2487);
					}
					{	/* SawMill/remove.scm 14 */
						obj_t BgL_arg1979z00_2497;

						BgL_arg1979z00_2497 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(10),
							BGl_proc2128z00zzsaw_removez00, BGl_proc2127z00zzsaw_removez00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(9));
						VECTOR_SET(BgL_v1586z00_2486, 1L, BgL_arg1979z00_2497);
					}
					BgL_arg1962z00_2455 = BgL_v1586z00_2486;
				}
				{	/* SawMill/remove.scm 14 */
					obj_t BgL_v1588z00_2507;

					BgL_v1588z00_2507 = create_vector(0L);
					BgL_arg1963z00_2456 = BgL_v1588z00_2507;
				}
				return (BGl_cregz00zzsaw_removez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(11),
						CNST_TABLE_REF(2), BGl_rtl_regz00zzsaw_defsz00, 9551L,
						BGl_proc2132z00zzsaw_removez00, BGl_proc2131z00zzsaw_removez00,
						BFALSE, BGl_proc2130z00zzsaw_removez00,
						BGl_proc2129z00zzsaw_removez00, BgL_arg1962z00_2455,
						BgL_arg1963z00_2456), BUNSPEC);
			}
		}

	}



/* &lambda1970 */
	BgL_rtl_regz00_bglt BGl_z62lambda1970z62zzsaw_removez00(obj_t BgL_envz00_3395,
		obj_t BgL_o1223z00_3396)
	{
		{	/* SawMill/remove.scm 14 */
			{	/* SawMill/remove.scm 14 */
				long BgL_arg1971z00_3514;

				{	/* SawMill/remove.scm 14 */
					obj_t BgL_arg1972z00_3515;

					{	/* SawMill/remove.scm 14 */
						obj_t BgL_arg1973z00_3516;

						{	/* SawMill/remove.scm 14 */
							obj_t BgL_arg1815z00_3517;
							long BgL_arg1816z00_3518;

							BgL_arg1815z00_3517 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/remove.scm 14 */
								long BgL_arg1817z00_3519;

								BgL_arg1817z00_3519 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_o1223z00_3396)));
								BgL_arg1816z00_3518 = (BgL_arg1817z00_3519 - OBJECT_TYPE);
							}
							BgL_arg1973z00_3516 =
								VECTOR_REF(BgL_arg1815z00_3517, BgL_arg1816z00_3518);
						}
						BgL_arg1972z00_3515 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1973z00_3516);
					}
					{	/* SawMill/remove.scm 14 */
						obj_t BgL_tmpz00_4306;

						BgL_tmpz00_4306 = ((obj_t) BgL_arg1972z00_3515);
						BgL_arg1971z00_3514 = BGL_CLASS_NUM(BgL_tmpz00_4306);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_rtl_regz00_bglt) BgL_o1223z00_3396)), BgL_arg1971z00_3514);
			}
			{	/* SawMill/remove.scm 14 */
				BgL_objectz00_bglt BgL_tmpz00_4312;

				BgL_tmpz00_4312 =
					((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1223z00_3396));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4312, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1223z00_3396));
			return ((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1223z00_3396));
		}

	}



/* &<@anonymous:1969> */
	obj_t BGl_z62zc3z04anonymousza31969ze3ze5zzsaw_removez00(obj_t
		BgL_envz00_3397, obj_t BgL_new1222z00_3398)
	{
		{	/* SawMill/remove.scm 14 */
			{
				BgL_rtl_regz00_bglt BgL_auxz00_4320;

				{
					BgL_typez00_bglt BgL_auxz00_4321;

					{	/* SawMill/remove.scm 14 */
						obj_t BgL_classz00_3521;

						BgL_classz00_3521 = BGl_typez00zztype_typez00;
						{	/* SawMill/remove.scm 14 */
							obj_t BgL__ortest_1117z00_3522;

							BgL__ortest_1117z00_3522 = BGL_CLASS_NIL(BgL_classz00_3521);
							if (CBOOL(BgL__ortest_1117z00_3522))
								{	/* SawMill/remove.scm 14 */
									BgL_auxz00_4321 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3522);
								}
							else
								{	/* SawMill/remove.scm 14 */
									BgL_auxz00_4321 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3521));
								}
						}
					}
					((((BgL_rtl_regz00_bglt) COBJECT(
									((BgL_rtl_regz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_new1222z00_3398))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_4321), BUNSPEC);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt)
									((BgL_rtl_regz00_bglt) BgL_new1222z00_3398))))->BgL_varz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1222z00_3398))))->BgL_onexprzf3zf3) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1222z00_3398))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1222z00_3398))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1222z00_3398))))->BgL_debugnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1222z00_3398))))->BgL_hardwarez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_cregz00_bglt BgL_auxz00_4349;

					{
						obj_t BgL_auxz00_4350;

						{	/* SawMill/remove.scm 14 */
							BgL_objectz00_bglt BgL_tmpz00_4351;

							BgL_tmpz00_4351 =
								((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_new1222z00_3398));
							BgL_auxz00_4350 = BGL_OBJECT_WIDENING(BgL_tmpz00_4351);
						}
						BgL_auxz00_4349 = ((BgL_cregz00_bglt) BgL_auxz00_4350);
					}
					((((BgL_cregz00_bglt) COBJECT(BgL_auxz00_4349))->BgL_defsz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_cregz00_bglt BgL_auxz00_4357;

					{
						obj_t BgL_auxz00_4358;

						{	/* SawMill/remove.scm 14 */
							BgL_objectz00_bglt BgL_tmpz00_4359;

							BgL_tmpz00_4359 =
								((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_new1222z00_3398));
							BgL_auxz00_4358 = BGL_OBJECT_WIDENING(BgL_tmpz00_4359);
						}
						BgL_auxz00_4357 = ((BgL_cregz00_bglt) BgL_auxz00_4358);
					}
					((((BgL_cregz00_bglt) COBJECT(BgL_auxz00_4357))->BgL_nbusesz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_4320 = ((BgL_rtl_regz00_bglt) BgL_new1222z00_3398);
				return ((obj_t) BgL_auxz00_4320);
			}
		}

	}



/* &lambda1967 */
	BgL_rtl_regz00_bglt BGl_z62lambda1967z62zzsaw_removez00(obj_t BgL_envz00_3399,
		obj_t BgL_o1219z00_3400)
	{
		{	/* SawMill/remove.scm 14 */
			{	/* SawMill/remove.scm 14 */
				BgL_cregz00_bglt BgL_wide1221z00_3524;

				BgL_wide1221z00_3524 =
					((BgL_cregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_cregz00_bgl))));
				{	/* SawMill/remove.scm 14 */
					obj_t BgL_auxz00_4372;
					BgL_objectz00_bglt BgL_tmpz00_4368;

					BgL_auxz00_4372 = ((obj_t) BgL_wide1221z00_3524);
					BgL_tmpz00_4368 =
						((BgL_objectz00_bglt)
						((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1219z00_3400)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4368, BgL_auxz00_4372);
				}
				((BgL_objectz00_bglt)
					((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1219z00_3400)));
				{	/* SawMill/remove.scm 14 */
					long BgL_arg1968z00_3525;

					BgL_arg1968z00_3525 = BGL_CLASS_NUM(BGl_cregz00zzsaw_removez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_rtl_regz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_o1219z00_3400))),
						BgL_arg1968z00_3525);
				}
				return
					((BgL_rtl_regz00_bglt)
					((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1219z00_3400)));
			}
		}

	}



/* &lambda1964 */
	BgL_rtl_regz00_bglt BGl_z62lambda1964z62zzsaw_removez00(obj_t BgL_envz00_3401,
		obj_t BgL_type1210z00_3402, obj_t BgL_var1211z00_3403,
		obj_t BgL_onexprzf31212zf3_3404, obj_t BgL_name1213z00_3405,
		obj_t BgL_key1214z00_3406, obj_t BgL_debugname1215z00_3407,
		obj_t BgL_hardware1216z00_3408, obj_t BgL_defs1217z00_3409,
		obj_t BgL_nbuses1218z00_3410)
	{
		{	/* SawMill/remove.scm 14 */
			{	/* SawMill/remove.scm 14 */
				BgL_rtl_regz00_bglt BgL_new1332z00_3527;

				{	/* SawMill/remove.scm 14 */
					BgL_rtl_regz00_bglt BgL_tmp1330z00_3528;
					BgL_cregz00_bglt BgL_wide1331z00_3529;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_4386;

						{	/* SawMill/remove.scm 14 */
							BgL_rtl_regz00_bglt BgL_new1329z00_3530;

							BgL_new1329z00_3530 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/remove.scm 14 */
								long BgL_arg1966z00_3531;

								BgL_arg1966z00_3531 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1329z00_3530),
									BgL_arg1966z00_3531);
							}
							{	/* SawMill/remove.scm 14 */
								BgL_objectz00_bglt BgL_tmpz00_4391;

								BgL_tmpz00_4391 = ((BgL_objectz00_bglt) BgL_new1329z00_3530);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4391, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1329z00_3530);
							BgL_auxz00_4386 = BgL_new1329z00_3530;
						}
						BgL_tmp1330z00_3528 = ((BgL_rtl_regz00_bglt) BgL_auxz00_4386);
					}
					BgL_wide1331z00_3529 =
						((BgL_cregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_cregz00_bgl))));
					{	/* SawMill/remove.scm 14 */
						obj_t BgL_auxz00_4399;
						BgL_objectz00_bglt BgL_tmpz00_4397;

						BgL_auxz00_4399 = ((obj_t) BgL_wide1331z00_3529);
						BgL_tmpz00_4397 = ((BgL_objectz00_bglt) BgL_tmp1330z00_3528);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4397, BgL_auxz00_4399);
					}
					((BgL_objectz00_bglt) BgL_tmp1330z00_3528);
					{	/* SawMill/remove.scm 14 */
						long BgL_arg1965z00_3532;

						BgL_arg1965z00_3532 = BGL_CLASS_NUM(BGl_cregz00zzsaw_removez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1330z00_3528), BgL_arg1965z00_3532);
					}
					BgL_new1332z00_3527 = ((BgL_rtl_regz00_bglt) BgL_tmp1330z00_3528);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1332z00_3527)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1210z00_3402)),
					BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1332z00_3527)))->BgL_varz00) =
					((obj_t) BgL_var1211z00_3403), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1332z00_3527)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31212zf3_3404), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1332z00_3527)))->BgL_namez00) =
					((obj_t) BgL_name1213z00_3405), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1332z00_3527)))->BgL_keyz00) =
					((obj_t) BgL_key1214z00_3406), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1332z00_3527)))->BgL_debugnamez00) =
					((obj_t) BgL_debugname1215z00_3407), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1332z00_3527)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1216z00_3408), BUNSPEC);
				{
					BgL_cregz00_bglt BgL_auxz00_4422;

					{
						obj_t BgL_auxz00_4423;

						{	/* SawMill/remove.scm 14 */
							BgL_objectz00_bglt BgL_tmpz00_4424;

							BgL_tmpz00_4424 = ((BgL_objectz00_bglt) BgL_new1332z00_3527);
							BgL_auxz00_4423 = BGL_OBJECT_WIDENING(BgL_tmpz00_4424);
						}
						BgL_auxz00_4422 = ((BgL_cregz00_bglt) BgL_auxz00_4423);
					}
					((((BgL_cregz00_bglt) COBJECT(BgL_auxz00_4422))->BgL_defsz00) =
						((obj_t) BgL_defs1217z00_3409), BUNSPEC);
				}
				{
					BgL_cregz00_bglt BgL_auxz00_4429;

					{
						obj_t BgL_auxz00_4430;

						{	/* SawMill/remove.scm 14 */
							BgL_objectz00_bglt BgL_tmpz00_4431;

							BgL_tmpz00_4431 = ((BgL_objectz00_bglt) BgL_new1332z00_3527);
							BgL_auxz00_4430 = BGL_OBJECT_WIDENING(BgL_tmpz00_4431);
						}
						BgL_auxz00_4429 = ((BgL_cregz00_bglt) BgL_auxz00_4430);
					}
					((((BgL_cregz00_bglt) COBJECT(BgL_auxz00_4429))->BgL_nbusesz00) =
						((obj_t) BgL_nbuses1218z00_3410), BUNSPEC);
				}
				return BgL_new1332z00_3527;
			}
		}

	}



/* &lambda1983 */
	obj_t BGl_z62lambda1983z62zzsaw_removez00(obj_t BgL_envz00_3411,
		obj_t BgL_oz00_3412, obj_t BgL_vz00_3413)
	{
		{	/* SawMill/remove.scm 14 */
			{
				BgL_cregz00_bglt BgL_auxz00_4436;

				{
					obj_t BgL_auxz00_4437;

					{	/* SawMill/remove.scm 14 */
						BgL_objectz00_bglt BgL_tmpz00_4438;

						BgL_tmpz00_4438 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3412));
						BgL_auxz00_4437 = BGL_OBJECT_WIDENING(BgL_tmpz00_4438);
					}
					BgL_auxz00_4436 = ((BgL_cregz00_bglt) BgL_auxz00_4437);
				}
				return
					((((BgL_cregz00_bglt) COBJECT(BgL_auxz00_4436))->BgL_nbusesz00) =
					((obj_t) BgL_vz00_3413), BUNSPEC);
			}
		}

	}



/* &lambda1982 */
	obj_t BGl_z62lambda1982z62zzsaw_removez00(obj_t BgL_envz00_3414,
		obj_t BgL_oz00_3415)
	{
		{	/* SawMill/remove.scm 14 */
			{
				BgL_cregz00_bglt BgL_auxz00_4444;

				{
					obj_t BgL_auxz00_4445;

					{	/* SawMill/remove.scm 14 */
						BgL_objectz00_bglt BgL_tmpz00_4446;

						BgL_tmpz00_4446 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3415));
						BgL_auxz00_4445 = BGL_OBJECT_WIDENING(BgL_tmpz00_4446);
					}
					BgL_auxz00_4444 = ((BgL_cregz00_bglt) BgL_auxz00_4445);
				}
				return (((BgL_cregz00_bglt) COBJECT(BgL_auxz00_4444))->BgL_nbusesz00);
			}
		}

	}



/* &lambda1978 */
	obj_t BGl_z62lambda1978z62zzsaw_removez00(obj_t BgL_envz00_3416,
		obj_t BgL_oz00_3417, obj_t BgL_vz00_3418)
	{
		{	/* SawMill/remove.scm 14 */
			{
				BgL_cregz00_bglt BgL_auxz00_4452;

				{
					obj_t BgL_auxz00_4453;

					{	/* SawMill/remove.scm 14 */
						BgL_objectz00_bglt BgL_tmpz00_4454;

						BgL_tmpz00_4454 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3417));
						BgL_auxz00_4453 = BGL_OBJECT_WIDENING(BgL_tmpz00_4454);
					}
					BgL_auxz00_4452 = ((BgL_cregz00_bglt) BgL_auxz00_4453);
				}
				return
					((((BgL_cregz00_bglt) COBJECT(BgL_auxz00_4452))->BgL_defsz00) =
					((obj_t) BgL_vz00_3418), BUNSPEC);
			}
		}

	}



/* &lambda1977 */
	obj_t BGl_z62lambda1977z62zzsaw_removez00(obj_t BgL_envz00_3419,
		obj_t BgL_oz00_3420)
	{
		{	/* SawMill/remove.scm 14 */
			{
				BgL_cregz00_bglt BgL_auxz00_4460;

				{
					obj_t BgL_auxz00_4461;

					{	/* SawMill/remove.scm 14 */
						BgL_objectz00_bglt BgL_tmpz00_4462;

						BgL_tmpz00_4462 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3420));
						BgL_auxz00_4461 = BGL_OBJECT_WIDENING(BgL_tmpz00_4462);
					}
					BgL_auxz00_4460 = ((BgL_cregz00_bglt) BgL_auxz00_4461);
				}
				return (((BgL_cregz00_bglt) COBJECT(BgL_auxz00_4460))->BgL_defsz00);
			}
		}

	}



/* &lambda1954 */
	BgL_blockz00_bglt BGl_z62lambda1954z62zzsaw_removez00(obj_t BgL_envz00_3421,
		obj_t BgL_o1208z00_3422)
	{
		{	/* SawMill/remove.scm 13 */
			{	/* SawMill/remove.scm 13 */
				long BgL_arg1955z00_3538;

				{	/* SawMill/remove.scm 13 */
					obj_t BgL_arg1956z00_3539;

					{	/* SawMill/remove.scm 13 */
						obj_t BgL_arg1957z00_3540;

						{	/* SawMill/remove.scm 13 */
							obj_t BgL_arg1815z00_3541;
							long BgL_arg1816z00_3542;

							BgL_arg1815z00_3541 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/remove.scm 13 */
								long BgL_arg1817z00_3543;

								BgL_arg1817z00_3543 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_o1208z00_3422)));
								BgL_arg1816z00_3542 = (BgL_arg1817z00_3543 - OBJECT_TYPE);
							}
							BgL_arg1957z00_3540 =
								VECTOR_REF(BgL_arg1815z00_3541, BgL_arg1816z00_3542);
						}
						BgL_arg1956z00_3539 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1957z00_3540);
					}
					{	/* SawMill/remove.scm 13 */
						obj_t BgL_tmpz00_4475;

						BgL_tmpz00_4475 = ((obj_t) BgL_arg1956z00_3539);
						BgL_arg1955z00_3538 = BGL_CLASS_NUM(BgL_tmpz00_4475);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) BgL_o1208z00_3422)), BgL_arg1955z00_3538);
			}
			{	/* SawMill/remove.scm 13 */
				BgL_objectz00_bglt BgL_tmpz00_4481;

				BgL_tmpz00_4481 =
					((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1208z00_3422));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4481, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1208z00_3422));
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1208z00_3422));
		}

	}



/* &<@anonymous:1953> */
	obj_t BGl_z62zc3z04anonymousza31953ze3ze5zzsaw_removez00(obj_t
		BgL_envz00_3423, obj_t BgL_new1207z00_3424)
	{
		{	/* SawMill/remove.scm 13 */
			{
				BgL_blockz00_bglt BgL_auxz00_4489;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_new1207z00_3424))))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1207z00_3424))))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1207z00_3424))))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1207z00_3424))))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_4489 = ((BgL_blockz00_bglt) BgL_new1207z00_3424);
				return ((obj_t) BgL_auxz00_4489);
			}
		}

	}



/* &lambda1951 */
	BgL_blockz00_bglt BGl_z62lambda1951z62zzsaw_removez00(obj_t BgL_envz00_3425,
		obj_t BgL_o1204z00_3426)
	{
		{	/* SawMill/remove.scm 13 */
			{	/* SawMill/remove.scm 13 */
				BgL_bremovedz00_bglt BgL_wide1206z00_3546;

				BgL_wide1206z00_3546 =
					((BgL_bremovedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_bremovedz00_bgl))));
				{	/* SawMill/remove.scm 13 */
					obj_t BgL_auxz00_4510;
					BgL_objectz00_bglt BgL_tmpz00_4506;

					BgL_auxz00_4510 = ((obj_t) BgL_wide1206z00_3546);
					BgL_tmpz00_4506 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1204z00_3426)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4506, BgL_auxz00_4510);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1204z00_3426)));
				{	/* SawMill/remove.scm 13 */
					long BgL_arg1952z00_3547;

					BgL_arg1952z00_3547 = BGL_CLASS_NUM(BGl_bremovedz00zzsaw_removez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_o1204z00_3426))), BgL_arg1952z00_3547);
				}
				return
					((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1204z00_3426)));
			}
		}

	}



/* &lambda1948 */
	BgL_blockz00_bglt BGl_z62lambda1948z62zzsaw_removez00(obj_t BgL_envz00_3427,
		obj_t BgL_label1200z00_3428, obj_t BgL_preds1201z00_3429,
		obj_t BgL_succs1202z00_3430, obj_t BgL_first1203z00_3431)
	{
		{	/* SawMill/remove.scm 13 */
			{	/* SawMill/remove.scm 13 */
				int BgL_label1200z00_3548;

				BgL_label1200z00_3548 = CINT(BgL_label1200z00_3428);
				{	/* SawMill/remove.scm 13 */
					BgL_blockz00_bglt BgL_new1327z00_3552;

					{	/* SawMill/remove.scm 13 */
						BgL_blockz00_bglt BgL_tmp1325z00_3553;
						BgL_bremovedz00_bglt BgL_wide1326z00_3554;

						{
							BgL_blockz00_bglt BgL_auxz00_4525;

							{	/* SawMill/remove.scm 13 */
								BgL_blockz00_bglt BgL_new1324z00_3555;

								BgL_new1324z00_3555 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawMill/remove.scm 13 */
									long BgL_arg1950z00_3556;

									BgL_arg1950z00_3556 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1324z00_3555),
										BgL_arg1950z00_3556);
								}
								{	/* SawMill/remove.scm 13 */
									BgL_objectz00_bglt BgL_tmpz00_4530;

									BgL_tmpz00_4530 = ((BgL_objectz00_bglt) BgL_new1324z00_3555);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4530, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1324z00_3555);
								BgL_auxz00_4525 = BgL_new1324z00_3555;
							}
							BgL_tmp1325z00_3553 = ((BgL_blockz00_bglt) BgL_auxz00_4525);
						}
						BgL_wide1326z00_3554 =
							((BgL_bremovedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_bremovedz00_bgl))));
						{	/* SawMill/remove.scm 13 */
							obj_t BgL_auxz00_4538;
							BgL_objectz00_bglt BgL_tmpz00_4536;

							BgL_auxz00_4538 = ((obj_t) BgL_wide1326z00_3554);
							BgL_tmpz00_4536 = ((BgL_objectz00_bglt) BgL_tmp1325z00_3553);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4536, BgL_auxz00_4538);
						}
						((BgL_objectz00_bglt) BgL_tmp1325z00_3553);
						{	/* SawMill/remove.scm 13 */
							long BgL_arg1949z00_3557;

							BgL_arg1949z00_3557 =
								BGL_CLASS_NUM(BGl_bremovedz00zzsaw_removez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1325z00_3553),
								BgL_arg1949z00_3557);
						}
						BgL_new1327z00_3552 = ((BgL_blockz00_bglt) BgL_tmp1325z00_3553);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_new1327z00_3552)))->BgL_labelz00) =
						((int) BgL_label1200z00_3548), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1327z00_3552)))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1201z00_3429)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1327z00_3552)))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1202z00_3430)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1327z00_3552)))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1203z00_3431)), BUNSPEC);
					return BgL_new1327z00_3552;
				}
			}
		}

	}



/* &lambda1938 */
	BgL_blockz00_bglt BGl_z62lambda1938z62zzsaw_removez00(obj_t BgL_envz00_3432,
		obj_t BgL_o1198z00_3433)
	{
		{	/* SawMill/remove.scm 12 */
			{	/* SawMill/remove.scm 12 */
				long BgL_arg1939z00_3559;

				{	/* SawMill/remove.scm 12 */
					obj_t BgL_arg1940z00_3560;

					{	/* SawMill/remove.scm 12 */
						obj_t BgL_arg1941z00_3561;

						{	/* SawMill/remove.scm 12 */
							obj_t BgL_arg1815z00_3562;
							long BgL_arg1816z00_3563;

							BgL_arg1815z00_3562 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/remove.scm 12 */
								long BgL_arg1817z00_3564;

								BgL_arg1817z00_3564 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_o1198z00_3433)));
								BgL_arg1816z00_3563 = (BgL_arg1817z00_3564 - OBJECT_TYPE);
							}
							BgL_arg1941z00_3561 =
								VECTOR_REF(BgL_arg1815z00_3562, BgL_arg1816z00_3563);
						}
						BgL_arg1940z00_3560 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1941z00_3561);
					}
					{	/* SawMill/remove.scm 12 */
						obj_t BgL_tmpz00_4564;

						BgL_tmpz00_4564 = ((obj_t) BgL_arg1940z00_3560);
						BgL_arg1939z00_3559 = BGL_CLASS_NUM(BgL_tmpz00_4564);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) BgL_o1198z00_3433)), BgL_arg1939z00_3559);
			}
			{	/* SawMill/remove.scm 12 */
				BgL_objectz00_bglt BgL_tmpz00_4570;

				BgL_tmpz00_4570 =
					((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1198z00_3433));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4570, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1198z00_3433));
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1198z00_3433));
		}

	}



/* &<@anonymous:1937> */
	obj_t BGl_z62zc3z04anonymousza31937ze3ze5zzsaw_removez00(obj_t
		BgL_envz00_3434, obj_t BgL_new1197z00_3435)
	{
		{	/* SawMill/remove.scm 12 */
			{
				BgL_blockz00_bglt BgL_auxz00_4578;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_new1197z00_3435))))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1197z00_3435))))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1197z00_3435))))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1197z00_3435))))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_4578 = ((BgL_blockz00_bglt) BgL_new1197z00_3435);
				return ((obj_t) BgL_auxz00_4578);
			}
		}

	}



/* &lambda1935 */
	BgL_blockz00_bglt BGl_z62lambda1935z62zzsaw_removez00(obj_t BgL_envz00_3436,
		obj_t BgL_o1194z00_3437)
	{
		{	/* SawMill/remove.scm 12 */
			{	/* SawMill/remove.scm 12 */
				BgL_visitedz00_bglt BgL_wide1196z00_3567;

				BgL_wide1196z00_3567 =
					((BgL_visitedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_visitedz00_bgl))));
				{	/* SawMill/remove.scm 12 */
					obj_t BgL_auxz00_4599;
					BgL_objectz00_bglt BgL_tmpz00_4595;

					BgL_auxz00_4599 = ((obj_t) BgL_wide1196z00_3567);
					BgL_tmpz00_4595 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1194z00_3437)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4595, BgL_auxz00_4599);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1194z00_3437)));
				{	/* SawMill/remove.scm 12 */
					long BgL_arg1936z00_3568;

					BgL_arg1936z00_3568 = BGL_CLASS_NUM(BGl_visitedz00zzsaw_removez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_o1194z00_3437))), BgL_arg1936z00_3568);
				}
				return
					((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1194z00_3437)));
			}
		}

	}



/* &lambda1932 */
	BgL_blockz00_bglt BGl_z62lambda1932z62zzsaw_removez00(obj_t BgL_envz00_3438,
		obj_t BgL_label1190z00_3439, obj_t BgL_preds1191z00_3440,
		obj_t BgL_succs1192z00_3441, obj_t BgL_first1193z00_3442)
	{
		{	/* SawMill/remove.scm 12 */
			{	/* SawMill/remove.scm 12 */
				int BgL_label1190z00_3569;

				BgL_label1190z00_3569 = CINT(BgL_label1190z00_3439);
				{	/* SawMill/remove.scm 12 */
					BgL_blockz00_bglt BgL_new1322z00_3573;

					{	/* SawMill/remove.scm 12 */
						BgL_blockz00_bglt BgL_tmp1320z00_3574;
						BgL_visitedz00_bglt BgL_wide1321z00_3575;

						{
							BgL_blockz00_bglt BgL_auxz00_4614;

							{	/* SawMill/remove.scm 12 */
								BgL_blockz00_bglt BgL_new1319z00_3576;

								BgL_new1319z00_3576 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawMill/remove.scm 12 */
									long BgL_arg1934z00_3577;

									BgL_arg1934z00_3577 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1319z00_3576),
										BgL_arg1934z00_3577);
								}
								{	/* SawMill/remove.scm 12 */
									BgL_objectz00_bglt BgL_tmpz00_4619;

									BgL_tmpz00_4619 = ((BgL_objectz00_bglt) BgL_new1319z00_3576);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4619, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1319z00_3576);
								BgL_auxz00_4614 = BgL_new1319z00_3576;
							}
							BgL_tmp1320z00_3574 = ((BgL_blockz00_bglt) BgL_auxz00_4614);
						}
						BgL_wide1321z00_3575 =
							((BgL_visitedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_visitedz00_bgl))));
						{	/* SawMill/remove.scm 12 */
							obj_t BgL_auxz00_4627;
							BgL_objectz00_bglt BgL_tmpz00_4625;

							BgL_auxz00_4627 = ((obj_t) BgL_wide1321z00_3575);
							BgL_tmpz00_4625 = ((BgL_objectz00_bglt) BgL_tmp1320z00_3574);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4625, BgL_auxz00_4627);
						}
						((BgL_objectz00_bglt) BgL_tmp1320z00_3574);
						{	/* SawMill/remove.scm 12 */
							long BgL_arg1933z00_3578;

							BgL_arg1933z00_3578 =
								BGL_CLASS_NUM(BGl_visitedz00zzsaw_removez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1320z00_3574),
								BgL_arg1933z00_3578);
						}
						BgL_new1322z00_3573 = ((BgL_blockz00_bglt) BgL_tmp1320z00_3574);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_new1322z00_3573)))->BgL_labelz00) =
						((int) BgL_label1190z00_3569), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1322z00_3573)))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1191z00_3440)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1322z00_3573)))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1192z00_3441)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1322z00_3573)))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1193z00_3442)), BUNSPEC);
					return BgL_new1322z00_3573;
				}
			}
		}

	}



/* &lambda1921 */
	BgL_rtl_insz00_bglt BGl_z62lambda1921z62zzsaw_removez00(obj_t BgL_envz00_3443,
		obj_t BgL_o1188z00_3444)
	{
		{	/* SawMill/remove.scm 11 */
			{	/* SawMill/remove.scm 11 */
				long BgL_arg1923z00_3580;

				{	/* SawMill/remove.scm 11 */
					obj_t BgL_arg1924z00_3581;

					{	/* SawMill/remove.scm 11 */
						obj_t BgL_arg1925z00_3582;

						{	/* SawMill/remove.scm 11 */
							obj_t BgL_arg1815z00_3583;
							long BgL_arg1816z00_3584;

							BgL_arg1815z00_3583 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/remove.scm 11 */
								long BgL_arg1817z00_3585;

								BgL_arg1817z00_3585 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt) BgL_o1188z00_3444)));
								BgL_arg1816z00_3584 = (BgL_arg1817z00_3585 - OBJECT_TYPE);
							}
							BgL_arg1925z00_3582 =
								VECTOR_REF(BgL_arg1815z00_3583, BgL_arg1816z00_3584);
						}
						BgL_arg1924z00_3581 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1925z00_3582);
					}
					{	/* SawMill/remove.scm 11 */
						obj_t BgL_tmpz00_4653;

						BgL_tmpz00_4653 = ((obj_t) BgL_arg1924z00_3581);
						BgL_arg1923z00_3580 = BGL_CLASS_NUM(BgL_tmpz00_4653);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_rtl_insz00_bglt) BgL_o1188z00_3444)), BgL_arg1923z00_3580);
			}
			{	/* SawMill/remove.scm 11 */
				BgL_objectz00_bglt BgL_tmpz00_4659;

				BgL_tmpz00_4659 =
					((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1188z00_3444));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4659, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1188z00_3444));
			return ((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1188z00_3444));
		}

	}



/* &<@anonymous:1920> */
	obj_t BGl_z62zc3z04anonymousza31920ze3ze5zzsaw_removez00(obj_t
		BgL_envz00_3445, obj_t BgL_new1187z00_3446)
	{
		{	/* SawMill/remove.scm 11 */
			{
				BgL_rtl_insz00_bglt BgL_auxz00_4667;

				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt)
									((BgL_rtl_insz00_bglt) BgL_new1187z00_3446))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_insz00_bglt)
							COBJECT(((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt)
										BgL_new1187z00_3446))))->BgL_z52spillz52) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_rtl_insz00_bglt)
							COBJECT(((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt)
										BgL_new1187z00_3446))))->BgL_destz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_rtl_funz00_bglt BgL_auxz00_4677;

					{	/* SawMill/remove.scm 11 */
						obj_t BgL_classz00_3587;

						BgL_classz00_3587 = BGl_rtl_funz00zzsaw_defsz00;
						{	/* SawMill/remove.scm 11 */
							obj_t BgL__ortest_1117z00_3588;

							BgL__ortest_1117z00_3588 = BGL_CLASS_NIL(BgL_classz00_3587);
							if (CBOOL(BgL__ortest_1117z00_3588))
								{	/* SawMill/remove.scm 11 */
									BgL_auxz00_4677 =
										((BgL_rtl_funz00_bglt) BgL__ortest_1117z00_3588);
								}
							else
								{	/* SawMill/remove.scm 11 */
									BgL_auxz00_4677 =
										((BgL_rtl_funz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3587));
								}
						}
					}
					((((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt)
										((BgL_rtl_insz00_bglt) BgL_new1187z00_3446))))->
							BgL_funz00) = ((BgL_rtl_funz00_bglt) BgL_auxz00_4677), BUNSPEC);
				}
				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt)
									((BgL_rtl_insz00_bglt) BgL_new1187z00_3446))))->BgL_argsz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_4667 = ((BgL_rtl_insz00_bglt) BgL_new1187z00_3446);
				return ((obj_t) BgL_auxz00_4667);
			}
		}

	}



/* &lambda1918 */
	BgL_rtl_insz00_bglt BGl_z62lambda1918z62zzsaw_removez00(obj_t BgL_envz00_3447,
		obj_t BgL_o1184z00_3448)
	{
		{	/* SawMill/remove.scm 11 */
			{	/* SawMill/remove.scm 11 */
				BgL_removedz00_bglt BgL_wide1186z00_3590;

				BgL_wide1186z00_3590 =
					((BgL_removedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_removedz00_bgl))));
				{	/* SawMill/remove.scm 11 */
					obj_t BgL_auxz00_4697;
					BgL_objectz00_bglt BgL_tmpz00_4693;

					BgL_auxz00_4697 = ((obj_t) BgL_wide1186z00_3590);
					BgL_tmpz00_4693 =
						((BgL_objectz00_bglt)
						((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1184z00_3448)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4693, BgL_auxz00_4697);
				}
				((BgL_objectz00_bglt)
					((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1184z00_3448)));
				{	/* SawMill/remove.scm 11 */
					long BgL_arg1919z00_3591;

					BgL_arg1919z00_3591 = BGL_CLASS_NUM(BGl_removedz00zzsaw_removez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_rtl_insz00_bglt)
								((BgL_rtl_insz00_bglt) BgL_o1184z00_3448))),
						BgL_arg1919z00_3591);
				}
				return
					((BgL_rtl_insz00_bglt)
					((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1184z00_3448)));
			}
		}

	}



/* &lambda1915 */
	BgL_rtl_insz00_bglt BGl_z62lambda1915z62zzsaw_removez00(obj_t BgL_envz00_3449,
		obj_t BgL_loc1179z00_3450, obj_t BgL_z52spill1180z52_3451,
		obj_t BgL_dest1181z00_3452, obj_t BgL_fun1182z00_3453,
		obj_t BgL_args1183z00_3454)
	{
		{	/* SawMill/remove.scm 11 */
			{	/* SawMill/remove.scm 11 */
				BgL_rtl_insz00_bglt BgL_new1316z00_3595;

				{	/* SawMill/remove.scm 11 */
					BgL_rtl_insz00_bglt BgL_tmp1314z00_3596;
					BgL_removedz00_bglt BgL_wide1315z00_3597;

					{
						BgL_rtl_insz00_bglt BgL_auxz00_4711;

						{	/* SawMill/remove.scm 11 */
							BgL_rtl_insz00_bglt BgL_new1313z00_3598;

							BgL_new1313z00_3598 =
								((BgL_rtl_insz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_insz00_bgl))));
							{	/* SawMill/remove.scm 11 */
								long BgL_arg1917z00_3599;

								BgL_arg1917z00_3599 =
									BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1313z00_3598),
									BgL_arg1917z00_3599);
							}
							{	/* SawMill/remove.scm 11 */
								BgL_objectz00_bglt BgL_tmpz00_4716;

								BgL_tmpz00_4716 = ((BgL_objectz00_bglt) BgL_new1313z00_3598);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4716, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1313z00_3598);
							BgL_auxz00_4711 = BgL_new1313z00_3598;
						}
						BgL_tmp1314z00_3596 = ((BgL_rtl_insz00_bglt) BgL_auxz00_4711);
					}
					BgL_wide1315z00_3597 =
						((BgL_removedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_removedz00_bgl))));
					{	/* SawMill/remove.scm 11 */
						obj_t BgL_auxz00_4724;
						BgL_objectz00_bglt BgL_tmpz00_4722;

						BgL_auxz00_4724 = ((obj_t) BgL_wide1315z00_3597);
						BgL_tmpz00_4722 = ((BgL_objectz00_bglt) BgL_tmp1314z00_3596);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4722, BgL_auxz00_4724);
					}
					((BgL_objectz00_bglt) BgL_tmp1314z00_3596);
					{	/* SawMill/remove.scm 11 */
						long BgL_arg1916z00_3600;

						BgL_arg1916z00_3600 = BGL_CLASS_NUM(BGl_removedz00zzsaw_removez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1314z00_3596), BgL_arg1916z00_3600);
					}
					BgL_new1316z00_3595 = ((BgL_rtl_insz00_bglt) BgL_tmp1314z00_3596);
				}
				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_new1316z00_3595)))->BgL_locz00) =
					((obj_t) BgL_loc1179z00_3450), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1316z00_3595)))->BgL_z52spillz52) =
					((obj_t) ((obj_t) BgL_z52spill1180z52_3451)), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1316z00_3595)))->BgL_destz00) =
					((obj_t) BgL_dest1181z00_3452), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1316z00_3595)))->BgL_funz00) =
					((BgL_rtl_funz00_bglt) ((BgL_rtl_funz00_bglt) BgL_fun1182z00_3453)),
					BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1316z00_3595)))->BgL_argsz00) =
					((obj_t) ((obj_t) BgL_args1183z00_3454)), BUNSPEC);
				return BgL_new1316z00_3595;
			}
		}

	}



/* &lambda1901 */
	BgL_blockz00_bglt BGl_z62lambda1901z62zzsaw_removez00(obj_t BgL_envz00_3455,
		obj_t BgL_o1177z00_3456)
	{
		{	/* SawMill/remove.scm 10 */
			{	/* SawMill/remove.scm 10 */
				long BgL_arg1902z00_3602;

				{	/* SawMill/remove.scm 10 */
					obj_t BgL_arg1903z00_3603;

					{	/* SawMill/remove.scm 10 */
						obj_t BgL_arg1904z00_3604;

						{	/* SawMill/remove.scm 10 */
							obj_t BgL_arg1815z00_3605;
							long BgL_arg1816z00_3606;

							BgL_arg1815z00_3605 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/remove.scm 10 */
								long BgL_arg1817z00_3607;

								BgL_arg1817z00_3607 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_o1177z00_3456)));
								BgL_arg1816z00_3606 = (BgL_arg1817z00_3607 - OBJECT_TYPE);
							}
							BgL_arg1904z00_3604 =
								VECTOR_REF(BgL_arg1815z00_3605, BgL_arg1816z00_3606);
						}
						BgL_arg1903z00_3603 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1904z00_3604);
					}
					{	/* SawMill/remove.scm 10 */
						obj_t BgL_tmpz00_4752;

						BgL_tmpz00_4752 = ((obj_t) BgL_arg1903z00_3603);
						BgL_arg1902z00_3602 = BGL_CLASS_NUM(BgL_tmpz00_4752);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) BgL_o1177z00_3456)), BgL_arg1902z00_3602);
			}
			{	/* SawMill/remove.scm 10 */
				BgL_objectz00_bglt BgL_tmpz00_4758;

				BgL_tmpz00_4758 =
					((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1177z00_3456));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4758, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1177z00_3456));
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1177z00_3456));
		}

	}



/* &<@anonymous:1900> */
	obj_t BGl_z62zc3z04anonymousza31900ze3ze5zzsaw_removez00(obj_t
		BgL_envz00_3457, obj_t BgL_new1176z00_3458)
	{
		{	/* SawMill/remove.scm 10 */
			{
				BgL_blockz00_bglt BgL_auxz00_4766;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_new1176z00_3458))))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1176z00_3458))))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1176z00_3458))))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1176z00_3458))))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_4766 = ((BgL_blockz00_bglt) BgL_new1176z00_3458);
				return ((obj_t) BgL_auxz00_4766);
			}
		}

	}



/* &lambda1898 */
	BgL_blockz00_bglt BGl_z62lambda1898z62zzsaw_removez00(obj_t BgL_envz00_3459,
		obj_t BgL_o1173z00_3460)
	{
		{	/* SawMill/remove.scm 10 */
			{	/* SawMill/remove.scm 10 */
				BgL_ucollectz00_bglt BgL_wide1175z00_3610;

				BgL_wide1175z00_3610 =
					((BgL_ucollectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_ucollectz00_bgl))));
				{	/* SawMill/remove.scm 10 */
					obj_t BgL_auxz00_4787;
					BgL_objectz00_bglt BgL_tmpz00_4783;

					BgL_auxz00_4787 = ((obj_t) BgL_wide1175z00_3610);
					BgL_tmpz00_4783 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1173z00_3460)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4783, BgL_auxz00_4787);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1173z00_3460)));
				{	/* SawMill/remove.scm 10 */
					long BgL_arg1899z00_3611;

					BgL_arg1899z00_3611 = BGL_CLASS_NUM(BGl_ucollectz00zzsaw_removez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_o1173z00_3460))), BgL_arg1899z00_3611);
				}
				return
					((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1173z00_3460)));
			}
		}

	}



/* &lambda1895 */
	BgL_blockz00_bglt BGl_z62lambda1895z62zzsaw_removez00(obj_t BgL_envz00_3461,
		obj_t BgL_label1169z00_3462, obj_t BgL_preds1170z00_3463,
		obj_t BgL_succs1171z00_3464, obj_t BgL_first1172z00_3465)
	{
		{	/* SawMill/remove.scm 10 */
			{	/* SawMill/remove.scm 10 */
				int BgL_label1169z00_3612;

				BgL_label1169z00_3612 = CINT(BgL_label1169z00_3462);
				{	/* SawMill/remove.scm 10 */
					BgL_blockz00_bglt BgL_new1310z00_3616;

					{	/* SawMill/remove.scm 10 */
						BgL_blockz00_bglt BgL_tmp1308z00_3617;
						BgL_ucollectz00_bglt BgL_wide1309z00_3618;

						{
							BgL_blockz00_bglt BgL_auxz00_4802;

							{	/* SawMill/remove.scm 10 */
								BgL_blockz00_bglt BgL_new1307z00_3619;

								BgL_new1307z00_3619 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawMill/remove.scm 10 */
									long BgL_arg1897z00_3620;

									BgL_arg1897z00_3620 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1307z00_3619),
										BgL_arg1897z00_3620);
								}
								{	/* SawMill/remove.scm 10 */
									BgL_objectz00_bglt BgL_tmpz00_4807;

									BgL_tmpz00_4807 = ((BgL_objectz00_bglt) BgL_new1307z00_3619);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4807, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1307z00_3619);
								BgL_auxz00_4802 = BgL_new1307z00_3619;
							}
							BgL_tmp1308z00_3617 = ((BgL_blockz00_bglt) BgL_auxz00_4802);
						}
						BgL_wide1309z00_3618 =
							((BgL_ucollectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_ucollectz00_bgl))));
						{	/* SawMill/remove.scm 10 */
							obj_t BgL_auxz00_4815;
							BgL_objectz00_bglt BgL_tmpz00_4813;

							BgL_auxz00_4815 = ((obj_t) BgL_wide1309z00_3618);
							BgL_tmpz00_4813 = ((BgL_objectz00_bglt) BgL_tmp1308z00_3617);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4813, BgL_auxz00_4815);
						}
						((BgL_objectz00_bglt) BgL_tmp1308z00_3617);
						{	/* SawMill/remove.scm 10 */
							long BgL_arg1896z00_3621;

							BgL_arg1896z00_3621 =
								BGL_CLASS_NUM(BGl_ucollectz00zzsaw_removez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1308z00_3617),
								BgL_arg1896z00_3621);
						}
						BgL_new1310z00_3616 = ((BgL_blockz00_bglt) BgL_tmp1308z00_3617);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_new1310z00_3616)))->BgL_labelz00) =
						((int) BgL_label1169z00_3612), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1310z00_3616)))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1170z00_3463)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1310z00_3616)))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1171z00_3464)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1310z00_3616)))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1172z00_3465)), BUNSPEC);
					return BgL_new1310z00_3616;
				}
			}
		}

	}



/* &lambda1884 */
	BgL_blockz00_bglt BGl_z62lambda1884z62zzsaw_removez00(obj_t BgL_envz00_3466,
		obj_t BgL_o1167z00_3467)
	{
		{	/* SawMill/remove.scm 9 */
			{	/* SawMill/remove.scm 9 */
				long BgL_arg1885z00_3623;

				{	/* SawMill/remove.scm 9 */
					obj_t BgL_arg1887z00_3624;

					{	/* SawMill/remove.scm 9 */
						obj_t BgL_arg1888z00_3625;

						{	/* SawMill/remove.scm 9 */
							obj_t BgL_arg1815z00_3626;
							long BgL_arg1816z00_3627;

							BgL_arg1815z00_3626 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/remove.scm 9 */
								long BgL_arg1817z00_3628;

								BgL_arg1817z00_3628 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_o1167z00_3467)));
								BgL_arg1816z00_3627 = (BgL_arg1817z00_3628 - OBJECT_TYPE);
							}
							BgL_arg1888z00_3625 =
								VECTOR_REF(BgL_arg1815z00_3626, BgL_arg1816z00_3627);
						}
						BgL_arg1887z00_3624 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1888z00_3625);
					}
					{	/* SawMill/remove.scm 9 */
						obj_t BgL_tmpz00_4841;

						BgL_tmpz00_4841 = ((obj_t) BgL_arg1887z00_3624);
						BgL_arg1885z00_3623 = BGL_CLASS_NUM(BgL_tmpz00_4841);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) BgL_o1167z00_3467)), BgL_arg1885z00_3623);
			}
			{	/* SawMill/remove.scm 9 */
				BgL_objectz00_bglt BgL_tmpz00_4847;

				BgL_tmpz00_4847 =
					((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1167z00_3467));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4847, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1167z00_3467));
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1167z00_3467));
		}

	}



/* &<@anonymous:1883> */
	obj_t BGl_z62zc3z04anonymousza31883ze3ze5zzsaw_removez00(obj_t
		BgL_envz00_3468, obj_t BgL_new1166z00_3469)
	{
		{	/* SawMill/remove.scm 9 */
			{
				BgL_blockz00_bglt BgL_auxz00_4855;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_new1166z00_3469))))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1166z00_3469))))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1166z00_3469))))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1166z00_3469))))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_4855 = ((BgL_blockz00_bglt) BgL_new1166z00_3469);
				return ((obj_t) BgL_auxz00_4855);
			}
		}

	}



/* &lambda1880 */
	BgL_blockz00_bglt BGl_z62lambda1880z62zzsaw_removez00(obj_t BgL_envz00_3470,
		obj_t BgL_o1163z00_3471)
	{
		{	/* SawMill/remove.scm 9 */
			{	/* SawMill/remove.scm 9 */
				BgL_rcollectz00_bglt BgL_wide1165z00_3631;

				BgL_wide1165z00_3631 =
					((BgL_rcollectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rcollectz00_bgl))));
				{	/* SawMill/remove.scm 9 */
					obj_t BgL_auxz00_4876;
					BgL_objectz00_bglt BgL_tmpz00_4872;

					BgL_auxz00_4876 = ((obj_t) BgL_wide1165z00_3631);
					BgL_tmpz00_4872 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1163z00_3471)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4872, BgL_auxz00_4876);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1163z00_3471)));
				{	/* SawMill/remove.scm 9 */
					long BgL_arg1882z00_3632;

					BgL_arg1882z00_3632 = BGL_CLASS_NUM(BGl_rcollectz00zzsaw_removez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_o1163z00_3471))), BgL_arg1882z00_3632);
				}
				return
					((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1163z00_3471)));
			}
		}

	}



/* &lambda1877 */
	BgL_blockz00_bglt BGl_z62lambda1877z62zzsaw_removez00(obj_t BgL_envz00_3472,
		obj_t BgL_label1159z00_3473, obj_t BgL_preds1160z00_3474,
		obj_t BgL_succs1161z00_3475, obj_t BgL_first1162z00_3476)
	{
		{	/* SawMill/remove.scm 9 */
			{	/* SawMill/remove.scm 9 */
				int BgL_label1159z00_3633;

				BgL_label1159z00_3633 = CINT(BgL_label1159z00_3473);
				{	/* SawMill/remove.scm 9 */
					BgL_blockz00_bglt BgL_new1304z00_3637;

					{	/* SawMill/remove.scm 9 */
						BgL_blockz00_bglt BgL_tmp1302z00_3638;
						BgL_rcollectz00_bglt BgL_wide1303z00_3639;

						{
							BgL_blockz00_bglt BgL_auxz00_4891;

							{	/* SawMill/remove.scm 9 */
								BgL_blockz00_bglt BgL_new1301z00_3640;

								BgL_new1301z00_3640 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawMill/remove.scm 9 */
									long BgL_arg1879z00_3641;

									BgL_arg1879z00_3641 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1301z00_3640),
										BgL_arg1879z00_3641);
								}
								{	/* SawMill/remove.scm 9 */
									BgL_objectz00_bglt BgL_tmpz00_4896;

									BgL_tmpz00_4896 = ((BgL_objectz00_bglt) BgL_new1301z00_3640);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4896, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1301z00_3640);
								BgL_auxz00_4891 = BgL_new1301z00_3640;
							}
							BgL_tmp1302z00_3638 = ((BgL_blockz00_bglt) BgL_auxz00_4891);
						}
						BgL_wide1303z00_3639 =
							((BgL_rcollectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rcollectz00_bgl))));
						{	/* SawMill/remove.scm 9 */
							obj_t BgL_auxz00_4904;
							BgL_objectz00_bglt BgL_tmpz00_4902;

							BgL_auxz00_4904 = ((obj_t) BgL_wide1303z00_3639);
							BgL_tmpz00_4902 = ((BgL_objectz00_bglt) BgL_tmp1302z00_3638);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4902, BgL_auxz00_4904);
						}
						((BgL_objectz00_bglt) BgL_tmp1302z00_3638);
						{	/* SawMill/remove.scm 9 */
							long BgL_arg1878z00_3642;

							BgL_arg1878z00_3642 =
								BGL_CLASS_NUM(BGl_rcollectz00zzsaw_removez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1302z00_3638),
								BgL_arg1878z00_3642);
						}
						BgL_new1304z00_3637 = ((BgL_blockz00_bglt) BgL_tmp1302z00_3638);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_new1304z00_3637)))->BgL_labelz00) =
						((int) BgL_label1159z00_3633), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1304z00_3637)))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1160z00_3474)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1304z00_3637)))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1161z00_3475)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1304z00_3637)))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1162z00_3476)), BUNSPEC);
					return BgL_new1304z00_3637;
				}
			}
		}

	}



/* &lambda1864 */
	BgL_blockz00_bglt BGl_z62lambda1864z62zzsaw_removez00(obj_t BgL_envz00_3477,
		obj_t BgL_o1157z00_3478)
	{
		{	/* SawMill/remove.scm 8 */
			{	/* SawMill/remove.scm 8 */
				long BgL_arg1866z00_3644;

				{	/* SawMill/remove.scm 8 */
					obj_t BgL_arg1868z00_3645;

					{	/* SawMill/remove.scm 8 */
						obj_t BgL_arg1869z00_3646;

						{	/* SawMill/remove.scm 8 */
							obj_t BgL_arg1815z00_3647;
							long BgL_arg1816z00_3648;

							BgL_arg1815z00_3647 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/remove.scm 8 */
								long BgL_arg1817z00_3649;

								BgL_arg1817z00_3649 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_o1157z00_3478)));
								BgL_arg1816z00_3648 = (BgL_arg1817z00_3649 - OBJECT_TYPE);
							}
							BgL_arg1869z00_3646 =
								VECTOR_REF(BgL_arg1815z00_3647, BgL_arg1816z00_3648);
						}
						BgL_arg1868z00_3645 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1869z00_3646);
					}
					{	/* SawMill/remove.scm 8 */
						obj_t BgL_tmpz00_4930;

						BgL_tmpz00_4930 = ((obj_t) BgL_arg1868z00_3645);
						BgL_arg1866z00_3644 = BGL_CLASS_NUM(BgL_tmpz00_4930);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) BgL_o1157z00_3478)), BgL_arg1866z00_3644);
			}
			{	/* SawMill/remove.scm 8 */
				BgL_objectz00_bglt BgL_tmpz00_4936;

				BgL_tmpz00_4936 =
					((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1157z00_3478));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4936, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1157z00_3478));
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1157z00_3478));
		}

	}



/* &<@anonymous:1863> */
	obj_t BGl_z62zc3z04anonymousza31863ze3ze5zzsaw_removez00(obj_t
		BgL_envz00_3479, obj_t BgL_new1156z00_3480)
	{
		{	/* SawMill/remove.scm 8 */
			{
				BgL_blockz00_bglt BgL_auxz00_4944;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_new1156z00_3480))))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1156z00_3480))))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1156z00_3480))))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1156z00_3480))))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_4944 = ((BgL_blockz00_bglt) BgL_new1156z00_3480);
				return ((obj_t) BgL_auxz00_4944);
			}
		}

	}



/* &lambda1860 */
	BgL_blockz00_bglt BGl_z62lambda1860z62zzsaw_removez00(obj_t BgL_envz00_3481,
		obj_t BgL_o1153z00_3482)
	{
		{	/* SawMill/remove.scm 8 */
			{	/* SawMill/remove.scm 8 */
				BgL_defcollectz00_bglt BgL_wide1155z00_3652;

				BgL_wide1155z00_3652 =
					((BgL_defcollectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_defcollectz00_bgl))));
				{	/* SawMill/remove.scm 8 */
					obj_t BgL_auxz00_4965;
					BgL_objectz00_bglt BgL_tmpz00_4961;

					BgL_auxz00_4965 = ((obj_t) BgL_wide1155z00_3652);
					BgL_tmpz00_4961 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1153z00_3482)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4961, BgL_auxz00_4965);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1153z00_3482)));
				{	/* SawMill/remove.scm 8 */
					long BgL_arg1862z00_3653;

					BgL_arg1862z00_3653 = BGL_CLASS_NUM(BGl_defcollectz00zzsaw_removez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_o1153z00_3482))), BgL_arg1862z00_3653);
				}
				return
					((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1153z00_3482)));
			}
		}

	}



/* &lambda1857 */
	BgL_blockz00_bglt BGl_z62lambda1857z62zzsaw_removez00(obj_t BgL_envz00_3483,
		obj_t BgL_label1149z00_3484, obj_t BgL_preds1150z00_3485,
		obj_t BgL_succs1151z00_3486, obj_t BgL_first1152z00_3487)
	{
		{	/* SawMill/remove.scm 8 */
			{	/* SawMill/remove.scm 8 */
				int BgL_label1149z00_3654;

				BgL_label1149z00_3654 = CINT(BgL_label1149z00_3484);
				{	/* SawMill/remove.scm 8 */
					BgL_blockz00_bglt BgL_new1299z00_3658;

					{	/* SawMill/remove.scm 8 */
						BgL_blockz00_bglt BgL_tmp1297z00_3659;
						BgL_defcollectz00_bglt BgL_wide1298z00_3660;

						{
							BgL_blockz00_bglt BgL_auxz00_4980;

							{	/* SawMill/remove.scm 8 */
								BgL_blockz00_bglt BgL_new1296z00_3661;

								BgL_new1296z00_3661 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawMill/remove.scm 8 */
									long BgL_arg1859z00_3662;

									BgL_arg1859z00_3662 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1296z00_3661),
										BgL_arg1859z00_3662);
								}
								{	/* SawMill/remove.scm 8 */
									BgL_objectz00_bglt BgL_tmpz00_4985;

									BgL_tmpz00_4985 = ((BgL_objectz00_bglt) BgL_new1296z00_3661);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4985, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1296z00_3661);
								BgL_auxz00_4980 = BgL_new1296z00_3661;
							}
							BgL_tmp1297z00_3659 = ((BgL_blockz00_bglt) BgL_auxz00_4980);
						}
						BgL_wide1298z00_3660 =
							((BgL_defcollectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_defcollectz00_bgl))));
						{	/* SawMill/remove.scm 8 */
							obj_t BgL_auxz00_4993;
							BgL_objectz00_bglt BgL_tmpz00_4991;

							BgL_auxz00_4993 = ((obj_t) BgL_wide1298z00_3660);
							BgL_tmpz00_4991 = ((BgL_objectz00_bglt) BgL_tmp1297z00_3659);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4991, BgL_auxz00_4993);
						}
						((BgL_objectz00_bglt) BgL_tmp1297z00_3659);
						{	/* SawMill/remove.scm 8 */
							long BgL_arg1858z00_3663;

							BgL_arg1858z00_3663 =
								BGL_CLASS_NUM(BGl_defcollectz00zzsaw_removez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1297z00_3659),
								BgL_arg1858z00_3663);
						}
						BgL_new1299z00_3658 = ((BgL_blockz00_bglt) BgL_tmp1297z00_3659);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_new1299z00_3658)))->BgL_labelz00) =
						((int) BgL_label1149z00_3654), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1299z00_3658)))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1150z00_3485)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1299z00_3658)))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1151z00_3486)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1299z00_3658)))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1152z00_3487)), BUNSPEC);
					return BgL_new1299z00_3658;
				}
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_removez00(void)
	{
		{	/* SawMill/remove.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_removez00(void)
	{
		{	/* SawMill/remove.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_removez00(void)
	{
		{	/* SawMill/remove.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2133z00zzsaw_removez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2133z00zzsaw_removez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2133z00zzsaw_removez00));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string2133z00zzsaw_removez00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string2133z00zzsaw_removez00));
		}

	}

#ifdef __cplusplus
}
#endif
