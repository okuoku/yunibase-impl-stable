/*===========================================================================*/
/*   (SawMill/inlinereturn.scm)                                              */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/inlinereturn.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_INLINE_RETURN_TYPE_DEFINITIONS
#define BGL_SAW_INLINE_RETURN_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_dfsz00_bgl
	{
	}             *BgL_dfsz00_bglt;


#endif													// BGL_SAW_INLINE_RETURN_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_inline_returnz00 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_toplevelzd2initzd2zzsaw_inline_returnz00(void);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_inline_returnz00(void);
	static obj_t BGl_findzd2returnszd2zzsaw_inline_returnz00(BgL_blockz00_bglt);
	static obj_t BGl_objectzd2initzd2zzsaw_inline_returnz00(void);
	static BgL_rtl_insz00_bglt BGl_renamezd2inszd2zzsaw_inline_returnz00(obj_t,
		obj_t);
	static obj_t BGl_reallocze70ze7zzsaw_inline_returnz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	extern obj_t BGl_rtl_movz00zzsaw_defsz00;
	static bool_t BGl_inlinezd2returnzd2zzsaw_inline_returnz00(BgL_blockz00_bglt);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzsaw_inline_returnz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_inline_returnz00(void);
	static obj_t BGl_dfsz00zzsaw_inline_returnz00 = BUNSPEC;
	static long BGl_za2inlinezd2depthza2zd2zzsaw_inline_returnz00 = 0L;
	BGL_IMPORT obj_t bgl_remq_bang(obj_t, obj_t);
	extern obj_t BGl_rtl_lastz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_inline_returnz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzsaw_inline_returnz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_inline_returnz00(void);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static bool_t BGl_dfsze70ze7zzsaw_inline_returnz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_inline_returnz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_inline_returnz00(void);
	static obj_t BGl_z62inlinezd2returnszb0zzsaw_inline_returnz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
	static bool_t BGl_simplezd2returnzf3ze70zc6zzsaw_inline_returnz00(obj_t,
		long);
	static BgL_rtl_regz00_bglt BGl_copyzd2regzd2zzsaw_inline_returnz00(obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31664ze3ze5zzsaw_inline_returnz00(obj_t,
		obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1652z62zzsaw_inline_returnz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1662z62zzsaw_inline_returnz00(obj_t,
		obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1665z62zzsaw_inline_returnz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_inlinezd2returnszd2zzsaw_inline_returnz00(BgL_blockz00_bglt);
	extern obj_t BGl_blockz00zzsaw_defsz00;
	static obj_t __cnst[2];


	   
		 
		DEFINE_STRING(BGl_string1889z00zzsaw_inline_returnz00,
		BgL_bgl_string1889za700za7za7s1892za7, "saw_inline_return", 17);
	      DEFINE_STRING(BGl_string1890z00zzsaw_inline_returnz00,
		BgL_bgl_string1890za700za7za7s1893za7, "saw_inline_return dfs ", 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1885z00zzsaw_inline_returnz00,
		BgL_bgl_za762lambda1665za7621894z00,
		BGl_z62lambda1665z62zzsaw_inline_returnz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1886z00zzsaw_inline_returnz00,
		BgL_bgl_za762za7c3za704anonymo1895za7,
		BGl_z62zc3z04anonymousza31664ze3ze5zzsaw_inline_returnz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1887z00zzsaw_inline_returnz00,
		BgL_bgl_za762lambda1662za7621896z00,
		BGl_z62lambda1662z62zzsaw_inline_returnz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1888z00zzsaw_inline_returnz00,
		BgL_bgl_za762lambda1652za7621897z00,
		BGl_z62lambda1652z62zzsaw_inline_returnz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_inlinezd2returnszd2envz00zzsaw_inline_returnz00,
		BgL_bgl_za762inlineza7d2retu1898z00,
		BGl_z62inlinezd2returnszb0zzsaw_inline_returnz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_inline_returnz00));
		     ADD_ROOT((void *) (&BGl_dfsz00zzsaw_inline_returnz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_inline_returnz00(long
		BgL_checksumz00_2579, char *BgL_fromz00_2580)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_inline_returnz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_inline_returnz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_inline_returnz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_inline_returnz00();
					BGl_cnstzd2initzd2zzsaw_inline_returnz00();
					BGl_importedzd2moduleszd2initz00zzsaw_inline_returnz00();
					BGl_objectzd2initzd2zzsaw_inline_returnz00();
					return BGl_toplevelzd2initzd2zzsaw_inline_returnz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_inline_returnz00(void)
	{
		{	/* SawMill/inlinereturn.scm 1 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_inline_return");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_inline_return");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_inline_return");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_inline_return");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_inline_return");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_inline_return");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_inline_return");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_inline_return");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_inline_returnz00(void)
	{
		{	/* SawMill/inlinereturn.scm 1 */
			{	/* SawMill/inlinereturn.scm 1 */
				obj_t BgL_cportz00_2547;

				{	/* SawMill/inlinereturn.scm 1 */
					obj_t BgL_stringz00_2554;

					BgL_stringz00_2554 = BGl_string1890z00zzsaw_inline_returnz00;
					{	/* SawMill/inlinereturn.scm 1 */
						obj_t BgL_startz00_2555;

						BgL_startz00_2555 = BINT(0L);
						{	/* SawMill/inlinereturn.scm 1 */
							obj_t BgL_endz00_2556;

							BgL_endz00_2556 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2554)));
							{	/* SawMill/inlinereturn.scm 1 */

								BgL_cportz00_2547 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2554, BgL_startz00_2555, BgL_endz00_2556);
				}}}}
				{
					long BgL_iz00_2548;

					BgL_iz00_2548 = 1L;
				BgL_loopz00_2549:
					if ((BgL_iz00_2548 == -1L))
						{	/* SawMill/inlinereturn.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawMill/inlinereturn.scm 1 */
							{	/* SawMill/inlinereturn.scm 1 */
								obj_t BgL_arg1891z00_2550;

								{	/* SawMill/inlinereturn.scm 1 */

									{	/* SawMill/inlinereturn.scm 1 */
										obj_t BgL_locationz00_2552;

										BgL_locationz00_2552 = BBOOL(((bool_t) 0));
										{	/* SawMill/inlinereturn.scm 1 */

											BgL_arg1891z00_2550 =
												BGl_readz00zz__readerz00(BgL_cportz00_2547,
												BgL_locationz00_2552);
										}
									}
								}
								{	/* SawMill/inlinereturn.scm 1 */
									int BgL_tmpz00_2607;

									BgL_tmpz00_2607 = (int) (BgL_iz00_2548);
									CNST_TABLE_SET(BgL_tmpz00_2607, BgL_arg1891z00_2550);
							}}
							{	/* SawMill/inlinereturn.scm 1 */
								int BgL_auxz00_2553;

								BgL_auxz00_2553 = (int) ((BgL_iz00_2548 - 1L));
								{
									long BgL_iz00_2612;

									BgL_iz00_2612 = (long) (BgL_auxz00_2553);
									BgL_iz00_2548 = BgL_iz00_2612;
									goto BgL_loopz00_2549;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_inline_returnz00(void)
	{
		{	/* SawMill/inlinereturn.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_inline_returnz00(void)
	{
		{	/* SawMill/inlinereturn.scm 1 */
			return (BGl_za2inlinezd2depthza2zd2zzsaw_inline_returnz00 = 2L, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzsaw_inline_returnz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1807;

				BgL_headz00_1807 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1808;
					obj_t BgL_tailz00_1809;

					BgL_prevz00_1808 = BgL_headz00_1807;
					BgL_tailz00_1809 = BgL_l1z00_1;
				BgL_loopz00_1810:
					if (PAIRP(BgL_tailz00_1809))
						{
							obj_t BgL_newzd2prevzd2_1812;

							BgL_newzd2prevzd2_1812 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1809), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1808, BgL_newzd2prevzd2_1812);
							{
								obj_t BgL_tailz00_2622;
								obj_t BgL_prevz00_2621;

								BgL_prevz00_2621 = BgL_newzd2prevzd2_1812;
								BgL_tailz00_2622 = CDR(BgL_tailz00_1809);
								BgL_tailz00_1809 = BgL_tailz00_2622;
								BgL_prevz00_1808 = BgL_prevz00_2621;
								goto BgL_loopz00_1810;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1807);
				}
			}
		}

	}



/* inline-returns */
	BGL_EXPORTED_DEF obj_t
		BGl_inlinezd2returnszd2zzsaw_inline_returnz00(BgL_blockz00_bglt BgL_bz00_20)
	{
		{	/* SawMill/inlinereturn.scm 8 */
			{	/* SawMill/inlinereturn.scm 9 */
				obj_t BgL_g1403z00_1829;

				BgL_g1403z00_1829 =
					BGl_findzd2returnszd2zzsaw_inline_returnz00(BgL_bz00_20);
				{
					obj_t BgL_l1401z00_1831;

					{	/* SawMill/inlinereturn.scm 9 */
						bool_t BgL_tmpz00_2626;

						BgL_l1401z00_1831 = BgL_g1403z00_1829;
					BgL_zc3z04anonymousza31438ze3z87_1832:
						if (PAIRP(BgL_l1401z00_1831))
							{	/* SawMill/inlinereturn.scm 9 */
								{	/* SawMill/inlinereturn.scm 9 */
									obj_t BgL_arg1448z00_1834;

									BgL_arg1448z00_1834 = CAR(BgL_l1401z00_1831);
									BGl_inlinezd2returnzd2zzsaw_inline_returnz00(
										((BgL_blockz00_bglt) BgL_arg1448z00_1834));
								}
								{
									obj_t BgL_l1401z00_2632;

									BgL_l1401z00_2632 = CDR(BgL_l1401z00_1831);
									BgL_l1401z00_1831 = BgL_l1401z00_2632;
									goto BgL_zc3z04anonymousza31438ze3z87_1832;
								}
							}
						else
							{	/* SawMill/inlinereturn.scm 9 */
								BgL_tmpz00_2626 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_2626);
					}
				}
			}
		}

	}



/* &inline-returns */
	obj_t BGl_z62inlinezd2returnszb0zzsaw_inline_returnz00(obj_t BgL_envz00_2526,
		obj_t BgL_bz00_2527)
	{
		{	/* SawMill/inlinereturn.scm 8 */
			return
				BGl_inlinezd2returnszd2zzsaw_inline_returnz00(
				((BgL_blockz00_bglt) BgL_bz00_2527));
		}

	}



/* inline-return */
	bool_t BGl_inlinezd2returnzd2zzsaw_inline_returnz00(BgL_blockz00_bglt
		BgL_bz00_21)
	{
		{	/* SawMill/inlinereturn.scm 17 */
			{
				BgL_blockz00_bglt BgL_bz00_1878;
				obj_t BgL_lz00_1879;
				BgL_blockz00_bglt BgL_sz00_1880;

				{	/* SawMill/inlinereturn.scm 37 */
					obj_t BgL_lz00_1840;

					BgL_lz00_1840 =
						(((BgL_blockz00_bglt) COBJECT(BgL_bz00_21))->BgL_firstz00);
					if (BGl_simplezd2returnzf3ze70zc6zzsaw_inline_returnz00(BgL_lz00_1840,
							2L))
						{	/* SawMill/inlinereturn.scm 39 */
							obj_t BgL_g1406z00_1842;

							BgL_g1406z00_1842 =
								(((BgL_blockz00_bglt) COBJECT(BgL_bz00_21))->BgL_predsz00);
							{
								obj_t BgL_l1404z00_1844;

								BgL_l1404z00_1844 = BgL_g1406z00_1842;
							BgL_zc3z04anonymousza31456ze3z87_1845:
								if (PAIRP(BgL_l1404z00_1844))
									{	/* SawMill/inlinereturn.scm 41 */
										{	/* SawMill/inlinereturn.scm 39 */
											obj_t BgL_pz00_1847;

											BgL_pz00_1847 = CAR(BgL_l1404z00_1844);
											{	/* SawMill/inlinereturn.scm 39 */
												bool_t BgL_test1905z00_2644;

												{	/* SawMill/inlinereturn.scm 39 */
													obj_t BgL_tmpz00_2645;

													{	/* SawMill/inlinereturn.scm 39 */
														obj_t BgL_pairz00_2353;

														BgL_pairz00_2353 =
															(((BgL_blockz00_bglt) COBJECT(
																	((BgL_blockz00_bglt) BgL_pz00_1847)))->
															BgL_succsz00);
														BgL_tmpz00_2645 = CDR(BgL_pairz00_2353);
													}
													BgL_test1905z00_2644 = NULLP(BgL_tmpz00_2645);
												}
												if (BgL_test1905z00_2644)
													{	/* SawMill/inlinereturn.scm 39 */
														BgL_bz00_1878 = ((BgL_blockz00_bglt) BgL_pz00_1847);
														BgL_lz00_1879 = BgL_lz00_1840;
														BgL_sz00_1880 = BgL_bz00_21;
														((((BgL_blockz00_bglt) COBJECT(BgL_bz00_1878))->
																BgL_firstz00) =
															((obj_t)
																BGl_reallocze70ze7zzsaw_inline_returnz00
																(BGl_appendzd221011zd2zzsaw_inline_returnz00(((
																				(BgL_blockz00_bglt)
																				COBJECT(BgL_bz00_1878))->BgL_firstz00),
																		BgL_lz00_1879), BNIL)), BUNSPEC);
														((((BgL_blockz00_bglt) COBJECT(BgL_bz00_1878))->
																BgL_succsz00) = ((obj_t) BNIL), BUNSPEC);
														{	/* SawMill/inlinereturn.scm 35 */
															obj_t BgL_arg1571z00_1885;

															{	/* SawMill/inlinereturn.scm 35 */
																obj_t BgL_arg1573z00_1886;

																BgL_arg1573z00_1886 =
																	(((BgL_blockz00_bglt)
																		COBJECT(BgL_sz00_1880))->BgL_predsz00);
																BgL_arg1571z00_1885 =
																	bgl_remq_bang(((obj_t) BgL_bz00_1878),
																	BgL_arg1573z00_1886);
															}
															((((BgL_blockz00_bglt) COBJECT(BgL_sz00_1880))->
																	BgL_predsz00) =
																((obj_t) BgL_arg1571z00_1885), BUNSPEC);
														}
														BGl_inlinezd2returnzd2zzsaw_inline_returnz00
															(BgL_bz00_1878);
													}
												else
													{	/* SawMill/inlinereturn.scm 39 */
														((bool_t) 0);
													}
											}
										}
										{
											obj_t BgL_l1404z00_2661;

											BgL_l1404z00_2661 = CDR(BgL_l1404z00_1844);
											BgL_l1404z00_1844 = BgL_l1404z00_2661;
											goto BgL_zc3z04anonymousza31456ze3z87_1845;
										}
									}
								else
									{	/* SawMill/inlinereturn.scm 41 */
										return ((bool_t) 1);
									}
							}
						}
					else
						{	/* SawMill/inlinereturn.scm 38 */
							return ((bool_t) 0);
						}
				}
			}
		}

	}



/* realloc~0 */
	obj_t BGl_reallocze70ze7zzsaw_inline_returnz00(obj_t BgL_lz00_1866,
		obj_t BgL_rmapz00_1867)
	{
		{	/* SawMill/inlinereturn.scm 31 */
			if (NULLP(BgL_lz00_1866))
				{	/* SawMill/inlinereturn.scm 26 */
					return BgL_lz00_1866;
				}
			else
				{	/* SawMill/inlinereturn.scm 28 */
					obj_t BgL_destz00_1870;

					BgL_destz00_1870 =
						(((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt)
									CAR(((obj_t) BgL_lz00_1866)))))->BgL_destz00);
					if (CBOOL(BgL_destz00_1870))
						{	/* SawMill/inlinereturn.scm 29 */
							obj_t BgL_arg1535z00_1871;

							{	/* SawMill/inlinereturn.scm 29 */
								BgL_rtl_regz00_bglt BgL_arg1540z00_1872;

								BgL_arg1540z00_1872 =
									BGl_copyzd2regzd2zzsaw_inline_returnz00(BgL_destz00_1870);
								BgL_arg1535z00_1871 =
									MAKE_YOUNG_PAIR(BgL_destz00_1870,
									((obj_t) BgL_arg1540z00_1872));
							}
							BgL_rmapz00_1867 =
								MAKE_YOUNG_PAIR(BgL_arg1535z00_1871, BgL_rmapz00_1867);
						}
					else
						{	/* SawMill/inlinereturn.scm 29 */
							BFALSE;
						}
					{	/* SawMill/inlinereturn.scm 30 */
						BgL_rtl_insz00_bglt BgL_arg1544z00_1873;
						obj_t BgL_arg1546z00_1874;

						{	/* SawMill/inlinereturn.scm 30 */
							obj_t BgL_arg1552z00_1875;

							BgL_arg1552z00_1875 = CAR(((obj_t) BgL_lz00_1866));
							BgL_arg1544z00_1873 =
								BGl_renamezd2inszd2zzsaw_inline_returnz00(BgL_arg1552z00_1875,
								BgL_rmapz00_1867);
						}
						{	/* SawMill/inlinereturn.scm 31 */
							obj_t BgL_arg1553z00_1876;

							BgL_arg1553z00_1876 = CDR(((obj_t) BgL_lz00_1866));
							BgL_arg1546z00_1874 =
								BGl_reallocze70ze7zzsaw_inline_returnz00(BgL_arg1553z00_1876,
								BgL_rmapz00_1867);
						}
						return
							MAKE_YOUNG_PAIR(
							((obj_t) BgL_arg1544z00_1873), BgL_arg1546z00_1874);
					}
				}
		}

	}



/* simple-return?~0 */
	bool_t BGl_simplezd2returnzf3ze70zc6zzsaw_inline_returnz00(obj_t
		BgL_lz00_1855, long BgL_nz00_1856)
	{
		{	/* SawMill/inlinereturn.scm 24 */
		BGl_simplezd2returnzf3ze70zc6zzsaw_inline_returnz00:
			{	/* SawMill/inlinereturn.scm 19 */
				obj_t BgL_rz00_1858;

				BgL_rz00_1858 = CDR(((obj_t) BgL_lz00_1855));
				{	/* SawMill/inlinereturn.scm 20 */
					bool_t BgL__ortest_1170z00_1859;

					BgL__ortest_1170z00_1859 = NULLP(BgL_rz00_1858);
					if (BgL__ortest_1170z00_1859)
						{	/* SawMill/inlinereturn.scm 20 */
							return BgL__ortest_1170z00_1859;
						}
					else
						{	/* SawMill/inlinereturn.scm 21 */
							bool_t BgL__ortest_1171z00_1860;

							{	/* SawMill/inlinereturn.scm 21 */
								bool_t BgL_test1909z00_2687;

								{	/* SawMill/inlinereturn.scm 21 */
									BgL_rtl_funz00_bglt BgL_arg1513z00_1864;

									BgL_arg1513z00_1864 =
										(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt)
													CAR(((obj_t) BgL_lz00_1855)))))->BgL_funz00);
									{	/* SawMill/inlinereturn.scm 21 */
										obj_t BgL_classz00_2301;

										BgL_classz00_2301 = BGl_rtl_movz00zzsaw_defsz00;
										{	/* SawMill/inlinereturn.scm 21 */
											BgL_objectz00_bglt BgL_arg1807z00_2303;

											{	/* SawMill/inlinereturn.scm 21 */
												obj_t BgL_tmpz00_2692;

												BgL_tmpz00_2692 =
													((obj_t) ((BgL_objectz00_bglt) BgL_arg1513z00_1864));
												BgL_arg1807z00_2303 =
													(BgL_objectz00_bglt) (BgL_tmpz00_2692);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawMill/inlinereturn.scm 21 */
													long BgL_idxz00_2309;

													BgL_idxz00_2309 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2303);
													BgL_test1909z00_2687 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2309 + 3L)) == BgL_classz00_2301);
												}
											else
												{	/* SawMill/inlinereturn.scm 21 */
													bool_t BgL_res1881z00_2334;

													{	/* SawMill/inlinereturn.scm 21 */
														obj_t BgL_oclassz00_2317;

														{	/* SawMill/inlinereturn.scm 21 */
															obj_t BgL_arg1815z00_2325;
															long BgL_arg1816z00_2326;

															BgL_arg1815z00_2325 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawMill/inlinereturn.scm 21 */
																long BgL_arg1817z00_2327;

																BgL_arg1817z00_2327 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2303);
																BgL_arg1816z00_2326 =
																	(BgL_arg1817z00_2327 - OBJECT_TYPE);
															}
															BgL_oclassz00_2317 =
																VECTOR_REF(BgL_arg1815z00_2325,
																BgL_arg1816z00_2326);
														}
														{	/* SawMill/inlinereturn.scm 21 */
															bool_t BgL__ortest_1115z00_2318;

															BgL__ortest_1115z00_2318 =
																(BgL_classz00_2301 == BgL_oclassz00_2317);
															if (BgL__ortest_1115z00_2318)
																{	/* SawMill/inlinereturn.scm 21 */
																	BgL_res1881z00_2334 =
																		BgL__ortest_1115z00_2318;
																}
															else
																{	/* SawMill/inlinereturn.scm 21 */
																	long BgL_odepthz00_2319;

																	{	/* SawMill/inlinereturn.scm 21 */
																		obj_t BgL_arg1804z00_2320;

																		BgL_arg1804z00_2320 = (BgL_oclassz00_2317);
																		BgL_odepthz00_2319 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2320);
																	}
																	if ((3L < BgL_odepthz00_2319))
																		{	/* SawMill/inlinereturn.scm 21 */
																			obj_t BgL_arg1802z00_2322;

																			{	/* SawMill/inlinereturn.scm 21 */
																				obj_t BgL_arg1803z00_2323;

																				BgL_arg1803z00_2323 =
																					(BgL_oclassz00_2317);
																				BgL_arg1802z00_2322 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2323, 3L);
																			}
																			BgL_res1881z00_2334 =
																				(BgL_arg1802z00_2322 ==
																				BgL_classz00_2301);
																		}
																	else
																		{	/* SawMill/inlinereturn.scm 21 */
																			BgL_res1881z00_2334 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1909z00_2687 = BgL_res1881z00_2334;
												}
										}
									}
								}
								if (BgL_test1909z00_2687)
									{	/* SawMill/inlinereturn.scm 21 */
										BgL__ortest_1171z00_1860 =
											BGl_simplezd2returnzf3ze70zc6zzsaw_inline_returnz00
											(BgL_rz00_1858, BgL_nz00_1856);
									}
								else
									{	/* SawMill/inlinereturn.scm 21 */
										BgL__ortest_1171z00_1860 = ((bool_t) 0);
									}
							}
							if (BgL__ortest_1171z00_1860)
								{	/* SawMill/inlinereturn.scm 21 */
									return BgL__ortest_1171z00_1860;
								}
							else
								{	/* SawMill/inlinereturn.scm 21 */
									if ((BgL_nz00_1856 > 0L))
										{
											long BgL_nz00_2720;
											obj_t BgL_lz00_2719;

											BgL_lz00_2719 = BgL_rz00_1858;
											BgL_nz00_2720 = (BgL_nz00_1856 - 1L);
											BgL_nz00_1856 = BgL_nz00_2720;
											BgL_lz00_1855 = BgL_lz00_2719;
											goto BGl_simplezd2returnzf3ze70zc6zzsaw_inline_returnz00;
										}
									else
										{	/* SawMill/inlinereturn.scm 23 */
											return ((bool_t) 0);
										}
								}
						}
				}
			}
		}

	}



/* find-returns */
	obj_t BGl_findzd2returnszd2zzsaw_inline_returnz00(BgL_blockz00_bglt
		BgL_bz00_22)
	{
		{	/* SawMill/inlinereturn.scm 46 */
			{	/* SawMill/inlinereturn.scm 47 */
				struct bgl_cell BgL_box1915_2545z00;
				obj_t BgL_retsz00_2545;

				BgL_retsz00_2545 = MAKE_CELL_STACK(BNIL, BgL_box1915_2545z00);
				BGl_dfsze70ze7zzsaw_inline_returnz00(BgL_retsz00_2545,
					((obj_t) BgL_bz00_22));
				return ((obj_t) ((obj_t) CELL_REF(BgL_retsz00_2545)));
			}
		}

	}



/* dfs~0 */
	bool_t BGl_dfsze70ze7zzsaw_inline_returnz00(obj_t BgL_retsz00_2543,
		obj_t BgL_bz00_1892)
	{
		{	/* SawMill/inlinereturn.scm 48 */
			{	/* SawMill/inlinereturn.scm 49 */
				BgL_dfsz00_bglt BgL_wide1176z00_1896;

				BgL_wide1176z00_1896 =
					((BgL_dfsz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct BgL_dfsz00_bgl))));
				{	/* SawMill/inlinereturn.scm 49 */
					obj_t BgL_auxz00_2730;
					BgL_objectz00_bglt BgL_tmpz00_2726;

					BgL_auxz00_2730 = ((obj_t) BgL_wide1176z00_1896);
					BgL_tmpz00_2726 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1892)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2726, BgL_auxz00_2730);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1892)));
				{	/* SawMill/inlinereturn.scm 49 */
					long BgL_arg1575z00_1897;

					BgL_arg1575z00_1897 = BGL_CLASS_NUM(BGl_dfsz00zzsaw_inline_returnz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_bz00_1892))), BgL_arg1575z00_1897);
				}
				((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1892)));
			}
			((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1892));
			{	/* SawMill/inlinereturn.scm 50 */
				BgL_rtl_funz00_bglt BgL_funz00_1899;

				BgL_funz00_1899 =
					(((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt)
								CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
										(((BgL_blockz00_bglt) COBJECT(
													((BgL_blockz00_bglt) BgL_bz00_1892)))->
											BgL_firstz00))))))->BgL_funz00);
				{	/* SawMill/inlinereturn.scm 51 */
					bool_t BgL_test1916z00_2752;

					{	/* SawMill/inlinereturn.scm 51 */
						obj_t BgL_classz00_2362;

						BgL_classz00_2362 = BGl_rtl_lastz00zzsaw_defsz00;
						{	/* SawMill/inlinereturn.scm 51 */
							BgL_objectz00_bglt BgL_arg1807z00_2364;

							{	/* SawMill/inlinereturn.scm 51 */
								obj_t BgL_tmpz00_2753;

								BgL_tmpz00_2753 =
									((obj_t) ((BgL_objectz00_bglt) BgL_funz00_1899));
								BgL_arg1807z00_2364 = (BgL_objectz00_bglt) (BgL_tmpz00_2753);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawMill/inlinereturn.scm 51 */
									long BgL_idxz00_2370;

									BgL_idxz00_2370 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2364);
									BgL_test1916z00_2752 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2370 + 2L)) == BgL_classz00_2362);
								}
							else
								{	/* SawMill/inlinereturn.scm 51 */
									bool_t BgL_res1882z00_2395;

									{	/* SawMill/inlinereturn.scm 51 */
										obj_t BgL_oclassz00_2378;

										{	/* SawMill/inlinereturn.scm 51 */
											obj_t BgL_arg1815z00_2386;
											long BgL_arg1816z00_2387;

											BgL_arg1815z00_2386 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawMill/inlinereturn.scm 51 */
												long BgL_arg1817z00_2388;

												BgL_arg1817z00_2388 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2364);
												BgL_arg1816z00_2387 =
													(BgL_arg1817z00_2388 - OBJECT_TYPE);
											}
											BgL_oclassz00_2378 =
												VECTOR_REF(BgL_arg1815z00_2386, BgL_arg1816z00_2387);
										}
										{	/* SawMill/inlinereturn.scm 51 */
											bool_t BgL__ortest_1115z00_2379;

											BgL__ortest_1115z00_2379 =
												(BgL_classz00_2362 == BgL_oclassz00_2378);
											if (BgL__ortest_1115z00_2379)
												{	/* SawMill/inlinereturn.scm 51 */
													BgL_res1882z00_2395 = BgL__ortest_1115z00_2379;
												}
											else
												{	/* SawMill/inlinereturn.scm 51 */
													long BgL_odepthz00_2380;

													{	/* SawMill/inlinereturn.scm 51 */
														obj_t BgL_arg1804z00_2381;

														BgL_arg1804z00_2381 = (BgL_oclassz00_2378);
														BgL_odepthz00_2380 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2381);
													}
													if ((2L < BgL_odepthz00_2380))
														{	/* SawMill/inlinereturn.scm 51 */
															obj_t BgL_arg1802z00_2383;

															{	/* SawMill/inlinereturn.scm 51 */
																obj_t BgL_arg1803z00_2384;

																BgL_arg1803z00_2384 = (BgL_oclassz00_2378);
																BgL_arg1802z00_2383 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2384,
																	2L);
															}
															BgL_res1882z00_2395 =
																(BgL_arg1802z00_2383 == BgL_classz00_2362);
														}
													else
														{	/* SawMill/inlinereturn.scm 51 */
															BgL_res1882z00_2395 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1916z00_2752 = BgL_res1882z00_2395;
								}
						}
					}
					if (BgL_test1916z00_2752)
						{	/* SawMill/inlinereturn.scm 51 */
							obj_t BgL_auxz00_2544;

							BgL_auxz00_2544 =
								MAKE_YOUNG_PAIR(BgL_bz00_1892,
								((obj_t) ((obj_t) CELL_REF(BgL_retsz00_2543))));
							CELL_SET(BgL_retsz00_2543, BgL_auxz00_2544);
						}
					else
						{	/* SawMill/inlinereturn.scm 51 */
							BFALSE;
						}
				}
			}
			{	/* SawMill/inlinereturn.scm 52 */
				obj_t BgL_g1409z00_1904;

				BgL_g1409z00_1904 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_bz00_1892)))->BgL_succsz00);
				{
					obj_t BgL_l1407z00_1906;

					BgL_l1407z00_1906 = BgL_g1409z00_1904;
				BgL_zc3z04anonymousza31590ze3z87_1907:
					if (PAIRP(BgL_l1407z00_1906))
						{	/* SawMill/inlinereturn.scm 52 */
							{	/* SawMill/inlinereturn.scm 52 */
								obj_t BgL_sz00_1909;

								BgL_sz00_1909 = CAR(BgL_l1407z00_1906);
								{	/* SawMill/inlinereturn.scm 52 */
									bool_t BgL_test1921z00_2783;

									{	/* SawMill/inlinereturn.scm 52 */
										obj_t BgL_classz00_2398;

										BgL_classz00_2398 = BGl_dfsz00zzsaw_inline_returnz00;
										if (BGL_OBJECTP(BgL_sz00_1909))
											{	/* SawMill/inlinereturn.scm 52 */
												obj_t BgL_oclassz00_2400;

												{	/* SawMill/inlinereturn.scm 52 */
													obj_t BgL_arg1815z00_2402;
													long BgL_arg1816z00_2403;

													BgL_arg1815z00_2402 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/inlinereturn.scm 52 */
														long BgL_arg1817z00_2404;

														BgL_arg1817z00_2404 =
															BGL_OBJECT_CLASS_NUM(
															((BgL_objectz00_bglt) BgL_sz00_1909));
														BgL_arg1816z00_2403 =
															(BgL_arg1817z00_2404 - OBJECT_TYPE);
													}
													BgL_oclassz00_2400 =
														VECTOR_REF(BgL_arg1815z00_2402,
														BgL_arg1816z00_2403);
												}
												BgL_test1921z00_2783 =
													(BgL_oclassz00_2400 == BgL_classz00_2398);
											}
										else
											{	/* SawMill/inlinereturn.scm 52 */
												BgL_test1921z00_2783 = ((bool_t) 0);
											}
									}
									if (BgL_test1921z00_2783)
										{	/* SawMill/inlinereturn.scm 52 */
											((bool_t) 0);
										}
									else
										{	/* SawMill/inlinereturn.scm 52 */
											BGl_dfsze70ze7zzsaw_inline_returnz00(BgL_retsz00_2543,
												BgL_sz00_1909);
										}
								}
							}
							{
								obj_t BgL_l1407z00_2793;

								BgL_l1407z00_2793 = CDR(BgL_l1407z00_1906);
								BgL_l1407z00_1906 = BgL_l1407z00_2793;
								goto BgL_zc3z04anonymousza31590ze3z87_1907;
							}
						}
					else
						{	/* SawMill/inlinereturn.scm 52 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* copy-reg */
	BgL_rtl_regz00_bglt BGl_copyzd2regzd2zzsaw_inline_returnz00(obj_t BgL_rz00_23)
	{
		{	/* SawMill/inlinereturn.scm 56 */
			{	/* SawMill/inlinereturn.scm 58 */
				BgL_rtl_regz00_bglt BgL_new1180z00_1915;

				{	/* SawMill/inlinereturn.scm 58 */
					BgL_rtl_regz00_bglt BgL_new1179z00_1920;

					BgL_new1179z00_1920 =
						((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regz00_bgl))));
					{	/* SawMill/inlinereturn.scm 58 */
						long BgL_arg1611z00_1921;

						BgL_arg1611z00_1921 = BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1179z00_1920), BgL_arg1611z00_1921);
					}
					{	/* SawMill/inlinereturn.scm 58 */
						BgL_objectz00_bglt BgL_tmpz00_2799;

						BgL_tmpz00_2799 = ((BgL_objectz00_bglt) BgL_new1179z00_1920);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2799, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1179z00_1920);
					BgL_new1180z00_1915 = BgL_new1179z00_1920;
				}
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1180z00_1915))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_rtl_regz00_bglt)
								COBJECT(((BgL_rtl_regz00_bglt) BgL_rz00_23)))->BgL_typez00)),
					BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1180z00_1915))->BgL_varz00) =
					((obj_t) (((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
										BgL_rz00_23)))->BgL_varz00)), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1180z00_1915))->
						BgL_onexprzf3zf3) = ((obj_t) BFALSE), BUNSPEC);
				{
					obj_t BgL_auxz00_2810;

					{	/* SawMill/inlinereturn.scm 58 */
						obj_t BgL_arg1602z00_1916;

						{	/* SawMill/inlinereturn.scm 58 */
							obj_t BgL_arg1605z00_1917;

							{	/* SawMill/inlinereturn.scm 58 */
								obj_t BgL_classz00_2415;

								BgL_classz00_2415 = BGl_rtl_regz00zzsaw_defsz00;
								BgL_arg1605z00_1917 = BGL_CLASS_ALL_FIELDS(BgL_classz00_2415);
							}
							BgL_arg1602z00_1916 = VECTOR_REF(BgL_arg1605z00_1917, 3L);
						}
						BgL_auxz00_2810 =
							BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
							(BgL_arg1602z00_1916);
					}
					((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1180z00_1915))->BgL_namez00) =
						((obj_t) BgL_auxz00_2810), BUNSPEC);
				}
				{
					obj_t BgL_auxz00_2815;

					{	/* SawMill/inlinereturn.scm 58 */
						obj_t BgL_arg1606z00_1918;

						{	/* SawMill/inlinereturn.scm 58 */
							obj_t BgL_arg1609z00_1919;

							{	/* SawMill/inlinereturn.scm 58 */
								obj_t BgL_classz00_2417;

								BgL_classz00_2417 = BGl_rtl_regz00zzsaw_defsz00;
								BgL_arg1609z00_1919 = BGL_CLASS_ALL_FIELDS(BgL_classz00_2417);
							}
							BgL_arg1606z00_1918 = VECTOR_REF(BgL_arg1609z00_1919, 4L);
						}
						BgL_auxz00_2815 =
							BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
							(BgL_arg1606z00_1918);
					}
					((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1180z00_1915))->BgL_keyz00) =
						((obj_t) BgL_auxz00_2815), BUNSPEC);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1180z00_1915))->
						BgL_debugnamez00) = ((obj_t) BFALSE), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1180z00_1915))->
						BgL_hardwarez00) = ((obj_t) BFALSE), BUNSPEC);
				return BgL_new1180z00_1915;
			}
		}

	}



/* rename-ins */
	BgL_rtl_insz00_bglt BGl_renamezd2inszd2zzsaw_inline_returnz00(obj_t
		BgL_insz00_24, obj_t BgL_rmapz00_25)
	{
		{	/* SawMill/inlinereturn.scm 60 */
			{	/* SawMill/inlinereturn.scm 65 */
				BgL_rtl_insz00_bglt BgL_new1183z00_1924;

				{	/* SawMill/inlinereturn.scm 65 */
					BgL_rtl_insz00_bglt BgL_new1182z00_1940;

					BgL_new1182z00_1940 =
						((BgL_rtl_insz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_insz00_bgl))));
					{	/* SawMill/inlinereturn.scm 65 */
						long BgL_arg1627z00_1941;

						BgL_arg1627z00_1941 = BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1182z00_1940), BgL_arg1627z00_1941);
					}
					{	/* SawMill/inlinereturn.scm 65 */
						BgL_objectz00_bglt BgL_tmpz00_2826;

						BgL_tmpz00_2826 = ((BgL_objectz00_bglt) BgL_new1182z00_1940);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2826, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1182z00_1940);
					BgL_new1183z00_1924 = BgL_new1182z00_1940;
				}
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1183z00_1924))->BgL_locz00) =
					((obj_t) (((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
										BgL_insz00_24)))->BgL_locz00)), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1183z00_1924))->
						BgL_z52spillz52) = ((obj_t) BNIL), BUNSPEC);
				{
					obj_t BgL_auxz00_2834;

					if (CBOOL(
							(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_insz00_24)))->BgL_destz00)))
						{	/* SawMill/inlinereturn.scm 66 */
							obj_t BgL_arg1613z00_1926;

							BgL_arg1613z00_1926 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_insz00_24)))->BgL_destz00);
							{	/* SawMill/inlinereturn.scm 62 */
								obj_t BgL_slotz00_2425;

								BgL_slotz00_2425 =
									BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg1613z00_1926,
									BgL_rmapz00_25);
								if (CBOOL(BgL_slotz00_2425))
									{	/* SawMill/inlinereturn.scm 63 */
										BgL_auxz00_2834 = CDR(((obj_t) BgL_slotz00_2425));
									}
								else
									{	/* SawMill/inlinereturn.scm 63 */
										BgL_auxz00_2834 = BgL_arg1613z00_1926;
									}
							}
						}
					else
						{	/* SawMill/inlinereturn.scm 66 */
							BgL_auxz00_2834 = BFALSE;
						}
					((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1183z00_1924))->BgL_destz00) =
						((obj_t) BgL_auxz00_2834), BUNSPEC);
				}
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1183z00_1924))->BgL_funz00) =
					((BgL_rtl_funz00_bglt) (((BgL_rtl_insz00_bglt)
								COBJECT(((BgL_rtl_insz00_bglt) BgL_insz00_24)))->BgL_funz00)),
					BUNSPEC);
				{
					obj_t BgL_auxz00_2850;

					{	/* SawMill/inlinereturn.scm 68 */
						obj_t BgL_l1410z00_1927;

						BgL_l1410z00_1927 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_insz00_24)))->BgL_argsz00);
						if (NULLP(BgL_l1410z00_1927))
							{	/* SawMill/inlinereturn.scm 68 */
								BgL_auxz00_2850 = BNIL;
							}
						else
							{	/* SawMill/inlinereturn.scm 68 */
								obj_t BgL_head1412z00_1929;

								BgL_head1412z00_1929 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1410z00_1931;
									obj_t BgL_tail1413z00_1932;

									BgL_l1410z00_1931 = BgL_l1410z00_1927;
									BgL_tail1413z00_1932 = BgL_head1412z00_1929;
								BgL_zc3z04anonymousza31615ze3z87_1933:
									if (NULLP(BgL_l1410z00_1931))
										{	/* SawMill/inlinereturn.scm 68 */
											BgL_auxz00_2850 = CDR(BgL_head1412z00_1929);
										}
									else
										{	/* SawMill/inlinereturn.scm 68 */
											obj_t BgL_newtail1414z00_1935;

											{	/* SawMill/inlinereturn.scm 68 */
												obj_t BgL_arg1626z00_1937;

												{	/* SawMill/inlinereturn.scm 68 */
													obj_t BgL_az00_1938;

													BgL_az00_1938 = CAR(((obj_t) BgL_l1410z00_1931));
													{	/* SawMill/inlinereturn.scm 62 */
														obj_t BgL_slotz00_2429;

														BgL_slotz00_2429 =
															BGl_assqz00zz__r4_pairs_and_lists_6_3z00
															(BgL_az00_1938, BgL_rmapz00_25);
														if (CBOOL(BgL_slotz00_2429))
															{	/* SawMill/inlinereturn.scm 63 */
																BgL_arg1626z00_1937 =
																	CDR(((obj_t) BgL_slotz00_2429));
															}
														else
															{	/* SawMill/inlinereturn.scm 63 */
																BgL_arg1626z00_1937 = BgL_az00_1938;
															}
													}
												}
												BgL_newtail1414z00_1935 =
													MAKE_YOUNG_PAIR(BgL_arg1626z00_1937, BNIL);
											}
											SET_CDR(BgL_tail1413z00_1932, BgL_newtail1414z00_1935);
											{	/* SawMill/inlinereturn.scm 68 */
												obj_t BgL_arg1625z00_1936;

												BgL_arg1625z00_1936 = CDR(((obj_t) BgL_l1410z00_1931));
												{
													obj_t BgL_tail1413z00_2871;
													obj_t BgL_l1410z00_2870;

													BgL_l1410z00_2870 = BgL_arg1625z00_1936;
													BgL_tail1413z00_2871 = BgL_newtail1414z00_1935;
													BgL_tail1413z00_1932 = BgL_tail1413z00_2871;
													BgL_l1410z00_1931 = BgL_l1410z00_2870;
													goto BgL_zc3z04anonymousza31615ze3z87_1933;
												}
											}
										}
								}
							}
					}
					((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1183z00_1924))->BgL_argsz00) =
						((obj_t) BgL_auxz00_2850), BUNSPEC);
				}
				return BgL_new1183z00_1924;
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_inline_returnz00(void)
	{
		{	/* SawMill/inlinereturn.scm 1 */
			{	/* SawMill/inlinereturn.scm 6 */
				obj_t BgL_arg1650z00_1951;
				obj_t BgL_arg1651z00_1952;

				{	/* SawMill/inlinereturn.scm 6 */
					obj_t BgL_v1415z00_1977;

					BgL_v1415z00_1977 = create_vector(0L);
					BgL_arg1650z00_1951 = BgL_v1415z00_1977;
				}
				{	/* SawMill/inlinereturn.scm 6 */
					obj_t BgL_v1416z00_1978;

					BgL_v1416z00_1978 = create_vector(0L);
					BgL_arg1651z00_1952 = BgL_v1416z00_1978;
				}
				return (BGl_dfsz00zzsaw_inline_returnz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(0),
						CNST_TABLE_REF(1), BGl_blockz00zzsaw_defsz00, 64313L,
						BGl_proc1888z00zzsaw_inline_returnz00,
						BGl_proc1887z00zzsaw_inline_returnz00, BFALSE,
						BGl_proc1886z00zzsaw_inline_returnz00,
						BGl_proc1885z00zzsaw_inline_returnz00, BgL_arg1650z00_1951,
						BgL_arg1651z00_1952), BUNSPEC);
			}
		}

	}



/* &lambda1665 */
	BgL_blockz00_bglt BGl_z62lambda1665z62zzsaw_inline_returnz00(obj_t
		BgL_envz00_2532, obj_t BgL_o1157z00_2533)
	{
		{	/* SawMill/inlinereturn.scm 6 */
			{	/* SawMill/inlinereturn.scm 6 */
				long BgL_arg1675z00_2559;

				{	/* SawMill/inlinereturn.scm 6 */
					obj_t BgL_arg1678z00_2560;

					{	/* SawMill/inlinereturn.scm 6 */
						obj_t BgL_arg1681z00_2561;

						{	/* SawMill/inlinereturn.scm 6 */
							obj_t BgL_arg1815z00_2562;
							long BgL_arg1816z00_2563;

							BgL_arg1815z00_2562 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/inlinereturn.scm 6 */
								long BgL_arg1817z00_2564;

								BgL_arg1817z00_2564 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_o1157z00_2533)));
								BgL_arg1816z00_2563 = (BgL_arg1817z00_2564 - OBJECT_TYPE);
							}
							BgL_arg1681z00_2561 =
								VECTOR_REF(BgL_arg1815z00_2562, BgL_arg1816z00_2563);
						}
						BgL_arg1678z00_2560 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1681z00_2561);
					}
					{	/* SawMill/inlinereturn.scm 6 */
						obj_t BgL_tmpz00_2885;

						BgL_tmpz00_2885 = ((obj_t) BgL_arg1678z00_2560);
						BgL_arg1675z00_2559 = BGL_CLASS_NUM(BgL_tmpz00_2885);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) BgL_o1157z00_2533)), BgL_arg1675z00_2559);
			}
			{	/* SawMill/inlinereturn.scm 6 */
				BgL_objectz00_bglt BgL_tmpz00_2891;

				BgL_tmpz00_2891 =
					((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1157z00_2533));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2891, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1157z00_2533));
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1157z00_2533));
		}

	}



/* &<@anonymous:1664> */
	obj_t BGl_z62zc3z04anonymousza31664ze3ze5zzsaw_inline_returnz00(obj_t
		BgL_envz00_2534, obj_t BgL_new1156z00_2535)
	{
		{	/* SawMill/inlinereturn.scm 6 */
			{
				BgL_blockz00_bglt BgL_auxz00_2899;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_new1156z00_2535))))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1156z00_2535))))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1156z00_2535))))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1156z00_2535))))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_2899 = ((BgL_blockz00_bglt) BgL_new1156z00_2535);
				return ((obj_t) BgL_auxz00_2899);
			}
		}

	}



/* &lambda1662 */
	BgL_blockz00_bglt BGl_z62lambda1662z62zzsaw_inline_returnz00(obj_t
		BgL_envz00_2536, obj_t BgL_o1153z00_2537)
	{
		{	/* SawMill/inlinereturn.scm 6 */
			{	/* SawMill/inlinereturn.scm 6 */
				BgL_dfsz00_bglt BgL_wide1155z00_2567;

				BgL_wide1155z00_2567 =
					((BgL_dfsz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct BgL_dfsz00_bgl))));
				{	/* SawMill/inlinereturn.scm 6 */
					obj_t BgL_auxz00_2920;
					BgL_objectz00_bglt BgL_tmpz00_2916;

					BgL_auxz00_2920 = ((obj_t) BgL_wide1155z00_2567);
					BgL_tmpz00_2916 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1153z00_2537)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2916, BgL_auxz00_2920);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1153z00_2537)));
				{	/* SawMill/inlinereturn.scm 6 */
					long BgL_arg1663z00_2568;

					BgL_arg1663z00_2568 = BGL_CLASS_NUM(BGl_dfsz00zzsaw_inline_returnz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_o1153z00_2537))), BgL_arg1663z00_2568);
				}
				return
					((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1153z00_2537)));
			}
		}

	}



/* &lambda1652 */
	BgL_blockz00_bglt BGl_z62lambda1652z62zzsaw_inline_returnz00(obj_t
		BgL_envz00_2538, obj_t BgL_label1149z00_2539, obj_t BgL_preds1150z00_2540,
		obj_t BgL_succs1151z00_2541, obj_t BgL_first1152z00_2542)
	{
		{	/* SawMill/inlinereturn.scm 6 */
			{	/* SawMill/inlinereturn.scm 6 */
				int BgL_label1149z00_2569;

				BgL_label1149z00_2569 = CINT(BgL_label1149z00_2539);
				{	/* SawMill/inlinereturn.scm 6 */
					BgL_blockz00_bglt BgL_new1189z00_2573;

					{	/* SawMill/inlinereturn.scm 6 */
						BgL_blockz00_bglt BgL_tmp1186z00_2574;
						BgL_dfsz00_bglt BgL_wide1187z00_2575;

						{
							BgL_blockz00_bglt BgL_auxz00_2935;

							{	/* SawMill/inlinereturn.scm 6 */
								BgL_blockz00_bglt BgL_new1185z00_2576;

								BgL_new1185z00_2576 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawMill/inlinereturn.scm 6 */
									long BgL_arg1661z00_2577;

									BgL_arg1661z00_2577 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1185z00_2576),
										BgL_arg1661z00_2577);
								}
								{	/* SawMill/inlinereturn.scm 6 */
									BgL_objectz00_bglt BgL_tmpz00_2940;

									BgL_tmpz00_2940 = ((BgL_objectz00_bglt) BgL_new1185z00_2576);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2940, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1185z00_2576);
								BgL_auxz00_2935 = BgL_new1185z00_2576;
							}
							BgL_tmp1186z00_2574 = ((BgL_blockz00_bglt) BgL_auxz00_2935);
						}
						BgL_wide1187z00_2575 =
							((BgL_dfsz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_dfsz00_bgl))));
						{	/* SawMill/inlinereturn.scm 6 */
							obj_t BgL_auxz00_2948;
							BgL_objectz00_bglt BgL_tmpz00_2946;

							BgL_auxz00_2948 = ((obj_t) BgL_wide1187z00_2575);
							BgL_tmpz00_2946 = ((BgL_objectz00_bglt) BgL_tmp1186z00_2574);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2946, BgL_auxz00_2948);
						}
						((BgL_objectz00_bglt) BgL_tmp1186z00_2574);
						{	/* SawMill/inlinereturn.scm 6 */
							long BgL_arg1654z00_2578;

							BgL_arg1654z00_2578 =
								BGL_CLASS_NUM(BGl_dfsz00zzsaw_inline_returnz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1186z00_2574),
								BgL_arg1654z00_2578);
						}
						BgL_new1189z00_2573 = ((BgL_blockz00_bglt) BgL_tmp1186z00_2574);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_new1189z00_2573)))->BgL_labelz00) =
						((int) BgL_label1149z00_2569), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1189z00_2573)))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1150z00_2540)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1189z00_2573)))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1151z00_2541)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1189z00_2573)))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1152z00_2542)), BUNSPEC);
					return BgL_new1189z00_2573;
				}
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_inline_returnz00(void)
	{
		{	/* SawMill/inlinereturn.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_inline_returnz00(void)
	{
		{	/* SawMill/inlinereturn.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_inline_returnz00(void)
	{
		{	/* SawMill/inlinereturn.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1889z00zzsaw_inline_returnz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1889z00zzsaw_inline_returnz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1889z00zzsaw_inline_returnz00));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string1889z00zzsaw_inline_returnz00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string1889z00zzsaw_inline_returnz00));
		}

	}

#ifdef __cplusplus
}
#endif
