/*===========================================================================*/
/*   (SawMill/procedures.scm)                                                */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/procedures.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_PROCEDURES_TYPE_DEFINITIONS
#define BGL_SAW_PROCEDURES_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_getfieldz00_bglt;

	typedef struct BgL_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_setfieldz00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_ftypez00;
	}                 *BgL_vlengthz00_bglt;

	typedef struct BgL_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_classz00;
	}                    *BgL_instanceofz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_SAW_PROCEDURES_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t
		BGl_needzd2functionzd2pointerz00zzsaw_elsewherez00(BgL_globalz00_bglt);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_proceduresz00 = BUNSPEC;
	extern obj_t BGl_newz00zzast_nodez00;
	static obj_t BGl_z62getzd2conditional1277zb0zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_vlengthz00zzast_nodez00;
	static obj_t BGl_z62getzd2fail1315zb0zzsaw_proceduresz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_toplevelzd2initzd2zzsaw_proceduresz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	static obj_t BGl_z62getzd2sequence1273zb0zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62getzd2setq1271zb0zzsaw_proceduresz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzsaw_proceduresz00(void);
	extern obj_t BGl_getfieldz00zzast_nodez00;
	static obj_t BGl_objectzd2initzd2zzsaw_proceduresz00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62getzd2vlength1303zb0zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62getzd2sync1275zb0zzsaw_proceduresz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62getzd2pragma1289zb0zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62getzd2vsetz121301za2zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62getzd2new1295zb0zzsaw_proceduresz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_proceduresz00(void);
	static obj_t BGl_z62get1262z62zzsaw_proceduresz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62getz62zzsaw_proceduresz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62getzd2setzd2exzd2it1311zb0zzsaw_proceduresz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62getzd2var1267zb0zzsaw_proceduresz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31328ze3ze5zzsaw_proceduresz00(obj_t,
		obj_t);
	extern obj_t BGl_pragmaz00zzast_nodez00;
	static obj_t BGl_z62getzd2letzd2fun1281z62zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62getzd2castzd2null1307z62zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_proceduresz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_elsewherez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62getzd2vref1299zb0zzsaw_proceduresz00(obj_t, obj_t, obj_t);
	static obj_t BGl_getz00zzsaw_proceduresz00(BgL_nodez00_bglt, obj_t);
	static obj_t BGl_z62getzd2valloc1297zb0zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62getzd2jumpzd2exzd2it1313zb0zzsaw_proceduresz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_vrefz00zzast_nodez00;
	static obj_t BGl_z62getzd2getfield1291zb0zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzsaw_proceduresz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_proceduresz00(void);
	static obj_t BGl_z62getzd2app1283zb0zzsaw_proceduresz00(obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_proceduresz00(void);
	static obj_t BGl_z62getzd2procedureszb0zzsaw_proceduresz00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_proceduresz00(void);
	extern obj_t BGl_vallocz00zzast_nodez00;
	static obj_t BGl_z62getzd2atom1265zb0zzsaw_proceduresz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62getzd2makezd2box1317z62zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62getzd2appzd2ly1285z62zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62getzd2boxzd2setz121321z70zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_instanceofz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
	static obj_t BGl_z62getzd2setfield1293zb0zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62getzd2boxzd2ref1319z62zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_getza2za2zzsaw_proceduresz00(obj_t, obj_t);
	extern obj_t BGl_setfieldz00zzast_nodez00;
	extern obj_t BGl_castzd2nullzd2zzast_nodez00;
	static obj_t BGl_z62getzd2switch1279zb0zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62getzd2letzd2var1269z62zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_getzd2procedureszd2zzsaw_proceduresz00(obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62getzd2funcall1287zb0zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t BGl_z62getzd2instanceof1305zb0zzsaw_proceduresz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	static obj_t BGl_z62getzd2cast1309zb0zzsaw_proceduresz00(obj_t, obj_t, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_STRING(BGl_string1862z00zzsaw_proceduresz00,
		BgL_bgl_string1862za700za7za7s1897za7, "get1262", 7);
	      DEFINE_STRING(BGl_string1863z00zzsaw_proceduresz00,
		BgL_bgl_string1863za700za7za7s1898za7, "hole", 4);
	      DEFINE_STRING(BGl_string1865z00zzsaw_proceduresz00,
		BgL_bgl_string1865za700za7za7s1899za7, "get", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1861z00zzsaw_proceduresz00,
		BgL_bgl_za762get1262za762za7za7s1900z00,
		BGl_z62get1262z62zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1864z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2atom1261901z00,
		BGl_z62getzd2atom1265zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1866z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2var12671902z00,
		BGl_z62getzd2var1267zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1867z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2letza7d2v1903za7,
		BGl_z62getzd2letzd2var1269z62zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1868z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2setq1271904z00,
		BGl_z62getzd2setq1271zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1869z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2sequenc1905z00,
		BGl_z62getzd2sequence1273zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1870z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2sync1271906z00,
		BGl_z62getzd2sync1275zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1871z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2conditi1907z00,
		BGl_z62getzd2conditional1277zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1872z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2switch11908z00,
		BGl_z62getzd2switch1279zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1873z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2letza7d2f1909za7,
		BGl_z62getzd2letzd2fun1281z62zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1874z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2app12831910z00,
		BGl_z62getzd2app1283zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1875z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2appza7d2l1911za7,
		BGl_z62getzd2appzd2ly1285z62zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1876z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2funcall1912z00,
		BGl_z62getzd2funcall1287zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1877z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2pragma11913z00,
		BGl_z62getzd2pragma1289zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1878z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2getfiel1914z00,
		BGl_z62getzd2getfield1291zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1879z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2setfiel1915z00,
		BGl_z62getzd2setfield1293zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1880z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2new12951916z00,
		BGl_z62getzd2new1295zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1881z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2valloc11917z00,
		BGl_z62getzd2valloc1297zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1882z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2vref1291918z00,
		BGl_z62getzd2vref1299zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1883z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2vsetza7121919za7,
		BGl_z62getzd2vsetz121301za2zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1884z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2vlength1920z00,
		BGl_z62getzd2vlength1303zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1885z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2instanc1921z00,
		BGl_z62getzd2instanceof1305zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1886z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2castza7d21922za7,
		BGl_z62getzd2castzd2null1307z62zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1887z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2cast1301923z00,
		BGl_z62getzd2cast1309zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1894z00zzsaw_proceduresz00,
		BgL_bgl_string1894za700za7za7s1924za7, "saw_procedures", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1888z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2setza7d2e1925za7,
		BGl_z62getzd2setzd2exzd2it1311zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1895z00zzsaw_proceduresz00,
		BgL_bgl_string1895za700za7za7s1926za7, "__evmeaning_address elight get ",
		31);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1889z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2jumpza7d21927za7,
		BGl_z62getzd2jumpzd2exzd2it1313zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_GENERIC(BGl_getzd2envzd2zzsaw_proceduresz00,
		BgL_bgl_za762getza762za7za7saw_p1928z00, BGl_z62getz62zzsaw_proceduresz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1890z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2fail1311929z00,
		BGl_z62getzd2fail1315zb0zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1891z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2makeza7d21930za7,
		BGl_z62getzd2makezd2box1317z62zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1892z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2boxza7d2r1931za7,
		BGl_z62getzd2boxzd2ref1319z62zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1893z00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2boxza7d2s1932za7,
		BGl_z62getzd2boxzd2setz121321z70zzsaw_proceduresz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2procedureszd2envz00zzsaw_proceduresz00,
		BgL_bgl_za762getza7d2procedu1933z00,
		BGl_z62getzd2procedureszb0zzsaw_proceduresz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_proceduresz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_proceduresz00(long
		BgL_checksumz00_2514, char *BgL_fromz00_2515)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_proceduresz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_proceduresz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_proceduresz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_proceduresz00();
					BGl_cnstzd2initzd2zzsaw_proceduresz00();
					BGl_importedzd2moduleszd2initz00zzsaw_proceduresz00();
					BGl_genericzd2initzd2zzsaw_proceduresz00();
					BGl_methodzd2initzd2zzsaw_proceduresz00();
					return BGl_toplevelzd2initzd2zzsaw_proceduresz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_proceduresz00(void)
	{
		{	/* SawMill/procedures.scm 1 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_procedures");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_procedures");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_procedures");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_procedures");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_procedures");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_procedures");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_procedures");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_procedures");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_procedures");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_procedures");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_proceduresz00(void)
	{
		{	/* SawMill/procedures.scm 1 */
			{	/* SawMill/procedures.scm 1 */
				obj_t BgL_cportz00_2361;

				{	/* SawMill/procedures.scm 1 */
					obj_t BgL_stringz00_2368;

					BgL_stringz00_2368 = BGl_string1895z00zzsaw_proceduresz00;
					{	/* SawMill/procedures.scm 1 */
						obj_t BgL_startz00_2369;

						BgL_startz00_2369 = BINT(0L);
						{	/* SawMill/procedures.scm 1 */
							obj_t BgL_endz00_2370;

							BgL_endz00_2370 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2368)));
							{	/* SawMill/procedures.scm 1 */

								BgL_cportz00_2361 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2368, BgL_startz00_2369, BgL_endz00_2370);
				}}}}
				{
					long BgL_iz00_2362;

					BgL_iz00_2362 = 2L;
				BgL_loopz00_2363:
					if ((BgL_iz00_2362 == -1L))
						{	/* SawMill/procedures.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawMill/procedures.scm 1 */
							{	/* SawMill/procedures.scm 1 */
								obj_t BgL_arg1896z00_2364;

								{	/* SawMill/procedures.scm 1 */

									{	/* SawMill/procedures.scm 1 */
										obj_t BgL_locationz00_2366;

										BgL_locationz00_2366 = BBOOL(((bool_t) 0));
										{	/* SawMill/procedures.scm 1 */

											BgL_arg1896z00_2364 =
												BGl_readz00zz__readerz00(BgL_cportz00_2361,
												BgL_locationz00_2366);
										}
									}
								}
								{	/* SawMill/procedures.scm 1 */
									int BgL_tmpz00_2545;

									BgL_tmpz00_2545 = (int) (BgL_iz00_2362);
									CNST_TABLE_SET(BgL_tmpz00_2545, BgL_arg1896z00_2364);
							}}
							{	/* SawMill/procedures.scm 1 */
								int BgL_auxz00_2367;

								BgL_auxz00_2367 = (int) ((BgL_iz00_2362 - 1L));
								{
									long BgL_iz00_2550;

									BgL_iz00_2550 = (long) (BgL_auxz00_2367);
									BgL_iz00_2362 = BgL_iz00_2550;
									goto BgL_loopz00_2363;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_proceduresz00(void)
	{
		{	/* SawMill/procedures.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_proceduresz00(void)
	{
		{	/* SawMill/procedures.scm 1 */
			return BUNSPEC;
		}

	}



/* get-procedures */
	BGL_EXPORTED_DEF obj_t BGl_getzd2procedureszd2zzsaw_proceduresz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* SawMill/procedures.scm 12 */
			{	/* SawMill/procedures.scm 13 */
				obj_t BgL_rz00_2235;

				BgL_rz00_2235 = MAKE_CELL(BNIL);
				{	/* SawMill/procedures.scm 15 */
					obj_t BgL_zc3z04anonymousza31328ze3z87_2228;

					BgL_zc3z04anonymousza31328ze3z87_2228 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31328ze3ze5zzsaw_proceduresz00, (int) (1L),
						(int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31328ze3z87_2228, (int) (0L),
						((obj_t) BgL_rz00_2235));
					BGl_forzd2eachzd2globalz12z12zzast_envz00
						(BgL_zc3z04anonymousza31328ze3z87_2228, BNIL);
				}
				{
					obj_t BgL_l1243z00_1427;

					BgL_l1243z00_1427 = BgL_globalsz00_3;
				BgL_zc3z04anonymousza31332ze3z87_1428:
					if (PAIRP(BgL_l1243z00_1427))
						{	/* SawMill/procedures.scm 18 */
							{	/* SawMill/procedures.scm 19 */
								obj_t BgL_globalz00_1430;

								BgL_globalz00_1430 = CAR(BgL_l1243z00_1427);
								{	/* SawMill/procedures.scm 19 */
									obj_t BgL_auxz00_2236;

									{	/* SawMill/procedures.scm 19 */
										obj_t BgL_arg1335z00_1431;

										BgL_arg1335z00_1431 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt)
														(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt)
																			BgL_globalz00_1430))))->BgL_valuez00))))->
											BgL_bodyz00);
										BgL_auxz00_2236 =
											BGl_getz00zzsaw_proceduresz00(((BgL_nodez00_bglt)
												BgL_arg1335z00_1431), CELL_REF(BgL_rz00_2235));
									}
									CELL_SET(BgL_rz00_2235, BgL_auxz00_2236);
								}
							}
							{
								obj_t BgL_l1243z00_2570;

								BgL_l1243z00_2570 = CDR(BgL_l1243z00_1427);
								BgL_l1243z00_1427 = BgL_l1243z00_2570;
								goto BgL_zc3z04anonymousza31332ze3z87_1428;
							}
						}
					else
						{	/* SawMill/procedures.scm 18 */
							((bool_t) 1);
						}
				}
				return CELL_REF(BgL_rz00_2235);
			}
		}

	}



/* &get-procedures */
	obj_t BGl_z62getzd2procedureszb0zzsaw_proceduresz00(obj_t BgL_envz00_2229,
		obj_t BgL_globalsz00_2230)
	{
		{	/* SawMill/procedures.scm 12 */
			return BGl_getzd2procedureszd2zzsaw_proceduresz00(BgL_globalsz00_2230);
		}

	}



/* &<@anonymous:1328> */
	obj_t BGl_z62zc3z04anonymousza31328ze3ze5zzsaw_proceduresz00(obj_t
		BgL_envz00_2231, obj_t BgL_globalz00_2233)
	{
		{	/* SawMill/procedures.scm 14 */
			{	/* SawMill/procedures.scm 15 */
				obj_t BgL_rz00_2232;

				BgL_rz00_2232 = PROCEDURE_REF(BgL_envz00_2231, (int) (0L));
				{	/* SawMill/procedures.scm 15 */
					bool_t BgL_test1937z00_2575;

					if (CBOOL(BGl_needzd2functionzd2pointerz00zzsaw_elsewherez00(
								((BgL_globalz00_bglt) BgL_globalz00_2233))))
						{	/* SawMill/procedures.scm 15 */
							if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
									(BgL_globalz00_2233, CELL_REF(BgL_rz00_2232))))
								{	/* SawMill/procedures.scm 16 */
									BgL_test1937z00_2575 = ((bool_t) 0);
								}
							else
								{	/* SawMill/procedures.scm 16 */
									BgL_test1937z00_2575 = ((bool_t) 1);
								}
						}
					else
						{	/* SawMill/procedures.scm 15 */
							BgL_test1937z00_2575 = ((bool_t) 0);
						}
					if (BgL_test1937z00_2575)
						{	/* SawMill/procedures.scm 17 */
							obj_t BgL_auxz00_2372;

							BgL_auxz00_2372 =
								MAKE_YOUNG_PAIR(BgL_globalz00_2233, CELL_REF(BgL_rz00_2232));
							return CELL_SET(BgL_rz00_2232, BgL_auxz00_2372);
						}
					else
						{	/* SawMill/procedures.scm 15 */
							return BFALSE;
						}
				}
			}
		}

	}



/* get* */
	obj_t BGl_getza2za2zzsaw_proceduresz00(obj_t BgL_lz00_4, obj_t BgL_rz00_5)
	{
		{	/* SawMill/procedures.scm 23 */
		BGl_getza2za2zzsaw_proceduresz00:
			if (NULLP(BgL_lz00_4))
				{	/* SawMill/procedures.scm 24 */
					return BgL_rz00_5;
				}
			else
				{	/* SawMill/procedures.scm 26 */
					obj_t BgL_arg1342z00_1436;
					obj_t BgL_arg1343z00_1437;

					BgL_arg1342z00_1436 = CDR(((obj_t) BgL_lz00_4));
					{	/* SawMill/procedures.scm 26 */
						obj_t BgL_arg1346z00_1438;

						BgL_arg1346z00_1438 = CAR(((obj_t) BgL_lz00_4));
						BgL_arg1343z00_1437 =
							BGl_getz00zzsaw_proceduresz00(
							((BgL_nodez00_bglt) BgL_arg1346z00_1438), BgL_rz00_5);
					}
					{
						obj_t BgL_rz00_2593;
						obj_t BgL_lz00_2592;

						BgL_lz00_2592 = BgL_arg1342z00_1436;
						BgL_rz00_2593 = BgL_arg1343z00_1437;
						BgL_rz00_5 = BgL_rz00_2593;
						BgL_lz00_4 = BgL_lz00_2592;
						goto BGl_getza2za2zzsaw_proceduresz00;
					}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_proceduresz00(void)
	{
		{	/* SawMill/procedures.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_proceduresz00(void)
	{
		{	/* SawMill/procedures.scm 1 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00,
				BGl_proc1861z00zzsaw_proceduresz00, BGl_nodez00zzast_nodez00,
				BGl_string1862z00zzsaw_proceduresz00);
		}

	}



/* &get1262 */
	obj_t BGl_z62get1262z62zzsaw_proceduresz00(obj_t BgL_envz00_2239,
		obj_t BgL_ez00_2240, obj_t BgL_rz00_2241)
	{
		{	/* SawMill/procedures.scm 31 */
			{	/* SawMill/procedures.scm 32 */
				obj_t BgL_arg1349z00_2374;

				{	/* SawMill/procedures.scm 32 */
					obj_t BgL_arg1351z00_2375;

					{	/* SawMill/procedures.scm 32 */
						obj_t BgL_arg1815z00_2376;
						long BgL_arg1816z00_2377;

						BgL_arg1815z00_2376 = (BGl_za2classesza2z00zz__objectz00);
						{	/* SawMill/procedures.scm 32 */
							long BgL_arg1817z00_2378;

							BgL_arg1817z00_2378 =
								BGL_OBJECT_CLASS_NUM(
								((BgL_objectz00_bglt) ((BgL_nodez00_bglt) BgL_ez00_2240)));
							BgL_arg1816z00_2377 = (BgL_arg1817z00_2378 - OBJECT_TYPE);
						}
						BgL_arg1351z00_2375 =
							VECTOR_REF(BgL_arg1815z00_2376, BgL_arg1816z00_2377);
					}
					BgL_arg1349z00_2374 =
						BGl_classzd2namezd2zz__objectz00(BgL_arg1351z00_2375);
				}
				return
					BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
					BGl_string1863z00zzsaw_proceduresz00, BgL_arg1349z00_2374);
			}
		}

	}



/* get */
	obj_t BGl_getz00zzsaw_proceduresz00(BgL_nodez00_bglt BgL_ez00_6,
		obj_t BgL_rz00_7)
	{
		{	/* SawMill/procedures.scm 31 */
			{	/* SawMill/procedures.scm 31 */
				obj_t BgL_method1263z00_1446;

				{	/* SawMill/procedures.scm 31 */
					obj_t BgL_res1854z00_1956;

					{	/* SawMill/procedures.scm 31 */
						long BgL_objzd2classzd2numz00_1927;

						BgL_objzd2classzd2numz00_1927 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_ez00_6));
						{	/* SawMill/procedures.scm 31 */
							obj_t BgL_arg1811z00_1928;

							BgL_arg1811z00_1928 =
								PROCEDURE_REF(BGl_getzd2envzd2zzsaw_proceduresz00, (int) (1L));
							{	/* SawMill/procedures.scm 31 */
								int BgL_offsetz00_1931;

								BgL_offsetz00_1931 = (int) (BgL_objzd2classzd2numz00_1927);
								{	/* SawMill/procedures.scm 31 */
									long BgL_offsetz00_1932;

									BgL_offsetz00_1932 =
										((long) (BgL_offsetz00_1931) - OBJECT_TYPE);
									{	/* SawMill/procedures.scm 31 */
										long BgL_modz00_1933;

										BgL_modz00_1933 =
											(BgL_offsetz00_1932 >> (int) ((long) ((int) (4L))));
										{	/* SawMill/procedures.scm 31 */
											long BgL_restz00_1935;

											BgL_restz00_1935 =
												(BgL_offsetz00_1932 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawMill/procedures.scm 31 */

												{	/* SawMill/procedures.scm 31 */
													obj_t BgL_bucketz00_1937;

													BgL_bucketz00_1937 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1928), BgL_modz00_1933);
													BgL_res1854z00_1956 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1937), BgL_restz00_1935);
					}}}}}}}}
					BgL_method1263z00_1446 = BgL_res1854z00_1956;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1263z00_1446,
					((obj_t) BgL_ez00_6), BgL_rz00_7);
			}
		}

	}



/* &get */
	obj_t BGl_z62getz62zzsaw_proceduresz00(obj_t BgL_envz00_2242,
		obj_t BgL_ez00_2243, obj_t BgL_rz00_2244)
	{
		{	/* SawMill/procedures.scm 31 */
			return
				BGl_getz00zzsaw_proceduresz00(
				((BgL_nodez00_bglt) BgL_ez00_2243), BgL_rz00_2244);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_proceduresz00(void)
	{
		{	/* SawMill/procedures.scm 1 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_atomz00zzast_nodez00,
				BGl_proc1864z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_varz00zzast_nodez00,
				BGl_proc1866z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc1867z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_setqz00zzast_nodez00,
				BGl_proc1868z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_sequencez00zzast_nodez00,
				BGl_proc1869z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_syncz00zzast_nodez00,
				BGl_proc1870z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc1871z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_switchz00zzast_nodez00,
				BGl_proc1872z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc1873z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_appz00zzast_nodez00,
				BGl_proc1874z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc1875z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_funcallz00zzast_nodez00,
				BGl_proc1876z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_pragmaz00zzast_nodez00,
				BGl_proc1877z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_getfieldz00zzast_nodez00,
				BGl_proc1878z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_setfieldz00zzast_nodez00,
				BGl_proc1879z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_newz00zzast_nodez00,
				BGl_proc1880z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_vallocz00zzast_nodez00,
				BGl_proc1881z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_vrefz00zzast_nodez00,
				BGl_proc1882z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_vsetz12z12zzast_nodez00,
				BGl_proc1883z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_vlengthz00zzast_nodez00,
				BGl_proc1884z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_instanceofz00zzast_nodez00,
				BGl_proc1885z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_castzd2nullzd2zzast_nodez00,
				BGl_proc1886z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_castz00zzast_nodez00,
				BGl_proc1887z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc1888z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1889z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_failz00zzast_nodez00,
				BGl_proc1890z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc1891z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc1892z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2envzd2zzsaw_proceduresz00, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc1893z00zzsaw_proceduresz00,
				BGl_string1865z00zzsaw_proceduresz00);
		}

	}



/* &get-box-set!1321 */
	obj_t BGl_z62getzd2boxzd2setz121321z70zzsaw_proceduresz00(obj_t
		BgL_envz00_2274, obj_t BgL_ez00_2275, obj_t BgL_rz00_2276)
	{
		{	/* SawMill/procedures.scm 188 */
			{	/* SawMill/procedures.scm 190 */
				BgL_varz00_bglt BgL_arg1720z00_2380;
				obj_t BgL_arg1722z00_2381;

				BgL_arg1720z00_2380 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_ez00_2275)))->BgL_varz00);
				BgL_arg1722z00_2381 =
					BGl_getz00zzsaw_proceduresz00(
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_ez00_2275)))->BgL_valuez00),
					BgL_rz00_2276);
				return BGl_getz00zzsaw_proceduresz00(((BgL_nodez00_bglt)
						BgL_arg1720z00_2380), BgL_arg1722z00_2381);
			}
		}

	}



/* &get-box-ref1319 */
	obj_t BGl_z62getzd2boxzd2ref1319z62zzsaw_proceduresz00(obj_t BgL_envz00_2277,
		obj_t BgL_ez00_2278, obj_t BgL_rz00_2279)
	{
		{	/* SawMill/procedures.scm 183 */
			{	/* SawMill/procedures.scm 185 */
				BgL_varz00_bglt BgL_arg1718z00_2383;

				BgL_arg1718z00_2383 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_ez00_2278)))->BgL_varz00);
				return
					BGl_getz00zzsaw_proceduresz00(
					((BgL_nodez00_bglt) BgL_arg1718z00_2383), BgL_rz00_2279);
			}
		}

	}



/* &get-make-box1317 */
	obj_t BGl_z62getzd2makezd2box1317z62zzsaw_proceduresz00(obj_t BgL_envz00_2280,
		obj_t BgL_ez00_2281, obj_t BgL_rz00_2282)
	{
		{	/* SawMill/procedures.scm 178 */
			return
				BGl_getz00zzsaw_proceduresz00(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_ez00_2281)))->BgL_valuez00),
				BgL_rz00_2282);
		}

	}



/* &get-fail1315 */
	obj_t BGl_z62getzd2fail1315zb0zzsaw_proceduresz00(obj_t BgL_envz00_2283,
		obj_t BgL_ez00_2284, obj_t BgL_rz00_2285)
	{
		{	/* SawMill/procedures.scm 173 */
			return
				BGl_getz00zzsaw_proceduresz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_ez00_2284)))->BgL_procz00),
				BGl_getz00zzsaw_proceduresz00(
					(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_ez00_2284)))->BgL_msgz00),
					BGl_getz00zzsaw_proceduresz00(
						(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_ez00_2284)))->BgL_objz00),
						BgL_rz00_2285)));
		}

	}



/* &get-jump-ex-it1313 */
	obj_t BGl_z62getzd2jumpzd2exzd2it1313zb0zzsaw_proceduresz00(obj_t
		BgL_envz00_2286, obj_t BgL_ez00_2287, obj_t BgL_rz00_2288)
	{
		{	/* SawMill/procedures.scm 168 */
			return
				BGl_getz00zzsaw_proceduresz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_ez00_2287)))->BgL_exitz00),
				BGl_getz00zzsaw_proceduresz00(
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_ez00_2287)))->BgL_valuez00),
					BgL_rz00_2288));
		}

	}



/* &get-set-ex-it1311 */
	obj_t BGl_z62getzd2setzd2exzd2it1311zb0zzsaw_proceduresz00(obj_t
		BgL_envz00_2289, obj_t BgL_ez00_2290, obj_t BgL_rz00_2291)
	{
		{	/* SawMill/procedures.scm 163 */
			{	/* SawMill/procedures.scm 165 */
				BgL_varz00_bglt BgL_arg1699z00_2388;
				obj_t BgL_arg1700z00_2389;

				BgL_arg1699z00_2388 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_ez00_2290)))->BgL_varz00);
				BgL_arg1700z00_2389 =
					BGl_getz00zzsaw_proceduresz00(
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_ez00_2290)))->BgL_bodyz00),
					BgL_rz00_2291);
				return BGl_getz00zzsaw_proceduresz00(((BgL_nodez00_bglt)
						BgL_arg1699z00_2388), BgL_arg1700z00_2389);
			}
		}

	}



/* &get-cast1309 */
	obj_t BGl_z62getzd2cast1309zb0zzsaw_proceduresz00(obj_t BgL_envz00_2292,
		obj_t BgL_ez00_2293, obj_t BgL_rz00_2294)
	{
		{	/* SawMill/procedures.scm 158 */
			return
				BGl_getz00zzsaw_proceduresz00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_ez00_2293)))->BgL_argz00), BgL_rz00_2294);
		}

	}



/* &get-cast-null1307 */
	obj_t BGl_z62getzd2castzd2null1307z62zzsaw_proceduresz00(obj_t
		BgL_envz00_2295, obj_t BgL_ez00_2296, obj_t BgL_rz00_2297)
	{
		{	/* SawMill/procedures.scm 154 */
			return BgL_rz00_2297;
		}

	}



/* &get-instanceof1305 */
	obj_t BGl_z62getzd2instanceof1305zb0zzsaw_proceduresz00(obj_t BgL_envz00_2298,
		obj_t BgL_ez00_2299, obj_t BgL_rz00_2300)
	{
		{	/* SawMill/procedures.scm 149 */
			{	/* SawMill/procedures.scm 151 */
				obj_t BgL_arg1691z00_2393;

				BgL_arg1691z00_2393 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_instanceofz00_bglt) BgL_ez00_2299))))->BgL_exprza2za2);
				return
					BGl_getza2za2zzsaw_proceduresz00(BgL_arg1691z00_2393, BgL_rz00_2300);
			}
		}

	}



/* &get-vlength1303 */
	obj_t BGl_z62getzd2vlength1303zb0zzsaw_proceduresz00(obj_t BgL_envz00_2301,
		obj_t BgL_ez00_2302, obj_t BgL_rz00_2303)
	{
		{	/* SawMill/procedures.scm 144 */
			{	/* SawMill/procedures.scm 146 */
				obj_t BgL_arg1689z00_2395;

				BgL_arg1689z00_2395 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vlengthz00_bglt) BgL_ez00_2302))))->BgL_exprza2za2);
				return
					BGl_getza2za2zzsaw_proceduresz00(BgL_arg1689z00_2395, BgL_rz00_2303);
			}
		}

	}



/* &get-vset!1301 */
	obj_t BGl_z62getzd2vsetz121301za2zzsaw_proceduresz00(obj_t BgL_envz00_2304,
		obj_t BgL_ez00_2305, obj_t BgL_rz00_2306)
	{
		{	/* SawMill/procedures.scm 139 */
			{	/* SawMill/procedures.scm 141 */
				obj_t BgL_arg1688z00_2397;

				BgL_arg1688z00_2397 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vsetz12z12_bglt) BgL_ez00_2305))))->BgL_exprza2za2);
				return
					BGl_getza2za2zzsaw_proceduresz00(BgL_arg1688z00_2397, BgL_rz00_2306);
			}
		}

	}



/* &get-vref1299 */
	obj_t BGl_z62getzd2vref1299zb0zzsaw_proceduresz00(obj_t BgL_envz00_2307,
		obj_t BgL_ez00_2308, obj_t BgL_rz00_2309)
	{
		{	/* SawMill/procedures.scm 134 */
			{	/* SawMill/procedures.scm 136 */
				obj_t BgL_arg1681z00_2399;

				BgL_arg1681z00_2399 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vrefz00_bglt) BgL_ez00_2308))))->BgL_exprza2za2);
				return
					BGl_getza2za2zzsaw_proceduresz00(BgL_arg1681z00_2399, BgL_rz00_2309);
			}
		}

	}



/* &get-valloc1297 */
	obj_t BGl_z62getzd2valloc1297zb0zzsaw_proceduresz00(obj_t BgL_envz00_2310,
		obj_t BgL_ez00_2311, obj_t BgL_rz00_2312)
	{
		{	/* SawMill/procedures.scm 129 */
			{	/* SawMill/procedures.scm 131 */
				obj_t BgL_arg1678z00_2401;

				BgL_arg1678z00_2401 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vallocz00_bglt) BgL_ez00_2311))))->BgL_exprza2za2);
				return
					BGl_getza2za2zzsaw_proceduresz00(BgL_arg1678z00_2401, BgL_rz00_2312);
			}
		}

	}



/* &get-new1295 */
	obj_t BGl_z62getzd2new1295zb0zzsaw_proceduresz00(obj_t BgL_envz00_2313,
		obj_t BgL_ez00_2314, obj_t BgL_rz00_2315)
	{
		{	/* SawMill/procedures.scm 125 */
			return BgL_rz00_2315;
		}

	}



/* &get-setfield1293 */
	obj_t BGl_z62getzd2setfield1293zb0zzsaw_proceduresz00(obj_t BgL_envz00_2316,
		obj_t BgL_ez00_2317, obj_t BgL_rz00_2318)
	{
		{	/* SawMill/procedures.scm 120 */
			{	/* SawMill/procedures.scm 122 */
				obj_t BgL_arg1675z00_2404;

				BgL_arg1675z00_2404 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_setfieldz00_bglt) BgL_ez00_2317))))->BgL_exprza2za2);
				return
					BGl_getza2za2zzsaw_proceduresz00(BgL_arg1675z00_2404, BgL_rz00_2318);
			}
		}

	}



/* &get-getfield1291 */
	obj_t BGl_z62getzd2getfield1291zb0zzsaw_proceduresz00(obj_t BgL_envz00_2319,
		obj_t BgL_ez00_2320, obj_t BgL_rz00_2321)
	{
		{	/* SawMill/procedures.scm 115 */
			{	/* SawMill/procedures.scm 117 */
				obj_t BgL_arg1663z00_2406;

				BgL_arg1663z00_2406 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_getfieldz00_bglt) BgL_ez00_2320))))->BgL_exprza2za2);
				return
					BGl_getza2za2zzsaw_proceduresz00(BgL_arg1663z00_2406, BgL_rz00_2321);
			}
		}

	}



/* &get-pragma1289 */
	obj_t BGl_z62getzd2pragma1289zb0zzsaw_proceduresz00(obj_t BgL_envz00_2322,
		obj_t BgL_ez00_2323, obj_t BgL_rz00_2324)
	{
		{	/* SawMill/procedures.scm 110 */
			{	/* SawMill/procedures.scm 112 */
				obj_t BgL_arg1661z00_2408;

				BgL_arg1661z00_2408 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_pragmaz00_bglt) BgL_ez00_2323))))->BgL_exprza2za2);
				return
					BGl_getza2za2zzsaw_proceduresz00(BgL_arg1661z00_2408, BgL_rz00_2324);
			}
		}

	}



/* &get-funcall1287 */
	obj_t BGl_z62getzd2funcall1287zb0zzsaw_proceduresz00(obj_t BgL_envz00_2325,
		obj_t BgL_ez00_2326, obj_t BgL_rz00_2327)
	{
		{	/* SawMill/procedures.scm 103 */
			if (
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_ez00_2326)))->BgL_strengthz00) ==
					CNST_TABLE_REF(1)))
				{	/* SawMill/procedures.scm 105 */
					return
						BGl_getza2za2zzsaw_proceduresz00(
						(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_ez00_2326)))->BgL_argsz00),
						BgL_rz00_2327);
				}
			else
				{	/* SawMill/procedures.scm 105 */
					return
						BGl_getz00zzsaw_proceduresz00(
						(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_ez00_2326)))->BgL_funz00),
						BGl_getza2za2zzsaw_proceduresz00(
							(((BgL_funcallz00_bglt) COBJECT(
										((BgL_funcallz00_bglt) BgL_ez00_2326)))->BgL_argsz00),
							BgL_rz00_2327));
				}
		}

	}



/* &get-app-ly1285 */
	obj_t BGl_z62getzd2appzd2ly1285z62zzsaw_proceduresz00(obj_t BgL_envz00_2328,
		obj_t BgL_ez00_2329, obj_t BgL_rz00_2330)
	{
		{	/* SawMill/procedures.scm 98 */
			return
				BGl_getz00zzsaw_proceduresz00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_ez00_2329)))->BgL_funz00),
				BGl_getz00zzsaw_proceduresz00(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_ez00_2329)))->BgL_argz00),
					BgL_rz00_2330));
		}

	}



/* &get-app1283 */
	obj_t BGl_z62getzd2app1283zb0zzsaw_proceduresz00(obj_t BgL_envz00_2331,
		obj_t BgL_ez00_2332, obj_t BgL_rz00_2333)
	{
		{	/* SawMill/procedures.scm 84 */
			{	/* SawMill/procedures.scm 86 */
				obj_t BgL_rz00_2412;
				BgL_variablez00_bglt BgL_vz00_2413;

				BgL_rz00_2412 =
					BGl_getza2za2zzsaw_proceduresz00(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_ez00_2332)))->BgL_argsz00),
					BgL_rz00_2333);
				BgL_vz00_2413 =
					(((BgL_varz00_bglt) COBJECT((((BgL_appz00_bglt)
									COBJECT(((BgL_appz00_bglt) BgL_ez00_2332)))->BgL_funz00)))->
					BgL_variablez00);
				{	/* SawMill/procedures.scm 87 */
					bool_t BgL_test1942z00_2763;

					{	/* SawMill/procedures.scm 87 */
						obj_t BgL_classz00_2414;

						BgL_classz00_2414 = BGl_localz00zzast_varz00;
						{	/* SawMill/procedures.scm 87 */
							BgL_objectz00_bglt BgL_arg1807z00_2415;

							{	/* SawMill/procedures.scm 87 */
								obj_t BgL_tmpz00_2764;

								BgL_tmpz00_2764 =
									((obj_t) ((BgL_objectz00_bglt) BgL_vz00_2413));
								BgL_arg1807z00_2415 = (BgL_objectz00_bglt) (BgL_tmpz00_2764);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawMill/procedures.scm 87 */
									long BgL_idxz00_2416;

									BgL_idxz00_2416 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2415);
									BgL_test1942z00_2763 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2416 + 2L)) == BgL_classz00_2414);
								}
							else
								{	/* SawMill/procedures.scm 87 */
									bool_t BgL_res1857z00_2419;

									{	/* SawMill/procedures.scm 87 */
										obj_t BgL_oclassz00_2420;

										{	/* SawMill/procedures.scm 87 */
											obj_t BgL_arg1815z00_2421;
											long BgL_arg1816z00_2422;

											BgL_arg1815z00_2421 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawMill/procedures.scm 87 */
												long BgL_arg1817z00_2423;

												BgL_arg1817z00_2423 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2415);
												BgL_arg1816z00_2422 =
													(BgL_arg1817z00_2423 - OBJECT_TYPE);
											}
											BgL_oclassz00_2420 =
												VECTOR_REF(BgL_arg1815z00_2421, BgL_arg1816z00_2422);
										}
										{	/* SawMill/procedures.scm 87 */
											bool_t BgL__ortest_1115z00_2424;

											BgL__ortest_1115z00_2424 =
												(BgL_classz00_2414 == BgL_oclassz00_2420);
											if (BgL__ortest_1115z00_2424)
												{	/* SawMill/procedures.scm 87 */
													BgL_res1857z00_2419 = BgL__ortest_1115z00_2424;
												}
											else
												{	/* SawMill/procedures.scm 87 */
													long BgL_odepthz00_2425;

													{	/* SawMill/procedures.scm 87 */
														obj_t BgL_arg1804z00_2426;

														BgL_arg1804z00_2426 = (BgL_oclassz00_2420);
														BgL_odepthz00_2425 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2426);
													}
													if ((2L < BgL_odepthz00_2425))
														{	/* SawMill/procedures.scm 87 */
															obj_t BgL_arg1802z00_2427;

															{	/* SawMill/procedures.scm 87 */
																obj_t BgL_arg1803z00_2428;

																BgL_arg1803z00_2428 = (BgL_oclassz00_2420);
																BgL_arg1802z00_2427 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2428,
																	2L);
															}
															BgL_res1857z00_2419 =
																(BgL_arg1802z00_2427 == BgL_classz00_2414);
														}
													else
														{	/* SawMill/procedures.scm 87 */
															BgL_res1857z00_2419 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1942z00_2763 = BgL_res1857z00_2419;
								}
						}
					}
					if (BgL_test1942z00_2763)
						{	/* SawMill/procedures.scm 87 */
							return BgL_rz00_2412;
						}
					else
						{	/* SawMill/procedures.scm 89 */
							obj_t BgL_idz00_2429;

							BgL_idz00_2429 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_globalz00_bglt) BgL_vz00_2413))))->BgL_idz00);
							if ((BgL_idz00_2429 == CNST_TABLE_REF(2)))
								{	/* SawMill/procedures.scm 91 */
									BgL_variablez00_bglt BgL_gz00_2430;

									{
										BgL_varz00_bglt BgL_auxz00_2793;

										{	/* SawMill/procedures.scm 91 */
											obj_t BgL_pairz00_2431;

											BgL_pairz00_2431 =
												(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_ez00_2332)))->BgL_argsz00);
											BgL_auxz00_2793 =
												((BgL_varz00_bglt) CAR(BgL_pairz00_2431));
										}
										BgL_gz00_2430 =
											(((BgL_varz00_bglt) COBJECT(BgL_auxz00_2793))->
											BgL_variablez00);
									}
									if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
												((obj_t) BgL_gz00_2430), BgL_rz00_2412)))
										{	/* SawMill/procedures.scm 92 */
											return BgL_rz00_2412;
										}
									else
										{	/* SawMill/procedures.scm 92 */
											return
												MAKE_YOUNG_PAIR(((obj_t) BgL_gz00_2430), BgL_rz00_2412);
										}
								}
							else
								{	/* SawMill/procedures.scm 90 */
									return BgL_rz00_2412;
								}
						}
				}
			}
		}

	}



/* &get-let-fun1281 */
	obj_t BGl_z62getzd2letzd2fun1281z62zzsaw_proceduresz00(obj_t BgL_envz00_2334,
		obj_t BgL_ez00_2335, obj_t BgL_rz00_2336)
	{
		{	/* SawMill/procedures.scm 78 */
			{	/* SawMill/procedures.scm 80 */
				obj_t BgL_arg1593z00_2433;
				obj_t BgL_arg1594z00_2434;

				{	/* SawMill/procedures.scm 80 */
					obj_t BgL_l1257z00_2435;

					BgL_l1257z00_2435 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_ez00_2335)))->BgL_localsz00);
					if (NULLP(BgL_l1257z00_2435))
						{	/* SawMill/procedures.scm 80 */
							BgL_arg1593z00_2433 = BNIL;
						}
					else
						{	/* SawMill/procedures.scm 80 */
							obj_t BgL_head1259z00_2436;

							BgL_head1259z00_2436 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1257z00_2438;
								obj_t BgL_tail1260z00_2439;

								BgL_l1257z00_2438 = BgL_l1257z00_2435;
								BgL_tail1260z00_2439 = BgL_head1259z00_2436;
							BgL_zc3z04anonymousza31596ze3z87_2437:
								if (NULLP(BgL_l1257z00_2438))
									{	/* SawMill/procedures.scm 80 */
										BgL_arg1593z00_2433 = CDR(BgL_head1259z00_2436);
									}
								else
									{	/* SawMill/procedures.scm 80 */
										obj_t BgL_newtail1261z00_2440;

										{	/* SawMill/procedures.scm 80 */
											obj_t BgL_arg1605z00_2441;

											BgL_arg1605z00_2441 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt)
															(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt)
																				CAR(
																					((obj_t) BgL_l1257z00_2438))))))->
																BgL_valuez00))))->BgL_bodyz00);
											BgL_newtail1261z00_2440 =
												MAKE_YOUNG_PAIR(BgL_arg1605z00_2441, BNIL);
										}
										SET_CDR(BgL_tail1260z00_2439, BgL_newtail1261z00_2440);
										{	/* SawMill/procedures.scm 80 */
											obj_t BgL_arg1602z00_2442;

											BgL_arg1602z00_2442 = CDR(((obj_t) BgL_l1257z00_2438));
											{
												obj_t BgL_tail1260z00_2825;
												obj_t BgL_l1257z00_2824;

												BgL_l1257z00_2824 = BgL_arg1602z00_2442;
												BgL_tail1260z00_2825 = BgL_newtail1261z00_2440;
												BgL_tail1260z00_2439 = BgL_tail1260z00_2825;
												BgL_l1257z00_2438 = BgL_l1257z00_2824;
												goto BgL_zc3z04anonymousza31596ze3z87_2437;
											}
										}
									}
							}
						}
				}
				BgL_arg1594z00_2434 =
					BGl_getz00zzsaw_proceduresz00(
					(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_ez00_2335)))->BgL_bodyz00),
					BgL_rz00_2336);
				return BGl_getza2za2zzsaw_proceduresz00(BgL_arg1593z00_2433,
					BgL_arg1594z00_2434);
			}
		}

	}



/* &get-switch1279 */
	obj_t BGl_z62getzd2switch1279zb0zzsaw_proceduresz00(obj_t BgL_envz00_2337,
		obj_t BgL_ez00_2338, obj_t BgL_rz00_2339)
	{
		{	/* SawMill/procedures.scm 73 */
			{	/* SawMill/procedures.scm 75 */
				BgL_nodez00_bglt BgL_arg1564z00_2444;
				obj_t BgL_arg1565z00_2445;

				BgL_arg1564z00_2444 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_ez00_2338)))->BgL_testz00);
				{	/* SawMill/procedures.scm 75 */
					obj_t BgL_arg1571z00_2446;

					{	/* SawMill/procedures.scm 75 */
						obj_t BgL_l1251z00_2447;

						BgL_l1251z00_2447 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_ez00_2338)))->BgL_clausesz00);
						if (NULLP(BgL_l1251z00_2447))
							{	/* SawMill/procedures.scm 75 */
								BgL_arg1571z00_2446 = BNIL;
							}
						else
							{	/* SawMill/procedures.scm 75 */
								obj_t BgL_head1253z00_2448;

								{	/* SawMill/procedures.scm 75 */
									obj_t BgL_arg1589z00_2449;

									{	/* SawMill/procedures.scm 75 */
										obj_t BgL_pairz00_2450;

										BgL_pairz00_2450 = CAR(((obj_t) BgL_l1251z00_2447));
										BgL_arg1589z00_2449 = CDR(BgL_pairz00_2450);
									}
									BgL_head1253z00_2448 =
										MAKE_YOUNG_PAIR(BgL_arg1589z00_2449, BNIL);
								}
								{	/* SawMill/procedures.scm 75 */
									obj_t BgL_g1256z00_2451;

									BgL_g1256z00_2451 = CDR(((obj_t) BgL_l1251z00_2447));
									{
										obj_t BgL_l1251z00_2453;
										obj_t BgL_tail1254z00_2454;

										BgL_l1251z00_2453 = BgL_g1256z00_2451;
										BgL_tail1254z00_2454 = BgL_head1253z00_2448;
									BgL_zc3z04anonymousza31574ze3z87_2452:
										if (NULLP(BgL_l1251z00_2453))
											{	/* SawMill/procedures.scm 75 */
												BgL_arg1571z00_2446 = BgL_head1253z00_2448;
											}
										else
											{	/* SawMill/procedures.scm 75 */
												obj_t BgL_newtail1255z00_2455;

												{	/* SawMill/procedures.scm 75 */
													obj_t BgL_arg1584z00_2456;

													{	/* SawMill/procedures.scm 75 */
														obj_t BgL_pairz00_2457;

														BgL_pairz00_2457 = CAR(((obj_t) BgL_l1251z00_2453));
														BgL_arg1584z00_2456 = CDR(BgL_pairz00_2457);
													}
													BgL_newtail1255z00_2455 =
														MAKE_YOUNG_PAIR(BgL_arg1584z00_2456, BNIL);
												}
												SET_CDR(BgL_tail1254z00_2454, BgL_newtail1255z00_2455);
												{	/* SawMill/procedures.scm 75 */
													obj_t BgL_arg1576z00_2458;

													BgL_arg1576z00_2458 =
														CDR(((obj_t) BgL_l1251z00_2453));
													{
														obj_t BgL_tail1254z00_2852;
														obj_t BgL_l1251z00_2851;

														BgL_l1251z00_2851 = BgL_arg1576z00_2458;
														BgL_tail1254z00_2852 = BgL_newtail1255z00_2455;
														BgL_tail1254z00_2454 = BgL_tail1254z00_2852;
														BgL_l1251z00_2453 = BgL_l1251z00_2851;
														goto BgL_zc3z04anonymousza31574ze3z87_2452;
													}
												}
											}
									}
								}
							}
					}
					BgL_arg1565z00_2445 =
						BGl_getza2za2zzsaw_proceduresz00(BgL_arg1571z00_2446,
						BgL_rz00_2339);
				}
				return
					BGl_getz00zzsaw_proceduresz00(BgL_arg1564z00_2444,
					BgL_arg1565z00_2445);
			}
		}

	}



/* &get-conditional1277 */
	obj_t BGl_z62getzd2conditional1277zb0zzsaw_proceduresz00(obj_t
		BgL_envz00_2340, obj_t BgL_ez00_2341, obj_t BgL_rz00_2342)
	{
		{	/* SawMill/procedures.scm 68 */
			return
				BGl_getz00zzsaw_proceduresz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_ez00_2341)))->BgL_testz00),
				BGl_getz00zzsaw_proceduresz00(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_ez00_2341)))->BgL_truez00),
					BGl_getz00zzsaw_proceduresz00(
						(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_ez00_2341)))->BgL_falsez00),
						BgL_rz00_2342)));
		}

	}



/* &get-sync1275 */
	obj_t BGl_z62getzd2sync1275zb0zzsaw_proceduresz00(obj_t BgL_envz00_2343,
		obj_t BgL_ez00_2344, obj_t BgL_rz00_2345)
	{
		{	/* SawMill/procedures.scm 63 */
			return
				BGl_getz00zzsaw_proceduresz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_ez00_2344)))->BgL_prelockz00),
				BGl_getz00zzsaw_proceduresz00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_ez00_2344)))->BgL_mutexz00),
					BGl_getz00zzsaw_proceduresz00(
						(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_ez00_2344)))->BgL_bodyz00),
						BgL_rz00_2345)));
		}

	}



/* &get-sequence1273 */
	obj_t BGl_z62getzd2sequence1273zb0zzsaw_proceduresz00(obj_t BgL_envz00_2346,
		obj_t BgL_ez00_2347, obj_t BgL_rz00_2348)
	{
		{	/* SawMill/procedures.scm 58 */
			return
				BGl_getza2za2zzsaw_proceduresz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_ez00_2347)))->BgL_nodesz00),
				BgL_rz00_2348);
		}

	}



/* &get-setq1271 */
	obj_t BGl_z62getzd2setq1271zb0zzsaw_proceduresz00(obj_t BgL_envz00_2349,
		obj_t BgL_ez00_2350, obj_t BgL_rz00_2351)
	{
		{	/* SawMill/procedures.scm 53 */
			return
				BGl_getz00zzsaw_proceduresz00(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_ez00_2350)))->BgL_valuez00),
				BgL_rz00_2351);
		}

	}



/* &get-let-var1269 */
	obj_t BGl_z62getzd2letzd2var1269z62zzsaw_proceduresz00(obj_t BgL_envz00_2352,
		obj_t BgL_ez00_2353, obj_t BgL_rz00_2354)
	{
		{	/* SawMill/procedures.scm 48 */
			{	/* SawMill/procedures.scm 50 */
				BgL_nodez00_bglt BgL_arg1437z00_2464;
				obj_t BgL_arg1448z00_2465;

				BgL_arg1437z00_2464 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_ez00_2353)))->BgL_bodyz00);
				{	/* SawMill/procedures.scm 50 */
					obj_t BgL_arg1453z00_2466;

					{	/* SawMill/procedures.scm 50 */
						obj_t BgL_l1245z00_2467;

						BgL_l1245z00_2467 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_ez00_2353)))->BgL_bindingsz00);
						if (NULLP(BgL_l1245z00_2467))
							{	/* SawMill/procedures.scm 50 */
								BgL_arg1453z00_2466 = BNIL;
							}
						else
							{	/* SawMill/procedures.scm 50 */
								obj_t BgL_head1247z00_2468;

								{	/* SawMill/procedures.scm 50 */
									obj_t BgL_arg1489z00_2469;

									{	/* SawMill/procedures.scm 50 */
										obj_t BgL_pairz00_2470;

										BgL_pairz00_2470 = CAR(((obj_t) BgL_l1245z00_2467));
										BgL_arg1489z00_2469 = CDR(BgL_pairz00_2470);
									}
									BgL_head1247z00_2468 =
										MAKE_YOUNG_PAIR(BgL_arg1489z00_2469, BNIL);
								}
								{	/* SawMill/procedures.scm 50 */
									obj_t BgL_g1250z00_2471;

									BgL_g1250z00_2471 = CDR(((obj_t) BgL_l1245z00_2467));
									{
										obj_t BgL_l1245z00_2473;
										obj_t BgL_tail1248z00_2474;

										BgL_l1245z00_2473 = BgL_g1250z00_2471;
										BgL_tail1248z00_2474 = BgL_head1247z00_2468;
									BgL_zc3z04anonymousza31456ze3z87_2472:
										if (NULLP(BgL_l1245z00_2473))
											{	/* SawMill/procedures.scm 50 */
												BgL_arg1453z00_2466 = BgL_head1247z00_2468;
											}
										else
											{	/* SawMill/procedures.scm 50 */
												obj_t BgL_newtail1249z00_2475;

												{	/* SawMill/procedures.scm 50 */
													obj_t BgL_arg1473z00_2476;

													{	/* SawMill/procedures.scm 50 */
														obj_t BgL_pairz00_2477;

														BgL_pairz00_2477 = CAR(((obj_t) BgL_l1245z00_2473));
														BgL_arg1473z00_2476 = CDR(BgL_pairz00_2477);
													}
													BgL_newtail1249z00_2475 =
														MAKE_YOUNG_PAIR(BgL_arg1473z00_2476, BNIL);
												}
												SET_CDR(BgL_tail1248z00_2474, BgL_newtail1249z00_2475);
												{	/* SawMill/procedures.scm 50 */
													obj_t BgL_arg1472z00_2478;

													BgL_arg1472z00_2478 =
														CDR(((obj_t) BgL_l1245z00_2473));
													{
														obj_t BgL_tail1248z00_2901;
														obj_t BgL_l1245z00_2900;

														BgL_l1245z00_2900 = BgL_arg1472z00_2478;
														BgL_tail1248z00_2901 = BgL_newtail1249z00_2475;
														BgL_tail1248z00_2474 = BgL_tail1248z00_2901;
														BgL_l1245z00_2473 = BgL_l1245z00_2900;
														goto BgL_zc3z04anonymousza31456ze3z87_2472;
													}
												}
											}
									}
								}
							}
					}
					BgL_arg1448z00_2465 =
						BGl_getza2za2zzsaw_proceduresz00(BgL_arg1453z00_2466,
						BgL_rz00_2354);
				}
				return
					BGl_getz00zzsaw_proceduresz00(BgL_arg1437z00_2464,
					BgL_arg1448z00_2465);
			}
		}

	}



/* &get-var1267 */
	obj_t BGl_z62getzd2var1267zb0zzsaw_proceduresz00(obj_t BgL_envz00_2355,
		obj_t BgL_ez00_2356, obj_t BgL_rz00_2357)
	{
		{	/* SawMill/procedures.scm 39 */
			{	/* SawMill/procedures.scm 41 */
				bool_t BgL_test1954z00_2904;

				{	/* SawMill/procedures.scm 41 */
					bool_t BgL_test1955z00_2905;

					{	/* SawMill/procedures.scm 41 */
						BgL_variablez00_bglt BgL_arg1434z00_2480;

						BgL_arg1434z00_2480 =
							(((BgL_varz00_bglt) COBJECT(
									((BgL_varz00_bglt) BgL_ez00_2356)))->BgL_variablez00);
						{	/* SawMill/procedures.scm 41 */
							obj_t BgL_classz00_2481;

							BgL_classz00_2481 = BGl_globalz00zzast_varz00;
							{	/* SawMill/procedures.scm 41 */
								BgL_objectz00_bglt BgL_arg1807z00_2482;

								{	/* SawMill/procedures.scm 41 */
									obj_t BgL_tmpz00_2908;

									BgL_tmpz00_2908 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg1434z00_2480));
									BgL_arg1807z00_2482 = (BgL_objectz00_bglt) (BgL_tmpz00_2908);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawMill/procedures.scm 41 */
										long BgL_idxz00_2483;

										BgL_idxz00_2483 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2482);
										BgL_test1955z00_2905 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2483 + 2L)) == BgL_classz00_2481);
									}
								else
									{	/* SawMill/procedures.scm 41 */
										bool_t BgL_res1855z00_2486;

										{	/* SawMill/procedures.scm 41 */
											obj_t BgL_oclassz00_2487;

											{	/* SawMill/procedures.scm 41 */
												obj_t BgL_arg1815z00_2488;
												long BgL_arg1816z00_2489;

												BgL_arg1815z00_2488 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawMill/procedures.scm 41 */
													long BgL_arg1817z00_2490;

													BgL_arg1817z00_2490 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2482);
													BgL_arg1816z00_2489 =
														(BgL_arg1817z00_2490 - OBJECT_TYPE);
												}
												BgL_oclassz00_2487 =
													VECTOR_REF(BgL_arg1815z00_2488, BgL_arg1816z00_2489);
											}
											{	/* SawMill/procedures.scm 41 */
												bool_t BgL__ortest_1115z00_2491;

												BgL__ortest_1115z00_2491 =
													(BgL_classz00_2481 == BgL_oclassz00_2487);
												if (BgL__ortest_1115z00_2491)
													{	/* SawMill/procedures.scm 41 */
														BgL_res1855z00_2486 = BgL__ortest_1115z00_2491;
													}
												else
													{	/* SawMill/procedures.scm 41 */
														long BgL_odepthz00_2492;

														{	/* SawMill/procedures.scm 41 */
															obj_t BgL_arg1804z00_2493;

															BgL_arg1804z00_2493 = (BgL_oclassz00_2487);
															BgL_odepthz00_2492 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2493);
														}
														if ((2L < BgL_odepthz00_2492))
															{	/* SawMill/procedures.scm 41 */
																obj_t BgL_arg1802z00_2494;

																{	/* SawMill/procedures.scm 41 */
																	obj_t BgL_arg1803z00_2495;

																	BgL_arg1803z00_2495 = (BgL_oclassz00_2487);
																	BgL_arg1802z00_2494 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2495,
																		2L);
																}
																BgL_res1855z00_2486 =
																	(BgL_arg1802z00_2494 == BgL_classz00_2481);
															}
														else
															{	/* SawMill/procedures.scm 41 */
																BgL_res1855z00_2486 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1955z00_2905 = BgL_res1855z00_2486;
									}
							}
						}
					}
					if (BgL_test1955z00_2905)
						{	/* SawMill/procedures.scm 42 */
							bool_t BgL_test1959z00_2931;

							{	/* SawMill/procedures.scm 42 */
								BgL_valuez00_bglt BgL_arg1421z00_2496;

								BgL_arg1421z00_2496 =
									(((BgL_variablez00_bglt) COBJECT(
											(((BgL_varz00_bglt) COBJECT(
														((BgL_varz00_bglt) BgL_ez00_2356)))->
												BgL_variablez00)))->BgL_valuez00);
								{	/* SawMill/procedures.scm 42 */
									obj_t BgL_classz00_2497;

									BgL_classz00_2497 = BGl_sfunz00zzast_varz00;
									{	/* SawMill/procedures.scm 42 */
										BgL_objectz00_bglt BgL_arg1807z00_2498;

										{	/* SawMill/procedures.scm 42 */
											obj_t BgL_tmpz00_2935;

											BgL_tmpz00_2935 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg1421z00_2496));
											BgL_arg1807z00_2498 =
												(BgL_objectz00_bglt) (BgL_tmpz00_2935);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawMill/procedures.scm 42 */
												long BgL_idxz00_2499;

												BgL_idxz00_2499 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2498);
												BgL_test1959z00_2931 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2499 + 3L)) == BgL_classz00_2497);
											}
										else
											{	/* SawMill/procedures.scm 42 */
												bool_t BgL_res1856z00_2502;

												{	/* SawMill/procedures.scm 42 */
													obj_t BgL_oclassz00_2503;

													{	/* SawMill/procedures.scm 42 */
														obj_t BgL_arg1815z00_2504;
														long BgL_arg1816z00_2505;

														BgL_arg1815z00_2504 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawMill/procedures.scm 42 */
															long BgL_arg1817z00_2506;

															BgL_arg1817z00_2506 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2498);
															BgL_arg1816z00_2505 =
																(BgL_arg1817z00_2506 - OBJECT_TYPE);
														}
														BgL_oclassz00_2503 =
															VECTOR_REF(BgL_arg1815z00_2504,
															BgL_arg1816z00_2505);
													}
													{	/* SawMill/procedures.scm 42 */
														bool_t BgL__ortest_1115z00_2507;

														BgL__ortest_1115z00_2507 =
															(BgL_classz00_2497 == BgL_oclassz00_2503);
														if (BgL__ortest_1115z00_2507)
															{	/* SawMill/procedures.scm 42 */
																BgL_res1856z00_2502 = BgL__ortest_1115z00_2507;
															}
														else
															{	/* SawMill/procedures.scm 42 */
																long BgL_odepthz00_2508;

																{	/* SawMill/procedures.scm 42 */
																	obj_t BgL_arg1804z00_2509;

																	BgL_arg1804z00_2509 = (BgL_oclassz00_2503);
																	BgL_odepthz00_2508 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2509);
																}
																if ((3L < BgL_odepthz00_2508))
																	{	/* SawMill/procedures.scm 42 */
																		obj_t BgL_arg1802z00_2510;

																		{	/* SawMill/procedures.scm 42 */
																			obj_t BgL_arg1803z00_2511;

																			BgL_arg1803z00_2511 =
																				(BgL_oclassz00_2503);
																			BgL_arg1802z00_2510 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2511, 3L);
																		}
																		BgL_res1856z00_2502 =
																			(BgL_arg1802z00_2510 ==
																			BgL_classz00_2497);
																	}
																else
																	{	/* SawMill/procedures.scm 42 */
																		BgL_res1856z00_2502 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1959z00_2931 = BgL_res1856z00_2502;
											}
									}
								}
							}
							if (BgL_test1959z00_2931)
								{	/* SawMill/procedures.scm 42 */
									if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
												((obj_t)
													(((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_ez00_2356)))->
														BgL_variablez00)), BgL_rz00_2357)))
										{	/* SawMill/procedures.scm 43 */
											BgL_test1954z00_2904 = ((bool_t) 0);
										}
									else
										{	/* SawMill/procedures.scm 43 */
											BgL_test1954z00_2904 = ((bool_t) 1);
										}
								}
							else
								{	/* SawMill/procedures.scm 42 */
									BgL_test1954z00_2904 = ((bool_t) 0);
								}
						}
					else
						{	/* SawMill/procedures.scm 41 */
							BgL_test1954z00_2904 = ((bool_t) 0);
						}
				}
				if (BgL_test1954z00_2904)
					{	/* SawMill/procedures.scm 44 */
						BgL_variablez00_bglt BgL_arg1408z00_2512;

						BgL_arg1408z00_2512 =
							(((BgL_varz00_bglt) COBJECT(
									((BgL_varz00_bglt) BgL_ez00_2356)))->BgL_variablez00);
						return
							MAKE_YOUNG_PAIR(((obj_t) BgL_arg1408z00_2512), BgL_rz00_2357);
					}
				else
					{	/* SawMill/procedures.scm 41 */
						return BgL_rz00_2357;
					}
			}
		}

	}



/* &get-atom1265 */
	obj_t BGl_z62getzd2atom1265zb0zzsaw_proceduresz00(obj_t BgL_envz00_2358,
		obj_t BgL_ez00_2359, obj_t BgL_rz00_2360)
	{
		{	/* SawMill/procedures.scm 35 */
			return BgL_rz00_2360;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_proceduresz00(void)
	{
		{	/* SawMill/procedures.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1894z00zzsaw_proceduresz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1894z00zzsaw_proceduresz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1894z00zzsaw_proceduresz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1894z00zzsaw_proceduresz00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_elsewherez00(10713639L,
				BSTRING_TO_STRING(BGl_string1894z00zzsaw_proceduresz00));
		}

	}

#ifdef __cplusplus
}
#endif
