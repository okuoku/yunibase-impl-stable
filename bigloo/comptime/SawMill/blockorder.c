/*===========================================================================*/
/*   (SawMill/blockorder.scm)                                                */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/blockorder.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_BLOCKORDER_TYPE_DEFINITIONS
#define BGL_SAW_BLOCKORDER_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_sawdonez00_bgl
	{
	}                 *BgL_sawdonez00_bglt;

	typedef struct BgL_sawrdonez00_bgl
	{
	}                  *BgL_sawrdonez00_bglt;

	typedef struct BgL_sawdfsz00_bgl
	{
		int BgL_nz00;
	}                *BgL_sawdfsz00_bglt;


#endif													// BGL_SAW_BLOCKORDER_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static bool_t BGl_visitze70ze7zzsaw_blockorderz00(obj_t, BgL_blockz00_bglt);
	static obj_t BGl_z62lambda1849z62zzsaw_blockorderz00(obj_t, obj_t);
	static obj_t BGl_predecessorsz00zzsaw_blockorderz00(BgL_blockz00_bglt);
	static obj_t BGl_z62lambda1850z62zzsaw_blockorderz00(obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_blockorderz00 = BUNSPEC;
	static BgL_blockz00_bglt BGl_z62lambda1799z62zzsaw_blockorderz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_findzd2exitzd2zzsaw_blockorderz00(BgL_blockz00_bglt);
	static obj_t BGl_toplevelzd2initzd2zzsaw_blockorderz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_blockzd2orderingzd2zzsaw_blockorderz00(BgL_blockz00_bglt);
	static obj_t BGl_genericzd2initzd2zzsaw_blockorderz00(void);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzsaw_blockorderz00(void);
	static obj_t BGl_SawRdonez00zzsaw_blockorderz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	static obj_t BGl_za2blockorderingza2z00zzsaw_blockorderz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzsaw_blockorderz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_blockorderz00(void);
	static obj_t BGl_SawDonez00zzsaw_blockorderz00 = BUNSPEC;
	static obj_t BGl_SawDfsz00zzsaw_blockorderz00 = BUNSPEC;
	static long BGl_lengthuncolsuccz00zzsaw_blockorderz00(BgL_blockz00_bglt);
	extern obj_t BGl_rtl_lastz00zzsaw_defsz00;
	extern obj_t BGl_rtl_returnz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_blockorderz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_blockorderz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_blockorderz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static bool_t BGl_dfsze70ze7zzsaw_blockorderz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_blockorderz00(void);
	static bool_t BGl_dfsze71ze7zzsaw_blockorderz00(obj_t, obj_t);
	static bool_t BGl_dfsze72ze7zzsaw_blockorderz00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_blockorderz00(void);
	static obj_t BGl_dfszd2orderingzd2zzsaw_blockorderz00(BgL_blockz00_bglt);
	static obj_t BGl_rdfszd2orderingzd2zzsaw_blockorderz00(BgL_blockz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31841ze3ze5zzsaw_blockorderz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31809ze3ze5zzsaw_blockorderz00(obj_t,
		obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1807z62zzsaw_blockorderz00(obj_t,
		obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1810z62zzsaw_blockorderz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t BGl_blockz00zzsaw_defsz00;
	static BgL_blockz00_bglt BGl_z62lambda1750z62zzsaw_blockorderz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31755ze3ze5zzsaw_blockorderz00(obj_t,
		obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1753z62zzsaw_blockorderz00(obj_t,
		obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1836z62zzsaw_blockorderz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1756z62zzsaw_blockorderz00(obj_t,
		obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1839z62zzsaw_blockorderz00(obj_t,
		obj_t);
	static obj_t BGl_z62lengthuncolsucczc3zd3zf3z81zzsaw_blockorderz00(obj_t,
		obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1842z62zzsaw_blockorderz00(obj_t,
		obj_t);
	static obj_t BGl_z62blockzd2orderingzb0zzsaw_blockorderz00(obj_t, obj_t);
	static obj_t __cnst[7];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockzd2orderingzd2envz00zzsaw_blockorderz00,
		BgL_bgl_za762blockza7d2order1989z00,
		BGl_z62blockzd2orderingzb0zzsaw_blockorderz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE
		(BGl_lengthuncolsucczc3zd3zf3zd2envz31zzsaw_blockorderz00,
		BgL_bgl_za762lengthuncolsu1990za7,
		BGl_z62lengthuncolsucczc3zd3zf3z81zzsaw_blockorderz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1972z00zzsaw_blockorderz00,
		BgL_bgl_za762lambda1756za7621991z00,
		BGl_z62lambda1756z62zzsaw_blockorderz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1973z00zzsaw_blockorderz00,
		BgL_bgl_za762za7c3za704anonymo1992za7,
		BGl_z62zc3z04anonymousza31755ze3ze5zzsaw_blockorderz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1974z00zzsaw_blockorderz00,
		BgL_bgl_za762lambda1753za7621993z00,
		BGl_z62lambda1753z62zzsaw_blockorderz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1975z00zzsaw_blockorderz00,
		BgL_bgl_za762lambda1750za7621994z00,
		BGl_z62lambda1750z62zzsaw_blockorderz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1976z00zzsaw_blockorderz00,
		BgL_bgl_za762lambda1810za7621995z00,
		BGl_z62lambda1810z62zzsaw_blockorderz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1977z00zzsaw_blockorderz00,
		BgL_bgl_za762za7c3za704anonymo1996za7,
		BGl_z62zc3z04anonymousza31809ze3ze5zzsaw_blockorderz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1978z00zzsaw_blockorderz00,
		BgL_bgl_za762lambda1807za7621997z00,
		BGl_z62lambda1807z62zzsaw_blockorderz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1979z00zzsaw_blockorderz00,
		BgL_bgl_za762lambda1799za7621998z00,
		BGl_z62lambda1799z62zzsaw_blockorderz00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string1986z00zzsaw_blockorderz00,
		BgL_bgl_string1986za700za7za7s1999za7, "saw_blockorder", 14);
	      DEFINE_STRING(BGl_string1987z00zzsaw_blockorderz00,
		BgL_bgl_string1987za700za7za7s2000za7,
		"SawDfs int n SawRdone saw_blockorder SawDone reverse-dfs ", 57);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1980z00zzsaw_blockorderz00,
		BgL_bgl_za762lambda1850za7622001z00,
		BGl_z62lambda1850z62zzsaw_blockorderz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1981z00zzsaw_blockorderz00,
		BgL_bgl_za762lambda1849za7622002z00,
		BGl_z62lambda1849z62zzsaw_blockorderz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1982z00zzsaw_blockorderz00,
		BgL_bgl_za762lambda1842za7622003z00,
		BGl_z62lambda1842z62zzsaw_blockorderz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1983z00zzsaw_blockorderz00,
		BgL_bgl_za762za7c3za704anonymo2004za7,
		BGl_z62zc3z04anonymousza31841ze3ze5zzsaw_blockorderz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1984z00zzsaw_blockorderz00,
		BgL_bgl_za762lambda1839za7622005z00,
		BGl_z62lambda1839z62zzsaw_blockorderz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1985z00zzsaw_blockorderz00,
		BgL_bgl_za762lambda1836za7622006z00,
		BGl_z62lambda1836z62zzsaw_blockorderz00, 0L, BUNSPEC, 5);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_blockorderz00));
		     ADD_ROOT((void *) (&BGl_SawRdonez00zzsaw_blockorderz00));
		     ADD_ROOT((void *) (&BGl_za2blockorderingza2z00zzsaw_blockorderz00));
		     ADD_ROOT((void *) (&BGl_SawDonez00zzsaw_blockorderz00));
		     ADD_ROOT((void *) (&BGl_SawDfsz00zzsaw_blockorderz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_blockorderz00(long
		BgL_checksumz00_3217, char *BgL_fromz00_3218)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_blockorderz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_blockorderz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_blockorderz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_blockorderz00();
					BGl_cnstzd2initzd2zzsaw_blockorderz00();
					BGl_importedzd2moduleszd2initz00zzsaw_blockorderz00();
					BGl_objectzd2initzd2zzsaw_blockorderz00();
					return BGl_toplevelzd2initzd2zzsaw_blockorderz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_blockorderz00(void)
	{
		{	/* SawMill/blockorder.scm 1 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_blockorder");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_blockorder");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_blockorder");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_blockorder");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_blockorder");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_blockorder");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_blockorder");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_blockorder");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_blockorderz00(void)
	{
		{	/* SawMill/blockorder.scm 1 */
			{	/* SawMill/blockorder.scm 1 */
				obj_t BgL_cportz00_3135;

				{	/* SawMill/blockorder.scm 1 */
					obj_t BgL_stringz00_3142;

					BgL_stringz00_3142 = BGl_string1987z00zzsaw_blockorderz00;
					{	/* SawMill/blockorder.scm 1 */
						obj_t BgL_startz00_3143;

						BgL_startz00_3143 = BINT(0L);
						{	/* SawMill/blockorder.scm 1 */
							obj_t BgL_endz00_3144;

							BgL_endz00_3144 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3142)));
							{	/* SawMill/blockorder.scm 1 */

								BgL_cportz00_3135 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3142, BgL_startz00_3143, BgL_endz00_3144);
				}}}}
				{
					long BgL_iz00_3136;

					BgL_iz00_3136 = 6L;
				BgL_loopz00_3137:
					if ((BgL_iz00_3136 == -1L))
						{	/* SawMill/blockorder.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawMill/blockorder.scm 1 */
							{	/* SawMill/blockorder.scm 1 */
								obj_t BgL_arg1988z00_3138;

								{	/* SawMill/blockorder.scm 1 */

									{	/* SawMill/blockorder.scm 1 */
										obj_t BgL_locationz00_3140;

										BgL_locationz00_3140 = BBOOL(((bool_t) 0));
										{	/* SawMill/blockorder.scm 1 */

											BgL_arg1988z00_3138 =
												BGl_readz00zz__readerz00(BgL_cportz00_3135,
												BgL_locationz00_3140);
										}
									}
								}
								{	/* SawMill/blockorder.scm 1 */
									int BgL_tmpz00_3245;

									BgL_tmpz00_3245 = (int) (BgL_iz00_3136);
									CNST_TABLE_SET(BgL_tmpz00_3245, BgL_arg1988z00_3138);
							}}
							{	/* SawMill/blockorder.scm 1 */
								int BgL_auxz00_3141;

								BgL_auxz00_3141 = (int) ((BgL_iz00_3136 - 1L));
								{
									long BgL_iz00_3250;

									BgL_iz00_3250 = (long) (BgL_auxz00_3141);
									BgL_iz00_3136 = BgL_iz00_3250;
									goto BgL_loopz00_3137;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_blockorderz00(void)
	{
		{	/* SawMill/blockorder.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_blockorderz00(void)
	{
		{	/* SawMill/blockorder.scm 1 */
			return (BGl_za2blockorderingza2z00zzsaw_blockorderz00 =
				CNST_TABLE_REF(0), BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzsaw_blockorderz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1849;

				BgL_headz00_1849 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1850;
					obj_t BgL_tailz00_1851;

					BgL_prevz00_1850 = BgL_headz00_1849;
					BgL_tailz00_1851 = BgL_l1z00_1;
				BgL_loopz00_1852:
					if (PAIRP(BgL_tailz00_1851))
						{
							obj_t BgL_newzd2prevzd2_1854;

							BgL_newzd2prevzd2_1854 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1851), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1850, BgL_newzd2prevzd2_1854);
							{
								obj_t BgL_tailz00_3261;
								obj_t BgL_prevz00_3260;

								BgL_prevz00_3260 = BgL_newzd2prevzd2_1854;
								BgL_tailz00_3261 = CDR(BgL_tailz00_1851);
								BgL_tailz00_1851 = BgL_tailz00_3261;
								BgL_prevz00_1850 = BgL_prevz00_3260;
								goto BgL_loopz00_1852;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1849);
				}
			}
		}

	}



/* block-ordering */
	BGL_EXPORTED_DEF obj_t
		BGl_blockzd2orderingzd2zzsaw_blockorderz00(BgL_blockz00_bglt BgL_bz00_58)
	{
		{	/* SawMill/blockorder.scm 12 */
			{	/* SawMill/blockorder.scm 13 */
				obj_t BgL_casezd2valuezd2_1901;

				BgL_casezd2valuezd2_1901 = CNST_TABLE_REF(0);
				if ((BgL_casezd2valuezd2_1901 == CNST_TABLE_REF(0)))
					{	/* SawMill/blockorder.scm 13 */
						BGL_TAIL return
							BGl_rdfszd2orderingzd2zzsaw_blockorderz00(BgL_bz00_58);
					}
				else
					{	/* SawMill/blockorder.scm 13 */
						BGL_TAIL return
							BGl_dfszd2orderingzd2zzsaw_blockorderz00(BgL_bz00_58);
					}
			}
		}

	}



/* &block-ordering */
	obj_t BGl_z62blockzd2orderingzb0zzsaw_blockorderz00(obj_t BgL_envz00_3055,
		obj_t BgL_bz00_3056)
	{
		{	/* SawMill/blockorder.scm 12 */
			return
				BGl_blockzd2orderingzd2zzsaw_blockorderz00(
				((BgL_blockz00_bglt) BgL_bz00_3056));
		}

	}



/* dfs-ordering */
	obj_t BGl_dfszd2orderingzd2zzsaw_blockorderz00(BgL_blockz00_bglt BgL_bz00_59)
	{
		{	/* SawMill/blockorder.scm 20 */
			{	/* SawMill/blockorder.scm 21 */
				struct bgl_cell BgL_box2011_3133z00;
				obj_t BgL_rz00_3133;

				BgL_rz00_3133 = MAKE_CELL_STACK(BNIL, BgL_box2011_3133z00);
				BGl_dfsze72ze7zzsaw_blockorderz00(BgL_rz00_3133, ((obj_t) BgL_bz00_59));
				return bgl_reverse_bang(((obj_t) ((obj_t) CELL_REF(BgL_rz00_3133))));
			}
		}

	}



/* dfs~2 */
	bool_t BGl_dfsze72ze7zzsaw_blockorderz00(obj_t BgL_rz00_3131,
		obj_t BgL_bz00_1905)
	{
		{	/* SawMill/blockorder.scm 22 */
			{	/* SawMill/blockorder.scm 23 */
				BgL_sawdonez00_bglt BgL_wide1220z00_1909;

				BgL_wide1220z00_1909 =
					((BgL_sawdonez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sawdonez00_bgl))));
				{	/* SawMill/blockorder.scm 23 */
					obj_t BgL_auxz00_3281;
					BgL_objectz00_bglt BgL_tmpz00_3277;

					BgL_auxz00_3281 = ((obj_t) BgL_wide1220z00_1909);
					BgL_tmpz00_3277 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1905)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3277, BgL_auxz00_3281);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1905)));
				{	/* SawMill/blockorder.scm 23 */
					long BgL_arg1552z00_1910;

					BgL_arg1552z00_1910 =
						BGL_CLASS_NUM(BGl_SawDonez00zzsaw_blockorderz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_bz00_1905))), BgL_arg1552z00_1910);
				}
				((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1905)));
			}
			((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1905));
			{	/* SawMill/blockorder.scm 24 */
				obj_t BgL_auxz00_3132;

				BgL_auxz00_3132 =
					MAKE_YOUNG_PAIR(BgL_bz00_1905,
					((obj_t) ((obj_t) CELL_REF(BgL_rz00_3131))));
				CELL_SET(BgL_rz00_3131, BgL_auxz00_3132);
			}
			{	/* SawMill/blockorder.scm 25 */
				obj_t BgL_g1470z00_1912;

				BgL_g1470z00_1912 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_bz00_1905)))->BgL_succsz00);
				{
					obj_t BgL_l1468z00_1914;

					BgL_l1468z00_1914 = BgL_g1470z00_1912;
				BgL_zc3z04anonymousza31553ze3z87_1915:
					if (PAIRP(BgL_l1468z00_1914))
						{	/* SawMill/blockorder.scm 26 */
							{	/* SawMill/blockorder.scm 25 */
								obj_t BgL_sz00_1917;

								BgL_sz00_1917 = CAR(BgL_l1468z00_1914);
								{	/* SawMill/blockorder.scm 25 */
									bool_t BgL_test2013z00_3304;

									{	/* SawMill/blockorder.scm 25 */
										obj_t BgL_classz00_2607;

										BgL_classz00_2607 = BGl_SawDonez00zzsaw_blockorderz00;
										if (BGL_OBJECTP(BgL_sz00_1917))
											{	/* SawMill/blockorder.scm 25 */
												obj_t BgL_oclassz00_2609;

												{	/* SawMill/blockorder.scm 25 */
													obj_t BgL_arg1815z00_2611;
													long BgL_arg1816z00_2612;

													BgL_arg1815z00_2611 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/blockorder.scm 25 */
														long BgL_arg1817z00_2613;

														BgL_arg1817z00_2613 =
															BGL_OBJECT_CLASS_NUM(
															((BgL_objectz00_bglt) BgL_sz00_1917));
														BgL_arg1816z00_2612 =
															(BgL_arg1817z00_2613 - OBJECT_TYPE);
													}
													BgL_oclassz00_2609 =
														VECTOR_REF(BgL_arg1815z00_2611,
														BgL_arg1816z00_2612);
												}
												BgL_test2013z00_3304 =
													(BgL_oclassz00_2609 == BgL_classz00_2607);
											}
										else
											{	/* SawMill/blockorder.scm 25 */
												BgL_test2013z00_3304 = ((bool_t) 0);
											}
									}
									if (BgL_test2013z00_3304)
										{	/* SawMill/blockorder.scm 25 */
											((bool_t) 0);
										}
									else
										{	/* SawMill/blockorder.scm 25 */
											BGl_dfsze72ze7zzsaw_blockorderz00(BgL_rz00_3131,
												BgL_sz00_1917);
										}
								}
							}
							{
								obj_t BgL_l1468z00_3314;

								BgL_l1468z00_3314 = CDR(BgL_l1468z00_1914);
								BgL_l1468z00_1914 = BgL_l1468z00_3314;
								goto BgL_zc3z04anonymousza31553ze3z87_1915;
							}
						}
					else
						{	/* SawMill/blockorder.scm 26 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* rdfs-ordering */
	obj_t BGl_rdfszd2orderingzd2zzsaw_blockorderz00(BgL_blockz00_bglt BgL_bz00_60)
	{
		{	/* SawMill/blockorder.scm 33 */
			{	/* SawMill/blockorder.scm 34 */
				struct bgl_cell BgL_box2015_3129z00;
				obj_t BgL_rz00_3129;

				BgL_rz00_3129 = MAKE_CELL_STACK(BNIL, BgL_box2015_3129z00);
				{	/* SawMill/blockorder.scm 42 */
					obj_t BgL_g1476z00_1924;

					BgL_g1476z00_1924 =
						BGl_findzd2exitzd2zzsaw_blockorderz00(BgL_bz00_60);
					{
						obj_t BgL_l1474z00_1926;

						BgL_l1474z00_1926 = BgL_g1476z00_1924;
					BgL_zc3z04anonymousza31560ze3z87_1927:
						if (PAIRP(BgL_l1474z00_1926))
							{	/* SawMill/blockorder.scm 42 */
								{	/* SawMill/blockorder.scm 42 */
									obj_t BgL_sz00_1929;

									BgL_sz00_1929 = CAR(BgL_l1474z00_1926);
									{	/* SawMill/blockorder.scm 42 */
										bool_t BgL_test2017z00_3320;

										{	/* SawMill/blockorder.scm 42 */
											obj_t BgL_classz00_2639;

											BgL_classz00_2639 = BGl_SawRdonez00zzsaw_blockorderz00;
											if (BGL_OBJECTP(BgL_sz00_1929))
												{	/* SawMill/blockorder.scm 42 */
													obj_t BgL_oclassz00_2641;

													{	/* SawMill/blockorder.scm 42 */
														obj_t BgL_arg1815z00_2643;
														long BgL_arg1816z00_2644;

														BgL_arg1815z00_2643 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawMill/blockorder.scm 42 */
															long BgL_arg1817z00_2645;

															BgL_arg1817z00_2645 =
																BGL_OBJECT_CLASS_NUM(
																((BgL_objectz00_bglt) BgL_sz00_1929));
															BgL_arg1816z00_2644 =
																(BgL_arg1817z00_2645 - OBJECT_TYPE);
														}
														BgL_oclassz00_2641 =
															VECTOR_REF(BgL_arg1815z00_2643,
															BgL_arg1816z00_2644);
													}
													BgL_test2017z00_3320 =
														(BgL_oclassz00_2641 == BgL_classz00_2639);
												}
											else
												{	/* SawMill/blockorder.scm 42 */
													BgL_test2017z00_3320 = ((bool_t) 0);
												}
										}
										if (BgL_test2017z00_3320)
											{	/* SawMill/blockorder.scm 42 */
												((bool_t) 0);
											}
										else
											{	/* SawMill/blockorder.scm 42 */
												BGl_visitze70ze7zzsaw_blockorderz00(BgL_rz00_3129,
													((BgL_blockz00_bglt) BgL_sz00_1929));
											}
									}
								}
								{
									obj_t BgL_l1474z00_3331;

									BgL_l1474z00_3331 = CDR(BgL_l1474z00_1926);
									BgL_l1474z00_1926 = BgL_l1474z00_3331;
									goto BgL_zc3z04anonymousza31560ze3z87_1927;
								}
							}
						else
							{	/* SawMill/blockorder.scm 42 */
								((bool_t) 1);
							}
					}
				}
				BGl_dfsze71ze7zzsaw_blockorderz00(BgL_rz00_3129, ((obj_t) BgL_bz00_60));
				{
					obj_t BgL_lz00_1955;
					obj_t BgL_prevz00_1956;
					obj_t BgL_lastfailz00_1957;

					BgL_lz00_1955 = ((obj_t) ((obj_t) CELL_REF(BgL_rz00_3129)));
					BgL_prevz00_1956 = BNIL;
					BgL_lastfailz00_1957 = BNIL;
				BgL_zc3z04anonymousza31576ze3z87_1958:
					{	/* SawMill/blockorder.scm 50 */
						obj_t BgL_xz00_1959;

						BgL_xz00_1959 = CAR(((obj_t) BgL_lz00_1955));
						if ((BgL_xz00_1959 == ((obj_t) BgL_bz00_60)))
							{	/* SawMill/blockorder.scm 51 */
								if (NULLP(BgL_prevz00_1956))
									{	/* SawMill/blockorder.scm 52 */
										BFALSE;
									}
								else
									{	/* SawMill/blockorder.scm 53 */
										obj_t BgL_endz00_1961;

										BgL_endz00_1961 =
											BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
											(BgL_lz00_1955);
										{	/* SawMill/blockorder.scm 54 */
											bool_t BgL_test2021z00_3343;

											if (NULLP(BgL_lastfailz00_1957))
												{	/* SawMill/blockorder.scm 54 */
													BgL_test2021z00_3343 = ((bool_t) 1);
												}
											else
												{	/* SawMill/blockorder.scm 54 */
													BgL_test2021z00_3343 =
														(BgL_prevz00_1956 == BgL_lastfailz00_1957);
												}
											if (BgL_test2021z00_3343)
												{	/* SawMill/blockorder.scm 54 */
													{	/* SawMill/blockorder.scm 55 */
														obj_t BgL_objz00_2685;

														BgL_objz00_2685 =
															((obj_t) ((obj_t) CELL_REF(BgL_rz00_3129)));
														SET_CDR(BgL_endz00_1961, BgL_objz00_2685);
													}
													{	/* SawMill/blockorder.scm 56 */
														obj_t BgL_tmpz00_3349;

														BgL_tmpz00_3349 = ((obj_t) BgL_prevz00_1956);
														SET_CDR(BgL_tmpz00_3349, BNIL);
													}
												}
											else
												{	/* SawMill/blockorder.scm 54 */
													{	/* SawMill/blockorder.scm 57 */
														obj_t BgL_arg1584z00_1964;

														BgL_arg1584z00_1964 =
															CDR(((obj_t) BgL_lastfailz00_1957));
														SET_CDR(BgL_endz00_1961, BgL_arg1584z00_1964);
													}
													{	/* SawMill/blockorder.scm 58 */
														obj_t BgL_objz00_2690;

														BgL_objz00_2690 =
															((obj_t) ((obj_t) CELL_REF(BgL_rz00_3129)));
														{	/* SawMill/blockorder.scm 58 */
															obj_t BgL_tmpz00_3356;

															BgL_tmpz00_3356 = ((obj_t) BgL_prevz00_1956);
															SET_CDR(BgL_tmpz00_3356, BgL_objz00_2690);
														}
													}
													{	/* SawMill/blockorder.scm 59 */
														obj_t BgL_tmpz00_3359;

														BgL_tmpz00_3359 = ((obj_t) BgL_lastfailz00_1957);
														SET_CDR(BgL_tmpz00_3359, BNIL);
													}
												}
										}
									}
								return BgL_lz00_1955;
							}
						else
							{	/* SawMill/blockorder.scm 61 */
								BgL_rtl_funz00_bglt BgL_funz00_1966;

								BgL_funz00_1966 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt)
												CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
														(((BgL_blockz00_bglt) COBJECT(
																	((BgL_blockz00_bglt) BgL_xz00_1959)))->
															BgL_firstz00))))))->BgL_funz00);
								{	/* SawMill/blockorder.scm 62 */
									bool_t BgL_test2023z00_3368;

									{	/* SawMill/blockorder.scm 62 */
										bool_t BgL_test2024z00_3369;

										{	/* SawMill/blockorder.scm 62 */
											obj_t BgL_classz00_2695;

											BgL_classz00_2695 = BGl_rtl_lastz00zzsaw_defsz00;
											{	/* SawMill/blockorder.scm 62 */
												BgL_objectz00_bglt BgL_arg1807z00_2697;

												{	/* SawMill/blockorder.scm 62 */
													obj_t BgL_tmpz00_3370;

													BgL_tmpz00_3370 =
														((obj_t) ((BgL_objectz00_bglt) BgL_funz00_1966));
													BgL_arg1807z00_2697 =
														(BgL_objectz00_bglt) (BgL_tmpz00_3370);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawMill/blockorder.scm 62 */
														long BgL_idxz00_2703;

														BgL_idxz00_2703 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2697);
														BgL_test2024z00_3369 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2703 + 2L)) == BgL_classz00_2695);
													}
												else
													{	/* SawMill/blockorder.scm 62 */
														bool_t BgL_res1966z00_2728;

														{	/* SawMill/blockorder.scm 62 */
															obj_t BgL_oclassz00_2711;

															{	/* SawMill/blockorder.scm 62 */
																obj_t BgL_arg1815z00_2719;
																long BgL_arg1816z00_2720;

																BgL_arg1815z00_2719 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawMill/blockorder.scm 62 */
																	long BgL_arg1817z00_2721;

																	BgL_arg1817z00_2721 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2697);
																	BgL_arg1816z00_2720 =
																		(BgL_arg1817z00_2721 - OBJECT_TYPE);
																}
																BgL_oclassz00_2711 =
																	VECTOR_REF(BgL_arg1815z00_2719,
																	BgL_arg1816z00_2720);
															}
															{	/* SawMill/blockorder.scm 62 */
																bool_t BgL__ortest_1115z00_2712;

																BgL__ortest_1115z00_2712 =
																	(BgL_classz00_2695 == BgL_oclassz00_2711);
																if (BgL__ortest_1115z00_2712)
																	{	/* SawMill/blockorder.scm 62 */
																		BgL_res1966z00_2728 =
																			BgL__ortest_1115z00_2712;
																	}
																else
																	{	/* SawMill/blockorder.scm 62 */
																		long BgL_odepthz00_2713;

																		{	/* SawMill/blockorder.scm 62 */
																			obj_t BgL_arg1804z00_2714;

																			BgL_arg1804z00_2714 =
																				(BgL_oclassz00_2711);
																			BgL_odepthz00_2713 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2714);
																		}
																		if ((2L < BgL_odepthz00_2713))
																			{	/* SawMill/blockorder.scm 62 */
																				obj_t BgL_arg1802z00_2716;

																				{	/* SawMill/blockorder.scm 62 */
																					obj_t BgL_arg1803z00_2717;

																					BgL_arg1803z00_2717 =
																						(BgL_oclassz00_2711);
																					BgL_arg1802z00_2716 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2717, 2L);
																				}
																				BgL_res1966z00_2728 =
																					(BgL_arg1802z00_2716 ==
																					BgL_classz00_2695);
																			}
																		else
																			{	/* SawMill/blockorder.scm 62 */
																				BgL_res1966z00_2728 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2024z00_3369 = BgL_res1966z00_2728;
													}
											}
										}
										if (BgL_test2024z00_3369)
											{	/* SawMill/blockorder.scm 62 */
												obj_t BgL_classz00_2729;

												BgL_classz00_2729 = BGl_rtl_returnz00zzsaw_defsz00;
												{	/* SawMill/blockorder.scm 62 */
													BgL_objectz00_bglt BgL_arg1807z00_2731;

													{	/* SawMill/blockorder.scm 62 */
														obj_t BgL_tmpz00_3393;

														BgL_tmpz00_3393 =
															((obj_t) ((BgL_objectz00_bglt) BgL_funz00_1966));
														BgL_arg1807z00_2731 =
															(BgL_objectz00_bglt) (BgL_tmpz00_3393);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* SawMill/blockorder.scm 62 */
															long BgL_idxz00_2737;

															BgL_idxz00_2737 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2731);
															BgL_test2023z00_3368 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2737 + 3L)) == BgL_classz00_2729);
														}
													else
														{	/* SawMill/blockorder.scm 62 */
															bool_t BgL_res1967z00_2762;

															{	/* SawMill/blockorder.scm 62 */
																obj_t BgL_oclassz00_2745;

																{	/* SawMill/blockorder.scm 62 */
																	obj_t BgL_arg1815z00_2753;
																	long BgL_arg1816z00_2754;

																	BgL_arg1815z00_2753 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* SawMill/blockorder.scm 62 */
																		long BgL_arg1817z00_2755;

																		BgL_arg1817z00_2755 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2731);
																		BgL_arg1816z00_2754 =
																			(BgL_arg1817z00_2755 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2745 =
																		VECTOR_REF(BgL_arg1815z00_2753,
																		BgL_arg1816z00_2754);
																}
																{	/* SawMill/blockorder.scm 62 */
																	bool_t BgL__ortest_1115z00_2746;

																	BgL__ortest_1115z00_2746 =
																		(BgL_classz00_2729 == BgL_oclassz00_2745);
																	if (BgL__ortest_1115z00_2746)
																		{	/* SawMill/blockorder.scm 62 */
																			BgL_res1967z00_2762 =
																				BgL__ortest_1115z00_2746;
																		}
																	else
																		{	/* SawMill/blockorder.scm 62 */
																			long BgL_odepthz00_2747;

																			{	/* SawMill/blockorder.scm 62 */
																				obj_t BgL_arg1804z00_2748;

																				BgL_arg1804z00_2748 =
																					(BgL_oclassz00_2745);
																				BgL_odepthz00_2747 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2748);
																			}
																			if ((3L < BgL_odepthz00_2747))
																				{	/* SawMill/blockorder.scm 62 */
																					obj_t BgL_arg1802z00_2750;

																					{	/* SawMill/blockorder.scm 62 */
																						obj_t BgL_arg1803z00_2751;

																						BgL_arg1803z00_2751 =
																							(BgL_oclassz00_2745);
																						BgL_arg1802z00_2750 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2751, 3L);
																					}
																					BgL_res1967z00_2762 =
																						(BgL_arg1802z00_2750 ==
																						BgL_classz00_2729);
																				}
																			else
																				{	/* SawMill/blockorder.scm 62 */
																					BgL_res1967z00_2762 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2023z00_3368 = BgL_res1967z00_2762;
														}
												}
											}
										else
											{	/* SawMill/blockorder.scm 62 */
												BgL_test2023z00_3368 = ((bool_t) 1);
											}
									}
									if (BgL_test2023z00_3368)
										{	/* SawMill/blockorder.scm 63 */
											obj_t BgL_arg1589z00_1969;

											BgL_arg1589z00_1969 = CDR(((obj_t) BgL_lz00_1955));
											{
												obj_t BgL_prevz00_3419;
												obj_t BgL_lz00_3418;

												BgL_lz00_3418 = BgL_arg1589z00_1969;
												BgL_prevz00_3419 = BgL_lz00_1955;
												BgL_prevz00_1956 = BgL_prevz00_3419;
												BgL_lz00_1955 = BgL_lz00_3418;
												goto BgL_zc3z04anonymousza31576ze3z87_1958;
											}
										}
									else
										{	/* SawMill/blockorder.scm 64 */
											obj_t BgL_arg1591z00_1970;

											BgL_arg1591z00_1970 = CDR(((obj_t) BgL_lz00_1955));
											{
												obj_t BgL_lastfailz00_3424;
												obj_t BgL_prevz00_3423;
												obj_t BgL_lz00_3422;

												BgL_lz00_3422 = BgL_arg1591z00_1970;
												BgL_prevz00_3423 = BgL_lz00_1955;
												BgL_lastfailz00_3424 = BgL_lz00_1955;
												BgL_lastfailz00_1957 = BgL_lastfailz00_3424;
												BgL_prevz00_1956 = BgL_prevz00_3423;
												BgL_lz00_1955 = BgL_lz00_3422;
												goto BgL_zc3z04anonymousza31576ze3z87_1958;
											}
										}
								}
							}
					}
				}
			}
		}

	}



/* visit~0 */
	bool_t BGl_visitze70ze7zzsaw_blockorderz00(obj_t BgL_rz00_3125,
		BgL_blockz00_bglt BgL_bz00_1976)
	{
		{	/* SawMill/blockorder.scm 36 */
			{	/* SawMill/blockorder.scm 36 */
				BgL_sawrdonez00_bglt BgL_wide1224z00_1980;

				BgL_wide1224z00_1980 =
					((BgL_sawrdonez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sawrdonez00_bgl))));
				{	/* SawMill/blockorder.scm 36 */
					obj_t BgL_auxz00_3430;
					BgL_objectz00_bglt BgL_tmpz00_3427;

					BgL_auxz00_3430 = ((obj_t) BgL_wide1224z00_1980);
					BgL_tmpz00_3427 =
						((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1976));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3427, BgL_auxz00_3430);
				}
				((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1976));
				{	/* SawMill/blockorder.scm 36 */
					long BgL_arg1602z00_1981;

					BgL_arg1602z00_1981 =
						BGL_CLASS_NUM(BGl_SawRdonez00zzsaw_blockorderz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt) BgL_bz00_1976)), BgL_arg1602z00_1981);
				}
				((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1976));
			}
			((BgL_blockz00_bglt) BgL_bz00_1976);
			{	/* SawMill/blockorder.scm 37 */
				obj_t BgL_auxz00_3126;

				BgL_auxz00_3126 =
					MAKE_YOUNG_PAIR(
					((obj_t) BgL_bz00_1976), ((obj_t) ((obj_t) CELL_REF(BgL_rz00_3125))));
				CELL_SET(BgL_rz00_3125, BgL_auxz00_3126);
			}
			{	/* SawMill/blockorder.scm 39 */
				obj_t BgL_g1473z00_1983;

				BgL_g1473z00_1983 =
					BGl_predecessorsz00zzsaw_blockorderz00(BgL_bz00_1976);
				{
					obj_t BgL_l1471z00_1985;

					BgL_l1471z00_1985 = BgL_g1473z00_1983;
				BgL_zc3z04anonymousza31603ze3z87_1986:
					if (PAIRP(BgL_l1471z00_1985))
						{	/* SawMill/blockorder.scm 40 */
							{	/* SawMill/blockorder.scm 39 */
								obj_t BgL_sz00_1988;

								BgL_sz00_1988 = CAR(BgL_l1471z00_1985);
								{	/* SawMill/blockorder.scm 39 */
									bool_t BgL_test2032z00_3449;

									{	/* SawMill/blockorder.scm 39 */
										obj_t BgL_classz00_2625;

										BgL_classz00_2625 = BGl_SawRdonez00zzsaw_blockorderz00;
										if (BGL_OBJECTP(BgL_sz00_1988))
											{	/* SawMill/blockorder.scm 39 */
												obj_t BgL_oclassz00_2627;

												{	/* SawMill/blockorder.scm 39 */
													obj_t BgL_arg1815z00_2629;
													long BgL_arg1816z00_2630;

													BgL_arg1815z00_2629 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/blockorder.scm 39 */
														long BgL_arg1817z00_2631;

														BgL_arg1817z00_2631 =
															BGL_OBJECT_CLASS_NUM(
															((BgL_objectz00_bglt) BgL_sz00_1988));
														BgL_arg1816z00_2630 =
															(BgL_arg1817z00_2631 - OBJECT_TYPE);
													}
													BgL_oclassz00_2627 =
														VECTOR_REF(BgL_arg1815z00_2629,
														BgL_arg1816z00_2630);
												}
												BgL_test2032z00_3449 =
													(BgL_oclassz00_2627 == BgL_classz00_2625);
											}
										else
											{	/* SawMill/blockorder.scm 39 */
												BgL_test2032z00_3449 = ((bool_t) 0);
											}
									}
									if (BgL_test2032z00_3449)
										{	/* SawMill/blockorder.scm 39 */
											((bool_t) 0);
										}
									else
										{	/* SawMill/blockorder.scm 39 */
											BGl_visitze70ze7zzsaw_blockorderz00(BgL_rz00_3125,
												((BgL_blockz00_bglt) BgL_sz00_1988));
										}
								}
							}
							{
								obj_t BgL_l1471z00_3460;

								BgL_l1471z00_3460 = CDR(BgL_l1471z00_1985);
								BgL_l1471z00_1985 = BgL_l1471z00_3460;
								goto BgL_zc3z04anonymousza31603ze3z87_1986;
							}
						}
					else
						{	/* SawMill/blockorder.scm 40 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* dfs~1 */
	bool_t BGl_dfsze71ze7zzsaw_blockorderz00(obj_t BgL_rz00_3127,
		obj_t BgL_bz00_1934)
	{
		{	/* SawMill/blockorder.scm 44 */
			{	/* SawMill/blockorder.scm 45 */
				bool_t BgL_test2034z00_3462;

				{	/* SawMill/blockorder.scm 45 */
					obj_t BgL_classz00_2652;

					BgL_classz00_2652 = BGl_SawRdonez00zzsaw_blockorderz00;
					if (BGL_OBJECTP(BgL_bz00_1934))
						{	/* SawMill/blockorder.scm 45 */
							obj_t BgL_oclassz00_2654;

							{	/* SawMill/blockorder.scm 45 */
								obj_t BgL_arg1815z00_2656;
								long BgL_arg1816z00_2657;

								BgL_arg1815z00_2656 = (BGl_za2classesza2z00zz__objectz00);
								{	/* SawMill/blockorder.scm 45 */
									long BgL_arg1817z00_2658;

									BgL_arg1817z00_2658 =
										BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_bz00_1934));
									BgL_arg1816z00_2657 = (BgL_arg1817z00_2658 - OBJECT_TYPE);
								}
								BgL_oclassz00_2654 =
									VECTOR_REF(BgL_arg1815z00_2656, BgL_arg1816z00_2657);
							}
							BgL_test2034z00_3462 = (BgL_oclassz00_2654 == BgL_classz00_2652);
						}
					else
						{	/* SawMill/blockorder.scm 45 */
							BgL_test2034z00_3462 = ((bool_t) 0);
						}
				}
				if (BgL_test2034z00_3462)
					{	/* SawMill/blockorder.scm 45 */
						BFALSE;
					}
				else
					{	/* SawMill/blockorder.scm 45 */
						obj_t BgL_auxz00_3128;

						BgL_auxz00_3128 =
							MAKE_YOUNG_PAIR(BgL_bz00_1934,
							((obj_t) ((obj_t) CELL_REF(BgL_rz00_3127))));
						CELL_SET(BgL_rz00_3127, BgL_auxz00_3128);
					}
			}
			{	/* SawMill/blockorder.scm 46 */
				BgL_sawdonez00_bglt BgL_wide1228z00_1939;

				BgL_wide1228z00_1939 =
					((BgL_sawdonez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sawdonez00_bgl))));
				{	/* SawMill/blockorder.scm 46 */
					obj_t BgL_auxz00_3478;
					BgL_objectz00_bglt BgL_tmpz00_3474;

					BgL_auxz00_3478 = ((obj_t) BgL_wide1228z00_1939);
					BgL_tmpz00_3474 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1934)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3474, BgL_auxz00_3478);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1934)));
				{	/* SawMill/blockorder.scm 46 */
					long BgL_arg1571z00_1940;

					BgL_arg1571z00_1940 =
						BGL_CLASS_NUM(BGl_SawDonez00zzsaw_blockorderz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_bz00_1934))), BgL_arg1571z00_1940);
				}
				((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1934)));
			}
			((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1934));
			{	/* SawMill/blockorder.scm 47 */
				obj_t BgL_g1479z00_1942;

				BgL_g1479z00_1942 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_bz00_1934)))->BgL_succsz00);
				{
					obj_t BgL_l1477z00_1944;

					BgL_l1477z00_1944 = BgL_g1479z00_1942;
				BgL_zc3z04anonymousza31572ze3z87_1945:
					if (PAIRP(BgL_l1477z00_1944))
						{	/* SawMill/blockorder.scm 47 */
							{	/* SawMill/blockorder.scm 47 */
								obj_t BgL_sz00_1947;

								BgL_sz00_1947 = CAR(BgL_l1477z00_1944);
								{	/* SawMill/blockorder.scm 47 */
									bool_t BgL_test2037z00_3499;

									{	/* SawMill/blockorder.scm 47 */
										obj_t BgL_classz00_2670;

										BgL_classz00_2670 = BGl_SawDonez00zzsaw_blockorderz00;
										if (BGL_OBJECTP(BgL_sz00_1947))
											{	/* SawMill/blockorder.scm 47 */
												obj_t BgL_oclassz00_2672;

												{	/* SawMill/blockorder.scm 47 */
													obj_t BgL_arg1815z00_2674;
													long BgL_arg1816z00_2675;

													BgL_arg1815z00_2674 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/blockorder.scm 47 */
														long BgL_arg1817z00_2676;

														BgL_arg1817z00_2676 =
															BGL_OBJECT_CLASS_NUM(
															((BgL_objectz00_bglt) BgL_sz00_1947));
														BgL_arg1816z00_2675 =
															(BgL_arg1817z00_2676 - OBJECT_TYPE);
													}
													BgL_oclassz00_2672 =
														VECTOR_REF(BgL_arg1815z00_2674,
														BgL_arg1816z00_2675);
												}
												BgL_test2037z00_3499 =
													(BgL_oclassz00_2672 == BgL_classz00_2670);
											}
										else
											{	/* SawMill/blockorder.scm 47 */
												BgL_test2037z00_3499 = ((bool_t) 0);
											}
									}
									if (BgL_test2037z00_3499)
										{	/* SawMill/blockorder.scm 47 */
											((bool_t) 0);
										}
									else
										{	/* SawMill/blockorder.scm 47 */
											BGl_dfsze71ze7zzsaw_blockorderz00(BgL_rz00_3127,
												BgL_sz00_1947);
										}
								}
							}
							{
								obj_t BgL_l1477z00_3509;

								BgL_l1477z00_3509 = CDR(BgL_l1477z00_1944);
								BgL_l1477z00_1944 = BgL_l1477z00_3509;
								goto BgL_zc3z04anonymousza31572ze3z87_1945;
							}
						}
					else
						{	/* SawMill/blockorder.scm 47 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* find-exit */
	obj_t BGl_findzd2exitzd2zzsaw_blockorderz00(BgL_blockz00_bglt BgL_bz00_61)
	{
		{	/* SawMill/blockorder.scm 73 */
			{	/* SawMill/blockorder.scm 74 */
				struct bgl_cell BgL_box2039_3119z00;
				struct bgl_cell BgL_box2040_3120z00;
				struct bgl_cell BgL_box2041_3121z00;
				obj_t BgL_retsz00_3119;
				obj_t BgL_failsz00_3120;
				obj_t BgL_nz00_3121;

				BgL_retsz00_3119 = MAKE_CELL_STACK(BNIL, BgL_box2039_3119z00);
				BgL_failsz00_3120 = MAKE_CELL_STACK(BNIL, BgL_box2040_3120z00);
				BgL_nz00_3121 = MAKE_CELL_STACK(BINT(0L), BgL_box2041_3121z00);
				BGl_dfsze70ze7zzsaw_blockorderz00(BgL_retsz00_3119, BgL_failsz00_3120,
					BgL_nz00_3121, ((obj_t) BgL_bz00_61));
				return BGl_appendzd221011zd2zzsaw_blockorderz00(((obj_t) ((obj_t)
							CELL_REF(BgL_retsz00_3119))),
					((obj_t) ((obj_t) CELL_REF(BgL_failsz00_3120))));
			}
		}

	}



/* dfs~0 */
	bool_t BGl_dfsze70ze7zzsaw_blockorderz00(obj_t BgL_retsz00_3115,
		obj_t BgL_failsz00_3114, obj_t BgL_nz00_3113, obj_t BgL_bz00_1997)
	{
		{	/* SawMill/blockorder.scm 75 */
			{	/* SawMill/blockorder.scm 76 */
				BgL_sawdfsz00_bglt BgL_wide1235z00_2001;

				BgL_wide1235z00_2001 =
					((BgL_sawdfsz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sawdfsz00_bgl))));
				{	/* SawMill/blockorder.scm 76 */
					obj_t BgL_auxz00_3522;
					BgL_objectz00_bglt BgL_tmpz00_3518;

					BgL_auxz00_3522 = ((obj_t) BgL_wide1235z00_2001);
					BgL_tmpz00_3518 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1997)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3518, BgL_auxz00_3522);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1997)));
				{	/* SawMill/blockorder.scm 76 */
					long BgL_arg1609z00_2002;

					BgL_arg1609z00_2002 = BGL_CLASS_NUM(BGl_SawDfsz00zzsaw_blockorderz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_bz00_1997))), BgL_arg1609z00_2002);
				}
				((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1997)));
			}
			{
				BgL_sawdfsz00_bglt BgL_auxz00_3536;

				{
					obj_t BgL_auxz00_3537;

					{	/* SawMill/blockorder.scm 76 */
						BgL_objectz00_bglt BgL_tmpz00_3538;

						BgL_tmpz00_3538 =
							((BgL_objectz00_bglt)
							((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1997)));
						BgL_auxz00_3537 = BGL_OBJECT_WIDENING(BgL_tmpz00_3538);
					}
					BgL_auxz00_3536 = ((BgL_sawdfsz00_bglt) BgL_auxz00_3537);
				}
				((((BgL_sawdfsz00_bglt) COBJECT(BgL_auxz00_3536))->BgL_nz00) = ((int)
						CINT(((obj_t) ((obj_t) CELL_REF(BgL_nz00_3113))))), BUNSPEC);
			}
			((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1997));
			{	/* SawMill/blockorder.scm 77 */
				obj_t BgL_auxz00_3116;

				BgL_auxz00_3116 =
					ADDFX(((obj_t) ((obj_t) CELL_REF(BgL_nz00_3113))), BINT(1L));
				CELL_SET(BgL_nz00_3113, BgL_auxz00_3116);
			}
			{	/* SawMill/blockorder.scm 80 */
				BgL_rtl_funz00_bglt BgL_funz00_2004;

				BgL_funz00_2004 =
					(((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt)
								CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
										(((BgL_blockz00_bglt) COBJECT(
													((BgL_blockz00_bglt) BgL_bz00_1997)))->
											BgL_firstz00))))))->BgL_funz00);
				{	/* SawMill/blockorder.scm 81 */
					bool_t BgL_test2042z00_3558;

					{	/* SawMill/blockorder.scm 81 */
						obj_t BgL_classz00_2774;

						BgL_classz00_2774 = BGl_rtl_lastz00zzsaw_defsz00;
						{	/* SawMill/blockorder.scm 81 */
							BgL_objectz00_bglt BgL_arg1807z00_2776;

							{	/* SawMill/blockorder.scm 81 */
								obj_t BgL_tmpz00_3559;

								BgL_tmpz00_3559 =
									((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2004));
								BgL_arg1807z00_2776 = (BgL_objectz00_bglt) (BgL_tmpz00_3559);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawMill/blockorder.scm 81 */
									long BgL_idxz00_2782;

									BgL_idxz00_2782 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2776);
									BgL_test2042z00_3558 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2782 + 2L)) == BgL_classz00_2774);
								}
							else
								{	/* SawMill/blockorder.scm 81 */
									bool_t BgL_res1968z00_2807;

									{	/* SawMill/blockorder.scm 81 */
										obj_t BgL_oclassz00_2790;

										{	/* SawMill/blockorder.scm 81 */
											obj_t BgL_arg1815z00_2798;
											long BgL_arg1816z00_2799;

											BgL_arg1815z00_2798 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawMill/blockorder.scm 81 */
												long BgL_arg1817z00_2800;

												BgL_arg1817z00_2800 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2776);
												BgL_arg1816z00_2799 =
													(BgL_arg1817z00_2800 - OBJECT_TYPE);
											}
											BgL_oclassz00_2790 =
												VECTOR_REF(BgL_arg1815z00_2798, BgL_arg1816z00_2799);
										}
										{	/* SawMill/blockorder.scm 81 */
											bool_t BgL__ortest_1115z00_2791;

											BgL__ortest_1115z00_2791 =
												(BgL_classz00_2774 == BgL_oclassz00_2790);
											if (BgL__ortest_1115z00_2791)
												{	/* SawMill/blockorder.scm 81 */
													BgL_res1968z00_2807 = BgL__ortest_1115z00_2791;
												}
											else
												{	/* SawMill/blockorder.scm 81 */
													long BgL_odepthz00_2792;

													{	/* SawMill/blockorder.scm 81 */
														obj_t BgL_arg1804z00_2793;

														BgL_arg1804z00_2793 = (BgL_oclassz00_2790);
														BgL_odepthz00_2792 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2793);
													}
													if ((2L < BgL_odepthz00_2792))
														{	/* SawMill/blockorder.scm 81 */
															obj_t BgL_arg1802z00_2795;

															{	/* SawMill/blockorder.scm 81 */
																obj_t BgL_arg1803z00_2796;

																BgL_arg1803z00_2796 = (BgL_oclassz00_2790);
																BgL_arg1802z00_2795 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2796,
																	2L);
															}
															BgL_res1968z00_2807 =
																(BgL_arg1802z00_2795 == BgL_classz00_2774);
														}
													else
														{	/* SawMill/blockorder.scm 81 */
															BgL_res1968z00_2807 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2042z00_3558 = BgL_res1968z00_2807;
								}
						}
					}
					if (BgL_test2042z00_3558)
						{	/* SawMill/blockorder.scm 82 */
							bool_t BgL_test2046z00_3582;

							{	/* SawMill/blockorder.scm 82 */
								obj_t BgL_classz00_2808;

								BgL_classz00_2808 = BGl_rtl_returnz00zzsaw_defsz00;
								{	/* SawMill/blockorder.scm 82 */
									BgL_objectz00_bglt BgL_arg1807z00_2810;

									{	/* SawMill/blockorder.scm 82 */
										obj_t BgL_tmpz00_3583;

										BgL_tmpz00_3583 =
											((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2004));
										BgL_arg1807z00_2810 =
											(BgL_objectz00_bglt) (BgL_tmpz00_3583);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawMill/blockorder.scm 82 */
											long BgL_idxz00_2816;

											BgL_idxz00_2816 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2810);
											BgL_test2046z00_3582 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2816 + 3L)) == BgL_classz00_2808);
										}
									else
										{	/* SawMill/blockorder.scm 82 */
											bool_t BgL_res1969z00_2841;

											{	/* SawMill/blockorder.scm 82 */
												obj_t BgL_oclassz00_2824;

												{	/* SawMill/blockorder.scm 82 */
													obj_t BgL_arg1815z00_2832;
													long BgL_arg1816z00_2833;

													BgL_arg1815z00_2832 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/blockorder.scm 82 */
														long BgL_arg1817z00_2834;

														BgL_arg1817z00_2834 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2810);
														BgL_arg1816z00_2833 =
															(BgL_arg1817z00_2834 - OBJECT_TYPE);
													}
													BgL_oclassz00_2824 =
														VECTOR_REF(BgL_arg1815z00_2832,
														BgL_arg1816z00_2833);
												}
												{	/* SawMill/blockorder.scm 82 */
													bool_t BgL__ortest_1115z00_2825;

													BgL__ortest_1115z00_2825 =
														(BgL_classz00_2808 == BgL_oclassz00_2824);
													if (BgL__ortest_1115z00_2825)
														{	/* SawMill/blockorder.scm 82 */
															BgL_res1969z00_2841 = BgL__ortest_1115z00_2825;
														}
													else
														{	/* SawMill/blockorder.scm 82 */
															long BgL_odepthz00_2826;

															{	/* SawMill/blockorder.scm 82 */
																obj_t BgL_arg1804z00_2827;

																BgL_arg1804z00_2827 = (BgL_oclassz00_2824);
																BgL_odepthz00_2826 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2827);
															}
															if ((3L < BgL_odepthz00_2826))
																{	/* SawMill/blockorder.scm 82 */
																	obj_t BgL_arg1802z00_2829;

																	{	/* SawMill/blockorder.scm 82 */
																		obj_t BgL_arg1803z00_2830;

																		BgL_arg1803z00_2830 = (BgL_oclassz00_2824);
																		BgL_arg1802z00_2829 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2830, 3L);
																	}
																	BgL_res1969z00_2841 =
																		(BgL_arg1802z00_2829 == BgL_classz00_2808);
																}
															else
																{	/* SawMill/blockorder.scm 82 */
																	BgL_res1969z00_2841 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2046z00_3582 = BgL_res1969z00_2841;
										}
								}
							}
							if (BgL_test2046z00_3582)
								{	/* SawMill/blockorder.scm 83 */
									obj_t BgL_auxz00_3117;

									BgL_auxz00_3117 =
										MAKE_YOUNG_PAIR(BgL_bz00_1997,
										((obj_t) ((obj_t) CELL_REF(BgL_retsz00_3115))));
									CELL_SET(BgL_retsz00_3115, BgL_auxz00_3117);
								}
							else
								{	/* SawMill/blockorder.scm 84 */
									obj_t BgL_auxz00_3118;

									BgL_auxz00_3118 =
										MAKE_YOUNG_PAIR(BgL_bz00_1997,
										((obj_t) ((obj_t) CELL_REF(BgL_failsz00_3114))));
									CELL_SET(BgL_failsz00_3114, BgL_auxz00_3118);
								}
						}
					else
						{	/* SawMill/blockorder.scm 81 */
							BFALSE;
						}
				}
			}
			{	/* SawMill/blockorder.scm 85 */
				obj_t BgL_g1482z00_2010;

				BgL_g1482z00_2010 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_bz00_1997)))->BgL_succsz00);
				{
					obj_t BgL_l1480z00_2012;

					BgL_l1480z00_2012 = BgL_g1482z00_2010;
				BgL_zc3z04anonymousza31617ze3z87_2013:
					if (PAIRP(BgL_l1480z00_2012))
						{	/* SawMill/blockorder.scm 85 */
							{	/* SawMill/blockorder.scm 85 */
								obj_t BgL_sz00_2015;

								BgL_sz00_2015 = CAR(BgL_l1480z00_2012);
								{	/* SawMill/blockorder.scm 85 */
									bool_t BgL_test2051z00_3615;

									{	/* SawMill/blockorder.scm 85 */
										obj_t BgL_classz00_2844;

										BgL_classz00_2844 = BGl_SawDfsz00zzsaw_blockorderz00;
										if (BGL_OBJECTP(BgL_sz00_2015))
											{	/* SawMill/blockorder.scm 85 */
												obj_t BgL_oclassz00_2846;

												{	/* SawMill/blockorder.scm 85 */
													obj_t BgL_arg1815z00_2848;
													long BgL_arg1816z00_2849;

													BgL_arg1815z00_2848 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/blockorder.scm 85 */
														long BgL_arg1817z00_2850;

														BgL_arg1817z00_2850 =
															BGL_OBJECT_CLASS_NUM(
															((BgL_objectz00_bglt) BgL_sz00_2015));
														BgL_arg1816z00_2849 =
															(BgL_arg1817z00_2850 - OBJECT_TYPE);
													}
													BgL_oclassz00_2846 =
														VECTOR_REF(BgL_arg1815z00_2848,
														BgL_arg1816z00_2849);
												}
												BgL_test2051z00_3615 =
													(BgL_oclassz00_2846 == BgL_classz00_2844);
											}
										else
											{	/* SawMill/blockorder.scm 85 */
												BgL_test2051z00_3615 = ((bool_t) 0);
											}
									}
									if (BgL_test2051z00_3615)
										{	/* SawMill/blockorder.scm 85 */
											((bool_t) 0);
										}
									else
										{	/* SawMill/blockorder.scm 85 */
											BGl_dfsze70ze7zzsaw_blockorderz00(BgL_retsz00_3115,
												BgL_failsz00_3114, BgL_nz00_3113, BgL_sz00_2015);
										}
								}
							}
							{
								obj_t BgL_l1480z00_3625;

								BgL_l1480z00_3625 = CDR(BgL_l1480z00_2012);
								BgL_l1480z00_2012 = BgL_l1480z00_3625;
								goto BgL_zc3z04anonymousza31617ze3z87_2013;
							}
						}
					else
						{	/* SawMill/blockorder.scm 85 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* predecessors */
	obj_t BGl_predecessorsz00zzsaw_blockorderz00(BgL_blockz00_bglt BgL_bz00_62)
	{
		{	/* SawMill/blockorder.scm 89 */
			{	/* SawMill/blockorder.scm 90 */
				obj_t BgL_arg1626z00_2020;

				{	/* SawMill/blockorder.scm 90 */
					obj_t BgL_hook1487z00_2021;

					BgL_hook1487z00_2021 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
					{	/* SawMill/blockorder.scm 90 */
						obj_t BgL_g1488z00_2022;

						BgL_g1488z00_2022 =
							(((BgL_blockz00_bglt) COBJECT(BgL_bz00_62))->BgL_predsz00);
						{
							obj_t BgL_l1484z00_2024;
							obj_t BgL_h1485z00_2025;

							BgL_l1484z00_2024 = BgL_g1488z00_2022;
							BgL_h1485z00_2025 = BgL_hook1487z00_2021;
						BgL_zc3z04anonymousza31627ze3z87_2026:
							if (NULLP(BgL_l1484z00_2024))
								{	/* SawMill/blockorder.scm 90 */
									BgL_arg1626z00_2020 = CDR(BgL_hook1487z00_2021);
								}
							else
								{	/* SawMill/blockorder.scm 90 */
									bool_t BgL_test2054z00_3632;

									{	/* SawMill/blockorder.scm 90 */
										obj_t BgL_xz00_2035;

										BgL_xz00_2035 = CAR(((obj_t) BgL_l1484z00_2024));
										{	/* SawMill/blockorder.scm 90 */
											bool_t BgL_test2055z00_3635;

											{	/* SawMill/blockorder.scm 90 */
												obj_t BgL_classz00_2860;

												BgL_classz00_2860 = BGl_SawRdonez00zzsaw_blockorderz00;
												if (BGL_OBJECTP(BgL_xz00_2035))
													{	/* SawMill/blockorder.scm 90 */
														obj_t BgL_oclassz00_2862;

														{	/* SawMill/blockorder.scm 90 */
															obj_t BgL_arg1815z00_2864;
															long BgL_arg1816z00_2865;

															BgL_arg1815z00_2864 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawMill/blockorder.scm 90 */
																long BgL_arg1817z00_2866;

																BgL_arg1817z00_2866 =
																	BGL_OBJECT_CLASS_NUM(
																	((BgL_objectz00_bglt) BgL_xz00_2035));
																BgL_arg1816z00_2865 =
																	(BgL_arg1817z00_2866 - OBJECT_TYPE);
															}
															BgL_oclassz00_2862 =
																VECTOR_REF(BgL_arg1815z00_2864,
																BgL_arg1816z00_2865);
														}
														BgL_test2055z00_3635 =
															(BgL_oclassz00_2862 == BgL_classz00_2860);
													}
												else
													{	/* SawMill/blockorder.scm 90 */
														BgL_test2055z00_3635 = ((bool_t) 0);
													}
											}
											if (BgL_test2055z00_3635)
												{	/* SawMill/blockorder.scm 90 */
													BgL_test2054z00_3632 = ((bool_t) 0);
												}
											else
												{	/* SawMill/blockorder.scm 90 */
													BgL_test2054z00_3632 = ((bool_t) 1);
												}
										}
									}
									if (BgL_test2054z00_3632)
										{	/* SawMill/blockorder.scm 90 */
											obj_t BgL_nh1486z00_2031;

											{	/* SawMill/blockorder.scm 90 */
												obj_t BgL_arg1646z00_2033;

												BgL_arg1646z00_2033 = CAR(((obj_t) BgL_l1484z00_2024));
												BgL_nh1486z00_2031 =
													MAKE_YOUNG_PAIR(BgL_arg1646z00_2033, BNIL);
											}
											SET_CDR(BgL_h1485z00_2025, BgL_nh1486z00_2031);
											{	/* SawMill/blockorder.scm 90 */
												obj_t BgL_arg1642z00_2032;

												BgL_arg1642z00_2032 = CDR(((obj_t) BgL_l1484z00_2024));
												{
													obj_t BgL_h1485z00_3651;
													obj_t BgL_l1484z00_3650;

													BgL_l1484z00_3650 = BgL_arg1642z00_2032;
													BgL_h1485z00_3651 = BgL_nh1486z00_2031;
													BgL_h1485z00_2025 = BgL_h1485z00_3651;
													BgL_l1484z00_2024 = BgL_l1484z00_3650;
													goto BgL_zc3z04anonymousza31627ze3z87_2026;
												}
											}
										}
									else
										{	/* SawMill/blockorder.scm 90 */
											obj_t BgL_arg1650z00_2034;

											BgL_arg1650z00_2034 = CDR(((obj_t) BgL_l1484z00_2024));
											{
												obj_t BgL_l1484z00_3654;

												BgL_l1484z00_3654 = BgL_arg1650z00_2034;
												BgL_l1484z00_2024 = BgL_l1484z00_3654;
												goto BgL_zc3z04anonymousza31627ze3z87_2026;
											}
										}
								}
						}
					}
				}
				return
					BGl_sortz00zz__r4_vectors_6_8z00(BgL_arg1626z00_2020,
					BGl_lengthuncolsucczc3zd3zf3zd2envz31zzsaw_blockorderz00);
			}
		}

	}



/* &lengthuncolsucc<=? */
	obj_t BGl_z62lengthuncolsucczc3zd3zf3z81zzsaw_blockorderz00(obj_t
		BgL_envz00_3057, obj_t BgL_b1z00_3058, obj_t BgL_b2z00_3059)
	{
		{	/* SawMill/blockorder.scm 93 */
			{	/* SawMill/blockorder.scm 94 */
				bool_t BgL_tmpz00_3656;

				{	/* SawMill/blockorder.scm 94 */
					long BgL_n1z00_3146;
					long BgL_n2z00_3147;

					BgL_n1z00_3146 =
						BGl_lengthuncolsuccz00zzsaw_blockorderz00(
						((BgL_blockz00_bglt) BgL_b1z00_3058));
					BgL_n2z00_3147 =
						BGl_lengthuncolsuccz00zzsaw_blockorderz00(
						((BgL_blockz00_bglt) BgL_b2z00_3059));
					if ((BgL_n1z00_3146 < BgL_n2z00_3147))
						{	/* SawMill/blockorder.scm 95 */
							BgL_tmpz00_3656 = ((bool_t) 1);
						}
					else
						{	/* SawMill/blockorder.scm 95 */
							if ((BgL_n1z00_3146 > BgL_n2z00_3147))
								{	/* SawMill/blockorder.scm 96 */
									BgL_tmpz00_3656 = ((bool_t) 0);
								}
							else
								{	/* SawMill/blockorder.scm 97 */
									int BgL_a1489z00_3148;

									{
										BgL_sawdfsz00_bglt BgL_auxz00_3665;

										{
											obj_t BgL_auxz00_3666;

											{	/* SawMill/blockorder.scm 97 */
												BgL_objectz00_bglt BgL_tmpz00_3667;

												BgL_tmpz00_3667 =
													((BgL_objectz00_bglt)
													((BgL_blockz00_bglt) BgL_b1z00_3058));
												BgL_auxz00_3666 = BGL_OBJECT_WIDENING(BgL_tmpz00_3667);
											}
											BgL_auxz00_3665 = ((BgL_sawdfsz00_bglt) BgL_auxz00_3666);
										}
										BgL_a1489z00_3148 =
											(((BgL_sawdfsz00_bglt) COBJECT(BgL_auxz00_3665))->
											BgL_nz00);
									}
									{	/* SawMill/blockorder.scm 97 */
										int BgL_b1490z00_3149;

										{
											BgL_sawdfsz00_bglt BgL_auxz00_3673;

											{
												obj_t BgL_auxz00_3674;

												{	/* SawMill/blockorder.scm 97 */
													BgL_objectz00_bglt BgL_tmpz00_3675;

													BgL_tmpz00_3675 =
														((BgL_objectz00_bglt)
														((BgL_blockz00_bglt) BgL_b2z00_3059));
													BgL_auxz00_3674 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3675);
												}
												BgL_auxz00_3673 =
													((BgL_sawdfsz00_bglt) BgL_auxz00_3674);
											}
											BgL_b1490z00_3149 =
												(((BgL_sawdfsz00_bglt) COBJECT(BgL_auxz00_3673))->
												BgL_nz00);
										}
										{	/* SawMill/blockorder.scm 97 */

											BgL_tmpz00_3656 =
												(
												(long) (BgL_a1489z00_3148) >
												(long) (BgL_b1490z00_3149));
				}}}}}
				return BBOOL(BgL_tmpz00_3656);
			}
		}

	}



/* lengthuncolsucc */
	long BGl_lengthuncolsuccz00zzsaw_blockorderz00(BgL_blockz00_bglt BgL_bz00_65)
	{
		{	/* SawMill/blockorder.scm 99 */
			{	/* SawMill/blockorder.scm 100 */
				obj_t BgL_arg1661z00_2049;

				{	/* SawMill/blockorder.scm 100 */
					obj_t BgL_hook1495z00_2050;

					BgL_hook1495z00_2050 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
					{	/* SawMill/blockorder.scm 100 */
						obj_t BgL_g1496z00_2051;

						BgL_g1496z00_2051 =
							(((BgL_blockz00_bglt) COBJECT(BgL_bz00_65))->BgL_succsz00);
						{
							obj_t BgL_l1492z00_2053;
							obj_t BgL_h1493z00_2054;

							BgL_l1492z00_2053 = BgL_g1496z00_2051;
							BgL_h1493z00_2054 = BgL_hook1495z00_2050;
						BgL_zc3z04anonymousza31662ze3z87_2055:
							if (NULLP(BgL_l1492z00_2053))
								{	/* SawMill/blockorder.scm 100 */
									BgL_arg1661z00_2049 = CDR(BgL_hook1495z00_2050);
								}
							else
								{	/* SawMill/blockorder.scm 100 */
									bool_t BgL_test2060z00_3690;

									{	/* SawMill/blockorder.scm 100 */
										obj_t BgL_xz00_2064;

										BgL_xz00_2064 = CAR(((obj_t) BgL_l1492z00_2053));
										{	/* SawMill/blockorder.scm 100 */
											bool_t BgL_test2061z00_3693;

											{	/* SawMill/blockorder.scm 100 */
												obj_t BgL_classz00_2891;

												BgL_classz00_2891 = BGl_SawRdonez00zzsaw_blockorderz00;
												if (BGL_OBJECTP(BgL_xz00_2064))
													{	/* SawMill/blockorder.scm 100 */
														obj_t BgL_oclassz00_2893;

														{	/* SawMill/blockorder.scm 100 */
															obj_t BgL_arg1815z00_2895;
															long BgL_arg1816z00_2896;

															BgL_arg1815z00_2895 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawMill/blockorder.scm 100 */
																long BgL_arg1817z00_2897;

																BgL_arg1817z00_2897 =
																	BGL_OBJECT_CLASS_NUM(
																	((BgL_objectz00_bglt) BgL_xz00_2064));
																BgL_arg1816z00_2896 =
																	(BgL_arg1817z00_2897 - OBJECT_TYPE);
															}
															BgL_oclassz00_2893 =
																VECTOR_REF(BgL_arg1815z00_2895,
																BgL_arg1816z00_2896);
														}
														BgL_test2061z00_3693 =
															(BgL_oclassz00_2893 == BgL_classz00_2891);
													}
												else
													{	/* SawMill/blockorder.scm 100 */
														BgL_test2061z00_3693 = ((bool_t) 0);
													}
											}
											if (BgL_test2061z00_3693)
												{	/* SawMill/blockorder.scm 100 */
													BgL_test2060z00_3690 = ((bool_t) 0);
												}
											else
												{	/* SawMill/blockorder.scm 100 */
													BgL_test2060z00_3690 = ((bool_t) 1);
												}
										}
									}
									if (BgL_test2060z00_3690)
										{	/* SawMill/blockorder.scm 100 */
											obj_t BgL_nh1494z00_2060;

											{	/* SawMill/blockorder.scm 100 */
												obj_t BgL_arg1678z00_2062;

												BgL_arg1678z00_2062 = CAR(((obj_t) BgL_l1492z00_2053));
												BgL_nh1494z00_2060 =
													MAKE_YOUNG_PAIR(BgL_arg1678z00_2062, BNIL);
											}
											SET_CDR(BgL_h1493z00_2054, BgL_nh1494z00_2060);
											{	/* SawMill/blockorder.scm 100 */
												obj_t BgL_arg1675z00_2061;

												BgL_arg1675z00_2061 = CDR(((obj_t) BgL_l1492z00_2053));
												{
													obj_t BgL_h1493z00_3709;
													obj_t BgL_l1492z00_3708;

													BgL_l1492z00_3708 = BgL_arg1675z00_2061;
													BgL_h1493z00_3709 = BgL_nh1494z00_2060;
													BgL_h1493z00_2054 = BgL_h1493z00_3709;
													BgL_l1492z00_2053 = BgL_l1492z00_3708;
													goto BgL_zc3z04anonymousza31662ze3z87_2055;
												}
											}
										}
									else
										{	/* SawMill/blockorder.scm 100 */
											obj_t BgL_arg1681z00_2063;

											BgL_arg1681z00_2063 = CDR(((obj_t) BgL_l1492z00_2053));
											{
												obj_t BgL_l1492z00_3712;

												BgL_l1492z00_3712 = BgL_arg1681z00_2063;
												BgL_l1492z00_2053 = BgL_l1492z00_3712;
												goto BgL_zc3z04anonymousza31662ze3z87_2055;
											}
										}
								}
						}
					}
				}
				return bgl_list_length(BgL_arg1661z00_2049);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_blockorderz00(void)
	{
		{	/* SawMill/blockorder.scm 1 */
			{	/* SawMill/blockorder.scm 6 */
				obj_t BgL_arg1748z00_2146;
				obj_t BgL_arg1749z00_2147;

				{	/* SawMill/blockorder.scm 6 */
					obj_t BgL_v1500z00_2172;

					BgL_v1500z00_2172 = create_vector(0L);
					BgL_arg1748z00_2146 = BgL_v1500z00_2172;
				}
				{	/* SawMill/blockorder.scm 6 */
					obj_t BgL_v1501z00_2173;

					BgL_v1501z00_2173 = create_vector(0L);
					BgL_arg1749z00_2147 = BgL_v1501z00_2173;
				}
				BGl_SawDonez00zzsaw_blockorderz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(1),
					CNST_TABLE_REF(2), BGl_blockz00zzsaw_defsz00, 37933L,
					BGl_proc1975z00zzsaw_blockorderz00,
					BGl_proc1974z00zzsaw_blockorderz00, BFALSE,
					BGl_proc1973z00zzsaw_blockorderz00,
					BGl_proc1972z00zzsaw_blockorderz00, BgL_arg1748z00_2146,
					BgL_arg1749z00_2147);
			}
			{	/* SawMill/blockorder.scm 7 */
				obj_t BgL_arg1775z00_2182;
				obj_t BgL_arg1798z00_2183;

				{	/* SawMill/blockorder.scm 7 */
					obj_t BgL_v1502z00_2208;

					BgL_v1502z00_2208 = create_vector(0L);
					BgL_arg1775z00_2182 = BgL_v1502z00_2208;
				}
				{	/* SawMill/blockorder.scm 7 */
					obj_t BgL_v1503z00_2209;

					BgL_v1503z00_2209 = create_vector(0L);
					BgL_arg1798z00_2183 = BgL_v1503z00_2209;
				}
				BGl_SawRdonez00zzsaw_blockorderz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(3),
					CNST_TABLE_REF(2), BGl_blockz00zzsaw_defsz00, 64371L,
					BGl_proc1979z00zzsaw_blockorderz00,
					BGl_proc1978z00zzsaw_blockorderz00, BFALSE,
					BGl_proc1977z00zzsaw_blockorderz00,
					BGl_proc1976z00zzsaw_blockorderz00, BgL_arg1775z00_2182,
					BgL_arg1798z00_2183);
			}
			{	/* SawMill/blockorder.scm 8 */
				obj_t BgL_arg1834z00_2218;
				obj_t BgL_arg1835z00_2219;

				{	/* SawMill/blockorder.scm 8 */
					obj_t BgL_v1504z00_2245;

					BgL_v1504z00_2245 = create_vector(1L);
					{	/* SawMill/blockorder.scm 8 */
						obj_t BgL_arg1846z00_2246;

						BgL_arg1846z00_2246 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc1981z00zzsaw_blockorderz00,
							BGl_proc1980z00zzsaw_blockorderz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1504z00_2245, 0L, BgL_arg1846z00_2246);
					}
					BgL_arg1834z00_2218 = BgL_v1504z00_2245;
				}
				{	/* SawMill/blockorder.scm 8 */
					obj_t BgL_v1505z00_2256;

					BgL_v1505z00_2256 = create_vector(0L);
					BgL_arg1835z00_2219 = BgL_v1505z00_2256;
				}
				return (BGl_SawDfsz00zzsaw_blockorderz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(6),
						CNST_TABLE_REF(2), BGl_blockz00zzsaw_defsz00, 18539L,
						BGl_proc1985z00zzsaw_blockorderz00,
						BGl_proc1984z00zzsaw_blockorderz00, BFALSE,
						BGl_proc1983z00zzsaw_blockorderz00,
						BGl_proc1982z00zzsaw_blockorderz00, BgL_arg1834z00_2218,
						BgL_arg1835z00_2219), BUNSPEC);
			}
		}

	}



/* &lambda1842 */
	BgL_blockz00_bglt BGl_z62lambda1842z62zzsaw_blockorderz00(obj_t
		BgL_envz00_3074, obj_t BgL_o1179z00_3075)
	{
		{	/* SawMill/blockorder.scm 8 */
			{	/* SawMill/blockorder.scm 8 */
				long BgL_arg1843z00_3151;

				{	/* SawMill/blockorder.scm 8 */
					obj_t BgL_arg1844z00_3152;

					{	/* SawMill/blockorder.scm 8 */
						obj_t BgL_arg1845z00_3153;

						{	/* SawMill/blockorder.scm 8 */
							obj_t BgL_arg1815z00_3154;
							long BgL_arg1816z00_3155;

							BgL_arg1815z00_3154 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/blockorder.scm 8 */
								long BgL_arg1817z00_3156;

								BgL_arg1817z00_3156 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_o1179z00_3075)));
								BgL_arg1816z00_3155 = (BgL_arg1817z00_3156 - OBJECT_TYPE);
							}
							BgL_arg1845z00_3153 =
								VECTOR_REF(BgL_arg1815z00_3154, BgL_arg1816z00_3155);
						}
						BgL_arg1844z00_3152 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1845z00_3153);
					}
					{	/* SawMill/blockorder.scm 8 */
						obj_t BgL_tmpz00_3740;

						BgL_tmpz00_3740 = ((obj_t) BgL_arg1844z00_3152);
						BgL_arg1843z00_3151 = BGL_CLASS_NUM(BgL_tmpz00_3740);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) BgL_o1179z00_3075)), BgL_arg1843z00_3151);
			}
			{	/* SawMill/blockorder.scm 8 */
				BgL_objectz00_bglt BgL_tmpz00_3746;

				BgL_tmpz00_3746 =
					((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1179z00_3075));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3746, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1179z00_3075));
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1179z00_3075));
		}

	}



/* &<@anonymous:1841> */
	obj_t BGl_z62zc3z04anonymousza31841ze3ze5zzsaw_blockorderz00(obj_t
		BgL_envz00_3076, obj_t BgL_new1178z00_3077)
	{
		{	/* SawMill/blockorder.scm 8 */
			{
				BgL_blockz00_bglt BgL_auxz00_3754;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_new1178z00_3077))))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1178z00_3077))))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1178z00_3077))))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1178z00_3077))))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				{
					BgL_sawdfsz00_bglt BgL_auxz00_3768;

					{
						obj_t BgL_auxz00_3769;

						{	/* SawMill/blockorder.scm 8 */
							BgL_objectz00_bglt BgL_tmpz00_3770;

							BgL_tmpz00_3770 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1178z00_3077));
							BgL_auxz00_3769 = BGL_OBJECT_WIDENING(BgL_tmpz00_3770);
						}
						BgL_auxz00_3768 = ((BgL_sawdfsz00_bglt) BgL_auxz00_3769);
					}
					((((BgL_sawdfsz00_bglt) COBJECT(BgL_auxz00_3768))->BgL_nz00) = ((int)
							(int) (0L)), BUNSPEC);
				}
				BgL_auxz00_3754 = ((BgL_blockz00_bglt) BgL_new1178z00_3077);
				return ((obj_t) BgL_auxz00_3754);
			}
		}

	}



/* &lambda1839 */
	BgL_blockz00_bglt BGl_z62lambda1839z62zzsaw_blockorderz00(obj_t
		BgL_envz00_3078, obj_t BgL_o1175z00_3079)
	{
		{	/* SawMill/blockorder.scm 8 */
			{	/* SawMill/blockorder.scm 8 */
				BgL_sawdfsz00_bglt BgL_wide1177z00_3159;

				BgL_wide1177z00_3159 =
					((BgL_sawdfsz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sawdfsz00_bgl))));
				{	/* SawMill/blockorder.scm 8 */
					obj_t BgL_auxz00_3784;
					BgL_objectz00_bglt BgL_tmpz00_3780;

					BgL_auxz00_3784 = ((obj_t) BgL_wide1177z00_3159);
					BgL_tmpz00_3780 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1175z00_3079)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3780, BgL_auxz00_3784);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1175z00_3079)));
				{	/* SawMill/blockorder.scm 8 */
					long BgL_arg1840z00_3160;

					BgL_arg1840z00_3160 = BGL_CLASS_NUM(BGl_SawDfsz00zzsaw_blockorderz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_o1175z00_3079))), BgL_arg1840z00_3160);
				}
				return
					((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1175z00_3079)));
			}
		}

	}



/* &lambda1836 */
	BgL_blockz00_bglt BGl_z62lambda1836z62zzsaw_blockorderz00(obj_t
		BgL_envz00_3080, obj_t BgL_label1170z00_3081, obj_t BgL_preds1171z00_3082,
		obj_t BgL_succs1172z00_3083, obj_t BgL_first1173z00_3084,
		obj_t BgL_n1174z00_3085)
	{
		{	/* SawMill/blockorder.scm 8 */
			{	/* SawMill/blockorder.scm 8 */
				int BgL_label1170z00_3161;
				int BgL_n1174z00_3165;

				BgL_label1170z00_3161 = CINT(BgL_label1170z00_3081);
				BgL_n1174z00_3165 = CINT(BgL_n1174z00_3085);
				{	/* SawMill/blockorder.scm 8 */
					BgL_blockz00_bglt BgL_new1261z00_3166;

					{	/* SawMill/blockorder.scm 8 */
						BgL_blockz00_bglt BgL_tmp1259z00_3167;
						BgL_sawdfsz00_bglt BgL_wide1260z00_3168;

						{
							BgL_blockz00_bglt BgL_auxz00_3800;

							{	/* SawMill/blockorder.scm 8 */
								BgL_blockz00_bglt BgL_new1258z00_3169;

								BgL_new1258z00_3169 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawMill/blockorder.scm 8 */
									long BgL_arg1838z00_3170;

									BgL_arg1838z00_3170 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1258z00_3169),
										BgL_arg1838z00_3170);
								}
								{	/* SawMill/blockorder.scm 8 */
									BgL_objectz00_bglt BgL_tmpz00_3805;

									BgL_tmpz00_3805 = ((BgL_objectz00_bglt) BgL_new1258z00_3169);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3805, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1258z00_3169);
								BgL_auxz00_3800 = BgL_new1258z00_3169;
							}
							BgL_tmp1259z00_3167 = ((BgL_blockz00_bglt) BgL_auxz00_3800);
						}
						BgL_wide1260z00_3168 =
							((BgL_sawdfsz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_sawdfsz00_bgl))));
						{	/* SawMill/blockorder.scm 8 */
							obj_t BgL_auxz00_3813;
							BgL_objectz00_bglt BgL_tmpz00_3811;

							BgL_auxz00_3813 = ((obj_t) BgL_wide1260z00_3168);
							BgL_tmpz00_3811 = ((BgL_objectz00_bglt) BgL_tmp1259z00_3167);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3811, BgL_auxz00_3813);
						}
						((BgL_objectz00_bglt) BgL_tmp1259z00_3167);
						{	/* SawMill/blockorder.scm 8 */
							long BgL_arg1837z00_3171;

							BgL_arg1837z00_3171 =
								BGL_CLASS_NUM(BGl_SawDfsz00zzsaw_blockorderz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1259z00_3167),
								BgL_arg1837z00_3171);
						}
						BgL_new1261z00_3166 = ((BgL_blockz00_bglt) BgL_tmp1259z00_3167);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_new1261z00_3166)))->BgL_labelz00) =
						((int) BgL_label1170z00_3161), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1261z00_3166)))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1171z00_3082)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1261z00_3166)))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1172z00_3083)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1261z00_3166)))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1173z00_3084)), BUNSPEC);
					{
						BgL_sawdfsz00_bglt BgL_auxz00_3832;

						{
							obj_t BgL_auxz00_3833;

							{	/* SawMill/blockorder.scm 8 */
								BgL_objectz00_bglt BgL_tmpz00_3834;

								BgL_tmpz00_3834 = ((BgL_objectz00_bglt) BgL_new1261z00_3166);
								BgL_auxz00_3833 = BGL_OBJECT_WIDENING(BgL_tmpz00_3834);
							}
							BgL_auxz00_3832 = ((BgL_sawdfsz00_bglt) BgL_auxz00_3833);
						}
						((((BgL_sawdfsz00_bglt) COBJECT(BgL_auxz00_3832))->BgL_nz00) =
							((int) BgL_n1174z00_3165), BUNSPEC);
					}
					return BgL_new1261z00_3166;
				}
			}
		}

	}



/* &lambda1850 */
	obj_t BGl_z62lambda1850z62zzsaw_blockorderz00(obj_t BgL_envz00_3086,
		obj_t BgL_oz00_3087, obj_t BgL_vz00_3088)
	{
		{	/* SawMill/blockorder.scm 8 */
			{	/* SawMill/blockorder.scm 8 */
				int BgL_vz00_3173;

				BgL_vz00_3173 = CINT(BgL_vz00_3088);
				{
					BgL_sawdfsz00_bglt BgL_auxz00_3840;

					{
						obj_t BgL_auxz00_3841;

						{	/* SawMill/blockorder.scm 8 */
							BgL_objectz00_bglt BgL_tmpz00_3842;

							BgL_tmpz00_3842 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_3087));
							BgL_auxz00_3841 = BGL_OBJECT_WIDENING(BgL_tmpz00_3842);
						}
						BgL_auxz00_3840 = ((BgL_sawdfsz00_bglt) BgL_auxz00_3841);
					}
					return
						((((BgL_sawdfsz00_bglt) COBJECT(BgL_auxz00_3840))->BgL_nz00) =
						((int) BgL_vz00_3173), BUNSPEC);
		}}}

	}



/* &lambda1849 */
	obj_t BGl_z62lambda1849z62zzsaw_blockorderz00(obj_t BgL_envz00_3089,
		obj_t BgL_oz00_3090)
	{
		{	/* SawMill/blockorder.scm 8 */
			{	/* SawMill/blockorder.scm 8 */
				int BgL_tmpz00_3848;

				{
					BgL_sawdfsz00_bglt BgL_auxz00_3849;

					{
						obj_t BgL_auxz00_3850;

						{	/* SawMill/blockorder.scm 8 */
							BgL_objectz00_bglt BgL_tmpz00_3851;

							BgL_tmpz00_3851 =
								((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_3090));
							BgL_auxz00_3850 = BGL_OBJECT_WIDENING(BgL_tmpz00_3851);
						}
						BgL_auxz00_3849 = ((BgL_sawdfsz00_bglt) BgL_auxz00_3850);
					}
					BgL_tmpz00_3848 =
						(((BgL_sawdfsz00_bglt) COBJECT(BgL_auxz00_3849))->BgL_nz00);
				}
				return BINT(BgL_tmpz00_3848);
			}
		}

	}



/* &lambda1810 */
	BgL_blockz00_bglt BGl_z62lambda1810z62zzsaw_blockorderz00(obj_t
		BgL_envz00_3091, obj_t BgL_o1167z00_3092)
	{
		{	/* SawMill/blockorder.scm 7 */
			{	/* SawMill/blockorder.scm 7 */
				long BgL_arg1812z00_3176;

				{	/* SawMill/blockorder.scm 7 */
					obj_t BgL_arg1820z00_3177;

					{	/* SawMill/blockorder.scm 7 */
						obj_t BgL_arg1822z00_3178;

						{	/* SawMill/blockorder.scm 7 */
							obj_t BgL_arg1815z00_3179;
							long BgL_arg1816z00_3180;

							BgL_arg1815z00_3179 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/blockorder.scm 7 */
								long BgL_arg1817z00_3181;

								BgL_arg1817z00_3181 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_o1167z00_3092)));
								BgL_arg1816z00_3180 = (BgL_arg1817z00_3181 - OBJECT_TYPE);
							}
							BgL_arg1822z00_3178 =
								VECTOR_REF(BgL_arg1815z00_3179, BgL_arg1816z00_3180);
						}
						BgL_arg1820z00_3177 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1822z00_3178);
					}
					{	/* SawMill/blockorder.scm 7 */
						obj_t BgL_tmpz00_3865;

						BgL_tmpz00_3865 = ((obj_t) BgL_arg1820z00_3177);
						BgL_arg1812z00_3176 = BGL_CLASS_NUM(BgL_tmpz00_3865);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) BgL_o1167z00_3092)), BgL_arg1812z00_3176);
			}
			{	/* SawMill/blockorder.scm 7 */
				BgL_objectz00_bglt BgL_tmpz00_3871;

				BgL_tmpz00_3871 =
					((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1167z00_3092));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3871, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1167z00_3092));
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1167z00_3092));
		}

	}



/* &<@anonymous:1809> */
	obj_t BGl_z62zc3z04anonymousza31809ze3ze5zzsaw_blockorderz00(obj_t
		BgL_envz00_3093, obj_t BgL_new1166z00_3094)
	{
		{	/* SawMill/blockorder.scm 7 */
			{
				BgL_blockz00_bglt BgL_auxz00_3879;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_new1166z00_3094))))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1166z00_3094))))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1166z00_3094))))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1166z00_3094))))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_3879 = ((BgL_blockz00_bglt) BgL_new1166z00_3094);
				return ((obj_t) BgL_auxz00_3879);
			}
		}

	}



/* &lambda1807 */
	BgL_blockz00_bglt BGl_z62lambda1807z62zzsaw_blockorderz00(obj_t
		BgL_envz00_3095, obj_t BgL_o1163z00_3096)
	{
		{	/* SawMill/blockorder.scm 7 */
			{	/* SawMill/blockorder.scm 7 */
				BgL_sawrdonez00_bglt BgL_wide1165z00_3184;

				BgL_wide1165z00_3184 =
					((BgL_sawrdonez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sawrdonez00_bgl))));
				{	/* SawMill/blockorder.scm 7 */
					obj_t BgL_auxz00_3900;
					BgL_objectz00_bglt BgL_tmpz00_3896;

					BgL_auxz00_3900 = ((obj_t) BgL_wide1165z00_3184);
					BgL_tmpz00_3896 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1163z00_3096)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3896, BgL_auxz00_3900);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1163z00_3096)));
				{	/* SawMill/blockorder.scm 7 */
					long BgL_arg1808z00_3185;

					BgL_arg1808z00_3185 =
						BGL_CLASS_NUM(BGl_SawRdonez00zzsaw_blockorderz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_o1163z00_3096))), BgL_arg1808z00_3185);
				}
				return
					((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1163z00_3096)));
			}
		}

	}



/* &lambda1799 */
	BgL_blockz00_bglt BGl_z62lambda1799z62zzsaw_blockorderz00(obj_t
		BgL_envz00_3097, obj_t BgL_label1159z00_3098, obj_t BgL_preds1160z00_3099,
		obj_t BgL_succs1161z00_3100, obj_t BgL_first1162z00_3101)
	{
		{	/* SawMill/blockorder.scm 7 */
			{	/* SawMill/blockorder.scm 7 */
				int BgL_label1159z00_3186;

				BgL_label1159z00_3186 = CINT(BgL_label1159z00_3098);
				{	/* SawMill/blockorder.scm 7 */
					BgL_blockz00_bglt BgL_new1256z00_3190;

					{	/* SawMill/blockorder.scm 7 */
						BgL_blockz00_bglt BgL_tmp1254z00_3191;
						BgL_sawrdonez00_bglt BgL_wide1255z00_3192;

						{
							BgL_blockz00_bglt BgL_auxz00_3915;

							{	/* SawMill/blockorder.scm 7 */
								BgL_blockz00_bglt BgL_new1253z00_3193;

								BgL_new1253z00_3193 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawMill/blockorder.scm 7 */
									long BgL_arg1806z00_3194;

									BgL_arg1806z00_3194 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1253z00_3193),
										BgL_arg1806z00_3194);
								}
								{	/* SawMill/blockorder.scm 7 */
									BgL_objectz00_bglt BgL_tmpz00_3920;

									BgL_tmpz00_3920 = ((BgL_objectz00_bglt) BgL_new1253z00_3193);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3920, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1253z00_3193);
								BgL_auxz00_3915 = BgL_new1253z00_3193;
							}
							BgL_tmp1254z00_3191 = ((BgL_blockz00_bglt) BgL_auxz00_3915);
						}
						BgL_wide1255z00_3192 =
							((BgL_sawrdonez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_sawrdonez00_bgl))));
						{	/* SawMill/blockorder.scm 7 */
							obj_t BgL_auxz00_3928;
							BgL_objectz00_bglt BgL_tmpz00_3926;

							BgL_auxz00_3928 = ((obj_t) BgL_wide1255z00_3192);
							BgL_tmpz00_3926 = ((BgL_objectz00_bglt) BgL_tmp1254z00_3191);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3926, BgL_auxz00_3928);
						}
						((BgL_objectz00_bglt) BgL_tmp1254z00_3191);
						{	/* SawMill/blockorder.scm 7 */
							long BgL_arg1805z00_3195;

							BgL_arg1805z00_3195 =
								BGL_CLASS_NUM(BGl_SawRdonez00zzsaw_blockorderz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1254z00_3191),
								BgL_arg1805z00_3195);
						}
						BgL_new1256z00_3190 = ((BgL_blockz00_bglt) BgL_tmp1254z00_3191);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_new1256z00_3190)))->BgL_labelz00) =
						((int) BgL_label1159z00_3186), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1256z00_3190)))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1160z00_3099)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1256z00_3190)))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1161z00_3100)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1256z00_3190)))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1162z00_3101)), BUNSPEC);
					return BgL_new1256z00_3190;
				}
			}
		}

	}



/* &lambda1756 */
	BgL_blockz00_bglt BGl_z62lambda1756z62zzsaw_blockorderz00(obj_t
		BgL_envz00_3102, obj_t BgL_o1157z00_3103)
	{
		{	/* SawMill/blockorder.scm 6 */
			{	/* SawMill/blockorder.scm 6 */
				long BgL_arg1761z00_3197;

				{	/* SawMill/blockorder.scm 6 */
					obj_t BgL_arg1762z00_3198;

					{	/* SawMill/blockorder.scm 6 */
						obj_t BgL_arg1765z00_3199;

						{	/* SawMill/blockorder.scm 6 */
							obj_t BgL_arg1815z00_3200;
							long BgL_arg1816z00_3201;

							BgL_arg1815z00_3200 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/blockorder.scm 6 */
								long BgL_arg1817z00_3202;

								BgL_arg1817z00_3202 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_o1157z00_3103)));
								BgL_arg1816z00_3201 = (BgL_arg1817z00_3202 - OBJECT_TYPE);
							}
							BgL_arg1765z00_3199 =
								VECTOR_REF(BgL_arg1815z00_3200, BgL_arg1816z00_3201);
						}
						BgL_arg1762z00_3198 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1765z00_3199);
					}
					{	/* SawMill/blockorder.scm 6 */
						obj_t BgL_tmpz00_3954;

						BgL_tmpz00_3954 = ((obj_t) BgL_arg1762z00_3198);
						BgL_arg1761z00_3197 = BGL_CLASS_NUM(BgL_tmpz00_3954);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) BgL_o1157z00_3103)), BgL_arg1761z00_3197);
			}
			{	/* SawMill/blockorder.scm 6 */
				BgL_objectz00_bglt BgL_tmpz00_3960;

				BgL_tmpz00_3960 =
					((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1157z00_3103));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3960, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1157z00_3103));
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1157z00_3103));
		}

	}



/* &<@anonymous:1755> */
	obj_t BGl_z62zc3z04anonymousza31755ze3ze5zzsaw_blockorderz00(obj_t
		BgL_envz00_3104, obj_t BgL_new1156z00_3105)
	{
		{	/* SawMill/blockorder.scm 6 */
			{
				BgL_blockz00_bglt BgL_auxz00_3968;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_new1156z00_3105))))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1156z00_3105))))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1156z00_3105))))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1156z00_3105))))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_3968 = ((BgL_blockz00_bglt) BgL_new1156z00_3105);
				return ((obj_t) BgL_auxz00_3968);
			}
		}

	}



/* &lambda1753 */
	BgL_blockz00_bglt BGl_z62lambda1753z62zzsaw_blockorderz00(obj_t
		BgL_envz00_3106, obj_t BgL_o1153z00_3107)
	{
		{	/* SawMill/blockorder.scm 6 */
			{	/* SawMill/blockorder.scm 6 */
				BgL_sawdonez00_bglt BgL_wide1155z00_3205;

				BgL_wide1155z00_3205 =
					((BgL_sawdonez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sawdonez00_bgl))));
				{	/* SawMill/blockorder.scm 6 */
					obj_t BgL_auxz00_3989;
					BgL_objectz00_bglt BgL_tmpz00_3985;

					BgL_auxz00_3989 = ((obj_t) BgL_wide1155z00_3205);
					BgL_tmpz00_3985 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1153z00_3107)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3985, BgL_auxz00_3989);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1153z00_3107)));
				{	/* SawMill/blockorder.scm 6 */
					long BgL_arg1754z00_3206;

					BgL_arg1754z00_3206 =
						BGL_CLASS_NUM(BGl_SawDonez00zzsaw_blockorderz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_o1153z00_3107))), BgL_arg1754z00_3206);
				}
				return
					((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1153z00_3107)));
			}
		}

	}



/* &lambda1750 */
	BgL_blockz00_bglt BGl_z62lambda1750z62zzsaw_blockorderz00(obj_t
		BgL_envz00_3108, obj_t BgL_label1149z00_3109, obj_t BgL_preds1150z00_3110,
		obj_t BgL_succs1151z00_3111, obj_t BgL_first1152z00_3112)
	{
		{	/* SawMill/blockorder.scm 6 */
			{	/* SawMill/blockorder.scm 6 */
				int BgL_label1149z00_3207;

				BgL_label1149z00_3207 = CINT(BgL_label1149z00_3109);
				{	/* SawMill/blockorder.scm 6 */
					BgL_blockz00_bglt BgL_new1251z00_3211;

					{	/* SawMill/blockorder.scm 6 */
						BgL_blockz00_bglt BgL_tmp1249z00_3212;
						BgL_sawdonez00_bglt BgL_wide1250z00_3213;

						{
							BgL_blockz00_bglt BgL_auxz00_4004;

							{	/* SawMill/blockorder.scm 6 */
								BgL_blockz00_bglt BgL_new1248z00_3214;

								BgL_new1248z00_3214 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawMill/blockorder.scm 6 */
									long BgL_arg1752z00_3215;

									BgL_arg1752z00_3215 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1248z00_3214),
										BgL_arg1752z00_3215);
								}
								{	/* SawMill/blockorder.scm 6 */
									BgL_objectz00_bglt BgL_tmpz00_4009;

									BgL_tmpz00_4009 = ((BgL_objectz00_bglt) BgL_new1248z00_3214);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4009, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1248z00_3214);
								BgL_auxz00_4004 = BgL_new1248z00_3214;
							}
							BgL_tmp1249z00_3212 = ((BgL_blockz00_bglt) BgL_auxz00_4004);
						}
						BgL_wide1250z00_3213 =
							((BgL_sawdonez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_sawdonez00_bgl))));
						{	/* SawMill/blockorder.scm 6 */
							obj_t BgL_auxz00_4017;
							BgL_objectz00_bglt BgL_tmpz00_4015;

							BgL_auxz00_4017 = ((obj_t) BgL_wide1250z00_3213);
							BgL_tmpz00_4015 = ((BgL_objectz00_bglt) BgL_tmp1249z00_3212);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4015, BgL_auxz00_4017);
						}
						((BgL_objectz00_bglt) BgL_tmp1249z00_3212);
						{	/* SawMill/blockorder.scm 6 */
							long BgL_arg1751z00_3216;

							BgL_arg1751z00_3216 =
								BGL_CLASS_NUM(BGl_SawDonez00zzsaw_blockorderz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1249z00_3212),
								BgL_arg1751z00_3216);
						}
						BgL_new1251z00_3211 = ((BgL_blockz00_bglt) BgL_tmp1249z00_3212);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_new1251z00_3211)))->BgL_labelz00) =
						((int) BgL_label1149z00_3207), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1251z00_3211)))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1150z00_3110)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1251z00_3211)))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1151z00_3111)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1251z00_3211)))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1152z00_3112)), BUNSPEC);
					return BgL_new1251z00_3211;
				}
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_blockorderz00(void)
	{
		{	/* SawMill/blockorder.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_blockorderz00(void)
	{
		{	/* SawMill/blockorder.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_blockorderz00(void)
	{
		{	/* SawMill/blockorder.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1986z00zzsaw_blockorderz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1986z00zzsaw_blockorderz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1986z00zzsaw_blockorderz00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string1986z00zzsaw_blockorderz00));
		}

	}

#ifdef __cplusplus
}
#endif
