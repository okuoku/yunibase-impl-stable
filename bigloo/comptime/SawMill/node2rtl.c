/*===========================================================================*/
/*   (SawMill/node2rtl.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/node2rtl.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_NODE2RTL_TYPE_DEFINITIONS
#define BGL_SAW_NODE2RTL_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_getfieldz00_bglt;

	typedef struct BgL_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_setfieldz00_bglt;

	typedef struct BgL_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_argszd2typezd2;
	}             *BgL_newz00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_ftypez00;
	}                 *BgL_vlengthz00_bglt;

	typedef struct BgL_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_classz00;
	}                    *BgL_instanceofz00_bglt;

	typedef struct BgL_castzd2nullzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                     *BgL_castzd2nullzd2_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                  *BgL_retblockz00_bglt;

	typedef struct BgL_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_retblockz00_bgl *BgL_blockz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                *BgL_returnz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                    *BgL_rtl_returnz00_bglt;

	typedef struct BgL_rtl_jumpexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                      *BgL_rtl_jumpexitz00_bglt;

	typedef struct BgL_rtl_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                  *BgL_rtl_failz00_bglt;

	typedef struct BgL_rtl_ifz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                *BgL_rtl_ifz00_bglt;

	typedef struct BgL_rtl_selectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_patternsz00;
	}                    *BgL_rtl_selectz00_bglt;

	typedef struct BgL_rtl_nopz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_nopz00_bglt;

	typedef struct BgL_rtl_movz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_movz00_bglt;

	typedef struct BgL_rtl_loadiz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_atomz00_bgl *BgL_constantz00;
	}                   *BgL_rtl_loadiz00_bglt;

	typedef struct BgL_rtl_loadgz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                   *BgL_rtl_loadgz00_bglt;

	typedef struct BgL_rtl_loadfunz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                     *BgL_rtl_loadfunz00_bglt;

	typedef struct BgL_rtl_globalrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                       *BgL_rtl_globalrefz00_bglt;

	typedef struct BgL_rtl_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_objtypez00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                      *BgL_rtl_getfieldz00_bglt;

	typedef struct BgL_rtl_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                    *BgL_rtl_vallocz00_bglt;

	typedef struct BgL_rtl_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                  *BgL_rtl_vrefz00_bglt;

	typedef struct BgL_rtl_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                     *BgL_rtl_vlengthz00_bglt;

	typedef struct BgL_rtl_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                        *BgL_rtl_instanceofz00_bglt;

	typedef struct BgL_rtl_makeboxz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                     *BgL_rtl_makeboxz00_bglt;

	typedef struct BgL_rtl_boxrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                    *BgL_rtl_boxrefz00_bglt;

	typedef struct BgL_rtl_storegz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                    *BgL_rtl_storegz00_bglt;

	typedef struct BgL_rtl_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_objtypez00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                      *BgL_rtl_setfieldz00_bglt;

	typedef struct BgL_rtl_vsetz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                  *BgL_rtl_vsetz00_bglt;

	typedef struct BgL_rtl_boxsetz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                    *BgL_rtl_boxsetz00_bglt;

	typedef struct BgL_rtl_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_constrz00;
	}                 *BgL_rtl_newz00_bglt;

	typedef struct BgL_rtl_callz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                  *BgL_rtl_callz00_bglt;

	typedef struct BgL_rtl_applyz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                   *BgL_rtl_applyz00_bglt;

	typedef struct BgL_rtl_lightfuncallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_funsz00;
		obj_t BgL_rettypez00;
	}                          *BgL_rtl_lightfuncallz00_bglt;

	typedef struct BgL_rtl_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                     *BgL_rtl_funcallz00_bglt;

	typedef struct BgL_rtl_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                    *BgL_rtl_pragmaz00_bglt;

	typedef struct BgL_rtl_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_totypez00;
		struct BgL_typez00_bgl *BgL_fromtypez00;
	}                  *BgL_rtl_castz00_bglt;

	typedef struct BgL_rtl_cast_nullz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                       *BgL_rtl_cast_nullz00_bglt;

	typedef struct BgL_rtl_protectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                     *BgL_rtl_protectz00_bglt;

	typedef struct BgL_rtl_protectedz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                       *BgL_rtl_protectedz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_areaz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_blockz00_bgl *BgL_entryz00;
		struct BgL_blockz00_bgl *BgL_exitz00;
	}              *BgL_areaz00_bglt;

	typedef struct BgL_reversedz00_bgl
	{
	}                  *BgL_reversedz00_bglt;

	typedef struct BgL_rlocalz00_bgl
	{
		obj_t BgL_regz00;
		obj_t BgL_codez00;
	}                *BgL_rlocalz00_bglt;

	typedef struct BgL_retblockzf2tozf2_bgl
	{
		struct BgL_blockz00_bgl *BgL_toz00;
		struct BgL_rtl_regz00_bgl *BgL_dstz00;
	}                       *BgL_retblockzf2tozf2_bglt;


#endif													// BGL_SAW_NODE2RTL_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	static BgL_areaz00_bglt
		BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(BgL_nodez00_bglt);
	static BgL_areaz00_bglt BGl_moveze70ze7zzsaw_node2rtlz00(BgL_rtl_regz00_bglt,
		BgL_localz00_bglt);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_globalzd2ze3rtlz31zzsaw_node2rtlz00(BgL_globalz00_bglt);
	extern obj_t BGl_rtl_funcallz00zzsaw_defsz00;
	extern obj_t BGl_rtl_newz00zzsaw_defsz00;
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_returnz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	extern obj_t BGl_rtl_pragmaz00zzsaw_defsz00;
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_node2rtlz00 = BUNSPEC;
	static obj_t BGl_imperativezf3zf3zzsaw_node2rtlz00(BgL_appz00_bglt,
		BgL_globalz00_bglt, obj_t);
	static BgL_areaz00_bglt BGl_z62predicatez62zzsaw_node2rtlz00(obj_t, obj_t,
		obj_t);
	static BgL_areaz00_bglt BGl_linkza2za2zzsaw_node2rtlz00(obj_t);
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2sequence1727z81zzsaw_node2rtlz00(obj_t, obj_t);
	static obj_t BGl_bdestinationz12z12zzsaw_node2rtlz00(BgL_blockz00_bglt,
		BgL_rtl_regz00_bglt);
	static BgL_areaz00_bglt BGl_callza2za2zzsaw_node2rtlz00(obj_t,
		BgL_rtl_funz00_bglt, obj_t);
	extern obj_t BGl_newz00zzast_nodez00;
	extern obj_t BGl_rtl_boxrefz00zzsaw_defsz00;
	extern obj_t BGl_vlengthz00zzast_nodez00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_retblockzf2tozf2zzsaw_node2rtlz00 = BUNSPEC;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2app1737z81zzsaw_node2rtlz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	extern obj_t BGl_sfunz00zzast_varz00;
	static BgL_areaz00_bglt
		BGl_compilezd2labelzd2callz00zzsaw_node2rtlz00(BgL_localz00_bglt, obj_t);
	extern obj_t BGl_rtl_vrefz00zzsaw_defsz00;
	static obj_t BGl_z62zc3z04anonymousza32103ze3ze5zzsaw_node2rtlz00(obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzsaw_node2rtlz00(void);
	extern obj_t BGl_rtl_protectz00zzsaw_defsz00;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2vref1753z81zzsaw_node2rtlz00(obj_t, obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_rtl_boxsetz00zzsaw_defsz00;
	extern obj_t BGl_rtl_vlengthz00zzsaw_defsz00;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2vsetz121755z93zzsaw_node2rtlz00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	static BgL_areaz00_bglt BGl_predicatez00zzsaw_node2rtlz00(BgL_nodez00_bglt,
		obj_t);
	extern obj_t BGl_rtl_makeboxz00zzsaw_defsz00;
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_node2rtlz00(void);
	extern obj_t BGl_getfieldz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_vsetz00zzsaw_defsz00;
	static obj_t BGl_objectzd2initzd2zzsaw_node2rtlz00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2atom1719z81zzsaw_node2rtlz00(obj_t, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	extern obj_t BGl_rtl_lightfuncallz00zzsaw_defsz00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	extern obj_t BGl_rtl_vallocz00zzsaw_defsz00;
	static BgL_blockz00_bglt BGl_z62globalzd2ze3rtlz53zzsaw_node2rtlz00(obj_t,
		obj_t);
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2instanceof1759z81zzsaw_node2rtlz00(obj_t, obj_t);
	static BgL_areaz00_bglt
		BGl_z62predicatezd2letzd2var1787z62zzsaw_node2rtlz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_cast_nullz00zzsaw_defsz00;
	extern obj_t BGl_rtl_loadfunz00zzsaw_defsz00;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2vlength1757z81zzsaw_node2rtlz00(obj_t, obj_t);
	extern obj_t BGl_rtl_movz00zzsaw_defsz00;
	static bool_t BGl_za2reversezd2callzd2argumentza2z00zzsaw_node2rtlz00;
	static BgL_areaz00_bglt BGl_singleza2za2zzsaw_node2rtlz00(obj_t,
		BgL_rtl_funz00_bglt, obj_t);
	static BgL_areaz00_bglt
		BGl_localzd2ze3codez31zzsaw_node2rtlz00(BgL_localz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_localzd2ze3regz31zzsaw_node2rtlz00(BgL_localz00_bglt);
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_reversedz00zzsaw_node2rtlz00 = BUNSPEC;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2letzd2fun1735z53zzsaw_node2rtlz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32125ze3ze5zzsaw_node2rtlz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_nopz00zzsaw_defsz00;
	static obj_t BGl_methodzd2initzd2zzsaw_node2rtlz00(void);
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2valloc1751z81zzsaw_node2rtlz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32142ze3ze5zzsaw_node2rtlz00(obj_t,
		obj_t);
	static BgL_areaz00_bglt BGl_forkz00zzsaw_node2rtlz00(BgL_areaz00_bglt, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62localzd2ze3regz53zzsaw_node2rtlz00(obj_t,
		obj_t);
	extern obj_t BGl_rtl_jumpexitz00zzsaw_defsz00;
	static obj_t BGl_z62nodezd2ze3rtl1716z53zzsaw_node2rtlz00(obj_t, obj_t);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static BgL_areaz00_bglt BGl_linkz00zzsaw_node2rtlz00(BgL_areaz00_bglt,
		BgL_areaz00_bglt);
	extern obj_t BGl_rtl_instanceofz00zzsaw_defsz00;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2appzd2ly1739z53zzsaw_node2rtlz00(obj_t, obj_t);
	extern obj_t BGl_rtl_setfieldz00zzsaw_defsz00;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2retblock1777z81zzsaw_node2rtlz00(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	static obj_t BGl_joinz00zzsaw_node2rtlz00(obj_t, BgL_rtl_regz00_bglt);
	static BgL_areaz00_bglt BGl_forkzf2joinzf2zzsaw_node2rtlz00(BgL_areaz00_bglt,
		obj_t, BgL_rtl_regz00_bglt);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_argszd21ze70z35zzsaw_node2rtlz00(obj_t);
	extern obj_t BGl_rtl_ifz00zzsaw_defsz00;
	static BgL_areaz00_bglt
		BGl_z62predicatezd2conditiona1785zb0zzsaw_node2rtlz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_callz00zzsaw_defsz00;
	extern obj_t BGl_rtl_selectz00zzsaw_defsz00;
	extern obj_t BGl_pragmaz00zzast_nodez00;
	static BgL_areaz00_bglt BGl_z62lambda2101z62zzsaw_node2rtlz00(obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2107z62zzsaw_node2rtlz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2108z62zzsaw_node2rtlz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_storegz00zzsaw_defsz00;
	static obj_t BGl_z62predicate1780z62zzsaw_node2rtlz00(obj_t, obj_t, obj_t);
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2getfield1745z81zzsaw_node2rtlz00(obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2112z62zzsaw_node2rtlz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2113z62zzsaw_node2rtlz00(obj_t, obj_t, obj_t);
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2switch1733z81zzsaw_node2rtlz00(obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static BgL_areaz00_bglt BGl_z62predicatezd2atom1783zb0zzsaw_node2rtlz00(obj_t,
		obj_t, obj_t);
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2conditiona1731z81zzsaw_node2rtlz00(obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2120z62zzsaw_node2rtlz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2123z62zzsaw_node2rtlz00(obj_t, obj_t);
	extern obj_t BGl_rtl_returnz00zzsaw_defsz00;
	static BgL_blockz00_bglt BGl_z62lambda2126z62zzsaw_node2rtlz00(obj_t, obj_t);
	extern obj_t BGl_rtl_applyz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_node2rtlz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsync_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2return1779z81zzsaw_node2rtlz00(obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static BgL_localz00_bglt BGl_z62lambda2137z62zzsaw_node2rtlz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern BgL_nodez00_bglt
		BGl_synczd2ze3sequencez31zzsync_nodez00(BgL_syncz00_bglt);
	static BgL_localz00_bglt BGl_z62lambda2140z62zzsaw_node2rtlz00(obj_t, obj_t);
	static BgL_localz00_bglt BGl_z62lambda2143z62zzsaw_node2rtlz00(obj_t, obj_t);
	extern obj_t BGl_vrefz00zzast_nodez00;
	extern obj_t BGl_rtl_loadgz00zzsaw_defsz00;
	static obj_t BGl_areaz00zzsaw_node2rtlz00 = BUNSPEC;
	extern obj_t BGl_rtl_loadiz00zzsaw_defsz00;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2letzd2var1723z53zzsaw_node2rtlz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_rtl_failz00zzsaw_defsz00;
	static obj_t BGl_z62lambda2150z62zzsaw_node2rtlz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2151z62zzsaw_node2rtlz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62lambda2156z62zzsaw_node2rtlz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2157z62zzsaw_node2rtlz00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_node2rtlz00(void);
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2setfield1747z81zzsaw_node2rtlz00(obj_t, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_node2rtlz00(void);
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2setq1725z81zzsaw_node2rtlz00(obj_t, obj_t);
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2setzd2exzd2it1765z81zzsaw_node2rtlz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static bool_t BGl_dfsze70ze7zzsaw_node2rtlz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_node2rtlz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_node2rtlz00(void);
	static BgL_retblockz00_bglt BGl_z62lambda2164z62zzsaw_node2rtlz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32169ze3ze5zzsaw_node2rtlz00(obj_t,
		obj_t);
	static BgL_retblockz00_bglt BGl_z62lambda2167z62zzsaw_node2rtlz00(obj_t,
		obj_t);
	extern obj_t BGl_rtl_castz00zzsaw_defsz00;
	extern obj_t BGl_vallocz00zzast_nodez00;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2funcall1741z81zzsaw_node2rtlz00(obj_t, obj_t);
	static BgL_retblockz00_bglt BGl_z62lambda2170z62zzsaw_node2rtlz00(obj_t,
		obj_t);
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2new1749z81zzsaw_node2rtlz00(obj_t, obj_t);
	extern obj_t BGl_retblockz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_objectz00zz__objectz00;
	static BgL_blockz00_bglt BGl_z62lambda2177z62zzsaw_node2rtlz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2178z62zzsaw_node2rtlz00(obj_t, obj_t, obj_t);
	static BgL_areaz00_bglt BGl_z62lambda2099z62zzsaw_node2rtlz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_rlocalz00zzsaw_node2rtlz00 = BUNSPEC;
	extern obj_t BGl_rtl_globalrefz00zzsaw_defsz00;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2sync1729z81zzsaw_node2rtlz00(obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_newzd2uregzd2zzsaw_node2rtlz00(BgL_localz00_bglt);
	extern obj_t BGl_instanceofz00zzast_nodez00;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2cast1761z81zzsaw_node2rtlz00(obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda2182z62zzsaw_node2rtlz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2183z62zzsaw_node2rtlz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
	static BgL_rtl_regz00_bglt
		BGl_newzd2regzd2zzsaw_node2rtlz00(BgL_nodez00_bglt);
	extern obj_t BGl_rtl_getfieldz00zzsaw_defsz00;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2castzd2null1763z53zzsaw_node2rtlz00(obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_valuez00zzast_varz00;
	extern BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t);
	extern obj_t BGl_setfieldz00zzast_nodez00;
	static obj_t BGl_coercez00zzsaw_node2rtlz00(obj_t);
	extern obj_t BGl_castzd2nullzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2fail1769z81zzsaw_node2rtlz00(obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2boxzd2ref1773z53zzsaw_node2rtlz00(obj_t, obj_t);
	static BgL_areaz00_bglt BGl_z62nodezd2ze3rtlz53zzsaw_node2rtlz00(obj_t,
		obj_t);
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2boxzd2setz121775z41zzsaw_node2rtlz00(obj_t, obj_t);
	extern obj_t
		BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(BgL_variablez00_bglt);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2makezd2box1771z53zzsaw_node2rtlz00(obj_t, obj_t);
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2var1721z81zzsaw_node2rtlz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t BGl_rtl_protectedz00zzsaw_defsz00;
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_blockz00zzsaw_defsz00;
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_findzd2registerzd2zzsaw_node2rtlz00(obj_t,
		BgL_rtl_regz00_bglt);
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2jumpzd2exzd2it1767z81zzsaw_node2rtlz00(obj_t, obj_t);
	static BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2pragma1743z81zzsaw_node2rtlz00(obj_t, obj_t);
	static BgL_globalz00_bglt
		BGl_getzd2globalzd2zzsaw_node2rtlz00(BgL_globalz00_bglt);
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	static BgL_areaz00_bglt BGl_unlinkz00zzsaw_node2rtlz00(BgL_areaz00_bglt);
	static obj_t __cnst[19];


	   
		 
		DEFINE_STATIC_BGL_GENERIC(BGl_predicatezd2envzd2zzsaw_node2rtlz00,
		BgL_bgl_za762predicateza762za72955za7, BGl_z62predicatez62zzsaw_node2rtlz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_GENERIC(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2956za7,
		BGl_z62nodezd2ze3rtlz53zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2900z00zzsaw_node2rtlz00,
		BgL_bgl_string2900za700za7za7s2957za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2902z00zzsaw_node2rtlz00,
		BgL_bgl_string2902za700za7za7s2958za7, "node->rtl::area", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2901z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2959za7,
		BGl_z62nodezd2ze3rtlzd2atom1719z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2903z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2960za7,
		BGl_z62nodezd2ze3rtlzd2var1721z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2904z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2961za7,
		BGl_z62nodezd2ze3rtlzd2letzd2var1723z53zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2905z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2962za7,
		BGl_z62nodezd2ze3rtlzd2setq1725z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2906z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2963za7,
		BGl_z62nodezd2ze3rtlzd2sequence1727z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2907z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2964za7,
		BGl_z62nodezd2ze3rtlzd2sync1729z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2908z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2965za7,
		BGl_z62nodezd2ze3rtlzd2conditiona1731z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2909z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2966za7,
		BGl_z62nodezd2ze3rtlzd2switch1733z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2910z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2967za7,
		BGl_z62nodezd2ze3rtlzd2letzd2fun1735z53zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2911z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2968za7,
		BGl_z62nodezd2ze3rtlzd2app1737z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2912z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2969za7,
		BGl_z62nodezd2ze3rtlzd2appzd2ly1739z53zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2913z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2970za7,
		BGl_z62nodezd2ze3rtlzd2funcall1741z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2914z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2971za7,
		BGl_z62nodezd2ze3rtlzd2pragma1743z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2915z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2972za7,
		BGl_z62nodezd2ze3rtlzd2getfield1745z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2916z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2973za7,
		BGl_z62nodezd2ze3rtlzd2setfield1747z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2917z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2974za7,
		BGl_z62nodezd2ze3rtlzd2new1749z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2918z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2975za7,
		BGl_z62nodezd2ze3rtlzd2valloc1751z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2919z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2976za7,
		BGl_z62nodezd2ze3rtlzd2vref1753z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2920z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2977za7,
		BGl_z62nodezd2ze3rtlzd2vsetz121755z93zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2921z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2978za7,
		BGl_z62nodezd2ze3rtlzd2vlength1757z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2922z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2979za7,
		BGl_z62nodezd2ze3rtlzd2instanceof1759z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2923z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2980za7,
		BGl_z62nodezd2ze3rtlzd2cast1761z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2924z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2981za7,
		BGl_z62nodezd2ze3rtlzd2castzd2null1763z53zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2925z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2982za7,
		BGl_z62nodezd2ze3rtlzd2setzd2exzd2it1765z81zzsaw_node2rtlz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2926z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2983za7,
		BGl_z62nodezd2ze3rtlzd2jumpzd2exzd2it1767z81zzsaw_node2rtlz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2927z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2984za7,
		BGl_z62nodezd2ze3rtlzd2fail1769z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2934z00zzsaw_node2rtlz00,
		BgL_bgl_string2934za700za7za7s2985za7, "predicate::area", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2928z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2986za7,
		BGl_z62nodezd2ze3rtlzd2makezd2box1771z53zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2929z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2987za7,
		BGl_z62nodezd2ze3rtlzd2boxzd2ref1773z53zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2937z00zzsaw_node2rtlz00,
		BgL_bgl_string2937za700za7za7s2988za7, " ", 1);
	      DEFINE_STRING(BGl_string2938z00zzsaw_node2rtlz00,
		BgL_bgl_string2938za700za7za7s2989za7, "node->rtl ", 10);
	      DEFINE_STRING(BGl_string2939z00zzsaw_node2rtlz00,
		BgL_bgl_string2939za700za7za7s2990za7, ":", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2930z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2991za7,
		BGl_z62nodezd2ze3rtlzd2boxzd2setz121775z41zzsaw_node2rtlz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2931z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2992za7,
		BGl_z62nodezd2ze3rtlzd2retblock1777z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2932z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl2993za7,
		BGl_z62nodezd2ze3rtlzd2return1779z81zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2933z00zzsaw_node2rtlz00,
		BgL_bgl_za762predicateza7d2a2994z00,
		BGl_z62predicatezd2atom1783zb0zzsaw_node2rtlz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2940z00zzsaw_node2rtlz00,
		BgL_bgl_string2940za700za7za7s2995za7, ",", 1);
	      DEFINE_STRING(BGl_string2941z00zzsaw_node2rtlz00,
		BgL_bgl_string2941za700za7za7s2996za7, "SawMill/node2rtl.scm", 20);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2935z00zzsaw_node2rtlz00,
		BgL_bgl_za762predicateza7d2c2997z00,
		BGl_z62predicatezd2conditiona1785zb0zzsaw_node2rtlz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2942z00zzsaw_node2rtlz00,
		BgL_bgl_string2942za700za7za7s2998za7, "LOAD FUN...", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2936z00zzsaw_node2rtlz00,
		BgL_bgl_za762predicateza7d2l2999z00,
		BGl_z62predicatezd2letzd2var1787z62zzsaw_node2rtlz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2943z00zzsaw_node2rtlz00,
		BgL_bgl_string2943za700za7za7s3000za7, "saw_node2rtl", 12);
	      DEFINE_STRING(BGl_string2944z00zzsaw_node2rtlz00,
		BgL_bgl_string2944za700za7za7s3001za7,
		"light elight node->rtl1716 _ retblock/to dst to rlocal code obj reg reversed saw_node2rtl area exit entry __evmeaning_address else procedure ",
		141);
	      DEFINE_STRING(BGl_string2867z00zzsaw_node2rtlz00,
		BgL_bgl_string2867za700za7za7s3002za7, "switch", 6);
	      DEFINE_STRING(BGl_string2868z00zzsaw_node2rtlz00,
		BgL_bgl_string2868za700za7za7s3003za7, "bad constant", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2869z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2108za7623004z00, BGl_z62lambda2108z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2870z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2107za7623005z00, BGl_z62lambda2107z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2871z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2113za7623006z00, BGl_z62lambda2113z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2872z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2112za7623007z00, BGl_z62lambda2112z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2873z00zzsaw_node2rtlz00,
		BgL_bgl_za762za7c3za704anonymo3008za7,
		BGl_z62zc3z04anonymousza32103ze3ze5zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2874z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2101za7623009z00, BGl_z62lambda2101z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2875z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2099za7623010z00, BGl_z62lambda2099z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2876z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2126za7623011z00, BGl_z62lambda2126z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2877z00zzsaw_node2rtlz00,
		BgL_bgl_za762za7c3za704anonymo3012za7,
		BGl_z62zc3z04anonymousza32125ze3ze5zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2878z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2123za7623013z00, BGl_z62lambda2123z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2879z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2120za7623014z00, BGl_z62lambda2120z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2880z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2151za7623015z00, BGl_z62lambda2151z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2881z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2150za7623016z00, BGl_z62lambda2150z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2882z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2157za7623017z00, BGl_z62lambda2157z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2883z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2156za7623018z00, BGl_z62lambda2156z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2884z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2143za7623019z00, BGl_z62lambda2143z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2885z00zzsaw_node2rtlz00,
		BgL_bgl_za762za7c3za704anonymo3020za7,
		BGl_z62zc3z04anonymousza32142ze3ze5zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2886z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2140za7623021z00, BGl_z62lambda2140z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2887z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2137za7623022z00, BGl_z62lambda2137z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2888z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2178za7623023z00, BGl_z62lambda2178z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2889z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2177za7623024z00, BGl_z62lambda2177z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2897z00zzsaw_node2rtlz00,
		BgL_bgl_string2897za700za7za7s3025za7, "node->rtl1716", 13);
	      DEFINE_STRING(BGl_string2899z00zzsaw_node2rtlz00,
		BgL_bgl_string2899za700za7za7s3026za7, "predicate1780", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2890z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2183za7623027z00, BGl_z62lambda2183z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2891z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2182za7623028z00, BGl_z62lambda2182z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2892z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2170za7623029z00, BGl_z62lambda2170z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2893z00zzsaw_node2rtlz00,
		BgL_bgl_za762za7c3za704anonymo3030za7,
		BGl_z62zc3z04anonymousza32169ze3ze5zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2894z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2167za7623031z00, BGl_z62lambda2167z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2895z00zzsaw_node2rtlz00,
		BgL_bgl_za762lambda2164za7623032z00, BGl_z62lambda2164z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2896z00zzsaw_node2rtlz00,
		BgL_bgl_za762nodeza7d2za7e3rtl3033za7,
		BGl_z62nodezd2ze3rtl1716z53zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2898z00zzsaw_node2rtlz00,
		BgL_bgl_za762predicate17803034za7, BGl_z62predicate1780z62zzsaw_node2rtlz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_globalzd2ze3rtlzd2envze3zzsaw_node2rtlz00,
		BgL_bgl_za762globalza7d2za7e3r3035za7,
		BGl_z62globalzd2ze3rtlz53zzsaw_node2rtlz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_localzd2ze3regzd2envze3zzsaw_node2rtlz00,
		BgL_bgl_za762localza7d2za7e3re3036za7,
		BGl_z62localzd2ze3regz53zzsaw_node2rtlz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_node2rtlz00));
		     ADD_ROOT((void *) (&BGl_retblockzf2tozf2zzsaw_node2rtlz00));
		     ADD_ROOT((void *) (&BGl_reversedz00zzsaw_node2rtlz00));
		     ADD_ROOT((void *) (&BGl_areaz00zzsaw_node2rtlz00));
		     ADD_ROOT((void *) (&BGl_rlocalz00zzsaw_node2rtlz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_node2rtlz00(long
		BgL_checksumz00_6717, char *BgL_fromz00_6718)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_node2rtlz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_node2rtlz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_node2rtlz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_node2rtlz00();
					BGl_cnstzd2initzd2zzsaw_node2rtlz00();
					BGl_importedzd2moduleszd2initz00zzsaw_node2rtlz00();
					BGl_objectzd2initzd2zzsaw_node2rtlz00();
					BGl_genericzd2initzd2zzsaw_node2rtlz00();
					BGl_methodzd2initzd2zzsaw_node2rtlz00();
					return BGl_toplevelzd2initzd2zzsaw_node2rtlz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_node2rtlz00(void)
	{
		{	/* SawMill/node2rtl.scm 1 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_node2rtl");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_node2rtl");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_node2rtl");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_node2rtl");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_node2rtl");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_node2rtl");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"saw_node2rtl");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "saw_node2rtl");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "saw_node2rtl");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_node2rtl");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"saw_node2rtl");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_node2rtl");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_node2rtl");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "saw_node2rtl");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_node2rtlz00(void)
	{
		{	/* SawMill/node2rtl.scm 1 */
			{	/* SawMill/node2rtl.scm 1 */
				obj_t BgL_cportz00_6029;

				{	/* SawMill/node2rtl.scm 1 */
					obj_t BgL_stringz00_6036;

					BgL_stringz00_6036 = BGl_string2944z00zzsaw_node2rtlz00;
					{	/* SawMill/node2rtl.scm 1 */
						obj_t BgL_startz00_6037;

						BgL_startz00_6037 = BINT(0L);
						{	/* SawMill/node2rtl.scm 1 */
							obj_t BgL_endz00_6038;

							BgL_endz00_6038 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_6036)));
							{	/* SawMill/node2rtl.scm 1 */

								BgL_cportz00_6029 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_6036, BgL_startz00_6037, BgL_endz00_6038);
				}}}}
				{
					long BgL_iz00_6030;

					BgL_iz00_6030 = 18L;
				BgL_loopz00_6031:
					if ((BgL_iz00_6030 == -1L))
						{	/* SawMill/node2rtl.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawMill/node2rtl.scm 1 */
							{	/* SawMill/node2rtl.scm 1 */
								obj_t BgL_arg2954z00_6032;

								{	/* SawMill/node2rtl.scm 1 */

									{	/* SawMill/node2rtl.scm 1 */
										obj_t BgL_locationz00_6034;

										BgL_locationz00_6034 = BBOOL(((bool_t) 0));
										{	/* SawMill/node2rtl.scm 1 */

											BgL_arg2954z00_6032 =
												BGl_readz00zz__readerz00(BgL_cportz00_6029,
												BgL_locationz00_6034);
										}
									}
								}
								{	/* SawMill/node2rtl.scm 1 */
									int BgL_tmpz00_6753;

									BgL_tmpz00_6753 = (int) (BgL_iz00_6030);
									CNST_TABLE_SET(BgL_tmpz00_6753, BgL_arg2954z00_6032);
							}}
							{	/* SawMill/node2rtl.scm 1 */
								int BgL_auxz00_6035;

								BgL_auxz00_6035 = (int) ((BgL_iz00_6030 - 1L));
								{
									long BgL_iz00_6758;

									BgL_iz00_6758 = (long) (BgL_auxz00_6035);
									BgL_iz00_6030 = BgL_iz00_6758;
									goto BgL_loopz00_6031;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_node2rtlz00(void)
	{
		{	/* SawMill/node2rtl.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_node2rtlz00(void)
	{
		{	/* SawMill/node2rtl.scm 1 */
			BGl_za2reversezd2callzd2argumentza2z00zzsaw_node2rtlz00 = ((bool_t) 0);
			return BUNSPEC;
		}

	}



/* global->rtl */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_globalzd2ze3rtlz31zzsaw_node2rtlz00(BgL_globalz00_bglt BgL_varz00_96)
	{
		{	/* SawMill/node2rtl.scm 35 */
			{	/* SawMill/node2rtl.scm 36 */
				BgL_areaz00_bglt BgL_az00_2355;

				{	/* SawMill/node2rtl.scm 36 */
					BgL_rtl_returnz00_bglt BgL_arg1843z00_2378;
					obj_t BgL_arg1844z00_2379;

					{	/* SawMill/node2rtl.scm 36 */
						BgL_rtl_returnz00_bglt BgL_new1211z00_2381;

						{	/* SawMill/node2rtl.scm 36 */
							BgL_rtl_returnz00_bglt BgL_new1210z00_2382;

							BgL_new1210z00_2382 =
								((BgL_rtl_returnz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_returnz00_bgl))));
							{	/* SawMill/node2rtl.scm 36 */
								long BgL_arg1846z00_2383;

								BgL_arg1846z00_2383 =
									BGL_CLASS_NUM(BGl_rtl_returnz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1210z00_2382),
									BgL_arg1846z00_2383);
							}
							BgL_new1211z00_2381 = BgL_new1210z00_2382;
						}
						((((BgL_rtl_funz00_bglt) COBJECT(
										((BgL_rtl_funz00_bglt) BgL_new1211z00_2381)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						((((BgL_rtl_returnz00_bglt) COBJECT(BgL_new1211z00_2381))->
								BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_variablez00_bglt)
										COBJECT(((BgL_variablez00_bglt) BgL_varz00_96)))->
									BgL_typez00)), BUNSPEC);
						BgL_arg1843z00_2378 = BgL_new1211z00_2381;
					}
					BgL_arg1844z00_2379 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_varz00_96)))->
										BgL_valuez00))))->BgL_bodyz00);
					{	/* SawMill/node2rtl.scm 36 */
						obj_t BgL_list1845z00_2380;

						BgL_list1845z00_2380 = MAKE_YOUNG_PAIR(BgL_arg1844z00_2379, BNIL);
						BgL_az00_2355 =
							BGl_callza2za2zzsaw_node2rtlz00(BFALSE,
							((BgL_rtl_funz00_bglt) BgL_arg1843z00_2378),
							BgL_list1845z00_2380);
				}}
				{	/* SawMill/node2rtl.scm 39 */
					struct bgl_cell BgL_box3039_6026z00;
					BgL_blockz00_bglt BgL_rz00_2356;
					obj_t BgL_nz00_6026;

					BgL_rz00_2356 =
						(((BgL_areaz00_bglt) COBJECT(BgL_az00_2355))->BgL_entryz00);
					BgL_nz00_6026 = MAKE_CELL_STACK(BINT(0L), BgL_box3039_6026z00);
					BGl_dfsze70ze7zzsaw_node2rtlz00(BgL_nz00_6026,
						((obj_t) BgL_rz00_2356));
					return BgL_rz00_2356;
				}
			}
		}

	}



/* dfs~0 */
	bool_t BGl_dfsze70ze7zzsaw_node2rtlz00(obj_t BgL_nz00_6024,
		obj_t BgL_bz00_2359)
	{
		{	/* SawMill/node2rtl.scm 40 */
			{	/* SawMill/node2rtl.scm 41 */
				BgL_reversedz00_bglt BgL_wide1214z00_2363;

				BgL_wide1214z00_2363 =
					((BgL_reversedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_reversedz00_bgl))));
				{	/* SawMill/node2rtl.scm 41 */
					obj_t BgL_auxz00_6786;
					BgL_objectz00_bglt BgL_tmpz00_6782;

					BgL_auxz00_6786 = ((obj_t) BgL_wide1214z00_2363);
					BgL_tmpz00_6782 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2359)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6782, BgL_auxz00_6786);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2359)));
				{	/* SawMill/node2rtl.scm 41 */
					long BgL_arg1836z00_2364;

					BgL_arg1836z00_2364 = BGL_CLASS_NUM(BGl_reversedz00zzsaw_node2rtlz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_bz00_2359))), BgL_arg1836z00_2364);
				}
				((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2359)));
			}
			((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_2359));
			{	/* SawMill/node2rtl.scm 42 */
				int BgL_vz00_4106;

				BgL_vz00_4106 = CINT(((obj_t) ((obj_t) CELL_REF(BgL_nz00_6024))));
				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_bz00_2359)))->BgL_labelz00) =
					((int) BgL_vz00_4106), BUNSPEC);
			}
			{	/* SawMill/node2rtl.scm 43 */
				obj_t BgL_auxz00_6025;

				BgL_auxz00_6025 =
					ADDFX(((obj_t) ((obj_t) CELL_REF(BgL_nz00_6024))), BINT(1L));
				CELL_SET(BgL_nz00_6024, BgL_auxz00_6025);
			}
			{	/* SawMill/node2rtl.scm 44 */
				obj_t BgL_g1619z00_2366;

				BgL_g1619z00_2366 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_bz00_2359)))->BgL_succsz00);
				{
					obj_t BgL_l1617z00_2368;

					BgL_l1617z00_2368 = BgL_g1619z00_2366;
				BgL_zc3z04anonymousza31837ze3z87_2369:
					if (PAIRP(BgL_l1617z00_2368))
						{	/* SawMill/node2rtl.scm 47 */
							{	/* SawMill/node2rtl.scm 45 */
								obj_t BgL_sz00_2371;

								BgL_sz00_2371 = CAR(BgL_l1617z00_2368);
								{	/* SawMill/node2rtl.scm 45 */
									obj_t BgL_arg1839z00_2372;

									{	/* SawMill/node2rtl.scm 45 */
										obj_t BgL_arg1840z00_2373;

										BgL_arg1840z00_2373 =
											(((BgL_blockz00_bglt) COBJECT(
													((BgL_blockz00_bglt) BgL_sz00_2371)))->BgL_predsz00);
										BgL_arg1839z00_2372 =
											MAKE_YOUNG_PAIR(BgL_bz00_2359, BgL_arg1840z00_2373);
									}
									{	/* SawMill/node2rtl.scm 45 */
										obj_t BgL_vz00_4112;

										BgL_vz00_4112 = BgL_arg1839z00_2372;
										((((BgL_blockz00_bglt) COBJECT(
														((BgL_blockz00_bglt) BgL_sz00_2371)))->
												BgL_predsz00) = ((obj_t) BgL_vz00_4112), BUNSPEC);
									}
								}
								{	/* SawMill/node2rtl.scm 46 */
									bool_t BgL_test3041z00_6819;

									{	/* SawMill/node2rtl.scm 46 */
										obj_t BgL_classz00_4113;

										BgL_classz00_4113 = BGl_reversedz00zzsaw_node2rtlz00;
										if (BGL_OBJECTP(BgL_sz00_2371))
											{	/* SawMill/node2rtl.scm 46 */
												obj_t BgL_oclassz00_4115;

												{	/* SawMill/node2rtl.scm 46 */
													obj_t BgL_arg1815z00_4117;
													long BgL_arg1816z00_4118;

													BgL_arg1815z00_4117 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/node2rtl.scm 46 */
														long BgL_arg1817z00_4119;

														BgL_arg1817z00_4119 =
															BGL_OBJECT_CLASS_NUM(
															((BgL_objectz00_bglt) BgL_sz00_2371));
														BgL_arg1816z00_4118 =
															(BgL_arg1817z00_4119 - OBJECT_TYPE);
													}
													BgL_oclassz00_4115 =
														VECTOR_REF(BgL_arg1815z00_4117,
														BgL_arg1816z00_4118);
												}
												BgL_test3041z00_6819 =
													(BgL_oclassz00_4115 == BgL_classz00_4113);
											}
										else
											{	/* SawMill/node2rtl.scm 46 */
												BgL_test3041z00_6819 = ((bool_t) 0);
											}
									}
									if (BgL_test3041z00_6819)
										{	/* SawMill/node2rtl.scm 46 */
											((bool_t) 0);
										}
									else
										{	/* SawMill/node2rtl.scm 46 */
											BGl_dfsze70ze7zzsaw_node2rtlz00(BgL_nz00_6024,
												BgL_sz00_2371);
										}
								}
							}
							{
								obj_t BgL_l1617z00_6829;

								BgL_l1617z00_6829 = CDR(BgL_l1617z00_2368);
								BgL_l1617z00_2368 = BgL_l1617z00_6829;
								goto BgL_zc3z04anonymousza31837ze3z87_2369;
							}
						}
					else
						{	/* SawMill/node2rtl.scm 47 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* &global->rtl */
	BgL_blockz00_bglt BGl_z62globalzd2ze3rtlz53zzsaw_node2rtlz00(obj_t
		BgL_envz00_5795, obj_t BgL_varz00_5796)
	{
		{	/* SawMill/node2rtl.scm 35 */
			return
				BGl_globalzd2ze3rtlz31zzsaw_node2rtlz00(
				((BgL_globalz00_bglt) BgL_varz00_5796));
		}

	}



/* get-global */
	BgL_globalz00_bglt BGl_getzd2globalzd2zzsaw_node2rtlz00(BgL_globalz00_bglt
		BgL_varz00_97)
	{
		{	/* SawMill/node2rtl.scm 50 */
			BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
				((BgL_variablez00_bglt) BgL_varz00_97));
			if (CBOOL((((BgL_globalz00_bglt) COBJECT(BgL_varz00_97))->BgL_aliasz00)))
				{	/* SawMill/node2rtl.scm 54 */
					obj_t BgL_unaliasz00_2387;

					{	/* SawMill/node2rtl.scm 54 */
						obj_t BgL_arg1850z00_2389;
						obj_t BgL_arg1851z00_2390;

						BgL_arg1850z00_2389 =
							(((BgL_globalz00_bglt) COBJECT(BgL_varz00_97))->BgL_aliasz00);
						BgL_arg1851z00_2390 =
							(((BgL_globalz00_bglt) COBJECT(BgL_varz00_97))->BgL_modulez00);
						{	/* SawMill/node2rtl.scm 54 */
							obj_t BgL_list1852z00_2391;

							BgL_list1852z00_2391 = MAKE_YOUNG_PAIR(BgL_arg1851z00_2390, BNIL);
							BgL_unaliasz00_2387 =
								BGl_findzd2globalzd2zzast_envz00(BgL_arg1850z00_2389,
								BgL_list1852z00_2391);
						}
					}
					{	/* SawMill/node2rtl.scm 55 */
						bool_t BgL_test3044z00_6842;

						{	/* SawMill/node2rtl.scm 55 */
							obj_t BgL_classz00_4126;

							BgL_classz00_4126 = BGl_globalz00zzast_varz00;
							if (BGL_OBJECTP(BgL_unaliasz00_2387))
								{	/* SawMill/node2rtl.scm 55 */
									BgL_objectz00_bglt BgL_arg1807z00_4128;

									BgL_arg1807z00_4128 =
										(BgL_objectz00_bglt) (BgL_unaliasz00_2387);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawMill/node2rtl.scm 55 */
											long BgL_idxz00_4134;

											BgL_idxz00_4134 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4128);
											BgL_test3044z00_6842 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4134 + 2L)) == BgL_classz00_4126);
										}
									else
										{	/* SawMill/node2rtl.scm 55 */
											bool_t BgL_res2822z00_4159;

											{	/* SawMill/node2rtl.scm 55 */
												obj_t BgL_oclassz00_4142;

												{	/* SawMill/node2rtl.scm 55 */
													obj_t BgL_arg1815z00_4150;
													long BgL_arg1816z00_4151;

													BgL_arg1815z00_4150 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/node2rtl.scm 55 */
														long BgL_arg1817z00_4152;

														BgL_arg1817z00_4152 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4128);
														BgL_arg1816z00_4151 =
															(BgL_arg1817z00_4152 - OBJECT_TYPE);
													}
													BgL_oclassz00_4142 =
														VECTOR_REF(BgL_arg1815z00_4150,
														BgL_arg1816z00_4151);
												}
												{	/* SawMill/node2rtl.scm 55 */
													bool_t BgL__ortest_1115z00_4143;

													BgL__ortest_1115z00_4143 =
														(BgL_classz00_4126 == BgL_oclassz00_4142);
													if (BgL__ortest_1115z00_4143)
														{	/* SawMill/node2rtl.scm 55 */
															BgL_res2822z00_4159 = BgL__ortest_1115z00_4143;
														}
													else
														{	/* SawMill/node2rtl.scm 55 */
															long BgL_odepthz00_4144;

															{	/* SawMill/node2rtl.scm 55 */
																obj_t BgL_arg1804z00_4145;

																BgL_arg1804z00_4145 = (BgL_oclassz00_4142);
																BgL_odepthz00_4144 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4145);
															}
															if ((2L < BgL_odepthz00_4144))
																{	/* SawMill/node2rtl.scm 55 */
																	obj_t BgL_arg1802z00_4147;

																	{	/* SawMill/node2rtl.scm 55 */
																		obj_t BgL_arg1803z00_4148;

																		BgL_arg1803z00_4148 = (BgL_oclassz00_4142);
																		BgL_arg1802z00_4147 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4148, 2L);
																	}
																	BgL_res2822z00_4159 =
																		(BgL_arg1802z00_4147 == BgL_classz00_4126);
																}
															else
																{	/* SawMill/node2rtl.scm 55 */
																	BgL_res2822z00_4159 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3044z00_6842 = BgL_res2822z00_4159;
										}
								}
							else
								{	/* SawMill/node2rtl.scm 55 */
									BgL_test3044z00_6842 = ((bool_t) 0);
								}
						}
						if (BgL_test3044z00_6842)
							{	/* SawMill/node2rtl.scm 55 */
								BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
									((BgL_variablez00_bglt) BgL_unaliasz00_2387));
								return ((BgL_globalz00_bglt) BgL_unaliasz00_2387);
							}
						else
							{	/* SawMill/node2rtl.scm 55 */
								return BgL_varz00_97;
							}
					}
				}
			else
				{	/* SawMill/node2rtl.scm 53 */
					return BgL_varz00_97;
				}
		}

	}



/* new-reg */
	BgL_rtl_regz00_bglt BGl_newzd2regzd2zzsaw_node2rtlz00(BgL_nodez00_bglt
		BgL_ez00_98)
	{
		{	/* SawMill/node2rtl.scm 65 */
			{	/* SawMill/node2rtl.scm 66 */
				bool_t BgL_test3049z00_6868;

				{	/* SawMill/node2rtl.scm 66 */
					obj_t BgL_classz00_4160;

					BgL_classz00_4160 = BGl_varz00zzast_nodez00;
					{	/* SawMill/node2rtl.scm 66 */
						BgL_objectz00_bglt BgL_arg1807z00_4162;

						{	/* SawMill/node2rtl.scm 66 */
							obj_t BgL_tmpz00_6869;

							BgL_tmpz00_6869 = ((obj_t) BgL_ez00_98);
							BgL_arg1807z00_4162 = (BgL_objectz00_bglt) (BgL_tmpz00_6869);
						}
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/node2rtl.scm 66 */
								long BgL_idxz00_4168;

								BgL_idxz00_4168 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4162);
								BgL_test3049z00_6868 =
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_4168 + 2L)) == BgL_classz00_4160);
							}
						else
							{	/* SawMill/node2rtl.scm 66 */
								bool_t BgL_res2823z00_4193;

								{	/* SawMill/node2rtl.scm 66 */
									obj_t BgL_oclassz00_4176;

									{	/* SawMill/node2rtl.scm 66 */
										obj_t BgL_arg1815z00_4184;
										long BgL_arg1816z00_4185;

										BgL_arg1815z00_4184 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/node2rtl.scm 66 */
											long BgL_arg1817z00_4186;

											BgL_arg1817z00_4186 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4162);
											BgL_arg1816z00_4185 = (BgL_arg1817z00_4186 - OBJECT_TYPE);
										}
										BgL_oclassz00_4176 =
											VECTOR_REF(BgL_arg1815z00_4184, BgL_arg1816z00_4185);
									}
									{	/* SawMill/node2rtl.scm 66 */
										bool_t BgL__ortest_1115z00_4177;

										BgL__ortest_1115z00_4177 =
											(BgL_classz00_4160 == BgL_oclassz00_4176);
										if (BgL__ortest_1115z00_4177)
											{	/* SawMill/node2rtl.scm 66 */
												BgL_res2823z00_4193 = BgL__ortest_1115z00_4177;
											}
										else
											{	/* SawMill/node2rtl.scm 66 */
												long BgL_odepthz00_4178;

												{	/* SawMill/node2rtl.scm 66 */
													obj_t BgL_arg1804z00_4179;

													BgL_arg1804z00_4179 = (BgL_oclassz00_4176);
													BgL_odepthz00_4178 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_4179);
												}
												if ((2L < BgL_odepthz00_4178))
													{	/* SawMill/node2rtl.scm 66 */
														obj_t BgL_arg1802z00_4181;

														{	/* SawMill/node2rtl.scm 66 */
															obj_t BgL_arg1803z00_4182;

															BgL_arg1803z00_4182 = (BgL_oclassz00_4176);
															BgL_arg1802z00_4181 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4182,
																2L);
														}
														BgL_res2823z00_4193 =
															(BgL_arg1802z00_4181 == BgL_classz00_4160);
													}
												else
													{	/* SawMill/node2rtl.scm 66 */
														BgL_res2823z00_4193 = ((bool_t) 0);
													}
											}
									}
								}
								BgL_test3049z00_6868 = BgL_res2823z00_4193;
							}
					}
				}
				if (BgL_test3049z00_6868)
					{	/* SawMill/node2rtl.scm 67 */
						bool_t BgL_test3053z00_6891;

						{	/* SawMill/node2rtl.scm 67 */
							BgL_valuez00_bglt BgL_arg1868z00_2409;

							BgL_arg1868z00_2409 =
								(((BgL_variablez00_bglt) COBJECT(
										(((BgL_varz00_bglt) COBJECT(
													((BgL_varz00_bglt) BgL_ez00_98)))->
											BgL_variablez00)))->BgL_valuez00);
							{	/* SawMill/node2rtl.scm 67 */
								obj_t BgL_classz00_4196;

								BgL_classz00_4196 = BGl_sfunz00zzast_varz00;
								{	/* SawMill/node2rtl.scm 67 */
									BgL_objectz00_bglt BgL_arg1807z00_4198;

									{	/* SawMill/node2rtl.scm 67 */
										obj_t BgL_tmpz00_6895;

										BgL_tmpz00_6895 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1868z00_2409));
										BgL_arg1807z00_4198 =
											(BgL_objectz00_bglt) (BgL_tmpz00_6895);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawMill/node2rtl.scm 67 */
											long BgL_idxz00_4204;

											BgL_idxz00_4204 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4198);
											BgL_test3053z00_6891 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4204 + 3L)) == BgL_classz00_4196);
										}
									else
										{	/* SawMill/node2rtl.scm 67 */
											bool_t BgL_res2824z00_4229;

											{	/* SawMill/node2rtl.scm 67 */
												obj_t BgL_oclassz00_4212;

												{	/* SawMill/node2rtl.scm 67 */
													obj_t BgL_arg1815z00_4220;
													long BgL_arg1816z00_4221;

													BgL_arg1815z00_4220 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/node2rtl.scm 67 */
														long BgL_arg1817z00_4222;

														BgL_arg1817z00_4222 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4198);
														BgL_arg1816z00_4221 =
															(BgL_arg1817z00_4222 - OBJECT_TYPE);
													}
													BgL_oclassz00_4212 =
														VECTOR_REF(BgL_arg1815z00_4220,
														BgL_arg1816z00_4221);
												}
												{	/* SawMill/node2rtl.scm 67 */
													bool_t BgL__ortest_1115z00_4213;

													BgL__ortest_1115z00_4213 =
														(BgL_classz00_4196 == BgL_oclassz00_4212);
													if (BgL__ortest_1115z00_4213)
														{	/* SawMill/node2rtl.scm 67 */
															BgL_res2824z00_4229 = BgL__ortest_1115z00_4213;
														}
													else
														{	/* SawMill/node2rtl.scm 67 */
															long BgL_odepthz00_4214;

															{	/* SawMill/node2rtl.scm 67 */
																obj_t BgL_arg1804z00_4215;

																BgL_arg1804z00_4215 = (BgL_oclassz00_4212);
																BgL_odepthz00_4214 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4215);
															}
															if ((3L < BgL_odepthz00_4214))
																{	/* SawMill/node2rtl.scm 67 */
																	obj_t BgL_arg1802z00_4217;

																	{	/* SawMill/node2rtl.scm 67 */
																		obj_t BgL_arg1803z00_4218;

																		BgL_arg1803z00_4218 = (BgL_oclassz00_4212);
																		BgL_arg1802z00_4217 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4218, 3L);
																	}
																	BgL_res2824z00_4229 =
																		(BgL_arg1802z00_4217 == BgL_classz00_4196);
																}
															else
																{	/* SawMill/node2rtl.scm 67 */
																	BgL_res2824z00_4229 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3053z00_6891 = BgL_res2824z00_4229;
										}
								}
							}
						}
						if (BgL_test3053z00_6891)
							{	/* SawMill/node2rtl.scm 68 */
								BgL_rtl_regz00_bglt BgL_new1218z00_2396;

								{	/* SawMill/node2rtl.scm 69 */
									BgL_rtl_regz00_bglt BgL_new1217z00_2400;

									BgL_new1217z00_2400 =
										((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_rtl_regz00_bgl))));
									{	/* SawMill/node2rtl.scm 69 */
										long BgL_arg1860z00_2401;

										BgL_arg1860z00_2401 =
											BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1217z00_2400),
											BgL_arg1860z00_2401);
									}
									{	/* SawMill/node2rtl.scm 69 */
										BgL_objectz00_bglt BgL_tmpz00_6922;

										BgL_tmpz00_6922 =
											((BgL_objectz00_bglt) BgL_new1217z00_2400);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6922, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1217z00_2400);
									BgL_new1218z00_2396 = BgL_new1217z00_2400;
								}
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1218z00_2396))->
										BgL_typez00) =
									((BgL_typez00_bglt)
										BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(0))),
									BUNSPEC);
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1218z00_2396))->
										BgL_varz00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1218z00_2396))->
										BgL_onexprzf3zf3) = ((obj_t) BFALSE), BUNSPEC);
								{
									obj_t BgL_auxz00_6931;

									{	/* SawMill/node2rtl.scm 71 */

										{	/* SawMill/node2rtl.scm 71 */

											BgL_auxz00_6931 =
												BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
									}}
									((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1218z00_2396))->
											BgL_namez00) = ((obj_t) BgL_auxz00_6931), BUNSPEC);
								}
								{
									obj_t BgL_auxz00_6934;

									{	/* SawMill/node2rtl.scm 71 */
										obj_t BgL_arg1858z00_2398;

										{	/* SawMill/node2rtl.scm 71 */
											obj_t BgL_arg1859z00_2399;

											{	/* SawMill/node2rtl.scm 71 */
												obj_t BgL_classz00_4234;

												BgL_classz00_4234 = BGl_rtl_regz00zzsaw_defsz00;
												BgL_arg1859z00_2399 =
													BGL_CLASS_ALL_FIELDS(BgL_classz00_4234);
											}
											BgL_arg1858z00_2398 = VECTOR_REF(BgL_arg1859z00_2399, 4L);
										}
										BgL_auxz00_6934 =
											BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
											(BgL_arg1858z00_2398);
									}
									((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1218z00_2396))->
											BgL_keyz00) = ((obj_t) BgL_auxz00_6934), BUNSPEC);
								}
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1218z00_2396))->
										BgL_debugnamez00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1218z00_2396))->
										BgL_hardwarez00) = ((obj_t) BFALSE), BUNSPEC);
								return BgL_new1218z00_2396;
							}
						else
							{	/* SawMill/node2rtl.scm 72 */
								BgL_rtl_regz00_bglt BgL_new1220z00_2402;

								{	/* SawMill/node2rtl.scm 73 */
									BgL_rtl_regz00_bglt BgL_new1219z00_2407;

									BgL_new1219z00_2407 =
										((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_rtl_regz00_bgl))));
									{	/* SawMill/node2rtl.scm 73 */
										long BgL_arg1866z00_2408;

										BgL_arg1866z00_2408 =
											BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1219z00_2407),
											BgL_arg1866z00_2408);
									}
									{	/* SawMill/node2rtl.scm 73 */
										BgL_objectz00_bglt BgL_tmpz00_6945;

										BgL_tmpz00_6945 =
											((BgL_objectz00_bglt) BgL_new1219z00_2407);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6945, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1219z00_2407);
									BgL_new1220z00_2402 = BgL_new1219z00_2407;
								}
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1220z00_2402))->
										BgL_typez00) =
									((BgL_typez00_bglt) (((BgL_variablez00_bglt)
												COBJECT((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																	BgL_ez00_98)))->BgL_variablez00)))->
											BgL_typez00)), BUNSPEC);
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1220z00_2402))->
										BgL_varz00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1220z00_2402))->
										BgL_onexprzf3zf3) = ((obj_t) BFALSE), BUNSPEC);
								{
									obj_t BgL_auxz00_6955;

									{	/* SawMill/node2rtl.scm 75 */

										{	/* SawMill/node2rtl.scm 75 */

											BgL_auxz00_6955 =
												BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
									}}
									((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1220z00_2402))->
											BgL_namez00) = ((obj_t) BgL_auxz00_6955), BUNSPEC);
								}
								{
									obj_t BgL_auxz00_6958;

									{	/* SawMill/node2rtl.scm 75 */
										obj_t BgL_arg1863z00_2405;

										{	/* SawMill/node2rtl.scm 75 */
											obj_t BgL_arg1864z00_2406;

											{	/* SawMill/node2rtl.scm 75 */
												obj_t BgL_classz00_4242;

												BgL_classz00_4242 = BGl_rtl_regz00zzsaw_defsz00;
												BgL_arg1864z00_2406 =
													BGL_CLASS_ALL_FIELDS(BgL_classz00_4242);
											}
											BgL_arg1863z00_2405 = VECTOR_REF(BgL_arg1864z00_2406, 4L);
										}
										BgL_auxz00_6958 =
											BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
											(BgL_arg1863z00_2405);
									}
									((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1220z00_2402))->
											BgL_keyz00) = ((obj_t) BgL_auxz00_6958), BUNSPEC);
								}
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1220z00_2402))->
										BgL_debugnamez00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1220z00_2402))->
										BgL_hardwarez00) = ((obj_t) BFALSE), BUNSPEC);
								return BgL_new1220z00_2402;
							}
					}
				else
					{	/* SawMill/node2rtl.scm 76 */
						BgL_rtl_regz00_bglt BgL_new1222z00_2411;

						{	/* SawMill/node2rtl.scm 76 */
							BgL_rtl_regz00_bglt BgL_new1221z00_2415;

							BgL_new1221z00_2415 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/node2rtl.scm 76 */
								long BgL_arg1873z00_2416;

								BgL_arg1873z00_2416 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1221z00_2415),
									BgL_arg1873z00_2416);
							}
							{	/* SawMill/node2rtl.scm 76 */
								BgL_objectz00_bglt BgL_tmpz00_6969;

								BgL_tmpz00_6969 = ((BgL_objectz00_bglt) BgL_new1221z00_2415);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6969, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1221z00_2415);
							BgL_new1222z00_2411 = BgL_new1221z00_2415;
						}
						((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1222z00_2411))->
								BgL_typez00) =
							((BgL_typez00_bglt) BGl_getzd2typezd2zztype_typeofz00(BgL_ez00_98,
									((bool_t) 0))), BUNSPEC);
						((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1222z00_2411))->
								BgL_varz00) = ((obj_t) BFALSE), BUNSPEC);
						((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1222z00_2411))->
								BgL_onexprzf3zf3) = ((obj_t) BFALSE), BUNSPEC);
						{
							obj_t BgL_auxz00_6977;

							{	/* SawMill/node2rtl.scm 76 */

								{	/* SawMill/node2rtl.scm 76 */

									BgL_auxz00_6977 = BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
							}}
							((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1222z00_2411))->
									BgL_namez00) = ((obj_t) BgL_auxz00_6977), BUNSPEC);
						}
						{
							obj_t BgL_auxz00_6980;

							{	/* SawMill/node2rtl.scm 76 */
								obj_t BgL_arg1870z00_2413;

								{	/* SawMill/node2rtl.scm 76 */
									obj_t BgL_arg1872z00_2414;

									{	/* SawMill/node2rtl.scm 76 */
										obj_t BgL_classz00_4248;

										BgL_classz00_4248 = BGl_rtl_regz00zzsaw_defsz00;
										BgL_arg1872z00_2414 =
											BGL_CLASS_ALL_FIELDS(BgL_classz00_4248);
									}
									BgL_arg1870z00_2413 = VECTOR_REF(BgL_arg1872z00_2414, 4L);
								}
								BgL_auxz00_6980 =
									BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
									(BgL_arg1870z00_2413);
							}
							((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1222z00_2411))->
									BgL_keyz00) = ((obj_t) BgL_auxz00_6980), BUNSPEC);
						}
						((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1222z00_2411))->
								BgL_debugnamez00) = ((obj_t) BFALSE), BUNSPEC);
						((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1222z00_2411))->
								BgL_hardwarez00) = ((obj_t) BFALSE), BUNSPEC);
						return BgL_new1222z00_2411;
					}
			}
		}

	}



/* new-ureg */
	BgL_rtl_regz00_bglt BGl_newzd2uregzd2zzsaw_node2rtlz00(BgL_localz00_bglt
		BgL_varz00_99)
	{
		{	/* SawMill/node2rtl.scm 78 */
			{	/* SawMill/node2rtl.scm 79 */
				BgL_rtl_regz00_bglt BgL_new1224z00_2417;

				{	/* SawMill/node2rtl.scm 79 */
					BgL_rtl_regz00_bglt BgL_new1223z00_2422;

					BgL_new1223z00_2422 =
						((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regz00_bgl))));
					{	/* SawMill/node2rtl.scm 79 */
						long BgL_arg1878z00_2423;

						BgL_arg1878z00_2423 = BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1223z00_2422), BgL_arg1878z00_2423);
					}
					{	/* SawMill/node2rtl.scm 79 */
						BgL_objectz00_bglt BgL_tmpz00_6991;

						BgL_tmpz00_6991 = ((BgL_objectz00_bglt) BgL_new1223z00_2422);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6991, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1223z00_2422);
					BgL_new1224z00_2417 = BgL_new1223z00_2422;
				}
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1224z00_2417))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_variablez00_bglt)
								COBJECT(((BgL_variablez00_bglt) BgL_varz00_99)))->BgL_typez00)),
					BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1224z00_2417))->BgL_varz00) =
					((obj_t) ((obj_t) BgL_varz00_99)), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1224z00_2417))->
						BgL_onexprzf3zf3) = ((obj_t) BFALSE), BUNSPEC);
				{
					obj_t BgL_auxz00_7001;

					{	/* SawMill/node2rtl.scm 79 */
						obj_t BgL_arg1874z00_2418;

						{	/* SawMill/node2rtl.scm 79 */
							obj_t BgL_arg1875z00_2419;

							{	/* SawMill/node2rtl.scm 79 */
								obj_t BgL_classz00_4255;

								BgL_classz00_4255 = BGl_rtl_regz00zzsaw_defsz00;
								BgL_arg1875z00_2419 = BGL_CLASS_ALL_FIELDS(BgL_classz00_4255);
							}
							BgL_arg1874z00_2418 = VECTOR_REF(BgL_arg1875z00_2419, 3L);
						}
						BgL_auxz00_7001 =
							BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
							(BgL_arg1874z00_2418);
					}
					((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1224z00_2417))->BgL_namez00) =
						((obj_t) BgL_auxz00_7001), BUNSPEC);
				}
				{
					obj_t BgL_auxz00_7006;

					{	/* SawMill/node2rtl.scm 79 */
						obj_t BgL_arg1876z00_2420;

						{	/* SawMill/node2rtl.scm 79 */
							obj_t BgL_arg1877z00_2421;

							{	/* SawMill/node2rtl.scm 79 */
								obj_t BgL_classz00_4257;

								BgL_classz00_4257 = BGl_rtl_regz00zzsaw_defsz00;
								BgL_arg1877z00_2421 = BGL_CLASS_ALL_FIELDS(BgL_classz00_4257);
							}
							BgL_arg1876z00_2420 = VECTOR_REF(BgL_arg1877z00_2421, 4L);
						}
						BgL_auxz00_7006 =
							BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
							(BgL_arg1876z00_2420);
					}
					((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1224z00_2417))->BgL_keyz00) =
						((obj_t) BgL_auxz00_7006), BUNSPEC);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1224z00_2417))->
						BgL_debugnamez00) = ((obj_t) BFALSE), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1224z00_2417))->
						BgL_hardwarez00) = ((obj_t) BFALSE), BUNSPEC);
				return BgL_new1224z00_2417;
			}
		}

	}



/* local->reg */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_localzd2ze3regz31zzsaw_node2rtlz00(BgL_localz00_bglt BgL_varz00_100)
	{
		{	/* SawMill/node2rtl.scm 81 */
			{	/* SawMill/node2rtl.scm 82 */
				bool_t BgL_test3057z00_7013;

				{	/* SawMill/node2rtl.scm 82 */
					obj_t BgL_classz00_4259;

					BgL_classz00_4259 = BGl_rlocalz00zzsaw_node2rtlz00;
					{	/* SawMill/node2rtl.scm 82 */
						obj_t BgL_oclassz00_4261;

						{	/* SawMill/node2rtl.scm 82 */
							obj_t BgL_arg1815z00_4263;
							long BgL_arg1816z00_4264;

							BgL_arg1815z00_4263 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/node2rtl.scm 82 */
								long BgL_arg1817z00_4265;

								BgL_arg1817z00_4265 =
									BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_varz00_100));
								BgL_arg1816z00_4264 = (BgL_arg1817z00_4265 - OBJECT_TYPE);
							}
							BgL_oclassz00_4261 =
								VECTOR_REF(BgL_arg1815z00_4263, BgL_arg1816z00_4264);
						}
						BgL_test3057z00_7013 = (BgL_oclassz00_4261 == BgL_classz00_4259);
				}}
				if (BgL_test3057z00_7013)
					{
						obj_t BgL_auxz00_7020;

						{
							BgL_rlocalz00_bglt BgL_auxz00_7021;

							{
								obj_t BgL_auxz00_7022;

								{	/* SawMill/node2rtl.scm 83 */
									BgL_objectz00_bglt BgL_tmpz00_7023;

									BgL_tmpz00_7023 =
										((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_varz00_100));
									BgL_auxz00_7022 = BGL_OBJECT_WIDENING(BgL_tmpz00_7023);
								}
								BgL_auxz00_7021 = ((BgL_rlocalz00_bglt) BgL_auxz00_7022);
							}
							BgL_auxz00_7020 =
								(((BgL_rlocalz00_bglt) COBJECT(BgL_auxz00_7021))->BgL_regz00);
						}
						return ((BgL_rtl_regz00_bglt) BgL_auxz00_7020);
					}
				else
					{	/* SawMill/node2rtl.scm 84 */
						BgL_rtl_regz00_bglt BgL_rz00_2425;

						BgL_rz00_2425 = BGl_newzd2uregzd2zzsaw_node2rtlz00(BgL_varz00_100);
						{	/* SawMill/node2rtl.scm 85 */
							BgL_rlocalz00_bglt BgL_wide1227z00_2428;

							BgL_wide1227z00_2428 =
								((BgL_rlocalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rlocalz00_bgl))));
							{	/* SawMill/node2rtl.scm 85 */
								obj_t BgL_auxz00_7035;
								BgL_objectz00_bglt BgL_tmpz00_7032;

								BgL_auxz00_7035 = ((obj_t) BgL_wide1227z00_2428);
								BgL_tmpz00_7032 =
									((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_varz00_100));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7032, BgL_auxz00_7035);
							}
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_varz00_100));
							{	/* SawMill/node2rtl.scm 85 */
								long BgL_arg1880z00_2429;

								BgL_arg1880z00_2429 =
									BGL_CLASS_NUM(BGl_rlocalz00zzsaw_node2rtlz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_varz00_100)), BgL_arg1880z00_2429);
							}
							((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_varz00_100));
						}
						{
							BgL_rlocalz00_bglt BgL_auxz00_7046;

							{
								obj_t BgL_auxz00_7047;

								{	/* SawMill/node2rtl.scm 85 */
									BgL_objectz00_bglt BgL_tmpz00_7048;

									BgL_tmpz00_7048 =
										((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_varz00_100));
									BgL_auxz00_7047 = BGL_OBJECT_WIDENING(BgL_tmpz00_7048);
								}
								BgL_auxz00_7046 = ((BgL_rlocalz00_bglt) BgL_auxz00_7047);
							}
							((((BgL_rlocalz00_bglt) COBJECT(BgL_auxz00_7046))->BgL_regz00) =
								((obj_t) ((obj_t) BgL_rz00_2425)), BUNSPEC);
						}
						{
							BgL_rlocalz00_bglt BgL_auxz00_7055;

							{
								obj_t BgL_auxz00_7056;

								{	/* SawMill/node2rtl.scm 85 */
									BgL_objectz00_bglt BgL_tmpz00_7057;

									BgL_tmpz00_7057 =
										((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_varz00_100));
									BgL_auxz00_7056 = BGL_OBJECT_WIDENING(BgL_tmpz00_7057);
								}
								BgL_auxz00_7055 = ((BgL_rlocalz00_bglt) BgL_auxz00_7056);
							}
							((((BgL_rlocalz00_bglt) COBJECT(BgL_auxz00_7055))->BgL_codez00) =
								((obj_t) BFALSE), BUNSPEC);
						}
						((BgL_localz00_bglt) BgL_varz00_100);
						return BgL_rz00_2425;
					}
			}
		}

	}



/* &local->reg */
	BgL_rtl_regz00_bglt BGl_z62localzd2ze3regz53zzsaw_node2rtlz00(obj_t
		BgL_envz00_5797, obj_t BgL_varz00_5798)
	{
		{	/* SawMill/node2rtl.scm 81 */
			return
				BGl_localzd2ze3regz31zzsaw_node2rtlz00(
				((BgL_localz00_bglt) BgL_varz00_5798));
		}

	}



/* bdestination! */
	obj_t BGl_bdestinationz12z12zzsaw_node2rtlz00(BgL_blockz00_bglt BgL_bz00_103,
		BgL_rtl_regz00_bglt BgL_regz00_104)
	{
		{	/* SawMill/node2rtl.scm 94 */
			{	/* SawMill/node2rtl.scm 95 */
				obj_t BgL_insz00_2433;

				BgL_insz00_2433 =
					CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
						(((BgL_blockz00_bglt) COBJECT(BgL_bz00_103))->BgL_firstz00)));
				{	/* SawMill/node2rtl.scm 96 */
					bool_t BgL_test3058z00_7069;

					{	/* SawMill/node2rtl.scm 96 */
						bool_t BgL_test3059z00_7070;

						{	/* SawMill/node2rtl.scm 96 */
							BgL_rtl_funz00_bglt BgL_arg1897z00_2447;

							BgL_arg1897z00_2447 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_insz00_2433)))->BgL_funz00);
							{	/* SawMill/node2rtl.scm 96 */
								obj_t BgL_classz00_4287;

								BgL_classz00_4287 = BGl_rtl_movz00zzsaw_defsz00;
								{	/* SawMill/node2rtl.scm 96 */
									BgL_objectz00_bglt BgL_arg1807z00_4289;

									{	/* SawMill/node2rtl.scm 96 */
										obj_t BgL_tmpz00_7073;

										BgL_tmpz00_7073 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1897z00_2447));
										BgL_arg1807z00_4289 =
											(BgL_objectz00_bglt) (BgL_tmpz00_7073);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawMill/node2rtl.scm 96 */
											long BgL_idxz00_4295;

											BgL_idxz00_4295 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4289);
											BgL_test3059z00_7070 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4295 + 3L)) == BgL_classz00_4287);
										}
									else
										{	/* SawMill/node2rtl.scm 96 */
											bool_t BgL_res2825z00_4320;

											{	/* SawMill/node2rtl.scm 96 */
												obj_t BgL_oclassz00_4303;

												{	/* SawMill/node2rtl.scm 96 */
													obj_t BgL_arg1815z00_4311;
													long BgL_arg1816z00_4312;

													BgL_arg1815z00_4311 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/node2rtl.scm 96 */
														long BgL_arg1817z00_4313;

														BgL_arg1817z00_4313 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4289);
														BgL_arg1816z00_4312 =
															(BgL_arg1817z00_4313 - OBJECT_TYPE);
													}
													BgL_oclassz00_4303 =
														VECTOR_REF(BgL_arg1815z00_4311,
														BgL_arg1816z00_4312);
												}
												{	/* SawMill/node2rtl.scm 96 */
													bool_t BgL__ortest_1115z00_4304;

													BgL__ortest_1115z00_4304 =
														(BgL_classz00_4287 == BgL_oclassz00_4303);
													if (BgL__ortest_1115z00_4304)
														{	/* SawMill/node2rtl.scm 96 */
															BgL_res2825z00_4320 = BgL__ortest_1115z00_4304;
														}
													else
														{	/* SawMill/node2rtl.scm 96 */
															long BgL_odepthz00_4305;

															{	/* SawMill/node2rtl.scm 96 */
																obj_t BgL_arg1804z00_4306;

																BgL_arg1804z00_4306 = (BgL_oclassz00_4303);
																BgL_odepthz00_4305 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4306);
															}
															if ((3L < BgL_odepthz00_4305))
																{	/* SawMill/node2rtl.scm 96 */
																	obj_t BgL_arg1802z00_4308;

																	{	/* SawMill/node2rtl.scm 96 */
																		obj_t BgL_arg1803z00_4309;

																		BgL_arg1803z00_4309 = (BgL_oclassz00_4303);
																		BgL_arg1802z00_4308 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4309, 3L);
																	}
																	BgL_res2825z00_4320 =
																		(BgL_arg1802z00_4308 == BgL_classz00_4287);
																}
															else
																{	/* SawMill/node2rtl.scm 96 */
																	BgL_res2825z00_4320 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3059z00_7070 = BgL_res2825z00_4320;
										}
								}
							}
						}
						if (BgL_test3059z00_7070)
							{	/* SawMill/node2rtl.scm 97 */
								bool_t BgL_test3063z00_7096;

								{	/* SawMill/node2rtl.scm 97 */
									obj_t BgL_tmpz00_7097;

									{	/* SawMill/node2rtl.scm 97 */
										obj_t BgL_pairz00_4322;

										BgL_pairz00_4322 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_insz00_2433)))->
											BgL_argsz00);
										BgL_tmpz00_7097 = CAR(BgL_pairz00_4322);
									}
									BgL_test3063z00_7096 =
										(BgL_tmpz00_7097 == ((obj_t) BgL_regz00_104));
								}
								if (BgL_test3063z00_7096)
									{	/* SawMill/node2rtl.scm 97 */
										BgL_test3058z00_7069 = ((bool_t) 0);
									}
								else
									{	/* SawMill/node2rtl.scm 97 */
										BgL_test3058z00_7069 = ((bool_t) 1);
									}
							}
						else
							{	/* SawMill/node2rtl.scm 96 */
								BgL_test3058z00_7069 = ((bool_t) 1);
							}
					}
					if (BgL_test3058z00_7069)
						{	/* SawMill/node2rtl.scm 96 */
							return
								((((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_insz00_2433)))->BgL_destz00) =
								((obj_t) ((obj_t) BgL_regz00_104)), BUNSPEC);
						}
					else
						{	/* SawMill/node2rtl.scm 96 */
							return BFALSE;
						}
				}
			}
		}

	}



/* single* */
	BgL_areaz00_bglt BGl_singleza2za2zzsaw_node2rtlz00(obj_t BgL_ez00_105,
		BgL_rtl_funz00_bglt BgL_funz00_106, obj_t BgL_argsz00_107)
	{
		{	/* SawMill/node2rtl.scm 103 */
			{	/* SawMill/node2rtl.scm 104 */
				BgL_rtl_insz00_bglt BgL_insz00_2450;

				{	/* SawMill/node2rtl.scm 104 */
					BgL_rtl_insz00_bglt BgL_new1230z00_2462;

					{	/* SawMill/node2rtl.scm 104 */
						BgL_rtl_insz00_bglt BgL_new1229z00_2463;

						BgL_new1229z00_2463 =
							((BgL_rtl_insz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_insz00_bgl))));
						{	/* SawMill/node2rtl.scm 104 */
							long BgL_arg1906z00_2464;

							BgL_arg1906z00_2464 = BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1229z00_2463),
								BgL_arg1906z00_2464);
						}
						{	/* SawMill/node2rtl.scm 104 */
							BgL_objectz00_bglt BgL_tmpz00_7110;

							BgL_tmpz00_7110 = ((BgL_objectz00_bglt) BgL_new1229z00_2463);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7110, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1229z00_2463);
						BgL_new1230z00_2462 = BgL_new1229z00_2463;
					}
					((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1230z00_2462))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1230z00_2462))->
							BgL_z52spillz52) = ((obj_t) BNIL), BUNSPEC);
					((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1230z00_2462))->BgL_destz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1230z00_2462))->BgL_funz00) =
						((BgL_rtl_funz00_bglt) BgL_funz00_106), BUNSPEC);
					((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1230z00_2462))->BgL_argsz00) =
						((obj_t) BgL_argsz00_107), BUNSPEC);
					BgL_insz00_2450 = BgL_new1230z00_2462;
				}
				{	/* SawMill/node2rtl.scm 105 */
					bool_t BgL_test3064z00_7119;

					{	/* SawMill/node2rtl.scm 105 */
						obj_t BgL_classz00_4328;

						BgL_classz00_4328 = BGl_nodez00zzast_nodez00;
						if (BGL_OBJECTP(BgL_ez00_105))
							{	/* SawMill/node2rtl.scm 105 */
								BgL_objectz00_bglt BgL_arg1807z00_4330;

								BgL_arg1807z00_4330 = (BgL_objectz00_bglt) (BgL_ez00_105);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawMill/node2rtl.scm 105 */
										long BgL_idxz00_4336;

										BgL_idxz00_4336 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4330);
										BgL_test3064z00_7119 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4336 + 1L)) == BgL_classz00_4328);
									}
								else
									{	/* SawMill/node2rtl.scm 105 */
										bool_t BgL_res2826z00_4361;

										{	/* SawMill/node2rtl.scm 105 */
											obj_t BgL_oclassz00_4344;

											{	/* SawMill/node2rtl.scm 105 */
												obj_t BgL_arg1815z00_4352;
												long BgL_arg1816z00_4353;

												BgL_arg1815z00_4352 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawMill/node2rtl.scm 105 */
													long BgL_arg1817z00_4354;

													BgL_arg1817z00_4354 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4330);
													BgL_arg1816z00_4353 =
														(BgL_arg1817z00_4354 - OBJECT_TYPE);
												}
												BgL_oclassz00_4344 =
													VECTOR_REF(BgL_arg1815z00_4352, BgL_arg1816z00_4353);
											}
											{	/* SawMill/node2rtl.scm 105 */
												bool_t BgL__ortest_1115z00_4345;

												BgL__ortest_1115z00_4345 =
													(BgL_classz00_4328 == BgL_oclassz00_4344);
												if (BgL__ortest_1115z00_4345)
													{	/* SawMill/node2rtl.scm 105 */
														BgL_res2826z00_4361 = BgL__ortest_1115z00_4345;
													}
												else
													{	/* SawMill/node2rtl.scm 105 */
														long BgL_odepthz00_4346;

														{	/* SawMill/node2rtl.scm 105 */
															obj_t BgL_arg1804z00_4347;

															BgL_arg1804z00_4347 = (BgL_oclassz00_4344);
															BgL_odepthz00_4346 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4347);
														}
														if ((1L < BgL_odepthz00_4346))
															{	/* SawMill/node2rtl.scm 105 */
																obj_t BgL_arg1802z00_4349;

																{	/* SawMill/node2rtl.scm 105 */
																	obj_t BgL_arg1803z00_4350;

																	BgL_arg1803z00_4350 = (BgL_oclassz00_4344);
																	BgL_arg1802z00_4349 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4350,
																		1L);
																}
																BgL_res2826z00_4361 =
																	(BgL_arg1802z00_4349 == BgL_classz00_4328);
															}
														else
															{	/* SawMill/node2rtl.scm 105 */
																BgL_res2826z00_4361 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3064z00_7119 = BgL_res2826z00_4361;
									}
							}
						else
							{	/* SawMill/node2rtl.scm 105 */
								BgL_test3064z00_7119 = ((bool_t) 0);
							}
					}
					if (BgL_test3064z00_7119)
						{	/* SawMill/node2rtl.scm 105 */
							{	/* SawMill/node2rtl.scm 106 */
								obj_t BgL_arg1901z00_2452;

								BgL_arg1901z00_2452 =
									(((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_ez00_105)))->BgL_locz00);
								((((BgL_rtl_funz00_bglt) COBJECT(BgL_funz00_106))->BgL_locz00) =
									((obj_t) BgL_arg1901z00_2452), BUNSPEC);
							}
							{	/* SawMill/node2rtl.scm 107 */
								obj_t BgL_arg1902z00_2453;

								BgL_arg1902z00_2453 =
									(((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_ez00_105)))->BgL_locz00);
								((((BgL_rtl_insz00_bglt) COBJECT(BgL_insz00_2450))->
										BgL_locz00) = ((obj_t) BgL_arg1902z00_2453), BUNSPEC);
							}
						}
					else
						{	/* SawMill/node2rtl.scm 105 */
							BFALSE;
						}
				}
				{	/* SawMill/node2rtl.scm 108 */
					obj_t BgL_lz00_2454;

					BgL_lz00_2454 = MAKE_YOUNG_PAIR(((obj_t) BgL_insz00_2450), BNIL);
					{	/* SawMill/node2rtl.scm 109 */
						BgL_blockz00_bglt BgL_bz00_2455;

						{	/* SawMill/node2rtl.scm 109 */
							BgL_blockz00_bglt BgL_new1232z00_2459;

							{	/* SawMill/node2rtl.scm 109 */
								BgL_blockz00_bglt BgL_new1231z00_2460;

								BgL_new1231z00_2460 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawMill/node2rtl.scm 109 */
									long BgL_arg1904z00_2461;

									BgL_arg1904z00_2461 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1231z00_2460),
										BgL_arg1904z00_2461);
								}
								{	/* SawMill/node2rtl.scm 109 */
									BgL_objectz00_bglt BgL_tmpz00_7154;

									BgL_tmpz00_7154 = ((BgL_objectz00_bglt) BgL_new1231z00_2460);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7154, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1231z00_2460);
								BgL_new1232z00_2459 = BgL_new1231z00_2460;
							}
							((((BgL_blockz00_bglt) COBJECT(BgL_new1232z00_2459))->
									BgL_labelz00) = ((int) (int) (0L)), BUNSPEC);
							((((BgL_blockz00_bglt) COBJECT(BgL_new1232z00_2459))->
									BgL_predsz00) = ((obj_t) BNIL), BUNSPEC);
							((((BgL_blockz00_bglt) COBJECT(BgL_new1232z00_2459))->
									BgL_succsz00) = ((obj_t) BNIL), BUNSPEC);
							((((BgL_blockz00_bglt) COBJECT(BgL_new1232z00_2459))->
									BgL_firstz00) = ((obj_t) BgL_lz00_2454), BUNSPEC);
							BgL_bz00_2455 = BgL_new1232z00_2459;
						}
						{	/* SawMill/node2rtl.scm 110 */
							BgL_areaz00_bglt BgL_new1234z00_2456;

							{	/* SawMill/node2rtl.scm 110 */
								BgL_areaz00_bglt BgL_new1233z00_2457;

								BgL_new1233z00_2457 =
									((BgL_areaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_areaz00_bgl))));
								{	/* SawMill/node2rtl.scm 110 */
									long BgL_arg1903z00_2458;

									BgL_arg1903z00_2458 =
										BGL_CLASS_NUM(BGl_areaz00zzsaw_node2rtlz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1233z00_2457),
										BgL_arg1903z00_2458);
								}
								BgL_new1234z00_2456 = BgL_new1233z00_2457;
							}
							((((BgL_areaz00_bglt) COBJECT(BgL_new1234z00_2456))->
									BgL_entryz00) = ((BgL_blockz00_bglt) BgL_bz00_2455), BUNSPEC);
							((((BgL_areaz00_bglt) COBJECT(BgL_new1234z00_2456))->
									BgL_exitz00) = ((BgL_blockz00_bglt) BgL_bz00_2455), BUNSPEC);
							return BgL_new1234z00_2456;
						}
					}
				}
			}
		}

	}



/* unlink */
	BgL_areaz00_bglt BGl_unlinkz00zzsaw_node2rtlz00(BgL_areaz00_bglt BgL_az00_113)
	{
		{	/* SawMill/node2rtl.scm 118 */
			{	/* SawMill/node2rtl.scm 119 */
				BgL_areaz00_bglt BgL_new1236z00_2466;

				{	/* SawMill/node2rtl.scm 120 */
					BgL_areaz00_bglt BgL_new1235z00_2473;

					BgL_new1235z00_2473 =
						((BgL_areaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_areaz00_bgl))));
					{	/* SawMill/node2rtl.scm 120 */
						long BgL_arg1916z00_2474;

						BgL_arg1916z00_2474 = BGL_CLASS_NUM(BGl_areaz00zzsaw_node2rtlz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1235z00_2473), BgL_arg1916z00_2474);
					}
					BgL_new1236z00_2466 = BgL_new1235z00_2473;
				}
				((((BgL_areaz00_bglt) COBJECT(BgL_new1236z00_2466))->BgL_entryz00) =
					((BgL_blockz00_bglt) (((BgL_areaz00_bglt) COBJECT(BgL_az00_113))->
							BgL_entryz00)), BUNSPEC);
				{
					BgL_blockz00_bglt BgL_auxz00_7175;

					{	/* SawMill/node2rtl.scm 122 */
						BgL_areaz00_bglt BgL_arg1911z00_2467;

						{	/* SawMill/node2rtl.scm 122 */
							BgL_rtl_nopz00_bglt BgL_arg1912z00_2468;

							{	/* SawMill/node2rtl.scm 122 */
								BgL_rtl_nopz00_bglt BgL_new1238z00_2470;

								{	/* SawMill/node2rtl.scm 122 */
									BgL_rtl_nopz00_bglt BgL_new1237z00_2471;

									BgL_new1237z00_2471 =
										((BgL_rtl_nopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_rtl_nopz00_bgl))));
									{	/* SawMill/node2rtl.scm 122 */
										long BgL_arg1914z00_2472;

										{	/* SawMill/node2rtl.scm 122 */
											obj_t BgL_classz00_4379;

											BgL_classz00_4379 = BGl_rtl_nopz00zzsaw_defsz00;
											BgL_arg1914z00_2472 = BGL_CLASS_NUM(BgL_classz00_4379);
										}
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1237z00_2471),
											BgL_arg1914z00_2472);
									}
									BgL_new1238z00_2470 = BgL_new1237z00_2471;
								}
								((((BgL_rtl_funz00_bglt) COBJECT(
												((BgL_rtl_funz00_bglt) BgL_new1238z00_2470)))->
										BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
								BgL_arg1912z00_2468 = BgL_new1238z00_2470;
							}
							BgL_arg1911z00_2467 =
								BGl_singleza2za2zzsaw_node2rtlz00(BFALSE,
								((BgL_rtl_funz00_bglt) BgL_arg1912z00_2468), BNIL);
						}
						BgL_auxz00_7175 =
							(((BgL_areaz00_bglt) COBJECT(BgL_arg1911z00_2467))->BgL_exitz00);
					}
					((((BgL_areaz00_bglt) COBJECT(BgL_new1236z00_2466))->BgL_exitz00) =
						((BgL_blockz00_bglt) BgL_auxz00_7175), BUNSPEC);
				}
				return BgL_new1236z00_2466;
			}
		}

	}



/* link */
	BgL_areaz00_bglt BGl_linkz00zzsaw_node2rtlz00(BgL_areaz00_bglt BgL_b1z00_114,
		BgL_areaz00_bglt BgL_b2z00_115)
	{
		{	/* SawMill/node2rtl.scm 124 */
			{	/* SawMill/node2rtl.scm 125 */
				BgL_blockz00_bglt BgL_arg1917z00_2475;
				BgL_blockz00_bglt BgL_arg1918z00_2476;

				BgL_arg1917z00_2475 =
					(((BgL_areaz00_bglt) COBJECT(BgL_b1z00_114))->BgL_exitz00);
				BgL_arg1918z00_2476 =
					(((BgL_areaz00_bglt) COBJECT(BgL_b2z00_115))->BgL_entryz00);
				{	/* SawMill/node2rtl.scm 92 */
					obj_t BgL_arg1882z00_4388;

					{	/* SawMill/node2rtl.scm 92 */
						obj_t BgL_arg1883z00_4389;

						BgL_arg1883z00_4389 =
							(((BgL_blockz00_bglt) COBJECT(BgL_arg1917z00_2475))->
							BgL_succsz00);
						BgL_arg1882z00_4388 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_arg1918z00_2476),
							BgL_arg1883z00_4389);
					}
					((((BgL_blockz00_bglt) COBJECT(BgL_arg1917z00_2475))->BgL_succsz00) =
						((obj_t) BgL_arg1882z00_4388), BUNSPEC);
				}
			}
			{	/* SawMill/node2rtl.scm 126 */
				BgL_areaz00_bglt BgL_new1240z00_2477;

				{	/* SawMill/node2rtl.scm 126 */
					BgL_areaz00_bglt BgL_new1239z00_2478;

					BgL_new1239z00_2478 =
						((BgL_areaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_areaz00_bgl))));
					{	/* SawMill/node2rtl.scm 126 */
						long BgL_arg1919z00_2479;

						BgL_arg1919z00_2479 = BGL_CLASS_NUM(BGl_areaz00zzsaw_node2rtlz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1239z00_2478), BgL_arg1919z00_2479);
					}
					BgL_new1240z00_2477 = BgL_new1239z00_2478;
				}
				((((BgL_areaz00_bglt) COBJECT(BgL_new1240z00_2477))->BgL_entryz00) =
					((BgL_blockz00_bglt) (((BgL_areaz00_bglt) COBJECT(BgL_b1z00_114))->
							BgL_entryz00)), BUNSPEC);
				((((BgL_areaz00_bglt) COBJECT(BgL_new1240z00_2477))->BgL_exitz00) =
					((BgL_blockz00_bglt) (((BgL_areaz00_bglt) COBJECT(BgL_b2z00_115))->
							BgL_exitz00)), BUNSPEC);
				return BgL_new1240z00_2477;
			}
		}

	}



/* link* */
	BgL_areaz00_bglt BGl_linkza2za2zzsaw_node2rtlz00(obj_t BgL_lz00_116)
	{
		{	/* SawMill/node2rtl.scm 128 */
			{
				BgL_areaz00_bglt BgL_rz00_2489;
				obj_t BgL_lz00_2490;

				if (NULLP(BgL_lz00_116))
					{	/* SawMill/node2rtl.scm 134 */
						BgL_rtl_nopz00_bglt BgL_arg1923z00_2482;

						{	/* SawMill/node2rtl.scm 134 */
							BgL_rtl_nopz00_bglt BgL_new1243z00_2484;

							{	/* SawMill/node2rtl.scm 134 */
								BgL_rtl_nopz00_bglt BgL_new1242z00_2485;

								BgL_new1242z00_2485 =
									((BgL_rtl_nopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_rtl_nopz00_bgl))));
								{	/* SawMill/node2rtl.scm 134 */
									long BgL_arg1925z00_2486;

									BgL_arg1925z00_2486 =
										BGL_CLASS_NUM(BGl_rtl_nopz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1242z00_2485),
										BgL_arg1925z00_2486);
								}
								BgL_new1243z00_2484 = BgL_new1242z00_2485;
							}
							((((BgL_rtl_funz00_bglt) COBJECT(
											((BgL_rtl_funz00_bglt) BgL_new1243z00_2484)))->
									BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
							BgL_arg1923z00_2482 = BgL_new1243z00_2484;
						}
						return
							BGl_singleza2za2zzsaw_node2rtlz00(BFALSE,
							((BgL_rtl_funz00_bglt) BgL_arg1923z00_2482), BNIL);
					}
				else
					{	/* SawMill/node2rtl.scm 135 */
						obj_t BgL_arg1926z00_2487;
						obj_t BgL_arg1927z00_2488;

						BgL_arg1926z00_2487 = CAR(((obj_t) BgL_lz00_116));
						BgL_arg1927z00_2488 = CDR(((obj_t) BgL_lz00_116));
						BgL_rz00_2489 = ((BgL_areaz00_bglt) BgL_arg1926z00_2487);
						BgL_lz00_2490 = BgL_arg1927z00_2488;
					BgL_zc3z04anonymousza31928ze3z87_2491:
						if (NULLP(BgL_lz00_2490))
							{	/* SawMill/node2rtl.scm 130 */
								return BgL_rz00_2489;
							}
						else
							{	/* SawMill/node2rtl.scm 132 */
								BgL_areaz00_bglt BgL_arg1930z00_2493;
								obj_t BgL_arg1931z00_2494;

								{	/* SawMill/node2rtl.scm 132 */
									obj_t BgL_arg1932z00_2495;

									BgL_arg1932z00_2495 = CAR(((obj_t) BgL_lz00_2490));
									BgL_arg1930z00_2493 =
										BGl_linkz00zzsaw_node2rtlz00(BgL_rz00_2489,
										((BgL_areaz00_bglt) BgL_arg1932z00_2495));
								}
								BgL_arg1931z00_2494 = CDR(((obj_t) BgL_lz00_2490));
								{
									obj_t BgL_lz00_7223;
									BgL_areaz00_bglt BgL_rz00_7222;

									BgL_rz00_7222 = BgL_arg1930z00_2493;
									BgL_lz00_7223 = BgL_arg1931z00_2494;
									BgL_lz00_2490 = BgL_lz00_7223;
									BgL_rz00_2489 = BgL_rz00_7222;
									goto BgL_zc3z04anonymousza31928ze3z87_2491;
								}
							}
					}
			}
		}

	}



/* fork/join */
	BgL_areaz00_bglt BGl_forkzf2joinzf2zzsaw_node2rtlz00(BgL_areaz00_bglt
		BgL_headz00_117, obj_t BgL_contsz00_118, BgL_rtl_regz00_bglt BgL_rz00_119)
	{
		{	/* SawMill/node2rtl.scm 137 */
			{	/* SawMill/node2rtl.scm 139 */
				BgL_blockz00_bglt BgL_arg1933z00_2497;
				obj_t BgL_arg1934z00_2498;

				BgL_arg1933z00_2497 =
					(((BgL_areaz00_bglt) COBJECT(BgL_headz00_117))->BgL_exitz00);
				if (NULLP(BgL_contsz00_118))
					{	/* SawMill/node2rtl.scm 139 */
						BgL_arg1934z00_2498 = BNIL;
					}
				else
					{	/* SawMill/node2rtl.scm 139 */
						obj_t BgL_head1622z00_2501;

						{	/* SawMill/node2rtl.scm 139 */
							BgL_blockz00_bglt BgL_arg1941z00_2513;

							BgL_arg1941z00_2513 =
								(((BgL_areaz00_bglt) COBJECT(
										((BgL_areaz00_bglt)
											CAR(((obj_t) BgL_contsz00_118)))))->BgL_entryz00);
							BgL_head1622z00_2501 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg1941z00_2513), BNIL);
						}
						{	/* SawMill/node2rtl.scm 139 */
							obj_t BgL_g1626z00_2502;

							BgL_g1626z00_2502 = CDR(((obj_t) BgL_contsz00_118));
							{
								obj_t BgL_l1620z00_2504;
								obj_t BgL_tail1623z00_2505;

								BgL_l1620z00_2504 = BgL_g1626z00_2502;
								BgL_tail1623z00_2505 = BgL_head1622z00_2501;
							BgL_zc3z04anonymousza31936ze3z87_2506:
								if (NULLP(BgL_l1620z00_2504))
									{	/* SawMill/node2rtl.scm 139 */
										BgL_arg1934z00_2498 = BgL_head1622z00_2501;
									}
								else
									{	/* SawMill/node2rtl.scm 139 */
										obj_t BgL_newtail1624z00_2508;

										{	/* SawMill/node2rtl.scm 139 */
											BgL_blockz00_bglt BgL_arg1939z00_2510;

											BgL_arg1939z00_2510 =
												(((BgL_areaz00_bglt) COBJECT(
														((BgL_areaz00_bglt)
															CAR(
																((obj_t) BgL_l1620z00_2504)))))->BgL_entryz00);
											BgL_newtail1624z00_2508 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1939z00_2510), BNIL);
										}
										SET_CDR(BgL_tail1623z00_2505, BgL_newtail1624z00_2508);
										{	/* SawMill/node2rtl.scm 139 */
											obj_t BgL_arg1938z00_2509;

											BgL_arg1938z00_2509 = CDR(((obj_t) BgL_l1620z00_2504));
											{
												obj_t BgL_tail1623z00_7248;
												obj_t BgL_l1620z00_7247;

												BgL_l1620z00_7247 = BgL_arg1938z00_2509;
												BgL_tail1623z00_7248 = BgL_newtail1624z00_2508;
												BgL_tail1623z00_2505 = BgL_tail1623z00_7248;
												BgL_l1620z00_2504 = BgL_l1620z00_7247;
												goto BgL_zc3z04anonymousza31936ze3z87_2506;
											}
										}
									}
							}
						}
					}
				((((BgL_blockz00_bglt) COBJECT(BgL_arg1933z00_2497))->BgL_succsz00) =
					((obj_t) BgL_arg1934z00_2498), BUNSPEC);
			}
			{	/* SawMill/node2rtl.scm 141 */
				BgL_blockz00_bglt BgL_joinz00_2515;

				{	/* SawMill/node2rtl.scm 141 */
					BgL_areaz00_bglt BgL_arg1947z00_2527;

					{	/* SawMill/node2rtl.scm 141 */
						BgL_rtl_movz00_bglt BgL_arg1948z00_2528;

						{	/* SawMill/node2rtl.scm 141 */
							BgL_rtl_movz00_bglt BgL_new1245z00_2530;

							{	/* SawMill/node2rtl.scm 141 */
								BgL_rtl_movz00_bglt BgL_new1244z00_2531;

								BgL_new1244z00_2531 =
									((BgL_rtl_movz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_rtl_movz00_bgl))));
								{	/* SawMill/node2rtl.scm 141 */
									long BgL_arg1950z00_2532;

									BgL_arg1950z00_2532 =
										BGL_CLASS_NUM(BGl_rtl_movz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1244z00_2531),
										BgL_arg1950z00_2532);
								}
								BgL_new1245z00_2530 = BgL_new1244z00_2531;
							}
							((((BgL_rtl_funz00_bglt) COBJECT(
											((BgL_rtl_funz00_bglt) BgL_new1245z00_2530)))->
									BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
							BgL_arg1948z00_2528 = BgL_new1245z00_2530;
						}
						{	/* SawMill/node2rtl.scm 141 */
							obj_t BgL_list1949z00_2529;

							BgL_list1949z00_2529 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_rz00_119), BNIL);
							BgL_arg1947z00_2527 =
								BGl_singleza2za2zzsaw_node2rtlz00(BFALSE,
								((BgL_rtl_funz00_bglt) BgL_arg1948z00_2528),
								BgL_list1949z00_2529);
					}}
					BgL_joinz00_2515 =
						(((BgL_areaz00_bglt) COBJECT(BgL_arg1947z00_2527))->BgL_entryz00);
				}
				{
					obj_t BgL_l1627z00_2517;

					BgL_l1627z00_2517 = BgL_contsz00_118;
				BgL_zc3z04anonymousza31943ze3z87_2518:
					if (PAIRP(BgL_l1627z00_2517))
						{	/* SawMill/node2rtl.scm 142 */
							{	/* SawMill/node2rtl.scm 143 */
								BgL_areaz00_bglt BgL_az00_2520;

								BgL_az00_2520 = ((BgL_areaz00_bglt) CAR(BgL_l1627z00_2517));
								{	/* SawMill/node2rtl.scm 143 */
									BgL_blockz00_bglt BgL_bz00_2521;

									BgL_bz00_2521 =
										(((BgL_areaz00_bglt) COBJECT(BgL_az00_2520))->BgL_exitz00);
									{	/* SawMill/node2rtl.scm 92 */
										obj_t BgL_arg1882z00_4425;

										{	/* SawMill/node2rtl.scm 92 */
											obj_t BgL_arg1883z00_4426;

											BgL_arg1883z00_4426 =
												(((BgL_blockz00_bglt) COBJECT(BgL_bz00_2521))->
												BgL_succsz00);
											BgL_arg1882z00_4425 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_joinz00_2515),
												BgL_arg1883z00_4426);
										}
										((((BgL_blockz00_bglt) COBJECT(BgL_bz00_2521))->
												BgL_succsz00) = ((obj_t) BgL_arg1882z00_4425), BUNSPEC);
									}
									BGl_bdestinationz12z12zzsaw_node2rtlz00(BgL_bz00_2521,
										BgL_rz00_119);
								}
							}
							{
								obj_t BgL_l1627z00_7271;

								BgL_l1627z00_7271 = CDR(BgL_l1627z00_2517);
								BgL_l1627z00_2517 = BgL_l1627z00_7271;
								goto BgL_zc3z04anonymousza31943ze3z87_2518;
							}
						}
					else
						{	/* SawMill/node2rtl.scm 142 */
							((bool_t) 1);
						}
				}
				{	/* SawMill/node2rtl.scm 148 */
					BgL_areaz00_bglt BgL_new1247z00_2524;

					{	/* SawMill/node2rtl.scm 148 */
						BgL_areaz00_bglt BgL_new1246z00_2525;

						BgL_new1246z00_2525 =
							((BgL_areaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_areaz00_bgl))));
						{	/* SawMill/node2rtl.scm 148 */
							long BgL_arg1946z00_2526;

							BgL_arg1946z00_2526 = BGL_CLASS_NUM(BGl_areaz00zzsaw_node2rtlz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1246z00_2525),
								BgL_arg1946z00_2526);
						}
						BgL_new1247z00_2524 = BgL_new1246z00_2525;
					}
					((((BgL_areaz00_bglt) COBJECT(BgL_new1247z00_2524))->BgL_entryz00) =
						((BgL_blockz00_bglt) (((BgL_areaz00_bglt)
									COBJECT(BgL_headz00_117))->BgL_entryz00)), BUNSPEC);
					((((BgL_areaz00_bglt) COBJECT(BgL_new1247z00_2524))->BgL_exitz00) =
						((BgL_blockz00_bglt) BgL_joinz00_2515), BUNSPEC);
					return BgL_new1247z00_2524;
				}
			}
		}

	}



/* join */
	obj_t BGl_joinz00zzsaw_node2rtlz00(obj_t BgL_contsz00_120,
		BgL_rtl_regz00_bglt BgL_xrz00_121)
	{
		{	/* SawMill/node2rtl.scm 150 */
			{	/* SawMill/node2rtl.scm 151 */
				obj_t BgL_rz00_2533;

				BgL_rz00_2533 =
					BGl_findzd2registerzd2zzsaw_node2rtlz00(BgL_contsz00_120,
					BgL_xrz00_121);
				{	/* SawMill/node2rtl.scm 153 */
					BgL_blockz00_bglt BgL_joinz00_2534;

					{	/* SawMill/node2rtl.scm 153 */
						BgL_areaz00_bglt BgL_arg1960z00_2559;

						{	/* SawMill/node2rtl.scm 153 */
							BgL_rtl_movz00_bglt BgL_arg1961z00_2560;

							{	/* SawMill/node2rtl.scm 153 */
								BgL_rtl_movz00_bglt BgL_new1249z00_2562;

								{	/* SawMill/node2rtl.scm 153 */
									BgL_rtl_movz00_bglt BgL_new1248z00_2563;

									BgL_new1248z00_2563 =
										((BgL_rtl_movz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_rtl_movz00_bgl))));
									{	/* SawMill/node2rtl.scm 153 */
										long BgL_arg1963z00_2564;

										BgL_arg1963z00_2564 =
											BGL_CLASS_NUM(BGl_rtl_movz00zzsaw_defsz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1248z00_2563),
											BgL_arg1963z00_2564);
									}
									BgL_new1249z00_2562 = BgL_new1248z00_2563;
								}
								((((BgL_rtl_funz00_bglt) COBJECT(
												((BgL_rtl_funz00_bglt) BgL_new1249z00_2562)))->
										BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
								BgL_arg1961z00_2560 = BgL_new1249z00_2562;
							}
							{	/* SawMill/node2rtl.scm 153 */
								obj_t BgL_list1962z00_2561;

								BgL_list1962z00_2561 = MAKE_YOUNG_PAIR(BgL_rz00_2533, BNIL);
								BgL_arg1960z00_2559 =
									BGl_singleza2za2zzsaw_node2rtlz00(BFALSE,
									((BgL_rtl_funz00_bglt) BgL_arg1961z00_2560),
									BgL_list1962z00_2561);
						}}
						BgL_joinz00_2534 =
							(((BgL_areaz00_bglt) COBJECT(BgL_arg1960z00_2559))->BgL_entryz00);
					}
					{
						obj_t BgL_l1629z00_2536;

						BgL_l1629z00_2536 = BgL_contsz00_120;
					BgL_zc3z04anonymousza31951ze3z87_2537:
						if (PAIRP(BgL_l1629z00_2536))
							{	/* SawMill/node2rtl.scm 154 */
								{	/* SawMill/node2rtl.scm 155 */
									BgL_areaz00_bglt BgL_az00_2539;

									BgL_az00_2539 = ((BgL_areaz00_bglt) CAR(BgL_l1629z00_2536));
									{	/* SawMill/node2rtl.scm 155 */
										BgL_blockz00_bglt BgL_bz00_2540;

										BgL_bz00_2540 =
											(((BgL_areaz00_bglt) COBJECT(BgL_az00_2539))->
											BgL_exitz00);
										{	/* SawMill/node2rtl.scm 92 */
											obj_t BgL_arg1882z00_4444;

											{	/* SawMill/node2rtl.scm 92 */
												obj_t BgL_arg1883z00_4445;

												BgL_arg1883z00_4445 =
													(((BgL_blockz00_bglt) COBJECT(BgL_bz00_2540))->
													BgL_succsz00);
												BgL_arg1882z00_4444 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_joinz00_2534),
													BgL_arg1883z00_4445);
											}
											((((BgL_blockz00_bglt) COBJECT(BgL_bz00_2540))->
													BgL_succsz00) =
												((obj_t) BgL_arg1882z00_4444), BUNSPEC);
										}
										BGl_bdestinationz12z12zzsaw_node2rtlz00(BgL_bz00_2540,
											((BgL_rtl_regz00_bglt) BgL_rz00_2533));
									}
								}
								{
									obj_t BgL_l1629z00_7302;

									BgL_l1629z00_7302 = CDR(BgL_l1629z00_2536);
									BgL_l1629z00_2536 = BgL_l1629z00_7302;
									goto BgL_zc3z04anonymousza31951ze3z87_2537;
								}
							}
						else
							{	/* SawMill/node2rtl.scm 154 */
								((bool_t) 1);
							}
					}
					if (NULLP(BgL_contsz00_120))
						{	/* SawMill/node2rtl.scm 160 */
							return BNIL;
						}
					else
						{	/* SawMill/node2rtl.scm 160 */
							obj_t BgL_head1633z00_2545;

							BgL_head1633z00_2545 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1631z00_2547;
								obj_t BgL_tail1634z00_2548;

								BgL_l1631z00_2547 = BgL_contsz00_120;
								BgL_tail1634z00_2548 = BgL_head1633z00_2545;
							BgL_zc3z04anonymousza31955ze3z87_2549:
								if (NULLP(BgL_l1631z00_2547))
									{	/* SawMill/node2rtl.scm 160 */
										return CDR(BgL_head1633z00_2545);
									}
								else
									{	/* SawMill/node2rtl.scm 160 */
										obj_t BgL_newtail1635z00_2551;

										{	/* SawMill/node2rtl.scm 160 */
											BgL_areaz00_bglt BgL_arg1958z00_2553;

											{	/* SawMill/node2rtl.scm 160 */
												BgL_areaz00_bglt BgL_az00_2554;

												BgL_az00_2554 =
													((BgL_areaz00_bglt) CAR(((obj_t) BgL_l1631z00_2547)));
												{	/* SawMill/node2rtl.scm 161 */
													BgL_areaz00_bglt BgL_new1252z00_2555;

													{	/* SawMill/node2rtl.scm 161 */
														BgL_areaz00_bglt BgL_new1251z00_2556;

														BgL_new1251z00_2556 =
															((BgL_areaz00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_areaz00_bgl))));
														{	/* SawMill/node2rtl.scm 161 */
															long BgL_arg1959z00_2557;

															BgL_arg1959z00_2557 =
																BGL_CLASS_NUM(BGl_areaz00zzsaw_node2rtlz00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1251z00_2556),
																BgL_arg1959z00_2557);
														}
														BgL_new1252z00_2555 = BgL_new1251z00_2556;
													}
													((((BgL_areaz00_bglt) COBJECT(BgL_new1252z00_2555))->
															BgL_entryz00) =
														((BgL_blockz00_bglt) (((BgL_areaz00_bglt)
																	COBJECT(BgL_az00_2554))->BgL_entryz00)),
														BUNSPEC);
													((((BgL_areaz00_bglt) COBJECT(BgL_new1252z00_2555))->
															BgL_exitz00) =
														((BgL_blockz00_bglt) BgL_joinz00_2534), BUNSPEC);
													BgL_arg1958z00_2553 = BgL_new1252z00_2555;
											}}
											BgL_newtail1635z00_2551 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1958z00_2553), BNIL);
										}
										SET_CDR(BgL_tail1634z00_2548, BgL_newtail1635z00_2551);
										{	/* SawMill/node2rtl.scm 160 */
											obj_t BgL_arg1957z00_2552;

											BgL_arg1957z00_2552 = CDR(((obj_t) BgL_l1631z00_2547));
											{
												obj_t BgL_tail1634z00_7326;
												obj_t BgL_l1631z00_7325;

												BgL_l1631z00_7325 = BgL_arg1957z00_2552;
												BgL_tail1634z00_7326 = BgL_newtail1635z00_2551;
												BgL_tail1634z00_2548 = BgL_tail1634z00_7326;
												BgL_l1631z00_2547 = BgL_l1631z00_7325;
												goto BgL_zc3z04anonymousza31955ze3z87_2549;
											}
										}
									}
							}
						}
				}
			}
		}

	}



/* fork */
	BgL_areaz00_bglt BGl_forkz00zzsaw_node2rtlz00(BgL_areaz00_bglt
		BgL_headz00_122, obj_t BgL_joinedz00_123)
	{
		{	/* SawMill/node2rtl.scm 164 */
			{	/* SawMill/node2rtl.scm 166 */
				BgL_blockz00_bglt BgL_arg1964z00_2565;
				obj_t BgL_arg1965z00_2566;

				BgL_arg1964z00_2565 =
					(((BgL_areaz00_bglt) COBJECT(BgL_headz00_122))->BgL_exitz00);
				if (NULLP(BgL_joinedz00_123))
					{	/* SawMill/node2rtl.scm 166 */
						BgL_arg1965z00_2566 = BNIL;
					}
				else
					{	/* SawMill/node2rtl.scm 166 */
						obj_t BgL_head1638z00_2569;

						{	/* SawMill/node2rtl.scm 166 */
							BgL_blockz00_bglt BgL_arg1972z00_2581;

							BgL_arg1972z00_2581 =
								(((BgL_areaz00_bglt) COBJECT(
										((BgL_areaz00_bglt)
											CAR(((obj_t) BgL_joinedz00_123)))))->BgL_entryz00);
							BgL_head1638z00_2569 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg1972z00_2581), BNIL);
						}
						{	/* SawMill/node2rtl.scm 166 */
							obj_t BgL_g1641z00_2570;

							BgL_g1641z00_2570 = CDR(((obj_t) BgL_joinedz00_123));
							{
								obj_t BgL_l1636z00_2572;
								obj_t BgL_tail1639z00_2573;

								BgL_l1636z00_2572 = BgL_g1641z00_2570;
								BgL_tail1639z00_2573 = BgL_head1638z00_2569;
							BgL_zc3z04anonymousza31967ze3z87_2574:
								if (NULLP(BgL_l1636z00_2572))
									{	/* SawMill/node2rtl.scm 166 */
										BgL_arg1965z00_2566 = BgL_head1638z00_2569;
									}
								else
									{	/* SawMill/node2rtl.scm 166 */
										obj_t BgL_newtail1640z00_2576;

										{	/* SawMill/node2rtl.scm 166 */
											BgL_blockz00_bglt BgL_arg1970z00_2578;

											BgL_arg1970z00_2578 =
												(((BgL_areaz00_bglt) COBJECT(
														((BgL_areaz00_bglt)
															CAR(
																((obj_t) BgL_l1636z00_2572)))))->BgL_entryz00);
											BgL_newtail1640z00_2576 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1970z00_2578), BNIL);
										}
										SET_CDR(BgL_tail1639z00_2573, BgL_newtail1640z00_2576);
										{	/* SawMill/node2rtl.scm 166 */
											obj_t BgL_arg1969z00_2577;

											BgL_arg1969z00_2577 = CDR(((obj_t) BgL_l1636z00_2572));
											{
												obj_t BgL_tail1639z00_7350;
												obj_t BgL_l1636z00_7349;

												BgL_l1636z00_7349 = BgL_arg1969z00_2577;
												BgL_tail1639z00_7350 = BgL_newtail1640z00_2576;
												BgL_tail1639z00_2573 = BgL_tail1639z00_7350;
												BgL_l1636z00_2572 = BgL_l1636z00_7349;
												goto BgL_zc3z04anonymousza31967ze3z87_2574;
											}
										}
									}
							}
						}
					}
				((((BgL_blockz00_bglt) COBJECT(BgL_arg1964z00_2565))->BgL_succsz00) =
					((obj_t) BgL_arg1965z00_2566), BUNSPEC);
			}
			{	/* SawMill/node2rtl.scm 168 */
				BgL_areaz00_bglt BgL_new1254z00_2583;

				{	/* SawMill/node2rtl.scm 168 */
					BgL_areaz00_bglt BgL_new1253z00_2585;

					BgL_new1253z00_2585 =
						((BgL_areaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_areaz00_bgl))));
					{	/* SawMill/node2rtl.scm 168 */
						long BgL_arg1975z00_2586;

						BgL_arg1975z00_2586 = BGL_CLASS_NUM(BGl_areaz00zzsaw_node2rtlz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1253z00_2585), BgL_arg1975z00_2586);
					}
					BgL_new1254z00_2583 = BgL_new1253z00_2585;
				}
				((((BgL_areaz00_bglt) COBJECT(BgL_new1254z00_2583))->BgL_entryz00) =
					((BgL_blockz00_bglt) (((BgL_areaz00_bglt) COBJECT(BgL_headz00_122))->
							BgL_entryz00)), BUNSPEC);
				((((BgL_areaz00_bglt) COBJECT(BgL_new1254z00_2583))->BgL_exitz00) =
					((BgL_blockz00_bglt) (((BgL_areaz00_bglt) COBJECT(((BgL_areaz00_bglt)
										CAR(((obj_t) BgL_joinedz00_123)))))->BgL_exitz00)),
					BUNSPEC);
				return BgL_new1254z00_2583;
			}
		}

	}



/* find-register */
	obj_t BGl_findzd2registerzd2zzsaw_node2rtlz00(obj_t BgL_areasz00_124,
		BgL_rtl_regz00_bglt BgL_regz00_125)
	{
		{	/* SawMill/node2rtl.scm 171 */
			{
				obj_t BgL_az00_2596;

				{
					obj_t BgL_lz00_2589;

					BgL_lz00_2589 = BgL_areasz00_124;
				BgL_zc3z04anonymousza31976ze3z87_2590:
					if (NULLP(BgL_lz00_2589))
						{	/* SawMill/node2rtl.scm 181 */
							return ((obj_t) BgL_regz00_125);
						}
					else
						{	/* SawMill/node2rtl.scm 183 */
							obj_t BgL__ortest_1256z00_2592;

							{	/* SawMill/node2rtl.scm 183 */
								obj_t BgL_arg1979z00_2594;

								BgL_arg1979z00_2594 = CAR(((obj_t) BgL_lz00_2589));
								BgL_az00_2596 = BgL_arg1979z00_2594;
								{	/* SawMill/node2rtl.scm 173 */
									obj_t BgL_insz00_2598;

									BgL_insz00_2598 =
										CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
											(((BgL_blockz00_bglt) COBJECT(
														(((BgL_areaz00_bglt) COBJECT(
																	((BgL_areaz00_bglt) BgL_az00_2596)))->
															BgL_exitz00)))->BgL_firstz00)));
									{	/* SawMill/node2rtl.scm 174 */
										bool_t BgL_test3080z00_7373;

										{	/* SawMill/node2rtl.scm 174 */
											BgL_rtl_funz00_bglt BgL_arg1991z00_2613;

											BgL_arg1991z00_2613 =
												(((BgL_rtl_insz00_bglt) COBJECT(
														((BgL_rtl_insz00_bglt) BgL_insz00_2598)))->
												BgL_funz00);
											{	/* SawMill/node2rtl.scm 174 */
												obj_t BgL_classz00_4478;

												BgL_classz00_4478 = BGl_rtl_movz00zzsaw_defsz00;
												{	/* SawMill/node2rtl.scm 174 */
													BgL_objectz00_bglt BgL_arg1807z00_4480;

													{	/* SawMill/node2rtl.scm 174 */
														obj_t BgL_tmpz00_7376;

														BgL_tmpz00_7376 =
															((obj_t)
															((BgL_objectz00_bglt) BgL_arg1991z00_2613));
														BgL_arg1807z00_4480 =
															(BgL_objectz00_bglt) (BgL_tmpz00_7376);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* SawMill/node2rtl.scm 174 */
															long BgL_idxz00_4486;

															BgL_idxz00_4486 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4480);
															BgL_test3080z00_7373 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_4486 + 3L)) == BgL_classz00_4478);
														}
													else
														{	/* SawMill/node2rtl.scm 174 */
															bool_t BgL_res2827z00_4511;

															{	/* SawMill/node2rtl.scm 174 */
																obj_t BgL_oclassz00_4494;

																{	/* SawMill/node2rtl.scm 174 */
																	obj_t BgL_arg1815z00_4502;
																	long BgL_arg1816z00_4503;

																	BgL_arg1815z00_4502 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* SawMill/node2rtl.scm 174 */
																		long BgL_arg1817z00_4504;

																		BgL_arg1817z00_4504 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4480);
																		BgL_arg1816z00_4503 =
																			(BgL_arg1817z00_4504 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_4494 =
																		VECTOR_REF(BgL_arg1815z00_4502,
																		BgL_arg1816z00_4503);
																}
																{	/* SawMill/node2rtl.scm 174 */
																	bool_t BgL__ortest_1115z00_4495;

																	BgL__ortest_1115z00_4495 =
																		(BgL_classz00_4478 == BgL_oclassz00_4494);
																	if (BgL__ortest_1115z00_4495)
																		{	/* SawMill/node2rtl.scm 174 */
																			BgL_res2827z00_4511 =
																				BgL__ortest_1115z00_4495;
																		}
																	else
																		{	/* SawMill/node2rtl.scm 174 */
																			long BgL_odepthz00_4496;

																			{	/* SawMill/node2rtl.scm 174 */
																				obj_t BgL_arg1804z00_4497;

																				BgL_arg1804z00_4497 =
																					(BgL_oclassz00_4494);
																				BgL_odepthz00_4496 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_4497);
																			}
																			if ((3L < BgL_odepthz00_4496))
																				{	/* SawMill/node2rtl.scm 174 */
																					obj_t BgL_arg1802z00_4499;

																					{	/* SawMill/node2rtl.scm 174 */
																						obj_t BgL_arg1803z00_4500;

																						BgL_arg1803z00_4500 =
																							(BgL_oclassz00_4494);
																						BgL_arg1802z00_4499 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_4500, 3L);
																					}
																					BgL_res2827z00_4511 =
																						(BgL_arg1802z00_4499 ==
																						BgL_classz00_4478);
																				}
																			else
																				{	/* SawMill/node2rtl.scm 174 */
																					BgL_res2827z00_4511 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test3080z00_7373 = BgL_res2827z00_4511;
														}
												}
											}
										}
										if (BgL_test3080z00_7373)
											{	/* SawMill/node2rtl.scm 175 */
												obj_t BgL_rz00_2600;

												{	/* SawMill/node2rtl.scm 175 */
													obj_t BgL_pairz00_4513;

													BgL_pairz00_4513 =
														(((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt) BgL_insz00_2598)))->
														BgL_argsz00);
													BgL_rz00_2600 = CAR(BgL_pairz00_4513);
												}
												{	/* SawMill/node2rtl.scm 176 */
													bool_t BgL_test3084z00_7402;

													if (CBOOL(
															(((BgL_rtl_regz00_bglt) COBJECT(
																		((BgL_rtl_regz00_bglt) BgL_rz00_2600)))->
																BgL_varz00)))
														{	/* SawMill/node2rtl.scm 176 */
															BgL_test3084z00_7402 = ((bool_t) 1);
														}
													else
														{	/* SawMill/node2rtl.scm 176 */
															if (
																(((obj_t)
																		(((BgL_rtl_regz00_bglt) COBJECT(
																					((BgL_rtl_regz00_bglt)
																						BgL_rz00_2600)))->BgL_typez00)) ==
																	((obj_t) (((BgL_rtl_regz00_bglt)
																				COBJECT(BgL_regz00_125))->
																			BgL_typez00))))
																{	/* SawMill/node2rtl.scm 177 */
																	BgL_test3084z00_7402 = ((bool_t) 0);
																}
															else
																{	/* SawMill/node2rtl.scm 177 */
																	BgL_test3084z00_7402 = ((bool_t) 1);
																}
														}
													if (BgL_test3084z00_7402)
														{	/* SawMill/node2rtl.scm 176 */
															BgL__ortest_1256z00_2592 = BFALSE;
														}
													else
														{	/* SawMill/node2rtl.scm 176 */
															BgL__ortest_1256z00_2592 = BgL_rz00_2600;
														}
												}
											}
										else
											{	/* SawMill/node2rtl.scm 174 */
												BgL__ortest_1256z00_2592 = BFALSE;
											}
									}
								}
							}
							if (CBOOL(BgL__ortest_1256z00_2592))
								{	/* SawMill/node2rtl.scm 183 */
									return BgL__ortest_1256z00_2592;
								}
							else
								{	/* SawMill/node2rtl.scm 184 */
									obj_t BgL_arg1978z00_2593;

									BgL_arg1978z00_2593 = CDR(((obj_t) BgL_lz00_2589));
									{
										obj_t BgL_lz00_7418;

										BgL_lz00_7418 = BgL_arg1978z00_2593;
										BgL_lz00_2589 = BgL_lz00_7418;
										goto BgL_zc3z04anonymousza31976ze3z87_2590;
									}
								}
						}
				}
			}
		}

	}



/* call* */
	BgL_areaz00_bglt BGl_callza2za2zzsaw_node2rtlz00(obj_t BgL_ez00_128,
		BgL_rtl_funz00_bglt BgL_funz00_129, obj_t BgL_exprsz00_130)
	{
		{	/* SawMill/node2rtl.scm 191 */
			{	/* SawMill/node2rtl.scm 192 */
				obj_t BgL_regsz00_2619;

				if (NULLP(BgL_exprsz00_130))
					{	/* SawMill/node2rtl.scm 192 */
						BgL_regsz00_2619 = BNIL;
					}
				else
					{	/* SawMill/node2rtl.scm 192 */
						obj_t BgL_head1644z00_2642;

						BgL_head1644z00_2642 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						{
							obj_t BgL_l1642z00_2644;
							obj_t BgL_tail1645z00_2645;

							BgL_l1642z00_2644 = BgL_exprsz00_130;
							BgL_tail1645z00_2645 = BgL_head1644z00_2642;
						BgL_zc3z04anonymousza32005ze3z87_2646:
							if (NULLP(BgL_l1642z00_2644))
								{	/* SawMill/node2rtl.scm 192 */
									BgL_regsz00_2619 = CDR(BgL_head1644z00_2642);
								}
							else
								{	/* SawMill/node2rtl.scm 192 */
									obj_t BgL_newtail1646z00_2648;

									{	/* SawMill/node2rtl.scm 192 */
										BgL_rtl_regz00_bglt BgL_arg2008z00_2650;

										{	/* SawMill/node2rtl.scm 192 */
											obj_t BgL_ez00_2651;

											BgL_ez00_2651 = CAR(((obj_t) BgL_l1642z00_2644));
											BgL_arg2008z00_2650 =
												BGl_newzd2regzd2zzsaw_node2rtlz00(
												((BgL_nodez00_bglt) BgL_ez00_2651));
										}
										BgL_newtail1646z00_2648 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_arg2008z00_2650), BNIL);
									}
									SET_CDR(BgL_tail1645z00_2645, BgL_newtail1646z00_2648);
									{	/* SawMill/node2rtl.scm 192 */
										obj_t BgL_arg2007z00_2649;

										BgL_arg2007z00_2649 = CDR(((obj_t) BgL_l1642z00_2644));
										{
											obj_t BgL_tail1645z00_7435;
											obj_t BgL_l1642z00_7434;

											BgL_l1642z00_7434 = BgL_arg2007z00_2649;
											BgL_tail1645z00_7435 = BgL_newtail1646z00_2648;
											BgL_tail1645z00_2645 = BgL_tail1645z00_7435;
											BgL_l1642z00_2644 = BgL_l1642z00_7434;
											goto BgL_zc3z04anonymousza32005ze3z87_2646;
										}
									}
								}
						}
					}
				{	/* SawMill/node2rtl.scm 193 */
					BgL_areaz00_bglt BgL_arg1995z00_2620;
					BgL_areaz00_bglt BgL_arg1996z00_2621;

					{	/* SawMill/node2rtl.scm 193 */
						obj_t BgL_arg1997z00_2622;

						if (NULLP(BgL_exprsz00_130))
							{	/* SawMill/node2rtl.scm 193 */
								BgL_arg1997z00_2622 = BNIL;
							}
						else
							{	/* SawMill/node2rtl.scm 193 */
								obj_t BgL_head1649z00_2626;

								BgL_head1649z00_2626 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_ll1647z00_2628;
									obj_t BgL_ll1648z00_2629;
									obj_t BgL_tail1650z00_2630;

									BgL_ll1647z00_2628 = BgL_exprsz00_130;
									BgL_ll1648z00_2629 = BgL_regsz00_2619;
									BgL_tail1650z00_2630 = BgL_head1649z00_2626;
								BgL_zc3z04anonymousza31999ze3z87_2631:
									if (NULLP(BgL_ll1647z00_2628))
										{	/* SawMill/node2rtl.scm 193 */
											BgL_arg1997z00_2622 = CDR(BgL_head1649z00_2626);
										}
									else
										{	/* SawMill/node2rtl.scm 193 */
											obj_t BgL_newtail1651z00_2633;

											{	/* SawMill/node2rtl.scm 193 */
												BgL_areaz00_bglt BgL_arg2003z00_2636;

												{	/* SawMill/node2rtl.scm 193 */
													obj_t BgL_az00_2637;
													obj_t BgL_rz00_2638;

													BgL_az00_2637 = CAR(((obj_t) BgL_ll1647z00_2628));
													BgL_rz00_2638 = CAR(((obj_t) BgL_ll1648z00_2629));
													{	/* SawMill/node2rtl.scm 193 */
														BgL_areaz00_bglt BgL_res2828z00_4538;

														{	/* SawMill/node2rtl.scm 187 */
															BgL_areaz00_bglt BgL_az00_4533;

															BgL_az00_4533 =
																BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(
																((BgL_nodez00_bglt) BgL_az00_2637));
															{	/* SawMill/node2rtl.scm 116 */
																BgL_blockz00_bglt BgL_arg1910z00_4536;

																BgL_arg1910z00_4536 =
																	(((BgL_areaz00_bglt) COBJECT(BgL_az00_4533))->
																	BgL_exitz00);
																BGl_bdestinationz12z12zzsaw_node2rtlz00
																	(BgL_arg1910z00_4536,
																	((BgL_rtl_regz00_bglt) BgL_rz00_2638));
															}
															BgL_res2828z00_4538 = BgL_az00_4533;
														}
														BgL_arg2003z00_2636 = BgL_res2828z00_4538;
													}
												}
												BgL_newtail1651z00_2633 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg2003z00_2636), BNIL);
											}
											SET_CDR(BgL_tail1650z00_2630, BgL_newtail1651z00_2633);
											{	/* SawMill/node2rtl.scm 193 */
												obj_t BgL_arg2001z00_2634;
												obj_t BgL_arg2002z00_2635;

												BgL_arg2001z00_2634 = CDR(((obj_t) BgL_ll1647z00_2628));
												BgL_arg2002z00_2635 = CDR(((obj_t) BgL_ll1648z00_2629));
												{
													obj_t BgL_tail1650z00_7460;
													obj_t BgL_ll1648z00_7459;
													obj_t BgL_ll1647z00_7458;

													BgL_ll1647z00_7458 = BgL_arg2001z00_2634;
													BgL_ll1648z00_7459 = BgL_arg2002z00_2635;
													BgL_tail1650z00_7460 = BgL_newtail1651z00_2633;
													BgL_tail1650z00_2630 = BgL_tail1650z00_7460;
													BgL_ll1648z00_2629 = BgL_ll1648z00_7459;
													BgL_ll1647z00_2628 = BgL_ll1647z00_7458;
													goto BgL_zc3z04anonymousza31999ze3z87_2631;
												}
											}
										}
								}
							}
						BgL_arg1995z00_2620 =
							BGl_linkza2za2zzsaw_node2rtlz00(BgL_arg1997z00_2622);
					}
					BgL_arg1996z00_2621 =
						BGl_singleza2za2zzsaw_node2rtlz00(BgL_ez00_128, BgL_funz00_129,
						BgL_regsz00_2619);
					return BGl_linkz00zzsaw_node2rtlz00(BgL_arg1995z00_2620,
						BgL_arg1996z00_2621);
				}
			}
		}

	}



/* coerce */
	obj_t BGl_coercez00zzsaw_node2rtlz00(obj_t BgL_clausesz00_146)
	{
		{	/* SawMill/node2rtl.scm 276 */
			if (NULLP(BgL_clausesz00_146))
				{	/* SawMill/node2rtl.scm 277 */
					return BNIL;
				}
			else
				{	/* SawMill/node2rtl.scm 277 */
					obj_t BgL_head1686z00_2690;

					BgL_head1686z00_2690 = MAKE_YOUNG_PAIR(BNIL, BNIL);
					{
						obj_t BgL_l1684z00_2692;
						obj_t BgL_tail1687z00_2693;

						BgL_l1684z00_2692 = BgL_clausesz00_146;
						BgL_tail1687z00_2693 = BgL_head1686z00_2690;
					BgL_zc3z04anonymousza32026ze3z87_2694:
						if (NULLP(BgL_l1684z00_2692))
							{	/* SawMill/node2rtl.scm 277 */
								return CDR(BgL_head1686z00_2690);
							}
						else
							{	/* SawMill/node2rtl.scm 277 */
								obj_t BgL_newtail1688z00_2696;

								{	/* SawMill/node2rtl.scm 277 */
									obj_t BgL_arg2030z00_2698;

									{	/* SawMill/node2rtl.scm 277 */
										obj_t BgL_cz00_2699;

										BgL_cz00_2699 = CAR(((obj_t) BgL_l1684z00_2692));
										{	/* SawMill/node2rtl.scm 278 */
											obj_t BgL_pz00_2700;

											BgL_pz00_2700 = CAR(((obj_t) BgL_cz00_2699));
											if ((BgL_pz00_2700 == CNST_TABLE_REF(1)))
												{	/* SawMill/node2rtl.scm 279 */
													BgL_arg2030z00_2698 = BgL_pz00_2700;
												}
											else
												{	/* SawMill/node2rtl.scm 279 */
													if (NULLP(BgL_pz00_2700))
														{	/* SawMill/node2rtl.scm 281 */
															BgL_arg2030z00_2698 = BNIL;
														}
													else
														{	/* SawMill/node2rtl.scm 281 */
															obj_t BgL_head1681z00_2703;

															BgL_head1681z00_2703 =
																MAKE_YOUNG_PAIR(BNIL, BNIL);
															{
																obj_t BgL_l1679z00_2705;
																obj_t BgL_tail1682z00_2706;

																BgL_l1679z00_2705 = BgL_pz00_2700;
																BgL_tail1682z00_2706 = BgL_head1681z00_2703;
															BgL_zc3z04anonymousza32032ze3z87_2707:
																if (NULLP(BgL_l1679z00_2705))
																	{	/* SawMill/node2rtl.scm 281 */
																		BgL_arg2030z00_2698 =
																			CDR(BgL_head1681z00_2703);
																	}
																else
																	{	/* SawMill/node2rtl.scm 281 */
																		obj_t BgL_newtail1683z00_2709;

																		{	/* SawMill/node2rtl.scm 281 */
																			obj_t BgL_arg2036z00_2711;

																			{	/* SawMill/node2rtl.scm 281 */
																				obj_t BgL_nz00_2712;

																				BgL_nz00_2712 =
																					CAR(((obj_t) BgL_l1679z00_2705));
																				if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(BgL_nz00_2712))
																					{	/* SawMill/node2rtl.scm 282 */
																						BgL_arg2036z00_2711 = BgL_nz00_2712;
																					}
																				else
																					{	/* SawMill/node2rtl.scm 282 */
																						if (CHARP(BgL_nz00_2712))
																							{	/* SawMill/node2rtl.scm 283 */
																								BgL_arg2036z00_2711 =
																									BINT((CCHAR(BgL_nz00_2712)));
																							}
																						else
																							{	/* SawMill/node2rtl.scm 283 */
																								BgL_arg2036z00_2711 =
																									BGl_errorz00zz__errorz00
																									(BGl_string2867z00zzsaw_node2rtlz00,
																									BGl_string2868z00zzsaw_node2rtlz00,
																									BgL_nz00_2712);
																							}
																					}
																			}
																			BgL_newtail1683z00_2709 =
																				MAKE_YOUNG_PAIR(BgL_arg2036z00_2711,
																				BNIL);
																		}
																		SET_CDR(BgL_tail1682z00_2706,
																			BgL_newtail1683z00_2709);
																		{	/* SawMill/node2rtl.scm 281 */
																			obj_t BgL_arg2034z00_2710;

																			BgL_arg2034z00_2710 =
																				CDR(((obj_t) BgL_l1679z00_2705));
																			{
																				obj_t BgL_tail1682z00_7498;
																				obj_t BgL_l1679z00_7497;

																				BgL_l1679z00_7497 = BgL_arg2034z00_2710;
																				BgL_tail1682z00_7498 =
																					BgL_newtail1683z00_2709;
																				BgL_tail1682z00_2706 =
																					BgL_tail1682z00_7498;
																				BgL_l1679z00_2705 = BgL_l1679z00_7497;
																				goto
																					BgL_zc3z04anonymousza32032ze3z87_2707;
																			}
																		}
																	}
															}
														}
												}
										}
									}
									BgL_newtail1688z00_2696 =
										MAKE_YOUNG_PAIR(BgL_arg2030z00_2698, BNIL);
								}
								SET_CDR(BgL_tail1687z00_2693, BgL_newtail1688z00_2696);
								{	/* SawMill/node2rtl.scm 277 */
									obj_t BgL_arg2029z00_2697;

									BgL_arg2029z00_2697 = CDR(((obj_t) BgL_l1684z00_2692));
									{
										obj_t BgL_tail1687z00_7504;
										obj_t BgL_l1684z00_7503;

										BgL_l1684z00_7503 = BgL_arg2029z00_2697;
										BgL_tail1687z00_7504 = BgL_newtail1688z00_2696;
										BgL_tail1687z00_2693 = BgL_tail1687z00_7504;
										BgL_l1684z00_2692 = BgL_l1684z00_7503;
										goto BgL_zc3z04anonymousza32026ze3z87_2694;
									}
								}
							}
					}
				}
		}

	}



/* compile-label-call */
	BgL_areaz00_bglt
		BGl_compilezd2labelzd2callz00zzsaw_node2rtlz00(BgL_localz00_bglt
		BgL_vz00_148, obj_t BgL_argsz00_149)
	{
		{	/* SawMill/node2rtl.scm 294 */
			{	/* SawMill/node2rtl.scm 301 */
				obj_t BgL_regsz00_2718;

				if (NULLP(BgL_argsz00_149))
					{	/* SawMill/node2rtl.scm 301 */
						BgL_regsz00_2718 = BNIL;
					}
				else
					{	/* SawMill/node2rtl.scm 301 */
						obj_t BgL_head1691z00_2768;

						BgL_head1691z00_2768 = MAKE_YOUNG_PAIR(BNIL, BNIL);
						{
							obj_t BgL_l1689z00_2770;
							obj_t BgL_tail1692z00_2771;

							BgL_l1689z00_2770 = BgL_argsz00_149;
							BgL_tail1692z00_2771 = BgL_head1691z00_2768;
						BgL_zc3z04anonymousza32065ze3z87_2772:
							if (NULLP(BgL_l1689z00_2770))
								{	/* SawMill/node2rtl.scm 301 */
									BgL_regsz00_2718 = CDR(BgL_head1691z00_2768);
								}
							else
								{	/* SawMill/node2rtl.scm 301 */
									obj_t BgL_newtail1693z00_2774;

									{	/* SawMill/node2rtl.scm 301 */
										BgL_rtl_regz00_bglt BgL_arg2068z00_2776;

										{	/* SawMill/node2rtl.scm 301 */
											obj_t BgL_az00_2777;

											BgL_az00_2777 = CAR(((obj_t) BgL_l1689z00_2770));
											BgL_arg2068z00_2776 =
												BGl_newzd2regzd2zzsaw_node2rtlz00(
												((BgL_nodez00_bglt) BgL_az00_2777));
										}
										BgL_newtail1693z00_2774 =
											MAKE_YOUNG_PAIR(((obj_t) BgL_arg2068z00_2776), BNIL);
									}
									SET_CDR(BgL_tail1692z00_2771, BgL_newtail1693z00_2774);
									{	/* SawMill/node2rtl.scm 301 */
										obj_t BgL_arg2067z00_2775;

										BgL_arg2067z00_2775 = CDR(((obj_t) BgL_l1689z00_2770));
										{
											obj_t BgL_tail1692z00_7521;
											obj_t BgL_l1689z00_7520;

											BgL_l1689z00_7520 = BgL_arg2067z00_2775;
											BgL_tail1692z00_7521 = BgL_newtail1693z00_2774;
											BgL_tail1692z00_2771 = BgL_tail1692z00_7521;
											BgL_l1689z00_2770 = BgL_l1689z00_7520;
											goto BgL_zc3z04anonymousza32065ze3z87_2772;
										}
									}
								}
						}
					}
				{	/* SawMill/node2rtl.scm 302 */
					obj_t BgL_paramsz00_2719;

					BgL_paramsz00_2719 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_vz00_148)))->
										BgL_valuez00))))->BgL_argsz00);
					{	/* SawMill/node2rtl.scm 304 */
						BgL_areaz00_bglt BgL_arg2039z00_2720;
						BgL_areaz00_bglt BgL_arg2040z00_2721;

						{	/* SawMill/node2rtl.scm 304 */
							obj_t BgL_arg2041z00_2722;

							if (NULLP(BgL_argsz00_149))
								{	/* SawMill/node2rtl.scm 304 */
									BgL_arg2041z00_2722 = BNIL;
								}
							else
								{	/* SawMill/node2rtl.scm 304 */
									obj_t BgL_head1696z00_2726;

									BgL_head1696z00_2726 = MAKE_YOUNG_PAIR(BNIL, BNIL);
									{
										obj_t BgL_ll1694z00_2728;
										obj_t BgL_ll1695z00_2729;
										obj_t BgL_tail1697z00_2730;

										BgL_ll1694z00_2728 = BgL_argsz00_149;
										BgL_ll1695z00_2729 = BgL_regsz00_2718;
										BgL_tail1697z00_2730 = BgL_head1696z00_2726;
									BgL_zc3z04anonymousza32043ze3z87_2731:
										if (NULLP(BgL_ll1694z00_2728))
											{	/* SawMill/node2rtl.scm 304 */
												BgL_arg2041z00_2722 = CDR(BgL_head1696z00_2726);
											}
										else
											{	/* SawMill/node2rtl.scm 304 */
												obj_t BgL_newtail1698z00_2733;

												{	/* SawMill/node2rtl.scm 304 */
													BgL_areaz00_bglt BgL_arg2047z00_2736;

													{	/* SawMill/node2rtl.scm 304 */
														obj_t BgL_ez00_2737;
														obj_t BgL_rz00_2738;

														BgL_ez00_2737 = CAR(((obj_t) BgL_ll1694z00_2728));
														BgL_rz00_2738 = CAR(((obj_t) BgL_ll1695z00_2729));
														{	/* SawMill/node2rtl.scm 304 */
															BgL_areaz00_bglt BgL_res2830z00_4594;

															{	/* SawMill/node2rtl.scm 187 */
																BgL_areaz00_bglt BgL_az00_4589;

																BgL_az00_4589 =
																	BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(
																	((BgL_nodez00_bglt) BgL_ez00_2737));
																{	/* SawMill/node2rtl.scm 116 */
																	BgL_blockz00_bglt BgL_arg1910z00_4592;

																	BgL_arg1910z00_4592 =
																		(((BgL_areaz00_bglt)
																			COBJECT(BgL_az00_4589))->BgL_exitz00);
																	BGl_bdestinationz12z12zzsaw_node2rtlz00
																		(BgL_arg1910z00_4592,
																		((BgL_rtl_regz00_bglt) BgL_rz00_2738));
																}
																BgL_res2830z00_4594 = BgL_az00_4589;
															}
															BgL_arg2047z00_2736 = BgL_res2830z00_4594;
														}
													}
													BgL_newtail1698z00_2733 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_arg2047z00_2736), BNIL);
												}
												SET_CDR(BgL_tail1697z00_2730, BgL_newtail1698z00_2733);
												{	/* SawMill/node2rtl.scm 304 */
													obj_t BgL_arg2045z00_2734;
													obj_t BgL_arg2046z00_2735;

													BgL_arg2045z00_2734 =
														CDR(((obj_t) BgL_ll1694z00_2728));
													BgL_arg2046z00_2735 =
														CDR(((obj_t) BgL_ll1695z00_2729));
													{
														obj_t BgL_tail1697z00_7550;
														obj_t BgL_ll1695z00_7549;
														obj_t BgL_ll1694z00_7548;

														BgL_ll1694z00_7548 = BgL_arg2045z00_2734;
														BgL_ll1695z00_7549 = BgL_arg2046z00_2735;
														BgL_tail1697z00_7550 = BgL_newtail1698z00_2733;
														BgL_tail1697z00_2730 = BgL_tail1697z00_7550;
														BgL_ll1695z00_2729 = BgL_ll1695z00_7549;
														BgL_ll1694z00_2728 = BgL_ll1694z00_7548;
														goto BgL_zc3z04anonymousza32043ze3z87_2731;
													}
												}
											}
									}
								}
							BgL_arg2039z00_2720 =
								BGl_linkza2za2zzsaw_node2rtlz00(BgL_arg2041z00_2722);
						}
						{	/* SawMill/node2rtl.scm 306 */
							BgL_areaz00_bglt BgL_arg2048z00_2740;
							BgL_areaz00_bglt BgL_arg2049z00_2741;

							{	/* SawMill/node2rtl.scm 306 */
								obj_t BgL_arg2050z00_2742;

								{	/* SawMill/node2rtl.scm 306 */
									obj_t BgL_ll1700z00_2743;
									obj_t BgL_ll1701z00_2744;

									BgL_ll1700z00_2743 = bgl_reverse(BgL_regsz00_2718);
									BgL_ll1701z00_2744 = bgl_reverse(BgL_paramsz00_2719);
									if (NULLP(BgL_ll1700z00_2743))
										{	/* SawMill/node2rtl.scm 306 */
											BgL_arg2050z00_2742 = BNIL;
										}
									else
										{	/* SawMill/node2rtl.scm 306 */
											obj_t BgL_head1702z00_2746;

											{	/* SawMill/node2rtl.scm 306 */
												BgL_areaz00_bglt BgL_arg2060z00_2762;

												{	/* SawMill/node2rtl.scm 306 */
													obj_t BgL_arg2061z00_2763;
													obj_t BgL_arg2062z00_2764;

													BgL_arg2061z00_2763 = CAR(BgL_ll1700z00_2743);
													BgL_arg2062z00_2764 =
														CAR(((obj_t) BgL_ll1701z00_2744));
													BgL_arg2060z00_2762 =
														BGl_moveze70ze7zzsaw_node2rtlz00(
														((BgL_rtl_regz00_bglt) BgL_arg2061z00_2763),
														((BgL_localz00_bglt) BgL_arg2062z00_2764));
												}
												BgL_head1702z00_2746 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg2060z00_2762), BNIL);
											}
											{	/* SawMill/node2rtl.scm 306 */
												obj_t BgL_g1706z00_2747;
												obj_t BgL_g1707z00_2748;

												BgL_g1706z00_2747 = CDR(BgL_ll1700z00_2743);
												BgL_g1707z00_2748 = CDR(((obj_t) BgL_ll1701z00_2744));
												{
													obj_t BgL_ll1700z00_2750;
													obj_t BgL_ll1701z00_2751;
													obj_t BgL_tail1703z00_2752;

													BgL_ll1700z00_2750 = BgL_g1706z00_2747;
													BgL_ll1701z00_2751 = BgL_g1707z00_2748;
													BgL_tail1703z00_2752 = BgL_head1702z00_2746;
												BgL_zc3z04anonymousza32052ze3z87_2753:
													if (NULLP(BgL_ll1700z00_2750))
														{	/* SawMill/node2rtl.scm 306 */
															BgL_arg2050z00_2742 = BgL_head1702z00_2746;
														}
													else
														{	/* SawMill/node2rtl.scm 306 */
															obj_t BgL_newtail1704z00_2755;

															{	/* SawMill/node2rtl.scm 306 */
																BgL_areaz00_bglt BgL_arg2057z00_2758;

																{	/* SawMill/node2rtl.scm 306 */
																	obj_t BgL_arg2058z00_2759;
																	obj_t BgL_arg2059z00_2760;

																	BgL_arg2058z00_2759 =
																		CAR(((obj_t) BgL_ll1700z00_2750));
																	BgL_arg2059z00_2760 =
																		CAR(((obj_t) BgL_ll1701z00_2751));
																	BgL_arg2057z00_2758 =
																		BGl_moveze70ze7zzsaw_node2rtlz00(
																		((BgL_rtl_regz00_bglt) BgL_arg2058z00_2759),
																		((BgL_localz00_bglt) BgL_arg2059z00_2760));
																}
																BgL_newtail1704z00_2755 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_arg2057z00_2758), BNIL);
															}
															SET_CDR(BgL_tail1703z00_2752,
																BgL_newtail1704z00_2755);
															{	/* SawMill/node2rtl.scm 306 */
																obj_t BgL_arg2055z00_2756;
																obj_t BgL_arg2056z00_2757;

																BgL_arg2055z00_2756 =
																	CDR(((obj_t) BgL_ll1700z00_2750));
																BgL_arg2056z00_2757 =
																	CDR(((obj_t) BgL_ll1701z00_2751));
																{
																	obj_t BgL_tail1703z00_7585;
																	obj_t BgL_ll1701z00_7584;
																	obj_t BgL_ll1700z00_7583;

																	BgL_ll1700z00_7583 = BgL_arg2055z00_2756;
																	BgL_ll1701z00_7584 = BgL_arg2056z00_2757;
																	BgL_tail1703z00_7585 =
																		BgL_newtail1704z00_2755;
																	BgL_tail1703z00_2752 = BgL_tail1703z00_7585;
																	BgL_ll1701z00_2751 = BgL_ll1701z00_7584;
																	BgL_ll1700z00_2750 = BgL_ll1700z00_7583;
																	goto BgL_zc3z04anonymousza32052ze3z87_2753;
																}
															}
														}
												}
											}
										}
								}
								BgL_arg2048z00_2740 =
									BGl_linkza2za2zzsaw_node2rtlz00(BgL_arg2050z00_2742);
							}
							BgL_arg2049z00_2741 =
								BGl_localzd2ze3codez31zzsaw_node2rtlz00(BgL_vz00_148);
							BgL_arg2040z00_2721 =
								BGl_linkz00zzsaw_node2rtlz00(BgL_arg2048z00_2740,
								BgL_arg2049z00_2741);
						}
						return
							BGl_linkz00zzsaw_node2rtlz00(BgL_arg2039z00_2720,
							BgL_arg2040z00_2721);
					}
				}
			}
		}

	}



/* move~0 */
	BgL_areaz00_bglt BGl_moveze70ze7zzsaw_node2rtlz00(BgL_rtl_regz00_bglt
		BgL_rz00_2779, BgL_localz00_bglt BgL_vz00_2780)
	{
		{	/* SawMill/node2rtl.scm 300 */
			{	/* SawMill/node2rtl.scm 298 */
				BgL_areaz00_bglt BgL_az00_2782;

				{	/* SawMill/node2rtl.scm 298 */
					BgL_rtl_movz00_bglt BgL_arg2072z00_2784;

					{	/* SawMill/node2rtl.scm 298 */
						BgL_rtl_movz00_bglt BgL_new1280z00_2786;

						{	/* SawMill/node2rtl.scm 298 */
							BgL_rtl_movz00_bglt BgL_new1279z00_2787;

							BgL_new1279z00_2787 =
								((BgL_rtl_movz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_movz00_bgl))));
							{	/* SawMill/node2rtl.scm 298 */
								long BgL_arg2074z00_2788;

								BgL_arg2074z00_2788 =
									BGL_CLASS_NUM(BGl_rtl_movz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1279z00_2787),
									BgL_arg2074z00_2788);
							}
							BgL_new1280z00_2786 = BgL_new1279z00_2787;
						}
						((((BgL_rtl_funz00_bglt) COBJECT(
										((BgL_rtl_funz00_bglt) BgL_new1280z00_2786)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						BgL_arg2072z00_2784 = BgL_new1280z00_2786;
					}
					{	/* SawMill/node2rtl.scm 298 */
						obj_t BgL_list2073z00_2785;

						BgL_list2073z00_2785 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_rz00_2779), BNIL);
						BgL_az00_2782 =
							BGl_singleza2za2zzsaw_node2rtlz00(BFALSE,
							((BgL_rtl_funz00_bglt) BgL_arg2072z00_2784),
							BgL_list2073z00_2785);
				}}
				BGl_bdestinationz12z12zzsaw_node2rtlz00(
					(((BgL_areaz00_bglt) COBJECT(BgL_az00_2782))->BgL_exitz00),
					BGl_localzd2ze3regz31zzsaw_node2rtlz00(BgL_vz00_2780));
				return BgL_az00_2782;
			}
		}

	}



/* local->code */
	BgL_areaz00_bglt BGl_localzd2ze3codez31zzsaw_node2rtlz00(BgL_localz00_bglt
		BgL_vz00_150)
	{
		{	/* SawMill/node2rtl.scm 310 */
			{	/* SawMill/node2rtl.scm 311 */
				bool_t BgL_test3105z00_7603;

				{	/* SawMill/node2rtl.scm 311 */
					obj_t BgL_classz00_4607;

					BgL_classz00_4607 = BGl_rlocalz00zzsaw_node2rtlz00;
					{	/* SawMill/node2rtl.scm 311 */
						obj_t BgL_oclassz00_4609;

						{	/* SawMill/node2rtl.scm 311 */
							obj_t BgL_arg1815z00_4611;
							long BgL_arg1816z00_4612;

							BgL_arg1815z00_4611 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/node2rtl.scm 311 */
								long BgL_arg1817z00_4613;

								BgL_arg1817z00_4613 =
									BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_vz00_150));
								BgL_arg1816z00_4612 = (BgL_arg1817z00_4613 - OBJECT_TYPE);
							}
							BgL_oclassz00_4609 =
								VECTOR_REF(BgL_arg1815z00_4611, BgL_arg1816z00_4612);
						}
						BgL_test3105z00_7603 = (BgL_oclassz00_4609 == BgL_classz00_4607);
				}}
				if (BgL_test3105z00_7603)
					{	/* SawMill/node2rtl.scm 320 */
						obj_t BgL_arg2076z00_2791;

						{
							BgL_rlocalz00_bglt BgL_auxz00_7610;

							{
								obj_t BgL_auxz00_7611;

								{	/* SawMill/node2rtl.scm 320 */
									BgL_objectz00_bglt BgL_tmpz00_7612;

									BgL_tmpz00_7612 =
										((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_vz00_150));
									BgL_auxz00_7611 = BGL_OBJECT_WIDENING(BgL_tmpz00_7612);
								}
								BgL_auxz00_7610 = ((BgL_rlocalz00_bglt) BgL_auxz00_7611);
							}
							BgL_arg2076z00_2791 =
								(((BgL_rlocalz00_bglt) COBJECT(BgL_auxz00_7610))->BgL_codez00);
						}
						return
							BGl_unlinkz00zzsaw_node2rtlz00(
							((BgL_areaz00_bglt) BgL_arg2076z00_2791));
					}
				else
					{	/* SawMill/node2rtl.scm 313 */
						BgL_areaz00_bglt BgL_nopz00_2792;
						obj_t BgL_bodyz00_2793;

						{	/* SawMill/node2rtl.scm 313 */
							BgL_rtl_nopz00_bglt BgL_arg2079z00_2800;

							{	/* SawMill/node2rtl.scm 313 */
								BgL_rtl_nopz00_bglt BgL_new1282z00_2802;

								{	/* SawMill/node2rtl.scm 313 */
									BgL_rtl_nopz00_bglt BgL_new1281z00_2803;

									BgL_new1281z00_2803 =
										((BgL_rtl_nopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_rtl_nopz00_bgl))));
									{	/* SawMill/node2rtl.scm 313 */
										long BgL_arg2081z00_2804;

										BgL_arg2081z00_2804 =
											BGL_CLASS_NUM(BGl_rtl_nopz00zzsaw_defsz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1281z00_2803),
											BgL_arg2081z00_2804);
									}
									BgL_new1282z00_2802 = BgL_new1281z00_2803;
								}
								((((BgL_rtl_funz00_bglt) COBJECT(
												((BgL_rtl_funz00_bglt) BgL_new1282z00_2802)))->
										BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
								BgL_arg2079z00_2800 = BgL_new1282z00_2802;
							}
							BgL_nopz00_2792 =
								BGl_singleza2za2zzsaw_node2rtlz00(BFALSE,
								((BgL_rtl_funz00_bglt) BgL_arg2079z00_2800), BNIL);
						}
						BgL_bodyz00_2793 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_vz00_150)))->
											BgL_valuez00))))->BgL_bodyz00);
						{	/* SawMill/node2rtl.scm 316 */
							BgL_rlocalz00_bglt BgL_wide1285z00_2796;

							BgL_wide1285z00_2796 =
								((BgL_rlocalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rlocalz00_bgl))));
							{	/* SawMill/node2rtl.scm 316 */
								obj_t BgL_auxz00_7636;
								BgL_objectz00_bglt BgL_tmpz00_7633;

								BgL_auxz00_7636 = ((obj_t) BgL_wide1285z00_2796);
								BgL_tmpz00_7633 =
									((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_vz00_150));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7633, BgL_auxz00_7636);
							}
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_vz00_150));
							{	/* SawMill/node2rtl.scm 316 */
								long BgL_arg2077z00_2797;

								BgL_arg2077z00_2797 =
									BGL_CLASS_NUM(BGl_rlocalz00zzsaw_node2rtlz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_vz00_150)), BgL_arg2077z00_2797);
							}
							((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_vz00_150));
						}
						{
							BgL_rlocalz00_bglt BgL_auxz00_7647;

							{
								obj_t BgL_auxz00_7648;

								{	/* SawMill/node2rtl.scm 316 */
									BgL_objectz00_bglt BgL_tmpz00_7649;

									BgL_tmpz00_7649 =
										((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_vz00_150));
									BgL_auxz00_7648 = BGL_OBJECT_WIDENING(BgL_tmpz00_7649);
								}
								BgL_auxz00_7647 = ((BgL_rlocalz00_bglt) BgL_auxz00_7648);
							}
							((((BgL_rlocalz00_bglt) COBJECT(BgL_auxz00_7647))->BgL_regz00) =
								((obj_t) BFALSE), BUNSPEC);
						}
						{
							BgL_rlocalz00_bglt BgL_auxz00_7655;

							{
								obj_t BgL_auxz00_7656;

								{	/* SawMill/node2rtl.scm 316 */
									BgL_objectz00_bglt BgL_tmpz00_7657;

									BgL_tmpz00_7657 =
										((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_vz00_150));
									BgL_auxz00_7656 = BGL_OBJECT_WIDENING(BgL_tmpz00_7657);
								}
								BgL_auxz00_7655 = ((BgL_rlocalz00_bglt) BgL_auxz00_7656);
							}
							((((BgL_rlocalz00_bglt) COBJECT(BgL_auxz00_7655))->BgL_codez00) =
								((obj_t) ((obj_t) BgL_nopz00_2792)), BUNSPEC);
						}
						((BgL_localz00_bglt) BgL_vz00_150);
						{	/* SawMill/node2rtl.scm 318 */
							BgL_areaz00_bglt BgL_arg2078z00_2799;

							BgL_arg2078z00_2799 =
								BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(
								((BgL_nodez00_bglt) BgL_bodyz00_2793));
							return
								BGl_linkz00zzsaw_node2rtlz00(BgL_nopz00_2792,
								BgL_arg2078z00_2799);
						}
					}
			}
		}

	}



/* imperative? */
	obj_t BGl_imperativezf3zf3zzsaw_node2rtlz00(BgL_appz00_bglt BgL_ez00_152,
		BgL_globalz00_bglt BgL_vz00_153, obj_t BgL_argsz00_154)
	{
		{	/* SawMill/node2rtl.scm 333 */
			{	/* SawMill/node2rtl.scm 334 */
				obj_t BgL_idz00_2806;

				BgL_idz00_2806 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_vz00_153)))->BgL_idz00);
				if ((BgL_idz00_2806 == CNST_TABLE_REF(2)))
					{	/* SawMill/node2rtl.scm 337 */
						obj_t BgL_az00_2807;

						{	/* SawMill/node2rtl.scm 337 */
							bool_t BgL_test3107z00_7673;

							{	/* SawMill/node2rtl.scm 337 */
								obj_t BgL_arg2093z00_2818;

								BgL_arg2093z00_2818 = CAR(((obj_t) BgL_argsz00_154));
								{	/* SawMill/node2rtl.scm 337 */
									obj_t BgL_classz00_4635;

									BgL_classz00_4635 = BGl_pragmaz00zzast_nodez00;
									if (BGL_OBJECTP(BgL_arg2093z00_2818))
										{	/* SawMill/node2rtl.scm 337 */
											BgL_objectz00_bglt BgL_arg1807z00_4637;

											BgL_arg1807z00_4637 =
												(BgL_objectz00_bglt) (BgL_arg2093z00_2818);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawMill/node2rtl.scm 337 */
													long BgL_idxz00_4643;

													BgL_idxz00_4643 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4637);
													BgL_test3107z00_7673 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_4643 + 4L)) == BgL_classz00_4635);
												}
											else
												{	/* SawMill/node2rtl.scm 337 */
													bool_t BgL_res2831z00_4668;

													{	/* SawMill/node2rtl.scm 337 */
														obj_t BgL_oclassz00_4651;

														{	/* SawMill/node2rtl.scm 337 */
															obj_t BgL_arg1815z00_4659;
															long BgL_arg1816z00_4660;

															BgL_arg1815z00_4659 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawMill/node2rtl.scm 337 */
																long BgL_arg1817z00_4661;

																BgL_arg1817z00_4661 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4637);
																BgL_arg1816z00_4660 =
																	(BgL_arg1817z00_4661 - OBJECT_TYPE);
															}
															BgL_oclassz00_4651 =
																VECTOR_REF(BgL_arg1815z00_4659,
																BgL_arg1816z00_4660);
														}
														{	/* SawMill/node2rtl.scm 337 */
															bool_t BgL__ortest_1115z00_4652;

															BgL__ortest_1115z00_4652 =
																(BgL_classz00_4635 == BgL_oclassz00_4651);
															if (BgL__ortest_1115z00_4652)
																{	/* SawMill/node2rtl.scm 337 */
																	BgL_res2831z00_4668 =
																		BgL__ortest_1115z00_4652;
																}
															else
																{	/* SawMill/node2rtl.scm 337 */
																	long BgL_odepthz00_4653;

																	{	/* SawMill/node2rtl.scm 337 */
																		obj_t BgL_arg1804z00_4654;

																		BgL_arg1804z00_4654 = (BgL_oclassz00_4651);
																		BgL_odepthz00_4653 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_4654);
																	}
																	if ((4L < BgL_odepthz00_4653))
																		{	/* SawMill/node2rtl.scm 337 */
																			obj_t BgL_arg1802z00_4656;

																			{	/* SawMill/node2rtl.scm 337 */
																				obj_t BgL_arg1803z00_4657;

																				BgL_arg1803z00_4657 =
																					(BgL_oclassz00_4651);
																				BgL_arg1802z00_4656 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_4657, 4L);
																			}
																			BgL_res2831z00_4668 =
																				(BgL_arg1802z00_4656 ==
																				BgL_classz00_4635);
																		}
																	else
																		{	/* SawMill/node2rtl.scm 337 */
																			BgL_res2831z00_4668 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test3107z00_7673 = BgL_res2831z00_4668;
												}
										}
									else
										{	/* SawMill/node2rtl.scm 337 */
											BgL_test3107z00_7673 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test3107z00_7673)
								{	/* SawMill/node2rtl.scm 338 */
									obj_t BgL_pairz00_4671;

									BgL_pairz00_4671 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt)
													((BgL_pragmaz00_bglt)
														CAR(((obj_t) BgL_argsz00_154))))))->BgL_exprza2za2);
									BgL_az00_2807 = CAR(BgL_pairz00_4671);
								}
							else
								{	/* SawMill/node2rtl.scm 337 */
									BgL_az00_2807 = CAR(((obj_t) BgL_argsz00_154));
								}
						}
						{	/* SawMill/node2rtl.scm 341 */
							BgL_rtl_globalrefz00_bglt BgL_arg2083z00_2808;

							{	/* SawMill/node2rtl.scm 341 */
								BgL_rtl_globalrefz00_bglt BgL_new1294z00_2810;

								{	/* SawMill/node2rtl.scm 341 */
									BgL_rtl_globalrefz00_bglt BgL_new1293z00_2812;

									BgL_new1293z00_2812 =
										((BgL_rtl_globalrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_rtl_globalrefz00_bgl))));
									{	/* SawMill/node2rtl.scm 341 */
										long BgL_arg2087z00_2813;

										BgL_arg2087z00_2813 =
											BGL_CLASS_NUM(BGl_rtl_globalrefz00zzsaw_defsz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1293z00_2812),
											BgL_arg2087z00_2813);
									}
									BgL_new1294z00_2810 = BgL_new1293z00_2812;
								}
								((((BgL_rtl_funz00_bglt) COBJECT(
												((BgL_rtl_funz00_bglt) BgL_new1294z00_2810)))->
										BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
								{
									BgL_globalz00_bglt BgL_auxz00_7712;

									{	/* SawMill/node2rtl.scm 342 */
										BgL_variablez00_bglt BgL_arg2086z00_2811;

										BgL_arg2086z00_2811 =
											(((BgL_varz00_bglt) COBJECT(
													((BgL_varz00_bglt) BgL_az00_2807)))->BgL_variablez00);
										BgL_auxz00_7712 =
											BGl_getzd2globalzd2zzsaw_node2rtlz00(
											((BgL_globalz00_bglt) BgL_arg2086z00_2811));
									}
									((((BgL_rtl_globalrefz00_bglt) COBJECT(BgL_new1294z00_2810))->
											BgL_varz00) =
										((BgL_globalz00_bglt) BgL_auxz00_7712), BUNSPEC);
								}
								BgL_arg2083z00_2808 = BgL_new1294z00_2810;
							}
							return
								((obj_t)
								BGl_callza2za2zzsaw_node2rtlz00(
									((obj_t) BgL_ez00_152),
									((BgL_rtl_funz00_bglt) BgL_arg2083z00_2808), BNIL));
						}
					}
				else
					{	/* SawMill/node2rtl.scm 336 */
						return BFALSE;
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_node2rtlz00(void)
	{
		{	/* SawMill/node2rtl.scm 1 */
			{	/* SawMill/node2rtl.scm 20 */
				obj_t BgL_arg2097z00_2822;
				obj_t BgL_arg2098z00_2823;

				{	/* SawMill/node2rtl.scm 20 */
					obj_t BgL_v1708z00_2835;

					BgL_v1708z00_2835 = create_vector(2L);
					{	/* SawMill/node2rtl.scm 20 */
						obj_t BgL_arg2104z00_2836;

						BgL_arg2104z00_2836 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(3),
							BGl_proc2870z00zzsaw_node2rtlz00,
							BGl_proc2869z00zzsaw_node2rtlz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_blockz00zzsaw_defsz00);
						VECTOR_SET(BgL_v1708z00_2835, 0L, BgL_arg2104z00_2836);
					}
					{	/* SawMill/node2rtl.scm 20 */
						obj_t BgL_arg2109z00_2846;

						BgL_arg2109z00_2846 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc2872z00zzsaw_node2rtlz00,
							BGl_proc2871z00zzsaw_node2rtlz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_blockz00zzsaw_defsz00);
						VECTOR_SET(BgL_v1708z00_2835, 1L, BgL_arg2109z00_2846);
					}
					BgL_arg2097z00_2822 = BgL_v1708z00_2835;
				}
				{	/* SawMill/node2rtl.scm 20 */
					obj_t BgL_v1709z00_2856;

					BgL_v1709z00_2856 = create_vector(0L);
					BgL_arg2098z00_2823 = BgL_v1709z00_2856;
				}
				BGl_areaz00zzsaw_node2rtlz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(5),
					CNST_TABLE_REF(6), BGl_objectz00zz__objectz00, 23170L,
					BGl_proc2875z00zzsaw_node2rtlz00, BGl_proc2874z00zzsaw_node2rtlz00,
					BFALSE, BGl_proc2873z00zzsaw_node2rtlz00, BFALSE, BgL_arg2097z00_2822,
					BgL_arg2098z00_2823);
			}
			{	/* SawMill/node2rtl.scm 21 */
				obj_t BgL_arg2118z00_2864;
				obj_t BgL_arg2119z00_2865;

				{	/* SawMill/node2rtl.scm 21 */
					obj_t BgL_v1710z00_2890;

					BgL_v1710z00_2890 = create_vector(0L);
					BgL_arg2118z00_2864 = BgL_v1710z00_2890;
				}
				{	/* SawMill/node2rtl.scm 21 */
					obj_t BgL_v1711z00_2891;

					BgL_v1711z00_2891 = create_vector(0L);
					BgL_arg2119z00_2865 = BgL_v1711z00_2891;
				}
				BGl_reversedz00zzsaw_node2rtlz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(7),
					CNST_TABLE_REF(6), BGl_blockz00zzsaw_defsz00, 28990L,
					BGl_proc2879z00zzsaw_node2rtlz00, BGl_proc2878z00zzsaw_node2rtlz00,
					BFALSE, BGl_proc2877z00zzsaw_node2rtlz00,
					BGl_proc2876z00zzsaw_node2rtlz00, BgL_arg2118z00_2864,
					BgL_arg2119z00_2865);
			}
			{	/* SawMill/node2rtl.scm 22 */
				obj_t BgL_arg2135z00_2900;
				obj_t BgL_arg2136z00_2901;

				{	/* SawMill/node2rtl.scm 22 */
					obj_t BgL_v1712z00_2937;

					BgL_v1712z00_2937 = create_vector(2L);
					{	/* SawMill/node2rtl.scm 22 */
						obj_t BgL_arg2147z00_2938;

						BgL_arg2147z00_2938 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc2881z00zzsaw_node2rtlz00,
							BGl_proc2880z00zzsaw_node2rtlz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(9));
						VECTOR_SET(BgL_v1712z00_2937, 0L, BgL_arg2147z00_2938);
					}
					{	/* SawMill/node2rtl.scm 22 */
						obj_t BgL_arg2152z00_2948;

						BgL_arg2152z00_2948 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(10),
							BGl_proc2883z00zzsaw_node2rtlz00,
							BGl_proc2882z00zzsaw_node2rtlz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(9));
						VECTOR_SET(BgL_v1712z00_2937, 1L, BgL_arg2152z00_2948);
					}
					BgL_arg2135z00_2900 = BgL_v1712z00_2937;
				}
				{	/* SawMill/node2rtl.scm 22 */
					obj_t BgL_v1713z00_2958;

					BgL_v1713z00_2958 = create_vector(0L);
					BgL_arg2136z00_2901 = BgL_v1713z00_2958;
				}
				BGl_rlocalz00zzsaw_node2rtlz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(11),
					CNST_TABLE_REF(6), BGl_localz00zzast_varz00, 58620L,
					BGl_proc2887z00zzsaw_node2rtlz00, BGl_proc2886z00zzsaw_node2rtlz00,
					BFALSE, BGl_proc2885z00zzsaw_node2rtlz00,
					BGl_proc2884z00zzsaw_node2rtlz00, BgL_arg2135z00_2900,
					BgL_arg2136z00_2901);
			}
			{	/* SawMill/node2rtl.scm 23 */
				obj_t BgL_arg2162z00_2967;
				obj_t BgL_arg2163z00_2968;

				{	/* SawMill/node2rtl.scm 23 */
					obj_t BgL_v1714z00_2994;

					BgL_v1714z00_2994 = create_vector(2L);
					{	/* SawMill/node2rtl.scm 23 */
						obj_t BgL_arg2174z00_2995;

						BgL_arg2174z00_2995 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(12),
							BGl_proc2889z00zzsaw_node2rtlz00,
							BGl_proc2888z00zzsaw_node2rtlz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_blockz00zzsaw_defsz00);
						VECTOR_SET(BgL_v1714z00_2994, 0L, BgL_arg2174z00_2995);
					}
					{	/* SawMill/node2rtl.scm 23 */
						obj_t BgL_arg2179z00_3005;

						BgL_arg2179z00_3005 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(13),
							BGl_proc2891z00zzsaw_node2rtlz00,
							BGl_proc2890z00zzsaw_node2rtlz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, BGl_rtl_regz00zzsaw_defsz00);
						VECTOR_SET(BgL_v1714z00_2994, 1L, BgL_arg2179z00_3005);
					}
					BgL_arg2162z00_2967 = BgL_v1714z00_2994;
				}
				{	/* SawMill/node2rtl.scm 23 */
					obj_t BgL_v1715z00_3015;

					BgL_v1715z00_3015 = create_vector(0L);
					BgL_arg2163z00_2968 = BgL_v1715z00_3015;
				}
				return (BGl_retblockzf2tozf2zzsaw_node2rtlz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(14),
						CNST_TABLE_REF(6), BGl_retblockz00zzast_nodez00, 53023L,
						BGl_proc2895z00zzsaw_node2rtlz00, BGl_proc2894z00zzsaw_node2rtlz00,
						BFALSE, BGl_proc2893z00zzsaw_node2rtlz00,
						BGl_proc2892z00zzsaw_node2rtlz00, BgL_arg2162z00_2967,
						BgL_arg2163z00_2968), BUNSPEC);
			}
		}

	}



/* &lambda2170 */
	BgL_retblockz00_bglt BGl_z62lambda2170z62zzsaw_node2rtlz00(obj_t
		BgL_envz00_5826, obj_t BgL_o1199z00_5827)
	{
		{	/* SawMill/node2rtl.scm 23 */
			{	/* SawMill/node2rtl.scm 23 */
				long BgL_arg2171z00_6041;

				{	/* SawMill/node2rtl.scm 23 */
					obj_t BgL_arg2172z00_6042;

					{	/* SawMill/node2rtl.scm 23 */
						obj_t BgL_arg2173z00_6043;

						{	/* SawMill/node2rtl.scm 23 */
							obj_t BgL_arg1815z00_6044;
							long BgL_arg1816z00_6045;

							BgL_arg1815z00_6044 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/node2rtl.scm 23 */
								long BgL_arg1817z00_6046;

								BgL_arg1817z00_6046 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_retblockz00_bglt) BgL_o1199z00_5827)));
								BgL_arg1816z00_6045 = (BgL_arg1817z00_6046 - OBJECT_TYPE);
							}
							BgL_arg2173z00_6043 =
								VECTOR_REF(BgL_arg1815z00_6044, BgL_arg1816z00_6045);
						}
						BgL_arg2172z00_6042 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2173z00_6043);
					}
					{	/* SawMill/node2rtl.scm 23 */
						obj_t BgL_tmpz00_7769;

						BgL_tmpz00_7769 = ((obj_t) BgL_arg2172z00_6042);
						BgL_arg2171z00_6041 = BGL_CLASS_NUM(BgL_tmpz00_7769);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_retblockz00_bglt) BgL_o1199z00_5827)), BgL_arg2171z00_6041);
			}
			{	/* SawMill/node2rtl.scm 23 */
				BgL_objectz00_bglt BgL_tmpz00_7775;

				BgL_tmpz00_7775 =
					((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_o1199z00_5827));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7775, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_o1199z00_5827));
			return
				((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_o1199z00_5827));
		}

	}



/* &<@anonymous:2169> */
	obj_t BGl_z62zc3z04anonymousza32169ze3ze5zzsaw_node2rtlz00(obj_t
		BgL_envz00_5828, obj_t BgL_new1198z00_5829)
	{
		{	/* SawMill/node2rtl.scm 23 */
			{
				BgL_retblockz00_bglt BgL_auxz00_7783;

				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_retblockz00_bglt) BgL_new1198z00_5829))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_7787;

					{	/* SawMill/node2rtl.scm 23 */
						obj_t BgL_classz00_6048;

						BgL_classz00_6048 = BGl_typez00zztype_typez00;
						{	/* SawMill/node2rtl.scm 23 */
							obj_t BgL__ortest_1117z00_6049;

							BgL__ortest_1117z00_6049 = BGL_CLASS_NIL(BgL_classz00_6048);
							if (CBOOL(BgL__ortest_1117z00_6049))
								{	/* SawMill/node2rtl.scm 23 */
									BgL_auxz00_7787 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6049);
								}
							else
								{	/* SawMill/node2rtl.scm 23 */
									BgL_auxz00_7787 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6048));
								}
						}
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_retblockz00_bglt) BgL_new1198z00_5829))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_7787), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_7797;

					{	/* SawMill/node2rtl.scm 23 */
						obj_t BgL_classz00_6050;

						BgL_classz00_6050 = BGl_nodez00zzast_nodez00;
						{	/* SawMill/node2rtl.scm 23 */
							obj_t BgL__ortest_1117z00_6051;

							BgL__ortest_1117z00_6051 = BGL_CLASS_NIL(BgL_classz00_6050);
							if (CBOOL(BgL__ortest_1117z00_6051))
								{	/* SawMill/node2rtl.scm 23 */
									BgL_auxz00_7797 =
										((BgL_nodez00_bglt) BgL__ortest_1117z00_6051);
								}
							else
								{	/* SawMill/node2rtl.scm 23 */
									BgL_auxz00_7797 =
										((BgL_nodez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6050));
								}
						}
					}
					((((BgL_retblockz00_bglt) COBJECT(
									((BgL_retblockz00_bglt)
										((BgL_retblockz00_bglt) BgL_new1198z00_5829))))->
							BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_7797), BUNSPEC);
				}
				{
					BgL_blockz00_bglt BgL_auxz00_7814;
					BgL_retblockzf2tozf2_bglt BgL_auxz00_7807;

					{	/* SawMill/node2rtl.scm 23 */
						obj_t BgL_classz00_6052;

						BgL_classz00_6052 = BGl_blockz00zzsaw_defsz00;
						{	/* SawMill/node2rtl.scm 23 */
							obj_t BgL__ortest_1117z00_6053;

							BgL__ortest_1117z00_6053 = BGL_CLASS_NIL(BgL_classz00_6052);
							if (CBOOL(BgL__ortest_1117z00_6053))
								{	/* SawMill/node2rtl.scm 23 */
									BgL_auxz00_7814 =
										((BgL_blockz00_bglt) BgL__ortest_1117z00_6053);
								}
							else
								{	/* SawMill/node2rtl.scm 23 */
									BgL_auxz00_7814 =
										((BgL_blockz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6052));
								}
						}
					}
					{
						obj_t BgL_auxz00_7808;

						{	/* SawMill/node2rtl.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_7809;

							BgL_tmpz00_7809 =
								((BgL_objectz00_bglt)
								((BgL_retblockz00_bglt) BgL_new1198z00_5829));
							BgL_auxz00_7808 = BGL_OBJECT_WIDENING(BgL_tmpz00_7809);
						}
						BgL_auxz00_7807 = ((BgL_retblockzf2tozf2_bglt) BgL_auxz00_7808);
					}
					((((BgL_retblockzf2tozf2_bglt) COBJECT(BgL_auxz00_7807))->BgL_toz00) =
						((BgL_blockz00_bglt) BgL_auxz00_7814), BUNSPEC);
				}
				{
					BgL_rtl_regz00_bglt BgL_auxz00_7829;
					BgL_retblockzf2tozf2_bglt BgL_auxz00_7822;

					{	/* SawMill/node2rtl.scm 23 */
						obj_t BgL_classz00_6054;

						BgL_classz00_6054 = BGl_rtl_regz00zzsaw_defsz00;
						{	/* SawMill/node2rtl.scm 23 */
							obj_t BgL__ortest_1117z00_6055;

							BgL__ortest_1117z00_6055 = BGL_CLASS_NIL(BgL_classz00_6054);
							if (CBOOL(BgL__ortest_1117z00_6055))
								{	/* SawMill/node2rtl.scm 23 */
									BgL_auxz00_7829 =
										((BgL_rtl_regz00_bglt) BgL__ortest_1117z00_6055);
								}
							else
								{	/* SawMill/node2rtl.scm 23 */
									BgL_auxz00_7829 =
										((BgL_rtl_regz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6054));
								}
						}
					}
					{
						obj_t BgL_auxz00_7823;

						{	/* SawMill/node2rtl.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_7824;

							BgL_tmpz00_7824 =
								((BgL_objectz00_bglt)
								((BgL_retblockz00_bglt) BgL_new1198z00_5829));
							BgL_auxz00_7823 = BGL_OBJECT_WIDENING(BgL_tmpz00_7824);
						}
						BgL_auxz00_7822 = ((BgL_retblockzf2tozf2_bglt) BgL_auxz00_7823);
					}
					((((BgL_retblockzf2tozf2_bglt) COBJECT(BgL_auxz00_7822))->
							BgL_dstz00) = ((BgL_rtl_regz00_bglt) BgL_auxz00_7829), BUNSPEC);
				}
				BgL_auxz00_7783 = ((BgL_retblockz00_bglt) BgL_new1198z00_5829);
				return ((obj_t) BgL_auxz00_7783);
			}
		}

	}



/* &lambda2167 */
	BgL_retblockz00_bglt BGl_z62lambda2167z62zzsaw_node2rtlz00(obj_t
		BgL_envz00_5830, obj_t BgL_o1195z00_5831)
	{
		{	/* SawMill/node2rtl.scm 23 */
			{	/* SawMill/node2rtl.scm 23 */
				BgL_retblockzf2tozf2_bglt BgL_wide1197z00_6057;

				BgL_wide1197z00_6057 =
					((BgL_retblockzf2tozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_retblockzf2tozf2_bgl))));
				{	/* SawMill/node2rtl.scm 23 */
					obj_t BgL_auxz00_7844;
					BgL_objectz00_bglt BgL_tmpz00_7840;

					BgL_auxz00_7844 = ((obj_t) BgL_wide1197z00_6057);
					BgL_tmpz00_7840 =
						((BgL_objectz00_bglt)
						((BgL_retblockz00_bglt)
							((BgL_retblockz00_bglt) BgL_o1195z00_5831)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7840, BgL_auxz00_7844);
				}
				((BgL_objectz00_bglt)
					((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_o1195z00_5831)));
				{	/* SawMill/node2rtl.scm 23 */
					long BgL_arg2168z00_6058;

					BgL_arg2168z00_6058 =
						BGL_CLASS_NUM(BGl_retblockzf2tozf2zzsaw_node2rtlz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_retblockz00_bglt)
								((BgL_retblockz00_bglt) BgL_o1195z00_5831))),
						BgL_arg2168z00_6058);
				}
				return
					((BgL_retblockz00_bglt)
					((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_o1195z00_5831)));
			}
		}

	}



/* &lambda2164 */
	BgL_retblockz00_bglt BGl_z62lambda2164z62zzsaw_node2rtlz00(obj_t
		BgL_envz00_5832, obj_t BgL_loc1190z00_5833, obj_t BgL_type1191z00_5834,
		obj_t BgL_body1192z00_5835, obj_t BgL_to1193z00_5836,
		obj_t BgL_dst1194z00_5837)
	{
		{	/* SawMill/node2rtl.scm 23 */
			{	/* SawMill/node2rtl.scm 23 */
				BgL_retblockz00_bglt BgL_new1400z00_6063;

				{	/* SawMill/node2rtl.scm 23 */
					BgL_retblockz00_bglt BgL_tmp1398z00_6064;
					BgL_retblockzf2tozf2_bglt BgL_wide1399z00_6065;

					{
						BgL_retblockz00_bglt BgL_auxz00_7858;

						{	/* SawMill/node2rtl.scm 23 */
							BgL_retblockz00_bglt BgL_new1397z00_6066;

							BgL_new1397z00_6066 =
								((BgL_retblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_retblockz00_bgl))));
							{	/* SawMill/node2rtl.scm 23 */
								long BgL_arg2166z00_6067;

								BgL_arg2166z00_6067 =
									BGL_CLASS_NUM(BGl_retblockz00zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1397z00_6066),
									BgL_arg2166z00_6067);
							}
							{	/* SawMill/node2rtl.scm 23 */
								BgL_objectz00_bglt BgL_tmpz00_7863;

								BgL_tmpz00_7863 = ((BgL_objectz00_bglt) BgL_new1397z00_6066);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7863, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1397z00_6066);
							BgL_auxz00_7858 = BgL_new1397z00_6066;
						}
						BgL_tmp1398z00_6064 = ((BgL_retblockz00_bglt) BgL_auxz00_7858);
					}
					BgL_wide1399z00_6065 =
						((BgL_retblockzf2tozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_retblockzf2tozf2_bgl))));
					{	/* SawMill/node2rtl.scm 23 */
						obj_t BgL_auxz00_7871;
						BgL_objectz00_bglt BgL_tmpz00_7869;

						BgL_auxz00_7871 = ((obj_t) BgL_wide1399z00_6065);
						BgL_tmpz00_7869 = ((BgL_objectz00_bglt) BgL_tmp1398z00_6064);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7869, BgL_auxz00_7871);
					}
					((BgL_objectz00_bglt) BgL_tmp1398z00_6064);
					{	/* SawMill/node2rtl.scm 23 */
						long BgL_arg2165z00_6068;

						BgL_arg2165z00_6068 =
							BGL_CLASS_NUM(BGl_retblockzf2tozf2zzsaw_node2rtlz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1398z00_6064), BgL_arg2165z00_6068);
					}
					BgL_new1400z00_6063 = ((BgL_retblockz00_bglt) BgL_tmp1398z00_6064);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1400z00_6063)))->BgL_locz00) =
					((obj_t) BgL_loc1190z00_5833), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1400z00_6063)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1191z00_5834)),
					BUNSPEC);
				((((BgL_retblockz00_bglt) COBJECT(((BgL_retblockz00_bglt)
									BgL_new1400z00_6063)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_body1192z00_5835)),
					BUNSPEC);
				{
					BgL_retblockzf2tozf2_bglt BgL_auxz00_7887;

					{
						obj_t BgL_auxz00_7888;

						{	/* SawMill/node2rtl.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_7889;

							BgL_tmpz00_7889 = ((BgL_objectz00_bglt) BgL_new1400z00_6063);
							BgL_auxz00_7888 = BGL_OBJECT_WIDENING(BgL_tmpz00_7889);
						}
						BgL_auxz00_7887 = ((BgL_retblockzf2tozf2_bglt) BgL_auxz00_7888);
					}
					((((BgL_retblockzf2tozf2_bglt) COBJECT(BgL_auxz00_7887))->BgL_toz00) =
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_to1193z00_5836)),
						BUNSPEC);
				}
				{
					BgL_retblockzf2tozf2_bglt BgL_auxz00_7895;

					{
						obj_t BgL_auxz00_7896;

						{	/* SawMill/node2rtl.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_7897;

							BgL_tmpz00_7897 = ((BgL_objectz00_bglt) BgL_new1400z00_6063);
							BgL_auxz00_7896 = BGL_OBJECT_WIDENING(BgL_tmpz00_7897);
						}
						BgL_auxz00_7895 = ((BgL_retblockzf2tozf2_bglt) BgL_auxz00_7896);
					}
					((((BgL_retblockzf2tozf2_bglt) COBJECT(BgL_auxz00_7895))->
							BgL_dstz00) =
						((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_dst1194z00_5837)),
						BUNSPEC);
				}
				return BgL_new1400z00_6063;
			}
		}

	}



/* &lambda2183 */
	obj_t BGl_z62lambda2183z62zzsaw_node2rtlz00(obj_t BgL_envz00_5838,
		obj_t BgL_oz00_5839, obj_t BgL_vz00_5840)
	{
		{	/* SawMill/node2rtl.scm 23 */
			{
				BgL_retblockzf2tozf2_bglt BgL_auxz00_7903;

				{
					obj_t BgL_auxz00_7904;

					{	/* SawMill/node2rtl.scm 23 */
						BgL_objectz00_bglt BgL_tmpz00_7905;

						BgL_tmpz00_7905 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_5839));
						BgL_auxz00_7904 = BGL_OBJECT_WIDENING(BgL_tmpz00_7905);
					}
					BgL_auxz00_7903 = ((BgL_retblockzf2tozf2_bglt) BgL_auxz00_7904);
				}
				return
					((((BgL_retblockzf2tozf2_bglt) COBJECT(BgL_auxz00_7903))->
						BgL_dstz00) =
					((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_vz00_5840)),
					BUNSPEC);
			}
		}

	}



/* &lambda2182 */
	BgL_rtl_regz00_bglt BGl_z62lambda2182z62zzsaw_node2rtlz00(obj_t
		BgL_envz00_5841, obj_t BgL_oz00_5842)
	{
		{	/* SawMill/node2rtl.scm 23 */
			{
				BgL_retblockzf2tozf2_bglt BgL_auxz00_7912;

				{
					obj_t BgL_auxz00_7913;

					{	/* SawMill/node2rtl.scm 23 */
						BgL_objectz00_bglt BgL_tmpz00_7914;

						BgL_tmpz00_7914 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_5842));
						BgL_auxz00_7913 = BGL_OBJECT_WIDENING(BgL_tmpz00_7914);
					}
					BgL_auxz00_7912 = ((BgL_retblockzf2tozf2_bglt) BgL_auxz00_7913);
				}
				return
					(((BgL_retblockzf2tozf2_bglt) COBJECT(BgL_auxz00_7912))->BgL_dstz00);
			}
		}

	}



/* &lambda2178 */
	obj_t BGl_z62lambda2178z62zzsaw_node2rtlz00(obj_t BgL_envz00_5843,
		obj_t BgL_oz00_5844, obj_t BgL_vz00_5845)
	{
		{	/* SawMill/node2rtl.scm 23 */
			{
				BgL_retblockzf2tozf2_bglt BgL_auxz00_7920;

				{
					obj_t BgL_auxz00_7921;

					{	/* SawMill/node2rtl.scm 23 */
						BgL_objectz00_bglt BgL_tmpz00_7922;

						BgL_tmpz00_7922 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_5844));
						BgL_auxz00_7921 = BGL_OBJECT_WIDENING(BgL_tmpz00_7922);
					}
					BgL_auxz00_7920 = ((BgL_retblockzf2tozf2_bglt) BgL_auxz00_7921);
				}
				return
					((((BgL_retblockzf2tozf2_bglt) COBJECT(BgL_auxz00_7920))->BgL_toz00) =
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_vz00_5845)), BUNSPEC);
			}
		}

	}



/* &lambda2177 */
	BgL_blockz00_bglt BGl_z62lambda2177z62zzsaw_node2rtlz00(obj_t BgL_envz00_5846,
		obj_t BgL_oz00_5847)
	{
		{	/* SawMill/node2rtl.scm 23 */
			{
				BgL_retblockzf2tozf2_bglt BgL_auxz00_7929;

				{
					obj_t BgL_auxz00_7930;

					{	/* SawMill/node2rtl.scm 23 */
						BgL_objectz00_bglt BgL_tmpz00_7931;

						BgL_tmpz00_7931 =
							((BgL_objectz00_bglt) ((BgL_retblockz00_bglt) BgL_oz00_5847));
						BgL_auxz00_7930 = BGL_OBJECT_WIDENING(BgL_tmpz00_7931);
					}
					BgL_auxz00_7929 = ((BgL_retblockzf2tozf2_bglt) BgL_auxz00_7930);
				}
				return
					(((BgL_retblockzf2tozf2_bglt) COBJECT(BgL_auxz00_7929))->BgL_toz00);
			}
		}

	}



/* &lambda2143 */
	BgL_localz00_bglt BGl_z62lambda2143z62zzsaw_node2rtlz00(obj_t BgL_envz00_5848,
		obj_t BgL_o1188z00_5849)
	{
		{	/* SawMill/node2rtl.scm 22 */
			{	/* SawMill/node2rtl.scm 22 */
				long BgL_arg2144z00_6076;

				{	/* SawMill/node2rtl.scm 22 */
					obj_t BgL_arg2145z00_6077;

					{	/* SawMill/node2rtl.scm 22 */
						obj_t BgL_arg2146z00_6078;

						{	/* SawMill/node2rtl.scm 22 */
							obj_t BgL_arg1815z00_6079;
							long BgL_arg1816z00_6080;

							BgL_arg1815z00_6079 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/node2rtl.scm 22 */
								long BgL_arg1817z00_6081;

								BgL_arg1817z00_6081 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_localz00_bglt) BgL_o1188z00_5849)));
								BgL_arg1816z00_6080 = (BgL_arg1817z00_6081 - OBJECT_TYPE);
							}
							BgL_arg2146z00_6078 =
								VECTOR_REF(BgL_arg1815z00_6079, BgL_arg1816z00_6080);
						}
						BgL_arg2145z00_6077 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2146z00_6078);
					}
					{	/* SawMill/node2rtl.scm 22 */
						obj_t BgL_tmpz00_7944;

						BgL_tmpz00_7944 = ((obj_t) BgL_arg2145z00_6077);
						BgL_arg2144z00_6076 = BGL_CLASS_NUM(BgL_tmpz00_7944);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_localz00_bglt) BgL_o1188z00_5849)), BgL_arg2144z00_6076);
			}
			{	/* SawMill/node2rtl.scm 22 */
				BgL_objectz00_bglt BgL_tmpz00_7950;

				BgL_tmpz00_7950 =
					((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1188z00_5849));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7950, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_o1188z00_5849));
			return ((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1188z00_5849));
		}

	}



/* &<@anonymous:2142> */
	obj_t BGl_z62zc3z04anonymousza32142ze3ze5zzsaw_node2rtlz00(obj_t
		BgL_envz00_5850, obj_t BgL_new1187z00_5851)
	{
		{	/* SawMill/node2rtl.scm 22 */
			{
				BgL_localz00_bglt BgL_auxz00_7958;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1187z00_5851))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(15)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1187z00_5851))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_7966;

					{	/* SawMill/node2rtl.scm 22 */
						obj_t BgL_classz00_6083;

						BgL_classz00_6083 = BGl_typez00zztype_typez00;
						{	/* SawMill/node2rtl.scm 22 */
							obj_t BgL__ortest_1117z00_6084;

							BgL__ortest_1117z00_6084 = BGL_CLASS_NIL(BgL_classz00_6083);
							if (CBOOL(BgL__ortest_1117z00_6084))
								{	/* SawMill/node2rtl.scm 22 */
									BgL_auxz00_7966 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_6084);
								}
							else
								{	/* SawMill/node2rtl.scm 22 */
									BgL_auxz00_7966 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6083));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1187z00_5851))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_7966), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_7976;

					{	/* SawMill/node2rtl.scm 22 */
						obj_t BgL_classz00_6085;

						BgL_classz00_6085 = BGl_valuez00zzast_varz00;
						{	/* SawMill/node2rtl.scm 22 */
							obj_t BgL__ortest_1117z00_6086;

							BgL__ortest_1117z00_6086 = BGL_CLASS_NIL(BgL_classz00_6085);
							if (CBOOL(BgL__ortest_1117z00_6086))
								{	/* SawMill/node2rtl.scm 22 */
									BgL_auxz00_7976 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_6086);
								}
							else
								{	/* SawMill/node2rtl.scm 22 */
									BgL_auxz00_7976 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6085));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_new1187z00_5851))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_7976), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_new1187z00_5851))))->BgL_accessz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1187z00_5851))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1187z00_5851))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1187z00_5851))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1187z00_5851))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt)
										BgL_new1187z00_5851))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1187z00_5851))))->BgL_keyz00) =
					((long) 0L), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1187z00_5851))))->BgL_valzd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt) ((BgL_localz00_bglt)
										BgL_new1187z00_5851))))->BgL_volatilez00) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_rlocalz00_bglt BgL_auxz00_8013;

					{
						obj_t BgL_auxz00_8014;

						{	/* SawMill/node2rtl.scm 22 */
							BgL_objectz00_bglt BgL_tmpz00_8015;

							BgL_tmpz00_8015 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_new1187z00_5851));
							BgL_auxz00_8014 = BGL_OBJECT_WIDENING(BgL_tmpz00_8015);
						}
						BgL_auxz00_8013 = ((BgL_rlocalz00_bglt) BgL_auxz00_8014);
					}
					((((BgL_rlocalz00_bglt) COBJECT(BgL_auxz00_8013))->BgL_regz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_rlocalz00_bglt BgL_auxz00_8021;

					{
						obj_t BgL_auxz00_8022;

						{	/* SawMill/node2rtl.scm 22 */
							BgL_objectz00_bglt BgL_tmpz00_8023;

							BgL_tmpz00_8023 =
								((BgL_objectz00_bglt)
								((BgL_localz00_bglt) BgL_new1187z00_5851));
							BgL_auxz00_8022 = BGL_OBJECT_WIDENING(BgL_tmpz00_8023);
						}
						BgL_auxz00_8021 = ((BgL_rlocalz00_bglt) BgL_auxz00_8022);
					}
					((((BgL_rlocalz00_bglt) COBJECT(BgL_auxz00_8021))->BgL_codez00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_7958 = ((BgL_localz00_bglt) BgL_new1187z00_5851);
				return ((obj_t) BgL_auxz00_7958);
			}
		}

	}



/* &lambda2140 */
	BgL_localz00_bglt BGl_z62lambda2140z62zzsaw_node2rtlz00(obj_t BgL_envz00_5852,
		obj_t BgL_o1184z00_5853)
	{
		{	/* SawMill/node2rtl.scm 22 */
			{	/* SawMill/node2rtl.scm 22 */
				BgL_rlocalz00_bglt BgL_wide1186z00_6088;

				BgL_wide1186z00_6088 =
					((BgL_rlocalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rlocalz00_bgl))));
				{	/* SawMill/node2rtl.scm 22 */
					obj_t BgL_auxz00_8036;
					BgL_objectz00_bglt BgL_tmpz00_8032;

					BgL_auxz00_8036 = ((obj_t) BgL_wide1186z00_6088);
					BgL_tmpz00_8032 =
						((BgL_objectz00_bglt)
						((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1184z00_5853)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8032, BgL_auxz00_8036);
				}
				((BgL_objectz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1184z00_5853)));
				{	/* SawMill/node2rtl.scm 22 */
					long BgL_arg2141z00_6089;

					BgL_arg2141z00_6089 = BGL_CLASS_NUM(BGl_rlocalz00zzsaw_node2rtlz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_localz00_bglt)
								((BgL_localz00_bglt) BgL_o1184z00_5853))), BgL_arg2141z00_6089);
				}
				return
					((BgL_localz00_bglt)
					((BgL_localz00_bglt) ((BgL_localz00_bglt) BgL_o1184z00_5853)));
			}
		}

	}



/* &lambda2137 */
	BgL_localz00_bglt BGl_z62lambda2137z62zzsaw_node2rtlz00(obj_t BgL_envz00_5854,
		obj_t BgL_id1169z00_5855, obj_t BgL_name1170z00_5856,
		obj_t BgL_type1171z00_5857, obj_t BgL_value1172z00_5858,
		obj_t BgL_access1173z00_5859, obj_t BgL_fastzd2alpha1174zd2_5860,
		obj_t BgL_removable1175z00_5861, obj_t BgL_occurrence1176z00_5862,
		obj_t BgL_occurrencew1177z00_5863, obj_t BgL_userzf31178zf3_5864,
		obj_t BgL_key1179z00_5865, obj_t BgL_valzd2noescape1180zd2_5866,
		obj_t BgL_volatile1181z00_5867, obj_t BgL_reg1182z00_5868,
		obj_t BgL_code1183z00_5869)
	{
		{	/* SawMill/node2rtl.scm 22 */
			{	/* SawMill/node2rtl.scm 22 */
				long BgL_occurrence1176z00_6093;
				long BgL_occurrencew1177z00_6094;
				bool_t BgL_userzf31178zf3_6095;
				long BgL_key1179z00_6096;
				bool_t BgL_volatile1181z00_6097;

				BgL_occurrence1176z00_6093 = (long) CINT(BgL_occurrence1176z00_5862);
				BgL_occurrencew1177z00_6094 = (long) CINT(BgL_occurrencew1177z00_5863);
				BgL_userzf31178zf3_6095 = CBOOL(BgL_userzf31178zf3_5864);
				BgL_key1179z00_6096 = (long) CINT(BgL_key1179z00_5865);
				BgL_volatile1181z00_6097 = CBOOL(BgL_volatile1181z00_5867);
				{	/* SawMill/node2rtl.scm 22 */
					BgL_localz00_bglt BgL_new1395z00_6098;

					{	/* SawMill/node2rtl.scm 22 */
						BgL_localz00_bglt BgL_tmp1393z00_6099;
						BgL_rlocalz00_bglt BgL_wide1394z00_6100;

						{
							BgL_localz00_bglt BgL_auxz00_8055;

							{	/* SawMill/node2rtl.scm 22 */
								BgL_localz00_bglt BgL_new1392z00_6101;

								BgL_new1392z00_6101 =
									((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_localz00_bgl))));
								{	/* SawMill/node2rtl.scm 22 */
									long BgL_arg2139z00_6102;

									BgL_arg2139z00_6102 = BGL_CLASS_NUM(BGl_localz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1392z00_6101),
										BgL_arg2139z00_6102);
								}
								{	/* SawMill/node2rtl.scm 22 */
									BgL_objectz00_bglt BgL_tmpz00_8060;

									BgL_tmpz00_8060 = ((BgL_objectz00_bglt) BgL_new1392z00_6101);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8060, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1392z00_6101);
								BgL_auxz00_8055 = BgL_new1392z00_6101;
							}
							BgL_tmp1393z00_6099 = ((BgL_localz00_bglt) BgL_auxz00_8055);
						}
						BgL_wide1394z00_6100 =
							((BgL_rlocalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rlocalz00_bgl))));
						{	/* SawMill/node2rtl.scm 22 */
							obj_t BgL_auxz00_8068;
							BgL_objectz00_bglt BgL_tmpz00_8066;

							BgL_auxz00_8068 = ((obj_t) BgL_wide1394z00_6100);
							BgL_tmpz00_8066 = ((BgL_objectz00_bglt) BgL_tmp1393z00_6099);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8066, BgL_auxz00_8068);
						}
						((BgL_objectz00_bglt) BgL_tmp1393z00_6099);
						{	/* SawMill/node2rtl.scm 22 */
							long BgL_arg2138z00_6103;

							BgL_arg2138z00_6103 =
								BGL_CLASS_NUM(BGl_rlocalz00zzsaw_node2rtlz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1393z00_6099),
								BgL_arg2138z00_6103);
						}
						BgL_new1395z00_6098 = ((BgL_localz00_bglt) BgL_tmp1393z00_6099);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1395z00_6098)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1169z00_5855)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1395z00_6098)))->BgL_namez00) =
						((obj_t) BgL_name1170z00_5856), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1395z00_6098)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1171z00_5857)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1395z00_6098)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1172z00_5858)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1395z00_6098)))->BgL_accessz00) =
						((obj_t) BgL_access1173z00_5859), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1395z00_6098)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1174zd2_5860), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1395z00_6098)))->BgL_removablez00) =
						((obj_t) BgL_removable1175z00_5861), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1395z00_6098)))->BgL_occurrencez00) =
						((long) BgL_occurrence1176z00_6093), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1395z00_6098)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1177z00_6094), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1395z00_6098)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31178zf3_6095), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1395z00_6098)))->BgL_keyz00) =
						((long) BgL_key1179z00_6096), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1395z00_6098)))->BgL_valzd2noescapezd2) =
						((obj_t) BgL_valzd2noescape1180zd2_5866), BUNSPEC);
					((((BgL_localz00_bglt) COBJECT(((BgL_localz00_bglt)
										BgL_new1395z00_6098)))->BgL_volatilez00) =
						((bool_t) BgL_volatile1181z00_6097), BUNSPEC);
					{
						BgL_rlocalz00_bglt BgL_auxz00_8105;

						{
							obj_t BgL_auxz00_8106;

							{	/* SawMill/node2rtl.scm 22 */
								BgL_objectz00_bglt BgL_tmpz00_8107;

								BgL_tmpz00_8107 = ((BgL_objectz00_bglt) BgL_new1395z00_6098);
								BgL_auxz00_8106 = BGL_OBJECT_WIDENING(BgL_tmpz00_8107);
							}
							BgL_auxz00_8105 = ((BgL_rlocalz00_bglt) BgL_auxz00_8106);
						}
						((((BgL_rlocalz00_bglt) COBJECT(BgL_auxz00_8105))->BgL_regz00) =
							((obj_t) BgL_reg1182z00_5868), BUNSPEC);
					}
					{
						BgL_rlocalz00_bglt BgL_auxz00_8112;

						{
							obj_t BgL_auxz00_8113;

							{	/* SawMill/node2rtl.scm 22 */
								BgL_objectz00_bglt BgL_tmpz00_8114;

								BgL_tmpz00_8114 = ((BgL_objectz00_bglt) BgL_new1395z00_6098);
								BgL_auxz00_8113 = BGL_OBJECT_WIDENING(BgL_tmpz00_8114);
							}
							BgL_auxz00_8112 = ((BgL_rlocalz00_bglt) BgL_auxz00_8113);
						}
						((((BgL_rlocalz00_bglt) COBJECT(BgL_auxz00_8112))->BgL_codez00) =
							((obj_t) BgL_code1183z00_5869), BUNSPEC);
					}
					return BgL_new1395z00_6098;
				}
			}
		}

	}



/* &lambda2157 */
	obj_t BGl_z62lambda2157z62zzsaw_node2rtlz00(obj_t BgL_envz00_5870,
		obj_t BgL_oz00_5871, obj_t BgL_vz00_5872)
	{
		{	/* SawMill/node2rtl.scm 22 */
			{
				BgL_rlocalz00_bglt BgL_auxz00_8119;

				{
					obj_t BgL_auxz00_8120;

					{	/* SawMill/node2rtl.scm 22 */
						BgL_objectz00_bglt BgL_tmpz00_8121;

						BgL_tmpz00_8121 =
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_5871));
						BgL_auxz00_8120 = BGL_OBJECT_WIDENING(BgL_tmpz00_8121);
					}
					BgL_auxz00_8119 = ((BgL_rlocalz00_bglt) BgL_auxz00_8120);
				}
				return
					((((BgL_rlocalz00_bglt) COBJECT(BgL_auxz00_8119))->BgL_codez00) =
					((obj_t) BgL_vz00_5872), BUNSPEC);
			}
		}

	}



/* &lambda2156 */
	obj_t BGl_z62lambda2156z62zzsaw_node2rtlz00(obj_t BgL_envz00_5873,
		obj_t BgL_oz00_5874)
	{
		{	/* SawMill/node2rtl.scm 22 */
			{
				BgL_rlocalz00_bglt BgL_auxz00_8127;

				{
					obj_t BgL_auxz00_8128;

					{	/* SawMill/node2rtl.scm 22 */
						BgL_objectz00_bglt BgL_tmpz00_8129;

						BgL_tmpz00_8129 =
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_5874));
						BgL_auxz00_8128 = BGL_OBJECT_WIDENING(BgL_tmpz00_8129);
					}
					BgL_auxz00_8127 = ((BgL_rlocalz00_bglt) BgL_auxz00_8128);
				}
				return (((BgL_rlocalz00_bglt) COBJECT(BgL_auxz00_8127))->BgL_codez00);
			}
		}

	}



/* &lambda2151 */
	obj_t BGl_z62lambda2151z62zzsaw_node2rtlz00(obj_t BgL_envz00_5875,
		obj_t BgL_oz00_5876, obj_t BgL_vz00_5877)
	{
		{	/* SawMill/node2rtl.scm 22 */
			{
				BgL_rlocalz00_bglt BgL_auxz00_8135;

				{
					obj_t BgL_auxz00_8136;

					{	/* SawMill/node2rtl.scm 22 */
						BgL_objectz00_bglt BgL_tmpz00_8137;

						BgL_tmpz00_8137 =
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_5876));
						BgL_auxz00_8136 = BGL_OBJECT_WIDENING(BgL_tmpz00_8137);
					}
					BgL_auxz00_8135 = ((BgL_rlocalz00_bglt) BgL_auxz00_8136);
				}
				return
					((((BgL_rlocalz00_bglt) COBJECT(BgL_auxz00_8135))->BgL_regz00) =
					((obj_t) BgL_vz00_5877), BUNSPEC);
			}
		}

	}



/* &lambda2150 */
	obj_t BGl_z62lambda2150z62zzsaw_node2rtlz00(obj_t BgL_envz00_5878,
		obj_t BgL_oz00_5879)
	{
		{	/* SawMill/node2rtl.scm 22 */
			{
				BgL_rlocalz00_bglt BgL_auxz00_8143;

				{
					obj_t BgL_auxz00_8144;

					{	/* SawMill/node2rtl.scm 22 */
						BgL_objectz00_bglt BgL_tmpz00_8145;

						BgL_tmpz00_8145 =
							((BgL_objectz00_bglt) ((BgL_localz00_bglt) BgL_oz00_5879));
						BgL_auxz00_8144 = BGL_OBJECT_WIDENING(BgL_tmpz00_8145);
					}
					BgL_auxz00_8143 = ((BgL_rlocalz00_bglt) BgL_auxz00_8144);
				}
				return (((BgL_rlocalz00_bglt) COBJECT(BgL_auxz00_8143))->BgL_regz00);
			}
		}

	}



/* &lambda2126 */
	BgL_blockz00_bglt BGl_z62lambda2126z62zzsaw_node2rtlz00(obj_t BgL_envz00_5880,
		obj_t BgL_o1167z00_5881)
	{
		{	/* SawMill/node2rtl.scm 21 */
			{	/* SawMill/node2rtl.scm 21 */
				long BgL_arg2127z00_6109;

				{	/* SawMill/node2rtl.scm 21 */
					obj_t BgL_arg2129z00_6110;

					{	/* SawMill/node2rtl.scm 21 */
						obj_t BgL_arg2130z00_6111;

						{	/* SawMill/node2rtl.scm 21 */
							obj_t BgL_arg1815z00_6112;
							long BgL_arg1816z00_6113;

							BgL_arg1815z00_6112 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/node2rtl.scm 21 */
								long BgL_arg1817z00_6114;

								BgL_arg1817z00_6114 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_o1167z00_5881)));
								BgL_arg1816z00_6113 = (BgL_arg1817z00_6114 - OBJECT_TYPE);
							}
							BgL_arg2130z00_6111 =
								VECTOR_REF(BgL_arg1815z00_6112, BgL_arg1816z00_6113);
						}
						BgL_arg2129z00_6110 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2130z00_6111);
					}
					{	/* SawMill/node2rtl.scm 21 */
						obj_t BgL_tmpz00_8158;

						BgL_tmpz00_8158 = ((obj_t) BgL_arg2129z00_6110);
						BgL_arg2127z00_6109 = BGL_CLASS_NUM(BgL_tmpz00_8158);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) BgL_o1167z00_5881)), BgL_arg2127z00_6109);
			}
			{	/* SawMill/node2rtl.scm 21 */
				BgL_objectz00_bglt BgL_tmpz00_8164;

				BgL_tmpz00_8164 =
					((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1167z00_5881));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8164, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1167z00_5881));
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1167z00_5881));
		}

	}



/* &<@anonymous:2125> */
	obj_t BGl_z62zc3z04anonymousza32125ze3ze5zzsaw_node2rtlz00(obj_t
		BgL_envz00_5882, obj_t BgL_new1166z00_5883)
	{
		{	/* SawMill/node2rtl.scm 21 */
			{
				BgL_blockz00_bglt BgL_auxz00_8172;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_new1166z00_5883))))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1166z00_5883))))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1166z00_5883))))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1166z00_5883))))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_8172 = ((BgL_blockz00_bglt) BgL_new1166z00_5883);
				return ((obj_t) BgL_auxz00_8172);
			}
		}

	}



/* &lambda2123 */
	BgL_blockz00_bglt BGl_z62lambda2123z62zzsaw_node2rtlz00(obj_t BgL_envz00_5884,
		obj_t BgL_o1163z00_5885)
	{
		{	/* SawMill/node2rtl.scm 21 */
			{	/* SawMill/node2rtl.scm 21 */
				BgL_reversedz00_bglt BgL_wide1165z00_6117;

				BgL_wide1165z00_6117 =
					((BgL_reversedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_reversedz00_bgl))));
				{	/* SawMill/node2rtl.scm 21 */
					obj_t BgL_auxz00_8193;
					BgL_objectz00_bglt BgL_tmpz00_8189;

					BgL_auxz00_8193 = ((obj_t) BgL_wide1165z00_6117);
					BgL_tmpz00_8189 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1163z00_5885)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8189, BgL_auxz00_8193);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1163z00_5885)));
				{	/* SawMill/node2rtl.scm 21 */
					long BgL_arg2124z00_6118;

					BgL_arg2124z00_6118 = BGL_CLASS_NUM(BGl_reversedz00zzsaw_node2rtlz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_o1163z00_5885))), BgL_arg2124z00_6118);
				}
				return
					((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1163z00_5885)));
			}
		}

	}



/* &lambda2120 */
	BgL_blockz00_bglt BGl_z62lambda2120z62zzsaw_node2rtlz00(obj_t BgL_envz00_5886,
		obj_t BgL_label1159z00_5887, obj_t BgL_preds1160z00_5888,
		obj_t BgL_succs1161z00_5889, obj_t BgL_first1162z00_5890)
	{
		{	/* SawMill/node2rtl.scm 21 */
			{	/* SawMill/node2rtl.scm 21 */
				int BgL_label1159z00_6119;

				BgL_label1159z00_6119 = CINT(BgL_label1159z00_5887);
				{	/* SawMill/node2rtl.scm 21 */
					BgL_blockz00_bglt BgL_new1390z00_6123;

					{	/* SawMill/node2rtl.scm 21 */
						BgL_blockz00_bglt BgL_tmp1388z00_6124;
						BgL_reversedz00_bglt BgL_wide1389z00_6125;

						{
							BgL_blockz00_bglt BgL_auxz00_8208;

							{	/* SawMill/node2rtl.scm 21 */
								BgL_blockz00_bglt BgL_new1387z00_6126;

								BgL_new1387z00_6126 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawMill/node2rtl.scm 21 */
									long BgL_arg2122z00_6127;

									BgL_arg2122z00_6127 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1387z00_6126),
										BgL_arg2122z00_6127);
								}
								{	/* SawMill/node2rtl.scm 21 */
									BgL_objectz00_bglt BgL_tmpz00_8213;

									BgL_tmpz00_8213 = ((BgL_objectz00_bglt) BgL_new1387z00_6126);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8213, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1387z00_6126);
								BgL_auxz00_8208 = BgL_new1387z00_6126;
							}
							BgL_tmp1388z00_6124 = ((BgL_blockz00_bglt) BgL_auxz00_8208);
						}
						BgL_wide1389z00_6125 =
							((BgL_reversedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_reversedz00_bgl))));
						{	/* SawMill/node2rtl.scm 21 */
							obj_t BgL_auxz00_8221;
							BgL_objectz00_bglt BgL_tmpz00_8219;

							BgL_auxz00_8221 = ((obj_t) BgL_wide1389z00_6125);
							BgL_tmpz00_8219 = ((BgL_objectz00_bglt) BgL_tmp1388z00_6124);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8219, BgL_auxz00_8221);
						}
						((BgL_objectz00_bglt) BgL_tmp1388z00_6124);
						{	/* SawMill/node2rtl.scm 21 */
							long BgL_arg2121z00_6128;

							BgL_arg2121z00_6128 =
								BGL_CLASS_NUM(BGl_reversedz00zzsaw_node2rtlz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1388z00_6124),
								BgL_arg2121z00_6128);
						}
						BgL_new1390z00_6123 = ((BgL_blockz00_bglt) BgL_tmp1388z00_6124);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_new1390z00_6123)))->BgL_labelz00) =
						((int) BgL_label1159z00_6119), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1390z00_6123)))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1160z00_5888)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1390z00_6123)))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1161z00_5889)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1390z00_6123)))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1162z00_5890)), BUNSPEC);
					return BgL_new1390z00_6123;
				}
			}
		}

	}



/* &<@anonymous:2103> */
	obj_t BGl_z62zc3z04anonymousza32103ze3ze5zzsaw_node2rtlz00(obj_t
		BgL_envz00_5891, obj_t BgL_new1157z00_5892)
	{
		{	/* SawMill/node2rtl.scm 20 */
			{
				BgL_areaz00_bglt BgL_auxz00_8240;

				{
					BgL_blockz00_bglt BgL_auxz00_8241;

					{	/* SawMill/node2rtl.scm 20 */
						obj_t BgL_classz00_6130;

						BgL_classz00_6130 = BGl_blockz00zzsaw_defsz00;
						{	/* SawMill/node2rtl.scm 20 */
							obj_t BgL__ortest_1117z00_6131;

							BgL__ortest_1117z00_6131 = BGL_CLASS_NIL(BgL_classz00_6130);
							if (CBOOL(BgL__ortest_1117z00_6131))
								{	/* SawMill/node2rtl.scm 20 */
									BgL_auxz00_8241 =
										((BgL_blockz00_bglt) BgL__ortest_1117z00_6131);
								}
							else
								{	/* SawMill/node2rtl.scm 20 */
									BgL_auxz00_8241 =
										((BgL_blockz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6130));
								}
						}
					}
					((((BgL_areaz00_bglt) COBJECT(
									((BgL_areaz00_bglt) BgL_new1157z00_5892)))->BgL_entryz00) =
						((BgL_blockz00_bglt) BgL_auxz00_8241), BUNSPEC);
				}
				{
					BgL_blockz00_bglt BgL_auxz00_8250;

					{	/* SawMill/node2rtl.scm 20 */
						obj_t BgL_classz00_6132;

						BgL_classz00_6132 = BGl_blockz00zzsaw_defsz00;
						{	/* SawMill/node2rtl.scm 20 */
							obj_t BgL__ortest_1117z00_6133;

							BgL__ortest_1117z00_6133 = BGL_CLASS_NIL(BgL_classz00_6132);
							if (CBOOL(BgL__ortest_1117z00_6133))
								{	/* SawMill/node2rtl.scm 20 */
									BgL_auxz00_8250 =
										((BgL_blockz00_bglt) BgL__ortest_1117z00_6133);
								}
							else
								{	/* SawMill/node2rtl.scm 20 */
									BgL_auxz00_8250 =
										((BgL_blockz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6132));
								}
						}
					}
					((((BgL_areaz00_bglt) COBJECT(
									((BgL_areaz00_bglt) BgL_new1157z00_5892)))->BgL_exitz00) =
						((BgL_blockz00_bglt) BgL_auxz00_8250), BUNSPEC);
				}
				BgL_auxz00_8240 = ((BgL_areaz00_bglt) BgL_new1157z00_5892);
				return ((obj_t) BgL_auxz00_8240);
			}
		}

	}



/* &lambda2101 */
	BgL_areaz00_bglt BGl_z62lambda2101z62zzsaw_node2rtlz00(obj_t BgL_envz00_5893)
	{
		{	/* SawMill/node2rtl.scm 20 */
			{	/* SawMill/node2rtl.scm 20 */
				BgL_areaz00_bglt BgL_new1156z00_6134;

				BgL_new1156z00_6134 =
					((BgL_areaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_areaz00_bgl))));
				{	/* SawMill/node2rtl.scm 20 */
					long BgL_arg2102z00_6135;

					BgL_arg2102z00_6135 = BGL_CLASS_NUM(BGl_areaz00zzsaw_node2rtlz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1156z00_6134), BgL_arg2102z00_6135);
				}
				return BgL_new1156z00_6134;
			}
		}

	}



/* &lambda2099 */
	BgL_areaz00_bglt BGl_z62lambda2099z62zzsaw_node2rtlz00(obj_t BgL_envz00_5894,
		obj_t BgL_entry1154z00_5895, obj_t BgL_exit1155z00_5896)
	{
		{	/* SawMill/node2rtl.scm 20 */
			{	/* SawMill/node2rtl.scm 20 */
				BgL_areaz00_bglt BgL_new1386z00_6138;

				{	/* SawMill/node2rtl.scm 20 */
					BgL_areaz00_bglt BgL_new1385z00_6139;

					BgL_new1385z00_6139 =
						((BgL_areaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_areaz00_bgl))));
					{	/* SawMill/node2rtl.scm 20 */
						long BgL_arg2100z00_6140;

						BgL_arg2100z00_6140 = BGL_CLASS_NUM(BGl_areaz00zzsaw_node2rtlz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1385z00_6139), BgL_arg2100z00_6140);
					}
					BgL_new1386z00_6138 = BgL_new1385z00_6139;
				}
				((((BgL_areaz00_bglt) COBJECT(BgL_new1386z00_6138))->BgL_entryz00) =
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_entry1154z00_5895)),
					BUNSPEC);
				((((BgL_areaz00_bglt) COBJECT(BgL_new1386z00_6138))->BgL_exitz00) =
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_exit1155z00_5896)),
					BUNSPEC);
				return BgL_new1386z00_6138;
			}
		}

	}



/* &lambda2113 */
	obj_t BGl_z62lambda2113z62zzsaw_node2rtlz00(obj_t BgL_envz00_5897,
		obj_t BgL_oz00_5898, obj_t BgL_vz00_5899)
	{
		{	/* SawMill/node2rtl.scm 20 */
			return
				((((BgL_areaz00_bglt) COBJECT(
							((BgL_areaz00_bglt) BgL_oz00_5898)))->BgL_exitz00) =
				((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_vz00_5899)), BUNSPEC);
		}

	}



/* &lambda2112 */
	BgL_blockz00_bglt BGl_z62lambda2112z62zzsaw_node2rtlz00(obj_t BgL_envz00_5900,
		obj_t BgL_oz00_5901)
	{
		{	/* SawMill/node2rtl.scm 20 */
			return
				(((BgL_areaz00_bglt) COBJECT(
						((BgL_areaz00_bglt) BgL_oz00_5901)))->BgL_exitz00);
		}

	}



/* &lambda2108 */
	obj_t BGl_z62lambda2108z62zzsaw_node2rtlz00(obj_t BgL_envz00_5902,
		obj_t BgL_oz00_5903, obj_t BgL_vz00_5904)
	{
		{	/* SawMill/node2rtl.scm 20 */
			return
				((((BgL_areaz00_bglt) COBJECT(
							((BgL_areaz00_bglt) BgL_oz00_5903)))->BgL_entryz00) =
				((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_vz00_5904)), BUNSPEC);
		}

	}



/* &lambda2107 */
	BgL_blockz00_bglt BGl_z62lambda2107z62zzsaw_node2rtlz00(obj_t BgL_envz00_5905,
		obj_t BgL_oz00_5906)
	{
		{	/* SawMill/node2rtl.scm 20 */
			return
				(((BgL_areaz00_bglt) COBJECT(
						((BgL_areaz00_bglt) BgL_oz00_5906)))->BgL_entryz00);
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_node2rtlz00(void)
	{
		{	/* SawMill/node2rtl.scm 1 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_proc2896z00zzsaw_node2rtlz00, BGl_nodez00zzast_nodez00,
				BGl_string2897z00zzsaw_node2rtlz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_predicatezd2envzd2zzsaw_node2rtlz00,
				BGl_proc2898z00zzsaw_node2rtlz00, BGl_nodez00zzast_nodez00,
				BGl_string2899z00zzsaw_node2rtlz00);
		}

	}



/* &predicate1780 */
	obj_t BGl_z62predicate1780z62zzsaw_node2rtlz00(obj_t BgL_envz00_5909,
		obj_t BgL_ez00_5910, obj_t BgL_joinedz00_5911)
	{
		{	/* SawMill/node2rtl.scm 513 */
			{
				BgL_areaz00_bglt BgL_auxz00_8285;

				{	/* SawMill/node2rtl.scm 514 */
					BgL_areaz00_bglt BgL_arg2186z00_6148;

					{	/* SawMill/node2rtl.scm 514 */
						BgL_rtl_ifz00_bglt BgL_arg2187z00_6149;

						{	/* SawMill/node2rtl.scm 514 */
							BgL_rtl_ifz00_bglt BgL_new1379z00_6150;

							{	/* SawMill/node2rtl.scm 514 */
								BgL_rtl_ifz00_bglt BgL_new1378z00_6151;

								BgL_new1378z00_6151 =
									((BgL_rtl_ifz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_rtl_ifz00_bgl))));
								{	/* SawMill/node2rtl.scm 514 */
									long BgL_arg2189z00_6152;

									BgL_arg2189z00_6152 =
										BGL_CLASS_NUM(BGl_rtl_ifz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1378z00_6151),
										BgL_arg2189z00_6152);
								}
								BgL_new1379z00_6150 = BgL_new1378z00_6151;
							}
							((((BgL_rtl_funz00_bglt) COBJECT(
											((BgL_rtl_funz00_bglt) BgL_new1379z00_6150)))->
									BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
							BgL_arg2187z00_6149 = BgL_new1379z00_6150;
						}
						{	/* SawMill/node2rtl.scm 514 */
							obj_t BgL_list2188z00_6153;

							BgL_list2188z00_6153 =
								MAKE_YOUNG_PAIR(
								((obj_t) ((BgL_nodez00_bglt) BgL_ez00_5910)), BNIL);
							BgL_arg2186z00_6148 =
								BGl_callza2za2zzsaw_node2rtlz00(
								((obj_t)
									((BgL_nodez00_bglt) BgL_ez00_5910)),
								((BgL_rtl_funz00_bglt) BgL_arg2187z00_6149),
								BgL_list2188z00_6153);
					}}
					BgL_auxz00_8285 =
						BGl_forkz00zzsaw_node2rtlz00(BgL_arg2186z00_6148,
						BgL_joinedz00_5911);
				}
				return ((obj_t) BgL_auxz00_8285);
			}
		}

	}



/* &node->rtl1716 */
	obj_t BGl_z62nodezd2ze3rtl1716z53zzsaw_node2rtlz00(obj_t BgL_envz00_5912,
		obj_t BgL_ez00_5913)
	{
		{	/* SawMill/node2rtl.scm 207 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(16),
				BGl_string2900z00zzsaw_node2rtlz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_ez00_5913)));
		}

	}



/* node->rtl */
	BgL_areaz00_bglt BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(BgL_nodez00_bglt
		BgL_ez00_137)
	{
		{	/* SawMill/node2rtl.scm 207 */
			{	/* SawMill/node2rtl.scm 207 */
				obj_t BgL_method1717z00_3035;

				{	/* SawMill/node2rtl.scm 207 */
					obj_t BgL_res2836z00_4831;

					{	/* SawMill/node2rtl.scm 207 */
						long BgL_objzd2classzd2numz00_4802;

						BgL_objzd2classzd2numz00_4802 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_ez00_137));
						{	/* SawMill/node2rtl.scm 207 */
							obj_t BgL_arg1811z00_4803;

							BgL_arg1811z00_4803 =
								PROCEDURE_REF(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
								(int) (1L));
							{	/* SawMill/node2rtl.scm 207 */
								int BgL_offsetz00_4806;

								BgL_offsetz00_4806 = (int) (BgL_objzd2classzd2numz00_4802);
								{	/* SawMill/node2rtl.scm 207 */
									long BgL_offsetz00_4807;

									BgL_offsetz00_4807 =
										((long) (BgL_offsetz00_4806) - OBJECT_TYPE);
									{	/* SawMill/node2rtl.scm 207 */
										long BgL_modz00_4808;

										BgL_modz00_4808 =
											(BgL_offsetz00_4807 >> (int) ((long) ((int) (4L))));
										{	/* SawMill/node2rtl.scm 207 */
											long BgL_restz00_4810;

											BgL_restz00_4810 =
												(BgL_offsetz00_4807 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawMill/node2rtl.scm 207 */

												{	/* SawMill/node2rtl.scm 207 */
													obj_t BgL_bucketz00_4812;

													BgL_bucketz00_4812 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4803), BgL_modz00_4808);
													BgL_res2836z00_4831 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4812), BgL_restz00_4810);
					}}}}}}}}
					BgL_method1717z00_3035 = BgL_res2836z00_4831;
				}
				return
					((BgL_areaz00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1717z00_3035, ((obj_t) BgL_ez00_137)));
			}
		}

	}



/* &node->rtl */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlz53zzsaw_node2rtlz00(obj_t
		BgL_envz00_5914, obj_t BgL_ez00_5915)
	{
		{	/* SawMill/node2rtl.scm 207 */
			return
				BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(
				((BgL_nodez00_bglt) BgL_ez00_5915));
		}

	}



/* predicate */
	BgL_areaz00_bglt BGl_predicatez00zzsaw_node2rtlz00(BgL_nodez00_bglt
		BgL_ez00_176, obj_t BgL_joinedz00_177)
	{
		{	/* SawMill/node2rtl.scm 513 */
			{	/* SawMill/node2rtl.scm 513 */
				obj_t BgL_method1781z00_3036;

				{	/* SawMill/node2rtl.scm 513 */
					obj_t BgL_res2841z00_4862;

					{	/* SawMill/node2rtl.scm 513 */
						long BgL_objzd2classzd2numz00_4833;

						BgL_objzd2classzd2numz00_4833 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_ez00_176));
						{	/* SawMill/node2rtl.scm 513 */
							obj_t BgL_arg1811z00_4834;

							BgL_arg1811z00_4834 =
								PROCEDURE_REF(BGl_predicatezd2envzd2zzsaw_node2rtlz00,
								(int) (1L));
							{	/* SawMill/node2rtl.scm 513 */
								int BgL_offsetz00_4837;

								BgL_offsetz00_4837 = (int) (BgL_objzd2classzd2numz00_4833);
								{	/* SawMill/node2rtl.scm 513 */
									long BgL_offsetz00_4838;

									BgL_offsetz00_4838 =
										((long) (BgL_offsetz00_4837) - OBJECT_TYPE);
									{	/* SawMill/node2rtl.scm 513 */
										long BgL_modz00_4839;

										BgL_modz00_4839 =
											(BgL_offsetz00_4838 >> (int) ((long) ((int) (4L))));
										{	/* SawMill/node2rtl.scm 513 */
											long BgL_restz00_4841;

											BgL_restz00_4841 =
												(BgL_offsetz00_4838 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawMill/node2rtl.scm 513 */

												{	/* SawMill/node2rtl.scm 513 */
													obj_t BgL_bucketz00_4843;

													BgL_bucketz00_4843 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4834), BgL_modz00_4839);
													BgL_res2841z00_4862 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4843), BgL_restz00_4841);
					}}}}}}}}
					BgL_method1781z00_3036 = BgL_res2841z00_4862;
				}
				return
					((BgL_areaz00_bglt)
					BGL_PROCEDURE_CALL2(BgL_method1781z00_3036,
						((obj_t) BgL_ez00_176), BgL_joinedz00_177));
			}
		}

	}



/* &predicate */
	BgL_areaz00_bglt BGl_z62predicatez62zzsaw_node2rtlz00(obj_t BgL_envz00_5916,
		obj_t BgL_ez00_5917, obj_t BgL_joinedz00_5918)
	{
		{	/* SawMill/node2rtl.scm 513 */
			return
				BGl_predicatez00zzsaw_node2rtlz00(
				((BgL_nodez00_bglt) BgL_ez00_5917), BgL_joinedz00_5918);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_node2rtlz00(void)
	{
		{	/* SawMill/node2rtl.scm 1 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00, BGl_atomz00zzast_nodez00,
				BGl_proc2901z00zzsaw_node2rtlz00, BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00, BGl_varz00zzast_nodez00,
				BGl_proc2903z00zzsaw_node2rtlz00, BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2904z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00, BGl_setqz00zzast_nodez00,
				BGl_proc2905z00zzsaw_node2rtlz00, BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_sequencez00zzast_nodez00, BGl_proc2906z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00, BGl_syncz00zzast_nodez00,
				BGl_proc2907z00zzsaw_node2rtlz00, BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2908z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_switchz00zzast_nodez00, BGl_proc2909z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2910z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00, BGl_appz00zzast_nodez00,
				BGl_proc2911z00zzsaw_node2rtlz00, BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2912z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_funcallz00zzast_nodez00, BGl_proc2913z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_pragmaz00zzast_nodez00, BGl_proc2914z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_getfieldz00zzast_nodez00, BGl_proc2915z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_setfieldz00zzast_nodez00, BGl_proc2916z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00, BGl_newz00zzast_nodez00,
				BGl_proc2917z00zzsaw_node2rtlz00, BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_vallocz00zzast_nodez00, BGl_proc2918z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00, BGl_vrefz00zzast_nodez00,
				BGl_proc2919z00zzsaw_node2rtlz00, BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_vsetz12z12zzast_nodez00, BGl_proc2920z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_vlengthz00zzast_nodez00, BGl_proc2921z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_instanceofz00zzast_nodez00, BGl_proc2922z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00, BGl_castz00zzast_nodez00,
				BGl_proc2923z00zzsaw_node2rtlz00, BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_castzd2nullzd2zzast_nodez00, BGl_proc2924z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2925z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2926z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00, BGl_failz00zzast_nodez00,
				BGl_proc2927z00zzsaw_node2rtlz00, BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2928z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2929z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2930z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_retblockz00zzast_nodez00, BGl_proc2931z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2ze3rtlzd2envze3zzsaw_node2rtlz00,
				BGl_returnz00zzast_nodez00, BGl_proc2932z00zzsaw_node2rtlz00,
				BGl_string2902z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_predicatezd2envzd2zzsaw_node2rtlz00, BGl_atomz00zzast_nodez00,
				BGl_proc2933z00zzsaw_node2rtlz00, BGl_string2934z00zzsaw_node2rtlz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_predicatezd2envzd2zzsaw_node2rtlz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2935z00zzsaw_node2rtlz00,
				BGl_string2934z00zzsaw_node2rtlz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_predicatezd2envzd2zzsaw_node2rtlz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc2936z00zzsaw_node2rtlz00, BGl_string2934z00zzsaw_node2rtlz00);
		}

	}



/* &predicate-let-var1787 */
	BgL_areaz00_bglt BGl_z62predicatezd2letzd2var1787z62zzsaw_node2rtlz00(obj_t
		BgL_envz00_5953, obj_t BgL_ez00_5954, obj_t BgL_lz00_5955)
	{
		{	/* SawMill/node2rtl.scm 529 */
			{	/* SawMill/node2rtl.scm 533 */
				bool_t BgL_test3120z00_8406;

				if (NULLP(
						(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_ez00_5954)))->BgL_bindingsz00)))
					{	/* SawMill/node2rtl.scm 533 */
						BgL_test3120z00_8406 = ((bool_t) 0);
					}
				else
					{	/* SawMill/node2rtl.scm 533 */
						if (NULLP(CDR(
									(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_ez00_5954)))->
										BgL_bindingsz00))))
							{	/* SawMill/node2rtl.scm 535 */
								bool_t BgL_test3123z00_8416;

								{	/* SawMill/node2rtl.scm 535 */
									BgL_nodez00_bglt BgL_arg2674z00_6156;

									BgL_arg2674z00_6156 =
										(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_ez00_5954)))->BgL_bodyz00);
									{	/* SawMill/node2rtl.scm 535 */
										obj_t BgL_classz00_6157;

										BgL_classz00_6157 = BGl_varz00zzast_nodez00;
										{	/* SawMill/node2rtl.scm 535 */
											BgL_objectz00_bglt BgL_arg1807z00_6158;

											{	/* SawMill/node2rtl.scm 535 */
												obj_t BgL_tmpz00_8419;

												BgL_tmpz00_8419 =
													((obj_t) ((BgL_objectz00_bglt) BgL_arg2674z00_6156));
												BgL_arg1807z00_6158 =
													(BgL_objectz00_bglt) (BgL_tmpz00_8419);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawMill/node2rtl.scm 535 */
													long BgL_idxz00_6159;

													BgL_idxz00_6159 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6158);
													BgL_test3123z00_8416 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_6159 + 2L)) == BgL_classz00_6157);
												}
											else
												{	/* SawMill/node2rtl.scm 535 */
													bool_t BgL_res2857z00_6162;

													{	/* SawMill/node2rtl.scm 535 */
														obj_t BgL_oclassz00_6163;

														{	/* SawMill/node2rtl.scm 535 */
															obj_t BgL_arg1815z00_6164;
															long BgL_arg1816z00_6165;

															BgL_arg1815z00_6164 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawMill/node2rtl.scm 535 */
																long BgL_arg1817z00_6166;

																BgL_arg1817z00_6166 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6158);
																BgL_arg1816z00_6165 =
																	(BgL_arg1817z00_6166 - OBJECT_TYPE);
															}
															BgL_oclassz00_6163 =
																VECTOR_REF(BgL_arg1815z00_6164,
																BgL_arg1816z00_6165);
														}
														{	/* SawMill/node2rtl.scm 535 */
															bool_t BgL__ortest_1115z00_6167;

															BgL__ortest_1115z00_6167 =
																(BgL_classz00_6157 == BgL_oclassz00_6163);
															if (BgL__ortest_1115z00_6167)
																{	/* SawMill/node2rtl.scm 535 */
																	BgL_res2857z00_6162 =
																		BgL__ortest_1115z00_6167;
																}
															else
																{	/* SawMill/node2rtl.scm 535 */
																	long BgL_odepthz00_6168;

																	{	/* SawMill/node2rtl.scm 535 */
																		obj_t BgL_arg1804z00_6169;

																		BgL_arg1804z00_6169 = (BgL_oclassz00_6163);
																		BgL_odepthz00_6168 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_6169);
																	}
																	if ((2L < BgL_odepthz00_6168))
																		{	/* SawMill/node2rtl.scm 535 */
																			obj_t BgL_arg1802z00_6170;

																			{	/* SawMill/node2rtl.scm 535 */
																				obj_t BgL_arg1803z00_6171;

																				BgL_arg1803z00_6171 =
																					(BgL_oclassz00_6163);
																				BgL_arg1802z00_6170 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_6171, 2L);
																			}
																			BgL_res2857z00_6162 =
																				(BgL_arg1802z00_6170 ==
																				BgL_classz00_6157);
																		}
																	else
																		{	/* SawMill/node2rtl.scm 535 */
																			BgL_res2857z00_6162 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test3123z00_8416 = BgL_res2857z00_6162;
												}
										}
									}
								}
								if (BgL_test3123z00_8416)
									{	/* SawMill/node2rtl.scm 536 */
										obj_t BgL_tmpz00_8442;

										{	/* SawMill/node2rtl.scm 536 */
											obj_t BgL_pairz00_6172;

											BgL_pairz00_6172 =
												(((BgL_letzd2varzd2_bglt) COBJECT(
														((BgL_letzd2varzd2_bglt) BgL_ez00_5954)))->
												BgL_bindingsz00);
											BgL_tmpz00_8442 = CAR(CAR(BgL_pairz00_6172));
										}
										BgL_test3120z00_8406 =
											(BgL_tmpz00_8442 ==
											((obj_t)
												(((BgL_varz00_bglt) COBJECT(
															((BgL_varz00_bglt)
																(((BgL_letzd2varzd2_bglt) COBJECT(
																			((BgL_letzd2varzd2_bglt)
																				BgL_ez00_5954)))->BgL_bodyz00))))->
													BgL_variablez00)));
									}
								else
									{	/* SawMill/node2rtl.scm 535 */
										BgL_test3120z00_8406 = ((bool_t) 0);
									}
							}
						else
							{	/* SawMill/node2rtl.scm 534 */
								BgL_test3120z00_8406 = ((bool_t) 0);
							}
					}
				if (BgL_test3120z00_8406)
					{	/* SawMill/node2rtl.scm 538 */
						obj_t BgL_arg2607z00_6173;

						{	/* SawMill/node2rtl.scm 538 */
							obj_t BgL_pairz00_6174;

							BgL_pairz00_6174 =
								(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_ez00_5954)))->BgL_bindingsz00);
							BgL_arg2607z00_6173 = CDR(CAR(BgL_pairz00_6174));
						}
						return
							BGl_predicatez00zzsaw_node2rtlz00(
							((BgL_nodez00_bglt) BgL_arg2607z00_6173), BgL_lz00_5955);
					}
				else
					{	/* SawMill/node2rtl.scm 539 */
						bool_t BgL_test3127z00_8459;

						{	/* SawMill/node2rtl.scm 539 */
							bool_t BgL_test3128z00_8460;

							{	/* SawMill/node2rtl.scm 539 */
								BgL_nodez00_bglt BgL_arg2667z00_6175;

								BgL_arg2667z00_6175 =
									(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_ez00_5954)))->BgL_bodyz00);
								{	/* SawMill/node2rtl.scm 539 */
									obj_t BgL_classz00_6176;

									BgL_classz00_6176 = BGl_conditionalz00zzast_nodez00;
									{	/* SawMill/node2rtl.scm 539 */
										BgL_objectz00_bglt BgL_arg1807z00_6177;

										{	/* SawMill/node2rtl.scm 539 */
											obj_t BgL_tmpz00_8463;

											BgL_tmpz00_8463 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg2667z00_6175));
											BgL_arg1807z00_6177 =
												(BgL_objectz00_bglt) (BgL_tmpz00_8463);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawMill/node2rtl.scm 539 */
												long BgL_idxz00_6178;

												BgL_idxz00_6178 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6177);
												BgL_test3128z00_8460 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_6178 + 3L)) == BgL_classz00_6176);
											}
										else
											{	/* SawMill/node2rtl.scm 539 */
												bool_t BgL_res2858z00_6181;

												{	/* SawMill/node2rtl.scm 539 */
													obj_t BgL_oclassz00_6182;

													{	/* SawMill/node2rtl.scm 539 */
														obj_t BgL_arg1815z00_6183;
														long BgL_arg1816z00_6184;

														BgL_arg1815z00_6183 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawMill/node2rtl.scm 539 */
															long BgL_arg1817z00_6185;

															BgL_arg1817z00_6185 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6177);
															BgL_arg1816z00_6184 =
																(BgL_arg1817z00_6185 - OBJECT_TYPE);
														}
														BgL_oclassz00_6182 =
															VECTOR_REF(BgL_arg1815z00_6183,
															BgL_arg1816z00_6184);
													}
													{	/* SawMill/node2rtl.scm 539 */
														bool_t BgL__ortest_1115z00_6186;

														BgL__ortest_1115z00_6186 =
															(BgL_classz00_6176 == BgL_oclassz00_6182);
														if (BgL__ortest_1115z00_6186)
															{	/* SawMill/node2rtl.scm 539 */
																BgL_res2858z00_6181 = BgL__ortest_1115z00_6186;
															}
														else
															{	/* SawMill/node2rtl.scm 539 */
																long BgL_odepthz00_6187;

																{	/* SawMill/node2rtl.scm 539 */
																	obj_t BgL_arg1804z00_6188;

																	BgL_arg1804z00_6188 = (BgL_oclassz00_6182);
																	BgL_odepthz00_6187 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_6188);
																}
																if ((3L < BgL_odepthz00_6187))
																	{	/* SawMill/node2rtl.scm 539 */
																		obj_t BgL_arg1802z00_6189;

																		{	/* SawMill/node2rtl.scm 539 */
																			obj_t BgL_arg1803z00_6190;

																			BgL_arg1803z00_6190 =
																				(BgL_oclassz00_6182);
																			BgL_arg1802z00_6189 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_6190, 3L);
																		}
																		BgL_res2858z00_6181 =
																			(BgL_arg1802z00_6189 ==
																			BgL_classz00_6176);
																	}
																else
																	{	/* SawMill/node2rtl.scm 539 */
																		BgL_res2858z00_6181 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test3128z00_8460 = BgL_res2858z00_6181;
											}
									}
								}
							}
							if (BgL_test3128z00_8460)
								{	/* SawMill/node2rtl.scm 539 */
									if (NULLP(
											(((BgL_letzd2varzd2_bglt) COBJECT(
														((BgL_letzd2varzd2_bglt) BgL_ez00_5954)))->
												BgL_bindingsz00)))
										{	/* SawMill/node2rtl.scm 540 */
											BgL_test3127z00_8459 = ((bool_t) 0);
										}
									else
										{	/* SawMill/node2rtl.scm 540 */
											if (NULLP(CDR(
														(((BgL_letzd2varzd2_bglt) COBJECT(
																	((BgL_letzd2varzd2_bglt) BgL_ez00_5954)))->
															BgL_bindingsz00))))
												{	/* SawMill/node2rtl.scm 542 */
													bool_t BgL_test3134z00_8495;

													{	/* SawMill/node2rtl.scm 542 */
														BgL_nodez00_bglt BgL_arg2660z00_6191;

														BgL_arg2660z00_6191 =
															(((BgL_conditionalz00_bglt) COBJECT(
																	((BgL_conditionalz00_bglt)
																		(((BgL_letzd2varzd2_bglt) COBJECT(
																					((BgL_letzd2varzd2_bglt)
																						BgL_ez00_5954)))->BgL_bodyz00))))->
															BgL_testz00);
														{	/* SawMill/node2rtl.scm 542 */
															obj_t BgL_classz00_6192;

															BgL_classz00_6192 = BGl_varz00zzast_nodez00;
															{	/* SawMill/node2rtl.scm 542 */
																BgL_objectz00_bglt BgL_arg1807z00_6193;

																{	/* SawMill/node2rtl.scm 542 */
																	obj_t BgL_tmpz00_8500;

																	BgL_tmpz00_8500 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_arg2660z00_6191));
																	BgL_arg1807z00_6193 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_8500);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* SawMill/node2rtl.scm 542 */
																		long BgL_idxz00_6194;

																		BgL_idxz00_6194 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_6193);
																		BgL_test3134z00_8495 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_6194 + 2L)) ==
																			BgL_classz00_6192);
																	}
																else
																	{	/* SawMill/node2rtl.scm 542 */
																		bool_t BgL_res2859z00_6197;

																		{	/* SawMill/node2rtl.scm 542 */
																			obj_t BgL_oclassz00_6198;

																			{	/* SawMill/node2rtl.scm 542 */
																				obj_t BgL_arg1815z00_6199;
																				long BgL_arg1816z00_6200;

																				BgL_arg1815z00_6199 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* SawMill/node2rtl.scm 542 */
																					long BgL_arg1817z00_6201;

																					BgL_arg1817z00_6201 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_6193);
																					BgL_arg1816z00_6200 =
																						(BgL_arg1817z00_6201 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_6198 =
																					VECTOR_REF(BgL_arg1815z00_6199,
																					BgL_arg1816z00_6200);
																			}
																			{	/* SawMill/node2rtl.scm 542 */
																				bool_t BgL__ortest_1115z00_6202;

																				BgL__ortest_1115z00_6202 =
																					(BgL_classz00_6192 ==
																					BgL_oclassz00_6198);
																				if (BgL__ortest_1115z00_6202)
																					{	/* SawMill/node2rtl.scm 542 */
																						BgL_res2859z00_6197 =
																							BgL__ortest_1115z00_6202;
																					}
																				else
																					{	/* SawMill/node2rtl.scm 542 */
																						long BgL_odepthz00_6203;

																						{	/* SawMill/node2rtl.scm 542 */
																							obj_t BgL_arg1804z00_6204;

																							BgL_arg1804z00_6204 =
																								(BgL_oclassz00_6198);
																							BgL_odepthz00_6203 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_6204);
																						}
																						if ((2L < BgL_odepthz00_6203))
																							{	/* SawMill/node2rtl.scm 542 */
																								obj_t BgL_arg1802z00_6205;

																								{	/* SawMill/node2rtl.scm 542 */
																									obj_t BgL_arg1803z00_6206;

																									BgL_arg1803z00_6206 =
																										(BgL_oclassz00_6198);
																									BgL_arg1802z00_6205 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_6206, 2L);
																								}
																								BgL_res2859z00_6197 =
																									(BgL_arg1802z00_6205 ==
																									BgL_classz00_6192);
																							}
																						else
																							{	/* SawMill/node2rtl.scm 542 */
																								BgL_res2859z00_6197 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test3134z00_8495 = BgL_res2859z00_6197;
																	}
															}
														}
													}
													if (BgL_test3134z00_8495)
														{	/* SawMill/node2rtl.scm 543 */
															bool_t BgL_test3138z00_8523;

															{	/* SawMill/node2rtl.scm 543 */
																long BgL_tmpz00_8524;

																{
																	BgL_variablez00_bglt BgL_auxz00_8525;

																	{
																		BgL_localz00_bglt BgL_auxz00_8526;

																		{
																			obj_t BgL_auxz00_8527;

																			{	/* SawMill/node2rtl.scm 543 */
																				obj_t BgL_pairz00_6207;

																				BgL_pairz00_6207 =
																					(((BgL_letzd2varzd2_bglt) COBJECT(
																							((BgL_letzd2varzd2_bglt)
																								BgL_ez00_5954)))->
																					BgL_bindingsz00);
																				{	/* SawMill/node2rtl.scm 543 */
																					obj_t BgL_pairz00_6208;

																					BgL_pairz00_6208 =
																						CAR(BgL_pairz00_6207);
																					BgL_auxz00_8527 =
																						CAR(BgL_pairz00_6208);
																			}}
																			BgL_auxz00_8526 =
																				((BgL_localz00_bglt) BgL_auxz00_8527);
																		}
																		BgL_auxz00_8525 =
																			((BgL_variablez00_bglt) BgL_auxz00_8526);
																	}
																	BgL_tmpz00_8524 =
																		(((BgL_variablez00_bglt)
																			COBJECT(BgL_auxz00_8525))->
																		BgL_occurrencez00);
																}
																BgL_test3138z00_8523 = (1L == BgL_tmpz00_8524);
															}
															if (BgL_test3138z00_8523)
																{	/* SawMill/node2rtl.scm 544 */
																	obj_t BgL_tmpz00_8536;

																	{	/* SawMill/node2rtl.scm 544 */
																		obj_t BgL_pairz00_6209;

																		BgL_pairz00_6209 =
																			(((BgL_letzd2varzd2_bglt) COBJECT(
																					((BgL_letzd2varzd2_bglt)
																						BgL_ez00_5954)))->BgL_bindingsz00);
																		BgL_tmpz00_8536 =
																			CAR(CAR(BgL_pairz00_6209));
																	}
																	BgL_test3127z00_8459 =
																		(BgL_tmpz00_8536 ==
																		((obj_t)
																			(((BgL_varz00_bglt) COBJECT(
																						((BgL_varz00_bglt)
																							(((BgL_conditionalz00_bglt)
																									COBJECT((
																											(BgL_conditionalz00_bglt)
																											(((BgL_letzd2varzd2_bglt)
																													COBJECT((
																															(BgL_letzd2varzd2_bglt)
																															BgL_ez00_5954)))->
																												BgL_bodyz00))))->
																								BgL_testz00))))->
																				BgL_variablez00)));
																}
															else
																{	/* SawMill/node2rtl.scm 543 */
																	BgL_test3127z00_8459 = ((bool_t) 0);
																}
														}
													else
														{	/* SawMill/node2rtl.scm 542 */
															BgL_test3127z00_8459 = ((bool_t) 0);
														}
												}
											else
												{	/* SawMill/node2rtl.scm 541 */
													BgL_test3127z00_8459 = ((bool_t) 0);
												}
										}
								}
							else
								{	/* SawMill/node2rtl.scm 539 */
									BgL_test3127z00_8459 = ((bool_t) 0);
								}
						}
						if (BgL_test3127z00_8459)
							{	/* SawMill/node2rtl.scm 539 */
								{	/* SawMill/node2rtl.scm 546 */
									BgL_nodez00_bglt BgL_arg2641z00_6210;
									obj_t BgL_arg2642z00_6211;

									BgL_arg2641z00_6210 =
										(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_ez00_5954)))->BgL_bodyz00);
									{	/* SawMill/node2rtl.scm 546 */
										obj_t BgL_pairz00_6212;

										BgL_pairz00_6212 =
											(((BgL_letzd2varzd2_bglt) COBJECT(
													((BgL_letzd2varzd2_bglt) BgL_ez00_5954)))->
											BgL_bindingsz00);
										BgL_arg2642z00_6211 = CDR(CAR(BgL_pairz00_6212));
									}
									((((BgL_conditionalz00_bglt) COBJECT(
													((BgL_conditionalz00_bglt) BgL_arg2641z00_6210)))->
											BgL_testz00) =
										((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
												BgL_arg2642z00_6211)), BUNSPEC);
								}
								return
									BGl_predicatez00zzsaw_node2rtlz00(
									(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_ez00_5954)))->BgL_bodyz00),
									BgL_lz00_5955);
							}
						else
							{	/* SawMill/node2rtl.scm 549 */
								BgL_areaz00_bglt BgL_arg2645z00_6213;

								{	/* SawMill/node2rtl.scm 549 */
									BgL_rtl_ifz00_bglt BgL_arg2647z00_6214;

									{	/* SawMill/node2rtl.scm 549 */
										BgL_rtl_ifz00_bglt BgL_new1384z00_6215;

										{	/* SawMill/node2rtl.scm 549 */
											BgL_rtl_ifz00_bglt BgL_new1383z00_6216;

											BgL_new1383z00_6216 =
												((BgL_rtl_ifz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_rtl_ifz00_bgl))));
											{	/* SawMill/node2rtl.scm 549 */
												long BgL_arg2649z00_6217;

												BgL_arg2649z00_6217 =
													BGL_CLASS_NUM(BGl_rtl_ifz00zzsaw_defsz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1383z00_6216),
													BgL_arg2649z00_6217);
											}
											BgL_new1384z00_6215 = BgL_new1383z00_6216;
										}
										((((BgL_rtl_funz00_bglt) COBJECT(
														((BgL_rtl_funz00_bglt) BgL_new1384z00_6215)))->
												BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
										BgL_arg2647z00_6214 = BgL_new1384z00_6215;
									}
									{	/* SawMill/node2rtl.scm 549 */
										obj_t BgL_list2648z00_6218;

										BgL_list2648z00_6218 =
											MAKE_YOUNG_PAIR(
											((obj_t) ((BgL_letzd2varzd2_bglt) BgL_ez00_5954)), BNIL);
										BgL_arg2645z00_6213 =
											BGl_callza2za2zzsaw_node2rtlz00(
											((obj_t)
												((BgL_letzd2varzd2_bglt) BgL_ez00_5954)),
											((BgL_rtl_funz00_bglt) BgL_arg2647z00_6214),
											BgL_list2648z00_6218);
								}}
								return
									BGl_forkz00zzsaw_node2rtlz00(BgL_arg2645z00_6213,
									BgL_lz00_5955);
							}
					}
			}
		}

	}



/* &predicate-conditiona1785 */
	BgL_areaz00_bglt BGl_z62predicatezd2conditiona1785zb0zzsaw_node2rtlz00(obj_t
		BgL_envz00_5956, obj_t BgL_ez00_5957, obj_t BgL_lz00_5958)
	{
		{	/* SawMill/node2rtl.scm 524 */
			{	/* SawMill/node2rtl.scm 526 */
				BgL_nodez00_bglt BgL_arg2566z00_6220;
				obj_t BgL_arg2567z00_6221;

				BgL_arg2566z00_6220 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_ez00_5957)))->BgL_testz00);
				{	/* SawMill/node2rtl.scm 526 */
					BgL_areaz00_bglt BgL_arg2568z00_6222;
					BgL_areaz00_bglt BgL_arg2572z00_6223;

					BgL_arg2568z00_6222 =
						BGl_predicatez00zzsaw_node2rtlz00(
						(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_ez00_5957)))->BgL_truez00),
						BgL_lz00_5958);
					BgL_arg2572z00_6223 =
						BGl_predicatez00zzsaw_node2rtlz00((((BgL_conditionalz00_bglt)
								COBJECT(((BgL_conditionalz00_bglt) BgL_ez00_5957)))->
							BgL_falsez00), BgL_lz00_5958);
					{	/* SawMill/node2rtl.scm 526 */
						obj_t BgL_list2573z00_6224;

						{	/* SawMill/node2rtl.scm 526 */
							obj_t BgL_arg2578z00_6225;

							BgL_arg2578z00_6225 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg2572z00_6223), BNIL);
							BgL_list2573z00_6224 =
								MAKE_YOUNG_PAIR(
								((obj_t) BgL_arg2568z00_6222), BgL_arg2578z00_6225);
						}
						BgL_arg2567z00_6221 = BgL_list2573z00_6224;
					}
				}
				return
					BGl_predicatez00zzsaw_node2rtlz00(BgL_arg2566z00_6220,
					BgL_arg2567z00_6221);
			}
		}

	}



/* &predicate-atom1783 */
	BgL_areaz00_bglt BGl_z62predicatezd2atom1783zb0zzsaw_node2rtlz00(obj_t
		BgL_envz00_5959, obj_t BgL_ez00_5960, obj_t BgL_lz00_5961)
	{
		{	/* SawMill/node2rtl.scm 517 */
			if (CBOOL(
					(((BgL_atomz00_bglt) COBJECT(
								((BgL_atomz00_bglt) BgL_ez00_5960)))->BgL_valuez00)))
				{	/* SawMill/node2rtl.scm 519 */
					return ((BgL_areaz00_bglt) CAR(((obj_t) BgL_lz00_5961)));
				}
			else
				{	/* SawMill/node2rtl.scm 521 */
					obj_t BgL_pairz00_6227;

					BgL_pairz00_6227 = CDR(((obj_t) BgL_lz00_5961));
					return ((BgL_areaz00_bglt) CAR(BgL_pairz00_6227));
				}
		}

	}



/* &node->rtl-return1779 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2return1779z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_5962, obj_t BgL_ez00_5963)
	{
		{	/* SawMill/node2rtl.scm 499 */
			{	/* SawMill/node2rtl.scm 501 */
				BgL_retblockz00_bglt BgL_i1377z00_6229;

				BgL_i1377z00_6229 =
					((BgL_retblockz00_bglt)
					(((BgL_returnz00_bglt) COBJECT(
								((BgL_returnz00_bglt) BgL_ez00_5963)))->BgL_blockz00));
				{	/* SawMill/node2rtl.scm 502 */
					BgL_areaz00_bglt BgL_rz00_6230;

					{	/* SawMill/node2rtl.scm 502 */
						BgL_nodez00_bglt BgL_arg2563z00_6231;
						BgL_rtl_regz00_bglt BgL_arg2564z00_6232;

						BgL_arg2563z00_6231 =
							(((BgL_returnz00_bglt) COBJECT(
									((BgL_returnz00_bglt) BgL_ez00_5963)))->BgL_valuez00);
						{
							BgL_retblockzf2tozf2_bglt BgL_auxz00_8604;

							{
								obj_t BgL_auxz00_8605;

								{	/* SawMill/node2rtl.scm 502 */
									BgL_objectz00_bglt BgL_tmpz00_8606;

									BgL_tmpz00_8606 = ((BgL_objectz00_bglt) BgL_i1377z00_6229);
									BgL_auxz00_8605 = BGL_OBJECT_WIDENING(BgL_tmpz00_8606);
								}
								BgL_auxz00_8604 = ((BgL_retblockzf2tozf2_bglt) BgL_auxz00_8605);
							}
							BgL_arg2564z00_6232 =
								(((BgL_retblockzf2tozf2_bglt) COBJECT(BgL_auxz00_8604))->
								BgL_dstz00);
						}
						{	/* SawMill/node2rtl.scm 502 */
							BgL_areaz00_bglt BgL_res2855z00_6233;

							{	/* SawMill/node2rtl.scm 187 */
								BgL_areaz00_bglt BgL_az00_6234;

								BgL_az00_6234 =
									BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(BgL_arg2563z00_6231);
								BGl_bdestinationz12z12zzsaw_node2rtlz00(
									(((BgL_areaz00_bglt) COBJECT(BgL_az00_6234))->BgL_exitz00),
									BgL_arg2564z00_6232);
								BgL_res2855z00_6233 = BgL_az00_6234;
							}
							BgL_rz00_6230 = BgL_res2855z00_6233;
						}
					}
					{	/* SawMill/node2rtl.scm 503 */
						BgL_blockz00_bglt BgL_arg2561z00_6235;
						BgL_blockz00_bglt BgL_arg2562z00_6236;

						BgL_arg2561z00_6235 =
							(((BgL_areaz00_bglt) COBJECT(BgL_rz00_6230))->BgL_exitz00);
						{
							BgL_retblockzf2tozf2_bglt BgL_auxz00_8615;

							{
								obj_t BgL_auxz00_8616;

								{	/* SawMill/node2rtl.scm 503 */
									BgL_objectz00_bglt BgL_tmpz00_8617;

									BgL_tmpz00_8617 = ((BgL_objectz00_bglt) BgL_i1377z00_6229);
									BgL_auxz00_8616 = BGL_OBJECT_WIDENING(BgL_tmpz00_8617);
								}
								BgL_auxz00_8615 = ((BgL_retblockzf2tozf2_bglt) BgL_auxz00_8616);
							}
							BgL_arg2562z00_6236 =
								(((BgL_retblockzf2tozf2_bglt) COBJECT(BgL_auxz00_8615))->
								BgL_toz00);
						}
						{	/* SawMill/node2rtl.scm 92 */
							obj_t BgL_arg1882z00_6237;

							{	/* SawMill/node2rtl.scm 92 */
								obj_t BgL_arg1883z00_6238;

								BgL_arg1883z00_6238 =
									(((BgL_blockz00_bglt) COBJECT(BgL_arg2561z00_6235))->
									BgL_succsz00);
								BgL_arg1882z00_6237 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_arg2562z00_6236),
									BgL_arg1883z00_6238);
							}
							((((BgL_blockz00_bglt) COBJECT(BgL_arg2561z00_6235))->
									BgL_succsz00) = ((obj_t) BgL_arg1882z00_6237), BUNSPEC);
						}
					}
					return BGl_unlinkz00zzsaw_node2rtlz00(BgL_rz00_6230);
				}
			}
		}

	}



/* &node->rtl-retblock1777 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2retblock1777z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_5964, obj_t BgL_ez00_5965)
	{
		{	/* SawMill/node2rtl.scm 483 */
			{	/* SawMill/node2rtl.scm 486 */
				BgL_rtl_regz00_bglt BgL_dstz00_6240;

				BgL_dstz00_6240 =
					BGl_newzd2regzd2zzsaw_node2rtlz00(
					((BgL_nodez00_bglt) ((BgL_retblockz00_bglt) BgL_ez00_5965)));
				{	/* SawMill/node2rtl.scm 487 */
					BgL_areaz00_bglt BgL_joinz00_6241;

					{	/* SawMill/node2rtl.scm 487 */
						BgL_rtl_movz00_bglt BgL_arg2557z00_6242;

						{	/* SawMill/node2rtl.scm 487 */
							BgL_rtl_movz00_bglt BgL_new1371z00_6243;

							{	/* SawMill/node2rtl.scm 487 */
								BgL_rtl_movz00_bglt BgL_new1370z00_6244;

								BgL_new1370z00_6244 =
									((BgL_rtl_movz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_rtl_movz00_bgl))));
								{	/* SawMill/node2rtl.scm 487 */
									long BgL_arg2560z00_6245;

									BgL_arg2560z00_6245 =
										BGL_CLASS_NUM(BGl_rtl_movz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1370z00_6244),
										BgL_arg2560z00_6245);
								}
								BgL_new1371z00_6243 = BgL_new1370z00_6244;
							}
							((((BgL_rtl_funz00_bglt) COBJECT(
											((BgL_rtl_funz00_bglt) BgL_new1371z00_6243)))->
									BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
							BgL_arg2557z00_6242 = BgL_new1371z00_6243;
						}
						{	/* SawMill/node2rtl.scm 487 */
							obj_t BgL_list2558z00_6246;

							BgL_list2558z00_6246 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_dstz00_6240), BNIL);
							BgL_joinz00_6241 =
								BGl_singleza2za2zzsaw_node2rtlz00(BFALSE,
								((BgL_rtl_funz00_bglt) BgL_arg2557z00_6242),
								BgL_list2558z00_6246);
					}}
					{	/* SawMill/node2rtl.scm 488 */
						BgL_retblockzf2tozf2_bglt BgL_wide1374z00_6247;

						BgL_wide1374z00_6247 =
							((BgL_retblockzf2tozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_retblockzf2tozf2_bgl))));
						{	/* SawMill/node2rtl.scm 488 */
							obj_t BgL_auxz00_8645;
							BgL_objectz00_bglt BgL_tmpz00_8641;

							BgL_auxz00_8645 = ((obj_t) BgL_wide1374z00_6247);
							BgL_tmpz00_8641 =
								((BgL_objectz00_bglt)
								((BgL_retblockz00_bglt)
									((BgL_retblockz00_bglt) BgL_ez00_5965)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8641, BgL_auxz00_8645);
						}
						((BgL_objectz00_bglt)
							((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_ez00_5965)));
						{	/* SawMill/node2rtl.scm 488 */
							long BgL_arg2554z00_6248;

							BgL_arg2554z00_6248 =
								BGL_CLASS_NUM(BGl_retblockzf2tozf2zzsaw_node2rtlz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_retblockz00_bglt)
										((BgL_retblockz00_bglt) BgL_ez00_5965))),
								BgL_arg2554z00_6248);
						}
						((BgL_retblockz00_bglt)
							((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_ez00_5965)));
					}
					{
						BgL_retblockzf2tozf2_bglt BgL_auxz00_8659;

						{
							obj_t BgL_auxz00_8660;

							{	/* SawMill/node2rtl.scm 488 */
								BgL_objectz00_bglt BgL_tmpz00_8661;

								BgL_tmpz00_8661 =
									((BgL_objectz00_bglt)
									((BgL_retblockz00_bglt)
										((BgL_retblockz00_bglt) BgL_ez00_5965)));
								BgL_auxz00_8660 = BGL_OBJECT_WIDENING(BgL_tmpz00_8661);
							}
							BgL_auxz00_8659 = ((BgL_retblockzf2tozf2_bglt) BgL_auxz00_8660);
						}
						((((BgL_retblockzf2tozf2_bglt) COBJECT(BgL_auxz00_8659))->
								BgL_toz00) =
							((BgL_blockz00_bglt) (((BgL_areaz00_bglt)
										COBJECT(BgL_joinz00_6241))->BgL_entryz00)), BUNSPEC);
					}
					{
						BgL_retblockzf2tozf2_bglt BgL_auxz00_8669;

						{
							obj_t BgL_auxz00_8670;

							{	/* SawMill/node2rtl.scm 488 */
								BgL_objectz00_bglt BgL_tmpz00_8671;

								BgL_tmpz00_8671 =
									((BgL_objectz00_bglt)
									((BgL_retblockz00_bglt)
										((BgL_retblockz00_bglt) BgL_ez00_5965)));
								BgL_auxz00_8670 = BGL_OBJECT_WIDENING(BgL_tmpz00_8671);
							}
							BgL_auxz00_8669 = ((BgL_retblockzf2tozf2_bglt) BgL_auxz00_8670);
						}
						((((BgL_retblockzf2tozf2_bglt) COBJECT(BgL_auxz00_8669))->
								BgL_dstz00) = ((BgL_rtl_regz00_bglt) BgL_dstz00_6240), BUNSPEC);
					}
					((BgL_retblockz00_bglt) ((BgL_retblockz00_bglt) BgL_ez00_5965));
					{	/* SawMill/node2rtl.scm 489 */
						BgL_areaz00_bglt BgL_arg2555z00_6249;

						{	/* SawMill/node2rtl.scm 489 */
							BgL_nodez00_bglt BgL_arg2556z00_6250;

							BgL_arg2556z00_6250 =
								(((BgL_retblockz00_bglt) COBJECT(
										((BgL_retblockz00_bglt) BgL_ez00_5965)))->BgL_bodyz00);
							{	/* SawMill/node2rtl.scm 489 */
								BgL_areaz00_bglt BgL_res2854z00_6251;

								{	/* SawMill/node2rtl.scm 187 */
									BgL_areaz00_bglt BgL_az00_6252;

									BgL_az00_6252 =
										BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(BgL_arg2556z00_6250);
									BGl_bdestinationz12z12zzsaw_node2rtlz00(
										(((BgL_areaz00_bglt) COBJECT(BgL_az00_6252))->BgL_exitz00),
										BgL_dstz00_6240);
									BgL_res2854z00_6251 = BgL_az00_6252;
								}
								BgL_arg2555z00_6249 = BgL_res2854z00_6251;
						}}
						return
							BGl_linkz00zzsaw_node2rtlz00(BgL_arg2555z00_6249,
							BgL_joinz00_6241);
					}
				}
			}
		}

	}



/* &node->rtl-box-set!1775 */
	BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2boxzd2setz121775z41zzsaw_node2rtlz00(obj_t
		BgL_envz00_5966, obj_t BgL_ez00_5967)
	{
		{	/* SawMill/node2rtl.scm 477 */
			{	/* SawMill/node2rtl.scm 479 */
				BgL_areaz00_bglt BgL_arg2539z00_6254;
				BgL_areaz00_bglt BgL_arg2540z00_6255;

				{	/* SawMill/node2rtl.scm 479 */
					BgL_rtl_boxsetz00_bglt BgL_arg2542z00_6256;
					BgL_varz00_bglt BgL_arg2543z00_6257;
					BgL_nodez00_bglt BgL_arg2545z00_6258;

					{	/* SawMill/node2rtl.scm 479 */
						BgL_rtl_boxsetz00_bglt BgL_new1366z00_6259;

						{	/* SawMill/node2rtl.scm 479 */
							BgL_rtl_boxsetz00_bglt BgL_new1365z00_6260;

							BgL_new1365z00_6260 =
								((BgL_rtl_boxsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_boxsetz00_bgl))));
							{	/* SawMill/node2rtl.scm 479 */
								long BgL_arg2549z00_6261;

								BgL_arg2549z00_6261 =
									BGL_CLASS_NUM(BGl_rtl_boxsetz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1365z00_6260),
									BgL_arg2549z00_6261);
							}
							BgL_new1366z00_6259 = BgL_new1365z00_6260;
						}
						((((BgL_rtl_funz00_bglt) COBJECT(
										((BgL_rtl_funz00_bglt) BgL_new1366z00_6259)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						BgL_arg2542z00_6256 = BgL_new1366z00_6259;
					}
					BgL_arg2543z00_6257 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_ez00_5967)))->BgL_varz00);
					BgL_arg2545z00_6258 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_ez00_5967)))->BgL_valuez00);
					{	/* SawMill/node2rtl.scm 479 */
						obj_t BgL_list2546z00_6262;

						{	/* SawMill/node2rtl.scm 479 */
							obj_t BgL_arg2548z00_6263;

							BgL_arg2548z00_6263 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg2545z00_6258), BNIL);
							BgL_list2546z00_6262 =
								MAKE_YOUNG_PAIR(
								((obj_t) BgL_arg2543z00_6257), BgL_arg2548z00_6263);
						}
						BgL_arg2539z00_6254 =
							BGl_callza2za2zzsaw_node2rtlz00(
							((obj_t)
								((BgL_boxzd2setz12zc0_bglt) BgL_ez00_5967)),
							((BgL_rtl_funz00_bglt) BgL_arg2542z00_6256),
							BgL_list2546z00_6262);
				}}
				{	/* SawMill/node2rtl.scm 480 */
					BgL_rtl_nopz00_bglt BgL_arg2551z00_6264;

					{	/* SawMill/node2rtl.scm 480 */
						BgL_rtl_nopz00_bglt BgL_new1368z00_6265;

						{	/* SawMill/node2rtl.scm 480 */
							BgL_rtl_nopz00_bglt BgL_new1367z00_6266;

							BgL_new1367z00_6266 =
								((BgL_rtl_nopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_nopz00_bgl))));
							{	/* SawMill/node2rtl.scm 480 */
								long BgL_arg2553z00_6267;

								BgL_arg2553z00_6267 =
									BGL_CLASS_NUM(BGl_rtl_nopz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1367z00_6266),
									BgL_arg2553z00_6267);
							}
							BgL_new1368z00_6265 = BgL_new1367z00_6266;
						}
						((((BgL_rtl_funz00_bglt) COBJECT(
										((BgL_rtl_funz00_bglt) BgL_new1368z00_6265)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						BgL_arg2551z00_6264 = BgL_new1368z00_6265;
					}
					BgL_arg2540z00_6255 =
						BGl_singleza2za2zzsaw_node2rtlz00(BFALSE,
						((BgL_rtl_funz00_bglt) BgL_arg2551z00_6264), BNIL);
				}
				return
					BGl_linkz00zzsaw_node2rtlz00(BgL_arg2539z00_6254,
					BgL_arg2540z00_6255);
			}
		}

	}



/* &node->rtl-box-ref1773 */
	BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2boxzd2ref1773z53zzsaw_node2rtlz00(obj_t
		BgL_envz00_5968, obj_t BgL_ez00_5969)
	{
		{	/* SawMill/node2rtl.scm 472 */
			{	/* SawMill/node2rtl.scm 474 */
				BgL_rtl_boxrefz00_bglt BgL_arg2534z00_6269;
				BgL_varz00_bglt BgL_arg2536z00_6270;

				{	/* SawMill/node2rtl.scm 474 */
					BgL_rtl_boxrefz00_bglt BgL_new1363z00_6271;

					{	/* SawMill/node2rtl.scm 474 */
						BgL_rtl_boxrefz00_bglt BgL_new1362z00_6272;

						BgL_new1362z00_6272 =
							((BgL_rtl_boxrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_boxrefz00_bgl))));
						{	/* SawMill/node2rtl.scm 474 */
							long BgL_arg2538z00_6273;

							BgL_arg2538z00_6273 =
								BGL_CLASS_NUM(BGl_rtl_boxrefz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1362z00_6272),
								BgL_arg2538z00_6273);
						}
						BgL_new1363z00_6271 = BgL_new1362z00_6272;
					}
					((((BgL_rtl_funz00_bglt) COBJECT(
									((BgL_rtl_funz00_bglt) BgL_new1363z00_6271)))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					BgL_arg2534z00_6269 = BgL_new1363z00_6271;
				}
				BgL_arg2536z00_6270 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_ez00_5969)))->BgL_varz00);
				{	/* SawMill/node2rtl.scm 474 */
					obj_t BgL_list2537z00_6274;

					BgL_list2537z00_6274 =
						MAKE_YOUNG_PAIR(((obj_t) BgL_arg2536z00_6270), BNIL);
					return
						BGl_callza2za2zzsaw_node2rtlz00(
						((obj_t)
							((BgL_boxzd2refzd2_bglt) BgL_ez00_5969)),
						((BgL_rtl_funz00_bglt) BgL_arg2534z00_6269), BgL_list2537z00_6274);
				}
			}
		}

	}



/* &node->rtl-make-box1771 */
	BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2makezd2box1771z53zzsaw_node2rtlz00(obj_t
		BgL_envz00_5970, obj_t BgL_ez00_5971)
	{
		{	/* SawMill/node2rtl.scm 467 */
			{	/* SawMill/node2rtl.scm 469 */
				BgL_rtl_makeboxz00_bglt BgL_arg2526z00_6276;
				BgL_nodez00_bglt BgL_arg2527z00_6277;

				{	/* SawMill/node2rtl.scm 469 */
					BgL_rtl_makeboxz00_bglt BgL_new1360z00_6278;

					{	/* SawMill/node2rtl.scm 469 */
						BgL_rtl_makeboxz00_bglt BgL_new1359z00_6279;

						BgL_new1359z00_6279 =
							((BgL_rtl_makeboxz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_makeboxz00_bgl))));
						{	/* SawMill/node2rtl.scm 469 */
							long BgL_arg2529z00_6280;

							BgL_arg2529z00_6280 =
								BGL_CLASS_NUM(BGl_rtl_makeboxz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1359z00_6279),
								BgL_arg2529z00_6280);
						}
						BgL_new1360z00_6278 = BgL_new1359z00_6279;
					}
					((((BgL_rtl_funz00_bglt) COBJECT(
									((BgL_rtl_funz00_bglt) BgL_new1360z00_6278)))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					BgL_arg2526z00_6276 = BgL_new1360z00_6278;
				}
				BgL_arg2527z00_6277 =
					(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_ez00_5971)))->BgL_valuez00);
				{	/* SawMill/node2rtl.scm 469 */
					obj_t BgL_list2528z00_6281;

					BgL_list2528z00_6281 =
						MAKE_YOUNG_PAIR(((obj_t) BgL_arg2527z00_6277), BNIL);
					return
						BGl_callza2za2zzsaw_node2rtlz00(
						((obj_t)
							((BgL_makezd2boxzd2_bglt) BgL_ez00_5971)),
						((BgL_rtl_funz00_bglt) BgL_arg2526z00_6276), BgL_list2528z00_6281);
				}
			}
		}

	}



/* &node->rtl-fail1769 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2fail1769z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_5972, obj_t BgL_ez00_5973)
	{
		{	/* SawMill/node2rtl.scm 462 */
			{	/* SawMill/node2rtl.scm 464 */
				BgL_areaz00_bglt BgL_arg2513z00_6283;

				{	/* SawMill/node2rtl.scm 464 */
					BgL_rtl_failz00_bglt BgL_arg2514z00_6284;
					BgL_nodez00_bglt BgL_arg2515z00_6285;
					BgL_nodez00_bglt BgL_arg2516z00_6286;
					BgL_nodez00_bglt BgL_arg2518z00_6287;

					{	/* SawMill/node2rtl.scm 464 */
						BgL_rtl_failz00_bglt BgL_new1357z00_6288;

						{	/* SawMill/node2rtl.scm 464 */
							BgL_rtl_failz00_bglt BgL_new1356z00_6289;

							BgL_new1356z00_6289 =
								((BgL_rtl_failz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_failz00_bgl))));
							{	/* SawMill/node2rtl.scm 464 */
								long BgL_arg2525z00_6290;

								BgL_arg2525z00_6290 =
									BGL_CLASS_NUM(BGl_rtl_failz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1356z00_6289),
									BgL_arg2525z00_6290);
							}
							BgL_new1357z00_6288 = BgL_new1356z00_6289;
						}
						((((BgL_rtl_funz00_bglt) COBJECT(
										((BgL_rtl_funz00_bglt) BgL_new1357z00_6288)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						BgL_arg2514z00_6284 = BgL_new1357z00_6288;
					}
					BgL_arg2515z00_6285 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_ez00_5973)))->BgL_procz00);
					BgL_arg2516z00_6286 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_ez00_5973)))->BgL_msgz00);
					BgL_arg2518z00_6287 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_ez00_5973)))->BgL_objz00);
					{	/* SawMill/node2rtl.scm 464 */
						obj_t BgL_list2519z00_6291;

						{	/* SawMill/node2rtl.scm 464 */
							obj_t BgL_arg2521z00_6292;

							{	/* SawMill/node2rtl.scm 464 */
								obj_t BgL_arg2524z00_6293;

								BgL_arg2524z00_6293 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_arg2518z00_6287), BNIL);
								BgL_arg2521z00_6292 =
									MAKE_YOUNG_PAIR(
									((obj_t) BgL_arg2516z00_6286), BgL_arg2524z00_6293);
							}
							BgL_list2519z00_6291 =
								MAKE_YOUNG_PAIR(
								((obj_t) BgL_arg2515z00_6285), BgL_arg2521z00_6292);
						}
						BgL_arg2513z00_6283 =
							BGl_callza2za2zzsaw_node2rtlz00(
							((obj_t)
								((BgL_failz00_bglt) BgL_ez00_5973)),
							((BgL_rtl_funz00_bglt) BgL_arg2514z00_6284),
							BgL_list2519z00_6291);
				}}
				return BGl_unlinkz00zzsaw_node2rtlz00(BgL_arg2513z00_6283);
			}
		}

	}



/* &node->rtl-jump-ex-it1767 */
	BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2jumpzd2exzd2it1767z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_5974, obj_t BgL_ez00_5975)
	{
		{	/* SawMill/node2rtl.scm 457 */
			{	/* SawMill/node2rtl.scm 459 */
				BgL_areaz00_bglt BgL_arg2505z00_6295;

				{	/* SawMill/node2rtl.scm 459 */
					BgL_rtl_jumpexitz00_bglt BgL_arg2506z00_6296;
					BgL_nodez00_bglt BgL_arg2508z00_6297;
					BgL_nodez00_bglt BgL_arg2509z00_6298;

					{	/* SawMill/node2rtl.scm 459 */
						BgL_rtl_jumpexitz00_bglt BgL_new1354z00_6299;

						{	/* SawMill/node2rtl.scm 459 */
							BgL_rtl_jumpexitz00_bglt BgL_new1353z00_6300;

							BgL_new1353z00_6300 =
								((BgL_rtl_jumpexitz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_jumpexitz00_bgl))));
							{	/* SawMill/node2rtl.scm 459 */
								long BgL_arg2512z00_6301;

								BgL_arg2512z00_6301 =
									BGL_CLASS_NUM(BGl_rtl_jumpexitz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1353z00_6300),
									BgL_arg2512z00_6301);
							}
							BgL_new1354z00_6299 = BgL_new1353z00_6300;
						}
						((((BgL_rtl_funz00_bglt) COBJECT(
										((BgL_rtl_funz00_bglt) BgL_new1354z00_6299)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						BgL_arg2506z00_6296 = BgL_new1354z00_6299;
					}
					BgL_arg2508z00_6297 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_ez00_5975)))->BgL_exitz00);
					BgL_arg2509z00_6298 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_ez00_5975)))->BgL_valuez00);
					{	/* SawMill/node2rtl.scm 459 */
						obj_t BgL_list2510z00_6302;

						{	/* SawMill/node2rtl.scm 459 */
							obj_t BgL_arg2511z00_6303;

							BgL_arg2511z00_6303 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg2509z00_6298), BNIL);
							BgL_list2510z00_6302 =
								MAKE_YOUNG_PAIR(
								((obj_t) BgL_arg2508z00_6297), BgL_arg2511z00_6303);
						}
						BgL_arg2505z00_6295 =
							BGl_callza2za2zzsaw_node2rtlz00(
							((obj_t)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_ez00_5975)),
							((BgL_rtl_funz00_bglt) BgL_arg2506z00_6296),
							BgL_list2510z00_6302);
				}}
				return BGl_unlinkz00zzsaw_node2rtlz00(BgL_arg2505z00_6295);
			}
		}

	}



/* &node->rtl-set-ex-it1765 */
	BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2setzd2exzd2it1765z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_5976, obj_t BgL_ez00_5977)
	{
		{	/* SawMill/node2rtl.scm 449 */
			{	/* SawMill/node2rtl.scm 451 */
				BgL_areaz00_bglt BgL_arg2488z00_6305;
				BgL_areaz00_bglt BgL_arg2490z00_6306;

				{	/* SawMill/node2rtl.scm 451 */
					BgL_areaz00_bglt BgL_az00_6307;

					{	/* SawMill/node2rtl.scm 451 */
						BgL_rtl_protectz00_bglt BgL_arg2495z00_6308;

						{	/* SawMill/node2rtl.scm 451 */
							BgL_rtl_protectz00_bglt BgL_new1349z00_6309;

							{	/* SawMill/node2rtl.scm 451 */
								BgL_rtl_protectz00_bglt BgL_new1348z00_6310;

								BgL_new1348z00_6310 =
									((BgL_rtl_protectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_rtl_protectz00_bgl))));
								{	/* SawMill/node2rtl.scm 451 */
									long BgL_arg2497z00_6311;

									BgL_arg2497z00_6311 =
										BGL_CLASS_NUM(BGl_rtl_protectz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1348z00_6310),
										BgL_arg2497z00_6311);
								}
								BgL_new1349z00_6309 = BgL_new1348z00_6310;
							}
							((((BgL_rtl_funz00_bglt) COBJECT(
											((BgL_rtl_funz00_bglt) BgL_new1349z00_6309)))->
									BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
							BgL_arg2495z00_6308 = BgL_new1349z00_6309;
						}
						BgL_az00_6307 =
							BGl_singleza2za2zzsaw_node2rtlz00(
							((obj_t)
								((BgL_setzd2exzd2itz00_bglt) BgL_ez00_5977)),
							((BgL_rtl_funz00_bglt) BgL_arg2495z00_6308), BNIL);
					}
					{	/* SawMill/node2rtl.scm 452 */
						BgL_rtl_regz00_bglt BgL_arg2491z00_6312;

						{	/* SawMill/node2rtl.scm 452 */
							BgL_variablez00_bglt BgL_arg2492z00_6313;

							BgL_arg2492z00_6313 =
								(((BgL_varz00_bglt) COBJECT(
										(((BgL_setzd2exzd2itz00_bglt) COBJECT(
													((BgL_setzd2exzd2itz00_bglt) BgL_ez00_5977)))->
											BgL_varz00)))->BgL_variablez00);
							BgL_arg2491z00_6312 =
								BGl_localzd2ze3regz31zzsaw_node2rtlz00(((BgL_localz00_bglt)
									BgL_arg2492z00_6313));
						}
						BGl_bdestinationz12z12zzsaw_node2rtlz00(
							(((BgL_areaz00_bglt) COBJECT(BgL_az00_6307))->BgL_exitz00),
							BgL_arg2491z00_6312);
					}
					BgL_arg2488z00_6305 = BgL_az00_6307;
				}
				{	/* SawMill/node2rtl.scm 454 */
					BgL_rtl_protectedz00_bglt BgL_arg2500z00_6314;
					BgL_nodez00_bglt BgL_arg2501z00_6315;

					{	/* SawMill/node2rtl.scm 454 */
						BgL_rtl_protectedz00_bglt BgL_new1351z00_6316;

						{	/* SawMill/node2rtl.scm 454 */
							BgL_rtl_protectedz00_bglt BgL_new1350z00_6317;

							BgL_new1350z00_6317 =
								((BgL_rtl_protectedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_protectedz00_bgl))));
							{	/* SawMill/node2rtl.scm 454 */
								long BgL_arg2503z00_6318;

								BgL_arg2503z00_6318 =
									BGL_CLASS_NUM(BGl_rtl_protectedz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1350z00_6317),
									BgL_arg2503z00_6318);
							}
							BgL_new1351z00_6316 = BgL_new1350z00_6317;
						}
						((((BgL_rtl_funz00_bglt) COBJECT(
										((BgL_rtl_funz00_bglt) BgL_new1351z00_6316)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						BgL_arg2500z00_6314 = BgL_new1351z00_6316;
					}
					BgL_arg2501z00_6315 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_ez00_5977)))->BgL_bodyz00);
					{	/* SawMill/node2rtl.scm 454 */
						obj_t BgL_list2502z00_6319;

						BgL_list2502z00_6319 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_arg2501z00_6315), BNIL);
						BgL_arg2490z00_6306 =
							BGl_callza2za2zzsaw_node2rtlz00(
							((obj_t)
								((BgL_setzd2exzd2itz00_bglt) BgL_ez00_5977)),
							((BgL_rtl_funz00_bglt) BgL_arg2500z00_6314),
							BgL_list2502z00_6319);
				}}
				return
					BGl_linkz00zzsaw_node2rtlz00(BgL_arg2488z00_6305,
					BgL_arg2490z00_6306);
			}
		}

	}



/* &node->rtl-cast-null1763 */
	BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2castzd2null1763z53zzsaw_node2rtlz00(obj_t
		BgL_envz00_5978, obj_t BgL_ez00_5979)
	{
		{	/* SawMill/node2rtl.scm 444 */
			{	/* SawMill/node2rtl.scm 446 */
				BgL_rtl_cast_nullz00_bglt BgL_arg2486z00_6321;

				{	/* SawMill/node2rtl.scm 446 */
					BgL_rtl_cast_nullz00_bglt BgL_new1346z00_6322;

					{	/* SawMill/node2rtl.scm 446 */
						BgL_rtl_cast_nullz00_bglt BgL_new1345z00_6323;

						BgL_new1345z00_6323 =
							((BgL_rtl_cast_nullz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_cast_nullz00_bgl))));
						{	/* SawMill/node2rtl.scm 446 */
							long BgL_arg2487z00_6324;

							BgL_arg2487z00_6324 =
								BGL_CLASS_NUM(BGl_rtl_cast_nullz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1345z00_6323),
								BgL_arg2487z00_6324);
						}
						BgL_new1346z00_6322 = BgL_new1345z00_6323;
					}
					((((BgL_rtl_funz00_bglt) COBJECT(
									((BgL_rtl_funz00_bglt) BgL_new1346z00_6322)))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_rtl_cast_nullz00_bglt) COBJECT(BgL_new1346z00_6322))->
							BgL_typez00) =
						((BgL_typez00_bglt) (((BgL_nodez00_bglt)
									COBJECT(((BgL_nodez00_bglt) ((BgL_castzd2nullzd2_bglt)
												BgL_ez00_5979))))->BgL_typez00)), BUNSPEC);
					BgL_arg2486z00_6321 = BgL_new1346z00_6322;
				}
				return
					BGl_callza2za2zzsaw_node2rtlz00(
					((obj_t)
						((BgL_castzd2nullzd2_bglt) BgL_ez00_5979)),
					((BgL_rtl_funz00_bglt) BgL_arg2486z00_6321), BNIL);
			}
		}

	}



/* &node->rtl-cast1761 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2cast1761z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_5980, obj_t BgL_ez00_5981)
	{
		{	/* SawMill/node2rtl.scm 437 */
			{	/* SawMill/node2rtl.scm 439 */
				bool_t BgL_test3140z00_8829;

				{	/* SawMill/node2rtl.scm 439 */
					obj_t BgL_arg2484z00_6326;

					BgL_arg2484z00_6326 = BGl_thezd2backendzd2zzbackend_backendz00();
					BgL_test3140z00_8829 =
						(((BgL_backendz00_bglt) COBJECT(
								((BgL_backendz00_bglt) BgL_arg2484z00_6326)))->
						BgL_strictzd2typezd2castz00);
				}
				if (BgL_test3140z00_8829)
					{	/* SawMill/node2rtl.scm 440 */
						BgL_rtl_castz00_bglt BgL_arg2476z00_6327;
						BgL_nodez00_bglt BgL_arg2479z00_6328;

						{	/* SawMill/node2rtl.scm 440 */
							BgL_rtl_castz00_bglt BgL_new1343z00_6329;

							{	/* SawMill/node2rtl.scm 440 */
								BgL_rtl_castz00_bglt BgL_new1342z00_6330;

								BgL_new1342z00_6330 =
									((BgL_rtl_castz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_rtl_castz00_bgl))));
								{	/* SawMill/node2rtl.scm 440 */
									long BgL_arg2482z00_6331;

									BgL_arg2482z00_6331 =
										BGL_CLASS_NUM(BGl_rtl_castz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1342z00_6330),
										BgL_arg2482z00_6331);
								}
								BgL_new1343z00_6329 = BgL_new1342z00_6330;
							}
							((((BgL_rtl_funz00_bglt) COBJECT(
											((BgL_rtl_funz00_bglt) BgL_new1343z00_6329)))->
									BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
							((((BgL_rtl_castz00_bglt) COBJECT(BgL_new1343z00_6329))->
									BgL_totypez00) =
								((BgL_typez00_bglt) (((BgL_nodez00_bglt)
											COBJECT(((BgL_nodez00_bglt) ((BgL_castz00_bglt)
														BgL_ez00_5981))))->BgL_typez00)), BUNSPEC);
							{
								BgL_typez00_bglt BgL_auxz00_8843;

								{	/* SawMill/node2rtl.scm 440 */
									BgL_nodez00_bglt BgL_arg2481z00_6332;

									BgL_arg2481z00_6332 =
										(((BgL_castz00_bglt) COBJECT(
												((BgL_castz00_bglt) BgL_ez00_5981)))->BgL_argz00);
									BgL_auxz00_8843 =
										BGl_getzd2typezd2zztype_typeofz00(BgL_arg2481z00_6332,
										((bool_t) 0));
								}
								((((BgL_rtl_castz00_bglt) COBJECT(BgL_new1343z00_6329))->
										BgL_fromtypez00) =
									((BgL_typez00_bglt) BgL_auxz00_8843), BUNSPEC);
							}
							BgL_arg2476z00_6327 = BgL_new1343z00_6329;
						}
						BgL_arg2479z00_6328 =
							(((BgL_castz00_bglt) COBJECT(
									((BgL_castz00_bglt) BgL_ez00_5981)))->BgL_argz00);
						{	/* SawMill/node2rtl.scm 440 */
							obj_t BgL_list2480z00_6333;

							BgL_list2480z00_6333 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg2479z00_6328), BNIL);
							return
								BGl_callza2za2zzsaw_node2rtlz00(
								((obj_t)
									((BgL_castz00_bglt) BgL_ez00_5981)),
								((BgL_rtl_funz00_bglt) BgL_arg2476z00_6327),
								BgL_list2480z00_6333);
						}
					}
				else
					{	/* SawMill/node2rtl.scm 439 */
						return
							BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(
							(((BgL_castz00_bglt) COBJECT(
										((BgL_castz00_bglt) BgL_ez00_5981)))->BgL_argz00));
					}
			}
		}

	}



/* &node->rtl-instanceof1759 */
	BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2instanceof1759z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_5982, obj_t BgL_ez00_5983)
	{
		{	/* SawMill/node2rtl.scm 432 */
			{	/* SawMill/node2rtl.scm 434 */
				BgL_rtl_instanceofz00_bglt BgL_arg2470z00_6335;
				obj_t BgL_arg2471z00_6336;

				{	/* SawMill/node2rtl.scm 434 */
					BgL_rtl_instanceofz00_bglt BgL_new1340z00_6337;

					{	/* SawMill/node2rtl.scm 434 */
						BgL_rtl_instanceofz00_bglt BgL_new1339z00_6338;

						BgL_new1339z00_6338 =
							((BgL_rtl_instanceofz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_instanceofz00_bgl))));
						{	/* SawMill/node2rtl.scm 434 */
							long BgL_arg2473z00_6339;

							BgL_arg2473z00_6339 =
								BGL_CLASS_NUM(BGl_rtl_instanceofz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1339z00_6338),
								BgL_arg2473z00_6339);
						}
						BgL_new1340z00_6337 = BgL_new1339z00_6338;
					}
					((((BgL_rtl_funz00_bglt) COBJECT(
									((BgL_rtl_funz00_bglt) BgL_new1340z00_6337)))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_rtl_instanceofz00_bglt) COBJECT(BgL_new1340z00_6337))->
							BgL_typez00) =
						((BgL_typez00_bglt) (((BgL_instanceofz00_bglt)
									COBJECT(((BgL_instanceofz00_bglt) BgL_ez00_5983)))->
								BgL_classz00)), BUNSPEC);
					BgL_arg2470z00_6335 = BgL_new1340z00_6337;
				}
				BgL_arg2471z00_6336 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_instanceofz00_bglt) BgL_ez00_5983))))->BgL_exprza2za2);
				return
					BGl_callza2za2zzsaw_node2rtlz00(
					((obj_t)
						((BgL_instanceofz00_bglt) BgL_ez00_5983)),
					((BgL_rtl_funz00_bglt) BgL_arg2470z00_6335), BgL_arg2471z00_6336);
			}
		}

	}



/* &node->rtl-vlength1757 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2vlength1757z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_5984, obj_t BgL_ez00_5985)
	{
		{	/* SawMill/node2rtl.scm 427 */
			{	/* SawMill/node2rtl.scm 429 */
				BgL_rtl_vlengthz00_bglt BgL_arg2465z00_6341;
				obj_t BgL_arg2466z00_6342;

				{	/* SawMill/node2rtl.scm 429 */
					BgL_rtl_vlengthz00_bglt BgL_new1337z00_6343;

					{	/* SawMill/node2rtl.scm 429 */
						BgL_rtl_vlengthz00_bglt BgL_new1336z00_6344;

						BgL_new1336z00_6344 =
							((BgL_rtl_vlengthz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_vlengthz00_bgl))));
						{	/* SawMill/node2rtl.scm 429 */
							long BgL_arg2469z00_6345;

							BgL_arg2469z00_6345 =
								BGL_CLASS_NUM(BGl_rtl_vlengthz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1336z00_6344),
								BgL_arg2469z00_6345);
						}
						BgL_new1337z00_6343 = BgL_new1336z00_6344;
					}
					((((BgL_rtl_funz00_bglt) COBJECT(
									((BgL_rtl_funz00_bglt) BgL_new1337z00_6343)))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_rtl_vlengthz00_bglt) COBJECT(BgL_new1337z00_6343))->
							BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) (((BgL_vlengthz00_bglt)
										COBJECT(((BgL_vlengthz00_bglt) BgL_ez00_5985)))->
									BgL_ftypez00))), BUNSPEC);
					((((BgL_rtl_vlengthz00_bglt) COBJECT(BgL_new1337z00_6343))->
							BgL_vtypez00) =
						((BgL_typez00_bglt) (((BgL_vlengthz00_bglt)
									COBJECT(((BgL_vlengthz00_bglt) BgL_ez00_5985)))->
								BgL_vtypez00)), BUNSPEC);
					BgL_arg2465z00_6341 = BgL_new1337z00_6343;
				}
				BgL_arg2466z00_6342 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vlengthz00_bglt) BgL_ez00_5985))))->BgL_exprza2za2);
				return
					BGl_callza2za2zzsaw_node2rtlz00(
					((obj_t)
						((BgL_vlengthz00_bglt) BgL_ez00_5985)),
					((BgL_rtl_funz00_bglt) BgL_arg2465z00_6341), BgL_arg2466z00_6342);
			}
		}

	}



/* &node->rtl-vset!1755 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2vsetz121755z93zzsaw_node2rtlz00(obj_t
		BgL_envz00_5986, obj_t BgL_ez00_5987)
	{
		{	/* SawMill/node2rtl.scm 421 */
			{	/* SawMill/node2rtl.scm 423 */
				BgL_areaz00_bglt BgL_arg2456z00_6347;
				BgL_areaz00_bglt BgL_arg2457z00_6348;

				{	/* SawMill/node2rtl.scm 423 */
					BgL_rtl_vsetz00_bglt BgL_arg2458z00_6349;
					obj_t BgL_arg2459z00_6350;

					{	/* SawMill/node2rtl.scm 423 */
						BgL_rtl_vsetz00_bglt BgL_new1332z00_6351;

						{	/* SawMill/node2rtl.scm 423 */
							BgL_rtl_vsetz00_bglt BgL_new1331z00_6352;

							BgL_new1331z00_6352 =
								((BgL_rtl_vsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_vsetz00_bgl))));
							{	/* SawMill/node2rtl.scm 423 */
								long BgL_arg2460z00_6353;

								BgL_arg2460z00_6353 =
									BGL_CLASS_NUM(BGl_rtl_vsetz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1331z00_6352),
									BgL_arg2460z00_6353);
							}
							BgL_new1332z00_6351 = BgL_new1331z00_6352;
						}
						((((BgL_rtl_funz00_bglt) COBJECT(
										((BgL_rtl_funz00_bglt) BgL_new1332z00_6351)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						((((BgL_rtl_vsetz00_bglt) COBJECT(BgL_new1332z00_6351))->
								BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_vsetz12z12_bglt)
										COBJECT(((BgL_vsetz12z12_bglt) BgL_ez00_5987)))->
									BgL_ftypez00)), BUNSPEC);
						((((BgL_rtl_vsetz00_bglt) COBJECT(BgL_new1332z00_6351))->
								BgL_vtypez00) =
							((BgL_typez00_bglt) (((BgL_vsetz12z12_bglt)
										COBJECT(((BgL_vsetz12z12_bglt) BgL_ez00_5987)))->
									BgL_vtypez00)), BUNSPEC);
						BgL_arg2458z00_6349 = BgL_new1332z00_6351;
					}
					BgL_arg2459z00_6350 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vsetz12z12_bglt) BgL_ez00_5987))))->BgL_exprza2za2);
					BgL_arg2456z00_6347 =
						BGl_callza2za2zzsaw_node2rtlz00(
						((obj_t)
							((BgL_vsetz12z12_bglt) BgL_ez00_5987)),
						((BgL_rtl_funz00_bglt) BgL_arg2458z00_6349), BgL_arg2459z00_6350);
				}
				{	/* SawMill/node2rtl.scm 424 */
					BgL_rtl_nopz00_bglt BgL_arg2461z00_6354;

					{	/* SawMill/node2rtl.scm 424 */
						BgL_rtl_nopz00_bglt BgL_new1334z00_6355;

						{	/* SawMill/node2rtl.scm 424 */
							BgL_rtl_nopz00_bglt BgL_new1333z00_6356;

							BgL_new1333z00_6356 =
								((BgL_rtl_nopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_nopz00_bgl))));
							{	/* SawMill/node2rtl.scm 424 */
								long BgL_arg2463z00_6357;

								BgL_arg2463z00_6357 =
									BGL_CLASS_NUM(BGl_rtl_nopz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1333z00_6356),
									BgL_arg2463z00_6357);
							}
							BgL_new1334z00_6355 = BgL_new1333z00_6356;
						}
						((((BgL_rtl_funz00_bglt) COBJECT(
										((BgL_rtl_funz00_bglt) BgL_new1334z00_6355)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						BgL_arg2461z00_6354 = BgL_new1334z00_6355;
					}
					BgL_arg2457z00_6348 =
						BGl_singleza2za2zzsaw_node2rtlz00(BFALSE,
						((BgL_rtl_funz00_bglt) BgL_arg2461z00_6354), BNIL);
				}
				return
					BGl_linkz00zzsaw_node2rtlz00(BgL_arg2456z00_6347,
					BgL_arg2457z00_6348);
			}
		}

	}



/* &node->rtl-vref1753 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2vref1753z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_5988, obj_t BgL_ez00_5989)
	{
		{	/* SawMill/node2rtl.scm 416 */
			{	/* SawMill/node2rtl.scm 418 */
				BgL_rtl_vrefz00_bglt BgL_arg2452z00_6359;
				obj_t BgL_arg2453z00_6360;

				{	/* SawMill/node2rtl.scm 418 */
					BgL_rtl_vrefz00_bglt BgL_new1329z00_6361;

					{	/* SawMill/node2rtl.scm 418 */
						BgL_rtl_vrefz00_bglt BgL_new1328z00_6362;

						BgL_new1328z00_6362 =
							((BgL_rtl_vrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_vrefz00_bgl))));
						{	/* SawMill/node2rtl.scm 418 */
							long BgL_arg2455z00_6363;

							BgL_arg2455z00_6363 = BGL_CLASS_NUM(BGl_rtl_vrefz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1328z00_6362),
								BgL_arg2455z00_6363);
						}
						BgL_new1329z00_6361 = BgL_new1328z00_6362;
					}
					((((BgL_rtl_funz00_bglt) COBJECT(
									((BgL_rtl_funz00_bglt) BgL_new1329z00_6361)))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_rtl_vrefz00_bglt) COBJECT(BgL_new1329z00_6361))->
							BgL_typez00) =
						((BgL_typez00_bglt) (((BgL_vrefz00_bglt) COBJECT(((BgL_vrefz00_bglt)
											BgL_ez00_5989)))->BgL_ftypez00)), BUNSPEC);
					((((BgL_rtl_vrefz00_bglt) COBJECT(BgL_new1329z00_6361))->
							BgL_vtypez00) =
						((BgL_typez00_bglt) (((BgL_vrefz00_bglt) COBJECT(((BgL_vrefz00_bglt)
											BgL_ez00_5989)))->BgL_vtypez00)), BUNSPEC);
					BgL_arg2452z00_6359 = BgL_new1329z00_6361;
				}
				BgL_arg2453z00_6360 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vrefz00_bglt) BgL_ez00_5989))))->BgL_exprza2za2);
				return
					BGl_callza2za2zzsaw_node2rtlz00(
					((obj_t)
						((BgL_vrefz00_bglt) BgL_ez00_5989)),
					((BgL_rtl_funz00_bglt) BgL_arg2452z00_6359), BgL_arg2453z00_6360);
			}
		}

	}



/* &node->rtl-valloc1751 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2valloc1751z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_5990, obj_t BgL_ez00_5991)
	{
		{	/* SawMill/node2rtl.scm 410 */
			{	/* SawMill/node2rtl.scm 412 */
				BgL_rtl_vallocz00_bglt BgL_arg2449z00_6365;
				obj_t BgL_arg2450z00_6366;

				{	/* SawMill/node2rtl.scm 412 */
					BgL_rtl_vallocz00_bglt BgL_new1326z00_6367;

					{	/* SawMill/node2rtl.scm 412 */
						BgL_rtl_vallocz00_bglt BgL_new1325z00_6368;

						BgL_new1325z00_6368 =
							((BgL_rtl_vallocz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_vallocz00_bgl))));
						{	/* SawMill/node2rtl.scm 412 */
							long BgL_arg2451z00_6369;

							BgL_arg2451z00_6369 =
								BGL_CLASS_NUM(BGl_rtl_vallocz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1325z00_6368),
								BgL_arg2451z00_6369);
						}
						BgL_new1326z00_6367 = BgL_new1325z00_6368;
					}
					((((BgL_rtl_funz00_bglt) COBJECT(
									((BgL_rtl_funz00_bglt) BgL_new1326z00_6367)))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_rtl_vallocz00_bglt) COBJECT(BgL_new1326z00_6367))->
							BgL_typez00) =
						((BgL_typez00_bglt) (((BgL_vallocz00_bglt)
									COBJECT(((BgL_vallocz00_bglt) BgL_ez00_5991)))->
								BgL_ftypez00)), BUNSPEC);
					((((BgL_rtl_vallocz00_bglt) COBJECT(BgL_new1326z00_6367))->
							BgL_vtypez00) =
						((BgL_typez00_bglt)
							BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt) (
										(BgL_vallocz00_bglt) BgL_ez00_5991)), ((bool_t) 0))),
						BUNSPEC);
					BgL_arg2449z00_6365 = BgL_new1326z00_6367;
				}
				BgL_arg2450z00_6366 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vallocz00_bglt) BgL_ez00_5991))))->BgL_exprza2za2);
				return
					BGl_callza2za2zzsaw_node2rtlz00(
					((obj_t)
						((BgL_vallocz00_bglt) BgL_ez00_5991)),
					((BgL_rtl_funz00_bglt) BgL_arg2449z00_6365), BgL_arg2450z00_6366);
			}
		}

	}



/* &node->rtl-new1749 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2new1749z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_5992, obj_t BgL_ez00_5993)
	{
		{	/* SawMill/node2rtl.scm 404 */
			{	/* SawMill/node2rtl.scm 406 */
				BgL_rtl_newz00_bglt BgL_arg2445z00_6371;
				obj_t BgL_arg2446z00_6372;

				{	/* SawMill/node2rtl.scm 406 */
					BgL_rtl_newz00_bglt BgL_new1323z00_6373;

					{	/* SawMill/node2rtl.scm 406 */
						BgL_rtl_newz00_bglt BgL_new1322z00_6374;

						BgL_new1322z00_6374 =
							((BgL_rtl_newz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_newz00_bgl))));
						{	/* SawMill/node2rtl.scm 406 */
							long BgL_arg2447z00_6375;

							BgL_arg2447z00_6375 = BGL_CLASS_NUM(BGl_rtl_newz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1322z00_6374),
								BgL_arg2447z00_6375);
						}
						BgL_new1323z00_6373 = BgL_new1322z00_6374;
					}
					((((BgL_rtl_funz00_bglt) COBJECT(
									((BgL_rtl_funz00_bglt) BgL_new1323z00_6373)))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_rtl_newz00_bglt) COBJECT(BgL_new1323z00_6373))->BgL_typez00) =
						((BgL_typez00_bglt)
							BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt) (
										(BgL_newz00_bglt) BgL_ez00_5993)), ((bool_t) 0))), BUNSPEC);
					((((BgL_rtl_newz00_bglt) COBJECT(BgL_new1323z00_6373))->
							BgL_constrz00) =
						((obj_t) (((BgL_newz00_bglt) COBJECT(((BgL_newz00_bglt)
											BgL_ez00_5993)))->BgL_argszd2typezd2)), BUNSPEC);
					BgL_arg2445z00_6371 = BgL_new1323z00_6373;
				}
				BgL_arg2446z00_6372 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_newz00_bglt) BgL_ez00_5993))))->BgL_exprza2za2);
				return
					BGl_callza2za2zzsaw_node2rtlz00(
					((obj_t)
						((BgL_newz00_bglt) BgL_ez00_5993)),
					((BgL_rtl_funz00_bglt) BgL_arg2445z00_6371), BgL_arg2446z00_6372);
			}
		}

	}



/* &node->rtl-setfield1747 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2setfield1747z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_5994, obj_t BgL_ez00_5995)
	{
		{	/* SawMill/node2rtl.scm 395 */
			{	/* SawMill/node2rtl.scm 397 */
				BgL_areaz00_bglt BgL_arg2434z00_6377;
				BgL_areaz00_bglt BgL_arg2435z00_6378;

				{	/* SawMill/node2rtl.scm 397 */
					BgL_rtl_setfieldz00_bglt BgL_arg2437z00_6379;
					obj_t BgL_arg2438z00_6380;

					{	/* SawMill/node2rtl.scm 397 */
						BgL_rtl_setfieldz00_bglt BgL_new1317z00_6381;

						{	/* SawMill/node2rtl.scm 397 */
							BgL_rtl_setfieldz00_bglt BgL_new1316z00_6382;

							BgL_new1316z00_6382 =
								((BgL_rtl_setfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_setfieldz00_bgl))));
							{	/* SawMill/node2rtl.scm 397 */
								long BgL_arg2439z00_6383;

								BgL_arg2439z00_6383 =
									BGL_CLASS_NUM(BGl_rtl_setfieldz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1316z00_6382),
									BgL_arg2439z00_6383);
							}
							BgL_new1317z00_6381 = BgL_new1316z00_6382;
						}
						((((BgL_rtl_funz00_bglt) COBJECT(
										((BgL_rtl_funz00_bglt) BgL_new1317z00_6381)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						((((BgL_rtl_setfieldz00_bglt) COBJECT(BgL_new1317z00_6381))->
								BgL_namez00) =
							((obj_t) (((BgL_setfieldz00_bglt) COBJECT(((BgL_setfieldz00_bglt)
												BgL_ez00_5995)))->BgL_fnamez00)), BUNSPEC);
						((((BgL_rtl_setfieldz00_bglt) COBJECT(BgL_new1317z00_6381))->
								BgL_objtypez00) =
							((BgL_typez00_bglt) (((BgL_setfieldz00_bglt)
										COBJECT(((BgL_setfieldz00_bglt) BgL_ez00_5995)))->
									BgL_otypez00)), BUNSPEC);
						((((BgL_rtl_setfieldz00_bglt) COBJECT(BgL_new1317z00_6381))->
								BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_setfieldz00_bglt)
										COBJECT(((BgL_setfieldz00_bglt) BgL_ez00_5995)))->
									BgL_ftypez00)), BUNSPEC);
						BgL_arg2437z00_6379 = BgL_new1317z00_6381;
					}
					BgL_arg2438z00_6380 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_setfieldz00_bglt) BgL_ez00_5995))))->BgL_exprza2za2);
					BgL_arg2434z00_6377 =
						BGl_callza2za2zzsaw_node2rtlz00(
						((obj_t)
							((BgL_setfieldz00_bglt) BgL_ez00_5995)),
						((BgL_rtl_funz00_bglt) BgL_arg2437z00_6379), BgL_arg2438z00_6380);
				}
				{	/* SawMill/node2rtl.scm 401 */
					BgL_rtl_nopz00_bglt BgL_arg2442z00_6384;

					{	/* SawMill/node2rtl.scm 401 */
						BgL_rtl_nopz00_bglt BgL_new1320z00_6385;

						{	/* SawMill/node2rtl.scm 401 */
							BgL_rtl_nopz00_bglt BgL_new1319z00_6386;

							BgL_new1319z00_6386 =
								((BgL_rtl_nopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_nopz00_bgl))));
							{	/* SawMill/node2rtl.scm 401 */
								long BgL_arg2444z00_6387;

								BgL_arg2444z00_6387 =
									BGL_CLASS_NUM(BGl_rtl_nopz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1319z00_6386),
									BgL_arg2444z00_6387);
							}
							BgL_new1320z00_6385 = BgL_new1319z00_6386;
						}
						((((BgL_rtl_funz00_bglt) COBJECT(
										((BgL_rtl_funz00_bglt) BgL_new1320z00_6385)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						BgL_arg2442z00_6384 = BgL_new1320z00_6385;
					}
					BgL_arg2435z00_6378 =
						BGl_singleza2za2zzsaw_node2rtlz00(BFALSE,
						((BgL_rtl_funz00_bglt) BgL_arg2442z00_6384), BNIL);
				}
				return
					BGl_linkz00zzsaw_node2rtlz00(BgL_arg2434z00_6377,
					BgL_arg2435z00_6378);
			}
		}

	}



/* &node->rtl-getfield1745 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2getfield1745z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_5996, obj_t BgL_ez00_5997)
	{
		{	/* SawMill/node2rtl.scm 387 */
			{	/* SawMill/node2rtl.scm 389 */
				BgL_rtl_getfieldz00_bglt BgL_arg2430z00_6389;
				obj_t BgL_arg2431z00_6390;

				{	/* SawMill/node2rtl.scm 389 */
					BgL_rtl_getfieldz00_bglt BgL_new1314z00_6391;

					{	/* SawMill/node2rtl.scm 389 */
						BgL_rtl_getfieldz00_bglt BgL_new1313z00_6392;

						BgL_new1313z00_6392 =
							((BgL_rtl_getfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_getfieldz00_bgl))));
						{	/* SawMill/node2rtl.scm 389 */
							long BgL_arg2432z00_6393;

							BgL_arg2432z00_6393 =
								BGL_CLASS_NUM(BGl_rtl_getfieldz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1313z00_6392),
								BgL_arg2432z00_6393);
						}
						BgL_new1314z00_6391 = BgL_new1313z00_6392;
					}
					((((BgL_rtl_funz00_bglt) COBJECT(
									((BgL_rtl_funz00_bglt) BgL_new1314z00_6391)))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_rtl_getfieldz00_bglt) COBJECT(BgL_new1314z00_6391))->
							BgL_namez00) =
						((obj_t) (((BgL_getfieldz00_bglt) COBJECT(((BgL_getfieldz00_bglt)
											BgL_ez00_5997)))->BgL_fnamez00)), BUNSPEC);
					((((BgL_rtl_getfieldz00_bglt) COBJECT(BgL_new1314z00_6391))->
							BgL_objtypez00) =
						((BgL_typez00_bglt) (((BgL_getfieldz00_bglt)
									COBJECT(((BgL_getfieldz00_bglt) BgL_ez00_5997)))->
								BgL_otypez00)), BUNSPEC);
					((((BgL_rtl_getfieldz00_bglt) COBJECT(BgL_new1314z00_6391))->
							BgL_typez00) =
						((BgL_typez00_bglt) (((BgL_getfieldz00_bglt)
									COBJECT(((BgL_getfieldz00_bglt) BgL_ez00_5997)))->
								BgL_ftypez00)), BUNSPEC);
					BgL_arg2430z00_6389 = BgL_new1314z00_6391;
				}
				BgL_arg2431z00_6390 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_getfieldz00_bglt) BgL_ez00_5997))))->BgL_exprza2za2);
				return
					BGl_callza2za2zzsaw_node2rtlz00(
					((obj_t)
						((BgL_getfieldz00_bglt) BgL_ez00_5997)),
					((BgL_rtl_funz00_bglt) BgL_arg2430z00_6389), BgL_arg2431z00_6390);
			}
		}

	}



/* &node->rtl-pragma1743 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2pragma1743z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_5998, obj_t BgL_ez00_5999)
	{
		{	/* SawMill/node2rtl.scm 376 */
			{	/* SawMill/node2rtl.scm 378 */
				bool_t BgL_test3141z00_9035;

				if (
					(STRING_LENGTH(
							(((BgL_pragmaz00_bglt) COBJECT(
										((BgL_pragmaz00_bglt) BgL_ez00_5999)))->BgL_formatz00)) ==
						0L))
					{	/* SawMill/node2rtl.scm 378 */
						if (NULLP(
								(((BgL_externz00_bglt) COBJECT(
											((BgL_externz00_bglt)
												((BgL_pragmaz00_bglt) BgL_ez00_5999))))->
									BgL_exprza2za2)))
							{	/* SawMill/node2rtl.scm 379 */
								BgL_test3141z00_9035 = ((bool_t) 0);
							}
						else
							{	/* SawMill/node2rtl.scm 380 */
								bool_t BgL_test3144z00_9046;

								{	/* SawMill/node2rtl.scm 380 */
									obj_t BgL_tmpz00_9047;

									{	/* SawMill/node2rtl.scm 380 */
										obj_t BgL_pairz00_6395;

										BgL_pairz00_6395 =
											(((BgL_externz00_bglt) COBJECT(
													((BgL_externz00_bglt)
														((BgL_pragmaz00_bglt) BgL_ez00_5999))))->
											BgL_exprza2za2);
										BgL_tmpz00_9047 = CDR(BgL_pairz00_6395);
									}
									BgL_test3144z00_9046 = NULLP(BgL_tmpz00_9047);
								}
								if (BgL_test3144z00_9046)
									{	/* SawMill/node2rtl.scm 381 */
										obj_t BgL_arg2423z00_6396;

										{	/* SawMill/node2rtl.scm 381 */
											obj_t BgL_pairz00_6397;

											BgL_pairz00_6397 =
												(((BgL_externz00_bglt) COBJECT(
														((BgL_externz00_bglt)
															((BgL_pragmaz00_bglt) BgL_ez00_5999))))->
												BgL_exprza2za2);
											BgL_arg2423z00_6396 = CAR(BgL_pairz00_6397);
										}
										{	/* SawMill/node2rtl.scm 381 */
											obj_t BgL_classz00_6398;

											BgL_classz00_6398 = BGl_varz00zzast_nodez00;
											if (BGL_OBJECTP(BgL_arg2423z00_6396))
												{	/* SawMill/node2rtl.scm 381 */
													BgL_objectz00_bglt BgL_arg1807z00_6399;

													BgL_arg1807z00_6399 =
														(BgL_objectz00_bglt) (BgL_arg2423z00_6396);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* SawMill/node2rtl.scm 381 */
															long BgL_idxz00_6400;

															BgL_idxz00_6400 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6399);
															BgL_test3141z00_9035 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_6400 + 2L)) == BgL_classz00_6398);
														}
													else
														{	/* SawMill/node2rtl.scm 381 */
															bool_t BgL_res2853z00_6403;

															{	/* SawMill/node2rtl.scm 381 */
																obj_t BgL_oclassz00_6404;

																{	/* SawMill/node2rtl.scm 381 */
																	obj_t BgL_arg1815z00_6405;
																	long BgL_arg1816z00_6406;

																	BgL_arg1815z00_6405 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* SawMill/node2rtl.scm 381 */
																		long BgL_arg1817z00_6407;

																		BgL_arg1817z00_6407 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6399);
																		BgL_arg1816z00_6406 =
																			(BgL_arg1817z00_6407 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_6404 =
																		VECTOR_REF(BgL_arg1815z00_6405,
																		BgL_arg1816z00_6406);
																}
																{	/* SawMill/node2rtl.scm 381 */
																	bool_t BgL__ortest_1115z00_6408;

																	BgL__ortest_1115z00_6408 =
																		(BgL_classz00_6398 == BgL_oclassz00_6404);
																	if (BgL__ortest_1115z00_6408)
																		{	/* SawMill/node2rtl.scm 381 */
																			BgL_res2853z00_6403 =
																				BgL__ortest_1115z00_6408;
																		}
																	else
																		{	/* SawMill/node2rtl.scm 381 */
																			long BgL_odepthz00_6409;

																			{	/* SawMill/node2rtl.scm 381 */
																				obj_t BgL_arg1804z00_6410;

																				BgL_arg1804z00_6410 =
																					(BgL_oclassz00_6404);
																				BgL_odepthz00_6409 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_6410);
																			}
																			if ((2L < BgL_odepthz00_6409))
																				{	/* SawMill/node2rtl.scm 381 */
																					obj_t BgL_arg1802z00_6411;

																					{	/* SawMill/node2rtl.scm 381 */
																						obj_t BgL_arg1803z00_6412;

																						BgL_arg1803z00_6412 =
																							(BgL_oclassz00_6404);
																						BgL_arg1802z00_6411 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_6412, 2L);
																					}
																					BgL_res2853z00_6403 =
																						(BgL_arg1802z00_6411 ==
																						BgL_classz00_6398);
																				}
																			else
																				{	/* SawMill/node2rtl.scm 381 */
																					BgL_res2853z00_6403 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test3141z00_9035 = BgL_res2853z00_6403;
														}
												}
											else
												{	/* SawMill/node2rtl.scm 381 */
													BgL_test3141z00_9035 = ((bool_t) 0);
												}
										}
									}
								else
									{	/* SawMill/node2rtl.scm 380 */
										BgL_test3141z00_9035 = ((bool_t) 0);
									}
							}
					}
				else
					{	/* SawMill/node2rtl.scm 378 */
						BgL_test3141z00_9035 = ((bool_t) 0);
					}
				if (BgL_test3141z00_9035)
					{	/* SawMill/node2rtl.scm 382 */
						obj_t BgL_fmtz00_6413;

						{
							BgL_variablez00_bglt BgL_auxz00_9079;

							{
								BgL_varz00_bglt BgL_auxz00_9080;

								{	/* SawMill/node2rtl.scm 382 */
									obj_t BgL_pairz00_6414;

									BgL_pairz00_6414 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt)
													((BgL_pragmaz00_bglt) BgL_ez00_5999))))->
										BgL_exprza2za2);
									BgL_auxz00_9080 = ((BgL_varz00_bglt) CAR(BgL_pairz00_6414));
								}
								BgL_auxz00_9079 =
									(((BgL_varz00_bglt) COBJECT(BgL_auxz00_9080))->
									BgL_variablez00);
							}
							BgL_fmtz00_6413 =
								(((BgL_variablez00_bglt) COBJECT(BgL_auxz00_9079))->
								BgL_namez00);
						}
						{	/* SawMill/node2rtl.scm 383 */
							BgL_rtl_pragmaz00_bglt BgL_arg2410z00_6415;
							obj_t BgL_arg2411z00_6416;

							{	/* SawMill/node2rtl.scm 383 */
								BgL_rtl_pragmaz00_bglt BgL_new1308z00_6417;

								{	/* SawMill/node2rtl.scm 383 */
									BgL_rtl_pragmaz00_bglt BgL_new1307z00_6418;

									BgL_new1307z00_6418 =
										((BgL_rtl_pragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_rtl_pragmaz00_bgl))));
									{	/* SawMill/node2rtl.scm 383 */
										long BgL_arg2412z00_6419;

										BgL_arg2412z00_6419 =
											BGL_CLASS_NUM(BGl_rtl_pragmaz00zzsaw_defsz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1307z00_6418),
											BgL_arg2412z00_6419);
									}
									BgL_new1308z00_6417 = BgL_new1307z00_6418;
								}
								((((BgL_rtl_funz00_bglt) COBJECT(
												((BgL_rtl_funz00_bglt) BgL_new1308z00_6417)))->
										BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_new1308z00_6417))->
										BgL_formatz00) = ((obj_t) BgL_fmtz00_6413), BUNSPEC);
								((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_new1308z00_6417))->
										BgL_srfi0z00) =
									((obj_t) (((BgL_pragmaz00_bglt) COBJECT(((BgL_pragmaz00_bglt)
														BgL_ez00_5999)))->BgL_srfi0z00)), BUNSPEC);
								BgL_arg2410z00_6415 = BgL_new1308z00_6417;
							}
							{	/* SawMill/node2rtl.scm 383 */
								obj_t BgL_pairz00_6420;

								BgL_pairz00_6420 =
									(((BgL_externz00_bglt) COBJECT(
											((BgL_externz00_bglt)
												((BgL_pragmaz00_bglt) BgL_ez00_5999))))->
									BgL_exprza2za2);
								BgL_arg2411z00_6416 = CDR(BgL_pairz00_6420);
							}
							return
								BGl_callza2za2zzsaw_node2rtlz00(
								((obj_t)
									((BgL_pragmaz00_bglt) BgL_ez00_5999)),
								((BgL_rtl_funz00_bglt) BgL_arg2410z00_6415),
								BgL_arg2411z00_6416);
						}
					}
				else
					{	/* SawMill/node2rtl.scm 384 */
						BgL_rtl_pragmaz00_bglt BgL_arg2419z00_6421;
						obj_t BgL_arg2420z00_6422;

						{	/* SawMill/node2rtl.scm 384 */
							BgL_rtl_pragmaz00_bglt BgL_new1310z00_6423;

							{	/* SawMill/node2rtl.scm 384 */
								BgL_rtl_pragmaz00_bglt BgL_new1309z00_6424;

								BgL_new1309z00_6424 =
									((BgL_rtl_pragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_rtl_pragmaz00_bgl))));
								{	/* SawMill/node2rtl.scm 384 */
									long BgL_arg2421z00_6425;

									BgL_arg2421z00_6425 =
										BGL_CLASS_NUM(BGl_rtl_pragmaz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1309z00_6424),
										BgL_arg2421z00_6425);
								}
								BgL_new1310z00_6423 = BgL_new1309z00_6424;
							}
							((((BgL_rtl_funz00_bglt) COBJECT(
											((BgL_rtl_funz00_bglt) BgL_new1310z00_6423)))->
									BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
							((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_new1310z00_6423))->
									BgL_formatz00) =
								((obj_t) (((BgL_pragmaz00_bglt) COBJECT(((BgL_pragmaz00_bglt)
													BgL_ez00_5999)))->BgL_formatz00)), BUNSPEC);
							((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_new1310z00_6423))->
									BgL_srfi0z00) =
								((obj_t) (((BgL_pragmaz00_bglt) COBJECT(((BgL_pragmaz00_bglt)
													BgL_ez00_5999)))->BgL_srfi0z00)), BUNSPEC);
							BgL_arg2419z00_6421 = BgL_new1310z00_6423;
						}
						BgL_arg2420z00_6422 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_pragmaz00_bglt) BgL_ez00_5999))))->BgL_exprza2za2);
						return
							BGl_callza2za2zzsaw_node2rtlz00(
							((obj_t)
								((BgL_pragmaz00_bglt) BgL_ez00_5999)),
							((BgL_rtl_funz00_bglt) BgL_arg2419z00_6421), BgL_arg2420z00_6422);
					}
			}
		}

	}



/* &node->rtl-funcall1741 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2funcall1741z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_6000, obj_t BgL_ez00_6001)
	{
		{	/* SawMill/node2rtl.scm 351 */
			{	/* SawMill/node2rtl.scm 358 */
				obj_t BgL_argsz00_6427;

				BgL_argsz00_6427 =
					BGl_argszd21ze70z35zzsaw_node2rtlz00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_ez00_6001)))->BgL_argsz00));
				{	/* SawMill/node2rtl.scm 359 */
					obj_t BgL_casezd2valuezd2_6428;

					BgL_casezd2valuezd2_6428 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_ez00_6001)))->BgL_strengthz00);
					if ((BgL_casezd2valuezd2_6428 == CNST_TABLE_REF(17)))
						{	/* SawMill/node2rtl.scm 362 */
							BgL_rtl_callz00_bglt BgL_arg2375z00_6429;

							{	/* SawMill/node2rtl.scm 362 */
								BgL_rtl_callz00_bglt BgL_new1300z00_6430;

								{	/* SawMill/node2rtl.scm 362 */
									BgL_rtl_callz00_bglt BgL_new1299z00_6431;

									BgL_new1299z00_6431 =
										((BgL_rtl_callz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_rtl_callz00_bgl))));
									{	/* SawMill/node2rtl.scm 362 */
										long BgL_arg2378z00_6432;

										BgL_arg2378z00_6432 =
											BGL_CLASS_NUM(BGl_rtl_callz00zzsaw_defsz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1299z00_6431),
											BgL_arg2378z00_6432);
									}
									BgL_new1300z00_6430 = BgL_new1299z00_6431;
								}
								((((BgL_rtl_funz00_bglt) COBJECT(
												((BgL_rtl_funz00_bglt) BgL_new1300z00_6430)))->
										BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
								{
									BgL_globalz00_bglt BgL_auxz00_9139;

									{	/* SawMill/node2rtl.scm 362 */
										BgL_variablez00_bglt BgL_arg2376z00_6433;

										BgL_arg2376z00_6433 =
											(((BgL_varz00_bglt) COBJECT(
													((BgL_varz00_bglt)
														(((BgL_funcallz00_bglt) COBJECT(
																	((BgL_funcallz00_bglt) BgL_ez00_6001)))->
															BgL_funz00))))->BgL_variablez00);
										BgL_auxz00_9139 =
											BGl_getzd2globalzd2zzsaw_node2rtlz00(((BgL_globalz00_bglt)
												BgL_arg2376z00_6433));
									}
									((((BgL_rtl_callz00_bglt) COBJECT(BgL_new1300z00_6430))->
											BgL_varz00) =
										((BgL_globalz00_bglt) BgL_auxz00_9139), BUNSPEC);
								}
								BgL_arg2375z00_6429 = BgL_new1300z00_6430;
							}
							return
								BGl_callza2za2zzsaw_node2rtlz00(
								((obj_t)
									((BgL_funcallz00_bglt) BgL_ez00_6001)),
								((BgL_rtl_funz00_bglt) BgL_arg2375z00_6429), BgL_argsz00_6427);
						}
					else
						{	/* SawMill/node2rtl.scm 359 */
							if ((BgL_casezd2valuezd2_6428 == CNST_TABLE_REF(18)))
								{	/* SawMill/node2rtl.scm 366 */
									BgL_rtl_lightfuncallz00_bglt BgL_arg2380z00_6434;

									{	/* SawMill/node2rtl.scm 366 */
										BgL_rtl_lightfuncallz00_bglt BgL_new1303z00_6435;

										{	/* SawMill/node2rtl.scm 366 */
											BgL_rtl_lightfuncallz00_bglt BgL_new1301z00_6436;

											BgL_new1301z00_6436 =
												((BgL_rtl_lightfuncallz00_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_rtl_lightfuncallz00_bgl))));
											{	/* SawMill/node2rtl.scm 366 */
												long BgL_arg2381z00_6437;

												BgL_arg2381z00_6437 =
													BGL_CLASS_NUM(BGl_rtl_lightfuncallz00zzsaw_defsz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1301z00_6436),
													BgL_arg2381z00_6437);
											}
											BgL_new1303z00_6435 = BgL_new1301z00_6436;
										}
										((((BgL_rtl_funz00_bglt) COBJECT(
														((BgL_rtl_funz00_bglt) BgL_new1303z00_6435)))->
												BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
										{
											obj_t BgL_auxz00_9160;

											{	/* SawMill/node2rtl.scm 368 */

												{	/* SawMill/node2rtl.scm 368 */

													BgL_auxz00_9160 =
														BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
											}}
											((((BgL_rtl_lightfuncallz00_bglt)
														COBJECT(BgL_new1303z00_6435))->BgL_namez00) =
												((obj_t) BgL_auxz00_9160), BUNSPEC);
										}
										((((BgL_rtl_lightfuncallz00_bglt)
													COBJECT(BgL_new1303z00_6435))->BgL_funsz00) =
											((obj_t) (((BgL_funcallz00_bglt)
														COBJECT(((BgL_funcallz00_bglt) BgL_ez00_6001)))->
													BgL_functionsz00)), BUNSPEC);
										((((BgL_rtl_lightfuncallz00_bglt)
													COBJECT(BgL_new1303z00_6435))->BgL_rettypez00) =
											((obj_t) ((obj_t)
													BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt)
															((BgL_funcallz00_bglt) BgL_ez00_6001)),
														((bool_t) 0)))), BUNSPEC);
										BgL_arg2380z00_6434 = BgL_new1303z00_6435;
									}
									return
										BGl_callza2za2zzsaw_node2rtlz00(
										((obj_t)
											((BgL_funcallz00_bglt) BgL_ez00_6001)),
										((BgL_rtl_funz00_bglt) BgL_arg2380z00_6434),
										BgL_argsz00_6427);
								}
							else
								{	/* SawMill/node2rtl.scm 373 */
									BgL_rtl_funcallz00_bglt BgL_arg2382z00_6438;

									{	/* SawMill/node2rtl.scm 373 */
										BgL_rtl_funcallz00_bglt BgL_new1305z00_6439;

										{	/* SawMill/node2rtl.scm 373 */
											BgL_rtl_funcallz00_bglt BgL_new1304z00_6440;

											BgL_new1304z00_6440 =
												((BgL_rtl_funcallz00_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_rtl_funcallz00_bgl))));
											{	/* SawMill/node2rtl.scm 373 */
												long BgL_arg2383z00_6441;

												BgL_arg2383z00_6441 =
													BGL_CLASS_NUM(BGl_rtl_funcallz00zzsaw_defsz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1304z00_6440),
													BgL_arg2383z00_6441);
											}
											BgL_new1305z00_6439 = BgL_new1304z00_6440;
										}
										((((BgL_rtl_funz00_bglt) COBJECT(
														((BgL_rtl_funz00_bglt) BgL_new1305z00_6439)))->
												BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
										BgL_arg2382z00_6438 = BgL_new1305z00_6439;
									}
									return
										BGl_callza2za2zzsaw_node2rtlz00(
										((obj_t)
											((BgL_funcallz00_bglt) BgL_ez00_6001)),
										((BgL_rtl_funz00_bglt) BgL_arg2382z00_6438),
										BgL_argsz00_6427);
								}
						}
				}
			}
		}

	}



/* args-1~0 */
	obj_t BGl_argszd21ze70z35zzsaw_node2rtlz00(obj_t BgL_argsz00_3338)
	{
		{	/* SawMill/node2rtl.scm 355 */
			if (NULLP(CDR(((obj_t) BgL_argsz00_3338))))
				{	/* SawMill/node2rtl.scm 353 */
					return BNIL;
				}
			else
				{	/* SawMill/node2rtl.scm 355 */
					obj_t BgL_arg2388z00_3342;
					obj_t BgL_arg2389z00_3343;

					BgL_arg2388z00_3342 = CAR(((obj_t) BgL_argsz00_3338));
					{	/* SawMill/node2rtl.scm 355 */
						obj_t BgL_arg2390z00_3344;

						BgL_arg2390z00_3344 = CDR(((obj_t) BgL_argsz00_3338));
						BgL_arg2389z00_3343 =
							BGl_argszd21ze70z35zzsaw_node2rtlz00(BgL_arg2390z00_3344);
					}
					return MAKE_YOUNG_PAIR(BgL_arg2388z00_3342, BgL_arg2389z00_3343);
				}
		}

	}



/* &node->rtl-app-ly1739 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2appzd2ly1739z53zzsaw_node2rtlz00(obj_t
		BgL_envz00_6002, obj_t BgL_ez00_6003)
	{
		{	/* SawMill/node2rtl.scm 346 */
			{	/* SawMill/node2rtl.scm 348 */
				BgL_rtl_applyz00_bglt BgL_arg2367z00_6443;
				BgL_nodez00_bglt BgL_arg2368z00_6444;
				BgL_nodez00_bglt BgL_arg2369z00_6445;

				{	/* SawMill/node2rtl.scm 348 */
					BgL_rtl_applyz00_bglt BgL_new1297z00_6446;

					{	/* SawMill/node2rtl.scm 348 */
						BgL_rtl_applyz00_bglt BgL_new1296z00_6447;

						BgL_new1296z00_6447 =
							((BgL_rtl_applyz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_applyz00_bgl))));
						{	/* SawMill/node2rtl.scm 348 */
							long BgL_arg2373z00_6448;

							BgL_arg2373z00_6448 =
								BGL_CLASS_NUM(BGl_rtl_applyz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1296z00_6447),
								BgL_arg2373z00_6448);
						}
						BgL_new1297z00_6446 = BgL_new1296z00_6447;
					}
					((((BgL_rtl_funz00_bglt) COBJECT(
									((BgL_rtl_funz00_bglt) BgL_new1297z00_6446)))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					BgL_arg2367z00_6443 = BgL_new1297z00_6446;
				}
				BgL_arg2368z00_6444 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_ez00_6003)))->BgL_funz00);
				BgL_arg2369z00_6445 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_ez00_6003)))->BgL_argz00);
				{	/* SawMill/node2rtl.scm 348 */
					obj_t BgL_list2370z00_6449;

					{	/* SawMill/node2rtl.scm 348 */
						obj_t BgL_arg2371z00_6450;

						BgL_arg2371z00_6450 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_arg2369z00_6445), BNIL);
						BgL_list2370z00_6449 =
							MAKE_YOUNG_PAIR(
							((obj_t) BgL_arg2368z00_6444), BgL_arg2371z00_6450);
					}
					return
						BGl_callza2za2zzsaw_node2rtlz00(
						((obj_t)
							((BgL_appzd2lyzd2_bglt) BgL_ez00_6003)),
						((BgL_rtl_funz00_bglt) BgL_arg2367z00_6443), BgL_list2370z00_6449);
				}
			}
		}

	}



/* &node->rtl-app1737 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2app1737z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_6004, obj_t BgL_ez00_6005)
	{
		{	/* SawMill/node2rtl.scm 323 */
			{	/* SawMill/node2rtl.scm 325 */
				BgL_variablez00_bglt BgL_vz00_6452;

				BgL_vz00_6452 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_ez00_6005)))->BgL_funz00)))->
					BgL_variablez00);
				{	/* SawMill/node2rtl.scm 326 */
					bool_t BgL_test3152z00_9216;

					{	/* SawMill/node2rtl.scm 326 */
						obj_t BgL_classz00_6453;

						BgL_classz00_6453 = BGl_localz00zzast_varz00;
						{	/* SawMill/node2rtl.scm 326 */
							BgL_objectz00_bglt BgL_arg1807z00_6454;

							{	/* SawMill/node2rtl.scm 326 */
								obj_t BgL_tmpz00_9217;

								BgL_tmpz00_9217 =
									((obj_t) ((BgL_objectz00_bglt) BgL_vz00_6452));
								BgL_arg1807z00_6454 = (BgL_objectz00_bglt) (BgL_tmpz00_9217);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawMill/node2rtl.scm 326 */
									long BgL_idxz00_6455;

									BgL_idxz00_6455 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6454);
									BgL_test3152z00_9216 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_6455 + 2L)) == BgL_classz00_6453);
								}
							else
								{	/* SawMill/node2rtl.scm 326 */
									bool_t BgL_res2852z00_6458;

									{	/* SawMill/node2rtl.scm 326 */
										obj_t BgL_oclassz00_6459;

										{	/* SawMill/node2rtl.scm 326 */
											obj_t BgL_arg1815z00_6460;
											long BgL_arg1816z00_6461;

											BgL_arg1815z00_6460 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawMill/node2rtl.scm 326 */
												long BgL_arg1817z00_6462;

												BgL_arg1817z00_6462 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6454);
												BgL_arg1816z00_6461 =
													(BgL_arg1817z00_6462 - OBJECT_TYPE);
											}
											BgL_oclassz00_6459 =
												VECTOR_REF(BgL_arg1815z00_6460, BgL_arg1816z00_6461);
										}
										{	/* SawMill/node2rtl.scm 326 */
											bool_t BgL__ortest_1115z00_6463;

											BgL__ortest_1115z00_6463 =
												(BgL_classz00_6453 == BgL_oclassz00_6459);
											if (BgL__ortest_1115z00_6463)
												{	/* SawMill/node2rtl.scm 326 */
													BgL_res2852z00_6458 = BgL__ortest_1115z00_6463;
												}
											else
												{	/* SawMill/node2rtl.scm 326 */
													long BgL_odepthz00_6464;

													{	/* SawMill/node2rtl.scm 326 */
														obj_t BgL_arg1804z00_6465;

														BgL_arg1804z00_6465 = (BgL_oclassz00_6459);
														BgL_odepthz00_6464 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_6465);
													}
													if ((2L < BgL_odepthz00_6464))
														{	/* SawMill/node2rtl.scm 326 */
															obj_t BgL_arg1802z00_6466;

															{	/* SawMill/node2rtl.scm 326 */
																obj_t BgL_arg1803z00_6467;

																BgL_arg1803z00_6467 = (BgL_oclassz00_6459);
																BgL_arg1802z00_6466 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6467,
																	2L);
															}
															BgL_res2852z00_6458 =
																(BgL_arg1802z00_6466 == BgL_classz00_6453);
														}
													else
														{	/* SawMill/node2rtl.scm 326 */
															BgL_res2852z00_6458 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test3152z00_9216 = BgL_res2852z00_6458;
								}
						}
					}
					if (BgL_test3152z00_9216)
						{	/* SawMill/node2rtl.scm 327 */
							obj_t BgL_arg2355z00_6468;

							BgL_arg2355z00_6468 =
								(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_ez00_6005)))->BgL_argsz00);
							return
								BGl_compilezd2labelzd2callz00zzsaw_node2rtlz00(
								((BgL_localz00_bglt) BgL_vz00_6452), BgL_arg2355z00_6468);
						}
					else
						{	/* SawMill/node2rtl.scm 328 */
							obj_t BgL__ortest_1288z00_6469;

							{	/* SawMill/node2rtl.scm 328 */
								obj_t BgL_arg2365z00_6470;

								BgL_arg2365z00_6470 =
									(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_ez00_6005)))->BgL_argsz00);
								BgL__ortest_1288z00_6469 =
									BGl_imperativezf3zf3zzsaw_node2rtlz00(
									((BgL_appz00_bglt) BgL_ez00_6005),
									((BgL_globalz00_bglt) BgL_vz00_6452), BgL_arg2365z00_6470);
							}
							if (CBOOL(BgL__ortest_1288z00_6469))
								{	/* SawMill/node2rtl.scm 328 */
									return ((BgL_areaz00_bglt) BgL__ortest_1288z00_6469);
								}
							else
								{	/* SawMill/node2rtl.scm 331 */
									BgL_rtl_callz00_bglt BgL_arg2361z00_6471;
									obj_t BgL_arg2363z00_6472;

									{	/* SawMill/node2rtl.scm 331 */
										BgL_rtl_callz00_bglt BgL_new1292z00_6473;

										{	/* SawMill/node2rtl.scm 331 */
											BgL_rtl_callz00_bglt BgL_new1291z00_6474;

											BgL_new1291z00_6474 =
												((BgL_rtl_callz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_rtl_callz00_bgl))));
											{	/* SawMill/node2rtl.scm 331 */
												long BgL_arg2364z00_6475;

												BgL_arg2364z00_6475 =
													BGL_CLASS_NUM(BGl_rtl_callz00zzsaw_defsz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1291z00_6474),
													BgL_arg2364z00_6475);
											}
											BgL_new1292z00_6473 = BgL_new1291z00_6474;
										}
										((((BgL_rtl_funz00_bglt) COBJECT(
														((BgL_rtl_funz00_bglt) BgL_new1292z00_6473)))->
												BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
										((((BgL_rtl_callz00_bglt) COBJECT(BgL_new1292z00_6473))->
												BgL_varz00) =
											((BgL_globalz00_bglt)
												BGl_getzd2globalzd2zzsaw_node2rtlz00((
														(BgL_globalz00_bglt) BgL_vz00_6452))), BUNSPEC);
										BgL_arg2361z00_6471 = BgL_new1292z00_6473;
									}
									BgL_arg2363z00_6472 =
										(((BgL_appz00_bglt) COBJECT(
												((BgL_appz00_bglt) BgL_ez00_6005)))->BgL_argsz00);
									return
										BGl_callza2za2zzsaw_node2rtlz00(
										((obj_t)
											((BgL_appz00_bglt) BgL_ez00_6005)),
										((BgL_rtl_funz00_bglt) BgL_arg2361z00_6471),
										BgL_arg2363z00_6472);
								}
						}
				}
			}
		}

	}



/* &node->rtl-let-fun1735 */
	BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2letzd2fun1735z53zzsaw_node2rtlz00(obj_t
		BgL_envz00_6006, obj_t BgL_ez00_6007)
	{
		{	/* SawMill/node2rtl.scm 289 */
			return
				BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_ez00_6007)))->BgL_bodyz00));
		}

	}



/* &node->rtl-switch1733 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2switch1733z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_6008, obj_t BgL_ez00_6009)
	{
		{	/* SawMill/node2rtl.scm 268 */
			{	/* SawMill/node2rtl.scm 270 */
				BgL_areaz00_bglt BgL_arg2338z00_6478;
				obj_t BgL_arg2339z00_6479;
				BgL_rtl_regz00_bglt BgL_arg2340z00_6480;

				{	/* SawMill/node2rtl.scm 270 */
					BgL_rtl_selectz00_bglt BgL_arg2341z00_6481;
					BgL_nodez00_bglt BgL_arg2342z00_6482;

					{	/* SawMill/node2rtl.scm 270 */
						BgL_rtl_selectz00_bglt BgL_new1277z00_6483;

						{	/* SawMill/node2rtl.scm 270 */
							BgL_rtl_selectz00_bglt BgL_new1276z00_6484;

							BgL_new1276z00_6484 =
								((BgL_rtl_selectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_selectz00_bgl))));
							{	/* SawMill/node2rtl.scm 270 */
								long BgL_arg2346z00_6485;

								BgL_arg2346z00_6485 =
									BGL_CLASS_NUM(BGl_rtl_selectz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1276z00_6484),
									BgL_arg2346z00_6485);
							}
							BgL_new1277z00_6483 = BgL_new1276z00_6484;
						}
						((((BgL_rtl_funz00_bglt) COBJECT(
										((BgL_rtl_funz00_bglt) BgL_new1277z00_6483)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						((((BgL_rtl_selectz00_bglt) COBJECT(BgL_new1277z00_6483))->
								BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_switchz00_bglt)
										COBJECT(((BgL_switchz00_bglt) BgL_ez00_6009)))->
									BgL_itemzd2typezd2)), BUNSPEC);
						{
							obj_t BgL_auxz00_9279;

							{	/* SawMill/node2rtl.scm 271 */
								obj_t BgL_arg2345z00_6486;

								BgL_arg2345z00_6486 =
									(((BgL_switchz00_bglt) COBJECT(
											((BgL_switchz00_bglt) BgL_ez00_6009)))->BgL_clausesz00);
								BgL_auxz00_9279 =
									BGl_coercez00zzsaw_node2rtlz00(BgL_arg2345z00_6486);
							}
							((((BgL_rtl_selectz00_bglt) COBJECT(BgL_new1277z00_6483))->
									BgL_patternsz00) = ((obj_t) BgL_auxz00_9279), BUNSPEC);
						}
						BgL_arg2341z00_6481 = BgL_new1277z00_6483;
					}
					BgL_arg2342z00_6482 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_ez00_6009)))->BgL_testz00);
					{	/* SawMill/node2rtl.scm 270 */
						obj_t BgL_list2343z00_6487;

						BgL_list2343z00_6487 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_arg2342z00_6482), BNIL);
						BgL_arg2338z00_6478 =
							BGl_callza2za2zzsaw_node2rtlz00(
							((obj_t)
								((BgL_switchz00_bglt) BgL_ez00_6009)),
							((BgL_rtl_funz00_bglt) BgL_arg2341z00_6481),
							BgL_list2343z00_6487);
				}}
				{	/* SawMill/node2rtl.scm 273 */
					obj_t BgL_l1674z00_6488;

					BgL_l1674z00_6488 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_ez00_6009)))->BgL_clausesz00);
					if (NULLP(BgL_l1674z00_6488))
						{	/* SawMill/node2rtl.scm 273 */
							BgL_arg2339z00_6479 = BNIL;
						}
					else
						{	/* SawMill/node2rtl.scm 273 */
							obj_t BgL_head1676z00_6489;

							BgL_head1676z00_6489 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1674z00_6491;
								obj_t BgL_tail1677z00_6492;

								BgL_l1674z00_6491 = BgL_l1674z00_6488;
								BgL_tail1677z00_6492 = BgL_head1676z00_6489;
							BgL_zc3z04anonymousza32348ze3z87_6490:
								if (NULLP(BgL_l1674z00_6491))
									{	/* SawMill/node2rtl.scm 273 */
										BgL_arg2339z00_6479 = CDR(BgL_head1676z00_6489);
									}
								else
									{	/* SawMill/node2rtl.scm 273 */
										obj_t BgL_newtail1678z00_6493;

										{	/* SawMill/node2rtl.scm 273 */
											BgL_areaz00_bglt BgL_arg2351z00_6494;

											{	/* SawMill/node2rtl.scm 273 */
												obj_t BgL_cz00_6495;

												BgL_cz00_6495 = CAR(((obj_t) BgL_l1674z00_6491));
												{	/* SawMill/node2rtl.scm 273 */
													obj_t BgL_arg2352z00_6496;

													BgL_arg2352z00_6496 = CDR(((obj_t) BgL_cz00_6495));
													BgL_arg2351z00_6494 =
														BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(
														((BgL_nodez00_bglt) BgL_arg2352z00_6496));
												}
											}
											BgL_newtail1678z00_6493 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg2351z00_6494), BNIL);
										}
										SET_CDR(BgL_tail1677z00_6492, BgL_newtail1678z00_6493);
										{	/* SawMill/node2rtl.scm 273 */
											obj_t BgL_arg2350z00_6497;

											BgL_arg2350z00_6497 = CDR(((obj_t) BgL_l1674z00_6491));
											{
												obj_t BgL_tail1677z00_9312;
												obj_t BgL_l1674z00_9311;

												BgL_l1674z00_9311 = BgL_arg2350z00_6497;
												BgL_tail1677z00_9312 = BgL_newtail1678z00_6493;
												BgL_tail1677z00_6492 = BgL_tail1677z00_9312;
												BgL_l1674z00_6491 = BgL_l1674z00_9311;
												goto BgL_zc3z04anonymousza32348ze3z87_6490;
											}
										}
									}
							}
						}
				}
				BgL_arg2340z00_6480 =
					BGl_newzd2regzd2zzsaw_node2rtlz00(
					((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_ez00_6009)));
				return
					BGl_forkzf2joinzf2zzsaw_node2rtlz00(BgL_arg2338z00_6478,
					BgL_arg2339z00_6479, BgL_arg2340z00_6480);
			}
		}

	}



/* &node->rtl-conditiona1731 */
	BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2conditiona1731z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_6010, obj_t BgL_ez00_6011)
	{
		{	/* SawMill/node2rtl.scm 262 */
			{	/* SawMill/node2rtl.scm 264 */
				BgL_nodez00_bglt BgL_arg2326z00_6499;
				obj_t BgL_arg2327z00_6500;

				BgL_arg2326z00_6499 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_ez00_6011)))->BgL_testz00);
				{	/* SawMill/node2rtl.scm 264 */
					obj_t BgL_arg2328z00_6501;
					BgL_rtl_regz00_bglt BgL_arg2330z00_6502;

					{	/* SawMill/node2rtl.scm 264 */
						BgL_areaz00_bglt BgL_arg2331z00_6503;
						BgL_areaz00_bglt BgL_arg2333z00_6504;

						BgL_arg2331z00_6503 =
							BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(
							(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_ez00_6011)))->BgL_truez00));
						BgL_arg2333z00_6504 =
							BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(
							(((BgL_conditionalz00_bglt) COBJECT(
										((BgL_conditionalz00_bglt) BgL_ez00_6011)))->BgL_falsez00));
						{	/* SawMill/node2rtl.scm 264 */
							obj_t BgL_list2334z00_6505;

							{	/* SawMill/node2rtl.scm 264 */
								obj_t BgL_arg2335z00_6506;

								BgL_arg2335z00_6506 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_arg2333z00_6504), BNIL);
								BgL_list2334z00_6505 =
									MAKE_YOUNG_PAIR(
									((obj_t) BgL_arg2331z00_6503), BgL_arg2335z00_6506);
							}
							BgL_arg2328z00_6501 = BgL_list2334z00_6505;
						}
					}
					BgL_arg2330z00_6502 =
						BGl_newzd2regzd2zzsaw_node2rtlz00(
						((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_ez00_6011)));
					BgL_arg2327z00_6500 =
						BGl_joinz00zzsaw_node2rtlz00(BgL_arg2328z00_6501,
						BgL_arg2330z00_6502);
				}
				return
					BGl_predicatez00zzsaw_node2rtlz00(BgL_arg2326z00_6499,
					BgL_arg2327z00_6500);
			}
		}

	}



/* &node->rtl-sync1729 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2sync1729z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_6012, obj_t BgL_ez00_6013)
	{
		{	/* SawMill/node2rtl.scm 258 */
			return
				BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00
				(BGl_synczd2ze3sequencez31zzsync_nodez00(((BgL_syncz00_bglt)
						BgL_ez00_6013)));
		}

	}



/* &node->rtl-sequence1727 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2sequence1727z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_6014, obj_t BgL_ez00_6015)
	{
		{	/* SawMill/node2rtl.scm 253 */
			{	/* SawMill/node2rtl.scm 255 */
				obj_t BgL_arg2318z00_6509;

				{	/* SawMill/node2rtl.scm 255 */
					obj_t BgL_l1669z00_6510;

					BgL_l1669z00_6510 =
						(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_ez00_6015)))->BgL_nodesz00);
					if (NULLP(BgL_l1669z00_6510))
						{	/* SawMill/node2rtl.scm 255 */
							BgL_arg2318z00_6509 = BNIL;
						}
					else
						{	/* SawMill/node2rtl.scm 255 */
							obj_t BgL_head1671z00_6511;

							BgL_head1671z00_6511 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1669z00_6513;
								obj_t BgL_tail1672z00_6514;

								BgL_l1669z00_6513 = BgL_l1669z00_6510;
								BgL_tail1672z00_6514 = BgL_head1671z00_6511;
							BgL_zc3z04anonymousza32320ze3z87_6512:
								if (NULLP(BgL_l1669z00_6513))
									{	/* SawMill/node2rtl.scm 255 */
										BgL_arg2318z00_6509 = CDR(BgL_head1671z00_6511);
									}
								else
									{	/* SawMill/node2rtl.scm 255 */
										obj_t BgL_newtail1673z00_6515;

										{	/* SawMill/node2rtl.scm 255 */
											BgL_areaz00_bglt BgL_arg2324z00_6516;

											{	/* SawMill/node2rtl.scm 255 */
												obj_t BgL_ez00_6517;

												BgL_ez00_6517 = CAR(((obj_t) BgL_l1669z00_6513));
												BgL_arg2324z00_6516 =
													BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(
													((BgL_nodez00_bglt) BgL_ez00_6517));
											}
											BgL_newtail1673z00_6515 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg2324z00_6516), BNIL);
										}
										SET_CDR(BgL_tail1672z00_6514, BgL_newtail1673z00_6515);
										{	/* SawMill/node2rtl.scm 255 */
											obj_t BgL_arg2323z00_6518;

											BgL_arg2323z00_6518 = CDR(((obj_t) BgL_l1669z00_6513));
											{
												obj_t BgL_tail1672z00_9355;
												obj_t BgL_l1669z00_9354;

												BgL_l1669z00_9354 = BgL_arg2323z00_6518;
												BgL_tail1672z00_9355 = BgL_newtail1673z00_6515;
												BgL_tail1672z00_6514 = BgL_tail1672z00_9355;
												BgL_l1669z00_6513 = BgL_l1669z00_9354;
												goto BgL_zc3z04anonymousza32320ze3z87_6512;
											}
										}
									}
							}
						}
				}
				return BGl_linkza2za2zzsaw_node2rtlz00(BgL_arg2318z00_6509);
			}
		}

	}



/* &node->rtl-setq1725 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2setq1725z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_6016, obj_t BgL_ez00_6017)
	{
		{	/* SawMill/node2rtl.scm 244 */
			{	/* SawMill/node2rtl.scm 246 */
				BgL_varz00_bglt BgL_i1268z00_6520;

				BgL_i1268z00_6520 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_ez00_6017)))->BgL_varz00);
				{	/* SawMill/node2rtl.scm 247 */
					BgL_areaz00_bglt BgL_arg2301z00_6521;
					BgL_areaz00_bglt BgL_arg2302z00_6522;

					{	/* SawMill/node2rtl.scm 247 */
						bool_t BgL_test3161z00_9359;

						{	/* SawMill/node2rtl.scm 247 */
							BgL_variablez00_bglt BgL_arg2314z00_6523;

							BgL_arg2314z00_6523 =
								(((BgL_varz00_bglt) COBJECT(BgL_i1268z00_6520))->
								BgL_variablez00);
							{	/* SawMill/node2rtl.scm 247 */
								obj_t BgL_classz00_6524;

								BgL_classz00_6524 = BGl_globalz00zzast_varz00;
								{	/* SawMill/node2rtl.scm 247 */
									BgL_objectz00_bglt BgL_arg1807z00_6525;

									{	/* SawMill/node2rtl.scm 247 */
										obj_t BgL_tmpz00_9361;

										BgL_tmpz00_9361 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg2314z00_6523));
										BgL_arg1807z00_6525 =
											(BgL_objectz00_bglt) (BgL_tmpz00_9361);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawMill/node2rtl.scm 247 */
											long BgL_idxz00_6526;

											BgL_idxz00_6526 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6525);
											BgL_test3161z00_9359 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_6526 + 2L)) == BgL_classz00_6524);
										}
									else
										{	/* SawMill/node2rtl.scm 247 */
											bool_t BgL_res2849z00_6529;

											{	/* SawMill/node2rtl.scm 247 */
												obj_t BgL_oclassz00_6530;

												{	/* SawMill/node2rtl.scm 247 */
													obj_t BgL_arg1815z00_6531;
													long BgL_arg1816z00_6532;

													BgL_arg1815z00_6531 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/node2rtl.scm 247 */
														long BgL_arg1817z00_6533;

														BgL_arg1817z00_6533 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6525);
														BgL_arg1816z00_6532 =
															(BgL_arg1817z00_6533 - OBJECT_TYPE);
													}
													BgL_oclassz00_6530 =
														VECTOR_REF(BgL_arg1815z00_6531,
														BgL_arg1816z00_6532);
												}
												{	/* SawMill/node2rtl.scm 247 */
													bool_t BgL__ortest_1115z00_6534;

													BgL__ortest_1115z00_6534 =
														(BgL_classz00_6524 == BgL_oclassz00_6530);
													if (BgL__ortest_1115z00_6534)
														{	/* SawMill/node2rtl.scm 247 */
															BgL_res2849z00_6529 = BgL__ortest_1115z00_6534;
														}
													else
														{	/* SawMill/node2rtl.scm 247 */
															long BgL_odepthz00_6535;

															{	/* SawMill/node2rtl.scm 247 */
																obj_t BgL_arg1804z00_6536;

																BgL_arg1804z00_6536 = (BgL_oclassz00_6530);
																BgL_odepthz00_6535 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_6536);
															}
															if ((2L < BgL_odepthz00_6535))
																{	/* SawMill/node2rtl.scm 247 */
																	obj_t BgL_arg1802z00_6537;

																	{	/* SawMill/node2rtl.scm 247 */
																		obj_t BgL_arg1803z00_6538;

																		BgL_arg1803z00_6538 = (BgL_oclassz00_6530);
																		BgL_arg1802z00_6537 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_6538, 2L);
																	}
																	BgL_res2849z00_6529 =
																		(BgL_arg1802z00_6537 == BgL_classz00_6524);
																}
															else
																{	/* SawMill/node2rtl.scm 247 */
																	BgL_res2849z00_6529 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3161z00_9359 = BgL_res2849z00_6529;
										}
								}
							}
						}
						if (BgL_test3161z00_9359)
							{	/* SawMill/node2rtl.scm 248 */
								BgL_rtl_storegz00_bglt BgL_arg2306z00_6539;
								BgL_nodez00_bglt BgL_arg2307z00_6540;

								{	/* SawMill/node2rtl.scm 248 */
									BgL_rtl_storegz00_bglt BgL_new1270z00_6541;

									{	/* SawMill/node2rtl.scm 248 */
										BgL_rtl_storegz00_bglt BgL_new1269z00_6542;

										BgL_new1269z00_6542 =
											((BgL_rtl_storegz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_rtl_storegz00_bgl))));
										{	/* SawMill/node2rtl.scm 248 */
											long BgL_arg2310z00_6543;

											BgL_arg2310z00_6543 =
												BGL_CLASS_NUM(BGl_rtl_storegz00zzsaw_defsz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1269z00_6542),
												BgL_arg2310z00_6543);
										}
										BgL_new1270z00_6541 = BgL_new1269z00_6542;
									}
									((((BgL_rtl_funz00_bglt) COBJECT(
													((BgL_rtl_funz00_bglt) BgL_new1270z00_6541)))->
											BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
									{
										BgL_globalz00_bglt BgL_auxz00_9390;

										{	/* SawMill/node2rtl.scm 248 */
											BgL_variablez00_bglt BgL_arg2309z00_6544;

											BgL_arg2309z00_6544 =
												(((BgL_varz00_bglt) COBJECT(BgL_i1268z00_6520))->
												BgL_variablez00);
											BgL_auxz00_9390 =
												BGl_getzd2globalzd2zzsaw_node2rtlz00((
													(BgL_globalz00_bglt) BgL_arg2309z00_6544));
										}
										((((BgL_rtl_storegz00_bglt) COBJECT(BgL_new1270z00_6541))->
												BgL_varz00) =
											((BgL_globalz00_bglt) BgL_auxz00_9390), BUNSPEC);
									}
									BgL_arg2306z00_6539 = BgL_new1270z00_6541;
								}
								BgL_arg2307z00_6540 =
									(((BgL_setqz00_bglt) COBJECT(
											((BgL_setqz00_bglt) BgL_ez00_6017)))->BgL_valuez00);
								{	/* SawMill/node2rtl.scm 248 */
									obj_t BgL_list2308z00_6545;

									BgL_list2308z00_6545 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_arg2307z00_6540), BNIL);
									BgL_arg2301z00_6521 =
										BGl_callza2za2zzsaw_node2rtlz00(
										((obj_t)
											((BgL_setqz00_bglt) BgL_ez00_6017)),
										((BgL_rtl_funz00_bglt) BgL_arg2306z00_6539),
										BgL_list2308z00_6545);
							}}
						else
							{	/* SawMill/node2rtl.scm 249 */
								BgL_nodez00_bglt BgL_arg2311z00_6546;
								BgL_rtl_regz00_bglt BgL_arg2312z00_6547;

								BgL_arg2311z00_6546 =
									(((BgL_setqz00_bglt) COBJECT(
											((BgL_setqz00_bglt) BgL_ez00_6017)))->BgL_valuez00);
								{	/* SawMill/node2rtl.scm 249 */
									BgL_variablez00_bglt BgL_arg2313z00_6548;

									BgL_arg2313z00_6548 =
										(((BgL_varz00_bglt) COBJECT(BgL_i1268z00_6520))->
										BgL_variablez00);
									BgL_arg2312z00_6547 =
										BGl_localzd2ze3regz31zzsaw_node2rtlz00(((BgL_localz00_bglt)
											BgL_arg2313z00_6548));
								}
								{	/* SawMill/node2rtl.scm 249 */
									BgL_areaz00_bglt BgL_res2850z00_6549;

									{	/* SawMill/node2rtl.scm 187 */
										BgL_areaz00_bglt BgL_az00_6550;

										BgL_az00_6550 =
											BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00
											(BgL_arg2311z00_6546);
										BGl_bdestinationz12z12zzsaw_node2rtlz00((((BgL_areaz00_bglt)
													COBJECT(BgL_az00_6550))->BgL_exitz00),
											BgL_arg2312z00_6547);
										BgL_res2850z00_6549 = BgL_az00_6550;
									}
									BgL_arg2301z00_6521 = BgL_res2850z00_6549;
								}
							}
					}
					{	/* SawMill/node2rtl.scm 250 */
						BgL_rtl_nopz00_bglt BgL_arg2315z00_6551;

						{	/* SawMill/node2rtl.scm 250 */
							BgL_rtl_nopz00_bglt BgL_new1272z00_6552;

							{	/* SawMill/node2rtl.scm 250 */
								BgL_rtl_nopz00_bglt BgL_new1271z00_6553;

								BgL_new1271z00_6553 =
									((BgL_rtl_nopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_rtl_nopz00_bgl))));
								{	/* SawMill/node2rtl.scm 250 */
									long BgL_arg2317z00_6554;

									BgL_arg2317z00_6554 =
										BGL_CLASS_NUM(BGl_rtl_nopz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1271z00_6553),
										BgL_arg2317z00_6554);
								}
								BgL_new1272z00_6552 = BgL_new1271z00_6553;
							}
							((((BgL_rtl_funz00_bglt) COBJECT(
											((BgL_rtl_funz00_bglt) BgL_new1272z00_6552)))->
									BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
							BgL_arg2315z00_6551 = BgL_new1272z00_6552;
						}
						BgL_arg2302z00_6522 =
							BGl_singleza2za2zzsaw_node2rtlz00(BFALSE,
							((BgL_rtl_funz00_bglt) BgL_arg2315z00_6551), BNIL);
					}
					return
						BGl_linkz00zzsaw_node2rtlz00(BgL_arg2301z00_6521,
						BgL_arg2302z00_6522);
				}
			}
		}

	}



/* &node->rtl-let-var1723 */
	BgL_areaz00_bglt
		BGl_z62nodezd2ze3rtlzd2letzd2var1723z53zzsaw_node2rtlz00(obj_t
		BgL_envz00_6018, obj_t BgL_ez00_6019)
	{
		{	/* SawMill/node2rtl.scm 227 */
			{	/* SawMill/node2rtl.scm 230 */
				bool_t BgL_test3165z00_9420;

				{	/* SawMill/node2rtl.scm 230 */
					bool_t BgL_test3166z00_9421;

					{	/* SawMill/node2rtl.scm 230 */
						BgL_nodez00_bglt BgL_arg2299z00_6556;

						BgL_arg2299z00_6556 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_ez00_6019)))->BgL_bodyz00);
						{	/* SawMill/node2rtl.scm 230 */
							obj_t BgL_classz00_6557;

							BgL_classz00_6557 = BGl_conditionalz00zzast_nodez00;
							{	/* SawMill/node2rtl.scm 230 */
								BgL_objectz00_bglt BgL_arg1807z00_6558;

								{	/* SawMill/node2rtl.scm 230 */
									obj_t BgL_tmpz00_9424;

									BgL_tmpz00_9424 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg2299z00_6556));
									BgL_arg1807z00_6558 = (BgL_objectz00_bglt) (BgL_tmpz00_9424);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawMill/node2rtl.scm 230 */
										long BgL_idxz00_6559;

										BgL_idxz00_6559 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6558);
										BgL_test3166z00_9421 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_6559 + 3L)) == BgL_classz00_6557);
									}
								else
									{	/* SawMill/node2rtl.scm 230 */
										bool_t BgL_res2846z00_6562;

										{	/* SawMill/node2rtl.scm 230 */
											obj_t BgL_oclassz00_6563;

											{	/* SawMill/node2rtl.scm 230 */
												obj_t BgL_arg1815z00_6564;
												long BgL_arg1816z00_6565;

												BgL_arg1815z00_6564 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawMill/node2rtl.scm 230 */
													long BgL_arg1817z00_6566;

													BgL_arg1817z00_6566 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6558);
													BgL_arg1816z00_6565 =
														(BgL_arg1817z00_6566 - OBJECT_TYPE);
												}
												BgL_oclassz00_6563 =
													VECTOR_REF(BgL_arg1815z00_6564, BgL_arg1816z00_6565);
											}
											{	/* SawMill/node2rtl.scm 230 */
												bool_t BgL__ortest_1115z00_6567;

												BgL__ortest_1115z00_6567 =
													(BgL_classz00_6557 == BgL_oclassz00_6563);
												if (BgL__ortest_1115z00_6567)
													{	/* SawMill/node2rtl.scm 230 */
														BgL_res2846z00_6562 = BgL__ortest_1115z00_6567;
													}
												else
													{	/* SawMill/node2rtl.scm 230 */
														long BgL_odepthz00_6568;

														{	/* SawMill/node2rtl.scm 230 */
															obj_t BgL_arg1804z00_6569;

															BgL_arg1804z00_6569 = (BgL_oclassz00_6563);
															BgL_odepthz00_6568 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_6569);
														}
														if ((3L < BgL_odepthz00_6568))
															{	/* SawMill/node2rtl.scm 230 */
																obj_t BgL_arg1802z00_6570;

																{	/* SawMill/node2rtl.scm 230 */
																	obj_t BgL_arg1803z00_6571;

																	BgL_arg1803z00_6571 = (BgL_oclassz00_6563);
																	BgL_arg1802z00_6570 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6571,
																		3L);
																}
																BgL_res2846z00_6562 =
																	(BgL_arg1802z00_6570 == BgL_classz00_6557);
															}
														else
															{	/* SawMill/node2rtl.scm 230 */
																BgL_res2846z00_6562 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3166z00_9421 = BgL_res2846z00_6562;
									}
							}
						}
					}
					if (BgL_test3166z00_9421)
						{	/* SawMill/node2rtl.scm 230 */
							if (NULLP(
									(((BgL_letzd2varzd2_bglt) COBJECT(
												((BgL_letzd2varzd2_bglt) BgL_ez00_6019)))->
										BgL_bindingsz00)))
								{	/* SawMill/node2rtl.scm 231 */
									BgL_test3165z00_9420 = ((bool_t) 0);
								}
							else
								{	/* SawMill/node2rtl.scm 231 */
									if (NULLP(CDR(
												(((BgL_letzd2varzd2_bglt) COBJECT(
															((BgL_letzd2varzd2_bglt) BgL_ez00_6019)))->
													BgL_bindingsz00))))
										{	/* SawMill/node2rtl.scm 233 */
											bool_t BgL_test3172z00_9456;

											{	/* SawMill/node2rtl.scm 233 */
												BgL_nodez00_bglt BgL_arg2294z00_6572;

												BgL_arg2294z00_6572 =
													(((BgL_conditionalz00_bglt) COBJECT(
															((BgL_conditionalz00_bglt)
																(((BgL_letzd2varzd2_bglt) COBJECT(
																			((BgL_letzd2varzd2_bglt)
																				BgL_ez00_6019)))->BgL_bodyz00))))->
													BgL_testz00);
												{	/* SawMill/node2rtl.scm 233 */
													obj_t BgL_classz00_6573;

													BgL_classz00_6573 = BGl_varz00zzast_nodez00;
													{	/* SawMill/node2rtl.scm 233 */
														BgL_objectz00_bglt BgL_arg1807z00_6574;

														{	/* SawMill/node2rtl.scm 233 */
															obj_t BgL_tmpz00_9461;

															BgL_tmpz00_9461 =
																((obj_t)
																((BgL_objectz00_bglt) BgL_arg2294z00_6572));
															BgL_arg1807z00_6574 =
																(BgL_objectz00_bglt) (BgL_tmpz00_9461);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawMill/node2rtl.scm 233 */
																long BgL_idxz00_6575;

																BgL_idxz00_6575 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_6574);
																BgL_test3172z00_9456 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_6575 + 2L)) ==
																	BgL_classz00_6573);
															}
														else
															{	/* SawMill/node2rtl.scm 233 */
																bool_t BgL_res2847z00_6578;

																{	/* SawMill/node2rtl.scm 233 */
																	obj_t BgL_oclassz00_6579;

																	{	/* SawMill/node2rtl.scm 233 */
																		obj_t BgL_arg1815z00_6580;
																		long BgL_arg1816z00_6581;

																		BgL_arg1815z00_6580 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawMill/node2rtl.scm 233 */
																			long BgL_arg1817z00_6582;

																			BgL_arg1817z00_6582 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_6574);
																			BgL_arg1816z00_6581 =
																				(BgL_arg1817z00_6582 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_6579 =
																			VECTOR_REF(BgL_arg1815z00_6580,
																			BgL_arg1816z00_6581);
																	}
																	{	/* SawMill/node2rtl.scm 233 */
																		bool_t BgL__ortest_1115z00_6583;

																		BgL__ortest_1115z00_6583 =
																			(BgL_classz00_6573 == BgL_oclassz00_6579);
																		if (BgL__ortest_1115z00_6583)
																			{	/* SawMill/node2rtl.scm 233 */
																				BgL_res2847z00_6578 =
																					BgL__ortest_1115z00_6583;
																			}
																		else
																			{	/* SawMill/node2rtl.scm 233 */
																				long BgL_odepthz00_6584;

																				{	/* SawMill/node2rtl.scm 233 */
																					obj_t BgL_arg1804z00_6585;

																					BgL_arg1804z00_6585 =
																						(BgL_oclassz00_6579);
																					BgL_odepthz00_6584 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_6585);
																				}
																				if ((2L < BgL_odepthz00_6584))
																					{	/* SawMill/node2rtl.scm 233 */
																						obj_t BgL_arg1802z00_6586;

																						{	/* SawMill/node2rtl.scm 233 */
																							obj_t BgL_arg1803z00_6587;

																							BgL_arg1803z00_6587 =
																								(BgL_oclassz00_6579);
																							BgL_arg1802z00_6586 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_6587, 2L);
																						}
																						BgL_res2847z00_6578 =
																							(BgL_arg1802z00_6586 ==
																							BgL_classz00_6573);
																					}
																				else
																					{	/* SawMill/node2rtl.scm 233 */
																						BgL_res2847z00_6578 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test3172z00_9456 = BgL_res2847z00_6578;
															}
													}
												}
											}
											if (BgL_test3172z00_9456)
												{	/* SawMill/node2rtl.scm 234 */
													bool_t BgL_test3176z00_9484;

													{	/* SawMill/node2rtl.scm 234 */
														long BgL_tmpz00_9485;

														{
															BgL_variablez00_bglt BgL_auxz00_9486;

															{
																BgL_localz00_bglt BgL_auxz00_9487;

																{
																	obj_t BgL_auxz00_9488;

																	{	/* SawMill/node2rtl.scm 234 */
																		obj_t BgL_pairz00_6588;

																		BgL_pairz00_6588 =
																			(((BgL_letzd2varzd2_bglt) COBJECT(
																					((BgL_letzd2varzd2_bglt)
																						BgL_ez00_6019)))->BgL_bindingsz00);
																		{	/* SawMill/node2rtl.scm 234 */
																			obj_t BgL_pairz00_6589;

																			BgL_pairz00_6589 = CAR(BgL_pairz00_6588);
																			BgL_auxz00_9488 = CAR(BgL_pairz00_6589);
																	}}
																	BgL_auxz00_9487 =
																		((BgL_localz00_bglt) BgL_auxz00_9488);
																}
																BgL_auxz00_9486 =
																	((BgL_variablez00_bglt) BgL_auxz00_9487);
															}
															BgL_tmpz00_9485 =
																(((BgL_variablez00_bglt)
																	COBJECT(BgL_auxz00_9486))->BgL_occurrencez00);
														}
														BgL_test3176z00_9484 = (1L == BgL_tmpz00_9485);
													}
													if (BgL_test3176z00_9484)
														{	/* SawMill/node2rtl.scm 235 */
															obj_t BgL_tmpz00_9497;

															{	/* SawMill/node2rtl.scm 235 */
																obj_t BgL_pairz00_6590;

																BgL_pairz00_6590 =
																	(((BgL_letzd2varzd2_bglt) COBJECT(
																			((BgL_letzd2varzd2_bglt)
																				BgL_ez00_6019)))->BgL_bindingsz00);
																BgL_tmpz00_9497 = CAR(CAR(BgL_pairz00_6590));
															}
															BgL_test3165z00_9420 =
																(BgL_tmpz00_9497 ==
																((obj_t)
																	(((BgL_varz00_bglt) COBJECT(
																				((BgL_varz00_bglt)
																					(((BgL_conditionalz00_bglt) COBJECT(
																								((BgL_conditionalz00_bglt)
																									(((BgL_letzd2varzd2_bglt)
																											COBJECT((
																													(BgL_letzd2varzd2_bglt)
																													BgL_ez00_6019)))->
																										BgL_bodyz00))))->
																						BgL_testz00))))->BgL_variablez00)));
														}
													else
														{	/* SawMill/node2rtl.scm 234 */
															BgL_test3165z00_9420 = ((bool_t) 0);
														}
												}
											else
												{	/* SawMill/node2rtl.scm 233 */
													BgL_test3165z00_9420 = ((bool_t) 0);
												}
										}
									else
										{	/* SawMill/node2rtl.scm 232 */
											BgL_test3165z00_9420 = ((bool_t) 0);
										}
								}
						}
					else
						{	/* SawMill/node2rtl.scm 230 */
							BgL_test3165z00_9420 = ((bool_t) 0);
						}
				}
				if (BgL_test3165z00_9420)
					{	/* SawMill/node2rtl.scm 230 */
						{	/* SawMill/node2rtl.scm 236 */
							BgL_nodez00_bglt BgL_arg2269z00_6591;
							obj_t BgL_arg2270z00_6592;

							BgL_arg2269z00_6591 =
								(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_ez00_6019)))->BgL_bodyz00);
							{	/* SawMill/node2rtl.scm 236 */
								obj_t BgL_pairz00_6593;

								BgL_pairz00_6593 =
									(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_ez00_6019)))->
									BgL_bindingsz00);
								BgL_arg2270z00_6592 = CDR(CAR(BgL_pairz00_6593));
							}
							((((BgL_conditionalz00_bglt) COBJECT(
											((BgL_conditionalz00_bglt) BgL_arg2269z00_6591)))->
									BgL_testz00) =
								((BgL_nodez00_bglt) ((BgL_nodez00_bglt) BgL_arg2270z00_6592)),
								BUNSPEC);
						}
						return
							BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(
							(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_ez00_6019)))->BgL_bodyz00));
					}
				else
					{	/* SawMill/node2rtl.scm 239 */
						BgL_areaz00_bglt BgL_arg2273z00_6594;
						BgL_areaz00_bglt BgL_arg2274z00_6595;

						{	/* SawMill/node2rtl.scm 239 */
							obj_t BgL_arg2275z00_6596;

							{	/* SawMill/node2rtl.scm 239 */
								obj_t BgL_l1664z00_6597;

								BgL_l1664z00_6597 =
									(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_ez00_6019)))->
									BgL_bindingsz00);
								if (NULLP(BgL_l1664z00_6597))
									{	/* SawMill/node2rtl.scm 239 */
										BgL_arg2275z00_6596 = BNIL;
									}
								else
									{	/* SawMill/node2rtl.scm 239 */
										obj_t BgL_head1666z00_6598;

										BgL_head1666z00_6598 = MAKE_YOUNG_PAIR(BNIL, BNIL);
										{
											obj_t BgL_l1664z00_6600;
											obj_t BgL_tail1667z00_6601;

											BgL_l1664z00_6600 = BgL_l1664z00_6597;
											BgL_tail1667z00_6601 = BgL_head1666z00_6598;
										BgL_zc3z04anonymousza32277ze3z87_6599:
											if (NULLP(BgL_l1664z00_6600))
												{	/* SawMill/node2rtl.scm 239 */
													BgL_arg2275z00_6596 = CDR(BgL_head1666z00_6598);
												}
											else
												{	/* SawMill/node2rtl.scm 239 */
													obj_t BgL_newtail1668z00_6602;

													{	/* SawMill/node2rtl.scm 239 */
														BgL_areaz00_bglt BgL_arg2280z00_6603;

														{	/* SawMill/node2rtl.scm 239 */
															obj_t BgL_bz00_6604;

															BgL_bz00_6604 = CAR(((obj_t) BgL_l1664z00_6600));
															{	/* SawMill/node2rtl.scm 239 */
																obj_t BgL_arg2281z00_6605;
																BgL_rtl_regz00_bglt BgL_arg2282z00_6606;

																BgL_arg2281z00_6605 =
																	CDR(((obj_t) BgL_bz00_6604));
																{	/* SawMill/node2rtl.scm 239 */
																	obj_t BgL_arg2283z00_6607;

																	BgL_arg2283z00_6607 =
																		CAR(((obj_t) BgL_bz00_6604));
																	BgL_arg2282z00_6606 =
																		BGl_localzd2ze3regz31zzsaw_node2rtlz00(
																		((BgL_localz00_bglt) BgL_arg2283z00_6607));
																}
																{	/* SawMill/node2rtl.scm 239 */
																	BgL_areaz00_bglt BgL_res2848z00_6608;

																	{	/* SawMill/node2rtl.scm 187 */
																		BgL_areaz00_bglt BgL_az00_6609;

																		BgL_az00_6609 =
																			BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(
																			((BgL_nodez00_bglt) BgL_arg2281z00_6605));
																		BGl_bdestinationz12z12zzsaw_node2rtlz00(
																			(((BgL_areaz00_bglt)
																					COBJECT(BgL_az00_6609))->BgL_exitz00),
																			BgL_arg2282z00_6606);
																		BgL_res2848z00_6608 = BgL_az00_6609;
																	}
																	BgL_arg2280z00_6603 = BgL_res2848z00_6608;
																}
															}
														}
														BgL_newtail1668z00_6602 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg2280z00_6603), BNIL);
													}
													SET_CDR(BgL_tail1667z00_6601,
														BgL_newtail1668z00_6602);
													{	/* SawMill/node2rtl.scm 239 */
														obj_t BgL_arg2279z00_6610;

														BgL_arg2279z00_6610 =
															CDR(((obj_t) BgL_l1664z00_6600));
														{
															obj_t BgL_tail1667z00_9548;
															obj_t BgL_l1664z00_9547;

															BgL_l1664z00_9547 = BgL_arg2279z00_6610;
															BgL_tail1667z00_9548 = BgL_newtail1668z00_6602;
															BgL_tail1667z00_6601 = BgL_tail1667z00_9548;
															BgL_l1664z00_6600 = BgL_l1664z00_9547;
															goto BgL_zc3z04anonymousza32277ze3z87_6599;
														}
													}
												}
										}
									}
							}
							BgL_arg2273z00_6594 =
								BGl_linkza2za2zzsaw_node2rtlz00(BgL_arg2275z00_6596);
						}
						BgL_arg2274z00_6595 =
							BGl_nodezd2ze3rtlz31zzsaw_node2rtlz00(
							(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_ez00_6019)))->BgL_bodyz00));
						return
							BGl_linkz00zzsaw_node2rtlz00(BgL_arg2273z00_6594,
							BgL_arg2274z00_6595);
					}
			}
		}

	}



/* &node->rtl-var1721 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2var1721z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_6020, obj_t BgL_ez00_6021)
	{
		{	/* SawMill/node2rtl.scm 214 */
			{	/* SawMill/node2rtl.scm 216 */
				bool_t BgL_test3179z00_9554;

				{	/* SawMill/node2rtl.scm 216 */
					BgL_variablez00_bglt BgL_arg2209z00_6612;

					BgL_arg2209z00_6612 =
						(((BgL_varz00_bglt) COBJECT(
								((BgL_varz00_bglt) BgL_ez00_6021)))->BgL_variablez00);
					{	/* SawMill/node2rtl.scm 216 */
						obj_t BgL_classz00_6613;

						BgL_classz00_6613 = BGl_localz00zzast_varz00;
						{	/* SawMill/node2rtl.scm 216 */
							BgL_objectz00_bglt BgL_arg1807z00_6614;

							{	/* SawMill/node2rtl.scm 216 */
								obj_t BgL_tmpz00_9557;

								BgL_tmpz00_9557 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg2209z00_6612));
								BgL_arg1807z00_6614 = (BgL_objectz00_bglt) (BgL_tmpz00_9557);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawMill/node2rtl.scm 216 */
									long BgL_idxz00_6615;

									BgL_idxz00_6615 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6614);
									BgL_test3179z00_9554 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_6615 + 2L)) == BgL_classz00_6613);
								}
							else
								{	/* SawMill/node2rtl.scm 216 */
									bool_t BgL_res2842z00_6618;

									{	/* SawMill/node2rtl.scm 216 */
										obj_t BgL_oclassz00_6619;

										{	/* SawMill/node2rtl.scm 216 */
											obj_t BgL_arg1815z00_6620;
											long BgL_arg1816z00_6621;

											BgL_arg1815z00_6620 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawMill/node2rtl.scm 216 */
												long BgL_arg1817z00_6622;

												BgL_arg1817z00_6622 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6614);
												BgL_arg1816z00_6621 =
													(BgL_arg1817z00_6622 - OBJECT_TYPE);
											}
											BgL_oclassz00_6619 =
												VECTOR_REF(BgL_arg1815z00_6620, BgL_arg1816z00_6621);
										}
										{	/* SawMill/node2rtl.scm 216 */
											bool_t BgL__ortest_1115z00_6623;

											BgL__ortest_1115z00_6623 =
												(BgL_classz00_6613 == BgL_oclassz00_6619);
											if (BgL__ortest_1115z00_6623)
												{	/* SawMill/node2rtl.scm 216 */
													BgL_res2842z00_6618 = BgL__ortest_1115z00_6623;
												}
											else
												{	/* SawMill/node2rtl.scm 216 */
													long BgL_odepthz00_6624;

													{	/* SawMill/node2rtl.scm 216 */
														obj_t BgL_arg1804z00_6625;

														BgL_arg1804z00_6625 = (BgL_oclassz00_6619);
														BgL_odepthz00_6624 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_6625);
													}
													if ((2L < BgL_odepthz00_6624))
														{	/* SawMill/node2rtl.scm 216 */
															obj_t BgL_arg1802z00_6626;

															{	/* SawMill/node2rtl.scm 216 */
																obj_t BgL_arg1803z00_6627;

																BgL_arg1803z00_6627 = (BgL_oclassz00_6619);
																BgL_arg1802z00_6626 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6627,
																	2L);
															}
															BgL_res2842z00_6618 =
																(BgL_arg1802z00_6626 == BgL_classz00_6613);
														}
													else
														{	/* SawMill/node2rtl.scm 216 */
															BgL_res2842z00_6618 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test3179z00_9554 = BgL_res2842z00_6618;
								}
						}
					}
				}
				if (BgL_test3179z00_9554)
					{	/* SawMill/node2rtl.scm 216 */
						BFALSE;
					}
				else
					{	/* SawMill/node2rtl.scm 217 */
						obj_t BgL_arg2196z00_6628;
						obj_t BgL_arg2197z00_6629;
						bool_t BgL_arg2198z00_6630;

						{	/* SawMill/node2rtl.scm 217 */
							obj_t BgL_tmpz00_9580;

							BgL_tmpz00_9580 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_arg2196z00_6628 = BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_9580);
						}
						BgL_arg2197z00_6629 =
							BGl_shapez00zztools_shapez00(
							((obj_t) ((BgL_varz00_bglt) BgL_ez00_6021)));
						{	/* SawMill/node2rtl.scm 217 */
							BgL_valuez00_bglt BgL_arg2207z00_6631;

							BgL_arg2207z00_6631 =
								(((BgL_variablez00_bglt) COBJECT(
										(((BgL_varz00_bglt) COBJECT(
													((BgL_varz00_bglt) BgL_ez00_6021)))->
											BgL_variablez00)))->BgL_valuez00);
							{	/* SawMill/node2rtl.scm 217 */
								obj_t BgL_classz00_6632;

								BgL_classz00_6632 = BGl_sfunz00zzast_varz00;
								{	/* SawMill/node2rtl.scm 217 */
									BgL_objectz00_bglt BgL_arg1807z00_6633;

									{	/* SawMill/node2rtl.scm 217 */
										obj_t BgL_tmpz00_9589;

										BgL_tmpz00_9589 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg2207z00_6631));
										BgL_arg1807z00_6633 =
											(BgL_objectz00_bglt) (BgL_tmpz00_9589);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawMill/node2rtl.scm 217 */
											long BgL_idxz00_6634;

											BgL_idxz00_6634 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6633);
											BgL_arg2198z00_6630 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_6634 + 3L)) == BgL_classz00_6632);
										}
									else
										{	/* SawMill/node2rtl.scm 217 */
											bool_t BgL_res2843z00_6637;

											{	/* SawMill/node2rtl.scm 217 */
												obj_t BgL_oclassz00_6638;

												{	/* SawMill/node2rtl.scm 217 */
													obj_t BgL_arg1815z00_6639;
													long BgL_arg1816z00_6640;

													BgL_arg1815z00_6639 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/node2rtl.scm 217 */
														long BgL_arg1817z00_6641;

														BgL_arg1817z00_6641 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6633);
														BgL_arg1816z00_6640 =
															(BgL_arg1817z00_6641 - OBJECT_TYPE);
													}
													BgL_oclassz00_6638 =
														VECTOR_REF(BgL_arg1815z00_6639,
														BgL_arg1816z00_6640);
												}
												{	/* SawMill/node2rtl.scm 217 */
													bool_t BgL__ortest_1115z00_6642;

													BgL__ortest_1115z00_6642 =
														(BgL_classz00_6632 == BgL_oclassz00_6638);
													if (BgL__ortest_1115z00_6642)
														{	/* SawMill/node2rtl.scm 217 */
															BgL_res2843z00_6637 = BgL__ortest_1115z00_6642;
														}
													else
														{	/* SawMill/node2rtl.scm 217 */
															long BgL_odepthz00_6643;

															{	/* SawMill/node2rtl.scm 217 */
																obj_t BgL_arg1804z00_6644;

																BgL_arg1804z00_6644 = (BgL_oclassz00_6638);
																BgL_odepthz00_6643 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_6644);
															}
															if ((3L < BgL_odepthz00_6643))
																{	/* SawMill/node2rtl.scm 217 */
																	obj_t BgL_arg1802z00_6645;

																	{	/* SawMill/node2rtl.scm 217 */
																		obj_t BgL_arg1803z00_6646;

																		BgL_arg1803z00_6646 = (BgL_oclassz00_6638);
																		BgL_arg1802z00_6645 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_6646, 3L);
																	}
																	BgL_res2843z00_6637 =
																		(BgL_arg1802z00_6645 == BgL_classz00_6632);
																}
															else
																{	/* SawMill/node2rtl.scm 217 */
																	BgL_res2843z00_6637 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_arg2198z00_6630 = BgL_res2843z00_6637;
										}
								}
							}
						}
						{	/* SawMill/node2rtl.scm 217 */
							obj_t BgL_list2199z00_6647;

							{	/* SawMill/node2rtl.scm 217 */
								obj_t BgL_arg2200z00_6648;

								{	/* SawMill/node2rtl.scm 217 */
									obj_t BgL_arg2201z00_6649;

									{	/* SawMill/node2rtl.scm 217 */
										obj_t BgL_arg2202z00_6650;

										{	/* SawMill/node2rtl.scm 217 */
											obj_t BgL_arg2203z00_6651;

											{	/* SawMill/node2rtl.scm 217 */
												obj_t BgL_arg2204z00_6652;

												{	/* SawMill/node2rtl.scm 217 */
													obj_t BgL_arg2205z00_6653;

													{	/* SawMill/node2rtl.scm 217 */
														obj_t BgL_arg2206z00_6654;

														BgL_arg2206z00_6654 =
															MAKE_YOUNG_PAIR(BBOOL(BgL_arg2198z00_6630), BNIL);
														BgL_arg2205z00_6653 =
															MAKE_YOUNG_PAIR
															(BGl_string2937z00zzsaw_node2rtlz00,
															BgL_arg2206z00_6654);
													}
													BgL_arg2204z00_6652 =
														MAKE_YOUNG_PAIR(BgL_arg2197z00_6629,
														BgL_arg2205z00_6653);
												}
												BgL_arg2203z00_6651 =
													MAKE_YOUNG_PAIR(BGl_string2938z00zzsaw_node2rtlz00,
													BgL_arg2204z00_6652);
											}
											BgL_arg2202z00_6650 =
												MAKE_YOUNG_PAIR(BGl_string2939z00zzsaw_node2rtlz00,
												BgL_arg2203z00_6651);
										}
										BgL_arg2201z00_6649 =
											MAKE_YOUNG_PAIR(BINT(217L), BgL_arg2202z00_6650);
									}
									BgL_arg2200z00_6648 =
										MAKE_YOUNG_PAIR(BGl_string2940z00zzsaw_node2rtlz00,
										BgL_arg2201z00_6649);
								}
								BgL_list2199z00_6647 =
									MAKE_YOUNG_PAIR(BGl_string2941z00zzsaw_node2rtlz00,
									BgL_arg2200z00_6648);
							}
							BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg2196z00_6628,
								BgL_list2199z00_6647);
						}
					}
			}
			{	/* SawMill/node2rtl.scm 218 */
				bool_t BgL_test3186z00_9623;

				{	/* SawMill/node2rtl.scm 218 */
					BgL_variablez00_bglt BgL_arg2239z00_6655;

					BgL_arg2239z00_6655 =
						(((BgL_varz00_bglt) COBJECT(
								((BgL_varz00_bglt) BgL_ez00_6021)))->BgL_variablez00);
					{	/* SawMill/node2rtl.scm 218 */
						obj_t BgL_classz00_6656;

						BgL_classz00_6656 = BGl_localz00zzast_varz00;
						{	/* SawMill/node2rtl.scm 218 */
							BgL_objectz00_bglt BgL_arg1807z00_6657;

							{	/* SawMill/node2rtl.scm 218 */
								obj_t BgL_tmpz00_9626;

								BgL_tmpz00_9626 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg2239z00_6655));
								BgL_arg1807z00_6657 = (BgL_objectz00_bglt) (BgL_tmpz00_9626);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawMill/node2rtl.scm 218 */
									long BgL_idxz00_6658;

									BgL_idxz00_6658 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6657);
									BgL_test3186z00_9623 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_6658 + 2L)) == BgL_classz00_6656);
								}
							else
								{	/* SawMill/node2rtl.scm 218 */
									bool_t BgL_res2844z00_6661;

									{	/* SawMill/node2rtl.scm 218 */
										obj_t BgL_oclassz00_6662;

										{	/* SawMill/node2rtl.scm 218 */
											obj_t BgL_arg1815z00_6663;
											long BgL_arg1816z00_6664;

											BgL_arg1815z00_6663 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawMill/node2rtl.scm 218 */
												long BgL_arg1817z00_6665;

												BgL_arg1817z00_6665 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6657);
												BgL_arg1816z00_6664 =
													(BgL_arg1817z00_6665 - OBJECT_TYPE);
											}
											BgL_oclassz00_6662 =
												VECTOR_REF(BgL_arg1815z00_6663, BgL_arg1816z00_6664);
										}
										{	/* SawMill/node2rtl.scm 218 */
											bool_t BgL__ortest_1115z00_6666;

											BgL__ortest_1115z00_6666 =
												(BgL_classz00_6656 == BgL_oclassz00_6662);
											if (BgL__ortest_1115z00_6666)
												{	/* SawMill/node2rtl.scm 218 */
													BgL_res2844z00_6661 = BgL__ortest_1115z00_6666;
												}
											else
												{	/* SawMill/node2rtl.scm 218 */
													long BgL_odepthz00_6667;

													{	/* SawMill/node2rtl.scm 218 */
														obj_t BgL_arg1804z00_6668;

														BgL_arg1804z00_6668 = (BgL_oclassz00_6662);
														BgL_odepthz00_6667 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_6668);
													}
													if ((2L < BgL_odepthz00_6667))
														{	/* SawMill/node2rtl.scm 218 */
															obj_t BgL_arg1802z00_6669;

															{	/* SawMill/node2rtl.scm 218 */
																obj_t BgL_arg1803z00_6670;

																BgL_arg1803z00_6670 = (BgL_oclassz00_6662);
																BgL_arg1802z00_6669 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6670,
																	2L);
															}
															BgL_res2844z00_6661 =
																(BgL_arg1802z00_6669 == BgL_classz00_6656);
														}
													else
														{	/* SawMill/node2rtl.scm 218 */
															BgL_res2844z00_6661 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test3186z00_9623 = BgL_res2844z00_6661;
								}
						}
					}
				}
				if (BgL_test3186z00_9623)
					{	/* SawMill/node2rtl.scm 219 */
						BgL_rtl_movz00_bglt BgL_arg2212z00_6671;
						BgL_rtl_regz00_bglt BgL_arg2213z00_6672;

						{	/* SawMill/node2rtl.scm 219 */
							BgL_rtl_movz00_bglt BgL_new1261z00_6673;

							{	/* SawMill/node2rtl.scm 219 */
								BgL_rtl_movz00_bglt BgL_new1260z00_6674;

								BgL_new1260z00_6674 =
									((BgL_rtl_movz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_rtl_movz00_bgl))));
								{	/* SawMill/node2rtl.scm 219 */
									long BgL_arg2216z00_6675;

									BgL_arg2216z00_6675 =
										BGL_CLASS_NUM(BGl_rtl_movz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1260z00_6674),
										BgL_arg2216z00_6675);
								}
								BgL_new1261z00_6673 = BgL_new1260z00_6674;
							}
							((((BgL_rtl_funz00_bglt) COBJECT(
											((BgL_rtl_funz00_bglt) BgL_new1261z00_6673)))->
									BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
							BgL_arg2212z00_6671 = BgL_new1261z00_6673;
						}
						{	/* SawMill/node2rtl.scm 219 */
							BgL_variablez00_bglt BgL_arg2217z00_6676;

							BgL_arg2217z00_6676 =
								(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_ez00_6021)))->BgL_variablez00);
							BgL_arg2213z00_6672 =
								BGl_localzd2ze3regz31zzsaw_node2rtlz00(
								((BgL_localz00_bglt) BgL_arg2217z00_6676));
						}
						{	/* SawMill/node2rtl.scm 219 */
							obj_t BgL_list2215z00_6677;

							BgL_list2215z00_6677 =
								MAKE_YOUNG_PAIR(((obj_t) BgL_arg2213z00_6672), BNIL);
							return
								BGl_singleza2za2zzsaw_node2rtlz00(
								((obj_t)
									((BgL_varz00_bglt) BgL_ez00_6021)),
								((BgL_rtl_funz00_bglt) BgL_arg2212z00_6671),
								BgL_list2215z00_6677);
						}
					}
				else
					{	/* SawMill/node2rtl.scm 220 */
						bool_t BgL_test3190z00_9665;

						{	/* SawMill/node2rtl.scm 220 */
							BgL_valuez00_bglt BgL_arg2237z00_6678;

							BgL_arg2237z00_6678 =
								(((BgL_variablez00_bglt) COBJECT(
										(((BgL_varz00_bglt) COBJECT(
													((BgL_varz00_bglt) BgL_ez00_6021)))->
											BgL_variablez00)))->BgL_valuez00);
							{	/* SawMill/node2rtl.scm 220 */
								obj_t BgL_classz00_6679;

								BgL_classz00_6679 = BGl_sfunz00zzast_varz00;
								{	/* SawMill/node2rtl.scm 220 */
									BgL_objectz00_bglt BgL_arg1807z00_6680;

									{	/* SawMill/node2rtl.scm 220 */
										obj_t BgL_tmpz00_9669;

										BgL_tmpz00_9669 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg2237z00_6678));
										BgL_arg1807z00_6680 =
											(BgL_objectz00_bglt) (BgL_tmpz00_9669);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawMill/node2rtl.scm 220 */
											long BgL_idxz00_6681;

											BgL_idxz00_6681 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6680);
											BgL_test3190z00_9665 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_6681 + 3L)) == BgL_classz00_6679);
										}
									else
										{	/* SawMill/node2rtl.scm 220 */
											bool_t BgL_res2845z00_6684;

											{	/* SawMill/node2rtl.scm 220 */
												obj_t BgL_oclassz00_6685;

												{	/* SawMill/node2rtl.scm 220 */
													obj_t BgL_arg1815z00_6686;
													long BgL_arg1816z00_6687;

													BgL_arg1815z00_6686 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/node2rtl.scm 220 */
														long BgL_arg1817z00_6688;

														BgL_arg1817z00_6688 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6680);
														BgL_arg1816z00_6687 =
															(BgL_arg1817z00_6688 - OBJECT_TYPE);
													}
													BgL_oclassz00_6685 =
														VECTOR_REF(BgL_arg1815z00_6686,
														BgL_arg1816z00_6687);
												}
												{	/* SawMill/node2rtl.scm 220 */
													bool_t BgL__ortest_1115z00_6689;

													BgL__ortest_1115z00_6689 =
														(BgL_classz00_6679 == BgL_oclassz00_6685);
													if (BgL__ortest_1115z00_6689)
														{	/* SawMill/node2rtl.scm 220 */
															BgL_res2845z00_6684 = BgL__ortest_1115z00_6689;
														}
													else
														{	/* SawMill/node2rtl.scm 220 */
															long BgL_odepthz00_6690;

															{	/* SawMill/node2rtl.scm 220 */
																obj_t BgL_arg1804z00_6691;

																BgL_arg1804z00_6691 = (BgL_oclassz00_6685);
																BgL_odepthz00_6690 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_6691);
															}
															if ((3L < BgL_odepthz00_6690))
																{	/* SawMill/node2rtl.scm 220 */
																	obj_t BgL_arg1802z00_6692;

																	{	/* SawMill/node2rtl.scm 220 */
																		obj_t BgL_arg1803z00_6693;

																		BgL_arg1803z00_6693 = (BgL_oclassz00_6685);
																		BgL_arg1802z00_6692 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_6693, 3L);
																	}
																	BgL_res2845z00_6684 =
																		(BgL_arg1802z00_6692 == BgL_classz00_6679);
																}
															else
																{	/* SawMill/node2rtl.scm 220 */
																	BgL_res2845z00_6684 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test3190z00_9665 = BgL_res2845z00_6684;
										}
								}
							}
						}
						if (BgL_test3190z00_9665)
							{	/* SawMill/node2rtl.scm 220 */
								{	/* SawMill/node2rtl.scm 221 */
									obj_t BgL_arg2221z00_6694;
									obj_t BgL_arg2222z00_6695;

									{	/* SawMill/node2rtl.scm 221 */
										obj_t BgL_tmpz00_9692;

										BgL_tmpz00_9692 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_arg2221z00_6694 =
											BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_9692);
									}
									BgL_arg2222z00_6695 =
										BGl_shapez00zztools_shapez00(
										((obj_t) ((BgL_varz00_bglt) BgL_ez00_6021)));
									{	/* SawMill/node2rtl.scm 221 */
										obj_t BgL_list2223z00_6696;

										{	/* SawMill/node2rtl.scm 221 */
											obj_t BgL_arg2224z00_6697;

											{	/* SawMill/node2rtl.scm 221 */
												obj_t BgL_arg2225z00_6698;

												{	/* SawMill/node2rtl.scm 221 */
													obj_t BgL_arg2226z00_6699;

													{	/* SawMill/node2rtl.scm 221 */
														obj_t BgL_arg2227z00_6700;

														{	/* SawMill/node2rtl.scm 221 */
															obj_t BgL_arg2228z00_6701;

															BgL_arg2228z00_6701 =
																MAKE_YOUNG_PAIR(BgL_arg2222z00_6695, BNIL);
															BgL_arg2227z00_6700 =
																MAKE_YOUNG_PAIR
																(BGl_string2942z00zzsaw_node2rtlz00,
																BgL_arg2228z00_6701);
														}
														BgL_arg2226z00_6699 =
															MAKE_YOUNG_PAIR
															(BGl_string2939z00zzsaw_node2rtlz00,
															BgL_arg2227z00_6700);
													}
													BgL_arg2225z00_6698 =
														MAKE_YOUNG_PAIR(BINT(221L), BgL_arg2226z00_6699);
												}
												BgL_arg2224z00_6697 =
													MAKE_YOUNG_PAIR(BGl_string2940z00zzsaw_node2rtlz00,
													BgL_arg2225z00_6698);
											}
											BgL_list2223z00_6696 =
												MAKE_YOUNG_PAIR(BGl_string2941z00zzsaw_node2rtlz00,
												BgL_arg2224z00_6697);
										}
										BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg2221z00_6694,
											BgL_list2223z00_6696);
									}
								}
								{	/* SawMill/node2rtl.scm 222 */
									BgL_rtl_loadfunz00_bglt BgL_arg2229z00_6702;

									{	/* SawMill/node2rtl.scm 222 */
										BgL_rtl_loadfunz00_bglt BgL_new1263z00_6703;

										{	/* SawMill/node2rtl.scm 222 */
											BgL_rtl_loadfunz00_bglt BgL_new1262z00_6704;

											BgL_new1262z00_6704 =
												((BgL_rtl_loadfunz00_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_rtl_loadfunz00_bgl))));
											{	/* SawMill/node2rtl.scm 222 */
												long BgL_arg2232z00_6705;

												BgL_arg2232z00_6705 =
													BGL_CLASS_NUM(BGl_rtl_loadfunz00zzsaw_defsz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1262z00_6704),
													BgL_arg2232z00_6705);
											}
											BgL_new1263z00_6703 = BgL_new1262z00_6704;
										}
										((((BgL_rtl_funz00_bglt) COBJECT(
														((BgL_rtl_funz00_bglt) BgL_new1263z00_6703)))->
												BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
										{
											BgL_globalz00_bglt BgL_auxz00_9712;

											{	/* SawMill/node2rtl.scm 222 */
												BgL_variablez00_bglt BgL_arg2231z00_6706;

												BgL_arg2231z00_6706 =
													(((BgL_varz00_bglt) COBJECT(
															((BgL_varz00_bglt) BgL_ez00_6021)))->
													BgL_variablez00);
												BgL_auxz00_9712 =
													BGl_getzd2globalzd2zzsaw_node2rtlz00((
														(BgL_globalz00_bglt) BgL_arg2231z00_6706));
											}
											((((BgL_rtl_loadfunz00_bglt)
														COBJECT(BgL_new1263z00_6703))->BgL_varz00) =
												((BgL_globalz00_bglt) BgL_auxz00_9712), BUNSPEC);
										}
										BgL_arg2229z00_6702 = BgL_new1263z00_6703;
									}
									return
										BGl_singleza2za2zzsaw_node2rtlz00(
										((obj_t)
											((BgL_varz00_bglt) BgL_ez00_6021)),
										((BgL_rtl_funz00_bglt) BgL_arg2229z00_6702), BNIL);
								}
							}
						else
							{	/* SawMill/node2rtl.scm 224 */
								BgL_rtl_loadgz00_bglt BgL_arg2233z00_6707;

								{	/* SawMill/node2rtl.scm 224 */
									BgL_rtl_loadgz00_bglt BgL_new1265z00_6708;

									{	/* SawMill/node2rtl.scm 224 */
										BgL_rtl_loadgz00_bglt BgL_new1264z00_6709;

										BgL_new1264z00_6709 =
											((BgL_rtl_loadgz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_rtl_loadgz00_bgl))));
										{	/* SawMill/node2rtl.scm 224 */
											long BgL_arg2236z00_6710;

											BgL_arg2236z00_6710 =
												BGL_CLASS_NUM(BGl_rtl_loadgz00zzsaw_defsz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1264z00_6709),
												BgL_arg2236z00_6710);
										}
										BgL_new1265z00_6708 = BgL_new1264z00_6709;
									}
									((((BgL_rtl_funz00_bglt) COBJECT(
													((BgL_rtl_funz00_bglt) BgL_new1265z00_6708)))->
											BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
									{
										BgL_globalz00_bglt BgL_auxz00_9728;

										{	/* SawMill/node2rtl.scm 224 */
											BgL_variablez00_bglt BgL_arg2235z00_6711;

											BgL_arg2235z00_6711 =
												(((BgL_varz00_bglt) COBJECT(
														((BgL_varz00_bglt) BgL_ez00_6021)))->
												BgL_variablez00);
											BgL_auxz00_9728 =
												BGl_getzd2globalzd2zzsaw_node2rtlz00((
													(BgL_globalz00_bglt) BgL_arg2235z00_6711));
										}
										((((BgL_rtl_loadgz00_bglt) COBJECT(BgL_new1265z00_6708))->
												BgL_varz00) =
											((BgL_globalz00_bglt) BgL_auxz00_9728), BUNSPEC);
									}
									BgL_arg2233z00_6707 = BgL_new1265z00_6708;
								}
								return
									BGl_singleza2za2zzsaw_node2rtlz00(
									((obj_t)
										((BgL_varz00_bglt) BgL_ez00_6021)),
									((BgL_rtl_funz00_bglt) BgL_arg2233z00_6707), BNIL);
							}
					}
			}
		}

	}



/* &node->rtl-atom1719 */
	BgL_areaz00_bglt BGl_z62nodezd2ze3rtlzd2atom1719z81zzsaw_node2rtlz00(obj_t
		BgL_envz00_6022, obj_t BgL_ez00_6023)
	{
		{	/* SawMill/node2rtl.scm 210 */
			{	/* SawMill/node2rtl.scm 211 */
				BgL_rtl_loadiz00_bglt BgL_arg2190z00_6713;

				{	/* SawMill/node2rtl.scm 211 */
					BgL_rtl_loadiz00_bglt BgL_new1258z00_6714;

					{	/* SawMill/node2rtl.scm 211 */
						BgL_rtl_loadiz00_bglt BgL_new1257z00_6715;

						BgL_new1257z00_6715 =
							((BgL_rtl_loadiz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_loadiz00_bgl))));
						{	/* SawMill/node2rtl.scm 211 */
							long BgL_arg2192z00_6716;

							BgL_arg2192z00_6716 =
								BGL_CLASS_NUM(BGl_rtl_loadiz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1257z00_6715),
								BgL_arg2192z00_6716);
						}
						BgL_new1258z00_6714 = BgL_new1257z00_6715;
					}
					((((BgL_rtl_funz00_bglt) COBJECT(
									((BgL_rtl_funz00_bglt) BgL_new1258z00_6714)))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_rtl_loadiz00_bglt) COBJECT(BgL_new1258z00_6714))->
							BgL_constantz00) =
						((BgL_atomz00_bglt) ((BgL_atomz00_bglt) BgL_ez00_6023)), BUNSPEC);
					BgL_arg2190z00_6713 = BgL_new1258z00_6714;
				}
				return
					BGl_singleza2za2zzsaw_node2rtlz00(
					((obj_t)
						((BgL_atomz00_bglt) BgL_ez00_6023)),
					((BgL_rtl_funz00_bglt) BgL_arg2190z00_6713), BNIL);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_node2rtlz00(void)
	{
		{	/* SawMill/node2rtl.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2943z00zzsaw_node2rtlz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2943z00zzsaw_node2rtlz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2943z00zzsaw_node2rtlz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2943z00zzsaw_node2rtlz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2943z00zzsaw_node2rtlz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2943z00zzsaw_node2rtlz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string2943z00zzsaw_node2rtlz00));
			BGl_modulezd2initializa7ationz75zzsync_nodez00(421078394L,
				BSTRING_TO_STRING(BGl_string2943z00zzsaw_node2rtlz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2943z00zzsaw_node2rtlz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2943z00zzsaw_node2rtlz00));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string2943z00zzsaw_node2rtlz00));
			BGl_modulezd2initializa7ationz75zzbackend_cplibz00(395792377L,
				BSTRING_TO_STRING(BGl_string2943z00zzsaw_node2rtlz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2943z00zzsaw_node2rtlz00));
		}

	}

#ifdef __cplusplus
}
#endif
