/*===========================================================================*/
/*   (SawMill/gotos.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/gotos.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_GOTOS_TYPE_DEFINITIONS
#define BGL_SAW_GOTOS_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_ifz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                *BgL_rtl_ifz00_bglt;

	typedef struct BgL_rtl_selectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_patternsz00;
	}                    *BgL_rtl_selectz00_bglt;

	typedef struct BgL_rtl_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_patternsz00;
		obj_t BgL_labelsz00;
	}                    *BgL_rtl_switchz00_bglt;

	typedef struct BgL_rtl_ifeqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_thenz00;
	}                  *BgL_rtl_ifeqz00_bglt;

	typedef struct BgL_rtl_ifnez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_thenz00;
	}                  *BgL_rtl_ifnez00_bglt;

	typedef struct BgL_rtl_goz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_toz00;
	}                *BgL_rtl_goz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;


#endif													// BGL_SAW_GOTOS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_gotosz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl__ifeqz12z12zzsaw_gotosz00(BgL_rtl_insz00_bglt,
		BgL_blockz00_bglt);
	extern obj_t BGl_rtl_ifeqz00zzsaw_defsz00;
	static obj_t BGl_genericzd2initzd2zzsaw_gotosz00(void);
	static obj_t BGl_objectzd2initzd2zzsaw_gotosz00(void);
	static obj_t BGl__ifz00zzsaw_gotosz00(BgL_rtl_ifz00_bglt, BgL_rtl_insz00_bglt,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl__ifnez12z12zzsaw_gotosz00(BgL_rtl_insz00_bglt,
		BgL_blockz00_bglt);
	static obj_t BGl_z62addzd2gotoszb0zzsaw_gotosz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_gotosz00(void);
	extern obj_t BGl_rtl_switchz00zzsaw_defsz00;
	extern obj_t BGl_rtl_goz00zzsaw_defsz00;
	extern obj_t BGl_rtl_ifnez00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t BGl_addzd2gotoszd2zzsaw_gotosz00(obj_t);
	extern obj_t BGl_rtl_ifz00zzsaw_defsz00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_selectz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzsaw_gotosz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl__switchz12z12zzsaw_gotosz00(BgL_rtl_selectz00_bglt,
		BgL_rtl_insz00_bglt, obj_t);
	static BgL_rtl_insz00_bglt BGl__gotoz00zzsaw_gotosz00(BgL_blockz00_bglt);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_gotosz00(void);
	static obj_t BGl_makezd2gotozd2zzsaw_gotosz00(BgL_blockz00_bglt, obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_gotosz00(void);
	static obj_t BGl_addzd2gotozd2zzsaw_gotosz00(BgL_blockz00_bglt, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_gotosz00(void);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1862z00zzsaw_gotosz00,
		BgL_bgl_string1862za700za7za7s1865za7, "add-goto", 8);
	      DEFINE_STRING(BGl_string1863z00zzsaw_gotosz00,
		BgL_bgl_string1863za700za7za7s1866za7, "CARE", 4);
	      DEFINE_STRING(BGl_string1864z00zzsaw_gotosz00,
		BgL_bgl_string1864za700za7za7s1867za7, "saw_gotos", 9);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_addzd2gotoszd2envz00zzsaw_gotosz00,
		BgL_bgl_za762addza7d2gotosza7b1868za7, BGl_z62addzd2gotoszb0zzsaw_gotosz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_gotosz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_gotosz00(long
		BgL_checksumz00_2355, char *BgL_fromz00_2356)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_gotosz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_gotosz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_gotosz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_gotosz00();
					BGl_importedzd2moduleszd2initz00zzsaw_gotosz00();
					return BGl_methodzd2initzd2zzsaw_gotosz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_gotosz00(void)
	{
		{	/* SawMill/gotos.scm 1 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_gotos");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_gotos");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_gotos");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_gotos");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_gotos");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_gotos");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_gotos");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_gotosz00(void)
	{
		{	/* SawMill/gotos.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* add-gotos */
	BGL_EXPORTED_DEF obj_t BGl_addzd2gotoszd2zzsaw_gotosz00(obj_t BgL_lz00_3)
	{
		{	/* SawMill/gotos.scm 9 */
			{
				obj_t BgL_lz00_1811;

				BgL_lz00_1811 = BgL_lz00_3;
			BgL_zc3z04anonymousza31411ze3z87_1812:
				if (PAIRP(BgL_lz00_1811))
					{	/* SawMill/gotos.scm 12 */
						obj_t BgL_rz00_1814;

						BgL_rz00_1814 = CDR(BgL_lz00_1811);
						{	/* SawMill/gotos.scm 13 */
							obj_t BgL_arg1421z00_1815;

							BgL_arg1421z00_1815 = CAR(BgL_lz00_1811);
							BGl_addzd2gotozd2zzsaw_gotosz00(
								((BgL_blockz00_bglt) BgL_arg1421z00_1815), BgL_rz00_1814);
						}
						{
							obj_t BgL_lz00_2378;

							BgL_lz00_2378 = BgL_rz00_1814;
							BgL_lz00_1811 = BgL_lz00_2378;
							goto BgL_zc3z04anonymousza31411ze3z87_1812;
						}
					}
				else
					{	/* SawMill/gotos.scm 11 */
						return BUNSPEC;
					}
			}
		}

	}



/* &add-gotos */
	obj_t BGl_z62addzd2gotoszb0zzsaw_gotosz00(obj_t BgL_envz00_2353,
		obj_t BgL_lz00_2354)
	{
		{	/* SawMill/gotos.scm 9 */
			return BGl_addzd2gotoszd2zzsaw_gotosz00(BgL_lz00_2354);
		}

	}



/* add-goto */
	obj_t BGl_addzd2gotozd2zzsaw_gotosz00(BgL_blockz00_bglt BgL_bz00_4,
		obj_t BgL_lz00_5)
	{
		{	/* SawMill/gotos.scm 20 */
			{	/* SawMill/gotos.scm 21 */
				obj_t BgL_lastz00_1817;

				BgL_lastz00_1817 =
					BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
					(((BgL_blockz00_bglt) COBJECT(BgL_bz00_4))->BgL_firstz00));
				{	/* SawMill/gotos.scm 22 */
					obj_t BgL_addedz00_1818;

					BgL_addedz00_1818 =
						BGl_makezd2gotozd2zzsaw_gotosz00(BgL_bz00_4,
						CAR(BgL_lastz00_1817), BgL_lz00_5);
					if (NULLP(BgL_addedz00_1818))
						{	/* SawMill/gotos.scm 23 */
							return BFALSE;
						}
					else
						{	/* SawMill/gotos.scm 23 */
							return SET_CDR(BgL_lastz00_1817, BgL_addedz00_1818);
						}
				}
			}
		}

	}



/* make-goto */
	obj_t BGl_makezd2gotozd2zzsaw_gotosz00(BgL_blockz00_bglt BgL_bz00_6,
		obj_t BgL_insz00_7, obj_t BgL_lz00_8)
	{
		{	/* SawMill/gotos.scm 29 */
			if (NULLP((((BgL_blockz00_bglt) COBJECT(BgL_bz00_6))->BgL_succsz00)))
				{	/* SawMill/gotos.scm 31 */
					return BNIL;
				}
			else
				{	/* SawMill/gotos.scm 31 */
					if (NULLP(CDR(
								(((BgL_blockz00_bglt) COBJECT(BgL_bz00_6))->BgL_succsz00))))
						{	/* SawMill/gotos.scm 34 */
							obj_t BgL_sz00_1828;

							BgL_sz00_1828 =
								CAR((((BgL_blockz00_bglt) COBJECT(BgL_bz00_6))->BgL_succsz00));
							{	/* SawMill/gotos.scm 35 */
								bool_t BgL_test1874z00_2396;

								if (NULLP(BgL_lz00_8))
									{	/* SawMill/gotos.scm 35 */
										BgL_test1874z00_2396 = ((bool_t) 1);
									}
								else
									{	/* SawMill/gotos.scm 35 */
										if ((CAR(BgL_lz00_8) == BgL_sz00_1828))
											{	/* SawMill/gotos.scm 35 */
												BgL_test1874z00_2396 = ((bool_t) 0);
											}
										else
											{	/* SawMill/gotos.scm 35 */
												BgL_test1874z00_2396 = ((bool_t) 1);
											}
									}
								if (BgL_test1874z00_2396)
									{	/* SawMill/gotos.scm 36 */
										BgL_rtl_insz00_bglt BgL_arg1489z00_1834;

										BgL_arg1489z00_1834 =
											BGl__gotoz00zzsaw_gotosz00(
											((BgL_blockz00_bglt) BgL_sz00_1828));
										return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1489z00_1834), BNIL);
									}
								else
									{	/* SawMill/gotos.scm 35 */
										return BNIL;
									}
							}
						}
					else
						{	/* SawMill/gotos.scm 41 */
							bool_t BgL_test1877z00_2406;

							{	/* SawMill/gotos.scm 41 */
								BgL_rtl_funz00_bglt BgL_arg1553z00_1849;

								BgL_arg1553z00_1849 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_insz00_7)))->BgL_funz00);
								{	/* SawMill/gotos.scm 41 */
									obj_t BgL_classz00_2188;

									BgL_classz00_2188 = BGl_rtl_selectz00zzsaw_defsz00;
									{	/* SawMill/gotos.scm 41 */
										BgL_objectz00_bglt BgL_arg1807z00_2190;

										{	/* SawMill/gotos.scm 41 */
											obj_t BgL_tmpz00_2409;

											BgL_tmpz00_2409 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg1553z00_1849));
											BgL_arg1807z00_2190 =
												(BgL_objectz00_bglt) (BgL_tmpz00_2409);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawMill/gotos.scm 41 */
												long BgL_idxz00_2196;

												BgL_idxz00_2196 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2190);
												BgL_test1877z00_2406 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_2196 + 3L)) == BgL_classz00_2188);
											}
										else
											{	/* SawMill/gotos.scm 41 */
												bool_t BgL_res1858z00_2221;

												{	/* SawMill/gotos.scm 41 */
													obj_t BgL_oclassz00_2204;

													{	/* SawMill/gotos.scm 41 */
														obj_t BgL_arg1815z00_2212;
														long BgL_arg1816z00_2213;

														BgL_arg1815z00_2212 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawMill/gotos.scm 41 */
															long BgL_arg1817z00_2214;

															BgL_arg1817z00_2214 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2190);
															BgL_arg1816z00_2213 =
																(BgL_arg1817z00_2214 - OBJECT_TYPE);
														}
														BgL_oclassz00_2204 =
															VECTOR_REF(BgL_arg1815z00_2212,
															BgL_arg1816z00_2213);
													}
													{	/* SawMill/gotos.scm 41 */
														bool_t BgL__ortest_1115z00_2205;

														BgL__ortest_1115z00_2205 =
															(BgL_classz00_2188 == BgL_oclassz00_2204);
														if (BgL__ortest_1115z00_2205)
															{	/* SawMill/gotos.scm 41 */
																BgL_res1858z00_2221 = BgL__ortest_1115z00_2205;
															}
														else
															{	/* SawMill/gotos.scm 41 */
																long BgL_odepthz00_2206;

																{	/* SawMill/gotos.scm 41 */
																	obj_t BgL_arg1804z00_2207;

																	BgL_arg1804z00_2207 = (BgL_oclassz00_2204);
																	BgL_odepthz00_2206 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_2207);
																}
																if ((3L < BgL_odepthz00_2206))
																	{	/* SawMill/gotos.scm 41 */
																		obj_t BgL_arg1802z00_2209;

																		{	/* SawMill/gotos.scm 41 */
																			obj_t BgL_arg1803z00_2210;

																			BgL_arg1803z00_2210 =
																				(BgL_oclassz00_2204);
																			BgL_arg1802z00_2209 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_2210, 3L);
																		}
																		BgL_res1858z00_2221 =
																			(BgL_arg1802z00_2209 ==
																			BgL_classz00_2188);
																	}
																else
																	{	/* SawMill/gotos.scm 41 */
																		BgL_res1858z00_2221 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test1877z00_2406 = BgL_res1858z00_2221;
											}
									}
								}
							}
							if (BgL_test1877z00_2406)
								{	/* SawMill/gotos.scm 41 */
									{	/* SawMill/gotos.scm 42 */
										BgL_rtl_funz00_bglt BgL_arg1516z00_1842;
										obj_t BgL_arg1535z00_1843;

										BgL_arg1516z00_1842 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_insz00_7)))->BgL_funz00);
										BgL_arg1535z00_1843 =
											(((BgL_blockz00_bglt) COBJECT(BgL_bz00_6))->BgL_succsz00);
										BGl__switchz12z12zzsaw_gotosz00(
											((BgL_rtl_selectz00_bglt) BgL_arg1516z00_1842),
											((BgL_rtl_insz00_bglt) BgL_insz00_7),
											BgL_arg1535z00_1843);
									}
									return BNIL;
								}
							else
								{	/* SawMill/gotos.scm 44 */
									bool_t BgL_test1881z00_2438;

									{	/* SawMill/gotos.scm 44 */
										BgL_rtl_funz00_bglt BgL_arg1552z00_1848;

										BgL_arg1552z00_1848 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_insz00_7)))->BgL_funz00);
										{	/* SawMill/gotos.scm 44 */
											obj_t BgL_classz00_2222;

											BgL_classz00_2222 = BGl_rtl_ifz00zzsaw_defsz00;
											{	/* SawMill/gotos.scm 44 */
												BgL_objectz00_bglt BgL_arg1807z00_2224;

												{	/* SawMill/gotos.scm 44 */
													obj_t BgL_tmpz00_2441;

													BgL_tmpz00_2441 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_arg1552z00_1848));
													BgL_arg1807z00_2224 =
														(BgL_objectz00_bglt) (BgL_tmpz00_2441);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawMill/gotos.scm 44 */
														long BgL_idxz00_2230;

														BgL_idxz00_2230 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2224);
														BgL_test1881z00_2438 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2230 + 3L)) == BgL_classz00_2222);
													}
												else
													{	/* SawMill/gotos.scm 44 */
														bool_t BgL_res1859z00_2255;

														{	/* SawMill/gotos.scm 44 */
															obj_t BgL_oclassz00_2238;

															{	/* SawMill/gotos.scm 44 */
																obj_t BgL_arg1815z00_2246;
																long BgL_arg1816z00_2247;

																BgL_arg1815z00_2246 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawMill/gotos.scm 44 */
																	long BgL_arg1817z00_2248;

																	BgL_arg1817z00_2248 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2224);
																	BgL_arg1816z00_2247 =
																		(BgL_arg1817z00_2248 - OBJECT_TYPE);
																}
																BgL_oclassz00_2238 =
																	VECTOR_REF(BgL_arg1815z00_2246,
																	BgL_arg1816z00_2247);
															}
															{	/* SawMill/gotos.scm 44 */
																bool_t BgL__ortest_1115z00_2239;

																BgL__ortest_1115z00_2239 =
																	(BgL_classz00_2222 == BgL_oclassz00_2238);
																if (BgL__ortest_1115z00_2239)
																	{	/* SawMill/gotos.scm 44 */
																		BgL_res1859z00_2255 =
																			BgL__ortest_1115z00_2239;
																	}
																else
																	{	/* SawMill/gotos.scm 44 */
																		long BgL_odepthz00_2240;

																		{	/* SawMill/gotos.scm 44 */
																			obj_t BgL_arg1804z00_2241;

																			BgL_arg1804z00_2241 =
																				(BgL_oclassz00_2238);
																			BgL_odepthz00_2240 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2241);
																		}
																		if ((3L < BgL_odepthz00_2240))
																			{	/* SawMill/gotos.scm 44 */
																				obj_t BgL_arg1802z00_2243;

																				{	/* SawMill/gotos.scm 44 */
																					obj_t BgL_arg1803z00_2244;

																					BgL_arg1803z00_2244 =
																						(BgL_oclassz00_2238);
																					BgL_arg1802z00_2243 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2244, 3L);
																				}
																				BgL_res1859z00_2255 =
																					(BgL_arg1802z00_2243 ==
																					BgL_classz00_2222);
																			}
																		else
																			{	/* SawMill/gotos.scm 44 */
																				BgL_res1859z00_2255 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1881z00_2438 = BgL_res1859z00_2255;
													}
											}
										}
									}
									if (BgL_test1881z00_2438)
										{	/* SawMill/gotos.scm 45 */
											BgL_rtl_funz00_bglt BgL_arg1544z00_1846;
											obj_t BgL_arg1546z00_1847;

											BgL_arg1544z00_1846 =
												(((BgL_rtl_insz00_bglt) COBJECT(
														((BgL_rtl_insz00_bglt) BgL_insz00_7)))->BgL_funz00);
											BgL_arg1546z00_1847 =
												(((BgL_blockz00_bglt) COBJECT(BgL_bz00_6))->
												BgL_succsz00);
											return BGl__ifz00zzsaw_gotosz00(((BgL_rtl_ifz00_bglt)
													BgL_arg1544z00_1846),
												((BgL_rtl_insz00_bglt) BgL_insz00_7),
												BgL_arg1546z00_1847, BgL_lz00_8);
										}
									else
										{	/* SawMill/gotos.scm 44 */
											return
												BGl_errorz00zz__errorz00
												(BGl_string1862z00zzsaw_gotosz00,
												BGl_string1863z00zzsaw_gotosz00, BgL_insz00_7);
										}
								}
						}
				}
		}

	}



/* _goto */
	BgL_rtl_insz00_bglt BGl__gotoz00zzsaw_gotosz00(BgL_blockz00_bglt BgL_bz00_9)
	{
		{	/* SawMill/gotos.scm 49 */
			{	/* SawMill/gotos.scm 50 */
				BgL_rtl_insz00_bglt BgL_new1151z00_1853;

				{	/* SawMill/gotos.scm 50 */
					BgL_rtl_insz00_bglt BgL_new1150z00_1857;

					BgL_new1150z00_1857 =
						((BgL_rtl_insz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_insz00_bgl))));
					{	/* SawMill/gotos.scm 50 */
						long BgL_arg1571z00_1858;

						BgL_arg1571z00_1858 = BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1150z00_1857), BgL_arg1571z00_1858);
					}
					{	/* SawMill/gotos.scm 50 */
						BgL_objectz00_bglt BgL_tmpz00_2475;

						BgL_tmpz00_2475 = ((BgL_objectz00_bglt) BgL_new1150z00_1857);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2475, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1150z00_1857);
					BgL_new1151z00_1853 = BgL_new1150z00_1857;
				}
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1151z00_1853))->BgL_locz00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1151z00_1853))->
						BgL_z52spillz52) = ((obj_t) BNIL), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1151z00_1853))->BgL_destz00) =
					((obj_t) BFALSE), BUNSPEC);
				{
					BgL_rtl_funz00_bglt BgL_auxz00_2482;

					{	/* SawMill/gotos.scm 51 */
						BgL_rtl_goz00_bglt BgL_new1153z00_1854;

						{	/* SawMill/gotos.scm 51 */
							BgL_rtl_goz00_bglt BgL_new1152z00_1855;

							BgL_new1152z00_1855 =
								((BgL_rtl_goz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_goz00_bgl))));
							{	/* SawMill/gotos.scm 51 */
								long BgL_arg1565z00_1856;

								{	/* SawMill/gotos.scm 51 */
									obj_t BgL_classz00_2260;

									BgL_classz00_2260 = BGl_rtl_goz00zzsaw_defsz00;
									BgL_arg1565z00_1856 = BGL_CLASS_NUM(BgL_classz00_2260);
								}
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1152z00_1855),
									BgL_arg1565z00_1856);
							}
							BgL_new1153z00_1854 = BgL_new1152z00_1855;
						}
						((((BgL_rtl_funz00_bglt) COBJECT(
										((BgL_rtl_funz00_bglt) BgL_new1153z00_1854)))->BgL_locz00) =
							((obj_t) BFALSE), BUNSPEC);
						((((BgL_rtl_goz00_bglt) COBJECT(BgL_new1153z00_1854))->BgL_toz00) =
							((BgL_blockz00_bglt) BgL_bz00_9), BUNSPEC);
						BgL_auxz00_2482 = ((BgL_rtl_funz00_bglt) BgL_new1153z00_1854);
					}
					((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1151z00_1853))->BgL_funz00) =
						((BgL_rtl_funz00_bglt) BgL_auxz00_2482), BUNSPEC);
				}
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1151z00_1853))->BgL_argsz00) =
					((obj_t) BNIL), BUNSPEC);
				return BgL_new1151z00_1853;
			}
		}

	}



/* _switch! */
	obj_t BGl__switchz12z12zzsaw_gotosz00(BgL_rtl_selectz00_bglt BgL_funz00_10,
		BgL_rtl_insz00_bglt BgL_insz00_11, obj_t BgL_succsz00_12)
	{
		{	/* SawMill/gotos.scm 55 */
			{	/* SawMill/gotos.scm 58 */
				BgL_rtl_switchz00_bglt BgL_arg1573z00_1860;

				{	/* SawMill/gotos.scm 58 */
					BgL_rtl_switchz00_bglt BgL_new1157z00_1861;

					{	/* SawMill/gotos.scm 58 */
						BgL_rtl_switchz00_bglt BgL_new1156z00_1862;

						BgL_new1156z00_1862 =
							((BgL_rtl_switchz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_switchz00_bgl))));
						{	/* SawMill/gotos.scm 58 */
							long BgL_arg1575z00_1863;

							BgL_arg1575z00_1863 =
								BGL_CLASS_NUM(BGl_rtl_switchz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1156z00_1862),
								BgL_arg1575z00_1863);
						}
						BgL_new1157z00_1861 = BgL_new1156z00_1862;
					}
					((((BgL_rtl_funz00_bglt) COBJECT(
									((BgL_rtl_funz00_bglt) BgL_new1157z00_1861)))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_rtl_selectz00_bglt) COBJECT(((BgL_rtl_selectz00_bglt)
										BgL_new1157z00_1861)))->BgL_typez00) =
						((BgL_typez00_bglt) (((BgL_rtl_selectz00_bglt)
									COBJECT(BgL_funz00_10))->BgL_typez00)), BUNSPEC);
					((((BgL_rtl_selectz00_bglt) COBJECT(((BgL_rtl_selectz00_bglt)
										BgL_new1157z00_1861)))->BgL_patternsz00) =
						((obj_t) (((BgL_rtl_selectz00_bglt) COBJECT(BgL_funz00_10))->
								BgL_patternsz00)), BUNSPEC);
					((((BgL_rtl_switchz00_bglt) COBJECT(BgL_new1157z00_1861))->
							BgL_labelsz00) = ((obj_t) BgL_succsz00_12), BUNSPEC);
					BgL_arg1573z00_1860 = BgL_new1157z00_1861;
				}
				return
					((((BgL_rtl_insz00_bglt) COBJECT(BgL_insz00_11))->BgL_funz00) =
					((BgL_rtl_funz00_bglt) ((BgL_rtl_funz00_bglt) BgL_arg1573z00_1860)),
					BUNSPEC);
			}
		}

	}



/* _if */
	obj_t BGl__ifz00zzsaw_gotosz00(BgL_rtl_ifz00_bglt BgL_funz00_13,
		BgL_rtl_insz00_bglt BgL_insz00_14, obj_t BgL_succsz00_15,
		obj_t BgL_nextsz00_16)
	{
		{	/* SawMill/gotos.scm 64 */
			{	/* SawMill/gotos.scm 65 */
				obj_t BgL_thenz00_1864;
				obj_t BgL_elsez00_1865;

				BgL_thenz00_1864 = CAR(((obj_t) BgL_succsz00_15));
				{	/* SawMill/gotos.scm 65 */
					obj_t BgL_pairz00_2272;

					BgL_pairz00_2272 = CDR(((obj_t) BgL_succsz00_15));
					BgL_elsez00_1865 = CAR(BgL_pairz00_2272);
				}
				if (NULLP(BgL_nextsz00_16))
					{	/* SawMill/gotos.scm 66 */
						BGl__ifeqz12z12zzsaw_gotosz00(BgL_insz00_14,
							((BgL_blockz00_bglt) BgL_elsez00_1865));
						{	/* SawMill/gotos.scm 68 */
							BgL_rtl_insz00_bglt BgL_arg1584z00_1867;

							BgL_arg1584z00_1867 =
								BGl__gotoz00zzsaw_gotosz00(
								((BgL_blockz00_bglt) BgL_thenz00_1864));
							return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1584z00_1867), BNIL);
						}
					}
				else
					{	/* SawMill/gotos.scm 66 */
						if ((BgL_thenz00_1864 == CAR(BgL_nextsz00_16)))
							{	/* SawMill/gotos.scm 69 */
								BGl__ifeqz12z12zzsaw_gotosz00(BgL_insz00_14,
									((BgL_blockz00_bglt) BgL_elsez00_1865));
								return BNIL;
							}
						else
							{	/* SawMill/gotos.scm 69 */
								if ((BgL_elsez00_1865 == CAR(BgL_nextsz00_16)))
									{	/* SawMill/gotos.scm 72 */
										BGl__ifnez12z12zzsaw_gotosz00(BgL_insz00_14,
											((BgL_blockz00_bglt) BgL_thenz00_1864));
										return BNIL;
									}
								else
									{	/* SawMill/gotos.scm 72 */
										BGl__ifeqz12z12zzsaw_gotosz00(BgL_insz00_14,
											((BgL_blockz00_bglt) BgL_elsez00_1865));
										{	/* SawMill/gotos.scm 77 */
											BgL_rtl_insz00_bglt BgL_arg1593z00_1872;

											BgL_arg1593z00_1872 =
												BGl__gotoz00zzsaw_gotosz00(
												((BgL_blockz00_bglt) BgL_thenz00_1864));
											return
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1593z00_1872), BNIL);
										}
									}
							}
					}
			}
		}

	}



/* _ifeq! */
	obj_t BGl__ifeqz12z12zzsaw_gotosz00(BgL_rtl_insz00_bglt BgL_insz00_17,
		BgL_blockz00_bglt BgL_thenz00_18)
	{
		{	/* SawMill/gotos.scm 79 */
			{	/* SawMill/gotos.scm 80 */
				BgL_rtl_ifeqz00_bglt BgL_arg1602z00_1875;

				{	/* SawMill/gotos.scm 80 */
					BgL_rtl_ifeqz00_bglt BgL_new1159z00_1876;

					{	/* SawMill/gotos.scm 80 */
						BgL_rtl_ifeqz00_bglt BgL_new1158z00_1877;

						BgL_new1158z00_1877 =
							((BgL_rtl_ifeqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_ifeqz00_bgl))));
						{	/* SawMill/gotos.scm 80 */
							long BgL_arg1605z00_1878;

							BgL_arg1605z00_1878 = BGL_CLASS_NUM(BGl_rtl_ifeqz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1158z00_1877),
								BgL_arg1605z00_1878);
						}
						BgL_new1159z00_1876 = BgL_new1158z00_1877;
					}
					((((BgL_rtl_funz00_bglt) COBJECT(
									((BgL_rtl_funz00_bglt) BgL_new1159z00_1876)))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_rtl_ifeqz00_bglt) COBJECT(BgL_new1159z00_1876))->
							BgL_thenz00) = ((BgL_blockz00_bglt) BgL_thenz00_18), BUNSPEC);
					BgL_arg1602z00_1875 = BgL_new1159z00_1876;
				}
				return
					((((BgL_rtl_insz00_bglt) COBJECT(BgL_insz00_17))->BgL_funz00) =
					((BgL_rtl_funz00_bglt) ((BgL_rtl_funz00_bglt) BgL_arg1602z00_1875)),
					BUNSPEC);
			}
		}

	}



/* _ifne! */
	obj_t BGl__ifnez12z12zzsaw_gotosz00(BgL_rtl_insz00_bglt BgL_insz00_19,
		BgL_blockz00_bglt BgL_thenz00_20)
	{
		{	/* SawMill/gotos.scm 82 */
			{	/* SawMill/gotos.scm 83 */
				BgL_rtl_ifnez00_bglt BgL_arg1606z00_1879;

				{	/* SawMill/gotos.scm 83 */
					BgL_rtl_ifnez00_bglt BgL_new1161z00_1880;

					{	/* SawMill/gotos.scm 83 */
						BgL_rtl_ifnez00_bglt BgL_new1160z00_1881;

						BgL_new1160z00_1881 =
							((BgL_rtl_ifnez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_ifnez00_bgl))));
						{	/* SawMill/gotos.scm 83 */
							long BgL_arg1609z00_1882;

							BgL_arg1609z00_1882 = BGL_CLASS_NUM(BGl_rtl_ifnez00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1160z00_1881),
								BgL_arg1609z00_1882);
						}
						BgL_new1161z00_1880 = BgL_new1160z00_1881;
					}
					((((BgL_rtl_funz00_bglt) COBJECT(
									((BgL_rtl_funz00_bglt) BgL_new1161z00_1880)))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_rtl_ifnez00_bglt) COBJECT(BgL_new1161z00_1880))->
							BgL_thenz00) = ((BgL_blockz00_bglt) BgL_thenz00_20), BUNSPEC);
					BgL_arg1606z00_1879 = BgL_new1161z00_1880;
				}
				return
					((((BgL_rtl_insz00_bglt) COBJECT(BgL_insz00_19))->BgL_funz00) =
					((BgL_rtl_funz00_bglt) ((BgL_rtl_funz00_bglt) BgL_arg1606z00_1879)),
					BUNSPEC);
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_gotosz00(void)
	{
		{	/* SawMill/gotos.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_gotosz00(void)
	{
		{	/* SawMill/gotos.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_gotosz00(void)
	{
		{	/* SawMill/gotos.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_gotosz00(void)
	{
		{	/* SawMill/gotos.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1864z00zzsaw_gotosz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1864z00zzsaw_gotosz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1864z00zzsaw_gotosz00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string1864z00zzsaw_gotosz00));
		}

	}

#ifdef __cplusplus
}
#endif
