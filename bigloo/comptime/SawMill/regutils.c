/*===========================================================================*/
/*   (SawMill/regutils.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/regutils.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_REGUTILS_TYPE_DEFINITIONS
#define BGL_SAW_REGUTILS_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_rtl_regzf2razf2_bgl
	{
		int BgL_numz00;
		obj_t BgL_colorz00;
		obj_t BgL_coalescez00;
		int BgL_occurrencesz00;
		obj_t BgL_interferez00;
		obj_t BgL_interfere2z00;
	}                      *BgL_rtl_regzf2razf2_bglt;

	typedef struct BgL_regsetz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_lengthz00;
		int BgL_msiza7eza7;
		obj_t BgL_regvz00;
		obj_t BgL_reglz00;
		obj_t BgL_stringz00;
	}                *BgL_regsetz00_bglt;


#endif													// BGL_SAW_REGUTILS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_regutilsz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	static obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_regutilsz00(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_regutilsz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_regutilsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_regutilsz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_regutilsz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_collectzd2registersz12zc0zzsaw_regutilsz00(obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_regutilsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_makezd2regsetzd2zzsaw_regutilsz00(int, int, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_regutilsz00
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_regutilsz00(BgL_typez00_bglt, obj_t, obj_t,
		obj_t, obj_t, obj_t, int, obj_t, obj_t, int, obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62makezd2rtl_regzf2raz42zzsaw_regutilsz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzsaw_regutilsz00(void);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_regutilsz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62regsetzd2lengthzb0zzsaw_regutilsz00(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2reglzb0zzsaw_regutilsz00(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2regvzb0zzsaw_regutilsz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_regutilsz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_regutilsz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_regutilsz00(obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_z62rtl_regzf2razd2nilz42zzsaw_regutilsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_regutilsz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_objectzd2initzd2zzsaw_regutilsz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_regutilsz00(BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_regutilsz00(BgL_rtl_regz00_bglt,
		obj_t);
	extern obj_t BGl_regsetz00zzsaw_regsetz00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	static obj_t BGl_z62argszd2collectzd2registersz12z70zzsaw_regutilsz00(obj_t,
		obj_t);
	static obj_t BGl_z62regsetzf3z91zzsaw_regutilsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_regutilsz00(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_regutilsz00(void);
	static obj_t
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_regutilsz00(obj_t, obj_t,
		obj_t);
	static BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_regutilsz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_regutilsz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_regutilsz00(void);
	static BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_regutilsz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_regutilsz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_regutilsz00(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2stringzb0zzsaw_regutilsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_collectzd2registerz12zc0zzsaw_regutilsz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2msiza7ez75zzsaw_regutilsz00(BgL_regsetz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_regutilsz00(obj_t,
		obj_t);
	static obj_t BGl_z62blockzd2collectzd2registersz12z70zzsaw_regutilsz00(obj_t,
		obj_t);
	static obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_regutilsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2lengthzd2zzsaw_regutilsz00(BgL_regsetz00_bglt);
	BGL_IMPORT obj_t BGl_appendzd2mapz12zc0zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_regutilsz00(BgL_regsetz00_bglt, obj_t);
	static long BGl_za2registerzd2countza2zd2zzsaw_regutilsz00 = 0L;
	static obj_t BGl_argszd2collectzd2registersz12ze70zf5zzsaw_regutilsz00(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_regutilsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_node2rtlz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_regutilsz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_regutilsz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62inszd2collectzd2registersz12z70zzsaw_regutilsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_regzf2razf3z01zzsaw_regutilsz00(obj_t);
	static obj_t BGl_z62collectzd2registersz12za2zzsaw_regutilsz00(obj_t, obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_regutilsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_regutilsz00(BgL_rtl_regz00_bglt,
		obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_regutilsz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_regutilsz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_regutilsz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_regutilsz00
		(BgL_rtl_regz00_bglt, int);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2numz20zzsaw_regutilsz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_regutilsz00
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_regsetzf3zf3zzsaw_regutilsz00(obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_regutilsz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_regutilsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_regutilsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_regutilsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2zzsaw_regutilsz00(BgL_regsetz00_bglt);
	static obj_t BGl_z62collectzd2registerz12za2zzsaw_regutilsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_regutilsz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_regutilsz00
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_regutilsz00
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_regsetzd2nilzd2zzsaw_regutilsz00(void);
	static obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_regutilsz00(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_regutilsz00(obj_t, obj_t);
	extern obj_t BGl_rtl_regzf2razf2zzsaw_regsetz00;
	static obj_t BGl_z62rtl_regzf2razf3z63zzsaw_regutilsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_regutilsz00(BgL_rtl_regz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2reglzd2zzsaw_regutilsz00(BgL_regsetz00_bglt);
	static obj_t BGl_z62regsetzd2msiza7ez17zzsaw_regutilsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2regvzd2zzsaw_regutilsz00(BgL_regsetz00_bglt);
	static obj_t
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_regutilsz00(obj_t, obj_t,
		obj_t);
	static BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_regutilsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_regutilsz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_regutilsz00(BgL_regsetz00_bglt, int);
	static obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_regutilsz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2namezd2envzf2zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71851za7,
		BGl_z62rtl_regzf2razd2namez42zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2nilzd2envz00zzsaw_regutilsz00,
		BgL_bgl_za762regsetza7d2nilza71852za7,
		BGl_z62regsetzd2nilzb0zzsaw_regutilsz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2setz12zd2envz32zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71853za7,
		BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_regutilsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2envzf2zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71854za7,
		BGl_z62rtl_regzf2razd2interferez42zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2setz12zd2envzc1zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71855za7,
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_regutilsz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2setz12zd2envz32zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71856za7,
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_regutilsz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2envzf2zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71857za7,
		BGl_z62rtl_regzf2razd2colorz42zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2setz12zd2envz32zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71858za7,
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_regutilsz00, 0L, BUNSPEC,
		2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2regvzd2envz00zzsaw_regutilsz00,
		BgL_bgl_za762regsetza7d2regv1859z00,
		BGl_z62regsetzd2regvzb0zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2setz12zd2envzc0zzsaw_regutilsz00,
		BgL_bgl_za762regsetza7d2leng1860z00,
		BGl_z62regsetzd2lengthzd2setz12z70zzsaw_regutilsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2msiza7ezd2envza7zzsaw_regutilsz00,
		BgL_bgl_za762regsetza7d2msiza71861za7,
		BGl_z62regsetzd2msiza7ez17zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2envzf2zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71862za7,
		BGl_z62rtl_regzf2razd2typez42zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2setz12zd2envz32zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71863za7,
		BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_regutilsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2envzf2zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71864za7,
		BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1850z00zzsaw_regutilsz00,
		BgL_bgl_string1850za700za7za7s1865za7, "saw_regutils", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1849z00zzsaw_regutilsz00,
		BgL_bgl_za762argsza7d2collec1866z00,
		BGl_z62argszd2collectzd2registersz12z70zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2nilzd2envzf2zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71867za7,
		BGl_z62rtl_regzf2razd2nilz42zzsaw_regutilsz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2envzf2zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71868za7,
		BGl_z62rtl_regzf2razd2varz42zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2numzd2envzf2zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71869za7,
		BGl_z62rtl_regzf2razd2numz42zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2setz12zd2envzc0zzsaw_regutilsz00,
		BgL_bgl_za762regsetza7d2stri1870z00,
		BGl_z62regsetzd2stringzd2setz12z70zzsaw_regutilsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2setz12zd2envz32zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71871za7,
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_regutilsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2envz01zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71872za7,
		BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_collectzd2registersz12zd2envz12zzsaw_regutilsz00,
		BgL_bgl_za762collectza7d2reg1873z00,
		BGl_z62collectzd2registersz12za2zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2stringzd2envz00zzsaw_regutilsz00,
		BgL_bgl_za762regsetza7d2stri1874z00,
		BGl_z62regsetzd2stringzb0zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_collectzd2registerz12zd2envz12zzsaw_regutilsz00,
		BgL_bgl_za762collectza7d2reg1875z00,
		BGl_z62collectzd2registerz12za2zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2setz12zd2envz32zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71876za7,
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_regutilsz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2rtl_regzf2razd2envzf2zzsaw_regutilsz00,
		BgL_bgl_za762makeza7d2rtl_re1877z00,
		BGl_z62makezd2rtl_regzf2raz42zzsaw_regutilsz00, 0L, BUNSPEC, 12);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razf3zd2envzd3zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71878za7,
		BGl_z62rtl_regzf2razf3z63zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2lengthzd2envz00zzsaw_regutilsz00,
		BgL_bgl_za762regsetza7d2leng1879z00,
		BGl_z62regsetzd2lengthzb0zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2envzf2zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71880za7,
		BGl_z62rtl_regzf2razd2coalescez42zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2envzf2zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71881za7,
		BGl_z62rtl_regzf2razd2interfere2z42zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2hardwarezd2envzf2zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71882za7,
		BGl_z62rtl_regzf2razd2hardwarez42zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2regsetzd2envz00zzsaw_regutilsz00,
		BgL_bgl_za762makeza7d2regset1883z00,
		BGl_z62makezd2regsetzb0zzsaw_regutilsz00, 0L, BUNSPEC, 5);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzf3zd2envz21zzsaw_regutilsz00,
		BgL_bgl_za762regsetza7f3za791za71884z00,
		BGl_z62regsetzf3z91zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2keyzd2envzf2zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71885za7,
		BGl_z62rtl_regzf2razd2keyz42zzsaw_regutilsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2setz12zd2envz32zzsaw_regutilsz00,
		BgL_bgl_za762rtl_regza7f2raza71886za7,
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_regutilsz00, 0L, BUNSPEC,
		2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2reglzd2envz00zzsaw_regutilsz00,
		BgL_bgl_za762regsetza7d2regl1887z00,
		BGl_z62regsetzd2reglzb0zzsaw_regutilsz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_regutilsz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(long
		BgL_checksumz00_2772, char *BgL_fromz00_2773)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_regutilsz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_regutilsz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_regutilsz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_regutilsz00();
					BGl_importedzd2moduleszd2initz00zzsaw_regutilsz00();
					return BGl_toplevelzd2initzd2zzsaw_regutilsz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_regutilsz00(void)
	{
		{	/* SawMill/regutils.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_regutils");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_regutils");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"saw_regutils");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_regutils");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_regutils");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_regutils");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_regutils");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_regutilsz00(void)
	{
		{	/* SawMill/regutils.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_regutilsz00(void)
	{
		{	/* SawMill/regutils.scm 15 */
			return (BGl_za2registerzd2countza2zd2zzsaw_regutilsz00 = -1L, BUNSPEC);
		}

	}



/* make-rtl_reg/ra */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_regutilsz00(BgL_typez00_bglt
		BgL_type1179z00_3, obj_t BgL_var1180z00_4, obj_t BgL_onexprzf31181zf3_5,
		obj_t BgL_name1182z00_6, obj_t BgL_key1183z00_7,
		obj_t BgL_hardware1184z00_8, int BgL_num1185z00_9,
		obj_t BgL_color1186z00_10, obj_t BgL_coalesce1187z00_11,
		int BgL_occurrences1188z00_12, obj_t BgL_interfere1189z00_13,
		obj_t BgL_interfere21190z00_14)
	{
		{	/* SawMill/regset.sch 55 */
			{	/* SawMill/regset.sch 55 */
				BgL_rtl_regz00_bglt BgL_new1159z00_2724;

				{	/* SawMill/regset.sch 55 */
					BgL_rtl_regz00_bglt BgL_tmp1157z00_2725;
					BgL_rtl_regzf2razf2_bglt BgL_wide1158z00_2726;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_2789;

						{	/* SawMill/regset.sch 55 */
							BgL_rtl_regz00_bglt BgL_new1156z00_2727;

							BgL_new1156z00_2727 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/regset.sch 55 */
								long BgL_arg1422z00_2728;

								BgL_arg1422z00_2728 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1156z00_2727),
									BgL_arg1422z00_2728);
							}
							{	/* SawMill/regset.sch 55 */
								BgL_objectz00_bglt BgL_tmpz00_2794;

								BgL_tmpz00_2794 = ((BgL_objectz00_bglt) BgL_new1156z00_2727);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2794, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1156z00_2727);
							BgL_auxz00_2789 = BgL_new1156z00_2727;
						}
						BgL_tmp1157z00_2725 = ((BgL_rtl_regz00_bglt) BgL_auxz00_2789);
					}
					BgL_wide1158z00_2726 =
						((BgL_rtl_regzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regzf2razf2_bgl))));
					{	/* SawMill/regset.sch 55 */
						obj_t BgL_auxz00_2802;
						BgL_objectz00_bglt BgL_tmpz00_2800;

						BgL_auxz00_2802 = ((obj_t) BgL_wide1158z00_2726);
						BgL_tmpz00_2800 = ((BgL_objectz00_bglt) BgL_tmp1157z00_2725);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2800, BgL_auxz00_2802);
					}
					((BgL_objectz00_bglt) BgL_tmp1157z00_2725);
					{	/* SawMill/regset.sch 55 */
						long BgL_arg1421z00_2729;

						BgL_arg1421z00_2729 =
							BGL_CLASS_NUM(BGl_rtl_regzf2razf2zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1157z00_2725), BgL_arg1421z00_2729);
					}
					BgL_new1159z00_2724 = ((BgL_rtl_regz00_bglt) BgL_tmp1157z00_2725);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1159z00_2724)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1179z00_3), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1159z00_2724)))->BgL_varz00) =
					((obj_t) BgL_var1180z00_4), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1159z00_2724)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31181zf3_5), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1159z00_2724)))->BgL_namez00) =
					((obj_t) BgL_name1182z00_6), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1159z00_2724)))->BgL_keyz00) =
					((obj_t) BgL_key1183z00_7), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1159z00_2724)))->BgL_debugnamez00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1159z00_2724)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1184z00_8), BUNSPEC);
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_2824;

					{
						obj_t BgL_auxz00_2825;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_2826;

							BgL_tmpz00_2826 = ((BgL_objectz00_bglt) BgL_new1159z00_2724);
							BgL_auxz00_2825 = BGL_OBJECT_WIDENING(BgL_tmpz00_2826);
						}
						BgL_auxz00_2824 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2825);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2824))->BgL_numz00) =
						((int) BgL_num1185z00_9), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_2831;

					{
						obj_t BgL_auxz00_2832;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_2833;

							BgL_tmpz00_2833 = ((BgL_objectz00_bglt) BgL_new1159z00_2724);
							BgL_auxz00_2832 = BGL_OBJECT_WIDENING(BgL_tmpz00_2833);
						}
						BgL_auxz00_2831 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2832);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2831))->
							BgL_colorz00) = ((obj_t) BgL_color1186z00_10), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_2838;

					{
						obj_t BgL_auxz00_2839;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_2840;

							BgL_tmpz00_2840 = ((BgL_objectz00_bglt) BgL_new1159z00_2724);
							BgL_auxz00_2839 = BGL_OBJECT_WIDENING(BgL_tmpz00_2840);
						}
						BgL_auxz00_2838 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2839);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2838))->
							BgL_coalescez00) = ((obj_t) BgL_coalesce1187z00_11), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_2845;

					{
						obj_t BgL_auxz00_2846;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_2847;

							BgL_tmpz00_2847 = ((BgL_objectz00_bglt) BgL_new1159z00_2724);
							BgL_auxz00_2846 = BGL_OBJECT_WIDENING(BgL_tmpz00_2847);
						}
						BgL_auxz00_2845 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2846);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2845))->
							BgL_occurrencesz00) = ((int) BgL_occurrences1188z00_12), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_2852;

					{
						obj_t BgL_auxz00_2853;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_2854;

							BgL_tmpz00_2854 = ((BgL_objectz00_bglt) BgL_new1159z00_2724);
							BgL_auxz00_2853 = BGL_OBJECT_WIDENING(BgL_tmpz00_2854);
						}
						BgL_auxz00_2852 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2853);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2852))->
							BgL_interferez00) = ((obj_t) BgL_interfere1189z00_13), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_2859;

					{
						obj_t BgL_auxz00_2860;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_2861;

							BgL_tmpz00_2861 = ((BgL_objectz00_bglt) BgL_new1159z00_2724);
							BgL_auxz00_2860 = BGL_OBJECT_WIDENING(BgL_tmpz00_2861);
						}
						BgL_auxz00_2859 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2860);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2859))->
							BgL_interfere2z00) = ((obj_t) BgL_interfere21190z00_14), BUNSPEC);
				}
				return BgL_new1159z00_2724;
			}
		}

	}



/* &make-rtl_reg/ra */
	BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzf2raz42zzsaw_regutilsz00(obj_t
		BgL_envz00_2618, obj_t BgL_type1179z00_2619, obj_t BgL_var1180z00_2620,
		obj_t BgL_onexprzf31181zf3_2621, obj_t BgL_name1182z00_2622,
		obj_t BgL_key1183z00_2623, obj_t BgL_hardware1184z00_2624,
		obj_t BgL_num1185z00_2625, obj_t BgL_color1186z00_2626,
		obj_t BgL_coalesce1187z00_2627, obj_t BgL_occurrences1188z00_2628,
		obj_t BgL_interfere1189z00_2629, obj_t BgL_interfere21190z00_2630)
	{
		{	/* SawMill/regset.sch 55 */
			return
				BGl_makezd2rtl_regzf2raz20zzsaw_regutilsz00(
				((BgL_typez00_bglt) BgL_type1179z00_2619), BgL_var1180z00_2620,
				BgL_onexprzf31181zf3_2621, BgL_name1182z00_2622, BgL_key1183z00_2623,
				BgL_hardware1184z00_2624, CINT(BgL_num1185z00_2625),
				BgL_color1186z00_2626, BgL_coalesce1187z00_2627,
				CINT(BgL_occurrences1188z00_2628), BgL_interfere1189z00_2629,
				BgL_interfere21190z00_2630);
		}

	}



/* rtl_reg/ra? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_regzf2razf3z01zzsaw_regutilsz00(obj_t
		BgL_objz00_15)
	{
		{	/* SawMill/regset.sch 56 */
			{	/* SawMill/regset.sch 56 */
				obj_t BgL_classz00_2730;

				BgL_classz00_2730 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_15))
					{	/* SawMill/regset.sch 56 */
						BgL_objectz00_bglt BgL_arg1807z00_2731;

						BgL_arg1807z00_2731 = (BgL_objectz00_bglt) (BgL_objz00_15);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 56 */
								long BgL_idxz00_2732;

								BgL_idxz00_2732 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2731);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2732 + 2L)) == BgL_classz00_2730);
							}
						else
							{	/* SawMill/regset.sch 56 */
								bool_t BgL_res1841z00_2735;

								{	/* SawMill/regset.sch 56 */
									obj_t BgL_oclassz00_2736;

									{	/* SawMill/regset.sch 56 */
										obj_t BgL_arg1815z00_2737;
										long BgL_arg1816z00_2738;

										BgL_arg1815z00_2737 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 56 */
											long BgL_arg1817z00_2739;

											BgL_arg1817z00_2739 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2731);
											BgL_arg1816z00_2738 = (BgL_arg1817z00_2739 - OBJECT_TYPE);
										}
										BgL_oclassz00_2736 =
											VECTOR_REF(BgL_arg1815z00_2737, BgL_arg1816z00_2738);
									}
									{	/* SawMill/regset.sch 56 */
										bool_t BgL__ortest_1115z00_2740;

										BgL__ortest_1115z00_2740 =
											(BgL_classz00_2730 == BgL_oclassz00_2736);
										if (BgL__ortest_1115z00_2740)
											{	/* SawMill/regset.sch 56 */
												BgL_res1841z00_2735 = BgL__ortest_1115z00_2740;
											}
										else
											{	/* SawMill/regset.sch 56 */
												long BgL_odepthz00_2741;

												{	/* SawMill/regset.sch 56 */
													obj_t BgL_arg1804z00_2742;

													BgL_arg1804z00_2742 = (BgL_oclassz00_2736);
													BgL_odepthz00_2741 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2742);
												}
												if ((2L < BgL_odepthz00_2741))
													{	/* SawMill/regset.sch 56 */
														obj_t BgL_arg1802z00_2743;

														{	/* SawMill/regset.sch 56 */
															obj_t BgL_arg1803z00_2744;

															BgL_arg1803z00_2744 = (BgL_oclassz00_2736);
															BgL_arg1802z00_2743 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2744,
																2L);
														}
														BgL_res1841z00_2735 =
															(BgL_arg1802z00_2743 == BgL_classz00_2730);
													}
												else
													{	/* SawMill/regset.sch 56 */
														BgL_res1841z00_2735 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1841z00_2735;
							}
					}
				else
					{	/* SawMill/regset.sch 56 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_reg/ra? */
	obj_t BGl_z62rtl_regzf2razf3z63zzsaw_regutilsz00(obj_t BgL_envz00_2631,
		obj_t BgL_objz00_2632)
	{
		{	/* SawMill/regset.sch 56 */
			return BBOOL(BGl_rtl_regzf2razf3z01zzsaw_regutilsz00(BgL_objz00_2632));
		}

	}



/* rtl_reg/ra-nil */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_regutilsz00(void)
	{
		{	/* SawMill/regset.sch 57 */
			{	/* SawMill/regset.sch 57 */
				obj_t BgL_classz00_2379;

				BgL_classz00_2379 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				{	/* SawMill/regset.sch 57 */
					obj_t BgL__ortest_1117z00_2380;

					BgL__ortest_1117z00_2380 = BGL_CLASS_NIL(BgL_classz00_2379);
					if (CBOOL(BgL__ortest_1117z00_2380))
						{	/* SawMill/regset.sch 57 */
							return ((BgL_rtl_regz00_bglt) BgL__ortest_1117z00_2380);
						}
					else
						{	/* SawMill/regset.sch 57 */
							return
								((BgL_rtl_regz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2379));
						}
				}
			}
		}

	}



/* &rtl_reg/ra-nil */
	BgL_rtl_regz00_bglt BGl_z62rtl_regzf2razd2nilz42zzsaw_regutilsz00(obj_t
		BgL_envz00_2633)
	{
		{	/* SawMill/regset.sch 57 */
			return BGl_rtl_regzf2razd2nilz20zzsaw_regutilsz00();
		}

	}



/* rtl_reg/ra-interfere2 */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_regutilsz00(BgL_rtl_regz00_bglt
		BgL_oz00_16)
	{
		{	/* SawMill/regset.sch 58 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_2901;

				{
					obj_t BgL_auxz00_2902;

					{	/* SawMill/regset.sch 58 */
						BgL_objectz00_bglt BgL_tmpz00_2903;

						BgL_tmpz00_2903 = ((BgL_objectz00_bglt) BgL_oz00_16);
						BgL_auxz00_2902 = BGL_OBJECT_WIDENING(BgL_tmpz00_2903);
					}
					BgL_auxz00_2901 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2902);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2901))->
					BgL_interfere2z00);
			}
		}

	}



/* &rtl_reg/ra-interfere2 */
	obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_regutilsz00(obj_t
		BgL_envz00_2634, obj_t BgL_oz00_2635)
	{
		{	/* SawMill/regset.sch 58 */
			return
				BGl_rtl_regzf2razd2interfere2z20zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2635));
		}

	}



/* rtl_reg/ra-interfere2-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_regutilsz00
		(BgL_rtl_regz00_bglt BgL_oz00_17, obj_t BgL_vz00_18)
	{
		{	/* SawMill/regset.sch 59 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_2910;

				{
					obj_t BgL_auxz00_2911;

					{	/* SawMill/regset.sch 59 */
						BgL_objectz00_bglt BgL_tmpz00_2912;

						BgL_tmpz00_2912 = ((BgL_objectz00_bglt) BgL_oz00_17);
						BgL_auxz00_2911 = BGL_OBJECT_WIDENING(BgL_tmpz00_2912);
					}
					BgL_auxz00_2910 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2911);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2910))->
						BgL_interfere2z00) = ((obj_t) BgL_vz00_18), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere2-set! */
	obj_t BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_regutilsz00(obj_t
		BgL_envz00_2636, obj_t BgL_oz00_2637, obj_t BgL_vz00_2638)
	{
		{	/* SawMill/regset.sch 59 */
			return
				BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2637), BgL_vz00_2638);
		}

	}



/* rtl_reg/ra-interfere */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_regutilsz00(BgL_rtl_regz00_bglt
		BgL_oz00_19)
	{
		{	/* SawMill/regset.sch 60 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_2919;

				{
					obj_t BgL_auxz00_2920;

					{	/* SawMill/regset.sch 60 */
						BgL_objectz00_bglt BgL_tmpz00_2921;

						BgL_tmpz00_2921 = ((BgL_objectz00_bglt) BgL_oz00_19);
						BgL_auxz00_2920 = BGL_OBJECT_WIDENING(BgL_tmpz00_2921);
					}
					BgL_auxz00_2919 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2920);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2919))->
					BgL_interferez00);
			}
		}

	}



/* &rtl_reg/ra-interfere */
	obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_regutilsz00(obj_t
		BgL_envz00_2639, obj_t BgL_oz00_2640)
	{
		{	/* SawMill/regset.sch 60 */
			return
				BGl_rtl_regzf2razd2interferez20zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2640));
		}

	}



/* rtl_reg/ra-interfere-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_regutilsz00
		(BgL_rtl_regz00_bglt BgL_oz00_20, obj_t BgL_vz00_21)
	{
		{	/* SawMill/regset.sch 61 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_2928;

				{
					obj_t BgL_auxz00_2929;

					{	/* SawMill/regset.sch 61 */
						BgL_objectz00_bglt BgL_tmpz00_2930;

						BgL_tmpz00_2930 = ((BgL_objectz00_bglt) BgL_oz00_20);
						BgL_auxz00_2929 = BGL_OBJECT_WIDENING(BgL_tmpz00_2930);
					}
					BgL_auxz00_2928 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2929);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2928))->
						BgL_interferez00) = ((obj_t) BgL_vz00_21), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere-set! */
	obj_t BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_regutilsz00(obj_t
		BgL_envz00_2641, obj_t BgL_oz00_2642, obj_t BgL_vz00_2643)
	{
		{	/* SawMill/regset.sch 61 */
			return
				BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2642), BgL_vz00_2643);
		}

	}



/* rtl_reg/ra-occurrences */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_regutilsz00(BgL_rtl_regz00_bglt
		BgL_oz00_22)
	{
		{	/* SawMill/regset.sch 62 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_2937;

				{
					obj_t BgL_auxz00_2938;

					{	/* SawMill/regset.sch 62 */
						BgL_objectz00_bglt BgL_tmpz00_2939;

						BgL_tmpz00_2939 = ((BgL_objectz00_bglt) BgL_oz00_22);
						BgL_auxz00_2938 = BGL_OBJECT_WIDENING(BgL_tmpz00_2939);
					}
					BgL_auxz00_2937 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2938);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2937))->
					BgL_occurrencesz00);
			}
		}

	}



/* &rtl_reg/ra-occurrences */
	obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_regutilsz00(obj_t
		BgL_envz00_2644, obj_t BgL_oz00_2645)
	{
		{	/* SawMill/regset.sch 62 */
			return
				BINT(BGl_rtl_regzf2razd2occurrencesz20zzsaw_regutilsz00(
					((BgL_rtl_regz00_bglt) BgL_oz00_2645)));
		}

	}



/* rtl_reg/ra-occurrences-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_regutilsz00
		(BgL_rtl_regz00_bglt BgL_oz00_23, int BgL_vz00_24)
	{
		{	/* SawMill/regset.sch 63 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_2947;

				{
					obj_t BgL_auxz00_2948;

					{	/* SawMill/regset.sch 63 */
						BgL_objectz00_bglt BgL_tmpz00_2949;

						BgL_tmpz00_2949 = ((BgL_objectz00_bglt) BgL_oz00_23);
						BgL_auxz00_2948 = BGL_OBJECT_WIDENING(BgL_tmpz00_2949);
					}
					BgL_auxz00_2947 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2948);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2947))->
						BgL_occurrencesz00) = ((int) BgL_vz00_24), BUNSPEC);
		}}

	}



/* &rtl_reg/ra-occurrences-set! */
	obj_t BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_regutilsz00(obj_t
		BgL_envz00_2646, obj_t BgL_oz00_2647, obj_t BgL_vz00_2648)
	{
		{	/* SawMill/regset.sch 63 */
			return
				BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2647), CINT(BgL_vz00_2648));
		}

	}



/* rtl_reg/ra-coalesce */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_regutilsz00(BgL_rtl_regz00_bglt
		BgL_oz00_25)
	{
		{	/* SawMill/regset.sch 64 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_2957;

				{
					obj_t BgL_auxz00_2958;

					{	/* SawMill/regset.sch 64 */
						BgL_objectz00_bglt BgL_tmpz00_2959;

						BgL_tmpz00_2959 = ((BgL_objectz00_bglt) BgL_oz00_25);
						BgL_auxz00_2958 = BGL_OBJECT_WIDENING(BgL_tmpz00_2959);
					}
					BgL_auxz00_2957 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2958);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2957))->
					BgL_coalescez00);
			}
		}

	}



/* &rtl_reg/ra-coalesce */
	obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_regutilsz00(obj_t
		BgL_envz00_2649, obj_t BgL_oz00_2650)
	{
		{	/* SawMill/regset.sch 64 */
			return
				BGl_rtl_regzf2razd2coalescez20zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2650));
		}

	}



/* rtl_reg/ra-coalesce-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_regutilsz00(BgL_rtl_regz00_bglt
		BgL_oz00_26, obj_t BgL_vz00_27)
	{
		{	/* SawMill/regset.sch 65 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_2966;

				{
					obj_t BgL_auxz00_2967;

					{	/* SawMill/regset.sch 65 */
						BgL_objectz00_bglt BgL_tmpz00_2968;

						BgL_tmpz00_2968 = ((BgL_objectz00_bglt) BgL_oz00_26);
						BgL_auxz00_2967 = BGL_OBJECT_WIDENING(BgL_tmpz00_2968);
					}
					BgL_auxz00_2966 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2967);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2966))->
						BgL_coalescez00) = ((obj_t) BgL_vz00_27), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-coalesce-set! */
	obj_t BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_regutilsz00(obj_t
		BgL_envz00_2651, obj_t BgL_oz00_2652, obj_t BgL_vz00_2653)
	{
		{	/* SawMill/regset.sch 65 */
			return
				BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2652), BgL_vz00_2653);
		}

	}



/* rtl_reg/ra-color */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_regutilsz00(BgL_rtl_regz00_bglt
		BgL_oz00_28)
	{
		{	/* SawMill/regset.sch 66 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_2975;

				{
					obj_t BgL_auxz00_2976;

					{	/* SawMill/regset.sch 66 */
						BgL_objectz00_bglt BgL_tmpz00_2977;

						BgL_tmpz00_2977 = ((BgL_objectz00_bglt) BgL_oz00_28);
						BgL_auxz00_2976 = BGL_OBJECT_WIDENING(BgL_tmpz00_2977);
					}
					BgL_auxz00_2975 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2976);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2975))->BgL_colorz00);
			}
		}

	}



/* &rtl_reg/ra-color */
	obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_regutilsz00(obj_t BgL_envz00_2654,
		obj_t BgL_oz00_2655)
	{
		{	/* SawMill/regset.sch 66 */
			return
				BGl_rtl_regzf2razd2colorz20zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2655));
		}

	}



/* rtl_reg/ra-color-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_regutilsz00(BgL_rtl_regz00_bglt
		BgL_oz00_29, obj_t BgL_vz00_30)
	{
		{	/* SawMill/regset.sch 67 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_2984;

				{
					obj_t BgL_auxz00_2985;

					{	/* SawMill/regset.sch 67 */
						BgL_objectz00_bglt BgL_tmpz00_2986;

						BgL_tmpz00_2986 = ((BgL_objectz00_bglt) BgL_oz00_29);
						BgL_auxz00_2985 = BGL_OBJECT_WIDENING(BgL_tmpz00_2986);
					}
					BgL_auxz00_2984 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2985);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2984))->
						BgL_colorz00) = ((obj_t) BgL_vz00_30), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-color-set! */
	obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_regutilsz00(obj_t
		BgL_envz00_2656, obj_t BgL_oz00_2657, obj_t BgL_vz00_2658)
	{
		{	/* SawMill/regset.sch 67 */
			return
				BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2657), BgL_vz00_2658);
		}

	}



/* rtl_reg/ra-num */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2numz20zzsaw_regutilsz00(BgL_rtl_regz00_bglt BgL_oz00_31)
	{
		{	/* SawMill/regset.sch 68 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_2993;

				{
					obj_t BgL_auxz00_2994;

					{	/* SawMill/regset.sch 68 */
						BgL_objectz00_bglt BgL_tmpz00_2995;

						BgL_tmpz00_2995 = ((BgL_objectz00_bglt) BgL_oz00_31);
						BgL_auxz00_2994 = BGL_OBJECT_WIDENING(BgL_tmpz00_2995);
					}
					BgL_auxz00_2993 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_2994);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_2993))->BgL_numz00);
			}
		}

	}



/* &rtl_reg/ra-num */
	obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_regutilsz00(obj_t BgL_envz00_2659,
		obj_t BgL_oz00_2660)
	{
		{	/* SawMill/regset.sch 68 */
			return
				BINT(BGl_rtl_regzf2razd2numz20zzsaw_regutilsz00(
					((BgL_rtl_regz00_bglt) BgL_oz00_2660)));
		}

	}



/* rtl_reg/ra-hardware */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_regutilsz00(BgL_rtl_regz00_bglt
		BgL_oz00_34)
	{
		{	/* SawMill/regset.sch 70 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_34)))->BgL_hardwarez00);
		}

	}



/* &rtl_reg/ra-hardware */
	obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_regutilsz00(obj_t
		BgL_envz00_2661, obj_t BgL_oz00_2662)
	{
		{	/* SawMill/regset.sch 70 */
			return
				BGl_rtl_regzf2razd2hardwarez20zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2662));
		}

	}



/* rtl_reg/ra-key */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_regutilsz00(BgL_rtl_regz00_bglt BgL_oz00_37)
	{
		{	/* SawMill/regset.sch 72 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_37)))->BgL_keyz00);
		}

	}



/* &rtl_reg/ra-key */
	obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_regutilsz00(obj_t BgL_envz00_2663,
		obj_t BgL_oz00_2664)
	{
		{	/* SawMill/regset.sch 72 */
			return
				BGl_rtl_regzf2razd2keyz20zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2664));
		}

	}



/* rtl_reg/ra-name */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_regutilsz00(BgL_rtl_regz00_bglt BgL_oz00_40)
	{
		{	/* SawMill/regset.sch 74 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_40)))->BgL_namez00);
		}

	}



/* &rtl_reg/ra-name */
	obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_regutilsz00(obj_t BgL_envz00_2665,
		obj_t BgL_oz00_2666)
	{
		{	/* SawMill/regset.sch 74 */
			return
				BGl_rtl_regzf2razd2namez20zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2666));
		}

	}



/* rtl_reg/ra-onexpr? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_regutilsz00(BgL_rtl_regz00_bglt
		BgL_oz00_43)
	{
		{	/* SawMill/regset.sch 76 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_43)))->BgL_onexprzf3zf3);
		}

	}



/* &rtl_reg/ra-onexpr? */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_regutilsz00(obj_t
		BgL_envz00_2667, obj_t BgL_oz00_2668)
	{
		{	/* SawMill/regset.sch 76 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2668));
		}

	}



/* rtl_reg/ra-onexpr?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_regutilsz00
		(BgL_rtl_regz00_bglt BgL_oz00_44, obj_t BgL_vz00_45)
	{
		{	/* SawMill/regset.sch 77 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_44)))->BgL_onexprzf3zf3) =
				((obj_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &rtl_reg/ra-onexpr?-set! */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_regutilsz00(obj_t
		BgL_envz00_2669, obj_t BgL_oz00_2670, obj_t BgL_vz00_2671)
	{
		{	/* SawMill/regset.sch 77 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2670), BgL_vz00_2671);
		}

	}



/* rtl_reg/ra-var */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_regutilsz00(BgL_rtl_regz00_bglt BgL_oz00_46)
	{
		{	/* SawMill/regset.sch 78 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_46)))->BgL_varz00);
		}

	}



/* &rtl_reg/ra-var */
	obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_regutilsz00(obj_t BgL_envz00_2672,
		obj_t BgL_oz00_2673)
	{
		{	/* SawMill/regset.sch 78 */
			return
				BGl_rtl_regzf2razd2varz20zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2673));
		}

	}



/* rtl_reg/ra-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_regutilsz00(BgL_rtl_regz00_bglt
		BgL_oz00_47, obj_t BgL_vz00_48)
	{
		{	/* SawMill/regset.sch 79 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_47)))->BgL_varz00) =
				((obj_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &rtl_reg/ra-var-set! */
	obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_regutilsz00(obj_t
		BgL_envz00_2674, obj_t BgL_oz00_2675, obj_t BgL_vz00_2676)
	{
		{	/* SawMill/regset.sch 79 */
			return
				BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2675), BgL_vz00_2676);
		}

	}



/* rtl_reg/ra-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_regutilsz00(BgL_rtl_regz00_bglt BgL_oz00_49)
	{
		{	/* SawMill/regset.sch 80 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_49)))->BgL_typez00);
		}

	}



/* &rtl_reg/ra-type */
	BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_regutilsz00(obj_t
		BgL_envz00_2677, obj_t BgL_oz00_2678)
	{
		{	/* SawMill/regset.sch 80 */
			return
				BGl_rtl_regzf2razd2typez20zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2678));
		}

	}



/* rtl_reg/ra-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_regutilsz00(BgL_rtl_regz00_bglt
		BgL_oz00_50, BgL_typez00_bglt BgL_vz00_51)
	{
		{	/* SawMill/regset.sch 81 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_50)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_51), BUNSPEC);
		}

	}



/* &rtl_reg/ra-type-set! */
	obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_regutilsz00(obj_t
		BgL_envz00_2679, obj_t BgL_oz00_2680, obj_t BgL_vz00_2681)
	{
		{	/* SawMill/regset.sch 81 */
			return
				BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2680),
				((BgL_typez00_bglt) BgL_vz00_2681));
		}

	}



/* make-regset */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt BGl_makezd2regsetzd2zzsaw_regutilsz00(int
		BgL_length1172z00_52, int BgL_msiza7e1173za7_53, obj_t BgL_regv1174z00_54,
		obj_t BgL_regl1175z00_55, obj_t BgL_string1176z00_56)
	{
		{	/* SawMill/regset.sch 84 */
			{	/* SawMill/regset.sch 84 */
				BgL_regsetz00_bglt BgL_new1161z00_2745;

				{	/* SawMill/regset.sch 84 */
					BgL_regsetz00_bglt BgL_new1160z00_2746;

					BgL_new1160z00_2746 =
						((BgL_regsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_regsetz00_bgl))));
					{	/* SawMill/regset.sch 84 */
						long BgL_arg1434z00_2747;

						BgL_arg1434z00_2747 = BGL_CLASS_NUM(BGl_regsetz00zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1160z00_2746), BgL_arg1434z00_2747);
					}
					BgL_new1161z00_2745 = BgL_new1160z00_2746;
				}
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1161z00_2745))->BgL_lengthz00) =
					((int) BgL_length1172z00_52), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1161z00_2745))->BgL_msiza7eza7) =
					((int) BgL_msiza7e1173za7_53), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1161z00_2745))->BgL_regvz00) =
					((obj_t) BgL_regv1174z00_54), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1161z00_2745))->BgL_reglz00) =
					((obj_t) BgL_regl1175z00_55), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1161z00_2745))->BgL_stringz00) =
					((obj_t) BgL_string1176z00_56), BUNSPEC);
				return BgL_new1161z00_2745;
			}
		}

	}



/* &make-regset */
	BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_regutilsz00(obj_t
		BgL_envz00_2682, obj_t BgL_length1172z00_2683,
		obj_t BgL_msiza7e1173za7_2684, obj_t BgL_regv1174z00_2685,
		obj_t BgL_regl1175z00_2686, obj_t BgL_string1176z00_2687)
	{
		{	/* SawMill/regset.sch 84 */
			return
				BGl_makezd2regsetzd2zzsaw_regutilsz00(CINT(BgL_length1172z00_2683),
				CINT(BgL_msiza7e1173za7_2684), BgL_regv1174z00_2685,
				BgL_regl1175z00_2686, BgL_string1176z00_2687);
		}

	}



/* regset? */
	BGL_EXPORTED_DEF bool_t BGl_regsetzf3zf3zzsaw_regutilsz00(obj_t BgL_objz00_57)
	{
		{	/* SawMill/regset.sch 85 */
			{	/* SawMill/regset.sch 85 */
				obj_t BgL_classz00_2748;

				BgL_classz00_2748 = BGl_regsetz00zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_57))
					{	/* SawMill/regset.sch 85 */
						BgL_objectz00_bglt BgL_arg1807z00_2749;

						BgL_arg1807z00_2749 = (BgL_objectz00_bglt) (BgL_objz00_57);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 85 */
								long BgL_idxz00_2750;

								BgL_idxz00_2750 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2749);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_2750 + 1L)) == BgL_classz00_2748);
							}
						else
							{	/* SawMill/regset.sch 85 */
								bool_t BgL_res1842z00_2753;

								{	/* SawMill/regset.sch 85 */
									obj_t BgL_oclassz00_2754;

									{	/* SawMill/regset.sch 85 */
										obj_t BgL_arg1815z00_2755;
										long BgL_arg1816z00_2756;

										BgL_arg1815z00_2755 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 85 */
											long BgL_arg1817z00_2757;

											BgL_arg1817z00_2757 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2749);
											BgL_arg1816z00_2756 = (BgL_arg1817z00_2757 - OBJECT_TYPE);
										}
										BgL_oclassz00_2754 =
											VECTOR_REF(BgL_arg1815z00_2755, BgL_arg1816z00_2756);
									}
									{	/* SawMill/regset.sch 85 */
										bool_t BgL__ortest_1115z00_2758;

										BgL__ortest_1115z00_2758 =
											(BgL_classz00_2748 == BgL_oclassz00_2754);
										if (BgL__ortest_1115z00_2758)
											{	/* SawMill/regset.sch 85 */
												BgL_res1842z00_2753 = BgL__ortest_1115z00_2758;
											}
										else
											{	/* SawMill/regset.sch 85 */
												long BgL_odepthz00_2759;

												{	/* SawMill/regset.sch 85 */
													obj_t BgL_arg1804z00_2760;

													BgL_arg1804z00_2760 = (BgL_oclassz00_2754);
													BgL_odepthz00_2759 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_2760);
												}
												if ((1L < BgL_odepthz00_2759))
													{	/* SawMill/regset.sch 85 */
														obj_t BgL_arg1802z00_2761;

														{	/* SawMill/regset.sch 85 */
															obj_t BgL_arg1803z00_2762;

															BgL_arg1803z00_2762 = (BgL_oclassz00_2754);
															BgL_arg1802z00_2761 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2762,
																1L);
														}
														BgL_res1842z00_2753 =
															(BgL_arg1802z00_2761 == BgL_classz00_2748);
													}
												else
													{	/* SawMill/regset.sch 85 */
														BgL_res1842z00_2753 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1842z00_2753;
							}
					}
				else
					{	/* SawMill/regset.sch 85 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &regset? */
	obj_t BGl_z62regsetzf3z91zzsaw_regutilsz00(obj_t BgL_envz00_2688,
		obj_t BgL_objz00_2689)
	{
		{	/* SawMill/regset.sch 85 */
			return BBOOL(BGl_regsetzf3zf3zzsaw_regutilsz00(BgL_objz00_2689));
		}

	}



/* regset-nil */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt BGl_regsetzd2nilzd2zzsaw_regutilsz00(void)
	{
		{	/* SawMill/regset.sch 86 */
			{	/* SawMill/regset.sch 86 */
				obj_t BgL_classz00_2432;

				BgL_classz00_2432 = BGl_regsetz00zzsaw_regsetz00;
				{	/* SawMill/regset.sch 86 */
					obj_t BgL__ortest_1117z00_2433;

					BgL__ortest_1117z00_2433 = BGL_CLASS_NIL(BgL_classz00_2432);
					if (CBOOL(BgL__ortest_1117z00_2433))
						{	/* SawMill/regset.sch 86 */
							return ((BgL_regsetz00_bglt) BgL__ortest_1117z00_2433);
						}
					else
						{	/* SawMill/regset.sch 86 */
							return
								((BgL_regsetz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2432));
						}
				}
			}
		}

	}



/* &regset-nil */
	BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_regutilsz00(obj_t
		BgL_envz00_2690)
	{
		{	/* SawMill/regset.sch 86 */
			return BGl_regsetzd2nilzd2zzsaw_regutilsz00();
		}

	}



/* regset-string */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2zzsaw_regutilsz00(BgL_regsetz00_bglt BgL_oz00_58)
	{
		{	/* SawMill/regset.sch 87 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_58))->BgL_stringz00);
		}

	}



/* &regset-string */
	obj_t BGl_z62regsetzd2stringzb0zzsaw_regutilsz00(obj_t BgL_envz00_2691,
		obj_t BgL_oz00_2692)
	{
		{	/* SawMill/regset.sch 87 */
			return
				BGl_regsetzd2stringzd2zzsaw_regutilsz00(
				((BgL_regsetz00_bglt) BgL_oz00_2692));
		}

	}



/* regset-string-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_regutilsz00(BgL_regsetz00_bglt
		BgL_oz00_59, obj_t BgL_vz00_60)
	{
		{	/* SawMill/regset.sch 88 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_59))->BgL_stringz00) =
				((obj_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &regset-string-set! */
	obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_regutilsz00(obj_t
		BgL_envz00_2693, obj_t BgL_oz00_2694, obj_t BgL_vz00_2695)
	{
		{	/* SawMill/regset.sch 88 */
			return
				BGl_regsetzd2stringzd2setz12z12zzsaw_regutilsz00(
				((BgL_regsetz00_bglt) BgL_oz00_2694), BgL_vz00_2695);
		}

	}



/* regset-regl */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2reglzd2zzsaw_regutilsz00(BgL_regsetz00_bglt BgL_oz00_61)
	{
		{	/* SawMill/regset.sch 89 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_61))->BgL_reglz00);
		}

	}



/* &regset-regl */
	obj_t BGl_z62regsetzd2reglzb0zzsaw_regutilsz00(obj_t BgL_envz00_2696,
		obj_t BgL_oz00_2697)
	{
		{	/* SawMill/regset.sch 89 */
			return
				BGl_regsetzd2reglzd2zzsaw_regutilsz00(
				((BgL_regsetz00_bglt) BgL_oz00_2697));
		}

	}



/* regset-regv */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2regvzd2zzsaw_regutilsz00(BgL_regsetz00_bglt BgL_oz00_64)
	{
		{	/* SawMill/regset.sch 91 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_64))->BgL_regvz00);
		}

	}



/* &regset-regv */
	obj_t BGl_z62regsetzd2regvzb0zzsaw_regutilsz00(obj_t BgL_envz00_2698,
		obj_t BgL_oz00_2699)
	{
		{	/* SawMill/regset.sch 91 */
			return
				BGl_regsetzd2regvzd2zzsaw_regutilsz00(
				((BgL_regsetz00_bglt) BgL_oz00_2699));
		}

	}



/* regset-msize */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2msiza7ez75zzsaw_regutilsz00(BgL_regsetz00_bglt BgL_oz00_67)
	{
		{	/* SawMill/regset.sch 93 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_67))->BgL_msiza7eza7);
		}

	}



/* &regset-msize */
	obj_t BGl_z62regsetzd2msiza7ez17zzsaw_regutilsz00(obj_t BgL_envz00_2700,
		obj_t BgL_oz00_2701)
	{
		{	/* SawMill/regset.sch 93 */
			return
				BINT(BGl_regsetzd2msiza7ez75zzsaw_regutilsz00(
					((BgL_regsetz00_bglt) BgL_oz00_2701)));
		}

	}



/* regset-length */
	BGL_EXPORTED_DEF int
		BGl_regsetzd2lengthzd2zzsaw_regutilsz00(BgL_regsetz00_bglt BgL_oz00_70)
	{
		{	/* SawMill/regset.sch 95 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_70))->BgL_lengthz00);
		}

	}



/* &regset-length */
	obj_t BGl_z62regsetzd2lengthzb0zzsaw_regutilsz00(obj_t BgL_envz00_2702,
		obj_t BgL_oz00_2703)
	{
		{	/* SawMill/regset.sch 95 */
			return
				BINT(BGl_regsetzd2lengthzd2zzsaw_regutilsz00(
					((BgL_regsetz00_bglt) BgL_oz00_2703)));
		}

	}



/* regset-length-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_regutilsz00(BgL_regsetz00_bglt
		BgL_oz00_71, int BgL_vz00_72)
	{
		{	/* SawMill/regset.sch 96 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_71))->BgL_lengthz00) =
				((int) BgL_vz00_72), BUNSPEC);
		}

	}



/* &regset-length-set! */
	obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_regutilsz00(obj_t
		BgL_envz00_2704, obj_t BgL_oz00_2705, obj_t BgL_vz00_2706)
	{
		{	/* SawMill/regset.sch 96 */
			return
				BGl_regsetzd2lengthzd2setz12z12zzsaw_regutilsz00(
				((BgL_regsetz00_bglt) BgL_oz00_2705), CINT(BgL_vz00_2706));
		}

	}



/* collect-register! */
	BGL_EXPORTED_DEF obj_t
		BGl_collectzd2registerz12zc0zzsaw_regutilsz00(BgL_rtl_regz00_bglt
		BgL_oz00_73)
	{
		{	/* SawMill/regutils.scm 39 */
			{	/* SawMill/regutils.scm 40 */
				bool_t BgL_test1899z00_3107;

				{	/* SawMill/regutils.scm 40 */
					bool_t BgL_test1900z00_3108;

					{	/* SawMill/regutils.scm 40 */
						obj_t BgL_classz00_2434;

						BgL_classz00_2434 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
						{	/* SawMill/regutils.scm 40 */
							BgL_objectz00_bglt BgL_arg1807z00_2436;

							{	/* SawMill/regutils.scm 40 */
								obj_t BgL_tmpz00_3109;

								BgL_tmpz00_3109 = ((obj_t) BgL_oz00_73);
								BgL_arg1807z00_2436 = (BgL_objectz00_bglt) (BgL_tmpz00_3109);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawMill/regutils.scm 40 */
									long BgL_idxz00_2442;

									BgL_idxz00_2442 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2436);
									BgL_test1900z00_3108 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2442 + 2L)) == BgL_classz00_2434);
								}
							else
								{	/* SawMill/regutils.scm 40 */
									bool_t BgL_res1843z00_2467;

									{	/* SawMill/regutils.scm 40 */
										obj_t BgL_oclassz00_2450;

										{	/* SawMill/regutils.scm 40 */
											obj_t BgL_arg1815z00_2458;
											long BgL_arg1816z00_2459;

											BgL_arg1815z00_2458 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawMill/regutils.scm 40 */
												long BgL_arg1817z00_2460;

												BgL_arg1817z00_2460 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2436);
												BgL_arg1816z00_2459 =
													(BgL_arg1817z00_2460 - OBJECT_TYPE);
											}
											BgL_oclassz00_2450 =
												VECTOR_REF(BgL_arg1815z00_2458, BgL_arg1816z00_2459);
										}
										{	/* SawMill/regutils.scm 40 */
											bool_t BgL__ortest_1115z00_2451;

											BgL__ortest_1115z00_2451 =
												(BgL_classz00_2434 == BgL_oclassz00_2450);
											if (BgL__ortest_1115z00_2451)
												{	/* SawMill/regutils.scm 40 */
													BgL_res1843z00_2467 = BgL__ortest_1115z00_2451;
												}
											else
												{	/* SawMill/regutils.scm 40 */
													long BgL_odepthz00_2452;

													{	/* SawMill/regutils.scm 40 */
														obj_t BgL_arg1804z00_2453;

														BgL_arg1804z00_2453 = (BgL_oclassz00_2450);
														BgL_odepthz00_2452 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2453);
													}
													if ((2L < BgL_odepthz00_2452))
														{	/* SawMill/regutils.scm 40 */
															obj_t BgL_arg1802z00_2455;

															{	/* SawMill/regutils.scm 40 */
																obj_t BgL_arg1803z00_2456;

																BgL_arg1803z00_2456 = (BgL_oclassz00_2450);
																BgL_arg1802z00_2455 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2456,
																	2L);
															}
															BgL_res1843z00_2467 =
																(BgL_arg1802z00_2455 == BgL_classz00_2434);
														}
													else
														{	/* SawMill/regutils.scm 40 */
															BgL_res1843z00_2467 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1900z00_3108 = BgL_res1843z00_2467;
								}
						}
					}
					if (BgL_test1900z00_3108)
						{	/* SawMill/regutils.scm 40 */
							BgL_test1899z00_3107 = ((bool_t) 1);
						}
					else
						{	/* SawMill/regutils.scm 40 */
							BgL_test1899z00_3107 =
								CBOOL(
								(((BgL_rtl_regz00_bglt) COBJECT(BgL_oz00_73))->
									BgL_onexprzf3zf3));
						}
				}
				if (BgL_test1899z00_3107)
					{	/* SawMill/regutils.scm 40 */
						return BNIL;
					}
				else
					{	/* SawMill/regutils.scm 40 */
						BGl_za2registerzd2countza2zd2zzsaw_regutilsz00 =
							(1L + BGl_za2registerzd2countza2zd2zzsaw_regutilsz00);
						{	/* SawMill/regutils.scm 44 */
							BgL_rtl_regz00_bglt BgL_arg1437z00_1976;

							{	/* SawMill/regutils.scm 44 */
								BgL_rtl_regzf2razf2_bglt BgL_wide1164z00_1980;

								BgL_wide1164z00_1980 =
									((BgL_rtl_regzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_rtl_regzf2razf2_bgl))));
								{	/* SawMill/regutils.scm 44 */
									obj_t BgL_auxz00_3138;
									BgL_objectz00_bglt BgL_tmpz00_3135;

									BgL_auxz00_3138 = ((obj_t) BgL_wide1164z00_1980);
									BgL_tmpz00_3135 =
										((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_73));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3135, BgL_auxz00_3138);
								}
								((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_73));
								{	/* SawMill/regutils.scm 44 */
									long BgL_arg1448z00_1981;

									BgL_arg1448z00_1981 =
										BGL_CLASS_NUM(BGl_rtl_regzf2razf2zzsaw_regsetz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_oz00_73)),
										BgL_arg1448z00_1981);
								}
								((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_73));
							}
							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_3149;

								{
									obj_t BgL_auxz00_3150;

									{	/* SawMill/regutils.scm 44 */
										BgL_objectz00_bglt BgL_tmpz00_3151;

										BgL_tmpz00_3151 =
											((BgL_objectz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_oz00_73));
										BgL_auxz00_3150 = BGL_OBJECT_WIDENING(BgL_tmpz00_3151);
									}
									BgL_auxz00_3149 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3150);
								}
								((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3149))->
										BgL_numz00) =
									((int) (int)
										(BGl_za2registerzd2countza2zd2zzsaw_regutilsz00)), BUNSPEC);
							}
							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_3158;

								{
									obj_t BgL_auxz00_3159;

									{	/* SawMill/regutils.scm 44 */
										BgL_objectz00_bglt BgL_tmpz00_3160;

										BgL_tmpz00_3160 =
											((BgL_objectz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_oz00_73));
										BgL_auxz00_3159 = BGL_OBJECT_WIDENING(BgL_tmpz00_3160);
									}
									BgL_auxz00_3158 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3159);
								}
								((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3158))->
										BgL_colorz00) = ((obj_t) BFALSE), BUNSPEC);
							}
							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_3166;

								{
									obj_t BgL_auxz00_3167;

									{	/* SawMill/regutils.scm 44 */
										BgL_objectz00_bglt BgL_tmpz00_3168;

										BgL_tmpz00_3168 =
											((BgL_objectz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_oz00_73));
										BgL_auxz00_3167 = BGL_OBJECT_WIDENING(BgL_tmpz00_3168);
									}
									BgL_auxz00_3166 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3167);
								}
								((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3166))->
										BgL_coalescez00) = ((obj_t) BFALSE), BUNSPEC);
							}
							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_3174;

								{
									obj_t BgL_auxz00_3175;

									{	/* SawMill/regutils.scm 44 */
										BgL_objectz00_bglt BgL_tmpz00_3176;

										BgL_tmpz00_3176 =
											((BgL_objectz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_oz00_73));
										BgL_auxz00_3175 = BGL_OBJECT_WIDENING(BgL_tmpz00_3176);
									}
									BgL_auxz00_3174 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3175);
								}
								((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3174))->
										BgL_occurrencesz00) = ((int) (int) (0L)), BUNSPEC);
							}
							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_3183;

								{
									obj_t BgL_auxz00_3184;

									{	/* SawMill/regutils.scm 44 */
										BgL_objectz00_bglt BgL_tmpz00_3185;

										BgL_tmpz00_3185 =
											((BgL_objectz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_oz00_73));
										BgL_auxz00_3184 = BGL_OBJECT_WIDENING(BgL_tmpz00_3185);
									}
									BgL_auxz00_3183 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3184);
								}
								((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3183))->
										BgL_interferez00) = ((obj_t) BUNSPEC), BUNSPEC);
							}
							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_3191;

								{
									obj_t BgL_auxz00_3192;

									{	/* SawMill/regutils.scm 44 */
										BgL_objectz00_bglt BgL_tmpz00_3193;

										BgL_tmpz00_3193 =
											((BgL_objectz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_oz00_73));
										BgL_auxz00_3192 = BGL_OBJECT_WIDENING(BgL_tmpz00_3193);
									}
									BgL_auxz00_3191 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3192);
								}
								((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3191))->
										BgL_interfere2z00) = ((obj_t) BUNSPEC), BUNSPEC);
							}
							BgL_arg1437z00_1976 = ((BgL_rtl_regz00_bglt) BgL_oz00_73);
							{	/* SawMill/regutils.scm 44 */
								obj_t BgL_list1438z00_1977;

								BgL_list1438z00_1977 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_arg1437z00_1976), BNIL);
								return BgL_list1438z00_1977;
							}
						}
					}
			}
		}

	}



/* &collect-register! */
	obj_t BGl_z62collectzd2registerz12za2zzsaw_regutilsz00(obj_t BgL_envz00_2707,
		obj_t BgL_oz00_2708)
	{
		{	/* SawMill/regutils.scm 39 */
			return
				BGl_collectzd2registerz12zc0zzsaw_regutilsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_2708));
		}

	}



/* collect-registers! */
	BGL_EXPORTED_DEF obj_t BGl_collectzd2registersz12zc0zzsaw_regutilsz00(obj_t
		BgL_oz00_74)
	{
		{	/* SawMill/regutils.scm 56 */
			{	/* SawMill/regutils.scm 60 */
				obj_t BgL_blockzd2collectzd2registersz12z12_2711;
				obj_t BgL_inszd2collectzd2registersz12z12_2710;

				BgL_blockzd2collectzd2registersz12z12_2711 =
					MAKE_FX_PROCEDURE
					(BGl_z62blockzd2collectzd2registersz12z70zzsaw_regutilsz00,
					(int) (1L), (int) (2L));
				BgL_inszd2collectzd2registersz12z12_2710 =
					MAKE_FX_PROCEDURE
					(BGl_z62inszd2collectzd2registersz12z70zzsaw_regutilsz00, (int) (1L),
					(int) (1L));
				PROCEDURE_SET(BgL_blockzd2collectzd2registersz12z12_2711, (int) (0L),
					BGl_proc1849z00zzsaw_regutilsz00);
				PROCEDURE_SET(BgL_blockzd2collectzd2registersz12z12_2711, (int) (1L),
					BgL_inszd2collectzd2registersz12z12_2710);
				PROCEDURE_SET(BgL_inszd2collectzd2registersz12z12_2710, (int) (0L),
					BGl_proc1849z00zzsaw_regutilsz00);
				BGl_za2registerzd2countza2zd2zzsaw_regutilsz00 = -1L;
				{	/* SawMill/regutils.scm 78 */
					obj_t BgL_list1449z00_1987;

					BgL_list1449z00_1987 = MAKE_YOUNG_PAIR(BgL_oz00_74, BNIL);
					return
						BGl_appendzd2mapz12zc0zz__r4_control_features_6_9z00
						(BgL_blockzd2collectzd2registersz12z12_2711, BgL_list1449z00_1987);
				}
			}
		}

	}



/* &collect-registers! */
	obj_t BGl_z62collectzd2registersz12za2zzsaw_regutilsz00(obj_t BgL_envz00_2712,
		obj_t BgL_oz00_2713)
	{
		{	/* SawMill/regutils.scm 56 */
			return BGl_collectzd2registersz12zc0zzsaw_regutilsz00(BgL_oz00_2713);
		}

	}



/* &block-collect-registers! */
	obj_t BGl_z62blockzd2collectzd2registersz12z70zzsaw_regutilsz00(obj_t
		BgL_envz00_2714, obj_t BgL_oz00_2717)
	{
		{	/* SawMill/regutils.scm 75 */
			{	/* SawMill/regutils.scm 75 */
				obj_t BgL_inszd2collectzd2registersz12z12_2716;

				BgL_inszd2collectzd2registersz12z12_2716 =
					((obj_t) PROCEDURE_REF(BgL_envz00_2714, (int) (1L)));
				{	/* SawMill/regutils.scm 75 */
					obj_t BgL_arg1513z00_2763;

					BgL_arg1513z00_2763 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_oz00_2717)))->BgL_firstz00);
					{	/* SawMill/regutils.scm 75 */
						obj_t BgL_list1514z00_2764;

						BgL_list1514z00_2764 = MAKE_YOUNG_PAIR(BgL_arg1513z00_2763, BNIL);
						BGL_TAIL return
							BGl_appendzd2mapz12zc0zz__r4_control_features_6_9z00
							(BgL_inszd2collectzd2registersz12z12_2716, BgL_list1514z00_2764);
					}
				}
			}
		}

	}



/* &ins-collect-registers! */
	obj_t BGl_z62inszd2collectzd2registersz12z70zzsaw_regutilsz00(obj_t
		BgL_envz00_2718, obj_t BgL_oz00_2720)
	{
		{	/* SawMill/regutils.scm 72 */
			{	/* SawMill/regutils.scm 70 */
				obj_t BgL_argszd2collectzd2registersz12z12_2719;

				BgL_argszd2collectzd2registersz12z12_2719 =
					((obj_t) PROCEDURE_REF(BgL_envz00_2718, (int) (0L)));
				{	/* SawMill/regutils.scm 71 */
					obj_t BgL_arg1485z00_2766;
					obj_t BgL_arg1489z00_2767;

					{	/* SawMill/regutils.scm 71 */
						obj_t BgL_arg1502z00_2768;

						BgL_arg1502z00_2768 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_oz00_2720)))->BgL_destz00);
						BgL_arg1485z00_2766 =
							BGl_argszd2collectzd2registersz12ze70zf5zzsaw_regutilsz00
							(BgL_argszd2collectzd2registersz12z12_2719, BgL_arg1502z00_2768);
					}
					{	/* SawMill/regutils.scm 72 */
						obj_t BgL_arg1509z00_2770;

						BgL_arg1509z00_2770 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_oz00_2720)))->BgL_argsz00);
						{	/* SawMill/regutils.scm 72 */
							obj_t BgL_list1510z00_2771;

							BgL_list1510z00_2771 = MAKE_YOUNG_PAIR(BgL_arg1509z00_2770, BNIL);
							BgL_arg1489z00_2767 =
								BGl_appendzd2mapz12zc0zz__r4_control_features_6_9z00
								(BgL_argszd2collectzd2registersz12z12_2719,
								BgL_list1510z00_2771);
					}}
					return
						BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00
						(BgL_arg1485z00_2766, BgL_arg1489z00_2767);
				}
			}
		}

	}



/* &args-collect-registers! */
	obj_t BGl_z62argszd2collectzd2registersz12z70zzsaw_regutilsz00(obj_t
		BgL_envz00_2721, obj_t BgL_oz00_2722)
	{
		{	/* SawMill/regutils.scm 67 */
			BGL_TAIL return
				BGl_argszd2collectzd2registersz12ze70zf5zzsaw_regutilsz00
				(BgL_envz00_2721, BgL_oz00_2722);
		}

	}



/* args-collect-registers!~0 */
	obj_t BGl_argszd2collectzd2registersz12ze70zf5zzsaw_regutilsz00(obj_t
		BgL_envz00_2723, obj_t BgL_oz00_1988)
	{
		{	/* SawMill/regutils.scm 67 */
			{	/* SawMill/regutils.scm 60 */
				bool_t BgL_test1904z00_3238;

				{	/* SawMill/regutils.scm 60 */
					obj_t BgL_classz00_2481;

					BgL_classz00_2481 = BGl_rtl_regz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_oz00_1988))
						{	/* SawMill/regutils.scm 60 */
							BgL_objectz00_bglt BgL_arg1807z00_2483;

							BgL_arg1807z00_2483 = (BgL_objectz00_bglt) (BgL_oz00_1988);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawMill/regutils.scm 60 */
									long BgL_idxz00_2489;

									BgL_idxz00_2489 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2483);
									BgL_test1904z00_3238 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2489 + 1L)) == BgL_classz00_2481);
								}
							else
								{	/* SawMill/regutils.scm 60 */
									bool_t BgL_res1845z00_2514;

									{	/* SawMill/regutils.scm 60 */
										obj_t BgL_oclassz00_2497;

										{	/* SawMill/regutils.scm 60 */
											obj_t BgL_arg1815z00_2505;
											long BgL_arg1816z00_2506;

											BgL_arg1815z00_2505 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawMill/regutils.scm 60 */
												long BgL_arg1817z00_2507;

												BgL_arg1817z00_2507 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2483);
												BgL_arg1816z00_2506 =
													(BgL_arg1817z00_2507 - OBJECT_TYPE);
											}
											BgL_oclassz00_2497 =
												VECTOR_REF(BgL_arg1815z00_2505, BgL_arg1816z00_2506);
										}
										{	/* SawMill/regutils.scm 60 */
											bool_t BgL__ortest_1115z00_2498;

											BgL__ortest_1115z00_2498 =
												(BgL_classz00_2481 == BgL_oclassz00_2497);
											if (BgL__ortest_1115z00_2498)
												{	/* SawMill/regutils.scm 60 */
													BgL_res1845z00_2514 = BgL__ortest_1115z00_2498;
												}
											else
												{	/* SawMill/regutils.scm 60 */
													long BgL_odepthz00_2499;

													{	/* SawMill/regutils.scm 60 */
														obj_t BgL_arg1804z00_2500;

														BgL_arg1804z00_2500 = (BgL_oclassz00_2497);
														BgL_odepthz00_2499 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2500);
													}
													if ((1L < BgL_odepthz00_2499))
														{	/* SawMill/regutils.scm 60 */
															obj_t BgL_arg1802z00_2502;

															{	/* SawMill/regutils.scm 60 */
																obj_t BgL_arg1803z00_2503;

																BgL_arg1803z00_2503 = (BgL_oclassz00_2497);
																BgL_arg1802z00_2502 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2503,
																	1L);
															}
															BgL_res1845z00_2514 =
																(BgL_arg1802z00_2502 == BgL_classz00_2481);
														}
													else
														{	/* SawMill/regutils.scm 60 */
															BgL_res1845z00_2514 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1904z00_3238 = BgL_res1845z00_2514;
								}
						}
					else
						{	/* SawMill/regutils.scm 60 */
							BgL_test1904z00_3238 = ((bool_t) 0);
						}
				}
				if (BgL_test1904z00_3238)
					{	/* SawMill/regutils.scm 60 */
						return
							BGl_collectzd2registerz12zc0zzsaw_regutilsz00(
							((BgL_rtl_regz00_bglt) BgL_oz00_1988));
					}
				else
					{	/* SawMill/regutils.scm 62 */
						bool_t BgL_test1909z00_3263;

						{	/* SawMill/regutils.scm 62 */
							obj_t BgL_classz00_2515;

							BgL_classz00_2515 = BGl_rtl_insz00zzsaw_defsz00;
							if (BGL_OBJECTP(BgL_oz00_1988))
								{	/* SawMill/regutils.scm 62 */
									BgL_objectz00_bglt BgL_arg1807z00_2517;

									BgL_arg1807z00_2517 = (BgL_objectz00_bglt) (BgL_oz00_1988);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* SawMill/regutils.scm 62 */
											long BgL_idxz00_2523;

											BgL_idxz00_2523 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2517);
											BgL_test1909z00_3263 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2523 + 1L)) == BgL_classz00_2515);
										}
									else
										{	/* SawMill/regutils.scm 62 */
											bool_t BgL_res1846z00_2548;

											{	/* SawMill/regutils.scm 62 */
												obj_t BgL_oclassz00_2531;

												{	/* SawMill/regutils.scm 62 */
													obj_t BgL_arg1815z00_2539;
													long BgL_arg1816z00_2540;

													BgL_arg1815z00_2539 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* SawMill/regutils.scm 62 */
														long BgL_arg1817z00_2541;

														BgL_arg1817z00_2541 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2517);
														BgL_arg1816z00_2540 =
															(BgL_arg1817z00_2541 - OBJECT_TYPE);
													}
													BgL_oclassz00_2531 =
														VECTOR_REF(BgL_arg1815z00_2539,
														BgL_arg1816z00_2540);
												}
												{	/* SawMill/regutils.scm 62 */
													bool_t BgL__ortest_1115z00_2532;

													BgL__ortest_1115z00_2532 =
														(BgL_classz00_2515 == BgL_oclassz00_2531);
													if (BgL__ortest_1115z00_2532)
														{	/* SawMill/regutils.scm 62 */
															BgL_res1846z00_2548 = BgL__ortest_1115z00_2532;
														}
													else
														{	/* SawMill/regutils.scm 62 */
															long BgL_odepthz00_2533;

															{	/* SawMill/regutils.scm 62 */
																obj_t BgL_arg1804z00_2534;

																BgL_arg1804z00_2534 = (BgL_oclassz00_2531);
																BgL_odepthz00_2533 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2534);
															}
															if ((1L < BgL_odepthz00_2533))
																{	/* SawMill/regutils.scm 62 */
																	obj_t BgL_arg1802z00_2536;

																	{	/* SawMill/regutils.scm 62 */
																		obj_t BgL_arg1803z00_2537;

																		BgL_arg1803z00_2537 = (BgL_oclassz00_2531);
																		BgL_arg1802z00_2536 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2537, 1L);
																	}
																	BgL_res1846z00_2548 =
																		(BgL_arg1802z00_2536 == BgL_classz00_2515);
																}
															else
																{	/* SawMill/regutils.scm 62 */
																	BgL_res1846z00_2548 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1909z00_3263 = BgL_res1846z00_2548;
										}
								}
							else
								{	/* SawMill/regutils.scm 62 */
									BgL_test1909z00_3263 = ((bool_t) 0);
								}
						}
						if (BgL_test1909z00_3263)
							{	/* SawMill/regutils.scm 64 */
								obj_t BgL_arg1453z00_1993;
								obj_t BgL_arg1454z00_1994;

								{	/* SawMill/regutils.scm 64 */
									obj_t BgL_arg1472z00_1995;

									BgL_arg1472z00_1995 =
										(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_oz00_1988)))->BgL_destz00);
									BgL_arg1453z00_1993 =
										BGl_argszd2collectzd2registersz12ze70zf5zzsaw_regutilsz00
										(BgL_envz00_2723, BgL_arg1472z00_1995);
								}
								{	/* SawMill/regutils.scm 65 */
									obj_t BgL_arg1473z00_1996;

									BgL_arg1473z00_1996 =
										(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_oz00_1988)))->BgL_argsz00);
									{	/* SawMill/regutils.scm 65 */
										obj_t BgL_list1474z00_1997;

										BgL_list1474z00_1997 =
											MAKE_YOUNG_PAIR(BgL_arg1473z00_1996, BNIL);
										BgL_arg1454z00_1994 =
											BGl_appendzd2mapz12zc0zz__r4_control_features_6_9z00
											(BgL_envz00_2723, BgL_list1474z00_1997);
									}
								}
								return
									BGl_appendzd22z12zc0zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1453z00_1993, BgL_arg1454z00_1994);
							}
						else
							{	/* SawMill/regutils.scm 62 */
								return BNIL;
							}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_regutilsz00(void)
	{
		{	/* SawMill/regutils.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_regutilsz00(void)
	{
		{	/* SawMill/regutils.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_regutilsz00(void)
	{
		{	/* SawMill/regutils.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_regutilsz00(void)
	{
		{	/* SawMill/regutils.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1850z00zzsaw_regutilsz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1850z00zzsaw_regutilsz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1850z00zzsaw_regutilsz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1850z00zzsaw_regutilsz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1850z00zzsaw_regutilsz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1850z00zzsaw_regutilsz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1850z00zzsaw_regutilsz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1850z00zzsaw_regutilsz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1850z00zzsaw_regutilsz00));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string1850z00zzsaw_regutilsz00));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string1850z00zzsaw_regutilsz00));
			BGl_modulezd2initializa7ationz75zzsaw_node2rtlz00(20930040L,
				BSTRING_TO_STRING(BGl_string1850z00zzsaw_regutilsz00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_regsetz00(358079690L,
				BSTRING_TO_STRING(BGl_string1850z00zzsaw_regutilsz00));
		}

	}

#ifdef __cplusplus
}
#endif
