/*===========================================================================*/
/*   (SawMill/regalloc.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/regalloc.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_SAW_REGISTERzd2ALLOCATIONzd2_TYPE_DEFINITIONS
#define BGL_BgL_SAW_REGISTERzd2ALLOCATIONzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_nopz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_nopz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_rtl_regzf2razf2_bgl
	{
		int BgL_numz00;
		obj_t BgL_colorz00;
		obj_t BgL_coalescez00;
		int BgL_occurrencesz00;
		obj_t BgL_interferez00;
		obj_t BgL_interfere2z00;
	}                      *BgL_rtl_regzf2razf2_bglt;

	typedef struct BgL_regsetz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_lengthz00;
		int BgL_msiza7eza7;
		obj_t BgL_regvz00;
		obj_t BgL_reglz00;
		obj_t BgL_stringz00;
	}                *BgL_regsetz00_bglt;

	typedef struct BgL_blockzf2razf2_bgl
	{
		obj_t BgL_lastz00;
	}                    *BgL_blockzf2razf2_bglt;

	typedef struct BgL_rtl_inszf2razf2_bgl
	{
		obj_t BgL_defz00;
		obj_t BgL_outz00;
		obj_t BgL_inz00;
		obj_t BgL_spillz00;
	}                      *BgL_rtl_inszf2razf2_bglt;


#endif													// BGL_BgL_SAW_REGISTERzd2ALLOCATIONzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	extern obj_t BGl_rtl_funz00zzsaw_defsz00;
	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	static obj_t
		BGl_z62zc3z04anonymousza32100ze3ze5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t
		BGl_registerzd2hardwarezd2coloringz00zzsaw_registerzd2allocationzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_registerzd2allocationzd2 =
		BUNSPEC;
	extern bool_t BGl_regsetzd2addz12zc0zzsaw_regsetz00(BgL_regsetz00_bglt,
		BgL_rtl_regz00_bglt);
	static obj_t
		BGl_rtlzd2mapzd2registersz12z12zzsaw_registerzd2allocationzd2(obj_t, obj_t,
		obj_t);
	extern obj_t
		BGl_za2sawzd2registerzd2allocationzd2functionsza2zd2zzengine_paramz00;
	extern obj_t BGl_collectzd2registersz12zc0zzsaw_regutilsz00(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza32399ze3ze5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t
		BGl_z62zc3z04anonymousza32005ze3ze5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	static bool_t
		BGl_cleanupzd2movezd2treez12z12zzsaw_registerzd2allocationzd2(obj_t);
	static obj_t
		BGl_z62protectzd2interferencez12za2zzsaw_registerzd2allocationzd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_funcallzd2spillz12zc0zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_assocz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzsaw_registerzd2allocationzd2(void);
	extern obj_t BGl_rtl_protectz00zzsaw_defsz00;
	static obj_t
		BGl_simplifyzd2regz12zc0zzsaw_registerzd2allocationzd2(BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t
		BGl_z62typezd2interferencez12za2zzsaw_registerzd2allocationzd2(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t
		BGl_z62registerzd2allocationzb0zzsaw_registerzd2allocationzd2(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_registerzd2allocationzd2(void);
	static obj_t BGl_blockzf2razf2zzsaw_registerzd2allocationzd2 = BUNSPEC;
	static obj_t
		BGl_registerzd2stackzd2coloringz00zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzsaw_registerzd2allocationzd2(void);
	BGL_IMPORT obj_t BGl_appendz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_interferencez12z12zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	extern obj_t BGl_regsetzd2filterzd2zzsaw_regsetz00(obj_t, BgL_regsetz00_bglt);
	extern obj_t BGl_typez00zztype_typez00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	extern obj_t BGl_rtl_movz00zzsaw_defsz00;
	extern bool_t BGl_regsetzd2removez12zc0zzsaw_regsetz00(BgL_regsetz00_bglt,
		BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t bgl_bignum_add(obj_t, obj_t);
	static bool_t
		BGl_inszd2hardwarezd2interferencez12ze70zf5zzsaw_registerzd2allocationzd2
		(BgL_backendz00_bglt, BgL_rtl_insz00_bglt, obj_t, obj_t);
	static bool_t
		BGl_hardwarezd2mutualzd2interferencez12z12zzsaw_registerzd2allocationzd2
		(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t make_vector(long, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	extern obj_t BGl_rtl_nopz00zzsaw_defsz00;
	static obj_t BGl_z62rtlzd2siza7ez17zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_registerzd2allocationzd2(void);
	extern bool_t BGl_regsetzd2unionza2z12z62zzsaw_regsetz00(BgL_regsetz00_bglt,
		obj_t);
	static bool_t
		BGl_hardwarezd2parameterszd2interferencez12z12zzsaw_registerzd2allocationzd2
		(BgL_backendz00_bglt, obj_t);
	extern obj_t BGl_rtl_inszd2argsza2z70zzsaw_defsz00(BgL_rtl_insz00_bglt);
	static obj_t BGl_livenessz12z12zzsaw_registerzd2allocationzd2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_registerzd2allocationzd2zzsaw_registerzd2allocationzd2
		(BgL_backendz00_bglt, BgL_globalz00_bglt, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_long_to_bignum(long);
	BGL_IMPORT obj_t BGl_zb2zb2zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_registerzd2coloringzd2zzsaw_registerzd2allocationzd2(obj_t,
		obj_t, obj_t);
	extern BgL_regsetz00_bglt BGl_listzd2ze3regsetz31zzsaw_regsetz00(obj_t,
		obj_t);
	static obj_t
		BGl_argszd2cleanupzd2movezd2treez12ze70z27zzsaw_registerzd2allocationzd2
		(obj_t);
	static obj_t BGl_cleanupzd2destz12zc0zzsaw_registerzd2allocationzd2(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza32090ze3ze5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t
		BGl_z62rtlzd2siza7ezd2rtl_ins1718zc5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_appendzd2mapz12zc0zz__r4_control_features_6_9z00(obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_remq_bang(obj_t, obj_t);
	static obj_t
		BGl_z62typezd2interferencez121711za2zzsaw_registerzd2allocationzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_mapzd2registerze70z35zzsaw_registerzd2allocationzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza32173ze3ze5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza32246ze3ze5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_registerzd2allocationzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_node2rtlz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static bool_t
		BGl_z52registerzd2allocationz80zzsaw_registerzd2allocationzd2
		(BgL_backendz00_bglt, BgL_globalz00_bglt, obj_t, obj_t);
	static obj_t
		BGl_z62protectzd2interference1709zb0zzsaw_registerzd2allocationzd2(obj_t,
		obj_t, obj_t);
	static BgL_rtl_regz00_bglt
		BGl_usezd2registerz12zc0zzsaw_registerzd2allocationzd2(BgL_rtl_regz00_bglt);
	BGL_IMPORT obj_t BGl_sortz00zz__r4_vectors_6_8z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_typezd2interferencez12zc0zzsaw_registerzd2allocationzd2
		(BgL_backendz00_bglt, obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza32530ze3ze5zzsaw_registerzd2allocationzd2(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza32522ze3ze5zzsaw_registerzd2allocationzd2(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza32514ze3ze5zzsaw_registerzd2allocationzd2(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza32506ze3ze5zzsaw_registerzd2allocationzd2(obj_t);
	extern obj_t BGl_za2sawzd2registerzd2allocationzf3za2zf3zzengine_paramz00;
	BGL_IMPORT obj_t BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00(obj_t, obj_t,
		long, long);
	static obj_t
		BGl_z62zc3z04anonymousza32094ze3ze5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza32159ze3ze5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t, obj_t);
	static obj_t BGl_rtlzd2siza7ez75zzsaw_registerzd2allocationzd2(obj_t);
	static obj_t
		BGl_z62rtlzd2siza7ezd2block1716zc5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	extern obj_t BGl_regsetzd2ze3listz31zzsaw_regsetz00(BgL_regsetz00_bglt);
	static obj_t BGl_cnstzd2initzd2zzsaw_registerzd2allocationzd2(void);
	static obj_t
		BGl_libraryzd2moduleszd2initz00zzsaw_registerzd2allocationzd2(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t
		BGl_importedzd2moduleszd2initz00zzsaw_registerzd2allocationzd2(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_registerzd2allocationzd2(void);
	extern BgL_regsetz00_bglt
		BGl_duplicatezd2regsetzd2zzsaw_regsetz00(BgL_regsetz00_bglt);
	static obj_t
		BGl_z62zc3z04anonymousza32266ze3ze5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	extern obj_t
		BGl_za2sawzd2nozd2registerzd2allocationzd2functionsza2z00zzengine_paramz00;
	BGL_IMPORT obj_t bgl_remq(obj_t, obj_t);
	extern obj_t BGl_regsetzd2forzd2eachz00zzsaw_regsetz00(obj_t,
		BgL_regsetz00_bglt);
	extern obj_t BGl_backendz00zzbackend_backendz00;
	static obj_t BGl_z62rtlzd2siza7e1713z17zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza32097ze3ze5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	static obj_t BGl_rtl_inszf2razf2zzsaw_registerzd2allocationzd2 = BUNSPEC;
	static bool_t BGl_interferezd2regz12zc0zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2504z62zzsaw_registerzd2allocationzd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2505z62zzsaw_registerzd2allocationzd2(obj_t, obj_t,
		obj_t);
	static bool_t
		BGl_inszd2funcallzd2spillz12ze70zf5zzsaw_registerzd2allocationzd2
		(BgL_rtl_insz00_bglt);
	static bool_t
		BGl_argszd2widenzd2raz12ze70zf5zzsaw_registerzd2allocationzd2(obj_t, obj_t);
	static obj_t BGl_livenesszd2blockz12zc0zzsaw_registerzd2allocationzd2(obj_t);
	static obj_t BGl_z62lambda2512z62zzsaw_registerzd2allocationzd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2513z62zzsaw_registerzd2allocationzd2(obj_t, obj_t,
		obj_t);
	extern obj_t
		BGl_backendzd2instrzd2resetzd2registerszd2zzbackend_backendz00
		(BgL_backendz00_bglt, obj_t);
	static obj_t BGl_z62lambda2520z62zzsaw_registerzd2allocationzd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2521z62zzsaw_registerzd2allocationzd2(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_z62lambda2528z62zzsaw_registerzd2allocationzd2(obj_t, obj_t);
	static bool_t BGl_widenzd2raz12zc0zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2529z62zzsaw_registerzd2allocationzd2(obj_t, obj_t,
		obj_t);
	extern obj_t
		BGl_za2sawzd2registerzd2allocationzd2maxzd2siza7eza2za7zzengine_paramz00;
	extern bool_t BGl_regsetzd2unionz12zc0zzsaw_regsetz00(BgL_regsetz00_bglt,
		BgL_regsetz00_bglt);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_interferezd2insz12zc0zzsaw_registerzd2allocationzd2
		(BgL_rtl_insz00_bglt);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	static obj_t
		BGl_z62dumpzd2rtl_inszf2ra1720z42zzsaw_registerzd2allocationzd2(obj_t,
		obj_t, obj_t, obj_t);
	extern BgL_regsetz00_bglt BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00(obj_t);
	extern obj_t BGl_rtl_regzf2razf2zzsaw_regsetz00;
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static BgL_blockz00_bglt
		BGl_z62lambda2461z62zzsaw_registerzd2allocationzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static long BGl_removezd2blockz12ze70z27zzsaw_registerzd2allocationzd2(obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza32466ze3ze5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	static BgL_blockz00_bglt
		BGl_z62lambda2464z62zzsaw_registerzd2allocationzd2(obj_t, obj_t);
	static BgL_blockz00_bglt
		BGl_z62lambda2467z62zzsaw_registerzd2allocationzd2(obj_t, obj_t);
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza32491ze3ze5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2476z62zzsaw_registerzd2allocationzd2(obj_t, obj_t);
	static obj_t BGl_z62lambda2477z62zzsaw_registerzd2allocationzd2(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza32000ze3ze5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	extern obj_t BGl_blockz00zzsaw_defsz00;
	static obj_t
		BGl_removezd2nopzd2movez12z12zzsaw_registerzd2allocationzd2(obj_t);
	static bool_t
		BGl_hardwarezd2interferencez12zc0zzsaw_registerzd2allocationzd2
		(BgL_backendz00_bglt, obj_t);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static BgL_rtl_insz00_bglt
		BGl_z62lambda2485z62zzsaw_registerzd2allocationzd2(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_rtl_insz00_bglt
		BGl_z62lambda2488z62zzsaw_registerzd2allocationzd2(obj_t, obj_t);
	static obj_t BGl_getzd2argsze70z35zzsaw_registerzd2allocationzd2(obj_t);
	static obj_t
		BGl_protectzd2interferencez12zc0zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62zc3z04anonymousza31950ze3ze5zzsaw_registerzd2allocationzd2(obj_t,
		obj_t);
	static BgL_rtl_insz00_bglt
		BGl_z62lambda2492z62zzsaw_registerzd2allocationzd2(obj_t, obj_t);
	static obj_t __cnst[10];


	   
		 
		DEFINE_STATIC_BGL_GENERIC
		(BGl_protectzd2interferencez12zd2envz12zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762protectza7d2int2845z00,
		BGl_z62protectzd2interferencez12za2zzsaw_registerzd2allocationzd2, 0L,
		BUNSPEC, 2);
	extern obj_t BGl_dumpzd2envzd2zzsaw_defsz00;
	   
		 
		DEFINE_STRING(BGl_string2800z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2800za700za7za7s2846za7, " (of ", 5);
	      DEFINE_STRING(BGl_string2801z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2801za700za7za7s2847za7, "          stack coloring... ", 28);
	      DEFINE_STRING(BGl_string2802z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2802za700za7za7s2848za7, " spills", 7);
	      DEFINE_STRING(BGl_string2803z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2803za700za7za7s2849za7, " regs, ", 7);
	      DEFINE_STRING(BGl_string2804z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2804za700za7za7s2850za7, " temps -> ", 10);
	      DEFINE_STRING(BGl_string2805z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2805za700za7za7s2851za7, " h-regs, ", 9);
	      DEFINE_STRING(BGl_string2806z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2806za700za7za7s2852za7, "          register coloring... ",
		31);
	      DEFINE_STRING(BGl_string2807z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2807za700za7za7s2853za7, " removed\n", 9);
	      DEFINE_STRING(BGl_string2808z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2808za700za7za7s2854za7, "          move... ", 18);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_registerzd2allocationzd2envz00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762registerza7d2al2855z00,
		BGl_z62registerzd2allocationzb0zzsaw_registerzd2allocationzd2, 0L, BUNSPEC,
		4);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2809z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2477za7622856z00,
		BGl_z62lambda2477z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2810z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2476za7622857z00,
		BGl_z62lambda2476z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2811z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2467za7622858z00,
		BGl_z62lambda2467z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2812z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762za7c3za704anonymo2859za7,
		BGl_z62zc3z04anonymousza32466ze3ze5zzsaw_registerzd2allocationzd2, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2813z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2464za7622860z00,
		BGl_z62lambda2464z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2814z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2461za7622861z00,
		BGl_z62lambda2461z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 5);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2815z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762za7c3za704anonymo2862za7,
		BGl_z62zc3z04anonymousza32506ze3ze5zzsaw_registerzd2allocationzd2, 0L,
		BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2816z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2505za7622863z00,
		BGl_z62lambda2505z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2817z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2504za7622864z00,
		BGl_z62lambda2504z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2818z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762za7c3za704anonymo2865za7,
		BGl_z62zc3z04anonymousza32514ze3ze5zzsaw_registerzd2allocationzd2, 0L,
		BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2819z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2513za7622866z00,
		BGl_z62lambda2513z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2820z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2512za7622867z00,
		BGl_z62lambda2512z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2821z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762za7c3za704anonymo2868za7,
		BGl_z62zc3z04anonymousza32522ze3ze5zzsaw_registerzd2allocationzd2, 0L,
		BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2822z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2521za7622869z00,
		BGl_z62lambda2521z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2823z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2520za7622870z00,
		BGl_z62lambda2520z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2824z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762za7c3za704anonymo2871za7,
		BGl_z62zc3z04anonymousza32530ze3ze5zzsaw_registerzd2allocationzd2, 0L,
		BUNSPEC, 0);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2825z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2529za7622872z00,
		BGl_z62lambda2529z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2832z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2832za700za7za7s2873za7, "protect-interference1709", 24);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2826z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2528za7622874z00,
		BGl_z62lambda2528z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2827z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2492za7622875z00,
		BGl_z62lambda2492z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2834z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2834za700za7za7s2876za7, "type-interference!1711", 22);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2828z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762za7c3za704anonymo2877za7,
		BGl_z62zc3z04anonymousza32491ze3ze5zzsaw_registerzd2allocationzd2, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2829z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2488za7622878z00,
		BGl_z62lambda2488z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2836z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2836za700za7za7s2879za7, "rtl-size1713", 12);
	      DEFINE_STRING(BGl_string2838z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2838za700za7za7s2880za7, "rtl-size", 8);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2830z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762lambda2485za7622881z00,
		BGl_z62lambda2485z62zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 9);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2831z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762protectza7d2int2882z00,
		BGl_z62protectzd2interference1709zb0zzsaw_registerzd2allocationzd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2833z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762typeza7d2interf2883z00,
		BGl_z62typezd2interferencez121711za2zzsaw_registerzd2allocationzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2841z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2841za700za7za7s2884za7, "dump", 4);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2835z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762rtlza7d2siza7a7e12885za7,
		BGl_z62rtlzd2siza7e1713z17zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2842z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2842za700za7za7s2886za7, "saw_register-allocation", 23);
	      DEFINE_STRING(BGl_string2843z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2843za700za7za7s2887za7,
		"rtl_ins/ra spill in out obj def saw_register-allocation block/ra pair-nil last ",
		79);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2837z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762rtlza7d2siza7a7eza72888z00,
		BGl_z62rtlzd2siza7ezd2block1716zc5zzsaw_registerzd2allocationzd2, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2839z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762rtlza7d2siza7a7eza72889z00,
		BGl_z62rtlzd2siza7ezd2rtl_ins1718zc5zzsaw_registerzd2allocationzd2, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2840z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762dumpza7d2rtl_in2890z00,
		BGl_z62dumpzd2rtl_inszf2ra1720z42zzsaw_registerzd2allocationzd2, 0L,
		BUNSPEC, 3);
	extern obj_t BGl_collectzd2registerz12zd2envz12zzsaw_regutilsz00;
	   
		 
		DEFINE_STRING(BGl_string2781z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2781za700za7za7s2891za7, "\n", 1);
	      DEFINE_STRING(BGl_string2782z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2782za700za7za7s2892za7, "        no reg. alloc. ", 23);
	      DEFINE_STRING(BGl_string2783z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2783za700za7za7s2893za7, "blacklist", 9);
	      DEFINE_STRING(BGl_string2784z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2784za700za7za7s2894za7, "too large [~a/~a]", 17);
	      DEFINE_STRING(BGl_string2785z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2785za700za7za7s2895za7, " instrs]:\n", 10);
	      DEFINE_STRING(BGl_string2786z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2786za700za7za7s2896za7, " [size=", 7);
	      DEFINE_STRING(BGl_string2787z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2787za700za7za7s2897za7, "        reg. alloc. ", 20);
	      DEFINE_STRING(BGl_string2788z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2788za700za7za7s2898za7, "          number of registers... ",
		33);
	      DEFINE_STRING(BGl_string2789z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2789za700za7za7s2899za7, "          number of parameters... ",
		34);
	      DEFINE_STRING(BGl_string2790z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2790za700za7za7s2900za7,
		"          number of temporaries... ", 35);
	      DEFINE_STRING(BGl_string2791z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2791za700za7za7s2901za7, "          liveness..", 20);
	      DEFINE_STRING(BGl_string2792z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2792za700za7za7s2902za7, ".", 1);
	      DEFINE_STRING(BGl_string2793z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2793za700za7za7s2903za7, " done\n", 6);
	      DEFINE_STRING(BGl_string2794z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2794za700za7za7s2904za7, "          graph interference... ",
		32);
	      DEFINE_STRING(BGl_string2795z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2795za700za7za7s2905za7, "done\n", 5);
	      DEFINE_STRING(BGl_string2796z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2796za700za7za7s2906za7,
		"          removed useless dest. registers... ", 45);
	      DEFINE_STRING(BGl_string2798z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2798za700za7za7s2907za7, " registers\n", 11);
	      DEFINE_STRING(BGl_string2799z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_string2799za700za7za7s2908za7, ")", 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2797z00zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762za7c3za704anonymo2909za7,
		BGl_z62zc3z04anonymousza32159ze3ze5zzsaw_registerzd2allocationzd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_typezd2interferencez12zd2envz12zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762typeza7d2interf2910z00,
		BGl_z62typezd2interferencez12za2zzsaw_registerzd2allocationzd2, 0L, BUNSPEC,
		2);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_rtlzd2siza7ezd2envza7zzsaw_registerzd2allocationzd2,
		BgL_bgl_za762rtlza7d2siza7a7eza72911z00,
		BGl_z62rtlzd2siza7ez17zzsaw_registerzd2allocationzd2, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzsaw_registerzd2allocationzd2));
		     ADD_ROOT((void *) (&BGl_blockzf2razf2zzsaw_registerzd2allocationzd2));
		   
			 ADD_ROOT((void *) (&BGl_rtl_inszf2razf2zzsaw_registerzd2allocationzd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzsaw_registerzd2allocationzd2(long
		BgL_checksumz00_6266, char *BgL_fromz00_6267)
	{
		{
			if (CBOOL
				(BGl_requirezd2initializa7ationz75zzsaw_registerzd2allocationzd2))
				{
					BGl_requirezd2initializa7ationz75zzsaw_registerzd2allocationzd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_registerzd2allocationzd2();
					BGl_libraryzd2moduleszd2initz00zzsaw_registerzd2allocationzd2();
					BGl_cnstzd2initzd2zzsaw_registerzd2allocationzd2();
					BGl_importedzd2moduleszd2initz00zzsaw_registerzd2allocationzd2();
					BGl_objectzd2initzd2zzsaw_registerzd2allocationzd2();
					BGl_genericzd2initzd2zzsaw_registerzd2allocationzd2();
					BGl_methodzd2initzd2zzsaw_registerzd2allocationzd2();
					return BGl_toplevelzd2initzd2zzsaw_registerzd2allocationzd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_registerzd2allocationzd2(void)
	{
		{	/* SawMill/regalloc.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L,
				"saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L,
				"saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L,
				"saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_flonumz00(0L,
				"saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_register-allocation");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_register-allocation");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_registerzd2allocationzd2(void)
	{
		{	/* SawMill/regalloc.scm 15 */
			{	/* SawMill/regalloc.scm 15 */
				obj_t BgL_cportz00_6053;

				{	/* SawMill/regalloc.scm 15 */
					obj_t BgL_stringz00_6060;

					BgL_stringz00_6060 = BGl_string2843z00zzsaw_registerzd2allocationzd2;
					{	/* SawMill/regalloc.scm 15 */
						obj_t BgL_startz00_6061;

						BgL_startz00_6061 = BINT(0L);
						{	/* SawMill/regalloc.scm 15 */
							obj_t BgL_endz00_6062;

							BgL_endz00_6062 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_6060)));
							{	/* SawMill/regalloc.scm 15 */

								BgL_cportz00_6053 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_6060, BgL_startz00_6061, BgL_endz00_6062);
				}}}}
				{
					long BgL_iz00_6054;

					BgL_iz00_6054 = 9L;
				BgL_loopz00_6055:
					if ((BgL_iz00_6054 == -1L))
						{	/* SawMill/regalloc.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* SawMill/regalloc.scm 15 */
							{	/* SawMill/regalloc.scm 15 */
								obj_t BgL_arg2844z00_6056;

								{	/* SawMill/regalloc.scm 15 */

									{	/* SawMill/regalloc.scm 15 */
										obj_t BgL_locationz00_6058;

										BgL_locationz00_6058 = BBOOL(((bool_t) 0));
										{	/* SawMill/regalloc.scm 15 */

											BgL_arg2844z00_6056 =
												BGl_readz00zz__readerz00(BgL_cportz00_6053,
												BgL_locationz00_6058);
										}
									}
								}
								{	/* SawMill/regalloc.scm 15 */
									int BgL_tmpz00_6305;

									BgL_tmpz00_6305 = (int) (BgL_iz00_6054);
									CNST_TABLE_SET(BgL_tmpz00_6305, BgL_arg2844z00_6056);
							}}
							{	/* SawMill/regalloc.scm 15 */
								int BgL_auxz00_6059;

								BgL_auxz00_6059 = (int) ((BgL_iz00_6054 - 1L));
								{
									long BgL_iz00_6310;

									BgL_iz00_6310 = (long) (BgL_auxz00_6059);
									BgL_iz00_6054 = BgL_iz00_6310;
									goto BgL_loopz00_6055;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_registerzd2allocationzd2(void)
	{
		{	/* SawMill/regalloc.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_registerzd2allocationzd2(void)
	{
		{	/* SawMill/regalloc.scm 15 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzsaw_registerzd2allocationzd2(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_2051;

				BgL_headz00_2051 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2052;
					obj_t BgL_tailz00_2053;

					BgL_prevz00_2052 = BgL_headz00_2051;
					BgL_tailz00_2053 = BgL_l1z00_1;
				BgL_loopz00_2054:
					if (PAIRP(BgL_tailz00_2053))
						{
							obj_t BgL_newzd2prevzd2_2056;

							BgL_newzd2prevzd2_2056 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2053), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2052, BgL_newzd2prevzd2_2056);
							{
								obj_t BgL_tailz00_6320;
								obj_t BgL_prevz00_6319;

								BgL_prevz00_6319 = BgL_newzd2prevzd2_2056;
								BgL_tailz00_6320 = CDR(BgL_tailz00_2053);
								BgL_tailz00_2053 = BgL_tailz00_6320;
								BgL_prevz00_2052 = BgL_prevz00_6319;
								goto BgL_loopz00_2054;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_2051);
				}
			}
		}

	}



/* register-allocation */
	BGL_EXPORTED_DEF obj_t
		BGl_registerzd2allocationzd2zzsaw_registerzd2allocationzd2
		(BgL_backendz00_bglt BgL_backz00_61, BgL_globalz00_bglt BgL_globalz00_62,
		obj_t BgL_paramsz00_63, obj_t BgL_blocksz00_64)
	{
		{	/* SawMill/regalloc.scm 50 */
			{
				obj_t BgL_msgz00_2083;

				{	/* SawMill/regalloc.scm 55 */
					obj_t BgL_idz00_2072;

					BgL_idz00_2072 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_globalz00_62)))->BgL_idz00);
					if (CBOOL
						(BGl_za2sawzd2registerzd2allocationzf3za2zf3zzengine_paramz00))
						{	/* SawMill/regalloc.scm 56 */
							if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(BgL_idz00_2072,
										BGl_za2sawzd2nozd2registerzd2allocationzd2functionsza2z00zzengine_paramz00)))
								{	/* SawMill/regalloc.scm 58 */
									BgL_msgz00_2083 =
										BGl_string2783z00zzsaw_registerzd2allocationzd2;
								BgL_zc3z04anonymousza31751ze3z87_2084:
									{	/* SawMill/regalloc.scm 53 */
										obj_t BgL_list1752z00_2085;

										{	/* SawMill/regalloc.scm 53 */
											obj_t BgL_arg1753z00_2086;

											{	/* SawMill/regalloc.scm 53 */
												obj_t BgL_arg1754z00_2087;

												BgL_arg1754z00_2087 =
													MAKE_YOUNG_PAIR
													(BGl_string2781z00zzsaw_registerzd2allocationzd2,
													BNIL);
												BgL_arg1753z00_2086 =
													MAKE_YOUNG_PAIR(BgL_msgz00_2083, BgL_arg1754z00_2087);
											}
											BgL_list1752z00_2085 =
												MAKE_YOUNG_PAIR
												(BGl_string2782z00zzsaw_registerzd2allocationzd2,
												BgL_arg1753z00_2086);
										}
										BGl_verbosez00zztools_speekz00(BINT(2L),
											BgL_list1752z00_2085);
									}
								}
							else
								{	/* SawMill/regalloc.scm 58 */
									if (PAIRP
										(BGl_za2sawzd2registerzd2allocationzd2functionsza2zd2zzengine_paramz00))
										{	/* SawMill/regalloc.scm 60 */
											BBOOL
												(BGl_z52registerzd2allocationz80zzsaw_registerzd2allocationzd2
												(BgL_backz00_61, BgL_globalz00_62, BgL_paramsz00_63,
													BgL_blocksz00_64));
										}
									else
										{	/* SawMill/regalloc.scm 60 */
											if (
												((long)
													CINT
													(BGl_za2sawzd2registerzd2allocationzd2maxzd2siza7eza2za7zzengine_paramz00)
													< 0L))
												{	/* SawMill/regalloc.scm 62 */
													BBOOL
														(BGl_z52registerzd2allocationz80zzsaw_registerzd2allocationzd2
														(BgL_backz00_61, BgL_globalz00_62, BgL_paramsz00_63,
															BgL_blocksz00_64));
												}
											else
												{	/* SawMill/regalloc.scm 64 */
													bool_t BgL_test2919z00_6344;

													{	/* SawMill/regalloc.scm 64 */
														obj_t BgL_arg1750z00_2082;

														BgL_arg1750z00_2082 =
															BGl_rtlzd2siza7ez75zzsaw_registerzd2allocationzd2
															(BgL_blocksz00_64);
														BgL_test2919z00_6344 =
															((long) CINT(BgL_arg1750z00_2082) >=
															(long)
															CINT
															(BGl_za2sawzd2registerzd2allocationzd2maxzd2siza7eza2za7zzengine_paramz00));
													}
													if (BgL_test2919z00_6344)
														{	/* SawMill/regalloc.scm 67 */
															obj_t BgL_arg1746z00_2078;

															{	/* SawMill/regalloc.scm 67 */
																obj_t BgL_arg1747z00_2079;

																BgL_arg1747z00_2079 =
																	BGl_rtlzd2siza7ez75zzsaw_registerzd2allocationzd2
																	(BgL_blocksz00_64);
																{	/* SawMill/regalloc.scm 66 */
																	obj_t BgL_list1748z00_2080;

																	{	/* SawMill/regalloc.scm 66 */
																		obj_t BgL_arg1749z00_2081;

																		BgL_arg1749z00_2081 =
																			MAKE_YOUNG_PAIR
																			(BGl_za2sawzd2registerzd2allocationzd2maxzd2siza7eza2za7zzengine_paramz00,
																			BNIL);
																		BgL_list1748z00_2080 =
																			MAKE_YOUNG_PAIR(BgL_arg1747z00_2079,
																			BgL_arg1749z00_2081);
																	}
																	BgL_arg1746z00_2078 =
																		BGl_formatz00zz__r4_output_6_10_3z00
																		(BGl_string2784z00zzsaw_registerzd2allocationzd2,
																		BgL_list1748z00_2080);
																}
															}
															{
																obj_t BgL_msgz00_6353;

																BgL_msgz00_6353 = BgL_arg1746z00_2078;
																BgL_msgz00_2083 = BgL_msgz00_6353;
																goto BgL_zc3z04anonymousza31751ze3z87_2084;
															}
														}
													else
														{	/* SawMill/regalloc.scm 64 */
															BBOOL
																(BGl_z52registerzd2allocationz80zzsaw_registerzd2allocationzd2
																(BgL_backz00_61, BgL_globalz00_62,
																	BgL_paramsz00_63, BgL_blocksz00_64));
														}
												}
										}
								}
						}
					else
						{	/* SawMill/regalloc.scm 56 */
							BFALSE;
						}
					return BgL_blocksz00_64;
				}
			}
		}

	}



/* &register-allocation */
	obj_t BGl_z62registerzd2allocationzb0zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5872, obj_t BgL_backz00_5873, obj_t BgL_globalz00_5874,
		obj_t BgL_paramsz00_5875, obj_t BgL_blocksz00_5876)
	{
		{	/* SawMill/regalloc.scm 50 */
			return
				BGl_registerzd2allocationzd2zzsaw_registerzd2allocationzd2(
				((BgL_backendz00_bglt) BgL_backz00_5873),
				((BgL_globalz00_bglt) BgL_globalz00_5874), BgL_paramsz00_5875,
				BgL_blocksz00_5876);
		}

	}



/* %register-allocation */
	bool_t
		BGl_z52registerzd2allocationz80zzsaw_registerzd2allocationzd2
		(BgL_backendz00_bglt BgL_backz00_65, BgL_globalz00_bglt BgL_globalz00_66,
		obj_t BgL_paramsz00_67, obj_t BgL_blocksz00_68)
	{
		{	/* SawMill/regalloc.scm 75 */
			{	/* SawMill/regalloc.scm 77 */
				obj_t BgL_arg1755z00_2089;
				obj_t BgL_arg1761z00_2090;

				BgL_arg1755z00_2089 =
					BGl_shapez00zztools_shapez00(((obj_t) BgL_globalz00_66));
				BgL_arg1761z00_2090 =
					BGl_rtlzd2siza7ez75zzsaw_registerzd2allocationzd2(BgL_blocksz00_68);
				{	/* SawMill/regalloc.scm 76 */
					obj_t BgL_list1762z00_2091;

					{	/* SawMill/regalloc.scm 76 */
						obj_t BgL_arg1765z00_2092;

						{	/* SawMill/regalloc.scm 76 */
							obj_t BgL_arg1767z00_2093;

							{	/* SawMill/regalloc.scm 76 */
								obj_t BgL_arg1770z00_2094;

								{	/* SawMill/regalloc.scm 76 */
									obj_t BgL_arg1771z00_2095;

									BgL_arg1771z00_2095 =
										MAKE_YOUNG_PAIR
										(BGl_string2785z00zzsaw_registerzd2allocationzd2, BNIL);
									BgL_arg1770z00_2094 =
										MAKE_YOUNG_PAIR(BgL_arg1761z00_2090, BgL_arg1771z00_2095);
								}
								BgL_arg1767z00_2093 =
									MAKE_YOUNG_PAIR
									(BGl_string2786z00zzsaw_registerzd2allocationzd2,
									BgL_arg1770z00_2094);
							}
							BgL_arg1765z00_2092 =
								MAKE_YOUNG_PAIR(BgL_arg1755z00_2089, BgL_arg1767z00_2093);
						}
						BgL_list1762z00_2091 =
							MAKE_YOUNG_PAIR(BGl_string2787z00zzsaw_registerzd2allocationzd2,
							BgL_arg1765z00_2092);
					}
					BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1762z00_2091);
				}
			}
			BGl_cleanupzd2movezd2treez12z12zzsaw_registerzd2allocationzd2
				(BgL_blocksz00_68);
			{	/* SawMill/regalloc.scm 81 */
				obj_t BgL_cregsz00_2096;

				BgL_cregsz00_2096 =
					BGl_collectzd2registersz12zc0zzsaw_regutilsz00(BgL_blocksz00_68);
				{	/* SawMill/regalloc.scm 81 */
					obj_t BgL_hregsz00_2097;

					{	/* SawMill/regalloc.scm 82 */
						obj_t BgL_arg1856z00_2158;

						BgL_arg1856z00_2158 =
							(((BgL_backendz00_bglt) COBJECT(BgL_backz00_65))->
							BgL_registersz00);
						{	/* SawMill/regalloc.scm 82 */
							obj_t BgL_list1857z00_2159;

							BgL_list1857z00_2159 = MAKE_YOUNG_PAIR(BgL_arg1856z00_2158, BNIL);
							BgL_hregsz00_2097 =
								BGl_appendzd2mapz12zc0zz__r4_control_features_6_9z00
								(BGl_collectzd2registerz12zd2envz12zzsaw_regutilsz00,
								BgL_list1857z00_2159);
						}
					}
					{	/* SawMill/regalloc.scm 82 */
						obj_t BgL_pregsz00_2098;

						{	/* SawMill/regalloc.scm 83 */
							obj_t BgL_hook1500z00_2144;

							BgL_hook1500z00_2144 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
							{
								obj_t BgL_l1497z00_2146;
								obj_t BgL_h1498z00_2147;

								BgL_l1497z00_2146 = BgL_paramsz00_67;
								BgL_h1498z00_2147 = BgL_hook1500z00_2144;
							BgL_zc3z04anonymousza31847ze3z87_2148:
								if (NULLP(BgL_l1497z00_2146))
									{	/* SawMill/regalloc.scm 83 */
										BgL_pregsz00_2098 = CDR(BgL_hook1500z00_2144);
									}
								else
									{	/* SawMill/regalloc.scm 83 */
										bool_t BgL_test2921z00_6378;

										{	/* SawMill/regalloc.scm 83 */
											obj_t BgL_arg1854z00_2156;

											BgL_arg1854z00_2156 = CAR(((obj_t) BgL_l1497z00_2146));
											{	/* SawMill/regalloc.scm 83 */
												obj_t BgL_classz00_3854;

												BgL_classz00_3854 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
												if (BGL_OBJECTP(BgL_arg1854z00_2156))
													{	/* SawMill/regalloc.scm 83 */
														BgL_objectz00_bglt BgL_arg1807z00_3856;

														BgL_arg1807z00_3856 =
															(BgL_objectz00_bglt) (BgL_arg1854z00_2156);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawMill/regalloc.scm 83 */
																long BgL_idxz00_3862;

																BgL_idxz00_3862 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3856);
																BgL_test2921z00_6378 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3862 + 2L)) ==
																	BgL_classz00_3854);
															}
														else
															{	/* SawMill/regalloc.scm 83 */
																bool_t BgL_res2729z00_3887;

																{	/* SawMill/regalloc.scm 83 */
																	obj_t BgL_oclassz00_3870;

																	{	/* SawMill/regalloc.scm 83 */
																		obj_t BgL_arg1815z00_3878;
																		long BgL_arg1816z00_3879;

																		BgL_arg1815z00_3878 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawMill/regalloc.scm 83 */
																			long BgL_arg1817z00_3880;

																			BgL_arg1817z00_3880 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3856);
																			BgL_arg1816z00_3879 =
																				(BgL_arg1817z00_3880 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3870 =
																			VECTOR_REF(BgL_arg1815z00_3878,
																			BgL_arg1816z00_3879);
																	}
																	{	/* SawMill/regalloc.scm 83 */
																		bool_t BgL__ortest_1115z00_3871;

																		BgL__ortest_1115z00_3871 =
																			(BgL_classz00_3854 == BgL_oclassz00_3870);
																		if (BgL__ortest_1115z00_3871)
																			{	/* SawMill/regalloc.scm 83 */
																				BgL_res2729z00_3887 =
																					BgL__ortest_1115z00_3871;
																			}
																		else
																			{	/* SawMill/regalloc.scm 83 */
																				long BgL_odepthz00_3872;

																				{	/* SawMill/regalloc.scm 83 */
																					obj_t BgL_arg1804z00_3873;

																					BgL_arg1804z00_3873 =
																						(BgL_oclassz00_3870);
																					BgL_odepthz00_3872 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3873);
																				}
																				if ((2L < BgL_odepthz00_3872))
																					{	/* SawMill/regalloc.scm 83 */
																						obj_t BgL_arg1802z00_3875;

																						{	/* SawMill/regalloc.scm 83 */
																							obj_t BgL_arg1803z00_3876;

																							BgL_arg1803z00_3876 =
																								(BgL_oclassz00_3870);
																							BgL_arg1802z00_3875 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3876, 2L);
																						}
																						BgL_res2729z00_3887 =
																							(BgL_arg1802z00_3875 ==
																							BgL_classz00_3854);
																					}
																				else
																					{	/* SawMill/regalloc.scm 83 */
																						BgL_res2729z00_3887 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2921z00_6378 = BgL_res2729z00_3887;
															}
													}
												else
													{	/* SawMill/regalloc.scm 83 */
														BgL_test2921z00_6378 = ((bool_t) 0);
													}
											}
										}
										if (BgL_test2921z00_6378)
											{	/* SawMill/regalloc.scm 83 */
												obj_t BgL_nh1499z00_2152;

												{	/* SawMill/regalloc.scm 83 */
													obj_t BgL_arg1852z00_2154;

													BgL_arg1852z00_2154 =
														CAR(((obj_t) BgL_l1497z00_2146));
													BgL_nh1499z00_2152 =
														MAKE_YOUNG_PAIR(BgL_arg1852z00_2154, BNIL);
												}
												SET_CDR(BgL_h1498z00_2147, BgL_nh1499z00_2152);
												{	/* SawMill/regalloc.scm 83 */
													obj_t BgL_arg1851z00_2153;

													BgL_arg1851z00_2153 =
														CDR(((obj_t) BgL_l1497z00_2146));
													{
														obj_t BgL_h1498z00_6410;
														obj_t BgL_l1497z00_6409;

														BgL_l1497z00_6409 = BgL_arg1851z00_2153;
														BgL_h1498z00_6410 = BgL_nh1499z00_2152;
														BgL_h1498z00_2147 = BgL_h1498z00_6410;
														BgL_l1497z00_2146 = BgL_l1497z00_6409;
														goto BgL_zc3z04anonymousza31847ze3z87_2148;
													}
												}
											}
										else
											{	/* SawMill/regalloc.scm 83 */
												obj_t BgL_arg1853z00_2155;

												BgL_arg1853z00_2155 = CDR(((obj_t) BgL_l1497z00_2146));
												{
													obj_t BgL_l1497z00_6413;

													BgL_l1497z00_6413 = BgL_arg1853z00_2155;
													BgL_l1497z00_2146 = BgL_l1497z00_6413;
													goto BgL_zc3z04anonymousza31847ze3z87_2148;
												}
											}
									}
							}
						}
						{	/* SawMill/regalloc.scm 83 */
							obj_t BgL_regsz00_2099;

							BgL_regsz00_2099 =
								BGl_appendzd221011zd2zzsaw_registerzd2allocationzd2
								(BgL_hregsz00_2097, BgL_cregsz00_2096);
							{	/* SawMill/regalloc.scm 84 */

								if (NULLP(BgL_cregsz00_2096))
									{	/* SawMill/regalloc.scm 85 */
										BFALSE;
									}
								else
									{	/* SawMill/regalloc.scm 85 */
										{
											obj_t BgL_l1501z00_2102;

											BgL_l1501z00_2102 = BgL_regsz00_2099;
										BgL_zc3z04anonymousza31773ze3z87_2103:
											if (PAIRP(BgL_l1501z00_2102))
												{	/* SawMill/regalloc.scm 87 */
													{	/* SawMill/regalloc.scm 88 */
														obj_t BgL_rz00_2105;

														BgL_rz00_2105 = CAR(BgL_l1501z00_2102);
														{
															BgL_rtl_regzf2razf2_bglt BgL_auxz00_6420;

															{
																obj_t BgL_auxz00_6421;

																{	/* SawMill/regalloc.scm 89 */
																	BgL_objectz00_bglt BgL_tmpz00_6422;

																	BgL_tmpz00_6422 =
																		((BgL_objectz00_bglt)
																		((BgL_rtl_regz00_bglt) BgL_rz00_2105));
																	BgL_auxz00_6421 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_6422);
																}
																BgL_auxz00_6420 =
																	((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_6421);
															}
															((((BgL_rtl_regzf2razf2_bglt)
																		COBJECT(BgL_auxz00_6420))->
																	BgL_interferez00) =
																((obj_t) ((obj_t)
																		BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00
																		(BgL_regsz00_2099))), BUNSPEC);
														}
													}
													{
														obj_t BgL_l1501z00_6430;

														BgL_l1501z00_6430 = CDR(BgL_l1501z00_2102);
														BgL_l1501z00_2102 = BgL_l1501z00_6430;
														goto BgL_zc3z04anonymousza31773ze3z87_2103;
													}
												}
											else
												{	/* SawMill/regalloc.scm 87 */
													((bool_t) 1);
												}
										}
										{	/* SawMill/regalloc.scm 91 */
											long BgL_arg1798z00_2109;

											BgL_arg1798z00_2109 = bgl_list_length(BgL_hregsz00_2097);
											{	/* SawMill/regalloc.scm 91 */
												obj_t BgL_list1799z00_2110;

												{	/* SawMill/regalloc.scm 91 */
													obj_t BgL_arg1805z00_2111;

													{	/* SawMill/regalloc.scm 91 */
														obj_t BgL_arg1806z00_2112;

														BgL_arg1806z00_2112 =
															MAKE_YOUNG_PAIR
															(BGl_string2781z00zzsaw_registerzd2allocationzd2,
															BNIL);
														BgL_arg1805z00_2111 =
															MAKE_YOUNG_PAIR(BINT(BgL_arg1798z00_2109),
															BgL_arg1806z00_2112);
													}
													BgL_list1799z00_2110 =
														MAKE_YOUNG_PAIR
														(BGl_string2788z00zzsaw_registerzd2allocationzd2,
														BgL_arg1805z00_2111);
												}
												BGl_verbosez00zztools_speekz00(BINT(3L),
													BgL_list1799z00_2110);
										}}
										{	/* SawMill/regalloc.scm 92 */
											long BgL_arg1808z00_2113;

											BgL_arg1808z00_2113 = bgl_list_length(BgL_pregsz00_2098);
											{	/* SawMill/regalloc.scm 92 */
												obj_t BgL_list1809z00_2114;

												{	/* SawMill/regalloc.scm 92 */
													obj_t BgL_arg1812z00_2115;

													{	/* SawMill/regalloc.scm 92 */
														obj_t BgL_arg1820z00_2116;

														BgL_arg1820z00_2116 =
															MAKE_YOUNG_PAIR
															(BGl_string2781z00zzsaw_registerzd2allocationzd2,
															BNIL);
														BgL_arg1812z00_2115 =
															MAKE_YOUNG_PAIR(BINT(BgL_arg1808z00_2113),
															BgL_arg1820z00_2116);
													}
													BgL_list1809z00_2114 =
														MAKE_YOUNG_PAIR
														(BGl_string2789z00zzsaw_registerzd2allocationzd2,
														BgL_arg1812z00_2115);
												}
												BGl_verbosez00zztools_speekz00(BINT(3L),
													BgL_list1809z00_2114);
										}}
										{	/* SawMill/regalloc.scm 93 */
											long BgL_arg1822z00_2117;

											BgL_arg1822z00_2117 = bgl_list_length(BgL_cregsz00_2096);
											{	/* SawMill/regalloc.scm 93 */
												obj_t BgL_list1823z00_2118;

												{	/* SawMill/regalloc.scm 93 */
													obj_t BgL_arg1831z00_2119;

													{	/* SawMill/regalloc.scm 93 */
														obj_t BgL_arg1832z00_2120;

														BgL_arg1832z00_2120 =
															MAKE_YOUNG_PAIR
															(BGl_string2781z00zzsaw_registerzd2allocationzd2,
															BNIL);
														BgL_arg1831z00_2119 =
															MAKE_YOUNG_PAIR(BINT(BgL_arg1822z00_2117),
															BgL_arg1832z00_2120);
													}
													BgL_list1823z00_2118 =
														MAKE_YOUNG_PAIR
														(BGl_string2790z00zzsaw_registerzd2allocationzd2,
														BgL_arg1831z00_2119);
												}
												BGl_verbosez00zztools_speekz00(BINT(3L),
													BgL_list1823z00_2118);
										}}
										BGl_protectzd2interferencez12zc0zzsaw_registerzd2allocationzd2
											(BgL_blocksz00_68, BgL_regsz00_2099);
										BGl_widenzd2raz12zc0zzsaw_registerzd2allocationzd2
											(BgL_blocksz00_68, BgL_regsz00_2099);
										BGl_livenessz12z12zzsaw_registerzd2allocationzd2
											(BgL_blocksz00_68, BgL_pregsz00_2098);
										BGl_hardwarezd2mutualzd2interferencez12z12zzsaw_registerzd2allocationzd2
											(BgL_hregsz00_2097);
										BGl_hardwarezd2parameterszd2interferencez12z12zzsaw_registerzd2allocationzd2
											(BgL_backz00_65, BgL_pregsz00_2098);
										BGl_hardwarezd2interferencez12zc0zzsaw_registerzd2allocationzd2
											(BgL_backz00_65, BgL_blocksz00_68);
										BGl_typezd2interferencez12zc0zzsaw_registerzd2allocationzd2
											(BgL_backz00_65, BgL_regsz00_2099);
										BGl_interferencez12z12zzsaw_registerzd2allocationzd2
											(BgL_blocksz00_68, BgL_regsz00_2099);
										BGl_cleanupzd2destz12zc0zzsaw_registerzd2allocationzd2
											(BgL_blocksz00_68);
										BGl_registerzd2coloringzd2zzsaw_registerzd2allocationzd2
											(BgL_pregsz00_2098, BgL_hregsz00_2097, BgL_cregsz00_2096);
										BGl_funcallzd2spillz12zc0zzsaw_registerzd2allocationzd2
											(BgL_hregsz00_2097, BgL_blocksz00_68);
										BGl_rtlzd2mapzd2registersz12z12zzsaw_registerzd2allocationzd2
											(BgL_pregsz00_2098, BgL_cregsz00_2096, BgL_blocksz00_68);
										if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) > 1L))
											{	/* SawMill/regalloc.scm 121 */
												BGl_removezd2nopzd2movez12z12zzsaw_registerzd2allocationzd2
													(BgL_blocksz00_68);
											}
										else
											{	/* SawMill/regalloc.scm 121 */
												BFALSE;
											}
									}
								{
									obj_t BgL_l1503z00_2123;

									BgL_l1503z00_2123 = BgL_regsz00_2099;
								BgL_zc3z04anonymousza31834ze3z87_2124:
									if (PAIRP(BgL_l1503z00_2123))
										{	/* SawMill/regalloc.scm 126 */
											{	/* SawMill/regalloc.scm 126 */
												obj_t BgL_o1505z00_2126;

												BgL_o1505z00_2126 = CAR(BgL_l1503z00_2123);
												{	/* SawMill/regalloc.scm 126 */
													long BgL_arg1836z00_2127;

													{	/* SawMill/regalloc.scm 126 */
														obj_t BgL_arg1837z00_2128;

														{	/* SawMill/regalloc.scm 126 */
															obj_t BgL_arg1838z00_2129;

															{	/* SawMill/regalloc.scm 126 */
																obj_t BgL_arg1815z00_3898;
																long BgL_arg1816z00_3899;

																BgL_arg1815z00_3898 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawMill/regalloc.scm 126 */
																	long BgL_arg1817z00_3900;

																	BgL_arg1817z00_3900 =
																		BGL_OBJECT_CLASS_NUM(
																		((BgL_objectz00_bglt) BgL_o1505z00_2126));
																	BgL_arg1816z00_3899 =
																		(BgL_arg1817z00_3900 - OBJECT_TYPE);
																}
																BgL_arg1838z00_2129 =
																	VECTOR_REF(BgL_arg1815z00_3898,
																	BgL_arg1816z00_3899);
															}
															BgL_arg1837z00_2128 =
																BGl_classzd2superzd2zz__objectz00
																(BgL_arg1838z00_2129);
														}
														{	/* SawMill/regalloc.scm 126 */
															obj_t BgL_tmpz00_6478;

															BgL_tmpz00_6478 = ((obj_t) BgL_arg1837z00_2128);
															BgL_arg1836z00_2127 =
																BGL_CLASS_NUM(BgL_tmpz00_6478);
													}}
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_o1505z00_2126),
														BgL_arg1836z00_2127);
												}
												{	/* SawMill/regalloc.scm 126 */
													BgL_objectz00_bglt BgL_tmpz00_6483;

													BgL_tmpz00_6483 =
														((BgL_objectz00_bglt) BgL_o1505z00_2126);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6483, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_o1505z00_2126);
												BgL_o1505z00_2126;
											}
											{
												obj_t BgL_l1503z00_6487;

												BgL_l1503z00_6487 = CDR(BgL_l1503z00_2123);
												BgL_l1503z00_2123 = BgL_l1503z00_6487;
												goto BgL_zc3z04anonymousza31834ze3z87_2124;
											}
										}
									else
										{	/* SawMill/regalloc.scm 126 */
											((bool_t) 1);
										}
								}
								{
									obj_t BgL_l1506z00_2133;

									BgL_l1506z00_2133 = BgL_pregsz00_2098;
								BgL_zc3z04anonymousza31840ze3z87_2134:
									if (PAIRP(BgL_l1506z00_2133))
										{	/* SawMill/regalloc.scm 127 */
											{	/* SawMill/regalloc.scm 127 */
												obj_t BgL_rz00_2136;

												BgL_rz00_2136 = CAR(BgL_l1506z00_2133);
												{	/* SawMill/regalloc.scm 127 */
													bool_t BgL_test2931z00_6492;

													{	/* SawMill/regalloc.scm 127 */
														obj_t BgL_classz00_3912;

														BgL_classz00_3912 =
															BGl_rtl_regzf2razf2zzsaw_regsetz00;
														if (BGL_OBJECTP(BgL_rz00_2136))
															{	/* SawMill/regalloc.scm 127 */
																BgL_objectz00_bglt BgL_arg1807z00_3914;

																BgL_arg1807z00_3914 =
																	(BgL_objectz00_bglt) (BgL_rz00_2136);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* SawMill/regalloc.scm 127 */
																		long BgL_idxz00_3920;

																		BgL_idxz00_3920 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_3914);
																		BgL_test2931z00_6492 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_3920 + 2L)) ==
																			BgL_classz00_3912);
																	}
																else
																	{	/* SawMill/regalloc.scm 127 */
																		bool_t BgL_res2730z00_3945;

																		{	/* SawMill/regalloc.scm 127 */
																			obj_t BgL_oclassz00_3928;

																			{	/* SawMill/regalloc.scm 127 */
																				obj_t BgL_arg1815z00_3936;
																				long BgL_arg1816z00_3937;

																				BgL_arg1815z00_3936 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* SawMill/regalloc.scm 127 */
																					long BgL_arg1817z00_3938;

																					BgL_arg1817z00_3938 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_3914);
																					BgL_arg1816z00_3937 =
																						(BgL_arg1817z00_3938 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_3928 =
																					VECTOR_REF(BgL_arg1815z00_3936,
																					BgL_arg1816z00_3937);
																			}
																			{	/* SawMill/regalloc.scm 127 */
																				bool_t BgL__ortest_1115z00_3929;

																				BgL__ortest_1115z00_3929 =
																					(BgL_classz00_3912 ==
																					BgL_oclassz00_3928);
																				if (BgL__ortest_1115z00_3929)
																					{	/* SawMill/regalloc.scm 127 */
																						BgL_res2730z00_3945 =
																							BgL__ortest_1115z00_3929;
																					}
																				else
																					{	/* SawMill/regalloc.scm 127 */
																						long BgL_odepthz00_3930;

																						{	/* SawMill/regalloc.scm 127 */
																							obj_t BgL_arg1804z00_3931;

																							BgL_arg1804z00_3931 =
																								(BgL_oclassz00_3928);
																							BgL_odepthz00_3930 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_3931);
																						}
																						if ((2L < BgL_odepthz00_3930))
																							{	/* SawMill/regalloc.scm 127 */
																								obj_t BgL_arg1802z00_3933;

																								{	/* SawMill/regalloc.scm 127 */
																									obj_t BgL_arg1803z00_3934;

																									BgL_arg1803z00_3934 =
																										(BgL_oclassz00_3928);
																									BgL_arg1802z00_3933 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_3934, 2L);
																								}
																								BgL_res2730z00_3945 =
																									(BgL_arg1802z00_3933 ==
																									BgL_classz00_3912);
																							}
																						else
																							{	/* SawMill/regalloc.scm 127 */
																								BgL_res2730z00_3945 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2931z00_6492 = BgL_res2730z00_3945;
																	}
															}
														else
															{	/* SawMill/regalloc.scm 127 */
																BgL_test2931z00_6492 = ((bool_t) 0);
															}
													}
													if (BgL_test2931z00_6492)
														{	/* SawMill/regalloc.scm 127 */
															{	/* SawMill/regalloc.scm 127 */
																long BgL_arg1843z00_2139;

																{	/* SawMill/regalloc.scm 127 */
																	obj_t BgL_arg1844z00_2140;

																	{	/* SawMill/regalloc.scm 127 */
																		obj_t BgL_arg1845z00_2141;

																		{	/* SawMill/regalloc.scm 127 */
																			obj_t BgL_arg1815z00_3947;
																			long BgL_arg1816z00_3948;

																			BgL_arg1815z00_3947 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* SawMill/regalloc.scm 127 */
																				long BgL_arg1817z00_3949;

																				BgL_arg1817z00_3949 =
																					BGL_OBJECT_CLASS_NUM(
																					((BgL_objectz00_bglt) BgL_rz00_2136));
																				BgL_arg1816z00_3948 =
																					(BgL_arg1817z00_3949 - OBJECT_TYPE);
																			}
																			BgL_arg1845z00_2141 =
																				VECTOR_REF(BgL_arg1815z00_3947,
																				BgL_arg1816z00_3948);
																		}
																		BgL_arg1844z00_2140 =
																			BGl_classzd2superzd2zz__objectz00
																			(BgL_arg1845z00_2141);
																	}
																	{	/* SawMill/regalloc.scm 127 */
																		obj_t BgL_tmpz00_6521;

																		BgL_tmpz00_6521 =
																			((obj_t) BgL_arg1844z00_2140);
																		BgL_arg1843z00_2139 =
																			BGL_CLASS_NUM(BgL_tmpz00_6521);
																}}
																BGL_OBJECT_CLASS_NUM_SET(
																	((BgL_objectz00_bglt) BgL_rz00_2136),
																	BgL_arg1843z00_2139);
															}
															{	/* SawMill/regalloc.scm 127 */
																BgL_objectz00_bglt BgL_tmpz00_6526;

																BgL_tmpz00_6526 =
																	((BgL_objectz00_bglt) BgL_rz00_2136);
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6526,
																	BFALSE);
															}
															((BgL_objectz00_bglt) BgL_rz00_2136);
															BgL_rz00_2136;
														}
													else
														{	/* SawMill/regalloc.scm 127 */
															BFALSE;
														}
												}
											}
											{
												obj_t BgL_l1506z00_6530;

												BgL_l1506z00_6530 = CDR(BgL_l1506z00_2133);
												BgL_l1506z00_2133 = BgL_l1506z00_6530;
												goto BgL_zc3z04anonymousza31840ze3z87_2134;
											}
										}
									else
										{	/* SawMill/regalloc.scm 127 */
											return ((bool_t) 1);
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* widen-ra! */
	bool_t BGl_widenzd2raz12zc0zzsaw_registerzd2allocationzd2(obj_t BgL_oz00_69,
		obj_t BgL_regsz00_70)
	{
		{	/* SawMill/regalloc.scm 135 */
			{
				obj_t BgL_oz00_2222;
				obj_t BgL_oz00_2264;

				{
					obj_t BgL_l1535z00_2165;

					BgL_l1535z00_2165 = BgL_oz00_69;
				BgL_zc3z04anonymousza31858ze3z87_2166:
					if (PAIRP(BgL_l1535z00_2165))
						{	/* SawMill/regalloc.scm 163 */
							BgL_oz00_2264 = CAR(BgL_l1535z00_2165);
							{	/* SawMill/regalloc.scm 159 */
								obj_t BgL_lastz00_2267;

								BgL_lastz00_2267 =
									bgl_reverse(
									(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt) BgL_oz00_2264)))->BgL_firstz00));
								{	/* SawMill/regalloc.scm 160 */
									obj_t BgL_g1534z00_2268;

									BgL_g1534z00_2268 =
										(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt) BgL_oz00_2264)))->BgL_firstz00);
									{
										obj_t BgL_l1532z00_2270;

										BgL_l1532z00_2270 = BgL_g1534z00_2268;
									BgL_zc3z04anonymousza31919ze3z87_2271:
										if (PAIRP(BgL_l1532z00_2270))
											{	/* SawMill/regalloc.scm 160 */
												BgL_oz00_2222 = CAR(BgL_l1532z00_2270);
												{	/* SawMill/regalloc.scm 149 */
													BgL_rtl_inszf2razf2_bglt BgL_wide1200z00_2227;

													BgL_wide1200z00_2227 =
														((BgL_rtl_inszf2razf2_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_rtl_inszf2razf2_bgl))));
													{	/* SawMill/regalloc.scm 149 */
														obj_t BgL_auxz00_6546;
														BgL_objectz00_bglt BgL_tmpz00_6542;

														BgL_auxz00_6546 = ((obj_t) BgL_wide1200z00_2227);
														BgL_tmpz00_6542 =
															((BgL_objectz00_bglt)
															((BgL_rtl_insz00_bglt)
																((BgL_rtl_insz00_bglt) BgL_oz00_2222)));
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6542,
															BgL_auxz00_6546);
													}
													((BgL_objectz00_bglt)
														((BgL_rtl_insz00_bglt)
															((BgL_rtl_insz00_bglt) BgL_oz00_2222)));
													{	/* SawMill/regalloc.scm 149 */
														long BgL_arg1891z00_2228;

														BgL_arg1891z00_2228 =
															BGL_CLASS_NUM
															(BGl_rtl_inszf2razf2zzsaw_registerzd2allocationzd2);
														BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																	(BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt)
																		BgL_oz00_2222))), BgL_arg1891z00_2228);
													}
													((BgL_rtl_insz00_bglt)
														((BgL_rtl_insz00_bglt)
															((BgL_rtl_insz00_bglt) BgL_oz00_2222)));
												}
												{
													obj_t BgL_auxz00_6568;
													BgL_rtl_inszf2razf2_bglt BgL_auxz00_6560;

													{	/* SawMill/regalloc.scm 150 */
														bool_t BgL_test2938z00_6569;

														if (CBOOL(
																(((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt) BgL_oz00_2222)))->
																	BgL_destz00)))
															{	/* SawMill/regalloc.scm 150 */
																BgL_test2938z00_6569 =
																	CBOOL(
																	(((BgL_rtl_regz00_bglt) COBJECT(
																				((BgL_rtl_regz00_bglt)
																					(((BgL_rtl_insz00_bglt) COBJECT(
																								((BgL_rtl_insz00_bglt)
																									BgL_oz00_2222)))->
																						BgL_destz00))))->BgL_onexprzf3zf3));
															}
														else
															{	/* SawMill/regalloc.scm 150 */
																BgL_test2938z00_6569 = ((bool_t) 1);
															}
														if (BgL_test2938z00_6569)
															{	/* SawMill/regalloc.scm 150 */
																BgL_auxz00_6568 =
																	((obj_t)
																	BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00
																	(BgL_regsz00_70));
															}
														else
															{	/* SawMill/regalloc.scm 152 */
																obj_t BgL_arg1896z00_2233;

																{	/* SawMill/regalloc.scm 152 */
																	BgL_rtl_regz00_bglt BgL_arg1897z00_2234;

																	{	/* SawMill/regalloc.scm 152 */
																		obj_t BgL_arg1899z00_2236;

																		BgL_arg1899z00_2236 =
																			(((BgL_rtl_insz00_bglt) COBJECT(
																					((BgL_rtl_insz00_bglt)
																						BgL_oz00_2222)))->BgL_destz00);
																		BgL_arg1897z00_2234 =
																			BGl_usezd2registerz12zc0zzsaw_registerzd2allocationzd2
																			(((BgL_rtl_regz00_bglt)
																				BgL_arg1899z00_2236));
																	}
																	{	/* SawMill/regalloc.scm 152 */
																		obj_t BgL_list1898z00_2235;

																		BgL_list1898z00_2235 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_arg1897z00_2234), BNIL);
																		BgL_arg1896z00_2233 = BgL_list1898z00_2235;
																	}
																}
																BgL_auxz00_6568 =
																	((obj_t)
																	BGl_listzd2ze3regsetz31zzsaw_regsetz00
																	(BgL_arg1896z00_2233, BgL_regsz00_70));
															}
													}
													{
														obj_t BgL_auxz00_6561;

														{	/* SawMill/regalloc.scm 150 */
															BgL_objectz00_bglt BgL_tmpz00_6562;

															BgL_tmpz00_6562 =
																((BgL_objectz00_bglt)
																((BgL_rtl_insz00_bglt)
																	((BgL_rtl_insz00_bglt) BgL_oz00_2222)));
															BgL_auxz00_6561 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_6562);
														}
														BgL_auxz00_6560 =
															((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_6561);
													}
													((((BgL_rtl_inszf2razf2_bglt)
																COBJECT(BgL_auxz00_6560))->BgL_defz00) =
														((obj_t) BgL_auxz00_6568), BUNSPEC);
												}
												{
													BgL_rtl_inszf2razf2_bglt BgL_auxz00_6590;

													{
														obj_t BgL_auxz00_6591;

														{	/* SawMill/regalloc.scm 154 */
															BgL_objectz00_bglt BgL_tmpz00_6592;

															BgL_tmpz00_6592 =
																((BgL_objectz00_bglt)
																((BgL_rtl_insz00_bglt)
																	((BgL_rtl_insz00_bglt) BgL_oz00_2222)));
															BgL_auxz00_6591 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_6592);
														}
														BgL_auxz00_6590 =
															((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_6591);
													}
													((((BgL_rtl_inszf2razf2_bglt)
																COBJECT(BgL_auxz00_6590))->BgL_outz00) =
														((obj_t) ((obj_t)
																BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00
																(BgL_regsz00_70))), BUNSPEC);
												}
												{
													obj_t BgL_auxz00_6609;
													BgL_rtl_inszf2razf2_bglt BgL_auxz00_6601;

													{	/* SawMill/regalloc.scm 153 */
														obj_t BgL_arg1902z00_2239;

														{	/* SawMill/regalloc.scm 153 */
															obj_t BgL_l1523z00_2240;

															BgL_l1523z00_2240 =
																BGl_getzd2argsze70z35zzsaw_registerzd2allocationzd2
																(BgL_oz00_2222);
															if (NULLP(BgL_l1523z00_2240))
																{	/* SawMill/regalloc.scm 153 */
																	BgL_arg1902z00_2239 = BNIL;
																}
															else
																{	/* SawMill/regalloc.scm 153 */
																	obj_t BgL_head1525z00_2242;

																	{	/* SawMill/regalloc.scm 153 */
																		BgL_rtl_regz00_bglt BgL_arg1912z00_2254;

																		{	/* SawMill/regalloc.scm 153 */
																			obj_t BgL_arg1913z00_2255;

																			BgL_arg1913z00_2255 =
																				CAR(((obj_t) BgL_l1523z00_2240));
																			BgL_arg1912z00_2254 =
																				BGl_usezd2registerz12zc0zzsaw_registerzd2allocationzd2
																				(((BgL_rtl_regz00_bglt)
																					BgL_arg1913z00_2255));
																		}
																		BgL_head1525z00_2242 =
																			MAKE_YOUNG_PAIR(
																			((obj_t) BgL_arg1912z00_2254), BNIL);
																	}
																	{	/* SawMill/regalloc.scm 153 */
																		obj_t BgL_g1528z00_2243;

																		BgL_g1528z00_2243 =
																			CDR(((obj_t) BgL_l1523z00_2240));
																		{
																			obj_t BgL_l1523z00_2245;
																			obj_t BgL_tail1526z00_2246;

																			BgL_l1523z00_2245 = BgL_g1528z00_2243;
																			BgL_tail1526z00_2246 =
																				BgL_head1525z00_2242;
																		BgL_zc3z04anonymousza31904ze3z87_2247:
																			if (NULLP(BgL_l1523z00_2245))
																				{	/* SawMill/regalloc.scm 153 */
																					BgL_arg1902z00_2239 =
																						BgL_head1525z00_2242;
																				}
																			else
																				{	/* SawMill/regalloc.scm 153 */
																					obj_t BgL_newtail1527z00_2249;

																					{	/* SawMill/regalloc.scm 153 */
																						BgL_rtl_regz00_bglt
																							BgL_arg1910z00_2251;
																						{	/* SawMill/regalloc.scm 153 */
																							obj_t BgL_arg1911z00_2252;

																							BgL_arg1911z00_2252 =
																								CAR(
																								((obj_t) BgL_l1523z00_2245));
																							BgL_arg1910z00_2251 =
																								BGl_usezd2registerz12zc0zzsaw_registerzd2allocationzd2
																								(((BgL_rtl_regz00_bglt)
																									BgL_arg1911z00_2252));
																						}
																						BgL_newtail1527z00_2249 =
																							MAKE_YOUNG_PAIR(
																							((obj_t) BgL_arg1910z00_2251),
																							BNIL);
																					}
																					SET_CDR(BgL_tail1526z00_2246,
																						BgL_newtail1527z00_2249);
																					{	/* SawMill/regalloc.scm 153 */
																						obj_t BgL_arg1906z00_2250;

																						BgL_arg1906z00_2250 =
																							CDR(((obj_t) BgL_l1523z00_2245));
																						{
																							obj_t BgL_tail1526z00_6633;
																							obj_t BgL_l1523z00_6632;

																							BgL_l1523z00_6632 =
																								BgL_arg1906z00_2250;
																							BgL_tail1526z00_6633 =
																								BgL_newtail1527z00_2249;
																							BgL_tail1526z00_2246 =
																								BgL_tail1526z00_6633;
																							BgL_l1523z00_2245 =
																								BgL_l1523z00_6632;
																							goto
																								BgL_zc3z04anonymousza31904ze3z87_2247;
																						}
																					}
																				}
																		}
																	}
																}
														}
														BgL_auxz00_6609 =
															((obj_t)
															BGl_listzd2ze3regsetz31zzsaw_regsetz00
															(BgL_arg1902z00_2239, BgL_regsz00_70));
													}
													{
														obj_t BgL_auxz00_6602;

														{	/* SawMill/regalloc.scm 153 */
															BgL_objectz00_bglt BgL_tmpz00_6603;

															BgL_tmpz00_6603 =
																((BgL_objectz00_bglt)
																((BgL_rtl_insz00_bglt)
																	((BgL_rtl_insz00_bglt) BgL_oz00_2222)));
															BgL_auxz00_6602 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_6603);
														}
														BgL_auxz00_6601 =
															((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_6602);
													}
													((((BgL_rtl_inszf2razf2_bglt)
																COBJECT(BgL_auxz00_6601))->BgL_inz00) =
														((obj_t) BgL_auxz00_6609), BUNSPEC);
												}
												{
													BgL_rtl_inszf2razf2_bglt BgL_auxz00_6637;

													{
														obj_t BgL_auxz00_6638;

														{	/* SawMill/regalloc.scm 155 */
															BgL_objectz00_bglt BgL_tmpz00_6639;

															BgL_tmpz00_6639 =
																((BgL_objectz00_bglt)
																((BgL_rtl_insz00_bglt)
																	((BgL_rtl_insz00_bglt) BgL_oz00_2222)));
															BgL_auxz00_6638 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_6639);
														}
														BgL_auxz00_6637 =
															((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_6638);
													}
													((((BgL_rtl_inszf2razf2_bglt)
																COBJECT(BgL_auxz00_6637))->BgL_spillz00) =
														((obj_t) ((obj_t)
																BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00
																(BgL_regsz00_70))), BUNSPEC);
												}
												((BgL_rtl_insz00_bglt)
													((BgL_rtl_insz00_bglt) BgL_oz00_2222));
												{	/* SawMill/regalloc.scm 156 */
													obj_t BgL_g1531z00_2256;

													BgL_g1531z00_2256 =
														(((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt) BgL_oz00_2222)))->
														BgL_argsz00);
													{
														obj_t BgL_l1529z00_2258;

														BgL_l1529z00_2258 = BgL_g1531z00_2256;
													BgL_zc3z04anonymousza31914ze3z87_2259:
														if (PAIRP(BgL_l1529z00_2258))
															{	/* SawMill/regalloc.scm 156 */
																BGl_argszd2widenzd2raz12ze70zf5zzsaw_registerzd2allocationzd2
																	(BgL_regsz00_70, CAR(BgL_l1529z00_2258));
																{
																	obj_t BgL_l1529z00_6656;

																	BgL_l1529z00_6656 = CDR(BgL_l1529z00_2258);
																	BgL_l1529z00_2258 = BgL_l1529z00_6656;
																	goto BgL_zc3z04anonymousza31914ze3z87_2259;
																}
															}
														else
															{	/* SawMill/regalloc.scm 156 */
																((bool_t) 1);
															}
													}
												}
												{
													obj_t BgL_l1532z00_6659;

													BgL_l1532z00_6659 = CDR(BgL_l1532z00_2270);
													BgL_l1532z00_2270 = BgL_l1532z00_6659;
													goto BgL_zc3z04anonymousza31919ze3z87_2271;
												}
											}
										else
											{	/* SawMill/regalloc.scm 160 */
												((bool_t) 1);
											}
									}
								}
								{	/* SawMill/regalloc.scm 161 */
									BgL_blockzf2razf2_bglt BgL_wide1205z00_2278;

									BgL_wide1205z00_2278 =
										((BgL_blockzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_blockzf2razf2_bgl))));
									{	/* SawMill/regalloc.scm 161 */
										obj_t BgL_auxz00_6666;
										BgL_objectz00_bglt BgL_tmpz00_6662;

										BgL_auxz00_6666 = ((obj_t) BgL_wide1205z00_2278);
										BgL_tmpz00_6662 =
											((BgL_objectz00_bglt)
											((BgL_blockz00_bglt)
												((BgL_blockz00_bglt) BgL_oz00_2264)));
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6662, BgL_auxz00_6666);
									}
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_2264)));
									{	/* SawMill/regalloc.scm 161 */
										long BgL_arg1925z00_2279;

										BgL_arg1925z00_2279 =
											BGL_CLASS_NUM
											(BGl_blockzf2razf2zzsaw_registerzd2allocationzd2);
										BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
													(BgL_blockz00_bglt) ((BgL_blockz00_bglt)
														BgL_oz00_2264))), BgL_arg1925z00_2279);
									}
									((BgL_blockz00_bglt)
										((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_2264)));
								}
								{
									BgL_blockzf2razf2_bglt BgL_auxz00_6680;

									{
										obj_t BgL_auxz00_6681;

										{	/* SawMill/regalloc.scm 162 */
											BgL_objectz00_bglt BgL_tmpz00_6682;

											BgL_tmpz00_6682 =
												((BgL_objectz00_bglt)
												((BgL_blockz00_bglt)
													((BgL_blockz00_bglt) BgL_oz00_2264)));
											BgL_auxz00_6681 = BGL_OBJECT_WIDENING(BgL_tmpz00_6682);
										}
										BgL_auxz00_6680 =
											((BgL_blockzf2razf2_bglt) BgL_auxz00_6681);
									}
									((((BgL_blockzf2razf2_bglt) COBJECT(BgL_auxz00_6680))->
											BgL_lastz00) = ((obj_t) BgL_lastz00_2267), BUNSPEC);
								}
								((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_2264));
							}
							{
								obj_t BgL_l1535z00_6692;

								BgL_l1535z00_6692 = CDR(BgL_l1535z00_2165);
								BgL_l1535z00_2165 = BgL_l1535z00_6692;
								goto BgL_zc3z04anonymousza31858ze3z87_2166;
							}
						}
					else
						{	/* SawMill/regalloc.scm 163 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* get-args~0 */
	obj_t BGl_getzd2argsze70z35zzsaw_registerzd2allocationzd2(obj_t BgL_oz00_2171)
	{
		{	/* SawMill/regalloc.scm 137 */
			{	/* SawMill/regalloc.scm 137 */
				obj_t BgL_hook1512z00_2173;

				BgL_hook1512z00_2173 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
				{	/* SawMill/regalloc.scm 137 */
					obj_t BgL_g1513z00_2174;

					BgL_g1513z00_2174 =
						BGl_rtl_inszd2argsza2z70zzsaw_defsz00(
						((BgL_rtl_insz00_bglt) BgL_oz00_2171));
					{
						obj_t BgL_l1509z00_2176;
						obj_t BgL_h1510z00_2177;

						BgL_l1509z00_2176 = BgL_g1513z00_2174;
						BgL_h1510z00_2177 = BgL_hook1512z00_2173;
					BgL_zc3z04anonymousza31864ze3z87_2178:
						if (NULLP(BgL_l1509z00_2176))
							{	/* SawMill/regalloc.scm 137 */
								return CDR(BgL_hook1512z00_2173);
							}
						else
							{	/* SawMill/regalloc.scm 137 */
								bool_t BgL_test2944z00_6700;

								{	/* SawMill/regalloc.scm 137 */
									obj_t BgL_arg1872z00_2186;

									BgL_arg1872z00_2186 = CAR(((obj_t) BgL_l1509z00_2176));
									{	/* SawMill/regalloc.scm 137 */
										obj_t BgL_classz00_3962;

										BgL_classz00_3962 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
										if (BGL_OBJECTP(BgL_arg1872z00_2186))
											{	/* SawMill/regalloc.scm 137 */
												BgL_objectz00_bglt BgL_arg1807z00_3964;

												BgL_arg1807z00_3964 =
													(BgL_objectz00_bglt) (BgL_arg1872z00_2186);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawMill/regalloc.scm 137 */
														long BgL_idxz00_3970;

														BgL_idxz00_3970 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3964);
														BgL_test2944z00_6700 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3970 + 2L)) == BgL_classz00_3962);
													}
												else
													{	/* SawMill/regalloc.scm 137 */
														bool_t BgL_res2731z00_3995;

														{	/* SawMill/regalloc.scm 137 */
															obj_t BgL_oclassz00_3978;

															{	/* SawMill/regalloc.scm 137 */
																obj_t BgL_arg1815z00_3986;
																long BgL_arg1816z00_3987;

																BgL_arg1815z00_3986 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawMill/regalloc.scm 137 */
																	long BgL_arg1817z00_3988;

																	BgL_arg1817z00_3988 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3964);
																	BgL_arg1816z00_3987 =
																		(BgL_arg1817z00_3988 - OBJECT_TYPE);
																}
																BgL_oclassz00_3978 =
																	VECTOR_REF(BgL_arg1815z00_3986,
																	BgL_arg1816z00_3987);
															}
															{	/* SawMill/regalloc.scm 137 */
																bool_t BgL__ortest_1115z00_3979;

																BgL__ortest_1115z00_3979 =
																	(BgL_classz00_3962 == BgL_oclassz00_3978);
																if (BgL__ortest_1115z00_3979)
																	{	/* SawMill/regalloc.scm 137 */
																		BgL_res2731z00_3995 =
																			BgL__ortest_1115z00_3979;
																	}
																else
																	{	/* SawMill/regalloc.scm 137 */
																		long BgL_odepthz00_3980;

																		{	/* SawMill/regalloc.scm 137 */
																			obj_t BgL_arg1804z00_3981;

																			BgL_arg1804z00_3981 =
																				(BgL_oclassz00_3978);
																			BgL_odepthz00_3980 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3981);
																		}
																		if ((2L < BgL_odepthz00_3980))
																			{	/* SawMill/regalloc.scm 137 */
																				obj_t BgL_arg1802z00_3983;

																				{	/* SawMill/regalloc.scm 137 */
																					obj_t BgL_arg1803z00_3984;

																					BgL_arg1803z00_3984 =
																						(BgL_oclassz00_3978);
																					BgL_arg1802z00_3983 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3984, 2L);
																				}
																				BgL_res2731z00_3995 =
																					(BgL_arg1802z00_3983 ==
																					BgL_classz00_3962);
																			}
																		else
																			{	/* SawMill/regalloc.scm 137 */
																				BgL_res2731z00_3995 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2944z00_6700 = BgL_res2731z00_3995;
													}
											}
										else
											{	/* SawMill/regalloc.scm 137 */
												BgL_test2944z00_6700 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2944z00_6700)
									{	/* SawMill/regalloc.scm 137 */
										obj_t BgL_nh1511z00_2182;

										{	/* SawMill/regalloc.scm 137 */
											obj_t BgL_arg1869z00_2184;

											BgL_arg1869z00_2184 = CAR(((obj_t) BgL_l1509z00_2176));
											BgL_nh1511z00_2182 =
												MAKE_YOUNG_PAIR(BgL_arg1869z00_2184, BNIL);
										}
										SET_CDR(BgL_h1510z00_2177, BgL_nh1511z00_2182);
										{	/* SawMill/regalloc.scm 137 */
											obj_t BgL_arg1868z00_2183;

											BgL_arg1868z00_2183 = CDR(((obj_t) BgL_l1509z00_2176));
											{
												obj_t BgL_h1510z00_6732;
												obj_t BgL_l1509z00_6731;

												BgL_l1509z00_6731 = BgL_arg1868z00_2183;
												BgL_h1510z00_6732 = BgL_nh1511z00_2182;
												BgL_h1510z00_2177 = BgL_h1510z00_6732;
												BgL_l1509z00_2176 = BgL_l1509z00_6731;
												goto BgL_zc3z04anonymousza31864ze3z87_2178;
											}
										}
									}
								else
									{	/* SawMill/regalloc.scm 137 */
										obj_t BgL_arg1870z00_2185;

										BgL_arg1870z00_2185 = CDR(((obj_t) BgL_l1509z00_2176));
										{
											obj_t BgL_l1509z00_6735;

											BgL_l1509z00_6735 = BgL_arg1870z00_2185;
											BgL_l1509z00_2176 = BgL_l1509z00_6735;
											goto BgL_zc3z04anonymousza31864ze3z87_2178;
										}
									}
							}
					}
				}
			}
		}

	}



/* args-widen-ra!~0 */
	bool_t BGl_argszd2widenzd2raz12ze70zf5zzsaw_registerzd2allocationzd2(obj_t
		BgL_regsz00_6051, obj_t BgL_oz00_2188)
	{
		{	/* SawMill/regalloc.scm 146 */
			{	/* SawMill/regalloc.scm 139 */
				bool_t BgL_test2949z00_6736;

				{	/* SawMill/regalloc.scm 139 */
					obj_t BgL_classz00_4000;

					BgL_classz00_4000 = BGl_rtl_insz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_oz00_2188))
						{	/* SawMill/regalloc.scm 139 */
							BgL_objectz00_bglt BgL_arg1807z00_4002;

							BgL_arg1807z00_4002 = (BgL_objectz00_bglt) (BgL_oz00_2188);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawMill/regalloc.scm 139 */
									long BgL_idxz00_4008;

									BgL_idxz00_4008 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4002);
									BgL_test2949z00_6736 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4008 + 1L)) == BgL_classz00_4000);
								}
							else
								{	/* SawMill/regalloc.scm 139 */
									bool_t BgL_res2732z00_4033;

									{	/* SawMill/regalloc.scm 139 */
										obj_t BgL_oclassz00_4016;

										{	/* SawMill/regalloc.scm 139 */
											obj_t BgL_arg1815z00_4024;
											long BgL_arg1816z00_4025;

											BgL_arg1815z00_4024 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawMill/regalloc.scm 139 */
												long BgL_arg1817z00_4026;

												BgL_arg1817z00_4026 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4002);
												BgL_arg1816z00_4025 =
													(BgL_arg1817z00_4026 - OBJECT_TYPE);
											}
											BgL_oclassz00_4016 =
												VECTOR_REF(BgL_arg1815z00_4024, BgL_arg1816z00_4025);
										}
										{	/* SawMill/regalloc.scm 139 */
											bool_t BgL__ortest_1115z00_4017;

											BgL__ortest_1115z00_4017 =
												(BgL_classz00_4000 == BgL_oclassz00_4016);
											if (BgL__ortest_1115z00_4017)
												{	/* SawMill/regalloc.scm 139 */
													BgL_res2732z00_4033 = BgL__ortest_1115z00_4017;
												}
											else
												{	/* SawMill/regalloc.scm 139 */
													long BgL_odepthz00_4018;

													{	/* SawMill/regalloc.scm 139 */
														obj_t BgL_arg1804z00_4019;

														BgL_arg1804z00_4019 = (BgL_oclassz00_4016);
														BgL_odepthz00_4018 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4019);
													}
													if ((1L < BgL_odepthz00_4018))
														{	/* SawMill/regalloc.scm 139 */
															obj_t BgL_arg1802z00_4021;

															{	/* SawMill/regalloc.scm 139 */
																obj_t BgL_arg1803z00_4022;

																BgL_arg1803z00_4022 = (BgL_oclassz00_4016);
																BgL_arg1802z00_4021 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4022,
																	1L);
															}
															BgL_res2732z00_4033 =
																(BgL_arg1802z00_4021 == BgL_classz00_4000);
														}
													else
														{	/* SawMill/regalloc.scm 139 */
															BgL_res2732z00_4033 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2949z00_6736 = BgL_res2732z00_4033;
								}
						}
					else
						{	/* SawMill/regalloc.scm 139 */
							BgL_test2949z00_6736 = ((bool_t) 0);
						}
				}
				if (BgL_test2949z00_6736)
					{	/* SawMill/regalloc.scm 139 */
						{	/* SawMill/regalloc.scm 141 */
							BgL_rtl_inszf2razf2_bglt BgL_wide1195z00_2194;

							BgL_wide1195z00_2194 =
								((BgL_rtl_inszf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_inszf2razf2_bgl))));
							{	/* SawMill/regalloc.scm 141 */
								obj_t BgL_auxz00_6764;
								BgL_objectz00_bglt BgL_tmpz00_6760;

								BgL_auxz00_6764 = ((obj_t) BgL_wide1195z00_2194);
								BgL_tmpz00_6760 =
									((BgL_objectz00_bglt)
									((BgL_rtl_insz00_bglt)
										((BgL_rtl_insz00_bglt) BgL_oz00_2188)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6760, BgL_auxz00_6764);
							}
							((BgL_objectz00_bglt)
								((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_2188)));
							{	/* SawMill/regalloc.scm 141 */
								long BgL_arg1875z00_2195;

								BgL_arg1875z00_2195 =
									BGL_CLASS_NUM
									(BGl_rtl_inszf2razf2zzsaw_registerzd2allocationzd2);
								BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
											(BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt)
												BgL_oz00_2188))), BgL_arg1875z00_2195);
							}
							((BgL_rtl_insz00_bglt)
								((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_2188)));
						}
						{
							BgL_rtl_inszf2razf2_bglt BgL_auxz00_6778;

							{
								obj_t BgL_auxz00_6779;

								{	/* SawMill/regalloc.scm 142 */
									BgL_objectz00_bglt BgL_tmpz00_6780;

									BgL_tmpz00_6780 =
										((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt)
											((BgL_rtl_insz00_bglt) BgL_oz00_2188)));
									BgL_auxz00_6779 = BGL_OBJECT_WIDENING(BgL_tmpz00_6780);
								}
								BgL_auxz00_6778 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_6779);
							}
							((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_6778))->
									BgL_defz00) =
								((obj_t) ((obj_t)
										BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00
										(BgL_regsz00_6051))), BUNSPEC);
						}
						{
							BgL_rtl_inszf2razf2_bglt BgL_auxz00_6789;

							{
								obj_t BgL_auxz00_6790;

								{	/* SawMill/regalloc.scm 144 */
									BgL_objectz00_bglt BgL_tmpz00_6791;

									BgL_tmpz00_6791 =
										((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt)
											((BgL_rtl_insz00_bglt) BgL_oz00_2188)));
									BgL_auxz00_6790 = BGL_OBJECT_WIDENING(BgL_tmpz00_6791);
								}
								BgL_auxz00_6789 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_6790);
							}
							((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_6789))->
									BgL_outz00) =
								((obj_t) ((obj_t)
										BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00
										(BgL_regsz00_6051))), BUNSPEC);
						}
						{
							obj_t BgL_auxz00_6808;
							BgL_rtl_inszf2razf2_bglt BgL_auxz00_6800;

							{	/* SawMill/regalloc.scm 143 */
								obj_t BgL_arg1876z00_2197;

								{	/* SawMill/regalloc.scm 143 */
									obj_t BgL_l1514z00_2198;

									BgL_l1514z00_2198 =
										BGl_getzd2argsze70z35zzsaw_registerzd2allocationzd2
										(BgL_oz00_2188);
									if (NULLP(BgL_l1514z00_2198))
										{	/* SawMill/regalloc.scm 143 */
											BgL_arg1876z00_2197 = BNIL;
										}
									else
										{	/* SawMill/regalloc.scm 143 */
											obj_t BgL_head1516z00_2200;

											{	/* SawMill/regalloc.scm 143 */
												BgL_rtl_regz00_bglt BgL_arg1884z00_2212;

												{	/* SawMill/regalloc.scm 143 */
													obj_t BgL_arg1885z00_2213;

													BgL_arg1885z00_2213 =
														CAR(((obj_t) BgL_l1514z00_2198));
													BgL_arg1884z00_2212 =
														BGl_usezd2registerz12zc0zzsaw_registerzd2allocationzd2
														(((BgL_rtl_regz00_bglt) BgL_arg1885z00_2213));
												}
												BgL_head1516z00_2200 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg1884z00_2212), BNIL);
											}
											{	/* SawMill/regalloc.scm 143 */
												obj_t BgL_g1519z00_2201;

												BgL_g1519z00_2201 = CDR(((obj_t) BgL_l1514z00_2198));
												{
													obj_t BgL_l1514z00_2203;
													obj_t BgL_tail1517z00_2204;

													BgL_l1514z00_2203 = BgL_g1519z00_2201;
													BgL_tail1517z00_2204 = BgL_head1516z00_2200;
												BgL_zc3z04anonymousza31878ze3z87_2205:
													if (NULLP(BgL_l1514z00_2203))
														{	/* SawMill/regalloc.scm 143 */
															BgL_arg1876z00_2197 = BgL_head1516z00_2200;
														}
													else
														{	/* SawMill/regalloc.scm 143 */
															obj_t BgL_newtail1518z00_2207;

															{	/* SawMill/regalloc.scm 143 */
																BgL_rtl_regz00_bglt BgL_arg1882z00_2209;

																{	/* SawMill/regalloc.scm 143 */
																	obj_t BgL_arg1883z00_2210;

																	BgL_arg1883z00_2210 =
																		CAR(((obj_t) BgL_l1514z00_2203));
																	BgL_arg1882z00_2209 =
																		BGl_usezd2registerz12zc0zzsaw_registerzd2allocationzd2
																		(((BgL_rtl_regz00_bglt)
																			BgL_arg1883z00_2210));
																}
																BgL_newtail1518z00_2207 =
																	MAKE_YOUNG_PAIR(
																	((obj_t) BgL_arg1882z00_2209), BNIL);
															}
															SET_CDR(BgL_tail1517z00_2204,
																BgL_newtail1518z00_2207);
															{	/* SawMill/regalloc.scm 143 */
																obj_t BgL_arg1880z00_2208;

																BgL_arg1880z00_2208 =
																	CDR(((obj_t) BgL_l1514z00_2203));
																{
																	obj_t BgL_tail1517z00_6832;
																	obj_t BgL_l1514z00_6831;

																	BgL_l1514z00_6831 = BgL_arg1880z00_2208;
																	BgL_tail1517z00_6832 =
																		BgL_newtail1518z00_2207;
																	BgL_tail1517z00_2204 = BgL_tail1517z00_6832;
																	BgL_l1514z00_2203 = BgL_l1514z00_6831;
																	goto BgL_zc3z04anonymousza31878ze3z87_2205;
																}
															}
														}
												}
											}
										}
								}
								BgL_auxz00_6808 =
									((obj_t)
									BGl_listzd2ze3regsetz31zzsaw_regsetz00(BgL_arg1876z00_2197,
										BgL_regsz00_6051));
							}
							{
								obj_t BgL_auxz00_6801;

								{	/* SawMill/regalloc.scm 143 */
									BgL_objectz00_bglt BgL_tmpz00_6802;

									BgL_tmpz00_6802 =
										((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt)
											((BgL_rtl_insz00_bglt) BgL_oz00_2188)));
									BgL_auxz00_6801 = BGL_OBJECT_WIDENING(BgL_tmpz00_6802);
								}
								BgL_auxz00_6800 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_6801);
							}
							((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_6800))->
									BgL_inz00) = ((obj_t) BgL_auxz00_6808), BUNSPEC);
						}
						{
							BgL_rtl_inszf2razf2_bglt BgL_auxz00_6836;

							{
								obj_t BgL_auxz00_6837;

								{	/* SawMill/regalloc.scm 145 */
									BgL_objectz00_bglt BgL_tmpz00_6838;

									BgL_tmpz00_6838 =
										((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt)
											((BgL_rtl_insz00_bglt) BgL_oz00_2188)));
									BgL_auxz00_6837 = BGL_OBJECT_WIDENING(BgL_tmpz00_6838);
								}
								BgL_auxz00_6836 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_6837);
							}
							((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_6836))->
									BgL_spillz00) =
								((obj_t) ((obj_t)
										BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00
										(BgL_regsz00_6051))), BUNSPEC);
						}
						((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_2188));
						{	/* SawMill/regalloc.scm 146 */
							obj_t BgL_g1522z00_2214;

							BgL_g1522z00_2214 =
								(((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_oz00_2188)))->BgL_argsz00);
							{
								obj_t BgL_l1520z00_2216;

								BgL_l1520z00_2216 = BgL_g1522z00_2214;
							BgL_zc3z04anonymousza31886ze3z87_2217:
								if (PAIRP(BgL_l1520z00_2216))
									{	/* SawMill/regalloc.scm 146 */
										BGl_argszd2widenzd2raz12ze70zf5zzsaw_registerzd2allocationzd2
											(BgL_regsz00_6051, CAR(BgL_l1520z00_2216));
										{
											obj_t BgL_l1520z00_6855;

											BgL_l1520z00_6855 = CDR(BgL_l1520z00_2216);
											BgL_l1520z00_2216 = BgL_l1520z00_6855;
											goto BgL_zc3z04anonymousza31886ze3z87_2217;
										}
									}
								else
									{	/* SawMill/regalloc.scm 146 */
										return ((bool_t) 1);
									}
							}
						}
					}
				else
					{	/* SawMill/regalloc.scm 139 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* liveness! */
	obj_t BGl_livenessz12z12zzsaw_registerzd2allocationzd2(obj_t BgL_blocksz00_71,
		obj_t BgL_argsz00_72)
	{
		{	/* SawMill/regalloc.scm 171 */
			{	/* SawMill/regalloc.scm 172 */
				obj_t BgL_list1927z00_2286;

				BgL_list1927z00_2286 =
					MAKE_YOUNG_PAIR(BGl_string2791z00zzsaw_registerzd2allocationzd2,
					BNIL);
				BGl_verbosez00zztools_speekz00(BINT(3L), BgL_list1927z00_2286);
			}
			if (NULLP(BgL_blocksz00_71))
				{	/* SawMill/regalloc.scm 175 */
					((bool_t) 0);
				}
			else
				{	/* SawMill/regalloc.scm 176 */
					obj_t BgL_inssz00_2288;

					BgL_inssz00_2288 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) CAR(BgL_blocksz00_71))))->BgL_firstz00);
					if (NULLP(BgL_inssz00_2288))
						{	/* SawMill/regalloc.scm 177 */
							((bool_t) 0);
						}
					else
						{	/* SawMill/regalloc.scm 178 */
							obj_t BgL_insz00_2290;

							BgL_insz00_2290 = CAR(BgL_inssz00_2288);
							{
								obj_t BgL_l1537z00_2293;

								BgL_l1537z00_2293 = BgL_argsz00_72;
							BgL_zc3z04anonymousza31930ze3z87_2294:
								if (PAIRP(BgL_l1537z00_2293))
									{	/* SawMill/regalloc.scm 180 */
										{	/* SawMill/regalloc.scm 180 */
											obj_t BgL_az00_2296;

											BgL_az00_2296 = CAR(BgL_l1537z00_2293);
											{	/* SawMill/regalloc.scm 180 */
												obj_t BgL_arg1932z00_2297;

												{
													BgL_rtl_inszf2razf2_bglt BgL_auxz00_6871;

													{
														obj_t BgL_auxz00_6872;

														{	/* SawMill/regalloc.scm 180 */
															BgL_objectz00_bglt BgL_tmpz00_6873;

															BgL_tmpz00_6873 =
																((BgL_objectz00_bglt)
																((BgL_rtl_insz00_bglt) BgL_insz00_2290));
															BgL_auxz00_6872 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_6873);
														}
														BgL_auxz00_6871 =
															((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_6872);
													}
													BgL_arg1932z00_2297 =
														(((BgL_rtl_inszf2razf2_bglt)
															COBJECT(BgL_auxz00_6871))->BgL_inz00);
												}
												BGl_regsetzd2addz12zc0zzsaw_regsetz00(
													((BgL_regsetz00_bglt) BgL_arg1932z00_2297),
													((BgL_rtl_regz00_bglt) BgL_az00_2296));
											}
										}
										{
											obj_t BgL_l1537z00_6882;

											BgL_l1537z00_6882 = CDR(BgL_l1537z00_2293);
											BgL_l1537z00_2293 = BgL_l1537z00_6882;
											goto BgL_zc3z04anonymousza31930ze3z87_2294;
										}
									}
								else
									{	/* SawMill/regalloc.scm 180 */
										((bool_t) 1);
									}
							}
						}
				}
			{
				long BgL_iz00_2302;

				BgL_iz00_2302 = 0L;
			BgL_zc3z04anonymousza31935ze3z87_2303:
				{	/* SawMill/regalloc.scm 183 */
					obj_t BgL_list1936z00_2304;

					{	/* SawMill/regalloc.scm 183 */
						obj_t BgL_arg1937z00_2305;

						BgL_arg1937z00_2305 = MAKE_YOUNG_PAIR(BINT(BgL_iz00_2302), BNIL);
						BgL_list1936z00_2304 =
							MAKE_YOUNG_PAIR(BGl_string2792z00zzsaw_registerzd2allocationzd2,
							BgL_arg1937z00_2305);
					}
					BGl_verbosez00zztools_speekz00(BINT(3L), BgL_list1936z00_2304);
				}
				{
					obj_t BgL_bsz00_2307;
					obj_t BgL_tz00_2308;

					BgL_bsz00_2307 = BgL_blocksz00_71;
					BgL_tz00_2308 = BFALSE;
				BgL_zc3z04anonymousza31938ze3z87_2309:
					if (NULLP(BgL_bsz00_2307))
						{	/* SawMill/regalloc.scm 186 */
							if (CBOOL(BgL_tz00_2308))
								{
									long BgL_iz00_6893;

									BgL_iz00_6893 = (BgL_iz00_2302 + 1L);
									BgL_iz00_2302 = BgL_iz00_6893;
									goto BgL_zc3z04anonymousza31935ze3z87_2303;
								}
							else
								{	/* SawMill/regalloc.scm 187 */
									((bool_t) 0);
								}
						}
					else
						{	/* SawMill/regalloc.scm 190 */
							obj_t BgL_arg1941z00_2312;
							obj_t BgL_arg1942z00_2313;

							BgL_arg1941z00_2312 = CDR(((obj_t) BgL_bsz00_2307));
							{	/* SawMill/regalloc.scm 190 */
								obj_t BgL__ortest_1208z00_2314;

								{	/* SawMill/regalloc.scm 190 */
									obj_t BgL_arg1943z00_2315;

									BgL_arg1943z00_2315 = CAR(((obj_t) BgL_bsz00_2307));
									BgL__ortest_1208z00_2314 =
										BGl_livenesszd2blockz12zc0zzsaw_registerzd2allocationzd2
										(BgL_arg1943z00_2315);
								}
								if (CBOOL(BgL__ortest_1208z00_2314))
									{	/* SawMill/regalloc.scm 190 */
										BgL_arg1942z00_2313 = BgL__ortest_1208z00_2314;
									}
								else
									{	/* SawMill/regalloc.scm 190 */
										BgL_arg1942z00_2313 = BgL_tz00_2308;
									}
							}
							{
								obj_t BgL_tz00_6903;
								obj_t BgL_bsz00_6902;

								BgL_bsz00_6902 = BgL_arg1941z00_2312;
								BgL_tz00_6903 = BgL_arg1942z00_2313;
								BgL_tz00_2308 = BgL_tz00_6903;
								BgL_bsz00_2307 = BgL_bsz00_6902;
								goto BgL_zc3z04anonymousza31938ze3z87_2309;
							}
						}
				}
			}
			{	/* SawMill/regalloc.scm 191 */
				obj_t BgL_list1944z00_2318;

				BgL_list1944z00_2318 =
					MAKE_YOUNG_PAIR(BGl_string2793z00zzsaw_registerzd2allocationzd2,
					BNIL);
				return BGl_verbosez00zztools_speekz00(BINT(3L), BgL_list1944z00_2318);
			}
		}

	}



/* liveness-block! */
	obj_t BGl_livenesszd2blockz12zc0zzsaw_registerzd2allocationzd2(obj_t
		BgL_blockz00_73)
	{
		{	/* SawMill/regalloc.scm 196 */
			{	/* SawMill/regalloc.scm 198 */
				obj_t BgL_g1210z00_2320;

				{	/* SawMill/regalloc.scm 199 */
					obj_t BgL_l1539z00_2367;

					BgL_l1539z00_2367 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_blockz00_73))))->BgL_succsz00);
					if (NULLP(BgL_l1539z00_2367))
						{	/* SawMill/regalloc.scm 199 */
							BgL_g1210z00_2320 = BNIL;
						}
					else
						{	/* SawMill/regalloc.scm 199 */
							obj_t BgL_head1541z00_2369;

							BgL_head1541z00_2369 = MAKE_YOUNG_PAIR(BNIL, BNIL);
							{
								obj_t BgL_l1539z00_2371;
								obj_t BgL_tail1542z00_2372;

								BgL_l1539z00_2371 = BgL_l1539z00_2367;
								BgL_tail1542z00_2372 = BgL_head1541z00_2369;
							BgL_zc3z04anonymousza31973ze3z87_2373:
								if (NULLP(BgL_l1539z00_2371))
									{	/* SawMill/regalloc.scm 199 */
										BgL_g1210z00_2320 = CDR(BgL_head1541z00_2369);
									}
								else
									{	/* SawMill/regalloc.scm 199 */
										obj_t BgL_newtail1543z00_2375;

										{	/* SawMill/regalloc.scm 199 */
											obj_t BgL_arg1976z00_2377;

											{	/* SawMill/regalloc.scm 199 */
												obj_t BgL_pairz00_4087;

												BgL_pairz00_4087 =
													(((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt)
																CAR(
																	((obj_t) BgL_l1539z00_2371)))))->
													BgL_firstz00);
												BgL_arg1976z00_2377 = CAR(BgL_pairz00_4087);
											}
											BgL_newtail1543z00_2375 =
												MAKE_YOUNG_PAIR(BgL_arg1976z00_2377, BNIL);
										}
										SET_CDR(BgL_tail1542z00_2372, BgL_newtail1543z00_2375);
										{	/* SawMill/regalloc.scm 199 */
											obj_t BgL_arg1975z00_2376;

											BgL_arg1975z00_2376 = CDR(((obj_t) BgL_l1539z00_2371));
											{
												obj_t BgL_tail1542z00_6926;
												obj_t BgL_l1539z00_6925;

												BgL_l1539z00_6925 = BgL_arg1975z00_2376;
												BgL_tail1542z00_6926 = BgL_newtail1543z00_2375;
												BgL_tail1542z00_2372 = BgL_tail1542z00_6926;
												BgL_l1539z00_2371 = BgL_l1539z00_6925;
												goto BgL_zc3z04anonymousza31973ze3z87_2373;
											}
										}
									}
							}
						}
				}
				{
					obj_t BgL_inssz00_2323;
					obj_t BgL_succz00_2324;
					obj_t BgL_tz00_2325;

					{	/* SawMill/regalloc.scm 198 */
						obj_t BgL_arg1945z00_2322;

						{
							BgL_blockzf2razf2_bglt BgL_auxz00_6927;

							{
								obj_t BgL_auxz00_6928;

								{	/* SawMill/regalloc.scm 198 */
									BgL_objectz00_bglt BgL_tmpz00_6929;

									BgL_tmpz00_6929 =
										((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_blockz00_73));
									BgL_auxz00_6928 = BGL_OBJECT_WIDENING(BgL_tmpz00_6929);
								}
								BgL_auxz00_6927 = ((BgL_blockzf2razf2_bglt) BgL_auxz00_6928);
							}
							BgL_arg1945z00_2322 =
								(((BgL_blockzf2razf2_bglt) COBJECT(BgL_auxz00_6927))->
								BgL_lastz00);
						}
						BgL_inssz00_2323 = BgL_arg1945z00_2322;
						BgL_succz00_2324 = BgL_g1210z00_2320;
						BgL_tz00_2325 = BFALSE;
					BgL_zc3z04anonymousza31946ze3z87_2326:
						if (PAIRP(BgL_inssz00_2323))
							{	/* SawMill/regalloc.scm 202 */
								BgL_rtl_insz00_bglt BgL_i1211z00_2328;

								BgL_i1211z00_2328 =
									((BgL_rtl_insz00_bglt) CAR(BgL_inssz00_2323));
								{	/* SawMill/regalloc.scm 203 */
									obj_t BgL_uz00_5885;

									{	/* SawMill/regalloc.scm 204 */
										obj_t BgL_cellvalz00_6939;

										{	/* SawMill/regalloc.scm 204 */
											bool_t BgL_test2966z00_6940;

											{	/* SawMill/regalloc.scm 204 */
												obj_t BgL_classz00_4091;

												BgL_classz00_4091 = BGl_rtl_insz00zzsaw_defsz00;
												if (BGL_OBJECTP(BgL_succz00_2324))
													{	/* SawMill/regalloc.scm 204 */
														BgL_objectz00_bglt BgL_arg1807z00_4093;

														BgL_arg1807z00_4093 =
															(BgL_objectz00_bglt) (BgL_succz00_2324);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawMill/regalloc.scm 204 */
																long BgL_idxz00_4099;

																BgL_idxz00_4099 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_4093);
																BgL_test2966z00_6940 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_4099 + 1L)) ==
																	BgL_classz00_4091);
															}
														else
															{	/* SawMill/regalloc.scm 204 */
																bool_t BgL_res2734z00_4124;

																{	/* SawMill/regalloc.scm 204 */
																	obj_t BgL_oclassz00_4107;

																	{	/* SawMill/regalloc.scm 204 */
																		obj_t BgL_arg1815z00_4115;
																		long BgL_arg1816z00_4116;

																		BgL_arg1815z00_4115 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawMill/regalloc.scm 204 */
																			long BgL_arg1817z00_4117;

																			BgL_arg1817z00_4117 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_4093);
																			BgL_arg1816z00_4116 =
																				(BgL_arg1817z00_4117 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_4107 =
																			VECTOR_REF(BgL_arg1815z00_4115,
																			BgL_arg1816z00_4116);
																	}
																	{	/* SawMill/regalloc.scm 204 */
																		bool_t BgL__ortest_1115z00_4108;

																		BgL__ortest_1115z00_4108 =
																			(BgL_classz00_4091 == BgL_oclassz00_4107);
																		if (BgL__ortest_1115z00_4108)
																			{	/* SawMill/regalloc.scm 204 */
																				BgL_res2734z00_4124 =
																					BgL__ortest_1115z00_4108;
																			}
																		else
																			{	/* SawMill/regalloc.scm 204 */
																				long BgL_odepthz00_4109;

																				{	/* SawMill/regalloc.scm 204 */
																					obj_t BgL_arg1804z00_4110;

																					BgL_arg1804z00_4110 =
																						(BgL_oclassz00_4107);
																					BgL_odepthz00_4109 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_4110);
																				}
																				if ((1L < BgL_odepthz00_4109))
																					{	/* SawMill/regalloc.scm 204 */
																						obj_t BgL_arg1802z00_4112;

																						{	/* SawMill/regalloc.scm 204 */
																							obj_t BgL_arg1803z00_4113;

																							BgL_arg1803z00_4113 =
																								(BgL_oclassz00_4107);
																							BgL_arg1802z00_4112 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_4113, 1L);
																						}
																						BgL_res2734z00_4124 =
																							(BgL_arg1802z00_4112 ==
																							BgL_classz00_4091);
																					}
																				else
																					{	/* SawMill/regalloc.scm 204 */
																						BgL_res2734z00_4124 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2966z00_6940 = BgL_res2734z00_4124;
															}
													}
												else
													{	/* SawMill/regalloc.scm 204 */
														BgL_test2966z00_6940 = ((bool_t) 0);
													}
											}
											if (BgL_test2966z00_6940)
												{	/* SawMill/regalloc.scm 205 */
													obj_t BgL_arg1959z00_2345;
													obj_t BgL_arg1960z00_2346;

													{
														BgL_rtl_inszf2razf2_bglt BgL_auxz00_6963;

														{
															obj_t BgL_auxz00_6964;

															{	/* SawMill/regalloc.scm 205 */
																BgL_objectz00_bglt BgL_tmpz00_6965;

																BgL_tmpz00_6965 =
																	((BgL_objectz00_bglt) BgL_i1211z00_2328);
																BgL_auxz00_6964 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_6965);
															}
															BgL_auxz00_6963 =
																((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_6964);
														}
														BgL_arg1959z00_2345 =
															(((BgL_rtl_inszf2razf2_bglt)
																COBJECT(BgL_auxz00_6963))->BgL_outz00);
													}
													{
														BgL_rtl_inszf2razf2_bglt BgL_auxz00_6970;

														{
															obj_t BgL_auxz00_6971;

															{	/* SawMill/regalloc.scm 205 */
																BgL_objectz00_bglt BgL_tmpz00_6972;

																BgL_tmpz00_6972 =
																	((BgL_objectz00_bglt)
																	((BgL_rtl_insz00_bglt) BgL_succz00_2324));
																BgL_auxz00_6971 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_6972);
															}
															BgL_auxz00_6970 =
																((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_6971);
														}
														BgL_arg1960z00_2346 =
															(((BgL_rtl_inszf2razf2_bglt)
																COBJECT(BgL_auxz00_6970))->BgL_inz00);
													}
													BgL_cellvalz00_6939 =
														BBOOL(BGl_regsetzd2unionz12zc0zzsaw_regsetz00(
															((BgL_regsetz00_bglt) BgL_arg1959z00_2345),
															((BgL_regsetz00_bglt) BgL_arg1960z00_2346)));
												}
											else
												{	/* SawMill/regalloc.scm 204 */
													if (PAIRP(BgL_succz00_2324))
														{	/* SawMill/regalloc.scm 207 */
															obj_t BgL_arg1962z00_2348;
															obj_t BgL_arg1963z00_2349;

															{
																BgL_rtl_inszf2razf2_bglt BgL_auxz00_6984;

																{
																	obj_t BgL_auxz00_6985;

																	{	/* SawMill/regalloc.scm 207 */
																		BgL_objectz00_bglt BgL_tmpz00_6986;

																		BgL_tmpz00_6986 =
																			((BgL_objectz00_bglt) BgL_i1211z00_2328);
																		BgL_auxz00_6985 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_6986);
																	}
																	BgL_auxz00_6984 =
																		((BgL_rtl_inszf2razf2_bglt)
																		BgL_auxz00_6985);
																}
																BgL_arg1962z00_2348 =
																	(((BgL_rtl_inszf2razf2_bglt)
																		COBJECT(BgL_auxz00_6984))->BgL_outz00);
															}
															{	/* SawMill/regalloc.scm 207 */
																obj_t BgL_head1546z00_2352;

																{	/* SawMill/regalloc.scm 207 */
																	obj_t BgL_arg1970z00_2364;

																	{	/* SawMill/regalloc.scm 207 */
																		BgL_rtl_insz00_bglt BgL_oz00_4130;

																		BgL_oz00_4130 =
																			((BgL_rtl_insz00_bglt)
																			CAR(BgL_succz00_2324));
																		{
																			BgL_rtl_inszf2razf2_bglt BgL_auxz00_6993;

																			{
																				obj_t BgL_auxz00_6994;

																				{	/* SawMill/regalloc.scm 207 */
																					BgL_objectz00_bglt BgL_tmpz00_6995;

																					BgL_tmpz00_6995 =
																						((BgL_objectz00_bglt)
																						BgL_oz00_4130);
																					BgL_auxz00_6994 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_6995);
																				}
																				BgL_auxz00_6993 =
																					((BgL_rtl_inszf2razf2_bglt)
																					BgL_auxz00_6994);
																			}
																			BgL_arg1970z00_2364 =
																				(((BgL_rtl_inszf2razf2_bglt)
																					COBJECT(BgL_auxz00_6993))->BgL_inz00);
																		}
																	}
																	BgL_head1546z00_2352 =
																		MAKE_YOUNG_PAIR(BgL_arg1970z00_2364, BNIL);
																}
																{	/* SawMill/regalloc.scm 207 */
																	obj_t BgL_g1549z00_2353;

																	BgL_g1549z00_2353 = CDR(BgL_succz00_2324);
																	{
																		obj_t BgL_l1544z00_2355;
																		obj_t BgL_tail1547z00_2356;

																		BgL_l1544z00_2355 = BgL_g1549z00_2353;
																		BgL_tail1547z00_2356 = BgL_head1546z00_2352;
																	BgL_zc3z04anonymousza31965ze3z87_2357:
																		if (NULLP(BgL_l1544z00_2355))
																			{	/* SawMill/regalloc.scm 207 */
																				BgL_arg1963z00_2349 =
																					BgL_head1546z00_2352;
																			}
																		else
																			{	/* SawMill/regalloc.scm 207 */
																				obj_t BgL_newtail1548z00_2359;

																				{	/* SawMill/regalloc.scm 207 */
																					obj_t BgL_arg1968z00_2361;

																					{	/* SawMill/regalloc.scm 207 */
																						BgL_rtl_insz00_bglt BgL_oz00_4134;

																						BgL_oz00_4134 =
																							((BgL_rtl_insz00_bglt)
																							CAR(((obj_t) BgL_l1544z00_2355)));
																						{
																							BgL_rtl_inszf2razf2_bglt
																								BgL_auxz00_7007;
																							{
																								obj_t BgL_auxz00_7008;

																								{	/* SawMill/regalloc.scm 207 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_7009;
																									BgL_tmpz00_7009 =
																										((BgL_objectz00_bglt)
																										BgL_oz00_4134);
																									BgL_auxz00_7008 =
																										BGL_OBJECT_WIDENING
																										(BgL_tmpz00_7009);
																								}
																								BgL_auxz00_7007 =
																									((BgL_rtl_inszf2razf2_bglt)
																									BgL_auxz00_7008);
																							}
																							BgL_arg1968z00_2361 =
																								(((BgL_rtl_inszf2razf2_bglt)
																									COBJECT(BgL_auxz00_7007))->
																								BgL_inz00);
																						}
																					}
																					BgL_newtail1548z00_2359 =
																						MAKE_YOUNG_PAIR(BgL_arg1968z00_2361,
																						BNIL);
																				}
																				SET_CDR(BgL_tail1547z00_2356,
																					BgL_newtail1548z00_2359);
																				{	/* SawMill/regalloc.scm 207 */
																					obj_t BgL_arg1967z00_2360;

																					BgL_arg1967z00_2360 =
																						CDR(((obj_t) BgL_l1544z00_2355));
																					{
																						obj_t BgL_tail1547z00_7019;
																						obj_t BgL_l1544z00_7018;

																						BgL_l1544z00_7018 =
																							BgL_arg1967z00_2360;
																						BgL_tail1547z00_7019 =
																							BgL_newtail1548z00_2359;
																						BgL_tail1547z00_2356 =
																							BgL_tail1547z00_7019;
																						BgL_l1544z00_2355 =
																							BgL_l1544z00_7018;
																						goto
																							BgL_zc3z04anonymousza31965ze3z87_2357;
																					}
																				}
																			}
																	}
																}
															}
															BgL_cellvalz00_6939 =
																BBOOL(BGl_regsetzd2unionza2z12z62zzsaw_regsetz00
																(((BgL_regsetz00_bglt) BgL_arg1962z00_2348),
																	BgL_arg1963z00_2349));
														}
													else
														{	/* SawMill/regalloc.scm 206 */
															BgL_cellvalz00_6939 = BFALSE;
														}
												}
										}
										BgL_uz00_5885 = MAKE_CELL(BgL_cellvalz00_6939);
									}
									{	/* SawMill/regalloc.scm 210 */
										obj_t BgL_arg1949z00_2331;

										{
											BgL_rtl_inszf2razf2_bglt BgL_auxz00_7023;

											{
												obj_t BgL_auxz00_7024;

												{	/* SawMill/regalloc.scm 210 */
													BgL_objectz00_bglt BgL_tmpz00_7025;

													BgL_tmpz00_7025 =
														((BgL_objectz00_bglt) BgL_i1211z00_2328);
													BgL_auxz00_7024 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_7025);
												}
												BgL_auxz00_7023 =
													((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_7024);
											}
											BgL_arg1949z00_2331 =
												(((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_7023))->
												BgL_outz00);
										}
										{	/* SawMill/regalloc.scm 211 */
											obj_t BgL_zc3z04anonymousza31950ze3z87_5879;

											BgL_zc3z04anonymousza31950ze3z87_5879 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31950ze3ze5zzsaw_registerzd2allocationzd2,
												(int) (1L), (int) (2L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31950ze3z87_5879,
												(int) (0L), ((obj_t) BgL_i1211z00_2328));
											PROCEDURE_SET(BgL_zc3z04anonymousza31950ze3z87_5879,
												(int) (1L), ((obj_t) BgL_uz00_5885));
											BGl_regsetzd2forzd2eachz00zzsaw_regsetz00
												(BgL_zc3z04anonymousza31950ze3z87_5879,
												((BgL_regsetz00_bglt) BgL_arg1949z00_2331));
									}}
									{	/* SawMill/regalloc.scm 214 */
										obj_t BgL_arg1955z00_2340;
										obj_t BgL_arg1956z00_2341;
										obj_t BgL_arg1957z00_2342;

										BgL_arg1955z00_2340 = CDR(BgL_inssz00_2323);
										BgL_arg1956z00_2341 = CAR(BgL_inssz00_2323);
										if (CBOOL(BgL_tz00_2325))
											{	/* SawMill/regalloc.scm 214 */
												BgL_arg1957z00_2342 = BgL_tz00_2325;
											}
										else
											{	/* SawMill/regalloc.scm 214 */
												BgL_arg1957z00_2342 = CELL_REF(BgL_uz00_5885);
											}
										{
											obj_t BgL_tz00_7047;
											obj_t BgL_succz00_7046;
											obj_t BgL_inssz00_7045;

											BgL_inssz00_7045 = BgL_arg1955z00_2340;
											BgL_succz00_7046 = BgL_arg1956z00_2341;
											BgL_tz00_7047 = BgL_arg1957z00_2342;
											BgL_tz00_2325 = BgL_tz00_7047;
											BgL_succz00_2324 = BgL_succz00_7046;
											BgL_inssz00_2323 = BgL_inssz00_7045;
											goto BgL_zc3z04anonymousza31946ze3z87_2326;
										}
									}
								}
							}
						else
							{	/* SawMill/regalloc.scm 201 */
								return BgL_tz00_2325;
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1950> */
	obj_t BGl_z62zc3z04anonymousza31950ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5880, obj_t BgL_rz00_5883)
	{
		{	/* SawMill/regalloc.scm 210 */
			{	/* SawMill/regalloc.scm 211 */
				BgL_rtl_insz00_bglt BgL_i1211z00_5881;
				obj_t BgL_uz00_5882;

				BgL_i1211z00_5881 =
					((BgL_rtl_insz00_bglt) PROCEDURE_REF(BgL_envz00_5880, (int) (0L)));
				BgL_uz00_5882 = PROCEDURE_REF(BgL_envz00_5880, (int) (1L));
				{	/* SawMill/regalloc.scm 211 */
					bool_t BgL_test2974z00_7053;

					{	/* SawMill/regalloc.scm 211 */
						obj_t BgL_arg1954z00_6064;

						{
							BgL_rtl_inszf2razf2_bglt BgL_auxz00_7054;

							{
								obj_t BgL_auxz00_7055;

								{	/* SawMill/regalloc.scm 211 */
									BgL_objectz00_bglt BgL_tmpz00_7056;

									BgL_tmpz00_7056 = ((BgL_objectz00_bglt) BgL_i1211z00_5881);
									BgL_auxz00_7055 = BGL_OBJECT_WIDENING(BgL_tmpz00_7056);
								}
								BgL_auxz00_7054 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_7055);
							}
							BgL_arg1954z00_6064 =
								(((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_7054))->
								BgL_defz00);
						}
						{	/* SawMill/regalloc.scm 211 */
							long BgL_basez00_6065;
							long BgL_bitz00_6066;

							{	/* SawMill/regalloc.scm 211 */
								int BgL_arg2607z00_6067;

								{
									BgL_rtl_regzf2razf2_bglt BgL_auxz00_7061;

									{
										obj_t BgL_auxz00_7062;

										{	/* SawMill/regalloc.scm 211 */
											BgL_objectz00_bglt BgL_tmpz00_7063;

											BgL_tmpz00_7063 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_rz00_5883));
											BgL_auxz00_7062 = BGL_OBJECT_WIDENING(BgL_tmpz00_7063);
										}
										BgL_auxz00_7061 =
											((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_7062);
									}
									BgL_arg2607z00_6067 =
										(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_7061))->
										BgL_numz00);
								}
								BgL_basez00_6065 = ((long) (BgL_arg2607z00_6067) / 8L);
							}
							{	/* SawMill/regalloc.scm 211 */
								int BgL_arg2608z00_6068;

								{
									BgL_rtl_regzf2razf2_bglt BgL_auxz00_7071;

									{
										obj_t BgL_auxz00_7072;

										{	/* SawMill/regalloc.scm 211 */
											BgL_objectz00_bglt BgL_tmpz00_7073;

											BgL_tmpz00_7073 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_rz00_5883));
											BgL_auxz00_7072 = BGL_OBJECT_WIDENING(BgL_tmpz00_7073);
										}
										BgL_auxz00_7071 =
											((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_7072);
									}
									BgL_arg2608z00_6068 =
										(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_7071))->
										BgL_numz00);
								}
								{	/* SawMill/regalloc.scm 211 */
									long BgL_n1z00_6069;
									long BgL_n2z00_6070;

									BgL_n1z00_6069 = (long) (BgL_arg2608z00_6068);
									BgL_n2z00_6070 = 8L;
									{	/* SawMill/regalloc.scm 211 */
										bool_t BgL_test2975z00_7080;

										{	/* SawMill/regalloc.scm 211 */
											long BgL_arg1338z00_6071;

											BgL_arg1338z00_6071 =
												(((BgL_n1z00_6069) | (BgL_n2z00_6070)) & -2147483648);
											BgL_test2975z00_7080 = (BgL_arg1338z00_6071 == 0L);
										}
										if (BgL_test2975z00_7080)
											{	/* SawMill/regalloc.scm 211 */
												int32_t BgL_arg1334z00_6072;

												{	/* SawMill/regalloc.scm 211 */
													int32_t BgL_arg1336z00_6073;
													int32_t BgL_arg1337z00_6074;

													BgL_arg1336z00_6073 = (int32_t) (BgL_n1z00_6069);
													BgL_arg1337z00_6074 = (int32_t) (BgL_n2z00_6070);
													BgL_arg1334z00_6072 =
														(BgL_arg1336z00_6073 % BgL_arg1337z00_6074);
												}
												{	/* SawMill/regalloc.scm 211 */
													long BgL_arg1446z00_6075;

													BgL_arg1446z00_6075 = (long) (BgL_arg1334z00_6072);
													BgL_bitz00_6066 = (long) (BgL_arg1446z00_6075);
											}}
										else
											{	/* SawMill/regalloc.scm 211 */
												BgL_bitz00_6066 = (BgL_n1z00_6069 % BgL_n2z00_6070);
											}
									}
								}
							}
							if (
								(BgL_basez00_6065 <
									STRING_LENGTH(
										(((BgL_regsetz00_bglt) COBJECT(
													((BgL_regsetz00_bglt) BgL_arg1954z00_6064)))->
											BgL_stringz00))))
								{	/* SawMill/regalloc.scm 211 */
									BgL_test2974z00_7053 =
										(
										((STRING_REF(
													(((BgL_regsetz00_bglt) COBJECT(
																((BgL_regsetz00_bglt) BgL_arg1954z00_6064)))->
														BgL_stringz00),
													BgL_basez00_6065)) & (1L << (int) (BgL_bitz00_6066)))
										> 0L);
								}
							else
								{	/* SawMill/regalloc.scm 211 */
									BgL_test2974z00_7053 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test2974z00_7053)
						{	/* SawMill/regalloc.scm 211 */
							return BFALSE;
						}
					else
						{	/* SawMill/regalloc.scm 212 */
							obj_t BgL_auxz00_6076;

							{	/* SawMill/regalloc.scm 212 */
								bool_t BgL__ortest_1212z00_6077;

								{	/* SawMill/regalloc.scm 212 */
									obj_t BgL_arg1953z00_6078;

									{
										BgL_rtl_inszf2razf2_bglt BgL_auxz00_7102;

										{
											obj_t BgL_auxz00_7103;

											{	/* SawMill/regalloc.scm 212 */
												BgL_objectz00_bglt BgL_tmpz00_7104;

												BgL_tmpz00_7104 =
													((BgL_objectz00_bglt) BgL_i1211z00_5881);
												BgL_auxz00_7103 = BGL_OBJECT_WIDENING(BgL_tmpz00_7104);
											}
											BgL_auxz00_7102 =
												((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_7103);
										}
										BgL_arg1953z00_6078 =
											(((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_7102))->
											BgL_inz00);
									}
									BgL__ortest_1212z00_6077 =
										BGl_regsetzd2addz12zc0zzsaw_regsetz00(
										((BgL_regsetz00_bglt) BgL_arg1953z00_6078),
										((BgL_rtl_regz00_bglt) BgL_rz00_5883));
								}
								if (BgL__ortest_1212z00_6077)
									{	/* SawMill/regalloc.scm 212 */
										BgL_auxz00_6076 = BBOOL(BgL__ortest_1212z00_6077);
									}
								else
									{	/* SawMill/regalloc.scm 212 */
										BgL_auxz00_6076 = CELL_REF(BgL_uz00_5882);
									}
							}
							return CELL_SET(BgL_uz00_5882, BgL_auxz00_6076);
						}
				}
			}
		}

	}



/* hardware-mutual-interference! */
	bool_t
		BGl_hardwarezd2mutualzd2interferencez12z12zzsaw_registerzd2allocationzd2
		(obj_t BgL_regsz00_76)
	{
		{	/* SawMill/regalloc.scm 237 */
			{
				obj_t BgL_rz00_2382;

				BgL_rz00_2382 = BgL_regsz00_76;
			BgL_zc3z04anonymousza31978ze3z87_2383:
				if (PAIRP(BgL_rz00_2382))
					{	/* SawMill/regalloc.scm 240 */
						obj_t BgL_r1z00_2385;

						BgL_r1z00_2385 = CAR(BgL_rz00_2382);
						{	/* SawMill/regalloc.scm 241 */
							obj_t BgL_g1559z00_2386;

							BgL_g1559z00_2386 = CDR(BgL_rz00_2382);
							{
								obj_t BgL_l1557z00_2388;

								BgL_l1557z00_2388 = BgL_g1559z00_2386;
							BgL_zc3z04anonymousza31980ze3z87_2389:
								if (PAIRP(BgL_l1557z00_2388))
									{	/* SawMill/regalloc.scm 241 */
										BGl_interferezd2regz12zc0zzsaw_registerzd2allocationzd2
											(BgL_r1z00_2385, CAR(BgL_l1557z00_2388));
										{
											obj_t BgL_l1557z00_7122;

											BgL_l1557z00_7122 = CDR(BgL_l1557z00_2388);
											BgL_l1557z00_2388 = BgL_l1557z00_7122;
											goto BgL_zc3z04anonymousza31980ze3z87_2389;
										}
									}
								else
									{	/* SawMill/regalloc.scm 241 */
										((bool_t) 1);
									}
							}
						}
						{
							obj_t BgL_rz00_7124;

							BgL_rz00_7124 = CDR(BgL_rz00_2382);
							BgL_rz00_2382 = BgL_rz00_7124;
							goto BgL_zc3z04anonymousza31978ze3z87_2383;
						}
					}
				else
					{	/* SawMill/regalloc.scm 239 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* hardware-parameters-interference! */
	bool_t
		BGl_hardwarezd2parameterszd2interferencez12z12zzsaw_registerzd2allocationzd2
		(BgL_backendz00_bglt BgL_backz00_77, obj_t BgL_pregsz00_78)
	{
		{	/* SawMill/regalloc.scm 254 */
			{	/* SawMill/regalloc.scm 256 */
				obj_t BgL_g1564z00_2397;

				BgL_g1564z00_2397 =
					(((BgL_backendz00_bglt) COBJECT(BgL_backz00_77))->BgL_registersz00);
				{
					obj_t BgL_l1562z00_2399;

					BgL_l1562z00_2399 = BgL_g1564z00_2397;
				BgL_zc3z04anonymousza31984ze3z87_2400:
					if (PAIRP(BgL_l1562z00_2399))
						{	/* SawMill/regalloc.scm 256 */
							{	/* SawMill/regalloc.scm 257 */
								obj_t BgL_rz00_2402;

								BgL_rz00_2402 = CAR(BgL_l1562z00_2399);
								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
										(BgL_rz00_2402,
											(((BgL_backendz00_bglt) COBJECT(BgL_backz00_77))->
												BgL_pregistersz00))))
									{	/* SawMill/regalloc.scm 257 */
										((bool_t) 0);
									}
								else
									{
										obj_t BgL_l1560z00_2406;

										BgL_l1560z00_2406 = BgL_pregsz00_78;
									BgL_zc3z04anonymousza31988ze3z87_2407:
										if (PAIRP(BgL_l1560z00_2406))
											{	/* SawMill/regalloc.scm 258 */
												BGl_interferezd2regz12zc0zzsaw_registerzd2allocationzd2
													(CAR(BgL_l1560z00_2406), BgL_rz00_2402);
												{
													obj_t BgL_l1560z00_7138;

													BgL_l1560z00_7138 = CDR(BgL_l1560z00_2406);
													BgL_l1560z00_2406 = BgL_l1560z00_7138;
													goto BgL_zc3z04anonymousza31988ze3z87_2407;
												}
											}
										else
											{	/* SawMill/regalloc.scm 258 */
												((bool_t) 1);
											}
									}
							}
							{
								obj_t BgL_l1562z00_7140;

								BgL_l1562z00_7140 = CDR(BgL_l1562z00_2399);
								BgL_l1562z00_2399 = BgL_l1562z00_7140;
								goto BgL_zc3z04anonymousza31984ze3z87_2400;
							}
						}
					else
						{	/* SawMill/regalloc.scm 256 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* hardware-interference! */
	bool_t
		BGl_hardwarezd2interferencez12zc0zzsaw_registerzd2allocationzd2
		(BgL_backendz00_bglt BgL_backz00_79, obj_t BgL_oz00_80)
	{
		{	/* SawMill/regalloc.scm 269 */
			{
				obj_t BgL_oz00_2544;

				{
					obj_t BgL_l1587z00_2418;

					BgL_l1587z00_2418 = BgL_oz00_80;
				BgL_zc3z04anonymousza31993ze3z87_2419:
					if (PAIRP(BgL_l1587z00_2418))
						{	/* SawMill/regalloc.scm 317 */
							BgL_oz00_2544 = CAR(BgL_l1587z00_2418);
							{	/* SawMill/regalloc.scm 313 */
								obj_t BgL_g1586z00_2546;

								BgL_g1586z00_2546 =
									(((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt) BgL_oz00_2544)))->BgL_firstz00);
								{
									obj_t BgL_l1584z00_2548;

									BgL_l1584z00_2548 = BgL_g1586z00_2546;
								BgL_zc3z04anonymousza32074ze3z87_2549:
									if (PAIRP(BgL_l1584z00_2548))
										{	/* SawMill/regalloc.scm 316 */
											{	/* SawMill/regalloc.scm 314 */
												obj_t BgL_oz00_2551;

												BgL_oz00_2551 = CAR(BgL_l1584z00_2548);
												{	/* SawMill/regalloc.scm 315 */
													obj_t BgL_arg2076z00_2553;
													obj_t BgL_arg2077z00_2554;

													{
														BgL_rtl_inszf2razf2_bglt BgL_auxz00_7149;

														{
															obj_t BgL_auxz00_7150;

															{	/* SawMill/regalloc.scm 315 */
																BgL_objectz00_bglt BgL_tmpz00_7151;

																BgL_tmpz00_7151 =
																	((BgL_objectz00_bglt)
																	((BgL_rtl_insz00_bglt) BgL_oz00_2551));
																BgL_auxz00_7150 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_7151);
															}
															BgL_auxz00_7149 =
																((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_7150);
														}
														BgL_arg2076z00_2553 =
															(((BgL_rtl_inszf2razf2_bglt)
																COBJECT(BgL_auxz00_7149))->BgL_inz00);
													}
													{
														BgL_rtl_inszf2razf2_bglt BgL_auxz00_7157;

														{
															obj_t BgL_auxz00_7158;

															{	/* SawMill/regalloc.scm 315 */
																BgL_objectz00_bglt BgL_tmpz00_7159;

																BgL_tmpz00_7159 =
																	((BgL_objectz00_bglt)
																	((BgL_rtl_insz00_bglt) BgL_oz00_2551));
																BgL_auxz00_7158 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_7159);
															}
															BgL_auxz00_7157 =
																((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_7158);
														}
														BgL_arg2077z00_2554 =
															(((BgL_rtl_inszf2razf2_bglt)
																COBJECT(BgL_auxz00_7157))->BgL_outz00);
													}
													BGl_inszd2hardwarezd2interferencez12ze70zf5zzsaw_registerzd2allocationzd2
														(BgL_backz00_79,
														((BgL_rtl_insz00_bglt) BgL_oz00_2551),
														BgL_arg2076z00_2553, BgL_arg2077z00_2554);
												}
											}
											{
												obj_t BgL_l1584z00_7167;

												BgL_l1584z00_7167 = CDR(BgL_l1584z00_2548);
												BgL_l1584z00_2548 = BgL_l1584z00_7167;
												goto BgL_zc3z04anonymousza32074ze3z87_2549;
											}
										}
									else
										{	/* SawMill/regalloc.scm 316 */
											((bool_t) 1);
										}
								}
							}
							{
								obj_t BgL_l1587z00_7170;

								BgL_l1587z00_7170 = CDR(BgL_l1587z00_2418);
								BgL_l1587z00_2418 = BgL_l1587z00_7170;
								goto BgL_zc3z04anonymousza31993ze3z87_2419;
							}
						}
					else
						{	/* SawMill/regalloc.scm 317 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* ins-hardware-interference!~0 */
	bool_t
		BGl_inszd2hardwarezd2interferencez12ze70zf5zzsaw_registerzd2allocationzd2
		(BgL_backendz00_bglt BgL_backz00_6050, BgL_rtl_insz00_bglt BgL_oz00_2424,
		obj_t BgL_inz00_2425, obj_t BgL_outz00_2426)
	{
		{	/* SawMill/regalloc.scm 311 */
			{	/* SawMill/regalloc.scm 271 */
				obj_t BgL_resetz00_2428;

				BgL_resetz00_2428 =
					BGl_backendzd2instrzd2resetzd2registerszd2zzbackend_backendz00
					(BgL_backz00_6050, ((obj_t) BgL_oz00_2424));
				{	/* SawMill/regalloc.scm 272 */
					obj_t BgL_spillz00_2429;
					obj_t BgL_rdestz00_2430;
					obj_t BgL_a0z00_2431;
					obj_t BgL_a1z00_2432;

					{	/* SawMill/regalloc.scm 281 */
						obj_t BgL_tmpz00_4196;

						{	/* SawMill/regalloc.scm 281 */
							int BgL_tmpz00_7174;

							BgL_tmpz00_7174 = (int) (1L);
							BgL_tmpz00_4196 = BGL_MVALUES_VAL(BgL_tmpz00_7174);
						}
						{	/* SawMill/regalloc.scm 281 */
							int BgL_tmpz00_7177;

							BgL_tmpz00_7177 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_7177, BUNSPEC);
						}
						BgL_spillz00_2429 = BgL_tmpz00_4196;
					}
					{	/* SawMill/regalloc.scm 281 */
						obj_t BgL_tmpz00_4197;

						{	/* SawMill/regalloc.scm 281 */
							int BgL_tmpz00_7180;

							BgL_tmpz00_7180 = (int) (2L);
							BgL_tmpz00_4197 = BGL_MVALUES_VAL(BgL_tmpz00_7180);
						}
						{	/* SawMill/regalloc.scm 281 */
							int BgL_tmpz00_7183;

							BgL_tmpz00_7183 = (int) (2L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_7183, BUNSPEC);
						}
						BgL_rdestz00_2430 = BgL_tmpz00_4197;
					}
					{	/* SawMill/regalloc.scm 281 */
						obj_t BgL_tmpz00_4198;

						{	/* SawMill/regalloc.scm 281 */
							int BgL_tmpz00_7186;

							BgL_tmpz00_7186 = (int) (3L);
							BgL_tmpz00_4198 = BGL_MVALUES_VAL(BgL_tmpz00_7186);
						}
						{	/* SawMill/regalloc.scm 281 */
							int BgL_tmpz00_7189;

							BgL_tmpz00_7189 = (int) (3L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_7189, BUNSPEC);
						}
						BgL_a0z00_2431 = BgL_tmpz00_4198;
					}
					{	/* SawMill/regalloc.scm 281 */
						obj_t BgL_tmpz00_4199;

						{	/* SawMill/regalloc.scm 281 */
							int BgL_tmpz00_7192;

							BgL_tmpz00_7192 = (int) (4L);
							BgL_tmpz00_4199 = BGL_MVALUES_VAL(BgL_tmpz00_7192);
						}
						{	/* SawMill/regalloc.scm 281 */
							int BgL_tmpz00_7195;

							BgL_tmpz00_7195 = (int) (4L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_7195, BUNSPEC);
						}
						BgL_a1z00_2432 = BgL_tmpz00_4199;
					}
					if (NULLP(BgL_resetz00_2428))
						{	/* SawMill/regalloc.scm 273 */
							BFALSE;
						}
					else
						{	/* SawMill/regalloc.scm 273 */
							{	/* SawMill/regalloc.scm 275 */
								obj_t BgL_zc3z04anonymousza32000ze3z87_5888;

								BgL_zc3z04anonymousza32000ze3z87_5888 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza32000ze3ze5zzsaw_registerzd2allocationzd2,
									(int) (1L), (int) (1L));
								PROCEDURE_SET(BgL_zc3z04anonymousza32000ze3z87_5888, (int) (0L),
									BgL_resetz00_2428);
								BGl_regsetzd2forzd2eachz00zzsaw_regsetz00
									(BgL_zc3z04anonymousza32000ze3z87_5888,
									((BgL_regsetz00_bglt) BgL_inz00_2425));
							}
							{	/* SawMill/regalloc.scm 279 */
								obj_t BgL_zc3z04anonymousza32005ze3z87_5887;

								BgL_zc3z04anonymousza32005ze3z87_5887 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza32005ze3ze5zzsaw_registerzd2allocationzd2,
									(int) (1L), (int) (1L));
								PROCEDURE_SET(BgL_zc3z04anonymousza32005ze3z87_5887, (int) (0L),
									BgL_resetz00_2428);
								BGl_regsetzd2forzd2eachz00zzsaw_regsetz00
									(BgL_zc3z04anonymousza32005ze3z87_5887,
									((BgL_regsetz00_bglt) BgL_outz00_2426));
						}}
					{	/* SawMill/regalloc.scm 285 */
						bool_t BgL_test2986z00_7214;

						if (PAIRP(BgL_rdestz00_2430))
							{	/* SawMill/regalloc.scm 285 */
								obj_t BgL_arg2017z00_2472;

								BgL_arg2017z00_2472 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_oz00_2424)))->BgL_destz00);
								{	/* SawMill/regalloc.scm 285 */
									obj_t BgL_classz00_4204;

									BgL_classz00_4204 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
									if (BGL_OBJECTP(BgL_arg2017z00_2472))
										{	/* SawMill/regalloc.scm 285 */
											BgL_objectz00_bglt BgL_arg1807z00_4206;

											BgL_arg1807z00_4206 =
												(BgL_objectz00_bglt) (BgL_arg2017z00_2472);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawMill/regalloc.scm 285 */
													long BgL_idxz00_4212;

													BgL_idxz00_4212 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4206);
													BgL_test2986z00_7214 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_4212 + 2L)) == BgL_classz00_4204);
												}
											else
												{	/* SawMill/regalloc.scm 285 */
													bool_t BgL_res2735z00_4237;

													{	/* SawMill/regalloc.scm 285 */
														obj_t BgL_oclassz00_4220;

														{	/* SawMill/regalloc.scm 285 */
															obj_t BgL_arg1815z00_4228;
															long BgL_arg1816z00_4229;

															BgL_arg1815z00_4228 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawMill/regalloc.scm 285 */
																long BgL_arg1817z00_4230;

																BgL_arg1817z00_4230 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4206);
																BgL_arg1816z00_4229 =
																	(BgL_arg1817z00_4230 - OBJECT_TYPE);
															}
															BgL_oclassz00_4220 =
																VECTOR_REF(BgL_arg1815z00_4228,
																BgL_arg1816z00_4229);
														}
														{	/* SawMill/regalloc.scm 285 */
															bool_t BgL__ortest_1115z00_4221;

															BgL__ortest_1115z00_4221 =
																(BgL_classz00_4204 == BgL_oclassz00_4220);
															if (BgL__ortest_1115z00_4221)
																{	/* SawMill/regalloc.scm 285 */
																	BgL_res2735z00_4237 =
																		BgL__ortest_1115z00_4221;
																}
															else
																{	/* SawMill/regalloc.scm 285 */
																	long BgL_odepthz00_4222;

																	{	/* SawMill/regalloc.scm 285 */
																		obj_t BgL_arg1804z00_4223;

																		BgL_arg1804z00_4223 = (BgL_oclassz00_4220);
																		BgL_odepthz00_4222 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_4223);
																	}
																	if ((2L < BgL_odepthz00_4222))
																		{	/* SawMill/regalloc.scm 285 */
																			obj_t BgL_arg1802z00_4225;

																			{	/* SawMill/regalloc.scm 285 */
																				obj_t BgL_arg1803z00_4226;

																				BgL_arg1803z00_4226 =
																					(BgL_oclassz00_4220);
																				BgL_arg1802z00_4225 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_4226, 2L);
																			}
																			BgL_res2735z00_4237 =
																				(BgL_arg1802z00_4225 ==
																				BgL_classz00_4204);
																		}
																	else
																		{	/* SawMill/regalloc.scm 285 */
																			BgL_res2735z00_4237 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2986z00_7214 = BgL_res2735z00_4237;
												}
										}
									else
										{	/* SawMill/regalloc.scm 285 */
											BgL_test2986z00_7214 = ((bool_t) 0);
										}
								}
							}
						else
							{	/* SawMill/regalloc.scm 285 */
								BgL_test2986z00_7214 = ((bool_t) 0);
							}
						if (BgL_test2986z00_7214)
							{	/* SawMill/regalloc.scm 286 */
								obj_t BgL_g1571z00_2461;

								BgL_g1571z00_2461 =
									(((BgL_backendz00_bglt) COBJECT(BgL_backz00_6050))->
									BgL_registersz00);
								{
									obj_t BgL_l1569z00_2463;

									BgL_l1569z00_2463 = BgL_g1571z00_2461;
								BgL_zc3z04anonymousza32012ze3z87_2464:
									if (PAIRP(BgL_l1569z00_2463))
										{	/* SawMill/regalloc.scm 286 */
											{	/* SawMill/regalloc.scm 287 */
												obj_t BgL_rz00_2466;

												BgL_rz00_2466 = CAR(BgL_l1569z00_2463);
												if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
														(BgL_rz00_2466, BgL_rdestz00_2430)))
													{	/* SawMill/regalloc.scm 287 */
														((bool_t) 0);
													}
												else
													{	/* SawMill/regalloc.scm 288 */
														obj_t BgL_arg2015z00_2468;

														BgL_arg2015z00_2468 =
															(((BgL_rtl_insz00_bglt) COBJECT(
																	((BgL_rtl_insz00_bglt) BgL_oz00_2424)))->
															BgL_destz00);
														BGl_interferezd2regz12zc0zzsaw_registerzd2allocationzd2
															(BgL_arg2015z00_2468, BgL_rz00_2466);
													}
											}
											{
												obj_t BgL_l1569z00_7251;

												BgL_l1569z00_7251 = CDR(BgL_l1569z00_2463);
												BgL_l1569z00_2463 = BgL_l1569z00_7251;
												goto BgL_zc3z04anonymousza32012ze3z87_2464;
											}
										}
									else
										{	/* SawMill/regalloc.scm 286 */
											((bool_t) 1);
										}
								}
							}
						else
							{	/* SawMill/regalloc.scm 285 */
								((bool_t) 0);
							}
					}
					{	/* SawMill/regalloc.scm 291 */
						bool_t BgL_test2994z00_7253;

						if (PAIRP(BgL_a0z00_2431))
							{	/* SawMill/regalloc.scm 291 */
								if (NULLP(
										(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_oz00_2424)))->
											BgL_argsz00)))
									{	/* SawMill/regalloc.scm 291 */
										BgL_test2994z00_7253 = ((bool_t) 0);
									}
								else
									{	/* SawMill/regalloc.scm 291 */
										obj_t BgL_arg2033z00_2493;

										{	/* SawMill/regalloc.scm 291 */
											obj_t BgL_pairz00_4240;

											BgL_pairz00_4240 =
												(((BgL_rtl_insz00_bglt) COBJECT(
														((BgL_rtl_insz00_bglt) BgL_oz00_2424)))->
												BgL_argsz00);
											BgL_arg2033z00_2493 = CAR(BgL_pairz00_4240);
										}
										{	/* SawMill/regalloc.scm 291 */
											obj_t BgL_classz00_4241;

											BgL_classz00_4241 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
											if (BGL_OBJECTP(BgL_arg2033z00_2493))
												{	/* SawMill/regalloc.scm 291 */
													BgL_objectz00_bglt BgL_arg1807z00_4243;

													BgL_arg1807z00_4243 =
														(BgL_objectz00_bglt) (BgL_arg2033z00_2493);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* SawMill/regalloc.scm 291 */
															long BgL_idxz00_4249;

															BgL_idxz00_4249 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4243);
															BgL_test2994z00_7253 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_4249 + 2L)) == BgL_classz00_4241);
														}
													else
														{	/* SawMill/regalloc.scm 291 */
															bool_t BgL_res2736z00_4274;

															{	/* SawMill/regalloc.scm 291 */
																obj_t BgL_oclassz00_4257;

																{	/* SawMill/regalloc.scm 291 */
																	obj_t BgL_arg1815z00_4265;
																	long BgL_arg1816z00_4266;

																	BgL_arg1815z00_4265 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* SawMill/regalloc.scm 291 */
																		long BgL_arg1817z00_4267;

																		BgL_arg1817z00_4267 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4243);
																		BgL_arg1816z00_4266 =
																			(BgL_arg1817z00_4267 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_4257 =
																		VECTOR_REF(BgL_arg1815z00_4265,
																		BgL_arg1816z00_4266);
																}
																{	/* SawMill/regalloc.scm 291 */
																	bool_t BgL__ortest_1115z00_4258;

																	BgL__ortest_1115z00_4258 =
																		(BgL_classz00_4241 == BgL_oclassz00_4257);
																	if (BgL__ortest_1115z00_4258)
																		{	/* SawMill/regalloc.scm 291 */
																			BgL_res2736z00_4274 =
																				BgL__ortest_1115z00_4258;
																		}
																	else
																		{	/* SawMill/regalloc.scm 291 */
																			long BgL_odepthz00_4259;

																			{	/* SawMill/regalloc.scm 291 */
																				obj_t BgL_arg1804z00_4260;

																				BgL_arg1804z00_4260 =
																					(BgL_oclassz00_4257);
																				BgL_odepthz00_4259 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_4260);
																			}
																			if ((2L < BgL_odepthz00_4259))
																				{	/* SawMill/regalloc.scm 291 */
																					obj_t BgL_arg1802z00_4262;

																					{	/* SawMill/regalloc.scm 291 */
																						obj_t BgL_arg1803z00_4263;

																						BgL_arg1803z00_4263 =
																							(BgL_oclassz00_4257);
																						BgL_arg1802z00_4262 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_4263, 2L);
																					}
																					BgL_res2736z00_4274 =
																						(BgL_arg1802z00_4262 ==
																						BgL_classz00_4241);
																				}
																			else
																				{	/* SawMill/regalloc.scm 291 */
																					BgL_res2736z00_4274 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2994z00_7253 = BgL_res2736z00_4274;
														}
												}
											else
												{	/* SawMill/regalloc.scm 291 */
													BgL_test2994z00_7253 = ((bool_t) 0);
												}
										}
									}
							}
						else
							{	/* SawMill/regalloc.scm 291 */
								BgL_test2994z00_7253 = ((bool_t) 0);
							}
						if (BgL_test2994z00_7253)
							{	/* SawMill/regalloc.scm 292 */
								obj_t BgL_g1574z00_2480;

								BgL_g1574z00_2480 =
									(((BgL_backendz00_bglt) COBJECT(BgL_backz00_6050))->
									BgL_registersz00);
								{
									obj_t BgL_l1572z00_2482;

									BgL_l1572z00_2482 = BgL_g1574z00_2480;
								BgL_zc3z04anonymousza32026ze3z87_2483:
									if (PAIRP(BgL_l1572z00_2482))
										{	/* SawMill/regalloc.scm 292 */
											{	/* SawMill/regalloc.scm 293 */
												obj_t BgL_rz00_2485;

												BgL_rz00_2485 = CAR(BgL_l1572z00_2482);
												if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
														(BgL_rz00_2485, BgL_a0z00_2431)))
													{	/* SawMill/regalloc.scm 293 */
														((bool_t) 0);
													}
												else
													{	/* SawMill/regalloc.scm 294 */
														obj_t BgL_arg2029z00_2487;

														{	/* SawMill/regalloc.scm 294 */
															obj_t BgL_pairz00_4276;

															BgL_pairz00_4276 =
																(((BgL_rtl_insz00_bglt) COBJECT(
																		((BgL_rtl_insz00_bglt) BgL_oz00_2424)))->
																BgL_argsz00);
															BgL_arg2029z00_2487 = CAR(BgL_pairz00_4276);
														}
														BGl_interferezd2regz12zc0zzsaw_registerzd2allocationzd2
															(BgL_rz00_2485, BgL_arg2029z00_2487);
													}
											}
											{
												obj_t BgL_l1572z00_7296;

												BgL_l1572z00_7296 = CDR(BgL_l1572z00_2482);
												BgL_l1572z00_2482 = BgL_l1572z00_7296;
												goto BgL_zc3z04anonymousza32026ze3z87_2483;
											}
										}
									else
										{	/* SawMill/regalloc.scm 292 */
											((bool_t) 1);
										}
								}
							}
						else
							{	/* SawMill/regalloc.scm 291 */
								((bool_t) 0);
							}
					}
					{	/* SawMill/regalloc.scm 297 */
						bool_t BgL_test3003z00_7298;

						if (PAIRP(BgL_a1z00_2432))
							{	/* SawMill/regalloc.scm 297 */
								if (NULLP(
										(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_oz00_2424)))->
											BgL_argsz00)))
									{	/* SawMill/regalloc.scm 298 */
										BgL_test3003z00_7298 = ((bool_t) 0);
									}
								else
									{	/* SawMill/regalloc.scm 298 */
										bool_t BgL_test3006z00_7305;

										{	/* SawMill/regalloc.scm 298 */
											obj_t BgL_tmpz00_7306;

											{	/* SawMill/regalloc.scm 298 */
												obj_t BgL_pairz00_4278;

												BgL_pairz00_4278 =
													(((BgL_rtl_insz00_bglt) COBJECT(
															((BgL_rtl_insz00_bglt) BgL_oz00_2424)))->
													BgL_argsz00);
												BgL_tmpz00_7306 = CDR(BgL_pairz00_4278);
											}
											BgL_test3006z00_7305 = PAIRP(BgL_tmpz00_7306);
										}
										if (BgL_test3006z00_7305)
											{	/* SawMill/regalloc.scm 299 */
												obj_t BgL_arg2058z00_2522;

												{	/* SawMill/regalloc.scm 299 */
													obj_t BgL_pairz00_4279;

													BgL_pairz00_4279 =
														(((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt) BgL_oz00_2424)))->
														BgL_argsz00);
													BgL_arg2058z00_2522 = CAR(CDR(BgL_pairz00_4279));
												}
												{	/* SawMill/regalloc.scm 299 */
													obj_t BgL_classz00_4283;

													BgL_classz00_4283 =
														BGl_rtl_regzf2razf2zzsaw_regsetz00;
													if (BGL_OBJECTP(BgL_arg2058z00_2522))
														{	/* SawMill/regalloc.scm 299 */
															BgL_objectz00_bglt BgL_arg1807z00_4285;

															BgL_arg1807z00_4285 =
																(BgL_objectz00_bglt) (BgL_arg2058z00_2522);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* SawMill/regalloc.scm 299 */
																	long BgL_idxz00_4291;

																	BgL_idxz00_4291 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_4285);
																	BgL_test3003z00_7298 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_4291 + 2L)) ==
																		BgL_classz00_4283);
																}
															else
																{	/* SawMill/regalloc.scm 299 */
																	bool_t BgL_res2737z00_4316;

																	{	/* SawMill/regalloc.scm 299 */
																		obj_t BgL_oclassz00_4299;

																		{	/* SawMill/regalloc.scm 299 */
																			obj_t BgL_arg1815z00_4307;
																			long BgL_arg1816z00_4308;

																			BgL_arg1815z00_4307 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* SawMill/regalloc.scm 299 */
																				long BgL_arg1817z00_4309;

																				BgL_arg1817z00_4309 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_4285);
																				BgL_arg1816z00_4308 =
																					(BgL_arg1817z00_4309 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_4299 =
																				VECTOR_REF(BgL_arg1815z00_4307,
																				BgL_arg1816z00_4308);
																		}
																		{	/* SawMill/regalloc.scm 299 */
																			bool_t BgL__ortest_1115z00_4300;

																			BgL__ortest_1115z00_4300 =
																				(BgL_classz00_4283 ==
																				BgL_oclassz00_4299);
																			if (BgL__ortest_1115z00_4300)
																				{	/* SawMill/regalloc.scm 299 */
																					BgL_res2737z00_4316 =
																						BgL__ortest_1115z00_4300;
																				}
																			else
																				{	/* SawMill/regalloc.scm 299 */
																					long BgL_odepthz00_4301;

																					{	/* SawMill/regalloc.scm 299 */
																						obj_t BgL_arg1804z00_4302;

																						BgL_arg1804z00_4302 =
																							(BgL_oclassz00_4299);
																						BgL_odepthz00_4301 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_4302);
																					}
																					if ((2L < BgL_odepthz00_4301))
																						{	/* SawMill/regalloc.scm 299 */
																							obj_t BgL_arg1802z00_4304;

																							{	/* SawMill/regalloc.scm 299 */
																								obj_t BgL_arg1803z00_4305;

																								BgL_arg1803z00_4305 =
																									(BgL_oclassz00_4299);
																								BgL_arg1802z00_4304 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_4305, 2L);
																							}
																							BgL_res2737z00_4316 =
																								(BgL_arg1802z00_4304 ==
																								BgL_classz00_4283);
																						}
																					else
																						{	/* SawMill/regalloc.scm 299 */
																							BgL_res2737z00_4316 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test3003z00_7298 = BgL_res2737z00_4316;
																}
														}
													else
														{	/* SawMill/regalloc.scm 299 */
															BgL_test3003z00_7298 = ((bool_t) 0);
														}
												}
											}
										else
											{	/* SawMill/regalloc.scm 298 */
												BgL_test3003z00_7298 = ((bool_t) 0);
											}
									}
							}
						else
							{	/* SawMill/regalloc.scm 297 */
								BgL_test3003z00_7298 = ((bool_t) 0);
							}
						if (BgL_test3003z00_7298)
							{	/* SawMill/regalloc.scm 300 */
								obj_t BgL_g1577z00_2508;

								BgL_g1577z00_2508 =
									(((BgL_backendz00_bglt) COBJECT(BgL_backz00_6050))->
									BgL_registersz00);
								{
									obj_t BgL_l1575z00_2510;

									BgL_l1575z00_2510 = BgL_g1577z00_2508;
								BgL_zc3z04anonymousza32050ze3z87_2511:
									if (PAIRP(BgL_l1575z00_2510))
										{	/* SawMill/regalloc.scm 300 */
											{	/* SawMill/regalloc.scm 301 */
												obj_t BgL_rz00_2513;

												BgL_rz00_2513 = CAR(BgL_l1575z00_2510);
												if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
														(BgL_rz00_2513, BgL_a1z00_2432)))
													{	/* SawMill/regalloc.scm 301 */
														((bool_t) 0);
													}
												else
													{	/* SawMill/regalloc.scm 302 */
														obj_t BgL_arg2055z00_2515;

														{	/* SawMill/regalloc.scm 302 */
															obj_t BgL_pairz00_4318;

															BgL_pairz00_4318 =
																(((BgL_rtl_insz00_bglt) COBJECT(
																		((BgL_rtl_insz00_bglt) BgL_oz00_2424)))->
																BgL_argsz00);
															BgL_arg2055z00_2515 = CAR(CDR(BgL_pairz00_4318));
														}
														BGl_interferezd2regz12zc0zzsaw_registerzd2allocationzd2
															(BgL_rz00_2513, BgL_arg2055z00_2515);
													}
											}
											{
												obj_t BgL_l1575z00_7349;

												BgL_l1575z00_7349 = CDR(BgL_l1575z00_2510);
												BgL_l1575z00_2510 = BgL_l1575z00_7349;
												goto BgL_zc3z04anonymousza32050ze3z87_2511;
											}
										}
									else
										{	/* SawMill/regalloc.scm 300 */
											((bool_t) 1);
										}
								}
							}
						else
							{	/* SawMill/regalloc.scm 297 */
								((bool_t) 0);
							}
					}
					{	/* SawMill/regalloc.scm 305 */
						obj_t BgL_g1580z00_2527;

						BgL_g1580z00_2527 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_oz00_2424)))->BgL_argsz00);
						{
							obj_t BgL_l1578z00_2529;

							BgL_l1578z00_2529 = BgL_g1580z00_2527;
						BgL_zc3z04anonymousza32063ze3z87_2530:
							if (PAIRP(BgL_l1578z00_2529))
								{	/* SawMill/regalloc.scm 305 */
									{	/* SawMill/regalloc.scm 306 */
										obj_t BgL_az00_2532;

										BgL_az00_2532 = CAR(BgL_l1578z00_2529);
										{	/* SawMill/regalloc.scm 306 */
											bool_t BgL_test3014z00_7356;

											{	/* SawMill/regalloc.scm 306 */
												obj_t BgL_classz00_4324;

												BgL_classz00_4324 = BGl_rtl_insz00zzsaw_defsz00;
												if (BGL_OBJECTP(BgL_az00_2532))
													{	/* SawMill/regalloc.scm 306 */
														BgL_objectz00_bglt BgL_arg1807z00_4326;

														BgL_arg1807z00_4326 =
															(BgL_objectz00_bglt) (BgL_az00_2532);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawMill/regalloc.scm 306 */
																long BgL_idxz00_4332;

																BgL_idxz00_4332 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_4326);
																BgL_test3014z00_7356 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_4332 + 1L)) ==
																	BgL_classz00_4324);
															}
														else
															{	/* SawMill/regalloc.scm 306 */
																bool_t BgL_res2738z00_4357;

																{	/* SawMill/regalloc.scm 306 */
																	obj_t BgL_oclassz00_4340;

																	{	/* SawMill/regalloc.scm 306 */
																		obj_t BgL_arg1815z00_4348;
																		long BgL_arg1816z00_4349;

																		BgL_arg1815z00_4348 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawMill/regalloc.scm 306 */
																			long BgL_arg1817z00_4350;

																			BgL_arg1817z00_4350 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_4326);
																			BgL_arg1816z00_4349 =
																				(BgL_arg1817z00_4350 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_4340 =
																			VECTOR_REF(BgL_arg1815z00_4348,
																			BgL_arg1816z00_4349);
																	}
																	{	/* SawMill/regalloc.scm 306 */
																		bool_t BgL__ortest_1115z00_4341;

																		BgL__ortest_1115z00_4341 =
																			(BgL_classz00_4324 == BgL_oclassz00_4340);
																		if (BgL__ortest_1115z00_4341)
																			{	/* SawMill/regalloc.scm 306 */
																				BgL_res2738z00_4357 =
																					BgL__ortest_1115z00_4341;
																			}
																		else
																			{	/* SawMill/regalloc.scm 306 */
																				long BgL_odepthz00_4342;

																				{	/* SawMill/regalloc.scm 306 */
																					obj_t BgL_arg1804z00_4343;

																					BgL_arg1804z00_4343 =
																						(BgL_oclassz00_4340);
																					BgL_odepthz00_4342 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_4343);
																				}
																				if ((1L < BgL_odepthz00_4342))
																					{	/* SawMill/regalloc.scm 306 */
																						obj_t BgL_arg1802z00_4345;

																						{	/* SawMill/regalloc.scm 306 */
																							obj_t BgL_arg1803z00_4346;

																							BgL_arg1803z00_4346 =
																								(BgL_oclassz00_4340);
																							BgL_arg1802z00_4345 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_4346, 1L);
																						}
																						BgL_res2738z00_4357 =
																							(BgL_arg1802z00_4345 ==
																							BgL_classz00_4324);
																					}
																				else
																					{	/* SawMill/regalloc.scm 306 */
																						BgL_res2738z00_4357 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test3014z00_7356 = BgL_res2738z00_4357;
															}
													}
												else
													{	/* SawMill/regalloc.scm 306 */
														BgL_test3014z00_7356 = ((bool_t) 0);
													}
											}
											if (BgL_test3014z00_7356)
												{	/* SawMill/regalloc.scm 306 */
													BGl_inszd2hardwarezd2interferencez12ze70zf5zzsaw_registerzd2allocationzd2
														(BgL_backz00_6050,
														((BgL_rtl_insz00_bglt) BgL_az00_2532),
														BgL_inz00_2425, BgL_outz00_2426);
												}
											else
												{	/* SawMill/regalloc.scm 306 */
													((bool_t) 0);
												}
										}
									}
									{
										obj_t BgL_l1578z00_7381;

										BgL_l1578z00_7381 = CDR(BgL_l1578z00_2529);
										BgL_l1578z00_2529 = BgL_l1578z00_7381;
										goto BgL_zc3z04anonymousza32063ze3z87_2530;
									}
								}
							else
								{	/* SawMill/regalloc.scm 305 */
									((bool_t) 1);
								}
						}
					}
					{
						obj_t BgL_l1582z00_2537;

						BgL_l1582z00_2537 = BgL_spillz00_2429;
					BgL_zc3z04anonymousza32068ze3z87_2538:
						if (PAIRP(BgL_l1582z00_2537))
							{	/* SawMill/regalloc.scm 309 */
								{	/* SawMill/regalloc.scm 310 */
									obj_t BgL_rz00_2540;

									BgL_rz00_2540 = CAR(BgL_l1582z00_2537);
									{	/* SawMill/regalloc.scm 310 */
										obj_t BgL_arg2070z00_2541;

										{
											BgL_rtl_inszf2razf2_bglt BgL_auxz00_7386;

											{
												obj_t BgL_auxz00_7387;

												{	/* SawMill/regalloc.scm 310 */
													BgL_objectz00_bglt BgL_tmpz00_7388;

													BgL_tmpz00_7388 =
														((BgL_objectz00_bglt) BgL_oz00_2424);
													BgL_auxz00_7387 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_7388);
												}
												BgL_auxz00_7386 =
													((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_7387);
											}
											BgL_arg2070z00_2541 =
												(((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_7386))->
												BgL_spillz00);
										}
										BGl_regsetzd2addz12zc0zzsaw_regsetz00(
											((BgL_regsetz00_bglt) BgL_arg2070z00_2541),
											((BgL_rtl_regz00_bglt) BgL_rz00_2540));
									}
								}
								{
									obj_t BgL_l1582z00_7396;

									BgL_l1582z00_7396 = CDR(BgL_l1582z00_2537);
									BgL_l1582z00_2537 = BgL_l1582z00_7396;
									goto BgL_zc3z04anonymousza32068ze3z87_2538;
								}
							}
						else
							{	/* SawMill/regalloc.scm 309 */
								return ((bool_t) 1);
							}
					}
				}
			}
		}

	}



/* &<@anonymous:2005> */
	obj_t BGl_z62zc3z04anonymousza32005ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5889, obj_t BgL_r1z00_5891)
	{
		{	/* SawMill/regalloc.scm 278 */
			{	/* SawMill/regalloc.scm 279 */
				obj_t BgL_resetz00_5890;

				BgL_resetz00_5890 =
					((obj_t) PROCEDURE_REF(BgL_envz00_5889, (int) (0L)));
				{	/* SawMill/regalloc.scm 279 */
					bool_t BgL_tmpz00_7401;

					{
						obj_t BgL_l1567z00_6080;

						BgL_l1567z00_6080 = BgL_resetz00_5890;
					BgL_zc3z04anonymousza32006ze3z87_6079:
						if (PAIRP(BgL_l1567z00_6080))
							{	/* SawMill/regalloc.scm 279 */
								BGl_interferezd2regz12zc0zzsaw_registerzd2allocationzd2
									(BgL_r1z00_5891, CAR(BgL_l1567z00_6080));
								{
									obj_t BgL_l1567z00_7406;

									BgL_l1567z00_7406 = CDR(BgL_l1567z00_6080);
									BgL_l1567z00_6080 = BgL_l1567z00_7406;
									goto BgL_zc3z04anonymousza32006ze3z87_6079;
								}
							}
						else
							{	/* SawMill/regalloc.scm 279 */
								BgL_tmpz00_7401 = ((bool_t) 1);
							}
					}
					return BBOOL(BgL_tmpz00_7401);
				}
			}
		}

	}



/* &<@anonymous:2000> */
	obj_t BGl_z62zc3z04anonymousza32000ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5892, obj_t BgL_r1z00_5894)
	{
		{	/* SawMill/regalloc.scm 274 */
			{	/* SawMill/regalloc.scm 275 */
				obj_t BgL_resetz00_5893;

				BgL_resetz00_5893 =
					((obj_t) PROCEDURE_REF(BgL_envz00_5892, (int) (0L)));
				{	/* SawMill/regalloc.scm 275 */
					bool_t BgL_tmpz00_7412;

					{
						obj_t BgL_l1565z00_6082;

						BgL_l1565z00_6082 = BgL_resetz00_5893;
					BgL_zc3z04anonymousza32001ze3z87_6081:
						if (PAIRP(BgL_l1565z00_6082))
							{	/* SawMill/regalloc.scm 275 */
								BGl_interferezd2regz12zc0zzsaw_registerzd2allocationzd2
									(BgL_r1z00_5894, CAR(BgL_l1565z00_6082));
								{
									obj_t BgL_l1565z00_7417;

									BgL_l1565z00_7417 = CDR(BgL_l1565z00_6082);
									BgL_l1565z00_6082 = BgL_l1565z00_7417;
									goto BgL_zc3z04anonymousza32001ze3z87_6081;
								}
							}
						else
							{	/* SawMill/regalloc.scm 275 */
								BgL_tmpz00_7412 = ((bool_t) 1);
							}
					}
					return BBOOL(BgL_tmpz00_7412);
				}
			}
		}

	}



/* interference! */
	obj_t BGl_interferencez12z12zzsaw_registerzd2allocationzd2(obj_t
		BgL_blocksz00_83, obj_t BgL_regsz00_84)
	{
		{	/* SawMill/regalloc.scm 350 */
			{	/* SawMill/regalloc.scm 351 */
				obj_t BgL_list2079z00_2559;

				BgL_list2079z00_2559 =
					MAKE_YOUNG_PAIR(BGl_string2794z00zzsaw_registerzd2allocationzd2,
					BNIL);
				BGl_verbosez00zztools_speekz00(BINT(3L), BgL_list2079z00_2559);
			}
			{
				obj_t BgL_l1596z00_2561;

				BgL_l1596z00_2561 = BgL_blocksz00_83;
			BgL_zc3z04anonymousza32080ze3z87_2562:
				if (PAIRP(BgL_l1596z00_2561))
					{	/* SawMill/regalloc.scm 353 */
						{	/* SawMill/regalloc.scm 354 */
							obj_t BgL_bz00_2564;

							BgL_bz00_2564 = CAR(BgL_l1596z00_2561);
							{	/* SawMill/regalloc.scm 354 */
								obj_t BgL_g1595z00_2565;

								BgL_g1595z00_2565 =
									(((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt) BgL_bz00_2564)))->BgL_firstz00);
								{
									obj_t BgL_l1593z00_2567;

									BgL_l1593z00_2567 = BgL_g1595z00_2565;
								BgL_zc3z04anonymousza32082ze3z87_2568:
									if (PAIRP(BgL_l1593z00_2567))
										{	/* SawMill/regalloc.scm 354 */
											{	/* SawMill/regalloc.scm 354 */
												obj_t BgL_iz00_2570;

												BgL_iz00_2570 = CAR(BgL_l1593z00_2567);
												BGl_interferezd2insz12zc0zzsaw_registerzd2allocationzd2(
													((BgL_rtl_insz00_bglt) BgL_iz00_2570));
											}
											{
												obj_t BgL_l1593z00_7433;

												BgL_l1593z00_7433 = CDR(BgL_l1593z00_2567);
												BgL_l1593z00_2567 = BgL_l1593z00_7433;
												goto BgL_zc3z04anonymousza32082ze3z87_2568;
											}
										}
									else
										{	/* SawMill/regalloc.scm 354 */
											((bool_t) 1);
										}
								}
							}
						}
						{
							obj_t BgL_l1596z00_7435;

							BgL_l1596z00_7435 = CDR(BgL_l1596z00_2561);
							BgL_l1596z00_2561 = BgL_l1596z00_7435;
							goto BgL_zc3z04anonymousza32080ze3z87_2562;
						}
					}
				else
					{	/* SawMill/regalloc.scm 353 */
						((bool_t) 1);
					}
			}
			{	/* SawMill/regalloc.scm 356 */
				obj_t BgL_list2087z00_2575;

				BgL_list2087z00_2575 =
					MAKE_YOUNG_PAIR(BGl_string2795z00zzsaw_registerzd2allocationzd2,
					BNIL);
				return BGl_verbosez00zztools_speekz00(BINT(3L), BgL_list2087z00_2575);
			}
		}

	}



/* interfere-ins! */
	obj_t
		BGl_interferezd2insz12zc0zzsaw_registerzd2allocationzd2(BgL_rtl_insz00_bglt
		BgL_iz00_85)
	{
		{	/* SawMill/regalloc.scm 361 */
			{	/* SawMill/regalloc.scm 364 */
				obj_t BgL_arg2089z00_2578;

				{
					BgL_rtl_inszf2razf2_bglt BgL_auxz00_7440;

					{
						obj_t BgL_auxz00_7441;

						{	/* SawMill/regalloc.scm 363 */
							BgL_objectz00_bglt BgL_tmpz00_7442;

							BgL_tmpz00_7442 = ((BgL_objectz00_bglt) BgL_iz00_85);
							BgL_auxz00_7441 = BGL_OBJECT_WIDENING(BgL_tmpz00_7442);
						}
						BgL_auxz00_7440 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_7441);
					}
					BgL_arg2089z00_2578 =
						(((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_7440))->BgL_inz00);
				}
				{	/* SawMill/regalloc.scm 366 */
					obj_t BgL_zc3z04anonymousza32090ze3z87_5898;

					BgL_zc3z04anonymousza32090ze3z87_5898 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza32090ze3ze5zzsaw_registerzd2allocationzd2,
						(int) (1L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32090ze3z87_5898, (int) (0L),
						((obj_t) BgL_iz00_85));
					BGl_regsetzd2forzd2eachz00zzsaw_regsetz00
						(BgL_zc3z04anonymousza32090ze3z87_5898,
						((BgL_regsetz00_bglt) BgL_arg2089z00_2578));
			}}
			{	/* SawMill/regalloc.scm 371 */
				obj_t BgL_arg2096z00_2588;

				{
					BgL_rtl_inszf2razf2_bglt BgL_auxz00_7455;

					{
						obj_t BgL_auxz00_7456;

						{	/* SawMill/regalloc.scm 370 */
							BgL_objectz00_bglt BgL_tmpz00_7457;

							BgL_tmpz00_7457 = ((BgL_objectz00_bglt) BgL_iz00_85);
							BgL_auxz00_7456 = BGL_OBJECT_WIDENING(BgL_tmpz00_7457);
						}
						BgL_auxz00_7455 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_7456);
					}
					BgL_arg2096z00_2588 =
						(((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_7455))->BgL_outz00);
				}
				{	/* SawMill/regalloc.scm 373 */
					obj_t BgL_zc3z04anonymousza32097ze3z87_5896;

					BgL_zc3z04anonymousza32097ze3z87_5896 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza32097ze3ze5zzsaw_registerzd2allocationzd2,
						(int) (1L), (int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32097ze3z87_5896, (int) (0L),
						((obj_t) BgL_iz00_85));
					return
						BGl_regsetzd2forzd2eachz00zzsaw_regsetz00
						(BgL_zc3z04anonymousza32097ze3z87_5896,
						((BgL_regsetz00_bglt) BgL_arg2096z00_2588));
				}
			}
		}

	}



/* &<@anonymous:2097> */
	obj_t BGl_z62zc3z04anonymousza32097ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5899, obj_t BgL_r1z00_5901)
	{
		{	/* SawMill/regalloc.scm 371 */
			{	/* SawMill/regalloc.scm 373 */
				BgL_rtl_insz00_bglt BgL_i1220z00_5900;

				BgL_i1220z00_5900 =
					((BgL_rtl_insz00_bglt) PROCEDURE_REF(BgL_envz00_5899, (int) (0L)));
				{	/* SawMill/regalloc.scm 373 */
					obj_t BgL_arg2099z00_6083;

					{
						BgL_rtl_inszf2razf2_bglt BgL_auxz00_7473;

						{
							obj_t BgL_auxz00_7474;

							{	/* SawMill/regalloc.scm 372 */
								BgL_objectz00_bglt BgL_tmpz00_7475;

								BgL_tmpz00_7475 = ((BgL_objectz00_bglt) BgL_i1220z00_5900);
								BgL_auxz00_7474 = BGL_OBJECT_WIDENING(BgL_tmpz00_7475);
							}
							BgL_auxz00_7473 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_7474);
						}
						BgL_arg2099z00_6083 =
							(((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_7473))->
							BgL_outz00);
					}
					{	/* SawMill/regalloc.scm 374 */
						obj_t BgL_zc3z04anonymousza32100ze3z87_6084;

						BgL_zc3z04anonymousza32100ze3z87_6084 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza32100ze3ze5zzsaw_registerzd2allocationzd2,
							(int) (1L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza32100ze3z87_6084, (int) (0L),
							BgL_r1z00_5901);
						return
							BGl_regsetzd2forzd2eachz00zzsaw_regsetz00
							(BgL_zc3z04anonymousza32100ze3z87_6084,
							((BgL_regsetz00_bglt) BgL_arg2099z00_6083));
					}
				}
			}
		}

	}



/* &<@anonymous:2100> */
	obj_t BGl_z62zc3z04anonymousza32100ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5902, obj_t BgL_r2z00_5904)
	{
		{	/* SawMill/regalloc.scm 373 */
			{	/* SawMill/regalloc.scm 374 */
				obj_t BgL_r1z00_5903;

				BgL_r1z00_5903 = PROCEDURE_REF(BgL_envz00_5902, (int) (0L));
				return
					BBOOL(BGl_interferezd2regz12zc0zzsaw_registerzd2allocationzd2
					(BgL_r1z00_5903, BgL_r2z00_5904));
			}
		}

	}



/* &<@anonymous:2090> */
	obj_t BGl_z62zc3z04anonymousza32090ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5905, obj_t BgL_r1z00_5907)
	{
		{	/* SawMill/regalloc.scm 364 */
			{	/* SawMill/regalloc.scm 366 */
				BgL_rtl_insz00_bglt BgL_i1220z00_5906;

				BgL_i1220z00_5906 =
					((BgL_rtl_insz00_bglt) PROCEDURE_REF(BgL_envz00_5905, (int) (0L)));
				{	/* SawMill/regalloc.scm 366 */
					obj_t BgL_arg2093z00_6085;

					{
						BgL_rtl_inszf2razf2_bglt BgL_auxz00_7494;

						{
							obj_t BgL_auxz00_7495;

							{	/* SawMill/regalloc.scm 365 */
								BgL_objectz00_bglt BgL_tmpz00_7496;

								BgL_tmpz00_7496 = ((BgL_objectz00_bglt) BgL_i1220z00_5906);
								BgL_auxz00_7495 = BGL_OBJECT_WIDENING(BgL_tmpz00_7496);
							}
							BgL_auxz00_7494 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_7495);
						}
						BgL_arg2093z00_6085 =
							(((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_7494))->
							BgL_inz00);
					}
					{	/* SawMill/regalloc.scm 367 */
						obj_t BgL_zc3z04anonymousza32094ze3z87_6086;

						BgL_zc3z04anonymousza32094ze3z87_6086 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza32094ze3ze5zzsaw_registerzd2allocationzd2,
							(int) (1L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza32094ze3z87_6086, (int) (0L),
							BgL_r1z00_5907);
						return
							BGl_regsetzd2forzd2eachz00zzsaw_regsetz00
							(BgL_zc3z04anonymousza32094ze3z87_6086,
							((BgL_regsetz00_bglt) BgL_arg2093z00_6085));
					}
				}
			}
		}

	}



/* &<@anonymous:2094> */
	obj_t BGl_z62zc3z04anonymousza32094ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5908, obj_t BgL_r2z00_5910)
	{
		{	/* SawMill/regalloc.scm 366 */
			{	/* SawMill/regalloc.scm 367 */
				obj_t BgL_r1z00_5909;

				BgL_r1z00_5909 = PROCEDURE_REF(BgL_envz00_5908, (int) (0L));
				return
					BBOOL(BGl_interferezd2regz12zc0zzsaw_registerzd2allocationzd2
					(BgL_r1z00_5909, BgL_r2z00_5910));
			}
		}

	}



/* interfere-reg! */
	bool_t BGl_interferezd2regz12zc0zzsaw_registerzd2allocationzd2(obj_t
		BgL_r1z00_86, obj_t BgL_r2z00_87)
	{
		{	/* SawMill/regalloc.scm 381 */
			{	/* SawMill/regalloc.scm 382 */
				bool_t BgL_test3024z00_7512;

				if ((BgL_r1z00_86 == BgL_r2z00_87))
					{	/* SawMill/regalloc.scm 382 */
						BgL_test3024z00_7512 = ((bool_t) 1);
					}
				else
					{	/* SawMill/regalloc.scm 382 */
						obj_t BgL_arg2105z00_2601;

						{
							BgL_rtl_regzf2razf2_bglt BgL_auxz00_7515;

							{
								obj_t BgL_auxz00_7516;

								{	/* SawMill/regalloc.scm 382 */
									BgL_objectz00_bglt BgL_tmpz00_7517;

									BgL_tmpz00_7517 =
										((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_r1z00_86));
									BgL_auxz00_7516 = BGL_OBJECT_WIDENING(BgL_tmpz00_7517);
								}
								BgL_auxz00_7515 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_7516);
							}
							BgL_arg2105z00_2601 =
								(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_7515))->
								BgL_interferez00);
						}
						{	/* SawMill/regalloc.scm 382 */
							long BgL_basez00_4384;
							long BgL_bitz00_4385;

							{	/* SawMill/regalloc.scm 382 */
								int BgL_arg2607z00_4386;

								{
									BgL_rtl_regzf2razf2_bglt BgL_auxz00_7523;

									{
										obj_t BgL_auxz00_7524;

										{	/* SawMill/regalloc.scm 382 */
											BgL_objectz00_bglt BgL_tmpz00_7525;

											BgL_tmpz00_7525 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_r2z00_87));
											BgL_auxz00_7524 = BGL_OBJECT_WIDENING(BgL_tmpz00_7525);
										}
										BgL_auxz00_7523 =
											((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_7524);
									}
									BgL_arg2607z00_4386 =
										(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_7523))->
										BgL_numz00);
								}
								BgL_basez00_4384 = ((long) (BgL_arg2607z00_4386) / 8L);
							}
							{	/* SawMill/regalloc.scm 382 */
								int BgL_arg2608z00_4387;

								{
									BgL_rtl_regzf2razf2_bglt BgL_auxz00_7533;

									{
										obj_t BgL_auxz00_7534;

										{	/* SawMill/regalloc.scm 382 */
											BgL_objectz00_bglt BgL_tmpz00_7535;

											BgL_tmpz00_7535 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_r2z00_87));
											BgL_auxz00_7534 = BGL_OBJECT_WIDENING(BgL_tmpz00_7535);
										}
										BgL_auxz00_7533 =
											((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_7534);
									}
									BgL_arg2608z00_4387 =
										(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_7533))->
										BgL_numz00);
								}
								{	/* SawMill/regalloc.scm 382 */
									long BgL_n1z00_4397;
									long BgL_n2z00_4398;

									BgL_n1z00_4397 = (long) (BgL_arg2608z00_4387);
									BgL_n2z00_4398 = 8L;
									{	/* SawMill/regalloc.scm 382 */
										bool_t BgL_test3026z00_7542;

										{	/* SawMill/regalloc.scm 382 */
											long BgL_arg1338z00_4400;

											BgL_arg1338z00_4400 =
												(((BgL_n1z00_4397) | (BgL_n2z00_4398)) & -2147483648);
											BgL_test3026z00_7542 = (BgL_arg1338z00_4400 == 0L);
										}
										if (BgL_test3026z00_7542)
											{	/* SawMill/regalloc.scm 382 */
												int32_t BgL_arg1334z00_4401;

												{	/* SawMill/regalloc.scm 382 */
													int32_t BgL_arg1336z00_4402;
													int32_t BgL_arg1337z00_4403;

													BgL_arg1336z00_4402 = (int32_t) (BgL_n1z00_4397);
													BgL_arg1337z00_4403 = (int32_t) (BgL_n2z00_4398);
													BgL_arg1334z00_4401 =
														(BgL_arg1336z00_4402 % BgL_arg1337z00_4403);
												}
												{	/* SawMill/regalloc.scm 382 */
													long BgL_arg1446z00_4408;

													BgL_arg1446z00_4408 = (long) (BgL_arg1334z00_4401);
													BgL_bitz00_4385 = (long) (BgL_arg1446z00_4408);
											}}
										else
											{	/* SawMill/regalloc.scm 382 */
												BgL_bitz00_4385 = (BgL_n1z00_4397 % BgL_n2z00_4398);
											}
									}
								}
							}
							if (
								(BgL_basez00_4384 <
									STRING_LENGTH(
										(((BgL_regsetz00_bglt) COBJECT(
													((BgL_regsetz00_bglt) BgL_arg2105z00_2601)))->
											BgL_stringz00))))
								{	/* SawMill/regalloc.scm 382 */
									BgL_test3024z00_7512 =
										(
										((STRING_REF(
													(((BgL_regsetz00_bglt) COBJECT(
																((BgL_regsetz00_bglt) BgL_arg2105z00_2601)))->
														BgL_stringz00),
													BgL_basez00_4384)) & (1L << (int) (BgL_bitz00_4385)))
										> 0L);
								}
							else
								{	/* SawMill/regalloc.scm 382 */
									BgL_test3024z00_7512 = ((bool_t) 0);
								}
						}
					}
				if (BgL_test3024z00_7512)
					{	/* SawMill/regalloc.scm 382 */
						return ((bool_t) 0);
					}
				else
					{	/* SawMill/regalloc.scm 382 */
						{	/* SawMill/regalloc.scm 383 */
							obj_t BgL_arg2103z00_2599;

							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_7564;

								{
									obj_t BgL_auxz00_7565;

									{	/* SawMill/regalloc.scm 383 */
										BgL_objectz00_bglt BgL_tmpz00_7566;

										BgL_tmpz00_7566 =
											((BgL_objectz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_r1z00_86));
										BgL_auxz00_7565 = BGL_OBJECT_WIDENING(BgL_tmpz00_7566);
									}
									BgL_auxz00_7564 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_7565);
								}
								BgL_arg2103z00_2599 =
									(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_7564))->
									BgL_interferez00);
							}
							BGl_regsetzd2addz12zc0zzsaw_regsetz00(
								((BgL_regsetz00_bglt) BgL_arg2103z00_2599),
								((BgL_rtl_regz00_bglt) BgL_r2z00_87));
						}
						{	/* SawMill/regalloc.scm 384 */
							obj_t BgL_arg2104z00_2600;

							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_7575;

								{
									obj_t BgL_auxz00_7576;

									{	/* SawMill/regalloc.scm 384 */
										BgL_objectz00_bglt BgL_tmpz00_7577;

										BgL_tmpz00_7577 =
											((BgL_objectz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_r2z00_87));
										BgL_auxz00_7576 = BGL_OBJECT_WIDENING(BgL_tmpz00_7577);
									}
									BgL_auxz00_7575 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_7576);
								}
								BgL_arg2104z00_2600 =
									(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_7575))->
									BgL_interferez00);
							}
							return
								BGl_regsetzd2addz12zc0zzsaw_regsetz00(
								((BgL_regsetz00_bglt) BgL_arg2104z00_2600),
								((BgL_rtl_regz00_bglt) BgL_r1z00_86));
						}
					}
			}
		}

	}



/* cleanup-dest! */
	obj_t BGl_cleanupzd2destz12zc0zzsaw_registerzd2allocationzd2(obj_t
		BgL_blocksz00_88)
	{
		{	/* SawMill/regalloc.scm 389 */
			{	/* SawMill/regalloc.scm 390 */
				obj_t BgL_list2106z00_2602;

				BgL_list2106z00_2602 =
					MAKE_YOUNG_PAIR(BGl_string2796z00zzsaw_registerzd2allocationzd2,
					BNIL);
				BGl_verbosez00zztools_speekz00(BINT(3L), BgL_list2106z00_2602);
			}
			{	/* SawMill/regalloc.scm 391 */
				long BgL_nz00_2603;

				BgL_nz00_2603 = 0L;
				{
					obj_t BgL_l1601z00_2605;

					BgL_l1601z00_2605 = BgL_blocksz00_88;
				BgL_zc3z04anonymousza32107ze3z87_2606:
					if (PAIRP(BgL_l1601z00_2605))
						{	/* SawMill/regalloc.scm 392 */
							{	/* SawMill/regalloc.scm 393 */
								obj_t BgL_bz00_2608;

								BgL_bz00_2608 = CAR(BgL_l1601z00_2605);
								{	/* SawMill/regalloc.scm 393 */
									obj_t BgL_g1600z00_2609;

									BgL_g1600z00_2609 =
										(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt) BgL_bz00_2608)))->BgL_firstz00);
									{
										obj_t BgL_l1598z00_2611;

										BgL_l1598z00_2611 = BgL_g1600z00_2609;
									BgL_zc3z04anonymousza32109ze3z87_2612:
										if (PAIRP(BgL_l1598z00_2611))
											{	/* SawMill/regalloc.scm 399 */
												{	/* SawMill/regalloc.scm 394 */
													obj_t BgL_iz00_2614;

													BgL_iz00_2614 = CAR(BgL_l1598z00_2611);
													{	/* SawMill/regalloc.scm 395 */
														bool_t BgL_test3030z00_7597;

														{	/* SawMill/regalloc.scm 395 */
															obj_t BgL_arg2118z00_2623;

															BgL_arg2118z00_2623 =
																(((BgL_rtl_insz00_bglt) COBJECT(
																		((BgL_rtl_insz00_bglt)
																			((BgL_rtl_insz00_bglt) BgL_iz00_2614))))->
																BgL_destz00);
															{	/* SawMill/regalloc.scm 395 */
																obj_t BgL_classz00_4431;

																BgL_classz00_4431 =
																	BGl_rtl_regzf2razf2zzsaw_regsetz00;
																if (BGL_OBJECTP(BgL_arg2118z00_2623))
																	{	/* SawMill/regalloc.scm 395 */
																		BgL_objectz00_bglt BgL_arg1807z00_4433;

																		BgL_arg1807z00_4433 =
																			(BgL_objectz00_bglt)
																			(BgL_arg2118z00_2623);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* SawMill/regalloc.scm 395 */
																				long BgL_idxz00_4439;

																				BgL_idxz00_4439 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_4433);
																				BgL_test3030z00_7597 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_4439 + 2L)) ==
																					BgL_classz00_4431);
																			}
																		else
																			{	/* SawMill/regalloc.scm 395 */
																				bool_t BgL_res2739z00_4464;

																				{	/* SawMill/regalloc.scm 395 */
																					obj_t BgL_oclassz00_4447;

																					{	/* SawMill/regalloc.scm 395 */
																						obj_t BgL_arg1815z00_4455;
																						long BgL_arg1816z00_4456;

																						BgL_arg1815z00_4455 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* SawMill/regalloc.scm 395 */
																							long BgL_arg1817z00_4457;

																							BgL_arg1817z00_4457 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_4433);
																							BgL_arg1816z00_4456 =
																								(BgL_arg1817z00_4457 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_4447 =
																							VECTOR_REF(BgL_arg1815z00_4455,
																							BgL_arg1816z00_4456);
																					}
																					{	/* SawMill/regalloc.scm 395 */
																						bool_t BgL__ortest_1115z00_4448;

																						BgL__ortest_1115z00_4448 =
																							(BgL_classz00_4431 ==
																							BgL_oclassz00_4447);
																						if (BgL__ortest_1115z00_4448)
																							{	/* SawMill/regalloc.scm 395 */
																								BgL_res2739z00_4464 =
																									BgL__ortest_1115z00_4448;
																							}
																						else
																							{	/* SawMill/regalloc.scm 395 */
																								long BgL_odepthz00_4449;

																								{	/* SawMill/regalloc.scm 395 */
																									obj_t BgL_arg1804z00_4450;

																									BgL_arg1804z00_4450 =
																										(BgL_oclassz00_4447);
																									BgL_odepthz00_4449 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_4450);
																								}
																								if ((2L < BgL_odepthz00_4449))
																									{	/* SawMill/regalloc.scm 395 */
																										obj_t BgL_arg1802z00_4452;

																										{	/* SawMill/regalloc.scm 395 */
																											obj_t BgL_arg1803z00_4453;

																											BgL_arg1803z00_4453 =
																												(BgL_oclassz00_4447);
																											BgL_arg1802z00_4452 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_4453,
																												2L);
																										}
																										BgL_res2739z00_4464 =
																											(BgL_arg1802z00_4452 ==
																											BgL_classz00_4431);
																									}
																								else
																									{	/* SawMill/regalloc.scm 395 */
																										BgL_res2739z00_4464 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test3030z00_7597 =
																					BgL_res2739z00_4464;
																			}
																	}
																else
																	{	/* SawMill/regalloc.scm 395 */
																		BgL_test3030z00_7597 = ((bool_t) 0);
																	}
															}
														}
														if (BgL_test3030z00_7597)
															{	/* SawMill/regalloc.scm 396 */
																bool_t BgL_test3035z00_7623;

																{	/* SawMill/regalloc.scm 396 */
																	obj_t BgL_arg2116z00_2621;
																	obj_t BgL_arg2117z00_2622;

																	BgL_arg2116z00_2621 =
																		(((BgL_rtl_insz00_bglt) COBJECT(
																				((BgL_rtl_insz00_bglt)
																					((BgL_rtl_insz00_bglt)
																						BgL_iz00_2614))))->BgL_destz00);
																	{
																		BgL_rtl_inszf2razf2_bglt BgL_auxz00_7627;

																		{
																			obj_t BgL_auxz00_7628;

																			{	/* SawMill/regalloc.scm 396 */
																				BgL_objectz00_bglt BgL_tmpz00_7629;

																				BgL_tmpz00_7629 =
																					((BgL_objectz00_bglt)
																					((BgL_rtl_insz00_bglt)
																						BgL_iz00_2614));
																				BgL_auxz00_7628 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_7629);
																			}
																			BgL_auxz00_7627 =
																				((BgL_rtl_inszf2razf2_bglt)
																				BgL_auxz00_7628);
																		}
																		BgL_arg2117z00_2622 =
																			(((BgL_rtl_inszf2razf2_bglt)
																				COBJECT(BgL_auxz00_7627))->BgL_outz00);
																	}
																	{	/* SawMill/regalloc.scm 396 */
																		long BgL_basez00_4469;
																		long BgL_bitz00_4470;

																		{	/* SawMill/regalloc.scm 396 */
																			int BgL_arg2607z00_4471;

																			{
																				BgL_rtl_regzf2razf2_bglt
																					BgL_auxz00_7635;
																				{
																					obj_t BgL_auxz00_7636;

																					{	/* SawMill/regalloc.scm 396 */
																						BgL_objectz00_bglt BgL_tmpz00_7637;

																						BgL_tmpz00_7637 =
																							((BgL_objectz00_bglt)
																							((BgL_rtl_regz00_bglt)
																								BgL_arg2116z00_2621));
																						BgL_auxz00_7636 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_7637);
																					}
																					BgL_auxz00_7635 =
																						((BgL_rtl_regzf2razf2_bglt)
																						BgL_auxz00_7636);
																				}
																				BgL_arg2607z00_4471 =
																					(((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_7635))->
																					BgL_numz00);
																			}
																			BgL_basez00_4469 =
																				((long) (BgL_arg2607z00_4471) / 8L);
																		}
																		{	/* SawMill/regalloc.scm 396 */
																			int BgL_arg2608z00_4472;

																			{
																				BgL_rtl_regzf2razf2_bglt
																					BgL_auxz00_7645;
																				{
																					obj_t BgL_auxz00_7646;

																					{	/* SawMill/regalloc.scm 396 */
																						BgL_objectz00_bglt BgL_tmpz00_7647;

																						BgL_tmpz00_7647 =
																							((BgL_objectz00_bglt)
																							((BgL_rtl_regz00_bglt)
																								BgL_arg2116z00_2621));
																						BgL_auxz00_7646 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_7647);
																					}
																					BgL_auxz00_7645 =
																						((BgL_rtl_regzf2razf2_bglt)
																						BgL_auxz00_7646);
																				}
																				BgL_arg2608z00_4472 =
																					(((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_7645))->
																					BgL_numz00);
																			}
																			{	/* SawMill/regalloc.scm 396 */
																				long BgL_n1z00_4482;
																				long BgL_n2z00_4483;

																				BgL_n1z00_4482 =
																					(long) (BgL_arg2608z00_4472);
																				BgL_n2z00_4483 = 8L;
																				{	/* SawMill/regalloc.scm 396 */
																					bool_t BgL_test3036z00_7654;

																					{	/* SawMill/regalloc.scm 396 */
																						long BgL_arg1338z00_4485;

																						BgL_arg1338z00_4485 =
																							(((BgL_n1z00_4482) |
																								(BgL_n2z00_4483)) &
																							-2147483648);
																						BgL_test3036z00_7654 =
																							(BgL_arg1338z00_4485 == 0L);
																					}
																					if (BgL_test3036z00_7654)
																						{	/* SawMill/regalloc.scm 396 */
																							int32_t BgL_arg1334z00_4486;

																							{	/* SawMill/regalloc.scm 396 */
																								int32_t BgL_arg1336z00_4487;
																								int32_t BgL_arg1337z00_4488;

																								BgL_arg1336z00_4487 =
																									(int32_t) (BgL_n1z00_4482);
																								BgL_arg1337z00_4488 =
																									(int32_t) (BgL_n2z00_4483);
																								BgL_arg1334z00_4486 =
																									(BgL_arg1336z00_4487 %
																									BgL_arg1337z00_4488);
																							}
																							{	/* SawMill/regalloc.scm 396 */
																								long BgL_arg1446z00_4493;

																								BgL_arg1446z00_4493 =
																									(long) (BgL_arg1334z00_4486);
																								BgL_bitz00_4470 =
																									(long) (BgL_arg1446z00_4493);
																						}}
																					else
																						{	/* SawMill/regalloc.scm 396 */
																							BgL_bitz00_4470 =
																								(BgL_n1z00_4482 %
																								BgL_n2z00_4483);
																						}
																				}
																			}
																		}
																		if (
																			(BgL_basez00_4469 <
																				STRING_LENGTH(
																					(((BgL_regsetz00_bglt) COBJECT(
																								((BgL_regsetz00_bglt)
																									BgL_arg2117z00_2622)))->
																						BgL_stringz00))))
																			{	/* SawMill/regalloc.scm 396 */
																				BgL_test3035z00_7623 =
																					(
																					((STRING_REF(
																								(((BgL_regsetz00_bglt) COBJECT(
																											((BgL_regsetz00_bglt)
																												BgL_arg2117z00_2622)))->
																									BgL_stringz00),
																								BgL_basez00_4469)) & (1L <<
																							(int) (BgL_bitz00_4470))) > 0L);
																			}
																		else
																			{	/* SawMill/regalloc.scm 396 */
																				BgL_test3035z00_7623 = ((bool_t) 0);
																			}
																	}
																}
																if (BgL_test3035z00_7623)
																	{	/* SawMill/regalloc.scm 396 */
																		BFALSE;
																	}
																else
																	{	/* SawMill/regalloc.scm 396 */
																		BgL_nz00_2603 = (BgL_nz00_2603 + 1L);
																		((((BgL_rtl_insz00_bglt) COBJECT(
																						((BgL_rtl_insz00_bglt)
																							((BgL_rtl_insz00_bglt)
																								BgL_iz00_2614))))->
																				BgL_destz00) =
																			((obj_t) BFALSE), BUNSPEC);
																	}
															}
														else
															{	/* SawMill/regalloc.scm 395 */
																BFALSE;
															}
													}
												}
												{
													obj_t BgL_l1598z00_7680;

													BgL_l1598z00_7680 = CDR(BgL_l1598z00_2611);
													BgL_l1598z00_2611 = BgL_l1598z00_7680;
													goto BgL_zc3z04anonymousza32109ze3z87_2612;
												}
											}
										else
											{	/* SawMill/regalloc.scm 399 */
												((bool_t) 1);
											}
									}
								}
							}
							{
								obj_t BgL_l1601z00_7682;

								BgL_l1601z00_7682 = CDR(BgL_l1601z00_2605);
								BgL_l1601z00_2605 = BgL_l1601z00_7682;
								goto BgL_zc3z04anonymousza32107ze3z87_2606;
							}
						}
					else
						{	/* SawMill/regalloc.scm 392 */
							((bool_t) 1);
						}
				}
				{	/* SawMill/regalloc.scm 401 */
					obj_t BgL_list2121z00_2628;

					{	/* SawMill/regalloc.scm 401 */
						obj_t BgL_arg2122z00_2629;

						BgL_arg2122z00_2629 =
							MAKE_YOUNG_PAIR(BGl_string2781z00zzsaw_registerzd2allocationzd2,
							BNIL);
						BgL_list2121z00_2628 =
							MAKE_YOUNG_PAIR(BINT(BgL_nz00_2603), BgL_arg2122z00_2629);
					}
					return BGl_verbosez00zztools_speekz00(BINT(3L), BgL_list2121z00_2628);
				}
			}
		}

	}



/* register-coloring */
	obj_t BGl_registerzd2coloringzd2zzsaw_registerzd2allocationzd2(obj_t
		BgL_pregsz00_89, obj_t BgL_hregsz00_90, obj_t BgL_cregsz00_91)
	{
		{	/* SawMill/regalloc.scm 406 */
			{	/* SawMill/regalloc.scm 408 */
				obj_t BgL_g1605z00_2630;

				{	/* SawMill/regalloc.scm 411 */
					obj_t BgL_list2127z00_2640;

					{	/* SawMill/regalloc.scm 411 */
						obj_t BgL_arg2129z00_2641;

						{	/* SawMill/regalloc.scm 411 */
							obj_t BgL_arg2130z00_2642;

							BgL_arg2130z00_2642 = MAKE_YOUNG_PAIR(BgL_cregsz00_91, BNIL);
							BgL_arg2129z00_2641 =
								MAKE_YOUNG_PAIR(BgL_hregsz00_90, BgL_arg2130z00_2642);
						}
						BgL_list2127z00_2640 =
							MAKE_YOUNG_PAIR(BgL_pregsz00_89, BgL_arg2129z00_2641);
					}
					BgL_g1605z00_2630 =
						BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list2127z00_2640);
				}
				{
					obj_t BgL_l1603z00_2632;

					BgL_l1603z00_2632 = BgL_g1605z00_2630;
				BgL_zc3z04anonymousza32123ze3z87_2633:
					if (PAIRP(BgL_l1603z00_2632))
						{	/* SawMill/regalloc.scm 411 */
							{	/* SawMill/regalloc.scm 409 */
								obj_t BgL_rz00_2635;

								BgL_rz00_2635 = CAR(BgL_l1603z00_2632);
								{
									obj_t BgL_auxz00_7703;
									BgL_rtl_regzf2razf2_bglt BgL_auxz00_7696;

									{	/* SawMill/regalloc.scm 410 */
										obj_t BgL_arg2125z00_2637;

										{
											BgL_rtl_regzf2razf2_bglt BgL_auxz00_7704;

											{
												obj_t BgL_auxz00_7705;

												{	/* SawMill/regalloc.scm 410 */
													BgL_objectz00_bglt BgL_tmpz00_7706;

													BgL_tmpz00_7706 =
														((BgL_objectz00_bglt)
														((BgL_rtl_regz00_bglt) BgL_rz00_2635));
													BgL_auxz00_7705 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_7706);
												}
												BgL_auxz00_7704 =
													((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_7705);
											}
											BgL_arg2125z00_2637 =
												(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_7704))->
												BgL_interferez00);
										}
										BgL_auxz00_7703 =
											((obj_t)
											BGl_duplicatezd2regsetzd2zzsaw_regsetz00(
												((BgL_regsetz00_bglt) BgL_arg2125z00_2637)));
									}
									{
										obj_t BgL_auxz00_7697;

										{	/* SawMill/regalloc.scm 410 */
											BgL_objectz00_bglt BgL_tmpz00_7698;

											BgL_tmpz00_7698 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_rz00_2635));
											BgL_auxz00_7697 = BGL_OBJECT_WIDENING(BgL_tmpz00_7698);
										}
										BgL_auxz00_7696 =
											((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_7697);
									}
									((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_7696))->
											BgL_interfere2z00) = ((obj_t) BgL_auxz00_7703), BUNSPEC);
								}
							}
							{
								obj_t BgL_l1603z00_7716;

								BgL_l1603z00_7716 = CDR(BgL_l1603z00_2632);
								BgL_l1603z00_2632 = BgL_l1603z00_7716;
								goto BgL_zc3z04anonymousza32123ze3z87_2633;
							}
						}
					else
						{	/* SawMill/regalloc.scm 411 */
							((bool_t) 1);
						}
				}
			}
			{	/* SawMill/regalloc.scm 413 */
				obj_t BgL_cregsz00_2643;

				{	/* SawMill/regalloc.scm 413 */
					obj_t BgL_hook1610z00_2647;

					BgL_hook1610z00_2647 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
					{
						obj_t BgL_l1607z00_2649;
						obj_t BgL_h1608z00_2650;

						BgL_l1607z00_2649 = BgL_cregsz00_91;
						BgL_h1608z00_2650 = BgL_hook1610z00_2647;
					BgL_zc3z04anonymousza32134ze3z87_2651:
						if (NULLP(BgL_l1607z00_2649))
							{	/* SawMill/regalloc.scm 413 */
								BgL_cregsz00_2643 = CDR(BgL_hook1610z00_2647);
							}
						else
							{	/* SawMill/regalloc.scm 413 */
								bool_t BgL_test3040z00_7722;

								{	/* SawMill/regalloc.scm 413 */
									obj_t BgL_arg2142z00_2661;

									{	/* SawMill/regalloc.scm 413 */
										BgL_rtl_regz00_bglt BgL_oz00_4518;

										BgL_oz00_4518 =
											((BgL_rtl_regz00_bglt) CAR(((obj_t) BgL_l1607z00_2649)));
										{
											BgL_rtl_regzf2razf2_bglt BgL_auxz00_7726;

											{
												obj_t BgL_auxz00_7727;

												{	/* SawMill/regalloc.scm 413 */
													BgL_objectz00_bglt BgL_tmpz00_7728;

													BgL_tmpz00_7728 =
														((BgL_objectz00_bglt) BgL_oz00_4518);
													BgL_auxz00_7727 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_7728);
												}
												BgL_auxz00_7726 =
													((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_7727);
											}
											BgL_arg2142z00_2661 =
												(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_7726))->
												BgL_colorz00);
										}
									}
									BgL_test3040z00_7722 = (BgL_arg2142z00_2661 == BFALSE);
								}
								if (BgL_test3040z00_7722)
									{	/* SawMill/regalloc.scm 413 */
										obj_t BgL_nh1609z00_2656;

										{	/* SawMill/regalloc.scm 413 */
											obj_t BgL_arg2139z00_2658;

											BgL_arg2139z00_2658 = CAR(((obj_t) BgL_l1607z00_2649));
											BgL_nh1609z00_2656 =
												MAKE_YOUNG_PAIR(BgL_arg2139z00_2658, BNIL);
										}
										SET_CDR(BgL_h1608z00_2650, BgL_nh1609z00_2656);
										{	/* SawMill/regalloc.scm 413 */
											obj_t BgL_arg2138z00_2657;

											BgL_arg2138z00_2657 = CDR(((obj_t) BgL_l1607z00_2649));
											{
												obj_t BgL_h1608z00_7741;
												obj_t BgL_l1607z00_7740;

												BgL_l1607z00_7740 = BgL_arg2138z00_2657;
												BgL_h1608z00_7741 = BgL_nh1609z00_2656;
												BgL_h1608z00_2650 = BgL_h1608z00_7741;
												BgL_l1607z00_2649 = BgL_l1607z00_7740;
												goto BgL_zc3z04anonymousza32134ze3z87_2651;
											}
										}
									}
								else
									{	/* SawMill/regalloc.scm 413 */
										obj_t BgL_arg2141z00_2659;

										BgL_arg2141z00_2659 = CDR(((obj_t) BgL_l1607z00_2649));
										{
											obj_t BgL_l1607z00_7744;

											BgL_l1607z00_7744 = BgL_arg2141z00_2659;
											BgL_l1607z00_2649 = BgL_l1607z00_7744;
											goto BgL_zc3z04anonymousza32134ze3z87_2651;
										}
									}
							}
					}
				}
				if (PAIRP(BgL_cregsz00_2643))
					{	/* SawMill/regalloc.scm 414 */
						if (NULLP(BgL_hregsz00_90))
							{	/* SawMill/regalloc.scm 416 */
								return
									BGl_registerzd2stackzd2coloringz00zzsaw_registerzd2allocationzd2
									(BgL_pregsz00_89, BgL_cregsz00_2643);
							}
						else
							{	/* SawMill/regalloc.scm 416 */
								return
									BGl_registerzd2hardwarezd2coloringz00zzsaw_registerzd2allocationzd2
									(BgL_pregsz00_89, BgL_hregsz00_90, BgL_cregsz00_2643);
							}
					}
				else
					{	/* SawMill/regalloc.scm 414 */
						return BFALSE;
					}
			}
		}

	}



/* register-stack-coloring */
	obj_t BGl_registerzd2stackzd2coloringz00zzsaw_registerzd2allocationzd2(obj_t
		BgL_pregsz00_92, obj_t BgL_cregsz00_93)
	{
		{	/* SawMill/regalloc.scm 424 */
			{
				obj_t BgL_registersz00_2752;
				obj_t BgL_siza7eza7_2753;

				{	/* SawMill/regalloc.scm 451 */
					obj_t BgL_colorsz00_2664;

					BgL_colorsz00_2664 =
						make_vector(bgl_list_length
						(BGl_appendzd221011zd2zzsaw_registerzd2allocationzd2
							(BgL_pregsz00_92, BgL_cregsz00_93)), BFALSE);
					{
						obj_t BgL_interferez00_2724;
						long BgL_maxz00_2725;

						{
							long BgL_minz00_2707;
							obj_t BgL_stackz00_2708;

							{	/* SawMill/regalloc.scm 474 */
								long BgL_nz00_2667;

								BgL_nz00_2667 = 0L;
								{
									obj_t BgL_l1620z00_2669;

									BgL_l1620z00_2669 = BgL_pregsz00_92;
								BgL_zc3z04anonymousza32143ze3z87_2670:
									if (PAIRP(BgL_l1620z00_2669))
										{	/* SawMill/regalloc.scm 475 */
											{	/* SawMill/regalloc.scm 476 */
												obj_t BgL_rz00_2672;

												BgL_rz00_2672 = CAR(BgL_l1620z00_2669);
												BGl_simplifyzd2regz12zc0zzsaw_registerzd2allocationzd2(
													((BgL_rtl_regz00_bglt) BgL_rz00_2672));
												{	/* SawMill/regalloc.scm 477 */
													obj_t BgL_vz00_4598;

													BgL_vz00_4598 = BINT(BgL_nz00_2667);
													{
														BgL_rtl_regzf2razf2_bglt BgL_auxz00_7760;

														{
															obj_t BgL_auxz00_7761;

															{	/* SawMill/regalloc.scm 477 */
																BgL_objectz00_bglt BgL_tmpz00_7762;

																BgL_tmpz00_7762 =
																	((BgL_objectz00_bglt)
																	((BgL_rtl_regz00_bglt) BgL_rz00_2672));
																BgL_auxz00_7761 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_7762);
															}
															BgL_auxz00_7760 =
																((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_7761);
														}
														((((BgL_rtl_regzf2razf2_bglt)
																	COBJECT(BgL_auxz00_7760))->BgL_colorz00) =
															((obj_t) BgL_vz00_4598), BUNSPEC);
													}
												}
												BgL_nz00_2667 = (BgL_nz00_2667 + 1L);
											}
											{
												obj_t BgL_l1620z00_7769;

												BgL_l1620z00_7769 = CDR(BgL_l1620z00_2669);
												BgL_l1620z00_2669 = BgL_l1620z00_7769;
												goto BgL_zc3z04anonymousza32143ze3z87_2670;
											}
										}
									else
										{	/* SawMill/regalloc.scm 475 */
											((bool_t) 1);
										}
								}
							}
							{	/* SawMill/regalloc.scm 481 */
								long BgL_minz00_2675;
								obj_t BgL_sregsz00_2676;

								BgL_minz00_2675 = bgl_list_length(BgL_pregsz00_92);
								BgL_sregsz00_2676 =
									BGl_sortz00zz__r4_vectors_6_8z00(BgL_cregsz00_93,
									BGl_proc2797z00zzsaw_registerzd2allocationzd2);
								{
									obj_t BgL_regsz00_2679;
									obj_t BgL_stackz00_2680;
									obj_t BgL_siza7eza7_2681;

									BgL_regsz00_2679 = BgL_sregsz00_2676;
									BgL_stackz00_2680 = BNIL;
									BgL_siza7eza7_2681 = BINT(0L);
								BgL_zc3z04anonymousza32146ze3z87_2682:
									if (NULLP(BgL_regsz00_2679))
										{	/* SawMill/regalloc.scm 490 */
											long BgL_nz00_2684;

											BgL_minz00_2707 = BgL_minz00_2675;
											BgL_stackz00_2708 = BgL_stackz00_2680;
											{
												obj_t BgL_stackz00_2711;
												long BgL_maxz00_2712;

												BgL_stackz00_2711 = BgL_stackz00_2708;
												BgL_maxz00_2712 = BgL_minz00_2707;
											BgL_zc3z04anonymousza32163ze3z87_2713:
												if (PAIRP(BgL_stackz00_2711))
													{	/* SawMill/regalloc.scm 469 */
														BgL_rtl_regz00_bglt BgL_i1225z00_2715;

														BgL_i1225z00_2715 =
															((BgL_rtl_regz00_bglt) CAR(BgL_stackz00_2711));
														{
															obj_t BgL_auxz00_7785;
															BgL_rtl_regzf2razf2_bglt BgL_auxz00_7779;

															{	/* SawMill/regalloc.scm 470 */
																obj_t BgL_arg2165z00_2716;

																{
																	BgL_rtl_regzf2razf2_bglt BgL_auxz00_7786;

																	{
																		obj_t BgL_auxz00_7787;

																		{	/* SawMill/regalloc.scm 470 */
																			BgL_objectz00_bglt BgL_tmpz00_7788;

																			BgL_tmpz00_7788 =
																				((BgL_objectz00_bglt)
																				BgL_i1225z00_2715);
																			BgL_auxz00_7787 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_7788);
																		}
																		BgL_auxz00_7786 =
																			((BgL_rtl_regzf2razf2_bglt)
																			BgL_auxz00_7787);
																	}
																	BgL_arg2165z00_2716 =
																		(((BgL_rtl_regzf2razf2_bglt)
																			COBJECT(BgL_auxz00_7786))->
																		BgL_interferez00);
																}
																{	/* SawMill/regalloc.scm 470 */
																	long BgL_tmpz00_7793;

																	BgL_interferez00_2724 = BgL_arg2165z00_2716;
																	BgL_maxz00_2725 = BgL_maxz00_2712;
																	{	/* SawMill/regalloc.scm 453 */

																		BGl_vectorzd2fillz12zc0zz__r4_vectors_6_8z00
																			(BgL_colorsz00_2664, BFALSE, 0L,
																			VECTOR_LENGTH(BgL_colorsz00_2664));
																	}
																	{	/* SawMill/regalloc.scm 455 */
																		obj_t BgL_zc3z04anonymousza32173ze3z87_5912;

																		BgL_zc3z04anonymousza32173ze3z87_5912 =
																			MAKE_FX_PROCEDURE
																			(BGl_z62zc3z04anonymousza32173ze3ze5zzsaw_registerzd2allocationzd2,
																			(int) (1L), (int) (1L));
																		PROCEDURE_SET
																			(BgL_zc3z04anonymousza32173ze3z87_5912,
																			(int) (0L), BgL_colorsz00_2664);
																		BGl_regsetzd2forzd2eachz00zzsaw_regsetz00
																			(BgL_zc3z04anonymousza32173ze3z87_5912,
																			((BgL_regsetz00_bglt)
																				BgL_interferez00_2724));
																	}
																	if (
																		(VECTOR_LENGTH(BgL_colorsz00_2664) ==
																			BgL_maxz00_2725))
																		{	/* SawMill/regalloc.scm 459 */
																			BgL_tmpz00_7793 = BgL_maxz00_2725;
																		}
																	else
																		{
																			long BgL_iz00_4582;

																			BgL_iz00_4582 = 0L;
																		BgL_loopz00_4581:
																			if (CBOOL(VECTOR_REF(BgL_colorsz00_2664,
																						BgL_iz00_4582)))
																				{
																					long BgL_iz00_7809;

																					BgL_iz00_7809 = (BgL_iz00_4582 + 1L);
																					BgL_iz00_4582 = BgL_iz00_7809;
																					goto BgL_loopz00_4581;
																				}
																			else
																				{	/* SawMill/regalloc.scm 462 */
																					BgL_tmpz00_7793 = BgL_iz00_4582;
																				}
																		}
																	BgL_auxz00_7785 = BINT(BgL_tmpz00_7793);
																}
															}
															{
																obj_t BgL_auxz00_7780;

																{	/* SawMill/regalloc.scm 470 */
																	BgL_objectz00_bglt BgL_tmpz00_7781;

																	BgL_tmpz00_7781 =
																		((BgL_objectz00_bglt) BgL_i1225z00_2715);
																	BgL_auxz00_7780 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_7781);
																}
																BgL_auxz00_7779 =
																	((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_7780);
															}
															((((BgL_rtl_regzf2razf2_bglt)
																		COBJECT(BgL_auxz00_7779))->BgL_colorz00) =
																((obj_t) BgL_auxz00_7785), BUNSPEC);
														}
														{	/* SawMill/regalloc.scm 471 */
															obj_t BgL_arg2166z00_2717;
															long BgL_arg2167z00_2718;

															BgL_arg2166z00_2717 = CDR(BgL_stackz00_2711);
															{	/* SawMill/regalloc.scm 471 */
																bool_t BgL_test3048z00_7814;

																{	/* SawMill/regalloc.scm 471 */
																	obj_t BgL_arg2170z00_2721;

																	{
																		BgL_rtl_regzf2razf2_bglt BgL_auxz00_7815;

																		{
																			obj_t BgL_auxz00_7816;

																			{	/* SawMill/regalloc.scm 471 */
																				BgL_objectz00_bglt BgL_tmpz00_7817;

																				BgL_tmpz00_7817 =
																					((BgL_objectz00_bglt)
																					BgL_i1225z00_2715);
																				BgL_auxz00_7816 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_7817);
																			}
																			BgL_auxz00_7815 =
																				((BgL_rtl_regzf2razf2_bglt)
																				BgL_auxz00_7816);
																		}
																		BgL_arg2170z00_2721 =
																			(((BgL_rtl_regzf2razf2_bglt)
																				COBJECT(BgL_auxz00_7815))->
																			BgL_colorz00);
																	}
																	BgL_test3048z00_7814 =
																		(
																		(long) CINT(BgL_arg2170z00_2721) ==
																		BgL_maxz00_2712);
																}
																if (BgL_test3048z00_7814)
																	{	/* SawMill/regalloc.scm 471 */
																		BgL_arg2167z00_2718 =
																			(BgL_maxz00_2712 + 1L);
																	}
																else
																	{	/* SawMill/regalloc.scm 471 */
																		BgL_arg2167z00_2718 = BgL_maxz00_2712;
																	}
															}
															{
																long BgL_maxz00_7826;
																obj_t BgL_stackz00_7825;

																BgL_stackz00_7825 = BgL_arg2166z00_2717;
																BgL_maxz00_7826 = BgL_arg2167z00_2718;
																BgL_maxz00_2712 = BgL_maxz00_7826;
																BgL_stackz00_2711 = BgL_stackz00_7825;
																goto BgL_zc3z04anonymousza32163ze3z87_2713;
															}
														}
													}
												else
													{	/* SawMill/regalloc.scm 468 */
														BgL_nz00_2684 = BgL_maxz00_2712;
													}
											}
											{	/* SawMill/regalloc.scm 492 */
												obj_t BgL_arg2148z00_2685;

												{	/* SawMill/regalloc.scm 492 */
													long BgL_a1622z00_2692;

													BgL_a1622z00_2692 = bgl_list_length(BgL_pregsz00_92);
													{	/* SawMill/regalloc.scm 492 */
														long BgL_b1623z00_2693;

														BgL_b1623z00_2693 =
															bgl_list_length(BgL_cregsz00_93);
														{	/* SawMill/regalloc.scm 492 */

															{	/* SawMill/regalloc.scm 492 */
																obj_t BgL_za71za7_4616;
																obj_t BgL_za72za7_4617;

																BgL_za71za7_4616 = BINT(BgL_a1622z00_2692);
																BgL_za72za7_4617 = BINT(BgL_b1623z00_2693);
																{	/* SawMill/regalloc.scm 492 */
																	obj_t BgL_tmpz00_4618;

																	BgL_tmpz00_4618 = BINT(0L);
																	if (BGL_ADDFX_OV(BgL_za71za7_4616,
																			BgL_za72za7_4617, BgL_tmpz00_4618))
																		{	/* SawMill/regalloc.scm 492 */
																			BgL_arg2148z00_2685 =
																				bgl_bignum_add(bgl_long_to_bignum(
																					(long) CINT(BgL_za71za7_4616)),
																				bgl_long_to_bignum(
																					(long) CINT(BgL_za72za7_4617)));
																		}
																	else
																		{	/* SawMill/regalloc.scm 492 */
																			BgL_arg2148z00_2685 = BgL_tmpz00_4618;
																		}
																}
															}
														}
													}
												}
												{	/* SawMill/regalloc.scm 491 */
													obj_t BgL_list2149z00_2686;

													{	/* SawMill/regalloc.scm 491 */
														obj_t BgL_arg2150z00_2687;

														{	/* SawMill/regalloc.scm 491 */
															obj_t BgL_arg2151z00_2688;

															{	/* SawMill/regalloc.scm 491 */
																obj_t BgL_arg2152z00_2689;

																{	/* SawMill/regalloc.scm 491 */
																	obj_t BgL_arg2154z00_2690;

																	{	/* SawMill/regalloc.scm 491 */
																		obj_t BgL_arg2155z00_2691;

																		BgL_arg2155z00_2691 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2798z00zzsaw_registerzd2allocationzd2,
																			BNIL);
																		BgL_arg2154z00_2690 =
																			MAKE_YOUNG_PAIR
																			(BGl_string2799z00zzsaw_registerzd2allocationzd2,
																			BgL_arg2155z00_2691);
																	}
																	BgL_arg2152z00_2689 =
																		MAKE_YOUNG_PAIR(BgL_arg2148z00_2685,
																		BgL_arg2154z00_2690);
																}
																BgL_arg2151z00_2688 =
																	MAKE_YOUNG_PAIR
																	(BGl_string2800z00zzsaw_registerzd2allocationzd2,
																	BgL_arg2152z00_2689);
															}
															BgL_arg2150z00_2687 =
																MAKE_YOUNG_PAIR(BINT(BgL_nz00_2684),
																BgL_arg2151z00_2688);
														}
														BgL_list2149z00_2686 =
															MAKE_YOUNG_PAIR
															(BGl_string2801z00zzsaw_registerzd2allocationzd2,
															BgL_arg2150z00_2687);
													}
													return
														BGl_verbosez00zztools_speekz00(BINT(3L),
														BgL_list2149z00_2686);
												}
											}
										}
									else
										{	/* SawMill/regalloc.scm 494 */
											obj_t BgL_regz00_2695;

											BgL_registersz00_2752 = BgL_regsz00_2679;
											BgL_siza7eza7_2753 = BgL_siza7eza7_2681;
											{	/* SawMill/regalloc.scm 426 */
												long BgL_g1223z00_2755;

												{	/* SawMill/regalloc.scm 428 */
													int BgL_arg2207z00_2792;

													{	/* SawMill/regalloc.scm 428 */
														BgL_rtl_regz00_bglt BgL_regz00_4525;

														BgL_regz00_4525 =
															((BgL_rtl_regz00_bglt)
															CAR(((obj_t) BgL_registersz00_2752)));
														{	/* SawMill/regalloc.scm 591 */
															obj_t BgL_arg2263z00_4526;

															{
																BgL_rtl_regzf2razf2_bglt BgL_auxz00_7851;

																{
																	obj_t BgL_auxz00_7852;

																	{	/* SawMill/regalloc.scm 591 */
																		BgL_objectz00_bglt BgL_tmpz00_7853;

																		BgL_tmpz00_7853 =
																			((BgL_objectz00_bglt) BgL_regz00_4525);
																		BgL_auxz00_7852 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_7853);
																	}
																	BgL_auxz00_7851 =
																		((BgL_rtl_regzf2razf2_bglt)
																		BgL_auxz00_7852);
																}
																BgL_arg2263z00_4526 =
																	(((BgL_rtl_regzf2razf2_bglt)
																		COBJECT(BgL_auxz00_7851))->
																	BgL_interfere2z00);
															}
															BgL_arg2207z00_2792 =
																(((BgL_regsetz00_bglt) COBJECT(
																		((BgL_regsetz00_bglt)
																			BgL_arg2263z00_4526)))->BgL_lengthz00);
													}}
													BgL_g1223z00_2755 =
														((long) (BgL_arg2207z00_2792) + 1L);
												}
												{
													obj_t BgL_regsz00_2757;
													obj_t BgL_minz00_2758;
													long BgL_mvalz00_2759;
													obj_t BgL_prevz00_2760;

													BgL_regsz00_2757 = BgL_registersz00_2752;
													BgL_minz00_2758 = BFALSE;
													BgL_mvalz00_2759 = BgL_g1223z00_2755;
													BgL_prevz00_2760 = BFALSE;
												BgL_zc3z04anonymousza32187ze3z87_2761:
													if (NULLP(BgL_regsz00_2757))
														{	/* SawMill/regalloc.scm 432 */
															int BgL_val1_1612z00_2764;
															obj_t BgL_val2_1613z00_2765;

															{	/* SawMill/regalloc.scm 591 */
																obj_t BgL_arg2263z00_4532;

																{
																	BgL_rtl_regzf2razf2_bglt BgL_auxz00_7864;

																	{
																		obj_t BgL_auxz00_7865;

																		{	/* SawMill/regalloc.scm 591 */
																			BgL_objectz00_bglt BgL_tmpz00_7866;

																			BgL_tmpz00_7866 =
																				((BgL_objectz00_bglt)
																				((BgL_rtl_regz00_bglt)
																					BgL_minz00_2758));
																			BgL_auxz00_7865 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_7866);
																		}
																		BgL_auxz00_7864 =
																			((BgL_rtl_regzf2razf2_bglt)
																			BgL_auxz00_7865);
																	}
																	BgL_arg2263z00_4532 =
																		(((BgL_rtl_regzf2razf2_bglt)
																			COBJECT(BgL_auxz00_7864))->
																		BgL_interfere2z00);
																}
																BgL_val1_1612z00_2764 =
																	(((BgL_regsetz00_bglt) COBJECT(
																			((BgL_regsetz00_bglt)
																				BgL_arg2263z00_4532)))->BgL_lengthz00);
															}
															BgL_val2_1613z00_2765 =
																bgl_remq_bang(BgL_minz00_2758,
																BgL_registersz00_2752);
															{	/* SawMill/regalloc.scm 432 */
																int BgL_tmpz00_7875;

																BgL_tmpz00_7875 = (int) (3L);
																BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7875);
															}
															{	/* SawMill/regalloc.scm 432 */
																obj_t BgL_auxz00_7880;
																int BgL_tmpz00_7878;

																BgL_auxz00_7880 = BINT(BgL_val1_1612z00_2764);
																BgL_tmpz00_7878 = (int) (1L);
																BGL_MVALUES_VAL_SET(BgL_tmpz00_7878,
																	BgL_auxz00_7880);
															}
															{	/* SawMill/regalloc.scm 432 */
																int BgL_tmpz00_7883;

																BgL_tmpz00_7883 = (int) (2L);
																BGL_MVALUES_VAL_SET(BgL_tmpz00_7883,
																	BgL_val2_1613z00_2765);
															}
															BgL_regz00_2695 = BgL_minz00_2758;
														}
													else
														{	/* SawMill/regalloc.scm 433 */
															bool_t BgL_test3051z00_7886;

															{	/* SawMill/regalloc.scm 433 */
																int BgL_arg2205z00_2789;

																{	/* SawMill/regalloc.scm 433 */
																	BgL_rtl_regz00_bglt BgL_regz00_4537;

																	BgL_regz00_4537 =
																		((BgL_rtl_regz00_bglt)
																		CAR(((obj_t) BgL_regsz00_2757)));
																	{	/* SawMill/regalloc.scm 591 */
																		obj_t BgL_arg2263z00_4538;

																		{
																			BgL_rtl_regzf2razf2_bglt BgL_auxz00_7890;

																			{
																				obj_t BgL_auxz00_7891;

																				{	/* SawMill/regalloc.scm 591 */
																					BgL_objectz00_bglt BgL_tmpz00_7892;

																					BgL_tmpz00_7892 =
																						((BgL_objectz00_bglt)
																						BgL_regz00_4537);
																					BgL_auxz00_7891 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_7892);
																				}
																				BgL_auxz00_7890 =
																					((BgL_rtl_regzf2razf2_bglt)
																					BgL_auxz00_7891);
																			}
																			BgL_arg2263z00_4538 =
																				(((BgL_rtl_regzf2razf2_bglt)
																					COBJECT(BgL_auxz00_7890))->
																				BgL_interfere2z00);
																		}
																		BgL_arg2205z00_2789 =
																			(((BgL_regsetz00_bglt) COBJECT(
																					((BgL_regsetz00_bglt)
																						BgL_arg2263z00_4538)))->
																			BgL_lengthz00);
																}}
																BgL_test3051z00_7886 =
																	(
																	(long) (BgL_arg2205z00_2789) <
																	(long) CINT(BgL_siza7eza7_2753));
															}
															if (BgL_test3051z00_7886)
																{	/* SawMill/regalloc.scm 434 */
																	obj_t BgL_regz00_2769;

																	BgL_regz00_2769 =
																		CAR(((obj_t) BgL_regsz00_2757));
																	{	/* SawMill/regalloc.scm 434 */
																		int BgL_sza7za7_2770;

																		{	/* SawMill/regalloc.scm 591 */
																			obj_t BgL_arg2263z00_4546;

																			{
																				BgL_rtl_regzf2razf2_bglt
																					BgL_auxz00_7904;
																				{
																					obj_t BgL_auxz00_7905;

																					{	/* SawMill/regalloc.scm 591 */
																						BgL_objectz00_bglt BgL_tmpz00_7906;

																						BgL_tmpz00_7906 =
																							((BgL_objectz00_bglt)
																							((BgL_rtl_regz00_bglt)
																								BgL_regz00_2769));
																						BgL_auxz00_7905 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_7906);
																					}
																					BgL_auxz00_7904 =
																						((BgL_rtl_regzf2razf2_bglt)
																						BgL_auxz00_7905);
																				}
																				BgL_arg2263z00_4546 =
																					(((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_7904))->
																					BgL_interfere2z00);
																			}
																			BgL_sza7za7_2770 =
																				(((BgL_regsetz00_bglt) COBJECT(
																						((BgL_regsetz00_bglt)
																							BgL_arg2263z00_4546)))->
																				BgL_lengthz00);
																		}
																		{	/* SawMill/regalloc.scm 435 */

																			if (PAIRP(BgL_prevz00_2760))
																				{	/* SawMill/regalloc.scm 436 */
																					{	/* SawMill/regalloc.scm 438 */
																						obj_t BgL_arg2193z00_2772;

																						BgL_arg2193z00_2772 =
																							CDR(((obj_t) BgL_regsz00_2757));
																						SET_CDR(BgL_prevz00_2760,
																							BgL_arg2193z00_2772);
																					}
																					{	/* SawMill/regalloc.scm 439 */
																						int BgL_tmpz00_7919;

																						BgL_tmpz00_7919 = (int) (3L);
																						BGL_MVALUES_NUMBER_SET
																							(BgL_tmpz00_7919);
																					}
																					{	/* SawMill/regalloc.scm 439 */
																						obj_t BgL_auxz00_7924;
																						int BgL_tmpz00_7922;

																						BgL_auxz00_7924 =
																							BINT(BgL_sza7za7_2770);
																						BgL_tmpz00_7922 = (int) (1L);
																						BGL_MVALUES_VAL_SET(BgL_tmpz00_7922,
																							BgL_auxz00_7924);
																					}
																					{	/* SawMill/regalloc.scm 439 */
																						int BgL_tmpz00_7927;

																						BgL_tmpz00_7927 = (int) (2L);
																						BGL_MVALUES_VAL_SET(BgL_tmpz00_7927,
																							BgL_registersz00_2752);
																					}
																					BgL_regz00_2695 = BgL_regz00_2769;
																				}
																			else
																				{	/* SawMill/regalloc.scm 440 */
																					obj_t BgL_val2_1619z00_2778;

																					BgL_val2_1619z00_2778 =
																						CDR(((obj_t) BgL_regsz00_2757));
																					{	/* SawMill/regalloc.scm 440 */
																						int BgL_tmpz00_7932;

																						BgL_tmpz00_7932 = (int) (3L);
																						BGL_MVALUES_NUMBER_SET
																							(BgL_tmpz00_7932);
																					}
																					{	/* SawMill/regalloc.scm 440 */
																						obj_t BgL_auxz00_7937;
																						int BgL_tmpz00_7935;

																						BgL_auxz00_7937 =
																							BINT(BgL_sza7za7_2770);
																						BgL_tmpz00_7935 = (int) (1L);
																						BGL_MVALUES_VAL_SET(BgL_tmpz00_7935,
																							BgL_auxz00_7937);
																					}
																					{	/* SawMill/regalloc.scm 440 */
																						int BgL_tmpz00_7940;

																						BgL_tmpz00_7940 = (int) (2L);
																						BGL_MVALUES_VAL_SET(BgL_tmpz00_7940,
																							BgL_val2_1619z00_2778);
																					}
																					BgL_regz00_2695 = BgL_regz00_2769;
																}}}}
															else
																{	/* SawMill/regalloc.scm 441 */
																	bool_t BgL_test3053z00_7943;

																	{	/* SawMill/regalloc.scm 441 */
																		int BgL_arg2203z00_2787;

																		{	/* SawMill/regalloc.scm 441 */
																			BgL_rtl_regz00_bglt BgL_regz00_4554;

																			BgL_regz00_4554 =
																				((BgL_rtl_regz00_bglt)
																				CAR(((obj_t) BgL_regsz00_2757)));
																			{	/* SawMill/regalloc.scm 591 */
																				obj_t BgL_arg2263z00_4555;

																				{
																					BgL_rtl_regzf2razf2_bglt
																						BgL_auxz00_7947;
																					{
																						obj_t BgL_auxz00_7948;

																						{	/* SawMill/regalloc.scm 591 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_7949;
																							BgL_tmpz00_7949 =
																								((BgL_objectz00_bglt)
																								BgL_regz00_4554);
																							BgL_auxz00_7948 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_7949);
																						}
																						BgL_auxz00_7947 =
																							((BgL_rtl_regzf2razf2_bglt)
																							BgL_auxz00_7948);
																					}
																					BgL_arg2263z00_4555 =
																						(((BgL_rtl_regzf2razf2_bglt)
																							COBJECT(BgL_auxz00_7947))->
																						BgL_interfere2z00);
																				}
																				BgL_arg2203z00_2787 =
																					(((BgL_regsetz00_bglt) COBJECT(
																							((BgL_regsetz00_bglt)
																								BgL_arg2263z00_4555)))->
																					BgL_lengthz00);
																		}}
																		BgL_test3053z00_7943 =
																			(
																			(long) (BgL_arg2203z00_2787) <
																			BgL_mvalz00_2759);
																	}
																	if (BgL_test3053z00_7943)
																		{	/* SawMill/regalloc.scm 442 */
																			obj_t BgL_arg2198z00_2782;
																			obj_t BgL_arg2199z00_2783;
																			int BgL_arg2200z00_2784;

																			BgL_arg2198z00_2782 =
																				CDR(((obj_t) BgL_regsz00_2757));
																			BgL_arg2199z00_2783 =
																				CAR(((obj_t) BgL_regsz00_2757));
																			{	/* SawMill/regalloc.scm 444 */
																				BgL_rtl_regz00_bglt BgL_regz00_4564;

																				BgL_regz00_4564 =
																					((BgL_rtl_regz00_bglt)
																					CAR(((obj_t) BgL_regsz00_2757)));
																				{	/* SawMill/regalloc.scm 591 */
																					obj_t BgL_arg2263z00_4565;

																					{
																						BgL_rtl_regzf2razf2_bglt
																							BgL_auxz00_7965;
																						{
																							obj_t BgL_auxz00_7966;

																							{	/* SawMill/regalloc.scm 591 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_7967;
																								BgL_tmpz00_7967 =
																									((BgL_objectz00_bglt)
																									BgL_regz00_4564);
																								BgL_auxz00_7966 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_7967);
																							}
																							BgL_auxz00_7965 =
																								((BgL_rtl_regzf2razf2_bglt)
																								BgL_auxz00_7966);
																						}
																						BgL_arg2263z00_4565 =
																							(((BgL_rtl_regzf2razf2_bglt)
																								COBJECT(BgL_auxz00_7965))->
																							BgL_interfere2z00);
																					}
																					BgL_arg2200z00_2784 =
																						(((BgL_regsetz00_bglt) COBJECT(
																								((BgL_regsetz00_bglt)
																									BgL_arg2263z00_4565)))->
																						BgL_lengthz00);
																			}}
																			{
																				obj_t BgL_prevz00_7978;
																				long BgL_mvalz00_7976;
																				obj_t BgL_minz00_7975;
																				obj_t BgL_regsz00_7974;

																				BgL_regsz00_7974 = BgL_arg2198z00_2782;
																				BgL_minz00_7975 = BgL_arg2199z00_2783;
																				BgL_mvalz00_7976 =
																					(long) (BgL_arg2200z00_2784);
																				BgL_prevz00_7978 = BgL_regsz00_2757;
																				BgL_prevz00_2760 = BgL_prevz00_7978;
																				BgL_mvalz00_2759 = BgL_mvalz00_7976;
																				BgL_minz00_2758 = BgL_minz00_7975;
																				BgL_regsz00_2757 = BgL_regsz00_7974;
																				goto
																					BgL_zc3z04anonymousza32187ze3z87_2761;
																			}
																		}
																	else
																		{	/* SawMill/regalloc.scm 447 */
																			obj_t BgL_arg2202z00_2786;

																			BgL_arg2202z00_2786 =
																				CDR(((obj_t) BgL_regsz00_2757));
																			{
																				obj_t BgL_prevz00_7982;
																				obj_t BgL_regsz00_7981;

																				BgL_regsz00_7981 = BgL_arg2202z00_2786;
																				BgL_prevz00_7982 = BgL_regsz00_2757;
																				BgL_prevz00_2760 = BgL_prevz00_7982;
																				BgL_regsz00_2757 = BgL_regsz00_7981;
																				goto
																					BgL_zc3z04anonymousza32187ze3z87_2761;
																			}
																		}
																}
														}
												}
											}
											{	/* SawMill/regalloc.scm 495 */
												obj_t BgL_nsiza7eza7_2696;
												obj_t BgL_regsz00_2697;

												{	/* SawMill/regalloc.scm 496 */
													obj_t BgL_tmpz00_4626;

													{	/* SawMill/regalloc.scm 496 */
														int BgL_tmpz00_7983;

														BgL_tmpz00_7983 = (int) (1L);
														BgL_tmpz00_4626 = BGL_MVALUES_VAL(BgL_tmpz00_7983);
													}
													{	/* SawMill/regalloc.scm 496 */
														int BgL_tmpz00_7986;

														BgL_tmpz00_7986 = (int) (1L);
														BGL_MVALUES_VAL_SET(BgL_tmpz00_7986, BUNSPEC);
													}
													BgL_nsiza7eza7_2696 = BgL_tmpz00_4626;
												}
												{	/* SawMill/regalloc.scm 496 */
													obj_t BgL_tmpz00_4627;

													{	/* SawMill/regalloc.scm 496 */
														int BgL_tmpz00_7989;

														BgL_tmpz00_7989 = (int) (2L);
														BgL_tmpz00_4627 = BGL_MVALUES_VAL(BgL_tmpz00_7989);
													}
													{	/* SawMill/regalloc.scm 496 */
														int BgL_tmpz00_7992;

														BgL_tmpz00_7992 = (int) (2L);
														BGL_MVALUES_VAL_SET(BgL_tmpz00_7992, BUNSPEC);
													}
													BgL_regsz00_2697 = BgL_tmpz00_4627;
												}
												BGl_simplifyzd2regz12zc0zzsaw_registerzd2allocationzd2(
													((BgL_rtl_regz00_bglt) BgL_regz00_2695));
												{	/* SawMill/regalloc.scm 497 */
													obj_t BgL_arg2157z00_2698;

													BgL_arg2157z00_2698 =
														MAKE_YOUNG_PAIR(BgL_regz00_2695, BgL_stackz00_2680);
													{
														obj_t BgL_siza7eza7_8000;
														obj_t BgL_stackz00_7999;
														obj_t BgL_regsz00_7998;

														BgL_regsz00_7998 = BgL_regsz00_2697;
														BgL_stackz00_7999 = BgL_arg2157z00_2698;
														BgL_siza7eza7_8000 = BgL_nsiza7eza7_2696;
														BgL_siza7eza7_2681 = BgL_siza7eza7_8000;
														BgL_stackz00_2680 = BgL_stackz00_7999;
														BgL_regsz00_2679 = BgL_regsz00_7998;
														goto BgL_zc3z04anonymousza32146ze3z87_2682;
													}
												}
											}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &<@anonymous:2159> */
	obj_t BGl_z62zc3z04anonymousza32159ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5913, obj_t BgL_r1z00_5914, obj_t BgL_r2z00_5915)
	{
		{	/* SawMill/regalloc.scm 483 */
			{	/* SawMill/regalloc.scm 484 */
				bool_t BgL_tmpz00_8002;

				{	/* SawMill/regalloc.scm 484 */
					int BgL_arg2160z00_6087;
					int BgL_arg2161z00_6088;

					{	/* SawMill/regalloc.scm 591 */
						obj_t BgL_arg2263z00_6089;

						{
							BgL_rtl_regzf2razf2_bglt BgL_auxz00_8003;

							{
								obj_t BgL_auxz00_8004;

								{	/* SawMill/regalloc.scm 591 */
									BgL_objectz00_bglt BgL_tmpz00_8005;

									BgL_tmpz00_8005 =
										((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_r1z00_5914));
									BgL_auxz00_8004 = BGL_OBJECT_WIDENING(BgL_tmpz00_8005);
								}
								BgL_auxz00_8003 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8004);
							}
							BgL_arg2263z00_6089 =
								(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_8003))->
								BgL_interfere2z00);
						}
						BgL_arg2160z00_6087 =
							(((BgL_regsetz00_bglt) COBJECT(
									((BgL_regsetz00_bglt) BgL_arg2263z00_6089)))->BgL_lengthz00);
					}
					{	/* SawMill/regalloc.scm 591 */
						obj_t BgL_arg2263z00_6090;

						{
							BgL_rtl_regzf2razf2_bglt BgL_auxz00_8013;

							{
								obj_t BgL_auxz00_8014;

								{	/* SawMill/regalloc.scm 591 */
									BgL_objectz00_bglt BgL_tmpz00_8015;

									BgL_tmpz00_8015 =
										((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_r2z00_5915));
									BgL_auxz00_8014 = BGL_OBJECT_WIDENING(BgL_tmpz00_8015);
								}
								BgL_auxz00_8013 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8014);
							}
							BgL_arg2263z00_6090 =
								(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_8013))->
								BgL_interfere2z00);
						}
						BgL_arg2161z00_6088 =
							(((BgL_regsetz00_bglt) COBJECT(
									((BgL_regsetz00_bglt) BgL_arg2263z00_6090)))->BgL_lengthz00);
					}
					BgL_tmpz00_8002 =
						((long) (BgL_arg2160z00_6087) < (long) (BgL_arg2161z00_6088));
				}
				return BBOOL(BgL_tmpz00_8002);
			}
		}

	}



/* &<@anonymous:2173> */
	obj_t BGl_z62zc3z04anonymousza32173ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5916, obj_t BgL_rz00_5918)
	{
		{	/* SawMill/regalloc.scm 454 */
			{	/* SawMill/regalloc.scm 455 */
				obj_t BgL_colorsz00_5917;

				BgL_colorsz00_5917 =
					((obj_t) PROCEDURE_REF(BgL_envz00_5916, (int) (0L)));
				{	/* SawMill/regalloc.scm 456 */
					bool_t BgL_test3054z00_8030;

					{	/* SawMill/regalloc.scm 456 */
						obj_t BgL_arg2177z00_6091;

						{
							BgL_rtl_regzf2razf2_bglt BgL_auxz00_8031;

							{
								obj_t BgL_auxz00_8032;

								{	/* SawMill/regalloc.scm 456 */
									BgL_objectz00_bglt BgL_tmpz00_8033;

									BgL_tmpz00_8033 =
										((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_rz00_5918));
									BgL_auxz00_8032 = BGL_OBJECT_WIDENING(BgL_tmpz00_8033);
								}
								BgL_auxz00_8031 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8032);
							}
							BgL_arg2177z00_6091 =
								(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_8031))->
								BgL_colorz00);
						}
						BgL_test3054z00_8030 = INTEGERP(BgL_arg2177z00_6091);
					}
					if (BgL_test3054z00_8030)
						{	/* SawMill/regalloc.scm 457 */
							obj_t BgL_arg2176z00_6092;

							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_8040;

								{
									obj_t BgL_auxz00_8041;

									{	/* SawMill/regalloc.scm 457 */
										BgL_objectz00_bglt BgL_tmpz00_8042;

										BgL_tmpz00_8042 =
											((BgL_objectz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_rz00_5918));
										BgL_auxz00_8041 = BGL_OBJECT_WIDENING(BgL_tmpz00_8042);
									}
									BgL_auxz00_8040 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8041);
								}
								BgL_arg2176z00_6092 =
									(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_8040))->
									BgL_colorz00);
							}
							{	/* SawMill/regalloc.scm 457 */
								long BgL_kz00_6093;

								BgL_kz00_6093 = (long) CINT(BgL_arg2176z00_6092);
								return
									VECTOR_SET(
									((obj_t) BgL_colorsz00_5917), BgL_kz00_6093, BTRUE);
							}
						}
					else
						{	/* SawMill/regalloc.scm 456 */
							return BFALSE;
						}
				}
			}
		}

	}



/* register-hardware-coloring */
	obj_t
		BGl_registerzd2hardwarezd2coloringz00zzsaw_registerzd2allocationzd2(obj_t
		BgL_pregsz00_94, obj_t BgL_hregsz00_95, obj_t BgL_cregsz00_96)
	{
		{	/* SawMill/regalloc.scm 502 */
			{
				obj_t BgL_stackz00_2898;
				obj_t BgL_rz00_2864;
				obj_t BgL_regsz00_2842;
				obj_t BgL_regsz00_2828;
				int BgL_kz00_2829;

				{	/* SawMill/regalloc.scm 542 */
					long BgL_kz00_2799;

					BgL_kz00_2799 = bgl_list_length(BgL_hregsz00_95);
					{
						obj_t BgL_regsz00_2802;
						obj_t BgL_stackz00_2803;
						long BgL_sz00_2804;
						long BgL_nz00_2805;

						BgL_regsz00_2802 = BgL_cregsz00_96;
						BgL_stackz00_2803 = BNIL;
						BgL_sz00_2804 = 0L;
						BgL_nz00_2805 = 0L;
					BgL_zc3z04anonymousza32209ze3z87_2806:
						if (NULLP(BgL_regsz00_2802))
							{	/* SawMill/regalloc.scm 565 */
								BgL_stackz00_2898 = BgL_stackz00_2803;
								{
									obj_t BgL_l1634z00_2901;

									BgL_l1634z00_2901 = BgL_stackz00_2898;
								BgL_zc3z04anonymousza32259ze3z87_2902:
									if (PAIRP(BgL_l1634z00_2901))
										{	/* SawMill/regalloc.scm 541 */
											BgL_rz00_2864 = CAR(BgL_l1634z00_2901);
											{	/* SawMill/regalloc.scm 526 */
												obj_t BgL_nrz00_2867;

												{
													obj_t BgL_l1631z00_2875;

													BgL_l1631z00_2875 = BgL_hregsz00_95;
												BgL_zc3z04anonymousza32248ze3z87_2876:
													if (NULLP(BgL_l1631z00_2875))
														{	/* SawMill/regalloc.scm 526 */
															BgL_nrz00_2867 = BFALSE;
														}
													else
														{	/* SawMill/regalloc.scm 527 */
															obj_t BgL__ortest_1633z00_2878;

															{	/* SawMill/regalloc.scm 527 */
																obj_t BgL_regz00_2880;

																BgL_regz00_2880 =
																	CAR(((obj_t) BgL_l1631z00_2875));
																{	/* SawMill/regalloc.scm 527 */
																	bool_t BgL_test3058z00_8060;

																	{	/* SawMill/regalloc.scm 527 */
																		obj_t BgL_arg2257z00_2896;

																		{
																			BgL_rtl_regzf2razf2_bglt BgL_auxz00_8061;

																			{
																				obj_t BgL_auxz00_8062;

																				{	/* SawMill/regalloc.scm 527 */
																					BgL_objectz00_bglt BgL_tmpz00_8063;

																					BgL_tmpz00_8063 =
																						((BgL_objectz00_bglt)
																						((BgL_rtl_regz00_bglt)
																							BgL_rz00_2864));
																					BgL_auxz00_8062 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_8063);
																				}
																				BgL_auxz00_8061 =
																					((BgL_rtl_regzf2razf2_bglt)
																					BgL_auxz00_8062);
																			}
																			BgL_arg2257z00_2896 =
																				(((BgL_rtl_regzf2razf2_bglt)
																					COBJECT(BgL_auxz00_8061))->
																				BgL_interferez00);
																		}
																		{	/* SawMill/regalloc.scm 527 */
																			long BgL_basez00_4659;
																			long BgL_bitz00_4660;

																			{	/* SawMill/regalloc.scm 527 */
																				int BgL_arg2607z00_4661;

																				{
																					BgL_rtl_regzf2razf2_bglt
																						BgL_auxz00_8069;
																					{
																						obj_t BgL_auxz00_8070;

																						{	/* SawMill/regalloc.scm 527 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_8071;
																							BgL_tmpz00_8071 =
																								((BgL_objectz00_bglt) (
																									(BgL_rtl_regz00_bglt)
																									BgL_regz00_2880));
																							BgL_auxz00_8070 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_8071);
																						}
																						BgL_auxz00_8069 =
																							((BgL_rtl_regzf2razf2_bglt)
																							BgL_auxz00_8070);
																					}
																					BgL_arg2607z00_4661 =
																						(((BgL_rtl_regzf2razf2_bglt)
																							COBJECT(BgL_auxz00_8069))->
																						BgL_numz00);
																				}
																				BgL_basez00_4659 =
																					((long) (BgL_arg2607z00_4661) / 8L);
																			}
																			{	/* SawMill/regalloc.scm 527 */
																				int BgL_arg2608z00_4662;

																				{
																					BgL_rtl_regzf2razf2_bglt
																						BgL_auxz00_8079;
																					{
																						obj_t BgL_auxz00_8080;

																						{	/* SawMill/regalloc.scm 527 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_8081;
																							BgL_tmpz00_8081 =
																								((BgL_objectz00_bglt) (
																									(BgL_rtl_regz00_bglt)
																									BgL_regz00_2880));
																							BgL_auxz00_8080 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_8081);
																						}
																						BgL_auxz00_8079 =
																							((BgL_rtl_regzf2razf2_bglt)
																							BgL_auxz00_8080);
																					}
																					BgL_arg2608z00_4662 =
																						(((BgL_rtl_regzf2razf2_bglt)
																							COBJECT(BgL_auxz00_8079))->
																						BgL_numz00);
																				}
																				{	/* SawMill/regalloc.scm 527 */
																					long BgL_n1z00_4672;
																					long BgL_n2z00_4673;

																					BgL_n1z00_4672 =
																						(long) (BgL_arg2608z00_4662);
																					BgL_n2z00_4673 = 8L;
																					{	/* SawMill/regalloc.scm 527 */
																						bool_t BgL_test3059z00_8088;

																						{	/* SawMill/regalloc.scm 527 */
																							long BgL_arg1338z00_4675;

																							BgL_arg1338z00_4675 =
																								(((BgL_n1z00_4672) |
																									(BgL_n2z00_4673)) &
																								-2147483648);
																							BgL_test3059z00_8088 =
																								(BgL_arg1338z00_4675 == 0L);
																						}
																						if (BgL_test3059z00_8088)
																							{	/* SawMill/regalloc.scm 527 */
																								int32_t BgL_arg1334z00_4676;

																								{	/* SawMill/regalloc.scm 527 */
																									int32_t BgL_arg1336z00_4677;
																									int32_t BgL_arg1337z00_4678;

																									BgL_arg1336z00_4677 =
																										(int32_t) (BgL_n1z00_4672);
																									BgL_arg1337z00_4678 =
																										(int32_t) (BgL_n2z00_4673);
																									BgL_arg1334z00_4676 =
																										(BgL_arg1336z00_4677 %
																										BgL_arg1337z00_4678);
																								}
																								{	/* SawMill/regalloc.scm 527 */
																									long BgL_arg1446z00_4683;

																									BgL_arg1446z00_4683 =
																										(long)
																										(BgL_arg1334z00_4676);
																									BgL_bitz00_4660 =
																										(long)
																										(BgL_arg1446z00_4683);
																							}}
																						else
																							{	/* SawMill/regalloc.scm 527 */
																								BgL_bitz00_4660 =
																									(BgL_n1z00_4672 %
																									BgL_n2z00_4673);
																							}
																					}
																				}
																			}
																			if (
																				(BgL_basez00_4659 <
																					STRING_LENGTH(
																						(((BgL_regsetz00_bglt) COBJECT(
																									((BgL_regsetz00_bglt)
																										BgL_arg2257z00_2896)))->
																							BgL_stringz00))))
																				{	/* SawMill/regalloc.scm 527 */
																					BgL_test3058z00_8060 =
																						(
																						((STRING_REF(
																									(((BgL_regsetz00_bglt)
																											COBJECT((
																													(BgL_regsetz00_bglt)
																													BgL_arg2257z00_2896)))->
																										BgL_stringz00),
																									BgL_basez00_4659)) & (1L <<
																								(int) (BgL_bitz00_4660))) > 0L);
																				}
																			else
																				{	/* SawMill/regalloc.scm 527 */
																					BgL_test3058z00_8060 = ((bool_t) 0);
																				}
																		}
																	}
																	if (BgL_test3058z00_8060)
																		{	/* SawMill/regalloc.scm 527 */
																			BgL__ortest_1633z00_2878 = BFALSE;
																		}
																	else
																		{	/* SawMill/regalloc.scm 528 */
																			bool_t BgL_test3061z00_8110;

																			{	/* SawMill/regalloc.scm 528 */
																				obj_t BgL_g1629z00_2885;

																				{	/* SawMill/regalloc.scm 530 */
																					obj_t BgL_arg2256z00_2895;

																					{
																						BgL_rtl_regzf2razf2_bglt
																							BgL_auxz00_8111;
																						{
																							obj_t BgL_auxz00_8112;

																							{	/* SawMill/regalloc.scm 530 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_8113;
																								BgL_tmpz00_8113 =
																									((BgL_objectz00_bglt) (
																										(BgL_rtl_regz00_bglt)
																										BgL_rz00_2864));
																								BgL_auxz00_8112 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_8113);
																							}
																							BgL_auxz00_8111 =
																								((BgL_rtl_regzf2razf2_bglt)
																								BgL_auxz00_8112);
																						}
																						BgL_arg2256z00_2895 =
																							(((BgL_rtl_regzf2razf2_bglt)
																								COBJECT(BgL_auxz00_8111))->
																							BgL_interferez00);
																					}
																					BgL_g1629z00_2885 =
																						BGl_regsetzd2ze3listz31zzsaw_regsetz00
																						(((BgL_regsetz00_bglt)
																							BgL_arg2256z00_2895));
																				}
																				{
																					obj_t BgL_l1627z00_2887;

																					BgL_l1627z00_2887 = BgL_g1629z00_2885;
																				BgL_zc3z04anonymousza32252ze3z87_2888:
																					if (NULLP(BgL_l1627z00_2887))
																						{	/* SawMill/regalloc.scm 530 */
																							BgL_test3061z00_8110 =
																								((bool_t) 0);
																						}
																					else
																						{	/* SawMill/regalloc.scm 529 */
																							bool_t BgL__ortest_1630z00_2890;

																							{	/* SawMill/regalloc.scm 529 */
																								obj_t BgL_arg2255z00_2893;

																								{	/* SawMill/regalloc.scm 529 */
																									BgL_rtl_regz00_bglt
																										BgL_oz00_4701;
																									BgL_oz00_4701 =
																										((BgL_rtl_regz00_bglt)
																										CAR(((obj_t)
																												BgL_l1627z00_2887)));
																									{
																										BgL_rtl_regzf2razf2_bglt
																											BgL_auxz00_8126;
																										{
																											obj_t BgL_auxz00_8127;

																											{	/* SawMill/regalloc.scm 529 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_8128;
																												BgL_tmpz00_8128 =
																													((BgL_objectz00_bglt)
																													BgL_oz00_4701);
																												BgL_auxz00_8127 =
																													BGL_OBJECT_WIDENING
																													(BgL_tmpz00_8128);
																											}
																											BgL_auxz00_8126 =
																												(
																												(BgL_rtl_regzf2razf2_bglt)
																												BgL_auxz00_8127);
																										}
																										BgL_arg2255z00_2893 =
																											(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_8126))->BgL_colorz00);
																									}
																								}
																								BgL__ortest_1630z00_2890 =
																									(BgL_arg2255z00_2893 ==
																									BgL_regz00_2880);
																							}
																							if (BgL__ortest_1630z00_2890)
																								{	/* SawMill/regalloc.scm 530 */
																									BgL_test3061z00_8110 =
																										BgL__ortest_1630z00_2890;
																								}
																							else
																								{	/* SawMill/regalloc.scm 530 */
																									obj_t BgL_arg2254z00_2891;

																									BgL_arg2254z00_2891 =
																										CDR(
																										((obj_t)
																											BgL_l1627z00_2887));
																									{
																										obj_t BgL_l1627z00_8137;

																										BgL_l1627z00_8137 =
																											BgL_arg2254z00_2891;
																										BgL_l1627z00_2887 =
																											BgL_l1627z00_8137;
																										goto
																											BgL_zc3z04anonymousza32252ze3z87_2888;
																									}
																								}
																						}
																				}
																			}
																			if (BgL_test3061z00_8110)
																				{	/* SawMill/regalloc.scm 528 */
																					BgL__ortest_1633z00_2878 = BFALSE;
																				}
																			else
																				{	/* SawMill/regalloc.scm 531 */
																					bool_t BgL_test3064z00_8138;

																					{	/* SawMill/regalloc.scm 533 */
																						obj_t BgL_arg2251z00_2884;

																						{
																							BgL_rtl_regzf2razf2_bglt
																								BgL_auxz00_8139;
																							{
																								obj_t BgL_auxz00_8140;

																								{	/* SawMill/regalloc.scm 533 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_8141;
																									BgL_tmpz00_8141 =
																										((BgL_objectz00_bglt) (
																											(BgL_rtl_regz00_bglt)
																											BgL_regz00_2880));
																									BgL_auxz00_8140 =
																										BGL_OBJECT_WIDENING
																										(BgL_tmpz00_8141);
																								}
																								BgL_auxz00_8139 =
																									((BgL_rtl_regzf2razf2_bglt)
																									BgL_auxz00_8140);
																							}
																							BgL_arg2251z00_2884 =
																								(((BgL_rtl_regzf2razf2_bglt)
																									COBJECT(BgL_auxz00_8139))->
																								BgL_interfere2z00);
																						}
																						{	/* SawMill/regalloc.scm 531 */
																							long BgL_basez00_4709;
																							long BgL_bitz00_4710;

																							{	/* SawMill/regalloc.scm 531 */
																								int BgL_arg2607z00_4711;

																								{
																									BgL_rtl_regzf2razf2_bglt
																										BgL_auxz00_8147;
																									{
																										obj_t BgL_auxz00_8148;

																										{	/* SawMill/regalloc.scm 531 */
																											BgL_objectz00_bglt
																												BgL_tmpz00_8149;
																											BgL_tmpz00_8149 =
																												((BgL_objectz00_bglt) (
																													(BgL_rtl_regz00_bglt)
																													BgL_rz00_2864));
																											BgL_auxz00_8148 =
																												BGL_OBJECT_WIDENING
																												(BgL_tmpz00_8149);
																										}
																										BgL_auxz00_8147 =
																											(
																											(BgL_rtl_regzf2razf2_bglt)
																											BgL_auxz00_8148);
																									}
																									BgL_arg2607z00_4711 =
																										(((BgL_rtl_regzf2razf2_bglt)
																											COBJECT
																											(BgL_auxz00_8147))->
																										BgL_numz00);
																								}
																								BgL_basez00_4709 =
																									(
																									(long) (BgL_arg2607z00_4711) /
																									8L);
																							}
																							{	/* SawMill/regalloc.scm 531 */
																								int BgL_arg2608z00_4712;

																								{
																									BgL_rtl_regzf2razf2_bglt
																										BgL_auxz00_8157;
																									{
																										obj_t BgL_auxz00_8158;

																										{	/* SawMill/regalloc.scm 531 */
																											BgL_objectz00_bglt
																												BgL_tmpz00_8159;
																											BgL_tmpz00_8159 =
																												((BgL_objectz00_bglt) (
																													(BgL_rtl_regz00_bglt)
																													BgL_rz00_2864));
																											BgL_auxz00_8158 =
																												BGL_OBJECT_WIDENING
																												(BgL_tmpz00_8159);
																										}
																										BgL_auxz00_8157 =
																											(
																											(BgL_rtl_regzf2razf2_bglt)
																											BgL_auxz00_8158);
																									}
																									BgL_arg2608z00_4712 =
																										(((BgL_rtl_regzf2razf2_bglt)
																											COBJECT
																											(BgL_auxz00_8157))->
																										BgL_numz00);
																								}
																								{	/* SawMill/regalloc.scm 531 */
																									long BgL_n1z00_4722;
																									long BgL_n2z00_4723;

																									BgL_n1z00_4722 =
																										(long)
																										(BgL_arg2608z00_4712);
																									BgL_n2z00_4723 = 8L;
																									{	/* SawMill/regalloc.scm 531 */
																										bool_t BgL_test3065z00_8166;

																										{	/* SawMill/regalloc.scm 531 */
																											long BgL_arg1338z00_4725;

																											BgL_arg1338z00_4725 =
																												(((BgL_n1z00_4722) |
																													(BgL_n2z00_4723)) &
																												-2147483648);
																											BgL_test3065z00_8166 =
																												(BgL_arg1338z00_4725 ==
																												0L);
																										}
																										if (BgL_test3065z00_8166)
																											{	/* SawMill/regalloc.scm 531 */
																												int32_t
																													BgL_arg1334z00_4726;
																												{	/* SawMill/regalloc.scm 531 */
																													int32_t
																														BgL_arg1336z00_4727;
																													int32_t
																														BgL_arg1337z00_4728;
																													BgL_arg1336z00_4727 =
																														(int32_t)
																														(BgL_n1z00_4722);
																													BgL_arg1337z00_4728 =
																														(int32_t)
																														(BgL_n2z00_4723);
																													BgL_arg1334z00_4726 =
																														(BgL_arg1336z00_4727
																														%
																														BgL_arg1337z00_4728);
																												}
																												{	/* SawMill/regalloc.scm 531 */
																													long
																														BgL_arg1446z00_4733;
																													BgL_arg1446z00_4733 =
																														(long)
																														(BgL_arg1334z00_4726);
																													BgL_bitz00_4710 =
																														(long)
																														(BgL_arg1446z00_4733);
																											}}
																										else
																											{	/* SawMill/regalloc.scm 531 */
																												BgL_bitz00_4710 =
																													(BgL_n1z00_4722 %
																													BgL_n2z00_4723);
																											}
																									}
																								}
																							}
																							if (
																								(BgL_basez00_4709 <
																									STRING_LENGTH(
																										(((BgL_regsetz00_bglt)
																												COBJECT((
																														(BgL_regsetz00_bglt)
																														BgL_arg2251z00_2884)))->
																											BgL_stringz00))))
																								{	/* SawMill/regalloc.scm 531 */
																									BgL_test3064z00_8138 =
																										(
																										((STRING_REF(
																													(((BgL_regsetz00_bglt)
																															COBJECT((
																																	(BgL_regsetz00_bglt)
																																	BgL_arg2251z00_2884)))->
																														BgL_stringz00),
																													BgL_basez00_4709)) &
																											(1L <<
																												(int)
																												(BgL_bitz00_4710))) >
																										0L);
																								}
																							else
																								{	/* SawMill/regalloc.scm 531 */
																									BgL_test3064z00_8138 =
																										((bool_t) 0);
																								}
																						}
																					}
																					if (BgL_test3064z00_8138)
																						{	/* SawMill/regalloc.scm 531 */
																							BgL__ortest_1633z00_2878 = BFALSE;
																						}
																					else
																						{	/* SawMill/regalloc.scm 531 */
																							BgL__ortest_1633z00_2878 =
																								BgL_regz00_2880;
																						}
																				}
																		}
																}
															}
															if (CBOOL(BgL__ortest_1633z00_2878))
																{	/* SawMill/regalloc.scm 526 */
																	BgL_nrz00_2867 = BgL__ortest_1633z00_2878;
																}
															else
																{	/* SawMill/regalloc.scm 526 */
																	obj_t BgL_arg2250z00_2879;

																	BgL_arg2250z00_2879 =
																		CDR(((obj_t) BgL_l1631z00_2875));
																	{
																		obj_t BgL_l1631z00_8192;

																		BgL_l1631z00_8192 = BgL_arg2250z00_2879;
																		BgL_l1631z00_2875 = BgL_l1631z00_8192;
																		goto BgL_zc3z04anonymousza32248ze3z87_2876;
																	}
																}
														}
												}
												{	/* SawMill/regalloc.scm 536 */
													obj_t BgL_arg2245z00_2869;

													{
														BgL_rtl_regzf2razf2_bglt BgL_auxz00_8193;

														{
															obj_t BgL_auxz00_8194;

															{	/* SawMill/regalloc.scm 538 */
																BgL_objectz00_bglt BgL_tmpz00_8195;

																BgL_tmpz00_8195 =
																	((BgL_objectz00_bglt)
																	((BgL_rtl_regz00_bglt) BgL_rz00_2864));
																BgL_auxz00_8194 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_8195);
															}
															BgL_auxz00_8193 =
																((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8194);
														}
														BgL_arg2245z00_2869 =
															(((BgL_rtl_regzf2razf2_bglt)
																COBJECT(BgL_auxz00_8193))->BgL_interferez00);
													}
													{	/* SawMill/regalloc.scm 537 */
														obj_t BgL_zc3z04anonymousza32246ze3z87_5919;

														BgL_zc3z04anonymousza32246ze3z87_5919 =
															MAKE_FX_PROCEDURE
															(BGl_z62zc3z04anonymousza32246ze3ze5zzsaw_registerzd2allocationzd2,
															(int) (1L), (int) (1L));
														PROCEDURE_SET(BgL_zc3z04anonymousza32246ze3z87_5919,
															(int) (0L), BgL_nrz00_2867);
														BGl_regsetzd2forzd2eachz00zzsaw_regsetz00
															(BgL_zc3z04anonymousza32246ze3z87_5919,
															((BgL_regsetz00_bglt) BgL_arg2245z00_2869));
												}}
												{
													BgL_rtl_regzf2razf2_bglt BgL_auxz00_8208;

													{
														obj_t BgL_auxz00_8209;

														{	/* SawMill/regalloc.scm 539 */
															BgL_objectz00_bglt BgL_tmpz00_8210;

															BgL_tmpz00_8210 =
																((BgL_objectz00_bglt)
																((BgL_rtl_regz00_bglt) BgL_rz00_2864));
															BgL_auxz00_8209 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_8210);
														}
														BgL_auxz00_8208 =
															((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8209);
													}
													((((BgL_rtl_regzf2razf2_bglt)
																COBJECT(BgL_auxz00_8208))->BgL_colorz00) =
														((obj_t) BgL_nrz00_2867), BUNSPEC);
											}}
											{
												obj_t BgL_l1634z00_8217;

												BgL_l1634z00_8217 = CDR(BgL_l1634z00_2901);
												BgL_l1634z00_2901 = BgL_l1634z00_8217;
												goto BgL_zc3z04anonymousza32259ze3z87_2902;
											}
										}
									else
										{	/* SawMill/regalloc.scm 541 */
											((bool_t) 1);
										}
								}
								{	/* SawMill/regalloc.scm 570 */
									long BgL_arg2211z00_2808;

									BgL_arg2211z00_2808 = bgl_list_length(BgL_cregsz00_96);
									{	/* SawMill/regalloc.scm 568 */
										obj_t BgL_list2212z00_2809;

										{	/* SawMill/regalloc.scm 568 */
											obj_t BgL_arg2213z00_2810;

											{	/* SawMill/regalloc.scm 568 */
												obj_t BgL_arg2214z00_2811;

												{	/* SawMill/regalloc.scm 568 */
													obj_t BgL_arg2215z00_2812;

													{	/* SawMill/regalloc.scm 568 */
														obj_t BgL_arg2216z00_2813;

														{	/* SawMill/regalloc.scm 568 */
															obj_t BgL_arg2217z00_2814;

															{	/* SawMill/regalloc.scm 568 */
																obj_t BgL_arg2218z00_2815;

																{	/* SawMill/regalloc.scm 568 */
																	obj_t BgL_arg2219z00_2816;

																	{	/* SawMill/regalloc.scm 568 */
																		obj_t BgL_arg2220z00_2817;

																		{	/* SawMill/regalloc.scm 568 */
																			obj_t BgL_arg2221z00_2818;

																			BgL_arg2221z00_2818 =
																				MAKE_YOUNG_PAIR
																				(BGl_string2781z00zzsaw_registerzd2allocationzd2,
																				BNIL);
																			BgL_arg2220z00_2817 =
																				MAKE_YOUNG_PAIR
																				(BGl_string2802z00zzsaw_registerzd2allocationzd2,
																				BgL_arg2221z00_2818);
																		}
																		BgL_arg2219z00_2816 =
																			MAKE_YOUNG_PAIR(BINT(BgL_sz00_2804),
																			BgL_arg2220z00_2817);
																	}
																	BgL_arg2218z00_2815 =
																		MAKE_YOUNG_PAIR
																		(BGl_string2803z00zzsaw_registerzd2allocationzd2,
																		BgL_arg2219z00_2816);
																}
																BgL_arg2217z00_2814 =
																	MAKE_YOUNG_PAIR(BINT(BgL_nz00_2805),
																	BgL_arg2218z00_2815);
															}
															BgL_arg2216z00_2813 =
																MAKE_YOUNG_PAIR
																(BGl_string2804z00zzsaw_registerzd2allocationzd2,
																BgL_arg2217z00_2814);
														}
														BgL_arg2215z00_2812 =
															MAKE_YOUNG_PAIR(BINT(BgL_arg2211z00_2808),
															BgL_arg2216z00_2813);
													}
													BgL_arg2214z00_2811 =
														MAKE_YOUNG_PAIR
														(BGl_string2805z00zzsaw_registerzd2allocationzd2,
														BgL_arg2215z00_2812);
												}
												BgL_arg2213z00_2810 =
													MAKE_YOUNG_PAIR(BINT(BgL_kz00_2799),
													BgL_arg2214z00_2811);
											}
											BgL_list2212z00_2809 =
												MAKE_YOUNG_PAIR
												(BGl_string2806z00zzsaw_registerzd2allocationzd2,
												BgL_arg2213z00_2810);
										}
										return
											BGl_verbosez00zztools_speekz00(BINT(3L),
											BgL_list2212z00_2809);
									}
								}
							}
						else
							{	/* SawMill/regalloc.scm 573 */
								obj_t BgL_srz00_2819;

								BgL_regsz00_2828 = BgL_regsz00_2802;
								BgL_kz00_2829 = (int) (BgL_kz00_2799);
								{
									obj_t BgL_l1624z00_2832;

									BgL_l1624z00_2832 = BgL_regsz00_2828;
								BgL_zc3z04anonymousza32228ze3z87_2833:
									if (NULLP(BgL_l1624z00_2832))
										{	/* SawMill/regalloc.scm 504 */
											BgL_srz00_2819 = BFALSE;
										}
									else
										{	/* SawMill/regalloc.scm 505 */
											obj_t BgL__ortest_1626z00_2835;

											{	/* SawMill/regalloc.scm 505 */
												obj_t BgL_rz00_2837;

												BgL_rz00_2837 = CAR(((obj_t) BgL_l1624z00_2832));
												{	/* SawMill/regalloc.scm 505 */
													obj_t BgL__andtest_1228z00_2838;

													BgL__andtest_1228z00_2838 =
														(((BgL_rtl_regz00_bglt) COBJECT(
																((BgL_rtl_regz00_bglt) BgL_rz00_2837)))->
														BgL_hardwarez00);
													if (CBOOL(BgL__andtest_1228z00_2838))
														{	/* SawMill/regalloc.scm 505 */
															BgL__ortest_1626z00_2835 = BFALSE;
														}
													else
														{	/* SawMill/regalloc.scm 506 */
															bool_t BgL_test3070z00_8244;

															{	/* SawMill/regalloc.scm 506 */
																int BgL_arg2231z00_2840;

																{	/* SawMill/regalloc.scm 591 */
																	obj_t BgL_arg2263z00_4631;

																	{
																		BgL_rtl_regzf2razf2_bglt BgL_auxz00_8245;

																		{
																			obj_t BgL_auxz00_8246;

																			{	/* SawMill/regalloc.scm 591 */
																				BgL_objectz00_bglt BgL_tmpz00_8247;

																				BgL_tmpz00_8247 =
																					((BgL_objectz00_bglt)
																					((BgL_rtl_regz00_bglt)
																						BgL_rz00_2837));
																				BgL_auxz00_8246 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_8247);
																			}
																			BgL_auxz00_8245 =
																				((BgL_rtl_regzf2razf2_bglt)
																				BgL_auxz00_8246);
																		}
																		BgL_arg2263z00_4631 =
																			(((BgL_rtl_regzf2razf2_bglt)
																				COBJECT(BgL_auxz00_8245))->
																			BgL_interfere2z00);
																	}
																	BgL_arg2231z00_2840 =
																		(((BgL_regsetz00_bglt) COBJECT(
																				((BgL_regsetz00_bglt)
																					BgL_arg2263z00_4631)))->
																		BgL_lengthz00);
																}
																BgL_test3070z00_8244 =
																	(
																	(long) (BgL_arg2231z00_2840) <
																	(long) (BgL_kz00_2829));
															}
															if (BgL_test3070z00_8244)
																{	/* SawMill/regalloc.scm 506 */
																	BgL__ortest_1626z00_2835 = BgL_rz00_2837;
																}
															else
																{	/* SawMill/regalloc.scm 506 */
																	BgL__ortest_1626z00_2835 = BFALSE;
																}
														}
												}
											}
											if (CBOOL(BgL__ortest_1626z00_2835))
												{	/* SawMill/regalloc.scm 504 */
													BgL_srz00_2819 = BgL__ortest_1626z00_2835;
												}
											else
												{	/* SawMill/regalloc.scm 504 */
													obj_t BgL_arg2230z00_2836;

													BgL_arg2230z00_2836 =
														CDR(((obj_t) BgL_l1624z00_2832));
													{
														obj_t BgL_l1624z00_8262;

														BgL_l1624z00_8262 = BgL_arg2230z00_2836;
														BgL_l1624z00_2832 = BgL_l1624z00_8262;
														goto BgL_zc3z04anonymousza32228ze3z87_2833;
													}
												}
										}
								}
								if (CBOOL(BgL_srz00_2819))
									{	/* SawMill/regalloc.scm 574 */
										BGl_simplifyzd2regz12zc0zzsaw_registerzd2allocationzd2(
											((BgL_rtl_regz00_bglt) BgL_srz00_2819));
										{	/* SawMill/regalloc.scm 578 */
											obj_t BgL_arg2222z00_2820;
											obj_t BgL_arg2223z00_2821;
											long BgL_arg2224z00_2822;

											BgL_arg2222z00_2820 =
												bgl_remq(BgL_srz00_2819, BgL_regsz00_2802);
											BgL_arg2223z00_2821 =
												MAKE_YOUNG_PAIR(BgL_srz00_2819, BgL_stackz00_2803);
											BgL_arg2224z00_2822 = (1L + BgL_nz00_2805);
											{
												long BgL_nz00_8273;
												obj_t BgL_stackz00_8272;
												obj_t BgL_regsz00_8271;

												BgL_regsz00_8271 = BgL_arg2222z00_2820;
												BgL_stackz00_8272 = BgL_arg2223z00_2821;
												BgL_nz00_8273 = BgL_arg2224z00_2822;
												BgL_nz00_2805 = BgL_nz00_8273;
												BgL_stackz00_2803 = BgL_stackz00_8272;
												BgL_regsz00_2802 = BgL_regsz00_8271;
												goto BgL_zc3z04anonymousza32209ze3z87_2806;
											}
										}
									}
								else
									{	/* SawMill/regalloc.scm 580 */
										BgL_rtl_regz00_bglt BgL_rz00_2823;

										BgL_regsz00_2842 = BgL_regsz00_2802;
										{
											obj_t BgL_regz00_2857;

											{
												obj_t BgL_regsz00_2846;
												obj_t BgL_regz00_2847;
												double BgL_msz00_2848;

												{
													obj_t BgL_auxz00_8274;

													BgL_regsz00_2846 = BgL_regsz00_2842;
													BgL_regz00_2847 = BUNSPEC;
													BgL_msz00_2848 = ((double) 0.0);
												BgL_zc3z04anonymousza32233ze3z87_2849:
													if (NULLP(BgL_regsz00_2846))
														{	/* SawMill/regalloc.scm 517 */
															BgL_auxz00_8274 = BgL_regz00_2847;
														}
													else
														{	/* SawMill/regalloc.scm 519 */
															obj_t BgL_rz00_2851;

															BgL_rz00_2851 = CAR(((obj_t) BgL_regsz00_2846));
															{	/* SawMill/regalloc.scm 519 */
																double BgL_sz00_2852;

																BgL_regz00_2857 = BgL_rz00_2851;
																{	/* SawMill/regalloc.scm 512 */
																	double BgL_arg2239z00_2859;
																	double BgL_arg2240z00_2860;

																	{	/* SawMill/regalloc.scm 512 */
																		int BgL_arg2241z00_2861;

																		{
																			BgL_rtl_regzf2razf2_bglt BgL_auxz00_8279;

																			{
																				obj_t BgL_auxz00_8280;

																				{	/* SawMill/regalloc.scm 512 */
																					BgL_objectz00_bglt BgL_tmpz00_8281;

																					BgL_tmpz00_8281 =
																						((BgL_objectz00_bglt)
																						((BgL_rtl_regz00_bglt)
																							BgL_regz00_2857));
																					BgL_auxz00_8280 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_8281);
																				}
																				BgL_auxz00_8279 =
																					((BgL_rtl_regzf2razf2_bglt)
																					BgL_auxz00_8280);
																			}
																			BgL_arg2241z00_2861 =
																				(((BgL_rtl_regzf2razf2_bglt)
																					COBJECT(BgL_auxz00_8279))->
																				BgL_occurrencesz00);
																		}
																		BgL_arg2239z00_2859 =
																			(double) ((long) (BgL_arg2241z00_2861));
																	}
																	{	/* SawMill/regalloc.scm 513 */
																		int BgL_arg2242z00_2862;

																		{	/* SawMill/regalloc.scm 591 */
																			obj_t BgL_arg2263z00_4642;

																			{
																				BgL_rtl_regzf2razf2_bglt
																					BgL_auxz00_8289;
																				{
																					obj_t BgL_auxz00_8290;

																					{	/* SawMill/regalloc.scm 591 */
																						BgL_objectz00_bglt BgL_tmpz00_8291;

																						BgL_tmpz00_8291 =
																							((BgL_objectz00_bglt)
																							((BgL_rtl_regz00_bglt)
																								BgL_regz00_2857));
																						BgL_auxz00_8290 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_8291);
																					}
																					BgL_auxz00_8289 =
																						((BgL_rtl_regzf2razf2_bglt)
																						BgL_auxz00_8290);
																				}
																				BgL_arg2263z00_4642 =
																					(((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_8289))->
																					BgL_interfere2z00);
																			}
																			BgL_arg2242z00_2862 =
																				(((BgL_regsetz00_bglt) COBJECT(
																						((BgL_regsetz00_bglt)
																							BgL_arg2263z00_4642)))->
																				BgL_lengthz00);
																		}
																		BgL_arg2240z00_2860 =
																			(double) ((long) (BgL_arg2242z00_2862));
																	}
																	BgL_sz00_2852 =
																		(BgL_arg2239z00_2859 / BgL_arg2240z00_2860);
																}
																{	/* SawMill/regalloc.scm 520 */

																	if ((BgL_sz00_2852 > BgL_msz00_2848))
																		{	/* SawMill/regalloc.scm 522 */
																			obj_t BgL_arg2236z00_2854;

																			BgL_arg2236z00_2854 =
																				CDR(((obj_t) BgL_regsz00_2846));
																			{
																				double BgL_msz00_8308;
																				obj_t BgL_regz00_8307;
																				obj_t BgL_regsz00_8306;

																				BgL_regsz00_8306 = BgL_arg2236z00_2854;
																				BgL_regz00_8307 = BgL_rz00_2851;
																				BgL_msz00_8308 = BgL_sz00_2852;
																				BgL_msz00_2848 = BgL_msz00_8308;
																				BgL_regz00_2847 = BgL_regz00_8307;
																				BgL_regsz00_2846 = BgL_regsz00_8306;
																				goto
																					BgL_zc3z04anonymousza32233ze3z87_2849;
																			}
																		}
																	else
																		{	/* SawMill/regalloc.scm 523 */
																			obj_t BgL_arg2237z00_2855;

																			BgL_arg2237z00_2855 =
																				CDR(((obj_t) BgL_regsz00_2846));
																			{
																				obj_t BgL_regsz00_8311;

																				BgL_regsz00_8311 = BgL_arg2237z00_2855;
																				BgL_regsz00_2846 = BgL_regsz00_8311;
																				goto
																					BgL_zc3z04anonymousza32233ze3z87_2849;
																			}
																		}
																}
															}
														}
													BgL_rz00_2823 =
														((BgL_rtl_regz00_bglt) BgL_auxz00_8274);
												}
											}
										}
										{
											BgL_rtl_regzf2razf2_bglt BgL_auxz00_8313;

											{
												obj_t BgL_auxz00_8314;

												{	/* SawMill/regalloc.scm 582 */
													BgL_objectz00_bglt BgL_tmpz00_8315;

													BgL_tmpz00_8315 =
														((BgL_objectz00_bglt) BgL_rz00_2823);
													BgL_auxz00_8314 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_8315);
												}
												BgL_auxz00_8313 =
													((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8314);
											}
											((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_8313))->
													BgL_colorz00) = ((obj_t) BUNSPEC), BUNSPEC);
										}
										{	/* SawMill/regalloc.scm 583 */
											obj_t BgL_arg2225z00_2825;
											long BgL_arg2226z00_2826;

											BgL_arg2225z00_2825 =
												bgl_remq(((obj_t) BgL_rz00_2823), BgL_regsz00_2802);
											BgL_arg2226z00_2826 = (1L + BgL_sz00_2804);
											{
												long BgL_sz00_8324;
												obj_t BgL_regsz00_8323;

												BgL_regsz00_8323 = BgL_arg2225z00_2825;
												BgL_sz00_8324 = BgL_arg2226z00_2826;
												BgL_sz00_2804 = BgL_sz00_8324;
												BgL_regsz00_2802 = BgL_regsz00_8323;
												goto BgL_zc3z04anonymousza32209ze3z87_2806;
											}
										}
									}
							}
					}
				}
			}
		}

	}



/* &<@anonymous:2246> */
	obj_t BGl_z62zc3z04anonymousza32246ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5920, obj_t BgL_riz00_5922)
	{
		{	/* SawMill/regalloc.scm 536 */
			{	/* SawMill/regalloc.scm 537 */
				obj_t BgL_nrz00_5921;

				BgL_nrz00_5921 = PROCEDURE_REF(BgL_envz00_5920, (int) (0L));
				{	/* SawMill/regalloc.scm 537 */
					bool_t BgL_tmpz00_8327;

					{	/* SawMill/regalloc.scm 537 */
						obj_t BgL_arg2247z00_6094;

						{
							BgL_rtl_regzf2razf2_bglt BgL_auxz00_8328;

							{
								obj_t BgL_auxz00_8329;

								{	/* SawMill/regalloc.scm 537 */
									BgL_objectz00_bglt BgL_tmpz00_8330;

									BgL_tmpz00_8330 =
										((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_nrz00_5921));
									BgL_auxz00_8329 = BGL_OBJECT_WIDENING(BgL_tmpz00_8330);
								}
								BgL_auxz00_8328 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8329);
							}
							BgL_arg2247z00_6094 =
								(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_8328))->
								BgL_interfere2z00);
						}
						BgL_tmpz00_8327 =
							BGl_regsetzd2addz12zc0zzsaw_regsetz00(
							((BgL_regsetz00_bglt) BgL_arg2247z00_6094),
							((BgL_rtl_regz00_bglt) BgL_riz00_5922));
					}
					return BBOOL(BgL_tmpz00_8327);
				}
			}
		}

	}



/* simplify-reg! */
	obj_t
		BGl_simplifyzd2regz12zc0zzsaw_registerzd2allocationzd2(BgL_rtl_regz00_bglt
		BgL_rz00_98)
	{
		{	/* SawMill/regalloc.scm 599 */
			{	/* SawMill/regalloc.scm 601 */
				obj_t BgL_arg2265z00_2914;

				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_8340;

					{
						obj_t BgL_auxz00_8341;

						{	/* SawMill/regalloc.scm 601 */
							BgL_objectz00_bglt BgL_tmpz00_8342;

							BgL_tmpz00_8342 = ((BgL_objectz00_bglt) BgL_rz00_98);
							BgL_auxz00_8341 = BGL_OBJECT_WIDENING(BgL_tmpz00_8342);
						}
						BgL_auxz00_8340 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8341);
					}
					BgL_arg2265z00_2914 =
						(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_8340))->
						BgL_interfere2z00);
				}
				{	/* SawMill/regalloc.scm 602 */
					obj_t BgL_zc3z04anonymousza32266ze3z87_5923;

					BgL_zc3z04anonymousza32266ze3z87_5923 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza32266ze3ze5zzsaw_registerzd2allocationzd2,
						(int) (1L), (int) (2L));
					PROCEDURE_SET(BgL_zc3z04anonymousza32266ze3z87_5923, (int) (0L),
						((obj_t) BgL_rz00_98));
					PROCEDURE_SET(BgL_zc3z04anonymousza32266ze3z87_5923, (int) (1L),
						((obj_t) BgL_rz00_98));
					return
						BGl_regsetzd2forzd2eachz00zzsaw_regsetz00
						(BgL_zc3z04anonymousza32266ze3z87_5923,
						((BgL_regsetz00_bglt) BgL_arg2265z00_2914));
				}
			}
		}

	}



/* &<@anonymous:2266> */
	obj_t BGl_z62zc3z04anonymousza32266ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5924, obj_t BgL_r2z00_5927)
	{
		{	/* SawMill/regalloc.scm 601 */
			{	/* SawMill/regalloc.scm 602 */
				BgL_rtl_regz00_bglt BgL_rz00_5925;
				BgL_rtl_regz00_bglt BgL_i1236z00_5926;

				BgL_rz00_5925 =
					((BgL_rtl_regz00_bglt) PROCEDURE_REF(BgL_envz00_5924, (int) (0L)));
				BgL_i1236z00_5926 =
					((BgL_rtl_regz00_bglt) PROCEDURE_REF(BgL_envz00_5924, (int) (1L)));
				{	/* SawMill/regalloc.scm 602 */
					bool_t BgL_tmpz00_8364;

					{	/* SawMill/regalloc.scm 602 */
						obj_t BgL_arg2267z00_6095;

						{
							BgL_rtl_regzf2razf2_bglt BgL_auxz00_8365;

							{
								obj_t BgL_auxz00_8366;

								{	/* SawMill/regalloc.scm 602 */
									BgL_objectz00_bglt BgL_tmpz00_8367;

									BgL_tmpz00_8367 =
										((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_r2z00_5927));
									BgL_auxz00_8366 = BGL_OBJECT_WIDENING(BgL_tmpz00_8367);
								}
								BgL_auxz00_8365 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8366);
							}
							BgL_arg2267z00_6095 =
								(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_8365))->
								BgL_interfere2z00);
						}
						BGl_regsetzd2removez12zc0zzsaw_regsetz00(
							((BgL_regsetz00_bglt) BgL_arg2267z00_6095), BgL_rz00_5925);
					}
					{	/* SawMill/regalloc.scm 603 */
						obj_t BgL_arg2268z00_6096;

						{
							BgL_rtl_regzf2razf2_bglt BgL_auxz00_8375;

							{
								obj_t BgL_auxz00_8376;

								{	/* SawMill/regalloc.scm 603 */
									BgL_objectz00_bglt BgL_tmpz00_8377;

									BgL_tmpz00_8377 = ((BgL_objectz00_bglt) BgL_i1236z00_5926);
									BgL_auxz00_8376 = BGL_OBJECT_WIDENING(BgL_tmpz00_8377);
								}
								BgL_auxz00_8375 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8376);
							}
							BgL_arg2268z00_6096 =
								(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_8375))->
								BgL_interfere2z00);
						}
						BgL_tmpz00_8364 =
							BGl_regsetzd2removez12zc0zzsaw_regsetz00(
							((BgL_regsetz00_bglt) BgL_arg2268z00_6096),
							((BgL_rtl_regz00_bglt) BgL_r2z00_5927));
					}
					return BBOOL(BgL_tmpz00_8364);
				}
			}
		}

	}



/* rtl-map-registers! */
	obj_t BGl_rtlzd2mapzd2registersz12z12zzsaw_registerzd2allocationzd2(obj_t
		BgL_pregsz00_99, obj_t BgL_cregsz00_100, obj_t BgL_blocksz00_101)
	{
		{	/* SawMill/regalloc.scm 614 */
			{	/* SawMill/regalloc.scm 615 */
				obj_t BgL_colorsz00_2920;

				BgL_colorsz00_2920 =
					make_vector(
					(bgl_list_length(BgL_pregsz00_99) +
						bgl_list_length(BgL_cregsz00_100)), BUNSPEC);
				{	/* SawMill/regalloc.scm 621 */
					struct bgl_cell BgL_box3075_6048z00;
					obj_t BgL_mappingz00_6048;

					BgL_mappingz00_6048 = MAKE_CELL_STACK(BNIL, BgL_box3075_6048z00);
					{	/* SawMill/regalloc.scm 616 */
						obj_t BgL_g1650z00_2924;

						BgL_g1650z00_2924 =
							BGl_appendzd221011zd2zzsaw_registerzd2allocationzd2
							(BgL_cregsz00_100, BgL_pregsz00_99);
						{
							obj_t BgL_l1648z00_2926;

							BgL_l1648z00_2926 = BgL_g1650z00_2924;
						BgL_zc3z04anonymousza32269ze3z87_2927:
							if (PAIRP(BgL_l1648z00_2926))
								{	/* SawMill/regalloc.scm 620 */
									{	/* SawMill/regalloc.scm 617 */
										obj_t BgL_rz00_2929;

										BgL_rz00_2929 = CAR(BgL_l1648z00_2926);
										{	/* SawMill/regalloc.scm 618 */
											bool_t BgL_test3077z00_8394;

											{	/* SawMill/regalloc.scm 618 */
												obj_t BgL_arg2274z00_2934;

												{
													BgL_rtl_regzf2razf2_bglt BgL_auxz00_8395;

													{
														obj_t BgL_auxz00_8396;

														{	/* SawMill/regalloc.scm 618 */
															BgL_objectz00_bglt BgL_tmpz00_8397;

															BgL_tmpz00_8397 =
																((BgL_objectz00_bglt)
																((BgL_rtl_regz00_bglt) BgL_rz00_2929));
															BgL_auxz00_8396 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_8397);
														}
														BgL_auxz00_8395 =
															((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8396);
													}
													BgL_arg2274z00_2934 =
														(((BgL_rtl_regzf2razf2_bglt)
															COBJECT(BgL_auxz00_8395))->BgL_colorz00);
												}
												BgL_test3077z00_8394 = INTEGERP(BgL_arg2274z00_2934);
											}
											if (BgL_test3077z00_8394)
												{	/* SawMill/regalloc.scm 619 */
													obj_t BgL_arg2273z00_2933;

													{
														BgL_rtl_regzf2razf2_bglt BgL_auxz00_8404;

														{
															obj_t BgL_auxz00_8405;

															{	/* SawMill/regalloc.scm 619 */
																BgL_objectz00_bglt BgL_tmpz00_8406;

																BgL_tmpz00_8406 =
																	((BgL_objectz00_bglt)
																	((BgL_rtl_regz00_bglt) BgL_rz00_2929));
																BgL_auxz00_8405 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_8406);
															}
															BgL_auxz00_8404 =
																((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8405);
														}
														BgL_arg2273z00_2933 =
															(((BgL_rtl_regzf2razf2_bglt)
																COBJECT(BgL_auxz00_8404))->BgL_colorz00);
													}
													VECTOR_SET(BgL_colorsz00_2920,
														(long) CINT(BgL_arg2273z00_2933), BgL_rz00_2929);
												}
											else
												{	/* SawMill/regalloc.scm 618 */
													BFALSE;
												}
										}
									}
									{
										obj_t BgL_l1648z00_8414;

										BgL_l1648z00_8414 = CDR(BgL_l1648z00_2926);
										BgL_l1648z00_2926 = BgL_l1648z00_8414;
										goto BgL_zc3z04anonymousza32269ze3z87_2927;
									}
								}
							else
								{	/* SawMill/regalloc.scm 620 */
									((bool_t) 1);
								}
						}
					}
					{
						obj_t BgL_l1666z00_2938;

						BgL_l1666z00_2938 = BgL_blocksz00_101;
					BgL_zc3z04anonymousza32276ze3z87_2939:
						if (PAIRP(BgL_l1666z00_2938))
							{	/* SawMill/regalloc.scm 660 */
								{	/* SawMill/regalloc.scm 661 */
									obj_t BgL_bz00_2941;

									BgL_bz00_2941 = CAR(BgL_l1666z00_2938);
									{	/* SawMill/regalloc.scm 661 */
										obj_t BgL_g1665z00_2942;

										BgL_g1665z00_2942 =
											(((BgL_blockz00_bglt) COBJECT(
													((BgL_blockz00_bglt) BgL_bz00_2941)))->BgL_firstz00);
										{
											obj_t BgL_l1663z00_2944;

											BgL_l1663z00_2944 = BgL_g1665z00_2942;
										BgL_zc3z04anonymousza32278ze3z87_2945:
											if (PAIRP(BgL_l1663z00_2944))
												{	/* SawMill/regalloc.scm 667 */
													{	/* SawMill/regalloc.scm 662 */
														obj_t BgL_iz00_2947;

														BgL_iz00_2947 = CAR(BgL_l1663z00_2944);
														{	/* SawMill/regalloc.scm 663 */
															bool_t BgL_test3080z00_8424;

															{	/* SawMill/regalloc.scm 663 */
																obj_t BgL_arg2283z00_2952;

																BgL_arg2283z00_2952 =
																	(((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt)
																				((BgL_rtl_insz00_bglt)
																					BgL_iz00_2947))))->BgL_destz00);
																{	/* SawMill/regalloc.scm 663 */
																	obj_t BgL_classz00_4957;

																	BgL_classz00_4957 =
																		BGl_rtl_regzf2razf2zzsaw_regsetz00;
																	if (BGL_OBJECTP(BgL_arg2283z00_2952))
																		{	/* SawMill/regalloc.scm 663 */
																			BgL_objectz00_bglt BgL_arg1807z00_4959;

																			BgL_arg1807z00_4959 =
																				(BgL_objectz00_bglt)
																				(BgL_arg2283z00_2952);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* SawMill/regalloc.scm 663 */
																					long BgL_idxz00_4965;

																					BgL_idxz00_4965 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_4959);
																					BgL_test3080z00_8424 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_4965 + 2L)) ==
																						BgL_classz00_4957);
																				}
																			else
																				{	/* SawMill/regalloc.scm 663 */
																					bool_t BgL_res2745z00_4990;

																					{	/* SawMill/regalloc.scm 663 */
																						obj_t BgL_oclassz00_4973;

																						{	/* SawMill/regalloc.scm 663 */
																							obj_t BgL_arg1815z00_4981;
																							long BgL_arg1816z00_4982;

																							BgL_arg1815z00_4981 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* SawMill/regalloc.scm 663 */
																								long BgL_arg1817z00_4983;

																								BgL_arg1817z00_4983 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_4959);
																								BgL_arg1816z00_4982 =
																									(BgL_arg1817z00_4983 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_4973 =
																								VECTOR_REF(BgL_arg1815z00_4981,
																								BgL_arg1816z00_4982);
																						}
																						{	/* SawMill/regalloc.scm 663 */
																							bool_t BgL__ortest_1115z00_4974;

																							BgL__ortest_1115z00_4974 =
																								(BgL_classz00_4957 ==
																								BgL_oclassz00_4973);
																							if (BgL__ortest_1115z00_4974)
																								{	/* SawMill/regalloc.scm 663 */
																									BgL_res2745z00_4990 =
																										BgL__ortest_1115z00_4974;
																								}
																							else
																								{	/* SawMill/regalloc.scm 663 */
																									long BgL_odepthz00_4975;

																									{	/* SawMill/regalloc.scm 663 */
																										obj_t BgL_arg1804z00_4976;

																										BgL_arg1804z00_4976 =
																											(BgL_oclassz00_4973);
																										BgL_odepthz00_4975 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_4976);
																									}
																									if ((2L < BgL_odepthz00_4975))
																										{	/* SawMill/regalloc.scm 663 */
																											obj_t BgL_arg1802z00_4978;

																											{	/* SawMill/regalloc.scm 663 */
																												obj_t
																													BgL_arg1803z00_4979;
																												BgL_arg1803z00_4979 =
																													(BgL_oclassz00_4973);
																												BgL_arg1802z00_4978 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_4979,
																													2L);
																											}
																											BgL_res2745z00_4990 =
																												(BgL_arg1802z00_4978 ==
																												BgL_classz00_4957);
																										}
																									else
																										{	/* SawMill/regalloc.scm 663 */
																											BgL_res2745z00_4990 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test3080z00_8424 =
																						BgL_res2745z00_4990;
																				}
																		}
																	else
																		{	/* SawMill/regalloc.scm 663 */
																			BgL_test3080z00_8424 = ((bool_t) 0);
																		}
																}
															}
															if (BgL_test3080z00_8424)
																{
																	obj_t BgL_auxz00_8450;

																	{	/* SawMill/regalloc.scm 664 */
																		obj_t BgL_arg2282z00_2951;

																		BgL_arg2282z00_2951 =
																			(((BgL_rtl_insz00_bglt) COBJECT(
																					((BgL_rtl_insz00_bglt)
																						((BgL_rtl_insz00_bglt)
																							BgL_iz00_2947))))->BgL_destz00);
																		BgL_auxz00_8450 =
																			BGl_mapzd2registerze70z35zzsaw_registerzd2allocationzd2
																			(BgL_colorsz00_2920, BgL_mappingz00_6048,
																			BgL_arg2282z00_2951);
																	}
																	((((BgL_rtl_insz00_bglt) COBJECT(
																					((BgL_rtl_insz00_bglt)
																						((BgL_rtl_insz00_bglt)
																							BgL_iz00_2947))))->BgL_destz00) =
																		((obj_t) BgL_auxz00_8450), BUNSPEC);
																}
															else
																{	/* SawMill/regalloc.scm 663 */
																	BFALSE;
																}
														}
														{
															obj_t BgL_auxz00_8458;

															{	/* SawMill/regalloc.scm 665 */
																obj_t BgL_l1651z00_2953;

																BgL_l1651z00_2953 =
																	(((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt)
																				((BgL_rtl_insz00_bglt)
																					BgL_iz00_2947))))->BgL_z52spillz52);
																if (NULLP(BgL_l1651z00_2953))
																	{	/* SawMill/regalloc.scm 665 */
																		BgL_auxz00_8458 = BNIL;
																	}
																else
																	{	/* SawMill/regalloc.scm 665 */
																		obj_t BgL_head1653z00_2955;

																		{	/* SawMill/regalloc.scm 665 */
																			obj_t BgL_arg2290z00_2967;

																			{	/* SawMill/regalloc.scm 665 */
																				obj_t BgL_arg2291z00_2968;

																				BgL_arg2291z00_2968 =
																					CAR(BgL_l1651z00_2953);
																				BgL_arg2290z00_2967 =
																					BGl_mapzd2registerze70z35zzsaw_registerzd2allocationzd2
																					(BgL_colorsz00_2920,
																					BgL_mappingz00_6048,
																					BgL_arg2291z00_2968);
																			}
																			BgL_head1653z00_2955 =
																				MAKE_YOUNG_PAIR(BgL_arg2290z00_2967,
																				BNIL);
																		}
																		{	/* SawMill/regalloc.scm 665 */
																			obj_t BgL_g1656z00_2956;

																			BgL_g1656z00_2956 =
																				CDR(BgL_l1651z00_2953);
																			{
																				obj_t BgL_l1651z00_2958;
																				obj_t BgL_tail1654z00_2959;

																				BgL_l1651z00_2958 = BgL_g1656z00_2956;
																				BgL_tail1654z00_2959 =
																					BgL_head1653z00_2955;
																			BgL_zc3z04anonymousza32285ze3z87_2960:
																				if (NULLP(BgL_l1651z00_2958))
																					{	/* SawMill/regalloc.scm 665 */
																						BgL_auxz00_8458 =
																							BgL_head1653z00_2955;
																					}
																				else
																					{	/* SawMill/regalloc.scm 665 */
																						obj_t BgL_newtail1655z00_2962;

																						{	/* SawMill/regalloc.scm 665 */
																							obj_t BgL_arg2288z00_2964;

																							{	/* SawMill/regalloc.scm 665 */
																								obj_t BgL_arg2289z00_2965;

																								BgL_arg2289z00_2965 =
																									CAR(
																									((obj_t) BgL_l1651z00_2958));
																								BgL_arg2288z00_2964 =
																									BGl_mapzd2registerze70z35zzsaw_registerzd2allocationzd2
																									(BgL_colorsz00_2920,
																									BgL_mappingz00_6048,
																									BgL_arg2289z00_2965);
																							}
																							BgL_newtail1655z00_2962 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2288z00_2964, BNIL);
																						}
																						SET_CDR(BgL_tail1654z00_2959,
																							BgL_newtail1655z00_2962);
																						{	/* SawMill/regalloc.scm 665 */
																							obj_t BgL_arg2287z00_2963;

																							BgL_arg2287z00_2963 =
																								CDR(
																								((obj_t) BgL_l1651z00_2958));
																							{
																								obj_t BgL_tail1654z00_8480;
																								obj_t BgL_l1651z00_8479;

																								BgL_l1651z00_8479 =
																									BgL_arg2287z00_2963;
																								BgL_tail1654z00_8480 =
																									BgL_newtail1655z00_2962;
																								BgL_tail1654z00_2959 =
																									BgL_tail1654z00_8480;
																								BgL_l1651z00_2958 =
																									BgL_l1651z00_8479;
																								goto
																									BgL_zc3z04anonymousza32285ze3z87_2960;
																							}
																						}
																					}
																			}
																		}
																	}
															}
															((((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt)
																				((BgL_rtl_insz00_bglt)
																					BgL_iz00_2947))))->BgL_z52spillz52) =
																((obj_t) BgL_auxz00_8458), BUNSPEC);
														}
														{
															obj_t BgL_auxz00_8482;

															{	/* SawMill/regalloc.scm 666 */
																obj_t BgL_l1657z00_2969;

																BgL_l1657z00_2969 =
																	(((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt)
																				((BgL_rtl_insz00_bglt)
																					BgL_iz00_2947))))->BgL_argsz00);
																if (NULLP(BgL_l1657z00_2969))
																	{	/* SawMill/regalloc.scm 666 */
																		BgL_auxz00_8482 = BNIL;
																	}
																else
																	{	/* SawMill/regalloc.scm 666 */
																		obj_t BgL_head1659z00_2971;

																		{	/* SawMill/regalloc.scm 666 */
																			obj_t BgL_arg2298z00_2983;

																			{	/* SawMill/regalloc.scm 666 */
																				obj_t BgL_arg2299z00_2984;

																				BgL_arg2299z00_2984 =
																					CAR(BgL_l1657z00_2969);
																				BgL_arg2298z00_2983 =
																					BGl_mapzd2registerze70z35zzsaw_registerzd2allocationzd2
																					(BgL_colorsz00_2920,
																					BgL_mappingz00_6048,
																					BgL_arg2299z00_2984);
																			}
																			BgL_head1659z00_2971 =
																				MAKE_YOUNG_PAIR(BgL_arg2298z00_2983,
																				BNIL);
																		}
																		{	/* SawMill/regalloc.scm 666 */
																			obj_t BgL_g1662z00_2972;

																			BgL_g1662z00_2972 =
																				CDR(BgL_l1657z00_2969);
																			{
																				obj_t BgL_l1657z00_2974;
																				obj_t BgL_tail1660z00_2975;

																				BgL_l1657z00_2974 = BgL_g1662z00_2972;
																				BgL_tail1660z00_2975 =
																					BgL_head1659z00_2971;
																			BgL_zc3z04anonymousza32293ze3z87_2976:
																				if (NULLP(BgL_l1657z00_2974))
																					{	/* SawMill/regalloc.scm 666 */
																						BgL_auxz00_8482 =
																							BgL_head1659z00_2971;
																					}
																				else
																					{	/* SawMill/regalloc.scm 666 */
																						obj_t BgL_newtail1661z00_2978;

																						{	/* SawMill/regalloc.scm 666 */
																							obj_t BgL_arg2296z00_2980;

																							{	/* SawMill/regalloc.scm 666 */
																								obj_t BgL_arg2297z00_2981;

																								BgL_arg2297z00_2981 =
																									CAR(
																									((obj_t) BgL_l1657z00_2974));
																								BgL_arg2296z00_2980 =
																									BGl_mapzd2registerze70z35zzsaw_registerzd2allocationzd2
																									(BgL_colorsz00_2920,
																									BgL_mappingz00_6048,
																									BgL_arg2297z00_2981);
																							}
																							BgL_newtail1661z00_2978 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg2296z00_2980, BNIL);
																						}
																						SET_CDR(BgL_tail1660z00_2975,
																							BgL_newtail1661z00_2978);
																						{	/* SawMill/regalloc.scm 666 */
																							obj_t BgL_arg2295z00_2979;

																							BgL_arg2295z00_2979 =
																								CDR(
																								((obj_t) BgL_l1657z00_2974));
																							{
																								obj_t BgL_tail1660z00_8504;
																								obj_t BgL_l1657z00_8503;

																								BgL_l1657z00_8503 =
																									BgL_arg2295z00_2979;
																								BgL_tail1660z00_8504 =
																									BgL_newtail1661z00_2978;
																								BgL_tail1660z00_2975 =
																									BgL_tail1660z00_8504;
																								BgL_l1657z00_2974 =
																									BgL_l1657z00_8503;
																								goto
																									BgL_zc3z04anonymousza32293ze3z87_2976;
																							}
																						}
																					}
																			}
																		}
																	}
															}
															((((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt)
																				((BgL_rtl_insz00_bglt)
																					BgL_iz00_2947))))->BgL_argsz00) =
																((obj_t) BgL_auxz00_8482), BUNSPEC);
														}
													}
													{
														obj_t BgL_l1663z00_8506;

														BgL_l1663z00_8506 = CDR(BgL_l1663z00_2944);
														BgL_l1663z00_2944 = BgL_l1663z00_8506;
														goto BgL_zc3z04anonymousza32278ze3z87_2945;
													}
												}
											else
												{	/* SawMill/regalloc.scm 667 */
													((bool_t) 1);
												}
										}
									}
								}
								{
									obj_t BgL_l1666z00_8508;

									BgL_l1666z00_8508 = CDR(BgL_l1666z00_2938);
									BgL_l1666z00_2938 = BgL_l1666z00_8508;
									goto BgL_zc3z04anonymousza32276ze3z87_2939;
								}
							}
						else
							{	/* SawMill/regalloc.scm 660 */
								((bool_t) 1);
							}
					}
					return BgL_blocksz00_101;
				}
			}
		}

	}



/* map-register~0 */
	obj_t BGl_mapzd2registerze70z35zzsaw_registerzd2allocationzd2(obj_t
		BgL_colorsz00_6046, obj_t BgL_mappingz00_6045, obj_t BgL_oz00_3013)
	{
		{	/* SawMill/regalloc.scm 659 */
			{
				BgL_typez00_bglt BgL_tyz00_2989;
				BgL_rtl_regz00_bglt BgL_rz00_2990;

				{	/* SawMill/regalloc.scm 642 */
					bool_t BgL_test3089z00_8510;

					{	/* SawMill/regalloc.scm 642 */
						obj_t BgL_classz00_4828;

						BgL_classz00_4828 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
						if (BGL_OBJECTP(BgL_oz00_3013))
							{	/* SawMill/regalloc.scm 642 */
								BgL_objectz00_bglt BgL_arg1807z00_4830;

								BgL_arg1807z00_4830 = (BgL_objectz00_bglt) (BgL_oz00_3013);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawMill/regalloc.scm 642 */
										long BgL_idxz00_4836;

										BgL_idxz00_4836 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4830);
										BgL_test3089z00_8510 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4836 + 2L)) == BgL_classz00_4828);
									}
								else
									{	/* SawMill/regalloc.scm 642 */
										bool_t BgL_res2742z00_4861;

										{	/* SawMill/regalloc.scm 642 */
											obj_t BgL_oclassz00_4844;

											{	/* SawMill/regalloc.scm 642 */
												obj_t BgL_arg1815z00_4852;
												long BgL_arg1816z00_4853;

												BgL_arg1815z00_4852 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawMill/regalloc.scm 642 */
													long BgL_arg1817z00_4854;

													BgL_arg1817z00_4854 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4830);
													BgL_arg1816z00_4853 =
														(BgL_arg1817z00_4854 - OBJECT_TYPE);
												}
												BgL_oclassz00_4844 =
													VECTOR_REF(BgL_arg1815z00_4852, BgL_arg1816z00_4853);
											}
											{	/* SawMill/regalloc.scm 642 */
												bool_t BgL__ortest_1115z00_4845;

												BgL__ortest_1115z00_4845 =
													(BgL_classz00_4828 == BgL_oclassz00_4844);
												if (BgL__ortest_1115z00_4845)
													{	/* SawMill/regalloc.scm 642 */
														BgL_res2742z00_4861 = BgL__ortest_1115z00_4845;
													}
												else
													{	/* SawMill/regalloc.scm 642 */
														long BgL_odepthz00_4846;

														{	/* SawMill/regalloc.scm 642 */
															obj_t BgL_arg1804z00_4847;

															BgL_arg1804z00_4847 = (BgL_oclassz00_4844);
															BgL_odepthz00_4846 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4847);
														}
														if ((2L < BgL_odepthz00_4846))
															{	/* SawMill/regalloc.scm 642 */
																obj_t BgL_arg1802z00_4849;

																{	/* SawMill/regalloc.scm 642 */
																	obj_t BgL_arg1803z00_4850;

																	BgL_arg1803z00_4850 = (BgL_oclassz00_4844);
																	BgL_arg1802z00_4849 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4850,
																		2L);
																}
																BgL_res2742z00_4861 =
																	(BgL_arg1802z00_4849 == BgL_classz00_4828);
															}
														else
															{	/* SawMill/regalloc.scm 642 */
																BgL_res2742z00_4861 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test3089z00_8510 = BgL_res2742z00_4861;
									}
							}
						else
							{	/* SawMill/regalloc.scm 642 */
								BgL_test3089z00_8510 = ((bool_t) 0);
							}
					}
					if (BgL_test3089z00_8510)
						{	/* SawMill/regalloc.scm 644 */
							BgL_typez00_bglt BgL_arg2312z00_3016;
							obj_t BgL_arg2313z00_3017;

							BgL_arg2312z00_3016 =
								(((BgL_rtl_regz00_bglt) COBJECT(
										((BgL_rtl_regz00_bglt) BgL_oz00_3013)))->BgL_typez00);
							{	/* SawMill/regalloc.scm 647 */
								bool_t BgL_test3094z00_8535;

								{	/* SawMill/regalloc.scm 647 */
									obj_t BgL_arg2320z00_3025;

									{
										BgL_rtl_regzf2razf2_bglt BgL_auxz00_8536;

										{
											obj_t BgL_auxz00_8537;

											{	/* SawMill/regalloc.scm 647 */
												BgL_objectz00_bglt BgL_tmpz00_8538;

												BgL_tmpz00_8538 =
													((BgL_objectz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_oz00_3013));
												BgL_auxz00_8537 = BGL_OBJECT_WIDENING(BgL_tmpz00_8538);
											}
											BgL_auxz00_8536 =
												((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8537);
										}
										BgL_arg2320z00_3025 =
											(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_8536))->
											BgL_colorz00);
									}
									BgL_test3094z00_8535 = INTEGERP(BgL_arg2320z00_3025);
								}
								if (BgL_test3094z00_8535)
									{	/* SawMill/regalloc.scm 648 */
										obj_t BgL_arg2316z00_3021;

										{
											BgL_rtl_regzf2razf2_bglt BgL_auxz00_8545;

											{
												obj_t BgL_auxz00_8546;

												{	/* SawMill/regalloc.scm 648 */
													BgL_objectz00_bglt BgL_tmpz00_8547;

													BgL_tmpz00_8547 =
														((BgL_objectz00_bglt)
														((BgL_rtl_regz00_bglt) BgL_oz00_3013));
													BgL_auxz00_8546 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_8547);
												}
												BgL_auxz00_8545 =
													((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8546);
											}
											BgL_arg2316z00_3021 =
												(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_8545))->
												BgL_colorz00);
										}
										BgL_arg2313z00_3017 =
											VECTOR_REF(BgL_colorsz00_6046,
											(long) CINT(BgL_arg2316z00_3021));
									}
								else
									{	/* SawMill/regalloc.scm 649 */
										bool_t BgL_test3095z00_8555;

										{	/* SawMill/regalloc.scm 649 */
											obj_t BgL_arg2319z00_3024;

											{
												BgL_rtl_regzf2razf2_bglt BgL_auxz00_8556;

												{
													obj_t BgL_auxz00_8557;

													{	/* SawMill/regalloc.scm 649 */
														BgL_objectz00_bglt BgL_tmpz00_8558;

														BgL_tmpz00_8558 =
															((BgL_objectz00_bglt)
															((BgL_rtl_regz00_bglt) BgL_oz00_3013));
														BgL_auxz00_8557 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_8558);
													}
													BgL_auxz00_8556 =
														((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8557);
												}
												BgL_arg2319z00_3024 =
													(((BgL_rtl_regzf2razf2_bglt)
														COBJECT(BgL_auxz00_8556))->BgL_colorz00);
											}
											{	/* SawMill/regalloc.scm 649 */
												obj_t BgL_classz00_4868;

												BgL_classz00_4868 = BGl_rtl_regz00zzsaw_defsz00;
												if (BGL_OBJECTP(BgL_arg2319z00_3024))
													{	/* SawMill/regalloc.scm 649 */
														BgL_objectz00_bglt BgL_arg1807z00_4870;

														BgL_arg1807z00_4870 =
															(BgL_objectz00_bglt) (BgL_arg2319z00_3024);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawMill/regalloc.scm 649 */
																long BgL_idxz00_4876;

																BgL_idxz00_4876 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_4870);
																BgL_test3095z00_8555 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_4876 + 1L)) ==
																	BgL_classz00_4868);
															}
														else
															{	/* SawMill/regalloc.scm 649 */
																bool_t BgL_res2743z00_4901;

																{	/* SawMill/regalloc.scm 649 */
																	obj_t BgL_oclassz00_4884;

																	{	/* SawMill/regalloc.scm 649 */
																		obj_t BgL_arg1815z00_4892;
																		long BgL_arg1816z00_4893;

																		BgL_arg1815z00_4892 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawMill/regalloc.scm 649 */
																			long BgL_arg1817z00_4894;

																			BgL_arg1817z00_4894 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_4870);
																			BgL_arg1816z00_4893 =
																				(BgL_arg1817z00_4894 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_4884 =
																			VECTOR_REF(BgL_arg1815z00_4892,
																			BgL_arg1816z00_4893);
																	}
																	{	/* SawMill/regalloc.scm 649 */
																		bool_t BgL__ortest_1115z00_4885;

																		BgL__ortest_1115z00_4885 =
																			(BgL_classz00_4868 == BgL_oclassz00_4884);
																		if (BgL__ortest_1115z00_4885)
																			{	/* SawMill/regalloc.scm 649 */
																				BgL_res2743z00_4901 =
																					BgL__ortest_1115z00_4885;
																			}
																		else
																			{	/* SawMill/regalloc.scm 649 */
																				long BgL_odepthz00_4886;

																				{	/* SawMill/regalloc.scm 649 */
																					obj_t BgL_arg1804z00_4887;

																					BgL_arg1804z00_4887 =
																						(BgL_oclassz00_4884);
																					BgL_odepthz00_4886 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_4887);
																				}
																				if ((1L < BgL_odepthz00_4886))
																					{	/* SawMill/regalloc.scm 649 */
																						obj_t BgL_arg1802z00_4889;

																						{	/* SawMill/regalloc.scm 649 */
																							obj_t BgL_arg1803z00_4890;

																							BgL_arg1803z00_4890 =
																								(BgL_oclassz00_4884);
																							BgL_arg1802z00_4889 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_4890, 1L);
																						}
																						BgL_res2743z00_4901 =
																							(BgL_arg1802z00_4889 ==
																							BgL_classz00_4868);
																					}
																				else
																					{	/* SawMill/regalloc.scm 649 */
																						BgL_res2743z00_4901 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test3095z00_8555 = BgL_res2743z00_4901;
															}
													}
												else
													{	/* SawMill/regalloc.scm 649 */
														BgL_test3095z00_8555 = ((bool_t) 0);
													}
											}
										}
										if (BgL_test3095z00_8555)
											{
												BgL_rtl_regzf2razf2_bglt BgL_auxz00_8586;

												{
													obj_t BgL_auxz00_8587;

													{	/* SawMill/regalloc.scm 650 */
														BgL_objectz00_bglt BgL_tmpz00_8588;

														BgL_tmpz00_8588 =
															((BgL_objectz00_bglt)
															((BgL_rtl_regz00_bglt) BgL_oz00_3013));
														BgL_auxz00_8587 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_8588);
													}
													BgL_auxz00_8586 =
														((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_8587);
												}
												BgL_arg2313z00_3017 =
													(((BgL_rtl_regzf2razf2_bglt)
														COBJECT(BgL_auxz00_8586))->BgL_colorz00);
											}
										else
											{	/* SawMill/regalloc.scm 649 */
												BgL_arg2313z00_3017 = BgL_oz00_3013;
											}
									}
							}
							BgL_tyz00_2989 = BgL_arg2312z00_3016;
							BgL_rz00_2990 = ((BgL_rtl_regz00_bglt) BgL_arg2313z00_3017);
							{	/* SawMill/regalloc.scm 624 */
								bool_t BgL_test3100z00_8594;

								{	/* SawMill/regalloc.scm 624 */
									obj_t BgL_classz00_4772;

									BgL_classz00_4772 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
									{	/* SawMill/regalloc.scm 624 */
										BgL_objectz00_bglt BgL_arg1807z00_4774;

										{	/* SawMill/regalloc.scm 624 */
											obj_t BgL_tmpz00_8595;

											BgL_tmpz00_8595 = ((obj_t) BgL_rz00_2990);
											BgL_arg1807z00_4774 =
												(BgL_objectz00_bglt) (BgL_tmpz00_8595);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawMill/regalloc.scm 624 */
												long BgL_idxz00_4780;

												BgL_idxz00_4780 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4774);
												BgL_test3100z00_8594 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4780 + 2L)) == BgL_classz00_4772);
											}
										else
											{	/* SawMill/regalloc.scm 624 */
												bool_t BgL_res2741z00_4805;

												{	/* SawMill/regalloc.scm 624 */
													obj_t BgL_oclassz00_4788;

													{	/* SawMill/regalloc.scm 624 */
														obj_t BgL_arg1815z00_4796;
														long BgL_arg1816z00_4797;

														BgL_arg1815z00_4796 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawMill/regalloc.scm 624 */
															long BgL_arg1817z00_4798;

															BgL_arg1817z00_4798 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4774);
															BgL_arg1816z00_4797 =
																(BgL_arg1817z00_4798 - OBJECT_TYPE);
														}
														BgL_oclassz00_4788 =
															VECTOR_REF(BgL_arg1815z00_4796,
															BgL_arg1816z00_4797);
													}
													{	/* SawMill/regalloc.scm 624 */
														bool_t BgL__ortest_1115z00_4789;

														BgL__ortest_1115z00_4789 =
															(BgL_classz00_4772 == BgL_oclassz00_4788);
														if (BgL__ortest_1115z00_4789)
															{	/* SawMill/regalloc.scm 624 */
																BgL_res2741z00_4805 = BgL__ortest_1115z00_4789;
															}
														else
															{	/* SawMill/regalloc.scm 624 */
																long BgL_odepthz00_4790;

																{	/* SawMill/regalloc.scm 624 */
																	obj_t BgL_arg1804z00_4791;

																	BgL_arg1804z00_4791 = (BgL_oclassz00_4788);
																	BgL_odepthz00_4790 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4791);
																}
																if ((2L < BgL_odepthz00_4790))
																	{	/* SawMill/regalloc.scm 624 */
																		obj_t BgL_arg1802z00_4793;

																		{	/* SawMill/regalloc.scm 624 */
																			obj_t BgL_arg1803z00_4794;

																			BgL_arg1803z00_4794 =
																				(BgL_oclassz00_4788);
																			BgL_arg1802z00_4793 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4794, 2L);
																		}
																		BgL_res2741z00_4805 =
																			(BgL_arg1802z00_4793 ==
																			BgL_classz00_4772);
																	}
																else
																	{	/* SawMill/regalloc.scm 624 */
																		BgL_res2741z00_4805 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test3100z00_8594 = BgL_res2741z00_4805;
											}
									}
								}
								if (BgL_test3100z00_8594)
									{	/* SawMill/regalloc.scm 624 */
										if (CBOOL(
												(((BgL_rtl_regz00_bglt) COBJECT(BgL_rz00_2990))->
													BgL_hardwarez00)))
											{	/* SawMill/regalloc.scm 626 */
												if (
													(((obj_t) BgL_tyz00_2989) ==
														BGl_za2objza2z00zztype_cachez00))
													{	/* SawMill/regalloc.scm 628 */
														return ((obj_t) BgL_rz00_2990);
													}
												else
													{	/* SawMill/regalloc.scm 631 */
														obj_t BgL_kz00_2994;

														BgL_kz00_2994 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_rz00_2990),
															((obj_t) BgL_tyz00_2989));
														{	/* SawMill/regalloc.scm 632 */
															obj_t BgL_cz00_2995;

															BgL_cz00_2995 =
																BGl_assocz00zz__r4_pairs_and_lists_6_3z00
																(BgL_kz00_2994,
																((obj_t) ((obj_t)
																		CELL_REF(BgL_mappingz00_6045))));
															if (PAIRP(BgL_cz00_2995))
																{	/* SawMill/regalloc.scm 633 */
																	return CDR(BgL_cz00_2995);
																}
															else
																{	/* SawMill/regalloc.scm 635 */
																	BgL_rtl_regz00_bglt BgL_newrz00_2997;

																	{	/* SawMill/regalloc.scm 635 */
																		BgL_rtl_regz00_bglt BgL_new1238z00_3000;

																		{	/* SawMill/regalloc.scm 637 */
																			BgL_rtl_regz00_bglt BgL_tmp1248z00_3008;
																			BgL_rtl_regzf2razf2_bglt
																				BgL_wide1249z00_3009;
																			{
																				BgL_rtl_regz00_bglt BgL_auxz00_8632;

																				{	/* SawMill/regalloc.scm 637 */
																					BgL_rtl_regz00_bglt
																						BgL_new1247z00_3011;
																					BgL_new1247z00_3011 =
																						((BgL_rtl_regz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_rtl_regz00_bgl))));
																					{	/* SawMill/regalloc.scm 637 */
																						long BgL_arg2309z00_3012;

																						BgL_arg2309z00_3012 =
																							BGL_CLASS_NUM
																							(BGl_rtl_regz00zzsaw_defsz00);
																						BGL_OBJECT_CLASS_NUM_SET((
																								(BgL_objectz00_bglt)
																								BgL_new1247z00_3011),
																							BgL_arg2309z00_3012);
																					}
																					{	/* SawMill/regalloc.scm 637 */
																						BgL_objectz00_bglt BgL_tmpz00_8637;

																						BgL_tmpz00_8637 =
																							((BgL_objectz00_bglt)
																							BgL_new1247z00_3011);
																						BGL_OBJECT_WIDENING_SET
																							(BgL_tmpz00_8637, BFALSE);
																					}
																					((BgL_objectz00_bglt)
																						BgL_new1247z00_3011);
																					BgL_auxz00_8632 = BgL_new1247z00_3011;
																				}
																				BgL_tmp1248z00_3008 =
																					((BgL_rtl_regz00_bglt)
																					BgL_auxz00_8632);
																			}
																			BgL_wide1249z00_3009 =
																				((BgL_rtl_regzf2razf2_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_rtl_regzf2razf2_bgl))));
																			{	/* SawMill/regalloc.scm 637 */
																				obj_t BgL_auxz00_8645;
																				BgL_objectz00_bglt BgL_tmpz00_8643;

																				BgL_auxz00_8645 =
																					((obj_t) BgL_wide1249z00_3009);
																				BgL_tmpz00_8643 =
																					((BgL_objectz00_bglt)
																					BgL_tmp1248z00_3008);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8643,
																					BgL_auxz00_8645);
																			}
																			((BgL_objectz00_bglt)
																				BgL_tmp1248z00_3008);
																			{	/* SawMill/regalloc.scm 637 */
																				long BgL_arg2308z00_3010;

																				BgL_arg2308z00_3010 =
																					BGL_CLASS_NUM
																					(BGl_rtl_regzf2razf2zzsaw_regsetz00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_tmp1248z00_3008),
																					BgL_arg2308z00_3010);
																			}
																			BgL_new1238z00_3000 =
																				((BgL_rtl_regz00_bglt)
																				BgL_tmp1248z00_3008);
																		}
																		((((BgL_rtl_regz00_bglt) COBJECT(
																						((BgL_rtl_regz00_bglt)
																							BgL_new1238z00_3000)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) BgL_tyz00_2989),
																			BUNSPEC);
																		((((BgL_rtl_regz00_bglt)
																					COBJECT(((BgL_rtl_regz00_bglt)
																							BgL_new1238z00_3000)))->
																				BgL_varz00) =
																			((obj_t) (((BgL_rtl_regz00_bglt)
																						COBJECT(BgL_rz00_2990))->
																					BgL_varz00)), BUNSPEC);
																		((((BgL_rtl_regz00_bglt)
																					COBJECT(((BgL_rtl_regz00_bglt)
																							BgL_new1238z00_3000)))->
																				BgL_onexprzf3zf3) =
																			((obj_t) (((BgL_rtl_regz00_bglt)
																						COBJECT(BgL_rz00_2990))->
																					BgL_onexprzf3zf3)), BUNSPEC);
																		((((BgL_rtl_regz00_bglt)
																					COBJECT(((BgL_rtl_regz00_bglt)
																							BgL_new1238z00_3000)))->
																				BgL_namez00) =
																			((obj_t) (((BgL_rtl_regz00_bglt)
																						COBJECT(BgL_rz00_2990))->
																					BgL_namez00)), BUNSPEC);
																		{
																			obj_t BgL_auxz00_8664;

																			{	/* SawMill/regalloc.scm 636 */

																				{	/* SawMill/regalloc.scm 636 */

																					BgL_auxz00_8664 =
																						BGl_gensymz00zz__r4_symbols_6_4z00
																						(BFALSE);
																			}}
																			((((BgL_rtl_regz00_bglt) COBJECT(
																							((BgL_rtl_regz00_bglt)
																								BgL_new1238z00_3000)))->
																					BgL_keyz00) =
																				((obj_t) BgL_auxz00_8664), BUNSPEC);
																		}
																		((((BgL_rtl_regz00_bglt) COBJECT(
																						((BgL_rtl_regz00_bglt)
																							BgL_new1238z00_3000)))->
																				BgL_debugnamez00) =
																			((obj_t) (((BgL_rtl_regz00_bglt)
																						COBJECT(BgL_rz00_2990))->
																					BgL_debugnamez00)), BUNSPEC);
																		((((BgL_rtl_regz00_bglt)
																					COBJECT(((BgL_rtl_regz00_bglt)
																							BgL_new1238z00_3000)))->
																				BgL_hardwarez00) =
																			((obj_t) (((BgL_rtl_regz00_bglt)
																						COBJECT(BgL_rz00_2990))->
																					BgL_hardwarez00)), BUNSPEC);
																		{
																			int BgL_auxz00_8680;
																			BgL_rtl_regzf2razf2_bglt BgL_auxz00_8674;

																			{
																				BgL_rtl_regzf2razf2_bglt
																					BgL_auxz00_8681;
																				{
																					obj_t BgL_auxz00_8682;

																					{	/* SawMill/regalloc.scm 636 */
																						BgL_objectz00_bglt BgL_tmpz00_8683;

																						BgL_tmpz00_8683 =
																							((BgL_objectz00_bglt)
																							((BgL_rtl_regz00_bglt)
																								BgL_rz00_2990));
																						BgL_auxz00_8682 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_8683);
																					}
																					BgL_auxz00_8681 =
																						((BgL_rtl_regzf2razf2_bglt)
																						BgL_auxz00_8682);
																				}
																				BgL_auxz00_8680 =
																					(((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_8681))->
																					BgL_numz00);
																			}
																			{
																				obj_t BgL_auxz00_8675;

																				{	/* SawMill/regalloc.scm 636 */
																					BgL_objectz00_bglt BgL_tmpz00_8676;

																					BgL_tmpz00_8676 =
																						((BgL_objectz00_bglt)
																						BgL_new1238z00_3000);
																					BgL_auxz00_8675 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_8676);
																				}
																				BgL_auxz00_8674 =
																					((BgL_rtl_regzf2razf2_bglt)
																					BgL_auxz00_8675);
																			}
																			((((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_8674))->
																					BgL_numz00) =
																				((int) BgL_auxz00_8680), BUNSPEC);
																		}
																		{
																			obj_t BgL_auxz00_8696;
																			BgL_rtl_regzf2razf2_bglt BgL_auxz00_8690;

																			{
																				BgL_rtl_regzf2razf2_bglt
																					BgL_auxz00_8697;
																				{
																					obj_t BgL_auxz00_8698;

																					{	/* SawMill/regalloc.scm 636 */
																						BgL_objectz00_bglt BgL_tmpz00_8699;

																						BgL_tmpz00_8699 =
																							((BgL_objectz00_bglt)
																							((BgL_rtl_regz00_bglt)
																								BgL_rz00_2990));
																						BgL_auxz00_8698 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_8699);
																					}
																					BgL_auxz00_8697 =
																						((BgL_rtl_regzf2razf2_bglt)
																						BgL_auxz00_8698);
																				}
																				BgL_auxz00_8696 =
																					(((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_8697))->
																					BgL_colorz00);
																			}
																			{
																				obj_t BgL_auxz00_8691;

																				{	/* SawMill/regalloc.scm 636 */
																					BgL_objectz00_bglt BgL_tmpz00_8692;

																					BgL_tmpz00_8692 =
																						((BgL_objectz00_bglt)
																						BgL_new1238z00_3000);
																					BgL_auxz00_8691 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_8692);
																				}
																				BgL_auxz00_8690 =
																					((BgL_rtl_regzf2razf2_bglt)
																					BgL_auxz00_8691);
																			}
																			((((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_8690))->
																					BgL_colorz00) =
																				((obj_t) BgL_auxz00_8696), BUNSPEC);
																		}
																		{
																			obj_t BgL_auxz00_8712;
																			BgL_rtl_regzf2razf2_bglt BgL_auxz00_8706;

																			{
																				BgL_rtl_regzf2razf2_bglt
																					BgL_auxz00_8713;
																				{
																					obj_t BgL_auxz00_8714;

																					{	/* SawMill/regalloc.scm 636 */
																						BgL_objectz00_bglt BgL_tmpz00_8715;

																						BgL_tmpz00_8715 =
																							((BgL_objectz00_bglt)
																							((BgL_rtl_regz00_bglt)
																								BgL_rz00_2990));
																						BgL_auxz00_8714 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_8715);
																					}
																					BgL_auxz00_8713 =
																						((BgL_rtl_regzf2razf2_bglt)
																						BgL_auxz00_8714);
																				}
																				BgL_auxz00_8712 =
																					(((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_8713))->
																					BgL_coalescez00);
																			}
																			{
																				obj_t BgL_auxz00_8707;

																				{	/* SawMill/regalloc.scm 636 */
																					BgL_objectz00_bglt BgL_tmpz00_8708;

																					BgL_tmpz00_8708 =
																						((BgL_objectz00_bglt)
																						BgL_new1238z00_3000);
																					BgL_auxz00_8707 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_8708);
																				}
																				BgL_auxz00_8706 =
																					((BgL_rtl_regzf2razf2_bglt)
																					BgL_auxz00_8707);
																			}
																			((((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_8706))->
																					BgL_coalescez00) =
																				((obj_t) BgL_auxz00_8712), BUNSPEC);
																		}
																		{
																			int BgL_auxz00_8728;
																			BgL_rtl_regzf2razf2_bglt BgL_auxz00_8722;

																			{
																				BgL_rtl_regzf2razf2_bglt
																					BgL_auxz00_8729;
																				{
																					obj_t BgL_auxz00_8730;

																					{	/* SawMill/regalloc.scm 636 */
																						BgL_objectz00_bglt BgL_tmpz00_8731;

																						BgL_tmpz00_8731 =
																							((BgL_objectz00_bglt)
																							((BgL_rtl_regz00_bglt)
																								BgL_rz00_2990));
																						BgL_auxz00_8730 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_8731);
																					}
																					BgL_auxz00_8729 =
																						((BgL_rtl_regzf2razf2_bglt)
																						BgL_auxz00_8730);
																				}
																				BgL_auxz00_8728 =
																					(((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_8729))->
																					BgL_occurrencesz00);
																			}
																			{
																				obj_t BgL_auxz00_8723;

																				{	/* SawMill/regalloc.scm 636 */
																					BgL_objectz00_bglt BgL_tmpz00_8724;

																					BgL_tmpz00_8724 =
																						((BgL_objectz00_bglt)
																						BgL_new1238z00_3000);
																					BgL_auxz00_8723 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_8724);
																				}
																				BgL_auxz00_8722 =
																					((BgL_rtl_regzf2razf2_bglt)
																					BgL_auxz00_8723);
																			}
																			((((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_8722))->
																					BgL_occurrencesz00) =
																				((int) BgL_auxz00_8728), BUNSPEC);
																		}
																		{
																			obj_t BgL_auxz00_8744;
																			BgL_rtl_regzf2razf2_bglt BgL_auxz00_8738;

																			{
																				BgL_rtl_regzf2razf2_bglt
																					BgL_auxz00_8745;
																				{
																					obj_t BgL_auxz00_8746;

																					{	/* SawMill/regalloc.scm 636 */
																						BgL_objectz00_bglt BgL_tmpz00_8747;

																						BgL_tmpz00_8747 =
																							((BgL_objectz00_bglt)
																							((BgL_rtl_regz00_bglt)
																								BgL_rz00_2990));
																						BgL_auxz00_8746 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_8747);
																					}
																					BgL_auxz00_8745 =
																						((BgL_rtl_regzf2razf2_bglt)
																						BgL_auxz00_8746);
																				}
																				BgL_auxz00_8744 =
																					(((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_8745))->
																					BgL_interferez00);
																			}
																			{
																				obj_t BgL_auxz00_8739;

																				{	/* SawMill/regalloc.scm 636 */
																					BgL_objectz00_bglt BgL_tmpz00_8740;

																					BgL_tmpz00_8740 =
																						((BgL_objectz00_bglt)
																						BgL_new1238z00_3000);
																					BgL_auxz00_8739 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_8740);
																				}
																				BgL_auxz00_8738 =
																					((BgL_rtl_regzf2razf2_bglt)
																					BgL_auxz00_8739);
																			}
																			((((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_8738))->
																					BgL_interferez00) =
																				((obj_t) BgL_auxz00_8744), BUNSPEC);
																		}
																		{
																			obj_t BgL_auxz00_8760;
																			BgL_rtl_regzf2razf2_bglt BgL_auxz00_8754;

																			{
																				BgL_rtl_regzf2razf2_bglt
																					BgL_auxz00_8761;
																				{
																					obj_t BgL_auxz00_8762;

																					{	/* SawMill/regalloc.scm 636 */
																						BgL_objectz00_bglt BgL_tmpz00_8763;

																						BgL_tmpz00_8763 =
																							((BgL_objectz00_bglt)
																							((BgL_rtl_regz00_bglt)
																								BgL_rz00_2990));
																						BgL_auxz00_8762 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_8763);
																					}
																					BgL_auxz00_8761 =
																						((BgL_rtl_regzf2razf2_bglt)
																						BgL_auxz00_8762);
																				}
																				BgL_auxz00_8760 =
																					(((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_8761))->
																					BgL_interfere2z00);
																			}
																			{
																				obj_t BgL_auxz00_8755;

																				{	/* SawMill/regalloc.scm 636 */
																					BgL_objectz00_bglt BgL_tmpz00_8756;

																					BgL_tmpz00_8756 =
																						((BgL_objectz00_bglt)
																						BgL_new1238z00_3000);
																					BgL_auxz00_8755 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_8756);
																				}
																				BgL_auxz00_8754 =
																					((BgL_rtl_regzf2razf2_bglt)
																					BgL_auxz00_8755);
																			}
																			((((BgL_rtl_regzf2razf2_bglt)
																						COBJECT(BgL_auxz00_8754))->
																					BgL_interfere2z00) =
																				((obj_t) BgL_auxz00_8760), BUNSPEC);
																		}
																		BgL_newrz00_2997 = BgL_new1238z00_3000;
																	}
																	{	/* SawMill/regalloc.scm 638 */
																		obj_t BgL_auxz00_6047;

																		{	/* SawMill/regalloc.scm 638 */
																			obj_t BgL_arg2307z00_2998;

																			BgL_arg2307z00_2998 =
																				MAKE_YOUNG_PAIR(BgL_kz00_2994,
																				((obj_t) BgL_newrz00_2997));
																			BgL_auxz00_6047 =
																				MAKE_YOUNG_PAIR(BgL_arg2307z00_2998,
																				((obj_t)
																					((obj_t)
																						CELL_REF(BgL_mappingz00_6045))));
																		}
																		CELL_SET(BgL_mappingz00_6045,
																			BgL_auxz00_6047);
																	}
																	return ((obj_t) BgL_newrz00_2997);
																}
														}
													}
											}
										else
											{	/* SawMill/regalloc.scm 626 */
												return ((obj_t) BgL_rz00_2990);
											}
									}
								else
									{	/* SawMill/regalloc.scm 624 */
										return ((obj_t) BgL_rz00_2990);
									}
							}
						}
					else
						{	/* SawMill/regalloc.scm 653 */
							bool_t BgL_test3107z00_8778;

							{	/* SawMill/regalloc.scm 653 */
								obj_t BgL_classz00_4903;

								BgL_classz00_4903 = BGl_rtl_insz00zzsaw_defsz00;
								if (BGL_OBJECTP(BgL_oz00_3013))
									{	/* SawMill/regalloc.scm 653 */
										BgL_objectz00_bglt BgL_arg1807z00_4905;

										BgL_arg1807z00_4905 = (BgL_objectz00_bglt) (BgL_oz00_3013);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawMill/regalloc.scm 653 */
												long BgL_idxz00_4911;

												BgL_idxz00_4911 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4905);
												BgL_test3107z00_8778 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4911 + 1L)) == BgL_classz00_4903);
											}
										else
											{	/* SawMill/regalloc.scm 653 */
												bool_t BgL_res2744z00_4936;

												{	/* SawMill/regalloc.scm 653 */
													obj_t BgL_oclassz00_4919;

													{	/* SawMill/regalloc.scm 653 */
														obj_t BgL_arg1815z00_4927;
														long BgL_arg1816z00_4928;

														BgL_arg1815z00_4927 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawMill/regalloc.scm 653 */
															long BgL_arg1817z00_4929;

															BgL_arg1817z00_4929 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4905);
															BgL_arg1816z00_4928 =
																(BgL_arg1817z00_4929 - OBJECT_TYPE);
														}
														BgL_oclassz00_4919 =
															VECTOR_REF(BgL_arg1815z00_4927,
															BgL_arg1816z00_4928);
													}
													{	/* SawMill/regalloc.scm 653 */
														bool_t BgL__ortest_1115z00_4920;

														BgL__ortest_1115z00_4920 =
															(BgL_classz00_4903 == BgL_oclassz00_4919);
														if (BgL__ortest_1115z00_4920)
															{	/* SawMill/regalloc.scm 653 */
																BgL_res2744z00_4936 = BgL__ortest_1115z00_4920;
															}
														else
															{	/* SawMill/regalloc.scm 653 */
																long BgL_odepthz00_4921;

																{	/* SawMill/regalloc.scm 653 */
																	obj_t BgL_arg1804z00_4922;

																	BgL_arg1804z00_4922 = (BgL_oclassz00_4919);
																	BgL_odepthz00_4921 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4922);
																}
																if ((1L < BgL_odepthz00_4921))
																	{	/* SawMill/regalloc.scm 653 */
																		obj_t BgL_arg1802z00_4924;

																		{	/* SawMill/regalloc.scm 653 */
																			obj_t BgL_arg1803z00_4925;

																			BgL_arg1803z00_4925 =
																				(BgL_oclassz00_4919);
																			BgL_arg1802z00_4924 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4925, 1L);
																		}
																		BgL_res2744z00_4936 =
																			(BgL_arg1802z00_4924 ==
																			BgL_classz00_4903);
																	}
																else
																	{	/* SawMill/regalloc.scm 653 */
																		BgL_res2744z00_4936 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test3107z00_8778 = BgL_res2744z00_4936;
											}
									}
								else
									{	/* SawMill/regalloc.scm 653 */
										BgL_test3107z00_8778 = ((bool_t) 0);
									}
							}
							if (BgL_test3107z00_8778)
								{	/* SawMill/regalloc.scm 653 */
									{
										obj_t BgL_auxz00_8801;

										{	/* SawMill/regalloc.scm 655 */
											obj_t BgL_l1636z00_3028;

											BgL_l1636z00_3028 =
												(((BgL_rtl_insz00_bglt) COBJECT(
														((BgL_rtl_insz00_bglt) BgL_oz00_3013)))->
												BgL_z52spillz52);
											if (NULLP(BgL_l1636z00_3028))
												{	/* SawMill/regalloc.scm 655 */
													BgL_auxz00_8801 = BNIL;
												}
											else
												{	/* SawMill/regalloc.scm 655 */
													obj_t BgL_head1638z00_3030;

													{	/* SawMill/regalloc.scm 655 */
														obj_t BgL_arg2328z00_3042;

														{	/* SawMill/regalloc.scm 655 */
															obj_t BgL_arg2330z00_3043;

															BgL_arg2330z00_3043 = CAR(BgL_l1636z00_3028);
															BgL_arg2328z00_3042 =
																BGl_mapzd2registerze70z35zzsaw_registerzd2allocationzd2
																(BgL_colorsz00_6046, BgL_mappingz00_6045,
																BgL_arg2330z00_3043);
														}
														BgL_head1638z00_3030 =
															MAKE_YOUNG_PAIR(BgL_arg2328z00_3042, BNIL);
													}
													{	/* SawMill/regalloc.scm 655 */
														obj_t BgL_g1641z00_3031;

														BgL_g1641z00_3031 = CDR(BgL_l1636z00_3028);
														{
															obj_t BgL_l1636z00_3033;
															obj_t BgL_tail1639z00_3034;

															BgL_l1636z00_3033 = BgL_g1641z00_3031;
															BgL_tail1639z00_3034 = BgL_head1638z00_3030;
														BgL_zc3z04anonymousza32323ze3z87_3035:
															if (NULLP(BgL_l1636z00_3033))
																{	/* SawMill/regalloc.scm 655 */
																	BgL_auxz00_8801 = BgL_head1638z00_3030;
																}
															else
																{	/* SawMill/regalloc.scm 655 */
																	obj_t BgL_newtail1640z00_3037;

																	{	/* SawMill/regalloc.scm 655 */
																		obj_t BgL_arg2326z00_3039;

																		{	/* SawMill/regalloc.scm 655 */
																			obj_t BgL_arg2327z00_3040;

																			BgL_arg2327z00_3040 =
																				CAR(((obj_t) BgL_l1636z00_3033));
																			BgL_arg2326z00_3039 =
																				BGl_mapzd2registerze70z35zzsaw_registerzd2allocationzd2
																				(BgL_colorsz00_6046,
																				BgL_mappingz00_6045,
																				BgL_arg2327z00_3040);
																		}
																		BgL_newtail1640z00_3037 =
																			MAKE_YOUNG_PAIR(BgL_arg2326z00_3039,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1639z00_3034,
																		BgL_newtail1640z00_3037);
																	{	/* SawMill/regalloc.scm 655 */
																		obj_t BgL_arg2325z00_3038;

																		BgL_arg2325z00_3038 =
																			CDR(((obj_t) BgL_l1636z00_3033));
																		{
																			obj_t BgL_tail1639z00_8821;
																			obj_t BgL_l1636z00_8820;

																			BgL_l1636z00_8820 = BgL_arg2325z00_3038;
																			BgL_tail1639z00_8821 =
																				BgL_newtail1640z00_3037;
																			BgL_tail1639z00_3034 =
																				BgL_tail1639z00_8821;
																			BgL_l1636z00_3033 = BgL_l1636z00_8820;
																			goto
																				BgL_zc3z04anonymousza32323ze3z87_3035;
																		}
																	}
																}
														}
													}
												}
										}
										((((BgL_rtl_insz00_bglt) COBJECT(
														((BgL_rtl_insz00_bglt) BgL_oz00_3013)))->
												BgL_z52spillz52) = ((obj_t) BgL_auxz00_8801), BUNSPEC);
									}
									{
										obj_t BgL_auxz00_8823;

										{	/* SawMill/regalloc.scm 656 */
											obj_t BgL_l1642z00_3044;

											BgL_l1642z00_3044 =
												(((BgL_rtl_insz00_bglt) COBJECT(
														((BgL_rtl_insz00_bglt) BgL_oz00_3013)))->
												BgL_argsz00);
											if (NULLP(BgL_l1642z00_3044))
												{	/* SawMill/regalloc.scm 656 */
													BgL_auxz00_8823 = BNIL;
												}
											else
												{	/* SawMill/regalloc.scm 656 */
													obj_t BgL_head1644z00_3046;

													{	/* SawMill/regalloc.scm 656 */
														obj_t BgL_arg2338z00_3058;

														{	/* SawMill/regalloc.scm 656 */
															obj_t BgL_arg2339z00_3059;

															BgL_arg2339z00_3059 = CAR(BgL_l1642z00_3044);
															BgL_arg2338z00_3058 =
																BGl_mapzd2registerze70z35zzsaw_registerzd2allocationzd2
																(BgL_colorsz00_6046, BgL_mappingz00_6045,
																BgL_arg2339z00_3059);
														}
														BgL_head1644z00_3046 =
															MAKE_YOUNG_PAIR(BgL_arg2338z00_3058, BNIL);
													}
													{	/* SawMill/regalloc.scm 656 */
														obj_t BgL_g1647z00_3047;

														BgL_g1647z00_3047 = CDR(BgL_l1642z00_3044);
														{
															obj_t BgL_l1642z00_3049;
															obj_t BgL_tail1645z00_3050;

															BgL_l1642z00_3049 = BgL_g1647z00_3047;
															BgL_tail1645z00_3050 = BgL_head1644z00_3046;
														BgL_zc3z04anonymousza32332ze3z87_3051:
															if (NULLP(BgL_l1642z00_3049))
																{	/* SawMill/regalloc.scm 656 */
																	BgL_auxz00_8823 = BgL_head1644z00_3046;
																}
															else
																{	/* SawMill/regalloc.scm 656 */
																	obj_t BgL_newtail1646z00_3053;

																	{	/* SawMill/regalloc.scm 656 */
																		obj_t BgL_arg2336z00_3055;

																		{	/* SawMill/regalloc.scm 656 */
																			obj_t BgL_arg2337z00_3056;

																			BgL_arg2337z00_3056 =
																				CAR(((obj_t) BgL_l1642z00_3049));
																			BgL_arg2336z00_3055 =
																				BGl_mapzd2registerze70z35zzsaw_registerzd2allocationzd2
																				(BgL_colorsz00_6046,
																				BgL_mappingz00_6045,
																				BgL_arg2337z00_3056);
																		}
																		BgL_newtail1646z00_3053 =
																			MAKE_YOUNG_PAIR(BgL_arg2336z00_3055,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1645z00_3050,
																		BgL_newtail1646z00_3053);
																	{	/* SawMill/regalloc.scm 656 */
																		obj_t BgL_arg2335z00_3054;

																		BgL_arg2335z00_3054 =
																			CDR(((obj_t) BgL_l1642z00_3049));
																		{
																			obj_t BgL_tail1645z00_8843;
																			obj_t BgL_l1642z00_8842;

																			BgL_l1642z00_8842 = BgL_arg2335z00_3054;
																			BgL_tail1645z00_8843 =
																				BgL_newtail1646z00_3053;
																			BgL_tail1645z00_3050 =
																				BgL_tail1645z00_8843;
																			BgL_l1642z00_3049 = BgL_l1642z00_8842;
																			goto
																				BgL_zc3z04anonymousza32332ze3z87_3051;
																		}
																	}
																}
														}
													}
												}
										}
										((((BgL_rtl_insz00_bglt) COBJECT(
														((BgL_rtl_insz00_bglt) BgL_oz00_3013)))->
												BgL_argsz00) = ((obj_t) BgL_auxz00_8823), BUNSPEC);
									}
									return BgL_oz00_3013;
								}
							else
								{	/* SawMill/regalloc.scm 653 */
									return BgL_oz00_3013;
								}
						}
				}
			}
		}

	}



/* remove-nop-move! */
	obj_t BGl_removezd2nopzd2movez12z12zzsaw_registerzd2allocationzd2(obj_t
		BgL_blocksz00_102)
	{
		{	/* SawMill/regalloc.scm 676 */
			{	/* SawMill/regalloc.scm 704 */
				obj_t BgL_nz00_3068;

				{	/* SawMill/regalloc.scm 704 */
					obj_t BgL_runner2358z00_3088;

					if (NULLP(BgL_blocksz00_102))
						{	/* SawMill/regalloc.scm 704 */
							BgL_runner2358z00_3088 = BNIL;
						}
					else
						{	/* SawMill/regalloc.scm 704 */
							obj_t BgL_head1670z00_3074;

							{	/* SawMill/regalloc.scm 704 */
								long BgL_arg2356z00_3086;

								{	/* SawMill/regalloc.scm 704 */
									obj_t BgL_arg2357z00_3087;

									BgL_arg2357z00_3087 = CAR(((obj_t) BgL_blocksz00_102));
									BgL_arg2356z00_3086 =
										BGl_removezd2blockz12ze70z27zzsaw_registerzd2allocationzd2
										(BgL_arg2357z00_3087);
								}
								BgL_head1670z00_3074 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg2356z00_3086), BNIL);
							}
							{	/* SawMill/regalloc.scm 704 */
								obj_t BgL_g1673z00_3075;

								BgL_g1673z00_3075 = CDR(((obj_t) BgL_blocksz00_102));
								{
									obj_t BgL_l1668z00_3077;
									obj_t BgL_tail1671z00_3078;

									BgL_l1668z00_3077 = BgL_g1673z00_3075;
									BgL_tail1671z00_3078 = BgL_head1670z00_3074;
								BgL_zc3z04anonymousza32351ze3z87_3079:
									if (NULLP(BgL_l1668z00_3077))
										{	/* SawMill/regalloc.scm 704 */
											BgL_runner2358z00_3088 = BgL_head1670z00_3074;
										}
									else
										{	/* SawMill/regalloc.scm 704 */
											obj_t BgL_newtail1672z00_3081;

											{	/* SawMill/regalloc.scm 704 */
												long BgL_arg2354z00_3083;

												{	/* SawMill/regalloc.scm 704 */
													obj_t BgL_arg2355z00_3084;

													BgL_arg2355z00_3084 =
														CAR(((obj_t) BgL_l1668z00_3077));
													BgL_arg2354z00_3083 =
														BGl_removezd2blockz12ze70z27zzsaw_registerzd2allocationzd2
														(BgL_arg2355z00_3084);
												}
												BgL_newtail1672z00_3081 =
													MAKE_YOUNG_PAIR(BINT(BgL_arg2354z00_3083), BNIL);
											}
											SET_CDR(BgL_tail1671z00_3078, BgL_newtail1672z00_3081);
											{	/* SawMill/regalloc.scm 704 */
												obj_t BgL_arg2353z00_3082;

												BgL_arg2353z00_3082 = CDR(((obj_t) BgL_l1668z00_3077));
												{
													obj_t BgL_tail1671z00_8865;
													obj_t BgL_l1668z00_8864;

													BgL_l1668z00_8864 = BgL_arg2353z00_3082;
													BgL_tail1671z00_8865 = BgL_newtail1672z00_3081;
													BgL_tail1671z00_3078 = BgL_tail1671z00_8865;
													BgL_l1668z00_3077 = BgL_l1668z00_8864;
													goto BgL_zc3z04anonymousza32351ze3z87_3079;
												}
											}
										}
								}
							}
						}
					BgL_nz00_3068 =
						BGl_zb2zb2zz__r4_numbers_6_5z00(BgL_runner2358z00_3088);
				}
				{	/* SawMill/regalloc.scm 705 */
					obj_t BgL_list2346z00_3069;

					{	/* SawMill/regalloc.scm 705 */
						obj_t BgL_arg2348z00_3070;

						{	/* SawMill/regalloc.scm 705 */
							obj_t BgL_arg2349z00_3071;

							BgL_arg2349z00_3071 =
								MAKE_YOUNG_PAIR(BGl_string2807z00zzsaw_registerzd2allocationzd2,
								BNIL);
							BgL_arg2348z00_3070 =
								MAKE_YOUNG_PAIR(BgL_nz00_3068, BgL_arg2349z00_3071);
						}
						BgL_list2346z00_3069 =
							MAKE_YOUNG_PAIR(BGl_string2808z00zzsaw_registerzd2allocationzd2,
							BgL_arg2348z00_3070);
					}
					return BGl_verbosez00zztools_speekz00(BINT(3L), BgL_list2346z00_3069);
				}
			}
		}

	}



/* remove-block!~0 */
	long BGl_removezd2blockz12ze70z27zzsaw_registerzd2allocationzd2(obj_t
		BgL_bz00_3102)
	{
		{	/* SawMill/regalloc.scm 703 */
			{
				obj_t BgL_iz00_3089;

				{	/* SawMill/regalloc.scm 684 */
					obj_t BgL_g1257z00_3104;

					BgL_g1257z00_3104 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_bz00_3102)))->BgL_firstz00);
					{
						obj_t BgL_insz00_3106;
						obj_t BgL_prevz00_3107;
						long BgL_nz00_3108;

						BgL_insz00_3106 = BgL_g1257z00_3104;
						BgL_prevz00_3107 = BFALSE;
						BgL_nz00_3108 = 0L;
					BgL_zc3z04anonymousza32370ze3z87_3109:
						if (PAIRP(BgL_insz00_3106))
							{	/* SawMill/regalloc.scm 688 */
								bool_t BgL_test3119z00_8876;

								BgL_iz00_3089 = CAR(BgL_insz00_3106);
								{	/* SawMill/regalloc.scm 679 */
									bool_t BgL_test3120z00_8877;

									{	/* SawMill/regalloc.scm 679 */
										BgL_rtl_funz00_bglt BgL_arg2368z00_3101;

										BgL_arg2368z00_3101 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt)
														((BgL_rtl_insz00_bglt) BgL_iz00_3089))))->
											BgL_funz00);
										{	/* SawMill/regalloc.scm 679 */
											obj_t BgL_classz00_5003;

											BgL_classz00_5003 = BGl_rtl_movz00zzsaw_defsz00;
											{	/* SawMill/regalloc.scm 679 */
												BgL_objectz00_bglt BgL_arg1807z00_5005;

												{	/* SawMill/regalloc.scm 679 */
													obj_t BgL_tmpz00_8881;

													BgL_tmpz00_8881 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_arg2368z00_3101));
													BgL_arg1807z00_5005 =
														(BgL_objectz00_bglt) (BgL_tmpz00_8881);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawMill/regalloc.scm 679 */
														long BgL_idxz00_5011;

														BgL_idxz00_5011 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5005);
														BgL_test3120z00_8877 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_5011 + 3L)) == BgL_classz00_5003);
													}
												else
													{	/* SawMill/regalloc.scm 679 */
														bool_t BgL_res2746z00_5036;

														{	/* SawMill/regalloc.scm 679 */
															obj_t BgL_oclassz00_5019;

															{	/* SawMill/regalloc.scm 679 */
																obj_t BgL_arg1815z00_5027;
																long BgL_arg1816z00_5028;

																BgL_arg1815z00_5027 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawMill/regalloc.scm 679 */
																	long BgL_arg1817z00_5029;

																	BgL_arg1817z00_5029 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5005);
																	BgL_arg1816z00_5028 =
																		(BgL_arg1817z00_5029 - OBJECT_TYPE);
																}
																BgL_oclassz00_5019 =
																	VECTOR_REF(BgL_arg1815z00_5027,
																	BgL_arg1816z00_5028);
															}
															{	/* SawMill/regalloc.scm 679 */
																bool_t BgL__ortest_1115z00_5020;

																BgL__ortest_1115z00_5020 =
																	(BgL_classz00_5003 == BgL_oclassz00_5019);
																if (BgL__ortest_1115z00_5020)
																	{	/* SawMill/regalloc.scm 679 */
																		BgL_res2746z00_5036 =
																			BgL__ortest_1115z00_5020;
																	}
																else
																	{	/* SawMill/regalloc.scm 679 */
																		long BgL_odepthz00_5021;

																		{	/* SawMill/regalloc.scm 679 */
																			obj_t BgL_arg1804z00_5022;

																			BgL_arg1804z00_5022 =
																				(BgL_oclassz00_5019);
																			BgL_odepthz00_5021 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_5022);
																		}
																		if ((3L < BgL_odepthz00_5021))
																			{	/* SawMill/regalloc.scm 679 */
																				obj_t BgL_arg1802z00_5024;

																				{	/* SawMill/regalloc.scm 679 */
																					obj_t BgL_arg1803z00_5025;

																					BgL_arg1803z00_5025 =
																						(BgL_oclassz00_5019);
																					BgL_arg1802z00_5024 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_5025, 3L);
																				}
																				BgL_res2746z00_5036 =
																					(BgL_arg1802z00_5024 ==
																					BgL_classz00_5003);
																			}
																		else
																			{	/* SawMill/regalloc.scm 679 */
																				BgL_res2746z00_5036 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test3120z00_8877 = BgL_res2746z00_5036;
													}
											}
										}
									}
									if (BgL_test3120z00_8877)
										{	/* SawMill/regalloc.scm 679 */
											if (NULLP(
													(((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt)
																	((BgL_rtl_insz00_bglt) BgL_iz00_3089))))->
														BgL_argsz00)))
												{	/* SawMill/regalloc.scm 680 */
													BgL_test3119z00_8876 = ((bool_t) 0);
												}
											else
												{	/* SawMill/regalloc.scm 681 */
													bool_t BgL_test3125z00_8909;

													{	/* SawMill/regalloc.scm 681 */
														obj_t BgL_tmpz00_8910;

														{	/* SawMill/regalloc.scm 681 */
															obj_t BgL_pairz00_5037;

															BgL_pairz00_5037 =
																(((BgL_rtl_insz00_bglt) COBJECT(
																		((BgL_rtl_insz00_bglt)
																			((BgL_rtl_insz00_bglt) BgL_iz00_3089))))->
																BgL_argsz00);
															BgL_tmpz00_8910 = CDR(BgL_pairz00_5037);
														}
														BgL_test3125z00_8909 = NULLP(BgL_tmpz00_8910);
													}
													if (BgL_test3125z00_8909)
														{	/* SawMill/regalloc.scm 682 */
															obj_t BgL_tmpz00_8916;

															{	/* SawMill/regalloc.scm 682 */
																obj_t BgL_pairz00_5038;

																BgL_pairz00_5038 =
																	(((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt)
																				((BgL_rtl_insz00_bglt)
																					BgL_iz00_3089))))->BgL_argsz00);
																BgL_tmpz00_8916 = CAR(BgL_pairz00_5038);
															}
															BgL_test3119z00_8876 =
																(
																(((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt)
																				((BgL_rtl_insz00_bglt)
																					BgL_iz00_3089))))->BgL_destz00) ==
																BgL_tmpz00_8916);
														}
													else
														{	/* SawMill/regalloc.scm 681 */
															BgL_test3119z00_8876 = ((bool_t) 0);
														}
												}
										}
									else
										{	/* SawMill/regalloc.scm 679 */
											BgL_test3119z00_8876 = ((bool_t) 0);
										}
								}
								if (BgL_test3119z00_8876)
									{	/* SawMill/regalloc.scm 689 */
										BgL_rtl_insz00_bglt BgL_i1258z00_3113;

										BgL_i1258z00_3113 =
											((BgL_rtl_insz00_bglt) CAR(BgL_insz00_3106));
										if (PAIRP(BgL_prevz00_3107))
											{	/* SawMill/regalloc.scm 691 */
												{	/* SawMill/regalloc.scm 692 */
													obj_t BgL_tmpz00_8930;

													BgL_tmpz00_8930 = CDR(BgL_insz00_3106);
													SET_CDR(BgL_prevz00_3107, BgL_tmpz00_8930);
												}
												{
													long BgL_nz00_8935;
													obj_t BgL_insz00_8933;

													BgL_insz00_8933 = CDR(BgL_insz00_3106);
													BgL_nz00_8935 = (1L + BgL_nz00_3108);
													BgL_nz00_3108 = BgL_nz00_8935;
													BgL_insz00_3106 = BgL_insz00_8933;
													goto BgL_zc3z04anonymousza32370ze3z87_3109;
												}
											}
										else
											{	/* SawMill/regalloc.scm 691 */
												if (NULLP(CDR(BgL_insz00_3106)))
													{	/* SawMill/regalloc.scm 694 */
														{
															BgL_rtl_funz00_bglt BgL_auxz00_8940;

															{	/* SawMill/regalloc.scm 695 */
																BgL_rtl_nopz00_bglt BgL_new1261z00_3120;

																{	/* SawMill/regalloc.scm 695 */
																	BgL_rtl_nopz00_bglt BgL_new1260z00_3121;

																	BgL_new1260z00_3121 =
																		((BgL_rtl_nopz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_rtl_nopz00_bgl))));
																	{	/* SawMill/regalloc.scm 695 */
																		long BgL_arg2381z00_3122;

																		{	/* SawMill/regalloc.scm 695 */
																			obj_t BgL_classz00_5047;

																			BgL_classz00_5047 =
																				BGl_rtl_nopz00zzsaw_defsz00;
																			BgL_arg2381z00_3122 =
																				BGL_CLASS_NUM(BgL_classz00_5047);
																		}
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1260z00_3121),
																			BgL_arg2381z00_3122);
																	}
																	BgL_new1261z00_3120 = BgL_new1260z00_3121;
																}
																((((BgL_rtl_funz00_bglt) COBJECT(
																				((BgL_rtl_funz00_bglt)
																					BgL_new1261z00_3120)))->BgL_locz00) =
																	((obj_t) BFALSE), BUNSPEC);
																BgL_auxz00_8940 =
																	((BgL_rtl_funz00_bglt) BgL_new1261z00_3120);
															}
															((((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt)
																				BgL_i1258z00_3113)))->BgL_funz00) =
																((BgL_rtl_funz00_bglt) BgL_auxz00_8940),
																BUNSPEC);
														}
														((((BgL_rtl_insz00_bglt) COBJECT(
																		((BgL_rtl_insz00_bglt)
																			BgL_i1258z00_3113)))->BgL_destz00) =
															((obj_t) BFALSE), BUNSPEC);
														((((BgL_rtl_insz00_bglt)
																	COBJECT(((BgL_rtl_insz00_bglt)
																			BgL_i1258z00_3113)))->BgL_argsz00) =
															((obj_t) BNIL), BUNSPEC);
														return (1L + BgL_nz00_3108);
													}
												else
													{	/* SawMill/regalloc.scm 694 */
														{	/* SawMill/regalloc.scm 700 */
															obj_t BgL_arg2382z00_3123;

															BgL_arg2382z00_3123 = CDR(BgL_insz00_3106);
															((((BgL_blockz00_bglt) COBJECT(
																			((BgL_blockz00_bglt) BgL_bz00_3102)))->
																	BgL_firstz00) =
																((obj_t) ((obj_t) BgL_arg2382z00_3123)),
																BUNSPEC);
														}
														{
															long BgL_nz00_8961;
															obj_t BgL_insz00_8959;

															BgL_insz00_8959 = CDR(BgL_insz00_3106);
															BgL_nz00_8961 = (1L + BgL_nz00_3108);
															BgL_nz00_3108 = BgL_nz00_8961;
															BgL_insz00_3106 = BgL_insz00_8959;
															goto BgL_zc3z04anonymousza32370ze3z87_3109;
														}
													}
											}
									}
								else
									{
										obj_t BgL_prevz00_8965;
										obj_t BgL_insz00_8963;

										BgL_insz00_8963 = CDR(BgL_insz00_3106);
										BgL_prevz00_8965 = BgL_insz00_3106;
										BgL_prevz00_3107 = BgL_prevz00_8965;
										BgL_insz00_3106 = BgL_insz00_8963;
										goto BgL_zc3z04anonymousza32370ze3z87_3109;
									}
							}
						else
							{	/* SawMill/regalloc.scm 687 */
								return BgL_nz00_3108;
							}
					}
				}
			}
		}

	}



/* funcall-spill! */
	obj_t BGl_funcallzd2spillz12zc0zzsaw_registerzd2allocationzd2(obj_t
		BgL_hregsz00_103, obj_t BgL_blocksz00_104)
	{
		{	/* SawMill/regalloc.scm 715 */
			{
				obj_t BgL_bz00_3171;

				if (NULLP(BgL_hregsz00_103))
					{	/* SawMill/regalloc.scm 734 */
						return BgL_blocksz00_104;
					}
				else
					{
						obj_t BgL_l1680z00_3137;

						{	/* SawMill/regalloc.scm 736 */
							bool_t BgL_tmpz00_8968;

							BgL_l1680z00_3137 = BgL_blocksz00_104;
						BgL_zc3z04anonymousza32389ze3z87_3138:
							if (PAIRP(BgL_l1680z00_3137))
								{	/* SawMill/regalloc.scm 736 */
									BgL_bz00_3171 = CAR(BgL_l1680z00_3137);
									{	/* SawMill/regalloc.scm 733 */
										obj_t BgL_g1679z00_3174;

										BgL_g1679z00_3174 =
											(((BgL_blockz00_bglt) COBJECT(
													((BgL_blockz00_bglt) BgL_bz00_3171)))->BgL_firstz00);
										{
											obj_t BgL_l1677z00_3176;

											BgL_l1677z00_3176 = BgL_g1679z00_3174;
										BgL_zc3z04anonymousza32410ze3z87_3177:
											if (PAIRP(BgL_l1677z00_3176))
												{	/* SawMill/regalloc.scm 733 */
													{	/* SawMill/regalloc.scm 733 */
														obj_t BgL_arg2412z00_3179;

														BgL_arg2412z00_3179 = CAR(BgL_l1677z00_3176);
														BGl_inszd2funcallzd2spillz12ze70zf5zzsaw_registerzd2allocationzd2
															(((BgL_rtl_insz00_bglt) BgL_arg2412z00_3179));
													}
													{
														obj_t BgL_l1677z00_8978;

														BgL_l1677z00_8978 = CDR(BgL_l1677z00_3176);
														BgL_l1677z00_3176 = BgL_l1677z00_8978;
														goto BgL_zc3z04anonymousza32410ze3z87_3177;
													}
												}
											else
												{	/* SawMill/regalloc.scm 733 */
													((bool_t) 1);
												}
										}
									}
									{
										obj_t BgL_l1680z00_8981;

										BgL_l1680z00_8981 = CDR(BgL_l1680z00_3137);
										BgL_l1680z00_3137 = BgL_l1680z00_8981;
										goto BgL_zc3z04anonymousza32389ze3z87_3138;
									}
								}
							else
								{	/* SawMill/regalloc.scm 736 */
									BgL_tmpz00_8968 = ((bool_t) 1);
								}
							return BBOOL(BgL_tmpz00_8968);
						}
					}
			}
		}

	}



/* ins-funcall-spill!~0 */
	bool_t
		BGl_inszd2funcallzd2spillz12ze70zf5zzsaw_registerzd2allocationzd2
		(BgL_rtl_insz00_bglt BgL_iz00_3146)
	{
		{	/* SawMill/regalloc.scm 730 */
			{	/* SawMill/regalloc.scm 721 */
				obj_t BgL_aspillz00_3149;

				{	/* SawMill/regalloc.scm 722 */
					obj_t BgL_arg2398z00_3152;

					{
						BgL_rtl_inszf2razf2_bglt BgL_auxz00_8984;

						{
							obj_t BgL_auxz00_8985;

							{	/* SawMill/regalloc.scm 721 */
								BgL_objectz00_bglt BgL_tmpz00_8986;

								BgL_tmpz00_8986 = ((BgL_objectz00_bglt) BgL_iz00_3146);
								BgL_auxz00_8985 = BGL_OBJECT_WIDENING(BgL_tmpz00_8986);
							}
							BgL_auxz00_8984 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_8985);
						}
						BgL_arg2398z00_3152 =
							(((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_8984))->
							BgL_outz00);
					}
					{	/* SawMill/regalloc.scm 723 */
						obj_t BgL_zc3z04anonymousza32399ze3z87_5928;

						BgL_zc3z04anonymousza32399ze3z87_5928 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza32399ze3ze5zzsaw_registerzd2allocationzd2,
							(int) (1L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza32399ze3z87_5928, (int) (0L),
							((obj_t) BgL_iz00_3146));
						BgL_aspillz00_3149 =
							BGl_regsetzd2filterzd2zzsaw_regsetz00
							(BgL_zc3z04anonymousza32399ze3z87_5928,
							((BgL_regsetz00_bglt) BgL_arg2398z00_3152));
				}}
				if (NULLP(BgL_aspillz00_3149))
					{	/* SawMill/regalloc.scm 728 */
						BFALSE;
					}
				else
					{	/* SawMill/regalloc.scm 729 */
						obj_t BgL_vz00_5223;

						BgL_vz00_5223 = BgL_aspillz00_3149;
						((((BgL_rtl_insz00_bglt) COBJECT(
										((BgL_rtl_insz00_bglt) BgL_iz00_3146)))->BgL_z52spillz52) =
							((obj_t) BgL_vz00_5223), BUNSPEC);
					}
			}
			{	/* SawMill/regalloc.scm 730 */
				obj_t BgL_g1676z00_3163;

				BgL_g1676z00_3163 =
					(((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_iz00_3146)))->BgL_argsz00);
				{
					obj_t BgL_l1674z00_3165;

					BgL_l1674z00_3165 = BgL_g1676z00_3163;
				BgL_zc3z04anonymousza32405ze3z87_3166:
					if (PAIRP(BgL_l1674z00_3165))
						{	/* SawMill/regalloc.scm 730 */
							{	/* SawMill/regalloc.scm 730 */
								obj_t BgL_arg2407z00_3168;

								BgL_arg2407z00_3168 = CAR(BgL_l1674z00_3165);
								{	/* SawMill/regalloc.scm 717 */
									bool_t BgL_test3133z00_9008;

									{	/* SawMill/regalloc.scm 717 */
										obj_t BgL_classz00_5226;

										BgL_classz00_5226 = BGl_rtl_insz00zzsaw_defsz00;
										if (BGL_OBJECTP(BgL_arg2407z00_3168))
											{	/* SawMill/regalloc.scm 717 */
												BgL_objectz00_bglt BgL_arg1807z00_5228;

												BgL_arg1807z00_5228 =
													(BgL_objectz00_bglt) (BgL_arg2407z00_3168);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawMill/regalloc.scm 717 */
														long BgL_idxz00_5234;

														BgL_idxz00_5234 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5228);
														BgL_test3133z00_9008 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_5234 + 1L)) == BgL_classz00_5226);
													}
												else
													{	/* SawMill/regalloc.scm 717 */
														bool_t BgL_res2749z00_5259;

														{	/* SawMill/regalloc.scm 717 */
															obj_t BgL_oclassz00_5242;

															{	/* SawMill/regalloc.scm 717 */
																obj_t BgL_arg1815z00_5250;
																long BgL_arg1816z00_5251;

																BgL_arg1815z00_5250 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawMill/regalloc.scm 717 */
																	long BgL_arg1817z00_5252;

																	BgL_arg1817z00_5252 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5228);
																	BgL_arg1816z00_5251 =
																		(BgL_arg1817z00_5252 - OBJECT_TYPE);
																}
																BgL_oclassz00_5242 =
																	VECTOR_REF(BgL_arg1815z00_5250,
																	BgL_arg1816z00_5251);
															}
															{	/* SawMill/regalloc.scm 717 */
																bool_t BgL__ortest_1115z00_5243;

																BgL__ortest_1115z00_5243 =
																	(BgL_classz00_5226 == BgL_oclassz00_5242);
																if (BgL__ortest_1115z00_5243)
																	{	/* SawMill/regalloc.scm 717 */
																		BgL_res2749z00_5259 =
																			BgL__ortest_1115z00_5243;
																	}
																else
																	{	/* SawMill/regalloc.scm 717 */
																		long BgL_odepthz00_5244;

																		{	/* SawMill/regalloc.scm 717 */
																			obj_t BgL_arg1804z00_5245;

																			BgL_arg1804z00_5245 =
																				(BgL_oclassz00_5242);
																			BgL_odepthz00_5244 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_5245);
																		}
																		if ((1L < BgL_odepthz00_5244))
																			{	/* SawMill/regalloc.scm 717 */
																				obj_t BgL_arg1802z00_5247;

																				{	/* SawMill/regalloc.scm 717 */
																					obj_t BgL_arg1803z00_5248;

																					BgL_arg1803z00_5248 =
																						(BgL_oclassz00_5242);
																					BgL_arg1802z00_5247 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_5248, 1L);
																				}
																				BgL_res2749z00_5259 =
																					(BgL_arg1802z00_5247 ==
																					BgL_classz00_5226);
																			}
																		else
																			{	/* SawMill/regalloc.scm 717 */
																				BgL_res2749z00_5259 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test3133z00_9008 = BgL_res2749z00_5259;
													}
											}
										else
											{	/* SawMill/regalloc.scm 717 */
												BgL_test3133z00_9008 = ((bool_t) 0);
											}
									}
									if (BgL_test3133z00_9008)
										{	/* SawMill/regalloc.scm 717 */
											BGl_inszd2funcallzd2spillz12ze70zf5zzsaw_registerzd2allocationzd2
												(((BgL_rtl_insz00_bglt) BgL_arg2407z00_3168));
										}
									else
										{	/* SawMill/regalloc.scm 717 */
											((bool_t) 0);
										}
								}
							}
							{
								obj_t BgL_l1674z00_9033;

								BgL_l1674z00_9033 = CDR(BgL_l1674z00_3165);
								BgL_l1674z00_3165 = BgL_l1674z00_9033;
								goto BgL_zc3z04anonymousza32405ze3z87_3166;
							}
						}
					else
						{	/* SawMill/regalloc.scm 730 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* &<@anonymous:2399> */
	obj_t BGl_z62zc3z04anonymousza32399ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5929, obj_t BgL_rz00_5931)
	{
		{	/* SawMill/regalloc.scm 722 */
			{	/* SawMill/regalloc.scm 723 */
				BgL_rtl_insz00_bglt BgL_i1262z00_5930;

				BgL_i1262z00_5930 =
					((BgL_rtl_insz00_bglt) PROCEDURE_REF(BgL_envz00_5929, (int) (0L)));
				{	/* SawMill/regalloc.scm 723 */
					bool_t BgL_tmpz00_9038;

					{	/* SawMill/regalloc.scm 723 */
						bool_t BgL_test3138z00_9039;

						{	/* SawMill/regalloc.scm 723 */
							obj_t BgL_arg2404z00_6097;

							{
								BgL_rtl_inszf2razf2_bglt BgL_auxz00_9040;

								{
									obj_t BgL_auxz00_9041;

									{	/* SawMill/regalloc.scm 723 */
										BgL_objectz00_bglt BgL_tmpz00_9042;

										BgL_tmpz00_9042 = ((BgL_objectz00_bglt) BgL_i1262z00_5930);
										BgL_auxz00_9041 = BGL_OBJECT_WIDENING(BgL_tmpz00_9042);
									}
									BgL_auxz00_9040 =
										((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9041);
								}
								BgL_arg2404z00_6097 =
									(((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9040))->
									BgL_defz00);
							}
							{	/* SawMill/regalloc.scm 723 */
								long BgL_basez00_6098;
								long BgL_bitz00_6099;

								{	/* SawMill/regalloc.scm 723 */
									int BgL_arg2607z00_6100;

									{
										BgL_rtl_regzf2razf2_bglt BgL_auxz00_9047;

										{
											obj_t BgL_auxz00_9048;

											{	/* SawMill/regalloc.scm 723 */
												BgL_objectz00_bglt BgL_tmpz00_9049;

												BgL_tmpz00_9049 =
													((BgL_objectz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_rz00_5931));
												BgL_auxz00_9048 = BGL_OBJECT_WIDENING(BgL_tmpz00_9049);
											}
											BgL_auxz00_9047 =
												((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_9048);
										}
										BgL_arg2607z00_6100 =
											(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_9047))->
											BgL_numz00);
									}
									BgL_basez00_6098 = ((long) (BgL_arg2607z00_6100) / 8L);
								}
								{	/* SawMill/regalloc.scm 723 */
									int BgL_arg2608z00_6101;

									{
										BgL_rtl_regzf2razf2_bglt BgL_auxz00_9057;

										{
											obj_t BgL_auxz00_9058;

											{	/* SawMill/regalloc.scm 723 */
												BgL_objectz00_bglt BgL_tmpz00_9059;

												BgL_tmpz00_9059 =
													((BgL_objectz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_rz00_5931));
												BgL_auxz00_9058 = BGL_OBJECT_WIDENING(BgL_tmpz00_9059);
											}
											BgL_auxz00_9057 =
												((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_9058);
										}
										BgL_arg2608z00_6101 =
											(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_9057))->
											BgL_numz00);
									}
									{	/* SawMill/regalloc.scm 723 */
										long BgL_n1z00_6102;
										long BgL_n2z00_6103;

										BgL_n1z00_6102 = (long) (BgL_arg2608z00_6101);
										BgL_n2z00_6103 = 8L;
										{	/* SawMill/regalloc.scm 723 */
											bool_t BgL_test3139z00_9066;

											{	/* SawMill/regalloc.scm 723 */
												long BgL_arg1338z00_6104;

												BgL_arg1338z00_6104 =
													(((BgL_n1z00_6102) | (BgL_n2z00_6103)) & -2147483648);
												BgL_test3139z00_9066 = (BgL_arg1338z00_6104 == 0L);
											}
											if (BgL_test3139z00_9066)
												{	/* SawMill/regalloc.scm 723 */
													int32_t BgL_arg1334z00_6105;

													{	/* SawMill/regalloc.scm 723 */
														int32_t BgL_arg1336z00_6106;
														int32_t BgL_arg1337z00_6107;

														BgL_arg1336z00_6106 = (int32_t) (BgL_n1z00_6102);
														BgL_arg1337z00_6107 = (int32_t) (BgL_n2z00_6103);
														BgL_arg1334z00_6105 =
															(BgL_arg1336z00_6106 % BgL_arg1337z00_6107);
													}
													{	/* SawMill/regalloc.scm 723 */
														long BgL_arg1446z00_6108;

														BgL_arg1446z00_6108 = (long) (BgL_arg1334z00_6105);
														BgL_bitz00_6099 = (long) (BgL_arg1446z00_6108);
												}}
											else
												{	/* SawMill/regalloc.scm 723 */
													BgL_bitz00_6099 = (BgL_n1z00_6102 % BgL_n2z00_6103);
												}
										}
									}
								}
								if (
									(BgL_basez00_6098 <
										STRING_LENGTH(
											(((BgL_regsetz00_bglt) COBJECT(
														((BgL_regsetz00_bglt) BgL_arg2404z00_6097)))->
												BgL_stringz00))))
									{	/* SawMill/regalloc.scm 723 */
										BgL_test3138z00_9039 =
											(
											((STRING_REF(
														(((BgL_regsetz00_bglt) COBJECT(
																	((BgL_regsetz00_bglt) BgL_arg2404z00_6097)))->
															BgL_stringz00),
														BgL_basez00_6098)) & (1L <<
													(int) (BgL_bitz00_6099))) > 0L);
									}
								else
									{	/* SawMill/regalloc.scm 723 */
										BgL_test3138z00_9039 = ((bool_t) 0);
									}
							}
						}
						if (BgL_test3138z00_9039)
							{	/* SawMill/regalloc.scm 723 */
								BgL_tmpz00_9038 = ((bool_t) 0);
							}
						else
							{	/* SawMill/regalloc.scm 725 */
								bool_t BgL_test3141z00_9088;

								{	/* SawMill/regalloc.scm 725 */
									obj_t BgL_arg2403z00_6109;

									{
										BgL_rtl_regzf2razf2_bglt BgL_auxz00_9089;

										{
											obj_t BgL_auxz00_9090;

											{	/* SawMill/regalloc.scm 725 */
												BgL_objectz00_bglt BgL_tmpz00_9091;

												BgL_tmpz00_9091 =
													((BgL_objectz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_rz00_5931));
												BgL_auxz00_9090 = BGL_OBJECT_WIDENING(BgL_tmpz00_9091);
											}
											BgL_auxz00_9089 =
												((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_9090);
										}
										BgL_arg2403z00_6109 =
											(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_9089))->
											BgL_colorz00);
									}
									{	/* SawMill/regalloc.scm 725 */
										obj_t BgL_classz00_6110;

										BgL_classz00_6110 = BGl_rtl_regz00zzsaw_defsz00;
										if (BGL_OBJECTP(BgL_arg2403z00_6109))
											{	/* SawMill/regalloc.scm 725 */
												BgL_objectz00_bglt BgL_arg1807z00_6111;

												BgL_arg1807z00_6111 =
													(BgL_objectz00_bglt) (BgL_arg2403z00_6109);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawMill/regalloc.scm 725 */
														long BgL_idxz00_6112;

														BgL_idxz00_6112 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6111);
														BgL_test3141z00_9088 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_6112 + 1L)) == BgL_classz00_6110);
													}
												else
													{	/* SawMill/regalloc.scm 725 */
														bool_t BgL_res2748z00_6115;

														{	/* SawMill/regalloc.scm 725 */
															obj_t BgL_oclassz00_6116;

															{	/* SawMill/regalloc.scm 725 */
																obj_t BgL_arg1815z00_6117;
																long BgL_arg1816z00_6118;

																BgL_arg1815z00_6117 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawMill/regalloc.scm 725 */
																	long BgL_arg1817z00_6119;

																	BgL_arg1817z00_6119 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6111);
																	BgL_arg1816z00_6118 =
																		(BgL_arg1817z00_6119 - OBJECT_TYPE);
																}
																BgL_oclassz00_6116 =
																	VECTOR_REF(BgL_arg1815z00_6117,
																	BgL_arg1816z00_6118);
															}
															{	/* SawMill/regalloc.scm 725 */
																bool_t BgL__ortest_1115z00_6120;

																BgL__ortest_1115z00_6120 =
																	(BgL_classz00_6110 == BgL_oclassz00_6116);
																if (BgL__ortest_1115z00_6120)
																	{	/* SawMill/regalloc.scm 725 */
																		BgL_res2748z00_6115 =
																			BgL__ortest_1115z00_6120;
																	}
																else
																	{	/* SawMill/regalloc.scm 725 */
																		long BgL_odepthz00_6121;

																		{	/* SawMill/regalloc.scm 725 */
																			obj_t BgL_arg1804z00_6122;

																			BgL_arg1804z00_6122 =
																				(BgL_oclassz00_6116);
																			BgL_odepthz00_6121 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_6122);
																		}
																		if ((1L < BgL_odepthz00_6121))
																			{	/* SawMill/regalloc.scm 725 */
																				obj_t BgL_arg1802z00_6123;

																				{	/* SawMill/regalloc.scm 725 */
																					obj_t BgL_arg1803z00_6124;

																					BgL_arg1803z00_6124 =
																						(BgL_oclassz00_6116);
																					BgL_arg1802z00_6123 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_6124, 1L);
																				}
																				BgL_res2748z00_6115 =
																					(BgL_arg1802z00_6123 ==
																					BgL_classz00_6110);
																			}
																		else
																			{	/* SawMill/regalloc.scm 725 */
																				BgL_res2748z00_6115 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test3141z00_9088 = BgL_res2748z00_6115;
													}
											}
										else
											{	/* SawMill/regalloc.scm 725 */
												BgL_test3141z00_9088 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test3141z00_9088)
									{	/* SawMill/regalloc.scm 726 */
										obj_t BgL_arg2401z00_6125;
										obj_t BgL_arg2402z00_6126;

										{
											BgL_rtl_regzf2razf2_bglt BgL_auxz00_9119;

											{
												obj_t BgL_auxz00_9120;

												{	/* SawMill/regalloc.scm 726 */
													BgL_objectz00_bglt BgL_tmpz00_9121;

													BgL_tmpz00_9121 =
														((BgL_objectz00_bglt)
														((BgL_rtl_regz00_bglt) BgL_rz00_5931));
													BgL_auxz00_9120 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_9121);
												}
												BgL_auxz00_9119 =
													((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_9120);
											}
											BgL_arg2401z00_6125 =
												(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_9119))->
												BgL_colorz00);
										}
										{
											BgL_rtl_inszf2razf2_bglt BgL_auxz00_9127;

											{
												obj_t BgL_auxz00_9128;

												{	/* SawMill/regalloc.scm 726 */
													BgL_objectz00_bglt BgL_tmpz00_9129;

													BgL_tmpz00_9129 =
														((BgL_objectz00_bglt) BgL_i1262z00_5930);
													BgL_auxz00_9128 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_9129);
												}
												BgL_auxz00_9127 =
													((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9128);
											}
											BgL_arg2402z00_6126 =
												(((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9127))->
												BgL_spillz00);
										}
										{	/* SawMill/regalloc.scm 726 */
											long BgL_basez00_6127;
											long BgL_bitz00_6128;

											{	/* SawMill/regalloc.scm 726 */
												int BgL_arg2607z00_6129;

												{
													BgL_rtl_regzf2razf2_bglt BgL_auxz00_9134;

													{
														obj_t BgL_auxz00_9135;

														{	/* SawMill/regalloc.scm 726 */
															BgL_objectz00_bglt BgL_tmpz00_9136;

															BgL_tmpz00_9136 =
																((BgL_objectz00_bglt)
																((BgL_rtl_regz00_bglt) BgL_arg2401z00_6125));
															BgL_auxz00_9135 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_9136);
														}
														BgL_auxz00_9134 =
															((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_9135);
													}
													BgL_arg2607z00_6129 =
														(((BgL_rtl_regzf2razf2_bglt)
															COBJECT(BgL_auxz00_9134))->BgL_numz00);
												}
												BgL_basez00_6127 = ((long) (BgL_arg2607z00_6129) / 8L);
											}
											{	/* SawMill/regalloc.scm 726 */
												int BgL_arg2608z00_6130;

												{
													BgL_rtl_regzf2razf2_bglt BgL_auxz00_9144;

													{
														obj_t BgL_auxz00_9145;

														{	/* SawMill/regalloc.scm 726 */
															BgL_objectz00_bglt BgL_tmpz00_9146;

															BgL_tmpz00_9146 =
																((BgL_objectz00_bglt)
																((BgL_rtl_regz00_bglt) BgL_arg2401z00_6125));
															BgL_auxz00_9145 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_9146);
														}
														BgL_auxz00_9144 =
															((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_9145);
													}
													BgL_arg2608z00_6130 =
														(((BgL_rtl_regzf2razf2_bglt)
															COBJECT(BgL_auxz00_9144))->BgL_numz00);
												}
												{	/* SawMill/regalloc.scm 726 */
													long BgL_n1z00_6131;
													long BgL_n2z00_6132;

													BgL_n1z00_6131 = (long) (BgL_arg2608z00_6130);
													BgL_n2z00_6132 = 8L;
													{	/* SawMill/regalloc.scm 726 */
														bool_t BgL_test3146z00_9153;

														{	/* SawMill/regalloc.scm 726 */
															long BgL_arg1338z00_6133;

															BgL_arg1338z00_6133 =
																(((BgL_n1z00_6131) | (BgL_n2z00_6132)) &
																-2147483648);
															BgL_test3146z00_9153 =
																(BgL_arg1338z00_6133 == 0L);
														}
														if (BgL_test3146z00_9153)
															{	/* SawMill/regalloc.scm 726 */
																int32_t BgL_arg1334z00_6134;

																{	/* SawMill/regalloc.scm 726 */
																	int32_t BgL_arg1336z00_6135;
																	int32_t BgL_arg1337z00_6136;

																	BgL_arg1336z00_6135 =
																		(int32_t) (BgL_n1z00_6131);
																	BgL_arg1337z00_6136 =
																		(int32_t) (BgL_n2z00_6132);
																	BgL_arg1334z00_6134 =
																		(BgL_arg1336z00_6135 % BgL_arg1337z00_6136);
																}
																{	/* SawMill/regalloc.scm 726 */
																	long BgL_arg1446z00_6137;

																	BgL_arg1446z00_6137 =
																		(long) (BgL_arg1334z00_6134);
																	BgL_bitz00_6128 =
																		(long) (BgL_arg1446z00_6137);
															}}
														else
															{	/* SawMill/regalloc.scm 726 */
																BgL_bitz00_6128 =
																	(BgL_n1z00_6131 % BgL_n2z00_6132);
															}
													}
												}
											}
											if (
												(BgL_basez00_6127 <
													STRING_LENGTH(
														(((BgL_regsetz00_bglt) COBJECT(
																	((BgL_regsetz00_bglt) BgL_arg2402z00_6126)))->
															BgL_stringz00))))
												{	/* SawMill/regalloc.scm 726 */
													BgL_tmpz00_9038 =
														(
														((STRING_REF(
																	(((BgL_regsetz00_bglt) COBJECT(
																				((BgL_regsetz00_bglt)
																					BgL_arg2402z00_6126)))->
																		BgL_stringz00),
																	BgL_basez00_6127)) & (1L <<
																(int) (BgL_bitz00_6128))) > 0L);
												}
											else
												{	/* SawMill/regalloc.scm 726 */
													BgL_tmpz00_9038 = ((bool_t) 0);
												}
										}
									}
								else
									{	/* SawMill/regalloc.scm 725 */
										BgL_tmpz00_9038 = ((bool_t) 0);
									}
							}
					}
					return BBOOL(BgL_tmpz00_9038);
				}
			}
		}

	}



/* cleanup-move-tree! */
	bool_t BGl_cleanupzd2movezd2treez12z12zzsaw_registerzd2allocationzd2(obj_t
		BgL_blocksz00_105)
	{
		{	/* SawMill/regalloc.scm 743 */
			{
				obj_t BgL_iz00_3224;
				obj_t BgL_bz00_3243;

				{
					obj_t BgL_l1697z00_3190;

					BgL_l1697z00_3190 = BgL_blocksz00_105;
				BgL_zc3z04anonymousza32415ze3z87_3191:
					if (PAIRP(BgL_l1697z00_3190))
						{	/* SawMill/regalloc.scm 767 */
							BgL_bz00_3243 = CAR(BgL_l1697z00_3190);
							{	/* SawMill/regalloc.scm 765 */
								obj_t BgL_g1696z00_3246;

								BgL_g1696z00_3246 =
									(((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt) BgL_bz00_3243)))->BgL_firstz00);
								{
									obj_t BgL_l1694z00_3248;

									BgL_l1694z00_3248 = BgL_g1696z00_3246;
								BgL_zc3z04anonymousza32449ze3z87_3249:
									if (PAIRP(BgL_l1694z00_3248))
										{	/* SawMill/regalloc.scm 765 */
											BgL_iz00_3224 = CAR(BgL_l1694z00_3248);
											{
												obj_t BgL_auxz00_9182;

												{	/* SawMill/regalloc.scm 760 */
													obj_t BgL_l1688z00_3227;

													BgL_l1688z00_3227 =
														(((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt) BgL_iz00_3224)))->
														BgL_argsz00);
													if (NULLP(BgL_l1688z00_3227))
														{	/* SawMill/regalloc.scm 760 */
															BgL_auxz00_9182 = BNIL;
														}
													else
														{	/* SawMill/regalloc.scm 760 */
															obj_t BgL_head1690z00_3229;

															{	/* SawMill/regalloc.scm 760 */
																obj_t BgL_arg2446z00_3241;

																{	/* SawMill/regalloc.scm 760 */
																	obj_t BgL_arg2447z00_3242;

																	BgL_arg2447z00_3242 = CAR(BgL_l1688z00_3227);
																	BgL_arg2446z00_3241 =
																		BGl_argszd2cleanupzd2movezd2treez12ze70z27zzsaw_registerzd2allocationzd2
																		(BgL_arg2447z00_3242);
																}
																BgL_head1690z00_3229 =
																	MAKE_YOUNG_PAIR(BgL_arg2446z00_3241, BNIL);
															}
															{	/* SawMill/regalloc.scm 760 */
																obj_t BgL_g1693z00_3230;

																BgL_g1693z00_3230 = CDR(BgL_l1688z00_3227);
																{
																	obj_t BgL_l1688z00_3232;
																	obj_t BgL_tail1691z00_3233;

																	BgL_l1688z00_3232 = BgL_g1693z00_3230;
																	BgL_tail1691z00_3233 = BgL_head1690z00_3229;
																BgL_zc3z04anonymousza32441ze3z87_3234:
																	if (NULLP(BgL_l1688z00_3232))
																		{	/* SawMill/regalloc.scm 760 */
																			BgL_auxz00_9182 = BgL_head1690z00_3229;
																		}
																	else
																		{	/* SawMill/regalloc.scm 760 */
																			obj_t BgL_newtail1692z00_3236;

																			{	/* SawMill/regalloc.scm 760 */
																				obj_t BgL_arg2444z00_3238;

																				{	/* SawMill/regalloc.scm 760 */
																					obj_t BgL_arg2445z00_3239;

																					BgL_arg2445z00_3239 =
																						CAR(((obj_t) BgL_l1688z00_3232));
																					BgL_arg2444z00_3238 =
																						BGl_argszd2cleanupzd2movezd2treez12ze70z27zzsaw_registerzd2allocationzd2
																						(BgL_arg2445z00_3239);
																				}
																				BgL_newtail1692z00_3236 =
																					MAKE_YOUNG_PAIR(BgL_arg2444z00_3238,
																					BNIL);
																			}
																			SET_CDR(BgL_tail1691z00_3233,
																				BgL_newtail1692z00_3236);
																			{	/* SawMill/regalloc.scm 760 */
																				obj_t BgL_arg2443z00_3237;

																				BgL_arg2443z00_3237 =
																					CDR(((obj_t) BgL_l1688z00_3232));
																				{
																					obj_t BgL_tail1691z00_9202;
																					obj_t BgL_l1688z00_9201;

																					BgL_l1688z00_9201 =
																						BgL_arg2443z00_3237;
																					BgL_tail1691z00_9202 =
																						BgL_newtail1692z00_3236;
																					BgL_tail1691z00_3233 =
																						BgL_tail1691z00_9202;
																					BgL_l1688z00_3232 = BgL_l1688z00_9201;
																					goto
																						BgL_zc3z04anonymousza32441ze3z87_3234;
																				}
																			}
																		}
																}
															}
														}
												}
												((((BgL_rtl_insz00_bglt) COBJECT(
																((BgL_rtl_insz00_bglt) BgL_iz00_3224)))->
														BgL_argsz00) = ((obj_t) BgL_auxz00_9182), BUNSPEC);
											}
											BgL_iz00_3224;
											{
												obj_t BgL_l1694z00_9205;

												BgL_l1694z00_9205 = CDR(BgL_l1694z00_3248);
												BgL_l1694z00_3248 = BgL_l1694z00_9205;
												goto BgL_zc3z04anonymousza32449ze3z87_3249;
											}
										}
									else
										{	/* SawMill/regalloc.scm 765 */
											((bool_t) 1);
										}
								}
							}
							{
								obj_t BgL_l1697z00_9208;

								BgL_l1697z00_9208 = CDR(BgL_l1697z00_3190);
								BgL_l1697z00_3190 = BgL_l1697z00_9208;
								goto BgL_zc3z04anonymousza32415ze3z87_3191;
							}
						}
					else
						{	/* SawMill/regalloc.scm 767 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* args-cleanup-move-tree!~0 */
	obj_t
		BGl_argszd2cleanupzd2movezd2treez12ze70z27zzsaw_registerzd2allocationzd2
		(obj_t BgL_az00_3200)
	{
		{	/* SawMill/regalloc.scm 756 */
			{	/* SawMill/regalloc.scm 750 */
				bool_t BgL_test3152z00_9210;

				{	/* SawMill/regalloc.scm 750 */
					obj_t BgL_classz00_5265;

					BgL_classz00_5265 = BGl_rtl_insz00zzsaw_defsz00;
					if (BGL_OBJECTP(BgL_az00_3200))
						{	/* SawMill/regalloc.scm 750 */
							BgL_objectz00_bglt BgL_arg1807z00_5267;

							BgL_arg1807z00_5267 = (BgL_objectz00_bglt) (BgL_az00_3200);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawMill/regalloc.scm 750 */
									long BgL_idxz00_5273;

									BgL_idxz00_5273 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5267);
									BgL_test3152z00_9210 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_5273 + 1L)) == BgL_classz00_5265);
								}
							else
								{	/* SawMill/regalloc.scm 750 */
									bool_t BgL_res2750z00_5298;

									{	/* SawMill/regalloc.scm 750 */
										obj_t BgL_oclassz00_5281;

										{	/* SawMill/regalloc.scm 750 */
											obj_t BgL_arg1815z00_5289;
											long BgL_arg1816z00_5290;

											BgL_arg1815z00_5289 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawMill/regalloc.scm 750 */
												long BgL_arg1817z00_5291;

												BgL_arg1817z00_5291 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5267);
												BgL_arg1816z00_5290 =
													(BgL_arg1817z00_5291 - OBJECT_TYPE);
											}
											BgL_oclassz00_5281 =
												VECTOR_REF(BgL_arg1815z00_5289, BgL_arg1816z00_5290);
										}
										{	/* SawMill/regalloc.scm 750 */
											bool_t BgL__ortest_1115z00_5282;

											BgL__ortest_1115z00_5282 =
												(BgL_classz00_5265 == BgL_oclassz00_5281);
											if (BgL__ortest_1115z00_5282)
												{	/* SawMill/regalloc.scm 750 */
													BgL_res2750z00_5298 = BgL__ortest_1115z00_5282;
												}
											else
												{	/* SawMill/regalloc.scm 750 */
													long BgL_odepthz00_5283;

													{	/* SawMill/regalloc.scm 750 */
														obj_t BgL_arg1804z00_5284;

														BgL_arg1804z00_5284 = (BgL_oclassz00_5281);
														BgL_odepthz00_5283 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_5284);
													}
													if ((1L < BgL_odepthz00_5283))
														{	/* SawMill/regalloc.scm 750 */
															obj_t BgL_arg1802z00_5286;

															{	/* SawMill/regalloc.scm 750 */
																obj_t BgL_arg1803z00_5287;

																BgL_arg1803z00_5287 = (BgL_oclassz00_5281);
																BgL_arg1802z00_5286 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5287,
																	1L);
															}
															BgL_res2750z00_5298 =
																(BgL_arg1802z00_5286 == BgL_classz00_5265);
														}
													else
														{	/* SawMill/regalloc.scm 750 */
															BgL_res2750z00_5298 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test3152z00_9210 = BgL_res2750z00_5298;
								}
						}
					else
						{	/* SawMill/regalloc.scm 750 */
							BgL_test3152z00_9210 = ((bool_t) 0);
						}
				}
				if (BgL_test3152z00_9210)
					{	/* SawMill/regalloc.scm 750 */
						{
							obj_t BgL_auxz00_9233;

							{	/* SawMill/regalloc.scm 752 */
								obj_t BgL_l1682z00_3204;

								BgL_l1682z00_3204 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_az00_3200)))->BgL_argsz00);
								if (NULLP(BgL_l1682z00_3204))
									{	/* SawMill/regalloc.scm 752 */
										BgL_auxz00_9233 = BNIL;
									}
								else
									{	/* SawMill/regalloc.scm 752 */
										obj_t BgL_head1684z00_3206;

										{	/* SawMill/regalloc.scm 752 */
											obj_t BgL_arg2431z00_3218;

											{	/* SawMill/regalloc.scm 752 */
												obj_t BgL_arg2432z00_3219;

												BgL_arg2432z00_3219 = CAR(BgL_l1682z00_3204);
												BgL_arg2431z00_3218 =
													BGl_argszd2cleanupzd2movezd2treez12ze70z27zzsaw_registerzd2allocationzd2
													(BgL_arg2432z00_3219);
											}
											BgL_head1684z00_3206 =
												MAKE_YOUNG_PAIR(BgL_arg2431z00_3218, BNIL);
										}
										{	/* SawMill/regalloc.scm 752 */
											obj_t BgL_g1687z00_3207;

											BgL_g1687z00_3207 = CDR(BgL_l1682z00_3204);
											{
												obj_t BgL_l1682z00_3209;
												obj_t BgL_tail1685z00_3210;

												BgL_l1682z00_3209 = BgL_g1687z00_3207;
												BgL_tail1685z00_3210 = BgL_head1684z00_3206;
											BgL_zc3z04anonymousza32425ze3z87_3211:
												if (NULLP(BgL_l1682z00_3209))
													{	/* SawMill/regalloc.scm 752 */
														BgL_auxz00_9233 = BgL_head1684z00_3206;
													}
												else
													{	/* SawMill/regalloc.scm 752 */
														obj_t BgL_newtail1686z00_3213;

														{	/* SawMill/regalloc.scm 752 */
															obj_t BgL_arg2429z00_3215;

															{	/* SawMill/regalloc.scm 752 */
																obj_t BgL_arg2430z00_3216;

																BgL_arg2430z00_3216 =
																	CAR(((obj_t) BgL_l1682z00_3209));
																BgL_arg2429z00_3215 =
																	BGl_argszd2cleanupzd2movezd2treez12ze70z27zzsaw_registerzd2allocationzd2
																	(BgL_arg2430z00_3216);
															}
															BgL_newtail1686z00_3213 =
																MAKE_YOUNG_PAIR(BgL_arg2429z00_3215, BNIL);
														}
														SET_CDR(BgL_tail1685z00_3210,
															BgL_newtail1686z00_3213);
														{	/* SawMill/regalloc.scm 752 */
															obj_t BgL_arg2428z00_3214;

															BgL_arg2428z00_3214 =
																CDR(((obj_t) BgL_l1682z00_3209));
															{
																obj_t BgL_tail1685z00_9253;
																obj_t BgL_l1682z00_9252;

																BgL_l1682z00_9252 = BgL_arg2428z00_3214;
																BgL_tail1685z00_9253 = BgL_newtail1686z00_3213;
																BgL_tail1685z00_3210 = BgL_tail1685z00_9253;
																BgL_l1682z00_3209 = BgL_l1682z00_9252;
																goto BgL_zc3z04anonymousza32425ze3z87_3211;
															}
														}
													}
											}
										}
									}
							}
							((((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_az00_3200)))->BgL_argsz00) =
								((obj_t) BgL_auxz00_9233), BUNSPEC);
						}
						{	/* SawMill/regalloc.scm 753 */
							bool_t BgL_test3159z00_9255;

							{	/* SawMill/regalloc.scm 753 */
								BgL_rtl_funz00_bglt BgL_arg2438z00_3223;

								BgL_arg2438z00_3223 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_az00_3200)))->BgL_funz00);
								{	/* SawMill/regalloc.scm 753 */
									obj_t BgL_classz00_5304;

									BgL_classz00_5304 = BGl_rtl_movz00zzsaw_defsz00;
									{	/* SawMill/regalloc.scm 753 */
										BgL_objectz00_bglt BgL_arg1807z00_5306;

										{	/* SawMill/regalloc.scm 753 */
											obj_t BgL_tmpz00_9258;

											BgL_tmpz00_9258 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg2438z00_3223));
											BgL_arg1807z00_5306 =
												(BgL_objectz00_bglt) (BgL_tmpz00_9258);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawMill/regalloc.scm 753 */
												long BgL_idxz00_5312;

												BgL_idxz00_5312 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5306);
												BgL_test3159z00_9255 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5312 + 3L)) == BgL_classz00_5304);
											}
										else
											{	/* SawMill/regalloc.scm 753 */
												bool_t BgL_res2751z00_5337;

												{	/* SawMill/regalloc.scm 753 */
													obj_t BgL_oclassz00_5320;

													{	/* SawMill/regalloc.scm 753 */
														obj_t BgL_arg1815z00_5328;
														long BgL_arg1816z00_5329;

														BgL_arg1815z00_5328 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawMill/regalloc.scm 753 */
															long BgL_arg1817z00_5330;

															BgL_arg1817z00_5330 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5306);
															BgL_arg1816z00_5329 =
																(BgL_arg1817z00_5330 - OBJECT_TYPE);
														}
														BgL_oclassz00_5320 =
															VECTOR_REF(BgL_arg1815z00_5328,
															BgL_arg1816z00_5329);
													}
													{	/* SawMill/regalloc.scm 753 */
														bool_t BgL__ortest_1115z00_5321;

														BgL__ortest_1115z00_5321 =
															(BgL_classz00_5304 == BgL_oclassz00_5320);
														if (BgL__ortest_1115z00_5321)
															{	/* SawMill/regalloc.scm 753 */
																BgL_res2751z00_5337 = BgL__ortest_1115z00_5321;
															}
														else
															{	/* SawMill/regalloc.scm 753 */
																long BgL_odepthz00_5322;

																{	/* SawMill/regalloc.scm 753 */
																	obj_t BgL_arg1804z00_5323;

																	BgL_arg1804z00_5323 = (BgL_oclassz00_5320);
																	BgL_odepthz00_5322 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_5323);
																}
																if ((3L < BgL_odepthz00_5322))
																	{	/* SawMill/regalloc.scm 753 */
																		obj_t BgL_arg1802z00_5325;

																		{	/* SawMill/regalloc.scm 753 */
																			obj_t BgL_arg1803z00_5326;

																			BgL_arg1803z00_5326 =
																				(BgL_oclassz00_5320);
																			BgL_arg1802z00_5325 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_5326, 3L);
																		}
																		BgL_res2751z00_5337 =
																			(BgL_arg1802z00_5325 ==
																			BgL_classz00_5304);
																	}
																else
																	{	/* SawMill/regalloc.scm 753 */
																		BgL_res2751z00_5337 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test3159z00_9255 = BgL_res2751z00_5337;
											}
									}
								}
							}
							if (BgL_test3159z00_9255)
								{	/* SawMill/regalloc.scm 754 */
									obj_t BgL_pairz00_5338;

									BgL_pairz00_5338 =
										(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_az00_3200)))->BgL_argsz00);
									return CAR(BgL_pairz00_5338);
								}
							else
								{	/* SawMill/regalloc.scm 753 */
									return BgL_az00_3200;
								}
						}
					}
				else
					{	/* SawMill/regalloc.scm 750 */
						return BgL_az00_3200;
					}
			}
		}

	}



/* use-register! */
	BgL_rtl_regz00_bglt
		BGl_usezd2registerz12zc0zzsaw_registerzd2allocationzd2(BgL_rtl_regz00_bglt
		BgL_regz00_106)
	{
		{	/* SawMill/regalloc.scm 776 */
			{
				int BgL_auxz00_9290;
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_9284;

				{	/* SawMill/regalloc.scm 778 */
					int BgL_arg2453z00_3259;

					{
						BgL_rtl_regzf2razf2_bglt BgL_auxz00_9291;

						{
							obj_t BgL_auxz00_9292;

							{	/* SawMill/regalloc.scm 778 */
								BgL_objectz00_bglt BgL_tmpz00_9293;

								BgL_tmpz00_9293 = ((BgL_objectz00_bglt) BgL_regz00_106);
								BgL_auxz00_9292 = BGL_OBJECT_WIDENING(BgL_tmpz00_9293);
							}
							BgL_auxz00_9291 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_9292);
						}
						BgL_arg2453z00_3259 =
							(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_9291))->
							BgL_occurrencesz00);
					}
					BgL_auxz00_9290 = (int) ((1L + (long) (BgL_arg2453z00_3259)));
				}
				{
					obj_t BgL_auxz00_9285;

					{	/* SawMill/regalloc.scm 778 */
						BgL_objectz00_bglt BgL_tmpz00_9286;

						BgL_tmpz00_9286 = ((BgL_objectz00_bglt) BgL_regz00_106);
						BgL_auxz00_9285 = BGL_OBJECT_WIDENING(BgL_tmpz00_9286);
					}
					BgL_auxz00_9284 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_9285);
				}
				((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_9284))->
						BgL_occurrencesz00) = ((int) BgL_auxz00_9290), BUNSPEC);
			}
			return BgL_regz00_106;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_registerzd2allocationzd2(void)
	{
		{	/* SawMill/regalloc.scm 15 */
			{	/* SawMill/regalloc.scm 34 */
				obj_t BgL_arg2459z00_3264;
				obj_t BgL_arg2460z00_3265;

				{	/* SawMill/regalloc.scm 34 */
					obj_t BgL_v1705z00_3291;

					BgL_v1705z00_3291 = create_vector(1L);
					{	/* SawMill/regalloc.scm 34 */
						obj_t BgL_arg2473z00_3292;

						BgL_arg2473z00_3292 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(0),
							BGl_proc2810z00zzsaw_registerzd2allocationzd2,
							BGl_proc2809z00zzsaw_registerzd2allocationzd2, ((bool_t) 0),
							((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1705z00_3291, 0L, BgL_arg2473z00_3292);
					}
					BgL_arg2459z00_3264 = BgL_v1705z00_3291;
				}
				{	/* SawMill/regalloc.scm 34 */
					obj_t BgL_v1706z00_3302;

					BgL_v1706z00_3302 = create_vector(0L);
					BgL_arg2460z00_3265 = BgL_v1706z00_3302;
				}
				BGl_blockzf2razf2zzsaw_registerzd2allocationzd2 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(2),
					CNST_TABLE_REF(3), BGl_blockz00zzsaw_defsz00, 29229L,
					BGl_proc2814z00zzsaw_registerzd2allocationzd2,
					BGl_proc2813z00zzsaw_registerzd2allocationzd2, BFALSE,
					BGl_proc2812z00zzsaw_registerzd2allocationzd2,
					BGl_proc2811z00zzsaw_registerzd2allocationzd2, BgL_arg2459z00_3264,
					BgL_arg2460z00_3265);
			}
			{	/* SawMill/regalloc.scm 37 */
				obj_t BgL_arg2483z00_3311;
				obj_t BgL_arg2484z00_3312;

				{	/* SawMill/regalloc.scm 37 */
					obj_t BgL_v1707z00_3342;

					BgL_v1707z00_3342 = create_vector(4L);
					{	/* SawMill/regalloc.scm 37 */
						obj_t BgL_arg2500z00_3343;

						BgL_arg2500z00_3343 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc2817z00zzsaw_registerzd2allocationzd2,
							BGl_proc2816z00zzsaw_registerzd2allocationzd2, ((bool_t) 0),
							((bool_t) 0), BFALSE,
							BGl_proc2815z00zzsaw_registerzd2allocationzd2, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1707z00_3342, 0L, BgL_arg2500z00_3343);
					}
					{	/* SawMill/regalloc.scm 37 */
						obj_t BgL_arg2508z00_3356;

						BgL_arg2508z00_3356 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc2820z00zzsaw_registerzd2allocationzd2,
							BGl_proc2819z00zzsaw_registerzd2allocationzd2, ((bool_t) 0),
							((bool_t) 0), BFALSE,
							BGl_proc2818z00zzsaw_registerzd2allocationzd2, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1707z00_3342, 1L, BgL_arg2508z00_3356);
					}
					{	/* SawMill/regalloc.scm 37 */
						obj_t BgL_arg2515z00_3369;

						BgL_arg2515z00_3369 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc2823z00zzsaw_registerzd2allocationzd2,
							BGl_proc2822z00zzsaw_registerzd2allocationzd2, ((bool_t) 0),
							((bool_t) 0), BFALSE,
							BGl_proc2821z00zzsaw_registerzd2allocationzd2, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1707z00_3342, 2L, BgL_arg2515z00_3369);
					}
					{	/* SawMill/regalloc.scm 37 */
						obj_t BgL_arg2524z00_3382;

						BgL_arg2524z00_3382 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc2826z00zzsaw_registerzd2allocationzd2,
							BGl_proc2825z00zzsaw_registerzd2allocationzd2, ((bool_t) 0),
							((bool_t) 0), BFALSE,
							BGl_proc2824z00zzsaw_registerzd2allocationzd2, CNST_TABLE_REF(5));
						VECTOR_SET(BgL_v1707z00_3342, 3L, BgL_arg2524z00_3382);
					}
					BgL_arg2483z00_3311 = BgL_v1707z00_3342;
				}
				{	/* SawMill/regalloc.scm 37 */
					obj_t BgL_v1708z00_3395;

					BgL_v1708z00_3395 = create_vector(0L);
					BgL_arg2484z00_3312 = BgL_v1708z00_3395;
				}
				return (BGl_rtl_inszf2razf2zzsaw_registerzd2allocationzd2 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(9),
						CNST_TABLE_REF(3), BGl_rtl_insz00zzsaw_defsz00, 22325L,
						BGl_proc2830z00zzsaw_registerzd2allocationzd2,
						BGl_proc2829z00zzsaw_registerzd2allocationzd2, BFALSE,
						BGl_proc2828z00zzsaw_registerzd2allocationzd2,
						BGl_proc2827z00zzsaw_registerzd2allocationzd2, BgL_arg2483z00_3311,
						BgL_arg2484z00_3312), BUNSPEC);
			}
		}

	}



/* &lambda2492 */
	BgL_rtl_insz00_bglt BGl_z62lambda2492z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5954, obj_t BgL_o1176z00_5955)
	{
		{	/* SawMill/regalloc.scm 37 */
			{	/* SawMill/regalloc.scm 37 */
				long BgL_arg2493z00_6139;

				{	/* SawMill/regalloc.scm 37 */
					obj_t BgL_arg2495z00_6140;

					{	/* SawMill/regalloc.scm 37 */
						obj_t BgL_arg2497z00_6141;

						{	/* SawMill/regalloc.scm 37 */
							obj_t BgL_arg1815z00_6142;
							long BgL_arg1816z00_6143;

							BgL_arg1815z00_6142 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/regalloc.scm 37 */
								long BgL_arg1817z00_6144;

								BgL_arg1817z00_6144 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt) BgL_o1176z00_5955)));
								BgL_arg1816z00_6143 = (BgL_arg1817z00_6144 - OBJECT_TYPE);
							}
							BgL_arg2497z00_6141 =
								VECTOR_REF(BgL_arg1815z00_6142, BgL_arg1816z00_6143);
						}
						BgL_arg2495z00_6140 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2497z00_6141);
					}
					{	/* SawMill/regalloc.scm 37 */
						obj_t BgL_tmpz00_9339;

						BgL_tmpz00_9339 = ((obj_t) BgL_arg2495z00_6140);
						BgL_arg2493z00_6139 = BGL_CLASS_NUM(BgL_tmpz00_9339);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_rtl_insz00_bglt) BgL_o1176z00_5955)), BgL_arg2493z00_6139);
			}
			{	/* SawMill/regalloc.scm 37 */
				BgL_objectz00_bglt BgL_tmpz00_9345;

				BgL_tmpz00_9345 =
					((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1176z00_5955));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9345, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1176z00_5955));
			return ((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1176z00_5955));
		}

	}



/* &<@anonymous:2491> */
	obj_t BGl_z62zc3z04anonymousza32491ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5956, obj_t BgL_new1175z00_5957)
	{
		{	/* SawMill/regalloc.scm 37 */
			{
				BgL_rtl_insz00_bglt BgL_auxz00_9353;

				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt)
									((BgL_rtl_insz00_bglt) BgL_new1175z00_5957))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_insz00_bglt)
							COBJECT(((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt)
										BgL_new1175z00_5957))))->BgL_z52spillz52) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_rtl_insz00_bglt)
							COBJECT(((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt)
										BgL_new1175z00_5957))))->BgL_destz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_rtl_funz00_bglt BgL_auxz00_9363;

					{	/* SawMill/regalloc.scm 37 */
						obj_t BgL_classz00_6146;

						BgL_classz00_6146 = BGl_rtl_funz00zzsaw_defsz00;
						{	/* SawMill/regalloc.scm 37 */
							obj_t BgL__ortest_1117z00_6147;

							BgL__ortest_1117z00_6147 = BGL_CLASS_NIL(BgL_classz00_6146);
							if (CBOOL(BgL__ortest_1117z00_6147))
								{	/* SawMill/regalloc.scm 37 */
									BgL_auxz00_9363 =
										((BgL_rtl_funz00_bglt) BgL__ortest_1117z00_6147);
								}
							else
								{	/* SawMill/regalloc.scm 37 */
									BgL_auxz00_9363 =
										((BgL_rtl_funz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_6146));
								}
						}
					}
					((((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt)
										((BgL_rtl_insz00_bglt) BgL_new1175z00_5957))))->
							BgL_funz00) = ((BgL_rtl_funz00_bglt) BgL_auxz00_9363), BUNSPEC);
				}
				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt)
									((BgL_rtl_insz00_bglt) BgL_new1175z00_5957))))->BgL_argsz00) =
					((obj_t) BNIL), BUNSPEC);
				{
					BgL_rtl_inszf2razf2_bglt BgL_auxz00_9376;

					{
						obj_t BgL_auxz00_9377;

						{	/* SawMill/regalloc.scm 37 */
							BgL_objectz00_bglt BgL_tmpz00_9378;

							BgL_tmpz00_9378 =
								((BgL_objectz00_bglt)
								((BgL_rtl_insz00_bglt) BgL_new1175z00_5957));
							BgL_auxz00_9377 = BGL_OBJECT_WIDENING(BgL_tmpz00_9378);
						}
						BgL_auxz00_9376 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9377);
					}
					((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9376))->BgL_defz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_rtl_inszf2razf2_bglt BgL_auxz00_9384;

					{
						obj_t BgL_auxz00_9385;

						{	/* SawMill/regalloc.scm 37 */
							BgL_objectz00_bglt BgL_tmpz00_9386;

							BgL_tmpz00_9386 =
								((BgL_objectz00_bglt)
								((BgL_rtl_insz00_bglt) BgL_new1175z00_5957));
							BgL_auxz00_9385 = BGL_OBJECT_WIDENING(BgL_tmpz00_9386);
						}
						BgL_auxz00_9384 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9385);
					}
					((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9384))->BgL_outz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_rtl_inszf2razf2_bglt BgL_auxz00_9392;

					{
						obj_t BgL_auxz00_9393;

						{	/* SawMill/regalloc.scm 37 */
							BgL_objectz00_bglt BgL_tmpz00_9394;

							BgL_tmpz00_9394 =
								((BgL_objectz00_bglt)
								((BgL_rtl_insz00_bglt) BgL_new1175z00_5957));
							BgL_auxz00_9393 = BGL_OBJECT_WIDENING(BgL_tmpz00_9394);
						}
						BgL_auxz00_9392 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9393);
					}
					((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9392))->BgL_inz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_rtl_inszf2razf2_bglt BgL_auxz00_9400;

					{
						obj_t BgL_auxz00_9401;

						{	/* SawMill/regalloc.scm 37 */
							BgL_objectz00_bglt BgL_tmpz00_9402;

							BgL_tmpz00_9402 =
								((BgL_objectz00_bglt)
								((BgL_rtl_insz00_bglt) BgL_new1175z00_5957));
							BgL_auxz00_9401 = BGL_OBJECT_WIDENING(BgL_tmpz00_9402);
						}
						BgL_auxz00_9400 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9401);
					}
					((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9400))->
							BgL_spillz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_9353 = ((BgL_rtl_insz00_bglt) BgL_new1175z00_5957);
				return ((obj_t) BgL_auxz00_9353);
			}
		}

	}



/* &lambda2488 */
	BgL_rtl_insz00_bglt BGl_z62lambda2488z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5958, obj_t BgL_o1172z00_5959)
	{
		{	/* SawMill/regalloc.scm 37 */
			{	/* SawMill/regalloc.scm 37 */
				BgL_rtl_inszf2razf2_bglt BgL_wide1174z00_6149;

				BgL_wide1174z00_6149 =
					((BgL_rtl_inszf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_inszf2razf2_bgl))));
				{	/* SawMill/regalloc.scm 37 */
					obj_t BgL_auxz00_9415;
					BgL_objectz00_bglt BgL_tmpz00_9411;

					BgL_auxz00_9415 = ((obj_t) BgL_wide1174z00_6149);
					BgL_tmpz00_9411 =
						((BgL_objectz00_bglt)
						((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1172z00_5959)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9411, BgL_auxz00_9415);
				}
				((BgL_objectz00_bglt)
					((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1172z00_5959)));
				{	/* SawMill/regalloc.scm 37 */
					long BgL_arg2490z00_6150;

					BgL_arg2490z00_6150 =
						BGL_CLASS_NUM(BGl_rtl_inszf2razf2zzsaw_registerzd2allocationzd2);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_rtl_insz00_bglt)
								((BgL_rtl_insz00_bglt) BgL_o1172z00_5959))),
						BgL_arg2490z00_6150);
				}
				return
					((BgL_rtl_insz00_bglt)
					((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1172z00_5959)));
			}
		}

	}



/* &lambda2485 */
	BgL_rtl_insz00_bglt BGl_z62lambda2485z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5960, obj_t BgL_loc1163z00_5961, obj_t BgL_z52spill1164z52_5962,
		obj_t BgL_dest1165z00_5963, obj_t BgL_fun1166z00_5964,
		obj_t BgL_args1167z00_5965, obj_t BgL_def1168z00_5966,
		obj_t BgL_out1169z00_5967, obj_t BgL_in1170z00_5968,
		obj_t BgL_spill1171z00_5969)
	{
		{	/* SawMill/regalloc.scm 37 */
			{	/* SawMill/regalloc.scm 37 */
				BgL_rtl_insz00_bglt BgL_new1282z00_6154;

				{	/* SawMill/regalloc.scm 37 */
					BgL_rtl_insz00_bglt BgL_tmp1280z00_6155;
					BgL_rtl_inszf2razf2_bglt BgL_wide1281z00_6156;

					{
						BgL_rtl_insz00_bglt BgL_auxz00_9429;

						{	/* SawMill/regalloc.scm 37 */
							BgL_rtl_insz00_bglt BgL_new1279z00_6157;

							BgL_new1279z00_6157 =
								((BgL_rtl_insz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_insz00_bgl))));
							{	/* SawMill/regalloc.scm 37 */
								long BgL_arg2487z00_6158;

								BgL_arg2487z00_6158 =
									BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1279z00_6157),
									BgL_arg2487z00_6158);
							}
							{	/* SawMill/regalloc.scm 37 */
								BgL_objectz00_bglt BgL_tmpz00_9434;

								BgL_tmpz00_9434 = ((BgL_objectz00_bglt) BgL_new1279z00_6157);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9434, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1279z00_6157);
							BgL_auxz00_9429 = BgL_new1279z00_6157;
						}
						BgL_tmp1280z00_6155 = ((BgL_rtl_insz00_bglt) BgL_auxz00_9429);
					}
					BgL_wide1281z00_6156 =
						((BgL_rtl_inszf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_inszf2razf2_bgl))));
					{	/* SawMill/regalloc.scm 37 */
						obj_t BgL_auxz00_9442;
						BgL_objectz00_bglt BgL_tmpz00_9440;

						BgL_auxz00_9442 = ((obj_t) BgL_wide1281z00_6156);
						BgL_tmpz00_9440 = ((BgL_objectz00_bglt) BgL_tmp1280z00_6155);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9440, BgL_auxz00_9442);
					}
					((BgL_objectz00_bglt) BgL_tmp1280z00_6155);
					{	/* SawMill/regalloc.scm 37 */
						long BgL_arg2486z00_6159;

						BgL_arg2486z00_6159 =
							BGL_CLASS_NUM(BGl_rtl_inszf2razf2zzsaw_registerzd2allocationzd2);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1280z00_6155), BgL_arg2486z00_6159);
					}
					BgL_new1282z00_6154 = ((BgL_rtl_insz00_bglt) BgL_tmp1280z00_6155);
				}
				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_new1282z00_6154)))->BgL_locz00) =
					((obj_t) BgL_loc1163z00_5961), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1282z00_6154)))->BgL_z52spillz52) =
					((obj_t) ((obj_t) BgL_z52spill1164z52_5962)), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1282z00_6154)))->BgL_destz00) =
					((obj_t) BgL_dest1165z00_5963), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1282z00_6154)))->BgL_funz00) =
					((BgL_rtl_funz00_bglt) ((BgL_rtl_funz00_bglt) BgL_fun1166z00_5964)),
					BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1282z00_6154)))->BgL_argsz00) =
					((obj_t) ((obj_t) BgL_args1167z00_5965)), BUNSPEC);
				{
					BgL_rtl_inszf2razf2_bglt BgL_auxz00_9463;

					{
						obj_t BgL_auxz00_9464;

						{	/* SawMill/regalloc.scm 37 */
							BgL_objectz00_bglt BgL_tmpz00_9465;

							BgL_tmpz00_9465 = ((BgL_objectz00_bglt) BgL_new1282z00_6154);
							BgL_auxz00_9464 = BGL_OBJECT_WIDENING(BgL_tmpz00_9465);
						}
						BgL_auxz00_9463 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9464);
					}
					((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9463))->BgL_defz00) =
						((obj_t) BgL_def1168z00_5966), BUNSPEC);
				}
				{
					BgL_rtl_inszf2razf2_bglt BgL_auxz00_9470;

					{
						obj_t BgL_auxz00_9471;

						{	/* SawMill/regalloc.scm 37 */
							BgL_objectz00_bglt BgL_tmpz00_9472;

							BgL_tmpz00_9472 = ((BgL_objectz00_bglt) BgL_new1282z00_6154);
							BgL_auxz00_9471 = BGL_OBJECT_WIDENING(BgL_tmpz00_9472);
						}
						BgL_auxz00_9470 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9471);
					}
					((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9470))->BgL_outz00) =
						((obj_t) BgL_out1169z00_5967), BUNSPEC);
				}
				{
					BgL_rtl_inszf2razf2_bglt BgL_auxz00_9477;

					{
						obj_t BgL_auxz00_9478;

						{	/* SawMill/regalloc.scm 37 */
							BgL_objectz00_bglt BgL_tmpz00_9479;

							BgL_tmpz00_9479 = ((BgL_objectz00_bglt) BgL_new1282z00_6154);
							BgL_auxz00_9478 = BGL_OBJECT_WIDENING(BgL_tmpz00_9479);
						}
						BgL_auxz00_9477 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9478);
					}
					((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9477))->BgL_inz00) =
						((obj_t) BgL_in1170z00_5968), BUNSPEC);
				}
				{
					BgL_rtl_inszf2razf2_bglt BgL_auxz00_9484;

					{
						obj_t BgL_auxz00_9485;

						{	/* SawMill/regalloc.scm 37 */
							BgL_objectz00_bglt BgL_tmpz00_9486;

							BgL_tmpz00_9486 = ((BgL_objectz00_bglt) BgL_new1282z00_6154);
							BgL_auxz00_9485 = BGL_OBJECT_WIDENING(BgL_tmpz00_9486);
						}
						BgL_auxz00_9484 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9485);
					}
					((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9484))->
							BgL_spillz00) = ((obj_t) BgL_spill1171z00_5969), BUNSPEC);
				}
				return BgL_new1282z00_6154;
			}
		}

	}



/* &<@anonymous:2530> */
	obj_t BGl_z62zc3z04anonymousza32530ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5970)
	{
		{	/* SawMill/regalloc.scm 37 */
			return BUNSPEC;
		}

	}



/* &lambda2529 */
	obj_t BGl_z62lambda2529z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5971, obj_t BgL_oz00_5972, obj_t BgL_vz00_5973)
	{
		{	/* SawMill/regalloc.scm 37 */
			{
				BgL_rtl_inszf2razf2_bglt BgL_auxz00_9491;

				{
					obj_t BgL_auxz00_9492;

					{	/* SawMill/regalloc.scm 37 */
						BgL_objectz00_bglt BgL_tmpz00_9493;

						BgL_tmpz00_9493 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_5972));
						BgL_auxz00_9492 = BGL_OBJECT_WIDENING(BgL_tmpz00_9493);
					}
					BgL_auxz00_9491 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9492);
				}
				return
					((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9491))->
						BgL_spillz00) = ((obj_t) BgL_vz00_5973), BUNSPEC);
			}
		}

	}



/* &lambda2528 */
	obj_t BGl_z62lambda2528z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5974, obj_t BgL_oz00_5975)
	{
		{	/* SawMill/regalloc.scm 37 */
			{
				BgL_rtl_inszf2razf2_bglt BgL_auxz00_9499;

				{
					obj_t BgL_auxz00_9500;

					{	/* SawMill/regalloc.scm 37 */
						BgL_objectz00_bglt BgL_tmpz00_9501;

						BgL_tmpz00_9501 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_5975));
						BgL_auxz00_9500 = BGL_OBJECT_WIDENING(BgL_tmpz00_9501);
					}
					BgL_auxz00_9499 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9500);
				}
				return
					(((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9499))->BgL_spillz00);
			}
		}

	}



/* &<@anonymous:2522> */
	obj_t BGl_z62zc3z04anonymousza32522ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5976)
	{
		{	/* SawMill/regalloc.scm 37 */
			return BUNSPEC;
		}

	}



/* &lambda2521 */
	obj_t BGl_z62lambda2521z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5977, obj_t BgL_oz00_5978, obj_t BgL_vz00_5979)
	{
		{	/* SawMill/regalloc.scm 37 */
			{
				BgL_rtl_inszf2razf2_bglt BgL_auxz00_9507;

				{
					obj_t BgL_auxz00_9508;

					{	/* SawMill/regalloc.scm 37 */
						BgL_objectz00_bglt BgL_tmpz00_9509;

						BgL_tmpz00_9509 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_5978));
						BgL_auxz00_9508 = BGL_OBJECT_WIDENING(BgL_tmpz00_9509);
					}
					BgL_auxz00_9507 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9508);
				}
				return
					((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9507))->BgL_inz00) =
					((obj_t) BgL_vz00_5979), BUNSPEC);
			}
		}

	}



/* &lambda2520 */
	obj_t BGl_z62lambda2520z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5980, obj_t BgL_oz00_5981)
	{
		{	/* SawMill/regalloc.scm 37 */
			{
				BgL_rtl_inszf2razf2_bglt BgL_auxz00_9515;

				{
					obj_t BgL_auxz00_9516;

					{	/* SawMill/regalloc.scm 37 */
						BgL_objectz00_bglt BgL_tmpz00_9517;

						BgL_tmpz00_9517 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_5981));
						BgL_auxz00_9516 = BGL_OBJECT_WIDENING(BgL_tmpz00_9517);
					}
					BgL_auxz00_9515 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9516);
				}
				return
					(((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9515))->BgL_inz00);
			}
		}

	}



/* &<@anonymous:2514> */
	obj_t BGl_z62zc3z04anonymousza32514ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5982)
	{
		{	/* SawMill/regalloc.scm 37 */
			return BUNSPEC;
		}

	}



/* &lambda2513 */
	obj_t BGl_z62lambda2513z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5983, obj_t BgL_oz00_5984, obj_t BgL_vz00_5985)
	{
		{	/* SawMill/regalloc.scm 37 */
			{
				BgL_rtl_inszf2razf2_bglt BgL_auxz00_9523;

				{
					obj_t BgL_auxz00_9524;

					{	/* SawMill/regalloc.scm 37 */
						BgL_objectz00_bglt BgL_tmpz00_9525;

						BgL_tmpz00_9525 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_5984));
						BgL_auxz00_9524 = BGL_OBJECT_WIDENING(BgL_tmpz00_9525);
					}
					BgL_auxz00_9523 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9524);
				}
				return
					((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9523))->BgL_outz00) =
					((obj_t) BgL_vz00_5985), BUNSPEC);
			}
		}

	}



/* &lambda2512 */
	obj_t BGl_z62lambda2512z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5986, obj_t BgL_oz00_5987)
	{
		{	/* SawMill/regalloc.scm 37 */
			{
				BgL_rtl_inszf2razf2_bglt BgL_auxz00_9531;

				{
					obj_t BgL_auxz00_9532;

					{	/* SawMill/regalloc.scm 37 */
						BgL_objectz00_bglt BgL_tmpz00_9533;

						BgL_tmpz00_9533 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_5987));
						BgL_auxz00_9532 = BGL_OBJECT_WIDENING(BgL_tmpz00_9533);
					}
					BgL_auxz00_9531 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9532);
				}
				return
					(((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9531))->BgL_outz00);
			}
		}

	}



/* &<@anonymous:2506> */
	obj_t BGl_z62zc3z04anonymousza32506ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5988)
	{
		{	/* SawMill/regalloc.scm 37 */
			return BUNSPEC;
		}

	}



/* &lambda2505 */
	obj_t BGl_z62lambda2505z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5989, obj_t BgL_oz00_5990, obj_t BgL_vz00_5991)
	{
		{	/* SawMill/regalloc.scm 37 */
			{
				BgL_rtl_inszf2razf2_bglt BgL_auxz00_9539;

				{
					obj_t BgL_auxz00_9540;

					{	/* SawMill/regalloc.scm 37 */
						BgL_objectz00_bglt BgL_tmpz00_9541;

						BgL_tmpz00_9541 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_5990));
						BgL_auxz00_9540 = BGL_OBJECT_WIDENING(BgL_tmpz00_9541);
					}
					BgL_auxz00_9539 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9540);
				}
				return
					((((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9539))->BgL_defz00) =
					((obj_t) BgL_vz00_5991), BUNSPEC);
			}
		}

	}



/* &lambda2504 */
	obj_t BGl_z62lambda2504z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5992, obj_t BgL_oz00_5993)
	{
		{	/* SawMill/regalloc.scm 37 */
			{
				BgL_rtl_inszf2razf2_bglt BgL_auxz00_9547;

				{
					obj_t BgL_auxz00_9548;

					{	/* SawMill/regalloc.scm 37 */
						BgL_objectz00_bglt BgL_tmpz00_9549;

						BgL_tmpz00_9549 =
							((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_oz00_5993));
						BgL_auxz00_9548 = BGL_OBJECT_WIDENING(BgL_tmpz00_9549);
					}
					BgL_auxz00_9547 = ((BgL_rtl_inszf2razf2_bglt) BgL_auxz00_9548);
				}
				return
					(((BgL_rtl_inszf2razf2_bglt) COBJECT(BgL_auxz00_9547))->BgL_defz00);
			}
		}

	}



/* &lambda2467 */
	BgL_blockz00_bglt BGl_z62lambda2467z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5994, obj_t BgL_o1161z00_5995)
	{
		{	/* SawMill/regalloc.scm 34 */
			{	/* SawMill/regalloc.scm 34 */
				long BgL_arg2469z00_6169;

				{	/* SawMill/regalloc.scm 34 */
					obj_t BgL_arg2470z00_6170;

					{	/* SawMill/regalloc.scm 34 */
						obj_t BgL_arg2471z00_6171;

						{	/* SawMill/regalloc.scm 34 */
							obj_t BgL_arg1815z00_6172;
							long BgL_arg1816z00_6173;

							BgL_arg1815z00_6172 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/regalloc.scm 34 */
								long BgL_arg1817z00_6174;

								BgL_arg1817z00_6174 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_o1161z00_5995)));
								BgL_arg1816z00_6173 = (BgL_arg1817z00_6174 - OBJECT_TYPE);
							}
							BgL_arg2471z00_6171 =
								VECTOR_REF(BgL_arg1815z00_6172, BgL_arg1816z00_6173);
						}
						BgL_arg2470z00_6170 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg2471z00_6171);
					}
					{	/* SawMill/regalloc.scm 34 */
						obj_t BgL_tmpz00_9562;

						BgL_tmpz00_9562 = ((obj_t) BgL_arg2470z00_6170);
						BgL_arg2469z00_6169 = BGL_CLASS_NUM(BgL_tmpz00_9562);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) BgL_o1161z00_5995)), BgL_arg2469z00_6169);
			}
			{	/* SawMill/regalloc.scm 34 */
				BgL_objectz00_bglt BgL_tmpz00_9568;

				BgL_tmpz00_9568 =
					((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1161z00_5995));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9568, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1161z00_5995));
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1161z00_5995));
		}

	}



/* &<@anonymous:2466> */
	obj_t BGl_z62zc3z04anonymousza32466ze3ze5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5996, obj_t BgL_new1160z00_5997)
	{
		{	/* SawMill/regalloc.scm 34 */
			{
				BgL_blockz00_bglt BgL_auxz00_9576;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_new1160z00_5997))))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1160z00_5997))))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1160z00_5997))))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1160z00_5997))))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				{
					BgL_blockzf2razf2_bglt BgL_auxz00_9590;

					{
						obj_t BgL_auxz00_9591;

						{	/* SawMill/regalloc.scm 34 */
							BgL_objectz00_bglt BgL_tmpz00_9592;

							BgL_tmpz00_9592 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1160z00_5997));
							BgL_auxz00_9591 = BGL_OBJECT_WIDENING(BgL_tmpz00_9592);
						}
						BgL_auxz00_9590 = ((BgL_blockzf2razf2_bglt) BgL_auxz00_9591);
					}
					((((BgL_blockzf2razf2_bglt) COBJECT(BgL_auxz00_9590))->BgL_lastz00) =
						((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_9576 = ((BgL_blockz00_bglt) BgL_new1160z00_5997);
				return ((obj_t) BgL_auxz00_9576);
			}
		}

	}



/* &lambda2464 */
	BgL_blockz00_bglt BGl_z62lambda2464z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_5998, obj_t BgL_o1157z00_5999)
	{
		{	/* SawMill/regalloc.scm 34 */
			{	/* SawMill/regalloc.scm 34 */
				BgL_blockzf2razf2_bglt BgL_wide1159z00_6177;

				BgL_wide1159z00_6177 =
					((BgL_blockzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_blockzf2razf2_bgl))));
				{	/* SawMill/regalloc.scm 34 */
					obj_t BgL_auxz00_9605;
					BgL_objectz00_bglt BgL_tmpz00_9601;

					BgL_auxz00_9605 = ((obj_t) BgL_wide1159z00_6177);
					BgL_tmpz00_9601 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1157z00_5999)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9601, BgL_auxz00_9605);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1157z00_5999)));
				{	/* SawMill/regalloc.scm 34 */
					long BgL_arg2465z00_6178;

					BgL_arg2465z00_6178 =
						BGL_CLASS_NUM(BGl_blockzf2razf2zzsaw_registerzd2allocationzd2);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_o1157z00_5999))), BgL_arg2465z00_6178);
				}
				return
					((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1157z00_5999)));
			}
		}

	}



/* &lambda2461 */
	BgL_blockz00_bglt BGl_z62lambda2461z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_6000, obj_t BgL_label1152z00_6001, obj_t BgL_preds1153z00_6002,
		obj_t BgL_succs1154z00_6003, obj_t BgL_first1155z00_6004,
		obj_t BgL_last1156z00_6005)
	{
		{	/* SawMill/regalloc.scm 34 */
			{	/* SawMill/regalloc.scm 34 */
				int BgL_label1152z00_6179;

				BgL_label1152z00_6179 = CINT(BgL_label1152z00_6001);
				{	/* SawMill/regalloc.scm 34 */
					BgL_blockz00_bglt BgL_new1276z00_6184;

					{	/* SawMill/regalloc.scm 34 */
						BgL_blockz00_bglt BgL_tmp1274z00_6185;
						BgL_blockzf2razf2_bglt BgL_wide1275z00_6186;

						{
							BgL_blockz00_bglt BgL_auxz00_9620;

							{	/* SawMill/regalloc.scm 34 */
								BgL_blockz00_bglt BgL_new1273z00_6187;

								BgL_new1273z00_6187 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawMill/regalloc.scm 34 */
									long BgL_arg2463z00_6188;

									BgL_arg2463z00_6188 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1273z00_6187),
										BgL_arg2463z00_6188);
								}
								{	/* SawMill/regalloc.scm 34 */
									BgL_objectz00_bglt BgL_tmpz00_9625;

									BgL_tmpz00_9625 = ((BgL_objectz00_bglt) BgL_new1273z00_6187);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9625, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1273z00_6187);
								BgL_auxz00_9620 = BgL_new1273z00_6187;
							}
							BgL_tmp1274z00_6185 = ((BgL_blockz00_bglt) BgL_auxz00_9620);
						}
						BgL_wide1275z00_6186 =
							((BgL_blockzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_blockzf2razf2_bgl))));
						{	/* SawMill/regalloc.scm 34 */
							obj_t BgL_auxz00_9633;
							BgL_objectz00_bglt BgL_tmpz00_9631;

							BgL_auxz00_9633 = ((obj_t) BgL_wide1275z00_6186);
							BgL_tmpz00_9631 = ((BgL_objectz00_bglt) BgL_tmp1274z00_6185);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_9631, BgL_auxz00_9633);
						}
						((BgL_objectz00_bglt) BgL_tmp1274z00_6185);
						{	/* SawMill/regalloc.scm 34 */
							long BgL_arg2462z00_6189;

							BgL_arg2462z00_6189 =
								BGL_CLASS_NUM(BGl_blockzf2razf2zzsaw_registerzd2allocationzd2);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1274z00_6185),
								BgL_arg2462z00_6189);
						}
						BgL_new1276z00_6184 = ((BgL_blockz00_bglt) BgL_tmp1274z00_6185);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_new1276z00_6184)))->BgL_labelz00) =
						((int) BgL_label1152z00_6179), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1276z00_6184)))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1153z00_6002)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1276z00_6184)))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1154z00_6003)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1276z00_6184)))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1155z00_6004)), BUNSPEC);
					{
						BgL_blockzf2razf2_bglt BgL_auxz00_9652;

						{
							obj_t BgL_auxz00_9653;

							{	/* SawMill/regalloc.scm 34 */
								BgL_objectz00_bglt BgL_tmpz00_9654;

								BgL_tmpz00_9654 = ((BgL_objectz00_bglt) BgL_new1276z00_6184);
								BgL_auxz00_9653 = BGL_OBJECT_WIDENING(BgL_tmpz00_9654);
							}
							BgL_auxz00_9652 = ((BgL_blockzf2razf2_bglt) BgL_auxz00_9653);
						}
						((((BgL_blockzf2razf2_bglt) COBJECT(BgL_auxz00_9652))->
								BgL_lastz00) =
							((obj_t) ((obj_t) BgL_last1156z00_6005)), BUNSPEC);
					}
					return BgL_new1276z00_6184;
				}
			}
		}

	}



/* &lambda2477 */
	obj_t BGl_z62lambda2477z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_6006, obj_t BgL_oz00_6007, obj_t BgL_vz00_6008)
	{
		{	/* SawMill/regalloc.scm 34 */
			{
				BgL_blockzf2razf2_bglt BgL_auxz00_9660;

				{
					obj_t BgL_auxz00_9661;

					{	/* SawMill/regalloc.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_9662;

						BgL_tmpz00_9662 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_6007));
						BgL_auxz00_9661 = BGL_OBJECT_WIDENING(BgL_tmpz00_9662);
					}
					BgL_auxz00_9660 = ((BgL_blockzf2razf2_bglt) BgL_auxz00_9661);
				}
				return
					((((BgL_blockzf2razf2_bglt) COBJECT(BgL_auxz00_9660))->BgL_lastz00) =
					((obj_t) ((obj_t) BgL_vz00_6008)), BUNSPEC);
			}
		}

	}



/* &lambda2476 */
	obj_t BGl_z62lambda2476z62zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_6009, obj_t BgL_oz00_6010)
	{
		{	/* SawMill/regalloc.scm 34 */
			{
				BgL_blockzf2razf2_bglt BgL_auxz00_9669;

				{
					obj_t BgL_auxz00_9670;

					{	/* SawMill/regalloc.scm 34 */
						BgL_objectz00_bglt BgL_tmpz00_9671;

						BgL_tmpz00_9671 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_6010));
						BgL_auxz00_9670 = BGL_OBJECT_WIDENING(BgL_tmpz00_9671);
					}
					BgL_auxz00_9669 = ((BgL_blockzf2razf2_bglt) BgL_auxz00_9670);
				}
				return
					(((BgL_blockzf2razf2_bglt) COBJECT(BgL_auxz00_9669))->BgL_lastz00);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_registerzd2allocationzd2(void)
	{
		{	/* SawMill/regalloc.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_protectzd2interferencez12zd2envz12zzsaw_registerzd2allocationzd2,
				BGl_proc2831z00zzsaw_registerzd2allocationzd2, BFALSE,
				BGl_string2832z00zzsaw_registerzd2allocationzd2);
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_typezd2interferencez12zd2envz12zzsaw_registerzd2allocationzd2,
				BGl_proc2833z00zzsaw_registerzd2allocationzd2,
				BGl_backendz00zzbackend_backendz00,
				BGl_string2834z00zzsaw_registerzd2allocationzd2);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_rtlzd2siza7ezd2envza7zzsaw_registerzd2allocationzd2,
				BGl_proc2835z00zzsaw_registerzd2allocationzd2, BFALSE,
				BGl_string2836z00zzsaw_registerzd2allocationzd2);
		}

	}



/* &rtl-size1713 */
	obj_t BGl_z62rtlzd2siza7e1713z17zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_6014, obj_t BgL_oz00_6015)
	{
		{	/* SawMill/regalloc.scm 784 */
			if (PAIRP(BgL_oz00_6015))
				{	/* SawMill/regalloc.scm 786 */
					obj_t BgL_runner2586z00_6193;

					{	/* SawMill/regalloc.scm 786 */
						obj_t BgL_head1701z00_6194;

						BgL_head1701z00_6194 =
							MAKE_YOUNG_PAIR(BGl_rtlzd2siza7ez75zzsaw_registerzd2allocationzd2
							(CAR(BgL_oz00_6015)), BNIL);
						{	/* SawMill/regalloc.scm 786 */
							obj_t BgL_g1704z00_6195;

							BgL_g1704z00_6195 = CDR(BgL_oz00_6015);
							{
								obj_t BgL_l1699z00_6197;
								obj_t BgL_tail1702z00_6198;

								BgL_l1699z00_6197 = BgL_g1704z00_6195;
								BgL_tail1702z00_6198 = BgL_head1701z00_6194;
							BgL_zc3z04anonymousza32570ze3z87_6196:
								if (NULLP(BgL_l1699z00_6197))
									{	/* SawMill/regalloc.scm 786 */
										BgL_runner2586z00_6193 = BgL_head1701z00_6194;
									}
								else
									{	/* SawMill/regalloc.scm 786 */
										obj_t BgL_newtail1703z00_6199;

										{	/* SawMill/regalloc.scm 786 */
											obj_t BgL_arg2578z00_6200;

											{	/* SawMill/regalloc.scm 786 */
												obj_t BgL_arg2579z00_6201;

												BgL_arg2579z00_6201 = CAR(((obj_t) BgL_l1699z00_6197));
												BgL_arg2578z00_6200 =
													BGl_rtlzd2siza7ez75zzsaw_registerzd2allocationzd2
													(BgL_arg2579z00_6201);
											}
											BgL_newtail1703z00_6199 =
												MAKE_YOUNG_PAIR(BgL_arg2578z00_6200, BNIL);
										}
										SET_CDR(BgL_tail1702z00_6198, BgL_newtail1703z00_6199);
										{	/* SawMill/regalloc.scm 786 */
											obj_t BgL_arg2572z00_6202;

											BgL_arg2572z00_6202 = CDR(((obj_t) BgL_l1699z00_6197));
											{
												obj_t BgL_tail1702z00_9696;
												obj_t BgL_l1699z00_9695;

												BgL_l1699z00_9695 = BgL_arg2572z00_6202;
												BgL_tail1702z00_9696 = BgL_newtail1703z00_6199;
												BgL_tail1702z00_6198 = BgL_tail1702z00_9696;
												BgL_l1699z00_6197 = BgL_l1699z00_9695;
												goto BgL_zc3z04anonymousza32570ze3z87_6196;
											}
										}
									}
							}
						}
					}
					return BGl_zb2zb2zz__r4_numbers_6_5z00(BgL_runner2586z00_6193);
				}
			else
				{	/* SawMill/regalloc.scm 785 */
					return BINT(1L);
				}
		}

	}



/* &type-interference!1711 */
	obj_t BGl_z62typezd2interferencez121711za2zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_6016, obj_t BgL_backz00_6017, obj_t BgL_regsz00_6018)
	{
		{	/* SawMill/regalloc.scm 326 */
			{	/* SawMill/regalloc.scm 327 */
				bool_t BgL_tmpz00_9699;

				if (NULLP(((obj_t) BgL_regsz00_6018)))
					{	/* SawMill/regalloc.scm 327 */
						BgL_tmpz00_9699 = ((bool_t) 0);
					}
				else
					{
						obj_t BgL_regsz00_6206;

						BgL_regsz00_6206 = ((obj_t) BgL_regsz00_6018);
					BgL_loopz00_6205:
						{	/* SawMill/regalloc.scm 329 */
							bool_t BgL_test3167z00_9703;

							{	/* SawMill/regalloc.scm 329 */
								obj_t BgL_tmpz00_9704;

								BgL_tmpz00_9704 = CDR(((obj_t) BgL_regsz00_6206));
								BgL_test3167z00_9703 = PAIRP(BgL_tmpz00_9704);
							}
							if (BgL_test3167z00_9703)
								{	/* SawMill/regalloc.scm 330 */
									obj_t BgL_r1z00_6207;

									BgL_r1z00_6207 = CAR(((obj_t) BgL_regsz00_6206));
									{	/* SawMill/regalloc.scm 330 */
										BgL_typez00_bglt BgL_t1z00_6208;

										BgL_t1z00_6208 =
											(((BgL_rtl_regz00_bglt) COBJECT(
													((BgL_rtl_regz00_bglt) BgL_r1z00_6207)))->
											BgL_typez00);
										{	/* SawMill/regalloc.scm 331 */

											{	/* SawMill/regalloc.scm 332 */
												bool_t BgL_test3168z00_9712;

												{	/* SawMill/regalloc.scm 332 */
													obj_t BgL_classz00_6209;

													BgL_classz00_6209 = BGl_typez00zztype_typez00;
													{	/* SawMill/regalloc.scm 332 */
														BgL_objectz00_bglt BgL_arg1807z00_6210;

														{	/* SawMill/regalloc.scm 332 */
															obj_t BgL_tmpz00_9713;

															BgL_tmpz00_9713 =
																((obj_t) ((BgL_objectz00_bglt) BgL_t1z00_6208));
															BgL_arg1807z00_6210 =
																(BgL_objectz00_bglt) (BgL_tmpz00_9713);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawMill/regalloc.scm 332 */
																long BgL_idxz00_6211;

																BgL_idxz00_6211 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_6210);
																BgL_test3168z00_9712 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_6211 + 1L)) ==
																	BgL_classz00_6209);
															}
														else
															{	/* SawMill/regalloc.scm 332 */
																bool_t BgL_res2753z00_6214;

																{	/* SawMill/regalloc.scm 332 */
																	obj_t BgL_oclassz00_6215;

																	{	/* SawMill/regalloc.scm 332 */
																		obj_t BgL_arg1815z00_6216;
																		long BgL_arg1816z00_6217;

																		BgL_arg1815z00_6216 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawMill/regalloc.scm 332 */
																			long BgL_arg1817z00_6218;

																			BgL_arg1817z00_6218 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_6210);
																			BgL_arg1816z00_6217 =
																				(BgL_arg1817z00_6218 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_6215 =
																			VECTOR_REF(BgL_arg1815z00_6216,
																			BgL_arg1816z00_6217);
																	}
																	{	/* SawMill/regalloc.scm 332 */
																		bool_t BgL__ortest_1115z00_6219;

																		BgL__ortest_1115z00_6219 =
																			(BgL_classz00_6209 == BgL_oclassz00_6215);
																		if (BgL__ortest_1115z00_6219)
																			{	/* SawMill/regalloc.scm 332 */
																				BgL_res2753z00_6214 =
																					BgL__ortest_1115z00_6219;
																			}
																		else
																			{	/* SawMill/regalloc.scm 332 */
																				long BgL_odepthz00_6220;

																				{	/* SawMill/regalloc.scm 332 */
																					obj_t BgL_arg1804z00_6221;

																					BgL_arg1804z00_6221 =
																						(BgL_oclassz00_6215);
																					BgL_odepthz00_6220 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_6221);
																				}
																				if ((1L < BgL_odepthz00_6220))
																					{	/* SawMill/regalloc.scm 332 */
																						obj_t BgL_arg1802z00_6222;

																						{	/* SawMill/regalloc.scm 332 */
																							obj_t BgL_arg1803z00_6223;

																							BgL_arg1803z00_6223 =
																								(BgL_oclassz00_6215);
																							BgL_arg1802z00_6222 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_6223, 1L);
																						}
																						BgL_res2753z00_6214 =
																							(BgL_arg1802z00_6222 ==
																							BgL_classz00_6209);
																					}
																				else
																					{	/* SawMill/regalloc.scm 332 */
																						BgL_res2753z00_6214 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test3168z00_9712 = BgL_res2753z00_6214;
															}
													}
												}
												if (BgL_test3168z00_9712)
													{	/* SawMill/regalloc.scm 333 */
														obj_t BgL_g1592z00_6224;

														BgL_g1592z00_6224 = CDR(((obj_t) BgL_regsz00_6206));
														{
															obj_t BgL_l1590z00_6226;

															BgL_l1590z00_6226 = BgL_g1592z00_6224;
														BgL_zc3z04anonymousza32560ze3z87_6225:
															if (PAIRP(BgL_l1590z00_6226))
																{	/* SawMill/regalloc.scm 341 */
																	{	/* SawMill/regalloc.scm 334 */
																		obj_t BgL_r2z00_6227;

																		BgL_r2z00_6227 = CAR(BgL_l1590z00_6226);
																		{	/* SawMill/regalloc.scm 334 */
																			BgL_typez00_bglt BgL_t2z00_6228;

																			BgL_t2z00_6228 =
																				(((BgL_rtl_regz00_bglt) COBJECT(
																						((BgL_rtl_regz00_bglt)
																							BgL_r2z00_6227)))->BgL_typez00);
																			{	/* SawMill/regalloc.scm 335 */
																				bool_t BgL_test3173z00_9743;

																				if (
																					(((obj_t) BgL_t1z00_6208) ==
																						((obj_t) BgL_t2z00_6228)))
																					{	/* SawMill/regalloc.scm 335 */
																						BgL_test3173z00_9743 = ((bool_t) 1);
																					}
																				else
																					{	/* SawMill/regalloc.scm 336 */
																						bool_t BgL_test3175z00_9748;

																						if (
																							(((obj_t) BgL_t1z00_6208) ==
																								BGl_za2objza2z00zztype_cachez00))
																							{	/* SawMill/regalloc.scm 336 */
																								BgL_test3175z00_9748 =
																									BGl_bigloozd2typezf3z21zztype_typez00
																									(BgL_t2z00_6228);
																							}
																						else
																							{	/* SawMill/regalloc.scm 336 */
																								BgL_test3175z00_9748 =
																									((bool_t) 0);
																							}
																						if (BgL_test3175z00_9748)
																							{	/* SawMill/regalloc.scm 336 */
																								BgL_test3173z00_9743 =
																									((bool_t) 1);
																							}
																						else
																							{	/* SawMill/regalloc.scm 336 */
																								if (
																									(((obj_t) BgL_t2z00_6228) ==
																										BGl_za2objza2z00zztype_cachez00))
																									{	/* SawMill/regalloc.scm 338 */
																										BgL_test3173z00_9743 =
																											BGl_bigloozd2typezf3z21zztype_typez00
																											(BgL_t1z00_6208);
																									}
																								else
																									{	/* SawMill/regalloc.scm 338 */
																										BgL_test3173z00_9743 =
																											((bool_t) 0);
																									}
																							}
																					}
																				if (BgL_test3173z00_9743)
																					{	/* SawMill/regalloc.scm 335 */
																						((bool_t) 0);
																					}
																				else
																					{	/* SawMill/regalloc.scm 335 */
																						BGl_interferezd2regz12zc0zzsaw_registerzd2allocationzd2
																							(BgL_r1z00_6207, BgL_r2z00_6227);
																					}
																			}
																		}
																	}
																	{
																		obj_t BgL_l1590z00_9758;

																		BgL_l1590z00_9758 = CDR(BgL_l1590z00_6226);
																		BgL_l1590z00_6226 = BgL_l1590z00_9758;
																		goto BgL_zc3z04anonymousza32560ze3z87_6225;
																	}
																}
															else
																{	/* SawMill/regalloc.scm 341 */
																	((bool_t) 1);
																}
														}
													}
												else
													{	/* SawMill/regalloc.scm 332 */
														((bool_t) 0);
													}
											}
											{	/* SawMill/regalloc.scm 342 */
												obj_t BgL_arg2565z00_6229;

												BgL_arg2565z00_6229 = CDR(((obj_t) BgL_regsz00_6206));
												{
													obj_t BgL_regsz00_9762;

													BgL_regsz00_9762 = BgL_arg2565z00_6229;
													BgL_regsz00_6206 = BgL_regsz00_9762;
													goto BgL_loopz00_6205;
												}
											}
										}
									}
								}
							else
								{	/* SawMill/regalloc.scm 329 */
									BgL_tmpz00_9699 = ((bool_t) 0);
								}
						}
					}
				return BBOOL(BgL_tmpz00_9699);
			}
		}

	}



/* &protect-interference1709 */
	obj_t BGl_z62protectzd2interference1709zb0zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_6019, obj_t BgL_oz00_6020, obj_t BgL_regsz00_6021)
	{
		{	/* SawMill/regalloc.scm 222 */
			{	/* SawMill/regalloc.scm 224 */
				bool_t BgL_tmpz00_9765;

				{
					obj_t BgL_oz00_6237;
					obj_t BgL_oz00_6233;

					{
						obj_t BgL_l1555z00_6259;

						BgL_l1555z00_6259 = BgL_oz00_6020;
					BgL_zc3z04anonymousza32535ze3z87_6258:
						if (PAIRP(BgL_l1555z00_6259))
							{	/* SawMill/regalloc.scm 230 */
								BgL_oz00_6233 = CAR(BgL_l1555z00_6259);
								{	/* SawMill/regalloc.scm 229 */
									obj_t BgL_g1554z00_6234;

									BgL_g1554z00_6234 =
										(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt) BgL_oz00_6233)))->BgL_firstz00);
									{
										obj_t BgL_l1552z00_6236;

										BgL_l1552z00_6236 = BgL_g1554z00_6234;
									BgL_zc3z04anonymousza32550ze3z87_6235:
										if (PAIRP(BgL_l1552z00_6236))
											{	/* SawMill/regalloc.scm 229 */
												BgL_oz00_6237 = CAR(BgL_l1552z00_6236);
												{	/* SawMill/regalloc.scm 225 */
													bool_t BgL_test3180z00_9772;

													{	/* SawMill/regalloc.scm 225 */
														BgL_rtl_funz00_bglt BgL_arg2548z00_6238;

														BgL_arg2548z00_6238 =
															(((BgL_rtl_insz00_bglt) COBJECT(
																	((BgL_rtl_insz00_bglt) BgL_oz00_6237)))->
															BgL_funz00);
														{	/* SawMill/regalloc.scm 225 */
															obj_t BgL_classz00_6239;

															BgL_classz00_6239 =
																BGl_rtl_protectz00zzsaw_defsz00;
															{	/* SawMill/regalloc.scm 225 */
																BgL_objectz00_bglt BgL_arg1807z00_6240;

																{	/* SawMill/regalloc.scm 225 */
																	obj_t BgL_tmpz00_9775;

																	BgL_tmpz00_9775 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_arg2548z00_6238));
																	BgL_arg1807z00_6240 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_9775);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* SawMill/regalloc.scm 225 */
																		long BgL_idxz00_6241;

																		BgL_idxz00_6241 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_6240);
																		BgL_test3180z00_9772 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_6241 + 2L)) ==
																			BgL_classz00_6239);
																	}
																else
																	{	/* SawMill/regalloc.scm 225 */
																		bool_t BgL_res2752z00_6244;

																		{	/* SawMill/regalloc.scm 225 */
																			obj_t BgL_oclassz00_6245;

																			{	/* SawMill/regalloc.scm 225 */
																				obj_t BgL_arg1815z00_6246;
																				long BgL_arg1816z00_6247;

																				BgL_arg1815z00_6246 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* SawMill/regalloc.scm 225 */
																					long BgL_arg1817z00_6248;

																					BgL_arg1817z00_6248 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_6240);
																					BgL_arg1816z00_6247 =
																						(BgL_arg1817z00_6248 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_6245 =
																					VECTOR_REF(BgL_arg1815z00_6246,
																					BgL_arg1816z00_6247);
																			}
																			{	/* SawMill/regalloc.scm 225 */
																				bool_t BgL__ortest_1115z00_6249;

																				BgL__ortest_1115z00_6249 =
																					(BgL_classz00_6239 ==
																					BgL_oclassz00_6245);
																				if (BgL__ortest_1115z00_6249)
																					{	/* SawMill/regalloc.scm 225 */
																						BgL_res2752z00_6244 =
																							BgL__ortest_1115z00_6249;
																					}
																				else
																					{	/* SawMill/regalloc.scm 225 */
																						long BgL_odepthz00_6250;

																						{	/* SawMill/regalloc.scm 225 */
																							obj_t BgL_arg1804z00_6251;

																							BgL_arg1804z00_6251 =
																								(BgL_oclassz00_6245);
																							BgL_odepthz00_6250 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_6251);
																						}
																						if ((2L < BgL_odepthz00_6250))
																							{	/* SawMill/regalloc.scm 225 */
																								obj_t BgL_arg1802z00_6252;

																								{	/* SawMill/regalloc.scm 225 */
																									obj_t BgL_arg1803z00_6253;

																									BgL_arg1803z00_6253 =
																										(BgL_oclassz00_6245);
																									BgL_arg1802z00_6252 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_6253, 2L);
																								}
																								BgL_res2752z00_6244 =
																									(BgL_arg1802z00_6252 ==
																									BgL_classz00_6239);
																							}
																						else
																							{	/* SawMill/regalloc.scm 225 */
																								BgL_res2752z00_6244 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test3180z00_9772 = BgL_res2752z00_6244;
																	}
															}
														}
													}
													if (BgL_test3180z00_9772)
														{
															obj_t BgL_l1550z00_6255;

															BgL_l1550z00_6255 = ((obj_t) BgL_regsz00_6021);
														BgL_zc3z04anonymousza32542ze3z87_6254:
															if (PAIRP(BgL_l1550z00_6255))
																{	/* SawMill/regalloc.scm 227 */
																	{	/* SawMill/regalloc.scm 227 */
																		obj_t BgL_rz00_6256;

																		BgL_rz00_6256 = CAR(BgL_l1550z00_6255);
																		{	/* SawMill/regalloc.scm 227 */
																			obj_t BgL_arg2545z00_6257;

																			BgL_arg2545z00_6257 =
																				(((BgL_rtl_insz00_bglt) COBJECT(
																						((BgL_rtl_insz00_bglt)
																							BgL_oz00_6237)))->BgL_destz00);
																			BGl_interferezd2regz12zc0zzsaw_registerzd2allocationzd2
																				(BgL_arg2545z00_6257, BgL_rz00_6256);
																		}
																	}
																	{
																		obj_t BgL_l1550z00_9804;

																		BgL_l1550z00_9804 = CDR(BgL_l1550z00_6255);
																		BgL_l1550z00_6255 = BgL_l1550z00_9804;
																		goto BgL_zc3z04anonymousza32542ze3z87_6254;
																	}
																}
															else
																{	/* SawMill/regalloc.scm 227 */
																	((bool_t) 1);
																}
														}
													else
														{	/* SawMill/regalloc.scm 225 */
															((bool_t) 0);
														}
												}
												{
													obj_t BgL_l1552z00_9808;

													BgL_l1552z00_9808 = CDR(BgL_l1552z00_6236);
													BgL_l1552z00_6236 = BgL_l1552z00_9808;
													goto BgL_zc3z04anonymousza32550ze3z87_6235;
												}
											}
										else
											{	/* SawMill/regalloc.scm 229 */
												((bool_t) 1);
											}
									}
								}
								{
									obj_t BgL_l1555z00_9811;

									BgL_l1555z00_9811 = CDR(BgL_l1555z00_6259);
									BgL_l1555z00_6259 = BgL_l1555z00_9811;
									goto BgL_zc3z04anonymousza32535ze3z87_6258;
								}
							}
						else
							{	/* SawMill/regalloc.scm 230 */
								BgL_tmpz00_9765 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_9765);
			}
		}

	}



/* protect-interference! */
	obj_t BGl_protectzd2interferencez12zc0zzsaw_registerzd2allocationzd2(obj_t
		BgL_oz00_74, obj_t BgL_regsz00_75)
	{
		{	/* SawMill/regalloc.scm 222 */
			if (BGL_OBJECTP(BgL_oz00_74))
				{	/* SawMill/regalloc.scm 222 */
					obj_t BgL_method1710z00_3494;

					{	/* SawMill/regalloc.scm 222 */
						obj_t BgL_res2758z00_5546;

						{	/* SawMill/regalloc.scm 222 */
							long BgL_objzd2classzd2numz00_5517;

							BgL_objzd2classzd2numz00_5517 =
								BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_oz00_74));
							{	/* SawMill/regalloc.scm 222 */
								obj_t BgL_arg1811z00_5518;

								BgL_arg1811z00_5518 =
									PROCEDURE_REF
									(BGl_protectzd2interferencez12zd2envz12zzsaw_registerzd2allocationzd2,
									(int) (1L));
								{	/* SawMill/regalloc.scm 222 */
									int BgL_offsetz00_5521;

									BgL_offsetz00_5521 = (int) (BgL_objzd2classzd2numz00_5517);
									{	/* SawMill/regalloc.scm 222 */
										long BgL_offsetz00_5522;

										BgL_offsetz00_5522 =
											((long) (BgL_offsetz00_5521) - OBJECT_TYPE);
										{	/* SawMill/regalloc.scm 222 */
											long BgL_modz00_5523;

											BgL_modz00_5523 =
												(BgL_offsetz00_5522 >> (int) ((long) ((int) (4L))));
											{	/* SawMill/regalloc.scm 222 */
												long BgL_restz00_5525;

												BgL_restz00_5525 =
													(BgL_offsetz00_5522 &
													(long) (
														(int) (
															((long) (
																	(int) (
																		(1L <<
																			(int) ((long) ((int) (4L)))))) - 1L))));
												{	/* SawMill/regalloc.scm 222 */

													{	/* SawMill/regalloc.scm 222 */
														obj_t BgL_bucketz00_5527;

														BgL_bucketz00_5527 =
															VECTOR_REF(
															((obj_t) BgL_arg1811z00_5518), BgL_modz00_5523);
														BgL_res2758z00_5546 =
															VECTOR_REF(
															((obj_t) BgL_bucketz00_5527), BgL_restz00_5525);
						}}}}}}}}
						BgL_method1710z00_3494 = BgL_res2758z00_5546;
					}
					return
						BGL_PROCEDURE_CALL2(BgL_method1710z00_3494, BgL_oz00_74,
						BgL_regsz00_75);
				}
			else
				{	/* SawMill/regalloc.scm 222 */
					obj_t BgL_fun2588z00_3495;

					BgL_fun2588z00_3495 =
						PROCEDURE_REF
						(BGl_protectzd2interferencez12zd2envz12zzsaw_registerzd2allocationzd2,
						(int) (0L));
					return BGL_PROCEDURE_CALL2(BgL_fun2588z00_3495, BgL_oz00_74,
						BgL_regsz00_75);
				}
		}

	}



/* &protect-interference! */
	obj_t BGl_z62protectzd2interferencez12za2zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_6022, obj_t BgL_oz00_6023, obj_t BgL_regsz00_6024)
	{
		{	/* SawMill/regalloc.scm 222 */
			return
				BGl_protectzd2interferencez12zc0zzsaw_registerzd2allocationzd2
				(BgL_oz00_6023, BgL_regsz00_6024);
		}

	}



/* type-interference! */
	BGL_EXPORTED_DEF obj_t
		BGl_typezd2interferencez12zc0zzsaw_registerzd2allocationzd2
		(BgL_backendz00_bglt BgL_backz00_81, obj_t BgL_regsz00_82)
	{
		{	/* SawMill/regalloc.scm 326 */
			{	/* SawMill/regalloc.scm 326 */
				obj_t BgL_method1712z00_3496;

				{	/* SawMill/regalloc.scm 326 */
					obj_t BgL_res2764z00_5578;

					{	/* SawMill/regalloc.scm 326 */
						long BgL_objzd2classzd2numz00_5549;

						BgL_objzd2classzd2numz00_5549 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_backz00_81));
						{	/* SawMill/regalloc.scm 326 */
							obj_t BgL_arg1811z00_5550;

							BgL_arg1811z00_5550 =
								PROCEDURE_REF
								(BGl_typezd2interferencez12zd2envz12zzsaw_registerzd2allocationzd2,
								(int) (1L));
							{	/* SawMill/regalloc.scm 326 */
								int BgL_offsetz00_5553;

								BgL_offsetz00_5553 = (int) (BgL_objzd2classzd2numz00_5549);
								{	/* SawMill/regalloc.scm 326 */
									long BgL_offsetz00_5554;

									BgL_offsetz00_5554 =
										((long) (BgL_offsetz00_5553) - OBJECT_TYPE);
									{	/* SawMill/regalloc.scm 326 */
										long BgL_modz00_5555;

										BgL_modz00_5555 =
											(BgL_offsetz00_5554 >> (int) ((long) ((int) (4L))));
										{	/* SawMill/regalloc.scm 326 */
											long BgL_restz00_5557;

											BgL_restz00_5557 =
												(BgL_offsetz00_5554 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawMill/regalloc.scm 326 */

												{	/* SawMill/regalloc.scm 326 */
													obj_t BgL_bucketz00_5559;

													BgL_bucketz00_5559 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_5550), BgL_modz00_5555);
													BgL_res2764z00_5578 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_5559), BgL_restz00_5557);
					}}}}}}}}
					BgL_method1712z00_3496 = BgL_res2764z00_5578;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1712z00_3496,
					((obj_t) BgL_backz00_81), BgL_regsz00_82);
			}
		}

	}



/* &type-interference! */
	obj_t BGl_z62typezd2interferencez12za2zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_6025, obj_t BgL_backz00_6026, obj_t BgL_regsz00_6027)
	{
		{	/* SawMill/regalloc.scm 326 */
			return
				BGl_typezd2interferencez12zc0zzsaw_registerzd2allocationzd2(
				((BgL_backendz00_bglt) BgL_backz00_6026), BgL_regsz00_6027);
		}

	}



/* rtl-size */
	obj_t BGl_rtlzd2siza7ez75zzsaw_registerzd2allocationzd2(obj_t BgL_oz00_107)
	{
		{	/* SawMill/regalloc.scm 784 */
			if (BGL_OBJECTP(BgL_oz00_107))
				{	/* SawMill/regalloc.scm 784 */
					obj_t BgL_method1714z00_3498;

					{	/* SawMill/regalloc.scm 784 */
						obj_t BgL_res2769z00_5609;

						{	/* SawMill/regalloc.scm 784 */
							long BgL_objzd2classzd2numz00_5580;

							BgL_objzd2classzd2numz00_5580 =
								BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_oz00_107));
							{	/* SawMill/regalloc.scm 784 */
								obj_t BgL_arg1811z00_5581;

								BgL_arg1811z00_5581 =
									PROCEDURE_REF
									(BGl_rtlzd2siza7ezd2envza7zzsaw_registerzd2allocationzd2,
									(int) (1L));
								{	/* SawMill/regalloc.scm 784 */
									int BgL_offsetz00_5584;

									BgL_offsetz00_5584 = (int) (BgL_objzd2classzd2numz00_5580);
									{	/* SawMill/regalloc.scm 784 */
										long BgL_offsetz00_5585;

										BgL_offsetz00_5585 =
											((long) (BgL_offsetz00_5584) - OBJECT_TYPE);
										{	/* SawMill/regalloc.scm 784 */
											long BgL_modz00_5586;

											BgL_modz00_5586 =
												(BgL_offsetz00_5585 >> (int) ((long) ((int) (4L))));
											{	/* SawMill/regalloc.scm 784 */
												long BgL_restz00_5588;

												BgL_restz00_5588 =
													(BgL_offsetz00_5585 &
													(long) (
														(int) (
															((long) (
																	(int) (
																		(1L <<
																			(int) ((long) ((int) (4L)))))) - 1L))));
												{	/* SawMill/regalloc.scm 784 */

													{	/* SawMill/regalloc.scm 784 */
														obj_t BgL_bucketz00_5590;

														BgL_bucketz00_5590 =
															VECTOR_REF(
															((obj_t) BgL_arg1811z00_5581), BgL_modz00_5586);
														BgL_res2769z00_5609 =
															VECTOR_REF(
															((obj_t) BgL_bucketz00_5590), BgL_restz00_5588);
						}}}}}}}}
						BgL_method1714z00_3498 = BgL_res2769z00_5609;
					}
					return BGL_PROCEDURE_CALL1(BgL_method1714z00_3498, BgL_oz00_107);
				}
			else
				{	/* SawMill/regalloc.scm 784 */
					obj_t BgL_fun2590z00_3499;

					BgL_fun2590z00_3499 =
						PROCEDURE_REF
						(BGl_rtlzd2siza7ezd2envza7zzsaw_registerzd2allocationzd2,
						(int) (0L));
					return BGL_PROCEDURE_CALL1(BgL_fun2590z00_3499, BgL_oz00_107);
				}
		}

	}



/* &rtl-size */
	obj_t BGl_z62rtlzd2siza7ez17zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_6028, obj_t BgL_oz00_6029)
	{
		{	/* SawMill/regalloc.scm 784 */
			return BGl_rtlzd2siza7ez75zzsaw_registerzd2allocationzd2(BgL_oz00_6029);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_registerzd2allocationzd2(void)
	{
		{	/* SawMill/regalloc.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_rtlzd2siza7ezd2envza7zzsaw_registerzd2allocationzd2,
				BGl_blockz00zzsaw_defsz00,
				BGl_proc2837z00zzsaw_registerzd2allocationzd2,
				BGl_string2838z00zzsaw_registerzd2allocationzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_rtlzd2siza7ezd2envza7zzsaw_registerzd2allocationzd2,
				BGl_rtl_insz00zzsaw_defsz00,
				BGl_proc2839z00zzsaw_registerzd2allocationzd2,
				BGl_string2838z00zzsaw_registerzd2allocationzd2);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2envzd2zzsaw_defsz00,
				BGl_rtl_inszf2razf2zzsaw_registerzd2allocationzd2,
				BGl_proc2840z00zzsaw_registerzd2allocationzd2,
				BGl_string2841z00zzsaw_registerzd2allocationzd2);
		}

	}



/* &dump-rtl_ins/ra1720 */
	obj_t BGl_z62dumpzd2rtl_inszf2ra1720z42zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_6037, obj_t BgL_oz00_6038, obj_t BgL_pz00_6039,
		obj_t BgL_mz00_6040)
	{
		{	/* SawMill/regalloc.scm 801 */
			{

				{	/* SawMill/regalloc.scm 801 */
					obj_t BgL_nextzd2method1719zd2_6262;

					BgL_nextzd2method1719zd2_6262 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_rtl_insz00_bglt) BgL_oz00_6038)),
						BGl_dumpzd2envzd2zzsaw_defsz00,
						BGl_rtl_inszf2razf2zzsaw_registerzd2allocationzd2);
					BGL_PROCEDURE_CALL3(BgL_nextzd2method1719zd2_6262,
						((obj_t) ((BgL_rtl_insz00_bglt) BgL_oz00_6038)), BgL_pz00_6039,
						BgL_mz00_6040);
				}
				return BNIL;
			}
		}

	}



/* &rtl-size-rtl_ins1718 */
	obj_t BGl_z62rtlzd2siza7ezd2rtl_ins1718zc5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_6041, obj_t BgL_oz00_6042)
	{
		{	/* SawMill/regalloc.scm 794 */
			{	/* SawMill/regalloc.scm 795 */
				long BgL_tmpz00_9939;

				{	/* SawMill/regalloc.scm 796 */
					obj_t BgL_arg2592z00_6264;

					BgL_arg2592z00_6264 =
						BGl_rtlzd2siza7ez75zzsaw_registerzd2allocationzd2(
						(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_oz00_6042)))->BgL_argsz00));
					BgL_tmpz00_9939 = (1L + (long) CINT(BgL_arg2592z00_6264));
				}
				return BINT(BgL_tmpz00_9939);
			}
		}

	}



/* &rtl-size-block1716 */
	obj_t BGl_z62rtlzd2siza7ezd2block1716zc5zzsaw_registerzd2allocationzd2(obj_t
		BgL_envz00_6043, obj_t BgL_oz00_6044)
	{
		{	/* SawMill/regalloc.scm 790 */
			return
				BGl_rtlzd2siza7ez75zzsaw_registerzd2allocationzd2(
				(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_6044)))->BgL_firstz00));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_registerzd2allocationzd2(void)
	{
		{	/* SawMill/regalloc.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2842z00zzsaw_registerzd2allocationzd2));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2842z00zzsaw_registerzd2allocationzd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2842z00zzsaw_registerzd2allocationzd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2842z00zzsaw_registerzd2allocationzd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2842z00zzsaw_registerzd2allocationzd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2842z00zzsaw_registerzd2allocationzd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2842z00zzsaw_registerzd2allocationzd2));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2842z00zzsaw_registerzd2allocationzd2));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2842z00zzsaw_registerzd2allocationzd2));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string2842z00zzsaw_registerzd2allocationzd2));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string2842z00zzsaw_registerzd2allocationzd2));
			BGl_modulezd2initializa7ationz75zzsaw_node2rtlz00(20930040L,
				BSTRING_TO_STRING(BGl_string2842z00zzsaw_registerzd2allocationzd2));
			BGl_modulezd2initializa7ationz75zzsaw_regsetz00(358079690L,
				BSTRING_TO_STRING(BGl_string2842z00zzsaw_registerzd2allocationzd2));
			return
				BGl_modulezd2initializa7ationz75zzsaw_regutilsz00(237915200L,
				BSTRING_TO_STRING(BGl_string2842z00zzsaw_registerzd2allocationzd2));
		}

	}

#ifdef __cplusplus
}
#endif
