/*===========================================================================*/
/*   (SawMill/defs.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/defs.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_DEFS_TYPE_DEFINITIONS
#define BGL_SAW_DEFS_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_lastz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                  *BgL_rtl_lastz00_bglt;

	typedef struct BgL_rtl_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                    *BgL_rtl_returnz00_bglt;

	typedef struct BgL_rtl_jumpexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                      *BgL_rtl_jumpexitz00_bglt;

	typedef struct BgL_rtl_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                  *BgL_rtl_failz00_bglt;

	typedef struct BgL_rtl_retblockz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                      *BgL_rtl_retblockz00_bglt;

	typedef struct BgL_rtl_retz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_toz00;
	}                 *BgL_rtl_retz00_bglt;

	typedef struct BgL_rtl_notseqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                    *BgL_rtl_notseqz00_bglt;

	typedef struct BgL_rtl_ifz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                *BgL_rtl_ifz00_bglt;

	typedef struct BgL_rtl_selectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_patternsz00;
	}                    *BgL_rtl_selectz00_bglt;

	typedef struct BgL_rtl_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_patternsz00;
		obj_t BgL_labelsz00;
	}                    *BgL_rtl_switchz00_bglt;

	typedef struct BgL_rtl_ifeqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_thenz00;
	}                  *BgL_rtl_ifeqz00_bglt;

	typedef struct BgL_rtl_ifnez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_thenz00;
	}                  *BgL_rtl_ifnez00_bglt;

	typedef struct BgL_rtl_goz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_blockz00_bgl *BgL_toz00;
	}                *BgL_rtl_goz00_bglt;

	typedef struct BgL_rtl_purez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                  *BgL_rtl_purez00_bglt;

	typedef struct BgL_rtl_nopz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_nopz00_bglt;

	typedef struct BgL_rtl_movz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_movz00_bglt;

	typedef struct BgL_rtl_loadiz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_atomz00_bgl *BgL_constantz00;
	}                   *BgL_rtl_loadiz00_bglt;

	typedef struct BgL_rtl_loadgz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                   *BgL_rtl_loadgz00_bglt;

	typedef struct BgL_rtl_loadfunz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                     *BgL_rtl_loadfunz00_bglt;

	typedef struct BgL_rtl_globalrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                       *BgL_rtl_globalrefz00_bglt;

	typedef struct BgL_rtl_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_objtypez00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                      *BgL_rtl_getfieldz00_bglt;

	typedef struct BgL_rtl_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                    *BgL_rtl_vallocz00_bglt;

	typedef struct BgL_rtl_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                  *BgL_rtl_vrefz00_bglt;

	typedef struct BgL_rtl_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                     *BgL_rtl_vlengthz00_bglt;

	typedef struct BgL_rtl_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                        *BgL_rtl_instanceofz00_bglt;

	typedef struct BgL_rtl_makeboxz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                     *BgL_rtl_makeboxz00_bglt;

	typedef struct BgL_rtl_boxrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                    *BgL_rtl_boxrefz00_bglt;

	typedef struct BgL_rtl_effectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                    *BgL_rtl_effectz00_bglt;

	typedef struct BgL_rtl_storegz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                    *BgL_rtl_storegz00_bglt;

	typedef struct BgL_rtl_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_objtypez00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                      *BgL_rtl_setfieldz00_bglt;

	typedef struct BgL_rtl_vsetz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                  *BgL_rtl_vsetz00_bglt;

	typedef struct BgL_rtl_boxsetz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                    *BgL_rtl_boxsetz00_bglt;

	typedef struct BgL_rtl_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_constrz00;
	}                 *BgL_rtl_newz00_bglt;

	typedef struct BgL_rtl_callz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                  *BgL_rtl_callz00_bglt;

	typedef struct BgL_rtl_applyz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                   *BgL_rtl_applyz00_bglt;

	typedef struct BgL_rtl_lightfuncallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		obj_t BgL_funsz00;
		obj_t BgL_rettypez00;
	}                          *BgL_rtl_lightfuncallz00_bglt;

	typedef struct BgL_rtl_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                     *BgL_rtl_funcallz00_bglt;

	typedef struct BgL_rtl_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                    *BgL_rtl_pragmaz00_bglt;

	typedef struct BgL_rtl_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_totypez00;
		struct BgL_typez00_bgl *BgL_fromtypez00;
	}                  *BgL_rtl_castz00_bglt;

	typedef struct BgL_rtl_cast_nullz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                       *BgL_rtl_cast_nullz00_bglt;

	typedef struct BgL_rtl_protectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                     *BgL_rtl_protectz00_bglt;

	typedef struct BgL_rtl_protectedz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                       *BgL_rtl_protectedz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;


#endif													// BGL_SAW_DEFS_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL bool_t BGl_rtl_protectedzf3zf3zzsaw_defsz00(obj_t);
	static BgL_rtl_castz00_bglt BGl_z62lambda2657z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda2738z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_cast_nullzf3z91zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_lastz00_bglt BGl_z62rtl_lastzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_insz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_ifeqzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_ifeqz00_bglt, obj_t);
	static obj_t BGl_z62lambda2739z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_rtl_castz00_bglt BGl_z62lambda2659z62zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_effectzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_vsetzd2loczd2zzsaw_defsz00(BgL_rtl_vsetz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_funzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_funz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_vsetzd2typezd2zzsaw_defsz00(BgL_rtl_vsetz00_bglt);
	static obj_t BGl_z62rtl_regzd2debugnamezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_inszd2argsza2z12zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_retzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_loadgz00_bglt
		BGl_rtl_loadgzd2nilzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_effectzd2loczd2zzsaw_defsz00(BgL_rtl_effectz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_globalrefz00_bglt
		BGl_makezd2rtl_globalrefzd2zzsaw_defsz00(obj_t, BgL_globalz00_bglt);
	static obj_t BGl_z62rtl_setfieldzd2objtypezd2setz12z70zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzd2varzb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_jumpexitzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_jumpexitz00_bglt,
		obj_t);
	static obj_t BGl_z62dumpzd2inszd2rhsz62zzsaw_defsz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_castzd2totypezd2zzsaw_defsz00(BgL_rtl_castz00_bglt);
	static obj_t BGl_z62rtl_lastzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_newz00_bglt BGl_z62rtl_newzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_valloczd2vtypezd2setz12z12zzsaw_defsz00(BgL_rtl_vallocz00_bglt,
		BgL_typez00_bglt);
	static BgL_rtl_castz00_bglt BGl_z62rtl_castzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_vsetzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_movz00_bglt BGl_rtl_movzd2nilzd2zzsaw_defsz00(void);
	static BgL_typez00_bglt BGl_z62rtl_castzd2totypezb0zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_switchzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62dumpzd2funzd2rtl_go1816z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_rtl_applyz00_bglt BGl_z62lambda2580z62zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_funcallz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t BGl_rtl_boxrefzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_returnzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_returnz00_bglt,
		obj_t);
	static obj_t BGl_z62rtl_inszd2destzd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_callz00_bglt
		BGl_makezd2rtl_callzd2zzsaw_defsz00(obj_t, BgL_globalz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_vsetzd2vtypezd2zzsaw_defsz00(BgL_rtl_vsetz00_bglt);
	static obj_t BGl_z62lambda2745z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2746z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_funz00zzsaw_defsz00 = BUNSPEC;
	static BgL_rtl_applyz00_bglt BGl_z62lambda2585z62zzsaw_defsz00(obj_t);
	static BgL_typez00_bglt BGl_z62lambda2666z62zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_newz00zzsaw_defsz00 = BUNSPEC;
	static obj_t BGl_z62lambda2667z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_selectzd2loczd2zzsaw_defsz00(BgL_rtl_selectz00_bglt);
	static obj_t BGl_z62rtl_regzd2typezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_lightfuncallzd2rettypezd2setz12z12zzsaw_defsz00
		(BgL_rtl_lightfuncallz00_bglt, obj_t);
	static obj_t BGl_z62rtl_getfieldzd2namezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_getfieldzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_instanceofz00_bglt
		BGl_makezd2rtl_instanceofzd2zzsaw_defsz00(obj_t, BgL_typez00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32011ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_pragmaz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_selectzd2patternszd2setz12z12zzsaw_defsz00(BgL_rtl_selectz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_globalrefzd2loczd2zzsaw_defsz00(BgL_rtl_globalrefz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzd2hardwarezd2zzsaw_defsz00(BgL_rtl_regz00_bglt);
	static BgL_rtl_loadfunz00_bglt
		BGl_z62makezd2rtl_loadfunzb0zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62dumpzd2rtl_reg1793zb0zzsaw_defsz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_inszd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_gozd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_goz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_switchzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_switchz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_vrefzd2loczd2zzsaw_defsz00(BgL_rtl_vrefz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_globalrefzd2varzd2setz12z12zzsaw_defsz00(BgL_rtl_globalrefz00_bglt,
		BgL_globalz00_bglt);
	static BgL_rtl_protectedz00_bglt
		BGl_z62rtl_protectedzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_valloczf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_defsz00 = BUNSPEC;
	static obj_t BGl_z62rtl_gozd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_ifeqzf3zf3zzsaw_defsz00(obj_t);
	static BgL_rtl_funz00_bglt BGl_z62lambda2751z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_lastzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2752z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_vsetzd2vtypezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2673z62zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_funzf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2674z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_inszf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2756z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_lightfuncallz00_bglt BGl_z62lambda2595z62zzsaw_defsz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2757z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_lightfuncallzd2namezd2setz12z70zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_loadgzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_loadgz00_bglt, obj_t);
	static BgL_rtl_lightfuncallz00_bglt BGl_z62lambda2597z62zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_getfieldzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_getfieldz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzd2varzd2setz12z12zzsaw_defsz00(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_returnzd2typezd2zzsaw_defsz00(BgL_rtl_returnz00_bglt);
	static obj_t BGl_z62rtl_newzd2constrzd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_retzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_returnz00_bglt
		BGl_rtl_returnzd2nilzd2zzsaw_defsz00(void);
	extern obj_t BGl_za2accesszd2shapezf3za2z21zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_vrefzd2vtypezd2setz12z12zzsaw_defsz00(BgL_rtl_vrefz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_loadgzd2loczd2zzsaw_defsz00(BgL_rtl_loadgz00_bglt);
	static BgL_typez00_bglt BGl_z62rtl_returnzd2typezb0zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62dumpzd2funzd2rtl_loadfun1806z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_effectz00_bglt
		BGl_makezd2rtl_effectzd2zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_newzd2typezd2zzsaw_defsz00(BgL_rtl_newz00_bglt);
	static obj_t BGl_z62rtl_newzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_rtl_retzd2tozd2zzsaw_defsz00(BgL_rtl_retz00_bglt);
	static obj_t BGl_z62rtl_castzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_vrefzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_vrefz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_movzd2loczd2zzsaw_defsz00(BgL_rtl_movz00_bglt);
	static BgL_rtl_effectz00_bglt BGl_z62makezd2rtl_effectzb0zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62dumpzd2funzb0zzsaw_defsz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda1951z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32747ze3ze5zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_boxrefz00zzsaw_defsz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza32496ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda1953z62zzsaw_defsz00(obj_t);
	static BgL_rtl_cast_nullz00_bglt BGl_z62lambda2682z62zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2763z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_newzf3zf3zzsaw_defsz00(obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2765z62zzsaw_defsz00(obj_t);
	static BgL_rtl_cast_nullz00_bglt BGl_z62lambda2685z62zzsaw_defsz00(obj_t);
	static BgL_rtl_boxsetz00_bglt BGl_z62rtl_boxsetzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_lightfuncallzd2namezd2zzsaw_defsz00(BgL_rtl_lightfuncallz00_bglt);
	static BgL_typez00_bglt BGl_z62rtl_valloczd2vtypezb0zzsaw_defsz00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda1959z62zzsaw_defsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62rtl_callzd2varzd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_failz00_bglt BGl_z62rtl_failzd2nilzb0zzsaw_defsz00(obj_t);
	static BgL_rtl_getfieldz00_bglt
		BGl_z62makezd2rtl_getfieldzb0zzsaw_defsz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_rtl_cast_nullz00_bglt
		BGl_z62makezd2rtl_cast_nullzb0zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_getfieldzd2namezb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_setfieldzd2namezd2setz12z12zzsaw_defsz00(BgL_rtl_setfieldz00_bglt,
		obj_t);
	static BgL_typez00_bglt BGl_z62rtl_cast_nullzd2typezb0zzsaw_defsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_loadizd2constantzd2setz12z12zzsaw_defsz00(BgL_rtl_loadiz00_bglt,
		BgL_atomz00_bglt);
	static obj_t BGl_z62rtl_protectedzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_callzf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_vlengthzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_vlengthzd2vtypezd2zzsaw_defsz00(BgL_rtl_vlengthz00_bglt);
	static obj_t BGl_z62rtl_loadfunzd2varzd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_lastz00_bglt
		BGl_makezd2rtl_lastzd2zzsaw_defsz00(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_z62rtl_retblockzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1960z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_rtl_setfieldz00_bglt
		BGl_z62rtl_setfieldzd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_ifnezf3z91zzsaw_defsz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2691z62zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_castzd2fromtypezd2setz12z12zzsaw_defsz00(BgL_rtl_castz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62lambda2692z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1964z62zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_cast_nullzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_cast_nullz00_bglt,
		obj_t);
	static obj_t BGl_z62lambda1965z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2775z62zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_returnzd2loczd2zzsaw_defsz00(BgL_rtl_returnz00_bglt);
	static obj_t BGl_z62lambda2776z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62rtl_gozd2tozb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_switchzd2labelszd2setz12z12zzsaw_defsz00(BgL_rtl_switchz00_bglt,
		obj_t);
	static BgL_rtl_protectz00_bglt BGl_z62lambda2698z62zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_pragmazd2srfi0zb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_switchzd2labelszd2zzsaw_defsz00(BgL_rtl_switchz00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_rtl_vrefz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_globalrefzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_globalrefz00_bglt,
		obj_t);
	static BgL_typez00_bglt BGl_z62rtl_castzd2fromtypezb0zzsaw_defsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_setfieldzd2objtypezd2setz12z12zzsaw_defsz00
		(BgL_rtl_setfieldz00_bglt, BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_ifzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_ifz00_bglt, obj_t);
	static obj_t BGl_z62blockzd2firstzb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_funcallz00_bglt
		BGl_rtl_funcallzd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_z62zc3z04anonymousza32030ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_vrefzd2vtypezd2zzsaw_defsz00(BgL_rtl_vrefz00_bglt);
	static obj_t BGl_z62rtl_ifzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_funcallz00_bglt
		BGl_z62rtl_funcallzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_retz00_bglt BGl_rtl_retzd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_toplevelzd2initzd2zzsaw_defsz00(void);
	static BgL_rtl_boxrefz00_bglt BGl_z62rtl_boxrefzd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_vlengthzd2vtypezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static bool_t BGl_dumpzd2argszd2zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_rtl_loadfunzd2varzd2zzsaw_defsz00(BgL_rtl_loadfunz00_bglt);
	static BgL_blockz00_bglt BGl_z62rtl_retzd2tozb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_jumpexitzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_protectz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_rtl_switchz00_bglt
		BGl_rtl_switchzd2nilzd2zzsaw_defsz00(void);
	static BgL_globalz00_bglt BGl_z62rtl_loadfunzd2varzb0zzsaw_defsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_ifeqz00zzsaw_defsz00 = BUNSPEC;
	static obj_t BGl_z62zc3z04anonymousza31955ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_boxsetzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_valloczd2vtypezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_makeboxzf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda1970z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1971z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32587ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2784z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2785z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_purez00_bglt
		BGl_rtl_purezd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_z62rtl_failzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1977z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1978z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_switchzd2patternszd2zzsaw_defsz00(BgL_rtl_switchz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_ifnez00_bglt
		BGl_rtl_ifnezd2nilzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_newzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_newz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_rtl_boxsetz00zzsaw_defsz00 = BUNSPEC;
	static obj_t BGl_z62shapezd2rtl_reg1785zb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_globalz00_bglt BGl_z62rtl_callzd2varzb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_vlengthz00zzsaw_defsz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_getenvz00zz__osz00(obj_t);
	static obj_t BGl_z62rtl_storegzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_loadfunzd2varzd2setz12z12zzsaw_defsz00(BgL_rtl_loadfunz00_bglt,
		BgL_globalz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_instanceofz00_bglt
		BGl_rtl_instanceofzd2nilzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DEF obj_t BGl_rtl_purez00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_selectzd2patternszd2zzsaw_defsz00(BgL_rtl_selectz00_bglt);
	static obj_t BGl_z62dumpzd2funzd2rtl_fun1798z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_setfieldzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_failzf3z91zzsaw_defsz00(obj_t, obj_t);
	extern obj_t BGl_za2typezd2shapezf3za2z21zzengine_paramz00;
	BGL_EXPORTED_DECL bool_t BGl_rtl_cast_nullzf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31972ze3ze5zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_notseqzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_gozd2tozd2setz12z70zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_callzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_makeboxz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_blockzd2succszd2zzsaw_defsz00(BgL_blockz00_bglt);
	static obj_t BGl_z62lambda1984z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1985z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2795z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_defsz00(void);
	static obj_t BGl_z62lambda2796z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_protectedzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_protectedz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_funcallzd2loczd2zzsaw_defsz00(BgL_rtl_funcallz00_bglt);
	static obj_t BGl_z62rtl_nopzf3z91zzsaw_defsz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62rtl_getfieldzd2objtypezb0zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_loadfunzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_loadizd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_ifzf3z91zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_funzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzd2keyzd2zzsaw_defsz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_instanceofzd2loczd2setz12z70zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_insz00_bglt
		BGl_makezd2rtl_inszd2zzsaw_defsz00(obj_t, obj_t, obj_t, BgL_rtl_funz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_protectedz00_bglt
		BGl_makezd2rtl_protectedzd2zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_funcallzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_retzd2loczd2zzsaw_defsz00(BgL_rtl_retz00_bglt);
	static obj_t BGl_z62rtl_boxrefzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_switchzd2loczd2zzsaw_defsz00(BgL_rtl_switchz00_bglt);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_vsetz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_vsetzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_vsetz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_purezd2loczd2zzsaw_defsz00(BgL_rtl_purez00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_failz00_bglt
		BGl_makezd2rtl_failzd2zzsaw_defsz00(obj_t);
	static obj_t BGl_objectzd2initzd2zzsaw_defsz00(void);
	static obj_t BGl_z62zc3z04anonymousza32767ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62rtl_vrefzd2typezb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1991z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1992z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_boxrefzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_boxrefz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_ifnezd2loczd2zzsaw_defsz00(BgL_rtl_ifnez00_bglt);
	static BgL_rtl_notseqz00_bglt BGl_z62rtl_notseqzd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda1998z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_lightfuncallzd2namezb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1999z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_funz00_bglt
		BGl_makezd2rtl_funzd2zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_newz00_bglt
		BGl_makezd2rtl_newzd2zzsaw_defsz00(obj_t, BgL_typez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_instanceofzd2loczd2zzsaw_defsz00(BgL_rtl_instanceofz00_bglt);
	static obj_t BGl_z62dumpzd2marginzb0zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32130ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_notseqzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_pragmazd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_pragmaz00_bglt,
		obj_t);
	static obj_t BGl_z62rtl_ifnezd2thenzd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_setfieldzd2namezd2zzsaw_defsz00(BgL_rtl_setfieldz00_bglt);
	static BgL_typez00_bglt BGl_z62rtl_instanceofzd2typezb0zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_regzd2hardwarezb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_selectzf3zf3zzsaw_defsz00(obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL bool_t BGl_rtl_ifnezf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_rtl_globalrefzd2varzd2zzsaw_defsz00(BgL_rtl_globalrefz00_bglt);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_rtl_gozd2tozd2zzsaw_defsz00(BgL_rtl_goz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32687ze3ze5zzsaw_defsz00(obj_t, obj_t);
	extern obj_t BGl_za2keyzd2shapezf3za2z21zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzd2namezd2zzsaw_defsz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_pragmazd2formatzd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_getfieldzd2objtypezd2zzsaw_defsz00(BgL_rtl_getfieldz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_loadfunzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_loadfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_insz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_castzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_castz00_bglt, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	static obj_t BGl_z62rtl_effectzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_stringzd2containszd2zz__r4_strings_6_7z00(obj_t, obj_t,
		int);
	static obj_t BGl_z62dumpzd2funzd2rtl_loadg1804z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_regzd2varzd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_ifeqz00_bglt
		BGl_rtl_ifeqzd2nilzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DECL BgL_rtl_castz00_bglt
		BGl_makezd2rtl_castzd2zzsaw_defsz00(obj_t, BgL_typez00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_lightfuncallzd2rettypezd2zzsaw_defsz00
		(BgL_rtl_lightfuncallz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32212ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_instanceofzd2typezd2setz12z70zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_loadiz00_bglt
		BGl_rtl_loadizd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_z62zc3z04anonymousza32042ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32018ze3ze5zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_lightfuncallz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_rtl_loadgzd2varzd2zzsaw_defsz00(BgL_rtl_loadgz00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_rtl_regz00zzsaw_defsz00 = BUNSPEC;
	static obj_t BGl_z62rtl_boxsetzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_vallocz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_rtl_goz00_bglt BGl_makezd2rtl_gozd2zzsaw_defsz00(obj_t,
		BgL_blockz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_retblockz00_bglt
		BGl_makezd2rtl_retblockzd2zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_pragmaz00_bglt
		BGl_makezd2rtl_pragmazd2zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62dumpzd2rtl_ins1791zb0zzsaw_defsz00(obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62dumpzd2basiczd2blocksz62zzsaw_defsz00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_cast_nullz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_rtl_boxsetz00_bglt
		BGl_rtl_boxsetzd2nilzd2zzsaw_defsz00(void);
	static BgL_rtl_movz00_bglt BGl_z62rtl_movzd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32777ze3ze5zzsaw_defsz00(obj_t);
	static BgL_rtl_pragmaz00_bglt BGl_z62makezd2rtl_pragmazb0zzsaw_defsz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_loadfunz00zzsaw_defsz00 = BUNSPEC;
	static obj_t BGl_z62rtlzd2dumpzb0zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_storegzd2varzd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_boxrefzf3z91zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_notseqzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_retz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_dumpza2za2zzsaw_defsz00(obj_t, obj_t, int);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_newzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_newz00_bglt, obj_t);
	static obj_t BGl_z62rtl_applyzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_movz00zzsaw_defsz00 = BUNSPEC;
	static BgL_rtl_applyz00_bglt BGl_z62makezd2rtl_applyzb0zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_movzf3z91zzsaw_defsz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62rtl_getfieldzd2typezb0zzsaw_defsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_failzf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_vrefzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_vsetzd2vtypezd2setz12z12zzsaw_defsz00(BgL_rtl_vsetz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_jumpexitz00_bglt
		BGl_makezd2rtl_jumpexitzd2zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_lightfuncallzf3z91zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_vlengthzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_vrefz00_bglt BGl_z62makezd2rtl_vrefzb0zzsaw_defsz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_valloczf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzd2onexprzf3z21zzsaw_defsz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_rtl_loadfunzf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32786ze3ze5zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_getfieldzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_newzd2constrzd2setz12z12zzsaw_defsz00(BgL_rtl_newz00_bglt, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzd2onexprzf3z43zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_ifeqzd2loczd2zzsaw_defsz00(BgL_rtl_ifeqz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_boxrefz00_bglt
		BGl_makezd2rtl_boxrefzd2zzsaw_defsz00(obj_t);
	static BgL_rtl_lightfuncallz00_bglt
		BGl_z62rtl_lightfuncallzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_loadizd2loczd2zzsaw_defsz00(BgL_rtl_loadiz00_bglt);
	static obj_t BGl_z62rtl_setfieldzf3z91zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_ifeqz00_bglt BGl_z62makezd2rtl_ifeqzb0zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_boxrefz00_bglt
		BGl_rtl_boxrefzd2nilzd2zzsaw_defsz00(void);
	static BgL_rtl_boxrefz00_bglt BGl_z62makezd2rtl_boxrefzb0zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32060ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32222ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_ifz00_bglt BGl_makezd2rtl_ifzd2zzsaw_defsz00(obj_t);
	BGL_IMPORT obj_t BGl_writez00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_ifeqzd2thenzd2setz12z12zzsaw_defsz00(BgL_rtl_ifeqz00_bglt,
		BgL_blockz00_bglt);
	static obj_t BGl_z62dumpzd2funzd2rtl_mov1802z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32109ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszd2argszd2setz12z12zzsaw_defsz00(BgL_rtl_insz00_bglt, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_returnzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_boxsetzd2loczd2zzsaw_defsz00(BgL_rtl_boxsetz00_bglt);
	static obj_t BGl_z62rtl_movzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31993ze3ze5zzsaw_defsz00(obj_t);
	static obj_t BGl_appendzd221011zd2zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_storegz00_bglt BGl_z62rtl_storegzd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62dumpz62zzsaw_defsz00(obj_t, obj_t, obj_t, obj_t);
	static BgL_rtl_purez00_bglt BGl_z62makezd2rtl_purezb0zzsaw_defsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_nopz00zzsaw_defsz00 = BUNSPEC;
	static BgL_rtl_pragmaz00_bglt BGl_z62rtl_pragmazd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_defsz00(void);
	static obj_t BGl_z62rtl_cast_nullzd2typezd2setz12z70zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_retblockz00zzsaw_defsz00 = BUNSPEC;
	static obj_t BGl_z62rtl_vrefzf3z91zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_loadgz00_bglt BGl_z62makezd2rtl_loadgzb0zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32150ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_loadiz00_bglt BGl_z62makezd2rtl_loadizb0zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_gozf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_setfieldz00_bglt
		BGl_makezd2rtl_setfieldzd2zzsaw_defsz00(obj_t, obj_t, BgL_typez00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_vlengthzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_vlengthz00_bglt,
		obj_t);
	static obj_t BGl_z62rtl_storegzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_gozd2tozd2setz12z12zzsaw_defsz00(BgL_rtl_goz00_bglt,
		BgL_blockz00_bglt);
	static obj_t BGl_z62rtl_globalrefzd2varzd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_globalrefz00_bglt
		BGl_z62rtl_globalrefzd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_protectzf3z91zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzd2onexprzf3zd2setz12z83zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_typeof(obj_t);
	static obj_t BGl_z62rtl_failzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszd2destzd2zzsaw_defsz00(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_getfieldzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_getfieldz00_bglt,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31986ze3ze5zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_funcallzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_retzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_lightfuncallz00_bglt
		BGl_rtl_lightfuncallzd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_z62rtl_lightfuncallzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszd2argsza2z70zzsaw_defsz00(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62rtl_ifeqzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_boxrefzd2loczd2zzsaw_defsz00(BgL_rtl_boxrefz00_bglt);
	static BgL_rtl_vsetz00_bglt BGl_z62makezd2rtl_vsetzb0zzsaw_defsz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_getfieldzd2typezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_cast_nullz00_bglt
		BGl_rtl_cast_nullzd2nilzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_setfieldzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_setfieldz00_bglt,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_jumpexitz00zzsaw_defsz00 = BUNSPEC;
	static BgL_rtl_vsetz00_bglt BGl_z62rtl_vsetzd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62dumpzd2funzd2rtl_switch1810z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32070ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32232ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_dumpzd2funzd2zzsaw_defsz00(BgL_rtl_funz00_bglt,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32305ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32119ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_boxsetz00_bglt
		BGl_makezd2rtl_boxsetzd2zzsaw_defsz00(obj_t);
	static obj_t BGl_z62shapezd2rtl_ins1783zb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_retz00_bglt BGl_z62rtl_retzd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_storegzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_dumpz00zzsaw_defsz00(obj_t, obj_t, int);
	static BgL_rtl_boxsetz00_bglt BGl_z62makezd2rtl_boxsetzb0zzsaw_defsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL int BGl_blockzd2labelzd2zzsaw_defsz00(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_vlengthzd2typezd2zzsaw_defsz00(BgL_rtl_vlengthz00_bglt);
	static obj_t BGl_z62rtl_pragmazd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockzd2firstzd2setz12z12zzsaw_defsz00(BgL_blockz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_notseqz00_bglt
		BGl_rtl_notseqzd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_z62zc3z04anonymousza32797ze3ze5zzsaw_defsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31979ze3ze5zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_vsetzd2typezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_switchz00zzsaw_defsz00 = BUNSPEC;
	static obj_t BGl_z62dumpza2zc0zzsaw_defsz00(obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t make_string(long, unsigned char);
	BGL_EXPORTED_DEF obj_t BGl_rtl_goz00zzsaw_defsz00 = BUNSPEC;
	static obj_t BGl_z62rtl_retblockzf3z91zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_setfieldzd2namezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_globalrefzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_setfieldzd2typezd2zzsaw_defsz00(BgL_rtl_setfieldz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_notseqzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_notseqz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzd2onexprzf3zd2setz12ze1zzsaw_defsz00(BgL_rtl_regz00_bglt,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_ifnez00zzsaw_defsz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
	static BgL_blockz00_bglt BGl_z62rtl_ifnezd2thenzb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_showzd2funzd2zzsaw_defsz00(BgL_rtl_funz00_bglt, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_ifnezd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_ifnez00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_lightfuncallzd2loczd2zzsaw_defsz00(BgL_rtl_lightfuncallz00_bglt);
	static BgL_rtl_vrefz00_bglt BGl_z62rtl_vrefzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_instanceofz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_regzd2typezd2zzsaw_defsz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62blockzd2succszb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_castzd2fromtypezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_selectzd2typezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_cast_nullzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_setfieldz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_cast_nullzd2loczd2zzsaw_defsz00(BgL_rtl_cast_nullz00_bglt);
	static obj_t BGl_z62rtl_switchzd2labelszd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_nopzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_nopz00_bglt, obj_t);
	static obj_t BGl_z62rtl_vsetzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_inszd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_newzd2constrzb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_globalrefzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_setfieldzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_setfieldz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62rtl_regzd2keyzb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32331ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_jumpexitzf3z91zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32242ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_applyz00_bglt BGl_z62rtl_applyzd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_retzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_nopz00_bglt BGl_rtl_nopzd2nilzd2zzsaw_defsz00(void);
	static BgL_rtl_funz00_bglt BGl_z62lambda2007z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62dump1786z62zzsaw_defsz00(obj_t, obj_t, obj_t, obj_t);
	static BgL_rtl_funz00_bglt BGl_z62lambda2009z62zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_blockz00_bglt BGl_makezd2blockzd2zzsaw_defsz00(int,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_notseqzd2loczd2zzsaw_defsz00(BgL_rtl_notseqz00_bglt);
	static obj_t BGl_z62rtl_inszd2z52spillze2zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzd2zzsaw_defsz00(BgL_typez00_bglt, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_callz00_bglt
		BGl_rtl_callzd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_z62dumpzd2funzd2rtl_ifne1814z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_ifz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_rtl_applyz00_bglt
		BGl_makezd2rtl_applyzd2zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_vrefzf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_newzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_retzd2tozd2setz12z12zzsaw_defsz00(BgL_rtl_retz00_bglt,
		BgL_blockz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_retz00_bglt
		BGl_makezd2rtl_retzd2zzsaw_defsz00(obj_t, BgL_blockz00_bglt);
	BGL_IMPORT obj_t
		BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_instanceofzd2typezd2zzsaw_defsz00(BgL_rtl_instanceofz00_bglt);
	BGL_IMPORT obj_t bgl_close_output_port(obj_t);
	static obj_t BGl_z62rtl_retzd2tozd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32413ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_callz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_rtl_movz00_bglt
		BGl_makezd2rtl_movzd2zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2016z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_notseqzf3z91zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2017z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_selectz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_purezd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_purez00_bglt, obj_t);
	static BgL_typez00_bglt BGl_z62rtl_vlengthzd2vtypezb0zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_vrefzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_selectzf3z91zzsaw_defsz00(obj_t, obj_t);
	static BgL_atomz00_bglt BGl_z62rtl_loadizd2constantzb0zzsaw_defsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_instanceofzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_selectzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_selectz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_vallocz00_bglt
		BGl_makezd2rtl_valloczd2zzsaw_defsz00(obj_t, BgL_typez00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_rtl_nopzf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_getfieldzd2objtypezd2setz12z70zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt BGl_rtl_regzd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_z62rtl_instanceofzf3z91zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_getfieldzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62dumpzd2funzd2rtl_loadi1800z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_rtl_vallocz00_bglt BGl_z62makezd2rtl_valloczb0zzsaw_defsz00(obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_protectedzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_applyzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_switchzd2labelszb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_nopzd2loczd2zzsaw_defsz00(BgL_rtl_nopz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_blockzf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32600ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_boxsetzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_boxsetz00_bglt,
		obj_t);
	static obj_t BGl_z62rtl_setfieldzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32082ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_notseqz00_bglt BGl_z62lambda2105z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_lastz00_bglt BGl_z62lambda2025z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62rtl_newzd2typezb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_notseqz00_bglt BGl_z62lambda2107z62zzsaw_defsz00(obj_t);
	static BgL_rtl_lastz00_bglt BGl_z62lambda2027z62zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_pragmazd2srfi0zd2setz12z12zzsaw_defsz00(BgL_rtl_pragmaz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszd2argszd2zzsaw_defsz00(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_storegzd2varzd2setz12z12zzsaw_defsz00(BgL_rtl_storegz00_bglt,
		BgL_globalz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_storegz00_bglt
		BGl_rtl_storegzd2nilzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DECL bool_t BGl_rtl_applyzf3zf3zzsaw_defsz00(obj_t);
	static BgL_rtl_goz00_bglt BGl_z62makezd2rtl_gozb0zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_callzd2loczd2zzsaw_defsz00(BgL_rtl_callz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_pragmaz00_bglt
		BGl_rtl_pragmazd2nilzd2zzsaw_defsz00(void);
	static BgL_blockz00_bglt BGl_z62makezd2blockzb0zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_returnzd2typezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_valloczd2typezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_switchzd2patternszd2setz12z70zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_cast_nullz00_bglt
		BGl_makezd2rtl_cast_nullzd2zzsaw_defsz00(obj_t, BgL_typez00_bglt);
	static obj_t BGl_z62rtl_makeboxzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_loadgz00_bglt
		BGl_makezd2rtl_loadgzd2zzsaw_defsz00(obj_t, BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzd2debugnamezd2zzsaw_defsz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_rtl_storegz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_cast_nullzd2typezd2zzsaw_defsz00(BgL_rtl_cast_nullz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_loadiz00_bglt
		BGl_makezd2rtl_loadizd2zzsaw_defsz00(obj_t, BgL_atomz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_rtl_pragmazf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_makeboxz00_bglt
		BGl_rtl_makeboxzd2nilzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DECL BgL_rtl_nopz00_bglt
		BGl_makezd2rtl_nopzd2zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_newzd2typezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_makeboxz00_bglt
		BGl_z62rtl_makeboxzd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_loadgzd2varzd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_ifz00_bglt BGl_z62lambda2115z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_ifz00_bglt BGl_z62lambda2117z62zzsaw_defsz00(obj_t);
	static BgL_rtl_returnz00_bglt BGl_z62lambda2038z62zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_lastz00_bglt
		BGl_rtl_lastzd2nilzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DECL BgL_rtl_lightfuncallz00_bglt
		BGl_makezd2rtl_lightfuncallzd2zzsaw_defsz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_pragmazd2formatzd2zzsaw_defsz00(BgL_rtl_pragmaz00_bglt);
	static obj_t BGl_z62dumpzd2funzd2rtl_ifeq1812z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62blockzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_instanceofzd2typezd2setz12z12zzsaw_defsz00
		(BgL_rtl_instanceofz00_bglt, BgL_typez00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_rtl_lastz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL bool_t BGl_rtl_boxsetzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_dumpzd2inszd2rhsz00zzsaw_defsz00(BgL_rtl_insz00_bglt, obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2201z62zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_castz00_bglt
		BGl_rtl_castzd2nilzd2zzsaw_defsz00(void);
	static BgL_rtl_returnz00_bglt BGl_z62lambda2040z62zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2202z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32181ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32424ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_globalrefz00_bglt
		BGl_z62makezd2rtl_globalrefzb0zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_storegzd2loczd2zzsaw_defsz00(BgL_rtl_storegz00_bglt);
	static BgL_rtl_selectz00_bglt BGl_z62lambda2125z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_inszd2z52spillzd2setz12z22zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_ifz00_bglt BGl_z62makezd2rtl_ifzb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_returnz00zzsaw_defsz00 = BUNSPEC;
	static BgL_rtl_selectz00_bglt BGl_z62lambda2127z62zzsaw_defsz00(obj_t);
	static BgL_rtl_purez00_bglt BGl_z62lambda2208z62zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_vrefzd2typezd2zzsaw_defsz00(BgL_rtl_vrefz00_bglt);
	static BgL_typez00_bglt BGl_z62lambda2047z62zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_pragmazd2loczd2zzsaw_defsz00(BgL_rtl_pragmaz00_bglt);
	static obj_t BGl_z62lambda2048z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rtlzd2dumpzd2zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_returnzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_applyz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_rtl_retblockz00_bglt
		BGl_rtl_retblockzd2nilzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_returnzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_returnz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_valloczd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_vallocz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__osz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62rtl_applyzf3z91zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_makeboxzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_makeboxzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_makeboxz00_bglt,
		obj_t);
	static obj_t BGl_z62rtl_vsetzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62rtl_vlengthzd2typezb0zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_debugzd2sawzd2zzsaw_defsz00 = BUNSPEC;
	static obj_t BGl_z62blockzd2firstzd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_globalz00_bglt BGl_z62rtl_storegzd2varzb0zzsaw_defsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_funcallz00_bglt
		BGl_makezd2rtl_funcallzd2zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_movzf3zf3zzsaw_defsz00(obj_t);
	static BgL_rtl_purez00_bglt BGl_z62rtl_purezd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_makeboxzd2loczd2zzsaw_defsz00(BgL_rtl_makeboxz00_bglt);
	static BgL_rtl_ifnez00_bglt BGl_z62makezd2rtl_ifnezb0zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_makeboxzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_ifnez00_bglt BGl_z62rtl_ifnezd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_storegzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_storegz00_bglt,
		obj_t);
	static BgL_rtl_purez00_bglt BGl_z62lambda2210z62zzsaw_defsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32522ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_lastzd2loczd2zzsaw_defsz00(BgL_rtl_lastz00_bglt);
	static BgL_typez00_bglt BGl_z62lambda2134z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_loadizd2constantzd2setz12z70zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_funz00_bglt
		BGl_rtl_inszd2funzd2zzsaw_defsz00(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62lambda2135z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32166ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockzd2succszd2setz12z12zzsaw_defsz00(BgL_blockz00_bglt, obj_t);
	static BgL_rtl_jumpexitz00_bglt BGl_z62lambda2056z62zzsaw_defsz00(obj_t,
		obj_t);
	static BgL_rtl_nopz00_bglt BGl_z62lambda2218z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_setfieldzd2namezb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_jumpexitz00_bglt BGl_z62lambda2058z62zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2139z62zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_loadgzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_jumpexitz00_bglt
		BGl_rtl_jumpexitzd2nilzd2zzsaw_defsz00(void);
	static BgL_blockz00_bglt BGl_z62rtl_ifeqzd2thenzb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_getfieldzd2objtypezd2setz12z12zzsaw_defsz00
		(BgL_rtl_getfieldz00_bglt, BgL_typez00_bglt);
	static BgL_globalz00_bglt BGl_z62rtl_globalrefzd2varzb0zzsaw_defsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_loadizd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_loadiz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_newzd2constrzd2zzsaw_defsz00(BgL_rtl_newz00_bglt);
	static obj_t BGl_z62rtl_loadgzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_castzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_castzd2loczd2zzsaw_defsz00(BgL_rtl_castz00_bglt);
	static BgL_typez00_bglt BGl_z62rtl_vsetzd2typezb0zzsaw_defsz00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62rtl_nopzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_lastzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_lastz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_funz00_bglt BGl_rtl_funzd2nilzd2zzsaw_defsz00(void);
	static BgL_rtl_getfieldz00_bglt BGl_z62lambda2300z62zzsaw_defsz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_rtl_insz00_bglt BGl_z62makezd2rtl_inszb0zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62dumpzd2block1789zb0zzsaw_defsz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_rtl_nopz00_bglt BGl_z62lambda2220z62zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2140z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_rtl_getfieldz00_bglt BGl_z62lambda2302z62zzsaw_defsz00(obj_t);
	static BgL_rtl_loadgz00_bglt BGl_z62rtl_loadgzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_switchz00_bglt
		BGl_makezd2rtl_switchzd2zzsaw_defsz00(obj_t, BgL_typez00_bglt, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32272ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockzd2predszd2setz12z12zzsaw_defsz00(BgL_blockz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszd2z52spillz80zzsaw_defsz00(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32094ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszd2destzd2setz12z12zzsaw_defsz00(BgL_rtl_insz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_retblockzd2loczd2zzsaw_defsz00(BgL_rtl_retblockz00_bglt);
	static obj_t BGl_z62rtl_vrefzd2typezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_switchz00_bglt BGl_z62lambda2146z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static BgL_rtl_switchz00_bglt BGl_z62makezd2rtl_switchzb0zzsaw_defsz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf3z91zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_failz00_bglt BGl_z62lambda2066z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_movz00_bglt BGl_z62lambda2228z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2309z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_switchz00_bglt BGl_z62lambda2148z62zzsaw_defsz00(obj_t);
	static BgL_rtl_failz00_bglt BGl_z62lambda2068z62zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_lightfuncallzd2funszd2zzsaw_defsz00(BgL_rtl_lightfuncallz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_goz00_bglt BGl_rtl_gozd2nilzd2zzsaw_defsz00(void);
	static BgL_rtl_retblockz00_bglt
		BGl_z62makezd2rtl_retblockzb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_loadgz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_regz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_vlengthz00_bglt
		BGl_rtl_vlengthzd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_z62rtl_pragmazd2srfi0zd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_loadiz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_rtl_failz00_bglt
		BGl_rtl_failzd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_z62blockzd2labelzb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_callz00_bglt BGl_z62makezd2rtl_callzb0zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_purezd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_vlengthz00_bglt
		BGl_z62rtl_vlengthzd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_ifnezd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_lightfuncallzd2rettypezd2setz12z70zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_vlengthzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_ifnezd2thenzd2setz12z12zzsaw_defsz00(BgL_rtl_ifnez00_bglt,
		BgL_blockz00_bglt);
	static BgL_rtl_funz00_bglt BGl_z62makezd2rtl_funzb0zzsaw_defsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_notseqz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_rtl_protectedz00_bglt
		BGl_rtl_protectedzd2nilzd2zzsaw_defsz00(void);
	static BgL_rtl_newz00_bglt BGl_z62makezd2rtl_newzb0zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda2310z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_failz00zzsaw_defsz00 = BUNSPEC;
	static BgL_rtl_movz00_bglt BGl_z62lambda2230z62zzsaw_defsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32702ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_castzd2totypezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_switchzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_vrefz00_bglt
		BGl_makezd2rtl_vrefzd2zzsaw_defsz00(obj_t, BgL_typez00_bglt,
		BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62lambda2314z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32435ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2315z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_purezf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_blockzd2labelzd2setz12z12zzsaw_defsz00(BgL_blockz00_bglt, int);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_vlengthzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_vlengthz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_jumpexitzd2loczd2zzsaw_defsz00(BgL_rtl_jumpexitz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32257ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2155z62zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_getfieldzf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2156z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_rtl_loadiz00_bglt BGl_z62lambda2238z62zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda2319z62zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_blockzd2predszd2zzsaw_defsz00(BgL_blockz00_bglt);
	static BgL_rtl_retblockz00_bglt BGl_z62lambda2078z62zzsaw_defsz00(obj_t,
		obj_t);
	static BgL_rtl_jumpexitz00_bglt
		BGl_z62makezd2rtl_jumpexitzb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DECL bool_t BGl_rtl_loadizf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_blockz00_bglt BGl_blockzd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_defsz00(void);
	static obj_t BGl_z62rtl_selectzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_loadgzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzd2debugnamezd2setz12z12zzsaw_defsz00(BgL_rtl_regz00_bglt,
		obj_t);
	static BgL_rtl_lightfuncallz00_bglt
		BGl_z62makezd2rtl_lightfuncallzb0zzsaw_defsz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_ifzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_setfieldzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_instanceofzd2loczd2setz12z12zzsaw_defsz00
		(BgL_rtl_instanceofz00_bglt, obj_t);
	static obj_t BGl_z62rtl_castzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_ifeqz00_bglt
		BGl_makezd2rtl_ifeqzd2zzsaw_defsz00(obj_t, BgL_blockz00_bglt);
	static obj_t BGl_z62rtl_regzd2namezb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_funzd2loczd2zzsaw_defsz00(BgL_rtl_funz00_bglt);
	static BgL_rtl_vallocz00_bglt BGl_z62rtl_valloczd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_effectzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_loadgzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_defsz00(void);
	static BgL_rtl_ifeqz00_bglt BGl_z62rtl_ifeqzd2nilzb0zzsaw_defsz00(obj_t);
	static BgL_typez00_bglt BGl_z62lambda2400z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2320z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2401z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_rtl_loadiz00_bglt BGl_z62lambda2240z62zzsaw_defsz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_defsz00(void);
	static BgL_rtl_retblockz00_bglt BGl_z62lambda2080z62zzsaw_defsz00(obj_t);
	static BgL_rtl_ifeqz00_bglt BGl_z62lambda2162z62zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32355ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_rtl_gozd2loczd2zzsaw_defsz00(BgL_rtl_goz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_switchzd2typezd2zzsaw_defsz00(BgL_rtl_switchz00_bglt);
	static BgL_rtl_ifeqz00_bglt BGl_z62lambda2164z62zzsaw_defsz00(obj_t);
	static BgL_rtl_nopz00_bglt BGl_z62rtl_nopzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_vlengthzd2loczd2zzsaw_defsz00(BgL_rtl_vlengthz00_bglt);
	static BgL_atomz00_bglt BGl_z62lambda2246z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_vallocz00_bglt BGl_z62lambda2327z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_rtl_makeboxz00_bglt BGl_z62lambda2408z62zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_inszd2argszd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2247z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_rtl_vallocz00_bglt BGl_z62lambda2329z62zzsaw_defsz00(obj_t);
	static BgL_typez00_bglt BGl_z62rtl_switchzd2typezb0zzsaw_defsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_purez00_bglt
		BGl_makezd2rtl_purezd2zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_failzd2loczd2zzsaw_defsz00(BgL_rtl_failz00_bglt);
	static BgL_rtl_retz00_bglt BGl_z62lambda2089z62zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_vlengthzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_castz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_applyzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_applyz00_bglt, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_selectz00_bglt
		BGl_makezd2rtl_selectzd2zzsaw_defsz00(obj_t, BgL_typez00_bglt, obj_t);
	static BgL_rtl_goz00_bglt BGl_z62rtl_gozd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_protectz00_bglt
		BGl_makezd2rtl_protectzd2zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_callzd2varzd2setz12z12zzsaw_defsz00(BgL_rtl_callz00_bglt,
		BgL_globalz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_getfieldzd2namezd2setz12z12zzsaw_defsz00(BgL_rtl_getfieldz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_getfieldz00_bglt
		BGl_makezd2rtl_getfieldzd2zzsaw_defsz00(obj_t, obj_t, BgL_typez00_bglt,
		BgL_typez00_bglt);
	static BgL_rtl_selectz00_bglt BGl_z62makezd2rtl_selectzb0zzsaw_defsz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_switchzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_rtl_callzd2varzd2zzsaw_defsz00(BgL_rtl_callz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_getfieldzd2namezd2zzsaw_defsz00(BgL_rtl_getfieldz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_protectedzd2loczd2zzsaw_defsz00(BgL_rtl_protectedz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_castzd2totypezd2setz12z12zzsaw_defsz00(BgL_rtl_castz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_insz00_bglt BGl_rtl_inszd2nilzd2zzsaw_defsz00(void);
	static BgL_rtl_makeboxz00_bglt BGl_z62lambda2411z62zzsaw_defsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32712ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_selectzd2patternszd2setz12z70zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2170z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_instanceofz00_bglt
		BGl_z62rtl_instanceofzd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2171z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_rtl_setfieldz00_bglt
		BGl_z62makezd2rtl_setfieldzb0zzsaw_defsz00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static BgL_rtl_retz00_bglt BGl_z62lambda2091z62zzsaw_defsz00(obj_t);
	static BgL_rtl_loadgz00_bglt BGl_z62lambda2253z62zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_vlengthz00_bglt
		BGl_makezd2rtl_vlengthzd2zzsaw_defsz00(obj_t, BgL_typez00_bglt,
		BgL_typez00_bglt);
	static BgL_rtl_loadgz00_bglt BGl_z62lambda2255z62zzsaw_defsz00(obj_t);
	static BgL_typez00_bglt BGl_z62lambda2337z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_lastz00_bglt BGl_z62makezd2rtl_lastzb0zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2338z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_objectz00zz__objectz00;
	static BgL_rtl_ifnez00_bglt BGl_z62lambda2177z62zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_lightfuncallzd2loczd2setz12z70zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2098z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_ifnez00_bglt BGl_z62lambda2179z62zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2099z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_valloczd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_vsetz00_bglt
		BGl_makezd2rtl_vsetzd2zzsaw_defsz00(obj_t, BgL_typez00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62rtl_loadfunzf3z91zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_loadizf3z91zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_funzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_globalrefz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_castzd2fromtypezd2zzsaw_defsz00(BgL_rtl_castz00_bglt);
	static obj_t BGl_z62rtl_inszf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_protectz00_bglt
		BGl_rtl_protectzd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_z62rtl_valloczd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_makeboxz00_bglt
		BGl_makezd2rtl_makeboxzd2zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_globalrefzf3z91zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62rtl_regzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzd2varzd2zzsaw_defsz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_ifeqzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_protectz00_bglt
		BGl_z62rtl_protectzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_newz00_bglt BGl_rtl_newzd2nilzd2zzsaw_defsz00(void);
	static BgL_rtl_protectedz00_bglt
		BGl_z62makezd2rtl_protectedzb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_cast_nullz00_bglt
		BGl_z62rtl_cast_nullzd2nilzb0zzsaw_defsz00(obj_t);
	static BgL_rtl_boxrefz00_bglt BGl_z62lambda2420z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_nopzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2502z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_setfieldzd2typezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_storegz00_bglt
		BGl_makezd2rtl_storegzd2zzsaw_defsz00(obj_t, BgL_globalz00_bglt);
	static BgL_rtl_boxrefz00_bglt BGl_z62lambda2422z62zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2503z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_lightfuncallzd2funszd2setz12z12zzsaw_defsz00
		(BgL_rtl_lightfuncallz00_bglt, obj_t);
	static BgL_globalz00_bglt BGl_z62lambda2261z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2342z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2262z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2343z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_rtl_effectz00_bglt BGl_z62rtl_effectzd2nilzb0zzsaw_defsz00(obj_t);
	static BgL_rtl_storegz00_bglt BGl_z62makezd2rtl_storegzb0zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	static BgL_blockz00_bglt BGl_z62lambda2185z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2509z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2186z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_gozd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_loadfunz00_bglt BGl_z62lambda2268z62zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_rtl_storegzd2varzd2zzsaw_defsz00(BgL_rtl_storegz00_bglt);
	static obj_t BGl_z62rtl_returnzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_lightfuncallzd2funszb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_retblockz00_bglt
		BGl_z62rtl_retblockzd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62dumpzd2funzd2rtl_call1818z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_newzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_getfieldz00_bglt
		BGl_rtl_getfieldzd2nilzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszd2loczd2zzsaw_defsz00(BgL_rtl_insz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_rtl_retblockzf3zf3zzsaw_defsz00(obj_t);
	static BgL_rtl_funcallz00_bglt
		BGl_z62makezd2rtl_funcallzb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_selectz00_bglt BGl_z62rtl_selectzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_movzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_movz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszd2funzd2setz12z12zzsaw_defsz00(BgL_rtl_insz00_bglt,
		BgL_rtl_funz00_bglt);
	static obj_t BGl_z62rtl_instanceofzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_instanceofz00_bglt
		BGl_z62makezd2rtl_instanceofzb0zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_purezf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_pragmazf3z91zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2510z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_rtl_ifnezd2thenzd2zzsaw_defsz00(BgL_rtl_ifnez00_bglt);
	extern obj_t BGl_za2userzd2shapezf3za2z21zzengine_paramz00;
	static BgL_rtl_effectz00_bglt BGl_z62lambda2431z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_loadfunz00_bglt BGl_z62lambda2270z62zzsaw_defsz00(obj_t);
	static BgL_rtl_vrefz00_bglt BGl_z62lambda2351z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_callzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_callz00_bglt, obj_t);
	static obj_t BGl_z62dumpzd2funzd2rtl_pragma1820z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_rtl_effectz00_bglt BGl_z62lambda2433z62zzsaw_defsz00(obj_t);
	static BgL_rtl_vrefz00_bglt BGl_z62lambda2353z62zzsaw_defsz00(obj_t);
	static BgL_rtl_goz00_bglt BGl_z62lambda2192z62zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_boxsetz00_bglt BGl_z62lambda2516z62zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_getfieldz00zzsaw_defsz00 = BUNSPEC;
	static BgL_typez00_bglt BGl_z62rtl_setfieldzd2typezb0zzsaw_defsz00(obj_t,
		obj_t);
	static BgL_rtl_goz00_bglt BGl_z62lambda2194z62zzsaw_defsz00(obj_t);
	static BgL_globalz00_bglt BGl_z62lambda2276z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_boxsetz00_bglt BGl_z62lambda2519z62zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_vlengthzd2typezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda2277z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62blockzd2succszd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda2359z62zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_castzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_returnz00_bglt
		BGl_makezd2rtl_returnzd2zzsaw_defsz00(obj_t, BgL_typez00_bglt);
	static obj_t BGl_z62rtl_protectzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_jumpexitz00_bglt
		BGl_z62rtl_jumpexitzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_protectzd2loczd2zzsaw_defsz00(BgL_rtl_protectz00_bglt);
	static obj_t BGl_z62rtl_inszd2destzb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_ifnezd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_protectedzf3z91zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_funcallzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_retzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_retz00_bglt, obj_t);
	static BgL_rtl_returnz00_bglt BGl_z62makezd2rtl_returnzb0zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_protectzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_IMPORT obj_t c_substring(obj_t, long, long);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_newzd2loczd2zzsaw_defsz00(BgL_rtl_newz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_rtl_jumpexitzf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_cast_nullzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_applyz00_bglt
		BGl_rtl_applyzd2nilzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_vlengthzd2vtypezd2setz12z12zzsaw_defsz00(BgL_rtl_vlengthz00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62rtl_vrefzd2vtypezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_lastzf3z91zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_boxsetzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_inszd2z52spillzd2setz12z40zzsaw_defsz00(BgL_rtl_insz00_bglt, obj_t);
	static obj_t BGl_z62rtl_effectzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32731ze3ze5zzsaw_defsz00(obj_t);
	static obj_t BGl_z62blockzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_ifz00_bglt BGl_rtl_ifzd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_z62zc3z04anonymousza32561ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62rtl_setfieldzd2objtypezb0zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2360z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32464ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32537ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_storegz00_bglt BGl_z62lambda2444z62zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza32448ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2606z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32197ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2607z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62blockzd2predszd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_globalrefz00_bglt BGl_z62lambda2284z62zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2365z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_storegz00_bglt BGl_z62lambda2446z62zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_lightfuncallzd2funszd2setz12z70zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda2366z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_retblockzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_newz00_bglt BGl_z62lambda2529z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_rtl_globalrefz00_bglt BGl_z62lambda2287z62zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_switchzd2patternszd2setz12z12zzsaw_defsz00(BgL_rtl_switchz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_vsetzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_vsetz00_bglt,
		BgL_typez00_bglt);
	static BgL_typez00_bglt BGl_z62rtl_vsetzd2vtypezb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_failz00_bglt BGl_z62makezd2rtl_failzb0zzsaw_defsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_effectz00zzsaw_defsz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_cast_nullzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_cast_nullz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_getfieldzd2loczd2zzsaw_defsz00(BgL_rtl_getfieldz00_bglt);
	static obj_t BGl_z62rtl_selectzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_loadfunz00_bglt
		BGl_makezd2rtl_loadfunzd2zzsaw_defsz00(obj_t, BgL_globalz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_rtl_gozf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32740ze3ze5zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2611z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2612z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32724ze3ze5zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_lightfuncallzd2loczd2setz12z12zzsaw_defsz00
		(BgL_rtl_lightfuncallz00_bglt, obj_t);
	static BgL_globalz00_bglt BGl_z62lambda2452z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32627ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_vlengthz00_bglt BGl_z62lambda2372z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32376ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2453z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_rtl_newz00_bglt BGl_z62lambda2535z62zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2616z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_globalz00_bglt BGl_z62lambda2293z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_vlengthz00_bglt BGl_z62lambda2374z62zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2617z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_rtl_funz00_bglt BGl_z62rtl_inszd2funzb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_protectzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_protectz00_bglt,
		obj_t);
	static obj_t BGl_z62lambda2294z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62blockzd2labelzd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_purezd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62rtl_jumpexitzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_funcallzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_funcallz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_blockzd2firstzd2zzsaw_defsz00(BgL_blockz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_applyzd2loczd2zzsaw_defsz00(BgL_rtl_applyz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_vallocz00_bglt
		BGl_rtl_valloczd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_z62dumpzd2funzd2rtl_globalr1808z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62rtl_regzd2typezb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_returnz00_bglt BGl_z62rtl_returnzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_loadfunz00_bglt
		BGl_rtl_loadfunzd2nilzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_setfieldzd2objtypezd2zzsaw_defsz00(BgL_rtl_setfieldz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_rtl_ifzd2loczd2zzsaw_defsz00(BgL_rtl_ifz00_bglt);
	static BgL_rtl_castz00_bglt BGl_z62makezd2rtl_castzb0zzsaw_defsz00(obj_t,
		obj_t, obj_t, obj_t);
	static BgL_rtl_protectz00_bglt BGl_z62lambda2700z62zzsaw_defsz00(obj_t);
	static BgL_rtl_loadfunz00_bglt
		BGl_z62rtl_loadfunzd2nilzb0zzsaw_defsz00(obj_t);
	static BgL_rtl_loadiz00_bglt BGl_z62rtl_loadizd2nilzb0zzsaw_defsz00(obj_t);
	static BgL_rtl_funz00_bglt BGl_z62rtl_funzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_dumpzd2basiczd2blocksz00zzsaw_defsz00(obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT bool_t BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_ifeqzd2thenzd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_setfieldz00_bglt BGl_z62lambda2460z62zzsaw_defsz00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2541z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2380z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2542z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_rtl_funcallz00_bglt BGl_z62lambda2623z62zzsaw_defsz00(obj_t,
		obj_t);
	static BgL_globalz00_bglt BGl_z62rtl_loadgzd2varzb0zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2381z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_rtl_setfieldz00_bglt BGl_z62lambda2462z62zzsaw_defsz00(obj_t);
	static BgL_rtl_funcallz00_bglt BGl_z62lambda2625z62zzsaw_defsz00(obj_t);
	static BgL_rtl_ifz00_bglt BGl_z62rtl_ifzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_notseqz00_bglt
		BGl_makezd2rtl_notseqzd2zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_valloczd2vtypezd2zzsaw_defsz00(BgL_rtl_vallocz00_bglt);
	static BgL_rtl_protectedz00_bglt BGl_z62lambda2708z62zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_vsetzf3z91zzsaw_defsz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2385z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2547z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2386z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2548z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_rtl_notseqz00_bglt BGl_z62makezd2rtl_notseqzb0zzsaw_defsz00(obj_t,
		obj_t);
	static BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzb0zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_valloczd2typezd2zzsaw_defsz00(BgL_rtl_vallocz00_bglt);
	static BgL_rtl_protectz00_bglt
		BGl_z62makezd2rtl_protectzb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_callz00_bglt BGl_z62rtl_callzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_storegzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_getfieldzd2typezd2zzsaw_defsz00(BgL_rtl_getfieldz00_bglt);
	static BgL_typez00_bglt BGl_z62rtl_valloczd2typezb0zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62rtl_switchzd2patternszb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_switchzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_switchz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_vrefzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_vrefz00_bglt, obj_t);
	static obj_t BGl_z62rtl_regzd2debugnamezb0zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_retz00_bglt BGl_z62makezd2rtl_retzb0zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_lightfuncallzf3zf3zzsaw_defsz00(obj_t);
	static BgL_rtl_protectedz00_bglt BGl_z62lambda2710z62zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_protectzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL BgL_rtl_setfieldz00_bglt
		BGl_rtl_setfieldzd2nilzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DECL BgL_atomz00_bglt
		BGl_rtl_loadizd2constantzd2zzsaw_defsz00(BgL_rtl_loadiz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza32661ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2470z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_movz00_bglt BGl_z62makezd2rtl_movzb0zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda2471z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_rtl_pragmaz00_bglt BGl_z62lambda2633z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_selectzd2patternszb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_funcallzf3zf3zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_selectzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_selectz00_bglt,
		obj_t);
	static BgL_rtl_instanceofz00_bglt BGl_z62lambda2392z62zzsaw_defsz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32289ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_callz00_bglt BGl_z62lambda2555z62zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_pragmaz00_bglt BGl_z62lambda2636z62zzsaw_defsz00(obj_t);
	static BgL_rtl_instanceofz00_bglt BGl_z62lambda2394z62zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_pragmazd2formatzd2setz12z12zzsaw_defsz00(BgL_rtl_pragmaz00_bglt,
		obj_t);
	static BgL_typez00_bglt BGl_z62lambda2476z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_callz00_bglt BGl_z62lambda2557z62zzsaw_defsz00(obj_t);
	static BgL_rtl_vlengthz00_bglt
		BGl_z62makezd2rtl_vlengthzb0zzsaw_defsz00(obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_pragmazd2srfi0zd2zzsaw_defsz00(BgL_rtl_pragmaz00_bglt);
	static obj_t BGl_z62lambda2477z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_ifeqzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_protectedz00zzsaw_defsz00 = BUNSPEC;
	static obj_t BGl_z62blockzd2predszb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_valloczd2loczd2zzsaw_defsz00(BgL_rtl_vallocz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_rtl_globalrefzf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_returnzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_vsetz00_bglt
		BGl_rtl_vsetzd2nilzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_effectzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_effectz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_rtl_lastzf3zf3zzsaw_defsz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza32000ze3ze5zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_pragmazd2formatzb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_loadfunzd2loczd2zzsaw_defsz00(BgL_rtl_loadfunz00_bglt);
	static BgL_rtl_makeboxz00_bglt
		BGl_z62makezd2rtl_makeboxzb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_loadfunzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_loadizd2loczb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_funzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_blockz00zzsaw_defsz00 = BUNSPEC;
	static obj_t BGl_z62rtl_lightfuncallzd2rettypezb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_effectz00_bglt
		BGl_rtl_effectzd2nilzd2zzsaw_defsz00(void);
	BGL_EXPORTED_DECL bool_t BGl_rtl_regzf3zf3zzsaw_defsz00(obj_t);
	static BgL_typez00_bglt BGl_z62rtl_vrefzd2vtypezb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_ifzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static BgL_rtl_insz00_bglt BGl_z62lambda2720z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32824ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_insz00_bglt BGl_z62lambda2722z62zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2642z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32808ze3ze5zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2643z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32638ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static BgL_typez00_bglt BGl_z62lambda2482z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2806z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2483z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2807z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static BgL_globalz00_bglt BGl_z62lambda2565z62zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_switchz00_bglt BGl_z62rtl_switchzd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62lambda2566z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda2648z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2729z62zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_inszd2argszb0zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62lambda2649z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_callzd2loczb0zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_rtl_ifnez00_bglt
		BGl_makezd2rtl_ifnezd2zzsaw_defsz00(obj_t, BgL_blockz00_bglt);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_selectzd2typezd2zzsaw_defsz00(BgL_rtl_selectz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_dumpzd2marginzd2zzsaw_defsz00(obj_t, int);
	BGL_EXPORTED_DECL BgL_rtl_selectz00_bglt
		BGl_rtl_selectzd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_z62rtl_boxrefzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62rtl_selectzd2typezb0zzsaw_defsz00(obj_t,
		obj_t);
	static obj_t BGl_z62dumpzd2fun1794zb0zzsaw_defsz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_switchzf3z91zzsaw_defsz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_loadgzd2varzd2setz12z12zzsaw_defsz00(BgL_rtl_loadgz00_bglt,
		BgL_globalz00_bglt);
	static BgL_rtl_getfieldz00_bglt
		BGl_z62rtl_getfieldzd2nilzb0zzsaw_defsz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_setfieldzd2loczd2zzsaw_defsz00(BgL_rtl_setfieldz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_retblockzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_retblockz00_bglt,
		obj_t);
	static obj_t BGl_z62rtl_movzd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_nopz00_bglt BGl_z62makezd2rtl_nopzb0zzsaw_defsz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_globalrefz00_bglt
		BGl_rtl_globalrefzd2nilzd2zzsaw_defsz00(void);
	static obj_t BGl_z62rtl_inszd2funzd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_insz00_bglt BGl_z62rtl_inszd2nilzb0zzsaw_defsz00(obj_t);
	static obj_t BGl_z62rtl_callzf3z91zzsaw_defsz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_switchzd2typezd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_blockz00_bglt
		BGl_rtl_ifeqzd2thenzd2zzsaw_defsz00(BgL_rtl_ifeqz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_failzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_failz00_bglt, obj_t);
	static obj_t BGl_z62lambda2730z62zzsaw_defsz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_pragmazd2loczd2setz12z70zzsaw_defsz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_vrefz00_bglt
		BGl_rtl_vrefzd2nilzd2zzsaw_defsz00(void);
	extern obj_t BGl_za2tracezd2portza2zd2zztools_tracez00;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_lightfuncallzd2namezd2setz12z12zzsaw_defsz00
		(BgL_rtl_lightfuncallz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza32396ze3ze5zzsaw_defsz00(obj_t, obj_t);
	static BgL_rtl_vsetz00_bglt BGl_z62lambda2491z62zzsaw_defsz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_valloczd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_vallocz00_bglt,
		obj_t);
	static BgL_rtl_vsetz00_bglt BGl_z62lambda2493z62zzsaw_defsz00(obj_t);
	static obj_t __cnst[84];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzd2debugnamezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_regza7d2deb3395z00,
		BGl_z62rtl_regzd2debugnamezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_vrefzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_vrefza7d2lo3396z00,
		BGl_z62rtl_vrefzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_protectedzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_protected3397za7,
		BGl_z62rtl_protectedzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_selectzd2patternszd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_selectza7d23398z00,
		BGl_z62rtl_selectzd2patternszb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_protectzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_protectza7f3399z00, BGl_z62rtl_protectzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_setfieldzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_setfieldza73400z00,
		BGl_z62rtl_setfieldzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_loadizd2constantzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_loadiza7d2c3401z00,
		BGl_z62rtl_loadizd2constantzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_callzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_callza7d2lo3402z00,
		BGl_z62rtl_callzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_pragmazd2formatzd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_pragmaza7d23403z00,
		BGl_z62rtl_pragmazd2formatzd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762blockza7d2nilza7b3404za7, BGl_z62blockzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_returnzd2typezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_returnza7d23405z00,
		BGl_z62rtl_returnzd2typezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_instanceofzd2typezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_instanceo3406za7,
		BGl_z62rtl_instanceofzd2typezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_retzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_retza7d2loc3407z00, BGl_z62rtl_retzd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_castzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_castza7d2ni3408z00, BGl_z62rtl_castzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_pragmazd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_pragmaza7d23409z00,
		BGl_z62rtl_pragmazd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_callzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_callza7d2ni3410z00, BGl_z62rtl_callzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_dumpzd2marginzd2envz00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2margin3411z00, BGl_z62dumpzd2marginzb0zzsaw_defsz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_selectzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_selectza7d23412z00,
		BGl_z62rtl_selectzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3100z00zzsaw_defsz00,
		BgL_bgl_za762lambda1977za7623413z00, BGl_z62lambda1977z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3101z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3414za7,
		BGl_z62zc3z04anonymousza31986ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_callzd2varzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_callza7d2va3415z00, BGl_z62rtl_callzd2varzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_getfieldzd2namezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_getfieldza73416z00,
		BGl_z62rtl_getfieldzd2namezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3102z00zzsaw_defsz00,
		BgL_bgl_za762lambda1985za7623417z00, BGl_z62lambda1985z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3103z00zzsaw_defsz00,
		BgL_bgl_za762lambda1984za7623418z00, BGl_z62lambda1984z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3104z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3419za7,
		BGl_z62zc3z04anonymousza31993ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3105z00zzsaw_defsz00,
		BgL_bgl_za762lambda1992za7623420z00, BGl_z62lambda1992z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3106z00zzsaw_defsz00,
		BgL_bgl_za762lambda1991za7623421z00, BGl_z62lambda1991z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3107z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3422za7,
		BGl_z62zc3z04anonymousza32000ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3108z00zzsaw_defsz00,
		BgL_bgl_za762lambda1999za7623423z00, BGl_z62lambda1999z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3109z00zzsaw_defsz00,
		BgL_bgl_za762lambda1998za7623424z00, BGl_z62lambda1998z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_inszd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_insza7d2nil3425z00, BGl_z62rtl_inszd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_vlengthzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_vlengthza7d3426z00,
		BGl_z62rtl_vlengthzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_setfieldzd2namezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_setfieldza73427z00,
		BGl_z62rtl_setfieldzd2namezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_lastzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_la3428z00, BGl_z62makezd2rtl_lastzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_gozd2tozd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_goza7d2toza7b3429za7, BGl_z62rtl_gozd2tozb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_valloczd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_va3430z00,
		BGl_z62makezd2rtl_valloczb0zzsaw_defsz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_nopzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_nopza7f3za7913431za7, BGl_z62rtl_nopzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_lightfuncallzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_lightfunc3432za7,
		BGl_z62rtl_lightfuncallzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_getfieldzd2objtypezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_getfieldza73433z00,
		BGl_z62rtl_getfieldzd2objtypezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_loadfunzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_loadfunza7d3434z00,
		BGl_z62rtl_loadfunzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_applyzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_applyza7d2l3435z00, BGl_z62rtl_applyzd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_valloczd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vallocza7d23436z00,
		BGl_z62rtl_valloczd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3110z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3437za7,
		BGl_z62zc3z04anonymousza31955ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_makeboxzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_ma3438z00,
		BGl_z62makezd2rtl_makeboxzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3111z00zzsaw_defsz00,
		BgL_bgl_za762lambda1953za7623439z00, BGl_z62lambda1953z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3112z00zzsaw_defsz00,
		BgL_bgl_za762lambda1951za7623440z00, BGl_z62lambda1951z62zzsaw_defsz00, 0L,
		BUNSPEC, 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3113z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3441za7,
		BGl_z62zc3z04anonymousza32018ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_cast_nullzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_cast_null3442za7,
		BGl_z62rtl_cast_nullzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_setfieldzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_setfieldza73443z00,
		BGl_z62rtl_setfieldzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3114z00zzsaw_defsz00,
		BgL_bgl_za762lambda2017za7623444z00, BGl_z62lambda2017z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_castzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_ca3445z00, BGl_z62makezd2rtl_castzb0zzsaw_defsz00,
		0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3115z00zzsaw_defsz00,
		BgL_bgl_za762lambda2016za7623446z00, BGl_z62lambda2016z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3116z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3447za7,
		BGl_z62zc3z04anonymousza32011ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_effectzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_effectza7d23448z00,
		BGl_z62rtl_effectzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3117z00zzsaw_defsz00,
		BgL_bgl_za762lambda2009za7623449z00, BGl_z62lambda2009z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_loadfunzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_loadfunza7d3450z00,
		BGl_z62rtl_loadfunzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3118z00zzsaw_defsz00,
		BgL_bgl_za762lambda2007za7623451z00, BGl_z62lambda2007z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_newzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_newza7d2nil3452z00, BGl_z62rtl_newzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3119z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3453za7,
		BGl_z62zc3z04anonymousza32030ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_loadfunzd2varzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_loadfunza7d3454z00,
		BGl_z62rtl_loadfunzd2varzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_valloczd2vtypezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_vallocza7d23455z00,
		BGl_z62rtl_valloczd2vtypezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_inszd2argszd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_insza7d2arg3456z00, BGl_z62rtl_inszd2argszb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_storegzd2varzd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_storegza7d23457z00,
		BGl_z62rtl_storegzd2varzd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_storegzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_storegza7d23458z00,
		BGl_z62rtl_storegzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_dumpzd2envzd2zzsaw_defsz00,
		BgL_bgl_za762dumpza762za7za7saw_3459z00, BGl_z62dumpz62zzsaw_defsz00, 0L,
		BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_storegzd2varzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_storegza7d23460z00,
		BGl_z62rtl_storegzd2varzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3200z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3461za7,
		BGl_z62zc3z04anonymousza32289ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3201z00zzsaw_defsz00,
		BgL_bgl_za762lambda2287za7623462z00, BGl_z62lambda2287z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3120z00zzsaw_defsz00,
		BgL_bgl_za762lambda2027za7623463z00, BGl_z62lambda2027z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3202z00zzsaw_defsz00,
		BgL_bgl_za762lambda2284za7623464z00, BGl_z62lambda2284z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3121z00zzsaw_defsz00,
		BgL_bgl_za762lambda2025za7623465z00, BGl_z62lambda2025z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_cast_nullzd2typezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_cast_null3466za7,
		BGl_z62rtl_cast_nullzd2typezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_loadizd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_lo3467z00,
		BGl_z62makezd2rtl_loadizb0zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3203z00zzsaw_defsz00,
		BgL_bgl_za762lambda2310za7623468z00, BGl_z62lambda2310z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3122z00zzsaw_defsz00,
		BgL_bgl_za762lambda2048za7623469z00, BGl_z62lambda2048z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3204z00zzsaw_defsz00,
		BgL_bgl_za762lambda2309za7623470z00, BGl_z62lambda2309z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3123z00zzsaw_defsz00,
		BgL_bgl_za762lambda2047za7623471z00, BGl_z62lambda2047z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3205z00zzsaw_defsz00,
		BgL_bgl_za762lambda2315za7623472z00, BGl_z62lambda2315z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3124z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3473za7,
		BGl_z62zc3z04anonymousza32042ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3206z00zzsaw_defsz00,
		BgL_bgl_za762lambda2314za7623474z00, BGl_z62lambda2314z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3125z00zzsaw_defsz00,
		BgL_bgl_za762lambda2040za7623475z00, BGl_z62lambda2040z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_selectzd2patternszd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_selectza7d23476z00,
		BGl_z62rtl_selectzd2patternszd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3207z00zzsaw_defsz00,
		BgL_bgl_za762lambda2320za7623477z00, BGl_z62lambda2320z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3126z00zzsaw_defsz00,
		BgL_bgl_za762lambda2038za7623478z00, BGl_z62lambda2038z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3208z00zzsaw_defsz00,
		BgL_bgl_za762lambda2319za7623479z00, BGl_z62lambda2319z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3127z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3480za7,
		BGl_z62zc3z04anonymousza32060ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2funzd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_insza7d2fun3481z00,
		BGl_z62rtl_inszd2funzd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3209z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3482za7,
		BGl_z62zc3z04anonymousza32305ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3128z00zzsaw_defsz00,
		BgL_bgl_za762lambda2058za7623483z00, BGl_z62lambda2058z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3129z00zzsaw_defsz00,
		BgL_bgl_za762lambda2056za7623484z00, BGl_z62lambda2056z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_valloczf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_vallocza7f33485z00, BGl_z62rtl_valloczf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_getfieldzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_getfieldza73486z00, BGl_z62rtl_getfieldzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_lastzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_lastza7d2ni3487z00, BGl_z62rtl_lastzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzd2onexprzf3zd2envzf3zzsaw_defsz00,
		BgL_bgl_za762rtl_regza7d2one3488z00,
		BGl_z62rtl_regzd2onexprzf3z43zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_funzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_funza7f3za7913489za7, BGl_z62rtl_funzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_pragmazd2srfi0zd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_pragmaza7d23490z00,
		BGl_z62rtl_pragmazd2srfi0zb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_castzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_castza7f3za793491za7, BGl_z62rtl_castzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzd2typezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_regza7d2typ3492z00, BGl_z62rtl_regzd2typezb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_protectedzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_protected3493za7,
		BGl_z62rtl_protectedzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_returnzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_returnza7d23494z00,
		BGl_z62rtl_returnzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3210z00zzsaw_defsz00,
		BgL_bgl_za762lambda2302za7623495z00, BGl_z62lambda2302z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3211z00zzsaw_defsz00,
		BgL_bgl_za762lambda2300za7623496z00, BGl_z62lambda2300z62zzsaw_defsz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3130z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3497za7,
		BGl_z62zc3z04anonymousza32070ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_boxsetzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_boxsetza7f33498z00, BGl_z62rtl_boxsetzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3212z00zzsaw_defsz00,
		BgL_bgl_za762lambda2338za7623499z00, BGl_z62lambda2338z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3131z00zzsaw_defsz00,
		BgL_bgl_za762lambda2068za7623500z00, BGl_z62lambda2068z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3213z00zzsaw_defsz00,
		BgL_bgl_za762lambda2337za7623501z00, BGl_z62lambda2337z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3132z00zzsaw_defsz00,
		BgL_bgl_za762lambda2066za7623502z00, BGl_z62lambda2066z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_boxrefzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_boxrefza7d23503z00,
		BGl_z62rtl_boxrefzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3214z00zzsaw_defsz00,
		BgL_bgl_za762lambda2343za7623504z00, BGl_z62lambda2343z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3133z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3505za7,
		BGl_z62zc3z04anonymousza32082ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3215z00zzsaw_defsz00,
		BgL_bgl_za762lambda2342za7623506z00, BGl_z62lambda2342z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3134z00zzsaw_defsz00,
		BgL_bgl_za762lambda2080za7623507z00, BGl_z62lambda2080z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_effectzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_ef3508z00,
		BGl_z62makezd2rtl_effectzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3216z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3509za7,
		BGl_z62zc3z04anonymousza32331ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3135z00zzsaw_defsz00,
		BgL_bgl_za762lambda2078za7623510z00, BGl_z62lambda2078z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3217z00zzsaw_defsz00,
		BgL_bgl_za762lambda2329za7623511z00, BGl_z62lambda2329z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3136z00zzsaw_defsz00,
		BgL_bgl_za762lambda2099za7623512z00, BGl_z62lambda2099z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2z52spillzd2setz12zd2envz92zzsaw_defsz00,
		BgL_bgl_za762rtl_insza7d2za7523513za7,
		BGl_z62rtl_inszd2z52spillzd2setz12z22zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3218z00zzsaw_defsz00,
		BgL_bgl_za762lambda2327za7623514z00, BGl_z62lambda2327z62zzsaw_defsz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3137z00zzsaw_defsz00,
		BgL_bgl_za762lambda2098za7623515z00, BGl_z62lambda2098z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3219z00zzsaw_defsz00,
		BgL_bgl_za762lambda2360za7623516z00, BGl_z62lambda2360z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3138z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3517za7,
		BGl_z62zc3z04anonymousza32094ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3139z00zzsaw_defsz00,
		BgL_bgl_za762lambda2091za7623518z00, BGl_z62lambda2091z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_nopzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_nopza7d2nil3519z00, BGl_z62rtl_nopzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2argszd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_insza7d2arg3520z00,
		BGl_z62rtl_inszd2argszd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_vlengthzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vlengthza7d3521z00,
		BGl_z62rtl_vlengthzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtlzd2dumpzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtlza7d2dumpza7b03522za7, BGl_z62rtlzd2dumpzb0zzsaw_defsz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2blockzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2blockza73523za7, BGl_z62makezd2blockzb0zzsaw_defsz00,
		0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_notseqzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_notseqza7d23524z00,
		BGl_z62rtl_notseqzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_vsetzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_vsetza7d2lo3525z00,
		BGl_z62rtl_vsetzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_purezd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_pureza7d2lo3526z00, BGl_z62rtl_purezd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_failzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_fa3527z00, BGl_z62makezd2rtl_failzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_switchzd2patternszd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_switchza7d23528z00,
		BGl_z62rtl_switchzd2patternszd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_applyzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_applyza7d2l3529z00,
		BGl_z62rtl_applyzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_selectzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_se3530z00,
		BGl_z62makezd2rtl_selectzb0zzsaw_defsz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_makeboxzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_makeboxza7d3531z00,
		BGl_z62rtl_makeboxzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3300z00zzsaw_defsz00,
		BgL_bgl_za762lambda2649za7623532z00, BGl_z62lambda2649z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3301z00zzsaw_defsz00,
		BgL_bgl_za762lambda2648za7623533z00, BGl_z62lambda2648z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3220z00zzsaw_defsz00,
		BgL_bgl_za762lambda2359za7623534z00, BGl_z62lambda2359z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3302z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3535za7,
		BGl_z62zc3z04anonymousza32638ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3221z00zzsaw_defsz00,
		BgL_bgl_za762lambda2366za7623536z00, BGl_z62lambda2366z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3140z00zzsaw_defsz00,
		BgL_bgl_za762lambda2089za7623537z00, BGl_z62lambda2089z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_retzd2tozd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_retza7d2toza73538za7,
		BGl_z62rtl_retzd2tozd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3303z00zzsaw_defsz00,
		BgL_bgl_za762lambda2636za7623539z00, BGl_z62lambda2636z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3222z00zzsaw_defsz00,
		BgL_bgl_za762lambda2365za7623540z00, BGl_z62lambda2365z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3141z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3541za7,
		BGl_z62zc3z04anonymousza32109ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3304z00zzsaw_defsz00,
		BgL_bgl_za762lambda2633za7623542z00, BGl_z62lambda2633z62zzsaw_defsz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3223z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3543za7,
		BGl_z62zc3z04anonymousza32355ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3142z00zzsaw_defsz00,
		BgL_bgl_za762lambda2107za7623544z00, BGl_z62lambda2107z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3305z00zzsaw_defsz00,
		BgL_bgl_za762lambda2667za7623545z00, BGl_z62lambda2667z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3224z00zzsaw_defsz00,
		BgL_bgl_za762lambda2353za7623546z00, BGl_z62lambda2353z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3143z00zzsaw_defsz00,
		BgL_bgl_za762lambda2105za7623547z00, BGl_z62lambda2105z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_setfieldzd2namezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_setfieldza73548z00,
		BGl_z62rtl_setfieldzd2namezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3306z00zzsaw_defsz00,
		BgL_bgl_za762lambda2666za7623549z00, BGl_z62lambda2666z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3225z00zzsaw_defsz00,
		BgL_bgl_za762lambda2351za7623550z00, BGl_z62lambda2351z62zzsaw_defsz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3144z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3551za7,
		BGl_z62zc3z04anonymousza32119ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3307z00zzsaw_defsz00,
		BgL_bgl_za762lambda2674za7623552z00, BGl_z62lambda2674z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3226z00zzsaw_defsz00,
		BgL_bgl_za762lambda2381za7623553z00, BGl_z62lambda2381z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3145z00zzsaw_defsz00,
		BgL_bgl_za762lambda2117za7623554z00, BGl_z62lambda2117z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3308z00zzsaw_defsz00,
		BgL_bgl_za762lambda2673za7623555z00, BGl_z62lambda2673z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3227z00zzsaw_defsz00,
		BgL_bgl_za762lambda2380za7623556z00, BGl_z62lambda2380z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3146z00zzsaw_defsz00,
		BgL_bgl_za762lambda2115za7623557z00, BGl_z62lambda2115z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3309z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3558za7,
		BGl_z62zc3z04anonymousza32661ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3228z00zzsaw_defsz00,
		BgL_bgl_za762lambda2386za7623559z00, BGl_z62lambda2386z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3147z00zzsaw_defsz00,
		BgL_bgl_za762lambda2135za7623560z00, BGl_z62lambda2135z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3229z00zzsaw_defsz00,
		BgL_bgl_za762lambda2385za7623561z00, BGl_z62lambda2385z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3148z00zzsaw_defsz00,
		BgL_bgl_za762lambda2134za7623562z00, BGl_z62lambda2134z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_movzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_mo3563z00, BGl_z62makezd2rtl_movzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3149z00zzsaw_defsz00,
		BgL_bgl_za762lambda2140za7623564z00, BGl_z62lambda2140z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_selectzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_selectza7f33565z00, BGl_z62rtl_selectzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_ifeqzd2thenzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_ifeqza7d2th3566z00, BGl_z62rtl_ifeqzd2thenzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_failzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_failza7d2lo3567z00,
		BGl_z62rtl_failzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_lightfuncallzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_lightfunc3568za7,
		BGl_z62rtl_lightfuncallzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3079z00zzsaw_defsz00,
		BgL_bgl_string3079za700za7za7s3569za7, "BIGLOOTRACE", 11);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_vlengthzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_vl3570z00,
		BGl_z62makezd2rtl_vlengthzb0zzsaw_defsz00, 0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_castzd2fromtypezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_castza7d2fr3571z00,
		BGl_z62rtl_castzd2fromtypezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_protectedzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_protected3572za7, BGl_z62rtl_protectedzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_funcallzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_funcallza7d3573z00,
		BGl_z62rtl_funcallzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_inszd2argsza2zd2envza2zzsaw_defsz00,
		BgL_bgl_za762rtl_insza7d2arg3574z00,
		BGl_z62rtl_inszd2argsza2z12zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_insza7d2loc3575z00,
		BGl_z62rtl_inszd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_vsetzd2typezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vsetza7d2ty3576z00, BGl_z62rtl_vsetzd2typezb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3310z00zzsaw_defsz00,
		BgL_bgl_za762lambda2659za7623577z00, BGl_z62lambda2659z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3311z00zzsaw_defsz00,
		BgL_bgl_za762lambda2657za7623578z00, BGl_z62lambda2657z62zzsaw_defsz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3230z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3579za7,
		BGl_z62zc3z04anonymousza32376ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3312z00zzsaw_defsz00,
		BgL_bgl_za762lambda2692za7623580z00, BGl_z62lambda2692z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3231z00zzsaw_defsz00,
		BgL_bgl_za762lambda2374za7623581z00, BGl_z62lambda2374z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3150z00zzsaw_defsz00,
		BgL_bgl_za762lambda2139za7623582z00, BGl_z62lambda2139z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3313z00zzsaw_defsz00,
		BgL_bgl_za762lambda2691za7623583z00, BGl_z62lambda2691z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3232z00zzsaw_defsz00,
		BgL_bgl_za762lambda2372za7623584z00, BGl_z62lambda2372z62zzsaw_defsz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3151z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3585za7,
		BGl_z62zc3z04anonymousza32130ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_lightfuncallzd2rettypezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_lightfunc3586za7,
		BGl_z62rtl_lightfuncallzd2rettypezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3314z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3587za7,
		BGl_z62zc3z04anonymousza32687ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3233z00zzsaw_defsz00,
		BgL_bgl_za762lambda2401za7623588z00, BGl_z62lambda2401z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3152z00zzsaw_defsz00,
		BgL_bgl_za762lambda2127za7623589z00, BGl_z62lambda2127z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_vrefzd2vtypezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vrefza7d2vt3590z00,
		BGl_z62rtl_vrefzd2vtypezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3315z00zzsaw_defsz00,
		BgL_bgl_za762lambda2685za7623591z00, BGl_z62lambda2685z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3234z00zzsaw_defsz00,
		BgL_bgl_za762lambda2400za7623592z00, BGl_z62lambda2400z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3153z00zzsaw_defsz00,
		BgL_bgl_za762lambda2125za7623593z00, BGl_z62lambda2125z62zzsaw_defsz00, 0L,
		BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_globalrefzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_gl3594z00,
		BGl_z62makezd2rtl_globalrefzb0zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3316z00zzsaw_defsz00,
		BgL_bgl_za762lambda2682za7623595z00, BGl_z62lambda2682z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3235z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3596za7,
		BGl_z62zc3z04anonymousza32396ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3154z00zzsaw_defsz00,
		BgL_bgl_za762lambda2156za7623597z00, BGl_z62lambda2156z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_setfieldzd2objtypezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_setfieldza73598z00,
		BGl_z62rtl_setfieldzd2objtypezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_ifzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_ifza7d2locza73599za7, BGl_z62rtl_ifzd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3317z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3600za7,
		BGl_z62zc3z04anonymousza32702ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3236z00zzsaw_defsz00,
		BgL_bgl_za762lambda2394za7623601z00, BGl_z62lambda2394z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3155z00zzsaw_defsz00,
		BgL_bgl_za762lambda2155za7623602z00, BGl_z62lambda2155z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3080z00zzsaw_defsz00,
		BgL_bgl_string3080za700za7za7s3603za7, "bbv ", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3318z00zzsaw_defsz00,
		BgL_bgl_za762lambda2700za7623604z00, BGl_z62lambda2700z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3237z00zzsaw_defsz00,
		BgL_bgl_za762lambda2392za7623605z00, BGl_z62lambda2392z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3156z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3606za7,
		BGl_z62zc3z04anonymousza32150ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3081z00zzsaw_defsz00,
		BgL_bgl_string3081za700za7za7s3607za7, " bbv", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3319z00zzsaw_defsz00,
		BgL_bgl_za762lambda2698za7623608z00, BGl_z62lambda2698z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3238z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3609za7,
		BGl_z62zc3z04anonymousza32413ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3157z00zzsaw_defsz00,
		BgL_bgl_za762lambda2148za7623610z00, BGl_z62lambda2148z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3082z00zzsaw_defsz00,
		BgL_bgl_string3082za700za7za7s3611za7, " bbv ", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3239z00zzsaw_defsz00,
		BgL_bgl_za762lambda2411za7623612z00, BGl_z62lambda2411z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3158z00zzsaw_defsz00,
		BgL_bgl_za762lambda2146za7623613z00, BGl_z62lambda2146z62zzsaw_defsz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STRING(BGl_string3083z00zzsaw_defsz00,
		BgL_bgl_string3083za700za7za7s3614za7, "bbv", 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3159z00zzsaw_defsz00,
		BgL_bgl_za762lambda2171za7623615z00, BGl_z62lambda2171z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3084z00zzsaw_defsz00,
		BgL_bgl_string3084za700za7za7s3616za7, "+-- ", 4);
	      DEFINE_STRING(BGl_string3085z00zzsaw_defsz00,
		BgL_bgl_string3085za700za7za7s3617za7, " ", 1);
	      DEFINE_STRING(BGl_string3086z00zzsaw_defsz00,
		BgL_bgl_string3086za700za7za7s3618za7, "| args:", 7);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_vsetzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_vsetza7f3za793619za7, BGl_z62rtl_vsetzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3087z00zzsaw_defsz00,
		BgL_bgl_string3087za700za7za7s3620za7, "| Basic blocks: ", 16);
	      DEFINE_STRING(BGl_string3088z00zzsaw_defsz00,
		BgL_bgl_string3088za700za7za7s3621za7, "(", 1);
	      DEFINE_STRING(BGl_string3089z00zzsaw_defsz00,
		BgL_bgl_string3089za700za7za7s3622za7, ")", 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_applyzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_applyza7f3za73623za7, BGl_z62rtl_applyzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_failzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_failza7d2ni3624z00, BGl_z62rtl_failzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_dumpza2zd2envz70zzsaw_defsz00,
		BgL_bgl_za762dumpza7a2za7c0za7za7s3625za7, BGl_z62dumpza2zc0zzsaw_defsz00,
		0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_newzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_newza7d2loc3626z00,
		BGl_z62rtl_newzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_switchzd2patternszd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_switchza7d23627z00,
		BGl_z62rtl_switchzd2patternszb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_lightfuncallzd2rettypezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_lightfunc3628za7,
		BGl_z62rtl_lightfuncallzd2rettypezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3320z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3629za7,
		BGl_z62zc3z04anonymousza32712ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3321z00zzsaw_defsz00,
		BgL_bgl_za762lambda2710za7623630z00, BGl_z62lambda2710z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3240z00zzsaw_defsz00,
		BgL_bgl_za762lambda2408za7623631z00, BGl_z62lambda2408z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3322z00zzsaw_defsz00,
		BgL_bgl_za762lambda2708za7623632z00, BGl_z62lambda2708z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3241z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3633za7,
		BGl_z62zc3z04anonymousza32424ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3160z00zzsaw_defsz00,
		BgL_bgl_za762lambda2170za7623634z00, BGl_z62lambda2170z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3323z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3635za7,
		BGl_z62zc3z04anonymousza32731ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3242z00zzsaw_defsz00,
		BgL_bgl_za762lambda2422za7623636z00, BGl_z62lambda2422z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3161z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3637za7,
		BGl_z62zc3z04anonymousza32166ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3324z00zzsaw_defsz00,
		BgL_bgl_za762lambda2730za7623638z00, BGl_z62lambda2730z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3243z00zzsaw_defsz00,
		BgL_bgl_za762lambda2420za7623639z00, BGl_z62lambda2420z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3162z00zzsaw_defsz00,
		BgL_bgl_za762lambda2164za7623640z00, BGl_z62lambda2164z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_globalrefzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_globalref3641za7,
		BGl_z62rtl_globalrefzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzd2hardwarezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_regza7d2har3642z00,
		BGl_z62rtl_regzd2hardwarezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3325z00zzsaw_defsz00,
		BgL_bgl_za762lambda2729za7623643z00, BGl_z62lambda2729z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3244z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3644za7,
		BGl_z62zc3z04anonymousza32435ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3163z00zzsaw_defsz00,
		BgL_bgl_za762lambda2162za7623645z00, BGl_z62lambda2162z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3326z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3646za7,
		BGl_z62zc3z04anonymousza32740ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3245z00zzsaw_defsz00,
		BgL_bgl_za762lambda2433za7623647z00, BGl_z62lambda2433z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3164z00zzsaw_defsz00,
		BgL_bgl_za762lambda2186za7623648z00, BGl_z62lambda2186z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_purezf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_pureza7f3za793649za7, BGl_z62rtl_purezf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3327z00zzsaw_defsz00,
		BgL_bgl_za762lambda2739za7623650z00, BGl_z62lambda2739z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3246z00zzsaw_defsz00,
		BgL_bgl_za762lambda2431za7623651z00, BGl_z62lambda2431z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3165z00zzsaw_defsz00,
		BgL_bgl_za762lambda2185za7623652z00, BGl_z62lambda2185z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3090z00zzsaw_defsz00,
		BgL_bgl_string3090za700za7za7s3653za7, " #;(", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3328z00zzsaw_defsz00,
		BgL_bgl_za762lambda2738za7623654z00, BGl_z62lambda2738z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3247z00zzsaw_defsz00,
		BgL_bgl_za762lambda2453za7623655z00, BGl_z62lambda2453z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3166z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3656za7,
		BGl_z62zc3z04anonymousza32181ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3329z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3657za7,
		BGl_z62zc3z04anonymousza32747ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3248z00zzsaw_defsz00,
		BgL_bgl_za762lambda2452za7623658z00, BGl_z62lambda2452z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3167z00zzsaw_defsz00,
		BgL_bgl_za762lambda2179za7623659z00, BGl_z62lambda2179z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3249z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3660za7,
		BGl_z62zc3z04anonymousza32448ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3168z00zzsaw_defsz00,
		BgL_bgl_za762lambda2177za7623661z00, BGl_z62lambda2177z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3169z00zzsaw_defsz00,
		BgL_bgl_za762lambda2202za7623662z00, BGl_z62lambda2202z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockzd2succszd2envz00zzsaw_defsz00,
		BgL_bgl_za762blockza7d2succs3663z00, BGl_z62blockzd2succszb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_inszf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_insza7f3za7913664za7, BGl_z62rtl_inszf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_returnzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_re3665z00,
		BGl_z62makezd2rtl_returnzb0zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_funcallzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_funcallza7d3666z00,
		BGl_z62rtl_funcallzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_vsetzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vsetza7d2ni3667z00, BGl_z62rtl_vsetzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_effectzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_effectza7d23668z00,
		BGl_z62rtl_effectzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_funzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_funza7d2loc3669z00, BGl_z62rtl_funzd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3330z00zzsaw_defsz00,
		BgL_bgl_za762lambda2746za7623670z00, BGl_z62lambda2746z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3331z00zzsaw_defsz00,
		BgL_bgl_za762lambda2745za7623671z00, BGl_z62lambda2745z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3250z00zzsaw_defsz00,
		BgL_bgl_za762lambda2446za7623672z00, BGl_z62lambda2446z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3332z00zzsaw_defsz00,
		BgL_bgl_za762lambda2752za7623673z00, BGl_z62lambda2752z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3251z00zzsaw_defsz00,
		BgL_bgl_za762lambda2444za7623674z00, BGl_z62lambda2444z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3170z00zzsaw_defsz00,
		BgL_bgl_za762lambda2201za7623675z00, BGl_z62lambda2201z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3333z00zzsaw_defsz00,
		BgL_bgl_za762lambda2751za7623676z00, BGl_z62lambda2751z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3252z00zzsaw_defsz00,
		BgL_bgl_za762lambda2471za7623677z00, BGl_z62lambda2471z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3171z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3678za7,
		BGl_z62zc3z04anonymousza32197ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_newzd2typezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_newza7d2typ3679z00, BGl_z62rtl_newzd2typezb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3334z00zzsaw_defsz00,
		BgL_bgl_za762lambda2757za7623680z00, BGl_z62lambda2757z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3253z00zzsaw_defsz00,
		BgL_bgl_za762lambda2470za7623681z00, BGl_z62lambda2470z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3172z00zzsaw_defsz00,
		BgL_bgl_za762lambda2194za7623682z00, BGl_z62lambda2194z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3091z00zzsaw_defsz00,
		BgL_bgl_za762lambda1960za7623683z00, BGl_z62lambda1960z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3335z00zzsaw_defsz00,
		BgL_bgl_za762lambda2756za7623684z00, BGl_z62lambda2756z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3254z00zzsaw_defsz00,
		BgL_bgl_za762lambda2477za7623685z00, BGl_z62lambda2477z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3173z00zzsaw_defsz00,
		BgL_bgl_za762lambda2192za7623686z00, BGl_z62lambda2192z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3092z00zzsaw_defsz00,
		BgL_bgl_za762lambda1959za7623687z00, BGl_z62lambda1959z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3336z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3688za7,
		BGl_z62zc3z04anonymousza32724ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3255z00zzsaw_defsz00,
		BgL_bgl_za762lambda2476za7623689z00, BGl_z62lambda2476z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3174z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3690za7,
		BGl_z62zc3z04anonymousza32212ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3093z00zzsaw_defsz00,
		BgL_bgl_za762lambda1965za7623691z00, BGl_z62lambda1965z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3337z00zzsaw_defsz00,
		BgL_bgl_za762lambda2722za7623692z00, BGl_z62lambda2722z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3256z00zzsaw_defsz00,
		BgL_bgl_za762lambda2483za7623693z00, BGl_z62lambda2483z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3175z00zzsaw_defsz00,
		BgL_bgl_za762lambda2210za7623694z00, BGl_z62lambda2210z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3094z00zzsaw_defsz00,
		BgL_bgl_za762lambda1964za7623695z00, BGl_z62lambda1964z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3338z00zzsaw_defsz00,
		BgL_bgl_za762lambda2720za7623696z00, BGl_z62lambda2720z62zzsaw_defsz00, 0L,
		BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3257z00zzsaw_defsz00,
		BgL_bgl_za762lambda2482za7623697z00, BGl_z62lambda2482z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3176z00zzsaw_defsz00,
		BgL_bgl_za762lambda2208za7623698z00, BGl_z62lambda2208z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3095z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3699za7,
		BGl_z62zc3z04anonymousza31972ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3339z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3700za7,
		BGl_z62zc3z04anonymousza32777ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3258z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3701za7,
		BGl_z62zc3z04anonymousza32464ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3177z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3702za7,
		BGl_z62zc3z04anonymousza32222ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3096z00zzsaw_defsz00,
		BgL_bgl_za762lambda1971za7623703z00, BGl_z62lambda1971z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_returnzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_returnza7f33704z00, BGl_z62rtl_returnzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3259z00zzsaw_defsz00,
		BgL_bgl_za762lambda2462za7623705z00, BGl_z62lambda2462z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3178z00zzsaw_defsz00,
		BgL_bgl_za762lambda2220za7623706z00, BGl_z62lambda2220z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3097z00zzsaw_defsz00,
		BgL_bgl_za762lambda1970za7623707z00, BGl_z62lambda1970z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3179z00zzsaw_defsz00,
		BgL_bgl_za762lambda2218za7623708z00, BGl_z62lambda2218z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3098z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3709za7,
		BGl_z62zc3z04anonymousza31979ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3099z00zzsaw_defsz00,
		BgL_bgl_za762lambda1978za7623710z00, BGl_z62lambda1978z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_newzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_newza7f3za7913711za7, BGl_z62rtl_newzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_vsetzd2typezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_vsetza7d2ty3712z00,
		BGl_z62rtl_vsetzd2typezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_protectzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_pr3713z00,
		BGl_z62makezd2rtl_protectzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_callzd2varzd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_callza7d2va3714z00,
		BGl_z62rtl_callzd2varzd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_getfieldzd2namezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_getfieldza73715z00,
		BGl_z62rtl_getfieldzd2namezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_retzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_re3716z00, BGl_z62makezd2rtl_retzb0zzsaw_defsz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3340z00zzsaw_defsz00,
		BgL_bgl_za762lambda2776za7623717z00, BGl_z62lambda2776z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3341z00zzsaw_defsz00,
		BgL_bgl_za762lambda2775za7623718z00, BGl_z62lambda2775z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3260z00zzsaw_defsz00,
		BgL_bgl_za762lambda2460za7623719z00, BGl_z62lambda2460z62zzsaw_defsz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3342z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3720za7,
		BGl_z62zc3z04anonymousza32786ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3261z00zzsaw_defsz00,
		BgL_bgl_za762lambda2503za7623721z00, BGl_z62lambda2503z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3180z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3722za7,
		BGl_z62zc3z04anonymousza32232ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3343z00zzsaw_defsz00,
		BgL_bgl_za762lambda2785za7623723z00, BGl_z62lambda2785z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3262z00zzsaw_defsz00,
		BgL_bgl_za762lambda2502za7623724z00, BGl_z62lambda2502z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3181z00zzsaw_defsz00,
		BgL_bgl_za762lambda2230za7623725z00, BGl_z62lambda2230z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3344z00zzsaw_defsz00,
		BgL_bgl_za762lambda2784za7623726z00, BGl_z62lambda2784z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3263z00zzsaw_defsz00,
		BgL_bgl_za762lambda2510za7623727z00, BGl_z62lambda2510z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3182z00zzsaw_defsz00,
		BgL_bgl_za762lambda2228za7623728z00, BGl_z62lambda2228z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3345z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3729za7,
		BGl_z62zc3z04anonymousza32797ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3264z00zzsaw_defsz00,
		BgL_bgl_za762lambda2509za7623730z00, BGl_z62lambda2509z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3183z00zzsaw_defsz00,
		BgL_bgl_za762lambda2247za7623731z00, BGl_z62lambda2247z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3346z00zzsaw_defsz00,
		BgL_bgl_za762lambda2796za7623732z00, BGl_z62lambda2796z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3265z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3733za7,
		BGl_z62zc3z04anonymousza32496ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3184z00zzsaw_defsz00,
		BgL_bgl_za762lambda2246za7623734z00, BGl_z62lambda2246z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3347z00zzsaw_defsz00,
		BgL_bgl_za762lambda2795za7623735z00, BGl_z62lambda2795z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3266z00zzsaw_defsz00,
		BgL_bgl_za762lambda2493za7623736z00, BGl_z62lambda2493z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3185z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3737za7,
		BGl_z62zc3z04anonymousza32242ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3354z00zzsaw_defsz00,
		BgL_bgl_string3354za700za7za7s3738za7, "", 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3348z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3739za7,
		BGl_z62zc3z04anonymousza32808ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3267z00zzsaw_defsz00,
		BgL_bgl_za762lambda2491za7623740z00, BGl_z62lambda2491z62zzsaw_defsz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3186z00zzsaw_defsz00,
		BgL_bgl_za762lambda2240za7623741z00, BGl_z62lambda2240z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3349z00zzsaw_defsz00,
		BgL_bgl_za762lambda2807za7623742z00, BGl_z62lambda2807z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3268z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3743za7,
		BGl_z62zc3z04anonymousza32522ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3187z00zzsaw_defsz00,
		BgL_bgl_za762lambda2238za7623744z00, BGl_z62lambda2238z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	extern obj_t BGl_shapezd2envzd2zztools_shapez00;
	   
		 
		DEFINE_STRING(BGl_string3356z00zzsaw_defsz00,
		BgL_bgl_string3356za700za7za7s3745za7, "dump1786", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3269z00zzsaw_defsz00,
		BgL_bgl_za762lambda2519za7623746z00, BGl_z62lambda2519z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3188z00zzsaw_defsz00,
		BgL_bgl_za762lambda2262za7623747z00, BGl_z62lambda2262z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3189z00zzsaw_defsz00,
		BgL_bgl_za762lambda2261za7623748z00, BGl_z62lambda2261z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3358z00zzsaw_defsz00,
		BgL_bgl_string3358za700za7za7s3749za7, "dump-fun1794", 12);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_inszd2destzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_insza7d2des3750z00, BGl_z62rtl_inszd2destzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_ifeqzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_ifeqza7d2lo3751z00,
		BGl_z62rtl_ifeqzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_regza7d2nil3752z00, BGl_z62rtl_regzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_castzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_castza7d2lo3753z00,
		BGl_z62rtl_castzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzd2varzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_regza7d2var3754z00, BGl_z62rtl_regzd2varzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3350z00zzsaw_defsz00,
		BgL_bgl_za762lambda2806za7623755z00, BGl_z62lambda2806z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3351z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3756za7,
		BGl_z62zc3z04anonymousza32767ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3270z00zzsaw_defsz00,
		BgL_bgl_za762lambda2516za7623757z00, BGl_z62lambda2516z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3352z00zzsaw_defsz00,
		BgL_bgl_za762lambda2765za7623758z00, BGl_z62lambda2765z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3271z00zzsaw_defsz00,
		BgL_bgl_za762lambda2542za7623759z00, BGl_z62lambda2542z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3190z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3760za7,
		BGl_z62zc3z04anonymousza32257ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3353z00zzsaw_defsz00,
		BgL_bgl_za762lambda2763za7623761z00, BGl_z62lambda2763z62zzsaw_defsz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3272z00zzsaw_defsz00,
		BgL_bgl_za762lambda2541za7623762z00, BGl_z62lambda2541z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3191z00zzsaw_defsz00,
		BgL_bgl_za762lambda2255za7623763z00, BGl_z62lambda2255z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3360z00zzsaw_defsz00,
		BgL_bgl_string3360za700za7za7s3764za7, "shape", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3273z00zzsaw_defsz00,
		BgL_bgl_za762lambda2548za7623765z00, BGl_z62lambda2548z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3192z00zzsaw_defsz00,
		BgL_bgl_za762lambda2253za7623766z00, BGl_z62lambda2253z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3355z00zzsaw_defsz00,
		BgL_bgl_za762dump1786za762za7za73767z00, BGl_z62dump1786z62zzsaw_defsz00,
		0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3274z00zzsaw_defsz00,
		BgL_bgl_za762lambda2547za7623768z00, BGl_z62lambda2547z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3193z00zzsaw_defsz00,
		BgL_bgl_za762lambda2277za7623769z00, BGl_z62lambda2277z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_switchzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_sw3770z00,
		BGl_z62makezd2rtl_switchzb0zzsaw_defsz00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3275z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3771za7,
		BGl_z62zc3z04anonymousza32537ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3194z00zzsaw_defsz00,
		BgL_bgl_za762lambda2276za7623772z00, BGl_z62lambda2276z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3363z00zzsaw_defsz00,
		BgL_bgl_string3363za700za7za7s3773za7, "dump", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3357z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2fun1793774z00, BGl_z62dumpzd2fun1794zb0zzsaw_defsz00,
		0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3276z00zzsaw_defsz00,
		BgL_bgl_za762lambda2535za7623775z00, BGl_z62lambda2535z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3195z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3776za7,
		BGl_z62zc3z04anonymousza32272ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3277z00zzsaw_defsz00,
		BgL_bgl_za762lambda2529za7623777z00, BGl_z62lambda2529z62zzsaw_defsz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3196z00zzsaw_defsz00,
		BgL_bgl_za762lambda2270za7623778z00, BGl_z62lambda2270z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3359z00zzsaw_defsz00,
		BgL_bgl_za762shapeza7d2rtl_i3779z00,
		BGl_z62shapezd2rtl_ins1783zb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3278z00zzsaw_defsz00,
		BgL_bgl_za762lambda2566za7623780z00, BGl_z62lambda2566z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3197z00zzsaw_defsz00,
		BgL_bgl_za762lambda2268za7623781z00, BGl_z62lambda2268z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3279z00zzsaw_defsz00,
		BgL_bgl_za762lambda2565za7623782z00, BGl_z62lambda2565z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3198z00zzsaw_defsz00,
		BgL_bgl_za762lambda2294za7623783z00, BGl_z62lambda2294z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_movzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_movza7d2nil3784z00, BGl_z62rtl_movzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3367z00zzsaw_defsz00,
		BgL_bgl_string3367za700za7za7s3785za7, "dump-fun", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3199z00zzsaw_defsz00,
		BgL_bgl_za762lambda2293za7623786z00, BGl_z62lambda2293z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockzd2labelzd2envz00zzsaw_defsz00,
		BgL_bgl_za762blockza7d2label3787z00, BGl_z62blockzd2labelzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_vlengthzd2typezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vlengthza7d3788z00,
		BGl_z62rtl_vlengthzd2typezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_storegzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_storegza7f33789z00, BGl_z62rtl_storegzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_getfieldzd2typezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_getfieldza73790z00,
		BGl_z62rtl_getfieldzd2typezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_getfieldzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_getfieldza73791z00,
		BGl_z62rtl_getfieldzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_loadfunzd2varzd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_loadfunza7d3792z00,
		BGl_z62rtl_loadfunzd2varzd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3361z00zzsaw_defsz00,
		BgL_bgl_za762shapeza7d2rtl_r3793z00,
		BGl_z62shapezd2rtl_reg1785zb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3280z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3794za7,
		BGl_z62zc3z04anonymousza32561ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3362z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2block13795z00,
		BGl_z62dumpzd2block1789zb0zzsaw_defsz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3281z00zzsaw_defsz00,
		BgL_bgl_za762lambda2557za7623796z00, BGl_z62lambda2557z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3282z00zzsaw_defsz00,
		BgL_bgl_za762lambda2555za7623797z00, BGl_z62lambda2555z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3364z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2rtl_in3798z00,
		BGl_z62dumpzd2rtl_ins1791zb0zzsaw_defsz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3283z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3799za7,
		BGl_z62zc3z04anonymousza32587ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3365z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2rtl_re3800z00,
		BGl_z62dumpzd2rtl_reg1793zb0zzsaw_defsz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3284z00zzsaw_defsz00,
		BgL_bgl_za762lambda2585za7623801z00, BGl_z62lambda2585z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3366z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2funza7d23802za7,
		BGl_z62dumpzd2funzd2rtl_fun1798z62zzsaw_defsz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3285z00zzsaw_defsz00,
		BgL_bgl_za762lambda2580za7623803z00, BGl_z62lambda2580z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_setfieldzd2typezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_setfieldza73804z00,
		BGl_z62rtl_setfieldzd2typezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_notseqzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_notseqza7d23805z00,
		BGl_z62rtl_notseqzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3286z00zzsaw_defsz00,
		BgL_bgl_za762lambda2607za7623806z00, BGl_z62lambda2607z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3368z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2funza7d23807za7,
		BGl_z62dumpzd2funzd2rtl_loadi1800z62zzsaw_defsz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3287z00zzsaw_defsz00,
		BgL_bgl_za762lambda2606za7623808z00, BGl_z62lambda2606z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3369z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2funza7d23809za7,
		BGl_z62dumpzd2funzd2rtl_mov1802z62zzsaw_defsz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3288z00zzsaw_defsz00,
		BgL_bgl_za762lambda2612za7623810z00, BGl_z62lambda2612z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3289z00zzsaw_defsz00,
		BgL_bgl_za762lambda2611za7623811z00, BGl_z62lambda2611z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockzd2predszd2envz00zzsaw_defsz00,
		BgL_bgl_za762blockza7d2preds3812z00, BGl_z62blockzd2predszb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3379z00zzsaw_defsz00,
		BgL_bgl_string3379za700za7za7s3813za7, " (go ", 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_ifnezd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_ifneza7d2lo3814z00,
		BGl_z62rtl_ifnezd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_loadfunzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_loadfunza7f3815z00, BGl_z62rtl_loadfunzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_protectzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_protectza7d3816z00,
		BGl_z62rtl_protectzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzd2debugnamezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_regza7d2deb3817z00,
		BGl_z62rtl_regzd2debugnamezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_pragmazd2formatzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_pragmaza7d23818z00,
		BGl_z62rtl_pragmazd2formatzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_ifeqzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_ifeqza7d2lo3819z00, BGl_z62rtl_ifeqzd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_vrefzd2vtypezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_vrefza7d2vt3820z00,
		BGl_z62rtl_vrefzd2vtypezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_loadgzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_loadgza7d2l3821z00, BGl_z62rtl_loadgzd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_lastzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_lastza7f3za793822za7, BGl_z62rtl_lastzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_dumpzd2inszd2rhszd2envzd2zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2insza7d23823za7,
		BGl_z62dumpzd2inszd2rhsz62zzsaw_defsz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3370z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2funza7d23824za7,
		BGl_z62dumpzd2funzd2rtl_loadg1804z62zzsaw_defsz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3371z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2funza7d23825za7,
		BGl_z62dumpzd2funzd2rtl_loadfun1806z62zzsaw_defsz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3290z00zzsaw_defsz00,
		BgL_bgl_za762lambda2617za7623826z00, BGl_z62lambda2617z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3372z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2funza7d23827za7,
		BGl_z62dumpzd2funzd2rtl_globalr1808z62zzsaw_defsz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3291z00zzsaw_defsz00,
		BgL_bgl_za762lambda2616za7623828z00, BGl_z62lambda2616z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3373z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2funza7d23829za7,
		BGl_z62dumpzd2funzd2rtl_switch1810z62zzsaw_defsz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3292z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3830za7,
		BGl_z62zc3z04anonymousza32600ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3380z00zzsaw_defsz00,
		BgL_bgl_string3380za700za7za7s3831za7, "::", 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3374z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2funza7d23832za7,
		BGl_z62dumpzd2funzd2rtl_ifeq1812z62zzsaw_defsz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3293z00zzsaw_defsz00,
		BgL_bgl_za762lambda2597za7623833z00, BGl_z62lambda2597z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3381z00zzsaw_defsz00,
		BgL_bgl_string3381za700za7za7s3834za7, "[", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3375z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2funza7d23835za7,
		BGl_z62dumpzd2funzd2rtl_ifne1814z62zzsaw_defsz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3294z00zzsaw_defsz00,
		BgL_bgl_za762lambda2595za7623836z00, BGl_z62lambda2595z62zzsaw_defsz00, 0L,
		BUNSPEC, 4);
	      DEFINE_STRING(BGl_string3382z00zzsaw_defsz00,
		BgL_bgl_string3382za700za7za7s3837za7, " <- ", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3376z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2funza7d23838za7,
		BGl_z62dumpzd2funzd2rtl_go1816z62zzsaw_defsz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3295z00zzsaw_defsz00,
		BgL_bgl_za762za7c3za704anonymo3839za7,
		BGl_z62zc3z04anonymousza32627ze3ze5zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_boxsetzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_boxsetza7d23840z00,
		BGl_z62rtl_boxsetzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3383z00zzsaw_defsz00,
		BgL_bgl_string3383za700za7za7s3841za7, " (", 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3377z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2funza7d23842za7,
		BGl_z62dumpzd2funzd2rtl_call1818z62zzsaw_defsz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3296z00zzsaw_defsz00,
		BgL_bgl_za762lambda2625za7623843z00, BGl_z62lambda2625z62zzsaw_defsz00, 0L,
		BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3384z00zzsaw_defsz00,
		BgL_bgl_string3384za700za7za7s3844za7, "]", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3378z00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2funza7d23845za7,
		BGl_z62dumpzd2funzd2rtl_pragma1820z62zzsaw_defsz00, 0L, BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3297z00zzsaw_defsz00,
		BgL_bgl_za762lambda2623za7623846z00, BGl_z62lambda2623z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3385z00zzsaw_defsz00,
		BgL_bgl_string3385za700za7za7s3847za7, ":preds ", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3298z00zzsaw_defsz00,
		BgL_bgl_za762lambda2643za7623848z00, BGl_z62lambda2643z62zzsaw_defsz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3386z00zzsaw_defsz00,
		BgL_bgl_string3386za700za7za7s3849za7, ":succs ", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc3299z00zzsaw_defsz00,
		BgL_bgl_za762lambda2642za7623850z00, BGl_z62lambda2642z62zzsaw_defsz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3387z00zzsaw_defsz00,
		BgL_bgl_string3387za700za7za7s3851za7, "\n )\n", 4);
	      DEFINE_STRING(BGl_string3388z00zzsaw_defsz00,
		BgL_bgl_string3388za700za7za7s3852za7, "*", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_retblockzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_retblockza73853z00,
		BGl_z62rtl_retblockzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string3389z00zzsaw_defsz00,
		BgL_bgl_string3389za700za7za7s3854za7, "%", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_loadfunzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_lo3855z00,
		BGl_z62makezd2rtl_loadfunzb0zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_protectedzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_protected3856za7,
		BGl_z62rtl_protectedzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_notseqzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_notseqza7f33857z00, BGl_z62rtl_notseqzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_loadizd2constantzd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_loadiza7d2c3858z00,
		BGl_z62rtl_loadizd2constantzd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_setfieldzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_setfieldza73859z00,
		BGl_z62rtl_setfieldzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_retblockzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_retblockza73860z00,
		BGl_z62rtl_retblockzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3390z00zzsaw_defsz00,
		BgL_bgl_string3390za700za7za7s3861za7, "$", 1);
	      DEFINE_STRING(BGl_string3391z00zzsaw_defsz00,
		BgL_bgl_string3391za700za7za7s3862za7, "@", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_gozd2tozd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_goza7d2toza7d3863za7,
		BGl_z62rtl_gozd2tozd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string3392z00zzsaw_defsz00,
		BgL_bgl_string3392za700za7za7s3864za7, "saw_defs", 8);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_ifnezf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_ifneza7f3za793865za7, BGl_z62rtl_ifnezf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string3393z00zzsaw_defsz00,
		BgL_bgl_string3393za700za7za7s3866za7,
		"_ block first succs preds int label rtl_ins args fun dest %spill rtl_protected rtl_protect rtl_cast_null rtl_cast fromtype totype rtl_pragma srfi0 format rtl_funcall rtl_lightfuncall rettype funs symbol rtl_apply rtl_call rtl_new pair-nil constr rtl_boxset rtl_vset rtl_setfield rtl_storeg rtl_effect rtl_boxref rtl_makebox rtl_instanceof rtl_vlength rtl_vref rtl_valloc vtype rtl_getfield objtype bstring rtl_globalref rtl_loadfun rtl_loadg rtl_loadi constant rtl_mov rtl_nop rtl_pure rtl_go rtl_ifne rtl_ifeq then rtl_switch labels rtl_select patterns rtl_if rtl_notseq rtl_ret to rtl_retblock rtl_fail rtl_jumpexit rtl_return rtl_last rtl_fun loc saw_defs rtl_reg hardware debugname key name onexpr? obj var type #(\"\" \" \" \"  \" \"   \" \"    \" \"     \" \"      \" \"       \") ",
		771);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_vrefzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vrefza7d2ni3867z00, BGl_z62rtl_vrefzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_loadgzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_loadgza7f3za73868za7, BGl_z62rtl_loadgzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_jumpexitzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_jumpexitza73869z00,
		BGl_z62rtl_jumpexitzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_cast_nullzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_cast_null3870za7,
		BGl_z62rtl_cast_nullzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_vsetzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_vs3871z00, BGl_z62makezd2rtl_vsetzb0zzsaw_defsz00,
		0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_loadizd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_loadiza7d2l3872z00,
		BGl_z62rtl_loadizd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_switchzd2labelszd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_switchza7d23873z00,
		BGl_z62rtl_switchzd2labelszb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_castzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_castza7d2lo3874z00, BGl_z62rtl_castzd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_loadizd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_loadiza7d2n3875z00, BGl_z62rtl_loadizd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_jumpexitzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_jumpexitza73876z00,
		BGl_z62rtl_jumpexitzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_boxsetzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_bo3877z00,
		BGl_z62makezd2rtl_boxsetzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_gozd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_go3878z00, BGl_z62makezd2rtl_gozb0zzsaw_defsz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_retblockzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_re3879z00,
		BGl_z62makezd2rtl_retblockzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_switchzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_switchza7d23880z00,
		BGl_z62rtl_switchzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_lightfuncallzd2funszd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_lightfunc3881za7,
		BGl_z62rtl_lightfuncallzd2funszb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_gozd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_goza7d2nilza73882za7, BGl_z62rtl_gozd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_callzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_callza7d2lo3883z00, BGl_z62rtl_callzd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_selectzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_selectza7d23884z00,
		BGl_z62rtl_selectzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_ifnezd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_ifneza7d2ni3885z00, BGl_z62rtl_ifnezd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_inszd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_insza7d2loc3886z00, BGl_z62rtl_inszd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_retblockzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_retblockza73887z00, BGl_z62rtl_retblockzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_pragmazf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_pragmaza7f33888z00, BGl_z62rtl_pragmazf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_makeboxzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_makeboxza7d3889z00,
		BGl_z62rtl_makeboxzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_instanceofzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_instanceo3890za7,
		BGl_z62rtl_instanceofzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_gozd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_goza7d2locza73891za7,
		BGl_z62rtl_gozd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_switchzd2typezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_switchza7d23892z00,
		BGl_z62rtl_switchzd2typezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_lightfuncallzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_lightfunc3893za7,
		BGl_z62rtl_lightfuncallzf3z91zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_vrefzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_vr3894z00, BGl_z62makezd2rtl_vrefzb0zzsaw_defsz00,
		0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzd2onexprzf3zd2setz12zd2envz33zzsaw_defsz00,
		BgL_bgl_za762rtl_regza7d2one3895z00,
		BGl_z62rtl_regzd2onexprzf3zd2setz12z83zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_vsetzd2vtypezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_vsetza7d2vt3896z00,
		BGl_z62rtl_vsetzd2vtypezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_jumpexitzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_ju3897z00,
		BGl_z62makezd2rtl_jumpexitzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_funcallzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_funcallza7f3898z00, BGl_z62rtl_funcallzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_lightfuncallzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_lightfunc3899za7,
		BGl_z62rtl_lightfuncallzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_valloczd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vallocza7d23900z00,
		BGl_z62rtl_valloczd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_ifzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_ifza7f3za791za73901z00, BGl_z62rtl_ifzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_cast_nullzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_cast_null3902za7,
		BGl_z62rtl_cast_nullzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_boxrefzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_bo3903z00,
		BGl_z62makezd2rtl_boxrefzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_loadfunzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_loadfunza7d3904z00,
		BGl_z62rtl_loadfunzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_newzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_newza7d2loc3905z00, BGl_z62rtl_newzd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_jumpexitzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_jumpexitza73906z00, BGl_z62rtl_jumpexitzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_retzd2tozd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_retza7d2toza73907za7, BGl_z62rtl_retzd2tozb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_storegzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_storegza7d23908z00,
		BGl_z62rtl_storegzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_lightfuncallzd2namezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_lightfunc3909za7,
		BGl_z62rtl_lightfuncallzd2namezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_gozf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_goza7f3za791za73910z00, BGl_z62rtl_gozf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_callzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_callza7f3za793911za7, BGl_z62rtl_callzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_lastzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_lastza7d2lo3912z00, BGl_z62rtl_lastzd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_getfieldzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_getfieldza73913z00,
		BGl_z62rtl_getfieldzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_returnzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_returnza7d23914z00,
		BGl_z62rtl_returnzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_effectzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_effectza7f33915z00, BGl_z62rtl_effectzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_castzd2fromtypezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_castza7d2fr3916z00,
		BGl_z62rtl_castzd2fromtypezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_boxrefzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_boxrefza7d23917z00,
		BGl_z62rtl_boxrefzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_globalrefzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_globalref3918za7,
		BGl_z62rtl_globalrefzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_nopzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_nopza7d2loc3919z00, BGl_z62rtl_nopzd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_retzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_retza7d2nil3920z00, BGl_z62rtl_retzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockzd2predszd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762blockza7d2preds3921z00,
		BGl_z62blockzd2predszd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_notseqzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_no3922z00,
		BGl_z62makezd2rtl_notseqzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzd2typezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_regza7d2typ3923z00,
		BGl_z62rtl_regzd2typezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_pragmazd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_pragmaza7d23924z00,
		BGl_z62rtl_pragmazd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockzd2firstzd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762blockza7d2first3925z00,
		BGl_z62blockzd2firstzd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_cast_nullzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_ca3926z00,
		BGl_z62makezd2rtl_cast_nullzb0zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_newzd2typezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_newza7d2typ3927z00,
		BGl_z62rtl_newzd2typezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_vlengthzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_vlengthza7f3928z00, BGl_z62rtl_vlengthzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_nopzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_no3929z00, BGl_z62makezd2rtl_nopzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_ifnezd2thenzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_ifneza7d2th3930z00, BGl_z62rtl_ifnezd2thenzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_ifeqzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_ifeqza7f3za793931za7, BGl_z62rtl_ifeqzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_selectzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_selectza7d23932z00,
		BGl_z62rtl_selectzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2rtl_lightfuncallzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_li3933z00,
		BGl_z62makezd2rtl_lightfuncallzb0zzsaw_defsz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_newzd2constrzd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_newza7d2con3934z00,
		BGl_z62rtl_newzd2constrzd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_loadgzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_loadgza7d2l3935z00,
		BGl_z62rtl_loadgzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_globalrefzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_globalref3936za7, BGl_z62rtl_globalrefzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_setfieldzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_setfieldza73937z00, BGl_z62rtl_setfieldzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_instanceofzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_instanceo3938za7,
		BGl_z62rtl_instanceofzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_applyzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_applyza7d2n3939z00, BGl_z62rtl_applyzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_setfieldzd2typezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_setfieldza73940z00,
		BGl_z62rtl_setfieldzd2typezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_ifzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_if3941z00, BGl_z62makezd2rtl_ifzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_vrefzd2typezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_vrefza7d2ty3942z00,
		BGl_z62rtl_vrefzd2typezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_effectzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_effectza7d23943z00,
		BGl_z62rtl_effectzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_regza7f3za7913944za7, BGl_z62rtl_regzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_purezd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_pu3945z00, BGl_z62makezd2rtl_purezb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_failzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_failza7d2lo3946z00, BGl_z62rtl_failzd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_regzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_re3947z00, BGl_z62makezd2rtl_regzb0zzsaw_defsz00,
		0L, BUNSPEC, 7);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_boxrefzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_boxrefza7d23948z00,
		BGl_z62rtl_boxrefzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_ifnezd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_if3949z00, BGl_z62makezd2rtl_ifnezb0zzsaw_defsz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_cast_nullzd2typezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_cast_null3950za7,
		BGl_z62rtl_cast_nullzd2typezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_getfieldzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_ge3951z00,
		BGl_z62makezd2rtl_getfieldzb0zzsaw_defsz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_funcallzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_fu3952z00,
		BGl_z62makezd2rtl_funcallzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_applyzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_ap3953z00,
		BGl_z62makezd2rtl_applyzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_castzd2totypezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_castza7d2to3954z00,
		BGl_z62rtl_castzd2totypezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_instanceofzd2typezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_instanceo3955za7,
		BGl_z62rtl_instanceofzd2typezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_inszd2funzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_insza7d2fun3956z00, BGl_z62rtl_inszd2funzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_purezd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_pureza7d2lo3957z00,
		BGl_z62rtl_purezd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_lightfuncallzd2namezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_lightfunc3958za7,
		BGl_z62rtl_lightfuncallzd2namezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_protectzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_protectza7d3959z00,
		BGl_z62rtl_protectzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_getfieldzd2objtypezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_getfieldza73960z00,
		BGl_z62rtl_getfieldzd2objtypezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_instanceofzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_instanceo3961za7, BGl_z62rtl_instanceofzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_selectzd2typezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_selectza7d23962z00,
		BGl_z62rtl_selectzd2typezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_retzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_retza7f3za7913963za7, BGl_z62rtl_retzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_switchzd2labelszd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_switchza7d23964z00,
		BGl_z62rtl_switchzd2labelszd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_vsetzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vsetza7d2lo3965z00, BGl_z62rtl_vsetzd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_newzd2constrzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_newza7d2con3966z00,
		BGl_z62rtl_newzd2constrzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_setfieldzd2objtypezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_setfieldza73967z00,
		BGl_z62rtl_setfieldzd2objtypezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_ifzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_ifza7d2locza73968za7,
		BGl_z62rtl_ifzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762blockza7f3za791za7za73969za7, BGl_z62blockzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_castzd2totypezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_castza7d2to3970z00,
		BGl_z62rtl_castzd2totypezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_storegzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_st3971z00,
		BGl_z62makezd2rtl_storegzb0zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_inszd2z52spillzd2envz52zzsaw_defsz00,
		BgL_bgl_za762rtl_insza7d2za7523972za7,
		BGl_z62rtl_inszd2z52spillze2zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_pragmazd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_pr3973z00,
		BGl_z62makezd2rtl_pragmazb0zzsaw_defsz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_boxrefzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_boxrefza7f33974z00, BGl_z62rtl_boxrefzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_returnzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_returnza7d23975z00,
		BGl_z62rtl_returnzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_valloczd2vtypezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vallocza7d23976z00,
		BGl_z62rtl_valloczd2vtypezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_vlengthzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vlengthza7d3977z00,
		BGl_z62rtl_vlengthzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_notseqzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_notseqza7d23978z00,
		BGl_z62rtl_notseqzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_purezd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_pureza7d2ni3979z00, BGl_z62rtl_purezd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_instanceofzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_in3980z00,
		BGl_z62makezd2rtl_instanceofzb0zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_movzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_movza7d2loc3981z00,
		BGl_z62rtl_movzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_ifnezd2thenzd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_ifneza7d2th3982z00,
		BGl_z62rtl_ifnezd2thenzd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_failzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_failza7f3za793983za7, BGl_z62rtl_failzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_cast_nullzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_cast_null3984za7, BGl_z62rtl_cast_nullzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_loadizf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_loadiza7f3za73985za7, BGl_z62rtl_loadizf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_getfieldzd2typezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_getfieldza73986z00,
		BGl_z62rtl_getfieldzd2typezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_ifeqzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_if3987z00, BGl_z62makezd2rtl_ifeqzb0zzsaw_defsz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_nopzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_nopza7d2loc3988z00,
		BGl_z62rtl_nopzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzd2keyzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_regza7d2key3989z00, BGl_z62rtl_regzd2keyzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_vlengthzd2vtypezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_vlengthza7d3990z00,
		BGl_z62rtl_vlengthzd2vtypezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_ifeqzd2thenzd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_ifeqza7d2th3991z00,
		BGl_z62rtl_ifeqzd2thenzd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_movzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_movza7d2loc3992z00, BGl_z62rtl_movzd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_ifzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_ifza7d2nilza73993za7, BGl_z62rtl_ifzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_returnzd2typezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_returnza7d23994z00,
		BGl_z62rtl_returnzd2typezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_getfieldzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_getfieldza73995z00,
		BGl_z62rtl_getfieldzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_movzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_movza7f3za7913996za7, BGl_z62rtl_movzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_vrefzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_vrefza7f3za793997za7, BGl_z62rtl_vrefzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_funzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_fu3998z00, BGl_z62makezd2rtl_funzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_storegzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_storegza7d23999z00,
		BGl_z62rtl_storegzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_setfieldzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_se4000z00,
		BGl_z62makezd2rtl_setfieldzb0zzsaw_defsz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_vlengthzd2vtypezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vlengthza7d4001z00,
		BGl_z62rtl_vlengthzd2vtypezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_globalrefzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_globalref4002za7,
		BGl_z62rtl_globalrefzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_globalrefzd2varzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_globalref4003za7,
		BGl_z62rtl_globalrefzd2varzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_valloczd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_vallocza7d24004z00,
		BGl_z62rtl_valloczd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzd2namezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_regza7d2nam4005z00, BGl_z62rtl_regzd2namezb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_funzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_funza7d2loc4006z00,
		BGl_z62rtl_funzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_blockzd2firstzd2envz00zzsaw_defsz00,
		BgL_bgl_za762blockza7d2first4007z00, BGl_z62blockzd2firstzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_funcallzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_funcallza7d4008z00,
		BGl_z62rtl_funcallzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_lastzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_lastza7d2lo4009z00,
		BGl_z62rtl_lastzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_funzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_funza7d2nil4010z00, BGl_z62rtl_funzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_boxsetzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_boxsetza7d24011z00,
		BGl_z62rtl_boxsetzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_dumpzd2funzd2envz00zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2funza7b04012za7, BGl_z62dumpzd2funzb0zzsaw_defsz00,
		0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_dumpzd2basiczd2blockszd2envzd2zzsaw_defsz00,
		BgL_bgl_za762dumpza7d2basicza74013za7,
		BGl_z62dumpzd2basiczd2blocksz62zzsaw_defsz00, 0L, BUNSPEC, 4);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_lightfuncallzd2funszd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_lightfunc4014za7,
		BGl_z62rtl_lightfuncallzd2funszd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_inszd2destzd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_insza7d2des4015z00,
		BGl_z62rtl_inszd2destzd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_retblockzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_retblockza74016z00,
		BGl_z62rtl_retblockzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_loadgzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_lo4017z00,
		BGl_z62makezd2rtl_loadgzb0zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_switchzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_switchza7d24018z00,
		BGl_z62rtl_switchzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_vrefzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vrefza7d2lo4019z00, BGl_z62rtl_vrefzd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_globalrefzd2varzd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_globalref4020za7,
		BGl_z62rtl_globalrefzd2varzd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockzd2labelzd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762blockza7d2label4021z00,
		BGl_z62blockzd2labelzd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_vlengthzd2typezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_vlengthza7d4022z00,
		BGl_z62rtl_vlengthzd2typezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_jumpexitzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_jumpexitza74023z00,
		BGl_z62rtl_jumpexitzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzd2varzd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_regza7d2var4024z00,
		BGl_z62rtl_regzd2varzd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_retzd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_retza7d2loc4025z00,
		BGl_z62rtl_retzd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_loadizd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_loadiza7d2l4026z00, BGl_z62rtl_loadizd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_inszd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_in4027z00, BGl_z62makezd2rtl_inszb0zzsaw_defsz00,
		0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_protectedzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_pr4028z00,
		BGl_z62makezd2rtl_protectedzb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_switchzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_switchza7d24029z00,
		BGl_z62rtl_switchzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_gozd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_goza7d2locza74030za7, BGl_z62rtl_gozd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_switchzd2typezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_switchza7d24031z00,
		BGl_z62rtl_switchzd2typezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_vrefzd2typezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vrefza7d2ty4032z00, BGl_z62rtl_vrefzd2typezb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_valloczd2typezd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_vallocza7d24033z00,
		BGl_z62rtl_valloczd2typezd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_ifnezd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_ifneza7d2lo4034z00, BGl_z62rtl_ifnezd2loczb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_selectzd2typezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_selectza7d24035z00,
		BGl_z62rtl_selectzd2typezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_switchzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_switchza7f34036z00, BGl_z62rtl_switchzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_makeboxzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_makeboxza7d4037z00,
		BGl_z62rtl_makeboxzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_newzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_ne4038z00, BGl_z62makezd2rtl_newzb0zzsaw_defsz00,
		0L, BUNSPEC, 3);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_instanceofzd2loczd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_instanceo4039za7,
		BGl_z62rtl_instanceofzd2loczb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_loadgzd2varzd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_loadgza7d2v4040z00,
		BGl_z62rtl_loadgzd2varzd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_pragmazd2loczd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_pragmaza7d24041z00,
		BGl_z62rtl_pragmazd2loczd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_blockzd2succszd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762blockza7d2succs4042z00,
		BGl_z62blockzd2succszd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_protectzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_protectza7d4043z00,
		BGl_z62rtl_protectzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_ifeqzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_ifeqza7d2ni4044z00, BGl_z62rtl_ifeqzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_loadgzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_loadgza7d2n4045z00, BGl_z62rtl_loadgzd2nilzb0zzsaw_defsz00,
		0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_loadgzd2varzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_loadgza7d2v4046z00, BGl_z62rtl_loadgzd2varzb0zzsaw_defsz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_pragmazd2srfi0zd2setz12zd2envzc0zzsaw_defsz00,
		BgL_bgl_za762rtl_pragmaza7d24047z00,
		BGl_z62rtl_pragmazd2srfi0zd2setz12z70zzsaw_defsz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_boxsetzd2nilzd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_boxsetza7d24048z00,
		BGl_z62rtl_boxsetzd2nilzb0zzsaw_defsz00, 0L, BUNSPEC, 0);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_makeboxzf3zd2envz21zzsaw_defsz00,
		BgL_bgl_za762rtl_makeboxza7f4049z00, BGl_z62rtl_makeboxzf3z91zzsaw_defsz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_callzd2envz00zzsaw_defsz00,
		BgL_bgl_za762makeza7d2rtl_ca4050z00, BGl_z62makezd2rtl_callzb0zzsaw_defsz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_vsetzd2vtypezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vsetza7d2vt4051z00,
		BGl_z62rtl_vsetzd2vtypezb0zzsaw_defsz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_valloczd2typezd2envz00zzsaw_defsz00,
		BgL_bgl_za762rtl_vallocza7d24052z00,
		BGl_z62rtl_valloczd2typezb0zzsaw_defsz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_rtl_insz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_funcallz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_funz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_newz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_pragmaz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_boxrefz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_vrefz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_protectz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_ifeqz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_boxsetz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_vlengthz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_purez00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_makeboxz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_vsetz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_lightfuncallz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_regz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_vallocz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_cast_nullz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_loadfunz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_retz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_movz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_nopz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_retblockz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_jumpexitz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_switchz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_goz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_ifnez00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_instanceofz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_setfieldz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_ifz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_callz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_selectz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_storegz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_lastz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_returnz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_applyz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_debugzd2sawzd2zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_loadgz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_loadiz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_notseqz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_failz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_castz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_globalrefz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_getfieldz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_effectz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_rtl_protectedz00zzsaw_defsz00));
		     ADD_ROOT((void *) (&BGl_blockz00zzsaw_defsz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long
		BgL_checksumz00_10164, char *BgL_fromz00_10165)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_defsz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_defsz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_defsz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_defsz00();
					BGl_cnstzd2initzd2zzsaw_defsz00();
					BGl_importedzd2moduleszd2initz00zzsaw_defsz00();
					BGl_objectzd2initzd2zzsaw_defsz00();
					BGl_genericzd2initzd2zzsaw_defsz00();
					BGl_methodzd2initzd2zzsaw_defsz00();
					return BGl_toplevelzd2initzd2zzsaw_defsz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.scm 1 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_defs");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "saw_defs");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "saw_defs");
			BGl_modulezd2initializa7ationz75zz__osz00(0L, "saw_defs");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_defs");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "saw_defs");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_defs");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "saw_defs");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "saw_defs");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_defs");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "saw_defs");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_defs");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_defs");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_defs");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.scm 1 */
			{	/* SawMill/defs.scm 1 */
				obj_t BgL_cportz00_8694;

				{	/* SawMill/defs.scm 1 */
					obj_t BgL_stringz00_8701;

					BgL_stringz00_8701 = BGl_string3393z00zzsaw_defsz00;
					{	/* SawMill/defs.scm 1 */
						obj_t BgL_startz00_8702;

						BgL_startz00_8702 = BINT(0L);
						{	/* SawMill/defs.scm 1 */
							obj_t BgL_endz00_8703;

							BgL_endz00_8703 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_8701)));
							{	/* SawMill/defs.scm 1 */

								BgL_cportz00_8694 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_8701, BgL_startz00_8702, BgL_endz00_8703);
				}}}}
				{
					long BgL_iz00_8695;

					BgL_iz00_8695 = 83L;
				BgL_loopz00_8696:
					if ((BgL_iz00_8695 == -1L))
						{	/* SawMill/defs.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawMill/defs.scm 1 */
							{	/* SawMill/defs.scm 1 */
								obj_t BgL_arg3394z00_8697;

								{	/* SawMill/defs.scm 1 */

									{	/* SawMill/defs.scm 1 */
										obj_t BgL_locationz00_8699;

										BgL_locationz00_8699 = BBOOL(((bool_t) 0));
										{	/* SawMill/defs.scm 1 */

											BgL_arg3394z00_8697 =
												BGl_readz00zz__readerz00(BgL_cportz00_8694,
												BgL_locationz00_8699);
										}
									}
								}
								{	/* SawMill/defs.scm 1 */
									int BgL_tmpz00_10200;

									BgL_tmpz00_10200 = (int) (BgL_iz00_8695);
									CNST_TABLE_SET(BgL_tmpz00_10200, BgL_arg3394z00_8697);
							}}
							{	/* SawMill/defs.scm 1 */
								int BgL_auxz00_8700;

								BgL_auxz00_8700 = (int) ((BgL_iz00_8695 - 1L));
								{
									long BgL_iz00_10205;

									BgL_iz00_10205 = (long) (BgL_auxz00_8700);
									BgL_iz00_8695 = BgL_iz00_10205;
									goto BgL_loopz00_8696;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.scm 1 */
			{	/* SawMill/defs.scm 100 */
				obj_t BgL_ez00_1889;

				BgL_ez00_1889 = BGl_getenvz00zz__osz00(BGl_string3079z00zzsaw_defsz00);
				if (STRINGP(BgL_ez00_1889))
					{	/* SawMill/defs.scm 102 */
						bool_t BgL__ortest_1439z00_1891;

						{	/* SawMill/defs.scm 102 */

							BgL__ortest_1439z00_1891 =
								BGl_stringzd2prefixzf3z21zz__r4_strings_6_7z00
								(BGl_string3080z00zzsaw_defsz00, BgL_ez00_1889, BFALSE, BFALSE,
								BFALSE, BFALSE);
						}
						if (BgL__ortest_1439z00_1891)
							{	/* SawMill/defs.scm 102 */
								BGl_debugzd2sawzd2zzsaw_defsz00 =
									BBOOL(BgL__ortest_1439z00_1891);
							}
						else
							{	/* SawMill/defs.scm 103 */
								bool_t BgL__ortest_1440z00_1892;

								{	/* SawMill/defs.scm 103 */

									BgL__ortest_1440z00_1892 =
										BGl_stringzd2suffixzf3z21zz__r4_strings_6_7z00
										(BGl_string3081z00zzsaw_defsz00, BgL_ez00_1889, BFALSE,
										BFALSE, BFALSE, BFALSE);
								}
								if (BgL__ortest_1440z00_1892)
									{	/* SawMill/defs.scm 103 */
										BGl_debugzd2sawzd2zzsaw_defsz00 =
											BBOOL(BgL__ortest_1440z00_1892);
									}
								else
									{	/* SawMill/defs.scm 104 */
										obj_t BgL__ortest_1441z00_1893;

										{	/* SawMill/defs.scm 104 */

											BgL__ortest_1441z00_1893 =
												BGl_stringzd2containszd2zz__r4_strings_6_7z00
												(BgL_ez00_1889, BGl_string3082z00zzsaw_defsz00,
												(int) (0L));
										}
										if (CBOOL(BgL__ortest_1441z00_1893))
											{	/* SawMill/defs.scm 104 */
												BGl_debugzd2sawzd2zzsaw_defsz00 =
													BgL__ortest_1441z00_1893;
											}
										else
											{	/* SawMill/defs.scm 104 */
												if ((3L == STRING_LENGTH(BgL_ez00_1889)))
													{	/* SawMill/defs.scm 105 */
														int BgL_arg1282z00_4160;

														{	/* SawMill/defs.scm 105 */
															char *BgL_auxz00_10226;
															char *BgL_tmpz00_10224;

															BgL_auxz00_10226 =
																BSTRING_TO_STRING(BgL_ez00_1889);
															BgL_tmpz00_10224 =
																BSTRING_TO_STRING
																(BGl_string3083z00zzsaw_defsz00);
															BgL_arg1282z00_4160 =
																memcmp(BgL_tmpz00_10224, BgL_auxz00_10226, 3L);
														}
														BGl_debugzd2sawzd2zzsaw_defsz00 =
															BBOOL(((long) (BgL_arg1282z00_4160) == 0L));
													}
												else
													{	/* SawMill/defs.scm 105 */
														BGl_debugzd2sawzd2zzsaw_defsz00 = BFALSE;
													}
											}
									}
							}
					}
				else
					{	/* SawMill/defs.scm 101 */
						BGl_debugzd2sawzd2zzsaw_defsz00 = BFALSE;
					}
			}
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzsaw_defsz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1909;

				BgL_headz00_1909 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1910;
					obj_t BgL_tailz00_1911;

					BgL_prevz00_1910 = BgL_headz00_1909;
					BgL_tailz00_1911 = BgL_l1z00_1;
				BgL_loopz00_1912:
					if (PAIRP(BgL_tailz00_1911))
						{
							obj_t BgL_newzd2prevzd2_1914;

							BgL_newzd2prevzd2_1914 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1911), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1910, BgL_newzd2prevzd2_1914);
							{
								obj_t BgL_tailz00_10239;
								obj_t BgL_prevz00_10238;

								BgL_prevz00_10238 = BgL_newzd2prevzd2_1914;
								BgL_tailz00_10239 = CDR(BgL_tailz00_1911);
								BgL_tailz00_1911 = BgL_tailz00_10239;
								BgL_prevz00_1910 = BgL_prevz00_10238;
								goto BgL_loopz00_1912;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1909);
				}
			}
		}

	}



/* make-rtl_reg */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzd2zzsaw_defsz00(BgL_typez00_bglt BgL_type1505z00_3,
		obj_t BgL_var1506z00_4, obj_t BgL_onexprzf31507zf3_5,
		obj_t BgL_name1508z00_6, obj_t BgL_key1509z00_7,
		obj_t BgL_debugname1510z00_8, obj_t BgL_hardware1511z00_9)
	{
		{	/* SawMill/defs.sch 533 */
			{	/* SawMill/defs.sch 533 */
				BgL_rtl_regz00_bglt BgL_new1348z00_8705;

				{	/* SawMill/defs.sch 533 */
					BgL_rtl_regz00_bglt BgL_new1347z00_8706;

					BgL_new1347z00_8706 =
						((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regz00_bgl))));
					{	/* SawMill/defs.sch 533 */
						long BgL_arg1832z00_8707;

						BgL_arg1832z00_8707 = BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1347z00_8706), BgL_arg1832z00_8707);
					}
					{	/* SawMill/defs.sch 533 */
						BgL_objectz00_bglt BgL_tmpz00_10246;

						BgL_tmpz00_10246 = ((BgL_objectz00_bglt) BgL_new1347z00_8706);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_10246, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1347z00_8706);
					BgL_new1348z00_8705 = BgL_new1347z00_8706;
				}
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1348z00_8705))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1505z00_3), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1348z00_8705))->BgL_varz00) =
					((obj_t) BgL_var1506z00_4), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1348z00_8705))->
						BgL_onexprzf3zf3) = ((obj_t) BgL_onexprzf31507zf3_5), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1348z00_8705))->BgL_namez00) =
					((obj_t) BgL_name1508z00_6), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1348z00_8705))->BgL_keyz00) =
					((obj_t) BgL_key1509z00_7), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1348z00_8705))->
						BgL_debugnamez00) = ((obj_t) BgL_debugname1510z00_8), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1348z00_8705))->
						BgL_hardwarez00) = ((obj_t) BgL_hardware1511z00_9), BUNSPEC);
				return BgL_new1348z00_8705;
			}
		}

	}



/* &make-rtl_reg */
	BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzb0zzsaw_defsz00(obj_t
		BgL_envz00_6925, obj_t BgL_type1505z00_6926, obj_t BgL_var1506z00_6927,
		obj_t BgL_onexprzf31507zf3_6928, obj_t BgL_name1508z00_6929,
		obj_t BgL_key1509z00_6930, obj_t BgL_debugname1510z00_6931,
		obj_t BgL_hardware1511z00_6932)
	{
		{	/* SawMill/defs.sch 533 */
			return
				BGl_makezd2rtl_regzd2zzsaw_defsz00(
				((BgL_typez00_bglt) BgL_type1505z00_6926), BgL_var1506z00_6927,
				BgL_onexprzf31507zf3_6928, BgL_name1508z00_6929, BgL_key1509z00_6930,
				BgL_debugname1510z00_6931, BgL_hardware1511z00_6932);
		}

	}



/* rtl_reg? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_regzf3zf3zzsaw_defsz00(obj_t BgL_objz00_10)
	{
		{	/* SawMill/defs.sch 534 */
			{	/* SawMill/defs.sch 534 */
				obj_t BgL_classz00_8708;

				BgL_classz00_8708 = BGl_rtl_regz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_10))
					{	/* SawMill/defs.sch 534 */
						BgL_objectz00_bglt BgL_arg1807z00_8709;

						BgL_arg1807z00_8709 = (BgL_objectz00_bglt) (BgL_objz00_10);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 534 */
								long BgL_idxz00_8710;

								BgL_idxz00_8710 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8709);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8710 + 1L)) == BgL_classz00_8708);
							}
						else
							{	/* SawMill/defs.sch 534 */
								bool_t BgL_res3017z00_8713;

								{	/* SawMill/defs.sch 534 */
									obj_t BgL_oclassz00_8714;

									{	/* SawMill/defs.sch 534 */
										obj_t BgL_arg1815z00_8715;
										long BgL_arg1816z00_8716;

										BgL_arg1815z00_8715 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 534 */
											long BgL_arg1817z00_8717;

											BgL_arg1817z00_8717 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8709);
											BgL_arg1816z00_8716 = (BgL_arg1817z00_8717 - OBJECT_TYPE);
										}
										BgL_oclassz00_8714 =
											VECTOR_REF(BgL_arg1815z00_8715, BgL_arg1816z00_8716);
									}
									{	/* SawMill/defs.sch 534 */
										bool_t BgL__ortest_1115z00_8718;

										BgL__ortest_1115z00_8718 =
											(BgL_classz00_8708 == BgL_oclassz00_8714);
										if (BgL__ortest_1115z00_8718)
											{	/* SawMill/defs.sch 534 */
												BgL_res3017z00_8713 = BgL__ortest_1115z00_8718;
											}
										else
											{	/* SawMill/defs.sch 534 */
												long BgL_odepthz00_8719;

												{	/* SawMill/defs.sch 534 */
													obj_t BgL_arg1804z00_8720;

													BgL_arg1804z00_8720 = (BgL_oclassz00_8714);
													BgL_odepthz00_8719 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8720);
												}
												if ((1L < BgL_odepthz00_8719))
													{	/* SawMill/defs.sch 534 */
														obj_t BgL_arg1802z00_8721;

														{	/* SawMill/defs.sch 534 */
															obj_t BgL_arg1803z00_8722;

															BgL_arg1803z00_8722 = (BgL_oclassz00_8714);
															BgL_arg1802z00_8721 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8722,
																1L);
														}
														BgL_res3017z00_8713 =
															(BgL_arg1802z00_8721 == BgL_classz00_8708);
													}
												else
													{	/* SawMill/defs.sch 534 */
														BgL_res3017z00_8713 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3017z00_8713;
							}
					}
				else
					{	/* SawMill/defs.sch 534 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_reg? */
	obj_t BGl_z62rtl_regzf3z91zzsaw_defsz00(obj_t BgL_envz00_6933,
		obj_t BgL_objz00_6934)
	{
		{	/* SawMill/defs.sch 534 */
			return BBOOL(BGl_rtl_regzf3zf3zzsaw_defsz00(BgL_objz00_6934));
		}

	}



/* rtl_reg-nil */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt BGl_rtl_regzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 535 */
			{	/* SawMill/defs.sch 535 */
				obj_t BgL_classz00_4211;

				BgL_classz00_4211 = BGl_rtl_regz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 535 */
					obj_t BgL__ortest_1117z00_4212;

					BgL__ortest_1117z00_4212 = BGL_CLASS_NIL(BgL_classz00_4211);
					if (CBOOL(BgL__ortest_1117z00_4212))
						{	/* SawMill/defs.sch 535 */
							return ((BgL_rtl_regz00_bglt) BgL__ortest_1117z00_4212);
						}
					else
						{	/* SawMill/defs.sch 535 */
							return
								((BgL_rtl_regz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4211));
						}
				}
			}
		}

	}



/* &rtl_reg-nil */
	BgL_rtl_regz00_bglt BGl_z62rtl_regzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_6935)
	{
		{	/* SawMill/defs.sch 535 */
			return BGl_rtl_regzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_reg-hardware */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzd2hardwarezd2zzsaw_defsz00(BgL_rtl_regz00_bglt BgL_oz00_11)
	{
		{	/* SawMill/defs.sch 536 */
			return (((BgL_rtl_regz00_bglt) COBJECT(BgL_oz00_11))->BgL_hardwarez00);
		}

	}



/* &rtl_reg-hardware */
	obj_t BGl_z62rtl_regzd2hardwarezb0zzsaw_defsz00(obj_t BgL_envz00_6936,
		obj_t BgL_oz00_6937)
	{
		{	/* SawMill/defs.sch 536 */
			return
				BGl_rtl_regzd2hardwarezd2zzsaw_defsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_6937));
		}

	}



/* rtl_reg-debugname */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzd2debugnamezd2zzsaw_defsz00(BgL_rtl_regz00_bglt BgL_oz00_14)
	{
		{	/* SawMill/defs.sch 538 */
			return (((BgL_rtl_regz00_bglt) COBJECT(BgL_oz00_14))->BgL_debugnamez00);
		}

	}



/* &rtl_reg-debugname */
	obj_t BGl_z62rtl_regzd2debugnamezb0zzsaw_defsz00(obj_t BgL_envz00_6938,
		obj_t BgL_oz00_6939)
	{
		{	/* SawMill/defs.sch 538 */
			return
				BGl_rtl_regzd2debugnamezd2zzsaw_defsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_6939));
		}

	}



/* rtl_reg-debugname-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzd2debugnamezd2setz12z12zzsaw_defsz00(BgL_rtl_regz00_bglt
		BgL_oz00_15, obj_t BgL_vz00_16)
	{
		{	/* SawMill/defs.sch 539 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_oz00_15))->BgL_debugnamez00) =
				((obj_t) BgL_vz00_16), BUNSPEC);
		}

	}



/* &rtl_reg-debugname-set! */
	obj_t BGl_z62rtl_regzd2debugnamezd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_6940, obj_t BgL_oz00_6941, obj_t BgL_vz00_6942)
	{
		{	/* SawMill/defs.sch 539 */
			return
				BGl_rtl_regzd2debugnamezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_6941), BgL_vz00_6942);
		}

	}



/* rtl_reg-key */
	BGL_EXPORTED_DEF obj_t BGl_rtl_regzd2keyzd2zzsaw_defsz00(BgL_rtl_regz00_bglt
		BgL_oz00_17)
	{
		{	/* SawMill/defs.sch 540 */
			return (((BgL_rtl_regz00_bglt) COBJECT(BgL_oz00_17))->BgL_keyz00);
		}

	}



/* &rtl_reg-key */
	obj_t BGl_z62rtl_regzd2keyzb0zzsaw_defsz00(obj_t BgL_envz00_6943,
		obj_t BgL_oz00_6944)
	{
		{	/* SawMill/defs.sch 540 */
			return
				BGl_rtl_regzd2keyzd2zzsaw_defsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_6944));
		}

	}



/* rtl_reg-name */
	BGL_EXPORTED_DEF obj_t BGl_rtl_regzd2namezd2zzsaw_defsz00(BgL_rtl_regz00_bglt
		BgL_oz00_20)
	{
		{	/* SawMill/defs.sch 542 */
			return (((BgL_rtl_regz00_bglt) COBJECT(BgL_oz00_20))->BgL_namez00);
		}

	}



/* &rtl_reg-name */
	obj_t BGl_z62rtl_regzd2namezb0zzsaw_defsz00(obj_t BgL_envz00_6945,
		obj_t BgL_oz00_6946)
	{
		{	/* SawMill/defs.sch 542 */
			return
				BGl_rtl_regzd2namezd2zzsaw_defsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_6946));
		}

	}



/* rtl_reg-onexpr? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzd2onexprzf3z21zzsaw_defsz00(BgL_rtl_regz00_bglt BgL_oz00_23)
	{
		{	/* SawMill/defs.sch 544 */
			return (((BgL_rtl_regz00_bglt) COBJECT(BgL_oz00_23))->BgL_onexprzf3zf3);
		}

	}



/* &rtl_reg-onexpr? */
	obj_t BGl_z62rtl_regzd2onexprzf3z43zzsaw_defsz00(obj_t BgL_envz00_6947,
		obj_t BgL_oz00_6948)
	{
		{	/* SawMill/defs.sch 544 */
			return
				BGl_rtl_regzd2onexprzf3z21zzsaw_defsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_6948));
		}

	}



/* rtl_reg-onexpr?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzd2onexprzf3zd2setz12ze1zzsaw_defsz00(BgL_rtl_regz00_bglt
		BgL_oz00_24, obj_t BgL_vz00_25)
	{
		{	/* SawMill/defs.sch 545 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_oz00_24))->BgL_onexprzf3zf3) =
				((obj_t) BgL_vz00_25), BUNSPEC);
		}

	}



/* &rtl_reg-onexpr?-set! */
	obj_t BGl_z62rtl_regzd2onexprzf3zd2setz12z83zzsaw_defsz00(obj_t
		BgL_envz00_6949, obj_t BgL_oz00_6950, obj_t BgL_vz00_6951)
	{
		{	/* SawMill/defs.sch 545 */
			return
				BGl_rtl_regzd2onexprzf3zd2setz12ze1zzsaw_defsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_6950), BgL_vz00_6951);
		}

	}



/* rtl_reg-var */
	BGL_EXPORTED_DEF obj_t BGl_rtl_regzd2varzd2zzsaw_defsz00(BgL_rtl_regz00_bglt
		BgL_oz00_26)
	{
		{	/* SawMill/defs.sch 546 */
			return (((BgL_rtl_regz00_bglt) COBJECT(BgL_oz00_26))->BgL_varz00);
		}

	}



/* &rtl_reg-var */
	obj_t BGl_z62rtl_regzd2varzb0zzsaw_defsz00(obj_t BgL_envz00_6952,
		obj_t BgL_oz00_6953)
	{
		{	/* SawMill/defs.sch 546 */
			return
				BGl_rtl_regzd2varzd2zzsaw_defsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_6953));
		}

	}



/* rtl_reg-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzd2varzd2setz12z12zzsaw_defsz00(BgL_rtl_regz00_bglt BgL_oz00_27,
		obj_t BgL_vz00_28)
	{
		{	/* SawMill/defs.sch 547 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_oz00_27))->BgL_varz00) =
				((obj_t) BgL_vz00_28), BUNSPEC);
		}

	}



/* &rtl_reg-var-set! */
	obj_t BGl_z62rtl_regzd2varzd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_6954,
		obj_t BgL_oz00_6955, obj_t BgL_vz00_6956)
	{
		{	/* SawMill/defs.sch 547 */
			return
				BGl_rtl_regzd2varzd2setz12z12zzsaw_defsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_6955), BgL_vz00_6956);
		}

	}



/* rtl_reg-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_regzd2typezd2zzsaw_defsz00(BgL_rtl_regz00_bglt BgL_oz00_29)
	{
		{	/* SawMill/defs.sch 548 */
			return (((BgL_rtl_regz00_bglt) COBJECT(BgL_oz00_29))->BgL_typez00);
		}

	}



/* &rtl_reg-type */
	BgL_typez00_bglt BGl_z62rtl_regzd2typezb0zzsaw_defsz00(obj_t BgL_envz00_6957,
		obj_t BgL_oz00_6958)
	{
		{	/* SawMill/defs.sch 548 */
			return
				BGl_rtl_regzd2typezd2zzsaw_defsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_6958));
		}

	}



/* rtl_reg-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_regz00_bglt BgL_oz00_30,
		BgL_typez00_bglt BgL_vz00_31)
	{
		{	/* SawMill/defs.sch 549 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_oz00_30))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_31), BUNSPEC);
		}

	}



/* &rtl_reg-type-set! */
	obj_t BGl_z62rtl_regzd2typezd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_6959,
		obj_t BgL_oz00_6960, obj_t BgL_vz00_6961)
	{
		{	/* SawMill/defs.sch 549 */
			return
				BGl_rtl_regzd2typezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_6960),
				((BgL_typez00_bglt) BgL_vz00_6961));
		}

	}



/* make-rtl_fun */
	BGL_EXPORTED_DEF BgL_rtl_funz00_bglt BGl_makezd2rtl_funzd2zzsaw_defsz00(obj_t
		BgL_loc1503z00_32)
	{
		{	/* SawMill/defs.sch 552 */
			{	/* SawMill/defs.sch 552 */
				BgL_rtl_funz00_bglt BgL_new1350z00_8723;

				{	/* SawMill/defs.sch 552 */
					BgL_rtl_funz00_bglt BgL_new1349z00_8724;

					BgL_new1349z00_8724 =
						((BgL_rtl_funz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_funz00_bgl))));
					{	/* SawMill/defs.sch 552 */
						long BgL_arg1833z00_8725;

						BgL_arg1833z00_8725 = BGL_CLASS_NUM(BGl_rtl_funz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1349z00_8724), BgL_arg1833z00_8725);
					}
					BgL_new1350z00_8723 = BgL_new1349z00_8724;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(BgL_new1350z00_8723))->BgL_locz00) =
					((obj_t) BgL_loc1503z00_32), BUNSPEC);
				return BgL_new1350z00_8723;
			}
		}

	}



/* &make-rtl_fun */
	BgL_rtl_funz00_bglt BGl_z62makezd2rtl_funzb0zzsaw_defsz00(obj_t
		BgL_envz00_6962, obj_t BgL_loc1503z00_6963)
	{
		{	/* SawMill/defs.sch 552 */
			return BGl_makezd2rtl_funzd2zzsaw_defsz00(BgL_loc1503z00_6963);
		}

	}



/* rtl_fun? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_funzf3zf3zzsaw_defsz00(obj_t BgL_objz00_33)
	{
		{	/* SawMill/defs.sch 553 */
			{	/* SawMill/defs.sch 553 */
				obj_t BgL_classz00_8726;

				BgL_classz00_8726 = BGl_rtl_funz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_33))
					{	/* SawMill/defs.sch 553 */
						BgL_objectz00_bglt BgL_arg1807z00_8727;

						BgL_arg1807z00_8727 = (BgL_objectz00_bglt) (BgL_objz00_33);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 553 */
								long BgL_idxz00_8728;

								BgL_idxz00_8728 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8727);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8728 + 1L)) == BgL_classz00_8726);
							}
						else
							{	/* SawMill/defs.sch 553 */
								bool_t BgL_res3018z00_8731;

								{	/* SawMill/defs.sch 553 */
									obj_t BgL_oclassz00_8732;

									{	/* SawMill/defs.sch 553 */
										obj_t BgL_arg1815z00_8733;
										long BgL_arg1816z00_8734;

										BgL_arg1815z00_8733 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 553 */
											long BgL_arg1817z00_8735;

											BgL_arg1817z00_8735 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8727);
											BgL_arg1816z00_8734 = (BgL_arg1817z00_8735 - OBJECT_TYPE);
										}
										BgL_oclassz00_8732 =
											VECTOR_REF(BgL_arg1815z00_8733, BgL_arg1816z00_8734);
									}
									{	/* SawMill/defs.sch 553 */
										bool_t BgL__ortest_1115z00_8736;

										BgL__ortest_1115z00_8736 =
											(BgL_classz00_8726 == BgL_oclassz00_8732);
										if (BgL__ortest_1115z00_8736)
											{	/* SawMill/defs.sch 553 */
												BgL_res3018z00_8731 = BgL__ortest_1115z00_8736;
											}
										else
											{	/* SawMill/defs.sch 553 */
												long BgL_odepthz00_8737;

												{	/* SawMill/defs.sch 553 */
													obj_t BgL_arg1804z00_8738;

													BgL_arg1804z00_8738 = (BgL_oclassz00_8732);
													BgL_odepthz00_8737 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8738);
												}
												if ((1L < BgL_odepthz00_8737))
													{	/* SawMill/defs.sch 553 */
														obj_t BgL_arg1802z00_8739;

														{	/* SawMill/defs.sch 553 */
															obj_t BgL_arg1803z00_8740;

															BgL_arg1803z00_8740 = (BgL_oclassz00_8732);
															BgL_arg1802z00_8739 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8740,
																1L);
														}
														BgL_res3018z00_8731 =
															(BgL_arg1802z00_8739 == BgL_classz00_8726);
													}
												else
													{	/* SawMill/defs.sch 553 */
														BgL_res3018z00_8731 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3018z00_8731;
							}
					}
				else
					{	/* SawMill/defs.sch 553 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_fun? */
	obj_t BGl_z62rtl_funzf3z91zzsaw_defsz00(obj_t BgL_envz00_6964,
		obj_t BgL_objz00_6965)
	{
		{	/* SawMill/defs.sch 553 */
			return BBOOL(BGl_rtl_funzf3zf3zzsaw_defsz00(BgL_objz00_6965));
		}

	}



/* rtl_fun-nil */
	BGL_EXPORTED_DEF BgL_rtl_funz00_bglt BGl_rtl_funzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 554 */
			{	/* SawMill/defs.sch 554 */
				obj_t BgL_classz00_4253;

				BgL_classz00_4253 = BGl_rtl_funz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 554 */
					obj_t BgL__ortest_1117z00_4254;

					BgL__ortest_1117z00_4254 = BGL_CLASS_NIL(BgL_classz00_4253);
					if (CBOOL(BgL__ortest_1117z00_4254))
						{	/* SawMill/defs.sch 554 */
							return ((BgL_rtl_funz00_bglt) BgL__ortest_1117z00_4254);
						}
					else
						{	/* SawMill/defs.sch 554 */
							return
								((BgL_rtl_funz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4253));
						}
				}
			}
		}

	}



/* &rtl_fun-nil */
	BgL_rtl_funz00_bglt BGl_z62rtl_funzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_6966)
	{
		{	/* SawMill/defs.sch 554 */
			return BGl_rtl_funzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_fun-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_funzd2loczd2zzsaw_defsz00(BgL_rtl_funz00_bglt
		BgL_oz00_34)
	{
		{	/* SawMill/defs.sch 555 */
			return (((BgL_rtl_funz00_bglt) COBJECT(BgL_oz00_34))->BgL_locz00);
		}

	}



/* &rtl_fun-loc */
	obj_t BGl_z62rtl_funzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_6967,
		obj_t BgL_oz00_6968)
	{
		{	/* SawMill/defs.sch 555 */
			return
				BGl_rtl_funzd2loczd2zzsaw_defsz00(
				((BgL_rtl_funz00_bglt) BgL_oz00_6968));
		}

	}



/* rtl_fun-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_funzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_funz00_bglt BgL_oz00_35,
		obj_t BgL_vz00_36)
	{
		{	/* SawMill/defs.sch 556 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(BgL_oz00_35))->BgL_locz00) =
				((obj_t) BgL_vz00_36), BUNSPEC);
		}

	}



/* &rtl_fun-loc-set! */
	obj_t BGl_z62rtl_funzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_6969,
		obj_t BgL_oz00_6970, obj_t BgL_vz00_6971)
	{
		{	/* SawMill/defs.sch 556 */
			return
				BGl_rtl_funzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_funz00_bglt) BgL_oz00_6970), BgL_vz00_6971);
		}

	}



/* make-rtl_last */
	BGL_EXPORTED_DEF BgL_rtl_lastz00_bglt
		BGl_makezd2rtl_lastzd2zzsaw_defsz00(obj_t BgL_loc1501z00_37)
	{
		{	/* SawMill/defs.sch 559 */
			{	/* SawMill/defs.sch 559 */
				BgL_rtl_lastz00_bglt BgL_new1352z00_8741;

				{	/* SawMill/defs.sch 559 */
					BgL_rtl_lastz00_bglt BgL_new1351z00_8742;

					BgL_new1351z00_8742 =
						((BgL_rtl_lastz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_lastz00_bgl))));
					{	/* SawMill/defs.sch 559 */
						long BgL_arg1834z00_8743;

						BgL_arg1834z00_8743 = BGL_CLASS_NUM(BGl_rtl_lastz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1351z00_8742), BgL_arg1834z00_8743);
					}
					BgL_new1352z00_8741 = BgL_new1351z00_8742;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1352z00_8741)))->BgL_locz00) =
					((obj_t) BgL_loc1501z00_37), BUNSPEC);
				return BgL_new1352z00_8741;
			}
		}

	}



/* &make-rtl_last */
	BgL_rtl_lastz00_bglt BGl_z62makezd2rtl_lastzb0zzsaw_defsz00(obj_t
		BgL_envz00_6972, obj_t BgL_loc1501z00_6973)
	{
		{	/* SawMill/defs.sch 559 */
			return BGl_makezd2rtl_lastzd2zzsaw_defsz00(BgL_loc1501z00_6973);
		}

	}



/* rtl_last? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_lastzf3zf3zzsaw_defsz00(obj_t BgL_objz00_38)
	{
		{	/* SawMill/defs.sch 560 */
			{	/* SawMill/defs.sch 560 */
				obj_t BgL_classz00_8744;

				BgL_classz00_8744 = BGl_rtl_lastz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_38))
					{	/* SawMill/defs.sch 560 */
						BgL_objectz00_bglt BgL_arg1807z00_8745;

						BgL_arg1807z00_8745 = (BgL_objectz00_bglt) (BgL_objz00_38);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 560 */
								long BgL_idxz00_8746;

								BgL_idxz00_8746 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8745);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8746 + 2L)) == BgL_classz00_8744);
							}
						else
							{	/* SawMill/defs.sch 560 */
								bool_t BgL_res3019z00_8749;

								{	/* SawMill/defs.sch 560 */
									obj_t BgL_oclassz00_8750;

									{	/* SawMill/defs.sch 560 */
										obj_t BgL_arg1815z00_8751;
										long BgL_arg1816z00_8752;

										BgL_arg1815z00_8751 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 560 */
											long BgL_arg1817z00_8753;

											BgL_arg1817z00_8753 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8745);
											BgL_arg1816z00_8752 = (BgL_arg1817z00_8753 - OBJECT_TYPE);
										}
										BgL_oclassz00_8750 =
											VECTOR_REF(BgL_arg1815z00_8751, BgL_arg1816z00_8752);
									}
									{	/* SawMill/defs.sch 560 */
										bool_t BgL__ortest_1115z00_8754;

										BgL__ortest_1115z00_8754 =
											(BgL_classz00_8744 == BgL_oclassz00_8750);
										if (BgL__ortest_1115z00_8754)
											{	/* SawMill/defs.sch 560 */
												BgL_res3019z00_8749 = BgL__ortest_1115z00_8754;
											}
										else
											{	/* SawMill/defs.sch 560 */
												long BgL_odepthz00_8755;

												{	/* SawMill/defs.sch 560 */
													obj_t BgL_arg1804z00_8756;

													BgL_arg1804z00_8756 = (BgL_oclassz00_8750);
													BgL_odepthz00_8755 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8756);
												}
												if ((2L < BgL_odepthz00_8755))
													{	/* SawMill/defs.sch 560 */
														obj_t BgL_arg1802z00_8757;

														{	/* SawMill/defs.sch 560 */
															obj_t BgL_arg1803z00_8758;

															BgL_arg1803z00_8758 = (BgL_oclassz00_8750);
															BgL_arg1802z00_8757 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8758,
																2L);
														}
														BgL_res3019z00_8749 =
															(BgL_arg1802z00_8757 == BgL_classz00_8744);
													}
												else
													{	/* SawMill/defs.sch 560 */
														BgL_res3019z00_8749 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3019z00_8749;
							}
					}
				else
					{	/* SawMill/defs.sch 560 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_last? */
	obj_t BGl_z62rtl_lastzf3z91zzsaw_defsz00(obj_t BgL_envz00_6974,
		obj_t BgL_objz00_6975)
	{
		{	/* SawMill/defs.sch 560 */
			return BBOOL(BGl_rtl_lastzf3zf3zzsaw_defsz00(BgL_objz00_6975));
		}

	}



/* rtl_last-nil */
	BGL_EXPORTED_DEF BgL_rtl_lastz00_bglt BGl_rtl_lastzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 561 */
			{	/* SawMill/defs.sch 561 */
				obj_t BgL_classz00_4295;

				BgL_classz00_4295 = BGl_rtl_lastz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 561 */
					obj_t BgL__ortest_1117z00_4296;

					BgL__ortest_1117z00_4296 = BGL_CLASS_NIL(BgL_classz00_4295);
					if (CBOOL(BgL__ortest_1117z00_4296))
						{	/* SawMill/defs.sch 561 */
							return ((BgL_rtl_lastz00_bglt) BgL__ortest_1117z00_4296);
						}
					else
						{	/* SawMill/defs.sch 561 */
							return
								((BgL_rtl_lastz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4295));
						}
				}
			}
		}

	}



/* &rtl_last-nil */
	BgL_rtl_lastz00_bglt BGl_z62rtl_lastzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_6976)
	{
		{	/* SawMill/defs.sch 561 */
			return BGl_rtl_lastzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_last-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_lastzd2loczd2zzsaw_defsz00(BgL_rtl_lastz00_bglt
		BgL_oz00_39)
	{
		{	/* SawMill/defs.sch 562 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_39)))->BgL_locz00);
		}

	}



/* &rtl_last-loc */
	obj_t BGl_z62rtl_lastzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_6977,
		obj_t BgL_oz00_6978)
	{
		{	/* SawMill/defs.sch 562 */
			return
				BGl_rtl_lastzd2loczd2zzsaw_defsz00(
				((BgL_rtl_lastz00_bglt) BgL_oz00_6978));
		}

	}



/* rtl_last-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_lastzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_lastz00_bglt
		BgL_oz00_40, obj_t BgL_vz00_41)
	{
		{	/* SawMill/defs.sch 563 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_40)))->BgL_locz00) =
				((obj_t) BgL_vz00_41), BUNSPEC);
		}

	}



/* &rtl_last-loc-set! */
	obj_t BGl_z62rtl_lastzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_6979,
		obj_t BgL_oz00_6980, obj_t BgL_vz00_6981)
	{
		{	/* SawMill/defs.sch 563 */
			return
				BGl_rtl_lastzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_lastz00_bglt) BgL_oz00_6980), BgL_vz00_6981);
		}

	}



/* make-rtl_return */
	BGL_EXPORTED_DEF BgL_rtl_returnz00_bglt
		BGl_makezd2rtl_returnzd2zzsaw_defsz00(obj_t BgL_loc1498z00_42,
		BgL_typez00_bglt BgL_type1499z00_43)
	{
		{	/* SawMill/defs.sch 566 */
			{	/* SawMill/defs.sch 566 */
				BgL_rtl_returnz00_bglt BgL_new1354z00_8759;

				{	/* SawMill/defs.sch 566 */
					BgL_rtl_returnz00_bglt BgL_new1353z00_8760;

					BgL_new1353z00_8760 =
						((BgL_rtl_returnz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_returnz00_bgl))));
					{	/* SawMill/defs.sch 566 */
						long BgL_arg1835z00_8761;

						BgL_arg1835z00_8761 = BGL_CLASS_NUM(BGl_rtl_returnz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1353z00_8760), BgL_arg1835z00_8761);
					}
					BgL_new1354z00_8759 = BgL_new1353z00_8760;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1354z00_8759)))->BgL_locz00) =
					((obj_t) BgL_loc1498z00_42), BUNSPEC);
				((((BgL_rtl_returnz00_bglt) COBJECT(BgL_new1354z00_8759))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1499z00_43), BUNSPEC);
				return BgL_new1354z00_8759;
			}
		}

	}



/* &make-rtl_return */
	BgL_rtl_returnz00_bglt BGl_z62makezd2rtl_returnzb0zzsaw_defsz00(obj_t
		BgL_envz00_6982, obj_t BgL_loc1498z00_6983, obj_t BgL_type1499z00_6984)
	{
		{	/* SawMill/defs.sch 566 */
			return
				BGl_makezd2rtl_returnzd2zzsaw_defsz00(BgL_loc1498z00_6983,
				((BgL_typez00_bglt) BgL_type1499z00_6984));
		}

	}



/* rtl_return? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_returnzf3zf3zzsaw_defsz00(obj_t BgL_objz00_44)
	{
		{	/* SawMill/defs.sch 567 */
			{	/* SawMill/defs.sch 567 */
				obj_t BgL_classz00_8762;

				BgL_classz00_8762 = BGl_rtl_returnz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_44))
					{	/* SawMill/defs.sch 567 */
						BgL_objectz00_bglt BgL_arg1807z00_8763;

						BgL_arg1807z00_8763 = (BgL_objectz00_bglt) (BgL_objz00_44);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 567 */
								long BgL_idxz00_8764;

								BgL_idxz00_8764 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8763);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8764 + 3L)) == BgL_classz00_8762);
							}
						else
							{	/* SawMill/defs.sch 567 */
								bool_t BgL_res3020z00_8767;

								{	/* SawMill/defs.sch 567 */
									obj_t BgL_oclassz00_8768;

									{	/* SawMill/defs.sch 567 */
										obj_t BgL_arg1815z00_8769;
										long BgL_arg1816z00_8770;

										BgL_arg1815z00_8769 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 567 */
											long BgL_arg1817z00_8771;

											BgL_arg1817z00_8771 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8763);
											BgL_arg1816z00_8770 = (BgL_arg1817z00_8771 - OBJECT_TYPE);
										}
										BgL_oclassz00_8768 =
											VECTOR_REF(BgL_arg1815z00_8769, BgL_arg1816z00_8770);
									}
									{	/* SawMill/defs.sch 567 */
										bool_t BgL__ortest_1115z00_8772;

										BgL__ortest_1115z00_8772 =
											(BgL_classz00_8762 == BgL_oclassz00_8768);
										if (BgL__ortest_1115z00_8772)
											{	/* SawMill/defs.sch 567 */
												BgL_res3020z00_8767 = BgL__ortest_1115z00_8772;
											}
										else
											{	/* SawMill/defs.sch 567 */
												long BgL_odepthz00_8773;

												{	/* SawMill/defs.sch 567 */
													obj_t BgL_arg1804z00_8774;

													BgL_arg1804z00_8774 = (BgL_oclassz00_8768);
													BgL_odepthz00_8773 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8774);
												}
												if ((3L < BgL_odepthz00_8773))
													{	/* SawMill/defs.sch 567 */
														obj_t BgL_arg1802z00_8775;

														{	/* SawMill/defs.sch 567 */
															obj_t BgL_arg1803z00_8776;

															BgL_arg1803z00_8776 = (BgL_oclassz00_8768);
															BgL_arg1802z00_8775 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8776,
																3L);
														}
														BgL_res3020z00_8767 =
															(BgL_arg1802z00_8775 == BgL_classz00_8762);
													}
												else
													{	/* SawMill/defs.sch 567 */
														BgL_res3020z00_8767 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3020z00_8767;
							}
					}
				else
					{	/* SawMill/defs.sch 567 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_return? */
	obj_t BGl_z62rtl_returnzf3z91zzsaw_defsz00(obj_t BgL_envz00_6985,
		obj_t BgL_objz00_6986)
	{
		{	/* SawMill/defs.sch 567 */
			return BBOOL(BGl_rtl_returnzf3zf3zzsaw_defsz00(BgL_objz00_6986));
		}

	}



/* rtl_return-nil */
	BGL_EXPORTED_DEF BgL_rtl_returnz00_bglt
		BGl_rtl_returnzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 568 */
			{	/* SawMill/defs.sch 568 */
				obj_t BgL_classz00_4337;

				BgL_classz00_4337 = BGl_rtl_returnz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 568 */
					obj_t BgL__ortest_1117z00_4338;

					BgL__ortest_1117z00_4338 = BGL_CLASS_NIL(BgL_classz00_4337);
					if (CBOOL(BgL__ortest_1117z00_4338))
						{	/* SawMill/defs.sch 568 */
							return ((BgL_rtl_returnz00_bglt) BgL__ortest_1117z00_4338);
						}
					else
						{	/* SawMill/defs.sch 568 */
							return
								((BgL_rtl_returnz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4337));
						}
				}
			}
		}

	}



/* &rtl_return-nil */
	BgL_rtl_returnz00_bglt BGl_z62rtl_returnzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_6987)
	{
		{	/* SawMill/defs.sch 568 */
			return BGl_rtl_returnzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_return-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_returnzd2typezd2zzsaw_defsz00(BgL_rtl_returnz00_bglt BgL_oz00_45)
	{
		{	/* SawMill/defs.sch 569 */
			return (((BgL_rtl_returnz00_bglt) COBJECT(BgL_oz00_45))->BgL_typez00);
		}

	}



/* &rtl_return-type */
	BgL_typez00_bglt BGl_z62rtl_returnzd2typezb0zzsaw_defsz00(obj_t
		BgL_envz00_6988, obj_t BgL_oz00_6989)
	{
		{	/* SawMill/defs.sch 569 */
			return
				BGl_rtl_returnzd2typezd2zzsaw_defsz00(
				((BgL_rtl_returnz00_bglt) BgL_oz00_6989));
		}

	}



/* rtl_return-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_returnzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_returnz00_bglt
		BgL_oz00_46, BgL_typez00_bglt BgL_vz00_47)
	{
		{	/* SawMill/defs.sch 570 */
			return
				((((BgL_rtl_returnz00_bglt) COBJECT(BgL_oz00_46))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_47), BUNSPEC);
		}

	}



/* &rtl_return-type-set! */
	obj_t BGl_z62rtl_returnzd2typezd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_6990,
		obj_t BgL_oz00_6991, obj_t BgL_vz00_6992)
	{
		{	/* SawMill/defs.sch 570 */
			return
				BGl_rtl_returnzd2typezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_returnz00_bglt) BgL_oz00_6991),
				((BgL_typez00_bglt) BgL_vz00_6992));
		}

	}



/* rtl_return-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_returnzd2loczd2zzsaw_defsz00(BgL_rtl_returnz00_bglt BgL_oz00_48)
	{
		{	/* SawMill/defs.sch 571 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_48)))->BgL_locz00);
		}

	}



/* &rtl_return-loc */
	obj_t BGl_z62rtl_returnzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_6993,
		obj_t BgL_oz00_6994)
	{
		{	/* SawMill/defs.sch 571 */
			return
				BGl_rtl_returnzd2loczd2zzsaw_defsz00(
				((BgL_rtl_returnz00_bglt) BgL_oz00_6994));
		}

	}



/* rtl_return-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_returnzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_returnz00_bglt
		BgL_oz00_49, obj_t BgL_vz00_50)
	{
		{	/* SawMill/defs.sch 572 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_49)))->BgL_locz00) =
				((obj_t) BgL_vz00_50), BUNSPEC);
		}

	}



/* &rtl_return-loc-set! */
	obj_t BGl_z62rtl_returnzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_6995,
		obj_t BgL_oz00_6996, obj_t BgL_vz00_6997)
	{
		{	/* SawMill/defs.sch 572 */
			return
				BGl_rtl_returnzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_returnz00_bglt) BgL_oz00_6996), BgL_vz00_6997);
		}

	}



/* make-rtl_jumpexit */
	BGL_EXPORTED_DEF BgL_rtl_jumpexitz00_bglt
		BGl_makezd2rtl_jumpexitzd2zzsaw_defsz00(obj_t BgL_loc1496z00_51)
	{
		{	/* SawMill/defs.sch 575 */
			{	/* SawMill/defs.sch 575 */
				BgL_rtl_jumpexitz00_bglt BgL_new1356z00_8777;

				{	/* SawMill/defs.sch 575 */
					BgL_rtl_jumpexitz00_bglt BgL_new1355z00_8778;

					BgL_new1355z00_8778 =
						((BgL_rtl_jumpexitz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_jumpexitz00_bgl))));
					{	/* SawMill/defs.sch 575 */
						long BgL_arg1836z00_8779;

						BgL_arg1836z00_8779 =
							BGL_CLASS_NUM(BGl_rtl_jumpexitz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1355z00_8778), BgL_arg1836z00_8779);
					}
					BgL_new1356z00_8777 = BgL_new1355z00_8778;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1356z00_8777)))->BgL_locz00) =
					((obj_t) BgL_loc1496z00_51), BUNSPEC);
				return BgL_new1356z00_8777;
			}
		}

	}



/* &make-rtl_jumpexit */
	BgL_rtl_jumpexitz00_bglt BGl_z62makezd2rtl_jumpexitzb0zzsaw_defsz00(obj_t
		BgL_envz00_6998, obj_t BgL_loc1496z00_6999)
	{
		{	/* SawMill/defs.sch 575 */
			return BGl_makezd2rtl_jumpexitzd2zzsaw_defsz00(BgL_loc1496z00_6999);
		}

	}



/* rtl_jumpexit? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_jumpexitzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_52)
	{
		{	/* SawMill/defs.sch 576 */
			{	/* SawMill/defs.sch 576 */
				obj_t BgL_classz00_8780;

				BgL_classz00_8780 = BGl_rtl_jumpexitz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_52))
					{	/* SawMill/defs.sch 576 */
						BgL_objectz00_bglt BgL_arg1807z00_8781;

						BgL_arg1807z00_8781 = (BgL_objectz00_bglt) (BgL_objz00_52);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 576 */
								long BgL_idxz00_8782;

								BgL_idxz00_8782 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8781);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8782 + 3L)) == BgL_classz00_8780);
							}
						else
							{	/* SawMill/defs.sch 576 */
								bool_t BgL_res3021z00_8785;

								{	/* SawMill/defs.sch 576 */
									obj_t BgL_oclassz00_8786;

									{	/* SawMill/defs.sch 576 */
										obj_t BgL_arg1815z00_8787;
										long BgL_arg1816z00_8788;

										BgL_arg1815z00_8787 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 576 */
											long BgL_arg1817z00_8789;

											BgL_arg1817z00_8789 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8781);
											BgL_arg1816z00_8788 = (BgL_arg1817z00_8789 - OBJECT_TYPE);
										}
										BgL_oclassz00_8786 =
											VECTOR_REF(BgL_arg1815z00_8787, BgL_arg1816z00_8788);
									}
									{	/* SawMill/defs.sch 576 */
										bool_t BgL__ortest_1115z00_8790;

										BgL__ortest_1115z00_8790 =
											(BgL_classz00_8780 == BgL_oclassz00_8786);
										if (BgL__ortest_1115z00_8790)
											{	/* SawMill/defs.sch 576 */
												BgL_res3021z00_8785 = BgL__ortest_1115z00_8790;
											}
										else
											{	/* SawMill/defs.sch 576 */
												long BgL_odepthz00_8791;

												{	/* SawMill/defs.sch 576 */
													obj_t BgL_arg1804z00_8792;

													BgL_arg1804z00_8792 = (BgL_oclassz00_8786);
													BgL_odepthz00_8791 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8792);
												}
												if ((3L < BgL_odepthz00_8791))
													{	/* SawMill/defs.sch 576 */
														obj_t BgL_arg1802z00_8793;

														{	/* SawMill/defs.sch 576 */
															obj_t BgL_arg1803z00_8794;

															BgL_arg1803z00_8794 = (BgL_oclassz00_8786);
															BgL_arg1802z00_8793 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8794,
																3L);
														}
														BgL_res3021z00_8785 =
															(BgL_arg1802z00_8793 == BgL_classz00_8780);
													}
												else
													{	/* SawMill/defs.sch 576 */
														BgL_res3021z00_8785 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3021z00_8785;
							}
					}
				else
					{	/* SawMill/defs.sch 576 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_jumpexit? */
	obj_t BGl_z62rtl_jumpexitzf3z91zzsaw_defsz00(obj_t BgL_envz00_7000,
		obj_t BgL_objz00_7001)
	{
		{	/* SawMill/defs.sch 576 */
			return BBOOL(BGl_rtl_jumpexitzf3zf3zzsaw_defsz00(BgL_objz00_7001));
		}

	}



/* rtl_jumpexit-nil */
	BGL_EXPORTED_DEF BgL_rtl_jumpexitz00_bglt
		BGl_rtl_jumpexitzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 577 */
			{	/* SawMill/defs.sch 577 */
				obj_t BgL_classz00_4379;

				BgL_classz00_4379 = BGl_rtl_jumpexitz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 577 */
					obj_t BgL__ortest_1117z00_4380;

					BgL__ortest_1117z00_4380 = BGL_CLASS_NIL(BgL_classz00_4379);
					if (CBOOL(BgL__ortest_1117z00_4380))
						{	/* SawMill/defs.sch 577 */
							return ((BgL_rtl_jumpexitz00_bglt) BgL__ortest_1117z00_4380);
						}
					else
						{	/* SawMill/defs.sch 577 */
							return
								((BgL_rtl_jumpexitz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4379));
						}
				}
			}
		}

	}



/* &rtl_jumpexit-nil */
	BgL_rtl_jumpexitz00_bglt BGl_z62rtl_jumpexitzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7002)
	{
		{	/* SawMill/defs.sch 577 */
			return BGl_rtl_jumpexitzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_jumpexit-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_jumpexitzd2loczd2zzsaw_defsz00(BgL_rtl_jumpexitz00_bglt BgL_oz00_53)
	{
		{	/* SawMill/defs.sch 578 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_53)))->BgL_locz00);
		}

	}



/* &rtl_jumpexit-loc */
	obj_t BGl_z62rtl_jumpexitzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7003,
		obj_t BgL_oz00_7004)
	{
		{	/* SawMill/defs.sch 578 */
			return
				BGl_rtl_jumpexitzd2loczd2zzsaw_defsz00(
				((BgL_rtl_jumpexitz00_bglt) BgL_oz00_7004));
		}

	}



/* rtl_jumpexit-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_jumpexitzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_jumpexitz00_bglt
		BgL_oz00_54, obj_t BgL_vz00_55)
	{
		{	/* SawMill/defs.sch 579 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_54)))->BgL_locz00) =
				((obj_t) BgL_vz00_55), BUNSPEC);
		}

	}



/* &rtl_jumpexit-loc-set! */
	obj_t BGl_z62rtl_jumpexitzd2loczd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7005, obj_t BgL_oz00_7006, obj_t BgL_vz00_7007)
	{
		{	/* SawMill/defs.sch 579 */
			return
				BGl_rtl_jumpexitzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_jumpexitz00_bglt) BgL_oz00_7006), BgL_vz00_7007);
		}

	}



/* make-rtl_fail */
	BGL_EXPORTED_DEF BgL_rtl_failz00_bglt
		BGl_makezd2rtl_failzd2zzsaw_defsz00(obj_t BgL_loc1494z00_56)
	{
		{	/* SawMill/defs.sch 582 */
			{	/* SawMill/defs.sch 582 */
				BgL_rtl_failz00_bglt BgL_new1358z00_8795;

				{	/* SawMill/defs.sch 582 */
					BgL_rtl_failz00_bglt BgL_new1357z00_8796;

					BgL_new1357z00_8796 =
						((BgL_rtl_failz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_failz00_bgl))));
					{	/* SawMill/defs.sch 582 */
						long BgL_arg1837z00_8797;

						BgL_arg1837z00_8797 = BGL_CLASS_NUM(BGl_rtl_failz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1357z00_8796), BgL_arg1837z00_8797);
					}
					BgL_new1358z00_8795 = BgL_new1357z00_8796;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1358z00_8795)))->BgL_locz00) =
					((obj_t) BgL_loc1494z00_56), BUNSPEC);
				return BgL_new1358z00_8795;
			}
		}

	}



/* &make-rtl_fail */
	BgL_rtl_failz00_bglt BGl_z62makezd2rtl_failzb0zzsaw_defsz00(obj_t
		BgL_envz00_7008, obj_t BgL_loc1494z00_7009)
	{
		{	/* SawMill/defs.sch 582 */
			return BGl_makezd2rtl_failzd2zzsaw_defsz00(BgL_loc1494z00_7009);
		}

	}



/* rtl_fail? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_failzf3zf3zzsaw_defsz00(obj_t BgL_objz00_57)
	{
		{	/* SawMill/defs.sch 583 */
			{	/* SawMill/defs.sch 583 */
				obj_t BgL_classz00_8798;

				BgL_classz00_8798 = BGl_rtl_failz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_57))
					{	/* SawMill/defs.sch 583 */
						BgL_objectz00_bglt BgL_arg1807z00_8799;

						BgL_arg1807z00_8799 = (BgL_objectz00_bglt) (BgL_objz00_57);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 583 */
								long BgL_idxz00_8800;

								BgL_idxz00_8800 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8799);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8800 + 3L)) == BgL_classz00_8798);
							}
						else
							{	/* SawMill/defs.sch 583 */
								bool_t BgL_res3022z00_8803;

								{	/* SawMill/defs.sch 583 */
									obj_t BgL_oclassz00_8804;

									{	/* SawMill/defs.sch 583 */
										obj_t BgL_arg1815z00_8805;
										long BgL_arg1816z00_8806;

										BgL_arg1815z00_8805 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 583 */
											long BgL_arg1817z00_8807;

											BgL_arg1817z00_8807 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8799);
											BgL_arg1816z00_8806 = (BgL_arg1817z00_8807 - OBJECT_TYPE);
										}
										BgL_oclassz00_8804 =
											VECTOR_REF(BgL_arg1815z00_8805, BgL_arg1816z00_8806);
									}
									{	/* SawMill/defs.sch 583 */
										bool_t BgL__ortest_1115z00_8808;

										BgL__ortest_1115z00_8808 =
											(BgL_classz00_8798 == BgL_oclassz00_8804);
										if (BgL__ortest_1115z00_8808)
											{	/* SawMill/defs.sch 583 */
												BgL_res3022z00_8803 = BgL__ortest_1115z00_8808;
											}
										else
											{	/* SawMill/defs.sch 583 */
												long BgL_odepthz00_8809;

												{	/* SawMill/defs.sch 583 */
													obj_t BgL_arg1804z00_8810;

													BgL_arg1804z00_8810 = (BgL_oclassz00_8804);
													BgL_odepthz00_8809 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8810);
												}
												if ((3L < BgL_odepthz00_8809))
													{	/* SawMill/defs.sch 583 */
														obj_t BgL_arg1802z00_8811;

														{	/* SawMill/defs.sch 583 */
															obj_t BgL_arg1803z00_8812;

															BgL_arg1803z00_8812 = (BgL_oclassz00_8804);
															BgL_arg1802z00_8811 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8812,
																3L);
														}
														BgL_res3022z00_8803 =
															(BgL_arg1802z00_8811 == BgL_classz00_8798);
													}
												else
													{	/* SawMill/defs.sch 583 */
														BgL_res3022z00_8803 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3022z00_8803;
							}
					}
				else
					{	/* SawMill/defs.sch 583 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_fail? */
	obj_t BGl_z62rtl_failzf3z91zzsaw_defsz00(obj_t BgL_envz00_7010,
		obj_t BgL_objz00_7011)
	{
		{	/* SawMill/defs.sch 583 */
			return BBOOL(BGl_rtl_failzf3zf3zzsaw_defsz00(BgL_objz00_7011));
		}

	}



/* rtl_fail-nil */
	BGL_EXPORTED_DEF BgL_rtl_failz00_bglt BGl_rtl_failzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 584 */
			{	/* SawMill/defs.sch 584 */
				obj_t BgL_classz00_4421;

				BgL_classz00_4421 = BGl_rtl_failz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 584 */
					obj_t BgL__ortest_1117z00_4422;

					BgL__ortest_1117z00_4422 = BGL_CLASS_NIL(BgL_classz00_4421);
					if (CBOOL(BgL__ortest_1117z00_4422))
						{	/* SawMill/defs.sch 584 */
							return ((BgL_rtl_failz00_bglt) BgL__ortest_1117z00_4422);
						}
					else
						{	/* SawMill/defs.sch 584 */
							return
								((BgL_rtl_failz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4421));
						}
				}
			}
		}

	}



/* &rtl_fail-nil */
	BgL_rtl_failz00_bglt BGl_z62rtl_failzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7012)
	{
		{	/* SawMill/defs.sch 584 */
			return BGl_rtl_failzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_fail-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_failzd2loczd2zzsaw_defsz00(BgL_rtl_failz00_bglt
		BgL_oz00_58)
	{
		{	/* SawMill/defs.sch 585 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_58)))->BgL_locz00);
		}

	}



/* &rtl_fail-loc */
	obj_t BGl_z62rtl_failzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7013,
		obj_t BgL_oz00_7014)
	{
		{	/* SawMill/defs.sch 585 */
			return
				BGl_rtl_failzd2loczd2zzsaw_defsz00(
				((BgL_rtl_failz00_bglt) BgL_oz00_7014));
		}

	}



/* rtl_fail-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_failzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_failz00_bglt
		BgL_oz00_59, obj_t BgL_vz00_60)
	{
		{	/* SawMill/defs.sch 586 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_59)))->BgL_locz00) =
				((obj_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &rtl_fail-loc-set! */
	obj_t BGl_z62rtl_failzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7015,
		obj_t BgL_oz00_7016, obj_t BgL_vz00_7017)
	{
		{	/* SawMill/defs.sch 586 */
			return
				BGl_rtl_failzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_failz00_bglt) BgL_oz00_7016), BgL_vz00_7017);
		}

	}



/* make-rtl_retblock */
	BGL_EXPORTED_DEF BgL_rtl_retblockz00_bglt
		BGl_makezd2rtl_retblockzd2zzsaw_defsz00(obj_t BgL_loc1492z00_61)
	{
		{	/* SawMill/defs.sch 589 */
			{	/* SawMill/defs.sch 589 */
				BgL_rtl_retblockz00_bglt BgL_new1360z00_8813;

				{	/* SawMill/defs.sch 589 */
					BgL_rtl_retblockz00_bglt BgL_new1359z00_8814;

					BgL_new1359z00_8814 =
						((BgL_rtl_retblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_retblockz00_bgl))));
					{	/* SawMill/defs.sch 589 */
						long BgL_arg1838z00_8815;

						BgL_arg1838z00_8815 =
							BGL_CLASS_NUM(BGl_rtl_retblockz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1359z00_8814), BgL_arg1838z00_8815);
					}
					BgL_new1360z00_8813 = BgL_new1359z00_8814;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1360z00_8813)))->BgL_locz00) =
					((obj_t) BgL_loc1492z00_61), BUNSPEC);
				return BgL_new1360z00_8813;
			}
		}

	}



/* &make-rtl_retblock */
	BgL_rtl_retblockz00_bglt BGl_z62makezd2rtl_retblockzb0zzsaw_defsz00(obj_t
		BgL_envz00_7018, obj_t BgL_loc1492z00_7019)
	{
		{	/* SawMill/defs.sch 589 */
			return BGl_makezd2rtl_retblockzd2zzsaw_defsz00(BgL_loc1492z00_7019);
		}

	}



/* rtl_retblock? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_retblockzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_62)
	{
		{	/* SawMill/defs.sch 590 */
			{	/* SawMill/defs.sch 590 */
				obj_t BgL_classz00_8816;

				BgL_classz00_8816 = BGl_rtl_retblockz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_62))
					{	/* SawMill/defs.sch 590 */
						BgL_objectz00_bglt BgL_arg1807z00_8817;

						BgL_arg1807z00_8817 = (BgL_objectz00_bglt) (BgL_objz00_62);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 590 */
								long BgL_idxz00_8818;

								BgL_idxz00_8818 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8817);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8818 + 2L)) == BgL_classz00_8816);
							}
						else
							{	/* SawMill/defs.sch 590 */
								bool_t BgL_res3023z00_8821;

								{	/* SawMill/defs.sch 590 */
									obj_t BgL_oclassz00_8822;

									{	/* SawMill/defs.sch 590 */
										obj_t BgL_arg1815z00_8823;
										long BgL_arg1816z00_8824;

										BgL_arg1815z00_8823 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 590 */
											long BgL_arg1817z00_8825;

											BgL_arg1817z00_8825 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8817);
											BgL_arg1816z00_8824 = (BgL_arg1817z00_8825 - OBJECT_TYPE);
										}
										BgL_oclassz00_8822 =
											VECTOR_REF(BgL_arg1815z00_8823, BgL_arg1816z00_8824);
									}
									{	/* SawMill/defs.sch 590 */
										bool_t BgL__ortest_1115z00_8826;

										BgL__ortest_1115z00_8826 =
											(BgL_classz00_8816 == BgL_oclassz00_8822);
										if (BgL__ortest_1115z00_8826)
											{	/* SawMill/defs.sch 590 */
												BgL_res3023z00_8821 = BgL__ortest_1115z00_8826;
											}
										else
											{	/* SawMill/defs.sch 590 */
												long BgL_odepthz00_8827;

												{	/* SawMill/defs.sch 590 */
													obj_t BgL_arg1804z00_8828;

													BgL_arg1804z00_8828 = (BgL_oclassz00_8822);
													BgL_odepthz00_8827 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8828);
												}
												if ((2L < BgL_odepthz00_8827))
													{	/* SawMill/defs.sch 590 */
														obj_t BgL_arg1802z00_8829;

														{	/* SawMill/defs.sch 590 */
															obj_t BgL_arg1803z00_8830;

															BgL_arg1803z00_8830 = (BgL_oclassz00_8822);
															BgL_arg1802z00_8829 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8830,
																2L);
														}
														BgL_res3023z00_8821 =
															(BgL_arg1802z00_8829 == BgL_classz00_8816);
													}
												else
													{	/* SawMill/defs.sch 590 */
														BgL_res3023z00_8821 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3023z00_8821;
							}
					}
				else
					{	/* SawMill/defs.sch 590 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_retblock? */
	obj_t BGl_z62rtl_retblockzf3z91zzsaw_defsz00(obj_t BgL_envz00_7020,
		obj_t BgL_objz00_7021)
	{
		{	/* SawMill/defs.sch 590 */
			return BBOOL(BGl_rtl_retblockzf3zf3zzsaw_defsz00(BgL_objz00_7021));
		}

	}



/* rtl_retblock-nil */
	BGL_EXPORTED_DEF BgL_rtl_retblockz00_bglt
		BGl_rtl_retblockzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 591 */
			{	/* SawMill/defs.sch 591 */
				obj_t BgL_classz00_4463;

				BgL_classz00_4463 = BGl_rtl_retblockz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 591 */
					obj_t BgL__ortest_1117z00_4464;

					BgL__ortest_1117z00_4464 = BGL_CLASS_NIL(BgL_classz00_4463);
					if (CBOOL(BgL__ortest_1117z00_4464))
						{	/* SawMill/defs.sch 591 */
							return ((BgL_rtl_retblockz00_bglt) BgL__ortest_1117z00_4464);
						}
					else
						{	/* SawMill/defs.sch 591 */
							return
								((BgL_rtl_retblockz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4463));
						}
				}
			}
		}

	}



/* &rtl_retblock-nil */
	BgL_rtl_retblockz00_bglt BGl_z62rtl_retblockzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7022)
	{
		{	/* SawMill/defs.sch 591 */
			return BGl_rtl_retblockzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_retblock-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_retblockzd2loczd2zzsaw_defsz00(BgL_rtl_retblockz00_bglt BgL_oz00_63)
	{
		{	/* SawMill/defs.sch 592 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_63)))->BgL_locz00);
		}

	}



/* &rtl_retblock-loc */
	obj_t BGl_z62rtl_retblockzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7023,
		obj_t BgL_oz00_7024)
	{
		{	/* SawMill/defs.sch 592 */
			return
				BGl_rtl_retblockzd2loczd2zzsaw_defsz00(
				((BgL_rtl_retblockz00_bglt) BgL_oz00_7024));
		}

	}



/* rtl_retblock-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_retblockzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_retblockz00_bglt
		BgL_oz00_64, obj_t BgL_vz00_65)
	{
		{	/* SawMill/defs.sch 593 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_64)))->BgL_locz00) =
				((obj_t) BgL_vz00_65), BUNSPEC);
		}

	}



/* &rtl_retblock-loc-set! */
	obj_t BGl_z62rtl_retblockzd2loczd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7025, obj_t BgL_oz00_7026, obj_t BgL_vz00_7027)
	{
		{	/* SawMill/defs.sch 593 */
			return
				BGl_rtl_retblockzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_retblockz00_bglt) BgL_oz00_7026), BgL_vz00_7027);
		}

	}



/* make-rtl_ret */
	BGL_EXPORTED_DEF BgL_rtl_retz00_bglt BGl_makezd2rtl_retzd2zzsaw_defsz00(obj_t
		BgL_loc1488z00_66, BgL_blockz00_bglt BgL_to1489z00_67)
	{
		{	/* SawMill/defs.sch 596 */
			{	/* SawMill/defs.sch 596 */
				BgL_rtl_retz00_bglt BgL_new1362z00_8831;

				{	/* SawMill/defs.sch 596 */
					BgL_rtl_retz00_bglt BgL_new1361z00_8832;

					BgL_new1361z00_8832 =
						((BgL_rtl_retz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_retz00_bgl))));
					{	/* SawMill/defs.sch 596 */
						long BgL_arg1839z00_8833;

						BgL_arg1839z00_8833 = BGL_CLASS_NUM(BGl_rtl_retz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1361z00_8832), BgL_arg1839z00_8833);
					}
					BgL_new1362z00_8831 = BgL_new1361z00_8832;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1362z00_8831)))->BgL_locz00) =
					((obj_t) BgL_loc1488z00_66), BUNSPEC);
				((((BgL_rtl_retz00_bglt) COBJECT(BgL_new1362z00_8831))->BgL_toz00) =
					((BgL_blockz00_bglt) BgL_to1489z00_67), BUNSPEC);
				return BgL_new1362z00_8831;
			}
		}

	}



/* &make-rtl_ret */
	BgL_rtl_retz00_bglt BGl_z62makezd2rtl_retzb0zzsaw_defsz00(obj_t
		BgL_envz00_7028, obj_t BgL_loc1488z00_7029, obj_t BgL_to1489z00_7030)
	{
		{	/* SawMill/defs.sch 596 */
			return
				BGl_makezd2rtl_retzd2zzsaw_defsz00(BgL_loc1488z00_7029,
				((BgL_blockz00_bglt) BgL_to1489z00_7030));
		}

	}



/* rtl_ret? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_retzf3zf3zzsaw_defsz00(obj_t BgL_objz00_68)
	{
		{	/* SawMill/defs.sch 597 */
			{	/* SawMill/defs.sch 597 */
				obj_t BgL_classz00_8834;

				BgL_classz00_8834 = BGl_rtl_retz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_68))
					{	/* SawMill/defs.sch 597 */
						BgL_objectz00_bglt BgL_arg1807z00_8835;

						BgL_arg1807z00_8835 = (BgL_objectz00_bglt) (BgL_objz00_68);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 597 */
								long BgL_idxz00_8836;

								BgL_idxz00_8836 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8835);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8836 + 3L)) == BgL_classz00_8834);
							}
						else
							{	/* SawMill/defs.sch 597 */
								bool_t BgL_res3024z00_8839;

								{	/* SawMill/defs.sch 597 */
									obj_t BgL_oclassz00_8840;

									{	/* SawMill/defs.sch 597 */
										obj_t BgL_arg1815z00_8841;
										long BgL_arg1816z00_8842;

										BgL_arg1815z00_8841 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 597 */
											long BgL_arg1817z00_8843;

											BgL_arg1817z00_8843 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8835);
											BgL_arg1816z00_8842 = (BgL_arg1817z00_8843 - OBJECT_TYPE);
										}
										BgL_oclassz00_8840 =
											VECTOR_REF(BgL_arg1815z00_8841, BgL_arg1816z00_8842);
									}
									{	/* SawMill/defs.sch 597 */
										bool_t BgL__ortest_1115z00_8844;

										BgL__ortest_1115z00_8844 =
											(BgL_classz00_8834 == BgL_oclassz00_8840);
										if (BgL__ortest_1115z00_8844)
											{	/* SawMill/defs.sch 597 */
												BgL_res3024z00_8839 = BgL__ortest_1115z00_8844;
											}
										else
											{	/* SawMill/defs.sch 597 */
												long BgL_odepthz00_8845;

												{	/* SawMill/defs.sch 597 */
													obj_t BgL_arg1804z00_8846;

													BgL_arg1804z00_8846 = (BgL_oclassz00_8840);
													BgL_odepthz00_8845 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8846);
												}
												if ((3L < BgL_odepthz00_8845))
													{	/* SawMill/defs.sch 597 */
														obj_t BgL_arg1802z00_8847;

														{	/* SawMill/defs.sch 597 */
															obj_t BgL_arg1803z00_8848;

															BgL_arg1803z00_8848 = (BgL_oclassz00_8840);
															BgL_arg1802z00_8847 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8848,
																3L);
														}
														BgL_res3024z00_8839 =
															(BgL_arg1802z00_8847 == BgL_classz00_8834);
													}
												else
													{	/* SawMill/defs.sch 597 */
														BgL_res3024z00_8839 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3024z00_8839;
							}
					}
				else
					{	/* SawMill/defs.sch 597 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_ret? */
	obj_t BGl_z62rtl_retzf3z91zzsaw_defsz00(obj_t BgL_envz00_7031,
		obj_t BgL_objz00_7032)
	{
		{	/* SawMill/defs.sch 597 */
			return BBOOL(BGl_rtl_retzf3zf3zzsaw_defsz00(BgL_objz00_7032));
		}

	}



/* rtl_ret-nil */
	BGL_EXPORTED_DEF BgL_rtl_retz00_bglt BGl_rtl_retzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 598 */
			{	/* SawMill/defs.sch 598 */
				obj_t BgL_classz00_4505;

				BgL_classz00_4505 = BGl_rtl_retz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 598 */
					obj_t BgL__ortest_1117z00_4506;

					BgL__ortest_1117z00_4506 = BGL_CLASS_NIL(BgL_classz00_4505);
					if (CBOOL(BgL__ortest_1117z00_4506))
						{	/* SawMill/defs.sch 598 */
							return ((BgL_rtl_retz00_bglt) BgL__ortest_1117z00_4506);
						}
					else
						{	/* SawMill/defs.sch 598 */
							return
								((BgL_rtl_retz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4505));
						}
				}
			}
		}

	}



/* &rtl_ret-nil */
	BgL_rtl_retz00_bglt BGl_z62rtl_retzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7033)
	{
		{	/* SawMill/defs.sch 598 */
			return BGl_rtl_retzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_ret-to */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_rtl_retzd2tozd2zzsaw_defsz00(BgL_rtl_retz00_bglt BgL_oz00_69)
	{
		{	/* SawMill/defs.sch 599 */
			return (((BgL_rtl_retz00_bglt) COBJECT(BgL_oz00_69))->BgL_toz00);
		}

	}



/* &rtl_ret-to */
	BgL_blockz00_bglt BGl_z62rtl_retzd2tozb0zzsaw_defsz00(obj_t BgL_envz00_7034,
		obj_t BgL_oz00_7035)
	{
		{	/* SawMill/defs.sch 599 */
			return
				BGl_rtl_retzd2tozd2zzsaw_defsz00(((BgL_rtl_retz00_bglt) BgL_oz00_7035));
		}

	}



/* rtl_ret-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_retzd2tozd2setz12z12zzsaw_defsz00(BgL_rtl_retz00_bglt BgL_oz00_70,
		BgL_blockz00_bglt BgL_vz00_71)
	{
		{	/* SawMill/defs.sch 600 */
			return
				((((BgL_rtl_retz00_bglt) COBJECT(BgL_oz00_70))->BgL_toz00) =
				((BgL_blockz00_bglt) BgL_vz00_71), BUNSPEC);
		}

	}



/* &rtl_ret-to-set! */
	obj_t BGl_z62rtl_retzd2tozd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7036,
		obj_t BgL_oz00_7037, obj_t BgL_vz00_7038)
	{
		{	/* SawMill/defs.sch 600 */
			return
				BGl_rtl_retzd2tozd2setz12z12zzsaw_defsz00(
				((BgL_rtl_retz00_bglt) BgL_oz00_7037),
				((BgL_blockz00_bglt) BgL_vz00_7038));
		}

	}



/* rtl_ret-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_retzd2loczd2zzsaw_defsz00(BgL_rtl_retz00_bglt
		BgL_oz00_72)
	{
		{	/* SawMill/defs.sch 601 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_72)))->BgL_locz00);
		}

	}



/* &rtl_ret-loc */
	obj_t BGl_z62rtl_retzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7039,
		obj_t BgL_oz00_7040)
	{
		{	/* SawMill/defs.sch 601 */
			return
				BGl_rtl_retzd2loczd2zzsaw_defsz00(
				((BgL_rtl_retz00_bglt) BgL_oz00_7040));
		}

	}



/* rtl_ret-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_retzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_retz00_bglt BgL_oz00_73,
		obj_t BgL_vz00_74)
	{
		{	/* SawMill/defs.sch 602 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_73)))->BgL_locz00) =
				((obj_t) BgL_vz00_74), BUNSPEC);
		}

	}



/* &rtl_ret-loc-set! */
	obj_t BGl_z62rtl_retzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7041,
		obj_t BgL_oz00_7042, obj_t BgL_vz00_7043)
	{
		{	/* SawMill/defs.sch 602 */
			return
				BGl_rtl_retzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_retz00_bglt) BgL_oz00_7042), BgL_vz00_7043);
		}

	}



/* make-rtl_notseq */
	BGL_EXPORTED_DEF BgL_rtl_notseqz00_bglt
		BGl_makezd2rtl_notseqzd2zzsaw_defsz00(obj_t BgL_loc1486z00_75)
	{
		{	/* SawMill/defs.sch 605 */
			{	/* SawMill/defs.sch 605 */
				BgL_rtl_notseqz00_bglt BgL_new1364z00_8849;

				{	/* SawMill/defs.sch 605 */
					BgL_rtl_notseqz00_bglt BgL_new1363z00_8850;

					BgL_new1363z00_8850 =
						((BgL_rtl_notseqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_notseqz00_bgl))));
					{	/* SawMill/defs.sch 605 */
						long BgL_arg1840z00_8851;

						BgL_arg1840z00_8851 = BGL_CLASS_NUM(BGl_rtl_notseqz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1363z00_8850), BgL_arg1840z00_8851);
					}
					BgL_new1364z00_8849 = BgL_new1363z00_8850;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1364z00_8849)))->BgL_locz00) =
					((obj_t) BgL_loc1486z00_75), BUNSPEC);
				return BgL_new1364z00_8849;
			}
		}

	}



/* &make-rtl_notseq */
	BgL_rtl_notseqz00_bglt BGl_z62makezd2rtl_notseqzb0zzsaw_defsz00(obj_t
		BgL_envz00_7044, obj_t BgL_loc1486z00_7045)
	{
		{	/* SawMill/defs.sch 605 */
			return BGl_makezd2rtl_notseqzd2zzsaw_defsz00(BgL_loc1486z00_7045);
		}

	}



/* rtl_notseq? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_notseqzf3zf3zzsaw_defsz00(obj_t BgL_objz00_76)
	{
		{	/* SawMill/defs.sch 606 */
			{	/* SawMill/defs.sch 606 */
				obj_t BgL_classz00_8852;

				BgL_classz00_8852 = BGl_rtl_notseqz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_76))
					{	/* SawMill/defs.sch 606 */
						BgL_objectz00_bglt BgL_arg1807z00_8853;

						BgL_arg1807z00_8853 = (BgL_objectz00_bglt) (BgL_objz00_76);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 606 */
								long BgL_idxz00_8854;

								BgL_idxz00_8854 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8853);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8854 + 2L)) == BgL_classz00_8852);
							}
						else
							{	/* SawMill/defs.sch 606 */
								bool_t BgL_res3025z00_8857;

								{	/* SawMill/defs.sch 606 */
									obj_t BgL_oclassz00_8858;

									{	/* SawMill/defs.sch 606 */
										obj_t BgL_arg1815z00_8859;
										long BgL_arg1816z00_8860;

										BgL_arg1815z00_8859 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 606 */
											long BgL_arg1817z00_8861;

											BgL_arg1817z00_8861 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8853);
											BgL_arg1816z00_8860 = (BgL_arg1817z00_8861 - OBJECT_TYPE);
										}
										BgL_oclassz00_8858 =
											VECTOR_REF(BgL_arg1815z00_8859, BgL_arg1816z00_8860);
									}
									{	/* SawMill/defs.sch 606 */
										bool_t BgL__ortest_1115z00_8862;

										BgL__ortest_1115z00_8862 =
											(BgL_classz00_8852 == BgL_oclassz00_8858);
										if (BgL__ortest_1115z00_8862)
											{	/* SawMill/defs.sch 606 */
												BgL_res3025z00_8857 = BgL__ortest_1115z00_8862;
											}
										else
											{	/* SawMill/defs.sch 606 */
												long BgL_odepthz00_8863;

												{	/* SawMill/defs.sch 606 */
													obj_t BgL_arg1804z00_8864;

													BgL_arg1804z00_8864 = (BgL_oclassz00_8858);
													BgL_odepthz00_8863 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8864);
												}
												if ((2L < BgL_odepthz00_8863))
													{	/* SawMill/defs.sch 606 */
														obj_t BgL_arg1802z00_8865;

														{	/* SawMill/defs.sch 606 */
															obj_t BgL_arg1803z00_8866;

															BgL_arg1803z00_8866 = (BgL_oclassz00_8858);
															BgL_arg1802z00_8865 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8866,
																2L);
														}
														BgL_res3025z00_8857 =
															(BgL_arg1802z00_8865 == BgL_classz00_8852);
													}
												else
													{	/* SawMill/defs.sch 606 */
														BgL_res3025z00_8857 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3025z00_8857;
							}
					}
				else
					{	/* SawMill/defs.sch 606 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_notseq? */
	obj_t BGl_z62rtl_notseqzf3z91zzsaw_defsz00(obj_t BgL_envz00_7046,
		obj_t BgL_objz00_7047)
	{
		{	/* SawMill/defs.sch 606 */
			return BBOOL(BGl_rtl_notseqzf3zf3zzsaw_defsz00(BgL_objz00_7047));
		}

	}



/* rtl_notseq-nil */
	BGL_EXPORTED_DEF BgL_rtl_notseqz00_bglt
		BGl_rtl_notseqzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 607 */
			{	/* SawMill/defs.sch 607 */
				obj_t BgL_classz00_4547;

				BgL_classz00_4547 = BGl_rtl_notseqz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 607 */
					obj_t BgL__ortest_1117z00_4548;

					BgL__ortest_1117z00_4548 = BGL_CLASS_NIL(BgL_classz00_4547);
					if (CBOOL(BgL__ortest_1117z00_4548))
						{	/* SawMill/defs.sch 607 */
							return ((BgL_rtl_notseqz00_bglt) BgL__ortest_1117z00_4548);
						}
					else
						{	/* SawMill/defs.sch 607 */
							return
								((BgL_rtl_notseqz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4547));
						}
				}
			}
		}

	}



/* &rtl_notseq-nil */
	BgL_rtl_notseqz00_bglt BGl_z62rtl_notseqzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7048)
	{
		{	/* SawMill/defs.sch 607 */
			return BGl_rtl_notseqzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_notseq-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_notseqzd2loczd2zzsaw_defsz00(BgL_rtl_notseqz00_bglt BgL_oz00_77)
	{
		{	/* SawMill/defs.sch 608 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_77)))->BgL_locz00);
		}

	}



/* &rtl_notseq-loc */
	obj_t BGl_z62rtl_notseqzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7049,
		obj_t BgL_oz00_7050)
	{
		{	/* SawMill/defs.sch 608 */
			return
				BGl_rtl_notseqzd2loczd2zzsaw_defsz00(
				((BgL_rtl_notseqz00_bglt) BgL_oz00_7050));
		}

	}



/* rtl_notseq-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_notseqzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_notseqz00_bglt
		BgL_oz00_78, obj_t BgL_vz00_79)
	{
		{	/* SawMill/defs.sch 609 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_78)))->BgL_locz00) =
				((obj_t) BgL_vz00_79), BUNSPEC);
		}

	}



/* &rtl_notseq-loc-set! */
	obj_t BGl_z62rtl_notseqzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7051,
		obj_t BgL_oz00_7052, obj_t BgL_vz00_7053)
	{
		{	/* SawMill/defs.sch 609 */
			return
				BGl_rtl_notseqzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_notseqz00_bglt) BgL_oz00_7052), BgL_vz00_7053);
		}

	}



/* make-rtl_if */
	BGL_EXPORTED_DEF BgL_rtl_ifz00_bglt BGl_makezd2rtl_ifzd2zzsaw_defsz00(obj_t
		BgL_loc1484z00_80)
	{
		{	/* SawMill/defs.sch 612 */
			{	/* SawMill/defs.sch 612 */
				BgL_rtl_ifz00_bglt BgL_new1366z00_8867;

				{	/* SawMill/defs.sch 612 */
					BgL_rtl_ifz00_bglt BgL_new1365z00_8868;

					BgL_new1365z00_8868 =
						((BgL_rtl_ifz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_ifz00_bgl))));
					{	/* SawMill/defs.sch 612 */
						long BgL_arg1842z00_8869;

						BgL_arg1842z00_8869 = BGL_CLASS_NUM(BGl_rtl_ifz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1365z00_8868), BgL_arg1842z00_8869);
					}
					BgL_new1366z00_8867 = BgL_new1365z00_8868;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1366z00_8867)))->BgL_locz00) =
					((obj_t) BgL_loc1484z00_80), BUNSPEC);
				return BgL_new1366z00_8867;
			}
		}

	}



/* &make-rtl_if */
	BgL_rtl_ifz00_bglt BGl_z62makezd2rtl_ifzb0zzsaw_defsz00(obj_t BgL_envz00_7054,
		obj_t BgL_loc1484z00_7055)
	{
		{	/* SawMill/defs.sch 612 */
			return BGl_makezd2rtl_ifzd2zzsaw_defsz00(BgL_loc1484z00_7055);
		}

	}



/* rtl_if? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_ifzf3zf3zzsaw_defsz00(obj_t BgL_objz00_81)
	{
		{	/* SawMill/defs.sch 613 */
			{	/* SawMill/defs.sch 613 */
				obj_t BgL_classz00_8870;

				BgL_classz00_8870 = BGl_rtl_ifz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_81))
					{	/* SawMill/defs.sch 613 */
						BgL_objectz00_bglt BgL_arg1807z00_8871;

						BgL_arg1807z00_8871 = (BgL_objectz00_bglt) (BgL_objz00_81);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 613 */
								long BgL_idxz00_8872;

								BgL_idxz00_8872 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8871);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8872 + 3L)) == BgL_classz00_8870);
							}
						else
							{	/* SawMill/defs.sch 613 */
								bool_t BgL_res3026z00_8875;

								{	/* SawMill/defs.sch 613 */
									obj_t BgL_oclassz00_8876;

									{	/* SawMill/defs.sch 613 */
										obj_t BgL_arg1815z00_8877;
										long BgL_arg1816z00_8878;

										BgL_arg1815z00_8877 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 613 */
											long BgL_arg1817z00_8879;

											BgL_arg1817z00_8879 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8871);
											BgL_arg1816z00_8878 = (BgL_arg1817z00_8879 - OBJECT_TYPE);
										}
										BgL_oclassz00_8876 =
											VECTOR_REF(BgL_arg1815z00_8877, BgL_arg1816z00_8878);
									}
									{	/* SawMill/defs.sch 613 */
										bool_t BgL__ortest_1115z00_8880;

										BgL__ortest_1115z00_8880 =
											(BgL_classz00_8870 == BgL_oclassz00_8876);
										if (BgL__ortest_1115z00_8880)
											{	/* SawMill/defs.sch 613 */
												BgL_res3026z00_8875 = BgL__ortest_1115z00_8880;
											}
										else
											{	/* SawMill/defs.sch 613 */
												long BgL_odepthz00_8881;

												{	/* SawMill/defs.sch 613 */
													obj_t BgL_arg1804z00_8882;

													BgL_arg1804z00_8882 = (BgL_oclassz00_8876);
													BgL_odepthz00_8881 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8882);
												}
												if ((3L < BgL_odepthz00_8881))
													{	/* SawMill/defs.sch 613 */
														obj_t BgL_arg1802z00_8883;

														{	/* SawMill/defs.sch 613 */
															obj_t BgL_arg1803z00_8884;

															BgL_arg1803z00_8884 = (BgL_oclassz00_8876);
															BgL_arg1802z00_8883 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8884,
																3L);
														}
														BgL_res3026z00_8875 =
															(BgL_arg1802z00_8883 == BgL_classz00_8870);
													}
												else
													{	/* SawMill/defs.sch 613 */
														BgL_res3026z00_8875 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3026z00_8875;
							}
					}
				else
					{	/* SawMill/defs.sch 613 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_if? */
	obj_t BGl_z62rtl_ifzf3z91zzsaw_defsz00(obj_t BgL_envz00_7056,
		obj_t BgL_objz00_7057)
	{
		{	/* SawMill/defs.sch 613 */
			return BBOOL(BGl_rtl_ifzf3zf3zzsaw_defsz00(BgL_objz00_7057));
		}

	}



/* rtl_if-nil */
	BGL_EXPORTED_DEF BgL_rtl_ifz00_bglt BGl_rtl_ifzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 614 */
			{	/* SawMill/defs.sch 614 */
				obj_t BgL_classz00_4589;

				BgL_classz00_4589 = BGl_rtl_ifz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 614 */
					obj_t BgL__ortest_1117z00_4590;

					BgL__ortest_1117z00_4590 = BGL_CLASS_NIL(BgL_classz00_4589);
					if (CBOOL(BgL__ortest_1117z00_4590))
						{	/* SawMill/defs.sch 614 */
							return ((BgL_rtl_ifz00_bglt) BgL__ortest_1117z00_4590);
						}
					else
						{	/* SawMill/defs.sch 614 */
							return
								((BgL_rtl_ifz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4589));
						}
				}
			}
		}

	}



/* &rtl_if-nil */
	BgL_rtl_ifz00_bglt BGl_z62rtl_ifzd2nilzb0zzsaw_defsz00(obj_t BgL_envz00_7058)
	{
		{	/* SawMill/defs.sch 614 */
			return BGl_rtl_ifzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_if-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_ifzd2loczd2zzsaw_defsz00(BgL_rtl_ifz00_bglt
		BgL_oz00_82)
	{
		{	/* SawMill/defs.sch 615 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_82)))->BgL_locz00);
		}

	}



/* &rtl_if-loc */
	obj_t BGl_z62rtl_ifzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7059,
		obj_t BgL_oz00_7060)
	{
		{	/* SawMill/defs.sch 615 */
			return
				BGl_rtl_ifzd2loczd2zzsaw_defsz00(((BgL_rtl_ifz00_bglt) BgL_oz00_7060));
		}

	}



/* rtl_if-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_ifzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_ifz00_bglt BgL_oz00_83,
		obj_t BgL_vz00_84)
	{
		{	/* SawMill/defs.sch 616 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_83)))->BgL_locz00) =
				((obj_t) BgL_vz00_84), BUNSPEC);
		}

	}



/* &rtl_if-loc-set! */
	obj_t BGl_z62rtl_ifzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7061,
		obj_t BgL_oz00_7062, obj_t BgL_vz00_7063)
	{
		{	/* SawMill/defs.sch 616 */
			return
				BGl_rtl_ifzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_ifz00_bglt) BgL_oz00_7062), BgL_vz00_7063);
		}

	}



/* make-rtl_select */
	BGL_EXPORTED_DEF BgL_rtl_selectz00_bglt
		BGl_makezd2rtl_selectzd2zzsaw_defsz00(obj_t BgL_loc1480z00_85,
		BgL_typez00_bglt BgL_type1481z00_86, obj_t BgL_patterns1482z00_87)
	{
		{	/* SawMill/defs.sch 619 */
			{	/* SawMill/defs.sch 619 */
				BgL_rtl_selectz00_bglt BgL_new1368z00_8885;

				{	/* SawMill/defs.sch 619 */
					BgL_rtl_selectz00_bglt BgL_new1367z00_8886;

					BgL_new1367z00_8886 =
						((BgL_rtl_selectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_selectz00_bgl))));
					{	/* SawMill/defs.sch 619 */
						long BgL_arg1843z00_8887;

						BgL_arg1843z00_8887 = BGL_CLASS_NUM(BGl_rtl_selectz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1367z00_8886), BgL_arg1843z00_8887);
					}
					BgL_new1368z00_8885 = BgL_new1367z00_8886;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1368z00_8885)))->BgL_locz00) =
					((obj_t) BgL_loc1480z00_85), BUNSPEC);
				((((BgL_rtl_selectz00_bglt) COBJECT(BgL_new1368z00_8885))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1481z00_86), BUNSPEC);
				((((BgL_rtl_selectz00_bglt) COBJECT(BgL_new1368z00_8885))->
						BgL_patternsz00) = ((obj_t) BgL_patterns1482z00_87), BUNSPEC);
				return BgL_new1368z00_8885;
			}
		}

	}



/* &make-rtl_select */
	BgL_rtl_selectz00_bglt BGl_z62makezd2rtl_selectzb0zzsaw_defsz00(obj_t
		BgL_envz00_7064, obj_t BgL_loc1480z00_7065, obj_t BgL_type1481z00_7066,
		obj_t BgL_patterns1482z00_7067)
	{
		{	/* SawMill/defs.sch 619 */
			return
				BGl_makezd2rtl_selectzd2zzsaw_defsz00(BgL_loc1480z00_7065,
				((BgL_typez00_bglt) BgL_type1481z00_7066), BgL_patterns1482z00_7067);
		}

	}



/* rtl_select? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_selectzf3zf3zzsaw_defsz00(obj_t BgL_objz00_88)
	{
		{	/* SawMill/defs.sch 620 */
			{	/* SawMill/defs.sch 620 */
				obj_t BgL_classz00_8888;

				BgL_classz00_8888 = BGl_rtl_selectz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_88))
					{	/* SawMill/defs.sch 620 */
						BgL_objectz00_bglt BgL_arg1807z00_8889;

						BgL_arg1807z00_8889 = (BgL_objectz00_bglt) (BgL_objz00_88);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 620 */
								long BgL_idxz00_8890;

								BgL_idxz00_8890 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8889);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8890 + 3L)) == BgL_classz00_8888);
							}
						else
							{	/* SawMill/defs.sch 620 */
								bool_t BgL_res3027z00_8893;

								{	/* SawMill/defs.sch 620 */
									obj_t BgL_oclassz00_8894;

									{	/* SawMill/defs.sch 620 */
										obj_t BgL_arg1815z00_8895;
										long BgL_arg1816z00_8896;

										BgL_arg1815z00_8895 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 620 */
											long BgL_arg1817z00_8897;

											BgL_arg1817z00_8897 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8889);
											BgL_arg1816z00_8896 = (BgL_arg1817z00_8897 - OBJECT_TYPE);
										}
										BgL_oclassz00_8894 =
											VECTOR_REF(BgL_arg1815z00_8895, BgL_arg1816z00_8896);
									}
									{	/* SawMill/defs.sch 620 */
										bool_t BgL__ortest_1115z00_8898;

										BgL__ortest_1115z00_8898 =
											(BgL_classz00_8888 == BgL_oclassz00_8894);
										if (BgL__ortest_1115z00_8898)
											{	/* SawMill/defs.sch 620 */
												BgL_res3027z00_8893 = BgL__ortest_1115z00_8898;
											}
										else
											{	/* SawMill/defs.sch 620 */
												long BgL_odepthz00_8899;

												{	/* SawMill/defs.sch 620 */
													obj_t BgL_arg1804z00_8900;

													BgL_arg1804z00_8900 = (BgL_oclassz00_8894);
													BgL_odepthz00_8899 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8900);
												}
												if ((3L < BgL_odepthz00_8899))
													{	/* SawMill/defs.sch 620 */
														obj_t BgL_arg1802z00_8901;

														{	/* SawMill/defs.sch 620 */
															obj_t BgL_arg1803z00_8902;

															BgL_arg1803z00_8902 = (BgL_oclassz00_8894);
															BgL_arg1802z00_8901 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8902,
																3L);
														}
														BgL_res3027z00_8893 =
															(BgL_arg1802z00_8901 == BgL_classz00_8888);
													}
												else
													{	/* SawMill/defs.sch 620 */
														BgL_res3027z00_8893 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3027z00_8893;
							}
					}
				else
					{	/* SawMill/defs.sch 620 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_select? */
	obj_t BGl_z62rtl_selectzf3z91zzsaw_defsz00(obj_t BgL_envz00_7068,
		obj_t BgL_objz00_7069)
	{
		{	/* SawMill/defs.sch 620 */
			return BBOOL(BGl_rtl_selectzf3zf3zzsaw_defsz00(BgL_objz00_7069));
		}

	}



/* rtl_select-nil */
	BGL_EXPORTED_DEF BgL_rtl_selectz00_bglt
		BGl_rtl_selectzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 621 */
			{	/* SawMill/defs.sch 621 */
				obj_t BgL_classz00_4631;

				BgL_classz00_4631 = BGl_rtl_selectz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 621 */
					obj_t BgL__ortest_1117z00_4632;

					BgL__ortest_1117z00_4632 = BGL_CLASS_NIL(BgL_classz00_4631);
					if (CBOOL(BgL__ortest_1117z00_4632))
						{	/* SawMill/defs.sch 621 */
							return ((BgL_rtl_selectz00_bglt) BgL__ortest_1117z00_4632);
						}
					else
						{	/* SawMill/defs.sch 621 */
							return
								((BgL_rtl_selectz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4631));
						}
				}
			}
		}

	}



/* &rtl_select-nil */
	BgL_rtl_selectz00_bglt BGl_z62rtl_selectzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7070)
	{
		{	/* SawMill/defs.sch 621 */
			return BGl_rtl_selectzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_select-patterns */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_selectzd2patternszd2zzsaw_defsz00(BgL_rtl_selectz00_bglt
		BgL_oz00_89)
	{
		{	/* SawMill/defs.sch 622 */
			return (((BgL_rtl_selectz00_bglt) COBJECT(BgL_oz00_89))->BgL_patternsz00);
		}

	}



/* &rtl_select-patterns */
	obj_t BGl_z62rtl_selectzd2patternszb0zzsaw_defsz00(obj_t BgL_envz00_7071,
		obj_t BgL_oz00_7072)
	{
		{	/* SawMill/defs.sch 622 */
			return
				BGl_rtl_selectzd2patternszd2zzsaw_defsz00(
				((BgL_rtl_selectz00_bglt) BgL_oz00_7072));
		}

	}



/* rtl_select-patterns-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_selectzd2patternszd2setz12z12zzsaw_defsz00(BgL_rtl_selectz00_bglt
		BgL_oz00_90, obj_t BgL_vz00_91)
	{
		{	/* SawMill/defs.sch 623 */
			return
				((((BgL_rtl_selectz00_bglt) COBJECT(BgL_oz00_90))->BgL_patternsz00) =
				((obj_t) BgL_vz00_91), BUNSPEC);
		}

	}



/* &rtl_select-patterns-set! */
	obj_t BGl_z62rtl_selectzd2patternszd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7073, obj_t BgL_oz00_7074, obj_t BgL_vz00_7075)
	{
		{	/* SawMill/defs.sch 623 */
			return
				BGl_rtl_selectzd2patternszd2setz12z12zzsaw_defsz00(
				((BgL_rtl_selectz00_bglt) BgL_oz00_7074), BgL_vz00_7075);
		}

	}



/* rtl_select-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_selectzd2typezd2zzsaw_defsz00(BgL_rtl_selectz00_bglt BgL_oz00_92)
	{
		{	/* SawMill/defs.sch 624 */
			return (((BgL_rtl_selectz00_bglt) COBJECT(BgL_oz00_92))->BgL_typez00);
		}

	}



/* &rtl_select-type */
	BgL_typez00_bglt BGl_z62rtl_selectzd2typezb0zzsaw_defsz00(obj_t
		BgL_envz00_7076, obj_t BgL_oz00_7077)
	{
		{	/* SawMill/defs.sch 624 */
			return
				BGl_rtl_selectzd2typezd2zzsaw_defsz00(
				((BgL_rtl_selectz00_bglt) BgL_oz00_7077));
		}

	}



/* rtl_select-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_selectzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_selectz00_bglt
		BgL_oz00_93, BgL_typez00_bglt BgL_vz00_94)
	{
		{	/* SawMill/defs.sch 625 */
			return
				((((BgL_rtl_selectz00_bglt) COBJECT(BgL_oz00_93))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_94), BUNSPEC);
		}

	}



/* &rtl_select-type-set! */
	obj_t BGl_z62rtl_selectzd2typezd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7078,
		obj_t BgL_oz00_7079, obj_t BgL_vz00_7080)
	{
		{	/* SawMill/defs.sch 625 */
			return
				BGl_rtl_selectzd2typezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_selectz00_bglt) BgL_oz00_7079),
				((BgL_typez00_bglt) BgL_vz00_7080));
		}

	}



/* rtl_select-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_selectzd2loczd2zzsaw_defsz00(BgL_rtl_selectz00_bglt BgL_oz00_95)
	{
		{	/* SawMill/defs.sch 626 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_95)))->BgL_locz00);
		}

	}



/* &rtl_select-loc */
	obj_t BGl_z62rtl_selectzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7081,
		obj_t BgL_oz00_7082)
	{
		{	/* SawMill/defs.sch 626 */
			return
				BGl_rtl_selectzd2loczd2zzsaw_defsz00(
				((BgL_rtl_selectz00_bglt) BgL_oz00_7082));
		}

	}



/* rtl_select-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_selectzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_selectz00_bglt
		BgL_oz00_96, obj_t BgL_vz00_97)
	{
		{	/* SawMill/defs.sch 627 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_96)))->BgL_locz00) =
				((obj_t) BgL_vz00_97), BUNSPEC);
		}

	}



/* &rtl_select-loc-set! */
	obj_t BGl_z62rtl_selectzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7083,
		obj_t BgL_oz00_7084, obj_t BgL_vz00_7085)
	{
		{	/* SawMill/defs.sch 627 */
			return
				BGl_rtl_selectzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_selectz00_bglt) BgL_oz00_7084), BgL_vz00_7085);
		}

	}



/* make-rtl_switch */
	BGL_EXPORTED_DEF BgL_rtl_switchz00_bglt
		BGl_makezd2rtl_switchzd2zzsaw_defsz00(obj_t BgL_loc1475z00_98,
		BgL_typez00_bglt BgL_type1476z00_99, obj_t BgL_patterns1477z00_100,
		obj_t BgL_labels1478z00_101)
	{
		{	/* SawMill/defs.sch 630 */
			{	/* SawMill/defs.sch 630 */
				BgL_rtl_switchz00_bglt BgL_new1370z00_8903;

				{	/* SawMill/defs.sch 630 */
					BgL_rtl_switchz00_bglt BgL_new1369z00_8904;

					BgL_new1369z00_8904 =
						((BgL_rtl_switchz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_switchz00_bgl))));
					{	/* SawMill/defs.sch 630 */
						long BgL_arg1844z00_8905;

						BgL_arg1844z00_8905 = BGL_CLASS_NUM(BGl_rtl_switchz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1369z00_8904), BgL_arg1844z00_8905);
					}
					BgL_new1370z00_8903 = BgL_new1369z00_8904;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1370z00_8903)))->BgL_locz00) =
					((obj_t) BgL_loc1475z00_98), BUNSPEC);
				((((BgL_rtl_selectz00_bglt) COBJECT(((BgL_rtl_selectz00_bglt)
									BgL_new1370z00_8903)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1476z00_99), BUNSPEC);
				((((BgL_rtl_selectz00_bglt) COBJECT(((BgL_rtl_selectz00_bglt)
									BgL_new1370z00_8903)))->BgL_patternsz00) =
					((obj_t) BgL_patterns1477z00_100), BUNSPEC);
				((((BgL_rtl_switchz00_bglt) COBJECT(BgL_new1370z00_8903))->
						BgL_labelsz00) = ((obj_t) BgL_labels1478z00_101), BUNSPEC);
				return BgL_new1370z00_8903;
			}
		}

	}



/* &make-rtl_switch */
	BgL_rtl_switchz00_bglt BGl_z62makezd2rtl_switchzb0zzsaw_defsz00(obj_t
		BgL_envz00_7086, obj_t BgL_loc1475z00_7087, obj_t BgL_type1476z00_7088,
		obj_t BgL_patterns1477z00_7089, obj_t BgL_labels1478z00_7090)
	{
		{	/* SawMill/defs.sch 630 */
			return
				BGl_makezd2rtl_switchzd2zzsaw_defsz00(BgL_loc1475z00_7087,
				((BgL_typez00_bglt) BgL_type1476z00_7088), BgL_patterns1477z00_7089,
				BgL_labels1478z00_7090);
		}

	}



/* rtl_switch? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_switchzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_102)
	{
		{	/* SawMill/defs.sch 631 */
			{	/* SawMill/defs.sch 631 */
				obj_t BgL_classz00_8906;

				BgL_classz00_8906 = BGl_rtl_switchz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_102))
					{	/* SawMill/defs.sch 631 */
						BgL_objectz00_bglt BgL_arg1807z00_8907;

						BgL_arg1807z00_8907 = (BgL_objectz00_bglt) (BgL_objz00_102);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 631 */
								long BgL_idxz00_8908;

								BgL_idxz00_8908 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8907);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8908 + 4L)) == BgL_classz00_8906);
							}
						else
							{	/* SawMill/defs.sch 631 */
								bool_t BgL_res3028z00_8911;

								{	/* SawMill/defs.sch 631 */
									obj_t BgL_oclassz00_8912;

									{	/* SawMill/defs.sch 631 */
										obj_t BgL_arg1815z00_8913;
										long BgL_arg1816z00_8914;

										BgL_arg1815z00_8913 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 631 */
											long BgL_arg1817z00_8915;

											BgL_arg1817z00_8915 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8907);
											BgL_arg1816z00_8914 = (BgL_arg1817z00_8915 - OBJECT_TYPE);
										}
										BgL_oclassz00_8912 =
											VECTOR_REF(BgL_arg1815z00_8913, BgL_arg1816z00_8914);
									}
									{	/* SawMill/defs.sch 631 */
										bool_t BgL__ortest_1115z00_8916;

										BgL__ortest_1115z00_8916 =
											(BgL_classz00_8906 == BgL_oclassz00_8912);
										if (BgL__ortest_1115z00_8916)
											{	/* SawMill/defs.sch 631 */
												BgL_res3028z00_8911 = BgL__ortest_1115z00_8916;
											}
										else
											{	/* SawMill/defs.sch 631 */
												long BgL_odepthz00_8917;

												{	/* SawMill/defs.sch 631 */
													obj_t BgL_arg1804z00_8918;

													BgL_arg1804z00_8918 = (BgL_oclassz00_8912);
													BgL_odepthz00_8917 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8918);
												}
												if ((4L < BgL_odepthz00_8917))
													{	/* SawMill/defs.sch 631 */
														obj_t BgL_arg1802z00_8919;

														{	/* SawMill/defs.sch 631 */
															obj_t BgL_arg1803z00_8920;

															BgL_arg1803z00_8920 = (BgL_oclassz00_8912);
															BgL_arg1802z00_8919 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8920,
																4L);
														}
														BgL_res3028z00_8911 =
															(BgL_arg1802z00_8919 == BgL_classz00_8906);
													}
												else
													{	/* SawMill/defs.sch 631 */
														BgL_res3028z00_8911 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3028z00_8911;
							}
					}
				else
					{	/* SawMill/defs.sch 631 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_switch? */
	obj_t BGl_z62rtl_switchzf3z91zzsaw_defsz00(obj_t BgL_envz00_7091,
		obj_t BgL_objz00_7092)
	{
		{	/* SawMill/defs.sch 631 */
			return BBOOL(BGl_rtl_switchzf3zf3zzsaw_defsz00(BgL_objz00_7092));
		}

	}



/* rtl_switch-nil */
	BGL_EXPORTED_DEF BgL_rtl_switchz00_bglt
		BGl_rtl_switchzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 632 */
			{	/* SawMill/defs.sch 632 */
				obj_t BgL_classz00_4673;

				BgL_classz00_4673 = BGl_rtl_switchz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 632 */
					obj_t BgL__ortest_1117z00_4674;

					BgL__ortest_1117z00_4674 = BGL_CLASS_NIL(BgL_classz00_4673);
					if (CBOOL(BgL__ortest_1117z00_4674))
						{	/* SawMill/defs.sch 632 */
							return ((BgL_rtl_switchz00_bglt) BgL__ortest_1117z00_4674);
						}
					else
						{	/* SawMill/defs.sch 632 */
							return
								((BgL_rtl_switchz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4673));
						}
				}
			}
		}

	}



/* &rtl_switch-nil */
	BgL_rtl_switchz00_bglt BGl_z62rtl_switchzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7093)
	{
		{	/* SawMill/defs.sch 632 */
			return BGl_rtl_switchzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_switch-labels */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_switchzd2labelszd2zzsaw_defsz00(BgL_rtl_switchz00_bglt BgL_oz00_103)
	{
		{	/* SawMill/defs.sch 633 */
			return (((BgL_rtl_switchz00_bglt) COBJECT(BgL_oz00_103))->BgL_labelsz00);
		}

	}



/* &rtl_switch-labels */
	obj_t BGl_z62rtl_switchzd2labelszb0zzsaw_defsz00(obj_t BgL_envz00_7094,
		obj_t BgL_oz00_7095)
	{
		{	/* SawMill/defs.sch 633 */
			return
				BGl_rtl_switchzd2labelszd2zzsaw_defsz00(
				((BgL_rtl_switchz00_bglt) BgL_oz00_7095));
		}

	}



/* rtl_switch-labels-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_switchzd2labelszd2setz12z12zzsaw_defsz00(BgL_rtl_switchz00_bglt
		BgL_oz00_104, obj_t BgL_vz00_105)
	{
		{	/* SawMill/defs.sch 634 */
			return
				((((BgL_rtl_switchz00_bglt) COBJECT(BgL_oz00_104))->BgL_labelsz00) =
				((obj_t) BgL_vz00_105), BUNSPEC);
		}

	}



/* &rtl_switch-labels-set! */
	obj_t BGl_z62rtl_switchzd2labelszd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7096, obj_t BgL_oz00_7097, obj_t BgL_vz00_7098)
	{
		{	/* SawMill/defs.sch 634 */
			return
				BGl_rtl_switchzd2labelszd2setz12z12zzsaw_defsz00(
				((BgL_rtl_switchz00_bglt) BgL_oz00_7097), BgL_vz00_7098);
		}

	}



/* rtl_switch-patterns */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_switchzd2patternszd2zzsaw_defsz00(BgL_rtl_switchz00_bglt
		BgL_oz00_106)
	{
		{	/* SawMill/defs.sch 635 */
			return
				(((BgL_rtl_selectz00_bglt) COBJECT(
						((BgL_rtl_selectz00_bglt) BgL_oz00_106)))->BgL_patternsz00);
		}

	}



/* &rtl_switch-patterns */
	obj_t BGl_z62rtl_switchzd2patternszb0zzsaw_defsz00(obj_t BgL_envz00_7099,
		obj_t BgL_oz00_7100)
	{
		{	/* SawMill/defs.sch 635 */
			return
				BGl_rtl_switchzd2patternszd2zzsaw_defsz00(
				((BgL_rtl_switchz00_bglt) BgL_oz00_7100));
		}

	}



/* rtl_switch-patterns-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_switchzd2patternszd2setz12z12zzsaw_defsz00(BgL_rtl_switchz00_bglt
		BgL_oz00_107, obj_t BgL_vz00_108)
	{
		{	/* SawMill/defs.sch 636 */
			return
				((((BgL_rtl_selectz00_bglt) COBJECT(
							((BgL_rtl_selectz00_bglt) BgL_oz00_107)))->BgL_patternsz00) =
				((obj_t) BgL_vz00_108), BUNSPEC);
		}

	}



/* &rtl_switch-patterns-set! */
	obj_t BGl_z62rtl_switchzd2patternszd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7101, obj_t BgL_oz00_7102, obj_t BgL_vz00_7103)
	{
		{	/* SawMill/defs.sch 636 */
			return
				BGl_rtl_switchzd2patternszd2setz12z12zzsaw_defsz00(
				((BgL_rtl_switchz00_bglt) BgL_oz00_7102), BgL_vz00_7103);
		}

	}



/* rtl_switch-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_switchzd2typezd2zzsaw_defsz00(BgL_rtl_switchz00_bglt BgL_oz00_109)
	{
		{	/* SawMill/defs.sch 637 */
			return
				(((BgL_rtl_selectz00_bglt) COBJECT(
						((BgL_rtl_selectz00_bglt) BgL_oz00_109)))->BgL_typez00);
		}

	}



/* &rtl_switch-type */
	BgL_typez00_bglt BGl_z62rtl_switchzd2typezb0zzsaw_defsz00(obj_t
		BgL_envz00_7104, obj_t BgL_oz00_7105)
	{
		{	/* SawMill/defs.sch 637 */
			return
				BGl_rtl_switchzd2typezd2zzsaw_defsz00(
				((BgL_rtl_switchz00_bglt) BgL_oz00_7105));
		}

	}



/* rtl_switch-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_switchzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_switchz00_bglt
		BgL_oz00_110, BgL_typez00_bglt BgL_vz00_111)
	{
		{	/* SawMill/defs.sch 638 */
			return
				((((BgL_rtl_selectz00_bglt) COBJECT(
							((BgL_rtl_selectz00_bglt) BgL_oz00_110)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_111), BUNSPEC);
		}

	}



/* &rtl_switch-type-set! */
	obj_t BGl_z62rtl_switchzd2typezd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7106,
		obj_t BgL_oz00_7107, obj_t BgL_vz00_7108)
	{
		{	/* SawMill/defs.sch 638 */
			return
				BGl_rtl_switchzd2typezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_switchz00_bglt) BgL_oz00_7107),
				((BgL_typez00_bglt) BgL_vz00_7108));
		}

	}



/* rtl_switch-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_switchzd2loczd2zzsaw_defsz00(BgL_rtl_switchz00_bglt BgL_oz00_112)
	{
		{	/* SawMill/defs.sch 639 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_112)))->BgL_locz00);
		}

	}



/* &rtl_switch-loc */
	obj_t BGl_z62rtl_switchzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7109,
		obj_t BgL_oz00_7110)
	{
		{	/* SawMill/defs.sch 639 */
			return
				BGl_rtl_switchzd2loczd2zzsaw_defsz00(
				((BgL_rtl_switchz00_bglt) BgL_oz00_7110));
		}

	}



/* rtl_switch-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_switchzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_switchz00_bglt
		BgL_oz00_113, obj_t BgL_vz00_114)
	{
		{	/* SawMill/defs.sch 640 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_113)))->BgL_locz00) =
				((obj_t) BgL_vz00_114), BUNSPEC);
		}

	}



/* &rtl_switch-loc-set! */
	obj_t BGl_z62rtl_switchzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7111,
		obj_t BgL_oz00_7112, obj_t BgL_vz00_7113)
	{
		{	/* SawMill/defs.sch 640 */
			return
				BGl_rtl_switchzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_switchz00_bglt) BgL_oz00_7112), BgL_vz00_7113);
		}

	}



/* make-rtl_ifeq */
	BGL_EXPORTED_DEF BgL_rtl_ifeqz00_bglt
		BGl_makezd2rtl_ifeqzd2zzsaw_defsz00(obj_t BgL_loc1471z00_115,
		BgL_blockz00_bglt BgL_then1472z00_116)
	{
		{	/* SawMill/defs.sch 643 */
			{	/* SawMill/defs.sch 643 */
				BgL_rtl_ifeqz00_bglt BgL_new1372z00_8921;

				{	/* SawMill/defs.sch 643 */
					BgL_rtl_ifeqz00_bglt BgL_new1371z00_8922;

					BgL_new1371z00_8922 =
						((BgL_rtl_ifeqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_ifeqz00_bgl))));
					{	/* SawMill/defs.sch 643 */
						long BgL_arg1845z00_8923;

						BgL_arg1845z00_8923 = BGL_CLASS_NUM(BGl_rtl_ifeqz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1371z00_8922), BgL_arg1845z00_8923);
					}
					BgL_new1372z00_8921 = BgL_new1371z00_8922;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1372z00_8921)))->BgL_locz00) =
					((obj_t) BgL_loc1471z00_115), BUNSPEC);
				((((BgL_rtl_ifeqz00_bglt) COBJECT(BgL_new1372z00_8921))->BgL_thenz00) =
					((BgL_blockz00_bglt) BgL_then1472z00_116), BUNSPEC);
				return BgL_new1372z00_8921;
			}
		}

	}



/* &make-rtl_ifeq */
	BgL_rtl_ifeqz00_bglt BGl_z62makezd2rtl_ifeqzb0zzsaw_defsz00(obj_t
		BgL_envz00_7114, obj_t BgL_loc1471z00_7115, obj_t BgL_then1472z00_7116)
	{
		{	/* SawMill/defs.sch 643 */
			return
				BGl_makezd2rtl_ifeqzd2zzsaw_defsz00(BgL_loc1471z00_7115,
				((BgL_blockz00_bglt) BgL_then1472z00_7116));
		}

	}



/* rtl_ifeq? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_ifeqzf3zf3zzsaw_defsz00(obj_t BgL_objz00_117)
	{
		{	/* SawMill/defs.sch 644 */
			{	/* SawMill/defs.sch 644 */
				obj_t BgL_classz00_8924;

				BgL_classz00_8924 = BGl_rtl_ifeqz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_117))
					{	/* SawMill/defs.sch 644 */
						BgL_objectz00_bglt BgL_arg1807z00_8925;

						BgL_arg1807z00_8925 = (BgL_objectz00_bglt) (BgL_objz00_117);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 644 */
								long BgL_idxz00_8926;

								BgL_idxz00_8926 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8925);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8926 + 3L)) == BgL_classz00_8924);
							}
						else
							{	/* SawMill/defs.sch 644 */
								bool_t BgL_res3029z00_8929;

								{	/* SawMill/defs.sch 644 */
									obj_t BgL_oclassz00_8930;

									{	/* SawMill/defs.sch 644 */
										obj_t BgL_arg1815z00_8931;
										long BgL_arg1816z00_8932;

										BgL_arg1815z00_8931 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 644 */
											long BgL_arg1817z00_8933;

											BgL_arg1817z00_8933 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8925);
											BgL_arg1816z00_8932 = (BgL_arg1817z00_8933 - OBJECT_TYPE);
										}
										BgL_oclassz00_8930 =
											VECTOR_REF(BgL_arg1815z00_8931, BgL_arg1816z00_8932);
									}
									{	/* SawMill/defs.sch 644 */
										bool_t BgL__ortest_1115z00_8934;

										BgL__ortest_1115z00_8934 =
											(BgL_classz00_8924 == BgL_oclassz00_8930);
										if (BgL__ortest_1115z00_8934)
											{	/* SawMill/defs.sch 644 */
												BgL_res3029z00_8929 = BgL__ortest_1115z00_8934;
											}
										else
											{	/* SawMill/defs.sch 644 */
												long BgL_odepthz00_8935;

												{	/* SawMill/defs.sch 644 */
													obj_t BgL_arg1804z00_8936;

													BgL_arg1804z00_8936 = (BgL_oclassz00_8930);
													BgL_odepthz00_8935 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8936);
												}
												if ((3L < BgL_odepthz00_8935))
													{	/* SawMill/defs.sch 644 */
														obj_t BgL_arg1802z00_8937;

														{	/* SawMill/defs.sch 644 */
															obj_t BgL_arg1803z00_8938;

															BgL_arg1803z00_8938 = (BgL_oclassz00_8930);
															BgL_arg1802z00_8937 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8938,
																3L);
														}
														BgL_res3029z00_8929 =
															(BgL_arg1802z00_8937 == BgL_classz00_8924);
													}
												else
													{	/* SawMill/defs.sch 644 */
														BgL_res3029z00_8929 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3029z00_8929;
							}
					}
				else
					{	/* SawMill/defs.sch 644 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_ifeq? */
	obj_t BGl_z62rtl_ifeqzf3z91zzsaw_defsz00(obj_t BgL_envz00_7117,
		obj_t BgL_objz00_7118)
	{
		{	/* SawMill/defs.sch 644 */
			return BBOOL(BGl_rtl_ifeqzf3zf3zzsaw_defsz00(BgL_objz00_7118));
		}

	}



/* rtl_ifeq-nil */
	BGL_EXPORTED_DEF BgL_rtl_ifeqz00_bglt BGl_rtl_ifeqzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 645 */
			{	/* SawMill/defs.sch 645 */
				obj_t BgL_classz00_4715;

				BgL_classz00_4715 = BGl_rtl_ifeqz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 645 */
					obj_t BgL__ortest_1117z00_4716;

					BgL__ortest_1117z00_4716 = BGL_CLASS_NIL(BgL_classz00_4715);
					if (CBOOL(BgL__ortest_1117z00_4716))
						{	/* SawMill/defs.sch 645 */
							return ((BgL_rtl_ifeqz00_bglt) BgL__ortest_1117z00_4716);
						}
					else
						{	/* SawMill/defs.sch 645 */
							return
								((BgL_rtl_ifeqz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4715));
						}
				}
			}
		}

	}



/* &rtl_ifeq-nil */
	BgL_rtl_ifeqz00_bglt BGl_z62rtl_ifeqzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7119)
	{
		{	/* SawMill/defs.sch 645 */
			return BGl_rtl_ifeqzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_ifeq-then */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_rtl_ifeqzd2thenzd2zzsaw_defsz00(BgL_rtl_ifeqz00_bglt BgL_oz00_118)
	{
		{	/* SawMill/defs.sch 646 */
			return (((BgL_rtl_ifeqz00_bglt) COBJECT(BgL_oz00_118))->BgL_thenz00);
		}

	}



/* &rtl_ifeq-then */
	BgL_blockz00_bglt BGl_z62rtl_ifeqzd2thenzb0zzsaw_defsz00(obj_t
		BgL_envz00_7120, obj_t BgL_oz00_7121)
	{
		{	/* SawMill/defs.sch 646 */
			return
				BGl_rtl_ifeqzd2thenzd2zzsaw_defsz00(
				((BgL_rtl_ifeqz00_bglt) BgL_oz00_7121));
		}

	}



/* rtl_ifeq-then-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_ifeqzd2thenzd2setz12z12zzsaw_defsz00(BgL_rtl_ifeqz00_bglt
		BgL_oz00_119, BgL_blockz00_bglt BgL_vz00_120)
	{
		{	/* SawMill/defs.sch 647 */
			return
				((((BgL_rtl_ifeqz00_bglt) COBJECT(BgL_oz00_119))->BgL_thenz00) =
				((BgL_blockz00_bglt) BgL_vz00_120), BUNSPEC);
		}

	}



/* &rtl_ifeq-then-set! */
	obj_t BGl_z62rtl_ifeqzd2thenzd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7122,
		obj_t BgL_oz00_7123, obj_t BgL_vz00_7124)
	{
		{	/* SawMill/defs.sch 647 */
			return
				BGl_rtl_ifeqzd2thenzd2setz12z12zzsaw_defsz00(
				((BgL_rtl_ifeqz00_bglt) BgL_oz00_7123),
				((BgL_blockz00_bglt) BgL_vz00_7124));
		}

	}



/* rtl_ifeq-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_ifeqzd2loczd2zzsaw_defsz00(BgL_rtl_ifeqz00_bglt
		BgL_oz00_121)
	{
		{	/* SawMill/defs.sch 648 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_121)))->BgL_locz00);
		}

	}



/* &rtl_ifeq-loc */
	obj_t BGl_z62rtl_ifeqzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7125,
		obj_t BgL_oz00_7126)
	{
		{	/* SawMill/defs.sch 648 */
			return
				BGl_rtl_ifeqzd2loczd2zzsaw_defsz00(
				((BgL_rtl_ifeqz00_bglt) BgL_oz00_7126));
		}

	}



/* rtl_ifeq-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_ifeqzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_ifeqz00_bglt
		BgL_oz00_122, obj_t BgL_vz00_123)
	{
		{	/* SawMill/defs.sch 649 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_122)))->BgL_locz00) =
				((obj_t) BgL_vz00_123), BUNSPEC);
		}

	}



/* &rtl_ifeq-loc-set! */
	obj_t BGl_z62rtl_ifeqzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7127,
		obj_t BgL_oz00_7128, obj_t BgL_vz00_7129)
	{
		{	/* SawMill/defs.sch 649 */
			return
				BGl_rtl_ifeqzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_ifeqz00_bglt) BgL_oz00_7128), BgL_vz00_7129);
		}

	}



/* make-rtl_ifne */
	BGL_EXPORTED_DEF BgL_rtl_ifnez00_bglt
		BGl_makezd2rtl_ifnezd2zzsaw_defsz00(obj_t BgL_loc1468z00_124,
		BgL_blockz00_bglt BgL_then1469z00_125)
	{
		{	/* SawMill/defs.sch 652 */
			{	/* SawMill/defs.sch 652 */
				BgL_rtl_ifnez00_bglt BgL_new1374z00_8939;

				{	/* SawMill/defs.sch 652 */
					BgL_rtl_ifnez00_bglt BgL_new1373z00_8940;

					BgL_new1373z00_8940 =
						((BgL_rtl_ifnez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_ifnez00_bgl))));
					{	/* SawMill/defs.sch 652 */
						long BgL_arg1846z00_8941;

						BgL_arg1846z00_8941 = BGL_CLASS_NUM(BGl_rtl_ifnez00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1373z00_8940), BgL_arg1846z00_8941);
					}
					BgL_new1374z00_8939 = BgL_new1373z00_8940;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1374z00_8939)))->BgL_locz00) =
					((obj_t) BgL_loc1468z00_124), BUNSPEC);
				((((BgL_rtl_ifnez00_bglt) COBJECT(BgL_new1374z00_8939))->BgL_thenz00) =
					((BgL_blockz00_bglt) BgL_then1469z00_125), BUNSPEC);
				return BgL_new1374z00_8939;
			}
		}

	}



/* &make-rtl_ifne */
	BgL_rtl_ifnez00_bglt BGl_z62makezd2rtl_ifnezb0zzsaw_defsz00(obj_t
		BgL_envz00_7130, obj_t BgL_loc1468z00_7131, obj_t BgL_then1469z00_7132)
	{
		{	/* SawMill/defs.sch 652 */
			return
				BGl_makezd2rtl_ifnezd2zzsaw_defsz00(BgL_loc1468z00_7131,
				((BgL_blockz00_bglt) BgL_then1469z00_7132));
		}

	}



/* rtl_ifne? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_ifnezf3zf3zzsaw_defsz00(obj_t BgL_objz00_126)
	{
		{	/* SawMill/defs.sch 653 */
			{	/* SawMill/defs.sch 653 */
				obj_t BgL_classz00_8942;

				BgL_classz00_8942 = BGl_rtl_ifnez00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_126))
					{	/* SawMill/defs.sch 653 */
						BgL_objectz00_bglt BgL_arg1807z00_8943;

						BgL_arg1807z00_8943 = (BgL_objectz00_bglt) (BgL_objz00_126);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 653 */
								long BgL_idxz00_8944;

								BgL_idxz00_8944 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8943);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8944 + 3L)) == BgL_classz00_8942);
							}
						else
							{	/* SawMill/defs.sch 653 */
								bool_t BgL_res3030z00_8947;

								{	/* SawMill/defs.sch 653 */
									obj_t BgL_oclassz00_8948;

									{	/* SawMill/defs.sch 653 */
										obj_t BgL_arg1815z00_8949;
										long BgL_arg1816z00_8950;

										BgL_arg1815z00_8949 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 653 */
											long BgL_arg1817z00_8951;

											BgL_arg1817z00_8951 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8943);
											BgL_arg1816z00_8950 = (BgL_arg1817z00_8951 - OBJECT_TYPE);
										}
										BgL_oclassz00_8948 =
											VECTOR_REF(BgL_arg1815z00_8949, BgL_arg1816z00_8950);
									}
									{	/* SawMill/defs.sch 653 */
										bool_t BgL__ortest_1115z00_8952;

										BgL__ortest_1115z00_8952 =
											(BgL_classz00_8942 == BgL_oclassz00_8948);
										if (BgL__ortest_1115z00_8952)
											{	/* SawMill/defs.sch 653 */
												BgL_res3030z00_8947 = BgL__ortest_1115z00_8952;
											}
										else
											{	/* SawMill/defs.sch 653 */
												long BgL_odepthz00_8953;

												{	/* SawMill/defs.sch 653 */
													obj_t BgL_arg1804z00_8954;

													BgL_arg1804z00_8954 = (BgL_oclassz00_8948);
													BgL_odepthz00_8953 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8954);
												}
												if ((3L < BgL_odepthz00_8953))
													{	/* SawMill/defs.sch 653 */
														obj_t BgL_arg1802z00_8955;

														{	/* SawMill/defs.sch 653 */
															obj_t BgL_arg1803z00_8956;

															BgL_arg1803z00_8956 = (BgL_oclassz00_8948);
															BgL_arg1802z00_8955 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8956,
																3L);
														}
														BgL_res3030z00_8947 =
															(BgL_arg1802z00_8955 == BgL_classz00_8942);
													}
												else
													{	/* SawMill/defs.sch 653 */
														BgL_res3030z00_8947 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3030z00_8947;
							}
					}
				else
					{	/* SawMill/defs.sch 653 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_ifne? */
	obj_t BGl_z62rtl_ifnezf3z91zzsaw_defsz00(obj_t BgL_envz00_7133,
		obj_t BgL_objz00_7134)
	{
		{	/* SawMill/defs.sch 653 */
			return BBOOL(BGl_rtl_ifnezf3zf3zzsaw_defsz00(BgL_objz00_7134));
		}

	}



/* rtl_ifne-nil */
	BGL_EXPORTED_DEF BgL_rtl_ifnez00_bglt BGl_rtl_ifnezd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 654 */
			{	/* SawMill/defs.sch 654 */
				obj_t BgL_classz00_4757;

				BgL_classz00_4757 = BGl_rtl_ifnez00zzsaw_defsz00;
				{	/* SawMill/defs.sch 654 */
					obj_t BgL__ortest_1117z00_4758;

					BgL__ortest_1117z00_4758 = BGL_CLASS_NIL(BgL_classz00_4757);
					if (CBOOL(BgL__ortest_1117z00_4758))
						{	/* SawMill/defs.sch 654 */
							return ((BgL_rtl_ifnez00_bglt) BgL__ortest_1117z00_4758);
						}
					else
						{	/* SawMill/defs.sch 654 */
							return
								((BgL_rtl_ifnez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4757));
						}
				}
			}
		}

	}



/* &rtl_ifne-nil */
	BgL_rtl_ifnez00_bglt BGl_z62rtl_ifnezd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7135)
	{
		{	/* SawMill/defs.sch 654 */
			return BGl_rtl_ifnezd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_ifne-then */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_rtl_ifnezd2thenzd2zzsaw_defsz00(BgL_rtl_ifnez00_bglt BgL_oz00_127)
	{
		{	/* SawMill/defs.sch 655 */
			return (((BgL_rtl_ifnez00_bglt) COBJECT(BgL_oz00_127))->BgL_thenz00);
		}

	}



/* &rtl_ifne-then */
	BgL_blockz00_bglt BGl_z62rtl_ifnezd2thenzb0zzsaw_defsz00(obj_t
		BgL_envz00_7136, obj_t BgL_oz00_7137)
	{
		{	/* SawMill/defs.sch 655 */
			return
				BGl_rtl_ifnezd2thenzd2zzsaw_defsz00(
				((BgL_rtl_ifnez00_bglt) BgL_oz00_7137));
		}

	}



/* rtl_ifne-then-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_ifnezd2thenzd2setz12z12zzsaw_defsz00(BgL_rtl_ifnez00_bglt
		BgL_oz00_128, BgL_blockz00_bglt BgL_vz00_129)
	{
		{	/* SawMill/defs.sch 656 */
			return
				((((BgL_rtl_ifnez00_bglt) COBJECT(BgL_oz00_128))->BgL_thenz00) =
				((BgL_blockz00_bglt) BgL_vz00_129), BUNSPEC);
		}

	}



/* &rtl_ifne-then-set! */
	obj_t BGl_z62rtl_ifnezd2thenzd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7138,
		obj_t BgL_oz00_7139, obj_t BgL_vz00_7140)
	{
		{	/* SawMill/defs.sch 656 */
			return
				BGl_rtl_ifnezd2thenzd2setz12z12zzsaw_defsz00(
				((BgL_rtl_ifnez00_bglt) BgL_oz00_7139),
				((BgL_blockz00_bglt) BgL_vz00_7140));
		}

	}



/* rtl_ifne-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_ifnezd2loczd2zzsaw_defsz00(BgL_rtl_ifnez00_bglt
		BgL_oz00_130)
	{
		{	/* SawMill/defs.sch 657 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_130)))->BgL_locz00);
		}

	}



/* &rtl_ifne-loc */
	obj_t BGl_z62rtl_ifnezd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7141,
		obj_t BgL_oz00_7142)
	{
		{	/* SawMill/defs.sch 657 */
			return
				BGl_rtl_ifnezd2loczd2zzsaw_defsz00(
				((BgL_rtl_ifnez00_bglt) BgL_oz00_7142));
		}

	}



/* rtl_ifne-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_ifnezd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_ifnez00_bglt
		BgL_oz00_131, obj_t BgL_vz00_132)
	{
		{	/* SawMill/defs.sch 658 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_131)))->BgL_locz00) =
				((obj_t) BgL_vz00_132), BUNSPEC);
		}

	}



/* &rtl_ifne-loc-set! */
	obj_t BGl_z62rtl_ifnezd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7143,
		obj_t BgL_oz00_7144, obj_t BgL_vz00_7145)
	{
		{	/* SawMill/defs.sch 658 */
			return
				BGl_rtl_ifnezd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_ifnez00_bglt) BgL_oz00_7144), BgL_vz00_7145);
		}

	}



/* make-rtl_go */
	BGL_EXPORTED_DEF BgL_rtl_goz00_bglt BGl_makezd2rtl_gozd2zzsaw_defsz00(obj_t
		BgL_loc1465z00_133, BgL_blockz00_bglt BgL_to1466z00_134)
	{
		{	/* SawMill/defs.sch 661 */
			{	/* SawMill/defs.sch 661 */
				BgL_rtl_goz00_bglt BgL_new1376z00_8957;

				{	/* SawMill/defs.sch 661 */
					BgL_rtl_goz00_bglt BgL_new1375z00_8958;

					BgL_new1375z00_8958 =
						((BgL_rtl_goz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_goz00_bgl))));
					{	/* SawMill/defs.sch 661 */
						long BgL_arg1847z00_8959;

						BgL_arg1847z00_8959 = BGL_CLASS_NUM(BGl_rtl_goz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1375z00_8958), BgL_arg1847z00_8959);
					}
					BgL_new1376z00_8957 = BgL_new1375z00_8958;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1376z00_8957)))->BgL_locz00) =
					((obj_t) BgL_loc1465z00_133), BUNSPEC);
				((((BgL_rtl_goz00_bglt) COBJECT(BgL_new1376z00_8957))->BgL_toz00) =
					((BgL_blockz00_bglt) BgL_to1466z00_134), BUNSPEC);
				return BgL_new1376z00_8957;
			}
		}

	}



/* &make-rtl_go */
	BgL_rtl_goz00_bglt BGl_z62makezd2rtl_gozb0zzsaw_defsz00(obj_t BgL_envz00_7146,
		obj_t BgL_loc1465z00_7147, obj_t BgL_to1466z00_7148)
	{
		{	/* SawMill/defs.sch 661 */
			return
				BGl_makezd2rtl_gozd2zzsaw_defsz00(BgL_loc1465z00_7147,
				((BgL_blockz00_bglt) BgL_to1466z00_7148));
		}

	}



/* rtl_go? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_gozf3zf3zzsaw_defsz00(obj_t BgL_objz00_135)
	{
		{	/* SawMill/defs.sch 662 */
			{	/* SawMill/defs.sch 662 */
				obj_t BgL_classz00_8960;

				BgL_classz00_8960 = BGl_rtl_goz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_135))
					{	/* SawMill/defs.sch 662 */
						BgL_objectz00_bglt BgL_arg1807z00_8961;

						BgL_arg1807z00_8961 = (BgL_objectz00_bglt) (BgL_objz00_135);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 662 */
								long BgL_idxz00_8962;

								BgL_idxz00_8962 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8961);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8962 + 3L)) == BgL_classz00_8960);
							}
						else
							{	/* SawMill/defs.sch 662 */
								bool_t BgL_res3031z00_8965;

								{	/* SawMill/defs.sch 662 */
									obj_t BgL_oclassz00_8966;

									{	/* SawMill/defs.sch 662 */
										obj_t BgL_arg1815z00_8967;
										long BgL_arg1816z00_8968;

										BgL_arg1815z00_8967 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 662 */
											long BgL_arg1817z00_8969;

											BgL_arg1817z00_8969 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8961);
											BgL_arg1816z00_8968 = (BgL_arg1817z00_8969 - OBJECT_TYPE);
										}
										BgL_oclassz00_8966 =
											VECTOR_REF(BgL_arg1815z00_8967, BgL_arg1816z00_8968);
									}
									{	/* SawMill/defs.sch 662 */
										bool_t BgL__ortest_1115z00_8970;

										BgL__ortest_1115z00_8970 =
											(BgL_classz00_8960 == BgL_oclassz00_8966);
										if (BgL__ortest_1115z00_8970)
											{	/* SawMill/defs.sch 662 */
												BgL_res3031z00_8965 = BgL__ortest_1115z00_8970;
											}
										else
											{	/* SawMill/defs.sch 662 */
												long BgL_odepthz00_8971;

												{	/* SawMill/defs.sch 662 */
													obj_t BgL_arg1804z00_8972;

													BgL_arg1804z00_8972 = (BgL_oclassz00_8966);
													BgL_odepthz00_8971 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8972);
												}
												if ((3L < BgL_odepthz00_8971))
													{	/* SawMill/defs.sch 662 */
														obj_t BgL_arg1802z00_8973;

														{	/* SawMill/defs.sch 662 */
															obj_t BgL_arg1803z00_8974;

															BgL_arg1803z00_8974 = (BgL_oclassz00_8966);
															BgL_arg1802z00_8973 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8974,
																3L);
														}
														BgL_res3031z00_8965 =
															(BgL_arg1802z00_8973 == BgL_classz00_8960);
													}
												else
													{	/* SawMill/defs.sch 662 */
														BgL_res3031z00_8965 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3031z00_8965;
							}
					}
				else
					{	/* SawMill/defs.sch 662 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_go? */
	obj_t BGl_z62rtl_gozf3z91zzsaw_defsz00(obj_t BgL_envz00_7149,
		obj_t BgL_objz00_7150)
	{
		{	/* SawMill/defs.sch 662 */
			return BBOOL(BGl_rtl_gozf3zf3zzsaw_defsz00(BgL_objz00_7150));
		}

	}



/* rtl_go-nil */
	BGL_EXPORTED_DEF BgL_rtl_goz00_bglt BGl_rtl_gozd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 663 */
			{	/* SawMill/defs.sch 663 */
				obj_t BgL_classz00_4799;

				BgL_classz00_4799 = BGl_rtl_goz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 663 */
					obj_t BgL__ortest_1117z00_4800;

					BgL__ortest_1117z00_4800 = BGL_CLASS_NIL(BgL_classz00_4799);
					if (CBOOL(BgL__ortest_1117z00_4800))
						{	/* SawMill/defs.sch 663 */
							return ((BgL_rtl_goz00_bglt) BgL__ortest_1117z00_4800);
						}
					else
						{	/* SawMill/defs.sch 663 */
							return
								((BgL_rtl_goz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4799));
						}
				}
			}
		}

	}



/* &rtl_go-nil */
	BgL_rtl_goz00_bglt BGl_z62rtl_gozd2nilzb0zzsaw_defsz00(obj_t BgL_envz00_7151)
	{
		{	/* SawMill/defs.sch 663 */
			return BGl_rtl_gozd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_go-to */
	BGL_EXPORTED_DEF BgL_blockz00_bglt
		BGl_rtl_gozd2tozd2zzsaw_defsz00(BgL_rtl_goz00_bglt BgL_oz00_136)
	{
		{	/* SawMill/defs.sch 664 */
			return (((BgL_rtl_goz00_bglt) COBJECT(BgL_oz00_136))->BgL_toz00);
		}

	}



/* &rtl_go-to */
	BgL_blockz00_bglt BGl_z62rtl_gozd2tozb0zzsaw_defsz00(obj_t BgL_envz00_7152,
		obj_t BgL_oz00_7153)
	{
		{	/* SawMill/defs.sch 664 */
			return
				BGl_rtl_gozd2tozd2zzsaw_defsz00(((BgL_rtl_goz00_bglt) BgL_oz00_7153));
		}

	}



/* rtl_go-to-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_gozd2tozd2setz12z12zzsaw_defsz00(BgL_rtl_goz00_bglt BgL_oz00_137,
		BgL_blockz00_bglt BgL_vz00_138)
	{
		{	/* SawMill/defs.sch 665 */
			return
				((((BgL_rtl_goz00_bglt) COBJECT(BgL_oz00_137))->BgL_toz00) =
				((BgL_blockz00_bglt) BgL_vz00_138), BUNSPEC);
		}

	}



/* &rtl_go-to-set! */
	obj_t BGl_z62rtl_gozd2tozd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7154,
		obj_t BgL_oz00_7155, obj_t BgL_vz00_7156)
	{
		{	/* SawMill/defs.sch 665 */
			return
				BGl_rtl_gozd2tozd2setz12z12zzsaw_defsz00(
				((BgL_rtl_goz00_bglt) BgL_oz00_7155),
				((BgL_blockz00_bglt) BgL_vz00_7156));
		}

	}



/* rtl_go-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_gozd2loczd2zzsaw_defsz00(BgL_rtl_goz00_bglt
		BgL_oz00_139)
	{
		{	/* SawMill/defs.sch 666 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_139)))->BgL_locz00);
		}

	}



/* &rtl_go-loc */
	obj_t BGl_z62rtl_gozd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7157,
		obj_t BgL_oz00_7158)
	{
		{	/* SawMill/defs.sch 666 */
			return
				BGl_rtl_gozd2loczd2zzsaw_defsz00(((BgL_rtl_goz00_bglt) BgL_oz00_7158));
		}

	}



/* rtl_go-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_gozd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_goz00_bglt BgL_oz00_140,
		obj_t BgL_vz00_141)
	{
		{	/* SawMill/defs.sch 667 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_140)))->BgL_locz00) =
				((obj_t) BgL_vz00_141), BUNSPEC);
		}

	}



/* &rtl_go-loc-set! */
	obj_t BGl_z62rtl_gozd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7159,
		obj_t BgL_oz00_7160, obj_t BgL_vz00_7161)
	{
		{	/* SawMill/defs.sch 667 */
			return
				BGl_rtl_gozd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_goz00_bglt) BgL_oz00_7160), BgL_vz00_7161);
		}

	}



/* make-rtl_pure */
	BGL_EXPORTED_DEF BgL_rtl_purez00_bglt
		BGl_makezd2rtl_purezd2zzsaw_defsz00(obj_t BgL_loc1461z00_142)
	{
		{	/* SawMill/defs.sch 670 */
			{	/* SawMill/defs.sch 670 */
				BgL_rtl_purez00_bglt BgL_new1378z00_8975;

				{	/* SawMill/defs.sch 670 */
					BgL_rtl_purez00_bglt BgL_new1377z00_8976;

					BgL_new1377z00_8976 =
						((BgL_rtl_purez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_purez00_bgl))));
					{	/* SawMill/defs.sch 670 */
						long BgL_arg1848z00_8977;

						BgL_arg1848z00_8977 = BGL_CLASS_NUM(BGl_rtl_purez00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1377z00_8976), BgL_arg1848z00_8977);
					}
					BgL_new1378z00_8975 = BgL_new1377z00_8976;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1378z00_8975)))->BgL_locz00) =
					((obj_t) BgL_loc1461z00_142), BUNSPEC);
				return BgL_new1378z00_8975;
			}
		}

	}



/* &make-rtl_pure */
	BgL_rtl_purez00_bglt BGl_z62makezd2rtl_purezb0zzsaw_defsz00(obj_t
		BgL_envz00_7162, obj_t BgL_loc1461z00_7163)
	{
		{	/* SawMill/defs.sch 670 */
			return BGl_makezd2rtl_purezd2zzsaw_defsz00(BgL_loc1461z00_7163);
		}

	}



/* rtl_pure? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_purezf3zf3zzsaw_defsz00(obj_t BgL_objz00_143)
	{
		{	/* SawMill/defs.sch 671 */
			{	/* SawMill/defs.sch 671 */
				obj_t BgL_classz00_8978;

				BgL_classz00_8978 = BGl_rtl_purez00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_143))
					{	/* SawMill/defs.sch 671 */
						BgL_objectz00_bglt BgL_arg1807z00_8979;

						BgL_arg1807z00_8979 = (BgL_objectz00_bglt) (BgL_objz00_143);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 671 */
								long BgL_idxz00_8980;

								BgL_idxz00_8980 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8979);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8980 + 2L)) == BgL_classz00_8978);
							}
						else
							{	/* SawMill/defs.sch 671 */
								bool_t BgL_res3032z00_8983;

								{	/* SawMill/defs.sch 671 */
									obj_t BgL_oclassz00_8984;

									{	/* SawMill/defs.sch 671 */
										obj_t BgL_arg1815z00_8985;
										long BgL_arg1816z00_8986;

										BgL_arg1815z00_8985 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 671 */
											long BgL_arg1817z00_8987;

											BgL_arg1817z00_8987 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8979);
											BgL_arg1816z00_8986 = (BgL_arg1817z00_8987 - OBJECT_TYPE);
										}
										BgL_oclassz00_8984 =
											VECTOR_REF(BgL_arg1815z00_8985, BgL_arg1816z00_8986);
									}
									{	/* SawMill/defs.sch 671 */
										bool_t BgL__ortest_1115z00_8988;

										BgL__ortest_1115z00_8988 =
											(BgL_classz00_8978 == BgL_oclassz00_8984);
										if (BgL__ortest_1115z00_8988)
											{	/* SawMill/defs.sch 671 */
												BgL_res3032z00_8983 = BgL__ortest_1115z00_8988;
											}
										else
											{	/* SawMill/defs.sch 671 */
												long BgL_odepthz00_8989;

												{	/* SawMill/defs.sch 671 */
													obj_t BgL_arg1804z00_8990;

													BgL_arg1804z00_8990 = (BgL_oclassz00_8984);
													BgL_odepthz00_8989 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_8990);
												}
												if ((2L < BgL_odepthz00_8989))
													{	/* SawMill/defs.sch 671 */
														obj_t BgL_arg1802z00_8991;

														{	/* SawMill/defs.sch 671 */
															obj_t BgL_arg1803z00_8992;

															BgL_arg1803z00_8992 = (BgL_oclassz00_8984);
															BgL_arg1802z00_8991 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_8992,
																2L);
														}
														BgL_res3032z00_8983 =
															(BgL_arg1802z00_8991 == BgL_classz00_8978);
													}
												else
													{	/* SawMill/defs.sch 671 */
														BgL_res3032z00_8983 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3032z00_8983;
							}
					}
				else
					{	/* SawMill/defs.sch 671 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_pure? */
	obj_t BGl_z62rtl_purezf3z91zzsaw_defsz00(obj_t BgL_envz00_7164,
		obj_t BgL_objz00_7165)
	{
		{	/* SawMill/defs.sch 671 */
			return BBOOL(BGl_rtl_purezf3zf3zzsaw_defsz00(BgL_objz00_7165));
		}

	}



/* rtl_pure-nil */
	BGL_EXPORTED_DEF BgL_rtl_purez00_bglt BGl_rtl_purezd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 672 */
			{	/* SawMill/defs.sch 672 */
				obj_t BgL_classz00_4841;

				BgL_classz00_4841 = BGl_rtl_purez00zzsaw_defsz00;
				{	/* SawMill/defs.sch 672 */
					obj_t BgL__ortest_1117z00_4842;

					BgL__ortest_1117z00_4842 = BGL_CLASS_NIL(BgL_classz00_4841);
					if (CBOOL(BgL__ortest_1117z00_4842))
						{	/* SawMill/defs.sch 672 */
							return ((BgL_rtl_purez00_bglt) BgL__ortest_1117z00_4842);
						}
					else
						{	/* SawMill/defs.sch 672 */
							return
								((BgL_rtl_purez00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4841));
						}
				}
			}
		}

	}



/* &rtl_pure-nil */
	BgL_rtl_purez00_bglt BGl_z62rtl_purezd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7166)
	{
		{	/* SawMill/defs.sch 672 */
			return BGl_rtl_purezd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_pure-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_purezd2loczd2zzsaw_defsz00(BgL_rtl_purez00_bglt
		BgL_oz00_144)
	{
		{	/* SawMill/defs.sch 673 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_144)))->BgL_locz00);
		}

	}



/* &rtl_pure-loc */
	obj_t BGl_z62rtl_purezd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7167,
		obj_t BgL_oz00_7168)
	{
		{	/* SawMill/defs.sch 673 */
			return
				BGl_rtl_purezd2loczd2zzsaw_defsz00(
				((BgL_rtl_purez00_bglt) BgL_oz00_7168));
		}

	}



/* rtl_pure-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_purezd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_purez00_bglt
		BgL_oz00_145, obj_t BgL_vz00_146)
	{
		{	/* SawMill/defs.sch 674 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_145)))->BgL_locz00) =
				((obj_t) BgL_vz00_146), BUNSPEC);
		}

	}



/* &rtl_pure-loc-set! */
	obj_t BGl_z62rtl_purezd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7169,
		obj_t BgL_oz00_7170, obj_t BgL_vz00_7171)
	{
		{	/* SawMill/defs.sch 674 */
			return
				BGl_rtl_purezd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_purez00_bglt) BgL_oz00_7170), BgL_vz00_7171);
		}

	}



/* make-rtl_nop */
	BGL_EXPORTED_DEF BgL_rtl_nopz00_bglt BGl_makezd2rtl_nopzd2zzsaw_defsz00(obj_t
		BgL_loc1459z00_147)
	{
		{	/* SawMill/defs.sch 677 */
			{	/* SawMill/defs.sch 677 */
				BgL_rtl_nopz00_bglt BgL_new1380z00_8993;

				{	/* SawMill/defs.sch 677 */
					BgL_rtl_nopz00_bglt BgL_new1379z00_8994;

					BgL_new1379z00_8994 =
						((BgL_rtl_nopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_nopz00_bgl))));
					{	/* SawMill/defs.sch 677 */
						long BgL_arg1849z00_8995;

						BgL_arg1849z00_8995 = BGL_CLASS_NUM(BGl_rtl_nopz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1379z00_8994), BgL_arg1849z00_8995);
					}
					BgL_new1380z00_8993 = BgL_new1379z00_8994;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1380z00_8993)))->BgL_locz00) =
					((obj_t) BgL_loc1459z00_147), BUNSPEC);
				return BgL_new1380z00_8993;
			}
		}

	}



/* &make-rtl_nop */
	BgL_rtl_nopz00_bglt BGl_z62makezd2rtl_nopzb0zzsaw_defsz00(obj_t
		BgL_envz00_7172, obj_t BgL_loc1459z00_7173)
	{
		{	/* SawMill/defs.sch 677 */
			return BGl_makezd2rtl_nopzd2zzsaw_defsz00(BgL_loc1459z00_7173);
		}

	}



/* rtl_nop? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_nopzf3zf3zzsaw_defsz00(obj_t BgL_objz00_148)
	{
		{	/* SawMill/defs.sch 678 */
			{	/* SawMill/defs.sch 678 */
				obj_t BgL_classz00_8996;

				BgL_classz00_8996 = BGl_rtl_nopz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_148))
					{	/* SawMill/defs.sch 678 */
						BgL_objectz00_bglt BgL_arg1807z00_8997;

						BgL_arg1807z00_8997 = (BgL_objectz00_bglt) (BgL_objz00_148);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 678 */
								long BgL_idxz00_8998;

								BgL_idxz00_8998 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_8997);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_8998 + 3L)) == BgL_classz00_8996);
							}
						else
							{	/* SawMill/defs.sch 678 */
								bool_t BgL_res3033z00_9001;

								{	/* SawMill/defs.sch 678 */
									obj_t BgL_oclassz00_9002;

									{	/* SawMill/defs.sch 678 */
										obj_t BgL_arg1815z00_9003;
										long BgL_arg1816z00_9004;

										BgL_arg1815z00_9003 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 678 */
											long BgL_arg1817z00_9005;

											BgL_arg1817z00_9005 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_8997);
											BgL_arg1816z00_9004 = (BgL_arg1817z00_9005 - OBJECT_TYPE);
										}
										BgL_oclassz00_9002 =
											VECTOR_REF(BgL_arg1815z00_9003, BgL_arg1816z00_9004);
									}
									{	/* SawMill/defs.sch 678 */
										bool_t BgL__ortest_1115z00_9006;

										BgL__ortest_1115z00_9006 =
											(BgL_classz00_8996 == BgL_oclassz00_9002);
										if (BgL__ortest_1115z00_9006)
											{	/* SawMill/defs.sch 678 */
												BgL_res3033z00_9001 = BgL__ortest_1115z00_9006;
											}
										else
											{	/* SawMill/defs.sch 678 */
												long BgL_odepthz00_9007;

												{	/* SawMill/defs.sch 678 */
													obj_t BgL_arg1804z00_9008;

													BgL_arg1804z00_9008 = (BgL_oclassz00_9002);
													BgL_odepthz00_9007 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9008);
												}
												if ((3L < BgL_odepthz00_9007))
													{	/* SawMill/defs.sch 678 */
														obj_t BgL_arg1802z00_9009;

														{	/* SawMill/defs.sch 678 */
															obj_t BgL_arg1803z00_9010;

															BgL_arg1803z00_9010 = (BgL_oclassz00_9002);
															BgL_arg1802z00_9009 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9010,
																3L);
														}
														BgL_res3033z00_9001 =
															(BgL_arg1802z00_9009 == BgL_classz00_8996);
													}
												else
													{	/* SawMill/defs.sch 678 */
														BgL_res3033z00_9001 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3033z00_9001;
							}
					}
				else
					{	/* SawMill/defs.sch 678 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_nop? */
	obj_t BGl_z62rtl_nopzf3z91zzsaw_defsz00(obj_t BgL_envz00_7174,
		obj_t BgL_objz00_7175)
	{
		{	/* SawMill/defs.sch 678 */
			return BBOOL(BGl_rtl_nopzf3zf3zzsaw_defsz00(BgL_objz00_7175));
		}

	}



/* rtl_nop-nil */
	BGL_EXPORTED_DEF BgL_rtl_nopz00_bglt BGl_rtl_nopzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 679 */
			{	/* SawMill/defs.sch 679 */
				obj_t BgL_classz00_4883;

				BgL_classz00_4883 = BGl_rtl_nopz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 679 */
					obj_t BgL__ortest_1117z00_4884;

					BgL__ortest_1117z00_4884 = BGL_CLASS_NIL(BgL_classz00_4883);
					if (CBOOL(BgL__ortest_1117z00_4884))
						{	/* SawMill/defs.sch 679 */
							return ((BgL_rtl_nopz00_bglt) BgL__ortest_1117z00_4884);
						}
					else
						{	/* SawMill/defs.sch 679 */
							return
								((BgL_rtl_nopz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4883));
						}
				}
			}
		}

	}



/* &rtl_nop-nil */
	BgL_rtl_nopz00_bglt BGl_z62rtl_nopzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7176)
	{
		{	/* SawMill/defs.sch 679 */
			return BGl_rtl_nopzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_nop-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_nopzd2loczd2zzsaw_defsz00(BgL_rtl_nopz00_bglt
		BgL_oz00_149)
	{
		{	/* SawMill/defs.sch 680 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_149)))->BgL_locz00);
		}

	}



/* &rtl_nop-loc */
	obj_t BGl_z62rtl_nopzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7177,
		obj_t BgL_oz00_7178)
	{
		{	/* SawMill/defs.sch 680 */
			return
				BGl_rtl_nopzd2loczd2zzsaw_defsz00(
				((BgL_rtl_nopz00_bglt) BgL_oz00_7178));
		}

	}



/* rtl_nop-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_nopzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_nopz00_bglt BgL_oz00_150,
		obj_t BgL_vz00_151)
	{
		{	/* SawMill/defs.sch 681 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_150)))->BgL_locz00) =
				((obj_t) BgL_vz00_151), BUNSPEC);
		}

	}



/* &rtl_nop-loc-set! */
	obj_t BGl_z62rtl_nopzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7179,
		obj_t BgL_oz00_7180, obj_t BgL_vz00_7181)
	{
		{	/* SawMill/defs.sch 681 */
			return
				BGl_rtl_nopzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_nopz00_bglt) BgL_oz00_7180), BgL_vz00_7181);
		}

	}



/* make-rtl_mov */
	BGL_EXPORTED_DEF BgL_rtl_movz00_bglt BGl_makezd2rtl_movzd2zzsaw_defsz00(obj_t
		BgL_loc1457z00_152)
	{
		{	/* SawMill/defs.sch 684 */
			{	/* SawMill/defs.sch 684 */
				BgL_rtl_movz00_bglt BgL_new1382z00_9011;

				{	/* SawMill/defs.sch 684 */
					BgL_rtl_movz00_bglt BgL_new1381z00_9012;

					BgL_new1381z00_9012 =
						((BgL_rtl_movz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_movz00_bgl))));
					{	/* SawMill/defs.sch 684 */
						long BgL_arg1850z00_9013;

						BgL_arg1850z00_9013 = BGL_CLASS_NUM(BGl_rtl_movz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1381z00_9012), BgL_arg1850z00_9013);
					}
					BgL_new1382z00_9011 = BgL_new1381z00_9012;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1382z00_9011)))->BgL_locz00) =
					((obj_t) BgL_loc1457z00_152), BUNSPEC);
				return BgL_new1382z00_9011;
			}
		}

	}



/* &make-rtl_mov */
	BgL_rtl_movz00_bglt BGl_z62makezd2rtl_movzb0zzsaw_defsz00(obj_t
		BgL_envz00_7182, obj_t BgL_loc1457z00_7183)
	{
		{	/* SawMill/defs.sch 684 */
			return BGl_makezd2rtl_movzd2zzsaw_defsz00(BgL_loc1457z00_7183);
		}

	}



/* rtl_mov? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_movzf3zf3zzsaw_defsz00(obj_t BgL_objz00_153)
	{
		{	/* SawMill/defs.sch 685 */
			{	/* SawMill/defs.sch 685 */
				obj_t BgL_classz00_9014;

				BgL_classz00_9014 = BGl_rtl_movz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_153))
					{	/* SawMill/defs.sch 685 */
						BgL_objectz00_bglt BgL_arg1807z00_9015;

						BgL_arg1807z00_9015 = (BgL_objectz00_bglt) (BgL_objz00_153);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 685 */
								long BgL_idxz00_9016;

								BgL_idxz00_9016 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9015);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9016 + 3L)) == BgL_classz00_9014);
							}
						else
							{	/* SawMill/defs.sch 685 */
								bool_t BgL_res3034z00_9019;

								{	/* SawMill/defs.sch 685 */
									obj_t BgL_oclassz00_9020;

									{	/* SawMill/defs.sch 685 */
										obj_t BgL_arg1815z00_9021;
										long BgL_arg1816z00_9022;

										BgL_arg1815z00_9021 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 685 */
											long BgL_arg1817z00_9023;

											BgL_arg1817z00_9023 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9015);
											BgL_arg1816z00_9022 = (BgL_arg1817z00_9023 - OBJECT_TYPE);
										}
										BgL_oclassz00_9020 =
											VECTOR_REF(BgL_arg1815z00_9021, BgL_arg1816z00_9022);
									}
									{	/* SawMill/defs.sch 685 */
										bool_t BgL__ortest_1115z00_9024;

										BgL__ortest_1115z00_9024 =
											(BgL_classz00_9014 == BgL_oclassz00_9020);
										if (BgL__ortest_1115z00_9024)
											{	/* SawMill/defs.sch 685 */
												BgL_res3034z00_9019 = BgL__ortest_1115z00_9024;
											}
										else
											{	/* SawMill/defs.sch 685 */
												long BgL_odepthz00_9025;

												{	/* SawMill/defs.sch 685 */
													obj_t BgL_arg1804z00_9026;

													BgL_arg1804z00_9026 = (BgL_oclassz00_9020);
													BgL_odepthz00_9025 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9026);
												}
												if ((3L < BgL_odepthz00_9025))
													{	/* SawMill/defs.sch 685 */
														obj_t BgL_arg1802z00_9027;

														{	/* SawMill/defs.sch 685 */
															obj_t BgL_arg1803z00_9028;

															BgL_arg1803z00_9028 = (BgL_oclassz00_9020);
															BgL_arg1802z00_9027 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9028,
																3L);
														}
														BgL_res3034z00_9019 =
															(BgL_arg1802z00_9027 == BgL_classz00_9014);
													}
												else
													{	/* SawMill/defs.sch 685 */
														BgL_res3034z00_9019 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3034z00_9019;
							}
					}
				else
					{	/* SawMill/defs.sch 685 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_mov? */
	obj_t BGl_z62rtl_movzf3z91zzsaw_defsz00(obj_t BgL_envz00_7184,
		obj_t BgL_objz00_7185)
	{
		{	/* SawMill/defs.sch 685 */
			return BBOOL(BGl_rtl_movzf3zf3zzsaw_defsz00(BgL_objz00_7185));
		}

	}



/* rtl_mov-nil */
	BGL_EXPORTED_DEF BgL_rtl_movz00_bglt BGl_rtl_movzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 686 */
			{	/* SawMill/defs.sch 686 */
				obj_t BgL_classz00_4925;

				BgL_classz00_4925 = BGl_rtl_movz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 686 */
					obj_t BgL__ortest_1117z00_4926;

					BgL__ortest_1117z00_4926 = BGL_CLASS_NIL(BgL_classz00_4925);
					if (CBOOL(BgL__ortest_1117z00_4926))
						{	/* SawMill/defs.sch 686 */
							return ((BgL_rtl_movz00_bglt) BgL__ortest_1117z00_4926);
						}
					else
						{	/* SawMill/defs.sch 686 */
							return
								((BgL_rtl_movz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4925));
						}
				}
			}
		}

	}



/* &rtl_mov-nil */
	BgL_rtl_movz00_bglt BGl_z62rtl_movzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7186)
	{
		{	/* SawMill/defs.sch 686 */
			return BGl_rtl_movzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_mov-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_movzd2loczd2zzsaw_defsz00(BgL_rtl_movz00_bglt
		BgL_oz00_154)
	{
		{	/* SawMill/defs.sch 687 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_154)))->BgL_locz00);
		}

	}



/* &rtl_mov-loc */
	obj_t BGl_z62rtl_movzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7187,
		obj_t BgL_oz00_7188)
	{
		{	/* SawMill/defs.sch 687 */
			return
				BGl_rtl_movzd2loczd2zzsaw_defsz00(
				((BgL_rtl_movz00_bglt) BgL_oz00_7188));
		}

	}



/* rtl_mov-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_movzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_movz00_bglt BgL_oz00_155,
		obj_t BgL_vz00_156)
	{
		{	/* SawMill/defs.sch 688 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_155)))->BgL_locz00) =
				((obj_t) BgL_vz00_156), BUNSPEC);
		}

	}



/* &rtl_mov-loc-set! */
	obj_t BGl_z62rtl_movzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7189,
		obj_t BgL_oz00_7190, obj_t BgL_vz00_7191)
	{
		{	/* SawMill/defs.sch 688 */
			return
				BGl_rtl_movzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_movz00_bglt) BgL_oz00_7190), BgL_vz00_7191);
		}

	}



/* make-rtl_loadi */
	BGL_EXPORTED_DEF BgL_rtl_loadiz00_bglt
		BGl_makezd2rtl_loadizd2zzsaw_defsz00(obj_t BgL_loc1452z00_157,
		BgL_atomz00_bglt BgL_constant1453z00_158)
	{
		{	/* SawMill/defs.sch 691 */
			{	/* SawMill/defs.sch 691 */
				BgL_rtl_loadiz00_bglt BgL_new1384z00_9029;

				{	/* SawMill/defs.sch 691 */
					BgL_rtl_loadiz00_bglt BgL_new1383z00_9030;

					BgL_new1383z00_9030 =
						((BgL_rtl_loadiz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_loadiz00_bgl))));
					{	/* SawMill/defs.sch 691 */
						long BgL_arg1851z00_9031;

						BgL_arg1851z00_9031 = BGL_CLASS_NUM(BGl_rtl_loadiz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1383z00_9030), BgL_arg1851z00_9031);
					}
					BgL_new1384z00_9029 = BgL_new1383z00_9030;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1384z00_9029)))->BgL_locz00) =
					((obj_t) BgL_loc1452z00_157), BUNSPEC);
				((((BgL_rtl_loadiz00_bglt) COBJECT(BgL_new1384z00_9029))->
						BgL_constantz00) =
					((BgL_atomz00_bglt) BgL_constant1453z00_158), BUNSPEC);
				return BgL_new1384z00_9029;
			}
		}

	}



/* &make-rtl_loadi */
	BgL_rtl_loadiz00_bglt BGl_z62makezd2rtl_loadizb0zzsaw_defsz00(obj_t
		BgL_envz00_7192, obj_t BgL_loc1452z00_7193, obj_t BgL_constant1453z00_7194)
	{
		{	/* SawMill/defs.sch 691 */
			return
				BGl_makezd2rtl_loadizd2zzsaw_defsz00(BgL_loc1452z00_7193,
				((BgL_atomz00_bglt) BgL_constant1453z00_7194));
		}

	}



/* rtl_loadi? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_loadizf3zf3zzsaw_defsz00(obj_t BgL_objz00_159)
	{
		{	/* SawMill/defs.sch 692 */
			{	/* SawMill/defs.sch 692 */
				obj_t BgL_classz00_9032;

				BgL_classz00_9032 = BGl_rtl_loadiz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_159))
					{	/* SawMill/defs.sch 692 */
						BgL_objectz00_bglt BgL_arg1807z00_9033;

						BgL_arg1807z00_9033 = (BgL_objectz00_bglt) (BgL_objz00_159);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 692 */
								long BgL_idxz00_9034;

								BgL_idxz00_9034 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9033);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9034 + 3L)) == BgL_classz00_9032);
							}
						else
							{	/* SawMill/defs.sch 692 */
								bool_t BgL_res3035z00_9037;

								{	/* SawMill/defs.sch 692 */
									obj_t BgL_oclassz00_9038;

									{	/* SawMill/defs.sch 692 */
										obj_t BgL_arg1815z00_9039;
										long BgL_arg1816z00_9040;

										BgL_arg1815z00_9039 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 692 */
											long BgL_arg1817z00_9041;

											BgL_arg1817z00_9041 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9033);
											BgL_arg1816z00_9040 = (BgL_arg1817z00_9041 - OBJECT_TYPE);
										}
										BgL_oclassz00_9038 =
											VECTOR_REF(BgL_arg1815z00_9039, BgL_arg1816z00_9040);
									}
									{	/* SawMill/defs.sch 692 */
										bool_t BgL__ortest_1115z00_9042;

										BgL__ortest_1115z00_9042 =
											(BgL_classz00_9032 == BgL_oclassz00_9038);
										if (BgL__ortest_1115z00_9042)
											{	/* SawMill/defs.sch 692 */
												BgL_res3035z00_9037 = BgL__ortest_1115z00_9042;
											}
										else
											{	/* SawMill/defs.sch 692 */
												long BgL_odepthz00_9043;

												{	/* SawMill/defs.sch 692 */
													obj_t BgL_arg1804z00_9044;

													BgL_arg1804z00_9044 = (BgL_oclassz00_9038);
													BgL_odepthz00_9043 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9044);
												}
												if ((3L < BgL_odepthz00_9043))
													{	/* SawMill/defs.sch 692 */
														obj_t BgL_arg1802z00_9045;

														{	/* SawMill/defs.sch 692 */
															obj_t BgL_arg1803z00_9046;

															BgL_arg1803z00_9046 = (BgL_oclassz00_9038);
															BgL_arg1802z00_9045 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9046,
																3L);
														}
														BgL_res3035z00_9037 =
															(BgL_arg1802z00_9045 == BgL_classz00_9032);
													}
												else
													{	/* SawMill/defs.sch 692 */
														BgL_res3035z00_9037 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3035z00_9037;
							}
					}
				else
					{	/* SawMill/defs.sch 692 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_loadi? */
	obj_t BGl_z62rtl_loadizf3z91zzsaw_defsz00(obj_t BgL_envz00_7195,
		obj_t BgL_objz00_7196)
	{
		{	/* SawMill/defs.sch 692 */
			return BBOOL(BGl_rtl_loadizf3zf3zzsaw_defsz00(BgL_objz00_7196));
		}

	}



/* rtl_loadi-nil */
	BGL_EXPORTED_DEF BgL_rtl_loadiz00_bglt
		BGl_rtl_loadizd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 693 */
			{	/* SawMill/defs.sch 693 */
				obj_t BgL_classz00_4967;

				BgL_classz00_4967 = BGl_rtl_loadiz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 693 */
					obj_t BgL__ortest_1117z00_4968;

					BgL__ortest_1117z00_4968 = BGL_CLASS_NIL(BgL_classz00_4967);
					if (CBOOL(BgL__ortest_1117z00_4968))
						{	/* SawMill/defs.sch 693 */
							return ((BgL_rtl_loadiz00_bglt) BgL__ortest_1117z00_4968);
						}
					else
						{	/* SawMill/defs.sch 693 */
							return
								((BgL_rtl_loadiz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_4967));
						}
				}
			}
		}

	}



/* &rtl_loadi-nil */
	BgL_rtl_loadiz00_bglt BGl_z62rtl_loadizd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7197)
	{
		{	/* SawMill/defs.sch 693 */
			return BGl_rtl_loadizd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_loadi-constant */
	BGL_EXPORTED_DEF BgL_atomz00_bglt
		BGl_rtl_loadizd2constantzd2zzsaw_defsz00(BgL_rtl_loadiz00_bglt BgL_oz00_160)
	{
		{	/* SawMill/defs.sch 694 */
			return (((BgL_rtl_loadiz00_bglt) COBJECT(BgL_oz00_160))->BgL_constantz00);
		}

	}



/* &rtl_loadi-constant */
	BgL_atomz00_bglt BGl_z62rtl_loadizd2constantzb0zzsaw_defsz00(obj_t
		BgL_envz00_7198, obj_t BgL_oz00_7199)
	{
		{	/* SawMill/defs.sch 694 */
			return
				BGl_rtl_loadizd2constantzd2zzsaw_defsz00(
				((BgL_rtl_loadiz00_bglt) BgL_oz00_7199));
		}

	}



/* rtl_loadi-constant-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_loadizd2constantzd2setz12z12zzsaw_defsz00(BgL_rtl_loadiz00_bglt
		BgL_oz00_161, BgL_atomz00_bglt BgL_vz00_162)
	{
		{	/* SawMill/defs.sch 695 */
			return
				((((BgL_rtl_loadiz00_bglt) COBJECT(BgL_oz00_161))->BgL_constantz00) =
				((BgL_atomz00_bglt) BgL_vz00_162), BUNSPEC);
		}

	}



/* &rtl_loadi-constant-set! */
	obj_t BGl_z62rtl_loadizd2constantzd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7200, obj_t BgL_oz00_7201, obj_t BgL_vz00_7202)
	{
		{	/* SawMill/defs.sch 695 */
			return
				BGl_rtl_loadizd2constantzd2setz12z12zzsaw_defsz00(
				((BgL_rtl_loadiz00_bglt) BgL_oz00_7201),
				((BgL_atomz00_bglt) BgL_vz00_7202));
		}

	}



/* rtl_loadi-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_loadizd2loczd2zzsaw_defsz00(BgL_rtl_loadiz00_bglt BgL_oz00_163)
	{
		{	/* SawMill/defs.sch 696 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_163)))->BgL_locz00);
		}

	}



/* &rtl_loadi-loc */
	obj_t BGl_z62rtl_loadizd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7203,
		obj_t BgL_oz00_7204)
	{
		{	/* SawMill/defs.sch 696 */
			return
				BGl_rtl_loadizd2loczd2zzsaw_defsz00(
				((BgL_rtl_loadiz00_bglt) BgL_oz00_7204));
		}

	}



/* rtl_loadi-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_loadizd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_loadiz00_bglt
		BgL_oz00_164, obj_t BgL_vz00_165)
	{
		{	/* SawMill/defs.sch 697 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_164)))->BgL_locz00) =
				((obj_t) BgL_vz00_165), BUNSPEC);
		}

	}



/* &rtl_loadi-loc-set! */
	obj_t BGl_z62rtl_loadizd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7205,
		obj_t BgL_oz00_7206, obj_t BgL_vz00_7207)
	{
		{	/* SawMill/defs.sch 697 */
			return
				BGl_rtl_loadizd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_loadiz00_bglt) BgL_oz00_7206), BgL_vz00_7207);
		}

	}



/* make-rtl_loadg */
	BGL_EXPORTED_DEF BgL_rtl_loadgz00_bglt
		BGl_makezd2rtl_loadgzd2zzsaw_defsz00(obj_t BgL_loc1449z00_166,
		BgL_globalz00_bglt BgL_var1450z00_167)
	{
		{	/* SawMill/defs.sch 700 */
			{	/* SawMill/defs.sch 700 */
				BgL_rtl_loadgz00_bglt BgL_new1386z00_9047;

				{	/* SawMill/defs.sch 700 */
					BgL_rtl_loadgz00_bglt BgL_new1385z00_9048;

					BgL_new1385z00_9048 =
						((BgL_rtl_loadgz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_loadgz00_bgl))));
					{	/* SawMill/defs.sch 700 */
						long BgL_arg1852z00_9049;

						BgL_arg1852z00_9049 = BGL_CLASS_NUM(BGl_rtl_loadgz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1385z00_9048), BgL_arg1852z00_9049);
					}
					BgL_new1386z00_9047 = BgL_new1385z00_9048;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1386z00_9047)))->BgL_locz00) =
					((obj_t) BgL_loc1449z00_166), BUNSPEC);
				((((BgL_rtl_loadgz00_bglt) COBJECT(BgL_new1386z00_9047))->BgL_varz00) =
					((BgL_globalz00_bglt) BgL_var1450z00_167), BUNSPEC);
				return BgL_new1386z00_9047;
			}
		}

	}



/* &make-rtl_loadg */
	BgL_rtl_loadgz00_bglt BGl_z62makezd2rtl_loadgzb0zzsaw_defsz00(obj_t
		BgL_envz00_7208, obj_t BgL_loc1449z00_7209, obj_t BgL_var1450z00_7210)
	{
		{	/* SawMill/defs.sch 700 */
			return
				BGl_makezd2rtl_loadgzd2zzsaw_defsz00(BgL_loc1449z00_7209,
				((BgL_globalz00_bglt) BgL_var1450z00_7210));
		}

	}



/* rtl_loadg? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_loadgzf3zf3zzsaw_defsz00(obj_t BgL_objz00_168)
	{
		{	/* SawMill/defs.sch 701 */
			{	/* SawMill/defs.sch 701 */
				obj_t BgL_classz00_9050;

				BgL_classz00_9050 = BGl_rtl_loadgz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_168))
					{	/* SawMill/defs.sch 701 */
						BgL_objectz00_bglt BgL_arg1807z00_9051;

						BgL_arg1807z00_9051 = (BgL_objectz00_bglt) (BgL_objz00_168);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 701 */
								long BgL_idxz00_9052;

								BgL_idxz00_9052 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9051);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9052 + 3L)) == BgL_classz00_9050);
							}
						else
							{	/* SawMill/defs.sch 701 */
								bool_t BgL_res3036z00_9055;

								{	/* SawMill/defs.sch 701 */
									obj_t BgL_oclassz00_9056;

									{	/* SawMill/defs.sch 701 */
										obj_t BgL_arg1815z00_9057;
										long BgL_arg1816z00_9058;

										BgL_arg1815z00_9057 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 701 */
											long BgL_arg1817z00_9059;

											BgL_arg1817z00_9059 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9051);
											BgL_arg1816z00_9058 = (BgL_arg1817z00_9059 - OBJECT_TYPE);
										}
										BgL_oclassz00_9056 =
											VECTOR_REF(BgL_arg1815z00_9057, BgL_arg1816z00_9058);
									}
									{	/* SawMill/defs.sch 701 */
										bool_t BgL__ortest_1115z00_9060;

										BgL__ortest_1115z00_9060 =
											(BgL_classz00_9050 == BgL_oclassz00_9056);
										if (BgL__ortest_1115z00_9060)
											{	/* SawMill/defs.sch 701 */
												BgL_res3036z00_9055 = BgL__ortest_1115z00_9060;
											}
										else
											{	/* SawMill/defs.sch 701 */
												long BgL_odepthz00_9061;

												{	/* SawMill/defs.sch 701 */
													obj_t BgL_arg1804z00_9062;

													BgL_arg1804z00_9062 = (BgL_oclassz00_9056);
													BgL_odepthz00_9061 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9062);
												}
												if ((3L < BgL_odepthz00_9061))
													{	/* SawMill/defs.sch 701 */
														obj_t BgL_arg1802z00_9063;

														{	/* SawMill/defs.sch 701 */
															obj_t BgL_arg1803z00_9064;

															BgL_arg1803z00_9064 = (BgL_oclassz00_9056);
															BgL_arg1802z00_9063 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9064,
																3L);
														}
														BgL_res3036z00_9055 =
															(BgL_arg1802z00_9063 == BgL_classz00_9050);
													}
												else
													{	/* SawMill/defs.sch 701 */
														BgL_res3036z00_9055 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3036z00_9055;
							}
					}
				else
					{	/* SawMill/defs.sch 701 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_loadg? */
	obj_t BGl_z62rtl_loadgzf3z91zzsaw_defsz00(obj_t BgL_envz00_7211,
		obj_t BgL_objz00_7212)
	{
		{	/* SawMill/defs.sch 701 */
			return BBOOL(BGl_rtl_loadgzf3zf3zzsaw_defsz00(BgL_objz00_7212));
		}

	}



/* rtl_loadg-nil */
	BGL_EXPORTED_DEF BgL_rtl_loadgz00_bglt
		BGl_rtl_loadgzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 702 */
			{	/* SawMill/defs.sch 702 */
				obj_t BgL_classz00_5009;

				BgL_classz00_5009 = BGl_rtl_loadgz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 702 */
					obj_t BgL__ortest_1117z00_5010;

					BgL__ortest_1117z00_5010 = BGL_CLASS_NIL(BgL_classz00_5009);
					if (CBOOL(BgL__ortest_1117z00_5010))
						{	/* SawMill/defs.sch 702 */
							return ((BgL_rtl_loadgz00_bglt) BgL__ortest_1117z00_5010);
						}
					else
						{	/* SawMill/defs.sch 702 */
							return
								((BgL_rtl_loadgz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5009));
						}
				}
			}
		}

	}



/* &rtl_loadg-nil */
	BgL_rtl_loadgz00_bglt BGl_z62rtl_loadgzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7213)
	{
		{	/* SawMill/defs.sch 702 */
			return BGl_rtl_loadgzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_loadg-var */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_rtl_loadgzd2varzd2zzsaw_defsz00(BgL_rtl_loadgz00_bglt BgL_oz00_169)
	{
		{	/* SawMill/defs.sch 703 */
			return (((BgL_rtl_loadgz00_bglt) COBJECT(BgL_oz00_169))->BgL_varz00);
		}

	}



/* &rtl_loadg-var */
	BgL_globalz00_bglt BGl_z62rtl_loadgzd2varzb0zzsaw_defsz00(obj_t
		BgL_envz00_7214, obj_t BgL_oz00_7215)
	{
		{	/* SawMill/defs.sch 703 */
			return
				BGl_rtl_loadgzd2varzd2zzsaw_defsz00(
				((BgL_rtl_loadgz00_bglt) BgL_oz00_7215));
		}

	}



/* rtl_loadg-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_loadgzd2varzd2setz12z12zzsaw_defsz00(BgL_rtl_loadgz00_bglt
		BgL_oz00_170, BgL_globalz00_bglt BgL_vz00_171)
	{
		{	/* SawMill/defs.sch 704 */
			return
				((((BgL_rtl_loadgz00_bglt) COBJECT(BgL_oz00_170))->BgL_varz00) =
				((BgL_globalz00_bglt) BgL_vz00_171), BUNSPEC);
		}

	}



/* &rtl_loadg-var-set! */
	obj_t BGl_z62rtl_loadgzd2varzd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7216,
		obj_t BgL_oz00_7217, obj_t BgL_vz00_7218)
	{
		{	/* SawMill/defs.sch 704 */
			return
				BGl_rtl_loadgzd2varzd2setz12z12zzsaw_defsz00(
				((BgL_rtl_loadgz00_bglt) BgL_oz00_7217),
				((BgL_globalz00_bglt) BgL_vz00_7218));
		}

	}



/* rtl_loadg-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_loadgzd2loczd2zzsaw_defsz00(BgL_rtl_loadgz00_bglt BgL_oz00_172)
	{
		{	/* SawMill/defs.sch 705 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_172)))->BgL_locz00);
		}

	}



/* &rtl_loadg-loc */
	obj_t BGl_z62rtl_loadgzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7219,
		obj_t BgL_oz00_7220)
	{
		{	/* SawMill/defs.sch 705 */
			return
				BGl_rtl_loadgzd2loczd2zzsaw_defsz00(
				((BgL_rtl_loadgz00_bglt) BgL_oz00_7220));
		}

	}



/* rtl_loadg-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_loadgzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_loadgz00_bglt
		BgL_oz00_173, obj_t BgL_vz00_174)
	{
		{	/* SawMill/defs.sch 706 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_173)))->BgL_locz00) =
				((obj_t) BgL_vz00_174), BUNSPEC);
		}

	}



/* &rtl_loadg-loc-set! */
	obj_t BGl_z62rtl_loadgzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7221,
		obj_t BgL_oz00_7222, obj_t BgL_vz00_7223)
	{
		{	/* SawMill/defs.sch 706 */
			return
				BGl_rtl_loadgzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_loadgz00_bglt) BgL_oz00_7222), BgL_vz00_7223);
		}

	}



/* make-rtl_loadfun */
	BGL_EXPORTED_DEF BgL_rtl_loadfunz00_bglt
		BGl_makezd2rtl_loadfunzd2zzsaw_defsz00(obj_t BgL_loc1444z00_175,
		BgL_globalz00_bglt BgL_var1446z00_176)
	{
		{	/* SawMill/defs.sch 709 */
			{	/* SawMill/defs.sch 709 */
				BgL_rtl_loadfunz00_bglt BgL_new1388z00_9065;

				{	/* SawMill/defs.sch 709 */
					BgL_rtl_loadfunz00_bglt BgL_new1387z00_9066;

					BgL_new1387z00_9066 =
						((BgL_rtl_loadfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_loadfunz00_bgl))));
					{	/* SawMill/defs.sch 709 */
						long BgL_arg1853z00_9067;

						BgL_arg1853z00_9067 =
							BGL_CLASS_NUM(BGl_rtl_loadfunz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1387z00_9066), BgL_arg1853z00_9067);
					}
					BgL_new1388z00_9065 = BgL_new1387z00_9066;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1388z00_9065)))->BgL_locz00) =
					((obj_t) BgL_loc1444z00_175), BUNSPEC);
				((((BgL_rtl_loadfunz00_bglt) COBJECT(BgL_new1388z00_9065))->
						BgL_varz00) = ((BgL_globalz00_bglt) BgL_var1446z00_176), BUNSPEC);
				return BgL_new1388z00_9065;
			}
		}

	}



/* &make-rtl_loadfun */
	BgL_rtl_loadfunz00_bglt BGl_z62makezd2rtl_loadfunzb0zzsaw_defsz00(obj_t
		BgL_envz00_7224, obj_t BgL_loc1444z00_7225, obj_t BgL_var1446z00_7226)
	{
		{	/* SawMill/defs.sch 709 */
			return
				BGl_makezd2rtl_loadfunzd2zzsaw_defsz00(BgL_loc1444z00_7225,
				((BgL_globalz00_bglt) BgL_var1446z00_7226));
		}

	}



/* rtl_loadfun? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_loadfunzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_177)
	{
		{	/* SawMill/defs.sch 710 */
			{	/* SawMill/defs.sch 710 */
				obj_t BgL_classz00_9068;

				BgL_classz00_9068 = BGl_rtl_loadfunz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_177))
					{	/* SawMill/defs.sch 710 */
						BgL_objectz00_bglt BgL_arg1807z00_9069;

						BgL_arg1807z00_9069 = (BgL_objectz00_bglt) (BgL_objz00_177);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 710 */
								long BgL_idxz00_9070;

								BgL_idxz00_9070 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9069);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9070 + 3L)) == BgL_classz00_9068);
							}
						else
							{	/* SawMill/defs.sch 710 */
								bool_t BgL_res3037z00_9073;

								{	/* SawMill/defs.sch 710 */
									obj_t BgL_oclassz00_9074;

									{	/* SawMill/defs.sch 710 */
										obj_t BgL_arg1815z00_9075;
										long BgL_arg1816z00_9076;

										BgL_arg1815z00_9075 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 710 */
											long BgL_arg1817z00_9077;

											BgL_arg1817z00_9077 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9069);
											BgL_arg1816z00_9076 = (BgL_arg1817z00_9077 - OBJECT_TYPE);
										}
										BgL_oclassz00_9074 =
											VECTOR_REF(BgL_arg1815z00_9075, BgL_arg1816z00_9076);
									}
									{	/* SawMill/defs.sch 710 */
										bool_t BgL__ortest_1115z00_9078;

										BgL__ortest_1115z00_9078 =
											(BgL_classz00_9068 == BgL_oclassz00_9074);
										if (BgL__ortest_1115z00_9078)
											{	/* SawMill/defs.sch 710 */
												BgL_res3037z00_9073 = BgL__ortest_1115z00_9078;
											}
										else
											{	/* SawMill/defs.sch 710 */
												long BgL_odepthz00_9079;

												{	/* SawMill/defs.sch 710 */
													obj_t BgL_arg1804z00_9080;

													BgL_arg1804z00_9080 = (BgL_oclassz00_9074);
													BgL_odepthz00_9079 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9080);
												}
												if ((3L < BgL_odepthz00_9079))
													{	/* SawMill/defs.sch 710 */
														obj_t BgL_arg1802z00_9081;

														{	/* SawMill/defs.sch 710 */
															obj_t BgL_arg1803z00_9082;

															BgL_arg1803z00_9082 = (BgL_oclassz00_9074);
															BgL_arg1802z00_9081 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9082,
																3L);
														}
														BgL_res3037z00_9073 =
															(BgL_arg1802z00_9081 == BgL_classz00_9068);
													}
												else
													{	/* SawMill/defs.sch 710 */
														BgL_res3037z00_9073 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3037z00_9073;
							}
					}
				else
					{	/* SawMill/defs.sch 710 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_loadfun? */
	obj_t BGl_z62rtl_loadfunzf3z91zzsaw_defsz00(obj_t BgL_envz00_7227,
		obj_t BgL_objz00_7228)
	{
		{	/* SawMill/defs.sch 710 */
			return BBOOL(BGl_rtl_loadfunzf3zf3zzsaw_defsz00(BgL_objz00_7228));
		}

	}



/* rtl_loadfun-nil */
	BGL_EXPORTED_DEF BgL_rtl_loadfunz00_bglt
		BGl_rtl_loadfunzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 711 */
			{	/* SawMill/defs.sch 711 */
				obj_t BgL_classz00_5051;

				BgL_classz00_5051 = BGl_rtl_loadfunz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 711 */
					obj_t BgL__ortest_1117z00_5052;

					BgL__ortest_1117z00_5052 = BGL_CLASS_NIL(BgL_classz00_5051);
					if (CBOOL(BgL__ortest_1117z00_5052))
						{	/* SawMill/defs.sch 711 */
							return ((BgL_rtl_loadfunz00_bglt) BgL__ortest_1117z00_5052);
						}
					else
						{	/* SawMill/defs.sch 711 */
							return
								((BgL_rtl_loadfunz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5051));
						}
				}
			}
		}

	}



/* &rtl_loadfun-nil */
	BgL_rtl_loadfunz00_bglt BGl_z62rtl_loadfunzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7229)
	{
		{	/* SawMill/defs.sch 711 */
			return BGl_rtl_loadfunzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_loadfun-var */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_rtl_loadfunzd2varzd2zzsaw_defsz00(BgL_rtl_loadfunz00_bglt BgL_oz00_178)
	{
		{	/* SawMill/defs.sch 712 */
			return (((BgL_rtl_loadfunz00_bglt) COBJECT(BgL_oz00_178))->BgL_varz00);
		}

	}



/* &rtl_loadfun-var */
	BgL_globalz00_bglt BGl_z62rtl_loadfunzd2varzb0zzsaw_defsz00(obj_t
		BgL_envz00_7230, obj_t BgL_oz00_7231)
	{
		{	/* SawMill/defs.sch 712 */
			return
				BGl_rtl_loadfunzd2varzd2zzsaw_defsz00(
				((BgL_rtl_loadfunz00_bglt) BgL_oz00_7231));
		}

	}



/* rtl_loadfun-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_loadfunzd2varzd2setz12z12zzsaw_defsz00(BgL_rtl_loadfunz00_bglt
		BgL_oz00_179, BgL_globalz00_bglt BgL_vz00_180)
	{
		{	/* SawMill/defs.sch 713 */
			return
				((((BgL_rtl_loadfunz00_bglt) COBJECT(BgL_oz00_179))->BgL_varz00) =
				((BgL_globalz00_bglt) BgL_vz00_180), BUNSPEC);
		}

	}



/* &rtl_loadfun-var-set! */
	obj_t BGl_z62rtl_loadfunzd2varzd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7232,
		obj_t BgL_oz00_7233, obj_t BgL_vz00_7234)
	{
		{	/* SawMill/defs.sch 713 */
			return
				BGl_rtl_loadfunzd2varzd2setz12z12zzsaw_defsz00(
				((BgL_rtl_loadfunz00_bglt) BgL_oz00_7233),
				((BgL_globalz00_bglt) BgL_vz00_7234));
		}

	}



/* rtl_loadfun-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_loadfunzd2loczd2zzsaw_defsz00(BgL_rtl_loadfunz00_bglt BgL_oz00_181)
	{
		{	/* SawMill/defs.sch 714 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_181)))->BgL_locz00);
		}

	}



/* &rtl_loadfun-loc */
	obj_t BGl_z62rtl_loadfunzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7235,
		obj_t BgL_oz00_7236)
	{
		{	/* SawMill/defs.sch 714 */
			return
				BGl_rtl_loadfunzd2loczd2zzsaw_defsz00(
				((BgL_rtl_loadfunz00_bglt) BgL_oz00_7236));
		}

	}



/* rtl_loadfun-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_loadfunzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_loadfunz00_bglt
		BgL_oz00_182, obj_t BgL_vz00_183)
	{
		{	/* SawMill/defs.sch 715 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_182)))->BgL_locz00) =
				((obj_t) BgL_vz00_183), BUNSPEC);
		}

	}



/* &rtl_loadfun-loc-set! */
	obj_t BGl_z62rtl_loadfunzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7237,
		obj_t BgL_oz00_7238, obj_t BgL_vz00_7239)
	{
		{	/* SawMill/defs.sch 715 */
			return
				BGl_rtl_loadfunzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_loadfunz00_bglt) BgL_oz00_7238), BgL_vz00_7239);
		}

	}



/* make-rtl_globalref */
	BGL_EXPORTED_DEF BgL_rtl_globalrefz00_bglt
		BGl_makezd2rtl_globalrefzd2zzsaw_defsz00(obj_t BgL_loc1439z00_184,
		BgL_globalz00_bglt BgL_var1440z00_185)
	{
		{	/* SawMill/defs.sch 718 */
			{	/* SawMill/defs.sch 718 */
				BgL_rtl_globalrefz00_bglt BgL_new1390z00_9083;

				{	/* SawMill/defs.sch 718 */
					BgL_rtl_globalrefz00_bglt BgL_new1389z00_9084;

					BgL_new1389z00_9084 =
						((BgL_rtl_globalrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_globalrefz00_bgl))));
					{	/* SawMill/defs.sch 718 */
						long BgL_arg1854z00_9085;

						BgL_arg1854z00_9085 =
							BGL_CLASS_NUM(BGl_rtl_globalrefz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1389z00_9084), BgL_arg1854z00_9085);
					}
					BgL_new1390z00_9083 = BgL_new1389z00_9084;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1390z00_9083)))->BgL_locz00) =
					((obj_t) BgL_loc1439z00_184), BUNSPEC);
				((((BgL_rtl_globalrefz00_bglt) COBJECT(BgL_new1390z00_9083))->
						BgL_varz00) = ((BgL_globalz00_bglt) BgL_var1440z00_185), BUNSPEC);
				return BgL_new1390z00_9083;
			}
		}

	}



/* &make-rtl_globalref */
	BgL_rtl_globalrefz00_bglt BGl_z62makezd2rtl_globalrefzb0zzsaw_defsz00(obj_t
		BgL_envz00_7240, obj_t BgL_loc1439z00_7241, obj_t BgL_var1440z00_7242)
	{
		{	/* SawMill/defs.sch 718 */
			return
				BGl_makezd2rtl_globalrefzd2zzsaw_defsz00(BgL_loc1439z00_7241,
				((BgL_globalz00_bglt) BgL_var1440z00_7242));
		}

	}



/* rtl_globalref? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_globalrefzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_186)
	{
		{	/* SawMill/defs.sch 719 */
			{	/* SawMill/defs.sch 719 */
				obj_t BgL_classz00_9086;

				BgL_classz00_9086 = BGl_rtl_globalrefz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_186))
					{	/* SawMill/defs.sch 719 */
						BgL_objectz00_bglt BgL_arg1807z00_9087;

						BgL_arg1807z00_9087 = (BgL_objectz00_bglt) (BgL_objz00_186);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 719 */
								long BgL_idxz00_9088;

								BgL_idxz00_9088 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9087);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9088 + 3L)) == BgL_classz00_9086);
							}
						else
							{	/* SawMill/defs.sch 719 */
								bool_t BgL_res3038z00_9091;

								{	/* SawMill/defs.sch 719 */
									obj_t BgL_oclassz00_9092;

									{	/* SawMill/defs.sch 719 */
										obj_t BgL_arg1815z00_9093;
										long BgL_arg1816z00_9094;

										BgL_arg1815z00_9093 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 719 */
											long BgL_arg1817z00_9095;

											BgL_arg1817z00_9095 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9087);
											BgL_arg1816z00_9094 = (BgL_arg1817z00_9095 - OBJECT_TYPE);
										}
										BgL_oclassz00_9092 =
											VECTOR_REF(BgL_arg1815z00_9093, BgL_arg1816z00_9094);
									}
									{	/* SawMill/defs.sch 719 */
										bool_t BgL__ortest_1115z00_9096;

										BgL__ortest_1115z00_9096 =
											(BgL_classz00_9086 == BgL_oclassz00_9092);
										if (BgL__ortest_1115z00_9096)
											{	/* SawMill/defs.sch 719 */
												BgL_res3038z00_9091 = BgL__ortest_1115z00_9096;
											}
										else
											{	/* SawMill/defs.sch 719 */
												long BgL_odepthz00_9097;

												{	/* SawMill/defs.sch 719 */
													obj_t BgL_arg1804z00_9098;

													BgL_arg1804z00_9098 = (BgL_oclassz00_9092);
													BgL_odepthz00_9097 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9098);
												}
												if ((3L < BgL_odepthz00_9097))
													{	/* SawMill/defs.sch 719 */
														obj_t BgL_arg1802z00_9099;

														{	/* SawMill/defs.sch 719 */
															obj_t BgL_arg1803z00_9100;

															BgL_arg1803z00_9100 = (BgL_oclassz00_9092);
															BgL_arg1802z00_9099 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9100,
																3L);
														}
														BgL_res3038z00_9091 =
															(BgL_arg1802z00_9099 == BgL_classz00_9086);
													}
												else
													{	/* SawMill/defs.sch 719 */
														BgL_res3038z00_9091 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3038z00_9091;
							}
					}
				else
					{	/* SawMill/defs.sch 719 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_globalref? */
	obj_t BGl_z62rtl_globalrefzf3z91zzsaw_defsz00(obj_t BgL_envz00_7243,
		obj_t BgL_objz00_7244)
	{
		{	/* SawMill/defs.sch 719 */
			return BBOOL(BGl_rtl_globalrefzf3zf3zzsaw_defsz00(BgL_objz00_7244));
		}

	}



/* rtl_globalref-nil */
	BGL_EXPORTED_DEF BgL_rtl_globalrefz00_bglt
		BGl_rtl_globalrefzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 720 */
			{	/* SawMill/defs.sch 720 */
				obj_t BgL_classz00_5093;

				BgL_classz00_5093 = BGl_rtl_globalrefz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 720 */
					obj_t BgL__ortest_1117z00_5094;

					BgL__ortest_1117z00_5094 = BGL_CLASS_NIL(BgL_classz00_5093);
					if (CBOOL(BgL__ortest_1117z00_5094))
						{	/* SawMill/defs.sch 720 */
							return ((BgL_rtl_globalrefz00_bglt) BgL__ortest_1117z00_5094);
						}
					else
						{	/* SawMill/defs.sch 720 */
							return
								((BgL_rtl_globalrefz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5093));
						}
				}
			}
		}

	}



/* &rtl_globalref-nil */
	BgL_rtl_globalrefz00_bglt BGl_z62rtl_globalrefzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7245)
	{
		{	/* SawMill/defs.sch 720 */
			return BGl_rtl_globalrefzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_globalref-var */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_rtl_globalrefzd2varzd2zzsaw_defsz00(BgL_rtl_globalrefz00_bglt
		BgL_oz00_187)
	{
		{	/* SawMill/defs.sch 721 */
			return (((BgL_rtl_globalrefz00_bglt) COBJECT(BgL_oz00_187))->BgL_varz00);
		}

	}



/* &rtl_globalref-var */
	BgL_globalz00_bglt BGl_z62rtl_globalrefzd2varzb0zzsaw_defsz00(obj_t
		BgL_envz00_7246, obj_t BgL_oz00_7247)
	{
		{	/* SawMill/defs.sch 721 */
			return
				BGl_rtl_globalrefzd2varzd2zzsaw_defsz00(
				((BgL_rtl_globalrefz00_bglt) BgL_oz00_7247));
		}

	}



/* rtl_globalref-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_globalrefzd2varzd2setz12z12zzsaw_defsz00(BgL_rtl_globalrefz00_bglt
		BgL_oz00_188, BgL_globalz00_bglt BgL_vz00_189)
	{
		{	/* SawMill/defs.sch 722 */
			return
				((((BgL_rtl_globalrefz00_bglt) COBJECT(BgL_oz00_188))->BgL_varz00) =
				((BgL_globalz00_bglt) BgL_vz00_189), BUNSPEC);
		}

	}



/* &rtl_globalref-var-set! */
	obj_t BGl_z62rtl_globalrefzd2varzd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7248, obj_t BgL_oz00_7249, obj_t BgL_vz00_7250)
	{
		{	/* SawMill/defs.sch 722 */
			return
				BGl_rtl_globalrefzd2varzd2setz12z12zzsaw_defsz00(
				((BgL_rtl_globalrefz00_bglt) BgL_oz00_7249),
				((BgL_globalz00_bglt) BgL_vz00_7250));
		}

	}



/* rtl_globalref-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_globalrefzd2loczd2zzsaw_defsz00(BgL_rtl_globalrefz00_bglt
		BgL_oz00_190)
	{
		{	/* SawMill/defs.sch 723 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_190)))->BgL_locz00);
		}

	}



/* &rtl_globalref-loc */
	obj_t BGl_z62rtl_globalrefzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7251,
		obj_t BgL_oz00_7252)
	{
		{	/* SawMill/defs.sch 723 */
			return
				BGl_rtl_globalrefzd2loczd2zzsaw_defsz00(
				((BgL_rtl_globalrefz00_bglt) BgL_oz00_7252));
		}

	}



/* rtl_globalref-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_globalrefzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_globalrefz00_bglt
		BgL_oz00_191, obj_t BgL_vz00_192)
	{
		{	/* SawMill/defs.sch 724 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_191)))->BgL_locz00) =
				((obj_t) BgL_vz00_192), BUNSPEC);
		}

	}



/* &rtl_globalref-loc-set! */
	obj_t BGl_z62rtl_globalrefzd2loczd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7253, obj_t BgL_oz00_7254, obj_t BgL_vz00_7255)
	{
		{	/* SawMill/defs.sch 724 */
			return
				BGl_rtl_globalrefzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_globalrefz00_bglt) BgL_oz00_7254), BgL_vz00_7255);
		}

	}



/* make-rtl_getfield */
	BGL_EXPORTED_DEF BgL_rtl_getfieldz00_bglt
		BGl_makezd2rtl_getfieldzd2zzsaw_defsz00(obj_t BgL_loc1433z00_193,
		obj_t BgL_name1434z00_194, BgL_typez00_bglt BgL_objtype1435z00_195,
		BgL_typez00_bglt BgL_type1436z00_196)
	{
		{	/* SawMill/defs.sch 727 */
			{	/* SawMill/defs.sch 727 */
				BgL_rtl_getfieldz00_bglt BgL_new1392z00_9101;

				{	/* SawMill/defs.sch 727 */
					BgL_rtl_getfieldz00_bglt BgL_new1391z00_9102;

					BgL_new1391z00_9102 =
						((BgL_rtl_getfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_getfieldz00_bgl))));
					{	/* SawMill/defs.sch 727 */
						long BgL_arg1856z00_9103;

						BgL_arg1856z00_9103 =
							BGL_CLASS_NUM(BGl_rtl_getfieldz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1391z00_9102), BgL_arg1856z00_9103);
					}
					BgL_new1392z00_9101 = BgL_new1391z00_9102;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1392z00_9101)))->BgL_locz00) =
					((obj_t) BgL_loc1433z00_193), BUNSPEC);
				((((BgL_rtl_getfieldz00_bglt) COBJECT(BgL_new1392z00_9101))->
						BgL_namez00) = ((obj_t) BgL_name1434z00_194), BUNSPEC);
				((((BgL_rtl_getfieldz00_bglt) COBJECT(BgL_new1392z00_9101))->
						BgL_objtypez00) =
					((BgL_typez00_bglt) BgL_objtype1435z00_195), BUNSPEC);
				((((BgL_rtl_getfieldz00_bglt) COBJECT(BgL_new1392z00_9101))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1436z00_196), BUNSPEC);
				return BgL_new1392z00_9101;
			}
		}

	}



/* &make-rtl_getfield */
	BgL_rtl_getfieldz00_bglt BGl_z62makezd2rtl_getfieldzb0zzsaw_defsz00(obj_t
		BgL_envz00_7256, obj_t BgL_loc1433z00_7257, obj_t BgL_name1434z00_7258,
		obj_t BgL_objtype1435z00_7259, obj_t BgL_type1436z00_7260)
	{
		{	/* SawMill/defs.sch 727 */
			return
				BGl_makezd2rtl_getfieldzd2zzsaw_defsz00(BgL_loc1433z00_7257,
				BgL_name1434z00_7258, ((BgL_typez00_bglt) BgL_objtype1435z00_7259),
				((BgL_typez00_bglt) BgL_type1436z00_7260));
		}

	}



/* rtl_getfield? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_getfieldzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_197)
	{
		{	/* SawMill/defs.sch 728 */
			{	/* SawMill/defs.sch 728 */
				obj_t BgL_classz00_9104;

				BgL_classz00_9104 = BGl_rtl_getfieldz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_197))
					{	/* SawMill/defs.sch 728 */
						BgL_objectz00_bglt BgL_arg1807z00_9105;

						BgL_arg1807z00_9105 = (BgL_objectz00_bglt) (BgL_objz00_197);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 728 */
								long BgL_idxz00_9106;

								BgL_idxz00_9106 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9105);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9106 + 3L)) == BgL_classz00_9104);
							}
						else
							{	/* SawMill/defs.sch 728 */
								bool_t BgL_res3039z00_9109;

								{	/* SawMill/defs.sch 728 */
									obj_t BgL_oclassz00_9110;

									{	/* SawMill/defs.sch 728 */
										obj_t BgL_arg1815z00_9111;
										long BgL_arg1816z00_9112;

										BgL_arg1815z00_9111 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 728 */
											long BgL_arg1817z00_9113;

											BgL_arg1817z00_9113 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9105);
											BgL_arg1816z00_9112 = (BgL_arg1817z00_9113 - OBJECT_TYPE);
										}
										BgL_oclassz00_9110 =
											VECTOR_REF(BgL_arg1815z00_9111, BgL_arg1816z00_9112);
									}
									{	/* SawMill/defs.sch 728 */
										bool_t BgL__ortest_1115z00_9114;

										BgL__ortest_1115z00_9114 =
											(BgL_classz00_9104 == BgL_oclassz00_9110);
										if (BgL__ortest_1115z00_9114)
											{	/* SawMill/defs.sch 728 */
												BgL_res3039z00_9109 = BgL__ortest_1115z00_9114;
											}
										else
											{	/* SawMill/defs.sch 728 */
												long BgL_odepthz00_9115;

												{	/* SawMill/defs.sch 728 */
													obj_t BgL_arg1804z00_9116;

													BgL_arg1804z00_9116 = (BgL_oclassz00_9110);
													BgL_odepthz00_9115 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9116);
												}
												if ((3L < BgL_odepthz00_9115))
													{	/* SawMill/defs.sch 728 */
														obj_t BgL_arg1802z00_9117;

														{	/* SawMill/defs.sch 728 */
															obj_t BgL_arg1803z00_9118;

															BgL_arg1803z00_9118 = (BgL_oclassz00_9110);
															BgL_arg1802z00_9117 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9118,
																3L);
														}
														BgL_res3039z00_9109 =
															(BgL_arg1802z00_9117 == BgL_classz00_9104);
													}
												else
													{	/* SawMill/defs.sch 728 */
														BgL_res3039z00_9109 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3039z00_9109;
							}
					}
				else
					{	/* SawMill/defs.sch 728 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_getfield? */
	obj_t BGl_z62rtl_getfieldzf3z91zzsaw_defsz00(obj_t BgL_envz00_7261,
		obj_t BgL_objz00_7262)
	{
		{	/* SawMill/defs.sch 728 */
			return BBOOL(BGl_rtl_getfieldzf3zf3zzsaw_defsz00(BgL_objz00_7262));
		}

	}



/* rtl_getfield-nil */
	BGL_EXPORTED_DEF BgL_rtl_getfieldz00_bglt
		BGl_rtl_getfieldzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 729 */
			{	/* SawMill/defs.sch 729 */
				obj_t BgL_classz00_5135;

				BgL_classz00_5135 = BGl_rtl_getfieldz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 729 */
					obj_t BgL__ortest_1117z00_5136;

					BgL__ortest_1117z00_5136 = BGL_CLASS_NIL(BgL_classz00_5135);
					if (CBOOL(BgL__ortest_1117z00_5136))
						{	/* SawMill/defs.sch 729 */
							return ((BgL_rtl_getfieldz00_bglt) BgL__ortest_1117z00_5136);
						}
					else
						{	/* SawMill/defs.sch 729 */
							return
								((BgL_rtl_getfieldz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5135));
						}
				}
			}
		}

	}



/* &rtl_getfield-nil */
	BgL_rtl_getfieldz00_bglt BGl_z62rtl_getfieldzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7263)
	{
		{	/* SawMill/defs.sch 729 */
			return BGl_rtl_getfieldzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_getfield-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_getfieldzd2typezd2zzsaw_defsz00(BgL_rtl_getfieldz00_bglt
		BgL_oz00_198)
	{
		{	/* SawMill/defs.sch 730 */
			return (((BgL_rtl_getfieldz00_bglt) COBJECT(BgL_oz00_198))->BgL_typez00);
		}

	}



/* &rtl_getfield-type */
	BgL_typez00_bglt BGl_z62rtl_getfieldzd2typezb0zzsaw_defsz00(obj_t
		BgL_envz00_7264, obj_t BgL_oz00_7265)
	{
		{	/* SawMill/defs.sch 730 */
			return
				BGl_rtl_getfieldzd2typezd2zzsaw_defsz00(
				((BgL_rtl_getfieldz00_bglt) BgL_oz00_7265));
		}

	}



/* rtl_getfield-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_getfieldzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_getfieldz00_bglt
		BgL_oz00_199, BgL_typez00_bglt BgL_vz00_200)
	{
		{	/* SawMill/defs.sch 731 */
			return
				((((BgL_rtl_getfieldz00_bglt) COBJECT(BgL_oz00_199))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_200), BUNSPEC);
		}

	}



/* &rtl_getfield-type-set! */
	obj_t BGl_z62rtl_getfieldzd2typezd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7266, obj_t BgL_oz00_7267, obj_t BgL_vz00_7268)
	{
		{	/* SawMill/defs.sch 731 */
			return
				BGl_rtl_getfieldzd2typezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_getfieldz00_bglt) BgL_oz00_7267),
				((BgL_typez00_bglt) BgL_vz00_7268));
		}

	}



/* rtl_getfield-objtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_getfieldzd2objtypezd2zzsaw_defsz00(BgL_rtl_getfieldz00_bglt
		BgL_oz00_201)
	{
		{	/* SawMill/defs.sch 732 */
			return
				(((BgL_rtl_getfieldz00_bglt) COBJECT(BgL_oz00_201))->BgL_objtypez00);
		}

	}



/* &rtl_getfield-objtype */
	BgL_typez00_bglt BGl_z62rtl_getfieldzd2objtypezb0zzsaw_defsz00(obj_t
		BgL_envz00_7269, obj_t BgL_oz00_7270)
	{
		{	/* SawMill/defs.sch 732 */
			return
				BGl_rtl_getfieldzd2objtypezd2zzsaw_defsz00(
				((BgL_rtl_getfieldz00_bglt) BgL_oz00_7270));
		}

	}



/* rtl_getfield-objtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_getfieldzd2objtypezd2setz12z12zzsaw_defsz00(BgL_rtl_getfieldz00_bglt
		BgL_oz00_202, BgL_typez00_bglt BgL_vz00_203)
	{
		{	/* SawMill/defs.sch 733 */
			return
				((((BgL_rtl_getfieldz00_bglt) COBJECT(BgL_oz00_202))->BgL_objtypez00) =
				((BgL_typez00_bglt) BgL_vz00_203), BUNSPEC);
		}

	}



/* &rtl_getfield-objtype-set! */
	obj_t BGl_z62rtl_getfieldzd2objtypezd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7271, obj_t BgL_oz00_7272, obj_t BgL_vz00_7273)
	{
		{	/* SawMill/defs.sch 733 */
			return
				BGl_rtl_getfieldzd2objtypezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_getfieldz00_bglt) BgL_oz00_7272),
				((BgL_typez00_bglt) BgL_vz00_7273));
		}

	}



/* rtl_getfield-name */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_getfieldzd2namezd2zzsaw_defsz00(BgL_rtl_getfieldz00_bglt
		BgL_oz00_204)
	{
		{	/* SawMill/defs.sch 734 */
			return (((BgL_rtl_getfieldz00_bglt) COBJECT(BgL_oz00_204))->BgL_namez00);
		}

	}



/* &rtl_getfield-name */
	obj_t BGl_z62rtl_getfieldzd2namezb0zzsaw_defsz00(obj_t BgL_envz00_7274,
		obj_t BgL_oz00_7275)
	{
		{	/* SawMill/defs.sch 734 */
			return
				BGl_rtl_getfieldzd2namezd2zzsaw_defsz00(
				((BgL_rtl_getfieldz00_bglt) BgL_oz00_7275));
		}

	}



/* rtl_getfield-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_getfieldzd2namezd2setz12z12zzsaw_defsz00(BgL_rtl_getfieldz00_bglt
		BgL_oz00_205, obj_t BgL_vz00_206)
	{
		{	/* SawMill/defs.sch 735 */
			return
				((((BgL_rtl_getfieldz00_bglt) COBJECT(BgL_oz00_205))->BgL_namez00) =
				((obj_t) BgL_vz00_206), BUNSPEC);
		}

	}



/* &rtl_getfield-name-set! */
	obj_t BGl_z62rtl_getfieldzd2namezd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7276, obj_t BgL_oz00_7277, obj_t BgL_vz00_7278)
	{
		{	/* SawMill/defs.sch 735 */
			return
				BGl_rtl_getfieldzd2namezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_getfieldz00_bglt) BgL_oz00_7277), BgL_vz00_7278);
		}

	}



/* rtl_getfield-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_getfieldzd2loczd2zzsaw_defsz00(BgL_rtl_getfieldz00_bglt
		BgL_oz00_207)
	{
		{	/* SawMill/defs.sch 736 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_207)))->BgL_locz00);
		}

	}



/* &rtl_getfield-loc */
	obj_t BGl_z62rtl_getfieldzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7279,
		obj_t BgL_oz00_7280)
	{
		{	/* SawMill/defs.sch 736 */
			return
				BGl_rtl_getfieldzd2loczd2zzsaw_defsz00(
				((BgL_rtl_getfieldz00_bglt) BgL_oz00_7280));
		}

	}



/* rtl_getfield-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_getfieldzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_getfieldz00_bglt
		BgL_oz00_208, obj_t BgL_vz00_209)
	{
		{	/* SawMill/defs.sch 737 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_208)))->BgL_locz00) =
				((obj_t) BgL_vz00_209), BUNSPEC);
		}

	}



/* &rtl_getfield-loc-set! */
	obj_t BGl_z62rtl_getfieldzd2loczd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7281, obj_t BgL_oz00_7282, obj_t BgL_vz00_7283)
	{
		{	/* SawMill/defs.sch 737 */
			return
				BGl_rtl_getfieldzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_getfieldz00_bglt) BgL_oz00_7282), BgL_vz00_7283);
		}

	}



/* make-rtl_valloc */
	BGL_EXPORTED_DEF BgL_rtl_vallocz00_bglt
		BGl_makezd2rtl_valloczd2zzsaw_defsz00(obj_t BgL_loc1429z00_210,
		BgL_typez00_bglt BgL_type1430z00_211, BgL_typez00_bglt BgL_vtype1431z00_212)
	{
		{	/* SawMill/defs.sch 740 */
			{	/* SawMill/defs.sch 740 */
				BgL_rtl_vallocz00_bglt BgL_new1394z00_9119;

				{	/* SawMill/defs.sch 740 */
					BgL_rtl_vallocz00_bglt BgL_new1393z00_9120;

					BgL_new1393z00_9120 =
						((BgL_rtl_vallocz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_vallocz00_bgl))));
					{	/* SawMill/defs.sch 740 */
						long BgL_arg1857z00_9121;

						BgL_arg1857z00_9121 = BGL_CLASS_NUM(BGl_rtl_vallocz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1393z00_9120), BgL_arg1857z00_9121);
					}
					BgL_new1394z00_9119 = BgL_new1393z00_9120;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1394z00_9119)))->BgL_locz00) =
					((obj_t) BgL_loc1429z00_210), BUNSPEC);
				((((BgL_rtl_vallocz00_bglt) COBJECT(BgL_new1394z00_9119))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1430z00_211), BUNSPEC);
				((((BgL_rtl_vallocz00_bglt) COBJECT(BgL_new1394z00_9119))->
						BgL_vtypez00) = ((BgL_typez00_bglt) BgL_vtype1431z00_212), BUNSPEC);
				return BgL_new1394z00_9119;
			}
		}

	}



/* &make-rtl_valloc */
	BgL_rtl_vallocz00_bglt BGl_z62makezd2rtl_valloczb0zzsaw_defsz00(obj_t
		BgL_envz00_7284, obj_t BgL_loc1429z00_7285, obj_t BgL_type1430z00_7286,
		obj_t BgL_vtype1431z00_7287)
	{
		{	/* SawMill/defs.sch 740 */
			return
				BGl_makezd2rtl_valloczd2zzsaw_defsz00(BgL_loc1429z00_7285,
				((BgL_typez00_bglt) BgL_type1430z00_7286),
				((BgL_typez00_bglt) BgL_vtype1431z00_7287));
		}

	}



/* rtl_valloc? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_valloczf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_213)
	{
		{	/* SawMill/defs.sch 741 */
			{	/* SawMill/defs.sch 741 */
				obj_t BgL_classz00_9122;

				BgL_classz00_9122 = BGl_rtl_vallocz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_213))
					{	/* SawMill/defs.sch 741 */
						BgL_objectz00_bglt BgL_arg1807z00_9123;

						BgL_arg1807z00_9123 = (BgL_objectz00_bglt) (BgL_objz00_213);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 741 */
								long BgL_idxz00_9124;

								BgL_idxz00_9124 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9123);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9124 + 3L)) == BgL_classz00_9122);
							}
						else
							{	/* SawMill/defs.sch 741 */
								bool_t BgL_res3040z00_9127;

								{	/* SawMill/defs.sch 741 */
									obj_t BgL_oclassz00_9128;

									{	/* SawMill/defs.sch 741 */
										obj_t BgL_arg1815z00_9129;
										long BgL_arg1816z00_9130;

										BgL_arg1815z00_9129 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 741 */
											long BgL_arg1817z00_9131;

											BgL_arg1817z00_9131 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9123);
											BgL_arg1816z00_9130 = (BgL_arg1817z00_9131 - OBJECT_TYPE);
										}
										BgL_oclassz00_9128 =
											VECTOR_REF(BgL_arg1815z00_9129, BgL_arg1816z00_9130);
									}
									{	/* SawMill/defs.sch 741 */
										bool_t BgL__ortest_1115z00_9132;

										BgL__ortest_1115z00_9132 =
											(BgL_classz00_9122 == BgL_oclassz00_9128);
										if (BgL__ortest_1115z00_9132)
											{	/* SawMill/defs.sch 741 */
												BgL_res3040z00_9127 = BgL__ortest_1115z00_9132;
											}
										else
											{	/* SawMill/defs.sch 741 */
												long BgL_odepthz00_9133;

												{	/* SawMill/defs.sch 741 */
													obj_t BgL_arg1804z00_9134;

													BgL_arg1804z00_9134 = (BgL_oclassz00_9128);
													BgL_odepthz00_9133 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9134);
												}
												if ((3L < BgL_odepthz00_9133))
													{	/* SawMill/defs.sch 741 */
														obj_t BgL_arg1802z00_9135;

														{	/* SawMill/defs.sch 741 */
															obj_t BgL_arg1803z00_9136;

															BgL_arg1803z00_9136 = (BgL_oclassz00_9128);
															BgL_arg1802z00_9135 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9136,
																3L);
														}
														BgL_res3040z00_9127 =
															(BgL_arg1802z00_9135 == BgL_classz00_9122);
													}
												else
													{	/* SawMill/defs.sch 741 */
														BgL_res3040z00_9127 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3040z00_9127;
							}
					}
				else
					{	/* SawMill/defs.sch 741 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_valloc? */
	obj_t BGl_z62rtl_valloczf3z91zzsaw_defsz00(obj_t BgL_envz00_7288,
		obj_t BgL_objz00_7289)
	{
		{	/* SawMill/defs.sch 741 */
			return BBOOL(BGl_rtl_valloczf3zf3zzsaw_defsz00(BgL_objz00_7289));
		}

	}



/* rtl_valloc-nil */
	BGL_EXPORTED_DEF BgL_rtl_vallocz00_bglt
		BGl_rtl_valloczd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 742 */
			{	/* SawMill/defs.sch 742 */
				obj_t BgL_classz00_5177;

				BgL_classz00_5177 = BGl_rtl_vallocz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 742 */
					obj_t BgL__ortest_1117z00_5178;

					BgL__ortest_1117z00_5178 = BGL_CLASS_NIL(BgL_classz00_5177);
					if (CBOOL(BgL__ortest_1117z00_5178))
						{	/* SawMill/defs.sch 742 */
							return ((BgL_rtl_vallocz00_bglt) BgL__ortest_1117z00_5178);
						}
					else
						{	/* SawMill/defs.sch 742 */
							return
								((BgL_rtl_vallocz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5177));
						}
				}
			}
		}

	}



/* &rtl_valloc-nil */
	BgL_rtl_vallocz00_bglt BGl_z62rtl_valloczd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7290)
	{
		{	/* SawMill/defs.sch 742 */
			return BGl_rtl_valloczd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_valloc-vtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_valloczd2vtypezd2zzsaw_defsz00(BgL_rtl_vallocz00_bglt BgL_oz00_214)
	{
		{	/* SawMill/defs.sch 743 */
			return (((BgL_rtl_vallocz00_bglt) COBJECT(BgL_oz00_214))->BgL_vtypez00);
		}

	}



/* &rtl_valloc-vtype */
	BgL_typez00_bglt BGl_z62rtl_valloczd2vtypezb0zzsaw_defsz00(obj_t
		BgL_envz00_7291, obj_t BgL_oz00_7292)
	{
		{	/* SawMill/defs.sch 743 */
			return
				BGl_rtl_valloczd2vtypezd2zzsaw_defsz00(
				((BgL_rtl_vallocz00_bglt) BgL_oz00_7292));
		}

	}



/* rtl_valloc-vtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_valloczd2vtypezd2setz12z12zzsaw_defsz00(BgL_rtl_vallocz00_bglt
		BgL_oz00_215, BgL_typez00_bglt BgL_vz00_216)
	{
		{	/* SawMill/defs.sch 744 */
			return
				((((BgL_rtl_vallocz00_bglt) COBJECT(BgL_oz00_215))->BgL_vtypez00) =
				((BgL_typez00_bglt) BgL_vz00_216), BUNSPEC);
		}

	}



/* &rtl_valloc-vtype-set! */
	obj_t BGl_z62rtl_valloczd2vtypezd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7293, obj_t BgL_oz00_7294, obj_t BgL_vz00_7295)
	{
		{	/* SawMill/defs.sch 744 */
			return
				BGl_rtl_valloczd2vtypezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_vallocz00_bglt) BgL_oz00_7294),
				((BgL_typez00_bglt) BgL_vz00_7295));
		}

	}



/* rtl_valloc-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_valloczd2typezd2zzsaw_defsz00(BgL_rtl_vallocz00_bglt BgL_oz00_217)
	{
		{	/* SawMill/defs.sch 745 */
			return (((BgL_rtl_vallocz00_bglt) COBJECT(BgL_oz00_217))->BgL_typez00);
		}

	}



/* &rtl_valloc-type */
	BgL_typez00_bglt BGl_z62rtl_valloczd2typezb0zzsaw_defsz00(obj_t
		BgL_envz00_7296, obj_t BgL_oz00_7297)
	{
		{	/* SawMill/defs.sch 745 */
			return
				BGl_rtl_valloczd2typezd2zzsaw_defsz00(
				((BgL_rtl_vallocz00_bglt) BgL_oz00_7297));
		}

	}



/* rtl_valloc-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_valloczd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_vallocz00_bglt
		BgL_oz00_218, BgL_typez00_bglt BgL_vz00_219)
	{
		{	/* SawMill/defs.sch 746 */
			return
				((((BgL_rtl_vallocz00_bglt) COBJECT(BgL_oz00_218))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_219), BUNSPEC);
		}

	}



/* &rtl_valloc-type-set! */
	obj_t BGl_z62rtl_valloczd2typezd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7298,
		obj_t BgL_oz00_7299, obj_t BgL_vz00_7300)
	{
		{	/* SawMill/defs.sch 746 */
			return
				BGl_rtl_valloczd2typezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_vallocz00_bglt) BgL_oz00_7299),
				((BgL_typez00_bglt) BgL_vz00_7300));
		}

	}



/* rtl_valloc-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_valloczd2loczd2zzsaw_defsz00(BgL_rtl_vallocz00_bglt BgL_oz00_220)
	{
		{	/* SawMill/defs.sch 747 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_220)))->BgL_locz00);
		}

	}



/* &rtl_valloc-loc */
	obj_t BGl_z62rtl_valloczd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7301,
		obj_t BgL_oz00_7302)
	{
		{	/* SawMill/defs.sch 747 */
			return
				BGl_rtl_valloczd2loczd2zzsaw_defsz00(
				((BgL_rtl_vallocz00_bglt) BgL_oz00_7302));
		}

	}



/* rtl_valloc-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_valloczd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_vallocz00_bglt
		BgL_oz00_221, obj_t BgL_vz00_222)
	{
		{	/* SawMill/defs.sch 748 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_221)))->BgL_locz00) =
				((obj_t) BgL_vz00_222), BUNSPEC);
		}

	}



/* &rtl_valloc-loc-set! */
	obj_t BGl_z62rtl_valloczd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7303,
		obj_t BgL_oz00_7304, obj_t BgL_vz00_7305)
	{
		{	/* SawMill/defs.sch 748 */
			return
				BGl_rtl_valloczd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_vallocz00_bglt) BgL_oz00_7304), BgL_vz00_7305);
		}

	}



/* make-rtl_vref */
	BGL_EXPORTED_DEF BgL_rtl_vrefz00_bglt
		BGl_makezd2rtl_vrefzd2zzsaw_defsz00(obj_t BgL_loc1425z00_223,
		BgL_typez00_bglt BgL_type1426z00_224, BgL_typez00_bglt BgL_vtype1427z00_225)
	{
		{	/* SawMill/defs.sch 751 */
			{	/* SawMill/defs.sch 751 */
				BgL_rtl_vrefz00_bglt BgL_new1396z00_9137;

				{	/* SawMill/defs.sch 751 */
					BgL_rtl_vrefz00_bglt BgL_new1395z00_9138;

					BgL_new1395z00_9138 =
						((BgL_rtl_vrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_vrefz00_bgl))));
					{	/* SawMill/defs.sch 751 */
						long BgL_arg1858z00_9139;

						BgL_arg1858z00_9139 = BGL_CLASS_NUM(BGl_rtl_vrefz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1395z00_9138), BgL_arg1858z00_9139);
					}
					BgL_new1396z00_9137 = BgL_new1395z00_9138;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1396z00_9137)))->BgL_locz00) =
					((obj_t) BgL_loc1425z00_223), BUNSPEC);
				((((BgL_rtl_vrefz00_bglt) COBJECT(BgL_new1396z00_9137))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1426z00_224), BUNSPEC);
				((((BgL_rtl_vrefz00_bglt) COBJECT(BgL_new1396z00_9137))->BgL_vtypez00) =
					((BgL_typez00_bglt) BgL_vtype1427z00_225), BUNSPEC);
				return BgL_new1396z00_9137;
			}
		}

	}



/* &make-rtl_vref */
	BgL_rtl_vrefz00_bglt BGl_z62makezd2rtl_vrefzb0zzsaw_defsz00(obj_t
		BgL_envz00_7306, obj_t BgL_loc1425z00_7307, obj_t BgL_type1426z00_7308,
		obj_t BgL_vtype1427z00_7309)
	{
		{	/* SawMill/defs.sch 751 */
			return
				BGl_makezd2rtl_vrefzd2zzsaw_defsz00(BgL_loc1425z00_7307,
				((BgL_typez00_bglt) BgL_type1426z00_7308),
				((BgL_typez00_bglt) BgL_vtype1427z00_7309));
		}

	}



/* rtl_vref? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_vrefzf3zf3zzsaw_defsz00(obj_t BgL_objz00_226)
	{
		{	/* SawMill/defs.sch 752 */
			{	/* SawMill/defs.sch 752 */
				obj_t BgL_classz00_9140;

				BgL_classz00_9140 = BGl_rtl_vrefz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_226))
					{	/* SawMill/defs.sch 752 */
						BgL_objectz00_bglt BgL_arg1807z00_9141;

						BgL_arg1807z00_9141 = (BgL_objectz00_bglt) (BgL_objz00_226);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 752 */
								long BgL_idxz00_9142;

								BgL_idxz00_9142 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9141);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9142 + 3L)) == BgL_classz00_9140);
							}
						else
							{	/* SawMill/defs.sch 752 */
								bool_t BgL_res3041z00_9145;

								{	/* SawMill/defs.sch 752 */
									obj_t BgL_oclassz00_9146;

									{	/* SawMill/defs.sch 752 */
										obj_t BgL_arg1815z00_9147;
										long BgL_arg1816z00_9148;

										BgL_arg1815z00_9147 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 752 */
											long BgL_arg1817z00_9149;

											BgL_arg1817z00_9149 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9141);
											BgL_arg1816z00_9148 = (BgL_arg1817z00_9149 - OBJECT_TYPE);
										}
										BgL_oclassz00_9146 =
											VECTOR_REF(BgL_arg1815z00_9147, BgL_arg1816z00_9148);
									}
									{	/* SawMill/defs.sch 752 */
										bool_t BgL__ortest_1115z00_9150;

										BgL__ortest_1115z00_9150 =
											(BgL_classz00_9140 == BgL_oclassz00_9146);
										if (BgL__ortest_1115z00_9150)
											{	/* SawMill/defs.sch 752 */
												BgL_res3041z00_9145 = BgL__ortest_1115z00_9150;
											}
										else
											{	/* SawMill/defs.sch 752 */
												long BgL_odepthz00_9151;

												{	/* SawMill/defs.sch 752 */
													obj_t BgL_arg1804z00_9152;

													BgL_arg1804z00_9152 = (BgL_oclassz00_9146);
													BgL_odepthz00_9151 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9152);
												}
												if ((3L < BgL_odepthz00_9151))
													{	/* SawMill/defs.sch 752 */
														obj_t BgL_arg1802z00_9153;

														{	/* SawMill/defs.sch 752 */
															obj_t BgL_arg1803z00_9154;

															BgL_arg1803z00_9154 = (BgL_oclassz00_9146);
															BgL_arg1802z00_9153 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9154,
																3L);
														}
														BgL_res3041z00_9145 =
															(BgL_arg1802z00_9153 == BgL_classz00_9140);
													}
												else
													{	/* SawMill/defs.sch 752 */
														BgL_res3041z00_9145 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3041z00_9145;
							}
					}
				else
					{	/* SawMill/defs.sch 752 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_vref? */
	obj_t BGl_z62rtl_vrefzf3z91zzsaw_defsz00(obj_t BgL_envz00_7310,
		obj_t BgL_objz00_7311)
	{
		{	/* SawMill/defs.sch 752 */
			return BBOOL(BGl_rtl_vrefzf3zf3zzsaw_defsz00(BgL_objz00_7311));
		}

	}



/* rtl_vref-nil */
	BGL_EXPORTED_DEF BgL_rtl_vrefz00_bglt BGl_rtl_vrefzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 753 */
			{	/* SawMill/defs.sch 753 */
				obj_t BgL_classz00_5219;

				BgL_classz00_5219 = BGl_rtl_vrefz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 753 */
					obj_t BgL__ortest_1117z00_5220;

					BgL__ortest_1117z00_5220 = BGL_CLASS_NIL(BgL_classz00_5219);
					if (CBOOL(BgL__ortest_1117z00_5220))
						{	/* SawMill/defs.sch 753 */
							return ((BgL_rtl_vrefz00_bglt) BgL__ortest_1117z00_5220);
						}
					else
						{	/* SawMill/defs.sch 753 */
							return
								((BgL_rtl_vrefz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5219));
						}
				}
			}
		}

	}



/* &rtl_vref-nil */
	BgL_rtl_vrefz00_bglt BGl_z62rtl_vrefzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7312)
	{
		{	/* SawMill/defs.sch 753 */
			return BGl_rtl_vrefzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_vref-vtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_vrefzd2vtypezd2zzsaw_defsz00(BgL_rtl_vrefz00_bglt BgL_oz00_227)
	{
		{	/* SawMill/defs.sch 754 */
			return (((BgL_rtl_vrefz00_bglt) COBJECT(BgL_oz00_227))->BgL_vtypez00);
		}

	}



/* &rtl_vref-vtype */
	BgL_typez00_bglt BGl_z62rtl_vrefzd2vtypezb0zzsaw_defsz00(obj_t
		BgL_envz00_7313, obj_t BgL_oz00_7314)
	{
		{	/* SawMill/defs.sch 754 */
			return
				BGl_rtl_vrefzd2vtypezd2zzsaw_defsz00(
				((BgL_rtl_vrefz00_bglt) BgL_oz00_7314));
		}

	}



/* rtl_vref-vtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_vrefzd2vtypezd2setz12z12zzsaw_defsz00(BgL_rtl_vrefz00_bglt
		BgL_oz00_228, BgL_typez00_bglt BgL_vz00_229)
	{
		{	/* SawMill/defs.sch 755 */
			return
				((((BgL_rtl_vrefz00_bglt) COBJECT(BgL_oz00_228))->BgL_vtypez00) =
				((BgL_typez00_bglt) BgL_vz00_229), BUNSPEC);
		}

	}



/* &rtl_vref-vtype-set! */
	obj_t BGl_z62rtl_vrefzd2vtypezd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7315,
		obj_t BgL_oz00_7316, obj_t BgL_vz00_7317)
	{
		{	/* SawMill/defs.sch 755 */
			return
				BGl_rtl_vrefzd2vtypezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_vrefz00_bglt) BgL_oz00_7316),
				((BgL_typez00_bglt) BgL_vz00_7317));
		}

	}



/* rtl_vref-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_vrefzd2typezd2zzsaw_defsz00(BgL_rtl_vrefz00_bglt BgL_oz00_230)
	{
		{	/* SawMill/defs.sch 756 */
			return (((BgL_rtl_vrefz00_bglt) COBJECT(BgL_oz00_230))->BgL_typez00);
		}

	}



/* &rtl_vref-type */
	BgL_typez00_bglt BGl_z62rtl_vrefzd2typezb0zzsaw_defsz00(obj_t BgL_envz00_7318,
		obj_t BgL_oz00_7319)
	{
		{	/* SawMill/defs.sch 756 */
			return
				BGl_rtl_vrefzd2typezd2zzsaw_defsz00(
				((BgL_rtl_vrefz00_bglt) BgL_oz00_7319));
		}

	}



/* rtl_vref-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_vrefzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_vrefz00_bglt
		BgL_oz00_231, BgL_typez00_bglt BgL_vz00_232)
	{
		{	/* SawMill/defs.sch 757 */
			return
				((((BgL_rtl_vrefz00_bglt) COBJECT(BgL_oz00_231))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_232), BUNSPEC);
		}

	}



/* &rtl_vref-type-set! */
	obj_t BGl_z62rtl_vrefzd2typezd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7320,
		obj_t BgL_oz00_7321, obj_t BgL_vz00_7322)
	{
		{	/* SawMill/defs.sch 757 */
			return
				BGl_rtl_vrefzd2typezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_vrefz00_bglt) BgL_oz00_7321),
				((BgL_typez00_bglt) BgL_vz00_7322));
		}

	}



/* rtl_vref-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_vrefzd2loczd2zzsaw_defsz00(BgL_rtl_vrefz00_bglt
		BgL_oz00_233)
	{
		{	/* SawMill/defs.sch 758 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_233)))->BgL_locz00);
		}

	}



/* &rtl_vref-loc */
	obj_t BGl_z62rtl_vrefzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7323,
		obj_t BgL_oz00_7324)
	{
		{	/* SawMill/defs.sch 758 */
			return
				BGl_rtl_vrefzd2loczd2zzsaw_defsz00(
				((BgL_rtl_vrefz00_bglt) BgL_oz00_7324));
		}

	}



/* rtl_vref-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_vrefzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_vrefz00_bglt
		BgL_oz00_234, obj_t BgL_vz00_235)
	{
		{	/* SawMill/defs.sch 759 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_234)))->BgL_locz00) =
				((obj_t) BgL_vz00_235), BUNSPEC);
		}

	}



/* &rtl_vref-loc-set! */
	obj_t BGl_z62rtl_vrefzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7325,
		obj_t BgL_oz00_7326, obj_t BgL_vz00_7327)
	{
		{	/* SawMill/defs.sch 759 */
			return
				BGl_rtl_vrefzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_vrefz00_bglt) BgL_oz00_7326), BgL_vz00_7327);
		}

	}



/* make-rtl_vlength */
	BGL_EXPORTED_DEF BgL_rtl_vlengthz00_bglt
		BGl_makezd2rtl_vlengthzd2zzsaw_defsz00(obj_t BgL_loc1420z00_236,
		BgL_typez00_bglt BgL_type1421z00_237, BgL_typez00_bglt BgL_vtype1422z00_238)
	{
		{	/* SawMill/defs.sch 762 */
			{	/* SawMill/defs.sch 762 */
				BgL_rtl_vlengthz00_bglt BgL_new1398z00_9155;

				{	/* SawMill/defs.sch 762 */
					BgL_rtl_vlengthz00_bglt BgL_new1397z00_9156;

					BgL_new1397z00_9156 =
						((BgL_rtl_vlengthz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_vlengthz00_bgl))));
					{	/* SawMill/defs.sch 762 */
						long BgL_arg1859z00_9157;

						BgL_arg1859z00_9157 =
							BGL_CLASS_NUM(BGl_rtl_vlengthz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1397z00_9156), BgL_arg1859z00_9157);
					}
					BgL_new1398z00_9155 = BgL_new1397z00_9156;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1398z00_9155)))->BgL_locz00) =
					((obj_t) BgL_loc1420z00_236), BUNSPEC);
				((((BgL_rtl_vlengthz00_bglt) COBJECT(BgL_new1398z00_9155))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1421z00_237), BUNSPEC);
				((((BgL_rtl_vlengthz00_bglt) COBJECT(BgL_new1398z00_9155))->
						BgL_vtypez00) = ((BgL_typez00_bglt) BgL_vtype1422z00_238), BUNSPEC);
				return BgL_new1398z00_9155;
			}
		}

	}



/* &make-rtl_vlength */
	BgL_rtl_vlengthz00_bglt BGl_z62makezd2rtl_vlengthzb0zzsaw_defsz00(obj_t
		BgL_envz00_7328, obj_t BgL_loc1420z00_7329, obj_t BgL_type1421z00_7330,
		obj_t BgL_vtype1422z00_7331)
	{
		{	/* SawMill/defs.sch 762 */
			return
				BGl_makezd2rtl_vlengthzd2zzsaw_defsz00(BgL_loc1420z00_7329,
				((BgL_typez00_bglt) BgL_type1421z00_7330),
				((BgL_typez00_bglt) BgL_vtype1422z00_7331));
		}

	}



/* rtl_vlength? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_vlengthzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_239)
	{
		{	/* SawMill/defs.sch 763 */
			{	/* SawMill/defs.sch 763 */
				obj_t BgL_classz00_9158;

				BgL_classz00_9158 = BGl_rtl_vlengthz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_239))
					{	/* SawMill/defs.sch 763 */
						BgL_objectz00_bglt BgL_arg1807z00_9159;

						BgL_arg1807z00_9159 = (BgL_objectz00_bglt) (BgL_objz00_239);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 763 */
								long BgL_idxz00_9160;

								BgL_idxz00_9160 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9159);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9160 + 3L)) == BgL_classz00_9158);
							}
						else
							{	/* SawMill/defs.sch 763 */
								bool_t BgL_res3042z00_9163;

								{	/* SawMill/defs.sch 763 */
									obj_t BgL_oclassz00_9164;

									{	/* SawMill/defs.sch 763 */
										obj_t BgL_arg1815z00_9165;
										long BgL_arg1816z00_9166;

										BgL_arg1815z00_9165 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 763 */
											long BgL_arg1817z00_9167;

											BgL_arg1817z00_9167 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9159);
											BgL_arg1816z00_9166 = (BgL_arg1817z00_9167 - OBJECT_TYPE);
										}
										BgL_oclassz00_9164 =
											VECTOR_REF(BgL_arg1815z00_9165, BgL_arg1816z00_9166);
									}
									{	/* SawMill/defs.sch 763 */
										bool_t BgL__ortest_1115z00_9168;

										BgL__ortest_1115z00_9168 =
											(BgL_classz00_9158 == BgL_oclassz00_9164);
										if (BgL__ortest_1115z00_9168)
											{	/* SawMill/defs.sch 763 */
												BgL_res3042z00_9163 = BgL__ortest_1115z00_9168;
											}
										else
											{	/* SawMill/defs.sch 763 */
												long BgL_odepthz00_9169;

												{	/* SawMill/defs.sch 763 */
													obj_t BgL_arg1804z00_9170;

													BgL_arg1804z00_9170 = (BgL_oclassz00_9164);
													BgL_odepthz00_9169 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9170);
												}
												if ((3L < BgL_odepthz00_9169))
													{	/* SawMill/defs.sch 763 */
														obj_t BgL_arg1802z00_9171;

														{	/* SawMill/defs.sch 763 */
															obj_t BgL_arg1803z00_9172;

															BgL_arg1803z00_9172 = (BgL_oclassz00_9164);
															BgL_arg1802z00_9171 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9172,
																3L);
														}
														BgL_res3042z00_9163 =
															(BgL_arg1802z00_9171 == BgL_classz00_9158);
													}
												else
													{	/* SawMill/defs.sch 763 */
														BgL_res3042z00_9163 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3042z00_9163;
							}
					}
				else
					{	/* SawMill/defs.sch 763 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_vlength? */
	obj_t BGl_z62rtl_vlengthzf3z91zzsaw_defsz00(obj_t BgL_envz00_7332,
		obj_t BgL_objz00_7333)
	{
		{	/* SawMill/defs.sch 763 */
			return BBOOL(BGl_rtl_vlengthzf3zf3zzsaw_defsz00(BgL_objz00_7333));
		}

	}



/* rtl_vlength-nil */
	BGL_EXPORTED_DEF BgL_rtl_vlengthz00_bglt
		BGl_rtl_vlengthzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 764 */
			{	/* SawMill/defs.sch 764 */
				obj_t BgL_classz00_5261;

				BgL_classz00_5261 = BGl_rtl_vlengthz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 764 */
					obj_t BgL__ortest_1117z00_5262;

					BgL__ortest_1117z00_5262 = BGL_CLASS_NIL(BgL_classz00_5261);
					if (CBOOL(BgL__ortest_1117z00_5262))
						{	/* SawMill/defs.sch 764 */
							return ((BgL_rtl_vlengthz00_bglt) BgL__ortest_1117z00_5262);
						}
					else
						{	/* SawMill/defs.sch 764 */
							return
								((BgL_rtl_vlengthz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5261));
						}
				}
			}
		}

	}



/* &rtl_vlength-nil */
	BgL_rtl_vlengthz00_bglt BGl_z62rtl_vlengthzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7334)
	{
		{	/* SawMill/defs.sch 764 */
			return BGl_rtl_vlengthzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_vlength-vtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_vlengthzd2vtypezd2zzsaw_defsz00(BgL_rtl_vlengthz00_bglt
		BgL_oz00_240)
	{
		{	/* SawMill/defs.sch 765 */
			return (((BgL_rtl_vlengthz00_bglt) COBJECT(BgL_oz00_240))->BgL_vtypez00);
		}

	}



/* &rtl_vlength-vtype */
	BgL_typez00_bglt BGl_z62rtl_vlengthzd2vtypezb0zzsaw_defsz00(obj_t
		BgL_envz00_7335, obj_t BgL_oz00_7336)
	{
		{	/* SawMill/defs.sch 765 */
			return
				BGl_rtl_vlengthzd2vtypezd2zzsaw_defsz00(
				((BgL_rtl_vlengthz00_bglt) BgL_oz00_7336));
		}

	}



/* rtl_vlength-vtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_vlengthzd2vtypezd2setz12z12zzsaw_defsz00(BgL_rtl_vlengthz00_bglt
		BgL_oz00_241, BgL_typez00_bglt BgL_vz00_242)
	{
		{	/* SawMill/defs.sch 766 */
			return
				((((BgL_rtl_vlengthz00_bglt) COBJECT(BgL_oz00_241))->BgL_vtypez00) =
				((BgL_typez00_bglt) BgL_vz00_242), BUNSPEC);
		}

	}



/* &rtl_vlength-vtype-set! */
	obj_t BGl_z62rtl_vlengthzd2vtypezd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7337, obj_t BgL_oz00_7338, obj_t BgL_vz00_7339)
	{
		{	/* SawMill/defs.sch 766 */
			return
				BGl_rtl_vlengthzd2vtypezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_vlengthz00_bglt) BgL_oz00_7338),
				((BgL_typez00_bglt) BgL_vz00_7339));
		}

	}



/* rtl_vlength-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_vlengthzd2typezd2zzsaw_defsz00(BgL_rtl_vlengthz00_bglt BgL_oz00_243)
	{
		{	/* SawMill/defs.sch 767 */
			return (((BgL_rtl_vlengthz00_bglt) COBJECT(BgL_oz00_243))->BgL_typez00);
		}

	}



/* &rtl_vlength-type */
	BgL_typez00_bglt BGl_z62rtl_vlengthzd2typezb0zzsaw_defsz00(obj_t
		BgL_envz00_7340, obj_t BgL_oz00_7341)
	{
		{	/* SawMill/defs.sch 767 */
			return
				BGl_rtl_vlengthzd2typezd2zzsaw_defsz00(
				((BgL_rtl_vlengthz00_bglt) BgL_oz00_7341));
		}

	}



/* rtl_vlength-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_vlengthzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_vlengthz00_bglt
		BgL_oz00_244, BgL_typez00_bglt BgL_vz00_245)
	{
		{	/* SawMill/defs.sch 768 */
			return
				((((BgL_rtl_vlengthz00_bglt) COBJECT(BgL_oz00_244))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_245), BUNSPEC);
		}

	}



/* &rtl_vlength-type-set! */
	obj_t BGl_z62rtl_vlengthzd2typezd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7342, obj_t BgL_oz00_7343, obj_t BgL_vz00_7344)
	{
		{	/* SawMill/defs.sch 768 */
			return
				BGl_rtl_vlengthzd2typezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_vlengthz00_bglt) BgL_oz00_7343),
				((BgL_typez00_bglt) BgL_vz00_7344));
		}

	}



/* rtl_vlength-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_vlengthzd2loczd2zzsaw_defsz00(BgL_rtl_vlengthz00_bglt BgL_oz00_246)
	{
		{	/* SawMill/defs.sch 769 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_246)))->BgL_locz00);
		}

	}



/* &rtl_vlength-loc */
	obj_t BGl_z62rtl_vlengthzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7345,
		obj_t BgL_oz00_7346)
	{
		{	/* SawMill/defs.sch 769 */
			return
				BGl_rtl_vlengthzd2loczd2zzsaw_defsz00(
				((BgL_rtl_vlengthz00_bglt) BgL_oz00_7346));
		}

	}



/* rtl_vlength-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_vlengthzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_vlengthz00_bglt
		BgL_oz00_247, obj_t BgL_vz00_248)
	{
		{	/* SawMill/defs.sch 770 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_247)))->BgL_locz00) =
				((obj_t) BgL_vz00_248), BUNSPEC);
		}

	}



/* &rtl_vlength-loc-set! */
	obj_t BGl_z62rtl_vlengthzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7347,
		obj_t BgL_oz00_7348, obj_t BgL_vz00_7349)
	{
		{	/* SawMill/defs.sch 770 */
			return
				BGl_rtl_vlengthzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_vlengthz00_bglt) BgL_oz00_7348), BgL_vz00_7349);
		}

	}



/* make-rtl_instanceof */
	BGL_EXPORTED_DEF BgL_rtl_instanceofz00_bglt
		BGl_makezd2rtl_instanceofzd2zzsaw_defsz00(obj_t BgL_loc1417z00_249,
		BgL_typez00_bglt BgL_type1418z00_250)
	{
		{	/* SawMill/defs.sch 773 */
			{	/* SawMill/defs.sch 773 */
				BgL_rtl_instanceofz00_bglt BgL_new1400z00_9173;

				{	/* SawMill/defs.sch 773 */
					BgL_rtl_instanceofz00_bglt BgL_new1399z00_9174;

					BgL_new1399z00_9174 =
						((BgL_rtl_instanceofz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_instanceofz00_bgl))));
					{	/* SawMill/defs.sch 773 */
						long BgL_arg1860z00_9175;

						BgL_arg1860z00_9175 =
							BGL_CLASS_NUM(BGl_rtl_instanceofz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1399z00_9174), BgL_arg1860z00_9175);
					}
					BgL_new1400z00_9173 = BgL_new1399z00_9174;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1400z00_9173)))->BgL_locz00) =
					((obj_t) BgL_loc1417z00_249), BUNSPEC);
				((((BgL_rtl_instanceofz00_bglt) COBJECT(BgL_new1400z00_9173))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1418z00_250), BUNSPEC);
				return BgL_new1400z00_9173;
			}
		}

	}



/* &make-rtl_instanceof */
	BgL_rtl_instanceofz00_bglt BGl_z62makezd2rtl_instanceofzb0zzsaw_defsz00(obj_t
		BgL_envz00_7350, obj_t BgL_loc1417z00_7351, obj_t BgL_type1418z00_7352)
	{
		{	/* SawMill/defs.sch 773 */
			return
				BGl_makezd2rtl_instanceofzd2zzsaw_defsz00(BgL_loc1417z00_7351,
				((BgL_typez00_bglt) BgL_type1418z00_7352));
		}

	}



/* rtl_instanceof? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_instanceofzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_251)
	{
		{	/* SawMill/defs.sch 774 */
			{	/* SawMill/defs.sch 774 */
				obj_t BgL_classz00_9176;

				BgL_classz00_9176 = BGl_rtl_instanceofz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_251))
					{	/* SawMill/defs.sch 774 */
						BgL_objectz00_bglt BgL_arg1807z00_9177;

						BgL_arg1807z00_9177 = (BgL_objectz00_bglt) (BgL_objz00_251);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 774 */
								long BgL_idxz00_9178;

								BgL_idxz00_9178 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9177);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9178 + 3L)) == BgL_classz00_9176);
							}
						else
							{	/* SawMill/defs.sch 774 */
								bool_t BgL_res3043z00_9181;

								{	/* SawMill/defs.sch 774 */
									obj_t BgL_oclassz00_9182;

									{	/* SawMill/defs.sch 774 */
										obj_t BgL_arg1815z00_9183;
										long BgL_arg1816z00_9184;

										BgL_arg1815z00_9183 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 774 */
											long BgL_arg1817z00_9185;

											BgL_arg1817z00_9185 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9177);
											BgL_arg1816z00_9184 = (BgL_arg1817z00_9185 - OBJECT_TYPE);
										}
										BgL_oclassz00_9182 =
											VECTOR_REF(BgL_arg1815z00_9183, BgL_arg1816z00_9184);
									}
									{	/* SawMill/defs.sch 774 */
										bool_t BgL__ortest_1115z00_9186;

										BgL__ortest_1115z00_9186 =
											(BgL_classz00_9176 == BgL_oclassz00_9182);
										if (BgL__ortest_1115z00_9186)
											{	/* SawMill/defs.sch 774 */
												BgL_res3043z00_9181 = BgL__ortest_1115z00_9186;
											}
										else
											{	/* SawMill/defs.sch 774 */
												long BgL_odepthz00_9187;

												{	/* SawMill/defs.sch 774 */
													obj_t BgL_arg1804z00_9188;

													BgL_arg1804z00_9188 = (BgL_oclassz00_9182);
													BgL_odepthz00_9187 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9188);
												}
												if ((3L < BgL_odepthz00_9187))
													{	/* SawMill/defs.sch 774 */
														obj_t BgL_arg1802z00_9189;

														{	/* SawMill/defs.sch 774 */
															obj_t BgL_arg1803z00_9190;

															BgL_arg1803z00_9190 = (BgL_oclassz00_9182);
															BgL_arg1802z00_9189 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9190,
																3L);
														}
														BgL_res3043z00_9181 =
															(BgL_arg1802z00_9189 == BgL_classz00_9176);
													}
												else
													{	/* SawMill/defs.sch 774 */
														BgL_res3043z00_9181 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3043z00_9181;
							}
					}
				else
					{	/* SawMill/defs.sch 774 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_instanceof? */
	obj_t BGl_z62rtl_instanceofzf3z91zzsaw_defsz00(obj_t BgL_envz00_7353,
		obj_t BgL_objz00_7354)
	{
		{	/* SawMill/defs.sch 774 */
			return BBOOL(BGl_rtl_instanceofzf3zf3zzsaw_defsz00(BgL_objz00_7354));
		}

	}



/* rtl_instanceof-nil */
	BGL_EXPORTED_DEF BgL_rtl_instanceofz00_bglt
		BGl_rtl_instanceofzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 775 */
			{	/* SawMill/defs.sch 775 */
				obj_t BgL_classz00_5303;

				BgL_classz00_5303 = BGl_rtl_instanceofz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 775 */
					obj_t BgL__ortest_1117z00_5304;

					BgL__ortest_1117z00_5304 = BGL_CLASS_NIL(BgL_classz00_5303);
					if (CBOOL(BgL__ortest_1117z00_5304))
						{	/* SawMill/defs.sch 775 */
							return ((BgL_rtl_instanceofz00_bglt) BgL__ortest_1117z00_5304);
						}
					else
						{	/* SawMill/defs.sch 775 */
							return
								((BgL_rtl_instanceofz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5303));
						}
				}
			}
		}

	}



/* &rtl_instanceof-nil */
	BgL_rtl_instanceofz00_bglt BGl_z62rtl_instanceofzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7355)
	{
		{	/* SawMill/defs.sch 775 */
			return BGl_rtl_instanceofzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_instanceof-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_instanceofzd2typezd2zzsaw_defsz00(BgL_rtl_instanceofz00_bglt
		BgL_oz00_252)
	{
		{	/* SawMill/defs.sch 776 */
			return
				(((BgL_rtl_instanceofz00_bglt) COBJECT(BgL_oz00_252))->BgL_typez00);
		}

	}



/* &rtl_instanceof-type */
	BgL_typez00_bglt BGl_z62rtl_instanceofzd2typezb0zzsaw_defsz00(obj_t
		BgL_envz00_7356, obj_t BgL_oz00_7357)
	{
		{	/* SawMill/defs.sch 776 */
			return
				BGl_rtl_instanceofzd2typezd2zzsaw_defsz00(
				((BgL_rtl_instanceofz00_bglt) BgL_oz00_7357));
		}

	}



/* rtl_instanceof-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_instanceofzd2typezd2setz12z12zzsaw_defsz00
		(BgL_rtl_instanceofz00_bglt BgL_oz00_253, BgL_typez00_bglt BgL_vz00_254)
	{
		{	/* SawMill/defs.sch 777 */
			return
				((((BgL_rtl_instanceofz00_bglt) COBJECT(BgL_oz00_253))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_254), BUNSPEC);
		}

	}



/* &rtl_instanceof-type-set! */
	obj_t BGl_z62rtl_instanceofzd2typezd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7358, obj_t BgL_oz00_7359, obj_t BgL_vz00_7360)
	{
		{	/* SawMill/defs.sch 777 */
			return
				BGl_rtl_instanceofzd2typezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_instanceofz00_bglt) BgL_oz00_7359),
				((BgL_typez00_bglt) BgL_vz00_7360));
		}

	}



/* rtl_instanceof-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_instanceofzd2loczd2zzsaw_defsz00(BgL_rtl_instanceofz00_bglt
		BgL_oz00_255)
	{
		{	/* SawMill/defs.sch 778 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_255)))->BgL_locz00);
		}

	}



/* &rtl_instanceof-loc */
	obj_t BGl_z62rtl_instanceofzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7361,
		obj_t BgL_oz00_7362)
	{
		{	/* SawMill/defs.sch 778 */
			return
				BGl_rtl_instanceofzd2loczd2zzsaw_defsz00(
				((BgL_rtl_instanceofz00_bglt) BgL_oz00_7362));
		}

	}



/* rtl_instanceof-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_instanceofzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_instanceofz00_bglt
		BgL_oz00_256, obj_t BgL_vz00_257)
	{
		{	/* SawMill/defs.sch 779 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_256)))->BgL_locz00) =
				((obj_t) BgL_vz00_257), BUNSPEC);
		}

	}



/* &rtl_instanceof-loc-set! */
	obj_t BGl_z62rtl_instanceofzd2loczd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7363, obj_t BgL_oz00_7364, obj_t BgL_vz00_7365)
	{
		{	/* SawMill/defs.sch 779 */
			return
				BGl_rtl_instanceofzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_instanceofz00_bglt) BgL_oz00_7364), BgL_vz00_7365);
		}

	}



/* make-rtl_makebox */
	BGL_EXPORTED_DEF BgL_rtl_makeboxz00_bglt
		BGl_makezd2rtl_makeboxzd2zzsaw_defsz00(obj_t BgL_loc1415z00_258)
	{
		{	/* SawMill/defs.sch 782 */
			{	/* SawMill/defs.sch 782 */
				BgL_rtl_makeboxz00_bglt BgL_new1402z00_9191;

				{	/* SawMill/defs.sch 782 */
					BgL_rtl_makeboxz00_bglt BgL_new1401z00_9192;

					BgL_new1401z00_9192 =
						((BgL_rtl_makeboxz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_makeboxz00_bgl))));
					{	/* SawMill/defs.sch 782 */
						long BgL_arg1862z00_9193;

						BgL_arg1862z00_9193 =
							BGL_CLASS_NUM(BGl_rtl_makeboxz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1401z00_9192), BgL_arg1862z00_9193);
					}
					BgL_new1402z00_9191 = BgL_new1401z00_9192;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1402z00_9191)))->BgL_locz00) =
					((obj_t) BgL_loc1415z00_258), BUNSPEC);
				return BgL_new1402z00_9191;
			}
		}

	}



/* &make-rtl_makebox */
	BgL_rtl_makeboxz00_bglt BGl_z62makezd2rtl_makeboxzb0zzsaw_defsz00(obj_t
		BgL_envz00_7366, obj_t BgL_loc1415z00_7367)
	{
		{	/* SawMill/defs.sch 782 */
			return BGl_makezd2rtl_makeboxzd2zzsaw_defsz00(BgL_loc1415z00_7367);
		}

	}



/* rtl_makebox? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_makeboxzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_259)
	{
		{	/* SawMill/defs.sch 783 */
			{	/* SawMill/defs.sch 783 */
				obj_t BgL_classz00_9194;

				BgL_classz00_9194 = BGl_rtl_makeboxz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_259))
					{	/* SawMill/defs.sch 783 */
						BgL_objectz00_bglt BgL_arg1807z00_9195;

						BgL_arg1807z00_9195 = (BgL_objectz00_bglt) (BgL_objz00_259);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 783 */
								long BgL_idxz00_9196;

								BgL_idxz00_9196 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9195);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9196 + 3L)) == BgL_classz00_9194);
							}
						else
							{	/* SawMill/defs.sch 783 */
								bool_t BgL_res3044z00_9199;

								{	/* SawMill/defs.sch 783 */
									obj_t BgL_oclassz00_9200;

									{	/* SawMill/defs.sch 783 */
										obj_t BgL_arg1815z00_9201;
										long BgL_arg1816z00_9202;

										BgL_arg1815z00_9201 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 783 */
											long BgL_arg1817z00_9203;

											BgL_arg1817z00_9203 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9195);
											BgL_arg1816z00_9202 = (BgL_arg1817z00_9203 - OBJECT_TYPE);
										}
										BgL_oclassz00_9200 =
											VECTOR_REF(BgL_arg1815z00_9201, BgL_arg1816z00_9202);
									}
									{	/* SawMill/defs.sch 783 */
										bool_t BgL__ortest_1115z00_9204;

										BgL__ortest_1115z00_9204 =
											(BgL_classz00_9194 == BgL_oclassz00_9200);
										if (BgL__ortest_1115z00_9204)
											{	/* SawMill/defs.sch 783 */
												BgL_res3044z00_9199 = BgL__ortest_1115z00_9204;
											}
										else
											{	/* SawMill/defs.sch 783 */
												long BgL_odepthz00_9205;

												{	/* SawMill/defs.sch 783 */
													obj_t BgL_arg1804z00_9206;

													BgL_arg1804z00_9206 = (BgL_oclassz00_9200);
													BgL_odepthz00_9205 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9206);
												}
												if ((3L < BgL_odepthz00_9205))
													{	/* SawMill/defs.sch 783 */
														obj_t BgL_arg1802z00_9207;

														{	/* SawMill/defs.sch 783 */
															obj_t BgL_arg1803z00_9208;

															BgL_arg1803z00_9208 = (BgL_oclassz00_9200);
															BgL_arg1802z00_9207 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9208,
																3L);
														}
														BgL_res3044z00_9199 =
															(BgL_arg1802z00_9207 == BgL_classz00_9194);
													}
												else
													{	/* SawMill/defs.sch 783 */
														BgL_res3044z00_9199 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3044z00_9199;
							}
					}
				else
					{	/* SawMill/defs.sch 783 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_makebox? */
	obj_t BGl_z62rtl_makeboxzf3z91zzsaw_defsz00(obj_t BgL_envz00_7368,
		obj_t BgL_objz00_7369)
	{
		{	/* SawMill/defs.sch 783 */
			return BBOOL(BGl_rtl_makeboxzf3zf3zzsaw_defsz00(BgL_objz00_7369));
		}

	}



/* rtl_makebox-nil */
	BGL_EXPORTED_DEF BgL_rtl_makeboxz00_bglt
		BGl_rtl_makeboxzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 784 */
			{	/* SawMill/defs.sch 784 */
				obj_t BgL_classz00_5345;

				BgL_classz00_5345 = BGl_rtl_makeboxz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 784 */
					obj_t BgL__ortest_1117z00_5346;

					BgL__ortest_1117z00_5346 = BGL_CLASS_NIL(BgL_classz00_5345);
					if (CBOOL(BgL__ortest_1117z00_5346))
						{	/* SawMill/defs.sch 784 */
							return ((BgL_rtl_makeboxz00_bglt) BgL__ortest_1117z00_5346);
						}
					else
						{	/* SawMill/defs.sch 784 */
							return
								((BgL_rtl_makeboxz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5345));
						}
				}
			}
		}

	}



/* &rtl_makebox-nil */
	BgL_rtl_makeboxz00_bglt BGl_z62rtl_makeboxzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7370)
	{
		{	/* SawMill/defs.sch 784 */
			return BGl_rtl_makeboxzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_makebox-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_makeboxzd2loczd2zzsaw_defsz00(BgL_rtl_makeboxz00_bglt BgL_oz00_260)
	{
		{	/* SawMill/defs.sch 785 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_260)))->BgL_locz00);
		}

	}



/* &rtl_makebox-loc */
	obj_t BGl_z62rtl_makeboxzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7371,
		obj_t BgL_oz00_7372)
	{
		{	/* SawMill/defs.sch 785 */
			return
				BGl_rtl_makeboxzd2loczd2zzsaw_defsz00(
				((BgL_rtl_makeboxz00_bglt) BgL_oz00_7372));
		}

	}



/* rtl_makebox-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_makeboxzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_makeboxz00_bglt
		BgL_oz00_261, obj_t BgL_vz00_262)
	{
		{	/* SawMill/defs.sch 786 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_261)))->BgL_locz00) =
				((obj_t) BgL_vz00_262), BUNSPEC);
		}

	}



/* &rtl_makebox-loc-set! */
	obj_t BGl_z62rtl_makeboxzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7373,
		obj_t BgL_oz00_7374, obj_t BgL_vz00_7375)
	{
		{	/* SawMill/defs.sch 786 */
			return
				BGl_rtl_makeboxzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_makeboxz00_bglt) BgL_oz00_7374), BgL_vz00_7375);
		}

	}



/* make-rtl_boxref */
	BGL_EXPORTED_DEF BgL_rtl_boxrefz00_bglt
		BGl_makezd2rtl_boxrefzd2zzsaw_defsz00(obj_t BgL_loc1412z00_263)
	{
		{	/* SawMill/defs.sch 789 */
			{	/* SawMill/defs.sch 789 */
				BgL_rtl_boxrefz00_bglt BgL_new1404z00_9209;

				{	/* SawMill/defs.sch 789 */
					BgL_rtl_boxrefz00_bglt BgL_new1403z00_9210;

					BgL_new1403z00_9210 =
						((BgL_rtl_boxrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_boxrefz00_bgl))));
					{	/* SawMill/defs.sch 789 */
						long BgL_arg1863z00_9211;

						BgL_arg1863z00_9211 = BGL_CLASS_NUM(BGl_rtl_boxrefz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1403z00_9210), BgL_arg1863z00_9211);
					}
					BgL_new1404z00_9209 = BgL_new1403z00_9210;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1404z00_9209)))->BgL_locz00) =
					((obj_t) BgL_loc1412z00_263), BUNSPEC);
				return BgL_new1404z00_9209;
			}
		}

	}



/* &make-rtl_boxref */
	BgL_rtl_boxrefz00_bglt BGl_z62makezd2rtl_boxrefzb0zzsaw_defsz00(obj_t
		BgL_envz00_7376, obj_t BgL_loc1412z00_7377)
	{
		{	/* SawMill/defs.sch 789 */
			return BGl_makezd2rtl_boxrefzd2zzsaw_defsz00(BgL_loc1412z00_7377);
		}

	}



/* rtl_boxref? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_boxrefzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_264)
	{
		{	/* SawMill/defs.sch 790 */
			{	/* SawMill/defs.sch 790 */
				obj_t BgL_classz00_9212;

				BgL_classz00_9212 = BGl_rtl_boxrefz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_264))
					{	/* SawMill/defs.sch 790 */
						BgL_objectz00_bglt BgL_arg1807z00_9213;

						BgL_arg1807z00_9213 = (BgL_objectz00_bglt) (BgL_objz00_264);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 790 */
								long BgL_idxz00_9214;

								BgL_idxz00_9214 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9213);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9214 + 3L)) == BgL_classz00_9212);
							}
						else
							{	/* SawMill/defs.sch 790 */
								bool_t BgL_res3045z00_9217;

								{	/* SawMill/defs.sch 790 */
									obj_t BgL_oclassz00_9218;

									{	/* SawMill/defs.sch 790 */
										obj_t BgL_arg1815z00_9219;
										long BgL_arg1816z00_9220;

										BgL_arg1815z00_9219 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 790 */
											long BgL_arg1817z00_9221;

											BgL_arg1817z00_9221 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9213);
											BgL_arg1816z00_9220 = (BgL_arg1817z00_9221 - OBJECT_TYPE);
										}
										BgL_oclassz00_9218 =
											VECTOR_REF(BgL_arg1815z00_9219, BgL_arg1816z00_9220);
									}
									{	/* SawMill/defs.sch 790 */
										bool_t BgL__ortest_1115z00_9222;

										BgL__ortest_1115z00_9222 =
											(BgL_classz00_9212 == BgL_oclassz00_9218);
										if (BgL__ortest_1115z00_9222)
											{	/* SawMill/defs.sch 790 */
												BgL_res3045z00_9217 = BgL__ortest_1115z00_9222;
											}
										else
											{	/* SawMill/defs.sch 790 */
												long BgL_odepthz00_9223;

												{	/* SawMill/defs.sch 790 */
													obj_t BgL_arg1804z00_9224;

													BgL_arg1804z00_9224 = (BgL_oclassz00_9218);
													BgL_odepthz00_9223 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9224);
												}
												if ((3L < BgL_odepthz00_9223))
													{	/* SawMill/defs.sch 790 */
														obj_t BgL_arg1802z00_9225;

														{	/* SawMill/defs.sch 790 */
															obj_t BgL_arg1803z00_9226;

															BgL_arg1803z00_9226 = (BgL_oclassz00_9218);
															BgL_arg1802z00_9225 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9226,
																3L);
														}
														BgL_res3045z00_9217 =
															(BgL_arg1802z00_9225 == BgL_classz00_9212);
													}
												else
													{	/* SawMill/defs.sch 790 */
														BgL_res3045z00_9217 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3045z00_9217;
							}
					}
				else
					{	/* SawMill/defs.sch 790 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_boxref? */
	obj_t BGl_z62rtl_boxrefzf3z91zzsaw_defsz00(obj_t BgL_envz00_7378,
		obj_t BgL_objz00_7379)
	{
		{	/* SawMill/defs.sch 790 */
			return BBOOL(BGl_rtl_boxrefzf3zf3zzsaw_defsz00(BgL_objz00_7379));
		}

	}



/* rtl_boxref-nil */
	BGL_EXPORTED_DEF BgL_rtl_boxrefz00_bglt
		BGl_rtl_boxrefzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 791 */
			{	/* SawMill/defs.sch 791 */
				obj_t BgL_classz00_5387;

				BgL_classz00_5387 = BGl_rtl_boxrefz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 791 */
					obj_t BgL__ortest_1117z00_5388;

					BgL__ortest_1117z00_5388 = BGL_CLASS_NIL(BgL_classz00_5387);
					if (CBOOL(BgL__ortest_1117z00_5388))
						{	/* SawMill/defs.sch 791 */
							return ((BgL_rtl_boxrefz00_bglt) BgL__ortest_1117z00_5388);
						}
					else
						{	/* SawMill/defs.sch 791 */
							return
								((BgL_rtl_boxrefz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5387));
						}
				}
			}
		}

	}



/* &rtl_boxref-nil */
	BgL_rtl_boxrefz00_bglt BGl_z62rtl_boxrefzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7380)
	{
		{	/* SawMill/defs.sch 791 */
			return BGl_rtl_boxrefzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_boxref-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_boxrefzd2loczd2zzsaw_defsz00(BgL_rtl_boxrefz00_bglt BgL_oz00_265)
	{
		{	/* SawMill/defs.sch 792 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_265)))->BgL_locz00);
		}

	}



/* &rtl_boxref-loc */
	obj_t BGl_z62rtl_boxrefzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7381,
		obj_t BgL_oz00_7382)
	{
		{	/* SawMill/defs.sch 792 */
			return
				BGl_rtl_boxrefzd2loczd2zzsaw_defsz00(
				((BgL_rtl_boxrefz00_bglt) BgL_oz00_7382));
		}

	}



/* rtl_boxref-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_boxrefzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_boxrefz00_bglt
		BgL_oz00_266, obj_t BgL_vz00_267)
	{
		{	/* SawMill/defs.sch 793 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_266)))->BgL_locz00) =
				((obj_t) BgL_vz00_267), BUNSPEC);
		}

	}



/* &rtl_boxref-loc-set! */
	obj_t BGl_z62rtl_boxrefzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7383,
		obj_t BgL_oz00_7384, obj_t BgL_vz00_7385)
	{
		{	/* SawMill/defs.sch 793 */
			return
				BGl_rtl_boxrefzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_boxrefz00_bglt) BgL_oz00_7384), BgL_vz00_7385);
		}

	}



/* make-rtl_effect */
	BGL_EXPORTED_DEF BgL_rtl_effectz00_bglt
		BGl_makezd2rtl_effectzd2zzsaw_defsz00(obj_t BgL_loc1409z00_268)
	{
		{	/* SawMill/defs.sch 796 */
			{	/* SawMill/defs.sch 796 */
				BgL_rtl_effectz00_bglt BgL_new1406z00_9227;

				{	/* SawMill/defs.sch 796 */
					BgL_rtl_effectz00_bglt BgL_new1405z00_9228;

					BgL_new1405z00_9228 =
						((BgL_rtl_effectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_effectz00_bgl))));
					{	/* SawMill/defs.sch 796 */
						long BgL_arg1864z00_9229;

						BgL_arg1864z00_9229 = BGL_CLASS_NUM(BGl_rtl_effectz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1405z00_9228), BgL_arg1864z00_9229);
					}
					BgL_new1406z00_9227 = BgL_new1405z00_9228;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1406z00_9227)))->BgL_locz00) =
					((obj_t) BgL_loc1409z00_268), BUNSPEC);
				return BgL_new1406z00_9227;
			}
		}

	}



/* &make-rtl_effect */
	BgL_rtl_effectz00_bglt BGl_z62makezd2rtl_effectzb0zzsaw_defsz00(obj_t
		BgL_envz00_7386, obj_t BgL_loc1409z00_7387)
	{
		{	/* SawMill/defs.sch 796 */
			return BGl_makezd2rtl_effectzd2zzsaw_defsz00(BgL_loc1409z00_7387);
		}

	}



/* rtl_effect? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_effectzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_269)
	{
		{	/* SawMill/defs.sch 797 */
			{	/* SawMill/defs.sch 797 */
				obj_t BgL_classz00_9230;

				BgL_classz00_9230 = BGl_rtl_effectz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_269))
					{	/* SawMill/defs.sch 797 */
						BgL_objectz00_bglt BgL_arg1807z00_9231;

						BgL_arg1807z00_9231 = (BgL_objectz00_bglt) (BgL_objz00_269);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 797 */
								long BgL_idxz00_9232;

								BgL_idxz00_9232 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9231);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9232 + 2L)) == BgL_classz00_9230);
							}
						else
							{	/* SawMill/defs.sch 797 */
								bool_t BgL_res3046z00_9235;

								{	/* SawMill/defs.sch 797 */
									obj_t BgL_oclassz00_9236;

									{	/* SawMill/defs.sch 797 */
										obj_t BgL_arg1815z00_9237;
										long BgL_arg1816z00_9238;

										BgL_arg1815z00_9237 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 797 */
											long BgL_arg1817z00_9239;

											BgL_arg1817z00_9239 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9231);
											BgL_arg1816z00_9238 = (BgL_arg1817z00_9239 - OBJECT_TYPE);
										}
										BgL_oclassz00_9236 =
											VECTOR_REF(BgL_arg1815z00_9237, BgL_arg1816z00_9238);
									}
									{	/* SawMill/defs.sch 797 */
										bool_t BgL__ortest_1115z00_9240;

										BgL__ortest_1115z00_9240 =
											(BgL_classz00_9230 == BgL_oclassz00_9236);
										if (BgL__ortest_1115z00_9240)
											{	/* SawMill/defs.sch 797 */
												BgL_res3046z00_9235 = BgL__ortest_1115z00_9240;
											}
										else
											{	/* SawMill/defs.sch 797 */
												long BgL_odepthz00_9241;

												{	/* SawMill/defs.sch 797 */
													obj_t BgL_arg1804z00_9242;

													BgL_arg1804z00_9242 = (BgL_oclassz00_9236);
													BgL_odepthz00_9241 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9242);
												}
												if ((2L < BgL_odepthz00_9241))
													{	/* SawMill/defs.sch 797 */
														obj_t BgL_arg1802z00_9243;

														{	/* SawMill/defs.sch 797 */
															obj_t BgL_arg1803z00_9244;

															BgL_arg1803z00_9244 = (BgL_oclassz00_9236);
															BgL_arg1802z00_9243 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9244,
																2L);
														}
														BgL_res3046z00_9235 =
															(BgL_arg1802z00_9243 == BgL_classz00_9230);
													}
												else
													{	/* SawMill/defs.sch 797 */
														BgL_res3046z00_9235 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3046z00_9235;
							}
					}
				else
					{	/* SawMill/defs.sch 797 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_effect? */
	obj_t BGl_z62rtl_effectzf3z91zzsaw_defsz00(obj_t BgL_envz00_7388,
		obj_t BgL_objz00_7389)
	{
		{	/* SawMill/defs.sch 797 */
			return BBOOL(BGl_rtl_effectzf3zf3zzsaw_defsz00(BgL_objz00_7389));
		}

	}



/* rtl_effect-nil */
	BGL_EXPORTED_DEF BgL_rtl_effectz00_bglt
		BGl_rtl_effectzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 798 */
			{	/* SawMill/defs.sch 798 */
				obj_t BgL_classz00_5429;

				BgL_classz00_5429 = BGl_rtl_effectz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 798 */
					obj_t BgL__ortest_1117z00_5430;

					BgL__ortest_1117z00_5430 = BGL_CLASS_NIL(BgL_classz00_5429);
					if (CBOOL(BgL__ortest_1117z00_5430))
						{	/* SawMill/defs.sch 798 */
							return ((BgL_rtl_effectz00_bglt) BgL__ortest_1117z00_5430);
						}
					else
						{	/* SawMill/defs.sch 798 */
							return
								((BgL_rtl_effectz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5429));
						}
				}
			}
		}

	}



/* &rtl_effect-nil */
	BgL_rtl_effectz00_bglt BGl_z62rtl_effectzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7390)
	{
		{	/* SawMill/defs.sch 798 */
			return BGl_rtl_effectzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_effect-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_effectzd2loczd2zzsaw_defsz00(BgL_rtl_effectz00_bglt BgL_oz00_270)
	{
		{	/* SawMill/defs.sch 799 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_270)))->BgL_locz00);
		}

	}



/* &rtl_effect-loc */
	obj_t BGl_z62rtl_effectzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7391,
		obj_t BgL_oz00_7392)
	{
		{	/* SawMill/defs.sch 799 */
			return
				BGl_rtl_effectzd2loczd2zzsaw_defsz00(
				((BgL_rtl_effectz00_bglt) BgL_oz00_7392));
		}

	}



/* rtl_effect-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_effectzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_effectz00_bglt
		BgL_oz00_271, obj_t BgL_vz00_272)
	{
		{	/* SawMill/defs.sch 800 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_271)))->BgL_locz00) =
				((obj_t) BgL_vz00_272), BUNSPEC);
		}

	}



/* &rtl_effect-loc-set! */
	obj_t BGl_z62rtl_effectzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7393,
		obj_t BgL_oz00_7394, obj_t BgL_vz00_7395)
	{
		{	/* SawMill/defs.sch 800 */
			return
				BGl_rtl_effectzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_effectz00_bglt) BgL_oz00_7394), BgL_vz00_7395);
		}

	}



/* make-rtl_storeg */
	BGL_EXPORTED_DEF BgL_rtl_storegz00_bglt
		BGl_makezd2rtl_storegzd2zzsaw_defsz00(obj_t BgL_loc1406z00_273,
		BgL_globalz00_bglt BgL_var1407z00_274)
	{
		{	/* SawMill/defs.sch 803 */
			{	/* SawMill/defs.sch 803 */
				BgL_rtl_storegz00_bglt BgL_new1408z00_9245;

				{	/* SawMill/defs.sch 803 */
					BgL_rtl_storegz00_bglt BgL_new1407z00_9246;

					BgL_new1407z00_9246 =
						((BgL_rtl_storegz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_storegz00_bgl))));
					{	/* SawMill/defs.sch 803 */
						long BgL_arg1866z00_9247;

						BgL_arg1866z00_9247 = BGL_CLASS_NUM(BGl_rtl_storegz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1407z00_9246), BgL_arg1866z00_9247);
					}
					BgL_new1408z00_9245 = BgL_new1407z00_9246;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1408z00_9245)))->BgL_locz00) =
					((obj_t) BgL_loc1406z00_273), BUNSPEC);
				((((BgL_rtl_storegz00_bglt) COBJECT(BgL_new1408z00_9245))->BgL_varz00) =
					((BgL_globalz00_bglt) BgL_var1407z00_274), BUNSPEC);
				return BgL_new1408z00_9245;
			}
		}

	}



/* &make-rtl_storeg */
	BgL_rtl_storegz00_bglt BGl_z62makezd2rtl_storegzb0zzsaw_defsz00(obj_t
		BgL_envz00_7396, obj_t BgL_loc1406z00_7397, obj_t BgL_var1407z00_7398)
	{
		{	/* SawMill/defs.sch 803 */
			return
				BGl_makezd2rtl_storegzd2zzsaw_defsz00(BgL_loc1406z00_7397,
				((BgL_globalz00_bglt) BgL_var1407z00_7398));
		}

	}



/* rtl_storeg? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_storegzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_275)
	{
		{	/* SawMill/defs.sch 804 */
			{	/* SawMill/defs.sch 804 */
				obj_t BgL_classz00_9248;

				BgL_classz00_9248 = BGl_rtl_storegz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_275))
					{	/* SawMill/defs.sch 804 */
						BgL_objectz00_bglt BgL_arg1807z00_9249;

						BgL_arg1807z00_9249 = (BgL_objectz00_bglt) (BgL_objz00_275);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 804 */
								long BgL_idxz00_9250;

								BgL_idxz00_9250 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9249);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9250 + 3L)) == BgL_classz00_9248);
							}
						else
							{	/* SawMill/defs.sch 804 */
								bool_t BgL_res3047z00_9253;

								{	/* SawMill/defs.sch 804 */
									obj_t BgL_oclassz00_9254;

									{	/* SawMill/defs.sch 804 */
										obj_t BgL_arg1815z00_9255;
										long BgL_arg1816z00_9256;

										BgL_arg1815z00_9255 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 804 */
											long BgL_arg1817z00_9257;

											BgL_arg1817z00_9257 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9249);
											BgL_arg1816z00_9256 = (BgL_arg1817z00_9257 - OBJECT_TYPE);
										}
										BgL_oclassz00_9254 =
											VECTOR_REF(BgL_arg1815z00_9255, BgL_arg1816z00_9256);
									}
									{	/* SawMill/defs.sch 804 */
										bool_t BgL__ortest_1115z00_9258;

										BgL__ortest_1115z00_9258 =
											(BgL_classz00_9248 == BgL_oclassz00_9254);
										if (BgL__ortest_1115z00_9258)
											{	/* SawMill/defs.sch 804 */
												BgL_res3047z00_9253 = BgL__ortest_1115z00_9258;
											}
										else
											{	/* SawMill/defs.sch 804 */
												long BgL_odepthz00_9259;

												{	/* SawMill/defs.sch 804 */
													obj_t BgL_arg1804z00_9260;

													BgL_arg1804z00_9260 = (BgL_oclassz00_9254);
													BgL_odepthz00_9259 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9260);
												}
												if ((3L < BgL_odepthz00_9259))
													{	/* SawMill/defs.sch 804 */
														obj_t BgL_arg1802z00_9261;

														{	/* SawMill/defs.sch 804 */
															obj_t BgL_arg1803z00_9262;

															BgL_arg1803z00_9262 = (BgL_oclassz00_9254);
															BgL_arg1802z00_9261 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9262,
																3L);
														}
														BgL_res3047z00_9253 =
															(BgL_arg1802z00_9261 == BgL_classz00_9248);
													}
												else
													{	/* SawMill/defs.sch 804 */
														BgL_res3047z00_9253 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3047z00_9253;
							}
					}
				else
					{	/* SawMill/defs.sch 804 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_storeg? */
	obj_t BGl_z62rtl_storegzf3z91zzsaw_defsz00(obj_t BgL_envz00_7399,
		obj_t BgL_objz00_7400)
	{
		{	/* SawMill/defs.sch 804 */
			return BBOOL(BGl_rtl_storegzf3zf3zzsaw_defsz00(BgL_objz00_7400));
		}

	}



/* rtl_storeg-nil */
	BGL_EXPORTED_DEF BgL_rtl_storegz00_bglt
		BGl_rtl_storegzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 805 */
			{	/* SawMill/defs.sch 805 */
				obj_t BgL_classz00_5471;

				BgL_classz00_5471 = BGl_rtl_storegz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 805 */
					obj_t BgL__ortest_1117z00_5472;

					BgL__ortest_1117z00_5472 = BGL_CLASS_NIL(BgL_classz00_5471);
					if (CBOOL(BgL__ortest_1117z00_5472))
						{	/* SawMill/defs.sch 805 */
							return ((BgL_rtl_storegz00_bglt) BgL__ortest_1117z00_5472);
						}
					else
						{	/* SawMill/defs.sch 805 */
							return
								((BgL_rtl_storegz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5471));
						}
				}
			}
		}

	}



/* &rtl_storeg-nil */
	BgL_rtl_storegz00_bglt BGl_z62rtl_storegzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7401)
	{
		{	/* SawMill/defs.sch 805 */
			return BGl_rtl_storegzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_storeg-var */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_rtl_storegzd2varzd2zzsaw_defsz00(BgL_rtl_storegz00_bglt BgL_oz00_276)
	{
		{	/* SawMill/defs.sch 806 */
			return (((BgL_rtl_storegz00_bglt) COBJECT(BgL_oz00_276))->BgL_varz00);
		}

	}



/* &rtl_storeg-var */
	BgL_globalz00_bglt BGl_z62rtl_storegzd2varzb0zzsaw_defsz00(obj_t
		BgL_envz00_7402, obj_t BgL_oz00_7403)
	{
		{	/* SawMill/defs.sch 806 */
			return
				BGl_rtl_storegzd2varzd2zzsaw_defsz00(
				((BgL_rtl_storegz00_bglt) BgL_oz00_7403));
		}

	}



/* rtl_storeg-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_storegzd2varzd2setz12z12zzsaw_defsz00(BgL_rtl_storegz00_bglt
		BgL_oz00_277, BgL_globalz00_bglt BgL_vz00_278)
	{
		{	/* SawMill/defs.sch 807 */
			return
				((((BgL_rtl_storegz00_bglt) COBJECT(BgL_oz00_277))->BgL_varz00) =
				((BgL_globalz00_bglt) BgL_vz00_278), BUNSPEC);
		}

	}



/* &rtl_storeg-var-set! */
	obj_t BGl_z62rtl_storegzd2varzd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7404,
		obj_t BgL_oz00_7405, obj_t BgL_vz00_7406)
	{
		{	/* SawMill/defs.sch 807 */
			return
				BGl_rtl_storegzd2varzd2setz12z12zzsaw_defsz00(
				((BgL_rtl_storegz00_bglt) BgL_oz00_7405),
				((BgL_globalz00_bglt) BgL_vz00_7406));
		}

	}



/* rtl_storeg-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_storegzd2loczd2zzsaw_defsz00(BgL_rtl_storegz00_bglt BgL_oz00_279)
	{
		{	/* SawMill/defs.sch 808 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_279)))->BgL_locz00);
		}

	}



/* &rtl_storeg-loc */
	obj_t BGl_z62rtl_storegzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7407,
		obj_t BgL_oz00_7408)
	{
		{	/* SawMill/defs.sch 808 */
			return
				BGl_rtl_storegzd2loczd2zzsaw_defsz00(
				((BgL_rtl_storegz00_bglt) BgL_oz00_7408));
		}

	}



/* rtl_storeg-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_storegzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_storegz00_bglt
		BgL_oz00_280, obj_t BgL_vz00_281)
	{
		{	/* SawMill/defs.sch 809 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_280)))->BgL_locz00) =
				((obj_t) BgL_vz00_281), BUNSPEC);
		}

	}



/* &rtl_storeg-loc-set! */
	obj_t BGl_z62rtl_storegzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7409,
		obj_t BgL_oz00_7410, obj_t BgL_vz00_7411)
	{
		{	/* SawMill/defs.sch 809 */
			return
				BGl_rtl_storegzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_storegz00_bglt) BgL_oz00_7410), BgL_vz00_7411);
		}

	}



/* make-rtl_setfield */
	BGL_EXPORTED_DEF BgL_rtl_setfieldz00_bglt
		BGl_makezd2rtl_setfieldzd2zzsaw_defsz00(obj_t BgL_loc1401z00_282,
		obj_t BgL_name1402z00_283, BgL_typez00_bglt BgL_objtype1403z00_284,
		BgL_typez00_bglt BgL_type1404z00_285)
	{
		{	/* SawMill/defs.sch 812 */
			{	/* SawMill/defs.sch 812 */
				BgL_rtl_setfieldz00_bglt BgL_new1410z00_9263;

				{	/* SawMill/defs.sch 812 */
					BgL_rtl_setfieldz00_bglt BgL_new1409z00_9264;

					BgL_new1409z00_9264 =
						((BgL_rtl_setfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_setfieldz00_bgl))));
					{	/* SawMill/defs.sch 812 */
						long BgL_arg1868z00_9265;

						BgL_arg1868z00_9265 =
							BGL_CLASS_NUM(BGl_rtl_setfieldz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1409z00_9264), BgL_arg1868z00_9265);
					}
					BgL_new1410z00_9263 = BgL_new1409z00_9264;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1410z00_9263)))->BgL_locz00) =
					((obj_t) BgL_loc1401z00_282), BUNSPEC);
				((((BgL_rtl_setfieldz00_bglt) COBJECT(BgL_new1410z00_9263))->
						BgL_namez00) = ((obj_t) BgL_name1402z00_283), BUNSPEC);
				((((BgL_rtl_setfieldz00_bglt) COBJECT(BgL_new1410z00_9263))->
						BgL_objtypez00) =
					((BgL_typez00_bglt) BgL_objtype1403z00_284), BUNSPEC);
				((((BgL_rtl_setfieldz00_bglt) COBJECT(BgL_new1410z00_9263))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1404z00_285), BUNSPEC);
				return BgL_new1410z00_9263;
			}
		}

	}



/* &make-rtl_setfield */
	BgL_rtl_setfieldz00_bglt BGl_z62makezd2rtl_setfieldzb0zzsaw_defsz00(obj_t
		BgL_envz00_7412, obj_t BgL_loc1401z00_7413, obj_t BgL_name1402z00_7414,
		obj_t BgL_objtype1403z00_7415, obj_t BgL_type1404z00_7416)
	{
		{	/* SawMill/defs.sch 812 */
			return
				BGl_makezd2rtl_setfieldzd2zzsaw_defsz00(BgL_loc1401z00_7413,
				BgL_name1402z00_7414, ((BgL_typez00_bglt) BgL_objtype1403z00_7415),
				((BgL_typez00_bglt) BgL_type1404z00_7416));
		}

	}



/* rtl_setfield? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_setfieldzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_286)
	{
		{	/* SawMill/defs.sch 813 */
			{	/* SawMill/defs.sch 813 */
				obj_t BgL_classz00_9266;

				BgL_classz00_9266 = BGl_rtl_setfieldz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_286))
					{	/* SawMill/defs.sch 813 */
						BgL_objectz00_bglt BgL_arg1807z00_9267;

						BgL_arg1807z00_9267 = (BgL_objectz00_bglt) (BgL_objz00_286);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 813 */
								long BgL_idxz00_9268;

								BgL_idxz00_9268 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9267);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9268 + 3L)) == BgL_classz00_9266);
							}
						else
							{	/* SawMill/defs.sch 813 */
								bool_t BgL_res3048z00_9271;

								{	/* SawMill/defs.sch 813 */
									obj_t BgL_oclassz00_9272;

									{	/* SawMill/defs.sch 813 */
										obj_t BgL_arg1815z00_9273;
										long BgL_arg1816z00_9274;

										BgL_arg1815z00_9273 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 813 */
											long BgL_arg1817z00_9275;

											BgL_arg1817z00_9275 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9267);
											BgL_arg1816z00_9274 = (BgL_arg1817z00_9275 - OBJECT_TYPE);
										}
										BgL_oclassz00_9272 =
											VECTOR_REF(BgL_arg1815z00_9273, BgL_arg1816z00_9274);
									}
									{	/* SawMill/defs.sch 813 */
										bool_t BgL__ortest_1115z00_9276;

										BgL__ortest_1115z00_9276 =
											(BgL_classz00_9266 == BgL_oclassz00_9272);
										if (BgL__ortest_1115z00_9276)
											{	/* SawMill/defs.sch 813 */
												BgL_res3048z00_9271 = BgL__ortest_1115z00_9276;
											}
										else
											{	/* SawMill/defs.sch 813 */
												long BgL_odepthz00_9277;

												{	/* SawMill/defs.sch 813 */
													obj_t BgL_arg1804z00_9278;

													BgL_arg1804z00_9278 = (BgL_oclassz00_9272);
													BgL_odepthz00_9277 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9278);
												}
												if ((3L < BgL_odepthz00_9277))
													{	/* SawMill/defs.sch 813 */
														obj_t BgL_arg1802z00_9279;

														{	/* SawMill/defs.sch 813 */
															obj_t BgL_arg1803z00_9280;

															BgL_arg1803z00_9280 = (BgL_oclassz00_9272);
															BgL_arg1802z00_9279 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9280,
																3L);
														}
														BgL_res3048z00_9271 =
															(BgL_arg1802z00_9279 == BgL_classz00_9266);
													}
												else
													{	/* SawMill/defs.sch 813 */
														BgL_res3048z00_9271 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3048z00_9271;
							}
					}
				else
					{	/* SawMill/defs.sch 813 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_setfield? */
	obj_t BGl_z62rtl_setfieldzf3z91zzsaw_defsz00(obj_t BgL_envz00_7417,
		obj_t BgL_objz00_7418)
	{
		{	/* SawMill/defs.sch 813 */
			return BBOOL(BGl_rtl_setfieldzf3zf3zzsaw_defsz00(BgL_objz00_7418));
		}

	}



/* rtl_setfield-nil */
	BGL_EXPORTED_DEF BgL_rtl_setfieldz00_bglt
		BGl_rtl_setfieldzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 814 */
			{	/* SawMill/defs.sch 814 */
				obj_t BgL_classz00_5513;

				BgL_classz00_5513 = BGl_rtl_setfieldz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 814 */
					obj_t BgL__ortest_1117z00_5514;

					BgL__ortest_1117z00_5514 = BGL_CLASS_NIL(BgL_classz00_5513);
					if (CBOOL(BgL__ortest_1117z00_5514))
						{	/* SawMill/defs.sch 814 */
							return ((BgL_rtl_setfieldz00_bglt) BgL__ortest_1117z00_5514);
						}
					else
						{	/* SawMill/defs.sch 814 */
							return
								((BgL_rtl_setfieldz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5513));
						}
				}
			}
		}

	}



/* &rtl_setfield-nil */
	BgL_rtl_setfieldz00_bglt BGl_z62rtl_setfieldzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7419)
	{
		{	/* SawMill/defs.sch 814 */
			return BGl_rtl_setfieldzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_setfield-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_setfieldzd2typezd2zzsaw_defsz00(BgL_rtl_setfieldz00_bglt
		BgL_oz00_287)
	{
		{	/* SawMill/defs.sch 815 */
			return (((BgL_rtl_setfieldz00_bglt) COBJECT(BgL_oz00_287))->BgL_typez00);
		}

	}



/* &rtl_setfield-type */
	BgL_typez00_bglt BGl_z62rtl_setfieldzd2typezb0zzsaw_defsz00(obj_t
		BgL_envz00_7420, obj_t BgL_oz00_7421)
	{
		{	/* SawMill/defs.sch 815 */
			return
				BGl_rtl_setfieldzd2typezd2zzsaw_defsz00(
				((BgL_rtl_setfieldz00_bglt) BgL_oz00_7421));
		}

	}



/* rtl_setfield-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_setfieldzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_setfieldz00_bglt
		BgL_oz00_288, BgL_typez00_bglt BgL_vz00_289)
	{
		{	/* SawMill/defs.sch 816 */
			return
				((((BgL_rtl_setfieldz00_bglt) COBJECT(BgL_oz00_288))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_289), BUNSPEC);
		}

	}



/* &rtl_setfield-type-set! */
	obj_t BGl_z62rtl_setfieldzd2typezd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7422, obj_t BgL_oz00_7423, obj_t BgL_vz00_7424)
	{
		{	/* SawMill/defs.sch 816 */
			return
				BGl_rtl_setfieldzd2typezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_setfieldz00_bglt) BgL_oz00_7423),
				((BgL_typez00_bglt) BgL_vz00_7424));
		}

	}



/* rtl_setfield-objtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_setfieldzd2objtypezd2zzsaw_defsz00(BgL_rtl_setfieldz00_bglt
		BgL_oz00_290)
	{
		{	/* SawMill/defs.sch 817 */
			return
				(((BgL_rtl_setfieldz00_bglt) COBJECT(BgL_oz00_290))->BgL_objtypez00);
		}

	}



/* &rtl_setfield-objtype */
	BgL_typez00_bglt BGl_z62rtl_setfieldzd2objtypezb0zzsaw_defsz00(obj_t
		BgL_envz00_7425, obj_t BgL_oz00_7426)
	{
		{	/* SawMill/defs.sch 817 */
			return
				BGl_rtl_setfieldzd2objtypezd2zzsaw_defsz00(
				((BgL_rtl_setfieldz00_bglt) BgL_oz00_7426));
		}

	}



/* rtl_setfield-objtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_setfieldzd2objtypezd2setz12z12zzsaw_defsz00(BgL_rtl_setfieldz00_bglt
		BgL_oz00_291, BgL_typez00_bglt BgL_vz00_292)
	{
		{	/* SawMill/defs.sch 818 */
			return
				((((BgL_rtl_setfieldz00_bglt) COBJECT(BgL_oz00_291))->BgL_objtypez00) =
				((BgL_typez00_bglt) BgL_vz00_292), BUNSPEC);
		}

	}



/* &rtl_setfield-objtype-set! */
	obj_t BGl_z62rtl_setfieldzd2objtypezd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7427, obj_t BgL_oz00_7428, obj_t BgL_vz00_7429)
	{
		{	/* SawMill/defs.sch 818 */
			return
				BGl_rtl_setfieldzd2objtypezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_setfieldz00_bglt) BgL_oz00_7428),
				((BgL_typez00_bglt) BgL_vz00_7429));
		}

	}



/* rtl_setfield-name */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_setfieldzd2namezd2zzsaw_defsz00(BgL_rtl_setfieldz00_bglt
		BgL_oz00_293)
	{
		{	/* SawMill/defs.sch 819 */
			return (((BgL_rtl_setfieldz00_bglt) COBJECT(BgL_oz00_293))->BgL_namez00);
		}

	}



/* &rtl_setfield-name */
	obj_t BGl_z62rtl_setfieldzd2namezb0zzsaw_defsz00(obj_t BgL_envz00_7430,
		obj_t BgL_oz00_7431)
	{
		{	/* SawMill/defs.sch 819 */
			return
				BGl_rtl_setfieldzd2namezd2zzsaw_defsz00(
				((BgL_rtl_setfieldz00_bglt) BgL_oz00_7431));
		}

	}



/* rtl_setfield-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_setfieldzd2namezd2setz12z12zzsaw_defsz00(BgL_rtl_setfieldz00_bglt
		BgL_oz00_294, obj_t BgL_vz00_295)
	{
		{	/* SawMill/defs.sch 820 */
			return
				((((BgL_rtl_setfieldz00_bglt) COBJECT(BgL_oz00_294))->BgL_namez00) =
				((obj_t) BgL_vz00_295), BUNSPEC);
		}

	}



/* &rtl_setfield-name-set! */
	obj_t BGl_z62rtl_setfieldzd2namezd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7432, obj_t BgL_oz00_7433, obj_t BgL_vz00_7434)
	{
		{	/* SawMill/defs.sch 820 */
			return
				BGl_rtl_setfieldzd2namezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_setfieldz00_bglt) BgL_oz00_7433), BgL_vz00_7434);
		}

	}



/* rtl_setfield-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_setfieldzd2loczd2zzsaw_defsz00(BgL_rtl_setfieldz00_bglt
		BgL_oz00_296)
	{
		{	/* SawMill/defs.sch 821 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_296)))->BgL_locz00);
		}

	}



/* &rtl_setfield-loc */
	obj_t BGl_z62rtl_setfieldzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7435,
		obj_t BgL_oz00_7436)
	{
		{	/* SawMill/defs.sch 821 */
			return
				BGl_rtl_setfieldzd2loczd2zzsaw_defsz00(
				((BgL_rtl_setfieldz00_bglt) BgL_oz00_7436));
		}

	}



/* rtl_setfield-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_setfieldzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_setfieldz00_bglt
		BgL_oz00_297, obj_t BgL_vz00_298)
	{
		{	/* SawMill/defs.sch 822 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_297)))->BgL_locz00) =
				((obj_t) BgL_vz00_298), BUNSPEC);
		}

	}



/* &rtl_setfield-loc-set! */
	obj_t BGl_z62rtl_setfieldzd2loczd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7437, obj_t BgL_oz00_7438, obj_t BgL_vz00_7439)
	{
		{	/* SawMill/defs.sch 822 */
			return
				BGl_rtl_setfieldzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_setfieldz00_bglt) BgL_oz00_7438), BgL_vz00_7439);
		}

	}



/* make-rtl_vset */
	BGL_EXPORTED_DEF BgL_rtl_vsetz00_bglt
		BGl_makezd2rtl_vsetzd2zzsaw_defsz00(obj_t BgL_loc1396z00_299,
		BgL_typez00_bglt BgL_type1397z00_300, BgL_typez00_bglt BgL_vtype1398z00_301)
	{
		{	/* SawMill/defs.sch 825 */
			{	/* SawMill/defs.sch 825 */
				BgL_rtl_vsetz00_bglt BgL_new1412z00_9281;

				{	/* SawMill/defs.sch 825 */
					BgL_rtl_vsetz00_bglt BgL_new1411z00_9282;

					BgL_new1411z00_9282 =
						((BgL_rtl_vsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_vsetz00_bgl))));
					{	/* SawMill/defs.sch 825 */
						long BgL_arg1869z00_9283;

						BgL_arg1869z00_9283 = BGL_CLASS_NUM(BGl_rtl_vsetz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1411z00_9282), BgL_arg1869z00_9283);
					}
					BgL_new1412z00_9281 = BgL_new1411z00_9282;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1412z00_9281)))->BgL_locz00) =
					((obj_t) BgL_loc1396z00_299), BUNSPEC);
				((((BgL_rtl_vsetz00_bglt) COBJECT(BgL_new1412z00_9281))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1397z00_300), BUNSPEC);
				((((BgL_rtl_vsetz00_bglt) COBJECT(BgL_new1412z00_9281))->BgL_vtypez00) =
					((BgL_typez00_bglt) BgL_vtype1398z00_301), BUNSPEC);
				return BgL_new1412z00_9281;
			}
		}

	}



/* &make-rtl_vset */
	BgL_rtl_vsetz00_bglt BGl_z62makezd2rtl_vsetzb0zzsaw_defsz00(obj_t
		BgL_envz00_7440, obj_t BgL_loc1396z00_7441, obj_t BgL_type1397z00_7442,
		obj_t BgL_vtype1398z00_7443)
	{
		{	/* SawMill/defs.sch 825 */
			return
				BGl_makezd2rtl_vsetzd2zzsaw_defsz00(BgL_loc1396z00_7441,
				((BgL_typez00_bglt) BgL_type1397z00_7442),
				((BgL_typez00_bglt) BgL_vtype1398z00_7443));
		}

	}



/* rtl_vset? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_vsetzf3zf3zzsaw_defsz00(obj_t BgL_objz00_302)
	{
		{	/* SawMill/defs.sch 826 */
			{	/* SawMill/defs.sch 826 */
				obj_t BgL_classz00_9284;

				BgL_classz00_9284 = BGl_rtl_vsetz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_302))
					{	/* SawMill/defs.sch 826 */
						BgL_objectz00_bglt BgL_arg1807z00_9285;

						BgL_arg1807z00_9285 = (BgL_objectz00_bglt) (BgL_objz00_302);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 826 */
								long BgL_idxz00_9286;

								BgL_idxz00_9286 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9285);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9286 + 3L)) == BgL_classz00_9284);
							}
						else
							{	/* SawMill/defs.sch 826 */
								bool_t BgL_res3049z00_9289;

								{	/* SawMill/defs.sch 826 */
									obj_t BgL_oclassz00_9290;

									{	/* SawMill/defs.sch 826 */
										obj_t BgL_arg1815z00_9291;
										long BgL_arg1816z00_9292;

										BgL_arg1815z00_9291 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 826 */
											long BgL_arg1817z00_9293;

											BgL_arg1817z00_9293 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9285);
											BgL_arg1816z00_9292 = (BgL_arg1817z00_9293 - OBJECT_TYPE);
										}
										BgL_oclassz00_9290 =
											VECTOR_REF(BgL_arg1815z00_9291, BgL_arg1816z00_9292);
									}
									{	/* SawMill/defs.sch 826 */
										bool_t BgL__ortest_1115z00_9294;

										BgL__ortest_1115z00_9294 =
											(BgL_classz00_9284 == BgL_oclassz00_9290);
										if (BgL__ortest_1115z00_9294)
											{	/* SawMill/defs.sch 826 */
												BgL_res3049z00_9289 = BgL__ortest_1115z00_9294;
											}
										else
											{	/* SawMill/defs.sch 826 */
												long BgL_odepthz00_9295;

												{	/* SawMill/defs.sch 826 */
													obj_t BgL_arg1804z00_9296;

													BgL_arg1804z00_9296 = (BgL_oclassz00_9290);
													BgL_odepthz00_9295 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9296);
												}
												if ((3L < BgL_odepthz00_9295))
													{	/* SawMill/defs.sch 826 */
														obj_t BgL_arg1802z00_9297;

														{	/* SawMill/defs.sch 826 */
															obj_t BgL_arg1803z00_9298;

															BgL_arg1803z00_9298 = (BgL_oclassz00_9290);
															BgL_arg1802z00_9297 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9298,
																3L);
														}
														BgL_res3049z00_9289 =
															(BgL_arg1802z00_9297 == BgL_classz00_9284);
													}
												else
													{	/* SawMill/defs.sch 826 */
														BgL_res3049z00_9289 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3049z00_9289;
							}
					}
				else
					{	/* SawMill/defs.sch 826 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_vset? */
	obj_t BGl_z62rtl_vsetzf3z91zzsaw_defsz00(obj_t BgL_envz00_7444,
		obj_t BgL_objz00_7445)
	{
		{	/* SawMill/defs.sch 826 */
			return BBOOL(BGl_rtl_vsetzf3zf3zzsaw_defsz00(BgL_objz00_7445));
		}

	}



/* rtl_vset-nil */
	BGL_EXPORTED_DEF BgL_rtl_vsetz00_bglt BGl_rtl_vsetzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 827 */
			{	/* SawMill/defs.sch 827 */
				obj_t BgL_classz00_5555;

				BgL_classz00_5555 = BGl_rtl_vsetz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 827 */
					obj_t BgL__ortest_1117z00_5556;

					BgL__ortest_1117z00_5556 = BGL_CLASS_NIL(BgL_classz00_5555);
					if (CBOOL(BgL__ortest_1117z00_5556))
						{	/* SawMill/defs.sch 827 */
							return ((BgL_rtl_vsetz00_bglt) BgL__ortest_1117z00_5556);
						}
					else
						{	/* SawMill/defs.sch 827 */
							return
								((BgL_rtl_vsetz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5555));
						}
				}
			}
		}

	}



/* &rtl_vset-nil */
	BgL_rtl_vsetz00_bglt BGl_z62rtl_vsetzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7446)
	{
		{	/* SawMill/defs.sch 827 */
			return BGl_rtl_vsetzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_vset-vtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_vsetzd2vtypezd2zzsaw_defsz00(BgL_rtl_vsetz00_bglt BgL_oz00_303)
	{
		{	/* SawMill/defs.sch 828 */
			return (((BgL_rtl_vsetz00_bglt) COBJECT(BgL_oz00_303))->BgL_vtypez00);
		}

	}



/* &rtl_vset-vtype */
	BgL_typez00_bglt BGl_z62rtl_vsetzd2vtypezb0zzsaw_defsz00(obj_t
		BgL_envz00_7447, obj_t BgL_oz00_7448)
	{
		{	/* SawMill/defs.sch 828 */
			return
				BGl_rtl_vsetzd2vtypezd2zzsaw_defsz00(
				((BgL_rtl_vsetz00_bglt) BgL_oz00_7448));
		}

	}



/* rtl_vset-vtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_vsetzd2vtypezd2setz12z12zzsaw_defsz00(BgL_rtl_vsetz00_bglt
		BgL_oz00_304, BgL_typez00_bglt BgL_vz00_305)
	{
		{	/* SawMill/defs.sch 829 */
			return
				((((BgL_rtl_vsetz00_bglt) COBJECT(BgL_oz00_304))->BgL_vtypez00) =
				((BgL_typez00_bglt) BgL_vz00_305), BUNSPEC);
		}

	}



/* &rtl_vset-vtype-set! */
	obj_t BGl_z62rtl_vsetzd2vtypezd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7449,
		obj_t BgL_oz00_7450, obj_t BgL_vz00_7451)
	{
		{	/* SawMill/defs.sch 829 */
			return
				BGl_rtl_vsetzd2vtypezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_vsetz00_bglt) BgL_oz00_7450),
				((BgL_typez00_bglt) BgL_vz00_7451));
		}

	}



/* rtl_vset-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_vsetzd2typezd2zzsaw_defsz00(BgL_rtl_vsetz00_bglt BgL_oz00_306)
	{
		{	/* SawMill/defs.sch 830 */
			return (((BgL_rtl_vsetz00_bglt) COBJECT(BgL_oz00_306))->BgL_typez00);
		}

	}



/* &rtl_vset-type */
	BgL_typez00_bglt BGl_z62rtl_vsetzd2typezb0zzsaw_defsz00(obj_t BgL_envz00_7452,
		obj_t BgL_oz00_7453)
	{
		{	/* SawMill/defs.sch 830 */
			return
				BGl_rtl_vsetzd2typezd2zzsaw_defsz00(
				((BgL_rtl_vsetz00_bglt) BgL_oz00_7453));
		}

	}



/* rtl_vset-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_vsetzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_vsetz00_bglt
		BgL_oz00_307, BgL_typez00_bglt BgL_vz00_308)
	{
		{	/* SawMill/defs.sch 831 */
			return
				((((BgL_rtl_vsetz00_bglt) COBJECT(BgL_oz00_307))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_308), BUNSPEC);
		}

	}



/* &rtl_vset-type-set! */
	obj_t BGl_z62rtl_vsetzd2typezd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7454,
		obj_t BgL_oz00_7455, obj_t BgL_vz00_7456)
	{
		{	/* SawMill/defs.sch 831 */
			return
				BGl_rtl_vsetzd2typezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_vsetz00_bglt) BgL_oz00_7455),
				((BgL_typez00_bglt) BgL_vz00_7456));
		}

	}



/* rtl_vset-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_vsetzd2loczd2zzsaw_defsz00(BgL_rtl_vsetz00_bglt
		BgL_oz00_309)
	{
		{	/* SawMill/defs.sch 832 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_309)))->BgL_locz00);
		}

	}



/* &rtl_vset-loc */
	obj_t BGl_z62rtl_vsetzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7457,
		obj_t BgL_oz00_7458)
	{
		{	/* SawMill/defs.sch 832 */
			return
				BGl_rtl_vsetzd2loczd2zzsaw_defsz00(
				((BgL_rtl_vsetz00_bglt) BgL_oz00_7458));
		}

	}



/* rtl_vset-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_vsetzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_vsetz00_bglt
		BgL_oz00_310, obj_t BgL_vz00_311)
	{
		{	/* SawMill/defs.sch 833 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_310)))->BgL_locz00) =
				((obj_t) BgL_vz00_311), BUNSPEC);
		}

	}



/* &rtl_vset-loc-set! */
	obj_t BGl_z62rtl_vsetzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7459,
		obj_t BgL_oz00_7460, obj_t BgL_vz00_7461)
	{
		{	/* SawMill/defs.sch 833 */
			return
				BGl_rtl_vsetzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_vsetz00_bglt) BgL_oz00_7460), BgL_vz00_7461);
		}

	}



/* make-rtl_boxset */
	BGL_EXPORTED_DEF BgL_rtl_boxsetz00_bglt
		BGl_makezd2rtl_boxsetzd2zzsaw_defsz00(obj_t BgL_loc1394z00_312)
	{
		{	/* SawMill/defs.sch 836 */
			{	/* SawMill/defs.sch 836 */
				BgL_rtl_boxsetz00_bglt BgL_new1414z00_9299;

				{	/* SawMill/defs.sch 836 */
					BgL_rtl_boxsetz00_bglt BgL_new1413z00_9300;

					BgL_new1413z00_9300 =
						((BgL_rtl_boxsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_boxsetz00_bgl))));
					{	/* SawMill/defs.sch 836 */
						long BgL_arg1870z00_9301;

						BgL_arg1870z00_9301 = BGL_CLASS_NUM(BGl_rtl_boxsetz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1413z00_9300), BgL_arg1870z00_9301);
					}
					BgL_new1414z00_9299 = BgL_new1413z00_9300;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1414z00_9299)))->BgL_locz00) =
					((obj_t) BgL_loc1394z00_312), BUNSPEC);
				return BgL_new1414z00_9299;
			}
		}

	}



/* &make-rtl_boxset */
	BgL_rtl_boxsetz00_bglt BGl_z62makezd2rtl_boxsetzb0zzsaw_defsz00(obj_t
		BgL_envz00_7462, obj_t BgL_loc1394z00_7463)
	{
		{	/* SawMill/defs.sch 836 */
			return BGl_makezd2rtl_boxsetzd2zzsaw_defsz00(BgL_loc1394z00_7463);
		}

	}



/* rtl_boxset? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_boxsetzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_313)
	{
		{	/* SawMill/defs.sch 837 */
			{	/* SawMill/defs.sch 837 */
				obj_t BgL_classz00_9302;

				BgL_classz00_9302 = BGl_rtl_boxsetz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_313))
					{	/* SawMill/defs.sch 837 */
						BgL_objectz00_bglt BgL_arg1807z00_9303;

						BgL_arg1807z00_9303 = (BgL_objectz00_bglt) (BgL_objz00_313);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 837 */
								long BgL_idxz00_9304;

								BgL_idxz00_9304 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9303);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9304 + 3L)) == BgL_classz00_9302);
							}
						else
							{	/* SawMill/defs.sch 837 */
								bool_t BgL_res3050z00_9307;

								{	/* SawMill/defs.sch 837 */
									obj_t BgL_oclassz00_9308;

									{	/* SawMill/defs.sch 837 */
										obj_t BgL_arg1815z00_9309;
										long BgL_arg1816z00_9310;

										BgL_arg1815z00_9309 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 837 */
											long BgL_arg1817z00_9311;

											BgL_arg1817z00_9311 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9303);
											BgL_arg1816z00_9310 = (BgL_arg1817z00_9311 - OBJECT_TYPE);
										}
										BgL_oclassz00_9308 =
											VECTOR_REF(BgL_arg1815z00_9309, BgL_arg1816z00_9310);
									}
									{	/* SawMill/defs.sch 837 */
										bool_t BgL__ortest_1115z00_9312;

										BgL__ortest_1115z00_9312 =
											(BgL_classz00_9302 == BgL_oclassz00_9308);
										if (BgL__ortest_1115z00_9312)
											{	/* SawMill/defs.sch 837 */
												BgL_res3050z00_9307 = BgL__ortest_1115z00_9312;
											}
										else
											{	/* SawMill/defs.sch 837 */
												long BgL_odepthz00_9313;

												{	/* SawMill/defs.sch 837 */
													obj_t BgL_arg1804z00_9314;

													BgL_arg1804z00_9314 = (BgL_oclassz00_9308);
													BgL_odepthz00_9313 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9314);
												}
												if ((3L < BgL_odepthz00_9313))
													{	/* SawMill/defs.sch 837 */
														obj_t BgL_arg1802z00_9315;

														{	/* SawMill/defs.sch 837 */
															obj_t BgL_arg1803z00_9316;

															BgL_arg1803z00_9316 = (BgL_oclassz00_9308);
															BgL_arg1802z00_9315 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9316,
																3L);
														}
														BgL_res3050z00_9307 =
															(BgL_arg1802z00_9315 == BgL_classz00_9302);
													}
												else
													{	/* SawMill/defs.sch 837 */
														BgL_res3050z00_9307 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3050z00_9307;
							}
					}
				else
					{	/* SawMill/defs.sch 837 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_boxset? */
	obj_t BGl_z62rtl_boxsetzf3z91zzsaw_defsz00(obj_t BgL_envz00_7464,
		obj_t BgL_objz00_7465)
	{
		{	/* SawMill/defs.sch 837 */
			return BBOOL(BGl_rtl_boxsetzf3zf3zzsaw_defsz00(BgL_objz00_7465));
		}

	}



/* rtl_boxset-nil */
	BGL_EXPORTED_DEF BgL_rtl_boxsetz00_bglt
		BGl_rtl_boxsetzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 838 */
			{	/* SawMill/defs.sch 838 */
				obj_t BgL_classz00_5597;

				BgL_classz00_5597 = BGl_rtl_boxsetz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 838 */
					obj_t BgL__ortest_1117z00_5598;

					BgL__ortest_1117z00_5598 = BGL_CLASS_NIL(BgL_classz00_5597);
					if (CBOOL(BgL__ortest_1117z00_5598))
						{	/* SawMill/defs.sch 838 */
							return ((BgL_rtl_boxsetz00_bglt) BgL__ortest_1117z00_5598);
						}
					else
						{	/* SawMill/defs.sch 838 */
							return
								((BgL_rtl_boxsetz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5597));
						}
				}
			}
		}

	}



/* &rtl_boxset-nil */
	BgL_rtl_boxsetz00_bglt BGl_z62rtl_boxsetzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7466)
	{
		{	/* SawMill/defs.sch 838 */
			return BGl_rtl_boxsetzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_boxset-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_boxsetzd2loczd2zzsaw_defsz00(BgL_rtl_boxsetz00_bglt BgL_oz00_314)
	{
		{	/* SawMill/defs.sch 839 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_314)))->BgL_locz00);
		}

	}



/* &rtl_boxset-loc */
	obj_t BGl_z62rtl_boxsetzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7467,
		obj_t BgL_oz00_7468)
	{
		{	/* SawMill/defs.sch 839 */
			return
				BGl_rtl_boxsetzd2loczd2zzsaw_defsz00(
				((BgL_rtl_boxsetz00_bglt) BgL_oz00_7468));
		}

	}



/* rtl_boxset-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_boxsetzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_boxsetz00_bglt
		BgL_oz00_315, obj_t BgL_vz00_316)
	{
		{	/* SawMill/defs.sch 840 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_315)))->BgL_locz00) =
				((obj_t) BgL_vz00_316), BUNSPEC);
		}

	}



/* &rtl_boxset-loc-set! */
	obj_t BGl_z62rtl_boxsetzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7469,
		obj_t BgL_oz00_7470, obj_t BgL_vz00_7471)
	{
		{	/* SawMill/defs.sch 840 */
			return
				BGl_rtl_boxsetzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_boxsetz00_bglt) BgL_oz00_7470), BgL_vz00_7471);
		}

	}



/* make-rtl_new */
	BGL_EXPORTED_DEF BgL_rtl_newz00_bglt BGl_makezd2rtl_newzd2zzsaw_defsz00(obj_t
		BgL_loc1390z00_317, BgL_typez00_bglt BgL_type1391z00_318,
		obj_t BgL_constr1392z00_319)
	{
		{	/* SawMill/defs.sch 843 */
			{	/* SawMill/defs.sch 843 */
				BgL_rtl_newz00_bglt BgL_new1416z00_9317;

				{	/* SawMill/defs.sch 843 */
					BgL_rtl_newz00_bglt BgL_new1415z00_9318;

					BgL_new1415z00_9318 =
						((BgL_rtl_newz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_newz00_bgl))));
					{	/* SawMill/defs.sch 843 */
						long BgL_arg1872z00_9319;

						BgL_arg1872z00_9319 = BGL_CLASS_NUM(BGl_rtl_newz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1415z00_9318), BgL_arg1872z00_9319);
					}
					BgL_new1416z00_9317 = BgL_new1415z00_9318;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1416z00_9317)))->BgL_locz00) =
					((obj_t) BgL_loc1390z00_317), BUNSPEC);
				((((BgL_rtl_newz00_bglt) COBJECT(BgL_new1416z00_9317))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1391z00_318), BUNSPEC);
				((((BgL_rtl_newz00_bglt) COBJECT(BgL_new1416z00_9317))->BgL_constrz00) =
					((obj_t) BgL_constr1392z00_319), BUNSPEC);
				return BgL_new1416z00_9317;
			}
		}

	}



/* &make-rtl_new */
	BgL_rtl_newz00_bglt BGl_z62makezd2rtl_newzb0zzsaw_defsz00(obj_t
		BgL_envz00_7472, obj_t BgL_loc1390z00_7473, obj_t BgL_type1391z00_7474,
		obj_t BgL_constr1392z00_7475)
	{
		{	/* SawMill/defs.sch 843 */
			return
				BGl_makezd2rtl_newzd2zzsaw_defsz00(BgL_loc1390z00_7473,
				((BgL_typez00_bglt) BgL_type1391z00_7474), BgL_constr1392z00_7475);
		}

	}



/* rtl_new? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_newzf3zf3zzsaw_defsz00(obj_t BgL_objz00_320)
	{
		{	/* SawMill/defs.sch 844 */
			{	/* SawMill/defs.sch 844 */
				obj_t BgL_classz00_9320;

				BgL_classz00_9320 = BGl_rtl_newz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_320))
					{	/* SawMill/defs.sch 844 */
						BgL_objectz00_bglt BgL_arg1807z00_9321;

						BgL_arg1807z00_9321 = (BgL_objectz00_bglt) (BgL_objz00_320);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 844 */
								long BgL_idxz00_9322;

								BgL_idxz00_9322 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9321);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9322 + 2L)) == BgL_classz00_9320);
							}
						else
							{	/* SawMill/defs.sch 844 */
								bool_t BgL_res3051z00_9325;

								{	/* SawMill/defs.sch 844 */
									obj_t BgL_oclassz00_9326;

									{	/* SawMill/defs.sch 844 */
										obj_t BgL_arg1815z00_9327;
										long BgL_arg1816z00_9328;

										BgL_arg1815z00_9327 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 844 */
											long BgL_arg1817z00_9329;

											BgL_arg1817z00_9329 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9321);
											BgL_arg1816z00_9328 = (BgL_arg1817z00_9329 - OBJECT_TYPE);
										}
										BgL_oclassz00_9326 =
											VECTOR_REF(BgL_arg1815z00_9327, BgL_arg1816z00_9328);
									}
									{	/* SawMill/defs.sch 844 */
										bool_t BgL__ortest_1115z00_9330;

										BgL__ortest_1115z00_9330 =
											(BgL_classz00_9320 == BgL_oclassz00_9326);
										if (BgL__ortest_1115z00_9330)
											{	/* SawMill/defs.sch 844 */
												BgL_res3051z00_9325 = BgL__ortest_1115z00_9330;
											}
										else
											{	/* SawMill/defs.sch 844 */
												long BgL_odepthz00_9331;

												{	/* SawMill/defs.sch 844 */
													obj_t BgL_arg1804z00_9332;

													BgL_arg1804z00_9332 = (BgL_oclassz00_9326);
													BgL_odepthz00_9331 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9332);
												}
												if ((2L < BgL_odepthz00_9331))
													{	/* SawMill/defs.sch 844 */
														obj_t BgL_arg1802z00_9333;

														{	/* SawMill/defs.sch 844 */
															obj_t BgL_arg1803z00_9334;

															BgL_arg1803z00_9334 = (BgL_oclassz00_9326);
															BgL_arg1802z00_9333 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9334,
																2L);
														}
														BgL_res3051z00_9325 =
															(BgL_arg1802z00_9333 == BgL_classz00_9320);
													}
												else
													{	/* SawMill/defs.sch 844 */
														BgL_res3051z00_9325 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3051z00_9325;
							}
					}
				else
					{	/* SawMill/defs.sch 844 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_new? */
	obj_t BGl_z62rtl_newzf3z91zzsaw_defsz00(obj_t BgL_envz00_7476,
		obj_t BgL_objz00_7477)
	{
		{	/* SawMill/defs.sch 844 */
			return BBOOL(BGl_rtl_newzf3zf3zzsaw_defsz00(BgL_objz00_7477));
		}

	}



/* rtl_new-nil */
	BGL_EXPORTED_DEF BgL_rtl_newz00_bglt BGl_rtl_newzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 845 */
			{	/* SawMill/defs.sch 845 */
				obj_t BgL_classz00_5639;

				BgL_classz00_5639 = BGl_rtl_newz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 845 */
					obj_t BgL__ortest_1117z00_5640;

					BgL__ortest_1117z00_5640 = BGL_CLASS_NIL(BgL_classz00_5639);
					if (CBOOL(BgL__ortest_1117z00_5640))
						{	/* SawMill/defs.sch 845 */
							return ((BgL_rtl_newz00_bglt) BgL__ortest_1117z00_5640);
						}
					else
						{	/* SawMill/defs.sch 845 */
							return
								((BgL_rtl_newz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5639));
						}
				}
			}
		}

	}



/* &rtl_new-nil */
	BgL_rtl_newz00_bglt BGl_z62rtl_newzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7478)
	{
		{	/* SawMill/defs.sch 845 */
			return BGl_rtl_newzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_new-constr */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_newzd2constrzd2zzsaw_defsz00(BgL_rtl_newz00_bglt BgL_oz00_321)
	{
		{	/* SawMill/defs.sch 846 */
			return (((BgL_rtl_newz00_bglt) COBJECT(BgL_oz00_321))->BgL_constrz00);
		}

	}



/* &rtl_new-constr */
	obj_t BGl_z62rtl_newzd2constrzb0zzsaw_defsz00(obj_t BgL_envz00_7479,
		obj_t BgL_oz00_7480)
	{
		{	/* SawMill/defs.sch 846 */
			return
				BGl_rtl_newzd2constrzd2zzsaw_defsz00(
				((BgL_rtl_newz00_bglt) BgL_oz00_7480));
		}

	}



/* rtl_new-constr-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_newzd2constrzd2setz12z12zzsaw_defsz00(BgL_rtl_newz00_bglt
		BgL_oz00_322, obj_t BgL_vz00_323)
	{
		{	/* SawMill/defs.sch 847 */
			return
				((((BgL_rtl_newz00_bglt) COBJECT(BgL_oz00_322))->BgL_constrz00) =
				((obj_t) BgL_vz00_323), BUNSPEC);
		}

	}



/* &rtl_new-constr-set! */
	obj_t BGl_z62rtl_newzd2constrzd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7481,
		obj_t BgL_oz00_7482, obj_t BgL_vz00_7483)
	{
		{	/* SawMill/defs.sch 847 */
			return
				BGl_rtl_newzd2constrzd2setz12z12zzsaw_defsz00(
				((BgL_rtl_newz00_bglt) BgL_oz00_7482), BgL_vz00_7483);
		}

	}



/* rtl_new-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_newzd2typezd2zzsaw_defsz00(BgL_rtl_newz00_bglt BgL_oz00_324)
	{
		{	/* SawMill/defs.sch 848 */
			return (((BgL_rtl_newz00_bglt) COBJECT(BgL_oz00_324))->BgL_typez00);
		}

	}



/* &rtl_new-type */
	BgL_typez00_bglt BGl_z62rtl_newzd2typezb0zzsaw_defsz00(obj_t BgL_envz00_7484,
		obj_t BgL_oz00_7485)
	{
		{	/* SawMill/defs.sch 848 */
			return
				BGl_rtl_newzd2typezd2zzsaw_defsz00(
				((BgL_rtl_newz00_bglt) BgL_oz00_7485));
		}

	}



/* rtl_new-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_newzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_newz00_bglt
		BgL_oz00_325, BgL_typez00_bglt BgL_vz00_326)
	{
		{	/* SawMill/defs.sch 849 */
			return
				((((BgL_rtl_newz00_bglt) COBJECT(BgL_oz00_325))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_326), BUNSPEC);
		}

	}



/* &rtl_new-type-set! */
	obj_t BGl_z62rtl_newzd2typezd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7486,
		obj_t BgL_oz00_7487, obj_t BgL_vz00_7488)
	{
		{	/* SawMill/defs.sch 849 */
			return
				BGl_rtl_newzd2typezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_newz00_bglt) BgL_oz00_7487),
				((BgL_typez00_bglt) BgL_vz00_7488));
		}

	}



/* rtl_new-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_newzd2loczd2zzsaw_defsz00(BgL_rtl_newz00_bglt
		BgL_oz00_327)
	{
		{	/* SawMill/defs.sch 850 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_327)))->BgL_locz00);
		}

	}



/* &rtl_new-loc */
	obj_t BGl_z62rtl_newzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7489,
		obj_t BgL_oz00_7490)
	{
		{	/* SawMill/defs.sch 850 */
			return
				BGl_rtl_newzd2loczd2zzsaw_defsz00(
				((BgL_rtl_newz00_bglt) BgL_oz00_7490));
		}

	}



/* rtl_new-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_newzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_newz00_bglt BgL_oz00_328,
		obj_t BgL_vz00_329)
	{
		{	/* SawMill/defs.sch 851 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_328)))->BgL_locz00) =
				((obj_t) BgL_vz00_329), BUNSPEC);
		}

	}



/* &rtl_new-loc-set! */
	obj_t BGl_z62rtl_newzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7491,
		obj_t BgL_oz00_7492, obj_t BgL_vz00_7493)
	{
		{	/* SawMill/defs.sch 851 */
			return
				BGl_rtl_newzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_newz00_bglt) BgL_oz00_7492), BgL_vz00_7493);
		}

	}



/* make-rtl_call */
	BGL_EXPORTED_DEF BgL_rtl_callz00_bglt
		BGl_makezd2rtl_callzd2zzsaw_defsz00(obj_t BgL_loc1387z00_330,
		BgL_globalz00_bglt BgL_var1388z00_331)
	{
		{	/* SawMill/defs.sch 854 */
			{	/* SawMill/defs.sch 854 */
				BgL_rtl_callz00_bglt BgL_new1418z00_9335;

				{	/* SawMill/defs.sch 854 */
					BgL_rtl_callz00_bglt BgL_new1417z00_9336;

					BgL_new1417z00_9336 =
						((BgL_rtl_callz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_callz00_bgl))));
					{	/* SawMill/defs.sch 854 */
						long BgL_arg1873z00_9337;

						BgL_arg1873z00_9337 = BGL_CLASS_NUM(BGl_rtl_callz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1417z00_9336), BgL_arg1873z00_9337);
					}
					BgL_new1418z00_9335 = BgL_new1417z00_9336;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1418z00_9335)))->BgL_locz00) =
					((obj_t) BgL_loc1387z00_330), BUNSPEC);
				((((BgL_rtl_callz00_bglt) COBJECT(BgL_new1418z00_9335))->BgL_varz00) =
					((BgL_globalz00_bglt) BgL_var1388z00_331), BUNSPEC);
				return BgL_new1418z00_9335;
			}
		}

	}



/* &make-rtl_call */
	BgL_rtl_callz00_bglt BGl_z62makezd2rtl_callzb0zzsaw_defsz00(obj_t
		BgL_envz00_7494, obj_t BgL_loc1387z00_7495, obj_t BgL_var1388z00_7496)
	{
		{	/* SawMill/defs.sch 854 */
			return
				BGl_makezd2rtl_callzd2zzsaw_defsz00(BgL_loc1387z00_7495,
				((BgL_globalz00_bglt) BgL_var1388z00_7496));
		}

	}



/* rtl_call? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_callzf3zf3zzsaw_defsz00(obj_t BgL_objz00_332)
	{
		{	/* SawMill/defs.sch 855 */
			{	/* SawMill/defs.sch 855 */
				obj_t BgL_classz00_9338;

				BgL_classz00_9338 = BGl_rtl_callz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_332))
					{	/* SawMill/defs.sch 855 */
						BgL_objectz00_bglt BgL_arg1807z00_9339;

						BgL_arg1807z00_9339 = (BgL_objectz00_bglt) (BgL_objz00_332);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 855 */
								long BgL_idxz00_9340;

								BgL_idxz00_9340 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9339);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9340 + 2L)) == BgL_classz00_9338);
							}
						else
							{	/* SawMill/defs.sch 855 */
								bool_t BgL_res3052z00_9343;

								{	/* SawMill/defs.sch 855 */
									obj_t BgL_oclassz00_9344;

									{	/* SawMill/defs.sch 855 */
										obj_t BgL_arg1815z00_9345;
										long BgL_arg1816z00_9346;

										BgL_arg1815z00_9345 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 855 */
											long BgL_arg1817z00_9347;

											BgL_arg1817z00_9347 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9339);
											BgL_arg1816z00_9346 = (BgL_arg1817z00_9347 - OBJECT_TYPE);
										}
										BgL_oclassz00_9344 =
											VECTOR_REF(BgL_arg1815z00_9345, BgL_arg1816z00_9346);
									}
									{	/* SawMill/defs.sch 855 */
										bool_t BgL__ortest_1115z00_9348;

										BgL__ortest_1115z00_9348 =
											(BgL_classz00_9338 == BgL_oclassz00_9344);
										if (BgL__ortest_1115z00_9348)
											{	/* SawMill/defs.sch 855 */
												BgL_res3052z00_9343 = BgL__ortest_1115z00_9348;
											}
										else
											{	/* SawMill/defs.sch 855 */
												long BgL_odepthz00_9349;

												{	/* SawMill/defs.sch 855 */
													obj_t BgL_arg1804z00_9350;

													BgL_arg1804z00_9350 = (BgL_oclassz00_9344);
													BgL_odepthz00_9349 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9350);
												}
												if ((2L < BgL_odepthz00_9349))
													{	/* SawMill/defs.sch 855 */
														obj_t BgL_arg1802z00_9351;

														{	/* SawMill/defs.sch 855 */
															obj_t BgL_arg1803z00_9352;

															BgL_arg1803z00_9352 = (BgL_oclassz00_9344);
															BgL_arg1802z00_9351 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9352,
																2L);
														}
														BgL_res3052z00_9343 =
															(BgL_arg1802z00_9351 == BgL_classz00_9338);
													}
												else
													{	/* SawMill/defs.sch 855 */
														BgL_res3052z00_9343 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3052z00_9343;
							}
					}
				else
					{	/* SawMill/defs.sch 855 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_call? */
	obj_t BGl_z62rtl_callzf3z91zzsaw_defsz00(obj_t BgL_envz00_7497,
		obj_t BgL_objz00_7498)
	{
		{	/* SawMill/defs.sch 855 */
			return BBOOL(BGl_rtl_callzf3zf3zzsaw_defsz00(BgL_objz00_7498));
		}

	}



/* rtl_call-nil */
	BGL_EXPORTED_DEF BgL_rtl_callz00_bglt BGl_rtl_callzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 856 */
			{	/* SawMill/defs.sch 856 */
				obj_t BgL_classz00_5681;

				BgL_classz00_5681 = BGl_rtl_callz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 856 */
					obj_t BgL__ortest_1117z00_5682;

					BgL__ortest_1117z00_5682 = BGL_CLASS_NIL(BgL_classz00_5681);
					if (CBOOL(BgL__ortest_1117z00_5682))
						{	/* SawMill/defs.sch 856 */
							return ((BgL_rtl_callz00_bglt) BgL__ortest_1117z00_5682);
						}
					else
						{	/* SawMill/defs.sch 856 */
							return
								((BgL_rtl_callz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5681));
						}
				}
			}
		}

	}



/* &rtl_call-nil */
	BgL_rtl_callz00_bglt BGl_z62rtl_callzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7499)
	{
		{	/* SawMill/defs.sch 856 */
			return BGl_rtl_callzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_call-var */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_rtl_callzd2varzd2zzsaw_defsz00(BgL_rtl_callz00_bglt BgL_oz00_333)
	{
		{	/* SawMill/defs.sch 857 */
			return (((BgL_rtl_callz00_bglt) COBJECT(BgL_oz00_333))->BgL_varz00);
		}

	}



/* &rtl_call-var */
	BgL_globalz00_bglt BGl_z62rtl_callzd2varzb0zzsaw_defsz00(obj_t
		BgL_envz00_7500, obj_t BgL_oz00_7501)
	{
		{	/* SawMill/defs.sch 857 */
			return
				BGl_rtl_callzd2varzd2zzsaw_defsz00(
				((BgL_rtl_callz00_bglt) BgL_oz00_7501));
		}

	}



/* rtl_call-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_callzd2varzd2setz12z12zzsaw_defsz00(BgL_rtl_callz00_bglt
		BgL_oz00_334, BgL_globalz00_bglt BgL_vz00_335)
	{
		{	/* SawMill/defs.sch 858 */
			return
				((((BgL_rtl_callz00_bglt) COBJECT(BgL_oz00_334))->BgL_varz00) =
				((BgL_globalz00_bglt) BgL_vz00_335), BUNSPEC);
		}

	}



/* &rtl_call-var-set! */
	obj_t BGl_z62rtl_callzd2varzd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7502,
		obj_t BgL_oz00_7503, obj_t BgL_vz00_7504)
	{
		{	/* SawMill/defs.sch 858 */
			return
				BGl_rtl_callzd2varzd2setz12z12zzsaw_defsz00(
				((BgL_rtl_callz00_bglt) BgL_oz00_7503),
				((BgL_globalz00_bglt) BgL_vz00_7504));
		}

	}



/* rtl_call-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_callzd2loczd2zzsaw_defsz00(BgL_rtl_callz00_bglt
		BgL_oz00_336)
	{
		{	/* SawMill/defs.sch 859 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_336)))->BgL_locz00);
		}

	}



/* &rtl_call-loc */
	obj_t BGl_z62rtl_callzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7505,
		obj_t BgL_oz00_7506)
	{
		{	/* SawMill/defs.sch 859 */
			return
				BGl_rtl_callzd2loczd2zzsaw_defsz00(
				((BgL_rtl_callz00_bglt) BgL_oz00_7506));
		}

	}



/* rtl_call-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_callzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_callz00_bglt
		BgL_oz00_337, obj_t BgL_vz00_338)
	{
		{	/* SawMill/defs.sch 860 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_337)))->BgL_locz00) =
				((obj_t) BgL_vz00_338), BUNSPEC);
		}

	}



/* &rtl_call-loc-set! */
	obj_t BGl_z62rtl_callzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7507,
		obj_t BgL_oz00_7508, obj_t BgL_vz00_7509)
	{
		{	/* SawMill/defs.sch 860 */
			return
				BGl_rtl_callzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_callz00_bglt) BgL_oz00_7508), BgL_vz00_7509);
		}

	}



/* make-rtl_apply */
	BGL_EXPORTED_DEF BgL_rtl_applyz00_bglt
		BGl_makezd2rtl_applyzd2zzsaw_defsz00(obj_t BgL_loc1385z00_339)
	{
		{	/* SawMill/defs.sch 863 */
			{	/* SawMill/defs.sch 863 */
				BgL_rtl_applyz00_bglt BgL_new1420z00_9353;

				{	/* SawMill/defs.sch 863 */
					BgL_rtl_applyz00_bglt BgL_new1419z00_9354;

					BgL_new1419z00_9354 =
						((BgL_rtl_applyz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_applyz00_bgl))));
					{	/* SawMill/defs.sch 863 */
						long BgL_arg1874z00_9355;

						BgL_arg1874z00_9355 = BGL_CLASS_NUM(BGl_rtl_applyz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1419z00_9354), BgL_arg1874z00_9355);
					}
					BgL_new1420z00_9353 = BgL_new1419z00_9354;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1420z00_9353)))->BgL_locz00) =
					((obj_t) BgL_loc1385z00_339), BUNSPEC);
				return BgL_new1420z00_9353;
			}
		}

	}



/* &make-rtl_apply */
	BgL_rtl_applyz00_bglt BGl_z62makezd2rtl_applyzb0zzsaw_defsz00(obj_t
		BgL_envz00_7510, obj_t BgL_loc1385z00_7511)
	{
		{	/* SawMill/defs.sch 863 */
			return BGl_makezd2rtl_applyzd2zzsaw_defsz00(BgL_loc1385z00_7511);
		}

	}



/* rtl_apply? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_applyzf3zf3zzsaw_defsz00(obj_t BgL_objz00_340)
	{
		{	/* SawMill/defs.sch 864 */
			{	/* SawMill/defs.sch 864 */
				obj_t BgL_classz00_9356;

				BgL_classz00_9356 = BGl_rtl_applyz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_340))
					{	/* SawMill/defs.sch 864 */
						BgL_objectz00_bglt BgL_arg1807z00_9357;

						BgL_arg1807z00_9357 = (BgL_objectz00_bglt) (BgL_objz00_340);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 864 */
								long BgL_idxz00_9358;

								BgL_idxz00_9358 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9357);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9358 + 2L)) == BgL_classz00_9356);
							}
						else
							{	/* SawMill/defs.sch 864 */
								bool_t BgL_res3053z00_9361;

								{	/* SawMill/defs.sch 864 */
									obj_t BgL_oclassz00_9362;

									{	/* SawMill/defs.sch 864 */
										obj_t BgL_arg1815z00_9363;
										long BgL_arg1816z00_9364;

										BgL_arg1815z00_9363 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 864 */
											long BgL_arg1817z00_9365;

											BgL_arg1817z00_9365 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9357);
											BgL_arg1816z00_9364 = (BgL_arg1817z00_9365 - OBJECT_TYPE);
										}
										BgL_oclassz00_9362 =
											VECTOR_REF(BgL_arg1815z00_9363, BgL_arg1816z00_9364);
									}
									{	/* SawMill/defs.sch 864 */
										bool_t BgL__ortest_1115z00_9366;

										BgL__ortest_1115z00_9366 =
											(BgL_classz00_9356 == BgL_oclassz00_9362);
										if (BgL__ortest_1115z00_9366)
											{	/* SawMill/defs.sch 864 */
												BgL_res3053z00_9361 = BgL__ortest_1115z00_9366;
											}
										else
											{	/* SawMill/defs.sch 864 */
												long BgL_odepthz00_9367;

												{	/* SawMill/defs.sch 864 */
													obj_t BgL_arg1804z00_9368;

													BgL_arg1804z00_9368 = (BgL_oclassz00_9362);
													BgL_odepthz00_9367 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9368);
												}
												if ((2L < BgL_odepthz00_9367))
													{	/* SawMill/defs.sch 864 */
														obj_t BgL_arg1802z00_9369;

														{	/* SawMill/defs.sch 864 */
															obj_t BgL_arg1803z00_9370;

															BgL_arg1803z00_9370 = (BgL_oclassz00_9362);
															BgL_arg1802z00_9369 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9370,
																2L);
														}
														BgL_res3053z00_9361 =
															(BgL_arg1802z00_9369 == BgL_classz00_9356);
													}
												else
													{	/* SawMill/defs.sch 864 */
														BgL_res3053z00_9361 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3053z00_9361;
							}
					}
				else
					{	/* SawMill/defs.sch 864 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_apply? */
	obj_t BGl_z62rtl_applyzf3z91zzsaw_defsz00(obj_t BgL_envz00_7512,
		obj_t BgL_objz00_7513)
	{
		{	/* SawMill/defs.sch 864 */
			return BBOOL(BGl_rtl_applyzf3zf3zzsaw_defsz00(BgL_objz00_7513));
		}

	}



/* rtl_apply-nil */
	BGL_EXPORTED_DEF BgL_rtl_applyz00_bglt
		BGl_rtl_applyzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 865 */
			{	/* SawMill/defs.sch 865 */
				obj_t BgL_classz00_5723;

				BgL_classz00_5723 = BGl_rtl_applyz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 865 */
					obj_t BgL__ortest_1117z00_5724;

					BgL__ortest_1117z00_5724 = BGL_CLASS_NIL(BgL_classz00_5723);
					if (CBOOL(BgL__ortest_1117z00_5724))
						{	/* SawMill/defs.sch 865 */
							return ((BgL_rtl_applyz00_bglt) BgL__ortest_1117z00_5724);
						}
					else
						{	/* SawMill/defs.sch 865 */
							return
								((BgL_rtl_applyz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5723));
						}
				}
			}
		}

	}



/* &rtl_apply-nil */
	BgL_rtl_applyz00_bglt BGl_z62rtl_applyzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7514)
	{
		{	/* SawMill/defs.sch 865 */
			return BGl_rtl_applyzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_apply-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_applyzd2loczd2zzsaw_defsz00(BgL_rtl_applyz00_bglt BgL_oz00_341)
	{
		{	/* SawMill/defs.sch 866 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_341)))->BgL_locz00);
		}

	}



/* &rtl_apply-loc */
	obj_t BGl_z62rtl_applyzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7515,
		obj_t BgL_oz00_7516)
	{
		{	/* SawMill/defs.sch 866 */
			return
				BGl_rtl_applyzd2loczd2zzsaw_defsz00(
				((BgL_rtl_applyz00_bglt) BgL_oz00_7516));
		}

	}



/* rtl_apply-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_applyzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_applyz00_bglt
		BgL_oz00_342, obj_t BgL_vz00_343)
	{
		{	/* SawMill/defs.sch 867 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_342)))->BgL_locz00) =
				((obj_t) BgL_vz00_343), BUNSPEC);
		}

	}



/* &rtl_apply-loc-set! */
	obj_t BGl_z62rtl_applyzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7517,
		obj_t BgL_oz00_7518, obj_t BgL_vz00_7519)
	{
		{	/* SawMill/defs.sch 867 */
			return
				BGl_rtl_applyzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_applyz00_bglt) BgL_oz00_7518), BgL_vz00_7519);
		}

	}



/* make-rtl_lightfuncall */
	BGL_EXPORTED_DEF BgL_rtl_lightfuncallz00_bglt
		BGl_makezd2rtl_lightfuncallzd2zzsaw_defsz00(obj_t BgL_loc1380z00_344,
		obj_t BgL_name1381z00_345, obj_t BgL_funs1382z00_346,
		obj_t BgL_rettype1383z00_347)
	{
		{	/* SawMill/defs.sch 870 */
			{	/* SawMill/defs.sch 870 */
				BgL_rtl_lightfuncallz00_bglt BgL_new1422z00_9371;

				{	/* SawMill/defs.sch 870 */
					BgL_rtl_lightfuncallz00_bglt BgL_new1421z00_9372;

					BgL_new1421z00_9372 =
						((BgL_rtl_lightfuncallz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_lightfuncallz00_bgl))));
					{	/* SawMill/defs.sch 870 */
						long BgL_arg1875z00_9373;

						BgL_arg1875z00_9373 =
							BGL_CLASS_NUM(BGl_rtl_lightfuncallz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1421z00_9372), BgL_arg1875z00_9373);
					}
					BgL_new1422z00_9371 = BgL_new1421z00_9372;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1422z00_9371)))->BgL_locz00) =
					((obj_t) BgL_loc1380z00_344), BUNSPEC);
				((((BgL_rtl_lightfuncallz00_bglt) COBJECT(BgL_new1422z00_9371))->
						BgL_namez00) = ((obj_t) BgL_name1381z00_345), BUNSPEC);
				((((BgL_rtl_lightfuncallz00_bglt) COBJECT(BgL_new1422z00_9371))->
						BgL_funsz00) = ((obj_t) BgL_funs1382z00_346), BUNSPEC);
				((((BgL_rtl_lightfuncallz00_bglt) COBJECT(BgL_new1422z00_9371))->
						BgL_rettypez00) = ((obj_t) BgL_rettype1383z00_347), BUNSPEC);
				return BgL_new1422z00_9371;
			}
		}

	}



/* &make-rtl_lightfuncall */
	BgL_rtl_lightfuncallz00_bglt
		BGl_z62makezd2rtl_lightfuncallzb0zzsaw_defsz00(obj_t BgL_envz00_7520,
		obj_t BgL_loc1380z00_7521, obj_t BgL_name1381z00_7522,
		obj_t BgL_funs1382z00_7523, obj_t BgL_rettype1383z00_7524)
	{
		{	/* SawMill/defs.sch 870 */
			return
				BGl_makezd2rtl_lightfuncallzd2zzsaw_defsz00(BgL_loc1380z00_7521,
				BgL_name1381z00_7522, BgL_funs1382z00_7523, BgL_rettype1383z00_7524);
		}

	}



/* rtl_lightfuncall? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_lightfuncallzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_348)
	{
		{	/* SawMill/defs.sch 871 */
			{	/* SawMill/defs.sch 871 */
				obj_t BgL_classz00_9374;

				BgL_classz00_9374 = BGl_rtl_lightfuncallz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_348))
					{	/* SawMill/defs.sch 871 */
						BgL_objectz00_bglt BgL_arg1807z00_9375;

						BgL_arg1807z00_9375 = (BgL_objectz00_bglt) (BgL_objz00_348);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 871 */
								long BgL_idxz00_9376;

								BgL_idxz00_9376 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9375);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9376 + 2L)) == BgL_classz00_9374);
							}
						else
							{	/* SawMill/defs.sch 871 */
								bool_t BgL_res3054z00_9379;

								{	/* SawMill/defs.sch 871 */
									obj_t BgL_oclassz00_9380;

									{	/* SawMill/defs.sch 871 */
										obj_t BgL_arg1815z00_9381;
										long BgL_arg1816z00_9382;

										BgL_arg1815z00_9381 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 871 */
											long BgL_arg1817z00_9383;

											BgL_arg1817z00_9383 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9375);
											BgL_arg1816z00_9382 = (BgL_arg1817z00_9383 - OBJECT_TYPE);
										}
										BgL_oclassz00_9380 =
											VECTOR_REF(BgL_arg1815z00_9381, BgL_arg1816z00_9382);
									}
									{	/* SawMill/defs.sch 871 */
										bool_t BgL__ortest_1115z00_9384;

										BgL__ortest_1115z00_9384 =
											(BgL_classz00_9374 == BgL_oclassz00_9380);
										if (BgL__ortest_1115z00_9384)
											{	/* SawMill/defs.sch 871 */
												BgL_res3054z00_9379 = BgL__ortest_1115z00_9384;
											}
										else
											{	/* SawMill/defs.sch 871 */
												long BgL_odepthz00_9385;

												{	/* SawMill/defs.sch 871 */
													obj_t BgL_arg1804z00_9386;

													BgL_arg1804z00_9386 = (BgL_oclassz00_9380);
													BgL_odepthz00_9385 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9386);
												}
												if ((2L < BgL_odepthz00_9385))
													{	/* SawMill/defs.sch 871 */
														obj_t BgL_arg1802z00_9387;

														{	/* SawMill/defs.sch 871 */
															obj_t BgL_arg1803z00_9388;

															BgL_arg1803z00_9388 = (BgL_oclassz00_9380);
															BgL_arg1802z00_9387 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9388,
																2L);
														}
														BgL_res3054z00_9379 =
															(BgL_arg1802z00_9387 == BgL_classz00_9374);
													}
												else
													{	/* SawMill/defs.sch 871 */
														BgL_res3054z00_9379 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3054z00_9379;
							}
					}
				else
					{	/* SawMill/defs.sch 871 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_lightfuncall? */
	obj_t BGl_z62rtl_lightfuncallzf3z91zzsaw_defsz00(obj_t BgL_envz00_7525,
		obj_t BgL_objz00_7526)
	{
		{	/* SawMill/defs.sch 871 */
			return BBOOL(BGl_rtl_lightfuncallzf3zf3zzsaw_defsz00(BgL_objz00_7526));
		}

	}



/* rtl_lightfuncall-nil */
	BGL_EXPORTED_DEF BgL_rtl_lightfuncallz00_bglt
		BGl_rtl_lightfuncallzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 872 */
			{	/* SawMill/defs.sch 872 */
				obj_t BgL_classz00_5765;

				BgL_classz00_5765 = BGl_rtl_lightfuncallz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 872 */
					obj_t BgL__ortest_1117z00_5766;

					BgL__ortest_1117z00_5766 = BGL_CLASS_NIL(BgL_classz00_5765);
					if (CBOOL(BgL__ortest_1117z00_5766))
						{	/* SawMill/defs.sch 872 */
							return ((BgL_rtl_lightfuncallz00_bglt) BgL__ortest_1117z00_5766);
						}
					else
						{	/* SawMill/defs.sch 872 */
							return
								((BgL_rtl_lightfuncallz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5765));
						}
				}
			}
		}

	}



/* &rtl_lightfuncall-nil */
	BgL_rtl_lightfuncallz00_bglt
		BGl_z62rtl_lightfuncallzd2nilzb0zzsaw_defsz00(obj_t BgL_envz00_7527)
	{
		{	/* SawMill/defs.sch 872 */
			return BGl_rtl_lightfuncallzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_lightfuncall-rettype */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_lightfuncallzd2rettypezd2zzsaw_defsz00(BgL_rtl_lightfuncallz00_bglt
		BgL_oz00_349)
	{
		{	/* SawMill/defs.sch 873 */
			return
				(((BgL_rtl_lightfuncallz00_bglt) COBJECT(BgL_oz00_349))->
				BgL_rettypez00);
		}

	}



/* &rtl_lightfuncall-rettype */
	obj_t BGl_z62rtl_lightfuncallzd2rettypezb0zzsaw_defsz00(obj_t BgL_envz00_7528,
		obj_t BgL_oz00_7529)
	{
		{	/* SawMill/defs.sch 873 */
			return
				BGl_rtl_lightfuncallzd2rettypezd2zzsaw_defsz00(
				((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_7529));
		}

	}



/* rtl_lightfuncall-rettype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_lightfuncallzd2rettypezd2setz12z12zzsaw_defsz00
		(BgL_rtl_lightfuncallz00_bglt BgL_oz00_350, obj_t BgL_vz00_351)
	{
		{	/* SawMill/defs.sch 874 */
			return
				((((BgL_rtl_lightfuncallz00_bglt) COBJECT(BgL_oz00_350))->
					BgL_rettypez00) = ((obj_t) BgL_vz00_351), BUNSPEC);
		}

	}



/* &rtl_lightfuncall-rettype-set! */
	obj_t BGl_z62rtl_lightfuncallzd2rettypezd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7530, obj_t BgL_oz00_7531, obj_t BgL_vz00_7532)
	{
		{	/* SawMill/defs.sch 874 */
			return
				BGl_rtl_lightfuncallzd2rettypezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_7531), BgL_vz00_7532);
		}

	}



/* rtl_lightfuncall-funs */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_lightfuncallzd2funszd2zzsaw_defsz00(BgL_rtl_lightfuncallz00_bglt
		BgL_oz00_352)
	{
		{	/* SawMill/defs.sch 875 */
			return
				(((BgL_rtl_lightfuncallz00_bglt) COBJECT(BgL_oz00_352))->BgL_funsz00);
		}

	}



/* &rtl_lightfuncall-funs */
	obj_t BGl_z62rtl_lightfuncallzd2funszb0zzsaw_defsz00(obj_t BgL_envz00_7533,
		obj_t BgL_oz00_7534)
	{
		{	/* SawMill/defs.sch 875 */
			return
				BGl_rtl_lightfuncallzd2funszd2zzsaw_defsz00(
				((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_7534));
		}

	}



/* rtl_lightfuncall-funs-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_lightfuncallzd2funszd2setz12z12zzsaw_defsz00
		(BgL_rtl_lightfuncallz00_bglt BgL_oz00_353, obj_t BgL_vz00_354)
	{
		{	/* SawMill/defs.sch 876 */
			return
				((((BgL_rtl_lightfuncallz00_bglt) COBJECT(BgL_oz00_353))->BgL_funsz00) =
				((obj_t) BgL_vz00_354), BUNSPEC);
		}

	}



/* &rtl_lightfuncall-funs-set! */
	obj_t BGl_z62rtl_lightfuncallzd2funszd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7535, obj_t BgL_oz00_7536, obj_t BgL_vz00_7537)
	{
		{	/* SawMill/defs.sch 876 */
			return
				BGl_rtl_lightfuncallzd2funszd2setz12z12zzsaw_defsz00(
				((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_7536), BgL_vz00_7537);
		}

	}



/* rtl_lightfuncall-name */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_lightfuncallzd2namezd2zzsaw_defsz00(BgL_rtl_lightfuncallz00_bglt
		BgL_oz00_355)
	{
		{	/* SawMill/defs.sch 877 */
			return
				(((BgL_rtl_lightfuncallz00_bglt) COBJECT(BgL_oz00_355))->BgL_namez00);
		}

	}



/* &rtl_lightfuncall-name */
	obj_t BGl_z62rtl_lightfuncallzd2namezb0zzsaw_defsz00(obj_t BgL_envz00_7538,
		obj_t BgL_oz00_7539)
	{
		{	/* SawMill/defs.sch 877 */
			return
				BGl_rtl_lightfuncallzd2namezd2zzsaw_defsz00(
				((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_7539));
		}

	}



/* rtl_lightfuncall-name-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_lightfuncallzd2namezd2setz12z12zzsaw_defsz00
		(BgL_rtl_lightfuncallz00_bglt BgL_oz00_356, obj_t BgL_vz00_357)
	{
		{	/* SawMill/defs.sch 878 */
			return
				((((BgL_rtl_lightfuncallz00_bglt) COBJECT(BgL_oz00_356))->BgL_namez00) =
				((obj_t) BgL_vz00_357), BUNSPEC);
		}

	}



/* &rtl_lightfuncall-name-set! */
	obj_t BGl_z62rtl_lightfuncallzd2namezd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7540, obj_t BgL_oz00_7541, obj_t BgL_vz00_7542)
	{
		{	/* SawMill/defs.sch 878 */
			return
				BGl_rtl_lightfuncallzd2namezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_7541), BgL_vz00_7542);
		}

	}



/* rtl_lightfuncall-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_lightfuncallzd2loczd2zzsaw_defsz00(BgL_rtl_lightfuncallz00_bglt
		BgL_oz00_358)
	{
		{	/* SawMill/defs.sch 879 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_358)))->BgL_locz00);
		}

	}



/* &rtl_lightfuncall-loc */
	obj_t BGl_z62rtl_lightfuncallzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7543,
		obj_t BgL_oz00_7544)
	{
		{	/* SawMill/defs.sch 879 */
			return
				BGl_rtl_lightfuncallzd2loczd2zzsaw_defsz00(
				((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_7544));
		}

	}



/* rtl_lightfuncall-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_lightfuncallzd2loczd2setz12z12zzsaw_defsz00
		(BgL_rtl_lightfuncallz00_bglt BgL_oz00_359, obj_t BgL_vz00_360)
	{
		{	/* SawMill/defs.sch 880 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_359)))->BgL_locz00) =
				((obj_t) BgL_vz00_360), BUNSPEC);
		}

	}



/* &rtl_lightfuncall-loc-set! */
	obj_t BGl_z62rtl_lightfuncallzd2loczd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7545, obj_t BgL_oz00_7546, obj_t BgL_vz00_7547)
	{
		{	/* SawMill/defs.sch 880 */
			return
				BGl_rtl_lightfuncallzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_7546), BgL_vz00_7547);
		}

	}



/* make-rtl_funcall */
	BGL_EXPORTED_DEF BgL_rtl_funcallz00_bglt
		BGl_makezd2rtl_funcallzd2zzsaw_defsz00(obj_t BgL_loc1377z00_361)
	{
		{	/* SawMill/defs.sch 883 */
			{	/* SawMill/defs.sch 883 */
				BgL_rtl_funcallz00_bglt BgL_new1424z00_9389;

				{	/* SawMill/defs.sch 883 */
					BgL_rtl_funcallz00_bglt BgL_new1423z00_9390;

					BgL_new1423z00_9390 =
						((BgL_rtl_funcallz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_funcallz00_bgl))));
					{	/* SawMill/defs.sch 883 */
						long BgL_arg1876z00_9391;

						BgL_arg1876z00_9391 =
							BGL_CLASS_NUM(BGl_rtl_funcallz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1423z00_9390), BgL_arg1876z00_9391);
					}
					BgL_new1424z00_9389 = BgL_new1423z00_9390;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1424z00_9389)))->BgL_locz00) =
					((obj_t) BgL_loc1377z00_361), BUNSPEC);
				return BgL_new1424z00_9389;
			}
		}

	}



/* &make-rtl_funcall */
	BgL_rtl_funcallz00_bglt BGl_z62makezd2rtl_funcallzb0zzsaw_defsz00(obj_t
		BgL_envz00_7548, obj_t BgL_loc1377z00_7549)
	{
		{	/* SawMill/defs.sch 883 */
			return BGl_makezd2rtl_funcallzd2zzsaw_defsz00(BgL_loc1377z00_7549);
		}

	}



/* rtl_funcall? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_funcallzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_362)
	{
		{	/* SawMill/defs.sch 884 */
			{	/* SawMill/defs.sch 884 */
				obj_t BgL_classz00_9392;

				BgL_classz00_9392 = BGl_rtl_funcallz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_362))
					{	/* SawMill/defs.sch 884 */
						BgL_objectz00_bglt BgL_arg1807z00_9393;

						BgL_arg1807z00_9393 = (BgL_objectz00_bglt) (BgL_objz00_362);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 884 */
								long BgL_idxz00_9394;

								BgL_idxz00_9394 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9393);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9394 + 2L)) == BgL_classz00_9392);
							}
						else
							{	/* SawMill/defs.sch 884 */
								bool_t BgL_res3055z00_9397;

								{	/* SawMill/defs.sch 884 */
									obj_t BgL_oclassz00_9398;

									{	/* SawMill/defs.sch 884 */
										obj_t BgL_arg1815z00_9399;
										long BgL_arg1816z00_9400;

										BgL_arg1815z00_9399 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 884 */
											long BgL_arg1817z00_9401;

											BgL_arg1817z00_9401 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9393);
											BgL_arg1816z00_9400 = (BgL_arg1817z00_9401 - OBJECT_TYPE);
										}
										BgL_oclassz00_9398 =
											VECTOR_REF(BgL_arg1815z00_9399, BgL_arg1816z00_9400);
									}
									{	/* SawMill/defs.sch 884 */
										bool_t BgL__ortest_1115z00_9402;

										BgL__ortest_1115z00_9402 =
											(BgL_classz00_9392 == BgL_oclassz00_9398);
										if (BgL__ortest_1115z00_9402)
											{	/* SawMill/defs.sch 884 */
												BgL_res3055z00_9397 = BgL__ortest_1115z00_9402;
											}
										else
											{	/* SawMill/defs.sch 884 */
												long BgL_odepthz00_9403;

												{	/* SawMill/defs.sch 884 */
													obj_t BgL_arg1804z00_9404;

													BgL_arg1804z00_9404 = (BgL_oclassz00_9398);
													BgL_odepthz00_9403 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9404);
												}
												if ((2L < BgL_odepthz00_9403))
													{	/* SawMill/defs.sch 884 */
														obj_t BgL_arg1802z00_9405;

														{	/* SawMill/defs.sch 884 */
															obj_t BgL_arg1803z00_9406;

															BgL_arg1803z00_9406 = (BgL_oclassz00_9398);
															BgL_arg1802z00_9405 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9406,
																2L);
														}
														BgL_res3055z00_9397 =
															(BgL_arg1802z00_9405 == BgL_classz00_9392);
													}
												else
													{	/* SawMill/defs.sch 884 */
														BgL_res3055z00_9397 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3055z00_9397;
							}
					}
				else
					{	/* SawMill/defs.sch 884 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_funcall? */
	obj_t BGl_z62rtl_funcallzf3z91zzsaw_defsz00(obj_t BgL_envz00_7550,
		obj_t BgL_objz00_7551)
	{
		{	/* SawMill/defs.sch 884 */
			return BBOOL(BGl_rtl_funcallzf3zf3zzsaw_defsz00(BgL_objz00_7551));
		}

	}



/* rtl_funcall-nil */
	BGL_EXPORTED_DEF BgL_rtl_funcallz00_bglt
		BGl_rtl_funcallzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 885 */
			{	/* SawMill/defs.sch 885 */
				obj_t BgL_classz00_5807;

				BgL_classz00_5807 = BGl_rtl_funcallz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 885 */
					obj_t BgL__ortest_1117z00_5808;

					BgL__ortest_1117z00_5808 = BGL_CLASS_NIL(BgL_classz00_5807);
					if (CBOOL(BgL__ortest_1117z00_5808))
						{	/* SawMill/defs.sch 885 */
							return ((BgL_rtl_funcallz00_bglt) BgL__ortest_1117z00_5808);
						}
					else
						{	/* SawMill/defs.sch 885 */
							return
								((BgL_rtl_funcallz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5807));
						}
				}
			}
		}

	}



/* &rtl_funcall-nil */
	BgL_rtl_funcallz00_bglt BGl_z62rtl_funcallzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7552)
	{
		{	/* SawMill/defs.sch 885 */
			return BGl_rtl_funcallzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_funcall-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_funcallzd2loczd2zzsaw_defsz00(BgL_rtl_funcallz00_bglt BgL_oz00_363)
	{
		{	/* SawMill/defs.sch 886 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_363)))->BgL_locz00);
		}

	}



/* &rtl_funcall-loc */
	obj_t BGl_z62rtl_funcallzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7553,
		obj_t BgL_oz00_7554)
	{
		{	/* SawMill/defs.sch 886 */
			return
				BGl_rtl_funcallzd2loczd2zzsaw_defsz00(
				((BgL_rtl_funcallz00_bglt) BgL_oz00_7554));
		}

	}



/* rtl_funcall-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_funcallzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_funcallz00_bglt
		BgL_oz00_364, obj_t BgL_vz00_365)
	{
		{	/* SawMill/defs.sch 887 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_364)))->BgL_locz00) =
				((obj_t) BgL_vz00_365), BUNSPEC);
		}

	}



/* &rtl_funcall-loc-set! */
	obj_t BGl_z62rtl_funcallzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7555,
		obj_t BgL_oz00_7556, obj_t BgL_vz00_7557)
	{
		{	/* SawMill/defs.sch 887 */
			return
				BGl_rtl_funcallzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_funcallz00_bglt) BgL_oz00_7556), BgL_vz00_7557);
		}

	}



/* make-rtl_pragma */
	BGL_EXPORTED_DEF BgL_rtl_pragmaz00_bglt
		BGl_makezd2rtl_pragmazd2zzsaw_defsz00(obj_t BgL_loc1373z00_366,
		obj_t BgL_format1374z00_367, obj_t BgL_srfi01375z00_368)
	{
		{	/* SawMill/defs.sch 890 */
			{	/* SawMill/defs.sch 890 */
				BgL_rtl_pragmaz00_bglt BgL_new1426z00_9407;

				{	/* SawMill/defs.sch 890 */
					BgL_rtl_pragmaz00_bglt BgL_new1425z00_9408;

					BgL_new1425z00_9408 =
						((BgL_rtl_pragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_pragmaz00_bgl))));
					{	/* SawMill/defs.sch 890 */
						long BgL_arg1877z00_9409;

						BgL_arg1877z00_9409 = BGL_CLASS_NUM(BGl_rtl_pragmaz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1425z00_9408), BgL_arg1877z00_9409);
					}
					BgL_new1426z00_9407 = BgL_new1425z00_9408;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1426z00_9407)))->BgL_locz00) =
					((obj_t) BgL_loc1373z00_366), BUNSPEC);
				((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_new1426z00_9407))->
						BgL_formatz00) = ((obj_t) BgL_format1374z00_367), BUNSPEC);
				((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_new1426z00_9407))->
						BgL_srfi0z00) = ((obj_t) BgL_srfi01375z00_368), BUNSPEC);
				return BgL_new1426z00_9407;
			}
		}

	}



/* &make-rtl_pragma */
	BgL_rtl_pragmaz00_bglt BGl_z62makezd2rtl_pragmazb0zzsaw_defsz00(obj_t
		BgL_envz00_7558, obj_t BgL_loc1373z00_7559, obj_t BgL_format1374z00_7560,
		obj_t BgL_srfi01375z00_7561)
	{
		{	/* SawMill/defs.sch 890 */
			return
				BGl_makezd2rtl_pragmazd2zzsaw_defsz00(BgL_loc1373z00_7559,
				BgL_format1374z00_7560, BgL_srfi01375z00_7561);
		}

	}



/* rtl_pragma? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_pragmazf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_369)
	{
		{	/* SawMill/defs.sch 891 */
			{	/* SawMill/defs.sch 891 */
				obj_t BgL_classz00_9410;

				BgL_classz00_9410 = BGl_rtl_pragmaz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_369))
					{	/* SawMill/defs.sch 891 */
						BgL_objectz00_bglt BgL_arg1807z00_9411;

						BgL_arg1807z00_9411 = (BgL_objectz00_bglt) (BgL_objz00_369);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 891 */
								long BgL_idxz00_9412;

								BgL_idxz00_9412 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9411);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9412 + 2L)) == BgL_classz00_9410);
							}
						else
							{	/* SawMill/defs.sch 891 */
								bool_t BgL_res3056z00_9415;

								{	/* SawMill/defs.sch 891 */
									obj_t BgL_oclassz00_9416;

									{	/* SawMill/defs.sch 891 */
										obj_t BgL_arg1815z00_9417;
										long BgL_arg1816z00_9418;

										BgL_arg1815z00_9417 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 891 */
											long BgL_arg1817z00_9419;

											BgL_arg1817z00_9419 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9411);
											BgL_arg1816z00_9418 = (BgL_arg1817z00_9419 - OBJECT_TYPE);
										}
										BgL_oclassz00_9416 =
											VECTOR_REF(BgL_arg1815z00_9417, BgL_arg1816z00_9418);
									}
									{	/* SawMill/defs.sch 891 */
										bool_t BgL__ortest_1115z00_9420;

										BgL__ortest_1115z00_9420 =
											(BgL_classz00_9410 == BgL_oclassz00_9416);
										if (BgL__ortest_1115z00_9420)
											{	/* SawMill/defs.sch 891 */
												BgL_res3056z00_9415 = BgL__ortest_1115z00_9420;
											}
										else
											{	/* SawMill/defs.sch 891 */
												long BgL_odepthz00_9421;

												{	/* SawMill/defs.sch 891 */
													obj_t BgL_arg1804z00_9422;

													BgL_arg1804z00_9422 = (BgL_oclassz00_9416);
													BgL_odepthz00_9421 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9422);
												}
												if ((2L < BgL_odepthz00_9421))
													{	/* SawMill/defs.sch 891 */
														obj_t BgL_arg1802z00_9423;

														{	/* SawMill/defs.sch 891 */
															obj_t BgL_arg1803z00_9424;

															BgL_arg1803z00_9424 = (BgL_oclassz00_9416);
															BgL_arg1802z00_9423 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9424,
																2L);
														}
														BgL_res3056z00_9415 =
															(BgL_arg1802z00_9423 == BgL_classz00_9410);
													}
												else
													{	/* SawMill/defs.sch 891 */
														BgL_res3056z00_9415 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3056z00_9415;
							}
					}
				else
					{	/* SawMill/defs.sch 891 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_pragma? */
	obj_t BGl_z62rtl_pragmazf3z91zzsaw_defsz00(obj_t BgL_envz00_7562,
		obj_t BgL_objz00_7563)
	{
		{	/* SawMill/defs.sch 891 */
			return BBOOL(BGl_rtl_pragmazf3zf3zzsaw_defsz00(BgL_objz00_7563));
		}

	}



/* rtl_pragma-nil */
	BGL_EXPORTED_DEF BgL_rtl_pragmaz00_bglt
		BGl_rtl_pragmazd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 892 */
			{	/* SawMill/defs.sch 892 */
				obj_t BgL_classz00_5849;

				BgL_classz00_5849 = BGl_rtl_pragmaz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 892 */
					obj_t BgL__ortest_1117z00_5850;

					BgL__ortest_1117z00_5850 = BGL_CLASS_NIL(BgL_classz00_5849);
					if (CBOOL(BgL__ortest_1117z00_5850))
						{	/* SawMill/defs.sch 892 */
							return ((BgL_rtl_pragmaz00_bglt) BgL__ortest_1117z00_5850);
						}
					else
						{	/* SawMill/defs.sch 892 */
							return
								((BgL_rtl_pragmaz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5849));
						}
				}
			}
		}

	}



/* &rtl_pragma-nil */
	BgL_rtl_pragmaz00_bglt BGl_z62rtl_pragmazd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7564)
	{
		{	/* SawMill/defs.sch 892 */
			return BGl_rtl_pragmazd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_pragma-srfi0 */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_pragmazd2srfi0zd2zzsaw_defsz00(BgL_rtl_pragmaz00_bglt BgL_oz00_370)
	{
		{	/* SawMill/defs.sch 893 */
			return (((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_oz00_370))->BgL_srfi0z00);
		}

	}



/* &rtl_pragma-srfi0 */
	obj_t BGl_z62rtl_pragmazd2srfi0zb0zzsaw_defsz00(obj_t BgL_envz00_7565,
		obj_t BgL_oz00_7566)
	{
		{	/* SawMill/defs.sch 893 */
			return
				BGl_rtl_pragmazd2srfi0zd2zzsaw_defsz00(
				((BgL_rtl_pragmaz00_bglt) BgL_oz00_7566));
		}

	}



/* rtl_pragma-srfi0-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_pragmazd2srfi0zd2setz12z12zzsaw_defsz00(BgL_rtl_pragmaz00_bglt
		BgL_oz00_371, obj_t BgL_vz00_372)
	{
		{	/* SawMill/defs.sch 894 */
			return
				((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_oz00_371))->BgL_srfi0z00) =
				((obj_t) BgL_vz00_372), BUNSPEC);
		}

	}



/* &rtl_pragma-srfi0-set! */
	obj_t BGl_z62rtl_pragmazd2srfi0zd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7567, obj_t BgL_oz00_7568, obj_t BgL_vz00_7569)
	{
		{	/* SawMill/defs.sch 894 */
			return
				BGl_rtl_pragmazd2srfi0zd2setz12z12zzsaw_defsz00(
				((BgL_rtl_pragmaz00_bglt) BgL_oz00_7568), BgL_vz00_7569);
		}

	}



/* rtl_pragma-format */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_pragmazd2formatzd2zzsaw_defsz00(BgL_rtl_pragmaz00_bglt BgL_oz00_373)
	{
		{	/* SawMill/defs.sch 895 */
			return (((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_oz00_373))->BgL_formatz00);
		}

	}



/* &rtl_pragma-format */
	obj_t BGl_z62rtl_pragmazd2formatzb0zzsaw_defsz00(obj_t BgL_envz00_7570,
		obj_t BgL_oz00_7571)
	{
		{	/* SawMill/defs.sch 895 */
			return
				BGl_rtl_pragmazd2formatzd2zzsaw_defsz00(
				((BgL_rtl_pragmaz00_bglt) BgL_oz00_7571));
		}

	}



/* rtl_pragma-format-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_pragmazd2formatzd2setz12z12zzsaw_defsz00(BgL_rtl_pragmaz00_bglt
		BgL_oz00_374, obj_t BgL_vz00_375)
	{
		{	/* SawMill/defs.sch 896 */
			return
				((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_oz00_374))->BgL_formatz00) =
				((obj_t) BgL_vz00_375), BUNSPEC);
		}

	}



/* &rtl_pragma-format-set! */
	obj_t BGl_z62rtl_pragmazd2formatzd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7572, obj_t BgL_oz00_7573, obj_t BgL_vz00_7574)
	{
		{	/* SawMill/defs.sch 896 */
			return
				BGl_rtl_pragmazd2formatzd2setz12z12zzsaw_defsz00(
				((BgL_rtl_pragmaz00_bglt) BgL_oz00_7573), BgL_vz00_7574);
		}

	}



/* rtl_pragma-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_pragmazd2loczd2zzsaw_defsz00(BgL_rtl_pragmaz00_bglt BgL_oz00_376)
	{
		{	/* SawMill/defs.sch 897 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_376)))->BgL_locz00);
		}

	}



/* &rtl_pragma-loc */
	obj_t BGl_z62rtl_pragmazd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7575,
		obj_t BgL_oz00_7576)
	{
		{	/* SawMill/defs.sch 897 */
			return
				BGl_rtl_pragmazd2loczd2zzsaw_defsz00(
				((BgL_rtl_pragmaz00_bglt) BgL_oz00_7576));
		}

	}



/* rtl_pragma-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_pragmazd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_pragmaz00_bglt
		BgL_oz00_377, obj_t BgL_vz00_378)
	{
		{	/* SawMill/defs.sch 898 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_377)))->BgL_locz00) =
				((obj_t) BgL_vz00_378), BUNSPEC);
		}

	}



/* &rtl_pragma-loc-set! */
	obj_t BGl_z62rtl_pragmazd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7577,
		obj_t BgL_oz00_7578, obj_t BgL_vz00_7579)
	{
		{	/* SawMill/defs.sch 898 */
			return
				BGl_rtl_pragmazd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_pragmaz00_bglt) BgL_oz00_7578), BgL_vz00_7579);
		}

	}



/* make-rtl_cast */
	BGL_EXPORTED_DEF BgL_rtl_castz00_bglt
		BGl_makezd2rtl_castzd2zzsaw_defsz00(obj_t BgL_loc1369z00_379,
		BgL_typez00_bglt BgL_totype1370z00_380,
		BgL_typez00_bglt BgL_fromtype1371z00_381)
	{
		{	/* SawMill/defs.sch 901 */
			{	/* SawMill/defs.sch 901 */
				BgL_rtl_castz00_bglt BgL_new1428z00_9425;

				{	/* SawMill/defs.sch 901 */
					BgL_rtl_castz00_bglt BgL_new1427z00_9426;

					BgL_new1427z00_9426 =
						((BgL_rtl_castz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_castz00_bgl))));
					{	/* SawMill/defs.sch 901 */
						long BgL_arg1878z00_9427;

						BgL_arg1878z00_9427 = BGL_CLASS_NUM(BGl_rtl_castz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1427z00_9426), BgL_arg1878z00_9427);
					}
					BgL_new1428z00_9425 = BgL_new1427z00_9426;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1428z00_9425)))->BgL_locz00) =
					((obj_t) BgL_loc1369z00_379), BUNSPEC);
				((((BgL_rtl_castz00_bglt) COBJECT(BgL_new1428z00_9425))->
						BgL_totypez00) =
					((BgL_typez00_bglt) BgL_totype1370z00_380), BUNSPEC);
				((((BgL_rtl_castz00_bglt) COBJECT(BgL_new1428z00_9425))->
						BgL_fromtypez00) =
					((BgL_typez00_bglt) BgL_fromtype1371z00_381), BUNSPEC);
				return BgL_new1428z00_9425;
			}
		}

	}



/* &make-rtl_cast */
	BgL_rtl_castz00_bglt BGl_z62makezd2rtl_castzb0zzsaw_defsz00(obj_t
		BgL_envz00_7580, obj_t BgL_loc1369z00_7581, obj_t BgL_totype1370z00_7582,
		obj_t BgL_fromtype1371z00_7583)
	{
		{	/* SawMill/defs.sch 901 */
			return
				BGl_makezd2rtl_castzd2zzsaw_defsz00(BgL_loc1369z00_7581,
				((BgL_typez00_bglt) BgL_totype1370z00_7582),
				((BgL_typez00_bglt) BgL_fromtype1371z00_7583));
		}

	}



/* rtl_cast? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_castzf3zf3zzsaw_defsz00(obj_t BgL_objz00_382)
	{
		{	/* SawMill/defs.sch 902 */
			{	/* SawMill/defs.sch 902 */
				obj_t BgL_classz00_9428;

				BgL_classz00_9428 = BGl_rtl_castz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_382))
					{	/* SawMill/defs.sch 902 */
						BgL_objectz00_bglt BgL_arg1807z00_9429;

						BgL_arg1807z00_9429 = (BgL_objectz00_bglt) (BgL_objz00_382);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 902 */
								long BgL_idxz00_9430;

								BgL_idxz00_9430 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9429);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9430 + 2L)) == BgL_classz00_9428);
							}
						else
							{	/* SawMill/defs.sch 902 */
								bool_t BgL_res3057z00_9433;

								{	/* SawMill/defs.sch 902 */
									obj_t BgL_oclassz00_9434;

									{	/* SawMill/defs.sch 902 */
										obj_t BgL_arg1815z00_9435;
										long BgL_arg1816z00_9436;

										BgL_arg1815z00_9435 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 902 */
											long BgL_arg1817z00_9437;

											BgL_arg1817z00_9437 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9429);
											BgL_arg1816z00_9436 = (BgL_arg1817z00_9437 - OBJECT_TYPE);
										}
										BgL_oclassz00_9434 =
											VECTOR_REF(BgL_arg1815z00_9435, BgL_arg1816z00_9436);
									}
									{	/* SawMill/defs.sch 902 */
										bool_t BgL__ortest_1115z00_9438;

										BgL__ortest_1115z00_9438 =
											(BgL_classz00_9428 == BgL_oclassz00_9434);
										if (BgL__ortest_1115z00_9438)
											{	/* SawMill/defs.sch 902 */
												BgL_res3057z00_9433 = BgL__ortest_1115z00_9438;
											}
										else
											{	/* SawMill/defs.sch 902 */
												long BgL_odepthz00_9439;

												{	/* SawMill/defs.sch 902 */
													obj_t BgL_arg1804z00_9440;

													BgL_arg1804z00_9440 = (BgL_oclassz00_9434);
													BgL_odepthz00_9439 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9440);
												}
												if ((2L < BgL_odepthz00_9439))
													{	/* SawMill/defs.sch 902 */
														obj_t BgL_arg1802z00_9441;

														{	/* SawMill/defs.sch 902 */
															obj_t BgL_arg1803z00_9442;

															BgL_arg1803z00_9442 = (BgL_oclassz00_9434);
															BgL_arg1802z00_9441 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9442,
																2L);
														}
														BgL_res3057z00_9433 =
															(BgL_arg1802z00_9441 == BgL_classz00_9428);
													}
												else
													{	/* SawMill/defs.sch 902 */
														BgL_res3057z00_9433 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3057z00_9433;
							}
					}
				else
					{	/* SawMill/defs.sch 902 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_cast? */
	obj_t BGl_z62rtl_castzf3z91zzsaw_defsz00(obj_t BgL_envz00_7584,
		obj_t BgL_objz00_7585)
	{
		{	/* SawMill/defs.sch 902 */
			return BBOOL(BGl_rtl_castzf3zf3zzsaw_defsz00(BgL_objz00_7585));
		}

	}



/* rtl_cast-nil */
	BGL_EXPORTED_DEF BgL_rtl_castz00_bglt BGl_rtl_castzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 903 */
			{	/* SawMill/defs.sch 903 */
				obj_t BgL_classz00_5891;

				BgL_classz00_5891 = BGl_rtl_castz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 903 */
					obj_t BgL__ortest_1117z00_5892;

					BgL__ortest_1117z00_5892 = BGL_CLASS_NIL(BgL_classz00_5891);
					if (CBOOL(BgL__ortest_1117z00_5892))
						{	/* SawMill/defs.sch 903 */
							return ((BgL_rtl_castz00_bglt) BgL__ortest_1117z00_5892);
						}
					else
						{	/* SawMill/defs.sch 903 */
							return
								((BgL_rtl_castz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5891));
						}
				}
			}
		}

	}



/* &rtl_cast-nil */
	BgL_rtl_castz00_bglt BGl_z62rtl_castzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7586)
	{
		{	/* SawMill/defs.sch 903 */
			return BGl_rtl_castzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_cast-fromtype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_castzd2fromtypezd2zzsaw_defsz00(BgL_rtl_castz00_bglt BgL_oz00_383)
	{
		{	/* SawMill/defs.sch 904 */
			return (((BgL_rtl_castz00_bglt) COBJECT(BgL_oz00_383))->BgL_fromtypez00);
		}

	}



/* &rtl_cast-fromtype */
	BgL_typez00_bglt BGl_z62rtl_castzd2fromtypezb0zzsaw_defsz00(obj_t
		BgL_envz00_7587, obj_t BgL_oz00_7588)
	{
		{	/* SawMill/defs.sch 904 */
			return
				BGl_rtl_castzd2fromtypezd2zzsaw_defsz00(
				((BgL_rtl_castz00_bglt) BgL_oz00_7588));
		}

	}



/* rtl_cast-fromtype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_castzd2fromtypezd2setz12z12zzsaw_defsz00(BgL_rtl_castz00_bglt
		BgL_oz00_384, BgL_typez00_bglt BgL_vz00_385)
	{
		{	/* SawMill/defs.sch 905 */
			return
				((((BgL_rtl_castz00_bglt) COBJECT(BgL_oz00_384))->BgL_fromtypez00) =
				((BgL_typez00_bglt) BgL_vz00_385), BUNSPEC);
		}

	}



/* &rtl_cast-fromtype-set! */
	obj_t BGl_z62rtl_castzd2fromtypezd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7589, obj_t BgL_oz00_7590, obj_t BgL_vz00_7591)
	{
		{	/* SawMill/defs.sch 905 */
			return
				BGl_rtl_castzd2fromtypezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_castz00_bglt) BgL_oz00_7590),
				((BgL_typez00_bglt) BgL_vz00_7591));
		}

	}



/* rtl_cast-totype */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_castzd2totypezd2zzsaw_defsz00(BgL_rtl_castz00_bglt BgL_oz00_386)
	{
		{	/* SawMill/defs.sch 906 */
			return (((BgL_rtl_castz00_bglt) COBJECT(BgL_oz00_386))->BgL_totypez00);
		}

	}



/* &rtl_cast-totype */
	BgL_typez00_bglt BGl_z62rtl_castzd2totypezb0zzsaw_defsz00(obj_t
		BgL_envz00_7592, obj_t BgL_oz00_7593)
	{
		{	/* SawMill/defs.sch 906 */
			return
				BGl_rtl_castzd2totypezd2zzsaw_defsz00(
				((BgL_rtl_castz00_bglt) BgL_oz00_7593));
		}

	}



/* rtl_cast-totype-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_castzd2totypezd2setz12z12zzsaw_defsz00(BgL_rtl_castz00_bglt
		BgL_oz00_387, BgL_typez00_bglt BgL_vz00_388)
	{
		{	/* SawMill/defs.sch 907 */
			return
				((((BgL_rtl_castz00_bglt) COBJECT(BgL_oz00_387))->BgL_totypez00) =
				((BgL_typez00_bglt) BgL_vz00_388), BUNSPEC);
		}

	}



/* &rtl_cast-totype-set! */
	obj_t BGl_z62rtl_castzd2totypezd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7594,
		obj_t BgL_oz00_7595, obj_t BgL_vz00_7596)
	{
		{	/* SawMill/defs.sch 907 */
			return
				BGl_rtl_castzd2totypezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_castz00_bglt) BgL_oz00_7595),
				((BgL_typez00_bglt) BgL_vz00_7596));
		}

	}



/* rtl_cast-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_castzd2loczd2zzsaw_defsz00(BgL_rtl_castz00_bglt
		BgL_oz00_389)
	{
		{	/* SawMill/defs.sch 908 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_389)))->BgL_locz00);
		}

	}



/* &rtl_cast-loc */
	obj_t BGl_z62rtl_castzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7597,
		obj_t BgL_oz00_7598)
	{
		{	/* SawMill/defs.sch 908 */
			return
				BGl_rtl_castzd2loczd2zzsaw_defsz00(
				((BgL_rtl_castz00_bglt) BgL_oz00_7598));
		}

	}



/* rtl_cast-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_castzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_castz00_bglt
		BgL_oz00_390, obj_t BgL_vz00_391)
	{
		{	/* SawMill/defs.sch 909 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_390)))->BgL_locz00) =
				((obj_t) BgL_vz00_391), BUNSPEC);
		}

	}



/* &rtl_cast-loc-set! */
	obj_t BGl_z62rtl_castzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7599,
		obj_t BgL_oz00_7600, obj_t BgL_vz00_7601)
	{
		{	/* SawMill/defs.sch 909 */
			return
				BGl_rtl_castzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_castz00_bglt) BgL_oz00_7600), BgL_vz00_7601);
		}

	}



/* make-rtl_cast_null */
	BGL_EXPORTED_DEF BgL_rtl_cast_nullz00_bglt
		BGl_makezd2rtl_cast_nullzd2zzsaw_defsz00(obj_t BgL_loc1366z00_392,
		BgL_typez00_bglt BgL_type1367z00_393)
	{
		{	/* SawMill/defs.sch 912 */
			{	/* SawMill/defs.sch 912 */
				BgL_rtl_cast_nullz00_bglt BgL_new1430z00_9443;

				{	/* SawMill/defs.sch 912 */
					BgL_rtl_cast_nullz00_bglt BgL_new1429z00_9444;

					BgL_new1429z00_9444 =
						((BgL_rtl_cast_nullz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_cast_nullz00_bgl))));
					{	/* SawMill/defs.sch 912 */
						long BgL_arg1879z00_9445;

						BgL_arg1879z00_9445 =
							BGL_CLASS_NUM(BGl_rtl_cast_nullz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1429z00_9444), BgL_arg1879z00_9445);
					}
					BgL_new1430z00_9443 = BgL_new1429z00_9444;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1430z00_9443)))->BgL_locz00) =
					((obj_t) BgL_loc1366z00_392), BUNSPEC);
				((((BgL_rtl_cast_nullz00_bglt) COBJECT(BgL_new1430z00_9443))->
						BgL_typez00) = ((BgL_typez00_bglt) BgL_type1367z00_393), BUNSPEC);
				return BgL_new1430z00_9443;
			}
		}

	}



/* &make-rtl_cast_null */
	BgL_rtl_cast_nullz00_bglt BGl_z62makezd2rtl_cast_nullzb0zzsaw_defsz00(obj_t
		BgL_envz00_7602, obj_t BgL_loc1366z00_7603, obj_t BgL_type1367z00_7604)
	{
		{	/* SawMill/defs.sch 912 */
			return
				BGl_makezd2rtl_cast_nullzd2zzsaw_defsz00(BgL_loc1366z00_7603,
				((BgL_typez00_bglt) BgL_type1367z00_7604));
		}

	}



/* rtl_cast_null? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_cast_nullzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_394)
	{
		{	/* SawMill/defs.sch 913 */
			{	/* SawMill/defs.sch 913 */
				obj_t BgL_classz00_9446;

				BgL_classz00_9446 = BGl_rtl_cast_nullz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_394))
					{	/* SawMill/defs.sch 913 */
						BgL_objectz00_bglt BgL_arg1807z00_9447;

						BgL_arg1807z00_9447 = (BgL_objectz00_bglt) (BgL_objz00_394);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 913 */
								long BgL_idxz00_9448;

								BgL_idxz00_9448 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9447);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9448 + 2L)) == BgL_classz00_9446);
							}
						else
							{	/* SawMill/defs.sch 913 */
								bool_t BgL_res3058z00_9451;

								{	/* SawMill/defs.sch 913 */
									obj_t BgL_oclassz00_9452;

									{	/* SawMill/defs.sch 913 */
										obj_t BgL_arg1815z00_9453;
										long BgL_arg1816z00_9454;

										BgL_arg1815z00_9453 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 913 */
											long BgL_arg1817z00_9455;

											BgL_arg1817z00_9455 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9447);
											BgL_arg1816z00_9454 = (BgL_arg1817z00_9455 - OBJECT_TYPE);
										}
										BgL_oclassz00_9452 =
											VECTOR_REF(BgL_arg1815z00_9453, BgL_arg1816z00_9454);
									}
									{	/* SawMill/defs.sch 913 */
										bool_t BgL__ortest_1115z00_9456;

										BgL__ortest_1115z00_9456 =
											(BgL_classz00_9446 == BgL_oclassz00_9452);
										if (BgL__ortest_1115z00_9456)
											{	/* SawMill/defs.sch 913 */
												BgL_res3058z00_9451 = BgL__ortest_1115z00_9456;
											}
										else
											{	/* SawMill/defs.sch 913 */
												long BgL_odepthz00_9457;

												{	/* SawMill/defs.sch 913 */
													obj_t BgL_arg1804z00_9458;

													BgL_arg1804z00_9458 = (BgL_oclassz00_9452);
													BgL_odepthz00_9457 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9458);
												}
												if ((2L < BgL_odepthz00_9457))
													{	/* SawMill/defs.sch 913 */
														obj_t BgL_arg1802z00_9459;

														{	/* SawMill/defs.sch 913 */
															obj_t BgL_arg1803z00_9460;

															BgL_arg1803z00_9460 = (BgL_oclassz00_9452);
															BgL_arg1802z00_9459 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9460,
																2L);
														}
														BgL_res3058z00_9451 =
															(BgL_arg1802z00_9459 == BgL_classz00_9446);
													}
												else
													{	/* SawMill/defs.sch 913 */
														BgL_res3058z00_9451 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3058z00_9451;
							}
					}
				else
					{	/* SawMill/defs.sch 913 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_cast_null? */
	obj_t BGl_z62rtl_cast_nullzf3z91zzsaw_defsz00(obj_t BgL_envz00_7605,
		obj_t BgL_objz00_7606)
	{
		{	/* SawMill/defs.sch 913 */
			return BBOOL(BGl_rtl_cast_nullzf3zf3zzsaw_defsz00(BgL_objz00_7606));
		}

	}



/* rtl_cast_null-nil */
	BGL_EXPORTED_DEF BgL_rtl_cast_nullz00_bglt
		BGl_rtl_cast_nullzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 914 */
			{	/* SawMill/defs.sch 914 */
				obj_t BgL_classz00_5933;

				BgL_classz00_5933 = BGl_rtl_cast_nullz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 914 */
					obj_t BgL__ortest_1117z00_5934;

					BgL__ortest_1117z00_5934 = BGL_CLASS_NIL(BgL_classz00_5933);
					if (CBOOL(BgL__ortest_1117z00_5934))
						{	/* SawMill/defs.sch 914 */
							return ((BgL_rtl_cast_nullz00_bglt) BgL__ortest_1117z00_5934);
						}
					else
						{	/* SawMill/defs.sch 914 */
							return
								((BgL_rtl_cast_nullz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5933));
						}
				}
			}
		}

	}



/* &rtl_cast_null-nil */
	BgL_rtl_cast_nullz00_bglt BGl_z62rtl_cast_nullzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7607)
	{
		{	/* SawMill/defs.sch 914 */
			return BGl_rtl_cast_nullzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_cast_null-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_cast_nullzd2typezd2zzsaw_defsz00(BgL_rtl_cast_nullz00_bglt
		BgL_oz00_395)
	{
		{	/* SawMill/defs.sch 915 */
			return (((BgL_rtl_cast_nullz00_bglt) COBJECT(BgL_oz00_395))->BgL_typez00);
		}

	}



/* &rtl_cast_null-type */
	BgL_typez00_bglt BGl_z62rtl_cast_nullzd2typezb0zzsaw_defsz00(obj_t
		BgL_envz00_7608, obj_t BgL_oz00_7609)
	{
		{	/* SawMill/defs.sch 915 */
			return
				BGl_rtl_cast_nullzd2typezd2zzsaw_defsz00(
				((BgL_rtl_cast_nullz00_bglt) BgL_oz00_7609));
		}

	}



/* rtl_cast_null-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_cast_nullzd2typezd2setz12z12zzsaw_defsz00(BgL_rtl_cast_nullz00_bglt
		BgL_oz00_396, BgL_typez00_bglt BgL_vz00_397)
	{
		{	/* SawMill/defs.sch 916 */
			return
				((((BgL_rtl_cast_nullz00_bglt) COBJECT(BgL_oz00_396))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_397), BUNSPEC);
		}

	}



/* &rtl_cast_null-type-set! */
	obj_t BGl_z62rtl_cast_nullzd2typezd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7610, obj_t BgL_oz00_7611, obj_t BgL_vz00_7612)
	{
		{	/* SawMill/defs.sch 916 */
			return
				BGl_rtl_cast_nullzd2typezd2setz12z12zzsaw_defsz00(
				((BgL_rtl_cast_nullz00_bglt) BgL_oz00_7611),
				((BgL_typez00_bglt) BgL_vz00_7612));
		}

	}



/* rtl_cast_null-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_cast_nullzd2loczd2zzsaw_defsz00(BgL_rtl_cast_nullz00_bglt
		BgL_oz00_398)
	{
		{	/* SawMill/defs.sch 917 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_398)))->BgL_locz00);
		}

	}



/* &rtl_cast_null-loc */
	obj_t BGl_z62rtl_cast_nullzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7613,
		obj_t BgL_oz00_7614)
	{
		{	/* SawMill/defs.sch 917 */
			return
				BGl_rtl_cast_nullzd2loczd2zzsaw_defsz00(
				((BgL_rtl_cast_nullz00_bglt) BgL_oz00_7614));
		}

	}



/* rtl_cast_null-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_cast_nullzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_cast_nullz00_bglt
		BgL_oz00_399, obj_t BgL_vz00_400)
	{
		{	/* SawMill/defs.sch 918 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_399)))->BgL_locz00) =
				((obj_t) BgL_vz00_400), BUNSPEC);
		}

	}



/* &rtl_cast_null-loc-set! */
	obj_t BGl_z62rtl_cast_nullzd2loczd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7615, obj_t BgL_oz00_7616, obj_t BgL_vz00_7617)
	{
		{	/* SawMill/defs.sch 918 */
			return
				BGl_rtl_cast_nullzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_cast_nullz00_bglt) BgL_oz00_7616), BgL_vz00_7617);
		}

	}



/* make-rtl_protect */
	BGL_EXPORTED_DEF BgL_rtl_protectz00_bglt
		BGl_makezd2rtl_protectzd2zzsaw_defsz00(obj_t BgL_loc1363z00_401)
	{
		{	/* SawMill/defs.sch 921 */
			{	/* SawMill/defs.sch 921 */
				BgL_rtl_protectz00_bglt BgL_new1432z00_9461;

				{	/* SawMill/defs.sch 921 */
					BgL_rtl_protectz00_bglt BgL_new1431z00_9462;

					BgL_new1431z00_9462 =
						((BgL_rtl_protectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_protectz00_bgl))));
					{	/* SawMill/defs.sch 921 */
						long BgL_arg1880z00_9463;

						BgL_arg1880z00_9463 =
							BGL_CLASS_NUM(BGl_rtl_protectz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1431z00_9462), BgL_arg1880z00_9463);
					}
					BgL_new1432z00_9461 = BgL_new1431z00_9462;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1432z00_9461)))->BgL_locz00) =
					((obj_t) BgL_loc1363z00_401), BUNSPEC);
				return BgL_new1432z00_9461;
			}
		}

	}



/* &make-rtl_protect */
	BgL_rtl_protectz00_bglt BGl_z62makezd2rtl_protectzb0zzsaw_defsz00(obj_t
		BgL_envz00_7618, obj_t BgL_loc1363z00_7619)
	{
		{	/* SawMill/defs.sch 921 */
			return BGl_makezd2rtl_protectzd2zzsaw_defsz00(BgL_loc1363z00_7619);
		}

	}



/* rtl_protect? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_protectzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_402)
	{
		{	/* SawMill/defs.sch 922 */
			{	/* SawMill/defs.sch 922 */
				obj_t BgL_classz00_9464;

				BgL_classz00_9464 = BGl_rtl_protectz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_402))
					{	/* SawMill/defs.sch 922 */
						BgL_objectz00_bglt BgL_arg1807z00_9465;

						BgL_arg1807z00_9465 = (BgL_objectz00_bglt) (BgL_objz00_402);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 922 */
								long BgL_idxz00_9466;

								BgL_idxz00_9466 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9465);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9466 + 2L)) == BgL_classz00_9464);
							}
						else
							{	/* SawMill/defs.sch 922 */
								bool_t BgL_res3059z00_9469;

								{	/* SawMill/defs.sch 922 */
									obj_t BgL_oclassz00_9470;

									{	/* SawMill/defs.sch 922 */
										obj_t BgL_arg1815z00_9471;
										long BgL_arg1816z00_9472;

										BgL_arg1815z00_9471 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 922 */
											long BgL_arg1817z00_9473;

											BgL_arg1817z00_9473 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9465);
											BgL_arg1816z00_9472 = (BgL_arg1817z00_9473 - OBJECT_TYPE);
										}
										BgL_oclassz00_9470 =
											VECTOR_REF(BgL_arg1815z00_9471, BgL_arg1816z00_9472);
									}
									{	/* SawMill/defs.sch 922 */
										bool_t BgL__ortest_1115z00_9474;

										BgL__ortest_1115z00_9474 =
											(BgL_classz00_9464 == BgL_oclassz00_9470);
										if (BgL__ortest_1115z00_9474)
											{	/* SawMill/defs.sch 922 */
												BgL_res3059z00_9469 = BgL__ortest_1115z00_9474;
											}
										else
											{	/* SawMill/defs.sch 922 */
												long BgL_odepthz00_9475;

												{	/* SawMill/defs.sch 922 */
													obj_t BgL_arg1804z00_9476;

													BgL_arg1804z00_9476 = (BgL_oclassz00_9470);
													BgL_odepthz00_9475 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9476);
												}
												if ((2L < BgL_odepthz00_9475))
													{	/* SawMill/defs.sch 922 */
														obj_t BgL_arg1802z00_9477;

														{	/* SawMill/defs.sch 922 */
															obj_t BgL_arg1803z00_9478;

															BgL_arg1803z00_9478 = (BgL_oclassz00_9470);
															BgL_arg1802z00_9477 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9478,
																2L);
														}
														BgL_res3059z00_9469 =
															(BgL_arg1802z00_9477 == BgL_classz00_9464);
													}
												else
													{	/* SawMill/defs.sch 922 */
														BgL_res3059z00_9469 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3059z00_9469;
							}
					}
				else
					{	/* SawMill/defs.sch 922 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_protect? */
	obj_t BGl_z62rtl_protectzf3z91zzsaw_defsz00(obj_t BgL_envz00_7620,
		obj_t BgL_objz00_7621)
	{
		{	/* SawMill/defs.sch 922 */
			return BBOOL(BGl_rtl_protectzf3zf3zzsaw_defsz00(BgL_objz00_7621));
		}

	}



/* rtl_protect-nil */
	BGL_EXPORTED_DEF BgL_rtl_protectz00_bglt
		BGl_rtl_protectzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 923 */
			{	/* SawMill/defs.sch 923 */
				obj_t BgL_classz00_5975;

				BgL_classz00_5975 = BGl_rtl_protectz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 923 */
					obj_t BgL__ortest_1117z00_5976;

					BgL__ortest_1117z00_5976 = BGL_CLASS_NIL(BgL_classz00_5975);
					if (CBOOL(BgL__ortest_1117z00_5976))
						{	/* SawMill/defs.sch 923 */
							return ((BgL_rtl_protectz00_bglt) BgL__ortest_1117z00_5976);
						}
					else
						{	/* SawMill/defs.sch 923 */
							return
								((BgL_rtl_protectz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_5975));
						}
				}
			}
		}

	}



/* &rtl_protect-nil */
	BgL_rtl_protectz00_bglt BGl_z62rtl_protectzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7622)
	{
		{	/* SawMill/defs.sch 923 */
			return BGl_rtl_protectzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_protect-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_protectzd2loczd2zzsaw_defsz00(BgL_rtl_protectz00_bglt BgL_oz00_403)
	{
		{	/* SawMill/defs.sch 924 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_403)))->BgL_locz00);
		}

	}



/* &rtl_protect-loc */
	obj_t BGl_z62rtl_protectzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7623,
		obj_t BgL_oz00_7624)
	{
		{	/* SawMill/defs.sch 924 */
			return
				BGl_rtl_protectzd2loczd2zzsaw_defsz00(
				((BgL_rtl_protectz00_bglt) BgL_oz00_7624));
		}

	}



/* rtl_protect-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_protectzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_protectz00_bglt
		BgL_oz00_404, obj_t BgL_vz00_405)
	{
		{	/* SawMill/defs.sch 925 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_404)))->BgL_locz00) =
				((obj_t) BgL_vz00_405), BUNSPEC);
		}

	}



/* &rtl_protect-loc-set! */
	obj_t BGl_z62rtl_protectzd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7625,
		obj_t BgL_oz00_7626, obj_t BgL_vz00_7627)
	{
		{	/* SawMill/defs.sch 925 */
			return
				BGl_rtl_protectzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_protectz00_bglt) BgL_oz00_7626), BgL_vz00_7627);
		}

	}



/* make-rtl_protected */
	BGL_EXPORTED_DEF BgL_rtl_protectedz00_bglt
		BGl_makezd2rtl_protectedzd2zzsaw_defsz00(obj_t BgL_loc1361z00_406)
	{
		{	/* SawMill/defs.sch 928 */
			{	/* SawMill/defs.sch 928 */
				BgL_rtl_protectedz00_bglt BgL_new1434z00_9479;

				{	/* SawMill/defs.sch 928 */
					BgL_rtl_protectedz00_bglt BgL_new1433z00_9480;

					BgL_new1433z00_9480 =
						((BgL_rtl_protectedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_protectedz00_bgl))));
					{	/* SawMill/defs.sch 928 */
						long BgL_arg1882z00_9481;

						BgL_arg1882z00_9481 =
							BGL_CLASS_NUM(BGl_rtl_protectedz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1433z00_9480), BgL_arg1882z00_9481);
					}
					BgL_new1434z00_9479 = BgL_new1433z00_9480;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1434z00_9479)))->BgL_locz00) =
					((obj_t) BgL_loc1361z00_406), BUNSPEC);
				return BgL_new1434z00_9479;
			}
		}

	}



/* &make-rtl_protected */
	BgL_rtl_protectedz00_bglt BGl_z62makezd2rtl_protectedzb0zzsaw_defsz00(obj_t
		BgL_envz00_7628, obj_t BgL_loc1361z00_7629)
	{
		{	/* SawMill/defs.sch 928 */
			return BGl_makezd2rtl_protectedzd2zzsaw_defsz00(BgL_loc1361z00_7629);
		}

	}



/* rtl_protected? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_protectedzf3zf3zzsaw_defsz00(obj_t
		BgL_objz00_407)
	{
		{	/* SawMill/defs.sch 929 */
			{	/* SawMill/defs.sch 929 */
				obj_t BgL_classz00_9482;

				BgL_classz00_9482 = BGl_rtl_protectedz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_407))
					{	/* SawMill/defs.sch 929 */
						BgL_objectz00_bglt BgL_arg1807z00_9483;

						BgL_arg1807z00_9483 = (BgL_objectz00_bglt) (BgL_objz00_407);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 929 */
								long BgL_idxz00_9484;

								BgL_idxz00_9484 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9483);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9484 + 2L)) == BgL_classz00_9482);
							}
						else
							{	/* SawMill/defs.sch 929 */
								bool_t BgL_res3060z00_9487;

								{	/* SawMill/defs.sch 929 */
									obj_t BgL_oclassz00_9488;

									{	/* SawMill/defs.sch 929 */
										obj_t BgL_arg1815z00_9489;
										long BgL_arg1816z00_9490;

										BgL_arg1815z00_9489 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 929 */
											long BgL_arg1817z00_9491;

											BgL_arg1817z00_9491 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9483);
											BgL_arg1816z00_9490 = (BgL_arg1817z00_9491 - OBJECT_TYPE);
										}
										BgL_oclassz00_9488 =
											VECTOR_REF(BgL_arg1815z00_9489, BgL_arg1816z00_9490);
									}
									{	/* SawMill/defs.sch 929 */
										bool_t BgL__ortest_1115z00_9492;

										BgL__ortest_1115z00_9492 =
											(BgL_classz00_9482 == BgL_oclassz00_9488);
										if (BgL__ortest_1115z00_9492)
											{	/* SawMill/defs.sch 929 */
												BgL_res3060z00_9487 = BgL__ortest_1115z00_9492;
											}
										else
											{	/* SawMill/defs.sch 929 */
												long BgL_odepthz00_9493;

												{	/* SawMill/defs.sch 929 */
													obj_t BgL_arg1804z00_9494;

													BgL_arg1804z00_9494 = (BgL_oclassz00_9488);
													BgL_odepthz00_9493 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9494);
												}
												if ((2L < BgL_odepthz00_9493))
													{	/* SawMill/defs.sch 929 */
														obj_t BgL_arg1802z00_9495;

														{	/* SawMill/defs.sch 929 */
															obj_t BgL_arg1803z00_9496;

															BgL_arg1803z00_9496 = (BgL_oclassz00_9488);
															BgL_arg1802z00_9495 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9496,
																2L);
														}
														BgL_res3060z00_9487 =
															(BgL_arg1802z00_9495 == BgL_classz00_9482);
													}
												else
													{	/* SawMill/defs.sch 929 */
														BgL_res3060z00_9487 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3060z00_9487;
							}
					}
				else
					{	/* SawMill/defs.sch 929 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_protected? */
	obj_t BGl_z62rtl_protectedzf3z91zzsaw_defsz00(obj_t BgL_envz00_7630,
		obj_t BgL_objz00_7631)
	{
		{	/* SawMill/defs.sch 929 */
			return BBOOL(BGl_rtl_protectedzf3zf3zzsaw_defsz00(BgL_objz00_7631));
		}

	}



/* rtl_protected-nil */
	BGL_EXPORTED_DEF BgL_rtl_protectedz00_bglt
		BGl_rtl_protectedzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 930 */
			{	/* SawMill/defs.sch 930 */
				obj_t BgL_classz00_6017;

				BgL_classz00_6017 = BGl_rtl_protectedz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 930 */
					obj_t BgL__ortest_1117z00_6018;

					BgL__ortest_1117z00_6018 = BGL_CLASS_NIL(BgL_classz00_6017);
					if (CBOOL(BgL__ortest_1117z00_6018))
						{	/* SawMill/defs.sch 930 */
							return ((BgL_rtl_protectedz00_bglt) BgL__ortest_1117z00_6018);
						}
					else
						{	/* SawMill/defs.sch 930 */
							return
								((BgL_rtl_protectedz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_6017));
						}
				}
			}
		}

	}



/* &rtl_protected-nil */
	BgL_rtl_protectedz00_bglt BGl_z62rtl_protectedzd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7632)
	{
		{	/* SawMill/defs.sch 930 */
			return BGl_rtl_protectedzd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_protected-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_protectedzd2loczd2zzsaw_defsz00(BgL_rtl_protectedz00_bglt
		BgL_oz00_408)
	{
		{	/* SawMill/defs.sch 931 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_408)))->BgL_locz00);
		}

	}



/* &rtl_protected-loc */
	obj_t BGl_z62rtl_protectedzd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7633,
		obj_t BgL_oz00_7634)
	{
		{	/* SawMill/defs.sch 931 */
			return
				BGl_rtl_protectedzd2loczd2zzsaw_defsz00(
				((BgL_rtl_protectedz00_bglt) BgL_oz00_7634));
		}

	}



/* rtl_protected-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_protectedzd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_protectedz00_bglt
		BgL_oz00_409, obj_t BgL_vz00_410)
	{
		{	/* SawMill/defs.sch 932 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_409)))->BgL_locz00) =
				((obj_t) BgL_vz00_410), BUNSPEC);
		}

	}



/* &rtl_protected-loc-set! */
	obj_t BGl_z62rtl_protectedzd2loczd2setz12z70zzsaw_defsz00(obj_t
		BgL_envz00_7635, obj_t BgL_oz00_7636, obj_t BgL_vz00_7637)
	{
		{	/* SawMill/defs.sch 932 */
			return
				BGl_rtl_protectedzd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_protectedz00_bglt) BgL_oz00_7636), BgL_vz00_7637);
		}

	}



/* make-rtl_ins */
	BGL_EXPORTED_DEF BgL_rtl_insz00_bglt BGl_makezd2rtl_inszd2zzsaw_defsz00(obj_t
		BgL_loc1355z00_411, obj_t BgL_z52spill1356z52_412,
		obj_t BgL_dest1357z00_413, BgL_rtl_funz00_bglt BgL_fun1358z00_414,
		obj_t BgL_args1359z00_415)
	{
		{	/* SawMill/defs.sch 935 */
			{	/* SawMill/defs.sch 935 */
				BgL_rtl_insz00_bglt BgL_new1436z00_9497;

				{	/* SawMill/defs.sch 935 */
					BgL_rtl_insz00_bglt BgL_new1435z00_9498;

					BgL_new1435z00_9498 =
						((BgL_rtl_insz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_insz00_bgl))));
					{	/* SawMill/defs.sch 935 */
						long BgL_arg1883z00_9499;

						BgL_arg1883z00_9499 = BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1435z00_9498), BgL_arg1883z00_9499);
					}
					{	/* SawMill/defs.sch 935 */
						BgL_objectz00_bglt BgL_tmpz00_12656;

						BgL_tmpz00_12656 = ((BgL_objectz00_bglt) BgL_new1435z00_9498);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12656, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1435z00_9498);
					BgL_new1436z00_9497 = BgL_new1435z00_9498;
				}
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1436z00_9497))->BgL_locz00) =
					((obj_t) BgL_loc1355z00_411), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1436z00_9497))->
						BgL_z52spillz52) = ((obj_t) BgL_z52spill1356z52_412), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1436z00_9497))->BgL_destz00) =
					((obj_t) BgL_dest1357z00_413), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1436z00_9497))->BgL_funz00) =
					((BgL_rtl_funz00_bglt) BgL_fun1358z00_414), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1436z00_9497))->BgL_argsz00) =
					((obj_t) BgL_args1359z00_415), BUNSPEC);
				return BgL_new1436z00_9497;
			}
		}

	}



/* &make-rtl_ins */
	BgL_rtl_insz00_bglt BGl_z62makezd2rtl_inszb0zzsaw_defsz00(obj_t
		BgL_envz00_7638, obj_t BgL_loc1355z00_7639, obj_t BgL_z52spill1356z52_7640,
		obj_t BgL_dest1357z00_7641, obj_t BgL_fun1358z00_7642,
		obj_t BgL_args1359z00_7643)
	{
		{	/* SawMill/defs.sch 935 */
			return
				BGl_makezd2rtl_inszd2zzsaw_defsz00(BgL_loc1355z00_7639,
				BgL_z52spill1356z52_7640, BgL_dest1357z00_7641,
				((BgL_rtl_funz00_bglt) BgL_fun1358z00_7642), BgL_args1359z00_7643);
		}

	}



/* rtl_ins? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_inszf3zf3zzsaw_defsz00(obj_t BgL_objz00_416)
	{
		{	/* SawMill/defs.sch 936 */
			{	/* SawMill/defs.sch 936 */
				obj_t BgL_classz00_9500;

				BgL_classz00_9500 = BGl_rtl_insz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_416))
					{	/* SawMill/defs.sch 936 */
						BgL_objectz00_bglt BgL_arg1807z00_9501;

						BgL_arg1807z00_9501 = (BgL_objectz00_bglt) (BgL_objz00_416);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 936 */
								long BgL_idxz00_9502;

								BgL_idxz00_9502 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9501);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9502 + 1L)) == BgL_classz00_9500);
							}
						else
							{	/* SawMill/defs.sch 936 */
								bool_t BgL_res3061z00_9505;

								{	/* SawMill/defs.sch 936 */
									obj_t BgL_oclassz00_9506;

									{	/* SawMill/defs.sch 936 */
										obj_t BgL_arg1815z00_9507;
										long BgL_arg1816z00_9508;

										BgL_arg1815z00_9507 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 936 */
											long BgL_arg1817z00_9509;

											BgL_arg1817z00_9509 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9501);
											BgL_arg1816z00_9508 = (BgL_arg1817z00_9509 - OBJECT_TYPE);
										}
										BgL_oclassz00_9506 =
											VECTOR_REF(BgL_arg1815z00_9507, BgL_arg1816z00_9508);
									}
									{	/* SawMill/defs.sch 936 */
										bool_t BgL__ortest_1115z00_9510;

										BgL__ortest_1115z00_9510 =
											(BgL_classz00_9500 == BgL_oclassz00_9506);
										if (BgL__ortest_1115z00_9510)
											{	/* SawMill/defs.sch 936 */
												BgL_res3061z00_9505 = BgL__ortest_1115z00_9510;
											}
										else
											{	/* SawMill/defs.sch 936 */
												long BgL_odepthz00_9511;

												{	/* SawMill/defs.sch 936 */
													obj_t BgL_arg1804z00_9512;

													BgL_arg1804z00_9512 = (BgL_oclassz00_9506);
													BgL_odepthz00_9511 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9512);
												}
												if ((1L < BgL_odepthz00_9511))
													{	/* SawMill/defs.sch 936 */
														obj_t BgL_arg1802z00_9513;

														{	/* SawMill/defs.sch 936 */
															obj_t BgL_arg1803z00_9514;

															BgL_arg1803z00_9514 = (BgL_oclassz00_9506);
															BgL_arg1802z00_9513 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9514,
																1L);
														}
														BgL_res3061z00_9505 =
															(BgL_arg1802z00_9513 == BgL_classz00_9500);
													}
												else
													{	/* SawMill/defs.sch 936 */
														BgL_res3061z00_9505 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3061z00_9505;
							}
					}
				else
					{	/* SawMill/defs.sch 936 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_ins? */
	obj_t BGl_z62rtl_inszf3z91zzsaw_defsz00(obj_t BgL_envz00_7644,
		obj_t BgL_objz00_7645)
	{
		{	/* SawMill/defs.sch 936 */
			return BBOOL(BGl_rtl_inszf3zf3zzsaw_defsz00(BgL_objz00_7645));
		}

	}



/* rtl_ins-nil */
	BGL_EXPORTED_DEF BgL_rtl_insz00_bglt BGl_rtl_inszd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 937 */
			{	/* SawMill/defs.sch 937 */
				obj_t BgL_classz00_6060;

				BgL_classz00_6060 = BGl_rtl_insz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 937 */
					obj_t BgL__ortest_1117z00_6061;

					BgL__ortest_1117z00_6061 = BGL_CLASS_NIL(BgL_classz00_6060);
					if (CBOOL(BgL__ortest_1117z00_6061))
						{	/* SawMill/defs.sch 937 */
							return ((BgL_rtl_insz00_bglt) BgL__ortest_1117z00_6061);
						}
					else
						{	/* SawMill/defs.sch 937 */
							return
								((BgL_rtl_insz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_6060));
						}
				}
			}
		}

	}



/* &rtl_ins-nil */
	BgL_rtl_insz00_bglt BGl_z62rtl_inszd2nilzb0zzsaw_defsz00(obj_t
		BgL_envz00_7646)
	{
		{	/* SawMill/defs.sch 937 */
			return BGl_rtl_inszd2nilzd2zzsaw_defsz00();
		}

	}



/* rtl_ins-args */
	BGL_EXPORTED_DEF obj_t BGl_rtl_inszd2argszd2zzsaw_defsz00(BgL_rtl_insz00_bglt
		BgL_oz00_417)
	{
		{	/* SawMill/defs.sch 938 */
			return (((BgL_rtl_insz00_bglt) COBJECT(BgL_oz00_417))->BgL_argsz00);
		}

	}



/* &rtl_ins-args */
	obj_t BGl_z62rtl_inszd2argszb0zzsaw_defsz00(obj_t BgL_envz00_7647,
		obj_t BgL_oz00_7648)
	{
		{	/* SawMill/defs.sch 938 */
			return
				BGl_rtl_inszd2argszd2zzsaw_defsz00(
				((BgL_rtl_insz00_bglt) BgL_oz00_7648));
		}

	}



/* rtl_ins-args-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszd2argszd2setz12z12zzsaw_defsz00(BgL_rtl_insz00_bglt
		BgL_oz00_418, obj_t BgL_vz00_419)
	{
		{	/* SawMill/defs.sch 939 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_oz00_418))->BgL_argsz00) =
				((obj_t) BgL_vz00_419), BUNSPEC);
		}

	}



/* &rtl_ins-args-set! */
	obj_t BGl_z62rtl_inszd2argszd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7649,
		obj_t BgL_oz00_7650, obj_t BgL_vz00_7651)
	{
		{	/* SawMill/defs.sch 939 */
			return
				BGl_rtl_inszd2argszd2setz12z12zzsaw_defsz00(
				((BgL_rtl_insz00_bglt) BgL_oz00_7650), BgL_vz00_7651);
		}

	}



/* rtl_ins-fun */
	BGL_EXPORTED_DEF BgL_rtl_funz00_bglt
		BGl_rtl_inszd2funzd2zzsaw_defsz00(BgL_rtl_insz00_bglt BgL_oz00_420)
	{
		{	/* SawMill/defs.sch 940 */
			return (((BgL_rtl_insz00_bglt) COBJECT(BgL_oz00_420))->BgL_funz00);
		}

	}



/* &rtl_ins-fun */
	BgL_rtl_funz00_bglt BGl_z62rtl_inszd2funzb0zzsaw_defsz00(obj_t
		BgL_envz00_7652, obj_t BgL_oz00_7653)
	{
		{	/* SawMill/defs.sch 940 */
			return
				BGl_rtl_inszd2funzd2zzsaw_defsz00(
				((BgL_rtl_insz00_bglt) BgL_oz00_7653));
		}

	}



/* rtl_ins-fun-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszd2funzd2setz12z12zzsaw_defsz00(BgL_rtl_insz00_bglt BgL_oz00_421,
		BgL_rtl_funz00_bglt BgL_vz00_422)
	{
		{	/* SawMill/defs.sch 941 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_oz00_421))->BgL_funz00) =
				((BgL_rtl_funz00_bglt) BgL_vz00_422), BUNSPEC);
		}

	}



/* &rtl_ins-fun-set! */
	obj_t BGl_z62rtl_inszd2funzd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7654,
		obj_t BgL_oz00_7655, obj_t BgL_vz00_7656)
	{
		{	/* SawMill/defs.sch 941 */
			return
				BGl_rtl_inszd2funzd2setz12z12zzsaw_defsz00(
				((BgL_rtl_insz00_bglt) BgL_oz00_7655),
				((BgL_rtl_funz00_bglt) BgL_vz00_7656));
		}

	}



/* rtl_ins-dest */
	BGL_EXPORTED_DEF obj_t BGl_rtl_inszd2destzd2zzsaw_defsz00(BgL_rtl_insz00_bglt
		BgL_oz00_423)
	{
		{	/* SawMill/defs.sch 942 */
			return (((BgL_rtl_insz00_bglt) COBJECT(BgL_oz00_423))->BgL_destz00);
		}

	}



/* &rtl_ins-dest */
	obj_t BGl_z62rtl_inszd2destzb0zzsaw_defsz00(obj_t BgL_envz00_7657,
		obj_t BgL_oz00_7658)
	{
		{	/* SawMill/defs.sch 942 */
			return
				BGl_rtl_inszd2destzd2zzsaw_defsz00(
				((BgL_rtl_insz00_bglt) BgL_oz00_7658));
		}

	}



/* rtl_ins-dest-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszd2destzd2setz12z12zzsaw_defsz00(BgL_rtl_insz00_bglt
		BgL_oz00_424, obj_t BgL_vz00_425)
	{
		{	/* SawMill/defs.sch 943 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_oz00_424))->BgL_destz00) =
				((obj_t) BgL_vz00_425), BUNSPEC);
		}

	}



/* &rtl_ins-dest-set! */
	obj_t BGl_z62rtl_inszd2destzd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7659,
		obj_t BgL_oz00_7660, obj_t BgL_vz00_7661)
	{
		{	/* SawMill/defs.sch 943 */
			return
				BGl_rtl_inszd2destzd2setz12z12zzsaw_defsz00(
				((BgL_rtl_insz00_bglt) BgL_oz00_7660), BgL_vz00_7661);
		}

	}



/* rtl_ins-%spill */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszd2z52spillz80zzsaw_defsz00(BgL_rtl_insz00_bglt BgL_oz00_426)
	{
		{	/* SawMill/defs.sch 944 */
			return (((BgL_rtl_insz00_bglt) COBJECT(BgL_oz00_426))->BgL_z52spillz52);
		}

	}



/* &rtl_ins-%spill */
	obj_t BGl_z62rtl_inszd2z52spillze2zzsaw_defsz00(obj_t BgL_envz00_7662,
		obj_t BgL_oz00_7663)
	{
		{	/* SawMill/defs.sch 944 */
			return
				BGl_rtl_inszd2z52spillz80zzsaw_defsz00(
				((BgL_rtl_insz00_bglt) BgL_oz00_7663));
		}

	}



/* rtl_ins-%spill-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszd2z52spillzd2setz12z40zzsaw_defsz00(BgL_rtl_insz00_bglt
		BgL_oz00_427, obj_t BgL_vz00_428)
	{
		{	/* SawMill/defs.sch 945 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_oz00_427))->BgL_z52spillz52) =
				((obj_t) BgL_vz00_428), BUNSPEC);
		}

	}



/* &rtl_ins-%spill-set! */
	obj_t BGl_z62rtl_inszd2z52spillzd2setz12z22zzsaw_defsz00(obj_t
		BgL_envz00_7664, obj_t BgL_oz00_7665, obj_t BgL_vz00_7666)
	{
		{	/* SawMill/defs.sch 945 */
			return
				BGl_rtl_inszd2z52spillzd2setz12z40zzsaw_defsz00(
				((BgL_rtl_insz00_bglt) BgL_oz00_7665), BgL_vz00_7666);
		}

	}



/* rtl_ins-loc */
	BGL_EXPORTED_DEF obj_t BGl_rtl_inszd2loczd2zzsaw_defsz00(BgL_rtl_insz00_bglt
		BgL_oz00_429)
	{
		{	/* SawMill/defs.sch 946 */
			return (((BgL_rtl_insz00_bglt) COBJECT(BgL_oz00_429))->BgL_locz00);
		}

	}



/* &rtl_ins-loc */
	obj_t BGl_z62rtl_inszd2loczb0zzsaw_defsz00(obj_t BgL_envz00_7667,
		obj_t BgL_oz00_7668)
	{
		{	/* SawMill/defs.sch 946 */
			return
				BGl_rtl_inszd2loczd2zzsaw_defsz00(
				((BgL_rtl_insz00_bglt) BgL_oz00_7668));
		}

	}



/* rtl_ins-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszd2loczd2setz12z12zzsaw_defsz00(BgL_rtl_insz00_bglt BgL_oz00_430,
		obj_t BgL_vz00_431)
	{
		{	/* SawMill/defs.sch 947 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_oz00_430))->BgL_locz00) =
				((obj_t) BgL_vz00_431), BUNSPEC);
		}

	}



/* &rtl_ins-loc-set! */
	obj_t BGl_z62rtl_inszd2loczd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7669,
		obj_t BgL_oz00_7670, obj_t BgL_vz00_7671)
	{
		{	/* SawMill/defs.sch 947 */
			return
				BGl_rtl_inszd2loczd2setz12z12zzsaw_defsz00(
				((BgL_rtl_insz00_bglt) BgL_oz00_7670), BgL_vz00_7671);
		}

	}



/* make-block */
	BGL_EXPORTED_DEF BgL_blockz00_bglt BGl_makezd2blockzd2zzsaw_defsz00(int
		BgL_label1350z00_432, obj_t BgL_preds1351z00_433,
		obj_t BgL_succs1352z00_434, obj_t BgL_first1353z00_435)
	{
		{	/* SawMill/defs.sch 950 */
			{	/* SawMill/defs.sch 950 */
				BgL_blockz00_bglt BgL_new1438z00_9515;

				{	/* SawMill/defs.sch 950 */
					BgL_blockz00_bglt BgL_new1437z00_9516;

					BgL_new1437z00_9516 =
						((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_blockz00_bgl))));
					{	/* SawMill/defs.sch 950 */
						long BgL_arg1884z00_9517;

						BgL_arg1884z00_9517 = BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1437z00_9516), BgL_arg1884z00_9517);
					}
					{	/* SawMill/defs.sch 950 */
						BgL_objectz00_bglt BgL_tmpz00_12733;

						BgL_tmpz00_12733 = ((BgL_objectz00_bglt) BgL_new1437z00_9516);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_12733, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1437z00_9516);
					BgL_new1438z00_9515 = BgL_new1437z00_9516;
				}
				((((BgL_blockz00_bglt) COBJECT(BgL_new1438z00_9515))->BgL_labelz00) =
					((int) BgL_label1350z00_432), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(BgL_new1438z00_9515))->BgL_predsz00) =
					((obj_t) BgL_preds1351z00_433), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(BgL_new1438z00_9515))->BgL_succsz00) =
					((obj_t) BgL_succs1352z00_434), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(BgL_new1438z00_9515))->BgL_firstz00) =
					((obj_t) BgL_first1353z00_435), BUNSPEC);
				return BgL_new1438z00_9515;
			}
		}

	}



/* &make-block */
	BgL_blockz00_bglt BGl_z62makezd2blockzb0zzsaw_defsz00(obj_t BgL_envz00_7672,
		obj_t BgL_label1350z00_7673, obj_t BgL_preds1351z00_7674,
		obj_t BgL_succs1352z00_7675, obj_t BgL_first1353z00_7676)
	{
		{	/* SawMill/defs.sch 950 */
			return
				BGl_makezd2blockzd2zzsaw_defsz00(CINT(BgL_label1350z00_7673),
				BgL_preds1351z00_7674, BgL_succs1352z00_7675, BgL_first1353z00_7676);
		}

	}



/* block? */
	BGL_EXPORTED_DEF bool_t BGl_blockzf3zf3zzsaw_defsz00(obj_t BgL_objz00_436)
	{
		{	/* SawMill/defs.sch 951 */
			{	/* SawMill/defs.sch 951 */
				obj_t BgL_classz00_9518;

				BgL_classz00_9518 = BGl_blockz00zzsaw_defsz00;
				if (BGL_OBJECTP(BgL_objz00_436))
					{	/* SawMill/defs.sch 951 */
						BgL_objectz00_bglt BgL_arg1807z00_9519;

						BgL_arg1807z00_9519 = (BgL_objectz00_bglt) (BgL_objz00_436);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/defs.sch 951 */
								long BgL_idxz00_9520;

								BgL_idxz00_9520 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_9519);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_9520 + 1L)) == BgL_classz00_9518);
							}
						else
							{	/* SawMill/defs.sch 951 */
								bool_t BgL_res3062z00_9523;

								{	/* SawMill/defs.sch 951 */
									obj_t BgL_oclassz00_9524;

									{	/* SawMill/defs.sch 951 */
										obj_t BgL_arg1815z00_9525;
										long BgL_arg1816z00_9526;

										BgL_arg1815z00_9525 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/defs.sch 951 */
											long BgL_arg1817z00_9527;

											BgL_arg1817z00_9527 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_9519);
											BgL_arg1816z00_9526 = (BgL_arg1817z00_9527 - OBJECT_TYPE);
										}
										BgL_oclassz00_9524 =
											VECTOR_REF(BgL_arg1815z00_9525, BgL_arg1816z00_9526);
									}
									{	/* SawMill/defs.sch 951 */
										bool_t BgL__ortest_1115z00_9528;

										BgL__ortest_1115z00_9528 =
											(BgL_classz00_9518 == BgL_oclassz00_9524);
										if (BgL__ortest_1115z00_9528)
											{	/* SawMill/defs.sch 951 */
												BgL_res3062z00_9523 = BgL__ortest_1115z00_9528;
											}
										else
											{	/* SawMill/defs.sch 951 */
												long BgL_odepthz00_9529;

												{	/* SawMill/defs.sch 951 */
													obj_t BgL_arg1804z00_9530;

													BgL_arg1804z00_9530 = (BgL_oclassz00_9524);
													BgL_odepthz00_9529 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_9530);
												}
												if ((1L < BgL_odepthz00_9529))
													{	/* SawMill/defs.sch 951 */
														obj_t BgL_arg1802z00_9531;

														{	/* SawMill/defs.sch 951 */
															obj_t BgL_arg1803z00_9532;

															BgL_arg1803z00_9532 = (BgL_oclassz00_9524);
															BgL_arg1802z00_9531 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_9532,
																1L);
														}
														BgL_res3062z00_9523 =
															(BgL_arg1802z00_9531 == BgL_classz00_9518);
													}
												else
													{	/* SawMill/defs.sch 951 */
														BgL_res3062z00_9523 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res3062z00_9523;
							}
					}
				else
					{	/* SawMill/defs.sch 951 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &block? */
	obj_t BGl_z62blockzf3z91zzsaw_defsz00(obj_t BgL_envz00_7677,
		obj_t BgL_objz00_7678)
	{
		{	/* SawMill/defs.sch 951 */
			return BBOOL(BGl_blockzf3zf3zzsaw_defsz00(BgL_objz00_7678));
		}

	}



/* block-nil */
	BGL_EXPORTED_DEF BgL_blockz00_bglt BGl_blockzd2nilzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.sch 952 */
			{	/* SawMill/defs.sch 952 */
				obj_t BgL_classz00_6103;

				BgL_classz00_6103 = BGl_blockz00zzsaw_defsz00;
				{	/* SawMill/defs.sch 952 */
					obj_t BgL__ortest_1117z00_6104;

					BgL__ortest_1117z00_6104 = BGL_CLASS_NIL(BgL_classz00_6103);
					if (CBOOL(BgL__ortest_1117z00_6104))
						{	/* SawMill/defs.sch 952 */
							return ((BgL_blockz00_bglt) BgL__ortest_1117z00_6104);
						}
					else
						{	/* SawMill/defs.sch 952 */
							return
								((BgL_blockz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_6103));
						}
				}
			}
		}

	}



/* &block-nil */
	BgL_blockz00_bglt BGl_z62blockzd2nilzb0zzsaw_defsz00(obj_t BgL_envz00_7679)
	{
		{	/* SawMill/defs.sch 952 */
			return BGl_blockzd2nilzd2zzsaw_defsz00();
		}

	}



/* block-first */
	BGL_EXPORTED_DEF obj_t BGl_blockzd2firstzd2zzsaw_defsz00(BgL_blockz00_bglt
		BgL_oz00_437)
	{
		{	/* SawMill/defs.sch 953 */
			return (((BgL_blockz00_bglt) COBJECT(BgL_oz00_437))->BgL_firstz00);
		}

	}



/* &block-first */
	obj_t BGl_z62blockzd2firstzb0zzsaw_defsz00(obj_t BgL_envz00_7680,
		obj_t BgL_oz00_7681)
	{
		{	/* SawMill/defs.sch 953 */
			return
				BGl_blockzd2firstzd2zzsaw_defsz00(((BgL_blockz00_bglt) BgL_oz00_7681));
		}

	}



/* block-first-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockzd2firstzd2setz12z12zzsaw_defsz00(BgL_blockz00_bglt BgL_oz00_438,
		obj_t BgL_vz00_439)
	{
		{	/* SawMill/defs.sch 954 */
			return
				((((BgL_blockz00_bglt) COBJECT(BgL_oz00_438))->BgL_firstz00) =
				((obj_t) BgL_vz00_439), BUNSPEC);
		}

	}



/* &block-first-set! */
	obj_t BGl_z62blockzd2firstzd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7682,
		obj_t BgL_oz00_7683, obj_t BgL_vz00_7684)
	{
		{	/* SawMill/defs.sch 954 */
			return
				BGl_blockzd2firstzd2setz12z12zzsaw_defsz00(
				((BgL_blockz00_bglt) BgL_oz00_7683), BgL_vz00_7684);
		}

	}



/* block-succs */
	BGL_EXPORTED_DEF obj_t BGl_blockzd2succszd2zzsaw_defsz00(BgL_blockz00_bglt
		BgL_oz00_440)
	{
		{	/* SawMill/defs.sch 955 */
			return (((BgL_blockz00_bglt) COBJECT(BgL_oz00_440))->BgL_succsz00);
		}

	}



/* &block-succs */
	obj_t BGl_z62blockzd2succszb0zzsaw_defsz00(obj_t BgL_envz00_7685,
		obj_t BgL_oz00_7686)
	{
		{	/* SawMill/defs.sch 955 */
			return
				BGl_blockzd2succszd2zzsaw_defsz00(((BgL_blockz00_bglt) BgL_oz00_7686));
		}

	}



/* block-succs-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockzd2succszd2setz12z12zzsaw_defsz00(BgL_blockz00_bglt BgL_oz00_441,
		obj_t BgL_vz00_442)
	{
		{	/* SawMill/defs.sch 956 */
			return
				((((BgL_blockz00_bglt) COBJECT(BgL_oz00_441))->BgL_succsz00) =
				((obj_t) BgL_vz00_442), BUNSPEC);
		}

	}



/* &block-succs-set! */
	obj_t BGl_z62blockzd2succszd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7687,
		obj_t BgL_oz00_7688, obj_t BgL_vz00_7689)
	{
		{	/* SawMill/defs.sch 956 */
			return
				BGl_blockzd2succszd2setz12z12zzsaw_defsz00(
				((BgL_blockz00_bglt) BgL_oz00_7688), BgL_vz00_7689);
		}

	}



/* block-preds */
	BGL_EXPORTED_DEF obj_t BGl_blockzd2predszd2zzsaw_defsz00(BgL_blockz00_bglt
		BgL_oz00_443)
	{
		{	/* SawMill/defs.sch 957 */
			return (((BgL_blockz00_bglt) COBJECT(BgL_oz00_443))->BgL_predsz00);
		}

	}



/* &block-preds */
	obj_t BGl_z62blockzd2predszb0zzsaw_defsz00(obj_t BgL_envz00_7690,
		obj_t BgL_oz00_7691)
	{
		{	/* SawMill/defs.sch 957 */
			return
				BGl_blockzd2predszd2zzsaw_defsz00(((BgL_blockz00_bglt) BgL_oz00_7691));
		}

	}



/* block-preds-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockzd2predszd2setz12z12zzsaw_defsz00(BgL_blockz00_bglt BgL_oz00_444,
		obj_t BgL_vz00_445)
	{
		{	/* SawMill/defs.sch 958 */
			return
				((((BgL_blockz00_bglt) COBJECT(BgL_oz00_444))->BgL_predsz00) =
				((obj_t) BgL_vz00_445), BUNSPEC);
		}

	}



/* &block-preds-set! */
	obj_t BGl_z62blockzd2predszd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7692,
		obj_t BgL_oz00_7693, obj_t BgL_vz00_7694)
	{
		{	/* SawMill/defs.sch 958 */
			return
				BGl_blockzd2predszd2setz12z12zzsaw_defsz00(
				((BgL_blockz00_bglt) BgL_oz00_7693), BgL_vz00_7694);
		}

	}



/* block-label */
	BGL_EXPORTED_DEF int BGl_blockzd2labelzd2zzsaw_defsz00(BgL_blockz00_bglt
		BgL_oz00_446)
	{
		{	/* SawMill/defs.sch 959 */
			return (((BgL_blockz00_bglt) COBJECT(BgL_oz00_446))->BgL_labelz00);
		}

	}



/* &block-label */
	obj_t BGl_z62blockzd2labelzb0zzsaw_defsz00(obj_t BgL_envz00_7695,
		obj_t BgL_oz00_7696)
	{
		{	/* SawMill/defs.sch 959 */
			return
				BINT(BGl_blockzd2labelzd2zzsaw_defsz00(
					((BgL_blockz00_bglt) BgL_oz00_7696)));
		}

	}



/* block-label-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_blockzd2labelzd2setz12z12zzsaw_defsz00(BgL_blockz00_bglt BgL_oz00_447,
		int BgL_vz00_448)
	{
		{	/* SawMill/defs.sch 960 */
			return
				((((BgL_blockz00_bglt) COBJECT(BgL_oz00_447))->BgL_labelz00) =
				((int) BgL_vz00_448), BUNSPEC);
		}

	}



/* &block-label-set! */
	obj_t BGl_z62blockzd2labelzd2setz12z70zzsaw_defsz00(obj_t BgL_envz00_7697,
		obj_t BgL_oz00_7698, obj_t BgL_vz00_7699)
	{
		{	/* SawMill/defs.sch 960 */
			return
				BGl_blockzd2labelzd2setz12z12zzsaw_defsz00(
				((BgL_blockz00_bglt) BgL_oz00_7698), CINT(BgL_vz00_7699));
		}

	}



/* rtl_ins-args* */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_inszd2argsza2z70zzsaw_defsz00(BgL_rtl_insz00_bglt BgL_insz00_449)
	{
		{	/* SawMill/defs.scm 110 */
			{	/* SawMill/defs.scm 111 */
				obj_t BgL_g1442z00_2055;

				BgL_g1442z00_2055 =
					(((BgL_rtl_insz00_bglt) COBJECT(BgL_insz00_449))->BgL_argsz00);
				{
					obj_t BgL_argsz00_2058;
					obj_t BgL_resz00_2059;

					BgL_argsz00_2058 = BgL_g1442z00_2055;
					BgL_resz00_2059 = BNIL;
				BgL_zc3z04anonymousza31885ze3z87_2060:
					if (NULLP(BgL_argsz00_2058))
						{	/* SawMill/defs.scm 114 */
							return BgL_resz00_2059;
						}
					else
						{	/* SawMill/defs.scm 116 */
							bool_t BgL_test4292z00_12803;

							{	/* SawMill/defs.scm 116 */
								obj_t BgL_arg1902z00_2075;

								BgL_arg1902z00_2075 = CAR(((obj_t) BgL_argsz00_2058));
								{	/* SawMill/defs.scm 116 */
									obj_t BgL_classz00_6107;

									BgL_classz00_6107 = BGl_rtl_regz00zzsaw_defsz00;
									if (BGL_OBJECTP(BgL_arg1902z00_2075))
										{	/* SawMill/defs.scm 116 */
											BgL_objectz00_bglt BgL_arg1807z00_6109;

											BgL_arg1807z00_6109 =
												(BgL_objectz00_bglt) (BgL_arg1902z00_2075);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawMill/defs.scm 116 */
													long BgL_idxz00_6115;

													BgL_idxz00_6115 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6109);
													BgL_test4292z00_12803 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_6115 + 1L)) == BgL_classz00_6107);
												}
											else
												{	/* SawMill/defs.scm 116 */
													bool_t BgL_res3063z00_6140;

													{	/* SawMill/defs.scm 116 */
														obj_t BgL_oclassz00_6123;

														{	/* SawMill/defs.scm 116 */
															obj_t BgL_arg1815z00_6131;
															long BgL_arg1816z00_6132;

															BgL_arg1815z00_6131 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawMill/defs.scm 116 */
																long BgL_arg1817z00_6133;

																BgL_arg1817z00_6133 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6109);
																BgL_arg1816z00_6132 =
																	(BgL_arg1817z00_6133 - OBJECT_TYPE);
															}
															BgL_oclassz00_6123 =
																VECTOR_REF(BgL_arg1815z00_6131,
																BgL_arg1816z00_6132);
														}
														{	/* SawMill/defs.scm 116 */
															bool_t BgL__ortest_1115z00_6124;

															BgL__ortest_1115z00_6124 =
																(BgL_classz00_6107 == BgL_oclassz00_6123);
															if (BgL__ortest_1115z00_6124)
																{	/* SawMill/defs.scm 116 */
																	BgL_res3063z00_6140 =
																		BgL__ortest_1115z00_6124;
																}
															else
																{	/* SawMill/defs.scm 116 */
																	long BgL_odepthz00_6125;

																	{	/* SawMill/defs.scm 116 */
																		obj_t BgL_arg1804z00_6126;

																		BgL_arg1804z00_6126 = (BgL_oclassz00_6123);
																		BgL_odepthz00_6125 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_6126);
																	}
																	if ((1L < BgL_odepthz00_6125))
																		{	/* SawMill/defs.scm 116 */
																			obj_t BgL_arg1802z00_6128;

																			{	/* SawMill/defs.scm 116 */
																				obj_t BgL_arg1803z00_6129;

																				BgL_arg1803z00_6129 =
																					(BgL_oclassz00_6123);
																				BgL_arg1802z00_6128 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_6129, 1L);
																			}
																			BgL_res3063z00_6140 =
																				(BgL_arg1802z00_6128 ==
																				BgL_classz00_6107);
																		}
																	else
																		{	/* SawMill/defs.scm 116 */
																			BgL_res3063z00_6140 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test4292z00_12803 = BgL_res3063z00_6140;
												}
										}
									else
										{	/* SawMill/defs.scm 116 */
											BgL_test4292z00_12803 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test4292z00_12803)
								{	/* SawMill/defs.scm 117 */
									obj_t BgL_arg1889z00_2064;
									obj_t BgL_arg1890z00_2065;

									BgL_arg1889z00_2064 = CDR(((obj_t) BgL_argsz00_2058));
									{	/* SawMill/defs.scm 117 */
										obj_t BgL_arg1891z00_2066;

										BgL_arg1891z00_2066 = CAR(((obj_t) BgL_argsz00_2058));
										BgL_arg1890z00_2065 =
											MAKE_YOUNG_PAIR(BgL_arg1891z00_2066, BgL_resz00_2059);
									}
									{
										obj_t BgL_resz00_12834;
										obj_t BgL_argsz00_12833;

										BgL_argsz00_12833 = BgL_arg1889z00_2064;
										BgL_resz00_12834 = BgL_arg1890z00_2065;
										BgL_resz00_2059 = BgL_resz00_12834;
										BgL_argsz00_2058 = BgL_argsz00_12833;
										goto BgL_zc3z04anonymousza31885ze3z87_2060;
									}
								}
							else
								{	/* SawMill/defs.scm 118 */
									bool_t BgL_test4297z00_12835;

									{	/* SawMill/defs.scm 118 */
										obj_t BgL_arg1901z00_2074;

										BgL_arg1901z00_2074 = CAR(((obj_t) BgL_argsz00_2058));
										{	/* SawMill/defs.scm 118 */
											obj_t BgL_classz00_6144;

											BgL_classz00_6144 = BGl_rtl_insz00zzsaw_defsz00;
											if (BGL_OBJECTP(BgL_arg1901z00_2074))
												{	/* SawMill/defs.scm 118 */
													BgL_objectz00_bglt BgL_arg1807z00_6146;

													BgL_arg1807z00_6146 =
														(BgL_objectz00_bglt) (BgL_arg1901z00_2074);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* SawMill/defs.scm 118 */
															long BgL_idxz00_6152;

															BgL_idxz00_6152 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6146);
															BgL_test4297z00_12835 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_6152 + 1L)) == BgL_classz00_6144);
														}
													else
														{	/* SawMill/defs.scm 118 */
															bool_t BgL_res3064z00_6177;

															{	/* SawMill/defs.scm 118 */
																obj_t BgL_oclassz00_6160;

																{	/* SawMill/defs.scm 118 */
																	obj_t BgL_arg1815z00_6168;
																	long BgL_arg1816z00_6169;

																	BgL_arg1815z00_6168 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* SawMill/defs.scm 118 */
																		long BgL_arg1817z00_6170;

																		BgL_arg1817z00_6170 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6146);
																		BgL_arg1816z00_6169 =
																			(BgL_arg1817z00_6170 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_6160 =
																		VECTOR_REF(BgL_arg1815z00_6168,
																		BgL_arg1816z00_6169);
																}
																{	/* SawMill/defs.scm 118 */
																	bool_t BgL__ortest_1115z00_6161;

																	BgL__ortest_1115z00_6161 =
																		(BgL_classz00_6144 == BgL_oclassz00_6160);
																	if (BgL__ortest_1115z00_6161)
																		{	/* SawMill/defs.scm 118 */
																			BgL_res3064z00_6177 =
																				BgL__ortest_1115z00_6161;
																		}
																	else
																		{	/* SawMill/defs.scm 118 */
																			long BgL_odepthz00_6162;

																			{	/* SawMill/defs.scm 118 */
																				obj_t BgL_arg1804z00_6163;

																				BgL_arg1804z00_6163 =
																					(BgL_oclassz00_6160);
																				BgL_odepthz00_6162 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_6163);
																			}
																			if ((1L < BgL_odepthz00_6162))
																				{	/* SawMill/defs.scm 118 */
																					obj_t BgL_arg1802z00_6165;

																					{	/* SawMill/defs.scm 118 */
																						obj_t BgL_arg1803z00_6166;

																						BgL_arg1803z00_6166 =
																							(BgL_oclassz00_6160);
																						BgL_arg1802z00_6165 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_6166, 1L);
																					}
																					BgL_res3064z00_6177 =
																						(BgL_arg1802z00_6165 ==
																						BgL_classz00_6144);
																				}
																			else
																				{	/* SawMill/defs.scm 118 */
																					BgL_res3064z00_6177 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test4297z00_12835 = BgL_res3064z00_6177;
														}
												}
											else
												{	/* SawMill/defs.scm 118 */
													BgL_test4297z00_12835 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test4297z00_12835)
										{	/* SawMill/defs.scm 119 */
											obj_t BgL_arg1894z00_2069;
											obj_t BgL_arg1896z00_2070;

											BgL_arg1894z00_2069 = CDR(((obj_t) BgL_argsz00_2058));
											{	/* SawMill/defs.scm 119 */
												obj_t BgL_arg1897z00_2071;

												{	/* SawMill/defs.scm 119 */
													obj_t BgL_arg1898z00_2072;

													BgL_arg1898z00_2072 = CAR(((obj_t) BgL_argsz00_2058));
													BgL_arg1897z00_2071 =
														BGl_rtl_inszd2argsza2z70zzsaw_defsz00(
														((BgL_rtl_insz00_bglt) BgL_arg1898z00_2072));
												}
												BgL_arg1896z00_2070 =
													BGl_appendzd221011zd2zzsaw_defsz00
													(BgL_arg1897z00_2071, BgL_resz00_2059);
											}
											{
												obj_t BgL_resz00_12868;
												obj_t BgL_argsz00_12867;

												BgL_argsz00_12867 = BgL_arg1894z00_2069;
												BgL_resz00_12868 = BgL_arg1896z00_2070;
												BgL_resz00_2059 = BgL_resz00_12868;
												BgL_argsz00_2058 = BgL_argsz00_12867;
												goto BgL_zc3z04anonymousza31885ze3z87_2060;
											}
										}
									else
										{	/* SawMill/defs.scm 121 */
											obj_t BgL_arg1899z00_2073;

											BgL_arg1899z00_2073 = CDR(((obj_t) BgL_argsz00_2058));
											{
												obj_t BgL_argsz00_12871;

												BgL_argsz00_12871 = BgL_arg1899z00_2073;
												BgL_argsz00_2058 = BgL_argsz00_12871;
												goto BgL_zc3z04anonymousza31885ze3z87_2060;
											}
										}
								}
						}
				}
			}
		}

	}



/* &rtl_ins-args* */
	obj_t BGl_z62rtl_inszd2argsza2z12zzsaw_defsz00(obj_t BgL_envz00_7700,
		obj_t BgL_insz00_7701)
	{
		{	/* SawMill/defs.scm 110 */
			return
				BGl_rtl_inszd2argsza2z70zzsaw_defsz00(
				((BgL_rtl_insz00_bglt) BgL_insz00_7701));
		}

	}



/* dump-basic-blocks */
	BGL_EXPORTED_DEF obj_t BGl_dumpzd2basiczd2blocksz00zzsaw_defsz00(obj_t
		BgL_idz00_452, obj_t BgL_vz00_453, obj_t BgL_paramsz00_454,
		obj_t BgL_lz00_455)
	{
		{	/* SawMill/defs.scm 160 */
			{	/* SawMill/defs.scm 161 */
				obj_t BgL_port1655z00_2077;

				BgL_port1655z00_2077 = BGl_za2tracezd2portza2zd2zztools_tracez00;
				{	/* SawMill/defs.scm 161 */
					obj_t BgL_tmpz00_12874;

					BgL_tmpz00_12874 = ((obj_t) BgL_port1655z00_2077);
					bgl_display_string(BGl_string3084z00zzsaw_defsz00, BgL_tmpz00_12874);
				}
				bgl_display_obj(BgL_idz00_452, BgL_port1655z00_2077);
				{	/* SawMill/defs.scm 161 */
					obj_t BgL_tmpz00_12878;

					BgL_tmpz00_12878 = ((obj_t) BgL_port1655z00_2077);
					bgl_display_string(BGl_string3085z00zzsaw_defsz00, BgL_tmpz00_12878);
				}
				bgl_display_obj(BGl_shapez00zztools_shapez00(BgL_vz00_453),
					BgL_port1655z00_2077);
				{	/* SawMill/defs.scm 161 */
					obj_t BgL_tmpz00_12883;

					BgL_tmpz00_12883 = ((obj_t) BgL_port1655z00_2077);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_12883);
			}}
			bgl_display_string(BGl_string3086z00zzsaw_defsz00,
				BGl_za2tracezd2portza2zd2zztools_tracez00);
			if (NULLP(BgL_paramsz00_454))
				{	/* SawMill/defs.scm 163 */
					BNIL;
				}
			else
				{	/* SawMill/defs.scm 163 */
					obj_t BgL_head1658z00_2081;

					BgL_head1658z00_2081 = MAKE_YOUNG_PAIR(BNIL, BNIL);
					{
						obj_t BgL_l1656z00_2083;
						obj_t BgL_tail1659z00_2084;

						BgL_l1656z00_2083 = BgL_paramsz00_454;
						BgL_tail1659z00_2084 = BgL_head1658z00_2081;
					BgL_zc3z04anonymousza31905ze3z87_2085:
						if (NULLP(BgL_l1656z00_2083))
							{	/* SawMill/defs.scm 163 */
								CDR(BgL_head1658z00_2081);
							}
						else
							{	/* SawMill/defs.scm 163 */
								obj_t BgL_newtail1660z00_2087;

								{	/* SawMill/defs.scm 163 */
									obj_t BgL_arg1911z00_2089;

									{	/* SawMill/defs.scm 163 */
										obj_t BgL_az00_2090;

										BgL_az00_2090 = CAR(((obj_t) BgL_l1656z00_2083));
										bgl_display_string(BGl_string3085z00zzsaw_defsz00,
											BGl_za2tracezd2portza2zd2zztools_tracez00);
										BgL_arg1911z00_2089 =
											BGl_dumpz00zzsaw_defsz00(BgL_az00_2090,
											BGl_za2tracezd2portza2zd2zztools_tracez00, (int) (0L));
									}
									BgL_newtail1660z00_2087 =
										MAKE_YOUNG_PAIR(BgL_arg1911z00_2089, BNIL);
								}
								SET_CDR(BgL_tail1659z00_2084, BgL_newtail1660z00_2087);
								{	/* SawMill/defs.scm 163 */
									obj_t BgL_arg1910z00_2088;

									BgL_arg1910z00_2088 = CDR(((obj_t) BgL_l1656z00_2083));
									{
										obj_t BgL_tail1659z00_12903;
										obj_t BgL_l1656z00_12902;

										BgL_l1656z00_12902 = BgL_arg1910z00_2088;
										BgL_tail1659z00_12903 = BgL_newtail1660z00_2087;
										BgL_tail1659z00_2084 = BgL_tail1659z00_12903;
										BgL_l1656z00_2083 = BgL_l1656z00_12902;
										goto BgL_zc3z04anonymousza31905ze3z87_2085;
									}
								}
							}
					}
				}
			bgl_display_char(((unsigned char) 10),
				BGl_za2tracezd2portza2zd2zztools_tracez00);
			{	/* SawMill/defs.scm 168 */
				obj_t BgL_port1661z00_2092;

				BgL_port1661z00_2092 = BGl_za2tracezd2portza2zd2zztools_tracez00;
				{	/* SawMill/defs.scm 168 */
					obj_t BgL_tmpz00_12905;

					BgL_tmpz00_12905 = ((obj_t) BgL_port1661z00_2092);
					bgl_display_string(BGl_string3087z00zzsaw_defsz00, BgL_tmpz00_12905);
				}
				{	/* SawMill/defs.scm 168 */
					obj_t BgL_tmpz00_12908;

					BgL_tmpz00_12908 = ((obj_t) BgL_port1661z00_2092);
					bgl_display_char(((unsigned char) 10), BgL_tmpz00_12908);
			}}
			{
				obj_t BgL_l1662z00_2094;

				{	/* SawMill/defs.scm 169 */
					bool_t BgL_tmpz00_12911;

					BgL_l1662z00_2094 = BgL_lz00_455;
				BgL_zc3z04anonymousza31912ze3z87_2095:
					if (PAIRP(BgL_l1662z00_2094))
						{	/* SawMill/defs.scm 169 */
							{	/* SawMill/defs.scm 170 */
								obj_t BgL_bz00_2097;

								BgL_bz00_2097 = CAR(BgL_l1662z00_2094);
								{	/* SawMill/defs.scm 170 */
									obj_t BgL_portz00_6199;

									BgL_portz00_6199 = BGl_za2tracezd2portza2zd2zztools_tracez00;
									BGl_dumpz00zzsaw_defsz00(BgL_bz00_2097, BgL_portz00_6199,
										(int) (0L));
									bgl_display_char(((unsigned char) 10), BgL_portz00_6199);
								}
								bgl_display_char(((unsigned char) 10),
									BGl_za2tracezd2portza2zd2zztools_tracez00);
							}
							{
								obj_t BgL_l1662z00_12919;

								BgL_l1662z00_12919 = CDR(BgL_l1662z00_2094);
								BgL_l1662z00_2094 = BgL_l1662z00_12919;
								goto BgL_zc3z04anonymousza31912ze3z87_2095;
							}
						}
					else
						{	/* SawMill/defs.scm 169 */
							BgL_tmpz00_12911 = ((bool_t) 1);
						}
					return BBOOL(BgL_tmpz00_12911);
				}
			}
		}

	}



/* &dump-basic-blocks */
	obj_t BGl_z62dumpzd2basiczd2blocksz62zzsaw_defsz00(obj_t BgL_envz00_7702,
		obj_t BgL_idz00_7703, obj_t BgL_vz00_7704, obj_t BgL_paramsz00_7705,
		obj_t BgL_lz00_7706)
	{
		{	/* SawMill/defs.scm 160 */
			return
				BGl_dumpzd2basiczd2blocksz00zzsaw_defsz00(BgL_idz00_7703, BgL_vz00_7704,
				BgL_paramsz00_7705, BgL_lz00_7706);
		}

	}



/* rtl-dump */
	BGL_EXPORTED_DEF obj_t BGl_rtlzd2dumpzd2zzsaw_defsz00(obj_t BgL_objz00_456,
		obj_t BgL_portz00_457)
	{
		{	/* SawMill/defs.scm 177 */
			BGl_dumpz00zzsaw_defsz00(BgL_objz00_456, BgL_portz00_457, (int) (0L));
			return bgl_display_char(((unsigned char) 10), BgL_portz00_457);
		}

	}



/* &rtl-dump */
	obj_t BGl_z62rtlzd2dumpzb0zzsaw_defsz00(obj_t BgL_envz00_7707,
		obj_t BgL_objz00_7708, obj_t BgL_portz00_7709)
	{
		{	/* SawMill/defs.scm 177 */
			return BGl_rtlzd2dumpzd2zzsaw_defsz00(BgL_objz00_7708, BgL_portz00_7709);
		}

	}



/* dump-margin */
	BGL_EXPORTED_DEF obj_t BGl_dumpzd2marginzd2zzsaw_defsz00(obj_t BgL_pz00_458,
		int BgL_mz00_459)
	{
		{	/* SawMill/defs.scm 184 */
			{	/* SawMill/defs.scm 185 */
				obj_t BgL_mgsz00_2100;

				BgL_mgsz00_2100 = CNST_TABLE_REF(0);
				if (((long) (BgL_mz00_459) < VECTOR_LENGTH(BgL_mgsz00_2100)))
					{	/* SawMill/defs.scm 186 */
						return
							bgl_display_obj(VECTOR_REF(BgL_mgsz00_2100,
								(long) (BgL_mz00_459)), BgL_pz00_458);
					}
				else
					{	/* SawMill/defs.scm 186 */
						return
							bgl_display_obj(make_string(
								(long) (BgL_mz00_459), ((unsigned char) ' ')), BgL_pz00_458);
		}}}

	}



/* &dump-margin */
	obj_t BGl_z62dumpzd2marginzb0zzsaw_defsz00(obj_t BgL_envz00_7710,
		obj_t BgL_pz00_7711, obj_t BgL_mz00_7712)
	{
		{	/* SawMill/defs.scm 184 */
			return
				BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_7711, CINT(BgL_mz00_7712));
		}

	}



/* dump* */
	BGL_EXPORTED_DEF obj_t BGl_dumpza2za2zzsaw_defsz00(obj_t BgL_oz00_463,
		obj_t BgL_pz00_464, int BgL_mz00_465)
	{
		{	/* SawMill/defs.scm 211 */
			if (NULLP(BgL_oz00_463))
				{	/* SawMill/defs.scm 213 */
					return BUNSPEC;
				}
			else
				{	/* SawMill/defs.scm 213 */
					if (NULLP(CDR(((obj_t) BgL_oz00_463))))
						{	/* SawMill/defs.scm 216 */
							obj_t BgL_arg1925z00_2109;

							BgL_arg1925z00_2109 = CAR(((obj_t) BgL_oz00_463));
							BGL_TAIL return
								BGl_dumpz00zzsaw_defsz00(BgL_arg1925z00_2109, BgL_pz00_464,
								BgL_mz00_465);
						}
					else
						{
							obj_t BgL_oz00_2111;

							{	/* SawMill/defs.scm 218 */
								bool_t BgL_tmpz00_12949;

								BgL_oz00_2111 = BgL_oz00_463;
							BgL_zc3z04anonymousza31926ze3z87_2112:
								{	/* SawMill/defs.scm 219 */
									obj_t BgL_arg1927z00_2113;

									BgL_arg1927z00_2113 = CAR(((obj_t) BgL_oz00_2111));
									BGl_dumpz00zzsaw_defsz00(BgL_arg1927z00_2113, BgL_pz00_464,
										BgL_mz00_465);
								}
								{	/* SawMill/defs.scm 220 */
									bool_t BgL_test4308z00_12953;

									{	/* SawMill/defs.scm 220 */
										obj_t BgL_tmpz00_12954;

										BgL_tmpz00_12954 = CDR(((obj_t) BgL_oz00_2111));
										BgL_test4308z00_12953 = PAIRP(BgL_tmpz00_12954);
									}
									if (BgL_test4308z00_12953)
										{	/* SawMill/defs.scm 220 */
											bgl_display_char(((unsigned char) 10), BgL_pz00_464);
											BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_464,
												BgL_mz00_465);
											{	/* SawMill/defs.scm 223 */
												obj_t BgL_arg1930z00_2116;

												BgL_arg1930z00_2116 = CDR(((obj_t) BgL_oz00_2111));
												{
													obj_t BgL_oz00_12962;

													BgL_oz00_12962 = BgL_arg1930z00_2116;
													BgL_oz00_2111 = BgL_oz00_12962;
													goto BgL_zc3z04anonymousza31926ze3z87_2112;
												}
											}
										}
									else
										{	/* SawMill/defs.scm 220 */
											BgL_tmpz00_12949 = ((bool_t) 0);
										}
								}
								return BBOOL(BgL_tmpz00_12949);
							}
						}
				}
		}

	}



/* &dump* */
	obj_t BGl_z62dumpza2zc0zzsaw_defsz00(obj_t BgL_envz00_7713,
		obj_t BgL_oz00_7714, obj_t BgL_pz00_7715, obj_t BgL_mz00_7716)
	{
		{	/* SawMill/defs.scm 211 */
			return
				BGl_dumpza2za2zzsaw_defsz00(BgL_oz00_7714, BgL_pz00_7715,
				CINT(BgL_mz00_7716));
		}

	}



/* dump-args */
	bool_t BGl_dumpzd2argszd2zzsaw_defsz00(obj_t BgL_argsz00_466,
		obj_t BgL_pz00_467)
	{
		{	/* SawMill/defs.scm 228 */
			{
				obj_t BgL_argsz00_2121;

				BgL_argsz00_2121 = BgL_argsz00_466;
			BgL_zc3z04anonymousza31933ze3z87_2122:
				if (PAIRP(BgL_argsz00_2121))
					{	/* SawMill/defs.scm 231 */
						obj_t BgL_az00_2124;

						BgL_az00_2124 = CAR(BgL_argsz00_2121);
						{	/* SawMill/defs.scm 233 */
							bool_t BgL_test4310z00_12969;

							{	/* SawMill/defs.scm 233 */
								obj_t BgL_classz00_6217;

								BgL_classz00_6217 = BGl_rtl_regz00zzsaw_defsz00;
								if (BGL_OBJECTP(BgL_az00_2124))
									{	/* SawMill/defs.scm 233 */
										BgL_objectz00_bglt BgL_arg1807z00_6219;

										BgL_arg1807z00_6219 = (BgL_objectz00_bglt) (BgL_az00_2124);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* SawMill/defs.scm 233 */
												long BgL_idxz00_6225;

												BgL_idxz00_6225 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6219);
												BgL_test4310z00_12969 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_6225 + 1L)) == BgL_classz00_6217);
											}
										else
											{	/* SawMill/defs.scm 233 */
												bool_t BgL_res3065z00_6250;

												{	/* SawMill/defs.scm 233 */
													obj_t BgL_oclassz00_6233;

													{	/* SawMill/defs.scm 233 */
														obj_t BgL_arg1815z00_6241;
														long BgL_arg1816z00_6242;

														BgL_arg1815z00_6241 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* SawMill/defs.scm 233 */
															long BgL_arg1817z00_6243;

															BgL_arg1817z00_6243 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6219);
															BgL_arg1816z00_6242 =
																(BgL_arg1817z00_6243 - OBJECT_TYPE);
														}
														BgL_oclassz00_6233 =
															VECTOR_REF(BgL_arg1815z00_6241,
															BgL_arg1816z00_6242);
													}
													{	/* SawMill/defs.scm 233 */
														bool_t BgL__ortest_1115z00_6234;

														BgL__ortest_1115z00_6234 =
															(BgL_classz00_6217 == BgL_oclassz00_6233);
														if (BgL__ortest_1115z00_6234)
															{	/* SawMill/defs.scm 233 */
																BgL_res3065z00_6250 = BgL__ortest_1115z00_6234;
															}
														else
															{	/* SawMill/defs.scm 233 */
																long BgL_odepthz00_6235;

																{	/* SawMill/defs.scm 233 */
																	obj_t BgL_arg1804z00_6236;

																	BgL_arg1804z00_6236 = (BgL_oclassz00_6233);
																	BgL_odepthz00_6235 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_6236);
																}
																if ((1L < BgL_odepthz00_6235))
																	{	/* SawMill/defs.scm 233 */
																		obj_t BgL_arg1802z00_6238;

																		{	/* SawMill/defs.scm 233 */
																			obj_t BgL_arg1803z00_6239;

																			BgL_arg1803z00_6239 =
																				(BgL_oclassz00_6233);
																			BgL_arg1802z00_6238 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_6239, 1L);
																		}
																		BgL_res3065z00_6250 =
																			(BgL_arg1802z00_6238 ==
																			BgL_classz00_6217);
																	}
																else
																	{	/* SawMill/defs.scm 233 */
																		BgL_res3065z00_6250 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test4310z00_12969 = BgL_res3065z00_6250;
											}
									}
								else
									{	/* SawMill/defs.scm 233 */
										BgL_test4310z00_12969 = ((bool_t) 0);
									}
							}
							if (BgL_test4310z00_12969)
								{	/* SawMill/defs.scm 233 */
									{	/* SawMill/defs.scm 234 */
										obj_t BgL_tmpz00_12992;

										BgL_tmpz00_12992 = ((obj_t) BgL_pz00_467);
										bgl_display_string(BGl_string3085z00zzsaw_defsz00,
											BgL_tmpz00_12992);
									}
									BGl_dumpz00zzsaw_defsz00(BgL_az00_2124, BgL_pz00_467,
										(int) (0L));
								}
							else
								{	/* SawMill/defs.scm 236 */
									bool_t BgL_test4315z00_12997;

									{	/* SawMill/defs.scm 236 */
										obj_t BgL_classz00_6253;

										BgL_classz00_6253 = BGl_rtl_insz00zzsaw_defsz00;
										if (BGL_OBJECTP(BgL_az00_2124))
											{	/* SawMill/defs.scm 236 */
												BgL_objectz00_bglt BgL_arg1807z00_6255;

												BgL_arg1807z00_6255 =
													(BgL_objectz00_bglt) (BgL_az00_2124);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* SawMill/defs.scm 236 */
														long BgL_idxz00_6261;

														BgL_idxz00_6261 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6255);
														BgL_test4315z00_12997 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_6261 + 1L)) == BgL_classz00_6253);
													}
												else
													{	/* SawMill/defs.scm 236 */
														bool_t BgL_res3066z00_6286;

														{	/* SawMill/defs.scm 236 */
															obj_t BgL_oclassz00_6269;

															{	/* SawMill/defs.scm 236 */
																obj_t BgL_arg1815z00_6277;
																long BgL_arg1816z00_6278;

																BgL_arg1815z00_6277 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* SawMill/defs.scm 236 */
																	long BgL_arg1817z00_6279;

																	BgL_arg1817z00_6279 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6255);
																	BgL_arg1816z00_6278 =
																		(BgL_arg1817z00_6279 - OBJECT_TYPE);
																}
																BgL_oclassz00_6269 =
																	VECTOR_REF(BgL_arg1815z00_6277,
																	BgL_arg1816z00_6278);
															}
															{	/* SawMill/defs.scm 236 */
																bool_t BgL__ortest_1115z00_6270;

																BgL__ortest_1115z00_6270 =
																	(BgL_classz00_6253 == BgL_oclassz00_6269);
																if (BgL__ortest_1115z00_6270)
																	{	/* SawMill/defs.scm 236 */
																		BgL_res3066z00_6286 =
																			BgL__ortest_1115z00_6270;
																	}
																else
																	{	/* SawMill/defs.scm 236 */
																		long BgL_odepthz00_6271;

																		{	/* SawMill/defs.scm 236 */
																			obj_t BgL_arg1804z00_6272;

																			BgL_arg1804z00_6272 =
																				(BgL_oclassz00_6269);
																			BgL_odepthz00_6271 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_6272);
																		}
																		if ((1L < BgL_odepthz00_6271))
																			{	/* SawMill/defs.scm 236 */
																				obj_t BgL_arg1802z00_6274;

																				{	/* SawMill/defs.scm 236 */
																					obj_t BgL_arg1803z00_6275;

																					BgL_arg1803z00_6275 =
																						(BgL_oclassz00_6269);
																					BgL_arg1802z00_6274 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_6275, 1L);
																				}
																				BgL_res3066z00_6286 =
																					(BgL_arg1802z00_6274 ==
																					BgL_classz00_6253);
																			}
																		else
																			{	/* SawMill/defs.scm 236 */
																				BgL_res3066z00_6286 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test4315z00_12997 = BgL_res3066z00_6286;
													}
											}
										else
											{	/* SawMill/defs.scm 236 */
												BgL_test4315z00_12997 = ((bool_t) 0);
											}
									}
									if (BgL_test4315z00_12997)
										{	/* SawMill/defs.scm 236 */
											{	/* SawMill/defs.scm 237 */
												obj_t BgL_tmpz00_13020;

												BgL_tmpz00_13020 = ((obj_t) BgL_pz00_467);
												bgl_display_string(BGl_string3085z00zzsaw_defsz00,
													BgL_tmpz00_13020);
											}
											{	/* SawMill/defs.scm 287 */
												obj_t BgL_tmpz00_13023;

												BgL_tmpz00_13023 = ((obj_t) BgL_pz00_467);
												bgl_display_string(BGl_string3088z00zzsaw_defsz00,
													BgL_tmpz00_13023);
											}
											{	/* SawMill/defs.scm 288 */
												BgL_rtl_funz00_bglt BgL_arg1938z00_6291;
												obj_t BgL_arg1939z00_6292;
												obj_t BgL_arg1940z00_6293;

												BgL_arg1938z00_6291 =
													(((BgL_rtl_insz00_bglt) COBJECT(
															((BgL_rtl_insz00_bglt) BgL_az00_2124)))->
													BgL_funz00);
												BgL_arg1939z00_6292 =
													(((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
																BgL_az00_2124)))->BgL_destz00);
												BgL_arg1940z00_6293 =
													(((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
																BgL_az00_2124)))->BgL_argsz00);
												BGl_dumpzd2funzd2zzsaw_defsz00(BgL_arg1938z00_6291,
													BgL_arg1939z00_6292, BgL_arg1940z00_6293,
													BgL_pz00_467, BINT(0L));
											}
											{	/* SawMill/defs.scm 289 */
												obj_t BgL_tmpz00_13034;

												BgL_tmpz00_13034 = ((obj_t) BgL_pz00_467);
												bgl_display_string(BGl_string3089z00zzsaw_defsz00,
													BgL_tmpz00_13034);
											}
										}
									else
										{	/* SawMill/defs.scm 236 */
											{	/* SawMill/defs.scm 240 */
												obj_t BgL_tmpz00_13037;

												BgL_tmpz00_13037 = ((obj_t) BgL_pz00_467);
												bgl_display_string(BGl_string3085z00zzsaw_defsz00,
													BgL_tmpz00_13037);
											}
											bgl_display_obj(BgL_az00_2124, BgL_pz00_467);
										}
								}
						}
						{
							obj_t BgL_argsz00_13041;

							BgL_argsz00_13041 = CDR(BgL_argsz00_2121);
							BgL_argsz00_2121 = BgL_argsz00_13041;
							goto BgL_zc3z04anonymousza31933ze3z87_2122;
						}
					}
				else
					{	/* SawMill/defs.scm 230 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* dump-ins-rhs */
	BGL_EXPORTED_DEF obj_t
		BGl_dumpzd2inszd2rhsz00zzsaw_defsz00(BgL_rtl_insz00_bglt BgL_oz00_474,
		obj_t BgL_pz00_475, obj_t BgL_mz00_476)
	{
		{	/* SawMill/defs.scm 285 */
			{	/* SawMill/defs.scm 287 */
				obj_t BgL_tmpz00_13043;

				BgL_tmpz00_13043 = ((obj_t) BgL_pz00_475);
				bgl_display_string(BGl_string3088z00zzsaw_defsz00, BgL_tmpz00_13043);
			}
			BGl_dumpzd2funzd2zzsaw_defsz00(
				(((BgL_rtl_insz00_bglt) COBJECT(BgL_oz00_474))->BgL_funz00),
				(((BgL_rtl_insz00_bglt) COBJECT(BgL_oz00_474))->BgL_destz00),
				(((BgL_rtl_insz00_bglt) COBJECT(BgL_oz00_474))->BgL_argsz00),
				BgL_pz00_475, BgL_mz00_476);
			{	/* SawMill/defs.scm 289 */
				obj_t BgL_tmpz00_13050;

				BgL_tmpz00_13050 = ((obj_t) BgL_pz00_475);
				return
					bgl_display_string(BGl_string3089z00zzsaw_defsz00, BgL_tmpz00_13050);
			}
		}

	}



/* &dump-ins-rhs */
	obj_t BGl_z62dumpzd2inszd2rhsz62zzsaw_defsz00(obj_t BgL_envz00_7717,
		obj_t BgL_oz00_7718, obj_t BgL_pz00_7719, obj_t BgL_mz00_7720)
	{
		{	/* SawMill/defs.scm 285 */
			return
				BGl_dumpzd2inszd2rhsz00zzsaw_defsz00(
				((BgL_rtl_insz00_bglt) BgL_oz00_7718), BgL_pz00_7719, BgL_mz00_7720);
		}

	}



/* show-fun */
	obj_t BGl_showzd2funzd2zzsaw_defsz00(BgL_rtl_funz00_bglt BgL_oz00_480,
		obj_t BgL_destz00_481, obj_t BgL_pz00_482)
	{
		{	/* SawMill/defs.scm 303 */
			{	/* SawMill/defs.scm 304 */
				obj_t BgL_cz00_2133;

				{	/* SawMill/defs.scm 304 */
					obj_t BgL_arg1944z00_2137;

					{	/* SawMill/defs.scm 304 */
						obj_t BgL_arg1945z00_2138;

						{	/* SawMill/defs.scm 304 */
							obj_t BgL_arg1815z00_6306;
							long BgL_arg1816z00_6307;

							BgL_arg1815z00_6306 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/defs.scm 304 */
								long BgL_arg1817z00_6308;

								BgL_arg1817z00_6308 =
									BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_oz00_480));
								BgL_arg1816z00_6307 = (BgL_arg1817z00_6308 - OBJECT_TYPE);
							}
							BgL_arg1945z00_2138 =
								VECTOR_REF(BgL_arg1815z00_6306, BgL_arg1816z00_6307);
						}
						BgL_arg1944z00_2137 =
							BGl_classzd2namezd2zz__objectz00(BgL_arg1945z00_2138);
					}
					{	/* SawMill/defs.scm 304 */
						obj_t BgL_arg1455z00_6315;

						BgL_arg1455z00_6315 = SYMBOL_TO_STRING(BgL_arg1944z00_2137);
						BgL_cz00_2133 =
							BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_6315);
				}}
				bgl_display_obj(c_substring(BgL_cz00_2133, 4L,
						STRING_LENGTH(BgL_cz00_2133)), BgL_pz00_482);
				{	/* SawMill/defs.scm 306 */
					bool_t BgL_test4320z00_13066;

					if (CBOOL(BgL_destz00_481))
						{	/* SawMill/defs.scm 306 */
							BgL_test4320z00_13066 = CBOOL(BGl_debugzd2sawzd2zzsaw_defsz00);
						}
					else
						{	/* SawMill/defs.scm 306 */
							BgL_test4320z00_13066 = ((bool_t) 0);
						}
					if (BgL_test4320z00_13066)
						{	/* SawMill/defs.scm 306 */
							{	/* SawMill/defs.scm 307 */
								obj_t BgL_tmpz00_13070;

								BgL_tmpz00_13070 = ((obj_t) BgL_pz00_482);
								bgl_display_string(BGl_string3090z00zzsaw_defsz00,
									BgL_tmpz00_13070);
							}
							BGl_dumpz00zzsaw_defsz00(BgL_destz00_481, BgL_pz00_482,
								(int) (0L));
							{	/* SawMill/defs.scm 309 */
								obj_t BgL_tmpz00_13075;

								BgL_tmpz00_13075 = ((obj_t) BgL_pz00_482);
								return
									bgl_display_string(BGl_string3089z00zzsaw_defsz00,
									BgL_tmpz00_13075);
							}
						}
					else
						{	/* SawMill/defs.scm 306 */
							return BFALSE;
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.scm 1 */
			{	/* SawMill/defs.scm 12 */
				obj_t BgL_arg1949z00_2142;
				obj_t BgL_arg1950z00_2143;

				{	/* SawMill/defs.scm 12 */
					obj_t BgL_v1690z00_2160;

					BgL_v1690z00_2160 = create_vector(7L);
					{	/* SawMill/defs.scm 12 */
						obj_t BgL_arg1956z00_2161;

						BgL_arg1956z00_2161 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc3092z00zzsaw_defsz00, BGl_proc3091z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1690z00_2160, 0L, BgL_arg1956z00_2161);
					}
					{	/* SawMill/defs.scm 12 */
						obj_t BgL_arg1961z00_2171;

						BgL_arg1961z00_2171 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc3094z00zzsaw_defsz00, BGl_proc3093z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1690z00_2160, 1L, BgL_arg1961z00_2171);
					}
					{	/* SawMill/defs.scm 12 */
						obj_t BgL_arg1966z00_2181;

						BgL_arg1966z00_2181 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc3097z00zzsaw_defsz00, BGl_proc3096z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc3095z00zzsaw_defsz00,
							CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1690z00_2160, 2L, BgL_arg1966z00_2181);
					}
					{	/* SawMill/defs.scm 12 */
						obj_t BgL_arg1973z00_2194;

						BgL_arg1973z00_2194 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(5),
							BGl_proc3100z00zzsaw_defsz00, BGl_proc3099z00zzsaw_defsz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BGl_proc3098z00zzsaw_defsz00,
							CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1690z00_2160, 3L, BgL_arg1973z00_2194);
					}
					{	/* SawMill/defs.scm 12 */
						obj_t BgL_arg1980z00_2208;

						BgL_arg1980z00_2208 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc3103z00zzsaw_defsz00, BGl_proc3102z00zzsaw_defsz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BGl_proc3101z00zzsaw_defsz00,
							CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1690z00_2160, 4L, BgL_arg1980z00_2208);
					}
					{	/* SawMill/defs.scm 12 */
						obj_t BgL_arg1987z00_2222;

						BgL_arg1987z00_2222 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc3106z00zzsaw_defsz00, BGl_proc3105z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc3104z00zzsaw_defsz00,
							CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1690z00_2160, 5L, BgL_arg1987z00_2222);
					}
					{	/* SawMill/defs.scm 12 */
						obj_t BgL_arg1994z00_2235;

						BgL_arg1994z00_2235 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc3109z00zzsaw_defsz00, BGl_proc3108z00zzsaw_defsz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BGl_proc3107z00zzsaw_defsz00,
							CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1690z00_2160, 6L, BgL_arg1994z00_2235);
					}
					BgL_arg1949z00_2142 = BgL_v1690z00_2160;
				}
				{	/* SawMill/defs.scm 12 */
					obj_t BgL_v1691z00_2248;

					BgL_v1691z00_2248 = create_vector(0L);
					BgL_arg1950z00_2143 = BgL_v1691z00_2248;
				}
				BGl_rtl_regz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(9),
					CNST_TABLE_REF(10), BGl_objectz00zz__objectz00, 23530L,
					BGl_proc3112z00zzsaw_defsz00, BGl_proc3111z00zzsaw_defsz00, BFALSE,
					BGl_proc3110z00zzsaw_defsz00, BFALSE, BgL_arg1949z00_2142,
					BgL_arg1950z00_2143);
			}
			{	/* SawMill/defs.scm 21 */
				obj_t BgL_arg2004z00_2255;
				obj_t BgL_arg2006z00_2256;

				{	/* SawMill/defs.scm 21 */
					obj_t BgL_v1692z00_2267;

					BgL_v1692z00_2267 = create_vector(1L);
					{	/* SawMill/defs.scm 21 */
						obj_t BgL_arg2012z00_2268;

						BgL_arg2012z00_2268 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(11),
							BGl_proc3115z00zzsaw_defsz00, BGl_proc3114z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc3113z00zzsaw_defsz00,
							CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1692z00_2267, 0L, BgL_arg2012z00_2268);
					}
					BgL_arg2004z00_2255 = BgL_v1692z00_2267;
				}
				{	/* SawMill/defs.scm 21 */
					obj_t BgL_v1693z00_2281;

					BgL_v1693z00_2281 = create_vector(0L);
					BgL_arg2006z00_2256 = BgL_v1693z00_2281;
				}
				BGl_rtl_funz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(12),
					CNST_TABLE_REF(10), BGl_objectz00zz__objectz00, 50121L,
					BGl_proc3118z00zzsaw_defsz00, BGl_proc3117z00zzsaw_defsz00, BFALSE,
					BGl_proc3116z00zzsaw_defsz00, BFALSE, BgL_arg2004z00_2255,
					BgL_arg2006z00_2256);
			}
			{	/* SawMill/defs.scm 23 */
				obj_t BgL_arg2022z00_2288;
				obj_t BgL_arg2024z00_2289;

				{	/* SawMill/defs.scm 23 */
					obj_t BgL_v1694z00_2300;

					BgL_v1694z00_2300 = create_vector(0L);
					BgL_arg2022z00_2288 = BgL_v1694z00_2300;
				}
				{	/* SawMill/defs.scm 23 */
					obj_t BgL_v1695z00_2301;

					BgL_v1695z00_2301 = create_vector(0L);
					BgL_arg2024z00_2289 = BgL_v1695z00_2301;
				}
				BGl_rtl_lastz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(13),
					CNST_TABLE_REF(10), BGl_rtl_funz00zzsaw_defsz00, 46348L,
					BGl_proc3121z00zzsaw_defsz00, BGl_proc3120z00zzsaw_defsz00, BFALSE,
					BGl_proc3119z00zzsaw_defsz00, BFALSE, BgL_arg2022z00_2288,
					BgL_arg2024z00_2289);
			}
			{	/* SawMill/defs.scm 24 */
				obj_t BgL_arg2036z00_2308;
				obj_t BgL_arg2037z00_2309;

				{	/* SawMill/defs.scm 24 */
					obj_t BgL_v1696z00_2321;

					BgL_v1696z00_2321 = create_vector(1L);
					{	/* SawMill/defs.scm 24 */
						obj_t BgL_arg2044z00_2322;

						BgL_arg2044z00_2322 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc3123z00zzsaw_defsz00, BGl_proc3122z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1696z00_2321, 0L, BgL_arg2044z00_2322);
					}
					BgL_arg2036z00_2308 = BgL_v1696z00_2321;
				}
				{	/* SawMill/defs.scm 24 */
					obj_t BgL_v1697z00_2332;

					BgL_v1697z00_2332 = create_vector(0L);
					BgL_arg2037z00_2309 = BgL_v1697z00_2332;
				}
				BGl_rtl_returnz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(14),
					CNST_TABLE_REF(10), BGl_rtl_lastz00zzsaw_defsz00, 2933L,
					BGl_proc3126z00zzsaw_defsz00, BGl_proc3125z00zzsaw_defsz00, BFALSE,
					BGl_proc3124z00zzsaw_defsz00, BFALSE, BgL_arg2036z00_2308,
					BgL_arg2037z00_2309);
			}
			{	/* SawMill/defs.scm 25 */
				obj_t BgL_arg2052z00_2339;
				obj_t BgL_arg2055z00_2340;

				{	/* SawMill/defs.scm 25 */
					obj_t BgL_v1698z00_2351;

					BgL_v1698z00_2351 = create_vector(0L);
					BgL_arg2052z00_2339 = BgL_v1698z00_2351;
				}
				{	/* SawMill/defs.scm 25 */
					obj_t BgL_v1699z00_2352;

					BgL_v1699z00_2352 = create_vector(0L);
					BgL_arg2055z00_2340 = BgL_v1699z00_2352;
				}
				BGl_rtl_jumpexitz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(15),
					CNST_TABLE_REF(10), BGl_rtl_lastz00zzsaw_defsz00, 2291L,
					BGl_proc3129z00zzsaw_defsz00, BGl_proc3128z00zzsaw_defsz00, BFALSE,
					BGl_proc3127z00zzsaw_defsz00, BFALSE, BgL_arg2052z00_2339,
					BgL_arg2055z00_2340);
			}
			{	/* SawMill/defs.scm 26 */
				obj_t BgL_arg2064z00_2359;
				obj_t BgL_arg2065z00_2360;

				{	/* SawMill/defs.scm 26 */
					obj_t BgL_v1700z00_2371;

					BgL_v1700z00_2371 = create_vector(0L);
					BgL_arg2064z00_2359 = BgL_v1700z00_2371;
				}
				{	/* SawMill/defs.scm 26 */
					obj_t BgL_v1701z00_2372;

					BgL_v1701z00_2372 = create_vector(0L);
					BgL_arg2065z00_2360 = BgL_v1701z00_2372;
				}
				BGl_rtl_failz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(16),
					CNST_TABLE_REF(10), BGl_rtl_lastz00zzsaw_defsz00, 45401L,
					BGl_proc3132z00zzsaw_defsz00, BGl_proc3131z00zzsaw_defsz00, BFALSE,
					BGl_proc3130z00zzsaw_defsz00, BFALSE, BgL_arg2064z00_2359,
					BgL_arg2065z00_2360);
			}
			{	/* SawMill/defs.scm 27 */
				obj_t BgL_arg2076z00_2379;
				obj_t BgL_arg2077z00_2380;

				{	/* SawMill/defs.scm 27 */
					obj_t BgL_v1702z00_2391;

					BgL_v1702z00_2391 = create_vector(0L);
					BgL_arg2076z00_2379 = BgL_v1702z00_2391;
				}
				{	/* SawMill/defs.scm 27 */
					obj_t BgL_v1703z00_2392;

					BgL_v1703z00_2392 = create_vector(0L);
					BgL_arg2077z00_2380 = BgL_v1703z00_2392;
				}
				BGl_rtl_retblockz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(17),
					CNST_TABLE_REF(10), BGl_rtl_funz00zzsaw_defsz00, 32430L,
					BGl_proc3135z00zzsaw_defsz00, BGl_proc3134z00zzsaw_defsz00, BFALSE,
					BGl_proc3133z00zzsaw_defsz00, BFALSE, BgL_arg2076z00_2379,
					BgL_arg2077z00_2380);
			}
			{	/* SawMill/defs.scm 28 */
				obj_t BgL_arg2087z00_2399;
				obj_t BgL_arg2088z00_2400;

				{	/* SawMill/defs.scm 28 */
					obj_t BgL_v1704z00_2412;

					BgL_v1704z00_2412 = create_vector(1L);
					{	/* SawMill/defs.scm 28 */
						obj_t BgL_arg2095z00_2413;

						BgL_arg2095z00_2413 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(18),
							BGl_proc3137z00zzsaw_defsz00, BGl_proc3136z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_blockz00zzsaw_defsz00);
						VECTOR_SET(BgL_v1704z00_2412, 0L, BgL_arg2095z00_2413);
					}
					BgL_arg2087z00_2399 = BgL_v1704z00_2412;
				}
				{	/* SawMill/defs.scm 28 */
					obj_t BgL_v1705z00_2423;

					BgL_v1705z00_2423 = create_vector(0L);
					BgL_arg2088z00_2400 = BgL_v1705z00_2423;
				}
				BGl_rtl_retz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(19),
					CNST_TABLE_REF(10), BGl_rtl_lastz00zzsaw_defsz00, 55560L,
					BGl_proc3140z00zzsaw_defsz00, BGl_proc3139z00zzsaw_defsz00, BFALSE,
					BGl_proc3138z00zzsaw_defsz00, BFALSE, BgL_arg2087z00_2399,
					BgL_arg2088z00_2400);
			}
			{	/* SawMill/defs.scm 30 */
				obj_t BgL_arg2103z00_2430;
				obj_t BgL_arg2104z00_2431;

				{	/* SawMill/defs.scm 30 */
					obj_t BgL_v1706z00_2442;

					BgL_v1706z00_2442 = create_vector(0L);
					BgL_arg2103z00_2430 = BgL_v1706z00_2442;
				}
				{	/* SawMill/defs.scm 30 */
					obj_t BgL_v1707z00_2443;

					BgL_v1707z00_2443 = create_vector(0L);
					BgL_arg2104z00_2431 = BgL_v1707z00_2443;
				}
				BGl_rtl_notseqz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(20),
					CNST_TABLE_REF(10), BGl_rtl_funz00zzsaw_defsz00, 57578L,
					BGl_proc3143z00zzsaw_defsz00, BGl_proc3142z00zzsaw_defsz00, BFALSE,
					BGl_proc3141z00zzsaw_defsz00, BFALSE, BgL_arg2103z00_2430,
					BgL_arg2104z00_2431);
			}
			{	/* SawMill/defs.scm 31 */
				obj_t BgL_arg2113z00_2450;
				obj_t BgL_arg2114z00_2451;

				{	/* SawMill/defs.scm 31 */
					obj_t BgL_v1708z00_2462;

					BgL_v1708z00_2462 = create_vector(0L);
					BgL_arg2113z00_2450 = BgL_v1708z00_2462;
				}
				{	/* SawMill/defs.scm 31 */
					obj_t BgL_v1709z00_2463;

					BgL_v1709z00_2463 = create_vector(0L);
					BgL_arg2114z00_2451 = BgL_v1709z00_2463;
				}
				BGl_rtl_ifz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(21),
					CNST_TABLE_REF(10), BGl_rtl_notseqz00zzsaw_defsz00, 31216L,
					BGl_proc3146z00zzsaw_defsz00, BGl_proc3145z00zzsaw_defsz00, BFALSE,
					BGl_proc3144z00zzsaw_defsz00, BFALSE, BgL_arg2113z00_2450,
					BgL_arg2114z00_2451);
			}
			{	/* SawMill/defs.scm 32 */
				obj_t BgL_arg2123z00_2470;
				obj_t BgL_arg2124z00_2471;

				{	/* SawMill/defs.scm 32 */
					obj_t BgL_v1710z00_2484;

					BgL_v1710z00_2484 = create_vector(2L);
					{	/* SawMill/defs.scm 32 */
						obj_t BgL_arg2131z00_2485;

						BgL_arg2131z00_2485 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc3148z00zzsaw_defsz00, BGl_proc3147z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1710z00_2484, 0L, BgL_arg2131z00_2485);
					}
					{	/* SawMill/defs.scm 32 */
						obj_t BgL_arg2136z00_2495;

						BgL_arg2136z00_2495 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(22),
							BGl_proc3150z00zzsaw_defsz00, BGl_proc3149z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1710z00_2484, 1L, BgL_arg2136z00_2495);
					}
					BgL_arg2123z00_2470 = BgL_v1710z00_2484;
				}
				{	/* SawMill/defs.scm 32 */
					obj_t BgL_v1711z00_2505;

					BgL_v1711z00_2505 = create_vector(0L);
					BgL_arg2124z00_2471 = BgL_v1711z00_2505;
				}
				BGl_rtl_selectz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(23),
					CNST_TABLE_REF(10), BGl_rtl_notseqz00zzsaw_defsz00, 6346L,
					BGl_proc3153z00zzsaw_defsz00, BGl_proc3152z00zzsaw_defsz00, BFALSE,
					BGl_proc3151z00zzsaw_defsz00, BFALSE, BgL_arg2123z00_2470,
					BgL_arg2124z00_2471);
			}
			{	/* SawMill/defs.scm 33 */
				obj_t BgL_arg2144z00_2512;
				obj_t BgL_arg2145z00_2513;

				{	/* SawMill/defs.scm 33 */
					obj_t BgL_v1712z00_2527;

					BgL_v1712z00_2527 = create_vector(1L);
					{	/* SawMill/defs.scm 33 */
						obj_t BgL_arg2151z00_2528;

						BgL_arg2151z00_2528 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(24),
							BGl_proc3155z00zzsaw_defsz00, BGl_proc3154z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1712z00_2527, 0L, BgL_arg2151z00_2528);
					}
					BgL_arg2144z00_2512 = BgL_v1712z00_2527;
				}
				{	/* SawMill/defs.scm 33 */
					obj_t BgL_v1713z00_2538;

					BgL_v1713z00_2538 = create_vector(0L);
					BgL_arg2145z00_2513 = BgL_v1713z00_2538;
				}
				BGl_rtl_switchz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(25),
					CNST_TABLE_REF(10), BGl_rtl_selectz00zzsaw_defsz00, 61548L,
					BGl_proc3158z00zzsaw_defsz00, BGl_proc3157z00zzsaw_defsz00, BFALSE,
					BGl_proc3156z00zzsaw_defsz00, BFALSE, BgL_arg2144z00_2512,
					BgL_arg2145z00_2513);
			}
			{	/* SawMill/defs.scm 34 */
				obj_t BgL_arg2160z00_2545;
				obj_t BgL_arg2161z00_2546;

				{	/* SawMill/defs.scm 34 */
					obj_t BgL_v1714z00_2558;

					BgL_v1714z00_2558 = create_vector(1L);
					{	/* SawMill/defs.scm 34 */
						obj_t BgL_arg2167z00_2559;

						BgL_arg2167z00_2559 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(26),
							BGl_proc3160z00zzsaw_defsz00, BGl_proc3159z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_blockz00zzsaw_defsz00);
						VECTOR_SET(BgL_v1714z00_2558, 0L, BgL_arg2167z00_2559);
					}
					BgL_arg2160z00_2545 = BgL_v1714z00_2558;
				}
				{	/* SawMill/defs.scm 34 */
					obj_t BgL_v1715z00_2569;

					BgL_v1715z00_2569 = create_vector(0L);
					BgL_arg2161z00_2546 = BgL_v1715z00_2569;
				}
				BGl_rtl_ifeqz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(27),
					CNST_TABLE_REF(10), BGl_rtl_notseqz00zzsaw_defsz00, 50772L,
					BGl_proc3163z00zzsaw_defsz00, BGl_proc3162z00zzsaw_defsz00, BFALSE,
					BGl_proc3161z00zzsaw_defsz00, BFALSE, BgL_arg2160z00_2545,
					BgL_arg2161z00_2546);
			}
			{	/* SawMill/defs.scm 35 */
				obj_t BgL_arg2175z00_2576;
				obj_t BgL_arg2176z00_2577;

				{	/* SawMill/defs.scm 35 */
					obj_t BgL_v1716z00_2589;

					BgL_v1716z00_2589 = create_vector(1L);
					{	/* SawMill/defs.scm 35 */
						obj_t BgL_arg2182z00_2590;

						BgL_arg2182z00_2590 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(26),
							BGl_proc3165z00zzsaw_defsz00, BGl_proc3164z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_blockz00zzsaw_defsz00);
						VECTOR_SET(BgL_v1716z00_2589, 0L, BgL_arg2182z00_2590);
					}
					BgL_arg2175z00_2576 = BgL_v1716z00_2589;
				}
				{	/* SawMill/defs.scm 35 */
					obj_t BgL_v1717z00_2600;

					BgL_v1717z00_2600 = create_vector(0L);
					BgL_arg2176z00_2577 = BgL_v1717z00_2600;
				}
				BGl_rtl_ifnez00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(28),
					CNST_TABLE_REF(10), BGl_rtl_notseqz00zzsaw_defsz00, 43703L,
					BGl_proc3168z00zzsaw_defsz00, BGl_proc3167z00zzsaw_defsz00, BFALSE,
					BGl_proc3166z00zzsaw_defsz00, BFALSE, BgL_arg2175z00_2576,
					BgL_arg2176z00_2577);
			}
			{	/* SawMill/defs.scm 36 */
				obj_t BgL_arg2190z00_2607;
				obj_t BgL_arg2191z00_2608;

				{	/* SawMill/defs.scm 36 */
					obj_t BgL_v1718z00_2620;

					BgL_v1718z00_2620 = create_vector(1L);
					{	/* SawMill/defs.scm 36 */
						obj_t BgL_arg2198z00_2621;

						BgL_arg2198z00_2621 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(18),
							BGl_proc3170z00zzsaw_defsz00, BGl_proc3169z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_blockz00zzsaw_defsz00);
						VECTOR_SET(BgL_v1718z00_2620, 0L, BgL_arg2198z00_2621);
					}
					BgL_arg2190z00_2607 = BgL_v1718z00_2620;
				}
				{	/* SawMill/defs.scm 36 */
					obj_t BgL_v1719z00_2631;

					BgL_v1719z00_2631 = create_vector(0L);
					BgL_arg2191z00_2608 = BgL_v1719z00_2631;
				}
				BGl_rtl_goz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(29),
					CNST_TABLE_REF(10), BGl_rtl_notseqz00zzsaw_defsz00, 55455L,
					BGl_proc3173z00zzsaw_defsz00, BGl_proc3172z00zzsaw_defsz00, BFALSE,
					BGl_proc3171z00zzsaw_defsz00, BFALSE, BgL_arg2190z00_2607,
					BgL_arg2191z00_2608);
			}
			{	/* SawMill/defs.scm 38 */
				obj_t BgL_arg2206z00_2638;
				obj_t BgL_arg2207z00_2639;

				{	/* SawMill/defs.scm 38 */
					obj_t BgL_v1720z00_2650;

					BgL_v1720z00_2650 = create_vector(0L);
					BgL_arg2206z00_2638 = BgL_v1720z00_2650;
				}
				{	/* SawMill/defs.scm 38 */
					obj_t BgL_v1721z00_2651;

					BgL_v1721z00_2651 = create_vector(0L);
					BgL_arg2207z00_2639 = BgL_v1721z00_2651;
				}
				BGl_rtl_purez00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(30),
					CNST_TABLE_REF(10), BGl_rtl_funz00zzsaw_defsz00, 28820L,
					BGl_proc3176z00zzsaw_defsz00, BGl_proc3175z00zzsaw_defsz00, BFALSE,
					BGl_proc3174z00zzsaw_defsz00, BFALSE, BgL_arg2206z00_2638,
					BgL_arg2207z00_2639);
			}
			{	/* SawMill/defs.scm 39 */
				obj_t BgL_arg2216z00_2658;
				obj_t BgL_arg2217z00_2659;

				{	/* SawMill/defs.scm 39 */
					obj_t BgL_v1722z00_2670;

					BgL_v1722z00_2670 = create_vector(0L);
					BgL_arg2216z00_2658 = BgL_v1722z00_2670;
				}
				{	/* SawMill/defs.scm 39 */
					obj_t BgL_v1723z00_2671;

					BgL_v1723z00_2671 = create_vector(0L);
					BgL_arg2217z00_2659 = BgL_v1723z00_2671;
				}
				BGl_rtl_nopz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(31),
					CNST_TABLE_REF(10), BGl_rtl_purez00zzsaw_defsz00, 27632L,
					BGl_proc3179z00zzsaw_defsz00, BGl_proc3178z00zzsaw_defsz00, BFALSE,
					BGl_proc3177z00zzsaw_defsz00, BFALSE, BgL_arg2216z00_2658,
					BgL_arg2217z00_2659);
			}
			{	/* SawMill/defs.scm 40 */
				obj_t BgL_arg2226z00_2678;
				obj_t BgL_arg2227z00_2679;

				{	/* SawMill/defs.scm 40 */
					obj_t BgL_v1724z00_2690;

					BgL_v1724z00_2690 = create_vector(0L);
					BgL_arg2226z00_2678 = BgL_v1724z00_2690;
				}
				{	/* SawMill/defs.scm 40 */
					obj_t BgL_v1725z00_2691;

					BgL_v1725z00_2691 = create_vector(0L);
					BgL_arg2227z00_2679 = BgL_v1725z00_2691;
				}
				BGl_rtl_movz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(32),
					CNST_TABLE_REF(10), BGl_rtl_purez00zzsaw_defsz00, 12799L,
					BGl_proc3182z00zzsaw_defsz00, BGl_proc3181z00zzsaw_defsz00, BFALSE,
					BGl_proc3180z00zzsaw_defsz00, BFALSE, BgL_arg2226z00_2678,
					BgL_arg2227z00_2679);
			}
			{	/* SawMill/defs.scm 41 */
				obj_t BgL_arg2236z00_2698;
				obj_t BgL_arg2237z00_2699;

				{	/* SawMill/defs.scm 41 */
					obj_t BgL_v1726z00_2711;

					BgL_v1726z00_2711 = create_vector(1L);
					{	/* SawMill/defs.scm 41 */
						obj_t BgL_arg2243z00_2712;

						BgL_arg2243z00_2712 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(33),
							BGl_proc3184z00zzsaw_defsz00, BGl_proc3183z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_atomz00zzast_nodez00);
						VECTOR_SET(BgL_v1726z00_2711, 0L, BgL_arg2243z00_2712);
					}
					BgL_arg2236z00_2698 = BgL_v1726z00_2711;
				}
				{	/* SawMill/defs.scm 41 */
					obj_t BgL_v1727z00_2722;

					BgL_v1727z00_2722 = create_vector(0L);
					BgL_arg2237z00_2699 = BgL_v1727z00_2722;
				}
				BGl_rtl_loadiz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(34),
					CNST_TABLE_REF(10), BGl_rtl_purez00zzsaw_defsz00, 16623L,
					BGl_proc3187z00zzsaw_defsz00, BGl_proc3186z00zzsaw_defsz00, BFALSE,
					BGl_proc3185z00zzsaw_defsz00, BFALSE, BgL_arg2236z00_2698,
					BgL_arg2237z00_2699);
			}
			{	/* SawMill/defs.scm 42 */
				obj_t BgL_arg2251z00_2729;
				obj_t BgL_arg2252z00_2730;

				{	/* SawMill/defs.scm 42 */
					obj_t BgL_v1728z00_2742;

					BgL_v1728z00_2742 = create_vector(1L);
					{	/* SawMill/defs.scm 42 */
						obj_t BgL_arg2258z00_2743;

						BgL_arg2258z00_2743 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc3189z00zzsaw_defsz00, BGl_proc3188z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_globalz00zzast_varz00);
						VECTOR_SET(BgL_v1728z00_2742, 0L, BgL_arg2258z00_2743);
					}
					BgL_arg2251z00_2729 = BgL_v1728z00_2742;
				}
				{	/* SawMill/defs.scm 42 */
					obj_t BgL_v1729z00_2753;

					BgL_v1729z00_2753 = create_vector(0L);
					BgL_arg2252z00_2730 = BgL_v1729z00_2753;
				}
				BGl_rtl_loadgz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(35),
					CNST_TABLE_REF(10), BGl_rtl_purez00zzsaw_defsz00, 34672L,
					BGl_proc3192z00zzsaw_defsz00, BGl_proc3191z00zzsaw_defsz00, BFALSE,
					BGl_proc3190z00zzsaw_defsz00, BFALSE, BgL_arg2251z00_2729,
					BgL_arg2252z00_2730);
			}
			{	/* SawMill/defs.scm 43 */
				obj_t BgL_arg2266z00_2760;
				obj_t BgL_arg2267z00_2761;

				{	/* SawMill/defs.scm 43 */
					obj_t BgL_v1730z00_2773;

					BgL_v1730z00_2773 = create_vector(1L);
					{	/* SawMill/defs.scm 43 */
						obj_t BgL_arg2273z00_2774;

						BgL_arg2273z00_2774 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc3194z00zzsaw_defsz00, BGl_proc3193z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_globalz00zzast_varz00);
						VECTOR_SET(BgL_v1730z00_2773, 0L, BgL_arg2273z00_2774);
					}
					BgL_arg2266z00_2760 = BgL_v1730z00_2773;
				}
				{	/* SawMill/defs.scm 43 */
					obj_t BgL_v1731z00_2784;

					BgL_v1731z00_2784 = create_vector(0L);
					BgL_arg2267z00_2761 = BgL_v1731z00_2784;
				}
				BGl_rtl_loadfunz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(36),
					CNST_TABLE_REF(10), BGl_rtl_purez00zzsaw_defsz00, 13326L,
					BGl_proc3197z00zzsaw_defsz00, BGl_proc3196z00zzsaw_defsz00, BFALSE,
					BGl_proc3195z00zzsaw_defsz00, BFALSE, BgL_arg2266z00_2760,
					BgL_arg2267z00_2761);
			}
			{	/* SawMill/defs.scm 44 */
				obj_t BgL_arg2282z00_2791;
				obj_t BgL_arg2283z00_2792;

				{	/* SawMill/defs.scm 44 */
					obj_t BgL_v1732z00_2804;

					BgL_v1732z00_2804 = create_vector(1L);
					{	/* SawMill/defs.scm 44 */
						obj_t BgL_arg2290z00_2805;

						BgL_arg2290z00_2805 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc3199z00zzsaw_defsz00, BGl_proc3198z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_globalz00zzast_varz00);
						VECTOR_SET(BgL_v1732z00_2804, 0L, BgL_arg2290z00_2805);
					}
					BgL_arg2282z00_2791 = BgL_v1732z00_2804;
				}
				{	/* SawMill/defs.scm 44 */
					obj_t BgL_v1733z00_2815;

					BgL_v1733z00_2815 = create_vector(0L);
					BgL_arg2283z00_2792 = BgL_v1733z00_2815;
				}
				BGl_rtl_globalrefz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(37),
					CNST_TABLE_REF(10), BGl_rtl_purez00zzsaw_defsz00, 23593L,
					BGl_proc3202z00zzsaw_defsz00, BGl_proc3201z00zzsaw_defsz00, BFALSE,
					BGl_proc3200z00zzsaw_defsz00, BFALSE, BgL_arg2282z00_2791,
					BgL_arg2283z00_2792);
			}
			{	/* SawMill/defs.scm 45 */
				obj_t BgL_arg2298z00_2822;
				obj_t BgL_arg2299z00_2823;

				{	/* SawMill/defs.scm 45 */
					obj_t BgL_v1734z00_2837;

					BgL_v1734z00_2837 = create_vector(3L);
					{	/* SawMill/defs.scm 45 */
						obj_t BgL_arg2306z00_2838;

						BgL_arg2306z00_2838 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(5),
							BGl_proc3204z00zzsaw_defsz00, BGl_proc3203z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(38));
						VECTOR_SET(BgL_v1734z00_2837, 0L, BgL_arg2306z00_2838);
					}
					{	/* SawMill/defs.scm 45 */
						obj_t BgL_arg2311z00_2848;

						BgL_arg2311z00_2848 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(39),
							BGl_proc3206z00zzsaw_defsz00, BGl_proc3205z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1734z00_2837, 1L, BgL_arg2311z00_2848);
					}
					{	/* SawMill/defs.scm 45 */
						obj_t BgL_arg2316z00_2858;

						BgL_arg2316z00_2858 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc3208z00zzsaw_defsz00, BGl_proc3207z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1734z00_2837, 2L, BgL_arg2316z00_2858);
					}
					BgL_arg2298z00_2822 = BgL_v1734z00_2837;
				}
				{	/* SawMill/defs.scm 45 */
					obj_t BgL_v1735z00_2868;

					BgL_v1735z00_2868 = create_vector(0L);
					BgL_arg2299z00_2823 = BgL_v1735z00_2868;
				}
				BGl_rtl_getfieldz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(40),
					CNST_TABLE_REF(10), BGl_rtl_purez00zzsaw_defsz00, 51124L,
					BGl_proc3211z00zzsaw_defsz00, BGl_proc3210z00zzsaw_defsz00, BFALSE,
					BGl_proc3209z00zzsaw_defsz00, BFALSE, BgL_arg2298z00_2822,
					BgL_arg2299z00_2823);
			}
			{	/* SawMill/defs.scm 46 */
				obj_t BgL_arg2325z00_2875;
				obj_t BgL_arg2326z00_2876;

				{	/* SawMill/defs.scm 46 */
					obj_t BgL_v1736z00_2889;

					BgL_v1736z00_2889 = create_vector(2L);
					{	/* SawMill/defs.scm 46 */
						obj_t BgL_arg2333z00_2890;

						BgL_arg2333z00_2890 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc3213z00zzsaw_defsz00, BGl_proc3212z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1736z00_2889, 0L, BgL_arg2333z00_2890);
					}
					{	/* SawMill/defs.scm 46 */
						obj_t BgL_arg2339z00_2900;

						BgL_arg2339z00_2900 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(41),
							BGl_proc3215z00zzsaw_defsz00, BGl_proc3214z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1736z00_2889, 1L, BgL_arg2339z00_2900);
					}
					BgL_arg2325z00_2875 = BgL_v1736z00_2889;
				}
				{	/* SawMill/defs.scm 46 */
					obj_t BgL_v1737z00_2910;

					BgL_v1737z00_2910 = create_vector(0L);
					BgL_arg2326z00_2876 = BgL_v1737z00_2910;
				}
				BGl_rtl_vallocz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(42),
					CNST_TABLE_REF(10), BGl_rtl_purez00zzsaw_defsz00, 58150L,
					BGl_proc3218z00zzsaw_defsz00, BGl_proc3217z00zzsaw_defsz00, BFALSE,
					BGl_proc3216z00zzsaw_defsz00, BFALSE, BgL_arg2325z00_2875,
					BgL_arg2326z00_2876);
			}
			{	/* SawMill/defs.scm 47 */
				obj_t BgL_arg2349z00_2917;
				obj_t BgL_arg2350z00_2918;

				{	/* SawMill/defs.scm 47 */
					obj_t BgL_v1738z00_2931;

					BgL_v1738z00_2931 = create_vector(2L);
					{	/* SawMill/defs.scm 47 */
						obj_t BgL_arg2356z00_2932;

						BgL_arg2356z00_2932 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc3220z00zzsaw_defsz00, BGl_proc3219z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1738z00_2931, 0L, BgL_arg2356z00_2932);
					}
					{	/* SawMill/defs.scm 47 */
						obj_t BgL_arg2361z00_2942;

						BgL_arg2361z00_2942 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(41),
							BGl_proc3222z00zzsaw_defsz00, BGl_proc3221z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1738z00_2931, 1L, BgL_arg2361z00_2942);
					}
					BgL_arg2349z00_2917 = BgL_v1738z00_2931;
				}
				{	/* SawMill/defs.scm 47 */
					obj_t BgL_v1739z00_2952;

					BgL_v1739z00_2952 = create_vector(0L);
					BgL_arg2350z00_2918 = BgL_v1739z00_2952;
				}
				BGl_rtl_vrefz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(43),
					CNST_TABLE_REF(10), BGl_rtl_purez00zzsaw_defsz00, 12596L,
					BGl_proc3225z00zzsaw_defsz00, BGl_proc3224z00zzsaw_defsz00, BFALSE,
					BGl_proc3223z00zzsaw_defsz00, BFALSE, BgL_arg2349z00_2917,
					BgL_arg2350z00_2918);
			}
			{	/* SawMill/defs.scm 48 */
				obj_t BgL_arg2370z00_2959;
				obj_t BgL_arg2371z00_2960;

				{	/* SawMill/defs.scm 48 */
					obj_t BgL_v1740z00_2973;

					BgL_v1740z00_2973 = create_vector(2L);
					{	/* SawMill/defs.scm 48 */
						obj_t BgL_arg2377z00_2974;

						BgL_arg2377z00_2974 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc3227z00zzsaw_defsz00, BGl_proc3226z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1740z00_2973, 0L, BgL_arg2377z00_2974);
					}
					{	/* SawMill/defs.scm 48 */
						obj_t BgL_arg2382z00_2984;

						BgL_arg2382z00_2984 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(41),
							BGl_proc3229z00zzsaw_defsz00, BGl_proc3228z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1740z00_2973, 1L, BgL_arg2382z00_2984);
					}
					BgL_arg2370z00_2959 = BgL_v1740z00_2973;
				}
				{	/* SawMill/defs.scm 48 */
					obj_t BgL_v1741z00_2994;

					BgL_v1741z00_2994 = create_vector(0L);
					BgL_arg2371z00_2960 = BgL_v1741z00_2994;
				}
				BGl_rtl_vlengthz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(44),
					CNST_TABLE_REF(10), BGl_rtl_purez00zzsaw_defsz00, 48015L,
					BGl_proc3232z00zzsaw_defsz00, BGl_proc3231z00zzsaw_defsz00, BFALSE,
					BGl_proc3230z00zzsaw_defsz00, BFALSE, BgL_arg2370z00_2959,
					BgL_arg2371z00_2960);
			}
			{	/* SawMill/defs.scm 49 */
				obj_t BgL_arg2390z00_3001;
				obj_t BgL_arg2391z00_3002;

				{	/* SawMill/defs.scm 49 */
					obj_t BgL_v1742z00_3014;

					BgL_v1742z00_3014 = create_vector(1L);
					{	/* SawMill/defs.scm 49 */
						obj_t BgL_arg2397z00_3015;

						BgL_arg2397z00_3015 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc3234z00zzsaw_defsz00, BGl_proc3233z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1742z00_3014, 0L, BgL_arg2397z00_3015);
					}
					BgL_arg2390z00_3001 = BgL_v1742z00_3014;
				}
				{	/* SawMill/defs.scm 49 */
					obj_t BgL_v1743z00_3025;

					BgL_v1743z00_3025 = create_vector(0L);
					BgL_arg2391z00_3002 = BgL_v1743z00_3025;
				}
				BGl_rtl_instanceofz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(45),
					CNST_TABLE_REF(10), BGl_rtl_purez00zzsaw_defsz00, 47751L,
					BGl_proc3237z00zzsaw_defsz00, BGl_proc3236z00zzsaw_defsz00, BFALSE,
					BGl_proc3235z00zzsaw_defsz00, BFALSE, BgL_arg2390z00_3001,
					BgL_arg2391z00_3002);
			}
			{	/* SawMill/defs.scm 50 */
				obj_t BgL_arg2405z00_3032;
				obj_t BgL_arg2407z00_3033;

				{	/* SawMill/defs.scm 50 */
					obj_t BgL_v1744z00_3044;

					BgL_v1744z00_3044 = create_vector(0L);
					BgL_arg2405z00_3032 = BgL_v1744z00_3044;
				}
				{	/* SawMill/defs.scm 50 */
					obj_t BgL_v1745z00_3045;

					BgL_v1745z00_3045 = create_vector(0L);
					BgL_arg2407z00_3033 = BgL_v1745z00_3045;
				}
				BGl_rtl_makeboxz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(46),
					CNST_TABLE_REF(10), BGl_rtl_purez00zzsaw_defsz00, 40746L,
					BGl_proc3240z00zzsaw_defsz00, BGl_proc3239z00zzsaw_defsz00, BFALSE,
					BGl_proc3238z00zzsaw_defsz00, BFALSE, BgL_arg2405z00_3032,
					BgL_arg2407z00_3033);
			}
			{	/* SawMill/defs.scm 51 */
				obj_t BgL_arg2418z00_3052;
				obj_t BgL_arg2419z00_3053;

				{	/* SawMill/defs.scm 51 */
					obj_t BgL_v1746z00_3064;

					BgL_v1746z00_3064 = create_vector(0L);
					BgL_arg2418z00_3052 = BgL_v1746z00_3064;
				}
				{	/* SawMill/defs.scm 51 */
					obj_t BgL_v1747z00_3065;

					BgL_v1747z00_3065 = create_vector(0L);
					BgL_arg2419z00_3053 = BgL_v1747z00_3065;
				}
				BGl_rtl_boxrefz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(47),
					CNST_TABLE_REF(10), BGl_rtl_purez00zzsaw_defsz00, 52683L,
					BGl_proc3243z00zzsaw_defsz00, BGl_proc3242z00zzsaw_defsz00, BFALSE,
					BGl_proc3241z00zzsaw_defsz00, BFALSE, BgL_arg2418z00_3052,
					BgL_arg2419z00_3053);
			}
			{	/* SawMill/defs.scm 53 */
				obj_t BgL_arg2429z00_3072;
				obj_t BgL_arg2430z00_3073;

				{	/* SawMill/defs.scm 53 */
					obj_t BgL_v1748z00_3084;

					BgL_v1748z00_3084 = create_vector(0L);
					BgL_arg2429z00_3072 = BgL_v1748z00_3084;
				}
				{	/* SawMill/defs.scm 53 */
					obj_t BgL_v1749z00_3085;

					BgL_v1749z00_3085 = create_vector(0L);
					BgL_arg2430z00_3073 = BgL_v1749z00_3085;
				}
				BGl_rtl_effectz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(48),
					CNST_TABLE_REF(10), BGl_rtl_funz00zzsaw_defsz00, 9927L,
					BGl_proc3246z00zzsaw_defsz00, BGl_proc3245z00zzsaw_defsz00, BFALSE,
					BGl_proc3244z00zzsaw_defsz00, BFALSE, BgL_arg2429z00_3072,
					BgL_arg2430z00_3073);
			}
			{	/* SawMill/defs.scm 54 */
				obj_t BgL_arg2442z00_3092;
				obj_t BgL_arg2443z00_3093;

				{	/* SawMill/defs.scm 54 */
					obj_t BgL_v1750z00_3105;

					BgL_v1750z00_3105 = create_vector(1L);
					{	/* SawMill/defs.scm 54 */
						obj_t BgL_arg2449z00_3106;

						BgL_arg2449z00_3106 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc3248z00zzsaw_defsz00, BGl_proc3247z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_globalz00zzast_varz00);
						VECTOR_SET(BgL_v1750z00_3105, 0L, BgL_arg2449z00_3106);
					}
					BgL_arg2442z00_3092 = BgL_v1750z00_3105;
				}
				{	/* SawMill/defs.scm 54 */
					obj_t BgL_v1751z00_3116;

					BgL_v1751z00_3116 = create_vector(0L);
					BgL_arg2443z00_3093 = BgL_v1751z00_3116;
				}
				BGl_rtl_storegz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(49),
					CNST_TABLE_REF(10), BGl_rtl_effectz00zzsaw_defsz00, 10162L,
					BGl_proc3251z00zzsaw_defsz00, BGl_proc3250z00zzsaw_defsz00, BFALSE,
					BGl_proc3249z00zzsaw_defsz00, BFALSE, BgL_arg2442z00_3092,
					BgL_arg2443z00_3093);
			}
			{	/* SawMill/defs.scm 55 */
				obj_t BgL_arg2458z00_3123;
				obj_t BgL_arg2459z00_3124;

				{	/* SawMill/defs.scm 55 */
					obj_t BgL_v1752z00_3138;

					BgL_v1752z00_3138 = create_vector(3L);
					{	/* SawMill/defs.scm 55 */
						obj_t BgL_arg2465z00_3139;

						BgL_arg2465z00_3139 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(5),
							BGl_proc3253z00zzsaw_defsz00, BGl_proc3252z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(38));
						VECTOR_SET(BgL_v1752z00_3138, 0L, BgL_arg2465z00_3139);
					}
					{	/* SawMill/defs.scm 55 */
						obj_t BgL_arg2473z00_3149;

						BgL_arg2473z00_3149 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(39),
							BGl_proc3255z00zzsaw_defsz00, BGl_proc3254z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1752z00_3138, 1L, BgL_arg2473z00_3149);
					}
					{	/* SawMill/defs.scm 55 */
						obj_t BgL_arg2479z00_3159;

						BgL_arg2479z00_3159 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc3257z00zzsaw_defsz00, BGl_proc3256z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1752z00_3138, 2L, BgL_arg2479z00_3159);
					}
					BgL_arg2458z00_3123 = BgL_v1752z00_3138;
				}
				{	/* SawMill/defs.scm 55 */
					obj_t BgL_v1753z00_3169;

					BgL_v1753z00_3169 = create_vector(0L);
					BgL_arg2459z00_3124 = BgL_v1753z00_3169;
				}
				BGl_rtl_setfieldz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(50),
					CNST_TABLE_REF(10), BGl_rtl_effectz00zzsaw_defsz00, 44593L,
					BGl_proc3260z00zzsaw_defsz00, BGl_proc3259z00zzsaw_defsz00, BFALSE,
					BGl_proc3258z00zzsaw_defsz00, BFALSE, BgL_arg2458z00_3123,
					BgL_arg2459z00_3124);
			}
			{	/* SawMill/defs.scm 56 */
				obj_t BgL_arg2488z00_3176;
				obj_t BgL_arg2490z00_3177;

				{	/* SawMill/defs.scm 56 */
					obj_t BgL_v1754z00_3190;

					BgL_v1754z00_3190 = create_vector(2L);
					{	/* SawMill/defs.scm 56 */
						obj_t BgL_arg2497z00_3191;

						BgL_arg2497z00_3191 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc3262z00zzsaw_defsz00, BGl_proc3261z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1754z00_3190, 0L, BgL_arg2497z00_3191);
					}
					{	/* SawMill/defs.scm 56 */
						obj_t BgL_arg2505z00_3201;

						BgL_arg2505z00_3201 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(41),
							BGl_proc3264z00zzsaw_defsz00, BGl_proc3263z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1754z00_3190, 1L, BgL_arg2505z00_3201);
					}
					BgL_arg2488z00_3176 = BgL_v1754z00_3190;
				}
				{	/* SawMill/defs.scm 56 */
					obj_t BgL_v1755z00_3211;

					BgL_v1755z00_3211 = create_vector(0L);
					BgL_arg2490z00_3177 = BgL_v1755z00_3211;
				}
				BGl_rtl_vsetz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(51),
					CNST_TABLE_REF(10), BGl_rtl_effectz00zzsaw_defsz00, 41652L,
					BGl_proc3267z00zzsaw_defsz00, BGl_proc3266z00zzsaw_defsz00, BFALSE,
					BGl_proc3265z00zzsaw_defsz00, BFALSE, BgL_arg2488z00_3176,
					BgL_arg2490z00_3177);
			}
			{	/* SawMill/defs.scm 57 */
				obj_t BgL_arg2514z00_3218;
				obj_t BgL_arg2515z00_3219;

				{	/* SawMill/defs.scm 57 */
					obj_t BgL_v1756z00_3230;

					BgL_v1756z00_3230 = create_vector(0L);
					BgL_arg2514z00_3218 = BgL_v1756z00_3230;
				}
				{	/* SawMill/defs.scm 57 */
					obj_t BgL_v1757z00_3231;

					BgL_v1757z00_3231 = create_vector(0L);
					BgL_arg2515z00_3219 = BgL_v1757z00_3231;
				}
				BGl_rtl_boxsetz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(52),
					CNST_TABLE_REF(10), BGl_rtl_effectz00zzsaw_defsz00, 28043L,
					BGl_proc3270z00zzsaw_defsz00, BGl_proc3269z00zzsaw_defsz00, BFALSE,
					BGl_proc3268z00zzsaw_defsz00, BFALSE, BgL_arg2514z00_3218,
					BgL_arg2515z00_3219);
			}
			{	/* SawMill/defs.scm 59 */
				obj_t BgL_arg2527z00_3238;
				obj_t BgL_arg2528z00_3239;

				{	/* SawMill/defs.scm 59 */
					obj_t BgL_v1758z00_3252;

					BgL_v1758z00_3252 = create_vector(2L);
					{	/* SawMill/defs.scm 59 */
						obj_t BgL_arg2538z00_3253;

						BgL_arg2538z00_3253 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc3272z00zzsaw_defsz00, BGl_proc3271z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1758z00_3252, 0L, BgL_arg2538z00_3253);
					}
					{	/* SawMill/defs.scm 59 */
						obj_t BgL_arg2543z00_3263;

						BgL_arg2543z00_3263 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(53),
							BGl_proc3274z00zzsaw_defsz00, BGl_proc3273z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(54));
						VECTOR_SET(BgL_v1758z00_3252, 1L, BgL_arg2543z00_3263);
					}
					BgL_arg2527z00_3238 = BgL_v1758z00_3252;
				}
				{	/* SawMill/defs.scm 59 */
					obj_t BgL_v1759z00_3273;

					BgL_v1759z00_3273 = create_vector(0L);
					BgL_arg2528z00_3239 = BgL_v1759z00_3273;
				}
				BGl_rtl_newz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(55),
					CNST_TABLE_REF(10), BGl_rtl_funz00zzsaw_defsz00, 55659L,
					BGl_proc3277z00zzsaw_defsz00, BGl_proc3276z00zzsaw_defsz00, BFALSE,
					BGl_proc3275z00zzsaw_defsz00, BFALSE, BgL_arg2527z00_3238,
					BgL_arg2528z00_3239);
			}
			{	/* SawMill/defs.scm 60 */
				obj_t BgL_arg2553z00_3280;
				obj_t BgL_arg2554z00_3281;

				{	/* SawMill/defs.scm 60 */
					obj_t BgL_v1760z00_3293;

					BgL_v1760z00_3293 = create_vector(1L);
					{	/* SawMill/defs.scm 60 */
						obj_t BgL_arg2562z00_3294;

						BgL_arg2562z00_3294 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc3279z00zzsaw_defsz00, BGl_proc3278z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_globalz00zzast_varz00);
						VECTOR_SET(BgL_v1760z00_3293, 0L, BgL_arg2562z00_3294);
					}
					BgL_arg2553z00_3280 = BgL_v1760z00_3293;
				}
				{	/* SawMill/defs.scm 60 */
					obj_t BgL_v1761z00_3304;

					BgL_v1761z00_3304 = create_vector(0L);
					BgL_arg2554z00_3281 = BgL_v1761z00_3304;
				}
				BGl_rtl_callz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(56),
					CNST_TABLE_REF(10), BGl_rtl_funz00zzsaw_defsz00, 39022L,
					BGl_proc3282z00zzsaw_defsz00, BGl_proc3281z00zzsaw_defsz00, BFALSE,
					BGl_proc3280z00zzsaw_defsz00, BFALSE, BgL_arg2553z00_3280,
					BgL_arg2554z00_3281);
			}
			{	/* SawMill/defs.scm 61 */
				obj_t BgL_arg2578z00_3311;
				obj_t BgL_arg2579z00_3312;

				{	/* SawMill/defs.scm 61 */
					obj_t BgL_v1762z00_3323;

					BgL_v1762z00_3323 = create_vector(0L);
					BgL_arg2578z00_3311 = BgL_v1762z00_3323;
				}
				{	/* SawMill/defs.scm 61 */
					obj_t BgL_v1763z00_3324;

					BgL_v1763z00_3324 = create_vector(0L);
					BgL_arg2579z00_3312 = BgL_v1763z00_3324;
				}
				BGl_rtl_applyz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(57),
					CNST_TABLE_REF(10), BGl_rtl_funz00zzsaw_defsz00, 20926L,
					BGl_proc3285z00zzsaw_defsz00, BGl_proc3284z00zzsaw_defsz00, BFALSE,
					BGl_proc3283z00zzsaw_defsz00, BFALSE, BgL_arg2578z00_3311,
					BgL_arg2579z00_3312);
			}
			{	/* SawMill/defs.scm 62 */
				obj_t BgL_arg2592z00_3331;
				obj_t BgL_arg2594z00_3332;

				{	/* SawMill/defs.scm 62 */
					obj_t BgL_v1764z00_3346;

					BgL_v1764z00_3346 = create_vector(3L);
					{	/* SawMill/defs.scm 62 */
						obj_t BgL_arg2601z00_3347;

						BgL_arg2601z00_3347 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(5),
							BGl_proc3287z00zzsaw_defsz00, BGl_proc3286z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(58));
						VECTOR_SET(BgL_v1764z00_3346, 0L, BgL_arg2601z00_3347);
					}
					{	/* SawMill/defs.scm 62 */
						obj_t BgL_arg2608z00_3357;

						BgL_arg2608z00_3357 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(59),
							BGl_proc3289z00zzsaw_defsz00, BGl_proc3288z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(54));
						VECTOR_SET(BgL_v1764z00_3346, 1L, BgL_arg2608z00_3357);
					}
					{	/* SawMill/defs.scm 62 */
						obj_t BgL_arg2613z00_3367;

						BgL_arg2613z00_3367 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(60),
							BGl_proc3291z00zzsaw_defsz00, BGl_proc3290z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1764z00_3346, 2L, BgL_arg2613z00_3367);
					}
					BgL_arg2592z00_3331 = BgL_v1764z00_3346;
				}
				{	/* SawMill/defs.scm 62 */
					obj_t BgL_v1765z00_3377;

					BgL_v1765z00_3377 = create_vector(0L);
					BgL_arg2594z00_3332 = BgL_v1765z00_3377;
				}
				BGl_rtl_lightfuncallz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(61),
					CNST_TABLE_REF(10), BGl_rtl_funz00zzsaw_defsz00, 38185L,
					BGl_proc3294z00zzsaw_defsz00, BGl_proc3293z00zzsaw_defsz00, BFALSE,
					BGl_proc3292z00zzsaw_defsz00, BFALSE, BgL_arg2592z00_3331,
					BgL_arg2594z00_3332);
			}
			{	/* SawMill/defs.scm 63 */
				obj_t BgL_arg2621z00_3384;
				obj_t BgL_arg2622z00_3385;

				{	/* SawMill/defs.scm 63 */
					obj_t BgL_v1766z00_3396;

					BgL_v1766z00_3396 = create_vector(0L);
					BgL_arg2621z00_3384 = BgL_v1766z00_3396;
				}
				{	/* SawMill/defs.scm 63 */
					obj_t BgL_v1767z00_3397;

					BgL_v1767z00_3397 = create_vector(0L);
					BgL_arg2622z00_3385 = BgL_v1767z00_3397;
				}
				BGl_rtl_funcallz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(62),
					CNST_TABLE_REF(10), BGl_rtl_funz00zzsaw_defsz00, 41503L,
					BGl_proc3297z00zzsaw_defsz00, BGl_proc3296z00zzsaw_defsz00, BFALSE,
					BGl_proc3295z00zzsaw_defsz00, BFALSE, BgL_arg2621z00_3384,
					BgL_arg2622z00_3385);
			}
			{	/* SawMill/defs.scm 64 */
				obj_t BgL_arg2631z00_3404;
				obj_t BgL_arg2632z00_3405;

				{	/* SawMill/defs.scm 64 */
					obj_t BgL_v1768z00_3418;

					BgL_v1768z00_3418 = create_vector(2L);
					{	/* SawMill/defs.scm 64 */
						obj_t BgL_arg2639z00_3419;

						BgL_arg2639z00_3419 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(63),
							BGl_proc3299z00zzsaw_defsz00, BGl_proc3298z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(38));
						VECTOR_SET(BgL_v1768z00_3418, 0L, BgL_arg2639z00_3419);
					}
					{	/* SawMill/defs.scm 64 */
						obj_t BgL_arg2644z00_3429;

						BgL_arg2644z00_3429 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(64),
							BGl_proc3301z00zzsaw_defsz00, BGl_proc3300z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(58));
						VECTOR_SET(BgL_v1768z00_3418, 1L, BgL_arg2644z00_3429);
					}
					BgL_arg2631z00_3404 = BgL_v1768z00_3418;
				}
				{	/* SawMill/defs.scm 64 */
					obj_t BgL_v1769z00_3439;

					BgL_v1769z00_3439 = create_vector(0L);
					BgL_arg2632z00_3405 = BgL_v1769z00_3439;
				}
				BGl_rtl_pragmaz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(65),
					CNST_TABLE_REF(10), BGl_rtl_funz00zzsaw_defsz00, 59328L,
					BGl_proc3304z00zzsaw_defsz00, BGl_proc3303z00zzsaw_defsz00, BFALSE,
					BGl_proc3302z00zzsaw_defsz00, BFALSE, BgL_arg2631z00_3404,
					BgL_arg2632z00_3405);
			}
			{	/* SawMill/defs.scm 65 */
				obj_t BgL_arg2654z00_3446;
				obj_t BgL_arg2656z00_3447;

				{	/* SawMill/defs.scm 65 */
					obj_t BgL_v1770z00_3460;

					BgL_v1770z00_3460 = create_vector(2L);
					{	/* SawMill/defs.scm 65 */
						obj_t BgL_arg2662z00_3461;

						BgL_arg2662z00_3461 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(66),
							BGl_proc3306z00zzsaw_defsz00, BGl_proc3305z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1770z00_3460, 0L, BgL_arg2662z00_3461);
					}
					{	/* SawMill/defs.scm 65 */
						obj_t BgL_arg2669z00_3471;

						BgL_arg2669z00_3471 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(67),
							BGl_proc3308z00zzsaw_defsz00, BGl_proc3307z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1770z00_3460, 1L, BgL_arg2669z00_3471);
					}
					BgL_arg2654z00_3446 = BgL_v1770z00_3460;
				}
				{	/* SawMill/defs.scm 65 */
					obj_t BgL_v1771z00_3481;

					BgL_v1771z00_3481 = create_vector(0L);
					BgL_arg2656z00_3447 = BgL_v1771z00_3481;
				}
				BGl_rtl_castz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(68),
					CNST_TABLE_REF(10), BGl_rtl_funz00zzsaw_defsz00, 9526L,
					BGl_proc3311z00zzsaw_defsz00, BGl_proc3310z00zzsaw_defsz00, BFALSE,
					BGl_proc3309z00zzsaw_defsz00, BFALSE, BgL_arg2654z00_3446,
					BgL_arg2656z00_3447);
			}
			{	/* SawMill/defs.scm 66 */
				obj_t BgL_arg2680z00_3488;
				obj_t BgL_arg2681z00_3489;

				{	/* SawMill/defs.scm 66 */
					obj_t BgL_v1772z00_3501;

					BgL_v1772z00_3501 = create_vector(1L);
					{	/* SawMill/defs.scm 66 */
						obj_t BgL_arg2688z00_3502;

						BgL_arg2688z00_3502 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(1),
							BGl_proc3313z00zzsaw_defsz00, BGl_proc3312z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_typez00zztype_typez00);
						VECTOR_SET(BgL_v1772z00_3501, 0L, BgL_arg2688z00_3502);
					}
					BgL_arg2680z00_3488 = BgL_v1772z00_3501;
				}
				{	/* SawMill/defs.scm 66 */
					obj_t BgL_v1773z00_3512;

					BgL_v1773z00_3512 = create_vector(0L);
					BgL_arg2681z00_3489 = BgL_v1773z00_3512;
				}
				BGl_rtl_cast_nullz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(69),
					CNST_TABLE_REF(10), BGl_rtl_funz00zzsaw_defsz00, 56783L,
					BGl_proc3316z00zzsaw_defsz00, BGl_proc3315z00zzsaw_defsz00, BFALSE,
					BGl_proc3314z00zzsaw_defsz00, BFALSE, BgL_arg2680z00_3488,
					BgL_arg2681z00_3489);
			}
			{	/* SawMill/defs.scm 67 */
				obj_t BgL_arg2696z00_3519;
				obj_t BgL_arg2697z00_3520;

				{	/* SawMill/defs.scm 67 */
					obj_t BgL_v1774z00_3531;

					BgL_v1774z00_3531 = create_vector(0L);
					BgL_arg2696z00_3519 = BgL_v1774z00_3531;
				}
				{	/* SawMill/defs.scm 67 */
					obj_t BgL_v1775z00_3532;

					BgL_v1775z00_3532 = create_vector(0L);
					BgL_arg2697z00_3520 = BgL_v1775z00_3532;
				}
				BGl_rtl_protectz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(70),
					CNST_TABLE_REF(10), BGl_rtl_funz00zzsaw_defsz00, 65043L,
					BGl_proc3319z00zzsaw_defsz00, BGl_proc3318z00zzsaw_defsz00, BFALSE,
					BGl_proc3317z00zzsaw_defsz00, BFALSE, BgL_arg2696z00_3519,
					BgL_arg2697z00_3520);
			}
			{	/* SawMill/defs.scm 68 */
				obj_t BgL_arg2706z00_3539;
				obj_t BgL_arg2707z00_3540;

				{	/* SawMill/defs.scm 68 */
					obj_t BgL_v1776z00_3551;

					BgL_v1776z00_3551 = create_vector(0L);
					BgL_arg2706z00_3539 = BgL_v1776z00_3551;
				}
				{	/* SawMill/defs.scm 68 */
					obj_t BgL_v1777z00_3552;

					BgL_v1777z00_3552 = create_vector(0L);
					BgL_arg2707z00_3540 = BgL_v1777z00_3552;
				}
				BGl_rtl_protectedz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(71),
					CNST_TABLE_REF(10), BGl_rtl_funz00zzsaw_defsz00, 3034L,
					BGl_proc3322z00zzsaw_defsz00, BGl_proc3321z00zzsaw_defsz00, BFALSE,
					BGl_proc3320z00zzsaw_defsz00, BFALSE, BgL_arg2706z00_3539,
					BgL_arg2707z00_3540);
			}
			{	/* SawMill/defs.scm 71 */
				obj_t BgL_arg2717z00_3559;
				obj_t BgL_arg2719z00_3560;

				{	/* SawMill/defs.scm 71 */
					obj_t BgL_v1778z00_3575;

					BgL_v1778z00_3575 = create_vector(5L);
					{	/* SawMill/defs.scm 71 */
						obj_t BgL_arg2725z00_3576;

						BgL_arg2725z00_3576 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(11),
							BGl_proc3325z00zzsaw_defsz00, BGl_proc3324z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc3323z00zzsaw_defsz00,
							CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1778z00_3575, 0L, BgL_arg2725z00_3576);
					}
					{	/* SawMill/defs.scm 71 */
						obj_t BgL_arg2733z00_3589;

						BgL_arg2733z00_3589 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(72),
							BGl_proc3328z00zzsaw_defsz00, BGl_proc3327z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc3326z00zzsaw_defsz00,
							CNST_TABLE_REF(54));
						VECTOR_SET(BgL_v1778z00_3575, 1L, BgL_arg2733z00_3589);
					}
					{	/* SawMill/defs.scm 71 */
						obj_t BgL_arg2741z00_3602;

						BgL_arg2741z00_3602 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(73),
							BGl_proc3331z00zzsaw_defsz00, BGl_proc3330z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc3329z00zzsaw_defsz00,
							CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1778z00_3575, 2L, BgL_arg2741z00_3602);
					}
					{	/* SawMill/defs.scm 71 */
						obj_t BgL_arg2748z00_3615;

						BgL_arg2748z00_3615 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(74),
							BGl_proc3333z00zzsaw_defsz00, BGl_proc3332z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE,
							BGl_rtl_funz00zzsaw_defsz00);
						VECTOR_SET(BgL_v1778z00_3575, 3L, BgL_arg2748z00_3615);
					}
					{	/* SawMill/defs.scm 71 */
						obj_t BgL_arg2753z00_3625;

						BgL_arg2753z00_3625 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(75),
							BGl_proc3335z00zzsaw_defsz00, BGl_proc3334z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(54));
						VECTOR_SET(BgL_v1778z00_3575, 4L, BgL_arg2753z00_3625);
					}
					BgL_arg2717z00_3559 = BgL_v1778z00_3575;
				}
				{	/* SawMill/defs.scm 71 */
					obj_t BgL_v1779z00_3635;

					BgL_v1779z00_3635 = create_vector(0L);
					BgL_arg2719z00_3560 = BgL_v1779z00_3635;
				}
				BGl_rtl_insz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(76),
					CNST_TABLE_REF(10), BGl_objectz00zz__objectz00, 17433L,
					BGl_proc3338z00zzsaw_defsz00, BGl_proc3337z00zzsaw_defsz00, BFALSE,
					BGl_proc3336z00zzsaw_defsz00, BFALSE, BgL_arg2717z00_3559,
					BgL_arg2719z00_3560);
			}
			{	/* SawMill/defs.scm 79 */
				obj_t BgL_arg2761z00_3642;
				obj_t BgL_arg2762z00_3643;

				{	/* SawMill/defs.scm 79 */
					obj_t BgL_v1780z00_3657;

					BgL_v1780z00_3657 = create_vector(4L);
					{	/* SawMill/defs.scm 79 */
						obj_t BgL_arg2771z00_3658;

						BgL_arg2771z00_3658 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(77),
							BGl_proc3341z00zzsaw_defsz00, BGl_proc3340z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc3339z00zzsaw_defsz00,
							CNST_TABLE_REF(78));
						VECTOR_SET(BgL_v1780z00_3657, 0L, BgL_arg2771z00_3658);
					}
					{	/* SawMill/defs.scm 79 */
						obj_t BgL_arg2778z00_3671;

						BgL_arg2778z00_3671 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(79),
							BGl_proc3344z00zzsaw_defsz00, BGl_proc3343z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc3342z00zzsaw_defsz00,
							CNST_TABLE_REF(54));
						VECTOR_SET(BgL_v1780z00_3657, 1L, BgL_arg2778z00_3671);
					}
					{	/* SawMill/defs.scm 79 */
						obj_t BgL_arg2787z00_3684;

						BgL_arg2787z00_3684 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(80),
							BGl_proc3347z00zzsaw_defsz00, BGl_proc3346z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc3345z00zzsaw_defsz00,
							CNST_TABLE_REF(54));
						VECTOR_SET(BgL_v1780z00_3657, 2L, BgL_arg2787z00_3684);
					}
					{	/* SawMill/defs.scm 79 */
						obj_t BgL_arg2799z00_3697;

						BgL_arg2799z00_3697 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(81),
							BGl_proc3350z00zzsaw_defsz00, BGl_proc3349z00zzsaw_defsz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BGl_proc3348z00zzsaw_defsz00,
							CNST_TABLE_REF(54));
						VECTOR_SET(BgL_v1780z00_3657, 3L, BgL_arg2799z00_3697);
					}
					BgL_arg2761z00_3642 = BgL_v1780z00_3657;
				}
				{	/* SawMill/defs.scm 79 */
					obj_t BgL_v1781z00_3710;

					BgL_v1781z00_3710 = create_vector(0L);
					BgL_arg2762z00_3643 = BgL_v1781z00_3710;
				}
				return (BGl_blockz00zzsaw_defsz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(82),
						CNST_TABLE_REF(10), BGl_objectz00zz__objectz00, 60935L,
						BGl_proc3353z00zzsaw_defsz00, BGl_proc3352z00zzsaw_defsz00, BFALSE,
						BGl_proc3351z00zzsaw_defsz00, BFALSE, BgL_arg2761z00_3642,
						BgL_arg2762z00_3643), BUNSPEC);
			}
		}

	}



/* &<@anonymous:2767> */
	obj_t BGl_z62zc3z04anonymousza32767ze3ze5zzsaw_defsz00(obj_t BgL_envz00_7984,
		obj_t BgL_new1346z00_7985)
	{
		{	/* SawMill/defs.scm 79 */
			{
				BgL_blockz00_bglt BgL_auxz00_13501;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_new1346z00_7985)))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1346z00_7985)))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1346z00_7985)))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
									BgL_new1346z00_7985)))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_13501 = ((BgL_blockz00_bglt) BgL_new1346z00_7985);
				return ((obj_t) BgL_auxz00_13501);
			}
		}

	}



/* &lambda2765 */
	BgL_blockz00_bglt BGl_z62lambda2765z62zzsaw_defsz00(obj_t BgL_envz00_7986)
	{
		{	/* SawMill/defs.scm 79 */
			{	/* SawMill/defs.scm 79 */
				BgL_blockz00_bglt BgL_new1345z00_9534;

				BgL_new1345z00_9534 =
					((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_blockz00_bgl))));
				{	/* SawMill/defs.scm 79 */
					long BgL_arg2766z00_9535;

					BgL_arg2766z00_9535 = BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1345z00_9534), BgL_arg2766z00_9535);
				}
				{	/* SawMill/defs.scm 79 */
					BgL_objectz00_bglt BgL_tmpz00_13517;

					BgL_tmpz00_13517 = ((BgL_objectz00_bglt) BgL_new1345z00_9534);
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13517, BFALSE);
				}
				((BgL_objectz00_bglt) BgL_new1345z00_9534);
				return BgL_new1345z00_9534;
			}
		}

	}



/* &lambda2763 */
	BgL_blockz00_bglt BGl_z62lambda2763z62zzsaw_defsz00(obj_t BgL_envz00_7987,
		obj_t BgL_label1340z00_7988, obj_t BgL_preds1341z00_7989,
		obj_t BgL_succs1342z00_7990, obj_t BgL_first1343z00_7991)
	{
		{	/* SawMill/defs.scm 79 */
			{	/* SawMill/defs.scm 79 */
				int BgL_label1340z00_9536;

				BgL_label1340z00_9536 = CINT(BgL_label1340z00_7988);
				{	/* SawMill/defs.scm 79 */
					BgL_blockz00_bglt BgL_new1548z00_9540;

					{	/* SawMill/defs.scm 79 */
						BgL_blockz00_bglt BgL_new1547z00_9541;

						BgL_new1547z00_9541 =
							((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_blockz00_bgl))));
						{	/* SawMill/defs.scm 79 */
							long BgL_arg2764z00_9542;

							BgL_arg2764z00_9542 = BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1547z00_9541),
								BgL_arg2764z00_9542);
						}
						{	/* SawMill/defs.scm 79 */
							BgL_objectz00_bglt BgL_tmpz00_13526;

							BgL_tmpz00_13526 = ((BgL_objectz00_bglt) BgL_new1547z00_9541);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13526, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1547z00_9541);
						BgL_new1548z00_9540 = BgL_new1547z00_9541;
					}
					((((BgL_blockz00_bglt) COBJECT(BgL_new1548z00_9540))->BgL_labelz00) =
						((int) BgL_label1340z00_9536), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(BgL_new1548z00_9540))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1341z00_7989)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(BgL_new1548z00_9540))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1342z00_7990)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(BgL_new1548z00_9540))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1343z00_7991)), BUNSPEC);
					return BgL_new1548z00_9540;
				}
			}
		}

	}



/* &<@anonymous:2808> */
	obj_t BGl_z62zc3z04anonymousza32808ze3ze5zzsaw_defsz00(obj_t BgL_envz00_7992)
	{
		{	/* SawMill/defs.scm 79 */
			return BNIL;
		}

	}



/* &lambda2807 */
	obj_t BGl_z62lambda2807z62zzsaw_defsz00(obj_t BgL_envz00_7993,
		obj_t BgL_oz00_7994, obj_t BgL_vz00_7995)
	{
		{	/* SawMill/defs.scm 79 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_7994)))->BgL_firstz00) = ((obj_t)
					((obj_t) BgL_vz00_7995)), BUNSPEC);
		}

	}



/* &lambda2806 */
	obj_t BGl_z62lambda2806z62zzsaw_defsz00(obj_t BgL_envz00_7996,
		obj_t BgL_oz00_7997)
	{
		{	/* SawMill/defs.scm 79 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_7997)))->BgL_firstz00);
		}

	}



/* &<@anonymous:2797> */
	obj_t BGl_z62zc3z04anonymousza32797ze3ze5zzsaw_defsz00(obj_t BgL_envz00_7998)
	{
		{	/* SawMill/defs.scm 79 */
			return BNIL;
		}

	}



/* &lambda2796 */
	obj_t BGl_z62lambda2796z62zzsaw_defsz00(obj_t BgL_envz00_7999,
		obj_t BgL_oz00_8000, obj_t BgL_vz00_8001)
	{
		{	/* SawMill/defs.scm 79 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_8000)))->BgL_succsz00) = ((obj_t)
					((obj_t) BgL_vz00_8001)), BUNSPEC);
		}

	}



/* &lambda2795 */
	obj_t BGl_z62lambda2795z62zzsaw_defsz00(obj_t BgL_envz00_8002,
		obj_t BgL_oz00_8003)
	{
		{	/* SawMill/defs.scm 79 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_8003)))->BgL_succsz00);
		}

	}



/* &<@anonymous:2786> */
	obj_t BGl_z62zc3z04anonymousza32786ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8004)
	{
		{	/* SawMill/defs.scm 79 */
			return BNIL;
		}

	}



/* &lambda2785 */
	obj_t BGl_z62lambda2785z62zzsaw_defsz00(obj_t BgL_envz00_8005,
		obj_t BgL_oz00_8006, obj_t BgL_vz00_8007)
	{
		{	/* SawMill/defs.scm 79 */
			return
				((((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_8006)))->BgL_predsz00) = ((obj_t)
					((obj_t) BgL_vz00_8007)), BUNSPEC);
		}

	}



/* &lambda2784 */
	obj_t BGl_z62lambda2784z62zzsaw_defsz00(obj_t BgL_envz00_8008,
		obj_t BgL_oz00_8009)
	{
		{	/* SawMill/defs.scm 79 */
			return
				(((BgL_blockz00_bglt) COBJECT(
						((BgL_blockz00_bglt) BgL_oz00_8009)))->BgL_predsz00);
		}

	}



/* &<@anonymous:2777> */
	obj_t BGl_z62zc3z04anonymousza32777ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8010)
	{
		{	/* SawMill/defs.scm 79 */
			return BINT(0L);
		}

	}



/* &lambda2776 */
	obj_t BGl_z62lambda2776z62zzsaw_defsz00(obj_t BgL_envz00_8011,
		obj_t BgL_oz00_8012, obj_t BgL_vz00_8013)
	{
		{	/* SawMill/defs.scm 79 */
			{	/* SawMill/defs.scm 79 */
				int BgL_vz00_9553;

				BgL_vz00_9553 = CINT(BgL_vz00_8013);
				return
					((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_oz00_8012)))->BgL_labelz00) =
					((int) BgL_vz00_9553), BUNSPEC);
		}}

	}



/* &lambda2775 */
	obj_t BGl_z62lambda2775z62zzsaw_defsz00(obj_t BgL_envz00_8014,
		obj_t BgL_oz00_8015)
	{
		{	/* SawMill/defs.scm 79 */
			return
				BINT(
				(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_8015)))->BgL_labelz00));
		}

	}



/* &<@anonymous:2724> */
	obj_t BGl_z62zc3z04anonymousza32724ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8016,
		obj_t BgL_new1338z00_8017)
	{
		{	/* SawMill/defs.scm 71 */
			{
				BgL_rtl_insz00_bglt BgL_auxz00_13559;

				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_new1338z00_8017)))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1338z00_8017)))->BgL_z52spillz52) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1338z00_8017)))->BgL_destz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_rtl_funz00_bglt BgL_auxz00_13566;

					{	/* SawMill/defs.scm 71 */
						obj_t BgL_classz00_9556;

						BgL_classz00_9556 = BGl_rtl_funz00zzsaw_defsz00;
						{	/* SawMill/defs.scm 71 */
							obj_t BgL__ortest_1117z00_9557;

							BgL__ortest_1117z00_9557 = BGL_CLASS_NIL(BgL_classz00_9556);
							if (CBOOL(BgL__ortest_1117z00_9557))
								{	/* SawMill/defs.scm 71 */
									BgL_auxz00_13566 =
										((BgL_rtl_funz00_bglt) BgL__ortest_1117z00_9557);
								}
							else
								{	/* SawMill/defs.scm 71 */
									BgL_auxz00_13566 =
										((BgL_rtl_funz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9556));
								}
						}
					}
					((((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_new1338z00_8017)))->BgL_funz00) =
						((BgL_rtl_funz00_bglt) BgL_auxz00_13566), BUNSPEC);
				}
				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_new1338z00_8017)))->BgL_argsz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_13559 = ((BgL_rtl_insz00_bglt) BgL_new1338z00_8017);
				return ((obj_t) BgL_auxz00_13559);
			}
		}

	}



/* &lambda2722 */
	BgL_rtl_insz00_bglt BGl_z62lambda2722z62zzsaw_defsz00(obj_t BgL_envz00_8018)
	{
		{	/* SawMill/defs.scm 71 */
			{	/* SawMill/defs.scm 71 */
				BgL_rtl_insz00_bglt BgL_new1337z00_9558;

				BgL_new1337z00_9558 =
					((BgL_rtl_insz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_insz00_bgl))));
				{	/* SawMill/defs.scm 71 */
					long BgL_arg2723z00_9559;

					BgL_arg2723z00_9559 = BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1337z00_9558), BgL_arg2723z00_9559);
				}
				{	/* SawMill/defs.scm 71 */
					BgL_objectz00_bglt BgL_tmpz00_13583;

					BgL_tmpz00_13583 = ((BgL_objectz00_bglt) BgL_new1337z00_9558);
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13583, BFALSE);
				}
				((BgL_objectz00_bglt) BgL_new1337z00_9558);
				return BgL_new1337z00_9558;
			}
		}

	}



/* &lambda2720 */
	BgL_rtl_insz00_bglt BGl_z62lambda2720z62zzsaw_defsz00(obj_t BgL_envz00_8019,
		obj_t BgL_loc1332z00_8020, obj_t BgL_z52spill1333z52_8021,
		obj_t BgL_dest1334z00_8022, obj_t BgL_fun1335z00_8023,
		obj_t BgL_args1336z00_8024)
	{
		{	/* SawMill/defs.scm 71 */
			{	/* SawMill/defs.scm 71 */
				BgL_rtl_insz00_bglt BgL_new1546z00_9563;

				{	/* SawMill/defs.scm 71 */
					BgL_rtl_insz00_bglt BgL_new1545z00_9564;

					BgL_new1545z00_9564 =
						((BgL_rtl_insz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_insz00_bgl))));
					{	/* SawMill/defs.scm 71 */
						long BgL_arg2721z00_9565;

						BgL_arg2721z00_9565 = BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1545z00_9564), BgL_arg2721z00_9565);
					}
					{	/* SawMill/defs.scm 71 */
						BgL_objectz00_bglt BgL_tmpz00_13591;

						BgL_tmpz00_13591 = ((BgL_objectz00_bglt) BgL_new1545z00_9564);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_13591, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1545z00_9564);
					BgL_new1546z00_9563 = BgL_new1545z00_9564;
				}
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1546z00_9563))->BgL_locz00) =
					((obj_t) BgL_loc1332z00_8020), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1546z00_9563))->
						BgL_z52spillz52) =
					((obj_t) ((obj_t) BgL_z52spill1333z52_8021)), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1546z00_9563))->BgL_destz00) =
					((obj_t) BgL_dest1334z00_8022), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1546z00_9563))->BgL_funz00) =
					((BgL_rtl_funz00_bglt) ((BgL_rtl_funz00_bglt) BgL_fun1335z00_8023)),
					BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1546z00_9563))->BgL_argsz00) =
					((obj_t) ((obj_t) BgL_args1336z00_8024)), BUNSPEC);
				return BgL_new1546z00_9563;
			}
		}

	}



/* &lambda2757 */
	obj_t BGl_z62lambda2757z62zzsaw_defsz00(obj_t BgL_envz00_8025,
		obj_t BgL_oz00_8026, obj_t BgL_vz00_8027)
	{
		{	/* SawMill/defs.scm 71 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_8026)))->BgL_argsz00) = ((obj_t)
					((obj_t) BgL_vz00_8027)), BUNSPEC);
		}

	}



/* &lambda2756 */
	obj_t BGl_z62lambda2756z62zzsaw_defsz00(obj_t BgL_envz00_8028,
		obj_t BgL_oz00_8029)
	{
		{	/* SawMill/defs.scm 71 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_8029)))->BgL_argsz00);
		}

	}



/* &lambda2752 */
	obj_t BGl_z62lambda2752z62zzsaw_defsz00(obj_t BgL_envz00_8030,
		obj_t BgL_oz00_8031, obj_t BgL_vz00_8032)
	{
		{	/* SawMill/defs.scm 71 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_8031)))->BgL_funz00) =
				((BgL_rtl_funz00_bglt) ((BgL_rtl_funz00_bglt) BgL_vz00_8032)), BUNSPEC);
		}

	}



/* &lambda2751 */
	BgL_rtl_funz00_bglt BGl_z62lambda2751z62zzsaw_defsz00(obj_t BgL_envz00_8033,
		obj_t BgL_oz00_8034)
	{
		{	/* SawMill/defs.scm 71 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_8034)))->BgL_funz00);
		}

	}



/* &<@anonymous:2747> */
	obj_t BGl_z62zc3z04anonymousza32747ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8035)
	{
		{	/* SawMill/defs.scm 71 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2746 */
	obj_t BGl_z62lambda2746z62zzsaw_defsz00(obj_t BgL_envz00_8036,
		obj_t BgL_oz00_8037, obj_t BgL_vz00_8038)
	{
		{	/* SawMill/defs.scm 71 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_8037)))->BgL_destz00) =
				((obj_t) BgL_vz00_8038), BUNSPEC);
		}

	}



/* &lambda2745 */
	obj_t BGl_z62lambda2745z62zzsaw_defsz00(obj_t BgL_envz00_8039,
		obj_t BgL_oz00_8040)
	{
		{	/* SawMill/defs.scm 71 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_8040)))->BgL_destz00);
		}

	}



/* &<@anonymous:2740> */
	obj_t BGl_z62zc3z04anonymousza32740ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8041)
	{
		{	/* SawMill/defs.scm 71 */
			return BNIL;
		}

	}



/* &lambda2739 */
	obj_t BGl_z62lambda2739z62zzsaw_defsz00(obj_t BgL_envz00_8042,
		obj_t BgL_oz00_8043, obj_t BgL_vz00_8044)
	{
		{	/* SawMill/defs.scm 71 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_8043)))->BgL_z52spillz52) =
				((obj_t) ((obj_t) BgL_vz00_8044)), BUNSPEC);
		}

	}



/* &lambda2738 */
	obj_t BGl_z62lambda2738z62zzsaw_defsz00(obj_t BgL_envz00_8045,
		obj_t BgL_oz00_8046)
	{
		{	/* SawMill/defs.scm 71 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_8046)))->BgL_z52spillz52);
		}

	}



/* &<@anonymous:2731> */
	obj_t BGl_z62zc3z04anonymousza32731ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8047)
	{
		{	/* SawMill/defs.scm 71 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2730 */
	obj_t BGl_z62lambda2730z62zzsaw_defsz00(obj_t BgL_envz00_8048,
		obj_t BgL_oz00_8049, obj_t BgL_vz00_8050)
	{
		{	/* SawMill/defs.scm 71 */
			return
				((((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_oz00_8049)))->BgL_locz00) =
				((obj_t) BgL_vz00_8050), BUNSPEC);
		}

	}



/* &lambda2729 */
	obj_t BGl_z62lambda2729z62zzsaw_defsz00(obj_t BgL_envz00_8051,
		obj_t BgL_oz00_8052)
	{
		{	/* SawMill/defs.scm 71 */
			return
				(((BgL_rtl_insz00_bglt) COBJECT(
						((BgL_rtl_insz00_bglt) BgL_oz00_8052)))->BgL_locz00);
		}

	}



/* &<@anonymous:2712> */
	obj_t BGl_z62zc3z04anonymousza32712ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8053,
		obj_t BgL_new1330z00_8054)
	{
		{	/* SawMill/defs.scm 68 */
			{
				BgL_rtl_protectedz00_bglt BgL_auxz00_13628;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_protectedz00_bglt) BgL_new1330z00_8054))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_13628 = ((BgL_rtl_protectedz00_bglt) BgL_new1330z00_8054);
				return ((obj_t) BgL_auxz00_13628);
			}
		}

	}



/* &lambda2710 */
	BgL_rtl_protectedz00_bglt BGl_z62lambda2710z62zzsaw_defsz00(obj_t
		BgL_envz00_8055)
	{
		{	/* SawMill/defs.scm 68 */
			{	/* SawMill/defs.scm 68 */
				BgL_rtl_protectedz00_bglt BgL_new1329z00_9580;

				BgL_new1329z00_9580 =
					((BgL_rtl_protectedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_protectedz00_bgl))));
				{	/* SawMill/defs.scm 68 */
					long BgL_arg2711z00_9581;

					BgL_arg2711z00_9581 =
						BGL_CLASS_NUM(BGl_rtl_protectedz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1329z00_9580), BgL_arg2711z00_9581);
				}
				return BgL_new1329z00_9580;
			}
		}

	}



/* &lambda2708 */
	BgL_rtl_protectedz00_bglt BGl_z62lambda2708z62zzsaw_defsz00(obj_t
		BgL_envz00_8056, obj_t BgL_loc1328z00_8057)
	{
		{	/* SawMill/defs.scm 68 */
			{	/* SawMill/defs.scm 68 */
				BgL_rtl_protectedz00_bglt BgL_new1544z00_9582;

				{	/* SawMill/defs.scm 68 */
					BgL_rtl_protectedz00_bglt BgL_new1543z00_9583;

					BgL_new1543z00_9583 =
						((BgL_rtl_protectedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_protectedz00_bgl))));
					{	/* SawMill/defs.scm 68 */
						long BgL_arg2709z00_9584;

						BgL_arg2709z00_9584 =
							BGL_CLASS_NUM(BGl_rtl_protectedz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1543z00_9583), BgL_arg2709z00_9584);
					}
					BgL_new1544z00_9582 = BgL_new1543z00_9583;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1544z00_9582)))->BgL_locz00) =
					((obj_t) BgL_loc1328z00_8057), BUNSPEC);
				return BgL_new1544z00_9582;
			}
		}

	}



/* &<@anonymous:2702> */
	obj_t BGl_z62zc3z04anonymousza32702ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8058,
		obj_t BgL_new1326z00_8059)
	{
		{	/* SawMill/defs.scm 67 */
			{
				BgL_rtl_protectz00_bglt BgL_auxz00_13644;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_protectz00_bglt) BgL_new1326z00_8059))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_13644 = ((BgL_rtl_protectz00_bglt) BgL_new1326z00_8059);
				return ((obj_t) BgL_auxz00_13644);
			}
		}

	}



/* &lambda2700 */
	BgL_rtl_protectz00_bglt BGl_z62lambda2700z62zzsaw_defsz00(obj_t
		BgL_envz00_8060)
	{
		{	/* SawMill/defs.scm 67 */
			{	/* SawMill/defs.scm 67 */
				BgL_rtl_protectz00_bglt BgL_new1325z00_9586;

				BgL_new1325z00_9586 =
					((BgL_rtl_protectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_protectz00_bgl))));
				{	/* SawMill/defs.scm 67 */
					long BgL_arg2701z00_9587;

					BgL_arg2701z00_9587 = BGL_CLASS_NUM(BGl_rtl_protectz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1325z00_9586), BgL_arg2701z00_9587);
				}
				return BgL_new1325z00_9586;
			}
		}

	}



/* &lambda2698 */
	BgL_rtl_protectz00_bglt BGl_z62lambda2698z62zzsaw_defsz00(obj_t
		BgL_envz00_8061, obj_t BgL_loc1324z00_8062)
	{
		{	/* SawMill/defs.scm 67 */
			{	/* SawMill/defs.scm 67 */
				BgL_rtl_protectz00_bglt BgL_new1542z00_9588;

				{	/* SawMill/defs.scm 67 */
					BgL_rtl_protectz00_bglt BgL_new1541z00_9589;

					BgL_new1541z00_9589 =
						((BgL_rtl_protectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_protectz00_bgl))));
					{	/* SawMill/defs.scm 67 */
						long BgL_arg2699z00_9590;

						BgL_arg2699z00_9590 =
							BGL_CLASS_NUM(BGl_rtl_protectz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1541z00_9589), BgL_arg2699z00_9590);
					}
					BgL_new1542z00_9588 = BgL_new1541z00_9589;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1542z00_9588)))->BgL_locz00) =
					((obj_t) BgL_loc1324z00_8062), BUNSPEC);
				return BgL_new1542z00_9588;
			}
		}

	}



/* &<@anonymous:2687> */
	obj_t BGl_z62zc3z04anonymousza32687ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8063,
		obj_t BgL_new1322z00_8064)
	{
		{	/* SawMill/defs.scm 66 */
			{
				BgL_rtl_cast_nullz00_bglt BgL_auxz00_13660;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_cast_nullz00_bglt) BgL_new1322z00_8064))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_13664;

					{	/* SawMill/defs.scm 66 */
						obj_t BgL_classz00_9592;

						BgL_classz00_9592 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 66 */
							obj_t BgL__ortest_1117z00_9593;

							BgL__ortest_1117z00_9593 = BGL_CLASS_NIL(BgL_classz00_9592);
							if (CBOOL(BgL__ortest_1117z00_9593))
								{	/* SawMill/defs.scm 66 */
									BgL_auxz00_13664 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9593);
								}
							else
								{	/* SawMill/defs.scm 66 */
									BgL_auxz00_13664 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9592));
								}
						}
					}
					((((BgL_rtl_cast_nullz00_bglt) COBJECT(
									((BgL_rtl_cast_nullz00_bglt) BgL_new1322z00_8064)))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_13664), BUNSPEC);
				}
				BgL_auxz00_13660 = ((BgL_rtl_cast_nullz00_bglt) BgL_new1322z00_8064);
				return ((obj_t) BgL_auxz00_13660);
			}
		}

	}



/* &lambda2685 */
	BgL_rtl_cast_nullz00_bglt BGl_z62lambda2685z62zzsaw_defsz00(obj_t
		BgL_envz00_8065)
	{
		{	/* SawMill/defs.scm 66 */
			{	/* SawMill/defs.scm 66 */
				BgL_rtl_cast_nullz00_bglt BgL_new1321z00_9594;

				BgL_new1321z00_9594 =
					((BgL_rtl_cast_nullz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_cast_nullz00_bgl))));
				{	/* SawMill/defs.scm 66 */
					long BgL_arg2686z00_9595;

					BgL_arg2686z00_9595 =
						BGL_CLASS_NUM(BGl_rtl_cast_nullz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1321z00_9594), BgL_arg2686z00_9595);
				}
				return BgL_new1321z00_9594;
			}
		}

	}



/* &lambda2682 */
	BgL_rtl_cast_nullz00_bglt BGl_z62lambda2682z62zzsaw_defsz00(obj_t
		BgL_envz00_8066, obj_t BgL_loc1319z00_8067, obj_t BgL_type1320z00_8068)
	{
		{	/* SawMill/defs.scm 66 */
			{	/* SawMill/defs.scm 66 */
				BgL_rtl_cast_nullz00_bglt BgL_new1540z00_9597;

				{	/* SawMill/defs.scm 66 */
					BgL_rtl_cast_nullz00_bglt BgL_new1539z00_9598;

					BgL_new1539z00_9598 =
						((BgL_rtl_cast_nullz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_cast_nullz00_bgl))));
					{	/* SawMill/defs.scm 66 */
						long BgL_arg2684z00_9599;

						BgL_arg2684z00_9599 =
							BGL_CLASS_NUM(BGl_rtl_cast_nullz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1539z00_9598), BgL_arg2684z00_9599);
					}
					BgL_new1540z00_9597 = BgL_new1539z00_9598;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1540z00_9597)))->BgL_locz00) =
					((obj_t) BgL_loc1319z00_8067), BUNSPEC);
				((((BgL_rtl_cast_nullz00_bglt) COBJECT(BgL_new1540z00_9597))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1320z00_8068)),
					BUNSPEC);
				return BgL_new1540z00_9597;
			}
		}

	}



/* &lambda2692 */
	obj_t BGl_z62lambda2692z62zzsaw_defsz00(obj_t BgL_envz00_8069,
		obj_t BgL_oz00_8070, obj_t BgL_vz00_8071)
	{
		{	/* SawMill/defs.scm 66 */
			return
				((((BgL_rtl_cast_nullz00_bglt) COBJECT(
							((BgL_rtl_cast_nullz00_bglt) BgL_oz00_8070)))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8071)), BUNSPEC);
		}

	}



/* &lambda2691 */
	BgL_typez00_bglt BGl_z62lambda2691z62zzsaw_defsz00(obj_t BgL_envz00_8072,
		obj_t BgL_oz00_8073)
	{
		{	/* SawMill/defs.scm 66 */
			return
				(((BgL_rtl_cast_nullz00_bglt) COBJECT(
						((BgL_rtl_cast_nullz00_bglt) BgL_oz00_8073)))->BgL_typez00);
		}

	}



/* &<@anonymous:2661> */
	obj_t BGl_z62zc3z04anonymousza32661ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8074,
		obj_t BgL_new1317z00_8075)
	{
		{	/* SawMill/defs.scm 65 */
			{
				BgL_rtl_castz00_bglt BgL_auxz00_13692;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_castz00_bglt) BgL_new1317z00_8075))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_13696;

					{	/* SawMill/defs.scm 65 */
						obj_t BgL_classz00_9604;

						BgL_classz00_9604 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 65 */
							obj_t BgL__ortest_1117z00_9605;

							BgL__ortest_1117z00_9605 = BGL_CLASS_NIL(BgL_classz00_9604);
							if (CBOOL(BgL__ortest_1117z00_9605))
								{	/* SawMill/defs.scm 65 */
									BgL_auxz00_13696 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9605);
								}
							else
								{	/* SawMill/defs.scm 65 */
									BgL_auxz00_13696 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9604));
								}
						}
					}
					((((BgL_rtl_castz00_bglt) COBJECT(
									((BgL_rtl_castz00_bglt) BgL_new1317z00_8075)))->
							BgL_totypez00) = ((BgL_typez00_bglt) BgL_auxz00_13696), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_13705;

					{	/* SawMill/defs.scm 65 */
						obj_t BgL_classz00_9606;

						BgL_classz00_9606 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 65 */
							obj_t BgL__ortest_1117z00_9607;

							BgL__ortest_1117z00_9607 = BGL_CLASS_NIL(BgL_classz00_9606);
							if (CBOOL(BgL__ortest_1117z00_9607))
								{	/* SawMill/defs.scm 65 */
									BgL_auxz00_13705 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9607);
								}
							else
								{	/* SawMill/defs.scm 65 */
									BgL_auxz00_13705 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9606));
								}
						}
					}
					((((BgL_rtl_castz00_bglt) COBJECT(
									((BgL_rtl_castz00_bglt) BgL_new1317z00_8075)))->
							BgL_fromtypez00) =
						((BgL_typez00_bglt) BgL_auxz00_13705), BUNSPEC);
				}
				BgL_auxz00_13692 = ((BgL_rtl_castz00_bglt) BgL_new1317z00_8075);
				return ((obj_t) BgL_auxz00_13692);
			}
		}

	}



/* &lambda2659 */
	BgL_rtl_castz00_bglt BGl_z62lambda2659z62zzsaw_defsz00(obj_t BgL_envz00_8076)
	{
		{	/* SawMill/defs.scm 65 */
			{	/* SawMill/defs.scm 65 */
				BgL_rtl_castz00_bglt BgL_new1316z00_9608;

				BgL_new1316z00_9608 =
					((BgL_rtl_castz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_castz00_bgl))));
				{	/* SawMill/defs.scm 65 */
					long BgL_arg2660z00_9609;

					BgL_arg2660z00_9609 = BGL_CLASS_NUM(BGl_rtl_castz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1316z00_9608), BgL_arg2660z00_9609);
				}
				return BgL_new1316z00_9608;
			}
		}

	}



/* &lambda2657 */
	BgL_rtl_castz00_bglt BGl_z62lambda2657z62zzsaw_defsz00(obj_t BgL_envz00_8077,
		obj_t BgL_loc1313z00_8078, obj_t BgL_totype1314z00_8079,
		obj_t BgL_fromtype1315z00_8080)
	{
		{	/* SawMill/defs.scm 65 */
			{	/* SawMill/defs.scm 65 */
				BgL_rtl_castz00_bglt BgL_new1538z00_9612;

				{	/* SawMill/defs.scm 65 */
					BgL_rtl_castz00_bglt BgL_new1537z00_9613;

					BgL_new1537z00_9613 =
						((BgL_rtl_castz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_castz00_bgl))));
					{	/* SawMill/defs.scm 65 */
						long BgL_arg2658z00_9614;

						BgL_arg2658z00_9614 = BGL_CLASS_NUM(BGl_rtl_castz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1537z00_9613), BgL_arg2658z00_9614);
					}
					BgL_new1538z00_9612 = BgL_new1537z00_9613;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1538z00_9612)))->BgL_locz00) =
					((obj_t) BgL_loc1313z00_8078), BUNSPEC);
				((((BgL_rtl_castz00_bglt) COBJECT(BgL_new1538z00_9612))->
						BgL_totypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_totype1314z00_8079)),
					BUNSPEC);
				((((BgL_rtl_castz00_bglt) COBJECT(BgL_new1538z00_9612))->
						BgL_fromtypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_fromtype1315z00_8080)),
					BUNSPEC);
				return BgL_new1538z00_9612;
			}
		}

	}



/* &lambda2674 */
	obj_t BGl_z62lambda2674z62zzsaw_defsz00(obj_t BgL_envz00_8081,
		obj_t BgL_oz00_8082, obj_t BgL_vz00_8083)
	{
		{	/* SawMill/defs.scm 65 */
			return
				((((BgL_rtl_castz00_bglt) COBJECT(
							((BgL_rtl_castz00_bglt) BgL_oz00_8082)))->BgL_fromtypez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8083)), BUNSPEC);
		}

	}



/* &lambda2673 */
	BgL_typez00_bglt BGl_z62lambda2673z62zzsaw_defsz00(obj_t BgL_envz00_8084,
		obj_t BgL_oz00_8085)
	{
		{	/* SawMill/defs.scm 65 */
			return
				(((BgL_rtl_castz00_bglt) COBJECT(
						((BgL_rtl_castz00_bglt) BgL_oz00_8085)))->BgL_fromtypez00);
		}

	}



/* &lambda2667 */
	obj_t BGl_z62lambda2667z62zzsaw_defsz00(obj_t BgL_envz00_8086,
		obj_t BgL_oz00_8087, obj_t BgL_vz00_8088)
	{
		{	/* SawMill/defs.scm 65 */
			return
				((((BgL_rtl_castz00_bglt) COBJECT(
							((BgL_rtl_castz00_bglt) BgL_oz00_8087)))->BgL_totypez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8088)), BUNSPEC);
		}

	}



/* &lambda2666 */
	BgL_typez00_bglt BGl_z62lambda2666z62zzsaw_defsz00(obj_t BgL_envz00_8089,
		obj_t BgL_oz00_8090)
	{
		{	/* SawMill/defs.scm 65 */
			return
				(((BgL_rtl_castz00_bglt) COBJECT(
						((BgL_rtl_castz00_bglt) BgL_oz00_8090)))->BgL_totypez00);
		}

	}



/* &<@anonymous:2638> */
	obj_t BGl_z62zc3z04anonymousza32638ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8091,
		obj_t BgL_new1311z00_8092)
	{
		{	/* SawMill/defs.scm 64 */
			{
				BgL_rtl_pragmaz00_bglt BgL_auxz00_13740;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_pragmaz00_bglt) BgL_new1311z00_8092))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_pragmaz00_bglt) COBJECT(((BgL_rtl_pragmaz00_bglt)
									BgL_new1311z00_8092)))->BgL_formatz00) =
					((obj_t) BGl_string3354z00zzsaw_defsz00), BUNSPEC);
				((((BgL_rtl_pragmaz00_bglt) COBJECT(((BgL_rtl_pragmaz00_bglt)
									BgL_new1311z00_8092)))->BgL_srfi0z00) =
					((obj_t) CNST_TABLE_REF(83)), BUNSPEC);
				BgL_auxz00_13740 = ((BgL_rtl_pragmaz00_bglt) BgL_new1311z00_8092);
				return ((obj_t) BgL_auxz00_13740);
			}
		}

	}



/* &lambda2636 */
	BgL_rtl_pragmaz00_bglt BGl_z62lambda2636z62zzsaw_defsz00(obj_t
		BgL_envz00_8093)
	{
		{	/* SawMill/defs.scm 64 */
			{	/* SawMill/defs.scm 64 */
				BgL_rtl_pragmaz00_bglt BgL_new1310z00_9622;

				BgL_new1310z00_9622 =
					((BgL_rtl_pragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_pragmaz00_bgl))));
				{	/* SawMill/defs.scm 64 */
					long BgL_arg2637z00_9623;

					BgL_arg2637z00_9623 = BGL_CLASS_NUM(BGl_rtl_pragmaz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1310z00_9622), BgL_arg2637z00_9623);
				}
				return BgL_new1310z00_9622;
			}
		}

	}



/* &lambda2633 */
	BgL_rtl_pragmaz00_bglt BGl_z62lambda2633z62zzsaw_defsz00(obj_t
		BgL_envz00_8094, obj_t BgL_loc1307z00_8095, obj_t BgL_format1308z00_8096,
		obj_t BgL_srfi01309z00_8097)
	{
		{	/* SawMill/defs.scm 64 */
			{	/* SawMill/defs.scm 64 */
				BgL_rtl_pragmaz00_bglt BgL_new1536z00_9626;

				{	/* SawMill/defs.scm 64 */
					BgL_rtl_pragmaz00_bglt BgL_new1535z00_9627;

					BgL_new1535z00_9627 =
						((BgL_rtl_pragmaz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_pragmaz00_bgl))));
					{	/* SawMill/defs.scm 64 */
						long BgL_arg2635z00_9628;

						BgL_arg2635z00_9628 = BGL_CLASS_NUM(BGl_rtl_pragmaz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1535z00_9627), BgL_arg2635z00_9628);
					}
					BgL_new1536z00_9626 = BgL_new1535z00_9627;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1536z00_9626)))->BgL_locz00) =
					((obj_t) BgL_loc1307z00_8095), BUNSPEC);
				((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_new1536z00_9626))->
						BgL_formatz00) =
					((obj_t) ((obj_t) BgL_format1308z00_8096)), BUNSPEC);
				((((BgL_rtl_pragmaz00_bglt) COBJECT(BgL_new1536z00_9626))->
						BgL_srfi0z00) = ((obj_t) ((obj_t) BgL_srfi01309z00_8097)), BUNSPEC);
				return BgL_new1536z00_9626;
			}
		}

	}



/* &lambda2649 */
	obj_t BGl_z62lambda2649z62zzsaw_defsz00(obj_t BgL_envz00_8098,
		obj_t BgL_oz00_8099, obj_t BgL_vz00_8100)
	{
		{	/* SawMill/defs.scm 64 */
			return
				((((BgL_rtl_pragmaz00_bglt) COBJECT(
							((BgL_rtl_pragmaz00_bglt) BgL_oz00_8099)))->BgL_srfi0z00) =
				((obj_t) ((obj_t) BgL_vz00_8100)), BUNSPEC);
		}

	}



/* &lambda2648 */
	obj_t BGl_z62lambda2648z62zzsaw_defsz00(obj_t BgL_envz00_8101,
		obj_t BgL_oz00_8102)
	{
		{	/* SawMill/defs.scm 64 */
			return
				(((BgL_rtl_pragmaz00_bglt) COBJECT(
						((BgL_rtl_pragmaz00_bglt) BgL_oz00_8102)))->BgL_srfi0z00);
		}

	}



/* &lambda2643 */
	obj_t BGl_z62lambda2643z62zzsaw_defsz00(obj_t BgL_envz00_8103,
		obj_t BgL_oz00_8104, obj_t BgL_vz00_8105)
	{
		{	/* SawMill/defs.scm 64 */
			return
				((((BgL_rtl_pragmaz00_bglt) COBJECT(
							((BgL_rtl_pragmaz00_bglt) BgL_oz00_8104)))->BgL_formatz00) =
				((obj_t) ((obj_t) BgL_vz00_8105)), BUNSPEC);
		}

	}



/* &lambda2642 */
	obj_t BGl_z62lambda2642z62zzsaw_defsz00(obj_t BgL_envz00_8106,
		obj_t BgL_oz00_8107)
	{
		{	/* SawMill/defs.scm 64 */
			return
				(((BgL_rtl_pragmaz00_bglt) COBJECT(
						((BgL_rtl_pragmaz00_bglt) BgL_oz00_8107)))->BgL_formatz00);
		}

	}



/* &<@anonymous:2627> */
	obj_t BGl_z62zc3z04anonymousza32627ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8108,
		obj_t BgL_new1305z00_8109)
	{
		{	/* SawMill/defs.scm 63 */
			{
				BgL_rtl_funcallz00_bglt BgL_auxz00_13775;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_funcallz00_bglt) BgL_new1305z00_8109))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_13775 = ((BgL_rtl_funcallz00_bglt) BgL_new1305z00_8109);
				return ((obj_t) BgL_auxz00_13775);
			}
		}

	}



/* &lambda2625 */
	BgL_rtl_funcallz00_bglt BGl_z62lambda2625z62zzsaw_defsz00(obj_t
		BgL_envz00_8110)
	{
		{	/* SawMill/defs.scm 63 */
			{	/* SawMill/defs.scm 63 */
				BgL_rtl_funcallz00_bglt BgL_new1304z00_9636;

				BgL_new1304z00_9636 =
					((BgL_rtl_funcallz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_funcallz00_bgl))));
				{	/* SawMill/defs.scm 63 */
					long BgL_arg2626z00_9637;

					BgL_arg2626z00_9637 = BGL_CLASS_NUM(BGl_rtl_funcallz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1304z00_9636), BgL_arg2626z00_9637);
				}
				return BgL_new1304z00_9636;
			}
		}

	}



/* &lambda2623 */
	BgL_rtl_funcallz00_bglt BGl_z62lambda2623z62zzsaw_defsz00(obj_t
		BgL_envz00_8111, obj_t BgL_loc1303z00_8112)
	{
		{	/* SawMill/defs.scm 63 */
			{	/* SawMill/defs.scm 63 */
				BgL_rtl_funcallz00_bglt BgL_new1534z00_9638;

				{	/* SawMill/defs.scm 63 */
					BgL_rtl_funcallz00_bglt BgL_new1533z00_9639;

					BgL_new1533z00_9639 =
						((BgL_rtl_funcallz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_funcallz00_bgl))));
					{	/* SawMill/defs.scm 63 */
						long BgL_arg2624z00_9640;

						BgL_arg2624z00_9640 =
							BGL_CLASS_NUM(BGl_rtl_funcallz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1533z00_9639), BgL_arg2624z00_9640);
					}
					BgL_new1534z00_9638 = BgL_new1533z00_9639;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1534z00_9638)))->BgL_locz00) =
					((obj_t) BgL_loc1303z00_8112), BUNSPEC);
				return BgL_new1534z00_9638;
			}
		}

	}



/* &<@anonymous:2600> */
	obj_t BGl_z62zc3z04anonymousza32600ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8113,
		obj_t BgL_new1301z00_8114)
	{
		{	/* SawMill/defs.scm 62 */
			{
				BgL_rtl_lightfuncallz00_bglt BgL_auxz00_13791;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_lightfuncallz00_bglt) BgL_new1301z00_8114))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_lightfuncallz00_bglt)
							COBJECT(((BgL_rtl_lightfuncallz00_bglt) BgL_new1301z00_8114)))->
						BgL_namez00) = ((obj_t) CNST_TABLE_REF(83)), BUNSPEC);
				((((BgL_rtl_lightfuncallz00_bglt)
							COBJECT(((BgL_rtl_lightfuncallz00_bglt) BgL_new1301z00_8114)))->
						BgL_funsz00) = ((obj_t) BNIL), BUNSPEC);
				((((BgL_rtl_lightfuncallz00_bglt)
							COBJECT(((BgL_rtl_lightfuncallz00_bglt) BgL_new1301z00_8114)))->
						BgL_rettypez00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_13791 = ((BgL_rtl_lightfuncallz00_bglt) BgL_new1301z00_8114);
				return ((obj_t) BgL_auxz00_13791);
			}
		}

	}



/* &lambda2597 */
	BgL_rtl_lightfuncallz00_bglt BGl_z62lambda2597z62zzsaw_defsz00(obj_t
		BgL_envz00_8115)
	{
		{	/* SawMill/defs.scm 62 */
			{	/* SawMill/defs.scm 62 */
				BgL_rtl_lightfuncallz00_bglt BgL_new1300z00_9642;

				BgL_new1300z00_9642 =
					((BgL_rtl_lightfuncallz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_lightfuncallz00_bgl))));
				{	/* SawMill/defs.scm 62 */
					long BgL_arg2599z00_9643;

					BgL_arg2599z00_9643 =
						BGL_CLASS_NUM(BGl_rtl_lightfuncallz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1300z00_9642), BgL_arg2599z00_9643);
				}
				return BgL_new1300z00_9642;
			}
		}

	}



/* &lambda2595 */
	BgL_rtl_lightfuncallz00_bglt BGl_z62lambda2595z62zzsaw_defsz00(obj_t
		BgL_envz00_8116, obj_t BgL_loc1296z00_8117, obj_t BgL_name1297z00_8118,
		obj_t BgL_funs1298z00_8119, obj_t BgL_rettype1299z00_8120)
	{
		{	/* SawMill/defs.scm 62 */
			{	/* SawMill/defs.scm 62 */
				BgL_rtl_lightfuncallz00_bglt BgL_new1532z00_9646;

				{	/* SawMill/defs.scm 62 */
					BgL_rtl_lightfuncallz00_bglt BgL_new1531z00_9647;

					BgL_new1531z00_9647 =
						((BgL_rtl_lightfuncallz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_lightfuncallz00_bgl))));
					{	/* SawMill/defs.scm 62 */
						long BgL_arg2596z00_9648;

						BgL_arg2596z00_9648 =
							BGL_CLASS_NUM(BGl_rtl_lightfuncallz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1531z00_9647), BgL_arg2596z00_9648);
					}
					BgL_new1532z00_9646 = BgL_new1531z00_9647;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1532z00_9646)))->BgL_locz00) =
					((obj_t) BgL_loc1296z00_8117), BUNSPEC);
				((((BgL_rtl_lightfuncallz00_bglt) COBJECT(BgL_new1532z00_9646))->
						BgL_namez00) = ((obj_t) ((obj_t) BgL_name1297z00_8118)), BUNSPEC);
				((((BgL_rtl_lightfuncallz00_bglt) COBJECT(BgL_new1532z00_9646))->
						BgL_funsz00) = ((obj_t) ((obj_t) BgL_funs1298z00_8119)), BUNSPEC);
				((((BgL_rtl_lightfuncallz00_bglt) COBJECT(BgL_new1532z00_9646))->
						BgL_rettypez00) = ((obj_t) BgL_rettype1299z00_8120), BUNSPEC);
				return BgL_new1532z00_9646;
			}
		}

	}



/* &lambda2617 */
	obj_t BGl_z62lambda2617z62zzsaw_defsz00(obj_t BgL_envz00_8121,
		obj_t BgL_oz00_8122, obj_t BgL_vz00_8123)
	{
		{	/* SawMill/defs.scm 62 */
			return
				((((BgL_rtl_lightfuncallz00_bglt) COBJECT(
							((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_8122)))->
					BgL_rettypez00) = ((obj_t) BgL_vz00_8123), BUNSPEC);
		}

	}



/* &lambda2616 */
	obj_t BGl_z62lambda2616z62zzsaw_defsz00(obj_t BgL_envz00_8124,
		obj_t BgL_oz00_8125)
	{
		{	/* SawMill/defs.scm 62 */
			return
				(((BgL_rtl_lightfuncallz00_bglt) COBJECT(
						((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_8125)))->BgL_rettypez00);
		}

	}



/* &lambda2612 */
	obj_t BGl_z62lambda2612z62zzsaw_defsz00(obj_t BgL_envz00_8126,
		obj_t BgL_oz00_8127, obj_t BgL_vz00_8128)
	{
		{	/* SawMill/defs.scm 62 */
			return
				((((BgL_rtl_lightfuncallz00_bglt) COBJECT(
							((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_8127)))->BgL_funsz00) =
				((obj_t) ((obj_t) BgL_vz00_8128)), BUNSPEC);
		}

	}



/* &lambda2611 */
	obj_t BGl_z62lambda2611z62zzsaw_defsz00(obj_t BgL_envz00_8129,
		obj_t BgL_oz00_8130)
	{
		{	/* SawMill/defs.scm 62 */
			return
				(((BgL_rtl_lightfuncallz00_bglt) COBJECT(
						((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_8130)))->BgL_funsz00);
		}

	}



/* &lambda2607 */
	obj_t BGl_z62lambda2607z62zzsaw_defsz00(obj_t BgL_envz00_8131,
		obj_t BgL_oz00_8132, obj_t BgL_vz00_8133)
	{
		{	/* SawMill/defs.scm 62 */
			return
				((((BgL_rtl_lightfuncallz00_bglt) COBJECT(
							((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_8132)))->BgL_namez00) =
				((obj_t) ((obj_t) BgL_vz00_8133)), BUNSPEC);
		}

	}



/* &lambda2606 */
	obj_t BGl_z62lambda2606z62zzsaw_defsz00(obj_t BgL_envz00_8134,
		obj_t BgL_oz00_8135)
	{
		{	/* SawMill/defs.scm 62 */
			return
				(((BgL_rtl_lightfuncallz00_bglt) COBJECT(
						((BgL_rtl_lightfuncallz00_bglt) BgL_oz00_8135)))->BgL_namez00);
		}

	}



/* &<@anonymous:2587> */
	obj_t BGl_z62zc3z04anonymousza32587ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8136,
		obj_t BgL_new1294z00_8137)
	{
		{	/* SawMill/defs.scm 61 */
			{
				BgL_rtl_applyz00_bglt BgL_auxz00_13833;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_applyz00_bglt) BgL_new1294z00_8137))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_13833 = ((BgL_rtl_applyz00_bglt) BgL_new1294z00_8137);
				return ((obj_t) BgL_auxz00_13833);
			}
		}

	}



/* &lambda2585 */
	BgL_rtl_applyz00_bglt BGl_z62lambda2585z62zzsaw_defsz00(obj_t BgL_envz00_8138)
	{
		{	/* SawMill/defs.scm 61 */
			{	/* SawMill/defs.scm 61 */
				BgL_rtl_applyz00_bglt BgL_new1293z00_9658;

				BgL_new1293z00_9658 =
					((BgL_rtl_applyz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_applyz00_bgl))));
				{	/* SawMill/defs.scm 61 */
					long BgL_arg2586z00_9659;

					BgL_arg2586z00_9659 = BGL_CLASS_NUM(BGl_rtl_applyz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1293z00_9658), BgL_arg2586z00_9659);
				}
				return BgL_new1293z00_9658;
			}
		}

	}



/* &lambda2580 */
	BgL_rtl_applyz00_bglt BGl_z62lambda2580z62zzsaw_defsz00(obj_t BgL_envz00_8139,
		obj_t BgL_loc1292z00_8140)
	{
		{	/* SawMill/defs.scm 61 */
			{	/* SawMill/defs.scm 61 */
				BgL_rtl_applyz00_bglt BgL_new1530z00_9660;

				{	/* SawMill/defs.scm 61 */
					BgL_rtl_applyz00_bglt BgL_new1529z00_9661;

					BgL_new1529z00_9661 =
						((BgL_rtl_applyz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_applyz00_bgl))));
					{	/* SawMill/defs.scm 61 */
						long BgL_arg2584z00_9662;

						BgL_arg2584z00_9662 = BGL_CLASS_NUM(BGl_rtl_applyz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1529z00_9661), BgL_arg2584z00_9662);
					}
					BgL_new1530z00_9660 = BgL_new1529z00_9661;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1530z00_9660)))->BgL_locz00) =
					((obj_t) BgL_loc1292z00_8140), BUNSPEC);
				return BgL_new1530z00_9660;
			}
		}

	}



/* &<@anonymous:2561> */
	obj_t BGl_z62zc3z04anonymousza32561ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8141,
		obj_t BgL_new1290z00_8142)
	{
		{	/* SawMill/defs.scm 60 */
			{
				BgL_rtl_callz00_bglt BgL_auxz00_13849;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_callz00_bglt) BgL_new1290z00_8142))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_globalz00_bglt BgL_auxz00_13853;

					{	/* SawMill/defs.scm 60 */
						obj_t BgL_classz00_9664;

						BgL_classz00_9664 = BGl_globalz00zzast_varz00;
						{	/* SawMill/defs.scm 60 */
							obj_t BgL__ortest_1117z00_9665;

							BgL__ortest_1117z00_9665 = BGL_CLASS_NIL(BgL_classz00_9664);
							if (CBOOL(BgL__ortest_1117z00_9665))
								{	/* SawMill/defs.scm 60 */
									BgL_auxz00_13853 =
										((BgL_globalz00_bglt) BgL__ortest_1117z00_9665);
								}
							else
								{	/* SawMill/defs.scm 60 */
									BgL_auxz00_13853 =
										((BgL_globalz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9664));
								}
						}
					}
					((((BgL_rtl_callz00_bglt) COBJECT(
									((BgL_rtl_callz00_bglt) BgL_new1290z00_8142)))->BgL_varz00) =
						((BgL_globalz00_bglt) BgL_auxz00_13853), BUNSPEC);
				}
				BgL_auxz00_13849 = ((BgL_rtl_callz00_bglt) BgL_new1290z00_8142);
				return ((obj_t) BgL_auxz00_13849);
			}
		}

	}



/* &lambda2557 */
	BgL_rtl_callz00_bglt BGl_z62lambda2557z62zzsaw_defsz00(obj_t BgL_envz00_8143)
	{
		{	/* SawMill/defs.scm 60 */
			{	/* SawMill/defs.scm 60 */
				BgL_rtl_callz00_bglt BgL_new1289z00_9666;

				BgL_new1289z00_9666 =
					((BgL_rtl_callz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_callz00_bgl))));
				{	/* SawMill/defs.scm 60 */
					long BgL_arg2560z00_9667;

					BgL_arg2560z00_9667 = BGL_CLASS_NUM(BGl_rtl_callz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1289z00_9666), BgL_arg2560z00_9667);
				}
				return BgL_new1289z00_9666;
			}
		}

	}



/* &lambda2555 */
	BgL_rtl_callz00_bglt BGl_z62lambda2555z62zzsaw_defsz00(obj_t BgL_envz00_8144,
		obj_t BgL_loc1287z00_8145, obj_t BgL_var1288z00_8146)
	{
		{	/* SawMill/defs.scm 60 */
			{	/* SawMill/defs.scm 60 */
				BgL_rtl_callz00_bglt BgL_new1528z00_9669;

				{	/* SawMill/defs.scm 60 */
					BgL_rtl_callz00_bglt BgL_new1527z00_9670;

					BgL_new1527z00_9670 =
						((BgL_rtl_callz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_callz00_bgl))));
					{	/* SawMill/defs.scm 60 */
						long BgL_arg2556z00_9671;

						BgL_arg2556z00_9671 = BGL_CLASS_NUM(BGl_rtl_callz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1527z00_9670), BgL_arg2556z00_9671);
					}
					BgL_new1528z00_9669 = BgL_new1527z00_9670;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1528z00_9669)))->BgL_locz00) =
					((obj_t) BgL_loc1287z00_8145), BUNSPEC);
				((((BgL_rtl_callz00_bglt) COBJECT(BgL_new1528z00_9669))->BgL_varz00) =
					((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_var1288z00_8146)),
					BUNSPEC);
				return BgL_new1528z00_9669;
			}
		}

	}



/* &lambda2566 */
	obj_t BGl_z62lambda2566z62zzsaw_defsz00(obj_t BgL_envz00_8147,
		obj_t BgL_oz00_8148, obj_t BgL_vz00_8149)
	{
		{	/* SawMill/defs.scm 60 */
			return
				((((BgL_rtl_callz00_bglt) COBJECT(
							((BgL_rtl_callz00_bglt) BgL_oz00_8148)))->BgL_varz00) =
				((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_vz00_8149)), BUNSPEC);
		}

	}



/* &lambda2565 */
	BgL_globalz00_bglt BGl_z62lambda2565z62zzsaw_defsz00(obj_t BgL_envz00_8150,
		obj_t BgL_oz00_8151)
	{
		{	/* SawMill/defs.scm 60 */
			return
				(((BgL_rtl_callz00_bglt) COBJECT(
						((BgL_rtl_callz00_bglt) BgL_oz00_8151)))->BgL_varz00);
		}

	}



/* &<@anonymous:2537> */
	obj_t BGl_z62zc3z04anonymousza32537ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8152,
		obj_t BgL_new1285z00_8153)
	{
		{	/* SawMill/defs.scm 59 */
			{
				BgL_rtl_newz00_bglt BgL_auxz00_13881;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_newz00_bglt) BgL_new1285z00_8153))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_13885;

					{	/* SawMill/defs.scm 59 */
						obj_t BgL_classz00_9676;

						BgL_classz00_9676 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 59 */
							obj_t BgL__ortest_1117z00_9677;

							BgL__ortest_1117z00_9677 = BGL_CLASS_NIL(BgL_classz00_9676);
							if (CBOOL(BgL__ortest_1117z00_9677))
								{	/* SawMill/defs.scm 59 */
									BgL_auxz00_13885 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9677);
								}
							else
								{	/* SawMill/defs.scm 59 */
									BgL_auxz00_13885 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9676));
								}
						}
					}
					((((BgL_rtl_newz00_bglt) COBJECT(
									((BgL_rtl_newz00_bglt) BgL_new1285z00_8153)))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_13885), BUNSPEC);
				}
				((((BgL_rtl_newz00_bglt) COBJECT(
								((BgL_rtl_newz00_bglt) BgL_new1285z00_8153)))->BgL_constrz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_13881 = ((BgL_rtl_newz00_bglt) BgL_new1285z00_8153);
				return ((obj_t) BgL_auxz00_13881);
			}
		}

	}



/* &lambda2535 */
	BgL_rtl_newz00_bglt BGl_z62lambda2535z62zzsaw_defsz00(obj_t BgL_envz00_8154)
	{
		{	/* SawMill/defs.scm 59 */
			{	/* SawMill/defs.scm 59 */
				BgL_rtl_newz00_bglt BgL_new1284z00_9678;

				BgL_new1284z00_9678 =
					((BgL_rtl_newz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_newz00_bgl))));
				{	/* SawMill/defs.scm 59 */
					long BgL_arg2536z00_9679;

					BgL_arg2536z00_9679 = BGL_CLASS_NUM(BGl_rtl_newz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1284z00_9678), BgL_arg2536z00_9679);
				}
				return BgL_new1284z00_9678;
			}
		}

	}



/* &lambda2529 */
	BgL_rtl_newz00_bglt BGl_z62lambda2529z62zzsaw_defsz00(obj_t BgL_envz00_8155,
		obj_t BgL_loc1281z00_8156, obj_t BgL_type1282z00_8157,
		obj_t BgL_constr1283z00_8158)
	{
		{	/* SawMill/defs.scm 59 */
			{	/* SawMill/defs.scm 59 */
				BgL_rtl_newz00_bglt BgL_new1526z00_9682;

				{	/* SawMill/defs.scm 59 */
					BgL_rtl_newz00_bglt BgL_new1525z00_9683;

					BgL_new1525z00_9683 =
						((BgL_rtl_newz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_newz00_bgl))));
					{	/* SawMill/defs.scm 59 */
						long BgL_arg2534z00_9684;

						BgL_arg2534z00_9684 = BGL_CLASS_NUM(BGl_rtl_newz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1525z00_9683), BgL_arg2534z00_9684);
					}
					BgL_new1526z00_9682 = BgL_new1525z00_9683;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1526z00_9682)))->BgL_locz00) =
					((obj_t) BgL_loc1281z00_8156), BUNSPEC);
				((((BgL_rtl_newz00_bglt) COBJECT(BgL_new1526z00_9682))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1282z00_8157)),
					BUNSPEC);
				((((BgL_rtl_newz00_bglt) COBJECT(BgL_new1526z00_9682))->BgL_constrz00) =
					((obj_t) ((obj_t) BgL_constr1283z00_8158)), BUNSPEC);
				return BgL_new1526z00_9682;
			}
		}

	}



/* &lambda2548 */
	obj_t BGl_z62lambda2548z62zzsaw_defsz00(obj_t BgL_envz00_8159,
		obj_t BgL_oz00_8160, obj_t BgL_vz00_8161)
	{
		{	/* SawMill/defs.scm 59 */
			return
				((((BgL_rtl_newz00_bglt) COBJECT(
							((BgL_rtl_newz00_bglt) BgL_oz00_8160)))->BgL_constrz00) = ((obj_t)
					((obj_t) BgL_vz00_8161)), BUNSPEC);
		}

	}



/* &lambda2547 */
	obj_t BGl_z62lambda2547z62zzsaw_defsz00(obj_t BgL_envz00_8162,
		obj_t BgL_oz00_8163)
	{
		{	/* SawMill/defs.scm 59 */
			return
				(((BgL_rtl_newz00_bglt) COBJECT(
						((BgL_rtl_newz00_bglt) BgL_oz00_8163)))->BgL_constrz00);
		}

	}



/* &lambda2542 */
	obj_t BGl_z62lambda2542z62zzsaw_defsz00(obj_t BgL_envz00_8164,
		obj_t BgL_oz00_8165, obj_t BgL_vz00_8166)
	{
		{	/* SawMill/defs.scm 59 */
			return
				((((BgL_rtl_newz00_bglt) COBJECT(
							((BgL_rtl_newz00_bglt) BgL_oz00_8165)))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8166)), BUNSPEC);
		}

	}



/* &lambda2541 */
	BgL_typez00_bglt BGl_z62lambda2541z62zzsaw_defsz00(obj_t BgL_envz00_8167,
		obj_t BgL_oz00_8168)
	{
		{	/* SawMill/defs.scm 59 */
			return
				(((BgL_rtl_newz00_bglt) COBJECT(
						((BgL_rtl_newz00_bglt) BgL_oz00_8168)))->BgL_typez00);
		}

	}



/* &<@anonymous:2522> */
	obj_t BGl_z62zc3z04anonymousza32522ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8169,
		obj_t BgL_new1279z00_8170)
	{
		{	/* SawMill/defs.scm 57 */
			{
				BgL_rtl_boxsetz00_bglt BgL_auxz00_13922;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_boxsetz00_bglt) BgL_new1279z00_8170))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_13922 = ((BgL_rtl_boxsetz00_bglt) BgL_new1279z00_8170);
				return ((obj_t) BgL_auxz00_13922);
			}
		}

	}



/* &lambda2519 */
	BgL_rtl_boxsetz00_bglt BGl_z62lambda2519z62zzsaw_defsz00(obj_t
		BgL_envz00_8171)
	{
		{	/* SawMill/defs.scm 57 */
			{	/* SawMill/defs.scm 57 */
				BgL_rtl_boxsetz00_bglt BgL_new1277z00_9692;

				BgL_new1277z00_9692 =
					((BgL_rtl_boxsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_boxsetz00_bgl))));
				{	/* SawMill/defs.scm 57 */
					long BgL_arg2521z00_9693;

					BgL_arg2521z00_9693 = BGL_CLASS_NUM(BGl_rtl_boxsetz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1277z00_9692), BgL_arg2521z00_9693);
				}
				return BgL_new1277z00_9692;
			}
		}

	}



/* &lambda2516 */
	BgL_rtl_boxsetz00_bglt BGl_z62lambda2516z62zzsaw_defsz00(obj_t
		BgL_envz00_8172, obj_t BgL_loc1276z00_8173)
	{
		{	/* SawMill/defs.scm 57 */
			{	/* SawMill/defs.scm 57 */
				BgL_rtl_boxsetz00_bglt BgL_new1524z00_9694;

				{	/* SawMill/defs.scm 57 */
					BgL_rtl_boxsetz00_bglt BgL_new1523z00_9695;

					BgL_new1523z00_9695 =
						((BgL_rtl_boxsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_boxsetz00_bgl))));
					{	/* SawMill/defs.scm 57 */
						long BgL_arg2518z00_9696;

						BgL_arg2518z00_9696 = BGL_CLASS_NUM(BGl_rtl_boxsetz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1523z00_9695), BgL_arg2518z00_9696);
					}
					BgL_new1524z00_9694 = BgL_new1523z00_9695;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1524z00_9694)))->BgL_locz00) =
					((obj_t) BgL_loc1276z00_8173), BUNSPEC);
				return BgL_new1524z00_9694;
			}
		}

	}



/* &<@anonymous:2496> */
	obj_t BGl_z62zc3z04anonymousza32496ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8174,
		obj_t BgL_new1274z00_8175)
	{
		{	/* SawMill/defs.scm 56 */
			{
				BgL_rtl_vsetz00_bglt BgL_auxz00_13938;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_vsetz00_bglt) BgL_new1274z00_8175))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_13942;

					{	/* SawMill/defs.scm 56 */
						obj_t BgL_classz00_9698;

						BgL_classz00_9698 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 56 */
							obj_t BgL__ortest_1117z00_9699;

							BgL__ortest_1117z00_9699 = BGL_CLASS_NIL(BgL_classz00_9698);
							if (CBOOL(BgL__ortest_1117z00_9699))
								{	/* SawMill/defs.scm 56 */
									BgL_auxz00_13942 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9699);
								}
							else
								{	/* SawMill/defs.scm 56 */
									BgL_auxz00_13942 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9698));
								}
						}
					}
					((((BgL_rtl_vsetz00_bglt) COBJECT(
									((BgL_rtl_vsetz00_bglt) BgL_new1274z00_8175)))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_13942), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_13951;

					{	/* SawMill/defs.scm 56 */
						obj_t BgL_classz00_9700;

						BgL_classz00_9700 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 56 */
							obj_t BgL__ortest_1117z00_9701;

							BgL__ortest_1117z00_9701 = BGL_CLASS_NIL(BgL_classz00_9700);
							if (CBOOL(BgL__ortest_1117z00_9701))
								{	/* SawMill/defs.scm 56 */
									BgL_auxz00_13951 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9701);
								}
							else
								{	/* SawMill/defs.scm 56 */
									BgL_auxz00_13951 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9700));
								}
						}
					}
					((((BgL_rtl_vsetz00_bglt) COBJECT(
									((BgL_rtl_vsetz00_bglt) BgL_new1274z00_8175)))->
							BgL_vtypez00) = ((BgL_typez00_bglt) BgL_auxz00_13951), BUNSPEC);
				}
				BgL_auxz00_13938 = ((BgL_rtl_vsetz00_bglt) BgL_new1274z00_8175);
				return ((obj_t) BgL_auxz00_13938);
			}
		}

	}



/* &lambda2493 */
	BgL_rtl_vsetz00_bglt BGl_z62lambda2493z62zzsaw_defsz00(obj_t BgL_envz00_8176)
	{
		{	/* SawMill/defs.scm 56 */
			{	/* SawMill/defs.scm 56 */
				BgL_rtl_vsetz00_bglt BgL_new1273z00_9702;

				BgL_new1273z00_9702 =
					((BgL_rtl_vsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_vsetz00_bgl))));
				{	/* SawMill/defs.scm 56 */
					long BgL_arg2495z00_9703;

					BgL_arg2495z00_9703 = BGL_CLASS_NUM(BGl_rtl_vsetz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1273z00_9702), BgL_arg2495z00_9703);
				}
				return BgL_new1273z00_9702;
			}
		}

	}



/* &lambda2491 */
	BgL_rtl_vsetz00_bglt BGl_z62lambda2491z62zzsaw_defsz00(obj_t BgL_envz00_8177,
		obj_t BgL_loc1270z00_8178, obj_t BgL_type1271z00_8179,
		obj_t BgL_vtype1272z00_8180)
	{
		{	/* SawMill/defs.scm 56 */
			{	/* SawMill/defs.scm 56 */
				BgL_rtl_vsetz00_bglt BgL_new1522z00_9706;

				{	/* SawMill/defs.scm 56 */
					BgL_rtl_vsetz00_bglt BgL_new1521z00_9707;

					BgL_new1521z00_9707 =
						((BgL_rtl_vsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_vsetz00_bgl))));
					{	/* SawMill/defs.scm 56 */
						long BgL_arg2492z00_9708;

						BgL_arg2492z00_9708 = BGL_CLASS_NUM(BGl_rtl_vsetz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1521z00_9707), BgL_arg2492z00_9708);
					}
					BgL_new1522z00_9706 = BgL_new1521z00_9707;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1522z00_9706)))->BgL_locz00) =
					((obj_t) BgL_loc1270z00_8178), BUNSPEC);
				((((BgL_rtl_vsetz00_bglt) COBJECT(BgL_new1522z00_9706))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1271z00_8179)),
					BUNSPEC);
				((((BgL_rtl_vsetz00_bglt) COBJECT(BgL_new1522z00_9706))->BgL_vtypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vtype1272z00_8180)),
					BUNSPEC);
				return BgL_new1522z00_9706;
			}
		}

	}



/* &lambda2510 */
	obj_t BGl_z62lambda2510z62zzsaw_defsz00(obj_t BgL_envz00_8181,
		obj_t BgL_oz00_8182, obj_t BgL_vz00_8183)
	{
		{	/* SawMill/defs.scm 56 */
			return
				((((BgL_rtl_vsetz00_bglt) COBJECT(
							((BgL_rtl_vsetz00_bglt) BgL_oz00_8182)))->BgL_vtypez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8183)), BUNSPEC);
		}

	}



/* &lambda2509 */
	BgL_typez00_bglt BGl_z62lambda2509z62zzsaw_defsz00(obj_t BgL_envz00_8184,
		obj_t BgL_oz00_8185)
	{
		{	/* SawMill/defs.scm 56 */
			return
				(((BgL_rtl_vsetz00_bglt) COBJECT(
						((BgL_rtl_vsetz00_bglt) BgL_oz00_8185)))->BgL_vtypez00);
		}

	}



/* &lambda2503 */
	obj_t BGl_z62lambda2503z62zzsaw_defsz00(obj_t BgL_envz00_8186,
		obj_t BgL_oz00_8187, obj_t BgL_vz00_8188)
	{
		{	/* SawMill/defs.scm 56 */
			return
				((((BgL_rtl_vsetz00_bglt) COBJECT(
							((BgL_rtl_vsetz00_bglt) BgL_oz00_8187)))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8188)), BUNSPEC);
		}

	}



/* &lambda2502 */
	BgL_typez00_bglt BGl_z62lambda2502z62zzsaw_defsz00(obj_t BgL_envz00_8189,
		obj_t BgL_oz00_8190)
	{
		{	/* SawMill/defs.scm 56 */
			return
				(((BgL_rtl_vsetz00_bglt) COBJECT(
						((BgL_rtl_vsetz00_bglt) BgL_oz00_8190)))->BgL_typez00);
		}

	}



/* &<@anonymous:2464> */
	obj_t BGl_z62zc3z04anonymousza32464ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8191,
		obj_t BgL_new1268z00_8192)
	{
		{	/* SawMill/defs.scm 55 */
			{
				BgL_rtl_setfieldz00_bglt BgL_auxz00_13986;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_setfieldz00_bglt) BgL_new1268z00_8192))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_setfieldz00_bglt) COBJECT(((BgL_rtl_setfieldz00_bglt)
									BgL_new1268z00_8192)))->BgL_namez00) =
					((obj_t) BGl_string3354z00zzsaw_defsz00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_13992;

					{	/* SawMill/defs.scm 55 */
						obj_t BgL_classz00_9716;

						BgL_classz00_9716 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 55 */
							obj_t BgL__ortest_1117z00_9717;

							BgL__ortest_1117z00_9717 = BGL_CLASS_NIL(BgL_classz00_9716);
							if (CBOOL(BgL__ortest_1117z00_9717))
								{	/* SawMill/defs.scm 55 */
									BgL_auxz00_13992 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9717);
								}
							else
								{	/* SawMill/defs.scm 55 */
									BgL_auxz00_13992 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9716));
								}
						}
					}
					((((BgL_rtl_setfieldz00_bglt) COBJECT(
									((BgL_rtl_setfieldz00_bglt) BgL_new1268z00_8192)))->
							BgL_objtypez00) = ((BgL_typez00_bglt) BgL_auxz00_13992), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_14001;

					{	/* SawMill/defs.scm 55 */
						obj_t BgL_classz00_9718;

						BgL_classz00_9718 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 55 */
							obj_t BgL__ortest_1117z00_9719;

							BgL__ortest_1117z00_9719 = BGL_CLASS_NIL(BgL_classz00_9718);
							if (CBOOL(BgL__ortest_1117z00_9719))
								{	/* SawMill/defs.scm 55 */
									BgL_auxz00_14001 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9719);
								}
							else
								{	/* SawMill/defs.scm 55 */
									BgL_auxz00_14001 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9718));
								}
						}
					}
					((((BgL_rtl_setfieldz00_bglt) COBJECT(
									((BgL_rtl_setfieldz00_bglt) BgL_new1268z00_8192)))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_14001), BUNSPEC);
				}
				BgL_auxz00_13986 = ((BgL_rtl_setfieldz00_bglt) BgL_new1268z00_8192);
				return ((obj_t) BgL_auxz00_13986);
			}
		}

	}



/* &lambda2462 */
	BgL_rtl_setfieldz00_bglt BGl_z62lambda2462z62zzsaw_defsz00(obj_t
		BgL_envz00_8193)
	{
		{	/* SawMill/defs.scm 55 */
			{	/* SawMill/defs.scm 55 */
				BgL_rtl_setfieldz00_bglt BgL_new1267z00_9720;

				BgL_new1267z00_9720 =
					((BgL_rtl_setfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_setfieldz00_bgl))));
				{	/* SawMill/defs.scm 55 */
					long BgL_arg2463z00_9721;

					BgL_arg2463z00_9721 = BGL_CLASS_NUM(BGl_rtl_setfieldz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1267z00_9720), BgL_arg2463z00_9721);
				}
				return BgL_new1267z00_9720;
			}
		}

	}



/* &lambda2460 */
	BgL_rtl_setfieldz00_bglt BGl_z62lambda2460z62zzsaw_defsz00(obj_t
		BgL_envz00_8194, obj_t BgL_loc1263z00_8195, obj_t BgL_name1264z00_8196,
		obj_t BgL_objtype1265z00_8197, obj_t BgL_type1266z00_8198)
	{
		{	/* SawMill/defs.scm 55 */
			{	/* SawMill/defs.scm 55 */
				BgL_rtl_setfieldz00_bglt BgL_new1520z00_9725;

				{	/* SawMill/defs.scm 55 */
					BgL_rtl_setfieldz00_bglt BgL_new1519z00_9726;

					BgL_new1519z00_9726 =
						((BgL_rtl_setfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_setfieldz00_bgl))));
					{	/* SawMill/defs.scm 55 */
						long BgL_arg2461z00_9727;

						BgL_arg2461z00_9727 =
							BGL_CLASS_NUM(BGl_rtl_setfieldz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1519z00_9726), BgL_arg2461z00_9727);
					}
					BgL_new1520z00_9725 = BgL_new1519z00_9726;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1520z00_9725)))->BgL_locz00) =
					((obj_t) BgL_loc1263z00_8195), BUNSPEC);
				((((BgL_rtl_setfieldz00_bglt) COBJECT(BgL_new1520z00_9725))->
						BgL_namez00) = ((obj_t) ((obj_t) BgL_name1264z00_8196)), BUNSPEC);
				((((BgL_rtl_setfieldz00_bglt) COBJECT(BgL_new1520z00_9725))->
						BgL_objtypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_objtype1265z00_8197)),
					BUNSPEC);
				((((BgL_rtl_setfieldz00_bglt) COBJECT(BgL_new1520z00_9725))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1266z00_8198)),
					BUNSPEC);
				return BgL_new1520z00_9725;
			}
		}

	}



/* &lambda2483 */
	obj_t BGl_z62lambda2483z62zzsaw_defsz00(obj_t BgL_envz00_8199,
		obj_t BgL_oz00_8200, obj_t BgL_vz00_8201)
	{
		{	/* SawMill/defs.scm 55 */
			return
				((((BgL_rtl_setfieldz00_bglt) COBJECT(
							((BgL_rtl_setfieldz00_bglt) BgL_oz00_8200)))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8201)), BUNSPEC);
		}

	}



/* &lambda2482 */
	BgL_typez00_bglt BGl_z62lambda2482z62zzsaw_defsz00(obj_t BgL_envz00_8202,
		obj_t BgL_oz00_8203)
	{
		{	/* SawMill/defs.scm 55 */
			return
				(((BgL_rtl_setfieldz00_bglt) COBJECT(
						((BgL_rtl_setfieldz00_bglt) BgL_oz00_8203)))->BgL_typez00);
		}

	}



/* &lambda2477 */
	obj_t BGl_z62lambda2477z62zzsaw_defsz00(obj_t BgL_envz00_8204,
		obj_t BgL_oz00_8205, obj_t BgL_vz00_8206)
	{
		{	/* SawMill/defs.scm 55 */
			return
				((((BgL_rtl_setfieldz00_bglt) COBJECT(
							((BgL_rtl_setfieldz00_bglt) BgL_oz00_8205)))->BgL_objtypez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8206)), BUNSPEC);
		}

	}



/* &lambda2476 */
	BgL_typez00_bglt BGl_z62lambda2476z62zzsaw_defsz00(obj_t BgL_envz00_8207,
		obj_t BgL_oz00_8208)
	{
		{	/* SawMill/defs.scm 55 */
			return
				(((BgL_rtl_setfieldz00_bglt) COBJECT(
						((BgL_rtl_setfieldz00_bglt) BgL_oz00_8208)))->BgL_objtypez00);
		}

	}



/* &lambda2471 */
	obj_t BGl_z62lambda2471z62zzsaw_defsz00(obj_t BgL_envz00_8209,
		obj_t BgL_oz00_8210, obj_t BgL_vz00_8211)
	{
		{	/* SawMill/defs.scm 55 */
			return
				((((BgL_rtl_setfieldz00_bglt) COBJECT(
							((BgL_rtl_setfieldz00_bglt) BgL_oz00_8210)))->BgL_namez00) =
				((obj_t) ((obj_t) BgL_vz00_8211)), BUNSPEC);
		}

	}



/* &lambda2470 */
	obj_t BGl_z62lambda2470z62zzsaw_defsz00(obj_t BgL_envz00_8212,
		obj_t BgL_oz00_8213)
	{
		{	/* SawMill/defs.scm 55 */
			return
				(((BgL_rtl_setfieldz00_bglt) COBJECT(
						((BgL_rtl_setfieldz00_bglt) BgL_oz00_8213)))->BgL_namez00);
		}

	}



/* &<@anonymous:2448> */
	obj_t BGl_z62zc3z04anonymousza32448ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8214,
		obj_t BgL_new1261z00_8215)
	{
		{	/* SawMill/defs.scm 54 */
			{
				BgL_rtl_storegz00_bglt BgL_auxz00_14043;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_storegz00_bglt) BgL_new1261z00_8215))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_globalz00_bglt BgL_auxz00_14047;

					{	/* SawMill/defs.scm 54 */
						obj_t BgL_classz00_9738;

						BgL_classz00_9738 = BGl_globalz00zzast_varz00;
						{	/* SawMill/defs.scm 54 */
							obj_t BgL__ortest_1117z00_9739;

							BgL__ortest_1117z00_9739 = BGL_CLASS_NIL(BgL_classz00_9738);
							if (CBOOL(BgL__ortest_1117z00_9739))
								{	/* SawMill/defs.scm 54 */
									BgL_auxz00_14047 =
										((BgL_globalz00_bglt) BgL__ortest_1117z00_9739);
								}
							else
								{	/* SawMill/defs.scm 54 */
									BgL_auxz00_14047 =
										((BgL_globalz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9738));
								}
						}
					}
					((((BgL_rtl_storegz00_bglt) COBJECT(
									((BgL_rtl_storegz00_bglt) BgL_new1261z00_8215)))->
							BgL_varz00) = ((BgL_globalz00_bglt) BgL_auxz00_14047), BUNSPEC);
				}
				BgL_auxz00_14043 = ((BgL_rtl_storegz00_bglt) BgL_new1261z00_8215);
				return ((obj_t) BgL_auxz00_14043);
			}
		}

	}



/* &lambda2446 */
	BgL_rtl_storegz00_bglt BGl_z62lambda2446z62zzsaw_defsz00(obj_t
		BgL_envz00_8216)
	{
		{	/* SawMill/defs.scm 54 */
			{	/* SawMill/defs.scm 54 */
				BgL_rtl_storegz00_bglt BgL_new1260z00_9740;

				BgL_new1260z00_9740 =
					((BgL_rtl_storegz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_storegz00_bgl))));
				{	/* SawMill/defs.scm 54 */
					long BgL_arg2447z00_9741;

					BgL_arg2447z00_9741 = BGL_CLASS_NUM(BGl_rtl_storegz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1260z00_9740), BgL_arg2447z00_9741);
				}
				return BgL_new1260z00_9740;
			}
		}

	}



/* &lambda2444 */
	BgL_rtl_storegz00_bglt BGl_z62lambda2444z62zzsaw_defsz00(obj_t
		BgL_envz00_8217, obj_t BgL_loc1257z00_8218, obj_t BgL_var1258z00_8219)
	{
		{	/* SawMill/defs.scm 54 */
			{	/* SawMill/defs.scm 54 */
				BgL_rtl_storegz00_bglt BgL_new1518z00_9743;

				{	/* SawMill/defs.scm 54 */
					BgL_rtl_storegz00_bglt BgL_new1517z00_9744;

					BgL_new1517z00_9744 =
						((BgL_rtl_storegz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_storegz00_bgl))));
					{	/* SawMill/defs.scm 54 */
						long BgL_arg2445z00_9745;

						BgL_arg2445z00_9745 = BGL_CLASS_NUM(BGl_rtl_storegz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1517z00_9744), BgL_arg2445z00_9745);
					}
					BgL_new1518z00_9743 = BgL_new1517z00_9744;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1518z00_9743)))->BgL_locz00) =
					((obj_t) BgL_loc1257z00_8218), BUNSPEC);
				((((BgL_rtl_storegz00_bglt) COBJECT(BgL_new1518z00_9743))->BgL_varz00) =
					((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_var1258z00_8219)),
					BUNSPEC);
				return BgL_new1518z00_9743;
			}
		}

	}



/* &lambda2453 */
	obj_t BGl_z62lambda2453z62zzsaw_defsz00(obj_t BgL_envz00_8220,
		obj_t BgL_oz00_8221, obj_t BgL_vz00_8222)
	{
		{	/* SawMill/defs.scm 54 */
			return
				((((BgL_rtl_storegz00_bglt) COBJECT(
							((BgL_rtl_storegz00_bglt) BgL_oz00_8221)))->BgL_varz00) =
				((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_vz00_8222)), BUNSPEC);
		}

	}



/* &lambda2452 */
	BgL_globalz00_bglt BGl_z62lambda2452z62zzsaw_defsz00(obj_t BgL_envz00_8223,
		obj_t BgL_oz00_8224)
	{
		{	/* SawMill/defs.scm 54 */
			return
				(((BgL_rtl_storegz00_bglt) COBJECT(
						((BgL_rtl_storegz00_bglt) BgL_oz00_8224)))->BgL_varz00);
		}

	}



/* &<@anonymous:2435> */
	obj_t BGl_z62zc3z04anonymousza32435ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8225,
		obj_t BgL_new1255z00_8226)
	{
		{	/* SawMill/defs.scm 53 */
			{
				BgL_rtl_effectz00_bglt BgL_auxz00_14075;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_effectz00_bglt) BgL_new1255z00_8226))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14075 = ((BgL_rtl_effectz00_bglt) BgL_new1255z00_8226);
				return ((obj_t) BgL_auxz00_14075);
			}
		}

	}



/* &lambda2433 */
	BgL_rtl_effectz00_bglt BGl_z62lambda2433z62zzsaw_defsz00(obj_t
		BgL_envz00_8227)
	{
		{	/* SawMill/defs.scm 53 */
			{	/* SawMill/defs.scm 53 */
				BgL_rtl_effectz00_bglt BgL_new1254z00_9750;

				BgL_new1254z00_9750 =
					((BgL_rtl_effectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_effectz00_bgl))));
				{	/* SawMill/defs.scm 53 */
					long BgL_arg2434z00_9751;

					BgL_arg2434z00_9751 = BGL_CLASS_NUM(BGl_rtl_effectz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1254z00_9750), BgL_arg2434z00_9751);
				}
				return BgL_new1254z00_9750;
			}
		}

	}



/* &lambda2431 */
	BgL_rtl_effectz00_bglt BGl_z62lambda2431z62zzsaw_defsz00(obj_t
		BgL_envz00_8228, obj_t BgL_loc1253z00_8229)
	{
		{	/* SawMill/defs.scm 53 */
			{	/* SawMill/defs.scm 53 */
				BgL_rtl_effectz00_bglt BgL_new1516z00_9752;

				{	/* SawMill/defs.scm 53 */
					BgL_rtl_effectz00_bglt BgL_new1515z00_9753;

					BgL_new1515z00_9753 =
						((BgL_rtl_effectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_effectz00_bgl))));
					{	/* SawMill/defs.scm 53 */
						long BgL_arg2432z00_9754;

						BgL_arg2432z00_9754 = BGL_CLASS_NUM(BGl_rtl_effectz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1515z00_9753), BgL_arg2432z00_9754);
					}
					BgL_new1516z00_9752 = BgL_new1515z00_9753;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1516z00_9752)))->BgL_locz00) =
					((obj_t) BgL_loc1253z00_8229), BUNSPEC);
				return BgL_new1516z00_9752;
			}
		}

	}



/* &<@anonymous:2424> */
	obj_t BGl_z62zc3z04anonymousza32424ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8230,
		obj_t BgL_new1251z00_8231)
	{
		{	/* SawMill/defs.scm 51 */
			{
				BgL_rtl_boxrefz00_bglt BgL_auxz00_14091;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_boxrefz00_bglt) BgL_new1251z00_8231))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14091 = ((BgL_rtl_boxrefz00_bglt) BgL_new1251z00_8231);
				return ((obj_t) BgL_auxz00_14091);
			}
		}

	}



/* &lambda2422 */
	BgL_rtl_boxrefz00_bglt BGl_z62lambda2422z62zzsaw_defsz00(obj_t
		BgL_envz00_8232)
	{
		{	/* SawMill/defs.scm 51 */
			{	/* SawMill/defs.scm 51 */
				BgL_rtl_boxrefz00_bglt BgL_new1249z00_9756;

				BgL_new1249z00_9756 =
					((BgL_rtl_boxrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_boxrefz00_bgl))));
				{	/* SawMill/defs.scm 51 */
					long BgL_arg2423z00_9757;

					BgL_arg2423z00_9757 = BGL_CLASS_NUM(BGl_rtl_boxrefz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1249z00_9756), BgL_arg2423z00_9757);
				}
				return BgL_new1249z00_9756;
			}
		}

	}



/* &lambda2420 */
	BgL_rtl_boxrefz00_bglt BGl_z62lambda2420z62zzsaw_defsz00(obj_t
		BgL_envz00_8233, obj_t BgL_loc1248z00_8234)
	{
		{	/* SawMill/defs.scm 51 */
			{	/* SawMill/defs.scm 51 */
				BgL_rtl_boxrefz00_bglt BgL_new1513z00_9758;

				{	/* SawMill/defs.scm 51 */
					BgL_rtl_boxrefz00_bglt BgL_new1512z00_9759;

					BgL_new1512z00_9759 =
						((BgL_rtl_boxrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_boxrefz00_bgl))));
					{	/* SawMill/defs.scm 51 */
						long BgL_arg2421z00_9760;

						BgL_arg2421z00_9760 = BGL_CLASS_NUM(BGl_rtl_boxrefz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1512z00_9759), BgL_arg2421z00_9760);
					}
					BgL_new1513z00_9758 = BgL_new1512z00_9759;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1513z00_9758)))->BgL_locz00) =
					((obj_t) BgL_loc1248z00_8234), BUNSPEC);
				return BgL_new1513z00_9758;
			}
		}

	}



/* &<@anonymous:2413> */
	obj_t BGl_z62zc3z04anonymousza32413ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8235,
		obj_t BgL_new1246z00_8236)
	{
		{	/* SawMill/defs.scm 50 */
			{
				BgL_rtl_makeboxz00_bglt BgL_auxz00_14107;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_makeboxz00_bglt) BgL_new1246z00_8236))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14107 = ((BgL_rtl_makeboxz00_bglt) BgL_new1246z00_8236);
				return ((obj_t) BgL_auxz00_14107);
			}
		}

	}



/* &lambda2411 */
	BgL_rtl_makeboxz00_bglt BGl_z62lambda2411z62zzsaw_defsz00(obj_t
		BgL_envz00_8237)
	{
		{	/* SawMill/defs.scm 50 */
			{	/* SawMill/defs.scm 50 */
				BgL_rtl_makeboxz00_bglt BgL_new1245z00_9762;

				BgL_new1245z00_9762 =
					((BgL_rtl_makeboxz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_makeboxz00_bgl))));
				{	/* SawMill/defs.scm 50 */
					long BgL_arg2412z00_9763;

					BgL_arg2412z00_9763 = BGL_CLASS_NUM(BGl_rtl_makeboxz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1245z00_9762), BgL_arg2412z00_9763);
				}
				return BgL_new1245z00_9762;
			}
		}

	}



/* &lambda2408 */
	BgL_rtl_makeboxz00_bglt BGl_z62lambda2408z62zzsaw_defsz00(obj_t
		BgL_envz00_8238, obj_t BgL_loc1244z00_8239)
	{
		{	/* SawMill/defs.scm 50 */
			{	/* SawMill/defs.scm 50 */
				BgL_rtl_makeboxz00_bglt BgL_new1511z00_9764;

				{	/* SawMill/defs.scm 50 */
					BgL_rtl_makeboxz00_bglt BgL_new1510z00_9765;

					BgL_new1510z00_9765 =
						((BgL_rtl_makeboxz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_makeboxz00_bgl))));
					{	/* SawMill/defs.scm 50 */
						long BgL_arg2410z00_9766;

						BgL_arg2410z00_9766 =
							BGL_CLASS_NUM(BGl_rtl_makeboxz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1510z00_9765), BgL_arg2410z00_9766);
					}
					BgL_new1511z00_9764 = BgL_new1510z00_9765;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1511z00_9764)))->BgL_locz00) =
					((obj_t) BgL_loc1244z00_8239), BUNSPEC);
				return BgL_new1511z00_9764;
			}
		}

	}



/* &<@anonymous:2396> */
	obj_t BGl_z62zc3z04anonymousza32396ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8240,
		obj_t BgL_new1242z00_8241)
	{
		{	/* SawMill/defs.scm 49 */
			{
				BgL_rtl_instanceofz00_bglt BgL_auxz00_14123;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_instanceofz00_bglt) BgL_new1242z00_8241))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_14127;

					{	/* SawMill/defs.scm 49 */
						obj_t BgL_classz00_9768;

						BgL_classz00_9768 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 49 */
							obj_t BgL__ortest_1117z00_9769;

							BgL__ortest_1117z00_9769 = BGL_CLASS_NIL(BgL_classz00_9768);
							if (CBOOL(BgL__ortest_1117z00_9769))
								{	/* SawMill/defs.scm 49 */
									BgL_auxz00_14127 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9769);
								}
							else
								{	/* SawMill/defs.scm 49 */
									BgL_auxz00_14127 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9768));
								}
						}
					}
					((((BgL_rtl_instanceofz00_bglt) COBJECT(
									((BgL_rtl_instanceofz00_bglt) BgL_new1242z00_8241)))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_14127), BUNSPEC);
				}
				BgL_auxz00_14123 = ((BgL_rtl_instanceofz00_bglt) BgL_new1242z00_8241);
				return ((obj_t) BgL_auxz00_14123);
			}
		}

	}



/* &lambda2394 */
	BgL_rtl_instanceofz00_bglt BGl_z62lambda2394z62zzsaw_defsz00(obj_t
		BgL_envz00_8242)
	{
		{	/* SawMill/defs.scm 49 */
			{	/* SawMill/defs.scm 49 */
				BgL_rtl_instanceofz00_bglt BgL_new1240z00_9770;

				BgL_new1240z00_9770 =
					((BgL_rtl_instanceofz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_instanceofz00_bgl))));
				{	/* SawMill/defs.scm 49 */
					long BgL_arg2395z00_9771;

					BgL_arg2395z00_9771 =
						BGL_CLASS_NUM(BGl_rtl_instanceofz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1240z00_9770), BgL_arg2395z00_9771);
				}
				return BgL_new1240z00_9770;
			}
		}

	}



/* &lambda2392 */
	BgL_rtl_instanceofz00_bglt BGl_z62lambda2392z62zzsaw_defsz00(obj_t
		BgL_envz00_8243, obj_t BgL_loc1238z00_8244, obj_t BgL_type1239z00_8245)
	{
		{	/* SawMill/defs.scm 49 */
			{	/* SawMill/defs.scm 49 */
				BgL_rtl_instanceofz00_bglt BgL_new1509z00_9773;

				{	/* SawMill/defs.scm 49 */
					BgL_rtl_instanceofz00_bglt BgL_new1508z00_9774;

					BgL_new1508z00_9774 =
						((BgL_rtl_instanceofz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_instanceofz00_bgl))));
					{	/* SawMill/defs.scm 49 */
						long BgL_arg2393z00_9775;

						BgL_arg2393z00_9775 =
							BGL_CLASS_NUM(BGl_rtl_instanceofz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1508z00_9774), BgL_arg2393z00_9775);
					}
					BgL_new1509z00_9773 = BgL_new1508z00_9774;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1509z00_9773)))->BgL_locz00) =
					((obj_t) BgL_loc1238z00_8244), BUNSPEC);
				((((BgL_rtl_instanceofz00_bglt) COBJECT(BgL_new1509z00_9773))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1239z00_8245)),
					BUNSPEC);
				return BgL_new1509z00_9773;
			}
		}

	}



/* &lambda2401 */
	obj_t BGl_z62lambda2401z62zzsaw_defsz00(obj_t BgL_envz00_8246,
		obj_t BgL_oz00_8247, obj_t BgL_vz00_8248)
	{
		{	/* SawMill/defs.scm 49 */
			return
				((((BgL_rtl_instanceofz00_bglt) COBJECT(
							((BgL_rtl_instanceofz00_bglt) BgL_oz00_8247)))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8248)), BUNSPEC);
		}

	}



/* &lambda2400 */
	BgL_typez00_bglt BGl_z62lambda2400z62zzsaw_defsz00(obj_t BgL_envz00_8249,
		obj_t BgL_oz00_8250)
	{
		{	/* SawMill/defs.scm 49 */
			return
				(((BgL_rtl_instanceofz00_bglt) COBJECT(
						((BgL_rtl_instanceofz00_bglt) BgL_oz00_8250)))->BgL_typez00);
		}

	}



/* &<@anonymous:2376> */
	obj_t BGl_z62zc3z04anonymousza32376ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8251,
		obj_t BgL_new1236z00_8252)
	{
		{	/* SawMill/defs.scm 48 */
			{
				BgL_rtl_vlengthz00_bglt BgL_auxz00_14155;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_vlengthz00_bglt) BgL_new1236z00_8252))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_14159;

					{	/* SawMill/defs.scm 48 */
						obj_t BgL_classz00_9780;

						BgL_classz00_9780 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 48 */
							obj_t BgL__ortest_1117z00_9781;

							BgL__ortest_1117z00_9781 = BGL_CLASS_NIL(BgL_classz00_9780);
							if (CBOOL(BgL__ortest_1117z00_9781))
								{	/* SawMill/defs.scm 48 */
									BgL_auxz00_14159 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9781);
								}
							else
								{	/* SawMill/defs.scm 48 */
									BgL_auxz00_14159 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9780));
								}
						}
					}
					((((BgL_rtl_vlengthz00_bglt) COBJECT(
									((BgL_rtl_vlengthz00_bglt) BgL_new1236z00_8252)))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_14159), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_14168;

					{	/* SawMill/defs.scm 48 */
						obj_t BgL_classz00_9782;

						BgL_classz00_9782 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 48 */
							obj_t BgL__ortest_1117z00_9783;

							BgL__ortest_1117z00_9783 = BGL_CLASS_NIL(BgL_classz00_9782);
							if (CBOOL(BgL__ortest_1117z00_9783))
								{	/* SawMill/defs.scm 48 */
									BgL_auxz00_14168 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9783);
								}
							else
								{	/* SawMill/defs.scm 48 */
									BgL_auxz00_14168 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9782));
								}
						}
					}
					((((BgL_rtl_vlengthz00_bglt) COBJECT(
									((BgL_rtl_vlengthz00_bglt) BgL_new1236z00_8252)))->
							BgL_vtypez00) = ((BgL_typez00_bglt) BgL_auxz00_14168), BUNSPEC);
				}
				BgL_auxz00_14155 = ((BgL_rtl_vlengthz00_bglt) BgL_new1236z00_8252);
				return ((obj_t) BgL_auxz00_14155);
			}
		}

	}



/* &lambda2374 */
	BgL_rtl_vlengthz00_bglt BGl_z62lambda2374z62zzsaw_defsz00(obj_t
		BgL_envz00_8253)
	{
		{	/* SawMill/defs.scm 48 */
			{	/* SawMill/defs.scm 48 */
				BgL_rtl_vlengthz00_bglt BgL_new1235z00_9784;

				BgL_new1235z00_9784 =
					((BgL_rtl_vlengthz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_vlengthz00_bgl))));
				{	/* SawMill/defs.scm 48 */
					long BgL_arg2375z00_9785;

					BgL_arg2375z00_9785 = BGL_CLASS_NUM(BGl_rtl_vlengthz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1235z00_9784), BgL_arg2375z00_9785);
				}
				return BgL_new1235z00_9784;
			}
		}

	}



/* &lambda2372 */
	BgL_rtl_vlengthz00_bglt BGl_z62lambda2372z62zzsaw_defsz00(obj_t
		BgL_envz00_8254, obj_t BgL_loc1232z00_8255, obj_t BgL_type1233z00_8256,
		obj_t BgL_vtype1234z00_8257)
	{
		{	/* SawMill/defs.scm 48 */
			{	/* SawMill/defs.scm 48 */
				BgL_rtl_vlengthz00_bglt BgL_new1507z00_9788;

				{	/* SawMill/defs.scm 48 */
					BgL_rtl_vlengthz00_bglt BgL_new1506z00_9789;

					BgL_new1506z00_9789 =
						((BgL_rtl_vlengthz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_vlengthz00_bgl))));
					{	/* SawMill/defs.scm 48 */
						long BgL_arg2373z00_9790;

						BgL_arg2373z00_9790 =
							BGL_CLASS_NUM(BGl_rtl_vlengthz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1506z00_9789), BgL_arg2373z00_9790);
					}
					BgL_new1507z00_9788 = BgL_new1506z00_9789;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1507z00_9788)))->BgL_locz00) =
					((obj_t) BgL_loc1232z00_8255), BUNSPEC);
				((((BgL_rtl_vlengthz00_bglt) COBJECT(BgL_new1507z00_9788))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1233z00_8256)),
					BUNSPEC);
				((((BgL_rtl_vlengthz00_bglt) COBJECT(BgL_new1507z00_9788))->
						BgL_vtypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vtype1234z00_8257)),
					BUNSPEC);
				return BgL_new1507z00_9788;
			}
		}

	}



/* &lambda2386 */
	obj_t BGl_z62lambda2386z62zzsaw_defsz00(obj_t BgL_envz00_8258,
		obj_t BgL_oz00_8259, obj_t BgL_vz00_8260)
	{
		{	/* SawMill/defs.scm 48 */
			return
				((((BgL_rtl_vlengthz00_bglt) COBJECT(
							((BgL_rtl_vlengthz00_bglt) BgL_oz00_8259)))->BgL_vtypez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8260)), BUNSPEC);
		}

	}



/* &lambda2385 */
	BgL_typez00_bglt BGl_z62lambda2385z62zzsaw_defsz00(obj_t BgL_envz00_8261,
		obj_t BgL_oz00_8262)
	{
		{	/* SawMill/defs.scm 48 */
			return
				(((BgL_rtl_vlengthz00_bglt) COBJECT(
						((BgL_rtl_vlengthz00_bglt) BgL_oz00_8262)))->BgL_vtypez00);
		}

	}



/* &lambda2381 */
	obj_t BGl_z62lambda2381z62zzsaw_defsz00(obj_t BgL_envz00_8263,
		obj_t BgL_oz00_8264, obj_t BgL_vz00_8265)
	{
		{	/* SawMill/defs.scm 48 */
			return
				((((BgL_rtl_vlengthz00_bglt) COBJECT(
							((BgL_rtl_vlengthz00_bglt) BgL_oz00_8264)))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8265)), BUNSPEC);
		}

	}



/* &lambda2380 */
	BgL_typez00_bglt BGl_z62lambda2380z62zzsaw_defsz00(obj_t BgL_envz00_8266,
		obj_t BgL_oz00_8267)
	{
		{	/* SawMill/defs.scm 48 */
			return
				(((BgL_rtl_vlengthz00_bglt) COBJECT(
						((BgL_rtl_vlengthz00_bglt) BgL_oz00_8267)))->BgL_typez00);
		}

	}



/* &<@anonymous:2355> */
	obj_t BGl_z62zc3z04anonymousza32355ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8268,
		obj_t BgL_new1230z00_8269)
	{
		{	/* SawMill/defs.scm 47 */
			{
				BgL_rtl_vrefz00_bglt BgL_auxz00_14203;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_vrefz00_bglt) BgL_new1230z00_8269))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_14207;

					{	/* SawMill/defs.scm 47 */
						obj_t BgL_classz00_9798;

						BgL_classz00_9798 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 47 */
							obj_t BgL__ortest_1117z00_9799;

							BgL__ortest_1117z00_9799 = BGL_CLASS_NIL(BgL_classz00_9798);
							if (CBOOL(BgL__ortest_1117z00_9799))
								{	/* SawMill/defs.scm 47 */
									BgL_auxz00_14207 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9799);
								}
							else
								{	/* SawMill/defs.scm 47 */
									BgL_auxz00_14207 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9798));
								}
						}
					}
					((((BgL_rtl_vrefz00_bglt) COBJECT(
									((BgL_rtl_vrefz00_bglt) BgL_new1230z00_8269)))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_14207), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_14216;

					{	/* SawMill/defs.scm 47 */
						obj_t BgL_classz00_9800;

						BgL_classz00_9800 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 47 */
							obj_t BgL__ortest_1117z00_9801;

							BgL__ortest_1117z00_9801 = BGL_CLASS_NIL(BgL_classz00_9800);
							if (CBOOL(BgL__ortest_1117z00_9801))
								{	/* SawMill/defs.scm 47 */
									BgL_auxz00_14216 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9801);
								}
							else
								{	/* SawMill/defs.scm 47 */
									BgL_auxz00_14216 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9800));
								}
						}
					}
					((((BgL_rtl_vrefz00_bglt) COBJECT(
									((BgL_rtl_vrefz00_bglt) BgL_new1230z00_8269)))->
							BgL_vtypez00) = ((BgL_typez00_bglt) BgL_auxz00_14216), BUNSPEC);
				}
				BgL_auxz00_14203 = ((BgL_rtl_vrefz00_bglt) BgL_new1230z00_8269);
				return ((obj_t) BgL_auxz00_14203);
			}
		}

	}



/* &lambda2353 */
	BgL_rtl_vrefz00_bglt BGl_z62lambda2353z62zzsaw_defsz00(obj_t BgL_envz00_8270)
	{
		{	/* SawMill/defs.scm 47 */
			{	/* SawMill/defs.scm 47 */
				BgL_rtl_vrefz00_bglt BgL_new1229z00_9802;

				BgL_new1229z00_9802 =
					((BgL_rtl_vrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_vrefz00_bgl))));
				{	/* SawMill/defs.scm 47 */
					long BgL_arg2354z00_9803;

					BgL_arg2354z00_9803 = BGL_CLASS_NUM(BGl_rtl_vrefz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1229z00_9802), BgL_arg2354z00_9803);
				}
				return BgL_new1229z00_9802;
			}
		}

	}



/* &lambda2351 */
	BgL_rtl_vrefz00_bglt BGl_z62lambda2351z62zzsaw_defsz00(obj_t BgL_envz00_8271,
		obj_t BgL_loc1226z00_8272, obj_t BgL_type1227z00_8273,
		obj_t BgL_vtype1228z00_8274)
	{
		{	/* SawMill/defs.scm 47 */
			{	/* SawMill/defs.scm 47 */
				BgL_rtl_vrefz00_bglt BgL_new1505z00_9806;

				{	/* SawMill/defs.scm 47 */
					BgL_rtl_vrefz00_bglt BgL_new1504z00_9807;

					BgL_new1504z00_9807 =
						((BgL_rtl_vrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_vrefz00_bgl))));
					{	/* SawMill/defs.scm 47 */
						long BgL_arg2352z00_9808;

						BgL_arg2352z00_9808 = BGL_CLASS_NUM(BGl_rtl_vrefz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1504z00_9807), BgL_arg2352z00_9808);
					}
					BgL_new1505z00_9806 = BgL_new1504z00_9807;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1505z00_9806)))->BgL_locz00) =
					((obj_t) BgL_loc1226z00_8272), BUNSPEC);
				((((BgL_rtl_vrefz00_bglt) COBJECT(BgL_new1505z00_9806))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1227z00_8273)),
					BUNSPEC);
				((((BgL_rtl_vrefz00_bglt) COBJECT(BgL_new1505z00_9806))->BgL_vtypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vtype1228z00_8274)),
					BUNSPEC);
				return BgL_new1505z00_9806;
			}
		}

	}



/* &lambda2366 */
	obj_t BGl_z62lambda2366z62zzsaw_defsz00(obj_t BgL_envz00_8275,
		obj_t BgL_oz00_8276, obj_t BgL_vz00_8277)
	{
		{	/* SawMill/defs.scm 47 */
			return
				((((BgL_rtl_vrefz00_bglt) COBJECT(
							((BgL_rtl_vrefz00_bglt) BgL_oz00_8276)))->BgL_vtypez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8277)), BUNSPEC);
		}

	}



/* &lambda2365 */
	BgL_typez00_bglt BGl_z62lambda2365z62zzsaw_defsz00(obj_t BgL_envz00_8278,
		obj_t BgL_oz00_8279)
	{
		{	/* SawMill/defs.scm 47 */
			return
				(((BgL_rtl_vrefz00_bglt) COBJECT(
						((BgL_rtl_vrefz00_bglt) BgL_oz00_8279)))->BgL_vtypez00);
		}

	}



/* &lambda2360 */
	obj_t BGl_z62lambda2360z62zzsaw_defsz00(obj_t BgL_envz00_8280,
		obj_t BgL_oz00_8281, obj_t BgL_vz00_8282)
	{
		{	/* SawMill/defs.scm 47 */
			return
				((((BgL_rtl_vrefz00_bglt) COBJECT(
							((BgL_rtl_vrefz00_bglt) BgL_oz00_8281)))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8282)), BUNSPEC);
		}

	}



/* &lambda2359 */
	BgL_typez00_bglt BGl_z62lambda2359z62zzsaw_defsz00(obj_t BgL_envz00_8283,
		obj_t BgL_oz00_8284)
	{
		{	/* SawMill/defs.scm 47 */
			return
				(((BgL_rtl_vrefz00_bglt) COBJECT(
						((BgL_rtl_vrefz00_bglt) BgL_oz00_8284)))->BgL_typez00);
		}

	}



/* &<@anonymous:2331> */
	obj_t BGl_z62zc3z04anonymousza32331ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8285,
		obj_t BgL_new1224z00_8286)
	{
		{	/* SawMill/defs.scm 46 */
			{
				BgL_rtl_vallocz00_bglt BgL_auxz00_14251;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_vallocz00_bglt) BgL_new1224z00_8286))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_14255;

					{	/* SawMill/defs.scm 46 */
						obj_t BgL_classz00_9816;

						BgL_classz00_9816 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 46 */
							obj_t BgL__ortest_1117z00_9817;

							BgL__ortest_1117z00_9817 = BGL_CLASS_NIL(BgL_classz00_9816);
							if (CBOOL(BgL__ortest_1117z00_9817))
								{	/* SawMill/defs.scm 46 */
									BgL_auxz00_14255 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9817);
								}
							else
								{	/* SawMill/defs.scm 46 */
									BgL_auxz00_14255 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9816));
								}
						}
					}
					((((BgL_rtl_vallocz00_bglt) COBJECT(
									((BgL_rtl_vallocz00_bglt) BgL_new1224z00_8286)))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_14255), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_14264;

					{	/* SawMill/defs.scm 46 */
						obj_t BgL_classz00_9818;

						BgL_classz00_9818 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 46 */
							obj_t BgL__ortest_1117z00_9819;

							BgL__ortest_1117z00_9819 = BGL_CLASS_NIL(BgL_classz00_9818);
							if (CBOOL(BgL__ortest_1117z00_9819))
								{	/* SawMill/defs.scm 46 */
									BgL_auxz00_14264 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9819);
								}
							else
								{	/* SawMill/defs.scm 46 */
									BgL_auxz00_14264 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9818));
								}
						}
					}
					((((BgL_rtl_vallocz00_bglt) COBJECT(
									((BgL_rtl_vallocz00_bglt) BgL_new1224z00_8286)))->
							BgL_vtypez00) = ((BgL_typez00_bglt) BgL_auxz00_14264), BUNSPEC);
				}
				BgL_auxz00_14251 = ((BgL_rtl_vallocz00_bglt) BgL_new1224z00_8286);
				return ((obj_t) BgL_auxz00_14251);
			}
		}

	}



/* &lambda2329 */
	BgL_rtl_vallocz00_bglt BGl_z62lambda2329z62zzsaw_defsz00(obj_t
		BgL_envz00_8287)
	{
		{	/* SawMill/defs.scm 46 */
			{	/* SawMill/defs.scm 46 */
				BgL_rtl_vallocz00_bglt BgL_new1223z00_9820;

				BgL_new1223z00_9820 =
					((BgL_rtl_vallocz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_vallocz00_bgl))));
				{	/* SawMill/defs.scm 46 */
					long BgL_arg2330z00_9821;

					BgL_arg2330z00_9821 = BGL_CLASS_NUM(BGl_rtl_vallocz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1223z00_9820), BgL_arg2330z00_9821);
				}
				return BgL_new1223z00_9820;
			}
		}

	}



/* &lambda2327 */
	BgL_rtl_vallocz00_bglt BGl_z62lambda2327z62zzsaw_defsz00(obj_t
		BgL_envz00_8288, obj_t BgL_loc1220z00_8289, obj_t BgL_type1221z00_8290,
		obj_t BgL_vtype1222z00_8291)
	{
		{	/* SawMill/defs.scm 46 */
			{	/* SawMill/defs.scm 46 */
				BgL_rtl_vallocz00_bglt BgL_new1503z00_9824;

				{	/* SawMill/defs.scm 46 */
					BgL_rtl_vallocz00_bglt BgL_new1502z00_9825;

					BgL_new1502z00_9825 =
						((BgL_rtl_vallocz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_vallocz00_bgl))));
					{	/* SawMill/defs.scm 46 */
						long BgL_arg2328z00_9826;

						BgL_arg2328z00_9826 = BGL_CLASS_NUM(BGl_rtl_vallocz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1502z00_9825), BgL_arg2328z00_9826);
					}
					BgL_new1503z00_9824 = BgL_new1502z00_9825;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1503z00_9824)))->BgL_locz00) =
					((obj_t) BgL_loc1220z00_8289), BUNSPEC);
				((((BgL_rtl_vallocz00_bglt) COBJECT(BgL_new1503z00_9824))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1221z00_8290)),
					BUNSPEC);
				((((BgL_rtl_vallocz00_bglt) COBJECT(BgL_new1503z00_9824))->
						BgL_vtypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vtype1222z00_8291)),
					BUNSPEC);
				return BgL_new1503z00_9824;
			}
		}

	}



/* &lambda2343 */
	obj_t BGl_z62lambda2343z62zzsaw_defsz00(obj_t BgL_envz00_8292,
		obj_t BgL_oz00_8293, obj_t BgL_vz00_8294)
	{
		{	/* SawMill/defs.scm 46 */
			return
				((((BgL_rtl_vallocz00_bglt) COBJECT(
							((BgL_rtl_vallocz00_bglt) BgL_oz00_8293)))->BgL_vtypez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8294)), BUNSPEC);
		}

	}



/* &lambda2342 */
	BgL_typez00_bglt BGl_z62lambda2342z62zzsaw_defsz00(obj_t BgL_envz00_8295,
		obj_t BgL_oz00_8296)
	{
		{	/* SawMill/defs.scm 46 */
			return
				(((BgL_rtl_vallocz00_bglt) COBJECT(
						((BgL_rtl_vallocz00_bglt) BgL_oz00_8296)))->BgL_vtypez00);
		}

	}



/* &lambda2338 */
	obj_t BGl_z62lambda2338z62zzsaw_defsz00(obj_t BgL_envz00_8297,
		obj_t BgL_oz00_8298, obj_t BgL_vz00_8299)
	{
		{	/* SawMill/defs.scm 46 */
			return
				((((BgL_rtl_vallocz00_bglt) COBJECT(
							((BgL_rtl_vallocz00_bglt) BgL_oz00_8298)))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8299)), BUNSPEC);
		}

	}



/* &lambda2337 */
	BgL_typez00_bglt BGl_z62lambda2337z62zzsaw_defsz00(obj_t BgL_envz00_8300,
		obj_t BgL_oz00_8301)
	{
		{	/* SawMill/defs.scm 46 */
			return
				(((BgL_rtl_vallocz00_bglt) COBJECT(
						((BgL_rtl_vallocz00_bglt) BgL_oz00_8301)))->BgL_typez00);
		}

	}



/* &<@anonymous:2305> */
	obj_t BGl_z62zc3z04anonymousza32305ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8302,
		obj_t BgL_new1218z00_8303)
	{
		{	/* SawMill/defs.scm 45 */
			{
				BgL_rtl_getfieldz00_bglt BgL_auxz00_14299;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_getfieldz00_bglt) BgL_new1218z00_8303))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_getfieldz00_bglt) COBJECT(((BgL_rtl_getfieldz00_bglt)
									BgL_new1218z00_8303)))->BgL_namez00) =
					((obj_t) BGl_string3354z00zzsaw_defsz00), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_14305;

					{	/* SawMill/defs.scm 45 */
						obj_t BgL_classz00_9834;

						BgL_classz00_9834 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 45 */
							obj_t BgL__ortest_1117z00_9835;

							BgL__ortest_1117z00_9835 = BGL_CLASS_NIL(BgL_classz00_9834);
							if (CBOOL(BgL__ortest_1117z00_9835))
								{	/* SawMill/defs.scm 45 */
									BgL_auxz00_14305 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9835);
								}
							else
								{	/* SawMill/defs.scm 45 */
									BgL_auxz00_14305 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9834));
								}
						}
					}
					((((BgL_rtl_getfieldz00_bglt) COBJECT(
									((BgL_rtl_getfieldz00_bglt) BgL_new1218z00_8303)))->
							BgL_objtypez00) = ((BgL_typez00_bglt) BgL_auxz00_14305), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_14314;

					{	/* SawMill/defs.scm 45 */
						obj_t BgL_classz00_9836;

						BgL_classz00_9836 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 45 */
							obj_t BgL__ortest_1117z00_9837;

							BgL__ortest_1117z00_9837 = BGL_CLASS_NIL(BgL_classz00_9836);
							if (CBOOL(BgL__ortest_1117z00_9837))
								{	/* SawMill/defs.scm 45 */
									BgL_auxz00_14314 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9837);
								}
							else
								{	/* SawMill/defs.scm 45 */
									BgL_auxz00_14314 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9836));
								}
						}
					}
					((((BgL_rtl_getfieldz00_bglt) COBJECT(
									((BgL_rtl_getfieldz00_bglt) BgL_new1218z00_8303)))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_14314), BUNSPEC);
				}
				BgL_auxz00_14299 = ((BgL_rtl_getfieldz00_bglt) BgL_new1218z00_8303);
				return ((obj_t) BgL_auxz00_14299);
			}
		}

	}



/* &lambda2302 */
	BgL_rtl_getfieldz00_bglt BGl_z62lambda2302z62zzsaw_defsz00(obj_t
		BgL_envz00_8304)
	{
		{	/* SawMill/defs.scm 45 */
			{	/* SawMill/defs.scm 45 */
				BgL_rtl_getfieldz00_bglt BgL_new1217z00_9838;

				BgL_new1217z00_9838 =
					((BgL_rtl_getfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_getfieldz00_bgl))));
				{	/* SawMill/defs.scm 45 */
					long BgL_arg2304z00_9839;

					BgL_arg2304z00_9839 = BGL_CLASS_NUM(BGl_rtl_getfieldz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1217z00_9838), BgL_arg2304z00_9839);
				}
				return BgL_new1217z00_9838;
			}
		}

	}



/* &lambda2300 */
	BgL_rtl_getfieldz00_bglt BGl_z62lambda2300z62zzsaw_defsz00(obj_t
		BgL_envz00_8305, obj_t BgL_loc1213z00_8306, obj_t BgL_name1214z00_8307,
		obj_t BgL_objtype1215z00_8308, obj_t BgL_type1216z00_8309)
	{
		{	/* SawMill/defs.scm 45 */
			{	/* SawMill/defs.scm 45 */
				BgL_rtl_getfieldz00_bglt BgL_new1501z00_9843;

				{	/* SawMill/defs.scm 45 */
					BgL_rtl_getfieldz00_bglt BgL_new1500z00_9844;

					BgL_new1500z00_9844 =
						((BgL_rtl_getfieldz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_getfieldz00_bgl))));
					{	/* SawMill/defs.scm 45 */
						long BgL_arg2301z00_9845;

						BgL_arg2301z00_9845 =
							BGL_CLASS_NUM(BGl_rtl_getfieldz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1500z00_9844), BgL_arg2301z00_9845);
					}
					BgL_new1501z00_9843 = BgL_new1500z00_9844;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1501z00_9843)))->BgL_locz00) =
					((obj_t) BgL_loc1213z00_8306), BUNSPEC);
				((((BgL_rtl_getfieldz00_bglt) COBJECT(BgL_new1501z00_9843))->
						BgL_namez00) = ((obj_t) ((obj_t) BgL_name1214z00_8307)), BUNSPEC);
				((((BgL_rtl_getfieldz00_bglt) COBJECT(BgL_new1501z00_9843))->
						BgL_objtypez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_objtype1215z00_8308)),
					BUNSPEC);
				((((BgL_rtl_getfieldz00_bglt) COBJECT(BgL_new1501z00_9843))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1216z00_8309)),
					BUNSPEC);
				return BgL_new1501z00_9843;
			}
		}

	}



/* &lambda2320 */
	obj_t BGl_z62lambda2320z62zzsaw_defsz00(obj_t BgL_envz00_8310,
		obj_t BgL_oz00_8311, obj_t BgL_vz00_8312)
	{
		{	/* SawMill/defs.scm 45 */
			return
				((((BgL_rtl_getfieldz00_bglt) COBJECT(
							((BgL_rtl_getfieldz00_bglt) BgL_oz00_8311)))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8312)), BUNSPEC);
		}

	}



/* &lambda2319 */
	BgL_typez00_bglt BGl_z62lambda2319z62zzsaw_defsz00(obj_t BgL_envz00_8313,
		obj_t BgL_oz00_8314)
	{
		{	/* SawMill/defs.scm 45 */
			return
				(((BgL_rtl_getfieldz00_bglt) COBJECT(
						((BgL_rtl_getfieldz00_bglt) BgL_oz00_8314)))->BgL_typez00);
		}

	}



/* &lambda2315 */
	obj_t BGl_z62lambda2315z62zzsaw_defsz00(obj_t BgL_envz00_8315,
		obj_t BgL_oz00_8316, obj_t BgL_vz00_8317)
	{
		{	/* SawMill/defs.scm 45 */
			return
				((((BgL_rtl_getfieldz00_bglt) COBJECT(
							((BgL_rtl_getfieldz00_bglt) BgL_oz00_8316)))->BgL_objtypez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8317)), BUNSPEC);
		}

	}



/* &lambda2314 */
	BgL_typez00_bglt BGl_z62lambda2314z62zzsaw_defsz00(obj_t BgL_envz00_8318,
		obj_t BgL_oz00_8319)
	{
		{	/* SawMill/defs.scm 45 */
			return
				(((BgL_rtl_getfieldz00_bglt) COBJECT(
						((BgL_rtl_getfieldz00_bglt) BgL_oz00_8319)))->BgL_objtypez00);
		}

	}



/* &lambda2310 */
	obj_t BGl_z62lambda2310z62zzsaw_defsz00(obj_t BgL_envz00_8320,
		obj_t BgL_oz00_8321, obj_t BgL_vz00_8322)
	{
		{	/* SawMill/defs.scm 45 */
			return
				((((BgL_rtl_getfieldz00_bglt) COBJECT(
							((BgL_rtl_getfieldz00_bglt) BgL_oz00_8321)))->BgL_namez00) =
				((obj_t) ((obj_t) BgL_vz00_8322)), BUNSPEC);
		}

	}



/* &lambda2309 */
	obj_t BGl_z62lambda2309z62zzsaw_defsz00(obj_t BgL_envz00_8323,
		obj_t BgL_oz00_8324)
	{
		{	/* SawMill/defs.scm 45 */
			return
				(((BgL_rtl_getfieldz00_bglt) COBJECT(
						((BgL_rtl_getfieldz00_bglt) BgL_oz00_8324)))->BgL_namez00);
		}

	}



/* &<@anonymous:2289> */
	obj_t BGl_z62zc3z04anonymousza32289ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8325,
		obj_t BgL_new1211z00_8326)
	{
		{	/* SawMill/defs.scm 44 */
			{
				BgL_rtl_globalrefz00_bglt BgL_auxz00_14356;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_globalrefz00_bglt) BgL_new1211z00_8326))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_globalz00_bglt BgL_auxz00_14360;

					{	/* SawMill/defs.scm 44 */
						obj_t BgL_classz00_9856;

						BgL_classz00_9856 = BGl_globalz00zzast_varz00;
						{	/* SawMill/defs.scm 44 */
							obj_t BgL__ortest_1117z00_9857;

							BgL__ortest_1117z00_9857 = BGL_CLASS_NIL(BgL_classz00_9856);
							if (CBOOL(BgL__ortest_1117z00_9857))
								{	/* SawMill/defs.scm 44 */
									BgL_auxz00_14360 =
										((BgL_globalz00_bglt) BgL__ortest_1117z00_9857);
								}
							else
								{	/* SawMill/defs.scm 44 */
									BgL_auxz00_14360 =
										((BgL_globalz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9856));
								}
						}
					}
					((((BgL_rtl_globalrefz00_bglt) COBJECT(
									((BgL_rtl_globalrefz00_bglt) BgL_new1211z00_8326)))->
							BgL_varz00) = ((BgL_globalz00_bglt) BgL_auxz00_14360), BUNSPEC);
				}
				BgL_auxz00_14356 = ((BgL_rtl_globalrefz00_bglt) BgL_new1211z00_8326);
				return ((obj_t) BgL_auxz00_14356);
			}
		}

	}



/* &lambda2287 */
	BgL_rtl_globalrefz00_bglt BGl_z62lambda2287z62zzsaw_defsz00(obj_t
		BgL_envz00_8327)
	{
		{	/* SawMill/defs.scm 44 */
			{	/* SawMill/defs.scm 44 */
				BgL_rtl_globalrefz00_bglt BgL_new1210z00_9858;

				BgL_new1210z00_9858 =
					((BgL_rtl_globalrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_globalrefz00_bgl))));
				{	/* SawMill/defs.scm 44 */
					long BgL_arg2288z00_9859;

					BgL_arg2288z00_9859 =
						BGL_CLASS_NUM(BGl_rtl_globalrefz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1210z00_9858), BgL_arg2288z00_9859);
				}
				return BgL_new1210z00_9858;
			}
		}

	}



/* &lambda2284 */
	BgL_rtl_globalrefz00_bglt BGl_z62lambda2284z62zzsaw_defsz00(obj_t
		BgL_envz00_8328, obj_t BgL_loc1208z00_8329, obj_t BgL_var1209z00_8330)
	{
		{	/* SawMill/defs.scm 44 */
			{	/* SawMill/defs.scm 44 */
				BgL_rtl_globalrefz00_bglt BgL_new1499z00_9861;

				{	/* SawMill/defs.scm 44 */
					BgL_rtl_globalrefz00_bglt BgL_new1498z00_9862;

					BgL_new1498z00_9862 =
						((BgL_rtl_globalrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_globalrefz00_bgl))));
					{	/* SawMill/defs.scm 44 */
						long BgL_arg2286z00_9863;

						BgL_arg2286z00_9863 =
							BGL_CLASS_NUM(BGl_rtl_globalrefz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1498z00_9862), BgL_arg2286z00_9863);
					}
					BgL_new1499z00_9861 = BgL_new1498z00_9862;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1499z00_9861)))->BgL_locz00) =
					((obj_t) BgL_loc1208z00_8329), BUNSPEC);
				((((BgL_rtl_globalrefz00_bglt) COBJECT(BgL_new1499z00_9861))->
						BgL_varz00) =
					((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_var1209z00_8330)),
					BUNSPEC);
				return BgL_new1499z00_9861;
			}
		}

	}



/* &lambda2294 */
	obj_t BGl_z62lambda2294z62zzsaw_defsz00(obj_t BgL_envz00_8331,
		obj_t BgL_oz00_8332, obj_t BgL_vz00_8333)
	{
		{	/* SawMill/defs.scm 44 */
			return
				((((BgL_rtl_globalrefz00_bglt) COBJECT(
							((BgL_rtl_globalrefz00_bglt) BgL_oz00_8332)))->BgL_varz00) =
				((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_vz00_8333)), BUNSPEC);
		}

	}



/* &lambda2293 */
	BgL_globalz00_bglt BGl_z62lambda2293z62zzsaw_defsz00(obj_t BgL_envz00_8334,
		obj_t BgL_oz00_8335)
	{
		{	/* SawMill/defs.scm 44 */
			return
				(((BgL_rtl_globalrefz00_bglt) COBJECT(
						((BgL_rtl_globalrefz00_bglt) BgL_oz00_8335)))->BgL_varz00);
		}

	}



/* &<@anonymous:2272> */
	obj_t BGl_z62zc3z04anonymousza32272ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8336,
		obj_t BgL_new1206z00_8337)
	{
		{	/* SawMill/defs.scm 43 */
			{
				BgL_rtl_loadfunz00_bglt BgL_auxz00_14388;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_loadfunz00_bglt) BgL_new1206z00_8337))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_globalz00_bglt BgL_auxz00_14392;

					{	/* SawMill/defs.scm 43 */
						obj_t BgL_classz00_9868;

						BgL_classz00_9868 = BGl_globalz00zzast_varz00;
						{	/* SawMill/defs.scm 43 */
							obj_t BgL__ortest_1117z00_9869;

							BgL__ortest_1117z00_9869 = BGL_CLASS_NIL(BgL_classz00_9868);
							if (CBOOL(BgL__ortest_1117z00_9869))
								{	/* SawMill/defs.scm 43 */
									BgL_auxz00_14392 =
										((BgL_globalz00_bglt) BgL__ortest_1117z00_9869);
								}
							else
								{	/* SawMill/defs.scm 43 */
									BgL_auxz00_14392 =
										((BgL_globalz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9868));
								}
						}
					}
					((((BgL_rtl_loadfunz00_bglt) COBJECT(
									((BgL_rtl_loadfunz00_bglt) BgL_new1206z00_8337)))->
							BgL_varz00) = ((BgL_globalz00_bglt) BgL_auxz00_14392), BUNSPEC);
				}
				BgL_auxz00_14388 = ((BgL_rtl_loadfunz00_bglt) BgL_new1206z00_8337);
				return ((obj_t) BgL_auxz00_14388);
			}
		}

	}



/* &lambda2270 */
	BgL_rtl_loadfunz00_bglt BGl_z62lambda2270z62zzsaw_defsz00(obj_t
		BgL_envz00_8338)
	{
		{	/* SawMill/defs.scm 43 */
			{	/* SawMill/defs.scm 43 */
				BgL_rtl_loadfunz00_bglt BgL_new1205z00_9870;

				BgL_new1205z00_9870 =
					((BgL_rtl_loadfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_loadfunz00_bgl))));
				{	/* SawMill/defs.scm 43 */
					long BgL_arg2271z00_9871;

					BgL_arg2271z00_9871 = BGL_CLASS_NUM(BGl_rtl_loadfunz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1205z00_9870), BgL_arg2271z00_9871);
				}
				return BgL_new1205z00_9870;
			}
		}

	}



/* &lambda2268 */
	BgL_rtl_loadfunz00_bglt BGl_z62lambda2268z62zzsaw_defsz00(obj_t
		BgL_envz00_8339, obj_t BgL_loc1203z00_8340, obj_t BgL_var1204z00_8341)
	{
		{	/* SawMill/defs.scm 43 */
			{	/* SawMill/defs.scm 43 */
				BgL_rtl_loadfunz00_bglt BgL_new1497z00_9873;

				{	/* SawMill/defs.scm 43 */
					BgL_rtl_loadfunz00_bglt BgL_new1496z00_9874;

					BgL_new1496z00_9874 =
						((BgL_rtl_loadfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_loadfunz00_bgl))));
					{	/* SawMill/defs.scm 43 */
						long BgL_arg2269z00_9875;

						BgL_arg2269z00_9875 =
							BGL_CLASS_NUM(BGl_rtl_loadfunz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1496z00_9874), BgL_arg2269z00_9875);
					}
					BgL_new1497z00_9873 = BgL_new1496z00_9874;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1497z00_9873)))->BgL_locz00) =
					((obj_t) BgL_loc1203z00_8340), BUNSPEC);
				((((BgL_rtl_loadfunz00_bglt) COBJECT(BgL_new1497z00_9873))->
						BgL_varz00) =
					((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_var1204z00_8341)),
					BUNSPEC);
				return BgL_new1497z00_9873;
			}
		}

	}



/* &lambda2277 */
	obj_t BGl_z62lambda2277z62zzsaw_defsz00(obj_t BgL_envz00_8342,
		obj_t BgL_oz00_8343, obj_t BgL_vz00_8344)
	{
		{	/* SawMill/defs.scm 43 */
			return
				((((BgL_rtl_loadfunz00_bglt) COBJECT(
							((BgL_rtl_loadfunz00_bglt) BgL_oz00_8343)))->BgL_varz00) =
				((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_vz00_8344)), BUNSPEC);
		}

	}



/* &lambda2276 */
	BgL_globalz00_bglt BGl_z62lambda2276z62zzsaw_defsz00(obj_t BgL_envz00_8345,
		obj_t BgL_oz00_8346)
	{
		{	/* SawMill/defs.scm 43 */
			return
				(((BgL_rtl_loadfunz00_bglt) COBJECT(
						((BgL_rtl_loadfunz00_bglt) BgL_oz00_8346)))->BgL_varz00);
		}

	}



/* &<@anonymous:2257> */
	obj_t BGl_z62zc3z04anonymousza32257ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8347,
		obj_t BgL_new1201z00_8348)
	{
		{	/* SawMill/defs.scm 42 */
			{
				BgL_rtl_loadgz00_bglt BgL_auxz00_14420;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_loadgz00_bglt) BgL_new1201z00_8348))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_globalz00_bglt BgL_auxz00_14424;

					{	/* SawMill/defs.scm 42 */
						obj_t BgL_classz00_9880;

						BgL_classz00_9880 = BGl_globalz00zzast_varz00;
						{	/* SawMill/defs.scm 42 */
							obj_t BgL__ortest_1117z00_9881;

							BgL__ortest_1117z00_9881 = BGL_CLASS_NIL(BgL_classz00_9880);
							if (CBOOL(BgL__ortest_1117z00_9881))
								{	/* SawMill/defs.scm 42 */
									BgL_auxz00_14424 =
										((BgL_globalz00_bglt) BgL__ortest_1117z00_9881);
								}
							else
								{	/* SawMill/defs.scm 42 */
									BgL_auxz00_14424 =
										((BgL_globalz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9880));
								}
						}
					}
					((((BgL_rtl_loadgz00_bglt) COBJECT(
									((BgL_rtl_loadgz00_bglt) BgL_new1201z00_8348)))->BgL_varz00) =
						((BgL_globalz00_bglt) BgL_auxz00_14424), BUNSPEC);
				}
				BgL_auxz00_14420 = ((BgL_rtl_loadgz00_bglt) BgL_new1201z00_8348);
				return ((obj_t) BgL_auxz00_14420);
			}
		}

	}



/* &lambda2255 */
	BgL_rtl_loadgz00_bglt BGl_z62lambda2255z62zzsaw_defsz00(obj_t BgL_envz00_8349)
	{
		{	/* SawMill/defs.scm 42 */
			{	/* SawMill/defs.scm 42 */
				BgL_rtl_loadgz00_bglt BgL_new1200z00_9882;

				BgL_new1200z00_9882 =
					((BgL_rtl_loadgz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_loadgz00_bgl))));
				{	/* SawMill/defs.scm 42 */
					long BgL_arg2256z00_9883;

					BgL_arg2256z00_9883 = BGL_CLASS_NUM(BGl_rtl_loadgz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1200z00_9882), BgL_arg2256z00_9883);
				}
				return BgL_new1200z00_9882;
			}
		}

	}



/* &lambda2253 */
	BgL_rtl_loadgz00_bglt BGl_z62lambda2253z62zzsaw_defsz00(obj_t BgL_envz00_8350,
		obj_t BgL_loc1198z00_8351, obj_t BgL_var1199z00_8352)
	{
		{	/* SawMill/defs.scm 42 */
			{	/* SawMill/defs.scm 42 */
				BgL_rtl_loadgz00_bglt BgL_new1495z00_9885;

				{	/* SawMill/defs.scm 42 */
					BgL_rtl_loadgz00_bglt BgL_new1494z00_9886;

					BgL_new1494z00_9886 =
						((BgL_rtl_loadgz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_loadgz00_bgl))));
					{	/* SawMill/defs.scm 42 */
						long BgL_arg2254z00_9887;

						BgL_arg2254z00_9887 = BGL_CLASS_NUM(BGl_rtl_loadgz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1494z00_9886), BgL_arg2254z00_9887);
					}
					BgL_new1495z00_9885 = BgL_new1494z00_9886;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1495z00_9885)))->BgL_locz00) =
					((obj_t) BgL_loc1198z00_8351), BUNSPEC);
				((((BgL_rtl_loadgz00_bglt) COBJECT(BgL_new1495z00_9885))->BgL_varz00) =
					((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_var1199z00_8352)),
					BUNSPEC);
				return BgL_new1495z00_9885;
			}
		}

	}



/* &lambda2262 */
	obj_t BGl_z62lambda2262z62zzsaw_defsz00(obj_t BgL_envz00_8353,
		obj_t BgL_oz00_8354, obj_t BgL_vz00_8355)
	{
		{	/* SawMill/defs.scm 42 */
			return
				((((BgL_rtl_loadgz00_bglt) COBJECT(
							((BgL_rtl_loadgz00_bglt) BgL_oz00_8354)))->BgL_varz00) =
				((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_vz00_8355)), BUNSPEC);
		}

	}



/* &lambda2261 */
	BgL_globalz00_bglt BGl_z62lambda2261z62zzsaw_defsz00(obj_t BgL_envz00_8356,
		obj_t BgL_oz00_8357)
	{
		{	/* SawMill/defs.scm 42 */
			return
				(((BgL_rtl_loadgz00_bglt) COBJECT(
						((BgL_rtl_loadgz00_bglt) BgL_oz00_8357)))->BgL_varz00);
		}

	}



/* &<@anonymous:2242> */
	obj_t BGl_z62zc3z04anonymousza32242ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8358,
		obj_t BgL_new1196z00_8359)
	{
		{	/* SawMill/defs.scm 41 */
			{
				BgL_rtl_loadiz00_bglt BgL_auxz00_14452;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_loadiz00_bglt) BgL_new1196z00_8359))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_atomz00_bglt BgL_auxz00_14456;

					{	/* SawMill/defs.scm 41 */
						obj_t BgL_classz00_9892;

						BgL_classz00_9892 = BGl_atomz00zzast_nodez00;
						{	/* SawMill/defs.scm 41 */
							obj_t BgL__ortest_1117z00_9893;

							BgL__ortest_1117z00_9893 = BGL_CLASS_NIL(BgL_classz00_9892);
							if (CBOOL(BgL__ortest_1117z00_9893))
								{	/* SawMill/defs.scm 41 */
									BgL_auxz00_14456 =
										((BgL_atomz00_bglt) BgL__ortest_1117z00_9893);
								}
							else
								{	/* SawMill/defs.scm 41 */
									BgL_auxz00_14456 =
										((BgL_atomz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9892));
								}
						}
					}
					((((BgL_rtl_loadiz00_bglt) COBJECT(
									((BgL_rtl_loadiz00_bglt) BgL_new1196z00_8359)))->
							BgL_constantz00) =
						((BgL_atomz00_bglt) BgL_auxz00_14456), BUNSPEC);
				}
				BgL_auxz00_14452 = ((BgL_rtl_loadiz00_bglt) BgL_new1196z00_8359);
				return ((obj_t) BgL_auxz00_14452);
			}
		}

	}



/* &lambda2240 */
	BgL_rtl_loadiz00_bglt BGl_z62lambda2240z62zzsaw_defsz00(obj_t BgL_envz00_8360)
	{
		{	/* SawMill/defs.scm 41 */
			{	/* SawMill/defs.scm 41 */
				BgL_rtl_loadiz00_bglt BgL_new1195z00_9894;

				BgL_new1195z00_9894 =
					((BgL_rtl_loadiz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_loadiz00_bgl))));
				{	/* SawMill/defs.scm 41 */
					long BgL_arg2241z00_9895;

					BgL_arg2241z00_9895 = BGL_CLASS_NUM(BGl_rtl_loadiz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1195z00_9894), BgL_arg2241z00_9895);
				}
				return BgL_new1195z00_9894;
			}
		}

	}



/* &lambda2238 */
	BgL_rtl_loadiz00_bglt BGl_z62lambda2238z62zzsaw_defsz00(obj_t BgL_envz00_8361,
		obj_t BgL_loc1193z00_8362, obj_t BgL_constant1194z00_8363)
	{
		{	/* SawMill/defs.scm 41 */
			{	/* SawMill/defs.scm 41 */
				BgL_rtl_loadiz00_bglt BgL_new1493z00_9897;

				{	/* SawMill/defs.scm 41 */
					BgL_rtl_loadiz00_bglt BgL_new1492z00_9898;

					BgL_new1492z00_9898 =
						((BgL_rtl_loadiz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_loadiz00_bgl))));
					{	/* SawMill/defs.scm 41 */
						long BgL_arg2239z00_9899;

						BgL_arg2239z00_9899 = BGL_CLASS_NUM(BGl_rtl_loadiz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1492z00_9898), BgL_arg2239z00_9899);
					}
					BgL_new1493z00_9897 = BgL_new1492z00_9898;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1493z00_9897)))->BgL_locz00) =
					((obj_t) BgL_loc1193z00_8362), BUNSPEC);
				((((BgL_rtl_loadiz00_bglt) COBJECT(BgL_new1493z00_9897))->
						BgL_constantz00) =
					((BgL_atomz00_bglt) ((BgL_atomz00_bglt) BgL_constant1194z00_8363)),
					BUNSPEC);
				return BgL_new1493z00_9897;
			}
		}

	}



/* &lambda2247 */
	obj_t BGl_z62lambda2247z62zzsaw_defsz00(obj_t BgL_envz00_8364,
		obj_t BgL_oz00_8365, obj_t BgL_vz00_8366)
	{
		{	/* SawMill/defs.scm 41 */
			return
				((((BgL_rtl_loadiz00_bglt) COBJECT(
							((BgL_rtl_loadiz00_bglt) BgL_oz00_8365)))->BgL_constantz00) =
				((BgL_atomz00_bglt) ((BgL_atomz00_bglt) BgL_vz00_8366)), BUNSPEC);
		}

	}



/* &lambda2246 */
	BgL_atomz00_bglt BGl_z62lambda2246z62zzsaw_defsz00(obj_t BgL_envz00_8367,
		obj_t BgL_oz00_8368)
	{
		{	/* SawMill/defs.scm 41 */
			return
				(((BgL_rtl_loadiz00_bglt) COBJECT(
						((BgL_rtl_loadiz00_bglt) BgL_oz00_8368)))->BgL_constantz00);
		}

	}



/* &<@anonymous:2232> */
	obj_t BGl_z62zc3z04anonymousza32232ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8369,
		obj_t BgL_new1191z00_8370)
	{
		{	/* SawMill/defs.scm 40 */
			{
				BgL_rtl_movz00_bglt BgL_auxz00_14484;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_movz00_bglt) BgL_new1191z00_8370))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14484 = ((BgL_rtl_movz00_bglt) BgL_new1191z00_8370);
				return ((obj_t) BgL_auxz00_14484);
			}
		}

	}



/* &lambda2230 */
	BgL_rtl_movz00_bglt BGl_z62lambda2230z62zzsaw_defsz00(obj_t BgL_envz00_8371)
	{
		{	/* SawMill/defs.scm 40 */
			{	/* SawMill/defs.scm 40 */
				BgL_rtl_movz00_bglt BgL_new1190z00_9904;

				BgL_new1190z00_9904 =
					((BgL_rtl_movz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_movz00_bgl))));
				{	/* SawMill/defs.scm 40 */
					long BgL_arg2231z00_9905;

					BgL_arg2231z00_9905 = BGL_CLASS_NUM(BGl_rtl_movz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1190z00_9904), BgL_arg2231z00_9905);
				}
				return BgL_new1190z00_9904;
			}
		}

	}



/* &lambda2228 */
	BgL_rtl_movz00_bglt BGl_z62lambda2228z62zzsaw_defsz00(obj_t BgL_envz00_8372,
		obj_t BgL_loc1189z00_8373)
	{
		{	/* SawMill/defs.scm 40 */
			{	/* SawMill/defs.scm 40 */
				BgL_rtl_movz00_bglt BgL_new1491z00_9906;

				{	/* SawMill/defs.scm 40 */
					BgL_rtl_movz00_bglt BgL_new1490z00_9907;

					BgL_new1490z00_9907 =
						((BgL_rtl_movz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_movz00_bgl))));
					{	/* SawMill/defs.scm 40 */
						long BgL_arg2229z00_9908;

						BgL_arg2229z00_9908 = BGL_CLASS_NUM(BGl_rtl_movz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1490z00_9907), BgL_arg2229z00_9908);
					}
					BgL_new1491z00_9906 = BgL_new1490z00_9907;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1491z00_9906)))->BgL_locz00) =
					((obj_t) BgL_loc1189z00_8373), BUNSPEC);
				return BgL_new1491z00_9906;
			}
		}

	}



/* &<@anonymous:2222> */
	obj_t BGl_z62zc3z04anonymousza32222ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8374,
		obj_t BgL_new1187z00_8375)
	{
		{	/* SawMill/defs.scm 39 */
			{
				BgL_rtl_nopz00_bglt BgL_auxz00_14500;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_nopz00_bglt) BgL_new1187z00_8375))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14500 = ((BgL_rtl_nopz00_bglt) BgL_new1187z00_8375);
				return ((obj_t) BgL_auxz00_14500);
			}
		}

	}



/* &lambda2220 */
	BgL_rtl_nopz00_bglt BGl_z62lambda2220z62zzsaw_defsz00(obj_t BgL_envz00_8376)
	{
		{	/* SawMill/defs.scm 39 */
			{	/* SawMill/defs.scm 39 */
				BgL_rtl_nopz00_bglt BgL_new1186z00_9910;

				BgL_new1186z00_9910 =
					((BgL_rtl_nopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_nopz00_bgl))));
				{	/* SawMill/defs.scm 39 */
					long BgL_arg2221z00_9911;

					BgL_arg2221z00_9911 = BGL_CLASS_NUM(BGl_rtl_nopz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1186z00_9910), BgL_arg2221z00_9911);
				}
				return BgL_new1186z00_9910;
			}
		}

	}



/* &lambda2218 */
	BgL_rtl_nopz00_bglt BGl_z62lambda2218z62zzsaw_defsz00(obj_t BgL_envz00_8377,
		obj_t BgL_loc1185z00_8378)
	{
		{	/* SawMill/defs.scm 39 */
			{	/* SawMill/defs.scm 39 */
				BgL_rtl_nopz00_bglt BgL_new1489z00_9912;

				{	/* SawMill/defs.scm 39 */
					BgL_rtl_nopz00_bglt BgL_new1488z00_9913;

					BgL_new1488z00_9913 =
						((BgL_rtl_nopz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_nopz00_bgl))));
					{	/* SawMill/defs.scm 39 */
						long BgL_arg2219z00_9914;

						BgL_arg2219z00_9914 = BGL_CLASS_NUM(BGl_rtl_nopz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1488z00_9913), BgL_arg2219z00_9914);
					}
					BgL_new1489z00_9912 = BgL_new1488z00_9913;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1489z00_9912)))->BgL_locz00) =
					((obj_t) BgL_loc1185z00_8378), BUNSPEC);
				return BgL_new1489z00_9912;
			}
		}

	}



/* &<@anonymous:2212> */
	obj_t BGl_z62zc3z04anonymousza32212ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8379,
		obj_t BgL_new1183z00_8380)
	{
		{	/* SawMill/defs.scm 38 */
			{
				BgL_rtl_purez00_bglt BgL_auxz00_14516;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_purez00_bglt) BgL_new1183z00_8380))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14516 = ((BgL_rtl_purez00_bglt) BgL_new1183z00_8380);
				return ((obj_t) BgL_auxz00_14516);
			}
		}

	}



/* &lambda2210 */
	BgL_rtl_purez00_bglt BGl_z62lambda2210z62zzsaw_defsz00(obj_t BgL_envz00_8381)
	{
		{	/* SawMill/defs.scm 38 */
			{	/* SawMill/defs.scm 38 */
				BgL_rtl_purez00_bglt BgL_new1182z00_9916;

				BgL_new1182z00_9916 =
					((BgL_rtl_purez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_purez00_bgl))));
				{	/* SawMill/defs.scm 38 */
					long BgL_arg2211z00_9917;

					BgL_arg2211z00_9917 = BGL_CLASS_NUM(BGl_rtl_purez00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1182z00_9916), BgL_arg2211z00_9917);
				}
				return BgL_new1182z00_9916;
			}
		}

	}



/* &lambda2208 */
	BgL_rtl_purez00_bglt BGl_z62lambda2208z62zzsaw_defsz00(obj_t BgL_envz00_8382,
		obj_t BgL_loc1181z00_8383)
	{
		{	/* SawMill/defs.scm 38 */
			{	/* SawMill/defs.scm 38 */
				BgL_rtl_purez00_bglt BgL_new1487z00_9918;

				{	/* SawMill/defs.scm 38 */
					BgL_rtl_purez00_bglt BgL_new1486z00_9919;

					BgL_new1486z00_9919 =
						((BgL_rtl_purez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_purez00_bgl))));
					{	/* SawMill/defs.scm 38 */
						long BgL_arg2209z00_9920;

						BgL_arg2209z00_9920 = BGL_CLASS_NUM(BGl_rtl_purez00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1486z00_9919), BgL_arg2209z00_9920);
					}
					BgL_new1487z00_9918 = BgL_new1486z00_9919;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1487z00_9918)))->BgL_locz00) =
					((obj_t) BgL_loc1181z00_8383), BUNSPEC);
				return BgL_new1487z00_9918;
			}
		}

	}



/* &<@anonymous:2197> */
	obj_t BGl_z62zc3z04anonymousza32197ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8384,
		obj_t BgL_new1179z00_8385)
	{
		{	/* SawMill/defs.scm 36 */
			{
				BgL_rtl_goz00_bglt BgL_auxz00_14532;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_goz00_bglt) BgL_new1179z00_8385))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_blockz00_bglt BgL_auxz00_14536;

					{	/* SawMill/defs.scm 36 */
						obj_t BgL_classz00_9922;

						BgL_classz00_9922 = BGl_blockz00zzsaw_defsz00;
						{	/* SawMill/defs.scm 36 */
							obj_t BgL__ortest_1117z00_9923;

							BgL__ortest_1117z00_9923 = BGL_CLASS_NIL(BgL_classz00_9922);
							if (CBOOL(BgL__ortest_1117z00_9923))
								{	/* SawMill/defs.scm 36 */
									BgL_auxz00_14536 =
										((BgL_blockz00_bglt) BgL__ortest_1117z00_9923);
								}
							else
								{	/* SawMill/defs.scm 36 */
									BgL_auxz00_14536 =
										((BgL_blockz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9922));
								}
						}
					}
					((((BgL_rtl_goz00_bglt) COBJECT(
									((BgL_rtl_goz00_bglt) BgL_new1179z00_8385)))->BgL_toz00) =
						((BgL_blockz00_bglt) BgL_auxz00_14536), BUNSPEC);
				}
				BgL_auxz00_14532 = ((BgL_rtl_goz00_bglt) BgL_new1179z00_8385);
				return ((obj_t) BgL_auxz00_14532);
			}
		}

	}



/* &lambda2194 */
	BgL_rtl_goz00_bglt BGl_z62lambda2194z62zzsaw_defsz00(obj_t BgL_envz00_8386)
	{
		{	/* SawMill/defs.scm 36 */
			{	/* SawMill/defs.scm 36 */
				BgL_rtl_goz00_bglt BgL_new1178z00_9924;

				BgL_new1178z00_9924 =
					((BgL_rtl_goz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_goz00_bgl))));
				{	/* SawMill/defs.scm 36 */
					long BgL_arg2196z00_9925;

					BgL_arg2196z00_9925 = BGL_CLASS_NUM(BGl_rtl_goz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1178z00_9924), BgL_arg2196z00_9925);
				}
				return BgL_new1178z00_9924;
			}
		}

	}



/* &lambda2192 */
	BgL_rtl_goz00_bglt BGl_z62lambda2192z62zzsaw_defsz00(obj_t BgL_envz00_8387,
		obj_t BgL_loc1176z00_8388, obj_t BgL_to1177z00_8389)
	{
		{	/* SawMill/defs.scm 36 */
			{	/* SawMill/defs.scm 36 */
				BgL_rtl_goz00_bglt BgL_new1485z00_9927;

				{	/* SawMill/defs.scm 36 */
					BgL_rtl_goz00_bglt BgL_new1484z00_9928;

					BgL_new1484z00_9928 =
						((BgL_rtl_goz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_goz00_bgl))));
					{	/* SawMill/defs.scm 36 */
						long BgL_arg2193z00_9929;

						BgL_arg2193z00_9929 = BGL_CLASS_NUM(BGl_rtl_goz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1484z00_9928), BgL_arg2193z00_9929);
					}
					BgL_new1485z00_9927 = BgL_new1484z00_9928;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1485z00_9927)))->BgL_locz00) =
					((obj_t) BgL_loc1176z00_8388), BUNSPEC);
				((((BgL_rtl_goz00_bglt) COBJECT(BgL_new1485z00_9927))->BgL_toz00) =
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_to1177z00_8389)),
					BUNSPEC);
				return BgL_new1485z00_9927;
			}
		}

	}



/* &lambda2202 */
	obj_t BGl_z62lambda2202z62zzsaw_defsz00(obj_t BgL_envz00_8390,
		obj_t BgL_oz00_8391, obj_t BgL_vz00_8392)
	{
		{	/* SawMill/defs.scm 36 */
			return
				((((BgL_rtl_goz00_bglt) COBJECT(
							((BgL_rtl_goz00_bglt) BgL_oz00_8391)))->BgL_toz00) =
				((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_vz00_8392)), BUNSPEC);
		}

	}



/* &lambda2201 */
	BgL_blockz00_bglt BGl_z62lambda2201z62zzsaw_defsz00(obj_t BgL_envz00_8393,
		obj_t BgL_oz00_8394)
	{
		{	/* SawMill/defs.scm 36 */
			return
				(((BgL_rtl_goz00_bglt) COBJECT(
						((BgL_rtl_goz00_bglt) BgL_oz00_8394)))->BgL_toz00);
		}

	}



/* &<@anonymous:2181> */
	obj_t BGl_z62zc3z04anonymousza32181ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8395,
		obj_t BgL_new1174z00_8396)
	{
		{	/* SawMill/defs.scm 35 */
			{
				BgL_rtl_ifnez00_bglt BgL_auxz00_14564;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_ifnez00_bglt) BgL_new1174z00_8396))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_blockz00_bglt BgL_auxz00_14568;

					{	/* SawMill/defs.scm 35 */
						obj_t BgL_classz00_9934;

						BgL_classz00_9934 = BGl_blockz00zzsaw_defsz00;
						{	/* SawMill/defs.scm 35 */
							obj_t BgL__ortest_1117z00_9935;

							BgL__ortest_1117z00_9935 = BGL_CLASS_NIL(BgL_classz00_9934);
							if (CBOOL(BgL__ortest_1117z00_9935))
								{	/* SawMill/defs.scm 35 */
									BgL_auxz00_14568 =
										((BgL_blockz00_bglt) BgL__ortest_1117z00_9935);
								}
							else
								{	/* SawMill/defs.scm 35 */
									BgL_auxz00_14568 =
										((BgL_blockz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9934));
								}
						}
					}
					((((BgL_rtl_ifnez00_bglt) COBJECT(
									((BgL_rtl_ifnez00_bglt) BgL_new1174z00_8396)))->BgL_thenz00) =
						((BgL_blockz00_bglt) BgL_auxz00_14568), BUNSPEC);
				}
				BgL_auxz00_14564 = ((BgL_rtl_ifnez00_bglt) BgL_new1174z00_8396);
				return ((obj_t) BgL_auxz00_14564);
			}
		}

	}



/* &lambda2179 */
	BgL_rtl_ifnez00_bglt BGl_z62lambda2179z62zzsaw_defsz00(obj_t BgL_envz00_8397)
	{
		{	/* SawMill/defs.scm 35 */
			{	/* SawMill/defs.scm 35 */
				BgL_rtl_ifnez00_bglt BgL_new1173z00_9936;

				BgL_new1173z00_9936 =
					((BgL_rtl_ifnez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_ifnez00_bgl))));
				{	/* SawMill/defs.scm 35 */
					long BgL_arg2180z00_9937;

					BgL_arg2180z00_9937 = BGL_CLASS_NUM(BGl_rtl_ifnez00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1173z00_9936), BgL_arg2180z00_9937);
				}
				return BgL_new1173z00_9936;
			}
		}

	}



/* &lambda2177 */
	BgL_rtl_ifnez00_bglt BGl_z62lambda2177z62zzsaw_defsz00(obj_t BgL_envz00_8398,
		obj_t BgL_loc1171z00_8399, obj_t BgL_then1172z00_8400)
	{
		{	/* SawMill/defs.scm 35 */
			{	/* SawMill/defs.scm 35 */
				BgL_rtl_ifnez00_bglt BgL_new1483z00_9939;

				{	/* SawMill/defs.scm 35 */
					BgL_rtl_ifnez00_bglt BgL_new1482z00_9940;

					BgL_new1482z00_9940 =
						((BgL_rtl_ifnez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_ifnez00_bgl))));
					{	/* SawMill/defs.scm 35 */
						long BgL_arg2178z00_9941;

						BgL_arg2178z00_9941 = BGL_CLASS_NUM(BGl_rtl_ifnez00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1482z00_9940), BgL_arg2178z00_9941);
					}
					BgL_new1483z00_9939 = BgL_new1482z00_9940;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1483z00_9939)))->BgL_locz00) =
					((obj_t) BgL_loc1171z00_8399), BUNSPEC);
				((((BgL_rtl_ifnez00_bglt) COBJECT(BgL_new1483z00_9939))->BgL_thenz00) =
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_then1172z00_8400)),
					BUNSPEC);
				return BgL_new1483z00_9939;
			}
		}

	}



/* &lambda2186 */
	obj_t BGl_z62lambda2186z62zzsaw_defsz00(obj_t BgL_envz00_8401,
		obj_t BgL_oz00_8402, obj_t BgL_vz00_8403)
	{
		{	/* SawMill/defs.scm 35 */
			return
				((((BgL_rtl_ifnez00_bglt) COBJECT(
							((BgL_rtl_ifnez00_bglt) BgL_oz00_8402)))->BgL_thenz00) =
				((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_vz00_8403)), BUNSPEC);
		}

	}



/* &lambda2185 */
	BgL_blockz00_bglt BGl_z62lambda2185z62zzsaw_defsz00(obj_t BgL_envz00_8404,
		obj_t BgL_oz00_8405)
	{
		{	/* SawMill/defs.scm 35 */
			return
				(((BgL_rtl_ifnez00_bglt) COBJECT(
						((BgL_rtl_ifnez00_bglt) BgL_oz00_8405)))->BgL_thenz00);
		}

	}



/* &<@anonymous:2166> */
	obj_t BGl_z62zc3z04anonymousza32166ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8406,
		obj_t BgL_new1169z00_8407)
	{
		{	/* SawMill/defs.scm 34 */
			{
				BgL_rtl_ifeqz00_bglt BgL_auxz00_14596;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_ifeqz00_bglt) BgL_new1169z00_8407))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_blockz00_bglt BgL_auxz00_14600;

					{	/* SawMill/defs.scm 34 */
						obj_t BgL_classz00_9946;

						BgL_classz00_9946 = BGl_blockz00zzsaw_defsz00;
						{	/* SawMill/defs.scm 34 */
							obj_t BgL__ortest_1117z00_9947;

							BgL__ortest_1117z00_9947 = BGL_CLASS_NIL(BgL_classz00_9946);
							if (CBOOL(BgL__ortest_1117z00_9947))
								{	/* SawMill/defs.scm 34 */
									BgL_auxz00_14600 =
										((BgL_blockz00_bglt) BgL__ortest_1117z00_9947);
								}
							else
								{	/* SawMill/defs.scm 34 */
									BgL_auxz00_14600 =
										((BgL_blockz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9946));
								}
						}
					}
					((((BgL_rtl_ifeqz00_bglt) COBJECT(
									((BgL_rtl_ifeqz00_bglt) BgL_new1169z00_8407)))->BgL_thenz00) =
						((BgL_blockz00_bglt) BgL_auxz00_14600), BUNSPEC);
				}
				BgL_auxz00_14596 = ((BgL_rtl_ifeqz00_bglt) BgL_new1169z00_8407);
				return ((obj_t) BgL_auxz00_14596);
			}
		}

	}



/* &lambda2164 */
	BgL_rtl_ifeqz00_bglt BGl_z62lambda2164z62zzsaw_defsz00(obj_t BgL_envz00_8408)
	{
		{	/* SawMill/defs.scm 34 */
			{	/* SawMill/defs.scm 34 */
				BgL_rtl_ifeqz00_bglt BgL_new1168z00_9948;

				BgL_new1168z00_9948 =
					((BgL_rtl_ifeqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_ifeqz00_bgl))));
				{	/* SawMill/defs.scm 34 */
					long BgL_arg2165z00_9949;

					BgL_arg2165z00_9949 = BGL_CLASS_NUM(BGl_rtl_ifeqz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1168z00_9948), BgL_arg2165z00_9949);
				}
				return BgL_new1168z00_9948;
			}
		}

	}



/* &lambda2162 */
	BgL_rtl_ifeqz00_bglt BGl_z62lambda2162z62zzsaw_defsz00(obj_t BgL_envz00_8409,
		obj_t BgL_loc1166z00_8410, obj_t BgL_then1167z00_8411)
	{
		{	/* SawMill/defs.scm 34 */
			{	/* SawMill/defs.scm 34 */
				BgL_rtl_ifeqz00_bglt BgL_new1480z00_9951;

				{	/* SawMill/defs.scm 34 */
					BgL_rtl_ifeqz00_bglt BgL_new1479z00_9952;

					BgL_new1479z00_9952 =
						((BgL_rtl_ifeqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_ifeqz00_bgl))));
					{	/* SawMill/defs.scm 34 */
						long BgL_arg2163z00_9953;

						BgL_arg2163z00_9953 = BGL_CLASS_NUM(BGl_rtl_ifeqz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1479z00_9952), BgL_arg2163z00_9953);
					}
					BgL_new1480z00_9951 = BgL_new1479z00_9952;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1480z00_9951)))->BgL_locz00) =
					((obj_t) BgL_loc1166z00_8410), BUNSPEC);
				((((BgL_rtl_ifeqz00_bglt) COBJECT(BgL_new1480z00_9951))->BgL_thenz00) =
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_then1167z00_8411)),
					BUNSPEC);
				return BgL_new1480z00_9951;
			}
		}

	}



/* &lambda2171 */
	obj_t BGl_z62lambda2171z62zzsaw_defsz00(obj_t BgL_envz00_8412,
		obj_t BgL_oz00_8413, obj_t BgL_vz00_8414)
	{
		{	/* SawMill/defs.scm 34 */
			return
				((((BgL_rtl_ifeqz00_bglt) COBJECT(
							((BgL_rtl_ifeqz00_bglt) BgL_oz00_8413)))->BgL_thenz00) =
				((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_vz00_8414)), BUNSPEC);
		}

	}



/* &lambda2170 */
	BgL_blockz00_bglt BGl_z62lambda2170z62zzsaw_defsz00(obj_t BgL_envz00_8415,
		obj_t BgL_oz00_8416)
	{
		{	/* SawMill/defs.scm 34 */
			return
				(((BgL_rtl_ifeqz00_bglt) COBJECT(
						((BgL_rtl_ifeqz00_bglt) BgL_oz00_8416)))->BgL_thenz00);
		}

	}



/* &<@anonymous:2150> */
	obj_t BGl_z62zc3z04anonymousza32150ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8417,
		obj_t BgL_new1164z00_8418)
	{
		{	/* SawMill/defs.scm 33 */
			{
				BgL_rtl_switchz00_bglt BgL_auxz00_14628;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_switchz00_bglt) BgL_new1164z00_8418))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_14632;

					{	/* SawMill/defs.scm 33 */
						obj_t BgL_classz00_9958;

						BgL_classz00_9958 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 33 */
							obj_t BgL__ortest_1117z00_9959;

							BgL__ortest_1117z00_9959 = BGL_CLASS_NIL(BgL_classz00_9958);
							if (CBOOL(BgL__ortest_1117z00_9959))
								{	/* SawMill/defs.scm 33 */
									BgL_auxz00_14632 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9959);
								}
							else
								{	/* SawMill/defs.scm 33 */
									BgL_auxz00_14632 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9958));
								}
						}
					}
					((((BgL_rtl_selectz00_bglt) COBJECT(
									((BgL_rtl_selectz00_bglt)
										((BgL_rtl_switchz00_bglt) BgL_new1164z00_8418))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_14632), BUNSPEC);
				}
				((((BgL_rtl_selectz00_bglt) COBJECT(
								((BgL_rtl_selectz00_bglt)
									((BgL_rtl_switchz00_bglt) BgL_new1164z00_8418))))->
						BgL_patternsz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_switchz00_bglt) COBJECT(((BgL_rtl_switchz00_bglt)
									BgL_new1164z00_8418)))->BgL_labelsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14628 = ((BgL_rtl_switchz00_bglt) BgL_new1164z00_8418);
				return ((obj_t) BgL_auxz00_14628);
			}
		}

	}



/* &lambda2148 */
	BgL_rtl_switchz00_bglt BGl_z62lambda2148z62zzsaw_defsz00(obj_t
		BgL_envz00_8419)
	{
		{	/* SawMill/defs.scm 33 */
			{	/* SawMill/defs.scm 33 */
				BgL_rtl_switchz00_bglt BgL_new1163z00_9960;

				BgL_new1163z00_9960 =
					((BgL_rtl_switchz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_switchz00_bgl))));
				{	/* SawMill/defs.scm 33 */
					long BgL_arg2149z00_9961;

					BgL_arg2149z00_9961 = BGL_CLASS_NUM(BGl_rtl_switchz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1163z00_9960), BgL_arg2149z00_9961);
				}
				return BgL_new1163z00_9960;
			}
		}

	}



/* &lambda2146 */
	BgL_rtl_switchz00_bglt BGl_z62lambda2146z62zzsaw_defsz00(obj_t
		BgL_envz00_8420, obj_t BgL_loc1159z00_8421, obj_t BgL_type1160z00_8422,
		obj_t BgL_patterns1161z00_8423, obj_t BgL_labels1162z00_8424)
	{
		{	/* SawMill/defs.scm 33 */
			{	/* SawMill/defs.scm 33 */
				BgL_rtl_switchz00_bglt BgL_new1478z00_9963;

				{	/* SawMill/defs.scm 33 */
					BgL_rtl_switchz00_bglt BgL_new1477z00_9964;

					BgL_new1477z00_9964 =
						((BgL_rtl_switchz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_switchz00_bgl))));
					{	/* SawMill/defs.scm 33 */
						long BgL_arg2147z00_9965;

						BgL_arg2147z00_9965 = BGL_CLASS_NUM(BGl_rtl_switchz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1477z00_9964), BgL_arg2147z00_9965);
					}
					BgL_new1478z00_9963 = BgL_new1477z00_9964;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1478z00_9963)))->BgL_locz00) =
					((obj_t) BgL_loc1159z00_8421), BUNSPEC);
				((((BgL_rtl_selectz00_bglt) COBJECT(((BgL_rtl_selectz00_bglt)
									BgL_new1478z00_9963)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1160z00_8422)),
					BUNSPEC);
				((((BgL_rtl_selectz00_bglt) COBJECT(((BgL_rtl_selectz00_bglt)
									BgL_new1478z00_9963)))->BgL_patternsz00) =
					((obj_t) BgL_patterns1161z00_8423), BUNSPEC);
				((((BgL_rtl_switchz00_bglt) COBJECT(BgL_new1478z00_9963))->
						BgL_labelsz00) = ((obj_t) BgL_labels1162z00_8424), BUNSPEC);
				return BgL_new1478z00_9963;
			}
		}

	}



/* &lambda2156 */
	obj_t BGl_z62lambda2156z62zzsaw_defsz00(obj_t BgL_envz00_8425,
		obj_t BgL_oz00_8426, obj_t BgL_vz00_8427)
	{
		{	/* SawMill/defs.scm 33 */
			return
				((((BgL_rtl_switchz00_bglt) COBJECT(
							((BgL_rtl_switchz00_bglt) BgL_oz00_8426)))->BgL_labelsz00) =
				((obj_t) BgL_vz00_8427), BUNSPEC);
		}

	}



/* &lambda2155 */
	obj_t BGl_z62lambda2155z62zzsaw_defsz00(obj_t BgL_envz00_8428,
		obj_t BgL_oz00_8429)
	{
		{	/* SawMill/defs.scm 33 */
			return
				(((BgL_rtl_switchz00_bglt) COBJECT(
						((BgL_rtl_switchz00_bglt) BgL_oz00_8429)))->BgL_labelsz00);
		}

	}



/* &<@anonymous:2130> */
	obj_t BGl_z62zc3z04anonymousza32130ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8430,
		obj_t BgL_new1157z00_8431)
	{
		{	/* SawMill/defs.scm 32 */
			{
				BgL_rtl_selectz00_bglt BgL_auxz00_14669;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_selectz00_bglt) BgL_new1157z00_8431))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_14673;

					{	/* SawMill/defs.scm 32 */
						obj_t BgL_classz00_9969;

						BgL_classz00_9969 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 32 */
							obj_t BgL__ortest_1117z00_9970;

							BgL__ortest_1117z00_9970 = BGL_CLASS_NIL(BgL_classz00_9969);
							if (CBOOL(BgL__ortest_1117z00_9970))
								{	/* SawMill/defs.scm 32 */
									BgL_auxz00_14673 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_9970);
								}
							else
								{	/* SawMill/defs.scm 32 */
									BgL_auxz00_14673 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9969));
								}
						}
					}
					((((BgL_rtl_selectz00_bglt) COBJECT(
									((BgL_rtl_selectz00_bglt) BgL_new1157z00_8431)))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_14673), BUNSPEC);
				}
				((((BgL_rtl_selectz00_bglt) COBJECT(
								((BgL_rtl_selectz00_bglt) BgL_new1157z00_8431)))->
						BgL_patternsz00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14669 = ((BgL_rtl_selectz00_bglt) BgL_new1157z00_8431);
				return ((obj_t) BgL_auxz00_14669);
			}
		}

	}



/* &lambda2127 */
	BgL_rtl_selectz00_bglt BGl_z62lambda2127z62zzsaw_defsz00(obj_t
		BgL_envz00_8432)
	{
		{	/* SawMill/defs.scm 32 */
			{	/* SawMill/defs.scm 32 */
				BgL_rtl_selectz00_bglt BgL_new1156z00_9971;

				BgL_new1156z00_9971 =
					((BgL_rtl_selectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_selectz00_bgl))));
				{	/* SawMill/defs.scm 32 */
					long BgL_arg2129z00_9972;

					BgL_arg2129z00_9972 = BGL_CLASS_NUM(BGl_rtl_selectz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1156z00_9971), BgL_arg2129z00_9972);
				}
				return BgL_new1156z00_9971;
			}
		}

	}



/* &lambda2125 */
	BgL_rtl_selectz00_bglt BGl_z62lambda2125z62zzsaw_defsz00(obj_t
		BgL_envz00_8433, obj_t BgL_loc1153z00_8434, obj_t BgL_type1154z00_8435,
		obj_t BgL_patterns1155z00_8436)
	{
		{	/* SawMill/defs.scm 32 */
			{	/* SawMill/defs.scm 32 */
				BgL_rtl_selectz00_bglt BgL_new1476z00_9974;

				{	/* SawMill/defs.scm 32 */
					BgL_rtl_selectz00_bglt BgL_new1475z00_9975;

					BgL_new1475z00_9975 =
						((BgL_rtl_selectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_selectz00_bgl))));
					{	/* SawMill/defs.scm 32 */
						long BgL_arg2126z00_9976;

						BgL_arg2126z00_9976 = BGL_CLASS_NUM(BGl_rtl_selectz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1475z00_9975), BgL_arg2126z00_9976);
					}
					BgL_new1476z00_9974 = BgL_new1475z00_9975;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1476z00_9974)))->BgL_locz00) =
					((obj_t) BgL_loc1153z00_8434), BUNSPEC);
				((((BgL_rtl_selectz00_bglt) COBJECT(BgL_new1476z00_9974))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1154z00_8435)),
					BUNSPEC);
				((((BgL_rtl_selectz00_bglt) COBJECT(BgL_new1476z00_9974))->
						BgL_patternsz00) = ((obj_t) BgL_patterns1155z00_8436), BUNSPEC);
				return BgL_new1476z00_9974;
			}
		}

	}



/* &lambda2140 */
	obj_t BGl_z62lambda2140z62zzsaw_defsz00(obj_t BgL_envz00_8437,
		obj_t BgL_oz00_8438, obj_t BgL_vz00_8439)
	{
		{	/* SawMill/defs.scm 32 */
			return
				((((BgL_rtl_selectz00_bglt) COBJECT(
							((BgL_rtl_selectz00_bglt) BgL_oz00_8438)))->BgL_patternsz00) =
				((obj_t) BgL_vz00_8439), BUNSPEC);
		}

	}



/* &lambda2139 */
	obj_t BGl_z62lambda2139z62zzsaw_defsz00(obj_t BgL_envz00_8440,
		obj_t BgL_oz00_8441)
	{
		{	/* SawMill/defs.scm 32 */
			return
				(((BgL_rtl_selectz00_bglt) COBJECT(
						((BgL_rtl_selectz00_bglt) BgL_oz00_8441)))->BgL_patternsz00);
		}

	}



/* &lambda2135 */
	obj_t BGl_z62lambda2135z62zzsaw_defsz00(obj_t BgL_envz00_8442,
		obj_t BgL_oz00_8443, obj_t BgL_vz00_8444)
	{
		{	/* SawMill/defs.scm 32 */
			return
				((((BgL_rtl_selectz00_bglt) COBJECT(
							((BgL_rtl_selectz00_bglt) BgL_oz00_8443)))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8444)), BUNSPEC);
		}

	}



/* &lambda2134 */
	BgL_typez00_bglt BGl_z62lambda2134z62zzsaw_defsz00(obj_t BgL_envz00_8445,
		obj_t BgL_oz00_8446)
	{
		{	/* SawMill/defs.scm 32 */
			return
				(((BgL_rtl_selectz00_bglt) COBJECT(
						((BgL_rtl_selectz00_bglt) BgL_oz00_8446)))->BgL_typez00);
		}

	}



/* &<@anonymous:2119> */
	obj_t BGl_z62zc3z04anonymousza32119ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8447,
		obj_t BgL_new1151z00_8448)
	{
		{	/* SawMill/defs.scm 31 */
			{
				BgL_rtl_ifz00_bglt BgL_auxz00_14708;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_ifz00_bglt) BgL_new1151z00_8448))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14708 = ((BgL_rtl_ifz00_bglt) BgL_new1151z00_8448);
				return ((obj_t) BgL_auxz00_14708);
			}
		}

	}



/* &lambda2117 */
	BgL_rtl_ifz00_bglt BGl_z62lambda2117z62zzsaw_defsz00(obj_t BgL_envz00_8449)
	{
		{	/* SawMill/defs.scm 31 */
			{	/* SawMill/defs.scm 31 */
				BgL_rtl_ifz00_bglt BgL_new1150z00_9983;

				BgL_new1150z00_9983 =
					((BgL_rtl_ifz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_ifz00_bgl))));
				{	/* SawMill/defs.scm 31 */
					long BgL_arg2118z00_9984;

					BgL_arg2118z00_9984 = BGL_CLASS_NUM(BGl_rtl_ifz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1150z00_9983), BgL_arg2118z00_9984);
				}
				return BgL_new1150z00_9983;
			}
		}

	}



/* &lambda2115 */
	BgL_rtl_ifz00_bglt BGl_z62lambda2115z62zzsaw_defsz00(obj_t BgL_envz00_8450,
		obj_t BgL_loc1149z00_8451)
	{
		{	/* SawMill/defs.scm 31 */
			{	/* SawMill/defs.scm 31 */
				BgL_rtl_ifz00_bglt BgL_new1474z00_9985;

				{	/* SawMill/defs.scm 31 */
					BgL_rtl_ifz00_bglt BgL_new1473z00_9986;

					BgL_new1473z00_9986 =
						((BgL_rtl_ifz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_ifz00_bgl))));
					{	/* SawMill/defs.scm 31 */
						long BgL_arg2116z00_9987;

						BgL_arg2116z00_9987 = BGL_CLASS_NUM(BGl_rtl_ifz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1473z00_9986), BgL_arg2116z00_9987);
					}
					BgL_new1474z00_9985 = BgL_new1473z00_9986;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1474z00_9985)))->BgL_locz00) =
					((obj_t) BgL_loc1149z00_8451), BUNSPEC);
				return BgL_new1474z00_9985;
			}
		}

	}



/* &<@anonymous:2109> */
	obj_t BGl_z62zc3z04anonymousza32109ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8452,
		obj_t BgL_new1147z00_8453)
	{
		{	/* SawMill/defs.scm 30 */
			{
				BgL_rtl_notseqz00_bglt BgL_auxz00_14724;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_notseqz00_bglt) BgL_new1147z00_8453))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14724 = ((BgL_rtl_notseqz00_bglt) BgL_new1147z00_8453);
				return ((obj_t) BgL_auxz00_14724);
			}
		}

	}



/* &lambda2107 */
	BgL_rtl_notseqz00_bglt BGl_z62lambda2107z62zzsaw_defsz00(obj_t
		BgL_envz00_8454)
	{
		{	/* SawMill/defs.scm 30 */
			{	/* SawMill/defs.scm 30 */
				BgL_rtl_notseqz00_bglt BgL_new1146z00_9989;

				BgL_new1146z00_9989 =
					((BgL_rtl_notseqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_notseqz00_bgl))));
				{	/* SawMill/defs.scm 30 */
					long BgL_arg2108z00_9990;

					BgL_arg2108z00_9990 = BGL_CLASS_NUM(BGl_rtl_notseqz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1146z00_9989), BgL_arg2108z00_9990);
				}
				return BgL_new1146z00_9989;
			}
		}

	}



/* &lambda2105 */
	BgL_rtl_notseqz00_bglt BGl_z62lambda2105z62zzsaw_defsz00(obj_t
		BgL_envz00_8455, obj_t BgL_loc1145z00_8456)
	{
		{	/* SawMill/defs.scm 30 */
			{	/* SawMill/defs.scm 30 */
				BgL_rtl_notseqz00_bglt BgL_new1472z00_9991;

				{	/* SawMill/defs.scm 30 */
					BgL_rtl_notseqz00_bglt BgL_new1471z00_9992;

					BgL_new1471z00_9992 =
						((BgL_rtl_notseqz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_notseqz00_bgl))));
					{	/* SawMill/defs.scm 30 */
						long BgL_arg2106z00_9993;

						BgL_arg2106z00_9993 = BGL_CLASS_NUM(BGl_rtl_notseqz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1471z00_9992), BgL_arg2106z00_9993);
					}
					BgL_new1472z00_9991 = BgL_new1471z00_9992;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1472z00_9991)))->BgL_locz00) =
					((obj_t) BgL_loc1145z00_8456), BUNSPEC);
				return BgL_new1472z00_9991;
			}
		}

	}



/* &<@anonymous:2094> */
	obj_t BGl_z62zc3z04anonymousza32094ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8457,
		obj_t BgL_new1143z00_8458)
	{
		{	/* SawMill/defs.scm 28 */
			{
				BgL_rtl_retz00_bglt BgL_auxz00_14740;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_retz00_bglt) BgL_new1143z00_8458))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_blockz00_bglt BgL_auxz00_14744;

					{	/* SawMill/defs.scm 28 */
						obj_t BgL_classz00_9995;

						BgL_classz00_9995 = BGl_blockz00zzsaw_defsz00;
						{	/* SawMill/defs.scm 28 */
							obj_t BgL__ortest_1117z00_9996;

							BgL__ortest_1117z00_9996 = BGL_CLASS_NIL(BgL_classz00_9995);
							if (CBOOL(BgL__ortest_1117z00_9996))
								{	/* SawMill/defs.scm 28 */
									BgL_auxz00_14744 =
										((BgL_blockz00_bglt) BgL__ortest_1117z00_9996);
								}
							else
								{	/* SawMill/defs.scm 28 */
									BgL_auxz00_14744 =
										((BgL_blockz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_9995));
								}
						}
					}
					((((BgL_rtl_retz00_bglt) COBJECT(
									((BgL_rtl_retz00_bglt) BgL_new1143z00_8458)))->BgL_toz00) =
						((BgL_blockz00_bglt) BgL_auxz00_14744), BUNSPEC);
				}
				BgL_auxz00_14740 = ((BgL_rtl_retz00_bglt) BgL_new1143z00_8458);
				return ((obj_t) BgL_auxz00_14740);
			}
		}

	}



/* &lambda2091 */
	BgL_rtl_retz00_bglt BGl_z62lambda2091z62zzsaw_defsz00(obj_t BgL_envz00_8459)
	{
		{	/* SawMill/defs.scm 28 */
			{	/* SawMill/defs.scm 28 */
				BgL_rtl_retz00_bglt BgL_new1142z00_9997;

				BgL_new1142z00_9997 =
					((BgL_rtl_retz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_retz00_bgl))));
				{	/* SawMill/defs.scm 28 */
					long BgL_arg2093z00_9998;

					BgL_arg2093z00_9998 = BGL_CLASS_NUM(BGl_rtl_retz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1142z00_9997), BgL_arg2093z00_9998);
				}
				return BgL_new1142z00_9997;
			}
		}

	}



/* &lambda2089 */
	BgL_rtl_retz00_bglt BGl_z62lambda2089z62zzsaw_defsz00(obj_t BgL_envz00_8460,
		obj_t BgL_loc1140z00_8461, obj_t BgL_to1141z00_8462)
	{
		{	/* SawMill/defs.scm 28 */
			{	/* SawMill/defs.scm 28 */
				BgL_rtl_retz00_bglt BgL_new1470z00_10000;

				{	/* SawMill/defs.scm 28 */
					BgL_rtl_retz00_bglt BgL_new1469z00_10001;

					BgL_new1469z00_10001 =
						((BgL_rtl_retz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_retz00_bgl))));
					{	/* SawMill/defs.scm 28 */
						long BgL_arg2090z00_10002;

						BgL_arg2090z00_10002 = BGL_CLASS_NUM(BGl_rtl_retz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1469z00_10001),
							BgL_arg2090z00_10002);
					}
					BgL_new1470z00_10000 = BgL_new1469z00_10001;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1470z00_10000)))->BgL_locz00) =
					((obj_t) BgL_loc1140z00_8461), BUNSPEC);
				((((BgL_rtl_retz00_bglt) COBJECT(BgL_new1470z00_10000))->BgL_toz00) =
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_to1141z00_8462)),
					BUNSPEC);
				return BgL_new1470z00_10000;
			}
		}

	}



/* &lambda2099 */
	obj_t BGl_z62lambda2099z62zzsaw_defsz00(obj_t BgL_envz00_8463,
		obj_t BgL_oz00_8464, obj_t BgL_vz00_8465)
	{
		{	/* SawMill/defs.scm 28 */
			return
				((((BgL_rtl_retz00_bglt) COBJECT(
							((BgL_rtl_retz00_bglt) BgL_oz00_8464)))->BgL_toz00) =
				((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_vz00_8465)), BUNSPEC);
		}

	}



/* &lambda2098 */
	BgL_blockz00_bglt BGl_z62lambda2098z62zzsaw_defsz00(obj_t BgL_envz00_8466,
		obj_t BgL_oz00_8467)
	{
		{	/* SawMill/defs.scm 28 */
			return
				(((BgL_rtl_retz00_bglt) COBJECT(
						((BgL_rtl_retz00_bglt) BgL_oz00_8467)))->BgL_toz00);
		}

	}



/* &<@anonymous:2082> */
	obj_t BGl_z62zc3z04anonymousza32082ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8468,
		obj_t BgL_new1138z00_8469)
	{
		{	/* SawMill/defs.scm 27 */
			{
				BgL_rtl_retblockz00_bglt BgL_auxz00_14772;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_retblockz00_bglt) BgL_new1138z00_8469))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14772 = ((BgL_rtl_retblockz00_bglt) BgL_new1138z00_8469);
				return ((obj_t) BgL_auxz00_14772);
			}
		}

	}



/* &lambda2080 */
	BgL_rtl_retblockz00_bglt BGl_z62lambda2080z62zzsaw_defsz00(obj_t
		BgL_envz00_8470)
	{
		{	/* SawMill/defs.scm 27 */
			{	/* SawMill/defs.scm 27 */
				BgL_rtl_retblockz00_bglt BgL_new1137z00_10007;

				BgL_new1137z00_10007 =
					((BgL_rtl_retblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_retblockz00_bgl))));
				{	/* SawMill/defs.scm 27 */
					long BgL_arg2081z00_10008;

					BgL_arg2081z00_10008 =
						BGL_CLASS_NUM(BGl_rtl_retblockz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1137z00_10007), BgL_arg2081z00_10008);
				}
				return BgL_new1137z00_10007;
			}
		}

	}



/* &lambda2078 */
	BgL_rtl_retblockz00_bglt BGl_z62lambda2078z62zzsaw_defsz00(obj_t
		BgL_envz00_8471, obj_t BgL_loc1136z00_8472)
	{
		{	/* SawMill/defs.scm 27 */
			{	/* SawMill/defs.scm 27 */
				BgL_rtl_retblockz00_bglt BgL_new1468z00_10009;

				{	/* SawMill/defs.scm 27 */
					BgL_rtl_retblockz00_bglt BgL_new1467z00_10010;

					BgL_new1467z00_10010 =
						((BgL_rtl_retblockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_retblockz00_bgl))));
					{	/* SawMill/defs.scm 27 */
						long BgL_arg2079z00_10011;

						BgL_arg2079z00_10011 =
							BGL_CLASS_NUM(BGl_rtl_retblockz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1467z00_10010),
							BgL_arg2079z00_10011);
					}
					BgL_new1468z00_10009 = BgL_new1467z00_10010;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1468z00_10009)))->BgL_locz00) =
					((obj_t) BgL_loc1136z00_8472), BUNSPEC);
				return BgL_new1468z00_10009;
			}
		}

	}



/* &<@anonymous:2070> */
	obj_t BGl_z62zc3z04anonymousza32070ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8473,
		obj_t BgL_new1134z00_8474)
	{
		{	/* SawMill/defs.scm 26 */
			{
				BgL_rtl_failz00_bglt BgL_auxz00_14788;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_failz00_bglt) BgL_new1134z00_8474))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14788 = ((BgL_rtl_failz00_bglt) BgL_new1134z00_8474);
				return ((obj_t) BgL_auxz00_14788);
			}
		}

	}



/* &lambda2068 */
	BgL_rtl_failz00_bglt BGl_z62lambda2068z62zzsaw_defsz00(obj_t BgL_envz00_8475)
	{
		{	/* SawMill/defs.scm 26 */
			{	/* SawMill/defs.scm 26 */
				BgL_rtl_failz00_bglt BgL_new1133z00_10013;

				BgL_new1133z00_10013 =
					((BgL_rtl_failz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_failz00_bgl))));
				{	/* SawMill/defs.scm 26 */
					long BgL_arg2069z00_10014;

					BgL_arg2069z00_10014 = BGL_CLASS_NUM(BGl_rtl_failz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1133z00_10013), BgL_arg2069z00_10014);
				}
				return BgL_new1133z00_10013;
			}
		}

	}



/* &lambda2066 */
	BgL_rtl_failz00_bglt BGl_z62lambda2066z62zzsaw_defsz00(obj_t BgL_envz00_8476,
		obj_t BgL_loc1131z00_8477)
	{
		{	/* SawMill/defs.scm 26 */
			{	/* SawMill/defs.scm 26 */
				BgL_rtl_failz00_bglt BgL_new1466z00_10015;

				{	/* SawMill/defs.scm 26 */
					BgL_rtl_failz00_bglt BgL_new1465z00_10016;

					BgL_new1465z00_10016 =
						((BgL_rtl_failz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_failz00_bgl))));
					{	/* SawMill/defs.scm 26 */
						long BgL_arg2067z00_10017;

						BgL_arg2067z00_10017 = BGL_CLASS_NUM(BGl_rtl_failz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1465z00_10016),
							BgL_arg2067z00_10017);
					}
					BgL_new1466z00_10015 = BgL_new1465z00_10016;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1466z00_10015)))->BgL_locz00) =
					((obj_t) BgL_loc1131z00_8477), BUNSPEC);
				return BgL_new1466z00_10015;
			}
		}

	}



/* &<@anonymous:2060> */
	obj_t BGl_z62zc3z04anonymousza32060ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8478,
		obj_t BgL_new1129z00_8479)
	{
		{	/* SawMill/defs.scm 25 */
			{
				BgL_rtl_jumpexitz00_bglt BgL_auxz00_14804;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_jumpexitz00_bglt) BgL_new1129z00_8479))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14804 = ((BgL_rtl_jumpexitz00_bglt) BgL_new1129z00_8479);
				return ((obj_t) BgL_auxz00_14804);
			}
		}

	}



/* &lambda2058 */
	BgL_rtl_jumpexitz00_bglt BGl_z62lambda2058z62zzsaw_defsz00(obj_t
		BgL_envz00_8480)
	{
		{	/* SawMill/defs.scm 25 */
			{	/* SawMill/defs.scm 25 */
				BgL_rtl_jumpexitz00_bglt BgL_new1127z00_10019;

				BgL_new1127z00_10019 =
					((BgL_rtl_jumpexitz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_jumpexitz00_bgl))));
				{	/* SawMill/defs.scm 25 */
					long BgL_arg2059z00_10020;

					BgL_arg2059z00_10020 =
						BGL_CLASS_NUM(BGl_rtl_jumpexitz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1127z00_10019), BgL_arg2059z00_10020);
				}
				return BgL_new1127z00_10019;
			}
		}

	}



/* &lambda2056 */
	BgL_rtl_jumpexitz00_bglt BGl_z62lambda2056z62zzsaw_defsz00(obj_t
		BgL_envz00_8481, obj_t BgL_loc1126z00_8482)
	{
		{	/* SawMill/defs.scm 25 */
			{	/* SawMill/defs.scm 25 */
				BgL_rtl_jumpexitz00_bglt BgL_new1464z00_10021;

				{	/* SawMill/defs.scm 25 */
					BgL_rtl_jumpexitz00_bglt BgL_new1463z00_10022;

					BgL_new1463z00_10022 =
						((BgL_rtl_jumpexitz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_jumpexitz00_bgl))));
					{	/* SawMill/defs.scm 25 */
						long BgL_arg2057z00_10023;

						BgL_arg2057z00_10023 =
							BGL_CLASS_NUM(BGl_rtl_jumpexitz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1463z00_10022),
							BgL_arg2057z00_10023);
					}
					BgL_new1464z00_10021 = BgL_new1463z00_10022;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1464z00_10021)))->BgL_locz00) =
					((obj_t) BgL_loc1126z00_8482), BUNSPEC);
				return BgL_new1464z00_10021;
			}
		}

	}



/* &<@anonymous:2042> */
	obj_t BGl_z62zc3z04anonymousza32042ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8483,
		obj_t BgL_new1124z00_8484)
	{
		{	/* SawMill/defs.scm 24 */
			{
				BgL_rtl_returnz00_bglt BgL_auxz00_14820;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_returnz00_bglt) BgL_new1124z00_8484))))->
						BgL_locz00) = ((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_14824;

					{	/* SawMill/defs.scm 24 */
						obj_t BgL_classz00_10025;

						BgL_classz00_10025 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 24 */
							obj_t BgL__ortest_1117z00_10026;

							BgL__ortest_1117z00_10026 = BGL_CLASS_NIL(BgL_classz00_10025);
							if (CBOOL(BgL__ortest_1117z00_10026))
								{	/* SawMill/defs.scm 24 */
									BgL_auxz00_14824 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_10026);
								}
							else
								{	/* SawMill/defs.scm 24 */
									BgL_auxz00_14824 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_10025));
								}
						}
					}
					((((BgL_rtl_returnz00_bglt) COBJECT(
									((BgL_rtl_returnz00_bglt) BgL_new1124z00_8484)))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_14824), BUNSPEC);
				}
				BgL_auxz00_14820 = ((BgL_rtl_returnz00_bglt) BgL_new1124z00_8484);
				return ((obj_t) BgL_auxz00_14820);
			}
		}

	}



/* &lambda2040 */
	BgL_rtl_returnz00_bglt BGl_z62lambda2040z62zzsaw_defsz00(obj_t
		BgL_envz00_8485)
	{
		{	/* SawMill/defs.scm 24 */
			{	/* SawMill/defs.scm 24 */
				BgL_rtl_returnz00_bglt BgL_new1123z00_10027;

				BgL_new1123z00_10027 =
					((BgL_rtl_returnz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_returnz00_bgl))));
				{	/* SawMill/defs.scm 24 */
					long BgL_arg2041z00_10028;

					BgL_arg2041z00_10028 = BGL_CLASS_NUM(BGl_rtl_returnz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1123z00_10027), BgL_arg2041z00_10028);
				}
				return BgL_new1123z00_10027;
			}
		}

	}



/* &lambda2038 */
	BgL_rtl_returnz00_bglt BGl_z62lambda2038z62zzsaw_defsz00(obj_t
		BgL_envz00_8486, obj_t BgL_loc1121z00_8487, obj_t BgL_type1122z00_8488)
	{
		{	/* SawMill/defs.scm 24 */
			{	/* SawMill/defs.scm 24 */
				BgL_rtl_returnz00_bglt BgL_new1462z00_10030;

				{	/* SawMill/defs.scm 24 */
					BgL_rtl_returnz00_bglt BgL_new1461z00_10031;

					BgL_new1461z00_10031 =
						((BgL_rtl_returnz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_returnz00_bgl))));
					{	/* SawMill/defs.scm 24 */
						long BgL_arg2039z00_10032;

						BgL_arg2039z00_10032 =
							BGL_CLASS_NUM(BGl_rtl_returnz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1461z00_10031),
							BgL_arg2039z00_10032);
					}
					BgL_new1462z00_10030 = BgL_new1461z00_10031;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1462z00_10030)))->BgL_locz00) =
					((obj_t) BgL_loc1121z00_8487), BUNSPEC);
				((((BgL_rtl_returnz00_bglt) COBJECT(BgL_new1462z00_10030))->
						BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1122z00_8488)),
					BUNSPEC);
				return BgL_new1462z00_10030;
			}
		}

	}



/* &lambda2048 */
	obj_t BGl_z62lambda2048z62zzsaw_defsz00(obj_t BgL_envz00_8489,
		obj_t BgL_oz00_8490, obj_t BgL_vz00_8491)
	{
		{	/* SawMill/defs.scm 24 */
			return
				((((BgL_rtl_returnz00_bglt) COBJECT(
							((BgL_rtl_returnz00_bglt) BgL_oz00_8490)))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8491)), BUNSPEC);
		}

	}



/* &lambda2047 */
	BgL_typez00_bglt BGl_z62lambda2047z62zzsaw_defsz00(obj_t BgL_envz00_8492,
		obj_t BgL_oz00_8493)
	{
		{	/* SawMill/defs.scm 24 */
			return
				(((BgL_rtl_returnz00_bglt) COBJECT(
						((BgL_rtl_returnz00_bglt) BgL_oz00_8493)))->BgL_typez00);
		}

	}



/* &<@anonymous:2030> */
	obj_t BGl_z62zc3z04anonymousza32030ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8494,
		obj_t BgL_new1119z00_8495)
	{
		{	/* SawMill/defs.scm 23 */
			{
				BgL_rtl_lastz00_bglt BgL_auxz00_14852;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt)
									((BgL_rtl_lastz00_bglt) BgL_new1119z00_8495))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14852 = ((BgL_rtl_lastz00_bglt) BgL_new1119z00_8495);
				return ((obj_t) BgL_auxz00_14852);
			}
		}

	}



/* &lambda2027 */
	BgL_rtl_lastz00_bglt BGl_z62lambda2027z62zzsaw_defsz00(obj_t BgL_envz00_8496)
	{
		{	/* SawMill/defs.scm 23 */
			{	/* SawMill/defs.scm 23 */
				BgL_rtl_lastz00_bglt BgL_new1118z00_10037;

				BgL_new1118z00_10037 =
					((BgL_rtl_lastz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_lastz00_bgl))));
				{	/* SawMill/defs.scm 23 */
					long BgL_arg2029z00_10038;

					BgL_arg2029z00_10038 = BGL_CLASS_NUM(BGl_rtl_lastz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1118z00_10037), BgL_arg2029z00_10038);
				}
				return BgL_new1118z00_10037;
			}
		}

	}



/* &lambda2025 */
	BgL_rtl_lastz00_bglt BGl_z62lambda2025z62zzsaw_defsz00(obj_t BgL_envz00_8497,
		obj_t BgL_loc1117z00_8498)
	{
		{	/* SawMill/defs.scm 23 */
			{	/* SawMill/defs.scm 23 */
				BgL_rtl_lastz00_bglt BgL_new1460z00_10039;

				{	/* SawMill/defs.scm 23 */
					BgL_rtl_lastz00_bglt BgL_new1458z00_10040;

					BgL_new1458z00_10040 =
						((BgL_rtl_lastz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_lastz00_bgl))));
					{	/* SawMill/defs.scm 23 */
						long BgL_arg2026z00_10041;

						BgL_arg2026z00_10041 = BGL_CLASS_NUM(BGl_rtl_lastz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1458z00_10040),
							BgL_arg2026z00_10041);
					}
					BgL_new1460z00_10039 = BgL_new1458z00_10040;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1460z00_10039)))->BgL_locz00) =
					((obj_t) BgL_loc1117z00_8498), BUNSPEC);
				return BgL_new1460z00_10039;
			}
		}

	}



/* &<@anonymous:2011> */
	obj_t BGl_z62zc3z04anonymousza32011ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8499,
		obj_t BgL_new1115z00_8500)
	{
		{	/* SawMill/defs.scm 21 */
			{
				BgL_rtl_funz00_bglt BgL_auxz00_14868;

				((((BgL_rtl_funz00_bglt) COBJECT(
								((BgL_rtl_funz00_bglt) BgL_new1115z00_8500)))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14868 = ((BgL_rtl_funz00_bglt) BgL_new1115z00_8500);
				return ((obj_t) BgL_auxz00_14868);
			}
		}

	}



/* &lambda2009 */
	BgL_rtl_funz00_bglt BGl_z62lambda2009z62zzsaw_defsz00(obj_t BgL_envz00_8501)
	{
		{	/* SawMill/defs.scm 21 */
			{	/* SawMill/defs.scm 21 */
				BgL_rtl_funz00_bglt BgL_new1114z00_10043;

				BgL_new1114z00_10043 =
					((BgL_rtl_funz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_funz00_bgl))));
				{	/* SawMill/defs.scm 21 */
					long BgL_arg2010z00_10044;

					BgL_arg2010z00_10044 = BGL_CLASS_NUM(BGl_rtl_funz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1114z00_10043), BgL_arg2010z00_10044);
				}
				return BgL_new1114z00_10043;
			}
		}

	}



/* &lambda2007 */
	BgL_rtl_funz00_bglt BGl_z62lambda2007z62zzsaw_defsz00(obj_t BgL_envz00_8502,
		obj_t BgL_loc1113z00_8503)
	{
		{	/* SawMill/defs.scm 21 */
			{	/* SawMill/defs.scm 21 */
				BgL_rtl_funz00_bglt BgL_new1457z00_10045;

				{	/* SawMill/defs.scm 21 */
					BgL_rtl_funz00_bglt BgL_new1456z00_10046;

					BgL_new1456z00_10046 =
						((BgL_rtl_funz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_funz00_bgl))));
					{	/* SawMill/defs.scm 21 */
						long BgL_arg2008z00_10047;

						BgL_arg2008z00_10047 = BGL_CLASS_NUM(BGl_rtl_funz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1456z00_10046),
							BgL_arg2008z00_10047);
					}
					BgL_new1457z00_10045 = BgL_new1456z00_10046;
				}
				((((BgL_rtl_funz00_bglt) COBJECT(BgL_new1457z00_10045))->BgL_locz00) =
					((obj_t) BgL_loc1113z00_8503), BUNSPEC);
				return BgL_new1457z00_10045;
			}
		}

	}



/* &<@anonymous:2018> */
	obj_t BGl_z62zc3z04anonymousza32018ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8504)
	{
		{	/* SawMill/defs.scm 21 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda2017 */
	obj_t BGl_z62lambda2017z62zzsaw_defsz00(obj_t BgL_envz00_8505,
		obj_t BgL_oz00_8506, obj_t BgL_vz00_8507)
	{
		{	/* SawMill/defs.scm 21 */
			return
				((((BgL_rtl_funz00_bglt) COBJECT(
							((BgL_rtl_funz00_bglt) BgL_oz00_8506)))->BgL_locz00) =
				((obj_t) BgL_vz00_8507), BUNSPEC);
		}

	}



/* &lambda2016 */
	obj_t BGl_z62lambda2016z62zzsaw_defsz00(obj_t BgL_envz00_8508,
		obj_t BgL_oz00_8509)
	{
		{	/* SawMill/defs.scm 21 */
			return
				(((BgL_rtl_funz00_bglt) COBJECT(
						((BgL_rtl_funz00_bglt) BgL_oz00_8509)))->BgL_locz00);
		}

	}



/* &<@anonymous:1955> */
	obj_t BGl_z62zc3z04anonymousza31955ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8510,
		obj_t BgL_new1111z00_8511)
	{
		{	/* SawMill/defs.scm 12 */
			{
				BgL_rtl_regz00_bglt BgL_auxz00_14887;

				{
					BgL_typez00_bglt BgL_auxz00_14888;

					{	/* SawMill/defs.scm 12 */
						obj_t BgL_classz00_10051;

						BgL_classz00_10051 = BGl_typez00zztype_typez00;
						{	/* SawMill/defs.scm 12 */
							obj_t BgL__ortest_1117z00_10052;

							BgL__ortest_1117z00_10052 = BGL_CLASS_NIL(BgL_classz00_10051);
							if (CBOOL(BgL__ortest_1117z00_10052))
								{	/* SawMill/defs.scm 12 */
									BgL_auxz00_14888 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_10052);
								}
							else
								{	/* SawMill/defs.scm 12 */
									BgL_auxz00_14888 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_10051));
								}
						}
					}
					((((BgL_rtl_regz00_bglt) COBJECT(
									((BgL_rtl_regz00_bglt) BgL_new1111z00_8511)))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_14888), BUNSPEC);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1111z00_8511)))->BgL_varz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1111z00_8511)))->BgL_onexprzf3zf3) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1111z00_8511)))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1111z00_8511)))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1111z00_8511)))->BgL_debugnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1111z00_8511)))->BgL_hardwarez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				BgL_auxz00_14887 = ((BgL_rtl_regz00_bglt) BgL_new1111z00_8511);
				return ((obj_t) BgL_auxz00_14887);
			}
		}

	}



/* &lambda1953 */
	BgL_rtl_regz00_bglt BGl_z62lambda1953z62zzsaw_defsz00(obj_t BgL_envz00_8512)
	{
		{	/* SawMill/defs.scm 12 */
			{	/* SawMill/defs.scm 12 */
				BgL_rtl_regz00_bglt BgL_new1110z00_10053;

				BgL_new1110z00_10053 =
					((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_regz00_bgl))));
				{	/* SawMill/defs.scm 12 */
					long BgL_arg1954z00_10054;

					BgL_arg1954z00_10054 = BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1110z00_10053), BgL_arg1954z00_10054);
				}
				{	/* SawMill/defs.scm 12 */
					BgL_objectz00_bglt BgL_tmpz00_14915;

					BgL_tmpz00_14915 = ((BgL_objectz00_bglt) BgL_new1110z00_10053);
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14915, BFALSE);
				}
				((BgL_objectz00_bglt) BgL_new1110z00_10053);
				return BgL_new1110z00_10053;
			}
		}

	}



/* &lambda1951 */
	BgL_rtl_regz00_bglt BGl_z62lambda1951z62zzsaw_defsz00(obj_t BgL_envz00_8513,
		obj_t BgL_type1103z00_8514, obj_t BgL_var1104z00_8515,
		obj_t BgL_onexprzf31105zf3_8516, obj_t BgL_name1106z00_8517,
		obj_t BgL_key1107z00_8518, obj_t BgL_debugname1108z00_8519,
		obj_t BgL_hardware1109z00_8520)
	{
		{	/* SawMill/defs.scm 12 */
			{	/* SawMill/defs.scm 12 */
				BgL_rtl_regz00_bglt BgL_new1455z00_10056;

				{	/* SawMill/defs.scm 12 */
					BgL_rtl_regz00_bglt BgL_new1454z00_10057;

					BgL_new1454z00_10057 =
						((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regz00_bgl))));
					{	/* SawMill/defs.scm 12 */
						long BgL_arg1952z00_10058;

						BgL_arg1952z00_10058 = BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1454z00_10057),
							BgL_arg1952z00_10058);
					}
					{	/* SawMill/defs.scm 12 */
						BgL_objectz00_bglt BgL_tmpz00_14923;

						BgL_tmpz00_14923 = ((BgL_objectz00_bglt) BgL_new1454z00_10057);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_14923, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1454z00_10057);
					BgL_new1455z00_10056 = BgL_new1454z00_10057;
				}
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1455z00_10056))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1103z00_8514)),
					BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1455z00_10056))->BgL_varz00) =
					((obj_t) BgL_var1104z00_8515), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1455z00_10056))->
						BgL_onexprzf3zf3) = ((obj_t) BgL_onexprzf31105zf3_8516), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1455z00_10056))->BgL_namez00) =
					((obj_t) BgL_name1106z00_8517), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1455z00_10056))->BgL_keyz00) =
					((obj_t) BgL_key1107z00_8518), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1455z00_10056))->
						BgL_debugnamez00) = ((obj_t) BgL_debugname1108z00_8519), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1455z00_10056))->
						BgL_hardwarez00) = ((obj_t) BgL_hardware1109z00_8520), BUNSPEC);
				return BgL_new1455z00_10056;
			}
		}

	}



/* &<@anonymous:2000> */
	obj_t BGl_z62zc3z04anonymousza32000ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8521)
	{
		{	/* SawMill/defs.scm 12 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1999 */
	obj_t BGl_z62lambda1999z62zzsaw_defsz00(obj_t BgL_envz00_8522,
		obj_t BgL_oz00_8523, obj_t BgL_vz00_8524)
	{
		{	/* SawMill/defs.scm 12 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_8523)))->BgL_hardwarez00) =
				((obj_t) BgL_vz00_8524), BUNSPEC);
		}

	}



/* &lambda1998 */
	obj_t BGl_z62lambda1998z62zzsaw_defsz00(obj_t BgL_envz00_8525,
		obj_t BgL_oz00_8526)
	{
		{	/* SawMill/defs.scm 12 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_8526)))->BgL_hardwarez00);
		}

	}



/* &<@anonymous:1993> */
	obj_t BGl_z62zc3z04anonymousza31993ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8527)
	{
		{	/* SawMill/defs.scm 12 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1992 */
	obj_t BGl_z62lambda1992z62zzsaw_defsz00(obj_t BgL_envz00_8528,
		obj_t BgL_oz00_8529, obj_t BgL_vz00_8530)
	{
		{	/* SawMill/defs.scm 12 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_8529)))->BgL_debugnamez00) =
				((obj_t) BgL_vz00_8530), BUNSPEC);
		}

	}



/* &lambda1991 */
	obj_t BGl_z62lambda1991z62zzsaw_defsz00(obj_t BgL_envz00_8531,
		obj_t BgL_oz00_8532)
	{
		{	/* SawMill/defs.scm 12 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_8532)))->BgL_debugnamez00);
		}

	}



/* &<@anonymous:1986> */
	obj_t BGl_z62zc3z04anonymousza31986ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8533)
	{
		{	/* SawMill/defs.scm 12 */
			{	/* SawMill/defs.scm 17 */

				{	/* SawMill/defs.scm 17 */

					return BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
				}
			}
		}

	}



/* &lambda1985 */
	obj_t BGl_z62lambda1985z62zzsaw_defsz00(obj_t BgL_envz00_8534,
		obj_t BgL_oz00_8535, obj_t BgL_vz00_8536)
	{
		{	/* SawMill/defs.scm 12 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_8535)))->BgL_keyz00) =
				((obj_t) BgL_vz00_8536), BUNSPEC);
		}

	}



/* &lambda1984 */
	obj_t BGl_z62lambda1984z62zzsaw_defsz00(obj_t BgL_envz00_8537,
		obj_t BgL_oz00_8538)
	{
		{	/* SawMill/defs.scm 12 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_8538)))->BgL_keyz00);
		}

	}



/* &<@anonymous:1979> */
	obj_t BGl_z62zc3z04anonymousza31979ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8539)
	{
		{	/* SawMill/defs.scm 12 */
			{	/* SawMill/defs.scm 16 */

				{	/* SawMill/defs.scm 16 */

					return BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
				}
			}
		}

	}



/* &lambda1978 */
	obj_t BGl_z62lambda1978z62zzsaw_defsz00(obj_t BgL_envz00_8540,
		obj_t BgL_oz00_8541, obj_t BgL_vz00_8542)
	{
		{	/* SawMill/defs.scm 12 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_8541)))->BgL_namez00) =
				((obj_t) BgL_vz00_8542), BUNSPEC);
		}

	}



/* &lambda1977 */
	obj_t BGl_z62lambda1977z62zzsaw_defsz00(obj_t BgL_envz00_8543,
		obj_t BgL_oz00_8544)
	{
		{	/* SawMill/defs.scm 12 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_8544)))->BgL_namez00);
		}

	}



/* &<@anonymous:1972> */
	obj_t BGl_z62zc3z04anonymousza31972ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8545)
	{
		{	/* SawMill/defs.scm 12 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1971 */
	obj_t BGl_z62lambda1971z62zzsaw_defsz00(obj_t BgL_envz00_8546,
		obj_t BgL_oz00_8547, obj_t BgL_vz00_8548)
	{
		{	/* SawMill/defs.scm 12 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_8547)))->BgL_onexprzf3zf3) =
				((obj_t) BgL_vz00_8548), BUNSPEC);
		}

	}



/* &lambda1970 */
	obj_t BGl_z62lambda1970z62zzsaw_defsz00(obj_t BgL_envz00_8549,
		obj_t BgL_oz00_8550)
	{
		{	/* SawMill/defs.scm 12 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_8550)))->BgL_onexprzf3zf3);
		}

	}



/* &lambda1965 */
	obj_t BGl_z62lambda1965z62zzsaw_defsz00(obj_t BgL_envz00_8551,
		obj_t BgL_oz00_8552, obj_t BgL_vz00_8553)
	{
		{	/* SawMill/defs.scm 12 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_8552)))->BgL_varz00) =
				((obj_t) BgL_vz00_8553), BUNSPEC);
		}

	}



/* &lambda1964 */
	obj_t BGl_z62lambda1964z62zzsaw_defsz00(obj_t BgL_envz00_8554,
		obj_t BgL_oz00_8555)
	{
		{	/* SawMill/defs.scm 12 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_8555)))->BgL_varz00);
		}

	}



/* &lambda1960 */
	obj_t BGl_z62lambda1960z62zzsaw_defsz00(obj_t BgL_envz00_8556,
		obj_t BgL_oz00_8557, obj_t BgL_vz00_8558)
	{
		{	/* SawMill/defs.scm 12 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_8557)))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_vz00_8558)), BUNSPEC);
		}

	}



/* &lambda1959 */
	BgL_typez00_bglt BGl_z62lambda1959z62zzsaw_defsz00(obj_t BgL_envz00_8559,
		obj_t BgL_oz00_8560)
	{
		{	/* SawMill/defs.scm 12 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_8560)))->BgL_typez00);
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.scm 1 */
			BGl_registerzd2genericz12zc0zz__objectz00(BGl_dumpzd2envzd2zzsaw_defsz00,
				BGl_proc3355z00zzsaw_defsz00, BFALSE, BGl_string3356z00zzsaw_defsz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_dumpzd2funzd2envz00zzsaw_defsz00, BGl_proc3357z00zzsaw_defsz00,
				BGl_rtl_funz00zzsaw_defsz00, BGl_string3358z00zzsaw_defsz00);
		}

	}



/* &dump-fun1794 */
	obj_t BGl_z62dumpzd2fun1794zb0zzsaw_defsz00(obj_t BgL_envz00_8563,
		obj_t BgL_oz00_8564, obj_t BgL_destz00_8565, obj_t BgL_argsz00_8566,
		obj_t BgL_pz00_8567, obj_t BgL_mz00_8568)
	{
		{	/* SawMill/defs.scm 314 */
			return BUNSPEC;
		}

	}



/* &dump1786 */
	obj_t BGl_z62dump1786z62zzsaw_defsz00(obj_t BgL_envz00_8569,
		obj_t BgL_oz00_8570, obj_t BgL_pz00_8571, obj_t BgL_mz00_8572)
	{
		{	/* SawMill/defs.scm 193 */
			{	/* SawMill/defs.scm 195 */
				int BgL_mz00_10076;

				BgL_mz00_10076 = CINT(BgL_mz00_8572);
				{	/* SawMill/defs.scm 195 */
					bool_t BgL_test4354z00_14972;

					if (STRINGP(BgL_oz00_8570))
						{	/* SawMill/defs.scm 195 */
							BgL_test4354z00_14972 = ((bool_t) 1);
						}
					else
						{	/* SawMill/defs.scm 195 */
							if (BGl_numberzf3zf3zz__r4_numbers_6_5z00(BgL_oz00_8570))
								{	/* SawMill/defs.scm 195 */
									BgL_test4354z00_14972 = ((bool_t) 1);
								}
							else
								{	/* SawMill/defs.scm 195 */
									BgL_test4354z00_14972 = SYMBOLP(BgL_oz00_8570);
								}
						}
					if (BgL_test4354z00_14972)
						{	/* SawMill/defs.scm 195 */
							return bgl_display_obj(BgL_oz00_8570, ((obj_t) BgL_pz00_8571));
						}
					else
						{	/* SawMill/defs.scm 195 */
							if (PAIRP(BgL_oz00_8570))
								{
									obj_t BgL_l1664z00_10078;

									{	/* SawMill/defs.scm 198 */
										bool_t BgL_tmpz00_14982;

										BgL_l1664z00_10078 = BgL_oz00_8570;
									BgL_zc3z04anonymousza32814ze3z87_10077:
										if (PAIRP(BgL_l1664z00_10078))
											{	/* SawMill/defs.scm 198 */
												{	/* SawMill/defs.scm 199 */
													obj_t BgL_oz00_10079;

													BgL_oz00_10079 = CAR(BgL_l1664z00_10078);
													BGl_dumpz00zzsaw_defsz00(BgL_oz00_10079,
														((obj_t) BgL_pz00_8571), BgL_mz00_10076);
													{	/* SawMill/defs.scm 200 */
														obj_t BgL_tmpz00_14988;

														BgL_tmpz00_14988 = ((obj_t) BgL_pz00_8571);
														bgl_display_char(((unsigned char) 10),
															BgL_tmpz00_14988);
													}
													if (((long) (BgL_mz00_10076) > 0L))
														{	/* SawMill/defs.scm 201 */
															BGl_dumpzd2marginzd2zzsaw_defsz00(
																((obj_t) BgL_pz00_8571), BgL_mz00_10076);
														}
													else
														{	/* SawMill/defs.scm 203 */
															obj_t BgL_tmpz00_14996;

															BgL_tmpz00_14996 = ((obj_t) BgL_pz00_8571);
															bgl_display_char(((unsigned char) 10),
																BgL_tmpz00_14996);
												}}
												{
													obj_t BgL_l1664z00_14999;

													BgL_l1664z00_14999 = CDR(BgL_l1664z00_10078);
													BgL_l1664z00_10078 = BgL_l1664z00_14999;
													goto BgL_zc3z04anonymousza32814ze3z87_10077;
												}
											}
										else
											{	/* SawMill/defs.scm 198 */
												BgL_tmpz00_14982 = ((bool_t) 1);
											}
										return BBOOL(BgL_tmpz00_14982);
									}
								}
							else
								{	/* SawMill/defs.scm 206 */
									obj_t BgL_list2819z00_10080;

									BgL_list2819z00_10080 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_pz00_8571), BNIL);
									return
										BGl_writez00zz__r4_output_6_10_3z00(BgL_oz00_8570,
										BgL_list2819z00_10080);
								}
						}
				}
			}
		}

	}



/* dump */
	BGL_EXPORTED_DEF obj_t BGl_dumpz00zzsaw_defsz00(obj_t BgL_oz00_460,
		obj_t BgL_pz00_461, int BgL_mz00_462)
	{
		{	/* SawMill/defs.scm 193 */
			if (BGL_OBJECTP(BgL_oz00_460))
				{	/* SawMill/defs.scm 193 */
					obj_t BgL_method1787z00_3744;

					{	/* SawMill/defs.scm 193 */
						obj_t BgL_res3071z00_6762;

						{	/* SawMill/defs.scm 193 */
							long BgL_objzd2classzd2numz00_6733;

							BgL_objzd2classzd2numz00_6733 =
								BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_oz00_460));
							{	/* SawMill/defs.scm 193 */
								obj_t BgL_arg1811z00_6734;

								BgL_arg1811z00_6734 =
									PROCEDURE_REF(BGl_dumpzd2envzd2zzsaw_defsz00, (int) (1L));
								{	/* SawMill/defs.scm 193 */
									int BgL_offsetz00_6737;

									BgL_offsetz00_6737 = (int) (BgL_objzd2classzd2numz00_6733);
									{	/* SawMill/defs.scm 193 */
										long BgL_offsetz00_6738;

										BgL_offsetz00_6738 =
											((long) (BgL_offsetz00_6737) - OBJECT_TYPE);
										{	/* SawMill/defs.scm 193 */
											long BgL_modz00_6739;

											BgL_modz00_6739 =
												(BgL_offsetz00_6738 >> (int) ((long) ((int) (4L))));
											{	/* SawMill/defs.scm 193 */
												long BgL_restz00_6741;

												BgL_restz00_6741 =
													(BgL_offsetz00_6738 &
													(long) (
														(int) (
															((long) (
																	(int) (
																		(1L <<
																			(int) ((long) ((int) (4L)))))) - 1L))));
												{	/* SawMill/defs.scm 193 */

													{	/* SawMill/defs.scm 193 */
														obj_t BgL_bucketz00_6743;

														BgL_bucketz00_6743 =
															VECTOR_REF(
															((obj_t) BgL_arg1811z00_6734), BgL_modz00_6739);
														BgL_res3071z00_6762 =
															VECTOR_REF(
															((obj_t) BgL_bucketz00_6743), BgL_restz00_6741);
						}}}}}}}}
						BgL_method1787z00_3744 = BgL_res3071z00_6762;
					}
					return
						BGL_PROCEDURE_CALL3(BgL_method1787z00_3744, BgL_oz00_460,
						BgL_pz00_461, BINT(BgL_mz00_462));
				}
			else
				{	/* SawMill/defs.scm 193 */
					obj_t BgL_fun2822z00_3745;

					BgL_fun2822z00_3745 =
						PROCEDURE_REF(BGl_dumpzd2envzd2zzsaw_defsz00, (int) (0L));
					return
						BGL_PROCEDURE_CALL3(BgL_fun2822z00_3745, BgL_oz00_460, BgL_pz00_461,
						BINT(BgL_mz00_462));
				}
		}

	}



/* &dump */
	obj_t BGl_z62dumpz62zzsaw_defsz00(obj_t BgL_envz00_8573, obj_t BgL_oz00_8574,
		obj_t BgL_pz00_8575, obj_t BgL_mz00_8576)
	{
		{	/* SawMill/defs.scm 193 */
			return
				BGl_dumpz00zzsaw_defsz00(BgL_oz00_8574, BgL_pz00_8575,
				CINT(BgL_mz00_8576));
		}

	}



/* dump-fun */
	BGL_EXPORTED_DEF obj_t BGl_dumpzd2funzd2zzsaw_defsz00(BgL_rtl_funz00_bglt
		BgL_oz00_483, obj_t BgL_destz00_484, obj_t BgL_argsz00_485,
		obj_t BgL_pz00_486, obj_t BgL_mz00_487)
	{
		{	/* SawMill/defs.scm 314 */
			{	/* SawMill/defs.scm 314 */
				obj_t BgL_method1796z00_3746;

				{	/* SawMill/defs.scm 314 */
					obj_t BgL_res3077z00_6794;

					{	/* SawMill/defs.scm 314 */
						long BgL_objzd2classzd2numz00_6765;

						BgL_objzd2classzd2numz00_6765 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_oz00_483));
						{	/* SawMill/defs.scm 314 */
							obj_t BgL_arg1811z00_6766;

							BgL_arg1811z00_6766 =
								PROCEDURE_REF(BGl_dumpzd2funzd2envz00zzsaw_defsz00, (int) (1L));
							{	/* SawMill/defs.scm 314 */
								int BgL_offsetz00_6769;

								BgL_offsetz00_6769 = (int) (BgL_objzd2classzd2numz00_6765);
								{	/* SawMill/defs.scm 314 */
									long BgL_offsetz00_6770;

									BgL_offsetz00_6770 =
										((long) (BgL_offsetz00_6769) - OBJECT_TYPE);
									{	/* SawMill/defs.scm 314 */
										long BgL_modz00_6771;

										BgL_modz00_6771 =
											(BgL_offsetz00_6770 >> (int) ((long) ((int) (4L))));
										{	/* SawMill/defs.scm 314 */
											long BgL_restz00_6773;

											BgL_restz00_6773 =
												(BgL_offsetz00_6770 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawMill/defs.scm 314 */

												{	/* SawMill/defs.scm 314 */
													obj_t BgL_bucketz00_6775;

													BgL_bucketz00_6775 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_6766), BgL_modz00_6771);
													BgL_res3077z00_6794 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_6775), BgL_restz00_6773);
					}}}}}}}}
					BgL_method1796z00_3746 = BgL_res3077z00_6794;
				}
				return
					BGL_PROCEDURE_CALL5(BgL_method1796z00_3746,
					((obj_t) BgL_oz00_483), BgL_destz00_484, BgL_argsz00_485,
					BgL_pz00_486, BgL_mz00_487);
			}
		}

	}



/* &dump-fun */
	obj_t BGl_z62dumpzd2funzb0zzsaw_defsz00(obj_t BgL_envz00_8577,
		obj_t BgL_oz00_8578, obj_t BgL_destz00_8579, obj_t BgL_argsz00_8580,
		obj_t BgL_pz00_8581, obj_t BgL_mz00_8582)
	{
		{	/* SawMill/defs.scm 314 */
			return
				BGl_dumpzd2funzd2zzsaw_defsz00(
				((BgL_rtl_funz00_bglt) BgL_oz00_8578), BgL_destz00_8579,
				BgL_argsz00_8580, BgL_pz00_8581, BgL_mz00_8582);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.scm 1 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00, BGl_rtl_insz00zzsaw_defsz00,
				BGl_proc3359z00zzsaw_defsz00, BGl_string3360z00zzsaw_defsz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00, BGl_rtl_regz00zzsaw_defsz00,
				BGl_proc3361z00zzsaw_defsz00, BGl_string3360z00zzsaw_defsz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2envzd2zzsaw_defsz00, BGl_blockz00zzsaw_defsz00,
				BGl_proc3362z00zzsaw_defsz00, BGl_string3363z00zzsaw_defsz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2envzd2zzsaw_defsz00, BGl_rtl_insz00zzsaw_defsz00,
				BGl_proc3364z00zzsaw_defsz00, BGl_string3363z00zzsaw_defsz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2envzd2zzsaw_defsz00, BGl_rtl_regz00zzsaw_defsz00,
				BGl_proc3365z00zzsaw_defsz00, BGl_string3363z00zzsaw_defsz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2funzd2envz00zzsaw_defsz00, BGl_rtl_funz00zzsaw_defsz00,
				BGl_proc3366z00zzsaw_defsz00, BGl_string3367z00zzsaw_defsz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2funzd2envz00zzsaw_defsz00, BGl_rtl_loadiz00zzsaw_defsz00,
				BGl_proc3368z00zzsaw_defsz00, BGl_string3367z00zzsaw_defsz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2funzd2envz00zzsaw_defsz00, BGl_rtl_movz00zzsaw_defsz00,
				BGl_proc3369z00zzsaw_defsz00, BGl_string3367z00zzsaw_defsz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2funzd2envz00zzsaw_defsz00, BGl_rtl_loadgz00zzsaw_defsz00,
				BGl_proc3370z00zzsaw_defsz00, BGl_string3367z00zzsaw_defsz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2funzd2envz00zzsaw_defsz00, BGl_rtl_loadfunz00zzsaw_defsz00,
				BGl_proc3371z00zzsaw_defsz00, BGl_string3367z00zzsaw_defsz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2funzd2envz00zzsaw_defsz00,
				BGl_rtl_globalrefz00zzsaw_defsz00, BGl_proc3372z00zzsaw_defsz00,
				BGl_string3367z00zzsaw_defsz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2funzd2envz00zzsaw_defsz00, BGl_rtl_switchz00zzsaw_defsz00,
				BGl_proc3373z00zzsaw_defsz00, BGl_string3367z00zzsaw_defsz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2funzd2envz00zzsaw_defsz00, BGl_rtl_ifeqz00zzsaw_defsz00,
				BGl_proc3374z00zzsaw_defsz00, BGl_string3367z00zzsaw_defsz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2funzd2envz00zzsaw_defsz00, BGl_rtl_ifnez00zzsaw_defsz00,
				BGl_proc3375z00zzsaw_defsz00, BGl_string3367z00zzsaw_defsz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2funzd2envz00zzsaw_defsz00, BGl_rtl_goz00zzsaw_defsz00,
				BGl_proc3376z00zzsaw_defsz00, BGl_string3367z00zzsaw_defsz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2funzd2envz00zzsaw_defsz00, BGl_rtl_callz00zzsaw_defsz00,
				BGl_proc3377z00zzsaw_defsz00, BGl_string3367z00zzsaw_defsz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2funzd2envz00zzsaw_defsz00, BGl_rtl_pragmaz00zzsaw_defsz00,
				BGl_proc3378z00zzsaw_defsz00, BGl_string3367z00zzsaw_defsz00);
		}

	}



/* &dump-fun-rtl_pragma1820 */
	obj_t BGl_z62dumpzd2funzd2rtl_pragma1820z62zzsaw_defsz00(obj_t
		BgL_envz00_8601, obj_t BgL_oz00_8602, obj_t BgL_destz00_8603,
		obj_t BgL_argsz00_8604, obj_t BgL_pz00_8605, obj_t BgL_mz00_8606)
	{
		{	/* SawMill/defs.scm 432 */
			BGl_showzd2funzd2zzsaw_defsz00(
				((BgL_rtl_funz00_bglt)
					((BgL_rtl_pragmaz00_bglt) BgL_oz00_8602)), BgL_destz00_8603,
				BgL_pz00_8605);
			{	/* SawMill/defs.scm 434 */
				obj_t BgL_tmpz00_15106;

				BgL_tmpz00_15106 = ((obj_t) BgL_pz00_8605);
				bgl_display_string(BGl_string3085z00zzsaw_defsz00, BgL_tmpz00_15106);
			}
			bgl_display_obj(
				(((BgL_rtl_pragmaz00_bglt) COBJECT(
							((BgL_rtl_pragmaz00_bglt) BgL_oz00_8602)))->BgL_formatz00),
				BgL_pz00_8605);
			{	/* SawMill/defs.scm 436 */
				obj_t BgL_tmpz00_15112;

				BgL_tmpz00_15112 = ((obj_t) BgL_pz00_8605);
				bgl_display_string(BGl_string3085z00zzsaw_defsz00, BgL_tmpz00_15112);
			}
			return
				BBOOL(BGl_dumpzd2argszd2zzsaw_defsz00(BgL_argsz00_8604, BgL_pz00_8605));
		}

	}



/* &dump-fun-rtl_call1818 */
	obj_t BGl_z62dumpzd2funzd2rtl_call1818z62zzsaw_defsz00(obj_t BgL_envz00_8607,
		obj_t BgL_oz00_8608, obj_t BgL_destz00_8609, obj_t BgL_argsz00_8610,
		obj_t BgL_pz00_8611, obj_t BgL_mz00_8612)
	{
		{	/* SawMill/defs.scm 412 */
			{	/* SawMill/defs.scm 414 */
				obj_t BgL_arg2925z00_10083;

				{	/* SawMill/defs.scm 414 */
					obj_t BgL_ouz00_10084;
					obj_t BgL_oaz00_10085;

					BgL_ouz00_10084 = BGl_za2userzd2shapezf3za2z21zzengine_paramz00;
					BgL_oaz00_10085 = BGl_za2accesszd2shapezf3za2z21zzengine_paramz00;
					BGl_za2userzd2shapezf3za2z21zzengine_paramz00 = BFALSE;
					BGl_za2accesszd2shapezf3za2z21zzengine_paramz00 = BFALSE;
					{	/* SawMill/defs.scm 418 */
						obj_t BgL_rz00_10086;

						{	/* SawMill/defs.scm 418 */
							BgL_globalz00_bglt BgL_arg2926z00_10087;

							BgL_arg2926z00_10087 =
								(((BgL_rtl_callz00_bglt) COBJECT(
										((BgL_rtl_callz00_bglt) BgL_oz00_8608)))->BgL_varz00);
							BgL_rz00_10086 =
								BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2926z00_10087));
						}
						BGl_za2userzd2shapezf3za2z21zzengine_paramz00 = BgL_ouz00_10084;
						BGl_za2accesszd2shapezf3za2z21zzengine_paramz00 = BgL_oaz00_10085;
						BgL_arg2925z00_10083 = BgL_rz00_10086;
					}
				}
				bgl_display_obj(BgL_arg2925z00_10083, BgL_pz00_8611);
			}
			{	/* SawMill/defs.scm 423 */
				bool_t BgL_test4361z00_15122;

				if (CBOOL(BgL_destz00_8609))
					{	/* SawMill/defs.scm 423 */
						BgL_test4361z00_15122 = CBOOL(BGl_debugzd2sawzd2zzsaw_defsz00);
					}
				else
					{	/* SawMill/defs.scm 423 */
						BgL_test4361z00_15122 = ((bool_t) 0);
					}
				if (BgL_test4361z00_15122)
					{	/* SawMill/defs.scm 423 */
						{	/* SawMill/defs.scm 424 */
							obj_t BgL_tmpz00_15126;

							BgL_tmpz00_15126 = ((obj_t) BgL_pz00_8611);
							bgl_display_string(BGl_string3090z00zzsaw_defsz00,
								BgL_tmpz00_15126);
						}
						BGl_dumpz00zzsaw_defsz00(BgL_destz00_8609, BgL_pz00_8611,
							(int) (0L));
						{	/* SawMill/defs.scm 426 */
							obj_t BgL_tmpz00_15131;

							BgL_tmpz00_15131 = ((obj_t) BgL_pz00_8611);
							bgl_display_string(BGl_string3089z00zzsaw_defsz00,
								BgL_tmpz00_15131);
					}}
				else
					{	/* SawMill/defs.scm 423 */
						BFALSE;
					}
			}
			return
				BBOOL(BGl_dumpzd2argszd2zzsaw_defsz00(BgL_argsz00_8610, BgL_pz00_8611));
		}

	}



/* &dump-fun-rtl_go1816 */
	obj_t BGl_z62dumpzd2funzd2rtl_go1816z62zzsaw_defsz00(obj_t BgL_envz00_8613,
		obj_t BgL_oz00_8614, obj_t BgL_destz00_8615, obj_t BgL_argsz00_8616,
		obj_t BgL_pz00_8617, obj_t BgL_mz00_8618)
	{
		{	/* SawMill/defs.scm 402 */
			BGl_showzd2funzd2zzsaw_defsz00(
				((BgL_rtl_funz00_bglt)
					((BgL_rtl_goz00_bglt) BgL_oz00_8614)), BgL_destz00_8615,
				BgL_pz00_8617);
			{	/* SawMill/defs.scm 405 */
				obj_t BgL_tmpz00_15139;

				BgL_tmpz00_15139 = ((obj_t) BgL_pz00_8617);
				bgl_display_string(BGl_string3085z00zzsaw_defsz00, BgL_tmpz00_15139);
			}
			{	/* SawMill/defs.scm 406 */
				int BgL_arg2923z00_10089;

				BgL_arg2923z00_10089 =
					(((BgL_blockz00_bglt) COBJECT(
							(((BgL_rtl_goz00_bglt) COBJECT(
										((BgL_rtl_goz00_bglt) BgL_oz00_8614)))->BgL_toz00)))->
					BgL_labelz00);
				bgl_display_obj(BINT(BgL_arg2923z00_10089), BgL_pz00_8617);
			}
			return
				BBOOL(BGl_dumpzd2argszd2zzsaw_defsz00(BgL_argsz00_8616, BgL_pz00_8617));
		}

	}



/* &dump-fun-rtl_ifne1814 */
	obj_t BGl_z62dumpzd2funzd2rtl_ifne1814z62zzsaw_defsz00(obj_t BgL_envz00_8619,
		obj_t BgL_oz00_8620, obj_t BgL_destz00_8621, obj_t BgL_argsz00_8622,
		obj_t BgL_pz00_8623, obj_t BgL_mz00_8624)
	{
		{	/* SawMill/defs.scm 391 */
			BGl_showzd2funzd2zzsaw_defsz00(
				((BgL_rtl_funz00_bglt)
					((BgL_rtl_ifnez00_bglt) BgL_oz00_8620)), BgL_destz00_8621,
				BgL_pz00_8623);
			BGl_dumpzd2argszd2zzsaw_defsz00(BgL_argsz00_8622, BgL_pz00_8623);
			{	/* SawMill/defs.scm 395 */
				obj_t BgL_tmpz00_15153;

				BgL_tmpz00_15153 = ((obj_t) BgL_pz00_8623);
				bgl_display_string(BGl_string3379z00zzsaw_defsz00, BgL_tmpz00_15153);
			}
			{	/* SawMill/defs.scm 396 */
				int BgL_arg2919z00_10091;

				BgL_arg2919z00_10091 =
					(((BgL_blockz00_bglt) COBJECT(
							(((BgL_rtl_ifnez00_bglt) COBJECT(
										((BgL_rtl_ifnez00_bglt) BgL_oz00_8620)))->BgL_thenz00)))->
					BgL_labelz00);
				bgl_display_obj(BINT(BgL_arg2919z00_10091), BgL_pz00_8623);
			}
			{	/* SawMill/defs.scm 397 */
				obj_t BgL_tmpz00_15161;

				BgL_tmpz00_15161 = ((obj_t) BgL_pz00_8623);
				return
					bgl_display_string(BGl_string3089z00zzsaw_defsz00, BgL_tmpz00_15161);
			}
		}

	}



/* &dump-fun-rtl_ifeq1812 */
	obj_t BGl_z62dumpzd2funzd2rtl_ifeq1812z62zzsaw_defsz00(obj_t BgL_envz00_8625,
		obj_t BgL_oz00_8626, obj_t BgL_destz00_8627, obj_t BgL_argsz00_8628,
		obj_t BgL_pz00_8629, obj_t BgL_mz00_8630)
	{
		{	/* SawMill/defs.scm 380 */
			BGl_showzd2funzd2zzsaw_defsz00(
				((BgL_rtl_funz00_bglt)
					((BgL_rtl_ifeqz00_bglt) BgL_oz00_8626)), BgL_destz00_8627,
				BgL_pz00_8629);
			BGl_dumpzd2argszd2zzsaw_defsz00(BgL_argsz00_8628, BgL_pz00_8629);
			{	/* SawMill/defs.scm 384 */
				obj_t BgL_tmpz00_15168;

				BgL_tmpz00_15168 = ((obj_t) BgL_pz00_8629);
				bgl_display_string(BGl_string3379z00zzsaw_defsz00, BgL_tmpz00_15168);
			}
			{	/* SawMill/defs.scm 385 */
				int BgL_arg2917z00_10093;

				BgL_arg2917z00_10093 =
					(((BgL_blockz00_bglt) COBJECT(
							(((BgL_rtl_ifeqz00_bglt) COBJECT(
										((BgL_rtl_ifeqz00_bglt) BgL_oz00_8626)))->BgL_thenz00)))->
					BgL_labelz00);
				bgl_display_obj(BINT(BgL_arg2917z00_10093), BgL_pz00_8629);
			}
			{	/* SawMill/defs.scm 386 */
				obj_t BgL_tmpz00_15176;

				BgL_tmpz00_15176 = ((obj_t) BgL_pz00_8629);
				return
					bgl_display_string(BGl_string3089z00zzsaw_defsz00, BgL_tmpz00_15176);
			}
		}

	}



/* &dump-fun-rtl_switch1810 */
	obj_t BGl_z62dumpzd2funzd2rtl_switch1810z62zzsaw_defsz00(obj_t
		BgL_envz00_8631, obj_t BgL_oz00_8632, obj_t BgL_destz00_8633,
		obj_t BgL_argsz00_8634, obj_t BgL_pz00_8635, obj_t BgL_mz00_8636)
	{
		{	/* SawMill/defs.scm 370 */
			BGl_showzd2funzd2zzsaw_defsz00(
				((BgL_rtl_funz00_bglt)
					((BgL_rtl_switchz00_bglt) BgL_oz00_8632)), BgL_destz00_8633,
				BgL_pz00_8635);
			BGl_dumpzd2argszd2zzsaw_defsz00(BgL_argsz00_8634, BgL_pz00_8635);
			{	/* SawMill/defs.scm 374 */
				obj_t BgL_tmpz00_15183;

				BgL_tmpz00_15183 = ((obj_t) BgL_pz00_8635);
				bgl_display_string(BGl_string3085z00zzsaw_defsz00, BgL_tmpz00_15183);
			}
			{	/* SawMill/defs.scm 375 */
				obj_t BgL_arg2905z00_10095;

				{	/* SawMill/defs.scm 375 */
					obj_t BgL_l1684z00_10096;

					BgL_l1684z00_10096 =
						(((BgL_rtl_switchz00_bglt) COBJECT(
								((BgL_rtl_switchz00_bglt) BgL_oz00_8632)))->BgL_labelsz00);
					if (NULLP(BgL_l1684z00_10096))
						{	/* SawMill/defs.scm 375 */
							BgL_arg2905z00_10095 = BNIL;
						}
					else
						{	/* SawMill/defs.scm 375 */
							obj_t BgL_head1686z00_10097;

							{	/* SawMill/defs.scm 375 */
								int BgL_arg2915z00_10098;

								BgL_arg2915z00_10098 =
									(((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt)
												CAR(((obj_t) BgL_l1684z00_10096)))))->BgL_labelz00);
								BgL_head1686z00_10097 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg2915z00_10098), BNIL);
							}
							{	/* SawMill/defs.scm 375 */
								obj_t BgL_g1689z00_10099;

								BgL_g1689z00_10099 = CDR(((obj_t) BgL_l1684z00_10096));
								{
									obj_t BgL_l1684z00_10101;
									obj_t BgL_tail1687z00_10102;

									BgL_l1684z00_10101 = BgL_g1689z00_10099;
									BgL_tail1687z00_10102 = BgL_head1686z00_10097;
								BgL_zc3z04anonymousza32907ze3z87_10100:
									if (NULLP(BgL_l1684z00_10101))
										{	/* SawMill/defs.scm 375 */
											BgL_arg2905z00_10095 = BgL_head1686z00_10097;
										}
									else
										{	/* SawMill/defs.scm 375 */
											obj_t BgL_newtail1688z00_10103;

											{	/* SawMill/defs.scm 375 */
												int BgL_arg2913z00_10104;

												BgL_arg2913z00_10104 =
													(((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt)
																CAR(
																	((obj_t) BgL_l1684z00_10101)))))->
													BgL_labelz00);
												BgL_newtail1688z00_10103 =
													MAKE_YOUNG_PAIR(BINT(BgL_arg2913z00_10104), BNIL);
											}
											SET_CDR(BgL_tail1687z00_10102, BgL_newtail1688z00_10103);
											{	/* SawMill/defs.scm 375 */
												obj_t BgL_arg2912z00_10105;

												BgL_arg2912z00_10105 =
													CDR(((obj_t) BgL_l1684z00_10101));
												{
													obj_t BgL_tail1687z00_15210;
													obj_t BgL_l1684z00_15209;

													BgL_l1684z00_15209 = BgL_arg2912z00_10105;
													BgL_tail1687z00_15210 = BgL_newtail1688z00_10103;
													BgL_tail1687z00_10102 = BgL_tail1687z00_15210;
													BgL_l1684z00_10101 = BgL_l1684z00_15209;
													goto BgL_zc3z04anonymousza32907ze3z87_10100;
												}
											}
										}
								}
							}
						}
				}
				return bgl_display_obj(BgL_arg2905z00_10095, BgL_pz00_8635);
			}
		}

	}



/* &dump-fun-rtl_globalr1808 */
	obj_t BGl_z62dumpzd2funzd2rtl_globalr1808z62zzsaw_defsz00(obj_t
		BgL_envz00_8637, obj_t BgL_oz00_8638, obj_t BgL_destz00_8639,
		obj_t BgL_argsz00_8640, obj_t BgL_pz00_8641, obj_t BgL_mz00_8642)
	{
		{	/* SawMill/defs.scm 361 */
			BGl_showzd2funzd2zzsaw_defsz00(
				((BgL_rtl_funz00_bglt)
					((BgL_rtl_globalrefz00_bglt) BgL_oz00_8638)), BgL_destz00_8639,
				BgL_pz00_8641);
			{	/* SawMill/defs.scm 363 */
				obj_t BgL_tmpz00_15215;

				BgL_tmpz00_15215 = ((obj_t) BgL_pz00_8641);
				bgl_display_string(BGl_string3085z00zzsaw_defsz00, BgL_tmpz00_15215);
			}
			{	/* SawMill/defs.scm 364 */
				obj_t BgL_arg2903z00_10107;

				{	/* SawMill/defs.scm 364 */
					BgL_globalz00_bglt BgL_arg2904z00_10108;

					BgL_arg2904z00_10108 =
						(((BgL_rtl_globalrefz00_bglt) COBJECT(
								((BgL_rtl_globalrefz00_bglt) BgL_oz00_8638)))->BgL_varz00);
					BgL_arg2903z00_10107 =
						BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2904z00_10108));
				}
				bgl_display_obj(BgL_arg2903z00_10107, BgL_pz00_8641);
			}
			return
				BBOOL(BGl_dumpzd2argszd2zzsaw_defsz00(BgL_argsz00_8640, BgL_pz00_8641));
		}

	}



/* &dump-fun-rtl_loadfun1806 */
	obj_t BGl_z62dumpzd2funzd2rtl_loadfun1806z62zzsaw_defsz00(obj_t
		BgL_envz00_8643, obj_t BgL_oz00_8644, obj_t BgL_destz00_8645,
		obj_t BgL_argsz00_8646, obj_t BgL_pz00_8647, obj_t BgL_mz00_8648)
	{
		{	/* SawMill/defs.scm 352 */
			BGl_showzd2funzd2zzsaw_defsz00(
				((BgL_rtl_funz00_bglt)
					((BgL_rtl_loadfunz00_bglt) BgL_oz00_8644)), BgL_destz00_8645,
				BgL_pz00_8647);
			{	/* SawMill/defs.scm 354 */
				obj_t BgL_tmpz00_15228;

				BgL_tmpz00_15228 = ((obj_t) BgL_pz00_8647);
				bgl_display_string(BGl_string3085z00zzsaw_defsz00, BgL_tmpz00_15228);
			}
			{	/* SawMill/defs.scm 355 */
				obj_t BgL_arg2898z00_10110;

				{	/* SawMill/defs.scm 355 */
					BgL_globalz00_bglt BgL_arg2899z00_10111;

					BgL_arg2899z00_10111 =
						(((BgL_rtl_loadfunz00_bglt) COBJECT(
								((BgL_rtl_loadfunz00_bglt) BgL_oz00_8644)))->BgL_varz00);
					BgL_arg2898z00_10110 =
						BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2899z00_10111));
				}
				bgl_display_obj(BgL_arg2898z00_10110, BgL_pz00_8647);
			}
			return
				BBOOL(BGl_dumpzd2argszd2zzsaw_defsz00(BgL_argsz00_8646, BgL_pz00_8647));
		}

	}



/* &dump-fun-rtl_loadg1804 */
	obj_t BGl_z62dumpzd2funzd2rtl_loadg1804z62zzsaw_defsz00(obj_t BgL_envz00_8649,
		obj_t BgL_oz00_8650, obj_t BgL_destz00_8651, obj_t BgL_argsz00_8652,
		obj_t BgL_pz00_8653, obj_t BgL_mz00_8654)
	{
		{	/* SawMill/defs.scm 343 */
			BGl_showzd2funzd2zzsaw_defsz00(
				((BgL_rtl_funz00_bglt)
					((BgL_rtl_loadgz00_bglt) BgL_oz00_8650)), BgL_destz00_8651,
				BgL_pz00_8653);
			{	/* SawMill/defs.scm 345 */
				obj_t BgL_tmpz00_15241;

				BgL_tmpz00_15241 = ((obj_t) BgL_pz00_8653);
				bgl_display_string(BGl_string3085z00zzsaw_defsz00, BgL_tmpz00_15241);
			}
			{	/* SawMill/defs.scm 346 */
				obj_t BgL_arg2896z00_10113;

				{	/* SawMill/defs.scm 346 */
					BgL_globalz00_bglt BgL_arg2897z00_10114;

					BgL_arg2897z00_10114 =
						(((BgL_rtl_loadgz00_bglt) COBJECT(
								((BgL_rtl_loadgz00_bglt) BgL_oz00_8650)))->BgL_varz00);
					BgL_arg2896z00_10113 =
						BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2897z00_10114));
				}
				bgl_display_obj(BgL_arg2896z00_10113, BgL_pz00_8653);
			}
			return
				BBOOL(BGl_dumpzd2argszd2zzsaw_defsz00(BgL_argsz00_8652, BgL_pz00_8653));
		}

	}



/* &dump-fun-rtl_mov1802 */
	obj_t BGl_z62dumpzd2funzd2rtl_mov1802z62zzsaw_defsz00(obj_t BgL_envz00_8655,
		obj_t BgL_oz00_8656, obj_t BgL_destz00_8657, obj_t BgL_argsz00_8658,
		obj_t BgL_pz00_8659, obj_t BgL_mz00_8660)
	{
		{	/* SawMill/defs.scm 336 */
			BGl_showzd2funzd2zzsaw_defsz00(
				((BgL_rtl_funz00_bglt)
					((BgL_rtl_movz00_bglt) BgL_oz00_8656)), BgL_destz00_8657,
				BgL_pz00_8659);
			return BBOOL(BGl_dumpzd2argszd2zzsaw_defsz00(BgL_argsz00_8658,
					BgL_pz00_8659));
		}

	}



/* &dump-fun-rtl_loadi1800 */
	obj_t BGl_z62dumpzd2funzd2rtl_loadi1800z62zzsaw_defsz00(obj_t BgL_envz00_8661,
		obj_t BgL_oz00_8662, obj_t BgL_destz00_8663, obj_t BgL_argsz00_8664,
		obj_t BgL_pz00_8665, obj_t BgL_mz00_8666)
	{
		{	/* SawMill/defs.scm 327 */
			BGl_showzd2funzd2zzsaw_defsz00(
				((BgL_rtl_funz00_bglt)
					((BgL_rtl_loadiz00_bglt) BgL_oz00_8662)), BgL_destz00_8663,
				BgL_pz00_8665);
			{	/* SawMill/defs.scm 329 */
				obj_t BgL_tmpz00_15259;

				BgL_tmpz00_15259 = ((obj_t) BgL_pz00_8665);
				bgl_display_string(BGl_string3085z00zzsaw_defsz00, BgL_tmpz00_15259);
			}
			{	/* SawMill/defs.scm 330 */
				obj_t BgL_arg2893z00_10117;

				BgL_arg2893z00_10117 =
					(((BgL_atomz00_bglt) COBJECT(
							(((BgL_rtl_loadiz00_bglt) COBJECT(
										((BgL_rtl_loadiz00_bglt) BgL_oz00_8662)))->
								BgL_constantz00)))->BgL_valuez00);
				{	/* SawMill/defs.scm 330 */
					obj_t BgL_list2894z00_10118;

					BgL_list2894z00_10118 = MAKE_YOUNG_PAIR(BgL_pz00_8665, BNIL);
					BGl_writez00zz__r4_output_6_10_3z00(BgL_arg2893z00_10117,
						BgL_list2894z00_10118);
				}
			}
			return
				BBOOL(BGl_dumpzd2argszd2zzsaw_defsz00(BgL_argsz00_8664, BgL_pz00_8665));
		}

	}



/* &dump-fun-rtl_fun1798 */
	obj_t BGl_z62dumpzd2funzd2rtl_fun1798z62zzsaw_defsz00(obj_t BgL_envz00_8667,
		obj_t BgL_oz00_8668, obj_t BgL_destz00_8669, obj_t BgL_argsz00_8670,
		obj_t BgL_pz00_8671, obj_t BgL_mz00_8672)
	{
		{	/* SawMill/defs.scm 320 */
			BGl_showzd2funzd2zzsaw_defsz00(
				((BgL_rtl_funz00_bglt) BgL_oz00_8668), BgL_destz00_8669, BgL_pz00_8671);
			return
				BBOOL(BGl_dumpzd2argszd2zzsaw_defsz00(BgL_argsz00_8670, BgL_pz00_8671));
		}

	}



/* &dump-rtl_reg1793 */
	obj_t BGl_z62dumpzd2rtl_reg1793zb0zzsaw_defsz00(obj_t BgL_envz00_8673,
		obj_t BgL_oz00_8674, obj_t BgL_pz00_8675, obj_t BgL_mz00_8676)
	{
		{	/* SawMill/defs.scm 294 */
			{	/* SawMill/defs.scm 295 */
				obj_t BgL_arg2890z00_10121;

				BgL_arg2890z00_10121 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_rtl_regz00_bglt) BgL_oz00_8674)));
				bgl_display_obj(BgL_arg2890z00_10121, BgL_pz00_8675);
			}
			if (CBOOL(BGl_za2typezd2shapezf3za2z21zzengine_paramz00))
				{	/* SawMill/defs.scm 296 */
					{	/* SawMill/defs.scm 297 */
						obj_t BgL_tmpz00_15279;

						BgL_tmpz00_15279 = ((obj_t) BgL_pz00_8675);
						bgl_display_string(BGl_string3380z00zzsaw_defsz00,
							BgL_tmpz00_15279);
					}
					{	/* SawMill/defs.scm 298 */
						obj_t BgL_arg2891z00_10122;

						{	/* SawMill/defs.scm 298 */
							BgL_typez00_bglt BgL_arg2892z00_10123;

							BgL_arg2892z00_10123 =
								(((BgL_rtl_regz00_bglt) COBJECT(
										((BgL_rtl_regz00_bglt) BgL_oz00_8674)))->BgL_typez00);
							BgL_arg2891z00_10122 =
								BGl_shapez00zztools_shapez00(((obj_t) BgL_arg2892z00_10123));
						}
						return bgl_display_obj(BgL_arg2891z00_10122, BgL_pz00_8675);
					}
				}
			else
				{	/* SawMill/defs.scm 296 */
					return BFALSE;
				}
		}

	}



/* &dump-rtl_ins1791 */
	obj_t BGl_z62dumpzd2rtl_ins1791zb0zzsaw_defsz00(obj_t BgL_envz00_8677,
		obj_t BgL_oz00_8678, obj_t BgL_pz00_8679, obj_t BgL_mz00_8680)
	{
		{	/* SawMill/defs.scm 262 */
			{	/* SawMill/defs.scm 264 */
				obj_t BgL_tmpz00_15287;

				BgL_tmpz00_15287 = ((obj_t) BgL_pz00_8679);
				bgl_display_string(BGl_string3381z00zzsaw_defsz00, BgL_tmpz00_15287);
			}
			if (CBOOL(
					(((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_oz00_8678)))->BgL_destz00)))
				{	/* SawMill/defs.scm 265 */
					{	/* SawMill/defs.scm 266 */
						obj_t BgL_tmpz00_15294;

						BgL_tmpz00_15294 = ((obj_t) BgL_pz00_8679);
						bgl_display_string(BGl_string3088z00zzsaw_defsz00,
							BgL_tmpz00_15294);
					}
					BGl_dumpz00zzsaw_defsz00(
						(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_oz00_8678)))->BgL_destz00),
						BgL_pz00_8679, CINT(BgL_mz00_8680));
					{	/* SawMill/defs.scm 268 */
						obj_t BgL_tmpz00_15301;

						BgL_tmpz00_15301 = ((obj_t) BgL_pz00_8679);
						bgl_display_string(BGl_string3382z00zzsaw_defsz00,
							BgL_tmpz00_15301);
					}
				}
			else
				{	/* SawMill/defs.scm 265 */
					BFALSE;
				}
			BGl_dumpzd2inszd2rhsz00zzsaw_defsz00(
				((BgL_rtl_insz00_bglt) BgL_oz00_8678), BgL_pz00_8679, BgL_mz00_8680);
			if (CBOOL(
					(((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_oz00_8678)))->BgL_destz00)))
				{	/* SawMill/defs.scm 271 */
					obj_t BgL_tmpz00_15310;

					BgL_tmpz00_15310 = ((obj_t) BgL_pz00_8679);
					bgl_display_string(BGl_string3089z00zzsaw_defsz00, BgL_tmpz00_15310);
				}
			else
				{	/* SawMill/defs.scm 270 */
					BFALSE;
				}
			{	/* SawMill/defs.scm 272 */
				obj_t BgL_tmpz00_15313;

				BgL_tmpz00_15313 = ((obj_t) BgL_pz00_8679);
				bgl_display_string(BGl_string3085z00zzsaw_defsz00, BgL_tmpz00_15313);
			}
			{	/* SawMill/defs.scm 273 */
				obj_t BgL_tmpz00_15316;

				BgL_tmpz00_15316 = ((obj_t) BgL_pz00_8679);
				bgl_display_string(BGl_string3383z00zzsaw_defsz00, BgL_tmpz00_15316);
			}
			if (NULLP(
					(((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_oz00_8678)))->BgL_z52spillz52)))
				{	/* SawMill/defs.scm 274 */
					((bool_t) 0);
				}
			else
				{	/* SawMill/defs.scm 275 */
					obj_t BgL_g1683z00_10125;

					BgL_g1683z00_10125 =
						(((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_oz00_8678)))->BgL_z52spillz52);
					{
						obj_t BgL_l1681z00_10127;

						BgL_l1681z00_10127 = BgL_g1683z00_10125;
					BgL_zc3z04anonymousza32882ze3z87_10126:
						if (PAIRP(BgL_l1681z00_10127))
							{	/* SawMill/defs.scm 275 */
								{	/* SawMill/defs.scm 276 */
									obj_t BgL_rz00_10128;

									BgL_rz00_10128 = CAR(BgL_l1681z00_10127);
									bgl_display_obj(BGl_shapez00zztools_shapez00(BgL_rz00_10128),
										BgL_pz00_8679);
									{	/* SawMill/defs.scm 277 */
										obj_t BgL_tmpz00_15330;

										BgL_tmpz00_15330 = ((obj_t) BgL_pz00_8679);
										bgl_display_string(BGl_string3085z00zzsaw_defsz00,
											BgL_tmpz00_15330);
									}
								}
								{
									obj_t BgL_l1681z00_15333;

									BgL_l1681z00_15333 = CDR(BgL_l1681z00_10127);
									BgL_l1681z00_10127 = BgL_l1681z00_15333;
									goto BgL_zc3z04anonymousza32882ze3z87_10126;
								}
							}
						else
							{	/* SawMill/defs.scm 275 */
								((bool_t) 1);
							}
					}
				}
			{	/* SawMill/defs.scm 279 */
				obj_t BgL_tmpz00_15335;

				BgL_tmpz00_15335 = ((obj_t) BgL_pz00_8679);
				bgl_display_string(BGl_string3089z00zzsaw_defsz00, BgL_tmpz00_15335);
			}
			{	/* SawMill/defs.scm 280 */
				obj_t BgL_tmpz00_15338;

				BgL_tmpz00_15338 = ((obj_t) BgL_pz00_8679);
				return
					bgl_display_string(BGl_string3384z00zzsaw_defsz00, BgL_tmpz00_15338);
			}
		}

	}



/* &dump-block1789 */
	obj_t BGl_z62dumpzd2block1789zb0zzsaw_defsz00(obj_t BgL_envz00_8681,
		obj_t BgL_oz00_8682, obj_t BgL_pz00_8683, obj_t BgL_mz00_8684)
	{
		{	/* SawMill/defs.scm 247 */
			{	/* SawMill/defs.scm 249 */
				obj_t BgL_tmpz00_15341;

				BgL_tmpz00_15341 = ((obj_t) BgL_pz00_8683);
				bgl_display_string(BGl_string3088z00zzsaw_defsz00, BgL_tmpz00_15341);
			}
			{	/* SawMill/defs.scm 249 */
				obj_t BgL_arg2836z00_10130;

				BgL_arg2836z00_10130 =
					bgl_typeof(((obj_t) ((BgL_blockz00_bglt) BgL_oz00_8682)));
				bgl_display_obj(BgL_arg2836z00_10130, BgL_pz00_8683);
			}
			{	/* SawMill/defs.scm 249 */
				obj_t BgL_tmpz00_15348;

				BgL_tmpz00_15348 = ((obj_t) BgL_pz00_8683);
				bgl_display_string(BGl_string3085z00zzsaw_defsz00, BgL_tmpz00_15348);
			}
			{	/* SawMill/defs.scm 249 */
				int BgL_arg2837z00_10131;

				BgL_arg2837z00_10131 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_8682)))->BgL_labelz00);
				bgl_display_obj(BINT(BgL_arg2837z00_10131), BgL_pz00_8683);
			}
			{	/* SawMill/defs.scm 249 */
				obj_t BgL_tmpz00_15355;

				BgL_tmpz00_15355 = ((obj_t) BgL_pz00_8683);
				bgl_display_char(((unsigned char) 10), BgL_tmpz00_15355);
			}
			{	/* SawMill/defs.scm 251 */
				long BgL_arg2838z00_10132;

				BgL_arg2838z00_10132 = ((long) CINT(BgL_mz00_8684) + 1L);
				BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_8683,
					(int) (BgL_arg2838z00_10132));
			}
			{	/* SawMill/defs.scm 252 */
				obj_t BgL_tmpz00_15362;

				BgL_tmpz00_15362 = ((obj_t) BgL_pz00_8683);
				bgl_display_string(BGl_string3385z00zzsaw_defsz00, BgL_tmpz00_15362);
			}
			{	/* SawMill/defs.scm 252 */
				obj_t BgL_arg2839z00_10133;

				{	/* SawMill/defs.scm 252 */
					obj_t BgL_l1667z00_10134;

					BgL_l1667z00_10134 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_oz00_8682)))->BgL_predsz00);
					if (NULLP(BgL_l1667z00_10134))
						{	/* SawMill/defs.scm 252 */
							BgL_arg2839z00_10133 = BNIL;
						}
					else
						{	/* SawMill/defs.scm 252 */
							obj_t BgL_head1669z00_10135;

							{	/* SawMill/defs.scm 252 */
								int BgL_arg2848z00_10136;

								BgL_arg2848z00_10136 =
									(((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt)
												CAR(BgL_l1667z00_10134))))->BgL_labelz00);
								BgL_head1669z00_10135 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg2848z00_10136), BNIL);
							}
							{	/* SawMill/defs.scm 252 */
								obj_t BgL_g1672z00_10137;

								BgL_g1672z00_10137 = CDR(BgL_l1667z00_10134);
								{
									obj_t BgL_l1667z00_10139;
									obj_t BgL_tail1670z00_10140;

									BgL_l1667z00_10139 = BgL_g1672z00_10137;
									BgL_tail1670z00_10140 = BgL_head1669z00_10135;
								BgL_zc3z04anonymousza32841ze3z87_10138:
									if (NULLP(BgL_l1667z00_10139))
										{	/* SawMill/defs.scm 252 */
											BgL_arg2839z00_10133 = BgL_head1669z00_10135;
										}
									else
										{	/* SawMill/defs.scm 252 */
											obj_t BgL_newtail1671z00_10141;

											{	/* SawMill/defs.scm 252 */
												int BgL_arg2846z00_10142;

												BgL_arg2846z00_10142 =
													(((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt)
																CAR(
																	((obj_t) BgL_l1667z00_10139)))))->
													BgL_labelz00);
												BgL_newtail1671z00_10141 =
													MAKE_YOUNG_PAIR(BINT(BgL_arg2846z00_10142), BNIL);
											}
											SET_CDR(BgL_tail1670z00_10140, BgL_newtail1671z00_10141);
											{	/* SawMill/defs.scm 252 */
												obj_t BgL_arg2844z00_10143;

												BgL_arg2844z00_10143 =
													CDR(((obj_t) BgL_l1667z00_10139));
												{
													obj_t BgL_tail1670z00_15387;
													obj_t BgL_l1667z00_15386;

													BgL_l1667z00_15386 = BgL_arg2844z00_10143;
													BgL_tail1670z00_15387 = BgL_newtail1671z00_10141;
													BgL_tail1670z00_10140 = BgL_tail1670z00_15387;
													BgL_l1667z00_10139 = BgL_l1667z00_15386;
													goto BgL_zc3z04anonymousza32841ze3z87_10138;
												}
											}
										}
								}
							}
						}
				}
				bgl_display_obj(BgL_arg2839z00_10133, BgL_pz00_8683);
			}
			{	/* SawMill/defs.scm 252 */
				obj_t BgL_tmpz00_15389;

				BgL_tmpz00_15389 = ((obj_t) BgL_pz00_8683);
				bgl_display_char(((unsigned char) 10), BgL_tmpz00_15389);
			}
			{	/* SawMill/defs.scm 253 */
				long BgL_arg2858z00_10144;

				BgL_arg2858z00_10144 = ((long) CINT(BgL_mz00_8684) + 1L);
				BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_8683,
					(int) (BgL_arg2858z00_10144));
			}
			{	/* SawMill/defs.scm 254 */
				obj_t BgL_tmpz00_15396;

				BgL_tmpz00_15396 = ((obj_t) BgL_pz00_8683);
				bgl_display_string(BGl_string3386z00zzsaw_defsz00, BgL_tmpz00_15396);
			}
			{	/* SawMill/defs.scm 254 */
				obj_t BgL_arg2859z00_10145;

				{	/* SawMill/defs.scm 254 */
					obj_t BgL_l1674z00_10146;

					BgL_l1674z00_10146 =
						(((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt) BgL_oz00_8682)))->BgL_succsz00);
					if (NULLP(BgL_l1674z00_10146))
						{	/* SawMill/defs.scm 254 */
							BgL_arg2859z00_10145 = BNIL;
						}
					else
						{	/* SawMill/defs.scm 254 */
							obj_t BgL_head1676z00_10147;

							{	/* SawMill/defs.scm 254 */
								int BgL_arg2872z00_10148;

								BgL_arg2872z00_10148 =
									(((BgL_blockz00_bglt) COBJECT(
											((BgL_blockz00_bglt)
												CAR(BgL_l1674z00_10146))))->BgL_labelz00);
								BgL_head1676z00_10147 =
									MAKE_YOUNG_PAIR(BINT(BgL_arg2872z00_10148), BNIL);
							}
							{	/* SawMill/defs.scm 254 */
								obj_t BgL_g1679z00_10149;

								BgL_g1679z00_10149 = CDR(BgL_l1674z00_10146);
								{
									obj_t BgL_l1674z00_10151;
									obj_t BgL_tail1677z00_10152;

									BgL_l1674z00_10151 = BgL_g1679z00_10149;
									BgL_tail1677z00_10152 = BgL_head1676z00_10147;
								BgL_zc3z04anonymousza32861ze3z87_10150:
									if (NULLP(BgL_l1674z00_10151))
										{	/* SawMill/defs.scm 254 */
											BgL_arg2859z00_10145 = BgL_head1676z00_10147;
										}
									else
										{	/* SawMill/defs.scm 254 */
											obj_t BgL_newtail1678z00_10153;

											{	/* SawMill/defs.scm 254 */
												int BgL_arg2870z00_10154;

												BgL_arg2870z00_10154 =
													(((BgL_blockz00_bglt) COBJECT(
															((BgL_blockz00_bglt)
																CAR(
																	((obj_t) BgL_l1674z00_10151)))))->
													BgL_labelz00);
												BgL_newtail1678z00_10153 =
													MAKE_YOUNG_PAIR(BINT(BgL_arg2870z00_10154), BNIL);
											}
											SET_CDR(BgL_tail1677z00_10152, BgL_newtail1678z00_10153);
											{	/* SawMill/defs.scm 254 */
												obj_t BgL_arg2864z00_10155;

												BgL_arg2864z00_10155 =
													CDR(((obj_t) BgL_l1674z00_10151));
												{
													obj_t BgL_tail1677z00_15421;
													obj_t BgL_l1674z00_15420;

													BgL_l1674z00_15420 = BgL_arg2864z00_10155;
													BgL_tail1677z00_15421 = BgL_newtail1678z00_10153;
													BgL_tail1677z00_10152 = BgL_tail1677z00_15421;
													BgL_l1674z00_10151 = BgL_l1674z00_15420;
													goto BgL_zc3z04anonymousza32861ze3z87_10150;
												}
											}
										}
								}
							}
						}
				}
				bgl_display_obj(BgL_arg2859z00_10145, BgL_pz00_8683);
			}
			{	/* SawMill/defs.scm 254 */
				obj_t BgL_tmpz00_15423;

				BgL_tmpz00_15423 = ((obj_t) BgL_pz00_8683);
				bgl_display_char(((unsigned char) 10), BgL_tmpz00_15423);
			}
			{	/* SawMill/defs.scm 255 */
				long BgL_arg2874z00_10156;

				BgL_arg2874z00_10156 = ((long) CINT(BgL_mz00_8684) + 1L);
				BGl_dumpzd2marginzd2zzsaw_defsz00(BgL_pz00_8683,
					(int) (BgL_arg2874z00_10156));
			}
			{	/* SawMill/defs.scm 256 */
				obj_t BgL_arg2875z00_10157;
				long BgL_arg2876z00_10158;

				BgL_arg2875z00_10157 =
					(((BgL_blockz00_bglt) COBJECT(
							((BgL_blockz00_bglt) BgL_oz00_8682)))->BgL_firstz00);
				BgL_arg2876z00_10158 = ((long) CINT(BgL_mz00_8684) + 1L);
				BGl_dumpza2za2zzsaw_defsz00(BgL_arg2875z00_10157, BgL_pz00_8683,
					(int) (BgL_arg2876z00_10158));
			}
			{	/* SawMill/defs.scm 257 */
				obj_t BgL_tmpz00_15436;

				BgL_tmpz00_15436 = ((obj_t) BgL_pz00_8683);
				return
					bgl_display_string(BGl_string3387z00zzsaw_defsz00, BgL_tmpz00_15436);
			}
		}

	}



/* &shape-rtl_reg1785 */
	obj_t BGl_z62shapezd2rtl_reg1785zb0zzsaw_defsz00(obj_t BgL_envz00_8685,
		obj_t BgL_oz00_8686)
	{
		{	/* SawMill/defs.scm 134 */
			{	/* SawMill/defs.scm 135 */
				obj_t BgL_pz00_10160;

				{	/* SawMill/defs.scm 135 */

					{	/* SawMill/defs.scm 135 */

						BgL_pz00_10160 =
							BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(BTRUE);
					}
				}
				if (CBOOL(
						(((BgL_rtl_regz00_bglt) COBJECT(
									((BgL_rtl_regz00_bglt) BgL_oz00_8686)))->BgL_onexprzf3zf3)))
					{	/* SawMill/defs.scm 138 */
						bgl_display_string(BGl_string3388z00zzsaw_defsz00, BgL_pz00_10160);
						bgl_display_obj(
							(((BgL_rtl_regz00_bglt) COBJECT(
										((BgL_rtl_regz00_bglt) BgL_oz00_8686)))->BgL_namez00),
							BgL_pz00_10160);
					}
				else
					{	/* SawMill/defs.scm 138 */
						if (CBOOL(
								(((BgL_rtl_regz00_bglt) COBJECT(
											((BgL_rtl_regz00_bglt) BgL_oz00_8686)))->
									BgL_hardwarez00)))
							{	/* SawMill/defs.scm 141 */
								bgl_display_string(BGl_string3389z00zzsaw_defsz00,
									BgL_pz00_10160);
								bgl_display_obj((((BgL_rtl_regz00_bglt)
											COBJECT(((BgL_rtl_regz00_bglt) BgL_oz00_8686)))->
										BgL_hardwarez00), BgL_pz00_10160);
							}
						else
							{	/* SawMill/defs.scm 141 */
								if (CBOOL(
										(((BgL_rtl_regz00_bglt) COBJECT(
													((BgL_rtl_regz00_bglt) BgL_oz00_8686)))->BgL_varz00)))
									{	/* SawMill/defs.scm 145 */
										obj_t BgL_arg2830z00_10161;

										BgL_arg2830z00_10161 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														(((BgL_rtl_regz00_bglt) COBJECT(
																	((BgL_rtl_regz00_bglt) BgL_oz00_8686)))->
															BgL_varz00))))->BgL_idz00);
										bgl_display_obj(BgL_arg2830z00_10161, BgL_pz00_10160);
									}
								else
									{	/* SawMill/defs.scm 144 */
										bgl_display_string(BGl_string3390z00zzsaw_defsz00,
											BgL_pz00_10160);
										bgl_display_obj((((BgL_rtl_regz00_bglt)
													COBJECT(((BgL_rtl_regz00_bglt) BgL_oz00_8686)))->
												BgL_namez00), BgL_pz00_10160);
									}
							}
					}
				if (CBOOL(BGl_za2typezd2shapezf3za2z21zzengine_paramz00))
					{	/* SawMill/defs.scm 149 */
						bgl_display_string(BGl_string3380z00zzsaw_defsz00, BgL_pz00_10160);
						bgl_display_obj(
							(((BgL_typez00_bglt) COBJECT(
										(((BgL_rtl_regz00_bglt) COBJECT(
													((BgL_rtl_regz00_bglt) BgL_oz00_8686)))->
											BgL_typez00)))->BgL_idz00), BgL_pz00_10160);
					}
				else
					{	/* SawMill/defs.scm 149 */
						BFALSE;
					}
				if (CBOOL(BGl_za2keyzd2shapezf3za2z21zzengine_paramz00))
					{	/* SawMill/defs.scm 152 */
						bgl_display_string(BGl_string3391z00zzsaw_defsz00, BgL_pz00_10160);
						bgl_display_obj(
							(((BgL_rtl_regz00_bglt) COBJECT(
										((BgL_rtl_regz00_bglt) BgL_oz00_8686)))->BgL_keyz00),
							BgL_pz00_10160);
					}
				else
					{	/* SawMill/defs.scm 152 */
						BFALSE;
					}
				return bgl_close_output_port(BgL_pz00_10160);
			}
		}

	}



/* &shape-rtl_ins1783 */
	obj_t BGl_z62shapezd2rtl_ins1783zb0zzsaw_defsz00(obj_t BgL_envz00_8687,
		obj_t BgL_iz00_8688)
	{
		{	/* SawMill/defs.scm 126 */
			{	/* SawMill/defs.scm 129 */
				obj_t BgL_zc3z04anonymousza32824ze3z87_10163;

				BgL_zc3z04anonymousza32824ze3z87_10163 =
					MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza32824ze3ze5zzsaw_defsz00,
					(int) (1L), (int) (1L));
				PROCEDURE_SET(BgL_zc3z04anonymousza32824ze3z87_10163,
					(int) (0L), ((obj_t) ((BgL_rtl_insz00_bglt) BgL_iz00_8688)));
				return
					BGl_callzd2withzd2outputzd2stringzd2zz__r4_ports_6_10_1z00
					(BgL_zc3z04anonymousza32824ze3z87_10163);
			}
		}

	}



/* &<@anonymous:2824> */
	obj_t BGl_z62zc3z04anonymousza32824ze3ze5zzsaw_defsz00(obj_t BgL_envz00_8689,
		obj_t BgL_opz00_8691)
	{
		{	/* SawMill/defs.scm 128 */
			{	/* SawMill/defs.scm 129 */
				BgL_rtl_insz00_bglt BgL_iz00_8690;

				BgL_iz00_8690 =
					((BgL_rtl_insz00_bglt) PROCEDURE_REF(BgL_envz00_8689, (int) (0L)));
				return
					BGl_dumpz00zzsaw_defsz00(
					((obj_t) BgL_iz00_8690), BgL_opz00_8691, (int) (0L));
		}}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_defsz00(void)
	{
		{	/* SawMill/defs.scm 1 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string3392z00zzsaw_defsz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string3392z00zzsaw_defsz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string3392z00zzsaw_defsz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string3392z00zzsaw_defsz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string3392z00zzsaw_defsz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string3392z00zzsaw_defsz00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string3392z00zzsaw_defsz00));
		}

	}

#ifdef __cplusplus
}
#endif
