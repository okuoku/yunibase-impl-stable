/*===========================================================================*/
/*   (SawMill/expr.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/expr.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_EXPR_TYPE_DEFINITIONS
#define BGL_SAW_EXPR_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_feffectz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_readz00;
		obj_t BgL_writez00;
	}                 *BgL_feffectz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_nopz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_nopz00_bglt;

	typedef struct BgL_rtl_callz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                  *BgL_rtl_callz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_iregz00_bgl
	{
		obj_t BgL_indexz00;
		obj_t BgL_statusz00;
	}              *BgL_iregz00_bglt;

	typedef struct BgL_pregz00_bgl
	{
		obj_t BgL_indexz00;
		obj_t BgL_statusz00;
	}              *BgL_pregz00_bglt;

	typedef struct BgL_inlinedz00_bgl
	{
	}                 *BgL_inlinedz00_bglt;


#endif													// BGL_SAW_EXPR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	BGL_IMPORT obj_t BGl_putpropz12z12zz__r4_symbols_6_4z00(obj_t, obj_t, obj_t);
	static obj_t BGl_prezd2analysezd2zzsaw_exprz00(BgL_backendz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_rtl_funcallz00zzsaw_defsz00;
	static BgL_rtl_regz00_bglt BGl_z62lambda1933z62zzsaw_exprz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda1936z62zzsaw_exprz00(obj_t, obj_t);
	static BgL_feffectz00_bglt BGl_za2getfza2z00zzsaw_exprz00;
	extern obj_t BGl_rtl_funz00zzsaw_defsz00;
	static BgL_rtl_regz00_bglt BGl_z62lambda1939z62zzsaw_exprz00(obj_t, obj_t);
	static bool_t BGl_intersectionzf3ze70z14zzsaw_exprz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	extern obj_t BGl_rtl_pragmaz00zzsaw_defsz00;
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_exprz00 = BUNSPEC;
	static obj_t BGl_z62effectszd2rtl_lightfun1564zb0zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1946z62zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1947z62zzsaw_exprz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62prezd2buildzd2treez62zzsaw_exprz00(obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_funz00zzast_varz00;
	static obj_t BGl_pregz00zzsaw_exprz00 = BUNSPEC;
	static obj_t BGl_z62lambda1951z62zzsaw_exprz00(obj_t, obj_t);
	extern obj_t BGl_rtl_boxrefz00zzsaw_defsz00;
	static obj_t BGl_z62lambda1952z62zzsaw_exprz00(obj_t, obj_t, obj_t);
	static bool_t BGl_matcheffectz00zzsaw_exprz00(obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda1959z62zzsaw_exprz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62effectszd2rtl_setfield1560zb0zzsaw_exprz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static BgL_feffectz00_bglt BGl_za2fullza2z00zzsaw_exprz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31938ze3ze5zzsaw_exprz00(obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda1962z62zzsaw_exprz00(obj_t, obj_t);
	static BgL_feffectz00_bglt BGl_za2loadgza2z00zzsaw_exprz00;
	static BgL_rtl_regz00_bglt BGl_z62lambda1965z62zzsaw_exprz00(obj_t, obj_t);
	extern obj_t BGl_rtl_vrefz00zzsaw_defsz00;
	static obj_t BGl_toplevelzd2initzd2zzsaw_exprz00(void);
	extern obj_t BGl_rtl_protectz00zzsaw_defsz00;
	static obj_t BGl_z62lambda1972z62zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1973z62zzsaw_exprz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1977z62zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1978z62zzsaw_exprz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_boxsetz00zzsaw_defsz00;
	static obj_t BGl_z62zc3z04anonymousza31964ze3ze5zzsaw_exprz00(obj_t, obj_t);
	static BgL_rtl_insz00_bglt BGl_z62lambda1985z62zzsaw_exprz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_exprz00(void);
	static BgL_rtl_insz00_bglt BGl_z62lambda1988z62zzsaw_exprz00(obj_t, obj_t);
	static BgL_feffectz00_bglt BGl_za2vrefza2z00zzsaw_exprz00;
	static BgL_feffectz00_bglt BGl_za2storegza2z00zzsaw_exprz00;
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_vsetz00zzsaw_defsz00;
	static bool_t BGl_markzd2localszd2zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_z62effectszd2rtl_apply1562zb0zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzsaw_exprz00(void);
	static BgL_rtl_insz00_bglt BGl_z62lambda1991z62zzsaw_exprz00(obj_t, obj_t);
	static BgL_feffectz00_bglt BGl_za2noeffectza2z00zzsaw_exprz00;
	static obj_t BGl_z62effectszd2rtl_boxset1556zb0zzsaw_exprz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_buildzd2treezd2zzsaw_exprz00(BgL_backendz00_bglt,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62zc3z04anonymousza31990ze3ze5zzsaw_exprz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_prezd2buildzd2treez00zzsaw_exprz00(BgL_backendz00_bglt, obj_t, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	extern obj_t BGl_rtl_lightfuncallz00zzsaw_defsz00;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	static obj_t BGl_z62effectszd2rtl_boxref1554zb0zzsaw_exprz00(obj_t, obj_t);
	extern obj_t BGl_rtl_movz00zzsaw_defsz00;
	static obj_t BGl_z62effectszd2rtl_call1572zb0zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_z62buildzd2treezb0zzsaw_exprz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62usefulz62zzsaw_exprz00(obj_t, obj_t);
	static BgL_feffectz00_bglt BGl_za2boxrefza2z00zzsaw_exprz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_inlinedz00zzsaw_exprz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62effectszd2rtl_loadg1546zb0zzsaw_exprz00(obj_t, obj_t);
	extern obj_t BGl_rtl_nopz00zzsaw_defsz00;
	static obj_t BGl_methodzd2initzd2zzsaw_exprz00(void);
	static obj_t BGl_z62effectszd2rtl_protect1570zb0zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_regzd2ze3pregzf2writeze70z24zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_iregz00zzsaw_exprz00 = BUNSPEC;
	static obj_t BGl_manuzd2effectzd2zzsaw_exprz00(BgL_globalz00_bglt);
	static BgL_feffectz00_bglt BGl_za2prefza2z00zzsaw_exprz00;
	extern obj_t BGl_rtl_setfieldz00zzsaw_defsz00;
	static obj_t BGl_z62effectszd2rtl_storeg1548zb0zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_z62effectsz62zzsaw_exprz00(obj_t, obj_t);
	extern obj_t BGl_rtl_callz00zzsaw_defsz00;
	static obj_t BGl_z62effectszd2rtl_pragma1568zb0zzsaw_exprz00(obj_t, obj_t);
	extern obj_t BGl_rtl_storegz00zzsaw_defsz00;
	static BgL_feffectz00_bglt BGl_za2setfza2z00zzsaw_exprz00;
	static obj_t BGl_xxz00zzsaw_exprz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62effects1543z62zzsaw_exprz00(obj_t, obj_t);
	extern obj_t BGl_rtl_applyz00zzsaw_defsz00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzsaw_exprz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_rtl_loadgz00zzsaw_defsz00;
	static long BGl_za2countza2z00zzsaw_exprz00 = 0L;
	extern obj_t BGl_feffectz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zzsaw_exprz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_exprz00(void);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_exprz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_exprz00(void);
	static obj_t BGl_z62effectszd2rtl_vset1552zb0zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_z62effectszd2rtl_funcall1566zb0zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_regzd2ze3iregze70zd6zzsaw_exprz00(obj_t, obj_t);
	extern obj_t BGl_backendz00zzbackend_backendz00;
	static BgL_feffectz00_bglt BGl_za2vsetza2z00zzsaw_exprz00;
	static obj_t BGl_z62effectszd2rtl_vref1550zb0zzsaw_exprz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_getpropz00zz__r4_symbols_6_4z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_filterz12z12zz__r4_control_features_6_9z00(obj_t, obj_t);
	extern obj_t BGl_rtl_getfieldz00zzsaw_defsz00;
	static obj_t BGl_regzd2ze3pregze70zd6zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_z62acceptzd2foldingzf31541z43zzsaw_exprz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_acceptzd2foldingzf3z21zzsaw_exprz00(BgL_backendz00_bglt, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62acceptzd2foldingzf3z43zzsaw_exprz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_feffectz00_bglt BGl_za2boxsetza2z00zzsaw_exprz00;
	static bool_t BGl_prezd2markzd2localsz00zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_analysez00zzsaw_exprz00(BgL_backendz00_bglt, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t BGl_z62effectszd2rtl_getfield1558zb0zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_regzd2ze3iregzf2writeze70z24zzsaw_exprz00(obj_t, obj_t);
	static obj_t BGl_effectsz00zzsaw_exprz00(BgL_rtl_funz00_bglt);
	static BgL_feffectz00_bglt BGl_za2psetza2z00zzsaw_exprz00;
	static obj_t __cnst[28];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2132z00zzsaw_exprz00,
		BgL_bgl_za762usefulza762za7za7sa2175z00, BGl_z62usefulz62zzsaw_exprz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2133z00zzsaw_exprz00,
		BgL_bgl_za762lambda1947za7622176z00, BGl_z62lambda1947z62zzsaw_exprz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2134z00zzsaw_exprz00,
		BgL_bgl_za762lambda1946za7622177z00, BGl_z62lambda1946z62zzsaw_exprz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2135z00zzsaw_exprz00,
		BgL_bgl_za762lambda1952za7622178z00, BGl_z62lambda1952z62zzsaw_exprz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2136z00zzsaw_exprz00,
		BgL_bgl_za762lambda1951za7622179z00, BGl_z62lambda1951z62zzsaw_exprz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2137z00zzsaw_exprz00,
		BgL_bgl_za762lambda1939za7622180z00, BGl_z62lambda1939z62zzsaw_exprz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2138z00zzsaw_exprz00,
		BgL_bgl_za762za7c3za704anonymo2181za7,
		BGl_z62zc3z04anonymousza31938ze3ze5zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2139z00zzsaw_exprz00,
		BgL_bgl_za762lambda1936za7622182z00, BGl_z62lambda1936z62zzsaw_exprz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2140z00zzsaw_exprz00,
		BgL_bgl_za762lambda1933za7622183z00, BGl_z62lambda1933z62zzsaw_exprz00, 0L,
		BUNSPEC, 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2141z00zzsaw_exprz00,
		BgL_bgl_za762lambda1973za7622184z00, BGl_z62lambda1973z62zzsaw_exprz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2142z00zzsaw_exprz00,
		BgL_bgl_za762lambda1972za7622185z00, BGl_z62lambda1972z62zzsaw_exprz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2143z00zzsaw_exprz00,
		BgL_bgl_za762lambda1978za7622186z00, BGl_z62lambda1978z62zzsaw_exprz00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2144z00zzsaw_exprz00,
		BgL_bgl_za762lambda1977za7622187z00, BGl_z62lambda1977z62zzsaw_exprz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2145z00zzsaw_exprz00,
		BgL_bgl_za762lambda1965za7622188z00, BGl_z62lambda1965z62zzsaw_exprz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2146z00zzsaw_exprz00,
		BgL_bgl_za762za7c3za704anonymo2189za7,
		BGl_z62zc3z04anonymousza31964ze3ze5zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2147z00zzsaw_exprz00,
		BgL_bgl_za762lambda1962za7622190z00, BGl_z62lambda1962z62zzsaw_exprz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2154z00zzsaw_exprz00,
		BgL_bgl_string2154za700za7za7s2191za7, "accept-folding?1541", 19);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2148z00zzsaw_exprz00,
		BgL_bgl_za762lambda1959za7622192z00, BGl_z62lambda1959z62zzsaw_exprz00, 0L,
		BUNSPEC, 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2149z00zzsaw_exprz00,
		BgL_bgl_za762lambda1991za7622193z00, BGl_z62lambda1991z62zzsaw_exprz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2156z00zzsaw_exprz00,
		BgL_bgl_string2156za700za7za7s2194za7, "effects1543", 11);
	      DEFINE_STRING(BGl_string2158z00zzsaw_exprz00,
		BgL_bgl_string2158za700za7za7s2195za7, "effects", 7);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2150z00zzsaw_exprz00,
		BgL_bgl_za762za7c3za704anonymo2196za7,
		BGl_z62zc3z04anonymousza31990ze3ze5zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2151z00zzsaw_exprz00,
		BgL_bgl_za762lambda1988za7622197z00, BGl_z62lambda1988z62zzsaw_exprz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2152z00zzsaw_exprz00,
		BgL_bgl_za762lambda1985za7622198z00, BGl_z62lambda1985z62zzsaw_exprz00, 0L,
		BUNSPEC, 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2153z00zzsaw_exprz00,
		BgL_bgl_za762acceptza7d2fold2199z00,
		BGl_z62acceptzd2foldingzf31541z43zzsaw_exprz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2155z00zzsaw_exprz00,
		BgL_bgl_za762effects1543za762200z00, BGl_z62effects1543z62zzsaw_exprz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2157z00zzsaw_exprz00,
		BgL_bgl_za762effectsza7d2rtl2201z00,
		BGl_z62effectszd2rtl_loadg1546zb0zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2159z00zzsaw_exprz00,
		BgL_bgl_za762effectsza7d2rtl2202z00,
		BGl_z62effectszd2rtl_storeg1548zb0zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2160z00zzsaw_exprz00,
		BgL_bgl_za762effectsza7d2rtl2203z00,
		BGl_z62effectszd2rtl_vref1550zb0zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2161z00zzsaw_exprz00,
		BgL_bgl_za762effectsza7d2rtl2204z00,
		BGl_z62effectszd2rtl_vset1552zb0zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2162z00zzsaw_exprz00,
		BgL_bgl_za762effectsza7d2rtl2205z00,
		BGl_z62effectszd2rtl_boxref1554zb0zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2163z00zzsaw_exprz00,
		BgL_bgl_za762effectsza7d2rtl2206z00,
		BGl_z62effectszd2rtl_boxset1556zb0zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2164z00zzsaw_exprz00,
		BgL_bgl_za762effectsza7d2rtl2207z00,
		BGl_z62effectszd2rtl_getfield1558zb0zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2165z00zzsaw_exprz00,
		BgL_bgl_za762effectsza7d2rtl2208z00,
		BGl_z62effectszd2rtl_setfield1560zb0zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2172z00zzsaw_exprz00,
		BgL_bgl_string2172za700za7za7s2209za7, "saw_expr", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2166z00zzsaw_exprz00,
		BgL_bgl_za762effectsza7d2rtl2210z00,
		BGl_z62effectszd2rtl_apply1562zb0zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2173z00zzsaw_exprz00,
		BgL_bgl_string2173za700za7za7s2211za7,
		"inlined preg saw_expr ireg status obj index (print (global-id var) \" has no effect\") ko ok written readen reset c-write-char c-current-output-port (output-port) $tvector-descr-set! procedure-set! procedure-ref make-fx-procedure foreign $cons (procedure) top (field) (box) (vector) (memory) ",
		290);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2167z00zzsaw_exprz00,
		BgL_bgl_za762effectsza7d2rtl2212z00,
		BGl_z62effectszd2rtl_lightfun1564zb0zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2168z00zzsaw_exprz00,
		BgL_bgl_za762effectsza7d2rtl2213z00,
		BGl_z62effectszd2rtl_funcall1566zb0zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2169z00zzsaw_exprz00,
		BgL_bgl_za762effectsza7d2rtl2214z00,
		BGl_z62effectszd2rtl_pragma1568zb0zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2170z00zzsaw_exprz00,
		BgL_bgl_za762effectsza7d2rtl2215z00,
		BGl_z62effectszd2rtl_protect1570zb0zzsaw_exprz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2171z00zzsaw_exprz00,
		BgL_bgl_za762effectsza7d2rtl2216z00,
		BGl_z62effectszd2rtl_call1572zb0zzsaw_exprz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_prezd2buildzd2treezd2envzd2zzsaw_exprz00,
		BgL_bgl_za762preza7d2buildza7d2217za7,
		BGl_z62prezd2buildzd2treez62zzsaw_exprz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_acceptzd2foldingzf3zd2envzf3zzsaw_exprz00,
		BgL_bgl_za762acceptza7d2fold2218z00,
		BGl_z62acceptzd2foldingzf3z43zzsaw_exprz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_buildzd2treezd2envz00zzsaw_exprz00,
		BgL_bgl_za762buildza7d2treeza72219za7, BGl_z62buildzd2treezb0zzsaw_exprz00,
		0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_GENERIC(BGl_effectszd2envzd2zzsaw_exprz00,
		BgL_bgl_za762effectsza762za7za7s2220z00, BGl_z62effectsz62zzsaw_exprz00, 0L,
		BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2getfza2z00zzsaw_exprz00));
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_exprz00));
		     ADD_ROOT((void *) (&BGl_pregz00zzsaw_exprz00));
		     ADD_ROOT((void *) (&BGl_za2fullza2z00zzsaw_exprz00));
		     ADD_ROOT((void *) (&BGl_za2loadgza2z00zzsaw_exprz00));
		     ADD_ROOT((void *) (&BGl_za2vrefza2z00zzsaw_exprz00));
		     ADD_ROOT((void *) (&BGl_za2storegza2z00zzsaw_exprz00));
		     ADD_ROOT((void *) (&BGl_za2noeffectza2z00zzsaw_exprz00));
		     ADD_ROOT((void *) (&BGl_za2boxrefza2z00zzsaw_exprz00));
		     ADD_ROOT((void *) (&BGl_inlinedz00zzsaw_exprz00));
		     ADD_ROOT((void *) (&BGl_iregz00zzsaw_exprz00));
		     ADD_ROOT((void *) (&BGl_za2prefza2z00zzsaw_exprz00));
		     ADD_ROOT((void *) (&BGl_za2setfza2z00zzsaw_exprz00));
		     ADD_ROOT((void *) (&BGl_za2vsetza2z00zzsaw_exprz00));
		     ADD_ROOT((void *) (&BGl_za2boxsetza2z00zzsaw_exprz00));
		     ADD_ROOT((void *) (&BGl_za2psetza2z00zzsaw_exprz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_exprz00(long
		BgL_checksumz00_4157, char *BgL_fromz00_4158)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_exprz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_exprz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_exprz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_exprz00();
					BGl_cnstzd2initzd2zzsaw_exprz00();
					BGl_importedzd2moduleszd2initz00zzsaw_exprz00();
					BGl_objectzd2initzd2zzsaw_exprz00();
					BGl_genericzd2initzd2zzsaw_exprz00();
					BGl_methodzd2initzd2zzsaw_exprz00();
					return BGl_toplevelzd2initzd2zzsaw_exprz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_exprz00(void)
	{
		{	/* SawMill/expr.scm 1 */
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "saw_expr");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_expr");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_expr");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_expr");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "saw_expr");
			BGl_modulezd2initializa7ationz75zz__r4_control_features_6_9z00(0L,
				"saw_expr");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "saw_expr");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_expr");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "saw_expr");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_expr");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_expr");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_exprz00(void)
	{
		{	/* SawMill/expr.scm 1 */
			{	/* SawMill/expr.scm 1 */
				obj_t BgL_cportz00_4016;

				{	/* SawMill/expr.scm 1 */
					obj_t BgL_stringz00_4023;

					BgL_stringz00_4023 = BGl_string2173z00zzsaw_exprz00;
					{	/* SawMill/expr.scm 1 */
						obj_t BgL_startz00_4024;

						BgL_startz00_4024 = BINT(0L);
						{	/* SawMill/expr.scm 1 */
							obj_t BgL_endz00_4025;

							BgL_endz00_4025 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4023)));
							{	/* SawMill/expr.scm 1 */

								BgL_cportz00_4016 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4023, BgL_startz00_4024, BgL_endz00_4025);
				}}}}
				{
					long BgL_iz00_4017;

					BgL_iz00_4017 = 27L;
				BgL_loopz00_4018:
					if ((BgL_iz00_4017 == -1L))
						{	/* SawMill/expr.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawMill/expr.scm 1 */
							{	/* SawMill/expr.scm 1 */
								obj_t BgL_arg2174z00_4019;

								{	/* SawMill/expr.scm 1 */

									{	/* SawMill/expr.scm 1 */
										obj_t BgL_locationz00_4021;

										BgL_locationz00_4021 = BBOOL(((bool_t) 0));
										{	/* SawMill/expr.scm 1 */

											BgL_arg2174z00_4019 =
												BGl_readz00zz__readerz00(BgL_cportz00_4016,
												BgL_locationz00_4021);
										}
									}
								}
								{	/* SawMill/expr.scm 1 */
									int BgL_tmpz00_4190;

									BgL_tmpz00_4190 = (int) (BgL_iz00_4017);
									CNST_TABLE_SET(BgL_tmpz00_4190, BgL_arg2174z00_4019);
							}}
							{	/* SawMill/expr.scm 1 */
								int BgL_auxz00_4022;

								BgL_auxz00_4022 = (int) ((BgL_iz00_4017 - 1L));
								{
									long BgL_iz00_4195;

									BgL_iz00_4195 = (long) (BgL_auxz00_4022);
									BgL_iz00_4017 = BgL_iz00_4195;
									goto BgL_loopz00_4018;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_exprz00(void)
	{
		{	/* SawMill/expr.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_exprz00(void)
	{
		{	/* SawMill/expr.scm 1 */
			BGl_za2countza2z00zzsaw_exprz00 = 0L;
			{	/* SawMill/expr.scm 218 */
				BgL_feffectz00_bglt BgL_new1233z00_2012;

				{	/* Ast/var.scm 179 */
					BgL_feffectz00_bglt BgL_new1232z00_2013;

					BgL_new1232z00_2013 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* Ast/var.scm 179 */
						long BgL_arg1573z00_2014;

						BgL_arg1573z00_2014 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1232z00_2013), BgL_arg1573z00_2014);
					}
					BgL_new1233z00_2012 = BgL_new1232z00_2013;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1233z00_2012))->BgL_readz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1233z00_2012))->BgL_writez00) =
					((obj_t) BNIL), BUNSPEC);
				BGl_za2noeffectza2z00zzsaw_exprz00 = BgL_new1233z00_2012;
			}
			{	/* SawMill/expr.scm 219 */
				BgL_feffectz00_bglt BgL_new1235z00_2015;

				{	/* SawMill/expr.scm 219 */
					BgL_feffectz00_bglt BgL_new1234z00_2016;

					BgL_new1234z00_2016 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* SawMill/expr.scm 219 */
						long BgL_arg1575z00_2017;

						BgL_arg1575z00_2017 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1234z00_2016), BgL_arg1575z00_2017);
					}
					BgL_new1235z00_2015 = BgL_new1234z00_2016;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1235z00_2015))->BgL_readz00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1235z00_2015))->BgL_writez00) =
					((obj_t) BNIL), BUNSPEC);
				BGl_za2loadgza2z00zzsaw_exprz00 = BgL_new1235z00_2015;
			}
			{	/* SawMill/expr.scm 220 */
				BgL_feffectz00_bglt BgL_new1237z00_2018;

				{	/* Ast/var.scm 179 */
					BgL_feffectz00_bglt BgL_new1236z00_2019;

					BgL_new1236z00_2019 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* Ast/var.scm 179 */
						long BgL_arg1576z00_2020;

						BgL_arg1576z00_2020 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1236z00_2019), BgL_arg1576z00_2020);
					}
					BgL_new1237z00_2018 = BgL_new1236z00_2019;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1237z00_2018))->BgL_readz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1237z00_2018))->BgL_writez00) =
					((obj_t) CNST_TABLE_REF(0)), BUNSPEC);
				BGl_za2storegza2z00zzsaw_exprz00 = BgL_new1237z00_2018;
			}
			{	/* SawMill/expr.scm 221 */
				BgL_feffectz00_bglt BgL_new1239z00_2021;

				{	/* SawMill/expr.scm 221 */
					BgL_feffectz00_bglt BgL_new1238z00_2022;

					BgL_new1238z00_2022 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* SawMill/expr.scm 221 */
						long BgL_arg1584z00_2023;

						BgL_arg1584z00_2023 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1238z00_2022), BgL_arg1584z00_2023);
					}
					BgL_new1239z00_2021 = BgL_new1238z00_2022;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1239z00_2021))->BgL_readz00) =
					((obj_t) CNST_TABLE_REF(1)), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1239z00_2021))->BgL_writez00) =
					((obj_t) BNIL), BUNSPEC);
				BGl_za2vrefza2z00zzsaw_exprz00 = BgL_new1239z00_2021;
			}
			{	/* SawMill/expr.scm 222 */
				BgL_feffectz00_bglt BgL_new1242z00_2024;

				{	/* Ast/var.scm 179 */
					BgL_feffectz00_bglt BgL_new1240z00_2025;

					BgL_new1240z00_2025 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* Ast/var.scm 179 */
						long BgL_arg1585z00_2026;

						BgL_arg1585z00_2026 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1240z00_2025), BgL_arg1585z00_2026);
					}
					BgL_new1242z00_2024 = BgL_new1240z00_2025;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1242z00_2024))->BgL_readz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1242z00_2024))->BgL_writez00) =
					((obj_t) CNST_TABLE_REF(1)), BUNSPEC);
				BGl_za2vsetza2z00zzsaw_exprz00 = BgL_new1242z00_2024;
			}
			{	/* SawMill/expr.scm 223 */
				BgL_feffectz00_bglt BgL_new1244z00_2027;

				{	/* SawMill/expr.scm 223 */
					BgL_feffectz00_bglt BgL_new1243z00_2028;

					BgL_new1243z00_2028 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* SawMill/expr.scm 223 */
						long BgL_arg1589z00_2029;

						BgL_arg1589z00_2029 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1243z00_2028), BgL_arg1589z00_2029);
					}
					BgL_new1244z00_2027 = BgL_new1243z00_2028;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1244z00_2027))->BgL_readz00) =
					((obj_t) CNST_TABLE_REF(2)), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1244z00_2027))->BgL_writez00) =
					((obj_t) BNIL), BUNSPEC);
				BGl_za2boxrefza2z00zzsaw_exprz00 = BgL_new1244z00_2027;
			}
			{	/* SawMill/expr.scm 224 */
				BgL_feffectz00_bglt BgL_new1246z00_2030;

				{	/* Ast/var.scm 179 */
					BgL_feffectz00_bglt BgL_new1245z00_2031;

					BgL_new1245z00_2031 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* Ast/var.scm 179 */
						long BgL_arg1591z00_2032;

						BgL_arg1591z00_2032 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1245z00_2031), BgL_arg1591z00_2032);
					}
					BgL_new1246z00_2030 = BgL_new1245z00_2031;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1246z00_2030))->BgL_readz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1246z00_2030))->BgL_writez00) =
					((obj_t) CNST_TABLE_REF(2)), BUNSPEC);
				BGl_za2boxsetza2z00zzsaw_exprz00 = BgL_new1246z00_2030;
			}
			{	/* SawMill/expr.scm 225 */
				BgL_feffectz00_bglt BgL_new1248z00_2033;

				{	/* SawMill/expr.scm 225 */
					BgL_feffectz00_bglt BgL_new1247z00_2034;

					BgL_new1247z00_2034 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* SawMill/expr.scm 225 */
						long BgL_arg1593z00_2035;

						BgL_arg1593z00_2035 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1247z00_2034), BgL_arg1593z00_2035);
					}
					BgL_new1248z00_2033 = BgL_new1247z00_2034;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1248z00_2033))->BgL_readz00) =
					((obj_t) CNST_TABLE_REF(3)), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1248z00_2033))->BgL_writez00) =
					((obj_t) BNIL), BUNSPEC);
				BGl_za2getfza2z00zzsaw_exprz00 = BgL_new1248z00_2033;
			}
			{	/* SawMill/expr.scm 226 */
				BgL_feffectz00_bglt BgL_new1251z00_2036;

				{	/* Ast/var.scm 179 */
					BgL_feffectz00_bglt BgL_new1249z00_2037;

					BgL_new1249z00_2037 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* Ast/var.scm 179 */
						long BgL_arg1594z00_2038;

						BgL_arg1594z00_2038 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1249z00_2037), BgL_arg1594z00_2038);
					}
					BgL_new1251z00_2036 = BgL_new1249z00_2037;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1251z00_2036))->BgL_readz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1251z00_2036))->BgL_writez00) =
					((obj_t) CNST_TABLE_REF(3)), BUNSPEC);
				BGl_za2setfza2z00zzsaw_exprz00 = BgL_new1251z00_2036;
			}
			{	/* SawMill/expr.scm 227 */
				BgL_feffectz00_bglt BgL_new1253z00_2039;

				{	/* SawMill/expr.scm 227 */
					BgL_feffectz00_bglt BgL_new1252z00_2040;

					BgL_new1252z00_2040 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* SawMill/expr.scm 227 */
						long BgL_arg1595z00_2041;

						BgL_arg1595z00_2041 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1252z00_2040), BgL_arg1595z00_2041);
					}
					BgL_new1253z00_2039 = BgL_new1252z00_2040;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1253z00_2039))->BgL_readz00) =
					((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1253z00_2039))->BgL_writez00) =
					((obj_t) CNST_TABLE_REF(4)), BUNSPEC);
				BGl_za2fullza2z00zzsaw_exprz00 = BgL_new1253z00_2039;
			}
			{	/* SawMill/expr.scm 266 */
				BgL_feffectz00_bglt BgL_new1260z00_2042;

				{	/* SawMill/expr.scm 266 */
					BgL_feffectz00_bglt BgL_new1258z00_2043;

					BgL_new1258z00_2043 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* SawMill/expr.scm 266 */
						long BgL_arg1602z00_2044;

						BgL_arg1602z00_2044 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1258z00_2043), BgL_arg1602z00_2044);
					}
					BgL_new1260z00_2042 = BgL_new1258z00_2043;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1260z00_2042))->BgL_readz00) =
					((obj_t) CNST_TABLE_REF(5)), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1260z00_2042))->BgL_writez00) =
					((obj_t) BNIL), BUNSPEC);
				BGl_za2prefza2z00zzsaw_exprz00 = BgL_new1260z00_2042;
			}
			{	/* SawMill/expr.scm 267 */
				BgL_feffectz00_bglt BgL_new1262z00_2045;

				{	/* Ast/var.scm 179 */
					BgL_feffectz00_bglt BgL_new1261z00_2046;

					BgL_new1261z00_2046 =
						((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_feffectz00_bgl))));
					{	/* Ast/var.scm 179 */
						long BgL_arg1605z00_2047;

						BgL_arg1605z00_2047 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1261z00_2046), BgL_arg1605z00_2047);
					}
					BgL_new1262z00_2045 = BgL_new1261z00_2046;
				}
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1262z00_2045))->BgL_readz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_feffectz00_bglt) COBJECT(BgL_new1262z00_2045))->BgL_writez00) =
					((obj_t) CNST_TABLE_REF(5)), BUNSPEC);
				BGl_za2psetza2z00zzsaw_exprz00 = BgL_new1262z00_2045;
			}
			{	/* SawMill/expr.scm 269 */
				BgL_feffectz00_bglt BgL_arg1606z00_2048;

				{	/* SawMill/expr.scm 269 */
					BgL_feffectz00_bglt BgL_new1264z00_2049;

					{	/* SawMill/expr.scm 269 */
						BgL_feffectz00_bglt BgL_new1263z00_2050;

						BgL_new1263z00_2050 =
							((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_feffectz00_bgl))));
						{	/* SawMill/expr.scm 269 */
							long BgL_arg1609z00_2051;

							BgL_arg1609z00_2051 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1263z00_2050),
								BgL_arg1609z00_2051);
						}
						BgL_new1264z00_2049 = BgL_new1263z00_2050;
					}
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1264z00_2049))->BgL_readz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1264z00_2049))->
							BgL_writez00) = ((obj_t) BNIL), BUNSPEC);
					BgL_arg1606z00_2048 = BgL_new1264z00_2049;
				}
				BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(6),
					CNST_TABLE_REF(7), ((obj_t) BgL_arg1606z00_2048));
			}
			{	/* SawMill/expr.scm 270 */
				BgL_feffectz00_bglt BgL_arg1611z00_2052;

				{	/* SawMill/expr.scm 270 */
					BgL_feffectz00_bglt BgL_new1266z00_2053;

					{	/* SawMill/expr.scm 270 */
						BgL_feffectz00_bglt BgL_new1265z00_2054;

						BgL_new1265z00_2054 =
							((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_feffectz00_bgl))));
						{	/* SawMill/expr.scm 270 */
							long BgL_arg1613z00_2055;

							BgL_arg1613z00_2055 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1265z00_2054),
								BgL_arg1613z00_2055);
						}
						BgL_new1266z00_2053 = BgL_new1265z00_2054;
					}
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1266z00_2053))->BgL_readz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1266z00_2053))->
							BgL_writez00) = ((obj_t) BNIL), BUNSPEC);
					BgL_arg1611z00_2052 = BgL_new1266z00_2053;
				}
				BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(8),
					CNST_TABLE_REF(7), ((obj_t) BgL_arg1611z00_2052));
			}
			{	/* SawMill/expr.scm 271 */
				BgL_feffectz00_bglt BgL_arg1615z00_2056;

				{	/* SawMill/expr.scm 271 */
					BgL_feffectz00_bglt BgL_new1268z00_2057;

					{	/* SawMill/expr.scm 271 */
						BgL_feffectz00_bglt BgL_new1267z00_2058;

						BgL_new1267z00_2058 =
							((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_feffectz00_bgl))));
						{	/* SawMill/expr.scm 271 */
							long BgL_arg1616z00_2059;

							BgL_arg1616z00_2059 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1267z00_2058),
								BgL_arg1616z00_2059);
						}
						BgL_new1268z00_2057 = BgL_new1267z00_2058;
					}
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1268z00_2057))->BgL_readz00) =
						((obj_t) CNST_TABLE_REF(5)), BUNSPEC);
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1268z00_2057))->
							BgL_writez00) = ((obj_t) BNIL), BUNSPEC);
					BgL_arg1615z00_2056 = BgL_new1268z00_2057;
				}
				BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(9),
					CNST_TABLE_REF(7), ((obj_t) BgL_arg1615z00_2056));
			}
			{	/* SawMill/expr.scm 272 */
				BgL_feffectz00_bglt BgL_arg1625z00_2060;

				{	/* SawMill/expr.scm 272 */
					BgL_feffectz00_bglt BgL_new1270z00_2061;

					{	/* SawMill/expr.scm 272 */
						BgL_feffectz00_bglt BgL_new1269z00_2062;

						BgL_new1269z00_2062 =
							((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_feffectz00_bgl))));
						{	/* SawMill/expr.scm 272 */
							long BgL_arg1626z00_2063;

							BgL_arg1626z00_2063 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1269z00_2062),
								BgL_arg1626z00_2063);
						}
						BgL_new1270z00_2061 = BgL_new1269z00_2062;
					}
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1270z00_2061))->BgL_readz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1270z00_2061))->
							BgL_writez00) = ((obj_t) CNST_TABLE_REF(5)), BUNSPEC);
					BgL_arg1625z00_2060 = BgL_new1270z00_2061;
				}
				BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(10),
					CNST_TABLE_REF(7), ((obj_t) BgL_arg1625z00_2060));
			}
			{	/* SawMill/expr.scm 273 */
				BgL_feffectz00_bglt BgL_arg1627z00_2064;

				{	/* SawMill/expr.scm 273 */
					BgL_feffectz00_bglt BgL_new1272z00_2065;

					{	/* SawMill/expr.scm 273 */
						BgL_feffectz00_bglt BgL_new1271z00_2066;

						BgL_new1271z00_2066 =
							((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_feffectz00_bgl))));
						{	/* SawMill/expr.scm 273 */
							long BgL_arg1629z00_2067;

							BgL_arg1629z00_2067 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1271z00_2066),
								BgL_arg1629z00_2067);
						}
						BgL_new1272z00_2065 = BgL_new1271z00_2066;
					}
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1272z00_2065))->BgL_readz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1272z00_2065))->
							BgL_writez00) = ((obj_t) CNST_TABLE_REF(1)), BUNSPEC);
					BgL_arg1627z00_2064 = BgL_new1272z00_2065;
				}
				BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(11),
					CNST_TABLE_REF(7), ((obj_t) BgL_arg1627z00_2064));
			}
			{	/* SawMill/expr.scm 274 */
				BgL_feffectz00_bglt BgL_arg1630z00_2068;

				{	/* SawMill/expr.scm 274 */
					BgL_feffectz00_bglt BgL_new1274z00_2069;

					{	/* SawMill/expr.scm 274 */
						BgL_feffectz00_bglt BgL_new1273z00_2070;

						BgL_new1273z00_2070 =
							((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_feffectz00_bgl))));
						{	/* SawMill/expr.scm 274 */
							long BgL_arg1642z00_2071;

							BgL_arg1642z00_2071 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1273z00_2070),
								BgL_arg1642z00_2071);
						}
						BgL_new1274z00_2069 = BgL_new1273z00_2070;
					}
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1274z00_2069))->BgL_readz00) =
						((obj_t) CNST_TABLE_REF(12)), BUNSPEC);
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1274z00_2069))->
							BgL_writez00) = ((obj_t) BNIL), BUNSPEC);
					BgL_arg1630z00_2068 = BgL_new1274z00_2069;
				}
				BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(13),
					CNST_TABLE_REF(7), ((obj_t) BgL_arg1630z00_2068));
			}
			{	/* SawMill/expr.scm 275 */
				BgL_feffectz00_bglt BgL_arg1646z00_2072;

				{	/* SawMill/expr.scm 275 */
					BgL_feffectz00_bglt BgL_new1276z00_2073;

					{	/* SawMill/expr.scm 275 */
						BgL_feffectz00_bglt BgL_new1275z00_2074;

						BgL_new1275z00_2074 =
							((BgL_feffectz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_feffectz00_bgl))));
						{	/* SawMill/expr.scm 275 */
							long BgL_arg1650z00_2075;

							BgL_arg1650z00_2075 = BGL_CLASS_NUM(BGl_feffectz00zzast_varz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1275z00_2074),
								BgL_arg1650z00_2075);
						}
						BgL_new1276z00_2073 = BgL_new1275z00_2074;
					}
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1276z00_2073))->BgL_readz00) =
						((obj_t) BNIL), BUNSPEC);
					((((BgL_feffectz00_bglt) COBJECT(BgL_new1276z00_2073))->
							BgL_writez00) = ((obj_t) CNST_TABLE_REF(12)), BUNSPEC);
					BgL_arg1646z00_2072 = BgL_new1276z00_2073;
				}
				return
					BGl_putpropz12z12zz__r4_symbols_6_4z00(CNST_TABLE_REF(14),
					CNST_TABLE_REF(7), ((obj_t) BgL_arg1646z00_2072));
			}
		}

	}



/* pre-build-tree */
	BGL_EXPORTED_DEF obj_t
		BGl_prezd2buildzd2treez00zzsaw_exprz00(BgL_backendz00_bglt BgL_bez00_90,
		obj_t BgL_paramsz00_91, obj_t BgL_lz00_92)
	{
		{	/* SawMill/expr.scm 17 */
			BGl_prezd2markzd2localsz00zzsaw_exprz00(BgL_paramsz00_91, BgL_lz00_92);
			BGl_za2countza2z00zzsaw_exprz00 = 0L;
			{
				obj_t BgL_l1499z00_2103;

				{	/* SawMill/expr.scm 20 */
					bool_t BgL_tmpz00_4358;

					BgL_l1499z00_2103 = BgL_lz00_92;
				BgL_zc3z04anonymousza31690ze3z87_2104:
					if (PAIRP(BgL_l1499z00_2103))
						{	/* SawMill/expr.scm 20 */
							{	/* SawMill/expr.scm 22 */
								obj_t BgL_bz00_2106;

								BgL_bz00_2106 = CAR(BgL_l1499z00_2103);
								{	/* SawMill/expr.scm 22 */
									obj_t BgL_lz00_2107;
									obj_t BgL_movesz00_2108;

									BgL_lz00_2107 =
										(((BgL_blockz00_bglt) COBJECT(
												((BgL_blockz00_bglt) BgL_bz00_2106)))->BgL_firstz00);
									BgL_movesz00_2108 = BNIL;
									{
										obj_t BgL_l1497z00_2110;

										BgL_l1497z00_2110 = BgL_lz00_2107;
									BgL_zc3z04anonymousza31692ze3z87_2111:
										if (PAIRP(BgL_l1497z00_2110))
											{	/* SawMill/expr.scm 23 */
												{	/* SawMill/expr.scm 23 */
													obj_t BgL_insz00_2113;

													BgL_insz00_2113 = CAR(BgL_l1497z00_2110);
													BgL_movesz00_2108 =
														BGl_prezd2analysezd2zzsaw_exprz00(BgL_bez00_90,
														BgL_movesz00_2108, BgL_insz00_2113);
												}
												{
													obj_t BgL_l1497z00_4368;

													BgL_l1497z00_4368 = CDR(BgL_l1497z00_2110);
													BgL_l1497z00_2110 = BgL_l1497z00_4368;
													goto BgL_zc3z04anonymousza31692ze3z87_2111;
												}
											}
										else
											{	/* SawMill/expr.scm 23 */
												((bool_t) 1);
											}
									}
								}
							}
							{
								obj_t BgL_l1499z00_4370;

								BgL_l1499z00_4370 = CDR(BgL_l1499z00_2103);
								BgL_l1499z00_2103 = BgL_l1499z00_4370;
								goto BgL_zc3z04anonymousza31690ze3z87_2104;
							}
						}
					else
						{	/* SawMill/expr.scm 20 */
							BgL_tmpz00_4358 = ((bool_t) 1);
						}
					return BBOOL(BgL_tmpz00_4358);
				}
			}
		}

	}



/* &pre-build-tree */
	obj_t BGl_z62prezd2buildzd2treez62zzsaw_exprz00(obj_t BgL_envz00_3855,
		obj_t BgL_bez00_3856, obj_t BgL_paramsz00_3857, obj_t BgL_lz00_3858)
	{
		{	/* SawMill/expr.scm 17 */
			return
				BGl_prezd2buildzd2treez00zzsaw_exprz00(
				((BgL_backendz00_bglt) BgL_bez00_3856), BgL_paramsz00_3857,
				BgL_lz00_3858);
		}

	}



/* pre-mark-locals */
	bool_t BGl_prezd2markzd2localsz00zzsaw_exprz00(obj_t BgL_paramsz00_93,
		obj_t BgL_lz00_94)
	{
		{	/* SawMill/expr.scm 27 */
			{	/* SawMill/expr.scm 28 */
				struct bgl_cell BgL_box2225_4014z00;
				obj_t BgL_nz00_4014;

				BgL_nz00_4014 = MAKE_CELL_STACK(BINT(0L), BgL_box2225_4014z00);
				{
					obj_t BgL_rz00_2149;
					BgL_blockz00_bglt BgL_bz00_2155;

					{
						obj_t BgL_l1507z00_2125;

						BgL_l1507z00_2125 = BgL_paramsz00_93;
					BgL_zc3z04anonymousza31701ze3z87_2126:
						if (PAIRP(BgL_l1507z00_2125))
							{	/* SawMill/expr.scm 52 */
								BGl_regzd2ze3pregzf2writeze70z24zzsaw_exprz00(BgL_nz00_4014,
									CAR(BgL_l1507z00_2125));
								{
									obj_t BgL_l1507z00_4380;

									BgL_l1507z00_4380 = CDR(BgL_l1507z00_2125);
									BgL_l1507z00_2125 = BgL_l1507z00_4380;
									goto BgL_zc3z04anonymousza31701ze3z87_2126;
								}
							}
						else
							{	/* SawMill/expr.scm 52 */
								((bool_t) 1);
							}
					}
					{
						obj_t BgL_l1509z00_2132;

						BgL_l1509z00_2132 = BgL_lz00_94;
					BgL_zc3z04anonymousza31706ze3z87_2133:
						if (PAIRP(BgL_l1509z00_2132))
							{	/* SawMill/expr.scm 53 */
								{	/* SawMill/expr.scm 53 */
									obj_t BgL_arg1708z00_2135;

									BgL_arg1708z00_2135 = CAR(BgL_l1509z00_2132);
									BgL_bz00_2155 = ((BgL_blockz00_bglt) BgL_arg1708z00_2135);
									{	/* SawMill/expr.scm 47 */
										obj_t BgL_g1506z00_2157;

										BgL_g1506z00_2157 =
											(((BgL_blockz00_bglt) COBJECT(BgL_bz00_2155))->
											BgL_firstz00);
										{
											obj_t BgL_l1504z00_2159;

											BgL_l1504z00_2159 = BgL_g1506z00_2157;
										BgL_zc3z04anonymousza31718ze3z87_2160:
											if (PAIRP(BgL_l1504z00_2159))
												{	/* SawMill/expr.scm 51 */
													{	/* SawMill/expr.scm 48 */
														obj_t BgL_insz00_2162;

														BgL_insz00_2162 = CAR(BgL_l1504z00_2159);
														if (CBOOL(
																(((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt)
																				BgL_insz00_2162)))->BgL_destz00)))
															{	/* SawMill/expr.scm 49 */
																obj_t BgL_arg1722z00_2165;

																BgL_arg1722z00_2165 =
																	(((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt)
																				BgL_insz00_2162)))->BgL_destz00);
																BGl_regzd2ze3pregzf2writeze70z24zzsaw_exprz00
																	(BgL_nz00_4014, BgL_arg1722z00_2165);
															}
														else
															{	/* SawMill/expr.scm 49 */
																BFALSE;
															}
														{	/* SawMill/expr.scm 50 */
															obj_t BgL_g1503z00_2166;

															BgL_g1503z00_2166 =
																(((BgL_rtl_insz00_bglt) COBJECT(
																		((BgL_rtl_insz00_bglt) BgL_insz00_2162)))->
																BgL_argsz00);
															{
																obj_t BgL_l1501z00_2168;

																BgL_l1501z00_2168 = BgL_g1503z00_2166;
															BgL_zc3z04anonymousza31723ze3z87_2169:
																if (PAIRP(BgL_l1501z00_2168))
																	{	/* SawMill/expr.scm 50 */
																		BgL_rz00_2149 = CAR(BgL_l1501z00_2168);
																		{	/* SawMill/expr.scm 37 */
																			obj_t BgL_sz00_2151;

																			BgL_sz00_2151 =
																				BGl_regzd2ze3pregze70zd6zzsaw_exprz00
																				(BgL_nz00_4014, BgL_rz00_2149);
																			if ((BgL_sz00_2151 == CNST_TABLE_REF(15)))
																				{	/* SawMill/expr.scm 38 */
																					obj_t BgL_nstatz00_3095;

																					BgL_nstatz00_3095 =
																						CNST_TABLE_REF(16);
																					{
																						BgL_pregz00_bglt BgL_auxz00_4405;

																						{
																							obj_t BgL_auxz00_4406;

																							{	/* SawMill/expr.scm 29 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_4407;
																								BgL_tmpz00_4407 =
																									((BgL_objectz00_bglt) (
																										(BgL_rtl_regz00_bglt)
																										BgL_rz00_2149));
																								BgL_auxz00_4406 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_4407);
																							}
																							BgL_auxz00_4405 =
																								((BgL_pregz00_bglt)
																								BgL_auxz00_4406);
																						}
																						((((BgL_pregz00_bglt)
																									COBJECT(BgL_auxz00_4405))->
																								BgL_statusz00) =
																							((obj_t) BgL_nstatz00_3095),
																							BUNSPEC);
																					}
																				}
																			else
																				{	/* SawMill/expr.scm 38 */
																					if (
																						(BgL_sz00_2151 ==
																							CNST_TABLE_REF(17)))
																						{	/* SawMill/expr.scm 39 */
																							obj_t BgL_nstatz00_3098;

																							BgL_nstatz00_3098 =
																								CNST_TABLE_REF(18);
																							{
																								BgL_pregz00_bglt
																									BgL_auxz00_4417;
																								{
																									obj_t BgL_auxz00_4418;

																									{	/* SawMill/expr.scm 29 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_4419;
																										BgL_tmpz00_4419 =
																											((BgL_objectz00_bglt) (
																												(BgL_rtl_regz00_bglt)
																												BgL_rz00_2149));
																										BgL_auxz00_4418 =
																											BGL_OBJECT_WIDENING
																											(BgL_tmpz00_4419);
																									}
																									BgL_auxz00_4417 =
																										((BgL_pregz00_bglt)
																										BgL_auxz00_4418);
																								}
																								((((BgL_pregz00_bglt)
																											COBJECT
																											(BgL_auxz00_4417))->
																										BgL_statusz00) =
																									((obj_t) BgL_nstatz00_3098),
																									BUNSPEC);
																							}
																						}
																					else
																						{	/* SawMill/expr.scm 40 */
																							obj_t BgL_nstatz00_3101;

																							BgL_nstatz00_3101 =
																								CNST_TABLE_REF(19);
																							{
																								BgL_pregz00_bglt
																									BgL_auxz00_4426;
																								{
																									obj_t BgL_auxz00_4427;

																									{	/* SawMill/expr.scm 29 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_4428;
																										BgL_tmpz00_4428 =
																											((BgL_objectz00_bglt) (
																												(BgL_rtl_regz00_bglt)
																												BgL_rz00_2149));
																										BgL_auxz00_4427 =
																											BGL_OBJECT_WIDENING
																											(BgL_tmpz00_4428);
																									}
																									BgL_auxz00_4426 =
																										((BgL_pregz00_bglt)
																										BgL_auxz00_4427);
																								}
																								((((BgL_pregz00_bglt)
																											COBJECT
																											(BgL_auxz00_4426))->
																										BgL_statusz00) =
																									((obj_t) BgL_nstatz00_3101),
																									BUNSPEC);
																							}
																						}
																				}
																		}
																		{
																			obj_t BgL_l1501z00_4435;

																			BgL_l1501z00_4435 =
																				CDR(BgL_l1501z00_2168);
																			BgL_l1501z00_2168 = BgL_l1501z00_4435;
																			goto
																				BgL_zc3z04anonymousza31723ze3z87_2169;
																		}
																	}
																else
																	{	/* SawMill/expr.scm 50 */
																		((bool_t) 1);
																	}
															}
														}
													}
													{
														obj_t BgL_l1504z00_4437;

														BgL_l1504z00_4437 = CDR(BgL_l1504z00_2159);
														BgL_l1504z00_2159 = BgL_l1504z00_4437;
														goto BgL_zc3z04anonymousza31718ze3z87_2160;
													}
												}
											else
												{	/* SawMill/expr.scm 51 */
													((bool_t) 1);
												}
										}
									}
								}
								{
									obj_t BgL_l1509z00_4440;

									BgL_l1509z00_4440 = CDR(BgL_l1509z00_2132);
									BgL_l1509z00_2132 = BgL_l1509z00_4440;
									goto BgL_zc3z04anonymousza31706ze3z87_2133;
								}
							}
						else
							{	/* SawMill/expr.scm 53 */
								return ((bool_t) 1);
							}
					}
				}
			}
		}

	}



/* reg->preg~0 */
	obj_t BGl_regzd2ze3pregze70zd6zzsaw_exprz00(obj_t BgL_nz00_4011,
		obj_t BgL_rz00_2141)
	{
		{	/* SawMill/expr.scm 35 */
			{	/* SawMill/expr.scm 31 */
				bool_t BgL_test2233z00_4442;

				{	/* SawMill/expr.scm 31 */
					obj_t BgL_classz00_3074;

					BgL_classz00_3074 = BGl_pregz00zzsaw_exprz00;
					if (BGL_OBJECTP(BgL_rz00_2141))
						{	/* SawMill/expr.scm 31 */
							obj_t BgL_oclassz00_3076;

							{	/* SawMill/expr.scm 31 */
								obj_t BgL_arg1815z00_3078;
								long BgL_arg1816z00_3079;

								BgL_arg1815z00_3078 = (BGl_za2classesza2z00zz__objectz00);
								{	/* SawMill/expr.scm 31 */
									long BgL_arg1817z00_3080;

									BgL_arg1817z00_3080 =
										BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_rz00_2141));
									BgL_arg1816z00_3079 = (BgL_arg1817z00_3080 - OBJECT_TYPE);
								}
								BgL_oclassz00_3076 =
									VECTOR_REF(BgL_arg1815z00_3078, BgL_arg1816z00_3079);
							}
							BgL_test2233z00_4442 = (BgL_oclassz00_3076 == BgL_classz00_3074);
						}
					else
						{	/* SawMill/expr.scm 31 */
							BgL_test2233z00_4442 = ((bool_t) 0);
						}
				}
				if (BgL_test2233z00_4442)
					{
						BgL_pregz00_bglt BgL_auxz00_4451;

						{
							obj_t BgL_auxz00_4452;

							{	/* SawMill/expr.scm 32 */
								BgL_objectz00_bglt BgL_tmpz00_4453;

								BgL_tmpz00_4453 =
									((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_2141));
								BgL_auxz00_4452 = BGL_OBJECT_WIDENING(BgL_tmpz00_4453);
							}
							BgL_auxz00_4451 = ((BgL_pregz00_bglt) BgL_auxz00_4452);
						}
						return
							(((BgL_pregz00_bglt) COBJECT(BgL_auxz00_4451))->BgL_statusz00);
					}
				else
					{	/* SawMill/expr.scm 31 */
						{	/* SawMill/expr.scm 33 */
							BgL_pregz00_bglt BgL_wide1207z00_2146;

							BgL_wide1207z00_2146 =
								((BgL_pregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_pregz00_bgl))));
							{	/* SawMill/expr.scm 33 */
								obj_t BgL_auxz00_4464;
								BgL_objectz00_bglt BgL_tmpz00_4460;

								BgL_auxz00_4464 = ((obj_t) BgL_wide1207z00_2146);
								BgL_tmpz00_4460 =
									((BgL_objectz00_bglt)
									((BgL_rtl_regz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_rz00_2141)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4460, BgL_auxz00_4464);
							}
							((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_2141)));
							{	/* SawMill/expr.scm 33 */
								long BgL_arg1714z00_2147;

								BgL_arg1714z00_2147 = BGL_CLASS_NUM(BGl_pregz00zzsaw_exprz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_rz00_2141))),
									BgL_arg1714z00_2147);
							}
							((BgL_rtl_regz00_bglt)
								((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_2141)));
						}
						{
							BgL_pregz00_bglt BgL_auxz00_4478;

							{
								obj_t BgL_auxz00_4479;

								{	/* SawMill/expr.scm 33 */
									BgL_objectz00_bglt BgL_tmpz00_4480;

									BgL_tmpz00_4480 =
										((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_rz00_2141)));
									BgL_auxz00_4479 = BGL_OBJECT_WIDENING(BgL_tmpz00_4480);
								}
								BgL_auxz00_4478 = ((BgL_pregz00_bglt) BgL_auxz00_4479);
							}
							((((BgL_pregz00_bglt) COBJECT(BgL_auxz00_4478))->BgL_indexz00) =
								((obj_t) ((obj_t) ((obj_t) CELL_REF(BgL_nz00_4011)))), BUNSPEC);
						}
						{
							BgL_pregz00_bglt BgL_auxz00_4488;

							{
								obj_t BgL_auxz00_4489;

								{	/* SawMill/expr.scm 33 */
									BgL_objectz00_bglt BgL_tmpz00_4490;

									BgL_tmpz00_4490 =
										((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_rz00_2141)));
									BgL_auxz00_4489 = BGL_OBJECT_WIDENING(BgL_tmpz00_4490);
								}
								BgL_auxz00_4488 = ((BgL_pregz00_bglt) BgL_auxz00_4489);
							}
							((((BgL_pregz00_bglt) COBJECT(BgL_auxz00_4488))->BgL_statusz00) =
								((obj_t) CNST_TABLE_REF(15)), BUNSPEC);
						}
						((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_2141));
						{	/* SawMill/expr.scm 34 */
							obj_t BgL_auxz00_4012;

							BgL_auxz00_4012 =
								ADDFX(((obj_t) ((obj_t) CELL_REF(BgL_nz00_4011))), BINT(1L));
							CELL_SET(BgL_nz00_4011, BgL_auxz00_4012);
						}
						return CNST_TABLE_REF(15);
					}
			}
		}

	}



/* reg->preg/write~0 */
	obj_t BGl_regzd2ze3pregzf2writeze70z24zzsaw_exprz00(obj_t BgL_nz00_4013,
		obj_t BgL_rz00_2152)
	{
		{	/* SawMill/expr.scm 45 */
			{	/* SawMill/expr.scm 42 */
				obj_t BgL_sz00_2154;

				BgL_sz00_2154 =
					BGl_regzd2ze3pregze70zd6zzsaw_exprz00(BgL_nz00_4013, BgL_rz00_2152);
				if ((BgL_sz00_2154 == CNST_TABLE_REF(15)))
					{	/* SawMill/expr.scm 43 */
						obj_t BgL_nstatz00_3104;

						BgL_nstatz00_3104 = CNST_TABLE_REF(17);
						{
							BgL_pregz00_bglt BgL_auxz00_4509;

							{
								obj_t BgL_auxz00_4510;

								{	/* SawMill/expr.scm 29 */
									BgL_objectz00_bglt BgL_tmpz00_4511;

									BgL_tmpz00_4511 =
										((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_rz00_2152));
									BgL_auxz00_4510 = BGL_OBJECT_WIDENING(BgL_tmpz00_4511);
								}
								BgL_auxz00_4509 = ((BgL_pregz00_bglt) BgL_auxz00_4510);
							}
							return
								((((BgL_pregz00_bglt) COBJECT(BgL_auxz00_4509))->
									BgL_statusz00) = ((obj_t) BgL_nstatz00_3104), BUNSPEC);
						}
					}
				else
					{	/* SawMill/expr.scm 43 */
						if ((BgL_sz00_2154 == CNST_TABLE_REF(16)))
							{	/* SawMill/expr.scm 44 */
								obj_t BgL_nstatz00_3107;

								BgL_nstatz00_3107 = CNST_TABLE_REF(18);
								{
									BgL_pregz00_bglt BgL_auxz00_4521;

									{
										obj_t BgL_auxz00_4522;

										{	/* SawMill/expr.scm 29 */
											BgL_objectz00_bglt BgL_tmpz00_4523;

											BgL_tmpz00_4523 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_rz00_2152));
											BgL_auxz00_4522 = BGL_OBJECT_WIDENING(BgL_tmpz00_4523);
										}
										BgL_auxz00_4521 = ((BgL_pregz00_bglt) BgL_auxz00_4522);
									}
									return
										((((BgL_pregz00_bglt) COBJECT(BgL_auxz00_4521))->
											BgL_statusz00) = ((obj_t) BgL_nstatz00_3107), BUNSPEC);
								}
							}
						else
							{	/* SawMill/expr.scm 45 */
								obj_t BgL_nstatz00_3110;

								BgL_nstatz00_3110 = CNST_TABLE_REF(19);
								{
									BgL_pregz00_bglt BgL_auxz00_4530;

									{
										obj_t BgL_auxz00_4531;

										{	/* SawMill/expr.scm 29 */
											BgL_objectz00_bglt BgL_tmpz00_4532;

											BgL_tmpz00_4532 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_rz00_2152));
											BgL_auxz00_4531 = BGL_OBJECT_WIDENING(BgL_tmpz00_4532);
										}
										BgL_auxz00_4530 = ((BgL_pregz00_bglt) BgL_auxz00_4531);
									}
									return
										((((BgL_pregz00_bglt) COBJECT(BgL_auxz00_4530))->
											BgL_statusz00) = ((obj_t) BgL_nstatz00_3110), BUNSPEC);
								}
							}
					}
			}
		}

	}



/* build-tree */
	BGL_EXPORTED_DEF obj_t BGl_buildzd2treezd2zzsaw_exprz00(BgL_backendz00_bglt
		BgL_bez00_95, obj_t BgL_paramsz00_96, obj_t BgL_lz00_97)
	{
		{	/* SawMill/expr.scm 58 */
			{

				BGl_markzd2localszd2zzsaw_exprz00(BgL_paramsz00_96, BgL_lz00_97);
				{
					obj_t BgL_l1513z00_2184;

					{	/* SawMill/expr.scm 72 */
						bool_t BgL_tmpz00_4539;

						BgL_l1513z00_2184 = BgL_lz00_97;
					BgL_zc3z04anonymousza31736ze3z87_2185:
						if (PAIRP(BgL_l1513z00_2184))
							{	/* SawMill/expr.scm 72 */
								{	/* SawMill/expr.scm 73 */
									obj_t BgL_bz00_2187;

									BgL_bz00_2187 = CAR(BgL_l1513z00_2184);
									{	/* SawMill/expr.scm 73 */
										obj_t BgL_lz00_2188;
										obj_t BgL_movesz00_2189;

										BgL_lz00_2188 =
											(((BgL_blockz00_bglt) COBJECT(
													((BgL_blockz00_bglt) BgL_bz00_2187)))->BgL_firstz00);
										BgL_movesz00_2189 = BNIL;
										{
											obj_t BgL_l1511z00_2191;

											BgL_l1511z00_2191 = BgL_lz00_2188;
										BgL_zc3z04anonymousza31738ze3z87_2192:
											if (PAIRP(BgL_l1511z00_2191))
												{	/* SawMill/expr.scm 74 */
													{	/* SawMill/expr.scm 74 */
														obj_t BgL_insz00_2194;

														BgL_insz00_2194 = CAR(BgL_l1511z00_2191);
														BgL_movesz00_2189 =
															BGl_analysez00zzsaw_exprz00(BgL_bez00_95,
															BgL_movesz00_2189, BgL_insz00_2194);
													}
													{
														obj_t BgL_l1511z00_4549;

														BgL_l1511z00_4549 = CDR(BgL_l1511z00_2191);
														BgL_l1511z00_2191 = BgL_l1511z00_4549;
														goto BgL_zc3z04anonymousza31738ze3z87_2192;
													}
												}
											else
												{	/* SawMill/expr.scm 74 */
													((bool_t) 1);
												}
										}
										{	/* SawMill/expr.scm 76 */
											obj_t BgL_rz00_2197;

											BgL_rz00_2197 =
												BGl_filterz12z12zz__r4_control_features_6_9z00
												(BGl_proc2132z00zzsaw_exprz00, BgL_lz00_2188);
											{	/* SawMill/expr.scm 77 */
												obj_t BgL_arg1746z00_2198;

												if (NULLP(BgL_rz00_2197))
													{	/* SawMill/expr.scm 77 */
														BgL_rtl_insz00_bglt BgL_arg1748z00_2200;

														{	/* SawMill/expr.scm 60 */
															BgL_rtl_insz00_bglt BgL_new1211z00_2205;

															{	/* SawMill/expr.scm 60 */
																BgL_rtl_insz00_bglt BgL_new1210z00_2209;

																BgL_new1210z00_2209 =
																	((BgL_rtl_insz00_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_rtl_insz00_bgl))));
																{	/* SawMill/expr.scm 60 */
																	long BgL_arg1753z00_2210;

																	BgL_arg1753z00_2210 =
																		BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
																	BGL_OBJECT_CLASS_NUM_SET(
																		((BgL_objectz00_bglt) BgL_new1210z00_2209),
																		BgL_arg1753z00_2210);
																}
																{	/* SawMill/expr.scm 60 */
																	BgL_objectz00_bglt BgL_tmpz00_4558;

																	BgL_tmpz00_4558 =
																		((BgL_objectz00_bglt) BgL_new1210z00_2209);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4558,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1210z00_2209);
																BgL_new1211z00_2205 = BgL_new1210z00_2209;
															}
															((((BgL_rtl_insz00_bglt)
																		COBJECT(BgL_new1211z00_2205))->BgL_locz00) =
																((obj_t) BFALSE), BUNSPEC);
															((((BgL_rtl_insz00_bglt)
																		COBJECT(BgL_new1211z00_2205))->
																	BgL_z52spillz52) = ((obj_t) BNIL), BUNSPEC);
															((((BgL_rtl_insz00_bglt)
																		COBJECT(BgL_new1211z00_2205))->
																	BgL_destz00) = ((obj_t) BFALSE), BUNSPEC);
															{
																BgL_rtl_funz00_bglt BgL_auxz00_4565;

																{	/* SawMill/expr.scm 60 */
																	BgL_rtl_nopz00_bglt BgL_new1213z00_2206;

																	{	/* SawMill/expr.scm 60 */
																		BgL_rtl_nopz00_bglt BgL_new1212z00_2207;

																		BgL_new1212z00_2207 =
																			((BgL_rtl_nopz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_rtl_nopz00_bgl))));
																		{	/* SawMill/expr.scm 60 */
																			long BgL_arg1752z00_2208;

																			{	/* SawMill/expr.scm 60 */
																				obj_t BgL_classz00_3126;

																				BgL_classz00_3126 =
																					BGl_rtl_nopz00zzsaw_defsz00;
																				BgL_arg1752z00_2208 =
																					BGL_CLASS_NUM(BgL_classz00_3126);
																			}
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1212z00_2207),
																				BgL_arg1752z00_2208);
																		}
																		BgL_new1213z00_2206 = BgL_new1212z00_2207;
																	}
																	((((BgL_rtl_funz00_bglt) COBJECT(
																					((BgL_rtl_funz00_bglt)
																						BgL_new1213z00_2206)))->
																			BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
																	BgL_auxz00_4565 =
																		((BgL_rtl_funz00_bglt) BgL_new1213z00_2206);
																}
																((((BgL_rtl_insz00_bglt)
																			COBJECT(BgL_new1211z00_2205))->
																		BgL_funz00) =
																	((BgL_rtl_funz00_bglt) BgL_auxz00_4565),
																	BUNSPEC);
															}
															((((BgL_rtl_insz00_bglt)
																		COBJECT(BgL_new1211z00_2205))->
																	BgL_argsz00) = ((obj_t) BNIL), BUNSPEC);
															BgL_arg1748z00_2200 = BgL_new1211z00_2205;
														}
														{	/* SawMill/expr.scm 77 */
															obj_t BgL_list1749z00_2201;

															BgL_list1749z00_2201 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg1748z00_2200), BNIL);
															BgL_arg1746z00_2198 = BgL_list1749z00_2201;
													}}
												else
													{	/* SawMill/expr.scm 77 */
														BgL_arg1746z00_2198 = BgL_rz00_2197;
													}
												((((BgL_blockz00_bglt) COBJECT(
																((BgL_blockz00_bglt) BgL_bz00_2187)))->
														BgL_firstz00) =
													((obj_t) BgL_arg1746z00_2198), BUNSPEC);
											}
										}
									}
								}
								{
									obj_t BgL_l1513z00_4579;

									BgL_l1513z00_4579 = CDR(BgL_l1513z00_2184);
									BgL_l1513z00_2184 = BgL_l1513z00_4579;
									goto BgL_zc3z04anonymousza31736ze3z87_2185;
								}
							}
						else
							{	/* SawMill/expr.scm 72 */
								BgL_tmpz00_4539 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_4539);
					}
				}
			}
		}

	}



/* &build-tree */
	obj_t BGl_z62buildzd2treezb0zzsaw_exprz00(obj_t BgL_envz00_3860,
		obj_t BgL_bez00_3861, obj_t BgL_paramsz00_3862, obj_t BgL_lz00_3863)
	{
		{	/* SawMill/expr.scm 58 */
			return
				BGl_buildzd2treezd2zzsaw_exprz00(
				((BgL_backendz00_bglt) BgL_bez00_3861), BgL_paramsz00_3862,
				BgL_lz00_3863);
		}

	}



/* &useful */
	obj_t BGl_z62usefulz62zzsaw_exprz00(obj_t BgL_envz00_3864,
		obj_t BgL_insz00_3865)
	{
		{	/* SawMill/expr.scm 69 */
			{	/* SawMill/expr.scm 63 */
				bool_t BgL_tmpz00_4584;

				{
					obj_t BgL_insz00_4028;
					obj_t BgL_destz00_4029;

					{	/* SawMill/expr.scm 70 */
						bool_t BgL_test2240z00_4585;

						{	/* SawMill/expr.scm 70 */
							bool_t BgL_test2241z00_4586;

							{	/* SawMill/expr.scm 70 */
								obj_t BgL_classz00_4063;

								BgL_classz00_4063 = BGl_inlinedz00zzsaw_exprz00;
								if (BGL_OBJECTP(BgL_insz00_3865))
									{	/* SawMill/expr.scm 70 */
										obj_t BgL_oclassz00_4064;

										{	/* SawMill/expr.scm 70 */
											obj_t BgL_arg1815z00_4065;
											long BgL_arg1816z00_4066;

											BgL_arg1815z00_4065 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawMill/expr.scm 70 */
												long BgL_arg1817z00_4067;

												BgL_arg1817z00_4067 =
													BGL_OBJECT_CLASS_NUM(
													((BgL_objectz00_bglt) BgL_insz00_3865));
												BgL_arg1816z00_4066 =
													(BgL_arg1817z00_4067 - OBJECT_TYPE);
											}
											BgL_oclassz00_4064 =
												VECTOR_REF(BgL_arg1815z00_4065, BgL_arg1816z00_4066);
										}
										BgL_test2241z00_4586 =
											(BgL_oclassz00_4064 == BgL_classz00_4063);
									}
								else
									{	/* SawMill/expr.scm 70 */
										BgL_test2241z00_4586 = ((bool_t) 0);
									}
							}
							if (BgL_test2241z00_4586)
								{	/* SawMill/expr.scm 70 */
									BgL_test2240z00_4585 = ((bool_t) 1);
								}
							else
								{	/* SawMill/expr.scm 70 */
									obj_t BgL_arg1765z00_4068;

									BgL_arg1765z00_4068 =
										(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_insz00_3865)))->BgL_destz00);
									BgL_insz00_4028 = BgL_insz00_3865;
									BgL_destz00_4029 = BgL_arg1765z00_4068;
								BgL_xzd3xzd3_4027:
									{	/* SawMill/expr.scm 63 */
										BgL_rtl_funz00_bglt BgL_funz00_4030;

										BgL_funz00_4030 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_insz00_4028)))->
											BgL_funz00);
										{	/* SawMill/expr.scm 64 */
											bool_t BgL_test2243z00_4599;

											{	/* SawMill/expr.scm 64 */
												obj_t BgL_classz00_4031;

												BgL_classz00_4031 = BGl_rtl_movz00zzsaw_defsz00;
												{	/* SawMill/expr.scm 64 */
													BgL_objectz00_bglt BgL_arg1807z00_4032;

													{	/* SawMill/expr.scm 64 */
														obj_t BgL_tmpz00_4600;

														BgL_tmpz00_4600 =
															((obj_t) ((BgL_objectz00_bglt) BgL_funz00_4030));
														BgL_arg1807z00_4032 =
															(BgL_objectz00_bglt) (BgL_tmpz00_4600);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* SawMill/expr.scm 64 */
															long BgL_idxz00_4033;

															BgL_idxz00_4033 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4032);
															BgL_test2243z00_4599 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_4033 + 3L)) == BgL_classz00_4031);
														}
													else
														{	/* SawMill/expr.scm 64 */
															bool_t BgL_res2109z00_4036;

															{	/* SawMill/expr.scm 64 */
																obj_t BgL_oclassz00_4037;

																{	/* SawMill/expr.scm 64 */
																	obj_t BgL_arg1815z00_4038;
																	long BgL_arg1816z00_4039;

																	BgL_arg1815z00_4038 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* SawMill/expr.scm 64 */
																		long BgL_arg1817z00_4040;

																		BgL_arg1817z00_4040 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4032);
																		BgL_arg1816z00_4039 =
																			(BgL_arg1817z00_4040 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_4037 =
																		VECTOR_REF(BgL_arg1815z00_4038,
																		BgL_arg1816z00_4039);
																}
																{	/* SawMill/expr.scm 64 */
																	bool_t BgL__ortest_1115z00_4041;

																	BgL__ortest_1115z00_4041 =
																		(BgL_classz00_4031 == BgL_oclassz00_4037);
																	if (BgL__ortest_1115z00_4041)
																		{	/* SawMill/expr.scm 64 */
																			BgL_res2109z00_4036 =
																				BgL__ortest_1115z00_4041;
																		}
																	else
																		{	/* SawMill/expr.scm 64 */
																			long BgL_odepthz00_4042;

																			{	/* SawMill/expr.scm 64 */
																				obj_t BgL_arg1804z00_4043;

																				BgL_arg1804z00_4043 =
																					(BgL_oclassz00_4037);
																				BgL_odepthz00_4042 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_4043);
																			}
																			if ((3L < BgL_odepthz00_4042))
																				{	/* SawMill/expr.scm 64 */
																					obj_t BgL_arg1802z00_4044;

																					{	/* SawMill/expr.scm 64 */
																						obj_t BgL_arg1803z00_4045;

																						BgL_arg1803z00_4045 =
																							(BgL_oclassz00_4037);
																						BgL_arg1802z00_4044 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_4045, 3L);
																					}
																					BgL_res2109z00_4036 =
																						(BgL_arg1802z00_4044 ==
																						BgL_classz00_4031);
																				}
																			else
																				{	/* SawMill/expr.scm 64 */
																					BgL_res2109z00_4036 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2243z00_4599 = BgL_res2109z00_4036;
														}
												}
											}
											if (BgL_test2243z00_4599)
												{	/* SawMill/expr.scm 65 */
													obj_t BgL_argz00_4046;

													{	/* SawMill/expr.scm 65 */
														obj_t BgL_pairz00_4047;

														BgL_pairz00_4047 =
															(((BgL_rtl_insz00_bglt) COBJECT(
																	((BgL_rtl_insz00_bglt) BgL_insz00_4028)))->
															BgL_argsz00);
														BgL_argz00_4046 = CAR(BgL_pairz00_4047);
													}
													{	/* SawMill/expr.scm 66 */
														bool_t BgL_test2247z00_4626;

														{	/* SawMill/expr.scm 66 */
															obj_t BgL_classz00_4048;

															BgL_classz00_4048 = BGl_rtl_insz00zzsaw_defsz00;
															if (BGL_OBJECTP(BgL_argz00_4046))
																{	/* SawMill/expr.scm 66 */
																	BgL_objectz00_bglt BgL_arg1807z00_4049;

																	BgL_arg1807z00_4049 =
																		(BgL_objectz00_bglt) (BgL_argz00_4046);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* SawMill/expr.scm 66 */
																			long BgL_idxz00_4050;

																			BgL_idxz00_4050 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_4049);
																			BgL_test2247z00_4626 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_4050 + 1L)) ==
																				BgL_classz00_4048);
																		}
																	else
																		{	/* SawMill/expr.scm 66 */
																			bool_t BgL_res2110z00_4053;

																			{	/* SawMill/expr.scm 66 */
																				obj_t BgL_oclassz00_4054;

																				{	/* SawMill/expr.scm 66 */
																					obj_t BgL_arg1815z00_4055;
																					long BgL_arg1816z00_4056;

																					BgL_arg1815z00_4055 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* SawMill/expr.scm 66 */
																						long BgL_arg1817z00_4057;

																						BgL_arg1817z00_4057 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_4049);
																						BgL_arg1816z00_4056 =
																							(BgL_arg1817z00_4057 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_4054 =
																						VECTOR_REF(BgL_arg1815z00_4055,
																						BgL_arg1816z00_4056);
																				}
																				{	/* SawMill/expr.scm 66 */
																					bool_t BgL__ortest_1115z00_4058;

																					BgL__ortest_1115z00_4058 =
																						(BgL_classz00_4048 ==
																						BgL_oclassz00_4054);
																					if (BgL__ortest_1115z00_4058)
																						{	/* SawMill/expr.scm 66 */
																							BgL_res2110z00_4053 =
																								BgL__ortest_1115z00_4058;
																						}
																					else
																						{	/* SawMill/expr.scm 66 */
																							long BgL_odepthz00_4059;

																							{	/* SawMill/expr.scm 66 */
																								obj_t BgL_arg1804z00_4060;

																								BgL_arg1804z00_4060 =
																									(BgL_oclassz00_4054);
																								BgL_odepthz00_4059 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_4060);
																							}
																							if ((1L < BgL_odepthz00_4059))
																								{	/* SawMill/expr.scm 66 */
																									obj_t BgL_arg1802z00_4061;

																									{	/* SawMill/expr.scm 66 */
																										obj_t BgL_arg1803z00_4062;

																										BgL_arg1803z00_4062 =
																											(BgL_oclassz00_4054);
																										BgL_arg1802z00_4061 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_4062, 1L);
																									}
																									BgL_res2110z00_4053 =
																										(BgL_arg1802z00_4061 ==
																										BgL_classz00_4048);
																								}
																							else
																								{	/* SawMill/expr.scm 66 */
																									BgL_res2110z00_4053 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2247z00_4626 =
																				BgL_res2110z00_4053;
																		}
																}
															else
																{	/* SawMill/expr.scm 66 */
																	BgL_test2247z00_4626 = ((bool_t) 0);
																}
														}
														if (BgL_test2247z00_4626)
															{
																obj_t BgL_insz00_4649;

																BgL_insz00_4649 = BgL_argz00_4046;
																BgL_insz00_4028 = BgL_insz00_4649;
																goto BgL_xzd3xzd3_4027;
															}
														else
															{	/* SawMill/expr.scm 66 */
																BgL_test2240z00_4585 =
																	(BgL_argz00_4046 == BgL_destz00_4029);
															}
													}
												}
											else
												{	/* SawMill/expr.scm 64 */
													BgL_test2240z00_4585 = ((bool_t) 0);
												}
										}
									}
								}
						}
						if (BgL_test2240z00_4585)
							{	/* SawMill/expr.scm 70 */
								BgL_tmpz00_4584 = ((bool_t) 0);
							}
						else
							{	/* SawMill/expr.scm 70 */
								BgL_tmpz00_4584 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_4584);
			}
		}

	}



/* mark-locals */
	bool_t BGl_markzd2localszd2zzsaw_exprz00(obj_t BgL_paramsz00_98,
		obj_t BgL_lz00_99)
	{
		{	/* SawMill/expr.scm 80 */
			{	/* SawMill/expr.scm 81 */
				struct bgl_cell BgL_box2252_4009z00;
				obj_t BgL_nz00_4009;

				BgL_nz00_4009 = MAKE_CELL_STACK(BINT(0L), BgL_box2252_4009z00);
				{
					obj_t BgL_rz00_2261;
					BgL_blockz00_bglt BgL_bz00_2267;

					{
						obj_t BgL_l1521z00_2237;

						BgL_l1521z00_2237 = BgL_paramsz00_98;
					BgL_zc3z04anonymousza31771ze3z87_2238:
						if (PAIRP(BgL_l1521z00_2237))
							{	/* SawMill/expr.scm 105 */
								BGl_regzd2ze3iregzf2writeze70z24zzsaw_exprz00(BgL_nz00_4009,
									CAR(BgL_l1521z00_2237));
								{
									obj_t BgL_l1521z00_4657;

									BgL_l1521z00_4657 = CDR(BgL_l1521z00_2237);
									BgL_l1521z00_2237 = BgL_l1521z00_4657;
									goto BgL_zc3z04anonymousza31771ze3z87_2238;
								}
							}
						else
							{	/* SawMill/expr.scm 105 */
								((bool_t) 1);
							}
					}
					{
						obj_t BgL_l1523z00_2244;

						BgL_l1523z00_2244 = BgL_lz00_99;
					BgL_zc3z04anonymousza31776ze3z87_2245:
						if (PAIRP(BgL_l1523z00_2244))
							{	/* SawMill/expr.scm 106 */
								{	/* SawMill/expr.scm 106 */
									obj_t BgL_arg1798z00_2247;

									BgL_arg1798z00_2247 = CAR(BgL_l1523z00_2244);
									BgL_bz00_2267 = ((BgL_blockz00_bglt) BgL_arg1798z00_2247);
									{	/* SawMill/expr.scm 100 */
										obj_t BgL_g1520z00_2269;

										BgL_g1520z00_2269 =
											(((BgL_blockz00_bglt) COBJECT(BgL_bz00_2267))->
											BgL_firstz00);
										{
											obj_t BgL_l1518z00_2271;

											BgL_l1518z00_2271 = BgL_g1520z00_2269;
										BgL_zc3z04anonymousza31809ze3z87_2272:
											if (PAIRP(BgL_l1518z00_2271))
												{	/* SawMill/expr.scm 104 */
													{	/* SawMill/expr.scm 101 */
														obj_t BgL_insz00_2274;

														BgL_insz00_2274 = CAR(BgL_l1518z00_2271);
														if (CBOOL(
																(((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt)
																				BgL_insz00_2274)))->BgL_destz00)))
															{	/* SawMill/expr.scm 102 */
																obj_t BgL_arg1812z00_2277;

																BgL_arg1812z00_2277 =
																	(((BgL_rtl_insz00_bglt) COBJECT(
																			((BgL_rtl_insz00_bglt)
																				BgL_insz00_2274)))->BgL_destz00);
																BGl_regzd2ze3iregzf2writeze70z24zzsaw_exprz00
																	(BgL_nz00_4009, BgL_arg1812z00_2277);
															}
														else
															{	/* SawMill/expr.scm 102 */
																BFALSE;
															}
														{	/* SawMill/expr.scm 103 */
															obj_t BgL_g1517z00_2278;

															BgL_g1517z00_2278 =
																(((BgL_rtl_insz00_bglt) COBJECT(
																		((BgL_rtl_insz00_bglt) BgL_insz00_2274)))->
																BgL_argsz00);
															{
																obj_t BgL_l1515z00_2280;

																BgL_l1515z00_2280 = BgL_g1517z00_2278;
															BgL_zc3z04anonymousza31813ze3z87_2281:
																if (PAIRP(BgL_l1515z00_2280))
																	{	/* SawMill/expr.scm 103 */
																		BgL_rz00_2261 = CAR(BgL_l1515z00_2280);
																		{	/* SawMill/expr.scm 90 */
																			obj_t BgL_sz00_2263;

																			BgL_sz00_2263 =
																				BGl_regzd2ze3iregze70zd6zzsaw_exprz00
																				(BgL_nz00_4009, BgL_rz00_2261);
																			if ((BgL_sz00_2263 == CNST_TABLE_REF(15)))
																				{	/* SawMill/expr.scm 91 */
																					obj_t BgL_nstatz00_3244;

																					BgL_nstatz00_3244 =
																						CNST_TABLE_REF(16);
																					{
																						BgL_iregz00_bglt BgL_auxz00_4682;

																						{
																							obj_t BgL_auxz00_4683;

																							{	/* SawMill/expr.scm 82 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_4684;
																								BgL_tmpz00_4684 =
																									((BgL_objectz00_bglt) (
																										(BgL_rtl_regz00_bglt)
																										BgL_rz00_2261));
																								BgL_auxz00_4683 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_4684);
																							}
																							BgL_auxz00_4682 =
																								((BgL_iregz00_bglt)
																								BgL_auxz00_4683);
																						}
																						((((BgL_iregz00_bglt)
																									COBJECT(BgL_auxz00_4682))->
																								BgL_statusz00) =
																							((obj_t) BgL_nstatz00_3244),
																							BUNSPEC);
																					}
																				}
																			else
																				{	/* SawMill/expr.scm 91 */
																					if (
																						(BgL_sz00_2263 ==
																							CNST_TABLE_REF(17)))
																						{	/* SawMill/expr.scm 92 */
																							obj_t BgL_nstatz00_3247;

																							BgL_nstatz00_3247 =
																								CNST_TABLE_REF(18);
																							{
																								BgL_iregz00_bglt
																									BgL_auxz00_4694;
																								{
																									obj_t BgL_auxz00_4695;

																									{	/* SawMill/expr.scm 82 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_4696;
																										BgL_tmpz00_4696 =
																											((BgL_objectz00_bglt) (
																												(BgL_rtl_regz00_bglt)
																												BgL_rz00_2261));
																										BgL_auxz00_4695 =
																											BGL_OBJECT_WIDENING
																											(BgL_tmpz00_4696);
																									}
																									BgL_auxz00_4694 =
																										((BgL_iregz00_bglt)
																										BgL_auxz00_4695);
																								}
																								((((BgL_iregz00_bglt)
																											COBJECT
																											(BgL_auxz00_4694))->
																										BgL_statusz00) =
																									((obj_t) BgL_nstatz00_3247),
																									BUNSPEC);
																							}
																						}
																					else
																						{	/* SawMill/expr.scm 93 */
																							obj_t BgL_nstatz00_3250;

																							BgL_nstatz00_3250 =
																								CNST_TABLE_REF(19);
																							{
																								BgL_iregz00_bglt
																									BgL_auxz00_4703;
																								{
																									obj_t BgL_auxz00_4704;

																									{	/* SawMill/expr.scm 82 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_4705;
																										BgL_tmpz00_4705 =
																											((BgL_objectz00_bglt) (
																												(BgL_rtl_regz00_bglt)
																												BgL_rz00_2261));
																										BgL_auxz00_4704 =
																											BGL_OBJECT_WIDENING
																											(BgL_tmpz00_4705);
																									}
																									BgL_auxz00_4703 =
																										((BgL_iregz00_bglt)
																										BgL_auxz00_4704);
																								}
																								((((BgL_iregz00_bglt)
																											COBJECT
																											(BgL_auxz00_4703))->
																										BgL_statusz00) =
																									((obj_t) BgL_nstatz00_3250),
																									BUNSPEC);
																							}
																						}
																				}
																		}
																		{
																			obj_t BgL_l1515z00_4712;

																			BgL_l1515z00_4712 =
																				CDR(BgL_l1515z00_2280);
																			BgL_l1515z00_2280 = BgL_l1515z00_4712;
																			goto
																				BgL_zc3z04anonymousza31813ze3z87_2281;
																		}
																	}
																else
																	{	/* SawMill/expr.scm 103 */
																		((bool_t) 1);
																	}
															}
														}
													}
													{
														obj_t BgL_l1518z00_4714;

														BgL_l1518z00_4714 = CDR(BgL_l1518z00_2271);
														BgL_l1518z00_2271 = BgL_l1518z00_4714;
														goto BgL_zc3z04anonymousza31809ze3z87_2272;
													}
												}
											else
												{	/* SawMill/expr.scm 104 */
													((bool_t) 1);
												}
										}
									}
								}
								{
									obj_t BgL_l1523z00_4717;

									BgL_l1523z00_4717 = CDR(BgL_l1523z00_2244);
									BgL_l1523z00_2244 = BgL_l1523z00_4717;
									goto BgL_zc3z04anonymousza31776ze3z87_2245;
								}
							}
						else
							{	/* SawMill/expr.scm 106 */
								return ((bool_t) 1);
							}
					}
				}
			}
		}

	}



/* reg->ireg~0 */
	obj_t BGl_regzd2ze3iregze70zd6zzsaw_exprz00(obj_t BgL_nz00_4006,
		obj_t BgL_rz00_2253)
	{
		{	/* SawMill/expr.scm 88 */
			{	/* SawMill/expr.scm 84 */
				bool_t BgL_test2260z00_4719;

				{	/* SawMill/expr.scm 84 */
					obj_t BgL_classz00_3223;

					BgL_classz00_3223 = BGl_iregz00zzsaw_exprz00;
					if (BGL_OBJECTP(BgL_rz00_2253))
						{	/* SawMill/expr.scm 84 */
							obj_t BgL_oclassz00_3225;

							{	/* SawMill/expr.scm 84 */
								obj_t BgL_arg1815z00_3227;
								long BgL_arg1816z00_3228;

								BgL_arg1815z00_3227 = (BGl_za2classesza2z00zz__objectz00);
								{	/* SawMill/expr.scm 84 */
									long BgL_arg1817z00_3229;

									BgL_arg1817z00_3229 =
										BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_rz00_2253));
									BgL_arg1816z00_3228 = (BgL_arg1817z00_3229 - OBJECT_TYPE);
								}
								BgL_oclassz00_3225 =
									VECTOR_REF(BgL_arg1815z00_3227, BgL_arg1816z00_3228);
							}
							BgL_test2260z00_4719 = (BgL_oclassz00_3225 == BgL_classz00_3223);
						}
					else
						{	/* SawMill/expr.scm 84 */
							BgL_test2260z00_4719 = ((bool_t) 0);
						}
				}
				if (BgL_test2260z00_4719)
					{
						BgL_iregz00_bglt BgL_auxz00_4728;

						{
							obj_t BgL_auxz00_4729;

							{	/* SawMill/expr.scm 85 */
								BgL_objectz00_bglt BgL_tmpz00_4730;

								BgL_tmpz00_4730 =
									((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_2253));
								BgL_auxz00_4729 = BGL_OBJECT_WIDENING(BgL_tmpz00_4730);
							}
							BgL_auxz00_4728 = ((BgL_iregz00_bglt) BgL_auxz00_4729);
						}
						return
							(((BgL_iregz00_bglt) COBJECT(BgL_auxz00_4728))->BgL_statusz00);
					}
				else
					{	/* SawMill/expr.scm 84 */
						{	/* SawMill/expr.scm 86 */
							BgL_iregz00_bglt BgL_wide1216z00_2258;

							BgL_wide1216z00_2258 =
								((BgL_iregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_iregz00_bgl))));
							{	/* SawMill/expr.scm 86 */
								obj_t BgL_auxz00_4741;
								BgL_objectz00_bglt BgL_tmpz00_4737;

								BgL_auxz00_4741 = ((obj_t) BgL_wide1216z00_2258);
								BgL_tmpz00_4737 =
									((BgL_objectz00_bglt)
									((BgL_rtl_regz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_rz00_2253)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4737, BgL_auxz00_4741);
							}
							((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_2253)));
							{	/* SawMill/expr.scm 86 */
								long BgL_arg1805z00_2259;

								BgL_arg1805z00_2259 = BGL_CLASS_NUM(BGl_iregz00zzsaw_exprz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_rz00_2253))),
									BgL_arg1805z00_2259);
							}
							((BgL_rtl_regz00_bglt)
								((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_2253)));
						}
						{
							BgL_iregz00_bglt BgL_auxz00_4755;

							{
								obj_t BgL_auxz00_4756;

								{	/* SawMill/expr.scm 86 */
									BgL_objectz00_bglt BgL_tmpz00_4757;

									BgL_tmpz00_4757 =
										((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_rz00_2253)));
									BgL_auxz00_4756 = BGL_OBJECT_WIDENING(BgL_tmpz00_4757);
								}
								BgL_auxz00_4755 = ((BgL_iregz00_bglt) BgL_auxz00_4756);
							}
							((((BgL_iregz00_bglt) COBJECT(BgL_auxz00_4755))->BgL_indexz00) =
								((obj_t) ((obj_t) ((obj_t) CELL_REF(BgL_nz00_4006)))), BUNSPEC);
						}
						{
							BgL_iregz00_bglt BgL_auxz00_4765;

							{
								obj_t BgL_auxz00_4766;

								{	/* SawMill/expr.scm 86 */
									BgL_objectz00_bglt BgL_tmpz00_4767;

									BgL_tmpz00_4767 =
										((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt)
											((BgL_rtl_regz00_bglt) BgL_rz00_2253)));
									BgL_auxz00_4766 = BGL_OBJECT_WIDENING(BgL_tmpz00_4767);
								}
								BgL_auxz00_4765 = ((BgL_iregz00_bglt) BgL_auxz00_4766);
							}
							((((BgL_iregz00_bglt) COBJECT(BgL_auxz00_4765))->BgL_statusz00) =
								((obj_t) CNST_TABLE_REF(15)), BUNSPEC);
						}
						((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_rz00_2253));
						{	/* SawMill/expr.scm 87 */
							obj_t BgL_auxz00_4007;

							BgL_auxz00_4007 =
								ADDFX(((obj_t) ((obj_t) CELL_REF(BgL_nz00_4006))), BINT(1L));
							CELL_SET(BgL_nz00_4006, BgL_auxz00_4007);
						}
						return CNST_TABLE_REF(15);
					}
			}
		}

	}



/* reg->ireg/write~0 */
	obj_t BGl_regzd2ze3iregzf2writeze70z24zzsaw_exprz00(obj_t BgL_nz00_4008,
		obj_t BgL_rz00_2264)
	{
		{	/* SawMill/expr.scm 98 */
			{	/* SawMill/expr.scm 95 */
				obj_t BgL_sz00_2266;

				BgL_sz00_2266 =
					BGl_regzd2ze3iregze70zd6zzsaw_exprz00(BgL_nz00_4008, BgL_rz00_2264);
				if ((BgL_sz00_2266 == CNST_TABLE_REF(15)))
					{	/* SawMill/expr.scm 96 */
						obj_t BgL_nstatz00_3253;

						BgL_nstatz00_3253 = CNST_TABLE_REF(17);
						{
							BgL_iregz00_bglt BgL_auxz00_4786;

							{
								obj_t BgL_auxz00_4787;

								{	/* SawMill/expr.scm 82 */
									BgL_objectz00_bglt BgL_tmpz00_4788;

									BgL_tmpz00_4788 =
										((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_rz00_2264));
									BgL_auxz00_4787 = BGL_OBJECT_WIDENING(BgL_tmpz00_4788);
								}
								BgL_auxz00_4786 = ((BgL_iregz00_bglt) BgL_auxz00_4787);
							}
							return
								((((BgL_iregz00_bglt) COBJECT(BgL_auxz00_4786))->
									BgL_statusz00) = ((obj_t) BgL_nstatz00_3253), BUNSPEC);
						}
					}
				else
					{	/* SawMill/expr.scm 96 */
						if ((BgL_sz00_2266 == CNST_TABLE_REF(16)))
							{	/* SawMill/expr.scm 97 */
								obj_t BgL_nstatz00_3256;

								BgL_nstatz00_3256 = CNST_TABLE_REF(18);
								{
									BgL_iregz00_bglt BgL_auxz00_4798;

									{
										obj_t BgL_auxz00_4799;

										{	/* SawMill/expr.scm 82 */
											BgL_objectz00_bglt BgL_tmpz00_4800;

											BgL_tmpz00_4800 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_rz00_2264));
											BgL_auxz00_4799 = BGL_OBJECT_WIDENING(BgL_tmpz00_4800);
										}
										BgL_auxz00_4798 = ((BgL_iregz00_bglt) BgL_auxz00_4799);
									}
									return
										((((BgL_iregz00_bglt) COBJECT(BgL_auxz00_4798))->
											BgL_statusz00) = ((obj_t) BgL_nstatz00_3256), BUNSPEC);
								}
							}
						else
							{	/* SawMill/expr.scm 98 */
								obj_t BgL_nstatz00_3259;

								BgL_nstatz00_3259 = CNST_TABLE_REF(19);
								{
									BgL_iregz00_bglt BgL_auxz00_4807;

									{
										obj_t BgL_auxz00_4808;

										{	/* SawMill/expr.scm 82 */
											BgL_objectz00_bglt BgL_tmpz00_4809;

											BgL_tmpz00_4809 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_rz00_2264));
											BgL_auxz00_4808 = BGL_OBJECT_WIDENING(BgL_tmpz00_4809);
										}
										BgL_auxz00_4807 = ((BgL_iregz00_bglt) BgL_auxz00_4808);
									}
									return
										((((BgL_iregz00_bglt) COBJECT(BgL_auxz00_4807))->
											BgL_statusz00) = ((obj_t) BgL_nstatz00_3259), BUNSPEC);
								}
							}
					}
			}
		}

	}



/* pre-analyse */
	obj_t BGl_prezd2analysezd2zzsaw_exprz00(BgL_backendz00_bglt BgL_bz00_100,
		obj_t BgL_movingz00_101, obj_t BgL_insz00_102)
	{
		{	/* SawMill/expr.scm 112 */
			{	/* SawMill/expr.scm 113 */
				obj_t BgL_inszd2effectzd2_2293;
				obj_t BgL_argsz00_2294;

				{	/* SawMill/expr.scm 113 */
					BgL_rtl_funz00_bglt BgL_arg1857z00_2335;

					BgL_arg1857z00_2335 =
						(((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_insz00_102)))->BgL_funz00);
					BgL_inszd2effectzd2_2293 =
						BGl_effectsz00zzsaw_exprz00(BgL_arg1857z00_2335);
				}
				BgL_argsz00_2294 =
					(((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_insz00_102)))->BgL_argsz00);
				{
					obj_t BgL_lz00_2315;
					obj_t BgL_rz00_2316;
					obj_t BgL_treez00_2310;
					obj_t BgL_posz00_2311;

					{	/* SawMill/expr.scm 128 */
						obj_t BgL_rz00_2297;
						obj_t BgL_dz00_2298;

						BgL_lz00_2315 = BgL_movingz00_101;
						BgL_rz00_2316 = BNIL;
					BgL_zc3z04anonymousza31843ze3z87_2317:
						if (NULLP(BgL_lz00_2315))
							{	/* SawMill/expr.scm 119 */
								BgL_rz00_2297 = BgL_rz00_2316;
							}
						else
							{	/* SawMill/expr.scm 120 */
								obj_t BgL_g1219z00_2319;

								BgL_g1219z00_2319 =
									BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
									(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt)
													CAR(
														((obj_t) BgL_lz00_2315)))))->BgL_destz00),
									BgL_argsz00_2294);
								if (CBOOL(BgL_g1219z00_2319))
									{	/* SawMill/expr.scm 120 */
										{	/* SawMill/expr.scm 122 */
											obj_t BgL_arg1845z00_2322;

											BgL_arg1845z00_2322 = CAR(((obj_t) BgL_lz00_2315));
											BgL_treez00_2310 = BgL_arg1845z00_2322;
											BgL_posz00_2311 = BgL_g1219z00_2319;
											if (CBOOL(BGl_acceptzd2foldingzf3z21zzsaw_exprz00
													(BgL_bz00_100, BgL_insz00_102, BgL_treez00_2310)))
												{	/* SawMill/expr.scm 115 */
													BGl_za2countza2z00zzsaw_exprz00 =
														(1L + BGl_za2countza2z00zzsaw_exprz00);
													{	/* SawMill/expr.scm 117 */
														obj_t BgL_arg1842z00_2314;

														BgL_arg1842z00_2314 =
															(((BgL_rtl_insz00_bglt) COBJECT(
																	((BgL_rtl_insz00_bglt) BgL_treez00_2310)))->
															BgL_destz00);
														((((BgL_rtl_regz00_bglt)
																	COBJECT(((BgL_rtl_regz00_bglt)
																			BgL_arg1842z00_2314)))->
																BgL_onexprzf3zf3) = ((obj_t) BTRUE), BUNSPEC);
													}
												}
											else
												{	/* SawMill/expr.scm 115 */
													BFALSE;
												}
										}
										{	/* SawMill/expr.scm 123 */
											obj_t BgL_arg1846z00_2323;

											BgL_arg1846z00_2323 = CDR(((obj_t) BgL_lz00_2315));
											{
												obj_t BgL_lz00_4841;

												BgL_lz00_4841 = BgL_arg1846z00_2323;
												BgL_lz00_2315 = BgL_lz00_4841;
												goto BgL_zc3z04anonymousza31843ze3z87_2317;
											}
										}
									}
								else
									{	/* SawMill/expr.scm 124 */
										bool_t BgL_test2267z00_4842;

										{	/* SawMill/expr.scm 124 */
											obj_t BgL_arg1853z00_2330;

											BgL_arg1853z00_2330 = CAR(((obj_t) BgL_lz00_2315));
											BgL_test2267z00_4842 =
												CBOOL(BGl_xxz00zzsaw_exprz00(BgL_arg1853z00_2330,
													BgL_insz00_102, BgL_inszd2effectzd2_2293));
										}
										if (BgL_test2267z00_4842)
											{	/* SawMill/expr.scm 126 */
												obj_t BgL_arg1849z00_2326;

												BgL_arg1849z00_2326 = CDR(((obj_t) BgL_lz00_2315));
												{
													obj_t BgL_lz00_4849;

													BgL_lz00_4849 = BgL_arg1849z00_2326;
													BgL_lz00_2315 = BgL_lz00_4849;
													goto BgL_zc3z04anonymousza31843ze3z87_2317;
												}
											}
										else
											{	/* SawMill/expr.scm 127 */
												obj_t BgL_arg1850z00_2327;
												obj_t BgL_arg1851z00_2328;

												BgL_arg1850z00_2327 = CDR(((obj_t) BgL_lz00_2315));
												{	/* SawMill/expr.scm 127 */
													obj_t BgL_arg1852z00_2329;

													BgL_arg1852z00_2329 = CAR(((obj_t) BgL_lz00_2315));
													BgL_arg1851z00_2328 =
														MAKE_YOUNG_PAIR(BgL_arg1852z00_2329, BgL_rz00_2316);
												}
												{
													obj_t BgL_rz00_4856;
													obj_t BgL_lz00_4855;

													BgL_lz00_4855 = BgL_arg1850z00_2327;
													BgL_rz00_4856 = BgL_arg1851z00_2328;
													BgL_rz00_2316 = BgL_rz00_4856;
													BgL_lz00_2315 = BgL_lz00_4855;
													goto BgL_zc3z04anonymousza31843ze3z87_2317;
												}
											}
									}
							}
						BgL_dz00_2298 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_insz00_102)))->BgL_destz00);
						{	/* SawMill/expr.scm 130 */
							bool_t BgL_test2268z00_4859;

							if (CBOOL(BgL_dz00_2298))
								{	/* SawMill/expr.scm 131 */
									bool_t BgL_test2270z00_4862;

									{	/* SawMill/expr.scm 131 */
										obj_t BgL_arg1839z00_2309;

										{
											BgL_pregz00_bglt BgL_auxz00_4863;

											{
												obj_t BgL_auxz00_4864;

												{	/* SawMill/expr.scm 131 */
													BgL_objectz00_bglt BgL_tmpz00_4865;

													BgL_tmpz00_4865 =
														((BgL_objectz00_bglt)
														((BgL_rtl_regz00_bglt) BgL_dz00_2298));
													BgL_auxz00_4864 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4865);
												}
												BgL_auxz00_4863 = ((BgL_pregz00_bglt) BgL_auxz00_4864);
											}
											BgL_arg1839z00_2309 =
												(((BgL_pregz00_bglt) COBJECT(BgL_auxz00_4863))->
												BgL_statusz00);
										}
										BgL_test2270z00_4862 =
											(BgL_arg1839z00_2309 == CNST_TABLE_REF(18));
									}
									if (BgL_test2270z00_4862)
										{	/* SawMill/expr.scm 132 */
											bool_t BgL_test2271z00_4873;

											{	/* SawMill/expr.scm 132 */
												BgL_rtl_funz00_bglt BgL_arg1838z00_2308;

												BgL_arg1838z00_2308 =
													(((BgL_rtl_insz00_bglt) COBJECT(
															((BgL_rtl_insz00_bglt) BgL_insz00_102)))->
													BgL_funz00);
												{	/* SawMill/expr.scm 132 */
													obj_t BgL_classz00_3289;

													BgL_classz00_3289 = BGl_rtl_protectz00zzsaw_defsz00;
													{	/* SawMill/expr.scm 132 */
														BgL_objectz00_bglt BgL_arg1807z00_3291;

														{	/* SawMill/expr.scm 132 */
															obj_t BgL_tmpz00_4876;

															BgL_tmpz00_4876 =
																((obj_t)
																((BgL_objectz00_bglt) BgL_arg1838z00_2308));
															BgL_arg1807z00_3291 =
																(BgL_objectz00_bglt) (BgL_tmpz00_4876);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawMill/expr.scm 132 */
																long BgL_idxz00_3297;

																BgL_idxz00_3297 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3291);
																BgL_test2271z00_4873 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3297 + 2L)) ==
																	BgL_classz00_3289);
															}
														else
															{	/* SawMill/expr.scm 132 */
																bool_t BgL_res2112z00_3322;

																{	/* SawMill/expr.scm 132 */
																	obj_t BgL_oclassz00_3305;

																	{	/* SawMill/expr.scm 132 */
																		obj_t BgL_arg1815z00_3313;
																		long BgL_arg1816z00_3314;

																		BgL_arg1815z00_3313 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawMill/expr.scm 132 */
																			long BgL_arg1817z00_3315;

																			BgL_arg1817z00_3315 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3291);
																			BgL_arg1816z00_3314 =
																				(BgL_arg1817z00_3315 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3305 =
																			VECTOR_REF(BgL_arg1815z00_3313,
																			BgL_arg1816z00_3314);
																	}
																	{	/* SawMill/expr.scm 132 */
																		bool_t BgL__ortest_1115z00_3306;

																		BgL__ortest_1115z00_3306 =
																			(BgL_classz00_3289 == BgL_oclassz00_3305);
																		if (BgL__ortest_1115z00_3306)
																			{	/* SawMill/expr.scm 132 */
																				BgL_res2112z00_3322 =
																					BgL__ortest_1115z00_3306;
																			}
																		else
																			{	/* SawMill/expr.scm 132 */
																				long BgL_odepthz00_3307;

																				{	/* SawMill/expr.scm 132 */
																					obj_t BgL_arg1804z00_3308;

																					BgL_arg1804z00_3308 =
																						(BgL_oclassz00_3305);
																					BgL_odepthz00_3307 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3308);
																				}
																				if ((2L < BgL_odepthz00_3307))
																					{	/* SawMill/expr.scm 132 */
																						obj_t BgL_arg1802z00_3310;

																						{	/* SawMill/expr.scm 132 */
																							obj_t BgL_arg1803z00_3311;

																							BgL_arg1803z00_3311 =
																								(BgL_oclassz00_3305);
																							BgL_arg1802z00_3310 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3311, 2L);
																						}
																						BgL_res2112z00_3322 =
																							(BgL_arg1802z00_3310 ==
																							BgL_classz00_3289);
																					}
																				else
																					{	/* SawMill/expr.scm 132 */
																						BgL_res2112z00_3322 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2271z00_4873 = BgL_res2112z00_3322;
															}
													}
												}
											}
											if (BgL_test2271z00_4873)
												{	/* SawMill/expr.scm 132 */
													BgL_test2268z00_4859 = ((bool_t) 0);
												}
											else
												{	/* SawMill/expr.scm 132 */
													BgL_test2268z00_4859 = ((bool_t) 1);
												}
										}
									else
										{	/* SawMill/expr.scm 131 */
											BgL_test2268z00_4859 = ((bool_t) 0);
										}
								}
							else
								{	/* SawMill/expr.scm 130 */
									BgL_test2268z00_4859 = ((bool_t) 0);
								}
							if (BgL_test2268z00_4859)
								{	/* SawMill/expr.scm 130 */
									return MAKE_YOUNG_PAIR(BgL_insz00_102, BgL_rz00_2297);
								}
							else
								{	/* SawMill/expr.scm 130 */
									return BgL_rz00_2297;
								}
						}
					}
				}
			}
		}

	}



/* analyse */
	obj_t BGl_analysez00zzsaw_exprz00(BgL_backendz00_bglt BgL_bz00_103,
		obj_t BgL_movingz00_104, obj_t BgL_insz00_105)
	{
		{	/* SawMill/expr.scm 137 */
			{	/* SawMill/expr.scm 139 */
				obj_t BgL_inszd2effectzd2_2336;
				obj_t BgL_argsz00_2337;

				{	/* SawMill/expr.scm 139 */
					BgL_rtl_funz00_bglt BgL_arg1888z00_2383;

					BgL_arg1888z00_2383 =
						(((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_insz00_105)))->BgL_funz00);
					BgL_inszd2effectzd2_2336 =
						BGl_effectsz00zzsaw_exprz00(BgL_arg1888z00_2383);
				}
				BgL_argsz00_2337 =
					(((BgL_rtl_insz00_bglt) COBJECT(
							((BgL_rtl_insz00_bglt) BgL_insz00_105)))->BgL_argsz00);
				{
					obj_t BgL_lz00_2363;
					obj_t BgL_rz00_2364;
					obj_t BgL_treez00_2353;
					obj_t BgL_posz00_2354;

					{	/* SawMill/expr.scm 157 */
						obj_t BgL_rz00_2340;
						obj_t BgL_dz00_2341;

						BgL_lz00_2363 = BgL_movingz00_104;
						BgL_rz00_2364 = BNIL;
					BgL_zc3z04anonymousza31873ze3z87_2365:
						if (NULLP(BgL_lz00_2363))
							{	/* SawMill/expr.scm 147 */
								BgL_rz00_2340 = BgL_rz00_2364;
							}
						else
							{	/* SawMill/expr.scm 148 */
								obj_t BgL_g1225z00_2367;

								BgL_g1225z00_2367 =
									BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
									(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt)
													CAR(
														((obj_t) BgL_lz00_2363)))))->BgL_destz00),
									BgL_argsz00_2337);
								if (CBOOL(BgL_g1225z00_2367))
									{	/* SawMill/expr.scm 148 */
										{	/* SawMill/expr.scm 150 */
											obj_t BgL_arg1875z00_2370;

											BgL_arg1875z00_2370 = CAR(((obj_t) BgL_lz00_2363));
											BgL_treez00_2353 = BgL_arg1875z00_2370;
											BgL_posz00_2354 = BgL_g1225z00_2367;
											if (CBOOL(BGl_acceptzd2foldingzf3z21zzsaw_exprz00
													(BgL_bz00_103, BgL_insz00_105, BgL_treez00_2353)))
												{	/* SawMill/expr.scm 141 */
													{	/* SawMill/expr.scm 143 */
														obj_t BgL_tmpz00_4919;

														BgL_tmpz00_4919 = ((obj_t) BgL_posz00_2354);
														SET_CAR(BgL_tmpz00_4919, BgL_treez00_2353);
													}
													{	/* SawMill/expr.scm 144 */
														bool_t BgL_test2278z00_4922;

														{	/* SawMill/expr.scm 144 */
															obj_t BgL_classz00_3326;

															BgL_classz00_3326 = BGl_inlinedz00zzsaw_exprz00;
															if (BGL_OBJECTP(BgL_treez00_2353))
																{	/* SawMill/expr.scm 144 */
																	obj_t BgL_oclassz00_3328;

																	{	/* SawMill/expr.scm 144 */
																		obj_t BgL_arg1815z00_3330;
																		long BgL_arg1816z00_3331;

																		BgL_arg1815z00_3330 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawMill/expr.scm 144 */
																			long BgL_arg1817z00_3332;

																			BgL_arg1817z00_3332 =
																				BGL_OBJECT_CLASS_NUM(
																				((BgL_objectz00_bglt)
																					BgL_treez00_2353));
																			BgL_arg1816z00_3331 =
																				(BgL_arg1817z00_3332 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3328 =
																			VECTOR_REF(BgL_arg1815z00_3330,
																			BgL_arg1816z00_3331);
																	}
																	BgL_test2278z00_4922 =
																		(BgL_oclassz00_3328 == BgL_classz00_3326);
																}
															else
																{	/* SawMill/expr.scm 144 */
																	BgL_test2278z00_4922 = ((bool_t) 0);
																}
														}
														if (BgL_test2278z00_4922)
															{	/* SawMill/expr.scm 144 */
																BFALSE;
															}
														else
															{	/* SawMill/expr.scm 144 */
																{	/* SawMill/expr.scm 144 */
																	BgL_inlinedz00_bglt BgL_wide1223z00_2360;

																	BgL_wide1223z00_2360 =
																		((BgL_inlinedz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_inlinedz00_bgl))));
																	{	/* SawMill/expr.scm 144 */
																		obj_t BgL_auxz00_4936;
																		BgL_objectz00_bglt BgL_tmpz00_4932;

																		BgL_auxz00_4936 =
																			((obj_t) BgL_wide1223z00_2360);
																		BgL_tmpz00_4932 =
																			((BgL_objectz00_bglt)
																			((BgL_rtl_insz00_bglt)
																				((BgL_rtl_insz00_bglt)
																					BgL_treez00_2353)));
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4932,
																			BgL_auxz00_4936);
																	}
																	((BgL_objectz00_bglt)
																		((BgL_rtl_insz00_bglt)
																			((BgL_rtl_insz00_bglt)
																				BgL_treez00_2353)));
																	{	/* SawMill/expr.scm 144 */
																		long BgL_arg1872z00_2361;

																		BgL_arg1872z00_2361 =
																			BGL_CLASS_NUM
																			(BGl_inlinedz00zzsaw_exprz00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt) (
																					(BgL_rtl_insz00_bglt) (
																						(BgL_rtl_insz00_bglt)
																						BgL_treez00_2353))),
																			BgL_arg1872z00_2361);
																	}
																	((BgL_rtl_insz00_bglt)
																		((BgL_rtl_insz00_bglt)
																			((BgL_rtl_insz00_bglt)
																				BgL_treez00_2353)));
																}
																((obj_t)
																	((BgL_rtl_insz00_bglt)
																		((BgL_rtl_insz00_bglt) BgL_treez00_2353)));
												}}}
											else
												{	/* SawMill/expr.scm 141 */
													BFALSE;
												}
										}
										{	/* SawMill/expr.scm 151 */
											obj_t BgL_arg1876z00_2371;

											BgL_arg1876z00_2371 = CDR(((obj_t) BgL_lz00_2363));
											{
												obj_t BgL_lz00_4955;

												BgL_lz00_4955 = BgL_arg1876z00_2371;
												BgL_lz00_2363 = BgL_lz00_4955;
												goto BgL_zc3z04anonymousza31873ze3z87_2365;
											}
										}
									}
								else
									{	/* SawMill/expr.scm 152 */
										bool_t BgL_test2280z00_4956;

										{	/* SawMill/expr.scm 152 */
											obj_t BgL_arg1884z00_2378;

											BgL_arg1884z00_2378 = CAR(((obj_t) BgL_lz00_2363));
											BgL_test2280z00_4956 =
												CBOOL(BGl_xxz00zzsaw_exprz00(BgL_arg1884z00_2378,
													BgL_insz00_105, BgL_inszd2effectzd2_2336));
										}
										if (BgL_test2280z00_4956)
											{	/* SawMill/expr.scm 154 */
												obj_t BgL_arg1879z00_2374;

												BgL_arg1879z00_2374 = CDR(((obj_t) BgL_lz00_2363));
												{
													obj_t BgL_lz00_4963;

													BgL_lz00_4963 = BgL_arg1879z00_2374;
													BgL_lz00_2363 = BgL_lz00_4963;
													goto BgL_zc3z04anonymousza31873ze3z87_2365;
												}
											}
										else
											{	/* SawMill/expr.scm 155 */
												obj_t BgL_arg1880z00_2375;
												obj_t BgL_arg1882z00_2376;

												BgL_arg1880z00_2375 = CDR(((obj_t) BgL_lz00_2363));
												{	/* SawMill/expr.scm 155 */
													obj_t BgL_arg1883z00_2377;

													BgL_arg1883z00_2377 = CAR(((obj_t) BgL_lz00_2363));
													BgL_arg1882z00_2376 =
														MAKE_YOUNG_PAIR(BgL_arg1883z00_2377, BgL_rz00_2364);
												}
												{
													obj_t BgL_rz00_4970;
													obj_t BgL_lz00_4969;

													BgL_lz00_4969 = BgL_arg1880z00_2375;
													BgL_rz00_4970 = BgL_arg1882z00_2376;
													BgL_rz00_2364 = BgL_rz00_4970;
													BgL_lz00_2363 = BgL_lz00_4969;
													goto BgL_zc3z04anonymousza31873ze3z87_2365;
												}
											}
									}
							}
						BgL_dz00_2341 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_insz00_105)))->BgL_destz00);
						{	/* SawMill/expr.scm 159 */
							bool_t BgL_test2281z00_4973;

							if (CBOOL(BgL_dz00_2341))
								{	/* SawMill/expr.scm 160 */
									bool_t BgL_test2283z00_4976;

									{	/* SawMill/expr.scm 160 */
										obj_t BgL_arg1868z00_2352;

										{
											BgL_iregz00_bglt BgL_auxz00_4977;

											{
												obj_t BgL_auxz00_4978;

												{	/* SawMill/expr.scm 160 */
													BgL_objectz00_bglt BgL_tmpz00_4979;

													BgL_tmpz00_4979 =
														((BgL_objectz00_bglt)
														((BgL_rtl_regz00_bglt) BgL_dz00_2341));
													BgL_auxz00_4978 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4979);
												}
												BgL_auxz00_4977 = ((BgL_iregz00_bglt) BgL_auxz00_4978);
											}
											BgL_arg1868z00_2352 =
												(((BgL_iregz00_bglt) COBJECT(BgL_auxz00_4977))->
												BgL_statusz00);
										}
										BgL_test2283z00_4976 =
											(BgL_arg1868z00_2352 == CNST_TABLE_REF(18));
									}
									if (BgL_test2283z00_4976)
										{	/* SawMill/expr.scm 161 */
											bool_t BgL_test2284z00_4987;

											{	/* SawMill/expr.scm 161 */
												BgL_rtl_funz00_bglt BgL_arg1866z00_2351;

												BgL_arg1866z00_2351 =
													(((BgL_rtl_insz00_bglt) COBJECT(
															((BgL_rtl_insz00_bglt) BgL_insz00_105)))->
													BgL_funz00);
												{	/* SawMill/expr.scm 161 */
													obj_t BgL_classz00_3355;

													BgL_classz00_3355 = BGl_rtl_protectz00zzsaw_defsz00;
													{	/* SawMill/expr.scm 161 */
														BgL_objectz00_bglt BgL_arg1807z00_3357;

														{	/* SawMill/expr.scm 161 */
															obj_t BgL_tmpz00_4990;

															BgL_tmpz00_4990 =
																((obj_t)
																((BgL_objectz00_bglt) BgL_arg1866z00_2351));
															BgL_arg1807z00_3357 =
																(BgL_objectz00_bglt) (BgL_tmpz00_4990);
														}
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawMill/expr.scm 161 */
																long BgL_idxz00_3363;

																BgL_idxz00_3363 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3357);
																BgL_test2284z00_4987 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3363 + 2L)) ==
																	BgL_classz00_3355);
															}
														else
															{	/* SawMill/expr.scm 161 */
																bool_t BgL_res2113z00_3388;

																{	/* SawMill/expr.scm 161 */
																	obj_t BgL_oclassz00_3371;

																	{	/* SawMill/expr.scm 161 */
																		obj_t BgL_arg1815z00_3379;
																		long BgL_arg1816z00_3380;

																		BgL_arg1815z00_3379 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawMill/expr.scm 161 */
																			long BgL_arg1817z00_3381;

																			BgL_arg1817z00_3381 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3357);
																			BgL_arg1816z00_3380 =
																				(BgL_arg1817z00_3381 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3371 =
																			VECTOR_REF(BgL_arg1815z00_3379,
																			BgL_arg1816z00_3380);
																	}
																	{	/* SawMill/expr.scm 161 */
																		bool_t BgL__ortest_1115z00_3372;

																		BgL__ortest_1115z00_3372 =
																			(BgL_classz00_3355 == BgL_oclassz00_3371);
																		if (BgL__ortest_1115z00_3372)
																			{	/* SawMill/expr.scm 161 */
																				BgL_res2113z00_3388 =
																					BgL__ortest_1115z00_3372;
																			}
																		else
																			{	/* SawMill/expr.scm 161 */
																				long BgL_odepthz00_3373;

																				{	/* SawMill/expr.scm 161 */
																					obj_t BgL_arg1804z00_3374;

																					BgL_arg1804z00_3374 =
																						(BgL_oclassz00_3371);
																					BgL_odepthz00_3373 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3374);
																				}
																				if ((2L < BgL_odepthz00_3373))
																					{	/* SawMill/expr.scm 161 */
																						obj_t BgL_arg1802z00_3376;

																						{	/* SawMill/expr.scm 161 */
																							obj_t BgL_arg1803z00_3377;

																							BgL_arg1803z00_3377 =
																								(BgL_oclassz00_3371);
																							BgL_arg1802z00_3376 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3377, 2L);
																						}
																						BgL_res2113z00_3388 =
																							(BgL_arg1802z00_3376 ==
																							BgL_classz00_3355);
																					}
																				else
																					{	/* SawMill/expr.scm 161 */
																						BgL_res2113z00_3388 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2284z00_4987 = BgL_res2113z00_3388;
															}
													}
												}
											}
											if (BgL_test2284z00_4987)
												{	/* SawMill/expr.scm 161 */
													BgL_test2281z00_4973 = ((bool_t) 0);
												}
											else
												{	/* SawMill/expr.scm 161 */
													BgL_test2281z00_4973 = ((bool_t) 1);
												}
										}
									else
										{	/* SawMill/expr.scm 160 */
											BgL_test2281z00_4973 = ((bool_t) 0);
										}
								}
							else
								{	/* SawMill/expr.scm 159 */
									BgL_test2281z00_4973 = ((bool_t) 0);
								}
							if (BgL_test2281z00_4973)
								{	/* SawMill/expr.scm 159 */
									return MAKE_YOUNG_PAIR(BgL_insz00_105, BgL_rz00_2340);
								}
							else
								{	/* SawMill/expr.scm 159 */
									return BgL_rz00_2340;
								}
						}
					}
				}
			}
		}

	}



/* xx */
	obj_t BGl_xxz00zzsaw_exprz00(obj_t BgL_moverz00_112, obj_t BgL_insz00_113,
		obj_t BgL_inszd2effectszd2_114)
	{
		{	/* SawMill/expr.scm 177 */
			{	/* SawMill/expr.scm 178 */
				obj_t BgL__ortest_1228z00_2385;

				BgL__ortest_1228z00_2385 =
					BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
					(((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_insz00_113)))->BgL_destz00),
					(((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_moverz00_112)))->BgL_argsz00));
				if (CBOOL(BgL__ortest_1228z00_2385))
					{	/* SawMill/expr.scm 178 */
						return BgL__ortest_1228z00_2385;
					}
				else
					{	/* SawMill/expr.scm 179 */
						bool_t BgL__ortest_1229z00_2386;

						{	/* SawMill/expr.scm 179 */
							obj_t BgL_arg1901z00_2411;

							{	/* SawMill/expr.scm 179 */
								BgL_rtl_funz00_bglt BgL_arg1902z00_2412;

								BgL_arg1902z00_2412 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_moverz00_112)))->BgL_funz00);
								BgL_arg1901z00_2411 =
									BGl_effectsz00zzsaw_exprz00(BgL_arg1902z00_2412);
							}
							BgL__ortest_1229z00_2386 =
								BGl_matcheffectz00zzsaw_exprz00(BgL_arg1901z00_2411,
								BgL_inszd2effectszd2_114);
						}
						if (BgL__ortest_1229z00_2386)
							{	/* SawMill/expr.scm 179 */
								return BBOOL(BgL__ortest_1229z00_2386);
							}
						else
							{	/* SawMill/expr.scm 180 */
								obj_t BgL_g1533z00_2387;

								{	/* SawMill/expr.scm 181 */
									obj_t BgL_hook1529z00_2396;

									BgL_hook1529z00_2396 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
									{	/* SawMill/expr.scm 181 */
										obj_t BgL_g1530z00_2397;

										BgL_g1530z00_2397 =
											(((BgL_rtl_insz00_bglt) COBJECT(
													((BgL_rtl_insz00_bglt) BgL_moverz00_112)))->
											BgL_argsz00);
										{
											obj_t BgL_l1526z00_2399;
											obj_t BgL_h1527z00_2400;

											BgL_l1526z00_2399 = BgL_g1530z00_2397;
											BgL_h1527z00_2400 = BgL_hook1529z00_2396;
										BgL_zc3z04anonymousza31892ze3z87_2401:
											if (NULLP(BgL_l1526z00_2399))
												{	/* SawMill/expr.scm 181 */
													BgL_g1533z00_2387 = CDR(BgL_hook1529z00_2396);
												}
											else
												{	/* SawMill/expr.scm 181 */
													bool_t BgL_test2291z00_5033;

													{	/* SawMill/expr.scm 181 */
														obj_t BgL_arg1899z00_2409;

														BgL_arg1899z00_2409 =
															CAR(((obj_t) BgL_l1526z00_2399));
														{	/* SawMill/expr.scm 181 */
															obj_t BgL_classz00_3396;

															BgL_classz00_3396 = BGl_rtl_insz00zzsaw_defsz00;
															if (BGL_OBJECTP(BgL_arg1899z00_2409))
																{	/* SawMill/expr.scm 181 */
																	BgL_objectz00_bglt BgL_arg1807z00_3398;

																	BgL_arg1807z00_3398 =
																		(BgL_objectz00_bglt) (BgL_arg1899z00_2409);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* SawMill/expr.scm 181 */
																			long BgL_idxz00_3404;

																			BgL_idxz00_3404 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_3398);
																			BgL_test2291z00_5033 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_3404 + 1L)) ==
																				BgL_classz00_3396);
																		}
																	else
																		{	/* SawMill/expr.scm 181 */
																			bool_t BgL_res2114z00_3429;

																			{	/* SawMill/expr.scm 181 */
																				obj_t BgL_oclassz00_3412;

																				{	/* SawMill/expr.scm 181 */
																					obj_t BgL_arg1815z00_3420;
																					long BgL_arg1816z00_3421;

																					BgL_arg1815z00_3420 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* SawMill/expr.scm 181 */
																						long BgL_arg1817z00_3422;

																						BgL_arg1817z00_3422 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_3398);
																						BgL_arg1816z00_3421 =
																							(BgL_arg1817z00_3422 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_3412 =
																						VECTOR_REF(BgL_arg1815z00_3420,
																						BgL_arg1816z00_3421);
																				}
																				{	/* SawMill/expr.scm 181 */
																					bool_t BgL__ortest_1115z00_3413;

																					BgL__ortest_1115z00_3413 =
																						(BgL_classz00_3396 ==
																						BgL_oclassz00_3412);
																					if (BgL__ortest_1115z00_3413)
																						{	/* SawMill/expr.scm 181 */
																							BgL_res2114z00_3429 =
																								BgL__ortest_1115z00_3413;
																						}
																					else
																						{	/* SawMill/expr.scm 181 */
																							long BgL_odepthz00_3414;

																							{	/* SawMill/expr.scm 181 */
																								obj_t BgL_arg1804z00_3415;

																								BgL_arg1804z00_3415 =
																									(BgL_oclassz00_3412);
																								BgL_odepthz00_3414 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_3415);
																							}
																							if ((1L < BgL_odepthz00_3414))
																								{	/* SawMill/expr.scm 181 */
																									obj_t BgL_arg1802z00_3417;

																									{	/* SawMill/expr.scm 181 */
																										obj_t BgL_arg1803z00_3418;

																										BgL_arg1803z00_3418 =
																											(BgL_oclassz00_3412);
																										BgL_arg1802z00_3417 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_3418, 1L);
																									}
																									BgL_res2114z00_3429 =
																										(BgL_arg1802z00_3417 ==
																										BgL_classz00_3396);
																								}
																							else
																								{	/* SawMill/expr.scm 181 */
																									BgL_res2114z00_3429 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2291z00_5033 =
																				BgL_res2114z00_3429;
																		}
																}
															else
																{	/* SawMill/expr.scm 181 */
																	BgL_test2291z00_5033 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test2291z00_5033)
														{	/* SawMill/expr.scm 181 */
															obj_t BgL_nh1528z00_2405;

															{	/* SawMill/expr.scm 181 */
																obj_t BgL_arg1897z00_2407;

																BgL_arg1897z00_2407 =
																	CAR(((obj_t) BgL_l1526z00_2399));
																BgL_nh1528z00_2405 =
																	MAKE_YOUNG_PAIR(BgL_arg1897z00_2407, BNIL);
															}
															SET_CDR(BgL_h1527z00_2400, BgL_nh1528z00_2405);
															{	/* SawMill/expr.scm 181 */
																obj_t BgL_arg1896z00_2406;

																BgL_arg1896z00_2406 =
																	CDR(((obj_t) BgL_l1526z00_2399));
																{
																	obj_t BgL_h1527z00_5065;
																	obj_t BgL_l1526z00_5064;

																	BgL_l1526z00_5064 = BgL_arg1896z00_2406;
																	BgL_h1527z00_5065 = BgL_nh1528z00_2405;
																	BgL_h1527z00_2400 = BgL_h1527z00_5065;
																	BgL_l1526z00_2399 = BgL_l1526z00_5064;
																	goto BgL_zc3z04anonymousza31892ze3z87_2401;
																}
															}
														}
													else
														{	/* SawMill/expr.scm 181 */
															obj_t BgL_arg1898z00_2408;

															BgL_arg1898z00_2408 =
																CDR(((obj_t) BgL_l1526z00_2399));
															{
																obj_t BgL_l1526z00_5068;

																BgL_l1526z00_5068 = BgL_arg1898z00_2408;
																BgL_l1526z00_2399 = BgL_l1526z00_5068;
																goto BgL_zc3z04anonymousza31892ze3z87_2401;
															}
														}
												}
										}
									}
								}
								{
									obj_t BgL_l1531z00_2389;

									BgL_l1531z00_2389 = BgL_g1533z00_2387;
								BgL_zc3z04anonymousza31889ze3z87_2390:
									if (NULLP(BgL_l1531z00_2389))
										{	/* SawMill/expr.scm 181 */
											return BFALSE;
										}
									else
										{	/* SawMill/expr.scm 180 */
											obj_t BgL__ortest_1534z00_2392;

											{	/* SawMill/expr.scm 180 */
												obj_t BgL_subz00_2394;

												BgL_subz00_2394 = CAR(((obj_t) BgL_l1531z00_2389));
												BgL__ortest_1534z00_2392 =
													BGl_xxz00zzsaw_exprz00(BgL_subz00_2394,
													BgL_insz00_113, BgL_inszd2effectszd2_114);
											}
											if (CBOOL(BgL__ortest_1534z00_2392))
												{	/* SawMill/expr.scm 181 */
													return BgL__ortest_1534z00_2392;
												}
											else
												{	/* SawMill/expr.scm 181 */
													obj_t BgL_arg1891z00_2393;

													BgL_arg1891z00_2393 =
														CDR(((obj_t) BgL_l1531z00_2389));
													{
														obj_t BgL_l1531z00_5078;

														BgL_l1531z00_5078 = BgL_arg1891z00_2393;
														BgL_l1531z00_2389 = BgL_l1531z00_5078;
														goto BgL_zc3z04anonymousza31889ze3z87_2390;
													}
												}
										}
								}
							}
					}
			}
		}

	}



/* matcheffect */
	bool_t BGl_matcheffectz00zzsaw_exprz00(obj_t BgL_f1z00_115,
		obj_t BgL_f2z00_116)
	{
		{	/* SawMill/expr.scm 199 */
			{	/* SawMill/expr.scm 210 */
				obj_t BgL_w1z00_2417;
				obj_t BgL_w2z00_2418;

				BgL_w1z00_2417 =
					(((BgL_feffectz00_bglt) COBJECT(
							((BgL_feffectz00_bglt) BgL_f1z00_115)))->BgL_writez00);
				BgL_w2z00_2418 =
					(((BgL_feffectz00_bglt) COBJECT(
							((BgL_feffectz00_bglt) BgL_f2z00_116)))->BgL_writez00);
				{	/* SawMill/expr.scm 211 */
					bool_t BgL__ortest_1230z00_2419;

					BgL__ortest_1230z00_2419 =
						BGl_intersectionzf3ze70z14zzsaw_exprz00(BgL_w1z00_2417,
						BgL_w2z00_2418);
					if (BgL__ortest_1230z00_2419)
						{	/* SawMill/expr.scm 211 */
							return BgL__ortest_1230z00_2419;
						}
					else
						{	/* SawMill/expr.scm 212 */
							bool_t BgL__ortest_1231z00_2420;

							BgL__ortest_1231z00_2420 =
								BGl_intersectionzf3ze70z14zzsaw_exprz00(
								(((BgL_feffectz00_bglt) COBJECT(
											((BgL_feffectz00_bglt) BgL_f1z00_115)))->BgL_readz00),
								BgL_w2z00_2418);
							if (BgL__ortest_1231z00_2420)
								{	/* SawMill/expr.scm 212 */
									return BgL__ortest_1231z00_2420;
								}
							else
								{	/* SawMill/expr.scm 212 */
									BGL_TAIL return
										BGl_intersectionzf3ze70z14zzsaw_exprz00(
										(((BgL_feffectz00_bglt) COBJECT(
													((BgL_feffectz00_bglt) BgL_f2z00_116)))->BgL_readz00),
										BgL_w1z00_2417);
								}
						}
				}
			}
		}

	}



/* intersection?~0 */
	bool_t BGl_intersectionzf3ze70z14zzsaw_exprz00(obj_t BgL_l1z00_2431,
		obj_t BgL_l2z00_2432)
	{
		{	/* SawMill/expr.scm 209 */
			{
				obj_t BgL_l1z00_2423;
				obj_t BgL_l2z00_2424;

				if (NULLP(BgL_l1z00_2431))
					{	/* SawMill/expr.scm 205 */
						return ((bool_t) 0);
					}
				else
					{	/* SawMill/expr.scm 205 */
						if (NULLP(BgL_l2z00_2432))
							{	/* SawMill/expr.scm 206 */
								return ((bool_t) 0);
							}
						else
							{	/* SawMill/expr.scm 206 */
								if ((BgL_l1z00_2431 == CNST_TABLE_REF(4)))
									{	/* SawMill/expr.scm 207 */
										return ((bool_t) 1);
									}
								else
									{	/* SawMill/expr.scm 207 */
										if ((BgL_l2z00_2432 == CNST_TABLE_REF(4)))
											{	/* SawMill/expr.scm 208 */
												return ((bool_t) 1);
											}
										else
											{	/* SawMill/expr.scm 208 */
												BgL_l1z00_2423 = BgL_l1z00_2431;
												BgL_l2z00_2424 = BgL_l2z00_2432;
											BgL_zc3z04anonymousza31911ze3z87_2425:
												if (NULLP(BgL_l1z00_2423))
													{	/* SawMill/expr.scm 201 */
														return ((bool_t) 0);
													}
												else
													{	/* SawMill/expr.scm 201 */
														if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																(CAR(((obj_t) BgL_l1z00_2423)),
																	BgL_l2z00_2424)))
															{	/* SawMill/expr.scm 202 */
																return ((bool_t) 1);
															}
														else
															{
																obj_t BgL_l1z00_5109;

																BgL_l1z00_5109 = CDR(((obj_t) BgL_l1z00_2423));
																BgL_l1z00_2423 = BgL_l1z00_5109;
																goto BgL_zc3z04anonymousza31911ze3z87_2425;
															}
													}
											}
									}
							}
					}
			}
		}

	}



/* manu-effect */
	obj_t BGl_manuzd2effectzd2zzsaw_exprz00(BgL_globalz00_bglt BgL_varz00_133)
	{
		{	/* SawMill/expr.scm 252 */
			{	/* SawMill/expr.scm 253 */
				BgL_valuez00_bglt BgL_funz00_2440;

				BgL_funz00_2440 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_133)))->BgL_valuez00);
				{	/* SawMill/expr.scm 254 */
					bool_t BgL_test2306z00_5114;

					{	/* SawMill/expr.scm 254 */
						obj_t BgL_classz00_3448;

						BgL_classz00_3448 = BGl_funz00zzast_varz00;
						{	/* SawMill/expr.scm 254 */
							BgL_objectz00_bglt BgL_arg1807z00_3450;

							{	/* SawMill/expr.scm 254 */
								obj_t BgL_tmpz00_5115;

								BgL_tmpz00_5115 =
									((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2440));
								BgL_arg1807z00_3450 = (BgL_objectz00_bglt) (BgL_tmpz00_5115);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* SawMill/expr.scm 254 */
									long BgL_idxz00_3456;

									BgL_idxz00_3456 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3450);
									BgL_test2306z00_5114 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3456 + 2L)) == BgL_classz00_3448);
								}
							else
								{	/* SawMill/expr.scm 254 */
									bool_t BgL_res2115z00_3481;

									{	/* SawMill/expr.scm 254 */
										obj_t BgL_oclassz00_3464;

										{	/* SawMill/expr.scm 254 */
											obj_t BgL_arg1815z00_3472;
											long BgL_arg1816z00_3473;

											BgL_arg1815z00_3472 = (BGl_za2classesza2z00zz__objectz00);
											{	/* SawMill/expr.scm 254 */
												long BgL_arg1817z00_3474;

												BgL_arg1817z00_3474 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3450);
												BgL_arg1816z00_3473 =
													(BgL_arg1817z00_3474 - OBJECT_TYPE);
											}
											BgL_oclassz00_3464 =
												VECTOR_REF(BgL_arg1815z00_3472, BgL_arg1816z00_3473);
										}
										{	/* SawMill/expr.scm 254 */
											bool_t BgL__ortest_1115z00_3465;

											BgL__ortest_1115z00_3465 =
												(BgL_classz00_3448 == BgL_oclassz00_3464);
											if (BgL__ortest_1115z00_3465)
												{	/* SawMill/expr.scm 254 */
													BgL_res2115z00_3481 = BgL__ortest_1115z00_3465;
												}
											else
												{	/* SawMill/expr.scm 254 */
													long BgL_odepthz00_3466;

													{	/* SawMill/expr.scm 254 */
														obj_t BgL_arg1804z00_3467;

														BgL_arg1804z00_3467 = (BgL_oclassz00_3464);
														BgL_odepthz00_3466 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3467);
													}
													if ((2L < BgL_odepthz00_3466))
														{	/* SawMill/expr.scm 254 */
															obj_t BgL_arg1802z00_3469;

															{	/* SawMill/expr.scm 254 */
																obj_t BgL_arg1803z00_3470;

																BgL_arg1803z00_3470 = (BgL_oclassz00_3464);
																BgL_arg1802z00_3469 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3470,
																	2L);
															}
															BgL_res2115z00_3481 =
																(BgL_arg1802z00_3469 == BgL_classz00_3448);
														}
													else
														{	/* SawMill/expr.scm 254 */
															BgL_res2115z00_3481 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2306z00_5114 = BgL_res2115z00_3481;
								}
						}
					}
					if (BgL_test2306z00_5114)
						{	/* SawMill/expr.scm 255 */
							obj_t BgL_effectz00_2442;

							BgL_effectz00_2442 =
								(((BgL_funz00_bglt) COBJECT(
										((BgL_funz00_bglt) BgL_funz00_2440)))->BgL_effectz00);
							{	/* SawMill/expr.scm 256 */
								bool_t BgL_test2310z00_5140;

								{	/* SawMill/expr.scm 256 */
									obj_t BgL_classz00_3483;

									BgL_classz00_3483 = BGl_feffectz00zzast_varz00;
									if (BGL_OBJECTP(BgL_effectz00_2442))
										{	/* SawMill/expr.scm 256 */
											BgL_objectz00_bglt BgL_arg1807z00_3485;

											BgL_arg1807z00_3485 =
												(BgL_objectz00_bglt) (BgL_effectz00_2442);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* SawMill/expr.scm 256 */
													long BgL_idxz00_3491;

													BgL_idxz00_3491 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3485);
													BgL_test2310z00_5140 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_3491 + 1L)) == BgL_classz00_3483);
												}
											else
												{	/* SawMill/expr.scm 256 */
													bool_t BgL_res2116z00_3516;

													{	/* SawMill/expr.scm 256 */
														obj_t BgL_oclassz00_3499;

														{	/* SawMill/expr.scm 256 */
															obj_t BgL_arg1815z00_3507;
															long BgL_arg1816z00_3508;

															BgL_arg1815z00_3507 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* SawMill/expr.scm 256 */
																long BgL_arg1817z00_3509;

																BgL_arg1817z00_3509 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3485);
																BgL_arg1816z00_3508 =
																	(BgL_arg1817z00_3509 - OBJECT_TYPE);
															}
															BgL_oclassz00_3499 =
																VECTOR_REF(BgL_arg1815z00_3507,
																BgL_arg1816z00_3508);
														}
														{	/* SawMill/expr.scm 256 */
															bool_t BgL__ortest_1115z00_3500;

															BgL__ortest_1115z00_3500 =
																(BgL_classz00_3483 == BgL_oclassz00_3499);
															if (BgL__ortest_1115z00_3500)
																{	/* SawMill/expr.scm 256 */
																	BgL_res2116z00_3516 =
																		BgL__ortest_1115z00_3500;
																}
															else
																{	/* SawMill/expr.scm 256 */
																	long BgL_odepthz00_3501;

																	{	/* SawMill/expr.scm 256 */
																		obj_t BgL_arg1804z00_3502;

																		BgL_arg1804z00_3502 = (BgL_oclassz00_3499);
																		BgL_odepthz00_3501 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_3502);
																	}
																	if ((1L < BgL_odepthz00_3501))
																		{	/* SawMill/expr.scm 256 */
																			obj_t BgL_arg1802z00_3504;

																			{	/* SawMill/expr.scm 256 */
																				obj_t BgL_arg1803z00_3505;

																				BgL_arg1803z00_3505 =
																					(BgL_oclassz00_3499);
																				BgL_arg1802z00_3504 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_3505, 1L);
																			}
																			BgL_res2116z00_3516 =
																				(BgL_arg1802z00_3504 ==
																				BgL_classz00_3483);
																		}
																	else
																		{	/* SawMill/expr.scm 256 */
																			BgL_res2116z00_3516 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2310z00_5140 = BgL_res2116z00_3516;
												}
										}
									else
										{	/* SawMill/expr.scm 256 */
											BgL_test2310z00_5140 = ((bool_t) 0);
										}
								}
								if (BgL_test2310z00_5140)
									{	/* SawMill/expr.scm 256 */
										return BgL_effectz00_2442;
									}
								else
									{	/* SawMill/expr.scm 256 */
										CNST_TABLE_REF(20);
										return ((obj_t) BGl_za2fullza2z00zzsaw_exprz00);
									}
							}
						}
					else
						{	/* SawMill/expr.scm 254 */
							return ((obj_t) BGl_za2fullza2z00zzsaw_exprz00);
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_exprz00(void)
	{
		{	/* SawMill/expr.scm 1 */
			{	/* SawMill/expr.scm 12 */
				obj_t BgL_arg1931z00_2448;
				obj_t BgL_arg1932z00_2449;

				{	/* SawMill/expr.scm 12 */
					obj_t BgL_v1535z00_2479;

					BgL_v1535z00_2479 = create_vector(2L);
					{	/* SawMill/expr.scm 12 */
						obj_t BgL_arg1943z00_2480;

						BgL_arg1943z00_2480 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(21),
							BGl_proc2134z00zzsaw_exprz00, BGl_proc2133z00zzsaw_exprz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(22));
						VECTOR_SET(BgL_v1535z00_2479, 0L, BgL_arg1943z00_2480);
					}
					{	/* SawMill/expr.scm 12 */
						obj_t BgL_arg1948z00_2490;

						BgL_arg1948z00_2490 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(23),
							BGl_proc2136z00zzsaw_exprz00, BGl_proc2135z00zzsaw_exprz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(22));
						VECTOR_SET(BgL_v1535z00_2479, 1L, BgL_arg1948z00_2490);
					}
					BgL_arg1931z00_2448 = BgL_v1535z00_2479;
				}
				{	/* SawMill/expr.scm 12 */
					obj_t BgL_v1536z00_2500;

					BgL_v1536z00_2500 = create_vector(0L);
					BgL_arg1932z00_2449 = BgL_v1536z00_2500;
				}
				BGl_iregz00zzsaw_exprz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(24),
					CNST_TABLE_REF(25), BGl_rtl_regz00zzsaw_defsz00, 43135L,
					BGl_proc2140z00zzsaw_exprz00, BGl_proc2139z00zzsaw_exprz00, BFALSE,
					BGl_proc2138z00zzsaw_exprz00, BGl_proc2137z00zzsaw_exprz00,
					BgL_arg1931z00_2448, BgL_arg1932z00_2449);
			}
			{	/* SawMill/expr.scm 13 */
				obj_t BgL_arg1957z00_2509;
				obj_t BgL_arg1958z00_2510;

				{	/* SawMill/expr.scm 13 */
					obj_t BgL_v1537z00_2540;

					BgL_v1537z00_2540 = create_vector(2L);
					{	/* SawMill/expr.scm 13 */
						obj_t BgL_arg1969z00_2541;

						BgL_arg1969z00_2541 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(21),
							BGl_proc2142z00zzsaw_exprz00, BGl_proc2141z00zzsaw_exprz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(22));
						VECTOR_SET(BgL_v1537z00_2540, 0L, BgL_arg1969z00_2541);
					}
					{	/* SawMill/expr.scm 13 */
						obj_t BgL_arg1974z00_2551;

						BgL_arg1974z00_2551 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(23),
							BGl_proc2144z00zzsaw_exprz00, BGl_proc2143z00zzsaw_exprz00,
							((bool_t) 0), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(22));
						VECTOR_SET(BgL_v1537z00_2540, 1L, BgL_arg1974z00_2551);
					}
					BgL_arg1957z00_2509 = BgL_v1537z00_2540;
				}
				{	/* SawMill/expr.scm 13 */
					obj_t BgL_v1538z00_2561;

					BgL_v1538z00_2561 = create_vector(0L);
					BgL_arg1958z00_2510 = BgL_v1538z00_2561;
				}
				BGl_pregz00zzsaw_exprz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(26),
					CNST_TABLE_REF(25), BGl_rtl_regz00zzsaw_defsz00, 27384L,
					BGl_proc2148z00zzsaw_exprz00, BGl_proc2147z00zzsaw_exprz00, BFALSE,
					BGl_proc2146z00zzsaw_exprz00, BGl_proc2145z00zzsaw_exprz00,
					BgL_arg1957z00_2509, BgL_arg1958z00_2510);
			}
			{	/* SawMill/expr.scm 14 */
				obj_t BgL_arg1983z00_2570;
				obj_t BgL_arg1984z00_2571;

				{	/* SawMill/expr.scm 14 */
					obj_t BgL_v1539z00_2597;

					BgL_v1539z00_2597 = create_vector(0L);
					BgL_arg1983z00_2570 = BgL_v1539z00_2597;
				}
				{	/* SawMill/expr.scm 14 */
					obj_t BgL_v1540z00_2598;

					BgL_v1540z00_2598 = create_vector(0L);
					BgL_arg1984z00_2571 = BgL_v1540z00_2598;
				}
				return (BGl_inlinedz00zzsaw_exprz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(27),
						CNST_TABLE_REF(25), BGl_rtl_insz00zzsaw_defsz00, 5795L,
						BGl_proc2152z00zzsaw_exprz00, BGl_proc2151z00zzsaw_exprz00, BFALSE,
						BGl_proc2150z00zzsaw_exprz00, BGl_proc2149z00zzsaw_exprz00,
						BgL_arg1983z00_2570, BgL_arg1984z00_2571), BUNSPEC);
			}
		}

	}



/* &lambda1991 */
	BgL_rtl_insz00_bglt BGl_z62lambda1991z62zzsaw_exprz00(obj_t BgL_envz00_3886,
		obj_t BgL_o1190z00_3887)
	{
		{	/* SawMill/expr.scm 14 */
			{	/* SawMill/expr.scm 14 */
				long BgL_arg1992z00_4070;

				{	/* SawMill/expr.scm 14 */
					obj_t BgL_arg1993z00_4071;

					{	/* SawMill/expr.scm 14 */
						obj_t BgL_arg1994z00_4072;

						{	/* SawMill/expr.scm 14 */
							obj_t BgL_arg1815z00_4073;
							long BgL_arg1816z00_4074;

							BgL_arg1815z00_4073 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/expr.scm 14 */
								long BgL_arg1817z00_4075;

								BgL_arg1817z00_4075 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_rtl_insz00_bglt) BgL_o1190z00_3887)));
								BgL_arg1816z00_4074 = (BgL_arg1817z00_4075 - OBJECT_TYPE);
							}
							BgL_arg1994z00_4072 =
								VECTOR_REF(BgL_arg1815z00_4073, BgL_arg1816z00_4074);
						}
						BgL_arg1993z00_4071 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1994z00_4072);
					}
					{	/* SawMill/expr.scm 14 */
						obj_t BgL_tmpz00_5204;

						BgL_tmpz00_5204 = ((obj_t) BgL_arg1993z00_4071);
						BgL_arg1992z00_4070 = BGL_CLASS_NUM(BgL_tmpz00_5204);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_rtl_insz00_bglt) BgL_o1190z00_3887)), BgL_arg1992z00_4070);
			}
			{	/* SawMill/expr.scm 14 */
				BgL_objectz00_bglt BgL_tmpz00_5210;

				BgL_tmpz00_5210 =
					((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1190z00_3887));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5210, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1190z00_3887));
			return ((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1190z00_3887));
		}

	}



/* &<@anonymous:1990> */
	obj_t BGl_z62zc3z04anonymousza31990ze3ze5zzsaw_exprz00(obj_t BgL_envz00_3888,
		obj_t BgL_new1189z00_3889)
	{
		{	/* SawMill/expr.scm 14 */
			{
				BgL_rtl_insz00_bglt BgL_auxz00_5218;

				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt)
									((BgL_rtl_insz00_bglt) BgL_new1189z00_3889))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_insz00_bglt)
							COBJECT(((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt)
										BgL_new1189z00_3889))))->BgL_z52spillz52) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_rtl_insz00_bglt)
							COBJECT(((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt)
										BgL_new1189z00_3889))))->BgL_destz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_rtl_funz00_bglt BgL_auxz00_5228;

					{	/* SawMill/expr.scm 14 */
						obj_t BgL_classz00_4077;

						BgL_classz00_4077 = BGl_rtl_funz00zzsaw_defsz00;
						{	/* SawMill/expr.scm 14 */
							obj_t BgL__ortest_1117z00_4078;

							BgL__ortest_1117z00_4078 = BGL_CLASS_NIL(BgL_classz00_4077);
							if (CBOOL(BgL__ortest_1117z00_4078))
								{	/* SawMill/expr.scm 14 */
									BgL_auxz00_5228 =
										((BgL_rtl_funz00_bglt) BgL__ortest_1117z00_4078);
								}
							else
								{	/* SawMill/expr.scm 14 */
									BgL_auxz00_5228 =
										((BgL_rtl_funz00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4077));
								}
						}
					}
					((((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt)
										((BgL_rtl_insz00_bglt) BgL_new1189z00_3889))))->
							BgL_funz00) = ((BgL_rtl_funz00_bglt) BgL_auxz00_5228), BUNSPEC);
				}
				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt)
									((BgL_rtl_insz00_bglt) BgL_new1189z00_3889))))->BgL_argsz00) =
					((obj_t) BNIL), BUNSPEC);
				BgL_auxz00_5218 = ((BgL_rtl_insz00_bglt) BgL_new1189z00_3889);
				return ((obj_t) BgL_auxz00_5218);
			}
		}

	}



/* &lambda1988 */
	BgL_rtl_insz00_bglt BGl_z62lambda1988z62zzsaw_exprz00(obj_t BgL_envz00_3890,
		obj_t BgL_o1185z00_3891)
	{
		{	/* SawMill/expr.scm 14 */
			{	/* SawMill/expr.scm 14 */
				BgL_inlinedz00_bglt BgL_wide1187z00_4080;

				BgL_wide1187z00_4080 =
					((BgL_inlinedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_inlinedz00_bgl))));
				{	/* SawMill/expr.scm 14 */
					obj_t BgL_auxz00_5248;
					BgL_objectz00_bglt BgL_tmpz00_5244;

					BgL_auxz00_5248 = ((obj_t) BgL_wide1187z00_4080);
					BgL_tmpz00_5244 =
						((BgL_objectz00_bglt)
						((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1185z00_3891)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5244, BgL_auxz00_5248);
				}
				((BgL_objectz00_bglt)
					((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1185z00_3891)));
				{	/* SawMill/expr.scm 14 */
					long BgL_arg1989z00_4081;

					BgL_arg1989z00_4081 = BGL_CLASS_NUM(BGl_inlinedz00zzsaw_exprz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_rtl_insz00_bglt)
								((BgL_rtl_insz00_bglt) BgL_o1185z00_3891))),
						BgL_arg1989z00_4081);
				}
				return
					((BgL_rtl_insz00_bglt)
					((BgL_rtl_insz00_bglt) ((BgL_rtl_insz00_bglt) BgL_o1185z00_3891)));
			}
		}

	}



/* &lambda1985 */
	BgL_rtl_insz00_bglt BGl_z62lambda1985z62zzsaw_exprz00(obj_t BgL_envz00_3892,
		obj_t BgL_loc1180z00_3893, obj_t BgL_z52spill1181z52_3894,
		obj_t BgL_dest1182z00_3895, obj_t BgL_fun1183z00_3896,
		obj_t BgL_args1184z00_3897)
	{
		{	/* SawMill/expr.scm 14 */
			{	/* SawMill/expr.scm 14 */
				BgL_rtl_insz00_bglt BgL_new1290z00_4085;

				{	/* SawMill/expr.scm 14 */
					BgL_rtl_insz00_bglt BgL_tmp1288z00_4086;
					BgL_inlinedz00_bglt BgL_wide1289z00_4087;

					{
						BgL_rtl_insz00_bglt BgL_auxz00_5262;

						{	/* SawMill/expr.scm 14 */
							BgL_rtl_insz00_bglt BgL_new1287z00_4088;

							BgL_new1287z00_4088 =
								((BgL_rtl_insz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_insz00_bgl))));
							{	/* SawMill/expr.scm 14 */
								long BgL_arg1987z00_4089;

								BgL_arg1987z00_4089 =
									BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1287z00_4088),
									BgL_arg1987z00_4089);
							}
							{	/* SawMill/expr.scm 14 */
								BgL_objectz00_bglt BgL_tmpz00_5267;

								BgL_tmpz00_5267 = ((BgL_objectz00_bglt) BgL_new1287z00_4088);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5267, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1287z00_4088);
							BgL_auxz00_5262 = BgL_new1287z00_4088;
						}
						BgL_tmp1288z00_4086 = ((BgL_rtl_insz00_bglt) BgL_auxz00_5262);
					}
					BgL_wide1289z00_4087 =
						((BgL_inlinedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_inlinedz00_bgl))));
					{	/* SawMill/expr.scm 14 */
						obj_t BgL_auxz00_5275;
						BgL_objectz00_bglt BgL_tmpz00_5273;

						BgL_auxz00_5275 = ((obj_t) BgL_wide1289z00_4087);
						BgL_tmpz00_5273 = ((BgL_objectz00_bglt) BgL_tmp1288z00_4086);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5273, BgL_auxz00_5275);
					}
					((BgL_objectz00_bglt) BgL_tmp1288z00_4086);
					{	/* SawMill/expr.scm 14 */
						long BgL_arg1986z00_4090;

						BgL_arg1986z00_4090 = BGL_CLASS_NUM(BGl_inlinedz00zzsaw_exprz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1288z00_4086), BgL_arg1986z00_4090);
					}
					BgL_new1290z00_4085 = ((BgL_rtl_insz00_bglt) BgL_tmp1288z00_4086);
				}
				((((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_new1290z00_4085)))->BgL_locz00) =
					((obj_t) BgL_loc1180z00_3893), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1290z00_4085)))->BgL_z52spillz52) =
					((obj_t) ((obj_t) BgL_z52spill1181z52_3894)), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1290z00_4085)))->BgL_destz00) =
					((obj_t) BgL_dest1182z00_3895), BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1290z00_4085)))->BgL_funz00) =
					((BgL_rtl_funz00_bglt) ((BgL_rtl_funz00_bglt) BgL_fun1183z00_3896)),
					BUNSPEC);
				((((BgL_rtl_insz00_bglt) COBJECT(((BgL_rtl_insz00_bglt)
									BgL_new1290z00_4085)))->BgL_argsz00) =
					((obj_t) ((obj_t) BgL_args1184z00_3897)), BUNSPEC);
				return BgL_new1290z00_4085;
			}
		}

	}



/* &lambda1965 */
	BgL_rtl_regz00_bglt BGl_z62lambda1965z62zzsaw_exprz00(obj_t BgL_envz00_3898,
		obj_t BgL_o1178z00_3899)
	{
		{	/* SawMill/expr.scm 13 */
			{	/* SawMill/expr.scm 13 */
				long BgL_arg1966z00_4092;

				{	/* SawMill/expr.scm 13 */
					obj_t BgL_arg1967z00_4093;

					{	/* SawMill/expr.scm 13 */
						obj_t BgL_arg1968z00_4094;

						{	/* SawMill/expr.scm 13 */
							obj_t BgL_arg1815z00_4095;
							long BgL_arg1816z00_4096;

							BgL_arg1815z00_4095 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/expr.scm 13 */
								long BgL_arg1817z00_4097;

								BgL_arg1817z00_4097 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_o1178z00_3899)));
								BgL_arg1816z00_4096 = (BgL_arg1817z00_4097 - OBJECT_TYPE);
							}
							BgL_arg1968z00_4094 =
								VECTOR_REF(BgL_arg1815z00_4095, BgL_arg1816z00_4096);
						}
						BgL_arg1967z00_4093 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1968z00_4094);
					}
					{	/* SawMill/expr.scm 13 */
						obj_t BgL_tmpz00_5303;

						BgL_tmpz00_5303 = ((obj_t) BgL_arg1967z00_4093);
						BgL_arg1966z00_4092 = BGL_CLASS_NUM(BgL_tmpz00_5303);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_rtl_regz00_bglt) BgL_o1178z00_3899)), BgL_arg1966z00_4092);
			}
			{	/* SawMill/expr.scm 13 */
				BgL_objectz00_bglt BgL_tmpz00_5309;

				BgL_tmpz00_5309 =
					((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1178z00_3899));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5309, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1178z00_3899));
			return ((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1178z00_3899));
		}

	}



/* &<@anonymous:1964> */
	obj_t BGl_z62zc3z04anonymousza31964ze3ze5zzsaw_exprz00(obj_t BgL_envz00_3900,
		obj_t BgL_new1177z00_3901)
	{
		{	/* SawMill/expr.scm 13 */
			{
				BgL_rtl_regz00_bglt BgL_auxz00_5317;

				{
					BgL_typez00_bglt BgL_auxz00_5318;

					{	/* SawMill/expr.scm 13 */
						obj_t BgL_classz00_4099;

						BgL_classz00_4099 = BGl_typez00zztype_typez00;
						{	/* SawMill/expr.scm 13 */
							obj_t BgL__ortest_1117z00_4100;

							BgL__ortest_1117z00_4100 = BGL_CLASS_NIL(BgL_classz00_4099);
							if (CBOOL(BgL__ortest_1117z00_4100))
								{	/* SawMill/expr.scm 13 */
									BgL_auxz00_5318 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_4100);
								}
							else
								{	/* SawMill/expr.scm 13 */
									BgL_auxz00_5318 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4099));
								}
						}
					}
					((((BgL_rtl_regz00_bglt) COBJECT(
									((BgL_rtl_regz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_new1177z00_3901))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_5318), BUNSPEC);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt)
									((BgL_rtl_regz00_bglt) BgL_new1177z00_3901))))->BgL_varz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1177z00_3901))))->BgL_onexprzf3zf3) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1177z00_3901))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1177z00_3901))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1177z00_3901))))->BgL_debugnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1177z00_3901))))->BgL_hardwarez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_pregz00_bglt BgL_auxz00_5346;

					{
						obj_t BgL_auxz00_5347;

						{	/* SawMill/expr.scm 13 */
							BgL_objectz00_bglt BgL_tmpz00_5348;

							BgL_tmpz00_5348 =
								((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_new1177z00_3901));
							BgL_auxz00_5347 = BGL_OBJECT_WIDENING(BgL_tmpz00_5348);
						}
						BgL_auxz00_5346 = ((BgL_pregz00_bglt) BgL_auxz00_5347);
					}
					((((BgL_pregz00_bglt) COBJECT(BgL_auxz00_5346))->BgL_indexz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_pregz00_bglt BgL_auxz00_5354;

					{
						obj_t BgL_auxz00_5355;

						{	/* SawMill/expr.scm 13 */
							BgL_objectz00_bglt BgL_tmpz00_5356;

							BgL_tmpz00_5356 =
								((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_new1177z00_3901));
							BgL_auxz00_5355 = BGL_OBJECT_WIDENING(BgL_tmpz00_5356);
						}
						BgL_auxz00_5354 = ((BgL_pregz00_bglt) BgL_auxz00_5355);
					}
					((((BgL_pregz00_bglt) COBJECT(BgL_auxz00_5354))->BgL_statusz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_5317 = ((BgL_rtl_regz00_bglt) BgL_new1177z00_3901);
				return ((obj_t) BgL_auxz00_5317);
			}
		}

	}



/* &lambda1962 */
	BgL_rtl_regz00_bglt BGl_z62lambda1962z62zzsaw_exprz00(obj_t BgL_envz00_3902,
		obj_t BgL_o1174z00_3903)
	{
		{	/* SawMill/expr.scm 13 */
			{	/* SawMill/expr.scm 13 */
				BgL_pregz00_bglt BgL_wide1176z00_4102;

				BgL_wide1176z00_4102 =
					((BgL_pregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_pregz00_bgl))));
				{	/* SawMill/expr.scm 13 */
					obj_t BgL_auxz00_5369;
					BgL_objectz00_bglt BgL_tmpz00_5365;

					BgL_auxz00_5369 = ((obj_t) BgL_wide1176z00_4102);
					BgL_tmpz00_5365 =
						((BgL_objectz00_bglt)
						((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1174z00_3903)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5365, BgL_auxz00_5369);
				}
				((BgL_objectz00_bglt)
					((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1174z00_3903)));
				{	/* SawMill/expr.scm 13 */
					long BgL_arg1963z00_4103;

					BgL_arg1963z00_4103 = BGL_CLASS_NUM(BGl_pregz00zzsaw_exprz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_rtl_regz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_o1174z00_3903))),
						BgL_arg1963z00_4103);
				}
				return
					((BgL_rtl_regz00_bglt)
					((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1174z00_3903)));
			}
		}

	}



/* &lambda1959 */
	BgL_rtl_regz00_bglt BGl_z62lambda1959z62zzsaw_exprz00(obj_t BgL_envz00_3904,
		obj_t BgL_type1165z00_3905, obj_t BgL_var1166z00_3906,
		obj_t BgL_onexprzf31167zf3_3907, obj_t BgL_name1168z00_3908,
		obj_t BgL_key1169z00_3909, obj_t BgL_debugname1170z00_3910,
		obj_t BgL_hardware1171z00_3911, obj_t BgL_index1172z00_3912,
		obj_t BgL_status1173z00_3913)
	{
		{	/* SawMill/expr.scm 13 */
			{	/* SawMill/expr.scm 13 */
				BgL_rtl_regz00_bglt BgL_new1285z00_4105;

				{	/* SawMill/expr.scm 13 */
					BgL_rtl_regz00_bglt BgL_tmp1283z00_4106;
					BgL_pregz00_bglt BgL_wide1284z00_4107;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_5383;

						{	/* SawMill/expr.scm 13 */
							BgL_rtl_regz00_bglt BgL_new1282z00_4108;

							BgL_new1282z00_4108 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/expr.scm 13 */
								long BgL_arg1961z00_4109;

								BgL_arg1961z00_4109 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1282z00_4108),
									BgL_arg1961z00_4109);
							}
							{	/* SawMill/expr.scm 13 */
								BgL_objectz00_bglt BgL_tmpz00_5388;

								BgL_tmpz00_5388 = ((BgL_objectz00_bglt) BgL_new1282z00_4108);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5388, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1282z00_4108);
							BgL_auxz00_5383 = BgL_new1282z00_4108;
						}
						BgL_tmp1283z00_4106 = ((BgL_rtl_regz00_bglt) BgL_auxz00_5383);
					}
					BgL_wide1284z00_4107 =
						((BgL_pregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_pregz00_bgl))));
					{	/* SawMill/expr.scm 13 */
						obj_t BgL_auxz00_5396;
						BgL_objectz00_bglt BgL_tmpz00_5394;

						BgL_auxz00_5396 = ((obj_t) BgL_wide1284z00_4107);
						BgL_tmpz00_5394 = ((BgL_objectz00_bglt) BgL_tmp1283z00_4106);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5394, BgL_auxz00_5396);
					}
					((BgL_objectz00_bglt) BgL_tmp1283z00_4106);
					{	/* SawMill/expr.scm 13 */
						long BgL_arg1960z00_4110;

						BgL_arg1960z00_4110 = BGL_CLASS_NUM(BGl_pregz00zzsaw_exprz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1283z00_4106), BgL_arg1960z00_4110);
					}
					BgL_new1285z00_4105 = ((BgL_rtl_regz00_bglt) BgL_tmp1283z00_4106);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1285z00_4105)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1165z00_3905)),
					BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1285z00_4105)))->BgL_varz00) =
					((obj_t) BgL_var1166z00_3906), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1285z00_4105)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31167zf3_3907), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1285z00_4105)))->BgL_namez00) =
					((obj_t) BgL_name1168z00_3908), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1285z00_4105)))->BgL_keyz00) =
					((obj_t) BgL_key1169z00_3909), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1285z00_4105)))->BgL_debugnamez00) =
					((obj_t) BgL_debugname1170z00_3910), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1285z00_4105)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1171z00_3911), BUNSPEC);
				{
					BgL_pregz00_bglt BgL_auxz00_5419;

					{
						obj_t BgL_auxz00_5420;

						{	/* SawMill/expr.scm 13 */
							BgL_objectz00_bglt BgL_tmpz00_5421;

							BgL_tmpz00_5421 = ((BgL_objectz00_bglt) BgL_new1285z00_4105);
							BgL_auxz00_5420 = BGL_OBJECT_WIDENING(BgL_tmpz00_5421);
						}
						BgL_auxz00_5419 = ((BgL_pregz00_bglt) BgL_auxz00_5420);
					}
					((((BgL_pregz00_bglt) COBJECT(BgL_auxz00_5419))->BgL_indexz00) =
						((obj_t) BgL_index1172z00_3912), BUNSPEC);
				}
				{
					BgL_pregz00_bglt BgL_auxz00_5426;

					{
						obj_t BgL_auxz00_5427;

						{	/* SawMill/expr.scm 13 */
							BgL_objectz00_bglt BgL_tmpz00_5428;

							BgL_tmpz00_5428 = ((BgL_objectz00_bglt) BgL_new1285z00_4105);
							BgL_auxz00_5427 = BGL_OBJECT_WIDENING(BgL_tmpz00_5428);
						}
						BgL_auxz00_5426 = ((BgL_pregz00_bglt) BgL_auxz00_5427);
					}
					((((BgL_pregz00_bglt) COBJECT(BgL_auxz00_5426))->BgL_statusz00) =
						((obj_t) BgL_status1173z00_3913), BUNSPEC);
				}
				return BgL_new1285z00_4105;
			}
		}

	}



/* &lambda1978 */
	obj_t BGl_z62lambda1978z62zzsaw_exprz00(obj_t BgL_envz00_3914,
		obj_t BgL_oz00_3915, obj_t BgL_vz00_3916)
	{
		{	/* SawMill/expr.scm 13 */
			{
				BgL_pregz00_bglt BgL_auxz00_5433;

				{
					obj_t BgL_auxz00_5434;

					{	/* SawMill/expr.scm 13 */
						BgL_objectz00_bglt BgL_tmpz00_5435;

						BgL_tmpz00_5435 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3915));
						BgL_auxz00_5434 = BGL_OBJECT_WIDENING(BgL_tmpz00_5435);
					}
					BgL_auxz00_5433 = ((BgL_pregz00_bglt) BgL_auxz00_5434);
				}
				return
					((((BgL_pregz00_bglt) COBJECT(BgL_auxz00_5433))->BgL_statusz00) =
					((obj_t) BgL_vz00_3916), BUNSPEC);
			}
		}

	}



/* &lambda1977 */
	obj_t BGl_z62lambda1977z62zzsaw_exprz00(obj_t BgL_envz00_3917,
		obj_t BgL_oz00_3918)
	{
		{	/* SawMill/expr.scm 13 */
			{
				BgL_pregz00_bglt BgL_auxz00_5441;

				{
					obj_t BgL_auxz00_5442;

					{	/* SawMill/expr.scm 13 */
						BgL_objectz00_bglt BgL_tmpz00_5443;

						BgL_tmpz00_5443 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3918));
						BgL_auxz00_5442 = BGL_OBJECT_WIDENING(BgL_tmpz00_5443);
					}
					BgL_auxz00_5441 = ((BgL_pregz00_bglt) BgL_auxz00_5442);
				}
				return (((BgL_pregz00_bglt) COBJECT(BgL_auxz00_5441))->BgL_statusz00);
			}
		}

	}



/* &lambda1973 */
	obj_t BGl_z62lambda1973z62zzsaw_exprz00(obj_t BgL_envz00_3919,
		obj_t BgL_oz00_3920, obj_t BgL_vz00_3921)
	{
		{	/* SawMill/expr.scm 13 */
			{
				BgL_pregz00_bglt BgL_auxz00_5449;

				{
					obj_t BgL_auxz00_5450;

					{	/* SawMill/expr.scm 13 */
						BgL_objectz00_bglt BgL_tmpz00_5451;

						BgL_tmpz00_5451 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3920));
						BgL_auxz00_5450 = BGL_OBJECT_WIDENING(BgL_tmpz00_5451);
					}
					BgL_auxz00_5449 = ((BgL_pregz00_bglt) BgL_auxz00_5450);
				}
				return
					((((BgL_pregz00_bglt) COBJECT(BgL_auxz00_5449))->BgL_indexz00) =
					((obj_t) BgL_vz00_3921), BUNSPEC);
			}
		}

	}



/* &lambda1972 */
	obj_t BGl_z62lambda1972z62zzsaw_exprz00(obj_t BgL_envz00_3922,
		obj_t BgL_oz00_3923)
	{
		{	/* SawMill/expr.scm 13 */
			{
				BgL_pregz00_bglt BgL_auxz00_5457;

				{
					obj_t BgL_auxz00_5458;

					{	/* SawMill/expr.scm 13 */
						BgL_objectz00_bglt BgL_tmpz00_5459;

						BgL_tmpz00_5459 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3923));
						BgL_auxz00_5458 = BGL_OBJECT_WIDENING(BgL_tmpz00_5459);
					}
					BgL_auxz00_5457 = ((BgL_pregz00_bglt) BgL_auxz00_5458);
				}
				return (((BgL_pregz00_bglt) COBJECT(BgL_auxz00_5457))->BgL_indexz00);
			}
		}

	}



/* &lambda1939 */
	BgL_rtl_regz00_bglt BGl_z62lambda1939z62zzsaw_exprz00(obj_t BgL_envz00_3924,
		obj_t BgL_o1163z00_3925)
	{
		{	/* SawMill/expr.scm 12 */
			{	/* SawMill/expr.scm 12 */
				long BgL_arg1940z00_4116;

				{	/* SawMill/expr.scm 12 */
					obj_t BgL_arg1941z00_4117;

					{	/* SawMill/expr.scm 12 */
						obj_t BgL_arg1942z00_4118;

						{	/* SawMill/expr.scm 12 */
							obj_t BgL_arg1815z00_4119;
							long BgL_arg1816z00_4120;

							BgL_arg1815z00_4119 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/expr.scm 12 */
								long BgL_arg1817z00_4121;

								BgL_arg1817z00_4121 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_o1163z00_3925)));
								BgL_arg1816z00_4120 = (BgL_arg1817z00_4121 - OBJECT_TYPE);
							}
							BgL_arg1942z00_4118 =
								VECTOR_REF(BgL_arg1815z00_4119, BgL_arg1816z00_4120);
						}
						BgL_arg1941z00_4117 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1942z00_4118);
					}
					{	/* SawMill/expr.scm 12 */
						obj_t BgL_tmpz00_5472;

						BgL_tmpz00_5472 = ((obj_t) BgL_arg1941z00_4117);
						BgL_arg1940z00_4116 = BGL_CLASS_NUM(BgL_tmpz00_5472);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_rtl_regz00_bglt) BgL_o1163z00_3925)), BgL_arg1940z00_4116);
			}
			{	/* SawMill/expr.scm 12 */
				BgL_objectz00_bglt BgL_tmpz00_5478;

				BgL_tmpz00_5478 =
					((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1163z00_3925));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5478, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1163z00_3925));
			return ((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1163z00_3925));
		}

	}



/* &<@anonymous:1938> */
	obj_t BGl_z62zc3z04anonymousza31938ze3ze5zzsaw_exprz00(obj_t BgL_envz00_3926,
		obj_t BgL_new1162z00_3927)
	{
		{	/* SawMill/expr.scm 12 */
			{
				BgL_rtl_regz00_bglt BgL_auxz00_5486;

				{
					BgL_typez00_bglt BgL_auxz00_5487;

					{	/* SawMill/expr.scm 12 */
						obj_t BgL_classz00_4123;

						BgL_classz00_4123 = BGl_typez00zztype_typez00;
						{	/* SawMill/expr.scm 12 */
							obj_t BgL__ortest_1117z00_4124;

							BgL__ortest_1117z00_4124 = BGL_CLASS_NIL(BgL_classz00_4123);
							if (CBOOL(BgL__ortest_1117z00_4124))
								{	/* SawMill/expr.scm 12 */
									BgL_auxz00_5487 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_4124);
								}
							else
								{	/* SawMill/expr.scm 12 */
									BgL_auxz00_5487 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_4123));
								}
						}
					}
					((((BgL_rtl_regz00_bglt) COBJECT(
									((BgL_rtl_regz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_new1162z00_3927))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_5487), BUNSPEC);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt)
									((BgL_rtl_regz00_bglt) BgL_new1162z00_3927))))->BgL_varz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1162z00_3927))))->BgL_onexprzf3zf3) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1162z00_3927))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1162z00_3927))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1162z00_3927))))->BgL_debugnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1162z00_3927))))->BgL_hardwarez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_iregz00_bglt BgL_auxz00_5515;

					{
						obj_t BgL_auxz00_5516;

						{	/* SawMill/expr.scm 12 */
							BgL_objectz00_bglt BgL_tmpz00_5517;

							BgL_tmpz00_5517 =
								((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_new1162z00_3927));
							BgL_auxz00_5516 = BGL_OBJECT_WIDENING(BgL_tmpz00_5517);
						}
						BgL_auxz00_5515 = ((BgL_iregz00_bglt) BgL_auxz00_5516);
					}
					((((BgL_iregz00_bglt) COBJECT(BgL_auxz00_5515))->BgL_indexz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_iregz00_bglt BgL_auxz00_5523;

					{
						obj_t BgL_auxz00_5524;

						{	/* SawMill/expr.scm 12 */
							BgL_objectz00_bglt BgL_tmpz00_5525;

							BgL_tmpz00_5525 =
								((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_new1162z00_3927));
							BgL_auxz00_5524 = BGL_OBJECT_WIDENING(BgL_tmpz00_5525);
						}
						BgL_auxz00_5523 = ((BgL_iregz00_bglt) BgL_auxz00_5524);
					}
					((((BgL_iregz00_bglt) COBJECT(BgL_auxz00_5523))->BgL_statusz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_5486 = ((BgL_rtl_regz00_bglt) BgL_new1162z00_3927);
				return ((obj_t) BgL_auxz00_5486);
			}
		}

	}



/* &lambda1936 */
	BgL_rtl_regz00_bglt BGl_z62lambda1936z62zzsaw_exprz00(obj_t BgL_envz00_3928,
		obj_t BgL_o1159z00_3929)
	{
		{	/* SawMill/expr.scm 12 */
			{	/* SawMill/expr.scm 12 */
				BgL_iregz00_bglt BgL_wide1161z00_4126;

				BgL_wide1161z00_4126 =
					((BgL_iregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_iregz00_bgl))));
				{	/* SawMill/expr.scm 12 */
					obj_t BgL_auxz00_5538;
					BgL_objectz00_bglt BgL_tmpz00_5534;

					BgL_auxz00_5538 = ((obj_t) BgL_wide1161z00_4126);
					BgL_tmpz00_5534 =
						((BgL_objectz00_bglt)
						((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1159z00_3929)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5534, BgL_auxz00_5538);
				}
				((BgL_objectz00_bglt)
					((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1159z00_3929)));
				{	/* SawMill/expr.scm 12 */
					long BgL_arg1937z00_4127;

					BgL_arg1937z00_4127 = BGL_CLASS_NUM(BGl_iregz00zzsaw_exprz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_rtl_regz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_o1159z00_3929))),
						BgL_arg1937z00_4127);
				}
				return
					((BgL_rtl_regz00_bglt)
					((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1159z00_3929)));
			}
		}

	}



/* &lambda1933 */
	BgL_rtl_regz00_bglt BGl_z62lambda1933z62zzsaw_exprz00(obj_t BgL_envz00_3930,
		obj_t BgL_type1150z00_3931, obj_t BgL_var1151z00_3932,
		obj_t BgL_onexprzf31152zf3_3933, obj_t BgL_name1153z00_3934,
		obj_t BgL_key1154z00_3935, obj_t BgL_debugname1155z00_3936,
		obj_t BgL_hardware1156z00_3937, obj_t BgL_index1157z00_3938,
		obj_t BgL_status1158z00_3939)
	{
		{	/* SawMill/expr.scm 12 */
			{	/* SawMill/expr.scm 12 */
				BgL_rtl_regz00_bglt BgL_new1280z00_4129;

				{	/* SawMill/expr.scm 12 */
					BgL_rtl_regz00_bglt BgL_tmp1278z00_4130;
					BgL_iregz00_bglt BgL_wide1279z00_4131;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_5552;

						{	/* SawMill/expr.scm 12 */
							BgL_rtl_regz00_bglt BgL_new1277z00_4132;

							BgL_new1277z00_4132 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/expr.scm 12 */
								long BgL_arg1935z00_4133;

								BgL_arg1935z00_4133 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1277z00_4132),
									BgL_arg1935z00_4133);
							}
							{	/* SawMill/expr.scm 12 */
								BgL_objectz00_bglt BgL_tmpz00_5557;

								BgL_tmpz00_5557 = ((BgL_objectz00_bglt) BgL_new1277z00_4132);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5557, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1277z00_4132);
							BgL_auxz00_5552 = BgL_new1277z00_4132;
						}
						BgL_tmp1278z00_4130 = ((BgL_rtl_regz00_bglt) BgL_auxz00_5552);
					}
					BgL_wide1279z00_4131 =
						((BgL_iregz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_iregz00_bgl))));
					{	/* SawMill/expr.scm 12 */
						obj_t BgL_auxz00_5565;
						BgL_objectz00_bglt BgL_tmpz00_5563;

						BgL_auxz00_5565 = ((obj_t) BgL_wide1279z00_4131);
						BgL_tmpz00_5563 = ((BgL_objectz00_bglt) BgL_tmp1278z00_4130);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5563, BgL_auxz00_5565);
					}
					((BgL_objectz00_bglt) BgL_tmp1278z00_4130);
					{	/* SawMill/expr.scm 12 */
						long BgL_arg1934z00_4134;

						BgL_arg1934z00_4134 = BGL_CLASS_NUM(BGl_iregz00zzsaw_exprz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1278z00_4130), BgL_arg1934z00_4134);
					}
					BgL_new1280z00_4129 = ((BgL_rtl_regz00_bglt) BgL_tmp1278z00_4130);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1280z00_4129)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1150z00_3931)),
					BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1280z00_4129)))->BgL_varz00) =
					((obj_t) BgL_var1151z00_3932), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1280z00_4129)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31152zf3_3933), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1280z00_4129)))->BgL_namez00) =
					((obj_t) BgL_name1153z00_3934), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1280z00_4129)))->BgL_keyz00) =
					((obj_t) BgL_key1154z00_3935), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1280z00_4129)))->BgL_debugnamez00) =
					((obj_t) BgL_debugname1155z00_3936), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1280z00_4129)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1156z00_3937), BUNSPEC);
				{
					BgL_iregz00_bglt BgL_auxz00_5588;

					{
						obj_t BgL_auxz00_5589;

						{	/* SawMill/expr.scm 12 */
							BgL_objectz00_bglt BgL_tmpz00_5590;

							BgL_tmpz00_5590 = ((BgL_objectz00_bglt) BgL_new1280z00_4129);
							BgL_auxz00_5589 = BGL_OBJECT_WIDENING(BgL_tmpz00_5590);
						}
						BgL_auxz00_5588 = ((BgL_iregz00_bglt) BgL_auxz00_5589);
					}
					((((BgL_iregz00_bglt) COBJECT(BgL_auxz00_5588))->BgL_indexz00) =
						((obj_t) BgL_index1157z00_3938), BUNSPEC);
				}
				{
					BgL_iregz00_bglt BgL_auxz00_5595;

					{
						obj_t BgL_auxz00_5596;

						{	/* SawMill/expr.scm 12 */
							BgL_objectz00_bglt BgL_tmpz00_5597;

							BgL_tmpz00_5597 = ((BgL_objectz00_bglt) BgL_new1280z00_4129);
							BgL_auxz00_5596 = BGL_OBJECT_WIDENING(BgL_tmpz00_5597);
						}
						BgL_auxz00_5595 = ((BgL_iregz00_bglt) BgL_auxz00_5596);
					}
					((((BgL_iregz00_bglt) COBJECT(BgL_auxz00_5595))->BgL_statusz00) =
						((obj_t) BgL_status1158z00_3939), BUNSPEC);
				}
				return BgL_new1280z00_4129;
			}
		}

	}



/* &lambda1952 */
	obj_t BGl_z62lambda1952z62zzsaw_exprz00(obj_t BgL_envz00_3940,
		obj_t BgL_oz00_3941, obj_t BgL_vz00_3942)
	{
		{	/* SawMill/expr.scm 12 */
			{
				BgL_iregz00_bglt BgL_auxz00_5602;

				{
					obj_t BgL_auxz00_5603;

					{	/* SawMill/expr.scm 12 */
						BgL_objectz00_bglt BgL_tmpz00_5604;

						BgL_tmpz00_5604 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3941));
						BgL_auxz00_5603 = BGL_OBJECT_WIDENING(BgL_tmpz00_5604);
					}
					BgL_auxz00_5602 = ((BgL_iregz00_bglt) BgL_auxz00_5603);
				}
				return
					((((BgL_iregz00_bglt) COBJECT(BgL_auxz00_5602))->BgL_statusz00) =
					((obj_t) BgL_vz00_3942), BUNSPEC);
			}
		}

	}



/* &lambda1951 */
	obj_t BGl_z62lambda1951z62zzsaw_exprz00(obj_t BgL_envz00_3943,
		obj_t BgL_oz00_3944)
	{
		{	/* SawMill/expr.scm 12 */
			{
				BgL_iregz00_bglt BgL_auxz00_5610;

				{
					obj_t BgL_auxz00_5611;

					{	/* SawMill/expr.scm 12 */
						BgL_objectz00_bglt BgL_tmpz00_5612;

						BgL_tmpz00_5612 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3944));
						BgL_auxz00_5611 = BGL_OBJECT_WIDENING(BgL_tmpz00_5612);
					}
					BgL_auxz00_5610 = ((BgL_iregz00_bglt) BgL_auxz00_5611);
				}
				return (((BgL_iregz00_bglt) COBJECT(BgL_auxz00_5610))->BgL_statusz00);
			}
		}

	}



/* &lambda1947 */
	obj_t BGl_z62lambda1947z62zzsaw_exprz00(obj_t BgL_envz00_3945,
		obj_t BgL_oz00_3946, obj_t BgL_vz00_3947)
	{
		{	/* SawMill/expr.scm 12 */
			{
				BgL_iregz00_bglt BgL_auxz00_5618;

				{
					obj_t BgL_auxz00_5619;

					{	/* SawMill/expr.scm 12 */
						BgL_objectz00_bglt BgL_tmpz00_5620;

						BgL_tmpz00_5620 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3946));
						BgL_auxz00_5619 = BGL_OBJECT_WIDENING(BgL_tmpz00_5620);
					}
					BgL_auxz00_5618 = ((BgL_iregz00_bglt) BgL_auxz00_5619);
				}
				return
					((((BgL_iregz00_bglt) COBJECT(BgL_auxz00_5618))->BgL_indexz00) =
					((obj_t) BgL_vz00_3947), BUNSPEC);
			}
		}

	}



/* &lambda1946 */
	obj_t BGl_z62lambda1946z62zzsaw_exprz00(obj_t BgL_envz00_3948,
		obj_t BgL_oz00_3949)
	{
		{	/* SawMill/expr.scm 12 */
			{
				BgL_iregz00_bglt BgL_auxz00_5626;

				{
					obj_t BgL_auxz00_5627;

					{	/* SawMill/expr.scm 12 */
						BgL_objectz00_bglt BgL_tmpz00_5628;

						BgL_tmpz00_5628 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3949));
						BgL_auxz00_5627 = BGL_OBJECT_WIDENING(BgL_tmpz00_5628);
					}
					BgL_auxz00_5626 = ((BgL_iregz00_bglt) BgL_auxz00_5627);
				}
				return (((BgL_iregz00_bglt) COBJECT(BgL_auxz00_5626))->BgL_indexz00);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_exprz00(void)
	{
		{	/* SawMill/expr.scm 1 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_acceptzd2foldingzf3zd2envzf3zzsaw_exprz00,
				BGl_proc2153z00zzsaw_exprz00, BGl_backendz00zzbackend_backendz00,
				BGl_string2154z00zzsaw_exprz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_effectszd2envzd2zzsaw_exprz00, BGl_proc2155z00zzsaw_exprz00,
				BGl_rtl_funz00zzsaw_defsz00, BGl_string2156z00zzsaw_exprz00);
		}

	}



/* &effects1543 */
	obj_t BGl_z62effects1543z62zzsaw_exprz00(obj_t BgL_envz00_3952,
		obj_t BgL_funz00_3953)
	{
		{	/* SawMill/expr.scm 229 */
			return ((obj_t) BGl_za2noeffectza2z00zzsaw_exprz00);
		}

	}



/* &accept-folding?1541 */
	obj_t BGl_z62acceptzd2foldingzf31541z43zzsaw_exprz00(obj_t BgL_envz00_3954,
		obj_t BgL_bz00_3955, obj_t BgL_insz00_3956, obj_t BgL_treez00_3957)
	{
		{	/* SawMill/expr.scm 166 */
			return BBOOL(((bool_t) 1));
		}

	}



/* accept-folding? */
	BGL_EXPORTED_DEF obj_t
		BGl_acceptzd2foldingzf3z21zzsaw_exprz00(BgL_backendz00_bglt BgL_bz00_106,
		obj_t BgL_insz00_107, obj_t BgL_treez00_108)
	{
		{	/* SawMill/expr.scm 166 */
			{	/* SawMill/expr.scm 166 */
				obj_t BgL_method1542z00_2613;

				{	/* SawMill/expr.scm 166 */
					obj_t BgL_res2121z00_3648;

					{	/* SawMill/expr.scm 166 */
						long BgL_objzd2classzd2numz00_3619;

						BgL_objzd2classzd2numz00_3619 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_bz00_106));
						{	/* SawMill/expr.scm 166 */
							obj_t BgL_arg1811z00_3620;

							BgL_arg1811z00_3620 =
								PROCEDURE_REF(BGl_acceptzd2foldingzf3zd2envzf3zzsaw_exprz00,
								(int) (1L));
							{	/* SawMill/expr.scm 166 */
								int BgL_offsetz00_3623;

								BgL_offsetz00_3623 = (int) (BgL_objzd2classzd2numz00_3619);
								{	/* SawMill/expr.scm 166 */
									long BgL_offsetz00_3624;

									BgL_offsetz00_3624 =
										((long) (BgL_offsetz00_3623) - OBJECT_TYPE);
									{	/* SawMill/expr.scm 166 */
										long BgL_modz00_3625;

										BgL_modz00_3625 =
											(BgL_offsetz00_3624 >> (int) ((long) ((int) (4L))));
										{	/* SawMill/expr.scm 166 */
											long BgL_restz00_3627;

											BgL_restz00_3627 =
												(BgL_offsetz00_3624 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawMill/expr.scm 166 */

												{	/* SawMill/expr.scm 166 */
													obj_t BgL_bucketz00_3629;

													BgL_bucketz00_3629 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3620), BgL_modz00_3625);
													BgL_res2121z00_3648 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3629), BgL_restz00_3627);
					}}}}}}}}
					BgL_method1542z00_2613 = BgL_res2121z00_3648;
				}
				return
					BGL_PROCEDURE_CALL3(BgL_method1542z00_2613,
					((obj_t) BgL_bz00_106), BgL_insz00_107, BgL_treez00_108);
			}
		}

	}



/* &accept-folding? */
	obj_t BGl_z62acceptzd2foldingzf3z43zzsaw_exprz00(obj_t BgL_envz00_3958,
		obj_t BgL_bz00_3959, obj_t BgL_insz00_3960, obj_t BgL_treez00_3961)
	{
		{	/* SawMill/expr.scm 166 */
			return
				BGl_acceptzd2foldingzf3z21zzsaw_exprz00(
				((BgL_backendz00_bglt) BgL_bz00_3959), BgL_insz00_3960,
				BgL_treez00_3961);
		}

	}



/* effects */
	obj_t BGl_effectsz00zzsaw_exprz00(BgL_rtl_funz00_bglt BgL_funz00_117)
	{
		{	/* SawMill/expr.scm 229 */
			{	/* SawMill/expr.scm 229 */
				obj_t BgL_method1544z00_2614;

				{	/* SawMill/expr.scm 229 */
					obj_t BgL_res2126z00_3679;

					{	/* SawMill/expr.scm 229 */
						long BgL_objzd2classzd2numz00_3650;

						BgL_objzd2classzd2numz00_3650 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_117));
						{	/* SawMill/expr.scm 229 */
							obj_t BgL_arg1811z00_3651;

							BgL_arg1811z00_3651 =
								PROCEDURE_REF(BGl_effectszd2envzd2zzsaw_exprz00, (int) (1L));
							{	/* SawMill/expr.scm 229 */
								int BgL_offsetz00_3654;

								BgL_offsetz00_3654 = (int) (BgL_objzd2classzd2numz00_3650);
								{	/* SawMill/expr.scm 229 */
									long BgL_offsetz00_3655;

									BgL_offsetz00_3655 =
										((long) (BgL_offsetz00_3654) - OBJECT_TYPE);
									{	/* SawMill/expr.scm 229 */
										long BgL_modz00_3656;

										BgL_modz00_3656 =
											(BgL_offsetz00_3655 >> (int) ((long) ((int) (4L))));
										{	/* SawMill/expr.scm 229 */
											long BgL_restz00_3658;

											BgL_restz00_3658 =
												(BgL_offsetz00_3655 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawMill/expr.scm 229 */

												{	/* SawMill/expr.scm 229 */
													obj_t BgL_bucketz00_3660;

													BgL_bucketz00_3660 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3651), BgL_modz00_3656);
													BgL_res2126z00_3679 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3660), BgL_restz00_3658);
					}}}}}}}}
					BgL_method1544z00_2614 = BgL_res2126z00_3679;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1544z00_2614, ((obj_t) BgL_funz00_117));
			}
		}

	}



/* &effects */
	obj_t BGl_z62effectsz62zzsaw_exprz00(obj_t BgL_envz00_3962,
		obj_t BgL_funz00_3963)
	{
		{	/* SawMill/expr.scm 229 */
			return
				BGl_effectsz00zzsaw_exprz00(((BgL_rtl_funz00_bglt) BgL_funz00_3963));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_exprz00(void)
	{
		{	/* SawMill/expr.scm 1 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_effectszd2envzd2zzsaw_exprz00, BGl_rtl_loadgz00zzsaw_defsz00,
				BGl_proc2157z00zzsaw_exprz00, BGl_string2158z00zzsaw_exprz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_effectszd2envzd2zzsaw_exprz00, BGl_rtl_storegz00zzsaw_defsz00,
				BGl_proc2159z00zzsaw_exprz00, BGl_string2158z00zzsaw_exprz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_effectszd2envzd2zzsaw_exprz00, BGl_rtl_vrefz00zzsaw_defsz00,
				BGl_proc2160z00zzsaw_exprz00, BGl_string2158z00zzsaw_exprz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_effectszd2envzd2zzsaw_exprz00, BGl_rtl_vsetz00zzsaw_defsz00,
				BGl_proc2161z00zzsaw_exprz00, BGl_string2158z00zzsaw_exprz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_effectszd2envzd2zzsaw_exprz00, BGl_rtl_boxrefz00zzsaw_defsz00,
				BGl_proc2162z00zzsaw_exprz00, BGl_string2158z00zzsaw_exprz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_effectszd2envzd2zzsaw_exprz00, BGl_rtl_boxsetz00zzsaw_defsz00,
				BGl_proc2163z00zzsaw_exprz00, BGl_string2158z00zzsaw_exprz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_effectszd2envzd2zzsaw_exprz00, BGl_rtl_getfieldz00zzsaw_defsz00,
				BGl_proc2164z00zzsaw_exprz00, BGl_string2158z00zzsaw_exprz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_effectszd2envzd2zzsaw_exprz00, BGl_rtl_setfieldz00zzsaw_defsz00,
				BGl_proc2165z00zzsaw_exprz00, BGl_string2158z00zzsaw_exprz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_effectszd2envzd2zzsaw_exprz00, BGl_rtl_applyz00zzsaw_defsz00,
				BGl_proc2166z00zzsaw_exprz00, BGl_string2158z00zzsaw_exprz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_effectszd2envzd2zzsaw_exprz00,
				BGl_rtl_lightfuncallz00zzsaw_defsz00, BGl_proc2167z00zzsaw_exprz00,
				BGl_string2158z00zzsaw_exprz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_effectszd2envzd2zzsaw_exprz00, BGl_rtl_funcallz00zzsaw_defsz00,
				BGl_proc2168z00zzsaw_exprz00, BGl_string2158z00zzsaw_exprz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_effectszd2envzd2zzsaw_exprz00, BGl_rtl_pragmaz00zzsaw_defsz00,
				BGl_proc2169z00zzsaw_exprz00, BGl_string2158z00zzsaw_exprz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_effectszd2envzd2zzsaw_exprz00, BGl_rtl_protectz00zzsaw_defsz00,
				BGl_proc2170z00zzsaw_exprz00, BGl_string2158z00zzsaw_exprz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_effectszd2envzd2zzsaw_exprz00, BGl_rtl_callz00zzsaw_defsz00,
				BGl_proc2171z00zzsaw_exprz00, BGl_string2158z00zzsaw_exprz00);
		}

	}



/* &effects-rtl_call1572 */
	obj_t BGl_z62effectszd2rtl_call1572zb0zzsaw_exprz00(obj_t BgL_envz00_3978,
		obj_t BgL_funz00_3979)
	{
		{	/* SawMill/expr.scm 244 */
			{	/* SawMill/expr.scm 245 */
				BgL_globalz00_bglt BgL_varz00_4142;

				BgL_varz00_4142 =
					(((BgL_rtl_callz00_bglt) COBJECT(
							((BgL_rtl_callz00_bglt) BgL_funz00_3979)))->BgL_varz00);
				{	/* SawMill/expr.scm 246 */
					obj_t BgL__ortest_1254z00_4143;

					BgL__ortest_1254z00_4143 =
						BGl_getpropz00zz__r4_symbols_6_4z00(
						(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_varz00_4142)))->BgL_idz00),
						(((BgL_globalz00_bglt) COBJECT(BgL_varz00_4142))->BgL_modulez00));
					if (CBOOL(BgL__ortest_1254z00_4143))
						{	/* SawMill/expr.scm 246 */
							return BgL__ortest_1254z00_4143;
						}
					else
						{	/* SawMill/expr.scm 246 */
							return BGl_manuzd2effectzd2zzsaw_exprz00(BgL_varz00_4142);
						}
				}
			}
		}

	}



/* &effects-rtl_protect1570 */
	obj_t BGl_z62effectszd2rtl_protect1570zb0zzsaw_exprz00(obj_t BgL_envz00_3980,
		obj_t BgL_funz00_3981)
	{
		{	/* SawMill/expr.scm 242 */
			return ((obj_t) BGl_za2fullza2z00zzsaw_exprz00);
		}

	}



/* &effects-rtl_pragma1568 */
	obj_t BGl_z62effectszd2rtl_pragma1568zb0zzsaw_exprz00(obj_t BgL_envz00_3982,
		obj_t BgL_funz00_3983)
	{
		{	/* SawMill/expr.scm 241 */
			return ((obj_t) BGl_za2fullza2z00zzsaw_exprz00);
		}

	}



/* &effects-rtl_funcall1566 */
	obj_t BGl_z62effectszd2rtl_funcall1566zb0zzsaw_exprz00(obj_t BgL_envz00_3984,
		obj_t BgL_funz00_3985)
	{
		{	/* SawMill/expr.scm 240 */
			return ((obj_t) BGl_za2fullza2z00zzsaw_exprz00);
		}

	}



/* &effects-rtl_lightfun1564 */
	obj_t BGl_z62effectszd2rtl_lightfun1564zb0zzsaw_exprz00(obj_t BgL_envz00_3986,
		obj_t BgL_funz00_3987)
	{
		{	/* SawMill/expr.scm 239 */
			return ((obj_t) BGl_za2fullza2z00zzsaw_exprz00);
		}

	}



/* &effects-rtl_apply1562 */
	obj_t BGl_z62effectszd2rtl_apply1562zb0zzsaw_exprz00(obj_t BgL_envz00_3988,
		obj_t BgL_funz00_3989)
	{
		{	/* SawMill/expr.scm 238 */
			return ((obj_t) BGl_za2fullza2z00zzsaw_exprz00);
		}

	}



/* &effects-rtl_setfield1560 */
	obj_t BGl_z62effectszd2rtl_setfield1560zb0zzsaw_exprz00(obj_t BgL_envz00_3990,
		obj_t BgL_funz00_3991)
	{
		{	/* SawMill/expr.scm 237 */
			return ((obj_t) BGl_za2setfza2z00zzsaw_exprz00);
		}

	}



/* &effects-rtl_getfield1558 */
	obj_t BGl_z62effectszd2rtl_getfield1558zb0zzsaw_exprz00(obj_t BgL_envz00_3992,
		obj_t BgL_funz00_3993)
	{
		{	/* SawMill/expr.scm 236 */
			return ((obj_t) BGl_za2getfza2z00zzsaw_exprz00);
		}

	}



/* &effects-rtl_boxset1556 */
	obj_t BGl_z62effectszd2rtl_boxset1556zb0zzsaw_exprz00(obj_t BgL_envz00_3994,
		obj_t BgL_funz00_3995)
	{
		{	/* SawMill/expr.scm 235 */
			return ((obj_t) BGl_za2boxsetza2z00zzsaw_exprz00);
		}

	}



/* &effects-rtl_boxref1554 */
	obj_t BGl_z62effectszd2rtl_boxref1554zb0zzsaw_exprz00(obj_t BgL_envz00_3996,
		obj_t BgL_funz00_3997)
	{
		{	/* SawMill/expr.scm 234 */
			return ((obj_t) BGl_za2boxrefza2z00zzsaw_exprz00);
		}

	}



/* &effects-rtl_vset1552 */
	obj_t BGl_z62effectszd2rtl_vset1552zb0zzsaw_exprz00(obj_t BgL_envz00_3998,
		obj_t BgL_funz00_3999)
	{
		{	/* SawMill/expr.scm 233 */
			return ((obj_t) BGl_za2vsetza2z00zzsaw_exprz00);
		}

	}



/* &effects-rtl_vref1550 */
	obj_t BGl_z62effectszd2rtl_vref1550zb0zzsaw_exprz00(obj_t BgL_envz00_4000,
		obj_t BgL_funz00_4001)
	{
		{	/* SawMill/expr.scm 232 */
			return ((obj_t) BGl_za2vrefza2z00zzsaw_exprz00);
		}

	}



/* &effects-rtl_storeg1548 */
	obj_t BGl_z62effectszd2rtl_storeg1548zb0zzsaw_exprz00(obj_t BgL_envz00_4002,
		obj_t BgL_funz00_4003)
	{
		{	/* SawMill/expr.scm 231 */
			return ((obj_t) BGl_za2storegza2z00zzsaw_exprz00);
		}

	}



/* &effects-rtl_loadg1546 */
	obj_t BGl_z62effectszd2rtl_loadg1546zb0zzsaw_exprz00(obj_t BgL_envz00_4004,
		obj_t BgL_funz00_4005)
	{
		{	/* SawMill/expr.scm 230 */
			return ((obj_t) BGl_za2loadgza2z00zzsaw_exprz00);
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_exprz00(void)
	{
		{	/* SawMill/expr.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2172z00zzsaw_exprz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2172z00zzsaw_exprz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2172z00zzsaw_exprz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2172z00zzsaw_exprz00));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string2172z00zzsaw_exprz00));
			return
				BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2172z00zzsaw_exprz00));
		}

	}

#ifdef __cplusplus
}
#endif
