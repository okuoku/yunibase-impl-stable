/*===========================================================================*/
/*   (SawMill/rtl.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/rtl.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_RTL_TYPE_DEFINITIONS
#define BGL_SAW_RTL_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;


#endif													// BGL_SAW_RTL_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62namez62zzsaw_rtlz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_rtlz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_namez00zzsaw_rtlz00(BgL_rtl_insz00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t BGl_neighboursz00zzsaw_rtlz00(BgL_rtl_insz00_bglt);
	static obj_t BGl_z62neighboursz62zzsaw_rtlz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_rtlz00(void);
	static obj_t BGl_z62unlinkz12z70zzsaw_rtlz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzsaw_rtlz00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_rtlz00(void);
	BGL_EXPORTED_DECL obj_t BGl_rtl_dfsz00zzsaw_rtlz00(BgL_blockz00_bglt, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzsaw_rtlz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_substz00zzsaw_libz00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_rtlz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_rtlz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_rtlz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_rtlz00(void);
	BGL_EXPORTED_DECL obj_t BGl_unlinkz12z12zzsaw_rtlz00(BgL_blockz00_bglt);
	BGL_IMPORT obj_t BGl_classzd2namezd2zz__objectz00(obj_t);
	static obj_t BGl_z62rtl_dfsz62zzsaw_rtlz00(obj_t, obj_t, obj_t, obj_t);
	extern obj_t BGl_substzd2appendzd2zzsaw_libz00(obj_t, obj_t, obj_t);
	static obj_t __cnst[4];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_dfszd2envzd2zzsaw_rtlz00,
		BgL_bgl_za762rtl_dfsza762za7za7s1838z00, BGl_z62rtl_dfsz62zzsaw_rtlz00, 0L,
		BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_namezd2envzd2zzsaw_rtlz00,
		BgL_bgl_za762nameza762za7za7saw_1839z00, BGl_z62namez62zzsaw_rtlz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1834z00zzsaw_rtlz00,
		BgL_bgl_string1834za700za7za7s1840za7, "must have only one successor", 28);
	      DEFINE_STRING(BGl_string1835z00zzsaw_rtlz00,
		BgL_bgl_string1835za700za7za7s1841za7, "saw_rtl", 7);
	      DEFINE_STRING(BGl_string1836z00zzsaw_rtlz00,
		BgL_bgl_string1836za700za7za7s1842za7, "unlink! (froms) -> (to) ", 24);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_neighbourszd2envzd2zzsaw_rtlz00,
		BgL_bgl_za762neighboursza7621843z00, BGl_z62neighboursz62zzsaw_rtlz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_unlinkz12zd2envzc0zzsaw_rtlz00,
		BgL_bgl_za762unlinkza712za770za71844z00, BGl_z62unlinkz12z70zzsaw_rtlz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_rtlz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_rtlz00(long
		BgL_checksumz00_2201, char *BgL_fromz00_2202)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_rtlz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_rtlz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_rtlz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_rtlz00();
					BGl_cnstzd2initzd2zzsaw_rtlz00();
					BGl_importedzd2moduleszd2initz00zzsaw_rtlz00();
					return BGl_methodzd2initzd2zzsaw_rtlz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_rtlz00(void)
	{
		{	/* SawMill/rtl.scm 1 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_rtl");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_rtl");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "saw_rtl");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "saw_rtl");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "saw_rtl");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_rtl");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_rtl");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_rtl");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_rtlz00(void)
	{
		{	/* SawMill/rtl.scm 1 */
			{	/* SawMill/rtl.scm 1 */
				obj_t BgL_cportz00_2190;

				{	/* SawMill/rtl.scm 1 */
					obj_t BgL_stringz00_2197;

					BgL_stringz00_2197 = BGl_string1836z00zzsaw_rtlz00;
					{	/* SawMill/rtl.scm 1 */
						obj_t BgL_startz00_2198;

						BgL_startz00_2198 = BINT(0L);
						{	/* SawMill/rtl.scm 1 */
							obj_t BgL_endz00_2199;

							BgL_endz00_2199 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2197)));
							{	/* SawMill/rtl.scm 1 */

								BgL_cportz00_2190 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2197, BgL_startz00_2198, BgL_endz00_2199);
				}}}}
				{
					long BgL_iz00_2191;

					BgL_iz00_2191 = 3L;
				BgL_loopz00_2192:
					if ((BgL_iz00_2191 == -1L))
						{	/* SawMill/rtl.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawMill/rtl.scm 1 */
							{	/* SawMill/rtl.scm 1 */
								obj_t BgL_arg1837z00_2193;

								{	/* SawMill/rtl.scm 1 */

									{	/* SawMill/rtl.scm 1 */
										obj_t BgL_locationz00_2195;

										BgL_locationz00_2195 = BBOOL(((bool_t) 0));
										{	/* SawMill/rtl.scm 1 */

											BgL_arg1837z00_2193 =
												BGl_readz00zz__readerz00(BgL_cportz00_2190,
												BgL_locationz00_2195);
										}
									}
								}
								{	/* SawMill/rtl.scm 1 */
									int BgL_tmpz00_2228;

									BgL_tmpz00_2228 = (int) (BgL_iz00_2191);
									CNST_TABLE_SET(BgL_tmpz00_2228, BgL_arg1837z00_2193);
							}}
							{	/* SawMill/rtl.scm 1 */
								int BgL_auxz00_2196;

								BgL_auxz00_2196 = (int) ((BgL_iz00_2191 - 1L));
								{
									long BgL_iz00_2233;

									BgL_iz00_2233 = (long) (BgL_auxz00_2196);
									BgL_iz00_2191 = BgL_iz00_2233;
									goto BgL_loopz00_2192;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_rtlz00(void)
	{
		{	/* SawMill/rtl.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* name */
	BGL_EXPORTED_DEF obj_t BGl_namez00zzsaw_rtlz00(BgL_rtl_insz00_bglt
		BgL_insz00_3)
	{
		{	/* SawMill/rtl.scm 21 */
			{	/* SawMill/rtl.scm 22 */
				obj_t BgL_arg1377z00_2144;

				{	/* SawMill/rtl.scm 22 */
					BgL_rtl_funz00_bglt BgL_arg1378z00_2145;

					BgL_arg1378z00_2145 =
						(((BgL_rtl_insz00_bglt) COBJECT(BgL_insz00_3))->BgL_funz00);
					{	/* SawMill/rtl.scm 22 */
						obj_t BgL_arg1815z00_2148;
						long BgL_arg1816z00_2149;

						BgL_arg1815z00_2148 = (BGl_za2classesza2z00zz__objectz00);
						{	/* SawMill/rtl.scm 22 */
							long BgL_arg1817z00_2150;

							BgL_arg1817z00_2150 =
								BGL_OBJECT_CLASS_NUM(
								((BgL_objectz00_bglt) BgL_arg1378z00_2145));
							BgL_arg1816z00_2149 = (BgL_arg1817z00_2150 - OBJECT_TYPE);
						}
						BgL_arg1377z00_2144 =
							VECTOR_REF(BgL_arg1815z00_2148, BgL_arg1816z00_2149);
				}}
				return BGl_classzd2namezd2zz__objectz00(BgL_arg1377z00_2144);
			}
		}

	}



/* &name */
	obj_t BGl_z62namez62zzsaw_rtlz00(obj_t BgL_envz00_2180, obj_t BgL_insz00_2181)
	{
		{	/* SawMill/rtl.scm 21 */
			return BGl_namez00zzsaw_rtlz00(((BgL_rtl_insz00_bglt) BgL_insz00_2181));
		}

	}



/* neighbours */
	BGL_EXPORTED_DEF obj_t BGl_neighboursz00zzsaw_rtlz00(BgL_rtl_insz00_bglt
		BgL_insz00_4)
	{
		{	/* SawMill/rtl.scm 27 */
			{	/* SawMill/rtl.scm 31 */
				obj_t BgL_arg1379z00_1802;

				{	/* SawMill/rtl.scm 22 */
					obj_t BgL_arg1377z00_2157;

					{	/* SawMill/rtl.scm 22 */
						BgL_rtl_funz00_bglt BgL_arg1378z00_2158;

						BgL_arg1378z00_2158 =
							(((BgL_rtl_insz00_bglt) COBJECT(BgL_insz00_4))->BgL_funz00);
						{	/* SawMill/rtl.scm 22 */
							obj_t BgL_arg1815z00_2161;
							long BgL_arg1816z00_2162;

							BgL_arg1815z00_2161 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/rtl.scm 22 */
								long BgL_arg1817z00_2163;

								BgL_arg1817z00_2163 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt) BgL_arg1378z00_2158));
								BgL_arg1816z00_2162 = (BgL_arg1817z00_2163 - OBJECT_TYPE);
							}
							BgL_arg1377z00_2157 =
								VECTOR_REF(BgL_arg1815z00_2161, BgL_arg1816z00_2162);
					}}
					BgL_arg1379z00_1802 =
						BGl_classzd2namezd2zz__objectz00(BgL_arg1377z00_2157);
				}
				{	/* SawMill/rtl.scm 29 */
					obj_t BgL_list1380z00_1803;

					{	/* SawMill/rtl.scm 29 */
						obj_t BgL_arg1408z00_1804;

						{	/* SawMill/rtl.scm 29 */
							obj_t BgL_arg1410z00_1805;

							{	/* SawMill/rtl.scm 29 */
								obj_t BgL_arg1421z00_1806;

								{	/* SawMill/rtl.scm 29 */
									obj_t BgL_arg1422z00_1807;

									BgL_arg1422z00_1807 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
									BgL_arg1421z00_1806 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), BgL_arg1422z00_1807);
								}
								BgL_arg1410z00_1805 =
									MAKE_YOUNG_PAIR(BgL_arg1379z00_1802, BgL_arg1421z00_1806);
							}
							BgL_arg1408z00_1804 =
								MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), BgL_arg1410z00_1805);
						}
						BgL_list1380z00_1803 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BgL_arg1408z00_1804);
					}
					return BgL_list1380z00_1803;
				}
			}
		}

	}



/* &neighbours */
	obj_t BGl_z62neighboursz62zzsaw_rtlz00(obj_t BgL_envz00_2182,
		obj_t BgL_insz00_2183)
	{
		{	/* SawMill/rtl.scm 27 */
			return
				BGl_neighboursz00zzsaw_rtlz00(((BgL_rtl_insz00_bglt) BgL_insz00_2183));
		}

	}



/* unlink! */
	BGL_EXPORTED_DEF obj_t BGl_unlinkz12z12zzsaw_rtlz00(BgL_blockz00_bglt
		BgL_bz00_5)
	{
		{	/* SawMill/rtl.scm 39 */
			{	/* SawMill/rtl.scm 41 */
				bool_t BgL_test1847z00_2263;

				if (NULLP((((BgL_blockz00_bglt) COBJECT(BgL_bz00_5))->BgL_succsz00)))
					{	/* SawMill/rtl.scm 41 */
						BgL_test1847z00_2263 = ((bool_t) 1);
					}
				else
					{	/* SawMill/rtl.scm 41 */
						obj_t BgL_tmpz00_2267;

						BgL_tmpz00_2267 =
							CDR((((BgL_blockz00_bglt) COBJECT(BgL_bz00_5))->BgL_succsz00));
						BgL_test1847z00_2263 = PAIRP(BgL_tmpz00_2267);
					}
				if (BgL_test1847z00_2263)
					{	/* SawMill/rtl.scm 41 */
						BGl_errorz00zz__errorz00(CNST_TABLE_REF(3),
							BGl_string1834z00zzsaw_rtlz00, ((obj_t) BgL_bz00_5));
					}
				else
					{	/* SawMill/rtl.scm 41 */
						BFALSE;
					}
			}
			{	/* SawMill/rtl.scm 43 */
				obj_t BgL_sz00_1819;

				BgL_sz00_1819 =
					CAR((((BgL_blockz00_bglt) COBJECT(BgL_bz00_5))->BgL_succsz00));
				{	/* SawMill/rtl.scm 44 */
					obj_t BgL_g1369z00_1820;

					BgL_g1369z00_1820 =
						(((BgL_blockz00_bglt) COBJECT(BgL_bz00_5))->BgL_predsz00);
					{
						obj_t BgL_l1367z00_1822;

						BgL_l1367z00_1822 = BgL_g1369z00_1820;
					BgL_zc3z04anonymousza31486ze3z87_1823:
						if (PAIRP(BgL_l1367z00_1822))
							{	/* SawMill/rtl.scm 44 */
								{	/* SawMill/rtl.scm 44 */
									obj_t BgL_pz00_1825;

									BgL_pz00_1825 = CAR(BgL_l1367z00_1822);
									{
										obj_t BgL_auxz00_2280;

										{	/* SawMill/rtl.scm 45 */
											obj_t BgL_arg1489z00_1827;

											BgL_arg1489z00_1827 =
												(((BgL_blockz00_bglt) COBJECT(
														((BgL_blockz00_bglt) BgL_pz00_1825)))->
												BgL_succsz00);
											BgL_auxz00_2280 =
												BGl_substz00zzsaw_libz00(BgL_arg1489z00_1827,
												((obj_t) BgL_bz00_5), BgL_sz00_1819);
										}
										((((BgL_blockz00_bglt) COBJECT(
														((BgL_blockz00_bglt) BgL_pz00_1825)))->
												BgL_succsz00) = ((obj_t) BgL_auxz00_2280), BUNSPEC);
									}
								}
								{
									obj_t BgL_l1367z00_2287;

									BgL_l1367z00_2287 = CDR(BgL_l1367z00_1822);
									BgL_l1367z00_1822 = BgL_l1367z00_2287;
									goto BgL_zc3z04anonymousza31486ze3z87_1823;
								}
							}
						else
							{	/* SawMill/rtl.scm 44 */
								((bool_t) 1);
							}
					}
				}
				{	/* SawMill/rtl.scm 47 */
					obj_t BgL_arg1509z00_1830;

					{	/* SawMill/rtl.scm 47 */
						obj_t BgL_arg1513z00_1831;
						obj_t BgL_arg1514z00_1832;

						BgL_arg1513z00_1831 =
							(((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_sz00_1819)))->BgL_predsz00);
						BgL_arg1514z00_1832 =
							(((BgL_blockz00_bglt) COBJECT(BgL_bz00_5))->BgL_predsz00);
						BgL_arg1509z00_1830 =
							BGl_substzd2appendzd2zzsaw_libz00(BgL_arg1513z00_1831,
							((obj_t) BgL_bz00_5), BgL_arg1514z00_1832);
					}
					return
						((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_sz00_1819)))->BgL_predsz00) =
						((obj_t) BgL_arg1509z00_1830), BUNSPEC);
				}
			}
		}

	}



/* &unlink! */
	obj_t BGl_z62unlinkz12z70zzsaw_rtlz00(obj_t BgL_envz00_2184,
		obj_t BgL_bz00_2185)
	{
		{	/* SawMill/rtl.scm 39 */
			return BGl_unlinkz12z12zzsaw_rtlz00(((BgL_blockz00_bglt) BgL_bz00_2185));
		}

	}



/* rtl_dfs */
	BGL_EXPORTED_DEF obj_t BGl_rtl_dfsz00zzsaw_rtlz00(BgL_blockz00_bglt
		BgL_bz00_7, obj_t BgL_visitz00_8, obj_t BgL_visitzf3zf3_9)
	{
		{	/* SawMill/rtl.scm 89 */
			BGL_PROCEDURE_CALL1(BgL_visitz00_8, ((obj_t) BgL_bz00_7));
			{	/* SawMill/rtl.scm 91 */
				obj_t BgL_g1372z00_1838;

				BgL_g1372z00_1838 =
					(((BgL_blockz00_bglt) COBJECT(BgL_bz00_7))->BgL_succsz00);
				{
					obj_t BgL_l1370z00_1840;

					{	/* SawMill/rtl.scm 93 */
						bool_t BgL_tmpz00_2304;

						BgL_l1370z00_1840 = BgL_g1372z00_1838;
					BgL_zc3z04anonymousza31536ze3z87_1841:
						if (PAIRP(BgL_l1370z00_1840))
							{	/* SawMill/rtl.scm 93 */
								{	/* SawMill/rtl.scm 92 */
									obj_t BgL_succz00_1843;

									BgL_succz00_1843 = CAR(BgL_l1370z00_1840);
									if (CBOOL(BGL_PROCEDURE_CALL1(BgL_visitzf3zf3_9,
												BgL_succz00_1843)))
										{	/* SawMill/rtl.scm 92 */
											BFALSE;
										}
									else
										{	/* SawMill/rtl.scm 92 */
											BGl_rtl_dfsz00zzsaw_rtlz00(
												((BgL_blockz00_bglt) BgL_succz00_1843), BgL_visitz00_8,
												BgL_visitzf3zf3_9);
										}
								}
								{
									obj_t BgL_l1370z00_2316;

									BgL_l1370z00_2316 = CDR(BgL_l1370z00_1840);
									BgL_l1370z00_1840 = BgL_l1370z00_2316;
									goto BgL_zc3z04anonymousza31536ze3z87_1841;
								}
							}
						else
							{	/* SawMill/rtl.scm 93 */
								BgL_tmpz00_2304 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_2304);
					}
				}
			}
		}

	}



/* &rtl_dfs */
	obj_t BGl_z62rtl_dfsz62zzsaw_rtlz00(obj_t BgL_envz00_2186,
		obj_t BgL_bz00_2187, obj_t BgL_visitz00_2188, obj_t BgL_visitzf3zf3_2189)
	{
		{	/* SawMill/rtl.scm 89 */
			return
				BGl_rtl_dfsz00zzsaw_rtlz00(
				((BgL_blockz00_bglt) BgL_bz00_2187), BgL_visitz00_2188,
				BgL_visitzf3zf3_2189);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_rtlz00(void)
	{
		{	/* SawMill/rtl.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_rtlz00(void)
	{
		{	/* SawMill/rtl.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_rtlz00(void)
	{
		{	/* SawMill/rtl.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_rtlz00(void)
	{
		{	/* SawMill/rtl.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1835z00zzsaw_rtlz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1835z00zzsaw_rtlz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1835z00zzsaw_rtlz00));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string1835z00zzsaw_rtlz00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string1835z00zzsaw_rtlz00));
		}

	}

#ifdef __cplusplus
}
#endif
