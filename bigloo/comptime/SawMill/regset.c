/*===========================================================================*/
/*   (SawMill/regset.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/regset.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_REGSET_TYPE_DEFINITIONS
#define BGL_SAW_REGSET_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_regzf2razf2_bgl
	{
		int BgL_numz00;
		obj_t BgL_colorz00;
		obj_t BgL_coalescez00;
		int BgL_occurrencesz00;
		obj_t BgL_interferez00;
		obj_t BgL_interfere2z00;
	}                      *BgL_rtl_regzf2razf2_bglt;

	typedef struct BgL_regsetz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_lengthz00;
		int BgL_msiza7eza7;
		obj_t BgL_regvz00;
		obj_t BgL_reglz00;
		obj_t BgL_stringz00;
	}                *BgL_regsetz00_bglt;


#endif													// BGL_SAW_REGSET_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_regsetz00(obj_t, obj_t,
		obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda1768z62zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_regsetz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62regsetzd2addza2z12z00zzsaw_regsetz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1850z62zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1851z62zzsaw_regsetz00(obj_t, obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda1772z62zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1858z62zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1859z62zzsaw_regsetz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62shapezd2rtl_regzf2ra1424z42zzsaw_regsetz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_regsetz00 = BUNSPEC;
	static obj_t BGl_z62regsetzd2dumpzb0zzsaw_regsetz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62regsetzd2refzb0zzsaw_regsetz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_regsetz00(BgL_rtl_regz00_bglt);
	static BgL_regsetz00_bglt BGl_z62lambda1869z62zzsaw_regsetz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_regsetzd2addz12zc0zzsaw_regsetz00(BgL_regsetz00_bglt,
		BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_regsetz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31880ze3ze5zzsaw_regsetz00(obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62lambda1871z62zzsaw_regsetz00(obj_t);
	static obj_t BGl_z62lambda1878z62zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1879z62zzsaw_regsetz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62regsetzd2removez12za2zzsaw_regsetz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_regsetz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt BGl_makezd2regsetzd2zzsaw_regsetz00(int,
		int, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31873ze3ze5zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2ze3listz53zzsaw_regsetz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_regsetz00(BgL_rtl_regz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_regsetz00(BgL_typez00_bglt, obj_t, obj_t,
		obj_t, obj_t, obj_t, int, obj_t, obj_t, int, obj_t, obj_t);
	static obj_t BGl_z62lambda1885z62zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1886z62zzsaw_regsetz00(obj_t, obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzf2raz42zzsaw_regsetz00(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzsaw_regsetz00(void);
	static obj_t BGl_z62lambda1890z62zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1891z62zzsaw_regsetz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_regsetz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62lambda1895z62zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1896z62zzsaw_regsetz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62regsetzd2lengthzb0zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2reglzb0zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2unionza2z12z00zzsaw_regsetz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62regsetzd2regvzb0zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzsaw_regsetz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_regsetz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_regsetz00(obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62rtl_regzf2razd2nilz42zzsaw_regsetz00(obj_t);
	static obj_t BGl_z62regsetzd2emptyzf3z43zzsaw_regsetz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_regsetz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_objectzd2initzd2zzsaw_regsetz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_regsetz00(BgL_rtl_regz00_bglt);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_za2registerzd2cacheza2zd2zzsaw_regsetz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_regsetzd2filterzd2zzsaw_regsetz00(obj_t,
		BgL_regsetz00_bglt);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_regsetz00(BgL_rtl_regz00_bglt,
		obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	BGL_EXPORTED_DEF obj_t BGl_regsetz00zzsaw_regsetz00 = BUNSPEC;
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	static BgL_regsetz00_bglt
		BGl_z62makezd2emptyzd2regsetz62zzsaw_regsetz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_regsetzd2removez12zc0zzsaw_regsetz00(BgL_regsetz00_bglt,
		BgL_rtl_regz00_bglt);
	static obj_t BGl_z62regsetzf3z91zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2memberzf3z43zzsaw_regsetz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t make_vector(long, obj_t);
	static obj_t BGl_z62regsetzd2addz12za2zzsaw_regsetz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_regsetz00(void);
	static obj_t BGl_z62dumpzd2rtl_regzf2ra1426z42zzsaw_regsetz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_regsetzd2unionza2z12z62zzsaw_regsetz00(BgL_regsetz00_bglt, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_regsetz00(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_regsetz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_regsetz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_regsetz00(void);
	static BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_regsetz00(obj_t,
		obj_t);
	extern obj_t BGl_dumpz00zzsaw_defsz00(obj_t, obj_t, int);
	BGL_IMPORT obj_t make_string(long, unsigned char);
	BGL_IMPORT obj_t BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_regsetz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2stringzb0zzsaw_regsetz00(obj_t, obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2msiza7ez75zzsaw_regsetz00(BgL_regsetz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_regsetz00(obj_t,
		obj_t);
	static obj_t BGl_z62regsetzd2unionz12za2zzsaw_regsetz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_close_output_port(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_regsetzd2memberzf3z21zzsaw_regsetz00(BgL_rtl_regz00_bglt,
		BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_listzd2ze3regsetz31zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_regsetz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL int
		BGl_regsetzd2lengthzd2zzsaw_regsetz00(BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_regsetz00(BgL_regsetz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2emptyzf3z21zzsaw_regsetz00(BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL bool_t
		BGl_regsetzd2addza2z12z62zzsaw_regsetz00(BgL_regsetz00_bglt, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_regsetz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long,
		char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzsaw_registerzd2allocationzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_node2rtlz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_za2registerzd2cachezd2cachedza2z00zzsaw_regsetz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_regsetz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_regsetz00(BgL_rtl_regz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_rtl_regzf2razf3z01zzsaw_regsetz00(obj_t);
	static obj_t BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_regsetz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2ze3listz31zzsaw_regsetz00(BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_regsetz00(BgL_rtl_regz00_bglt,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_regsetz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_regsetz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_regsetz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_regsetz00(void);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_duplicatezd2regsetzd2zzsaw_regsetz00(BgL_regsetz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_regsetzd2forzd2eachz00zzsaw_regsetz00(obj_t,
		BgL_regsetz00_bglt);
	static obj_t BGl_z62regsetzd2forzd2eachz62zzsaw_regsetz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_regsetz00
		(BgL_rtl_regz00_bglt, int);
	BGL_IMPORT obj_t BGl_objectz00zz__objectz00;
	BGL_EXPORTED_DECL int
		BGl_rtl_regzf2razd2numz20zzsaw_regsetz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31547ze3ze5zzsaw_regsetz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_regsetz00(BgL_rtl_regz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_regsetzf3zf3zzsaw_regsetz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2dumpzd2zzsaw_regsetz00(BgL_regsetz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31750ze3ze5zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31904ze3ze5zzsaw_regsetz00(obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_regsetz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_regsetz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_regsetz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2stringzd2zzsaw_regsetz00(BgL_regsetz00_bglt);
	static obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_regsetz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_regsetzd2unionz12zc0zzsaw_regsetz00(BgL_regsetz00_bglt,
		BgL_regsetz00_bglt);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_regsetz00
		(BgL_rtl_regz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_regsetz00(BgL_rtl_regz00_bglt,
		obj_t);
	static obj_t BGl_z62lambda1807z62zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1808z62zzsaw_regsetz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_regsetz00_bglt BGl_regsetzd2nilzd2zzsaw_regsetz00(void);
	static obj_t BGl_z62regsetzd2filterzb0zzsaw_regsetz00(obj_t, obj_t, obj_t);
	static obj_t BGl_loopze70ze7zzsaw_regsetz00(obj_t, BgL_regsetz00_bglt, long);
	static obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_regsetz00(obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_rtl_regzf2razf2zzsaw_regsetz00 = BUNSPEC;
	BGL_EXPORTED_DECL BgL_regsetz00_bglt
		BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00(obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31826ze3ze5zzsaw_regsetz00(obj_t);
	static obj_t BGl_z62rtl_regzf2razf3z63zzsaw_regsetz00(obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62duplicatezd2regsetzb0zzsaw_regsetz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_regsetz00(BgL_rtl_regz00_bglt,
		BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2reglzd2zzsaw_regsetz00(BgL_regsetz00_bglt);
	static obj_t BGl_z62regsetzd2msiza7ez17zzsaw_regsetz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2regvzd2zzsaw_regsetz00(BgL_regsetz00_bglt);
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1902z62zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1903z62zzsaw_regsetz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_regsetz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1824z62zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1825z62zzsaw_regsetz00(obj_t, obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62listzd2ze3regsetz53zzsaw_regsetz00(obj_t,
		obj_t, obj_t);
	static BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_regsetz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_regsetz00(BgL_rtl_regz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31860ze3ze5zzsaw_regsetz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31852ze3ze5zzsaw_regsetz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31771ze3ze5zzsaw_regsetz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_regsetz00(BgL_regsetz00_bglt, int);
	static obj_t BGl_z62lambda1835z62zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1836z62zzsaw_regsetz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_regsetz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_regsetzd2refzd2zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31845ze3ze5zzsaw_regsetz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31837ze3ze5zzsaw_regsetz00(obj_t);
	static obj_t BGl_z62lambda1843z62zzsaw_regsetz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1844z62zzsaw_regsetz00(obj_t, obj_t, obj_t);
	static BgL_rtl_regz00_bglt BGl_z62lambda1763z62zzsaw_regsetz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t);
	static obj_t __cnst[20];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2removez12zd2envz12zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2remo2080z00,
		BGl_z62regsetzd2removez12za2zzsaw_regsetz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2034z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1808za7622081z00, BGl_z62lambda1808z62zzsaw_regsetz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2035z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1807za7622082z00, BGl_z62lambda1807z62zzsaw_regsetz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2036z00zzsaw_regsetz00,
		BgL_bgl_za762za7c3za704anonymo2083za7,
		BGl_z62zc3z04anonymousza31826ze3ze5zzsaw_regsetz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2037z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1825za7622084z00, BGl_z62lambda1825z62zzsaw_regsetz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2038z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1824za7622085z00, BGl_z62lambda1824z62zzsaw_regsetz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2039z00zzsaw_regsetz00,
		BgL_bgl_za762za7c3za704anonymo2086za7,
		BGl_z62zc3z04anonymousza31837ze3ze5zzsaw_regsetz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razd2namezd2envzf2zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72087za7,
		BGl_z62rtl_regzf2razd2namez42zzsaw_regsetz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2040z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1836za7622088z00, BGl_z62lambda1836z62zzsaw_regsetz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2041z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1835za7622089z00, BGl_z62lambda1835z62zzsaw_regsetz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2042z00zzsaw_regsetz00,
		BgL_bgl_za762za7c3za704anonymo2090za7,
		BGl_z62zc3z04anonymousza31845ze3ze5zzsaw_regsetz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2043z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1844za7622091z00, BGl_z62lambda1844z62zzsaw_regsetz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2044z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1843za7622092z00, BGl_z62lambda1843z62zzsaw_regsetz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2045z00zzsaw_regsetz00,
		BgL_bgl_za762za7c3za704anonymo2093za7,
		BGl_z62zc3z04anonymousza31852ze3ze5zzsaw_regsetz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2046z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1851za7622094z00, BGl_z62lambda1851z62zzsaw_regsetz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2047z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1850za7622095z00, BGl_z62lambda1850z62zzsaw_regsetz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2048z00zzsaw_regsetz00,
		BgL_bgl_za762za7c3za704anonymo2096za7,
		BGl_z62zc3z04anonymousza31860ze3ze5zzsaw_regsetz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2049z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1859za7622097z00, BGl_z62lambda1859z62zzsaw_regsetz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2050z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1858za7622098z00, BGl_z62lambda1858z62zzsaw_regsetz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2051z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1772za7622099z00, BGl_z62lambda1772z62zzsaw_regsetz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2052z00zzsaw_regsetz00,
		BgL_bgl_za762za7c3za704anonymo2100za7,
		BGl_z62zc3z04anonymousza31771ze3ze5zzsaw_regsetz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2053z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1768za7622101z00, BGl_z62lambda1768z62zzsaw_regsetz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2054z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1763za7622102z00, BGl_z62lambda1763z62zzsaw_regsetz00,
		0L, BUNSPEC, 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2055z00zzsaw_regsetz00,
		BgL_bgl_za762za7c3za704anonymo2103za7,
		BGl_z62zc3z04anonymousza31880ze3ze5zzsaw_regsetz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2056z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1879za7622104z00, BGl_z62lambda1879z62zzsaw_regsetz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2057z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1878za7622105z00, BGl_z62lambda1878z62zzsaw_regsetz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2058z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1886za7622106z00, BGl_z62lambda1886z62zzsaw_regsetz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2059z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1885za7622107z00, BGl_z62lambda1885z62zzsaw_regsetz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2nilzd2envz00zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2nilza72108za7,
		BGl_z62regsetzd2nilzb0zzsaw_regsetz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2060z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1891za7622109z00, BGl_z62lambda1891z62zzsaw_regsetz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2061z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1890za7622110z00, BGl_z62lambda1890z62zzsaw_regsetz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2062z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1896za7622111z00, BGl_z62lambda1896z62zzsaw_regsetz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2063z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1895za7622112z00, BGl_z62lambda1895z62zzsaw_regsetz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2070z00zzsaw_regsetz00,
		BgL_bgl_string2070za700za7za7s2113za7, "", 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2064z00zzsaw_regsetz00,
		BgL_bgl_za762za7c3za704anonymo2114za7,
		BGl_z62zc3z04anonymousza31904ze3ze5zzsaw_regsetz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2065z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1903za7622115z00, BGl_z62lambda1903z62zzsaw_regsetz00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2072z00zzsaw_regsetz00,
		BgL_bgl_string2072za700za7za7s2116za7, "shape", 5);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2066z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1902za7622117z00, BGl_z62lambda1902z62zzsaw_regsetz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2067z00zzsaw_regsetz00,
		BgL_bgl_za762za7c3za704anonymo2118za7,
		BGl_z62zc3z04anonymousza31873ze3ze5zzsaw_regsetz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2074z00zzsaw_regsetz00,
		BgL_bgl_string2074za700za7za7s2119za7, "dump", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2068z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1871za7622120z00, BGl_z62lambda1871z62zzsaw_regsetz00,
		0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2075z00zzsaw_regsetz00,
		BgL_bgl_string2075za700za7za7s2121za7, "=", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2069z00zzsaw_regsetz00,
		BgL_bgl_za762lambda1869za7622122z00, BGl_z62lambda1869z62zzsaw_regsetz00,
		0L, BUNSPEC, 5);
	      DEFINE_STRING(BGl_string2076z00zzsaw_regsetz00,
		BgL_bgl_string2076za700za7za7s2123za7, ":=", 2);
	      DEFINE_STRING(BGl_string2077z00zzsaw_regsetz00,
		BgL_bgl_string2077za700za7za7s2124za7, "saw_regset", 10);
	      DEFINE_STRING(BGl_string2078z00zzsaw_regsetz00,
		BgL_bgl_string2078za700za7za7s2125za7,
		"#() regset bstring string pair-nil regl vector regv msize length saw_regset rtl_reg/ra interfere2 interfere occurrences coalesce obj color int num ",
		147);
	extern obj_t BGl_dumpzd2envzd2zzsaw_defsz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2typezd2setz12zd2envz32zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72126za7,
		BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_regsetz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2071z00zzsaw_regsetz00,
		BgL_bgl_za762shapeza7d2rtl_r2127z00,
		BGl_z62shapezd2rtl_regzf2ra1424z42zzsaw_regsetz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2073z00zzsaw_regsetz00,
		BgL_bgl_za762dumpza7d2rtl_re2128z00,
		BGl_z62dumpzd2rtl_regzf2ra1426z42zzsaw_regsetz00, 0L, BUNSPEC, 3);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2addz12zd2envz12zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2addza72129za7,
		BGl_z62regsetzd2addz12za2zzsaw_regsetz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2envzf2zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72130za7,
		BGl_z62rtl_regzf2razd2interferez42zzsaw_regsetz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2emptyzf3zd2envzf3zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2empt2131z00,
		BGl_z62regsetzd2emptyzf3z43zzsaw_regsetz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2forzd2eachzd2envzd2zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2forza72132za7,
		BGl_z62regsetzd2forzd2eachz62zzsaw_regsetz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2memberzf3zd2envzf3zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2memb2133z00,
		BGl_z62regsetzd2memberzf3z43zzsaw_regsetz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2setz12zd2envzc1zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72134za7,
		BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_regsetz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2setz12zd2envz32zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72135za7,
		BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_regsetz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2envzf2zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72136za7,
		BGl_z62rtl_regzf2razd2colorz42zzsaw_regsetz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interferezd2setz12zd2envz32zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72137za7,
		BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_regsetz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2regvzd2envz00zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2regv2138z00, BGl_z62regsetzd2regvzb0zzsaw_regsetz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2lengthzd2setz12zd2envzc0zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2leng2139z00,
		BGl_z62regsetzd2lengthzd2setz12z70zzsaw_regsetz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2msiza7ezd2envza7zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2msiza72140za7,
		BGl_z62regsetzd2msiza7ez17zzsaw_regsetz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_shapezd2envzd2zztools_shapez00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razd2typezd2envzf2zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72141za7,
		BGl_z62rtl_regzf2razd2typez42zzsaw_regsetz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2varzd2setz12zd2envz32zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72142za7,
		BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_regsetz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2occurrenceszd2envzf2zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72143za7,
		BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_regsetz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2dumpzd2envz00zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2dump2144z00, va_generic_entry,
		BGl_z62regsetzd2dumpzb0zzsaw_regsetz00, BUNSPEC, -2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2refzd2envz00zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2refza72145za7,
		BGl_z62regsetzd2refzb0zzsaw_regsetz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razd2nilzd2envzf2zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72146za7,
		BGl_z62rtl_regzf2razd2nilz42zzsaw_regsetz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razd2varzd2envzf2zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72147za7,
		BGl_z62rtl_regzf2razd2varz42zzsaw_regsetz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razd2numzd2envzf2zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72148za7,
		BGl_z62rtl_regzf2razd2numz42zzsaw_regsetz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2stringzd2setz12zd2envzc0zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2stri2149z00,
		BGl_z62regsetzd2stringzd2setz12z70zzsaw_regsetz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2colorzd2setz12zd2envz32zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72150za7,
		BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_regsetz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2onexprzf3zd2envz01zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72151za7,
		BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_regsetz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2stringzd2envz00zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2stri2152z00,
		BGl_z62regsetzd2stringzb0zzsaw_regsetz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2setz12zd2envz32zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72153za7,
		BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_regsetz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_listzd2ze3regsetzd2envze3zzsaw_regsetz00,
		BgL_bgl_za762listza7d2za7e3reg2154za7,
		BGl_z62listzd2ze3regsetz53zzsaw_regsetz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2rtl_regzf2razd2envzf2zzsaw_regsetz00,
		BgL_bgl_za762makeza7d2rtl_re2155z00,
		BGl_z62makezd2rtl_regzf2raz42zzsaw_regsetz00, 0L, BUNSPEC, 12);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razf3zd2envzd3zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72156za7,
		BGl_z62rtl_regzf2razf3z63zzsaw_regsetz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2lengthzd2envz00zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2leng2157z00,
		BGl_z62regsetzd2lengthzb0zzsaw_regsetz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2envzf2zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72158za7,
		BGl_z62rtl_regzf2razd2coalescez42zzsaw_regsetz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2interfere2zd2envzf2zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72159za7,
		BGl_z62rtl_regzf2razd2interfere2z42zzsaw_regsetz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_regsetzd2unionza2z12zd2envzb0zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2unio2160z00,
		BGl_z62regsetzd2unionza2z12z00zzsaw_regsetz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2hardwarezd2envzf2zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72161za7,
		BGl_z62rtl_regzf2razd2hardwarez42zzsaw_regsetz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2regsetzd2envz00zzsaw_regsetz00,
		BgL_bgl_za762makeza7d2regset2162z00, BGl_z62makezd2regsetzb0zzsaw_regsetz00,
		0L, BUNSPEC, 5);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2ze3listzd2envze3zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2za7e3l2163za7,
		BGl_z62regsetzd2ze3listz53zzsaw_regsetz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2filterzd2envz00zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2filt2164z00,
		BGl_z62regsetzd2filterzb0zzsaw_regsetz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzf3zd2envz21zzsaw_regsetz00,
		BgL_bgl_za762regsetza7f3za791za72165z00, BGl_z62regsetzf3z91zzsaw_regsetz00,
		0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_rtl_regzf2razd2keyzd2envzf2zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72166za7,
		BGl_z62rtl_regzf2razd2keyz42zzsaw_regsetz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2addza2z12zd2envzb0zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2addza72167za7,
		BGl_z62regsetzd2addza2z12z00zzsaw_regsetz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_duplicatezd2regsetzd2envz00zzsaw_regsetz00,
		BgL_bgl_za762duplicateza7d2r2168z00,
		BGl_z62duplicatezd2regsetzb0zzsaw_regsetz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2unionz12zd2envz12zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2unio2169z00,
		BGl_z62regsetzd2unionz12za2zzsaw_regsetz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_rtl_regzf2razd2coalescezd2setz12zd2envz32zzsaw_regsetz00,
		BgL_bgl_za762rtl_regza7f2raza72170za7,
		BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_regsetz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2031z00zzsaw_regsetz00,
		BgL_bgl_string2031za700za7za7s2171za7, "{", 1);
	      DEFINE_STRING(BGl_string2032z00zzsaw_regsetz00,
		BgL_bgl_string2032za700za7za7s2172za7, "}", 1);
	      DEFINE_STRING(BGl_string2033z00zzsaw_regsetz00,
		BgL_bgl_string2033za700za7za7s2173za7, " ", 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2emptyzd2regsetzd2envzd2zzsaw_regsetz00,
		BgL_bgl_za762makeza7d2emptyza72174za7,
		BGl_z62makezd2emptyzd2regsetz62zzsaw_regsetz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_regsetzd2reglzd2envz00zzsaw_regsetz00,
		BgL_bgl_za762regsetza7d2regl2175z00, BGl_z62regsetzd2reglzb0zzsaw_regsetz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_regsetz00));
		     ADD_ROOT((void *) (&BGl_za2registerzd2cacheza2zd2zzsaw_regsetz00));
		     ADD_ROOT((void *) (&BGl_regsetz00zzsaw_regsetz00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2registerzd2cachezd2cachedza2z00zzsaw_regsetz00));
		     ADD_ROOT((void *) (&BGl_rtl_regzf2razf2zzsaw_regsetz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_regsetz00(long
		BgL_checksumz00_3785, char *BgL_fromz00_3786)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_regsetz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_regsetz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_regsetz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_regsetz00();
					BGl_cnstzd2initzd2zzsaw_regsetz00();
					BGl_importedzd2moduleszd2initz00zzsaw_regsetz00();
					BGl_objectzd2initzd2zzsaw_regsetz00();
					BGl_methodzd2initzd2zzsaw_regsetz00();
					return BGl_toplevelzd2initzd2zzsaw_regsetz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_regsetz00(void)
	{
		{	/* SawMill/regset.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_regset");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "saw_regset");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_regset");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_regset");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "saw_regset");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_regset");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "saw_regset");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L,
				"saw_regset");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "saw_regset");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_regset");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_regset");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_regset");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_regsetz00(void)
	{
		{	/* SawMill/regset.scm 15 */
			{	/* SawMill/regset.scm 15 */
				obj_t BgL_cportz00_3634;

				{	/* SawMill/regset.scm 15 */
					obj_t BgL_stringz00_3641;

					BgL_stringz00_3641 = BGl_string2078z00zzsaw_regsetz00;
					{	/* SawMill/regset.scm 15 */
						obj_t BgL_startz00_3642;

						BgL_startz00_3642 = BINT(0L);
						{	/* SawMill/regset.scm 15 */
							obj_t BgL_endz00_3643;

							BgL_endz00_3643 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3641)));
							{	/* SawMill/regset.scm 15 */

								BgL_cportz00_3634 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3641, BgL_startz00_3642, BgL_endz00_3643);
				}}}}
				{
					long BgL_iz00_3635;

					BgL_iz00_3635 = 19L;
				BgL_loopz00_3636:
					if ((BgL_iz00_3635 == -1L))
						{	/* SawMill/regset.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* SawMill/regset.scm 15 */
							{	/* SawMill/regset.scm 15 */
								obj_t BgL_arg2079z00_3637;

								{	/* SawMill/regset.scm 15 */

									{	/* SawMill/regset.scm 15 */
										obj_t BgL_locationz00_3639;

										BgL_locationz00_3639 = BBOOL(((bool_t) 0));
										{	/* SawMill/regset.scm 15 */

											BgL_arg2079z00_3637 =
												BGl_readz00zz__readerz00(BgL_cportz00_3634,
												BgL_locationz00_3639);
										}
									}
								}
								{	/* SawMill/regset.scm 15 */
									int BgL_tmpz00_3818;

									BgL_tmpz00_3818 = (int) (BgL_iz00_3635);
									CNST_TABLE_SET(BgL_tmpz00_3818, BgL_arg2079z00_3637);
							}}
							{	/* SawMill/regset.scm 15 */
								int BgL_auxz00_3640;

								BgL_auxz00_3640 = (int) ((BgL_iz00_3635 - 1L));
								{
									long BgL_iz00_3823;

									BgL_iz00_3823 = (long) (BgL_auxz00_3640);
									BgL_iz00_3635 = BgL_iz00_3823;
									goto BgL_loopz00_3636;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_regsetz00(void)
	{
		{	/* SawMill/regset.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_regsetz00(void)
	{
		{	/* SawMill/regset.scm 15 */
			BGl_za2registerzd2cacheza2zd2zzsaw_regsetz00 = BFALSE;
			BGl_za2registerzd2cachezd2cachedza2z00zzsaw_regsetz00 = BFALSE;
			return BUNSPEC;
		}

	}



/* make-rtl_reg/ra */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_makezd2rtl_regzf2raz20zzsaw_regsetz00(BgL_typez00_bglt
		BgL_type1179z00_3, obj_t BgL_var1180z00_4, obj_t BgL_onexprzf31181zf3_5,
		obj_t BgL_name1182z00_6, obj_t BgL_key1183z00_7,
		obj_t BgL_hardware1184z00_8, int BgL_num1185z00_9,
		obj_t BgL_color1186z00_10, obj_t BgL_coalesce1187z00_11,
		int BgL_occurrences1188z00_12, obj_t BgL_interfere1189z00_13,
		obj_t BgL_interfere21190z00_14)
	{
		{	/* SawMill/regset.sch 55 */
			{	/* SawMill/regset.sch 55 */
				BgL_rtl_regz00_bglt BgL_new1179z00_3645;

				{	/* SawMill/regset.sch 55 */
					BgL_rtl_regz00_bglt BgL_tmp1177z00_3646;
					BgL_rtl_regzf2razf2_bglt BgL_wide1178z00_3647;

					{
						BgL_rtl_regz00_bglt BgL_auxz00_3826;

						{	/* SawMill/regset.sch 55 */
							BgL_rtl_regz00_bglt BgL_new1176z00_3648;

							BgL_new1176z00_3648 =
								((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_rtl_regz00_bgl))));
							{	/* SawMill/regset.sch 55 */
								long BgL_arg1453z00_3649;

								BgL_arg1453z00_3649 =
									BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1176z00_3648),
									BgL_arg1453z00_3649);
							}
							{	/* SawMill/regset.sch 55 */
								BgL_objectz00_bglt BgL_tmpz00_3831;

								BgL_tmpz00_3831 = ((BgL_objectz00_bglt) BgL_new1176z00_3648);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3831, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1176z00_3648);
							BgL_auxz00_3826 = BgL_new1176z00_3648;
						}
						BgL_tmp1177z00_3646 = ((BgL_rtl_regz00_bglt) BgL_auxz00_3826);
					}
					BgL_wide1178z00_3647 =
						((BgL_rtl_regzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_rtl_regzf2razf2_bgl))));
					{	/* SawMill/regset.sch 55 */
						obj_t BgL_auxz00_3839;
						BgL_objectz00_bglt BgL_tmpz00_3837;

						BgL_auxz00_3839 = ((obj_t) BgL_wide1178z00_3647);
						BgL_tmpz00_3837 = ((BgL_objectz00_bglt) BgL_tmp1177z00_3646);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3837, BgL_auxz00_3839);
					}
					((BgL_objectz00_bglt) BgL_tmp1177z00_3646);
					{	/* SawMill/regset.sch 55 */
						long BgL_arg1448z00_3650;

						BgL_arg1448z00_3650 =
							BGL_CLASS_NUM(BGl_rtl_regzf2razf2zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1177z00_3646), BgL_arg1448z00_3650);
					}
					BgL_new1179z00_3645 = ((BgL_rtl_regz00_bglt) BgL_tmp1177z00_3646);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt) BgL_new1179z00_3645)))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_type1179z00_3), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1179z00_3645)))->BgL_varz00) =
					((obj_t) BgL_var1180z00_4), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1179z00_3645)))->BgL_onexprzf3zf3) =
					((obj_t) BgL_onexprzf31181zf3_5), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1179z00_3645)))->BgL_namez00) =
					((obj_t) BgL_name1182z00_6), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1179z00_3645)))->BgL_keyz00) =
					((obj_t) BgL_key1183z00_7), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1179z00_3645)))->BgL_debugnamez00) =
					((obj_t) BFALSE), BUNSPEC);
				((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
									BgL_new1179z00_3645)))->BgL_hardwarez00) =
					((obj_t) BgL_hardware1184z00_8), BUNSPEC);
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3861;

					{
						obj_t BgL_auxz00_3862;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3863;

							BgL_tmpz00_3863 = ((BgL_objectz00_bglt) BgL_new1179z00_3645);
							BgL_auxz00_3862 = BGL_OBJECT_WIDENING(BgL_tmpz00_3863);
						}
						BgL_auxz00_3861 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3862);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3861))->BgL_numz00) =
						((int) BgL_num1185z00_9), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3868;

					{
						obj_t BgL_auxz00_3869;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3870;

							BgL_tmpz00_3870 = ((BgL_objectz00_bglt) BgL_new1179z00_3645);
							BgL_auxz00_3869 = BGL_OBJECT_WIDENING(BgL_tmpz00_3870);
						}
						BgL_auxz00_3868 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3869);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3868))->
							BgL_colorz00) = ((obj_t) BgL_color1186z00_10), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3875;

					{
						obj_t BgL_auxz00_3876;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3877;

							BgL_tmpz00_3877 = ((BgL_objectz00_bglt) BgL_new1179z00_3645);
							BgL_auxz00_3876 = BGL_OBJECT_WIDENING(BgL_tmpz00_3877);
						}
						BgL_auxz00_3875 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3876);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3875))->
							BgL_coalescez00) = ((obj_t) BgL_coalesce1187z00_11), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3882;

					{
						obj_t BgL_auxz00_3883;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3884;

							BgL_tmpz00_3884 = ((BgL_objectz00_bglt) BgL_new1179z00_3645);
							BgL_auxz00_3883 = BGL_OBJECT_WIDENING(BgL_tmpz00_3884);
						}
						BgL_auxz00_3882 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3883);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3882))->
							BgL_occurrencesz00) = ((int) BgL_occurrences1188z00_12), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3889;

					{
						obj_t BgL_auxz00_3890;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3891;

							BgL_tmpz00_3891 = ((BgL_objectz00_bglt) BgL_new1179z00_3645);
							BgL_auxz00_3890 = BGL_OBJECT_WIDENING(BgL_tmpz00_3891);
						}
						BgL_auxz00_3889 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3890);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3889))->
							BgL_interferez00) = ((obj_t) BgL_interfere1189z00_13), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_3896;

					{
						obj_t BgL_auxz00_3897;

						{	/* SawMill/regset.sch 55 */
							BgL_objectz00_bglt BgL_tmpz00_3898;

							BgL_tmpz00_3898 = ((BgL_objectz00_bglt) BgL_new1179z00_3645);
							BgL_auxz00_3897 = BGL_OBJECT_WIDENING(BgL_tmpz00_3898);
						}
						BgL_auxz00_3896 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3897);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3896))->
							BgL_interfere2z00) = ((obj_t) BgL_interfere21190z00_14), BUNSPEC);
				}
				return BgL_new1179z00_3645;
			}
		}

	}



/* &make-rtl_reg/ra */
	BgL_rtl_regz00_bglt BGl_z62makezd2rtl_regzf2raz42zzsaw_regsetz00(obj_t
		BgL_envz00_3348, obj_t BgL_type1179z00_3349, obj_t BgL_var1180z00_3350,
		obj_t BgL_onexprzf31181zf3_3351, obj_t BgL_name1182z00_3352,
		obj_t BgL_key1183z00_3353, obj_t BgL_hardware1184z00_3354,
		obj_t BgL_num1185z00_3355, obj_t BgL_color1186z00_3356,
		obj_t BgL_coalesce1187z00_3357, obj_t BgL_occurrences1188z00_3358,
		obj_t BgL_interfere1189z00_3359, obj_t BgL_interfere21190z00_3360)
	{
		{	/* SawMill/regset.sch 55 */
			return
				BGl_makezd2rtl_regzf2raz20zzsaw_regsetz00(
				((BgL_typez00_bglt) BgL_type1179z00_3349), BgL_var1180z00_3350,
				BgL_onexprzf31181zf3_3351, BgL_name1182z00_3352, BgL_key1183z00_3353,
				BgL_hardware1184z00_3354, CINT(BgL_num1185z00_3355),
				BgL_color1186z00_3356, BgL_coalesce1187z00_3357,
				CINT(BgL_occurrences1188z00_3358), BgL_interfere1189z00_3359,
				BgL_interfere21190z00_3360);
		}

	}



/* rtl_reg/ra? */
	BGL_EXPORTED_DEF bool_t BGl_rtl_regzf2razf3z01zzsaw_regsetz00(obj_t
		BgL_objz00_15)
	{
		{	/* SawMill/regset.sch 56 */
			{	/* SawMill/regset.sch 56 */
				obj_t BgL_classz00_3651;

				BgL_classz00_3651 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_15))
					{	/* SawMill/regset.sch 56 */
						BgL_objectz00_bglt BgL_arg1807z00_3652;

						BgL_arg1807z00_3652 = (BgL_objectz00_bglt) (BgL_objz00_15);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 56 */
								long BgL_idxz00_3653;

								BgL_idxz00_3653 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3652);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3653 + 2L)) == BgL_classz00_3651);
							}
						else
							{	/* SawMill/regset.sch 56 */
								bool_t BgL_res2021z00_3656;

								{	/* SawMill/regset.sch 56 */
									obj_t BgL_oclassz00_3657;

									{	/* SawMill/regset.sch 56 */
										obj_t BgL_arg1815z00_3658;
										long BgL_arg1816z00_3659;

										BgL_arg1815z00_3658 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 56 */
											long BgL_arg1817z00_3660;

											BgL_arg1817z00_3660 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3652);
											BgL_arg1816z00_3659 = (BgL_arg1817z00_3660 - OBJECT_TYPE);
										}
										BgL_oclassz00_3657 =
											VECTOR_REF(BgL_arg1815z00_3658, BgL_arg1816z00_3659);
									}
									{	/* SawMill/regset.sch 56 */
										bool_t BgL__ortest_1115z00_3661;

										BgL__ortest_1115z00_3661 =
											(BgL_classz00_3651 == BgL_oclassz00_3657);
										if (BgL__ortest_1115z00_3661)
											{	/* SawMill/regset.sch 56 */
												BgL_res2021z00_3656 = BgL__ortest_1115z00_3661;
											}
										else
											{	/* SawMill/regset.sch 56 */
												long BgL_odepthz00_3662;

												{	/* SawMill/regset.sch 56 */
													obj_t BgL_arg1804z00_3663;

													BgL_arg1804z00_3663 = (BgL_oclassz00_3657);
													BgL_odepthz00_3662 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3663);
												}
												if ((2L < BgL_odepthz00_3662))
													{	/* SawMill/regset.sch 56 */
														obj_t BgL_arg1802z00_3664;

														{	/* SawMill/regset.sch 56 */
															obj_t BgL_arg1803z00_3665;

															BgL_arg1803z00_3665 = (BgL_oclassz00_3657);
															BgL_arg1802z00_3664 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3665,
																2L);
														}
														BgL_res2021z00_3656 =
															(BgL_arg1802z00_3664 == BgL_classz00_3651);
													}
												else
													{	/* SawMill/regset.sch 56 */
														BgL_res2021z00_3656 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2021z00_3656;
							}
					}
				else
					{	/* SawMill/regset.sch 56 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &rtl_reg/ra? */
	obj_t BGl_z62rtl_regzf2razf3z63zzsaw_regsetz00(obj_t BgL_envz00_3361,
		obj_t BgL_objz00_3362)
	{
		{	/* SawMill/regset.sch 56 */
			return BBOOL(BGl_rtl_regzf2razf3z01zzsaw_regsetz00(BgL_objz00_3362));
		}

	}



/* rtl_reg/ra-nil */
	BGL_EXPORTED_DEF BgL_rtl_regz00_bglt
		BGl_rtl_regzf2razd2nilz20zzsaw_regsetz00(void)
	{
		{	/* SawMill/regset.sch 57 */
			{	/* SawMill/regset.sch 57 */
				obj_t BgL_classz00_2758;

				BgL_classz00_2758 = BGl_rtl_regzf2razf2zzsaw_regsetz00;
				{	/* SawMill/regset.sch 57 */
					obj_t BgL__ortest_1117z00_2759;

					BgL__ortest_1117z00_2759 = BGL_CLASS_NIL(BgL_classz00_2758);
					if (CBOOL(BgL__ortest_1117z00_2759))
						{	/* SawMill/regset.sch 57 */
							return ((BgL_rtl_regz00_bglt) BgL__ortest_1117z00_2759);
						}
					else
						{	/* SawMill/regset.sch 57 */
							return
								((BgL_rtl_regz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2758));
						}
				}
			}
		}

	}



/* &rtl_reg/ra-nil */
	BgL_rtl_regz00_bglt BGl_z62rtl_regzf2razd2nilz42zzsaw_regsetz00(obj_t
		BgL_envz00_3363)
	{
		{	/* SawMill/regset.sch 57 */
			return BGl_rtl_regzf2razd2nilz20zzsaw_regsetz00();
		}

	}



/* rtl_reg/ra-interfere2 */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2z20zzsaw_regsetz00(BgL_rtl_regz00_bglt
		BgL_oz00_16)
	{
		{	/* SawMill/regset.sch 58 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3938;

				{
					obj_t BgL_auxz00_3939;

					{	/* SawMill/regset.sch 58 */
						BgL_objectz00_bglt BgL_tmpz00_3940;

						BgL_tmpz00_3940 = ((BgL_objectz00_bglt) BgL_oz00_16);
						BgL_auxz00_3939 = BGL_OBJECT_WIDENING(BgL_tmpz00_3940);
					}
					BgL_auxz00_3938 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3939);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3938))->
					BgL_interfere2z00);
			}
		}

	}



/* &rtl_reg/ra-interfere2 */
	obj_t BGl_z62rtl_regzf2razd2interfere2z42zzsaw_regsetz00(obj_t
		BgL_envz00_3364, obj_t BgL_oz00_3365)
	{
		{	/* SawMill/regset.sch 58 */
			return
				BGl_rtl_regzf2razd2interfere2z20zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3365));
		}

	}



/* rtl_reg/ra-interfere2-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_regsetz00(BgL_rtl_regz00_bglt
		BgL_oz00_17, obj_t BgL_vz00_18)
	{
		{	/* SawMill/regset.sch 59 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3947;

				{
					obj_t BgL_auxz00_3948;

					{	/* SawMill/regset.sch 59 */
						BgL_objectz00_bglt BgL_tmpz00_3949;

						BgL_tmpz00_3949 = ((BgL_objectz00_bglt) BgL_oz00_17);
						BgL_auxz00_3948 = BGL_OBJECT_WIDENING(BgL_tmpz00_3949);
					}
					BgL_auxz00_3947 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3948);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3947))->
						BgL_interfere2z00) = ((obj_t) BgL_vz00_18), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere2-set! */
	obj_t BGl_z62rtl_regzf2razd2interfere2zd2setz12z82zzsaw_regsetz00(obj_t
		BgL_envz00_3366, obj_t BgL_oz00_3367, obj_t BgL_vz00_3368)
	{
		{	/* SawMill/regset.sch 59 */
			return
				BGl_rtl_regzf2razd2interfere2zd2setz12ze0zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3367), BgL_vz00_3368);
		}

	}



/* rtl_reg/ra-interfere */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferez20zzsaw_regsetz00(BgL_rtl_regz00_bglt
		BgL_oz00_19)
	{
		{	/* SawMill/regset.sch 60 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3956;

				{
					obj_t BgL_auxz00_3957;

					{	/* SawMill/regset.sch 60 */
						BgL_objectz00_bglt BgL_tmpz00_3958;

						BgL_tmpz00_3958 = ((BgL_objectz00_bglt) BgL_oz00_19);
						BgL_auxz00_3957 = BGL_OBJECT_WIDENING(BgL_tmpz00_3958);
					}
					BgL_auxz00_3956 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3957);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3956))->
					BgL_interferez00);
			}
		}

	}



/* &rtl_reg/ra-interfere */
	obj_t BGl_z62rtl_regzf2razd2interferez42zzsaw_regsetz00(obj_t BgL_envz00_3369,
		obj_t BgL_oz00_3370)
	{
		{	/* SawMill/regset.sch 60 */
			return
				BGl_rtl_regzf2razd2interferez20zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3370));
		}

	}



/* rtl_reg/ra-interfere-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_regsetz00(BgL_rtl_regz00_bglt
		BgL_oz00_20, obj_t BgL_vz00_21)
	{
		{	/* SawMill/regset.sch 61 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3965;

				{
					obj_t BgL_auxz00_3966;

					{	/* SawMill/regset.sch 61 */
						BgL_objectz00_bglt BgL_tmpz00_3967;

						BgL_tmpz00_3967 = ((BgL_objectz00_bglt) BgL_oz00_20);
						BgL_auxz00_3966 = BGL_OBJECT_WIDENING(BgL_tmpz00_3967);
					}
					BgL_auxz00_3965 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3966);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3965))->
						BgL_interferez00) = ((obj_t) BgL_vz00_21), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-interfere-set! */
	obj_t BGl_z62rtl_regzf2razd2interferezd2setz12z82zzsaw_regsetz00(obj_t
		BgL_envz00_3371, obj_t BgL_oz00_3372, obj_t BgL_vz00_3373)
	{
		{	/* SawMill/regset.sch 61 */
			return
				BGl_rtl_regzf2razd2interferezd2setz12ze0zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3372), BgL_vz00_3373);
		}

	}



/* rtl_reg/ra-occurrences */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2occurrencesz20zzsaw_regsetz00(BgL_rtl_regz00_bglt
		BgL_oz00_22)
	{
		{	/* SawMill/regset.sch 62 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3974;

				{
					obj_t BgL_auxz00_3975;

					{	/* SawMill/regset.sch 62 */
						BgL_objectz00_bglt BgL_tmpz00_3976;

						BgL_tmpz00_3976 = ((BgL_objectz00_bglt) BgL_oz00_22);
						BgL_auxz00_3975 = BGL_OBJECT_WIDENING(BgL_tmpz00_3976);
					}
					BgL_auxz00_3974 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3975);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3974))->
					BgL_occurrencesz00);
			}
		}

	}



/* &rtl_reg/ra-occurrences */
	obj_t BGl_z62rtl_regzf2razd2occurrencesz42zzsaw_regsetz00(obj_t
		BgL_envz00_3374, obj_t BgL_oz00_3375)
	{
		{	/* SawMill/regset.sch 62 */
			return
				BINT(BGl_rtl_regzf2razd2occurrencesz20zzsaw_regsetz00(
					((BgL_rtl_regz00_bglt) BgL_oz00_3375)));
		}

	}



/* rtl_reg/ra-occurrences-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_regsetz00
		(BgL_rtl_regz00_bglt BgL_oz00_23, int BgL_vz00_24)
	{
		{	/* SawMill/regset.sch 63 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3984;

				{
					obj_t BgL_auxz00_3985;

					{	/* SawMill/regset.sch 63 */
						BgL_objectz00_bglt BgL_tmpz00_3986;

						BgL_tmpz00_3986 = ((BgL_objectz00_bglt) BgL_oz00_23);
						BgL_auxz00_3985 = BGL_OBJECT_WIDENING(BgL_tmpz00_3986);
					}
					BgL_auxz00_3984 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3985);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3984))->
						BgL_occurrencesz00) = ((int) BgL_vz00_24), BUNSPEC);
		}}

	}



/* &rtl_reg/ra-occurrences-set! */
	obj_t BGl_z62rtl_regzf2razd2occurrenceszd2setz12z82zzsaw_regsetz00(obj_t
		BgL_envz00_3376, obj_t BgL_oz00_3377, obj_t BgL_vz00_3378)
	{
		{	/* SawMill/regset.sch 63 */
			return
				BGl_rtl_regzf2razd2occurrenceszd2setz12ze0zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3377), CINT(BgL_vz00_3378));
		}

	}



/* rtl_reg/ra-coalesce */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescez20zzsaw_regsetz00(BgL_rtl_regz00_bglt
		BgL_oz00_25)
	{
		{	/* SawMill/regset.sch 64 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_3994;

				{
					obj_t BgL_auxz00_3995;

					{	/* SawMill/regset.sch 64 */
						BgL_objectz00_bglt BgL_tmpz00_3996;

						BgL_tmpz00_3996 = ((BgL_objectz00_bglt) BgL_oz00_25);
						BgL_auxz00_3995 = BGL_OBJECT_WIDENING(BgL_tmpz00_3996);
					}
					BgL_auxz00_3994 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_3995);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_3994))->
					BgL_coalescez00);
			}
		}

	}



/* &rtl_reg/ra-coalesce */
	obj_t BGl_z62rtl_regzf2razd2coalescez42zzsaw_regsetz00(obj_t BgL_envz00_3379,
		obj_t BgL_oz00_3380)
	{
		{	/* SawMill/regset.sch 64 */
			return
				BGl_rtl_regzf2razd2coalescez20zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3380));
		}

	}



/* rtl_reg/ra-coalesce-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_regsetz00(BgL_rtl_regz00_bglt
		BgL_oz00_26, obj_t BgL_vz00_27)
	{
		{	/* SawMill/regset.sch 65 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4003;

				{
					obj_t BgL_auxz00_4004;

					{	/* SawMill/regset.sch 65 */
						BgL_objectz00_bglt BgL_tmpz00_4005;

						BgL_tmpz00_4005 = ((BgL_objectz00_bglt) BgL_oz00_26);
						BgL_auxz00_4004 = BGL_OBJECT_WIDENING(BgL_tmpz00_4005);
					}
					BgL_auxz00_4003 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4004);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4003))->
						BgL_coalescez00) = ((obj_t) BgL_vz00_27), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-coalesce-set! */
	obj_t BGl_z62rtl_regzf2razd2coalescezd2setz12z82zzsaw_regsetz00(obj_t
		BgL_envz00_3381, obj_t BgL_oz00_3382, obj_t BgL_vz00_3383)
	{
		{	/* SawMill/regset.sch 65 */
			return
				BGl_rtl_regzf2razd2coalescezd2setz12ze0zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3382), BgL_vz00_3383);
		}

	}



/* rtl_reg/ra-color */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorz20zzsaw_regsetz00(BgL_rtl_regz00_bglt BgL_oz00_28)
	{
		{	/* SawMill/regset.sch 66 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4012;

				{
					obj_t BgL_auxz00_4013;

					{	/* SawMill/regset.sch 66 */
						BgL_objectz00_bglt BgL_tmpz00_4014;

						BgL_tmpz00_4014 = ((BgL_objectz00_bglt) BgL_oz00_28);
						BgL_auxz00_4013 = BGL_OBJECT_WIDENING(BgL_tmpz00_4014);
					}
					BgL_auxz00_4012 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4013);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4012))->BgL_colorz00);
			}
		}

	}



/* &rtl_reg/ra-color */
	obj_t BGl_z62rtl_regzf2razd2colorz42zzsaw_regsetz00(obj_t BgL_envz00_3384,
		obj_t BgL_oz00_3385)
	{
		{	/* SawMill/regset.sch 66 */
			return
				BGl_rtl_regzf2razd2colorz20zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3385));
		}

	}



/* rtl_reg/ra-color-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_regsetz00(BgL_rtl_regz00_bglt
		BgL_oz00_29, obj_t BgL_vz00_30)
	{
		{	/* SawMill/regset.sch 67 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4021;

				{
					obj_t BgL_auxz00_4022;

					{	/* SawMill/regset.sch 67 */
						BgL_objectz00_bglt BgL_tmpz00_4023;

						BgL_tmpz00_4023 = ((BgL_objectz00_bglt) BgL_oz00_29);
						BgL_auxz00_4022 = BGL_OBJECT_WIDENING(BgL_tmpz00_4023);
					}
					BgL_auxz00_4021 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4022);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4021))->
						BgL_colorz00) = ((obj_t) BgL_vz00_30), BUNSPEC);
			}
		}

	}



/* &rtl_reg/ra-color-set! */
	obj_t BGl_z62rtl_regzf2razd2colorzd2setz12z82zzsaw_regsetz00(obj_t
		BgL_envz00_3386, obj_t BgL_oz00_3387, obj_t BgL_vz00_3388)
	{
		{	/* SawMill/regset.sch 67 */
			return
				BGl_rtl_regzf2razd2colorzd2setz12ze0zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3387), BgL_vz00_3388);
		}

	}



/* rtl_reg/ra-num */
	BGL_EXPORTED_DEF int
		BGl_rtl_regzf2razd2numz20zzsaw_regsetz00(BgL_rtl_regz00_bglt BgL_oz00_31)
	{
		{	/* SawMill/regset.sch 68 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4030;

				{
					obj_t BgL_auxz00_4031;

					{	/* SawMill/regset.sch 68 */
						BgL_objectz00_bglt BgL_tmpz00_4032;

						BgL_tmpz00_4032 = ((BgL_objectz00_bglt) BgL_oz00_31);
						BgL_auxz00_4031 = BGL_OBJECT_WIDENING(BgL_tmpz00_4032);
					}
					BgL_auxz00_4030 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4031);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4030))->BgL_numz00);
			}
		}

	}



/* &rtl_reg/ra-num */
	obj_t BGl_z62rtl_regzf2razd2numz42zzsaw_regsetz00(obj_t BgL_envz00_3389,
		obj_t BgL_oz00_3390)
	{
		{	/* SawMill/regset.sch 68 */
			return
				BINT(BGl_rtl_regzf2razd2numz20zzsaw_regsetz00(
					((BgL_rtl_regz00_bglt) BgL_oz00_3390)));
		}

	}



/* rtl_reg/ra-hardware */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2hardwarez20zzsaw_regsetz00(BgL_rtl_regz00_bglt
		BgL_oz00_34)
	{
		{	/* SawMill/regset.sch 70 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_34)))->BgL_hardwarez00);
		}

	}



/* &rtl_reg/ra-hardware */
	obj_t BGl_z62rtl_regzf2razd2hardwarez42zzsaw_regsetz00(obj_t BgL_envz00_3391,
		obj_t BgL_oz00_3392)
	{
		{	/* SawMill/regset.sch 70 */
			return
				BGl_rtl_regzf2razd2hardwarez20zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3392));
		}

	}



/* rtl_reg/ra-key */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2keyz20zzsaw_regsetz00(BgL_rtl_regz00_bglt BgL_oz00_37)
	{
		{	/* SawMill/regset.sch 72 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_37)))->BgL_keyz00);
		}

	}



/* &rtl_reg/ra-key */
	obj_t BGl_z62rtl_regzf2razd2keyz42zzsaw_regsetz00(obj_t BgL_envz00_3393,
		obj_t BgL_oz00_3394)
	{
		{	/* SawMill/regset.sch 72 */
			return
				BGl_rtl_regzf2razd2keyz20zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3394));
		}

	}



/* rtl_reg/ra-name */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2namez20zzsaw_regsetz00(BgL_rtl_regz00_bglt BgL_oz00_40)
	{
		{	/* SawMill/regset.sch 74 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_40)))->BgL_namez00);
		}

	}



/* &rtl_reg/ra-name */
	obj_t BGl_z62rtl_regzf2razd2namez42zzsaw_regsetz00(obj_t BgL_envz00_3395,
		obj_t BgL_oz00_3396)
	{
		{	/* SawMill/regset.sch 74 */
			return
				BGl_rtl_regzf2razd2namez20zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3396));
		}

	}



/* rtl_reg/ra-onexpr? */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_regsetz00(BgL_rtl_regz00_bglt
		BgL_oz00_43)
	{
		{	/* SawMill/regset.sch 76 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_43)))->BgL_onexprzf3zf3);
		}

	}



/* &rtl_reg/ra-onexpr? */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zb1zzsaw_regsetz00(obj_t BgL_envz00_3397,
		obj_t BgL_oz00_3398)
	{
		{	/* SawMill/regset.sch 76 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd3zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3398));
		}

	}



/* rtl_reg/ra-onexpr?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_regsetz00(BgL_rtl_regz00_bglt
		BgL_oz00_44, obj_t BgL_vz00_45)
	{
		{	/* SawMill/regset.sch 77 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_44)))->BgL_onexprzf3zf3) =
				((obj_t) BgL_vz00_45), BUNSPEC);
		}

	}



/* &rtl_reg/ra-onexpr?-set! */
	obj_t BGl_z62rtl_regzf2razd2onexprzf3zd2setz12z71zzsaw_regsetz00(obj_t
		BgL_envz00_3399, obj_t BgL_oz00_3400, obj_t BgL_vz00_3401)
	{
		{	/* SawMill/regset.sch 77 */
			return
				BGl_rtl_regzf2razd2onexprzf3zd2setz12z13zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3400), BgL_vz00_3401);
		}

	}



/* rtl_reg/ra-var */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varz20zzsaw_regsetz00(BgL_rtl_regz00_bglt BgL_oz00_46)
	{
		{	/* SawMill/regset.sch 78 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_46)))->BgL_varz00);
		}

	}



/* &rtl_reg/ra-var */
	obj_t BGl_z62rtl_regzf2razd2varz42zzsaw_regsetz00(obj_t BgL_envz00_3402,
		obj_t BgL_oz00_3403)
	{
		{	/* SawMill/regset.sch 78 */
			return
				BGl_rtl_regzf2razd2varz20zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3403));
		}

	}



/* rtl_reg/ra-var-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_regsetz00(BgL_rtl_regz00_bglt
		BgL_oz00_47, obj_t BgL_vz00_48)
	{
		{	/* SawMill/regset.sch 79 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_47)))->BgL_varz00) =
				((obj_t) BgL_vz00_48), BUNSPEC);
		}

	}



/* &rtl_reg/ra-var-set! */
	obj_t BGl_z62rtl_regzf2razd2varzd2setz12z82zzsaw_regsetz00(obj_t
		BgL_envz00_3404, obj_t BgL_oz00_3405, obj_t BgL_vz00_3406)
	{
		{	/* SawMill/regset.sch 79 */
			return
				BGl_rtl_regzf2razd2varzd2setz12ze0zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3405), BgL_vz00_3406);
		}

	}



/* rtl_reg/ra-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_rtl_regzf2razd2typez20zzsaw_regsetz00(BgL_rtl_regz00_bglt BgL_oz00_49)
	{
		{	/* SawMill/regset.sch 80 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt) BgL_oz00_49)))->BgL_typez00);
		}

	}



/* &rtl_reg/ra-type */
	BgL_typez00_bglt BGl_z62rtl_regzf2razd2typez42zzsaw_regsetz00(obj_t
		BgL_envz00_3407, obj_t BgL_oz00_3408)
	{
		{	/* SawMill/regset.sch 80 */
			return
				BGl_rtl_regzf2razd2typez20zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3408));
		}

	}



/* rtl_reg/ra-type-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_regsetz00(BgL_rtl_regz00_bglt
		BgL_oz00_50, BgL_typez00_bglt BgL_vz00_51)
	{
		{	/* SawMill/regset.sch 81 */
			return
				((((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_oz00_50)))->BgL_typez00) =
				((BgL_typez00_bglt) BgL_vz00_51), BUNSPEC);
		}

	}



/* &rtl_reg/ra-type-set! */
	obj_t BGl_z62rtl_regzf2razd2typezd2setz12z82zzsaw_regsetz00(obj_t
		BgL_envz00_3409, obj_t BgL_oz00_3410, obj_t BgL_vz00_3411)
	{
		{	/* SawMill/regset.sch 81 */
			return
				BGl_rtl_regzf2razd2typezd2setz12ze0zzsaw_regsetz00(
				((BgL_rtl_regz00_bglt) BgL_oz00_3410),
				((BgL_typez00_bglt) BgL_vz00_3411));
		}

	}



/* make-regset */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt BGl_makezd2regsetzd2zzsaw_regsetz00(int
		BgL_length1172z00_52, int BgL_msiza7e1173za7_53, obj_t BgL_regv1174z00_54,
		obj_t BgL_regl1175z00_55, obj_t BgL_string1176z00_56)
	{
		{	/* SawMill/regset.sch 84 */
			{	/* SawMill/regset.sch 84 */
				BgL_regsetz00_bglt BgL_new1182z00_3666;

				{	/* SawMill/regset.sch 84 */
					BgL_regsetz00_bglt BgL_new1180z00_3667;

					BgL_new1180z00_3667 =
						((BgL_regsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_regsetz00_bgl))));
					{	/* SawMill/regset.sch 84 */
						long BgL_arg1454z00_3668;

						BgL_arg1454z00_3668 = BGL_CLASS_NUM(BGl_regsetz00zzsaw_regsetz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1180z00_3667), BgL_arg1454z00_3668);
					}
					BgL_new1182z00_3666 = BgL_new1180z00_3667;
				}
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1182z00_3666))->BgL_lengthz00) =
					((int) BgL_length1172z00_52), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1182z00_3666))->BgL_msiza7eza7) =
					((int) BgL_msiza7e1173za7_53), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1182z00_3666))->BgL_regvz00) =
					((obj_t) BgL_regv1174z00_54), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1182z00_3666))->BgL_reglz00) =
					((obj_t) BgL_regl1175z00_55), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(BgL_new1182z00_3666))->BgL_stringz00) =
					((obj_t) BgL_string1176z00_56), BUNSPEC);
				return BgL_new1182z00_3666;
			}
		}

	}



/* &make-regset */
	BgL_regsetz00_bglt BGl_z62makezd2regsetzb0zzsaw_regsetz00(obj_t
		BgL_envz00_3412, obj_t BgL_length1172z00_3413,
		obj_t BgL_msiza7e1173za7_3414, obj_t BgL_regv1174z00_3415,
		obj_t BgL_regl1175z00_3416, obj_t BgL_string1176z00_3417)
	{
		{	/* SawMill/regset.sch 84 */
			return
				BGl_makezd2regsetzd2zzsaw_regsetz00(CINT(BgL_length1172z00_3413),
				CINT(BgL_msiza7e1173za7_3414), BgL_regv1174z00_3415,
				BgL_regl1175z00_3416, BgL_string1176z00_3417);
		}

	}



/* regset? */
	BGL_EXPORTED_DEF bool_t BGl_regsetzf3zf3zzsaw_regsetz00(obj_t BgL_objz00_57)
	{
		{	/* SawMill/regset.sch 85 */
			{	/* SawMill/regset.sch 85 */
				obj_t BgL_classz00_3669;

				BgL_classz00_3669 = BGl_regsetz00zzsaw_regsetz00;
				if (BGL_OBJECTP(BgL_objz00_57))
					{	/* SawMill/regset.sch 85 */
						BgL_objectz00_bglt BgL_arg1807z00_3670;

						BgL_arg1807z00_3670 = (BgL_objectz00_bglt) (BgL_objz00_57);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* SawMill/regset.sch 85 */
								long BgL_idxz00_3671;

								BgL_idxz00_3671 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3670);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3671 + 1L)) == BgL_classz00_3669);
							}
						else
							{	/* SawMill/regset.sch 85 */
								bool_t BgL_res2022z00_3674;

								{	/* SawMill/regset.sch 85 */
									obj_t BgL_oclassz00_3675;

									{	/* SawMill/regset.sch 85 */
										obj_t BgL_arg1815z00_3676;
										long BgL_arg1816z00_3677;

										BgL_arg1815z00_3676 = (BGl_za2classesza2z00zz__objectz00);
										{	/* SawMill/regset.sch 85 */
											long BgL_arg1817z00_3678;

											BgL_arg1817z00_3678 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3670);
											BgL_arg1816z00_3677 = (BgL_arg1817z00_3678 - OBJECT_TYPE);
										}
										BgL_oclassz00_3675 =
											VECTOR_REF(BgL_arg1815z00_3676, BgL_arg1816z00_3677);
									}
									{	/* SawMill/regset.sch 85 */
										bool_t BgL__ortest_1115z00_3679;

										BgL__ortest_1115z00_3679 =
											(BgL_classz00_3669 == BgL_oclassz00_3675);
										if (BgL__ortest_1115z00_3679)
											{	/* SawMill/regset.sch 85 */
												BgL_res2022z00_3674 = BgL__ortest_1115z00_3679;
											}
										else
											{	/* SawMill/regset.sch 85 */
												long BgL_odepthz00_3680;

												{	/* SawMill/regset.sch 85 */
													obj_t BgL_arg1804z00_3681;

													BgL_arg1804z00_3681 = (BgL_oclassz00_3675);
													BgL_odepthz00_3680 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3681);
												}
												if ((1L < BgL_odepthz00_3680))
													{	/* SawMill/regset.sch 85 */
														obj_t BgL_arg1802z00_3682;

														{	/* SawMill/regset.sch 85 */
															obj_t BgL_arg1803z00_3683;

															BgL_arg1803z00_3683 = (BgL_oclassz00_3675);
															BgL_arg1802z00_3682 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3683,
																1L);
														}
														BgL_res2022z00_3674 =
															(BgL_arg1802z00_3682 == BgL_classz00_3669);
													}
												else
													{	/* SawMill/regset.sch 85 */
														BgL_res2022z00_3674 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res2022z00_3674;
							}
					}
				else
					{	/* SawMill/regset.sch 85 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &regset? */
	obj_t BGl_z62regsetzf3z91zzsaw_regsetz00(obj_t BgL_envz00_3418,
		obj_t BgL_objz00_3419)
	{
		{	/* SawMill/regset.sch 85 */
			return BBOOL(BGl_regsetzf3zf3zzsaw_regsetz00(BgL_objz00_3419));
		}

	}



/* regset-nil */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt BGl_regsetzd2nilzd2zzsaw_regsetz00(void)
	{
		{	/* SawMill/regset.sch 86 */
			{	/* SawMill/regset.sch 86 */
				obj_t BgL_classz00_2811;

				BgL_classz00_2811 = BGl_regsetz00zzsaw_regsetz00;
				{	/* SawMill/regset.sch 86 */
					obj_t BgL__ortest_1117z00_2812;

					BgL__ortest_1117z00_2812 = BGL_CLASS_NIL(BgL_classz00_2811);
					if (CBOOL(BgL__ortest_1117z00_2812))
						{	/* SawMill/regset.sch 86 */
							return ((BgL_regsetz00_bglt) BgL__ortest_1117z00_2812);
						}
					else
						{	/* SawMill/regset.sch 86 */
							return
								((BgL_regsetz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2811));
						}
				}
			}
		}

	}



/* &regset-nil */
	BgL_regsetz00_bglt BGl_z62regsetzd2nilzb0zzsaw_regsetz00(obj_t
		BgL_envz00_3420)
	{
		{	/* SawMill/regset.sch 86 */
			return BGl_regsetzd2nilzd2zzsaw_regsetz00();
		}

	}



/* regset-string */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2zzsaw_regsetz00(BgL_regsetz00_bglt BgL_oz00_58)
	{
		{	/* SawMill/regset.sch 87 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_58))->BgL_stringz00);
		}

	}



/* &regset-string */
	obj_t BGl_z62regsetzd2stringzb0zzsaw_regsetz00(obj_t BgL_envz00_3421,
		obj_t BgL_oz00_3422)
	{
		{	/* SawMill/regset.sch 87 */
			return
				BGl_regsetzd2stringzd2zzsaw_regsetz00(
				((BgL_regsetz00_bglt) BgL_oz00_3422));
		}

	}



/* regset-string-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2stringzd2setz12z12zzsaw_regsetz00(BgL_regsetz00_bglt
		BgL_oz00_59, obj_t BgL_vz00_60)
	{
		{	/* SawMill/regset.sch 88 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_59))->BgL_stringz00) =
				((obj_t) BgL_vz00_60), BUNSPEC);
		}

	}



/* &regset-string-set! */
	obj_t BGl_z62regsetzd2stringzd2setz12z70zzsaw_regsetz00(obj_t BgL_envz00_3423,
		obj_t BgL_oz00_3424, obj_t BgL_vz00_3425)
	{
		{	/* SawMill/regset.sch 88 */
			return
				BGl_regsetzd2stringzd2setz12z12zzsaw_regsetz00(
				((BgL_regsetz00_bglt) BgL_oz00_3424), BgL_vz00_3425);
		}

	}



/* regset-regl */
	BGL_EXPORTED_DEF obj_t BGl_regsetzd2reglzd2zzsaw_regsetz00(BgL_regsetz00_bglt
		BgL_oz00_61)
	{
		{	/* SawMill/regset.sch 89 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_61))->BgL_reglz00);
		}

	}



/* &regset-regl */
	obj_t BGl_z62regsetzd2reglzb0zzsaw_regsetz00(obj_t BgL_envz00_3426,
		obj_t BgL_oz00_3427)
	{
		{	/* SawMill/regset.sch 89 */
			return
				BGl_regsetzd2reglzd2zzsaw_regsetz00(
				((BgL_regsetz00_bglt) BgL_oz00_3427));
		}

	}



/* regset-regv */
	BGL_EXPORTED_DEF obj_t BGl_regsetzd2regvzd2zzsaw_regsetz00(BgL_regsetz00_bglt
		BgL_oz00_64)
	{
		{	/* SawMill/regset.sch 91 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_64))->BgL_regvz00);
		}

	}



/* &regset-regv */
	obj_t BGl_z62regsetzd2regvzb0zzsaw_regsetz00(obj_t BgL_envz00_3428,
		obj_t BgL_oz00_3429)
	{
		{	/* SawMill/regset.sch 91 */
			return
				BGl_regsetzd2regvzd2zzsaw_regsetz00(
				((BgL_regsetz00_bglt) BgL_oz00_3429));
		}

	}



/* regset-msize */
	BGL_EXPORTED_DEF int BGl_regsetzd2msiza7ez75zzsaw_regsetz00(BgL_regsetz00_bglt
		BgL_oz00_67)
	{
		{	/* SawMill/regset.sch 93 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_67))->BgL_msiza7eza7);
		}

	}



/* &regset-msize */
	obj_t BGl_z62regsetzd2msiza7ez17zzsaw_regsetz00(obj_t BgL_envz00_3430,
		obj_t BgL_oz00_3431)
	{
		{	/* SawMill/regset.sch 93 */
			return
				BINT(BGl_regsetzd2msiza7ez75zzsaw_regsetz00(
					((BgL_regsetz00_bglt) BgL_oz00_3431)));
		}

	}



/* regset-length */
	BGL_EXPORTED_DEF int BGl_regsetzd2lengthzd2zzsaw_regsetz00(BgL_regsetz00_bglt
		BgL_oz00_70)
	{
		{	/* SawMill/regset.sch 95 */
			return (((BgL_regsetz00_bglt) COBJECT(BgL_oz00_70))->BgL_lengthz00);
		}

	}



/* &regset-length */
	obj_t BGl_z62regsetzd2lengthzb0zzsaw_regsetz00(obj_t BgL_envz00_3432,
		obj_t BgL_oz00_3433)
	{
		{	/* SawMill/regset.sch 95 */
			return
				BINT(BGl_regsetzd2lengthzd2zzsaw_regsetz00(
					((BgL_regsetz00_bglt) BgL_oz00_3433)));
		}

	}



/* regset-length-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2lengthzd2setz12z12zzsaw_regsetz00(BgL_regsetz00_bglt
		BgL_oz00_71, int BgL_vz00_72)
	{
		{	/* SawMill/regset.sch 96 */
			return
				((((BgL_regsetz00_bglt) COBJECT(BgL_oz00_71))->BgL_lengthz00) =
				((int) BgL_vz00_72), BUNSPEC);
		}

	}



/* &regset-length-set! */
	obj_t BGl_z62regsetzd2lengthzd2setz12z70zzsaw_regsetz00(obj_t BgL_envz00_3434,
		obj_t BgL_oz00_3435, obj_t BgL_vz00_3436)
	{
		{	/* SawMill/regset.sch 96 */
			return
				BGl_regsetzd2lengthzd2setz12z12zzsaw_regsetz00(
				((BgL_regsetz00_bglt) BgL_oz00_3435), CINT(BgL_vz00_3436));
		}

	}



/* regset-ref */
	BGL_EXPORTED_DEF obj_t BGl_regsetzd2refzd2zzsaw_regsetz00(obj_t BgL_sz00_73,
		obj_t BgL_iz00_74)
	{
		{	/* SawMill/regset.scm 70 */
			return
				BINT(
				(STRING_REF(
						(((BgL_regsetz00_bglt) COBJECT(
									((BgL_regsetz00_bglt) BgL_sz00_73)))->BgL_stringz00),
						(long) CINT(BgL_iz00_74))));
		}

	}



/* &regset-ref */
	obj_t BGl_z62regsetzd2refzb0zzsaw_regsetz00(obj_t BgL_envz00_3437,
		obj_t BgL_sz00_3438, obj_t BgL_iz00_3439)
	{
		{	/* SawMill/regset.scm 70 */
			return BGl_regsetzd2refzd2zzsaw_regsetz00(BgL_sz00_3438, BgL_iz00_3439);
		}

	}



/* make-empty-regset */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00(obj_t BgL_registersz00_78)
	{
		{	/* SawMill/regset.scm 82 */
			{	/* SawMill/regset.scm 83 */
				long BgL_lz00_2007;

				BgL_lz00_2007 = bgl_list_length(BgL_registersz00_78);
				{	/* SawMill/regset.scm 83 */
					obj_t BgL_vecz00_2008;

					if (
						(BGl_za2registerzd2cacheza2zd2zzsaw_regsetz00 ==
							BgL_registersz00_78))
						{	/* SawMill/regset.scm 84 */
							BgL_vecz00_2008 =
								BGl_za2registerzd2cachezd2cachedza2z00zzsaw_regsetz00;
						}
					else
						{	/* SawMill/regset.scm 86 */
							obj_t BgL_vz00_2014;

							BgL_vz00_2014 = make_vector(BgL_lz00_2007, BUNSPEC);
							{
								obj_t BgL_l1413z00_2016;

								BgL_l1413z00_2016 = BgL_registersz00_78;
							BgL_zc3z04anonymousza31514ze3z87_2017:
								if (PAIRP(BgL_l1413z00_2016))
									{	/* SawMill/regset.scm 87 */
										{	/* SawMill/regset.scm 88 */
											obj_t BgL_rz00_2019;

											BgL_rz00_2019 = CAR(BgL_l1413z00_2016);
											{	/* SawMill/regset.scm 88 */
												int BgL_arg1516z00_2020;

												{
													BgL_rtl_regzf2razf2_bglt BgL_auxz00_4158;

													{
														obj_t BgL_auxz00_4159;

														{	/* SawMill/regset.scm 88 */
															BgL_objectz00_bglt BgL_tmpz00_4160;

															BgL_tmpz00_4160 =
																((BgL_objectz00_bglt)
																((BgL_rtl_regz00_bglt) BgL_rz00_2019));
															BgL_auxz00_4159 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_4160);
														}
														BgL_auxz00_4158 =
															((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4159);
													}
													BgL_arg1516z00_2020 =
														(((BgL_rtl_regzf2razf2_bglt)
															COBJECT(BgL_auxz00_4158))->BgL_numz00);
												}
												VECTOR_SET(BgL_vz00_2014,
													(long) (BgL_arg1516z00_2020), BgL_rz00_2019);
										}}
										{
											obj_t BgL_l1413z00_4168;

											BgL_l1413z00_4168 = CDR(BgL_l1413z00_2016);
											BgL_l1413z00_2016 = BgL_l1413z00_4168;
											goto BgL_zc3z04anonymousza31514ze3z87_2017;
										}
									}
								else
									{	/* SawMill/regset.scm 87 */
										((bool_t) 1);
									}
							}
							BgL_vecz00_2008 = BgL_vz00_2014;
						}
					{	/* SawMill/regset.scm 84 */

						BGl_za2registerzd2cacheza2zd2zzsaw_regsetz00 = BgL_registersz00_78;
						BGl_za2registerzd2cachezd2cachedza2z00zzsaw_regsetz00 =
							BgL_vecz00_2008;
						{	/* SawMill/regset.scm 95 */
							BgL_regsetz00_bglt BgL_new1184z00_2009;

							{	/* SawMill/regset.scm 96 */
								BgL_regsetz00_bglt BgL_new1183z00_2012;

								BgL_new1183z00_2012 =
									((BgL_regsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_regsetz00_bgl))));
								{	/* SawMill/regset.scm 96 */
									long BgL_arg1513z00_2013;

									BgL_arg1513z00_2013 =
										BGL_CLASS_NUM(BGl_regsetz00zzsaw_regsetz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1183z00_2012),
										BgL_arg1513z00_2013);
								}
								BgL_new1184z00_2009 = BgL_new1183z00_2012;
							}
							((((BgL_regsetz00_bglt) COBJECT(BgL_new1184z00_2009))->
									BgL_lengthz00) = ((int) (int) (0L)), BUNSPEC);
							((((BgL_regsetz00_bglt) COBJECT(BgL_new1184z00_2009))->
									BgL_msiza7eza7) = ((int) (int) (BgL_lz00_2007)), BUNSPEC);
							((((BgL_regsetz00_bglt) COBJECT(BgL_new1184z00_2009))->
									BgL_regvz00) = ((obj_t) BgL_vecz00_2008), BUNSPEC);
							((((BgL_regsetz00_bglt) COBJECT(BgL_new1184z00_2009))->
									BgL_reglz00) = ((obj_t) BgL_registersz00_78), BUNSPEC);
							{
								obj_t BgL_auxz00_4180;

								{	/* SawMill/regset.scm 98 */
									long BgL_arg1502z00_2010;

									BgL_arg1502z00_2010 = (1L + (BgL_lz00_2007 / 8L));
									BgL_auxz00_4180 =
										make_string(BgL_arg1502z00_2010, ((unsigned char) '\000'));
								}
								((((BgL_regsetz00_bglt) COBJECT(BgL_new1184z00_2009))->
										BgL_stringz00) = ((obj_t) BgL_auxz00_4180), BUNSPEC);
							}
							return BgL_new1184z00_2009;
						}
					}
				}
			}
		}

	}



/* &make-empty-regset */
	BgL_regsetz00_bglt BGl_z62makezd2emptyzd2regsetz62zzsaw_regsetz00(obj_t
		BgL_envz00_3440, obj_t BgL_registersz00_3441)
	{
		{	/* SawMill/regset.scm 82 */
			return BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00(BgL_registersz00_3441);
		}

	}



/* list->regset */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_listzd2ze3regsetz31zzsaw_regsetz00(obj_t BgL_lstz00_79,
		obj_t BgL_registersz00_80)
	{
		{	/* SawMill/regset.scm 105 */
			{	/* SawMill/regset.scm 106 */
				BgL_regsetz00_bglt BgL_sz00_2024;

				BgL_sz00_2024 =
					BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00(BgL_registersz00_80);
				{
					obj_t BgL_l1415z00_2026;

					BgL_l1415z00_2026 = BgL_lstz00_79;
				BgL_zc3z04anonymousza31541ze3z87_2027:
					if (PAIRP(BgL_l1415z00_2026))
						{	/* SawMill/regset.scm 107 */
							{	/* SawMill/regset.scm 107 */
								obj_t BgL_ez00_2029;

								BgL_ez00_2029 = CAR(BgL_l1415z00_2026);
								BGl_regsetzd2addz12zc0zzsaw_regsetz00(BgL_sz00_2024,
									((BgL_rtl_regz00_bglt) BgL_ez00_2029));
							}
							{
								obj_t BgL_l1415z00_4192;

								BgL_l1415z00_4192 = CDR(BgL_l1415z00_2026);
								BgL_l1415z00_2026 = BgL_l1415z00_4192;
								goto BgL_zc3z04anonymousza31541ze3z87_2027;
							}
						}
					else
						{	/* SawMill/regset.scm 107 */
							((bool_t) 1);
						}
				}
				return BgL_sz00_2024;
			}
		}

	}



/* &list->regset */
	BgL_regsetz00_bglt BGl_z62listzd2ze3regsetz53zzsaw_regsetz00(obj_t
		BgL_envz00_3442, obj_t BgL_lstz00_3443, obj_t BgL_registersz00_3444)
	{
		{	/* SawMill/regset.scm 105 */
			return
				BGl_listzd2ze3regsetz31zzsaw_regsetz00(BgL_lstz00_3443,
				BgL_registersz00_3444);
		}

	}



/* regset->list */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2ze3listz31zzsaw_regsetz00(BgL_regsetz00_bglt BgL_sz00_81)
	{
		{	/* SawMill/regset.scm 113 */
			{	/* SawMill/regset.scm 114 */
				obj_t BgL_rz00_3452;

				BgL_rz00_3452 = MAKE_CELL(BNIL);
				{	/* SawMill/regset.scm 115 */
					obj_t BgL_zc3z04anonymousza31547ze3z87_3445;

					BgL_zc3z04anonymousza31547ze3z87_3445 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31547ze3ze5zzsaw_regsetz00, (int) (1L),
						(int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31547ze3z87_3445, (int) (0L),
						((obj_t) BgL_rz00_3452));
					BGl_regsetzd2forzd2eachz00zzsaw_regsetz00
						(BgL_zc3z04anonymousza31547ze3z87_3445, BgL_sz00_81);
				}
				return bgl_reverse_bang(CELL_REF(BgL_rz00_3452));
			}
		}

	}



/* &regset->list */
	obj_t BGl_z62regsetzd2ze3listz53zzsaw_regsetz00(obj_t BgL_envz00_3446,
		obj_t BgL_sz00_3447)
	{
		{	/* SawMill/regset.scm 113 */
			return
				BGl_regsetzd2ze3listz31zzsaw_regsetz00(
				((BgL_regsetz00_bglt) BgL_sz00_3447));
		}

	}



/* &<@anonymous:1547> */
	obj_t BGl_z62zc3z04anonymousza31547ze3ze5zzsaw_regsetz00(obj_t
		BgL_envz00_3448, obj_t BgL_ez00_3450)
	{
		{	/* SawMill/regset.scm 115 */
			{	/* SawMill/regset.scm 115 */
				obj_t BgL_rz00_3449;

				BgL_rz00_3449 = PROCEDURE_REF(BgL_envz00_3448, (int) (0L));
				{	/* SawMill/regset.scm 115 */
					obj_t BgL_auxz00_3684;

					BgL_auxz00_3684 =
						MAKE_YOUNG_PAIR(BgL_ez00_3450, CELL_REF(BgL_rz00_3449));
					return CELL_SET(BgL_rz00_3449, BgL_auxz00_3684);
				}
			}
		}

	}



/* duplicate-regset */
	BGL_EXPORTED_DEF BgL_regsetz00_bglt
		BGl_duplicatezd2regsetzd2zzsaw_regsetz00(BgL_regsetz00_bglt BgL_s0z00_82)
	{
		{	/* SawMill/regset.scm 121 */
			{	/* SawMill/regset.scm 122 */
				BgL_regsetz00_bglt BgL_sz00_2037;

				BgL_sz00_2037 =
					BGl_makezd2emptyzd2regsetz00zzsaw_regsetz00(
					(((BgL_regsetz00_bglt) COBJECT(BgL_s0z00_82))->BgL_reglz00));
				BGl_regsetzd2unionz12zc0zzsaw_regsetz00(BgL_sz00_2037, BgL_s0z00_82);
				return BgL_sz00_2037;
			}
		}

	}



/* &duplicate-regset */
	BgL_regsetz00_bglt BGl_z62duplicatezd2regsetzb0zzsaw_regsetz00(obj_t
		BgL_envz00_3454, obj_t BgL_s0z00_3455)
	{
		{	/* SawMill/regset.scm 121 */
			return
				BGl_duplicatezd2regsetzd2zzsaw_regsetz00(
				((BgL_regsetz00_bglt) BgL_s0z00_3455));
		}

	}



/* regset-member? */
	BGL_EXPORTED_DEF bool_t
		BGl_regsetzd2memberzf3z21zzsaw_regsetz00(BgL_rtl_regz00_bglt BgL_regz00_83,
		BgL_regsetz00_bglt BgL_sz00_84)
	{
		{	/* SawMill/regset.scm 129 */
			{	/* SawMill/regset.scm 131 */
				long BgL_basez00_3685;
				long BgL_bitz00_3686;

				{	/* SawMill/regset.scm 131 */
					int BgL_arg1576z00_3687;

					{
						BgL_rtl_regzf2razf2_bglt BgL_auxz00_4213;

						{
							obj_t BgL_auxz00_4214;

							{	/* SawMill/regset.scm 131 */
								BgL_objectz00_bglt BgL_tmpz00_4215;

								BgL_tmpz00_4215 = ((BgL_objectz00_bglt) BgL_regz00_83);
								BgL_auxz00_4214 = BGL_OBJECT_WIDENING(BgL_tmpz00_4215);
							}
							BgL_auxz00_4213 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4214);
						}
						BgL_arg1576z00_3687 =
							(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4213))->
							BgL_numz00);
					}
					BgL_basez00_3685 = ((long) (BgL_arg1576z00_3687) / 8L);
				}
				{	/* SawMill/regset.scm 132 */
					int BgL_arg1584z00_3688;

					{
						BgL_rtl_regzf2razf2_bglt BgL_auxz00_4222;

						{
							obj_t BgL_auxz00_4223;

							{	/* SawMill/regset.scm 132 */
								BgL_objectz00_bglt BgL_tmpz00_4224;

								BgL_tmpz00_4224 = ((BgL_objectz00_bglt) BgL_regz00_83);
								BgL_auxz00_4223 = BGL_OBJECT_WIDENING(BgL_tmpz00_4224);
							}
							BgL_auxz00_4222 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4223);
						}
						BgL_arg1584z00_3688 =
							(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4222))->
							BgL_numz00);
					}
					{	/* SawMill/regset.scm 132 */
						long BgL_n1z00_3689;
						long BgL_n2z00_3690;

						BgL_n1z00_3689 = (long) (BgL_arg1584z00_3688);
						BgL_n2z00_3690 = 8L;
						{	/* SawMill/regset.scm 132 */
							bool_t BgL_test2191z00_4230;

							{	/* SawMill/regset.scm 132 */
								long BgL_arg1338z00_3691;

								BgL_arg1338z00_3691 =
									(((BgL_n1z00_3689) | (BgL_n2z00_3690)) & -2147483648);
								BgL_test2191z00_4230 = (BgL_arg1338z00_3691 == 0L);
							}
							if (BgL_test2191z00_4230)
								{	/* SawMill/regset.scm 132 */
									int32_t BgL_arg1334z00_3692;

									{	/* SawMill/regset.scm 132 */
										int32_t BgL_arg1336z00_3693;
										int32_t BgL_arg1337z00_3694;

										BgL_arg1336z00_3693 = (int32_t) (BgL_n1z00_3689);
										BgL_arg1337z00_3694 = (int32_t) (BgL_n2z00_3690);
										BgL_arg1334z00_3692 =
											(BgL_arg1336z00_3693 % BgL_arg1337z00_3694);
									}
									{	/* SawMill/regset.scm 132 */
										long BgL_arg1446z00_3695;

										BgL_arg1446z00_3695 = (long) (BgL_arg1334z00_3692);
										BgL_bitz00_3686 = (long) (BgL_arg1446z00_3695);
								}}
							else
								{	/* SawMill/regset.scm 132 */
									BgL_bitz00_3686 = (BgL_n1z00_3689 % BgL_n2z00_3690);
								}
						}
					}
				}
				if (
					(BgL_basez00_3685 <
						STRING_LENGTH(
							(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_84))->BgL_stringz00))))
					{	/* SawMill/regset.scm 133 */
						return
							(
							((STRING_REF(
										(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_84))->
											BgL_stringz00),
										BgL_basez00_3685)) & (1L << (int) (BgL_bitz00_3686))) > 0L);
					}
				else
					{	/* SawMill/regset.scm 133 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &regset-member? */
	obj_t BGl_z62regsetzd2memberzf3z43zzsaw_regsetz00(obj_t BgL_envz00_3456,
		obj_t BgL_regz00_3457, obj_t BgL_sz00_3458)
	{
		{	/* SawMill/regset.scm 129 */
			return
				BBOOL(BGl_regsetzd2memberzf3z21zzsaw_regsetz00(
					((BgL_rtl_regz00_bglt) BgL_regz00_3457),
					((BgL_regsetz00_bglt) BgL_sz00_3458)));
		}

	}



/* regset-empty? */
	BGL_EXPORTED_DEF obj_t
		BGl_regsetzd2emptyzf3z21zzsaw_regsetz00(BgL_regsetz00_bglt BgL_sz00_85)
	{
		{	/* SawMill/regset.scm 139 */
			return
				BBOOL(
				((long) (
						(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_85))->BgL_lengthz00)) ==
					0L));
		}

	}



/* &regset-empty? */
	obj_t BGl_z62regsetzd2emptyzf3z43zzsaw_regsetz00(obj_t BgL_envz00_3459,
		obj_t BgL_sz00_3460)
	{
		{	/* SawMill/regset.scm 139 */
			return
				BGl_regsetzd2emptyzf3z21zzsaw_regsetz00(
				((BgL_regsetz00_bglt) BgL_sz00_3460));
		}

	}



/* regset-add! */
	BGL_EXPORTED_DEF bool_t
		BGl_regsetzd2addz12zc0zzsaw_regsetz00(BgL_regsetz00_bglt BgL_sz00_86,
		BgL_rtl_regz00_bglt BgL_regz00_87)
	{
		{	/* SawMill/regset.scm 145 */
			{	/* SawMill/regset.scm 146 */
				bool_t BgL_test2193z00_4260;

				{	/* SawMill/regset.scm 131 */
					long BgL_basez00_2889;
					long BgL_bitz00_2890;

					{	/* SawMill/regset.scm 131 */
						int BgL_arg1576z00_2891;

						{
							BgL_rtl_regzf2razf2_bglt BgL_auxz00_4261;

							{
								obj_t BgL_auxz00_4262;

								{	/* SawMill/regset.scm 131 */
									BgL_objectz00_bglt BgL_tmpz00_4263;

									BgL_tmpz00_4263 = ((BgL_objectz00_bglt) BgL_regz00_87);
									BgL_auxz00_4262 = BGL_OBJECT_WIDENING(BgL_tmpz00_4263);
								}
								BgL_auxz00_4261 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4262);
							}
							BgL_arg1576z00_2891 =
								(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4261))->
								BgL_numz00);
						}
						BgL_basez00_2889 = ((long) (BgL_arg1576z00_2891) / 8L);
					}
					{	/* SawMill/regset.scm 132 */
						int BgL_arg1584z00_2892;

						{
							BgL_rtl_regzf2razf2_bglt BgL_auxz00_4270;

							{
								obj_t BgL_auxz00_4271;

								{	/* SawMill/regset.scm 132 */
									BgL_objectz00_bglt BgL_tmpz00_4272;

									BgL_tmpz00_4272 = ((BgL_objectz00_bglt) BgL_regz00_87);
									BgL_auxz00_4271 = BGL_OBJECT_WIDENING(BgL_tmpz00_4272);
								}
								BgL_auxz00_4270 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4271);
							}
							BgL_arg1584z00_2892 =
								(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4270))->
								BgL_numz00);
						}
						{	/* SawMill/regset.scm 132 */
							long BgL_n1z00_2902;
							long BgL_n2z00_2903;

							BgL_n1z00_2902 = (long) (BgL_arg1584z00_2892);
							BgL_n2z00_2903 = 8L;
							{	/* SawMill/regset.scm 132 */
								bool_t BgL_test2194z00_4278;

								{	/* SawMill/regset.scm 132 */
									long BgL_arg1338z00_2905;

									BgL_arg1338z00_2905 =
										(((BgL_n1z00_2902) | (BgL_n2z00_2903)) & -2147483648);
									BgL_test2194z00_4278 = (BgL_arg1338z00_2905 == 0L);
								}
								if (BgL_test2194z00_4278)
									{	/* SawMill/regset.scm 132 */
										int32_t BgL_arg1334z00_2906;

										{	/* SawMill/regset.scm 132 */
											int32_t BgL_arg1336z00_2907;
											int32_t BgL_arg1337z00_2908;

											BgL_arg1336z00_2907 = (int32_t) (BgL_n1z00_2902);
											BgL_arg1337z00_2908 = (int32_t) (BgL_n2z00_2903);
											BgL_arg1334z00_2906 =
												(BgL_arg1336z00_2907 % BgL_arg1337z00_2908);
										}
										{	/* SawMill/regset.scm 132 */
											long BgL_arg1446z00_2913;

											BgL_arg1446z00_2913 = (long) (BgL_arg1334z00_2906);
											BgL_bitz00_2890 = (long) (BgL_arg1446z00_2913);
									}}
								else
									{	/* SawMill/regset.scm 132 */
										BgL_bitz00_2890 = (BgL_n1z00_2902 % BgL_n2z00_2903);
									}
							}
						}
					}
					if (
						(BgL_basez00_2889 <
							STRING_LENGTH(
								(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_86))->BgL_stringz00))))
						{	/* SawMill/regset.scm 133 */
							BgL_test2193z00_4260 =
								(
								((STRING_REF(
											(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_86))->
												BgL_stringz00),
											BgL_basez00_2889)) & (1L << (int) (BgL_bitz00_2890))) >
								0L);
						}
					else
						{	/* SawMill/regset.scm 133 */
							BgL_test2193z00_4260 = ((bool_t) 0);
						}
				}
				if (BgL_test2193z00_4260)
					{	/* SawMill/regset.scm 146 */
						return ((bool_t) 0);
					}
				else
					{	/* SawMill/regset.scm 148 */
						long BgL_basez00_2055;
						long BgL_bitz00_2056;

						{	/* SawMill/regset.scm 148 */
							int BgL_arg1594z00_2062;

							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_4298;

								{
									obj_t BgL_auxz00_4299;

									{	/* SawMill/regset.scm 148 */
										BgL_objectz00_bglt BgL_tmpz00_4300;

										BgL_tmpz00_4300 = ((BgL_objectz00_bglt) BgL_regz00_87);
										BgL_auxz00_4299 = BGL_OBJECT_WIDENING(BgL_tmpz00_4300);
									}
									BgL_auxz00_4298 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4299);
								}
								BgL_arg1594z00_2062 =
									(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4298))->
									BgL_numz00);
							}
							BgL_basez00_2055 = ((long) (BgL_arg1594z00_2062) / 8L);
						}
						{	/* SawMill/regset.scm 149 */
							int BgL_arg1595z00_2063;

							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_4307;

								{
									obj_t BgL_auxz00_4308;

									{	/* SawMill/regset.scm 149 */
										BgL_objectz00_bglt BgL_tmpz00_4309;

										BgL_tmpz00_4309 = ((BgL_objectz00_bglt) BgL_regz00_87);
										BgL_auxz00_4308 = BGL_OBJECT_WIDENING(BgL_tmpz00_4309);
									}
									BgL_auxz00_4307 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4308);
								}
								BgL_arg1595z00_2063 =
									(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4307))->
									BgL_numz00);
							}
							{	/* SawMill/regset.scm 149 */
								long BgL_n1z00_2932;
								long BgL_n2z00_2933;

								BgL_n1z00_2932 = (long) (BgL_arg1595z00_2063);
								BgL_n2z00_2933 = 8L;
								{	/* SawMill/regset.scm 149 */
									bool_t BgL_test2196z00_4315;

									{	/* SawMill/regset.scm 149 */
										long BgL_arg1338z00_2935;

										BgL_arg1338z00_2935 =
											(((BgL_n1z00_2932) | (BgL_n2z00_2933)) & -2147483648);
										BgL_test2196z00_4315 = (BgL_arg1338z00_2935 == 0L);
									}
									if (BgL_test2196z00_4315)
										{	/* SawMill/regset.scm 149 */
											int32_t BgL_arg1334z00_2936;

											{	/* SawMill/regset.scm 149 */
												int32_t BgL_arg1336z00_2937;
												int32_t BgL_arg1337z00_2938;

												BgL_arg1336z00_2937 = (int32_t) (BgL_n1z00_2932);
												BgL_arg1337z00_2938 = (int32_t) (BgL_n2z00_2933);
												BgL_arg1334z00_2936 =
													(BgL_arg1336z00_2937 % BgL_arg1337z00_2938);
											}
											{	/* SawMill/regset.scm 149 */
												long BgL_arg1446z00_2943;

												BgL_arg1446z00_2943 = (long) (BgL_arg1334z00_2936);
												BgL_bitz00_2056 = (long) (BgL_arg1446z00_2943);
										}}
									else
										{	/* SawMill/regset.scm 149 */
											BgL_bitz00_2056 = (BgL_n1z00_2932 % BgL_n2z00_2933);
										}
								}
							}
						}
						((((BgL_regsetz00_bglt) COBJECT(BgL_sz00_86))->BgL_lengthz00) =
							((int) (int) (((long) ((((BgL_regsetz00_bglt)
													COBJECT(BgL_sz00_86))->BgL_lengthz00)) + 1L))),
							BUNSPEC);
						{	/* SawMill/regset.scm 152 */
							long BgL_nvalz00_2059;

							BgL_nvalz00_2059 =
								(
								(STRING_REF(
										(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_86))->
											BgL_stringz00),
										BgL_basez00_2055)) | (1L << (int) (BgL_bitz00_2056)));
							{	/* SawMill/regset.scm 77 */
								unsigned char BgL_auxz00_4337;
								obj_t BgL_tmpz00_4335;

								BgL_auxz00_4337 = (BgL_nvalz00_2059);
								BgL_tmpz00_4335 =
									(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_86))->BgL_stringz00);
								STRING_SET(BgL_tmpz00_4335, BgL_basez00_2055, BgL_auxz00_4337);
							}
							return ((bool_t) 1);
						}
					}
			}
		}

	}



/* &regset-add! */
	obj_t BGl_z62regsetzd2addz12za2zzsaw_regsetz00(obj_t BgL_envz00_3461,
		obj_t BgL_sz00_3462, obj_t BgL_regz00_3463)
	{
		{	/* SawMill/regset.scm 145 */
			return
				BBOOL(BGl_regsetzd2addz12zc0zzsaw_regsetz00(
					((BgL_regsetz00_bglt) BgL_sz00_3462),
					((BgL_rtl_regz00_bglt) BgL_regz00_3463)));
		}

	}



/* regset-add*! */
	BGL_EXPORTED_DEF bool_t
		BGl_regsetzd2addza2z12z62zzsaw_regsetz00(BgL_regsetz00_bglt BgL_sz00_88,
		obj_t BgL_regsz00_89)
	{
		{	/* SawMill/regset.scm 159 */
			{
				obj_t BgL_l1417z00_2065;

				BgL_l1417z00_2065 = BgL_regsz00_89;
			BgL_zc3z04anonymousza31596ze3z87_2066:
				if (PAIRP(BgL_l1417z00_2065))
					{	/* SawMill/regset.scm 160 */
						{	/* SawMill/regset.scm 160 */
							obj_t BgL_rz00_2068;

							BgL_rz00_2068 = CAR(BgL_l1417z00_2065);
							BGl_regsetzd2addz12zc0zzsaw_regsetz00(BgL_sz00_88,
								((BgL_rtl_regz00_bglt) BgL_rz00_2068));
						}
						{
							obj_t BgL_l1417z00_4349;

							BgL_l1417z00_4349 = CDR(BgL_l1417z00_2065);
							BgL_l1417z00_2065 = BgL_l1417z00_4349;
							goto BgL_zc3z04anonymousza31596ze3z87_2066;
						}
					}
				else
					{	/* SawMill/regset.scm 160 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* &regset-add*! */
	obj_t BGl_z62regsetzd2addza2z12z00zzsaw_regsetz00(obj_t BgL_envz00_3464,
		obj_t BgL_sz00_3465, obj_t BgL_regsz00_3466)
	{
		{	/* SawMill/regset.scm 159 */
			return
				BBOOL(BGl_regsetzd2addza2z12z62zzsaw_regsetz00(
					((BgL_regsetz00_bglt) BgL_sz00_3465), BgL_regsz00_3466));
		}

	}



/* regset-remove! */
	BGL_EXPORTED_DEF bool_t
		BGl_regsetzd2removez12zc0zzsaw_regsetz00(BgL_regsetz00_bglt BgL_sz00_90,
		BgL_rtl_regz00_bglt BgL_regz00_91)
	{
		{	/* SawMill/regset.scm 165 */
			{	/* SawMill/regset.scm 166 */
				bool_t BgL_test2198z00_4354;

				{	/* SawMill/regset.scm 131 */
					long BgL_basez00_2967;
					long BgL_bitz00_2968;

					{	/* SawMill/regset.scm 131 */
						int BgL_arg1576z00_2969;

						{
							BgL_rtl_regzf2razf2_bglt BgL_auxz00_4355;

							{
								obj_t BgL_auxz00_4356;

								{	/* SawMill/regset.scm 131 */
									BgL_objectz00_bglt BgL_tmpz00_4357;

									BgL_tmpz00_4357 = ((BgL_objectz00_bglt) BgL_regz00_91);
									BgL_auxz00_4356 = BGL_OBJECT_WIDENING(BgL_tmpz00_4357);
								}
								BgL_auxz00_4355 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4356);
							}
							BgL_arg1576z00_2969 =
								(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4355))->
								BgL_numz00);
						}
						BgL_basez00_2967 = ((long) (BgL_arg1576z00_2969) / 8L);
					}
					{	/* SawMill/regset.scm 132 */
						int BgL_arg1584z00_2970;

						{
							BgL_rtl_regzf2razf2_bglt BgL_auxz00_4364;

							{
								obj_t BgL_auxz00_4365;

								{	/* SawMill/regset.scm 132 */
									BgL_objectz00_bglt BgL_tmpz00_4366;

									BgL_tmpz00_4366 = ((BgL_objectz00_bglt) BgL_regz00_91);
									BgL_auxz00_4365 = BGL_OBJECT_WIDENING(BgL_tmpz00_4366);
								}
								BgL_auxz00_4364 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4365);
							}
							BgL_arg1584z00_2970 =
								(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4364))->
								BgL_numz00);
						}
						{	/* SawMill/regset.scm 132 */
							long BgL_n1z00_2980;
							long BgL_n2z00_2981;

							BgL_n1z00_2980 = (long) (BgL_arg1584z00_2970);
							BgL_n2z00_2981 = 8L;
							{	/* SawMill/regset.scm 132 */
								bool_t BgL_test2199z00_4372;

								{	/* SawMill/regset.scm 132 */
									long BgL_arg1338z00_2983;

									BgL_arg1338z00_2983 =
										(((BgL_n1z00_2980) | (BgL_n2z00_2981)) & -2147483648);
									BgL_test2199z00_4372 = (BgL_arg1338z00_2983 == 0L);
								}
								if (BgL_test2199z00_4372)
									{	/* SawMill/regset.scm 132 */
										int32_t BgL_arg1334z00_2984;

										{	/* SawMill/regset.scm 132 */
											int32_t BgL_arg1336z00_2985;
											int32_t BgL_arg1337z00_2986;

											BgL_arg1336z00_2985 = (int32_t) (BgL_n1z00_2980);
											BgL_arg1337z00_2986 = (int32_t) (BgL_n2z00_2981);
											BgL_arg1334z00_2984 =
												(BgL_arg1336z00_2985 % BgL_arg1337z00_2986);
										}
										{	/* SawMill/regset.scm 132 */
											long BgL_arg1446z00_2991;

											BgL_arg1446z00_2991 = (long) (BgL_arg1334z00_2984);
											BgL_bitz00_2968 = (long) (BgL_arg1446z00_2991);
									}}
								else
									{	/* SawMill/regset.scm 132 */
										BgL_bitz00_2968 = (BgL_n1z00_2980 % BgL_n2z00_2981);
									}
							}
						}
					}
					if (
						(BgL_basez00_2967 <
							STRING_LENGTH(
								(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_90))->BgL_stringz00))))
						{	/* SawMill/regset.scm 133 */
							BgL_test2198z00_4354 =
								(
								((STRING_REF(
											(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_90))->
												BgL_stringz00),
											BgL_basez00_2967)) & (1L << (int) (BgL_bitz00_2968))) >
								0L);
						}
					else
						{	/* SawMill/regset.scm 133 */
							BgL_test2198z00_4354 = ((bool_t) 0);
						}
				}
				if (BgL_test2198z00_4354)
					{	/* SawMill/regset.scm 168 */
						long BgL_basez00_2073;
						long BgL_bitz00_2074;

						{	/* SawMill/regset.scm 168 */
							int BgL_arg1613z00_2081;

							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_4392;

								{
									obj_t BgL_auxz00_4393;

									{	/* SawMill/regset.scm 168 */
										BgL_objectz00_bglt BgL_tmpz00_4394;

										BgL_tmpz00_4394 = ((BgL_objectz00_bglt) BgL_regz00_91);
										BgL_auxz00_4393 = BGL_OBJECT_WIDENING(BgL_tmpz00_4394);
									}
									BgL_auxz00_4392 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4393);
								}
								BgL_arg1613z00_2081 =
									(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4392))->
									BgL_numz00);
							}
							BgL_basez00_2073 = ((long) (BgL_arg1613z00_2081) / 8L);
						}
						{	/* SawMill/regset.scm 169 */
							int BgL_arg1615z00_2082;

							{
								BgL_rtl_regzf2razf2_bglt BgL_auxz00_4401;

								{
									obj_t BgL_auxz00_4402;

									{	/* SawMill/regset.scm 169 */
										BgL_objectz00_bglt BgL_tmpz00_4403;

										BgL_tmpz00_4403 = ((BgL_objectz00_bglt) BgL_regz00_91);
										BgL_auxz00_4402 = BGL_OBJECT_WIDENING(BgL_tmpz00_4403);
									}
									BgL_auxz00_4401 =
										((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4402);
								}
								BgL_arg1615z00_2082 =
									(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4401))->
									BgL_numz00);
							}
							{	/* SawMill/regset.scm 169 */
								long BgL_n1z00_3010;
								long BgL_n2z00_3011;

								BgL_n1z00_3010 = (long) (BgL_arg1615z00_2082);
								BgL_n2z00_3011 = 8L;
								{	/* SawMill/regset.scm 169 */
									bool_t BgL_test2201z00_4409;

									{	/* SawMill/regset.scm 169 */
										long BgL_arg1338z00_3013;

										BgL_arg1338z00_3013 =
											(((BgL_n1z00_3010) | (BgL_n2z00_3011)) & -2147483648);
										BgL_test2201z00_4409 = (BgL_arg1338z00_3013 == 0L);
									}
									if (BgL_test2201z00_4409)
										{	/* SawMill/regset.scm 169 */
											int32_t BgL_arg1334z00_3014;

											{	/* SawMill/regset.scm 169 */
												int32_t BgL_arg1336z00_3015;
												int32_t BgL_arg1337z00_3016;

												BgL_arg1336z00_3015 = (int32_t) (BgL_n1z00_3010);
												BgL_arg1337z00_3016 = (int32_t) (BgL_n2z00_3011);
												BgL_arg1334z00_3014 =
													(BgL_arg1336z00_3015 % BgL_arg1337z00_3016);
											}
											{	/* SawMill/regset.scm 169 */
												long BgL_arg1446z00_3021;

												BgL_arg1446z00_3021 = (long) (BgL_arg1334z00_3014);
												BgL_bitz00_2074 = (long) (BgL_arg1446z00_3021);
										}}
									else
										{	/* SawMill/regset.scm 169 */
											BgL_bitz00_2074 = (BgL_n1z00_3010 % BgL_n2z00_3011);
										}
								}
							}
						}
						((((BgL_regsetz00_bglt) COBJECT(BgL_sz00_90))->BgL_lengthz00) =
							((int) (int) (((long) ((((BgL_regsetz00_bglt)
													COBJECT(BgL_sz00_90))->BgL_lengthz00)) - 1L))),
							BUNSPEC);
						{	/* SawMill/regset.scm 172 */
							long BgL_nvalz00_2077;

							BgL_nvalz00_2077 =
								(
								(STRING_REF(
										(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_90))->
											BgL_stringz00),
										BgL_basez00_2073)) & ~((1L << (int) (BgL_bitz00_2074))));
							{	/* SawMill/regset.scm 77 */
								unsigned char BgL_auxz00_4432;
								obj_t BgL_tmpz00_4430;

								BgL_auxz00_4432 = (BgL_nvalz00_2077);
								BgL_tmpz00_4430 =
									(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_90))->BgL_stringz00);
								STRING_SET(BgL_tmpz00_4430, BgL_basez00_2073, BgL_auxz00_4432);
							}
							return ((bool_t) 1);
						}
					}
				else
					{	/* SawMill/regset.scm 166 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &regset-remove! */
	obj_t BGl_z62regsetzd2removez12za2zzsaw_regsetz00(obj_t BgL_envz00_3467,
		obj_t BgL_sz00_3468, obj_t BgL_regz00_3469)
	{
		{	/* SawMill/regset.scm 165 */
			return
				BBOOL(BGl_regsetzd2removez12zc0zzsaw_regsetz00(
					((BgL_regsetz00_bglt) BgL_sz00_3468),
					((BgL_rtl_regz00_bglt) BgL_regz00_3469)));
		}

	}



/* regset-union! */
	BGL_EXPORTED_DEF bool_t
		BGl_regsetzd2unionz12zc0zzsaw_regsetz00(BgL_regsetz00_bglt BgL_s1z00_92,
		BgL_regsetz00_bglt BgL_s2z00_93)
	{
		{	/* SawMill/regset.scm 180 */
			{	/* SawMill/regset.scm 181 */
				obj_t BgL_st1z00_2083;
				obj_t BgL_st2z00_2084;

				BgL_st1z00_2083 =
					(((BgL_regsetz00_bglt) COBJECT(BgL_s1z00_92))->BgL_stringz00);
				BgL_st2z00_2084 =
					(((BgL_regsetz00_bglt) COBJECT(BgL_s2z00_93))->BgL_stringz00);
				{	/* SawMill/regset.scm 183 */
					long BgL_g1192z00_2085;

					BgL_g1192z00_2085 = (STRING_LENGTH(BgL_st1z00_2083) - 1L);
					{
						long BgL_iz00_2087;
						bool_t BgL_resz00_2088;

						BgL_iz00_2087 = BgL_g1192z00_2085;
						BgL_resz00_2088 = ((bool_t) 0);
					BgL_zc3z04anonymousza31616ze3z87_2089:
						if ((BgL_iz00_2087 == -1L))
							{	/* SawMill/regset.scm 186 */
								return BgL_resz00_2088;
							}
						else
							{	/* SawMill/regset.scm 186 */
								if (
									(STRING_REF(BgL_st1z00_2083, BgL_iz00_2087) ==
										STRING_REF(BgL_st2z00_2084, BgL_iz00_2087)))
									{
										long BgL_iz00_4449;

										BgL_iz00_4449 = (BgL_iz00_2087 - 1L);
										BgL_iz00_2087 = BgL_iz00_4449;
										goto BgL_zc3z04anonymousza31616ze3z87_2089;
									}
								else
									{	/* SawMill/regset.scm 191 */
										long BgL_n2z00_2095;

										BgL_n2z00_2095 =
											(STRING_REF(
												(((BgL_regsetz00_bglt) COBJECT(BgL_s2z00_93))->
													BgL_stringz00), BgL_iz00_2087));
										{	/* SawMill/regset.scm 192 */
											long BgL_g1193z00_2096;

											BgL_g1193z00_2096 =
												(STRING_REF(
													(((BgL_regsetz00_bglt) COBJECT(BgL_s1z00_92))->
														BgL_stringz00), BgL_iz00_2087));
											{
												long BgL_jz00_2098;
												bool_t BgL_resz00_2099;
												long BgL_n1z00_2100;

												BgL_jz00_2098 = 1L;
												BgL_resz00_2099 = BgL_resz00_2088;
												BgL_n1z00_2100 = BgL_g1193z00_2096;
											BgL_zc3z04anonymousza31630ze3z87_2101:
												if ((BgL_jz00_2098 == 256L))
													{
														bool_t BgL_resz00_4461;
														long BgL_iz00_4459;

														BgL_iz00_4459 = (BgL_iz00_2087 - 1L);
														BgL_resz00_4461 = BgL_resz00_2099;
														BgL_resz00_2088 = BgL_resz00_4461;
														BgL_iz00_2087 = BgL_iz00_4459;
														goto BgL_zc3z04anonymousza31616ze3z87_2089;
													}
												else
													{	/* SawMill/regset.scm 196 */
														if (
															((BgL_n1z00_2100 & BgL_jz00_2098) ==
																(BgL_n2z00_2095 & BgL_jz00_2098)))
															{
																long BgL_jz00_4466;

																BgL_jz00_4466 = (BgL_jz00_2098 << (int) (1L));
																BgL_jz00_2098 = BgL_jz00_4466;
																goto BgL_zc3z04anonymousza31630ze3z87_2101;
															}
														else
															{	/* SawMill/regset.scm 198 */
																if (((BgL_n1z00_2100 & BgL_jz00_2098) == 0L))
																	{	/* SawMill/regset.scm 200 */
																		((((BgL_regsetz00_bglt)
																					COBJECT(BgL_s1z00_92))->
																				BgL_lengthz00) =
																			((int) (int) (((long) (((
																									(BgL_regsetz00_bglt)
																									COBJECT(BgL_s1z00_92))->
																								BgL_lengthz00)) + 1L))),
																			BUNSPEC);
																		{	/* SawMill/regset.scm 203 */
																			long BgL_n1z00_2112;

																			BgL_n1z00_2112 =
																				(BgL_n1z00_2100 | BgL_jz00_2098);
																			{	/* SawMill/regset.scm 77 */
																				unsigned char BgL_auxz00_4480;
																				obj_t BgL_tmpz00_4478;

																				BgL_auxz00_4480 = (BgL_n1z00_2112);
																				BgL_tmpz00_4478 =
																					(((BgL_regsetz00_bglt)
																						COBJECT(BgL_s1z00_92))->
																					BgL_stringz00);
																				STRING_SET(BgL_tmpz00_4478,
																					BgL_iz00_2087, BgL_auxz00_4480);
																			}
																			{
																				long BgL_n1z00_4487;
																				bool_t BgL_resz00_4486;
																				long BgL_jz00_4483;

																				BgL_jz00_4483 =
																					(BgL_jz00_2098 << (int) (1L));
																				BgL_resz00_4486 = ((bool_t) 1);
																				BgL_n1z00_4487 = BgL_n1z00_2112;
																				BgL_n1z00_2100 = BgL_n1z00_4487;
																				BgL_resz00_2099 = BgL_resz00_4486;
																				BgL_jz00_2098 = BgL_jz00_4483;
																				goto
																					BgL_zc3z04anonymousza31630ze3z87_2101;
																			}
																		}
																	}
																else
																	{
																		long BgL_jz00_4488;

																		BgL_jz00_4488 =
																			(BgL_jz00_2098 << (int) (1L));
																		BgL_jz00_2098 = BgL_jz00_4488;
																		goto BgL_zc3z04anonymousza31630ze3z87_2101;
																	}
															}
													}
											}
										}
									}
							}
					}
				}
			}
		}

	}



/* &regset-union! */
	obj_t BGl_z62regsetzd2unionz12za2zzsaw_regsetz00(obj_t BgL_envz00_3470,
		obj_t BgL_s1z00_3471, obj_t BgL_s2z00_3472)
	{
		{	/* SawMill/regset.scm 180 */
			return
				BBOOL(BGl_regsetzd2unionz12zc0zzsaw_regsetz00(
					((BgL_regsetz00_bglt) BgL_s1z00_3471),
					((BgL_regsetz00_bglt) BgL_s2z00_3472)));
		}

	}



/* regset-union*! */
	BGL_EXPORTED_DEF bool_t
		BGl_regsetzd2unionza2z12z62zzsaw_regsetz00(BgL_regsetz00_bglt BgL_sz00_94,
		obj_t BgL_ssz00_95)
	{
		{	/* SawMill/regset.scm 212 */
			if (NULLP(BgL_ssz00_95))
				{	/* SawMill/regset.scm 214 */
					return ((bool_t) 0);
				}
			else
				{	/* SawMill/regset.scm 214 */
					if (NULLP(CDR(BgL_ssz00_95)))
						{	/* SawMill/regset.scm 217 */
							obj_t BgL_arg1705z00_2126;

							BgL_arg1705z00_2126 = CAR(BgL_ssz00_95);
							return
								BGl_regsetzd2unionz12zc0zzsaw_regsetz00(BgL_sz00_94,
								((BgL_regsetz00_bglt) BgL_arg1705z00_2126));
						}
					else
						{
							obj_t BgL_ssz00_2128;
							bool_t BgL_resz00_2129;

							BgL_ssz00_2128 = BgL_ssz00_95;
							BgL_resz00_2129 = ((bool_t) 0);
						BgL_zc3z04anonymousza31706ze3z87_2130:
							if (NULLP(BgL_ssz00_2128))
								{	/* SawMill/regset.scm 221 */
									return BgL_resz00_2129;
								}
							else
								{	/* SawMill/regset.scm 223 */
									obj_t BgL_arg1708z00_2132;
									bool_t BgL_arg1709z00_2133;

									BgL_arg1708z00_2132 = CDR(((obj_t) BgL_ssz00_2128));
									{	/* SawMill/regset.scm 223 */
										bool_t BgL__ortest_1195z00_2134;

										{	/* SawMill/regset.scm 223 */
											obj_t BgL_arg1710z00_2135;

											BgL_arg1710z00_2135 = CAR(((obj_t) BgL_ssz00_2128));
											BgL__ortest_1195z00_2134 =
												BGl_regsetzd2unionz12zc0zzsaw_regsetz00(BgL_sz00_94,
												((BgL_regsetz00_bglt) BgL_arg1710z00_2135));
										}
										if (BgL__ortest_1195z00_2134)
											{	/* SawMill/regset.scm 223 */
												BgL_arg1709z00_2133 = BgL__ortest_1195z00_2134;
											}
										else
											{	/* SawMill/regset.scm 223 */
												BgL_arg1709z00_2133 = BgL_resz00_2129;
											}
									}
									{
										bool_t BgL_resz00_4513;
										obj_t BgL_ssz00_4512;

										BgL_ssz00_4512 = BgL_arg1708z00_2132;
										BgL_resz00_4513 = BgL_arg1709z00_2133;
										BgL_resz00_2129 = BgL_resz00_4513;
										BgL_ssz00_2128 = BgL_ssz00_4512;
										goto BgL_zc3z04anonymousza31706ze3z87_2130;
									}
								}
						}
				}
		}

	}



/* &regset-union*! */
	obj_t BGl_z62regsetzd2unionza2z12z00zzsaw_regsetz00(obj_t BgL_envz00_3473,
		obj_t BgL_sz00_3474, obj_t BgL_ssz00_3475)
	{
		{	/* SawMill/regset.scm 212 */
			return
				BBOOL(BGl_regsetzd2unionza2z12z62zzsaw_regsetz00(
					((BgL_regsetz00_bglt) BgL_sz00_3474), BgL_ssz00_3475));
		}

	}



/* regset-for-each */
	BGL_EXPORTED_DEF obj_t BGl_regsetzd2forzd2eachz00zzsaw_regsetz00(obj_t
		BgL_procz00_96, BgL_regsetz00_bglt BgL_sz00_97)
	{
		{	/* SawMill/regset.scm 228 */
			{
				long BgL_iz00_2139;

				{	/* SawMill/regset.scm 229 */
					bool_t BgL_tmpz00_4517;

					BgL_iz00_2139 = 0L;
				BgL_zc3z04anonymousza31712ze3z87_2140:
					if (
						(BgL_iz00_2139 <
							(long) (
								(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_97))->BgL_msiza7eza7))))
						{	/* SawMill/regset.scm 231 */
							obj_t BgL_ez00_2143;

							BgL_ez00_2143 =
								VECTOR_REF(
								(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_97))->BgL_regvz00),
								BgL_iz00_2139);
							{	/* SawMill/regset.scm 232 */
								bool_t BgL_test2212z00_4524;

								{	/* SawMill/regset.scm 131 */
									long BgL_basez00_3102;
									long BgL_bitz00_3103;

									{	/* SawMill/regset.scm 131 */
										int BgL_arg1576z00_3104;

										{
											BgL_rtl_regzf2razf2_bglt BgL_auxz00_4525;

											{
												obj_t BgL_auxz00_4526;

												{	/* SawMill/regset.scm 131 */
													BgL_objectz00_bglt BgL_tmpz00_4527;

													BgL_tmpz00_4527 =
														((BgL_objectz00_bglt)
														((BgL_rtl_regz00_bglt) BgL_ez00_2143));
													BgL_auxz00_4526 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4527);
												}
												BgL_auxz00_4525 =
													((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4526);
											}
											BgL_arg1576z00_3104 =
												(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4525))->
												BgL_numz00);
										}
										BgL_basez00_3102 = ((long) (BgL_arg1576z00_3104) / 8L);
									}
									{	/* SawMill/regset.scm 132 */
										int BgL_arg1584z00_3105;

										{
											BgL_rtl_regzf2razf2_bglt BgL_auxz00_4535;

											{
												obj_t BgL_auxz00_4536;

												{	/* SawMill/regset.scm 132 */
													BgL_objectz00_bglt BgL_tmpz00_4537;

													BgL_tmpz00_4537 =
														((BgL_objectz00_bglt)
														((BgL_rtl_regz00_bglt) BgL_ez00_2143));
													BgL_auxz00_4536 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4537);
												}
												BgL_auxz00_4535 =
													((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4536);
											}
											BgL_arg1584z00_3105 =
												(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4535))->
												BgL_numz00);
										}
										{	/* SawMill/regset.scm 132 */
											long BgL_n1z00_3115;
											long BgL_n2z00_3116;

											BgL_n1z00_3115 = (long) (BgL_arg1584z00_3105);
											BgL_n2z00_3116 = 8L;
											{	/* SawMill/regset.scm 132 */
												bool_t BgL_test2213z00_4544;

												{	/* SawMill/regset.scm 132 */
													long BgL_arg1338z00_3118;

													BgL_arg1338z00_3118 =
														(((BgL_n1z00_3115) | (BgL_n2z00_3116)) &
														-2147483648);
													BgL_test2213z00_4544 = (BgL_arg1338z00_3118 == 0L);
												}
												if (BgL_test2213z00_4544)
													{	/* SawMill/regset.scm 132 */
														int32_t BgL_arg1334z00_3119;

														{	/* SawMill/regset.scm 132 */
															int32_t BgL_arg1336z00_3120;
															int32_t BgL_arg1337z00_3121;

															BgL_arg1336z00_3120 = (int32_t) (BgL_n1z00_3115);
															BgL_arg1337z00_3121 = (int32_t) (BgL_n2z00_3116);
															BgL_arg1334z00_3119 =
																(BgL_arg1336z00_3120 % BgL_arg1337z00_3121);
														}
														{	/* SawMill/regset.scm 132 */
															long BgL_arg1446z00_3126;

															BgL_arg1446z00_3126 =
																(long) (BgL_arg1334z00_3119);
															BgL_bitz00_3103 = (long) (BgL_arg1446z00_3126);
													}}
												else
													{	/* SawMill/regset.scm 132 */
														BgL_bitz00_3103 = (BgL_n1z00_3115 % BgL_n2z00_3116);
													}
											}
										}
									}
									if (
										(BgL_basez00_3102 <
											STRING_LENGTH(
												(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_97))->
													BgL_stringz00))))
										{	/* SawMill/regset.scm 133 */
											BgL_test2212z00_4524 =
												(
												((STRING_REF(
															(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_97))->
																BgL_stringz00),
															BgL_basez00_3102)) & (1L <<
														(int) (BgL_bitz00_3103))) > 0L);
										}
									else
										{	/* SawMill/regset.scm 133 */
											BgL_test2212z00_4524 = ((bool_t) 0);
										}
								}
								if (BgL_test2212z00_4524)
									{	/* SawMill/regset.scm 232 */
										BGL_PROCEDURE_CALL1(BgL_procz00_96, BgL_ez00_2143);
									}
								else
									{	/* SawMill/regset.scm 232 */
										BFALSE;
									}
							}
							{
								long BgL_iz00_4568;

								BgL_iz00_4568 = (BgL_iz00_2139 + 1L);
								BgL_iz00_2139 = BgL_iz00_4568;
								goto BgL_zc3z04anonymousza31712ze3z87_2140;
							}
						}
					else
						{	/* SawMill/regset.scm 230 */
							BgL_tmpz00_4517 = ((bool_t) 0);
						}
					return BBOOL(BgL_tmpz00_4517);
				}
			}
		}

	}



/* &regset-for-each */
	obj_t BGl_z62regsetzd2forzd2eachz62zzsaw_regsetz00(obj_t BgL_envz00_3476,
		obj_t BgL_procz00_3477, obj_t BgL_sz00_3478)
	{
		{	/* SawMill/regset.scm 228 */
			return
				BGl_regsetzd2forzd2eachz00zzsaw_regsetz00(BgL_procz00_3477,
				((BgL_regsetz00_bglt) BgL_sz00_3478));
		}

	}



/* regset-filter */
	BGL_EXPORTED_DEF obj_t BGl_regsetzd2filterzd2zzsaw_regsetz00(obj_t
		BgL_procz00_98, BgL_regsetz00_bglt BgL_sz00_99)
	{
		{	/* SawMill/regset.scm 238 */
			return BGl_loopze70ze7zzsaw_regsetz00(BgL_procz00_98, BgL_sz00_99, 0L);
		}

	}



/* loop~0 */
	obj_t BGl_loopze70ze7zzsaw_regsetz00(obj_t BgL_procz00_3632,
		BgL_regsetz00_bglt BgL_sz00_3631, long BgL_iz00_2150)
	{
		{	/* SawMill/regset.scm 239 */
		BGl_loopze70ze7zzsaw_regsetz00:
			if (
				(BgL_iz00_2150 <
					(long) (
						(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_3631))->BgL_msiza7eza7))))
				{	/* SawMill/regset.scm 241 */
					obj_t BgL_ez00_2154;

					BgL_ez00_2154 =
						VECTOR_REF(
						(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_3631))->BgL_regvz00),
						BgL_iz00_2150);
					{	/* SawMill/regset.scm 242 */
						bool_t BgL_test2216z00_4580;

						{	/* SawMill/regset.scm 242 */
							bool_t BgL_test2217z00_4581;

							{	/* SawMill/regset.scm 131 */
								long BgL_basez00_3152;
								long BgL_bitz00_3153;

								{	/* SawMill/regset.scm 131 */
									int BgL_arg1576z00_3154;

									{
										BgL_rtl_regzf2razf2_bglt BgL_auxz00_4582;

										{
											obj_t BgL_auxz00_4583;

											{	/* SawMill/regset.scm 131 */
												BgL_objectz00_bglt BgL_tmpz00_4584;

												BgL_tmpz00_4584 =
													((BgL_objectz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_ez00_2154));
												BgL_auxz00_4583 = BGL_OBJECT_WIDENING(BgL_tmpz00_4584);
											}
											BgL_auxz00_4582 =
												((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4583);
										}
										BgL_arg1576z00_3154 =
											(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4582))->
											BgL_numz00);
									}
									BgL_basez00_3152 = ((long) (BgL_arg1576z00_3154) / 8L);
								}
								{	/* SawMill/regset.scm 132 */
									int BgL_arg1584z00_3155;

									{
										BgL_rtl_regzf2razf2_bglt BgL_auxz00_4592;

										{
											obj_t BgL_auxz00_4593;

											{	/* SawMill/regset.scm 132 */
												BgL_objectz00_bglt BgL_tmpz00_4594;

												BgL_tmpz00_4594 =
													((BgL_objectz00_bglt)
													((BgL_rtl_regz00_bglt) BgL_ez00_2154));
												BgL_auxz00_4593 = BGL_OBJECT_WIDENING(BgL_tmpz00_4594);
											}
											BgL_auxz00_4592 =
												((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4593);
										}
										BgL_arg1584z00_3155 =
											(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4592))->
											BgL_numz00);
									}
									{	/* SawMill/regset.scm 132 */
										long BgL_n1z00_3165;
										long BgL_n2z00_3166;

										BgL_n1z00_3165 = (long) (BgL_arg1584z00_3155);
										BgL_n2z00_3166 = 8L;
										{	/* SawMill/regset.scm 132 */
											bool_t BgL_test2218z00_4601;

											{	/* SawMill/regset.scm 132 */
												long BgL_arg1338z00_3168;

												BgL_arg1338z00_3168 =
													(((BgL_n1z00_3165) | (BgL_n2z00_3166)) & -2147483648);
												BgL_test2218z00_4601 = (BgL_arg1338z00_3168 == 0L);
											}
											if (BgL_test2218z00_4601)
												{	/* SawMill/regset.scm 132 */
													int32_t BgL_arg1334z00_3169;

													{	/* SawMill/regset.scm 132 */
														int32_t BgL_arg1336z00_3170;
														int32_t BgL_arg1337z00_3171;

														BgL_arg1336z00_3170 = (int32_t) (BgL_n1z00_3165);
														BgL_arg1337z00_3171 = (int32_t) (BgL_n2z00_3166);
														BgL_arg1334z00_3169 =
															(BgL_arg1336z00_3170 % BgL_arg1337z00_3171);
													}
													{	/* SawMill/regset.scm 132 */
														long BgL_arg1446z00_3176;

														BgL_arg1446z00_3176 = (long) (BgL_arg1334z00_3169);
														BgL_bitz00_3153 = (long) (BgL_arg1446z00_3176);
												}}
											else
												{	/* SawMill/regset.scm 132 */
													BgL_bitz00_3153 = (BgL_n1z00_3165 % BgL_n2z00_3166);
												}
										}
									}
								}
								if (
									(BgL_basez00_3152 <
										STRING_LENGTH(
											(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_3631))->
												BgL_stringz00))))
									{	/* SawMill/regset.scm 133 */
										BgL_test2217z00_4581 =
											(
											((STRING_REF(
														(((BgL_regsetz00_bglt) COBJECT(BgL_sz00_3631))->
															BgL_stringz00),
														BgL_basez00_3152)) & (1L <<
													(int) (BgL_bitz00_3153))) > 0L);
									}
								else
									{	/* SawMill/regset.scm 133 */
										BgL_test2217z00_4581 = ((bool_t) 0);
									}
							}
							if (BgL_test2217z00_4581)
								{	/* SawMill/regset.scm 242 */
									BgL_test2216z00_4580 =
										CBOOL(BGL_PROCEDURE_CALL1(BgL_procz00_3632, BgL_ez00_2154));
								}
							else
								{	/* SawMill/regset.scm 242 */
									BgL_test2216z00_4580 = ((bool_t) 0);
								}
						}
						if (BgL_test2216z00_4580)
							{	/* SawMill/regset.scm 242 */
								return
									MAKE_YOUNG_PAIR(BgL_ez00_2154,
									BGl_loopze70ze7zzsaw_regsetz00(BgL_procz00_3632,
										BgL_sz00_3631, (BgL_iz00_2150 + 1L)));
							}
						else
							{
								long BgL_iz00_4629;

								BgL_iz00_4629 = (BgL_iz00_2150 + 1L);
								BgL_iz00_2150 = BgL_iz00_4629;
								goto BGl_loopze70ze7zzsaw_regsetz00;
							}
					}
				}
			else
				{	/* SawMill/regset.scm 240 */
					return BNIL;
				}
		}

	}



/* &regset-filter */
	obj_t BGl_z62regsetzd2filterzb0zzsaw_regsetz00(obj_t BgL_envz00_3479,
		obj_t BgL_procz00_3480, obj_t BgL_sz00_3481)
	{
		{	/* SawMill/regset.scm 238 */
			return
				BGl_regsetzd2filterzd2zzsaw_regsetz00(BgL_procz00_3480,
				((BgL_regsetz00_bglt) BgL_sz00_3481));
		}

	}



/* regset-dump */
	BGL_EXPORTED_DEF obj_t BGl_regsetzd2dumpzd2zzsaw_regsetz00(BgL_regsetz00_bglt
		BgL_sz00_102, obj_t BgL_pz00_103)
	{
		{	/* SawMill/regset.scm 262 */
			{	/* SawMill/regset.scm 263 */
				obj_t BgL_pz00_2177;

				if (PAIRP(BgL_pz00_103))
					{	/* SawMill/regset.scm 263 */
						BgL_pz00_2177 = CAR(BgL_pz00_103);
					}
				else
					{	/* SawMill/regset.scm 263 */
						obj_t BgL_tmpz00_4636;

						BgL_tmpz00_4636 = BGL_CURRENT_DYNAMIC_ENV();
						BgL_pz00_2177 = BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4636);
					}
				{	/* SawMill/regset.scm 264 */
					obj_t BgL_tmpz00_4639;

					BgL_tmpz00_4639 = ((obj_t) BgL_pz00_2177);
					bgl_display_string(BGl_string2031z00zzsaw_regsetz00, BgL_tmpz00_4639);
				}
				{	/* SawMill/regset.scm 266 */
					obj_t BgL_zc3z04anonymousza31750ze3z87_3482;

					BgL_zc3z04anonymousza31750ze3z87_3482 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31750ze3ze5zzsaw_regsetz00, (int) (1L),
						(int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31750ze3z87_3482, (int) (0L),
						BgL_pz00_2177);
					BGl_regsetzd2forzd2eachz00zzsaw_regsetz00
						(BgL_zc3z04anonymousza31750ze3z87_3482, BgL_sz00_102);
				}
				{	/* SawMill/regset.scm 269 */
					obj_t BgL_tmpz00_4648;

					BgL_tmpz00_4648 = ((obj_t) BgL_pz00_2177);
					return
						bgl_display_string(BGl_string2032z00zzsaw_regsetz00,
						BgL_tmpz00_4648);
				}
			}
		}

	}



/* &regset-dump */
	obj_t BGl_z62regsetzd2dumpzb0zzsaw_regsetz00(obj_t BgL_envz00_3483,
		obj_t BgL_sz00_3484, obj_t BgL_pz00_3485)
	{
		{	/* SawMill/regset.scm 262 */
			return
				BGl_regsetzd2dumpzd2zzsaw_regsetz00(
				((BgL_regsetz00_bglt) BgL_sz00_3484), BgL_pz00_3485);
		}

	}



/* &<@anonymous:1750> */
	obj_t BGl_z62zc3z04anonymousza31750ze3ze5zzsaw_regsetz00(obj_t
		BgL_envz00_3486, obj_t BgL_ez00_3488)
	{
		{	/* SawMill/regset.scm 265 */
			{	/* SawMill/regset.scm 266 */
				obj_t BgL_pz00_3487;

				BgL_pz00_3487 = PROCEDURE_REF(BgL_envz00_3486, (int) (0L));
				{	/* SawMill/regset.scm 266 */
					obj_t BgL_tmpz00_4655;

					BgL_tmpz00_4655 = ((obj_t) BgL_pz00_3487);
					bgl_display_string(BGl_string2033z00zzsaw_regsetz00, BgL_tmpz00_4655);
				}
				return
					BGl_dumpz00zzsaw_defsz00(BgL_ez00_3488, BgL_pz00_3487, (int) (0L));
		}}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_regsetz00(void)
	{
		{	/* SawMill/regset.scm 15 */
			{	/* SawMill/regset.scm 30 */
				obj_t BgL_arg1761z00_2187;
				obj_t BgL_arg1762z00_2188;

				{	/* SawMill/regset.scm 30 */
					obj_t BgL_v1419z00_2222;

					BgL_v1419z00_2222 = create_vector(6L);
					{	/* SawMill/regset.scm 30 */
						obj_t BgL_arg1799z00_2223;

						BgL_arg1799z00_2223 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(0),
							BGl_proc2035z00zzsaw_regsetz00, BGl_proc2034z00zzsaw_regsetz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1419z00_2222, 0L, BgL_arg1799z00_2223);
					}
					{	/* SawMill/regset.scm 30 */
						obj_t BgL_arg1812z00_2233;

						BgL_arg1812z00_2233 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc2038z00zzsaw_regsetz00, BGl_proc2037z00zzsaw_regsetz00,
							((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2036z00zzsaw_regsetz00, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1419z00_2222, 1L, BgL_arg1812z00_2233);
					}
					{	/* SawMill/regset.scm 30 */
						obj_t BgL_arg1831z00_2246;

						BgL_arg1831z00_2246 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(4),
							BGl_proc2041z00zzsaw_regsetz00, BGl_proc2040z00zzsaw_regsetz00,
							((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2039z00zzsaw_regsetz00, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1419z00_2222, 2L, BgL_arg1831z00_2246);
					}
					{	/* SawMill/regset.scm 30 */
						obj_t BgL_arg1838z00_2259;

						BgL_arg1838z00_2259 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(5),
							BGl_proc2044z00zzsaw_regsetz00, BGl_proc2043z00zzsaw_regsetz00,
							((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2042z00zzsaw_regsetz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1419z00_2222, 3L, BgL_arg1838z00_2259);
					}
					{	/* SawMill/regset.scm 30 */
						obj_t BgL_arg1846z00_2272;

						BgL_arg1846z00_2272 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc2047z00zzsaw_regsetz00, BGl_proc2046z00zzsaw_regsetz00,
							((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2045z00zzsaw_regsetz00, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1419z00_2222, 4L, BgL_arg1846z00_2272);
					}
					{	/* SawMill/regset.scm 30 */
						obj_t BgL_arg1853z00_2285;

						BgL_arg1853z00_2285 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(7),
							BGl_proc2050z00zzsaw_regsetz00, BGl_proc2049z00zzsaw_regsetz00,
							((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2048z00zzsaw_regsetz00, CNST_TABLE_REF(3));
						VECTOR_SET(BgL_v1419z00_2222, 5L, BgL_arg1853z00_2285);
					}
					BgL_arg1761z00_2187 = BgL_v1419z00_2222;
				}
				{	/* SawMill/regset.scm 30 */
					obj_t BgL_v1420z00_2298;

					BgL_v1420z00_2298 = create_vector(0L);
					BgL_arg1762z00_2188 = BgL_v1420z00_2298;
				}
				BGl_rtl_regzf2razf2zzsaw_regsetz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(8),
					CNST_TABLE_REF(9), BGl_rtl_regz00zzsaw_defsz00, 60121L,
					BGl_proc2054z00zzsaw_regsetz00, BGl_proc2053z00zzsaw_regsetz00,
					BFALSE, BGl_proc2052z00zzsaw_regsetz00,
					BGl_proc2051z00zzsaw_regsetz00, BgL_arg1761z00_2187,
					BgL_arg1762z00_2188);
			}
			{	/* SawMill/regset.scm 38 */
				obj_t BgL_arg1866z00_2306;
				obj_t BgL_arg1868z00_2307;

				{	/* SawMill/regset.scm 38 */
					obj_t BgL_v1421z00_2322;

					BgL_v1421z00_2322 = create_vector(5L);
					{	/* SawMill/regset.scm 38 */
						obj_t BgL_arg1874z00_2323;

						BgL_arg1874z00_2323 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(10),
							BGl_proc2057z00zzsaw_regsetz00, BGl_proc2056z00zzsaw_regsetz00,
							((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2055z00zzsaw_regsetz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1421z00_2322, 0L, BgL_arg1874z00_2323);
					}
					{	/* SawMill/regset.scm 38 */
						obj_t BgL_arg1882z00_2336;

						BgL_arg1882z00_2336 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(11),
							BGl_proc2059z00zzsaw_regsetz00, BGl_proc2058z00zzsaw_regsetz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1421z00_2322, 1L, BgL_arg1882z00_2336);
					}
					{	/* SawMill/regset.scm 38 */
						obj_t BgL_arg1887z00_2346;

						BgL_arg1887z00_2346 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(12),
							BGl_proc2061z00zzsaw_regsetz00, BGl_proc2060z00zzsaw_regsetz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(13));
						VECTOR_SET(BgL_v1421z00_2322, 2L, BgL_arg1887z00_2346);
					}
					{	/* SawMill/regset.scm 38 */
						obj_t BgL_arg1892z00_2356;

						BgL_arg1892z00_2356 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(14),
							BGl_proc2063z00zzsaw_regsetz00, BGl_proc2062z00zzsaw_regsetz00,
							((bool_t) 1), ((bool_t) 0), BFALSE, BFALSE, CNST_TABLE_REF(15));
						VECTOR_SET(BgL_v1421z00_2322, 3L, BgL_arg1892z00_2356);
					}
					{	/* SawMill/regset.scm 38 */
						obj_t BgL_arg1897z00_2366;

						BgL_arg1897z00_2366 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(16),
							BGl_proc2066z00zzsaw_regsetz00, BGl_proc2065z00zzsaw_regsetz00,
							((bool_t) 0), ((bool_t) 0), BFALSE,
							BGl_proc2064z00zzsaw_regsetz00, CNST_TABLE_REF(17));
						VECTOR_SET(BgL_v1421z00_2322, 4L, BgL_arg1897z00_2366);
					}
					BgL_arg1866z00_2306 = BgL_v1421z00_2322;
				}
				{	/* SawMill/regset.scm 38 */
					obj_t BgL_v1422z00_2379;

					BgL_v1422z00_2379 = create_vector(0L);
					BgL_arg1868z00_2307 = BgL_v1422z00_2379;
				}
				return (BGl_regsetz00zzsaw_regsetz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(18),
						CNST_TABLE_REF(9), BGl_objectz00zz__objectz00, 42366L,
						BGl_proc2069z00zzsaw_regsetz00, BGl_proc2068z00zzsaw_regsetz00,
						BFALSE, BGl_proc2067z00zzsaw_regsetz00, BFALSE, BgL_arg1866z00_2306,
						BgL_arg1868z00_2307), BUNSPEC);
			}
		}

	}



/* &<@anonymous:1873> */
	obj_t BGl_z62zc3z04anonymousza31873ze3ze5zzsaw_regsetz00(obj_t
		BgL_envz00_3525, obj_t BgL_new1175z00_3526)
	{
		{	/* SawMill/regset.scm 38 */
			{
				BgL_regsetz00_bglt BgL_auxz00_4714;

				((((BgL_regsetz00_bglt) COBJECT(
								((BgL_regsetz00_bglt) BgL_new1175z00_3526)))->BgL_lengthz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(((BgL_regsetz00_bglt)
									BgL_new1175z00_3526)))->BgL_msiza7eza7) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(((BgL_regsetz00_bglt)
									BgL_new1175z00_3526)))->BgL_regvz00) =
					((obj_t) CNST_TABLE_REF(19)), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(((BgL_regsetz00_bglt)
									BgL_new1175z00_3526)))->BgL_reglz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_regsetz00_bglt) COBJECT(((BgL_regsetz00_bglt)
									BgL_new1175z00_3526)))->BgL_stringz00) =
					((obj_t) BGl_string2070z00zzsaw_regsetz00), BUNSPEC);
				BgL_auxz00_4714 = ((BgL_regsetz00_bglt) BgL_new1175z00_3526);
				return ((obj_t) BgL_auxz00_4714);
			}
		}

	}



/* &lambda1871 */
	BgL_regsetz00_bglt BGl_z62lambda1871z62zzsaw_regsetz00(obj_t BgL_envz00_3527)
	{
		{	/* SawMill/regset.scm 38 */
			{	/* SawMill/regset.scm 38 */
				BgL_regsetz00_bglt BgL_new1174z00_3697;

				BgL_new1174z00_3697 =
					((BgL_regsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_regsetz00_bgl))));
				{	/* SawMill/regset.scm 38 */
					long BgL_arg1872z00_3698;

					BgL_arg1872z00_3698 = BGL_CLASS_NUM(BGl_regsetz00zzsaw_regsetz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt) BgL_new1174z00_3697), BgL_arg1872z00_3698);
				}
				return BgL_new1174z00_3697;
			}
		}

	}



/* &lambda1869 */
	BgL_regsetz00_bglt BGl_z62lambda1869z62zzsaw_regsetz00(obj_t BgL_envz00_3528,
		obj_t BgL_length1169z00_3529, obj_t BgL_msiza7e1170za7_3530,
		obj_t BgL_regv1171z00_3531, obj_t BgL_regl1172z00_3532,
		obj_t BgL_string1173z00_3533)
	{
		{	/* SawMill/regset.scm 38 */
			{	/* SawMill/regset.scm 38 */
				int BgL_length1169z00_3699;
				int BgL_msiza7e1170za7_3700;

				BgL_length1169z00_3699 = CINT(BgL_length1169z00_3529);
				BgL_msiza7e1170za7_3700 = CINT(BgL_msiza7e1170za7_3530);
				{	/* SawMill/regset.scm 38 */
					BgL_regsetz00_bglt BgL_new1204z00_3704;

					{	/* SawMill/regset.scm 38 */
						BgL_regsetz00_bglt BgL_new1202z00_3705;

						BgL_new1202z00_3705 =
							((BgL_regsetz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_regsetz00_bgl))));
						{	/* SawMill/regset.scm 38 */
							long BgL_arg1870z00_3706;

							BgL_arg1870z00_3706 = BGL_CLASS_NUM(BGl_regsetz00zzsaw_regsetz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1202z00_3705),
								BgL_arg1870z00_3706);
						}
						BgL_new1204z00_3704 = BgL_new1202z00_3705;
					}
					((((BgL_regsetz00_bglt) COBJECT(BgL_new1204z00_3704))->
							BgL_lengthz00) = ((int) BgL_length1169z00_3699), BUNSPEC);
					((((BgL_regsetz00_bglt) COBJECT(BgL_new1204z00_3704))->
							BgL_msiza7eza7) = ((int) BgL_msiza7e1170za7_3700), BUNSPEC);
					((((BgL_regsetz00_bglt) COBJECT(BgL_new1204z00_3704))->BgL_regvz00) =
						((obj_t) ((obj_t) BgL_regv1171z00_3531)), BUNSPEC);
					((((BgL_regsetz00_bglt) COBJECT(BgL_new1204z00_3704))->BgL_reglz00) =
						((obj_t) ((obj_t) BgL_regl1172z00_3532)), BUNSPEC);
					((((BgL_regsetz00_bglt) COBJECT(BgL_new1204z00_3704))->
							BgL_stringz00) =
						((obj_t) ((obj_t) BgL_string1173z00_3533)), BUNSPEC);
					return BgL_new1204z00_3704;
				}
			}
		}

	}



/* &<@anonymous:1904> */
	obj_t BGl_z62zc3z04anonymousza31904ze3ze5zzsaw_regsetz00(obj_t
		BgL_envz00_3534)
	{
		{	/* SawMill/regset.scm 38 */
			return BGl_string2070z00zzsaw_regsetz00;
		}

	}



/* &lambda1903 */
	obj_t BGl_z62lambda1903z62zzsaw_regsetz00(obj_t BgL_envz00_3535,
		obj_t BgL_oz00_3536, obj_t BgL_vz00_3537)
	{
		{	/* SawMill/regset.scm 38 */
			return
				((((BgL_regsetz00_bglt) COBJECT(
							((BgL_regsetz00_bglt) BgL_oz00_3536)))->BgL_stringz00) = ((obj_t)
					((obj_t) BgL_vz00_3537)), BUNSPEC);
		}

	}



/* &lambda1902 */
	obj_t BGl_z62lambda1902z62zzsaw_regsetz00(obj_t BgL_envz00_3538,
		obj_t BgL_oz00_3539)
	{
		{	/* SawMill/regset.scm 38 */
			return
				(((BgL_regsetz00_bglt) COBJECT(
						((BgL_regsetz00_bglt) BgL_oz00_3539)))->BgL_stringz00);
		}

	}



/* &lambda1896 */
	obj_t BGl_z62lambda1896z62zzsaw_regsetz00(obj_t BgL_envz00_3540,
		obj_t BgL_oz00_3541, obj_t BgL_vz00_3542)
	{
		{	/* SawMill/regset.scm 38 */
			return
				((((BgL_regsetz00_bglt) COBJECT(
							((BgL_regsetz00_bglt) BgL_oz00_3541)))->BgL_reglz00) = ((obj_t)
					((obj_t) BgL_vz00_3542)), BUNSPEC);
		}

	}



/* &lambda1895 */
	obj_t BGl_z62lambda1895z62zzsaw_regsetz00(obj_t BgL_envz00_3543,
		obj_t BgL_oz00_3544)
	{
		{	/* SawMill/regset.scm 38 */
			return
				(((BgL_regsetz00_bglt) COBJECT(
						((BgL_regsetz00_bglt) BgL_oz00_3544)))->BgL_reglz00);
		}

	}



/* &lambda1891 */
	obj_t BGl_z62lambda1891z62zzsaw_regsetz00(obj_t BgL_envz00_3545,
		obj_t BgL_oz00_3546, obj_t BgL_vz00_3547)
	{
		{	/* SawMill/regset.scm 38 */
			return
				((((BgL_regsetz00_bglt) COBJECT(
							((BgL_regsetz00_bglt) BgL_oz00_3546)))->BgL_regvz00) = ((obj_t)
					((obj_t) BgL_vz00_3547)), BUNSPEC);
		}

	}



/* &lambda1890 */
	obj_t BGl_z62lambda1890z62zzsaw_regsetz00(obj_t BgL_envz00_3548,
		obj_t BgL_oz00_3549)
	{
		{	/* SawMill/regset.scm 38 */
			return
				(((BgL_regsetz00_bglt) COBJECT(
						((BgL_regsetz00_bglt) BgL_oz00_3549)))->BgL_regvz00);
		}

	}



/* &lambda1886 */
	obj_t BGl_z62lambda1886z62zzsaw_regsetz00(obj_t BgL_envz00_3550,
		obj_t BgL_oz00_3551, obj_t BgL_vz00_3552)
	{
		{	/* SawMill/regset.scm 38 */
			{	/* SawMill/regset.scm 38 */
				int BgL_vz00_3717;

				BgL_vz00_3717 = CINT(BgL_vz00_3552);
				return
					((((BgL_regsetz00_bglt) COBJECT(
								((BgL_regsetz00_bglt) BgL_oz00_3551)))->BgL_msiza7eza7) =
					((int) BgL_vz00_3717), BUNSPEC);
		}}

	}



/* &lambda1885 */
	obj_t BGl_z62lambda1885z62zzsaw_regsetz00(obj_t BgL_envz00_3553,
		obj_t BgL_oz00_3554)
	{
		{	/* SawMill/regset.scm 38 */
			return
				BINT(
				(((BgL_regsetz00_bglt) COBJECT(
							((BgL_regsetz00_bglt) BgL_oz00_3554)))->BgL_msiza7eza7));
		}

	}



/* &<@anonymous:1880> */
	obj_t BGl_z62zc3z04anonymousza31880ze3ze5zzsaw_regsetz00(obj_t
		BgL_envz00_3555)
	{
		{	/* SawMill/regset.scm 38 */
			return BINT(0L);
		}

	}



/* &lambda1879 */
	obj_t BGl_z62lambda1879z62zzsaw_regsetz00(obj_t BgL_envz00_3556,
		obj_t BgL_oz00_3557, obj_t BgL_vz00_3558)
	{
		{	/* SawMill/regset.scm 38 */
			{	/* SawMill/regset.scm 38 */
				int BgL_vz00_3720;

				BgL_vz00_3720 = CINT(BgL_vz00_3558);
				return
					((((BgL_regsetz00_bglt) COBJECT(
								((BgL_regsetz00_bglt) BgL_oz00_3557)))->BgL_lengthz00) =
					((int) BgL_vz00_3720), BUNSPEC);
		}}

	}



/* &lambda1878 */
	obj_t BGl_z62lambda1878z62zzsaw_regsetz00(obj_t BgL_envz00_3559,
		obj_t BgL_oz00_3560)
	{
		{	/* SawMill/regset.scm 38 */
			return
				BINT(
				(((BgL_regsetz00_bglt) COBJECT(
							((BgL_regsetz00_bglt) BgL_oz00_3560)))->BgL_lengthz00));
		}

	}



/* &lambda1772 */
	BgL_rtl_regz00_bglt BGl_z62lambda1772z62zzsaw_regsetz00(obj_t BgL_envz00_3561,
		obj_t BgL_o1167z00_3562)
	{
		{	/* SawMill/regset.scm 30 */
			{	/* SawMill/regset.scm 30 */
				long BgL_arg1773z00_3723;

				{	/* SawMill/regset.scm 30 */
					obj_t BgL_arg1775z00_3724;

					{	/* SawMill/regset.scm 30 */
						obj_t BgL_arg1798z00_3725;

						{	/* SawMill/regset.scm 30 */
							obj_t BgL_arg1815z00_3726;
							long BgL_arg1816z00_3727;

							BgL_arg1815z00_3726 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/regset.scm 30 */
								long BgL_arg1817z00_3728;

								BgL_arg1817z00_3728 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_o1167z00_3562)));
								BgL_arg1816z00_3727 = (BgL_arg1817z00_3728 - OBJECT_TYPE);
							}
							BgL_arg1798z00_3725 =
								VECTOR_REF(BgL_arg1815z00_3726, BgL_arg1816z00_3727);
						}
						BgL_arg1775z00_3724 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1798z00_3725);
					}
					{	/* SawMill/regset.scm 30 */
						obj_t BgL_tmpz00_4783;

						BgL_tmpz00_4783 = ((obj_t) BgL_arg1775z00_3724);
						BgL_arg1773z00_3723 = BGL_CLASS_NUM(BgL_tmpz00_4783);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_rtl_regz00_bglt) BgL_o1167z00_3562)), BgL_arg1773z00_3723);
			}
			{	/* SawMill/regset.scm 30 */
				BgL_objectz00_bglt BgL_tmpz00_4789;

				BgL_tmpz00_4789 =
					((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1167z00_3562));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4789, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1167z00_3562));
			return ((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1167z00_3562));
		}

	}



/* &<@anonymous:1771> */
	obj_t BGl_z62zc3z04anonymousza31771ze3ze5zzsaw_regsetz00(obj_t
		BgL_envz00_3563, obj_t BgL_new1166z00_3564)
	{
		{	/* SawMill/regset.scm 30 */
			{
				BgL_rtl_regz00_bglt BgL_auxz00_4797;

				{
					BgL_typez00_bglt BgL_auxz00_4798;

					{	/* SawMill/regset.scm 30 */
						obj_t BgL_classz00_3730;

						BgL_classz00_3730 = BGl_typez00zztype_typez00;
						{	/* SawMill/regset.scm 30 */
							obj_t BgL__ortest_1117z00_3731;

							BgL__ortest_1117z00_3731 = BGL_CLASS_NIL(BgL_classz00_3730);
							if (CBOOL(BgL__ortest_1117z00_3731))
								{	/* SawMill/regset.scm 30 */
									BgL_auxz00_4798 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3731);
								}
							else
								{	/* SawMill/regset.scm 30 */
									BgL_auxz00_4798 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3730));
								}
						}
					}
					((((BgL_rtl_regz00_bglt) COBJECT(
									((BgL_rtl_regz00_bglt)
										((BgL_rtl_regz00_bglt) BgL_new1166z00_3564))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_4798), BUNSPEC);
				}
				((((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt)
									((BgL_rtl_regz00_bglt) BgL_new1166z00_3564))))->BgL_varz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1166z00_3564))))->BgL_onexprzf3zf3) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1166z00_3564))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1166z00_3564))))->BgL_keyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1166z00_3564))))->BgL_debugnamez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_rtl_regz00_bglt)
							COBJECT(((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt)
										BgL_new1166z00_3564))))->BgL_hardwarez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4826;

					{
						obj_t BgL_auxz00_4827;

						{	/* SawMill/regset.scm 30 */
							BgL_objectz00_bglt BgL_tmpz00_4828;

							BgL_tmpz00_4828 =
								((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_new1166z00_3564));
							BgL_auxz00_4827 = BGL_OBJECT_WIDENING(BgL_tmpz00_4828);
						}
						BgL_auxz00_4826 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4827);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4826))->BgL_numz00) =
						((int) (int) (0L)), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4835;

					{
						obj_t BgL_auxz00_4836;

						{	/* SawMill/regset.scm 30 */
							BgL_objectz00_bglt BgL_tmpz00_4837;

							BgL_tmpz00_4837 =
								((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_new1166z00_3564));
							BgL_auxz00_4836 = BGL_OBJECT_WIDENING(BgL_tmpz00_4837);
						}
						BgL_auxz00_4835 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4836);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4835))->
							BgL_colorz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4843;

					{
						obj_t BgL_auxz00_4844;

						{	/* SawMill/regset.scm 30 */
							BgL_objectz00_bglt BgL_tmpz00_4845;

							BgL_tmpz00_4845 =
								((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_new1166z00_3564));
							BgL_auxz00_4844 = BGL_OBJECT_WIDENING(BgL_tmpz00_4845);
						}
						BgL_auxz00_4843 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4844);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4843))->
							BgL_coalescez00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4851;

					{
						obj_t BgL_auxz00_4852;

						{	/* SawMill/regset.scm 30 */
							BgL_objectz00_bglt BgL_tmpz00_4853;

							BgL_tmpz00_4853 =
								((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_new1166z00_3564));
							BgL_auxz00_4852 = BGL_OBJECT_WIDENING(BgL_tmpz00_4853);
						}
						BgL_auxz00_4851 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4852);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4851))->
							BgL_occurrencesz00) = ((int) (int) (0L)), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4860;

					{
						obj_t BgL_auxz00_4861;

						{	/* SawMill/regset.scm 30 */
							BgL_objectz00_bglt BgL_tmpz00_4862;

							BgL_tmpz00_4862 =
								((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_new1166z00_3564));
							BgL_auxz00_4861 = BGL_OBJECT_WIDENING(BgL_tmpz00_4862);
						}
						BgL_auxz00_4860 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4861);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4860))->
							BgL_interferez00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_4868;

					{
						obj_t BgL_auxz00_4869;

						{	/* SawMill/regset.scm 30 */
							BgL_objectz00_bglt BgL_tmpz00_4870;

							BgL_tmpz00_4870 =
								((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_new1166z00_3564));
							BgL_auxz00_4869 = BGL_OBJECT_WIDENING(BgL_tmpz00_4870);
						}
						BgL_auxz00_4868 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4869);
					}
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4868))->
							BgL_interfere2z00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_4797 = ((BgL_rtl_regz00_bglt) BgL_new1166z00_3564);
				return ((obj_t) BgL_auxz00_4797);
			}
		}

	}



/* &lambda1768 */
	BgL_rtl_regz00_bglt BGl_z62lambda1768z62zzsaw_regsetz00(obj_t BgL_envz00_3565,
		obj_t BgL_o1163z00_3566)
	{
		{	/* SawMill/regset.scm 30 */
			{	/* SawMill/regset.scm 30 */
				BgL_rtl_regzf2razf2_bglt BgL_wide1165z00_3733;

				BgL_wide1165z00_3733 =
					((BgL_rtl_regzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_rtl_regzf2razf2_bgl))));
				{	/* SawMill/regset.scm 30 */
					obj_t BgL_auxz00_4883;
					BgL_objectz00_bglt BgL_tmpz00_4879;

					BgL_auxz00_4883 = ((obj_t) BgL_wide1165z00_3733);
					BgL_tmpz00_4879 =
						((BgL_objectz00_bglt)
						((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1163z00_3566)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4879, BgL_auxz00_4883);
				}
				((BgL_objectz00_bglt)
					((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1163z00_3566)));
				{	/* SawMill/regset.scm 30 */
					long BgL_arg1770z00_3734;

					BgL_arg1770z00_3734 =
						BGL_CLASS_NUM(BGl_rtl_regzf2razf2zzsaw_regsetz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_rtl_regz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_o1163z00_3566))),
						BgL_arg1770z00_3734);
				}
				return
					((BgL_rtl_regz00_bglt)
					((BgL_rtl_regz00_bglt) ((BgL_rtl_regz00_bglt) BgL_o1163z00_3566)));
			}
		}

	}



/* &lambda1763 */
	BgL_rtl_regz00_bglt BGl_z62lambda1763z62zzsaw_regsetz00(obj_t BgL_envz00_3567,
		obj_t BgL_type1150z00_3568, obj_t BgL_var1151z00_3569,
		obj_t BgL_onexprzf31152zf3_3570, obj_t BgL_name1153z00_3571,
		obj_t BgL_key1154z00_3572, obj_t BgL_debugname1155z00_3573,
		obj_t BgL_hardware1156z00_3574, obj_t BgL_num1157z00_3575,
		obj_t BgL_color1158z00_3576, obj_t BgL_coalesce1159z00_3577,
		obj_t BgL_occurrences1160z00_3578, obj_t BgL_interfere1161z00_3579,
		obj_t BgL_interfere21162z00_3580)
	{
		{	/* SawMill/regset.scm 30 */
			{	/* SawMill/regset.scm 30 */
				int BgL_num1157z00_3736;
				int BgL_occurrences1160z00_3737;

				BgL_num1157z00_3736 = CINT(BgL_num1157z00_3575);
				BgL_occurrences1160z00_3737 = CINT(BgL_occurrences1160z00_3578);
				{	/* SawMill/regset.scm 30 */
					BgL_rtl_regz00_bglt BgL_new1200z00_3738;

					{	/* SawMill/regset.scm 30 */
						BgL_rtl_regz00_bglt BgL_tmp1198z00_3739;
						BgL_rtl_regzf2razf2_bglt BgL_wide1199z00_3740;

						{
							BgL_rtl_regz00_bglt BgL_auxz00_4899;

							{	/* SawMill/regset.scm 30 */
								BgL_rtl_regz00_bglt BgL_new1197z00_3741;

								BgL_new1197z00_3741 =
									((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_rtl_regz00_bgl))));
								{	/* SawMill/regset.scm 30 */
									long BgL_arg1767z00_3742;

									BgL_arg1767z00_3742 =
										BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1197z00_3741),
										BgL_arg1767z00_3742);
								}
								{	/* SawMill/regset.scm 30 */
									BgL_objectz00_bglt BgL_tmpz00_4904;

									BgL_tmpz00_4904 = ((BgL_objectz00_bglt) BgL_new1197z00_3741);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4904, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1197z00_3741);
								BgL_auxz00_4899 = BgL_new1197z00_3741;
							}
							BgL_tmp1198z00_3739 = ((BgL_rtl_regz00_bglt) BgL_auxz00_4899);
						}
						BgL_wide1199z00_3740 =
							((BgL_rtl_regzf2razf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_regzf2razf2_bgl))));
						{	/* SawMill/regset.scm 30 */
							obj_t BgL_auxz00_4912;
							BgL_objectz00_bglt BgL_tmpz00_4910;

							BgL_auxz00_4912 = ((obj_t) BgL_wide1199z00_3740);
							BgL_tmpz00_4910 = ((BgL_objectz00_bglt) BgL_tmp1198z00_3739);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4910, BgL_auxz00_4912);
						}
						((BgL_objectz00_bglt) BgL_tmp1198z00_3739);
						{	/* SawMill/regset.scm 30 */
							long BgL_arg1765z00_3743;

							BgL_arg1765z00_3743 =
								BGL_CLASS_NUM(BGl_rtl_regzf2razf2zzsaw_regsetz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1198z00_3739),
								BgL_arg1765z00_3743);
						}
						BgL_new1200z00_3738 = ((BgL_rtl_regz00_bglt) BgL_tmp1198z00_3739);
					}
					((((BgL_rtl_regz00_bglt) COBJECT(
									((BgL_rtl_regz00_bglt) BgL_new1200z00_3738)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1150z00_3568)),
						BUNSPEC);
					((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
										BgL_new1200z00_3738)))->BgL_varz00) =
						((obj_t) BgL_var1151z00_3569), BUNSPEC);
					((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
										BgL_new1200z00_3738)))->BgL_onexprzf3zf3) =
						((obj_t) BgL_onexprzf31152zf3_3570), BUNSPEC);
					((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
										BgL_new1200z00_3738)))->BgL_namez00) =
						((obj_t) BgL_name1153z00_3571), BUNSPEC);
					((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
										BgL_new1200z00_3738)))->BgL_keyz00) =
						((obj_t) BgL_key1154z00_3572), BUNSPEC);
					((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
										BgL_new1200z00_3738)))->BgL_debugnamez00) =
						((obj_t) BgL_debugname1155z00_3573), BUNSPEC);
					((((BgL_rtl_regz00_bglt) COBJECT(((BgL_rtl_regz00_bglt)
										BgL_new1200z00_3738)))->BgL_hardwarez00) =
						((obj_t) BgL_hardware1156z00_3574), BUNSPEC);
					{
						BgL_rtl_regzf2razf2_bglt BgL_auxz00_4935;

						{
							obj_t BgL_auxz00_4936;

							{	/* SawMill/regset.scm 30 */
								BgL_objectz00_bglt BgL_tmpz00_4937;

								BgL_tmpz00_4937 = ((BgL_objectz00_bglt) BgL_new1200z00_3738);
								BgL_auxz00_4936 = BGL_OBJECT_WIDENING(BgL_tmpz00_4937);
							}
							BgL_auxz00_4935 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4936);
						}
						((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4935))->
								BgL_numz00) = ((int) BgL_num1157z00_3736), BUNSPEC);
					}
					{
						BgL_rtl_regzf2razf2_bglt BgL_auxz00_4942;

						{
							obj_t BgL_auxz00_4943;

							{	/* SawMill/regset.scm 30 */
								BgL_objectz00_bglt BgL_tmpz00_4944;

								BgL_tmpz00_4944 = ((BgL_objectz00_bglt) BgL_new1200z00_3738);
								BgL_auxz00_4943 = BGL_OBJECT_WIDENING(BgL_tmpz00_4944);
							}
							BgL_auxz00_4942 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4943);
						}
						((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4942))->
								BgL_colorz00) = ((obj_t) BgL_color1158z00_3576), BUNSPEC);
					}
					{
						BgL_rtl_regzf2razf2_bglt BgL_auxz00_4949;

						{
							obj_t BgL_auxz00_4950;

							{	/* SawMill/regset.scm 30 */
								BgL_objectz00_bglt BgL_tmpz00_4951;

								BgL_tmpz00_4951 = ((BgL_objectz00_bglt) BgL_new1200z00_3738);
								BgL_auxz00_4950 = BGL_OBJECT_WIDENING(BgL_tmpz00_4951);
							}
							BgL_auxz00_4949 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4950);
						}
						((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4949))->
								BgL_coalescez00) = ((obj_t) BgL_coalesce1159z00_3577), BUNSPEC);
					}
					{
						BgL_rtl_regzf2razf2_bglt BgL_auxz00_4956;

						{
							obj_t BgL_auxz00_4957;

							{	/* SawMill/regset.scm 30 */
								BgL_objectz00_bglt BgL_tmpz00_4958;

								BgL_tmpz00_4958 = ((BgL_objectz00_bglt) BgL_new1200z00_3738);
								BgL_auxz00_4957 = BGL_OBJECT_WIDENING(BgL_tmpz00_4958);
							}
							BgL_auxz00_4956 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4957);
						}
						((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4956))->
								BgL_occurrencesz00) =
							((int) BgL_occurrences1160z00_3737), BUNSPEC);
					}
					{
						BgL_rtl_regzf2razf2_bglt BgL_auxz00_4963;

						{
							obj_t BgL_auxz00_4964;

							{	/* SawMill/regset.scm 30 */
								BgL_objectz00_bglt BgL_tmpz00_4965;

								BgL_tmpz00_4965 = ((BgL_objectz00_bglt) BgL_new1200z00_3738);
								BgL_auxz00_4964 = BGL_OBJECT_WIDENING(BgL_tmpz00_4965);
							}
							BgL_auxz00_4963 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4964);
						}
						((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4963))->
								BgL_interferez00) =
							((obj_t) BgL_interfere1161z00_3579), BUNSPEC);
					}
					{
						BgL_rtl_regzf2razf2_bglt BgL_auxz00_4970;

						{
							obj_t BgL_auxz00_4971;

							{	/* SawMill/regset.scm 30 */
								BgL_objectz00_bglt BgL_tmpz00_4972;

								BgL_tmpz00_4972 = ((BgL_objectz00_bglt) BgL_new1200z00_3738);
								BgL_auxz00_4971 = BGL_OBJECT_WIDENING(BgL_tmpz00_4972);
							}
							BgL_auxz00_4970 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4971);
						}
						((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4970))->
								BgL_interfere2z00) =
							((obj_t) BgL_interfere21162z00_3580), BUNSPEC);
					}
					return BgL_new1200z00_3738;
				}
			}
		}

	}



/* &<@anonymous:1860> */
	obj_t BGl_z62zc3z04anonymousza31860ze3ze5zzsaw_regsetz00(obj_t
		BgL_envz00_3581)
	{
		{	/* SawMill/regset.scm 30 */
			return BUNSPEC;
		}

	}



/* &lambda1859 */
	obj_t BGl_z62lambda1859z62zzsaw_regsetz00(obj_t BgL_envz00_3582,
		obj_t BgL_oz00_3583, obj_t BgL_vz00_3584)
	{
		{	/* SawMill/regset.scm 30 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4977;

				{
					obj_t BgL_auxz00_4978;

					{	/* SawMill/regset.scm 30 */
						BgL_objectz00_bglt BgL_tmpz00_4979;

						BgL_tmpz00_4979 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3583));
						BgL_auxz00_4978 = BGL_OBJECT_WIDENING(BgL_tmpz00_4979);
					}
					BgL_auxz00_4977 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4978);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4977))->
						BgL_interfere2z00) = ((obj_t) BgL_vz00_3584), BUNSPEC);
			}
		}

	}



/* &lambda1858 */
	obj_t BGl_z62lambda1858z62zzsaw_regsetz00(obj_t BgL_envz00_3585,
		obj_t BgL_oz00_3586)
	{
		{	/* SawMill/regset.scm 30 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4985;

				{
					obj_t BgL_auxz00_4986;

					{	/* SawMill/regset.scm 30 */
						BgL_objectz00_bglt BgL_tmpz00_4987;

						BgL_tmpz00_4987 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3586));
						BgL_auxz00_4986 = BGL_OBJECT_WIDENING(BgL_tmpz00_4987);
					}
					BgL_auxz00_4985 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4986);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4985))->
					BgL_interfere2z00);
			}
		}

	}



/* &<@anonymous:1852> */
	obj_t BGl_z62zc3z04anonymousza31852ze3ze5zzsaw_regsetz00(obj_t
		BgL_envz00_3587)
	{
		{	/* SawMill/regset.scm 30 */
			return BUNSPEC;
		}

	}



/* &lambda1851 */
	obj_t BGl_z62lambda1851z62zzsaw_regsetz00(obj_t BgL_envz00_3588,
		obj_t BgL_oz00_3589, obj_t BgL_vz00_3590)
	{
		{	/* SawMill/regset.scm 30 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_4993;

				{
					obj_t BgL_auxz00_4994;

					{	/* SawMill/regset.scm 30 */
						BgL_objectz00_bglt BgL_tmpz00_4995;

						BgL_tmpz00_4995 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3589));
						BgL_auxz00_4994 = BGL_OBJECT_WIDENING(BgL_tmpz00_4995);
					}
					BgL_auxz00_4993 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_4994);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_4993))->
						BgL_interferez00) = ((obj_t) BgL_vz00_3590), BUNSPEC);
			}
		}

	}



/* &lambda1850 */
	obj_t BGl_z62lambda1850z62zzsaw_regsetz00(obj_t BgL_envz00_3591,
		obj_t BgL_oz00_3592)
	{
		{	/* SawMill/regset.scm 30 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_5001;

				{
					obj_t BgL_auxz00_5002;

					{	/* SawMill/regset.scm 30 */
						BgL_objectz00_bglt BgL_tmpz00_5003;

						BgL_tmpz00_5003 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3592));
						BgL_auxz00_5002 = BGL_OBJECT_WIDENING(BgL_tmpz00_5003);
					}
					BgL_auxz00_5001 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5002);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5001))->
					BgL_interferez00);
			}
		}

	}



/* &<@anonymous:1845> */
	obj_t BGl_z62zc3z04anonymousza31845ze3ze5zzsaw_regsetz00(obj_t
		BgL_envz00_3593)
	{
		{	/* SawMill/regset.scm 30 */
			return BINT(0L);
		}

	}



/* &lambda1844 */
	obj_t BGl_z62lambda1844z62zzsaw_regsetz00(obj_t BgL_envz00_3594,
		obj_t BgL_oz00_3595, obj_t BgL_vz00_3596)
	{
		{	/* SawMill/regset.scm 30 */
			{	/* SawMill/regset.scm 30 */
				int BgL_vz00_3749;

				BgL_vz00_3749 = CINT(BgL_vz00_3596);
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_5011;

					{
						obj_t BgL_auxz00_5012;

						{	/* SawMill/regset.scm 30 */
							BgL_objectz00_bglt BgL_tmpz00_5013;

							BgL_tmpz00_5013 =
								((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3595));
							BgL_auxz00_5012 = BGL_OBJECT_WIDENING(BgL_tmpz00_5013);
						}
						BgL_auxz00_5011 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5012);
					}
					return
						((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5011))->
							BgL_occurrencesz00) = ((int) BgL_vz00_3749), BUNSPEC);
		}}}

	}



/* &lambda1843 */
	obj_t BGl_z62lambda1843z62zzsaw_regsetz00(obj_t BgL_envz00_3597,
		obj_t BgL_oz00_3598)
	{
		{	/* SawMill/regset.scm 30 */
			{	/* SawMill/regset.scm 30 */
				int BgL_tmpz00_5019;

				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_5020;

					{
						obj_t BgL_auxz00_5021;

						{	/* SawMill/regset.scm 30 */
							BgL_objectz00_bglt BgL_tmpz00_5022;

							BgL_tmpz00_5022 =
								((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3598));
							BgL_auxz00_5021 = BGL_OBJECT_WIDENING(BgL_tmpz00_5022);
						}
						BgL_auxz00_5020 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5021);
					}
					BgL_tmpz00_5019 =
						(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5020))->
						BgL_occurrencesz00);
				}
				return BINT(BgL_tmpz00_5019);
			}
		}

	}



/* &<@anonymous:1837> */
	obj_t BGl_z62zc3z04anonymousza31837ze3ze5zzsaw_regsetz00(obj_t
		BgL_envz00_3599)
	{
		{	/* SawMill/regset.scm 30 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1836 */
	obj_t BGl_z62lambda1836z62zzsaw_regsetz00(obj_t BgL_envz00_3600,
		obj_t BgL_oz00_3601, obj_t BgL_vz00_3602)
	{
		{	/* SawMill/regset.scm 30 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_5030;

				{
					obj_t BgL_auxz00_5031;

					{	/* SawMill/regset.scm 30 */
						BgL_objectz00_bglt BgL_tmpz00_5032;

						BgL_tmpz00_5032 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3601));
						BgL_auxz00_5031 = BGL_OBJECT_WIDENING(BgL_tmpz00_5032);
					}
					BgL_auxz00_5030 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5031);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5030))->
						BgL_coalescez00) = ((obj_t) BgL_vz00_3602), BUNSPEC);
			}
		}

	}



/* &lambda1835 */
	obj_t BGl_z62lambda1835z62zzsaw_regsetz00(obj_t BgL_envz00_3603,
		obj_t BgL_oz00_3604)
	{
		{	/* SawMill/regset.scm 30 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_5038;

				{
					obj_t BgL_auxz00_5039;

					{	/* SawMill/regset.scm 30 */
						BgL_objectz00_bglt BgL_tmpz00_5040;

						BgL_tmpz00_5040 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3604));
						BgL_auxz00_5039 = BGL_OBJECT_WIDENING(BgL_tmpz00_5040);
					}
					BgL_auxz00_5038 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5039);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5038))->
					BgL_coalescez00);
			}
		}

	}



/* &<@anonymous:1826> */
	obj_t BGl_z62zc3z04anonymousza31826ze3ze5zzsaw_regsetz00(obj_t
		BgL_envz00_3605)
	{
		{	/* SawMill/regset.scm 30 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1825 */
	obj_t BGl_z62lambda1825z62zzsaw_regsetz00(obj_t BgL_envz00_3606,
		obj_t BgL_oz00_3607, obj_t BgL_vz00_3608)
	{
		{	/* SawMill/regset.scm 30 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_5047;

				{
					obj_t BgL_auxz00_5048;

					{	/* SawMill/regset.scm 30 */
						BgL_objectz00_bglt BgL_tmpz00_5049;

						BgL_tmpz00_5049 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3607));
						BgL_auxz00_5048 = BGL_OBJECT_WIDENING(BgL_tmpz00_5049);
					}
					BgL_auxz00_5047 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5048);
				}
				return
					((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5047))->
						BgL_colorz00) = ((obj_t) BgL_vz00_3608), BUNSPEC);
			}
		}

	}



/* &lambda1824 */
	obj_t BGl_z62lambda1824z62zzsaw_regsetz00(obj_t BgL_envz00_3609,
		obj_t BgL_oz00_3610)
	{
		{	/* SawMill/regset.scm 30 */
			{
				BgL_rtl_regzf2razf2_bglt BgL_auxz00_5055;

				{
					obj_t BgL_auxz00_5056;

					{	/* SawMill/regset.scm 30 */
						BgL_objectz00_bglt BgL_tmpz00_5057;

						BgL_tmpz00_5057 =
							((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3610));
						BgL_auxz00_5056 = BGL_OBJECT_WIDENING(BgL_tmpz00_5057);
					}
					BgL_auxz00_5055 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5056);
				}
				return
					(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5055))->BgL_colorz00);
			}
		}

	}



/* &lambda1808 */
	obj_t BGl_z62lambda1808z62zzsaw_regsetz00(obj_t BgL_envz00_3611,
		obj_t BgL_oz00_3612, obj_t BgL_vz00_3613)
	{
		{	/* SawMill/regset.scm 30 */
			{	/* SawMill/regset.scm 30 */
				int BgL_vz00_3756;

				BgL_vz00_3756 = CINT(BgL_vz00_3613);
				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_5064;

					{
						obj_t BgL_auxz00_5065;

						{	/* SawMill/regset.scm 30 */
							BgL_objectz00_bglt BgL_tmpz00_5066;

							BgL_tmpz00_5066 =
								((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3612));
							BgL_auxz00_5065 = BGL_OBJECT_WIDENING(BgL_tmpz00_5066);
						}
						BgL_auxz00_5064 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5065);
					}
					return
						((((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5064))->
							BgL_numz00) = ((int) BgL_vz00_3756), BUNSPEC);
		}}}

	}



/* &lambda1807 */
	obj_t BGl_z62lambda1807z62zzsaw_regsetz00(obj_t BgL_envz00_3614,
		obj_t BgL_oz00_3615)
	{
		{	/* SawMill/regset.scm 30 */
			{	/* SawMill/regset.scm 30 */
				int BgL_tmpz00_5072;

				{
					BgL_rtl_regzf2razf2_bglt BgL_auxz00_5073;

					{
						obj_t BgL_auxz00_5074;

						{	/* SawMill/regset.scm 30 */
							BgL_objectz00_bglt BgL_tmpz00_5075;

							BgL_tmpz00_5075 =
								((BgL_objectz00_bglt) ((BgL_rtl_regz00_bglt) BgL_oz00_3615));
							BgL_auxz00_5074 = BGL_OBJECT_WIDENING(BgL_tmpz00_5075);
						}
						BgL_auxz00_5073 = ((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5074);
					}
					BgL_tmpz00_5072 =
						(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5073))->BgL_numz00);
				}
				return BINT(BgL_tmpz00_5072);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_regsetz00(void)
	{
		{	/* SawMill/regset.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_regsetz00(void)
	{
		{	/* SawMill/regset.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00, BGl_rtl_regzf2razf2zzsaw_regsetz00,
				BGl_proc2071z00zzsaw_regsetz00, BGl_string2072z00zzsaw_regsetz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_dumpzd2envzd2zzsaw_defsz00, BGl_rtl_regzf2razf2zzsaw_regsetz00,
				BGl_proc2073z00zzsaw_regsetz00, BGl_string2074z00zzsaw_regsetz00);
		}

	}



/* &dump-rtl_reg/ra1426 */
	obj_t BGl_z62dumpzd2rtl_regzf2ra1426z42zzsaw_regsetz00(obj_t BgL_envz00_3620,
		obj_t BgL_oz00_3621, obj_t BgL_pz00_3622, obj_t BgL_mz00_3623)
	{
		{	/* SawMill/regset.scm 294 */
			{	/* SawMill/regset.scm 295 */
				obj_t BgL_arg1918z00_3759;

				BgL_arg1918z00_3759 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_rtl_regz00_bglt) BgL_oz00_3621)));
				return bgl_display_obj(BgL_arg1918z00_3759, BgL_pz00_3622);
			}
		}

	}



/* &shape-rtl_reg/ra1424 */
	obj_t BGl_z62shapezd2rtl_regzf2ra1424z42zzsaw_regsetz00(obj_t BgL_envz00_3624,
		obj_t BgL_oz00_3625)
	{
		{	/* SawMill/regset.scm 274 */
			{

				{	/* SawMill/regset.scm 276 */
					obj_t BgL_sz00_3763;

					{	/* SawMill/regset.scm 274 */
						obj_t BgL_nextzd2method1423zd2_3762;

						BgL_nextzd2method1423zd2_3762 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_rtl_regz00_bglt) BgL_oz00_3625)),
							BGl_shapezd2envzd2zztools_shapez00,
							BGl_rtl_regzf2razf2zzsaw_regsetz00);
						BgL_sz00_3763 =
							BGL_PROCEDURE_CALL1(BgL_nextzd2method1423zd2_3762,
							((obj_t) ((BgL_rtl_regz00_bglt) BgL_oz00_3625)));
					}
					{	/* SawMill/regset.scm 277 */
						obj_t BgL_pz00_3764;

						{	/* SawMill/regset.scm 277 */

							{	/* SawMill/regset.scm 277 */

								BgL_pz00_3764 =
									BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(BTRUE);
							}
						}
						{	/* SawMill/regset.scm 279 */
							bool_t BgL_test2222z00_5098;

							{	/* SawMill/regset.scm 279 */
								obj_t BgL_arg1917z00_3765;

								{
									BgL_rtl_regzf2razf2_bglt BgL_auxz00_5099;

									{
										obj_t BgL_auxz00_5100;

										{	/* SawMill/regset.scm 279 */
											BgL_objectz00_bglt BgL_tmpz00_5101;

											BgL_tmpz00_5101 =
												((BgL_objectz00_bglt)
												((BgL_rtl_regz00_bglt) BgL_oz00_3625));
											BgL_auxz00_5100 = BGL_OBJECT_WIDENING(BgL_tmpz00_5101);
										}
										BgL_auxz00_5099 =
											((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5100);
									}
									BgL_arg1917z00_3765 =
										(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5099))->
										BgL_colorz00);
								}
								BgL_test2222z00_5098 = INTEGERP(BgL_arg1917z00_3765);
							}
							if (BgL_test2222z00_5098)
								{	/* SawMill/regset.scm 279 */
									bgl_display_obj(BgL_sz00_3763, BgL_pz00_3764);
									bgl_display_string(BGl_string2075z00zzsaw_regsetz00,
										BgL_pz00_3764);
									{	/* SawMill/regset.scm 282 */
										obj_t BgL_arg1910z00_3766;

										{
											BgL_rtl_regzf2razf2_bglt BgL_auxz00_5110;

											{
												obj_t BgL_auxz00_5111;

												{	/* SawMill/regset.scm 282 */
													BgL_objectz00_bglt BgL_tmpz00_5112;

													BgL_tmpz00_5112 =
														((BgL_objectz00_bglt)
														((BgL_rtl_regz00_bglt) BgL_oz00_3625));
													BgL_auxz00_5111 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5112);
												}
												BgL_auxz00_5110 =
													((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5111);
											}
											BgL_arg1910z00_3766 =
												(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5110))->
												BgL_colorz00);
										}
										bgl_display_obj(BgL_arg1910z00_3766, BgL_pz00_3764);
									}
								}
							else
								{	/* SawMill/regset.scm 283 */
									bool_t BgL_test2223z00_5119;

									{	/* SawMill/regset.scm 283 */
										obj_t BgL_arg1916z00_3767;

										{
											BgL_rtl_regzf2razf2_bglt BgL_auxz00_5120;

											{
												obj_t BgL_auxz00_5121;

												{	/* SawMill/regset.scm 283 */
													BgL_objectz00_bglt BgL_tmpz00_5122;

													BgL_tmpz00_5122 =
														((BgL_objectz00_bglt)
														((BgL_rtl_regz00_bglt) BgL_oz00_3625));
													BgL_auxz00_5121 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5122);
												}
												BgL_auxz00_5120 =
													((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5121);
											}
											BgL_arg1916z00_3767 =
												(((BgL_rtl_regzf2razf2_bglt) COBJECT(BgL_auxz00_5120))->
												BgL_colorz00);
										}
										{	/* SawMill/regset.scm 283 */
											obj_t BgL_classz00_3768;

											BgL_classz00_3768 = BGl_rtl_regz00zzsaw_defsz00;
											if (BGL_OBJECTP(BgL_arg1916z00_3767))
												{	/* SawMill/regset.scm 283 */
													BgL_objectz00_bglt BgL_arg1807z00_3769;

													BgL_arg1807z00_3769 =
														(BgL_objectz00_bglt) (BgL_arg1916z00_3767);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* SawMill/regset.scm 283 */
															long BgL_idxz00_3770;

															BgL_idxz00_3770 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3769);
															BgL_test2223z00_5119 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_3770 + 1L)) == BgL_classz00_3768);
														}
													else
														{	/* SawMill/regset.scm 283 */
															bool_t BgL_res2024z00_3773;

															{	/* SawMill/regset.scm 283 */
																obj_t BgL_oclassz00_3774;

																{	/* SawMill/regset.scm 283 */
																	obj_t BgL_arg1815z00_3775;
																	long BgL_arg1816z00_3776;

																	BgL_arg1815z00_3775 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* SawMill/regset.scm 283 */
																		long BgL_arg1817z00_3777;

																		BgL_arg1817z00_3777 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3769);
																		BgL_arg1816z00_3776 =
																			(BgL_arg1817z00_3777 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_3774 =
																		VECTOR_REF(BgL_arg1815z00_3775,
																		BgL_arg1816z00_3776);
																}
																{	/* SawMill/regset.scm 283 */
																	bool_t BgL__ortest_1115z00_3778;

																	BgL__ortest_1115z00_3778 =
																		(BgL_classz00_3768 == BgL_oclassz00_3774);
																	if (BgL__ortest_1115z00_3778)
																		{	/* SawMill/regset.scm 283 */
																			BgL_res2024z00_3773 =
																				BgL__ortest_1115z00_3778;
																		}
																	else
																		{	/* SawMill/regset.scm 283 */
																			long BgL_odepthz00_3779;

																			{	/* SawMill/regset.scm 283 */
																				obj_t BgL_arg1804z00_3780;

																				BgL_arg1804z00_3780 =
																					(BgL_oclassz00_3774);
																				BgL_odepthz00_3779 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_3780);
																			}
																			if ((1L < BgL_odepthz00_3779))
																				{	/* SawMill/regset.scm 283 */
																					obj_t BgL_arg1802z00_3781;

																					{	/* SawMill/regset.scm 283 */
																						obj_t BgL_arg1803z00_3782;

																						BgL_arg1803z00_3782 =
																							(BgL_oclassz00_3774);
																						BgL_arg1802z00_3781 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_3782, 1L);
																					}
																					BgL_res2024z00_3773 =
																						(BgL_arg1802z00_3781 ==
																						BgL_classz00_3768);
																				}
																			else
																				{	/* SawMill/regset.scm 283 */
																					BgL_res2024z00_3773 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2223z00_5119 = BgL_res2024z00_3773;
														}
												}
											else
												{	/* SawMill/regset.scm 283 */
													BgL_test2223z00_5119 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test2223z00_5119)
										{	/* SawMill/regset.scm 283 */
											bgl_display_obj(BgL_sz00_3763, BgL_pz00_3764);
											bgl_display_string(BGl_string2076z00zzsaw_regsetz00,
												BgL_pz00_3764);
											{	/* SawMill/regset.scm 286 */
												obj_t BgL_arg1913z00_3783;

												{	/* SawMill/regset.scm 286 */
													obj_t BgL_arg1914z00_3784;

													{
														BgL_rtl_regzf2razf2_bglt BgL_auxz00_5152;

														{
															obj_t BgL_auxz00_5153;

															{	/* SawMill/regset.scm 286 */
																BgL_objectz00_bglt BgL_tmpz00_5154;

																BgL_tmpz00_5154 =
																	((BgL_objectz00_bglt)
																	((BgL_rtl_regz00_bglt) BgL_oz00_3625));
																BgL_auxz00_5153 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5154);
															}
															BgL_auxz00_5152 =
																((BgL_rtl_regzf2razf2_bglt) BgL_auxz00_5153);
														}
														BgL_arg1914z00_3784 =
															(((BgL_rtl_regzf2razf2_bglt)
																COBJECT(BgL_auxz00_5152))->BgL_colorz00);
													}
													BgL_arg1913z00_3783 =
														BGl_shapez00zztools_shapez00(BgL_arg1914z00_3784);
												}
												bgl_display_obj(BgL_arg1913z00_3783, BgL_pz00_3764);
											}
										}
									else
										{	/* SawMill/regset.scm 283 */
											bgl_display_obj(BgL_sz00_3763, BgL_pz00_3764);
										}
								}
						}
						return bgl_close_output_port(BgL_pz00_3764);
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_regsetz00(void)
	{
		{	/* SawMill/regset.scm 15 */
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2077z00zzsaw_regsetz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2077z00zzsaw_regsetz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2077z00zzsaw_regsetz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2077z00zzsaw_regsetz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2077z00zzsaw_regsetz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2077z00zzsaw_regsetz00));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string2077z00zzsaw_regsetz00));
			BGl_modulezd2initializa7ationz75zzsaw_node2rtlz00(20930040L,
				BSTRING_TO_STRING(BGl_string2077z00zzsaw_regsetz00));
			BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string2077z00zzsaw_regsetz00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_registerzd2allocationzd2
				(250697396L, BSTRING_TO_STRING(BGl_string2077z00zzsaw_regsetz00));
		}

	}

#ifdef __cplusplus
}
#endif
