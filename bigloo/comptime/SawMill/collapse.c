/*===========================================================================*/
/*   (SawMill/collapse.scm)                                                  */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/collapse.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_COLLAPSE_TYPE_DEFINITIONS
#define BGL_SAW_COLLAPSE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;

	typedef struct BgL_collapsedz00_bgl
	{
		obj_t BgL_lastz00;
	}                   *BgL_collapsedz00_bglt;


#endif													// BGL_SAW_COLLAPSE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzsaw_collapsez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_genericzd2initzd2zzsaw_collapsez00(void);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzsaw_collapsez00(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzsaw_collapsez00(void);
	static obj_t BGl_z62collapsez62zzsaw_collapsez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzsaw_collapsez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_libz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_substz00zzsaw_libz00(obj_t, obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_collapsez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_collapsez00(void);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static bool_t BGl_dfsze70ze7zzsaw_collapsez00(BgL_blockz00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_collapsez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_collapsez00(void);
	static obj_t BGl_collapsedz00zzsaw_collapsez00 = BUNSPEC;
	static obj_t BGl_z62lambda1607z62zzsaw_collapsez00(obj_t, obj_t);
	static obj_t BGl_z62lambda1608z62zzsaw_collapsez00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31590ze3ze5zzsaw_collapsez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_collapsez00zzsaw_collapsez00(BgL_blockz00_bglt);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1576z62zzsaw_collapsez00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	static BgL_blockz00_bglt BGl_z62lambda1586z62zzsaw_collapsez00(obj_t, obj_t);
	extern obj_t BGl_blockz00zzsaw_defsz00;
	static BgL_blockz00_bglt BGl_z62lambda1591z62zzsaw_collapsez00(obj_t, obj_t);
	static obj_t __cnst[4];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_collapsezd2envzd2zzsaw_collapsez00,
		BgL_bgl_za762collapseza762za7za71867z00,
		BGl_z62collapsez62zzsaw_collapsez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1863z00zzsaw_collapsez00,
		BgL_bgl_string1863za700za7za7s1868za7, "saw_collapse", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1857z00zzsaw_collapsez00,
		BgL_bgl_za762lambda1608za7621869z00, BGl_z62lambda1608z62zzsaw_collapsez00,
		0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1864z00zzsaw_collapsez00,
		BgL_bgl_string1864za700za7za7s1870za7, "saw_collapse collapsed obj last ",
		32);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1858z00zzsaw_collapsez00,
		BgL_bgl_za762lambda1607za7621871z00, BGl_z62lambda1607z62zzsaw_collapsez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1859z00zzsaw_collapsez00,
		BgL_bgl_za762lambda1591za7621872z00, BGl_z62lambda1591z62zzsaw_collapsez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1860z00zzsaw_collapsez00,
		BgL_bgl_za762za7c3za704anonymo1873za7,
		BGl_z62zc3z04anonymousza31590ze3ze5zzsaw_collapsez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1861z00zzsaw_collapsez00,
		BgL_bgl_za762lambda1586za7621874z00, BGl_z62lambda1586z62zzsaw_collapsez00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1862z00zzsaw_collapsez00,
		BgL_bgl_za762lambda1576za7621875z00, BGl_z62lambda1576z62zzsaw_collapsez00,
		0L, BUNSPEC, 5);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_collapsez00));
		     ADD_ROOT((void *) (&BGl_collapsedz00zzsaw_collapsez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_collapsez00(long
		BgL_checksumz00_2366, char *BgL_fromz00_2367)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_collapsez00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_collapsez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_collapsez00();
					BGl_libraryzd2moduleszd2initz00zzsaw_collapsez00();
					BGl_cnstzd2initzd2zzsaw_collapsez00();
					BGl_importedzd2moduleszd2initz00zzsaw_collapsez00();
					BGl_objectzd2initzd2zzsaw_collapsez00();
					return BGl_methodzd2initzd2zzsaw_collapsez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_collapsez00(void)
	{
		{	/* SawMill/collapse.scm 1 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_collapse");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_collapse");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"saw_collapse");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_collapse");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "saw_collapse");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"saw_collapse");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_collapse");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_collapse");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_collapsez00(void)
	{
		{	/* SawMill/collapse.scm 1 */
			{	/* SawMill/collapse.scm 1 */
				obj_t BgL_cportz00_2332;

				{	/* SawMill/collapse.scm 1 */
					obj_t BgL_stringz00_2339;

					BgL_stringz00_2339 = BGl_string1864z00zzsaw_collapsez00;
					{	/* SawMill/collapse.scm 1 */
						obj_t BgL_startz00_2340;

						BgL_startz00_2340 = BINT(0L);
						{	/* SawMill/collapse.scm 1 */
							obj_t BgL_endz00_2341;

							BgL_endz00_2341 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2339)));
							{	/* SawMill/collapse.scm 1 */

								BgL_cportz00_2332 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2339, BgL_startz00_2340, BgL_endz00_2341);
				}}}}
				{
					long BgL_iz00_2333;

					BgL_iz00_2333 = 3L;
				BgL_loopz00_2334:
					if ((BgL_iz00_2333 == -1L))
						{	/* SawMill/collapse.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawMill/collapse.scm 1 */
							{	/* SawMill/collapse.scm 1 */
								obj_t BgL_arg1866z00_2335;

								{	/* SawMill/collapse.scm 1 */

									{	/* SawMill/collapse.scm 1 */
										obj_t BgL_locationz00_2337;

										BgL_locationz00_2337 = BBOOL(((bool_t) 0));
										{	/* SawMill/collapse.scm 1 */

											BgL_arg1866z00_2335 =
												BGl_readz00zz__readerz00(BgL_cportz00_2332,
												BgL_locationz00_2337);
										}
									}
								}
								{	/* SawMill/collapse.scm 1 */
									int BgL_tmpz00_2394;

									BgL_tmpz00_2394 = (int) (BgL_iz00_2333);
									CNST_TABLE_SET(BgL_tmpz00_2394, BgL_arg1866z00_2335);
							}}
							{	/* SawMill/collapse.scm 1 */
								int BgL_auxz00_2338;

								BgL_auxz00_2338 = (int) ((BgL_iz00_2333 - 1L));
								{
									long BgL_iz00_2399;

									BgL_iz00_2399 = (long) (BgL_auxz00_2338);
									BgL_iz00_2333 = BgL_iz00_2399;
									goto BgL_loopz00_2334;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_collapsez00(void)
	{
		{	/* SawMill/collapse.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* collapse */
	BGL_EXPORTED_DEF obj_t BGl_collapsez00zzsaw_collapsez00(BgL_blockz00_bglt
		BgL_bz00_24)
	{
		{	/* SawMill/collapse.scm 13 */
			BGl_dfsze70ze7zzsaw_collapsez00(BgL_bz00_24);
			return BUNSPEC;
		}

	}



/* dfs~0 */
	bool_t BGl_dfsze70ze7zzsaw_collapsez00(BgL_blockz00_bglt BgL_bz00_1831)
	{
		{	/* SawMill/collapse.scm 14 */
			{	/* SawMill/collapse.scm 16 */
				BgL_collapsedz00_bglt BgL_wide1176z00_1836;

				BgL_wide1176z00_1836 =
					((BgL_collapsedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_collapsedz00_bgl))));
				{	/* SawMill/collapse.scm 16 */
					obj_t BgL_auxz00_2407;
					BgL_objectz00_bglt BgL_tmpz00_2404;

					BgL_auxz00_2407 = ((obj_t) BgL_wide1176z00_1836);
					BgL_tmpz00_2404 =
						((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1831));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2404, BgL_auxz00_2407);
				}
				((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1831));
				{	/* SawMill/collapse.scm 16 */
					long BgL_arg1434z00_1837;

					BgL_arg1434z00_1837 =
						BGL_CLASS_NUM(BGl_collapsedz00zzsaw_collapsez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt) BgL_bz00_1831)), BgL_arg1434z00_1837);
				}
				((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1831));
			}
			{
				BgL_collapsedz00_bglt BgL_auxz00_2418;

				{
					obj_t BgL_auxz00_2419;

					{	/* SawMill/collapse.scm 16 */
						BgL_objectz00_bglt BgL_tmpz00_2420;

						BgL_tmpz00_2420 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_bz00_1831));
						BgL_auxz00_2419 = BGL_OBJECT_WIDENING(BgL_tmpz00_2420);
					}
					BgL_auxz00_2418 = ((BgL_collapsedz00_bglt) BgL_auxz00_2419);
				}
				((((BgL_collapsedz00_bglt) COBJECT(BgL_auxz00_2418))->BgL_lastz00) =
					((obj_t)
						BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(((
									(BgL_blockz00_bglt) COBJECT(BgL_bz00_1831))->BgL_firstz00))),
					BUNSPEC);
			}
			((BgL_blockz00_bglt) BgL_bz00_1831);
			{	/* SawMill/collapse.scm 17 */
				bool_t BgL_test1878z00_2429;

				if (NULLP((((BgL_blockz00_bglt) COBJECT(BgL_bz00_1831))->BgL_predsz00)))
					{	/* SawMill/collapse.scm 17 */
						BgL_test1878z00_2429 = ((bool_t) 0);
					}
				else
					{	/* SawMill/collapse.scm 17 */
						BgL_test1878z00_2429 =
							NULLP(CDR(
								(((BgL_blockz00_bglt) COBJECT(BgL_bz00_1831))->BgL_predsz00)));
					}
				if (BgL_test1878z00_2429)
					{	/* SawMill/collapse.scm 18 */
						obj_t BgL_pz00_1846;

						BgL_pz00_1846 =
							CAR((((BgL_blockz00_bglt) COBJECT(BgL_bz00_1831))->BgL_predsz00));
						{	/* SawMill/collapse.scm 18 */
							obj_t BgL_psz00_1847;

							BgL_psz00_1847 =
								(((BgL_blockz00_bglt) COBJECT(
										((BgL_blockz00_bglt) BgL_pz00_1846)))->BgL_succsz00);
							{	/* SawMill/collapse.scm 18 */

								if (NULLP(CDR(((obj_t) BgL_psz00_1847))))
									{	/* SawMill/collapse.scm 19 */
										{	/* SawMill/collapse.scm 21 */
											obj_t BgL_arg1489z00_1850;
											obj_t BgL_arg1502z00_1851;

											{
												BgL_collapsedz00_bglt BgL_auxz00_2444;

												{
													obj_t BgL_auxz00_2445;

													{	/* SawMill/collapse.scm 21 */
														BgL_objectz00_bglt BgL_tmpz00_2446;

														BgL_tmpz00_2446 =
															((BgL_objectz00_bglt)
															((BgL_blockz00_bglt) BgL_pz00_1846));
														BgL_auxz00_2445 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_2446);
													}
													BgL_auxz00_2444 =
														((BgL_collapsedz00_bglt) BgL_auxz00_2445);
												}
												BgL_arg1489z00_1850 =
													(((BgL_collapsedz00_bglt) COBJECT(BgL_auxz00_2444))->
													BgL_lastz00);
											}
											BgL_arg1502z00_1851 =
												(((BgL_blockz00_bglt) COBJECT(BgL_bz00_1831))->
												BgL_firstz00);
											{	/* SawMill/collapse.scm 21 */
												obj_t BgL_tmpz00_2453;

												BgL_tmpz00_2453 = ((obj_t) BgL_arg1489z00_1850);
												SET_CDR(BgL_tmpz00_2453, BgL_arg1502z00_1851);
											}
										}
										{	/* SawMill/collapse.scm 22 */
											obj_t BgL_arg1509z00_1852;

											{
												BgL_collapsedz00_bglt BgL_auxz00_2456;

												{
													obj_t BgL_auxz00_2457;

													{	/* SawMill/collapse.scm 22 */
														BgL_objectz00_bglt BgL_tmpz00_2458;

														BgL_tmpz00_2458 =
															((BgL_objectz00_bglt)
															((BgL_blockz00_bglt) BgL_bz00_1831));
														BgL_auxz00_2457 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_2458);
													}
													BgL_auxz00_2456 =
														((BgL_collapsedz00_bglt) BgL_auxz00_2457);
												}
												BgL_arg1509z00_1852 =
													(((BgL_collapsedz00_bglt) COBJECT(BgL_auxz00_2456))->
													BgL_lastz00);
											}
											{
												BgL_collapsedz00_bglt BgL_auxz00_2464;

												{
													obj_t BgL_auxz00_2465;

													{	/* SawMill/collapse.scm 22 */
														BgL_objectz00_bglt BgL_tmpz00_2466;

														BgL_tmpz00_2466 =
															((BgL_objectz00_bglt)
															((BgL_blockz00_bglt) BgL_pz00_1846));
														BgL_auxz00_2465 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_2466);
													}
													BgL_auxz00_2464 =
														((BgL_collapsedz00_bglt) BgL_auxz00_2465);
												}
												((((BgL_collapsedz00_bglt) COBJECT(BgL_auxz00_2464))->
														BgL_lastz00) =
													((obj_t) BgL_arg1509z00_1852), BUNSPEC);
											}
										}
										{	/* SawMill/collapse.scm 23 */
											obj_t BgL_arg1513z00_1853;

											BgL_arg1513z00_1853 =
												(((BgL_blockz00_bglt) COBJECT(BgL_bz00_1831))->
												BgL_succsz00);
											((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
																BgL_pz00_1846)))->BgL_succsz00) =
												((obj_t) BgL_arg1513z00_1853), BUNSPEC);
										}
										{	/* SawMill/collapse.scm 24 */
											obj_t BgL_g1397z00_1854;

											BgL_g1397z00_1854 =
												(((BgL_blockz00_bglt) COBJECT(BgL_bz00_1831))->
												BgL_succsz00);
											{
												obj_t BgL_l1395z00_1856;

												BgL_l1395z00_1856 = BgL_g1397z00_1854;
											BgL_zc3z04anonymousza31514ze3z87_1857:
												if (PAIRP(BgL_l1395z00_1856))
													{	/* SawMill/collapse.scm 24 */
														{	/* SawMill/collapse.scm 25 */
															obj_t BgL_sz00_1859;

															BgL_sz00_1859 = CAR(BgL_l1395z00_1856);
															{
																obj_t BgL_auxz00_2479;

																{	/* SawMill/collapse.scm 26 */
																	obj_t BgL_arg1516z00_1861;

																	BgL_arg1516z00_1861 =
																		(((BgL_blockz00_bglt) COBJECT(
																				((BgL_blockz00_bglt) BgL_sz00_1859)))->
																		BgL_predsz00);
																	BgL_auxz00_2479 =
																		BGl_substz00zzsaw_libz00
																		(BgL_arg1516z00_1861,
																		((obj_t) BgL_bz00_1831), BgL_pz00_1846);
																}
																((((BgL_blockz00_bglt) COBJECT(
																				((BgL_blockz00_bglt) BgL_sz00_1859)))->
																		BgL_predsz00) =
																	((obj_t) BgL_auxz00_2479), BUNSPEC);
															}
														}
														{
															obj_t BgL_l1395z00_2486;

															BgL_l1395z00_2486 = CDR(BgL_l1395z00_1856);
															BgL_l1395z00_1856 = BgL_l1395z00_2486;
															goto BgL_zc3z04anonymousza31514ze3z87_1857;
														}
													}
												else
													{	/* SawMill/collapse.scm 24 */
														((bool_t) 1);
													}
											}
										}
									}
								else
									{	/* SawMill/collapse.scm 19 */
										((bool_t) 0);
									}
							}
						}
					}
				else
					{	/* SawMill/collapse.scm 17 */
						((bool_t) 0);
					}
			}
			{	/* SawMill/collapse.scm 28 */
				obj_t BgL_g1400z00_1870;

				BgL_g1400z00_1870 =
					(((BgL_blockz00_bglt) COBJECT(BgL_bz00_1831))->BgL_succsz00);
				{
					obj_t BgL_l1398z00_1872;

					BgL_l1398z00_1872 = BgL_g1400z00_1870;
				BgL_zc3z04anonymousza31554ze3z87_1873:
					if (PAIRP(BgL_l1398z00_1872))
						{	/* SawMill/collapse.scm 28 */
							{	/* SawMill/collapse.scm 29 */
								BgL_blockz00_bglt BgL_succz00_1875;

								BgL_succz00_1875 = ((BgL_blockz00_bglt) CAR(BgL_l1398z00_1872));
								{	/* SawMill/collapse.scm 29 */
									bool_t BgL_test1883z00_2493;

									{	/* SawMill/collapse.scm 29 */
										obj_t BgL_classz00_2264;

										BgL_classz00_2264 = BGl_collapsedz00zzsaw_collapsez00;
										{	/* SawMill/collapse.scm 29 */
											obj_t BgL_oclassz00_2266;

											{	/* SawMill/collapse.scm 29 */
												obj_t BgL_arg1815z00_2268;
												long BgL_arg1816z00_2269;

												BgL_arg1815z00_2268 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawMill/collapse.scm 29 */
													long BgL_arg1817z00_2270;

													BgL_arg1817z00_2270 =
														BGL_OBJECT_CLASS_NUM(
														((BgL_objectz00_bglt) BgL_succz00_1875));
													BgL_arg1816z00_2269 =
														(BgL_arg1817z00_2270 - OBJECT_TYPE);
												}
												BgL_oclassz00_2266 =
													VECTOR_REF(BgL_arg1815z00_2268, BgL_arg1816z00_2269);
											}
											BgL_test1883z00_2493 =
												(BgL_oclassz00_2266 == BgL_classz00_2264);
									}}
									if (BgL_test1883z00_2493)
										{	/* SawMill/collapse.scm 29 */
											((bool_t) 0);
										}
									else
										{	/* SawMill/collapse.scm 29 */
											BGl_dfsze70ze7zzsaw_collapsez00(BgL_succz00_1875);
										}
								}
							}
							{
								obj_t BgL_l1398z00_2501;

								BgL_l1398z00_2501 = CDR(BgL_l1398z00_1872);
								BgL_l1398z00_1872 = BgL_l1398z00_2501;
								goto BgL_zc3z04anonymousza31554ze3z87_1873;
							}
						}
					else
						{	/* SawMill/collapse.scm 28 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* &collapse */
	obj_t BGl_z62collapsez62zzsaw_collapsez00(obj_t BgL_envz00_2307,
		obj_t BgL_bz00_2308)
	{
		{	/* SawMill/collapse.scm 13 */
			return
				BGl_collapsez00zzsaw_collapsez00(((BgL_blockz00_bglt) BgL_bz00_2308));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_collapsez00(void)
	{
		{	/* SawMill/collapse.scm 1 */
			{	/* SawMill/collapse.scm 7 */
				obj_t BgL_arg1573z00_1884;
				obj_t BgL_arg1575z00_1885;

				{	/* SawMill/collapse.scm 7 */
					obj_t BgL_v1401z00_1911;

					BgL_v1401z00_1911 = create_vector(1L);
					{	/* SawMill/collapse.scm 7 */
						obj_t BgL_arg1602z00_1912;

						BgL_arg1602z00_1912 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(0),
							BGl_proc1858z00zzsaw_collapsez00,
							BGl_proc1857z00zzsaw_collapsez00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1401z00_1911, 0L, BgL_arg1602z00_1912);
					}
					BgL_arg1573z00_1884 = BgL_v1401z00_1911;
				}
				{	/* SawMill/collapse.scm 7 */
					obj_t BgL_v1402z00_1922;

					BgL_v1402z00_1922 = create_vector(0L);
					BgL_arg1575z00_1885 = BgL_v1402z00_1922;
				}
				return (BGl_collapsedz00zzsaw_collapsez00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(2),
						CNST_TABLE_REF(3), BGl_blockz00zzsaw_defsz00, 33907L,
						BGl_proc1862z00zzsaw_collapsez00, BGl_proc1861z00zzsaw_collapsez00,
						BFALSE, BGl_proc1860z00zzsaw_collapsez00,
						BGl_proc1859z00zzsaw_collapsez00, BgL_arg1573z00_1884,
						BgL_arg1575z00_1885), BUNSPEC);
			}
		}

	}



/* &lambda1591 */
	BgL_blockz00_bglt BGl_z62lambda1591z62zzsaw_collapsez00(obj_t BgL_envz00_2315,
		obj_t BgL_o1158z00_2316)
	{
		{	/* SawMill/collapse.scm 7 */
			{	/* SawMill/collapse.scm 7 */
				long BgL_arg1593z00_2344;

				{	/* SawMill/collapse.scm 7 */
					obj_t BgL_arg1594z00_2345;

					{	/* SawMill/collapse.scm 7 */
						obj_t BgL_arg1595z00_2346;

						{	/* SawMill/collapse.scm 7 */
							obj_t BgL_arg1815z00_2347;
							long BgL_arg1816z00_2348;

							BgL_arg1815z00_2347 = (BGl_za2classesza2z00zz__objectz00);
							{	/* SawMill/collapse.scm 7 */
								long BgL_arg1817z00_2349;

								BgL_arg1817z00_2349 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_blockz00_bglt) BgL_o1158z00_2316)));
								BgL_arg1816z00_2348 = (BgL_arg1817z00_2349 - OBJECT_TYPE);
							}
							BgL_arg1595z00_2346 =
								VECTOR_REF(BgL_arg1815z00_2347, BgL_arg1816z00_2348);
						}
						BgL_arg1594z00_2345 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1595z00_2346);
					}
					{	/* SawMill/collapse.scm 7 */
						obj_t BgL_tmpz00_2521;

						BgL_tmpz00_2521 = ((obj_t) BgL_arg1594z00_2345);
						BgL_arg1593z00_2344 = BGL_CLASS_NUM(BgL_tmpz00_2521);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) BgL_o1158z00_2316)), BgL_arg1593z00_2344);
			}
			{	/* SawMill/collapse.scm 7 */
				BgL_objectz00_bglt BgL_tmpz00_2527;

				BgL_tmpz00_2527 =
					((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1158z00_2316));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2527, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_o1158z00_2316));
			return ((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1158z00_2316));
		}

	}



/* &<@anonymous:1590> */
	obj_t BGl_z62zc3z04anonymousza31590ze3ze5zzsaw_collapsez00(obj_t
		BgL_envz00_2317, obj_t BgL_new1157z00_2318)
	{
		{	/* SawMill/collapse.scm 7 */
			{
				BgL_blockz00_bglt BgL_auxz00_2535;

				((((BgL_blockz00_bglt) COBJECT(
								((BgL_blockz00_bglt)
									((BgL_blockz00_bglt) BgL_new1157z00_2318))))->BgL_labelz00) =
					((int) (int) (0L)), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1157z00_2318))))->BgL_predsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1157z00_2318))))->BgL_succsz00) =
					((obj_t) BNIL), BUNSPEC);
				((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt) ((BgL_blockz00_bglt)
										BgL_new1157z00_2318))))->BgL_firstz00) =
					((obj_t) BNIL), BUNSPEC);
				{
					BgL_collapsedz00_bglt BgL_auxz00_2549;

					{
						obj_t BgL_auxz00_2550;

						{	/* SawMill/collapse.scm 7 */
							BgL_objectz00_bglt BgL_tmpz00_2551;

							BgL_tmpz00_2551 =
								((BgL_objectz00_bglt)
								((BgL_blockz00_bglt) BgL_new1157z00_2318));
							BgL_auxz00_2550 = BGL_OBJECT_WIDENING(BgL_tmpz00_2551);
						}
						BgL_auxz00_2549 = ((BgL_collapsedz00_bglt) BgL_auxz00_2550);
					}
					((((BgL_collapsedz00_bglt) COBJECT(BgL_auxz00_2549))->BgL_lastz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_2535 = ((BgL_blockz00_bglt) BgL_new1157z00_2318);
				return ((obj_t) BgL_auxz00_2535);
			}
		}

	}



/* &lambda1586 */
	BgL_blockz00_bglt BGl_z62lambda1586z62zzsaw_collapsez00(obj_t BgL_envz00_2319,
		obj_t BgL_o1154z00_2320)
	{
		{	/* SawMill/collapse.scm 7 */
			{	/* SawMill/collapse.scm 7 */
				BgL_collapsedz00_bglt BgL_wide1156z00_2352;

				BgL_wide1156z00_2352 =
					((BgL_collapsedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_collapsedz00_bgl))));
				{	/* SawMill/collapse.scm 7 */
					obj_t BgL_auxz00_2564;
					BgL_objectz00_bglt BgL_tmpz00_2560;

					BgL_auxz00_2564 = ((obj_t) BgL_wide1156z00_2352);
					BgL_tmpz00_2560 =
						((BgL_objectz00_bglt)
						((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1154z00_2320)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2560, BgL_auxz00_2564);
				}
				((BgL_objectz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1154z00_2320)));
				{	/* SawMill/collapse.scm 7 */
					long BgL_arg1589z00_2353;

					BgL_arg1589z00_2353 =
						BGL_CLASS_NUM(BGl_collapsedz00zzsaw_collapsez00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_blockz00_bglt)
								((BgL_blockz00_bglt) BgL_o1154z00_2320))), BgL_arg1589z00_2353);
				}
				return
					((BgL_blockz00_bglt)
					((BgL_blockz00_bglt) ((BgL_blockz00_bglt) BgL_o1154z00_2320)));
			}
		}

	}



/* &lambda1576 */
	BgL_blockz00_bglt BGl_z62lambda1576z62zzsaw_collapsez00(obj_t BgL_envz00_2321,
		obj_t BgL_label1149z00_2322, obj_t BgL_preds1150z00_2323,
		obj_t BgL_succs1151z00_2324, obj_t BgL_first1152z00_2325,
		obj_t BgL_last1153z00_2326)
	{
		{	/* SawMill/collapse.scm 7 */
			{	/* SawMill/collapse.scm 7 */
				int BgL_label1149z00_2354;

				BgL_label1149z00_2354 = CINT(BgL_label1149z00_2322);
				{	/* SawMill/collapse.scm 7 */
					BgL_blockz00_bglt BgL_new1182z00_2358;

					{	/* SawMill/collapse.scm 7 */
						BgL_blockz00_bglt BgL_tmp1180z00_2359;
						BgL_collapsedz00_bglt BgL_wide1181z00_2360;

						{
							BgL_blockz00_bglt BgL_auxz00_2579;

							{	/* SawMill/collapse.scm 7 */
								BgL_blockz00_bglt BgL_new1179z00_2361;

								BgL_new1179z00_2361 =
									((BgL_blockz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_blockz00_bgl))));
								{	/* SawMill/collapse.scm 7 */
									long BgL_arg1585z00_2362;

									BgL_arg1585z00_2362 =
										BGL_CLASS_NUM(BGl_blockz00zzsaw_defsz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1179z00_2361),
										BgL_arg1585z00_2362);
								}
								{	/* SawMill/collapse.scm 7 */
									BgL_objectz00_bglt BgL_tmpz00_2584;

									BgL_tmpz00_2584 = ((BgL_objectz00_bglt) BgL_new1179z00_2361);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2584, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1179z00_2361);
								BgL_auxz00_2579 = BgL_new1179z00_2361;
							}
							BgL_tmp1180z00_2359 = ((BgL_blockz00_bglt) BgL_auxz00_2579);
						}
						BgL_wide1181z00_2360 =
							((BgL_collapsedz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_collapsedz00_bgl))));
						{	/* SawMill/collapse.scm 7 */
							obj_t BgL_auxz00_2592;
							BgL_objectz00_bglt BgL_tmpz00_2590;

							BgL_auxz00_2592 = ((obj_t) BgL_wide1181z00_2360);
							BgL_tmpz00_2590 = ((BgL_objectz00_bglt) BgL_tmp1180z00_2359);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2590, BgL_auxz00_2592);
						}
						((BgL_objectz00_bglt) BgL_tmp1180z00_2359);
						{	/* SawMill/collapse.scm 7 */
							long BgL_arg1584z00_2363;

							BgL_arg1584z00_2363 =
								BGL_CLASS_NUM(BGl_collapsedz00zzsaw_collapsez00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1180z00_2359),
								BgL_arg1584z00_2363);
						}
						BgL_new1182z00_2358 = ((BgL_blockz00_bglt) BgL_tmp1180z00_2359);
					}
					((((BgL_blockz00_bglt) COBJECT(
									((BgL_blockz00_bglt) BgL_new1182z00_2358)))->BgL_labelz00) =
						((int) BgL_label1149z00_2354), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1182z00_2358)))->BgL_predsz00) =
						((obj_t) ((obj_t) BgL_preds1150z00_2323)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1182z00_2358)))->BgL_succsz00) =
						((obj_t) ((obj_t) BgL_succs1151z00_2324)), BUNSPEC);
					((((BgL_blockz00_bglt) COBJECT(((BgL_blockz00_bglt)
										BgL_new1182z00_2358)))->BgL_firstz00) =
						((obj_t) ((obj_t) BgL_first1152z00_2325)), BUNSPEC);
					{
						BgL_collapsedz00_bglt BgL_auxz00_2611;

						{
							obj_t BgL_auxz00_2612;

							{	/* SawMill/collapse.scm 7 */
								BgL_objectz00_bglt BgL_tmpz00_2613;

								BgL_tmpz00_2613 = ((BgL_objectz00_bglt) BgL_new1182z00_2358);
								BgL_auxz00_2612 = BGL_OBJECT_WIDENING(BgL_tmpz00_2613);
							}
							BgL_auxz00_2611 = ((BgL_collapsedz00_bglt) BgL_auxz00_2612);
						}
						((((BgL_collapsedz00_bglt) COBJECT(BgL_auxz00_2611))->BgL_lastz00) =
							((obj_t) BgL_last1153z00_2326), BUNSPEC);
					}
					return BgL_new1182z00_2358;
				}
			}
		}

	}



/* &lambda1608 */
	obj_t BGl_z62lambda1608z62zzsaw_collapsez00(obj_t BgL_envz00_2327,
		obj_t BgL_oz00_2328, obj_t BgL_vz00_2329)
	{
		{	/* SawMill/collapse.scm 7 */
			{
				BgL_collapsedz00_bglt BgL_auxz00_2618;

				{
					obj_t BgL_auxz00_2619;

					{	/* SawMill/collapse.scm 7 */
						BgL_objectz00_bglt BgL_tmpz00_2620;

						BgL_tmpz00_2620 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_2328));
						BgL_auxz00_2619 = BGL_OBJECT_WIDENING(BgL_tmpz00_2620);
					}
					BgL_auxz00_2618 = ((BgL_collapsedz00_bglt) BgL_auxz00_2619);
				}
				return
					((((BgL_collapsedz00_bglt) COBJECT(BgL_auxz00_2618))->BgL_lastz00) =
					((obj_t) BgL_vz00_2329), BUNSPEC);
			}
		}

	}



/* &lambda1607 */
	obj_t BGl_z62lambda1607z62zzsaw_collapsez00(obj_t BgL_envz00_2330,
		obj_t BgL_oz00_2331)
	{
		{	/* SawMill/collapse.scm 7 */
			{
				BgL_collapsedz00_bglt BgL_auxz00_2626;

				{
					obj_t BgL_auxz00_2627;

					{	/* SawMill/collapse.scm 7 */
						BgL_objectz00_bglt BgL_tmpz00_2628;

						BgL_tmpz00_2628 =
							((BgL_objectz00_bglt) ((BgL_blockz00_bglt) BgL_oz00_2331));
						BgL_auxz00_2627 = BGL_OBJECT_WIDENING(BgL_tmpz00_2628);
					}
					BgL_auxz00_2626 = ((BgL_collapsedz00_bglt) BgL_auxz00_2627);
				}
				return
					(((BgL_collapsedz00_bglt) COBJECT(BgL_auxz00_2626))->BgL_lastz00);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_collapsez00(void)
	{
		{	/* SawMill/collapse.scm 1 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_collapsez00(void)
	{
		{	/* SawMill/collapse.scm 1 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_collapsez00(void)
	{
		{	/* SawMill/collapse.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1863z00zzsaw_collapsez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1863z00zzsaw_collapsez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1863z00zzsaw_collapsez00));
			BGl_modulezd2initializa7ationz75zzsaw_libz00(57049157L,
				BSTRING_TO_STRING(BGl_string1863z00zzsaw_collapsez00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string1863z00zzsaw_collapsez00));
		}

	}

#ifdef __cplusplus
}
#endif
