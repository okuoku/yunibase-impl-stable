/*===========================================================================*/
/*   (SawMill/cast.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent SawMill/cast.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_SAW_CAST_TYPE_DEFINITIONS
#define BGL_SAW_CAST_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_rtl_regz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_varz00;
		obj_t BgL_onexprzf3zf3;
		obj_t BgL_namez00;
		obj_t BgL_keyz00;
		obj_t BgL_debugnamez00;
		obj_t BgL_hardwarez00;
	}                 *BgL_rtl_regz00_bglt;

	typedef struct BgL_rtl_funz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}                 *BgL_rtl_funz00_bglt;

	typedef struct BgL_rtl_returnz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                    *BgL_rtl_returnz00_bglt;

	typedef struct BgL_rtl_loadiz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_atomz00_bgl *BgL_constantz00;
	}                   *BgL_rtl_loadiz00_bglt;

	typedef struct BgL_rtl_loadgz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                   *BgL_rtl_loadgz00_bglt;

	typedef struct BgL_rtl_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_objtypez00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                      *BgL_rtl_getfieldz00_bglt;

	typedef struct BgL_rtl_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                    *BgL_rtl_vallocz00_bglt;

	typedef struct BgL_rtl_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                  *BgL_rtl_vrefz00_bglt;

	typedef struct BgL_rtl_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                     *BgL_rtl_vlengthz00_bglt;

	typedef struct BgL_rtl_storegz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                    *BgL_rtl_storegz00_bglt;

	typedef struct BgL_rtl_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_objtypez00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                      *BgL_rtl_setfieldz00_bglt;

	typedef struct BgL_rtl_vsetz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                  *BgL_rtl_vsetz00_bglt;

	typedef struct BgL_rtl_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_constrz00;
	}                 *BgL_rtl_newz00_bglt;

	typedef struct BgL_rtl_callz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_globalz00_bgl *BgL_varz00;
	}                  *BgL_rtl_callz00_bglt;

	typedef struct BgL_rtl_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_totypez00;
		struct BgL_typez00_bgl *BgL_fromtypez00;
	}                  *BgL_rtl_castz00_bglt;

	typedef struct BgL_rtl_cast_nullz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}                       *BgL_rtl_cast_nullz00_bglt;

	typedef struct BgL_rtl_insz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		obj_t BgL_z52spillz52;
		obj_t BgL_destz00;
		struct BgL_rtl_funz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
	}                 *BgL_rtl_insz00_bglt;

	typedef struct BgL_blockz00_bgl
	{
		header_t header;
		obj_t widening;
		int BgL_labelz00;
		obj_t BgL_predsz00;
		obj_t BgL_succsz00;
		obj_t BgL_firstz00;
	}               *BgL_blockz00_bglt;


#endif													// BGL_SAW_CAST_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_rtl_insz00zzsaw_defsz00;
	static BgL_typez00_bglt
		BGl_z62typezd2destzd2rtl_getfie1416z62zzsaw_castz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_funcallz00zzsaw_defsz00;
	extern obj_t BGl_rtl_funz00zzsaw_defsz00;
	extern obj_t BGl_rtl_newz00zzsaw_defsz00;
	static obj_t BGl_requirezd2initializa7ationz75zzsaw_castz00 = BUNSPEC;
	extern obj_t BGl_rtl_boxrefz00zzsaw_defsz00;
	static BgL_typez00_bglt
		BGl_z62typezd2destzd2rtl_loadi1408z62zzsaw_castz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_vrefz00zzsaw_defsz00;
	static obj_t BGl_toplevelzd2initzd2zzsaw_castz00(void);
	static BgL_typez00_bglt
		BGl_z62typezd2destzd2rtl_nop1403z62zzsaw_castz00(obj_t, obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62typezd2destzd2rtl_instan1428z62zzsaw_castz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_boxsetz00zzsaw_defsz00;
	extern obj_t BGl_rtl_vlengthz00zzsaw_defsz00;
	static obj_t BGl_genericzd2initzd2zzsaw_castz00(void);
	extern obj_t BGl_rtl_vsetz00zzsaw_defsz00;
	static obj_t BGl_objectzd2initzd2zzsaw_castz00(void);
	static obj_t BGl_z62typezd2argszd2rtl_setfie1442z62zzsaw_castz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_typez00zztype_typez00;
	extern obj_t BGl_rtl_lightfuncallz00zzsaw_defsz00;
	static obj_t BGl_z62typezd2args1435zb0zzsaw_castz00(obj_t, obj_t);
	extern obj_t BGl_rtl_regz00zzsaw_defsz00;
	static obj_t BGl_z62typezd2argszd2rtl_vset1446z62zzsaw_castz00(obj_t, obj_t);
	extern obj_t BGl_rtl_vallocz00zzsaw_defsz00;
	extern obj_t BGl_rtl_cast_nullz00zzsaw_defsz00;
	extern obj_t BGl_rtl_loadfunz00zzsaw_defsz00;
	extern obj_t BGl_rtl_movz00zzsaw_defsz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62typezd2destzd2rtl_vref1423z62zzsaw_castz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62typezd2argszd2rtl_vref1444z62zzsaw_castz00(obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62typezd2destzd2rtl_valloc1420z62zzsaw_castz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_rtl_nopz00zzsaw_defsz00;
	static obj_t BGl_methodzd2initzd2zzsaw_castz00(void);
	static obj_t BGl_z62typezd2argszd2rtl_funcal1456z62zzsaw_castz00(obj_t,
		obj_t);
	static obj_t BGl_z62typezd2argszd2rtl_return1454z62zzsaw_castz00(obj_t,
		obj_t);
	static obj_t BGl_assignzd2typezf3z21zzsaw_castz00(BgL_backendz00_bglt,
		BgL_typez00_bglt, obj_t);
	static BgL_rtl_insz00_bglt BGl_addzd2castzd2zzsaw_castz00(obj_t, obj_t);
	static obj_t BGl_addzd2castszd2insz00zzsaw_castz00(BgL_backendz00_bglt,
		obj_t);
	extern obj_t BGl_rtl_instanceofz00zzsaw_defsz00;
	extern obj_t BGl_rtl_setfieldz00zzsaw_defsz00;
	extern obj_t BGl_rtl_callz00zzsaw_defsz00;
	static BgL_typez00_bglt
		BGl_z62typezd2destzd2rtl_new1418z62zzsaw_castz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_rtl_storegz00zzsaw_defsz00;
	extern obj_t BGl_rtl_returnz00zzsaw_defsz00;
	extern obj_t BGl_rtl_applyz00zzsaw_defsz00;
	static obj_t BGl_z62addzd2castszb0zzsaw_castz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzsaw_castz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzsaw_defsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	static obj_t BGl_z62typezd2argszd2rtl_boxset1464z62zzsaw_castz00(obj_t,
		obj_t);
	static BgL_typez00_bglt BGl_z62typezd2destzb0zzsaw_castz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_cfunz00zzast_varz00;
	static obj_t BGl_z62typezd2dest1399zb0zzsaw_castz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62typezd2argszd2rtl_vlengt1448z62zzsaw_castz00(obj_t,
		obj_t);
	static obj_t BGl_z62typezd2argszd2rtl_boxref1462z62zzsaw_castz00(obj_t,
		obj_t);
	extern obj_t BGl_rtl_loadgz00zzsaw_defsz00;
	extern obj_t BGl_rtl_loadiz00zzsaw_defsz00;
	static obj_t BGl_z62typezd2argszd2rtl_lightf1458z62zzsaw_castz00(obj_t,
		obj_t);
	static obj_t BGl_z62typezd2argszd2rtl_apply1460z62zzsaw_castz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzsaw_castz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzsaw_castz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzsaw_castz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzsaw_castz00(void);
	BGL_EXPORTED_DECL obj_t BGl_addzd2castszd2zzsaw_castz00(BgL_backendz00_bglt,
		obj_t);
	extern obj_t BGl_rtl_castz00zzsaw_defsz00;
	static obj_t BGl_addzd2castzd2destz00zzsaw_castz00(BgL_backendz00_bglt,
		obj_t);
	static BgL_typez00_bglt BGl_typezd2destzd2zzsaw_castz00(BgL_rtl_funz00_bglt,
		obj_t);
	static BgL_typez00_bglt
		BGl_z62typezd2destzd2rtl_mov1406z62zzsaw_castz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62typezd2argszb0zzsaw_castz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00(obj_t);
	extern obj_t BGl_rtl_getfieldz00zzsaw_defsz00;
	static obj_t BGl_z62typezd2argszd2rtl_storeg1450z62zzsaw_castz00(obj_t,
		obj_t);
	static BgL_typez00_bglt
		BGl_z62typezd2destzd2rtl_vlengt1426z62zzsaw_castz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62typezd2argszd2rtl_call1452z62zzsaw_castz00(obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62typezd2destzd2rtl_call1432z62zzsaw_castz00(obj_t, obj_t, obj_t);
	extern BgL_typez00_bglt BGl_findzd2typezd2zztype_envz00(obj_t);
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_z62typezd2argszd2rtl_getfie1440z62zzsaw_castz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62typezd2destzd2rtl_loadg1414z62zzsaw_castz00(obj_t, obj_t, obj_t);
	static obj_t BGl_addzd2castzd2argsz00zzsaw_castz00(BgL_backendz00_bglt,
		BgL_rtl_funz00_bglt, obj_t, obj_t, obj_t);
	static obj_t BGl_typezd2argszd2zzsaw_castz00(BgL_rtl_funz00_bglt);
	extern obj_t
		BGl_backendzd2subtypezf3z21zzbackend_backendz00(BgL_backendz00_bglt, obj_t,
		obj_t);
	static BgL_typez00_bglt
		BGl_z62typezd2destzd2rtl_cast_n1430z62zzsaw_castz00(obj_t, obj_t, obj_t);
	static BgL_typez00_bglt
		BGl_z62typezd2destzd2rtl_loadfu1412z62zzsaw_castz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	static BgL_typez00_bglt
		BGl_z62typezd2destzd2rtl_cast1434z62zzsaw_castz00(obj_t, obj_t, obj_t);
	static obj_t __cnst[7];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_addzd2castszd2envz00zzsaw_castz00,
		BgL_bgl_za762addza7d2castsza7b1948za7, BGl_z62addzd2castszb0zzsaw_castz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_GENERIC(BGl_typezd2argszd2envz00zzsaw_castz00,
		BgL_bgl_za762typeza7d2argsza7b1949za7, BGl_z62typezd2argszb0zzsaw_castz00,
		0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1913z00zzsaw_castz00,
		BgL_bgl_string1913za700za7za7s1950za7, "type-dest1399", 13);
	      DEFINE_STRING(BGl_string1915z00zzsaw_castz00,
		BgL_bgl_string1915za700za7za7s1951za7, "type-args1435", 13);
	      DEFINE_STRING(BGl_string1917z00zzsaw_castz00,
		BgL_bgl_string1917za700za7za7s1952za7, "type-dest::type", 15);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1912z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2dest131953z00, BGl_z62typezd2dest1399zb0zzsaw_castz00,
		0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1914z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2args141954z00, BGl_z62typezd2args1435zb0zzsaw_castz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1916z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2destza7d1955za7,
		BGl_z62typezd2destzd2rtl_nop1403z62zzsaw_castz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1918z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2destza7d1956za7,
		BGl_z62typezd2destzd2rtl_mov1406z62zzsaw_castz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1919z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2destza7d1957za7,
		BGl_z62typezd2destzd2rtl_loadi1408z62zzsaw_castz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1920z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2destza7d1958za7,
		BGl_z62typezd2destzd2rtl_loadfu1412z62zzsaw_castz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1921z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2destza7d1959za7,
		BGl_z62typezd2destzd2rtl_loadg1414z62zzsaw_castz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1922z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2destza7d1960za7,
		BGl_z62typezd2destzd2rtl_getfie1416z62zzsaw_castz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1923z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2destza7d1961za7,
		BGl_z62typezd2destzd2rtl_new1418z62zzsaw_castz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1924z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2destza7d1962za7,
		BGl_z62typezd2destzd2rtl_valloc1420z62zzsaw_castz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1925z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2destza7d1963za7,
		BGl_z62typezd2destzd2rtl_vref1423z62zzsaw_castz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1932z00zzsaw_castz00,
		BgL_bgl_string1932za700za7za7s1964za7, "type-args", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1926z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2destza7d1965za7,
		BGl_z62typezd2destzd2rtl_vlengt1426z62zzsaw_castz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1927z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2destza7d1966za7,
		BGl_z62typezd2destzd2rtl_instan1428z62zzsaw_castz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1928z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2destza7d1967za7,
		BGl_z62typezd2destzd2rtl_cast_n1430z62zzsaw_castz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1929z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2destza7d1968za7,
		BGl_z62typezd2destzd2rtl_call1432z62zzsaw_castz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1930z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2destza7d1969za7,
		BGl_z62typezd2destzd2rtl_cast1434z62zzsaw_castz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1931z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2argsza7d1970za7,
		BGl_z62typezd2argszd2rtl_getfie1440z62zzsaw_castz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1933z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2argsza7d1971za7,
		BGl_z62typezd2argszd2rtl_setfie1442z62zzsaw_castz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1934z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2argsza7d1972za7,
		BGl_z62typezd2argszd2rtl_vref1444z62zzsaw_castz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1935z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2argsza7d1973za7,
		BGl_z62typezd2argszd2rtl_vset1446z62zzsaw_castz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1936z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2argsza7d1974za7,
		BGl_z62typezd2argszd2rtl_vlengt1448z62zzsaw_castz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1937z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2argsza7d1975za7,
		BGl_z62typezd2argszd2rtl_storeg1450z62zzsaw_castz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1938z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2argsza7d1976za7,
		BGl_z62typezd2argszd2rtl_call1452z62zzsaw_castz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1945z00zzsaw_castz00,
		BgL_bgl_string1945za700za7za7s1977za7, "saw_cast", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1939z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2argsza7d1978za7,
		BGl_z62typezd2argszd2rtl_return1454z62zzsaw_castz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1946z00zzsaw_castz00,
		BgL_bgl_string1946za700za7za7s1979za7,
		"unspecified int procedure cell obj (bool byte ubyte short ushort char uchar ucs2 void int long ulong elong llong uelong ullong float double) hook ",
		146);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1940z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2argsza7d1980za7,
		BGl_z62typezd2argszd2rtl_funcal1456z62zzsaw_castz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1941z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2argsza7d1981za7,
		BGl_z62typezd2argszd2rtl_lightf1458z62zzsaw_castz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1942z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2argsza7d1982za7,
		BGl_z62typezd2argszd2rtl_apply1460z62zzsaw_castz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1943z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2argsza7d1983za7,
		BGl_z62typezd2argszd2rtl_boxref1462z62zzsaw_castz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1944z00zzsaw_castz00,
		BgL_bgl_za762typeza7d2argsza7d1984za7,
		BGl_z62typezd2argszd2rtl_boxset1464z62zzsaw_castz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_typezd2destzd2envz00zzsaw_castz00,
		BgL_bgl_za762typeza7d2destza7b1985za7, BGl_z62typezd2destzb0zzsaw_castz00,
		0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzsaw_castz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzsaw_castz00(long
		BgL_checksumz00_3258, char *BgL_fromz00_3259)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzsaw_castz00))
				{
					BGl_requirezd2initializa7ationz75zzsaw_castz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzsaw_castz00();
					BGl_libraryzd2moduleszd2initz00zzsaw_castz00();
					BGl_cnstzd2initzd2zzsaw_castz00();
					BGl_importedzd2moduleszd2initz00zzsaw_castz00();
					BGl_genericzd2initzd2zzsaw_castz00();
					BGl_methodzd2initzd2zzsaw_castz00();
					return BGl_toplevelzd2initzd2zzsaw_castz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzsaw_castz00(void)
	{
		{	/* SawMill/cast.scm 1 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "saw_cast");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"saw_cast");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "saw_cast");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "saw_cast");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "saw_cast");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "saw_cast");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "saw_cast");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "saw_cast");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"saw_cast");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "saw_cast");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzsaw_castz00(void)
	{
		{	/* SawMill/cast.scm 1 */
			{	/* SawMill/cast.scm 1 */
				obj_t BgL_cportz00_3147;

				{	/* SawMill/cast.scm 1 */
					obj_t BgL_stringz00_3154;

					BgL_stringz00_3154 = BGl_string1946z00zzsaw_castz00;
					{	/* SawMill/cast.scm 1 */
						obj_t BgL_startz00_3155;

						BgL_startz00_3155 = BINT(0L);
						{	/* SawMill/cast.scm 1 */
							obj_t BgL_endz00_3156;

							BgL_endz00_3156 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3154)));
							{	/* SawMill/cast.scm 1 */

								BgL_cportz00_3147 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3154, BgL_startz00_3155, BgL_endz00_3156);
				}}}}
				{
					long BgL_iz00_3148;

					BgL_iz00_3148 = 6L;
				BgL_loopz00_3149:
					if ((BgL_iz00_3148 == -1L))
						{	/* SawMill/cast.scm 1 */
							return BUNSPEC;
						}
					else
						{	/* SawMill/cast.scm 1 */
							{	/* SawMill/cast.scm 1 */
								obj_t BgL_arg1947z00_3150;

								{	/* SawMill/cast.scm 1 */

									{	/* SawMill/cast.scm 1 */
										obj_t BgL_locationz00_3152;

										BgL_locationz00_3152 = BBOOL(((bool_t) 0));
										{	/* SawMill/cast.scm 1 */

											BgL_arg1947z00_3150 =
												BGl_readz00zz__readerz00(BgL_cportz00_3147,
												BgL_locationz00_3152);
										}
									}
								}
								{	/* SawMill/cast.scm 1 */
									int BgL_tmpz00_3289;

									BgL_tmpz00_3289 = (int) (BgL_iz00_3148);
									CNST_TABLE_SET(BgL_tmpz00_3289, BgL_arg1947z00_3150);
							}}
							{	/* SawMill/cast.scm 1 */
								int BgL_auxz00_3153;

								BgL_auxz00_3153 = (int) ((BgL_iz00_3148 - 1L));
								{
									long BgL_iz00_3294;

									BgL_iz00_3294 = (long) (BgL_auxz00_3153);
									BgL_iz00_3148 = BgL_iz00_3294;
									goto BgL_loopz00_3149;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzsaw_castz00(void)
	{
		{	/* SawMill/cast.scm 1 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzsaw_castz00(void)
	{
		{	/* SawMill/cast.scm 1 */
			return BUNSPEC;
		}

	}



/* add-casts */
	BGL_EXPORTED_DEF obj_t BGl_addzd2castszd2zzsaw_castz00(BgL_backendz00_bglt
		BgL_backz00_3, obj_t BgL_lz00_4)
	{
		{	/* SawMill/cast.scm 12 */
			{
				obj_t BgL_l1391z00_2176;

				{	/* SawMill/cast.scm 13 */
					bool_t BgL_tmpz00_3297;

					BgL_l1391z00_2176 = BgL_lz00_4;
				BgL_zc3z04anonymousza31474ze3z87_2177:
					if (PAIRP(BgL_l1391z00_2176))
						{	/* SawMill/cast.scm 13 */
							{	/* SawMill/cast.scm 14 */
								obj_t BgL_bz00_2179;

								BgL_bz00_2179 = CAR(BgL_l1391z00_2176);
								{	/* SawMill/cast.scm 14 */
									obj_t BgL_hookz00_2180;

									BgL_hookz00_2180 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
									{	/* SawMill/cast.scm 15 */
										obj_t BgL_g1152z00_2181;

										BgL_g1152z00_2181 =
											(((BgL_blockz00_bglt) COBJECT(
													((BgL_blockz00_bglt) BgL_bz00_2179)))->BgL_firstz00);
										{
											obj_t BgL_lz00_2183;
											obj_t BgL_lastz00_2184;

											BgL_lz00_2183 = BgL_g1152z00_2181;
											BgL_lastz00_2184 = BgL_hookz00_2180;
										BgL_zc3z04anonymousza31476ze3z87_2185:
											if (NULLP(BgL_lz00_2183))
												{	/* SawMill/cast.scm 17 */
													obj_t BgL_arg1485z00_2187;

													BgL_arg1485z00_2187 = CDR(BgL_hookz00_2180);
													((((BgL_blockz00_bglt) COBJECT(
																	((BgL_blockz00_bglt) BgL_bz00_2179)))->
															BgL_firstz00) =
														((obj_t) ((obj_t) BgL_arg1485z00_2187)), BUNSPEC);
												}
											else
												{	/* SawMill/cast.scm 18 */
													obj_t BgL_insz00_2188;

													BgL_insz00_2188 = CAR(((obj_t) BgL_lz00_2183));
													{	/* SawMill/cast.scm 19 */
														obj_t BgL_cinsz00_2190;

														BgL_cinsz00_2190 =
															BGl_addzd2castszd2insz00zzsaw_castz00
															(BgL_backz00_3, BgL_insz00_2188);
														SET_CDR(BgL_lastz00_2184, BgL_cinsz00_2190);
														{	/* SawMill/cast.scm 21 */
															obj_t BgL_arg1489z00_2191;
															obj_t BgL_arg1502z00_2192;

															BgL_arg1489z00_2191 =
																CDR(((obj_t) BgL_lz00_2183));
															BgL_arg1502z00_2192 =
																BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00
																(BgL_cinsz00_2190);
															{
																obj_t BgL_lastz00_3319;
																obj_t BgL_lz00_3318;

																BgL_lz00_3318 = BgL_arg1489z00_2191;
																BgL_lastz00_3319 = BgL_arg1502z00_2192;
																BgL_lastz00_2184 = BgL_lastz00_3319;
																BgL_lz00_2183 = BgL_lz00_3318;
																goto BgL_zc3z04anonymousza31476ze3z87_2185;
															}
														}
													}
												}
										}
									}
								}
							}
							{
								obj_t BgL_l1391z00_3320;

								BgL_l1391z00_3320 = CDR(BgL_l1391z00_2176);
								BgL_l1391z00_2176 = BgL_l1391z00_3320;
								goto BgL_zc3z04anonymousza31474ze3z87_2177;
							}
						}
					else
						{	/* SawMill/cast.scm 13 */
							BgL_tmpz00_3297 = ((bool_t) 1);
						}
					return BBOOL(BgL_tmpz00_3297);
				}
			}
		}

	}



/* &add-casts */
	obj_t BGl_z62addzd2castszb0zzsaw_castz00(obj_t BgL_envz00_3037,
		obj_t BgL_backz00_3038, obj_t BgL_lz00_3039)
	{
		{	/* SawMill/cast.scm 12 */
			return
				BGl_addzd2castszd2zzsaw_castz00(
				((BgL_backendz00_bglt) BgL_backz00_3038), BgL_lz00_3039);
		}

	}



/* add-cast */
	BgL_rtl_insz00_bglt BGl_addzd2castzd2zzsaw_castz00(obj_t BgL_fromz00_5,
		obj_t BgL_toz00_6)
	{
		{	/* SawMill/cast.scm 24 */
			{	/* SawMill/cast.scm 26 */
				BgL_typez00_bglt BgL_typez00_2196;

				BgL_typez00_2196 =
					(((BgL_rtl_regz00_bglt) COBJECT(
							((BgL_rtl_regz00_bglt) BgL_toz00_6)))->BgL_typez00);
				{	/* SawMill/cast.scm 27 */
					BgL_rtl_insz00_bglt BgL_new1154z00_2197;

					{	/* SawMill/cast.scm 27 */
						BgL_rtl_insz00_bglt BgL_new1153z00_2201;

						BgL_new1153z00_2201 =
							((BgL_rtl_insz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_rtl_insz00_bgl))));
						{	/* SawMill/cast.scm 27 */
							long BgL_arg1514z00_2202;

							BgL_arg1514z00_2202 = BGL_CLASS_NUM(BGl_rtl_insz00zzsaw_defsz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_new1153z00_2201),
								BgL_arg1514z00_2202);
						}
						{	/* SawMill/cast.scm 27 */
							BgL_objectz00_bglt BgL_tmpz00_3331;

							BgL_tmpz00_3331 = ((BgL_objectz00_bglt) BgL_new1153z00_2201);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3331, BFALSE);
						}
						((BgL_objectz00_bglt) BgL_new1153z00_2201);
						BgL_new1154z00_2197 = BgL_new1153z00_2201;
					}
					((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1154z00_2197))->BgL_locz00) =
						((obj_t) BFALSE), BUNSPEC);
					((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1154z00_2197))->
							BgL_z52spillz52) = ((obj_t) BNIL), BUNSPEC);
					((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1154z00_2197))->BgL_destz00) =
						((obj_t) BgL_toz00_6), BUNSPEC);
					{
						BgL_rtl_funz00_bglt BgL_auxz00_3338;

						{	/* SawMill/cast.scm 28 */
							BgL_rtl_castz00_bglt BgL_new1157z00_2198;

							{	/* SawMill/cast.scm 28 */
								BgL_rtl_castz00_bglt BgL_new1156z00_2199;

								BgL_new1156z00_2199 =
									((BgL_rtl_castz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_rtl_castz00_bgl))));
								{	/* SawMill/cast.scm 28 */
									long BgL_arg1513z00_2200;

									{	/* SawMill/cast.scm 28 */
										obj_t BgL_classz00_2757;

										BgL_classz00_2757 = BGl_rtl_castz00zzsaw_defsz00;
										BgL_arg1513z00_2200 = BGL_CLASS_NUM(BgL_classz00_2757);
									}
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1156z00_2199),
										BgL_arg1513z00_2200);
								}
								BgL_new1157z00_2198 = BgL_new1156z00_2199;
							}
							((((BgL_rtl_funz00_bglt) COBJECT(
											((BgL_rtl_funz00_bglt) BgL_new1157z00_2198)))->
									BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
							((((BgL_rtl_castz00_bglt) COBJECT(BgL_new1157z00_2198))->
									BgL_totypez00) =
								((BgL_typez00_bglt) BgL_typez00_2196), BUNSPEC);
							((((BgL_rtl_castz00_bglt) COBJECT(BgL_new1157z00_2198))->
									BgL_fromtypez00) =
								((BgL_typez00_bglt) (((BgL_rtl_regz00_bglt)
											COBJECT(((BgL_rtl_regz00_bglt) BgL_fromz00_5)))->
										BgL_typez00)), BUNSPEC);
							BgL_auxz00_3338 = ((BgL_rtl_funz00_bglt) BgL_new1157z00_2198);
						}
						((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1154z00_2197))->
								BgL_funz00) = ((BgL_rtl_funz00_bglt) BgL_auxz00_3338), BUNSPEC);
					}
					((((BgL_rtl_insz00_bglt) COBJECT(BgL_new1154z00_2197))->BgL_argsz00) =
						((obj_t) MAKE_YOUNG_PAIR(BgL_fromz00_5, BNIL)), BUNSPEC);
					return BgL_new1154z00_2197;
				}
			}
		}

	}



/* add-casts-ins */
	obj_t BGl_addzd2castszd2insz00zzsaw_castz00(BgL_backendz00_bglt BgL_backz00_7,
		obj_t BgL_insz00_8)
	{
		{	/* SawMill/cast.scm 33 */
			{	/* SawMill/cast.scm 35 */
				obj_t BgL_castsz00_2204;

				BgL_castsz00_2204 =
					BGl_addzd2castzd2destz00zzsaw_castz00(BgL_backz00_7, BgL_insz00_8);
				{	/* SawMill/cast.scm 36 */
					BgL_rtl_funz00_bglt BgL_arg1516z00_2205;
					obj_t BgL_arg1535z00_2206;
					obj_t BgL_arg1540z00_2207;
					obj_t BgL_arg1544z00_2208;

					BgL_arg1516z00_2205 =
						(((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_insz00_8)))->BgL_funz00);
					BgL_arg1535z00_2206 =
						(((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_insz00_8)))->BgL_argsz00);
					{	/* SawMill/cast.scm 36 */
						BgL_rtl_funz00_bglt BgL_arg1546z00_2209;

						BgL_arg1546z00_2209 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_insz00_8)))->BgL_funz00);
						BgL_arg1540z00_2207 =
							BGl_typezd2argszd2zzsaw_castz00(BgL_arg1546z00_2209);
					}
					BgL_arg1544z00_2208 =
						MAKE_YOUNG_PAIR(BgL_insz00_8, BgL_castsz00_2204);
					return
						BGl_addzd2castzd2argsz00zzsaw_castz00(BgL_backz00_7,
						BgL_arg1516z00_2205, BgL_arg1535z00_2206, BgL_arg1540z00_2207,
						BgL_arg1544z00_2208);
				}
			}
		}

	}



/* add-cast-dest */
	obj_t BGl_addzd2castzd2destz00zzsaw_castz00(BgL_backendz00_bglt BgL_backz00_9,
		obj_t BgL_insz00_10)
	{
		{	/* SawMill/cast.scm 38 */
			if (CBOOL(
					(((BgL_rtl_insz00_bglt) COBJECT(
								((BgL_rtl_insz00_bglt) BgL_insz00_10)))->BgL_destz00)))
				{	/* SawMill/cast.scm 41 */
					BgL_typez00_bglt BgL_realz00_2212;
					BgL_typez00_bglt BgL_declaredz00_2213;

					{	/* SawMill/cast.scm 41 */
						BgL_rtl_funz00_bglt BgL_arg1564z00_2224;
						obj_t BgL_arg1565z00_2225;

						BgL_arg1564z00_2224 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_insz00_10)))->BgL_funz00);
						BgL_arg1565z00_2225 =
							(((BgL_rtl_insz00_bglt) COBJECT(
									((BgL_rtl_insz00_bglt) BgL_insz00_10)))->BgL_argsz00);
						BgL_realz00_2212 =
							BGl_typezd2destzd2zzsaw_castz00(BgL_arg1564z00_2224,
							BgL_arg1565z00_2225);
					}
					BgL_declaredz00_2213 =
						(((BgL_rtl_regz00_bglt) COBJECT(
								((BgL_rtl_regz00_bglt)
									(((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_insz00_10)))->
										BgL_destz00))))->BgL_typez00);
					if (CBOOL(BGl_assignzd2typezf3z21zzsaw_castz00(BgL_backz00_9,
								BgL_realz00_2212, ((obj_t) BgL_declaredz00_2213))))
						{	/* SawMill/cast.scm 42 */
							return BNIL;
						}
					else
						{	/* SawMill/cast.scm 44 */
							BgL_rtl_regz00_bglt BgL_rrz00_2215;

							{	/* SawMill/cast.scm 44 */
								BgL_rtl_regz00_bglt BgL_new1161z00_2218;

								{	/* SawMill/cast.scm 45 */
									BgL_rtl_regz00_bglt BgL_new1160z00_2222;

									BgL_new1160z00_2222 =
										((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
													BgL_rtl_regz00_bgl))));
									{	/* SawMill/cast.scm 45 */
										long BgL_arg1561z00_2223;

										BgL_arg1561z00_2223 =
											BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
										BGL_OBJECT_CLASS_NUM_SET(
											((BgL_objectz00_bglt) BgL_new1160z00_2222),
											BgL_arg1561z00_2223);
									}
									{	/* SawMill/cast.scm 45 */
										BgL_objectz00_bglt BgL_tmpz00_3384;

										BgL_tmpz00_3384 =
											((BgL_objectz00_bglt) BgL_new1160z00_2222);
										BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3384, BFALSE);
									}
									((BgL_objectz00_bglt) BgL_new1160z00_2222);
									BgL_new1161z00_2218 = BgL_new1160z00_2222;
								}
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1161z00_2218))->
										BgL_typez00) =
									((BgL_typez00_bglt) BgL_realz00_2212), BUNSPEC);
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1161z00_2218))->
										BgL_varz00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1161z00_2218))->
										BgL_onexprzf3zf3) = ((obj_t) BFALSE), BUNSPEC);
								{
									obj_t BgL_auxz00_3391;

									{	/* SawMill/cast.scm 47 */

										{	/* SawMill/cast.scm 47 */

											BgL_auxz00_3391 =
												BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
									}}
									((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1161z00_2218))->
											BgL_namez00) = ((obj_t) BgL_auxz00_3391), BUNSPEC);
								}
								{
									obj_t BgL_auxz00_3394;

									{	/* SawMill/cast.scm 47 */
										obj_t BgL_arg1553z00_2220;

										{	/* SawMill/cast.scm 47 */
											obj_t BgL_arg1559z00_2221;

											{	/* SawMill/cast.scm 47 */
												obj_t BgL_classz00_2766;

												BgL_classz00_2766 = BGl_rtl_regz00zzsaw_defsz00;
												BgL_arg1559z00_2221 =
													BGL_CLASS_ALL_FIELDS(BgL_classz00_2766);
											}
											BgL_arg1553z00_2220 = VECTOR_REF(BgL_arg1559z00_2221, 4L);
										}
										BgL_auxz00_3394 =
											BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
											(BgL_arg1553z00_2220);
									}
									((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1161z00_2218))->
											BgL_keyz00) = ((obj_t) BgL_auxz00_3394), BUNSPEC);
								}
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1161z00_2218))->
										BgL_debugnamez00) = ((obj_t) BFALSE), BUNSPEC);
								((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1161z00_2218))->
										BgL_hardwarez00) = ((obj_t) BFALSE), BUNSPEC);
								BgL_rrz00_2215 = BgL_new1161z00_2218;
							}
							{	/* SawMill/cast.scm 48 */
								obj_t BgL_xz00_2216;

								BgL_xz00_2216 =
									(((BgL_rtl_insz00_bglt) COBJECT(
											((BgL_rtl_insz00_bglt) BgL_insz00_10)))->BgL_destz00);
								((((BgL_rtl_insz00_bglt) COBJECT(
												((BgL_rtl_insz00_bglt) BgL_insz00_10)))->BgL_destz00) =
									((obj_t) ((obj_t) BgL_rrz00_2215)), BUNSPEC);
								{	/* SawMill/cast.scm 50 */
									BgL_rtl_insz00_bglt BgL_arg1552z00_2217;

									BgL_arg1552z00_2217 =
										BGl_addzd2castzd2zzsaw_castz00(
										((obj_t) BgL_rrz00_2215), BgL_xz00_2216);
									return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1552z00_2217), BNIL);
								}
							}
						}
				}
			else
				{	/* SawMill/cast.scm 40 */
					return BNIL;
				}
		}

	}



/* add-cast-args */
	obj_t BGl_addzd2castzd2argsz00zzsaw_castz00(BgL_backendz00_bglt
		BgL_backz00_11, BgL_rtl_funz00_bglt BgL_funz00_12, obj_t BgL_argsz00_13,
		obj_t BgL_typesz00_14, obj_t BgL_castsz00_15)
	{
		{	/* SawMill/cast.scm 53 */
			if (NULLP(BgL_typesz00_14))
				{	/* SawMill/cast.scm 54 */
					return BgL_castsz00_15;
				}
			else
				{	/* SawMill/cast.scm 56 */
					obj_t BgL_castsz00_2228;

					{	/* SawMill/cast.scm 56 */
						obj_t BgL_arg1594z00_2242;
						obj_t BgL_arg1595z00_2243;

						BgL_arg1594z00_2242 = CDR(((obj_t) BgL_argsz00_13));
						BgL_arg1595z00_2243 = CDR(((obj_t) BgL_typesz00_14));
						BgL_castsz00_2228 =
							BGl_addzd2castzd2argsz00zzsaw_castz00(BgL_backz00_11,
							BgL_funz00_12, BgL_arg1594z00_2242, BgL_arg1595z00_2243,
							BgL_castsz00_15);
					}
					{	/* SawMill/cast.scm 57 */
						obj_t BgL_rz00_2229;
						obj_t BgL_tz00_2230;

						BgL_rz00_2229 = CAR(((obj_t) BgL_argsz00_13));
						BgL_tz00_2230 = CAR(((obj_t) BgL_typesz00_14));
						{	/* SawMill/cast.scm 58 */
							bool_t BgL_test1993z00_3421;

							{	/* SawMill/cast.scm 58 */
								BgL_typez00_bglt BgL_arg1593z00_2241;

								BgL_arg1593z00_2241 =
									(((BgL_rtl_regz00_bglt) COBJECT(
											((BgL_rtl_regz00_bglt) BgL_rz00_2229)))->BgL_typez00);
								BgL_test1993z00_3421 =
									CBOOL(BGl_assignzd2typezf3z21zzsaw_castz00(BgL_backz00_11,
										BgL_arg1593z00_2241, BgL_tz00_2230));
							}
							if (BgL_test1993z00_3421)
								{	/* SawMill/cast.scm 58 */
									return BgL_castsz00_2228;
								}
							else
								{	/* SawMill/cast.scm 60 */
									BgL_rtl_regz00_bglt BgL_rrz00_2233;

									{	/* SawMill/cast.scm 60 */
										BgL_rtl_regz00_bglt BgL_new1163z00_2235;

										{	/* SawMill/cast.scm 61 */
											BgL_rtl_regz00_bglt BgL_new1162z00_2239;

											BgL_new1162z00_2239 =
												((BgL_rtl_regz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_rtl_regz00_bgl))));
											{	/* SawMill/cast.scm 61 */
												long BgL_arg1591z00_2240;

												BgL_arg1591z00_2240 =
													BGL_CLASS_NUM(BGl_rtl_regz00zzsaw_defsz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1162z00_2239),
													BgL_arg1591z00_2240);
											}
											{	/* SawMill/cast.scm 61 */
												BgL_objectz00_bglt BgL_tmpz00_3430;

												BgL_tmpz00_3430 =
													((BgL_objectz00_bglt) BgL_new1162z00_2239);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3430, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1162z00_2239);
											BgL_new1163z00_2235 = BgL_new1162z00_2239;
										}
										((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1163z00_2235))->
												BgL_typez00) =
											((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_tz00_2230)),
											BUNSPEC);
										((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1163z00_2235))->
												BgL_varz00) = ((obj_t) BFALSE), BUNSPEC);
										((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1163z00_2235))->
												BgL_onexprzf3zf3) = ((obj_t) BFALSE), BUNSPEC);
										{
											obj_t BgL_auxz00_3438;

											{	/* SawMill/cast.scm 63 */

												{	/* SawMill/cast.scm 63 */

													BgL_auxz00_3438 =
														BGl_gensymz00zz__r4_symbols_6_4z00(BFALSE);
											}}
											((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1163z00_2235))->
													BgL_namez00) = ((obj_t) BgL_auxz00_3438), BUNSPEC);
										}
										{
											obj_t BgL_auxz00_3441;

											{	/* SawMill/cast.scm 63 */
												obj_t BgL_arg1585z00_2237;

												{	/* SawMill/cast.scm 63 */
													obj_t BgL_arg1589z00_2238;

													{	/* SawMill/cast.scm 63 */
														obj_t BgL_classz00_2778;

														BgL_classz00_2778 = BGl_rtl_regz00zzsaw_defsz00;
														BgL_arg1589z00_2238 =
															BGL_CLASS_ALL_FIELDS(BgL_classz00_2778);
													}
													BgL_arg1585z00_2237 =
														VECTOR_REF(BgL_arg1589z00_2238, 4L);
												}
												BgL_auxz00_3441 =
													BGl_classzd2fieldzd2defaultzd2valuezd2zz__objectz00
													(BgL_arg1585z00_2237);
											}
											((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1163z00_2235))->
													BgL_keyz00) = ((obj_t) BgL_auxz00_3441), BUNSPEC);
										}
										((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1163z00_2235))->
												BgL_debugnamez00) = ((obj_t) BFALSE), BUNSPEC);
										((((BgL_rtl_regz00_bglt) COBJECT(BgL_new1163z00_2235))->
												BgL_hardwarez00) = ((obj_t) BFALSE), BUNSPEC);
										BgL_rrz00_2233 = BgL_new1163z00_2235;
									}
									{	/* SawMill/cast.scm 64 */
										obj_t BgL_auxz00_3450;
										obj_t BgL_tmpz00_3448;

										BgL_auxz00_3450 = ((obj_t) BgL_rrz00_2233);
										BgL_tmpz00_3448 = ((obj_t) BgL_argsz00_13);
										SET_CAR(BgL_tmpz00_3448, BgL_auxz00_3450);
									}
									{	/* SawMill/cast.scm 65 */
										BgL_rtl_insz00_bglt BgL_arg1584z00_2234;

										BgL_arg1584z00_2234 =
											BGl_addzd2castzd2zzsaw_castz00(BgL_rz00_2229,
											((obj_t) BgL_rrz00_2233));
										return
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_arg1584z00_2234), BgL_castsz00_2228);
									}
								}
						}
					}
				}
		}

	}



/* assign-type? */
	obj_t BGl_assignzd2typezf3z21zzsaw_castz00(BgL_backendz00_bglt BgL_backz00_16,
		BgL_typez00_bglt BgL_t1z00_17, obj_t BgL_t2z00_18)
	{
		{	/* SawMill/cast.scm 67 */
			{	/* SawMill/cast.scm 68 */
				bool_t BgL__ortest_1164z00_2244;

				BgL__ortest_1164z00_2244 = (((obj_t) BgL_t1z00_17) == BgL_t2z00_18);
				if (BgL__ortest_1164z00_2244)
					{	/* SawMill/cast.scm 68 */
						return BBOOL(BgL__ortest_1164z00_2244);
					}
				else
					{	/* SawMill/cast.scm 69 */
						obj_t BgL__ortest_1165z00_2245;

						BgL__ortest_1165z00_2245 =
							BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
							(((BgL_typez00_bglt) COBJECT(BgL_t1z00_17))->BgL_idz00),
							CNST_TABLE_REF(1));
						if (CBOOL(BgL__ortest_1165z00_2245))
							{	/* SawMill/cast.scm 69 */
								return BgL__ortest_1165z00_2245;
							}
						else
							{	/* SawMill/cast.scm 70 */
								obj_t BgL__ortest_1166z00_2246;

								BgL__ortest_1166z00_2246 =
									BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
									(((BgL_typez00_bglt) COBJECT(
												((BgL_typez00_bglt) BgL_t2z00_18)))->BgL_idz00),
									CNST_TABLE_REF(1));
								if (CBOOL(BgL__ortest_1166z00_2246))
									{	/* SawMill/cast.scm 70 */
										return BgL__ortest_1166z00_2246;
									}
								else
									{	/* SawMill/cast.scm 70 */
										return
											BGl_backendzd2subtypezf3z21zzbackend_backendz00
											(BgL_backz00_16, ((obj_t) BgL_t1z00_17), BgL_t2z00_18);
									}
							}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzsaw_castz00(void)
	{
		{	/* SawMill/cast.scm 1 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzsaw_castz00(void)
	{
		{	/* SawMill/cast.scm 1 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_typezd2destzd2envz00zzsaw_castz00, BGl_proc1912z00zzsaw_castz00,
				BGl_rtl_funz00zzsaw_defsz00, BGl_string1913z00zzsaw_castz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_typezd2argszd2envz00zzsaw_castz00, BGl_proc1914z00zzsaw_castz00,
				BGl_rtl_funz00zzsaw_defsz00, BGl_string1915z00zzsaw_castz00);
		}

	}



/* &type-args1435 */
	obj_t BGl_z62typezd2args1435zb0zzsaw_castz00(obj_t BgL_envz00_3042,
		obj_t BgL_funz00_3043)
	{
		{	/* SawMill/cast.scm 130 */
			return BNIL;
		}

	}



/* &type-dest1399 */
	obj_t BGl_z62typezd2dest1399zb0zzsaw_castz00(obj_t BgL_envz00_3044,
		obj_t BgL_funz00_3045, obj_t BgL_argsz00_3046)
	{
		{	/* SawMill/cast.scm 81 */
			return ((obj_t) BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(2)));
		}

	}



/* type-dest */
	BgL_typez00_bglt BGl_typezd2destzd2zzsaw_castz00(BgL_rtl_funz00_bglt
		BgL_funz00_20, obj_t BgL_argsz00_21)
	{
		{	/* SawMill/cast.scm 81 */
			{	/* SawMill/cast.scm 81 */
				obj_t BgL_method1401z00_2257;

				{	/* SawMill/cast.scm 81 */
					obj_t BgL_res1902z00_2817;

					{	/* SawMill/cast.scm 81 */
						long BgL_objzd2classzd2numz00_2788;

						BgL_objzd2classzd2numz00_2788 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_20));
						{	/* SawMill/cast.scm 81 */
							obj_t BgL_arg1811z00_2789;

							BgL_arg1811z00_2789 =
								PROCEDURE_REF(BGl_typezd2destzd2envz00zzsaw_castz00,
								(int) (1L));
							{	/* SawMill/cast.scm 81 */
								int BgL_offsetz00_2792;

								BgL_offsetz00_2792 = (int) (BgL_objzd2classzd2numz00_2788);
								{	/* SawMill/cast.scm 81 */
									long BgL_offsetz00_2793;

									BgL_offsetz00_2793 =
										((long) (BgL_offsetz00_2792) - OBJECT_TYPE);
									{	/* SawMill/cast.scm 81 */
										long BgL_modz00_2794;

										BgL_modz00_2794 =
											(BgL_offsetz00_2793 >> (int) ((long) ((int) (4L))));
										{	/* SawMill/cast.scm 81 */
											long BgL_restz00_2796;

											BgL_restz00_2796 =
												(BgL_offsetz00_2793 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawMill/cast.scm 81 */

												{	/* SawMill/cast.scm 81 */
													obj_t BgL_bucketz00_2798;

													BgL_bucketz00_2798 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2789), BgL_modz00_2794);
													BgL_res1902z00_2817 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2798), BgL_restz00_2796);
					}}}}}}}}
					BgL_method1401z00_2257 = BgL_res1902z00_2817;
				}
				return
					((BgL_typez00_bglt)
					BGL_PROCEDURE_CALL2(BgL_method1401z00_2257,
						((obj_t) BgL_funz00_20), BgL_argsz00_21));
			}
		}

	}



/* &type-dest */
	BgL_typez00_bglt BGl_z62typezd2destzb0zzsaw_castz00(obj_t BgL_envz00_3047,
		obj_t BgL_funz00_3048, obj_t BgL_argsz00_3049)
	{
		{	/* SawMill/cast.scm 81 */
			return
				BGl_typezd2destzd2zzsaw_castz00(
				((BgL_rtl_funz00_bglt) BgL_funz00_3048), BgL_argsz00_3049);
		}

	}



/* type-args */
	obj_t BGl_typezd2argszd2zzsaw_castz00(BgL_rtl_funz00_bglt BgL_funz00_50)
	{
		{	/* SawMill/cast.scm 130 */
			{	/* SawMill/cast.scm 130 */
				obj_t BgL_method1437z00_2258;

				{	/* SawMill/cast.scm 130 */
					obj_t BgL_res1907z00_2848;

					{	/* SawMill/cast.scm 130 */
						long BgL_objzd2classzd2numz00_2819;

						BgL_objzd2classzd2numz00_2819 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_50));
						{	/* SawMill/cast.scm 130 */
							obj_t BgL_arg1811z00_2820;

							BgL_arg1811z00_2820 =
								PROCEDURE_REF(BGl_typezd2argszd2envz00zzsaw_castz00,
								(int) (1L));
							{	/* SawMill/cast.scm 130 */
								int BgL_offsetz00_2823;

								BgL_offsetz00_2823 = (int) (BgL_objzd2classzd2numz00_2819);
								{	/* SawMill/cast.scm 130 */
									long BgL_offsetz00_2824;

									BgL_offsetz00_2824 =
										((long) (BgL_offsetz00_2823) - OBJECT_TYPE);
									{	/* SawMill/cast.scm 130 */
										long BgL_modz00_2825;

										BgL_modz00_2825 =
											(BgL_offsetz00_2824 >> (int) ((long) ((int) (4L))));
										{	/* SawMill/cast.scm 130 */
											long BgL_restz00_2827;

											BgL_restz00_2827 =
												(BgL_offsetz00_2824 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* SawMill/cast.scm 130 */

												{	/* SawMill/cast.scm 130 */
													obj_t BgL_bucketz00_2829;

													BgL_bucketz00_2829 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2820), BgL_modz00_2825);
													BgL_res1907z00_2848 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2829), BgL_restz00_2827);
					}}}}}}}}
					BgL_method1437z00_2258 = BgL_res1907z00_2848;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1437z00_2258, ((obj_t) BgL_funz00_50));
			}
		}

	}



/* &type-args */
	obj_t BGl_z62typezd2argszb0zzsaw_castz00(obj_t BgL_envz00_3050,
		obj_t BgL_funz00_3051)
	{
		{	/* SawMill/cast.scm 130 */
			return
				BGl_typezd2argszd2zzsaw_castz00(
				((BgL_rtl_funz00_bglt) BgL_funz00_3051));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzsaw_castz00(void)
	{
		{	/* SawMill/cast.scm 1 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2destzd2envz00zzsaw_castz00, BGl_rtl_nopz00zzsaw_defsz00,
				BGl_proc1916z00zzsaw_castz00, BGl_string1917z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2destzd2envz00zzsaw_castz00, BGl_rtl_movz00zzsaw_defsz00,
				BGl_proc1918z00zzsaw_castz00, BGl_string1917z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2destzd2envz00zzsaw_castz00, BGl_rtl_loadiz00zzsaw_defsz00,
				BGl_proc1919z00zzsaw_castz00, BGl_string1917z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2destzd2envz00zzsaw_castz00, BGl_rtl_loadfunz00zzsaw_defsz00,
				BGl_proc1920z00zzsaw_castz00, BGl_string1917z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2destzd2envz00zzsaw_castz00, BGl_rtl_loadgz00zzsaw_defsz00,
				BGl_proc1921z00zzsaw_castz00, BGl_string1917z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2destzd2envz00zzsaw_castz00,
				BGl_rtl_getfieldz00zzsaw_defsz00, BGl_proc1922z00zzsaw_castz00,
				BGl_string1917z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2destzd2envz00zzsaw_castz00, BGl_rtl_newz00zzsaw_defsz00,
				BGl_proc1923z00zzsaw_castz00, BGl_string1917z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2destzd2envz00zzsaw_castz00, BGl_rtl_vallocz00zzsaw_defsz00,
				BGl_proc1924z00zzsaw_castz00, BGl_string1917z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2destzd2envz00zzsaw_castz00, BGl_rtl_vrefz00zzsaw_defsz00,
				BGl_proc1925z00zzsaw_castz00, BGl_string1917z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2destzd2envz00zzsaw_castz00, BGl_rtl_vlengthz00zzsaw_defsz00,
				BGl_proc1926z00zzsaw_castz00, BGl_string1917z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2destzd2envz00zzsaw_castz00,
				BGl_rtl_instanceofz00zzsaw_defsz00, BGl_proc1927z00zzsaw_castz00,
				BGl_string1917z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2destzd2envz00zzsaw_castz00,
				BGl_rtl_cast_nullz00zzsaw_defsz00, BGl_proc1928z00zzsaw_castz00,
				BGl_string1917z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2destzd2envz00zzsaw_castz00, BGl_rtl_callz00zzsaw_defsz00,
				BGl_proc1929z00zzsaw_castz00, BGl_string1917z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2destzd2envz00zzsaw_castz00, BGl_rtl_castz00zzsaw_defsz00,
				BGl_proc1930z00zzsaw_castz00, BGl_string1917z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2argszd2envz00zzsaw_castz00,
				BGl_rtl_getfieldz00zzsaw_defsz00, BGl_proc1931z00zzsaw_castz00,
				BGl_string1932z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2argszd2envz00zzsaw_castz00,
				BGl_rtl_setfieldz00zzsaw_defsz00, BGl_proc1933z00zzsaw_castz00,
				BGl_string1932z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2argszd2envz00zzsaw_castz00, BGl_rtl_vrefz00zzsaw_defsz00,
				BGl_proc1934z00zzsaw_castz00, BGl_string1932z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2argszd2envz00zzsaw_castz00, BGl_rtl_vsetz00zzsaw_defsz00,
				BGl_proc1935z00zzsaw_castz00, BGl_string1932z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2argszd2envz00zzsaw_castz00, BGl_rtl_vlengthz00zzsaw_defsz00,
				BGl_proc1936z00zzsaw_castz00, BGl_string1932z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2argszd2envz00zzsaw_castz00, BGl_rtl_storegz00zzsaw_defsz00,
				BGl_proc1937z00zzsaw_castz00, BGl_string1932z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2argszd2envz00zzsaw_castz00, BGl_rtl_callz00zzsaw_defsz00,
				BGl_proc1938z00zzsaw_castz00, BGl_string1932z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2argszd2envz00zzsaw_castz00, BGl_rtl_returnz00zzsaw_defsz00,
				BGl_proc1939z00zzsaw_castz00, BGl_string1932z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2argszd2envz00zzsaw_castz00, BGl_rtl_funcallz00zzsaw_defsz00,
				BGl_proc1940z00zzsaw_castz00, BGl_string1932z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2argszd2envz00zzsaw_castz00,
				BGl_rtl_lightfuncallz00zzsaw_defsz00, BGl_proc1941z00zzsaw_castz00,
				BGl_string1932z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2argszd2envz00zzsaw_castz00, BGl_rtl_applyz00zzsaw_defsz00,
				BGl_proc1942z00zzsaw_castz00, BGl_string1932z00zzsaw_castz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2argszd2envz00zzsaw_castz00, BGl_rtl_boxrefz00zzsaw_defsz00,
				BGl_proc1943z00zzsaw_castz00, BGl_string1932z00zzsaw_castz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2argszd2envz00zzsaw_castz00, BGl_rtl_boxsetz00zzsaw_defsz00,
				BGl_proc1944z00zzsaw_castz00, BGl_string1932z00zzsaw_castz00);
		}

	}



/* &type-args-rtl_boxset1464 */
	obj_t BGl_z62typezd2argszd2rtl_boxset1464z62zzsaw_castz00(obj_t
		BgL_envz00_3079, obj_t BgL_funz00_3080)
	{
		{	/* SawMill/cast.scm 176 */
			{	/* SawMill/cast.scm 177 */
				BgL_typez00_bglt BgL_arg1702z00_3162;

				BgL_arg1702z00_3162 =
					BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(3));
				return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1702z00_3162), BNIL);
			}
		}

	}



/* &type-args-rtl_boxref1462 */
	obj_t BGl_z62typezd2argszd2rtl_boxref1462z62zzsaw_castz00(obj_t
		BgL_envz00_3081, obj_t BgL_funz00_3082)
	{
		{	/* SawMill/cast.scm 173 */
			{	/* SawMill/cast.scm 174 */
				BgL_typez00_bglt BgL_arg1701z00_3164;

				BgL_arg1701z00_3164 =
					BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(3));
				return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1701z00_3164), BNIL);
			}
		}

	}



/* &type-args-rtl_apply1460 */
	obj_t BGl_z62typezd2argszd2rtl_apply1460z62zzsaw_castz00(obj_t
		BgL_envz00_3083, obj_t BgL_funz00_3084)
	{
		{	/* SawMill/cast.scm 170 */
			{	/* SawMill/cast.scm 171 */
				BgL_typez00_bglt BgL_arg1700z00_3166;

				BgL_arg1700z00_3166 =
					BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(4));
				return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1700z00_3166), BNIL);
			}
		}

	}



/* &type-args-rtl_lightf1458 */
	obj_t BGl_z62typezd2argszd2rtl_lightf1458z62zzsaw_castz00(obj_t
		BgL_envz00_3085, obj_t BgL_funz00_3086)
	{
		{	/* SawMill/cast.scm 167 */
			{	/* SawMill/cast.scm 168 */
				BgL_typez00_bglt BgL_arg1699z00_3168;

				BgL_arg1699z00_3168 =
					BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(4));
				return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1699z00_3168), BNIL);
			}
		}

	}



/* &type-args-rtl_funcal1456 */
	obj_t BGl_z62typezd2argszd2rtl_funcal1456z62zzsaw_castz00(obj_t
		BgL_envz00_3087, obj_t BgL_funz00_3088)
	{
		{	/* SawMill/cast.scm 164 */
			{	/* SawMill/cast.scm 165 */
				BgL_typez00_bglt BgL_arg1692z00_3170;

				BgL_arg1692z00_3170 =
					BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(4));
				return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1692z00_3170), BNIL);
			}
		}

	}



/* &type-args-rtl_return1454 */
	obj_t BGl_z62typezd2argszd2rtl_return1454z62zzsaw_castz00(obj_t
		BgL_envz00_3089, obj_t BgL_funz00_3090)
	{
		{	/* SawMill/cast.scm 161 */
			{	/* SawMill/cast.scm 162 */
				BgL_typez00_bglt BgL_arg1691z00_3172;

				BgL_arg1691z00_3172 =
					(((BgL_rtl_returnz00_bglt) COBJECT(
							((BgL_rtl_returnz00_bglt) BgL_funz00_3090)))->BgL_typez00);
				return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1691z00_3172), BNIL);
			}
		}

	}



/* &type-args-rtl_call1452 */
	obj_t BGl_z62typezd2argszd2rtl_call1452z62zzsaw_castz00(obj_t BgL_envz00_3091,
		obj_t BgL_funz00_3092)
	{
		{	/* SawMill/cast.scm 151 */
			{	/* SawMill/cast.scm 152 */
				BgL_valuez00_bglt BgL_fz00_3174;

				BgL_fz00_3174 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								(((BgL_rtl_callz00_bglt) COBJECT(
											((BgL_rtl_callz00_bglt) BgL_funz00_3092)))->
									BgL_varz00))))->BgL_valuez00);
				{	/* SawMill/cast.scm 153 */

					{	/* SawMill/cast.scm 154 */
						bool_t BgL_test1997z00_3600;

						{	/* SawMill/cast.scm 154 */
							obj_t BgL_classz00_3175;

							BgL_classz00_3175 = BGl_cfunz00zzast_varz00;
							{	/* SawMill/cast.scm 154 */
								BgL_objectz00_bglt BgL_arg1807z00_3176;

								{	/* SawMill/cast.scm 154 */
									obj_t BgL_tmpz00_3601;

									BgL_tmpz00_3601 =
										((obj_t) ((BgL_objectz00_bglt) BgL_fz00_3174));
									BgL_arg1807z00_3176 = (BgL_objectz00_bglt) (BgL_tmpz00_3601);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* SawMill/cast.scm 154 */
										long BgL_idxz00_3177;

										BgL_idxz00_3177 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3176);
										BgL_test1997z00_3600 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3177 + 3L)) == BgL_classz00_3175);
									}
								else
									{	/* SawMill/cast.scm 154 */
										bool_t BgL_res1908z00_3180;

										{	/* SawMill/cast.scm 154 */
											obj_t BgL_oclassz00_3181;

											{	/* SawMill/cast.scm 154 */
												obj_t BgL_arg1815z00_3182;
												long BgL_arg1816z00_3183;

												BgL_arg1815z00_3182 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* SawMill/cast.scm 154 */
													long BgL_arg1817z00_3184;

													BgL_arg1817z00_3184 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3176);
													BgL_arg1816z00_3183 =
														(BgL_arg1817z00_3184 - OBJECT_TYPE);
												}
												BgL_oclassz00_3181 =
													VECTOR_REF(BgL_arg1815z00_3182, BgL_arg1816z00_3183);
											}
											{	/* SawMill/cast.scm 154 */
												bool_t BgL__ortest_1115z00_3185;

												BgL__ortest_1115z00_3185 =
													(BgL_classz00_3175 == BgL_oclassz00_3181);
												if (BgL__ortest_1115z00_3185)
													{	/* SawMill/cast.scm 154 */
														BgL_res1908z00_3180 = BgL__ortest_1115z00_3185;
													}
												else
													{	/* SawMill/cast.scm 154 */
														long BgL_odepthz00_3186;

														{	/* SawMill/cast.scm 154 */
															obj_t BgL_arg1804z00_3187;

															BgL_arg1804z00_3187 = (BgL_oclassz00_3181);
															BgL_odepthz00_3186 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3187);
														}
														if ((3L < BgL_odepthz00_3186))
															{	/* SawMill/cast.scm 154 */
																obj_t BgL_arg1802z00_3188;

																{	/* SawMill/cast.scm 154 */
																	obj_t BgL_arg1803z00_3189;

																	BgL_arg1803z00_3189 = (BgL_oclassz00_3181);
																	BgL_arg1802z00_3188 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3189,
																		3L);
																}
																BgL_res1908z00_3180 =
																	(BgL_arg1802z00_3188 == BgL_classz00_3175);
															}
														else
															{	/* SawMill/cast.scm 154 */
																BgL_res1908z00_3180 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1997z00_3600 = BgL_res1908z00_3180;
									}
							}
						}
						if (BgL_test1997z00_3600)
							{	/* SawMill/cast.scm 154 */
								return
									(((BgL_cfunz00_bglt) COBJECT(
											((BgL_cfunz00_bglt) BgL_fz00_3174)))->BgL_argszd2typezd2);
							}
						else
							{	/* SawMill/cast.scm 156 */
								obj_t BgL_rz00_3190;

								BgL_rz00_3190 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt) BgL_fz00_3174)))->BgL_argsz00);
								{	/* SawMill/cast.scm 157 */
									bool_t BgL_test2001z00_3628;

									if (NULLP(BgL_rz00_3190))
										{	/* SawMill/cast.scm 157 */
											BgL_test2001z00_3628 = ((bool_t) 1);
										}
									else
										{	/* SawMill/cast.scm 157 */
											obj_t BgL_arg1688z00_3191;

											BgL_arg1688z00_3191 = CAR(((obj_t) BgL_rz00_3190));
											{	/* SawMill/cast.scm 157 */
												obj_t BgL_classz00_3192;

												BgL_classz00_3192 = BGl_typez00zztype_typez00;
												if (BGL_OBJECTP(BgL_arg1688z00_3191))
													{	/* SawMill/cast.scm 157 */
														BgL_objectz00_bglt BgL_arg1807z00_3193;

														BgL_arg1807z00_3193 =
															(BgL_objectz00_bglt) (BgL_arg1688z00_3191);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* SawMill/cast.scm 157 */
																long BgL_idxz00_3194;

																BgL_idxz00_3194 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3193);
																BgL_test2001z00_3628 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3194 + 1L)) ==
																	BgL_classz00_3192);
															}
														else
															{	/* SawMill/cast.scm 157 */
																bool_t BgL_res1909z00_3197;

																{	/* SawMill/cast.scm 157 */
																	obj_t BgL_oclassz00_3198;

																	{	/* SawMill/cast.scm 157 */
																		obj_t BgL_arg1815z00_3199;
																		long BgL_arg1816z00_3200;

																		BgL_arg1815z00_3199 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* SawMill/cast.scm 157 */
																			long BgL_arg1817z00_3201;

																			BgL_arg1817z00_3201 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3193);
																			BgL_arg1816z00_3200 =
																				(BgL_arg1817z00_3201 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3198 =
																			VECTOR_REF(BgL_arg1815z00_3199,
																			BgL_arg1816z00_3200);
																	}
																	{	/* SawMill/cast.scm 157 */
																		bool_t BgL__ortest_1115z00_3202;

																		BgL__ortest_1115z00_3202 =
																			(BgL_classz00_3192 == BgL_oclassz00_3198);
																		if (BgL__ortest_1115z00_3202)
																			{	/* SawMill/cast.scm 157 */
																				BgL_res1909z00_3197 =
																					BgL__ortest_1115z00_3202;
																			}
																		else
																			{	/* SawMill/cast.scm 157 */
																				long BgL_odepthz00_3203;

																				{	/* SawMill/cast.scm 157 */
																					obj_t BgL_arg1804z00_3204;

																					BgL_arg1804z00_3204 =
																						(BgL_oclassz00_3198);
																					BgL_odepthz00_3203 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3204);
																				}
																				if ((1L < BgL_odepthz00_3203))
																					{	/* SawMill/cast.scm 157 */
																						obj_t BgL_arg1802z00_3205;

																						{	/* SawMill/cast.scm 157 */
																							obj_t BgL_arg1803z00_3206;

																							BgL_arg1803z00_3206 =
																								(BgL_oclassz00_3198);
																							BgL_arg1802z00_3205 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3206, 1L);
																						}
																						BgL_res1909z00_3197 =
																							(BgL_arg1802z00_3205 ==
																							BgL_classz00_3192);
																					}
																				else
																					{	/* SawMill/cast.scm 157 */
																						BgL_res1909z00_3197 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2001z00_3628 = BgL_res1909z00_3197;
															}
													}
												else
													{	/* SawMill/cast.scm 157 */
														BgL_test2001z00_3628 = ((bool_t) 0);
													}
											}
										}
									if (BgL_test2001z00_3628)
										{	/* SawMill/cast.scm 157 */
											return BgL_rz00_3190;
										}
									else
										{	/* SawMill/cast.scm 157 */
											if (NULLP(BgL_rz00_3190))
												{	/* SawMill/cast.scm 159 */
													return BNIL;
												}
											else
												{	/* SawMill/cast.scm 159 */
													obj_t BgL_head1395z00_3207;

													{	/* SawMill/cast.scm 159 */
														BgL_typez00_bglt BgL_arg1678z00_3208;

														BgL_arg1678z00_3208 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt)
																			CAR(
																				((obj_t) BgL_rz00_3190))))))->
															BgL_typez00);
														BgL_head1395z00_3207 =
															MAKE_YOUNG_PAIR(((obj_t) BgL_arg1678z00_3208),
															BNIL);
													}
													{	/* SawMill/cast.scm 159 */
														obj_t BgL_g1398z00_3209;

														BgL_g1398z00_3209 = CDR(((obj_t) BgL_rz00_3190));
														{
															obj_t BgL_l1393z00_3211;
															obj_t BgL_tail1396z00_3212;

															BgL_l1393z00_3211 = BgL_g1398z00_3209;
															BgL_tail1396z00_3212 = BgL_head1395z00_3207;
														BgL_zc3z04anonymousza31657ze3z87_3210:
															if (NULLP(BgL_l1393z00_3211))
																{	/* SawMill/cast.scm 159 */
																	return BgL_head1395z00_3207;
																}
															else
																{	/* SawMill/cast.scm 159 */
																	obj_t BgL_newtail1397z00_3213;

																	{	/* SawMill/cast.scm 159 */
																		BgL_typez00_bglt BgL_arg1663z00_3214;

																		BgL_arg1663z00_3214 =
																			(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_localz00_bglt)
																							CAR(
																								((obj_t)
																									BgL_l1393z00_3211))))))->
																			BgL_typez00);
																		BgL_newtail1397z00_3213 =
																			MAKE_YOUNG_PAIR(((obj_t)
																				BgL_arg1663z00_3214), BNIL);
																	}
																	SET_CDR(BgL_tail1396z00_3212,
																		BgL_newtail1397z00_3213);
																	{	/* SawMill/cast.scm 159 */
																		obj_t BgL_arg1661z00_3215;

																		BgL_arg1661z00_3215 =
																			CDR(((obj_t) BgL_l1393z00_3211));
																		{
																			obj_t BgL_tail1396z00_3679;
																			obj_t BgL_l1393z00_3678;

																			BgL_l1393z00_3678 = BgL_arg1661z00_3215;
																			BgL_tail1396z00_3679 =
																				BgL_newtail1397z00_3213;
																			BgL_tail1396z00_3212 =
																				BgL_tail1396z00_3679;
																			BgL_l1393z00_3211 = BgL_l1393z00_3678;
																			goto
																				BgL_zc3z04anonymousza31657ze3z87_3210;
																		}
																	}
																}
														}
													}
												}
										}
								}
							}
					}
				}
			}
		}

	}



/* &type-args-rtl_storeg1450 */
	obj_t BGl_z62typezd2argszd2rtl_storeg1450z62zzsaw_castz00(obj_t
		BgL_envz00_3093, obj_t BgL_funz00_3094)
	{
		{	/* SawMill/cast.scm 148 */
			{	/* SawMill/cast.scm 149 */
				BgL_typez00_bglt BgL_arg1646z00_3217;

				BgL_arg1646z00_3217 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								(((BgL_rtl_storegz00_bglt) COBJECT(
											((BgL_rtl_storegz00_bglt) BgL_funz00_3094)))->
									BgL_varz00))))->BgL_typez00);
				return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1646z00_3217), BNIL);
			}
		}

	}



/* &type-args-rtl_vlengt1448 */
	obj_t BGl_z62typezd2argszd2rtl_vlengt1448z62zzsaw_castz00(obj_t
		BgL_envz00_3095, obj_t BgL_funz00_3096)
	{
		{	/* SawMill/cast.scm 145 */
			{	/* SawMill/cast.scm 146 */
				BgL_typez00_bglt BgL_arg1642z00_3219;

				BgL_arg1642z00_3219 =
					(((BgL_rtl_vlengthz00_bglt) COBJECT(
							((BgL_rtl_vlengthz00_bglt) BgL_funz00_3096)))->BgL_vtypez00);
				return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1642z00_3219), BNIL);
			}
		}

	}



/* &type-args-rtl_vset1446 */
	obj_t BGl_z62typezd2argszd2rtl_vset1446z62zzsaw_castz00(obj_t BgL_envz00_3097,
		obj_t BgL_funz00_3098)
	{
		{	/* SawMill/cast.scm 142 */
			{	/* SawMill/cast.scm 143 */
				BgL_typez00_bglt BgL_arg1630z00_3221;

				BgL_arg1630z00_3221 =
					(((BgL_rtl_vsetz00_bglt) COBJECT(
							((BgL_rtl_vsetz00_bglt) BgL_funz00_3098)))->BgL_vtypez00);
				return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1630z00_3221), BNIL);
			}
		}

	}



/* &type-args-rtl_vref1444 */
	obj_t BGl_z62typezd2argszd2rtl_vref1444z62zzsaw_castz00(obj_t BgL_envz00_3099,
		obj_t BgL_funz00_3100)
	{
		{	/* SawMill/cast.scm 139 */
			{	/* SawMill/cast.scm 140 */
				BgL_typez00_bglt BgL_arg1629z00_3223;

				BgL_arg1629z00_3223 =
					(((BgL_rtl_vrefz00_bglt) COBJECT(
							((BgL_rtl_vrefz00_bglt) BgL_funz00_3100)))->BgL_vtypez00);
				return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1629z00_3223), BNIL);
			}
		}

	}



/* &type-args-rtl_setfie1442 */
	obj_t BGl_z62typezd2argszd2rtl_setfie1442z62zzsaw_castz00(obj_t
		BgL_envz00_3101, obj_t BgL_funz00_3102)
	{
		{	/* SawMill/cast.scm 136 */
			{	/* SawMill/cast.scm 137 */
				BgL_typez00_bglt BgL_arg1625z00_3225;
				obj_t BgL_arg1626z00_3226;

				BgL_arg1625z00_3225 =
					(((BgL_rtl_setfieldz00_bglt) COBJECT(
							((BgL_rtl_setfieldz00_bglt) BgL_funz00_3102)))->BgL_objtypez00);
				{	/* SawMill/cast.scm 137 */
					BgL_typez00_bglt BgL_arg1627z00_3227;

					BgL_arg1627z00_3227 =
						(((BgL_rtl_setfieldz00_bglt) COBJECT(
								((BgL_rtl_setfieldz00_bglt) BgL_funz00_3102)))->BgL_typez00);
					BgL_arg1626z00_3226 =
						MAKE_YOUNG_PAIR(((obj_t) BgL_arg1627z00_3227), BNIL);
				}
				return
					MAKE_YOUNG_PAIR(((obj_t) BgL_arg1625z00_3225), BgL_arg1626z00_3226);
			}
		}

	}



/* &type-args-rtl_getfie1440 */
	obj_t BGl_z62typezd2argszd2rtl_getfie1440z62zzsaw_castz00(obj_t
		BgL_envz00_3103, obj_t BgL_funz00_3104)
	{
		{	/* SawMill/cast.scm 133 */
			{	/* SawMill/cast.scm 134 */
				BgL_typez00_bglt BgL_arg1616z00_3229;

				BgL_arg1616z00_3229 =
					(((BgL_rtl_getfieldz00_bglt) COBJECT(
							((BgL_rtl_getfieldz00_bglt) BgL_funz00_3104)))->BgL_objtypez00);
				return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1616z00_3229), BNIL);
			}
		}

	}



/* &type-dest-rtl_cast1434 */
	BgL_typez00_bglt BGl_z62typezd2destzd2rtl_cast1434z62zzsaw_castz00(obj_t
		BgL_envz00_3105, obj_t BgL_funz00_3106, obj_t BgL_argsz00_3107)
	{
		{	/* SawMill/cast.scm 126 */
			return
				(((BgL_rtl_castz00_bglt) COBJECT(
						((BgL_rtl_castz00_bglt) BgL_funz00_3106)))->BgL_totypez00);
		}

	}



/* &type-dest-rtl_call1432 */
	BgL_typez00_bglt BGl_z62typezd2destzd2rtl_call1432z62zzsaw_castz00(obj_t
		BgL_envz00_3108, obj_t BgL_funz00_3109, obj_t BgL_argsz00_3110)
	{
		{	/* SawMill/cast.scm 123 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt)
							(((BgL_rtl_callz00_bglt) COBJECT(
										((BgL_rtl_callz00_bglt) BgL_funz00_3109)))->BgL_varz00))))->
				BgL_typez00);
		}

	}



/* &type-dest-rtl_cast_n1430 */
	BgL_typez00_bglt BGl_z62typezd2destzd2rtl_cast_n1430z62zzsaw_castz00(obj_t
		BgL_envz00_3111, obj_t BgL_funz00_3112, obj_t BgL_argsz00_3113)
	{
		{	/* SawMill/cast.scm 119 */
			return
				(((BgL_rtl_cast_nullz00_bglt) COBJECT(
						((BgL_rtl_cast_nullz00_bglt) BgL_funz00_3112)))->BgL_typez00);
		}

	}



/* &type-dest-rtl_instan1428 */
	BgL_typez00_bglt BGl_z62typezd2destzd2rtl_instan1428z62zzsaw_castz00(obj_t
		BgL_envz00_3114, obj_t BgL_funz00_3115, obj_t BgL_argsz00_3116)
	{
		{	/* SawMill/cast.scm 116 */
			return BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(5));
		}

	}



/* &type-dest-rtl_vlengt1426 */
	BgL_typez00_bglt BGl_z62typezd2destzd2rtl_vlengt1426z62zzsaw_castz00(obj_t
		BgL_envz00_3117, obj_t BgL_funz00_3118, obj_t BgL_argsz00_3119)
	{
		{	/* SawMill/cast.scm 113 */
			return BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(5));
		}

	}



/* &type-dest-rtl_vref1423 */
	BgL_typez00_bglt BGl_z62typezd2destzd2rtl_vref1423z62zzsaw_castz00(obj_t
		BgL_envz00_3120, obj_t BgL_funz00_3121, obj_t BgL_argsz00_3122)
	{
		{	/* SawMill/cast.scm 110 */
			return
				(((BgL_rtl_vrefz00_bglt) COBJECT(
						((BgL_rtl_vrefz00_bglt) BgL_funz00_3121)))->BgL_typez00);
		}

	}



/* &type-dest-rtl_valloc1420 */
	BgL_typez00_bglt BGl_z62typezd2destzd2rtl_valloc1420z62zzsaw_castz00(obj_t
		BgL_envz00_3123, obj_t BgL_funz00_3124, obj_t BgL_argsz00_3125)
	{
		{	/* SawMill/cast.scm 107 */
			return
				(((BgL_rtl_vallocz00_bglt) COBJECT(
						((BgL_rtl_vallocz00_bglt) BgL_funz00_3124)))->BgL_vtypez00);
		}

	}



/* &type-dest-rtl_new1418 */
	BgL_typez00_bglt BGl_z62typezd2destzd2rtl_new1418z62zzsaw_castz00(obj_t
		BgL_envz00_3126, obj_t BgL_funz00_3127, obj_t BgL_argsz00_3128)
	{
		{	/* SawMill/cast.scm 104 */
			return
				(((BgL_rtl_newz00_bglt) COBJECT(
						((BgL_rtl_newz00_bglt) BgL_funz00_3127)))->BgL_typez00);
		}

	}



/* &type-dest-rtl_getfie1416 */
	BgL_typez00_bglt BGl_z62typezd2destzd2rtl_getfie1416z62zzsaw_castz00(obj_t
		BgL_envz00_3129, obj_t BgL_funz00_3130, obj_t BgL_argsz00_3131)
	{
		{	/* SawMill/cast.scm 101 */
			return
				(((BgL_rtl_getfieldz00_bglt) COBJECT(
						((BgL_rtl_getfieldz00_bglt) BgL_funz00_3130)))->BgL_typez00);
		}

	}



/* &type-dest-rtl_loadg1414 */
	BgL_typez00_bglt BGl_z62typezd2destzd2rtl_loadg1414z62zzsaw_castz00(obj_t
		BgL_envz00_3132, obj_t BgL_funz00_3133, obj_t BgL_argsz00_3134)
	{
		{	/* SawMill/cast.scm 98 */
			return
				(((BgL_variablez00_bglt) COBJECT(
						((BgL_variablez00_bglt)
							(((BgL_rtl_loadgz00_bglt) COBJECT(
										((BgL_rtl_loadgz00_bglt) BgL_funz00_3133)))->
								BgL_varz00))))->BgL_typez00);
		}

	}



/* &type-dest-rtl_loadfu1412 */
	BgL_typez00_bglt BGl_z62typezd2destzd2rtl_loadfu1412z62zzsaw_castz00(obj_t
		BgL_envz00_3135, obj_t BgL_funz00_3136, obj_t BgL_argsz00_3137)
	{
		{	/* SawMill/cast.scm 93 */
			return BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(4));
		}

	}



/* &type-dest-rtl_loadi1408 */
	BgL_typez00_bglt BGl_z62typezd2destzd2rtl_loadi1408z62zzsaw_castz00(obj_t
		BgL_envz00_3138, obj_t BgL_funz00_3139, obj_t BgL_argsz00_3140)
	{
		{	/* SawMill/cast.scm 90 */
			return
				(((BgL_nodez00_bglt) COBJECT(
						((BgL_nodez00_bglt)
							(((BgL_rtl_loadiz00_bglt) COBJECT(
										((BgL_rtl_loadiz00_bglt) BgL_funz00_3139)))->
								BgL_constantz00))))->BgL_typez00);
		}

	}



/* &type-dest-rtl_mov1406 */
	BgL_typez00_bglt BGl_z62typezd2destzd2rtl_mov1406z62zzsaw_castz00(obj_t
		BgL_envz00_3141, obj_t BgL_funz00_3142, obj_t BgL_argsz00_3143)
	{
		{	/* SawMill/cast.scm 87 */
			return
				(((BgL_rtl_regz00_bglt) COBJECT(
						((BgL_rtl_regz00_bglt)
							CAR(((obj_t) ((obj_t) BgL_argsz00_3143))))))->BgL_typez00);
		}

	}



/* &type-dest-rtl_nop1403 */
	BgL_typez00_bglt BGl_z62typezd2destzd2rtl_nop1403z62zzsaw_castz00(obj_t
		BgL_envz00_3144, obj_t BgL_funz00_3145, obj_t BgL_argsz00_3146)
	{
		{	/* SawMill/cast.scm 84 */
			return BGl_findzd2typezd2zztype_envz00(CNST_TABLE_REF(6));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzsaw_castz00(void)
	{
		{	/* SawMill/cast.scm 1 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1945z00zzsaw_castz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1945z00zzsaw_castz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1945z00zzsaw_castz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1945z00zzsaw_castz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1945z00zzsaw_castz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1945z00zzsaw_castz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1945z00zzsaw_castz00));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string1945z00zzsaw_castz00));
			return
				BGl_modulezd2initializa7ationz75zzsaw_defsz00(404844772L,
				BSTRING_TO_STRING(BGl_string1945z00zzsaw_castz00));
		}

	}

#ifdef __cplusplus
}
#endif
