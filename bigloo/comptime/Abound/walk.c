/*===========================================================================*/
/*   (Abound/walk.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Abound/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_ABOUND_WALK_TYPE_DEFINITIONS
#define BGL_ABOUND_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_privatez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                 *BgL_privatez00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_ABOUND_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_za2s16vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
	static obj_t BGl_z62aboundzd2walkz12za2zzabound_walkz00(obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_locationzd2fullzd2fnamez00zztools_locationz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzabound_walkz00 = BUNSPEC;
	extern obj_t BGl_makezd2typedzd2identz00zzast_identz00(obj_t, obj_t);
	static BgL_appz00_bglt
		BGl_z62zc3z04anonymousza31871ze3ze5zzabound_walkz00(obj_t,
		BgL_nodezf2effectzf2_bglt, obj_t, obj_t);
	extern obj_t BGl_za2vectorza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2boxzd2setz121348za2zzabound_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2setzd2exzd2i1340z62zzabound_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2appzd2ly1316zb0zzabound_walkz00(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2conditio1330z62zzabound_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2makezd2box1344zb0zzabound_walkz00(obj_t, obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62aboundzd2node1307zb0zzabound_walkz00(obj_t, obj_t);
	static obj_t BGl_za2s32vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
	static BgL_vsetz12z12_bglt
		BGl_z62zc3z04anonymousza32014ze3ze5zzabound_walkz00(obj_t,
		BgL_nodezf2effectzf2_bglt, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzabound_walkz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nodez00_bglt);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2jumpzd2exzd21342z62zzabound_walkz00(obj_t, obj_t);
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzabound_walkz00(void);
	static obj_t BGl_za2u64vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static obj_t BGl_za2stringzd2refza2zd2zzabound_walkz00 = BUNSPEC;
	static obj_t BGl_objectzd2initzd2zzabound_walkz00(void);
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2vref1322z62zzabound_walkz00(obj_t, obj_t);
	static obj_t BGl_za2u32vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2extern1320z62zzabound_walkz00(obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_lvtypezd2nodezd2zzast_lvtypez00(BgL_nodez00_bglt);
	static BgL_appz00_bglt
		BGl_z62zc3z04anonymousza31894ze3ze5zzabound_walkz00(obj_t,
		BgL_nodezf2effectzf2_bglt, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2switch1334z62zzabound_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2sequence1310z62zzabound_walkz00(obj_t, obj_t);
	static obj_t BGl_za2s64vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2letzd2fun1336zb0zzabound_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_za2s32vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
	static obj_t BGl_methodzd2initzd2zzabound_walkz00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static obj_t BGl_aboundzd2funz12zc0zzabound_walkz00(obj_t);
	extern BgL_nodez00_bglt
		BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_za2hvectorsza2z00zztype_cachez00;
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static BgL_vrefz00_bglt
		BGl_z62zc3z04anonymousza31998ze3ze5zzabound_walkz00(obj_t,
		BgL_nodezf2effectzf2_bglt, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2vsetz121324z70zzabound_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_aboundzd2nodezd2zzabound_walkz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	static obj_t BGl_z62initzd2cachez12za2zzabound_walkz00(obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t BGl_za2u16vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
	static obj_t BGl_za2u8vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_aboundzd2walkz12zc0zzabound_walkz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzabound_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_privatez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_za2f64vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_za2f32vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2funcall1318z62zzabound_walkz00(obj_t, obj_t);
	extern obj_t BGl_vrefz00zzast_nodez00;
	static obj_t BGl_za2f64vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzabound_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzabound_walkz00(void);
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2letzd2var1338zb0zzabound_walkz00(obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzabound_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzabound_walkz00(void);
	static obj_t BGl_za2u8vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
	static obj_t BGl_za2s16vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
	static obj_t BGl_za2s8vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
	static obj_t BGl_za2u64vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
	static BgL_nodez00_bglt BGl_z62aboundzd2nodezb0zzabound_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2boxzd2ref1346zb0zzabound_walkz00(obj_t, obj_t);
	static obj_t BGl_za2stringzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2cast1326z62zzabound_walkz00(obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	static obj_t BGl_za2structzd2refza2zd2zzabound_walkz00 = BUNSPEC;
	static obj_t BGl_za2structzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_gensymz00zz__r4_symbols_6_4z00(obj_t);
	static obj_t BGl_za2f32vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2setq1328z62zzabound_walkz00(obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00(obj_t);
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2sync1312z62zzabound_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_arrayzd2refzd2zzabound_walkz00(BgL_nodezf2effectzf2_bglt, obj_t, obj_t,
		obj_t, BgL_typez00_bglt, obj_t, obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_arrayzd2setz12zc0zzabound_walkz00(BgL_nodezf2effectzf2_bglt, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_za2u16vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2fail1332z62zzabound_walkz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2app1314z62zzabound_walkz00(obj_t, obj_t);
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t BGl_za2u32vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
	static obj_t BGl_za2s64vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static bool_t BGl_aboundzd2nodeza2z12z62zzabound_walkz00(obj_t);
	static obj_t BGl_za2s8vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	static obj_t BGl_z62clearzd2cachez12za2zzabound_walkz00(obj_t);
	static obj_t __cnst[45];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_initzd2cachez12zd2envz12zzabound_walkz00,
		BgL_bgl_za762initza7d2cacheza72182za7,
		BGl_z62initzd2cachez12za2zzabound_walkz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2140z00zzabound_walkz00,
		BgL_bgl_string2140za700za7za7a2183za7, "Abound", 6);
	      DEFINE_STRING(BGl_string2141z00zzabound_walkz00,
		BgL_bgl_string2141za700za7za7a2184za7, "   . ", 5);
	      DEFINE_STRING(BGl_string2142z00zzabound_walkz00,
		BgL_bgl_string2142za700za7za7a2185za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string2143z00zzabound_walkz00,
		BgL_bgl_string2143za700za7za7a2186za7, " error", 6);
	      DEFINE_STRING(BGl_string2144z00zzabound_walkz00,
		BgL_bgl_string2144za700za7za7a2187za7, "s", 1);
	      DEFINE_STRING(BGl_string2145z00zzabound_walkz00,
		BgL_bgl_string2145za700za7za7a2188za7, "", 0);
	      DEFINE_STRING(BGl_string2146z00zzabound_walkz00,
		BgL_bgl_string2146za700za7za7a2189za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string2147z00zzabound_walkz00,
		BgL_bgl_string2147za700za7za7a2190za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string2148z00zzabound_walkz00,
		BgL_bgl_string2148za700za7za7a2191za7, "-ref", 4);
	      DEFINE_STRING(BGl_string2149z00zzabound_walkz00,
		BgL_bgl_string2149za700za7za7a2192za7, "-set!", 5);
	      DEFINE_STRING(BGl_string2151z00zzabound_walkz00,
		BgL_bgl_string2151za700za7za7a2193za7, "abound-node1307", 15);
	      DEFINE_STRING(BGl_string2153z00zzabound_walkz00,
		BgL_bgl_string2153za700za7za7a2194za7, "abound-node", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2150z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2195z00,
		BGl_z62aboundzd2node1307zb0zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2152z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2196z00,
		BGl_z62aboundzd2nodezd2sequence1310z62zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2154z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2197z00,
		BGl_z62aboundzd2nodezd2sync1312z62zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2155z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2198z00,
		BGl_z62aboundzd2nodezd2app1314z62zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2156z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2199z00,
		BGl_z62aboundzd2nodezd2appzd2ly1316zb0zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2157z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2200z00,
		BGl_z62aboundzd2nodezd2funcall1318z62zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2158z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2201z00,
		BGl_z62aboundzd2nodezd2extern1320z62zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2159z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2202z00,
		BGl_z62aboundzd2nodezd2vref1322z62zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2160z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2203z00,
		BGl_z62aboundzd2nodezd2vsetz121324z70zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2161z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2204z00,
		BGl_z62aboundzd2nodezd2cast1326z62zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2162z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2205z00,
		BGl_z62aboundzd2nodezd2setq1328z62zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2163z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2206z00,
		BGl_z62aboundzd2nodezd2conditio1330z62zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2164z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2207z00,
		BGl_z62aboundzd2nodezd2fail1332z62zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2165z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2208z00,
		BGl_z62aboundzd2nodezd2switch1334z62zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2166z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2209z00,
		BGl_z62aboundzd2nodezd2letzd2fun1336zb0zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2167z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2210z00,
		BGl_z62aboundzd2nodezd2letzd2var1338zb0zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2168z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2211z00,
		BGl_z62aboundzd2nodezd2setzd2exzd2i1340z62zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2175z00zzabound_walkz00,
		BgL_bgl_string2175za700za7za7a2212za7, "string-ref", 10);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2169z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2213z00,
		BGl_z62aboundzd2nodezd2jumpzd2exzd21342z62zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2176z00zzabound_walkz00,
		BgL_bgl_string2176za700za7za7a2214za7, "string-set!", 11);
	      DEFINE_STRING(BGl_string2179z00zzabound_walkz00,
		BgL_bgl_string2179za700za7za7a2215za7, "abound_walk", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2170z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2216z00,
		BGl_z62aboundzd2nodezd2makezd2box1344zb0zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2171z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2217z00,
		BGl_z62aboundzd2nodezd2boxzd2ref1346zb0zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2172z00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2218z00,
		BGl_z62aboundzd2nodezd2boxzd2setz121348za2zzabound_walkz00, 0L, BUNSPEC, 1);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2173z00zzabound_walkz00,
		BgL_bgl_za762za7c3za704anonymo2219za7,
		BGl_z62zc3z04anonymousza32014ze3ze5zzabound_walkz00);
	      DEFINE_STRING(BGl_string2180z00zzabound_walkz00,
		BgL_bgl_string2180za700za7za7a2220za7,
		"$string-bound-check? $string-length s let if failure @ index-out-of-bounds-error __error $vector-bound-check? $tvector-length $hvector-length $vector-length location l i v $f64vector-set! $f32vector-set! $u64vector-set! $s64vector-set! $u32vector-set! $s32vector-set! $u16vector-set! $s16vector-set! $u8vector-set! $s8vector-set! $f64vector-ref $f32vector-ref $u64vector-ref $s64vector-ref $u32vector-ref $s32vector-ref $u16vector-ref $s16vector-ref $u8vector-ref $s8vector-ref $struct-set! $struct-ref $string-set! $string-ref foreign (clear-cache!) pass-started (init-cache!) ",
		578);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2174z00zzabound_walkz00,
		BgL_bgl_za762za7c3za704anonymo2221za7,
		BGl_z62zc3z04anonymousza31998ze3ze5zzabound_walkz00);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2177z00zzabound_walkz00,
		BgL_bgl_za762za7c3za704anonymo2222za7,
		BGl_z62zc3z04anonymousza31871ze3ze5zzabound_walkz00);
	      DEFINE_BGL_L_PROCEDURE(BGl_proc2178z00zzabound_walkz00,
		BgL_bgl_za762za7c3za704anonymo2223za7,
		BGl_z62zc3z04anonymousza31894ze3ze5zzabound_walkz00);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_aboundzd2walkz12zd2envz12zzabound_walkz00,
		BgL_bgl_za762aboundza7d2walk2224z00,
		BGl_z62aboundzd2walkz12za2zzabound_walkz00, 0L, BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_PROCEDURE(BGl_clearzd2cachez12zd2envz12zzabound_walkz00,
		BgL_bgl_za762clearza7d2cache2225z00,
		BGl_z62clearzd2cachez12za2zzabound_walkz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_GENERIC(BGl_aboundzd2nodezd2envz00zzabound_walkz00,
		BgL_bgl_za762aboundza7d2node2226z00,
		BGl_z62aboundzd2nodezb0zzabound_walkz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2s16vectorzd2setz12za2zc0zzabound_walkz00));
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2s32vectorzd2refza2zd2zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2u64vectorzd2setz12za2zc0zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2stringzd2refza2zd2zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2u32vectorzd2setz12za2zc0zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2s64vectorzd2setz12za2zc0zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2s32vectorzd2setz12za2zc0zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2u16vectorzd2refza2zd2zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2u8vectorzd2setz12za2zc0zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2f64vectorzd2setz12za2zc0zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2f32vectorzd2setz12za2zc0zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2f64vectorzd2refza2zd2zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2u8vectorzd2refza2zd2zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2s16vectorzd2refza2zd2zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2s8vectorzd2setz12za2zc0zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2u64vectorzd2refza2zd2zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2stringzd2setz12za2zc0zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2structzd2refza2zd2zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2structzd2setz12za2zc0zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2f32vectorzd2refza2zd2zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2u16vectorzd2setz12za2zc0zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2u32vectorzd2refza2zd2zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2s64vectorzd2refza2zd2zzabound_walkz00));
		     ADD_ROOT((void *) (&BGl_za2s8vectorzd2refza2zd2zzabound_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzabound_walkz00(long
		BgL_checksumz00_2859, char *BgL_fromz00_2860)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzabound_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzabound_walkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzabound_walkz00();
					BGl_libraryzd2moduleszd2initz00zzabound_walkz00();
					BGl_cnstzd2initzd2zzabound_walkz00();
					BGl_importedzd2moduleszd2initz00zzabound_walkz00();
					BGl_genericzd2initzd2zzabound_walkz00();
					BGl_methodzd2initzd2zzabound_walkz00();
					return BGl_toplevelzd2initzd2zzabound_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzabound_walkz00(void)
	{
		{	/* Abound/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"abound_walk");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "abound_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"abound_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"abound_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "abound_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "abound_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "abound_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "abound_walk");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "abound_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "abound_walk");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "abound_walk");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "abound_walk");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "abound_walk");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "abound_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzabound_walkz00(void)
	{
		{	/* Abound/walk.scm 15 */
			{	/* Abound/walk.scm 15 */
				obj_t BgL_cportz00_2602;

				{	/* Abound/walk.scm 15 */
					obj_t BgL_stringz00_2609;

					BgL_stringz00_2609 = BGl_string2180z00zzabound_walkz00;
					{	/* Abound/walk.scm 15 */
						obj_t BgL_startz00_2610;

						BgL_startz00_2610 = BINT(0L);
						{	/* Abound/walk.scm 15 */
							obj_t BgL_endz00_2611;

							BgL_endz00_2611 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2609)));
							{	/* Abound/walk.scm 15 */

								BgL_cportz00_2602 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2609, BgL_startz00_2610, BgL_endz00_2611);
				}}}}
				{
					long BgL_iz00_2603;

					BgL_iz00_2603 = 44L;
				BgL_loopz00_2604:
					if ((BgL_iz00_2603 == -1L))
						{	/* Abound/walk.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Abound/walk.scm 15 */
							{	/* Abound/walk.scm 15 */
								obj_t BgL_arg2181z00_2605;

								{	/* Abound/walk.scm 15 */

									{	/* Abound/walk.scm 15 */
										obj_t BgL_locationz00_2607;

										BgL_locationz00_2607 = BBOOL(((bool_t) 0));
										{	/* Abound/walk.scm 15 */

											BgL_arg2181z00_2605 =
												BGl_readz00zz__readerz00(BgL_cportz00_2602,
												BgL_locationz00_2607);
										}
									}
								}
								{	/* Abound/walk.scm 15 */
									int BgL_tmpz00_2894;

									BgL_tmpz00_2894 = (int) (BgL_iz00_2603);
									CNST_TABLE_SET(BgL_tmpz00_2894, BgL_arg2181z00_2605);
							}}
							{	/* Abound/walk.scm 15 */
								int BgL_auxz00_2608;

								BgL_auxz00_2608 = (int) ((BgL_iz00_2603 - 1L));
								{
									long BgL_iz00_2899;

									BgL_iz00_2899 = (long) (BgL_auxz00_2608);
									BgL_iz00_2603 = BgL_iz00_2899;
									goto BgL_loopz00_2604;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzabound_walkz00(void)
	{
		{	/* Abound/walk.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzabound_walkz00(void)
	{
		{	/* Abound/walk.scm 15 */
			BGl_za2stringzd2refza2zd2zzabound_walkz00 = BUNSPEC;
			BGl_za2stringzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
			BGl_za2structzd2refza2zd2zzabound_walkz00 = BUNSPEC;
			BGl_za2structzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
			BGl_za2s8vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
			BGl_za2u8vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
			BGl_za2s16vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
			BGl_za2u16vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
			BGl_za2s32vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
			BGl_za2u32vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
			BGl_za2s64vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
			BGl_za2u64vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
			BGl_za2f32vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
			BGl_za2f64vectorzd2refza2zd2zzabound_walkz00 = BUNSPEC;
			BGl_za2s8vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
			BGl_za2u8vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
			BGl_za2s16vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
			BGl_za2u16vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
			BGl_za2s32vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
			BGl_za2u32vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
			BGl_za2s64vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
			BGl_za2u64vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
			BGl_za2f32vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
			BGl_za2f64vectorzd2setz12za2zc0zzabound_walkz00 = BUNSPEC;
			return BUNSPEC;
		}

	}



/* abound-walk! */
	BGL_EXPORTED_DEF obj_t BGl_aboundzd2walkz12zc0zzabound_walkz00(obj_t
		BgL_globalsz00_17)
	{
		{	/* Abound/walk.scm 39 */
			{	/* Abound/walk.scm 40 */
				obj_t BgL_list1376z00_1515;

				{	/* Abound/walk.scm 40 */
					obj_t BgL_arg1377z00_1516;

					{	/* Abound/walk.scm 40 */
						obj_t BgL_arg1378z00_1517;

						BgL_arg1378z00_1517 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1377z00_1516 =
							MAKE_YOUNG_PAIR(BGl_string2140z00zzabound_walkz00,
							BgL_arg1378z00_1517);
					}
					BgL_list1376z00_1515 =
						MAKE_YOUNG_PAIR(BGl_string2141z00zzabound_walkz00,
						BgL_arg1377z00_1516);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1376z00_1515);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string2140z00zzabound_walkz00;
			{	/* Abound/walk.scm 40 */
				obj_t BgL_g1107z00_1518;
				obj_t BgL_g1108z00_1519;

				{	/* Abound/walk.scm 40 */
					obj_t BgL_list1424z00_1532;

					BgL_list1424z00_1532 =
						MAKE_YOUNG_PAIR(BGl_initzd2cachez12zd2envz12zzabound_walkz00, BNIL);
					BgL_g1107z00_1518 = BgL_list1424z00_1532;
				}
				BgL_g1108z00_1519 = CNST_TABLE_REF(0);
				{
					obj_t BgL_hooksz00_1521;
					obj_t BgL_hnamesz00_1522;

					BgL_hooksz00_1521 = BgL_g1107z00_1518;
					BgL_hnamesz00_1522 = BgL_g1108z00_1519;
				BgL_zc3z04anonymousza31379ze3z87_1523:
					if (NULLP(BgL_hooksz00_1521))
						{	/* Abound/walk.scm 40 */
							CNST_TABLE_REF(1);
						}
					else
						{	/* Abound/walk.scm 40 */
							bool_t BgL_test2230z00_2914;

							{	/* Abound/walk.scm 40 */
								obj_t BgL_fun1423z00_1530;

								BgL_fun1423z00_1530 = CAR(((obj_t) BgL_hooksz00_1521));
								BgL_test2230z00_2914 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1423z00_1530));
							}
							if (BgL_test2230z00_2914)
								{	/* Abound/walk.scm 40 */
									obj_t BgL_arg1408z00_1527;
									obj_t BgL_arg1410z00_1528;

									BgL_arg1408z00_1527 = CDR(((obj_t) BgL_hooksz00_1521));
									BgL_arg1410z00_1528 = CDR(((obj_t) BgL_hnamesz00_1522));
									{
										obj_t BgL_hnamesz00_2926;
										obj_t BgL_hooksz00_2925;

										BgL_hooksz00_2925 = BgL_arg1408z00_1527;
										BgL_hnamesz00_2926 = BgL_arg1410z00_1528;
										BgL_hnamesz00_1522 = BgL_hnamesz00_2926;
										BgL_hooksz00_1521 = BgL_hooksz00_2925;
										goto BgL_zc3z04anonymousza31379ze3z87_1523;
									}
								}
							else
								{	/* Abound/walk.scm 40 */
									obj_t BgL_arg1421z00_1529;

									BgL_arg1421z00_1529 = CAR(((obj_t) BgL_hnamesz00_1522));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string2140z00zzabound_walkz00,
										BGl_string2142z00zzabound_walkz00, BgL_arg1421z00_1529);
								}
						}
				}
			}
			{
				obj_t BgL_l1295z00_1534;

				BgL_l1295z00_1534 = BgL_globalsz00_17;
			BgL_zc3z04anonymousza31425ze3z87_1535:
				if (PAIRP(BgL_l1295z00_1534))
					{	/* Abound/walk.scm 41 */
						BGl_aboundzd2funz12zc0zzabound_walkz00(CAR(BgL_l1295z00_1534));
						{
							obj_t BgL_l1295z00_2934;

							BgL_l1295z00_2934 = CDR(BgL_l1295z00_1534);
							BgL_l1295z00_1534 = BgL_l1295z00_2934;
							goto BgL_zc3z04anonymousza31425ze3z87_1535;
						}
					}
				else
					{	/* Abound/walk.scm 41 */
						((bool_t) 1);
					}
			}
			if (
				((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
				{	/* Abound/walk.scm 42 */
					{	/* Abound/walk.scm 42 */
						obj_t BgL_port1297z00_1542;

						{	/* Abound/walk.scm 42 */
							obj_t BgL_tmpz00_2939;

							BgL_tmpz00_2939 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1297z00_1542 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2939);
						}
						bgl_display_obj(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
							BgL_port1297z00_1542);
						bgl_display_string(BGl_string2143z00zzabound_walkz00,
							BgL_port1297z00_1542);
						{	/* Abound/walk.scm 42 */
							obj_t BgL_arg1448z00_1543;

							{	/* Abound/walk.scm 42 */
								bool_t BgL_test2233z00_2944;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
									{	/* Abound/walk.scm 42 */
										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Abound/walk.scm 42 */
												BgL_test2233z00_2944 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													> 1L);
											}
										else
											{	/* Abound/walk.scm 42 */
												BgL_test2233z00_2944 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(1L));
											}
									}
								else
									{	/* Abound/walk.scm 42 */
										BgL_test2233z00_2944 = ((bool_t) 0);
									}
								if (BgL_test2233z00_2944)
									{	/* Abound/walk.scm 42 */
										BgL_arg1448z00_1543 = BGl_string2144z00zzabound_walkz00;
									}
								else
									{	/* Abound/walk.scm 42 */
										BgL_arg1448z00_1543 = BGl_string2145z00zzabound_walkz00;
									}
							}
							bgl_display_obj(BgL_arg1448z00_1543, BgL_port1297z00_1542);
						}
						bgl_display_string(BGl_string2146z00zzabound_walkz00,
							BgL_port1297z00_1542);
						bgl_display_char(((unsigned char) 10), BgL_port1297z00_1542);
					}
					{	/* Abound/walk.scm 42 */
						obj_t BgL_list1451z00_1547;

						BgL_list1451z00_1547 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
						BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1451z00_1547);
					}
				}
			else
				{	/* Abound/walk.scm 42 */
					obj_t BgL_g1109z00_1548;
					obj_t BgL_g1110z00_1549;

					{	/* Abound/walk.scm 42 */
						obj_t BgL_list1488z00_1562;

						BgL_list1488z00_1562 =
							MAKE_YOUNG_PAIR(BGl_clearzd2cachez12zd2envz12zzabound_walkz00,
							BNIL);
						BgL_g1109z00_1548 = BgL_list1488z00_1562;
					}
					BgL_g1110z00_1549 = CNST_TABLE_REF(2);
					{
						obj_t BgL_hooksz00_1551;
						obj_t BgL_hnamesz00_1552;

						BgL_hooksz00_1551 = BgL_g1109z00_1548;
						BgL_hnamesz00_1552 = BgL_g1110z00_1549;
					BgL_zc3z04anonymousza31452ze3z87_1553:
						if (NULLP(BgL_hooksz00_1551))
							{	/* Abound/walk.scm 42 */
								return BgL_globalsz00_17;
							}
						else
							{	/* Abound/walk.scm 42 */
								bool_t BgL_test2237z00_2963;

								{	/* Abound/walk.scm 42 */
									obj_t BgL_fun1487z00_1560;

									BgL_fun1487z00_1560 = CAR(((obj_t) BgL_hooksz00_1551));
									BgL_test2237z00_2963 =
										CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1487z00_1560));
								}
								if (BgL_test2237z00_2963)
									{	/* Abound/walk.scm 42 */
										obj_t BgL_arg1472z00_1557;
										obj_t BgL_arg1473z00_1558;

										BgL_arg1472z00_1557 = CDR(((obj_t) BgL_hooksz00_1551));
										BgL_arg1473z00_1558 = CDR(((obj_t) BgL_hnamesz00_1552));
										{
											obj_t BgL_hnamesz00_2975;
											obj_t BgL_hooksz00_2974;

											BgL_hooksz00_2974 = BgL_arg1472z00_1557;
											BgL_hnamesz00_2975 = BgL_arg1473z00_1558;
											BgL_hnamesz00_1552 = BgL_hnamesz00_2975;
											BgL_hooksz00_1551 = BgL_hooksz00_2974;
											goto BgL_zc3z04anonymousza31452ze3z87_1553;
										}
									}
								else
									{	/* Abound/walk.scm 42 */
										obj_t BgL_arg1485z00_1559;

										BgL_arg1485z00_1559 = CAR(((obj_t) BgL_hnamesz00_1552));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_za2currentzd2passza2zd2zzengine_passz00,
											BGl_string2147z00zzabound_walkz00, BgL_arg1485z00_1559);
									}
							}
					}
				}
		}

	}



/* &abound-walk! */
	obj_t BGl_z62aboundzd2walkz12za2zzabound_walkz00(obj_t BgL_envz00_2512,
		obj_t BgL_globalsz00_2513)
	{
		{	/* Abound/walk.scm 39 */
			return BGl_aboundzd2walkz12zc0zzabound_walkz00(BgL_globalsz00_2513);
		}

	}



/* &init-cache! */
	obj_t BGl_z62initzd2cachez12za2zzabound_walkz00(obj_t BgL_envz00_2514)
	{
		{	/* Abound/walk.scm 75 */
			{	/* Abound/walk.scm 76 */
				obj_t BgL_list1489z00_2613;

				BgL_list1489z00_2613 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2stringzd2refza2zd2zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(4),
					BgL_list1489z00_2613);
			}
			{	/* Abound/walk.scm 77 */
				obj_t BgL_list1490z00_2614;

				BgL_list1490z00_2614 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2stringzd2setz12za2zc0zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(5),
					BgL_list1490z00_2614);
			}
			{	/* Abound/walk.scm 78 */
				obj_t BgL_list1491z00_2615;

				BgL_list1491z00_2615 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2structzd2refza2zd2zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(6),
					BgL_list1491z00_2615);
			}
			{	/* Abound/walk.scm 79 */
				obj_t BgL_list1492z00_2616;

				BgL_list1492z00_2616 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2structzd2setz12za2zc0zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(7),
					BgL_list1492z00_2616);
			}
			{	/* Abound/walk.scm 80 */
				obj_t BgL_list1493z00_2617;

				BgL_list1493z00_2617 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2s8vectorzd2refza2zd2zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(8),
					BgL_list1493z00_2617);
			}
			{	/* Abound/walk.scm 81 */
				obj_t BgL_list1494z00_2618;

				BgL_list1494z00_2618 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2u8vectorzd2refza2zd2zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(9),
					BgL_list1494z00_2618);
			}
			{	/* Abound/walk.scm 82 */
				obj_t BgL_list1495z00_2619;

				BgL_list1495z00_2619 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2s16vectorzd2refza2zd2zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(10),
					BgL_list1495z00_2619);
			}
			{	/* Abound/walk.scm 83 */
				obj_t BgL_list1496z00_2620;

				BgL_list1496z00_2620 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2u16vectorzd2refza2zd2zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(11),
					BgL_list1496z00_2620);
			}
			{	/* Abound/walk.scm 84 */
				obj_t BgL_list1497z00_2621;

				BgL_list1497z00_2621 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2s32vectorzd2refza2zd2zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(12),
					BgL_list1497z00_2621);
			}
			{	/* Abound/walk.scm 85 */
				obj_t BgL_list1498z00_2622;

				BgL_list1498z00_2622 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2u32vectorzd2refza2zd2zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(13),
					BgL_list1498z00_2622);
			}
			{	/* Abound/walk.scm 86 */
				obj_t BgL_list1499z00_2623;

				BgL_list1499z00_2623 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2s64vectorzd2refza2zd2zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(14),
					BgL_list1499z00_2623);
			}
			{	/* Abound/walk.scm 87 */
				obj_t BgL_list1500z00_2624;

				BgL_list1500z00_2624 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2u64vectorzd2refza2zd2zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(15),
					BgL_list1500z00_2624);
			}
			{	/* Abound/walk.scm 88 */
				obj_t BgL_list1501z00_2625;

				BgL_list1501z00_2625 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2f32vectorzd2refza2zd2zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(16),
					BgL_list1501z00_2625);
			}
			{	/* Abound/walk.scm 89 */
				obj_t BgL_list1502z00_2626;

				BgL_list1502z00_2626 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2f64vectorzd2refza2zd2zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(17),
					BgL_list1502z00_2626);
			}
			{	/* Abound/walk.scm 90 */
				obj_t BgL_list1503z00_2627;

				BgL_list1503z00_2627 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2s8vectorzd2setz12za2zc0zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(18),
					BgL_list1503z00_2627);
			}
			{	/* Abound/walk.scm 91 */
				obj_t BgL_list1504z00_2628;

				BgL_list1504z00_2628 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2u8vectorzd2setz12za2zc0zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(19),
					BgL_list1504z00_2628);
			}
			{	/* Abound/walk.scm 92 */
				obj_t BgL_list1505z00_2629;

				BgL_list1505z00_2629 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2s16vectorzd2setz12za2zc0zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(20),
					BgL_list1505z00_2629);
			}
			{	/* Abound/walk.scm 93 */
				obj_t BgL_list1506z00_2630;

				BgL_list1506z00_2630 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2u16vectorzd2setz12za2zc0zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(21),
					BgL_list1506z00_2630);
			}
			{	/* Abound/walk.scm 94 */
				obj_t BgL_list1507z00_2631;

				BgL_list1507z00_2631 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2s32vectorzd2setz12za2zc0zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(22),
					BgL_list1507z00_2631);
			}
			{	/* Abound/walk.scm 95 */
				obj_t BgL_list1508z00_2632;

				BgL_list1508z00_2632 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2u32vectorzd2setz12za2zc0zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(23),
					BgL_list1508z00_2632);
			}
			{	/* Abound/walk.scm 96 */
				obj_t BgL_list1509z00_2633;

				BgL_list1509z00_2633 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2s64vectorzd2setz12za2zc0zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(24),
					BgL_list1509z00_2633);
			}
			{	/* Abound/walk.scm 97 */
				obj_t BgL_list1510z00_2634;

				BgL_list1510z00_2634 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2u64vectorzd2setz12za2zc0zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(25),
					BgL_list1510z00_2634);
			}
			{	/* Abound/walk.scm 98 */
				obj_t BgL_list1511z00_2635;

				BgL_list1511z00_2635 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2f32vectorzd2setz12za2zc0zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(26),
					BgL_list1511z00_2635);
			}
			{	/* Abound/walk.scm 99 */
				obj_t BgL_list1512z00_2636;

				BgL_list1512z00_2636 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(3), BNIL);
				BGl_za2f64vectorzd2setz12za2zc0zzabound_walkz00 =
					BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(27),
					BgL_list1512z00_2636);
			}
			return BUNSPEC;
		}

	}



/* &clear-cache! */
	obj_t BGl_z62clearzd2cachez12za2zzabound_walkz00(obj_t BgL_envz00_2515)
	{
		{	/* Abound/walk.scm 105 */
			return (BGl_za2stringzd2refza2zd2zzabound_walkz00 = BFALSE, BUNSPEC);
		}

	}



/* abound-fun! */
	obj_t BGl_aboundzd2funz12zc0zzabound_walkz00(obj_t BgL_varz00_18)
	{
		{	/* Abound/walk.scm 111 */
			{	/* Abound/walk.scm 112 */
				obj_t BgL_arg1513z00_1587;

				BgL_arg1513z00_1587 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_18)))->BgL_idz00);
				BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1513z00_1587);
			}
			{	/* Abound/walk.scm 113 */
				BgL_valuez00_bglt BgL_funz00_1588;

				BgL_funz00_1588 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_18)))->BgL_valuez00);
				{	/* Abound/walk.scm 114 */
					BgL_nodez00_bglt BgL_arg1514z00_1589;

					{	/* Abound/walk.scm 114 */
						obj_t BgL_arg1516z00_1590;

						BgL_arg1516z00_1590 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_1588)))->BgL_bodyz00);
						BgL_arg1514z00_1589 =
							BGl_aboundzd2nodezd2zzabound_walkz00(
							((BgL_nodez00_bglt) BgL_arg1516z00_1590));
					}
					((((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_1588)))->BgL_bodyz00) =
						((obj_t) ((obj_t) BgL_arg1514z00_1589)), BUNSPEC);
				}
				BGl_leavezd2functionzd2zztools_errorz00();
				return BgL_varz00_18;
			}
		}

	}



/* array-ref */
	BgL_nodez00_bglt BGl_arrayzd2refzd2zzabound_walkz00(BgL_nodezf2effectzf2_bglt
		BgL_nodez00_27, obj_t BgL_vecz00_28, obj_t BgL_offz00_29,
		obj_t BgL_locz00_30, BgL_typez00_bglt BgL_ftypez00_31,
		obj_t BgL_otypez00_32, obj_t BgL_vtypez00_33, obj_t BgL_dupz00_34)
	{
		{	/* Abound/walk.scm 266 */
			{	/* Abound/walk.scm 267 */
				obj_t BgL_vz00_1591;
				obj_t BgL_iz00_1592;
				obj_t BgL_lz00_1593;
				obj_t BgL_lnamez00_1594;
				obj_t BgL_lposz00_1595;

				BgL_vz00_1591 =
					BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
					(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(28)));
				BgL_iz00_1592 =
					BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
					(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(29)));
				BgL_lz00_1593 =
					BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
					(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(30)));
				{	/* Abound/walk.scm 270 */
					bool_t BgL_test2238z00_3098;

					if (STRUCTP(BgL_locz00_30))
						{	/* Abound/walk.scm 270 */
							BgL_test2238z00_3098 =
								(STRUCT_KEY(BgL_locz00_30) == CNST_TABLE_REF(31));
						}
					else
						{	/* Abound/walk.scm 270 */
							BgL_test2238z00_3098 = ((bool_t) 0);
						}
					if (BgL_test2238z00_3098)
						{	/* Abound/walk.scm 270 */
							BgL_lnamez00_1594 =
								BGl_locationzd2fullzd2fnamez00zztools_locationz00
								(BgL_locz00_30);
						}
					else
						{	/* Abound/walk.scm 270 */
							BgL_lnamez00_1594 = BFALSE;
						}
				}
				{	/* Abound/walk.scm 271 */
					bool_t BgL_test2240z00_3105;

					if (STRUCTP(BgL_locz00_30))
						{	/* Abound/walk.scm 271 */
							BgL_test2240z00_3105 =
								(STRUCT_KEY(BgL_locz00_30) == CNST_TABLE_REF(31));
						}
					else
						{	/* Abound/walk.scm 271 */
							BgL_test2240z00_3105 = ((bool_t) 0);
						}
					if (BgL_test2240z00_3105)
						{	/* Abound/walk.scm 271 */
							BgL_lposz00_1595 = STRUCT_REF(BgL_locz00_30, (int) (1L));
						}
					else
						{	/* Abound/walk.scm 271 */
							BgL_lposz00_1595 = BFALSE;
						}
				}
				{	/* Abound/walk.scm 273 */
					obj_t BgL_arg1535z00_1596;

					{	/* Abound/walk.scm 273 */
						obj_t BgL_arg1540z00_1597;

						{	/* Abound/walk.scm 273 */
							obj_t BgL_arg1544z00_1598;
							obj_t BgL_arg1546z00_1599;

							{	/* Abound/walk.scm 273 */
								obj_t BgL_arg1552z00_1600;
								obj_t BgL_arg1553z00_1601;

								{	/* Abound/walk.scm 273 */
									obj_t BgL_arg1559z00_1602;
									obj_t BgL_arg1561z00_1603;

									{	/* Abound/walk.scm 273 */
										obj_t BgL_arg1564z00_1604;

										BgL_arg1564z00_1604 =
											(((BgL_typez00_bglt) COBJECT(
													((BgL_typez00_bglt) BgL_vtypez00_33)))->BgL_idz00);
										BgL_arg1559z00_1602 =
											BGl_makezd2typedzd2identz00zzast_identz00(BgL_vz00_1591,
											BgL_arg1564z00_1604);
									}
									BgL_arg1561z00_1603 = MAKE_YOUNG_PAIR(BgL_vecz00_28, BNIL);
									BgL_arg1552z00_1600 =
										MAKE_YOUNG_PAIR(BgL_arg1559z00_1602, BgL_arg1561z00_1603);
								}
								{	/* Abound/walk.scm 274 */
									obj_t BgL_arg1565z00_1605;

									{	/* Abound/walk.scm 274 */
										obj_t BgL_arg1571z00_1606;
										obj_t BgL_arg1573z00_1607;

										{	/* Abound/walk.scm 274 */
											obj_t BgL_arg1575z00_1608;

											BgL_arg1575z00_1608 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt) BgL_otypez00_32)))->BgL_idz00);
											BgL_arg1571z00_1606 =
												BGl_makezd2typedzd2identz00zzast_identz00(BgL_iz00_1592,
												BgL_arg1575z00_1608);
										}
										BgL_arg1573z00_1607 = MAKE_YOUNG_PAIR(BgL_offz00_29, BNIL);
										BgL_arg1565z00_1605 =
											MAKE_YOUNG_PAIR(BgL_arg1571z00_1606, BgL_arg1573z00_1607);
									}
									BgL_arg1553z00_1601 =
										MAKE_YOUNG_PAIR(BgL_arg1565z00_1605, BNIL);
								}
								BgL_arg1544z00_1598 =
									MAKE_YOUNG_PAIR(BgL_arg1552z00_1600, BgL_arg1553z00_1601);
							}
							{	/* Abound/walk.scm 275 */
								obj_t BgL_arg1576z00_1609;

								{	/* Abound/walk.scm 275 */
									obj_t BgL_arg1584z00_1610;

									{	/* Abound/walk.scm 275 */
										obj_t BgL_arg1585z00_1611;
										obj_t BgL_arg1589z00_1612;

										{	/* Abound/walk.scm 275 */
											obj_t BgL_arg1591z00_1613;

											{	/* Abound/walk.scm 275 */
												obj_t BgL_arg1593z00_1614;
												obj_t BgL_arg1594z00_1615;

												{	/* Abound/walk.scm 275 */
													obj_t BgL_arg1595z00_1616;

													BgL_arg1595z00_1616 =
														(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt) BgL_otypez00_32)))->
														BgL_idz00);
													BgL_arg1593z00_1614 =
														BGl_makezd2typedzd2identz00zzast_identz00
														(BgL_lz00_1593, BgL_arg1595z00_1616);
												}
												{	/* Abound/walk.scm 277 */
													obj_t BgL_arg1602z00_1617;

													if (
														(BgL_vtypez00_33 ==
															BGl_za2vectorza2z00zztype_cachez00))
														{	/* Abound/walk.scm 278 */
															obj_t BgL_arg1605z00_1618;

															BgL_arg1605z00_1618 =
																MAKE_YOUNG_PAIR(BgL_vz00_1591, BNIL);
															BgL_arg1602z00_1617 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(32),
																BgL_arg1605z00_1618);
														}
													else
														{	/* Abound/walk.scm 277 */
															if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																	(BgL_vtypez00_33,
																		BGl_za2hvectorsza2z00zztype_cachez00)))
																{	/* Abound/walk.scm 280 */
																	obj_t BgL_arg1609z00_1620;

																	BgL_arg1609z00_1620 =
																		MAKE_YOUNG_PAIR(BgL_vz00_1591, BNIL);
																	BgL_arg1602z00_1617 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(33),
																		BgL_arg1609z00_1620);
																}
															else
																{	/* Abound/walk.scm 282 */
																	obj_t BgL_arg1611z00_1621;

																	BgL_arg1611z00_1621 =
																		MAKE_YOUNG_PAIR(BgL_vz00_1591, BNIL);
																	BgL_arg1602z00_1617 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(34),
																		BgL_arg1611z00_1621);
																}
														}
													BgL_arg1594z00_1615 =
														MAKE_YOUNG_PAIR(BgL_arg1602z00_1617, BNIL);
												}
												BgL_arg1591z00_1613 =
													MAKE_YOUNG_PAIR(BgL_arg1593z00_1614,
													BgL_arg1594z00_1615);
											}
											BgL_arg1585z00_1611 =
												MAKE_YOUNG_PAIR(BgL_arg1591z00_1613, BNIL);
										}
										{	/* Abound/walk.scm 283 */
											obj_t BgL_arg1613z00_1622;

											{	/* Abound/walk.scm 283 */
												obj_t BgL_arg1615z00_1623;

												{	/* Abound/walk.scm 283 */
													obj_t BgL_arg1616z00_1624;
													obj_t BgL_arg1625z00_1625;

													{	/* Abound/walk.scm 283 */
														obj_t BgL_arg1626z00_1626;

														{	/* Abound/walk.scm 283 */
															obj_t BgL_arg1627z00_1627;

															BgL_arg1627z00_1627 =
																MAKE_YOUNG_PAIR(BgL_lz00_1593, BNIL);
															BgL_arg1626z00_1626 =
																MAKE_YOUNG_PAIR(BgL_iz00_1592,
																BgL_arg1627z00_1627);
														}
														BgL_arg1616z00_1624 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(35),
															BgL_arg1626z00_1626);
													}
													{	/* Abound/walk.scm 284 */
														BgL_nodezf2effectzf2_bglt BgL_arg1629z00_1628;
														obj_t BgL_arg1630z00_1629;

														BgL_arg1629z00_1628 =
															((BgL_nodezf2effectzf2_bglt(*)(obj_t,
																	BgL_nodezf2effectzf2_bglt, obj_t,
																	obj_t))
															PROCEDURE_L_ENTRY(BgL_dupz00_34)) (BgL_dupz00_34,
															BgL_nodez00_27, BgL_vz00_1591, BgL_iz00_1592);
														{	/* Abound/walk.scm 286 */
															obj_t BgL_arg1642z00_1630;

															{	/* Abound/walk.scm 286 */
																obj_t BgL_arg1646z00_1631;

																{	/* Abound/walk.scm 286 */
																	obj_t BgL_arg1650z00_1632;
																	obj_t BgL_arg1651z00_1633;

																	{	/* Abound/walk.scm 286 */
																		obj_t BgL_arg1654z00_1634;
																		obj_t BgL_arg1661z00_1635;

																		{	/* Abound/walk.scm 286 */
																			obj_t BgL_arg1663z00_1636;

																			{	/* Abound/walk.scm 286 */
																				obj_t BgL_arg1675z00_1637;

																				BgL_arg1675z00_1637 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(36),
																					BNIL);
																				BgL_arg1663z00_1636 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(37),
																					BgL_arg1675z00_1637);
																			}
																			BgL_arg1654z00_1634 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(38),
																				BgL_arg1663z00_1636);
																		}
																		{	/* Abound/walk.scm 289 */
																			obj_t BgL_arg1678z00_1638;

																			{	/* Abound/walk.scm 289 */
																				obj_t BgL_arg1681z00_1639;

																				{	/* Abound/walk.scm 289 */
																					obj_t BgL_arg1688z00_1640;
																					obj_t BgL_arg1689z00_1641;

																					{	/* Abound/walk.scm 289 */
																						obj_t BgL_arg1691z00_1642;

																						{	/* Abound/walk.scm 289 */
																							obj_t BgL_arg1692z00_1643;

																							BgL_arg1692z00_1643 =
																								(((BgL_typez00_bglt) COBJECT(
																										((BgL_typez00_bglt)
																											BgL_vtypez00_33)))->
																								BgL_idz00);
																							BgL_arg1691z00_1642 =
																								SYMBOL_TO_STRING
																								(BgL_arg1692z00_1643);
																						}
																						BgL_arg1688z00_1640 =
																							string_append(BgL_arg1691z00_1642,
																							BGl_string2148z00zzabound_walkz00);
																					}
																					{	/* Abound/walk.scm 286 */
																						obj_t BgL_arg1699z00_1644;

																						{	/* Abound/walk.scm 286 */
																							obj_t BgL_arg1700z00_1645;

																							BgL_arg1700z00_1645 =
																								MAKE_YOUNG_PAIR(BgL_iz00_1592,
																								BNIL);
																							BgL_arg1699z00_1644 =
																								MAKE_YOUNG_PAIR(BgL_lz00_1593,
																								BgL_arg1700z00_1645);
																						}
																						BgL_arg1689z00_1641 =
																							MAKE_YOUNG_PAIR(BgL_vz00_1591,
																							BgL_arg1699z00_1644);
																					}
																					BgL_arg1681z00_1639 =
																						MAKE_YOUNG_PAIR(BgL_arg1688z00_1640,
																						BgL_arg1689z00_1641);
																				}
																				BgL_arg1678z00_1638 =
																					MAKE_YOUNG_PAIR(BgL_lposz00_1595,
																					BgL_arg1681z00_1639);
																			}
																			BgL_arg1661z00_1635 =
																				MAKE_YOUNG_PAIR(BgL_lnamez00_1594,
																				BgL_arg1678z00_1638);
																		}
																		BgL_arg1650z00_1632 =
																			MAKE_YOUNG_PAIR(BgL_arg1654z00_1634,
																			BgL_arg1661z00_1635);
																	}
																	{	/* Abound/walk.scm 285 */
																		obj_t BgL_arg1701z00_1646;

																		BgL_arg1701z00_1646 =
																			MAKE_YOUNG_PAIR(BFALSE, BNIL);
																		BgL_arg1651z00_1633 =
																			MAKE_YOUNG_PAIR(BFALSE,
																			BgL_arg1701z00_1646);
																	}
																	BgL_arg1646z00_1631 =
																		MAKE_YOUNG_PAIR(BgL_arg1650z00_1632,
																		BgL_arg1651z00_1633);
																}
																BgL_arg1642z00_1630 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(39),
																	BgL_arg1646z00_1631);
															}
															BgL_arg1630z00_1629 =
																MAKE_YOUNG_PAIR(BgL_arg1642z00_1630, BNIL);
														}
														BgL_arg1625z00_1625 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg1629z00_1628),
															BgL_arg1630z00_1629);
													}
													BgL_arg1615z00_1623 =
														MAKE_YOUNG_PAIR(BgL_arg1616z00_1624,
														BgL_arg1625z00_1625);
												}
												BgL_arg1613z00_1622 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(40),
													BgL_arg1615z00_1623);
											}
											BgL_arg1589z00_1612 =
												MAKE_YOUNG_PAIR(BgL_arg1613z00_1622, BNIL);
										}
										BgL_arg1584z00_1610 =
											MAKE_YOUNG_PAIR(BgL_arg1585z00_1611, BgL_arg1589z00_1612);
									}
									BgL_arg1576z00_1609 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(41), BgL_arg1584z00_1610);
								}
								BgL_arg1546z00_1599 =
									MAKE_YOUNG_PAIR(BgL_arg1576z00_1609, BNIL);
							}
							BgL_arg1540z00_1597 =
								MAKE_YOUNG_PAIR(BgL_arg1544z00_1598, BgL_arg1546z00_1599);
						}
						BgL_arg1535z00_1596 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(41), BgL_arg1540z00_1597);
					}
					return
						BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00
						(BgL_arg1535z00_1596, BgL_locz00_30);
				}
			}
		}

	}



/* array-set! */
	BgL_nodez00_bglt
		BGl_arrayzd2setz12zc0zzabound_walkz00(BgL_nodezf2effectzf2_bglt
		BgL_nodez00_36, obj_t BgL_vecz00_37, obj_t BgL_offz00_38,
		obj_t BgL_locz00_39, obj_t BgL_ftypez00_40, obj_t BgL_otypez00_41,
		obj_t BgL_vtypez00_42, obj_t BgL_dupz00_43)
	{
		{	/* Abound/walk.scm 318 */
			{	/* Abound/walk.scm 319 */
				obj_t BgL_vz00_1652;
				obj_t BgL_iz00_1653;
				obj_t BgL_lz00_1654;
				obj_t BgL_lnamez00_1655;
				obj_t BgL_lposz00_1656;

				BgL_vz00_1652 =
					BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
					(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(28)));
				BgL_iz00_1653 =
					BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
					(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(29)));
				BgL_lz00_1654 =
					BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
					(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(30)));
				{	/* Abound/walk.scm 322 */
					bool_t BgL_test2244z00_3201;

					if (STRUCTP(BgL_locz00_39))
						{	/* Abound/walk.scm 322 */
							BgL_test2244z00_3201 =
								(STRUCT_KEY(BgL_locz00_39) == CNST_TABLE_REF(31));
						}
					else
						{	/* Abound/walk.scm 322 */
							BgL_test2244z00_3201 = ((bool_t) 0);
						}
					if (BgL_test2244z00_3201)
						{	/* Abound/walk.scm 322 */
							BgL_lnamez00_1655 =
								BGl_locationzd2fullzd2fnamez00zztools_locationz00
								(BgL_locz00_39);
						}
					else
						{	/* Abound/walk.scm 322 */
							BgL_lnamez00_1655 = BFALSE;
						}
				}
				{	/* Abound/walk.scm 323 */
					bool_t BgL_test2246z00_3208;

					if (STRUCTP(BgL_locz00_39))
						{	/* Abound/walk.scm 323 */
							BgL_test2246z00_3208 =
								(STRUCT_KEY(BgL_locz00_39) == CNST_TABLE_REF(31));
						}
					else
						{	/* Abound/walk.scm 323 */
							BgL_test2246z00_3208 = ((bool_t) 0);
						}
					if (BgL_test2246z00_3208)
						{	/* Abound/walk.scm 323 */
							BgL_lposz00_1656 = STRUCT_REF(BgL_locz00_39, (int) (1L));
						}
					else
						{	/* Abound/walk.scm 323 */
							BgL_lposz00_1656 = BFALSE;
						}
				}
				{	/* Abound/walk.scm 325 */
					obj_t BgL_arg1708z00_1657;

					{	/* Abound/walk.scm 325 */
						obj_t BgL_arg1709z00_1658;

						{	/* Abound/walk.scm 325 */
							obj_t BgL_arg1710z00_1659;
							obj_t BgL_arg1711z00_1660;

							{	/* Abound/walk.scm 325 */
								obj_t BgL_arg1714z00_1661;
								obj_t BgL_arg1717z00_1662;

								{	/* Abound/walk.scm 325 */
									obj_t BgL_arg1718z00_1663;
									obj_t BgL_arg1720z00_1664;

									{	/* Abound/walk.scm 325 */
										obj_t BgL_arg1722z00_1665;

										BgL_arg1722z00_1665 =
											(((BgL_typez00_bglt) COBJECT(
													((BgL_typez00_bglt) BgL_vtypez00_42)))->BgL_idz00);
										BgL_arg1718z00_1663 =
											BGl_makezd2typedzd2identz00zzast_identz00(BgL_vz00_1652,
											BgL_arg1722z00_1665);
									}
									BgL_arg1720z00_1664 = MAKE_YOUNG_PAIR(BgL_vecz00_37, BNIL);
									BgL_arg1714z00_1661 =
										MAKE_YOUNG_PAIR(BgL_arg1718z00_1663, BgL_arg1720z00_1664);
								}
								{	/* Abound/walk.scm 326 */
									obj_t BgL_arg1724z00_1666;

									{	/* Abound/walk.scm 326 */
										obj_t BgL_arg1733z00_1667;
										obj_t BgL_arg1734z00_1668;

										{	/* Abound/walk.scm 326 */
											obj_t BgL_arg1735z00_1669;

											BgL_arg1735z00_1669 =
												(((BgL_typez00_bglt) COBJECT(
														((BgL_typez00_bglt) BgL_otypez00_41)))->BgL_idz00);
											BgL_arg1733z00_1667 =
												BGl_makezd2typedzd2identz00zzast_identz00(BgL_iz00_1653,
												BgL_arg1735z00_1669);
										}
										BgL_arg1734z00_1668 = MAKE_YOUNG_PAIR(BgL_offz00_38, BNIL);
										BgL_arg1724z00_1666 =
											MAKE_YOUNG_PAIR(BgL_arg1733z00_1667, BgL_arg1734z00_1668);
									}
									BgL_arg1717z00_1662 =
										MAKE_YOUNG_PAIR(BgL_arg1724z00_1666, BNIL);
								}
								BgL_arg1710z00_1659 =
									MAKE_YOUNG_PAIR(BgL_arg1714z00_1661, BgL_arg1717z00_1662);
							}
							{	/* Abound/walk.scm 327 */
								obj_t BgL_arg1736z00_1670;

								{	/* Abound/walk.scm 327 */
									obj_t BgL_arg1737z00_1671;

									{	/* Abound/walk.scm 327 */
										obj_t BgL_arg1738z00_1672;
										obj_t BgL_arg1739z00_1673;

										{	/* Abound/walk.scm 327 */
											obj_t BgL_arg1740z00_1674;

											{	/* Abound/walk.scm 327 */
												obj_t BgL_arg1746z00_1675;
												obj_t BgL_arg1747z00_1676;

												{	/* Abound/walk.scm 327 */
													obj_t BgL_arg1748z00_1677;

													BgL_arg1748z00_1677 =
														(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt) BgL_otypez00_41)))->
														BgL_idz00);
													BgL_arg1746z00_1675 =
														BGl_makezd2typedzd2identz00zzast_identz00
														(BgL_lz00_1654, BgL_arg1748z00_1677);
												}
												{	/* Abound/walk.scm 329 */
													obj_t BgL_arg1749z00_1678;

													if (
														(BgL_vtypez00_42 ==
															BGl_za2vectorza2z00zztype_cachez00))
														{	/* Abound/walk.scm 330 */
															obj_t BgL_arg1750z00_1679;

															BgL_arg1750z00_1679 =
																MAKE_YOUNG_PAIR(BgL_vz00_1652, BNIL);
															BgL_arg1749z00_1678 =
																MAKE_YOUNG_PAIR(CNST_TABLE_REF(32),
																BgL_arg1750z00_1679);
														}
													else
														{	/* Abound/walk.scm 329 */
															if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																	(BgL_vtypez00_42,
																		BGl_za2hvectorsza2z00zztype_cachez00)))
																{	/* Abound/walk.scm 332 */
																	obj_t BgL_arg1752z00_1681;

																	BgL_arg1752z00_1681 =
																		MAKE_YOUNG_PAIR(BgL_vz00_1652, BNIL);
																	BgL_arg1749z00_1678 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(33),
																		BgL_arg1752z00_1681);
																}
															else
																{	/* Abound/walk.scm 334 */
																	obj_t BgL_arg1753z00_1682;

																	BgL_arg1753z00_1682 =
																		MAKE_YOUNG_PAIR(BgL_vz00_1652, BNIL);
																	BgL_arg1749z00_1678 =
																		MAKE_YOUNG_PAIR(CNST_TABLE_REF(34),
																		BgL_arg1753z00_1682);
																}
														}
													BgL_arg1747z00_1676 =
														MAKE_YOUNG_PAIR(BgL_arg1749z00_1678, BNIL);
												}
												BgL_arg1740z00_1674 =
													MAKE_YOUNG_PAIR(BgL_arg1746z00_1675,
													BgL_arg1747z00_1676);
											}
											BgL_arg1738z00_1672 =
												MAKE_YOUNG_PAIR(BgL_arg1740z00_1674, BNIL);
										}
										{	/* Abound/walk.scm 335 */
											obj_t BgL_arg1754z00_1683;

											{	/* Abound/walk.scm 335 */
												obj_t BgL_arg1755z00_1684;

												{	/* Abound/walk.scm 335 */
													obj_t BgL_arg1761z00_1685;
													obj_t BgL_arg1762z00_1686;

													{	/* Abound/walk.scm 335 */
														obj_t BgL_arg1765z00_1687;

														{	/* Abound/walk.scm 335 */
															obj_t BgL_arg1767z00_1688;

															BgL_arg1767z00_1688 =
																MAKE_YOUNG_PAIR(BgL_lz00_1654, BNIL);
															BgL_arg1765z00_1687 =
																MAKE_YOUNG_PAIR(BgL_iz00_1653,
																BgL_arg1767z00_1688);
														}
														BgL_arg1761z00_1685 =
															MAKE_YOUNG_PAIR(CNST_TABLE_REF(35),
															BgL_arg1765z00_1687);
													}
													{	/* Abound/walk.scm 336 */
														BgL_nodezf2effectzf2_bglt BgL_arg1770z00_1689;
														obj_t BgL_arg1771z00_1690;

														BgL_arg1770z00_1689 =
															((BgL_nodezf2effectzf2_bglt(*)(obj_t,
																	BgL_nodezf2effectzf2_bglt, obj_t,
																	obj_t))
															PROCEDURE_L_ENTRY(BgL_dupz00_43)) (BgL_dupz00_43,
															BgL_nodez00_36, BgL_vz00_1652, BgL_iz00_1653);
														{	/* Abound/walk.scm 338 */
															obj_t BgL_arg1773z00_1691;

															{	/* Abound/walk.scm 338 */
																obj_t BgL_arg1775z00_1692;

																{	/* Abound/walk.scm 338 */
																	obj_t BgL_arg1798z00_1693;
																	obj_t BgL_arg1799z00_1694;

																	{	/* Abound/walk.scm 338 */
																		obj_t BgL_arg1805z00_1695;
																		obj_t BgL_arg1806z00_1696;

																		{	/* Abound/walk.scm 338 */
																			obj_t BgL_arg1808z00_1697;

																			{	/* Abound/walk.scm 338 */
																				obj_t BgL_arg1812z00_1698;

																				BgL_arg1812z00_1698 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(36),
																					BNIL);
																				BgL_arg1808z00_1697 =
																					MAKE_YOUNG_PAIR(CNST_TABLE_REF(37),
																					BgL_arg1812z00_1698);
																			}
																			BgL_arg1805z00_1695 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(38),
																				BgL_arg1808z00_1697);
																		}
																		{	/* Abound/walk.scm 341 */
																			obj_t BgL_arg1820z00_1699;

																			{	/* Abound/walk.scm 341 */
																				obj_t BgL_arg1822z00_1700;

																				{	/* Abound/walk.scm 341 */
																					obj_t BgL_arg1823z00_1701;
																					obj_t BgL_arg1831z00_1702;

																					{	/* Abound/walk.scm 341 */
																						obj_t BgL_arg1832z00_1703;

																						{	/* Abound/walk.scm 341 */
																							obj_t BgL_arg1833z00_1704;

																							BgL_arg1833z00_1704 =
																								(((BgL_typez00_bglt) COBJECT(
																										((BgL_typez00_bglt)
																											BgL_vtypez00_42)))->
																								BgL_idz00);
																							BgL_arg1832z00_1703 =
																								SYMBOL_TO_STRING
																								(BgL_arg1833z00_1704);
																						}
																						BgL_arg1823z00_1701 =
																							string_append(BgL_arg1832z00_1703,
																							BGl_string2149z00zzabound_walkz00);
																					}
																					{	/* Abound/walk.scm 338 */
																						obj_t BgL_arg1834z00_1705;

																						{	/* Abound/walk.scm 338 */
																							obj_t BgL_arg1835z00_1706;

																							BgL_arg1835z00_1706 =
																								MAKE_YOUNG_PAIR(BgL_iz00_1653,
																								BNIL);
																							BgL_arg1834z00_1705 =
																								MAKE_YOUNG_PAIR(BgL_lz00_1654,
																								BgL_arg1835z00_1706);
																						}
																						BgL_arg1831z00_1702 =
																							MAKE_YOUNG_PAIR(BgL_vz00_1652,
																							BgL_arg1834z00_1705);
																					}
																					BgL_arg1822z00_1700 =
																						MAKE_YOUNG_PAIR(BgL_arg1823z00_1701,
																						BgL_arg1831z00_1702);
																				}
																				BgL_arg1820z00_1699 =
																					MAKE_YOUNG_PAIR(BgL_lposz00_1656,
																					BgL_arg1822z00_1700);
																			}
																			BgL_arg1806z00_1696 =
																				MAKE_YOUNG_PAIR(BgL_lnamez00_1655,
																				BgL_arg1820z00_1699);
																		}
																		BgL_arg1798z00_1693 =
																			MAKE_YOUNG_PAIR(BgL_arg1805z00_1695,
																			BgL_arg1806z00_1696);
																	}
																	{	/* Abound/walk.scm 337 */
																		obj_t BgL_arg1836z00_1707;

																		BgL_arg1836z00_1707 =
																			MAKE_YOUNG_PAIR(BFALSE, BNIL);
																		BgL_arg1799z00_1694 =
																			MAKE_YOUNG_PAIR(BFALSE,
																			BgL_arg1836z00_1707);
																	}
																	BgL_arg1775z00_1692 =
																		MAKE_YOUNG_PAIR(BgL_arg1798z00_1693,
																		BgL_arg1799z00_1694);
																}
																BgL_arg1773z00_1691 =
																	MAKE_YOUNG_PAIR(CNST_TABLE_REF(39),
																	BgL_arg1775z00_1692);
															}
															BgL_arg1771z00_1690 =
																MAKE_YOUNG_PAIR(BgL_arg1773z00_1691, BNIL);
														}
														BgL_arg1762z00_1686 =
															MAKE_YOUNG_PAIR(
															((obj_t) BgL_arg1770z00_1689),
															BgL_arg1771z00_1690);
													}
													BgL_arg1755z00_1684 =
														MAKE_YOUNG_PAIR(BgL_arg1761z00_1685,
														BgL_arg1762z00_1686);
												}
												BgL_arg1754z00_1683 =
													MAKE_YOUNG_PAIR(CNST_TABLE_REF(40),
													BgL_arg1755z00_1684);
											}
											BgL_arg1739z00_1673 =
												MAKE_YOUNG_PAIR(BgL_arg1754z00_1683, BNIL);
										}
										BgL_arg1737z00_1671 =
											MAKE_YOUNG_PAIR(BgL_arg1738z00_1672, BgL_arg1739z00_1673);
									}
									BgL_arg1736z00_1670 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(41), BgL_arg1737z00_1671);
								}
								BgL_arg1711z00_1660 =
									MAKE_YOUNG_PAIR(BgL_arg1736z00_1670, BNIL);
							}
							BgL_arg1709z00_1658 =
								MAKE_YOUNG_PAIR(BgL_arg1710z00_1659, BgL_arg1711z00_1660);
						}
						BgL_arg1708z00_1657 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(41), BgL_arg1709z00_1658);
					}
					return
						BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00
						(BgL_arg1708z00_1657, BgL_locz00_39);
				}
			}
		}

	}



/* abound-node*! */
	bool_t BGl_aboundzd2nodeza2z12z62zzabound_walkz00(obj_t BgL_nodeza2za2_56)
	{
		{	/* Abound/walk.scm 454 */
		BGl_aboundzd2nodeza2z12z62zzabound_walkz00:
			if (PAIRP(BgL_nodeza2za2_56))
				{	/* Abound/walk.scm 455 */
					{	/* Abound/walk.scm 456 */
						BgL_nodez00_bglt BgL_arg1843z00_1714;

						{	/* Abound/walk.scm 456 */
							obj_t BgL_arg1844z00_1715;

							BgL_arg1844z00_1715 = CAR(BgL_nodeza2za2_56);
							BgL_arg1843z00_1714 =
								BGl_aboundzd2nodezd2zzabound_walkz00(
								((BgL_nodez00_bglt) BgL_arg1844z00_1715));
						}
						{	/* Abound/walk.scm 456 */
							obj_t BgL_tmpz00_3300;

							BgL_tmpz00_3300 = ((obj_t) BgL_arg1843z00_1714);
							SET_CAR(BgL_nodeza2za2_56, BgL_tmpz00_3300);
						}
					}
					{
						obj_t BgL_nodeza2za2_3303;

						BgL_nodeza2za2_3303 = CDR(BgL_nodeza2za2_56);
						BgL_nodeza2za2_56 = BgL_nodeza2za2_3303;
						goto BGl_aboundzd2nodeza2z12z62zzabound_walkz00;
					}
				}
			else
				{	/* Abound/walk.scm 455 */
					return ((bool_t) 0);
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzabound_walkz00(void)
	{
		{	/* Abound/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzabound_walkz00(void)
	{
		{	/* Abound/walk.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00,
				BGl_proc2150z00zzabound_walkz00, BGl_nodez00zzast_nodez00,
				BGl_string2151z00zzabound_walkz00);
		}

	}



/* &abound-node1307 */
	obj_t BGl_z62aboundzd2node1307zb0zzabound_walkz00(obj_t BgL_envz00_2517,
		obj_t BgL_nodez00_2518)
	{
		{	/* Abound/walk.scm 121 */
			return ((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2518));
		}

	}



/* abound-node */
	BgL_nodez00_bglt BGl_aboundzd2nodezd2zzabound_walkz00(BgL_nodez00_bglt
		BgL_nodez00_19)
	{
		{	/* Abound/walk.scm 121 */
			{	/* Abound/walk.scm 121 */
				obj_t BgL_method1308z00_1721;

				{	/* Abound/walk.scm 121 */
					obj_t BgL_res2133z00_2362;

					{	/* Abound/walk.scm 121 */
						long BgL_objzd2classzd2numz00_2333;

						BgL_objzd2classzd2numz00_2333 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_19));
						{	/* Abound/walk.scm 121 */
							obj_t BgL_arg1811z00_2334;

							BgL_arg1811z00_2334 =
								PROCEDURE_REF(BGl_aboundzd2nodezd2envz00zzabound_walkz00,
								(int) (1L));
							{	/* Abound/walk.scm 121 */
								int BgL_offsetz00_2337;

								BgL_offsetz00_2337 = (int) (BgL_objzd2classzd2numz00_2333);
								{	/* Abound/walk.scm 121 */
									long BgL_offsetz00_2338;

									BgL_offsetz00_2338 =
										((long) (BgL_offsetz00_2337) - OBJECT_TYPE);
									{	/* Abound/walk.scm 121 */
										long BgL_modz00_2339;

										BgL_modz00_2339 =
											(BgL_offsetz00_2338 >> (int) ((long) ((int) (4L))));
										{	/* Abound/walk.scm 121 */
											long BgL_restz00_2341;

											BgL_restz00_2341 =
												(BgL_offsetz00_2338 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Abound/walk.scm 121 */

												{	/* Abound/walk.scm 121 */
													obj_t BgL_bucketz00_2343;

													BgL_bucketz00_2343 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2334), BgL_modz00_2339);
													BgL_res2133z00_2362 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2343), BgL_restz00_2341);
					}}}}}}}}
					BgL_method1308z00_1721 = BgL_res2133z00_2362;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1308z00_1721,
						((obj_t) BgL_nodez00_19)));
			}
		}

	}



/* &abound-node */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezb0zzabound_walkz00(obj_t
		BgL_envz00_2519, obj_t BgL_nodez00_2520)
	{
		{	/* Abound/walk.scm 121 */
			return
				BGl_aboundzd2nodezd2zzabound_walkz00(
				((BgL_nodez00_bglt) BgL_nodez00_2520));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzabound_walkz00(void)
	{
		{	/* Abound/walk.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00,
				BGl_sequencez00zzast_nodez00, BGl_proc2152z00zzabound_walkz00,
				BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00, BGl_syncz00zzast_nodez00,
				BGl_proc2154z00zzabound_walkz00, BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00, BGl_appz00zzast_nodez00,
				BGl_proc2155z00zzabound_walkz00, BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2156z00zzabound_walkz00,
				BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00,
				BGl_funcallz00zzast_nodez00, BGl_proc2157z00zzabound_walkz00,
				BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00, BGl_externz00zzast_nodez00,
				BGl_proc2158z00zzabound_walkz00, BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00, BGl_vrefz00zzast_nodez00,
				BGl_proc2159z00zzabound_walkz00, BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00,
				BGl_vsetz12z12zzast_nodez00, BGl_proc2160z00zzabound_walkz00,
				BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00, BGl_castz00zzast_nodez00,
				BGl_proc2161z00zzabound_walkz00, BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00, BGl_setqz00zzast_nodez00,
				BGl_proc2162z00zzabound_walkz00, BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2163z00zzabound_walkz00,
				BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00, BGl_failz00zzast_nodez00,
				BGl_proc2164z00zzabound_walkz00, BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00, BGl_switchz00zzast_nodez00,
				BGl_proc2165z00zzabound_walkz00, BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2166z00zzabound_walkz00,
				BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2167z00zzabound_walkz00,
				BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2168z00zzabound_walkz00,
				BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2169z00zzabound_walkz00,
				BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2170z00zzabound_walkz00,
				BGl_string2153z00zzabound_walkz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2171z00zzabound_walkz00,
				BGl_string2153z00zzabound_walkz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_aboundzd2nodezd2envz00zzabound_walkz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2172z00zzabound_walkz00,
				BGl_string2153z00zzabound_walkz00);
		}

	}



/* &abound-node-box-set!1348 */
	BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2boxzd2setz121348za2zzabound_walkz00(obj_t
		BgL_envz00_2545, obj_t BgL_nodez00_2546)
	{
		{	/* Abound/walk.scm 445 */
			{
				BgL_varz00_bglt BgL_auxz00_3361;

				{	/* Abound/walk.scm 447 */
					BgL_varz00_bglt BgL_arg2061z00_2639;

					BgL_arg2061z00_2639 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2546)))->BgL_varz00);
					BgL_auxz00_3361 =
						((BgL_varz00_bglt)
						BGl_aboundzd2nodezd2zzabound_walkz00(
							((BgL_nodez00_bglt) BgL_arg2061z00_2639)));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2546)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3361), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3369;

				{	/* Abound/walk.scm 448 */
					BgL_nodez00_bglt BgL_arg2062z00_2640;

					BgL_arg2062z00_2640 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2546)))->BgL_valuez00);
					BgL_auxz00_3369 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg2062z00_2640);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2546)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3369), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2546));
		}

	}



/* &abound-node-box-ref1346 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2boxzd2ref1346zb0zzabound_walkz00(obj_t
		BgL_envz00_2547, obj_t BgL_nodez00_2548)
	{
		{	/* Abound/walk.scm 438 */
			{	/* Abound/walk.scm 439 */
				BgL_nodez00_bglt BgL_arg2059z00_2642;

				{	/* Abound/walk.scm 439 */
					BgL_varz00_bglt BgL_arg2060z00_2643;

					BgL_arg2060z00_2643 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2548)))->BgL_varz00);
					BgL_arg2059z00_2642 =
						BGl_aboundzd2nodezd2zzabound_walkz00(
						((BgL_nodez00_bglt) BgL_arg2060z00_2643));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2548)))->BgL_varz00) =
					((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_arg2059z00_2642)), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2548));
		}

	}



/* &abound-node-make-box1344 */
	BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2makezd2box1344zb0zzabound_walkz00(obj_t
		BgL_envz00_2549, obj_t BgL_nodez00_2550)
	{
		{	/* Abound/walk.scm 431 */
			((((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2550)))->BgL_valuez00) =
				((BgL_nodez00_bglt)
					BGl_aboundzd2nodezd2zzabound_walkz00((((BgL_makezd2boxzd2_bglt)
								COBJECT(((BgL_makezd2boxzd2_bglt) BgL_nodez00_2550)))->
							BgL_valuez00))), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2550));
		}

	}



/* &abound-node-jump-ex-1342 */
	BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2jumpzd2exzd21342z62zzabound_walkz00(obj_t
		BgL_envz00_2551, obj_t BgL_nodez00_2552)
	{
		{	/* Abound/walk.scm 422 */
			{
				BgL_nodez00_bglt BgL_auxz00_3393;

				{	/* Abound/walk.scm 424 */
					BgL_nodez00_bglt BgL_arg2055z00_2646;

					BgL_arg2055z00_2646 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2552)))->BgL_exitz00);
					BgL_auxz00_3393 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg2055z00_2646);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2552)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_3393), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3399;

				{	/* Abound/walk.scm 425 */
					BgL_nodez00_bglt BgL_arg2056z00_2647;

					BgL_arg2056z00_2647 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2552)))->
						BgL_valuez00);
					BgL_auxz00_3399 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg2056z00_2647);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2552)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_3399), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2552));
		}

	}



/* &abound-node-set-ex-i1340 */
	BgL_nodez00_bglt
		BGl_z62aboundzd2nodezd2setzd2exzd2i1340z62zzabound_walkz00(obj_t
		BgL_envz00_2553, obj_t BgL_nodez00_2554)
	{
		{	/* Abound/walk.scm 414 */
			((((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2554)))->BgL_bodyz00) =
				((BgL_nodez00_bglt)
					BGl_aboundzd2nodezd2zzabound_walkz00((((BgL_setzd2exzd2itz00_bglt)
								COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2554)))->
							BgL_bodyz00))), BUNSPEC);
			((((BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
								BgL_nodez00_2554)))->BgL_onexitz00) =
				((BgL_nodez00_bglt)
					BGl_aboundzd2nodezd2zzabound_walkz00((((BgL_setzd2exzd2itz00_bglt)
								COBJECT(((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2554)))->
							BgL_onexitz00))), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt)
					BgL_nodez00_2554));
		}

	}



/* &abound-node-let-var1338 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2letzd2var1338zb0zzabound_walkz00(obj_t
		BgL_envz00_2555, obj_t BgL_nodez00_2556)
	{
		{	/* Abound/walk.scm 403 */
			{	/* Abound/walk.scm 405 */
				obj_t BgL_g1306z00_2650;

				BgL_g1306z00_2650 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2556)))->BgL_bindingsz00);
				{
					obj_t BgL_l1304z00_2652;

					BgL_l1304z00_2652 = BgL_g1306z00_2650;
				BgL_zc3z04anonymousza32043ze3z87_2651:
					if (PAIRP(BgL_l1304z00_2652))
						{	/* Abound/walk.scm 405 */
							{	/* Abound/walk.scm 406 */
								obj_t BgL_bindingz00_2653;

								BgL_bindingz00_2653 = CAR(BgL_l1304z00_2652);
								{	/* Abound/walk.scm 406 */
									BgL_nodez00_bglt BgL_arg2045z00_2654;

									{	/* Abound/walk.scm 406 */
										obj_t BgL_arg2046z00_2655;

										BgL_arg2046z00_2655 = CDR(((obj_t) BgL_bindingz00_2653));
										BgL_arg2045z00_2654 =
											BGl_aboundzd2nodezd2zzabound_walkz00(
											((BgL_nodez00_bglt) BgL_arg2046z00_2655));
									}
									{	/* Abound/walk.scm 406 */
										obj_t BgL_auxz00_3430;
										obj_t BgL_tmpz00_3428;

										BgL_auxz00_3430 = ((obj_t) BgL_arg2045z00_2654);
										BgL_tmpz00_3428 = ((obj_t) BgL_bindingz00_2653);
										SET_CDR(BgL_tmpz00_3428, BgL_auxz00_3430);
									}
								}
							}
							{
								obj_t BgL_l1304z00_3433;

								BgL_l1304z00_3433 = CDR(BgL_l1304z00_2652);
								BgL_l1304z00_2652 = BgL_l1304z00_3433;
								goto BgL_zc3z04anonymousza32043ze3z87_2651;
							}
						}
					else
						{	/* Abound/walk.scm 405 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3435;

				{	/* Abound/walk.scm 408 */
					BgL_nodez00_bglt BgL_arg2048z00_2656;

					BgL_arg2048z00_2656 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2556)))->BgL_bodyz00);
					BgL_auxz00_3435 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg2048z00_2656);
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2556)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3435), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2556));
		}

	}



/* &abound-node-let-fun1336 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2letzd2fun1336zb0zzabound_walkz00(obj_t
		BgL_envz00_2557, obj_t BgL_nodez00_2558)
	{
		{	/* Abound/walk.scm 394 */
			{	/* Abound/walk.scm 396 */
				obj_t BgL_g1303z00_2658;

				BgL_g1303z00_2658 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2558)))->BgL_localsz00);
				{
					obj_t BgL_l1301z00_2660;

					BgL_l1301z00_2660 = BgL_g1303z00_2658;
				BgL_zc3z04anonymousza32039ze3z87_2659:
					if (PAIRP(BgL_l1301z00_2660))
						{	/* Abound/walk.scm 396 */
							BGl_aboundzd2funz12zc0zzabound_walkz00(CAR(BgL_l1301z00_2660));
							{
								obj_t BgL_l1301z00_3449;

								BgL_l1301z00_3449 = CDR(BgL_l1301z00_2660);
								BgL_l1301z00_2660 = BgL_l1301z00_3449;
								goto BgL_zc3z04anonymousza32039ze3z87_2659;
							}
						}
					else
						{	/* Abound/walk.scm 396 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3451;

				{	/* Abound/walk.scm 397 */
					BgL_nodez00_bglt BgL_arg2042z00_2661;

					BgL_arg2042z00_2661 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2558)))->BgL_bodyz00);
					BgL_auxz00_3451 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg2042z00_2661);
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2558)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3451), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2558));
		}

	}



/* &abound-node-switch1334 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2switch1334z62zzabound_walkz00(obj_t
		BgL_envz00_2559, obj_t BgL_nodez00_2560)
	{
		{	/* Abound/walk.scm 383 */
			{
				BgL_nodez00_bglt BgL_auxz00_3459;

				{	/* Abound/walk.scm 385 */
					BgL_nodez00_bglt BgL_arg2033z00_2663;

					BgL_arg2033z00_2663 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2560)))->BgL_testz00);
					BgL_auxz00_3459 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg2033z00_2663);
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2560)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3459), BUNSPEC);
			}
			{	/* Abound/walk.scm 386 */
				obj_t BgL_g1300z00_2664;

				BgL_g1300z00_2664 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2560)))->BgL_clausesz00);
				{
					obj_t BgL_l1298z00_2666;

					BgL_l1298z00_2666 = BgL_g1300z00_2664;
				BgL_zc3z04anonymousza32034ze3z87_2665:
					if (PAIRP(BgL_l1298z00_2666))
						{	/* Abound/walk.scm 386 */
							{	/* Abound/walk.scm 387 */
								obj_t BgL_clausez00_2667;

								BgL_clausez00_2667 = CAR(BgL_l1298z00_2666);
								{	/* Abound/walk.scm 387 */
									BgL_nodez00_bglt BgL_arg2036z00_2668;

									{	/* Abound/walk.scm 387 */
										obj_t BgL_arg2037z00_2669;

										BgL_arg2037z00_2669 = CDR(((obj_t) BgL_clausez00_2667));
										BgL_arg2036z00_2668 =
											BGl_aboundzd2nodezd2zzabound_walkz00(
											((BgL_nodez00_bglt) BgL_arg2037z00_2669));
									}
									{	/* Abound/walk.scm 387 */
										obj_t BgL_auxz00_3476;
										obj_t BgL_tmpz00_3474;

										BgL_auxz00_3476 = ((obj_t) BgL_arg2036z00_2668);
										BgL_tmpz00_3474 = ((obj_t) BgL_clausez00_2667);
										SET_CDR(BgL_tmpz00_3474, BgL_auxz00_3476);
									}
								}
							}
							{
								obj_t BgL_l1298z00_3479;

								BgL_l1298z00_3479 = CDR(BgL_l1298z00_2666);
								BgL_l1298z00_2666 = BgL_l1298z00_3479;
								goto BgL_zc3z04anonymousza32034ze3z87_2665;
							}
						}
					else
						{	/* Abound/walk.scm 386 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_2560));
		}

	}



/* &abound-node-fail1332 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2fail1332z62zzabound_walkz00(obj_t
		BgL_envz00_2561, obj_t BgL_nodez00_2562)
	{
		{	/* Abound/walk.scm 373 */
			{
				BgL_nodez00_bglt BgL_auxz00_3483;

				{	/* Abound/walk.scm 375 */
					BgL_nodez00_bglt BgL_arg2029z00_2671;

					BgL_arg2029z00_2671 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2562)))->BgL_procz00);
					BgL_auxz00_3483 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg2029z00_2671);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2562)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3483), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3489;

				{	/* Abound/walk.scm 376 */
					BgL_nodez00_bglt BgL_arg2030z00_2672;

					BgL_arg2030z00_2672 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2562)))->BgL_msgz00);
					BgL_auxz00_3489 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg2030z00_2672);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2562)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3489), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3495;

				{	/* Abound/walk.scm 377 */
					BgL_nodez00_bglt BgL_arg2031z00_2673;

					BgL_arg2031z00_2673 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2562)))->BgL_objz00);
					BgL_auxz00_3495 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg2031z00_2673);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2562)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3495), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_2562));
		}

	}



/* &abound-node-conditio1330 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2conditio1330z62zzabound_walkz00(obj_t
		BgL_envz00_2563, obj_t BgL_nodez00_2564)
	{
		{	/* Abound/walk.scm 363 */
			{
				BgL_nodez00_bglt BgL_auxz00_3503;

				{	/* Abound/walk.scm 365 */
					BgL_nodez00_bglt BgL_arg2025z00_2675;

					BgL_arg2025z00_2675 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2564)))->BgL_testz00);
					BgL_auxz00_3503 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg2025z00_2675);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2564)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3503), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3509;

				{	/* Abound/walk.scm 366 */
					BgL_nodez00_bglt BgL_arg2026z00_2676;

					BgL_arg2026z00_2676 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2564)))->BgL_truez00);
					BgL_auxz00_3509 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg2026z00_2676);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2564)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3509), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3515;

				{	/* Abound/walk.scm 367 */
					BgL_nodez00_bglt BgL_arg2027z00_2677;

					BgL_arg2027z00_2677 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2564)))->BgL_falsez00);
					BgL_auxz00_3515 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg2027z00_2677);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2564)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3515), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_2564));
		}

	}



/* &abound-node-setq1328 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2setq1328z62zzabound_walkz00(obj_t
		BgL_envz00_2565, obj_t BgL_nodez00_2566)
	{
		{	/* Abound/walk.scm 356 */
			((((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2566)))->BgL_valuez00) =
				((BgL_nodez00_bglt)
					BGl_aboundzd2nodezd2zzabound_walkz00((((BgL_setqz00_bglt)
								COBJECT(((BgL_setqz00_bglt) BgL_nodez00_2566)))->
							BgL_valuez00))), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_2566));
		}

	}



/* &abound-node-cast1326 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2cast1326z62zzabound_walkz00(obj_t
		BgL_envz00_2567, obj_t BgL_nodez00_2568)
	{
		{	/* Abound/walk.scm 349 */
			BGl_aboundzd2nodezd2zzabound_walkz00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2568)))->BgL_argz00));
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_2568));
		}

	}



/* &abound-node-vset!1324 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2vsetz121324z70zzabound_walkz00(obj_t
		BgL_envz00_2569, obj_t BgL_nodez00_2570)
	{
		{	/* Abound/walk.scm 297 */
			{

				{
					BgL_vsetz12z12_bglt BgL_nodez00_2684;

					{	/* Abound/walk.scm 297 */
						obj_t BgL_nextzd2method1323zd2_2682;

						BgL_nextzd2method1323zd2_2682 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_vsetz12z12_bglt) BgL_nodez00_2570)),
							BGl_aboundzd2nodezd2envz00zzabound_walkz00,
							BGl_vsetz12z12zzast_nodez00);
						BGL_PROCEDURE_CALL1(BgL_nextzd2method1323zd2_2682,
							((obj_t) ((BgL_vsetz12z12_bglt) BgL_nodez00_2570)));
					}
					if (
						(((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt) BgL_nodez00_2570)))->BgL_unsafez00))
						{	/* Abound/walk.scm 309 */
							return
								((BgL_nodez00_bglt) ((BgL_vsetz12z12_bglt) BgL_nodez00_2570));
						}
					else
						{	/* Abound/walk.scm 311 */
							BgL_nodez00_bglt BgL_nodez00_2694;

							BgL_nodez00_2684 = ((BgL_vsetz12z12_bglt) BgL_nodez00_2570);
							{	/* Abound/walk.scm 301 */
								obj_t BgL_arg2004z00_2685;
								obj_t BgL_arg2006z00_2686;
								obj_t BgL_arg2007z00_2687;
								BgL_typez00_bglt BgL_arg2008z00_2688;
								BgL_typez00_bglt BgL_arg2009z00_2689;
								BgL_typez00_bglt BgL_arg2010z00_2690;

								{	/* Abound/walk.scm 301 */
									obj_t BgL_pairz00_2691;

									BgL_pairz00_2691 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt) BgL_nodez00_2684)))->
										BgL_exprza2za2);
									BgL_arg2004z00_2685 = CAR(BgL_pairz00_2691);
								}
								{	/* Abound/walk.scm 301 */
									obj_t BgL_pairz00_2692;

									BgL_pairz00_2692 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt) BgL_nodez00_2684)))->
										BgL_exprza2za2);
									BgL_arg2006z00_2686 = CAR(CDR(BgL_pairz00_2692));
								}
								BgL_arg2007z00_2687 =
									(((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_nodez00_2684)))->BgL_locz00);
								BgL_arg2008z00_2688 =
									(((BgL_vsetz12z12_bglt) COBJECT(BgL_nodez00_2684))->
									BgL_ftypez00);
								BgL_arg2009z00_2689 =
									(((BgL_vsetz12z12_bglt) COBJECT(BgL_nodez00_2684))->
									BgL_otypez00);
								BgL_arg2010z00_2690 =
									(((BgL_vsetz12z12_bglt) COBJECT(BgL_nodez00_2684))->
									BgL_vtypez00);
								BgL_nodez00_2694 =
									BGl_arrayzd2setz12zc0zzabound_walkz00((
										(BgL_nodezf2effectzf2_bglt) BgL_nodez00_2684),
									BgL_arg2004z00_2685, BgL_arg2006z00_2686, BgL_arg2007z00_2687,
									((obj_t) BgL_arg2008z00_2688), ((obj_t) BgL_arg2009z00_2689),
									((obj_t) BgL_arg2010z00_2690),
									BGl_proc2173z00zzabound_walkz00);
							}
							BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nodez00_2694);
							return BgL_nodez00_2694;
						}
				}
			}
		}

	}



/* &<@anonymous:2014> */
	BgL_vsetz12z12_bglt BGl_z62zc3z04anonymousza32014ze3ze5zzabound_walkz00(obj_t
		BgL_envz00_2571, BgL_nodezf2effectzf2_bglt BgL_nodez00_2572,
		obj_t BgL_vz00_2573, obj_t BgL_iz00_2574)
	{
		{	/* Abound/walk.scm 302 */
			{	/* Abound/walk.scm 304 */
				BgL_vsetz12z12_bglt BgL_new1160z00_2698;

				{	/* Abound/walk.scm 304 */
					BgL_vsetz12z12_bglt BgL_new1171z00_2699;

					BgL_new1171z00_2699 =
						((BgL_vsetz12z12_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_vsetz12z12_bgl))));
					{	/* Abound/walk.scm 304 */
						long BgL_arg2020z00_2700;

						BgL_arg2020z00_2700 = BGL_CLASS_NUM(BGl_vsetz12z12zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1171z00_2699), BgL_arg2020z00_2700);
					}
					{	/* Abound/walk.scm 304 */
						BgL_objectz00_bglt BgL_tmpz00_3572;

						BgL_tmpz00_3572 = ((BgL_objectz00_bglt) BgL_new1171z00_2699);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3572, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1171z00_2699);
					BgL_new1160z00_2698 = BgL_new1171z00_2699;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1160z00_2698)))->BgL_locz00) =
					((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_nodez00_2572)))->BgL_locz00)), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1160z00_2698)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_nodez00_2572)))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1160z00_2698)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2572))))->BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1160z00_2698)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2572))))->BgL_keyz00)), BUNSPEC);
				{
					obj_t BgL_auxz00_3594;

					{	/* Abound/walk.scm 305 */
						obj_t BgL_arg2015z00_2701;

						{	/* Abound/walk.scm 305 */
							obj_t BgL_pairz00_2702;

							BgL_pairz00_2702 =
								(((BgL_externz00_bglt) COBJECT(
										((BgL_externz00_bglt)
											((BgL_vsetz12z12_bglt) BgL_nodez00_2572))))->
								BgL_exprza2za2);
							{	/* Abound/walk.scm 305 */
								obj_t BgL_pairz00_2703;

								{	/* Abound/walk.scm 305 */
									obj_t BgL_pairz00_2704;

									BgL_pairz00_2704 = CDR(BgL_pairz00_2702);
									BgL_pairz00_2703 = CDR(BgL_pairz00_2704);
								}
								BgL_arg2015z00_2701 = CAR(BgL_pairz00_2703);
						}}
						{	/* Abound/walk.scm 305 */
							obj_t BgL_list2016z00_2705;

							{	/* Abound/walk.scm 305 */
								obj_t BgL_arg2017z00_2706;

								{	/* Abound/walk.scm 305 */
									obj_t BgL_arg2018z00_2707;

									BgL_arg2018z00_2707 =
										MAKE_YOUNG_PAIR(BgL_arg2015z00_2701, BNIL);
									BgL_arg2017z00_2706 =
										MAKE_YOUNG_PAIR(BgL_iz00_2574, BgL_arg2018z00_2707);
								}
								BgL_list2016z00_2705 =
									MAKE_YOUNG_PAIR(BgL_vz00_2573, BgL_arg2017z00_2706);
							}
							BgL_auxz00_3594 = BgL_list2016z00_2705;
					}}
					((((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt) BgL_new1160z00_2698)))->
							BgL_exprza2za2) = ((obj_t) BgL_auxz00_3594), BUNSPEC);
				}
				((((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_new1160z00_2698)))->BgL_effectz00) =
					((obj_t) (((BgL_externz00_bglt)
								COBJECT(((BgL_externz00_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2572))))->BgL_effectz00)), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1160z00_2698)))->BgL_czd2formatzd2) =
					((obj_t) (((BgL_privatez00_bglt)
								COBJECT(((BgL_privatez00_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2572))))->BgL_czd2formatzd2)), BUNSPEC);
				((((BgL_vsetz12z12_bglt) COBJECT(BgL_new1160z00_2698))->BgL_ftypez00) =
					((BgL_typez00_bglt) (((BgL_vsetz12z12_bglt)
								COBJECT(((BgL_vsetz12z12_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2572))))->BgL_ftypez00)), BUNSPEC);
				((((BgL_vsetz12z12_bglt) COBJECT(BgL_new1160z00_2698))->BgL_otypez00) =
					((BgL_typez00_bglt) (((BgL_vsetz12z12_bglt)
								COBJECT(((BgL_vsetz12z12_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2572))))->BgL_otypez00)), BUNSPEC);
				((((BgL_vsetz12z12_bglt) COBJECT(BgL_new1160z00_2698))->BgL_vtypez00) =
					((BgL_typez00_bglt) (((BgL_vsetz12z12_bglt)
								COBJECT(((BgL_vsetz12z12_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2572))))->BgL_vtypez00)), BUNSPEC);
				((((BgL_vsetz12z12_bglt) COBJECT(BgL_new1160z00_2698))->BgL_unsafez00) =
					((bool_t) (((BgL_vsetz12z12_bglt)
								COBJECT(((BgL_vsetz12z12_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2572))))->BgL_unsafez00)), BUNSPEC);
				return BgL_new1160z00_2698;
			}
		}

	}



/* &abound-node-vref1322 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2vref1322z62zzabound_walkz00(obj_t
		BgL_envz00_2575, obj_t BgL_nodez00_2576)
	{
		{	/* Abound/walk.scm 248 */
			{

				{
					BgL_vrefz00_bglt BgL_nodez00_2712;

					{	/* Abound/walk.scm 248 */
						obj_t BgL_nextzd2method1321zd2_2710;

						BgL_nextzd2method1321zd2_2710 =
							BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
							((BgL_objectz00_bglt)
								((BgL_vrefz00_bglt) BgL_nodez00_2576)),
							BGl_aboundzd2nodezd2envz00zzabound_walkz00,
							BGl_vrefz00zzast_nodez00);
						BGL_PROCEDURE_CALL1(BgL_nextzd2method1321zd2_2710,
							((obj_t) ((BgL_vrefz00_bglt) BgL_nodez00_2576)));
					}
					if (
						(((BgL_vrefz00_bglt) COBJECT(
									((BgL_vrefz00_bglt) BgL_nodez00_2576)))->BgL_unsafez00))
						{	/* Abound/walk.scm 257 */
							return ((BgL_nodez00_bglt) ((BgL_vrefz00_bglt) BgL_nodez00_2576));
						}
					else
						{	/* Abound/walk.scm 259 */
							BgL_nodez00_bglt BgL_nodez00_2722;

							BgL_nodez00_2712 = ((BgL_vrefz00_bglt) BgL_nodez00_2576);
							{	/* Abound/walk.scm 252 */
								obj_t BgL_arg1989z00_2713;
								obj_t BgL_arg1990z00_2714;
								obj_t BgL_arg1991z00_2715;
								BgL_typez00_bglt BgL_arg1992z00_2716;
								BgL_typez00_bglt BgL_arg1993z00_2717;
								BgL_typez00_bglt BgL_arg1994z00_2718;

								{	/* Abound/walk.scm 252 */
									obj_t BgL_pairz00_2719;

									BgL_pairz00_2719 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt) BgL_nodez00_2712)))->
										BgL_exprza2za2);
									BgL_arg1989z00_2713 = CAR(BgL_pairz00_2719);
								}
								{	/* Abound/walk.scm 252 */
									obj_t BgL_pairz00_2720;

									BgL_pairz00_2720 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt) BgL_nodez00_2712)))->
										BgL_exprza2za2);
									BgL_arg1990z00_2714 = CAR(CDR(BgL_pairz00_2720));
								}
								BgL_arg1991z00_2715 =
									(((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt) BgL_nodez00_2712)))->BgL_locz00);
								BgL_arg1992z00_2716 =
									(((BgL_vrefz00_bglt) COBJECT(BgL_nodez00_2712))->
									BgL_ftypez00);
								BgL_arg1993z00_2717 =
									(((BgL_vrefz00_bglt) COBJECT(BgL_nodez00_2712))->
									BgL_otypez00);
								BgL_arg1994z00_2718 =
									(((BgL_vrefz00_bglt) COBJECT(BgL_nodez00_2712))->
									BgL_vtypez00);
								BgL_nodez00_2722 =
									BGl_arrayzd2refzd2zzabound_walkz00((
										(BgL_nodezf2effectzf2_bglt) BgL_nodez00_2712),
									BgL_arg1989z00_2713, BgL_arg1990z00_2714, BgL_arg1991z00_2715,
									BgL_arg1992z00_2716, ((obj_t) BgL_arg1993z00_2717),
									((obj_t) BgL_arg1994z00_2718),
									BGl_proc2174z00zzabound_walkz00);
							}
							BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nodez00_2722);
							return BgL_nodez00_2722;
						}
				}
			}
		}

	}



/* &<@anonymous:1998> */
	BgL_vrefz00_bglt BGl_z62zc3z04anonymousza31998ze3ze5zzabound_walkz00(obj_t
		BgL_envz00_2577, BgL_nodezf2effectzf2_bglt BgL_nodez00_2578,
		obj_t BgL_vz00_2579, obj_t BgL_iz00_2580)
	{
		{	/* Abound/walk.scm 253 */
			{	/* Abound/walk.scm 253 */
				BgL_vrefz00_bglt BgL_new1144z00_2726;

				{	/* Abound/walk.scm 253 */
					BgL_vrefz00_bglt BgL_new1156z00_2727;

					BgL_new1156z00_2727 =
						((BgL_vrefz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_vrefz00_bgl))));
					{	/* Abound/walk.scm 253 */
						long BgL_arg2001z00_2728;

						BgL_arg2001z00_2728 = BGL_CLASS_NUM(BGl_vrefz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1156z00_2727), BgL_arg2001z00_2728);
					}
					{	/* Abound/walk.scm 253 */
						BgL_objectz00_bglt BgL_tmpz00_3668;

						BgL_tmpz00_3668 = ((BgL_objectz00_bglt) BgL_new1156z00_2727);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3668, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1156z00_2727);
					BgL_new1144z00_2726 = BgL_new1156z00_2727;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1144z00_2726)))->BgL_locz00) =
					((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_nodez00_2578)))->BgL_locz00)), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1144z00_2726)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_nodez00_2578)))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1144z00_2726)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2578))))->BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1144z00_2726)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2578))))->BgL_keyz00)), BUNSPEC);
				{
					obj_t BgL_auxz00_3690;

					{	/* Abound/walk.scm 253 */
						obj_t BgL_list1999z00_2729;

						{	/* Abound/walk.scm 253 */
							obj_t BgL_arg2000z00_2730;

							BgL_arg2000z00_2730 = MAKE_YOUNG_PAIR(BgL_iz00_2580, BNIL);
							BgL_list1999z00_2729 =
								MAKE_YOUNG_PAIR(BgL_vz00_2579, BgL_arg2000z00_2730);
						}
						BgL_auxz00_3690 = BgL_list1999z00_2729;
					}
					((((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt) BgL_new1144z00_2726)))->
							BgL_exprza2za2) = ((obj_t) BgL_auxz00_3690), BUNSPEC);
				}
				((((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_new1144z00_2726)))->BgL_effectz00) =
					((obj_t) (((BgL_externz00_bglt)
								COBJECT(((BgL_externz00_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2578))))->BgL_effectz00)), BUNSPEC);
				((((BgL_privatez00_bglt) COBJECT(((BgL_privatez00_bglt)
									BgL_new1144z00_2726)))->BgL_czd2formatzd2) =
					((obj_t) (((BgL_privatez00_bglt)
								COBJECT(((BgL_privatez00_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2578))))->BgL_czd2formatzd2)), BUNSPEC);
				((((BgL_vrefz00_bglt) COBJECT(BgL_new1144z00_2726))->BgL_ftypez00) =
					((BgL_typez00_bglt) (((BgL_vrefz00_bglt)
								COBJECT(((BgL_vrefz00_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2578))))->BgL_ftypez00)), BUNSPEC);
				((((BgL_vrefz00_bglt) COBJECT(BgL_new1144z00_2726))->BgL_otypez00) =
					((BgL_typez00_bglt) (((BgL_vrefz00_bglt)
								COBJECT(((BgL_vrefz00_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2578))))->BgL_otypez00)), BUNSPEC);
				((((BgL_vrefz00_bglt) COBJECT(BgL_new1144z00_2726))->BgL_vtypez00) =
					((BgL_typez00_bglt) (((BgL_vrefz00_bglt)
								COBJECT(((BgL_vrefz00_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2578))))->BgL_vtypez00)), BUNSPEC);
				((((BgL_vrefz00_bglt) COBJECT(BgL_new1144z00_2726))->BgL_unsafez00) =
					((bool_t) (((BgL_vrefz00_bglt)
								COBJECT(((BgL_vrefz00_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2578))))->BgL_unsafez00)), BUNSPEC);
				return BgL_new1144z00_2726;
			}
		}

	}



/* &abound-node-extern1320 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2extern1320z62zzabound_walkz00(obj_t
		BgL_envz00_2581, obj_t BgL_nodez00_2582)
	{
		{	/* Abound/walk.scm 241 */
			BGl_aboundzd2nodeza2z12z62zzabound_walkz00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2582)))->BgL_exprza2za2));
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_2582));
		}

	}



/* &abound-node-funcall1318 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2funcall1318z62zzabound_walkz00(obj_t
		BgL_envz00_2583, obj_t BgL_nodez00_2584)
	{
		{	/* Abound/walk.scm 232 */
			{
				BgL_nodez00_bglt BgL_auxz00_3726;

				{	/* Abound/walk.scm 234 */
					BgL_nodez00_bglt BgL_arg1984z00_2733;

					BgL_arg1984z00_2733 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2584)))->BgL_funz00);
					BgL_auxz00_3726 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg1984z00_2733);
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2584)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3726), BUNSPEC);
			}
			BGl_aboundzd2nodeza2z12z62zzabound_walkz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2584)))->BgL_argsz00));
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_2584));
		}

	}



/* &abound-node-app-ly1316 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2appzd2ly1316zb0zzabound_walkz00(obj_t
		BgL_envz00_2585, obj_t BgL_nodez00_2586)
	{
		{	/* Abound/walk.scm 223 */
			{
				BgL_nodez00_bglt BgL_auxz00_3737;

				{	/* Abound/walk.scm 225 */
					BgL_nodez00_bglt BgL_arg1982z00_2735;

					BgL_arg1982z00_2735 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2586)))->BgL_funz00);
					BgL_auxz00_3737 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg1982z00_2735);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2586)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3737), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3743;

				{	/* Abound/walk.scm 226 */
					BgL_nodez00_bglt BgL_arg1983z00_2736;

					BgL_arg1983z00_2736 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2586)))->BgL_argz00);
					BgL_auxz00_3743 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg1983z00_2736);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2586)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3743), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_2586));
		}

	}



/* &abound-node-app1314 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2app1314z62zzabound_walkz00(obj_t
		BgL_envz00_2587, obj_t BgL_nodez00_2588)
	{
		{	/* Abound/walk.scm 143 */
			{
				BgL_appz00_bglt BgL_nodez00_2739;

				BGl_aboundzd2nodeza2z12z62zzabound_walkz00(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_2588)))->BgL_argsz00));
				{	/* Abound/walk.scm 172 */
					BgL_variablez00_bglt BgL_vz00_2809;

					BgL_vz00_2809 =
						(((BgL_varz00_bglt) COBJECT(
								(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_2588)))->BgL_funz00)))->
						BgL_variablez00);
					{	/* Abound/walk.scm 174 */
						bool_t BgL_test2256z00_3757;

						if (
							(((obj_t) BgL_vz00_2809) ==
								BGl_za2stringzd2refza2zd2zzabound_walkz00))
							{	/* Abound/walk.scm 174 */
								BgL_test2256z00_3757 = ((bool_t) 1);
							}
						else
							{	/* Abound/walk.scm 174 */
								BgL_test2256z00_3757 =
									(
									((obj_t) BgL_vz00_2809) ==
									BGl_za2stringzd2setz12za2zc0zzabound_walkz00);
							}
						if (BgL_test2256z00_3757)
							{	/* Abound/walk.scm 175 */
								BgL_nodez00_bglt BgL_nodez00_2810;

								BgL_nodez00_2739 = ((BgL_appz00_bglt) BgL_nodez00_2588);
								{	/* Abound/walk.scm 147 */
									obj_t BgL_sz00_2740;

									BgL_sz00_2740 =
										BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
										(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(42)));
									{	/* Abound/walk.scm 147 */
										obj_t BgL_iz00_2741;

										BgL_iz00_2741 =
											BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
											(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF(29)));
										{	/* Abound/walk.scm 148 */
											obj_t BgL_lz00_2742;

											BgL_lz00_2742 =
												BGl_markzd2symbolzd2nonzd2userz12zc0zzast_identz00
												(BGl_gensymz00zz__r4_symbols_6_4z00(CNST_TABLE_REF
													(30)));
											{	/* Abound/walk.scm 149 */
												obj_t BgL_lnamez00_2743;

												{	/* Abound/walk.scm 150 */
													bool_t BgL_test2258z00_3772;

													{	/* Abound/walk.scm 150 */
														obj_t BgL_arg1978z00_2744;

														BgL_arg1978z00_2744 =
															(((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_nodez00_2739)))->
															BgL_locz00);
														if (STRUCTP(BgL_arg1978z00_2744))
															{	/* Abound/walk.scm 150 */
																BgL_test2258z00_3772 =
																	(STRUCT_KEY(BgL_arg1978z00_2744) ==
																	CNST_TABLE_REF(31));
															}
														else
															{	/* Abound/walk.scm 150 */
																BgL_test2258z00_3772 = ((bool_t) 0);
															}
													}
													if (BgL_test2258z00_3772)
														{	/* Abound/walk.scm 150 */
															obj_t BgL_arg1977z00_2745;

															BgL_arg1977z00_2745 =
																(((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_nodez00_2739)))->
																BgL_locz00);
															BgL_lnamez00_2743 =
																BGl_locationzd2fullzd2fnamez00zztools_locationz00
																(BgL_arg1977z00_2745);
														}
													else
														{	/* Abound/walk.scm 150 */
															BgL_lnamez00_2743 = BFALSE;
														}
												}
												{	/* Abound/walk.scm 150 */
													obj_t BgL_lposz00_2746;

													{	/* Abound/walk.scm 151 */
														bool_t BgL_test2260z00_3783;

														{	/* Abound/walk.scm 151 */
															obj_t BgL_arg1974z00_2747;

															BgL_arg1974z00_2747 =
																(((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt) BgL_nodez00_2739)))->
																BgL_locz00);
															if (STRUCTP(BgL_arg1974z00_2747))
																{	/* Abound/walk.scm 151 */
																	BgL_test2260z00_3783 =
																		(STRUCT_KEY(BgL_arg1974z00_2747) ==
																		CNST_TABLE_REF(31));
																}
															else
																{	/* Abound/walk.scm 151 */
																	BgL_test2260z00_3783 = ((bool_t) 0);
																}
														}
														if (BgL_test2260z00_3783)
															{	/* Abound/walk.scm 151 */
																obj_t BgL_sz00_2748;

																BgL_sz00_2748 =
																	(((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt) BgL_nodez00_2739)))->
																	BgL_locz00);
																BgL_lposz00_2746 =
																	STRUCT_REF(BgL_sz00_2748, (int) (1L));
															}
														else
															{	/* Abound/walk.scm 151 */
																BgL_lposz00_2746 = BFALSE;
															}
													}
													{	/* Abound/walk.scm 151 */
														BgL_variablez00_bglt BgL_vz00_2749;

														BgL_vz00_2749 =
															(((BgL_varz00_bglt) COBJECT(
																	(((BgL_appz00_bglt)
																			COBJECT(BgL_nodez00_2739))->
																		BgL_funz00)))->BgL_variablez00);
														{	/* Abound/walk.scm 152 */
															obj_t BgL_namez00_2750;

															if (
																(((obj_t) BgL_vz00_2749) ==
																	BGl_za2stringzd2refza2zd2zzabound_walkz00))
																{	/* Abound/walk.scm 153 */
																	BgL_namez00_2750 =
																		BGl_string2175z00zzabound_walkz00;
																}
															else
																{	/* Abound/walk.scm 153 */
																	BgL_namez00_2750 =
																		BGl_string2176z00zzabound_walkz00;
																}
															{	/* Abound/walk.scm 153 */
																obj_t BgL_typesz00_2751;

																BgL_typesz00_2751 =
																	(((BgL_cfunz00_bglt) COBJECT(
																			((BgL_cfunz00_bglt)
																				(((BgL_variablez00_bglt)
																						COBJECT(BgL_vz00_2749))->
																					BgL_valuez00))))->BgL_argszd2typezd2);
																{	/* Abound/walk.scm 154 */

																	{	/* Abound/walk.scm 156 */
																		obj_t BgL_arg1906z00_2752;
																		obj_t BgL_arg1910z00_2753;

																		{	/* Abound/walk.scm 156 */
																			obj_t BgL_arg1911z00_2754;

																			{	/* Abound/walk.scm 156 */
																				obj_t BgL_arg1912z00_2755;
																				obj_t BgL_arg1913z00_2756;

																				{	/* Abound/walk.scm 156 */
																					obj_t BgL_arg1914z00_2757;
																					obj_t BgL_arg1916z00_2758;

																					{	/* Abound/walk.scm 156 */
																						obj_t BgL_arg1917z00_2759;
																						obj_t BgL_arg1918z00_2760;

																						{	/* Abound/walk.scm 156 */
																							obj_t BgL_arg1919z00_2761;

																							BgL_arg1919z00_2761 =
																								(((BgL_typez00_bglt) COBJECT(
																										((BgL_typez00_bglt)
																											CAR(
																												((obj_t)
																													BgL_typesz00_2751)))))->
																								BgL_idz00);
																							BgL_arg1917z00_2759 =
																								BGl_makezd2typedzd2identz00zzast_identz00
																								(BgL_sz00_2740,
																								BgL_arg1919z00_2761);
																						}
																						BgL_arg1918z00_2760 =
																							MAKE_YOUNG_PAIR(CAR(
																								(((BgL_appz00_bglt)
																										COBJECT(BgL_nodez00_2739))->
																									BgL_argsz00)), BNIL);
																						BgL_arg1914z00_2757 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1917z00_2759,
																							BgL_arg1918z00_2760);
																					}
																					{	/* Abound/walk.scm 157 */
																						obj_t BgL_arg1925z00_2762;

																						{	/* Abound/walk.scm 157 */
																							obj_t BgL_arg1926z00_2763;
																							obj_t BgL_arg1927z00_2764;

																							{	/* Abound/walk.scm 157 */
																								obj_t BgL_arg1928z00_2765;

																								{
																									BgL_typez00_bglt
																										BgL_auxz00_3812;
																									{	/* Abound/walk.scm 157 */
																										obj_t BgL_pairz00_2766;

																										BgL_pairz00_2766 =
																											CDR(
																											((obj_t)
																												BgL_typesz00_2751));
																										BgL_auxz00_3812 =
																											((BgL_typez00_bglt)
																											CAR(BgL_pairz00_2766));
																									}
																									BgL_arg1928z00_2765 =
																										(((BgL_typez00_bglt)
																											COBJECT
																											(BgL_auxz00_3812))->
																										BgL_idz00);
																								}
																								BgL_arg1926z00_2763 =
																									BGl_makezd2typedzd2identz00zzast_identz00
																									(BgL_iz00_2741,
																									BgL_arg1928z00_2765);
																							}
																							{	/* Abound/walk.scm 157 */
																								obj_t BgL_arg1930z00_2767;

																								{	/* Abound/walk.scm 157 */
																									obj_t BgL_pairz00_2768;

																									BgL_pairz00_2768 =
																										(((BgL_appz00_bglt)
																											COBJECT
																											(BgL_nodez00_2739))->
																										BgL_argsz00);
																									BgL_arg1930z00_2767 =
																										CAR(CDR(BgL_pairz00_2768));
																								}
																								BgL_arg1927z00_2764 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1930z00_2767, BNIL);
																							}
																							BgL_arg1925z00_2762 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1926z00_2763,
																								BgL_arg1927z00_2764);
																						}
																						BgL_arg1916z00_2758 =
																							MAKE_YOUNG_PAIR
																							(BgL_arg1925z00_2762, BNIL);
																					}
																					BgL_arg1912z00_2755 =
																						MAKE_YOUNG_PAIR(BgL_arg1914z00_2757,
																						BgL_arg1916z00_2758);
																				}
																				{	/* Abound/walk.scm 158 */
																					obj_t BgL_arg1932z00_2769;

																					{	/* Abound/walk.scm 158 */
																						obj_t BgL_arg1933z00_2770;

																						{	/* Abound/walk.scm 158 */
																							obj_t BgL_arg1934z00_2771;
																							obj_t BgL_arg1935z00_2772;

																							{	/* Abound/walk.scm 158 */
																								obj_t BgL_arg1936z00_2773;

																								{	/* Abound/walk.scm 158 */
																									obj_t BgL_arg1937z00_2774;
																									obj_t BgL_arg1938z00_2775;

																									{	/* Abound/walk.scm 158 */
																										obj_t BgL_arg1939z00_2776;

																										{
																											BgL_typez00_bglt
																												BgL_auxz00_3826;
																											{	/* Abound/walk.scm 158 */
																												obj_t BgL_pairz00_2777;

																												BgL_pairz00_2777 =
																													CDR(
																													((obj_t)
																														BgL_typesz00_2751));
																												BgL_auxz00_3826 =
																													((BgL_typez00_bglt)
																													CAR
																													(BgL_pairz00_2777));
																											}
																											BgL_arg1939z00_2776 =
																												(((BgL_typez00_bglt)
																													COBJECT
																													(BgL_auxz00_3826))->
																												BgL_idz00);
																										}
																										BgL_arg1937z00_2774 =
																											BGl_makezd2typedzd2identz00zzast_identz00
																											(BgL_lz00_2742,
																											BgL_arg1939z00_2776);
																									}
																									{	/* Abound/walk.scm 159 */
																										obj_t BgL_arg1941z00_2778;

																										{	/* Abound/walk.scm 159 */
																											obj_t BgL_arg1942z00_2779;

																											BgL_arg1942z00_2779 =
																												MAKE_YOUNG_PAIR
																												(BgL_sz00_2740, BNIL);
																											BgL_arg1941z00_2778 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(43),
																												BgL_arg1942z00_2779);
																										}
																										BgL_arg1938z00_2775 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1941z00_2778,
																											BNIL);
																									}
																									BgL_arg1936z00_2773 =
																										MAKE_YOUNG_PAIR
																										(BgL_arg1937z00_2774,
																										BgL_arg1938z00_2775);
																								}
																								BgL_arg1934z00_2771 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1936z00_2773, BNIL);
																							}
																							{	/* Abound/walk.scm 160 */
																								obj_t BgL_arg1943z00_2780;

																								{	/* Abound/walk.scm 160 */
																									obj_t BgL_arg1944z00_2781;

																									{	/* Abound/walk.scm 160 */
																										obj_t BgL_arg1945z00_2782;
																										obj_t BgL_arg1946z00_2783;

																										{	/* Abound/walk.scm 160 */
																											obj_t BgL_arg1947z00_2784;

																											{	/* Abound/walk.scm 160 */
																												obj_t
																													BgL_arg1948z00_2785;
																												BgL_arg1948z00_2785 =
																													MAKE_YOUNG_PAIR
																													(BgL_lz00_2742, BNIL);
																												BgL_arg1947z00_2784 =
																													MAKE_YOUNG_PAIR
																													(BgL_iz00_2741,
																													BgL_arg1948z00_2785);
																											}
																											BgL_arg1945z00_2782 =
																												MAKE_YOUNG_PAIR
																												(CNST_TABLE_REF(44),
																												BgL_arg1947z00_2784);
																										}
																										{	/* Abound/walk.scm 161 */
																											BgL_appz00_bglt
																												BgL_arg1949z00_2786;
																											obj_t BgL_arg1950z00_2787;

																											{	/* Abound/walk.scm 161 */
																												BgL_appz00_bglt
																													BgL_new1113z00_2788;
																												{	/* Abound/walk.scm 162 */
																													BgL_appz00_bglt
																														BgL_new1120z00_2789;
																													BgL_new1120z00_2789 =
																														((BgL_appz00_bglt)
																														BOBJECT(GC_MALLOC
																															(sizeof(struct
																																	BgL_appz00_bgl))));
																													{	/* Abound/walk.scm 162 */
																														long
																															BgL_arg1954z00_2790;
																														BgL_arg1954z00_2790
																															=
																															BGL_CLASS_NUM
																															(BGl_appz00zzast_nodez00);
																														BGL_OBJECT_CLASS_NUM_SET
																															(((BgL_objectz00_bglt) BgL_new1120z00_2789), BgL_arg1954z00_2790);
																													}
																													{	/* Abound/walk.scm 162 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_3847;
																														BgL_tmpz00_3847 =
																															(
																															(BgL_objectz00_bglt)
																															BgL_new1120z00_2789);
																														BGL_OBJECT_WIDENING_SET
																															(BgL_tmpz00_3847,
																															BFALSE);
																													}
																													((BgL_objectz00_bglt)
																														BgL_new1120z00_2789);
																													BgL_new1113z00_2788 =
																														BgL_new1120z00_2789;
																												}
																												((((BgL_nodez00_bglt)
																															COBJECT((
																																	(BgL_nodez00_bglt)
																																	BgL_new1113z00_2788)))->
																														BgL_locz00) =
																													((obj_t) ((
																																(BgL_nodez00_bglt)
																																COBJECT((
																																		(BgL_nodez00_bglt)
																																		BgL_nodez00_2739)))->
																															BgL_locz00)),
																													BUNSPEC);
																												((((BgL_nodez00_bglt)
																															COBJECT((
																																	(BgL_nodez00_bglt)
																																	BgL_new1113z00_2788)))->
																														BgL_typez00) =
																													((BgL_typez00_bglt) ((
																																(BgL_nodez00_bglt)
																																COBJECT((
																																		(BgL_nodez00_bglt)
																																		BgL_nodez00_2739)))->
																															BgL_typez00)),
																													BUNSPEC);
																												((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1113z00_2788)))->BgL_sidezd2effectzd2) = ((obj_t) (((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) BgL_nodez00_2739))))->BgL_sidezd2effectzd2)), BUNSPEC);
																												((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) BgL_new1113z00_2788)))->BgL_keyz00) = ((obj_t) (((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt) BgL_nodez00_2739))))->BgL_keyz00)), BUNSPEC);
																												((((BgL_appz00_bglt)
																															COBJECT
																															(BgL_new1113z00_2788))->
																														BgL_funz00) =
																													((BgL_varz00_bglt) ((
																																(BgL_appz00_bglt)
																																COBJECT((
																																		(BgL_appz00_bglt)
																																		((BgL_nodez00_bglt) BgL_nodez00_2739))))->BgL_funz00)), BUNSPEC);
																												{
																													obj_t BgL_auxz00_3873;

																													{	/* Abound/walk.scm 162 */
																														obj_t
																															BgL_arg1951z00_2791;
																														{	/* Abound/walk.scm 162 */
																															obj_t
																																BgL_arg1952z00_2792;
																															{	/* Abound/walk.scm 162 */
																																obj_t
																																	BgL_pairz00_2793;
																																BgL_pairz00_2793
																																	=
																																	(((BgL_appz00_bglt) COBJECT(BgL_nodez00_2739))->BgL_argsz00);
																																{	/* Abound/walk.scm 162 */
																																	obj_t
																																		BgL_pairz00_2794;
																																	BgL_pairz00_2794
																																		=
																																		CDR
																																		(BgL_pairz00_2793);
																																	BgL_arg1952z00_2792
																																		=
																																		CDR
																																		(BgL_pairz00_2794);
																															}}
																															BgL_arg1951z00_2791
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_iz00_2741,
																																BgL_arg1952z00_2792);
																														}
																														BgL_auxz00_3873 =
																															MAKE_YOUNG_PAIR
																															(BgL_sz00_2740,
																															BgL_arg1951z00_2791);
																													}
																													((((BgL_appz00_bglt)
																																COBJECT
																																(BgL_new1113z00_2788))->
																															BgL_argsz00) =
																														((obj_t)
																															BgL_auxz00_3873),
																														BUNSPEC);
																												}
																												((((BgL_appz00_bglt)
																															COBJECT
																															(BgL_new1113z00_2788))->
																														BgL_stackablez00) =
																													((obj_t) ((
																																(BgL_appz00_bglt)
																																COBJECT((
																																		(BgL_appz00_bglt)
																																		((BgL_nodez00_bglt) BgL_nodez00_2739))))->BgL_stackablez00)), BUNSPEC);
																												BgL_arg1949z00_2786 =
																													BgL_new1113z00_2788;
																											}
																											{	/* Abound/walk.scm 164 */
																												obj_t
																													BgL_arg1955z00_2795;
																												{	/* Abound/walk.scm 164 */
																													obj_t
																														BgL_arg1956z00_2796;
																													{	/* Abound/walk.scm 164 */
																														obj_t
																															BgL_arg1957z00_2797;
																														obj_t
																															BgL_arg1958z00_2798;
																														{	/* Abound/walk.scm 164 */
																															obj_t
																																BgL_arg1959z00_2799;
																															obj_t
																																BgL_arg1960z00_2800;
																															{	/* Abound/walk.scm 164 */
																																obj_t
																																	BgL_arg1961z00_2801;
																																{	/* Abound/walk.scm 164 */
																																	obj_t
																																		BgL_arg1962z00_2802;
																																	BgL_arg1962z00_2802
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(36), BNIL);
																																	BgL_arg1961z00_2801
																																		=
																																		MAKE_YOUNG_PAIR
																																		(CNST_TABLE_REF
																																		(37),
																																		BgL_arg1962z00_2802);
																																}
																																BgL_arg1959z00_2799
																																	=
																																	MAKE_YOUNG_PAIR
																																	(CNST_TABLE_REF
																																	(38),
																																	BgL_arg1961z00_2801);
																															}
																															{	/* Abound/walk.scm 164 */
																																obj_t
																																	BgL_arg1963z00_2803;
																																{	/* Abound/walk.scm 164 */
																																	obj_t
																																		BgL_arg1964z00_2804;
																																	{	/* Abound/walk.scm 164 */
																																		obj_t
																																			BgL_arg1965z00_2805;
																																		{	/* Abound/walk.scm 164 */
																																			obj_t
																																				BgL_arg1966z00_2806;
																																			{	/* Abound/walk.scm 164 */
																																				obj_t
																																					BgL_arg1967z00_2807;
																																				BgL_arg1967z00_2807
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_iz00_2741,
																																					BNIL);
																																				BgL_arg1966z00_2806
																																					=
																																					MAKE_YOUNG_PAIR
																																					(BgL_lz00_2742,
																																					BgL_arg1967z00_2807);
																																			}
																																			BgL_arg1965z00_2805
																																				=
																																				MAKE_YOUNG_PAIR
																																				(BgL_sz00_2740,
																																				BgL_arg1966z00_2806);
																																		}
																																		BgL_arg1964z00_2804
																																			=
																																			MAKE_YOUNG_PAIR
																																			(BgL_namez00_2750,
																																			BgL_arg1965z00_2805);
																																	}
																																	BgL_arg1963z00_2803
																																		=
																																		MAKE_YOUNG_PAIR
																																		(BgL_lposz00_2746,
																																		BgL_arg1964z00_2804);
																																}
																																BgL_arg1960z00_2800
																																	=
																																	MAKE_YOUNG_PAIR
																																	(BgL_lnamez00_2743,
																																	BgL_arg1963z00_2803);
																															}
																															BgL_arg1957z00_2797
																																=
																																MAKE_YOUNG_PAIR
																																(BgL_arg1959z00_2799,
																																BgL_arg1960z00_2800);
																														}
																														{	/* Abound/walk.scm 163 */
																															obj_t
																																BgL_arg1968z00_2808;
																															BgL_arg1968z00_2808
																																=
																																MAKE_YOUNG_PAIR
																																(BFALSE, BNIL);
																															BgL_arg1958z00_2798
																																=
																																MAKE_YOUNG_PAIR
																																(BFALSE,
																																BgL_arg1968z00_2808);
																														}
																														BgL_arg1956z00_2796
																															=
																															MAKE_YOUNG_PAIR
																															(BgL_arg1957z00_2797,
																															BgL_arg1958z00_2798);
																													}
																													BgL_arg1955z00_2795 =
																														MAKE_YOUNG_PAIR
																														(CNST_TABLE_REF(39),
																														BgL_arg1956z00_2796);
																												}
																												BgL_arg1950z00_2787 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1955z00_2795,
																													BNIL);
																											}
																											BgL_arg1946z00_2783 =
																												MAKE_YOUNG_PAIR(
																												((obj_t)
																													BgL_arg1949z00_2786),
																												BgL_arg1950z00_2787);
																										}
																										BgL_arg1944z00_2781 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1945z00_2782,
																											BgL_arg1946z00_2783);
																									}
																									BgL_arg1943z00_2780 =
																										MAKE_YOUNG_PAIR
																										(CNST_TABLE_REF(40),
																										BgL_arg1944z00_2781);
																								}
																								BgL_arg1935z00_2772 =
																									MAKE_YOUNG_PAIR
																									(BgL_arg1943z00_2780, BNIL);
																							}
																							BgL_arg1933z00_2770 =
																								MAKE_YOUNG_PAIR
																								(BgL_arg1934z00_2771,
																								BgL_arg1935z00_2772);
																						}
																						BgL_arg1932z00_2769 =
																							MAKE_YOUNG_PAIR(CNST_TABLE_REF
																							(41), BgL_arg1933z00_2770);
																					}
																					BgL_arg1913z00_2756 =
																						MAKE_YOUNG_PAIR(BgL_arg1932z00_2769,
																						BNIL);
																				}
																				BgL_arg1911z00_2754 =
																					MAKE_YOUNG_PAIR(BgL_arg1912z00_2755,
																					BgL_arg1913z00_2756);
																			}
																			BgL_arg1906z00_2752 =
																				MAKE_YOUNG_PAIR(CNST_TABLE_REF(41),
																				BgL_arg1911z00_2754);
																		}
																		BgL_arg1910z00_2753 =
																			(((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_nodez00_2739)))->BgL_locz00);
																		BgL_nodez00_2810 =
																			BGl_topzd2levelzd2sexpzd2ze3nodez31zzast_sexpz00
																			(BgL_arg1906z00_2752,
																			BgL_arg1910z00_2753);
								}}}}}}}}}}
								BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nodez00_2810);
								return BgL_nodez00_2810;
							}
						else
							{	/* Abound/walk.scm 178 */
								bool_t BgL_test2263z00_3921;

								if (
									(((obj_t) BgL_vz00_2809) ==
										BGl_za2s8vectorzd2refza2zd2zzabound_walkz00))
									{	/* Abound/walk.scm 178 */
										BgL_test2263z00_3921 = ((bool_t) 1);
									}
								else
									{	/* Abound/walk.scm 178 */
										if (
											(((obj_t) BgL_vz00_2809) ==
												BGl_za2s16vectorzd2refza2zd2zzabound_walkz00))
											{	/* Abound/walk.scm 179 */
												BgL_test2263z00_3921 = ((bool_t) 1);
											}
										else
											{	/* Abound/walk.scm 179 */
												if (
													(((obj_t) BgL_vz00_2809) ==
														BGl_za2s32vectorzd2refza2zd2zzabound_walkz00))
													{	/* Abound/walk.scm 180 */
														BgL_test2263z00_3921 = ((bool_t) 1);
													}
												else
													{	/* Abound/walk.scm 180 */
														if (
															(((obj_t) BgL_vz00_2809) ==
																BGl_za2s64vectorzd2refza2zd2zzabound_walkz00))
															{	/* Abound/walk.scm 181 */
																BgL_test2263z00_3921 = ((bool_t) 1);
															}
														else
															{	/* Abound/walk.scm 181 */
																if (
																	(((obj_t) BgL_vz00_2809) ==
																		BGl_za2u8vectorzd2refza2zd2zzabound_walkz00))
																	{	/* Abound/walk.scm 182 */
																		BgL_test2263z00_3921 = ((bool_t) 1);
																	}
																else
																	{	/* Abound/walk.scm 182 */
																		if (
																			(((obj_t) BgL_vz00_2809) ==
																				BGl_za2u16vectorzd2refza2zd2zzabound_walkz00))
																			{	/* Abound/walk.scm 183 */
																				BgL_test2263z00_3921 = ((bool_t) 1);
																			}
																		else
																			{	/* Abound/walk.scm 183 */
																				if (
																					(((obj_t) BgL_vz00_2809) ==
																						BGl_za2u32vectorzd2refza2zd2zzabound_walkz00))
																					{	/* Abound/walk.scm 184 */
																						BgL_test2263z00_3921 = ((bool_t) 1);
																					}
																				else
																					{	/* Abound/walk.scm 184 */
																						if (
																							(((obj_t) BgL_vz00_2809) ==
																								BGl_za2u64vectorzd2refza2zd2zzabound_walkz00))
																							{	/* Abound/walk.scm 185 */
																								BgL_test2263z00_3921 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Abound/walk.scm 185 */
																								if (
																									(((obj_t) BgL_vz00_2809) ==
																										BGl_za2f32vectorzd2refza2zd2zzabound_walkz00))
																									{	/* Abound/walk.scm 186 */
																										BgL_test2263z00_3921 =
																											((bool_t) 1);
																									}
																								else
																									{	/* Abound/walk.scm 186 */
																										BgL_test2263z00_3921 =
																											(
																											((obj_t) BgL_vz00_2809) ==
																											BGl_za2f64vectorzd2refza2zd2zzabound_walkz00);
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
								if (BgL_test2263z00_3921)
									{	/* Abound/walk.scm 189 */
										BgL_nodez00_bglt BgL_arg1853z00_2811;

										{	/* Abound/walk.scm 190 */
											obj_t BgL_arg1854z00_2812;
											obj_t BgL_arg1856z00_2813;
											obj_t BgL_arg1857z00_2814;
											BgL_typez00_bglt BgL_arg1858z00_2815;
											obj_t BgL_arg1859z00_2816;
											obj_t BgL_arg1860z00_2817;

											BgL_arg1854z00_2812 =
												CAR(
												(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_2588)))->
													BgL_argsz00));
											{	/* Abound/walk.scm 190 */
												obj_t BgL_pairz00_2818;

												BgL_pairz00_2818 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_2588)))->
													BgL_argsz00);
												BgL_arg1856z00_2813 = CAR(CDR(BgL_pairz00_2818));
											}
											BgL_arg1857z00_2814 =
												(((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt)
															((BgL_appz00_bglt) BgL_nodez00_2588))))->
												BgL_locz00);
											BgL_arg1858z00_2815 =
												(((BgL_variablez00_bglt)
													COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
																BgL_vz00_2809))))->BgL_typez00);
											{	/* Abound/walk.scm 192 */
												obj_t BgL_pairz00_2819;

												BgL_pairz00_2819 =
													(((BgL_cfunz00_bglt) COBJECT(
															((BgL_cfunz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_globalz00_bglt)
																					BgL_vz00_2809))))->BgL_valuez00))))->
													BgL_argszd2typezd2);
												BgL_arg1859z00_2816 = CAR(CDR(BgL_pairz00_2819));
											}
											{	/* Abound/walk.scm 193 */
												obj_t BgL_pairz00_2820;

												BgL_pairz00_2820 =
													(((BgL_cfunz00_bglt) COBJECT(
															((BgL_cfunz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_globalz00_bglt)
																					BgL_vz00_2809))))->BgL_valuez00))))->
													BgL_argszd2typezd2);
												BgL_arg1860z00_2817 = CAR(BgL_pairz00_2820);
											}
											BgL_arg1853z00_2811 =
												BGl_arrayzd2refzd2zzabound_walkz00(
												((BgL_nodezf2effectzf2_bglt)
													((BgL_appz00_bglt) BgL_nodez00_2588)),
												BgL_arg1854z00_2812, BgL_arg1856z00_2813,
												BgL_arg1857z00_2814, BgL_arg1858z00_2815,
												BgL_arg1859z00_2816, BgL_arg1860z00_2817,
												BGl_proc2177z00zzabound_walkz00);
										}
										return
											BGl_lvtypezd2nodezd2zzast_lvtypez00(BgL_arg1853z00_2811);
									}
								else
									{	/* Abound/walk.scm 197 */
										bool_t BgL_test2273z00_3981;

										if (
											(((obj_t) BgL_vz00_2809) ==
												BGl_za2s8vectorzd2setz12za2zc0zzabound_walkz00))
											{	/* Abound/walk.scm 197 */
												BgL_test2273z00_3981 = ((bool_t) 1);
											}
										else
											{	/* Abound/walk.scm 197 */
												if (
													(((obj_t) BgL_vz00_2809) ==
														BGl_za2s16vectorzd2setz12za2zc0zzabound_walkz00))
													{	/* Abound/walk.scm 198 */
														BgL_test2273z00_3981 = ((bool_t) 1);
													}
												else
													{	/* Abound/walk.scm 198 */
														if (
															(((obj_t) BgL_vz00_2809) ==
																BGl_za2s32vectorzd2setz12za2zc0zzabound_walkz00))
															{	/* Abound/walk.scm 199 */
																BgL_test2273z00_3981 = ((bool_t) 1);
															}
														else
															{	/* Abound/walk.scm 199 */
																if (
																	(((obj_t) BgL_vz00_2809) ==
																		BGl_za2s64vectorzd2setz12za2zc0zzabound_walkz00))
																	{	/* Abound/walk.scm 200 */
																		BgL_test2273z00_3981 = ((bool_t) 1);
																	}
																else
																	{	/* Abound/walk.scm 200 */
																		if (
																			(((obj_t) BgL_vz00_2809) ==
																				BGl_za2u8vectorzd2setz12za2zc0zzabound_walkz00))
																			{	/* Abound/walk.scm 201 */
																				BgL_test2273z00_3981 = ((bool_t) 1);
																			}
																		else
																			{	/* Abound/walk.scm 201 */
																				if (
																					(((obj_t) BgL_vz00_2809) ==
																						BGl_za2u16vectorzd2setz12za2zc0zzabound_walkz00))
																					{	/* Abound/walk.scm 202 */
																						BgL_test2273z00_3981 = ((bool_t) 1);
																					}
																				else
																					{	/* Abound/walk.scm 202 */
																						if (
																							(((obj_t) BgL_vz00_2809) ==
																								BGl_za2u32vectorzd2setz12za2zc0zzabound_walkz00))
																							{	/* Abound/walk.scm 203 */
																								BgL_test2273z00_3981 =
																									((bool_t) 1);
																							}
																						else
																							{	/* Abound/walk.scm 203 */
																								if (
																									(((obj_t) BgL_vz00_2809) ==
																										BGl_za2u64vectorzd2setz12za2zc0zzabound_walkz00))
																									{	/* Abound/walk.scm 204 */
																										BgL_test2273z00_3981 =
																											((bool_t) 1);
																									}
																								else
																									{	/* Abound/walk.scm 204 */
																										if (
																											(((obj_t) BgL_vz00_2809)
																												==
																												BGl_za2f32vectorzd2setz12za2zc0zzabound_walkz00))
																											{	/* Abound/walk.scm 205 */
																												BgL_test2273z00_3981 =
																													((bool_t) 1);
																											}
																										else
																											{	/* Abound/walk.scm 205 */
																												BgL_test2273z00_3981 =
																													(
																													((obj_t)
																														BgL_vz00_2809) ==
																													BGl_za2f64vectorzd2setz12za2zc0zzabound_walkz00);
																											}
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
										if (BgL_test2273z00_3981)
											{	/* Abound/walk.scm 208 */
												BgL_nodez00_bglt BgL_arg1876z00_2822;

												{	/* Abound/walk.scm 209 */
													obj_t BgL_arg1877z00_2823;
													obj_t BgL_arg1878z00_2824;
													obj_t BgL_arg1879z00_2825;
													obj_t BgL_arg1880z00_2826;
													obj_t BgL_arg1882z00_2827;
													obj_t BgL_arg1883z00_2828;

													BgL_arg1877z00_2823 =
														CAR(
														(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_2588)))->
															BgL_argsz00));
													{	/* Abound/walk.scm 209 */
														obj_t BgL_pairz00_2829;

														BgL_pairz00_2829 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_nodez00_2588)))->
															BgL_argsz00);
														BgL_arg1878z00_2824 = CAR(CDR(BgL_pairz00_2829));
													}
													BgL_arg1879z00_2825 =
														(((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt)
																	((BgL_appz00_bglt) BgL_nodez00_2588))))->
														BgL_locz00);
													{	/* Abound/walk.scm 210 */
														obj_t BgL_pairz00_2830;

														BgL_pairz00_2830 =
															(((BgL_cfunz00_bglt) COBJECT(
																	((BgL_cfunz00_bglt)
																		(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_globalz00_bglt)
																							BgL_vz00_2809))))->
																			BgL_valuez00))))->BgL_argszd2typezd2);
														BgL_arg1880z00_2826 =
															CAR(CDR(CDR(BgL_pairz00_2830)));
													}
													{	/* Abound/walk.scm 211 */
														obj_t BgL_pairz00_2831;

														BgL_pairz00_2831 =
															(((BgL_cfunz00_bglt) COBJECT(
																	((BgL_cfunz00_bglt)
																		(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_globalz00_bglt)
																							BgL_vz00_2809))))->
																			BgL_valuez00))))->BgL_argszd2typezd2);
														BgL_arg1882z00_2827 = CAR(CDR(BgL_pairz00_2831));
													}
													{	/* Abound/walk.scm 212 */
														obj_t BgL_pairz00_2832;

														BgL_pairz00_2832 =
															(((BgL_cfunz00_bglt) COBJECT(
																	((BgL_cfunz00_bglt)
																		(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						((BgL_globalz00_bglt)
																							BgL_vz00_2809))))->
																			BgL_valuez00))))->BgL_argszd2typezd2);
														BgL_arg1883z00_2828 = CAR(BgL_pairz00_2832);
													}
													BgL_arg1876z00_2822 =
														BGl_arrayzd2setz12zc0zzabound_walkz00(
														((BgL_nodezf2effectzf2_bglt)
															((BgL_appz00_bglt) BgL_nodez00_2588)),
														BgL_arg1877z00_2823, BgL_arg1878z00_2824,
														BgL_arg1879z00_2825, BgL_arg1880z00_2826,
														BgL_arg1882z00_2827, BgL_arg1883z00_2828,
														BGl_proc2178z00zzabound_walkz00);
												}
												return
													BGl_lvtypezd2nodezd2zzast_lvtypez00
													(BgL_arg1876z00_2822);
											}
										else
											{	/* Abound/walk.scm 197 */
												return
													((BgL_nodez00_bglt)
													((BgL_appz00_bglt) BgL_nodez00_2588));
											}
									}
							}
					}
				}
			}
		}

	}



/* &<@anonymous:1871> */
	BgL_appz00_bglt BGl_z62zc3z04anonymousza31871ze3ze5zzabound_walkz00(obj_t
		BgL_envz00_2589, BgL_nodezf2effectzf2_bglt BgL_nodez00_2590,
		obj_t BgL_vz00_2591, obj_t BgL_iz00_2592)
	{
		{	/* Abound/walk.scm 194 */
			{	/* Abound/walk.scm 195 */
				BgL_appz00_bglt BgL_new1123z00_2837;

				{	/* Abound/walk.scm 195 */
					BgL_appz00_bglt BgL_new1130z00_2838;

					BgL_new1130z00_2838 =
						((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_appz00_bgl))));
					{	/* Abound/walk.scm 195 */
						long BgL_arg1874z00_2839;

						BgL_arg1874z00_2839 = BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1130z00_2838), BgL_arg1874z00_2839);
					}
					{	/* Abound/walk.scm 195 */
						BgL_objectz00_bglt BgL_tmpz00_4052;

						BgL_tmpz00_4052 = ((BgL_objectz00_bglt) BgL_new1130z00_2838);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4052, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1130z00_2838);
					BgL_new1123z00_2837 = BgL_new1130z00_2838;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1123z00_2837)))->BgL_locz00) =
					((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_nodez00_2590)))->BgL_locz00)), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1123z00_2837)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_nodez00_2590)))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1123z00_2837)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2590))))->BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1123z00_2837)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2590))))->BgL_keyz00)), BUNSPEC);
				((((BgL_appz00_bglt) COBJECT(BgL_new1123z00_2837))->BgL_funz00) =
					((BgL_varz00_bglt) (((BgL_appz00_bglt)
								COBJECT(((BgL_appz00_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2590))))->BgL_funz00)), BUNSPEC);
				{
					obj_t BgL_auxz00_4078;

					{	/* Abound/walk.scm 196 */
						obj_t BgL_list1872z00_2840;

						{	/* Abound/walk.scm 196 */
							obj_t BgL_arg1873z00_2841;

							BgL_arg1873z00_2841 = MAKE_YOUNG_PAIR(BgL_iz00_2592, BNIL);
							BgL_list1872z00_2840 =
								MAKE_YOUNG_PAIR(BgL_vz00_2591, BgL_arg1873z00_2841);
						}
						BgL_auxz00_4078 = BgL_list1872z00_2840;
					}
					((((BgL_appz00_bglt) COBJECT(BgL_new1123z00_2837))->BgL_argsz00) =
						((obj_t) BgL_auxz00_4078), BUNSPEC);
				}
				((((BgL_appz00_bglt) COBJECT(BgL_new1123z00_2837))->BgL_stackablez00) =
					((obj_t) (((BgL_appz00_bglt)
								COBJECT(((BgL_appz00_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2590))))->BgL_stackablez00)), BUNSPEC);
				return BgL_new1123z00_2837;
			}
		}

	}



/* &<@anonymous:1894> */
	BgL_appz00_bglt BGl_z62zc3z04anonymousza31894ze3ze5zzabound_walkz00(obj_t
		BgL_envz00_2593, BgL_nodezf2effectzf2_bglt BgL_nodez00_2594,
		obj_t BgL_vecz00_2595, obj_t BgL_iz00_2596)
	{
		{	/* Abound/walk.scm 213 */
			{	/* Abound/walk.scm 215 */
				BgL_appz00_bglt BgL_new1133z00_2845;

				{	/* Abound/walk.scm 215 */
					BgL_appz00_bglt BgL_new1140z00_2846;

					BgL_new1140z00_2846 =
						((BgL_appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_appz00_bgl))));
					{	/* Abound/walk.scm 215 */
						long BgL_arg1902z00_2847;

						BgL_arg1902z00_2847 = BGL_CLASS_NUM(BGl_appz00zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1140z00_2846), BgL_arg1902z00_2847);
					}
					{	/* Abound/walk.scm 215 */
						BgL_objectz00_bglt BgL_tmpz00_4090;

						BgL_tmpz00_4090 = ((BgL_objectz00_bglt) BgL_new1140z00_2846);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4090, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1140z00_2846);
					BgL_new1133z00_2845 = BgL_new1140z00_2846;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1133z00_2845)))->BgL_locz00) =
					((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_nodez00_2594)))->BgL_locz00)), BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1133z00_2845)))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
										BgL_nodez00_2594)))->BgL_typez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1133z00_2845)))->BgL_sidezd2effectzd2) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2594))))->BgL_sidezd2effectzd2)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1133z00_2845)))->BgL_keyz00) =
					((obj_t) (((BgL_nodezf2effectzf2_bglt)
								COBJECT(((BgL_nodezf2effectzf2_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2594))))->BgL_keyz00)), BUNSPEC);
				((((BgL_appz00_bglt) COBJECT(BgL_new1133z00_2845))->BgL_funz00) =
					((BgL_varz00_bglt) (((BgL_appz00_bglt)
								COBJECT(((BgL_appz00_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2594))))->BgL_funz00)), BUNSPEC);
				{
					obj_t BgL_auxz00_4116;

					{	/* Abound/walk.scm 216 */
						obj_t BgL_arg1896z00_2848;

						{	/* Abound/walk.scm 216 */
							obj_t BgL_pairz00_2849;

							BgL_pairz00_2849 =
								(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_2594)))->BgL_argsz00);
							{	/* Abound/walk.scm 216 */
								obj_t BgL_pairz00_2850;

								{	/* Abound/walk.scm 216 */
									obj_t BgL_pairz00_2851;

									BgL_pairz00_2851 = CDR(BgL_pairz00_2849);
									BgL_pairz00_2850 = CDR(BgL_pairz00_2851);
								}
								BgL_arg1896z00_2848 = CAR(BgL_pairz00_2850);
						}}
						{	/* Abound/walk.scm 216 */
							obj_t BgL_list1897z00_2852;

							{	/* Abound/walk.scm 216 */
								obj_t BgL_arg1898z00_2853;

								{	/* Abound/walk.scm 216 */
									obj_t BgL_arg1899z00_2854;

									BgL_arg1899z00_2854 =
										MAKE_YOUNG_PAIR(BgL_arg1896z00_2848, BNIL);
									BgL_arg1898z00_2853 =
										MAKE_YOUNG_PAIR(BgL_iz00_2596, BgL_arg1899z00_2854);
								}
								BgL_list1897z00_2852 =
									MAKE_YOUNG_PAIR(BgL_vecz00_2595, BgL_arg1898z00_2853);
							}
							BgL_auxz00_4116 = BgL_list1897z00_2852;
					}}
					((((BgL_appz00_bglt) COBJECT(BgL_new1133z00_2845))->BgL_argsz00) =
						((obj_t) BgL_auxz00_4116), BUNSPEC);
				}
				((((BgL_appz00_bglt) COBJECT(BgL_new1133z00_2845))->BgL_stackablez00) =
					((obj_t) (((BgL_appz00_bglt)
								COBJECT(((BgL_appz00_bglt) ((BgL_nodez00_bglt)
											BgL_nodez00_2594))))->BgL_stackablez00)), BUNSPEC);
				return BgL_new1133z00_2845;
			}
		}

	}



/* &abound-node-sync1312 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2sync1312z62zzabound_walkz00(obj_t
		BgL_envz00_2597, obj_t BgL_nodez00_2598)
	{
		{	/* Abound/walk.scm 134 */
			{
				BgL_nodez00_bglt BgL_auxz00_4130;

				{	/* Abound/walk.scm 136 */
					BgL_nodez00_bglt BgL_arg1848z00_2856;

					BgL_arg1848z00_2856 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2598)))->BgL_mutexz00);
					BgL_auxz00_4130 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg1848z00_2856);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2598)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4130), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4136;

				{	/* Abound/walk.scm 137 */
					BgL_nodez00_bglt BgL_arg1849z00_2857;

					BgL_arg1849z00_2857 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2598)))->BgL_bodyz00);
					BgL_auxz00_4136 =
						BGl_aboundzd2nodezd2zzabound_walkz00(BgL_arg1849z00_2857);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2598)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4136), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_2598));
		}

	}



/* &abound-node-sequence1310 */
	BgL_nodez00_bglt BGl_z62aboundzd2nodezd2sequence1310z62zzabound_walkz00(obj_t
		BgL_envz00_2599, obj_t BgL_nodez00_2600)
	{
		{	/* Abound/walk.scm 127 */
			BGl_aboundzd2nodeza2z12z62zzabound_walkz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2600)))->BgL_nodesz00));
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_2600));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzabound_walkz00(void)
	{
		{	/* Abound/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			BGl_modulezd2initializa7ationz75zzast_privatez00(135263837L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2179z00zzabound_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
