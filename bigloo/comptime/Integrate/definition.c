/*===========================================================================*/
/*   (Integrate/definition.scm)                                              */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Integrate/definition.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INTEGRATE_DEFINITION_TYPE_DEFINITIONS
#define BGL_INTEGRATE_DEFINITION_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_sfunzf2iinfozf2_bgl
	{
		obj_t BgL_ownerz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
		obj_t BgL_cfromz00;
		obj_t BgL_ctoz00;
		obj_t BgL_kz00;
		obj_t BgL_kza2za2;
		obj_t BgL_uz00;
		obj_t BgL_cnz00;
		obj_t BgL_ctz00;
		obj_t BgL_kontz00;
		bool_t BgL_gzf3zf3;
		bool_t BgL_forcegzf3zf3;
		obj_t BgL_lz00;
		obj_t BgL_ledz00;
		obj_t BgL_istampz00;
		obj_t BgL_globalz00;
		obj_t BgL_kapturedz00;
		obj_t BgL_tailzd2coercionzd2;
		bool_t BgL_xhdlzf3zf3;
		obj_t BgL_xhdlsz00;
	}                      *BgL_sfunzf2iinfozf2_bglt;


#endif													// BGL_INTEGRATE_DEFINITION_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t
		BGl_localzd2ze3globalz31zzintegrate_localzd2ze3globalz31(BgL_localz00_bglt);
	extern obj_t BGl_Kz12z12zzintegrate_kkz00(obj_t, BgL_globalz00_bglt);
	static obj_t BGl_requirezd2initializa7ationz75zzintegrate_definitionz00 =
		BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_setzd2kapturedz12zc0zzintegrate_kapturedz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_integratezd2definitionz12zc0zzintegrate_definitionz00(obj_t);
	static obj_t BGl_genericzd2initzd2zzintegrate_definitionz00(void);
	extern obj_t BGl_Cnz62Ctz12z70zzintegrate_ctnz00(obj_t);
	static obj_t BGl_objectzd2initzd2zzintegrate_definitionz00(void);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t
		BGl_z62integratezd2definitionz12za2zzintegrate_definitionz00(obj_t, obj_t);
	static bool_t BGl_verbzd2globaliza7ationz75zzintegrate_definitionz00(obj_t,
		obj_t);
	extern obj_t BGl_Uz12z12zzintegrate_uz00(void);
	static obj_t BGl_methodzd2initzd2zzintegrate_definitionz00(void);
	extern obj_t
		BGl_integratezd2globaliza7ez12z67zzintegrate_nodez00(BgL_nodez00_bglt,
		BgL_variablez00_bglt, obj_t);
	extern obj_t BGl_Az00zzintegrate_az00(BgL_globalz00_bglt, BgL_nodez00_bglt);
	extern obj_t BGl_za2verboseza2z00zzengine_paramz00;
	extern obj_t BGl_displacezd2letzd2funz12z12zzintegrate_letzd2funzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_definitionz00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_localzd2ze3globalz31(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_nodez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_letzd2funzd2(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_kapturedz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_gz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_ctnz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_uz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_kkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_az00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_infoz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_definitionz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzintegrate_definitionz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzintegrate_definitionz00(void);
	extern obj_t BGl_Kza2z12zb0zzintegrate_kkz00(obj_t);
	extern obj_t BGl_Gz12z12zzintegrate_gz00(obj_t);
	BGL_IMPORT bool_t BGl_2zc3zd3z10zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_za2phiza2z00zzintegrate_az00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1610z00zzintegrate_definitionz00,
		BgL_bgl_string1610za700za7za7i1613za7, " -->", 4);
	      DEFINE_STRING(BGl_string1611z00zzintegrate_definitionz00,
		BgL_bgl_string1611za700za7za7i1614za7, "           ", 11);
	      DEFINE_STRING(BGl_string1612z00zzintegrate_definitionz00,
		BgL_bgl_string1612za700za7za7i1615za7, "integrate_definition", 20);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_integratezd2definitionz12zd2envz12zzintegrate_definitionz00,
		BgL_bgl_za762integrateza7d2d1616z00,
		BGl_z62integratezd2definitionz12za2zzintegrate_definitionz00, 0L, BUNSPEC,
		1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzintegrate_definitionz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_definitionz00(long
		BgL_checksumz00_1924, char *BgL_fromz00_1925)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzintegrate_definitionz00))
				{
					BGl_requirezd2initializa7ationz75zzintegrate_definitionz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzintegrate_definitionz00();
					BGl_libraryzd2moduleszd2initz00zzintegrate_definitionz00();
					BGl_importedzd2moduleszd2initz00zzintegrate_definitionz00();
					return BGl_methodzd2initzd2zzintegrate_definitionz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_definitionz00(void)
	{
		{	/* Integrate/definition.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "integrate_definition");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"integrate_definition");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"integrate_definition");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"integrate_definition");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"integrate_definition");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"integrate_definition");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "integrate_definition");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzintegrate_definitionz00(void)
	{
		{	/* Integrate/definition.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* integrate-definition! */
	BGL_EXPORTED_DEF obj_t
		BGl_integratezd2definitionz12zc0zzintegrate_definitionz00(obj_t
		BgL_globalz00_3)
	{
		{	/* Integrate/definition.scm 58 */
			{	/* Integrate/definition.scm 61 */
				BgL_valuez00_bglt BgL_funz00_1570;

				BgL_funz00_1570 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_globalz00_3))))->BgL_valuez00);
				{	/* Integrate/definition.scm 61 */
					obj_t BgL_bodyz00_1571;

					BgL_bodyz00_1571 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1570)))->BgL_bodyz00);
					{	/* Integrate/definition.scm 62 */
						obj_t BgL_az00_1572;

						BgL_az00_1572 =
							BGl_Az00zzintegrate_az00(
							((BgL_globalz00_bglt) BgL_globalz00_3),
							((BgL_nodez00_bglt) BgL_bodyz00_1571));
						{	/* Integrate/definition.scm 63 */

							{	/* Integrate/definition.scm 65 */
								obj_t BgL_arg1272z00_1573;

								BgL_arg1272z00_1573 =
									BGl_Kz12z12zzintegrate_kkz00(BgL_az00_1572,
									((BgL_globalz00_bglt) BgL_globalz00_3));
								BGl_Kza2z12zb0zzintegrate_kkz00(BgL_arg1272z00_1573);
							}
							BGl_Uz12z12zzintegrate_uz00();
							{	/* Integrate/definition.scm 67 */
								obj_t BgL_gz00_1574;

								BgL_gz00_1574 =
									BGl_Gz12z12zzintegrate_gz00
									(BGl_Cnz62Ctz12z70zzintegrate_ctnz00(BgL_az00_1572));
								if (NULLP(BgL_gz00_1574))
									{	/* Integrate/definition.scm 74 */
										obj_t BgL_list1274z00_1576;

										BgL_list1274z00_1576 =
											MAKE_YOUNG_PAIR(BgL_globalz00_3, BNIL);
										return BgL_list1274z00_1576;
									}
								else
									{	/* Integrate/definition.scm 72 */
										BGl_verbzd2globaliza7ationz75zzintegrate_definitionz00
											(BgL_globalz00_3, BgL_gz00_1574);
										{
											obj_t BgL_l1238z00_1578;

											BgL_l1238z00_1578 = BGl_za2phiza2z00zzintegrate_az00;
										BgL_zc3z04anonymousza31275ze3z87_1579:
											if (PAIRP(BgL_l1238z00_1578))
												{	/* Integrate/definition.scm 78 */
													{	/* Integrate/definition.scm 80 */
														obj_t BgL_fz00_1581;

														BgL_fz00_1581 = CAR(BgL_l1238z00_1578);
														{	/* Integrate/definition.scm 80 */
															bool_t BgL_test1620z00_1962;

															{	/* Integrate/definition.scm 80 */
																bool_t BgL_test1621z00_1963;

																{	/* Integrate/definition.scm 80 */
																	obj_t BgL_classz00_1824;

																	BgL_classz00_1824 = BGl_localz00zzast_varz00;
																	if (BGL_OBJECTP(BgL_fz00_1581))
																		{	/* Integrate/definition.scm 80 */
																			BgL_objectz00_bglt BgL_arg1807z00_1826;

																			BgL_arg1807z00_1826 =
																				(BgL_objectz00_bglt) (BgL_fz00_1581);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Integrate/definition.scm 80 */
																					long BgL_idxz00_1832;

																					BgL_idxz00_1832 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_1826);
																					BgL_test1621z00_1963 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_1832 + 2L)) ==
																						BgL_classz00_1824);
																				}
																			else
																				{	/* Integrate/definition.scm 80 */
																					bool_t BgL_res1608z00_1857;

																					{	/* Integrate/definition.scm 80 */
																						obj_t BgL_oclassz00_1840;

																						{	/* Integrate/definition.scm 80 */
																							obj_t BgL_arg1815z00_1848;
																							long BgL_arg1816z00_1849;

																							BgL_arg1815z00_1848 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Integrate/definition.scm 80 */
																								long BgL_arg1817z00_1850;

																								BgL_arg1817z00_1850 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_1826);
																								BgL_arg1816z00_1849 =
																									(BgL_arg1817z00_1850 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_1840 =
																								VECTOR_REF(BgL_arg1815z00_1848,
																								BgL_arg1816z00_1849);
																						}
																						{	/* Integrate/definition.scm 80 */
																							bool_t BgL__ortest_1115z00_1841;

																							BgL__ortest_1115z00_1841 =
																								(BgL_classz00_1824 ==
																								BgL_oclassz00_1840);
																							if (BgL__ortest_1115z00_1841)
																								{	/* Integrate/definition.scm 80 */
																									BgL_res1608z00_1857 =
																										BgL__ortest_1115z00_1841;
																								}
																							else
																								{	/* Integrate/definition.scm 80 */
																									long BgL_odepthz00_1842;

																									{	/* Integrate/definition.scm 80 */
																										obj_t BgL_arg1804z00_1843;

																										BgL_arg1804z00_1843 =
																											(BgL_oclassz00_1840);
																										BgL_odepthz00_1842 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_1843);
																									}
																									if ((2L < BgL_odepthz00_1842))
																										{	/* Integrate/definition.scm 80 */
																											obj_t BgL_arg1802z00_1845;

																											{	/* Integrate/definition.scm 80 */
																												obj_t
																													BgL_arg1803z00_1846;
																												BgL_arg1803z00_1846 =
																													(BgL_oclassz00_1840);
																												BgL_arg1802z00_1845 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_1846,
																													2L);
																											}
																											BgL_res1608z00_1857 =
																												(BgL_arg1802z00_1845 ==
																												BgL_classz00_1824);
																										}
																									else
																										{	/* Integrate/definition.scm 80 */
																											BgL_res1608z00_1857 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test1621z00_1963 =
																						BgL_res1608z00_1857;
																				}
																		}
																	else
																		{	/* Integrate/definition.scm 80 */
																			BgL_test1621z00_1963 = ((bool_t) 0);
																		}
																}
																if (BgL_test1621z00_1963)
																	{	/* Integrate/definition.scm 80 */
																		bool_t BgL_test1626z00_1986;

																		{	/* Integrate/definition.scm 80 */
																			BgL_sfunz00_bglt BgL_oz00_1859;

																			BgL_oz00_1859 =
																				((BgL_sfunz00_bglt)
																				(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_localz00_bglt)
																									BgL_fz00_1581))))->
																					BgL_valuez00));
																			{
																				BgL_sfunzf2iinfozf2_bglt
																					BgL_auxz00_1991;
																				{
																					obj_t BgL_auxz00_1992;

																					{	/* Integrate/definition.scm 80 */
																						BgL_objectz00_bglt BgL_tmpz00_1993;

																						BgL_tmpz00_1993 =
																							((BgL_objectz00_bglt)
																							BgL_oz00_1859);
																						BgL_auxz00_1992 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_1993);
																					}
																					BgL_auxz00_1991 =
																						((BgL_sfunzf2iinfozf2_bglt)
																						BgL_auxz00_1992);
																				}
																				BgL_test1626z00_1986 =
																					(((BgL_sfunzf2iinfozf2_bglt)
																						COBJECT(BgL_auxz00_1991))->
																					BgL_gzf3zf3);
																			}
																		}
																		if (BgL_test1626z00_1986)
																			{	/* Integrate/definition.scm 80 */
																				BgL_test1620z00_1962 = ((bool_t) 0);
																			}
																		else
																			{	/* Integrate/definition.scm 80 */
																				BgL_test1620z00_1962 = ((bool_t) 1);
																			}
																	}
																else
																	{	/* Integrate/definition.scm 80 */
																		BgL_test1620z00_1962 = ((bool_t) 0);
																	}
															}
															if (BgL_test1620z00_1962)
																{	/* Integrate/definition.scm 81 */
																	obj_t BgL_gz00_1587;

																	{	/* Integrate/definition.scm 81 */
																		BgL_sfunz00_bglt BgL_oz00_1862;

																		BgL_oz00_1862 =
																			((BgL_sfunz00_bglt)
																			(((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							((BgL_localz00_bglt)
																								BgL_fz00_1581))))->
																				BgL_valuez00));
																		{
																			BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2002;

																			{
																				obj_t BgL_auxz00_2003;

																				{	/* Integrate/definition.scm 81 */
																					BgL_objectz00_bglt BgL_tmpz00_2004;

																					BgL_tmpz00_2004 =
																						((BgL_objectz00_bglt)
																						BgL_oz00_1862);
																					BgL_auxz00_2003 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_2004);
																				}
																				BgL_auxz00_2002 =
																					((BgL_sfunzf2iinfozf2_bglt)
																					BgL_auxz00_2003);
																			}
																			BgL_gz00_1587 =
																				(((BgL_sfunzf2iinfozf2_bglt)
																					COBJECT(BgL_auxz00_2002))->BgL_lz00);
																		}
																	}
																	{	/* Integrate/definition.scm 81 */
																		BgL_valuez00_bglt BgL_ifuz00_1588;

																		BgL_ifuz00_1588 =
																			(((BgL_variablez00_bglt) COBJECT(
																					((BgL_variablez00_bglt)
																						BgL_gz00_1587)))->BgL_valuez00);
																		{	/* Integrate/definition.scm 82 */

																			{	/* Integrate/definition.scm 84 */
																				obj_t BgL_arg1306z00_1589;

																				{	/* Integrate/definition.scm 84 */
																					obj_t BgL_arg1307z00_1590;

																					{
																						BgL_sfunzf2iinfozf2_bglt
																							BgL_auxz00_2011;
																						{
																							obj_t BgL_auxz00_2012;

																							{	/* Integrate/definition.scm 84 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_2013;
																								BgL_tmpz00_2013 =
																									((BgL_objectz00_bglt) (
																										(BgL_sfunz00_bglt)
																										BgL_ifuz00_1588));
																								BgL_auxz00_2012 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_2013);
																							}
																							BgL_auxz00_2011 =
																								((BgL_sfunzf2iinfozf2_bglt)
																								BgL_auxz00_2012);
																						}
																						BgL_arg1307z00_1590 =
																							(((BgL_sfunzf2iinfozf2_bglt)
																								COBJECT(BgL_auxz00_2011))->
																							BgL_ledz00);
																					}
																					BgL_arg1306z00_1589 =
																						MAKE_YOUNG_PAIR(BgL_fz00_1581,
																						BgL_arg1307z00_1590);
																				}
																				{
																					BgL_sfunzf2iinfozf2_bglt
																						BgL_auxz00_2020;
																					{
																						obj_t BgL_auxz00_2021;

																						{	/* Integrate/definition.scm 83 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_2022;
																							BgL_tmpz00_2022 =
																								((BgL_objectz00_bglt) (
																									(BgL_sfunz00_bglt)
																									BgL_ifuz00_1588));
																							BgL_auxz00_2021 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_2022);
																						}
																						BgL_auxz00_2020 =
																							((BgL_sfunzf2iinfozf2_bglt)
																							BgL_auxz00_2021);
																					}
																					((((BgL_sfunzf2iinfozf2_bglt)
																								COBJECT(BgL_auxz00_2020))->
																							BgL_ledz00) =
																						((obj_t) BgL_arg1306z00_1589),
																						BUNSPEC);
																				}
																			}
																		}
																	}
																}
															else
																{	/* Integrate/definition.scm 80 */
																	BFALSE;
																}
														}
													}
													{
														obj_t BgL_l1238z00_2028;

														BgL_l1238z00_2028 = CDR(BgL_l1238z00_1578);
														BgL_l1238z00_1578 = BgL_l1238z00_2028;
														goto BgL_zc3z04anonymousza31275ze3z87_1579;
													}
												}
											else
												{	/* Integrate/definition.scm 78 */
													((bool_t) 1);
												}
										}
										{
											obj_t BgL_l1240z00_1598;

											BgL_l1240z00_1598 = BgL_gz00_1574;
										BgL_zc3z04anonymousza31312ze3z87_1599:
											if (PAIRP(BgL_l1240z00_1598))
												{	/* Integrate/definition.scm 88 */
													BGl_displacezd2letzd2funz12z12zzintegrate_letzd2funzd2
														(CAR(BgL_l1240z00_1598));
													{
														obj_t BgL_l1240z00_2034;

														BgL_l1240z00_2034 = CDR(BgL_l1240z00_1598);
														BgL_l1240z00_1598 = BgL_l1240z00_2034;
														goto BgL_zc3z04anonymousza31312ze3z87_1599;
													}
												}
											else
												{	/* Integrate/definition.scm 88 */
													((bool_t) 1);
												}
										}
										BGl_displacezd2letzd2funz12z12zzintegrate_letzd2funzd2
											(BgL_globalz00_3);
										BGl_setzd2kapturedz12zc0zzintegrate_kapturedz00
											(BgL_gz00_1574);
										{	/* Integrate/definition.scm 97 */
											obj_t BgL_newzd2gzd2_1604;

											{	/* Integrate/definition.scm 97 */
												obj_t BgL_head1244z00_1608;

												{	/* Integrate/definition.scm 97 */
													obj_t BgL_arg1323z00_1620;

													{	/* Integrate/definition.scm 97 */
														obj_t BgL_arg1325z00_1621;

														BgL_arg1325z00_1621 = CAR(((obj_t) BgL_gz00_1574));
														BgL_arg1323z00_1620 =
															BGl_localzd2ze3globalz31zzintegrate_localzd2ze3globalz31
															(((BgL_localz00_bglt) BgL_arg1325z00_1621));
													}
													BgL_head1244z00_1608 =
														MAKE_YOUNG_PAIR(BgL_arg1323z00_1620, BNIL);
												}
												{	/* Integrate/definition.scm 97 */
													obj_t BgL_g1247z00_1609;

													BgL_g1247z00_1609 = CDR(((obj_t) BgL_gz00_1574));
													{
														obj_t BgL_l1242z00_1611;
														obj_t BgL_tail1245z00_1612;

														BgL_l1242z00_1611 = BgL_g1247z00_1609;
														BgL_tail1245z00_1612 = BgL_head1244z00_1608;
													BgL_zc3z04anonymousza31318ze3z87_1613:
														if (NULLP(BgL_l1242z00_1611))
															{	/* Integrate/definition.scm 97 */
																BgL_newzd2gzd2_1604 = BgL_head1244z00_1608;
															}
														else
															{	/* Integrate/definition.scm 97 */
																obj_t BgL_newtail1246z00_1615;

																{	/* Integrate/definition.scm 97 */
																	obj_t BgL_arg1321z00_1617;

																	{	/* Integrate/definition.scm 97 */
																		obj_t BgL_arg1322z00_1618;

																		BgL_arg1322z00_1618 =
																			CAR(((obj_t) BgL_l1242z00_1611));
																		BgL_arg1321z00_1617 =
																			BGl_localzd2ze3globalz31zzintegrate_localzd2ze3globalz31
																			(((BgL_localz00_bglt)
																				BgL_arg1322z00_1618));
																	}
																	BgL_newtail1246z00_1615 =
																		MAKE_YOUNG_PAIR(BgL_arg1321z00_1617, BNIL);
																}
																SET_CDR(BgL_tail1245z00_1612,
																	BgL_newtail1246z00_1615);
																{	/* Integrate/definition.scm 97 */
																	obj_t BgL_arg1320z00_1616;

																	BgL_arg1320z00_1616 =
																		CDR(((obj_t) BgL_l1242z00_1611));
																	{
																		obj_t BgL_tail1245z00_2056;
																		obj_t BgL_l1242z00_2055;

																		BgL_l1242z00_2055 = BgL_arg1320z00_1616;
																		BgL_tail1245z00_2056 =
																			BgL_newtail1246z00_1615;
																		BgL_tail1245z00_1612 = BgL_tail1245z00_2056;
																		BgL_l1242z00_1611 = BgL_l1242z00_2055;
																		goto BgL_zc3z04anonymousza31318ze3z87_1613;
																	}
																}
															}
													}
												}
											}
											{	/* Integrate/definition.scm 98 */
												obj_t BgL_arg1316z00_1605;

												BgL_arg1316z00_1605 =
													BGl_integratezd2globaliza7ez12z67zzintegrate_nodez00(
													((BgL_nodez00_bglt) BgL_bodyz00_1571),
													((BgL_variablez00_bglt) BgL_globalz00_3), BNIL);
												((((BgL_sfunz00_bglt) COBJECT(
																((BgL_sfunz00_bglt) BgL_funz00_1570)))->
														BgL_bodyz00) =
													((obj_t) BgL_arg1316z00_1605), BUNSPEC);
											}
											return
												MAKE_YOUNG_PAIR(BgL_globalz00_3, BgL_newzd2gzd2_1604);
										}
									}
							}
						}
					}
				}
			}
		}

	}



/* &integrate-definition! */
	obj_t BGl_z62integratezd2definitionz12za2zzintegrate_definitionz00(obj_t
		BgL_envz00_1922, obj_t BgL_globalz00_1923)
	{
		{	/* Integrate/definition.scm 58 */
			return
				BGl_integratezd2definitionz12zc0zzintegrate_definitionz00
				(BgL_globalz00_1923);
		}

	}



/* verb-globalization */
	bool_t BGl_verbzd2globaliza7ationz75zzintegrate_definitionz00(obj_t
		BgL_globalz00_4, obj_t BgL_gz00_5)
	{
		{	/* Integrate/definition.scm 104 */
			{
				obj_t BgL_l1248z00_1624;

				BgL_l1248z00_1624 = BgL_gz00_5;
			BgL_zc3z04anonymousza31327ze3z87_1625:
				if (PAIRP(BgL_l1248z00_1624))
					{	/* Integrate/definition.scm 105 */
						{	/* Integrate/definition.scm 106 */
							obj_t BgL_localz00_1627;

							BgL_localz00_1627 = CAR(BgL_l1248z00_1624);
							{	/* Integrate/definition.scm 106 */
								bool_t BgL_test1631z00_2067;

								if (INTEGERP(BGl_za2verboseza2z00zzengine_paramz00))
									{	/* Integrate/definition.scm 106 */
										BgL_test1631z00_2067 =
											(3L <=
											(long) CINT(BGl_za2verboseza2z00zzengine_paramz00));
									}
								else
									{	/* Integrate/definition.scm 106 */
										BgL_test1631z00_2067 =
											BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BINT(3L),
											BGl_za2verboseza2z00zzengine_paramz00);
									}
								if (BgL_test1631z00_2067)
									{	/* Integrate/definition.scm 106 */
										bool_t BgL_test1633z00_2074;

										if (INTEGERP(BGl_za2verboseza2z00zzengine_paramz00))
											{	/* Integrate/definition.scm 106 */
												BgL_test1633z00_2074 =
													(3L <=
													(long) CINT(BGl_za2verboseza2z00zzengine_paramz00));
											}
										else
											{	/* Integrate/definition.scm 106 */
												BgL_test1633z00_2074 =
													BGl_2zc3zd3z10zz__r4_numbers_6_5z00(BINT(3L),
													BGl_za2verboseza2z00zzengine_paramz00);
											}
										if (BgL_test1633z00_2074)
											{	/* Integrate/definition.scm 106 */
												obj_t BgL_arg1333z00_1634;

												BgL_arg1333z00_1634 =
													BGl_shapez00zztools_shapez00(BgL_localz00_1627);
												{	/* Integrate/definition.scm 106 */
													obj_t BgL_list1334z00_1635;

													{	/* Integrate/definition.scm 106 */
														obj_t BgL_arg1335z00_1636;

														{	/* Integrate/definition.scm 106 */
															obj_t BgL_arg1339z00_1637;

															{	/* Integrate/definition.scm 106 */
																obj_t BgL_arg1340z00_1638;

																BgL_arg1340z00_1638 =
																	MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)),
																	BNIL);
																BgL_arg1339z00_1637 =
																	MAKE_YOUNG_PAIR
																	(BGl_string1610z00zzintegrate_definitionz00,
																	BgL_arg1340z00_1638);
															}
															BgL_arg1335z00_1636 =
																MAKE_YOUNG_PAIR(BgL_arg1333z00_1634,
																BgL_arg1339z00_1637);
														}
														BgL_list1334z00_1635 =
															MAKE_YOUNG_PAIR
															(BGl_string1611z00zzintegrate_definitionz00,
															BgL_arg1335z00_1636);
													}
													BGl_verbosez00zztools_speekz00(BINT(3L),
														BgL_list1334z00_1635);
											}}
										else
											{	/* Integrate/definition.scm 106 */
												BUNSPEC;
											}
									}
								else
									{	/* Integrate/definition.scm 106 */
										BFALSE;
									}
							}
						}
						{
							obj_t BgL_l1248z00_2089;

							BgL_l1248z00_2089 = CDR(BgL_l1248z00_1624);
							BgL_l1248z00_1624 = BgL_l1248z00_2089;
							goto BgL_zc3z04anonymousza31327ze3z87_1625;
						}
					}
				else
					{	/* Integrate/definition.scm 105 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzintegrate_definitionz00(void)
	{
		{	/* Integrate/definition.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzintegrate_definitionz00(void)
	{
		{	/* Integrate/definition.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzintegrate_definitionz00(void)
	{
		{	/* Integrate/definition.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzintegrate_definitionz00(void)
	{
		{	/* Integrate/definition.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zzintegrate_infoz00(0L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zzintegrate_az00(523633197L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zzintegrate_kkz00(75634339L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zzintegrate_uz00(147062505L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zzintegrate_ctnz00(240374541L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zzintegrate_gz00(52380304L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zzintegrate_kapturedz00(70748975L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zzintegrate_letzd2funzd2(450799956L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			BGl_modulezd2initializa7ationz75zzintegrate_nodez00(347237104L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
			return
				BGl_modulezd2initializa7ationz75zzintegrate_localzd2ze3globalz31
				(143977554L,
				BSTRING_TO_STRING(BGl_string1612z00zzintegrate_definitionz00));
		}

	}

#ifdef __cplusplus
}
#endif
