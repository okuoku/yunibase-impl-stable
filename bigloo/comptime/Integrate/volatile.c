/*===========================================================================*/
/*   (Integrate/volatile.scm)                                                */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Integrate/volatile.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INTEGRATE_VOLATILE_TYPE_DEFINITIONS
#define BGL_INTEGRATE_VOLATILE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;


#endif													// BGL_INTEGRATE_VOLATILE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzintegrate_volatilez00 =
		BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	static obj_t BGl_z62volatilezd2closure1266zb0zzintegrate_volatilez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzintegrate_volatilez00(void);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzintegrate_volatilez00(void);
	static obj_t BGl_usezd2setzd2exitz00zzintegrate_volatilez00(BgL_nodez00_bglt,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzintegrate_volatilez00(void);
	BGL_IMPORT obj_t BGl_appendz00zz__r4_pairs_and_lists_6_3z00(obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	static obj_t BGl_z62usezd2setzd2exit1273z62zzintegrate_volatilez00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_zc3z04anonymousza31381ze3ze70z60zzintegrate_volatilez00(obj_t);
	extern obj_t BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(BgL_nodez00_bglt);
	static obj_t BGl_appendzd221011zd2zzintegrate_volatilez00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzintegrate_volatilez00(void);
	static obj_t BGl_z62volatile1263z62zzintegrate_volatilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62volatilez62zzintegrate_volatilez00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t bgl_append2(obj_t, obj_t);
	extern obj_t BGl_walk1z00zzast_walkz00(BgL_nodez00_bglt, obj_t, obj_t);
	static obj_t BGl_z62volatilezd2letzd2fun1270z62zzintegrate_volatilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_volatilez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzliveness_livenessz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_shrinkifyz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_livenesszd2livezd2zzliveness_livenessz00(BgL_nodez00_bglt);
	static obj_t BGl_z62usezd2setzd2exitz62zzintegrate_volatilez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_volatilez00(void);
	static obj_t
		BGl_z62volatilezd2setzd2exzd2it1272zb0zzintegrate_volatilez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzintegrate_volatilez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzintegrate_volatilez00(void);
	static obj_t BGl_volatilez00zzintegrate_volatilez00(BgL_nodez00_bglt, obj_t);
	extern obj_t
		BGl_livenesszd2sfunz12zc0zzliveness_livenessz00(BgL_sfunz00_bglt);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_globalz00_bglt BGl_z62volatilez12z70zzintegrate_volatilez00(obj_t,
		obj_t);
	static obj_t
		BGl_z62usezd2setzd2exitzd2setzd2exzd21276zb0zzintegrate_volatilez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62volatilezd2letzd2var1268z62zzintegrate_volatilez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_globalz00_bglt
		BGl_volatilez12z12zzintegrate_volatilez00(BgL_globalz00_bglt);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_volatilez12zd2envzc0zzintegrate_volatilez00,
		BgL_bgl_za762volatileza712za771686za7,
		BGl_z62volatilez12z70zzintegrate_volatilez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1675z00zzintegrate_volatilez00,
		BgL_bgl_string1675za700za7za7i1687za7, "volatile1263", 12);
	      DEFINE_STRING(BGl_string1677z00zzintegrate_volatilez00,
		BgL_bgl_string1677za700za7za7i1688za7, "use-set-exit1273", 16);
	      DEFINE_STRING(BGl_string1679z00zzintegrate_volatilez00,
		BgL_bgl_string1679za700za7za7i1689za7, "volatile", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1674z00zzintegrate_volatilez00,
		BgL_bgl_za762volatile1263za71690z00,
		BGl_z62volatile1263z62zzintegrate_volatilez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1676z00zzintegrate_volatilez00,
		BgL_bgl_za762useza7d2setza7d2e1691za7,
		BGl_z62usezd2setzd2exit1273z62zzintegrate_volatilez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1684z00zzintegrate_volatilez00,
		BgL_bgl_string1684za700za7za7i1692za7, "use-set-exit", 12);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1678z00zzintegrate_volatilez00,
		BgL_bgl_za762volatileza7d2cl1693z00,
		BGl_z62volatilezd2closure1266zb0zzintegrate_volatilez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1685z00zzintegrate_volatilez00,
		BgL_bgl_string1685za700za7za7i1694za7, "integrate_volatile", 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1680z00zzintegrate_volatilez00,
		BgL_bgl_za762volatileza7d2le1695z00,
		BGl_z62volatilezd2letzd2var1268z62zzintegrate_volatilez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1681z00zzintegrate_volatilez00,
		BgL_bgl_za762volatileza7d2le1696z00,
		BGl_z62volatilezd2letzd2fun1270z62zzintegrate_volatilez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1682z00zzintegrate_volatilez00,
		BgL_bgl_za762volatileza7d2se1697z00,
		BGl_z62volatilezd2setzd2exzd2it1272zb0zzintegrate_volatilez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1683z00zzintegrate_volatilez00,
		BgL_bgl_za762useza7d2setza7d2e1698za7,
		BGl_z62usezd2setzd2exitzd2setzd2exzd21276zb0zzintegrate_volatilez00, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_GENERIC(BGl_volatilezd2envzd2zzintegrate_volatilez00,
		BgL_bgl_za762volatileza762za7za71699z00,
		BGl_z62volatilez62zzintegrate_volatilez00, 0L, BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_usezd2setzd2exitzd2envzd2zzintegrate_volatilez00,
		BgL_bgl_za762useza7d2setza7d2e1700za7,
		BGl_z62usezd2setzd2exitz62zzintegrate_volatilez00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzintegrate_volatilez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_volatilez00(long
		BgL_checksumz00_1959, char *BgL_fromz00_1960)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzintegrate_volatilez00))
				{
					BGl_requirezd2initializa7ationz75zzintegrate_volatilez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzintegrate_volatilez00();
					BGl_libraryzd2moduleszd2initz00zzintegrate_volatilez00();
					BGl_importedzd2moduleszd2initz00zzintegrate_volatilez00();
					BGl_genericzd2initzd2zzintegrate_volatilez00();
					BGl_methodzd2initzd2zzintegrate_volatilez00();
					return BGl_toplevelzd2initzd2zzintegrate_volatilez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_volatilez00(void)
	{
		{	/* Integrate/volatile.scm 16 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "integrate_volatile");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"integrate_volatile");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"integrate_volatile");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "integrate_volatile");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"integrate_volatile");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"integrate_volatile");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "integrate_volatile");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "integrate_volatile");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzintegrate_volatilez00(void)
	{
		{	/* Integrate/volatile.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzintegrate_volatilez00(void)
	{
		{	/* Integrate/volatile.scm 16 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzintegrate_volatilez00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1362;

				BgL_headz00_1362 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1363;
					obj_t BgL_tailz00_1364;

					BgL_prevz00_1363 = BgL_headz00_1362;
					BgL_tailz00_1364 = BgL_l1z00_1;
				BgL_loopz00_1365:
					if (PAIRP(BgL_tailz00_1364))
						{
							obj_t BgL_newzd2prevzd2_1367;

							BgL_newzd2prevzd2_1367 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1364), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1363, BgL_newzd2prevzd2_1367);
							{
								obj_t BgL_tailz00_1986;
								obj_t BgL_prevz00_1985;

								BgL_prevz00_1985 = BgL_newzd2prevzd2_1367;
								BgL_tailz00_1986 = CDR(BgL_tailz00_1364);
								BgL_tailz00_1364 = BgL_tailz00_1986;
								BgL_prevz00_1363 = BgL_prevz00_1985;
								goto BgL_loopz00_1365;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1362);
				}
			}
		}

	}



/* volatile! */
	BGL_EXPORTED_DEF BgL_globalz00_bglt
		BGl_volatilez12z12zzintegrate_volatilez00(BgL_globalz00_bglt
		BgL_globalz00_3)
	{
		{	/* Integrate/volatile.scm 38 */
			{	/* Integrate/volatile.scm 42 */
				BgL_sfunz00_bglt BgL_i1117z00_1371;

				BgL_i1117z00_1371 =
					((BgL_sfunz00_bglt)
					(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_globalz00_3)))->BgL_valuez00));
				{	/* Integrate/volatile.scm 43 */
					bool_t BgL_test1703z00_1992;

					{	/* Integrate/volatile.scm 43 */
						obj_t BgL_arg1312z00_1379;

						BgL_arg1312z00_1379 =
							(((BgL_sfunz00_bglt) COBJECT(BgL_i1117z00_1371))->BgL_bodyz00);
						{	/* Integrate/volatile.scm 120 */
							obj_t BgL_cellz00_1760;

							BgL_cellz00_1760 = MAKE_YOUNG_CELL(BFALSE);
							BGl_usezd2setzd2exitz00zzintegrate_volatilez00(
								((BgL_nodez00_bglt) BgL_arg1312z00_1379), BgL_cellz00_1760);
							BgL_test1703z00_1992 = CBOOL(CELL_REF(BgL_cellz00_1760));
						}
					}
					if (BgL_test1703z00_1992)
						{	/* Integrate/volatile.scm 44 */
							obj_t BgL_defz00_1374;

							{	/* Integrate/volatile.scm 45 */
								BgL_valuez00_bglt BgL_arg1311z00_1378;

								BgL_arg1311z00_1378 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_globalz00_3)))->BgL_valuez00);
								BgL_defz00_1374 =
									BGl_livenesszd2sfunz12zc0zzliveness_livenessz00(
									((BgL_sfunz00_bglt) BgL_arg1311z00_1378));
							}
							{	/* Integrate/volatile.scm 45 */
								obj_t BgL_usez00_1375;

								{	/* Integrate/volatile.scm 46 */
									obj_t BgL_tmpz00_1762;

									{	/* Integrate/volatile.scm 46 */
										int BgL_tmpz00_2003;

										BgL_tmpz00_2003 = (int) (1L);
										BgL_tmpz00_1762 = BGL_MVALUES_VAL(BgL_tmpz00_2003);
									}
									{	/* Integrate/volatile.scm 46 */
										int BgL_tmpz00_2006;

										BgL_tmpz00_2006 = (int) (1L);
										BGL_MVALUES_VAL_SET(BgL_tmpz00_2006, BUNSPEC);
									}
									BgL_usez00_1375 = BgL_tmpz00_1762;
								}
								{	/* Integrate/volatile.scm 46 */
									obj_t BgL_arg1308z00_1376;

									BgL_arg1308z00_1376 =
										(((BgL_sfunz00_bglt) COBJECT(BgL_i1117z00_1371))->
										BgL_bodyz00);
									BGl_volatilez00zzintegrate_volatilez00(((BgL_nodez00_bglt)
											BgL_arg1308z00_1376), BNIL);
								}
								{	/* Integrate/volatile.scm 47 */
									obj_t BgL_arg1310z00_1377;

									BgL_arg1310z00_1377 =
										(((BgL_sfunz00_bglt) COBJECT(BgL_i1117z00_1371))->
										BgL_bodyz00);
									BGl_shrinkzd2nodez12zc0zzast_shrinkifyz00(((BgL_nodez00_bglt)
											BgL_arg1310z00_1377));
						}}}
					else
						{	/* Integrate/volatile.scm 43 */
							BFALSE;
						}
				}
			}
			return BgL_globalz00_3;
		}

	}



/* &volatile! */
	BgL_globalz00_bglt BGl_z62volatilez12z70zzintegrate_volatilez00(obj_t
		BgL_envz00_1865, obj_t BgL_globalz00_1866)
	{
		{	/* Integrate/volatile.scm 38 */
			return
				BGl_volatilez12z12zzintegrate_volatilez00(
				((BgL_globalz00_bglt) BgL_globalz00_1866));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzintegrate_volatilez00(void)
	{
		{	/* Integrate/volatile.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzintegrate_volatilez00(void)
	{
		{	/* Integrate/volatile.scm 16 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_volatilezd2envzd2zzintegrate_volatilez00,
				BGl_proc1674z00zzintegrate_volatilez00, BGl_nodez00zzast_nodez00,
				BGl_string1675z00zzintegrate_volatilez00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_usezd2setzd2exitzd2envzd2zzintegrate_volatilez00,
				BGl_proc1676z00zzintegrate_volatilez00, BGl_nodez00zzast_nodez00,
				BGl_string1677z00zzintegrate_volatilez00);
		}

	}



/* &use-set-exit1273 */
	obj_t BGl_z62usezd2setzd2exit1273z62zzintegrate_volatilez00(obj_t
		BgL_envz00_1875, obj_t BgL_nodez00_1876, obj_t BgL_cellz00_1877)
	{
		{	/* Integrate/volatile.scm 127 */
			{

				if (CBOOL(CELL_REF(((obj_t) BgL_cellz00_1877))))
					{	/* Integrate/volatile.scm 128 */
						return BFALSE;
					}
				else
					{	/* Integrate/volatile.scm 128 */
						return
							BGl_walk1z00zzast_walkz00(
							((BgL_nodez00_bglt) BgL_nodez00_1876),
							BGl_usezd2setzd2exitzd2envzd2zzintegrate_volatilez00,
							BgL_cellz00_1877);
					}
			}
		}

	}



/* &volatile1263 */
	obj_t BGl_z62volatile1263z62zzintegrate_volatilez00(obj_t BgL_envz00_1878,
		obj_t BgL_nodez00_1879, obj_t BgL_envz00_1880)
	{
		{	/* Integrate/volatile.scm 61 */
			{

				return
					BGl_walk1z00zzast_walkz00(
					((BgL_nodez00_bglt) BgL_nodez00_1879),
					BGl_volatilezd2envzd2zzintegrate_volatilez00, BgL_envz00_1880);
			}
		}

	}



/* volatile */
	obj_t BGl_volatilez00zzintegrate_volatilez00(BgL_nodez00_bglt BgL_nodez00_6,
		obj_t BgL_envz00_7)
	{
		{	/* Integrate/volatile.scm 61 */
			{	/* Integrate/volatile.scm 61 */
				obj_t BgL_method1264z00_1422;

				{	/* Integrate/volatile.scm 61 */
					obj_t BgL_res1668z00_1801;

					{	/* Integrate/volatile.scm 61 */
						long BgL_objzd2classzd2numz00_1772;

						BgL_objzd2classzd2numz00_1772 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_6));
						{	/* Integrate/volatile.scm 61 */
							obj_t BgL_arg1811z00_1773;

							BgL_arg1811z00_1773 =
								PROCEDURE_REF(BGl_volatilezd2envzd2zzintegrate_volatilez00,
								(int) (1L));
							{	/* Integrate/volatile.scm 61 */
								int BgL_offsetz00_1776;

								BgL_offsetz00_1776 = (int) (BgL_objzd2classzd2numz00_1772);
								{	/* Integrate/volatile.scm 61 */
									long BgL_offsetz00_1777;

									BgL_offsetz00_1777 =
										((long) (BgL_offsetz00_1776) - OBJECT_TYPE);
									{	/* Integrate/volatile.scm 61 */
										long BgL_modz00_1778;

										BgL_modz00_1778 =
											(BgL_offsetz00_1777 >> (int) ((long) ((int) (4L))));
										{	/* Integrate/volatile.scm 61 */
											long BgL_restz00_1780;

											BgL_restz00_1780 =
												(BgL_offsetz00_1777 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Integrate/volatile.scm 61 */

												{	/* Integrate/volatile.scm 61 */
													obj_t BgL_bucketz00_1782;

													BgL_bucketz00_1782 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1773), BgL_modz00_1778);
													BgL_res1668z00_1801 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1782), BgL_restz00_1780);
					}}}}}}}}
					BgL_method1264z00_1422 = BgL_res1668z00_1801;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1264z00_1422,
					((obj_t) BgL_nodez00_6), BgL_envz00_7);
			}
		}

	}



/* &volatile */
	obj_t BGl_z62volatilez62zzintegrate_volatilez00(obj_t BgL_envz00_1872,
		obj_t BgL_nodez00_1873, obj_t BgL_envz00_1874)
	{
		{	/* Integrate/volatile.scm 61 */
			return
				BGl_volatilez00zzintegrate_volatilez00(
				((BgL_nodez00_bglt) BgL_nodez00_1873), BgL_envz00_1874);
		}

	}



/* use-set-exit */
	obj_t BGl_usezd2setzd2exitz00zzintegrate_volatilez00(BgL_nodez00_bglt
		BgL_nodez00_17, obj_t BgL_cellz00_18)
	{
		{	/* Integrate/volatile.scm 127 */
			{	/* Integrate/volatile.scm 127 */
				obj_t BgL_method1274z00_1423;

				{	/* Integrate/volatile.scm 127 */
					obj_t BgL_res1673z00_1832;

					{	/* Integrate/volatile.scm 127 */
						long BgL_objzd2classzd2numz00_1803;

						BgL_objzd2classzd2numz00_1803 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_17));
						{	/* Integrate/volatile.scm 127 */
							obj_t BgL_arg1811z00_1804;

							BgL_arg1811z00_1804 =
								PROCEDURE_REF
								(BGl_usezd2setzd2exitzd2envzd2zzintegrate_volatilez00,
								(int) (1L));
							{	/* Integrate/volatile.scm 127 */
								int BgL_offsetz00_1807;

								BgL_offsetz00_1807 = (int) (BgL_objzd2classzd2numz00_1803);
								{	/* Integrate/volatile.scm 127 */
									long BgL_offsetz00_1808;

									BgL_offsetz00_1808 =
										((long) (BgL_offsetz00_1807) - OBJECT_TYPE);
									{	/* Integrate/volatile.scm 127 */
										long BgL_modz00_1809;

										BgL_modz00_1809 =
											(BgL_offsetz00_1808 >> (int) ((long) ((int) (4L))));
										{	/* Integrate/volatile.scm 127 */
											long BgL_restz00_1811;

											BgL_restz00_1811 =
												(BgL_offsetz00_1808 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Integrate/volatile.scm 127 */

												{	/* Integrate/volatile.scm 127 */
													obj_t BgL_bucketz00_1813;

													BgL_bucketz00_1813 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1804), BgL_modz00_1809);
													BgL_res1673z00_1832 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1813), BgL_restz00_1811);
					}}}}}}}}
					BgL_method1274z00_1423 = BgL_res1673z00_1832;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1274z00_1423,
					((obj_t) BgL_nodez00_17), BgL_cellz00_18);
			}
		}

	}



/* &use-set-exit */
	obj_t BGl_z62usezd2setzd2exitz62zzintegrate_volatilez00(obj_t BgL_envz00_1868,
		obj_t BgL_nodez00_1869, obj_t BgL_cellz00_1870)
	{
		{	/* Integrate/volatile.scm 127 */
			return
				BGl_usezd2setzd2exitz00zzintegrate_volatilez00(
				((BgL_nodez00_bglt) BgL_nodez00_1869), BgL_cellz00_1870);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzintegrate_volatilez00(void)
	{
		{	/* Integrate/volatile.scm 16 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_volatilezd2envzd2zzintegrate_volatilez00,
				BGl_closurez00zzast_nodez00, BGl_proc1678z00zzintegrate_volatilez00,
				BGl_string1679z00zzintegrate_volatilez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_volatilezd2envzd2zzintegrate_volatilez00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1680z00zzintegrate_volatilez00,
				BGl_string1679z00zzintegrate_volatilez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_volatilezd2envzd2zzintegrate_volatilez00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1681z00zzintegrate_volatilez00,
				BGl_string1679z00zzintegrate_volatilez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_volatilezd2envzd2zzintegrate_volatilez00,
				BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc1682z00zzintegrate_volatilez00,
				BGl_string1679z00zzintegrate_volatilez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_usezd2setzd2exitzd2envzd2zzintegrate_volatilez00,
				BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc1683z00zzintegrate_volatilez00,
				BGl_string1684z00zzintegrate_volatilez00);
		}

	}



/* &use-set-exit-set-ex-1276 */
	obj_t
		BGl_z62usezd2setzd2exitzd2setzd2exzd21276zb0zzintegrate_volatilez00(obj_t
		BgL_envz00_1886, obj_t BgL_nodez00_1887, obj_t BgL_cellz00_1888)
	{
		{	/* Integrate/volatile.scm 134 */
			return CELL_SET(((obj_t) BgL_cellz00_1888), BTRUE);
		}

	}



/* &volatile-set-ex-it1272 */
	obj_t BGl_z62volatilezd2setzd2exzd2it1272zb0zzintegrate_volatilez00(obj_t
		BgL_envz00_1889, obj_t BgL_nodez00_1890, obj_t BgL_envz00_1891)
	{
		{	/* Integrate/volatile.scm 106 */
			{	/* Integrate/volatile.scm 107 */
				obj_t BgL_livez00_1907;

				BgL_livez00_1907 =
					BGl_livenesszd2livezd2zzliveness_livenessz00(
					((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_1890)));
				{
					obj_t BgL_l1261z00_1909;

					BgL_l1261z00_1909 = BgL_livez00_1907;
				BgL_zc3z04anonymousza31426ze3z87_1908:
					if (PAIRP(BgL_l1261z00_1909))
						{	/* Integrate/volatile.scm 108 */
							{	/* Integrate/volatile.scm 109 */
								obj_t BgL_lz00_1910;

								BgL_lz00_1910 = CAR(BgL_l1261z00_1909);
								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
										(BgL_lz00_1910, BgL_envz00_1891)))
									{	/* Integrate/volatile.scm 109 */
										((((BgL_localz00_bglt) COBJECT(
														((BgL_localz00_bglt) BgL_lz00_1910)))->
												BgL_volatilez00) = ((bool_t) ((bool_t) 1)), BUNSPEC);
									}
								else
									{	/* Integrate/volatile.scm 109 */
										BFALSE;
									}
							}
							{
								obj_t BgL_l1261z00_2111;

								BgL_l1261z00_2111 = CDR(BgL_l1261z00_1909);
								BgL_l1261z00_1909 = BgL_l1261z00_2111;
								goto BgL_zc3z04anonymousza31426ze3z87_1908;
							}
						}
					else
						{	/* Integrate/volatile.scm 108 */
							((bool_t) 1);
						}
				}
				return
					BGl_volatilez00zzintegrate_volatilez00(
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_1890)))->BgL_bodyz00),
					BgL_envz00_1891);
			}
		}

	}



/* &volatile-let-fun1270 */
	obj_t BGl_z62volatilezd2letzd2fun1270z62zzintegrate_volatilez00(obj_t
		BgL_envz00_1892, obj_t BgL_nodez00_1893, obj_t BgL_envz00_1894)
	{
		{	/* Integrate/volatile.scm 89 */
			{	/* Integrate/volatile.scm 91 */
				obj_t BgL_g1257z00_1912;

				BgL_g1257z00_1912 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_1893)))->BgL_localsz00);
				{
					obj_t BgL_l1255z00_1914;

					BgL_l1255z00_1914 = BgL_g1257z00_1912;
				BgL_zc3z04anonymousza31365ze3z87_1913:
					if (PAIRP(BgL_l1255z00_1914))
						{	/* Integrate/volatile.scm 91 */
							{	/* Integrate/volatile.scm 92 */
								obj_t BgL_lz00_1915;

								BgL_lz00_1915 = CAR(BgL_l1255z00_1914);
								{	/* Integrate/volatile.scm 93 */
									BgL_valuez00_bglt BgL_arg1367z00_1916;

									BgL_arg1367z00_1916 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_lz00_1915))))->BgL_valuez00);
									{	/* Integrate/volatile.scm 56 */
										obj_t BgL_arg1314z00_1917;
										obj_t BgL_arg1315z00_1918;

										BgL_arg1314z00_1917 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_arg1367z00_1916)))->
											BgL_bodyz00);
										{	/* Integrate/volatile.scm 56 */
											obj_t BgL_arg1316z00_1919;

											BgL_arg1316z00_1919 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_arg1367z00_1916)))->
												BgL_argsz00);
											BgL_arg1315z00_1918 =
												BGl_appendzd221011zd2zzintegrate_volatilez00
												(BgL_arg1316z00_1919, BgL_envz00_1894);
										}
										BGl_volatilez00zzintegrate_volatilez00(
											((BgL_nodez00_bglt) BgL_arg1314z00_1917),
											BgL_arg1315z00_1918);
									}
								}
							}
							{
								obj_t BgL_l1255z00_2131;

								BgL_l1255z00_2131 = CDR(BgL_l1255z00_1914);
								BgL_l1255z00_1914 = BgL_l1255z00_2131;
								goto BgL_zc3z04anonymousza31365ze3z87_1913;
							}
						}
					else
						{	/* Integrate/volatile.scm 91 */
							((bool_t) 1);
						}
				}
			}
			{	/* Integrate/volatile.scm 95 */
				BgL_nodez00_bglt BgL_arg1371z00_1920;
				obj_t BgL_arg1375z00_1921;

				BgL_arg1371z00_1920 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_1893)))->BgL_bodyz00);
				{	/* Integrate/volatile.scm 96 */
					obj_t BgL_arg1376z00_1922;
					obj_t BgL_arg1377z00_1923;

					BgL_arg1376z00_1922 =
						BGl_zc3z04anonymousza31381ze3ze70z60zzintegrate_volatilez00(
						(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nodez00_1893)))->BgL_localsz00));
					BgL_arg1377z00_1923 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_1893)))->BgL_localsz00);
					{	/* Integrate/volatile.scm 96 */
						obj_t BgL_list1378z00_1924;

						{	/* Integrate/volatile.scm 96 */
							obj_t BgL_arg1379z00_1925;

							{	/* Integrate/volatile.scm 96 */
								obj_t BgL_arg1380z00_1926;

								BgL_arg1380z00_1926 = MAKE_YOUNG_PAIR(BgL_envz00_1894, BNIL);
								BgL_arg1379z00_1925 =
									MAKE_YOUNG_PAIR(BgL_arg1377z00_1923, BgL_arg1380z00_1926);
							}
							BgL_list1378z00_1924 =
								MAKE_YOUNG_PAIR(BgL_arg1376z00_1922, BgL_arg1379z00_1925);
						}
						BgL_arg1375z00_1921 =
							BGl_appendz00zz__r4_pairs_and_lists_6_3z00(BgL_list1378z00_1924);
					}
				}
				return
					BGl_volatilez00zzintegrate_volatilez00(BgL_arg1371z00_1920,
					BgL_arg1375z00_1921);
			}
		}

	}



/* <@anonymous:1381>~0 */
	obj_t BGl_zc3z04anonymousza31381ze3ze70z60zzintegrate_volatilez00(obj_t
		BgL_l1259z00_1528)
	{
		{	/* Integrate/volatile.scm 96 */
			if (NULLP(BgL_l1259z00_1528))
				{	/* Integrate/volatile.scm 96 */
					return BNIL;
				}
			else
				{	/* Integrate/volatile.scm 97 */
					obj_t BgL_arg1408z00_1531;
					obj_t BgL_arg1410z00_1532;

					BgL_arg1408z00_1531 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt)
														CAR(
															((obj_t) BgL_l1259z00_1528))))))->
										BgL_valuez00))))->BgL_argsz00);
					{	/* Integrate/volatile.scm 96 */
						obj_t BgL_arg1422z00_1536;

						BgL_arg1422z00_1536 = CDR(((obj_t) BgL_l1259z00_1528));
						BgL_arg1410z00_1532 =
							BGl_zc3z04anonymousza31381ze3ze70z60zzintegrate_volatilez00
							(BgL_arg1422z00_1536);
					}
					return bgl_append2(BgL_arg1408z00_1531, BgL_arg1410z00_1532);
				}
		}

	}



/* &volatile-let-var1268 */
	obj_t BGl_z62volatilezd2letzd2var1268z62zzintegrate_volatilez00(obj_t
		BgL_envz00_1895, obj_t BgL_nodez00_1896, obj_t BgL_envz00_1897)
	{
		{	/* Integrate/volatile.scm 75 */
			{	/* Integrate/volatile.scm 77 */
				obj_t BgL_g1245z00_1928;

				BgL_g1245z00_1928 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_1896)))->BgL_bindingsz00);
				{
					obj_t BgL_l1243z00_1930;

					BgL_l1243z00_1930 = BgL_g1245z00_1928;
				BgL_zc3z04anonymousza31330ze3z87_1929:
					if (PAIRP(BgL_l1243z00_1930))
						{	/* Integrate/volatile.scm 77 */
							{	/* Integrate/volatile.scm 78 */
								obj_t BgL_bz00_1931;

								BgL_bz00_1931 = CAR(BgL_l1243z00_1930);
								{	/* Integrate/volatile.scm 78 */
									BgL_localz00_bglt BgL_i1122z00_1932;

									BgL_i1122z00_1932 =
										((BgL_localz00_bglt) CAR(((obj_t) BgL_bz00_1931)));
									((((BgL_localz00_bglt) COBJECT(BgL_i1122z00_1932))->
											BgL_volatilez00) = ((bool_t) ((bool_t) 0)), BUNSPEC);
								}
							}
							{
								obj_t BgL_l1243z00_2167;

								BgL_l1243z00_2167 = CDR(BgL_l1243z00_1930);
								BgL_l1243z00_1930 = BgL_l1243z00_2167;
								goto BgL_zc3z04anonymousza31330ze3z87_1929;
							}
						}
					else
						{	/* Integrate/volatile.scm 77 */
							((bool_t) 1);
						}
				}
			}
			{	/* Integrate/volatile.scm 81 */
				obj_t BgL_g1248z00_1933;

				BgL_g1248z00_1933 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_1896)))->BgL_bindingsz00);
				{
					obj_t BgL_l1246z00_1935;

					BgL_l1246z00_1935 = BgL_g1248z00_1933;
				BgL_zc3z04anonymousza31333ze3z87_1934:
					if (PAIRP(BgL_l1246z00_1935))
						{	/* Integrate/volatile.scm 81 */
							{	/* Integrate/volatile.scm 82 */
								obj_t BgL_bz00_1936;

								BgL_bz00_1936 = CAR(BgL_l1246z00_1935);
								{	/* Integrate/volatile.scm 82 */
									obj_t BgL_arg1335z00_1937;

									BgL_arg1335z00_1937 = CDR(((obj_t) BgL_bz00_1936));
									BGl_volatilez00zzintegrate_volatilez00(
										((BgL_nodez00_bglt) BgL_arg1335z00_1937), BgL_envz00_1897);
								}
							}
							{
								obj_t BgL_l1246z00_2178;

								BgL_l1246z00_2178 = CDR(BgL_l1246z00_1935);
								BgL_l1246z00_1935 = BgL_l1246z00_2178;
								goto BgL_zc3z04anonymousza31333ze3z87_1934;
							}
						}
					else
						{	/* Integrate/volatile.scm 81 */
							((bool_t) 1);
						}
				}
			}
			{	/* Integrate/volatile.scm 84 */
				BgL_nodez00_bglt BgL_arg1340z00_1938;
				obj_t BgL_arg1342z00_1939;

				BgL_arg1340z00_1938 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_1896)))->BgL_bodyz00);
				{	/* Integrate/volatile.scm 84 */
					obj_t BgL_arg1343z00_1940;

					{	/* Integrate/volatile.scm 84 */
						obj_t BgL_l1249z00_1941;

						BgL_l1249z00_1941 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_1896)))->
							BgL_bindingsz00);
						if (NULLP(BgL_l1249z00_1941))
							{	/* Integrate/volatile.scm 84 */
								BgL_arg1343z00_1940 = BNIL;
							}
						else
							{	/* Integrate/volatile.scm 84 */
								obj_t BgL_head1251z00_1942;

								{	/* Integrate/volatile.scm 84 */
									obj_t BgL_arg1352z00_1943;

									{	/* Integrate/volatile.scm 84 */
										obj_t BgL_pairz00_1944;

										BgL_pairz00_1944 = CAR(((obj_t) BgL_l1249z00_1941));
										BgL_arg1352z00_1943 = CAR(BgL_pairz00_1944);
									}
									BgL_head1251z00_1942 =
										MAKE_YOUNG_PAIR(BgL_arg1352z00_1943, BNIL);
								}
								{	/* Integrate/volatile.scm 84 */
									obj_t BgL_g1254z00_1945;

									BgL_g1254z00_1945 = CDR(((obj_t) BgL_l1249z00_1941));
									{
										obj_t BgL_l1249z00_1947;
										obj_t BgL_tail1252z00_1948;

										BgL_l1249z00_1947 = BgL_g1254z00_1945;
										BgL_tail1252z00_1948 = BgL_head1251z00_1942;
									BgL_zc3z04anonymousza31345ze3z87_1946:
										if (NULLP(BgL_l1249z00_1947))
											{	/* Integrate/volatile.scm 84 */
												BgL_arg1343z00_1940 = BgL_head1251z00_1942;
											}
										else
											{	/* Integrate/volatile.scm 84 */
												obj_t BgL_newtail1253z00_1949;

												{	/* Integrate/volatile.scm 84 */
													obj_t BgL_arg1349z00_1950;

													{	/* Integrate/volatile.scm 84 */
														obj_t BgL_pairz00_1951;

														BgL_pairz00_1951 = CAR(((obj_t) BgL_l1249z00_1947));
														BgL_arg1349z00_1950 = CAR(BgL_pairz00_1951);
													}
													BgL_newtail1253z00_1949 =
														MAKE_YOUNG_PAIR(BgL_arg1349z00_1950, BNIL);
												}
												SET_CDR(BgL_tail1252z00_1948, BgL_newtail1253z00_1949);
												{	/* Integrate/volatile.scm 84 */
													obj_t BgL_arg1348z00_1952;

													BgL_arg1348z00_1952 =
														CDR(((obj_t) BgL_l1249z00_1947));
													{
														obj_t BgL_tail1252z00_2202;
														obj_t BgL_l1249z00_2201;

														BgL_l1249z00_2201 = BgL_arg1348z00_1952;
														BgL_tail1252z00_2202 = BgL_newtail1253z00_1949;
														BgL_tail1252z00_1948 = BgL_tail1252z00_2202;
														BgL_l1249z00_1947 = BgL_l1249z00_2201;
														goto BgL_zc3z04anonymousza31345ze3z87_1946;
													}
												}
											}
									}
								}
							}
					}
					BgL_arg1342z00_1939 =
						BGl_appendzd221011zd2zzintegrate_volatilez00(BgL_arg1343z00_1940,
						BgL_envz00_1897);
				}
				return
					BGl_volatilez00zzintegrate_volatilez00(BgL_arg1340z00_1938,
					BgL_arg1342z00_1939);
			}
		}

	}



/* &volatile-closure1266 */
	obj_t BGl_z62volatilezd2closure1266zb0zzintegrate_volatilez00(obj_t
		BgL_envz00_1898, obj_t BgL_nodez00_1899, obj_t BgL_envz00_1900)
	{
		{	/* Integrate/volatile.scm 67 */
			{	/* Integrate/volatile.scm 69 */
				BgL_variablez00_bglt BgL_i1120z00_1954;

				BgL_i1120z00_1954 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt)
								((BgL_closurez00_bglt) BgL_nodez00_1899))))->BgL_variablez00);
				{	/* Integrate/volatile.scm 70 */
					BgL_valuez00_bglt BgL_arg1326z00_1955;

					BgL_arg1326z00_1955 =
						(((BgL_variablez00_bglt) COBJECT(BgL_i1120z00_1954))->BgL_valuez00);
					{	/* Integrate/volatile.scm 56 */
						obj_t BgL_arg1314z00_1956;
						obj_t BgL_arg1315z00_1957;

						BgL_arg1314z00_1956 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_arg1326z00_1955)))->BgL_bodyz00);
						{	/* Integrate/volatile.scm 56 */
							obj_t BgL_arg1316z00_1958;

							BgL_arg1316z00_1958 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt) BgL_arg1326z00_1955)))->BgL_argsz00);
							BgL_arg1315z00_1957 =
								BGl_appendzd221011zd2zzintegrate_volatilez00
								(BgL_arg1316z00_1958, BgL_envz00_1900);
						}
						return
							BGl_volatilez00zzintegrate_volatilez00(
							((BgL_nodez00_bglt) BgL_arg1314z00_1956), BgL_arg1315z00_1957);
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzintegrate_volatilez00(void)
	{
		{	/* Integrate/volatile.scm 16 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1685z00zzintegrate_volatilez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1685z00zzintegrate_volatilez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1685z00zzintegrate_volatilez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1685z00zzintegrate_volatilez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1685z00zzintegrate_volatilez00));
			BGl_modulezd2initializa7ationz75zzast_walkz00(343174225L,
				BSTRING_TO_STRING(BGl_string1685z00zzintegrate_volatilez00));
			BGl_modulezd2initializa7ationz75zzast_shrinkifyz00(45200572L,
				BSTRING_TO_STRING(BGl_string1685z00zzintegrate_volatilez00));
			BGl_modulezd2initializa7ationz75zzliveness_livenessz00(424064075L,
				BSTRING_TO_STRING(BGl_string1685z00zzintegrate_volatilez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1685z00zzintegrate_volatilez00));
			return
				BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1685z00zzintegrate_volatilez00));
		}

	}

#ifdef __cplusplus
}
#endif
