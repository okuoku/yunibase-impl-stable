/*===========================================================================*/
/*   (Integrate/kk.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Integrate/kk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INTEGRATE_KK_TYPE_DEFINITIONS
#define BGL_INTEGRATE_KK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_sfunzf2iinfozf2_bgl
	{
		obj_t BgL_ownerz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
		obj_t BgL_cfromz00;
		obj_t BgL_ctoz00;
		obj_t BgL_kz00;
		obj_t BgL_kza2za2;
		obj_t BgL_uz00;
		obj_t BgL_cnz00;
		obj_t BgL_ctz00;
		obj_t BgL_kontz00;
		bool_t BgL_gzf3zf3;
		bool_t BgL_forcegzf3zf3;
		obj_t BgL_lz00;
		obj_t BgL_ledz00;
		obj_t BgL_istampz00;
		obj_t BgL_globalz00;
		obj_t BgL_kapturedz00;
		obj_t BgL_tailzd2coercionzd2;
		bool_t BgL_xhdlzf3zf3;
		obj_t BgL_xhdlsz00;
	}                      *BgL_sfunzf2iinfozf2_bglt;


#endif													// BGL_INTEGRATE_KK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t BGl_Kz12z12zzintegrate_kkz00(obj_t,
		BgL_globalz00_bglt);
	static obj_t BGl_requirezd2initializa7ationz75zzintegrate_kkz00 = BUNSPEC;
	static obj_t BGl_genericzd2initzd2zzintegrate_kkz00(void);
	static obj_t BGl_objectzd2initzd2zzintegrate_kkz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzintegrate_kkz00(void);
	static obj_t BGl_Kzd21z12zc0zzintegrate_kkz00(obj_t, obj_t);
	static obj_t BGl_Kzd22z12zc0zzintegrate_kkz00(obj_t, obj_t);
	static obj_t BGl_z62Kz12z70zzintegrate_kkz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_kkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_az00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_infoz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_z62Kza2z12zd2zzintegrate_kkz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzintegrate_kkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_kkz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzintegrate_kkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzintegrate_kkz00(void);
	BGL_EXPORTED_DECL obj_t BGl_Kza2z12zb0zzintegrate_kkz00(obj_t);
	static obj_t __cnst[2];


	   
		 
		DEFINE_STRING(BGl_string1666z00zzintegrate_kkz00,
		BgL_bgl_string1666za700za7za7i1676za7, "integrate_kk", 12);
	      DEFINE_STRING(BGl_string1667z00zzintegrate_kkz00,
		BgL_bgl_string1667za700za7za7i1677za7, "tail bottom ", 12);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_Kz12zd2envzc0zzintegrate_kkz00,
		BgL_bgl_za762kza712za770za7za7inte1678za7, BGl_z62Kz12z70zzintegrate_kkz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_Kza2z12zd2envz62zzintegrate_kkz00,
		BgL_bgl_za762kza7a2za712za7d2za7za7i1679z00,
		BGl_z62Kza2z12zd2zzintegrate_kkz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzintegrate_kkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzintegrate_kkz00(long
		BgL_checksumz00_1953, char *BgL_fromz00_1954)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzintegrate_kkz00))
				{
					BGl_requirezd2initializa7ationz75zzintegrate_kkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzintegrate_kkz00();
					BGl_libraryzd2moduleszd2initz00zzintegrate_kkz00();
					BGl_cnstzd2initzd2zzintegrate_kkz00();
					BGl_importedzd2moduleszd2initz00zzintegrate_kkz00();
					return BGl_methodzd2initzd2zzintegrate_kkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_kkz00(void)
	{
		{	/* Integrate/kk.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"integrate_kk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "integrate_kk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"integrate_kk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "integrate_kk");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "integrate_kk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzintegrate_kkz00(void)
	{
		{	/* Integrate/kk.scm 15 */
			{	/* Integrate/kk.scm 15 */
				obj_t BgL_cportz00_1942;

				{	/* Integrate/kk.scm 15 */
					obj_t BgL_stringz00_1949;

					BgL_stringz00_1949 = BGl_string1667z00zzintegrate_kkz00;
					{	/* Integrate/kk.scm 15 */
						obj_t BgL_startz00_1950;

						BgL_startz00_1950 = BINT(0L);
						{	/* Integrate/kk.scm 15 */
							obj_t BgL_endz00_1951;

							BgL_endz00_1951 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1949)));
							{	/* Integrate/kk.scm 15 */

								BgL_cportz00_1942 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1949, BgL_startz00_1950, BgL_endz00_1951);
				}}}}
				{
					long BgL_iz00_1943;

					BgL_iz00_1943 = 1L;
				BgL_loopz00_1944:
					if ((BgL_iz00_1943 == -1L))
						{	/* Integrate/kk.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Integrate/kk.scm 15 */
							{	/* Integrate/kk.scm 15 */
								obj_t BgL_arg1675z00_1945;

								{	/* Integrate/kk.scm 15 */

									{	/* Integrate/kk.scm 15 */
										obj_t BgL_locationz00_1947;

										BgL_locationz00_1947 = BBOOL(((bool_t) 0));
										{	/* Integrate/kk.scm 15 */

											BgL_arg1675z00_1945 =
												BGl_readz00zz__readerz00(BgL_cportz00_1942,
												BgL_locationz00_1947);
										}
									}
								}
								{	/* Integrate/kk.scm 15 */
									int BgL_tmpz00_1977;

									BgL_tmpz00_1977 = (int) (BgL_iz00_1943);
									CNST_TABLE_SET(BgL_tmpz00_1977, BgL_arg1675z00_1945);
							}}
							{	/* Integrate/kk.scm 15 */
								int BgL_auxz00_1948;

								BgL_auxz00_1948 = (int) ((BgL_iz00_1943 - 1L));
								{
									long BgL_iz00_1982;

									BgL_iz00_1982 = (long) (BgL_auxz00_1948);
									BgL_iz00_1943 = BgL_iz00_1982;
									goto BgL_loopz00_1944;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzintegrate_kkz00(void)
	{
		{	/* Integrate/kk.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* K! */
	BGL_EXPORTED_DEF obj_t BGl_Kz12z12zzintegrate_kkz00(obj_t BgL_az00_3,
		BgL_globalz00_bglt BgL_varz00_4)
	{
		{	/* Integrate/kk.scm 44 */
			{	/* Integrate/kk.scm 48 */
				BgL_valuez00_bglt BgL_ifunz00_1574;

				BgL_ifunz00_1574 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_4)))->BgL_valuez00);
				{	/* Integrate/kk.scm 49 */
					obj_t BgL_arg1248z00_1575;

					{	/* Integrate/kk.scm 49 */
						obj_t BgL_list1249z00_1576;

						BgL_list1249z00_1576 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
						BgL_arg1248z00_1575 = BgL_list1249z00_1576;
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_1989;

						{
							obj_t BgL_auxz00_1990;

							{	/* Integrate/kk.scm 49 */
								BgL_objectz00_bglt BgL_tmpz00_1991;

								BgL_tmpz00_1991 =
									((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_ifunz00_1574));
								BgL_auxz00_1990 = BGL_OBJECT_WIDENING(BgL_tmpz00_1991);
							}
							BgL_auxz00_1989 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_1990);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_1989))->BgL_kz00) =
							((obj_t) BgL_arg1248z00_1575), BUNSPEC);
					}
				}
				{	/* Integrate/kk.scm 50 */
					obj_t BgL_arg1252z00_1577;

					{	/* Integrate/kk.scm 50 */
						obj_t BgL_list1253z00_1578;

						BgL_list1253z00_1578 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BNIL);
						BgL_arg1252z00_1577 = BgL_list1253z00_1578;
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_1999;

						{
							obj_t BgL_auxz00_2000;

							{	/* Integrate/kk.scm 50 */
								BgL_objectz00_bglt BgL_tmpz00_2001;

								BgL_tmpz00_2001 =
									((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_ifunz00_1574));
								BgL_auxz00_2000 = BGL_OBJECT_WIDENING(BgL_tmpz00_2001);
							}
							BgL_auxz00_1999 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2000);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_1999))->
								BgL_kza2za2) = ((obj_t) BgL_arg1252z00_1577), BUNSPEC);
					}
				}
				return
					BGl_Kzd22z12zc0zzintegrate_kkz00(BgL_az00_3,
					BGl_Kzd21z12zc0zzintegrate_kkz00(BgL_az00_3, BNIL));
			}
		}

	}



/* &K! */
	obj_t BGl_z62Kz12z70zzintegrate_kkz00(obj_t BgL_envz00_1937,
		obj_t BgL_az00_1938, obj_t BgL_varz00_1939)
	{
		{	/* Integrate/kk.scm 44 */
			return
				BGl_Kz12z12zzintegrate_kkz00(BgL_az00_1938,
				((BgL_globalz00_bglt) BgL_varz00_1939));
		}

	}



/* K-1! */
	obj_t BGl_Kzd21z12zc0zzintegrate_kkz00(obj_t BgL_az00_5,
		obj_t BgL_azd2tailzd2_6)
	{
		{	/* Integrate/kk.scm 56 */
		BGl_Kzd21z12zc0zzintegrate_kkz00:
			if (NULLP(BgL_az00_5))
				{	/* Integrate/kk.scm 58 */
					return BgL_azd2tailzd2_6;
				}
			else
				{	/* Integrate/kk.scm 60 */
					obj_t BgL_prz00_1581;

					BgL_prz00_1581 = CAR(((obj_t) BgL_az00_5));
					{	/* Integrate/kk.scm 60 */
						obj_t BgL_fz00_1582;

						BgL_fz00_1582 = CAR(((obj_t) BgL_prz00_1581));
						{	/* Integrate/kk.scm 61 */
							obj_t BgL_gz00_1583;

							{	/* Integrate/kk.scm 62 */
								obj_t BgL_pairz00_1860;

								BgL_pairz00_1860 = CDR(((obj_t) BgL_prz00_1581));
								BgL_gz00_1583 = CAR(BgL_pairz00_1860);
							}
							{	/* Integrate/kk.scm 62 */
								obj_t BgL_kz00_1584;

								{	/* Integrate/kk.scm 63 */
									obj_t BgL_pairz00_1866;

									{	/* Integrate/kk.scm 63 */
										obj_t BgL_pairz00_1865;

										BgL_pairz00_1865 = CDR(((obj_t) BgL_prz00_1581));
										BgL_pairz00_1866 = CDR(BgL_pairz00_1865);
									}
									BgL_kz00_1584 = CAR(BgL_pairz00_1866);
								}
								{	/* Integrate/kk.scm 63 */

									if ((BgL_kz00_1584 == CNST_TABLE_REF(1)))
										{	/* Integrate/kk.scm 65 */
											if ((BgL_fz00_1582 == BgL_gz00_1583))
												{	/* Integrate/kk.scm 67 */
													obj_t BgL_arg1272z00_1585;

													BgL_arg1272z00_1585 = CDR(((obj_t) BgL_az00_5));
													{
														obj_t BgL_az00_2031;

														BgL_az00_2031 = BgL_arg1272z00_1585;
														BgL_az00_5 = BgL_az00_2031;
														goto BGl_Kzd21z12zc0zzintegrate_kkz00;
													}
												}
											else
												{	/* Integrate/kk.scm 68 */
													obj_t BgL_arg1284z00_1586;
													obj_t BgL_arg1304z00_1587;

													BgL_arg1284z00_1586 = CDR(((obj_t) BgL_az00_5));
													BgL_arg1304z00_1587 =
														MAKE_YOUNG_PAIR(BgL_prz00_1581, BgL_azd2tailzd2_6);
													{
														obj_t BgL_azd2tailzd2_2036;
														obj_t BgL_az00_2035;

														BgL_az00_2035 = BgL_arg1284z00_1586;
														BgL_azd2tailzd2_2036 = BgL_arg1304z00_1587;
														BgL_azd2tailzd2_6 = BgL_azd2tailzd2_2036;
														BgL_az00_5 = BgL_az00_2035;
														goto BGl_Kzd21z12zc0zzintegrate_kkz00;
													}
												}
										}
									else
										{	/* Integrate/kk.scm 70 */
											BgL_valuez00_bglt BgL_ifunz00_1588;

											BgL_ifunz00_1588 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_gz00_1583)))->
												BgL_valuez00);
											{	/* Integrate/kk.scm 71 */
												bool_t BgL_test1686z00_2039;

												{	/* Integrate/kk.scm 71 */
													obj_t BgL_arg1316z00_1597;

													{
														BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2040;

														{
															obj_t BgL_auxz00_2041;

															{	/* Integrate/kk.scm 71 */
																BgL_objectz00_bglt BgL_tmpz00_2042;

																BgL_tmpz00_2042 =
																	((BgL_objectz00_bglt)
																	((BgL_sfunz00_bglt) BgL_ifunz00_1588));
																BgL_auxz00_2041 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_2042);
															}
															BgL_auxz00_2040 =
																((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2041);
														}
														BgL_arg1316z00_1597 =
															(((BgL_sfunzf2iinfozf2_bglt)
																COBJECT(BgL_auxz00_2040))->BgL_kz00);
													}
													BgL_test1686z00_2039 =
														CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
														(BgL_kz00_1584, BgL_arg1316z00_1597));
												}
												if (BgL_test1686z00_2039)
													{	/* Integrate/kk.scm 72 */
														obj_t BgL_arg1308z00_1591;

														BgL_arg1308z00_1591 = CDR(((obj_t) BgL_az00_5));
														{
															obj_t BgL_az00_2052;

															BgL_az00_2052 = BgL_arg1308z00_1591;
															BgL_az00_5 = BgL_az00_2052;
															goto BGl_Kzd21z12zc0zzintegrate_kkz00;
														}
													}
												else
													{	/* Integrate/kk.scm 71 */
														{	/* Integrate/kk.scm 74 */
															obj_t BgL_arg1310z00_1592;

															{	/* Integrate/kk.scm 74 */
																obj_t BgL_arg1311z00_1593;

																{
																	BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2053;

																	{
																		obj_t BgL_auxz00_2054;

																		{	/* Integrate/kk.scm 74 */
																			BgL_objectz00_bglt BgL_tmpz00_2055;

																			BgL_tmpz00_2055 =
																				((BgL_objectz00_bglt)
																				((BgL_sfunz00_bglt) BgL_ifunz00_1588));
																			BgL_auxz00_2054 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_2055);
																		}
																		BgL_auxz00_2053 =
																			((BgL_sfunzf2iinfozf2_bglt)
																			BgL_auxz00_2054);
																	}
																	BgL_arg1311z00_1593 =
																		(((BgL_sfunzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_2053))->BgL_kz00);
																}
																BgL_arg1310z00_1592 =
																	MAKE_YOUNG_PAIR(BgL_kz00_1584,
																	BgL_arg1311z00_1593);
															}
															{
																BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2062;

																{
																	obj_t BgL_auxz00_2063;

																	{	/* Integrate/kk.scm 74 */
																		BgL_objectz00_bglt BgL_tmpz00_2064;

																		BgL_tmpz00_2064 =
																			((BgL_objectz00_bglt)
																			((BgL_sfunz00_bglt) BgL_ifunz00_1588));
																		BgL_auxz00_2063 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2064);
																	}
																	BgL_auxz00_2062 =
																		((BgL_sfunzf2iinfozf2_bglt)
																		BgL_auxz00_2063);
																}
																((((BgL_sfunzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_2062))->BgL_kz00) =
																	((obj_t) BgL_arg1310z00_1592), BUNSPEC);
															}
														}
														{	/* Integrate/kk.scm 75 */
															obj_t BgL_arg1312z00_1594;

															{	/* Integrate/kk.scm 75 */
																obj_t BgL_arg1314z00_1595;

																{
																	BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2070;

																	{
																		obj_t BgL_auxz00_2071;

																		{	/* Integrate/kk.scm 75 */
																			BgL_objectz00_bglt BgL_tmpz00_2072;

																			BgL_tmpz00_2072 =
																				((BgL_objectz00_bglt)
																				((BgL_sfunz00_bglt) BgL_ifunz00_1588));
																			BgL_auxz00_2071 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_2072);
																		}
																		BgL_auxz00_2070 =
																			((BgL_sfunzf2iinfozf2_bglt)
																			BgL_auxz00_2071);
																	}
																	BgL_arg1314z00_1595 =
																		(((BgL_sfunzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_2070))->BgL_kza2za2);
																}
																BgL_arg1312z00_1594 =
																	MAKE_YOUNG_PAIR(BgL_kz00_1584,
																	BgL_arg1314z00_1595);
															}
															{
																BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2079;

																{
																	obj_t BgL_auxz00_2080;

																	{	/* Integrate/kk.scm 75 */
																		BgL_objectz00_bglt BgL_tmpz00_2081;

																		BgL_tmpz00_2081 =
																			((BgL_objectz00_bglt)
																			((BgL_sfunz00_bglt) BgL_ifunz00_1588));
																		BgL_auxz00_2080 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2081);
																	}
																	BgL_auxz00_2079 =
																		((BgL_sfunzf2iinfozf2_bglt)
																		BgL_auxz00_2080);
																}
																((((BgL_sfunzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_2079))->BgL_kza2za2) =
																	((obj_t) BgL_arg1312z00_1594), BUNSPEC);
															}
														}
														{	/* Integrate/kk.scm 76 */
															obj_t BgL_arg1315z00_1596;

															BgL_arg1315z00_1596 = CDR(((obj_t) BgL_az00_5));
															{
																obj_t BgL_az00_2089;

																BgL_az00_2089 = BgL_arg1315z00_1596;
																BgL_az00_5 = BgL_az00_2089;
																goto BGl_Kzd21z12zc0zzintegrate_kkz00;
															}
														}
													}
											}
										}
								}
							}
						}
					}
				}
		}

	}



/* K-2! */
	obj_t BGl_Kzd22z12zc0zzintegrate_kkz00(obj_t BgL_az00_7,
		obj_t BgL_azd2tailzd2_8)
	{
		{	/* Integrate/kk.scm 81 */
			{
				bool_t BgL_continuez00_1599;

				BgL_continuez00_1599 = ((bool_t) 1);
			BgL_zc3z04anonymousza31317ze3z87_1600:
				if (BgL_continuez00_1599)
					{
						obj_t BgL_atz00_1602;
						bool_t BgL_continuez00_1603;

						BgL_atz00_1602 = BgL_azd2tailzd2_8;
						BgL_continuez00_1603 = ((bool_t) 0);
					BgL_zc3z04anonymousza31318ze3z87_1604:
						if (NULLP(BgL_atz00_1602))
							{
								bool_t BgL_continuez00_2093;

								BgL_continuez00_2093 = BgL_continuez00_1603;
								BgL_continuez00_1599 = BgL_continuez00_2093;
								goto BgL_zc3z04anonymousza31317ze3z87_1600;
							}
						else
							{	/* Integrate/kk.scm 93 */
								BgL_valuez00_bglt BgL_ifunz00_1606;

								{
									BgL_variablez00_bglt BgL_auxz00_2094;

									{	/* Integrate/kk.scm 93 */
										obj_t BgL_pairz00_1883;

										BgL_pairz00_1883 = CAR(((obj_t) BgL_atz00_1602));
										BgL_auxz00_2094 =
											((BgL_variablez00_bglt) CAR(BgL_pairz00_1883));
									}
									BgL_ifunz00_1606 =
										(((BgL_variablez00_bglt) COBJECT(BgL_auxz00_2094))->
										BgL_valuez00);
								}
								{	/* Integrate/kk.scm 94 */
									bool_t BgL_test1689z00_2100;

									{	/* Integrate/kk.scm 94 */
										obj_t BgL_arg1342z00_1631;

										{
											BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2101;

											{
												obj_t BgL_auxz00_2102;

												{	/* Integrate/kk.scm 94 */
													BgL_objectz00_bglt BgL_tmpz00_2103;

													BgL_tmpz00_2103 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_ifunz00_1606));
													BgL_auxz00_2102 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2103);
												}
												BgL_auxz00_2101 =
													((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2102);
											}
											BgL_arg1342z00_1631 =
												(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2101))->
												BgL_kz00);
										}
										BgL_test1689z00_2100 = NULLP(BgL_arg1342z00_1631);
									}
									if (BgL_test1689z00_2100)
										{	/* Integrate/kk.scm 95 */
											obj_t BgL_arg1322z00_1609;

											BgL_arg1322z00_1609 = CDR(((obj_t) BgL_atz00_1602));
											{
												obj_t BgL_atz00_2112;

												BgL_atz00_2112 = BgL_arg1322z00_1609;
												BgL_atz00_1602 = BgL_atz00_2112;
												goto BgL_zc3z04anonymousza31318ze3z87_1604;
											}
										}
									else
										{	/* Integrate/kk.scm 96 */
											obj_t BgL_gz00_1610;

											{	/* Integrate/kk.scm 96 */
												obj_t BgL_pairz00_1889;

												BgL_pairz00_1889 = CAR(((obj_t) BgL_atz00_1602));
												BgL_gz00_1610 = CAR(CDR(BgL_pairz00_1889));
											}
											{	/* Integrate/kk.scm 96 */
												BgL_valuez00_bglt BgL_gifunz00_1611;

												BgL_gifunz00_1611 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_gz00_1610)))->
													BgL_valuez00);
												{	/* Integrate/kk.scm 97 */

													{	/* Integrate/kk.scm 98 */
														obj_t BgL_g1109z00_1612;

														{
															BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2119;

															{
																obj_t BgL_auxz00_2120;

																{	/* Integrate/kk.scm 98 */
																	BgL_objectz00_bglt BgL_tmpz00_2121;

																	BgL_tmpz00_2121 =
																		((BgL_objectz00_bglt)
																		((BgL_sfunz00_bglt) BgL_ifunz00_1606));
																	BgL_auxz00_2120 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2121);
																}
																BgL_auxz00_2119 =
																	((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2120);
															}
															BgL_g1109z00_1612 =
																(((BgL_sfunzf2iinfozf2_bglt)
																	COBJECT(BgL_auxz00_2119))->BgL_kz00);
														}
														{
															obj_t BgL_ksz00_1614;
															bool_t BgL_continuez00_1615;

															BgL_ksz00_1614 = BgL_g1109z00_1612;
															BgL_continuez00_1615 = BgL_continuez00_1603;
														BgL_zc3z04anonymousza31323ze3z87_1616:
															if (NULLP(BgL_ksz00_1614))
																{	/* Integrate/kk.scm 104 */
																	obj_t BgL_arg1325z00_1618;

																	BgL_arg1325z00_1618 =
																		CDR(((obj_t) BgL_atz00_1602));
																	{
																		bool_t BgL_continuez00_2132;
																		obj_t BgL_atz00_2131;

																		BgL_atz00_2131 = BgL_arg1325z00_1618;
																		BgL_continuez00_2132 = BgL_continuez00_1615;
																		BgL_continuez00_1603 = BgL_continuez00_2132;
																		BgL_atz00_1602 = BgL_atz00_2131;
																		goto BgL_zc3z04anonymousza31318ze3z87_1604;
																	}
																}
															else
																{	/* Integrate/kk.scm 105 */
																	obj_t BgL_kz00_1619;

																	BgL_kz00_1619 = CAR(((obj_t) BgL_ksz00_1614));
																	{	/* Integrate/kk.scm 106 */
																		bool_t BgL_test1691z00_2135;

																		{	/* Integrate/kk.scm 106 */
																			obj_t BgL_arg1339z00_1628;

																			{
																				BgL_sfunzf2iinfozf2_bglt
																					BgL_auxz00_2136;
																				{
																					obj_t BgL_auxz00_2137;

																					{	/* Integrate/kk.scm 106 */
																						BgL_objectz00_bglt BgL_tmpz00_2138;

																						BgL_tmpz00_2138 =
																							((BgL_objectz00_bglt)
																							((BgL_sfunz00_bglt)
																								BgL_gifunz00_1611));
																						BgL_auxz00_2137 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_2138);
																					}
																					BgL_auxz00_2136 =
																						((BgL_sfunzf2iinfozf2_bglt)
																						BgL_auxz00_2137);
																				}
																				BgL_arg1339z00_1628 =
																					(((BgL_sfunzf2iinfozf2_bglt)
																						COBJECT(BgL_auxz00_2136))->
																					BgL_kz00);
																			}
																			BgL_test1691z00_2135 =
																				CBOOL
																				(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																				(BgL_kz00_1619, BgL_arg1339z00_1628));
																		}
																		if (BgL_test1691z00_2135)
																			{	/* Integrate/kk.scm 107 */
																				obj_t BgL_arg1328z00_1622;

																				BgL_arg1328z00_1622 =
																					CDR(((obj_t) BgL_ksz00_1614));
																				{
																					obj_t BgL_ksz00_2148;

																					BgL_ksz00_2148 = BgL_arg1328z00_1622;
																					BgL_ksz00_1614 = BgL_ksz00_2148;
																					goto
																						BgL_zc3z04anonymousza31323ze3z87_1616;
																				}
																			}
																		else
																			{	/* Integrate/kk.scm 106 */
																				{	/* Integrate/kk.scm 111 */
																					obj_t BgL_arg1329z00_1623;

																					{	/* Integrate/kk.scm 111 */
																						obj_t BgL_arg1331z00_1624;

																						{
																							BgL_sfunzf2iinfozf2_bglt
																								BgL_auxz00_2149;
																							{
																								obj_t BgL_auxz00_2150;

																								{	/* Integrate/kk.scm 111 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_2151;
																									BgL_tmpz00_2151 =
																										((BgL_objectz00_bglt) (
																											(BgL_sfunz00_bglt)
																											BgL_gifunz00_1611));
																									BgL_auxz00_2150 =
																										BGL_OBJECT_WIDENING
																										(BgL_tmpz00_2151);
																								}
																								BgL_auxz00_2149 =
																									((BgL_sfunzf2iinfozf2_bglt)
																									BgL_auxz00_2150);
																							}
																							BgL_arg1331z00_1624 =
																								(((BgL_sfunzf2iinfozf2_bglt)
																									COBJECT(BgL_auxz00_2149))->
																								BgL_kz00);
																						}
																						BgL_arg1329z00_1623 =
																							MAKE_YOUNG_PAIR(BgL_kz00_1619,
																							BgL_arg1331z00_1624);
																					}
																					{
																						BgL_sfunzf2iinfozf2_bglt
																							BgL_auxz00_2158;
																						{
																							obj_t BgL_auxz00_2159;

																							{	/* Integrate/kk.scm 109 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_2160;
																								BgL_tmpz00_2160 =
																									((BgL_objectz00_bglt) (
																										(BgL_sfunz00_bglt)
																										BgL_gifunz00_1611));
																								BgL_auxz00_2159 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_2160);
																							}
																							BgL_auxz00_2158 =
																								((BgL_sfunzf2iinfozf2_bglt)
																								BgL_auxz00_2159);
																						}
																						((((BgL_sfunzf2iinfozf2_bglt)
																									COBJECT(BgL_auxz00_2158))->
																								BgL_kz00) =
																							((obj_t) BgL_arg1329z00_1623),
																							BUNSPEC);
																					}
																				}
																				{	/* Integrate/kk.scm 114 */
																					obj_t BgL_arg1332z00_1625;

																					{	/* Integrate/kk.scm 114 */
																						obj_t BgL_arg1333z00_1626;

																						{
																							BgL_sfunzf2iinfozf2_bglt
																								BgL_auxz00_2166;
																							{
																								obj_t BgL_auxz00_2167;

																								{	/* Integrate/kk.scm 114 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_2168;
																									BgL_tmpz00_2168 =
																										((BgL_objectz00_bglt) (
																											(BgL_sfunz00_bglt)
																											BgL_gifunz00_1611));
																									BgL_auxz00_2167 =
																										BGL_OBJECT_WIDENING
																										(BgL_tmpz00_2168);
																								}
																								BgL_auxz00_2166 =
																									((BgL_sfunzf2iinfozf2_bglt)
																									BgL_auxz00_2167);
																							}
																							BgL_arg1333z00_1626 =
																								(((BgL_sfunzf2iinfozf2_bglt)
																									COBJECT(BgL_auxz00_2166))->
																								BgL_kza2za2);
																						}
																						BgL_arg1332z00_1625 =
																							MAKE_YOUNG_PAIR(BgL_kz00_1619,
																							BgL_arg1333z00_1626);
																					}
																					{
																						BgL_sfunzf2iinfozf2_bglt
																							BgL_auxz00_2175;
																						{
																							obj_t BgL_auxz00_2176;

																							{	/* Integrate/kk.scm 112 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_2177;
																								BgL_tmpz00_2177 =
																									((BgL_objectz00_bglt) (
																										(BgL_sfunz00_bglt)
																										BgL_gifunz00_1611));
																								BgL_auxz00_2176 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_2177);
																							}
																							BgL_auxz00_2175 =
																								((BgL_sfunzf2iinfozf2_bglt)
																								BgL_auxz00_2176);
																						}
																						((((BgL_sfunzf2iinfozf2_bglt)
																									COBJECT(BgL_auxz00_2175))->
																								BgL_kza2za2) =
																							((obj_t) BgL_arg1332z00_1625),
																							BUNSPEC);
																					}
																				}
																				{	/* Integrate/kk.scm 115 */
																					obj_t BgL_arg1335z00_1627;

																					BgL_arg1335z00_1627 =
																						CDR(((obj_t) BgL_ksz00_1614));
																					{
																						bool_t BgL_continuez00_2186;
																						obj_t BgL_ksz00_2185;

																						BgL_ksz00_2185 =
																							BgL_arg1335z00_1627;
																						BgL_continuez00_2186 = ((bool_t) 1);
																						BgL_continuez00_1615 =
																							BgL_continuez00_2186;
																						BgL_ksz00_1614 = BgL_ksz00_2185;
																						goto
																							BgL_zc3z04anonymousza31323ze3z87_1616;
																					}
																				}
																			}
																	}
																}
														}
													}
												}
											}
										}
								}
							}
					}
				else
					{	/* Integrate/kk.scm 84 */
						return BgL_azd2tailzd2_8;
					}
			}
		}

	}



/* K*! */
	BGL_EXPORTED_DEF obj_t BGl_Kza2z12zb0zzintegrate_kkz00(obj_t
		BgL_azd2tailzd2_9)
	{
		{	/* Integrate/kk.scm 130 */
			{
				bool_t BgL_continuez00_1637;

				BgL_continuez00_1637 = ((bool_t) 1);
			BgL_zc3z04anonymousza31347ze3z87_1638:
				if (BgL_continuez00_1637)
					{
						obj_t BgL_atz00_1640;
						bool_t BgL_continuez00_1641;

						BgL_atz00_1640 = BgL_azd2tailzd2_9;
						BgL_continuez00_1641 = ((bool_t) 0);
					BgL_zc3z04anonymousza31348ze3z87_1642:
						if (NULLP(BgL_atz00_1640))
							{
								bool_t BgL_continuez00_2190;

								BgL_continuez00_2190 = BgL_continuez00_1641;
								BgL_continuez00_1637 = BgL_continuez00_2190;
								goto BgL_zc3z04anonymousza31347ze3z87_1638;
							}
						else
							{	/* Integrate/kk.scm 139 */
								BgL_valuez00_bglt BgL_ifunz00_1644;

								{
									BgL_variablez00_bglt BgL_auxz00_2191;

									{
										obj_t BgL_auxz00_2192;

										{	/* Integrate/kk.scm 139 */
											obj_t BgL_pairz00_1911;

											BgL_pairz00_1911 = CAR(((obj_t) BgL_atz00_1640));
											{	/* Integrate/kk.scm 139 */
												obj_t BgL_pairz00_1914;

												BgL_pairz00_1914 = CDR(BgL_pairz00_1911);
												BgL_auxz00_2192 = CAR(BgL_pairz00_1914);
											}
										}
										BgL_auxz00_2191 = ((BgL_variablez00_bglt) BgL_auxz00_2192);
									}
									BgL_ifunz00_1644 =
										(((BgL_variablez00_bglt) COBJECT(BgL_auxz00_2191))->
										BgL_valuez00);
								}
								{	/* Integrate/kk.scm 140 */
									bool_t BgL_test1694z00_2199;

									{	/* Integrate/kk.scm 140 */
										obj_t BgL_arg1378z00_1666;

										{
											BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2200;

											{
												obj_t BgL_auxz00_2201;

												{	/* Integrate/kk.scm 140 */
													BgL_objectz00_bglt BgL_tmpz00_2202;

													BgL_tmpz00_2202 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_ifunz00_1644));
													BgL_auxz00_2201 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2202);
												}
												BgL_auxz00_2200 =
													((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2201);
											}
											BgL_arg1378z00_1666 =
												(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2200))->
												BgL_kza2za2);
										}
										BgL_test1694z00_2199 = NULLP(BgL_arg1378z00_1666);
									}
									if (BgL_test1694z00_2199)
										{
											bool_t BgL_continuez00_2209;

											BgL_continuez00_2209 = BgL_continuez00_1641;
											BgL_continuez00_1637 = BgL_continuez00_2209;
											goto BgL_zc3z04anonymousza31347ze3z87_1638;
										}
									else
										{	/* Integrate/kk.scm 142 */
											obj_t BgL_g1110z00_1647;

											{
												BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2210;

												{
													obj_t BgL_auxz00_2211;

													{	/* Integrate/kk.scm 142 */
														BgL_objectz00_bglt BgL_tmpz00_2212;

														BgL_tmpz00_2212 =
															((BgL_objectz00_bglt)
															((BgL_sfunz00_bglt) BgL_ifunz00_1644));
														BgL_auxz00_2211 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_2212);
													}
													BgL_auxz00_2210 =
														((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2211);
												}
												BgL_g1110z00_1647 =
													(((BgL_sfunzf2iinfozf2_bglt)
														COBJECT(BgL_auxz00_2210))->BgL_kza2za2);
											}
											{
												obj_t BgL_ksz00_1649;
												bool_t BgL_continuez00_1650;

												BgL_ksz00_1649 = BgL_g1110z00_1647;
												BgL_continuez00_1650 = BgL_continuez00_1641;
											BgL_zc3z04anonymousza31355ze3z87_1651:
												if (NULLP(BgL_ksz00_1649))
													{	/* Integrate/kk.scm 145 */
														obj_t BgL_arg1361z00_1653;

														BgL_arg1361z00_1653 = CDR(((obj_t) BgL_atz00_1640));
														{
															bool_t BgL_continuez00_2223;
															obj_t BgL_atz00_2222;

															BgL_atz00_2222 = BgL_arg1361z00_1653;
															BgL_continuez00_2223 = BgL_continuez00_1650;
															BgL_continuez00_1641 = BgL_continuez00_2223;
															BgL_atz00_1640 = BgL_atz00_2222;
															goto BgL_zc3z04anonymousza31348ze3z87_1642;
														}
													}
												else
													{	/* Integrate/kk.scm 146 */
														obj_t BgL_fz00_1654;

														{	/* Integrate/kk.scm 146 */
															obj_t BgL_pairz00_1922;

															BgL_pairz00_1922 = CAR(((obj_t) BgL_atz00_1640));
															BgL_fz00_1654 = CAR(BgL_pairz00_1922);
														}
														{	/* Integrate/kk.scm 146 */
															BgL_valuez00_bglt BgL_fifunz00_1655;

															BgL_fifunz00_1655 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt) BgL_fz00_1654)))->
																BgL_valuez00);
															{	/* Integrate/kk.scm 147 */
																obj_t BgL_kz00_1656;

																BgL_kz00_1656 = CAR(((obj_t) BgL_ksz00_1649));
																{	/* Integrate/kk.scm 148 */

																	{	/* Integrate/kk.scm 149 */
																		bool_t BgL_test1696z00_2231;

																		{	/* Integrate/kk.scm 149 */
																			obj_t BgL_arg1376z00_1663;

																			{
																				BgL_sfunzf2iinfozf2_bglt
																					BgL_auxz00_2232;
																				{
																					obj_t BgL_auxz00_2233;

																					{	/* Integrate/kk.scm 149 */
																						BgL_objectz00_bglt BgL_tmpz00_2234;

																						BgL_tmpz00_2234 =
																							((BgL_objectz00_bglt)
																							((BgL_sfunz00_bglt)
																								BgL_fifunz00_1655));
																						BgL_auxz00_2233 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_2234);
																					}
																					BgL_auxz00_2232 =
																						((BgL_sfunzf2iinfozf2_bglt)
																						BgL_auxz00_2233);
																				}
																				BgL_arg1376z00_1663 =
																					(((BgL_sfunzf2iinfozf2_bglt)
																						COBJECT(BgL_auxz00_2232))->
																					BgL_kza2za2);
																			}
																			BgL_test1696z00_2231 =
																				CBOOL
																				(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																				(BgL_kz00_1656, BgL_arg1376z00_1663));
																		}
																		if (BgL_test1696z00_2231)
																			{	/* Integrate/kk.scm 150 */
																				obj_t BgL_arg1367z00_1659;

																				BgL_arg1367z00_1659 =
																					CDR(((obj_t) BgL_ksz00_1649));
																				{
																					obj_t BgL_ksz00_2244;

																					BgL_ksz00_2244 = BgL_arg1367z00_1659;
																					BgL_ksz00_1649 = BgL_ksz00_2244;
																					goto
																						BgL_zc3z04anonymousza31355ze3z87_1651;
																				}
																			}
																		else
																			{	/* Integrate/kk.scm 149 */
																				{	/* Integrate/kk.scm 154 */
																					obj_t BgL_arg1370z00_1660;

																					{	/* Integrate/kk.scm 154 */
																						obj_t BgL_arg1371z00_1661;

																						{
																							BgL_sfunzf2iinfozf2_bglt
																								BgL_auxz00_2245;
																							{
																								obj_t BgL_auxz00_2246;

																								{	/* Integrate/kk.scm 154 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_2247;
																									BgL_tmpz00_2247 =
																										((BgL_objectz00_bglt) (
																											(BgL_sfunz00_bglt)
																											BgL_fifunz00_1655));
																									BgL_auxz00_2246 =
																										BGL_OBJECT_WIDENING
																										(BgL_tmpz00_2247);
																								}
																								BgL_auxz00_2245 =
																									((BgL_sfunzf2iinfozf2_bglt)
																									BgL_auxz00_2246);
																							}
																							BgL_arg1371z00_1661 =
																								(((BgL_sfunzf2iinfozf2_bglt)
																									COBJECT(BgL_auxz00_2245))->
																								BgL_kza2za2);
																						}
																						BgL_arg1370z00_1660 =
																							MAKE_YOUNG_PAIR(BgL_kz00_1656,
																							BgL_arg1371z00_1661);
																					}
																					{
																						BgL_sfunzf2iinfozf2_bglt
																							BgL_auxz00_2254;
																						{
																							obj_t BgL_auxz00_2255;

																							{	/* Integrate/kk.scm 152 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_2256;
																								BgL_tmpz00_2256 =
																									((BgL_objectz00_bglt) (
																										(BgL_sfunz00_bglt)
																										BgL_fifunz00_1655));
																								BgL_auxz00_2255 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_2256);
																							}
																							BgL_auxz00_2254 =
																								((BgL_sfunzf2iinfozf2_bglt)
																								BgL_auxz00_2255);
																						}
																						((((BgL_sfunzf2iinfozf2_bglt)
																									COBJECT(BgL_auxz00_2254))->
																								BgL_kza2za2) =
																							((obj_t) BgL_arg1370z00_1660),
																							BUNSPEC);
																					}
																				}
																				{	/* Integrate/kk.scm 155 */
																					obj_t BgL_arg1375z00_1662;

																					BgL_arg1375z00_1662 =
																						CDR(((obj_t) BgL_ksz00_1649));
																					{
																						bool_t BgL_continuez00_2265;
																						obj_t BgL_ksz00_2264;

																						BgL_ksz00_2264 =
																							BgL_arg1375z00_1662;
																						BgL_continuez00_2265 = ((bool_t) 1);
																						BgL_continuez00_1650 =
																							BgL_continuez00_2265;
																						BgL_ksz00_1649 = BgL_ksz00_2264;
																						goto
																							BgL_zc3z04anonymousza31355ze3z87_1651;
																					}
																				}
																			}
																	}
																}
															}
														}
													}
											}
										}
								}
							}
					}
				else
					{	/* Integrate/kk.scm 133 */
						return BNIL;
					}
			}
		}

	}



/* &K*! */
	obj_t BGl_z62Kza2z12zd2zzintegrate_kkz00(obj_t BgL_envz00_1940,
		obj_t BgL_azd2tailzd2_1941)
	{
		{	/* Integrate/kk.scm 130 */
			return BGl_Kza2z12zb0zzintegrate_kkz00(BgL_azd2tailzd2_1941);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzintegrate_kkz00(void)
	{
		{	/* Integrate/kk.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzintegrate_kkz00(void)
	{
		{	/* Integrate/kk.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzintegrate_kkz00(void)
	{
		{	/* Integrate/kk.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzintegrate_kkz00(void)
	{
		{	/* Integrate/kk.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1666z00zzintegrate_kkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1666z00zzintegrate_kkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1666z00zzintegrate_kkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1666z00zzintegrate_kkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1666z00zzintegrate_kkz00));
			BGl_modulezd2initializa7ationz75zzintegrate_infoz00(0L,
				BSTRING_TO_STRING(BGl_string1666z00zzintegrate_kkz00));
			return
				BGl_modulezd2initializa7ationz75zzintegrate_az00(523633197L,
				BSTRING_TO_STRING(BGl_string1666z00zzintegrate_kkz00));
		}

	}

#ifdef __cplusplus
}
#endif
