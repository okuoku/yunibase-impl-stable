/*===========================================================================*/
/*   (Integrate/free.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Integrate/free.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INTEGRATE_FREE_TYPE_DEFINITIONS
#define BGL_INTEGRATE_FREE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_sexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_handlerz00;
		bool_t BgL_detachedzf3zf3;
	}               *BgL_sexitz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_svarzf2iinfozf2_bgl
	{
		obj_t BgL_fzd2markzd2;
		obj_t BgL_uzd2markzd2;
		bool_t BgL_kapturedzf3zf3;
		bool_t BgL_celledzf3zf3;
		obj_t BgL_xhdlz00;
	}                      *BgL_svarzf2iinfozf2_bglt;

	typedef struct BgL_sexitzf2iinfozf2_bgl
	{
		obj_t BgL_fzd2markzd2;
		obj_t BgL_uzd2markzd2;
		bool_t BgL_kapturedzf3zf3;
		bool_t BgL_celledzf3zf3;
	}                       *BgL_sexitzf2iinfozf2_bglt;

	typedef struct BgL_sfunzf2iinfozf2_bgl
	{
		obj_t BgL_ownerz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
		obj_t BgL_cfromz00;
		obj_t BgL_ctoz00;
		obj_t BgL_kz00;
		obj_t BgL_kza2za2;
		obj_t BgL_uz00;
		obj_t BgL_cnz00;
		obj_t BgL_ctz00;
		obj_t BgL_kontz00;
		bool_t BgL_gzf3zf3;
		bool_t BgL_forcegzf3zf3;
		obj_t BgL_lz00;
		obj_t BgL_ledz00;
		obj_t BgL_istampz00;
		obj_t BgL_globalz00;
		obj_t BgL_kapturedz00;
		obj_t BgL_tailzd2coercionzd2;
		bool_t BgL_xhdlzf3zf3;
		obj_t BgL_xhdlsz00;
	}                      *BgL_sfunzf2iinfozf2_bglt;


#endif													// BGL_INTEGRATE_FREE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_z62nodezd2freezd2makezd2box1307zb0zzintegrate_freez00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62nodezd2freezd2boxzd2setz121311za2zzintegrate_freez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzintegrate_freez00 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzintegrate_freez00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_nodezd2freeza2z70zzintegrate_freez00(obj_t, obj_t);
	static obj_t BGl_za2integratorza2z00zzintegrate_freez00 = BUNSPEC;
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62nodezd2freezb0zzintegrate_freez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzintegrate_freez00(void);
	static obj_t BGl_z62nodezd2freezd2conditiona1293z62zzintegrate_freez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzintegrate_freez00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2freezd2letzd2fun1299zb0zzintegrate_freez00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2freezd2varsz00zzintegrate_freez00(BgL_nodez00_bglt,
		BgL_localz00_bglt);
	static obj_t
		BGl_z62nodezd2freezd2jumpzd2exzd2it1305z62zzintegrate_freez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_nodezd2freezd2zzintegrate_freez00(BgL_nodez00_bglt, obj_t);
	static obj_t BGl_methodzd2initzd2zzintegrate_freez00(void);
	static obj_t BGl_z62freezd2fromzb0zzintegrate_freez00(obj_t, obj_t, obj_t);
	static obj_t BGl_bindzd2variablez12zc0zzintegrate_freez00(BgL_localz00_bglt,
		BgL_localz00_bglt);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62nodezd2freezd2letzd2var1301zb0zzintegrate_freez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_markzd2variablez12zc0zzintegrate_freez00(BgL_localz00_bglt);
	static obj_t
		BGl_z62nodezd2freezd2setzd2exzd2it1303z62zzintegrate_freez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2freezd2boxzd2ref1309zb0zzintegrate_freez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_freezd2fromzd2zzintegrate_freez00(obj_t,
		BgL_localz00_bglt);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_sexitzf2Iinfozf2zzintegrate_infoz00;
	static long BGl_za2roundza2z00zzintegrate_freez00 = 0L;
	static obj_t BGl_z62nodezd2freezd2sequence1277z62zzintegrate_freez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2freezd2var1273z62zzintegrate_freez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_freez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_nodez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_infoz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62nodezd2freezd2funcall1285z62zzintegrate_freez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_svarzf2Iinfozf2zzintegrate_infoz00;
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_freezd2variablezf3z21zzintegrate_freez00(obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62nodezd2freezd2closure1275z62zzintegrate_freez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_freez00(void);
	static obj_t
		BGl_internalzd2getzd2freezd2varsz12zc0zzintegrate_freez00(BgL_nodez00_bglt,
		BgL_localz00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzintegrate_freez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzintegrate_freez00(void);
	static obj_t BGl_z62nodezd2freezd2app1281z62zzintegrate_freez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2freezd2appzd2ly1283zb0zzintegrate_freez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2free1270zb0zzintegrate_freez00(obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static obj_t BGl_z62getzd2freezd2varsz62zzintegrate_freez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2freezd2cast1289z62zzintegrate_freez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2freezd2extern1287z62zzintegrate_freez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62nodezd2freezd2switch1297z62zzintegrate_freez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_z62nodezd2freezd2setq1291z62zzintegrate_freez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2freezd2sync1279z62zzintegrate_freez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2freezd2fail1295z62zzintegrate_freez00(obj_t, obj_t,
		obj_t);
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2freezd2varszd2envzd2zzintegrate_freez00,
		BgL_bgl_za762getza7d2freeza7d21886za7,
		BGl_z62getzd2freezd2varsz62zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1859z00zzintegrate_freez00,
		BgL_bgl_string1859za700za7za7i1887za7, "free-variable?", 14);
	      DEFINE_STRING(BGl_string1860z00zzintegrate_freez00,
		BgL_bgl_string1860za700za7za7i1888za7, "Unknown variable type", 21);
	      DEFINE_STRING(BGl_string1862z00zzintegrate_freez00,
		BgL_bgl_string1862za700za7za7i1889za7, "node-free1270", 13);
	      DEFINE_STRING(BGl_string1864z00zzintegrate_freez00,
		BgL_bgl_string1864za700za7za7i1890za7, "node-free", 9);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1861z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2free121891z00,
		BGl_z62nodezd2free1270zb0zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1863z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1892za7,
		BGl_z62nodezd2freezd2var1273z62zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1865z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1893za7,
		BGl_z62nodezd2freezd2closure1275z62zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1866z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1894za7,
		BGl_z62nodezd2freezd2sequence1277z62zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1867z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1895za7,
		BGl_z62nodezd2freezd2sync1279z62zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1868z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1896za7,
		BGl_z62nodezd2freezd2app1281z62zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1869z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1897za7,
		BGl_z62nodezd2freezd2appzd2ly1283zb0zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1870z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1898za7,
		BGl_z62nodezd2freezd2funcall1285z62zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1871z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1899za7,
		BGl_z62nodezd2freezd2extern1287z62zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1872z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1900za7,
		BGl_z62nodezd2freezd2cast1289z62zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1873z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1901za7,
		BGl_z62nodezd2freezd2setq1291z62zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1874z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1902za7,
		BGl_z62nodezd2freezd2conditiona1293z62zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1875z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1903za7,
		BGl_z62nodezd2freezd2fail1295z62zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1876z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1904za7,
		BGl_z62nodezd2freezd2switch1297z62zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1877z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1905za7,
		BGl_z62nodezd2freezd2letzd2fun1299zb0zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1884z00zzintegrate_freez00,
		BgL_bgl_string1884za700za7za7i1906za7, "Unexepected `closure' node", 26);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1878z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1907za7,
		BGl_z62nodezd2freezd2letzd2var1301zb0zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1885z00zzintegrate_freez00,
		BgL_bgl_string1885za700za7za7i1908za7, "integrate_free", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1879z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1909za7,
		BGl_z62nodezd2freezd2setzd2exzd2it1303z62zzintegrate_freez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1880z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1910za7,
		BGl_z62nodezd2freezd2jumpzd2exzd2it1305z62zzintegrate_freez00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1881z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1911za7,
		BGl_z62nodezd2freezd2makezd2box1307zb0zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1882z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1912za7,
		BGl_z62nodezd2freezd2boxzd2ref1309zb0zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1883z00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7d1913za7,
		BGl_z62nodezd2freezd2boxzd2setz121311za2zzintegrate_freez00, 0L, BUNSPEC,
		2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_freezd2fromzd2envz00zzintegrate_freez00,
		BgL_bgl_za762freeza7d2fromza7b1914za7,
		BGl_z62freezd2fromzb0zzintegrate_freez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_GENERIC(BGl_nodezd2freezd2envz00zzintegrate_freez00,
		BgL_bgl_za762nodeza7d2freeza7b1915za7,
		BGl_z62nodezd2freezb0zzintegrate_freez00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzintegrate_freez00));
		     ADD_ROOT((void *) (&BGl_za2integratorza2z00zzintegrate_freez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_freez00(long
		BgL_checksumz00_2672, char *BgL_fromz00_2673)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzintegrate_freez00))
				{
					BGl_requirezd2initializa7ationz75zzintegrate_freez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzintegrate_freez00();
					BGl_libraryzd2moduleszd2initz00zzintegrate_freez00();
					BGl_importedzd2moduleszd2initz00zzintegrate_freez00();
					BGl_genericzd2initzd2zzintegrate_freez00();
					BGl_methodzd2initzd2zzintegrate_freez00();
					return BGl_toplevelzd2initzd2zzintegrate_freez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_freez00(void)
	{
		{	/* Integrate/free.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "integrate_free");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "integrate_free");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "integrate_free");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"integrate_free");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"integrate_free");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"integrate_free");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"integrate_free");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "integrate_free");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzintegrate_freez00(void)
	{
		{	/* Integrate/free.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzintegrate_freez00(void)
	{
		{	/* Integrate/free.scm 15 */
			BGl_za2roundza2z00zzintegrate_freez00 = 0L;
			BGl_za2integratorza2z00zzintegrate_freez00 = BUNSPEC;
			return BUNSPEC;
		}

	}



/* mark-variable! */
	obj_t BGl_markzd2variablez12zc0zzintegrate_freez00(BgL_localz00_bglt
		BgL_localz00_3)
	{
		{	/* Integrate/free.scm 41 */
			{	/* Integrate/free.scm 42 */
				BgL_valuez00_bglt BgL_infoz00_1621;

				BgL_infoz00_1621 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_localz00_3)))->BgL_valuez00);
				{	/* Integrate/free.scm 44 */
					bool_t BgL_test1917z00_2694;

					{	/* Integrate/free.scm 44 */
						obj_t BgL_classz00_2099;

						BgL_classz00_2099 = BGl_svarzf2Iinfozf2zzintegrate_infoz00;
						{	/* Integrate/free.scm 44 */
							BgL_objectz00_bglt BgL_arg1807z00_2101;

							{	/* Integrate/free.scm 44 */
								obj_t BgL_tmpz00_2695;

								BgL_tmpz00_2695 =
									((obj_t) ((BgL_objectz00_bglt) BgL_infoz00_1621));
								BgL_arg1807z00_2101 = (BgL_objectz00_bglt) (BgL_tmpz00_2695);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Integrate/free.scm 44 */
									long BgL_idxz00_2107;

									BgL_idxz00_2107 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2101);
									BgL_test1917z00_2694 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2107 + 3L)) == BgL_classz00_2099);
								}
							else
								{	/* Integrate/free.scm 44 */
									bool_t BgL_res1846z00_2132;

									{	/* Integrate/free.scm 44 */
										obj_t BgL_oclassz00_2115;

										{	/* Integrate/free.scm 44 */
											obj_t BgL_arg1815z00_2123;
											long BgL_arg1816z00_2124;

											BgL_arg1815z00_2123 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Integrate/free.scm 44 */
												long BgL_arg1817z00_2125;

												BgL_arg1817z00_2125 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2101);
												BgL_arg1816z00_2124 =
													(BgL_arg1817z00_2125 - OBJECT_TYPE);
											}
											BgL_oclassz00_2115 =
												VECTOR_REF(BgL_arg1815z00_2123, BgL_arg1816z00_2124);
										}
										{	/* Integrate/free.scm 44 */
											bool_t BgL__ortest_1115z00_2116;

											BgL__ortest_1115z00_2116 =
												(BgL_classz00_2099 == BgL_oclassz00_2115);
											if (BgL__ortest_1115z00_2116)
												{	/* Integrate/free.scm 44 */
													BgL_res1846z00_2132 = BgL__ortest_1115z00_2116;
												}
											else
												{	/* Integrate/free.scm 44 */
													long BgL_odepthz00_2117;

													{	/* Integrate/free.scm 44 */
														obj_t BgL_arg1804z00_2118;

														BgL_arg1804z00_2118 = (BgL_oclassz00_2115);
														BgL_odepthz00_2117 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2118);
													}
													if ((3L < BgL_odepthz00_2117))
														{	/* Integrate/free.scm 44 */
															obj_t BgL_arg1802z00_2120;

															{	/* Integrate/free.scm 44 */
																obj_t BgL_arg1803z00_2121;

																BgL_arg1803z00_2121 = (BgL_oclassz00_2115);
																BgL_arg1802z00_2120 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2121,
																	3L);
															}
															BgL_res1846z00_2132 =
																(BgL_arg1802z00_2120 == BgL_classz00_2099);
														}
													else
														{	/* Integrate/free.scm 44 */
															BgL_res1846z00_2132 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1917z00_2694 = BgL_res1846z00_2132;
								}
						}
					}
					if (BgL_test1917z00_2694)
						{	/* Integrate/free.scm 45 */
							obj_t BgL_vz00_2134;

							BgL_vz00_2134 = BINT(BGl_za2roundza2z00zzintegrate_freez00);
							{
								BgL_svarzf2iinfozf2_bglt BgL_auxz00_2719;

								{
									obj_t BgL_auxz00_2720;

									{	/* Integrate/free.scm 45 */
										BgL_objectz00_bglt BgL_tmpz00_2721;

										BgL_tmpz00_2721 =
											((BgL_objectz00_bglt)
											((BgL_svarz00_bglt) BgL_infoz00_1621));
										BgL_auxz00_2720 = BGL_OBJECT_WIDENING(BgL_tmpz00_2721);
									}
									BgL_auxz00_2719 =
										((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_2720);
								}
								return
									((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2719))->
										BgL_fzd2markzd2) = ((obj_t) BgL_vz00_2134), BUNSPEC);
							}
						}
					else
						{	/* Integrate/free.scm 46 */
							bool_t BgL_test1921z00_2727;

							{	/* Integrate/free.scm 46 */
								obj_t BgL_classz00_2136;

								BgL_classz00_2136 = BGl_sexitzf2Iinfozf2zzintegrate_infoz00;
								{	/* Integrate/free.scm 46 */
									BgL_objectz00_bglt BgL_arg1807z00_2138;

									{	/* Integrate/free.scm 46 */
										obj_t BgL_tmpz00_2728;

										BgL_tmpz00_2728 =
											((obj_t) ((BgL_objectz00_bglt) BgL_infoz00_1621));
										BgL_arg1807z00_2138 =
											(BgL_objectz00_bglt) (BgL_tmpz00_2728);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Integrate/free.scm 46 */
											long BgL_idxz00_2144;

											BgL_idxz00_2144 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2138);
											BgL_test1921z00_2727 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2144 + 3L)) == BgL_classz00_2136);
										}
									else
										{	/* Integrate/free.scm 46 */
											bool_t BgL_res1847z00_2169;

											{	/* Integrate/free.scm 46 */
												obj_t BgL_oclassz00_2152;

												{	/* Integrate/free.scm 46 */
													obj_t BgL_arg1815z00_2160;
													long BgL_arg1816z00_2161;

													BgL_arg1815z00_2160 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Integrate/free.scm 46 */
														long BgL_arg1817z00_2162;

														BgL_arg1817z00_2162 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2138);
														BgL_arg1816z00_2161 =
															(BgL_arg1817z00_2162 - OBJECT_TYPE);
													}
													BgL_oclassz00_2152 =
														VECTOR_REF(BgL_arg1815z00_2160,
														BgL_arg1816z00_2161);
												}
												{	/* Integrate/free.scm 46 */
													bool_t BgL__ortest_1115z00_2153;

													BgL__ortest_1115z00_2153 =
														(BgL_classz00_2136 == BgL_oclassz00_2152);
													if (BgL__ortest_1115z00_2153)
														{	/* Integrate/free.scm 46 */
															BgL_res1847z00_2169 = BgL__ortest_1115z00_2153;
														}
													else
														{	/* Integrate/free.scm 46 */
															long BgL_odepthz00_2154;

															{	/* Integrate/free.scm 46 */
																obj_t BgL_arg1804z00_2155;

																BgL_arg1804z00_2155 = (BgL_oclassz00_2152);
																BgL_odepthz00_2154 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2155);
															}
															if ((3L < BgL_odepthz00_2154))
																{	/* Integrate/free.scm 46 */
																	obj_t BgL_arg1802z00_2157;

																	{	/* Integrate/free.scm 46 */
																		obj_t BgL_arg1803z00_2158;

																		BgL_arg1803z00_2158 = (BgL_oclassz00_2152);
																		BgL_arg1802z00_2157 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2158, 3L);
																	}
																	BgL_res1847z00_2169 =
																		(BgL_arg1802z00_2157 == BgL_classz00_2136);
																}
															else
																{	/* Integrate/free.scm 46 */
																	BgL_res1847z00_2169 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1921z00_2727 = BgL_res1847z00_2169;
										}
								}
							}
							if (BgL_test1921z00_2727)
								{	/* Integrate/free.scm 47 */
									obj_t BgL_vz00_2171;

									BgL_vz00_2171 = BINT(BGl_za2roundza2z00zzintegrate_freez00);
									{
										BgL_sexitzf2iinfozf2_bglt BgL_auxz00_2752;

										{
											obj_t BgL_auxz00_2753;

											{	/* Integrate/free.scm 47 */
												BgL_objectz00_bglt BgL_tmpz00_2754;

												BgL_tmpz00_2754 =
													((BgL_objectz00_bglt)
													((BgL_sexitz00_bglt) BgL_infoz00_1621));
												BgL_auxz00_2753 = BGL_OBJECT_WIDENING(BgL_tmpz00_2754);
											}
											BgL_auxz00_2752 =
												((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_2753);
										}
										return
											((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2752))->
												BgL_fzd2markzd2) = ((obj_t) BgL_vz00_2171), BUNSPEC);
									}
								}
							else
								{	/* Integrate/free.scm 46 */
									return BFALSE;
								}
						}
				}
			}
		}

	}



/* bind-variable! */
	obj_t BGl_bindzd2variablez12zc0zzintegrate_freez00(BgL_localz00_bglt
		BgL_localz00_4, BgL_localz00_bglt BgL_integratorz00_5)
	{
		{	/* Integrate/free.scm 52 */
			{	/* Integrate/free.scm 53 */
				BgL_valuez00_bglt BgL_finfoz00_1624;

				BgL_finfoz00_1624 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_integratorz00_5)))->BgL_valuez00);
				{	/* Integrate/free.scm 54 */
					obj_t BgL_arg1318z00_1625;

					{	/* Integrate/free.scm 54 */
						obj_t BgL_arg1319z00_1626;

						{
							BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2762;

							{
								obj_t BgL_auxz00_2763;

								{	/* Integrate/free.scm 54 */
									BgL_objectz00_bglt BgL_tmpz00_2764;

									BgL_tmpz00_2764 =
										((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) BgL_finfoz00_1624));
									BgL_auxz00_2763 = BGL_OBJECT_WIDENING(BgL_tmpz00_2764);
								}
								BgL_auxz00_2762 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2763);
							}
							BgL_arg1319z00_1626 =
								(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2762))->
								BgL_boundz00);
						}
						BgL_arg1318z00_1625 =
							MAKE_YOUNG_PAIR(((obj_t) BgL_localz00_4), BgL_arg1319z00_1626);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2772;

						{
							obj_t BgL_auxz00_2773;

							{	/* Integrate/free.scm 54 */
								BgL_objectz00_bglt BgL_tmpz00_2774;

								BgL_tmpz00_2774 =
									((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_finfoz00_1624));
								BgL_auxz00_2773 = BGL_OBJECT_WIDENING(BgL_tmpz00_2774);
							}
							BgL_auxz00_2772 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2773);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2772))->
								BgL_boundz00) = ((obj_t) BgL_arg1318z00_1625), BUNSPEC);
					}
				}
				return BGl_markzd2variablez12zc0zzintegrate_freez00(BgL_localz00_4);
			}
		}

	}



/* free-variable? */
	obj_t BGl_freezd2variablezf3z21zzintegrate_freez00(obj_t BgL_localz00_6)
	{
		{	/* Integrate/free.scm 60 */
			{	/* Integrate/free.scm 61 */
				BgL_valuez00_bglt BgL_infoz00_1627;

				BgL_infoz00_1627 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_localz00_bglt) BgL_localz00_6))))->BgL_valuez00);
				{	/* Integrate/free.scm 63 */
					bool_t BgL_test1925z00_2784;

					{	/* Integrate/free.scm 63 */
						obj_t BgL_classz00_2179;

						BgL_classz00_2179 = BGl_svarzf2Iinfozf2zzintegrate_infoz00;
						{	/* Integrate/free.scm 63 */
							BgL_objectz00_bglt BgL_arg1807z00_2181;

							{	/* Integrate/free.scm 63 */
								obj_t BgL_tmpz00_2785;

								BgL_tmpz00_2785 =
									((obj_t) ((BgL_objectz00_bglt) BgL_infoz00_1627));
								BgL_arg1807z00_2181 = (BgL_objectz00_bglt) (BgL_tmpz00_2785);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Integrate/free.scm 63 */
									long BgL_idxz00_2187;

									BgL_idxz00_2187 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2181);
									BgL_test1925z00_2784 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2187 + 3L)) == BgL_classz00_2179);
								}
							else
								{	/* Integrate/free.scm 63 */
									bool_t BgL_res1848z00_2212;

									{	/* Integrate/free.scm 63 */
										obj_t BgL_oclassz00_2195;

										{	/* Integrate/free.scm 63 */
											obj_t BgL_arg1815z00_2203;
											long BgL_arg1816z00_2204;

											BgL_arg1815z00_2203 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Integrate/free.scm 63 */
												long BgL_arg1817z00_2205;

												BgL_arg1817z00_2205 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2181);
												BgL_arg1816z00_2204 =
													(BgL_arg1817z00_2205 - OBJECT_TYPE);
											}
											BgL_oclassz00_2195 =
												VECTOR_REF(BgL_arg1815z00_2203, BgL_arg1816z00_2204);
										}
										{	/* Integrate/free.scm 63 */
											bool_t BgL__ortest_1115z00_2196;

											BgL__ortest_1115z00_2196 =
												(BgL_classz00_2179 == BgL_oclassz00_2195);
											if (BgL__ortest_1115z00_2196)
												{	/* Integrate/free.scm 63 */
													BgL_res1848z00_2212 = BgL__ortest_1115z00_2196;
												}
											else
												{	/* Integrate/free.scm 63 */
													long BgL_odepthz00_2197;

													{	/* Integrate/free.scm 63 */
														obj_t BgL_arg1804z00_2198;

														BgL_arg1804z00_2198 = (BgL_oclassz00_2195);
														BgL_odepthz00_2197 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2198);
													}
													if ((3L < BgL_odepthz00_2197))
														{	/* Integrate/free.scm 63 */
															obj_t BgL_arg1802z00_2200;

															{	/* Integrate/free.scm 63 */
																obj_t BgL_arg1803z00_2201;

																BgL_arg1803z00_2201 = (BgL_oclassz00_2195);
																BgL_arg1802z00_2200 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2201,
																	3L);
															}
															BgL_res1848z00_2212 =
																(BgL_arg1802z00_2200 == BgL_classz00_2179);
														}
													else
														{	/* Integrate/free.scm 63 */
															BgL_res1848z00_2212 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1925z00_2784 = BgL_res1848z00_2212;
								}
						}
					}
					if (BgL_test1925z00_2784)
						{	/* Integrate/free.scm 64 */
							bool_t BgL_test1929z00_2808;

							{	/* Integrate/free.scm 64 */
								obj_t BgL_arg1323z00_1631;

								{
									BgL_svarzf2iinfozf2_bglt BgL_auxz00_2809;

									{
										obj_t BgL_auxz00_2810;

										{	/* Integrate/free.scm 64 */
											BgL_objectz00_bglt BgL_tmpz00_2811;

											BgL_tmpz00_2811 =
												((BgL_objectz00_bglt)
												((BgL_svarz00_bglt) BgL_infoz00_1627));
											BgL_auxz00_2810 = BGL_OBJECT_WIDENING(BgL_tmpz00_2811);
										}
										BgL_auxz00_2809 =
											((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_2810);
									}
									BgL_arg1323z00_1631 =
										(((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2809))->
										BgL_fzd2markzd2);
								}
								BgL_test1929z00_2808 =
									(BgL_arg1323z00_1631 ==
									BINT(BGl_za2roundza2z00zzintegrate_freez00));
							}
							if (BgL_test1929z00_2808)
								{	/* Integrate/free.scm 64 */
									return BFALSE;
								}
							else
								{	/* Integrate/free.scm 64 */
									return BTRUE;
								}
						}
					else
						{	/* Integrate/free.scm 65 */
							bool_t BgL_test1930z00_2819;

							{	/* Integrate/free.scm 65 */
								obj_t BgL_classz00_2215;

								BgL_classz00_2215 = BGl_sexitzf2Iinfozf2zzintegrate_infoz00;
								{	/* Integrate/free.scm 65 */
									BgL_objectz00_bglt BgL_arg1807z00_2217;

									{	/* Integrate/free.scm 65 */
										obj_t BgL_tmpz00_2820;

										BgL_tmpz00_2820 =
											((obj_t) ((BgL_objectz00_bglt) BgL_infoz00_1627));
										BgL_arg1807z00_2217 =
											(BgL_objectz00_bglt) (BgL_tmpz00_2820);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Integrate/free.scm 65 */
											long BgL_idxz00_2223;

											BgL_idxz00_2223 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2217);
											BgL_test1930z00_2819 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2223 + 3L)) == BgL_classz00_2215);
										}
									else
										{	/* Integrate/free.scm 65 */
											bool_t BgL_res1849z00_2248;

											{	/* Integrate/free.scm 65 */
												obj_t BgL_oclassz00_2231;

												{	/* Integrate/free.scm 65 */
													obj_t BgL_arg1815z00_2239;
													long BgL_arg1816z00_2240;

													BgL_arg1815z00_2239 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Integrate/free.scm 65 */
														long BgL_arg1817z00_2241;

														BgL_arg1817z00_2241 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2217);
														BgL_arg1816z00_2240 =
															(BgL_arg1817z00_2241 - OBJECT_TYPE);
													}
													BgL_oclassz00_2231 =
														VECTOR_REF(BgL_arg1815z00_2239,
														BgL_arg1816z00_2240);
												}
												{	/* Integrate/free.scm 65 */
													bool_t BgL__ortest_1115z00_2232;

													BgL__ortest_1115z00_2232 =
														(BgL_classz00_2215 == BgL_oclassz00_2231);
													if (BgL__ortest_1115z00_2232)
														{	/* Integrate/free.scm 65 */
															BgL_res1849z00_2248 = BgL__ortest_1115z00_2232;
														}
													else
														{	/* Integrate/free.scm 65 */
															long BgL_odepthz00_2233;

															{	/* Integrate/free.scm 65 */
																obj_t BgL_arg1804z00_2234;

																BgL_arg1804z00_2234 = (BgL_oclassz00_2231);
																BgL_odepthz00_2233 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2234);
															}
															if ((3L < BgL_odepthz00_2233))
																{	/* Integrate/free.scm 65 */
																	obj_t BgL_arg1802z00_2236;

																	{	/* Integrate/free.scm 65 */
																		obj_t BgL_arg1803z00_2237;

																		BgL_arg1803z00_2237 = (BgL_oclassz00_2231);
																		BgL_arg1802z00_2236 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2237, 3L);
																	}
																	BgL_res1849z00_2248 =
																		(BgL_arg1802z00_2236 == BgL_classz00_2215);
																}
															else
																{	/* Integrate/free.scm 65 */
																	BgL_res1849z00_2248 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1930z00_2819 = BgL_res1849z00_2248;
										}
								}
							}
							if (BgL_test1930z00_2819)
								{	/* Integrate/free.scm 66 */
									bool_t BgL_test1934z00_2843;

									{	/* Integrate/free.scm 66 */
										obj_t BgL_arg1327z00_1635;

										{
											BgL_sexitzf2iinfozf2_bglt BgL_auxz00_2844;

											{
												obj_t BgL_auxz00_2845;

												{	/* Integrate/free.scm 66 */
													BgL_objectz00_bglt BgL_tmpz00_2846;

													BgL_tmpz00_2846 =
														((BgL_objectz00_bglt)
														((BgL_sexitz00_bglt) BgL_infoz00_1627));
													BgL_auxz00_2845 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2846);
												}
												BgL_auxz00_2844 =
													((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_2845);
											}
											BgL_arg1327z00_1635 =
												(((BgL_sexitzf2iinfozf2_bglt)
													COBJECT(BgL_auxz00_2844))->BgL_fzd2markzd2);
										}
										BgL_test1934z00_2843 =
											(BgL_arg1327z00_1635 ==
											BINT(BGl_za2roundza2z00zzintegrate_freez00));
									}
									if (BgL_test1934z00_2843)
										{	/* Integrate/free.scm 66 */
											return BFALSE;
										}
									else
										{	/* Integrate/free.scm 66 */
											return BTRUE;
										}
								}
							else
								{	/* Integrate/free.scm 70 */
									obj_t BgL_arg1328z00_1636;

									BgL_arg1328z00_1636 =
										MAKE_YOUNG_PAIR(BgL_localz00_6,
										BGl_shapez00zztools_shapez00(BgL_localz00_6));
									return
										BGl_errorz00zz__errorz00
										(BGl_string1859z00zzintegrate_freez00,
										BGl_string1860z00zzintegrate_freez00, BgL_arg1328z00_1636);
								}
						}
				}
			}
		}

	}



/* get-free-vars */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2freezd2varsz00zzintegrate_freez00(BgL_nodez00_bglt BgL_nodez00_7,
		BgL_localz00_bglt BgL_integratorz00_8)
	{
		{	/* Integrate/free.scm 78 */
			{	/* Integrate/free.scm 81 */
				obj_t BgL_freez00_1638;

				{	/* Integrate/free.scm 81 */
					BgL_sfunz00_bglt BgL_oz00_2252;

					BgL_oz00_2252 =
						((BgL_sfunz00_bglt)
						(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_integratorz00_8)))->
							BgL_valuez00));
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2860;

						{
							obj_t BgL_auxz00_2861;

							{	/* Integrate/free.scm 81 */
								BgL_objectz00_bglt BgL_tmpz00_2862;

								BgL_tmpz00_2862 = ((BgL_objectz00_bglt) BgL_oz00_2252);
								BgL_auxz00_2861 = BGL_OBJECT_WIDENING(BgL_tmpz00_2862);
							}
							BgL_auxz00_2860 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2861);
						}
						BgL_freez00_1638 =
							(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2860))->
							BgL_freez00);
					}
				}
				{	/* Integrate/free.scm 82 */
					bool_t BgL_test1935z00_2867;

					if (NULLP(BgL_freez00_1638))
						{	/* Integrate/free.scm 82 */
							BgL_test1935z00_2867 = ((bool_t) 1);
						}
					else
						{	/* Integrate/free.scm 82 */
							BgL_test1935z00_2867 = PAIRP(BgL_freez00_1638);
						}
					if (BgL_test1935z00_2867)
						{	/* Integrate/free.scm 82 */
							return BgL_freez00_1638;
						}
					else
						{	/* Integrate/free.scm 84 */
							obj_t BgL_freez00_1641;

							BgL_freez00_1641 =
								BGl_internalzd2getzd2freezd2varsz12zc0zzintegrate_freez00
								(BgL_nodez00_7, BgL_integratorz00_8);
							{	/* Integrate/free.scm 85 */
								BgL_valuez00_bglt BgL_arg1332z00_1642;

								BgL_arg1332z00_1642 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_integratorz00_8)))->
									BgL_valuez00);
								{
									BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2874;

									{
										obj_t BgL_auxz00_2875;

										{	/* Integrate/free.scm 85 */
											BgL_objectz00_bglt BgL_tmpz00_2876;

											BgL_tmpz00_2876 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_arg1332z00_1642));
											BgL_auxz00_2875 = BGL_OBJECT_WIDENING(BgL_tmpz00_2876);
										}
										BgL_auxz00_2874 =
											((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2875);
									}
									((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2874))->
											BgL_freez00) = ((obj_t) BgL_freez00_1641), BUNSPEC);
								}
							}
							return BgL_freez00_1641;
						}
				}
			}
		}

	}



/* &get-free-vars */
	obj_t BGl_z62getzd2freezd2varsz62zzintegrate_freez00(obj_t BgL_envz00_2503,
		obj_t BgL_nodez00_2504, obj_t BgL_integratorz00_2505)
	{
		{	/* Integrate/free.scm 78 */
			return
				BGl_getzd2freezd2varsz00zzintegrate_freez00(
				((BgL_nodez00_bglt) BgL_nodez00_2504),
				((BgL_localz00_bglt) BgL_integratorz00_2505));
		}

	}



/* internal-get-free-vars! */
	obj_t
		BGl_internalzd2getzd2freezd2varsz12zc0zzintegrate_freez00(BgL_nodez00_bglt
		BgL_nodez00_9, BgL_localz00_bglt BgL_integratorz00_10)
	{
		{	/* Integrate/free.scm 99 */
			BGl_za2roundza2z00zzintegrate_freez00 =
				(BGl_za2roundza2z00zzintegrate_freez00 + 1L);
			BGl_za2integratorza2z00zzintegrate_freez00 =
				((obj_t) BgL_integratorz00_10);
			{	/* Integrate/free.scm 103 */
				obj_t BgL_g1258z00_1645;

				BgL_g1258z00_1645 =
					(((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_integratorz00_10)))->
									BgL_valuez00))))->BgL_argsz00);
				{
					obj_t BgL_l1256z00_1647;

					BgL_l1256z00_1647 = BgL_g1258z00_1645;
				BgL_zc3z04anonymousza31334ze3z87_1648:
					if (PAIRP(BgL_l1256z00_1647))
						{	/* Integrate/free.scm 104 */
							{	/* Integrate/free.scm 103 */
								obj_t BgL_lz00_1650;

								BgL_lz00_1650 = CAR(BgL_l1256z00_1647);
								BGl_bindzd2variablez12zc0zzintegrate_freez00(
									((BgL_localz00_bglt) BgL_lz00_1650), BgL_integratorz00_10);
							}
							{
								obj_t BgL_l1256z00_2896;

								BgL_l1256z00_2896 = CDR(BgL_l1256z00_1647);
								BgL_l1256z00_1647 = BgL_l1256z00_2896;
								goto BgL_zc3z04anonymousza31334ze3z87_1648;
							}
						}
					else
						{	/* Integrate/free.scm 104 */
							((bool_t) 1);
						}
				}
			}
			return BGl_nodezd2freezd2zzintegrate_freez00(BgL_nodez00_9, BNIL);
		}

	}



/* node-free* */
	obj_t BGl_nodezd2freeza2z70zzintegrate_freez00(obj_t BgL_nodeza2za2_53,
		obj_t BgL_freez00_54)
	{
		{	/* Integrate/free.scm 285 */
			{
				obj_t BgL_nodeza2za2_1655;
				obj_t BgL_freez00_1656;

				BgL_nodeza2za2_1655 = BgL_nodeza2za2_53;
				BgL_freez00_1656 = BgL_freez00_54;
			BgL_zc3z04anonymousza31341ze3z87_1657:
				if (NULLP(BgL_nodeza2za2_1655))
					{	/* Integrate/free.scm 288 */
						return BgL_freez00_1656;
					}
				else
					{	/* Integrate/free.scm 290 */
						obj_t BgL_arg1343z00_1659;
						obj_t BgL_arg1346z00_1660;

						BgL_arg1343z00_1659 = CDR(((obj_t) BgL_nodeza2za2_1655));
						{	/* Integrate/free.scm 291 */
							obj_t BgL_arg1348z00_1661;

							BgL_arg1348z00_1661 = CAR(((obj_t) BgL_nodeza2za2_1655));
							BgL_arg1346z00_1660 =
								BGl_nodezd2freezd2zzintegrate_freez00(
								((BgL_nodez00_bglt) BgL_arg1348z00_1661), BgL_freez00_1656);
						}
						{
							obj_t BgL_freez00_2908;
							obj_t BgL_nodeza2za2_2907;

							BgL_nodeza2za2_2907 = BgL_arg1343z00_1659;
							BgL_freez00_2908 = BgL_arg1346z00_1660;
							BgL_freez00_1656 = BgL_freez00_2908;
							BgL_nodeza2za2_1655 = BgL_nodeza2za2_2907;
							goto BgL_zc3z04anonymousza31341ze3z87_1657;
						}
					}
			}
		}

	}



/* free-from */
	BGL_EXPORTED_DEF obj_t BGl_freezd2fromzd2zzintegrate_freez00(obj_t
		BgL_setsz00_55, BgL_localz00_bglt BgL_integratorz00_56)
	{
		{	/* Integrate/free.scm 296 */
			BGl_za2roundza2z00zzintegrate_freez00 =
				(BGl_za2roundza2z00zzintegrate_freez00 + 1L);
			{	/* Integrate/free.scm 299 */
				BgL_valuez00_bglt BgL_finfoz00_1663;

				BgL_finfoz00_1663 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_integratorz00_56)))->BgL_valuez00);
				{	/* Integrate/free.scm 306 */
					obj_t BgL_g1264z00_1664;

					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2912;

						{
							obj_t BgL_auxz00_2913;

							{	/* Integrate/free.scm 306 */
								BgL_objectz00_bglt BgL_tmpz00_2914;

								BgL_tmpz00_2914 =
									((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_finfoz00_1663));
								BgL_auxz00_2913 = BGL_OBJECT_WIDENING(BgL_tmpz00_2914);
							}
							BgL_auxz00_2912 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2913);
						}
						BgL_g1264z00_1664 =
							(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2912))->
							BgL_boundz00);
					}
					{
						obj_t BgL_l1262z00_1666;

						BgL_l1262z00_1666 = BgL_g1264z00_1664;
					BgL_zc3z04anonymousza31349ze3z87_1667:
						if (PAIRP(BgL_l1262z00_1666))
							{	/* Integrate/free.scm 306 */
								{	/* Integrate/free.scm 306 */
									obj_t BgL_arg1351z00_1669;

									BgL_arg1351z00_1669 = CAR(BgL_l1262z00_1666);
									BGl_markzd2variablez12zc0zzintegrate_freez00(
										((BgL_localz00_bglt) BgL_arg1351z00_1669));
								}
								{
									obj_t BgL_l1262z00_2925;

									BgL_l1262z00_2925 = CDR(BgL_l1262z00_1666);
									BgL_l1262z00_1666 = BgL_l1262z00_2925;
									goto BgL_zc3z04anonymousza31349ze3z87_1667;
								}
							}
						else
							{	/* Integrate/free.scm 306 */
								((bool_t) 1);
							}
					}
				}
			}
			if (NULLP(BgL_setsz00_55))
				{	/* Integrate/free.scm 308 */
					return BNIL;
				}
			else
				{	/* Integrate/free.scm 308 */
					obj_t BgL_head1267z00_1674;

					BgL_head1267z00_1674 = MAKE_YOUNG_PAIR(BNIL, BNIL);
					{
						obj_t BgL_l1265z00_1676;
						obj_t BgL_tail1268z00_1677;

						BgL_l1265z00_1676 = BgL_setsz00_55;
						BgL_tail1268z00_1677 = BgL_head1267z00_1674;
					BgL_zc3z04anonymousza31355ze3z87_1678:
						if (NULLP(BgL_l1265z00_1676))
							{	/* Integrate/free.scm 308 */
								return CDR(BgL_head1267z00_1674);
							}
						else
							{	/* Integrate/free.scm 308 */
								obj_t BgL_newtail1269z00_1680;

								{	/* Integrate/free.scm 308 */
									obj_t BgL_arg1364z00_1682;

									{	/* Integrate/free.scm 308 */
										obj_t BgL_setz00_1683;

										BgL_setz00_1683 = CAR(((obj_t) BgL_l1265z00_1676));
										{
											obj_t BgL_setz00_1686;
											obj_t BgL_resz00_1687;

											BgL_setz00_1686 = BgL_setz00_1683;
											BgL_resz00_1687 = BNIL;
										BgL_zc3z04anonymousza31365ze3z87_1688:
											if (NULLP(BgL_setz00_1686))
												{	/* Integrate/free.scm 312 */
													BgL_arg1364z00_1682 = BgL_resz00_1687;
												}
											else
												{	/* Integrate/free.scm 314 */
													bool_t BgL_test1943z00_2937;

													{	/* Integrate/free.scm 314 */
														obj_t BgL_arg1377z00_1696;

														BgL_arg1377z00_1696 =
															CAR(((obj_t) BgL_setz00_1686));
														BgL_test1943z00_2937 =
															CBOOL(BGl_freezd2variablezf3z21zzintegrate_freez00
															(BgL_arg1377z00_1696));
													}
													if (BgL_test1943z00_2937)
														{	/* Integrate/free.scm 315 */
															obj_t BgL_arg1370z00_1692;
															obj_t BgL_arg1371z00_1693;

															BgL_arg1370z00_1692 =
																CDR(((obj_t) BgL_setz00_1686));
															{	/* Integrate/free.scm 315 */
																obj_t BgL_arg1375z00_1694;

																BgL_arg1375z00_1694 =
																	CAR(((obj_t) BgL_setz00_1686));
																BgL_arg1371z00_1693 =
																	MAKE_YOUNG_PAIR(BgL_arg1375z00_1694,
																	BgL_resz00_1687);
															}
															{
																obj_t BgL_resz00_2948;
																obj_t BgL_setz00_2947;

																BgL_setz00_2947 = BgL_arg1370z00_1692;
																BgL_resz00_2948 = BgL_arg1371z00_1693;
																BgL_resz00_1687 = BgL_resz00_2948;
																BgL_setz00_1686 = BgL_setz00_2947;
																goto BgL_zc3z04anonymousza31365ze3z87_1688;
															}
														}
													else
														{	/* Integrate/free.scm 317 */
															obj_t BgL_arg1376z00_1695;

															BgL_arg1376z00_1695 =
																CDR(((obj_t) BgL_setz00_1686));
															{
																obj_t BgL_setz00_2951;

																BgL_setz00_2951 = BgL_arg1376z00_1695;
																BgL_setz00_1686 = BgL_setz00_2951;
																goto BgL_zc3z04anonymousza31365ze3z87_1688;
															}
														}
												}
										}
									}
									BgL_newtail1269z00_1680 =
										MAKE_YOUNG_PAIR(BgL_arg1364z00_1682, BNIL);
								}
								SET_CDR(BgL_tail1268z00_1677, BgL_newtail1269z00_1680);
								{	/* Integrate/free.scm 308 */
									obj_t BgL_arg1361z00_1681;

									BgL_arg1361z00_1681 = CDR(((obj_t) BgL_l1265z00_1676));
									{
										obj_t BgL_tail1268z00_2957;
										obj_t BgL_l1265z00_2956;

										BgL_l1265z00_2956 = BgL_arg1361z00_1681;
										BgL_tail1268z00_2957 = BgL_newtail1269z00_1680;
										BgL_tail1268z00_1677 = BgL_tail1268z00_2957;
										BgL_l1265z00_1676 = BgL_l1265z00_2956;
										goto BgL_zc3z04anonymousza31355ze3z87_1678;
									}
								}
							}
					}
				}
		}

	}



/* &free-from */
	obj_t BGl_z62freezd2fromzb0zzintegrate_freez00(obj_t BgL_envz00_2506,
		obj_t BgL_setsz00_2507, obj_t BgL_integratorz00_2508)
	{
		{	/* Integrate/free.scm 296 */
			return
				BGl_freezd2fromzd2zzintegrate_freez00(BgL_setsz00_2507,
				((BgL_localz00_bglt) BgL_integratorz00_2508));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzintegrate_freez00(void)
	{
		{	/* Integrate/free.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzintegrate_freez00(void)
	{
		{	/* Integrate/free.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00,
				BGl_proc1861z00zzintegrate_freez00, BGl_nodez00zzast_nodez00,
				BGl_string1862z00zzintegrate_freez00);
		}

	}



/* &node-free1270 */
	obj_t BGl_z62nodezd2free1270zb0zzintegrate_freez00(obj_t BgL_envz00_2510,
		obj_t BgL_nodez00_2511, obj_t BgL_freez00_2512)
	{
		{	/* Integrate/free.scm 111 */
			return BgL_freez00_2512;
		}

	}



/* node-free */
	obj_t BGl_nodezd2freezd2zzintegrate_freez00(BgL_nodez00_bglt BgL_nodez00_11,
		obj_t BgL_freez00_12)
	{
		{	/* Integrate/free.scm 111 */
			{	/* Integrate/free.scm 111 */
				obj_t BgL_method1271z00_1704;

				{	/* Integrate/free.scm 111 */
					obj_t BgL_res1854z00_2308;

					{	/* Integrate/free.scm 111 */
						long BgL_objzd2classzd2numz00_2279;

						BgL_objzd2classzd2numz00_2279 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_11));
						{	/* Integrate/free.scm 111 */
							obj_t BgL_arg1811z00_2280;

							BgL_arg1811z00_2280 =
								PROCEDURE_REF(BGl_nodezd2freezd2envz00zzintegrate_freez00,
								(int) (1L));
							{	/* Integrate/free.scm 111 */
								int BgL_offsetz00_2283;

								BgL_offsetz00_2283 = (int) (BgL_objzd2classzd2numz00_2279);
								{	/* Integrate/free.scm 111 */
									long BgL_offsetz00_2284;

									BgL_offsetz00_2284 =
										((long) (BgL_offsetz00_2283) - OBJECT_TYPE);
									{	/* Integrate/free.scm 111 */
										long BgL_modz00_2285;

										BgL_modz00_2285 =
											(BgL_offsetz00_2284 >> (int) ((long) ((int) (4L))));
										{	/* Integrate/free.scm 111 */
											long BgL_restz00_2287;

											BgL_restz00_2287 =
												(BgL_offsetz00_2284 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Integrate/free.scm 111 */

												{	/* Integrate/free.scm 111 */
													obj_t BgL_bucketz00_2289;

													BgL_bucketz00_2289 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2280), BgL_modz00_2285);
													BgL_res1854z00_2308 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2289), BgL_restz00_2287);
					}}}}}}}}
					BgL_method1271z00_1704 = BgL_res1854z00_2308;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1271z00_1704,
					((obj_t) BgL_nodez00_11), BgL_freez00_12);
			}
		}

	}



/* &node-free */
	obj_t BGl_z62nodezd2freezb0zzintegrate_freez00(obj_t BgL_envz00_2513,
		obj_t BgL_nodez00_2514, obj_t BgL_freez00_2515)
	{
		{	/* Integrate/free.scm 111 */
			return
				BGl_nodezd2freezd2zzintegrate_freez00(
				((BgL_nodez00_bglt) BgL_nodez00_2514), BgL_freez00_2515);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzintegrate_freez00(void)
	{
		{	/* Integrate/free.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00, BGl_varz00zzast_nodez00,
				BGl_proc1863z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00,
				BGl_closurez00zzast_nodez00, BGl_proc1865z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00,
				BGl_sequencez00zzast_nodez00, BGl_proc1866z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00, BGl_syncz00zzast_nodez00,
				BGl_proc1867z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00, BGl_appz00zzast_nodez00,
				BGl_proc1868z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1869z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00,
				BGl_funcallz00zzast_nodez00, BGl_proc1870z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00,
				BGl_externz00zzast_nodez00, BGl_proc1871z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00, BGl_castz00zzast_nodez00,
				BGl_proc1872z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00, BGl_setqz00zzast_nodez00,
				BGl_proc1873z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1874z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00, BGl_failz00zzast_nodez00,
				BGl_proc1875z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00,
				BGl_switchz00zzast_nodez00, BGl_proc1876z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1877z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1878z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1879z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1880z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1881z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1882z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2freezd2envz00zzintegrate_freez00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1883z00zzintegrate_freez00,
				BGl_string1864z00zzintegrate_freez00);
		}

	}



/* &node-free-box-set!1311 */
	obj_t BGl_z62nodezd2freezd2boxzd2setz121311za2zzintegrate_freez00(obj_t
		BgL_envz00_2536, obj_t BgL_nodez00_2537, obj_t BgL_freez00_2538)
	{
		{	/* Integrate/free.scm 278 */
			{	/* Integrate/free.scm 280 */
				BgL_varz00_bglt BgL_arg1702z00_2598;
				obj_t BgL_arg1703z00_2599;

				BgL_arg1702z00_2598 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2537)))->BgL_varz00);
				BgL_arg1703z00_2599 =
					BGl_nodezd2freezd2zzintegrate_freez00(
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2537)))->BgL_valuez00),
					BgL_freez00_2538);
				return BGl_nodezd2freezd2zzintegrate_freez00(((BgL_nodez00_bglt)
						BgL_arg1702z00_2598), BgL_arg1703z00_2599);
			}
		}

	}



/* &node-free-box-ref1309 */
	obj_t BGl_z62nodezd2freezd2boxzd2ref1309zb0zzintegrate_freez00(obj_t
		BgL_envz00_2539, obj_t BgL_nodez00_2540, obj_t BgL_freez00_2541)
	{
		{	/* Integrate/free.scm 271 */
			{	/* Integrate/free.scm 273 */
				BgL_varz00_bglt BgL_arg1701z00_2601;

				BgL_arg1701z00_2601 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_2540)))->BgL_varz00);
				return
					BGl_nodezd2freezd2zzintegrate_freez00(
					((BgL_nodez00_bglt) BgL_arg1701z00_2601), BgL_freez00_2541);
			}
		}

	}



/* &node-free-make-box1307 */
	obj_t BGl_z62nodezd2freezd2makezd2box1307zb0zzintegrate_freez00(obj_t
		BgL_envz00_2542, obj_t BgL_nodez00_2543, obj_t BgL_freez00_2544)
	{
		{	/* Integrate/free.scm 264 */
			return
				BGl_nodezd2freezd2zzintegrate_freez00(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2543)))->BgL_valuez00),
				BgL_freez00_2544);
		}

	}



/* &node-free-jump-ex-it1305 */
	obj_t BGl_z62nodezd2freezd2jumpzd2exzd2it1305z62zzintegrate_freez00(obj_t
		BgL_envz00_2545, obj_t BgL_nodez00_2546, obj_t BgL_freez00_2547)
	{
		{	/* Integrate/free.scm 257 */
			return
				BGl_nodezd2freezd2zzintegrate_freez00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2546)))->BgL_exitz00),
				BGl_nodezd2freezd2zzintegrate_freez00(
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2546)))->
						BgL_valuez00), BgL_freez00_2547));
		}

	}



/* &node-free-set-ex-it1303 */
	obj_t BGl_z62nodezd2freezd2setzd2exzd2it1303z62zzintegrate_freez00(obj_t
		BgL_envz00_2548, obj_t BgL_nodez00_2549, obj_t BgL_freez00_2550)
	{
		{	/* Integrate/free.scm 249 */
			{	/* Integrate/free.scm 251 */
				BgL_variablez00_bglt BgL_arg1675z00_2605;

				BgL_arg1675z00_2605 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
										((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2549)))->
								BgL_varz00)))->BgL_variablez00);
				BGl_bindzd2variablez12zc0zzintegrate_freez00(((BgL_localz00_bglt)
						BgL_arg1675z00_2605),
					((BgL_localz00_bglt) BGl_za2integratorza2z00zzintegrate_freez00));
			}
			return
				BGl_nodezd2freezd2zzintegrate_freez00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2549)))->BgL_bodyz00),
				BGl_nodezd2freezd2zzintegrate_freez00(
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2549)))->
						BgL_onexitz00), BgL_freez00_2550));
		}

	}



/* &node-free-let-var1301 */
	obj_t BGl_z62nodezd2freezd2letzd2var1301zb0zzintegrate_freez00(obj_t
		BgL_envz00_2551, obj_t BgL_nodez00_2552, obj_t BgL_freez00_2553)
	{
		{	/* Integrate/free.scm 235 */
			{
				obj_t BgL_bindingsz00_2608;
				obj_t BgL_freez00_2609;

				BgL_bindingsz00_2608 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2552)))->BgL_bindingsz00);
				BgL_freez00_2609 = BgL_freez00_2553;
			BgL_loopz00_2607:
				if (NULLP(BgL_bindingsz00_2608))
					{	/* Integrate/free.scm 239 */
						return
							BGl_nodezd2freezd2zzintegrate_freez00(
							(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_nodez00_2552)))->BgL_bodyz00),
							BgL_freez00_2609);
					}
				else
					{	/* Integrate/free.scm 239 */
						{	/* Integrate/free.scm 242 */
							obj_t BgL_arg1646z00_2610;

							{	/* Integrate/free.scm 242 */
								obj_t BgL_pairz00_2611;

								BgL_pairz00_2611 = CAR(((obj_t) BgL_bindingsz00_2608));
								BgL_arg1646z00_2610 = CAR(BgL_pairz00_2611);
							}
							BGl_bindzd2variablez12zc0zzintegrate_freez00(
								((BgL_localz00_bglt) BgL_arg1646z00_2610),
								((BgL_localz00_bglt)
									BGl_za2integratorza2z00zzintegrate_freez00));
						}
						{	/* Integrate/free.scm 243 */
							obj_t BgL_arg1651z00_2612;
							obj_t BgL_arg1654z00_2613;

							BgL_arg1651z00_2612 = CDR(((obj_t) BgL_bindingsz00_2608));
							{	/* Integrate/free.scm 244 */
								obj_t BgL_arg1661z00_2614;

								{	/* Integrate/free.scm 244 */
									obj_t BgL_pairz00_2615;

									BgL_pairz00_2615 = CAR(((obj_t) BgL_bindingsz00_2608));
									BgL_arg1661z00_2614 = CDR(BgL_pairz00_2615);
								}
								BgL_arg1654z00_2613 =
									BGl_nodezd2freezd2zzintegrate_freez00(
									((BgL_nodez00_bglt) BgL_arg1661z00_2614), BgL_freez00_2609);
							}
							{
								obj_t BgL_freez00_3065;
								obj_t BgL_bindingsz00_3064;

								BgL_bindingsz00_3064 = BgL_arg1651z00_2612;
								BgL_freez00_3065 = BgL_arg1654z00_2613;
								BgL_freez00_2609 = BgL_freez00_3065;
								BgL_bindingsz00_2608 = BgL_bindingsz00_3064;
								goto BgL_loopz00_2607;
							}
						}
					}
			}
		}

	}



/* &node-free-let-fun1299 */
	obj_t BGl_z62nodezd2freezd2letzd2fun1299zb0zzintegrate_freez00(obj_t
		BgL_envz00_2554, obj_t BgL_nodez00_2555, obj_t BgL_freez00_2556)
	{
		{	/* Integrate/free.scm 218 */
			{	/* Integrate/free.scm 221 */
				obj_t BgL_g1122z00_2617;

				BgL_g1122z00_2617 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2555)))->BgL_localsz00);
				{
					obj_t BgL_localsz00_2619;
					obj_t BgL_freez00_2620;

					BgL_localsz00_2619 = BgL_g1122z00_2617;
					BgL_freez00_2620 = BgL_freez00_2556;
				BgL_liipz00_2618:
					if (NULLP(BgL_localsz00_2619))
						{	/* Integrate/free.scm 223 */
							return
								BGl_nodezd2freezd2zzintegrate_freez00(
								(((BgL_letzd2funzd2_bglt) COBJECT(
											((BgL_letzd2funzd2_bglt) BgL_nodez00_2555)))->
									BgL_bodyz00), BgL_freez00_2620);
						}
					else
						{	/* Integrate/free.scm 225 */
							obj_t BgL_localz00_2621;

							BgL_localz00_2621 = CAR(((obj_t) BgL_localsz00_2619));
							{	/* Integrate/free.scm 225 */
								BgL_valuez00_bglt BgL_funz00_2622;

								BgL_funz00_2622 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_localz00_bglt) BgL_localz00_2621))))->
									BgL_valuez00);
								{	/* Integrate/free.scm 226 */

									{	/* Integrate/free.scm 227 */
										obj_t BgL_g1261z00_2623;

										BgL_g1261z00_2623 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_2622)))->BgL_argsz00);
										{
											obj_t BgL_l1259z00_2625;

											BgL_l1259z00_2625 = BgL_g1261z00_2623;
										BgL_zc3z04anonymousza31614ze3z87_2624:
											if (PAIRP(BgL_l1259z00_2625))
												{	/* Integrate/free.scm 228 */
													{	/* Integrate/free.scm 227 */
														obj_t BgL_lz00_2626;

														BgL_lz00_2626 = CAR(BgL_l1259z00_2625);
														BGl_bindzd2variablez12zc0zzintegrate_freez00(
															((BgL_localz00_bglt) BgL_lz00_2626),
															((BgL_localz00_bglt)
																BGl_za2integratorza2z00zzintegrate_freez00));
													}
													{
														obj_t BgL_l1259z00_3088;

														BgL_l1259z00_3088 = CDR(BgL_l1259z00_2625);
														BgL_l1259z00_2625 = BgL_l1259z00_3088;
														goto BgL_zc3z04anonymousza31614ze3z87_2624;
													}
												}
											else
												{	/* Integrate/free.scm 228 */
													((bool_t) 1);
												}
										}
									}
									{	/* Integrate/free.scm 229 */
										obj_t BgL_arg1625z00_2627;
										obj_t BgL_arg1626z00_2628;

										BgL_arg1625z00_2627 = CDR(((obj_t) BgL_localsz00_2619));
										{	/* Integrate/free.scm 230 */
											obj_t BgL_arg1627z00_2629;

											BgL_arg1627z00_2629 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_2622)))->
												BgL_bodyz00);
											BgL_arg1626z00_2628 =
												BGl_nodezd2freezd2zzintegrate_freez00((
													(BgL_nodez00_bglt) BgL_arg1627z00_2629),
												BgL_freez00_2620);
										}
										{
											obj_t BgL_freez00_3097;
											obj_t BgL_localsz00_3096;

											BgL_localsz00_3096 = BgL_arg1625z00_2627;
											BgL_freez00_3097 = BgL_arg1626z00_2628;
											BgL_freez00_2620 = BgL_freez00_3097;
											BgL_localsz00_2619 = BgL_localsz00_3096;
											goto BgL_liipz00_2618;
										}
									}
								}
							}
						}
				}
			}
		}

	}



/* &node-free-switch1297 */
	obj_t BGl_z62nodezd2freezd2switch1297z62zzintegrate_freez00(obj_t
		BgL_envz00_2557, obj_t BgL_nodez00_2558, obj_t BgL_freez00_2559)
	{
		{	/* Integrate/free.scm 207 */
			{
				obj_t BgL_clausesz00_2632;
				obj_t BgL_freez00_2633;

				BgL_clausesz00_2632 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2558)))->BgL_clausesz00);
				BgL_freez00_2633 = BgL_freez00_2559;
			BgL_loopz00_2631:
				if (NULLP(BgL_clausesz00_2632))
					{	/* Integrate/free.scm 211 */
						return
							BGl_nodezd2freezd2zzintegrate_freez00(
							(((BgL_switchz00_bglt) COBJECT(
										((BgL_switchz00_bglt) BgL_nodez00_2558)))->BgL_testz00),
							BgL_freez00_2633);
					}
				else
					{	/* Integrate/free.scm 213 */
						obj_t BgL_arg1602z00_2634;
						obj_t BgL_arg1605z00_2635;

						BgL_arg1602z00_2634 = CDR(((obj_t) BgL_clausesz00_2632));
						{	/* Integrate/free.scm 213 */
							obj_t BgL_arg1606z00_2636;

							{	/* Integrate/free.scm 213 */
								obj_t BgL_pairz00_2637;

								BgL_pairz00_2637 = CAR(((obj_t) BgL_clausesz00_2632));
								BgL_arg1606z00_2636 = CDR(BgL_pairz00_2637);
							}
							BgL_arg1605z00_2635 =
								BGl_nodezd2freezd2zzintegrate_freez00(
								((BgL_nodez00_bglt) BgL_arg1606z00_2636), BgL_freez00_2633);
						}
						{
							obj_t BgL_freez00_3111;
							obj_t BgL_clausesz00_3110;

							BgL_clausesz00_3110 = BgL_arg1602z00_2634;
							BgL_freez00_3111 = BgL_arg1605z00_2635;
							BgL_freez00_2633 = BgL_freez00_3111;
							BgL_clausesz00_2632 = BgL_clausesz00_3110;
							goto BgL_loopz00_2631;
						}
					}
			}
		}

	}



/* &node-free-fail1295 */
	obj_t BGl_z62nodezd2freezd2fail1295z62zzintegrate_freez00(obj_t
		BgL_envz00_2560, obj_t BgL_nodez00_2561, obj_t BgL_freez00_2562)
	{
		{	/* Integrate/free.scm 200 */
			return
				BGl_nodezd2freezd2zzintegrate_freez00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2561)))->BgL_procz00),
				BGl_nodezd2freezd2zzintegrate_freez00(
					(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2561)))->BgL_msgz00),
					BGl_nodezd2freezd2zzintegrate_freez00(
						(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_2561)))->BgL_objz00),
						BgL_freez00_2562)));
		}

	}



/* &node-free-conditiona1293 */
	obj_t BGl_z62nodezd2freezd2conditiona1293z62zzintegrate_freez00(obj_t
		BgL_envz00_2563, obj_t BgL_nodez00_2564, obj_t BgL_freez00_2565)
	{
		{	/* Integrate/free.scm 193 */
			return
				BGl_nodezd2freezd2zzintegrate_freez00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2564)))->BgL_testz00),
				BGl_nodezd2freezd2zzintegrate_freez00(
					(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2564)))->BgL_truez00),
					BGl_nodezd2freezd2zzintegrate_freez00(
						(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_2564)))->BgL_falsez00),
						BgL_freez00_2565)));
		}

	}



/* &node-free-setq1291 */
	obj_t BGl_z62nodezd2freezd2setq1291z62zzintegrate_freez00(obj_t
		BgL_envz00_2566, obj_t BgL_nodez00_2567, obj_t BgL_freez00_2568)
	{
		{	/* Integrate/free.scm 186 */
			{	/* Integrate/free.scm 188 */
				BgL_varz00_bglt BgL_arg1552z00_2641;
				obj_t BgL_arg1553z00_2642;

				BgL_arg1552z00_2641 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2567)))->BgL_varz00);
				BgL_arg1553z00_2642 =
					BGl_nodezd2freezd2zzintegrate_freez00(
					(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_2567)))->BgL_valuez00),
					BgL_freez00_2568);
				return BGl_nodezd2freezd2zzintegrate_freez00(((BgL_nodez00_bglt)
						BgL_arg1552z00_2641), BgL_arg1553z00_2642);
			}
		}

	}



/* &node-free-cast1289 */
	obj_t BGl_z62nodezd2freezd2cast1289z62zzintegrate_freez00(obj_t
		BgL_envz00_2569, obj_t BgL_nodez00_2570, obj_t BgL_freez00_2571)
	{
		{	/* Integrate/free.scm 179 */
			return
				BGl_nodezd2freezd2zzintegrate_freez00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2570)))->BgL_argz00),
				BgL_freez00_2571);
		}

	}



/* &node-free-extern1287 */
	obj_t BGl_z62nodezd2freezd2extern1287z62zzintegrate_freez00(obj_t
		BgL_envz00_2572, obj_t BgL_nodez00_2573, obj_t BgL_freez00_2574)
	{
		{	/* Integrate/free.scm 172 */
			return
				BGl_nodezd2freeza2z70zzintegrate_freez00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2573)))->BgL_exprza2za2),
				BgL_freez00_2574);
		}

	}



/* &node-free-funcall1285 */
	obj_t BGl_z62nodezd2freezd2funcall1285z62zzintegrate_freez00(obj_t
		BgL_envz00_2575, obj_t BgL_nodez00_2576, obj_t BgL_freez00_2577)
	{
		{	/* Integrate/free.scm 165 */
			return
				BGl_nodezd2freezd2zzintegrate_freez00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2576)))->BgL_funz00),
				BGl_nodezd2freeza2z70zzintegrate_freez00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_2576)))->BgL_argsz00),
					BgL_freez00_2577));
		}

	}



/* &node-free-app-ly1283 */
	obj_t BGl_z62nodezd2freezd2appzd2ly1283zb0zzintegrate_freez00(obj_t
		BgL_envz00_2578, obj_t BgL_nodez00_2579, obj_t BgL_freez00_2580)
	{
		{	/* Integrate/free.scm 158 */
			return
				BGl_nodezd2freezd2zzintegrate_freez00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2579)))->BgL_funz00),
				BGl_nodezd2freezd2zzintegrate_freez00(
					(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_2579)))->BgL_argz00),
					BgL_freez00_2580));
		}

	}



/* &node-free-app1281 */
	obj_t BGl_z62nodezd2freezd2app1281z62zzintegrate_freez00(obj_t
		BgL_envz00_2581, obj_t BgL_nodez00_2582, obj_t BgL_freez00_2583)
	{
		{	/* Integrate/free.scm 151 */
			return
				BGl_nodezd2freeza2z70zzintegrate_freez00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2582)))->BgL_argsz00),
				BgL_freez00_2583);
		}

	}



/* &node-free-sync1279 */
	obj_t BGl_z62nodezd2freezd2sync1279z62zzintegrate_freez00(obj_t
		BgL_envz00_2584, obj_t BgL_nodez00_2585, obj_t BgL_freez00_2586)
	{
		{	/* Integrate/free.scm 144 */
			return
				BGl_nodezd2freezd2zzintegrate_freez00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2585)))->BgL_bodyz00),
				BGl_nodezd2freezd2zzintegrate_freez00(
					(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_2585)))->BgL_prelockz00),
					BGl_nodezd2freezd2zzintegrate_freez00(
						(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_2585)))->BgL_mutexz00),
						BgL_freez00_2586)));
		}

	}



/* &node-free-sequence1277 */
	obj_t BGl_z62nodezd2freezd2sequence1277z62zzintegrate_freez00(obj_t
		BgL_envz00_2587, obj_t BgL_nodez00_2588, obj_t BgL_freez00_2589)
	{
		{	/* Integrate/free.scm 137 */
			return
				BGl_nodezd2freeza2z70zzintegrate_freez00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2588)))->BgL_nodesz00),
				BgL_freez00_2589);
		}

	}



/* &node-free-closure1275 */
	obj_t BGl_z62nodezd2freezd2closure1275z62zzintegrate_freez00(obj_t
		BgL_envz00_2590, obj_t BgL_nodez00_2591, obj_t BgL_freez00_2592)
	{
		{	/* Integrate/free.scm 131 */
			{	/* Integrate/free.scm 132 */
				obj_t BgL_arg1448z00_2651;

				BgL_arg1448z00_2651 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_2591)));
				return
					BGl_internalzd2errorzd2zztools_errorz00
					(BGl_string1864z00zzintegrate_freez00,
					BGl_string1884z00zzintegrate_freez00, BgL_arg1448z00_2651);
			}
		}

	}



/* &node-free-var1273 */
	obj_t BGl_z62nodezd2freezd2var1273z62zzintegrate_freez00(obj_t
		BgL_envz00_2593, obj_t BgL_nodez00_2594, obj_t BgL_freez00_2595)
	{
		{	/* Integrate/free.scm 117 */
			{	/* Integrate/free.scm 120 */
				bool_t BgL_test1948z00_3176;

				{	/* Integrate/free.scm 120 */
					BgL_variablez00_bglt BgL_arg1437z00_2653;

					BgL_arg1437z00_2653 =
						(((BgL_varz00_bglt) COBJECT(
								((BgL_varz00_bglt) BgL_nodez00_2594)))->BgL_variablez00);
					{	/* Integrate/free.scm 120 */
						obj_t BgL_classz00_2654;

						BgL_classz00_2654 = BGl_globalz00zzast_varz00;
						{	/* Integrate/free.scm 120 */
							BgL_objectz00_bglt BgL_arg1807z00_2655;

							{	/* Integrate/free.scm 120 */
								obj_t BgL_tmpz00_3179;

								BgL_tmpz00_3179 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg1437z00_2653));
								BgL_arg1807z00_2655 = (BgL_objectz00_bglt) (BgL_tmpz00_3179);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Integrate/free.scm 120 */
									long BgL_idxz00_2656;

									BgL_idxz00_2656 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2655);
									BgL_test1948z00_3176 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2656 + 2L)) == BgL_classz00_2654);
								}
							else
								{	/* Integrate/free.scm 120 */
									bool_t BgL_res1855z00_2659;

									{	/* Integrate/free.scm 120 */
										obj_t BgL_oclassz00_2660;

										{	/* Integrate/free.scm 120 */
											obj_t BgL_arg1815z00_2661;
											long BgL_arg1816z00_2662;

											BgL_arg1815z00_2661 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Integrate/free.scm 120 */
												long BgL_arg1817z00_2663;

												BgL_arg1817z00_2663 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2655);
												BgL_arg1816z00_2662 =
													(BgL_arg1817z00_2663 - OBJECT_TYPE);
											}
											BgL_oclassz00_2660 =
												VECTOR_REF(BgL_arg1815z00_2661, BgL_arg1816z00_2662);
										}
										{	/* Integrate/free.scm 120 */
											bool_t BgL__ortest_1115z00_2664;

											BgL__ortest_1115z00_2664 =
												(BgL_classz00_2654 == BgL_oclassz00_2660);
											if (BgL__ortest_1115z00_2664)
												{	/* Integrate/free.scm 120 */
													BgL_res1855z00_2659 = BgL__ortest_1115z00_2664;
												}
											else
												{	/* Integrate/free.scm 120 */
													long BgL_odepthz00_2665;

													{	/* Integrate/free.scm 120 */
														obj_t BgL_arg1804z00_2666;

														BgL_arg1804z00_2666 = (BgL_oclassz00_2660);
														BgL_odepthz00_2665 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2666);
													}
													if ((2L < BgL_odepthz00_2665))
														{	/* Integrate/free.scm 120 */
															obj_t BgL_arg1802z00_2667;

															{	/* Integrate/free.scm 120 */
																obj_t BgL_arg1803z00_2668;

																BgL_arg1803z00_2668 = (BgL_oclassz00_2660);
																BgL_arg1802z00_2667 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2668,
																	2L);
															}
															BgL_res1855z00_2659 =
																(BgL_arg1802z00_2667 == BgL_classz00_2654);
														}
													else
														{	/* Integrate/free.scm 120 */
															BgL_res1855z00_2659 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1948z00_3176 = BgL_res1855z00_2659;
								}
						}
					}
				}
				if (BgL_test1948z00_3176)
					{	/* Integrate/free.scm 120 */
						return BgL_freez00_2595;
					}
				else
					{	/* Integrate/free.scm 122 */
						bool_t BgL_test1952z00_3202;

						{	/* Integrate/free.scm 122 */
							BgL_variablez00_bglt BgL_arg1434z00_2669;

							BgL_arg1434z00_2669 =
								(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_nodez00_2594)))->BgL_variablez00);
							BgL_test1952z00_3202 =
								CBOOL(BGl_freezd2variablezf3z21zzintegrate_freez00(
									((obj_t) BgL_arg1434z00_2669)));
						}
						if (BgL_test1952z00_3202)
							{	/* Integrate/free.scm 122 */
								{	/* Integrate/free.scm 123 */
									BgL_variablez00_bglt BgL_arg1421z00_2670;

									BgL_arg1421z00_2670 =
										(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt) BgL_nodez00_2594)))->
										BgL_variablez00);
									BGl_markzd2variablez12zc0zzintegrate_freez00((
											(BgL_localz00_bglt) BgL_arg1421z00_2670));
								}
								{	/* Integrate/free.scm 124 */
									BgL_variablez00_bglt BgL_arg1422z00_2671;

									BgL_arg1422z00_2671 =
										(((BgL_varz00_bglt) COBJECT(
												((BgL_varz00_bglt) BgL_nodez00_2594)))->
										BgL_variablez00);
									return MAKE_YOUNG_PAIR(((obj_t) BgL_arg1422z00_2671),
										BgL_freez00_2595);
								}
							}
						else
							{	/* Integrate/free.scm 122 */
								return BgL_freez00_2595;
							}
					}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzintegrate_freez00(void)
	{
		{	/* Integrate/free.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1885z00zzintegrate_freez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1885z00zzintegrate_freez00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1885z00zzintegrate_freez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1885z00zzintegrate_freez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1885z00zzintegrate_freez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1885z00zzintegrate_freez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1885z00zzintegrate_freez00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1885z00zzintegrate_freez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1885z00zzintegrate_freez00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1885z00zzintegrate_freez00));
			BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(44601789L,
				BSTRING_TO_STRING(BGl_string1885z00zzintegrate_freez00));
			BGl_modulezd2initializa7ationz75zzintegrate_infoz00(0L,
				BSTRING_TO_STRING(BGl_string1885z00zzintegrate_freez00));
			BGl_modulezd2initializa7ationz75zzintegrate_nodez00(347237104L,
				BSTRING_TO_STRING(BGl_string1885z00zzintegrate_freez00));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1885z00zzintegrate_freez00));
		}

	}

#ifdef __cplusplus
}
#endif
