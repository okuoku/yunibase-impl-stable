/*===========================================================================*/
/*   (Integrate/walk.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Integrate/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INTEGRATE_WALK_TYPE_DEFINITIONS
#define BGL_INTEGRATE_WALK_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;


#endif													// BGL_INTEGRATE_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzintegrate_walkz00 = BUNSPEC;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_integratezd2definitionz12zc0zzintegrate_definitionz00(obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzintegrate_walkz00(void);
	static obj_t BGl_objectzd2initzd2zzintegrate_walkz00(void);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzintegrate_walkz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzintegrate_walkz00(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	extern obj_t BGl_removezd2varzd2zzast_removez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_volatilez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_definitionz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	static obj_t BGl_cnstzd2initzd2zzintegrate_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_walkz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzintegrate_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzintegrate_walkz00(void);
	BGL_EXPORTED_DECL obj_t BGl_integratezd2walkz12zc0zzintegrate_walkz00(obj_t);
	static obj_t BGl_z62integratezd2walkz12za2zzintegrate_walkz00(obj_t, obj_t);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	extern BgL_globalz00_bglt
		BGl_volatilez12z12zzintegrate_volatilez00(BgL_globalz00_bglt);
	extern obj_t BGl_za2localzd2exitzf3za2z21zzengine_paramz00;
	static obj_t __cnst[2];


	   
		 
		DEFINE_STRING(BGl_string1549z00zzintegrate_walkz00,
		BgL_bgl_string1549za700za7za7i1560za7, "Integration", 11);
	      DEFINE_STRING(BGl_string1550z00zzintegrate_walkz00,
		BgL_bgl_string1550za700za7za7i1561za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1551z00zzintegrate_walkz00,
		BgL_bgl_string1551za700za7za7i1562za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1552z00zzintegrate_walkz00,
		BgL_bgl_string1552za700za7za7i1563za7, " error", 6);
	      DEFINE_STRING(BGl_string1553z00zzintegrate_walkz00,
		BgL_bgl_string1553za700za7za7i1564za7, "s", 1);
	      DEFINE_STRING(BGl_string1554z00zzintegrate_walkz00,
		BgL_bgl_string1554za700za7za7i1565za7, "", 0);
	      DEFINE_STRING(BGl_string1555z00zzintegrate_walkz00,
		BgL_bgl_string1555za700za7za7i1566za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1556z00zzintegrate_walkz00,
		BgL_bgl_string1556za700za7za7i1567za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1557z00zzintegrate_walkz00,
		BgL_bgl_string1557za700za7za7i1568za7, "integrate_walk", 14);
	      DEFINE_STRING(BGl_string1558z00zzintegrate_walkz00,
		BgL_bgl_string1558za700za7za7i1569za7, "(integrate cfa) pass-started ", 29);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_integratezd2walkz12zd2envz12zzintegrate_walkz00,
		BgL_bgl_za762integrateza7d2w1570z00,
		BGl_z62integratezd2walkz12za2zzintegrate_walkz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzintegrate_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_walkz00(long
		BgL_checksumz00_1625, char *BgL_fromz00_1626)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzintegrate_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzintegrate_walkz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzintegrate_walkz00();
					BGl_libraryzd2moduleszd2initz00zzintegrate_walkz00();
					BGl_cnstzd2initzd2zzintegrate_walkz00();
					BGl_importedzd2moduleszd2initz00zzintegrate_walkz00();
					return BGl_methodzd2initzd2zzintegrate_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_walkz00(void)
	{
		{	/* Integrate/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"integrate_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"integrate_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"integrate_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "integrate_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"integrate_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "integrate_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"integrate_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"integrate_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzintegrate_walkz00(void)
	{
		{	/* Integrate/walk.scm 15 */
			{	/* Integrate/walk.scm 15 */
				obj_t BgL_cportz00_1614;

				{	/* Integrate/walk.scm 15 */
					obj_t BgL_stringz00_1621;

					BgL_stringz00_1621 = BGl_string1558z00zzintegrate_walkz00;
					{	/* Integrate/walk.scm 15 */
						obj_t BgL_startz00_1622;

						BgL_startz00_1622 = BINT(0L);
						{	/* Integrate/walk.scm 15 */
							obj_t BgL_endz00_1623;

							BgL_endz00_1623 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1621)));
							{	/* Integrate/walk.scm 15 */

								BgL_cportz00_1614 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1621, BgL_startz00_1622, BgL_endz00_1623);
				}}}}
				{
					long BgL_iz00_1615;

					BgL_iz00_1615 = 1L;
				BgL_loopz00_1616:
					if ((BgL_iz00_1615 == -1L))
						{	/* Integrate/walk.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Integrate/walk.scm 15 */
							{	/* Integrate/walk.scm 15 */
								obj_t BgL_arg1559z00_1617;

								{	/* Integrate/walk.scm 15 */

									{	/* Integrate/walk.scm 15 */
										obj_t BgL_locationz00_1619;

										BgL_locationz00_1619 = BBOOL(((bool_t) 0));
										{	/* Integrate/walk.scm 15 */

											BgL_arg1559z00_1617 =
												BGl_readz00zz__readerz00(BgL_cportz00_1614,
												BgL_locationz00_1619);
										}
									}
								}
								{	/* Integrate/walk.scm 15 */
									int BgL_tmpz00_1652;

									BgL_tmpz00_1652 = (int) (BgL_iz00_1615);
									CNST_TABLE_SET(BgL_tmpz00_1652, BgL_arg1559z00_1617);
							}}
							{	/* Integrate/walk.scm 15 */
								int BgL_auxz00_1620;

								BgL_auxz00_1620 = (int) ((BgL_iz00_1615 - 1L));
								{
									long BgL_iz00_1657;

									BgL_iz00_1657 = (long) (BgL_auxz00_1620);
									BgL_iz00_1615 = BgL_iz00_1657;
									goto BgL_loopz00_1616;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzintegrate_walkz00(void)
	{
		{	/* Integrate/walk.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzintegrate_walkz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1345;

				BgL_headz00_1345 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1346;
					obj_t BgL_tailz00_1347;

					BgL_prevz00_1346 = BgL_headz00_1345;
					BgL_tailz00_1347 = BgL_l1z00_1;
				BgL_loopz00_1348:
					if (PAIRP(BgL_tailz00_1347))
						{
							obj_t BgL_newzd2prevzd2_1350;

							BgL_newzd2prevzd2_1350 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1347), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1346, BgL_newzd2prevzd2_1350);
							{
								obj_t BgL_tailz00_1667;
								obj_t BgL_prevz00_1666;

								BgL_prevz00_1666 = BgL_newzd2prevzd2_1350;
								BgL_tailz00_1667 = CDR(BgL_tailz00_1347);
								BgL_tailz00_1347 = BgL_tailz00_1667;
								BgL_prevz00_1346 = BgL_prevz00_1666;
								goto BgL_loopz00_1348;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1345);
				}
			}
		}

	}



/* integrate-walk! */
	BGL_EXPORTED_DEF obj_t BGl_integratezd2walkz12zc0zzintegrate_walkz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Integrate/walk.scm 31 */
			{	/* Integrate/walk.scm 32 */
				obj_t BgL_list1240z00_1353;

				{	/* Integrate/walk.scm 32 */
					obj_t BgL_arg1242z00_1354;

					{	/* Integrate/walk.scm 32 */
						obj_t BgL_arg1244z00_1355;

						BgL_arg1244z00_1355 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1242z00_1354 =
							MAKE_YOUNG_PAIR(BGl_string1549z00zzintegrate_walkz00,
							BgL_arg1244z00_1355);
					}
					BgL_list1240z00_1353 =
						MAKE_YOUNG_PAIR(BGl_string1550z00zzintegrate_walkz00,
						BgL_arg1242z00_1354);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1240z00_1353);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1549z00zzintegrate_walkz00;
			{	/* Integrate/walk.scm 32 */
				obj_t BgL_g1106z00_1356;

				BgL_g1106z00_1356 = BNIL;
				{
					obj_t BgL_hooksz00_1359;
					obj_t BgL_hnamesz00_1360;

					BgL_hooksz00_1359 = BgL_g1106z00_1356;
					BgL_hnamesz00_1360 = BNIL;
				BgL_zc3z04anonymousza31245ze3z87_1361:
					if (NULLP(BgL_hooksz00_1359))
						{	/* Integrate/walk.scm 32 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Integrate/walk.scm 32 */
							bool_t BgL_test1576z00_1680;

							{	/* Integrate/walk.scm 32 */
								obj_t BgL_fun1269z00_1368;

								BgL_fun1269z00_1368 = CAR(((obj_t) BgL_hooksz00_1359));
								BgL_test1576z00_1680 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1269z00_1368));
							}
							if (BgL_test1576z00_1680)
								{	/* Integrate/walk.scm 32 */
									obj_t BgL_arg1249z00_1365;
									obj_t BgL_arg1252z00_1366;

									BgL_arg1249z00_1365 = CDR(((obj_t) BgL_hooksz00_1359));
									BgL_arg1252z00_1366 = CDR(((obj_t) BgL_hnamesz00_1360));
									{
										obj_t BgL_hnamesz00_1692;
										obj_t BgL_hooksz00_1691;

										BgL_hooksz00_1691 = BgL_arg1249z00_1365;
										BgL_hnamesz00_1692 = BgL_arg1252z00_1366;
										BgL_hnamesz00_1360 = BgL_hnamesz00_1692;
										BgL_hooksz00_1359 = BgL_hooksz00_1691;
										goto BgL_zc3z04anonymousza31245ze3z87_1361;
									}
								}
							else
								{	/* Integrate/walk.scm 32 */
									obj_t BgL_arg1268z00_1367;

									BgL_arg1268z00_1367 = CAR(((obj_t) BgL_hnamesz00_1360));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1549z00zzintegrate_walkz00,
										BGl_string1551z00zzintegrate_walkz00, BgL_arg1268z00_1367);
								}
						}
				}
			}
			{
				obj_t BgL_oldz00_1373;
				obj_t BgL_newz00_1374;

				BgL_oldz00_1373 = BgL_globalsz00_3;
				BgL_newz00_1374 = BNIL;
			BgL_zc3z04anonymousza31271ze3z87_1375:
				if (NULLP(BgL_oldz00_1373))
					{	/* Integrate/walk.scm 36 */
						obj_t BgL_varsz00_1377;

						if (CBOOL(BGl_za2localzd2exitzf3za2z21zzengine_paramz00))
							{	/* Integrate/walk.scm 36 */
								if (NULLP(BgL_newz00_1374))
									{	/* Integrate/walk.scm 36 */
										BgL_varsz00_1377 = BNIL;
									}
								else
									{	/* Integrate/walk.scm 36 */
										obj_t BgL_head1231z00_1403;

										{	/* Integrate/walk.scm 36 */
											BgL_globalz00_bglt BgL_arg1316z00_1415;

											{	/* Integrate/walk.scm 36 */
												obj_t BgL_arg1317z00_1416;

												BgL_arg1317z00_1416 = CAR(((obj_t) BgL_newz00_1374));
												BgL_arg1316z00_1415 =
													BGl_volatilez12z12zzintegrate_volatilez00(
													((BgL_globalz00_bglt) BgL_arg1317z00_1416));
											}
											BgL_head1231z00_1403 =
												MAKE_YOUNG_PAIR(((obj_t) BgL_arg1316z00_1415), BNIL);
										}
										{	/* Integrate/walk.scm 36 */
											obj_t BgL_g1234z00_1404;

											BgL_g1234z00_1404 = CDR(((obj_t) BgL_newz00_1374));
											{
												obj_t BgL_l1229z00_1406;
												obj_t BgL_tail1232z00_1407;

												BgL_l1229z00_1406 = BgL_g1234z00_1404;
												BgL_tail1232z00_1407 = BgL_head1231z00_1403;
											BgL_zc3z04anonymousza31310ze3z87_1408:
												if (NULLP(BgL_l1229z00_1406))
													{	/* Integrate/walk.scm 36 */
														BgL_varsz00_1377 = BgL_head1231z00_1403;
													}
												else
													{	/* Integrate/walk.scm 36 */
														obj_t BgL_newtail1233z00_1410;

														{	/* Integrate/walk.scm 36 */
															BgL_globalz00_bglt BgL_arg1314z00_1412;

															{	/* Integrate/walk.scm 36 */
																obj_t BgL_arg1315z00_1413;

																BgL_arg1315z00_1413 =
																	CAR(((obj_t) BgL_l1229z00_1406));
																BgL_arg1314z00_1412 =
																	BGl_volatilez12z12zzintegrate_volatilez00(
																	((BgL_globalz00_bglt) BgL_arg1315z00_1413));
															}
															BgL_newtail1233z00_1410 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_arg1314z00_1412), BNIL);
														}
														SET_CDR(BgL_tail1232z00_1407,
															BgL_newtail1233z00_1410);
														{	/* Integrate/walk.scm 36 */
															obj_t BgL_arg1312z00_1411;

															BgL_arg1312z00_1411 =
																CDR(((obj_t) BgL_l1229z00_1406));
															{
																obj_t BgL_tail1232z00_1722;
																obj_t BgL_l1229z00_1721;

																BgL_l1229z00_1721 = BgL_arg1312z00_1411;
																BgL_tail1232z00_1722 = BgL_newtail1233z00_1410;
																BgL_tail1232z00_1407 = BgL_tail1232z00_1722;
																BgL_l1229z00_1406 = BgL_l1229z00_1721;
																goto BgL_zc3z04anonymousza31310ze3z87_1408;
															}
														}
													}
											}
										}
									}
							}
						else
							{	/* Integrate/walk.scm 36 */
								BgL_varsz00_1377 = BgL_newz00_1374;
							}
						{	/* Integrate/walk.scm 37 */
							obj_t BgL_valuez00_1378;

							BgL_valuez00_1378 =
								BGl_removezd2varzd2zzast_removez00(CNST_TABLE_REF(1),
								BgL_varsz00_1377);
							if (((long)
									CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) >
									0L))
								{	/* Integrate/walk.scm 37 */
									{	/* Integrate/walk.scm 37 */
										obj_t BgL_port1235z00_1380;

										{	/* Integrate/walk.scm 37 */
											obj_t BgL_tmpz00_1728;

											BgL_tmpz00_1728 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_port1235z00_1380 =
												BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_1728);
										}
										bgl_display_obj
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
											BgL_port1235z00_1380);
										bgl_display_string(BGl_string1552z00zzintegrate_walkz00,
											BgL_port1235z00_1380);
										{	/* Integrate/walk.scm 37 */
											obj_t BgL_arg1284z00_1381;

											{	/* Integrate/walk.scm 37 */
												bool_t BgL_test1583z00_1733;

												if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
													{	/* Integrate/walk.scm 37 */
														if (INTEGERP
															(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
															{	/* Integrate/walk.scm 37 */
																BgL_test1583z00_1733 =
																	(
																	(long)
																	CINT
																	(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
																	> 1L);
															}
														else
															{	/* Integrate/walk.scm 37 */
																BgL_test1583z00_1733 =
																	BGl_2ze3ze3zz__r4_numbers_6_5z00
																	(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
																	BINT(1L));
															}
													}
												else
													{	/* Integrate/walk.scm 37 */
														BgL_test1583z00_1733 = ((bool_t) 0);
													}
												if (BgL_test1583z00_1733)
													{	/* Integrate/walk.scm 37 */
														BgL_arg1284z00_1381 =
															BGl_string1553z00zzintegrate_walkz00;
													}
												else
													{	/* Integrate/walk.scm 37 */
														BgL_arg1284z00_1381 =
															BGl_string1554z00zzintegrate_walkz00;
													}
											}
											bgl_display_obj(BgL_arg1284z00_1381,
												BgL_port1235z00_1380);
										}
										bgl_display_string(BGl_string1555z00zzintegrate_walkz00,
											BgL_port1235z00_1380);
										bgl_display_char(((unsigned char) 10),
											BgL_port1235z00_1380);
									}
									{	/* Integrate/walk.scm 37 */
										obj_t BgL_list1288z00_1385;

										BgL_list1288z00_1385 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
										BGL_TAIL return
											BGl_exitz00zz__errorz00(BgL_list1288z00_1385);
									}
								}
							else
								{	/* Integrate/walk.scm 37 */
									obj_t BgL_g1109z00_1386;

									BgL_g1109z00_1386 = BNIL;
									{
										obj_t BgL_hooksz00_1389;
										obj_t BgL_hnamesz00_1390;

										BgL_hooksz00_1389 = BgL_g1109z00_1386;
										BgL_hnamesz00_1390 = BNIL;
									BgL_zc3z04anonymousza31289ze3z87_1391:
										if (NULLP(BgL_hooksz00_1389))
											{	/* Integrate/walk.scm 37 */
												return BgL_valuez00_1378;
											}
										else
											{	/* Integrate/walk.scm 37 */
												bool_t BgL_test1587z00_1750;

												{	/* Integrate/walk.scm 37 */
													obj_t BgL_fun1307z00_1398;

													BgL_fun1307z00_1398 =
														CAR(((obj_t) BgL_hooksz00_1389));
													BgL_test1587z00_1750 =
														CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1307z00_1398));
												}
												if (BgL_test1587z00_1750)
													{	/* Integrate/walk.scm 37 */
														obj_t BgL_arg1304z00_1395;
														obj_t BgL_arg1305z00_1396;

														BgL_arg1304z00_1395 =
															CDR(((obj_t) BgL_hooksz00_1389));
														BgL_arg1305z00_1396 =
															CDR(((obj_t) BgL_hnamesz00_1390));
														{
															obj_t BgL_hnamesz00_1762;
															obj_t BgL_hooksz00_1761;

															BgL_hooksz00_1761 = BgL_arg1304z00_1395;
															BgL_hnamesz00_1762 = BgL_arg1305z00_1396;
															BgL_hnamesz00_1390 = BgL_hnamesz00_1762;
															BgL_hooksz00_1389 = BgL_hooksz00_1761;
															goto BgL_zc3z04anonymousza31289ze3z87_1391;
														}
													}
												else
													{	/* Integrate/walk.scm 37 */
														obj_t BgL_arg1306z00_1397;

														BgL_arg1306z00_1397 =
															CAR(((obj_t) BgL_hnamesz00_1390));
														return
															BGl_internalzd2errorzd2zztools_errorz00
															(BGl_za2currentzd2passza2zd2zzengine_passz00,
															BGl_string1556z00zzintegrate_walkz00,
															BgL_arg1306z00_1397);
													}
											}
									}
								}
						}
					}
				else
					{	/* Integrate/walk.scm 38 */
						obj_t BgL_globalz00_1417;

						BgL_globalz00_1417 = CAR(((obj_t) BgL_oldz00_1373));
						{	/* Integrate/walk.scm 39 */
							obj_t BgL_arg1318z00_1418;

							BgL_arg1318z00_1418 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_globalz00_bglt) BgL_globalz00_1417))))->BgL_idz00);
							BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1318z00_1418);
						}
						{	/* Integrate/walk.scm 40 */
							obj_t BgL_arg1319z00_1419;
							obj_t BgL_arg1320z00_1420;

							BgL_arg1319z00_1419 = CDR(((obj_t) BgL_oldz00_1373));
							BgL_arg1320z00_1420 =
								BGl_appendzd221011zd2zzintegrate_walkz00
								(BGl_integratezd2definitionz12zc0zzintegrate_definitionz00
								(BgL_globalz00_1417), BgL_newz00_1374);
							{
								obj_t BgL_newz00_1777;
								obj_t BgL_oldz00_1776;

								BgL_oldz00_1776 = BgL_arg1319z00_1419;
								BgL_newz00_1777 = BgL_arg1320z00_1420;
								BgL_newz00_1374 = BgL_newz00_1777;
								BgL_oldz00_1373 = BgL_oldz00_1776;
								goto BgL_zc3z04anonymousza31271ze3z87_1375;
							}
						}
					}
			}
		}

	}



/* &integrate-walk! */
	obj_t BGl_z62integratezd2walkz12za2zzintegrate_walkz00(obj_t BgL_envz00_1612,
		obj_t BgL_globalsz00_1613)
	{
		{	/* Integrate/walk.scm 31 */
			return BGl_integratezd2walkz12zc0zzintegrate_walkz00(BgL_globalsz00_1613);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzintegrate_walkz00(void)
	{
		{	/* Integrate/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzintegrate_walkz00(void)
	{
		{	/* Integrate/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzintegrate_walkz00(void)
	{
		{	/* Integrate/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzintegrate_walkz00(void)
	{
		{	/* Integrate/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1557z00zzintegrate_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1557z00zzintegrate_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1557z00zzintegrate_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1557z00zzintegrate_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1557z00zzintegrate_walkz00));
			BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1557z00zzintegrate_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1557z00zzintegrate_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1557z00zzintegrate_walkz00));
			BGl_modulezd2initializa7ationz75zzintegrate_definitionz00(67375820L,
				BSTRING_TO_STRING(BGl_string1557z00zzintegrate_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzintegrate_volatilez00(85351432L,
				BSTRING_TO_STRING(BGl_string1557z00zzintegrate_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
