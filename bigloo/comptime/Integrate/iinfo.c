/*===========================================================================*/
/*   (Integrate/iinfo.scm)                                                   */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Integrate/iinfo.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INTEGRATE_INFO_TYPE_DEFINITIONS
#define BGL_INTEGRATE_INFO_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_sexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_handlerz00;
		bool_t BgL_detachedzf3zf3;
	}               *BgL_sexitz00_bglt;

	typedef struct BgL_svarzf2iinfozf2_bgl
	{
		obj_t BgL_fzd2markzd2;
		obj_t BgL_uzd2markzd2;
		bool_t BgL_kapturedzf3zf3;
		bool_t BgL_celledzf3zf3;
		obj_t BgL_xhdlz00;
	}                      *BgL_svarzf2iinfozf2_bglt;

	typedef struct BgL_sexitzf2iinfozf2_bgl
	{
		obj_t BgL_fzd2markzd2;
		obj_t BgL_uzd2markzd2;
		bool_t BgL_kapturedzf3zf3;
		bool_t BgL_celledzf3zf3;
	}                       *BgL_sexitzf2iinfozf2_bglt;

	typedef struct BgL_sfunzf2iinfozf2_bgl
	{
		obj_t BgL_ownerz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
		obj_t BgL_cfromz00;
		obj_t BgL_ctoz00;
		obj_t BgL_kz00;
		obj_t BgL_kza2za2;
		obj_t BgL_uz00;
		obj_t BgL_cnz00;
		obj_t BgL_ctz00;
		obj_t BgL_kontz00;
		bool_t BgL_gzf3zf3;
		bool_t BgL_forcegzf3zf3;
		obj_t BgL_lz00;
		obj_t BgL_ledz00;
		obj_t BgL_istampz00;
		obj_t BgL_globalz00;
		obj_t BgL_kapturedz00;
		obj_t BgL_tailzd2coercionzd2;
		bool_t BgL_xhdlzf3zf3;
		obj_t BgL_xhdlsz00;
	}                      *BgL_sfunzf2iinfozf2_bglt;


#endif													// BGL_INTEGRATE_INFO_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62lambda1846z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1768z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1769z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2Kza2z82zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t
		BGl_z62sfunzf2Iinfozd2argszd2retescapezd2setz12z50zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Iinfozd2kapturedzf3zd2setz12z13zzintegrate_infoz00
		(BgL_svarz00_bglt, bool_t);
	static obj_t BGl_z62svarzf2Iinfozd2loczd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Iinfozd2locz20zzintegrate_infoz00(BgL_svarz00_bglt);
	static obj_t BGl_z62lambda1851z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31757ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62lambda1852z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2stackzd2allocatorz90zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2Cnzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2bodyzd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1859z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2ownerz42zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62sexitzf2Iinfozd2handlerz42zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2classzd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2argszd2noescapezd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzintegrate_infoz00 = BUNSPEC;
	static obj_t BGl_z62lambda1860z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2Kz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2ctoz42zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2Lz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2argszd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_sexitz00_bglt
		BGl_makezd2sexitzf2Iinfoz20zzintegrate_infoz00(obj_t, bool_t, obj_t, obj_t,
		bool_t, bool_t);
	static obj_t BGl_z62lambda1867z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1868z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2Uz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62svarzf2Iinfozf3z63zzintegrate_infoz00(obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2argszd2retescapez90zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2tailzd2coercionzd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Iinfozd2uzd2markzd2setz12z32zzintegrate_infoz00(BgL_svarz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2bodyzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t BGl_z62lambda1875z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2strengthzd2setz12z82zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sfunzf2Iinfozd2xhdlzf3zd3zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62lambda1876z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2stackablez42zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2globalzd2setz12z82zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1799z62zzintegrate_infoz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t
		BGl_z62sexitzf2Iinfozd2celledzf3zd2setz12z71zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2forceGzf3zb1zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2Ledz42zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2classz42zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Iinfozd2uzd2markzf2zzintegrate_infoz00(BgL_sexitz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2ownerzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2argszd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t
		BGl_z62svarzf2Iinfozd2celledzf3zd2setz12z71zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2dssslzd2keywordszd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2tailzd2coercionzf2zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31679ze3ze5zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1883z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1884z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2xhdlzf3zd2setz12z71zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2Ledzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2stackzd2allocatorzd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Iinfozd2fzd2markzf2zzintegrate_infoz00(BgL_svarz00_bglt);
	static obj_t BGl_toplevelzd2initzd2zzintegrate_infoz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2xhdlsz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t
		BGl_z62sfunzf2Iinfozd2stackzd2allocatorzd2setz12z50zzintegrate_infoz00
		(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2stackablezd2setz12z82zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2stackablez20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62lambda1891z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1892z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62sexitzf2Iinfozd2fzd2markzd2setz12z50zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1899z62zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2boundz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2kapturedzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2failsafez20zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2xhdlszd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2sidezd2effectzf2zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2argszd2namez90zzintegrate_infoz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Iinfozd2handlerzd2setz12ze0zzintegrate_infoz00
		(BgL_sexitz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2argszd2retescapezd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_genericzd2initzd2zzintegrate_infoz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2argszd2retescapezf2zzintegrate_infoz00(BgL_sfunz00_bglt);
	extern obj_t BGl_sexitz00zzast_varz00;
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzintegrate_infoz00(void);
	BGL_EXPORTED_DEF obj_t BGl_sfunzf2Iinfozf2zzintegrate_infoz00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2argszd2namezf2zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2keysz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2keysz42zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_sexitzf2Iinfozf3z01zzintegrate_infoz00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2propertyz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2ctoz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2argszd2noescapezf2zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31893ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2ownerzd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31885ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31877ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31869ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2dssslzd2keywordsz90zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2strengthz42zzintegrate_infoz00(obj_t,
		obj_t);
	static BgL_sexitz00_bglt
		BGl_z62makezd2sexitzf2Iinfoz42zzintegrate_infoz00(obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Iinfozd2loczd2setz12ze0zzintegrate_infoz00(BgL_svarz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2stackablezd2setz12ze0zzintegrate_infoz00
		(BgL_sfunz00_bglt, obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2propertyzd2setz12z82zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2thezd2closurezf2zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Iinfozd2handlerz20zzintegrate_infoz00(BgL_sexitz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2effectz42zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2Uzd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2xhdlzf3zb1zzintegrate_infoz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2Ledz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2cfromz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2xhdlszd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2istampzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62sexitzf2Iinfozd2fzd2markz90zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2predicatezd2ofz90zzintegrate_infoz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2Kzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2xhdlsz42zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2predicatezd2ofzd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2bodyz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62svarzf2Iinfozd2xhdlzd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Iinfozd2xhdlz20zzintegrate_infoz00(BgL_svarz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2bodyz42zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzintegrate_infoz00(void);
	static obj_t BGl_z62svarzf2Iinfozd2xhdlz42zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Iinfozd2uzd2markzd2setz12z32zzintegrate_infoz00
		(BgL_sexitz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Iinfozd2detachedzf3zd2setz12z13zzintegrate_infoz00
		(BgL_sexitz00_bglt, bool_t);
	static obj_t BGl_z62sfunzf2Iinfozd2boundz42zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2kapturedz42zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t BGl_z62svarzf2Iinfozd2celledzf3zb1zzintegrate_infoz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2dssslzd2keywordszf2zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2forceGzf3zd2setz12z13zzintegrate_infoz00
		(BgL_sfunz00_bglt, bool_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sfunzf2Iinfozd2topzf3zd3zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2topzf3zb1zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Iinfozd2fzd2markzd2setz12z32zzintegrate_infoz00(BgL_svarz00_bglt,
		obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2kontzd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2Gzf3zd2setz12z13zzintegrate_infoz00(BgL_sfunz00_bglt,
		bool_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2thezd2closurezd2globalzd2setz12z82zzintegrate_infoz00
		(obj_t, obj_t, obj_t);
	static BgL_sfunz00_bglt
		BGl_z62sfunzf2Iinfozd2nilz42zzintegrate_infoz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2strengthzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Iinfozd2xhdlzd2setz12ze0zzintegrate_infoz00(BgL_svarz00_bglt,
		obj_t);
	static obj_t
		BGl_z62svarzf2Iinfozd2uzd2markzd2setz12z50zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Iinfozd2celledzf3zd2setz12z13zzintegrate_infoz00
		(BgL_sexitz00_bglt, bool_t);
	static obj_t BGl_z62sfunzf2Iinfozd2Kza2zd2setz12z20zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2optionalsz42zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62sexitzf2Iinfozd2detachedzf3zd2setz12z71zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2cfromzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL bool_t BGl_sfunzf2Iinfozf3z01zzintegrate_infoz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2kontzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2Cnz42zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2Ctz42zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_sfunz00_bglt
		BGl_makezd2sfunzf2Iinfoz20zzintegrate_infoz00(long, obj_t, obj_t, obj_t,
		bool_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, bool_t, bool_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, bool_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2locz42zzintegrate_infoz00(obj_t, obj_t);
	static BgL_sfunz00_bglt
		BGl_z62makezd2sfunzf2Iinfoz42zzintegrate_infoz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2boundzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2optionalsz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2cfromz42zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31425ze3ze5zzintegrate_infoz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sexitzf2Iinfozd2detachedzf3zd3zzintegrate_infoz00(BgL_sexitz00_bglt);
	BGL_EXPORTED_DEF obj_t BGl_sexitzf2Iinfozf2zzintegrate_infoz00 = BUNSPEC;
	static obj_t BGl_z62sfunzf2Iinfozd2sidezd2effectz90zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2ctozd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2Lzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2effectzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Iinfozd2uzd2markzf2zzintegrate_infoz00(BgL_svarz00_bglt);
	static obj_t
		BGl_z62sfunzf2Iinfozd2thezd2closurezd2globalz42zzintegrate_infoz00(obj_t,
		obj_t);
	extern obj_t BGl_svarz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2strengthz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2Kz42zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2Lz42zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2Uz42zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2kontz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2kontz42zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62svarzf2Iinfozd2fzd2markz90zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2Gzf3zb1zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2argsz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2argsz42zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sfunzf2Iinfozd2forceGzf3zd3zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2Ctzd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31605ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62svarzf2Iinfozd2kapturedzf3zb1zzintegrate_infoz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2thezd2closurezd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2istampz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2globalz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2cfromzd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2sidezd2effectzd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2propertyzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31711ze3ze5zzintegrate_infoz00(obj_t);
	BGL_EXPORTED_DECL BgL_sfunz00_bglt
		BGl_sfunzf2Iinfozd2nilz20zzintegrate_infoz00(void);
	static obj_t BGl_z62zc3z04anonymousza31517ze3ze5zzintegrate_infoz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_z62sfunzf2Iinfozd2thezd2closurez90zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2Cnzd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_svarzf2Iinfozf2zzintegrate_infoz00 = BUNSPEC;
	static obj_t BGl_z62sfunzf2Iinfozd2boundzd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_svarzf2Iinfozd2kapturedzf3zd3zzintegrate_infoz00(BgL_svarz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31801ze3ze5zzintegrate_infoz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2kapturedz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_svarzf2Iinfozd2celledzf3zd2setz12z13zzintegrate_infoz00
		(BgL_svarz00_bglt, bool_t);
	static obj_t BGl_z62sexitzf2Iinfozd2detachedzf3zb1zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2istampzd2setz12z82zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2failsafezd2setz12z82zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2Kza2ze0zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2loczd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2locz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62zc3z04anonymousza31632ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31616ze3ze5zzintegrate_infoz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Iinfozd2fzd2markzd2setz12z32zzintegrate_infoz00
		(BgL_sexitz00_bglt, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31382ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62lambda1423z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62sexitzf2Iinfozd2uzd2markz90zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1424z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2argszd2noescapezd2setz12z50zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sexitzf2Iinfozf3z63zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzintegrate_infoz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_infoz00(void);
	static obj_t
		BGl_z62sfunzf2Iinfozd2topzf3zd2setz12z71zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Iinfozf3z63zzintegrate_infoz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62svarzf2Iinfozd2fzd2markzd2setz12z50zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzintegrate_infoz00(void);
	static obj_t BGl_z62sexitzf2Iinfozd2celledzf3zb1zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31901ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzintegrate_infoz00(void);
	static obj_t BGl_z62zc3z04anonymousza31723ze3ze5zzintegrate_infoz00(obj_t);
	static BgL_svarz00_bglt BGl_z62lambda1352z62zzintegrate_infoz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31456ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62lambda1515z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1516z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2tailzd2coercionzd2setz12z50zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2Kzd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2ctozd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2freezd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1603z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1604z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31368ze3ze5zzintegrate_infoz00(obj_t,
		obj_t);
	static BgL_svarz00_bglt BGl_z62lambda1365z62zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2topzf3zd2setz12z13zzintegrate_infoz00(BgL_sfunz00_bglt,
		bool_t);
	static BgL_svarz00_bglt BGl_z62lambda1369z62zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Iinfozd2fzd2markzf2zzintegrate_infoz00(BgL_sexitz00_bglt);
	static obj_t BGl_z62lambda1614z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1615z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1454z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1455z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static BgL_svarz00_bglt
		BGl_z62svarzf2Iinfozd2nilz42zzintegrate_infoz00(obj_t);
	BGL_EXPORTED_DECL long
		BGl_sfunzf2Iinfozd2arityz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2globalzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2predicatezd2ofzd2setz12z50zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2freezd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t BGl_z62lambda1700z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1701z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31750ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31815ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62lambda1380z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1381z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_svarzf2Iinfozd2celledzf3zd3zzintegrate_infoz00(BgL_svarz00_bglt);
	static obj_t BGl_z62lambda1709z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2thezd2closurezd2setz12z50zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2effectzd2setz12z82zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2tailzd2coercionz90zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sexitzf2Iinfozd2kapturedzf3zd3zzintegrate_infoz00(BgL_sexitz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2Cnz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL bool_t BGl_svarzf2Iinfozf3z01zzintegrate_infoz00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2predicatezd2ofzf2zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2Ctz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL BgL_svarz00_bglt
		BGl_makezd2svarzf2Iinfoz20zzintegrate_infoz00(obj_t, obj_t, obj_t, bool_t,
		bool_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2sidezd2effectzd2setz12z50zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1710z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2thezd2closurezd2globalzd2setz12ze0zzintegrate_infoz00
		(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_z62lambda1630z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62svarzf2Iinfozd2locz42zzintegrate_infoz00(obj_t, obj_t);
	static BgL_svarz00_bglt
		BGl_z62makezd2svarzf2Iinfoz42zzintegrate_infoz00(obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1631z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31492ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31565ze3ze5zzintegrate_infoz00(obj_t,
		obj_t);
	static BgL_sexitz00_bglt BGl_z62lambda1554z62zzintegrate_infoz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2ownerz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2Kza2zd2setz12z42zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t
		BGl_z62sexitzf2Iinfozd2uzd2markzd2setz12z50zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2kapturedzd2setz12z82zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62lambda1800z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31841ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62lambda1721z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1722z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static BgL_sexitz00_bglt BGl_z62lambda1562z62zzintegrate_infoz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2thezd2closurezd2globalz20zzintegrate_infoz00
		(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sexitzf2Iinfozd2kapturedzf3zd2setz12z13zzintegrate_infoz00
		(BgL_sexitz00_bglt, bool_t);
	static BgL_sexitz00_bglt BGl_z62lambda1566z62zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2Lzd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62sexitzf2Iinfozd2handlerzd2setz12z82zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62svarzf2Iinfozd2uzd2markz90zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2forceGzf3zd2setz12z71zzintegrate_infoz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2Ledzd2setz12z82zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2loczd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t
		BGl_z62svarzf2Iinfozd2kapturedzf3zd2setz12z71zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31834ze3ze5zzintegrate_infoz00(obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sfunzf2Iinfozd2Gzf3zd3zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62lambda1813z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1490z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1814z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1491z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1736z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1737z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2effectz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2Uzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2classzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2classz20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2failsafez42zzintegrate_infoz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2freez20zzintegrate_infoz00(BgL_sfunz00_bglt);
	static obj_t BGl_z62lambda1900z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31770ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31592ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31916ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2freez42zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31738ze3ze5zzintegrate_infoz00(obj_t);
	static BgL_sfunz00_bglt BGl_z62lambda1662z62zzintegrate_infoz00(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1907z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1908z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1748z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2argszd2noescapez90zzintegrate_infoz00(obj_t, obj_t);
	static obj_t
		BGl_z62sfunzf2Iinfozd2dssslzd2keywordszd2setz12z50zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1749z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62sexitzf2Iinfozd2kapturedzf3zd2setz12z71zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2istampz42zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2failsafezd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	static obj_t BGl_z62sexitzf2Iinfozd2kapturedzf3zb1zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2globalz42zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_sexitzf2Iinfozd2celledzf3zd3zzintegrate_infoz00(BgL_sexitz00_bglt);
	BGL_EXPORTED_DECL BgL_svarz00_bglt
		BGl_svarzf2Iinfozd2nilz20zzintegrate_infoz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2stackzd2allocatorzf2zzintegrate_infoz00
		(BgL_sfunz00_bglt);
	static obj_t BGl_z62sfunzf2Iinfozd2Gzf3zd2setz12z71zzintegrate_infoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62lambda1832z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1590z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1833z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31909ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62lambda1914z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1591z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1915z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62lambda1755z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1756z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static BgL_sfunz00_bglt BGl_z62lambda1676z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62lambda1839z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t BGl_z62sfunzf2Iinfozd2arityz42zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2Ctzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt,
		obj_t);
	BGL_EXPORTED_DECL BgL_sexitz00_bglt
		BGl_sexitzf2Iinfozd2nilz20zzintegrate_infoz00(void);
	static BgL_sexitz00_bglt
		BGl_z62sexitzf2Iinfozd2nilz42zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31861ze3ze5zzintegrate_infoz00(obj_t);
	static obj_t BGl_z62lambda1840z62zzintegrate_infoz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31853ze3ze5zzintegrate_infoz00(obj_t);
	static BgL_sfunz00_bglt BGl_z62lambda1680z62zzintegrate_infoz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_sfunzf2Iinfozd2xhdlzf3zd2setz12z13zzintegrate_infoz00(BgL_sfunz00_bglt,
		bool_t);
	static obj_t BGl_z62sfunzf2Iinfozd2propertyz42zzintegrate_infoz00(obj_t,
		obj_t);
	static obj_t BGl_z62lambda1845z62zzintegrate_infoz00(obj_t, obj_t);
	static obj_t __cnst[34];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2031z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1800za7622078z00,
		BGl_z62lambda1800z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2032z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1799za7622079z00,
		BGl_z62lambda1799z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2033z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2080za7,
		BGl_z62zc3z04anonymousza31815ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2034z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1814za7622081z00,
		BGl_z62lambda1814z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2kapturedzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72082za7,
		BGl_z62sfunzf2Iinfozd2kapturedzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2035z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1813za7622083z00,
		BGl_z62lambda1813z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2036z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2084za7,
		BGl_z62zc3z04anonymousza31834ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2037z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1833za7622085z00,
		BGl_z62lambda1833z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2038z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1832za7622086z00,
		BGl_z62lambda1832z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2039z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2087za7,
		BGl_z62zc3z04anonymousza31841ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Kzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72088za7,
		BGl_z62sfunzf2Iinfozd2Kz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2040z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1840za7622089z00,
		BGl_z62lambda1840z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2041z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1839za7622090z00,
		BGl_z62lambda1839z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2042z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1846za7622091z00,
		BGl_z62lambda1846z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2043z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1845za7622092z00,
		BGl_z62lambda1845z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2044z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2093za7,
		BGl_z62zc3z04anonymousza31853ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2045z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1852za7622094z00,
		BGl_z62lambda1852z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2046z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1851za7622095z00,
		BGl_z62lambda1851z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2047z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2096za7,
		BGl_z62zc3z04anonymousza31861ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2048z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1860za7622097z00,
		BGl_z62lambda1860z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2propertyzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72098za7,
		BGl_z62sfunzf2Iinfozd2propertyzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2049z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1859za7622099z00,
		BGl_z62lambda1859z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2tailzd2coercionzd2setz12zd2envze0zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72100za7,
		BGl_z62sfunzf2Iinfozd2tailzd2coercionzd2setz12z50zzintegrate_infoz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2stackzd2allocatorzd2envz20zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72101za7,
		BGl_z62sfunzf2Iinfozd2stackzd2allocatorz90zzintegrate_infoz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Iinfozd2nilzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sexitza7f2iinfo2102z00,
		BGl_z62sexitzf2Iinfozd2nilz42zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2050z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2103za7,
		BGl_z62zc3z04anonymousza31869ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2051z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1868za7622104z00,
		BGl_z62lambda1868z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2052z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1867za7622105z00,
		BGl_z62lambda1867z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2053z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2106za7,
		BGl_z62zc3z04anonymousza31877ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2054z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1876za7622107z00,
		BGl_z62lambda1876z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2055z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1875za7622108z00,
		BGl_z62lambda1875z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2056z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2109za7,
		BGl_z62zc3z04anonymousza31885ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2057z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1884za7622110z00,
		BGl_z62lambda1884z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Iinfozd2kapturedzf3zd2envz01zzintegrate_infoz00,
		BgL_bgl_za762svarza7f2iinfoza72111za7,
		BGl_z62svarzf2Iinfozd2kapturedzf3zb1zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2058z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1883za7622112z00,
		BGl_z62lambda1883z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2059z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2113za7,
		BGl_z62zc3z04anonymousza31893ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2060z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1892za7622114z00,
		BGl_z62lambda1892z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2061z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1891za7622115z00,
		BGl_z62lambda1891z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2062z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2116za7,
		BGl_z62zc3z04anonymousza31901ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2063z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1900za7622117z00,
		BGl_z62lambda1900z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2064z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1899za7622118z00,
		BGl_z62lambda1899z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2065z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2119za7,
		BGl_z62zc3z04anonymousza31909ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2066z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1908za7622120z00,
		BGl_z62lambda1908z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2067z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1907za7622121z00,
		BGl_z62lambda1907z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2068z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2122za7,
		BGl_z62zc3z04anonymousza31916ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string2075z00zzintegrate_infoz00,
		BgL_bgl_string2075za700za7za7i2123za7, "integrate_info", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2069z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1915za7622124z00,
		BGl_z62lambda1915z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2076z00zzintegrate_infoz00,
		BgL_bgl_string2076za700za7za7i2125za7,
		"_ sfun/Iinfo pair-nil xhdls xhdl? tail-coercion kaptured global istamp Led L forceG? G? kont Ct Cn U K* K cto cfrom bound free owner sexit/Iinfo integrate_info svar/Iinfo xhdl celled? bool kaptured? u-mark obj f-mark ",
		217);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2xhdlszd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72126za7,
		BGl_z62sfunzf2Iinfozd2xhdlsz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2strengthzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72127za7,
		BGl_z62sfunzf2Iinfozd2strengthzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2effectzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72128za7,
		BGl_z62sfunzf2Iinfozd2effectz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2070z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1914za7622129z00,
		BGl_z62lambda1914z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2071z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1680za7622130z00,
		BGl_z62lambda1680z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2failsafezd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72131za7,
		BGl_z62sfunzf2Iinfozd2failsafez42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2072z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2132za7,
		BGl_z62zc3z04anonymousza31679ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2073z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1676za7622133z00,
		BGl_z62lambda1676z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2074z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1662za7622134z00,
		BGl_z62lambda1662z62zzintegrate_infoz00, 0L, BUNSPEC, 43);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Lzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72135za7,
		BGl_z62sfunzf2Iinfozd2Lz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2cfromzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72136za7,
		BGl_z62sfunzf2Iinfozd2cfromzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2failsafezd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72137za7,
		BGl_z62sfunzf2Iinfozd2failsafezd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2svarzf2Iinfozd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762makeza7d2svarza7f2138za7,
		BGl_z62makezd2svarzf2Iinfoz42zzintegrate_infoz00, 0L, BUNSPEC, 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2optionalszd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72139za7,
		BGl_z62sfunzf2Iinfozd2optionalsz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2argszd2namezd2envz20zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72140za7,
		BGl_z62sfunzf2Iinfozd2argszd2namez90zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2kapturedzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72141za7,
		BGl_z62sfunzf2Iinfozd2kapturedz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Iinfozf3zd2envzd3zzintegrate_infoz00,
		BgL_bgl_za762sexitza7f2iinfo2142z00,
		BGl_z62sexitzf2Iinfozf3z63zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Iinfozd2fzd2markzd2envz20zzintegrate_infoz00,
		BgL_bgl_za762sexitza7f2iinfo2143z00,
		BGl_z62sexitzf2Iinfozd2fzd2markz90zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2topzf3zd2envz01zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72144za7,
		BGl_z62sfunzf2Iinfozd2topzf3zb1zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Kza2zd2envz50zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72145za7,
		BGl_z62sfunzf2Iinfozd2Kza2ze0zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Gzf3zd2envz01zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72146za7,
		BGl_z62sfunzf2Iinfozd2Gzf3zb1zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Iinfozd2xhdlzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762svarza7f2iinfoza72147za7,
		BGl_z62svarzf2Iinfozd2xhdlzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2thezd2closurezd2envz20zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72148za7,
		BGl_z62sfunzf2Iinfozd2thezd2closurez90zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2classzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72149za7,
		BGl_z62sfunzf2Iinfozd2classz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2globalzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72150za7,
		BGl_z62sfunzf2Iinfozd2globalz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_sfunzf2Iinfozf3zd2envzd3zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72151za7,
		BGl_z62sfunzf2Iinfozf3z63zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Uzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72152za7,
		BGl_z62sfunzf2Iinfozd2Uz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Ctzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72153za7,
		BGl_z62sfunzf2Iinfozd2Ctz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2sidezd2effectzd2setz12zd2envze0zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72154za7,
		BGl_z62sfunzf2Iinfozd2sidezd2effectzd2setz12z50zzintegrate_infoz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Kza2zd2setz12zd2envz90zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72155za7,
		BGl_z62sfunzf2Iinfozd2Kza2zd2setz12z20zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2boundzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72156za7,
		BGl_z62sfunzf2Iinfozd2boundzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2keyszd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72157za7,
		BGl_z62sfunzf2Iinfozd2keysz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2ctozd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72158za7,
		BGl_z62sfunzf2Iinfozd2ctozd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Iinfozd2detachedzf3zd2envz01zzintegrate_infoz00,
		BgL_bgl_za762sexitza7f2iinfo2159z00,
		BGl_z62sexitzf2Iinfozd2detachedzf3zb1zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2ownerzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72160za7,
		BGl_z62sfunzf2Iinfozd2ownerzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Iinfozd2loczd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762svarza7f2iinfoza72161za7,
		BGl_z62svarzf2Iinfozd2locz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2stackablezd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72162za7,
		BGl_z62sfunzf2Iinfozd2stackablezd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Iinfozd2fzd2markzd2setz12zd2envze0zzintegrate_infoz00,
		BgL_bgl_za762sexitza7f2iinfo2163z00,
		BGl_z62sexitzf2Iinfozd2fzd2markzd2setz12z50zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Uzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72164za7,
		BGl_z62sfunzf2Iinfozd2Uzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Iinfozd2celledzf3zd2setz12zd2envzc1zzintegrate_infoz00,
		BgL_bgl_za762sexitza7f2iinfo2165z00,
		BGl_z62sexitzf2Iinfozd2celledzf3zd2setz12z71zzintegrate_infoz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2xhdlszd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72166za7,
		BGl_z62sfunzf2Iinfozd2xhdlszd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Iinfozd2celledzf3zd2envz01zzintegrate_infoz00,
		BgL_bgl_za762svarza7f2iinfoza72167za7,
		BGl_z62svarzf2Iinfozd2celledzf3zb1zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Iinfozd2kapturedzf3zd2envz01zzintegrate_infoz00,
		BgL_bgl_za762sexitza7f2iinfo2168z00,
		BGl_z62sexitzf2Iinfozd2kapturedzf3zb1zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Iinfozd2detachedzf3zd2setz12zd2envzc1zzintegrate_infoz00,
		BgL_bgl_za762sexitza7f2iinfo2169z00,
		BGl_z62sexitzf2Iinfozd2detachedzf3zd2setz12z71zzintegrate_infoz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Iinfozd2uzd2markzd2envz20zzintegrate_infoz00,
		BgL_bgl_za762sexitza7f2iinfo2170z00,
		BGl_z62sexitzf2Iinfozd2uzd2markz90zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Iinfozd2celledzf3zd2setz12zd2envzc1zzintegrate_infoz00,
		BgL_bgl_za762svarza7f2iinfoza72171za7,
		BGl_z62svarzf2Iinfozd2celledzf3zd2setz12z71zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2propertyzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72172za7,
		BGl_z62sfunzf2Iinfozd2propertyz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2loczd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72173za7,
		BGl_z62sfunzf2Iinfozd2loczd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2stackzd2allocatorzd2setz12zd2envze0zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72174za7,
		BGl_z62sfunzf2Iinfozd2stackzd2allocatorzd2setz12z50zzintegrate_infoz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Iinfozd2fzd2markzd2envz20zzintegrate_infoz00,
		BgL_bgl_za762svarza7f2iinfoza72175za7,
		BGl_z62svarzf2Iinfozd2fzd2markz90zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2arityzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72176za7,
		BGl_z62sfunzf2Iinfozd2arityz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2classzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72177za7,
		BGl_z62sfunzf2Iinfozd2classzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Ledzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72178za7,
		BGl_z62sfunzf2Iinfozd2Ledz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2sexitzf2Iinfozd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762makeza7d2sexitza72179za7,
		BGl_z62makezd2sexitzf2Iinfoz42zzintegrate_infoz00, 0L, BUNSPEC, 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Cnzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72180za7,
		BGl_z62sfunzf2Iinfozd2Cnz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2argszd2retescapezd2envz20zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72181za7,
		BGl_z62sfunzf2Iinfozd2argszd2retescapez90zzintegrate_infoz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2nilzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72182za7,
		BGl_z62sfunzf2Iinfozd2nilz42zzintegrate_infoz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Kzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72183za7,
		BGl_z62sfunzf2Iinfozd2Kzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Iinfozd2uzd2markzd2setz12zd2envze0zzintegrate_infoz00,
		BgL_bgl_za762svarza7f2iinfoza72184za7,
		BGl_z62svarzf2Iinfozd2uzd2markzd2setz12z50zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2predicatezd2ofzd2setz12zd2envze0zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72185za7,
		BGl_z62sfunzf2Iinfozd2predicatezd2ofzd2setz12z50zzintegrate_infoz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2xhdlzf3zd2envz01zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72186za7,
		BGl_z62sfunzf2Iinfozd2xhdlzf3zb1zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2ctozd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72187za7,
		BGl_z62sfunzf2Iinfozd2ctoz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2argszd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72188za7,
		BGl_z62sfunzf2Iinfozd2argszd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2thezd2closurezd2globalzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72189za7,
		BGl_z62sfunzf2Iinfozd2thezd2closurezd2globalz42zzintegrate_infoz00, 0L,
		BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Ledzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72190za7,
		BGl_z62sfunzf2Iinfozd2Ledzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2kontzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72191za7,
		BGl_z62sfunzf2Iinfozd2kontz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1975z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2192za7,
		BGl_z62zc3z04anonymousza31382ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1976z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1381za7622193z00,
		BGl_z62lambda1381z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1977z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1380za7622194z00,
		BGl_z62lambda1380z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1978z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2195za7,
		BGl_z62zc3z04anonymousza31425ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1979z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1424za7622196z00,
		BGl_z62lambda1424z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2sidezd2effectzd2envz20zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72197za7,
		BGl_z62sfunzf2Iinfozd2sidezd2effectz90zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1980z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1423za7622198z00,
		BGl_z62lambda1423z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1981z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2199za7,
		BGl_z62zc3z04anonymousza31456ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1982z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1455za7622200z00,
		BGl_z62lambda1455z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1983z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1454za7622201z00,
		BGl_z62lambda1454z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1984z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2202za7,
		BGl_z62zc3z04anonymousza31492ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1985z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1491za7622203z00,
		BGl_z62lambda1491z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1986z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1490za7622204z00,
		BGl_z62lambda1490z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1987z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2205za7,
		BGl_z62zc3z04anonymousza31517ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1988z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1516za7622206z00,
		BGl_z62lambda1516z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1989z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1515za7622207z00,
		BGl_z62lambda1515z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_svarzf2Iinfozf3zd2envzd3zzintegrate_infoz00,
		BgL_bgl_za762svarza7f2iinfoza72208za7,
		BGl_z62svarzf2Iinfozf3z63zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Iinfozd2celledzf3zd2envz01zzintegrate_infoz00,
		BgL_bgl_za762sexitza7f2iinfo2209z00,
		BGl_z62sexitzf2Iinfozd2celledzf3zb1zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1990z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1369za7622210z00,
		BGl_z62lambda1369z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1991z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2211za7,
		BGl_z62zc3z04anonymousza31368ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1992z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1365za7622212z00,
		BGl_z62lambda1365z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1993z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1352za7622213z00,
		BGl_z62lambda1352z62zzintegrate_infoz00, 0L, BUNSPEC, 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1994z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2214za7,
		BGl_z62zc3z04anonymousza31592ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2bodyzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72215za7,
		BGl_z62sfunzf2Iinfozd2bodyzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1995z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1591za7622216z00,
		BGl_z62lambda1591z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1996z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1590za7622217z00,
		BGl_z62lambda1590z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1997z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2218za7,
		BGl_z62zc3z04anonymousza31605ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1998z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1604za7622219z00,
		BGl_z62lambda1604z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1999z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1603za7622220z00,
		BGl_z62lambda1603z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2bodyzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72221za7,
		BGl_z62sfunzf2Iinfozd2bodyz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Ctzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72222za7,
		BGl_z62sfunzf2Iinfozd2Ctzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2dssslzd2keywordszd2setz12zd2envze0zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72223za7,
		BGl_z62sfunzf2Iinfozd2dssslzd2keywordszd2setz12z50zzintegrate_infoz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2xhdlzf3zd2setz12zd2envzc1zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72224za7,
		BGl_z62sfunzf2Iinfozd2xhdlzf3zd2setz12z71zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Iinfozd2kapturedzf3zd2setz12zd2envzc1zzintegrate_infoz00,
		BgL_bgl_za762sexitza7f2iinfo2225z00,
		BGl_z62sexitzf2Iinfozd2kapturedzf3zd2setz12z71zzintegrate_infoz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Iinfozd2uzd2markzd2envz20zzintegrate_infoz00,
		BgL_bgl_za762svarza7f2iinfoza72226za7,
		BGl_z62svarzf2Iinfozd2uzd2markz90zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2dssslzd2keywordszd2envz20zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72227za7,
		BGl_z62sfunzf2Iinfozd2dssslzd2keywordsz90zzintegrate_infoz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Iinfozd2kapturedzf3zd2setz12zd2envzc1zzintegrate_infoz00,
		BgL_bgl_za762svarza7f2iinfoza72228za7,
		BGl_z62svarzf2Iinfozd2kapturedzf3zd2setz12z71zzintegrate_infoz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2globalzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72229za7,
		BGl_z62sfunzf2Iinfozd2globalzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2argszd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72230za7,
		BGl_z62sfunzf2Iinfozd2argsz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Iinfozd2handlerzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sexitza7f2iinfo2231z00,
		BGl_z62sexitzf2Iinfozd2handlerz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2forceGzf3zd2envz01zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72232za7,
		BGl_z62sfunzf2Iinfozd2forceGzf3zb1zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2cfromzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72233za7,
		BGl_z62sfunzf2Iinfozd2cfromz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2boundzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72234za7,
		BGl_z62sfunzf2Iinfozd2boundz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2freezd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72235za7,
		BGl_z62sfunzf2Iinfozd2freez42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2thezd2closurezd2setz12zd2envze0zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72236za7,
		BGl_z62sfunzf2Iinfozd2thezd2closurezd2setz12z50zzintegrate_infoz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2argszd2retescapezd2setz12zd2envze0zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72237za7,
		BGl_z62sfunzf2Iinfozd2argszd2retescapezd2setz12z50zzintegrate_infoz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2predicatezd2ofzd2envz20zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72238za7,
		BGl_z62sfunzf2Iinfozd2predicatezd2ofz90zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2kontzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72239za7,
		BGl_z62sfunzf2Iinfozd2kontzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_makezd2sfunzf2Iinfozd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762makeza7d2sfunza7f2240za7,
		BGl_z62makezd2sfunzf2Iinfoz42zzintegrate_infoz00, 0L, BUNSPEC, 43);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2thezd2closurezd2globalzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72241za7,
		BGl_z62sfunzf2Iinfozd2thezd2closurezd2globalzd2setz12z82zzintegrate_infoz00,
		0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2ownerzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72242za7,
		BGl_z62sfunzf2Iinfozd2ownerz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Lzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72243za7,
		BGl_z62sfunzf2Iinfozd2Lzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2argszd2noescapezd2envz20zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72244za7,
		BGl_z62sfunzf2Iinfozd2argszd2noescapez90zzintegrate_infoz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2tailzd2coercionzd2envz20zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72245za7,
		BGl_z62sfunzf2Iinfozd2tailzd2coercionz90zzintegrate_infoz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2topzf3zd2setz12zd2envzc1zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72246za7,
		BGl_z62sfunzf2Iinfozd2topzf3zd2setz12z71zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2forceGzf3zd2setz12zd2envzc1zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72247za7,
		BGl_z62sfunzf2Iinfozd2forceGzf3zd2setz12z71zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Iinfozd2loczd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762svarza7f2iinfoza72248za7,
		BGl_z62svarzf2Iinfozd2loczd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2freezd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72249za7,
		BGl_z62sfunzf2Iinfozd2freezd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2argszd2noescapezd2setz12zd2envze0zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72250za7,
		BGl_z62sfunzf2Iinfozd2argszd2noescapezd2setz12z50zzintegrate_infoz00, 0L,
		BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2istampzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72251za7,
		BGl_z62sfunzf2Iinfozd2istampzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Iinfozd2handlerzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sexitza7f2iinfo2252z00,
		BGl_z62sexitzf2Iinfozd2handlerzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2istampzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72253za7,
		BGl_z62sfunzf2Iinfozd2istampz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2000z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2254za7,
		BGl_z62zc3z04anonymousza31616ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2001z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1615za7622255z00,
		BGl_z62lambda1615z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2002z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1614za7622256z00,
		BGl_z62lambda1614z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2003z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2257za7,
		BGl_z62zc3z04anonymousza31632ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2004z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1631za7622258z00,
		BGl_z62lambda1631z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2005z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1630za7622259z00,
		BGl_z62lambda1630z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2006z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1566za7622260z00,
		BGl_z62lambda1566z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2007z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2261za7,
		BGl_z62zc3z04anonymousza31565ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2008z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1562za7622262z00,
		BGl_z62lambda1562z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2009z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1554za7622263z00,
		BGl_z62lambda1554z62zzintegrate_infoz00, 0L, BUNSPEC, 6);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Iinfozd2nilzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762svarza7f2iinfoza72264za7,
		BGl_z62svarzf2Iinfozd2nilz42zzintegrate_infoz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Iinfozd2xhdlzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762svarza7f2iinfoza72265za7,
		BGl_z62svarzf2Iinfozd2xhdlz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2010z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1701za7622266z00,
		BGl_z62lambda1701z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2011z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1700za7622267z00,
		BGl_z62lambda1700z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2012z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2268za7,
		BGl_z62zc3z04anonymousza31711ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2013z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1710za7622269z00,
		BGl_z62lambda1710z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2014z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1709za7622270z00,
		BGl_z62lambda1709z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sexitzf2Iinfozd2uzd2markzd2setz12zd2envze0zzintegrate_infoz00,
		BgL_bgl_za762sexitza7f2iinfo2271z00,
		BGl_z62sexitzf2Iinfozd2uzd2markzd2setz12z50zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2015z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2272za7,
		BGl_z62zc3z04anonymousza31723ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2016z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1722za7622273z00,
		BGl_z62lambda1722z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2017z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1721za7622274z00,
		BGl_z62lambda1721z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2018z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2275za7,
		BGl_z62zc3z04anonymousza31738ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2019z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1737za7622276z00,
		BGl_z62lambda1737z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2effectzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72277za7,
		BGl_z62sfunzf2Iinfozd2effectzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2strengthzd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72278za7,
		BGl_z62sfunzf2Iinfozd2strengthz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2020z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1736za7622279z00,
		BGl_z62lambda1736z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2021z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2280za7,
		BGl_z62zc3z04anonymousza31750ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2022z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1749za7622281z00,
		BGl_z62lambda1749z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2023z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1748za7622282z00,
		BGl_z62lambda1748z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_svarzf2Iinfozd2fzd2markzd2setz12zd2envze0zzintegrate_infoz00,
		BgL_bgl_za762svarza7f2iinfoza72283za7,
		BGl_z62svarzf2Iinfozd2fzd2markzd2setz12z50zzintegrate_infoz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2024z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2284za7,
		BGl_z62zc3z04anonymousza31757ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2025z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1756za7622285z00,
		BGl_z62lambda1756z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2026z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1755za7622286z00,
		BGl_z62lambda1755z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2027z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2287za7,
		BGl_z62zc3z04anonymousza31770ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2028z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1769za7622288z00,
		BGl_z62lambda1769z62zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2029z00zzintegrate_infoz00,
		BgL_bgl_za762lambda1768za7622289z00,
		BGl_z62lambda1768z62zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2loczd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72290za7,
		BGl_z62sfunzf2Iinfozd2locz42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Gzf3zd2setz12zd2envzc1zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72291za7,
		BGl_z62sfunzf2Iinfozd2Gzf3zd2setz12z71zzintegrate_infoz00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2stackablezd2envzf2zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72292za7,
		BGl_z62sfunzf2Iinfozd2stackablez42zzintegrate_infoz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_sfunzf2Iinfozd2Cnzd2setz12zd2envz32zzintegrate_infoz00,
		BgL_bgl_za762sfunza7f2iinfoza72293za7,
		BGl_z62sfunzf2Iinfozd2Cnzd2setz12z82zzintegrate_infoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2030z00zzintegrate_infoz00,
		BgL_bgl_za762za7c3za704anonymo2294za7,
		BGl_z62zc3z04anonymousza31801ze3ze5zzintegrate_infoz00, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzintegrate_infoz00));
		     ADD_ROOT((void *) (&BGl_sfunzf2Iinfozf2zzintegrate_infoz00));
		     ADD_ROOT((void *) (&BGl_sexitzf2Iinfozf2zzintegrate_infoz00));
		     ADD_ROOT((void *) (&BGl_svarzf2Iinfozf2zzintegrate_infoz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_infoz00(long
		BgL_checksumz00_3647, char *BgL_fromz00_3648)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzintegrate_infoz00))
				{
					BGl_requirezd2initializa7ationz75zzintegrate_infoz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzintegrate_infoz00();
					BGl_libraryzd2moduleszd2initz00zzintegrate_infoz00();
					BGl_cnstzd2initzd2zzintegrate_infoz00();
					BGl_importedzd2moduleszd2initz00zzintegrate_infoz00();
					BGl_objectzd2initzd2zzintegrate_infoz00();
					return BGl_toplevelzd2initzd2zzintegrate_infoz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_infoz00(void)
	{
		{	/* Integrate/iinfo.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "integrate_info");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "integrate_info");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"integrate_info");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"integrate_info");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"integrate_info");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"integrate_info");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"integrate_info");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzintegrate_infoz00(void)
	{
		{	/* Integrate/iinfo.scm 15 */
			{	/* Integrate/iinfo.scm 15 */
				obj_t BgL_cportz00_3442;

				{	/* Integrate/iinfo.scm 15 */
					obj_t BgL_stringz00_3449;

					BgL_stringz00_3449 = BGl_string2076z00zzintegrate_infoz00;
					{	/* Integrate/iinfo.scm 15 */
						obj_t BgL_startz00_3450;

						BgL_startz00_3450 = BINT(0L);
						{	/* Integrate/iinfo.scm 15 */
							obj_t BgL_endz00_3451;

							BgL_endz00_3451 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3449)));
							{	/* Integrate/iinfo.scm 15 */

								BgL_cportz00_3442 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3449, BgL_startz00_3450, BgL_endz00_3451);
				}}}}
				{
					long BgL_iz00_3443;

					BgL_iz00_3443 = 33L;
				BgL_loopz00_3444:
					if ((BgL_iz00_3443 == -1L))
						{	/* Integrate/iinfo.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Integrate/iinfo.scm 15 */
							{	/* Integrate/iinfo.scm 15 */
								obj_t BgL_arg2077z00_3445;

								{	/* Integrate/iinfo.scm 15 */

									{	/* Integrate/iinfo.scm 15 */
										obj_t BgL_locationz00_3447;

										BgL_locationz00_3447 = BBOOL(((bool_t) 0));
										{	/* Integrate/iinfo.scm 15 */

											BgL_arg2077z00_3445 =
												BGl_readz00zz__readerz00(BgL_cportz00_3442,
												BgL_locationz00_3447);
										}
									}
								}
								{	/* Integrate/iinfo.scm 15 */
									int BgL_tmpz00_3674;

									BgL_tmpz00_3674 = (int) (BgL_iz00_3443);
									CNST_TABLE_SET(BgL_tmpz00_3674, BgL_arg2077z00_3445);
							}}
							{	/* Integrate/iinfo.scm 15 */
								int BgL_auxz00_3448;

								BgL_auxz00_3448 = (int) ((BgL_iz00_3443 - 1L));
								{
									long BgL_iz00_3679;

									BgL_iz00_3679 = (long) (BgL_auxz00_3448);
									BgL_iz00_3443 = BgL_iz00_3679;
									goto BgL_loopz00_3444;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzintegrate_infoz00(void)
	{
		{	/* Integrate/iinfo.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzintegrate_infoz00(void)
	{
		{	/* Integrate/iinfo.scm 15 */
			return BUNSPEC;
		}

	}



/* make-svar/Iinfo */
	BGL_EXPORTED_DEF BgL_svarz00_bglt
		BGl_makezd2svarzf2Iinfoz20zzintegrate_infoz00(obj_t BgL_loc1227z00_3,
		obj_t BgL_fzd2mark1228zd2_4, obj_t BgL_uzd2mark1229zd2_5,
		bool_t BgL_kapturedzf31230zf3_6, bool_t BgL_celledzf31231zf3_7,
		obj_t BgL_xhdl1232z00_8)
	{
		{	/* Integrate/iinfo.sch 141 */
			{	/* Integrate/iinfo.sch 141 */
				BgL_svarz00_bglt BgL_new1179z00_3453;

				{	/* Integrate/iinfo.sch 141 */
					BgL_svarz00_bglt BgL_tmp1177z00_3454;
					BgL_svarzf2iinfozf2_bglt BgL_wide1178z00_3455;

					{
						BgL_svarz00_bglt BgL_auxz00_3682;

						{	/* Integrate/iinfo.sch 141 */
							BgL_svarz00_bglt BgL_new1176z00_3456;

							BgL_new1176z00_3456 =
								((BgL_svarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_svarz00_bgl))));
							{	/* Integrate/iinfo.sch 141 */
								long BgL_arg1332z00_3457;

								BgL_arg1332z00_3457 = BGL_CLASS_NUM(BGl_svarz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1176z00_3456),
									BgL_arg1332z00_3457);
							}
							{	/* Integrate/iinfo.sch 141 */
								BgL_objectz00_bglt BgL_tmpz00_3687;

								BgL_tmpz00_3687 = ((BgL_objectz00_bglt) BgL_new1176z00_3456);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3687, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1176z00_3456);
							BgL_auxz00_3682 = BgL_new1176z00_3456;
						}
						BgL_tmp1177z00_3454 = ((BgL_svarz00_bglt) BgL_auxz00_3682);
					}
					BgL_wide1178z00_3455 =
						((BgL_svarzf2iinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_svarzf2iinfozf2_bgl))));
					{	/* Integrate/iinfo.sch 141 */
						obj_t BgL_auxz00_3695;
						BgL_objectz00_bglt BgL_tmpz00_3693;

						BgL_auxz00_3695 = ((obj_t) BgL_wide1178z00_3455);
						BgL_tmpz00_3693 = ((BgL_objectz00_bglt) BgL_tmp1177z00_3454);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3693, BgL_auxz00_3695);
					}
					((BgL_objectz00_bglt) BgL_tmp1177z00_3454);
					{	/* Integrate/iinfo.sch 141 */
						long BgL_arg1331z00_3458;

						BgL_arg1331z00_3458 =
							BGL_CLASS_NUM(BGl_svarzf2Iinfozf2zzintegrate_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1177z00_3454), BgL_arg1331z00_3458);
					}
					BgL_new1179z00_3453 = ((BgL_svarz00_bglt) BgL_tmp1177z00_3454);
				}
				((((BgL_svarz00_bglt) COBJECT(
								((BgL_svarz00_bglt) BgL_new1179z00_3453)))->BgL_locz00) =
					((obj_t) BgL_loc1227z00_3), BUNSPEC);
				{
					BgL_svarzf2iinfozf2_bglt BgL_auxz00_3705;

					{
						obj_t BgL_auxz00_3706;

						{	/* Integrate/iinfo.sch 141 */
							BgL_objectz00_bglt BgL_tmpz00_3707;

							BgL_tmpz00_3707 = ((BgL_objectz00_bglt) BgL_new1179z00_3453);
							BgL_auxz00_3706 = BGL_OBJECT_WIDENING(BgL_tmpz00_3707);
						}
						BgL_auxz00_3705 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3706);
					}
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3705))->
							BgL_fzd2markzd2) = ((obj_t) BgL_fzd2mark1228zd2_4), BUNSPEC);
				}
				{
					BgL_svarzf2iinfozf2_bglt BgL_auxz00_3712;

					{
						obj_t BgL_auxz00_3713;

						{	/* Integrate/iinfo.sch 141 */
							BgL_objectz00_bglt BgL_tmpz00_3714;

							BgL_tmpz00_3714 = ((BgL_objectz00_bglt) BgL_new1179z00_3453);
							BgL_auxz00_3713 = BGL_OBJECT_WIDENING(BgL_tmpz00_3714);
						}
						BgL_auxz00_3712 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3713);
					}
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3712))->
							BgL_uzd2markzd2) = ((obj_t) BgL_uzd2mark1229zd2_5), BUNSPEC);
				}
				{
					BgL_svarzf2iinfozf2_bglt BgL_auxz00_3719;

					{
						obj_t BgL_auxz00_3720;

						{	/* Integrate/iinfo.sch 141 */
							BgL_objectz00_bglt BgL_tmpz00_3721;

							BgL_tmpz00_3721 = ((BgL_objectz00_bglt) BgL_new1179z00_3453);
							BgL_auxz00_3720 = BGL_OBJECT_WIDENING(BgL_tmpz00_3721);
						}
						BgL_auxz00_3719 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3720);
					}
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3719))->
							BgL_kapturedzf3zf3) =
						((bool_t) BgL_kapturedzf31230zf3_6), BUNSPEC);
				}
				{
					BgL_svarzf2iinfozf2_bglt BgL_auxz00_3726;

					{
						obj_t BgL_auxz00_3727;

						{	/* Integrate/iinfo.sch 141 */
							BgL_objectz00_bglt BgL_tmpz00_3728;

							BgL_tmpz00_3728 = ((BgL_objectz00_bglt) BgL_new1179z00_3453);
							BgL_auxz00_3727 = BGL_OBJECT_WIDENING(BgL_tmpz00_3728);
						}
						BgL_auxz00_3726 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3727);
					}
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3726))->
							BgL_celledzf3zf3) = ((bool_t) BgL_celledzf31231zf3_7), BUNSPEC);
				}
				{
					BgL_svarzf2iinfozf2_bglt BgL_auxz00_3733;

					{
						obj_t BgL_auxz00_3734;

						{	/* Integrate/iinfo.sch 141 */
							BgL_objectz00_bglt BgL_tmpz00_3735;

							BgL_tmpz00_3735 = ((BgL_objectz00_bglt) BgL_new1179z00_3453);
							BgL_auxz00_3734 = BGL_OBJECT_WIDENING(BgL_tmpz00_3735);
						}
						BgL_auxz00_3733 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3734);
					}
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3733))->
							BgL_xhdlz00) = ((obj_t) BgL_xhdl1232z00_8), BUNSPEC);
				}
				return BgL_new1179z00_3453;
			}
		}

	}



/* &make-svar/Iinfo */
	BgL_svarz00_bglt BGl_z62makezd2svarzf2Iinfoz42zzintegrate_infoz00(obj_t
		BgL_envz00_2758, obj_t BgL_loc1227z00_2759, obj_t BgL_fzd2mark1228zd2_2760,
		obj_t BgL_uzd2mark1229zd2_2761, obj_t BgL_kapturedzf31230zf3_2762,
		obj_t BgL_celledzf31231zf3_2763, obj_t BgL_xhdl1232z00_2764)
	{
		{	/* Integrate/iinfo.sch 141 */
			return
				BGl_makezd2svarzf2Iinfoz20zzintegrate_infoz00(BgL_loc1227z00_2759,
				BgL_fzd2mark1228zd2_2760, BgL_uzd2mark1229zd2_2761,
				CBOOL(BgL_kapturedzf31230zf3_2762), CBOOL(BgL_celledzf31231zf3_2763),
				BgL_xhdl1232z00_2764);
		}

	}



/* svar/Iinfo? */
	BGL_EXPORTED_DEF bool_t BGl_svarzf2Iinfozf3z01zzintegrate_infoz00(obj_t
		BgL_objz00_9)
	{
		{	/* Integrate/iinfo.sch 142 */
			{	/* Integrate/iinfo.sch 142 */
				obj_t BgL_classz00_3459;

				BgL_classz00_3459 = BGl_svarzf2Iinfozf2zzintegrate_infoz00;
				if (BGL_OBJECTP(BgL_objz00_9))
					{	/* Integrate/iinfo.sch 142 */
						BgL_objectz00_bglt BgL_arg1807z00_3460;

						BgL_arg1807z00_3460 = (BgL_objectz00_bglt) (BgL_objz00_9);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Integrate/iinfo.sch 142 */
								long BgL_idxz00_3461;

								BgL_idxz00_3461 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3460);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3461 + 3L)) == BgL_classz00_3459);
							}
						else
							{	/* Integrate/iinfo.sch 142 */
								bool_t BgL_res1972z00_3464;

								{	/* Integrate/iinfo.sch 142 */
									obj_t BgL_oclassz00_3465;

									{	/* Integrate/iinfo.sch 142 */
										obj_t BgL_arg1815z00_3466;
										long BgL_arg1816z00_3467;

										BgL_arg1815z00_3466 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Integrate/iinfo.sch 142 */
											long BgL_arg1817z00_3468;

											BgL_arg1817z00_3468 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3460);
											BgL_arg1816z00_3467 = (BgL_arg1817z00_3468 - OBJECT_TYPE);
										}
										BgL_oclassz00_3465 =
											VECTOR_REF(BgL_arg1815z00_3466, BgL_arg1816z00_3467);
									}
									{	/* Integrate/iinfo.sch 142 */
										bool_t BgL__ortest_1115z00_3469;

										BgL__ortest_1115z00_3469 =
											(BgL_classz00_3459 == BgL_oclassz00_3465);
										if (BgL__ortest_1115z00_3469)
											{	/* Integrate/iinfo.sch 142 */
												BgL_res1972z00_3464 = BgL__ortest_1115z00_3469;
											}
										else
											{	/* Integrate/iinfo.sch 142 */
												long BgL_odepthz00_3470;

												{	/* Integrate/iinfo.sch 142 */
													obj_t BgL_arg1804z00_3471;

													BgL_arg1804z00_3471 = (BgL_oclassz00_3465);
													BgL_odepthz00_3470 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3471);
												}
												if ((3L < BgL_odepthz00_3470))
													{	/* Integrate/iinfo.sch 142 */
														obj_t BgL_arg1802z00_3472;

														{	/* Integrate/iinfo.sch 142 */
															obj_t BgL_arg1803z00_3473;

															BgL_arg1803z00_3473 = (BgL_oclassz00_3465);
															BgL_arg1802z00_3472 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3473,
																3L);
														}
														BgL_res1972z00_3464 =
															(BgL_arg1802z00_3472 == BgL_classz00_3459);
													}
												else
													{	/* Integrate/iinfo.sch 142 */
														BgL_res1972z00_3464 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1972z00_3464;
							}
					}
				else
					{	/* Integrate/iinfo.sch 142 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &svar/Iinfo? */
	obj_t BGl_z62svarzf2Iinfozf3z63zzintegrate_infoz00(obj_t BgL_envz00_2765,
		obj_t BgL_objz00_2766)
	{
		{	/* Integrate/iinfo.sch 142 */
			return BBOOL(BGl_svarzf2Iinfozf3z01zzintegrate_infoz00(BgL_objz00_2766));
		}

	}



/* svar/Iinfo-nil */
	BGL_EXPORTED_DEF BgL_svarz00_bglt
		BGl_svarzf2Iinfozd2nilz20zzintegrate_infoz00(void)
	{
		{	/* Integrate/iinfo.sch 143 */
			{	/* Integrate/iinfo.sch 143 */
				obj_t BgL_classz00_2340;

				BgL_classz00_2340 = BGl_svarzf2Iinfozf2zzintegrate_infoz00;
				{	/* Integrate/iinfo.sch 143 */
					obj_t BgL__ortest_1117z00_2341;

					BgL__ortest_1117z00_2341 = BGL_CLASS_NIL(BgL_classz00_2340);
					if (CBOOL(BgL__ortest_1117z00_2341))
						{	/* Integrate/iinfo.sch 143 */
							return ((BgL_svarz00_bglt) BgL__ortest_1117z00_2341);
						}
					else
						{	/* Integrate/iinfo.sch 143 */
							return
								((BgL_svarz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2340));
						}
				}
			}
		}

	}



/* &svar/Iinfo-nil */
	BgL_svarz00_bglt BGl_z62svarzf2Iinfozd2nilz42zzintegrate_infoz00(obj_t
		BgL_envz00_2767)
	{
		{	/* Integrate/iinfo.sch 143 */
			return BGl_svarzf2Iinfozd2nilz20zzintegrate_infoz00();
		}

	}



/* svar/Iinfo-xhdl */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Iinfozd2xhdlz20zzintegrate_infoz00(BgL_svarz00_bglt BgL_oz00_10)
	{
		{	/* Integrate/iinfo.sch 144 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_3774;

				{
					obj_t BgL_auxz00_3775;

					{	/* Integrate/iinfo.sch 144 */
						BgL_objectz00_bglt BgL_tmpz00_3776;

						BgL_tmpz00_3776 = ((BgL_objectz00_bglt) BgL_oz00_10);
						BgL_auxz00_3775 = BGL_OBJECT_WIDENING(BgL_tmpz00_3776);
					}
					BgL_auxz00_3774 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3775);
				}
				return
					(((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3774))->BgL_xhdlz00);
			}
		}

	}



/* &svar/Iinfo-xhdl */
	obj_t BGl_z62svarzf2Iinfozd2xhdlz42zzintegrate_infoz00(obj_t BgL_envz00_2768,
		obj_t BgL_oz00_2769)
	{
		{	/* Integrate/iinfo.sch 144 */
			return
				BGl_svarzf2Iinfozd2xhdlz20zzintegrate_infoz00(
				((BgL_svarz00_bglt) BgL_oz00_2769));
		}

	}



/* svar/Iinfo-xhdl-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Iinfozd2xhdlzd2setz12ze0zzintegrate_infoz00(BgL_svarz00_bglt
		BgL_oz00_11, obj_t BgL_vz00_12)
	{
		{	/* Integrate/iinfo.sch 145 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_3783;

				{
					obj_t BgL_auxz00_3784;

					{	/* Integrate/iinfo.sch 145 */
						BgL_objectz00_bglt BgL_tmpz00_3785;

						BgL_tmpz00_3785 = ((BgL_objectz00_bglt) BgL_oz00_11);
						BgL_auxz00_3784 = BGL_OBJECT_WIDENING(BgL_tmpz00_3785);
					}
					BgL_auxz00_3783 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3784);
				}
				return
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3783))->
						BgL_xhdlz00) = ((obj_t) BgL_vz00_12), BUNSPEC);
			}
		}

	}



/* &svar/Iinfo-xhdl-set! */
	obj_t BGl_z62svarzf2Iinfozd2xhdlzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2770, obj_t BgL_oz00_2771, obj_t BgL_vz00_2772)
	{
		{	/* Integrate/iinfo.sch 145 */
			return
				BGl_svarzf2Iinfozd2xhdlzd2setz12ze0zzintegrate_infoz00(
				((BgL_svarz00_bglt) BgL_oz00_2771), BgL_vz00_2772);
		}

	}



/* svar/Iinfo-celled? */
	BGL_EXPORTED_DEF bool_t
		BGl_svarzf2Iinfozd2celledzf3zd3zzintegrate_infoz00(BgL_svarz00_bglt
		BgL_oz00_13)
	{
		{	/* Integrate/iinfo.sch 146 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_3792;

				{
					obj_t BgL_auxz00_3793;

					{	/* Integrate/iinfo.sch 146 */
						BgL_objectz00_bglt BgL_tmpz00_3794;

						BgL_tmpz00_3794 = ((BgL_objectz00_bglt) BgL_oz00_13);
						BgL_auxz00_3793 = BGL_OBJECT_WIDENING(BgL_tmpz00_3794);
					}
					BgL_auxz00_3792 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3793);
				}
				return
					(((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3792))->
					BgL_celledzf3zf3);
			}
		}

	}



/* &svar/Iinfo-celled? */
	obj_t BGl_z62svarzf2Iinfozd2celledzf3zb1zzintegrate_infoz00(obj_t
		BgL_envz00_2773, obj_t BgL_oz00_2774)
	{
		{	/* Integrate/iinfo.sch 146 */
			return
				BBOOL(BGl_svarzf2Iinfozd2celledzf3zd3zzintegrate_infoz00(
					((BgL_svarz00_bglt) BgL_oz00_2774)));
		}

	}



/* svar/Iinfo-celled?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Iinfozd2celledzf3zd2setz12z13zzintegrate_infoz00(BgL_svarz00_bglt
		BgL_oz00_14, bool_t BgL_vz00_15)
	{
		{	/* Integrate/iinfo.sch 147 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_3802;

				{
					obj_t BgL_auxz00_3803;

					{	/* Integrate/iinfo.sch 147 */
						BgL_objectz00_bglt BgL_tmpz00_3804;

						BgL_tmpz00_3804 = ((BgL_objectz00_bglt) BgL_oz00_14);
						BgL_auxz00_3803 = BGL_OBJECT_WIDENING(BgL_tmpz00_3804);
					}
					BgL_auxz00_3802 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3803);
				}
				return
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3802))->
						BgL_celledzf3zf3) = ((bool_t) BgL_vz00_15), BUNSPEC);
			}
		}

	}



/* &svar/Iinfo-celled?-set! */
	obj_t BGl_z62svarzf2Iinfozd2celledzf3zd2setz12z71zzintegrate_infoz00(obj_t
		BgL_envz00_2775, obj_t BgL_oz00_2776, obj_t BgL_vz00_2777)
	{
		{	/* Integrate/iinfo.sch 147 */
			return
				BGl_svarzf2Iinfozd2celledzf3zd2setz12z13zzintegrate_infoz00(
				((BgL_svarz00_bglt) BgL_oz00_2776), CBOOL(BgL_vz00_2777));
		}

	}



/* svar/Iinfo-kaptured? */
	BGL_EXPORTED_DEF bool_t
		BGl_svarzf2Iinfozd2kapturedzf3zd3zzintegrate_infoz00(BgL_svarz00_bglt
		BgL_oz00_16)
	{
		{	/* Integrate/iinfo.sch 148 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_3812;

				{
					obj_t BgL_auxz00_3813;

					{	/* Integrate/iinfo.sch 148 */
						BgL_objectz00_bglt BgL_tmpz00_3814;

						BgL_tmpz00_3814 = ((BgL_objectz00_bglt) BgL_oz00_16);
						BgL_auxz00_3813 = BGL_OBJECT_WIDENING(BgL_tmpz00_3814);
					}
					BgL_auxz00_3812 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3813);
				}
				return
					(((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3812))->
					BgL_kapturedzf3zf3);
			}
		}

	}



/* &svar/Iinfo-kaptured? */
	obj_t BGl_z62svarzf2Iinfozd2kapturedzf3zb1zzintegrate_infoz00(obj_t
		BgL_envz00_2778, obj_t BgL_oz00_2779)
	{
		{	/* Integrate/iinfo.sch 148 */
			return
				BBOOL(BGl_svarzf2Iinfozd2kapturedzf3zd3zzintegrate_infoz00(
					((BgL_svarz00_bglt) BgL_oz00_2779)));
		}

	}



/* svar/Iinfo-kaptured?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Iinfozd2kapturedzf3zd2setz12z13zzintegrate_infoz00
		(BgL_svarz00_bglt BgL_oz00_17, bool_t BgL_vz00_18)
	{
		{	/* Integrate/iinfo.sch 149 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_3822;

				{
					obj_t BgL_auxz00_3823;

					{	/* Integrate/iinfo.sch 149 */
						BgL_objectz00_bglt BgL_tmpz00_3824;

						BgL_tmpz00_3824 = ((BgL_objectz00_bglt) BgL_oz00_17);
						BgL_auxz00_3823 = BGL_OBJECT_WIDENING(BgL_tmpz00_3824);
					}
					BgL_auxz00_3822 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3823);
				}
				return
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3822))->
						BgL_kapturedzf3zf3) = ((bool_t) BgL_vz00_18), BUNSPEC);
			}
		}

	}



/* &svar/Iinfo-kaptured?-set! */
	obj_t BGl_z62svarzf2Iinfozd2kapturedzf3zd2setz12z71zzintegrate_infoz00(obj_t
		BgL_envz00_2780, obj_t BgL_oz00_2781, obj_t BgL_vz00_2782)
	{
		{	/* Integrate/iinfo.sch 149 */
			return
				BGl_svarzf2Iinfozd2kapturedzf3zd2setz12z13zzintegrate_infoz00(
				((BgL_svarz00_bglt) BgL_oz00_2781), CBOOL(BgL_vz00_2782));
		}

	}



/* svar/Iinfo-u-mark */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Iinfozd2uzd2markzf2zzintegrate_infoz00(BgL_svarz00_bglt
		BgL_oz00_19)
	{
		{	/* Integrate/iinfo.sch 150 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_3832;

				{
					obj_t BgL_auxz00_3833;

					{	/* Integrate/iinfo.sch 150 */
						BgL_objectz00_bglt BgL_tmpz00_3834;

						BgL_tmpz00_3834 = ((BgL_objectz00_bglt) BgL_oz00_19);
						BgL_auxz00_3833 = BGL_OBJECT_WIDENING(BgL_tmpz00_3834);
					}
					BgL_auxz00_3832 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3833);
				}
				return
					(((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3832))->
					BgL_uzd2markzd2);
			}
		}

	}



/* &svar/Iinfo-u-mark */
	obj_t BGl_z62svarzf2Iinfozd2uzd2markz90zzintegrate_infoz00(obj_t
		BgL_envz00_2783, obj_t BgL_oz00_2784)
	{
		{	/* Integrate/iinfo.sch 150 */
			return
				BGl_svarzf2Iinfozd2uzd2markzf2zzintegrate_infoz00(
				((BgL_svarz00_bglt) BgL_oz00_2784));
		}

	}



/* svar/Iinfo-u-mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Iinfozd2uzd2markzd2setz12z32zzintegrate_infoz00(BgL_svarz00_bglt
		BgL_oz00_20, obj_t BgL_vz00_21)
	{
		{	/* Integrate/iinfo.sch 151 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_3841;

				{
					obj_t BgL_auxz00_3842;

					{	/* Integrate/iinfo.sch 151 */
						BgL_objectz00_bglt BgL_tmpz00_3843;

						BgL_tmpz00_3843 = ((BgL_objectz00_bglt) BgL_oz00_20);
						BgL_auxz00_3842 = BGL_OBJECT_WIDENING(BgL_tmpz00_3843);
					}
					BgL_auxz00_3841 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3842);
				}
				return
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3841))->
						BgL_uzd2markzd2) = ((obj_t) BgL_vz00_21), BUNSPEC);
			}
		}

	}



/* &svar/Iinfo-u-mark-set! */
	obj_t BGl_z62svarzf2Iinfozd2uzd2markzd2setz12z50zzintegrate_infoz00(obj_t
		BgL_envz00_2785, obj_t BgL_oz00_2786, obj_t BgL_vz00_2787)
	{
		{	/* Integrate/iinfo.sch 151 */
			return
				BGl_svarzf2Iinfozd2uzd2markzd2setz12z32zzintegrate_infoz00(
				((BgL_svarz00_bglt) BgL_oz00_2786), BgL_vz00_2787);
		}

	}



/* svar/Iinfo-f-mark */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Iinfozd2fzd2markzf2zzintegrate_infoz00(BgL_svarz00_bglt
		BgL_oz00_22)
	{
		{	/* Integrate/iinfo.sch 152 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_3850;

				{
					obj_t BgL_auxz00_3851;

					{	/* Integrate/iinfo.sch 152 */
						BgL_objectz00_bglt BgL_tmpz00_3852;

						BgL_tmpz00_3852 = ((BgL_objectz00_bglt) BgL_oz00_22);
						BgL_auxz00_3851 = BGL_OBJECT_WIDENING(BgL_tmpz00_3852);
					}
					BgL_auxz00_3850 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3851);
				}
				return
					(((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3850))->
					BgL_fzd2markzd2);
			}
		}

	}



/* &svar/Iinfo-f-mark */
	obj_t BGl_z62svarzf2Iinfozd2fzd2markz90zzintegrate_infoz00(obj_t
		BgL_envz00_2788, obj_t BgL_oz00_2789)
	{
		{	/* Integrate/iinfo.sch 152 */
			return
				BGl_svarzf2Iinfozd2fzd2markzf2zzintegrate_infoz00(
				((BgL_svarz00_bglt) BgL_oz00_2789));
		}

	}



/* svar/Iinfo-f-mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Iinfozd2fzd2markzd2setz12z32zzintegrate_infoz00(BgL_svarz00_bglt
		BgL_oz00_23, obj_t BgL_vz00_24)
	{
		{	/* Integrate/iinfo.sch 153 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_3859;

				{
					obj_t BgL_auxz00_3860;

					{	/* Integrate/iinfo.sch 153 */
						BgL_objectz00_bglt BgL_tmpz00_3861;

						BgL_tmpz00_3861 = ((BgL_objectz00_bglt) BgL_oz00_23);
						BgL_auxz00_3860 = BGL_OBJECT_WIDENING(BgL_tmpz00_3861);
					}
					BgL_auxz00_3859 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3860);
				}
				return
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3859))->
						BgL_fzd2markzd2) = ((obj_t) BgL_vz00_24), BUNSPEC);
			}
		}

	}



/* &svar/Iinfo-f-mark-set! */
	obj_t BGl_z62svarzf2Iinfozd2fzd2markzd2setz12z50zzintegrate_infoz00(obj_t
		BgL_envz00_2790, obj_t BgL_oz00_2791, obj_t BgL_vz00_2792)
	{
		{	/* Integrate/iinfo.sch 153 */
			return
				BGl_svarzf2Iinfozd2fzd2markzd2setz12z32zzintegrate_infoz00(
				((BgL_svarz00_bglt) BgL_oz00_2791), BgL_vz00_2792);
		}

	}



/* svar/Iinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Iinfozd2locz20zzintegrate_infoz00(BgL_svarz00_bglt BgL_oz00_25)
	{
		{	/* Integrate/iinfo.sch 154 */
			return
				(((BgL_svarz00_bglt) COBJECT(
						((BgL_svarz00_bglt) BgL_oz00_25)))->BgL_locz00);
		}

	}



/* &svar/Iinfo-loc */
	obj_t BGl_z62svarzf2Iinfozd2locz42zzintegrate_infoz00(obj_t BgL_envz00_2793,
		obj_t BgL_oz00_2794)
	{
		{	/* Integrate/iinfo.sch 154 */
			return
				BGl_svarzf2Iinfozd2locz20zzintegrate_infoz00(
				((BgL_svarz00_bglt) BgL_oz00_2794));
		}

	}



/* svar/Iinfo-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_svarzf2Iinfozd2loczd2setz12ze0zzintegrate_infoz00(BgL_svarz00_bglt
		BgL_oz00_26, obj_t BgL_vz00_27)
	{
		{	/* Integrate/iinfo.sch 155 */
			return
				((((BgL_svarz00_bglt) COBJECT(
							((BgL_svarz00_bglt) BgL_oz00_26)))->BgL_locz00) =
				((obj_t) BgL_vz00_27), BUNSPEC);
		}

	}



/* &svar/Iinfo-loc-set! */
	obj_t BGl_z62svarzf2Iinfozd2loczd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2795, obj_t BgL_oz00_2796, obj_t BgL_vz00_2797)
	{
		{	/* Integrate/iinfo.sch 155 */
			return
				BGl_svarzf2Iinfozd2loczd2setz12ze0zzintegrate_infoz00(
				((BgL_svarz00_bglt) BgL_oz00_2796), BgL_vz00_2797);
		}

	}



/* make-sexit/Iinfo */
	BGL_EXPORTED_DEF BgL_sexitz00_bglt
		BGl_makezd2sexitzf2Iinfoz20zzintegrate_infoz00(obj_t BgL_handler1220z00_28,
		bool_t BgL_detachedzf31221zf3_29, obj_t BgL_fzd2mark1222zd2_30,
		obj_t BgL_uzd2mark1223zd2_31, bool_t BgL_kapturedzf31224zf3_32,
		bool_t BgL_celledzf31225zf3_33)
	{
		{	/* Integrate/iinfo.sch 158 */
			{	/* Integrate/iinfo.sch 158 */
				BgL_sexitz00_bglt BgL_new1183z00_3474;

				{	/* Integrate/iinfo.sch 158 */
					BgL_sexitz00_bglt BgL_tmp1181z00_3475;
					BgL_sexitzf2iinfozf2_bglt BgL_wide1182z00_3476;

					{
						BgL_sexitz00_bglt BgL_auxz00_3876;

						{	/* Integrate/iinfo.sch 158 */
							BgL_sexitz00_bglt BgL_new1180z00_3477;

							BgL_new1180z00_3477 =
								((BgL_sexitz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_sexitz00_bgl))));
							{	/* Integrate/iinfo.sch 158 */
								long BgL_arg1335z00_3478;

								BgL_arg1335z00_3478 = BGL_CLASS_NUM(BGl_sexitz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1180z00_3477),
									BgL_arg1335z00_3478);
							}
							{	/* Integrate/iinfo.sch 158 */
								BgL_objectz00_bglt BgL_tmpz00_3881;

								BgL_tmpz00_3881 = ((BgL_objectz00_bglt) BgL_new1180z00_3477);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3881, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1180z00_3477);
							BgL_auxz00_3876 = BgL_new1180z00_3477;
						}
						BgL_tmp1181z00_3475 = ((BgL_sexitz00_bglt) BgL_auxz00_3876);
					}
					BgL_wide1182z00_3476 =
						((BgL_sexitzf2iinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sexitzf2iinfozf2_bgl))));
					{	/* Integrate/iinfo.sch 158 */
						obj_t BgL_auxz00_3889;
						BgL_objectz00_bglt BgL_tmpz00_3887;

						BgL_auxz00_3889 = ((obj_t) BgL_wide1182z00_3476);
						BgL_tmpz00_3887 = ((BgL_objectz00_bglt) BgL_tmp1181z00_3475);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3887, BgL_auxz00_3889);
					}
					((BgL_objectz00_bglt) BgL_tmp1181z00_3475);
					{	/* Integrate/iinfo.sch 158 */
						long BgL_arg1333z00_3479;

						BgL_arg1333z00_3479 =
							BGL_CLASS_NUM(BGl_sexitzf2Iinfozf2zzintegrate_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1181z00_3475), BgL_arg1333z00_3479);
					}
					BgL_new1183z00_3474 = ((BgL_sexitz00_bglt) BgL_tmp1181z00_3475);
				}
				((((BgL_sexitz00_bglt) COBJECT(
								((BgL_sexitz00_bglt) BgL_new1183z00_3474)))->BgL_handlerz00) =
					((obj_t) BgL_handler1220z00_28), BUNSPEC);
				((((BgL_sexitz00_bglt) COBJECT(((BgL_sexitz00_bglt)
									BgL_new1183z00_3474)))->BgL_detachedzf3zf3) =
					((bool_t) BgL_detachedzf31221zf3_29), BUNSPEC);
				{
					BgL_sexitzf2iinfozf2_bglt BgL_auxz00_3901;

					{
						obj_t BgL_auxz00_3902;

						{	/* Integrate/iinfo.sch 158 */
							BgL_objectz00_bglt BgL_tmpz00_3903;

							BgL_tmpz00_3903 = ((BgL_objectz00_bglt) BgL_new1183z00_3474);
							BgL_auxz00_3902 = BGL_OBJECT_WIDENING(BgL_tmpz00_3903);
						}
						BgL_auxz00_3901 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_3902);
					}
					((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3901))->
							BgL_fzd2markzd2) = ((obj_t) BgL_fzd2mark1222zd2_30), BUNSPEC);
				}
				{
					BgL_sexitzf2iinfozf2_bglt BgL_auxz00_3908;

					{
						obj_t BgL_auxz00_3909;

						{	/* Integrate/iinfo.sch 158 */
							BgL_objectz00_bglt BgL_tmpz00_3910;

							BgL_tmpz00_3910 = ((BgL_objectz00_bglt) BgL_new1183z00_3474);
							BgL_auxz00_3909 = BGL_OBJECT_WIDENING(BgL_tmpz00_3910);
						}
						BgL_auxz00_3908 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_3909);
					}
					((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3908))->
							BgL_uzd2markzd2) = ((obj_t) BgL_uzd2mark1223zd2_31), BUNSPEC);
				}
				{
					BgL_sexitzf2iinfozf2_bglt BgL_auxz00_3915;

					{
						obj_t BgL_auxz00_3916;

						{	/* Integrate/iinfo.sch 158 */
							BgL_objectz00_bglt BgL_tmpz00_3917;

							BgL_tmpz00_3917 = ((BgL_objectz00_bglt) BgL_new1183z00_3474);
							BgL_auxz00_3916 = BGL_OBJECT_WIDENING(BgL_tmpz00_3917);
						}
						BgL_auxz00_3915 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_3916);
					}
					((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3915))->
							BgL_kapturedzf3zf3) =
						((bool_t) BgL_kapturedzf31224zf3_32), BUNSPEC);
				}
				{
					BgL_sexitzf2iinfozf2_bglt BgL_auxz00_3922;

					{
						obj_t BgL_auxz00_3923;

						{	/* Integrate/iinfo.sch 158 */
							BgL_objectz00_bglt BgL_tmpz00_3924;

							BgL_tmpz00_3924 = ((BgL_objectz00_bglt) BgL_new1183z00_3474);
							BgL_auxz00_3923 = BGL_OBJECT_WIDENING(BgL_tmpz00_3924);
						}
						BgL_auxz00_3922 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_3923);
					}
					((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3922))->
							BgL_celledzf3zf3) = ((bool_t) BgL_celledzf31225zf3_33), BUNSPEC);
				}
				return BgL_new1183z00_3474;
			}
		}

	}



/* &make-sexit/Iinfo */
	BgL_sexitz00_bglt BGl_z62makezd2sexitzf2Iinfoz42zzintegrate_infoz00(obj_t
		BgL_envz00_2798, obj_t BgL_handler1220z00_2799,
		obj_t BgL_detachedzf31221zf3_2800, obj_t BgL_fzd2mark1222zd2_2801,
		obj_t BgL_uzd2mark1223zd2_2802, obj_t BgL_kapturedzf31224zf3_2803,
		obj_t BgL_celledzf31225zf3_2804)
	{
		{	/* Integrate/iinfo.sch 158 */
			return
				BGl_makezd2sexitzf2Iinfoz20zzintegrate_infoz00(BgL_handler1220z00_2799,
				CBOOL(BgL_detachedzf31221zf3_2800), BgL_fzd2mark1222zd2_2801,
				BgL_uzd2mark1223zd2_2802, CBOOL(BgL_kapturedzf31224zf3_2803),
				CBOOL(BgL_celledzf31225zf3_2804));
		}

	}



/* sexit/Iinfo? */
	BGL_EXPORTED_DEF bool_t BGl_sexitzf2Iinfozf3z01zzintegrate_infoz00(obj_t
		BgL_objz00_34)
	{
		{	/* Integrate/iinfo.sch 159 */
			{	/* Integrate/iinfo.sch 159 */
				obj_t BgL_classz00_3480;

				BgL_classz00_3480 = BGl_sexitzf2Iinfozf2zzintegrate_infoz00;
				if (BGL_OBJECTP(BgL_objz00_34))
					{	/* Integrate/iinfo.sch 159 */
						BgL_objectz00_bglt BgL_arg1807z00_3481;

						BgL_arg1807z00_3481 = (BgL_objectz00_bglt) (BgL_objz00_34);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Integrate/iinfo.sch 159 */
								long BgL_idxz00_3482;

								BgL_idxz00_3482 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3481);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3482 + 3L)) == BgL_classz00_3480);
							}
						else
							{	/* Integrate/iinfo.sch 159 */
								bool_t BgL_res1973z00_3485;

								{	/* Integrate/iinfo.sch 159 */
									obj_t BgL_oclassz00_3486;

									{	/* Integrate/iinfo.sch 159 */
										obj_t BgL_arg1815z00_3487;
										long BgL_arg1816z00_3488;

										BgL_arg1815z00_3487 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Integrate/iinfo.sch 159 */
											long BgL_arg1817z00_3489;

											BgL_arg1817z00_3489 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3481);
											BgL_arg1816z00_3488 = (BgL_arg1817z00_3489 - OBJECT_TYPE);
										}
										BgL_oclassz00_3486 =
											VECTOR_REF(BgL_arg1815z00_3487, BgL_arg1816z00_3488);
									}
									{	/* Integrate/iinfo.sch 159 */
										bool_t BgL__ortest_1115z00_3490;

										BgL__ortest_1115z00_3490 =
											(BgL_classz00_3480 == BgL_oclassz00_3486);
										if (BgL__ortest_1115z00_3490)
											{	/* Integrate/iinfo.sch 159 */
												BgL_res1973z00_3485 = BgL__ortest_1115z00_3490;
											}
										else
											{	/* Integrate/iinfo.sch 159 */
												long BgL_odepthz00_3491;

												{	/* Integrate/iinfo.sch 159 */
													obj_t BgL_arg1804z00_3492;

													BgL_arg1804z00_3492 = (BgL_oclassz00_3486);
													BgL_odepthz00_3491 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3492);
												}
												if ((3L < BgL_odepthz00_3491))
													{	/* Integrate/iinfo.sch 159 */
														obj_t BgL_arg1802z00_3493;

														{	/* Integrate/iinfo.sch 159 */
															obj_t BgL_arg1803z00_3494;

															BgL_arg1803z00_3494 = (BgL_oclassz00_3486);
															BgL_arg1802z00_3493 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3494,
																3L);
														}
														BgL_res1973z00_3485 =
															(BgL_arg1802z00_3493 == BgL_classz00_3480);
													}
												else
													{	/* Integrate/iinfo.sch 159 */
														BgL_res1973z00_3485 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1973z00_3485;
							}
					}
				else
					{	/* Integrate/iinfo.sch 159 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &sexit/Iinfo? */
	obj_t BGl_z62sexitzf2Iinfozf3z63zzintegrate_infoz00(obj_t BgL_envz00_2805,
		obj_t BgL_objz00_2806)
	{
		{	/* Integrate/iinfo.sch 159 */
			return BBOOL(BGl_sexitzf2Iinfozf3z01zzintegrate_infoz00(BgL_objz00_2806));
		}

	}



/* sexit/Iinfo-nil */
	BGL_EXPORTED_DEF BgL_sexitz00_bglt
		BGl_sexitzf2Iinfozd2nilz20zzintegrate_infoz00(void)
	{
		{	/* Integrate/iinfo.sch 160 */
			{	/* Integrate/iinfo.sch 160 */
				obj_t BgL_classz00_2404;

				BgL_classz00_2404 = BGl_sexitzf2Iinfozf2zzintegrate_infoz00;
				{	/* Integrate/iinfo.sch 160 */
					obj_t BgL__ortest_1117z00_2405;

					BgL__ortest_1117z00_2405 = BGL_CLASS_NIL(BgL_classz00_2404);
					if (CBOOL(BgL__ortest_1117z00_2405))
						{	/* Integrate/iinfo.sch 160 */
							return ((BgL_sexitz00_bglt) BgL__ortest_1117z00_2405);
						}
					else
						{	/* Integrate/iinfo.sch 160 */
							return
								((BgL_sexitz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2404));
						}
				}
			}
		}

	}



/* &sexit/Iinfo-nil */
	BgL_sexitz00_bglt BGl_z62sexitzf2Iinfozd2nilz42zzintegrate_infoz00(obj_t
		BgL_envz00_2807)
	{
		{	/* Integrate/iinfo.sch 160 */
			return BGl_sexitzf2Iinfozd2nilz20zzintegrate_infoz00();
		}

	}



/* sexit/Iinfo-celled? */
	BGL_EXPORTED_DEF bool_t
		BGl_sexitzf2Iinfozd2celledzf3zd3zzintegrate_infoz00(BgL_sexitz00_bglt
		BgL_oz00_35)
	{
		{	/* Integrate/iinfo.sch 161 */
			{
				BgL_sexitzf2iinfozf2_bglt BgL_auxz00_3964;

				{
					obj_t BgL_auxz00_3965;

					{	/* Integrate/iinfo.sch 161 */
						BgL_objectz00_bglt BgL_tmpz00_3966;

						BgL_tmpz00_3966 = ((BgL_objectz00_bglt) BgL_oz00_35);
						BgL_auxz00_3965 = BGL_OBJECT_WIDENING(BgL_tmpz00_3966);
					}
					BgL_auxz00_3964 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_3965);
				}
				return
					(((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3964))->
					BgL_celledzf3zf3);
			}
		}

	}



/* &sexit/Iinfo-celled? */
	obj_t BGl_z62sexitzf2Iinfozd2celledzf3zb1zzintegrate_infoz00(obj_t
		BgL_envz00_2808, obj_t BgL_oz00_2809)
	{
		{	/* Integrate/iinfo.sch 161 */
			return
				BBOOL(BGl_sexitzf2Iinfozd2celledzf3zd3zzintegrate_infoz00(
					((BgL_sexitz00_bglt) BgL_oz00_2809)));
		}

	}



/* sexit/Iinfo-celled?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Iinfozd2celledzf3zd2setz12z13zzintegrate_infoz00
		(BgL_sexitz00_bglt BgL_oz00_36, bool_t BgL_vz00_37)
	{
		{	/* Integrate/iinfo.sch 162 */
			{
				BgL_sexitzf2iinfozf2_bglt BgL_auxz00_3974;

				{
					obj_t BgL_auxz00_3975;

					{	/* Integrate/iinfo.sch 162 */
						BgL_objectz00_bglt BgL_tmpz00_3976;

						BgL_tmpz00_3976 = ((BgL_objectz00_bglt) BgL_oz00_36);
						BgL_auxz00_3975 = BGL_OBJECT_WIDENING(BgL_tmpz00_3976);
					}
					BgL_auxz00_3974 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_3975);
				}
				return
					((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3974))->
						BgL_celledzf3zf3) = ((bool_t) BgL_vz00_37), BUNSPEC);
			}
		}

	}



/* &sexit/Iinfo-celled?-set! */
	obj_t BGl_z62sexitzf2Iinfozd2celledzf3zd2setz12z71zzintegrate_infoz00(obj_t
		BgL_envz00_2810, obj_t BgL_oz00_2811, obj_t BgL_vz00_2812)
	{
		{	/* Integrate/iinfo.sch 162 */
			return
				BGl_sexitzf2Iinfozd2celledzf3zd2setz12z13zzintegrate_infoz00(
				((BgL_sexitz00_bglt) BgL_oz00_2811), CBOOL(BgL_vz00_2812));
		}

	}



/* sexit/Iinfo-kaptured? */
	BGL_EXPORTED_DEF bool_t
		BGl_sexitzf2Iinfozd2kapturedzf3zd3zzintegrate_infoz00(BgL_sexitz00_bglt
		BgL_oz00_38)
	{
		{	/* Integrate/iinfo.sch 163 */
			{
				BgL_sexitzf2iinfozf2_bglt BgL_auxz00_3984;

				{
					obj_t BgL_auxz00_3985;

					{	/* Integrate/iinfo.sch 163 */
						BgL_objectz00_bglt BgL_tmpz00_3986;

						BgL_tmpz00_3986 = ((BgL_objectz00_bglt) BgL_oz00_38);
						BgL_auxz00_3985 = BGL_OBJECT_WIDENING(BgL_tmpz00_3986);
					}
					BgL_auxz00_3984 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_3985);
				}
				return
					(((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3984))->
					BgL_kapturedzf3zf3);
			}
		}

	}



/* &sexit/Iinfo-kaptured? */
	obj_t BGl_z62sexitzf2Iinfozd2kapturedzf3zb1zzintegrate_infoz00(obj_t
		BgL_envz00_2813, obj_t BgL_oz00_2814)
	{
		{	/* Integrate/iinfo.sch 163 */
			return
				BBOOL(BGl_sexitzf2Iinfozd2kapturedzf3zd3zzintegrate_infoz00(
					((BgL_sexitz00_bglt) BgL_oz00_2814)));
		}

	}



/* sexit/Iinfo-kaptured?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Iinfozd2kapturedzf3zd2setz12z13zzintegrate_infoz00
		(BgL_sexitz00_bglt BgL_oz00_39, bool_t BgL_vz00_40)
	{
		{	/* Integrate/iinfo.sch 164 */
			{
				BgL_sexitzf2iinfozf2_bglt BgL_auxz00_3994;

				{
					obj_t BgL_auxz00_3995;

					{	/* Integrate/iinfo.sch 164 */
						BgL_objectz00_bglt BgL_tmpz00_3996;

						BgL_tmpz00_3996 = ((BgL_objectz00_bglt) BgL_oz00_39);
						BgL_auxz00_3995 = BGL_OBJECT_WIDENING(BgL_tmpz00_3996);
					}
					BgL_auxz00_3994 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_3995);
				}
				return
					((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3994))->
						BgL_kapturedzf3zf3) = ((bool_t) BgL_vz00_40), BUNSPEC);
			}
		}

	}



/* &sexit/Iinfo-kaptured?-set! */
	obj_t BGl_z62sexitzf2Iinfozd2kapturedzf3zd2setz12z71zzintegrate_infoz00(obj_t
		BgL_envz00_2815, obj_t BgL_oz00_2816, obj_t BgL_vz00_2817)
	{
		{	/* Integrate/iinfo.sch 164 */
			return
				BGl_sexitzf2Iinfozd2kapturedzf3zd2setz12z13zzintegrate_infoz00(
				((BgL_sexitz00_bglt) BgL_oz00_2816), CBOOL(BgL_vz00_2817));
		}

	}



/* sexit/Iinfo-u-mark */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Iinfozd2uzd2markzf2zzintegrate_infoz00(BgL_sexitz00_bglt
		BgL_oz00_41)
	{
		{	/* Integrate/iinfo.sch 165 */
			{
				BgL_sexitzf2iinfozf2_bglt BgL_auxz00_4004;

				{
					obj_t BgL_auxz00_4005;

					{	/* Integrate/iinfo.sch 165 */
						BgL_objectz00_bglt BgL_tmpz00_4006;

						BgL_tmpz00_4006 = ((BgL_objectz00_bglt) BgL_oz00_41);
						BgL_auxz00_4005 = BGL_OBJECT_WIDENING(BgL_tmpz00_4006);
					}
					BgL_auxz00_4004 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_4005);
				}
				return
					(((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4004))->
					BgL_uzd2markzd2);
			}
		}

	}



/* &sexit/Iinfo-u-mark */
	obj_t BGl_z62sexitzf2Iinfozd2uzd2markz90zzintegrate_infoz00(obj_t
		BgL_envz00_2818, obj_t BgL_oz00_2819)
	{
		{	/* Integrate/iinfo.sch 165 */
			return
				BGl_sexitzf2Iinfozd2uzd2markzf2zzintegrate_infoz00(
				((BgL_sexitz00_bglt) BgL_oz00_2819));
		}

	}



/* sexit/Iinfo-u-mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Iinfozd2uzd2markzd2setz12z32zzintegrate_infoz00
		(BgL_sexitz00_bglt BgL_oz00_42, obj_t BgL_vz00_43)
	{
		{	/* Integrate/iinfo.sch 166 */
			{
				BgL_sexitzf2iinfozf2_bglt BgL_auxz00_4013;

				{
					obj_t BgL_auxz00_4014;

					{	/* Integrate/iinfo.sch 166 */
						BgL_objectz00_bglt BgL_tmpz00_4015;

						BgL_tmpz00_4015 = ((BgL_objectz00_bglt) BgL_oz00_42);
						BgL_auxz00_4014 = BGL_OBJECT_WIDENING(BgL_tmpz00_4015);
					}
					BgL_auxz00_4013 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_4014);
				}
				return
					((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4013))->
						BgL_uzd2markzd2) = ((obj_t) BgL_vz00_43), BUNSPEC);
			}
		}

	}



/* &sexit/Iinfo-u-mark-set! */
	obj_t BGl_z62sexitzf2Iinfozd2uzd2markzd2setz12z50zzintegrate_infoz00(obj_t
		BgL_envz00_2820, obj_t BgL_oz00_2821, obj_t BgL_vz00_2822)
	{
		{	/* Integrate/iinfo.sch 166 */
			return
				BGl_sexitzf2Iinfozd2uzd2markzd2setz12z32zzintegrate_infoz00(
				((BgL_sexitz00_bglt) BgL_oz00_2821), BgL_vz00_2822);
		}

	}



/* sexit/Iinfo-f-mark */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Iinfozd2fzd2markzf2zzintegrate_infoz00(BgL_sexitz00_bglt
		BgL_oz00_44)
	{
		{	/* Integrate/iinfo.sch 167 */
			{
				BgL_sexitzf2iinfozf2_bglt BgL_auxz00_4022;

				{
					obj_t BgL_auxz00_4023;

					{	/* Integrate/iinfo.sch 167 */
						BgL_objectz00_bglt BgL_tmpz00_4024;

						BgL_tmpz00_4024 = ((BgL_objectz00_bglt) BgL_oz00_44);
						BgL_auxz00_4023 = BGL_OBJECT_WIDENING(BgL_tmpz00_4024);
					}
					BgL_auxz00_4022 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_4023);
				}
				return
					(((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4022))->
					BgL_fzd2markzd2);
			}
		}

	}



/* &sexit/Iinfo-f-mark */
	obj_t BGl_z62sexitzf2Iinfozd2fzd2markz90zzintegrate_infoz00(obj_t
		BgL_envz00_2823, obj_t BgL_oz00_2824)
	{
		{	/* Integrate/iinfo.sch 167 */
			return
				BGl_sexitzf2Iinfozd2fzd2markzf2zzintegrate_infoz00(
				((BgL_sexitz00_bglt) BgL_oz00_2824));
		}

	}



/* sexit/Iinfo-f-mark-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Iinfozd2fzd2markzd2setz12z32zzintegrate_infoz00
		(BgL_sexitz00_bglt BgL_oz00_45, obj_t BgL_vz00_46)
	{
		{	/* Integrate/iinfo.sch 168 */
			{
				BgL_sexitzf2iinfozf2_bglt BgL_auxz00_4031;

				{
					obj_t BgL_auxz00_4032;

					{	/* Integrate/iinfo.sch 168 */
						BgL_objectz00_bglt BgL_tmpz00_4033;

						BgL_tmpz00_4033 = ((BgL_objectz00_bglt) BgL_oz00_45);
						BgL_auxz00_4032 = BGL_OBJECT_WIDENING(BgL_tmpz00_4033);
					}
					BgL_auxz00_4031 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_4032);
				}
				return
					((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4031))->
						BgL_fzd2markzd2) = ((obj_t) BgL_vz00_46), BUNSPEC);
			}
		}

	}



/* &sexit/Iinfo-f-mark-set! */
	obj_t BGl_z62sexitzf2Iinfozd2fzd2markzd2setz12z50zzintegrate_infoz00(obj_t
		BgL_envz00_2825, obj_t BgL_oz00_2826, obj_t BgL_vz00_2827)
	{
		{	/* Integrate/iinfo.sch 168 */
			return
				BGl_sexitzf2Iinfozd2fzd2markzd2setz12z32zzintegrate_infoz00(
				((BgL_sexitz00_bglt) BgL_oz00_2826), BgL_vz00_2827);
		}

	}



/* sexit/Iinfo-detached? */
	BGL_EXPORTED_DEF bool_t
		BGl_sexitzf2Iinfozd2detachedzf3zd3zzintegrate_infoz00(BgL_sexitz00_bglt
		BgL_oz00_47)
	{
		{	/* Integrate/iinfo.sch 169 */
			return
				(((BgL_sexitz00_bglt) COBJECT(
						((BgL_sexitz00_bglt) BgL_oz00_47)))->BgL_detachedzf3zf3);
		}

	}



/* &sexit/Iinfo-detached? */
	obj_t BGl_z62sexitzf2Iinfozd2detachedzf3zb1zzintegrate_infoz00(obj_t
		BgL_envz00_2828, obj_t BgL_oz00_2829)
	{
		{	/* Integrate/iinfo.sch 169 */
			return
				BBOOL(BGl_sexitzf2Iinfozd2detachedzf3zd3zzintegrate_infoz00(
					((BgL_sexitz00_bglt) BgL_oz00_2829)));
		}

	}



/* sexit/Iinfo-detached?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Iinfozd2detachedzf3zd2setz12z13zzintegrate_infoz00
		(BgL_sexitz00_bglt BgL_oz00_48, bool_t BgL_vz00_49)
	{
		{	/* Integrate/iinfo.sch 170 */
			return
				((((BgL_sexitz00_bglt) COBJECT(
							((BgL_sexitz00_bglt) BgL_oz00_48)))->BgL_detachedzf3zf3) =
				((bool_t) BgL_vz00_49), BUNSPEC);
		}

	}



/* &sexit/Iinfo-detached?-set! */
	obj_t BGl_z62sexitzf2Iinfozd2detachedzf3zd2setz12z71zzintegrate_infoz00(obj_t
		BgL_envz00_2830, obj_t BgL_oz00_2831, obj_t BgL_vz00_2832)
	{
		{	/* Integrate/iinfo.sch 170 */
			return
				BGl_sexitzf2Iinfozd2detachedzf3zd2setz12z13zzintegrate_infoz00(
				((BgL_sexitz00_bglt) BgL_oz00_2831), CBOOL(BgL_vz00_2832));
		}

	}



/* sexit/Iinfo-handler */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Iinfozd2handlerz20zzintegrate_infoz00(BgL_sexitz00_bglt
		BgL_oz00_50)
	{
		{	/* Integrate/iinfo.sch 171 */
			return
				(((BgL_sexitz00_bglt) COBJECT(
						((BgL_sexitz00_bglt) BgL_oz00_50)))->BgL_handlerz00);
		}

	}



/* &sexit/Iinfo-handler */
	obj_t BGl_z62sexitzf2Iinfozd2handlerz42zzintegrate_infoz00(obj_t
		BgL_envz00_2833, obj_t BgL_oz00_2834)
	{
		{	/* Integrate/iinfo.sch 171 */
			return
				BGl_sexitzf2Iinfozd2handlerz20zzintegrate_infoz00(
				((BgL_sexitz00_bglt) BgL_oz00_2834));
		}

	}



/* sexit/Iinfo-handler-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sexitzf2Iinfozd2handlerzd2setz12ze0zzintegrate_infoz00(BgL_sexitz00_bglt
		BgL_oz00_51, obj_t BgL_vz00_52)
	{
		{	/* Integrate/iinfo.sch 172 */
			return
				((((BgL_sexitz00_bglt) COBJECT(
							((BgL_sexitz00_bglt) BgL_oz00_51)))->BgL_handlerz00) =
				((obj_t) BgL_vz00_52), BUNSPEC);
		}

	}



/* &sexit/Iinfo-handler-set! */
	obj_t BGl_z62sexitzf2Iinfozd2handlerzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2835, obj_t BgL_oz00_2836, obj_t BgL_vz00_2837)
	{
		{	/* Integrate/iinfo.sch 172 */
			return
				BGl_sexitzf2Iinfozd2handlerzd2setz12ze0zzintegrate_infoz00(
				((BgL_sexitz00_bglt) BgL_oz00_2836), BgL_vz00_2837);
		}

	}



/* make-sfun/Iinfo */
	BGL_EXPORTED_DEF BgL_sfunz00_bglt
		BGl_makezd2sfunzf2Iinfoz20zzintegrate_infoz00(long BgL_arity1176z00_53,
		obj_t BgL_sidezd2effect1177zd2_54, obj_t BgL_predicatezd2of1178zd2_55,
		obj_t BgL_stackzd2allocator1179zd2_56, bool_t BgL_topzf31180zf3_57,
		obj_t BgL_thezd2closure1181zd2_58, obj_t BgL_effect1182z00_59,
		obj_t BgL_failsafe1183z00_60, obj_t BgL_argszd2noescape1184zd2_61,
		obj_t BgL_argszd2retescape1185zd2_62, obj_t BgL_property1186z00_63,
		obj_t BgL_args1187z00_64, obj_t BgL_argszd2name1188zd2_65,
		obj_t BgL_body1189z00_66, obj_t BgL_class1190z00_67,
		obj_t BgL_dssslzd2keywords1191zd2_68, obj_t BgL_loc1192z00_69,
		obj_t BgL_optionals1193z00_70, obj_t BgL_keys1194z00_71,
		obj_t BgL_thezd2closurezd2global1195z00_72, obj_t BgL_strength1196z00_73,
		obj_t BgL_stackable1197z00_74, obj_t BgL_owner1198z00_75,
		obj_t BgL_free1199z00_76, obj_t BgL_bound1200z00_77,
		obj_t BgL_cfrom1201z00_78, obj_t BgL_cto1202z00_79, obj_t BgL_k1203z00_80,
		obj_t BgL_kza21204za2_81, obj_t BgL_u1205z00_82, obj_t BgL_cn1206z00_83,
		obj_t BgL_ct1207z00_84, obj_t BgL_kont1208z00_85, bool_t BgL_gzf31209zf3_86,
		bool_t BgL_forcegzf31210zf3_87, obj_t BgL_l1211z00_88,
		obj_t BgL_led1212z00_89, obj_t BgL_istamp1213z00_90,
		obj_t BgL_global1214z00_91, obj_t BgL_kaptured1215z00_92,
		obj_t BgL_tailzd2coercion1216zd2_93, bool_t BgL_xhdlzf31217zf3_94,
		obj_t BgL_xhdls1218z00_95)
	{
		{	/* Integrate/iinfo.sch 175 */
			{	/* Integrate/iinfo.sch 175 */
				BgL_sfunz00_bglt BgL_new1187z00_3495;

				{	/* Integrate/iinfo.sch 175 */
					BgL_sfunz00_bglt BgL_tmp1185z00_3496;
					BgL_sfunzf2iinfozf2_bglt BgL_wide1186z00_3497;

					{
						BgL_sfunz00_bglt BgL_auxz00_4058;

						{	/* Integrate/iinfo.sch 175 */
							BgL_sfunz00_bglt BgL_new1184z00_3498;

							BgL_new1184z00_3498 =
								((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_sfunz00_bgl))));
							{	/* Integrate/iinfo.sch 175 */
								long BgL_arg1340z00_3499;

								BgL_arg1340z00_3499 = BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1184z00_3498),
									BgL_arg1340z00_3499);
							}
							{	/* Integrate/iinfo.sch 175 */
								BgL_objectz00_bglt BgL_tmpz00_4063;

								BgL_tmpz00_4063 = ((BgL_objectz00_bglt) BgL_new1184z00_3498);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4063, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1184z00_3498);
							BgL_auxz00_4058 = BgL_new1184z00_3498;
						}
						BgL_tmp1185z00_3496 = ((BgL_sfunz00_bglt) BgL_auxz00_4058);
					}
					BgL_wide1186z00_3497 =
						((BgL_sfunzf2iinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sfunzf2iinfozf2_bgl))));
					{	/* Integrate/iinfo.sch 175 */
						obj_t BgL_auxz00_4071;
						BgL_objectz00_bglt BgL_tmpz00_4069;

						BgL_auxz00_4071 = ((obj_t) BgL_wide1186z00_3497);
						BgL_tmpz00_4069 = ((BgL_objectz00_bglt) BgL_tmp1185z00_3496);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4069, BgL_auxz00_4071);
					}
					((BgL_objectz00_bglt) BgL_tmp1185z00_3496);
					{	/* Integrate/iinfo.sch 175 */
						long BgL_arg1339z00_3500;

						BgL_arg1339z00_3500 =
							BGL_CLASS_NUM(BGl_sfunzf2Iinfozf2zzintegrate_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_tmp1185z00_3496), BgL_arg1339z00_3500);
					}
					BgL_new1187z00_3495 = ((BgL_sfunz00_bglt) BgL_tmp1185z00_3496);
				}
				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_new1187z00_3495)))->BgL_arityz00) =
					((long) BgL_arity1176z00_53), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1187z00_3495)))->
						BgL_sidezd2effectzd2) =
					((obj_t) BgL_sidezd2effect1177zd2_54), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1187z00_3495)))->
						BgL_predicatezd2ofzd2) =
					((obj_t) BgL_predicatezd2of1178zd2_55), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1187z00_3495)))->
						BgL_stackzd2allocatorzd2) =
					((obj_t) BgL_stackzd2allocator1179zd2_56), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1187z00_3495)))->
						BgL_topzf3zf3) = ((bool_t) BgL_topzf31180zf3_57), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1187z00_3495)))->
						BgL_thezd2closurezd2) =
					((obj_t) BgL_thezd2closure1181zd2_58), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1187z00_3495)))->
						BgL_effectz00) = ((obj_t) BgL_effect1182z00_59), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1187z00_3495)))->
						BgL_failsafez00) = ((obj_t) BgL_failsafe1183z00_60), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1187z00_3495)))->
						BgL_argszd2noescapezd2) =
					((obj_t) BgL_argszd2noescape1184zd2_61), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) BgL_new1187z00_3495)))->
						BgL_argszd2retescapezd2) =
					((obj_t) BgL_argszd2retescape1185zd2_62), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1187z00_3495)))->BgL_propertyz00) =
					((obj_t) BgL_property1186z00_63), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1187z00_3495)))->BgL_argsz00) =
					((obj_t) BgL_args1187z00_64), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1187z00_3495)))->BgL_argszd2namezd2) =
					((obj_t) BgL_argszd2name1188zd2_65), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1187z00_3495)))->BgL_bodyz00) =
					((obj_t) BgL_body1189z00_66), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1187z00_3495)))->BgL_classz00) =
					((obj_t) BgL_class1190z00_67), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1187z00_3495)))->BgL_dssslzd2keywordszd2) =
					((obj_t) BgL_dssslzd2keywords1191zd2_68), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1187z00_3495)))->BgL_locz00) =
					((obj_t) BgL_loc1192z00_69), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1187z00_3495)))->BgL_optionalsz00) =
					((obj_t) BgL_optionals1193z00_70), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1187z00_3495)))->BgL_keysz00) =
					((obj_t) BgL_keys1194z00_71), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1187z00_3495)))->BgL_thezd2closurezd2globalz00) =
					((obj_t) BgL_thezd2closurezd2global1195z00_72), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1187z00_3495)))->BgL_strengthz00) =
					((obj_t) BgL_strength1196z00_73), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
									BgL_new1187z00_3495)))->BgL_stackablez00) =
					((obj_t) BgL_stackable1197z00_74), BUNSPEC);
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4123;

					{
						obj_t BgL_auxz00_4124;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4125;

							BgL_tmpz00_4125 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4124 = BGL_OBJECT_WIDENING(BgL_tmpz00_4125);
						}
						BgL_auxz00_4123 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4124);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4123))->
							BgL_ownerz00) = ((obj_t) BgL_owner1198z00_75), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4130;

					{
						obj_t BgL_auxz00_4131;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4132;

							BgL_tmpz00_4132 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4131 = BGL_OBJECT_WIDENING(BgL_tmpz00_4132);
						}
						BgL_auxz00_4130 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4131);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4130))->
							BgL_freez00) = ((obj_t) BgL_free1199z00_76), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4137;

					{
						obj_t BgL_auxz00_4138;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4139;

							BgL_tmpz00_4139 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4138 = BGL_OBJECT_WIDENING(BgL_tmpz00_4139);
						}
						BgL_auxz00_4137 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4138);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4137))->
							BgL_boundz00) = ((obj_t) BgL_bound1200z00_77), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4144;

					{
						obj_t BgL_auxz00_4145;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4146;

							BgL_tmpz00_4146 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4145 = BGL_OBJECT_WIDENING(BgL_tmpz00_4146);
						}
						BgL_auxz00_4144 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4145);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4144))->
							BgL_cfromz00) = ((obj_t) BgL_cfrom1201z00_78), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4151;

					{
						obj_t BgL_auxz00_4152;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4153;

							BgL_tmpz00_4153 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4152 = BGL_OBJECT_WIDENING(BgL_tmpz00_4153);
						}
						BgL_auxz00_4151 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4152);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4151))->BgL_ctoz00) =
						((obj_t) BgL_cto1202z00_79), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4158;

					{
						obj_t BgL_auxz00_4159;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4160;

							BgL_tmpz00_4160 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4159 = BGL_OBJECT_WIDENING(BgL_tmpz00_4160);
						}
						BgL_auxz00_4158 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4159);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4158))->BgL_kz00) =
						((obj_t) BgL_k1203z00_80), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4165;

					{
						obj_t BgL_auxz00_4166;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4167;

							BgL_tmpz00_4167 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4166 = BGL_OBJECT_WIDENING(BgL_tmpz00_4167);
						}
						BgL_auxz00_4165 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4166);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4165))->
							BgL_kza2za2) = ((obj_t) BgL_kza21204za2_81), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4172;

					{
						obj_t BgL_auxz00_4173;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4174;

							BgL_tmpz00_4174 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4173 = BGL_OBJECT_WIDENING(BgL_tmpz00_4174);
						}
						BgL_auxz00_4172 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4173);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4172))->BgL_uz00) =
						((obj_t) BgL_u1205z00_82), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4179;

					{
						obj_t BgL_auxz00_4180;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4181;

							BgL_tmpz00_4181 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4180 = BGL_OBJECT_WIDENING(BgL_tmpz00_4181);
						}
						BgL_auxz00_4179 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4180);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4179))->BgL_cnz00) =
						((obj_t) BgL_cn1206z00_83), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4186;

					{
						obj_t BgL_auxz00_4187;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4188;

							BgL_tmpz00_4188 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4187 = BGL_OBJECT_WIDENING(BgL_tmpz00_4188);
						}
						BgL_auxz00_4186 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4187);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4186))->BgL_ctz00) =
						((obj_t) BgL_ct1207z00_84), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4193;

					{
						obj_t BgL_auxz00_4194;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4195;

							BgL_tmpz00_4195 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4194 = BGL_OBJECT_WIDENING(BgL_tmpz00_4195);
						}
						BgL_auxz00_4193 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4194);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4193))->
							BgL_kontz00) = ((obj_t) BgL_kont1208z00_85), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4200;

					{
						obj_t BgL_auxz00_4201;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4202;

							BgL_tmpz00_4202 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4201 = BGL_OBJECT_WIDENING(BgL_tmpz00_4202);
						}
						BgL_auxz00_4200 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4201);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4200))->
							BgL_gzf3zf3) = ((bool_t) BgL_gzf31209zf3_86), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4207;

					{
						obj_t BgL_auxz00_4208;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4209;

							BgL_tmpz00_4209 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4208 = BGL_OBJECT_WIDENING(BgL_tmpz00_4209);
						}
						BgL_auxz00_4207 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4208);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4207))->
							BgL_forcegzf3zf3) = ((bool_t) BgL_forcegzf31210zf3_87), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4214;

					{
						obj_t BgL_auxz00_4215;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4216;

							BgL_tmpz00_4216 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4215 = BGL_OBJECT_WIDENING(BgL_tmpz00_4216);
						}
						BgL_auxz00_4214 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4215);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4214))->BgL_lz00) =
						((obj_t) BgL_l1211z00_88), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4221;

					{
						obj_t BgL_auxz00_4222;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4223;

							BgL_tmpz00_4223 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4222 = BGL_OBJECT_WIDENING(BgL_tmpz00_4223);
						}
						BgL_auxz00_4221 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4222);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4221))->BgL_ledz00) =
						((obj_t) BgL_led1212z00_89), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4228;

					{
						obj_t BgL_auxz00_4229;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4230;

							BgL_tmpz00_4230 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4229 = BGL_OBJECT_WIDENING(BgL_tmpz00_4230);
						}
						BgL_auxz00_4228 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4229);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4228))->
							BgL_istampz00) = ((obj_t) BgL_istamp1213z00_90), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4235;

					{
						obj_t BgL_auxz00_4236;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4237;

							BgL_tmpz00_4237 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4236 = BGL_OBJECT_WIDENING(BgL_tmpz00_4237);
						}
						BgL_auxz00_4235 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4236);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4235))->
							BgL_globalz00) = ((obj_t) BgL_global1214z00_91), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4242;

					{
						obj_t BgL_auxz00_4243;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4244;

							BgL_tmpz00_4244 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4243 = BGL_OBJECT_WIDENING(BgL_tmpz00_4244);
						}
						BgL_auxz00_4242 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4243);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4242))->
							BgL_kapturedz00) = ((obj_t) BgL_kaptured1215z00_92), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4249;

					{
						obj_t BgL_auxz00_4250;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4251;

							BgL_tmpz00_4251 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4250 = BGL_OBJECT_WIDENING(BgL_tmpz00_4251);
						}
						BgL_auxz00_4249 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4250);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4249))->
							BgL_tailzd2coercionzd2) =
						((obj_t) BgL_tailzd2coercion1216zd2_93), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4256;

					{
						obj_t BgL_auxz00_4257;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4258;

							BgL_tmpz00_4258 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4257 = BGL_OBJECT_WIDENING(BgL_tmpz00_4258);
						}
						BgL_auxz00_4256 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4257);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4256))->
							BgL_xhdlzf3zf3) = ((bool_t) BgL_xhdlzf31217zf3_94), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4263;

					{
						obj_t BgL_auxz00_4264;

						{	/* Integrate/iinfo.sch 175 */
							BgL_objectz00_bglt BgL_tmpz00_4265;

							BgL_tmpz00_4265 = ((BgL_objectz00_bglt) BgL_new1187z00_3495);
							BgL_auxz00_4264 = BGL_OBJECT_WIDENING(BgL_tmpz00_4265);
						}
						BgL_auxz00_4263 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4264);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4263))->
							BgL_xhdlsz00) = ((obj_t) BgL_xhdls1218z00_95), BUNSPEC);
				}
				return BgL_new1187z00_3495;
			}
		}

	}



/* &make-sfun/Iinfo */
	BgL_sfunz00_bglt BGl_z62makezd2sfunzf2Iinfoz42zzintegrate_infoz00(obj_t
		BgL_envz00_2838, obj_t BgL_arity1176z00_2839,
		obj_t BgL_sidezd2effect1177zd2_2840, obj_t BgL_predicatezd2of1178zd2_2841,
		obj_t BgL_stackzd2allocator1179zd2_2842, obj_t BgL_topzf31180zf3_2843,
		obj_t BgL_thezd2closure1181zd2_2844, obj_t BgL_effect1182z00_2845,
		obj_t BgL_failsafe1183z00_2846, obj_t BgL_argszd2noescape1184zd2_2847,
		obj_t BgL_argszd2retescape1185zd2_2848, obj_t BgL_property1186z00_2849,
		obj_t BgL_args1187z00_2850, obj_t BgL_argszd2name1188zd2_2851,
		obj_t BgL_body1189z00_2852, obj_t BgL_class1190z00_2853,
		obj_t BgL_dssslzd2keywords1191zd2_2854, obj_t BgL_loc1192z00_2855,
		obj_t BgL_optionals1193z00_2856, obj_t BgL_keys1194z00_2857,
		obj_t BgL_thezd2closurezd2global1195z00_2858,
		obj_t BgL_strength1196z00_2859, obj_t BgL_stackable1197z00_2860,
		obj_t BgL_owner1198z00_2861, obj_t BgL_free1199z00_2862,
		obj_t BgL_bound1200z00_2863, obj_t BgL_cfrom1201z00_2864,
		obj_t BgL_cto1202z00_2865, obj_t BgL_k1203z00_2866,
		obj_t BgL_kza21204za2_2867, obj_t BgL_u1205z00_2868,
		obj_t BgL_cn1206z00_2869, obj_t BgL_ct1207z00_2870,
		obj_t BgL_kont1208z00_2871, obj_t BgL_gzf31209zf3_2872,
		obj_t BgL_forcegzf31210zf3_2873, obj_t BgL_l1211z00_2874,
		obj_t BgL_led1212z00_2875, obj_t BgL_istamp1213z00_2876,
		obj_t BgL_global1214z00_2877, obj_t BgL_kaptured1215z00_2878,
		obj_t BgL_tailzd2coercion1216zd2_2879, obj_t BgL_xhdlzf31217zf3_2880,
		obj_t BgL_xhdls1218z00_2881)
	{
		{	/* Integrate/iinfo.sch 175 */
			return
				BGl_makezd2sfunzf2Iinfoz20zzintegrate_infoz00(
				(long) CINT(BgL_arity1176z00_2839), BgL_sidezd2effect1177zd2_2840,
				BgL_predicatezd2of1178zd2_2841, BgL_stackzd2allocator1179zd2_2842,
				CBOOL(BgL_topzf31180zf3_2843), BgL_thezd2closure1181zd2_2844,
				BgL_effect1182z00_2845, BgL_failsafe1183z00_2846,
				BgL_argszd2noescape1184zd2_2847, BgL_argszd2retescape1185zd2_2848,
				BgL_property1186z00_2849, BgL_args1187z00_2850,
				BgL_argszd2name1188zd2_2851, BgL_body1189z00_2852,
				BgL_class1190z00_2853, BgL_dssslzd2keywords1191zd2_2854,
				BgL_loc1192z00_2855, BgL_optionals1193z00_2856, BgL_keys1194z00_2857,
				BgL_thezd2closurezd2global1195z00_2858, BgL_strength1196z00_2859,
				BgL_stackable1197z00_2860, BgL_owner1198z00_2861, BgL_free1199z00_2862,
				BgL_bound1200z00_2863, BgL_cfrom1201z00_2864, BgL_cto1202z00_2865,
				BgL_k1203z00_2866, BgL_kza21204za2_2867, BgL_u1205z00_2868,
				BgL_cn1206z00_2869, BgL_ct1207z00_2870, BgL_kont1208z00_2871,
				CBOOL(BgL_gzf31209zf3_2872), CBOOL(BgL_forcegzf31210zf3_2873),
				BgL_l1211z00_2874, BgL_led1212z00_2875, BgL_istamp1213z00_2876,
				BgL_global1214z00_2877, BgL_kaptured1215z00_2878,
				BgL_tailzd2coercion1216zd2_2879, CBOOL(BgL_xhdlzf31217zf3_2880),
				BgL_xhdls1218z00_2881);
		}

	}



/* sfun/Iinfo? */
	BGL_EXPORTED_DEF bool_t BGl_sfunzf2Iinfozf3z01zzintegrate_infoz00(obj_t
		BgL_objz00_96)
	{
		{	/* Integrate/iinfo.sch 176 */
			{	/* Integrate/iinfo.sch 176 */
				obj_t BgL_classz00_3501;

				BgL_classz00_3501 = BGl_sfunzf2Iinfozf2zzintegrate_infoz00;
				if (BGL_OBJECTP(BgL_objz00_96))
					{	/* Integrate/iinfo.sch 176 */
						BgL_objectz00_bglt BgL_arg1807z00_3502;

						BgL_arg1807z00_3502 = (BgL_objectz00_bglt) (BgL_objz00_96);
						if (BGL_CONDEXPAND_ISA_ARCH64())
							{	/* Integrate/iinfo.sch 176 */
								long BgL_idxz00_3503;

								BgL_idxz00_3503 =
									BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3502);
								return
									(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
										(BgL_idxz00_3503 + 4L)) == BgL_classz00_3501);
							}
						else
							{	/* Integrate/iinfo.sch 176 */
								bool_t BgL_res1974z00_3506;

								{	/* Integrate/iinfo.sch 176 */
									obj_t BgL_oclassz00_3507;

									{	/* Integrate/iinfo.sch 176 */
										obj_t BgL_arg1815z00_3508;
										long BgL_arg1816z00_3509;

										BgL_arg1815z00_3508 = (BGl_za2classesza2z00zz__objectz00);
										{	/* Integrate/iinfo.sch 176 */
											long BgL_arg1817z00_3510;

											BgL_arg1817z00_3510 =
												BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3502);
											BgL_arg1816z00_3509 = (BgL_arg1817z00_3510 - OBJECT_TYPE);
										}
										BgL_oclassz00_3507 =
											VECTOR_REF(BgL_arg1815z00_3508, BgL_arg1816z00_3509);
									}
									{	/* Integrate/iinfo.sch 176 */
										bool_t BgL__ortest_1115z00_3511;

										BgL__ortest_1115z00_3511 =
											(BgL_classz00_3501 == BgL_oclassz00_3507);
										if (BgL__ortest_1115z00_3511)
											{	/* Integrate/iinfo.sch 176 */
												BgL_res1974z00_3506 = BgL__ortest_1115z00_3511;
											}
										else
											{	/* Integrate/iinfo.sch 176 */
												long BgL_odepthz00_3512;

												{	/* Integrate/iinfo.sch 176 */
													obj_t BgL_arg1804z00_3513;

													BgL_arg1804z00_3513 = (BgL_oclassz00_3507);
													BgL_odepthz00_3512 =
														BGL_CLASS_DEPTH(BgL_arg1804z00_3513);
												}
												if ((4L < BgL_odepthz00_3512))
													{	/* Integrate/iinfo.sch 176 */
														obj_t BgL_arg1802z00_3514;

														{	/* Integrate/iinfo.sch 176 */
															obj_t BgL_arg1803z00_3515;

															BgL_arg1803z00_3515 = (BgL_oclassz00_3507);
															BgL_arg1802z00_3514 =
																BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3515,
																4L);
														}
														BgL_res1974z00_3506 =
															(BgL_arg1802z00_3514 == BgL_classz00_3501);
													}
												else
													{	/* Integrate/iinfo.sch 176 */
														BgL_res1974z00_3506 = ((bool_t) 0);
													}
											}
									}
								}
								return BgL_res1974z00_3506;
							}
					}
				else
					{	/* Integrate/iinfo.sch 176 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &sfun/Iinfo? */
	obj_t BGl_z62sfunzf2Iinfozf3z63zzintegrate_infoz00(obj_t BgL_envz00_2882,
		obj_t BgL_objz00_2883)
	{
		{	/* Integrate/iinfo.sch 176 */
			return BBOOL(BGl_sfunzf2Iinfozf3z01zzintegrate_infoz00(BgL_objz00_2883));
		}

	}



/* sfun/Iinfo-nil */
	BGL_EXPORTED_DEF BgL_sfunz00_bglt
		BGl_sfunzf2Iinfozd2nilz20zzintegrate_infoz00(void)
	{
		{	/* Integrate/iinfo.sch 177 */
			{	/* Integrate/iinfo.sch 177 */
				obj_t BgL_classz00_2483;

				BgL_classz00_2483 = BGl_sfunzf2Iinfozf2zzintegrate_infoz00;
				{	/* Integrate/iinfo.sch 177 */
					obj_t BgL__ortest_1117z00_2484;

					BgL__ortest_1117z00_2484 = BGL_CLASS_NIL(BgL_classz00_2483);
					if (CBOOL(BgL__ortest_1117z00_2484))
						{	/* Integrate/iinfo.sch 177 */
							return ((BgL_sfunz00_bglt) BgL__ortest_1117z00_2484);
						}
					else
						{	/* Integrate/iinfo.sch 177 */
							return
								((BgL_sfunz00_bglt)
								BGl_classzd2nilzd2initz12z12zz__objectz00(BgL_classz00_2483));
						}
				}
			}
		}

	}



/* &sfun/Iinfo-nil */
	BgL_sfunz00_bglt BGl_z62sfunzf2Iinfozd2nilz42zzintegrate_infoz00(obj_t
		BgL_envz00_2884)
	{
		{	/* Integrate/iinfo.sch 177 */
			return BGl_sfunzf2Iinfozd2nilz20zzintegrate_infoz00();
		}

	}



/* sfun/Iinfo-xhdls */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2xhdlsz20zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_97)
	{
		{	/* Integrate/iinfo.sch 178 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4307;

				{
					obj_t BgL_auxz00_4308;

					{	/* Integrate/iinfo.sch 178 */
						BgL_objectz00_bglt BgL_tmpz00_4309;

						BgL_tmpz00_4309 = ((BgL_objectz00_bglt) BgL_oz00_97);
						BgL_auxz00_4308 = BGL_OBJECT_WIDENING(BgL_tmpz00_4309);
					}
					BgL_auxz00_4307 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4308);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4307))->BgL_xhdlsz00);
			}
		}

	}



/* &sfun/Iinfo-xhdls */
	obj_t BGl_z62sfunzf2Iinfozd2xhdlsz42zzintegrate_infoz00(obj_t BgL_envz00_2885,
		obj_t BgL_oz00_2886)
	{
		{	/* Integrate/iinfo.sch 178 */
			return
				BGl_sfunzf2Iinfozd2xhdlsz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2886));
		}

	}



/* sfun/Iinfo-xhdls-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2xhdlszd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_98, obj_t BgL_vz00_99)
	{
		{	/* Integrate/iinfo.sch 179 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4316;

				{
					obj_t BgL_auxz00_4317;

					{	/* Integrate/iinfo.sch 179 */
						BgL_objectz00_bglt BgL_tmpz00_4318;

						BgL_tmpz00_4318 = ((BgL_objectz00_bglt) BgL_oz00_98);
						BgL_auxz00_4317 = BGL_OBJECT_WIDENING(BgL_tmpz00_4318);
					}
					BgL_auxz00_4316 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4317);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4316))->
						BgL_xhdlsz00) = ((obj_t) BgL_vz00_99), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-xhdls-set! */
	obj_t BGl_z62sfunzf2Iinfozd2xhdlszd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2887, obj_t BgL_oz00_2888, obj_t BgL_vz00_2889)
	{
		{	/* Integrate/iinfo.sch 179 */
			return
				BGl_sfunzf2Iinfozd2xhdlszd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2888), BgL_vz00_2889);
		}

	}



/* sfun/Iinfo-xhdl? */
	BGL_EXPORTED_DEF bool_t
		BGl_sfunzf2Iinfozd2xhdlzf3zd3zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_100)
	{
		{	/* Integrate/iinfo.sch 180 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4325;

				{
					obj_t BgL_auxz00_4326;

					{	/* Integrate/iinfo.sch 180 */
						BgL_objectz00_bglt BgL_tmpz00_4327;

						BgL_tmpz00_4327 = ((BgL_objectz00_bglt) BgL_oz00_100);
						BgL_auxz00_4326 = BGL_OBJECT_WIDENING(BgL_tmpz00_4327);
					}
					BgL_auxz00_4325 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4326);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4325))->
					BgL_xhdlzf3zf3);
			}
		}

	}



/* &sfun/Iinfo-xhdl? */
	obj_t BGl_z62sfunzf2Iinfozd2xhdlzf3zb1zzintegrate_infoz00(obj_t
		BgL_envz00_2890, obj_t BgL_oz00_2891)
	{
		{	/* Integrate/iinfo.sch 180 */
			return
				BBOOL(BGl_sfunzf2Iinfozd2xhdlzf3zd3zzintegrate_infoz00(
					((BgL_sfunz00_bglt) BgL_oz00_2891)));
		}

	}



/* sfun/Iinfo-xhdl?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2xhdlzf3zd2setz12z13zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_101, bool_t BgL_vz00_102)
	{
		{	/* Integrate/iinfo.sch 181 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4335;

				{
					obj_t BgL_auxz00_4336;

					{	/* Integrate/iinfo.sch 181 */
						BgL_objectz00_bglt BgL_tmpz00_4337;

						BgL_tmpz00_4337 = ((BgL_objectz00_bglt) BgL_oz00_101);
						BgL_auxz00_4336 = BGL_OBJECT_WIDENING(BgL_tmpz00_4337);
					}
					BgL_auxz00_4335 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4336);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4335))->
						BgL_xhdlzf3zf3) = ((bool_t) BgL_vz00_102), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-xhdl?-set! */
	obj_t BGl_z62sfunzf2Iinfozd2xhdlzf3zd2setz12z71zzintegrate_infoz00(obj_t
		BgL_envz00_2892, obj_t BgL_oz00_2893, obj_t BgL_vz00_2894)
	{
		{	/* Integrate/iinfo.sch 181 */
			return
				BGl_sfunzf2Iinfozd2xhdlzf3zd2setz12z13zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2893), CBOOL(BgL_vz00_2894));
		}

	}



/* sfun/Iinfo-tail-coercion */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2tailzd2coercionzf2zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_103)
	{
		{	/* Integrate/iinfo.sch 182 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4345;

				{
					obj_t BgL_auxz00_4346;

					{	/* Integrate/iinfo.sch 182 */
						BgL_objectz00_bglt BgL_tmpz00_4347;

						BgL_tmpz00_4347 = ((BgL_objectz00_bglt) BgL_oz00_103);
						BgL_auxz00_4346 = BGL_OBJECT_WIDENING(BgL_tmpz00_4347);
					}
					BgL_auxz00_4345 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4346);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4345))->
					BgL_tailzd2coercionzd2);
			}
		}

	}



/* &sfun/Iinfo-tail-coercion */
	obj_t BGl_z62sfunzf2Iinfozd2tailzd2coercionz90zzintegrate_infoz00(obj_t
		BgL_envz00_2895, obj_t BgL_oz00_2896)
	{
		{	/* Integrate/iinfo.sch 182 */
			return
				BGl_sfunzf2Iinfozd2tailzd2coercionzf2zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2896));
		}

	}



/* sfun/Iinfo-tail-coercion-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2tailzd2coercionzd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt BgL_oz00_104, obj_t BgL_vz00_105)
	{
		{	/* Integrate/iinfo.sch 183 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4354;

				{
					obj_t BgL_auxz00_4355;

					{	/* Integrate/iinfo.sch 183 */
						BgL_objectz00_bglt BgL_tmpz00_4356;

						BgL_tmpz00_4356 = ((BgL_objectz00_bglt) BgL_oz00_104);
						BgL_auxz00_4355 = BGL_OBJECT_WIDENING(BgL_tmpz00_4356);
					}
					BgL_auxz00_4354 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4355);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4354))->
						BgL_tailzd2coercionzd2) = ((obj_t) BgL_vz00_105), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-tail-coercion-set! */
	obj_t
		BGl_z62sfunzf2Iinfozd2tailzd2coercionzd2setz12z50zzintegrate_infoz00(obj_t
		BgL_envz00_2897, obj_t BgL_oz00_2898, obj_t BgL_vz00_2899)
	{
		{	/* Integrate/iinfo.sch 183 */
			return
				BGl_sfunzf2Iinfozd2tailzd2coercionzd2setz12z32zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2898), BgL_vz00_2899);
		}

	}



/* sfun/Iinfo-kaptured */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2kapturedz20zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_106)
	{
		{	/* Integrate/iinfo.sch 184 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4363;

				{
					obj_t BgL_auxz00_4364;

					{	/* Integrate/iinfo.sch 184 */
						BgL_objectz00_bglt BgL_tmpz00_4365;

						BgL_tmpz00_4365 = ((BgL_objectz00_bglt) BgL_oz00_106);
						BgL_auxz00_4364 = BGL_OBJECT_WIDENING(BgL_tmpz00_4365);
					}
					BgL_auxz00_4363 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4364);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4363))->
					BgL_kapturedz00);
			}
		}

	}



/* &sfun/Iinfo-kaptured */
	obj_t BGl_z62sfunzf2Iinfozd2kapturedz42zzintegrate_infoz00(obj_t
		BgL_envz00_2900, obj_t BgL_oz00_2901)
	{
		{	/* Integrate/iinfo.sch 184 */
			return
				BGl_sfunzf2Iinfozd2kapturedz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2901));
		}

	}



/* sfun/Iinfo-kaptured-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2kapturedzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_107, obj_t BgL_vz00_108)
	{
		{	/* Integrate/iinfo.sch 185 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4372;

				{
					obj_t BgL_auxz00_4373;

					{	/* Integrate/iinfo.sch 185 */
						BgL_objectz00_bglt BgL_tmpz00_4374;

						BgL_tmpz00_4374 = ((BgL_objectz00_bglt) BgL_oz00_107);
						BgL_auxz00_4373 = BGL_OBJECT_WIDENING(BgL_tmpz00_4374);
					}
					BgL_auxz00_4372 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4373);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4372))->
						BgL_kapturedz00) = ((obj_t) BgL_vz00_108), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-kaptured-set! */
	obj_t BGl_z62sfunzf2Iinfozd2kapturedzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2902, obj_t BgL_oz00_2903, obj_t BgL_vz00_2904)
	{
		{	/* Integrate/iinfo.sch 185 */
			return
				BGl_sfunzf2Iinfozd2kapturedzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2903), BgL_vz00_2904);
		}

	}



/* sfun/Iinfo-global */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2globalz20zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_109)
	{
		{	/* Integrate/iinfo.sch 186 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4381;

				{
					obj_t BgL_auxz00_4382;

					{	/* Integrate/iinfo.sch 186 */
						BgL_objectz00_bglt BgL_tmpz00_4383;

						BgL_tmpz00_4383 = ((BgL_objectz00_bglt) BgL_oz00_109);
						BgL_auxz00_4382 = BGL_OBJECT_WIDENING(BgL_tmpz00_4383);
					}
					BgL_auxz00_4381 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4382);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4381))->
					BgL_globalz00);
			}
		}

	}



/* &sfun/Iinfo-global */
	obj_t BGl_z62sfunzf2Iinfozd2globalz42zzintegrate_infoz00(obj_t
		BgL_envz00_2905, obj_t BgL_oz00_2906)
	{
		{	/* Integrate/iinfo.sch 186 */
			return
				BGl_sfunzf2Iinfozd2globalz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2906));
		}

	}



/* sfun/Iinfo-global-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2globalzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_110, obj_t BgL_vz00_111)
	{
		{	/* Integrate/iinfo.sch 187 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4390;

				{
					obj_t BgL_auxz00_4391;

					{	/* Integrate/iinfo.sch 187 */
						BgL_objectz00_bglt BgL_tmpz00_4392;

						BgL_tmpz00_4392 = ((BgL_objectz00_bglt) BgL_oz00_110);
						BgL_auxz00_4391 = BGL_OBJECT_WIDENING(BgL_tmpz00_4392);
					}
					BgL_auxz00_4390 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4391);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4390))->
						BgL_globalz00) = ((obj_t) BgL_vz00_111), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-global-set! */
	obj_t BGl_z62sfunzf2Iinfozd2globalzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2907, obj_t BgL_oz00_2908, obj_t BgL_vz00_2909)
	{
		{	/* Integrate/iinfo.sch 187 */
			return
				BGl_sfunzf2Iinfozd2globalzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2908), BgL_vz00_2909);
		}

	}



/* sfun/Iinfo-istamp */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2istampz20zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_112)
	{
		{	/* Integrate/iinfo.sch 188 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4399;

				{
					obj_t BgL_auxz00_4400;

					{	/* Integrate/iinfo.sch 188 */
						BgL_objectz00_bglt BgL_tmpz00_4401;

						BgL_tmpz00_4401 = ((BgL_objectz00_bglt) BgL_oz00_112);
						BgL_auxz00_4400 = BGL_OBJECT_WIDENING(BgL_tmpz00_4401);
					}
					BgL_auxz00_4399 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4400);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4399))->
					BgL_istampz00);
			}
		}

	}



/* &sfun/Iinfo-istamp */
	obj_t BGl_z62sfunzf2Iinfozd2istampz42zzintegrate_infoz00(obj_t
		BgL_envz00_2910, obj_t BgL_oz00_2911)
	{
		{	/* Integrate/iinfo.sch 188 */
			return
				BGl_sfunzf2Iinfozd2istampz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2911));
		}

	}



/* sfun/Iinfo-istamp-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2istampzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_113, obj_t BgL_vz00_114)
	{
		{	/* Integrate/iinfo.sch 189 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4408;

				{
					obj_t BgL_auxz00_4409;

					{	/* Integrate/iinfo.sch 189 */
						BgL_objectz00_bglt BgL_tmpz00_4410;

						BgL_tmpz00_4410 = ((BgL_objectz00_bglt) BgL_oz00_113);
						BgL_auxz00_4409 = BGL_OBJECT_WIDENING(BgL_tmpz00_4410);
					}
					BgL_auxz00_4408 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4409);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4408))->
						BgL_istampz00) = ((obj_t) BgL_vz00_114), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-istamp-set! */
	obj_t BGl_z62sfunzf2Iinfozd2istampzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2912, obj_t BgL_oz00_2913, obj_t BgL_vz00_2914)
	{
		{	/* Integrate/iinfo.sch 189 */
			return
				BGl_sfunzf2Iinfozd2istampzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2913), BgL_vz00_2914);
		}

	}



/* sfun/Iinfo-Led */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2Ledz20zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_115)
	{
		{	/* Integrate/iinfo.sch 190 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4417;

				{
					obj_t BgL_auxz00_4418;

					{	/* Integrate/iinfo.sch 190 */
						BgL_objectz00_bglt BgL_tmpz00_4419;

						BgL_tmpz00_4419 = ((BgL_objectz00_bglt) BgL_oz00_115);
						BgL_auxz00_4418 = BGL_OBJECT_WIDENING(BgL_tmpz00_4419);
					}
					BgL_auxz00_4417 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4418);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4417))->BgL_ledz00);
			}
		}

	}



/* &sfun/Iinfo-Led */
	obj_t BGl_z62sfunzf2Iinfozd2Ledz42zzintegrate_infoz00(obj_t BgL_envz00_2915,
		obj_t BgL_oz00_2916)
	{
		{	/* Integrate/iinfo.sch 190 */
			return
				BGl_sfunzf2Iinfozd2Ledz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2916));
		}

	}



/* sfun/Iinfo-Led-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2Ledzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_116, obj_t BgL_vz00_117)
	{
		{	/* Integrate/iinfo.sch 191 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4426;

				{
					obj_t BgL_auxz00_4427;

					{	/* Integrate/iinfo.sch 191 */
						BgL_objectz00_bglt BgL_tmpz00_4428;

						BgL_tmpz00_4428 = ((BgL_objectz00_bglt) BgL_oz00_116);
						BgL_auxz00_4427 = BGL_OBJECT_WIDENING(BgL_tmpz00_4428);
					}
					BgL_auxz00_4426 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4427);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4426))->BgL_ledz00) =
					((obj_t) BgL_vz00_117), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-Led-set! */
	obj_t BGl_z62sfunzf2Iinfozd2Ledzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2917, obj_t BgL_oz00_2918, obj_t BgL_vz00_2919)
	{
		{	/* Integrate/iinfo.sch 191 */
			return
				BGl_sfunzf2Iinfozd2Ledzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2918), BgL_vz00_2919);
		}

	}



/* sfun/Iinfo-L */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2Lz20zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_118)
	{
		{	/* Integrate/iinfo.sch 192 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4435;

				{
					obj_t BgL_auxz00_4436;

					{	/* Integrate/iinfo.sch 192 */
						BgL_objectz00_bglt BgL_tmpz00_4437;

						BgL_tmpz00_4437 = ((BgL_objectz00_bglt) BgL_oz00_118);
						BgL_auxz00_4436 = BGL_OBJECT_WIDENING(BgL_tmpz00_4437);
					}
					BgL_auxz00_4435 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4436);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4435))->BgL_lz00);
			}
		}

	}



/* &sfun/Iinfo-L */
	obj_t BGl_z62sfunzf2Iinfozd2Lz42zzintegrate_infoz00(obj_t BgL_envz00_2920,
		obj_t BgL_oz00_2921)
	{
		{	/* Integrate/iinfo.sch 192 */
			return
				BGl_sfunzf2Iinfozd2Lz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2921));
		}

	}



/* sfun/Iinfo-L-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2Lzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_119, obj_t BgL_vz00_120)
	{
		{	/* Integrate/iinfo.sch 193 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4444;

				{
					obj_t BgL_auxz00_4445;

					{	/* Integrate/iinfo.sch 193 */
						BgL_objectz00_bglt BgL_tmpz00_4446;

						BgL_tmpz00_4446 = ((BgL_objectz00_bglt) BgL_oz00_119);
						BgL_auxz00_4445 = BGL_OBJECT_WIDENING(BgL_tmpz00_4446);
					}
					BgL_auxz00_4444 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4445);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4444))->BgL_lz00) =
					((obj_t) BgL_vz00_120), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-L-set! */
	obj_t BGl_z62sfunzf2Iinfozd2Lzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2922, obj_t BgL_oz00_2923, obj_t BgL_vz00_2924)
	{
		{	/* Integrate/iinfo.sch 193 */
			return
				BGl_sfunzf2Iinfozd2Lzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2923), BgL_vz00_2924);
		}

	}



/* sfun/Iinfo-forceG? */
	BGL_EXPORTED_DEF bool_t
		BGl_sfunzf2Iinfozd2forceGzf3zd3zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_121)
	{
		{	/* Integrate/iinfo.sch 194 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4453;

				{
					obj_t BgL_auxz00_4454;

					{	/* Integrate/iinfo.sch 194 */
						BgL_objectz00_bglt BgL_tmpz00_4455;

						BgL_tmpz00_4455 = ((BgL_objectz00_bglt) BgL_oz00_121);
						BgL_auxz00_4454 = BGL_OBJECT_WIDENING(BgL_tmpz00_4455);
					}
					BgL_auxz00_4453 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4454);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4453))->
					BgL_forcegzf3zf3);
			}
		}

	}



/* &sfun/Iinfo-forceG? */
	obj_t BGl_z62sfunzf2Iinfozd2forceGzf3zb1zzintegrate_infoz00(obj_t
		BgL_envz00_2925, obj_t BgL_oz00_2926)
	{
		{	/* Integrate/iinfo.sch 194 */
			return
				BBOOL(BGl_sfunzf2Iinfozd2forceGzf3zd3zzintegrate_infoz00(
					((BgL_sfunz00_bglt) BgL_oz00_2926)));
		}

	}



/* sfun/Iinfo-forceG?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2forceGzf3zd2setz12z13zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_122, bool_t BgL_vz00_123)
	{
		{	/* Integrate/iinfo.sch 195 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4463;

				{
					obj_t BgL_auxz00_4464;

					{	/* Integrate/iinfo.sch 195 */
						BgL_objectz00_bglt BgL_tmpz00_4465;

						BgL_tmpz00_4465 = ((BgL_objectz00_bglt) BgL_oz00_122);
						BgL_auxz00_4464 = BGL_OBJECT_WIDENING(BgL_tmpz00_4465);
					}
					BgL_auxz00_4463 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4464);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4463))->
						BgL_forcegzf3zf3) = ((bool_t) BgL_vz00_123), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-forceG?-set! */
	obj_t BGl_z62sfunzf2Iinfozd2forceGzf3zd2setz12z71zzintegrate_infoz00(obj_t
		BgL_envz00_2927, obj_t BgL_oz00_2928, obj_t BgL_vz00_2929)
	{
		{	/* Integrate/iinfo.sch 195 */
			return
				BGl_sfunzf2Iinfozd2forceGzf3zd2setz12z13zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2928), CBOOL(BgL_vz00_2929));
		}

	}



/* sfun/Iinfo-G? */
	BGL_EXPORTED_DEF bool_t
		BGl_sfunzf2Iinfozd2Gzf3zd3zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_124)
	{
		{	/* Integrate/iinfo.sch 196 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4473;

				{
					obj_t BgL_auxz00_4474;

					{	/* Integrate/iinfo.sch 196 */
						BgL_objectz00_bglt BgL_tmpz00_4475;

						BgL_tmpz00_4475 = ((BgL_objectz00_bglt) BgL_oz00_124);
						BgL_auxz00_4474 = BGL_OBJECT_WIDENING(BgL_tmpz00_4475);
					}
					BgL_auxz00_4473 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4474);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4473))->BgL_gzf3zf3);
			}
		}

	}



/* &sfun/Iinfo-G? */
	obj_t BGl_z62sfunzf2Iinfozd2Gzf3zb1zzintegrate_infoz00(obj_t BgL_envz00_2930,
		obj_t BgL_oz00_2931)
	{
		{	/* Integrate/iinfo.sch 196 */
			return
				BBOOL(BGl_sfunzf2Iinfozd2Gzf3zd3zzintegrate_infoz00(
					((BgL_sfunz00_bglt) BgL_oz00_2931)));
		}

	}



/* sfun/Iinfo-G?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2Gzf3zd2setz12z13zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_125, bool_t BgL_vz00_126)
	{
		{	/* Integrate/iinfo.sch 197 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4483;

				{
					obj_t BgL_auxz00_4484;

					{	/* Integrate/iinfo.sch 197 */
						BgL_objectz00_bglt BgL_tmpz00_4485;

						BgL_tmpz00_4485 = ((BgL_objectz00_bglt) BgL_oz00_125);
						BgL_auxz00_4484 = BGL_OBJECT_WIDENING(BgL_tmpz00_4485);
					}
					BgL_auxz00_4483 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4484);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4483))->
						BgL_gzf3zf3) = ((bool_t) BgL_vz00_126), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-G?-set! */
	obj_t BGl_z62sfunzf2Iinfozd2Gzf3zd2setz12z71zzintegrate_infoz00(obj_t
		BgL_envz00_2932, obj_t BgL_oz00_2933, obj_t BgL_vz00_2934)
	{
		{	/* Integrate/iinfo.sch 197 */
			return
				BGl_sfunzf2Iinfozd2Gzf3zd2setz12z13zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2933), CBOOL(BgL_vz00_2934));
		}

	}



/* sfun/Iinfo-kont */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2kontz20zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_127)
	{
		{	/* Integrate/iinfo.sch 198 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4493;

				{
					obj_t BgL_auxz00_4494;

					{	/* Integrate/iinfo.sch 198 */
						BgL_objectz00_bglt BgL_tmpz00_4495;

						BgL_tmpz00_4495 = ((BgL_objectz00_bglt) BgL_oz00_127);
						BgL_auxz00_4494 = BGL_OBJECT_WIDENING(BgL_tmpz00_4495);
					}
					BgL_auxz00_4493 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4494);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4493))->BgL_kontz00);
			}
		}

	}



/* &sfun/Iinfo-kont */
	obj_t BGl_z62sfunzf2Iinfozd2kontz42zzintegrate_infoz00(obj_t BgL_envz00_2935,
		obj_t BgL_oz00_2936)
	{
		{	/* Integrate/iinfo.sch 198 */
			return
				BGl_sfunzf2Iinfozd2kontz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2936));
		}

	}



/* sfun/Iinfo-kont-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2kontzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_128, obj_t BgL_vz00_129)
	{
		{	/* Integrate/iinfo.sch 199 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4502;

				{
					obj_t BgL_auxz00_4503;

					{	/* Integrate/iinfo.sch 199 */
						BgL_objectz00_bglt BgL_tmpz00_4504;

						BgL_tmpz00_4504 = ((BgL_objectz00_bglt) BgL_oz00_128);
						BgL_auxz00_4503 = BGL_OBJECT_WIDENING(BgL_tmpz00_4504);
					}
					BgL_auxz00_4502 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4503);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4502))->
						BgL_kontz00) = ((obj_t) BgL_vz00_129), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-kont-set! */
	obj_t BGl_z62sfunzf2Iinfozd2kontzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2937, obj_t BgL_oz00_2938, obj_t BgL_vz00_2939)
	{
		{	/* Integrate/iinfo.sch 199 */
			return
				BGl_sfunzf2Iinfozd2kontzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2938), BgL_vz00_2939);
		}

	}



/* sfun/Iinfo-Ct */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2Ctz20zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_130)
	{
		{	/* Integrate/iinfo.sch 200 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4511;

				{
					obj_t BgL_auxz00_4512;

					{	/* Integrate/iinfo.sch 200 */
						BgL_objectz00_bglt BgL_tmpz00_4513;

						BgL_tmpz00_4513 = ((BgL_objectz00_bglt) BgL_oz00_130);
						BgL_auxz00_4512 = BGL_OBJECT_WIDENING(BgL_tmpz00_4513);
					}
					BgL_auxz00_4511 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4512);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4511))->BgL_ctz00);
			}
		}

	}



/* &sfun/Iinfo-Ct */
	obj_t BGl_z62sfunzf2Iinfozd2Ctz42zzintegrate_infoz00(obj_t BgL_envz00_2940,
		obj_t BgL_oz00_2941)
	{
		{	/* Integrate/iinfo.sch 200 */
			return
				BGl_sfunzf2Iinfozd2Ctz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2941));
		}

	}



/* sfun/Iinfo-Ct-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2Ctzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_131, obj_t BgL_vz00_132)
	{
		{	/* Integrate/iinfo.sch 201 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4520;

				{
					obj_t BgL_auxz00_4521;

					{	/* Integrate/iinfo.sch 201 */
						BgL_objectz00_bglt BgL_tmpz00_4522;

						BgL_tmpz00_4522 = ((BgL_objectz00_bglt) BgL_oz00_131);
						BgL_auxz00_4521 = BGL_OBJECT_WIDENING(BgL_tmpz00_4522);
					}
					BgL_auxz00_4520 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4521);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4520))->BgL_ctz00) =
					((obj_t) BgL_vz00_132), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-Ct-set! */
	obj_t BGl_z62sfunzf2Iinfozd2Ctzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2942, obj_t BgL_oz00_2943, obj_t BgL_vz00_2944)
	{
		{	/* Integrate/iinfo.sch 201 */
			return
				BGl_sfunzf2Iinfozd2Ctzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2943), BgL_vz00_2944);
		}

	}



/* sfun/Iinfo-Cn */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2Cnz20zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_133)
	{
		{	/* Integrate/iinfo.sch 202 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4529;

				{
					obj_t BgL_auxz00_4530;

					{	/* Integrate/iinfo.sch 202 */
						BgL_objectz00_bglt BgL_tmpz00_4531;

						BgL_tmpz00_4531 = ((BgL_objectz00_bglt) BgL_oz00_133);
						BgL_auxz00_4530 = BGL_OBJECT_WIDENING(BgL_tmpz00_4531);
					}
					BgL_auxz00_4529 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4530);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4529))->BgL_cnz00);
			}
		}

	}



/* &sfun/Iinfo-Cn */
	obj_t BGl_z62sfunzf2Iinfozd2Cnz42zzintegrate_infoz00(obj_t BgL_envz00_2945,
		obj_t BgL_oz00_2946)
	{
		{	/* Integrate/iinfo.sch 202 */
			return
				BGl_sfunzf2Iinfozd2Cnz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2946));
		}

	}



/* sfun/Iinfo-Cn-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2Cnzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_134, obj_t BgL_vz00_135)
	{
		{	/* Integrate/iinfo.sch 203 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4538;

				{
					obj_t BgL_auxz00_4539;

					{	/* Integrate/iinfo.sch 203 */
						BgL_objectz00_bglt BgL_tmpz00_4540;

						BgL_tmpz00_4540 = ((BgL_objectz00_bglt) BgL_oz00_134);
						BgL_auxz00_4539 = BGL_OBJECT_WIDENING(BgL_tmpz00_4540);
					}
					BgL_auxz00_4538 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4539);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4538))->BgL_cnz00) =
					((obj_t) BgL_vz00_135), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-Cn-set! */
	obj_t BGl_z62sfunzf2Iinfozd2Cnzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2947, obj_t BgL_oz00_2948, obj_t BgL_vz00_2949)
	{
		{	/* Integrate/iinfo.sch 203 */
			return
				BGl_sfunzf2Iinfozd2Cnzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2948), BgL_vz00_2949);
		}

	}



/* sfun/Iinfo-U */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2Uz20zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_136)
	{
		{	/* Integrate/iinfo.sch 204 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4547;

				{
					obj_t BgL_auxz00_4548;

					{	/* Integrate/iinfo.sch 204 */
						BgL_objectz00_bglt BgL_tmpz00_4549;

						BgL_tmpz00_4549 = ((BgL_objectz00_bglt) BgL_oz00_136);
						BgL_auxz00_4548 = BGL_OBJECT_WIDENING(BgL_tmpz00_4549);
					}
					BgL_auxz00_4547 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4548);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4547))->BgL_uz00);
			}
		}

	}



/* &sfun/Iinfo-U */
	obj_t BGl_z62sfunzf2Iinfozd2Uz42zzintegrate_infoz00(obj_t BgL_envz00_2950,
		obj_t BgL_oz00_2951)
	{
		{	/* Integrate/iinfo.sch 204 */
			return
				BGl_sfunzf2Iinfozd2Uz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2951));
		}

	}



/* sfun/Iinfo-U-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2Uzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_137, obj_t BgL_vz00_138)
	{
		{	/* Integrate/iinfo.sch 205 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4556;

				{
					obj_t BgL_auxz00_4557;

					{	/* Integrate/iinfo.sch 205 */
						BgL_objectz00_bglt BgL_tmpz00_4558;

						BgL_tmpz00_4558 = ((BgL_objectz00_bglt) BgL_oz00_137);
						BgL_auxz00_4557 = BGL_OBJECT_WIDENING(BgL_tmpz00_4558);
					}
					BgL_auxz00_4556 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4557);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4556))->BgL_uz00) =
					((obj_t) BgL_vz00_138), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-U-set! */
	obj_t BGl_z62sfunzf2Iinfozd2Uzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2952, obj_t BgL_oz00_2953, obj_t BgL_vz00_2954)
	{
		{	/* Integrate/iinfo.sch 205 */
			return
				BGl_sfunzf2Iinfozd2Uzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2953), BgL_vz00_2954);
		}

	}



/* sfun/Iinfo-K* */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2Kza2z82zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_139)
	{
		{	/* Integrate/iinfo.sch 206 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4565;

				{
					obj_t BgL_auxz00_4566;

					{	/* Integrate/iinfo.sch 206 */
						BgL_objectz00_bglt BgL_tmpz00_4567;

						BgL_tmpz00_4567 = ((BgL_objectz00_bglt) BgL_oz00_139);
						BgL_auxz00_4566 = BGL_OBJECT_WIDENING(BgL_tmpz00_4567);
					}
					BgL_auxz00_4565 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4566);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4565))->BgL_kza2za2);
			}
		}

	}



/* &sfun/Iinfo-K* */
	obj_t BGl_z62sfunzf2Iinfozd2Kza2ze0zzintegrate_infoz00(obj_t BgL_envz00_2955,
		obj_t BgL_oz00_2956)
	{
		{	/* Integrate/iinfo.sch 206 */
			return
				BGl_sfunzf2Iinfozd2Kza2z82zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2956));
		}

	}



/* sfun/Iinfo-K*-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2Kza2zd2setz12z42zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_140, obj_t BgL_vz00_141)
	{
		{	/* Integrate/iinfo.sch 207 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4574;

				{
					obj_t BgL_auxz00_4575;

					{	/* Integrate/iinfo.sch 207 */
						BgL_objectz00_bglt BgL_tmpz00_4576;

						BgL_tmpz00_4576 = ((BgL_objectz00_bglt) BgL_oz00_140);
						BgL_auxz00_4575 = BGL_OBJECT_WIDENING(BgL_tmpz00_4576);
					}
					BgL_auxz00_4574 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4575);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4574))->
						BgL_kza2za2) = ((obj_t) BgL_vz00_141), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-K*-set! */
	obj_t BGl_z62sfunzf2Iinfozd2Kza2zd2setz12z20zzintegrate_infoz00(obj_t
		BgL_envz00_2957, obj_t BgL_oz00_2958, obj_t BgL_vz00_2959)
	{
		{	/* Integrate/iinfo.sch 207 */
			return
				BGl_sfunzf2Iinfozd2Kza2zd2setz12z42zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2958), BgL_vz00_2959);
		}

	}



/* sfun/Iinfo-K */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2Kz20zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_142)
	{
		{	/* Integrate/iinfo.sch 208 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4583;

				{
					obj_t BgL_auxz00_4584;

					{	/* Integrate/iinfo.sch 208 */
						BgL_objectz00_bglt BgL_tmpz00_4585;

						BgL_tmpz00_4585 = ((BgL_objectz00_bglt) BgL_oz00_142);
						BgL_auxz00_4584 = BGL_OBJECT_WIDENING(BgL_tmpz00_4585);
					}
					BgL_auxz00_4583 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4584);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4583))->BgL_kz00);
			}
		}

	}



/* &sfun/Iinfo-K */
	obj_t BGl_z62sfunzf2Iinfozd2Kz42zzintegrate_infoz00(obj_t BgL_envz00_2960,
		obj_t BgL_oz00_2961)
	{
		{	/* Integrate/iinfo.sch 208 */
			return
				BGl_sfunzf2Iinfozd2Kz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2961));
		}

	}



/* sfun/Iinfo-K-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2Kzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_143, obj_t BgL_vz00_144)
	{
		{	/* Integrate/iinfo.sch 209 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4592;

				{
					obj_t BgL_auxz00_4593;

					{	/* Integrate/iinfo.sch 209 */
						BgL_objectz00_bglt BgL_tmpz00_4594;

						BgL_tmpz00_4594 = ((BgL_objectz00_bglt) BgL_oz00_143);
						BgL_auxz00_4593 = BGL_OBJECT_WIDENING(BgL_tmpz00_4594);
					}
					BgL_auxz00_4592 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4593);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4592))->BgL_kz00) =
					((obj_t) BgL_vz00_144), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-K-set! */
	obj_t BGl_z62sfunzf2Iinfozd2Kzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2962, obj_t BgL_oz00_2963, obj_t BgL_vz00_2964)
	{
		{	/* Integrate/iinfo.sch 209 */
			return
				BGl_sfunzf2Iinfozd2Kzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2963), BgL_vz00_2964);
		}

	}



/* sfun/Iinfo-cto */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2ctoz20zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_145)
	{
		{	/* Integrate/iinfo.sch 210 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4601;

				{
					obj_t BgL_auxz00_4602;

					{	/* Integrate/iinfo.sch 210 */
						BgL_objectz00_bglt BgL_tmpz00_4603;

						BgL_tmpz00_4603 = ((BgL_objectz00_bglt) BgL_oz00_145);
						BgL_auxz00_4602 = BGL_OBJECT_WIDENING(BgL_tmpz00_4603);
					}
					BgL_auxz00_4601 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4602);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4601))->BgL_ctoz00);
			}
		}

	}



/* &sfun/Iinfo-cto */
	obj_t BGl_z62sfunzf2Iinfozd2ctoz42zzintegrate_infoz00(obj_t BgL_envz00_2965,
		obj_t BgL_oz00_2966)
	{
		{	/* Integrate/iinfo.sch 210 */
			return
				BGl_sfunzf2Iinfozd2ctoz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2966));
		}

	}



/* sfun/Iinfo-cto-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2ctozd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_146, obj_t BgL_vz00_147)
	{
		{	/* Integrate/iinfo.sch 211 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4610;

				{
					obj_t BgL_auxz00_4611;

					{	/* Integrate/iinfo.sch 211 */
						BgL_objectz00_bglt BgL_tmpz00_4612;

						BgL_tmpz00_4612 = ((BgL_objectz00_bglt) BgL_oz00_146);
						BgL_auxz00_4611 = BGL_OBJECT_WIDENING(BgL_tmpz00_4612);
					}
					BgL_auxz00_4610 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4611);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4610))->BgL_ctoz00) =
					((obj_t) BgL_vz00_147), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-cto-set! */
	obj_t BGl_z62sfunzf2Iinfozd2ctozd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2967, obj_t BgL_oz00_2968, obj_t BgL_vz00_2969)
	{
		{	/* Integrate/iinfo.sch 211 */
			return
				BGl_sfunzf2Iinfozd2ctozd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2968), BgL_vz00_2969);
		}

	}



/* sfun/Iinfo-cfrom */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2cfromz20zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_148)
	{
		{	/* Integrate/iinfo.sch 212 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4619;

				{
					obj_t BgL_auxz00_4620;

					{	/* Integrate/iinfo.sch 212 */
						BgL_objectz00_bglt BgL_tmpz00_4621;

						BgL_tmpz00_4621 = ((BgL_objectz00_bglt) BgL_oz00_148);
						BgL_auxz00_4620 = BGL_OBJECT_WIDENING(BgL_tmpz00_4621);
					}
					BgL_auxz00_4619 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4620);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4619))->BgL_cfromz00);
			}
		}

	}



/* &sfun/Iinfo-cfrom */
	obj_t BGl_z62sfunzf2Iinfozd2cfromz42zzintegrate_infoz00(obj_t BgL_envz00_2970,
		obj_t BgL_oz00_2971)
	{
		{	/* Integrate/iinfo.sch 212 */
			return
				BGl_sfunzf2Iinfozd2cfromz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2971));
		}

	}



/* sfun/Iinfo-cfrom-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2cfromzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_149, obj_t BgL_vz00_150)
	{
		{	/* Integrate/iinfo.sch 213 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4628;

				{
					obj_t BgL_auxz00_4629;

					{	/* Integrate/iinfo.sch 213 */
						BgL_objectz00_bglt BgL_tmpz00_4630;

						BgL_tmpz00_4630 = ((BgL_objectz00_bglt) BgL_oz00_149);
						BgL_auxz00_4629 = BGL_OBJECT_WIDENING(BgL_tmpz00_4630);
					}
					BgL_auxz00_4628 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4629);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4628))->
						BgL_cfromz00) = ((obj_t) BgL_vz00_150), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-cfrom-set! */
	obj_t BGl_z62sfunzf2Iinfozd2cfromzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2972, obj_t BgL_oz00_2973, obj_t BgL_vz00_2974)
	{
		{	/* Integrate/iinfo.sch 213 */
			return
				BGl_sfunzf2Iinfozd2cfromzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2973), BgL_vz00_2974);
		}

	}



/* sfun/Iinfo-bound */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2boundz20zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_151)
	{
		{	/* Integrate/iinfo.sch 214 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4637;

				{
					obj_t BgL_auxz00_4638;

					{	/* Integrate/iinfo.sch 214 */
						BgL_objectz00_bglt BgL_tmpz00_4639;

						BgL_tmpz00_4639 = ((BgL_objectz00_bglt) BgL_oz00_151);
						BgL_auxz00_4638 = BGL_OBJECT_WIDENING(BgL_tmpz00_4639);
					}
					BgL_auxz00_4637 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4638);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4637))->BgL_boundz00);
			}
		}

	}



/* &sfun/Iinfo-bound */
	obj_t BGl_z62sfunzf2Iinfozd2boundz42zzintegrate_infoz00(obj_t BgL_envz00_2975,
		obj_t BgL_oz00_2976)
	{
		{	/* Integrate/iinfo.sch 214 */
			return
				BGl_sfunzf2Iinfozd2boundz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2976));
		}

	}



/* sfun/Iinfo-bound-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2boundzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_152, obj_t BgL_vz00_153)
	{
		{	/* Integrate/iinfo.sch 215 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4646;

				{
					obj_t BgL_auxz00_4647;

					{	/* Integrate/iinfo.sch 215 */
						BgL_objectz00_bglt BgL_tmpz00_4648;

						BgL_tmpz00_4648 = ((BgL_objectz00_bglt) BgL_oz00_152);
						BgL_auxz00_4647 = BGL_OBJECT_WIDENING(BgL_tmpz00_4648);
					}
					BgL_auxz00_4646 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4647);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4646))->
						BgL_boundz00) = ((obj_t) BgL_vz00_153), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-bound-set! */
	obj_t BGl_z62sfunzf2Iinfozd2boundzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2977, obj_t BgL_oz00_2978, obj_t BgL_vz00_2979)
	{
		{	/* Integrate/iinfo.sch 215 */
			return
				BGl_sfunzf2Iinfozd2boundzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2978), BgL_vz00_2979);
		}

	}



/* sfun/Iinfo-free */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2freez20zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_154)
	{
		{	/* Integrate/iinfo.sch 216 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4655;

				{
					obj_t BgL_auxz00_4656;

					{	/* Integrate/iinfo.sch 216 */
						BgL_objectz00_bglt BgL_tmpz00_4657;

						BgL_tmpz00_4657 = ((BgL_objectz00_bglt) BgL_oz00_154);
						BgL_auxz00_4656 = BGL_OBJECT_WIDENING(BgL_tmpz00_4657);
					}
					BgL_auxz00_4655 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4656);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4655))->BgL_freez00);
			}
		}

	}



/* &sfun/Iinfo-free */
	obj_t BGl_z62sfunzf2Iinfozd2freez42zzintegrate_infoz00(obj_t BgL_envz00_2980,
		obj_t BgL_oz00_2981)
	{
		{	/* Integrate/iinfo.sch 216 */
			return
				BGl_sfunzf2Iinfozd2freez20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2981));
		}

	}



/* sfun/Iinfo-free-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2freezd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_155, obj_t BgL_vz00_156)
	{
		{	/* Integrate/iinfo.sch 217 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4664;

				{
					obj_t BgL_auxz00_4665;

					{	/* Integrate/iinfo.sch 217 */
						BgL_objectz00_bglt BgL_tmpz00_4666;

						BgL_tmpz00_4666 = ((BgL_objectz00_bglt) BgL_oz00_155);
						BgL_auxz00_4665 = BGL_OBJECT_WIDENING(BgL_tmpz00_4666);
					}
					BgL_auxz00_4664 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4665);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4664))->
						BgL_freez00) = ((obj_t) BgL_vz00_156), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-free-set! */
	obj_t BGl_z62sfunzf2Iinfozd2freezd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2982, obj_t BgL_oz00_2983, obj_t BgL_vz00_2984)
	{
		{	/* Integrate/iinfo.sch 217 */
			return
				BGl_sfunzf2Iinfozd2freezd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2983), BgL_vz00_2984);
		}

	}



/* sfun/Iinfo-owner */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2ownerz20zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_157)
	{
		{	/* Integrate/iinfo.sch 218 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4673;

				{
					obj_t BgL_auxz00_4674;

					{	/* Integrate/iinfo.sch 218 */
						BgL_objectz00_bglt BgL_tmpz00_4675;

						BgL_tmpz00_4675 = ((BgL_objectz00_bglt) BgL_oz00_157);
						BgL_auxz00_4674 = BGL_OBJECT_WIDENING(BgL_tmpz00_4675);
					}
					BgL_auxz00_4673 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4674);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4673))->BgL_ownerz00);
			}
		}

	}



/* &sfun/Iinfo-owner */
	obj_t BGl_z62sfunzf2Iinfozd2ownerz42zzintegrate_infoz00(obj_t BgL_envz00_2985,
		obj_t BgL_oz00_2986)
	{
		{	/* Integrate/iinfo.sch 218 */
			return
				BGl_sfunzf2Iinfozd2ownerz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2986));
		}

	}



/* sfun/Iinfo-owner-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2ownerzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_158, obj_t BgL_vz00_159)
	{
		{	/* Integrate/iinfo.sch 219 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4682;

				{
					obj_t BgL_auxz00_4683;

					{	/* Integrate/iinfo.sch 219 */
						BgL_objectz00_bglt BgL_tmpz00_4684;

						BgL_tmpz00_4684 = ((BgL_objectz00_bglt) BgL_oz00_158);
						BgL_auxz00_4683 = BGL_OBJECT_WIDENING(BgL_tmpz00_4684);
					}
					BgL_auxz00_4682 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4683);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4682))->
						BgL_ownerz00) = ((obj_t) BgL_vz00_159), BUNSPEC);
			}
		}

	}



/* &sfun/Iinfo-owner-set! */
	obj_t BGl_z62sfunzf2Iinfozd2ownerzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2987, obj_t BgL_oz00_2988, obj_t BgL_vz00_2989)
	{
		{	/* Integrate/iinfo.sch 219 */
			return
				BGl_sfunzf2Iinfozd2ownerzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2988), BgL_vz00_2989);
		}

	}



/* sfun/Iinfo-stackable */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2stackablez20zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_160)
	{
		{	/* Integrate/iinfo.sch 220 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_160)))->BgL_stackablez00);
		}

	}



/* &sfun/Iinfo-stackable */
	obj_t BGl_z62sfunzf2Iinfozd2stackablez42zzintegrate_infoz00(obj_t
		BgL_envz00_2990, obj_t BgL_oz00_2991)
	{
		{	/* Integrate/iinfo.sch 220 */
			return
				BGl_sfunzf2Iinfozd2stackablez20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2991));
		}

	}



/* sfun/Iinfo-stackable-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2stackablezd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_161, obj_t BgL_vz00_162)
	{
		{	/* Integrate/iinfo.sch 221 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_161)))->BgL_stackablez00) =
				((obj_t) BgL_vz00_162), BUNSPEC);
		}

	}



/* &sfun/Iinfo-stackable-set! */
	obj_t BGl_z62sfunzf2Iinfozd2stackablezd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2992, obj_t BgL_oz00_2993, obj_t BgL_vz00_2994)
	{
		{	/* Integrate/iinfo.sch 221 */
			return
				BGl_sfunzf2Iinfozd2stackablezd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2993), BgL_vz00_2994);
		}

	}



/* sfun/Iinfo-strength */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2strengthz20zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_163)
	{
		{	/* Integrate/iinfo.sch 222 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_163)))->BgL_strengthz00);
		}

	}



/* &sfun/Iinfo-strength */
	obj_t BGl_z62sfunzf2Iinfozd2strengthz42zzintegrate_infoz00(obj_t
		BgL_envz00_2995, obj_t BgL_oz00_2996)
	{
		{	/* Integrate/iinfo.sch 222 */
			return
				BGl_sfunzf2Iinfozd2strengthz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2996));
		}

	}



/* sfun/Iinfo-strength-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2strengthzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_164, obj_t BgL_vz00_165)
	{
		{	/* Integrate/iinfo.sch 223 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_164)))->BgL_strengthz00) =
				((obj_t) BgL_vz00_165), BUNSPEC);
		}

	}



/* &sfun/Iinfo-strength-set! */
	obj_t BGl_z62sfunzf2Iinfozd2strengthzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_2997, obj_t BgL_oz00_2998, obj_t BgL_vz00_2999)
	{
		{	/* Integrate/iinfo.sch 223 */
			return
				BGl_sfunzf2Iinfozd2strengthzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_2998), BgL_vz00_2999);
		}

	}



/* sfun/Iinfo-the-closure-global */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2thezd2closurezd2globalz20zzintegrate_infoz00
		(BgL_sfunz00_bglt BgL_oz00_166)
	{
		{	/* Integrate/iinfo.sch 224 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_166)))->BgL_thezd2closurezd2globalz00);
		}

	}



/* &sfun/Iinfo-the-closure-global */
	obj_t BGl_z62sfunzf2Iinfozd2thezd2closurezd2globalz42zzintegrate_infoz00(obj_t
		BgL_envz00_3000, obj_t BgL_oz00_3001)
	{
		{	/* Integrate/iinfo.sch 224 */
			return
				BGl_sfunzf2Iinfozd2thezd2closurezd2globalz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3001));
		}

	}



/* sfun/Iinfo-the-closure-global-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2thezd2closurezd2globalzd2setz12ze0zzintegrate_infoz00
		(BgL_sfunz00_bglt BgL_oz00_167, obj_t BgL_vz00_168)
	{
		{	/* Integrate/iinfo.sch 225 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_167)))->
					BgL_thezd2closurezd2globalz00) = ((obj_t) BgL_vz00_168), BUNSPEC);
		}

	}



/* &sfun/Iinfo-the-closure-global-set! */
	obj_t
		BGl_z62sfunzf2Iinfozd2thezd2closurezd2globalzd2setz12z82zzintegrate_infoz00
		(obj_t BgL_envz00_3002, obj_t BgL_oz00_3003, obj_t BgL_vz00_3004)
	{
		{	/* Integrate/iinfo.sch 225 */
			return
				BGl_sfunzf2Iinfozd2thezd2closurezd2globalzd2setz12ze0zzintegrate_infoz00
				(((BgL_sfunz00_bglt) BgL_oz00_3003), BgL_vz00_3004);
		}

	}



/* sfun/Iinfo-keys */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2keysz20zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_169)
	{
		{	/* Integrate/iinfo.sch 226 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_169)))->BgL_keysz00);
		}

	}



/* &sfun/Iinfo-keys */
	obj_t BGl_z62sfunzf2Iinfozd2keysz42zzintegrate_infoz00(obj_t BgL_envz00_3005,
		obj_t BgL_oz00_3006)
	{
		{	/* Integrate/iinfo.sch 226 */
			return
				BGl_sfunzf2Iinfozd2keysz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3006));
		}

	}



/* sfun/Iinfo-optionals */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2optionalsz20zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_172)
	{
		{	/* Integrate/iinfo.sch 228 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_172)))->BgL_optionalsz00);
		}

	}



/* &sfun/Iinfo-optionals */
	obj_t BGl_z62sfunzf2Iinfozd2optionalsz42zzintegrate_infoz00(obj_t
		BgL_envz00_3007, obj_t BgL_oz00_3008)
	{
		{	/* Integrate/iinfo.sch 228 */
			return
				BGl_sfunzf2Iinfozd2optionalsz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3008));
		}

	}



/* sfun/Iinfo-loc */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2locz20zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_175)
	{
		{	/* Integrate/iinfo.sch 230 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_175)))->BgL_locz00);
		}

	}



/* &sfun/Iinfo-loc */
	obj_t BGl_z62sfunzf2Iinfozd2locz42zzintegrate_infoz00(obj_t BgL_envz00_3009,
		obj_t BgL_oz00_3010)
	{
		{	/* Integrate/iinfo.sch 230 */
			return
				BGl_sfunzf2Iinfozd2locz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3010));
		}

	}



/* sfun/Iinfo-loc-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2loczd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_176, obj_t BgL_vz00_177)
	{
		{	/* Integrate/iinfo.sch 231 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_176)))->BgL_locz00) =
				((obj_t) BgL_vz00_177), BUNSPEC);
		}

	}



/* &sfun/Iinfo-loc-set! */
	obj_t BGl_z62sfunzf2Iinfozd2loczd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_3011, obj_t BgL_oz00_3012, obj_t BgL_vz00_3013)
	{
		{	/* Integrate/iinfo.sch 231 */
			return
				BGl_sfunzf2Iinfozd2loczd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3012), BgL_vz00_3013);
		}

	}



/* sfun/Iinfo-dsssl-keywords */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2dssslzd2keywordszf2zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_178)
	{
		{	/* Integrate/iinfo.sch 232 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_178)))->BgL_dssslzd2keywordszd2);
		}

	}



/* &sfun/Iinfo-dsssl-keywords */
	obj_t BGl_z62sfunzf2Iinfozd2dssslzd2keywordsz90zzintegrate_infoz00(obj_t
		BgL_envz00_3014, obj_t BgL_oz00_3015)
	{
		{	/* Integrate/iinfo.sch 232 */
			return
				BGl_sfunzf2Iinfozd2dssslzd2keywordszf2zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3015));
		}

	}



/* sfun/Iinfo-dsssl-keywords-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2dssslzd2keywordszd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt BgL_oz00_179, obj_t BgL_vz00_180)
	{
		{	/* Integrate/iinfo.sch 233 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_179)))->BgL_dssslzd2keywordszd2) =
				((obj_t) BgL_vz00_180), BUNSPEC);
		}

	}



/* &sfun/Iinfo-dsssl-keywords-set! */
	obj_t
		BGl_z62sfunzf2Iinfozd2dssslzd2keywordszd2setz12z50zzintegrate_infoz00(obj_t
		BgL_envz00_3016, obj_t BgL_oz00_3017, obj_t BgL_vz00_3018)
	{
		{	/* Integrate/iinfo.sch 233 */
			return
				BGl_sfunzf2Iinfozd2dssslzd2keywordszd2setz12z32zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3017), BgL_vz00_3018);
		}

	}



/* sfun/Iinfo-class */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2classz20zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_181)
	{
		{	/* Integrate/iinfo.sch 234 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_181)))->BgL_classz00);
		}

	}



/* &sfun/Iinfo-class */
	obj_t BGl_z62sfunzf2Iinfozd2classz42zzintegrate_infoz00(obj_t BgL_envz00_3019,
		obj_t BgL_oz00_3020)
	{
		{	/* Integrate/iinfo.sch 234 */
			return
				BGl_sfunzf2Iinfozd2classz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3020));
		}

	}



/* sfun/Iinfo-class-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2classzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_182, obj_t BgL_vz00_183)
	{
		{	/* Integrate/iinfo.sch 235 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_182)))->BgL_classz00) =
				((obj_t) BgL_vz00_183), BUNSPEC);
		}

	}



/* &sfun/Iinfo-class-set! */
	obj_t BGl_z62sfunzf2Iinfozd2classzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_3021, obj_t BgL_oz00_3022, obj_t BgL_vz00_3023)
	{
		{	/* Integrate/iinfo.sch 235 */
			return
				BGl_sfunzf2Iinfozd2classzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3022), BgL_vz00_3023);
		}

	}



/* sfun/Iinfo-body */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2bodyz20zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_184)
	{
		{	/* Integrate/iinfo.sch 236 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_184)))->BgL_bodyz00);
		}

	}



/* &sfun/Iinfo-body */
	obj_t BGl_z62sfunzf2Iinfozd2bodyz42zzintegrate_infoz00(obj_t BgL_envz00_3024,
		obj_t BgL_oz00_3025)
	{
		{	/* Integrate/iinfo.sch 236 */
			return
				BGl_sfunzf2Iinfozd2bodyz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3025));
		}

	}



/* sfun/Iinfo-body-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2bodyzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_185, obj_t BgL_vz00_186)
	{
		{	/* Integrate/iinfo.sch 237 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_185)))->BgL_bodyz00) =
				((obj_t) BgL_vz00_186), BUNSPEC);
		}

	}



/* &sfun/Iinfo-body-set! */
	obj_t BGl_z62sfunzf2Iinfozd2bodyzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_3026, obj_t BgL_oz00_3027, obj_t BgL_vz00_3028)
	{
		{	/* Integrate/iinfo.sch 237 */
			return
				BGl_sfunzf2Iinfozd2bodyzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3027), BgL_vz00_3028);
		}

	}



/* sfun/Iinfo-args-name */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2argszd2namezf2zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_187)
	{
		{	/* Integrate/iinfo.sch 238 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_187)))->BgL_argszd2namezd2);
		}

	}



/* &sfun/Iinfo-args-name */
	obj_t BGl_z62sfunzf2Iinfozd2argszd2namez90zzintegrate_infoz00(obj_t
		BgL_envz00_3029, obj_t BgL_oz00_3030)
	{
		{	/* Integrate/iinfo.sch 238 */
			return
				BGl_sfunzf2Iinfozd2argszd2namezf2zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3030));
		}

	}



/* sfun/Iinfo-args */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2argsz20zzintegrate_infoz00(BgL_sfunz00_bglt BgL_oz00_190)
	{
		{	/* Integrate/iinfo.sch 240 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_190)))->BgL_argsz00);
		}

	}



/* &sfun/Iinfo-args */
	obj_t BGl_z62sfunzf2Iinfozd2argsz42zzintegrate_infoz00(obj_t BgL_envz00_3031,
		obj_t BgL_oz00_3032)
	{
		{	/* Integrate/iinfo.sch 240 */
			return
				BGl_sfunzf2Iinfozd2argsz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3032));
		}

	}



/* sfun/Iinfo-args-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2argszd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_191, obj_t BgL_vz00_192)
	{
		{	/* Integrate/iinfo.sch 241 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_191)))->BgL_argsz00) =
				((obj_t) BgL_vz00_192), BUNSPEC);
		}

	}



/* &sfun/Iinfo-args-set! */
	obj_t BGl_z62sfunzf2Iinfozd2argszd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_3033, obj_t BgL_oz00_3034, obj_t BgL_vz00_3035)
	{
		{	/* Integrate/iinfo.sch 241 */
			return
				BGl_sfunzf2Iinfozd2argszd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3034), BgL_vz00_3035);
		}

	}



/* sfun/Iinfo-property */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2propertyz20zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_193)
	{
		{	/* Integrate/iinfo.sch 242 */
			return
				(((BgL_sfunz00_bglt) COBJECT(
						((BgL_sfunz00_bglt) BgL_oz00_193)))->BgL_propertyz00);
		}

	}



/* &sfun/Iinfo-property */
	obj_t BGl_z62sfunzf2Iinfozd2propertyz42zzintegrate_infoz00(obj_t
		BgL_envz00_3036, obj_t BgL_oz00_3037)
	{
		{	/* Integrate/iinfo.sch 242 */
			return
				BGl_sfunzf2Iinfozd2propertyz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3037));
		}

	}



/* sfun/Iinfo-property-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2propertyzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_194, obj_t BgL_vz00_195)
	{
		{	/* Integrate/iinfo.sch 243 */
			return
				((((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_oz00_194)))->BgL_propertyz00) =
				((obj_t) BgL_vz00_195), BUNSPEC);
		}

	}



/* &sfun/Iinfo-property-set! */
	obj_t BGl_z62sfunzf2Iinfozd2propertyzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_3038, obj_t BgL_oz00_3039, obj_t BgL_vz00_3040)
	{
		{	/* Integrate/iinfo.sch 243 */
			return
				BGl_sfunzf2Iinfozd2propertyzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3039), BgL_vz00_3040);
		}

	}



/* sfun/Iinfo-args-retescape */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2argszd2retescapezf2zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_196)
	{
		{	/* Integrate/iinfo.sch 244 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_196)))->BgL_argszd2retescapezd2);
		}

	}



/* &sfun/Iinfo-args-retescape */
	obj_t BGl_z62sfunzf2Iinfozd2argszd2retescapez90zzintegrate_infoz00(obj_t
		BgL_envz00_3041, obj_t BgL_oz00_3042)
	{
		{	/* Integrate/iinfo.sch 244 */
			return
				BGl_sfunzf2Iinfozd2argszd2retescapezf2zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3042));
		}

	}



/* sfun/Iinfo-args-retescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2argszd2retescapezd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt BgL_oz00_197, obj_t BgL_vz00_198)
	{
		{	/* Integrate/iinfo.sch 245 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_197)))->BgL_argszd2retescapezd2) =
				((obj_t) BgL_vz00_198), BUNSPEC);
		}

	}



/* &sfun/Iinfo-args-retescape-set! */
	obj_t
		BGl_z62sfunzf2Iinfozd2argszd2retescapezd2setz12z50zzintegrate_infoz00(obj_t
		BgL_envz00_3043, obj_t BgL_oz00_3044, obj_t BgL_vz00_3045)
	{
		{	/* Integrate/iinfo.sch 245 */
			return
				BGl_sfunzf2Iinfozd2argszd2retescapezd2setz12z32zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3044), BgL_vz00_3045);
		}

	}



/* sfun/Iinfo-args-noescape */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2argszd2noescapezf2zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_199)
	{
		{	/* Integrate/iinfo.sch 246 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_199)))->BgL_argszd2noescapezd2);
		}

	}



/* &sfun/Iinfo-args-noescape */
	obj_t BGl_z62sfunzf2Iinfozd2argszd2noescapez90zzintegrate_infoz00(obj_t
		BgL_envz00_3046, obj_t BgL_oz00_3047)
	{
		{	/* Integrate/iinfo.sch 246 */
			return
				BGl_sfunzf2Iinfozd2argszd2noescapezf2zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3047));
		}

	}



/* sfun/Iinfo-args-noescape-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2argszd2noescapezd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt BgL_oz00_200, obj_t BgL_vz00_201)
	{
		{	/* Integrate/iinfo.sch 247 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_200)))->BgL_argszd2noescapezd2) =
				((obj_t) BgL_vz00_201), BUNSPEC);
		}

	}



/* &sfun/Iinfo-args-noescape-set! */
	obj_t
		BGl_z62sfunzf2Iinfozd2argszd2noescapezd2setz12z50zzintegrate_infoz00(obj_t
		BgL_envz00_3048, obj_t BgL_oz00_3049, obj_t BgL_vz00_3050)
	{
		{	/* Integrate/iinfo.sch 247 */
			return
				BGl_sfunzf2Iinfozd2argszd2noescapezd2setz12z32zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3049), BgL_vz00_3050);
		}

	}



/* sfun/Iinfo-failsafe */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2failsafez20zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_202)
	{
		{	/* Integrate/iinfo.sch 248 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_202)))->BgL_failsafez00);
		}

	}



/* &sfun/Iinfo-failsafe */
	obj_t BGl_z62sfunzf2Iinfozd2failsafez42zzintegrate_infoz00(obj_t
		BgL_envz00_3051, obj_t BgL_oz00_3052)
	{
		{	/* Integrate/iinfo.sch 248 */
			return
				BGl_sfunzf2Iinfozd2failsafez20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3052));
		}

	}



/* sfun/Iinfo-failsafe-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2failsafezd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_203, obj_t BgL_vz00_204)
	{
		{	/* Integrate/iinfo.sch 249 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_203)))->BgL_failsafez00) =
				((obj_t) BgL_vz00_204), BUNSPEC);
		}

	}



/* &sfun/Iinfo-failsafe-set! */
	obj_t BGl_z62sfunzf2Iinfozd2failsafezd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_3053, obj_t BgL_oz00_3054, obj_t BgL_vz00_3055)
	{
		{	/* Integrate/iinfo.sch 249 */
			return
				BGl_sfunzf2Iinfozd2failsafezd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3054), BgL_vz00_3055);
		}

	}



/* sfun/Iinfo-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2effectz20zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_205)
	{
		{	/* Integrate/iinfo.sch 250 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_205)))->BgL_effectz00);
		}

	}



/* &sfun/Iinfo-effect */
	obj_t BGl_z62sfunzf2Iinfozd2effectz42zzintegrate_infoz00(obj_t
		BgL_envz00_3056, obj_t BgL_oz00_3057)
	{
		{	/* Integrate/iinfo.sch 250 */
			return
				BGl_sfunzf2Iinfozd2effectz20zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3057));
		}

	}



/* sfun/Iinfo-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2effectzd2setz12ze0zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_206, obj_t BgL_vz00_207)
	{
		{	/* Integrate/iinfo.sch 251 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_206)))->BgL_effectz00) =
				((obj_t) BgL_vz00_207), BUNSPEC);
		}

	}



/* &sfun/Iinfo-effect-set! */
	obj_t BGl_z62sfunzf2Iinfozd2effectzd2setz12z82zzintegrate_infoz00(obj_t
		BgL_envz00_3058, obj_t BgL_oz00_3059, obj_t BgL_vz00_3060)
	{
		{	/* Integrate/iinfo.sch 251 */
			return
				BGl_sfunzf2Iinfozd2effectzd2setz12ze0zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3059), BgL_vz00_3060);
		}

	}



/* sfun/Iinfo-the-closure */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2thezd2closurezf2zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_208)
	{
		{	/* Integrate/iinfo.sch 252 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_208)))->BgL_thezd2closurezd2);
		}

	}



/* &sfun/Iinfo-the-closure */
	obj_t BGl_z62sfunzf2Iinfozd2thezd2closurez90zzintegrate_infoz00(obj_t
		BgL_envz00_3061, obj_t BgL_oz00_3062)
	{
		{	/* Integrate/iinfo.sch 252 */
			return
				BGl_sfunzf2Iinfozd2thezd2closurezf2zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3062));
		}

	}



/* sfun/Iinfo-the-closure-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2thezd2closurezd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt BgL_oz00_209, obj_t BgL_vz00_210)
	{
		{	/* Integrate/iinfo.sch 253 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_209)))->BgL_thezd2closurezd2) =
				((obj_t) BgL_vz00_210), BUNSPEC);
		}

	}



/* &sfun/Iinfo-the-closure-set! */
	obj_t BGl_z62sfunzf2Iinfozd2thezd2closurezd2setz12z50zzintegrate_infoz00(obj_t
		BgL_envz00_3063, obj_t BgL_oz00_3064, obj_t BgL_vz00_3065)
	{
		{	/* Integrate/iinfo.sch 253 */
			return
				BGl_sfunzf2Iinfozd2thezd2closurezd2setz12z32zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3064), BgL_vz00_3065);
		}

	}



/* sfun/Iinfo-top? */
	BGL_EXPORTED_DEF bool_t
		BGl_sfunzf2Iinfozd2topzf3zd3zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_211)
	{
		{	/* Integrate/iinfo.sch 254 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_211)))->BgL_topzf3zf3);
		}

	}



/* &sfun/Iinfo-top? */
	obj_t BGl_z62sfunzf2Iinfozd2topzf3zb1zzintegrate_infoz00(obj_t
		BgL_envz00_3066, obj_t BgL_oz00_3067)
	{
		{	/* Integrate/iinfo.sch 254 */
			return
				BBOOL(BGl_sfunzf2Iinfozd2topzf3zd3zzintegrate_infoz00(
					((BgL_sfunz00_bglt) BgL_oz00_3067)));
		}

	}



/* sfun/Iinfo-top?-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2topzf3zd2setz12z13zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_212, bool_t BgL_vz00_213)
	{
		{	/* Integrate/iinfo.sch 255 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_212)))->BgL_topzf3zf3) =
				((bool_t) BgL_vz00_213), BUNSPEC);
		}

	}



/* &sfun/Iinfo-top?-set! */
	obj_t BGl_z62sfunzf2Iinfozd2topzf3zd2setz12z71zzintegrate_infoz00(obj_t
		BgL_envz00_3068, obj_t BgL_oz00_3069, obj_t BgL_vz00_3070)
	{
		{	/* Integrate/iinfo.sch 255 */
			return
				BGl_sfunzf2Iinfozd2topzf3zd2setz12z13zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3069), CBOOL(BgL_vz00_3070));
		}

	}



/* sfun/Iinfo-stack-allocator */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2stackzd2allocatorzf2zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_214)
	{
		{	/* Integrate/iinfo.sch 256 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_214)))->BgL_stackzd2allocatorzd2);
		}

	}



/* &sfun/Iinfo-stack-allocator */
	obj_t BGl_z62sfunzf2Iinfozd2stackzd2allocatorz90zzintegrate_infoz00(obj_t
		BgL_envz00_3071, obj_t BgL_oz00_3072)
	{
		{	/* Integrate/iinfo.sch 256 */
			return
				BGl_sfunzf2Iinfozd2stackzd2allocatorzf2zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3072));
		}

	}



/* sfun/Iinfo-stack-allocator-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2stackzd2allocatorzd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt BgL_oz00_215, obj_t BgL_vz00_216)
	{
		{	/* Integrate/iinfo.sch 257 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_215)))->BgL_stackzd2allocatorzd2) =
				((obj_t) BgL_vz00_216), BUNSPEC);
		}

	}



/* &sfun/Iinfo-stack-allocator-set! */
	obj_t
		BGl_z62sfunzf2Iinfozd2stackzd2allocatorzd2setz12z50zzintegrate_infoz00(obj_t
		BgL_envz00_3073, obj_t BgL_oz00_3074, obj_t BgL_vz00_3075)
	{
		{	/* Integrate/iinfo.sch 257 */
			return
				BGl_sfunzf2Iinfozd2stackzd2allocatorzd2setz12z32zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3074), BgL_vz00_3075);
		}

	}



/* sfun/Iinfo-predicate-of */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2predicatezd2ofzf2zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_217)
	{
		{	/* Integrate/iinfo.sch 258 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_217)))->BgL_predicatezd2ofzd2);
		}

	}



/* &sfun/Iinfo-predicate-of */
	obj_t BGl_z62sfunzf2Iinfozd2predicatezd2ofz90zzintegrate_infoz00(obj_t
		BgL_envz00_3076, obj_t BgL_oz00_3077)
	{
		{	/* Integrate/iinfo.sch 258 */
			return
				BGl_sfunzf2Iinfozd2predicatezd2ofzf2zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3077));
		}

	}



/* sfun/Iinfo-predicate-of-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2predicatezd2ofzd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt BgL_oz00_218, obj_t BgL_vz00_219)
	{
		{	/* Integrate/iinfo.sch 259 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_218)))->BgL_predicatezd2ofzd2) =
				((obj_t) BgL_vz00_219), BUNSPEC);
		}

	}



/* &sfun/Iinfo-predicate-of-set! */
	obj_t
		BGl_z62sfunzf2Iinfozd2predicatezd2ofzd2setz12z50zzintegrate_infoz00(obj_t
		BgL_envz00_3078, obj_t BgL_oz00_3079, obj_t BgL_vz00_3080)
	{
		{	/* Integrate/iinfo.sch 259 */
			return
				BGl_sfunzf2Iinfozd2predicatezd2ofzd2setz12z32zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3079), BgL_vz00_3080);
		}

	}



/* sfun/Iinfo-side-effect */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2sidezd2effectzf2zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_220)
	{
		{	/* Integrate/iinfo.sch 260 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_220)))->BgL_sidezd2effectzd2);
		}

	}



/* &sfun/Iinfo-side-effect */
	obj_t BGl_z62sfunzf2Iinfozd2sidezd2effectz90zzintegrate_infoz00(obj_t
		BgL_envz00_3081, obj_t BgL_oz00_3082)
	{
		{	/* Integrate/iinfo.sch 260 */
			return
				BGl_sfunzf2Iinfozd2sidezd2effectzf2zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3082));
		}

	}



/* sfun/Iinfo-side-effect-set! */
	BGL_EXPORTED_DEF obj_t
		BGl_sfunzf2Iinfozd2sidezd2effectzd2setz12z32zzintegrate_infoz00
		(BgL_sfunz00_bglt BgL_oz00_221, obj_t BgL_vz00_222)
	{
		{	/* Integrate/iinfo.sch 261 */
			return
				((((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt) BgL_oz00_221)))->BgL_sidezd2effectzd2) =
				((obj_t) BgL_vz00_222), BUNSPEC);
		}

	}



/* &sfun/Iinfo-side-effect-set! */
	obj_t BGl_z62sfunzf2Iinfozd2sidezd2effectzd2setz12z50zzintegrate_infoz00(obj_t
		BgL_envz00_3083, obj_t BgL_oz00_3084, obj_t BgL_vz00_3085)
	{
		{	/* Integrate/iinfo.sch 261 */
			return
				BGl_sfunzf2Iinfozd2sidezd2effectzd2setz12z32zzintegrate_infoz00(
				((BgL_sfunz00_bglt) BgL_oz00_3084), BgL_vz00_3085);
		}

	}



/* sfun/Iinfo-arity */
	BGL_EXPORTED_DEF long
		BGl_sfunzf2Iinfozd2arityz20zzintegrate_infoz00(BgL_sfunz00_bglt
		BgL_oz00_223)
	{
		{	/* Integrate/iinfo.sch 262 */
			return
				(((BgL_funz00_bglt) COBJECT(
						((BgL_funz00_bglt) BgL_oz00_223)))->BgL_arityz00);
		}

	}



/* &sfun/Iinfo-arity */
	obj_t BGl_z62sfunzf2Iinfozd2arityz42zzintegrate_infoz00(obj_t BgL_envz00_3086,
		obj_t BgL_oz00_3087)
	{
		{	/* Integrate/iinfo.sch 262 */
			return
				BINT(BGl_sfunzf2Iinfozd2arityz20zzintegrate_infoz00(
					((BgL_sfunz00_bglt) BgL_oz00_3087)));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzintegrate_infoz00(void)
	{
		{	/* Integrate/iinfo.scm 15 */
			{	/* Integrate/iinfo.scm 23 */
				obj_t BgL_arg1349z00_1597;
				obj_t BgL_arg1351z00_1598;

				{	/* Integrate/iinfo.scm 23 */
					obj_t BgL_v1321z00_1625;

					BgL_v1321z00_1625 = create_vector(5L);
					{	/* Integrate/iinfo.scm 23 */
						obj_t BgL_arg1376z00_1626;

						BgL_arg1376z00_1626 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(0),
							BGl_proc1977z00zzintegrate_infoz00,
							BGl_proc1976z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1975z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1321z00_1625, 0L, BgL_arg1376z00_1626);
					}
					{	/* Integrate/iinfo.scm 23 */
						obj_t BgL_arg1408z00_1639;

						BgL_arg1408z00_1639 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc1980z00zzintegrate_infoz00,
							BGl_proc1979z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1978z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1321z00_1625, 1L, BgL_arg1408z00_1639);
					}
					{	/* Integrate/iinfo.scm 23 */
						obj_t BgL_arg1434z00_1652;

						BgL_arg1434z00_1652 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(3),
							BGl_proc1983z00zzintegrate_infoz00,
							BGl_proc1982z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1981z00zzintegrate_infoz00, CNST_TABLE_REF(4));
						VECTOR_SET(BgL_v1321z00_1625, 2L, BgL_arg1434z00_1652);
					}
					{	/* Integrate/iinfo.scm 23 */
						obj_t BgL_arg1472z00_1665;

						BgL_arg1472z00_1665 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(5),
							BGl_proc1986z00zzintegrate_infoz00,
							BGl_proc1985z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1984z00zzintegrate_infoz00, CNST_TABLE_REF(4));
						VECTOR_SET(BgL_v1321z00_1625, 3L, BgL_arg1472z00_1665);
					}
					{	/* Integrate/iinfo.scm 23 */
						obj_t BgL_arg1502z00_1678;

						BgL_arg1502z00_1678 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(6),
							BGl_proc1989z00zzintegrate_infoz00,
							BGl_proc1988z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1987z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1321z00_1625, 4L, BgL_arg1502z00_1678);
					}
					BgL_arg1349z00_1597 = BgL_v1321z00_1625;
				}
				{	/* Integrate/iinfo.scm 23 */
					obj_t BgL_v1322z00_1691;

					BgL_v1322z00_1691 = create_vector(0L);
					BgL_arg1351z00_1598 = BgL_v1322z00_1691;
				}
				BGl_svarzf2Iinfozf2zzintegrate_infoz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(7),
					CNST_TABLE_REF(8), BGl_svarz00zzast_varz00, 59340L,
					BGl_proc1993z00zzintegrate_infoz00,
					BGl_proc1992z00zzintegrate_infoz00, BFALSE,
					BGl_proc1991z00zzintegrate_infoz00,
					BGl_proc1990z00zzintegrate_infoz00, BgL_arg1349z00_1597,
					BgL_arg1351z00_1598);
			}
			{	/* Integrate/iinfo.scm 35 */
				obj_t BgL_arg1552z00_1700;
				obj_t BgL_arg1553z00_1701;

				{	/* Integrate/iinfo.scm 35 */
					obj_t BgL_v1323z00_1728;

					BgL_v1323z00_1728 = create_vector(4L);
					{	/* Integrate/iinfo.scm 35 */
						obj_t BgL_arg1576z00_1729;

						BgL_arg1576z00_1729 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(0),
							BGl_proc1996z00zzintegrate_infoz00,
							BGl_proc1995z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1994z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1323z00_1728, 0L, BgL_arg1576z00_1729);
					}
					{	/* Integrate/iinfo.scm 35 */
						obj_t BgL_arg1593z00_1742;

						BgL_arg1593z00_1742 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(2),
							BGl_proc1999z00zzintegrate_infoz00,
							BGl_proc1998z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc1997z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1323z00_1728, 1L, BgL_arg1593z00_1742);
					}
					{	/* Integrate/iinfo.scm 35 */
						obj_t BgL_arg1606z00_1755;

						BgL_arg1606z00_1755 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(3),
							BGl_proc2002z00zzintegrate_infoz00,
							BGl_proc2001z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2000z00zzintegrate_infoz00, CNST_TABLE_REF(4));
						VECTOR_SET(BgL_v1323z00_1728, 2L, BgL_arg1606z00_1755);
					}
					{	/* Integrate/iinfo.scm 35 */
						obj_t BgL_arg1625z00_1768;

						BgL_arg1625z00_1768 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(5),
							BGl_proc2005z00zzintegrate_infoz00,
							BGl_proc2004z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2003z00zzintegrate_infoz00, CNST_TABLE_REF(4));
						VECTOR_SET(BgL_v1323z00_1728, 3L, BgL_arg1625z00_1768);
					}
					BgL_arg1552z00_1700 = BgL_v1323z00_1728;
				}
				{	/* Integrate/iinfo.scm 35 */
					obj_t BgL_v1324z00_1781;

					BgL_v1324z00_1781 = create_vector(0L);
					BgL_arg1553z00_1701 = BgL_v1324z00_1781;
				}
				BGl_sexitzf2Iinfozf2zzintegrate_infoz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(9),
					CNST_TABLE_REF(8), BGl_sexitz00zzast_varz00, 16223L,
					BGl_proc2009z00zzintegrate_infoz00,
					BGl_proc2008z00zzintegrate_infoz00, BFALSE,
					BGl_proc2007z00zzintegrate_infoz00,
					BGl_proc2006z00zzintegrate_infoz00, BgL_arg1552z00_1700,
					BgL_arg1553z00_1701);
			}
			{	/* Integrate/iinfo.scm 45 */
				obj_t BgL_arg1654z00_1790;
				obj_t BgL_arg1661z00_1791;

				{	/* Integrate/iinfo.scm 45 */
					obj_t BgL_v1325z00_1855;

					BgL_v1325z00_1855 = create_vector(21L);
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1691z00_1856;

						BgL_arg1691z00_1856 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(10),
							BGl_proc2011z00zzintegrate_infoz00,
							BGl_proc2010z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 0L, BgL_arg1691z00_1856);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1702z00_1866;

						BgL_arg1702z00_1866 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(11),
							BGl_proc2014z00zzintegrate_infoz00,
							BGl_proc2013z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2012z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 1L, BgL_arg1702z00_1866);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1714z00_1879;

						BgL_arg1714z00_1879 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(12),
							BGl_proc2017z00zzintegrate_infoz00,
							BGl_proc2016z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2015z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 2L, BgL_arg1714z00_1879);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1724z00_1892;

						BgL_arg1724z00_1892 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(13),
							BGl_proc2020z00zzintegrate_infoz00,
							BGl_proc2019z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2018z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 3L, BgL_arg1724z00_1892);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1739z00_1905;

						BgL_arg1739z00_1905 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(14),
							BGl_proc2023z00zzintegrate_infoz00,
							BGl_proc2022z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2021z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 4L, BgL_arg1739z00_1905);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1751z00_1918;

						BgL_arg1751z00_1918 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(15),
							BGl_proc2026z00zzintegrate_infoz00,
							BGl_proc2025z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2024z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 5L, BgL_arg1751z00_1918);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1761z00_1931;

						BgL_arg1761z00_1931 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(16),
							BGl_proc2029z00zzintegrate_infoz00,
							BGl_proc2028z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2027z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 6L, BgL_arg1761z00_1931);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1771z00_1944;

						BgL_arg1771z00_1944 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(17),
							BGl_proc2032z00zzintegrate_infoz00,
							BGl_proc2031z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2030z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 7L, BgL_arg1771z00_1944);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1805z00_1957;

						BgL_arg1805z00_1957 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(18),
							BGl_proc2035z00zzintegrate_infoz00,
							BGl_proc2034z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2033z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 8L, BgL_arg1805z00_1957);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1820z00_1970;

						BgL_arg1820z00_1970 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(19),
							BGl_proc2038z00zzintegrate_infoz00,
							BGl_proc2037z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2036z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 9L, BgL_arg1820z00_1970);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1835z00_1983;

						BgL_arg1835z00_1983 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(20),
							BGl_proc2041z00zzintegrate_infoz00,
							BGl_proc2040z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2039z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 10L, BgL_arg1835z00_1983);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1842z00_1996;

						BgL_arg1842z00_1996 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(21),
							BGl_proc2043z00zzintegrate_infoz00,
							BGl_proc2042z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(4));
						VECTOR_SET(BgL_v1325z00_1855, 11L, BgL_arg1842z00_1996);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1847z00_2006;

						BgL_arg1847z00_2006 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(22),
							BGl_proc2046z00zzintegrate_infoz00,
							BGl_proc2045z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2044z00zzintegrate_infoz00, CNST_TABLE_REF(4));
						VECTOR_SET(BgL_v1325z00_1855, 12L, BgL_arg1847z00_2006);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1854z00_2019;

						BgL_arg1854z00_2019 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(23),
							BGl_proc2049z00zzintegrate_infoz00,
							BGl_proc2048z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2047z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 13L, BgL_arg1854z00_2019);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1862z00_2032;

						BgL_arg1862z00_2032 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(24),
							BGl_proc2052z00zzintegrate_infoz00,
							BGl_proc2051z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2050z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 14L, BgL_arg1862z00_2032);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1870z00_2045;

						BgL_arg1870z00_2045 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(25),
							BGl_proc2055z00zzintegrate_infoz00,
							BGl_proc2054z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2053z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 15L, BgL_arg1870z00_2045);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1878z00_2058;

						BgL_arg1878z00_2058 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(26),
							BGl_proc2058z00zzintegrate_infoz00,
							BGl_proc2057z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2056z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 16L, BgL_arg1878z00_2058);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1887z00_2071;

						BgL_arg1887z00_2071 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(27),
							BGl_proc2061z00zzintegrate_infoz00,
							BGl_proc2060z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2059z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 17L, BgL_arg1887z00_2071);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1894z00_2084;

						BgL_arg1894z00_2084 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(28),
							BGl_proc2064z00zzintegrate_infoz00,
							BGl_proc2063z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2062z00zzintegrate_infoz00, CNST_TABLE_REF(1));
						VECTOR_SET(BgL_v1325z00_1855, 18L, BgL_arg1894z00_2084);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1902z00_2097;

						BgL_arg1902z00_2097 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(29),
							BGl_proc2067z00zzintegrate_infoz00,
							BGl_proc2066z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2065z00zzintegrate_infoz00, CNST_TABLE_REF(4));
						VECTOR_SET(BgL_v1325z00_1855, 19L, BgL_arg1902z00_2097);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1910z00_2110;

						BgL_arg1910z00_2110 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(30),
							BGl_proc2070z00zzintegrate_infoz00,
							BGl_proc2069z00zzintegrate_infoz00, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BGl_proc2068z00zzintegrate_infoz00, CNST_TABLE_REF(31));
						VECTOR_SET(BgL_v1325z00_1855, 20L, BgL_arg1910z00_2110);
					}
					BgL_arg1654z00_1790 = BgL_v1325z00_1855;
				}
				{	/* Integrate/iinfo.scm 45 */
					obj_t BgL_v1326z00_2123;

					BgL_v1326z00_2123 = create_vector(0L);
					BgL_arg1661z00_1791 = BgL_v1326z00_2123;
				}
				return (BGl_sfunzf2Iinfozf2zzintegrate_infoz00 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(32),
						CNST_TABLE_REF(8), BGl_sfunz00zzast_varz00, 11418L,
						BGl_proc2074z00zzintegrate_infoz00,
						BGl_proc2073z00zzintegrate_infoz00, BFALSE,
						BGl_proc2072z00zzintegrate_infoz00,
						BGl_proc2071z00zzintegrate_infoz00, BgL_arg1654z00_1790,
						BgL_arg1661z00_1791), BUNSPEC);
			}
		}

	}



/* &lambda1680 */
	BgL_sfunz00_bglt BGl_z62lambda1680z62zzintegrate_infoz00(obj_t
		BgL_envz00_3188, obj_t BgL_o1175z00_3189)
	{
		{	/* Integrate/iinfo.scm 45 */
			{	/* Integrate/iinfo.scm 45 */
				long BgL_arg1681z00_3517;

				{	/* Integrate/iinfo.scm 45 */
					obj_t BgL_arg1688z00_3518;

					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_arg1689z00_3519;

						{	/* Integrate/iinfo.scm 45 */
							obj_t BgL_arg1815z00_3520;
							long BgL_arg1816z00_3521;

							BgL_arg1815z00_3520 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Integrate/iinfo.scm 45 */
								long BgL_arg1817z00_3522;

								BgL_arg1817z00_3522 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) BgL_o1175z00_3189)));
								BgL_arg1816z00_3521 = (BgL_arg1817z00_3522 - OBJECT_TYPE);
							}
							BgL_arg1689z00_3519 =
								VECTOR_REF(BgL_arg1815z00_3520, BgL_arg1816z00_3521);
						}
						BgL_arg1688z00_3518 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1689z00_3519);
					}
					{	/* Integrate/iinfo.scm 45 */
						obj_t BgL_tmpz00_4996;

						BgL_tmpz00_4996 = ((obj_t) BgL_arg1688z00_3518);
						BgL_arg1681z00_3517 = BGL_CLASS_NUM(BgL_tmpz00_4996);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_sfunz00_bglt) BgL_o1175z00_3189)), BgL_arg1681z00_3517);
			}
			{	/* Integrate/iinfo.scm 45 */
				BgL_objectz00_bglt BgL_tmpz00_5002;

				BgL_tmpz00_5002 =
					((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_o1175z00_3189));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5002, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_o1175z00_3189));
			return ((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1175z00_3189));
		}

	}



/* &<@anonymous:1679> */
	obj_t BGl_z62zc3z04anonymousza31679ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3190, obj_t BgL_new1174z00_3191)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunz00_bglt BgL_auxz00_5010;

				((((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt)
									((BgL_sfunz00_bglt) BgL_new1174z00_3191))))->BgL_arityz00) =
					((long) 0L), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_sidezd2effectzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_predicatezd2ofzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_stackzd2allocatorzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_topzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_thezd2closurezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_effectz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_failsafez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_argszd2noescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_argszd2retescapezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_propertyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_argsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_argszd2namezd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_bodyz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_classz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_dssslzd2keywordszd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_optionalsz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_keysz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_thezd2closurezd2globalz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_strengthz00) =
					((obj_t) CNST_TABLE_REF(33)), BUNSPEC);
				((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt)
										BgL_new1174z00_3191))))->BgL_stackablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5078;

					{
						obj_t BgL_auxz00_5079;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5080;

							BgL_tmpz00_5080 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5079 = BGL_OBJECT_WIDENING(BgL_tmpz00_5080);
						}
						BgL_auxz00_5078 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5079);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5078))->
							BgL_ownerz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5086;

					{
						obj_t BgL_auxz00_5087;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5088;

							BgL_tmpz00_5088 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5087 = BGL_OBJECT_WIDENING(BgL_tmpz00_5088);
						}
						BgL_auxz00_5086 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5087);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5086))->
							BgL_freez00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5094;

					{
						obj_t BgL_auxz00_5095;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5096;

							BgL_tmpz00_5096 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5095 = BGL_OBJECT_WIDENING(BgL_tmpz00_5096);
						}
						BgL_auxz00_5094 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5095);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5094))->
							BgL_boundz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5102;

					{
						obj_t BgL_auxz00_5103;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5104;

							BgL_tmpz00_5104 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5103 = BGL_OBJECT_WIDENING(BgL_tmpz00_5104);
						}
						BgL_auxz00_5102 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5103);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5102))->
							BgL_cfromz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5110;

					{
						obj_t BgL_auxz00_5111;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5112;

							BgL_tmpz00_5112 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5111 = BGL_OBJECT_WIDENING(BgL_tmpz00_5112);
						}
						BgL_auxz00_5110 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5111);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5110))->BgL_ctoz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5118;

					{
						obj_t BgL_auxz00_5119;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5120;

							BgL_tmpz00_5120 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5119 = BGL_OBJECT_WIDENING(BgL_tmpz00_5120);
						}
						BgL_auxz00_5118 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5119);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5118))->BgL_kz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5126;

					{
						obj_t BgL_auxz00_5127;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5128;

							BgL_tmpz00_5128 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5127 = BGL_OBJECT_WIDENING(BgL_tmpz00_5128);
						}
						BgL_auxz00_5126 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5127);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5126))->
							BgL_kza2za2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5134;

					{
						obj_t BgL_auxz00_5135;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5136;

							BgL_tmpz00_5136 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5135 = BGL_OBJECT_WIDENING(BgL_tmpz00_5136);
						}
						BgL_auxz00_5134 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5135);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5134))->BgL_uz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5142;

					{
						obj_t BgL_auxz00_5143;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5144;

							BgL_tmpz00_5144 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5143 = BGL_OBJECT_WIDENING(BgL_tmpz00_5144);
						}
						BgL_auxz00_5142 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5143);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5142))->BgL_cnz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5150;

					{
						obj_t BgL_auxz00_5151;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5152;

							BgL_tmpz00_5152 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5151 = BGL_OBJECT_WIDENING(BgL_tmpz00_5152);
						}
						BgL_auxz00_5150 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5151);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5150))->BgL_ctz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5158;

					{
						obj_t BgL_auxz00_5159;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5160;

							BgL_tmpz00_5160 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5159 = BGL_OBJECT_WIDENING(BgL_tmpz00_5160);
						}
						BgL_auxz00_5158 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5159);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5158))->
							BgL_kontz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5166;

					{
						obj_t BgL_auxz00_5167;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5168;

							BgL_tmpz00_5168 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5167 = BGL_OBJECT_WIDENING(BgL_tmpz00_5168);
						}
						BgL_auxz00_5166 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5167);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5166))->
							BgL_gzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5174;

					{
						obj_t BgL_auxz00_5175;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5176;

							BgL_tmpz00_5176 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5175 = BGL_OBJECT_WIDENING(BgL_tmpz00_5176);
						}
						BgL_auxz00_5174 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5175);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5174))->
							BgL_forcegzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5182;

					{
						obj_t BgL_auxz00_5183;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5184;

							BgL_tmpz00_5184 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5183 = BGL_OBJECT_WIDENING(BgL_tmpz00_5184);
						}
						BgL_auxz00_5182 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5183);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5182))->BgL_lz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5190;

					{
						obj_t BgL_auxz00_5191;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5192;

							BgL_tmpz00_5192 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5191 = BGL_OBJECT_WIDENING(BgL_tmpz00_5192);
						}
						BgL_auxz00_5190 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5191);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5190))->BgL_ledz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5198;

					{
						obj_t BgL_auxz00_5199;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5200;

							BgL_tmpz00_5200 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5199 = BGL_OBJECT_WIDENING(BgL_tmpz00_5200);
						}
						BgL_auxz00_5198 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5199);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5198))->
							BgL_istampz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5206;

					{
						obj_t BgL_auxz00_5207;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5208;

							BgL_tmpz00_5208 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5207 = BGL_OBJECT_WIDENING(BgL_tmpz00_5208);
						}
						BgL_auxz00_5206 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5207);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5206))->
							BgL_globalz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5214;

					{
						obj_t BgL_auxz00_5215;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5216;

							BgL_tmpz00_5216 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5215 = BGL_OBJECT_WIDENING(BgL_tmpz00_5216);
						}
						BgL_auxz00_5214 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5215);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5214))->
							BgL_kapturedz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5222;

					{
						obj_t BgL_auxz00_5223;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5224;

							BgL_tmpz00_5224 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5223 = BGL_OBJECT_WIDENING(BgL_tmpz00_5224);
						}
						BgL_auxz00_5222 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5223);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5222))->
							BgL_tailzd2coercionzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5230;

					{
						obj_t BgL_auxz00_5231;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5232;

							BgL_tmpz00_5232 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5231 = BGL_OBJECT_WIDENING(BgL_tmpz00_5232);
						}
						BgL_auxz00_5230 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5231);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5230))->
							BgL_xhdlzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5238;

					{
						obj_t BgL_auxz00_5239;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5240;

							BgL_tmpz00_5240 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_new1174z00_3191));
							BgL_auxz00_5239 = BGL_OBJECT_WIDENING(BgL_tmpz00_5240);
						}
						BgL_auxz00_5238 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5239);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5238))->
							BgL_xhdlsz00) = ((obj_t) BNIL), BUNSPEC);
				}
				BgL_auxz00_5010 = ((BgL_sfunz00_bglt) BgL_new1174z00_3191);
				return ((obj_t) BgL_auxz00_5010);
			}
		}

	}



/* &lambda1676 */
	BgL_sfunz00_bglt BGl_z62lambda1676z62zzintegrate_infoz00(obj_t
		BgL_envz00_3192, obj_t BgL_o1171z00_3193)
	{
		{	/* Integrate/iinfo.scm 45 */
			{	/* Integrate/iinfo.scm 45 */
				BgL_sfunzf2iinfozf2_bglt BgL_wide1173z00_3525;

				BgL_wide1173z00_3525 =
					((BgL_sfunzf2iinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sfunzf2iinfozf2_bgl))));
				{	/* Integrate/iinfo.scm 45 */
					obj_t BgL_auxz00_5253;
					BgL_objectz00_bglt BgL_tmpz00_5249;

					BgL_auxz00_5253 = ((obj_t) BgL_wide1173z00_3525);
					BgL_tmpz00_5249 =
						((BgL_objectz00_bglt)
						((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1171z00_3193)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5249, BgL_auxz00_5253);
				}
				((BgL_objectz00_bglt)
					((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1171z00_3193)));
				{	/* Integrate/iinfo.scm 45 */
					long BgL_arg1678z00_3526;

					BgL_arg1678z00_3526 =
						BGL_CLASS_NUM(BGl_sfunzf2Iinfozf2zzintegrate_infoz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_sfunz00_bglt)
								((BgL_sfunz00_bglt) BgL_o1171z00_3193))), BgL_arg1678z00_3526);
				}
				return
					((BgL_sfunz00_bglt)
					((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_o1171z00_3193)));
			}
		}

	}



/* &lambda1662 */
	BgL_sfunz00_bglt BGl_z62lambda1662z62zzintegrate_infoz00(obj_t
		BgL_envz00_3194, obj_t BgL_arity1128z00_3195,
		obj_t BgL_sidezd2effect1129zd2_3196, obj_t BgL_predicatezd2of1130zd2_3197,
		obj_t BgL_stackzd2allocator1131zd2_3198, obj_t BgL_topzf31132zf3_3199,
		obj_t BgL_thezd2closure1133zd2_3200, obj_t BgL_effect1134z00_3201,
		obj_t BgL_failsafe1135z00_3202, obj_t BgL_argszd2noescape1136zd2_3203,
		obj_t BgL_argszd2retescape1137zd2_3204, obj_t BgL_property1138z00_3205,
		obj_t BgL_args1139z00_3206, obj_t BgL_argszd2name1140zd2_3207,
		obj_t BgL_body1141z00_3208, obj_t BgL_class1142z00_3209,
		obj_t BgL_dssslzd2keywords1143zd2_3210, obj_t BgL_loc1144z00_3211,
		obj_t BgL_optionals1145z00_3212, obj_t BgL_keys1146z00_3213,
		obj_t BgL_thezd2closurezd2global1147z00_3214,
		obj_t BgL_strength1148z00_3215, obj_t BgL_stackable1149z00_3216,
		obj_t BgL_owner1150z00_3217, obj_t BgL_free1151z00_3218,
		obj_t BgL_bound1152z00_3219, obj_t BgL_cfrom1153z00_3220,
		obj_t BgL_cto1154z00_3221, obj_t BgL_k1155z00_3222,
		obj_t BgL_kza21156za2_3223, obj_t BgL_u1157z00_3224,
		obj_t BgL_cn1158z00_3225, obj_t BgL_ct1159z00_3226,
		obj_t BgL_kont1160z00_3227, obj_t BgL_gzf31161zf3_3228,
		obj_t BgL_forcegzf31162zf3_3229, obj_t BgL_l1163z00_3230,
		obj_t BgL_led1164z00_3231, obj_t BgL_istamp1165z00_3232,
		obj_t BgL_global1166z00_3233, obj_t BgL_kaptured1167z00_3234,
		obj_t BgL_tailzd2coercion1168zd2_3235, obj_t BgL_xhdlzf31169zf3_3236,
		obj_t BgL_xhdls1170z00_3237)
	{
		{	/* Integrate/iinfo.scm 45 */
			{	/* Integrate/iinfo.scm 45 */
				long BgL_arity1128z00_3527;
				bool_t BgL_topzf31132zf3_3528;
				bool_t BgL_gzf31161zf3_3530;
				bool_t BgL_forcegzf31162zf3_3531;
				bool_t BgL_xhdlzf31169zf3_3532;

				BgL_arity1128z00_3527 = (long) CINT(BgL_arity1128z00_3195);
				BgL_topzf31132zf3_3528 = CBOOL(BgL_topzf31132zf3_3199);
				BgL_gzf31161zf3_3530 = CBOOL(BgL_gzf31161zf3_3228);
				BgL_forcegzf31162zf3_3531 = CBOOL(BgL_forcegzf31162zf3_3229);
				BgL_xhdlzf31169zf3_3532 = CBOOL(BgL_xhdlzf31169zf3_3236);
				{	/* Integrate/iinfo.scm 45 */
					BgL_sfunz00_bglt BgL_new1204z00_3534;

					{	/* Integrate/iinfo.scm 45 */
						BgL_sfunz00_bglt BgL_tmp1201z00_3535;
						BgL_sfunzf2iinfozf2_bglt BgL_wide1202z00_3536;

						{
							BgL_sfunz00_bglt BgL_auxz00_5272;

							{	/* Integrate/iinfo.scm 45 */
								BgL_sfunz00_bglt BgL_new1200z00_3537;

								BgL_new1200z00_3537 =
									((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_sfunz00_bgl))));
								{	/* Integrate/iinfo.scm 45 */
									long BgL_arg1675z00_3538;

									BgL_arg1675z00_3538 = BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1200z00_3537),
										BgL_arg1675z00_3538);
								}
								{	/* Integrate/iinfo.scm 45 */
									BgL_objectz00_bglt BgL_tmpz00_5277;

									BgL_tmpz00_5277 = ((BgL_objectz00_bglt) BgL_new1200z00_3537);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5277, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1200z00_3537);
								BgL_auxz00_5272 = BgL_new1200z00_3537;
							}
							BgL_tmp1201z00_3535 = ((BgL_sfunz00_bglt) BgL_auxz00_5272);
						}
						BgL_wide1202z00_3536 =
							((BgL_sfunzf2iinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_sfunzf2iinfozf2_bgl))));
						{	/* Integrate/iinfo.scm 45 */
							obj_t BgL_auxz00_5285;
							BgL_objectz00_bglt BgL_tmpz00_5283;

							BgL_auxz00_5285 = ((obj_t) BgL_wide1202z00_3536);
							BgL_tmpz00_5283 = ((BgL_objectz00_bglt) BgL_tmp1201z00_3535);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5283, BgL_auxz00_5285);
						}
						((BgL_objectz00_bglt) BgL_tmp1201z00_3535);
						{	/* Integrate/iinfo.scm 45 */
							long BgL_arg1663z00_3539;

							BgL_arg1663z00_3539 =
								BGL_CLASS_NUM(BGl_sfunzf2Iinfozf2zzintegrate_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1201z00_3535),
								BgL_arg1663z00_3539);
						}
						BgL_new1204z00_3534 = ((BgL_sfunz00_bglt) BgL_tmp1201z00_3535);
					}
					((((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt) BgL_new1204z00_3534)))->BgL_arityz00) =
						((long) BgL_arity1128z00_3527), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1204z00_3534)))->BgL_sidezd2effectzd2) =
						((obj_t) BgL_sidezd2effect1129zd2_3196), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1204z00_3534)))->BgL_predicatezd2ofzd2) =
						((obj_t) BgL_predicatezd2of1130zd2_3197), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1204z00_3534)))->BgL_stackzd2allocatorzd2) =
						((obj_t) BgL_stackzd2allocator1131zd2_3198), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1204z00_3534)))->BgL_topzf3zf3) =
						((bool_t) BgL_topzf31132zf3_3528), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1204z00_3534)))->BgL_thezd2closurezd2) =
						((obj_t) BgL_thezd2closure1133zd2_3200), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1204z00_3534)))->BgL_effectz00) =
						((obj_t) BgL_effect1134z00_3201), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1204z00_3534)))->BgL_failsafez00) =
						((obj_t) BgL_failsafe1135z00_3202), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1204z00_3534)))->BgL_argszd2noescapezd2) =
						((obj_t) BgL_argszd2noescape1136zd2_3203), BUNSPEC);
					((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
										BgL_new1204z00_3534)))->BgL_argszd2retescapezd2) =
						((obj_t) BgL_argszd2retescape1137zd2_3204), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1204z00_3534)))->BgL_propertyz00) =
						((obj_t) BgL_property1138z00_3205), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1204z00_3534)))->BgL_argsz00) =
						((obj_t) BgL_args1139z00_3206), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1204z00_3534)))->BgL_argszd2namezd2) =
						((obj_t) BgL_argszd2name1140zd2_3207), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1204z00_3534)))->BgL_bodyz00) =
						((obj_t) BgL_body1141z00_3208), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1204z00_3534)))->BgL_classz00) =
						((obj_t) BgL_class1142z00_3209), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1204z00_3534)))->BgL_dssslzd2keywordszd2) =
						((obj_t) BgL_dssslzd2keywords1143zd2_3210), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1204z00_3534)))->BgL_locz00) =
						((obj_t) BgL_loc1144z00_3211), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1204z00_3534)))->BgL_optionalsz00) =
						((obj_t) BgL_optionals1145z00_3212), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1204z00_3534)))->BgL_keysz00) =
						((obj_t) BgL_keys1146z00_3213), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1204z00_3534)))->BgL_thezd2closurezd2globalz00) =
						((obj_t) BgL_thezd2closurezd2global1147z00_3214), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1204z00_3534)))->BgL_strengthz00) =
						((obj_t) ((obj_t) BgL_strength1148z00_3215)), BUNSPEC);
					((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
										BgL_new1204z00_3534)))->BgL_stackablez00) =
						((obj_t) BgL_stackable1149z00_3216), BUNSPEC);
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5338;

						{
							obj_t BgL_auxz00_5339;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5340;

								BgL_tmpz00_5340 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5339 = BGL_OBJECT_WIDENING(BgL_tmpz00_5340);
							}
							BgL_auxz00_5338 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5339);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5338))->
								BgL_ownerz00) = ((obj_t) BgL_owner1150z00_3217), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5345;

						{
							obj_t BgL_auxz00_5346;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5347;

								BgL_tmpz00_5347 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5346 = BGL_OBJECT_WIDENING(BgL_tmpz00_5347);
							}
							BgL_auxz00_5345 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5346);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5345))->
								BgL_freez00) = ((obj_t) BgL_free1151z00_3218), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5352;

						{
							obj_t BgL_auxz00_5353;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5354;

								BgL_tmpz00_5354 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5353 = BGL_OBJECT_WIDENING(BgL_tmpz00_5354);
							}
							BgL_auxz00_5352 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5353);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5352))->
								BgL_boundz00) = ((obj_t) BgL_bound1152z00_3219), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5359;

						{
							obj_t BgL_auxz00_5360;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5361;

								BgL_tmpz00_5361 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5360 = BGL_OBJECT_WIDENING(BgL_tmpz00_5361);
							}
							BgL_auxz00_5359 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5360);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5359))->
								BgL_cfromz00) = ((obj_t) BgL_cfrom1153z00_3220), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5366;

						{
							obj_t BgL_auxz00_5367;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5368;

								BgL_tmpz00_5368 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5367 = BGL_OBJECT_WIDENING(BgL_tmpz00_5368);
							}
							BgL_auxz00_5366 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5367);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5366))->
								BgL_ctoz00) = ((obj_t) BgL_cto1154z00_3221), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5373;

						{
							obj_t BgL_auxz00_5374;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5375;

								BgL_tmpz00_5375 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5374 = BGL_OBJECT_WIDENING(BgL_tmpz00_5375);
							}
							BgL_auxz00_5373 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5374);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5373))->BgL_kz00) =
							((obj_t) BgL_k1155z00_3222), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5380;

						{
							obj_t BgL_auxz00_5381;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5382;

								BgL_tmpz00_5382 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5381 = BGL_OBJECT_WIDENING(BgL_tmpz00_5382);
							}
							BgL_auxz00_5380 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5381);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5380))->
								BgL_kza2za2) = ((obj_t) BgL_kza21156za2_3223), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5387;

						{
							obj_t BgL_auxz00_5388;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5389;

								BgL_tmpz00_5389 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5388 = BGL_OBJECT_WIDENING(BgL_tmpz00_5389);
							}
							BgL_auxz00_5387 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5388);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5387))->BgL_uz00) =
							((obj_t) BgL_u1157z00_3224), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5394;

						{
							obj_t BgL_auxz00_5395;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5396;

								BgL_tmpz00_5396 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5395 = BGL_OBJECT_WIDENING(BgL_tmpz00_5396);
							}
							BgL_auxz00_5394 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5395);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5394))->
								BgL_cnz00) = ((obj_t) BgL_cn1158z00_3225), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5401;

						{
							obj_t BgL_auxz00_5402;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5403;

								BgL_tmpz00_5403 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5402 = BGL_OBJECT_WIDENING(BgL_tmpz00_5403);
							}
							BgL_auxz00_5401 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5402);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5401))->
								BgL_ctz00) = ((obj_t) BgL_ct1159z00_3226), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5408;

						{
							obj_t BgL_auxz00_5409;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5410;

								BgL_tmpz00_5410 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5409 = BGL_OBJECT_WIDENING(BgL_tmpz00_5410);
							}
							BgL_auxz00_5408 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5409);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5408))->
								BgL_kontz00) = ((obj_t) BgL_kont1160z00_3227), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5415;

						{
							obj_t BgL_auxz00_5416;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5417;

								BgL_tmpz00_5417 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5416 = BGL_OBJECT_WIDENING(BgL_tmpz00_5417);
							}
							BgL_auxz00_5415 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5416);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5415))->
								BgL_gzf3zf3) = ((bool_t) BgL_gzf31161zf3_3530), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5422;

						{
							obj_t BgL_auxz00_5423;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5424;

								BgL_tmpz00_5424 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5423 = BGL_OBJECT_WIDENING(BgL_tmpz00_5424);
							}
							BgL_auxz00_5422 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5423);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5422))->
								BgL_forcegzf3zf3) =
							((bool_t) BgL_forcegzf31162zf3_3531), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5429;

						{
							obj_t BgL_auxz00_5430;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5431;

								BgL_tmpz00_5431 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5430 = BGL_OBJECT_WIDENING(BgL_tmpz00_5431);
							}
							BgL_auxz00_5429 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5430);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5429))->BgL_lz00) =
							((obj_t) BgL_l1163z00_3230), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5436;

						{
							obj_t BgL_auxz00_5437;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5438;

								BgL_tmpz00_5438 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5437 = BGL_OBJECT_WIDENING(BgL_tmpz00_5438);
							}
							BgL_auxz00_5436 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5437);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5436))->
								BgL_ledz00) = ((obj_t) BgL_led1164z00_3231), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5443;

						{
							obj_t BgL_auxz00_5444;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5445;

								BgL_tmpz00_5445 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5444 = BGL_OBJECT_WIDENING(BgL_tmpz00_5445);
							}
							BgL_auxz00_5443 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5444);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5443))->
								BgL_istampz00) = ((obj_t) BgL_istamp1165z00_3232), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5450;

						{
							obj_t BgL_auxz00_5451;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5452;

								BgL_tmpz00_5452 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5451 = BGL_OBJECT_WIDENING(BgL_tmpz00_5452);
							}
							BgL_auxz00_5450 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5451);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5450))->
								BgL_globalz00) = ((obj_t) BgL_global1166z00_3233), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5457;

						{
							obj_t BgL_auxz00_5458;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5459;

								BgL_tmpz00_5459 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5458 = BGL_OBJECT_WIDENING(BgL_tmpz00_5459);
							}
							BgL_auxz00_5457 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5458);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5457))->
								BgL_kapturedz00) = ((obj_t) BgL_kaptured1167z00_3234), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5464;

						{
							obj_t BgL_auxz00_5465;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5466;

								BgL_tmpz00_5466 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5465 = BGL_OBJECT_WIDENING(BgL_tmpz00_5466);
							}
							BgL_auxz00_5464 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5465);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5464))->
								BgL_tailzd2coercionzd2) =
							((obj_t) BgL_tailzd2coercion1168zd2_3235), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5471;

						{
							obj_t BgL_auxz00_5472;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5473;

								BgL_tmpz00_5473 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5472 = BGL_OBJECT_WIDENING(BgL_tmpz00_5473);
							}
							BgL_auxz00_5471 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5472);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5471))->
								BgL_xhdlzf3zf3) = ((bool_t) BgL_xhdlzf31169zf3_3532), BUNSPEC);
					}
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5478;

						{
							obj_t BgL_auxz00_5479;

							{	/* Integrate/iinfo.scm 45 */
								BgL_objectz00_bglt BgL_tmpz00_5480;

								BgL_tmpz00_5480 = ((BgL_objectz00_bglt) BgL_new1204z00_3534);
								BgL_auxz00_5479 = BGL_OBJECT_WIDENING(BgL_tmpz00_5480);
							}
							BgL_auxz00_5478 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5479);
						}
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5478))->
								BgL_xhdlsz00) =
							((obj_t) ((obj_t) BgL_xhdls1170z00_3237)), BUNSPEC);
					}
					return BgL_new1204z00_3534;
				}
			}
		}

	}



/* &<@anonymous:1916> */
	obj_t BGl_z62zc3z04anonymousza31916ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3238)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BNIL;
		}

	}



/* &lambda1915 */
	obj_t BGl_z62lambda1915z62zzintegrate_infoz00(obj_t BgL_envz00_3239,
		obj_t BgL_oz00_3240, obj_t BgL_vz00_3241)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5486;

				{
					obj_t BgL_auxz00_5487;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5488;

						BgL_tmpz00_5488 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3240));
						BgL_auxz00_5487 = BGL_OBJECT_WIDENING(BgL_tmpz00_5488);
					}
					BgL_auxz00_5486 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5487);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5486))->
						BgL_xhdlsz00) = ((obj_t) ((obj_t) BgL_vz00_3241)), BUNSPEC);
			}
		}

	}



/* &lambda1914 */
	obj_t BGl_z62lambda1914z62zzintegrate_infoz00(obj_t BgL_envz00_3242,
		obj_t BgL_oz00_3243)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5495;

				{
					obj_t BgL_auxz00_5496;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5497;

						BgL_tmpz00_5497 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3243));
						BgL_auxz00_5496 = BGL_OBJECT_WIDENING(BgL_tmpz00_5497);
					}
					BgL_auxz00_5495 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5496);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5495))->BgL_xhdlsz00);
			}
		}

	}



/* &<@anonymous:1909> */
	obj_t BGl_z62zc3z04anonymousza31909ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3244)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1908 */
	obj_t BGl_z62lambda1908z62zzintegrate_infoz00(obj_t BgL_envz00_3245,
		obj_t BgL_oz00_3246, obj_t BgL_vz00_3247)
	{
		{	/* Integrate/iinfo.scm 45 */
			{	/* Integrate/iinfo.scm 45 */
				bool_t BgL_vz00_3544;

				BgL_vz00_3544 = CBOOL(BgL_vz00_3247);
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5505;

					{
						obj_t BgL_auxz00_5506;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5507;

							BgL_tmpz00_5507 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3246));
							BgL_auxz00_5506 = BGL_OBJECT_WIDENING(BgL_tmpz00_5507);
						}
						BgL_auxz00_5505 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5506);
					}
					return
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5505))->
							BgL_xhdlzf3zf3) = ((bool_t) BgL_vz00_3544), BUNSPEC);
				}
			}
		}

	}



/* &lambda1907 */
	obj_t BGl_z62lambda1907z62zzintegrate_infoz00(obj_t BgL_envz00_3248,
		obj_t BgL_oz00_3249)
	{
		{	/* Integrate/iinfo.scm 45 */
			{	/* Integrate/iinfo.scm 45 */
				bool_t BgL_tmpz00_5513;

				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5514;

					{
						obj_t BgL_auxz00_5515;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5516;

							BgL_tmpz00_5516 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3249));
							BgL_auxz00_5515 = BGL_OBJECT_WIDENING(BgL_tmpz00_5516);
						}
						BgL_auxz00_5514 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5515);
					}
					BgL_tmpz00_5513 =
						(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5514))->
						BgL_xhdlzf3zf3);
				}
				return BBOOL(BgL_tmpz00_5513);
			}
		}

	}



/* &<@anonymous:1901> */
	obj_t BGl_z62zc3z04anonymousza31901ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3250)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BUNSPEC;
		}

	}



/* &lambda1900 */
	obj_t BGl_z62lambda1900z62zzintegrate_infoz00(obj_t BgL_envz00_3251,
		obj_t BgL_oz00_3252, obj_t BgL_vz00_3253)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5523;

				{
					obj_t BgL_auxz00_5524;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5525;

						BgL_tmpz00_5525 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3252));
						BgL_auxz00_5524 = BGL_OBJECT_WIDENING(BgL_tmpz00_5525);
					}
					BgL_auxz00_5523 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5524);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5523))->
						BgL_tailzd2coercionzd2) = ((obj_t) BgL_vz00_3253), BUNSPEC);
			}
		}

	}



/* &lambda1899 */
	obj_t BGl_z62lambda1899z62zzintegrate_infoz00(obj_t BgL_envz00_3254,
		obj_t BgL_oz00_3255)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5531;

				{
					obj_t BgL_auxz00_5532;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5533;

						BgL_tmpz00_5533 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3255));
						BgL_auxz00_5532 = BGL_OBJECT_WIDENING(BgL_tmpz00_5533);
					}
					BgL_auxz00_5531 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5532);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5531))->
					BgL_tailzd2coercionzd2);
			}
		}

	}



/* &<@anonymous:1893> */
	obj_t BGl_z62zc3z04anonymousza31893ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3256)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BUNSPEC;
		}

	}



/* &lambda1892 */
	obj_t BGl_z62lambda1892z62zzintegrate_infoz00(obj_t BgL_envz00_3257,
		obj_t BgL_oz00_3258, obj_t BgL_vz00_3259)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5539;

				{
					obj_t BgL_auxz00_5540;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5541;

						BgL_tmpz00_5541 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3258));
						BgL_auxz00_5540 = BGL_OBJECT_WIDENING(BgL_tmpz00_5541);
					}
					BgL_auxz00_5539 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5540);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5539))->
						BgL_kapturedz00) = ((obj_t) BgL_vz00_3259), BUNSPEC);
			}
		}

	}



/* &lambda1891 */
	obj_t BGl_z62lambda1891z62zzintegrate_infoz00(obj_t BgL_envz00_3260,
		obj_t BgL_oz00_3261)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5547;

				{
					obj_t BgL_auxz00_5548;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5549;

						BgL_tmpz00_5549 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3261));
						BgL_auxz00_5548 = BGL_OBJECT_WIDENING(BgL_tmpz00_5549);
					}
					BgL_auxz00_5547 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5548);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5547))->
					BgL_kapturedz00);
			}
		}

	}



/* &<@anonymous:1885> */
	obj_t BGl_z62zc3z04anonymousza31885ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3262)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BUNSPEC;
		}

	}



/* &lambda1884 */
	obj_t BGl_z62lambda1884z62zzintegrate_infoz00(obj_t BgL_envz00_3263,
		obj_t BgL_oz00_3264, obj_t BgL_vz00_3265)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5555;

				{
					obj_t BgL_auxz00_5556;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5557;

						BgL_tmpz00_5557 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3264));
						BgL_auxz00_5556 = BGL_OBJECT_WIDENING(BgL_tmpz00_5557);
					}
					BgL_auxz00_5555 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5556);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5555))->
						BgL_globalz00) = ((obj_t) BgL_vz00_3265), BUNSPEC);
			}
		}

	}



/* &lambda1883 */
	obj_t BGl_z62lambda1883z62zzintegrate_infoz00(obj_t BgL_envz00_3266,
		obj_t BgL_oz00_3267)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5563;

				{
					obj_t BgL_auxz00_5564;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5565;

						BgL_tmpz00_5565 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3267));
						BgL_auxz00_5564 = BGL_OBJECT_WIDENING(BgL_tmpz00_5565);
					}
					BgL_auxz00_5563 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5564);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5563))->
					BgL_globalz00);
			}
		}

	}



/* &<@anonymous:1877> */
	obj_t BGl_z62zc3z04anonymousza31877ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3268)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BUNSPEC;
		}

	}



/* &lambda1876 */
	obj_t BGl_z62lambda1876z62zzintegrate_infoz00(obj_t BgL_envz00_3269,
		obj_t BgL_oz00_3270, obj_t BgL_vz00_3271)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5571;

				{
					obj_t BgL_auxz00_5572;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5573;

						BgL_tmpz00_5573 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3270));
						BgL_auxz00_5572 = BGL_OBJECT_WIDENING(BgL_tmpz00_5573);
					}
					BgL_auxz00_5571 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5572);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5571))->
						BgL_istampz00) = ((obj_t) BgL_vz00_3271), BUNSPEC);
			}
		}

	}



/* &lambda1875 */
	obj_t BGl_z62lambda1875z62zzintegrate_infoz00(obj_t BgL_envz00_3272,
		obj_t BgL_oz00_3273)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5579;

				{
					obj_t BgL_auxz00_5580;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5581;

						BgL_tmpz00_5581 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3273));
						BgL_auxz00_5580 = BGL_OBJECT_WIDENING(BgL_tmpz00_5581);
					}
					BgL_auxz00_5579 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5580);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5579))->
					BgL_istampz00);
			}
		}

	}



/* &<@anonymous:1869> */
	obj_t BGl_z62zc3z04anonymousza31869ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3274)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BNIL;
		}

	}



/* &lambda1868 */
	obj_t BGl_z62lambda1868z62zzintegrate_infoz00(obj_t BgL_envz00_3275,
		obj_t BgL_oz00_3276, obj_t BgL_vz00_3277)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5587;

				{
					obj_t BgL_auxz00_5588;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5589;

						BgL_tmpz00_5589 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3276));
						BgL_auxz00_5588 = BGL_OBJECT_WIDENING(BgL_tmpz00_5589);
					}
					BgL_auxz00_5587 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5588);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5587))->BgL_ledz00) =
					((obj_t) BgL_vz00_3277), BUNSPEC);
			}
		}

	}



/* &lambda1867 */
	obj_t BGl_z62lambda1867z62zzintegrate_infoz00(obj_t BgL_envz00_3278,
		obj_t BgL_oz00_3279)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5595;

				{
					obj_t BgL_auxz00_5596;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5597;

						BgL_tmpz00_5597 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3279));
						BgL_auxz00_5596 = BGL_OBJECT_WIDENING(BgL_tmpz00_5597);
					}
					BgL_auxz00_5595 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5596);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5595))->BgL_ledz00);
			}
		}

	}



/* &<@anonymous:1861> */
	obj_t BGl_z62zc3z04anonymousza31861ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3280)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BUNSPEC;
		}

	}



/* &lambda1860 */
	obj_t BGl_z62lambda1860z62zzintegrate_infoz00(obj_t BgL_envz00_3281,
		obj_t BgL_oz00_3282, obj_t BgL_vz00_3283)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5603;

				{
					obj_t BgL_auxz00_5604;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5605;

						BgL_tmpz00_5605 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3282));
						BgL_auxz00_5604 = BGL_OBJECT_WIDENING(BgL_tmpz00_5605);
					}
					BgL_auxz00_5603 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5604);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5603))->BgL_lz00) =
					((obj_t) BgL_vz00_3283), BUNSPEC);
			}
		}

	}



/* &lambda1859 */
	obj_t BGl_z62lambda1859z62zzintegrate_infoz00(obj_t BgL_envz00_3284,
		obj_t BgL_oz00_3285)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5611;

				{
					obj_t BgL_auxz00_5612;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5613;

						BgL_tmpz00_5613 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3285));
						BgL_auxz00_5612 = BGL_OBJECT_WIDENING(BgL_tmpz00_5613);
					}
					BgL_auxz00_5611 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5612);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5611))->BgL_lz00);
			}
		}

	}



/* &<@anonymous:1853> */
	obj_t BGl_z62zc3z04anonymousza31853ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3286)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1852 */
	obj_t BGl_z62lambda1852z62zzintegrate_infoz00(obj_t BgL_envz00_3287,
		obj_t BgL_oz00_3288, obj_t BgL_vz00_3289)
	{
		{	/* Integrate/iinfo.scm 45 */
			{	/* Integrate/iinfo.scm 45 */
				bool_t BgL_vz00_3559;

				BgL_vz00_3559 = CBOOL(BgL_vz00_3289);
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5621;

					{
						obj_t BgL_auxz00_5622;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5623;

							BgL_tmpz00_5623 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3288));
							BgL_auxz00_5622 = BGL_OBJECT_WIDENING(BgL_tmpz00_5623);
						}
						BgL_auxz00_5621 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5622);
					}
					return
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5621))->
							BgL_forcegzf3zf3) = ((bool_t) BgL_vz00_3559), BUNSPEC);
				}
			}
		}

	}



/* &lambda1851 */
	obj_t BGl_z62lambda1851z62zzintegrate_infoz00(obj_t BgL_envz00_3290,
		obj_t BgL_oz00_3291)
	{
		{	/* Integrate/iinfo.scm 45 */
			{	/* Integrate/iinfo.scm 45 */
				bool_t BgL_tmpz00_5629;

				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5630;

					{
						obj_t BgL_auxz00_5631;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5632;

							BgL_tmpz00_5632 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3291));
							BgL_auxz00_5631 = BGL_OBJECT_WIDENING(BgL_tmpz00_5632);
						}
						BgL_auxz00_5630 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5631);
					}
					BgL_tmpz00_5629 =
						(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5630))->
						BgL_forcegzf3zf3);
				}
				return BBOOL(BgL_tmpz00_5629);
			}
		}

	}



/* &lambda1846 */
	obj_t BGl_z62lambda1846z62zzintegrate_infoz00(obj_t BgL_envz00_3292,
		obj_t BgL_oz00_3293, obj_t BgL_vz00_3294)
	{
		{	/* Integrate/iinfo.scm 45 */
			{	/* Integrate/iinfo.scm 45 */
				bool_t BgL_vz00_3562;

				BgL_vz00_3562 = CBOOL(BgL_vz00_3294);
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5640;

					{
						obj_t BgL_auxz00_5641;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5642;

							BgL_tmpz00_5642 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3293));
							BgL_auxz00_5641 = BGL_OBJECT_WIDENING(BgL_tmpz00_5642);
						}
						BgL_auxz00_5640 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5641);
					}
					return
						((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5640))->
							BgL_gzf3zf3) = ((bool_t) BgL_vz00_3562), BUNSPEC);
				}
			}
		}

	}



/* &lambda1845 */
	obj_t BGl_z62lambda1845z62zzintegrate_infoz00(obj_t BgL_envz00_3295,
		obj_t BgL_oz00_3296)
	{
		{	/* Integrate/iinfo.scm 45 */
			{	/* Integrate/iinfo.scm 45 */
				bool_t BgL_tmpz00_5648;

				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5649;

					{
						obj_t BgL_auxz00_5650;

						{	/* Integrate/iinfo.scm 45 */
							BgL_objectz00_bglt BgL_tmpz00_5651;

							BgL_tmpz00_5651 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3296));
							BgL_auxz00_5650 = BGL_OBJECT_WIDENING(BgL_tmpz00_5651);
						}
						BgL_auxz00_5649 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5650);
					}
					BgL_tmpz00_5648 =
						(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5649))->
						BgL_gzf3zf3);
				}
				return BBOOL(BgL_tmpz00_5648);
			}
		}

	}



/* &<@anonymous:1841> */
	obj_t BGl_z62zc3z04anonymousza31841ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3297)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BNIL;
		}

	}



/* &lambda1840 */
	obj_t BGl_z62lambda1840z62zzintegrate_infoz00(obj_t BgL_envz00_3298,
		obj_t BgL_oz00_3299, obj_t BgL_vz00_3300)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5658;

				{
					obj_t BgL_auxz00_5659;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5660;

						BgL_tmpz00_5660 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3299));
						BgL_auxz00_5659 = BGL_OBJECT_WIDENING(BgL_tmpz00_5660);
					}
					BgL_auxz00_5658 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5659);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5658))->
						BgL_kontz00) = ((obj_t) BgL_vz00_3300), BUNSPEC);
			}
		}

	}



/* &lambda1839 */
	obj_t BGl_z62lambda1839z62zzintegrate_infoz00(obj_t BgL_envz00_3301,
		obj_t BgL_oz00_3302)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5666;

				{
					obj_t BgL_auxz00_5667;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5668;

						BgL_tmpz00_5668 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3302));
						BgL_auxz00_5667 = BGL_OBJECT_WIDENING(BgL_tmpz00_5668);
					}
					BgL_auxz00_5666 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5667);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5666))->BgL_kontz00);
			}
		}

	}



/* &<@anonymous:1834> */
	obj_t BGl_z62zc3z04anonymousza31834ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3303)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BNIL;
		}

	}



/* &lambda1833 */
	obj_t BGl_z62lambda1833z62zzintegrate_infoz00(obj_t BgL_envz00_3304,
		obj_t BgL_oz00_3305, obj_t BgL_vz00_3306)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5674;

				{
					obj_t BgL_auxz00_5675;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5676;

						BgL_tmpz00_5676 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3305));
						BgL_auxz00_5675 = BGL_OBJECT_WIDENING(BgL_tmpz00_5676);
					}
					BgL_auxz00_5674 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5675);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5674))->BgL_ctz00) =
					((obj_t) BgL_vz00_3306), BUNSPEC);
			}
		}

	}



/* &lambda1832 */
	obj_t BGl_z62lambda1832z62zzintegrate_infoz00(obj_t BgL_envz00_3307,
		obj_t BgL_oz00_3308)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5682;

				{
					obj_t BgL_auxz00_5683;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5684;

						BgL_tmpz00_5684 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3308));
						BgL_auxz00_5683 = BGL_OBJECT_WIDENING(BgL_tmpz00_5684);
					}
					BgL_auxz00_5682 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5683);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5682))->BgL_ctz00);
			}
		}

	}



/* &<@anonymous:1815> */
	obj_t BGl_z62zc3z04anonymousza31815ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3309)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BNIL;
		}

	}



/* &lambda1814 */
	obj_t BGl_z62lambda1814z62zzintegrate_infoz00(obj_t BgL_envz00_3310,
		obj_t BgL_oz00_3311, obj_t BgL_vz00_3312)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5690;

				{
					obj_t BgL_auxz00_5691;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5692;

						BgL_tmpz00_5692 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3311));
						BgL_auxz00_5691 = BGL_OBJECT_WIDENING(BgL_tmpz00_5692);
					}
					BgL_auxz00_5690 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5691);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5690))->BgL_cnz00) =
					((obj_t) BgL_vz00_3312), BUNSPEC);
			}
		}

	}



/* &lambda1813 */
	obj_t BGl_z62lambda1813z62zzintegrate_infoz00(obj_t BgL_envz00_3313,
		obj_t BgL_oz00_3314)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5698;

				{
					obj_t BgL_auxz00_5699;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5700;

						BgL_tmpz00_5700 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3314));
						BgL_auxz00_5699 = BGL_OBJECT_WIDENING(BgL_tmpz00_5700);
					}
					BgL_auxz00_5698 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5699);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5698))->BgL_cnz00);
			}
		}

	}



/* &<@anonymous:1801> */
	obj_t BGl_z62zc3z04anonymousza31801ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3315)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BUNSPEC;
		}

	}



/* &lambda1800 */
	obj_t BGl_z62lambda1800z62zzintegrate_infoz00(obj_t BgL_envz00_3316,
		obj_t BgL_oz00_3317, obj_t BgL_vz00_3318)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5706;

				{
					obj_t BgL_auxz00_5707;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5708;

						BgL_tmpz00_5708 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3317));
						BgL_auxz00_5707 = BGL_OBJECT_WIDENING(BgL_tmpz00_5708);
					}
					BgL_auxz00_5706 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5707);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5706))->BgL_uz00) =
					((obj_t) BgL_vz00_3318), BUNSPEC);
			}
		}

	}



/* &lambda1799 */
	obj_t BGl_z62lambda1799z62zzintegrate_infoz00(obj_t BgL_envz00_3319,
		obj_t BgL_oz00_3320)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5714;

				{
					obj_t BgL_auxz00_5715;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5716;

						BgL_tmpz00_5716 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3320));
						BgL_auxz00_5715 = BGL_OBJECT_WIDENING(BgL_tmpz00_5716);
					}
					BgL_auxz00_5714 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5715);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5714))->BgL_uz00);
			}
		}

	}



/* &<@anonymous:1770> */
	obj_t BGl_z62zc3z04anonymousza31770ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3321)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BNIL;
		}

	}



/* &lambda1769 */
	obj_t BGl_z62lambda1769z62zzintegrate_infoz00(obj_t BgL_envz00_3322,
		obj_t BgL_oz00_3323, obj_t BgL_vz00_3324)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5722;

				{
					obj_t BgL_auxz00_5723;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5724;

						BgL_tmpz00_5724 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3323));
						BgL_auxz00_5723 = BGL_OBJECT_WIDENING(BgL_tmpz00_5724);
					}
					BgL_auxz00_5722 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5723);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5722))->
						BgL_kza2za2) = ((obj_t) BgL_vz00_3324), BUNSPEC);
			}
		}

	}



/* &lambda1768 */
	obj_t BGl_z62lambda1768z62zzintegrate_infoz00(obj_t BgL_envz00_3325,
		obj_t BgL_oz00_3326)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5730;

				{
					obj_t BgL_auxz00_5731;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5732;

						BgL_tmpz00_5732 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3326));
						BgL_auxz00_5731 = BGL_OBJECT_WIDENING(BgL_tmpz00_5732);
					}
					BgL_auxz00_5730 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5731);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5730))->BgL_kza2za2);
			}
		}

	}



/* &<@anonymous:1757> */
	obj_t BGl_z62zc3z04anonymousza31757ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3327)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BNIL;
		}

	}



/* &lambda1756 */
	obj_t BGl_z62lambda1756z62zzintegrate_infoz00(obj_t BgL_envz00_3328,
		obj_t BgL_oz00_3329, obj_t BgL_vz00_3330)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5738;

				{
					obj_t BgL_auxz00_5739;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5740;

						BgL_tmpz00_5740 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3329));
						BgL_auxz00_5739 = BGL_OBJECT_WIDENING(BgL_tmpz00_5740);
					}
					BgL_auxz00_5738 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5739);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5738))->BgL_kz00) =
					((obj_t) BgL_vz00_3330), BUNSPEC);
			}
		}

	}



/* &lambda1755 */
	obj_t BGl_z62lambda1755z62zzintegrate_infoz00(obj_t BgL_envz00_3331,
		obj_t BgL_oz00_3332)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5746;

				{
					obj_t BgL_auxz00_5747;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5748;

						BgL_tmpz00_5748 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3332));
						BgL_auxz00_5747 = BGL_OBJECT_WIDENING(BgL_tmpz00_5748);
					}
					BgL_auxz00_5746 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5747);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5746))->BgL_kz00);
			}
		}

	}



/* &<@anonymous:1750> */
	obj_t BGl_z62zc3z04anonymousza31750ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3333)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BUNSPEC;
		}

	}



/* &lambda1749 */
	obj_t BGl_z62lambda1749z62zzintegrate_infoz00(obj_t BgL_envz00_3334,
		obj_t BgL_oz00_3335, obj_t BgL_vz00_3336)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5754;

				{
					obj_t BgL_auxz00_5755;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5756;

						BgL_tmpz00_5756 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3335));
						BgL_auxz00_5755 = BGL_OBJECT_WIDENING(BgL_tmpz00_5756);
					}
					BgL_auxz00_5754 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5755);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5754))->BgL_ctoz00) =
					((obj_t) BgL_vz00_3336), BUNSPEC);
			}
		}

	}



/* &lambda1748 */
	obj_t BGl_z62lambda1748z62zzintegrate_infoz00(obj_t BgL_envz00_3337,
		obj_t BgL_oz00_3338)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5762;

				{
					obj_t BgL_auxz00_5763;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5764;

						BgL_tmpz00_5764 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3338));
						BgL_auxz00_5763 = BGL_OBJECT_WIDENING(BgL_tmpz00_5764);
					}
					BgL_auxz00_5762 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5763);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5762))->BgL_ctoz00);
			}
		}

	}



/* &<@anonymous:1738> */
	obj_t BGl_z62zc3z04anonymousza31738ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3339)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BNIL;
		}

	}



/* &lambda1737 */
	obj_t BGl_z62lambda1737z62zzintegrate_infoz00(obj_t BgL_envz00_3340,
		obj_t BgL_oz00_3341, obj_t BgL_vz00_3342)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5770;

				{
					obj_t BgL_auxz00_5771;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5772;

						BgL_tmpz00_5772 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3341));
						BgL_auxz00_5771 = BGL_OBJECT_WIDENING(BgL_tmpz00_5772);
					}
					BgL_auxz00_5770 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5771);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5770))->
						BgL_cfromz00) = ((obj_t) BgL_vz00_3342), BUNSPEC);
			}
		}

	}



/* &lambda1736 */
	obj_t BGl_z62lambda1736z62zzintegrate_infoz00(obj_t BgL_envz00_3343,
		obj_t BgL_oz00_3344)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5778;

				{
					obj_t BgL_auxz00_5779;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5780;

						BgL_tmpz00_5780 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3344));
						BgL_auxz00_5779 = BGL_OBJECT_WIDENING(BgL_tmpz00_5780);
					}
					BgL_auxz00_5778 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5779);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5778))->BgL_cfromz00);
			}
		}

	}



/* &<@anonymous:1723> */
	obj_t BGl_z62zc3z04anonymousza31723ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3345)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BNIL;
		}

	}



/* &lambda1722 */
	obj_t BGl_z62lambda1722z62zzintegrate_infoz00(obj_t BgL_envz00_3346,
		obj_t BgL_oz00_3347, obj_t BgL_vz00_3348)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5786;

				{
					obj_t BgL_auxz00_5787;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5788;

						BgL_tmpz00_5788 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3347));
						BgL_auxz00_5787 = BGL_OBJECT_WIDENING(BgL_tmpz00_5788);
					}
					BgL_auxz00_5786 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5787);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5786))->
						BgL_boundz00) = ((obj_t) BgL_vz00_3348), BUNSPEC);
			}
		}

	}



/* &lambda1721 */
	obj_t BGl_z62lambda1721z62zzintegrate_infoz00(obj_t BgL_envz00_3349,
		obj_t BgL_oz00_3350)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5794;

				{
					obj_t BgL_auxz00_5795;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5796;

						BgL_tmpz00_5796 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3350));
						BgL_auxz00_5795 = BGL_OBJECT_WIDENING(BgL_tmpz00_5796);
					}
					BgL_auxz00_5794 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5795);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5794))->BgL_boundz00);
			}
		}

	}



/* &<@anonymous:1711> */
	obj_t BGl_z62zc3z04anonymousza31711ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3351)
	{
		{	/* Integrate/iinfo.scm 45 */
			return BUNSPEC;
		}

	}



/* &lambda1710 */
	obj_t BGl_z62lambda1710z62zzintegrate_infoz00(obj_t BgL_envz00_3352,
		obj_t BgL_oz00_3353, obj_t BgL_vz00_3354)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5802;

				{
					obj_t BgL_auxz00_5803;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5804;

						BgL_tmpz00_5804 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3353));
						BgL_auxz00_5803 = BGL_OBJECT_WIDENING(BgL_tmpz00_5804);
					}
					BgL_auxz00_5802 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5803);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5802))->
						BgL_freez00) = ((obj_t) BgL_vz00_3354), BUNSPEC);
			}
		}

	}



/* &lambda1709 */
	obj_t BGl_z62lambda1709z62zzintegrate_infoz00(obj_t BgL_envz00_3355,
		obj_t BgL_oz00_3356)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5810;

				{
					obj_t BgL_auxz00_5811;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5812;

						BgL_tmpz00_5812 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3356));
						BgL_auxz00_5811 = BGL_OBJECT_WIDENING(BgL_tmpz00_5812);
					}
					BgL_auxz00_5810 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5811);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5810))->BgL_freez00);
			}
		}

	}



/* &lambda1701 */
	obj_t BGl_z62lambda1701z62zzintegrate_infoz00(obj_t BgL_envz00_3357,
		obj_t BgL_oz00_3358, obj_t BgL_vz00_3359)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5818;

				{
					obj_t BgL_auxz00_5819;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5820;

						BgL_tmpz00_5820 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3358));
						BgL_auxz00_5819 = BGL_OBJECT_WIDENING(BgL_tmpz00_5820);
					}
					BgL_auxz00_5818 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5819);
				}
				return
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5818))->
						BgL_ownerz00) = ((obj_t) BgL_vz00_3359), BUNSPEC);
			}
		}

	}



/* &lambda1700 */
	obj_t BGl_z62lambda1700z62zzintegrate_infoz00(obj_t BgL_envz00_3360,
		obj_t BgL_oz00_3361)
	{
		{	/* Integrate/iinfo.scm 45 */
			{
				BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5826;

				{
					obj_t BgL_auxz00_5827;

					{	/* Integrate/iinfo.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5828;

						BgL_tmpz00_5828 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_oz00_3361));
						BgL_auxz00_5827 = BGL_OBJECT_WIDENING(BgL_tmpz00_5828);
					}
					BgL_auxz00_5826 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5827);
				}
				return
					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5826))->BgL_ownerz00);
			}
		}

	}



/* &lambda1566 */
	BgL_sexitz00_bglt BGl_z62lambda1566z62zzintegrate_infoz00(obj_t
		BgL_envz00_3362, obj_t BgL_o1126z00_3363)
	{
		{	/* Integrate/iinfo.scm 35 */
			{	/* Integrate/iinfo.scm 35 */
				long BgL_arg1571z00_3587;

				{	/* Integrate/iinfo.scm 35 */
					obj_t BgL_arg1573z00_3588;

					{	/* Integrate/iinfo.scm 35 */
						obj_t BgL_arg1575z00_3589;

						{	/* Integrate/iinfo.scm 35 */
							obj_t BgL_arg1815z00_3590;
							long BgL_arg1816z00_3591;

							BgL_arg1815z00_3590 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Integrate/iinfo.scm 35 */
								long BgL_arg1817z00_3592;

								BgL_arg1817z00_3592 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_sexitz00_bglt) BgL_o1126z00_3363)));
								BgL_arg1816z00_3591 = (BgL_arg1817z00_3592 - OBJECT_TYPE);
							}
							BgL_arg1575z00_3589 =
								VECTOR_REF(BgL_arg1815z00_3590, BgL_arg1816z00_3591);
						}
						BgL_arg1573z00_3588 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1575z00_3589);
					}
					{	/* Integrate/iinfo.scm 35 */
						obj_t BgL_tmpz00_5841;

						BgL_tmpz00_5841 = ((obj_t) BgL_arg1573z00_3588);
						BgL_arg1571z00_3587 = BGL_CLASS_NUM(BgL_tmpz00_5841);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_sexitz00_bglt) BgL_o1126z00_3363)), BgL_arg1571z00_3587);
			}
			{	/* Integrate/iinfo.scm 35 */
				BgL_objectz00_bglt BgL_tmpz00_5847;

				BgL_tmpz00_5847 =
					((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_o1126z00_3363));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5847, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_o1126z00_3363));
			return ((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt) BgL_o1126z00_3363));
		}

	}



/* &<@anonymous:1565> */
	obj_t BGl_z62zc3z04anonymousza31565ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3364, obj_t BgL_new1125z00_3365)
	{
		{	/* Integrate/iinfo.scm 35 */
			{
				BgL_sexitz00_bglt BgL_auxz00_5855;

				((((BgL_sexitz00_bglt) COBJECT(
								((BgL_sexitz00_bglt)
									((BgL_sexitz00_bglt) BgL_new1125z00_3365))))->
						BgL_handlerz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_sexitz00_bglt) COBJECT(((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt)
										BgL_new1125z00_3365))))->BgL_detachedzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				{
					BgL_sexitzf2iinfozf2_bglt BgL_auxz00_5862;

					{
						obj_t BgL_auxz00_5863;

						{	/* Integrate/iinfo.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_5864;

							BgL_tmpz00_5864 =
								((BgL_objectz00_bglt)
								((BgL_sexitz00_bglt) BgL_new1125z00_3365));
							BgL_auxz00_5863 = BGL_OBJECT_WIDENING(BgL_tmpz00_5864);
						}
						BgL_auxz00_5862 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_5863);
					}
					((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5862))->
							BgL_fzd2markzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sexitzf2iinfozf2_bglt BgL_auxz00_5870;

					{
						obj_t BgL_auxz00_5871;

						{	/* Integrate/iinfo.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_5872;

							BgL_tmpz00_5872 =
								((BgL_objectz00_bglt)
								((BgL_sexitz00_bglt) BgL_new1125z00_3365));
							BgL_auxz00_5871 = BGL_OBJECT_WIDENING(BgL_tmpz00_5872);
						}
						BgL_auxz00_5870 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_5871);
					}
					((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5870))->
							BgL_uzd2markzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sexitzf2iinfozf2_bglt BgL_auxz00_5878;

					{
						obj_t BgL_auxz00_5879;

						{	/* Integrate/iinfo.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_5880;

							BgL_tmpz00_5880 =
								((BgL_objectz00_bglt)
								((BgL_sexitz00_bglt) BgL_new1125z00_3365));
							BgL_auxz00_5879 = BGL_OBJECT_WIDENING(BgL_tmpz00_5880);
						}
						BgL_auxz00_5878 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_5879);
					}
					((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5878))->
							BgL_kapturedzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_sexitzf2iinfozf2_bglt BgL_auxz00_5886;

					{
						obj_t BgL_auxz00_5887;

						{	/* Integrate/iinfo.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_5888;

							BgL_tmpz00_5888 =
								((BgL_objectz00_bglt)
								((BgL_sexitz00_bglt) BgL_new1125z00_3365));
							BgL_auxz00_5887 = BGL_OBJECT_WIDENING(BgL_tmpz00_5888);
						}
						BgL_auxz00_5886 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_5887);
					}
					((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5886))->
							BgL_celledzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				BgL_auxz00_5855 = ((BgL_sexitz00_bglt) BgL_new1125z00_3365);
				return ((obj_t) BgL_auxz00_5855);
			}
		}

	}



/* &lambda1562 */
	BgL_sexitz00_bglt BGl_z62lambda1562z62zzintegrate_infoz00(obj_t
		BgL_envz00_3366, obj_t BgL_o1122z00_3367)
	{
		{	/* Integrate/iinfo.scm 35 */
			{	/* Integrate/iinfo.scm 35 */
				BgL_sexitzf2iinfozf2_bglt BgL_wide1124z00_3595;

				BgL_wide1124z00_3595 =
					((BgL_sexitzf2iinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_sexitzf2iinfozf2_bgl))));
				{	/* Integrate/iinfo.scm 35 */
					obj_t BgL_auxz00_5901;
					BgL_objectz00_bglt BgL_tmpz00_5897;

					BgL_auxz00_5901 = ((obj_t) BgL_wide1124z00_3595);
					BgL_tmpz00_5897 =
						((BgL_objectz00_bglt)
						((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt) BgL_o1122z00_3367)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5897, BgL_auxz00_5901);
				}
				((BgL_objectz00_bglt)
					((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt) BgL_o1122z00_3367)));
				{	/* Integrate/iinfo.scm 35 */
					long BgL_arg1564z00_3596;

					BgL_arg1564z00_3596 =
						BGL_CLASS_NUM(BGl_sexitzf2Iinfozf2zzintegrate_infoz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_sexitz00_bglt)
								((BgL_sexitz00_bglt) BgL_o1122z00_3367))), BgL_arg1564z00_3596);
				}
				return
					((BgL_sexitz00_bglt)
					((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt) BgL_o1122z00_3367)));
			}
		}

	}



/* &lambda1554 */
	BgL_sexitz00_bglt BGl_z62lambda1554z62zzintegrate_infoz00(obj_t
		BgL_envz00_3368, obj_t BgL_handler1116z00_3369,
		obj_t BgL_detachedzf31117zf3_3370, obj_t BgL_fzd2mark1118zd2_3371,
		obj_t BgL_uzd2mark1119zd2_3372, obj_t BgL_kapturedzf31120zf3_3373,
		obj_t BgL_celledzf31121zf3_3374)
	{
		{	/* Integrate/iinfo.scm 35 */
			{	/* Integrate/iinfo.scm 35 */
				bool_t BgL_detachedzf31117zf3_3597;
				bool_t BgL_kapturedzf31120zf3_3598;
				bool_t BgL_celledzf31121zf3_3599;

				BgL_detachedzf31117zf3_3597 = CBOOL(BgL_detachedzf31117zf3_3370);
				BgL_kapturedzf31120zf3_3598 = CBOOL(BgL_kapturedzf31120zf3_3373);
				BgL_celledzf31121zf3_3599 = CBOOL(BgL_celledzf31121zf3_3374);
				{	/* Integrate/iinfo.scm 35 */
					BgL_sexitz00_bglt BgL_new1197z00_3600;

					{	/* Integrate/iinfo.scm 35 */
						BgL_sexitz00_bglt BgL_tmp1195z00_3601;
						BgL_sexitzf2iinfozf2_bglt BgL_wide1196z00_3602;

						{
							BgL_sexitz00_bglt BgL_auxz00_5918;

							{	/* Integrate/iinfo.scm 35 */
								BgL_sexitz00_bglt BgL_new1194z00_3603;

								BgL_new1194z00_3603 =
									((BgL_sexitz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_sexitz00_bgl))));
								{	/* Integrate/iinfo.scm 35 */
									long BgL_arg1561z00_3604;

									BgL_arg1561z00_3604 = BGL_CLASS_NUM(BGl_sexitz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1194z00_3603),
										BgL_arg1561z00_3604);
								}
								{	/* Integrate/iinfo.scm 35 */
									BgL_objectz00_bglt BgL_tmpz00_5923;

									BgL_tmpz00_5923 = ((BgL_objectz00_bglt) BgL_new1194z00_3603);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5923, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1194z00_3603);
								BgL_auxz00_5918 = BgL_new1194z00_3603;
							}
							BgL_tmp1195z00_3601 = ((BgL_sexitz00_bglt) BgL_auxz00_5918);
						}
						BgL_wide1196z00_3602 =
							((BgL_sexitzf2iinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_sexitzf2iinfozf2_bgl))));
						{	/* Integrate/iinfo.scm 35 */
							obj_t BgL_auxz00_5931;
							BgL_objectz00_bglt BgL_tmpz00_5929;

							BgL_auxz00_5931 = ((obj_t) BgL_wide1196z00_3602);
							BgL_tmpz00_5929 = ((BgL_objectz00_bglt) BgL_tmp1195z00_3601);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5929, BgL_auxz00_5931);
						}
						((BgL_objectz00_bglt) BgL_tmp1195z00_3601);
						{	/* Integrate/iinfo.scm 35 */
							long BgL_arg1559z00_3605;

							BgL_arg1559z00_3605 =
								BGL_CLASS_NUM(BGl_sexitzf2Iinfozf2zzintegrate_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1195z00_3601),
								BgL_arg1559z00_3605);
						}
						BgL_new1197z00_3600 = ((BgL_sexitz00_bglt) BgL_tmp1195z00_3601);
					}
					((((BgL_sexitz00_bglt) COBJECT(
									((BgL_sexitz00_bglt) BgL_new1197z00_3600)))->BgL_handlerz00) =
						((obj_t) BgL_handler1116z00_3369), BUNSPEC);
					((((BgL_sexitz00_bglt) COBJECT(((BgL_sexitz00_bglt)
										BgL_new1197z00_3600)))->BgL_detachedzf3zf3) =
						((bool_t) BgL_detachedzf31117zf3_3597), BUNSPEC);
					{
						BgL_sexitzf2iinfozf2_bglt BgL_auxz00_5943;

						{
							obj_t BgL_auxz00_5944;

							{	/* Integrate/iinfo.scm 35 */
								BgL_objectz00_bglt BgL_tmpz00_5945;

								BgL_tmpz00_5945 = ((BgL_objectz00_bglt) BgL_new1197z00_3600);
								BgL_auxz00_5944 = BGL_OBJECT_WIDENING(BgL_tmpz00_5945);
							}
							BgL_auxz00_5943 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_5944);
						}
						((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5943))->
								BgL_fzd2markzd2) = ((obj_t) BgL_fzd2mark1118zd2_3371), BUNSPEC);
					}
					{
						BgL_sexitzf2iinfozf2_bglt BgL_auxz00_5950;

						{
							obj_t BgL_auxz00_5951;

							{	/* Integrate/iinfo.scm 35 */
								BgL_objectz00_bglt BgL_tmpz00_5952;

								BgL_tmpz00_5952 = ((BgL_objectz00_bglt) BgL_new1197z00_3600);
								BgL_auxz00_5951 = BGL_OBJECT_WIDENING(BgL_tmpz00_5952);
							}
							BgL_auxz00_5950 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_5951);
						}
						((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5950))->
								BgL_uzd2markzd2) = ((obj_t) BgL_uzd2mark1119zd2_3372), BUNSPEC);
					}
					{
						BgL_sexitzf2iinfozf2_bglt BgL_auxz00_5957;

						{
							obj_t BgL_auxz00_5958;

							{	/* Integrate/iinfo.scm 35 */
								BgL_objectz00_bglt BgL_tmpz00_5959;

								BgL_tmpz00_5959 = ((BgL_objectz00_bglt) BgL_new1197z00_3600);
								BgL_auxz00_5958 = BGL_OBJECT_WIDENING(BgL_tmpz00_5959);
							}
							BgL_auxz00_5957 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_5958);
						}
						((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5957))->
								BgL_kapturedzf3zf3) =
							((bool_t) BgL_kapturedzf31120zf3_3598), BUNSPEC);
					}
					{
						BgL_sexitzf2iinfozf2_bglt BgL_auxz00_5964;

						{
							obj_t BgL_auxz00_5965;

							{	/* Integrate/iinfo.scm 35 */
								BgL_objectz00_bglt BgL_tmpz00_5966;

								BgL_tmpz00_5966 = ((BgL_objectz00_bglt) BgL_new1197z00_3600);
								BgL_auxz00_5965 = BGL_OBJECT_WIDENING(BgL_tmpz00_5966);
							}
							BgL_auxz00_5964 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_5965);
						}
						((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5964))->
								BgL_celledzf3zf3) =
							((bool_t) BgL_celledzf31121zf3_3599), BUNSPEC);
					}
					return BgL_new1197z00_3600;
				}
			}
		}

	}



/* &<@anonymous:1632> */
	obj_t BGl_z62zc3z04anonymousza31632ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3375)
	{
		{	/* Integrate/iinfo.scm 35 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1631 */
	obj_t BGl_z62lambda1631z62zzintegrate_infoz00(obj_t BgL_envz00_3376,
		obj_t BgL_oz00_3377, obj_t BgL_vz00_3378)
	{
		{	/* Integrate/iinfo.scm 35 */
			{	/* Integrate/iinfo.scm 35 */
				bool_t BgL_vz00_3607;

				BgL_vz00_3607 = CBOOL(BgL_vz00_3378);
				{
					BgL_sexitzf2iinfozf2_bglt BgL_auxz00_5973;

					{
						obj_t BgL_auxz00_5974;

						{	/* Integrate/iinfo.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_5975;

							BgL_tmpz00_5975 =
								((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3377));
							BgL_auxz00_5974 = BGL_OBJECT_WIDENING(BgL_tmpz00_5975);
						}
						BgL_auxz00_5973 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_5974);
					}
					return
						((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5973))->
							BgL_celledzf3zf3) = ((bool_t) BgL_vz00_3607), BUNSPEC);
				}
			}
		}

	}



/* &lambda1630 */
	obj_t BGl_z62lambda1630z62zzintegrate_infoz00(obj_t BgL_envz00_3379,
		obj_t BgL_oz00_3380)
	{
		{	/* Integrate/iinfo.scm 35 */
			{	/* Integrate/iinfo.scm 35 */
				bool_t BgL_tmpz00_5981;

				{
					BgL_sexitzf2iinfozf2_bglt BgL_auxz00_5982;

					{
						obj_t BgL_auxz00_5983;

						{	/* Integrate/iinfo.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_5984;

							BgL_tmpz00_5984 =
								((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3380));
							BgL_auxz00_5983 = BGL_OBJECT_WIDENING(BgL_tmpz00_5984);
						}
						BgL_auxz00_5982 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_5983);
					}
					BgL_tmpz00_5981 =
						(((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5982))->
						BgL_celledzf3zf3);
				}
				return BBOOL(BgL_tmpz00_5981);
			}
		}

	}



/* &<@anonymous:1616> */
	obj_t BGl_z62zc3z04anonymousza31616ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3381)
	{
		{	/* Integrate/iinfo.scm 35 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1615 */
	obj_t BGl_z62lambda1615z62zzintegrate_infoz00(obj_t BgL_envz00_3382,
		obj_t BgL_oz00_3383, obj_t BgL_vz00_3384)
	{
		{	/* Integrate/iinfo.scm 35 */
			{	/* Integrate/iinfo.scm 35 */
				bool_t BgL_vz00_3610;

				BgL_vz00_3610 = CBOOL(BgL_vz00_3384);
				{
					BgL_sexitzf2iinfozf2_bglt BgL_auxz00_5993;

					{
						obj_t BgL_auxz00_5994;

						{	/* Integrate/iinfo.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_5995;

							BgL_tmpz00_5995 =
								((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3383));
							BgL_auxz00_5994 = BGL_OBJECT_WIDENING(BgL_tmpz00_5995);
						}
						BgL_auxz00_5993 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_5994);
					}
					return
						((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5993))->
							BgL_kapturedzf3zf3) = ((bool_t) BgL_vz00_3610), BUNSPEC);
				}
			}
		}

	}



/* &lambda1614 */
	obj_t BGl_z62lambda1614z62zzintegrate_infoz00(obj_t BgL_envz00_3385,
		obj_t BgL_oz00_3386)
	{
		{	/* Integrate/iinfo.scm 35 */
			{	/* Integrate/iinfo.scm 35 */
				bool_t BgL_tmpz00_6001;

				{
					BgL_sexitzf2iinfozf2_bglt BgL_auxz00_6002;

					{
						obj_t BgL_auxz00_6003;

						{	/* Integrate/iinfo.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_6004;

							BgL_tmpz00_6004 =
								((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3386));
							BgL_auxz00_6003 = BGL_OBJECT_WIDENING(BgL_tmpz00_6004);
						}
						BgL_auxz00_6002 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_6003);
					}
					BgL_tmpz00_6001 =
						(((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6002))->
						BgL_kapturedzf3zf3);
				}
				return BBOOL(BgL_tmpz00_6001);
			}
		}

	}



/* &<@anonymous:1605> */
	obj_t BGl_z62zc3z04anonymousza31605ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3387)
	{
		{	/* Integrate/iinfo.scm 35 */
			return BUNSPEC;
		}

	}



/* &lambda1604 */
	obj_t BGl_z62lambda1604z62zzintegrate_infoz00(obj_t BgL_envz00_3388,
		obj_t BgL_oz00_3389, obj_t BgL_vz00_3390)
	{
		{	/* Integrate/iinfo.scm 35 */
			{
				BgL_sexitzf2iinfozf2_bglt BgL_auxz00_6011;

				{
					obj_t BgL_auxz00_6012;

					{	/* Integrate/iinfo.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_6013;

						BgL_tmpz00_6013 =
							((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3389));
						BgL_auxz00_6012 = BGL_OBJECT_WIDENING(BgL_tmpz00_6013);
					}
					BgL_auxz00_6011 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_6012);
				}
				return
					((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6011))->
						BgL_uzd2markzd2) = ((obj_t) BgL_vz00_3390), BUNSPEC);
			}
		}

	}



/* &lambda1603 */
	obj_t BGl_z62lambda1603z62zzintegrate_infoz00(obj_t BgL_envz00_3391,
		obj_t BgL_oz00_3392)
	{
		{	/* Integrate/iinfo.scm 35 */
			{
				BgL_sexitzf2iinfozf2_bglt BgL_auxz00_6019;

				{
					obj_t BgL_auxz00_6020;

					{	/* Integrate/iinfo.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_6021;

						BgL_tmpz00_6021 =
							((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3392));
						BgL_auxz00_6020 = BGL_OBJECT_WIDENING(BgL_tmpz00_6021);
					}
					BgL_auxz00_6019 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_6020);
				}
				return
					(((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6019))->
					BgL_uzd2markzd2);
			}
		}

	}



/* &<@anonymous:1592> */
	obj_t BGl_z62zc3z04anonymousza31592ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3393)
	{
		{	/* Integrate/iinfo.scm 35 */
			return BUNSPEC;
		}

	}



/* &lambda1591 */
	obj_t BGl_z62lambda1591z62zzintegrate_infoz00(obj_t BgL_envz00_3394,
		obj_t BgL_oz00_3395, obj_t BgL_vz00_3396)
	{
		{	/* Integrate/iinfo.scm 35 */
			{
				BgL_sexitzf2iinfozf2_bglt BgL_auxz00_6027;

				{
					obj_t BgL_auxz00_6028;

					{	/* Integrate/iinfo.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_6029;

						BgL_tmpz00_6029 =
							((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3395));
						BgL_auxz00_6028 = BGL_OBJECT_WIDENING(BgL_tmpz00_6029);
					}
					BgL_auxz00_6027 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_6028);
				}
				return
					((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6027))->
						BgL_fzd2markzd2) = ((obj_t) BgL_vz00_3396), BUNSPEC);
			}
		}

	}



/* &lambda1590 */
	obj_t BGl_z62lambda1590z62zzintegrate_infoz00(obj_t BgL_envz00_3397,
		obj_t BgL_oz00_3398)
	{
		{	/* Integrate/iinfo.scm 35 */
			{
				BgL_sexitzf2iinfozf2_bglt BgL_auxz00_6035;

				{
					obj_t BgL_auxz00_6036;

					{	/* Integrate/iinfo.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_6037;

						BgL_tmpz00_6037 =
							((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_oz00_3398));
						BgL_auxz00_6036 = BGL_OBJECT_WIDENING(BgL_tmpz00_6037);
					}
					BgL_auxz00_6035 = ((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_6036);
				}
				return
					(((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6035))->
					BgL_fzd2markzd2);
			}
		}

	}



/* &lambda1369 */
	BgL_svarz00_bglt BGl_z62lambda1369z62zzintegrate_infoz00(obj_t
		BgL_envz00_3399, obj_t BgL_o1114z00_3400)
	{
		{	/* Integrate/iinfo.scm 23 */
			{	/* Integrate/iinfo.scm 23 */
				long BgL_arg1370z00_3617;

				{	/* Integrate/iinfo.scm 23 */
					obj_t BgL_arg1371z00_3618;

					{	/* Integrate/iinfo.scm 23 */
						obj_t BgL_arg1375z00_3619;

						{	/* Integrate/iinfo.scm 23 */
							obj_t BgL_arg1815z00_3620;
							long BgL_arg1816z00_3621;

							BgL_arg1815z00_3620 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Integrate/iinfo.scm 23 */
								long BgL_arg1817z00_3622;

								BgL_arg1817z00_3622 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_svarz00_bglt) BgL_o1114z00_3400)));
								BgL_arg1816z00_3621 = (BgL_arg1817z00_3622 - OBJECT_TYPE);
							}
							BgL_arg1375z00_3619 =
								VECTOR_REF(BgL_arg1815z00_3620, BgL_arg1816z00_3621);
						}
						BgL_arg1371z00_3618 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1375z00_3619);
					}
					{	/* Integrate/iinfo.scm 23 */
						obj_t BgL_tmpz00_6050;

						BgL_tmpz00_6050 = ((obj_t) BgL_arg1371z00_3618);
						BgL_arg1370z00_3617 = BGL_CLASS_NUM(BgL_tmpz00_6050);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_svarz00_bglt) BgL_o1114z00_3400)), BgL_arg1370z00_3617);
			}
			{	/* Integrate/iinfo.scm 23 */
				BgL_objectz00_bglt BgL_tmpz00_6056;

				BgL_tmpz00_6056 =
					((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_o1114z00_3400));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6056, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_o1114z00_3400));
			return ((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_o1114z00_3400));
		}

	}



/* &<@anonymous:1368> */
	obj_t BGl_z62zc3z04anonymousza31368ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3401, obj_t BgL_new1113z00_3402)
	{
		{	/* Integrate/iinfo.scm 23 */
			{
				BgL_svarz00_bglt BgL_auxz00_6064;

				((((BgL_svarz00_bglt) COBJECT(
								((BgL_svarz00_bglt)
									((BgL_svarz00_bglt) BgL_new1113z00_3402))))->BgL_locz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_svarzf2iinfozf2_bglt BgL_auxz00_6068;

					{
						obj_t BgL_auxz00_6069;

						{	/* Integrate/iinfo.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_6070;

							BgL_tmpz00_6070 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_new1113z00_3402));
							BgL_auxz00_6069 = BGL_OBJECT_WIDENING(BgL_tmpz00_6070);
						}
						BgL_auxz00_6068 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6069);
					}
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6068))->
							BgL_fzd2markzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_svarzf2iinfozf2_bglt BgL_auxz00_6076;

					{
						obj_t BgL_auxz00_6077;

						{	/* Integrate/iinfo.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_6078;

							BgL_tmpz00_6078 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_new1113z00_3402));
							BgL_auxz00_6077 = BGL_OBJECT_WIDENING(BgL_tmpz00_6078);
						}
						BgL_auxz00_6076 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6077);
					}
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6076))->
							BgL_uzd2markzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_svarzf2iinfozf2_bglt BgL_auxz00_6084;

					{
						obj_t BgL_auxz00_6085;

						{	/* Integrate/iinfo.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_6086;

							BgL_tmpz00_6086 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_new1113z00_3402));
							BgL_auxz00_6085 = BGL_OBJECT_WIDENING(BgL_tmpz00_6086);
						}
						BgL_auxz00_6084 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6085);
					}
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6084))->
							BgL_kapturedzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_svarzf2iinfozf2_bglt BgL_auxz00_6092;

					{
						obj_t BgL_auxz00_6093;

						{	/* Integrate/iinfo.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_6094;

							BgL_tmpz00_6094 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_new1113z00_3402));
							BgL_auxz00_6093 = BGL_OBJECT_WIDENING(BgL_tmpz00_6094);
						}
						BgL_auxz00_6092 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6093);
					}
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6092))->
							BgL_celledzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_svarzf2iinfozf2_bglt BgL_auxz00_6100;

					{
						obj_t BgL_auxz00_6101;

						{	/* Integrate/iinfo.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_6102;

							BgL_tmpz00_6102 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_new1113z00_3402));
							BgL_auxz00_6101 = BGL_OBJECT_WIDENING(BgL_tmpz00_6102);
						}
						BgL_auxz00_6100 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6101);
					}
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6100))->
							BgL_xhdlz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_6064 = ((BgL_svarz00_bglt) BgL_new1113z00_3402);
				return ((obj_t) BgL_auxz00_6064);
			}
		}

	}



/* &lambda1365 */
	BgL_svarz00_bglt BGl_z62lambda1365z62zzintegrate_infoz00(obj_t
		BgL_envz00_3403, obj_t BgL_o1109z00_3404)
	{
		{	/* Integrate/iinfo.scm 23 */
			{	/* Integrate/iinfo.scm 23 */
				BgL_svarzf2iinfozf2_bglt BgL_wide1111z00_3625;

				BgL_wide1111z00_3625 =
					((BgL_svarzf2iinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_svarzf2iinfozf2_bgl))));
				{	/* Integrate/iinfo.scm 23 */
					obj_t BgL_auxz00_6115;
					BgL_objectz00_bglt BgL_tmpz00_6111;

					BgL_auxz00_6115 = ((obj_t) BgL_wide1111z00_3625);
					BgL_tmpz00_6111 =
						((BgL_objectz00_bglt)
						((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_o1109z00_3404)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6111, BgL_auxz00_6115);
				}
				((BgL_objectz00_bglt)
					((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_o1109z00_3404)));
				{	/* Integrate/iinfo.scm 23 */
					long BgL_arg1367z00_3626;

					BgL_arg1367z00_3626 =
						BGL_CLASS_NUM(BGl_svarzf2Iinfozf2zzintegrate_infoz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_svarz00_bglt)
								((BgL_svarz00_bglt) BgL_o1109z00_3404))), BgL_arg1367z00_3626);
				}
				return
					((BgL_svarz00_bglt)
					((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_o1109z00_3404)));
			}
		}

	}



/* &lambda1352 */
	BgL_svarz00_bglt BGl_z62lambda1352z62zzintegrate_infoz00(obj_t
		BgL_envz00_3405, obj_t BgL_loc1103z00_3406, obj_t BgL_fzd2mark1104zd2_3407,
		obj_t BgL_uzd2mark1105zd2_3408, obj_t BgL_kapturedzf31106zf3_3409,
		obj_t BgL_celledzf31107zf3_3410, obj_t BgL_xhdl1108z00_3411)
	{
		{	/* Integrate/iinfo.scm 23 */
			{	/* Integrate/iinfo.scm 23 */
				bool_t BgL_kapturedzf31106zf3_3627;
				bool_t BgL_celledzf31107zf3_3628;

				BgL_kapturedzf31106zf3_3627 = CBOOL(BgL_kapturedzf31106zf3_3409);
				BgL_celledzf31107zf3_3628 = CBOOL(BgL_celledzf31107zf3_3410);
				{	/* Integrate/iinfo.scm 23 */
					BgL_svarz00_bglt BgL_new1192z00_3629;

					{	/* Integrate/iinfo.scm 23 */
						BgL_svarz00_bglt BgL_tmp1190z00_3630;
						BgL_svarzf2iinfozf2_bglt BgL_wide1191z00_3631;

						{
							BgL_svarz00_bglt BgL_auxz00_6131;

							{	/* Integrate/iinfo.scm 23 */
								BgL_svarz00_bglt BgL_new1189z00_3632;

								BgL_new1189z00_3632 =
									((BgL_svarz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_svarz00_bgl))));
								{	/* Integrate/iinfo.scm 23 */
									long BgL_arg1364z00_3633;

									BgL_arg1364z00_3633 = BGL_CLASS_NUM(BGl_svarz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1189z00_3632),
										BgL_arg1364z00_3633);
								}
								{	/* Integrate/iinfo.scm 23 */
									BgL_objectz00_bglt BgL_tmpz00_6136;

									BgL_tmpz00_6136 = ((BgL_objectz00_bglt) BgL_new1189z00_3632);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6136, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1189z00_3632);
								BgL_auxz00_6131 = BgL_new1189z00_3632;
							}
							BgL_tmp1190z00_3630 = ((BgL_svarz00_bglt) BgL_auxz00_6131);
						}
						BgL_wide1191z00_3631 =
							((BgL_svarzf2iinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_svarzf2iinfozf2_bgl))));
						{	/* Integrate/iinfo.scm 23 */
							obj_t BgL_auxz00_6144;
							BgL_objectz00_bglt BgL_tmpz00_6142;

							BgL_auxz00_6144 = ((obj_t) BgL_wide1191z00_3631);
							BgL_tmpz00_6142 = ((BgL_objectz00_bglt) BgL_tmp1190z00_3630);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6142, BgL_auxz00_6144);
						}
						((BgL_objectz00_bglt) BgL_tmp1190z00_3630);
						{	/* Integrate/iinfo.scm 23 */
							long BgL_arg1361z00_3634;

							BgL_arg1361z00_3634 =
								BGL_CLASS_NUM(BGl_svarzf2Iinfozf2zzintegrate_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt) BgL_tmp1190z00_3630),
								BgL_arg1361z00_3634);
						}
						BgL_new1192z00_3629 = ((BgL_svarz00_bglt) BgL_tmp1190z00_3630);
					}
					((((BgL_svarz00_bglt) COBJECT(
									((BgL_svarz00_bglt) BgL_new1192z00_3629)))->BgL_locz00) =
						((obj_t) BgL_loc1103z00_3406), BUNSPEC);
					{
						BgL_svarzf2iinfozf2_bglt BgL_auxz00_6154;

						{
							obj_t BgL_auxz00_6155;

							{	/* Integrate/iinfo.scm 23 */
								BgL_objectz00_bglt BgL_tmpz00_6156;

								BgL_tmpz00_6156 = ((BgL_objectz00_bglt) BgL_new1192z00_3629);
								BgL_auxz00_6155 = BGL_OBJECT_WIDENING(BgL_tmpz00_6156);
							}
							BgL_auxz00_6154 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6155);
						}
						((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6154))->
								BgL_fzd2markzd2) = ((obj_t) BgL_fzd2mark1104zd2_3407), BUNSPEC);
					}
					{
						BgL_svarzf2iinfozf2_bglt BgL_auxz00_6161;

						{
							obj_t BgL_auxz00_6162;

							{	/* Integrate/iinfo.scm 23 */
								BgL_objectz00_bglt BgL_tmpz00_6163;

								BgL_tmpz00_6163 = ((BgL_objectz00_bglt) BgL_new1192z00_3629);
								BgL_auxz00_6162 = BGL_OBJECT_WIDENING(BgL_tmpz00_6163);
							}
							BgL_auxz00_6161 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6162);
						}
						((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6161))->
								BgL_uzd2markzd2) = ((obj_t) BgL_uzd2mark1105zd2_3408), BUNSPEC);
					}
					{
						BgL_svarzf2iinfozf2_bglt BgL_auxz00_6168;

						{
							obj_t BgL_auxz00_6169;

							{	/* Integrate/iinfo.scm 23 */
								BgL_objectz00_bglt BgL_tmpz00_6170;

								BgL_tmpz00_6170 = ((BgL_objectz00_bglt) BgL_new1192z00_3629);
								BgL_auxz00_6169 = BGL_OBJECT_WIDENING(BgL_tmpz00_6170);
							}
							BgL_auxz00_6168 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6169);
						}
						((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6168))->
								BgL_kapturedzf3zf3) =
							((bool_t) BgL_kapturedzf31106zf3_3627), BUNSPEC);
					}
					{
						BgL_svarzf2iinfozf2_bglt BgL_auxz00_6175;

						{
							obj_t BgL_auxz00_6176;

							{	/* Integrate/iinfo.scm 23 */
								BgL_objectz00_bglt BgL_tmpz00_6177;

								BgL_tmpz00_6177 = ((BgL_objectz00_bglt) BgL_new1192z00_3629);
								BgL_auxz00_6176 = BGL_OBJECT_WIDENING(BgL_tmpz00_6177);
							}
							BgL_auxz00_6175 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6176);
						}
						((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6175))->
								BgL_celledzf3zf3) =
							((bool_t) BgL_celledzf31107zf3_3628), BUNSPEC);
					}
					{
						BgL_svarzf2iinfozf2_bglt BgL_auxz00_6182;

						{
							obj_t BgL_auxz00_6183;

							{	/* Integrate/iinfo.scm 23 */
								BgL_objectz00_bglt BgL_tmpz00_6184;

								BgL_tmpz00_6184 = ((BgL_objectz00_bglt) BgL_new1192z00_3629);
								BgL_auxz00_6183 = BGL_OBJECT_WIDENING(BgL_tmpz00_6184);
							}
							BgL_auxz00_6182 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6183);
						}
						((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6182))->
								BgL_xhdlz00) = ((obj_t) BgL_xhdl1108z00_3411), BUNSPEC);
					}
					return BgL_new1192z00_3629;
				}
			}
		}

	}



/* &<@anonymous:1517> */
	obj_t BGl_z62zc3z04anonymousza31517ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3412)
	{
		{	/* Integrate/iinfo.scm 23 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1516 */
	obj_t BGl_z62lambda1516z62zzintegrate_infoz00(obj_t BgL_envz00_3413,
		obj_t BgL_oz00_3414, obj_t BgL_vz00_3415)
	{
		{	/* Integrate/iinfo.scm 23 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_6190;

				{
					obj_t BgL_auxz00_6191;

					{	/* Integrate/iinfo.scm 23 */
						BgL_objectz00_bglt BgL_tmpz00_6192;

						BgL_tmpz00_6192 =
							((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_3414));
						BgL_auxz00_6191 = BGL_OBJECT_WIDENING(BgL_tmpz00_6192);
					}
					BgL_auxz00_6190 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6191);
				}
				return
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6190))->
						BgL_xhdlz00) = ((obj_t) BgL_vz00_3415), BUNSPEC);
			}
		}

	}



/* &lambda1515 */
	obj_t BGl_z62lambda1515z62zzintegrate_infoz00(obj_t BgL_envz00_3416,
		obj_t BgL_oz00_3417)
	{
		{	/* Integrate/iinfo.scm 23 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_6198;

				{
					obj_t BgL_auxz00_6199;

					{	/* Integrate/iinfo.scm 23 */
						BgL_objectz00_bglt BgL_tmpz00_6200;

						BgL_tmpz00_6200 =
							((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_3417));
						BgL_auxz00_6199 = BGL_OBJECT_WIDENING(BgL_tmpz00_6200);
					}
					BgL_auxz00_6198 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6199);
				}
				return
					(((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6198))->BgL_xhdlz00);
			}
		}

	}



/* &<@anonymous:1492> */
	obj_t BGl_z62zc3z04anonymousza31492ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3418)
	{
		{	/* Integrate/iinfo.scm 23 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1491 */
	obj_t BGl_z62lambda1491z62zzintegrate_infoz00(obj_t BgL_envz00_3419,
		obj_t BgL_oz00_3420, obj_t BgL_vz00_3421)
	{
		{	/* Integrate/iinfo.scm 23 */
			{	/* Integrate/iinfo.scm 23 */
				bool_t BgL_vz00_3638;

				BgL_vz00_3638 = CBOOL(BgL_vz00_3421);
				{
					BgL_svarzf2iinfozf2_bglt BgL_auxz00_6208;

					{
						obj_t BgL_auxz00_6209;

						{	/* Integrate/iinfo.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_6210;

							BgL_tmpz00_6210 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_3420));
							BgL_auxz00_6209 = BGL_OBJECT_WIDENING(BgL_tmpz00_6210);
						}
						BgL_auxz00_6208 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6209);
					}
					return
						((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6208))->
							BgL_celledzf3zf3) = ((bool_t) BgL_vz00_3638), BUNSPEC);
				}
			}
		}

	}



/* &lambda1490 */
	obj_t BGl_z62lambda1490z62zzintegrate_infoz00(obj_t BgL_envz00_3422,
		obj_t BgL_oz00_3423)
	{
		{	/* Integrate/iinfo.scm 23 */
			{	/* Integrate/iinfo.scm 23 */
				bool_t BgL_tmpz00_6216;

				{
					BgL_svarzf2iinfozf2_bglt BgL_auxz00_6217;

					{
						obj_t BgL_auxz00_6218;

						{	/* Integrate/iinfo.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_6219;

							BgL_tmpz00_6219 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_3423));
							BgL_auxz00_6218 = BGL_OBJECT_WIDENING(BgL_tmpz00_6219);
						}
						BgL_auxz00_6217 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6218);
					}
					BgL_tmpz00_6216 =
						(((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6217))->
						BgL_celledzf3zf3);
				}
				return BBOOL(BgL_tmpz00_6216);
			}
		}

	}



/* &<@anonymous:1456> */
	obj_t BGl_z62zc3z04anonymousza31456ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3424)
	{
		{	/* Integrate/iinfo.scm 23 */
			return BBOOL(((bool_t) 0));
		}

	}



/* &lambda1455 */
	obj_t BGl_z62lambda1455z62zzintegrate_infoz00(obj_t BgL_envz00_3425,
		obj_t BgL_oz00_3426, obj_t BgL_vz00_3427)
	{
		{	/* Integrate/iinfo.scm 23 */
			{	/* Integrate/iinfo.scm 23 */
				bool_t BgL_vz00_3641;

				BgL_vz00_3641 = CBOOL(BgL_vz00_3427);
				{
					BgL_svarzf2iinfozf2_bglt BgL_auxz00_6228;

					{
						obj_t BgL_auxz00_6229;

						{	/* Integrate/iinfo.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_6230;

							BgL_tmpz00_6230 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_3426));
							BgL_auxz00_6229 = BGL_OBJECT_WIDENING(BgL_tmpz00_6230);
						}
						BgL_auxz00_6228 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6229);
					}
					return
						((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6228))->
							BgL_kapturedzf3zf3) = ((bool_t) BgL_vz00_3641), BUNSPEC);
				}
			}
		}

	}



/* &lambda1454 */
	obj_t BGl_z62lambda1454z62zzintegrate_infoz00(obj_t BgL_envz00_3428,
		obj_t BgL_oz00_3429)
	{
		{	/* Integrate/iinfo.scm 23 */
			{	/* Integrate/iinfo.scm 23 */
				bool_t BgL_tmpz00_6236;

				{
					BgL_svarzf2iinfozf2_bglt BgL_auxz00_6237;

					{
						obj_t BgL_auxz00_6238;

						{	/* Integrate/iinfo.scm 23 */
							BgL_objectz00_bglt BgL_tmpz00_6239;

							BgL_tmpz00_6239 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_3429));
							BgL_auxz00_6238 = BGL_OBJECT_WIDENING(BgL_tmpz00_6239);
						}
						BgL_auxz00_6237 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6238);
					}
					BgL_tmpz00_6236 =
						(((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6237))->
						BgL_kapturedzf3zf3);
				}
				return BBOOL(BgL_tmpz00_6236);
			}
		}

	}



/* &<@anonymous:1425> */
	obj_t BGl_z62zc3z04anonymousza31425ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3430)
	{
		{	/* Integrate/iinfo.scm 23 */
			return BUNSPEC;
		}

	}



/* &lambda1424 */
	obj_t BGl_z62lambda1424z62zzintegrate_infoz00(obj_t BgL_envz00_3431,
		obj_t BgL_oz00_3432, obj_t BgL_vz00_3433)
	{
		{	/* Integrate/iinfo.scm 23 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_6246;

				{
					obj_t BgL_auxz00_6247;

					{	/* Integrate/iinfo.scm 23 */
						BgL_objectz00_bglt BgL_tmpz00_6248;

						BgL_tmpz00_6248 =
							((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_3432));
						BgL_auxz00_6247 = BGL_OBJECT_WIDENING(BgL_tmpz00_6248);
					}
					BgL_auxz00_6246 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6247);
				}
				return
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6246))->
						BgL_uzd2markzd2) = ((obj_t) BgL_vz00_3433), BUNSPEC);
			}
		}

	}



/* &lambda1423 */
	obj_t BGl_z62lambda1423z62zzintegrate_infoz00(obj_t BgL_envz00_3434,
		obj_t BgL_oz00_3435)
	{
		{	/* Integrate/iinfo.scm 23 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_6254;

				{
					obj_t BgL_auxz00_6255;

					{	/* Integrate/iinfo.scm 23 */
						BgL_objectz00_bglt BgL_tmpz00_6256;

						BgL_tmpz00_6256 =
							((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_3435));
						BgL_auxz00_6255 = BGL_OBJECT_WIDENING(BgL_tmpz00_6256);
					}
					BgL_auxz00_6254 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6255);
				}
				return
					(((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6254))->
					BgL_uzd2markzd2);
			}
		}

	}



/* &<@anonymous:1382> */
	obj_t BGl_z62zc3z04anonymousza31382ze3ze5zzintegrate_infoz00(obj_t
		BgL_envz00_3436)
	{
		{	/* Integrate/iinfo.scm 23 */
			return BUNSPEC;
		}

	}



/* &lambda1381 */
	obj_t BGl_z62lambda1381z62zzintegrate_infoz00(obj_t BgL_envz00_3437,
		obj_t BgL_oz00_3438, obj_t BgL_vz00_3439)
	{
		{	/* Integrate/iinfo.scm 23 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_6262;

				{
					obj_t BgL_auxz00_6263;

					{	/* Integrate/iinfo.scm 23 */
						BgL_objectz00_bglt BgL_tmpz00_6264;

						BgL_tmpz00_6264 =
							((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_3438));
						BgL_auxz00_6263 = BGL_OBJECT_WIDENING(BgL_tmpz00_6264);
					}
					BgL_auxz00_6262 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6263);
				}
				return
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6262))->
						BgL_fzd2markzd2) = ((obj_t) BgL_vz00_3439), BUNSPEC);
			}
		}

	}



/* &lambda1380 */
	obj_t BGl_z62lambda1380z62zzintegrate_infoz00(obj_t BgL_envz00_3440,
		obj_t BgL_oz00_3441)
	{
		{	/* Integrate/iinfo.scm 23 */
			{
				BgL_svarzf2iinfozf2_bglt BgL_auxz00_6270;

				{
					obj_t BgL_auxz00_6271;

					{	/* Integrate/iinfo.scm 23 */
						BgL_objectz00_bglt BgL_tmpz00_6272;

						BgL_tmpz00_6272 =
							((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_oz00_3441));
						BgL_auxz00_6271 = BGL_OBJECT_WIDENING(BgL_tmpz00_6272);
					}
					BgL_auxz00_6270 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_6271);
				}
				return
					(((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_6270))->
					BgL_fzd2markzd2);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzintegrate_infoz00(void)
	{
		{	/* Integrate/iinfo.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzintegrate_infoz00(void)
	{
		{	/* Integrate/iinfo.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzintegrate_infoz00(void)
	{
		{	/* Integrate/iinfo.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2075z00zzintegrate_infoz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2075z00zzintegrate_infoz00));
			return
				BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2075z00zzintegrate_infoz00));
		}

	}

#ifdef __cplusplus
}
#endif
