/*===========================================================================*/
/*   (Integrate/u.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Integrate/u.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INTEGRATE_U_TYPE_DEFINITIONS
#define BGL_INTEGRATE_U_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_sfunzf2iinfozf2_bgl
	{
		obj_t BgL_ownerz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
		obj_t BgL_cfromz00;
		obj_t BgL_ctoz00;
		obj_t BgL_kz00;
		obj_t BgL_kza2za2;
		obj_t BgL_uz00;
		obj_t BgL_cnz00;
		obj_t BgL_ctz00;
		obj_t BgL_kontz00;
		bool_t BgL_gzf3zf3;
		bool_t BgL_forcegzf3zf3;
		obj_t BgL_lz00;
		obj_t BgL_ledz00;
		obj_t BgL_istampz00;
		obj_t BgL_globalz00;
		obj_t BgL_kapturedz00;
		obj_t BgL_tailzd2coercionzd2;
		bool_t BgL_xhdlzf3zf3;
		obj_t BgL_xhdlsz00;
	}                      *BgL_sfunzf2iinfozf2_bglt;


#endif													// BGL_INTEGRATE_U_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzintegrate_uz00 = BUNSPEC;
	static obj_t BGl_genericzd2initzd2zzintegrate_uz00(void);
	static obj_t BGl_objectzd2initzd2zzintegrate_uz00(void);
	BGL_EXPORTED_DECL obj_t BGl_Uz12z12zzintegrate_uz00(void);
	static obj_t BGl_methodzd2initzd2zzintegrate_uz00(void);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzintegrate_uz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_az00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_infoz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_uz00(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzintegrate_uz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzintegrate_uz00(void);
	static obj_t BGl_z62Uz12z70zzintegrate_uz00(obj_t);
	extern obj_t BGl_za2phiza2z00zzintegrate_az00;
	static obj_t *__cnst;


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_Uz12zd2envzc0zzintegrate_uz00,
		BgL_bgl_za762uza712za770za7za7inte1474za7, BGl_z62Uz12z70zzintegrate_uz00,
		0L, BUNSPEC, 0);
	      DEFINE_STRING(BGl_string1473z00zzintegrate_uz00,
		BgL_bgl_string1473za700za7za7i1475za7, "integrate_u", 11);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzintegrate_uz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzintegrate_uz00(long
		BgL_checksumz00_1766, char *BgL_fromz00_1767)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzintegrate_uz00))
				{
					BGl_requirezd2initializa7ationz75zzintegrate_uz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzintegrate_uz00();
					BGl_libraryzd2moduleszd2initz00zzintegrate_uz00();
					BGl_importedzd2moduleszd2initz00zzintegrate_uz00();
					return BGl_methodzd2initzd2zzintegrate_uz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_uz00(void)
	{
		{	/* Integrate/u.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"integrate_u");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"integrate_u");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "integrate_u");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "integrate_u");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzintegrate_uz00(void)
	{
		{	/* Integrate/u.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* U! */
	BGL_EXPORTED_DEF obj_t BGl_Uz12z12zzintegrate_uz00(void)
	{
		{	/* Integrate/u.scm 28 */
			{
				obj_t BgL_phiz00_1568;

				BgL_phiz00_1568 = BGl_za2phiza2z00zzintegrate_az00;
			BgL_zc3z04anonymousza31245ze3z87_1569:
				if (NULLP(BgL_phiz00_1568))
					{	/* Integrate/u.scm 30 */
						return BNIL;
					}
				else
					{	/* Integrate/u.scm 32 */
						obj_t BgL_pz00_1571;

						BgL_pz00_1571 = CAR(((obj_t) BgL_phiz00_1568));
						{	/* Integrate/u.scm 32 */
							BgL_valuez00_bglt BgL_ifunz00_1572;

							BgL_ifunz00_1572 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_pz00_1571)))->BgL_valuez00);
							{	/* Integrate/u.scm 33 */

								{	/* Integrate/u.scm 34 */
									bool_t BgL_arg1248z00_1573;

									{	/* Integrate/u.scm 34 */
										long BgL_arg1249z00_1574;

										{	/* Integrate/u.scm 34 */
											obj_t BgL_arg1252z00_1575;

											{
												BgL_sfunzf2iinfozf2_bglt BgL_auxz00_1786;

												{
													obj_t BgL_auxz00_1787;

													{	/* Integrate/u.scm 34 */
														BgL_objectz00_bglt BgL_tmpz00_1788;

														BgL_tmpz00_1788 =
															((BgL_objectz00_bglt)
															((BgL_sfunz00_bglt) BgL_ifunz00_1572));
														BgL_auxz00_1787 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_1788);
													}
													BgL_auxz00_1786 =
														((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_1787);
												}
												BgL_arg1252z00_1575 =
													(((BgL_sfunzf2iinfozf2_bglt)
														COBJECT(BgL_auxz00_1786))->BgL_kza2za2);
											}
											BgL_arg1249z00_1574 =
												bgl_list_length(BgL_arg1252z00_1575);
										}
										BgL_arg1248z00_1573 = (BgL_arg1249z00_1574 == 1L);
									}
									{
										BgL_sfunzf2iinfozf2_bglt BgL_auxz00_1796;

										{
											obj_t BgL_auxz00_1797;

											{	/* Integrate/u.scm 34 */
												BgL_objectz00_bglt BgL_tmpz00_1798;

												BgL_tmpz00_1798 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_ifunz00_1572));
												BgL_auxz00_1797 = BGL_OBJECT_WIDENING(BgL_tmpz00_1798);
											}
											BgL_auxz00_1796 =
												((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_1797);
										}
										((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_1796))->
												BgL_uz00) =
											((obj_t) BBOOL(BgL_arg1248z00_1573)), BUNSPEC);
								}}
								{	/* Integrate/u.scm 35 */
									obj_t BgL_arg1268z00_1576;

									BgL_arg1268z00_1576 = CDR(((obj_t) BgL_phiz00_1568));
									{
										obj_t BgL_phiz00_1807;

										BgL_phiz00_1807 = BgL_arg1268z00_1576;
										BgL_phiz00_1568 = BgL_phiz00_1807;
										goto BgL_zc3z04anonymousza31245ze3z87_1569;
									}
								}
							}
						}
					}
			}
		}

	}



/* &U! */
	obj_t BGl_z62Uz12z70zzintegrate_uz00(obj_t BgL_envz00_1765)
	{
		{	/* Integrate/u.scm 28 */
			return BGl_Uz12z12zzintegrate_uz00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzintegrate_uz00(void)
	{
		{	/* Integrate/u.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzintegrate_uz00(void)
	{
		{	/* Integrate/u.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzintegrate_uz00(void)
	{
		{	/* Integrate/u.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzintegrate_uz00(void)
	{
		{	/* Integrate/u.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1473z00zzintegrate_uz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1473z00zzintegrate_uz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1473z00zzintegrate_uz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1473z00zzintegrate_uz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1473z00zzintegrate_uz00));
			BGl_modulezd2initializa7ationz75zzintegrate_infoz00(0L,
				BSTRING_TO_STRING(BGl_string1473z00zzintegrate_uz00));
			return
				BGl_modulezd2initializa7ationz75zzintegrate_az00(523633197L,
				BSTRING_TO_STRING(BGl_string1473z00zzintegrate_uz00));
		}

	}

#ifdef __cplusplus
}
#endif
