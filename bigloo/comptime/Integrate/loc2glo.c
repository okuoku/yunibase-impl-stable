/*===========================================================================*/
/*   (Integrate/loc2glo.scm)                                                 */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Integrate/loc2glo.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_INTEGRATE_LOCALzd2ze3GLOBALz31_TYPE_DEFINITIONS
#define BGL_BgL_INTEGRATE_LOCALzd2ze3GLOBALz31_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_svarzf2iinfozf2_bgl
	{
		obj_t BgL_fzd2markzd2;
		obj_t BgL_uzd2markzd2;
		bool_t BgL_kapturedzf3zf3;
		bool_t BgL_celledzf3zf3;
		obj_t BgL_xhdlz00;
	}                      *BgL_svarzf2iinfozf2_bglt;

	typedef struct BgL_sfunzf2iinfozf2_bgl
	{
		obj_t BgL_ownerz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
		obj_t BgL_cfromz00;
		obj_t BgL_ctoz00;
		obj_t BgL_kz00;
		obj_t BgL_kza2za2;
		obj_t BgL_uz00;
		obj_t BgL_cnz00;
		obj_t BgL_ctz00;
		obj_t BgL_kontz00;
		bool_t BgL_gzf3zf3;
		bool_t BgL_forcegzf3zf3;
		obj_t BgL_lz00;
		obj_t BgL_ledz00;
		obj_t BgL_istampz00;
		obj_t BgL_globalz00;
		obj_t BgL_kapturedz00;
		obj_t BgL_tailzd2coercionzd2;
		bool_t BgL_xhdlzf3zf3;
		obj_t BgL_xhdlsz00;
	}                      *BgL_sfunzf2iinfozf2_bglt;


#endif													// BGL_BgL_INTEGRATE_LOCALzd2ze3GLOBALz31_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern BgL_globalz00_bglt
		BGl_defzd2globalzd2sfunzd2nozd2warningz12z12zzast_glozd2defzd2(obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_localzd2ze3globalz31zzintegrate_localzd2ze3globalz31(BgL_localz00_bglt);
	static obj_t BGl_requirezd2initializa7ationz75zzintegrate_localzd2ze3globalz31
		= BUNSPEC;
	extern obj_t BGl_zb2zd2arityz60zztools_argsz00(obj_t, obj_t);
	static obj_t BGl_symbolzd2quotezd2zzintegrate_localzd2ze3globalz31 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_thezd2globalzd2zzintegrate_localzd2ze3globalz31(BgL_localz00_bglt);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_toplevelzd2initzd2zzintegrate_localzd2ze3globalz31(void);
	static obj_t BGl_z62thezd2globalzb0zzintegrate_localzd2ze3globalz31(obj_t,
		obj_t);
	BGL_IMPORT obj_t bgl_reverse(obj_t);
	static obj_t BGl_genericzd2initzd2zzintegrate_localzd2ze3globalz31(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	static obj_t BGl_objectzd2initzd2zzintegrate_localzd2ze3globalz31(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t
		BGl_localzd2idzd2ze3globalzd2idz31zzintegrate_localzd2ze3globalz31
		(BgL_localz00_bglt);
	extern BgL_localz00_bglt BGl_clonezd2localzd2zzast_localz00(BgL_localz00_bglt,
		BgL_valuez00_bglt);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzintegrate_localzd2ze3globalz31(obj_t,
		obj_t);
	static obj_t BGl_methodzd2initzd2zzintegrate_localzd2ze3globalz31(void);
	extern obj_t
		BGl_integratezd2globaliza7ez12z67zzintegrate_nodez00(BgL_nodez00_bglt,
		BgL_variablez00_bglt, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_svarz00zzast_varz00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_localzd2ze3globalz31(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_nodez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_infoz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_svarzf2Iinfozf2zzintegrate_infoz00;
	static obj_t
		BGl_z62localzd2ze3globalz53zzintegrate_localzd2ze3globalz31(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	extern bool_t
		BGl_integratezd2celledzf3z21zzintegrate_nodez00(BgL_localz00_bglt);
	static obj_t BGl_cnstzd2initzd2zzintegrate_localzd2ze3globalz31(void);
	static obj_t
		BGl_libraryzd2moduleszd2initz00zzintegrate_localzd2ze3globalz31(void);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t
		BGl_importedzd2moduleszd2initz00zzintegrate_localzd2ze3globalz31(void);
	static obj_t BGl_gczd2rootszd2initz00zzintegrate_localzd2ze3globalz31(void);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t __cnst[3];


	   
		 
		DEFINE_STRING(BGl_string1695z00zzintegrate_localzd2ze3globalz31,
		BgL_bgl_string1695za700za7za7i1700za7, "'", 1);
	      DEFINE_STRING(BGl_string1696z00zzintegrate_localzd2ze3globalz31,
		BgL_bgl_string1696za700za7za7i1701za7, "~", 1);
	      DEFINE_STRING(BGl_string1697z00zzintegrate_localzd2ze3globalz31,
		BgL_bgl_string1697za700za7za7i1702za7, "integrate_local->global", 23);
	      DEFINE_STRING(BGl_string1698z00zzintegrate_localzd2ze3globalz31,
		BgL_bgl_string1698za700za7za7i1703za7, "now a-integrated-body sfun ", 27);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_localzd2ze3globalzd2envze3zzintegrate_localzd2ze3globalz31,
		BgL_bgl_za762localza7d2za7e3gl1704za7,
		BGl_z62localzd2ze3globalz53zzintegrate_localzd2ze3globalz31, 0L, BUNSPEC,
		1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_thezd2globalzd2envz00zzintegrate_localzd2ze3globalz31,
		BgL_bgl_za762theza7d2globalza71705za7,
		BGl_z62thezd2globalzb0zzintegrate_localzd2ze3globalz31, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*)
			(&BGl_requirezd2initializa7ationz75zzintegrate_localzd2ze3globalz31));
		     ADD_ROOT((void
				*) (&BGl_symbolzd2quotezd2zzintegrate_localzd2ze3globalz31));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_localzd2ze3globalz31(long
		BgL_checksumz00_2072, char *BgL_fromz00_2073)
	{
		{
			if (CBOOL
				(BGl_requirezd2initializa7ationz75zzintegrate_localzd2ze3globalz31))
				{
					BGl_requirezd2initializa7ationz75zzintegrate_localzd2ze3globalz31 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzintegrate_localzd2ze3globalz31();
					BGl_libraryzd2moduleszd2initz00zzintegrate_localzd2ze3globalz31();
					BGl_cnstzd2initzd2zzintegrate_localzd2ze3globalz31();
					BGl_importedzd2moduleszd2initz00zzintegrate_localzd2ze3globalz31();
					return BGl_toplevelzd2initzd2zzintegrate_localzd2ze3globalz31();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_localzd2ze3globalz31(void)
	{
		{	/* Integrate/loc2glo.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L,
				"integrate_local->global");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"integrate_local->global");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"integrate_local->global");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L,
				"integrate_local->global");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"integrate_local->global");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"integrate_local->global");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"integrate_local->global");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"integrate_local->global");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L,
				"integrate_local->global");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzintegrate_localzd2ze3globalz31(void)
	{
		{	/* Integrate/loc2glo.scm 15 */
			{	/* Integrate/loc2glo.scm 15 */
				obj_t BgL_cportz00_2061;

				{	/* Integrate/loc2glo.scm 15 */
					obj_t BgL_stringz00_2068;

					BgL_stringz00_2068 =
						BGl_string1698z00zzintegrate_localzd2ze3globalz31;
					{	/* Integrate/loc2glo.scm 15 */
						obj_t BgL_startz00_2069;

						BgL_startz00_2069 = BINT(0L);
						{	/* Integrate/loc2glo.scm 15 */
							obj_t BgL_endz00_2070;

							BgL_endz00_2070 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2068)));
							{	/* Integrate/loc2glo.scm 15 */

								BgL_cportz00_2061 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2068, BgL_startz00_2069, BgL_endz00_2070);
				}}}}
				{
					long BgL_iz00_2062;

					BgL_iz00_2062 = 2L;
				BgL_loopz00_2063:
					if ((BgL_iz00_2062 == -1L))
						{	/* Integrate/loc2glo.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Integrate/loc2glo.scm 15 */
							{	/* Integrate/loc2glo.scm 15 */
								obj_t BgL_arg1699z00_2064;

								{	/* Integrate/loc2glo.scm 15 */

									{	/* Integrate/loc2glo.scm 15 */
										obj_t BgL_locationz00_2066;

										BgL_locationz00_2066 = BBOOL(((bool_t) 0));
										{	/* Integrate/loc2glo.scm 15 */

											BgL_arg1699z00_2064 =
												BGl_readz00zz__readerz00(BgL_cportz00_2061,
												BgL_locationz00_2066);
										}
									}
								}
								{	/* Integrate/loc2glo.scm 15 */
									int BgL_tmpz00_2100;

									BgL_tmpz00_2100 = (int) (BgL_iz00_2062);
									CNST_TABLE_SET(BgL_tmpz00_2100, BgL_arg1699z00_2064);
							}}
							{	/* Integrate/loc2glo.scm 15 */
								int BgL_auxz00_2067;

								BgL_auxz00_2067 = (int) ((BgL_iz00_2062 - 1L));
								{
									long BgL_iz00_2105;

									BgL_iz00_2105 = (long) (BgL_auxz00_2067);
									BgL_iz00_2062 = BgL_iz00_2105;
									goto BgL_loopz00_2063;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzintegrate_localzd2ze3globalz31(void)
	{
		{	/* Integrate/loc2glo.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzintegrate_localzd2ze3globalz31(void)
	{
		{	/* Integrate/loc2glo.scm 15 */
			return (BGl_symbolzd2quotezd2zzintegrate_localzd2ze3globalz31 =
				bstring_to_symbol(BGl_string1695z00zzintegrate_localzd2ze3globalz31),
				BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzintegrate_localzd2ze3globalz31(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1571;

				BgL_headz00_1571 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1572;
					obj_t BgL_tailz00_1573;

					BgL_prevz00_1572 = BgL_headz00_1571;
					BgL_tailz00_1573 = BgL_l1z00_1;
				BgL_loopz00_1574:
					if (PAIRP(BgL_tailz00_1573))
						{
							obj_t BgL_newzd2prevzd2_1576;

							BgL_newzd2prevzd2_1576 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1573), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1572, BgL_newzd2prevzd2_1576);
							{
								obj_t BgL_tailz00_2116;
								obj_t BgL_prevz00_2115;

								BgL_prevz00_2115 = BgL_newzd2prevzd2_1576;
								BgL_tailz00_2116 = CDR(BgL_tailz00_1573);
								BgL_tailz00_1573 = BgL_tailz00_2116;
								BgL_prevz00_1572 = BgL_prevz00_2115;
								goto BgL_loopz00_1574;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1571);
				}
			}
		}

	}



/* local->global */
	BGL_EXPORTED_DEF obj_t
		BGl_localzd2ze3globalz31zzintegrate_localzd2ze3globalz31(BgL_localz00_bglt
		BgL_localz00_3)
	{
		{	/* Integrate/loc2glo.scm 35 */
			{	/* Integrate/loc2glo.scm 38 */
				obj_t BgL_globalz00_1579;

				BgL_globalz00_1579 =
					BGl_thezd2globalzd2zzintegrate_localzd2ze3globalz31(BgL_localz00_3);
				{	/* Integrate/loc2glo.scm 38 */
					obj_t BgL_kapturedz00_1580;

					{	/* Integrate/loc2glo.scm 39 */
						BgL_sfunz00_bglt BgL_oz00_1878;

						BgL_oz00_1878 =
							((BgL_sfunz00_bglt)
							(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_localz00_3)))->BgL_valuez00));
						{
							BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2123;

							{
								obj_t BgL_auxz00_2124;

								{	/* Integrate/loc2glo.scm 39 */
									BgL_objectz00_bglt BgL_tmpz00_2125;

									BgL_tmpz00_2125 = ((BgL_objectz00_bglt) BgL_oz00_1878);
									BgL_auxz00_2124 = BGL_OBJECT_WIDENING(BgL_tmpz00_2125);
								}
								BgL_auxz00_2123 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2124);
							}
							BgL_kapturedz00_1580 =
								(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2123))->
								BgL_kapturedz00);
						}
					}
					{	/* Integrate/loc2glo.scm 39 */
						obj_t BgL_addzd2argszd2_1581;

						if (NULLP(BgL_kapturedz00_1580))
							{	/* Integrate/loc2glo.scm 40 */
								BgL_addzd2argszd2_1581 = BNIL;
							}
						else
							{	/* Integrate/loc2glo.scm 40 */
								obj_t BgL_head1268z00_1640;

								BgL_head1268z00_1640 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1266z00_1642;
									obj_t BgL_tail1269z00_1643;

									BgL_l1266z00_1642 = BgL_kapturedz00_1580;
									BgL_tail1269z00_1643 = BgL_head1268z00_1640;
								BgL_zc3z04anonymousza31335ze3z87_1644:
									if (NULLP(BgL_l1266z00_1642))
										{	/* Integrate/loc2glo.scm 40 */
											BgL_addzd2argszd2_1581 = CDR(BgL_head1268z00_1640);
										}
									else
										{	/* Integrate/loc2glo.scm 40 */
											obj_t BgL_newtail1270z00_1646;

											{	/* Integrate/loc2glo.scm 40 */
												BgL_localz00_bglt BgL_arg1340z00_1648;

												{	/* Integrate/loc2glo.scm 40 */
													obj_t BgL_oldz00_1649;

													BgL_oldz00_1649 = CAR(((obj_t) BgL_l1266z00_1642));
													{	/* Integrate/loc2glo.scm 42 */
														BgL_svarz00_bglt BgL_arg1342z00_1650;

														{	/* Integrate/loc2glo.scm 42 */
															BgL_svarz00_bglt BgL_duplicated1112z00_1651;
															BgL_svarz00_bglt BgL_new1110z00_1652;

															BgL_duplicated1112z00_1651 =
																((BgL_svarz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					BgL_oldz00_1649))))->BgL_valuez00));
															{	/* Integrate/loc2glo.scm 42 */
																BgL_svarz00_bglt BgL_tmp1119z00_1658;
																BgL_svarzf2iinfozf2_bglt BgL_wide1120z00_1659;

																{
																	BgL_svarz00_bglt BgL_auxz00_2142;

																	{	/* Integrate/loc2glo.scm 42 */
																		BgL_svarz00_bglt BgL_new1118z00_1661;

																		BgL_new1118z00_1661 =
																			((BgL_svarz00_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_svarz00_bgl))));
																		{	/* Integrate/loc2glo.scm 42 */
																			long BgL_arg1346z00_1662;

																			BgL_arg1346z00_1662 =
																				BGL_CLASS_NUM(BGl_svarz00zzast_varz00);
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1118z00_1661),
																				BgL_arg1346z00_1662);
																		}
																		{	/* Integrate/loc2glo.scm 42 */
																			BgL_objectz00_bglt BgL_tmpz00_2147;

																			BgL_tmpz00_2147 =
																				((BgL_objectz00_bglt)
																				BgL_new1118z00_1661);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2147,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1118z00_1661);
																		BgL_auxz00_2142 = BgL_new1118z00_1661;
																	}
																	BgL_tmp1119z00_1658 =
																		((BgL_svarz00_bglt) BgL_auxz00_2142);
																}
																BgL_wide1120z00_1659 =
																	((BgL_svarzf2iinfozf2_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_svarzf2iinfozf2_bgl))));
																{	/* Integrate/loc2glo.scm 42 */
																	obj_t BgL_auxz00_2155;
																	BgL_objectz00_bglt BgL_tmpz00_2153;

																	BgL_auxz00_2155 =
																		((obj_t) BgL_wide1120z00_1659);
																	BgL_tmpz00_2153 =
																		((BgL_objectz00_bglt) BgL_tmp1119z00_1658);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2153,
																		BgL_auxz00_2155);
																}
																((BgL_objectz00_bglt) BgL_tmp1119z00_1658);
																{	/* Integrate/loc2glo.scm 42 */
																	long BgL_arg1343z00_1660;

																	BgL_arg1343z00_1660 =
																		BGL_CLASS_NUM
																		(BGl_svarzf2Iinfozf2zzintegrate_infoz00);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			BgL_tmp1119z00_1658),
																		BgL_arg1343z00_1660);
																}
																BgL_new1110z00_1652 =
																	((BgL_svarz00_bglt) BgL_tmp1119z00_1658);
															}
															((((BgL_svarz00_bglt) COBJECT(
																			((BgL_svarz00_bglt)
																				BgL_new1110z00_1652)))->BgL_locz00) =
																((obj_t) (((BgL_svarz00_bglt)
																			COBJECT(BgL_duplicated1112z00_1651))->
																		BgL_locz00)), BUNSPEC);
															{
																obj_t BgL_auxz00_2172;
																BgL_svarzf2iinfozf2_bglt BgL_auxz00_2166;

																{
																	BgL_svarzf2iinfozf2_bglt BgL_auxz00_2173;

																	{
																		obj_t BgL_auxz00_2174;

																		{	/* Integrate/loc2glo.scm 42 */
																			BgL_objectz00_bglt BgL_tmpz00_2175;

																			BgL_tmpz00_2175 =
																				((BgL_objectz00_bglt)
																				((BgL_svarz00_bglt)
																					BgL_duplicated1112z00_1651));
																			BgL_auxz00_2174 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_2175);
																		}
																		BgL_auxz00_2173 =
																			((BgL_svarzf2iinfozf2_bglt)
																			BgL_auxz00_2174);
																	}
																	BgL_auxz00_2172 =
																		(((BgL_svarzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_2173))->
																		BgL_fzd2markzd2);
																}
																{
																	obj_t BgL_auxz00_2167;

																	{	/* Integrate/loc2glo.scm 42 */
																		BgL_objectz00_bglt BgL_tmpz00_2168;

																		BgL_tmpz00_2168 =
																			((BgL_objectz00_bglt)
																			BgL_new1110z00_1652);
																		BgL_auxz00_2167 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2168);
																	}
																	BgL_auxz00_2166 =
																		((BgL_svarzf2iinfozf2_bglt)
																		BgL_auxz00_2167);
																}
																((((BgL_svarzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_2166))->
																		BgL_fzd2markzd2) =
																	((obj_t) BgL_auxz00_2172), BUNSPEC);
															}
															{
																obj_t BgL_auxz00_2188;
																BgL_svarzf2iinfozf2_bglt BgL_auxz00_2182;

																{
																	BgL_svarzf2iinfozf2_bglt BgL_auxz00_2189;

																	{
																		obj_t BgL_auxz00_2190;

																		{	/* Integrate/loc2glo.scm 42 */
																			BgL_objectz00_bglt BgL_tmpz00_2191;

																			BgL_tmpz00_2191 =
																				((BgL_objectz00_bglt)
																				((BgL_svarz00_bglt)
																					BgL_duplicated1112z00_1651));
																			BgL_auxz00_2190 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_2191);
																		}
																		BgL_auxz00_2189 =
																			((BgL_svarzf2iinfozf2_bglt)
																			BgL_auxz00_2190);
																	}
																	BgL_auxz00_2188 =
																		(((BgL_svarzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_2189))->
																		BgL_uzd2markzd2);
																}
																{
																	obj_t BgL_auxz00_2183;

																	{	/* Integrate/loc2glo.scm 42 */
																		BgL_objectz00_bglt BgL_tmpz00_2184;

																		BgL_tmpz00_2184 =
																			((BgL_objectz00_bglt)
																			BgL_new1110z00_1652);
																		BgL_auxz00_2183 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2184);
																	}
																	BgL_auxz00_2182 =
																		((BgL_svarzf2iinfozf2_bglt)
																		BgL_auxz00_2183);
																}
																((((BgL_svarzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_2182))->
																		BgL_uzd2markzd2) =
																	((obj_t) BgL_auxz00_2188), BUNSPEC);
															}
															{
																bool_t BgL_auxz00_2204;
																BgL_svarzf2iinfozf2_bglt BgL_auxz00_2198;

																{
																	BgL_svarzf2iinfozf2_bglt BgL_auxz00_2205;

																	{
																		obj_t BgL_auxz00_2206;

																		{	/* Integrate/loc2glo.scm 42 */
																			BgL_objectz00_bglt BgL_tmpz00_2207;

																			BgL_tmpz00_2207 =
																				((BgL_objectz00_bglt)
																				((BgL_svarz00_bglt)
																					BgL_duplicated1112z00_1651));
																			BgL_auxz00_2206 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_2207);
																		}
																		BgL_auxz00_2205 =
																			((BgL_svarzf2iinfozf2_bglt)
																			BgL_auxz00_2206);
																	}
																	BgL_auxz00_2204 =
																		(((BgL_svarzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_2205))->
																		BgL_kapturedzf3zf3);
																}
																{
																	obj_t BgL_auxz00_2199;

																	{	/* Integrate/loc2glo.scm 42 */
																		BgL_objectz00_bglt BgL_tmpz00_2200;

																		BgL_tmpz00_2200 =
																			((BgL_objectz00_bglt)
																			BgL_new1110z00_1652);
																		BgL_auxz00_2199 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2200);
																	}
																	BgL_auxz00_2198 =
																		((BgL_svarzf2iinfozf2_bglt)
																		BgL_auxz00_2199);
																}
																((((BgL_svarzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_2198))->
																		BgL_kapturedzf3zf3) =
																	((bool_t) BgL_auxz00_2204), BUNSPEC);
															}
															{
																bool_t BgL_auxz00_2220;
																BgL_svarzf2iinfozf2_bglt BgL_auxz00_2214;

																{
																	BgL_svarzf2iinfozf2_bglt BgL_auxz00_2221;

																	{
																		obj_t BgL_auxz00_2222;

																		{	/* Integrate/loc2glo.scm 42 */
																			BgL_objectz00_bglt BgL_tmpz00_2223;

																			BgL_tmpz00_2223 =
																				((BgL_objectz00_bglt)
																				((BgL_svarz00_bglt)
																					BgL_duplicated1112z00_1651));
																			BgL_auxz00_2222 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_2223);
																		}
																		BgL_auxz00_2221 =
																			((BgL_svarzf2iinfozf2_bglt)
																			BgL_auxz00_2222);
																	}
																	BgL_auxz00_2220 =
																		(((BgL_svarzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_2221))->
																		BgL_celledzf3zf3);
																}
																{
																	obj_t BgL_auxz00_2215;

																	{	/* Integrate/loc2glo.scm 42 */
																		BgL_objectz00_bglt BgL_tmpz00_2216;

																		BgL_tmpz00_2216 =
																			((BgL_objectz00_bglt)
																			BgL_new1110z00_1652);
																		BgL_auxz00_2215 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2216);
																	}
																	BgL_auxz00_2214 =
																		((BgL_svarzf2iinfozf2_bglt)
																		BgL_auxz00_2215);
																}
																((((BgL_svarzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_2214))->
																		BgL_celledzf3zf3) =
																	((bool_t) BgL_auxz00_2220), BUNSPEC);
															}
															{
																obj_t BgL_auxz00_2236;
																BgL_svarzf2iinfozf2_bglt BgL_auxz00_2230;

																{
																	BgL_svarzf2iinfozf2_bglt BgL_auxz00_2237;

																	{
																		obj_t BgL_auxz00_2238;

																		{	/* Integrate/loc2glo.scm 42 */
																			BgL_objectz00_bglt BgL_tmpz00_2239;

																			BgL_tmpz00_2239 =
																				((BgL_objectz00_bglt)
																				((BgL_svarz00_bglt)
																					BgL_duplicated1112z00_1651));
																			BgL_auxz00_2238 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_2239);
																		}
																		BgL_auxz00_2237 =
																			((BgL_svarzf2iinfozf2_bglt)
																			BgL_auxz00_2238);
																	}
																	BgL_auxz00_2236 =
																		(((BgL_svarzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_2237))->BgL_xhdlz00);
																}
																{
																	obj_t BgL_auxz00_2231;

																	{	/* Integrate/loc2glo.scm 42 */
																		BgL_objectz00_bglt BgL_tmpz00_2232;

																		BgL_tmpz00_2232 =
																			((BgL_objectz00_bglt)
																			BgL_new1110z00_1652);
																		BgL_auxz00_2231 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2232);
																	}
																	BgL_auxz00_2230 =
																		((BgL_svarzf2iinfozf2_bglt)
																		BgL_auxz00_2231);
																}
																((((BgL_svarzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_2230))->BgL_xhdlz00) =
																	((obj_t) BgL_auxz00_2236), BUNSPEC);
															}
															BgL_arg1342z00_1650 = BgL_new1110z00_1652;
														}
														BgL_arg1340z00_1648 =
															BGl_clonezd2localzd2zzast_localz00(
															((BgL_localz00_bglt) BgL_oldz00_1649),
															((BgL_valuez00_bglt) BgL_arg1342z00_1650));
												}}
												BgL_newtail1270z00_1646 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg1340z00_1648), BNIL);
											}
											SET_CDR(BgL_tail1269z00_1643, BgL_newtail1270z00_1646);
											{	/* Integrate/loc2glo.scm 40 */
												obj_t BgL_arg1339z00_1647;

												BgL_arg1339z00_1647 = CDR(((obj_t) BgL_l1266z00_1642));
												{
													obj_t BgL_tail1269z00_2255;
													obj_t BgL_l1266z00_2254;

													BgL_l1266z00_2254 = BgL_arg1339z00_1647;
													BgL_tail1269z00_2255 = BgL_newtail1270z00_1646;
													BgL_tail1269z00_1643 = BgL_tail1269z00_2255;
													BgL_l1266z00_1642 = BgL_l1266z00_2254;
													goto BgL_zc3z04anonymousza31335ze3z87_1644;
												}
											}
										}
								}
							}
						{	/* Integrate/loc2glo.scm 40 */
							BgL_valuez00_bglt BgL_oldzd2funzd2_1582;

							BgL_oldzd2funzd2_1582 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_localz00_3)))->BgL_valuez00);
							{	/* Integrate/loc2glo.scm 45 */
								BgL_sfunz00_bglt BgL_newzd2funzd2_1583;

								{	/* Integrate/loc2glo.scm 46 */
									BgL_sfunz00_bglt BgL_new1122z00_1620;

									{	/* Integrate/loc2glo.scm 47 */
										BgL_sfunz00_bglt BgL_new1136z00_1636;

										BgL_new1136z00_1636 =
											((BgL_sfunz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_sfunz00_bgl))));
										{	/* Integrate/loc2glo.scm 47 */
											long BgL_arg1332z00_1637;

											BgL_arg1332z00_1637 =
												BGL_CLASS_NUM(BGl_sfunz00zzast_varz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1136z00_1636),
												BgL_arg1332z00_1637);
										}
										{	/* Integrate/loc2glo.scm 47 */
											BgL_objectz00_bglt BgL_tmpz00_2262;

											BgL_tmpz00_2262 =
												((BgL_objectz00_bglt) BgL_new1136z00_1636);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2262, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1136z00_1636);
										BgL_new1122z00_1620 = BgL_new1136z00_1636;
									}
									{
										long BgL_auxz00_2266;

										{	/* Integrate/loc2glo.scm 47 */
											long BgL_arg1327z00_1621;
											long BgL_arg1328z00_1622;

											BgL_arg1327z00_1621 =
												(((BgL_funz00_bglt) COBJECT(
														((BgL_funz00_bglt)
															((BgL_sfunz00_bglt) BgL_oldzd2funzd2_1582))))->
												BgL_arityz00);
											BgL_arg1328z00_1622 =
												bgl_list_length(BgL_addzd2argszd2_1581);
											BgL_auxz00_2266 =
												(long)
												CINT(BGl_zb2zd2arityz60zztools_argsz00(BINT
													(BgL_arg1327z00_1621), BINT(BgL_arg1328z00_1622)));
										}
										((((BgL_funz00_bglt) COBJECT(
														((BgL_funz00_bglt) BgL_new1122z00_1620)))->
												BgL_arityz00) = ((long) BgL_auxz00_2266), BUNSPEC);
									}
									((((BgL_funz00_bglt) COBJECT(
													((BgL_funz00_bglt) BgL_new1122z00_1620)))->
											BgL_sidezd2effectzd2) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1582)))->BgL_sidezd2effectzd2)),
										BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1122z00_1620)))->BgL_predicatezd2ofzd2) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1582)))->BgL_predicatezd2ofzd2)),
										BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1122z00_1620)))->BgL_stackzd2allocatorzd2) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1582)))->
												BgL_stackzd2allocatorzd2)), BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1122z00_1620)))->BgL_topzf3zf3) =
										((bool_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1582)))->BgL_topzf3zf3)),
										BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1122z00_1620)))->BgL_thezd2closurezd2) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1582)))->BgL_thezd2closurezd2)),
										BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1122z00_1620)))->BgL_effectz00) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1582)))->BgL_effectz00)),
										BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1122z00_1620)))->BgL_failsafez00) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1582)))->BgL_failsafez00)),
										BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1122z00_1620)))->BgL_argszd2noescapezd2) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1582)))->
												BgL_argszd2noescapezd2)), BUNSPEC);
									((((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
														BgL_new1122z00_1620)))->BgL_argszd2retescapezd2) =
										((obj_t) (((BgL_funz00_bglt) COBJECT(((BgL_funz00_bglt)
															BgL_oldzd2funzd2_1582)))->
												BgL_argszd2retescapezd2)), BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1122z00_1620))->
											BgL_propertyz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1582))))->BgL_propertyz00)),
										BUNSPEC);
									{
										obj_t BgL_auxz00_2317;

										{	/* Integrate/loc2glo.scm 48 */
											obj_t BgL_arg1329z00_1624;
											obj_t BgL_arg1331z00_1625;

											BgL_arg1329z00_1624 = bgl_reverse(BgL_addzd2argszd2_1581);
											BgL_arg1331z00_1625 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_oldzd2funzd2_1582)))->
												BgL_argsz00);
											BgL_auxz00_2317 =
												BGl_appendzd221011zd2zzintegrate_localzd2ze3globalz31
												(BgL_arg1329z00_1624, BgL_arg1331z00_1625);
										}
										((((BgL_sfunz00_bglt) COBJECT(BgL_new1122z00_1620))->
												BgL_argsz00) = ((obj_t) BgL_auxz00_2317), BUNSPEC);
									}
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1122z00_1620))->
											BgL_argszd2namezd2) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1582))))->BgL_argszd2namezd2)),
										BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1122z00_1620))->
											BgL_bodyz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1582))))->BgL_bodyz00)),
										BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1122z00_1620))->
											BgL_classz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1582))))->BgL_classz00)),
										BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1122z00_1620))->
											BgL_dssslzd2keywordszd2) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1582))))->
												BgL_dssslzd2keywordszd2)), BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1122z00_1620))->
											BgL_locz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1582))))->BgL_locz00)),
										BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1122z00_1620))->
											BgL_optionalsz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1582))))->BgL_optionalsz00)),
										BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1122z00_1620))->
											BgL_keysz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1582))))->BgL_keysz00)),
										BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1122z00_1620))->
											BgL_thezd2closurezd2globalz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1582))))->
												BgL_thezd2closurezd2globalz00)), BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1122z00_1620))->
											BgL_strengthz00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1582))))->BgL_strengthz00)),
										BUNSPEC);
									((((BgL_sfunz00_bglt) COBJECT(BgL_new1122z00_1620))->
											BgL_stackablez00) =
										((obj_t) (((BgL_sfunz00_bglt)
													COBJECT(((BgL_sfunz00_bglt) ((BgL_funz00_bglt)
																BgL_oldzd2funzd2_1582))))->BgL_stackablez00)),
										BUNSPEC);
									BgL_newzd2funzd2_1583 = BgL_new1122z00_1620;
								}
								{	/* Integrate/loc2glo.scm 46 */

									{	/* Integrate/loc2glo.scm 51 */
										BgL_typez00_bglt BgL_arg1305z00_1584;

										BgL_arg1305z00_1584 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_localz00_3)))->
											BgL_typez00);
										((((BgL_variablez00_bglt)
													COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
																BgL_globalz00_1579))))->BgL_typez00) =
											((BgL_typez00_bglt) BgL_arg1305z00_1584), BUNSPEC);
									}
									{	/* Integrate/loc2glo.scm 52 */
										obj_t BgL_g1273z00_1585;

										BgL_g1273z00_1585 =
											(((BgL_sfunz00_bglt) COBJECT(BgL_newzd2funzd2_1583))->
											BgL_argsz00);
										{
											obj_t BgL_l1271z00_1587;

											BgL_l1271z00_1587 = BgL_g1273z00_1585;
										BgL_zc3z04anonymousza31306ze3z87_1588:
											if (PAIRP(BgL_l1271z00_1587))
												{	/* Integrate/loc2glo.scm 55 */
													{	/* Integrate/loc2glo.scm 53 */
														obj_t BgL_lz00_1590;

														BgL_lz00_1590 = CAR(BgL_l1271z00_1587);
														if (BGl_integratezd2celledzf3z21zzintegrate_nodez00(
																((BgL_localz00_bglt) BgL_lz00_1590)))
															{	/* Integrate/loc2glo.scm 54 */
																BgL_typez00_bglt BgL_vz00_1916;

																BgL_vz00_1916 =
																	((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00);
																((((BgL_variablez00_bglt)
																			COBJECT(((BgL_variablez00_bglt) (
																						(BgL_localz00_bglt)
																						BgL_lz00_1590))))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_vz00_1916), BUNSPEC);
															}
														else
															{	/* Integrate/loc2glo.scm 53 */
																BFALSE;
															}
													}
													{
														obj_t BgL_l1271z00_2379;

														BgL_l1271z00_2379 = CDR(BgL_l1271z00_1587);
														BgL_l1271z00_1587 = BgL_l1271z00_2379;
														goto BgL_zc3z04anonymousza31306ze3z87_1588;
													}
												}
											else
												{	/* Integrate/loc2glo.scm 55 */
													((bool_t) 1);
												}
										}
									}
									{	/* Integrate/loc2glo.scm 57 */
										obj_t BgL_arg1311z00_1594;

										{	/* Integrate/loc2glo.scm 57 */
											obj_t BgL_arg1312z00_1595;
											obj_t BgL_arg1314z00_1596;

											BgL_arg1312z00_1595 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_oldzd2funzd2_1582)))->
												BgL_bodyz00);
											if (NULLP(BgL_kapturedz00_1580))
												{	/* Integrate/loc2glo.scm 59 */
													BgL_arg1314z00_1596 = BNIL;
												}
											else
												{	/* Integrate/loc2glo.scm 59 */
													obj_t BgL_head1276z00_1600;

													{	/* Integrate/loc2glo.scm 59 */
														obj_t BgL_arg1323z00_1616;

														{	/* Integrate/loc2glo.scm 59 */
															obj_t BgL_arg1325z00_1617;
															obj_t BgL_arg1326z00_1618;

															BgL_arg1325z00_1617 =
																CAR(((obj_t) BgL_kapturedz00_1580));
															BgL_arg1326z00_1618 =
																CAR(((obj_t) BgL_addzd2argszd2_1581));
															BgL_arg1323z00_1616 =
																MAKE_YOUNG_PAIR(BgL_arg1325z00_1617,
																BgL_arg1326z00_1618);
														}
														BgL_head1276z00_1600 =
															MAKE_YOUNG_PAIR(BgL_arg1323z00_1616, BNIL);
													}
													{	/* Integrate/loc2glo.scm 59 */
														obj_t BgL_g1280z00_1601;
														obj_t BgL_g1281z00_1602;

														BgL_g1280z00_1601 =
															CDR(((obj_t) BgL_kapturedz00_1580));
														BgL_g1281z00_1602 =
															CDR(((obj_t) BgL_addzd2argszd2_1581));
														{
															obj_t BgL_ll1274z00_1604;
															obj_t BgL_ll1275z00_1605;
															obj_t BgL_tail1277z00_1606;

															BgL_ll1274z00_1604 = BgL_g1280z00_1601;
															BgL_ll1275z00_1605 = BgL_g1281z00_1602;
															BgL_tail1277z00_1606 = BgL_head1276z00_1600;
														BgL_zc3z04anonymousza31316ze3z87_1607:
															if (NULLP(BgL_ll1274z00_1604))
																{	/* Integrate/loc2glo.scm 59 */
																	BgL_arg1314z00_1596 = BgL_head1276z00_1600;
																}
															else
																{	/* Integrate/loc2glo.scm 59 */
																	obj_t BgL_newtail1278z00_1609;

																	{	/* Integrate/loc2glo.scm 59 */
																		obj_t BgL_arg1320z00_1612;

																		{	/* Integrate/loc2glo.scm 59 */
																			obj_t BgL_arg1321z00_1613;
																			obj_t BgL_arg1322z00_1614;

																			BgL_arg1321z00_1613 =
																				CAR(((obj_t) BgL_ll1274z00_1604));
																			BgL_arg1322z00_1614 =
																				CAR(((obj_t) BgL_ll1275z00_1605));
																			BgL_arg1320z00_1612 =
																				MAKE_YOUNG_PAIR(BgL_arg1321z00_1613,
																				BgL_arg1322z00_1614);
																		}
																		BgL_newtail1278z00_1609 =
																			MAKE_YOUNG_PAIR(BgL_arg1320z00_1612,
																			BNIL);
																	}
																	SET_CDR(BgL_tail1277z00_1606,
																		BgL_newtail1278z00_1609);
																	{	/* Integrate/loc2glo.scm 59 */
																		obj_t BgL_arg1318z00_1610;
																		obj_t BgL_arg1319z00_1611;

																		BgL_arg1318z00_1610 =
																			CDR(((obj_t) BgL_ll1274z00_1604));
																		BgL_arg1319z00_1611 =
																			CDR(((obj_t) BgL_ll1275z00_1605));
																		{
																			obj_t BgL_tail1277z00_2410;
																			obj_t BgL_ll1275z00_2409;
																			obj_t BgL_ll1274z00_2408;

																			BgL_ll1274z00_2408 = BgL_arg1318z00_1610;
																			BgL_ll1275z00_2409 = BgL_arg1319z00_1611;
																			BgL_tail1277z00_2410 =
																				BgL_newtail1278z00_1609;
																			BgL_tail1277z00_1606 =
																				BgL_tail1277z00_2410;
																			BgL_ll1275z00_1605 = BgL_ll1275z00_2409;
																			BgL_ll1274z00_1604 = BgL_ll1274z00_2408;
																			goto
																				BgL_zc3z04anonymousza31316ze3z87_1607;
																		}
																	}
																}
														}
													}
												}
											BgL_arg1311z00_1594 =
												BGl_integratezd2globaliza7ez12z67zzintegrate_nodez00(
												((BgL_nodez00_bglt) BgL_arg1312z00_1595),
												((BgL_variablez00_bglt) BgL_localz00_3),
												BgL_arg1314z00_1596);
										}
										((((BgL_sfunz00_bglt) COBJECT(BgL_newzd2funzd2_1583))->
												BgL_bodyz00) = ((obj_t) BgL_arg1311z00_1594), BUNSPEC);
									}
									((((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_globalz00_1579))))->
											BgL_valuez00) =
										((BgL_valuez00_bglt) ((BgL_valuez00_bglt)
												BgL_newzd2funzd2_1583)), BUNSPEC);
									return BgL_globalz00_1579;
								}
							}
						}
					}
				}
			}
		}

	}



/* &local->global */
	obj_t BGl_z62localzd2ze3globalz53zzintegrate_localzd2ze3globalz31(obj_t
		BgL_envz00_2057, obj_t BgL_localz00_2058)
	{
		{	/* Integrate/loc2glo.scm 35 */
			return
				BGl_localzd2ze3globalz31zzintegrate_localzd2ze3globalz31(
				((BgL_localz00_bglt) BgL_localz00_2058));
		}

	}



/* local-id->global-id */
	obj_t
		BGl_localzd2idzd2ze3globalzd2idz31zzintegrate_localzd2ze3globalz31
		(BgL_localz00_bglt BgL_localz00_4)
	{
		{	/* Integrate/loc2glo.scm 73 */
			{	/* Integrate/loc2glo.scm 74 */
				obj_t BgL_pz00_1665;

				{	/* Integrate/loc2glo.scm 74 */
					obj_t BgL_arg1371z00_1679;

					{	/* Integrate/loc2glo.scm 74 */
						obj_t BgL_arg1375z00_1680;

						BgL_arg1375z00_1680 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_localz00_4)))->BgL_idz00);
						{	/* Integrate/loc2glo.scm 74 */
							obj_t BgL_arg1455z00_1933;

							BgL_arg1455z00_1933 = SYMBOL_TO_STRING(BgL_arg1375z00_1680);
							BgL_arg1371z00_1679 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_1933);
						}
					}
					BgL_pz00_1665 =
						string_append(BgL_arg1371z00_1679,
						BGl_string1696z00zzintegrate_localzd2ze3globalz31);
				}
				{
					long BgL_countz00_1667;

					BgL_countz00_1667 = 0L;
				BgL_zc3z04anonymousza31349ze3z87_1668:
					{	/* Integrate/loc2glo.scm 76 */
						obj_t BgL_idz00_1669;

						{	/* Integrate/loc2glo.scm 76 */
							obj_t BgL_arg1367z00_1674;

							{	/* Integrate/loc2glo.scm 76 */
								obj_t BgL_arg1370z00_1675;

								{	/* Integrate/loc2glo.scm 76 */

									BgL_arg1370z00_1675 =
										BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
										(BgL_countz00_1667, 10L);
								}
								BgL_arg1367z00_1674 =
									string_append(BgL_pz00_1665, BgL_arg1370z00_1675);
							}
							BgL_idz00_1669 = bstring_to_symbol(BgL_arg1367z00_1674);
						}
						{	/* Integrate/loc2glo.scm 77 */
							bool_t BgL_test1720z00_2429;

							{	/* Integrate/loc2glo.scm 77 */
								obj_t BgL_arg1364z00_1673;

								BgL_arg1364z00_1673 =
									BGl_findzd2globalzf2modulez20zzast_envz00(BgL_idz00_1669,
									BGl_za2moduleza2z00zzmodule_modulez00);
								{	/* Integrate/loc2glo.scm 77 */
									obj_t BgL_classz00_1935;

									BgL_classz00_1935 = BGl_globalz00zzast_varz00;
									if (BGL_OBJECTP(BgL_arg1364z00_1673))
										{	/* Integrate/loc2glo.scm 77 */
											BgL_objectz00_bglt BgL_arg1807z00_1937;

											BgL_arg1807z00_1937 =
												(BgL_objectz00_bglt) (BgL_arg1364z00_1673);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Integrate/loc2glo.scm 77 */
													long BgL_idxz00_1943;

													BgL_idxz00_1943 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1937);
													BgL_test1720z00_2429 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_1943 + 2L)) == BgL_classz00_1935);
												}
											else
												{	/* Integrate/loc2glo.scm 77 */
													bool_t BgL_res1692z00_1968;

													{	/* Integrate/loc2glo.scm 77 */
														obj_t BgL_oclassz00_1951;

														{	/* Integrate/loc2glo.scm 77 */
															obj_t BgL_arg1815z00_1959;
															long BgL_arg1816z00_1960;

															BgL_arg1815z00_1959 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Integrate/loc2glo.scm 77 */
																long BgL_arg1817z00_1961;

																BgL_arg1817z00_1961 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1937);
																BgL_arg1816z00_1960 =
																	(BgL_arg1817z00_1961 - OBJECT_TYPE);
															}
															BgL_oclassz00_1951 =
																VECTOR_REF(BgL_arg1815z00_1959,
																BgL_arg1816z00_1960);
														}
														{	/* Integrate/loc2glo.scm 77 */
															bool_t BgL__ortest_1115z00_1952;

															BgL__ortest_1115z00_1952 =
																(BgL_classz00_1935 == BgL_oclassz00_1951);
															if (BgL__ortest_1115z00_1952)
																{	/* Integrate/loc2glo.scm 77 */
																	BgL_res1692z00_1968 =
																		BgL__ortest_1115z00_1952;
																}
															else
																{	/* Integrate/loc2glo.scm 77 */
																	long BgL_odepthz00_1953;

																	{	/* Integrate/loc2glo.scm 77 */
																		obj_t BgL_arg1804z00_1954;

																		BgL_arg1804z00_1954 = (BgL_oclassz00_1951);
																		BgL_odepthz00_1953 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_1954);
																	}
																	if ((2L < BgL_odepthz00_1953))
																		{	/* Integrate/loc2glo.scm 77 */
																			obj_t BgL_arg1802z00_1956;

																			{	/* Integrate/loc2glo.scm 77 */
																				obj_t BgL_arg1803z00_1957;

																				BgL_arg1803z00_1957 =
																					(BgL_oclassz00_1951);
																				BgL_arg1802z00_1956 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_1957, 2L);
																			}
																			BgL_res1692z00_1968 =
																				(BgL_arg1802z00_1956 ==
																				BgL_classz00_1935);
																		}
																	else
																		{	/* Integrate/loc2glo.scm 77 */
																			BgL_res1692z00_1968 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1720z00_2429 = BgL_res1692z00_1968;
												}
										}
									else
										{	/* Integrate/loc2glo.scm 77 */
											BgL_test1720z00_2429 = ((bool_t) 0);
										}
								}
							}
							if (BgL_test1720z00_2429)
								{
									long BgL_countz00_2453;

									BgL_countz00_2453 = (BgL_countz00_1667 + 1L);
									BgL_countz00_1667 = BgL_countz00_2453;
									goto BgL_zc3z04anonymousza31349ze3z87_1668;
								}
							else
								{	/* Integrate/loc2glo.scm 77 */
									return BgL_idz00_1669;
								}
						}
					}
				}
			}
		}

	}



/* the-global */
	BGL_EXPORTED_DEF obj_t
		BGl_thezd2globalzd2zzintegrate_localzd2ze3globalz31(BgL_localz00_bglt
		BgL_localz00_5)
	{
		{	/* Integrate/loc2glo.scm 84 */
			{	/* Integrate/loc2glo.scm 85 */
				BgL_valuez00_bglt BgL_valuez00_1681;

				BgL_valuez00_1681 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_localz00_5)))->BgL_valuez00);
				{	/* Integrate/loc2glo.scm 86 */
					bool_t BgL_test1726z00_2457;

					{	/* Integrate/loc2glo.scm 86 */
						obj_t BgL_arg1434z00_1691;

						{
							BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2458;

							{
								obj_t BgL_auxz00_2459;

								{	/* Integrate/loc2glo.scm 86 */
									BgL_objectz00_bglt BgL_tmpz00_2460;

									BgL_tmpz00_2460 =
										((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) BgL_valuez00_1681));
									BgL_auxz00_2459 = BGL_OBJECT_WIDENING(BgL_tmpz00_2460);
								}
								BgL_auxz00_2458 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2459);
							}
							BgL_arg1434z00_1691 =
								(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2458))->
								BgL_globalz00);
						}
						{	/* Integrate/loc2glo.scm 86 */
							obj_t BgL_classz00_1973;

							BgL_classz00_1973 = BGl_globalz00zzast_varz00;
							if (BGL_OBJECTP(BgL_arg1434z00_1691))
								{	/* Integrate/loc2glo.scm 86 */
									BgL_objectz00_bglt BgL_arg1807z00_1975;

									BgL_arg1807z00_1975 =
										(BgL_objectz00_bglt) (BgL_arg1434z00_1691);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Integrate/loc2glo.scm 86 */
											long BgL_idxz00_1981;

											BgL_idxz00_1981 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_1975);
											BgL_test1726z00_2457 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_1981 + 2L)) == BgL_classz00_1973);
										}
									else
										{	/* Integrate/loc2glo.scm 86 */
											bool_t BgL_res1693z00_2006;

											{	/* Integrate/loc2glo.scm 86 */
												obj_t BgL_oclassz00_1989;

												{	/* Integrate/loc2glo.scm 86 */
													obj_t BgL_arg1815z00_1997;
													long BgL_arg1816z00_1998;

													BgL_arg1815z00_1997 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Integrate/loc2glo.scm 86 */
														long BgL_arg1817z00_1999;

														BgL_arg1817z00_1999 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_1975);
														BgL_arg1816z00_1998 =
															(BgL_arg1817z00_1999 - OBJECT_TYPE);
													}
													BgL_oclassz00_1989 =
														VECTOR_REF(BgL_arg1815z00_1997,
														BgL_arg1816z00_1998);
												}
												{	/* Integrate/loc2glo.scm 86 */
													bool_t BgL__ortest_1115z00_1990;

													BgL__ortest_1115z00_1990 =
														(BgL_classz00_1973 == BgL_oclassz00_1989);
													if (BgL__ortest_1115z00_1990)
														{	/* Integrate/loc2glo.scm 86 */
															BgL_res1693z00_2006 = BgL__ortest_1115z00_1990;
														}
													else
														{	/* Integrate/loc2glo.scm 86 */
															long BgL_odepthz00_1991;

															{	/* Integrate/loc2glo.scm 86 */
																obj_t BgL_arg1804z00_1992;

																BgL_arg1804z00_1992 = (BgL_oclassz00_1989);
																BgL_odepthz00_1991 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_1992);
															}
															if ((2L < BgL_odepthz00_1991))
																{	/* Integrate/loc2glo.scm 86 */
																	obj_t BgL_arg1802z00_1994;

																	{	/* Integrate/loc2glo.scm 86 */
																		obj_t BgL_arg1803z00_1995;

																		BgL_arg1803z00_1995 = (BgL_oclassz00_1989);
																		BgL_arg1802z00_1994 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_1995, 2L);
																	}
																	BgL_res1693z00_2006 =
																		(BgL_arg1802z00_1994 == BgL_classz00_1973);
																}
															else
																{	/* Integrate/loc2glo.scm 86 */
																	BgL_res1693z00_2006 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1726z00_2457 = BgL_res1693z00_2006;
										}
								}
							else
								{	/* Integrate/loc2glo.scm 86 */
									BgL_test1726z00_2457 = ((bool_t) 0);
								}
						}
					}
					if (BgL_test1726z00_2457)
						{
							BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2488;

							{
								obj_t BgL_auxz00_2489;

								{	/* Integrate/loc2glo.scm 87 */
									BgL_objectz00_bglt BgL_tmpz00_2490;

									BgL_tmpz00_2490 =
										((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) BgL_valuez00_1681));
									BgL_auxz00_2489 = BGL_OBJECT_WIDENING(BgL_tmpz00_2490);
								}
								BgL_auxz00_2488 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2489);
							}
							return
								(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2488))->
								BgL_globalz00);
						}
					else
						{	/* Integrate/loc2glo.scm 88 */
							obj_t BgL_idz00_1684;

							BgL_idz00_1684 =
								BGl_localzd2idzd2ze3globalzd2idz31zzintegrate_localzd2ze3globalz31
								(BgL_localz00_5);
							{	/* Integrate/loc2glo.scm 88 */
								BgL_globalz00_bglt BgL_globalz00_1685;

								BgL_globalz00_1685 =
									BGl_defzd2globalzd2sfunzd2nozd2warningz12z12zzast_glozd2defzd2
									(BgL_idz00_1684, BNIL, BNIL,
									BGl_za2moduleza2z00zzmodule_modulez00, CNST_TABLE_REF(0),
									CNST_TABLE_REF(1), CNST_TABLE_REF(2), BUNSPEC);
								{	/* Integrate/loc2glo.scm 89 */

									{	/* Integrate/loc2glo.scm 102 */
										BgL_valuez00_bglt BgL_arg1408z00_1686;
										obj_t BgL_arg1410z00_1687;

										BgL_arg1408z00_1686 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_globalz00_1685)))->
											BgL_valuez00);
										BgL_arg1410z00_1687 =
											(((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
														BgL_valuez00_1681)))->BgL_locz00);
										((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
															BgL_arg1408z00_1686)))->BgL_locz00) =
											((obj_t) BgL_arg1410z00_1687), BUNSPEC);
									}
									if (
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_localz00_5)))->
											BgL_userzf3zf3))
										{	/* Integrate/loc2glo.scm 104 */
											BFALSE;
										}
									else
										{	/* Integrate/loc2glo.scm 104 */
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_globalz00_1685)))->
													BgL_userzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
										}
									{
										BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2512;

										{
											obj_t BgL_auxz00_2513;

											{	/* Integrate/loc2glo.scm 106 */
												BgL_objectz00_bglt BgL_tmpz00_2514;

												BgL_tmpz00_2514 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_valuez00_1681));
												BgL_auxz00_2513 = BGL_OBJECT_WIDENING(BgL_tmpz00_2514);
											}
											BgL_auxz00_2512 =
												((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2513);
										}
										((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2512))->
												BgL_globalz00) =
											((obj_t) ((obj_t) BgL_globalz00_1685)), BUNSPEC);
									}
									{	/* Integrate/loc2glo.scm 107 */
										BgL_valuez00_bglt BgL_arg1421z00_1689;
										obj_t BgL_arg1422z00_1690;

										BgL_arg1421z00_1689 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_globalz00_1685)))->
											BgL_valuez00);
										BgL_arg1422z00_1690 =
											(((BgL_funz00_bglt)
												COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
															BgL_valuez00_1681))))->BgL_sidezd2effectzd2);
										((((BgL_funz00_bglt)
													COBJECT(((BgL_funz00_bglt) ((BgL_sfunz00_bglt)
																BgL_arg1421z00_1689))))->BgL_sidezd2effectzd2) =
											((obj_t) BgL_arg1422z00_1690), BUNSPEC);
									}
									return ((obj_t) BgL_globalz00_1685);
								}
							}
						}
				}
			}
		}

	}



/* &the-global */
	obj_t BGl_z62thezd2globalzb0zzintegrate_localzd2ze3globalz31(obj_t
		BgL_envz00_2059, obj_t BgL_localz00_2060)
	{
		{	/* Integrate/loc2glo.scm 84 */
			return
				BGl_thezd2globalzd2zzintegrate_localzd2ze3globalz31(
				((BgL_localz00_bglt) BgL_localz00_2060));
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzintegrate_localzd2ze3globalz31(void)
	{
		{	/* Integrate/loc2glo.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzintegrate_localzd2ze3globalz31(void)
	{
		{	/* Integrate/loc2glo.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzintegrate_localzd2ze3globalz31(void)
	{
		{	/* Integrate/loc2glo.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzintegrate_localzd2ze3globalz31(void)
	{
		{	/* Integrate/loc2glo.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1697z00zzintegrate_localzd2ze3globalz31));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1697z00zzintegrate_localzd2ze3globalz31));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string1697z00zzintegrate_localzd2ze3globalz31));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1697z00zzintegrate_localzd2ze3globalz31));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1697z00zzintegrate_localzd2ze3globalz31));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1697z00zzintegrate_localzd2ze3globalz31));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1697z00zzintegrate_localzd2ze3globalz31));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1697z00zzintegrate_localzd2ze3globalz31));
			BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(44601789L,
				BSTRING_TO_STRING(BGl_string1697z00zzintegrate_localzd2ze3globalz31));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1697z00zzintegrate_localzd2ze3globalz31));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1697z00zzintegrate_localzd2ze3globalz31));
			BGl_modulezd2initializa7ationz75zzintegrate_infoz00(0L,
				BSTRING_TO_STRING(BGl_string1697z00zzintegrate_localzd2ze3globalz31));
			return
				BGl_modulezd2initializa7ationz75zzintegrate_nodez00(347237104L,
				BSTRING_TO_STRING(BGl_string1697z00zzintegrate_localzd2ze3globalz31));
		}

	}

#ifdef __cplusplus
}
#endif
