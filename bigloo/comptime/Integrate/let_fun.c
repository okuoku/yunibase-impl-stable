/*===========================================================================*/
/*   (Integrate/let_fun.scm)                                                 */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Integrate/let_fun.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_INTEGRATE_LETzd2FUNzd2_TYPE_DEFINITIONS
#define BGL_BgL_INTEGRATE_LETzd2FUNzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_sfunzf2iinfozf2_bgl
	{
		obj_t BgL_ownerz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
		obj_t BgL_cfromz00;
		obj_t BgL_ctoz00;
		obj_t BgL_kz00;
		obj_t BgL_kza2za2;
		obj_t BgL_uz00;
		obj_t BgL_cnz00;
		obj_t BgL_ctz00;
		obj_t BgL_kontz00;
		bool_t BgL_gzf3zf3;
		bool_t BgL_forcegzf3zf3;
		obj_t BgL_lz00;
		obj_t BgL_ledz00;
		obj_t BgL_istampz00;
		obj_t BgL_globalz00;
		obj_t BgL_kapturedz00;
		obj_t BgL_tailzd2coercionzd2;
		bool_t BgL_xhdlzf3zf3;
		obj_t BgL_xhdlsz00;
	}                      *BgL_sfunzf2iinfozf2_bglt;


#endif													// BGL_BgL_INTEGRATE_LETzd2FUNzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzintegrate_letzd2funzd2 =
		BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_bindzd2funz12zc0zzintegrate_letzd2funzd2(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzintegrate_letzd2funzd2(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzintegrate_letzd2funzd2(void);
	static obj_t BGl_objectzd2initzd2zzintegrate_letzd2funzd2(void);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1301zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1303zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1305zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1307zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1309zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzintegrate_letzd2funzd2(void);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1311zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1313zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1315zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t
		BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2
		(BgL_nodez00_bglt, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1270zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1273zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1275zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1277zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1279zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1281zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1283zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1285zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1287zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1289zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1291zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1293zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1295zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1297zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62displacezd2letzd2funzd2nod1299zb0zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_displacezd2letzd2funz12z12zzintegrate_letzd2funzd2(obj_t);
	static bool_t BGl_freezd2funzf3z21zzintegrate_letzd2funzd2(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_letzd2funzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_infoz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzintegrate_letzd2funzd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_letzd2funzd2(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzintegrate_letzd2funzd2(void);
	static obj_t BGl_gczd2rootszd2initz00zzintegrate_letzd2funzd2(void);
	static long BGl_za2stampza2z00zzintegrate_letzd2funzd2 = 0L;
	static obj_t
		BGl_z62displacezd2letzd2funzd2nodez12za2zzintegrate_letzd2funzd2(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t BGl_z62displacezd2letzd2funz12z70zzintegrate_letzd2funzd2(obj_t,
		obj_t);
	static obj_t __cnst[1];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_displacezd2letzd2funz12zd2envzc0zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1821z00,
		BGl_z62displacezd2letzd2funz12z70zzintegrate_letzd2funzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1800z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1822z00,
		BGl_z62displacezd2letzd2funzd2nod1295zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1801z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1823z00,
		BGl_z62displacezd2letzd2funzd2nod1297zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1802z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1824z00,
		BGl_z62displacezd2letzd2funzd2nod1299zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1803z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1825z00,
		BGl_z62displacezd2letzd2funzd2nod1301zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1804z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1826z00,
		BGl_z62displacezd2letzd2funzd2nod1303zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1811z00zzintegrate_letzd2funzd2,
		BgL_bgl_string1811za700za7za7i1827za7, "displace-let-fun-node", 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1805z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1828z00,
		BGl_z62displacezd2letzd2funzd2nod1305zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1812z00zzintegrate_letzd2funzd2,
		BgL_bgl_string1812za700za7za7i1829za7, "Unexpected closure", 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1806z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1830z00,
		BGl_z62displacezd2letzd2funzd2nod1307zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1813z00zzintegrate_letzd2funzd2,
		BgL_bgl_string1813za700za7za7i1831za7, "integrate_let-fun", 17);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1807z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1832z00,
		BGl_z62displacezd2letzd2funzd2nod1309zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1814z00zzintegrate_letzd2funzd2,
		BgL_bgl_string1814za700za7za7i1833za7, "displace-let-fun-nod1270 ", 25);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1808z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1834z00,
		BGl_z62displacezd2letzd2funzd2nod1311zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1809z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1835z00,
		BGl_z62displacezd2letzd2funzd2nod1313zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1810z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1836z00,
		BGl_z62displacezd2letzd2funzd2nod1315zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1786z00zzintegrate_letzd2funzd2,
		BgL_bgl_string1786za700za7za7i1837za7, "displace-let-fun-nod1270", 24);
	      DEFINE_STRING(BGl_string1787z00zzintegrate_letzd2funzd2,
		BgL_bgl_string1787za700za7za7i1838za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1789z00zzintegrate_letzd2funzd2,
		BgL_bgl_string1789za700za7za7i1839za7, "displace-let-fun-node!", 22);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1785z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1840z00,
		BGl_z62displacezd2letzd2funzd2nod1270zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1788z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1841z00,
		BGl_z62displacezd2letzd2funzd2nod1273zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1842z00,
		BGl_z62displacezd2letzd2funzd2nodez12za2zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1790z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1843z00,
		BGl_z62displacezd2letzd2funzd2nod1275zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1791z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1844z00,
		BGl_z62displacezd2letzd2funzd2nod1277zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1792z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1845z00,
		BGl_z62displacezd2letzd2funzd2nod1279zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1793z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1846z00,
		BGl_z62displacezd2letzd2funzd2nod1281zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1794z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1847z00,
		BGl_z62displacezd2letzd2funzd2nod1283zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1795z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1848z00,
		BGl_z62displacezd2letzd2funzd2nod1285zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1796z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1849z00,
		BGl_z62displacezd2letzd2funzd2nod1287zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1797z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1850z00,
		BGl_z62displacezd2letzd2funzd2nod1289zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1798z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1851z00,
		BGl_z62displacezd2letzd2funzd2nod1291zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1799z00zzintegrate_letzd2funzd2,
		BgL_bgl_za762displaceza7d2le1852z00,
		BGl_z62displacezd2letzd2funzd2nod1293zb0zzintegrate_letzd2funzd2, 0L,
		BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzintegrate_letzd2funzd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_letzd2funzd2(long
		BgL_checksumz00_2422, char *BgL_fromz00_2423)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzintegrate_letzd2funzd2))
				{
					BGl_requirezd2initializa7ationz75zzintegrate_letzd2funzd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzintegrate_letzd2funzd2();
					BGl_libraryzd2moduleszd2initz00zzintegrate_letzd2funzd2();
					BGl_cnstzd2initzd2zzintegrate_letzd2funzd2();
					BGl_importedzd2moduleszd2initz00zzintegrate_letzd2funzd2();
					BGl_genericzd2initzd2zzintegrate_letzd2funzd2();
					BGl_methodzd2initzd2zzintegrate_letzd2funzd2();
					return BGl_toplevelzd2initzd2zzintegrate_letzd2funzd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_letzd2funzd2(void)
	{
		{	/* Integrate/let_fun.scm 16 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "integrate_let-fun");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"integrate_let-fun");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "integrate_let-fun");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"integrate_let-fun");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "integrate_let-fun");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"integrate_let-fun");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "integrate_let-fun");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"integrate_let-fun");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"integrate_let-fun");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"integrate_let-fun");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzintegrate_letzd2funzd2(void)
	{
		{	/* Integrate/let_fun.scm 16 */
			{	/* Integrate/let_fun.scm 16 */
				obj_t BgL_cportz00_2345;

				{	/* Integrate/let_fun.scm 16 */
					obj_t BgL_stringz00_2352;

					BgL_stringz00_2352 = BGl_string1814z00zzintegrate_letzd2funzd2;
					{	/* Integrate/let_fun.scm 16 */
						obj_t BgL_startz00_2353;

						BgL_startz00_2353 = BINT(0L);
						{	/* Integrate/let_fun.scm 16 */
							obj_t BgL_endz00_2354;

							BgL_endz00_2354 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2352)));
							{	/* Integrate/let_fun.scm 16 */

								BgL_cportz00_2345 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2352, BgL_startz00_2353, BgL_endz00_2354);
				}}}}
				{
					long BgL_iz00_2346;

					BgL_iz00_2346 = 0L;
				BgL_loopz00_2347:
					if ((BgL_iz00_2346 == -1L))
						{	/* Integrate/let_fun.scm 16 */
							return BUNSPEC;
						}
					else
						{	/* Integrate/let_fun.scm 16 */
							{	/* Integrate/let_fun.scm 16 */
								obj_t BgL_arg1820z00_2348;

								{	/* Integrate/let_fun.scm 16 */

									{	/* Integrate/let_fun.scm 16 */
										obj_t BgL_locationz00_2350;

										BgL_locationz00_2350 = BBOOL(((bool_t) 0));
										{	/* Integrate/let_fun.scm 16 */

											BgL_arg1820z00_2348 =
												BGl_readz00zz__readerz00(BgL_cportz00_2345,
												BgL_locationz00_2350);
										}
									}
								}
								{	/* Integrate/let_fun.scm 16 */
									int BgL_tmpz00_2453;

									BgL_tmpz00_2453 = (int) (BgL_iz00_2346);
									CNST_TABLE_SET(BgL_tmpz00_2453, BgL_arg1820z00_2348);
							}}
							{	/* Integrate/let_fun.scm 16 */
								int BgL_auxz00_2351;

								BgL_auxz00_2351 = (int) ((BgL_iz00_2346 - 1L));
								{
									long BgL_iz00_2458;

									BgL_iz00_2458 = (long) (BgL_auxz00_2351);
									BgL_iz00_2346 = BgL_iz00_2458;
									goto BgL_loopz00_2347;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzintegrate_letzd2funzd2(void)
	{
		{	/* Integrate/let_fun.scm 16 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzintegrate_letzd2funzd2(void)
	{
		{	/* Integrate/let_fun.scm 16 */
			BGl_za2stampza2z00zzintegrate_letzd2funzd2 = 0L;
			return BUNSPEC;
		}

	}



/* bind-fun! */
	obj_t BGl_bindzd2funz12zc0zzintegrate_letzd2funzd2(obj_t BgL_varz00_3)
	{
		{	/* Integrate/let_fun.scm 36 */
			{	/* Integrate/let_fun.scm 38 */
				bool_t BgL_test1855z00_2461;

				{	/* Integrate/let_fun.scm 38 */
					obj_t BgL_classz00_2068;

					BgL_classz00_2068 = BGl_localz00zzast_varz00;
					if (BGL_OBJECTP(BgL_varz00_3))
						{	/* Integrate/let_fun.scm 38 */
							BgL_objectz00_bglt BgL_arg1807z00_2070;

							BgL_arg1807z00_2070 = (BgL_objectz00_bglt) (BgL_varz00_3);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Integrate/let_fun.scm 38 */
									long BgL_idxz00_2076;

									BgL_idxz00_2076 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2070);
									BgL_test1855z00_2461 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2076 + 2L)) == BgL_classz00_2068);
								}
							else
								{	/* Integrate/let_fun.scm 38 */
									bool_t BgL_res1776z00_2101;

									{	/* Integrate/let_fun.scm 38 */
										obj_t BgL_oclassz00_2084;

										{	/* Integrate/let_fun.scm 38 */
											obj_t BgL_arg1815z00_2092;
											long BgL_arg1816z00_2093;

											BgL_arg1815z00_2092 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Integrate/let_fun.scm 38 */
												long BgL_arg1817z00_2094;

												BgL_arg1817z00_2094 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2070);
												BgL_arg1816z00_2093 =
													(BgL_arg1817z00_2094 - OBJECT_TYPE);
											}
											BgL_oclassz00_2084 =
												VECTOR_REF(BgL_arg1815z00_2092, BgL_arg1816z00_2093);
										}
										{	/* Integrate/let_fun.scm 38 */
											bool_t BgL__ortest_1115z00_2085;

											BgL__ortest_1115z00_2085 =
												(BgL_classz00_2068 == BgL_oclassz00_2084);
											if (BgL__ortest_1115z00_2085)
												{	/* Integrate/let_fun.scm 38 */
													BgL_res1776z00_2101 = BgL__ortest_1115z00_2085;
												}
											else
												{	/* Integrate/let_fun.scm 38 */
													long BgL_odepthz00_2086;

													{	/* Integrate/let_fun.scm 38 */
														obj_t BgL_arg1804z00_2087;

														BgL_arg1804z00_2087 = (BgL_oclassz00_2084);
														BgL_odepthz00_2086 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2087);
													}
													if ((2L < BgL_odepthz00_2086))
														{	/* Integrate/let_fun.scm 38 */
															obj_t BgL_arg1802z00_2089;

															{	/* Integrate/let_fun.scm 38 */
																obj_t BgL_arg1803z00_2090;

																BgL_arg1803z00_2090 = (BgL_oclassz00_2084);
																BgL_arg1802z00_2089 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2090,
																	2L);
															}
															BgL_res1776z00_2101 =
																(BgL_arg1802z00_2089 == BgL_classz00_2068);
														}
													else
														{	/* Integrate/let_fun.scm 38 */
															BgL_res1776z00_2101 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1855z00_2461 = BgL_res1776z00_2101;
								}
						}
					else
						{	/* Integrate/let_fun.scm 38 */
							BgL_test1855z00_2461 = ((bool_t) 0);
						}
				}
				if (BgL_test1855z00_2461)
					{	/* Integrate/let_fun.scm 39 */
						BgL_valuez00_bglt BgL_arg1320z00_1617;

						BgL_arg1320z00_1617 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_varz00_3))))->BgL_valuez00);
						{	/* Integrate/let_fun.scm 39 */
							obj_t BgL_vz00_2104;

							BgL_vz00_2104 = BINT(BGl_za2stampza2z00zzintegrate_letzd2funzd2);
							{
								BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2488;

								{
									obj_t BgL_auxz00_2489;

									{	/* Integrate/let_fun.scm 39 */
										BgL_objectz00_bglt BgL_tmpz00_2490;

										BgL_tmpz00_2490 =
											((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt) BgL_arg1320z00_1617));
										BgL_auxz00_2489 = BGL_OBJECT_WIDENING(BgL_tmpz00_2490);
									}
									BgL_auxz00_2488 =
										((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2489);
								}
								return
									((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2488))->
										BgL_istampz00) = ((obj_t) BgL_vz00_2104), BUNSPEC);
							}
						}
					}
				else
					{	/* Integrate/let_fun.scm 38 */
						return BFALSE;
					}
			}
		}

	}



/* free-fun? */
	bool_t BGl_freezd2funzf3z21zzintegrate_letzd2funzd2(obj_t BgL_localz00_4)
	{
		{	/* Integrate/let_fun.scm 44 */
			{	/* Integrate/let_fun.scm 48 */
				bool_t BgL_test1860z00_2496;

				{	/* Integrate/let_fun.scm 48 */
					obj_t BgL_arg1325z00_1621;

					{	/* Integrate/let_fun.scm 48 */
						BgL_sfunz00_bglt BgL_oz00_2107;

						BgL_oz00_2107 =
							((BgL_sfunz00_bglt)
							(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_localz00_bglt) BgL_localz00_4))))->BgL_valuez00));
						{
							BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2501;

							{
								obj_t BgL_auxz00_2502;

								{	/* Integrate/let_fun.scm 48 */
									BgL_objectz00_bglt BgL_tmpz00_2503;

									BgL_tmpz00_2503 = ((BgL_objectz00_bglt) BgL_oz00_2107);
									BgL_auxz00_2502 = BGL_OBJECT_WIDENING(BgL_tmpz00_2503);
								}
								BgL_auxz00_2501 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2502);
							}
							BgL_arg1325z00_1621 =
								(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2501))->
								BgL_istampz00);
						}
					}
					BgL_test1860z00_2496 =
						(BgL_arg1325z00_1621 ==
						BINT(BGl_za2stampza2z00zzintegrate_letzd2funzd2));
				}
				if (BgL_test1860z00_2496)
					{	/* Integrate/let_fun.scm 48 */
						return ((bool_t) 0);
					}
				else
					{	/* Integrate/let_fun.scm 48 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* displace-let-fun! */
	BGL_EXPORTED_DEF obj_t
		BGl_displacezd2letzd2funz12z12zzintegrate_letzd2funzd2(obj_t BgL_varz00_5)
	{
		{	/* Integrate/let_fun.scm 53 */
			BGl_za2stampza2z00zzintegrate_letzd2funzd2 =
				(1L + BGl_za2stampza2z00zzintegrate_letzd2funzd2);
			{	/* Integrate/let_fun.scm 60 */
				obj_t BgL_arg1327z00_1623;
				obj_t BgL_arg1328z00_1624;

				BgL_arg1327z00_1623 =
					(((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_varz00_5)))->
									BgL_valuez00))))->BgL_bodyz00);
				{	/* Integrate/let_fun.scm 60 */
					obj_t BgL_list1330z00_1626;

					BgL_list1330z00_1626 = MAKE_YOUNG_PAIR(BgL_varz00_5, BNIL);
					BgL_arg1328z00_1624 = BgL_list1330z00_1626;
				}
				BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
					((BgL_nodez00_bglt) BgL_arg1327z00_1623), BgL_arg1328z00_1624);
			}
			{	/* Integrate/let_fun.scm 69 */
				obj_t BgL_ledz00_1627;

				{	/* Integrate/let_fun.scm 69 */
					BgL_sfunz00_bglt BgL_oz00_2114;

					BgL_oz00_2114 =
						((BgL_sfunz00_bglt)
						(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_varz00_5)))->BgL_valuez00));
					{
						BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2521;

						{
							obj_t BgL_auxz00_2522;

							{	/* Integrate/let_fun.scm 69 */
								BgL_objectz00_bglt BgL_tmpz00_2523;

								BgL_tmpz00_2523 = ((BgL_objectz00_bglt) BgL_oz00_2114);
								BgL_auxz00_2522 = BGL_OBJECT_WIDENING(BgL_tmpz00_2523);
							}
							BgL_auxz00_2521 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2522);
						}
						BgL_ledz00_1627 =
							(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2521))->
							BgL_ledz00);
					}
				}
				{
					obj_t BgL_l1262z00_1629;

					BgL_l1262z00_1629 = BgL_ledz00_1627;
				BgL_zc3z04anonymousza31331ze3z87_1630:
					if (PAIRP(BgL_l1262z00_1629))
						{	/* Integrate/let_fun.scm 70 */
							{	/* Integrate/let_fun.scm 73 */
								obj_t BgL_lz00_1632;

								BgL_lz00_1632 = CAR(BgL_l1262z00_1629);
								if (BGl_freezd2funzf3z21zzintegrate_letzd2funzd2(BgL_lz00_1632))
									{	/* Integrate/let_fun.scm 80 */
										obj_t BgL_arg1335z00_1634;
										obj_t BgL_arg1339z00_1635;

										BgL_arg1335z00_1634 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt)
														(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt) BgL_lz00_1632)))->
															BgL_valuez00))))->BgL_bodyz00);
										{	/* Integrate/let_fun.scm 81 */
											obj_t BgL_list1341z00_1637;

											BgL_list1341z00_1637 =
												MAKE_YOUNG_PAIR(BgL_varz00_5, BNIL);
											BgL_arg1339z00_1635 = BgL_list1341z00_1637;
										}
										BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2
											(((BgL_nodez00_bglt) BgL_arg1335z00_1634),
											BgL_arg1339z00_1635);
									}
								else
									{	/* Integrate/let_fun.scm 74 */
										BFALSE;
									}
							}
							{
								obj_t BgL_l1262z00_2540;

								BgL_l1262z00_2540 = CDR(BgL_l1262z00_1629);
								BgL_l1262z00_1629 = BgL_l1262z00_2540;
								goto BgL_zc3z04anonymousza31331ze3z87_1630;
							}
						}
					else
						{	/* Integrate/let_fun.scm 70 */
							((bool_t) 1);
						}
				}
				{
					obj_t BgL_ledz00_1642;
					obj_t BgL_addedz00_1643;

					BgL_ledz00_1642 = BgL_ledz00_1627;
					BgL_addedz00_1643 = BNIL;
				BgL_zc3z04anonymousza31343ze3z87_1644:
					if (NULLP(BgL_ledz00_1642))
						{	/* Integrate/let_fun.scm 88 */
							obj_t BgL_oldzd2bodyzd2_1646;

							BgL_oldzd2bodyzd2_1646 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_varz00_5)))->
												BgL_valuez00))))->BgL_bodyz00);
							if (NULLP(BgL_addedz00_1643))
								{	/* Integrate/let_fun.scm 89 */
									return BgL_oldzd2bodyzd2_1646;
								}
							else
								{	/* Integrate/let_fun.scm 90 */
									BgL_letzd2funzd2_bglt BgL_newzd2bodyzd2_1648;

									{	/* Integrate/let_fun.scm 90 */
										BgL_letzd2funzd2_bglt BgL_new1111z00_1650;

										{	/* Integrate/let_fun.scm 91 */
											BgL_letzd2funzd2_bglt BgL_new1110z00_1651;

											BgL_new1110z00_1651 =
												((BgL_letzd2funzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_letzd2funzd2_bgl))));
											{	/* Integrate/let_fun.scm 91 */
												long BgL_arg1348z00_1652;

												BgL_arg1348z00_1652 =
													BGL_CLASS_NUM(BGl_letzd2funzd2zzast_nodez00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1110z00_1651),
													BgL_arg1348z00_1652);
											}
											{	/* Integrate/let_fun.scm 91 */
												BgL_objectz00_bglt BgL_tmpz00_2554;

												BgL_tmpz00_2554 =
													((BgL_objectz00_bglt) BgL_new1110z00_1651);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_2554, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1110z00_1651);
											BgL_new1111z00_1650 = BgL_new1110z00_1651;
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1111z00_1650)))->
												BgL_locz00) =
											((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																BgL_oldzd2bodyzd2_1646)))->BgL_locz00)),
											BUNSPEC);
										((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
															BgL_new1111z00_1650)))->BgL_typez00) =
											((BgL_typez00_bglt)
												BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt)
														BgL_oldzd2bodyzd2_1646), ((bool_t) 0))), BUNSPEC);
										((((BgL_nodezf2effectzf2_bglt)
													COBJECT(((BgL_nodezf2effectzf2_bglt)
															BgL_new1111z00_1650)))->BgL_sidezd2effectzd2) =
											((obj_t) BUNSPEC), BUNSPEC);
										((((BgL_nodezf2effectzf2_bglt)
													COBJECT(((BgL_nodezf2effectzf2_bglt)
															BgL_new1111z00_1650)))->BgL_keyz00) =
											((obj_t) BINT(-1L)), BUNSPEC);
										((((BgL_letzd2funzd2_bglt) COBJECT(BgL_new1111z00_1650))->
												BgL_localsz00) = ((obj_t) BgL_addedz00_1643), BUNSPEC);
										((((BgL_letzd2funzd2_bglt) COBJECT(BgL_new1111z00_1650))->
												BgL_bodyz00) =
											((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
													BgL_oldzd2bodyzd2_1646)), BUNSPEC);
										BgL_newzd2bodyzd2_1648 = BgL_new1111z00_1650;
									}
									{	/* Integrate/let_fun.scm 101 */
										BgL_valuez00_bglt BgL_arg1346z00_1649;

										BgL_arg1346z00_1649 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_varz00_5)))->
											BgL_valuez00);
										((((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt)
															BgL_arg1346z00_1649)))->BgL_bodyz00) =
											((obj_t) ((obj_t) BgL_newzd2bodyzd2_1648)), BUNSPEC);
									}
									return ((obj_t) BgL_newzd2bodyzd2_1648);
								}
						}
					else
						{	/* Integrate/let_fun.scm 108 */
							bool_t BgL_test1865z00_2580;

							{	/* Integrate/let_fun.scm 108 */
								obj_t BgL_arg1371z00_1660;

								BgL_arg1371z00_1660 = CAR(((obj_t) BgL_ledz00_1642));
								BgL_test1865z00_2580 =
									BGl_freezd2funzf3z21zzintegrate_letzd2funzd2
									(BgL_arg1371z00_1660);
							}
							if (BgL_test1865z00_2580)
								{	/* Integrate/let_fun.scm 110 */
									obj_t BgL_arg1361z00_1656;
									obj_t BgL_arg1364z00_1657;

									BgL_arg1361z00_1656 = CDR(((obj_t) BgL_ledz00_1642));
									{	/* Integrate/let_fun.scm 110 */
										obj_t BgL_arg1367z00_1658;

										BgL_arg1367z00_1658 = CAR(((obj_t) BgL_ledz00_1642));
										BgL_arg1364z00_1657 =
											MAKE_YOUNG_PAIR(BgL_arg1367z00_1658, BgL_addedz00_1643);
									}
									{
										obj_t BgL_addedz00_2590;
										obj_t BgL_ledz00_2589;

										BgL_ledz00_2589 = BgL_arg1361z00_1656;
										BgL_addedz00_2590 = BgL_arg1364z00_1657;
										BgL_addedz00_1643 = BgL_addedz00_2590;
										BgL_ledz00_1642 = BgL_ledz00_2589;
										goto BgL_zc3z04anonymousza31343ze3z87_1644;
									}
								}
							else
								{	/* Integrate/let_fun.scm 113 */
									obj_t BgL_arg1370z00_1659;

									BgL_arg1370z00_1659 = CDR(((obj_t) BgL_ledz00_1642));
									{
										obj_t BgL_ledz00_2593;

										BgL_ledz00_2593 = BgL_arg1370z00_1659;
										BgL_ledz00_1642 = BgL_ledz00_2593;
										goto BgL_zc3z04anonymousza31343ze3z87_1644;
									}
								}
						}
				}
			}
		}

	}



/* &displace-let-fun! */
	obj_t BGl_z62displacezd2letzd2funz12z70zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2248, obj_t BgL_varz00_2249)
	{
		{	/* Integrate/let_fun.scm 53 */
			return
				BGl_displacezd2letzd2funz12z12zzintegrate_letzd2funzd2(BgL_varz00_2249);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzintegrate_letzd2funzd2(void)
	{
		{	/* Integrate/let_fun.scm 16 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzintegrate_letzd2funzd2(void)
	{
		{	/* Integrate/let_fun.scm 16 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_proc1785z00zzintegrate_letzd2funzd2, BGl_nodez00zzast_nodez00,
				BGl_string1786z00zzintegrate_letzd2funzd2);
		}

	}



/* &displace-let-fun-nod1270 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1270zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2251, obj_t BgL_nodez00_2252, obj_t BgL_hostsz00_2253)
	{
		{	/* Integrate/let_fun.scm 123 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
				BGl_string1787z00zzintegrate_letzd2funzd2,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2252)));
		}

	}



/* displace-let-fun-node! */
	obj_t
		BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2
		(BgL_nodez00_bglt BgL_nodez00_6, obj_t BgL_hostsz00_7)
	{
		{	/* Integrate/let_fun.scm 123 */
			{	/* Integrate/let_fun.scm 123 */
				obj_t BgL_method1271z00_1668;

				{	/* Integrate/let_fun.scm 123 */
					obj_t BgL_res1783z00_2164;

					{	/* Integrate/let_fun.scm 123 */
						long BgL_objzd2classzd2numz00_2135;

						BgL_objzd2classzd2numz00_2135 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_6));
						{	/* Integrate/let_fun.scm 123 */
							obj_t BgL_arg1811z00_2136;

							BgL_arg1811z00_2136 =
								PROCEDURE_REF
								(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
								(int) (1L));
							{	/* Integrate/let_fun.scm 123 */
								int BgL_offsetz00_2139;

								BgL_offsetz00_2139 = (int) (BgL_objzd2classzd2numz00_2135);
								{	/* Integrate/let_fun.scm 123 */
									long BgL_offsetz00_2140;

									BgL_offsetz00_2140 =
										((long) (BgL_offsetz00_2139) - OBJECT_TYPE);
									{	/* Integrate/let_fun.scm 123 */
										long BgL_modz00_2141;

										BgL_modz00_2141 =
											(BgL_offsetz00_2140 >> (int) ((long) ((int) (4L))));
										{	/* Integrate/let_fun.scm 123 */
											long BgL_restz00_2143;

											BgL_restz00_2143 =
												(BgL_offsetz00_2140 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Integrate/let_fun.scm 123 */

												{	/* Integrate/let_fun.scm 123 */
													obj_t BgL_bucketz00_2145;

													BgL_bucketz00_2145 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2136), BgL_modz00_2141);
													BgL_res1783z00_2164 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2145), BgL_restz00_2143);
					}}}}}}}}
					BgL_method1271z00_1668 = BgL_res1783z00_2164;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1271z00_1668,
					((obj_t) BgL_nodez00_6), BgL_hostsz00_7);
			}
		}

	}



/* &displace-let-fun-node! */
	obj_t BGl_z62displacezd2letzd2funzd2nodez12za2zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2254, obj_t BgL_nodez00_2255, obj_t BgL_hostsz00_2256)
	{
		{	/* Integrate/let_fun.scm 123 */
			return
				BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
				((BgL_nodez00_bglt) BgL_nodez00_2255), BgL_hostsz00_2256);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzintegrate_letzd2funzd2(void)
	{
		{	/* Integrate/let_fun.scm 16 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_atomz00zzast_nodez00, BGl_proc1788z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_kwotez00zzast_nodez00, BGl_proc1790z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_varz00zzast_nodez00, BGl_proc1791z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_closurez00zzast_nodez00, BGl_proc1792z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_sequencez00zzast_nodez00, BGl_proc1793z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_syncz00zzast_nodez00, BGl_proc1794z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_appz00zzast_nodez00, BGl_proc1795z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1796z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_funcallz00zzast_nodez00, BGl_proc1797z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_externz00zzast_nodez00, BGl_proc1798z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_castz00zzast_nodez00, BGl_proc1799z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_setqz00zzast_nodez00, BGl_proc1800z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_conditionalz00zzast_nodez00,
				BGl_proc1801z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_failz00zzast_nodez00, BGl_proc1802z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_switchz00zzast_nodez00, BGl_proc1803z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1804z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1805z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc1806z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc1807z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1808z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc1809z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_displacezd2letzd2funzd2nodez12zd2envz12zzintegrate_letzd2funzd2,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1810z00zzintegrate_letzd2funzd2,
				BGl_string1789z00zzintegrate_letzd2funzd2);
		}

	}



/* &displace-let-fun-nod1315 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1315zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2279, obj_t BgL_nodez00_2280, obj_t BgL_hostsz00_2281)
	{
		{	/* Integrate/let_fun.scm 323 */
			return BUNSPEC;
		}

	}



/* &displace-let-fun-nod1313 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1313zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2282, obj_t BgL_nodez00_2283, obj_t BgL_hostsz00_2284)
	{
		{	/* Integrate/let_fun.scm 317 */
			return BUNSPEC;
		}

	}



/* &displace-let-fun-nod1311 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1311zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2285, obj_t BgL_nodez00_2286, obj_t BgL_hostsz00_2287)
	{
		{	/* Integrate/let_fun.scm 310 */
			return
				BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2286)))->BgL_valuez00),
				BgL_hostsz00_2287);
		}

	}



/* &displace-let-fun-nod1309 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1309zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2288, obj_t BgL_nodez00_2289, obj_t BgL_hostsz00_2290)
	{
		{	/* Integrate/let_fun.scm 302 */
			BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2289)))->BgL_exitz00),
				BgL_hostsz00_2290);
			return
				BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(((
						(BgL_jumpzd2exzd2itz00_bglt) COBJECT(((BgL_jumpzd2exzd2itz00_bglt)
								BgL_nodez00_2289)))->BgL_valuez00), BgL_hostsz00_2290);
		}

	}



/* &displace-let-fun-nod1307 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1307zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2291, obj_t BgL_nodez00_2292, obj_t BgL_hostsz00_2293)
	{
		{	/* Integrate/let_fun.scm 294 */
			BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2292)))->BgL_onexitz00),
				BgL_hostsz00_2293);
			return
				BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(((
						(BgL_setzd2exzd2itz00_bglt) COBJECT(((BgL_setzd2exzd2itz00_bglt)
								BgL_nodez00_2292)))->BgL_bodyz00), BgL_hostsz00_2293);
		}

	}



/* &displace-let-fun-nod1305 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1305zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2294, obj_t BgL_nodez00_2295, obj_t BgL_hostsz00_2296)
	{
		{	/* Integrate/let_fun.scm 280 */
			{	/* Integrate/let_fun.scm 282 */
				obj_t BgL_g1131z00_2364;

				BgL_g1131z00_2364 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2295)))->BgL_bindingsz00);
				{
					obj_t BgL_bindingsz00_2366;

					BgL_bindingsz00_2366 = BgL_g1131z00_2364;
				BgL_liipz00_2365:
					if (NULLP(BgL_bindingsz00_2366))
						{	/* Integrate/let_fun.scm 283 */
							return
								BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
								(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_nodez00_2295)))->
									BgL_bodyz00), BgL_hostsz00_2296);
						}
					else
						{	/* Integrate/let_fun.scm 285 */
							obj_t BgL_bindingz00_2367;

							BgL_bindingz00_2367 = CAR(((obj_t) BgL_bindingsz00_2366));
							{	/* Integrate/let_fun.scm 286 */
								obj_t BgL_valz00_2368;

								BgL_valz00_2368 = CDR(((obj_t) BgL_bindingz00_2367));
								{	/* Integrate/let_fun.scm 287 */

									BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
										((BgL_nodez00_bglt) BgL_valz00_2368), BgL_hostsz00_2296);
									{	/* Integrate/let_fun.scm 289 */
										obj_t BgL_arg1611z00_2369;

										BgL_arg1611z00_2369 = CDR(((obj_t) BgL_bindingsz00_2366));
										{
											obj_t BgL_bindingsz00_2685;

											BgL_bindingsz00_2685 = BgL_arg1611z00_2369;
											BgL_bindingsz00_2366 = BgL_bindingsz00_2685;
											goto BgL_liipz00_2365;
										}
									}
								}
							}
						}
				}
			}
		}

	}



/* &displace-let-fun-nod1303 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1303zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2297, obj_t BgL_nodez00_2298, obj_t BgL_hostsz00_2299)
	{
		{	/* Integrate/let_fun.scm 250 */
			{	/* Tools/trace.sch 53 */
				obj_t BgL_g1125z00_2371;

				BgL_g1125z00_2371 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2298)))->BgL_localsz00);
				{
					obj_t BgL_oldz00_2373;
					obj_t BgL_newz00_2374;

					BgL_oldz00_2373 = BgL_g1125z00_2371;
					BgL_newz00_2374 = BNIL;
				BgL_liipz00_2372:
					if (NULLP(BgL_oldz00_2373))
						{	/* Tools/trace.sch 53 */
							((((BgL_letzd2funzd2_bglt) COBJECT(
											((BgL_letzd2funzd2_bglt) BgL_nodez00_2298)))->
									BgL_localsz00) = ((obj_t) BgL_newz00_2374), BUNSPEC);
							return
								BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(((
										(BgL_letzd2funzd2_bglt) COBJECT(((BgL_letzd2funzd2_bglt)
												BgL_nodez00_2298)))->BgL_bodyz00), BgL_hostsz00_2299);
						}
					else
						{	/* Tools/trace.sch 53 */
							bool_t BgL_test1868z00_2695;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_arg1595z00_2375;

								{	/* Tools/trace.sch 53 */
									BgL_sfunz00_bglt BgL_oz00_2376;

									BgL_oz00_2376 =
										((BgL_sfunz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt)
															CAR(
																((obj_t) BgL_oldz00_2373))))))->BgL_valuez00));
									{
										BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2702;

										{
											obj_t BgL_auxz00_2703;

											{	/* Tools/trace.sch 53 */
												BgL_objectz00_bglt BgL_tmpz00_2704;

												BgL_tmpz00_2704 = ((BgL_objectz00_bglt) BgL_oz00_2376);
												BgL_auxz00_2703 = BGL_OBJECT_WIDENING(BgL_tmpz00_2704);
											}
											BgL_auxz00_2702 =
												((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2703);
										}
										BgL_arg1595z00_2375 =
											(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2702))->
											BgL_lz00);
									}
								}
								BgL_test1868z00_2695 =
									CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
									(BgL_arg1595z00_2375, BgL_hostsz00_2299));
							}
							if (BgL_test1868z00_2695)
								{	/* Tools/trace.sch 53 */
									obj_t BgL_lz00_2377;

									BgL_lz00_2377 = CAR(((obj_t) BgL_oldz00_2373));
									BGl_bindzd2funz12zc0zzintegrate_letzd2funzd2(BgL_lz00_2377);
									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg1584z00_2378;
										obj_t BgL_arg1585z00_2379;

										BgL_arg1584z00_2378 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt)
														(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_lz00_2377))))->
															BgL_valuez00))))->BgL_bodyz00);
										BgL_arg1585z00_2379 =
											MAKE_YOUNG_PAIR(BgL_lz00_2377, BgL_hostsz00_2299);
										BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2
											(((BgL_nodez00_bglt) BgL_arg1584z00_2378),
											BgL_arg1585z00_2379);
									}
									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg1591z00_2380;
										obj_t BgL_arg1593z00_2381;

										BgL_arg1591z00_2380 = CDR(((obj_t) BgL_oldz00_2373));
										BgL_arg1593z00_2381 =
											MAKE_YOUNG_PAIR(BgL_lz00_2377, BgL_newz00_2374);
										{
											obj_t BgL_newz00_2726;
											obj_t BgL_oldz00_2725;

											BgL_oldz00_2725 = BgL_arg1591z00_2380;
											BgL_newz00_2726 = BgL_arg1593z00_2381;
											BgL_newz00_2374 = BgL_newz00_2726;
											BgL_oldz00_2373 = BgL_oldz00_2725;
											goto BgL_liipz00_2372;
										}
									}
								}
							else
								{	/* Tools/trace.sch 53 */
									obj_t BgL_arg1594z00_2382;

									BgL_arg1594z00_2382 = CDR(((obj_t) BgL_oldz00_2373));
									{
										obj_t BgL_oldz00_2729;

										BgL_oldz00_2729 = BgL_arg1594z00_2382;
										BgL_oldz00_2373 = BgL_oldz00_2729;
										goto BgL_liipz00_2372;
									}
								}
						}
				}
			}
		}

	}



/* &displace-let-fun-nod1301 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1301zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2300, obj_t BgL_nodez00_2301, obj_t BgL_hostsz00_2302)
	{
		{	/* Integrate/let_fun.scm 238 */
			{	/* Integrate/let_fun.scm 240 */
				obj_t BgL_g1123z00_2384;

				BgL_g1123z00_2384 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2301)))->BgL_clausesz00);
				{
					obj_t BgL_clausesz00_2386;

					BgL_clausesz00_2386 = BgL_g1123z00_2384;
				BgL_liipz00_2385:
					if (NULLP(BgL_clausesz00_2386))
						{	/* Integrate/let_fun.scm 241 */
							return
								BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
								(((BgL_switchz00_bglt) COBJECT(
											((BgL_switchz00_bglt) BgL_nodez00_2301)))->BgL_testz00),
								BgL_hostsz00_2302);
						}
					else
						{	/* Integrate/let_fun.scm 241 */
							{	/* Integrate/let_fun.scm 244 */
								obj_t BgL_arg1553z00_2387;

								{	/* Integrate/let_fun.scm 244 */
									obj_t BgL_pairz00_2388;

									BgL_pairz00_2388 = CAR(((obj_t) BgL_clausesz00_2386));
									BgL_arg1553z00_2387 = CDR(BgL_pairz00_2388);
								}
								BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
									((BgL_nodez00_bglt) BgL_arg1553z00_2387), BgL_hostsz00_2302);
							}
							{	/* Integrate/let_fun.scm 245 */
								obj_t BgL_arg1561z00_2389;

								BgL_arg1561z00_2389 = CDR(((obj_t) BgL_clausesz00_2386));
								{
									obj_t BgL_clausesz00_2744;

									BgL_clausesz00_2744 = BgL_arg1561z00_2389;
									BgL_clausesz00_2386 = BgL_clausesz00_2744;
									goto BgL_liipz00_2385;
								}
							}
						}
				}
			}
		}

	}



/* &displace-let-fun-nod1299 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1299zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2303, obj_t BgL_nodez00_2304, obj_t BgL_hostsz00_2305)
	{
		{	/* Integrate/let_fun.scm 229 */
			BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2304)))->BgL_procz00),
				BgL_hostsz00_2305);
			BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(((
						(BgL_failz00_bglt) COBJECT(((BgL_failz00_bglt) BgL_nodez00_2304)))->
					BgL_msgz00), BgL_hostsz00_2305);
			return
				BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(((
						(BgL_failz00_bglt) COBJECT(((BgL_failz00_bglt) BgL_nodez00_2304)))->
					BgL_objz00), BgL_hostsz00_2305);
		}

	}



/* &displace-let-fun-nod1297 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1297zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2306, obj_t BgL_nodez00_2307, obj_t BgL_hostsz00_2308)
	{
		{	/* Integrate/let_fun.scm 220 */
			BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2307)))->BgL_testz00),
				BgL_hostsz00_2308);
			BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(((
						(BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt)
								BgL_nodez00_2307)))->BgL_truez00), BgL_hostsz00_2308);
			return
				BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(((
						(BgL_conditionalz00_bglt) COBJECT(((BgL_conditionalz00_bglt)
								BgL_nodez00_2307)))->BgL_falsez00), BgL_hostsz00_2308);
		}

	}



/* &displace-let-fun-nod1295 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1295zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2309, obj_t BgL_nodez00_2310, obj_t BgL_hostsz00_2311)
	{
		{	/* Integrate/let_fun.scm 213 */
			return
				BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2310)))->BgL_valuez00),
				BgL_hostsz00_2311);
		}

	}



/* &displace-let-fun-nod1293 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1293zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2312, obj_t BgL_nodez00_2313, obj_t BgL_hostsz00_2314)
	{
		{	/* Integrate/let_fun.scm 206 */
			return
				BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2313)))->BgL_argz00),
				BgL_hostsz00_2314);
		}

	}



/* &displace-let-fun-nod1291 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1291zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2315, obj_t BgL_nodez00_2316, obj_t BgL_hostsz00_2317)
	{
		{	/* Integrate/let_fun.scm 199 */
			{	/* Integrate/let_fun.scm 200 */
				bool_t BgL_tmpz00_2769;

				{	/* Integrate/let_fun.scm 201 */
					obj_t BgL_g1269z00_2395;

					BgL_g1269z00_2395 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_2316)))->BgL_exprza2za2);
					{
						obj_t BgL_l1267z00_2397;

						BgL_l1267z00_2397 = BgL_g1269z00_2395;
					BgL_zc3z04anonymousza31486ze3z87_2396:
						if (PAIRP(BgL_l1267z00_2397))
							{	/* Integrate/let_fun.scm 201 */
								{	/* Integrate/let_fun.scm 201 */
									obj_t BgL_nodez00_2398;

									BgL_nodez00_2398 = CAR(BgL_l1267z00_2397);
									BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
										((BgL_nodez00_bglt) BgL_nodez00_2398), BgL_hostsz00_2317);
								}
								{
									obj_t BgL_l1267z00_2777;

									BgL_l1267z00_2777 = CDR(BgL_l1267z00_2397);
									BgL_l1267z00_2397 = BgL_l1267z00_2777;
									goto BgL_zc3z04anonymousza31486ze3z87_2396;
								}
							}
						else
							{	/* Integrate/let_fun.scm 201 */
								BgL_tmpz00_2769 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_2769);
			}
		}

	}



/* &displace-let-fun-nod1289 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1289zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2318, obj_t BgL_nodez00_2319, obj_t BgL_hostsz00_2320)
	{
		{	/* Integrate/let_fun.scm 187 */
			{
				obj_t BgL_astsz00_2401;

				BgL_astsz00_2401 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2319)))->BgL_argsz00);
			BgL_liipz00_2400:
				if (NULLP(BgL_astsz00_2401))
					{	/* Integrate/let_fun.scm 190 */
						return
							BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
							(((BgL_funcallz00_bglt) COBJECT(
										((BgL_funcallz00_bglt) BgL_nodez00_2319)))->BgL_funz00),
							BgL_hostsz00_2320);
					}
				else
					{	/* Integrate/let_fun.scm 190 */
						{	/* Integrate/let_fun.scm 193 */
							obj_t BgL_arg1473z00_2402;

							BgL_arg1473z00_2402 = CAR(((obj_t) BgL_astsz00_2401));
							BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
								((BgL_nodez00_bglt) BgL_arg1473z00_2402), BgL_hostsz00_2320);
						}
						{	/* Integrate/let_fun.scm 194 */
							obj_t BgL_arg1485z00_2403;

							BgL_arg1485z00_2403 = CDR(((obj_t) BgL_astsz00_2401));
							{
								obj_t BgL_astsz00_2791;

								BgL_astsz00_2791 = BgL_arg1485z00_2403;
								BgL_astsz00_2401 = BgL_astsz00_2791;
								goto BgL_liipz00_2400;
							}
						}
					}
			}
		}

	}



/* &displace-let-fun-nod1287 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1287zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2321, obj_t BgL_nodez00_2322, obj_t BgL_hostsz00_2323)
	{
		{	/* Integrate/let_fun.scm 179 */
			BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2322)))->BgL_funz00),
				BgL_hostsz00_2323);
			return
				BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(((
						(BgL_appzd2lyzd2_bglt) COBJECT(((BgL_appzd2lyzd2_bglt)
								BgL_nodez00_2322)))->BgL_argz00), BgL_hostsz00_2323);
		}

	}



/* &displace-let-fun-nod1285 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1285zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2324, obj_t BgL_nodez00_2325, obj_t BgL_hostsz00_2326)
	{
		{	/* Integrate/let_fun.scm 168 */
			{	/* Integrate/let_fun.scm 169 */
				obj_t BgL_g1114z00_2406;

				BgL_g1114z00_2406 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2325)))->BgL_argsz00);
				{
					obj_t BgL_argsz00_2408;

					BgL_argsz00_2408 = BgL_g1114z00_2406;
				BgL_liipz00_2407:
					if (NULLP(BgL_argsz00_2408))
						{	/* Integrate/let_fun.scm 170 */
							return BUNSPEC;
						}
					else
						{	/* Integrate/let_fun.scm 170 */
							{	/* Integrate/let_fun.scm 173 */
								obj_t BgL_arg1434z00_2409;

								BgL_arg1434z00_2409 = CAR(((obj_t) BgL_argsz00_2408));
								BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
									((BgL_nodez00_bglt) BgL_arg1434z00_2409), BgL_hostsz00_2326);
							}
							{	/* Integrate/let_fun.scm 174 */
								obj_t BgL_arg1437z00_2410;

								BgL_arg1437z00_2410 = CDR(((obj_t) BgL_argsz00_2408));
								{
									obj_t BgL_argsz00_2810;

									BgL_argsz00_2810 = BgL_arg1437z00_2410;
									BgL_argsz00_2408 = BgL_argsz00_2810;
									goto BgL_liipz00_2407;
								}
							}
						}
				}
			}
		}

	}



/* &displace-let-fun-nod1283 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1283zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2327, obj_t BgL_nodez00_2328, obj_t BgL_hostsz00_2329)
	{
		{	/* Integrate/let_fun.scm 159 */
			BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2328)))->BgL_mutexz00),
				BgL_hostsz00_2329);
			BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(((
						(BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2328)))->
					BgL_prelockz00), BgL_hostsz00_2329);
			return
				BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(((
						(BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt) BgL_nodez00_2328)))->
					BgL_bodyz00), BgL_hostsz00_2329);
		}

	}



/* &displace-let-fun-nod1281 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1281zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2330, obj_t BgL_nodez00_2331, obj_t BgL_hostsz00_2332)
	{
		{	/* Integrate/let_fun.scm 152 */
			{	/* Integrate/let_fun.scm 153 */
				bool_t BgL_tmpz00_2820;

				{	/* Integrate/let_fun.scm 154 */
					obj_t BgL_g1266z00_2413;

					BgL_g1266z00_2413 =
						(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_2331)))->BgL_nodesz00);
					{
						obj_t BgL_l1264z00_2415;

						BgL_l1264z00_2415 = BgL_g1266z00_2413;
					BgL_zc3z04anonymousza31378ze3z87_2414:
						if (PAIRP(BgL_l1264z00_2415))
							{	/* Integrate/let_fun.scm 154 */
								{	/* Integrate/let_fun.scm 154 */
									obj_t BgL_nodez00_2416;

									BgL_nodez00_2416 = CAR(BgL_l1264z00_2415);
									BGl_displacezd2letzd2funzd2nodez12zc0zzintegrate_letzd2funzd2(
										((BgL_nodez00_bglt) BgL_nodez00_2416), BgL_hostsz00_2332);
								}
								{
									obj_t BgL_l1264z00_2828;

									BgL_l1264z00_2828 = CDR(BgL_l1264z00_2415);
									BgL_l1264z00_2415 = BgL_l1264z00_2828;
									goto BgL_zc3z04anonymousza31378ze3z87_2414;
								}
							}
						else
							{	/* Integrate/let_fun.scm 154 */
								BgL_tmpz00_2820 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_2820);
			}
		}

	}



/* &displace-let-fun-nod1279 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1279zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2333, obj_t BgL_nodez00_2334, obj_t BgL_hostsz00_2335)
	{
		{	/* Integrate/let_fun.scm 146 */
			{	/* Integrate/let_fun.scm 147 */
				obj_t BgL_arg1377z00_2418;

				BgL_arg1377z00_2418 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_2334)));
				return
					BGl_internalzd2errorzd2zztools_errorz00
					(BGl_string1811z00zzintegrate_letzd2funzd2,
					BGl_string1812z00zzintegrate_letzd2funzd2, BgL_arg1377z00_2418);
			}
		}

	}



/* &displace-let-fun-nod1277 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1277zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2336, obj_t BgL_nodez00_2337, obj_t BgL_hostsz00_2338)
	{
		{	/* Integrate/let_fun.scm 140 */
			return BUNSPEC;
		}

	}



/* &displace-let-fun-nod1275 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1275zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2339, obj_t BgL_nodez00_2340, obj_t BgL_hostsz00_2341)
	{
		{	/* Integrate/let_fun.scm 134 */
			return BUNSPEC;
		}

	}



/* &displace-let-fun-nod1273 */
	obj_t BGl_z62displacezd2letzd2funzd2nod1273zb0zzintegrate_letzd2funzd2(obj_t
		BgL_envz00_2342, obj_t BgL_nodez00_2343, obj_t BgL_hostsz00_2344)
	{
		{	/* Integrate/let_fun.scm 128 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzintegrate_letzd2funzd2(void)
	{
		{	/* Integrate/let_fun.scm 16 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1813z00zzintegrate_letzd2funzd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1813z00zzintegrate_letzd2funzd2));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1813z00zzintegrate_letzd2funzd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1813z00zzintegrate_letzd2funzd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1813z00zzintegrate_letzd2funzd2));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1813z00zzintegrate_letzd2funzd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1813z00zzintegrate_letzd2funzd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1813z00zzintegrate_letzd2funzd2));
			return
				BGl_modulezd2initializa7ationz75zzintegrate_infoz00(0L,
				BSTRING_TO_STRING(BGl_string1813z00zzintegrate_letzd2funzd2));
		}

	}

#ifdef __cplusplus
}
#endif
