/*===========================================================================*/
/*   (Integrate/a.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Integrate/a.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INTEGRATE_A_TYPE_DEFINITIONS
#define BGL_INTEGRATE_A_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_sexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_handlerz00;
		bool_t BgL_detachedzf3zf3;
	}               *BgL_sexitz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_svarzf2iinfozf2_bgl
	{
		obj_t BgL_fzd2markzd2;
		obj_t BgL_uzd2markzd2;
		bool_t BgL_kapturedzf3zf3;
		bool_t BgL_celledzf3zf3;
		obj_t BgL_xhdlz00;
	}                      *BgL_svarzf2iinfozf2_bglt;

	typedef struct BgL_sexitzf2iinfozf2_bgl
	{
		obj_t BgL_fzd2markzd2;
		obj_t BgL_uzd2markzd2;
		bool_t BgL_kapturedzf3zf3;
		bool_t BgL_celledzf3zf3;
	}                       *BgL_sexitzf2iinfozf2_bglt;

	typedef struct BgL_sfunzf2iinfozf2_bgl
	{
		obj_t BgL_ownerz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
		obj_t BgL_cfromz00;
		obj_t BgL_ctoz00;
		obj_t BgL_kz00;
		obj_t BgL_kza2za2;
		obj_t BgL_uz00;
		obj_t BgL_cnz00;
		obj_t BgL_ctz00;
		obj_t BgL_kontz00;
		bool_t BgL_gzf3zf3;
		bool_t BgL_forcegzf3zf3;
		obj_t BgL_lz00;
		obj_t BgL_ledz00;
		obj_t BgL_istampz00;
		obj_t BgL_globalz00;
		obj_t BgL_kapturedz00;
		obj_t BgL_tailzd2coercionzd2;
		bool_t BgL_xhdlzf3zf3;
		obj_t BgL_xhdlsz00;
	}                      *BgL_sfunzf2iinfozf2_bglt;


#endif													// BGL_INTEGRATE_A_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62nodezd2Azd2kwote1334z62zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_za2intza2z00zztype_cachez00;
	static obj_t BGl_requirezd2initializa7ationz75zzintegrate_az00 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	static obj_t BGl_z62nodezd2Azd2sequence1340z62zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2pairzd2nilza2zd2zztype_cachez00;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_z62nodezd2Azd2atom1332z62zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzintegrate_az00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_z62nodezd2Azd2funcall1348z62zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern bool_t
		BGl_iszd2unwindzd2untilzf3zf3zzreturn_walkz00(BgL_variablez00_bglt);
	static obj_t BGl_z62nodezd2Azd2setzd2exzd2it1366z62zzintegrate_az00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_z62nodezd2Azb0zzintegrate_az00(obj_t, obj_t, obj_t, obj_t,
		obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzintegrate_az00(void);
	static bool_t BGl_tailzd2typezd2compatiblezf3zf3zzintegrate_az00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2Azd2letzd2var1364zb0zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzintegrate_az00(void);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	extern obj_t BGl_sfunzf2Iinfozf2zzintegrate_infoz00;
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62nodezd2Azd2closure1338z62zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2Azd2var1336z62zzintegrate_az00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	extern obj_t BGl_za2magicza2z00zztype_cachez00;
	static obj_t BGl_z62nodezd2Azd2boxzd2ref1377zb0zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzintegrate_az00(void);
	static obj_t BGl_z62nodezd2Azd2appzd2ly1346zb0zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62nodezd2Azd2app1344z62zzintegrate_az00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2mutexza2z00zztype_cachez00;
	extern obj_t BGl_varz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	extern obj_t BGl_za2pairza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t BGl_Az00zzintegrate_az00(BgL_globalz00_bglt,
		BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_sexitzf2Iinfozf2zzintegrate_infoz00;
	static obj_t BGl_z62nodezd2Azd2extern1350z62zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t BGl_z62nodezd2Azd2switch1360z62zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_za2kontza2z00zzintegrate_az00 = BUNSPEC;
	static obj_t BGl_z62nodezd2Azd2cast1352z62zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static bool_t
		BGl_initializa7ezd2funz12z67zzintegrate_az00(BgL_variablez00_bglt,
		BgL_variablez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzintegrate_az00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzreturn_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_infoz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_dumpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_svarzf2Iinfozf2zzintegrate_infoz00;
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_z62nodezd2A1328zb0zzintegrate_az00(obj_t, obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2Azd2setq1354z62zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62nodezd2Azd2sync1342z62zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62nodezd2Azd2fail1358z62zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_tracezd2Azd2zzintegrate_az00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t BGl_tailzd2coercionzd2zzintegrate_az00(obj_t,
		BgL_globalz00_bglt);
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzintegrate_az00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_az00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzintegrate_az00(void);
	BGL_IMPORT obj_t
		BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzintegrate_az00(void);
	static obj_t BGl_z62zc3z04anonymousza31473ze3ze5zzintegrate_az00(obj_t);
	static obj_t BGl_getzd2newzd2kontz00zzintegrate_az00(void);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_userzd2warningzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2Azd2letzd2fun1362zb0zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62nodezd2Azd2boxzd2setz121374za2zzintegrate_az00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62nodezd2Azd2makezd2box1371zb0zzintegrate_az00(obj_t, obj_t,
		obj_t, obj_t, obj_t);
	static obj_t BGl_z62Az62zzintegrate_az00(obj_t, obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static obj_t BGl_nodezd2Azd2zzintegrate_az00(BgL_nodez00_bglt,
		BgL_variablez00_bglt, obj_t, obj_t);
	static obj_t BGl_z62nodezd2Azd2conditional1356z62zzintegrate_az00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_normaliza7ezd2typez75zzintegrate_az00(BgL_typez00_bglt);
	extern obj_t BGl_initzd2returnzd2cachez12z12zzreturn_walkz00(void);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	BGL_EXPORTED_DEF obj_t BGl_za2phiza2z00zzintegrate_az00 = BUNSPEC;
	extern obj_t BGl_za2localzd2exitzf3za2z21zzengine_paramz00;
	static obj_t BGl_z62nodezd2Azd2jumpzd2exzd2it1368z62zzintegrate_az00(obj_t,
		obj_t, obj_t, obj_t, obj_t);
	static obj_t __cnst[4];


	   
		 
		DEFINE_STRING(BGl_string2128z00zzintegrate_az00,
		BgL_bgl_string2128za700za7za7i2165za7, "Before tail-coercion", 20);
	      DEFINE_STRING(BGl_string2129z00zzintegrate_az00,
		BgL_bgl_string2129za700za7za7i2166za7, "After tail-coercion", 19);
	      DEFINE_STRING(BGl_string2130z00zzintegrate_az00,
		BgL_bgl_string2130za700za7za7i2167za7, "A( ", 3);
	      DEFINE_STRING(BGl_string2131z00zzintegrate_az00,
		BgL_bgl_string2131za700za7za7i2168za7, ", ", 2);
	      DEFINE_STRING(BGl_string2132z00zzintegrate_az00,
		BgL_bgl_string2132za700za7za7i2169za7, " )", 2);
	      DEFINE_STRING(BGl_string2133z00zzintegrate_az00,
		BgL_bgl_string2133za700za7za7i2170za7, "~a/~a", 5);
	      DEFINE_STRING(BGl_string2134z00zzintegrate_az00,
		BgL_bgl_string2134za700za7za7i2171za7,
		"Globalized because used in two different type contexts", 54);
	      DEFINE_STRING(BGl_string2136z00zzintegrate_az00,
		BgL_bgl_string2136za700za7za7i2172za7, "node-A1328", 10);
	      DEFINE_STRING(BGl_string2137z00zzintegrate_az00,
		BgL_bgl_string2137za700za7za7i2173za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2139z00zzintegrate_az00,
		BgL_bgl_string2139za700za7za7i2174za7, "node-A", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2135z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2a1328za72175za7,
		BGl_z62nodezd2A1328zb0zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2138z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2at2176za7,
		BGl_z62nodezd2Azd2atom1332z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2140z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2kw2177za7,
		BGl_z62nodezd2Azd2kwote1334z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2141z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2va2178za7,
		BGl_z62nodezd2Azd2var1336z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2142z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2cl2179za7,
		BGl_z62nodezd2Azd2closure1338z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2143z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2se2180za7,
		BGl_z62nodezd2Azd2sequence1340z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2144z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2sy2181za7,
		BGl_z62nodezd2Azd2sync1342z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2145z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2ap2182za7,
		BGl_z62nodezd2Azd2app1344z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2146z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2ap2183za7,
		BGl_z62nodezd2Azd2appzd2ly1346zb0zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2147z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2fu2184za7,
		BGl_z62nodezd2Azd2funcall1348z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2148z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2ex2185za7,
		BGl_z62nodezd2Azd2extern1350z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2149z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2ca2186za7,
		BGl_z62nodezd2Azd2cast1352z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2150z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2se2187za7,
		BGl_z62nodezd2Azd2setq1354z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2151z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2co2188za7,
		BGl_z62nodezd2Azd2conditional1356z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2152z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2fa2189za7,
		BGl_z62nodezd2Azd2fail1358z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2153z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2sw2190za7,
		BGl_z62nodezd2Azd2switch1360z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2154z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2le2191za7,
		BGl_z62nodezd2Azd2letzd2fun1362zb0zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2161z00zzintegrate_az00,
		BgL_bgl_string2161za700za7za7i2192za7, "Unexpected closure", 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2155z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2le2193za7,
		BGl_z62nodezd2Azd2letzd2var1364zb0zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2162z00zzintegrate_az00,
		BgL_bgl_string2162za700za7za7i2194za7, "integrate_a", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2156z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2se2195za7,
		BGl_z62nodezd2Azd2setzd2exzd2it1366z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STRING(BGl_string2163z00zzintegrate_az00,
		BgL_bgl_string2163za700za7za7i2196za7,
		"$env-get-exitd-top $get-exitd-top node-A1328 tail ", 50);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2157z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2ju2197za7,
		BGl_z62nodezd2Azd2jumpzd2exzd2it1368z62zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2158z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2ma2198za7,
		BGl_z62nodezd2Azd2makezd2box1371zb0zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2159z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2bo2199za7,
		BGl_z62nodezd2Azd2boxzd2setz121374za2zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2160z00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7d2bo2200za7,
		BGl_z62nodezd2Azd2boxzd2ref1377zb0zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_STATIC_BGL_GENERIC(BGl_nodezd2Azd2envz00zzintegrate_az00,
		BgL_bgl_za762nodeza7d2aza7b0za7za72201za7,
		BGl_z62nodezd2Azb0zzintegrate_az00, 0L, BUNSPEC, 4);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_Azd2envzd2zzintegrate_az00,
		BgL_bgl_za762aza762za7za7integra2202z00, BGl_z62Az62zzintegrate_az00, 0L,
		BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzintegrate_az00));
		     ADD_ROOT((void *) (&BGl_za2kontza2z00zzintegrate_az00));
		     ADD_ROOT((void *) (&BGl_za2phiza2z00zzintegrate_az00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzintegrate_az00(long
		BgL_checksumz00_3781, char *BgL_fromz00_3782)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzintegrate_az00))
				{
					BGl_requirezd2initializa7ationz75zzintegrate_az00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzintegrate_az00();
					BGl_libraryzd2moduleszd2initz00zzintegrate_az00();
					BGl_cnstzd2initzd2zzintegrate_az00();
					BGl_importedzd2moduleszd2initz00zzintegrate_az00();
					BGl_genericzd2initzd2zzintegrate_az00();
					BGl_methodzd2initzd2zzintegrate_az00();
					return BGl_toplevelzd2initzd2zzintegrate_az00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_az00(void)
	{
		{	/* Integrate/a.scm 18 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"integrate_a");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "integrate_a");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"integrate_a");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "integrate_a");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "integrate_a");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "integrate_a");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "integrate_a");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "integrate_a");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"integrate_a");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "integrate_a");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"integrate_a");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzintegrate_az00(void)
	{
		{	/* Integrate/a.scm 18 */
			{	/* Integrate/a.scm 18 */
				obj_t BgL_cportz00_3402;

				{	/* Integrate/a.scm 18 */
					obj_t BgL_stringz00_3409;

					BgL_stringz00_3409 = BGl_string2163z00zzintegrate_az00;
					{	/* Integrate/a.scm 18 */
						obj_t BgL_startz00_3410;

						BgL_startz00_3410 = BINT(0L);
						{	/* Integrate/a.scm 18 */
							obj_t BgL_endz00_3411;

							BgL_endz00_3411 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3409)));
							{	/* Integrate/a.scm 18 */

								BgL_cportz00_3402 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3409, BgL_startz00_3410, BgL_endz00_3411);
				}}}}
				{
					long BgL_iz00_3403;

					BgL_iz00_3403 = 3L;
				BgL_loopz00_3404:
					if ((BgL_iz00_3403 == -1L))
						{	/* Integrate/a.scm 18 */
							return BUNSPEC;
						}
					else
						{	/* Integrate/a.scm 18 */
							{	/* Integrate/a.scm 18 */
								obj_t BgL_arg2164z00_3405;

								{	/* Integrate/a.scm 18 */

									{	/* Integrate/a.scm 18 */
										obj_t BgL_locationz00_3407;

										BgL_locationz00_3407 = BBOOL(((bool_t) 0));
										{	/* Integrate/a.scm 18 */

											BgL_arg2164z00_3405 =
												BGl_readz00zz__readerz00(BgL_cportz00_3402,
												BgL_locationz00_3407);
										}
									}
								}
								{	/* Integrate/a.scm 18 */
									int BgL_tmpz00_3813;

									BgL_tmpz00_3813 = (int) (BgL_iz00_3403);
									CNST_TABLE_SET(BgL_tmpz00_3813, BgL_arg2164z00_3405);
							}}
							{	/* Integrate/a.scm 18 */
								int BgL_auxz00_3408;

								BgL_auxz00_3408 = (int) ((BgL_iz00_3403 - 1L));
								{
									long BgL_iz00_3818;

									BgL_iz00_3818 = (long) (BgL_auxz00_3408);
									BgL_iz00_3403 = BgL_iz00_3818;
									goto BgL_loopz00_3404;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzintegrate_az00(void)
	{
		{	/* Integrate/a.scm 18 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzintegrate_az00(void)
	{
		{	/* Integrate/a.scm 18 */
			BGl_za2phiza2z00zzintegrate_az00 = BUNSPEC;
			BGl_za2kontza2z00zzintegrate_az00 = BUNSPEC;
			return BUNSPEC;
		}

	}



/* A */
	BGL_EXPORTED_DEF obj_t BGl_Az00zzintegrate_az00(BgL_globalz00_bglt
		BgL_globalz00_3, BgL_nodez00_bglt BgL_nodez00_4)
	{
		{	/* Integrate/a.scm 45 */
			if (CBOOL(BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00))
				{	/* Integrate/a.scm 46 */
					BGl_initzd2returnzd2cachez12z12zzreturn_walkz00();
				}
			else
				{	/* Integrate/a.scm 46 */
					BFALSE;
				}
			{	/* Integrate/a.scm 48 */
				obj_t BgL_list1411z00_1672;

				BgL_list1411z00_1672 = MAKE_YOUNG_PAIR(((obj_t) BgL_globalz00_3), BNIL);
				BGl_za2phiza2z00zzintegrate_az00 = BgL_list1411z00_1672;
			}
			BGl_za2kontza2z00zzintegrate_az00 = BINT(0L);
			BGl_initializa7ezd2funz12z67zzintegrate_az00(
				((BgL_variablez00_bglt) BgL_globalz00_3),
				((BgL_variablez00_bglt) BgL_globalz00_3));
			{	/* Integrate/a.scm 52 */
				obj_t BgL_az00_1673;

				{	/* Integrate/a.scm 53 */
					obj_t BgL_arg1421z00_1675;

					{	/* Integrate/a.scm 53 */
						obj_t BgL_arg1422z00_1676;

						{	/* Integrate/a.scm 53 */
							BgL_typez00_bglt BgL_arg1434z00_1677;

							BgL_arg1434z00_1677 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_globalz00_3)))->BgL_typez00);
							BgL_arg1422z00_1676 =
								BGl_normaliza7ezd2typez75zzintegrate_az00(BgL_arg1434z00_1677);
						}
						BgL_arg1421z00_1675 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1422z00_1676);
					}
					BgL_az00_1673 =
						BGl_nodezd2Azd2zzintegrate_az00(BgL_nodez00_4,
						((BgL_variablez00_bglt) BgL_globalz00_3), BgL_arg1421z00_1675,
						BNIL);
				}
				BGl_tracezd2Azd2zzintegrate_az00(BgL_az00_1673,
					BGl_string2128z00zzintegrate_az00);
				{	/* Integrate/a.scm 55 */
					obj_t BgL_az72z72_1674;

					BgL_az72z72_1674 =
						BGl_tailzd2coercionzd2zzintegrate_az00(BgL_az00_1673,
						BgL_globalz00_3);
					BGl_tracezd2Azd2zzintegrate_az00(BgL_az72z72_1674,
						BGl_string2129z00zzintegrate_az00);
					return BgL_az72z72_1674;
				}
			}
		}

	}



/* &A */
	obj_t BGl_z62Az62zzintegrate_az00(obj_t BgL_envz00_3253,
		obj_t BgL_globalz00_3254, obj_t BgL_nodez00_3255)
	{
		{	/* Integrate/a.scm 45 */
			return
				BGl_Az00zzintegrate_az00(
				((BgL_globalz00_bglt) BgL_globalz00_3254),
				((BgL_nodez00_bglt) BgL_nodez00_3255));
		}

	}



/* initialize-fun! */
	bool_t BGl_initializa7ezd2funz12z67zzintegrate_az00(BgL_variablez00_bglt
		BgL_funz00_5, BgL_variablez00_bglt BgL_ownerz00_6)
	{
		{	/* Integrate/a.scm 62 */
			{	/* Integrate/a.scm 63 */
				BgL_sfunz00_bglt BgL_tmp1109z00_1678;

				BgL_tmp1109z00_1678 =
					((BgL_sfunz00_bglt)
					(((BgL_variablez00_bglt) COBJECT(BgL_funz00_5))->BgL_valuez00));
				{	/* Integrate/a.scm 63 */
					BgL_sfunzf2iinfozf2_bglt BgL_wide1111z00_1680;

					BgL_wide1111z00_1680 =
						((BgL_sfunzf2iinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_sfunzf2iinfozf2_bgl))));
					{	/* Integrate/a.scm 63 */
						obj_t BgL_auxz00_3849;
						BgL_objectz00_bglt BgL_tmpz00_3846;

						BgL_auxz00_3849 = ((obj_t) BgL_wide1111z00_1680);
						BgL_tmpz00_3846 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3846, BgL_auxz00_3849);
					}
					((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
					{	/* Integrate/a.scm 63 */
						long BgL_arg1437z00_1681;

						BgL_arg1437z00_1681 =
							BGL_CLASS_NUM(BGl_sfunzf2Iinfozf2zzintegrate_infoz00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_sfunz00_bglt) BgL_tmp1109z00_1678)), BgL_arg1437z00_1681);
					}
					((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3860;

					{
						obj_t BgL_auxz00_3861;

						{	/* Integrate/a.scm 64 */
							BgL_objectz00_bglt BgL_tmpz00_3862;

							BgL_tmpz00_3862 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_3861 = BGL_OBJECT_WIDENING(BgL_tmpz00_3862);
						}
						BgL_auxz00_3860 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3861);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3860))->
							BgL_ownerz00) = ((obj_t) ((obj_t) BgL_ownerz00_6)), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3869;

					{
						obj_t BgL_auxz00_3870;

						{	/* Integrate/iinfo.scm 87 */
							BgL_objectz00_bglt BgL_tmpz00_3871;

							BgL_tmpz00_3871 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_3870 = BGL_OBJECT_WIDENING(BgL_tmpz00_3871);
						}
						BgL_auxz00_3869 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3870);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3869))->
							BgL_freez00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3877;

					{
						obj_t BgL_auxz00_3878;

						{	/* Integrate/iinfo.scm 51 */
							BgL_objectz00_bglt BgL_tmpz00_3879;

							BgL_tmpz00_3879 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_3878 = BGL_OBJECT_WIDENING(BgL_tmpz00_3879);
						}
						BgL_auxz00_3877 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3878);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3877))->
							BgL_boundz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3885;

					{
						obj_t BgL_auxz00_3886;

						{	/* Integrate/iinfo.scm 53 */
							BgL_objectz00_bglt BgL_tmpz00_3887;

							BgL_tmpz00_3887 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_3886 = BGL_OBJECT_WIDENING(BgL_tmpz00_3887);
						}
						BgL_auxz00_3885 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3886);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3885))->
							BgL_cfromz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3893;

					{
						obj_t BgL_auxz00_3894;

						{	/* Integrate/iinfo.scm 87 */
							BgL_objectz00_bglt BgL_tmpz00_3895;

							BgL_tmpz00_3895 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_3894 = BGL_OBJECT_WIDENING(BgL_tmpz00_3895);
						}
						BgL_auxz00_3893 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3894);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3893))->BgL_ctoz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3901;

					{
						obj_t BgL_auxz00_3902;

						{	/* Integrate/iinfo.scm 57 */
							BgL_objectz00_bglt BgL_tmpz00_3903;

							BgL_tmpz00_3903 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_3902 = BGL_OBJECT_WIDENING(BgL_tmpz00_3903);
						}
						BgL_auxz00_3901 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3902);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3901))->BgL_kz00) =
						((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3909;

					{
						obj_t BgL_auxz00_3910;

						{	/* Integrate/iinfo.scm 59 */
							BgL_objectz00_bglt BgL_tmpz00_3911;

							BgL_tmpz00_3911 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_3910 = BGL_OBJECT_WIDENING(BgL_tmpz00_3911);
						}
						BgL_auxz00_3909 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3910);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3909))->
							BgL_kza2za2) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3917;

					{
						obj_t BgL_auxz00_3918;

						{	/* Integrate/iinfo.scm 87 */
							BgL_objectz00_bglt BgL_tmpz00_3919;

							BgL_tmpz00_3919 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_3918 = BGL_OBJECT_WIDENING(BgL_tmpz00_3919);
						}
						BgL_auxz00_3917 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3918);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3917))->BgL_uz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3925;

					{
						obj_t BgL_auxz00_3926;

						{	/* Integrate/iinfo.scm 63 */
							BgL_objectz00_bglt BgL_tmpz00_3927;

							BgL_tmpz00_3927 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_3926 = BGL_OBJECT_WIDENING(BgL_tmpz00_3927);
						}
						BgL_auxz00_3925 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3926);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3925))->BgL_cnz00) =
						((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3933;

					{
						obj_t BgL_auxz00_3934;

						{	/* Integrate/iinfo.scm 65 */
							BgL_objectz00_bglt BgL_tmpz00_3935;

							BgL_tmpz00_3935 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_3934 = BGL_OBJECT_WIDENING(BgL_tmpz00_3935);
						}
						BgL_auxz00_3933 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3934);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3933))->BgL_ctz00) =
						((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3941;

					{
						obj_t BgL_auxz00_3942;

						{	/* Integrate/iinfo.scm 67 */
							BgL_objectz00_bglt BgL_tmpz00_3943;

							BgL_tmpz00_3943 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_3942 = BGL_OBJECT_WIDENING(BgL_tmpz00_3943);
						}
						BgL_auxz00_3941 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3942);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3941))->
							BgL_kontz00) = ((obj_t) BNIL), BUNSPEC);
				}
				{
					bool_t BgL_auxz00_3956;
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3949;

					{	/* Integrate/a.scm 65 */
						obj_t BgL_classz00_2546;

						BgL_classz00_2546 = BGl_globalz00zzast_varz00;
						{	/* Integrate/a.scm 65 */
							BgL_objectz00_bglt BgL_arg1807z00_2548;

							{	/* Integrate/a.scm 65 */
								obj_t BgL_tmpz00_3957;

								BgL_tmpz00_3957 = ((obj_t) BgL_funz00_5);
								BgL_arg1807z00_2548 = (BgL_objectz00_bglt) (BgL_tmpz00_3957);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Integrate/a.scm 65 */
									long BgL_idxz00_2554;

									BgL_idxz00_2554 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2548);
									{	/* Integrate/a.scm 65 */
										obj_t BgL_arg1800z00_2555;

										{	/* Integrate/a.scm 65 */
											long BgL_arg1801z00_2556;

											BgL_arg1801z00_2556 = (BgL_idxz00_2554 + 2L);
											{	/* Integrate/a.scm 65 */
												obj_t BgL_vectorz00_2558;

												BgL_vectorz00_2558 =
													BGl_za2inheritancesza2z00zz__objectz00;
												BgL_arg1800z00_2555 =
													VECTOR_REF(BgL_vectorz00_2558, BgL_arg1801z00_2556);
										}}
										BgL_auxz00_3956 =
											(BgL_arg1800z00_2555 == BgL_classz00_2546);
								}}
							else
								{	/* Integrate/a.scm 65 */
									bool_t BgL_res2107z00_2579;

									{	/* Integrate/a.scm 65 */
										obj_t BgL_oclassz00_2562;

										{	/* Integrate/a.scm 65 */
											obj_t BgL_arg1815z00_2570;
											long BgL_arg1816z00_2571;

											BgL_arg1815z00_2570 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Integrate/a.scm 65 */
												long BgL_arg1817z00_2572;

												BgL_arg1817z00_2572 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2548);
												BgL_arg1816z00_2571 =
													(BgL_arg1817z00_2572 - OBJECT_TYPE);
											}
											BgL_oclassz00_2562 =
												VECTOR_REF(BgL_arg1815z00_2570, BgL_arg1816z00_2571);
										}
										{	/* Integrate/a.scm 65 */
											bool_t BgL__ortest_1115z00_2563;

											BgL__ortest_1115z00_2563 =
												(BgL_classz00_2546 == BgL_oclassz00_2562);
											if (BgL__ortest_1115z00_2563)
												{	/* Integrate/a.scm 65 */
													BgL_res2107z00_2579 = BgL__ortest_1115z00_2563;
												}
											else
												{	/* Integrate/a.scm 65 */
													long BgL_odepthz00_2564;

													{	/* Integrate/a.scm 65 */
														obj_t BgL_arg1804z00_2565;

														BgL_arg1804z00_2565 = (BgL_oclassz00_2562);
														BgL_odepthz00_2564 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2565);
													}
													if ((2L < BgL_odepthz00_2564))
														{	/* Integrate/a.scm 65 */
															obj_t BgL_arg1802z00_2567;

															{	/* Integrate/a.scm 65 */
																obj_t BgL_arg1803z00_2568;

																BgL_arg1803z00_2568 = (BgL_oclassz00_2562);
																BgL_arg1802z00_2567 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2568,
																	2L);
															}
															BgL_res2107z00_2579 =
																(BgL_arg1802z00_2567 == BgL_classz00_2546);
														}
													else
														{	/* Integrate/a.scm 65 */
															BgL_res2107z00_2579 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_auxz00_3956 = BgL_res2107z00_2579;
								}
						}
					}
					{
						obj_t BgL_auxz00_3950;

						{	/* Integrate/a.scm 65 */
							BgL_objectz00_bglt BgL_tmpz00_3951;

							BgL_tmpz00_3951 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_3950 = BGL_OBJECT_WIDENING(BgL_tmpz00_3951);
						}
						BgL_auxz00_3949 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3950);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3949))->
							BgL_gzf3zf3) = ((bool_t) BgL_auxz00_3956), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3980;

					{
						obj_t BgL_auxz00_3981;

						{	/* Integrate/iinfo.scm 87 */
							BgL_objectz00_bglt BgL_tmpz00_3982;

							BgL_tmpz00_3982 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_3981 = BGL_OBJECT_WIDENING(BgL_tmpz00_3982);
						}
						BgL_auxz00_3980 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3981);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3980))->
							BgL_forcegzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3988;

					{
						obj_t BgL_auxz00_3989;

						{	/* Integrate/iinfo.scm 87 */
							BgL_objectz00_bglt BgL_tmpz00_3990;

							BgL_tmpz00_3990 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_3989 = BGL_OBJECT_WIDENING(BgL_tmpz00_3990);
						}
						BgL_auxz00_3988 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3989);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3988))->BgL_lz00) =
						((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3996;

					{
						obj_t BgL_auxz00_3997;

						{	/* Integrate/iinfo.scm 75 */
							BgL_objectz00_bglt BgL_tmpz00_3998;

							BgL_tmpz00_3998 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_3997 = BGL_OBJECT_WIDENING(BgL_tmpz00_3998);
						}
						BgL_auxz00_3996 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3997);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3996))->BgL_ledz00) =
						((obj_t) BNIL), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4004;

					{
						obj_t BgL_auxz00_4005;

						{	/* Integrate/iinfo.scm 87 */
							BgL_objectz00_bglt BgL_tmpz00_4006;

							BgL_tmpz00_4006 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_4005 = BGL_OBJECT_WIDENING(BgL_tmpz00_4006);
						}
						BgL_auxz00_4004 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4005);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4004))->
							BgL_istampz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4012;

					{
						obj_t BgL_auxz00_4013;

						{	/* Integrate/iinfo.scm 87 */
							BgL_objectz00_bglt BgL_tmpz00_4014;

							BgL_tmpz00_4014 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_4013 = BGL_OBJECT_WIDENING(BgL_tmpz00_4014);
						}
						BgL_auxz00_4012 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4013);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4012))->
							BgL_globalz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4020;

					{
						obj_t BgL_auxz00_4021;

						{	/* Integrate/iinfo.scm 87 */
							BgL_objectz00_bglt BgL_tmpz00_4022;

							BgL_tmpz00_4022 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_4021 = BGL_OBJECT_WIDENING(BgL_tmpz00_4022);
						}
						BgL_auxz00_4020 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4021);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4020))->
							BgL_kapturedz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4028;

					{
						obj_t BgL_auxz00_4029;

						{	/* Integrate/iinfo.scm 87 */
							BgL_objectz00_bglt BgL_tmpz00_4030;

							BgL_tmpz00_4030 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_4029 = BGL_OBJECT_WIDENING(BgL_tmpz00_4030);
						}
						BgL_auxz00_4028 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4029);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4028))->
							BgL_tailzd2coercionzd2) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4036;

					{
						obj_t BgL_auxz00_4037;

						{	/* Integrate/iinfo.scm 87 */
							BgL_objectz00_bglt BgL_tmpz00_4038;

							BgL_tmpz00_4038 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_4037 = BGL_OBJECT_WIDENING(BgL_tmpz00_4038);
						}
						BgL_auxz00_4036 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4037);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4036))->
							BgL_xhdlzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
				}
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4044;

					{
						obj_t BgL_auxz00_4045;

						{	/* Integrate/iinfo.scm 87 */
							BgL_objectz00_bglt BgL_tmpz00_4046;

							BgL_tmpz00_4046 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_tmp1109z00_1678));
							BgL_auxz00_4045 = BGL_OBJECT_WIDENING(BgL_tmpz00_4046);
						}
						BgL_auxz00_4044 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4045);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4044))->
							BgL_xhdlsz00) = ((obj_t) BNIL), BUNSPEC);
				}
				((BgL_sfunz00_bglt) BgL_tmp1109z00_1678);
			}
			{	/* Integrate/a.scm 66 */
				obj_t BgL_g1314z00_1683;

				BgL_g1314z00_1683 =
					(((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(BgL_funz00_5))->
									BgL_valuez00))))->BgL_argsz00);
				{
					obj_t BgL_l1312z00_1685;

					BgL_l1312z00_1685 = BgL_g1314z00_1683;
				BgL_zc3z04anonymousza31438ze3z87_1686:
					if (PAIRP(BgL_l1312z00_1685))
						{	/* Integrate/a.scm 68 */
							{	/* Integrate/a.scm 67 */
								obj_t BgL_xz00_1688;

								BgL_xz00_1688 = CAR(BgL_l1312z00_1685);
								{	/* Integrate/a.scm 67 */
									BgL_svarz00_bglt BgL_tmp1114z00_1689;

									BgL_tmp1114z00_1689 =
										((BgL_svarz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_xz00_1688))))->
											BgL_valuez00));
									{	/* Integrate/a.scm 67 */
										BgL_svarzf2iinfozf2_bglt BgL_wide1116z00_1691;

										BgL_wide1116z00_1691 =
											((BgL_svarzf2iinfozf2_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_svarzf2iinfozf2_bgl))));
										{	/* Integrate/a.scm 67 */
											obj_t BgL_auxz00_4067;
											BgL_objectz00_bglt BgL_tmpz00_4064;

											BgL_auxz00_4067 = ((obj_t) BgL_wide1116z00_1691);
											BgL_tmpz00_4064 =
												((BgL_objectz00_bglt)
												((BgL_svarz00_bglt) BgL_tmp1114z00_1689));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4064, BgL_auxz00_4067);
										}
										((BgL_objectz00_bglt)
											((BgL_svarz00_bglt) BgL_tmp1114z00_1689));
										{	/* Integrate/a.scm 67 */
											long BgL_arg1448z00_1692;

											BgL_arg1448z00_1692 =
												BGL_CLASS_NUM(BGl_svarzf2Iinfozf2zzintegrate_infoz00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt)
													((BgL_svarz00_bglt) BgL_tmp1114z00_1689)),
												BgL_arg1448z00_1692);
										}
										((BgL_svarz00_bglt)
											((BgL_svarz00_bglt) BgL_tmp1114z00_1689));
									}
									{
										BgL_svarzf2iinfozf2_bglt BgL_auxz00_4078;

										{
											obj_t BgL_auxz00_4079;

											{	/* Integrate/a.scm 67 */
												BgL_objectz00_bglt BgL_tmpz00_4080;

												BgL_tmpz00_4080 =
													((BgL_objectz00_bglt)
													((BgL_svarz00_bglt) BgL_tmp1114z00_1689));
												BgL_auxz00_4079 = BGL_OBJECT_WIDENING(BgL_tmpz00_4080);
											}
											BgL_auxz00_4078 =
												((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_4079);
										}
										((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4078))->
												BgL_fzd2markzd2) = ((obj_t) BUNSPEC), BUNSPEC);
									}
									{
										BgL_svarzf2iinfozf2_bglt BgL_auxz00_4086;

										{
											obj_t BgL_auxz00_4087;

											{	/* Integrate/a.scm 67 */
												BgL_objectz00_bglt BgL_tmpz00_4088;

												BgL_tmpz00_4088 =
													((BgL_objectz00_bglt)
													((BgL_svarz00_bglt) BgL_tmp1114z00_1689));
												BgL_auxz00_4087 = BGL_OBJECT_WIDENING(BgL_tmpz00_4088);
											}
											BgL_auxz00_4086 =
												((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_4087);
										}
										((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4086))->
												BgL_uzd2markzd2) = ((obj_t) BUNSPEC), BUNSPEC);
									}
									{
										BgL_svarzf2iinfozf2_bglt BgL_auxz00_4094;

										{
											obj_t BgL_auxz00_4095;

											{	/* Integrate/a.scm 67 */
												BgL_objectz00_bglt BgL_tmpz00_4096;

												BgL_tmpz00_4096 =
													((BgL_objectz00_bglt)
													((BgL_svarz00_bglt) BgL_tmp1114z00_1689));
												BgL_auxz00_4095 = BGL_OBJECT_WIDENING(BgL_tmpz00_4096);
											}
											BgL_auxz00_4094 =
												((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_4095);
										}
										((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4094))->
												BgL_kapturedzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
									}
									{
										BgL_svarzf2iinfozf2_bglt BgL_auxz00_4102;

										{
											obj_t BgL_auxz00_4103;

											{	/* Integrate/a.scm 67 */
												BgL_objectz00_bglt BgL_tmpz00_4104;

												BgL_tmpz00_4104 =
													((BgL_objectz00_bglt)
													((BgL_svarz00_bglt) BgL_tmp1114z00_1689));
												BgL_auxz00_4103 = BGL_OBJECT_WIDENING(BgL_tmpz00_4104);
											}
											BgL_auxz00_4102 =
												((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_4103);
										}
										((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4102))->
												BgL_celledzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
									}
									{
										BgL_svarzf2iinfozf2_bglt BgL_auxz00_4110;

										{
											obj_t BgL_auxz00_4111;

											{	/* Integrate/a.scm 67 */
												BgL_objectz00_bglt BgL_tmpz00_4112;

												BgL_tmpz00_4112 =
													((BgL_objectz00_bglt)
													((BgL_svarz00_bglt) BgL_tmp1114z00_1689));
												BgL_auxz00_4111 = BGL_OBJECT_WIDENING(BgL_tmpz00_4112);
											}
											BgL_auxz00_4110 =
												((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_4111);
										}
										((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4110))->
												BgL_xhdlz00) = ((obj_t) BFALSE), BUNSPEC);
									}
									((BgL_svarz00_bglt) BgL_tmp1114z00_1689);
							}}
							{
								obj_t BgL_l1312z00_4119;

								BgL_l1312z00_4119 = CDR(BgL_l1312z00_1685);
								BgL_l1312z00_1685 = BgL_l1312z00_4119;
								goto BgL_zc3z04anonymousza31438ze3z87_1686;
							}
						}
					else
						{	/* Integrate/a.scm 68 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* get-new-kont */
	obj_t BGl_getzd2newzd2kontz00zzintegrate_az00(void)
	{
		{	/* Integrate/a.scm 78 */
			BGl_za2kontza2z00zzintegrate_az00 =
				ADDFX(BINT(1L), BGl_za2kontza2z00zzintegrate_az00);
			return BGl_za2kontza2z00zzintegrate_az00;
		}

	}



/* trace-A */
	obj_t BGl_tracezd2Azd2zzintegrate_az00(obj_t BgL_az00_7, obj_t BgL_msgz00_8)
	{
		{	/* Integrate/a.scm 85 */
			{	/* Integrate/a.scm 86 */
				obj_t BgL_pz00_1697;

				{	/* Integrate/a.scm 88 */
					obj_t BgL_zc3z04anonymousza31473ze3z87_3256;

					BgL_zc3z04anonymousza31473ze3z87_3256 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31473ze3ze5zzintegrate_az00, (int) (0L),
						(int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31473ze3z87_3256, (int) (0L),
						BgL_az00_7);
					BgL_pz00_1697 =
						BGl_withzd2outputzd2tozd2stringzd2zz__r4_ports_6_10_1z00
						(BgL_zc3z04anonymousza31473ze3z87_3256);
				}
				return BNIL;
			}
		}

	}



/* &<@anonymous:1473> */
	obj_t BGl_z62zc3z04anonymousza31473ze3ze5zzintegrate_az00(obj_t
		BgL_envz00_3257)
	{
		{	/* Integrate/a.scm 87 */
			{	/* Integrate/a.scm 88 */
				obj_t BgL_az00_3258;

				BgL_az00_3258 = PROCEDURE_REF(BgL_envz00_3257, (int) (0L));
				{	/* Integrate/a.scm 88 */
					bool_t BgL_tmpz00_4131;

					{
						obj_t BgL_l1316z00_3414;

						BgL_l1316z00_3414 = BgL_az00_3258;
					BgL_zc3z04anonymousza31474ze3z87_3413:
						if (PAIRP(BgL_l1316z00_3414))
							{	/* Integrate/a.scm 88 */
								{	/* Integrate/a.scm 89 */
									obj_t BgL_az00_3415;

									BgL_az00_3415 = CAR(BgL_l1316z00_3414);
									{	/* Integrate/a.scm 89 */
										obj_t BgL_port1315z00_3416;

										{	/* Integrate/a.scm 89 */
											obj_t BgL_tmpz00_4135;

											BgL_tmpz00_4135 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_port1315z00_3416 =
												BGL_ENV_CURRENT_OUTPUT_PORT(BgL_tmpz00_4135);
										}
										bgl_display_string(BGl_string2130z00zzintegrate_az00,
											BgL_port1315z00_3416);
										{	/* Integrate/a.scm 89 */
											obj_t BgL_arg1485z00_3417;

											{	/* Integrate/a.scm 89 */
												obj_t BgL_arg1489z00_3418;

												BgL_arg1489z00_3418 = CAR(((obj_t) BgL_az00_3415));
												BgL_arg1485z00_3417 =
													BGl_shapez00zztools_shapez00(BgL_arg1489z00_3418);
											}
											bgl_display_obj(BgL_arg1485z00_3417,
												BgL_port1315z00_3416);
										}
										bgl_display_string(BGl_string2131z00zzintegrate_az00,
											BgL_port1315z00_3416);
										{	/* Integrate/a.scm 90 */
											obj_t BgL_arg1502z00_3419;

											{	/* Integrate/a.scm 90 */
												obj_t BgL_arg1509z00_3420;

												{	/* Integrate/a.scm 90 */
													obj_t BgL_pairz00_3421;

													BgL_pairz00_3421 = CDR(((obj_t) BgL_az00_3415));
													BgL_arg1509z00_3420 = CAR(BgL_pairz00_3421);
												}
												BgL_arg1502z00_3419 =
													BGl_shapez00zztools_shapez00(BgL_arg1509z00_3420);
											}
											bgl_display_obj(BgL_arg1502z00_3419,
												BgL_port1315z00_3416);
										}
										bgl_display_string(BGl_string2131z00zzintegrate_az00,
											BgL_port1315z00_3416);
										{	/* Integrate/a.scm 91 */
											obj_t BgL_arg1513z00_3422;

											{	/* Integrate/a.scm 91 */
												obj_t BgL_arg1514z00_3423;

												{	/* Integrate/a.scm 91 */
													obj_t BgL_pairz00_3424;

													{	/* Integrate/a.scm 91 */
														obj_t BgL_pairz00_3425;

														BgL_pairz00_3425 = CDR(((obj_t) BgL_az00_3415));
														BgL_pairz00_3424 = CDR(BgL_pairz00_3425);
													}
													BgL_arg1514z00_3423 = CAR(BgL_pairz00_3424);
												}
												BgL_arg1513z00_3422 =
													BGl_shapez00zztools_shapez00(BgL_arg1514z00_3423);
											}
											bgl_display_obj(BgL_arg1513z00_3422,
												BgL_port1315z00_3416);
										}
										bgl_display_string(BGl_string2132z00zzintegrate_az00,
											BgL_port1315z00_3416);
										bgl_display_char(((unsigned char) 10),
											BgL_port1315z00_3416);
								}}
								{
									obj_t BgL_l1316z00_4158;

									BgL_l1316z00_4158 = CDR(BgL_l1316z00_3414);
									BgL_l1316z00_3414 = BgL_l1316z00_4158;
									goto BgL_zc3z04anonymousza31474ze3z87_3413;
								}
							}
						else
							{	/* Integrate/a.scm 88 */
								BgL_tmpz00_4131 = ((bool_t) 1);
							}
					}
					return BBOOL(BgL_tmpz00_4131);
				}
			}
		}

	}



/* normalize-type */
	obj_t BGl_normaliza7ezd2typez75zzintegrate_az00(BgL_typez00_bglt BgL_tyz00_11)
	{
		{	/* Integrate/a.scm 110 */
			if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_tyz00_11))
				{	/* Integrate/a.scm 112 */
					return BGl_za2objza2z00zztype_cachez00;
				}
			else
				{	/* Integrate/a.scm 112 */
					if ((((obj_t) BgL_tyz00_11) == BGl_za2intza2z00zztype_cachez00))
						{	/* Integrate/a.scm 113 */
							return BGl_za2longza2z00zztype_cachez00;
						}
					else
						{	/* Integrate/a.scm 113 */
							return ((obj_t) BgL_tyz00_11);
						}
				}
		}

	}



/* tail-type-compatible? */
	bool_t BGl_tailzd2typezd2compatiblezf3zf3zzintegrate_az00(obj_t BgL_t1z00_12,
		obj_t BgL_t2z00_13)
	{
		{	/* Integrate/a.scm 122 */
			{	/* Integrate/a.scm 123 */
				bool_t BgL__ortest_1119z00_1717;

				BgL__ortest_1119z00_1717 = (BgL_t1z00_12 == BgL_t2z00_13);
				if (BgL__ortest_1119z00_1717)
					{	/* Integrate/a.scm 123 */
						return BgL__ortest_1119z00_1717;
					}
				else
					{	/* Integrate/a.scm 124 */
						bool_t BgL__ortest_1120z00_1718;

						if ((BgL_t1z00_12 == BGl_za2intza2z00zztype_cachez00))
							{	/* Integrate/a.scm 124 */
								BgL__ortest_1120z00_1718 =
									(BgL_t2z00_13 == BGl_za2longza2z00zztype_cachez00);
							}
						else
							{	/* Integrate/a.scm 124 */
								BgL__ortest_1120z00_1718 = ((bool_t) 0);
							}
						if (BgL__ortest_1120z00_1718)
							{	/* Integrate/a.scm 124 */
								return BgL__ortest_1120z00_1718;
							}
						else
							{	/* Integrate/a.scm 125 */
								bool_t BgL__ortest_1121z00_1719;

								if ((BgL_t1z00_12 == BGl_za2longza2z00zztype_cachez00))
									{	/* Integrate/a.scm 125 */
										BgL__ortest_1121z00_1719 =
											(BgL_t2z00_13 == BGl_za2intza2z00zztype_cachez00);
									}
								else
									{	/* Integrate/a.scm 125 */
										BgL__ortest_1121z00_1719 = ((bool_t) 0);
									}
								if (BgL__ortest_1121z00_1719)
									{	/* Integrate/a.scm 125 */
										return BgL__ortest_1121z00_1719;
									}
								else
									{	/* Integrate/a.scm 125 */
										if (BGl_bigloozd2typezf3z21zztype_typez00(
												((BgL_typez00_bglt) BgL_t1z00_12)))
											{	/* Integrate/a.scm 128 */
												return
													BGl_bigloozd2typezf3z21zztype_typez00(
													((BgL_typez00_bglt) BgL_t2z00_13));
											}
										else
											{	/* Integrate/a.scm 128 */
												return ((bool_t) 0);
											}
									}
							}
					}
			}
		}

	}



/* tail-coercion */
	obj_t BGl_tailzd2coercionzd2zzintegrate_az00(obj_t BgL_az00_14,
		BgL_globalz00_bglt BgL_globalz00_15)
	{
		{	/* Integrate/a.scm 138 */
			{
				obj_t BgL_l1318z00_1724;

				BgL_l1318z00_1724 = BgL_az00_14;
			BgL_zc3z04anonymousza31537ze3z87_1725:
				if (PAIRP(BgL_l1318z00_1724))
					{	/* Integrate/a.scm 140 */
						{	/* Integrate/a.scm 141 */
							obj_t BgL_az00_1727;

							BgL_az00_1727 = CAR(BgL_l1318z00_1724);
							{
								obj_t BgL_calleez00_1728;
								obj_t BgL_typez00_1729;

								if (PAIRP(BgL_az00_1727))
									{	/* Integrate/a.scm 141 */
										obj_t BgL_cdrzd2366zd2_1733;

										BgL_cdrzd2366zd2_1733 = CDR(((obj_t) BgL_az00_1727));
										if (PAIRP(BgL_cdrzd2366zd2_1733))
											{	/* Integrate/a.scm 141 */
												obj_t BgL_cdrzd2370zd2_1735;

												BgL_cdrzd2370zd2_1735 = CDR(BgL_cdrzd2366zd2_1733);
												if (PAIRP(BgL_cdrzd2370zd2_1735))
													{	/* Integrate/a.scm 141 */
														obj_t BgL_carzd2373zd2_1737;

														BgL_carzd2373zd2_1737 = CAR(BgL_cdrzd2370zd2_1735);
														if (PAIRP(BgL_carzd2373zd2_1737))
															{	/* Integrate/a.scm 141 */
																if (NULLP(CDR(BgL_cdrzd2370zd2_1735)))
																	{	/* Integrate/a.scm 141 */
																		BgL_calleez00_1728 =
																			CAR(BgL_cdrzd2366zd2_1733);
																		BgL_typez00_1729 =
																			CDR(BgL_carzd2373zd2_1737);
																		{	/* Integrate/a.scm 143 */
																			BgL_valuez00_bglt BgL_funz00_1744;

																			BgL_funz00_1744 =
																				(((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							BgL_calleez00_1728)))->
																				BgL_valuez00);
																			{	/* Integrate/a.scm 146 */
																				bool_t BgL_test2225z00_4202;

																				{	/* Integrate/a.scm 146 */
																					obj_t BgL_tmpz00_4203;

																					{
																						BgL_sfunzf2iinfozf2_bglt
																							BgL_auxz00_4204;
																						{
																							obj_t BgL_auxz00_4205;

																							{	/* Integrate/a.scm 146 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_4206;
																								BgL_tmpz00_4206 =
																									((BgL_objectz00_bglt) (
																										(BgL_sfunz00_bglt)
																										BgL_funz00_1744));
																								BgL_auxz00_4205 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_4206);
																							}
																							BgL_auxz00_4204 =
																								((BgL_sfunzf2iinfozf2_bglt)
																								BgL_auxz00_4205);
																						}
																						BgL_tmpz00_4203 =
																							(((BgL_sfunzf2iinfozf2_bglt)
																								COBJECT(BgL_auxz00_4204))->
																							BgL_tailzd2coercionzd2);
																					}
																					BgL_test2225z00_4202 =
																						CBOOL(BgL_tmpz00_4203);
																				}
																				if (BgL_test2225z00_4202)
																					{	/* Integrate/a.scm 148 */
																						bool_t BgL_test2226z00_4213;

																						{	/* Integrate/a.scm 148 */
																							obj_t BgL_classz00_2631;

																							BgL_classz00_2631 =
																								BGl_typez00zztype_typez00;
																							if (BGL_OBJECTP(BgL_typez00_1729))
																								{	/* Integrate/a.scm 148 */
																									BgL_objectz00_bglt
																										BgL_arg1807z00_2633;
																									BgL_arg1807z00_2633 =
																										(BgL_objectz00_bglt)
																										(BgL_typez00_1729);
																									if (BGL_CONDEXPAND_ISA_ARCH64
																										())
																										{	/* Integrate/a.scm 148 */
																											long BgL_idxz00_2639;

																											BgL_idxz00_2639 =
																												BGL_OBJECT_INHERITANCE_NUM
																												(BgL_arg1807z00_2633);
																											BgL_test2226z00_4213 =
																												(VECTOR_REF
																												(BGl_za2inheritancesza2z00zz__objectz00,
																													(BgL_idxz00_2639 +
																														1L)) ==
																												BgL_classz00_2631);
																										}
																									else
																										{	/* Integrate/a.scm 148 */
																											bool_t
																												BgL_res2108z00_2664;
																											{	/* Integrate/a.scm 148 */
																												obj_t
																													BgL_oclassz00_2647;
																												{	/* Integrate/a.scm 148 */
																													obj_t
																														BgL_arg1815z00_2655;
																													long
																														BgL_arg1816z00_2656;
																													BgL_arg1815z00_2655 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* Integrate/a.scm 148 */
																														long
																															BgL_arg1817z00_2657;
																														BgL_arg1817z00_2657
																															=
																															BGL_OBJECT_CLASS_NUM
																															(BgL_arg1807z00_2633);
																														BgL_arg1816z00_2656
																															=
																															(BgL_arg1817z00_2657
																															- OBJECT_TYPE);
																													}
																													BgL_oclassz00_2647 =
																														VECTOR_REF
																														(BgL_arg1815z00_2655,
																														BgL_arg1816z00_2656);
																												}
																												{	/* Integrate/a.scm 148 */
																													bool_t
																														BgL__ortest_1115z00_2648;
																													BgL__ortest_1115z00_2648
																														=
																														(BgL_classz00_2631
																														==
																														BgL_oclassz00_2647);
																													if (BgL__ortest_1115z00_2648)
																														{	/* Integrate/a.scm 148 */
																															BgL_res2108z00_2664
																																=
																																BgL__ortest_1115z00_2648;
																														}
																													else
																														{	/* Integrate/a.scm 148 */
																															long
																																BgL_odepthz00_2649;
																															{	/* Integrate/a.scm 148 */
																																obj_t
																																	BgL_arg1804z00_2650;
																																BgL_arg1804z00_2650
																																	=
																																	(BgL_oclassz00_2647);
																																BgL_odepthz00_2649
																																	=
																																	BGL_CLASS_DEPTH
																																	(BgL_arg1804z00_2650);
																															}
																															if (
																																(1L <
																																	BgL_odepthz00_2649))
																																{	/* Integrate/a.scm 148 */
																																	obj_t
																																		BgL_arg1802z00_2652;
																																	{	/* Integrate/a.scm 148 */
																																		obj_t
																																			BgL_arg1803z00_2653;
																																		BgL_arg1803z00_2653
																																			=
																																			(BgL_oclassz00_2647);
																																		BgL_arg1802z00_2652
																																			=
																																			BGL_CLASS_ANCESTORS_REF
																																			(BgL_arg1803z00_2653,
																																			1L);
																																	}
																																	BgL_res2108z00_2664
																																		=
																																		(BgL_arg1802z00_2652
																																		==
																																		BgL_classz00_2631);
																																}
																															else
																																{	/* Integrate/a.scm 148 */
																																	BgL_res2108z00_2664
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																											}
																											BgL_test2226z00_4213 =
																												BgL_res2108z00_2664;
																										}
																								}
																							else
																								{	/* Integrate/a.scm 148 */
																									BgL_test2226z00_4213 =
																										((bool_t) 0);
																								}
																						}
																						if (BgL_test2226z00_4213)
																							{	/* Integrate/a.scm 150 */
																								bool_t BgL_test2231z00_4236;

																								if (
																									(BgL_typez00_1729 ==
																										BGl_za2objza2z00zztype_cachez00))
																									{	/* Integrate/a.scm 150 */
																										BgL_test2231z00_4236 =
																											((bool_t) 1);
																									}
																								else
																									{	/* Integrate/a.scm 150 */
																										BgL_test2231z00_4236 =
																											(BgL_typez00_1729 ==
																											BGl_za2magicza2z00zztype_cachez00);
																									}
																								if (BgL_test2231z00_4236)
																									{	/* Integrate/a.scm 150 */
																										BUNSPEC;
																									}
																								else
																									{	/* Integrate/a.scm 152 */
																										bool_t BgL_test2233z00_4240;

																										{	/* Integrate/a.scm 152 */
																											obj_t BgL_arg1615z00_1775;

																											{
																												BgL_sfunzf2iinfozf2_bglt
																													BgL_auxz00_4241;
																												{
																													obj_t BgL_auxz00_4242;

																													{	/* Integrate/a.scm 152 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_4243;
																														BgL_tmpz00_4243 =
																															(
																															(BgL_objectz00_bglt)
																															((BgL_sfunz00_bglt) BgL_funz00_1744));
																														BgL_auxz00_4242 =
																															BGL_OBJECT_WIDENING
																															(BgL_tmpz00_4243);
																													}
																													BgL_auxz00_4241 =
																														(
																														(BgL_sfunzf2iinfozf2_bglt)
																														BgL_auxz00_4242);
																												}
																												BgL_arg1615z00_1775 =
																													(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4241))->BgL_tailzd2coercionzd2);
																											}
																											BgL_test2233z00_4240 =
																												(BgL_arg1615z00_1775 ==
																												BUNSPEC);
																										}
																										if (BgL_test2233z00_4240)
																											{
																												BgL_sfunzf2iinfozf2_bglt
																													BgL_auxz00_4250;
																												{
																													obj_t BgL_auxz00_4251;

																													{	/* Integrate/a.scm 153 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_4252;
																														BgL_tmpz00_4252 =
																															(
																															(BgL_objectz00_bglt)
																															((BgL_sfunz00_bglt) BgL_funz00_1744));
																														BgL_auxz00_4251 =
																															BGL_OBJECT_WIDENING
																															(BgL_tmpz00_4252);
																													}
																													BgL_auxz00_4250 =
																														(
																														(BgL_sfunzf2iinfozf2_bglt)
																														BgL_auxz00_4251);
																												}
																												((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4250))->BgL_tailzd2coercionzd2) = ((obj_t) BgL_typez00_1729), BUNSPEC);
																											}
																										else
																											{	/* Integrate/a.scm 154 */
																												bool_t
																													BgL_test2234z00_4258;
																												if ((BgL_typez00_1729 ==
																														BGl_za2pairzd2nilza2zd2zztype_cachez00))
																													{	/* Integrate/a.scm 154 */
																														BgL_test2234z00_4258
																															= ((bool_t) 1);
																													}
																												else
																													{	/* Integrate/a.scm 154 */
																														BgL_test2234z00_4258
																															=
																															(BgL_typez00_1729
																															==
																															BGl_za2magicza2z00zztype_cachez00);
																													}
																												if (BgL_test2234z00_4258)
																													{	/* Integrate/a.scm 154 */
																														BUNSPEC;
																													}
																												else
																													{	/* Integrate/a.scm 156 */
																														bool_t
																															BgL_test2236z00_4262;
																														{	/* Integrate/a.scm 156 */
																															bool_t
																																BgL_test2237z00_4263;
																															if (
																																(BgL_typez00_1729
																																	==
																																	BGl_za2pairzd2nilza2zd2zztype_cachez00))
																																{	/* Integrate/a.scm 157 */
																																	obj_t
																																		BgL_arg1613z00_1774;
																																	{
																																		BgL_sfunzf2iinfozf2_bglt
																																			BgL_auxz00_4266;
																																		{
																																			obj_t
																																				BgL_auxz00_4267;
																																			{	/* Integrate/a.scm 157 */
																																				BgL_objectz00_bglt
																																					BgL_tmpz00_4268;
																																				BgL_tmpz00_4268
																																					=
																																					(
																																					(BgL_objectz00_bglt)
																																					((BgL_sfunz00_bglt) BgL_funz00_1744));
																																				BgL_auxz00_4267
																																					=
																																					BGL_OBJECT_WIDENING
																																					(BgL_tmpz00_4268);
																																			}
																																			BgL_auxz00_4266
																																				=
																																				(
																																				(BgL_sfunzf2iinfozf2_bglt)
																																				BgL_auxz00_4267);
																																		}
																																		BgL_arg1613z00_1774
																																			=
																																			(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4266))->BgL_tailzd2coercionzd2);
																																	}
																																	BgL_test2237z00_4263
																																		=
																																		(BgL_arg1613z00_1774
																																		==
																																		BGl_za2pairza2z00zztype_cachez00);
																																}
																															else
																																{	/* Integrate/a.scm 156 */
																																	BgL_test2237z00_4263
																																		=
																																		((bool_t)
																																		0);
																																}
																															if (BgL_test2237z00_4263)
																																{	/* Integrate/a.scm 156 */
																																	BgL_test2236z00_4262
																																		=
																																		((bool_t)
																																		1);
																																}
																															else
																																{	/* Integrate/a.scm 156 */
																																	if (
																																		(BgL_typez00_1729
																																			==
																																			BGl_za2pairza2z00zztype_cachez00))
																																		{	/* Integrate/a.scm 159 */
																																			obj_t
																																				BgL_arg1611z00_1773;
																																			{
																																				BgL_sfunzf2iinfozf2_bglt
																																					BgL_auxz00_4277;
																																				{
																																					obj_t
																																						BgL_auxz00_4278;
																																					{	/* Integrate/a.scm 159 */
																																						BgL_objectz00_bglt
																																							BgL_tmpz00_4279;
																																						BgL_tmpz00_4279
																																							=
																																							(
																																							(BgL_objectz00_bglt)
																																							((BgL_sfunz00_bglt) BgL_funz00_1744));
																																						BgL_auxz00_4278
																																							=
																																							BGL_OBJECT_WIDENING
																																							(BgL_tmpz00_4279);
																																					}
																																					BgL_auxz00_4277
																																						=
																																						(
																																						(BgL_sfunzf2iinfozf2_bglt)
																																						BgL_auxz00_4278);
																																				}
																																				BgL_arg1611z00_1773
																																					=
																																					(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4277))->BgL_tailzd2coercionzd2);
																																			}
																																			BgL_test2236z00_4262
																																				=
																																				(BgL_arg1611z00_1773
																																				==
																																				BGl_za2pairzd2nilza2zd2zztype_cachez00);
																																		}
																																	else
																																		{	/* Integrate/a.scm 158 */
																																			BgL_test2236z00_4262
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																														if (BgL_test2236z00_4262)
																															{	/* Integrate/a.scm 156 */
																																BUNSPEC;
																															}
																														else
																															{	/* Integrate/a.scm 161 */
																																bool_t
																																	BgL_test2240z00_4286;
																																{	/* Integrate/a.scm 161 */
																																	obj_t
																																		BgL_arg1609z00_1770;
																																	{
																																		BgL_sfunzf2iinfozf2_bglt
																																			BgL_auxz00_4287;
																																		{
																																			obj_t
																																				BgL_auxz00_4288;
																																			{	/* Integrate/a.scm 161 */
																																				BgL_objectz00_bglt
																																					BgL_tmpz00_4289;
																																				BgL_tmpz00_4289
																																					=
																																					(
																																					(BgL_objectz00_bglt)
																																					((BgL_sfunz00_bglt) BgL_funz00_1744));
																																				BgL_auxz00_4288
																																					=
																																					BGL_OBJECT_WIDENING
																																					(BgL_tmpz00_4289);
																																			}
																																			BgL_auxz00_4287
																																				=
																																				(
																																				(BgL_sfunzf2iinfozf2_bglt)
																																				BgL_auxz00_4288);
																																		}
																																		BgL_arg1609z00_1770
																																			=
																																			(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4287))->BgL_tailzd2coercionzd2);
																																	}
																																	BgL_test2240z00_4286
																																		=
																																		BGl_tailzd2typezd2compatiblezf3zf3zzintegrate_az00
																																		(BgL_arg1609z00_1770,
																																		BgL_typez00_1729);
																																}
																																if (BgL_test2240z00_4286)
																																	{	/* Integrate/a.scm 161 */
																																		BFALSE;
																																	}
																																else
																																	{	/* Integrate/a.scm 161 */
																																		{	/* Integrate/a.scm 162 */
																																			bool_t
																																				BgL_test2241z00_4296;
																																			{	/* Integrate/a.scm 162 */
																																				obj_t
																																					BgL_classz00_2670;
																																				BgL_classz00_2670
																																					=
																																					BGl_localz00zzast_varz00;
																																				if (BGL_OBJECTP(BgL_calleez00_1728))
																																					{	/* Integrate/a.scm 162 */
																																						BgL_objectz00_bglt
																																							BgL_arg1807z00_2672;
																																						BgL_arg1807z00_2672
																																							=
																																							(BgL_objectz00_bglt)
																																							(BgL_calleez00_1728);
																																						if (BGL_CONDEXPAND_ISA_ARCH64())
																																							{	/* Integrate/a.scm 162 */
																																								long
																																									BgL_idxz00_2678;
																																								BgL_idxz00_2678
																																									=
																																									BGL_OBJECT_INHERITANCE_NUM
																																									(BgL_arg1807z00_2672);
																																								BgL_test2241z00_4296
																																									=
																																									(VECTOR_REF
																																									(BGl_za2inheritancesza2z00zz__objectz00,
																																										(BgL_idxz00_2678
																																											+
																																											2L))
																																									==
																																									BgL_classz00_2670);
																																							}
																																						else
																																							{	/* Integrate/a.scm 162 */
																																								bool_t
																																									BgL_res2109z00_2703;
																																								{	/* Integrate/a.scm 162 */
																																									obj_t
																																										BgL_oclassz00_2686;
																																									{	/* Integrate/a.scm 162 */
																																										obj_t
																																											BgL_arg1815z00_2694;
																																										long
																																											BgL_arg1816z00_2695;
																																										BgL_arg1815z00_2694
																																											=
																																											(BGl_za2classesza2z00zz__objectz00);
																																										{	/* Integrate/a.scm 162 */
																																											long
																																												BgL_arg1817z00_2696;
																																											BgL_arg1817z00_2696
																																												=
																																												BGL_OBJECT_CLASS_NUM
																																												(BgL_arg1807z00_2672);
																																											BgL_arg1816z00_2695
																																												=
																																												(BgL_arg1817z00_2696
																																												-
																																												OBJECT_TYPE);
																																										}
																																										BgL_oclassz00_2686
																																											=
																																											VECTOR_REF
																																											(BgL_arg1815z00_2694,
																																											BgL_arg1816z00_2695);
																																									}
																																									{	/* Integrate/a.scm 162 */
																																										bool_t
																																											BgL__ortest_1115z00_2687;
																																										BgL__ortest_1115z00_2687
																																											=
																																											(BgL_classz00_2670
																																											==
																																											BgL_oclassz00_2686);
																																										if (BgL__ortest_1115z00_2687)
																																											{	/* Integrate/a.scm 162 */
																																												BgL_res2109z00_2703
																																													=
																																													BgL__ortest_1115z00_2687;
																																											}
																																										else
																																											{	/* Integrate/a.scm 162 */
																																												long
																																													BgL_odepthz00_2688;
																																												{	/* Integrate/a.scm 162 */
																																													obj_t
																																														BgL_arg1804z00_2689;
																																													BgL_arg1804z00_2689
																																														=
																																														(BgL_oclassz00_2686);
																																													BgL_odepthz00_2688
																																														=
																																														BGL_CLASS_DEPTH
																																														(BgL_arg1804z00_2689);
																																												}
																																												if ((2L < BgL_odepthz00_2688))
																																													{	/* Integrate/a.scm 162 */
																																														obj_t
																																															BgL_arg1802z00_2691;
																																														{	/* Integrate/a.scm 162 */
																																															obj_t
																																																BgL_arg1803z00_2692;
																																															BgL_arg1803z00_2692
																																																=
																																																(BgL_oclassz00_2686);
																																															BgL_arg1802z00_2691
																																																=
																																																BGL_CLASS_ANCESTORS_REF
																																																(BgL_arg1803z00_2692,
																																																2L);
																																														}
																																														BgL_res2109z00_2703
																																															=
																																															(BgL_arg1802z00_2691
																																															==
																																															BgL_classz00_2670);
																																													}
																																												else
																																													{	/* Integrate/a.scm 162 */
																																														BgL_res2109z00_2703
																																															=
																																															(
																																															(bool_t)
																																															0);
																																													}
																																											}
																																									}
																																								}
																																								BgL_test2241z00_4296
																																									=
																																									BgL_res2109z00_2703;
																																							}
																																					}
																																				else
																																					{	/* Integrate/a.scm 162 */
																																						BgL_test2241z00_4296
																																							=
																																							(
																																							(bool_t)
																																							0);
																																					}
																																			}
																																			if (BgL_test2241z00_4296)
																																				{	/* Integrate/a.scm 164 */
																																					obj_t
																																						BgL_arg1589z00_1761;
																																					obj_t
																																						BgL_arg1591z00_1762;
																																					obj_t
																																						BgL_arg1593z00_1763;
																																					BgL_arg1589z00_1761
																																						=
																																						(((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) (((BgL_sfunz00_bglt) COBJECT(((BgL_sfunz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_1744))))->BgL_bodyz00))))->BgL_locz00);
																																					BgL_arg1591z00_1762
																																						=
																																						BGl_shapez00zztools_shapez00
																																						(BgL_calleez00_1728);
																																					{	/* Integrate/a.scm 168 */
																																						obj_t
																																							BgL_arg1595z00_1765;
																																						obj_t
																																							BgL_arg1602z00_1766;
																																						{	/* Integrate/a.scm 168 */
																																							obj_t
																																								BgL_arg1606z00_1769;
																																							{
																																								BgL_sfunzf2iinfozf2_bglt
																																									BgL_auxz00_4325;
																																								{
																																									obj_t
																																										BgL_auxz00_4326;
																																									{	/* Integrate/a.scm 168 */
																																										BgL_objectz00_bglt
																																											BgL_tmpz00_4327;
																																										BgL_tmpz00_4327
																																											=
																																											(
																																											(BgL_objectz00_bglt)
																																											((BgL_sfunz00_bglt) BgL_funz00_1744));
																																										BgL_auxz00_4326
																																											=
																																											BGL_OBJECT_WIDENING
																																											(BgL_tmpz00_4327);
																																									}
																																									BgL_auxz00_4325
																																										=
																																										(
																																										(BgL_sfunzf2iinfozf2_bglt)
																																										BgL_auxz00_4326);
																																								}
																																								BgL_arg1606z00_1769
																																									=
																																									(
																																									((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4325))->BgL_tailzd2coercionzd2);
																																							}
																																							BgL_arg1595z00_1765
																																								=
																																								BGl_shapez00zztools_shapez00
																																								(BgL_arg1606z00_1769);
																																						}
																																						BgL_arg1602z00_1766
																																							=
																																							BGl_shapez00zztools_shapez00
																																							(BgL_typez00_1729);
																																						{	/* Integrate/a.scm 168 */
																																							obj_t
																																								BgL_list1603z00_1767;
																																							{	/* Integrate/a.scm 168 */
																																								obj_t
																																									BgL_arg1605z00_1768;
																																								BgL_arg1605z00_1768
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1602z00_1766,
																																									BNIL);
																																								BgL_list1603z00_1767
																																									=
																																									MAKE_YOUNG_PAIR
																																									(BgL_arg1595z00_1765,
																																									BgL_arg1605z00_1768);
																																							}
																																							BgL_arg1593z00_1763
																																								=
																																								BGl_formatz00zz__r4_output_6_10_3z00
																																								(BGl_string2133z00zzintegrate_az00,
																																								BgL_list1603z00_1767);
																																						}
																																					}
																																					BGl_userzd2warningzf2locationz20zztools_errorz00
																																						(BgL_arg1589z00_1761,
																																						BgL_arg1591z00_1762,
																																						BGl_string2134z00zzintegrate_az00,
																																						BgL_arg1593z00_1763);
																																				}
																																			else
																																				{	/* Integrate/a.scm 162 */
																																					BFALSE;
																																				}
																																		}
																																		{
																																			BgL_sfunzf2iinfozf2_bglt
																																				BgL_auxz00_4339;
																																			{
																																				obj_t
																																					BgL_auxz00_4340;
																																				{	/* Integrate/a.scm 169 */
																																					BgL_objectz00_bglt
																																						BgL_tmpz00_4341;
																																					BgL_tmpz00_4341
																																						=
																																						(
																																						(BgL_objectz00_bglt)
																																						((BgL_sfunz00_bglt) BgL_funz00_1744));
																																					BgL_auxz00_4340
																																						=
																																						BGL_OBJECT_WIDENING
																																						(BgL_tmpz00_4341);
																																				}
																																				BgL_auxz00_4339
																																					=
																																					(
																																					(BgL_sfunzf2iinfozf2_bglt)
																																					BgL_auxz00_4340);
																																			}
																																			((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4339))->BgL_tailzd2coercionzd2) = ((obj_t) BFALSE), BUNSPEC);
																																		}
																																	}
																															}
																													}
																											}
																									}
																							}
																						else
																							{	/* Integrate/a.scm 148 */
																								BUNSPEC;
																							}
																					}
																				else
																					{	/* Integrate/a.scm 146 */
																						BUNSPEC;
																					}
																			}
																		}
																	}
																else
																	{	/* Integrate/a.scm 141 */
																		BFALSE;
																	}
															}
														else
															{	/* Integrate/a.scm 141 */
																BFALSE;
															}
													}
												else
													{	/* Integrate/a.scm 141 */
														BFALSE;
													}
											}
										else
											{	/* Integrate/a.scm 141 */
												BFALSE;
											}
									}
								else
									{	/* Integrate/a.scm 141 */
										BFALSE;
									}
							}
						}
						{
							obj_t BgL_l1318z00_4349;

							BgL_l1318z00_4349 = CDR(BgL_l1318z00_1724);
							BgL_l1318z00_1724 = BgL_l1318z00_4349;
							goto BgL_zc3z04anonymousza31537ze3z87_1725;
						}
					}
				else
					{	/* Integrate/a.scm 140 */
						((bool_t) 1);
					}
			}
			if (NULLP(BgL_az00_14))
				{	/* Integrate/a.scm 172 */
					return BNIL;
				}
			else
				{	/* Integrate/a.scm 172 */
					obj_t BgL_head1322z00_1780;

					BgL_head1322z00_1780 = MAKE_YOUNG_PAIR(BNIL, BNIL);
					{
						obj_t BgL_l1320z00_1782;
						obj_t BgL_tail1323z00_1783;

						BgL_l1320z00_1782 = BgL_az00_14;
						BgL_tail1323z00_1783 = BgL_head1322z00_1780;
					BgL_zc3z04anonymousza31618ze3z87_1784:
						if (NULLP(BgL_l1320z00_1782))
							{	/* Integrate/a.scm 172 */
								return CDR(BgL_head1322z00_1780);
							}
						else
							{	/* Integrate/a.scm 172 */
								obj_t BgL_newtail1324z00_1786;

								{	/* Integrate/a.scm 172 */
									obj_t BgL_arg1626z00_1788;

									{	/* Integrate/a.scm 172 */
										obj_t BgL_az00_1789;

										BgL_az00_1789 = CAR(((obj_t) BgL_l1320z00_1782));
										{
											obj_t BgL_callerz00_1790;
											obj_t BgL_calleez00_1791;
											obj_t BgL_typez00_1792;

											if (PAIRP(BgL_az00_1789))
												{	/* Integrate/a.scm 173 */
													obj_t BgL_cdrzd2391zd2_1802;

													BgL_cdrzd2391zd2_1802 = CDR(((obj_t) BgL_az00_1789));
													if (PAIRP(BgL_cdrzd2391zd2_1802))
														{	/* Integrate/a.scm 173 */
															obj_t BgL_cdrzd2396zd2_1804;

															BgL_cdrzd2396zd2_1804 =
																CDR(BgL_cdrzd2391zd2_1802);
															if (PAIRP(BgL_cdrzd2396zd2_1804))
																{	/* Integrate/a.scm 173 */
																	obj_t BgL_carzd2399zd2_1806;

																	BgL_carzd2399zd2_1806 =
																		CAR(BgL_cdrzd2396zd2_1804);
																	if (PAIRP(BgL_carzd2399zd2_1806))
																		{	/* Integrate/a.scm 173 */
																			if (
																				(CAR(BgL_carzd2399zd2_1806) ==
																					CNST_TABLE_REF(0)))
																				{	/* Integrate/a.scm 173 */
																					if (NULLP(CDR(BgL_cdrzd2396zd2_1804)))
																						{	/* Integrate/a.scm 173 */
																							obj_t BgL_arg1650z00_1812;
																							obj_t BgL_arg1651z00_1813;
																							obj_t BgL_arg1654z00_1814;

																							BgL_arg1650z00_1812 =
																								CAR(((obj_t) BgL_az00_1789));
																							BgL_arg1651z00_1813 =
																								CAR(BgL_cdrzd2391zd2_1802);
																							BgL_arg1654z00_1814 =
																								CDR(BgL_carzd2399zd2_1806);
																							BgL_callerz00_1790 =
																								BgL_arg1650z00_1812;
																							BgL_calleez00_1791 =
																								BgL_arg1651z00_1813;
																							BgL_typez00_1792 =
																								BgL_arg1654z00_1814;
																							{	/* Integrate/a.scm 175 */
																								BgL_valuez00_bglt
																									BgL_funz00_1827;
																								BgL_funz00_1827 =
																									(((BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt)
																												BgL_calleez00_1791)))->
																									BgL_valuez00);
																								{	/* Integrate/a.scm 179 */
																									obj_t BgL_arg1692z00_1829;

																									{	/* Integrate/a.scm 179 */
																										bool_t BgL_test2254z00_4384;

																										{	/* Integrate/a.scm 179 */
																											obj_t BgL_tmpz00_4385;

																											{
																												BgL_sfunzf2iinfozf2_bglt
																													BgL_auxz00_4386;
																												{
																													obj_t BgL_auxz00_4387;

																													{	/* Integrate/a.scm 179 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_4388;
																														BgL_tmpz00_4388 =
																															(
																															(BgL_objectz00_bglt)
																															((BgL_sfunz00_bglt) BgL_funz00_1827));
																														BgL_auxz00_4387 =
																															BGL_OBJECT_WIDENING
																															(BgL_tmpz00_4388);
																													}
																													BgL_auxz00_4386 =
																														(
																														(BgL_sfunzf2iinfozf2_bglt)
																														BgL_auxz00_4387);
																												}
																												BgL_tmpz00_4385 =
																													(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4386))->BgL_tailzd2coercionzd2);
																											}
																											BgL_test2254z00_4384 =
																												CBOOL(BgL_tmpz00_4385);
																										}
																										if (BgL_test2254z00_4384)
																											{	/* Integrate/a.scm 179 */
																												BgL_arg1692z00_1829 =
																													CNST_TABLE_REF(0);
																											}
																										else
																											{	/* Integrate/a.scm 179 */
																												BgL_arg1692z00_1829 =
																													BGl_getzd2newzd2kontz00zzintegrate_az00
																													();
																											}
																									}
																									{	/* Integrate/a.scm 177 */
																										obj_t BgL_list1693z00_1830;

																										{	/* Integrate/a.scm 177 */
																											obj_t BgL_arg1699z00_1831;

																											{	/* Integrate/a.scm 177 */
																												obj_t
																													BgL_arg1700z00_1832;
																												BgL_arg1700z00_1832 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1692z00_1829,
																													BNIL);
																												BgL_arg1699z00_1831 =
																													MAKE_YOUNG_PAIR
																													(BgL_calleez00_1791,
																													BgL_arg1700z00_1832);
																											}
																											BgL_list1693z00_1830 =
																												MAKE_YOUNG_PAIR
																												(BgL_callerz00_1790,
																												BgL_arg1699z00_1831);
																										}
																										BgL_arg1626z00_1788 =
																											BgL_list1693z00_1830;
																									}
																								}
																							}
																						}
																					else
																						{	/* Integrate/a.scm 173 */
																							BgL_arg1626z00_1788 =
																								BgL_az00_1789;
																						}
																				}
																			else
																				{	/* Integrate/a.scm 173 */
																					obj_t BgL_cdrzd2435zd2_1817;

																					BgL_cdrzd2435zd2_1817 =
																						CDR(
																						((obj_t) BgL_cdrzd2391zd2_1802));
																					{	/* Integrate/a.scm 173 */
																						obj_t BgL_carzd2440zd2_1818;

																						BgL_carzd2440zd2_1818 =
																							CAR(
																							((obj_t) BgL_cdrzd2435zd2_1817));
																						if (NULLP(CDR(
																									((obj_t)
																										BgL_cdrzd2435zd2_1817))))
																							{	/* Integrate/a.scm 173 */
																								obj_t BgL_arg1675z00_1821;
																								obj_t BgL_arg1678z00_1822;
																								obj_t BgL_arg1681z00_1823;

																								BgL_arg1675z00_1821 =
																									CAR(((obj_t) BgL_az00_1789));
																								BgL_arg1678z00_1822 =
																									CAR(
																									((obj_t)
																										BgL_cdrzd2391zd2_1802));
																								BgL_arg1681z00_1823 =
																									CAR(((obj_t)
																										BgL_carzd2440zd2_1818));
																								{	/* Integrate/a.scm 181 */
																									obj_t BgL_list1702z00_2739;

																									{	/* Integrate/a.scm 181 */
																										obj_t BgL_arg1703z00_2740;

																										{	/* Integrate/a.scm 181 */
																											obj_t BgL_arg1705z00_2741;

																											BgL_arg1705z00_2741 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1681z00_1823,
																												BNIL);
																											BgL_arg1703z00_2740 =
																												MAKE_YOUNG_PAIR
																												(BgL_arg1678z00_1822,
																												BgL_arg1705z00_2741);
																										}
																										BgL_list1702z00_2739 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1675z00_1821,
																											BgL_arg1703z00_2740);
																									}
																									BgL_arg1626z00_1788 =
																										BgL_list1702z00_2739;
																								}
																							}
																						else
																							{	/* Integrate/a.scm 173 */
																								BgL_arg1626z00_1788 =
																									BgL_az00_1789;
																							}
																					}
																				}
																		}
																	else
																		{	/* Integrate/a.scm 173 */
																			BgL_arg1626z00_1788 = BgL_az00_1789;
																		}
																}
															else
																{	/* Integrate/a.scm 173 */
																	BgL_arg1626z00_1788 = BgL_az00_1789;
																}
														}
													else
														{	/* Integrate/a.scm 173 */
															BgL_arg1626z00_1788 = BgL_az00_1789;
														}
												}
											else
												{	/* Integrate/a.scm 173 */
													BgL_arg1626z00_1788 = BgL_az00_1789;
												}
										}
									}
									BgL_newtail1324z00_1786 =
										MAKE_YOUNG_PAIR(BgL_arg1626z00_1788, BNIL);
								}
								SET_CDR(BgL_tail1323z00_1783, BgL_newtail1324z00_1786);
								{	/* Integrate/a.scm 172 */
									obj_t BgL_arg1625z00_1787;

									BgL_arg1625z00_1787 = CDR(((obj_t) BgL_l1320z00_1782));
									{
										obj_t BgL_tail1323z00_4422;
										obj_t BgL_l1320z00_4421;

										BgL_l1320z00_4421 = BgL_arg1625z00_1787;
										BgL_tail1323z00_4422 = BgL_newtail1324z00_1786;
										BgL_tail1323z00_1783 = BgL_tail1323z00_4422;
										BgL_l1320z00_1782 = BgL_l1320z00_4421;
										goto BgL_zc3z04anonymousza31618ze3z87_1784;
									}
								}
							}
					}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzintegrate_az00(void)
	{
		{	/* Integrate/a.scm 18 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzintegrate_az00(void)
	{
		{	/* Integrate/a.scm 18 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_proc2135z00zzintegrate_az00,
				BGl_nodez00zzast_nodez00, BGl_string2136z00zzintegrate_az00);
		}

	}



/* &node-A1328 */
	obj_t BGl_z62nodezd2A1328zb0zzintegrate_az00(obj_t BgL_envz00_3260,
		obj_t BgL_nodez00_3261, obj_t BgL_hostz00_3262, obj_t BgL_kz00_3263,
		obj_t BgL_az00_3264)
	{
		{	/* Integrate/a.scm 189 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(1),
				BGl_string2137z00zzintegrate_az00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_3261)));
		}

	}



/* node-A */
	obj_t BGl_nodezd2Azd2zzintegrate_az00(BgL_nodez00_bglt BgL_nodez00_16,
		BgL_variablez00_bglt BgL_hostz00_17, obj_t BgL_kz00_18, obj_t BgL_az00_19)
	{
		{	/* Integrate/a.scm 189 */
			{	/* Integrate/a.scm 189 */
				obj_t BgL_method1330z00_1845;

				{	/* Integrate/a.scm 189 */
					obj_t BgL_res2117z00_2775;

					{	/* Integrate/a.scm 189 */
						long BgL_objzd2classzd2numz00_2746;

						BgL_objzd2classzd2numz00_2746 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_16));
						{	/* Integrate/a.scm 189 */
							obj_t BgL_arg1811z00_2747;

							BgL_arg1811z00_2747 =
								PROCEDURE_REF(BGl_nodezd2Azd2envz00zzintegrate_az00,
								(int) (1L));
							{	/* Integrate/a.scm 189 */
								int BgL_offsetz00_2750;

								BgL_offsetz00_2750 = (int) (BgL_objzd2classzd2numz00_2746);
								{	/* Integrate/a.scm 189 */
									long BgL_offsetz00_2751;

									BgL_offsetz00_2751 =
										((long) (BgL_offsetz00_2750) - OBJECT_TYPE);
									{	/* Integrate/a.scm 189 */
										long BgL_modz00_2752;

										BgL_modz00_2752 =
											(BgL_offsetz00_2751 >> (int) ((long) ((int) (4L))));
										{	/* Integrate/a.scm 189 */
											long BgL_restz00_2754;

											BgL_restz00_2754 =
												(BgL_offsetz00_2751 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Integrate/a.scm 189 */

												{	/* Integrate/a.scm 189 */
													obj_t BgL_bucketz00_2756;

													BgL_bucketz00_2756 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2747), BgL_modz00_2752);
													BgL_res2117z00_2775 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2756), BgL_restz00_2754);
					}}}}}}}}
					BgL_method1330z00_1845 = BgL_res2117z00_2775;
				}
				return
					BGL_PROCEDURE_CALL4(BgL_method1330z00_1845,
					((obj_t) BgL_nodez00_16),
					((obj_t) BgL_hostz00_17), BgL_kz00_18, BgL_az00_19);
			}
		}

	}



/* &node-A */
	obj_t BGl_z62nodezd2Azb0zzintegrate_az00(obj_t BgL_envz00_3265,
		obj_t BgL_nodez00_3266, obj_t BgL_hostz00_3267, obj_t BgL_kz00_3268,
		obj_t BgL_az00_3269)
	{
		{	/* Integrate/a.scm 189 */
			return
				BGl_nodezd2Azd2zzintegrate_az00(
				((BgL_nodez00_bglt) BgL_nodez00_3266),
				((BgL_variablez00_bglt) BgL_hostz00_3267), BgL_kz00_3268,
				BgL_az00_3269);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzintegrate_az00(void)
	{
		{	/* Integrate/a.scm 18 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_atomz00zzast_nodez00,
				BGl_proc2138z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_kwotez00zzast_nodez00,
				BGl_proc2140z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_varz00zzast_nodez00,
				BGl_proc2141z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_closurez00zzast_nodez00,
				BGl_proc2142z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_sequencez00zzast_nodez00,
				BGl_proc2143z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_syncz00zzast_nodez00,
				BGl_proc2144z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_appz00zzast_nodez00,
				BGl_proc2145z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc2146z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_funcallz00zzast_nodez00,
				BGl_proc2147z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_externz00zzast_nodez00,
				BGl_proc2148z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_castz00zzast_nodez00,
				BGl_proc2149z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_setqz00zzast_nodez00,
				BGl_proc2150z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_conditionalz00zzast_nodez00,
				BGl_proc2151z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_failz00zzast_nodez00,
				BGl_proc2152z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_switchz00zzast_nodez00,
				BGl_proc2153z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc2154z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc2155z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2156z00zzintegrate_az00,
				BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2157z00zzintegrate_az00,
				BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc2158z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2159z00zzintegrate_az00,
				BGl_string2139z00zzintegrate_az00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2Azd2envz00zzintegrate_az00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc2160z00zzintegrate_az00, BGl_string2139z00zzintegrate_az00);
		}

	}



/* &node-A-box-ref1377 */
	obj_t BGl_z62nodezd2Azd2boxzd2ref1377zb0zzintegrate_az00(obj_t
		BgL_envz00_3292, obj_t BgL_nodez00_3293, obj_t BgL_hostz00_3294,
		obj_t BgL_kz00_3295, obj_t BgL_az00_3296)
	{
		{	/* Integrate/a.scm 508 */
			return BgL_az00_3296;
		}

	}



/* &node-A-box-set!1374 */
	obj_t BGl_z62nodezd2Azd2boxzd2setz121374za2zzintegrate_az00(obj_t
		BgL_envz00_3297, obj_t BgL_nodez00_3298, obj_t BgL_hostz00_3299,
		obj_t BgL_kz00_3300, obj_t BgL_az00_3301)
	{
		{	/* Integrate/a.scm 500 */
			{	/* Integrate/a.scm 502 */
				BgL_nodez00_bglt BgL_arg2033z00_3430;
				obj_t BgL_arg2034z00_3431;

				BgL_arg2033z00_3430 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3298)))->BgL_valuez00);
				{	/* Integrate/a.scm 503 */
					obj_t BgL_arg2036z00_3432;
					obj_t BgL_arg2037z00_3433;

					BgL_arg2036z00_3432 = BGl_getzd2newzd2kontz00zzintegrate_az00();
					{	/* Integrate/a.scm 503 */
						BgL_nodez00_bglt BgL_arg2038z00_3434;

						BgL_arg2038z00_3434 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3298)))->
							BgL_valuez00);
						BgL_arg2037z00_3433 =
							BGl_normaliza7ezd2typez75zzintegrate_az00
							(BGl_getzd2typezd2zztype_typeofz00(BgL_arg2038z00_3434,
								((bool_t) 0)));
					}
					BgL_arg2034z00_3431 =
						MAKE_YOUNG_PAIR(BgL_arg2036z00_3432, BgL_arg2037z00_3433);
				}
				return
					BGl_nodezd2Azd2zzintegrate_az00(BgL_arg2033z00_3430,
					((BgL_variablez00_bglt) BgL_hostz00_3299), BgL_arg2034z00_3431,
					BgL_az00_3301);
			}
		}

	}



/* &node-A-make-box1371 */
	obj_t BGl_z62nodezd2Azd2makezd2box1371zb0zzintegrate_az00(obj_t
		BgL_envz00_3302, obj_t BgL_nodez00_3303, obj_t BgL_hostz00_3304,
		obj_t BgL_kz00_3305, obj_t BgL_az00_3306)
	{
		{	/* Integrate/a.scm 492 */
			{	/* Integrate/a.scm 494 */
				BgL_nodez00_bglt BgL_arg2026z00_3436;
				obj_t BgL_arg2027z00_3437;

				BgL_arg2026z00_3436 =
					(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_3303)))->BgL_valuez00);
				{	/* Integrate/a.scm 495 */
					obj_t BgL_arg2029z00_3438;
					obj_t BgL_arg2030z00_3439;

					BgL_arg2029z00_3438 = BGl_getzd2newzd2kontz00zzintegrate_az00();
					{	/* Integrate/a.scm 495 */
						BgL_nodez00_bglt BgL_arg2031z00_3440;

						BgL_arg2031z00_3440 =
							(((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_3303)))->BgL_valuez00);
						BgL_arg2030z00_3439 =
							BGl_normaliza7ezd2typez75zzintegrate_az00
							(BGl_getzd2typezd2zztype_typeofz00(BgL_arg2031z00_3440,
								((bool_t) 0)));
					}
					BgL_arg2027z00_3437 =
						MAKE_YOUNG_PAIR(BgL_arg2029z00_3438, BgL_arg2030z00_3439);
				}
				return
					BGl_nodezd2Azd2zzintegrate_az00(BgL_arg2026z00_3436,
					((BgL_variablez00_bglt) BgL_hostz00_3304), BgL_arg2027z00_3437,
					BgL_az00_3306);
			}
		}

	}



/* &node-A-jump-ex-it1368 */
	obj_t BGl_z62nodezd2Azd2jumpzd2exzd2it1368z62zzintegrate_az00(obj_t
		BgL_envz00_3307, obj_t BgL_nodez00_3308, obj_t BgL_hostz00_3309,
		obj_t BgL_kz00_3310, obj_t BgL_az00_3311)
	{
		{	/* Integrate/a.scm 481 */
			{	/* Integrate/a.scm 483 */
				BgL_nodez00_bglt BgL_arg2014z00_3442;
				obj_t BgL_arg2015z00_3443;
				obj_t BgL_arg2016z00_3444;

				BgL_arg2014z00_3442 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3308)))->BgL_exitz00);
				{	/* Integrate/a.scm 485 */
					obj_t BgL_arg2017z00_3445;
					obj_t BgL_arg2018z00_3446;

					BgL_arg2017z00_3445 = BGl_getzd2newzd2kontz00zzintegrate_az00();
					{	/* Integrate/a.scm 485 */
						BgL_nodez00_bglt BgL_arg2019z00_3447;

						BgL_arg2019z00_3447 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3308)))->
							BgL_exitz00);
						BgL_arg2018z00_3446 =
							BGl_normaliza7ezd2typez75zzintegrate_az00
							(BGl_getzd2typezd2zztype_typeofz00(BgL_arg2019z00_3447,
								((bool_t) 0)));
					}
					BgL_arg2015z00_3443 =
						MAKE_YOUNG_PAIR(BgL_arg2017z00_3445, BgL_arg2018z00_3446);
				}
				{	/* Integrate/a.scm 486 */
					BgL_nodez00_bglt BgL_arg2020z00_3448;
					obj_t BgL_arg2021z00_3449;

					BgL_arg2020z00_3448 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3308)))->
						BgL_valuez00);
					{	/* Integrate/a.scm 487 */
						obj_t BgL_arg2022z00_3450;
						obj_t BgL_arg2024z00_3451;

						BgL_arg2022z00_3450 = BGl_getzd2newzd2kontz00zzintegrate_az00();
						{	/* Integrate/a.scm 487 */
							BgL_nodez00_bglt BgL_arg2025z00_3452;

							BgL_arg2025z00_3452 =
								(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
										((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3308)))->
								BgL_valuez00);
							BgL_arg2024z00_3451 =
								BGl_normaliza7ezd2typez75zzintegrate_az00
								(BGl_getzd2typezd2zztype_typeofz00(BgL_arg2025z00_3452,
									((bool_t) 0)));
						}
						BgL_arg2021z00_3449 =
							MAKE_YOUNG_PAIR(BgL_arg2022z00_3450, BgL_arg2024z00_3451);
					}
					BgL_arg2016z00_3444 =
						BGl_nodezd2Azd2zzintegrate_az00(BgL_arg2020z00_3448,
						((BgL_variablez00_bglt) BgL_hostz00_3309), BgL_arg2021z00_3449,
						BgL_az00_3311);
				}
				return
					BGl_nodezd2Azd2zzintegrate_az00(BgL_arg2014z00_3442,
					((BgL_variablez00_bglt) BgL_hostz00_3309), BgL_arg2015z00_3443,
					BgL_arg2016z00_3444);
			}
		}

	}



/* &node-A-set-ex-it1366 */
	obj_t BGl_z62nodezd2Azd2setzd2exzd2it1366z62zzintegrate_az00(obj_t
		BgL_envz00_3312, obj_t BgL_nodez00_3313, obj_t BgL_hostz00_3314,
		obj_t BgL_kz00_3315, obj_t BgL_az00_3316)
	{
		{	/* Integrate/a.scm 466 */
			{	/* Integrate/a.scm 468 */
				BgL_variablez00_bglt BgL_exitz00_3454;

				BgL_exitz00_3454 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
										((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3313)))->
								BgL_varz00)))->BgL_variablez00);
				{	/* Integrate/a.scm 468 */
					obj_t BgL_hdlgz00_3455;

					BgL_hdlgz00_3455 =
						(((BgL_sexitz00_bglt) COBJECT(
								((BgL_sexitz00_bglt)
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_exitz00_3454))))->
										BgL_valuez00))))->BgL_handlerz00);
					{	/* Integrate/a.scm 469 */

						{	/* Integrate/a.scm 470 */
							BgL_sexitz00_bglt BgL_tmp1176z00_3456;

							BgL_tmp1176z00_3456 =
								((BgL_sexitz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_localz00_bglt) BgL_exitz00_3454))))->
									BgL_valuez00));
							{	/* Integrate/a.scm 470 */
								BgL_sexitzf2iinfozf2_bglt BgL_wide1178z00_3457;

								BgL_wide1178z00_3457 =
									((BgL_sexitzf2iinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_sexitzf2iinfozf2_bgl))));
								{	/* Integrate/a.scm 470 */
									obj_t BgL_auxz00_4543;
									BgL_objectz00_bglt BgL_tmpz00_4540;

									BgL_auxz00_4543 = ((obj_t) BgL_wide1178z00_3457);
									BgL_tmpz00_4540 =
										((BgL_objectz00_bglt)
										((BgL_sexitz00_bglt) BgL_tmp1176z00_3456));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4540, BgL_auxz00_4543);
								}
								((BgL_objectz00_bglt)
									((BgL_sexitz00_bglt) BgL_tmp1176z00_3456));
								{	/* Integrate/a.scm 470 */
									long BgL_arg2003z00_3458;

									BgL_arg2003z00_3458 =
										BGL_CLASS_NUM(BGl_sexitzf2Iinfozf2zzintegrate_infoz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_sexitz00_bglt) BgL_tmp1176z00_3456)),
										BgL_arg2003z00_3458);
								}
								((BgL_sexitz00_bglt) ((BgL_sexitz00_bglt) BgL_tmp1176z00_3456));
							}
							{
								BgL_sexitzf2iinfozf2_bglt BgL_auxz00_4554;

								{
									obj_t BgL_auxz00_4555;

									{	/* Integrate/a.scm 470 */
										BgL_objectz00_bglt BgL_tmpz00_4556;

										BgL_tmpz00_4556 =
											((BgL_objectz00_bglt)
											((BgL_sexitz00_bglt) BgL_tmp1176z00_3456));
										BgL_auxz00_4555 = BGL_OBJECT_WIDENING(BgL_tmpz00_4556);
									}
									BgL_auxz00_4554 =
										((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_4555);
								}
								((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4554))->
										BgL_fzd2markzd2) = ((obj_t) BUNSPEC), BUNSPEC);
							}
							{
								BgL_sexitzf2iinfozf2_bglt BgL_auxz00_4562;

								{
									obj_t BgL_auxz00_4563;

									{	/* Integrate/a.scm 470 */
										BgL_objectz00_bglt BgL_tmpz00_4564;

										BgL_tmpz00_4564 =
											((BgL_objectz00_bglt)
											((BgL_sexitz00_bglt) BgL_tmp1176z00_3456));
										BgL_auxz00_4563 = BGL_OBJECT_WIDENING(BgL_tmpz00_4564);
									}
									BgL_auxz00_4562 =
										((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_4563);
								}
								((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4562))->
										BgL_uzd2markzd2) = ((obj_t) BUNSPEC), BUNSPEC);
							}
							{
								BgL_sexitzf2iinfozf2_bglt BgL_auxz00_4570;

								{
									obj_t BgL_auxz00_4571;

									{	/* Integrate/a.scm 470 */
										BgL_objectz00_bglt BgL_tmpz00_4572;

										BgL_tmpz00_4572 =
											((BgL_objectz00_bglt)
											((BgL_sexitz00_bglt) BgL_tmp1176z00_3456));
										BgL_auxz00_4571 = BGL_OBJECT_WIDENING(BgL_tmpz00_4572);
									}
									BgL_auxz00_4570 =
										((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_4571);
								}
								((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4570))->
										BgL_kapturedzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
							}
							{
								BgL_sexitzf2iinfozf2_bglt BgL_auxz00_4578;

								{
									obj_t BgL_auxz00_4579;

									{	/* Integrate/a.scm 470 */
										BgL_objectz00_bglt BgL_tmpz00_4580;

										BgL_tmpz00_4580 =
											((BgL_objectz00_bglt)
											((BgL_sexitz00_bglt) BgL_tmp1176z00_3456));
										BgL_auxz00_4579 = BGL_OBJECT_WIDENING(BgL_tmpz00_4580);
									}
									BgL_auxz00_4578 =
										((BgL_sexitzf2iinfozf2_bglt) BgL_auxz00_4579);
								}
								((((BgL_sexitzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4578))->
										BgL_celledzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
							}
							((BgL_sexitz00_bglt) BgL_tmp1176z00_3456);
						}
						{	/* Integrate/a.scm 471 */
							bool_t BgL_test2256z00_4587;

							if (CBOOL(BGl_za2localzd2exitzf3za2z21zzengine_paramz00))
								{	/* Integrate/a.scm 471 */
									BgL_test2256z00_4587 = ((bool_t) 0);
								}
							else
								{	/* Integrate/a.scm 471 */
									if (CBOOL
										(BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00))
										{	/* Integrate/a.scm 472 */
											BgL_test2256z00_4587 = ((bool_t) 0);
										}
									else
										{	/* Integrate/a.scm 472 */
											if (
												(((BgL_sexitz00_bglt) COBJECT(
															((BgL_sexitz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					BgL_exitz00_3454))))->
																	BgL_valuez00))))->BgL_detachedzf3zf3))
												{	/* Integrate/a.scm 473 */
													BgL_test2256z00_4587 = ((bool_t) 0);
												}
											else
												{	/* Integrate/a.scm 473 */
													BgL_test2256z00_4587 = ((bool_t) 1);
												}
										}
								}
							if (BgL_test2256z00_4587)
								{	/* Integrate/a.scm 474 */
									BgL_sfunz00_bglt BgL_i1180z00_3459;

									BgL_i1180z00_3459 =
										((BgL_sfunz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_hdlgz00_3455))))->
											BgL_valuez00));
									{
										BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4602;

										{
											obj_t BgL_auxz00_4603;

											{	/* Integrate/a.scm 475 */
												BgL_objectz00_bglt BgL_tmpz00_4604;

												BgL_tmpz00_4604 =
													((BgL_objectz00_bglt) BgL_i1180z00_3459);
												BgL_auxz00_4603 = BGL_OBJECT_WIDENING(BgL_tmpz00_4604);
											}
											BgL_auxz00_4602 =
												((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4603);
										}
										((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4602))->
												BgL_forcegzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
									}
								}
							else
								{	/* Integrate/a.scm 471 */
									BFALSE;
								}
						}
					}
				}
			}
			{	/* Integrate/a.scm 476 */
				BgL_nodez00_bglt BgL_arg2011z00_3460;
				obj_t BgL_arg2012z00_3461;

				BgL_arg2011z00_3460 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3313)))->BgL_bodyz00);
				{	/* Integrate/a.scm 476 */
					BgL_nodez00_bglt BgL_arg2013z00_3462;

					BgL_arg2013z00_3462 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3313)))->
						BgL_onexitz00);
					BgL_arg2012z00_3461 =
						BGl_nodezd2Azd2zzintegrate_az00(BgL_arg2013z00_3462,
						((BgL_variablez00_bglt) BgL_hostz00_3314), BgL_kz00_3315,
						BgL_az00_3316);
				}
				return
					BGl_nodezd2Azd2zzintegrate_az00(BgL_arg2011z00_3460,
					((BgL_variablez00_bglt) BgL_hostz00_3314), BgL_kz00_3315,
					BgL_arg2012z00_3461);
			}
		}

	}



/* &node-A-let-var1364 */
	obj_t BGl_z62nodezd2Azd2letzd2var1364zb0zzintegrate_az00(obj_t
		BgL_envz00_3317, obj_t BgL_nodez00_3318, obj_t BgL_hostz00_3319,
		obj_t BgL_kz00_3320, obj_t BgL_az00_3321)
	{
		{	/* Integrate/a.scm 433 */
			{
				BgL_variablez00_bglt BgL_varz00_3483;
				obj_t BgL_nodez00_3466;

				{	/* Integrate/a.scm 448 */
					obj_t BgL_g1166z00_3485;

					BgL_g1166z00_3485 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_3318)))->BgL_bindingsz00);
					{
						obj_t BgL_bindingsz00_3487;
						obj_t BgL_az00_3488;

						BgL_bindingsz00_3487 = BgL_g1166z00_3485;
						BgL_az00_3488 = BgL_az00_3321;
					BgL_liipz00_3486:
						if (NULLP(BgL_bindingsz00_3487))
							{	/* Integrate/a.scm 451 */
								BgL_nodez00_bglt BgL_arg1988z00_3489;

								BgL_arg1988z00_3489 =
									(((BgL_letzd2varzd2_bglt) COBJECT(
											((BgL_letzd2varzd2_bglt) BgL_nodez00_3318)))->
									BgL_bodyz00);
								return BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1988z00_3489,
									((BgL_variablez00_bglt) BgL_hostz00_3319), BgL_kz00_3320,
									BgL_az00_3488);
							}
						else
							{	/* Integrate/a.scm 452 */
								obj_t BgL_bindingz00_3490;

								BgL_bindingz00_3490 = CAR(((obj_t) BgL_bindingsz00_3487));
								{	/* Integrate/a.scm 452 */
									obj_t BgL_varz00_3491;

									BgL_varz00_3491 = CAR(((obj_t) BgL_bindingz00_3490));
									{	/* Integrate/a.scm 453 */
										obj_t BgL_valz00_3492;

										BgL_valz00_3492 = CDR(((obj_t) BgL_bindingz00_3490));
										{	/* Integrate/a.scm 454 */

											{	/* Integrate/a.scm 455 */
												bool_t BgL_test2261z00_4631;

												BgL_nodez00_3466 = BgL_valz00_3492;
												if (CBOOL
													(BGl_za2localzd2exitzf3za2z21zzengine_paramz00))
													{	/* Integrate/a.scm 440 */
														BgL_test2261z00_4631 = ((bool_t) 0);
													}
												else
													{	/* Integrate/a.scm 440 */
														if (CBOOL
															(BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00))
															{	/* Integrate/a.scm 442 */
																bool_t BgL_test2264z00_4636;

																{	/* Integrate/a.scm 442 */
																	obj_t BgL_classz00_3467;

																	BgL_classz00_3467 = BGl_appz00zzast_nodez00;
																	if (BGL_OBJECTP(BgL_nodez00_3466))
																		{	/* Integrate/a.scm 442 */
																			BgL_objectz00_bglt BgL_arg1807z00_3468;

																			BgL_arg1807z00_3468 =
																				(BgL_objectz00_bglt) (BgL_nodez00_3466);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Integrate/a.scm 442 */
																					long BgL_idxz00_3469;

																					BgL_idxz00_3469 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_3468);
																					BgL_test2264z00_4636 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_3469 + 3L)) ==
																						BgL_classz00_3467);
																				}
																			else
																				{	/* Integrate/a.scm 442 */
																					bool_t BgL_res2124z00_3472;

																					{	/* Integrate/a.scm 442 */
																						obj_t BgL_oclassz00_3473;

																						{	/* Integrate/a.scm 442 */
																							obj_t BgL_arg1815z00_3474;
																							long BgL_arg1816z00_3475;

																							BgL_arg1815z00_3474 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Integrate/a.scm 442 */
																								long BgL_arg1817z00_3476;

																								BgL_arg1817z00_3476 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_3468);
																								BgL_arg1816z00_3475 =
																									(BgL_arg1817z00_3476 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_3473 =
																								VECTOR_REF(BgL_arg1815z00_3474,
																								BgL_arg1816z00_3475);
																						}
																						{	/* Integrate/a.scm 442 */
																							bool_t BgL__ortest_1115z00_3477;

																							BgL__ortest_1115z00_3477 =
																								(BgL_classz00_3467 ==
																								BgL_oclassz00_3473);
																							if (BgL__ortest_1115z00_3477)
																								{	/* Integrate/a.scm 442 */
																									BgL_res2124z00_3472 =
																										BgL__ortest_1115z00_3477;
																								}
																							else
																								{	/* Integrate/a.scm 442 */
																									long BgL_odepthz00_3478;

																									{	/* Integrate/a.scm 442 */
																										obj_t BgL_arg1804z00_3479;

																										BgL_arg1804z00_3479 =
																											(BgL_oclassz00_3473);
																										BgL_odepthz00_3478 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_3479);
																									}
																									if ((3L < BgL_odepthz00_3478))
																										{	/* Integrate/a.scm 442 */
																											obj_t BgL_arg1802z00_3480;

																											{	/* Integrate/a.scm 442 */
																												obj_t
																													BgL_arg1803z00_3481;
																												BgL_arg1803z00_3481 =
																													(BgL_oclassz00_3473);
																												BgL_arg1802z00_3480 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_3481,
																													3L);
																											}
																											BgL_res2124z00_3472 =
																												(BgL_arg1802z00_3480 ==
																												BgL_classz00_3467);
																										}
																									else
																										{	/* Integrate/a.scm 442 */
																											BgL_res2124z00_3472 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2264z00_4636 =
																						BgL_res2124z00_3472;
																				}
																		}
																	else
																		{	/* Integrate/a.scm 442 */
																			BgL_test2264z00_4636 = ((bool_t) 0);
																		}
																}
																if (BgL_test2264z00_4636)
																	{	/* Integrate/a.scm 444 */
																		BgL_varz00_bglt BgL_i1162z00_3482;

																		BgL_i1162z00_3482 =
																			(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						BgL_nodez00_3466)))->BgL_funz00);
																		BgL_varz00_3483 =
																			(((BgL_varz00_bglt)
																				COBJECT(BgL_i1162z00_3482))->
																			BgL_variablez00);
																		{	/* Integrate/a.scm 436 */
																			bool_t BgL__ortest_1160z00_3484;

																			BgL__ortest_1160z00_3484 =
																				(
																				(((BgL_variablez00_bglt)
																						COBJECT(BgL_varz00_3483))->
																					BgL_idz00) == CNST_TABLE_REF(2));
																			if (BgL__ortest_1160z00_3484)
																				{	/* Integrate/a.scm 436 */
																					BgL_test2261z00_4631 =
																						BgL__ortest_1160z00_3484;
																				}
																			else
																				{	/* Integrate/a.scm 436 */
																					BgL_test2261z00_4631 =
																						(
																						(((BgL_variablez00_bglt)
																								COBJECT(BgL_varz00_3483))->
																							BgL_idz00) == CNST_TABLE_REF(3));
																				}
																		}
																	}
																else
																	{	/* Integrate/a.scm 442 */
																		BgL_test2261z00_4631 = ((bool_t) 0);
																	}
															}
														else
															{	/* Integrate/a.scm 441 */
																BgL_test2261z00_4631 = ((bool_t) 0);
															}
													}
												if (BgL_test2261z00_4631)
													{	/* Integrate/a.scm 456 */
														BgL_svarz00_bglt BgL_tmp1167z00_3493;

														BgL_tmp1167z00_3493 =
															((BgL_svarz00_bglt)
															(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt) BgL_varz00_3491))))->
																BgL_valuez00));
														{	/* Integrate/a.scm 456 */
															BgL_svarzf2iinfozf2_bglt BgL_wide1169z00_3494;

															BgL_wide1169z00_3494 =
																((BgL_svarzf2iinfozf2_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_svarzf2iinfozf2_bgl))));
															{	/* Integrate/a.scm 456 */
																obj_t BgL_auxz00_4677;
																BgL_objectz00_bglt BgL_tmpz00_4674;

																BgL_auxz00_4677 =
																	((obj_t) BgL_wide1169z00_3494);
																BgL_tmpz00_4674 =
																	((BgL_objectz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1167z00_3493));
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4674,
																	BgL_auxz00_4677);
															}
															((BgL_objectz00_bglt)
																((BgL_svarz00_bglt) BgL_tmp1167z00_3493));
															{	/* Integrate/a.scm 456 */
																long BgL_arg1990z00_3495;

																BgL_arg1990z00_3495 =
																	BGL_CLASS_NUM
																	(BGl_svarzf2Iinfozf2zzintegrate_infoz00);
																BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																			(BgL_svarz00_bglt) BgL_tmp1167z00_3493)),
																	BgL_arg1990z00_3495);
															}
															((BgL_svarz00_bglt)
																((BgL_svarz00_bglt) BgL_tmp1167z00_3493));
														}
														{
															BgL_svarzf2iinfozf2_bglt BgL_auxz00_4688;

															{
																obj_t BgL_auxz00_4689;

																{	/* Integrate/a.scm 457 */
																	BgL_objectz00_bglt BgL_tmpz00_4690;

																	BgL_tmpz00_4690 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1167z00_3493));
																	BgL_auxz00_4689 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_4690);
																}
																BgL_auxz00_4688 =
																	((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_4689);
															}
															((((BgL_svarzf2iinfozf2_bglt)
																		COBJECT(BgL_auxz00_4688))->
																	BgL_fzd2markzd2) =
																((obj_t) BUNSPEC), BUNSPEC);
														}
														{
															BgL_svarzf2iinfozf2_bglt BgL_auxz00_4696;

															{
																obj_t BgL_auxz00_4697;

																{	/* Integrate/a.scm 457 */
																	BgL_objectz00_bglt BgL_tmpz00_4698;

																	BgL_tmpz00_4698 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1167z00_3493));
																	BgL_auxz00_4697 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_4698);
																}
																BgL_auxz00_4696 =
																	((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_4697);
															}
															((((BgL_svarzf2iinfozf2_bglt)
																		COBJECT(BgL_auxz00_4696))->
																	BgL_uzd2markzd2) =
																((obj_t) BUNSPEC), BUNSPEC);
														}
														{
															BgL_svarzf2iinfozf2_bglt BgL_auxz00_4704;

															{
																obj_t BgL_auxz00_4705;

																{	/* Integrate/a.scm 457 */
																	BgL_objectz00_bglt BgL_tmpz00_4706;

																	BgL_tmpz00_4706 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1167z00_3493));
																	BgL_auxz00_4705 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_4706);
																}
																BgL_auxz00_4704 =
																	((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_4705);
															}
															((((BgL_svarzf2iinfozf2_bglt)
																		COBJECT(BgL_auxz00_4704))->
																	BgL_kapturedzf3zf3) =
																((bool_t) ((bool_t) 0)), BUNSPEC);
														}
														{
															BgL_svarzf2iinfozf2_bglt BgL_auxz00_4712;

															{
																obj_t BgL_auxz00_4713;

																{	/* Integrate/a.scm 457 */
																	BgL_objectz00_bglt BgL_tmpz00_4714;

																	BgL_tmpz00_4714 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1167z00_3493));
																	BgL_auxz00_4713 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_4714);
																}
																BgL_auxz00_4712 =
																	((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_4713);
															}
															((((BgL_svarzf2iinfozf2_bglt)
																		COBJECT(BgL_auxz00_4712))->
																	BgL_celledzf3zf3) =
																((bool_t) ((bool_t) 0)), BUNSPEC);
														}
														{
															BgL_svarzf2iinfozf2_bglt BgL_auxz00_4720;

															{
																obj_t BgL_auxz00_4721;

																{	/* Integrate/a.scm 457 */
																	BgL_objectz00_bglt BgL_tmpz00_4722;

																	BgL_tmpz00_4722 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1167z00_3493));
																	BgL_auxz00_4721 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_4722);
																}
																BgL_auxz00_4720 =
																	((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_4721);
															}
															((((BgL_svarzf2iinfozf2_bglt)
																		COBJECT(BgL_auxz00_4720))->BgL_xhdlz00) =
																((obj_t) BgL_hostz00_3319), BUNSPEC);
														}
														((BgL_svarz00_bglt) BgL_tmp1167z00_3493);
													}
												else
													{	/* Integrate/a.scm 458 */
														BgL_svarz00_bglt BgL_tmp1171z00_3496;

														BgL_tmp1171z00_3496 =
															((BgL_svarz00_bglt)
															(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt) BgL_varz00_3491))))->
																BgL_valuez00));
														{	/* Integrate/a.scm 458 */
															BgL_svarzf2iinfozf2_bglt BgL_wide1173z00_3497;

															BgL_wide1173z00_3497 =
																((BgL_svarzf2iinfozf2_bglt)
																BOBJECT(GC_MALLOC(sizeof(struct
																			BgL_svarzf2iinfozf2_bgl))));
															{	/* Integrate/a.scm 458 */
																obj_t BgL_auxz00_4737;
																BgL_objectz00_bglt BgL_tmpz00_4734;

																BgL_auxz00_4737 =
																	((obj_t) BgL_wide1173z00_3497);
																BgL_tmpz00_4734 =
																	((BgL_objectz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1171z00_3496));
																BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4734,
																	BgL_auxz00_4737);
															}
															((BgL_objectz00_bglt)
																((BgL_svarz00_bglt) BgL_tmp1171z00_3496));
															{	/* Integrate/a.scm 458 */
																long BgL_arg1991z00_3498;

																BgL_arg1991z00_3498 =
																	BGL_CLASS_NUM
																	(BGl_svarzf2Iinfozf2zzintegrate_infoz00);
																BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																			(BgL_svarz00_bglt) BgL_tmp1171z00_3496)),
																	BgL_arg1991z00_3498);
															}
															((BgL_svarz00_bglt)
																((BgL_svarz00_bglt) BgL_tmp1171z00_3496));
														}
														{
															BgL_svarzf2iinfozf2_bglt BgL_auxz00_4748;

															{
																obj_t BgL_auxz00_4749;

																{	/* Integrate/a.scm 458 */
																	BgL_objectz00_bglt BgL_tmpz00_4750;

																	BgL_tmpz00_4750 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1171z00_3496));
																	BgL_auxz00_4749 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_4750);
																}
																BgL_auxz00_4748 =
																	((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_4749);
															}
															((((BgL_svarzf2iinfozf2_bglt)
																		COBJECT(BgL_auxz00_4748))->
																	BgL_fzd2markzd2) =
																((obj_t) BUNSPEC), BUNSPEC);
														}
														{
															BgL_svarzf2iinfozf2_bglt BgL_auxz00_4756;

															{
																obj_t BgL_auxz00_4757;

																{	/* Integrate/a.scm 458 */
																	BgL_objectz00_bglt BgL_tmpz00_4758;

																	BgL_tmpz00_4758 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1171z00_3496));
																	BgL_auxz00_4757 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_4758);
																}
																BgL_auxz00_4756 =
																	((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_4757);
															}
															((((BgL_svarzf2iinfozf2_bglt)
																		COBJECT(BgL_auxz00_4756))->
																	BgL_uzd2markzd2) =
																((obj_t) BUNSPEC), BUNSPEC);
														}
														{
															BgL_svarzf2iinfozf2_bglt BgL_auxz00_4764;

															{
																obj_t BgL_auxz00_4765;

																{	/* Integrate/a.scm 458 */
																	BgL_objectz00_bglt BgL_tmpz00_4766;

																	BgL_tmpz00_4766 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1171z00_3496));
																	BgL_auxz00_4765 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_4766);
																}
																BgL_auxz00_4764 =
																	((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_4765);
															}
															((((BgL_svarzf2iinfozf2_bglt)
																		COBJECT(BgL_auxz00_4764))->
																	BgL_kapturedzf3zf3) =
																((bool_t) ((bool_t) 0)), BUNSPEC);
														}
														{
															BgL_svarzf2iinfozf2_bglt BgL_auxz00_4772;

															{
																obj_t BgL_auxz00_4773;

																{	/* Integrate/a.scm 458 */
																	BgL_objectz00_bglt BgL_tmpz00_4774;

																	BgL_tmpz00_4774 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1171z00_3496));
																	BgL_auxz00_4773 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_4774);
																}
																BgL_auxz00_4772 =
																	((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_4773);
															}
															((((BgL_svarzf2iinfozf2_bglt)
																		COBJECT(BgL_auxz00_4772))->
																	BgL_celledzf3zf3) =
																((bool_t) ((bool_t) 0)), BUNSPEC);
														}
														{
															BgL_svarzf2iinfozf2_bglt BgL_auxz00_4780;

															{
																obj_t BgL_auxz00_4781;

																{	/* Integrate/a.scm 458 */
																	BgL_objectz00_bglt BgL_tmpz00_4782;

																	BgL_tmpz00_4782 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1171z00_3496));
																	BgL_auxz00_4781 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_4782);
																}
																BgL_auxz00_4780 =
																	((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_4781);
															}
															((((BgL_svarzf2iinfozf2_bglt)
																		COBJECT(BgL_auxz00_4780))->BgL_xhdlz00) =
																((obj_t) BFALSE), BUNSPEC);
														}
														((BgL_svarz00_bglt) BgL_tmp1171z00_3496);
											}}
											{	/* Integrate/a.scm 459 */
												obj_t BgL_arg1992z00_3499;
												obj_t BgL_arg1993z00_3500;

												BgL_arg1992z00_3499 =
													CDR(((obj_t) BgL_bindingsz00_3487));
												{	/* Integrate/a.scm 461 */
													obj_t BgL_arg1994z00_3501;

													{	/* Integrate/a.scm 461 */
														obj_t BgL_arg1995z00_3502;
														BgL_typez00_bglt BgL_arg1996z00_3503;

														BgL_arg1995z00_3502 =
															BGl_getzd2newzd2kontz00zzintegrate_az00();
														BgL_arg1996z00_3503 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_varz00_3491))))->
															BgL_typez00);
														BgL_arg1994z00_3501 =
															MAKE_YOUNG_PAIR(BgL_arg1995z00_3502,
															((obj_t) BgL_arg1996z00_3503));
													}
													BgL_arg1993z00_3500 =
														BGl_nodezd2Azd2zzintegrate_az00(
														((BgL_nodez00_bglt) BgL_valz00_3492),
														((BgL_variablez00_bglt) BgL_hostz00_3319),
														BgL_arg1994z00_3501, BgL_az00_3488);
												}
												{
													obj_t BgL_az00_4801;
													obj_t BgL_bindingsz00_4800;

													BgL_bindingsz00_4800 = BgL_arg1992z00_3499;
													BgL_az00_4801 = BgL_arg1993z00_3500;
													BgL_az00_3488 = BgL_az00_4801;
													BgL_bindingsz00_3487 = BgL_bindingsz00_4800;
													goto BgL_liipz00_3486;
												}
											}
										}
									}
								}
							}
					}
				}
			}
		}

	}



/* &node-A-let-fun1362 */
	obj_t BGl_z62nodezd2Azd2letzd2fun1362zb0zzintegrate_az00(obj_t
		BgL_envz00_3322, obj_t BgL_nodez00_3323, obj_t BgL_hostz00_3324,
		obj_t BgL_kz00_3325, obj_t BgL_az00_3326)
	{
		{	/* Integrate/a.scm 386 */
			{
				BgL_letzd2funzd2_bglt BgL_nodez00_3511;
				BgL_letzd2funzd2_bglt BgL_nodez00_3507;

				{	/* Integrate/a.scm 412 */
					obj_t BgL_g1327z00_3546;

					BgL_g1327z00_3546 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_3323)))->BgL_localsz00);
					{
						obj_t BgL_l1325z00_3548;

						BgL_l1325z00_3548 = BgL_g1327z00_3546;
					BgL_zc3z04anonymousza31945ze3z87_3547:
						if (PAIRP(BgL_l1325z00_3548))
							{	/* Integrate/a.scm 415 */
								{	/* Integrate/a.scm 413 */
									obj_t BgL_fz00_3549;

									BgL_fz00_3549 = CAR(BgL_l1325z00_3548);
									BGl_initializa7ezd2funz12z67zzintegrate_az00(
										((BgL_variablez00_bglt) BgL_fz00_3549),
										((BgL_variablez00_bglt) BgL_hostz00_3324));
									BGl_za2phiza2z00zzintegrate_az00 =
										MAKE_YOUNG_PAIR(BgL_fz00_3549,
										BGl_za2phiza2z00zzintegrate_az00);
								}
								{
									obj_t BgL_l1325z00_4811;

									BgL_l1325z00_4811 = CDR(BgL_l1325z00_3548);
									BgL_l1325z00_3548 = BgL_l1325z00_4811;
									goto BgL_zc3z04anonymousza31945ze3z87_3547;
								}
							}
						else
							{	/* Integrate/a.scm 415 */
								((bool_t) 1);
							}
					}
				}
				{	/* Integrate/a.scm 417 */
					obj_t BgL_g1159z00_3550;

					BgL_g1159z00_3550 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_3323)))->BgL_localsz00);
					{
						obj_t BgL_localsz00_3552;
						obj_t BgL_az00_3553;

						BgL_localsz00_3552 = BgL_g1159z00_3550;
						BgL_az00_3553 = BgL_az00_3326;
					BgL_liipz00_3551:
						if (NULLP(BgL_localsz00_3552))
							{	/* Integrate/a.scm 419 */
								if (CBOOL(BGl_za2localzd2exitzf3za2z21zzengine_paramz00))
									{	/* Integrate/a.scm 421 */
										BFALSE;
									}
								else
									{	/* Integrate/a.scm 422 */
										bool_t BgL_test2273z00_4819;

										BgL_nodez00_3511 =
											((BgL_letzd2funzd2_bglt) BgL_nodez00_3323);
										{	/* Integrate/a.scm 390 */
											bool_t BgL_test2274z00_4820;

											{	/* Integrate/a.scm 390 */
												bool_t BgL_test2275z00_4821;

												{	/* Integrate/a.scm 390 */
													obj_t BgL_tmpz00_4822;

													BgL_tmpz00_4822 =
														(((BgL_letzd2funzd2_bglt)
															COBJECT(BgL_nodez00_3511))->BgL_localsz00);
													BgL_test2275z00_4821 = PAIRP(BgL_tmpz00_4822);
												}
												if (BgL_test2275z00_4821)
													{	/* Integrate/a.scm 390 */
														BgL_test2274z00_4820 =
															NULLP(CDR(
																(((BgL_letzd2funzd2_bglt)
																		COBJECT(BgL_nodez00_3511))->
																	BgL_localsz00)));
													}
												else
													{	/* Integrate/a.scm 390 */
														BgL_test2274z00_4820 = ((bool_t) 0);
													}
											}
											if (BgL_test2274z00_4820)
												{	/* Integrate/a.scm 391 */
													obj_t BgL_varz00_3512;

													BgL_varz00_3512 =
														CAR(
														(((BgL_letzd2funzd2_bglt)
																COBJECT(BgL_nodez00_3511))->BgL_localsz00));
													{	/* Integrate/a.scm 391 */
														BgL_valuez00_bglt BgL_funz00_3513;

														BgL_funz00_3513 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt) BgL_varz00_3512))))->
															BgL_valuez00);
														{	/* Integrate/a.scm 392 */

															{	/* Integrate/a.scm 393 */
																bool_t BgL_test2276z00_4833;

																{	/* Integrate/a.scm 393 */
																	obj_t BgL_arg1975z00_3514;

																	BgL_arg1975z00_3514 =
																		(((BgL_sfunz00_bglt) COBJECT(
																				((BgL_sfunz00_bglt) BgL_funz00_3513)))->
																		BgL_bodyz00);
																	{	/* Integrate/a.scm 393 */
																		obj_t BgL_classz00_3515;

																		BgL_classz00_3515 =
																			BGl_setzd2exzd2itz00zzast_nodez00;
																		if (BGL_OBJECTP(BgL_arg1975z00_3514))
																			{	/* Integrate/a.scm 393 */
																				BgL_objectz00_bglt BgL_arg1807z00_3516;

																				BgL_arg1807z00_3516 =
																					(BgL_objectz00_bglt)
																					(BgL_arg1975z00_3514);
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Integrate/a.scm 393 */
																						long BgL_idxz00_3517;

																						BgL_idxz00_3517 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_3516);
																						BgL_test2276z00_4833 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_3517 + 2L)) ==
																							BgL_classz00_3515);
																					}
																				else
																					{	/* Integrate/a.scm 393 */
																						bool_t BgL_res2122z00_3520;

																						{	/* Integrate/a.scm 393 */
																							obj_t BgL_oclassz00_3521;

																							{	/* Integrate/a.scm 393 */
																								obj_t BgL_arg1815z00_3522;
																								long BgL_arg1816z00_3523;

																								BgL_arg1815z00_3522 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Integrate/a.scm 393 */
																									long BgL_arg1817z00_3524;

																									BgL_arg1817z00_3524 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_3516);
																									BgL_arg1816z00_3523 =
																										(BgL_arg1817z00_3524 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_3521 =
																									VECTOR_REF
																									(BgL_arg1815z00_3522,
																									BgL_arg1816z00_3523);
																							}
																							{	/* Integrate/a.scm 393 */
																								bool_t BgL__ortest_1115z00_3525;

																								BgL__ortest_1115z00_3525 =
																									(BgL_classz00_3515 ==
																									BgL_oclassz00_3521);
																								if (BgL__ortest_1115z00_3525)
																									{	/* Integrate/a.scm 393 */
																										BgL_res2122z00_3520 =
																											BgL__ortest_1115z00_3525;
																									}
																								else
																									{	/* Integrate/a.scm 393 */
																										long BgL_odepthz00_3526;

																										{	/* Integrate/a.scm 393 */
																											obj_t BgL_arg1804z00_3527;

																											BgL_arg1804z00_3527 =
																												(BgL_oclassz00_3521);
																											BgL_odepthz00_3526 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_3527);
																										}
																										if (
																											(2L < BgL_odepthz00_3526))
																											{	/* Integrate/a.scm 393 */
																												obj_t
																													BgL_arg1802z00_3528;
																												{	/* Integrate/a.scm 393 */
																													obj_t
																														BgL_arg1803z00_3529;
																													BgL_arg1803z00_3529 =
																														(BgL_oclassz00_3521);
																													BgL_arg1802z00_3528 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_3529,
																														2L);
																												}
																												BgL_res2122z00_3520 =
																													(BgL_arg1802z00_3528
																													== BgL_classz00_3515);
																											}
																										else
																											{	/* Integrate/a.scm 393 */
																												BgL_res2122z00_3520 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test2276z00_4833 =
																							BgL_res2122z00_3520;
																					}
																			}
																		else
																			{	/* Integrate/a.scm 393 */
																				BgL_test2276z00_4833 = ((bool_t) 0);
																			}
																	}
																}
																if (BgL_test2276z00_4833)
																	{	/* Integrate/a.scm 394 */
																		bool_t BgL_test2281z00_4858;

																		{	/* Integrate/a.scm 394 */
																			BgL_nodez00_bglt BgL_arg1974z00_3530;

																			BgL_arg1974z00_3530 =
																				(((BgL_letzd2funzd2_bglt)
																					COBJECT(BgL_nodez00_3511))->
																				BgL_bodyz00);
																			{	/* Integrate/a.scm 394 */
																				obj_t BgL_classz00_3531;

																				BgL_classz00_3531 =
																					BGl_appz00zzast_nodez00;
																				{	/* Integrate/a.scm 394 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_3532;
																					{	/* Integrate/a.scm 394 */
																						obj_t BgL_tmpz00_4860;

																						BgL_tmpz00_4860 =
																							((obj_t)
																							((BgL_objectz00_bglt)
																								BgL_arg1974z00_3530));
																						BgL_arg1807z00_3532 =
																							(BgL_objectz00_bglt)
																							(BgL_tmpz00_4860);
																					}
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Integrate/a.scm 394 */
																							long BgL_idxz00_3533;

																							BgL_idxz00_3533 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_3532);
																							BgL_test2281z00_4858 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_3533 + 3L)) ==
																								BgL_classz00_3531);
																						}
																					else
																						{	/* Integrate/a.scm 394 */
																							bool_t BgL_res2123z00_3536;

																							{	/* Integrate/a.scm 394 */
																								obj_t BgL_oclassz00_3537;

																								{	/* Integrate/a.scm 394 */
																									obj_t BgL_arg1815z00_3538;
																									long BgL_arg1816z00_3539;

																									BgL_arg1815z00_3538 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Integrate/a.scm 394 */
																										long BgL_arg1817z00_3540;

																										BgL_arg1817z00_3540 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_3532);
																										BgL_arg1816z00_3539 =
																											(BgL_arg1817z00_3540 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_3537 =
																										VECTOR_REF
																										(BgL_arg1815z00_3538,
																										BgL_arg1816z00_3539);
																								}
																								{	/* Integrate/a.scm 394 */
																									bool_t
																										BgL__ortest_1115z00_3541;
																									BgL__ortest_1115z00_3541 =
																										(BgL_classz00_3531 ==
																										BgL_oclassz00_3537);
																									if (BgL__ortest_1115z00_3541)
																										{	/* Integrate/a.scm 394 */
																											BgL_res2123z00_3536 =
																												BgL__ortest_1115z00_3541;
																										}
																									else
																										{	/* Integrate/a.scm 394 */
																											long BgL_odepthz00_3542;

																											{	/* Integrate/a.scm 394 */
																												obj_t
																													BgL_arg1804z00_3543;
																												BgL_arg1804z00_3543 =
																													(BgL_oclassz00_3537);
																												BgL_odepthz00_3542 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_3543);
																											}
																											if (
																												(3L <
																													BgL_odepthz00_3542))
																												{	/* Integrate/a.scm 394 */
																													obj_t
																														BgL_arg1802z00_3544;
																													{	/* Integrate/a.scm 394 */
																														obj_t
																															BgL_arg1803z00_3545;
																														BgL_arg1803z00_3545
																															=
																															(BgL_oclassz00_3537);
																														BgL_arg1802z00_3544
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_3545,
																															3L);
																													}
																													BgL_res2123z00_3536 =
																														(BgL_arg1802z00_3544
																														==
																														BgL_classz00_3531);
																												}
																											else
																												{	/* Integrate/a.scm 394 */
																													BgL_res2123z00_3536 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test2281z00_4858 =
																								BgL_res2123z00_3536;
																						}
																				}
																			}
																		}
																		if (BgL_test2281z00_4858)
																			{	/* Integrate/a.scm 394 */
																				BgL_test2273z00_4819 =
																					(
																					((obj_t)
																						(((BgL_varz00_bglt) COBJECT(
																									(((BgL_appz00_bglt) COBJECT(
																												((BgL_appz00_bglt)
																													(((BgL_letzd2funzd2_bglt) COBJECT(BgL_nodez00_3511))->BgL_bodyz00))))->BgL_funz00)))->BgL_variablez00)) == BgL_varz00_3512);
																			}
																		else
																			{	/* Integrate/a.scm 394 */
																				BgL_test2273z00_4819 = ((bool_t) 0);
																			}
																	}
																else
																	{	/* Integrate/a.scm 393 */
																		BgL_test2273z00_4819 = ((bool_t) 0);
																	}
															}
														}
													}
												}
											else
												{	/* Integrate/a.scm 390 */
													BgL_test2273z00_4819 = ((bool_t) 0);
												}
										}
										if (BgL_test2273z00_4819)
											{	/* Integrate/a.scm 422 */
												BgL_nodez00_3507 =
													((BgL_letzd2funzd2_bglt) BgL_nodez00_3323);
												{	/* Integrate/a.scm 401 */
													BgL_sfunz00_bglt BgL_i1155z00_3508;

													{
														BgL_valuez00_bglt BgL_auxz00_4890;

														{
															BgL_variablez00_bglt BgL_auxz00_4891;

															{
																BgL_localz00_bglt BgL_auxz00_4892;

																{	/* Integrate/a.scm 401 */
																	obj_t BgL_pairz00_3509;

																	BgL_pairz00_3509 =
																		(((BgL_letzd2funzd2_bglt)
																			COBJECT(BgL_nodez00_3507))->
																		BgL_localsz00);
																	BgL_auxz00_4892 =
																		((BgL_localz00_bglt) CAR(BgL_pairz00_3509));
																}
																BgL_auxz00_4891 =
																	((BgL_variablez00_bglt) BgL_auxz00_4892);
															}
															BgL_auxz00_4890 =
																(((BgL_variablez00_bglt)
																	COBJECT(BgL_auxz00_4891))->BgL_valuez00);
														}
														BgL_i1155z00_3508 =
															((BgL_sfunz00_bglt) BgL_auxz00_4890);
													}
													{
														bool_t BgL_auxz00_4905;
														BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4899;

														{	/* Integrate/a.scm 405 */
															bool_t BgL__ortest_1156z00_3510;

															if (CBOOL
																(BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00))
																{	/* Integrate/a.scm 405 */
																	BgL__ortest_1156z00_3510 = ((bool_t) 0);
																}
															else
																{	/* Integrate/a.scm 405 */
																	BgL__ortest_1156z00_3510 = ((bool_t) 1);
																}
															if (BgL__ortest_1156z00_3510)
																{	/* Integrate/a.scm 405 */
																	BgL_auxz00_4905 = BgL__ortest_1156z00_3510;
																}
															else
																{	/* Integrate/a.scm 405 */
																	if (
																		(CAR(
																				((obj_t) BgL_kz00_3325)) ==
																			CNST_TABLE_REF(0)))
																		{	/* Integrate/a.scm 405 */
																			BgL_auxz00_4905 = ((bool_t) 0);
																		}
																	else
																		{	/* Integrate/a.scm 405 */
																			BgL_auxz00_4905 = ((bool_t) 1);
																		}
																}
														}
														{
															obj_t BgL_auxz00_4900;

															{	/* Integrate/a.scm 404 */
																BgL_objectz00_bglt BgL_tmpz00_4901;

																BgL_tmpz00_4901 =
																	((BgL_objectz00_bglt) BgL_i1155z00_3508);
																BgL_auxz00_4900 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_4901);
															}
															BgL_auxz00_4899 =
																((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4900);
														}
														((((BgL_sfunzf2iinfozf2_bglt)
																	COBJECT(BgL_auxz00_4899))->BgL_forcegzf3zf3) =
															((bool_t) BgL_auxz00_4905), BUNSPEC);
													}
													{
														bool_t BgL_auxz00_4921;
														BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4915;

														if (CBOOL
															(BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00))
															{	/* Integrate/a.scm 407 */
																BgL_auxz00_4921 = ((bool_t) 0);
															}
														else
															{	/* Integrate/a.scm 407 */
																BgL_auxz00_4921 = ((bool_t) 1);
															}
														{
															obj_t BgL_auxz00_4916;

															{	/* Integrate/a.scm 406 */
																BgL_objectz00_bglt BgL_tmpz00_4917;

																BgL_tmpz00_4917 =
																	((BgL_objectz00_bglt) BgL_i1155z00_3508);
																BgL_auxz00_4916 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_4917);
															}
															BgL_auxz00_4915 =
																((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4916);
														}
														((((BgL_sfunzf2iinfozf2_bglt)
																	COBJECT(BgL_auxz00_4915))->BgL_forcegzf3zf3) =
															((bool_t) BgL_auxz00_4921), BUNSPEC);
													}
													{
														BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4925;

														{
															obj_t BgL_auxz00_4926;

															{	/* Integrate/a.scm 408 */
																BgL_objectz00_bglt BgL_tmpz00_4927;

																BgL_tmpz00_4927 =
																	((BgL_objectz00_bglt) BgL_i1155z00_3508);
																BgL_auxz00_4926 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_4927);
															}
															BgL_auxz00_4925 =
																((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4926);
														}
														((((BgL_sfunzf2iinfozf2_bglt)
																	COBJECT(BgL_auxz00_4925))->BgL_xhdlzf3zf3) =
															((bool_t) ((bool_t) 1)), BUNSPEC);
													}
												}
											}
										else
											{	/* Integrate/a.scm 422 */
												BFALSE;
											}
									}
								{	/* Integrate/a.scm 423 */
									BgL_nodez00_bglt BgL_arg1951z00_3554;

									BgL_arg1951z00_3554 =
										(((BgL_letzd2funzd2_bglt) COBJECT(
												((BgL_letzd2funzd2_bglt) BgL_nodez00_3323)))->
										BgL_bodyz00);
									return BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1951z00_3554,
										((BgL_variablez00_bglt) BgL_hostz00_3324), BgL_kz00_3325,
										BgL_az00_3553);
								}
							}
						else
							{	/* Integrate/a.scm 424 */
								obj_t BgL_arg1952z00_3555;
								obj_t BgL_arg1953z00_3556;

								BgL_arg1952z00_3555 = CDR(((obj_t) BgL_localsz00_3552));
								{	/* Integrate/a.scm 425 */
									obj_t BgL_arg1954z00_3557;
									obj_t BgL_arg1955z00_3558;
									obj_t BgL_arg1956z00_3559;

									BgL_arg1954z00_3557 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt)
																		CAR(
																			((obj_t) BgL_localsz00_3552))))))->
														BgL_valuez00))))->BgL_bodyz00);
									BgL_arg1955z00_3558 = CAR(((obj_t) BgL_localsz00_3552));
									{	/* Integrate/a.scm 427 */
										obj_t BgL_arg1959z00_3560;

										{	/* Integrate/a.scm 427 */
											BgL_typez00_bglt BgL_arg1960z00_3561;

											BgL_arg1960z00_3561 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt)
																CAR(
																	((obj_t) BgL_localsz00_3552))))))->
												BgL_typez00);
											BgL_arg1959z00_3560 =
												BGl_normaliza7ezd2typez75zzintegrate_az00
												(BgL_arg1960z00_3561);
										}
										BgL_arg1956z00_3559 =
											MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1959z00_3560);
									}
									BgL_arg1953z00_3556 =
										BGl_nodezd2Azd2zzintegrate_az00(
										((BgL_nodez00_bglt) BgL_arg1954z00_3557),
										((BgL_variablez00_bglt) BgL_arg1955z00_3558),
										BgL_arg1956z00_3559, BgL_az00_3553);
								}
								{
									obj_t BgL_az00_4960;
									obj_t BgL_localsz00_4959;

									BgL_localsz00_4959 = BgL_arg1952z00_3555;
									BgL_az00_4960 = BgL_arg1953z00_3556;
									BgL_az00_3553 = BgL_az00_4960;
									BgL_localsz00_3552 = BgL_localsz00_4959;
									goto BgL_liipz00_3551;
								}
							}
					}
				}
			}
		}

	}



/* &node-A-switch1360 */
	obj_t BGl_z62nodezd2Azd2switch1360z62zzintegrate_az00(obj_t BgL_envz00_3327,
		obj_t BgL_nodez00_3328, obj_t BgL_hostz00_3329, obj_t BgL_kz00_3330,
		obj_t BgL_az00_3331)
	{
		{	/* Integrate/a.scm 373 */
			{	/* Integrate/a.scm 375 */
				obj_t BgL_g1149z00_3563;
				obj_t BgL_g1150z00_3564;

				BgL_g1149z00_3563 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_3328)))->BgL_clausesz00);
				{	/* Integrate/a.scm 376 */
					BgL_nodez00_bglt BgL_arg1941z00_3565;
					obj_t BgL_arg1942z00_3566;

					BgL_arg1941z00_3565 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_3328)))->BgL_testz00);
					{	/* Integrate/a.scm 377 */
						obj_t BgL_arg1943z00_3567;
						BgL_typez00_bglt BgL_arg1944z00_3568;

						BgL_arg1943z00_3567 = BGl_getzd2newzd2kontz00zzintegrate_az00();
						BgL_arg1944z00_3568 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_3328)))->
							BgL_itemzd2typezd2);
						BgL_arg1942z00_3566 =
							MAKE_YOUNG_PAIR(BgL_arg1943z00_3567,
							((obj_t) BgL_arg1944z00_3568));
					}
					BgL_g1150z00_3564 =
						BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1941z00_3565,
						((BgL_variablez00_bglt) BgL_hostz00_3329), BgL_arg1942z00_3566,
						BgL_az00_3331);
				}
				{
					obj_t BgL_clausesz00_3570;
					obj_t BgL_az00_3571;

					BgL_clausesz00_3570 = BgL_g1149z00_3563;
					BgL_az00_3571 = BgL_g1150z00_3564;
				BgL_liipz00_3569:
					if (NULLP(BgL_clausesz00_3570))
						{	/* Integrate/a.scm 378 */
							return BgL_az00_3571;
						}
					else
						{	/* Integrate/a.scm 380 */
							obj_t BgL_arg1937z00_3572;
							obj_t BgL_arg1938z00_3573;

							BgL_arg1937z00_3572 = CDR(((obj_t) BgL_clausesz00_3570));
							{	/* Integrate/a.scm 381 */
								obj_t BgL_arg1939z00_3574;

								{	/* Integrate/a.scm 381 */
									obj_t BgL_pairz00_3575;

									BgL_pairz00_3575 = CAR(((obj_t) BgL_clausesz00_3570));
									BgL_arg1939z00_3574 = CDR(BgL_pairz00_3575);
								}
								BgL_arg1938z00_3573 =
									BGl_nodezd2Azd2zzintegrate_az00(
									((BgL_nodez00_bglt) BgL_arg1939z00_3574),
									((BgL_variablez00_bglt) BgL_hostz00_3329), BgL_kz00_3330,
									BgL_az00_3571);
							}
							{
								obj_t BgL_az00_4983;
								obj_t BgL_clausesz00_4982;

								BgL_clausesz00_4982 = BgL_arg1937z00_3572;
								BgL_az00_4983 = BgL_arg1938z00_3573;
								BgL_az00_3571 = BgL_az00_4983;
								BgL_clausesz00_3570 = BgL_clausesz00_4982;
								goto BgL_liipz00_3569;
							}
						}
				}
			}
		}

	}



/* &node-A-fail1358 */
	obj_t BGl_z62nodezd2Azd2fail1358z62zzintegrate_az00(obj_t BgL_envz00_3332,
		obj_t BgL_nodez00_3333, obj_t BgL_hostz00_3334, obj_t BgL_kz00_3335,
		obj_t BgL_az00_3336)
	{
		{	/* Integrate/a.scm 359 */
			{	/* Integrate/a.scm 361 */
				BgL_nodez00_bglt BgL_arg1917z00_3577;
				obj_t BgL_arg1918z00_3578;
				obj_t BgL_arg1919z00_3579;

				BgL_arg1917z00_3577 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_3333)))->BgL_procz00);
				{	/* Integrate/a.scm 363 */
					obj_t BgL_arg1920z00_3580;
					BgL_nodez00_bglt BgL_arg1923z00_3581;

					BgL_arg1920z00_3580 = BGl_getzd2newzd2kontz00zzintegrate_az00();
					BgL_arg1923z00_3581 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3333)))->BgL_procz00);
					BgL_arg1918z00_3578 =
						MAKE_YOUNG_PAIR(BgL_arg1920z00_3580, ((obj_t) BgL_arg1923z00_3581));
				}
				{	/* Integrate/a.scm 364 */
					BgL_nodez00_bglt BgL_arg1924z00_3582;
					obj_t BgL_arg1925z00_3583;
					obj_t BgL_arg1926z00_3584;

					BgL_arg1924z00_3582 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_3333)))->BgL_msgz00);
					{	/* Integrate/a.scm 366 */
						obj_t BgL_arg1927z00_3585;
						obj_t BgL_arg1928z00_3586;

						BgL_arg1927z00_3585 = BGl_getzd2newzd2kontz00zzintegrate_az00();
						{	/* Integrate/a.scm 366 */
							BgL_nodez00_bglt BgL_arg1929z00_3587;

							BgL_arg1929z00_3587 =
								(((BgL_failz00_bglt) COBJECT(
										((BgL_failz00_bglt) BgL_nodez00_3333)))->BgL_msgz00);
							BgL_arg1928z00_3586 =
								BGl_normaliza7ezd2typez75zzintegrate_az00
								(BGl_getzd2typezd2zztype_typeofz00(BgL_arg1929z00_3587,
									((bool_t) 0)));
						}
						BgL_arg1925z00_3583 =
							MAKE_YOUNG_PAIR(BgL_arg1927z00_3585, BgL_arg1928z00_3586);
					}
					{	/* Integrate/a.scm 367 */
						BgL_nodez00_bglt BgL_arg1930z00_3588;
						obj_t BgL_arg1931z00_3589;

						BgL_arg1930z00_3588 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_3333)))->BgL_objz00);
						{	/* Integrate/a.scm 368 */
							obj_t BgL_arg1932z00_3590;
							obj_t BgL_arg1933z00_3591;

							BgL_arg1932z00_3590 = BGl_getzd2newzd2kontz00zzintegrate_az00();
							{	/* Integrate/a.scm 368 */
								BgL_nodez00_bglt BgL_arg1934z00_3592;

								BgL_arg1934z00_3592 =
									(((BgL_failz00_bglt) COBJECT(
											((BgL_failz00_bglt) BgL_nodez00_3333)))->BgL_objz00);
								BgL_arg1933z00_3591 =
									BGl_normaliza7ezd2typez75zzintegrate_az00
									(BGl_getzd2typezd2zztype_typeofz00(BgL_arg1934z00_3592,
										((bool_t) 0)));
							}
							BgL_arg1931z00_3589 =
								MAKE_YOUNG_PAIR(BgL_arg1932z00_3590, BgL_arg1933z00_3591);
						}
						BgL_arg1926z00_3584 =
							BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1930z00_3588,
							((BgL_variablez00_bglt) BgL_hostz00_3334), BgL_arg1931z00_3589,
							BgL_az00_3336);
					}
					BgL_arg1919z00_3579 =
						BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1924z00_3582,
						((BgL_variablez00_bglt) BgL_hostz00_3334), BgL_arg1925z00_3583,
						BgL_arg1926z00_3584);
				}
				return
					BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1917z00_3577,
					((BgL_variablez00_bglt) BgL_hostz00_3334), BgL_arg1918z00_3578,
					BgL_arg1919z00_3579);
			}
		}

	}



/* &node-A-conditional1356 */
	obj_t BGl_z62nodezd2Azd2conditional1356z62zzintegrate_az00(obj_t
		BgL_envz00_3337, obj_t BgL_nodez00_3338, obj_t BgL_hostz00_3339,
		obj_t BgL_kz00_3340, obj_t BgL_az00_3341)
	{
		{	/* Integrate/a.scm 351 */
			{	/* Integrate/a.scm 353 */
				obj_t BgL_az00_3594;

				{	/* Integrate/a.scm 353 */
					BgL_nodez00_bglt BgL_arg1913z00_3595;
					obj_t BgL_arg1914z00_3596;

					BgL_arg1913z00_3595 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3338)))->BgL_testz00);
					{	/* Integrate/a.scm 353 */
						obj_t BgL_arg1916z00_3597;

						BgL_arg1916z00_3597 = BGl_getzd2newzd2kontz00zzintegrate_az00();
						BgL_arg1914z00_3596 =
							MAKE_YOUNG_PAIR(BgL_arg1916z00_3597,
							BGl_za2boolza2z00zztype_cachez00);
					}
					BgL_az00_3594 =
						BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1913z00_3595,
						((BgL_variablez00_bglt) BgL_hostz00_3339), BgL_arg1914z00_3596,
						BgL_az00_3341);
				}
				{	/* Integrate/a.scm 354 */
					BgL_nodez00_bglt BgL_arg1910z00_3598;
					obj_t BgL_arg1911z00_3599;

					BgL_arg1910z00_3598 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_3338)))->BgL_truez00);
					{	/* Integrate/a.scm 354 */
						BgL_nodez00_bglt BgL_arg1912z00_3600;

						BgL_arg1912z00_3600 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_3338)))->BgL_falsez00);
						BgL_arg1911z00_3599 =
							BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1912z00_3600,
							((BgL_variablez00_bglt) BgL_hostz00_3339), BgL_kz00_3340,
							BgL_az00_3594);
					}
					return
						BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1910z00_3598,
						((BgL_variablez00_bglt) BgL_hostz00_3339), BgL_kz00_3340,
						BgL_arg1911z00_3599);
				}
			}
		}

	}



/* &node-A-setq1354 */
	obj_t BGl_z62nodezd2Azd2setq1354z62zzintegrate_az00(obj_t BgL_envz00_3342,
		obj_t BgL_nodez00_3343, obj_t BgL_hostz00_3344, obj_t BgL_kz00_3345,
		obj_t BgL_az00_3346)
	{
		{	/* Integrate/a.scm 344 */
			{	/* Integrate/a.scm 346 */
				BgL_nodez00_bglt BgL_arg1901z00_3602;
				obj_t BgL_arg1902z00_3603;

				BgL_arg1901z00_3602 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_3343)))->BgL_valuez00);
				{	/* Integrate/a.scm 346 */
					obj_t BgL_arg1903z00_3604;
					obj_t BgL_arg1904z00_3605;

					BgL_arg1903z00_3604 = BGl_getzd2newzd2kontz00zzintegrate_az00();
					{	/* Integrate/a.scm 346 */
						BgL_nodez00_bglt BgL_arg1906z00_3606;

						BgL_arg1906z00_3606 =
							(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nodez00_3343)))->BgL_valuez00);
						BgL_arg1904z00_3605 =
							BGl_normaliza7ezd2typez75zzintegrate_az00
							(BGl_getzd2typezd2zztype_typeofz00(BgL_arg1906z00_3606,
								((bool_t) 0)));
					}
					BgL_arg1902z00_3603 =
						MAKE_YOUNG_PAIR(BgL_arg1903z00_3604, BgL_arg1904z00_3605);
				}
				return
					BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1901z00_3602,
					((BgL_variablez00_bglt) BgL_hostz00_3344), BgL_arg1902z00_3603,
					BgL_az00_3346);
			}
		}

	}



/* &node-A-cast1352 */
	obj_t BGl_z62nodezd2Azd2cast1352z62zzintegrate_az00(obj_t BgL_envz00_3347,
		obj_t BgL_nodez00_3348, obj_t BgL_hostz00_3349, obj_t BgL_kz00_3350,
		obj_t BgL_az00_3351)
	{
		{	/* Integrate/a.scm 337 */
			{	/* Integrate/a.scm 339 */
				BgL_nodez00_bglt BgL_arg1894z00_3608;
				obj_t BgL_arg1896z00_3609;

				BgL_arg1894z00_3608 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_3348)))->BgL_argz00);
				{	/* Integrate/a.scm 339 */
					obj_t BgL_arg1897z00_3610;
					obj_t BgL_arg1898z00_3611;

					BgL_arg1897z00_3610 = BGl_getzd2newzd2kontz00zzintegrate_az00();
					{	/* Integrate/a.scm 339 */
						BgL_nodez00_bglt BgL_arg1899z00_3612;

						BgL_arg1899z00_3612 =
							(((BgL_castz00_bglt) COBJECT(
									((BgL_castz00_bglt) BgL_nodez00_3348)))->BgL_argz00);
						BgL_arg1898z00_3611 =
							BGl_normaliza7ezd2typez75zzintegrate_az00
							(BGl_getzd2typezd2zztype_typeofz00(BgL_arg1899z00_3612,
								((bool_t) 0)));
					}
					BgL_arg1896z00_3609 =
						MAKE_YOUNG_PAIR(BgL_arg1897z00_3610, BgL_arg1898z00_3611);
				}
				return
					BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1894z00_3608,
					((BgL_variablez00_bglt) BgL_hostz00_3349), BgL_arg1896z00_3609,
					BgL_az00_3351);
			}
		}

	}



/* &node-A-extern1350 */
	obj_t BGl_z62nodezd2Azd2extern1350z62zzintegrate_az00(obj_t BgL_envz00_3352,
		obj_t BgL_nodez00_3353, obj_t BgL_hostz00_3354, obj_t BgL_kz00_3355,
		obj_t BgL_az00_3356)
	{
		{	/* Integrate/a.scm 324 */
			{
				obj_t BgL_astsz00_3615;
				obj_t BgL_az00_3616;

				BgL_astsz00_3615 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_3353)))->BgL_exprza2za2);
				BgL_az00_3616 = BgL_az00_3356;
			BgL_liipz00_3614:
				if (NULLP(BgL_astsz00_3615))
					{	/* Integrate/a.scm 328 */
						return BgL_az00_3616;
					}
				else
					{	/* Integrate/a.scm 330 */
						obj_t BgL_arg1887z00_3617;
						obj_t BgL_arg1888z00_3618;

						BgL_arg1887z00_3617 = CDR(((obj_t) BgL_astsz00_3615));
						{	/* Integrate/a.scm 331 */
							obj_t BgL_arg1889z00_3619;
							obj_t BgL_arg1890z00_3620;

							BgL_arg1889z00_3619 = CAR(((obj_t) BgL_astsz00_3615));
							{	/* Integrate/a.scm 332 */
								obj_t BgL_arg1891z00_3621;
								obj_t BgL_arg1892z00_3622;

								BgL_arg1891z00_3621 = BGl_getzd2newzd2kontz00zzintegrate_az00();
								{	/* Integrate/a.scm 332 */
									obj_t BgL_arg1893z00_3623;

									BgL_arg1893z00_3623 = CAR(((obj_t) BgL_astsz00_3615));
									{	/* Integrate/a.scm 105 */
										BgL_typez00_bglt BgL_arg1535z00_3624;

										BgL_arg1535z00_3624 =
											BGl_getzd2typezd2zztype_typeofz00(
											((BgL_nodez00_bglt) BgL_arg1893z00_3623), ((bool_t) 0));
										BgL_arg1892z00_3622 =
											BGl_normaliza7ezd2typez75zzintegrate_az00
											(BgL_arg1535z00_3624);
									}
								}
								BgL_arg1890z00_3620 =
									MAKE_YOUNG_PAIR(BgL_arg1891z00_3621, BgL_arg1892z00_3622);
							}
							BgL_arg1888z00_3618 =
								BGl_nodezd2Azd2zzintegrate_az00(
								((BgL_nodez00_bglt) BgL_arg1889z00_3619),
								((BgL_variablez00_bglt) BgL_hostz00_3354), BgL_arg1890z00_3620,
								BgL_az00_3616);
						}
						{
							obj_t BgL_az00_5064;
							obj_t BgL_astsz00_5063;

							BgL_astsz00_5063 = BgL_arg1887z00_3617;
							BgL_az00_5064 = BgL_arg1888z00_3618;
							BgL_az00_3616 = BgL_az00_5064;
							BgL_astsz00_3615 = BgL_astsz00_5063;
							goto BgL_liipz00_3614;
						}
					}
			}
		}

	}



/* &node-A-funcall1348 */
	obj_t BGl_z62nodezd2Azd2funcall1348z62zzintegrate_az00(obj_t BgL_envz00_3357,
		obj_t BgL_nodez00_3358, obj_t BgL_hostz00_3359, obj_t BgL_kz00_3360,
		obj_t BgL_az00_3361)
	{
		{	/* Integrate/a.scm 308 */
			{	/* Integrate/a.scm 310 */
				BgL_nodez00_bglt BgL_arg1864z00_3626;
				obj_t BgL_arg1866z00_3627;
				obj_t BgL_arg1868z00_3628;

				BgL_arg1864z00_3626 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_3358)))->BgL_funz00);
				{	/* Integrate/a.scm 312 */
					obj_t BgL_arg1869z00_3629;
					obj_t BgL_arg1870z00_3630;

					BgL_arg1869z00_3629 = BGl_getzd2newzd2kontz00zzintegrate_az00();
					{	/* Integrate/a.scm 312 */
						BgL_nodez00_bglt BgL_arg1872z00_3631;

						BgL_arg1872z00_3631 =
							(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nodez00_3358)))->BgL_funz00);
						BgL_arg1870z00_3630 =
							BGl_normaliza7ezd2typez75zzintegrate_az00
							(BGl_getzd2typezd2zztype_typeofz00(BgL_arg1872z00_3631,
								((bool_t) 0)));
					}
					BgL_arg1866z00_3627 =
						MAKE_YOUNG_PAIR(BgL_arg1869z00_3629, BgL_arg1870z00_3630);
				}
				{
					obj_t BgL_argsz00_3633;
					obj_t BgL_az00_3634;

					BgL_argsz00_3633 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_3358)))->BgL_argsz00);
					BgL_az00_3634 = BgL_az00_3361;
				BgL_liipz00_3632:
					if (NULLP(BgL_argsz00_3633))
						{	/* Integrate/a.scm 315 */
							BgL_arg1868z00_3628 = BgL_az00_3634;
						}
					else
						{	/* Integrate/a.scm 317 */
							obj_t BgL_arg1876z00_3635;
							obj_t BgL_arg1877z00_3636;

							BgL_arg1876z00_3635 = CDR(((obj_t) BgL_argsz00_3633));
							{	/* Integrate/a.scm 318 */
								obj_t BgL_arg1878z00_3637;
								obj_t BgL_arg1879z00_3638;

								BgL_arg1878z00_3637 = CAR(((obj_t) BgL_argsz00_3633));
								{	/* Integrate/a.scm 319 */
									obj_t BgL_arg1880z00_3639;
									obj_t BgL_arg1882z00_3640;

									BgL_arg1880z00_3639 =
										BGl_getzd2newzd2kontz00zzintegrate_az00();
									{	/* Integrate/a.scm 319 */
										obj_t BgL_arg1883z00_3641;

										BgL_arg1883z00_3641 = CAR(((obj_t) BgL_argsz00_3633));
										{	/* Integrate/a.scm 105 */
											BgL_typez00_bglt BgL_arg1535z00_3642;

											BgL_arg1535z00_3642 =
												BGl_getzd2typezd2zztype_typeofz00(
												((BgL_nodez00_bglt) BgL_arg1883z00_3641), ((bool_t) 0));
											BgL_arg1882z00_3640 =
												BGl_normaliza7ezd2typez75zzintegrate_az00
												(BgL_arg1535z00_3642);
										}
									}
									BgL_arg1879z00_3638 =
										MAKE_YOUNG_PAIR(BgL_arg1880z00_3639, BgL_arg1882z00_3640);
								}
								BgL_arg1877z00_3636 =
									BGl_nodezd2Azd2zzintegrate_az00(
									((BgL_nodez00_bglt) BgL_arg1878z00_3637),
									((BgL_variablez00_bglt) BgL_hostz00_3359),
									BgL_arg1879z00_3638, BgL_az00_3634);
							}
							{
								obj_t BgL_az00_5092;
								obj_t BgL_argsz00_5091;

								BgL_argsz00_5091 = BgL_arg1876z00_3635;
								BgL_az00_5092 = BgL_arg1877z00_3636;
								BgL_az00_3634 = BgL_az00_5092;
								BgL_argsz00_3633 = BgL_argsz00_5091;
								goto BgL_liipz00_3632;
							}
						}
				}
				return
					BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1864z00_3626,
					((BgL_variablez00_bglt) BgL_hostz00_3359), BgL_arg1866z00_3627,
					BgL_arg1868z00_3628);
			}
		}

	}



/* &node-A-app-ly1346 */
	obj_t BGl_z62nodezd2Azd2appzd2ly1346zb0zzintegrate_az00(obj_t BgL_envz00_3362,
		obj_t BgL_nodez00_3363, obj_t BgL_hostz00_3364, obj_t BgL_kz00_3365,
		obj_t BgL_az00_3366)
	{
		{	/* Integrate/a.scm 298 */
			{	/* Integrate/a.scm 300 */
				BgL_nodez00_bglt BgL_arg1851z00_3644;
				obj_t BgL_arg1852z00_3645;
				obj_t BgL_arg1853z00_3646;

				BgL_arg1851z00_3644 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_3363)))->BgL_funz00);
				{	/* Integrate/a.scm 302 */
					obj_t BgL_arg1854z00_3647;
					obj_t BgL_arg1856z00_3648;

					BgL_arg1854z00_3647 = BGl_getzd2newzd2kontz00zzintegrate_az00();
					{	/* Integrate/a.scm 302 */
						BgL_nodez00_bglt BgL_arg1857z00_3649;

						BgL_arg1857z00_3649 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_3363)))->BgL_funz00);
						BgL_arg1856z00_3648 =
							BGl_normaliza7ezd2typez75zzintegrate_az00
							(BGl_getzd2typezd2zztype_typeofz00(BgL_arg1857z00_3649,
								((bool_t) 0)));
					}
					BgL_arg1852z00_3645 =
						MAKE_YOUNG_PAIR(BgL_arg1854z00_3647, BgL_arg1856z00_3648);
				}
				{	/* Integrate/a.scm 303 */
					BgL_nodez00_bglt BgL_arg1858z00_3650;
					obj_t BgL_arg1859z00_3651;

					BgL_arg1858z00_3650 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3363)))->BgL_argz00);
					{	/* Integrate/a.scm 303 */
						obj_t BgL_arg1860z00_3652;
						obj_t BgL_arg1862z00_3653;

						BgL_arg1860z00_3652 = BGl_getzd2newzd2kontz00zzintegrate_az00();
						{	/* Integrate/a.scm 303 */
							BgL_nodez00_bglt BgL_arg1863z00_3654;

							BgL_arg1863z00_3654 =
								(((BgL_appzd2lyzd2_bglt) COBJECT(
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_3363)))->BgL_argz00);
							BgL_arg1862z00_3653 =
								BGl_normaliza7ezd2typez75zzintegrate_az00
								(BGl_getzd2typezd2zztype_typeofz00(BgL_arg1863z00_3654,
									((bool_t) 0)));
						}
						BgL_arg1859z00_3651 =
							MAKE_YOUNG_PAIR(BgL_arg1860z00_3652, BgL_arg1862z00_3653);
					}
					BgL_arg1853z00_3646 =
						BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1858z00_3650,
						((BgL_variablez00_bglt) BgL_hostz00_3364), BgL_arg1859z00_3651,
						BgL_az00_3366);
				}
				return
					BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1851z00_3644,
					((BgL_variablez00_bglt) BgL_hostz00_3364), BgL_arg1852z00_3645,
					BgL_arg1853z00_3646);
			}
		}

	}



/* &node-A-app1344 */
	obj_t BGl_z62nodezd2Azd2app1344z62zzintegrate_az00(obj_t BgL_envz00_3367,
		obj_t BgL_nodez00_3368, obj_t BgL_hostz00_3369, obj_t BgL_kz00_3370,
		obj_t BgL_az00_3371)
	{
		{	/* Integrate/a.scm 254 */
			{
				BgL_nodez00_bglt BgL_nodez00_3657;

				{	/* Integrate/a.scm 269 */
					BgL_variablez00_bglt BgL_calleez00_3694;

					BgL_calleez00_3694 =
						(((BgL_varz00_bglt) COBJECT(
								(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_3368)))->BgL_funz00)))->
						BgL_variablez00);
					{	/* Integrate/a.scm 271 */
						obj_t BgL_g1137z00_3695;

						BgL_nodez00_3657 =
							((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3368));
						if (CBOOL(BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00))
							{	/* Integrate/a.scm 259 */
								BgL_varz00_bglt BgL_i1133z00_3658;

								BgL_i1133z00_3658 =
									(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_3657)))->BgL_funz00);
								if (BGl_iszd2unwindzd2untilzf3zf3zzreturn_walkz00(
										(((BgL_varz00_bglt) COBJECT(BgL_i1133z00_3658))->
											BgL_variablez00)))
									{	/* Integrate/a.scm 261 */
										bool_t BgL_test2294z00_5127;

										{	/* Integrate/a.scm 261 */
											obj_t BgL_arg1848z00_3659;

											{	/* Integrate/a.scm 261 */
												obj_t BgL_pairz00_3660;

												BgL_pairz00_3660 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_nodez00_3657)))->
													BgL_argsz00);
												BgL_arg1848z00_3659 = CAR(BgL_pairz00_3660);
											}
											{	/* Integrate/a.scm 261 */
												obj_t BgL_classz00_3661;

												BgL_classz00_3661 = BGl_varz00zzast_nodez00;
												if (BGL_OBJECTP(BgL_arg1848z00_3659))
													{	/* Integrate/a.scm 261 */
														BgL_objectz00_bglt BgL_arg1807z00_3662;

														BgL_arg1807z00_3662 =
															(BgL_objectz00_bglt) (BgL_arg1848z00_3659);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Integrate/a.scm 261 */
																long BgL_idxz00_3663;

																BgL_idxz00_3663 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3662);
																BgL_test2294z00_5127 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3663 + 2L)) ==
																	BgL_classz00_3661);
															}
														else
															{	/* Integrate/a.scm 261 */
																bool_t BgL_res2119z00_3666;

																{	/* Integrate/a.scm 261 */
																	obj_t BgL_oclassz00_3667;

																	{	/* Integrate/a.scm 261 */
																		obj_t BgL_arg1815z00_3668;
																		long BgL_arg1816z00_3669;

																		BgL_arg1815z00_3668 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Integrate/a.scm 261 */
																			long BgL_arg1817z00_3670;

																			BgL_arg1817z00_3670 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3662);
																			BgL_arg1816z00_3669 =
																				(BgL_arg1817z00_3670 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3667 =
																			VECTOR_REF(BgL_arg1815z00_3668,
																			BgL_arg1816z00_3669);
																	}
																	{	/* Integrate/a.scm 261 */
																		bool_t BgL__ortest_1115z00_3671;

																		BgL__ortest_1115z00_3671 =
																			(BgL_classz00_3661 == BgL_oclassz00_3667);
																		if (BgL__ortest_1115z00_3671)
																			{	/* Integrate/a.scm 261 */
																				BgL_res2119z00_3666 =
																					BgL__ortest_1115z00_3671;
																			}
																		else
																			{	/* Integrate/a.scm 261 */
																				long BgL_odepthz00_3672;

																				{	/* Integrate/a.scm 261 */
																					obj_t BgL_arg1804z00_3673;

																					BgL_arg1804z00_3673 =
																						(BgL_oclassz00_3667);
																					BgL_odepthz00_3672 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3673);
																				}
																				if ((2L < BgL_odepthz00_3672))
																					{	/* Integrate/a.scm 261 */
																						obj_t BgL_arg1802z00_3674;

																						{	/* Integrate/a.scm 261 */
																							obj_t BgL_arg1803z00_3675;

																							BgL_arg1803z00_3675 =
																								(BgL_oclassz00_3667);
																							BgL_arg1802z00_3674 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3675, 2L);
																						}
																						BgL_res2119z00_3666 =
																							(BgL_arg1802z00_3674 ==
																							BgL_classz00_3661);
																					}
																				else
																					{	/* Integrate/a.scm 261 */
																						BgL_res2119z00_3666 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2294z00_5127 = BgL_res2119z00_3666;
															}
													}
												else
													{	/* Integrate/a.scm 261 */
														BgL_test2294z00_5127 = ((bool_t) 0);
													}
											}
										}
										if (BgL_test2294z00_5127)
											{	/* Integrate/a.scm 262 */
												BgL_varz00_bglt BgL_i1134z00_3676;

												{	/* Integrate/a.scm 262 */
													obj_t BgL_pairz00_3677;

													BgL_pairz00_3677 =
														(((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_nodez00_3657)))->
														BgL_argsz00);
													BgL_i1134z00_3676 =
														((BgL_varz00_bglt) CAR(BgL_pairz00_3677));
												}
												{	/* Integrate/a.scm 263 */
													BgL_valuez00_bglt BgL_valz00_3678;

													BgL_valz00_3678 =
														(((BgL_variablez00_bglt) COBJECT(
																(((BgL_varz00_bglt)
																		COBJECT(BgL_i1134z00_3676))->
																	BgL_variablez00)))->BgL_valuez00);
													{	/* Integrate/a.scm 264 */
														bool_t BgL_test2299z00_5159;

														{	/* Integrate/a.scm 264 */
															obj_t BgL_classz00_3679;

															BgL_classz00_3679 =
																BGl_svarzf2Iinfozf2zzintegrate_infoz00;
															{	/* Integrate/a.scm 264 */
																BgL_objectz00_bglt BgL_arg1807z00_3680;

																{	/* Integrate/a.scm 264 */
																	obj_t BgL_tmpz00_5160;

																	BgL_tmpz00_5160 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_valz00_3678));
																	BgL_arg1807z00_3680 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_5160);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Integrate/a.scm 264 */
																		long BgL_idxz00_3681;

																		BgL_idxz00_3681 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_3680);
																		BgL_test2299z00_5159 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_3681 + 3L)) ==
																			BgL_classz00_3679);
																	}
																else
																	{	/* Integrate/a.scm 264 */
																		bool_t BgL_res2120z00_3684;

																		{	/* Integrate/a.scm 264 */
																			obj_t BgL_oclassz00_3685;

																			{	/* Integrate/a.scm 264 */
																				obj_t BgL_arg1815z00_3686;
																				long BgL_arg1816z00_3687;

																				BgL_arg1815z00_3686 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Integrate/a.scm 264 */
																					long BgL_arg1817z00_3688;

																					BgL_arg1817z00_3688 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_3680);
																					BgL_arg1816z00_3687 =
																						(BgL_arg1817z00_3688 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_3685 =
																					VECTOR_REF(BgL_arg1815z00_3686,
																					BgL_arg1816z00_3687);
																			}
																			{	/* Integrate/a.scm 264 */
																				bool_t BgL__ortest_1115z00_3689;

																				BgL__ortest_1115z00_3689 =
																					(BgL_classz00_3679 ==
																					BgL_oclassz00_3685);
																				if (BgL__ortest_1115z00_3689)
																					{	/* Integrate/a.scm 264 */
																						BgL_res2120z00_3684 =
																							BgL__ortest_1115z00_3689;
																					}
																				else
																					{	/* Integrate/a.scm 264 */
																						long BgL_odepthz00_3690;

																						{	/* Integrate/a.scm 264 */
																							obj_t BgL_arg1804z00_3691;

																							BgL_arg1804z00_3691 =
																								(BgL_oclassz00_3685);
																							BgL_odepthz00_3690 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_3691);
																						}
																						if ((3L < BgL_odepthz00_3690))
																							{	/* Integrate/a.scm 264 */
																								obj_t BgL_arg1802z00_3692;

																								{	/* Integrate/a.scm 264 */
																									obj_t BgL_arg1803z00_3693;

																									BgL_arg1803z00_3693 =
																										(BgL_oclassz00_3685);
																									BgL_arg1802z00_3692 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_3693, 3L);
																								}
																								BgL_res2120z00_3684 =
																									(BgL_arg1802z00_3692 ==
																									BgL_classz00_3679);
																							}
																						else
																							{	/* Integrate/a.scm 264 */
																								BgL_res2120z00_3684 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2299z00_5159 = BgL_res2120z00_3684;
																	}
															}
														}
														if (BgL_test2299z00_5159)
															{
																BgL_svarzf2iinfozf2_bglt BgL_auxz00_5183;

																{
																	obj_t BgL_auxz00_5184;

																	{	/* Integrate/a.scm 265 */
																		BgL_objectz00_bglt BgL_tmpz00_5185;

																		BgL_tmpz00_5185 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_valz00_3678));
																		BgL_auxz00_5184 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_5185);
																	}
																	BgL_auxz00_5183 =
																		((BgL_svarzf2iinfozf2_bglt)
																		BgL_auxz00_5184);
																}
																BgL_g1137z00_3695 =
																	(((BgL_svarzf2iinfozf2_bglt)
																		COBJECT(BgL_auxz00_5183))->BgL_xhdlz00);
															}
														else
															{	/* Integrate/a.scm 264 */
																BgL_g1137z00_3695 = BFALSE;
															}
													}
												}
											}
										else
											{	/* Integrate/a.scm 261 */
												BgL_g1137z00_3695 = BFALSE;
											}
									}
								else
									{	/* Integrate/a.scm 260 */
										BgL_g1137z00_3695 = BFALSE;
									}
							}
						else
							{	/* Integrate/a.scm 257 */
								BgL_g1137z00_3695 = BFALSE;
							}
						if (CBOOL(BgL_g1137z00_3695))
							{	/* Integrate/a.scm 271 */
								{	/* Integrate/a.scm 274 */
									BgL_sfunz00_bglt BgL_i1139z00_3696;

									BgL_i1139z00_3696 =
										((BgL_sfunz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_hostz00_3369)))->
											BgL_valuez00));
									{	/* Integrate/a.scm 275 */
										bool_t BgL_test2304z00_5198;

										{	/* Integrate/a.scm 275 */
											obj_t BgL_arg1806z00_3697;

											{
												BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5199;

												{
													obj_t BgL_auxz00_5200;

													{	/* Integrate/a.scm 275 */
														BgL_objectz00_bglt BgL_tmpz00_5201;

														BgL_tmpz00_5201 =
															((BgL_objectz00_bglt) BgL_i1139z00_3696);
														BgL_auxz00_5200 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5201);
													}
													BgL_auxz00_5199 =
														((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5200);
												}
												BgL_arg1806z00_3697 =
													(((BgL_sfunzf2iinfozf2_bglt)
														COBJECT(BgL_auxz00_5199))->BgL_xhdlsz00);
											}
											BgL_test2304z00_5198 =
												CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
												(BgL_g1137z00_3695, BgL_arg1806z00_3697));
										}
										if (BgL_test2304z00_5198)
											{	/* Integrate/a.scm 275 */
												BFALSE;
											}
										else
											{
												obj_t BgL_auxz00_5214;
												BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5208;

												{	/* Integrate/a.scm 276 */
													obj_t BgL_arg1805z00_3698;

													{
														BgL_sfunzf2iinfozf2_bglt BgL_auxz00_5215;

														{
															obj_t BgL_auxz00_5216;

															{	/* Integrate/a.scm 276 */
																BgL_objectz00_bglt BgL_tmpz00_5217;

																BgL_tmpz00_5217 =
																	((BgL_objectz00_bglt) BgL_i1139z00_3696);
																BgL_auxz00_5216 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5217);
															}
															BgL_auxz00_5215 =
																((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5216);
														}
														BgL_arg1805z00_3698 =
															(((BgL_sfunzf2iinfozf2_bglt)
																COBJECT(BgL_auxz00_5215))->BgL_xhdlsz00);
													}
													BgL_auxz00_5214 =
														MAKE_YOUNG_PAIR(BgL_g1137z00_3695,
														BgL_arg1805z00_3698);
												}
												{
													obj_t BgL_auxz00_5209;

													{	/* Integrate/a.scm 276 */
														BgL_objectz00_bglt BgL_tmpz00_5210;

														BgL_tmpz00_5210 =
															((BgL_objectz00_bglt) BgL_i1139z00_3696);
														BgL_auxz00_5209 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5210);
													}
													BgL_auxz00_5208 =
														((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_5209);
												}
												((((BgL_sfunzf2iinfozf2_bglt)
															COBJECT(BgL_auxz00_5208))->BgL_xhdlsz00) =
													((obj_t) BgL_auxz00_5214), BUNSPEC);
											}
									}
								}
								return BgL_az00_3371;
							}
						else
							{	/* Integrate/a.scm 279 */
								obj_t BgL_g1140z00_3699;

								BgL_g1140z00_3699 =
									(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_3368)))->BgL_argsz00);
								{
									obj_t BgL_argsz00_3701;
									obj_t BgL_az00_3702;

									BgL_argsz00_3701 = BgL_g1140z00_3699;
									BgL_az00_3702 = BgL_az00_3371;
								BgL_liipz00_3700:
									if (NULLP(BgL_argsz00_3701))
										{	/* Integrate/a.scm 284 */
											bool_t BgL_test2306z00_5228;

											{	/* Integrate/a.scm 284 */
												obj_t BgL_classz00_3703;

												BgL_classz00_3703 = BGl_localz00zzast_varz00;
												{	/* Integrate/a.scm 284 */
													BgL_objectz00_bglt BgL_arg1807z00_3704;

													{	/* Integrate/a.scm 284 */
														obj_t BgL_tmpz00_5229;

														BgL_tmpz00_5229 =
															((obj_t)
															((BgL_objectz00_bglt) BgL_calleez00_3694));
														BgL_arg1807z00_3704 =
															(BgL_objectz00_bglt) (BgL_tmpz00_5229);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Integrate/a.scm 284 */
															long BgL_idxz00_3705;

															BgL_idxz00_3705 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3704);
															BgL_test2306z00_5228 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_3705 + 2L)) == BgL_classz00_3703);
														}
													else
														{	/* Integrate/a.scm 284 */
															bool_t BgL_res2121z00_3708;

															{	/* Integrate/a.scm 284 */
																obj_t BgL_oclassz00_3709;

																{	/* Integrate/a.scm 284 */
																	obj_t BgL_arg1815z00_3710;
																	long BgL_arg1816z00_3711;

																	BgL_arg1815z00_3710 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Integrate/a.scm 284 */
																		long BgL_arg1817z00_3712;

																		BgL_arg1817z00_3712 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3704);
																		BgL_arg1816z00_3711 =
																			(BgL_arg1817z00_3712 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_3709 =
																		VECTOR_REF(BgL_arg1815z00_3710,
																		BgL_arg1816z00_3711);
																}
																{	/* Integrate/a.scm 284 */
																	bool_t BgL__ortest_1115z00_3713;

																	BgL__ortest_1115z00_3713 =
																		(BgL_classz00_3703 == BgL_oclassz00_3709);
																	if (BgL__ortest_1115z00_3713)
																		{	/* Integrate/a.scm 284 */
																			BgL_res2121z00_3708 =
																				BgL__ortest_1115z00_3713;
																		}
																	else
																		{	/* Integrate/a.scm 284 */
																			long BgL_odepthz00_3714;

																			{	/* Integrate/a.scm 284 */
																				obj_t BgL_arg1804z00_3715;

																				BgL_arg1804z00_3715 =
																					(BgL_oclassz00_3709);
																				BgL_odepthz00_3714 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_3715);
																			}
																			if ((2L < BgL_odepthz00_3714))
																				{	/* Integrate/a.scm 284 */
																					obj_t BgL_arg1802z00_3716;

																					{	/* Integrate/a.scm 284 */
																						obj_t BgL_arg1803z00_3717;

																						BgL_arg1803z00_3717 =
																							(BgL_oclassz00_3709);
																						BgL_arg1802z00_3716 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_3717, 2L);
																					}
																					BgL_res2121z00_3708 =
																						(BgL_arg1802z00_3716 ==
																						BgL_classz00_3703);
																				}
																			else
																				{	/* Integrate/a.scm 284 */
																					BgL_res2121z00_3708 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2306z00_5228 = BgL_res2121z00_3708;
														}
												}
											}
											if (BgL_test2306z00_5228)
												{	/* Integrate/a.scm 287 */
													obj_t BgL_arg1812z00_3718;

													{	/* Integrate/a.scm 287 */
														obj_t BgL_arg1820z00_3719;

														{	/* Integrate/a.scm 287 */
															obj_t BgL_arg1822z00_3720;

															BgL_arg1822z00_3720 =
																MAKE_YOUNG_PAIR(BgL_kz00_3370, BNIL);
															BgL_arg1820z00_3719 =
																MAKE_YOUNG_PAIR(
																((obj_t) BgL_calleez00_3694),
																BgL_arg1822z00_3720);
														}
														BgL_arg1812z00_3718 =
															MAKE_YOUNG_PAIR(BgL_hostz00_3369,
															BgL_arg1820z00_3719);
													}
													return
														MAKE_YOUNG_PAIR(BgL_arg1812z00_3718, BgL_az00_3702);
												}
											else
												{	/* Integrate/a.scm 284 */
													return BgL_az00_3702;
												}
										}
									else
										{	/* Integrate/a.scm 289 */
											obj_t BgL_arg1823z00_3721;
											obj_t BgL_arg1831z00_3722;

											BgL_arg1823z00_3721 = CDR(((obj_t) BgL_argsz00_3701));
											{	/* Integrate/a.scm 290 */
												obj_t BgL_arg1832z00_3723;
												obj_t BgL_arg1833z00_3724;

												BgL_arg1832z00_3723 = CAR(((obj_t) BgL_argsz00_3701));
												{	/* Integrate/a.scm 292 */
													obj_t BgL_arg1834z00_3725;
													obj_t BgL_arg1835z00_3726;

													BgL_arg1834z00_3725 =
														BGl_getzd2newzd2kontz00zzintegrate_az00();
													{	/* Integrate/a.scm 292 */
														obj_t BgL_arg1836z00_3727;

														BgL_arg1836z00_3727 =
															CAR(((obj_t) BgL_argsz00_3701));
														{	/* Integrate/a.scm 105 */
															BgL_typez00_bglt BgL_arg1535z00_3728;

															BgL_arg1535z00_3728 =
																BGl_getzd2typezd2zztype_typeofz00(
																((BgL_nodez00_bglt) BgL_arg1836z00_3727),
																((bool_t) 0));
															BgL_arg1835z00_3726 =
																BGl_normaliza7ezd2typez75zzintegrate_az00
																(BgL_arg1535z00_3728);
														}
													}
													BgL_arg1833z00_3724 =
														MAKE_YOUNG_PAIR(BgL_arg1834z00_3725,
														BgL_arg1835z00_3726);
												}
												BgL_arg1831z00_3722 =
													BGl_nodezd2Azd2zzintegrate_az00(
													((BgL_nodez00_bglt) BgL_arg1832z00_3723),
													((BgL_variablez00_bglt) BgL_hostz00_3369),
													BgL_arg1833z00_3724, BgL_az00_3702);
											}
											{
												obj_t BgL_az00_5272;
												obj_t BgL_argsz00_5271;

												BgL_argsz00_5271 = BgL_arg1823z00_3721;
												BgL_az00_5272 = BgL_arg1831z00_3722;
												BgL_az00_3702 = BgL_az00_5272;
												BgL_argsz00_3701 = BgL_argsz00_5271;
												goto BgL_liipz00_3700;
											}
										}
								}
							}
					}
				}
			}
		}

	}



/* &node-A-sync1342 */
	obj_t BGl_z62nodezd2Azd2sync1342z62zzintegrate_az00(obj_t BgL_envz00_3372,
		obj_t BgL_nodez00_3373, obj_t BgL_hostz00_3374, obj_t BgL_kz00_3375,
		obj_t BgL_az00_3376)
	{
		{	/* Integrate/a.scm 245 */
			{	/* Integrate/a.scm 247 */
				BgL_nodez00_bglt BgL_arg1761z00_3730;
				obj_t BgL_arg1762z00_3731;

				BgL_arg1761z00_3730 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_3373)))->BgL_bodyz00);
				{	/* Integrate/a.scm 248 */
					BgL_nodez00_bglt BgL_arg1765z00_3732;
					obj_t BgL_arg1767z00_3733;
					obj_t BgL_arg1770z00_3734;

					BgL_arg1765z00_3732 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3373)))->BgL_prelockz00);
					{	/* Integrate/a.scm 248 */
						obj_t BgL_arg1771z00_3735;

						BgL_arg1771z00_3735 = BGl_getzd2newzd2kontz00zzintegrate_az00();
						BgL_arg1767z00_3733 =
							MAKE_YOUNG_PAIR(BgL_arg1771z00_3735,
							BGl_za2objza2z00zztype_cachez00);
					}
					{	/* Integrate/a.scm 249 */
						BgL_nodez00_bglt BgL_arg1773z00_3736;
						obj_t BgL_arg1775z00_3737;

						BgL_arg1773z00_3736 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_3373)))->BgL_mutexz00);
						{	/* Integrate/a.scm 249 */
							obj_t BgL_arg1798z00_3738;

							BgL_arg1798z00_3738 = BGl_getzd2newzd2kontz00zzintegrate_az00();
							BgL_arg1775z00_3737 =
								MAKE_YOUNG_PAIR(BgL_arg1798z00_3738,
								BGl_za2mutexza2z00zztype_cachez00);
						}
						BgL_arg1770z00_3734 =
							BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1773z00_3736,
							((BgL_variablez00_bglt) BgL_hostz00_3374), BgL_arg1775z00_3737,
							BgL_az00_3376);
					}
					BgL_arg1762z00_3731 =
						BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1765z00_3732,
						((BgL_variablez00_bglt) BgL_hostz00_3374), BgL_arg1767z00_3733,
						BgL_arg1770z00_3734);
				}
				return
					BGl_nodezd2Azd2zzintegrate_az00(BgL_arg1761z00_3730,
					((BgL_variablez00_bglt) BgL_hostz00_3374), BgL_kz00_3375,
					BgL_arg1762z00_3731);
			}
		}

	}



/* &node-A-sequence1340 */
	obj_t BGl_z62nodezd2Azd2sequence1340z62zzintegrate_az00(obj_t BgL_envz00_3377,
		obj_t BgL_nodez00_3378, obj_t BgL_hostz00_3379, obj_t BgL_kz00_3380,
		obj_t BgL_az00_3381)
	{
		{	/* Integrate/a.scm 228 */
			if (NULLP(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_3378)))->BgL_nodesz00)))
				{	/* Integrate/a.scm 230 */
					return BgL_az00_3381;
				}
			else
				{
					obj_t BgL_ndsz00_3741;
					obj_t BgL_az00_3742;

					BgL_ndsz00_3741 =
						(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_3378)))->BgL_nodesz00);
					BgL_az00_3742 = BgL_az00_3381;
				BgL_liipz00_3740:
					if (NULLP(CDR(((obj_t) BgL_ndsz00_3741))))
						{	/* Integrate/a.scm 235 */
							obj_t BgL_arg1746z00_3743;

							BgL_arg1746z00_3743 = CAR(((obj_t) BgL_ndsz00_3741));
							return
								BGl_nodezd2Azd2zzintegrate_az00(
								((BgL_nodez00_bglt) BgL_arg1746z00_3743),
								((BgL_variablez00_bglt) BgL_hostz00_3379), BgL_kz00_3380,
								BgL_az00_3742);
						}
					else
						{	/* Integrate/a.scm 236 */
							obj_t BgL_arg1747z00_3744;
							obj_t BgL_arg1748z00_3745;

							BgL_arg1747z00_3744 = CDR(((obj_t) BgL_ndsz00_3741));
							{	/* Integrate/a.scm 237 */
								obj_t BgL_arg1749z00_3746;
								obj_t BgL_arg1750z00_3747;

								BgL_arg1749z00_3746 = CAR(((obj_t) BgL_ndsz00_3741));
								{	/* Integrate/a.scm 239 */
									obj_t BgL_arg1751z00_3748;
									obj_t BgL_arg1752z00_3749;

									BgL_arg1751z00_3748 =
										BGl_getzd2newzd2kontz00zzintegrate_az00();
									{	/* Integrate/a.scm 239 */
										obj_t BgL_arg1753z00_3750;

										BgL_arg1753z00_3750 = CAR(((obj_t) BgL_ndsz00_3741));
										{	/* Integrate/a.scm 105 */
											BgL_typez00_bglt BgL_arg1535z00_3751;

											BgL_arg1535z00_3751 =
												BGl_getzd2typezd2zztype_typeofz00(
												((BgL_nodez00_bglt) BgL_arg1753z00_3750), ((bool_t) 0));
											BgL_arg1752z00_3749 =
												BGl_normaliza7ezd2typez75zzintegrate_az00
												(BgL_arg1535z00_3751);
										}
									}
									BgL_arg1750z00_3747 =
										MAKE_YOUNG_PAIR(BgL_arg1751z00_3748, BgL_arg1752z00_3749);
								}
								BgL_arg1748z00_3745 =
									BGl_nodezd2Azd2zzintegrate_az00(
									((BgL_nodez00_bglt) BgL_arg1749z00_3746),
									((BgL_variablez00_bglt) BgL_hostz00_3379),
									BgL_arg1750z00_3747, BgL_az00_3742);
							}
							{
								obj_t BgL_az00_5317;
								obj_t BgL_ndsz00_5316;

								BgL_ndsz00_5316 = BgL_arg1747z00_3744;
								BgL_az00_5317 = BgL_arg1748z00_3745;
								BgL_az00_3742 = BgL_az00_5317;
								BgL_ndsz00_3741 = BgL_ndsz00_5316;
								goto BgL_liipz00_3740;
							}
						}
				}
		}

	}



/* &node-A-closure1338 */
	obj_t BGl_z62nodezd2Azd2closure1338z62zzintegrate_az00(obj_t BgL_envz00_3382,
		obj_t BgL_nodez00_3383, obj_t BgL_hostz00_3384, obj_t BgL_kz00_3385,
		obj_t BgL_az00_3386)
	{
		{	/* Integrate/a.scm 222 */
			{	/* Integrate/a.scm 223 */
				obj_t BgL_arg1734z00_3753;

				BgL_arg1734z00_3753 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_3383)));
				return
					BGl_internalzd2errorzd2zztools_errorz00
					(BGl_string2139z00zzintegrate_az00, BGl_string2161z00zzintegrate_az00,
					BgL_arg1734z00_3753);
			}
		}

	}



/* &node-A-var1336 */
	obj_t BGl_z62nodezd2Azd2var1336z62zzintegrate_az00(obj_t BgL_envz00_3387,
		obj_t BgL_nodez00_3388, obj_t BgL_hostz00_3389, obj_t BgL_kz00_3390,
		obj_t BgL_az00_3391)
	{
		{	/* Integrate/a.scm 206 */
			if (CBOOL(BGl_za2optimzd2returnzd2gotozf3za2zf3zzengine_paramz00))
				{	/* Integrate/a.scm 209 */
					BgL_valuez00_bglt BgL_valz00_3755;

					BgL_valz00_3755 =
						(((BgL_variablez00_bglt) COBJECT(
								(((BgL_varz00_bglt) COBJECT(
											((BgL_varz00_bglt) BgL_nodez00_3388)))->
									BgL_variablez00)))->BgL_valuez00);
					{	/* Integrate/a.scm 210 */
						bool_t BgL_test2313z00_5329;

						{	/* Integrate/a.scm 210 */
							obj_t BgL_classz00_3756;

							BgL_classz00_3756 = BGl_svarzf2Iinfozf2zzintegrate_infoz00;
							{	/* Integrate/a.scm 210 */
								BgL_objectz00_bglt BgL_arg1807z00_3757;

								{	/* Integrate/a.scm 210 */
									obj_t BgL_tmpz00_5330;

									BgL_tmpz00_5330 =
										((obj_t) ((BgL_objectz00_bglt) BgL_valz00_3755));
									BgL_arg1807z00_3757 = (BgL_objectz00_bglt) (BgL_tmpz00_5330);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Integrate/a.scm 210 */
										long BgL_idxz00_3758;

										BgL_idxz00_3758 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3757);
										BgL_test2313z00_5329 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3758 + 3L)) == BgL_classz00_3756);
									}
								else
									{	/* Integrate/a.scm 210 */
										bool_t BgL_res2118z00_3761;

										{	/* Integrate/a.scm 210 */
											obj_t BgL_oclassz00_3762;

											{	/* Integrate/a.scm 210 */
												obj_t BgL_arg1815z00_3763;
												long BgL_arg1816z00_3764;

												BgL_arg1815z00_3763 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Integrate/a.scm 210 */
													long BgL_arg1817z00_3765;

													BgL_arg1817z00_3765 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3757);
													BgL_arg1816z00_3764 =
														(BgL_arg1817z00_3765 - OBJECT_TYPE);
												}
												BgL_oclassz00_3762 =
													VECTOR_REF(BgL_arg1815z00_3763, BgL_arg1816z00_3764);
											}
											{	/* Integrate/a.scm 210 */
												bool_t BgL__ortest_1115z00_3766;

												BgL__ortest_1115z00_3766 =
													(BgL_classz00_3756 == BgL_oclassz00_3762);
												if (BgL__ortest_1115z00_3766)
													{	/* Integrate/a.scm 210 */
														BgL_res2118z00_3761 = BgL__ortest_1115z00_3766;
													}
												else
													{	/* Integrate/a.scm 210 */
														long BgL_odepthz00_3767;

														{	/* Integrate/a.scm 210 */
															obj_t BgL_arg1804z00_3768;

															BgL_arg1804z00_3768 = (BgL_oclassz00_3762);
															BgL_odepthz00_3767 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3768);
														}
														if ((3L < BgL_odepthz00_3767))
															{	/* Integrate/a.scm 210 */
																obj_t BgL_arg1802z00_3769;

																{	/* Integrate/a.scm 210 */
																	obj_t BgL_arg1803z00_3770;

																	BgL_arg1803z00_3770 = (BgL_oclassz00_3762);
																	BgL_arg1802z00_3769 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3770,
																		3L);
																}
																BgL_res2118z00_3761 =
																	(BgL_arg1802z00_3769 == BgL_classz00_3756);
															}
														else
															{	/* Integrate/a.scm 210 */
																BgL_res2118z00_3761 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2313z00_5329 = BgL_res2118z00_3761;
									}
							}
						}
						if (BgL_test2313z00_5329)
							{	/* Integrate/a.scm 212 */
								bool_t BgL_test2317z00_5353;

								{	/* Integrate/a.scm 212 */
									obj_t BgL_tmpz00_5354;

									{
										BgL_svarzf2iinfozf2_bglt BgL_auxz00_5355;

										{
											obj_t BgL_auxz00_5356;

											{	/* Integrate/a.scm 212 */
												BgL_objectz00_bglt BgL_tmpz00_5357;

												BgL_tmpz00_5357 =
													((BgL_objectz00_bglt)
													((BgL_svarz00_bglt) BgL_valz00_3755));
												BgL_auxz00_5356 = BGL_OBJECT_WIDENING(BgL_tmpz00_5357);
											}
											BgL_auxz00_5355 =
												((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_5356);
										}
										BgL_tmpz00_5354 =
											(((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_5355))->
											BgL_xhdlz00);
									}
									BgL_test2317z00_5353 = CBOOL(BgL_tmpz00_5354);
								}
								if (BgL_test2317z00_5353)
									{	/* Integrate/a.scm 213 */
										obj_t BgL_nkz00_3771;

										{	/* Integrate/a.scm 213 */
											obj_t BgL_arg1722z00_3772;
											obj_t BgL_arg1724z00_3773;

											BgL_arg1722z00_3772 =
												BGl_getzd2newzd2kontz00zzintegrate_az00();
											{	/* Integrate/a.scm 105 */
												BgL_typez00_bglt BgL_arg1535z00_3774;

												BgL_arg1535z00_3774 =
													BGl_getzd2typezd2zztype_typeofz00(
													((BgL_nodez00_bglt)
														((BgL_varz00_bglt) BgL_nodez00_3388)),
													((bool_t) 0));
												BgL_arg1724z00_3773 =
													BGl_normaliza7ezd2typez75zzintegrate_az00
													(BgL_arg1535z00_3774);
											}
											BgL_nkz00_3771 =
												MAKE_YOUNG_PAIR(BgL_arg1722z00_3772,
												BgL_arg1724z00_3773);
										}
										{	/* Integrate/a.scm 214 */
											obj_t BgL_arg1714z00_3775;

											{	/* Integrate/a.scm 214 */
												obj_t BgL_arg1717z00_3776;

												{	/* Integrate/a.scm 214 */
													obj_t BgL_arg1718z00_3777;
													obj_t BgL_arg1720z00_3778;

													{
														BgL_svarzf2iinfozf2_bglt BgL_auxz00_5370;

														{
															obj_t BgL_auxz00_5371;

															{	/* Integrate/a.scm 214 */
																BgL_objectz00_bglt BgL_tmpz00_5372;

																BgL_tmpz00_5372 =
																	((BgL_objectz00_bglt)
																	((BgL_svarz00_bglt) BgL_valz00_3755));
																BgL_auxz00_5371 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5372);
															}
															BgL_auxz00_5370 =
																((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_5371);
														}
														BgL_arg1718z00_3777 =
															(((BgL_svarzf2iinfozf2_bglt)
																COBJECT(BgL_auxz00_5370))->BgL_xhdlz00);
													}
													BgL_arg1720z00_3778 =
														MAKE_YOUNG_PAIR(BgL_nkz00_3771, BNIL);
													BgL_arg1717z00_3776 =
														MAKE_YOUNG_PAIR(BgL_arg1718z00_3777,
														BgL_arg1720z00_3778);
												}
												BgL_arg1714z00_3775 =
													MAKE_YOUNG_PAIR(BgL_hostz00_3389,
													BgL_arg1717z00_3776);
											}
											return
												MAKE_YOUNG_PAIR(BgL_arg1714z00_3775, BgL_az00_3391);
										}
									}
								else
									{	/* Integrate/a.scm 212 */
										return BgL_az00_3391;
									}
							}
						else
							{	/* Integrate/a.scm 210 */
								return BgL_az00_3391;
							}
					}
				}
			else
				{	/* Integrate/a.scm 207 */
					return BgL_az00_3391;
				}
		}

	}



/* &node-A-kwote1334 */
	obj_t BGl_z62nodezd2Azd2kwote1334z62zzintegrate_az00(obj_t BgL_envz00_3392,
		obj_t BgL_nodez00_3393, obj_t BgL_hostz00_3394, obj_t BgL_kz00_3395,
		obj_t BgL_az00_3396)
	{
		{	/* Integrate/a.scm 200 */
			return BgL_az00_3396;
		}

	}



/* &node-A-atom1332 */
	obj_t BGl_z62nodezd2Azd2atom1332z62zzintegrate_az00(obj_t BgL_envz00_3397,
		obj_t BgL_nodez00_3398, obj_t BgL_hostz00_3399, obj_t BgL_kz00_3400,
		obj_t BgL_az00_3401)
	{
		{	/* Integrate/a.scm 194 */
			return BgL_az00_3401;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzintegrate_az00(void)
	{
		{	/* Integrate/a.scm 18 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2162z00zzintegrate_az00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2162z00zzintegrate_az00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2162z00zzintegrate_az00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2162z00zzintegrate_az00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2162z00zzintegrate_az00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2162z00zzintegrate_az00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2162z00zzintegrate_az00));
			BGl_modulezd2initializa7ationz75zzast_dumpz00(271707717L,
				BSTRING_TO_STRING(BGl_string2162z00zzintegrate_az00));
			BGl_modulezd2initializa7ationz75zzintegrate_infoz00(0L,
				BSTRING_TO_STRING(BGl_string2162z00zzintegrate_az00));
			BGl_modulezd2initializa7ationz75zzreturn_walkz00(216820585L,
				BSTRING_TO_STRING(BGl_string2162z00zzintegrate_az00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2162z00zzintegrate_az00));
			return
				BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2162z00zzintegrate_az00));
		}

	}

#ifdef __cplusplus
}
#endif
