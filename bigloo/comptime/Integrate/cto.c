/*===========================================================================*/
/*   (Integrate/cto.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Integrate/cto.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INTEGRATE_CTO_TYPE_DEFINITIONS
#define BGL_INTEGRATE_CTO_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_sfunzf2iinfozf2_bgl
	{
		obj_t BgL_ownerz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
		obj_t BgL_cfromz00;
		obj_t BgL_ctoz00;
		obj_t BgL_kz00;
		obj_t BgL_kza2za2;
		obj_t BgL_uz00;
		obj_t BgL_cnz00;
		obj_t BgL_ctz00;
		obj_t BgL_kontz00;
		bool_t BgL_gzf3zf3;
		bool_t BgL_forcegzf3zf3;
		obj_t BgL_lz00;
		obj_t BgL_ledz00;
		obj_t BgL_istampz00;
		obj_t BgL_globalz00;
		obj_t BgL_kapturedz00;
		obj_t BgL_tailzd2coercionzd2;
		bool_t BgL_xhdlzf3zf3;
		obj_t BgL_xhdlsz00;
	}                      *BgL_sfunzf2iinfozf2_bglt;


#endif													// BGL_INTEGRATE_CTO_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_z62setzd2ctoz12zd2app1277z70zzintegrate_ctoz00(obj_t, obj_t,
		obj_t);
	static obj_t
		BGl_z62setzd2ctoz12zd2setzd2exzd2it1299z70zzintegrate_ctoz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzintegrate_ctoz00 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	static obj_t
		BGl_z62setzd2ctoz12zd2boxzd2setz121305zb0zzintegrate_ctoz00(obj_t, obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_nodez00_bglt, BgL_localz00_bglt);
	static obj_t BGl_z62setzd2ctoz12zd2makezd2box1303za2zzintegrate_ctoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62setzd2ctoz12zd2appzd2ly1279za2zzintegrate_ctoz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_z62setzd2ctoz12zd2atom1265z70zzintegrate_ctoz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzintegrate_ctoz00(void);
	static obj_t BGl_z62setzd2ctoz12zd2boxzd2ref1307za2zzintegrate_ctoz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzintegrate_ctoz00(void);
	static obj_t BGl_objectzd2initzd2zzintegrate_ctoz00(void);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62setzd2ctoz12zd2extern1283z70zzintegrate_ctoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62setzd2ctoz12zd2switch1293z70zzintegrate_ctoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62setzd2ctoz12zd2funcall1281z70zzintegrate_ctoz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62setzd2ctoz12zd2letzd2var1297za2zzintegrate_ctoz00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzintegrate_ctoz00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62setzd2ctoz12zd2closure1271z70zzintegrate_ctoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62setzd2ctoz12zd2kwote1267z70zzintegrate_ctoz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62setzd2ctoz12zd2conditional1289z70zzintegrate_ctoz00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62setzd2ctoz12zd2cast1285z70zzintegrate_ctoz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62setzd2ctoz12zd2sequence1273z70zzintegrate_ctoz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_ctoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_infoz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_z62setzd2ctoz12zd2setq1287z70zzintegrate_ctoz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62setzd2ctoz12zd2sync1275z70zzintegrate_ctoz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62setzd2ctoz12zd2fail1291z70zzintegrate_ctoz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzintegrate_ctoz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_ctoz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzintegrate_ctoz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzintegrate_ctoz00(void);
	static obj_t BGl_z62setzd2ctoz121262za2zzintegrate_ctoz00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62setzd2ctoz12za2zzintegrate_ctoz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62setzd2ctoz12zd2var1269z70zzintegrate_ctoz00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62setzd2ctoz12zd2letzd2fun1295za2zzintegrate_ctoz00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static obj_t
		BGl_z62setzd2ctoz12zd2jumpzd2exzd2it1301z70zzintegrate_ctoz00(obj_t, obj_t,
		obj_t);
	static obj_t __cnst[1];


	   
		 
		DEFINE_EXPORT_BGL_GENERIC(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71799z00,
		BGl_z62setzd2ctoz12za2zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1749z00zzintegrate_ctoz00,
		BgL_bgl_string1749za700za7za7i1800za7, "set-cto!1262", 12);
	      DEFINE_STRING(BGl_string1750z00zzintegrate_ctoz00,
		BgL_bgl_string1750za700za7za7i1801za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1752z00zzintegrate_ctoz00,
		BgL_bgl_string1752za700za7za7i1802za7, "set-cto!", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1748z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza71211803za7,
		BGl_z62setzd2ctoz121262za2zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1751z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71804z00,
		BGl_z62setzd2ctoz12zd2atom1265z70zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1753z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71805z00,
		BGl_z62setzd2ctoz12zd2kwote1267z70zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1754z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71806z00,
		BGl_z62setzd2ctoz12zd2var1269z70zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1755z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71807z00,
		BGl_z62setzd2ctoz12zd2closure1271z70zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1756z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71808z00,
		BGl_z62setzd2ctoz12zd2sequence1273z70zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1757z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71809z00,
		BGl_z62setzd2ctoz12zd2sync1275z70zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1758z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71810z00,
		BGl_z62setzd2ctoz12zd2app1277z70zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1759z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71811z00,
		BGl_z62setzd2ctoz12zd2appzd2ly1279za2zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1760z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71812z00,
		BGl_z62setzd2ctoz12zd2funcall1281z70zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1761z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71813z00,
		BGl_z62setzd2ctoz12zd2extern1283z70zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1762z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71814z00,
		BGl_z62setzd2ctoz12zd2cast1285z70zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1763z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71815z00,
		BGl_z62setzd2ctoz12zd2setq1287z70zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1764z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71816z00,
		BGl_z62setzd2ctoz12zd2conditional1289z70zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1765z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71817z00,
		BGl_z62setzd2ctoz12zd2fail1291z70zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1766z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71818z00,
		BGl_z62setzd2ctoz12zd2switch1293z70zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1767z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71819z00,
		BGl_z62setzd2ctoz12zd2letzd2fun1295za2zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1774z00zzintegrate_ctoz00,
		BgL_bgl_string1774za700za7za7i1820za7, "Unexpected closure", 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1768z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71821z00,
		BGl_z62setzd2ctoz12zd2letzd2var1297za2zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1775z00zzintegrate_ctoz00,
		BgL_bgl_string1775za700za7za7i1822za7, "integrate_cto", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1769z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71823z00,
		BGl_z62setzd2ctoz12zd2setzd2exzd2it1299z70zzintegrate_ctoz00, 0L, BUNSPEC,
		2);
	      DEFINE_STRING(BGl_string1776z00zzintegrate_ctoz00,
		BgL_bgl_string1776za700za7za7i1824za7, "set-cto!1262 ", 13);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1770z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71825z00,
		BGl_z62setzd2ctoz12zd2jumpzd2exzd2it1301z70zzintegrate_ctoz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1771z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71826z00,
		BGl_z62setzd2ctoz12zd2makezd2box1303za2zzintegrate_ctoz00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1772z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71827z00,
		BGl_z62setzd2ctoz12zd2boxzd2setz121305zb0zzintegrate_ctoz00, 0L, BUNSPEC,
		2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1773z00zzintegrate_ctoz00,
		BgL_bgl_za762setza7d2ctoza712za71828z00,
		BGl_z62setzd2ctoz12zd2boxzd2ref1307za2zzintegrate_ctoz00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzintegrate_ctoz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzintegrate_ctoz00(long
		BgL_checksumz00_2461, char *BgL_fromz00_2462)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzintegrate_ctoz00))
				{
					BGl_requirezd2initializa7ationz75zzintegrate_ctoz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzintegrate_ctoz00();
					BGl_libraryzd2moduleszd2initz00zzintegrate_ctoz00();
					BGl_cnstzd2initzd2zzintegrate_ctoz00();
					BGl_importedzd2moduleszd2initz00zzintegrate_ctoz00();
					BGl_genericzd2initzd2zzintegrate_ctoz00();
					BGl_methodzd2initzd2zzintegrate_ctoz00();
					return BGl_toplevelzd2initzd2zzintegrate_ctoz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_ctoz00(void)
	{
		{	/* Integrate/cto.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "integrate_cto");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"integrate_cto");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "integrate_cto");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"integrate_cto");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "integrate_cto");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"integrate_cto");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"integrate_cto");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"integrate_cto");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "integrate_cto");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"integrate_cto");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzintegrate_ctoz00(void)
	{
		{	/* Integrate/cto.scm 15 */
			{	/* Integrate/cto.scm 15 */
				obj_t BgL_cportz00_2326;

				{	/* Integrate/cto.scm 15 */
					obj_t BgL_stringz00_2333;

					BgL_stringz00_2333 = BGl_string1776z00zzintegrate_ctoz00;
					{	/* Integrate/cto.scm 15 */
						obj_t BgL_startz00_2334;

						BgL_startz00_2334 = BINT(0L);
						{	/* Integrate/cto.scm 15 */
							obj_t BgL_endz00_2335;

							BgL_endz00_2335 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2333)));
							{	/* Integrate/cto.scm 15 */

								BgL_cportz00_2326 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2333, BgL_startz00_2334, BgL_endz00_2335);
				}}}}
				{
					long BgL_iz00_2327;

					BgL_iz00_2327 = 0L;
				BgL_loopz00_2328:
					if ((BgL_iz00_2327 == -1L))
						{	/* Integrate/cto.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Integrate/cto.scm 15 */
							{	/* Integrate/cto.scm 15 */
								obj_t BgL_arg1798z00_2329;

								{	/* Integrate/cto.scm 15 */

									{	/* Integrate/cto.scm 15 */
										obj_t BgL_locationz00_2331;

										BgL_locationz00_2331 = BBOOL(((bool_t) 0));
										{	/* Integrate/cto.scm 15 */

											BgL_arg1798z00_2329 =
												BGl_readz00zz__readerz00(BgL_cportz00_2326,
												BgL_locationz00_2331);
										}
									}
								}
								{	/* Integrate/cto.scm 15 */
									int BgL_tmpz00_2492;

									BgL_tmpz00_2492 = (int) (BgL_iz00_2327);
									CNST_TABLE_SET(BgL_tmpz00_2492, BgL_arg1798z00_2329);
							}}
							{	/* Integrate/cto.scm 15 */
								int BgL_auxz00_2332;

								BgL_auxz00_2332 = (int) ((BgL_iz00_2327 - 1L));
								{
									long BgL_iz00_2497;

									BgL_iz00_2497 = (long) (BgL_auxz00_2332);
									BgL_iz00_2327 = BgL_iz00_2497;
									goto BgL_loopz00_2328;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzintegrate_ctoz00(void)
	{
		{	/* Integrate/cto.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzintegrate_ctoz00(void)
	{
		{	/* Integrate/cto.scm 15 */
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzintegrate_ctoz00(void)
	{
		{	/* Integrate/cto.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzintegrate_ctoz00(void)
	{
		{	/* Integrate/cto.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
				BGl_proc1748z00zzintegrate_ctoz00, BGl_nodez00zzast_nodez00,
				BGl_string1749z00zzintegrate_ctoz00);
		}

	}



/* &set-cto!1262 */
	obj_t BGl_z62setzd2ctoz121262za2zzintegrate_ctoz00(obj_t BgL_envz00_2232,
		obj_t BgL_nodez00_2233, obj_t BgL_localz00_2234)
	{
		{	/* Integrate/cto.scm 28 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
				BGl_string1750z00zzintegrate_ctoz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2233)));
		}

	}



/* set-cto! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_nodez00_bglt
		BgL_nodez00_3, BgL_localz00_bglt BgL_localz00_4)
	{
		{	/* Integrate/cto.scm 28 */
			{	/* Integrate/cto.scm 28 */
				obj_t BgL_method1263z00_1618;

				{	/* Integrate/cto.scm 28 */
					obj_t BgL_res1744z00_2067;

					{	/* Integrate/cto.scm 28 */
						long BgL_objzd2classzd2numz00_2038;

						BgL_objzd2classzd2numz00_2038 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_3));
						{	/* Integrate/cto.scm 28 */
							obj_t BgL_arg1811z00_2039;

							BgL_arg1811z00_2039 =
								PROCEDURE_REF(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
								(int) (1L));
							{	/* Integrate/cto.scm 28 */
								int BgL_offsetz00_2042;

								BgL_offsetz00_2042 = (int) (BgL_objzd2classzd2numz00_2038);
								{	/* Integrate/cto.scm 28 */
									long BgL_offsetz00_2043;

									BgL_offsetz00_2043 =
										((long) (BgL_offsetz00_2042) - OBJECT_TYPE);
									{	/* Integrate/cto.scm 28 */
										long BgL_modz00_2044;

										BgL_modz00_2044 =
											(BgL_offsetz00_2043 >> (int) ((long) ((int) (4L))));
										{	/* Integrate/cto.scm 28 */
											long BgL_restz00_2046;

											BgL_restz00_2046 =
												(BgL_offsetz00_2043 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Integrate/cto.scm 28 */

												{	/* Integrate/cto.scm 28 */
													obj_t BgL_bucketz00_2048;

													BgL_bucketz00_2048 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2039), BgL_modz00_2044);
													BgL_res1744z00_2067 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2048), BgL_restz00_2046);
					}}}}}}}}
					BgL_method1263z00_1618 = BgL_res1744z00_2067;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1263z00_1618,
					((obj_t) BgL_nodez00_3), ((obj_t) BgL_localz00_4));
			}
		}

	}



/* &set-cto! */
	obj_t BGl_z62setzd2ctoz12za2zzintegrate_ctoz00(obj_t BgL_envz00_2235,
		obj_t BgL_nodez00_2236, obj_t BgL_localz00_2237)
	{
		{	/* Integrate/cto.scm 28 */
			return
				BGl_setzd2ctoz12zc0zzintegrate_ctoz00(
				((BgL_nodez00_bglt) BgL_nodez00_2236),
				((BgL_localz00_bglt) BgL_localz00_2237));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzintegrate_ctoz00(void)
	{
		{	/* Integrate/cto.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00, BGl_atomz00zzast_nodez00,
				BGl_proc1751z00zzintegrate_ctoz00, BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00, BGl_kwotez00zzast_nodez00,
				BGl_proc1753z00zzintegrate_ctoz00, BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00, BGl_varz00zzast_nodez00,
				BGl_proc1754z00zzintegrate_ctoz00, BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
				BGl_closurez00zzast_nodez00, BGl_proc1755z00zzintegrate_ctoz00,
				BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
				BGl_sequencez00zzast_nodez00, BGl_proc1756z00zzintegrate_ctoz00,
				BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00, BGl_syncz00zzast_nodez00,
				BGl_proc1757z00zzintegrate_ctoz00, BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00, BGl_appz00zzast_nodez00,
				BGl_proc1758z00zzintegrate_ctoz00, BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1759z00zzintegrate_ctoz00,
				BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
				BGl_funcallz00zzast_nodez00, BGl_proc1760z00zzintegrate_ctoz00,
				BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
				BGl_externz00zzast_nodez00, BGl_proc1761z00zzintegrate_ctoz00,
				BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00, BGl_castz00zzast_nodez00,
				BGl_proc1762z00zzintegrate_ctoz00, BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00, BGl_setqz00zzast_nodez00,
				BGl_proc1763z00zzintegrate_ctoz00, BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1764z00zzintegrate_ctoz00,
				BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00, BGl_failz00zzast_nodez00,
				BGl_proc1765z00zzintegrate_ctoz00, BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
				BGl_switchz00zzast_nodez00, BGl_proc1766z00zzintegrate_ctoz00,
				BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1767z00zzintegrate_ctoz00,
				BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1768z00zzintegrate_ctoz00,
				BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1769z00zzintegrate_ctoz00,
				BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1770z00zzintegrate_ctoz00,
				BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1771z00zzintegrate_ctoz00,
				BGl_string1752z00zzintegrate_ctoz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1772z00zzintegrate_ctoz00,
				BGl_string1752z00zzintegrate_ctoz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_setzd2ctoz12zd2envz12zzintegrate_ctoz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1773z00zzintegrate_ctoz00,
				BGl_string1752z00zzintegrate_ctoz00);
		}

	}



/* &set-cto!-box-ref1307 */
	obj_t BGl_z62setzd2ctoz12zd2boxzd2ref1307za2zzintegrate_ctoz00(obj_t
		BgL_envz00_2260, obj_t BgL_nodez00_2261, obj_t BgL_localz00_2262)
	{
		{	/* Integrate/cto.scm 233 */
			{	/* Integrate/cto.scm 235 */
				BgL_varz00_bglt BgL_arg1573z00_2340;

				BgL_arg1573z00_2340 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_2261)))->BgL_varz00);
				return
					BGl_setzd2ctoz12zc0zzintegrate_ctoz00(
					((BgL_nodez00_bglt) BgL_arg1573z00_2340),
					((BgL_localz00_bglt) BgL_localz00_2262));
			}
		}

	}



/* &set-cto!-box-set!1305 */
	obj_t BGl_z62setzd2ctoz12zd2boxzd2setz121305zb0zzintegrate_ctoz00(obj_t
		BgL_envz00_2263, obj_t BgL_nodez00_2264, obj_t BgL_localz00_2265)
	{
		{	/* Integrate/cto.scm 225 */
			{	/* Integrate/cto.scm 227 */
				BgL_varz00_bglt BgL_arg1565z00_2342;

				BgL_arg1565z00_2342 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2264)))->BgL_varz00);
				BGl_setzd2ctoz12zc0zzintegrate_ctoz00(
					((BgL_nodez00_bglt) BgL_arg1565z00_2342),
					((BgL_localz00_bglt) BgL_localz00_2265));
			}
			{	/* Integrate/cto.scm 228 */
				BgL_nodez00_bglt BgL_arg1571z00_2343;

				BgL_arg1571z00_2343 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2264)))->BgL_valuez00);
				return
					BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1571z00_2343,
					((BgL_localz00_bglt) BgL_localz00_2265));
			}
		}

	}



/* &set-cto!-make-box1303 */
	obj_t BGl_z62setzd2ctoz12zd2makezd2box1303za2zzintegrate_ctoz00(obj_t
		BgL_envz00_2266, obj_t BgL_nodez00_2267, obj_t BgL_localz00_2268)
	{
		{	/* Integrate/cto.scm 218 */
			{	/* Integrate/cto.scm 220 */
				BgL_nodez00_bglt BgL_arg1564z00_2345;

				BgL_arg1564z00_2345 =
					(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2267)))->BgL_valuez00);
				return
					BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1564z00_2345,
					((BgL_localz00_bglt) BgL_localz00_2268));
			}
		}

	}



/* &set-cto!-jump-ex-it1301 */
	obj_t BGl_z62setzd2ctoz12zd2jumpzd2exzd2it1301z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2269, obj_t BgL_nodez00_2270, obj_t BgL_localz00_2271)
	{
		{	/* Integrate/cto.scm 210 */
			{	/* Integrate/cto.scm 212 */
				BgL_nodez00_bglt BgL_arg1559z00_2347;

				BgL_arg1559z00_2347 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2270)))->BgL_exitz00);
				BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1559z00_2347,
					((BgL_localz00_bglt) BgL_localz00_2271));
			}
			{	/* Integrate/cto.scm 213 */
				BgL_nodez00_bglt BgL_arg1561z00_2348;

				BgL_arg1561z00_2348 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2270)))->BgL_valuez00);
				return
					BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1561z00_2348,
					((BgL_localz00_bglt) BgL_localz00_2271));
			}
		}

	}



/* &set-cto!-set-ex-it1299 */
	obj_t BGl_z62setzd2ctoz12zd2setzd2exzd2it1299z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2272, obj_t BgL_nodez00_2273, obj_t BgL_localz00_2274)
	{
		{	/* Integrate/cto.scm 202 */
			{	/* Integrate/cto.scm 204 */
				BgL_nodez00_bglt BgL_arg1552z00_2350;

				BgL_arg1552z00_2350 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2273)))->BgL_onexitz00);
				BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1552z00_2350,
					((BgL_localz00_bglt) BgL_localz00_2274));
			}
			{	/* Integrate/cto.scm 205 */
				BgL_nodez00_bglt BgL_arg1553z00_2351;

				BgL_arg1553z00_2351 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2273)))->BgL_bodyz00);
				return
					BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1553z00_2351,
					((BgL_localz00_bglt) BgL_localz00_2274));
			}
		}

	}



/* &set-cto!-let-var1297 */
	obj_t BGl_z62setzd2ctoz12zd2letzd2var1297za2zzintegrate_ctoz00(obj_t
		BgL_envz00_2275, obj_t BgL_nodez00_2276, obj_t BgL_localz00_2277)
	{
		{	/* Integrate/cto.scm 190 */
			{	/* Integrate/cto.scm 192 */
				obj_t BgL_g1124z00_2353;

				BgL_g1124z00_2353 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2276)))->BgL_bindingsz00);
				{
					obj_t BgL_bindingsz00_2355;

					BgL_bindingsz00_2355 = BgL_g1124z00_2353;
				BgL_liipz00_2354:
					if (NULLP(BgL_bindingsz00_2355))
						{	/* Integrate/cto.scm 194 */
							BgL_nodez00_bglt BgL_arg1535z00_2356;

							BgL_arg1535z00_2356 =
								(((BgL_letzd2varzd2_bglt) COBJECT(
										((BgL_letzd2varzd2_bglt) BgL_nodez00_2276)))->BgL_bodyz00);
							return
								BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1535z00_2356,
								((BgL_localz00_bglt) BgL_localz00_2277));
						}
					else
						{	/* Integrate/cto.scm 193 */
							{	/* Integrate/cto.scm 196 */
								obj_t BgL_arg1540z00_2357;

								{	/* Integrate/cto.scm 196 */
									obj_t BgL_pairz00_2358;

									BgL_pairz00_2358 = CAR(((obj_t) BgL_bindingsz00_2355));
									BgL_arg1540z00_2357 = CDR(BgL_pairz00_2358);
								}
								BGl_setzd2ctoz12zc0zzintegrate_ctoz00(
									((BgL_nodez00_bglt) BgL_arg1540z00_2357),
									((BgL_localz00_bglt) BgL_localz00_2277));
							}
							{	/* Integrate/cto.scm 197 */
								obj_t BgL_arg1546z00_2359;

								BgL_arg1546z00_2359 = CDR(((obj_t) BgL_bindingsz00_2355));
								{
									obj_t BgL_bindingsz00_2612;

									BgL_bindingsz00_2612 = BgL_arg1546z00_2359;
									BgL_bindingsz00_2355 = BgL_bindingsz00_2612;
									goto BgL_liipz00_2354;
								}
							}
						}
				}
			}
		}

	}



/* &set-cto!-let-fun1295 */
	obj_t BGl_z62setzd2ctoz12zd2letzd2fun1295za2zzintegrate_ctoz00(obj_t
		BgL_envz00_2278, obj_t BgL_nodez00_2279, obj_t BgL_localz00_2280)
	{
		{	/* Integrate/cto.scm 175 */
			{	/* Integrate/cto.scm 177 */
				obj_t BgL_g1122z00_2361;

				BgL_g1122z00_2361 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2279)))->BgL_localsz00);
				{
					obj_t BgL_localsz00_2363;

					BgL_localsz00_2363 = BgL_g1122z00_2361;
				BgL_liipz00_2362:
					if (NULLP(BgL_localsz00_2363))
						{	/* Integrate/cto.scm 179 */
							BgL_nodez00_bglt BgL_arg1513z00_2364;

							BgL_arg1513z00_2364 =
								(((BgL_letzd2funzd2_bglt) COBJECT(
										((BgL_letzd2funzd2_bglt) BgL_nodez00_2279)))->BgL_bodyz00);
							return
								BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1513z00_2364,
								((BgL_localz00_bglt) BgL_localz00_2280));
						}
					else
						{	/* Integrate/cto.scm 180 */
							obj_t BgL_llocalz00_2365;

							BgL_llocalz00_2365 = CAR(((obj_t) BgL_localsz00_2363));
							{	/* Integrate/cto.scm 180 */
								BgL_valuez00_bglt BgL_funz00_2366;

								BgL_funz00_2366 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_localz00_bglt) BgL_llocalz00_2365))))->
									BgL_valuez00);
								{	/* Integrate/cto.scm 182 */

									{	/* Integrate/cto.scm 184 */
										obj_t BgL_arg1514z00_2367;

										BgL_arg1514z00_2367 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_2366)))->BgL_bodyz00);
										BGl_setzd2ctoz12zc0zzintegrate_ctoz00(
											((BgL_nodez00_bglt) BgL_arg1514z00_2367),
											((BgL_localz00_bglt) BgL_localz00_2280));
									}
									{	/* Integrate/cto.scm 185 */
										obj_t BgL_arg1516z00_2368;

										BgL_arg1516z00_2368 = CDR(((obj_t) BgL_localsz00_2363));
										{
											obj_t BgL_localsz00_2633;

											BgL_localsz00_2633 = BgL_arg1516z00_2368;
											BgL_localsz00_2363 = BgL_localsz00_2633;
											goto BgL_liipz00_2362;
										}
									}
								}
							}
						}
				}
			}
		}

	}



/* &set-cto!-switch1293 */
	obj_t BGl_z62setzd2ctoz12zd2switch1293z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2281, obj_t BgL_nodez00_2282, obj_t BgL_localz00_2283)
	{
		{	/* Integrate/cto.scm 163 */
			{	/* Integrate/cto.scm 165 */
				obj_t BgL_g1120z00_2370;

				BgL_g1120z00_2370 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2282)))->BgL_clausesz00);
				{
					obj_t BgL_clausesz00_2372;

					BgL_clausesz00_2372 = BgL_g1120z00_2370;
				BgL_liipz00_2371:
					if (NULLP(BgL_clausesz00_2372))
						{	/* Integrate/cto.scm 167 */
							BgL_nodez00_bglt BgL_arg1485z00_2373;

							BgL_arg1485z00_2373 =
								(((BgL_switchz00_bglt) COBJECT(
										((BgL_switchz00_bglt) BgL_nodez00_2282)))->BgL_testz00);
							return
								BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1485z00_2373,
								((BgL_localz00_bglt) BgL_localz00_2283));
						}
					else
						{	/* Integrate/cto.scm 166 */
							{	/* Integrate/cto.scm 169 */
								obj_t BgL_arg1489z00_2374;

								{	/* Integrate/cto.scm 169 */
									obj_t BgL_pairz00_2375;

									BgL_pairz00_2375 = CAR(((obj_t) BgL_clausesz00_2372));
									BgL_arg1489z00_2374 = CDR(BgL_pairz00_2375);
								}
								BGl_setzd2ctoz12zc0zzintegrate_ctoz00(
									((BgL_nodez00_bglt) BgL_arg1489z00_2374),
									((BgL_localz00_bglt) BgL_localz00_2283));
							}
							{	/* Integrate/cto.scm 170 */
								obj_t BgL_arg1509z00_2376;

								BgL_arg1509z00_2376 = CDR(((obj_t) BgL_clausesz00_2372));
								{
									obj_t BgL_clausesz00_2650;

									BgL_clausesz00_2650 = BgL_arg1509z00_2376;
									BgL_clausesz00_2372 = BgL_clausesz00_2650;
									goto BgL_liipz00_2371;
								}
							}
						}
				}
			}
		}

	}



/* &set-cto!-fail1291 */
	obj_t BGl_z62setzd2ctoz12zd2fail1291z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2284, obj_t BgL_nodez00_2285, obj_t BgL_localz00_2286)
	{
		{	/* Integrate/cto.scm 154 */
			{	/* Integrate/cto.scm 156 */
				BgL_nodez00_bglt BgL_arg1453z00_2378;

				BgL_arg1453z00_2378 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2285)))->BgL_procz00);
				BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1453z00_2378,
					((BgL_localz00_bglt) BgL_localz00_2286));
			}
			{	/* Integrate/cto.scm 157 */
				BgL_nodez00_bglt BgL_arg1454z00_2379;

				BgL_arg1454z00_2379 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2285)))->BgL_msgz00);
				BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1454z00_2379,
					((BgL_localz00_bglt) BgL_localz00_2286));
			}
			{	/* Integrate/cto.scm 158 */
				BgL_nodez00_bglt BgL_arg1472z00_2380;

				BgL_arg1472z00_2380 =
					(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2285)))->BgL_objz00);
				return
					BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1472z00_2380,
					((BgL_localz00_bglt) BgL_localz00_2286));
			}
		}

	}



/* &set-cto!-conditional1289 */
	obj_t BGl_z62setzd2ctoz12zd2conditional1289z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2287, obj_t BgL_nodez00_2288, obj_t BgL_localz00_2289)
	{
		{	/* Integrate/cto.scm 145 */
			{	/* Integrate/cto.scm 147 */
				BgL_nodez00_bglt BgL_arg1434z00_2382;

				BgL_arg1434z00_2382 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2288)))->BgL_testz00);
				BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1434z00_2382,
					((BgL_localz00_bglt) BgL_localz00_2289));
			}
			{	/* Integrate/cto.scm 148 */
				BgL_nodez00_bglt BgL_arg1437z00_2383;

				BgL_arg1437z00_2383 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2288)))->BgL_truez00);
				BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1437z00_2383,
					((BgL_localz00_bglt) BgL_localz00_2289));
			}
			{	/* Integrate/cto.scm 149 */
				BgL_nodez00_bglt BgL_arg1448z00_2384;

				BgL_arg1448z00_2384 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2288)))->BgL_falsez00);
				return
					BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1448z00_2384,
					((BgL_localz00_bglt) BgL_localz00_2289));
			}
		}

	}



/* &set-cto!-setq1287 */
	obj_t BGl_z62setzd2ctoz12zd2setq1287z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2290, obj_t BgL_nodez00_2291, obj_t BgL_localz00_2292)
	{
		{	/* Integrate/cto.scm 137 */
			{	/* Integrate/cto.scm 139 */
				BgL_varz00_bglt BgL_arg1421z00_2386;

				BgL_arg1421z00_2386 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2291)))->BgL_varz00);
				BGl_setzd2ctoz12zc0zzintegrate_ctoz00(
					((BgL_nodez00_bglt) BgL_arg1421z00_2386),
					((BgL_localz00_bglt) BgL_localz00_2292));
			}
			{	/* Integrate/cto.scm 140 */
				BgL_nodez00_bglt BgL_arg1422z00_2387;

				BgL_arg1422z00_2387 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2291)))->BgL_valuez00);
				return
					BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1422z00_2387,
					((BgL_localz00_bglt) BgL_localz00_2292));
			}
		}

	}



/* &set-cto!-cast1285 */
	obj_t BGl_z62setzd2ctoz12zd2cast1285z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2293, obj_t BgL_nodez00_2294, obj_t BgL_localz00_2295)
	{
		{	/* Integrate/cto.scm 130 */
			{	/* Integrate/cto.scm 132 */
				BgL_nodez00_bglt BgL_arg1410z00_2389;

				BgL_arg1410z00_2389 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2294)))->BgL_argz00);
				return
					BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1410z00_2389,
					((BgL_localz00_bglt) BgL_localz00_2295));
			}
		}

	}



/* &set-cto!-extern1283 */
	obj_t BGl_z62setzd2ctoz12zd2extern1283z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2296, obj_t BgL_nodez00_2297, obj_t BgL_localz00_2298)
	{
		{	/* Integrate/cto.scm 123 */
			{	/* Integrate/cto.scm 125 */
				obj_t BgL_g1261z00_2391;

				BgL_g1261z00_2391 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2297)))->BgL_exprza2za2);
				{
					obj_t BgL_l1259z00_2393;

					{	/* Integrate/cto.scm 125 */
						bool_t BgL_tmpz00_2690;

						BgL_l1259z00_2393 = BgL_g1261z00_2391;
					BgL_zc3z04anonymousza31378ze3z87_2392:
						if (PAIRP(BgL_l1259z00_2393))
							{	/* Integrate/cto.scm 125 */
								{	/* Integrate/cto.scm 125 */
									obj_t BgL_nodez00_2394;

									BgL_nodez00_2394 = CAR(BgL_l1259z00_2393);
									BGl_setzd2ctoz12zc0zzintegrate_ctoz00(
										((BgL_nodez00_bglt) BgL_nodez00_2394),
										((BgL_localz00_bglt) BgL_localz00_2298));
								}
								{
									obj_t BgL_l1259z00_2697;

									BgL_l1259z00_2697 = CDR(BgL_l1259z00_2393);
									BgL_l1259z00_2393 = BgL_l1259z00_2697;
									goto BgL_zc3z04anonymousza31378ze3z87_2392;
								}
							}
						else
							{	/* Integrate/cto.scm 125 */
								BgL_tmpz00_2690 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_2690);
					}
				}
			}
		}

	}



/* &set-cto!-funcall1281 */
	obj_t BGl_z62setzd2ctoz12zd2funcall1281z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2299, obj_t BgL_nodez00_2300, obj_t BgL_localz00_2301)
	{
		{	/* Integrate/cto.scm 111 */
			{
				obj_t BgL_astsz00_2397;

				BgL_astsz00_2397 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2300)))->BgL_argsz00);
			BgL_liipz00_2396:
				if (NULLP(BgL_astsz00_2397))
					{	/* Integrate/cto.scm 115 */
						BgL_nodez00_bglt BgL_arg1375z00_2398;

						BgL_arg1375z00_2398 =
							(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nodez00_2300)))->BgL_funz00);
						return
							BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1375z00_2398,
							((BgL_localz00_bglt) BgL_localz00_2301));
					}
				else
					{	/* Integrate/cto.scm 114 */
						{	/* Integrate/cto.scm 117 */
							obj_t BgL_arg1376z00_2399;

							BgL_arg1376z00_2399 = CAR(((obj_t) BgL_astsz00_2397));
							BGl_setzd2ctoz12zc0zzintegrate_ctoz00(
								((BgL_nodez00_bglt) BgL_arg1376z00_2399),
								((BgL_localz00_bglt) BgL_localz00_2301));
						}
						{	/* Integrate/cto.scm 118 */
							obj_t BgL_arg1377z00_2400;

							BgL_arg1377z00_2400 = CDR(((obj_t) BgL_astsz00_2397));
							{
								obj_t BgL_astsz00_2713;

								BgL_astsz00_2713 = BgL_arg1377z00_2400;
								BgL_astsz00_2397 = BgL_astsz00_2713;
								goto BgL_liipz00_2396;
							}
						}
					}
			}
		}

	}



/* &set-cto!-app-ly1279 */
	obj_t BGl_z62setzd2ctoz12zd2appzd2ly1279za2zzintegrate_ctoz00(obj_t
		BgL_envz00_2302, obj_t BgL_nodez00_2303, obj_t BgL_localz00_2304)
	{
		{	/* Integrate/cto.scm 103 */
			{	/* Integrate/cto.scm 105 */
				BgL_nodez00_bglt BgL_arg1367z00_2402;

				BgL_arg1367z00_2402 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2303)))->BgL_funz00);
				BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1367z00_2402,
					((BgL_localz00_bglt) BgL_localz00_2304));
			}
			{	/* Integrate/cto.scm 106 */
				BgL_nodez00_bglt BgL_arg1370z00_2403;

				BgL_arg1370z00_2403 =
					(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2303)))->BgL_argz00);
				return
					BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1370z00_2403,
					((BgL_localz00_bglt) BgL_localz00_2304));
			}
		}

	}



/* &set-cto!-app1277 */
	obj_t BGl_z62setzd2ctoz12zd2app1277z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2305, obj_t BgL_nodez00_2306, obj_t BgL_localz00_2307)
	{
		{	/* Integrate/cto.scm 73 */
			{	/* Integrate/cto.scm 77 */
				BgL_variablez00_bglt BgL_funz00_2405;

				BgL_funz00_2405 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_2306)))->BgL_funz00)))->
					BgL_variablez00);
				{	/* Integrate/cto.scm 78 */
					bool_t BgL_test1837z00_2727;

					{	/* Integrate/cto.scm 78 */
						bool_t BgL_test1838z00_2728;

						{	/* Integrate/cto.scm 78 */
							obj_t BgL_classz00_2406;

							BgL_classz00_2406 = BGl_localz00zzast_varz00;
							{	/* Integrate/cto.scm 78 */
								BgL_objectz00_bglt BgL_arg1807z00_2407;

								{	/* Integrate/cto.scm 78 */
									obj_t BgL_tmpz00_2729;

									BgL_tmpz00_2729 =
										((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2405));
									BgL_arg1807z00_2407 = (BgL_objectz00_bglt) (BgL_tmpz00_2729);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Integrate/cto.scm 78 */
										long BgL_idxz00_2408;

										BgL_idxz00_2408 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2407);
										BgL_test1838z00_2728 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2408 + 2L)) == BgL_classz00_2406);
									}
								else
									{	/* Integrate/cto.scm 78 */
										bool_t BgL_res1745z00_2411;

										{	/* Integrate/cto.scm 78 */
											obj_t BgL_oclassz00_2412;

											{	/* Integrate/cto.scm 78 */
												obj_t BgL_arg1815z00_2413;
												long BgL_arg1816z00_2414;

												BgL_arg1815z00_2413 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Integrate/cto.scm 78 */
													long BgL_arg1817z00_2415;

													BgL_arg1817z00_2415 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2407);
													BgL_arg1816z00_2414 =
														(BgL_arg1817z00_2415 - OBJECT_TYPE);
												}
												BgL_oclassz00_2412 =
													VECTOR_REF(BgL_arg1815z00_2413, BgL_arg1816z00_2414);
											}
											{	/* Integrate/cto.scm 78 */
												bool_t BgL__ortest_1115z00_2416;

												BgL__ortest_1115z00_2416 =
													(BgL_classz00_2406 == BgL_oclassz00_2412);
												if (BgL__ortest_1115z00_2416)
													{	/* Integrate/cto.scm 78 */
														BgL_res1745z00_2411 = BgL__ortest_1115z00_2416;
													}
												else
													{	/* Integrate/cto.scm 78 */
														long BgL_odepthz00_2417;

														{	/* Integrate/cto.scm 78 */
															obj_t BgL_arg1804z00_2418;

															BgL_arg1804z00_2418 = (BgL_oclassz00_2412);
															BgL_odepthz00_2417 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2418);
														}
														if ((2L < BgL_odepthz00_2417))
															{	/* Integrate/cto.scm 78 */
																obj_t BgL_arg1802z00_2419;

																{	/* Integrate/cto.scm 78 */
																	obj_t BgL_arg1803z00_2420;

																	BgL_arg1803z00_2420 = (BgL_oclassz00_2412);
																	BgL_arg1802z00_2419 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2420,
																		2L);
																}
																BgL_res1745z00_2411 =
																	(BgL_arg1802z00_2419 == BgL_classz00_2406);
															}
														else
															{	/* Integrate/cto.scm 78 */
																BgL_res1745z00_2411 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1838z00_2728 = BgL_res1745z00_2411;
									}
							}
						}
						if (BgL_test1838z00_2728)
							{	/* Integrate/cto.scm 78 */
								if ((BgL_localz00_2307 == ((obj_t) BgL_funz00_2405)))
									{	/* Integrate/cto.scm 78 */
										BgL_test1837z00_2727 = ((bool_t) 0);
									}
								else
									{	/* Integrate/cto.scm 78 */
										BgL_test1837z00_2727 = ((bool_t) 1);
									}
							}
						else
							{	/* Integrate/cto.scm 78 */
								BgL_test1837z00_2727 = ((bool_t) 0);
							}
					}
					if (BgL_test1837z00_2727)
						{	/* Integrate/cto.scm 78 */
							BNIL;
						}
					else
						{	/* Integrate/cto.scm 78 */
							BFALSE;
						}
				}
				{	/* Integrate/cto.scm 85 */
					bool_t BgL_test1843z00_2755;

					{	/* Integrate/cto.scm 85 */
						bool_t BgL_test1844z00_2756;

						{	/* Integrate/cto.scm 85 */
							obj_t BgL_classz00_2421;

							BgL_classz00_2421 = BGl_localz00zzast_varz00;
							{	/* Integrate/cto.scm 85 */
								BgL_objectz00_bglt BgL_arg1807z00_2422;

								{	/* Integrate/cto.scm 85 */
									obj_t BgL_tmpz00_2757;

									BgL_tmpz00_2757 =
										((obj_t) ((BgL_objectz00_bglt) BgL_funz00_2405));
									BgL_arg1807z00_2422 = (BgL_objectz00_bglt) (BgL_tmpz00_2757);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Integrate/cto.scm 85 */
										long BgL_idxz00_2423;

										BgL_idxz00_2423 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2422);
										BgL_test1844z00_2756 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_2423 + 2L)) == BgL_classz00_2421);
									}
								else
									{	/* Integrate/cto.scm 85 */
										bool_t BgL_res1746z00_2426;

										{	/* Integrate/cto.scm 85 */
											obj_t BgL_oclassz00_2427;

											{	/* Integrate/cto.scm 85 */
												obj_t BgL_arg1815z00_2428;
												long BgL_arg1816z00_2429;

												BgL_arg1815z00_2428 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Integrate/cto.scm 85 */
													long BgL_arg1817z00_2430;

													BgL_arg1817z00_2430 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2422);
													BgL_arg1816z00_2429 =
														(BgL_arg1817z00_2430 - OBJECT_TYPE);
												}
												BgL_oclassz00_2427 =
													VECTOR_REF(BgL_arg1815z00_2428, BgL_arg1816z00_2429);
											}
											{	/* Integrate/cto.scm 85 */
												bool_t BgL__ortest_1115z00_2431;

												BgL__ortest_1115z00_2431 =
													(BgL_classz00_2421 == BgL_oclassz00_2427);
												if (BgL__ortest_1115z00_2431)
													{	/* Integrate/cto.scm 85 */
														BgL_res1746z00_2426 = BgL__ortest_1115z00_2431;
													}
												else
													{	/* Integrate/cto.scm 85 */
														long BgL_odepthz00_2432;

														{	/* Integrate/cto.scm 85 */
															obj_t BgL_arg1804z00_2433;

															BgL_arg1804z00_2433 = (BgL_oclassz00_2427);
															BgL_odepthz00_2432 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_2433);
														}
														if ((2L < BgL_odepthz00_2432))
															{	/* Integrate/cto.scm 85 */
																obj_t BgL_arg1802z00_2434;

																{	/* Integrate/cto.scm 85 */
																	obj_t BgL_arg1803z00_2435;

																	BgL_arg1803z00_2435 = (BgL_oclassz00_2427);
																	BgL_arg1802z00_2434 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2435,
																		2L);
																}
																BgL_res1746z00_2426 =
																	(BgL_arg1802z00_2434 == BgL_classz00_2421);
															}
														else
															{	/* Integrate/cto.scm 85 */
																BgL_res1746z00_2426 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1844z00_2756 = BgL_res1746z00_2426;
									}
							}
						}
						if (BgL_test1844z00_2756)
							{	/* Integrate/cto.scm 86 */
								bool_t BgL_test1848z00_2780;

								{	/* Integrate/cto.scm 86 */
									BgL_sfunz00_bglt BgL_oz00_2436;

									BgL_oz00_2436 =
										((BgL_sfunz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_funz00_2405))))->
											BgL_valuez00));
									{
										BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2785;

										{
											obj_t BgL_auxz00_2786;

											{	/* Integrate/cto.scm 86 */
												BgL_objectz00_bglt BgL_tmpz00_2787;

												BgL_tmpz00_2787 = ((BgL_objectz00_bglt) BgL_oz00_2436);
												BgL_auxz00_2786 = BGL_OBJECT_WIDENING(BgL_tmpz00_2787);
											}
											BgL_auxz00_2785 =
												((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2786);
										}
										BgL_test1848z00_2780 =
											(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2785))->
											BgL_gzf3zf3);
									}
								}
								if (BgL_test1848z00_2780)
									{	/* Integrate/cto.scm 86 */
										if ((BgL_localz00_2307 == ((obj_t) BgL_funz00_2405)))
											{	/* Integrate/cto.scm 87 */
												BgL_test1843z00_2755 = ((bool_t) 0);
											}
										else
											{	/* Integrate/cto.scm 88 */
												bool_t BgL_test1850z00_2795;

												{	/* Integrate/cto.scm 88 */
													obj_t BgL_arg1343z00_2437;

													{	/* Integrate/cto.scm 88 */
														BgL_sfunz00_bglt BgL_oz00_2438;

														BgL_oz00_2438 =
															((BgL_sfunz00_bglt)
															(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt)
																				BgL_localz00_2307))))->BgL_valuez00));
														{
															BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2800;

															{
																obj_t BgL_auxz00_2801;

																{	/* Integrate/cto.scm 88 */
																	BgL_objectz00_bglt BgL_tmpz00_2802;

																	BgL_tmpz00_2802 =
																		((BgL_objectz00_bglt) BgL_oz00_2438);
																	BgL_auxz00_2801 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_2802);
																}
																BgL_auxz00_2800 =
																	((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2801);
															}
															BgL_arg1343z00_2437 =
																(((BgL_sfunzf2iinfozf2_bglt)
																	COBJECT(BgL_auxz00_2800))->BgL_ctoz00);
														}
													}
													BgL_test1850z00_2795 =
														CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
															((obj_t) BgL_funz00_2405), BgL_arg1343z00_2437));
												}
												if (BgL_test1850z00_2795)
													{	/* Integrate/cto.scm 88 */
														BgL_test1843z00_2755 = ((bool_t) 0);
													}
												else
													{	/* Integrate/cto.scm 88 */
														BgL_test1843z00_2755 = ((bool_t) 1);
													}
											}
									}
								else
									{	/* Integrate/cto.scm 86 */
										BgL_test1843z00_2755 = ((bool_t) 0);
									}
							}
						else
							{	/* Integrate/cto.scm 85 */
								BgL_test1843z00_2755 = ((bool_t) 0);
							}
					}
					if (BgL_test1843z00_2755)
						{	/* Integrate/cto.scm 89 */
							BgL_valuez00_bglt BgL_arg1335z00_2439;
							obj_t BgL_arg1339z00_2440;

							BgL_arg1335z00_2439 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_localz00_bglt) BgL_localz00_2307))))->BgL_valuez00);
							{	/* Integrate/cto.scm 92 */
								obj_t BgL_arg1340z00_2441;

								{	/* Integrate/cto.scm 91 */
									BgL_sfunz00_bglt BgL_oz00_2442;

									BgL_oz00_2442 =
										((BgL_sfunz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_localz00_2307))))->
											BgL_valuez00));
									{
										BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2817;

										{
											obj_t BgL_auxz00_2818;

											{	/* Integrate/cto.scm 91 */
												BgL_objectz00_bglt BgL_tmpz00_2819;

												BgL_tmpz00_2819 = ((BgL_objectz00_bglt) BgL_oz00_2442);
												BgL_auxz00_2818 = BGL_OBJECT_WIDENING(BgL_tmpz00_2819);
											}
											BgL_auxz00_2817 =
												((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2818);
										}
										BgL_arg1340z00_2441 =
											(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2817))->
											BgL_ctoz00);
									}
								}
								BgL_arg1339z00_2440 =
									MAKE_YOUNG_PAIR(
									((obj_t) BgL_funz00_2405), BgL_arg1340z00_2441);
							}
							{
								BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2826;

								{
									obj_t BgL_auxz00_2827;

									{	/* Integrate/cto.scm 89 */
										BgL_objectz00_bglt BgL_tmpz00_2828;

										BgL_tmpz00_2828 =
											((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt) BgL_arg1335z00_2439));
										BgL_auxz00_2827 = BGL_OBJECT_WIDENING(BgL_tmpz00_2828);
									}
									BgL_auxz00_2826 =
										((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2827);
								}
								((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2826))->
										BgL_ctoz00) = ((obj_t) BgL_arg1339z00_2440), BUNSPEC);
							}
						}
					else
						{	/* Integrate/cto.scm 85 */
							BFALSE;
						}
				}
			}
			{
				obj_t BgL_astsz00_2444;

				BgL_astsz00_2444 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2306)))->BgL_argsz00);
			BgL_liipz00_2443:
				if (NULLP(BgL_astsz00_2444))
					{	/* Integrate/cto.scm 94 */
						return BUNSPEC;
					}
				else
					{	/* Integrate/cto.scm 94 */
						{	/* Integrate/cto.scm 97 */
							obj_t BgL_arg1361z00_2445;

							BgL_arg1361z00_2445 = CAR(((obj_t) BgL_astsz00_2444));
							BGl_setzd2ctoz12zc0zzintegrate_ctoz00(
								((BgL_nodez00_bglt) BgL_arg1361z00_2445),
								((BgL_localz00_bglt) BgL_localz00_2307));
						}
						{	/* Integrate/cto.scm 98 */
							obj_t BgL_arg1364z00_2446;

							BgL_arg1364z00_2446 = CDR(((obj_t) BgL_astsz00_2444));
							{
								obj_t BgL_astsz00_2843;

								BgL_astsz00_2843 = BgL_arg1364z00_2446;
								BgL_astsz00_2444 = BgL_astsz00_2843;
								goto BgL_liipz00_2443;
							}
						}
					}
			}
		}

	}



/* &set-cto!-sync1275 */
	obj_t BGl_z62setzd2ctoz12zd2sync1275z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2308, obj_t BgL_nodez00_2309, obj_t BgL_localz00_2310)
	{
		{	/* Integrate/cto.scm 64 */
			{	/* Integrate/cto.scm 66 */
				BgL_nodez00_bglt BgL_arg1318z00_2448;

				BgL_arg1318z00_2448 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2309)))->BgL_mutexz00);
				BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1318z00_2448,
					((BgL_localz00_bglt) BgL_localz00_2310));
			}
			{	/* Integrate/cto.scm 67 */
				BgL_nodez00_bglt BgL_arg1319z00_2449;

				BgL_arg1319z00_2449 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2309)))->BgL_prelockz00);
				BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1319z00_2449,
					((BgL_localz00_bglt) BgL_localz00_2310));
			}
			{	/* Integrate/cto.scm 68 */
				BgL_nodez00_bglt BgL_arg1320z00_2450;

				BgL_arg1320z00_2450 =
					(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2309)))->BgL_bodyz00);
				return
					BGl_setzd2ctoz12zc0zzintegrate_ctoz00(BgL_arg1320z00_2450,
					((BgL_localz00_bglt) BgL_localz00_2310));
			}
		}

	}



/* &set-cto!-sequence1273 */
	obj_t BGl_z62setzd2ctoz12zd2sequence1273z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2311, obj_t BgL_nodez00_2312, obj_t BgL_localz00_2313)
	{
		{	/* Integrate/cto.scm 57 */
			{	/* Integrate/cto.scm 59 */
				obj_t BgL_g1258z00_2452;

				BgL_g1258z00_2452 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2312)))->BgL_nodesz00);
				{
					obj_t BgL_l1256z00_2454;

					{	/* Integrate/cto.scm 59 */
						bool_t BgL_tmpz00_2860;

						BgL_l1256z00_2454 = BgL_g1258z00_2452;
					BgL_zc3z04anonymousza31315ze3z87_2453:
						if (PAIRP(BgL_l1256z00_2454))
							{	/* Integrate/cto.scm 59 */
								{	/* Integrate/cto.scm 59 */
									obj_t BgL_nodez00_2455;

									BgL_nodez00_2455 = CAR(BgL_l1256z00_2454);
									BGl_setzd2ctoz12zc0zzintegrate_ctoz00(
										((BgL_nodez00_bglt) BgL_nodez00_2455),
										((BgL_localz00_bglt) BgL_localz00_2313));
								}
								{
									obj_t BgL_l1256z00_2867;

									BgL_l1256z00_2867 = CDR(BgL_l1256z00_2454);
									BgL_l1256z00_2454 = BgL_l1256z00_2867;
									goto BgL_zc3z04anonymousza31315ze3z87_2453;
								}
							}
						else
							{	/* Integrate/cto.scm 59 */
								BgL_tmpz00_2860 = ((bool_t) 1);
							}
						return BBOOL(BgL_tmpz00_2860);
					}
				}
			}
		}

	}



/* &set-cto!-closure1271 */
	obj_t BGl_z62setzd2ctoz12zd2closure1271z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2314, obj_t BgL_nodez00_2315, obj_t BgL_localz00_2316)
	{
		{	/* Integrate/cto.scm 51 */
			{	/* Integrate/cto.scm 52 */
				obj_t BgL_arg1314z00_2457;

				BgL_arg1314z00_2457 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_2315)));
				return
					BGl_internalzd2errorzd2zztools_errorz00
					(BGl_string1752z00zzintegrate_ctoz00,
					BGl_string1774z00zzintegrate_ctoz00, BgL_arg1314z00_2457);
			}
		}

	}



/* &set-cto!-var1269 */
	obj_t BGl_z62setzd2ctoz12zd2var1269z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2317, obj_t BgL_nodez00_2318, obj_t BgL_localz00_2319)
	{
		{	/* Integrate/cto.scm 45 */
			return BUNSPEC;
		}

	}



/* &set-cto!-kwote1267 */
	obj_t BGl_z62setzd2ctoz12zd2kwote1267z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2320, obj_t BgL_nodez00_2321, obj_t BgL_localz00_2322)
	{
		{	/* Integrate/cto.scm 39 */
			return BUNSPEC;
		}

	}



/* &set-cto!-atom1265 */
	obj_t BGl_z62setzd2ctoz12zd2atom1265z70zzintegrate_ctoz00(obj_t
		BgL_envz00_2323, obj_t BgL_nodez00_2324, obj_t BgL_localz00_2325)
	{
		{	/* Integrate/cto.scm 33 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzintegrate_ctoz00(void)
	{
		{	/* Integrate/cto.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1775z00zzintegrate_ctoz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1775z00zzintegrate_ctoz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1775z00zzintegrate_ctoz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1775z00zzintegrate_ctoz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1775z00zzintegrate_ctoz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1775z00zzintegrate_ctoz00));
			return
				BGl_modulezd2initializa7ationz75zzintegrate_infoz00(0L,
				BSTRING_TO_STRING(BGl_string1775z00zzintegrate_ctoz00));
		}

	}

#ifdef __cplusplus
}
#endif
