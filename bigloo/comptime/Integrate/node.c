/*===========================================================================*/
/*   (Integrate/node.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Integrate/node.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INTEGRATE_NODE_TYPE_DEFINITIONS
#define BGL_INTEGRATE_NODE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_svarzf2iinfozf2_bgl
	{
		obj_t BgL_fzd2markzd2;
		obj_t BgL_uzd2markzd2;
		bool_t BgL_kapturedzf3zf3;
		bool_t BgL_celledzf3zf3;
		obj_t BgL_xhdlz00;
	}                      *BgL_svarzf2iinfozf2_bglt;

	typedef struct BgL_sfunzf2iinfozf2_bgl
	{
		obj_t BgL_ownerz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
		obj_t BgL_cfromz00;
		obj_t BgL_ctoz00;
		obj_t BgL_kz00;
		obj_t BgL_kza2za2;
		obj_t BgL_uz00;
		obj_t BgL_cnz00;
		obj_t BgL_ctz00;
		obj_t BgL_kontz00;
		bool_t BgL_gzf3zf3;
		bool_t BgL_forcegzf3zf3;
		obj_t BgL_lz00;
		obj_t BgL_ledz00;
		obj_t BgL_istampz00;
		obj_t BgL_globalz00;
		obj_t BgL_kapturedz00;
		obj_t BgL_tailzd2coercionzd2;
		bool_t BgL_xhdlzf3zf3;
		obj_t BgL_xhdlsz00;
	}                      *BgL_sfunzf2iinfozf2_bglt;


#endif													// BGL_INTEGRATE_NODE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_nodez00_bglt BGl_z62gloz12zd2app1331za2zzintegrate_nodez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_z62integratezd2celledzf3z43zzintegrate_nodez00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62gloz12z70zzintegrate_nodez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzintegrate_nodez00 = BUNSPEC;
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t
		BGl_thezd2globalzd2zzintegrate_localzd2ze3globalz31(BgL_localz00_bglt);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static BgL_nodez00_bglt BGl_gloz12z12zzintegrate_nodez00(BgL_nodez00_bglt,
		BgL_variablez00_bglt);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzintegrate_nodez00(void);
	static BgL_nodez00_bglt BGl_z62gloz12zd2atom1319za2zzintegrate_nodez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzintegrate_nodez00(void);
	extern obj_t BGl_za2cellza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2conditional1344za2zzintegrate_nodez00(obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzintegrate_nodez00(void);
	extern obj_t BGl_sfunzf2Iinfozf2zzintegrate_infoz00;
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2letzd2fun1350z70zzintegrate_nodez00(obj_t, obj_t, obj_t);
	static BgL_localz00_bglt
		BGl_cellzd2variablezd2zzintegrate_nodez00(BgL_localz00_bglt);
	static obj_t BGl_z62gloz121316z70zzintegrate_nodez00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2appzd2ly1333z70zzintegrate_nodez00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzintegrate_nodez00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzintegrate_nodez00(void);
	static obj_t BGl_z62integratezd2globaliza7ez12z05zzintegrate_nodez00(obj_t,
		obj_t, obj_t, obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	extern BgL_typez00_bglt
		BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_integratezd2globaliza7ez12z67zzintegrate_nodez00(BgL_nodez00_bglt,
		BgL_variablez00_bglt, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2extern1337za2zzintegrate_nodez00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2switch1348za2zzintegrate_nodez00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_cellzd2formalszd2zzintegrate_nodez00(obj_t,
		BgL_nodez00_bglt);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static BgL_nodez00_bglt BGl_z62gloz12zd2kwote1321za2zzintegrate_nodez00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2setzd2exzd2it1354za2zzintegrate_nodez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_nodez00(long, char *);
	extern obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_localzd2ze3globalz31(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_infoz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_localz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_svarzf2Iinfozf2zzintegrate_infoz00;
	extern obj_t BGl_appz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2funcall1335za2zzintegrate_nodez00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62gloz12zd2cast1340za2zzintegrate_nodez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	BGL_EXPORTED_DECL bool_t
		BGl_integratezd2celledzf3z21zzintegrate_nodez00(BgL_localz00_bglt);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2letzd2var1352z70zzintegrate_nodez00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62gloz12zd2setq1342za2zzintegrate_nodez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62gloz12zd2sync1329za2zzintegrate_nodez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_gloza2z12zb0zzintegrate_nodez00(obj_t, obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzintegrate_nodez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_nodez00(void);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2closure1325za2zzintegrate_nodez00(obj_t, obj_t, obj_t);
	extern BgL_localz00_bglt BGl_makezd2localzd2svarz00zzast_localz00(obj_t,
		BgL_typez00_bglt);
	static BgL_nodez00_bglt BGl_z62gloz12zd2fail1346za2zzintegrate_nodez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzintegrate_nodez00(void);
	static obj_t BGl_gczd2rootszd2initz00zzintegrate_nodez00(void);
	static obj_t BGl_celledzd2bindingszd2zzintegrate_nodez00(obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2boxzd2ref1360z70zzintegrate_nodez00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2makezd2box1358z70zzintegrate_nodez00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2boxzd2setz121362z62zzintegrate_nodez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	static BgL_nodez00_bglt BGl_z62gloz12zd2var1323za2zzintegrate_nodez00(obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_globaliza7ezd2localzd2funz12zb5zzintegrate_nodez00(BgL_localz00_bglt,
		BgL_variablez00_bglt);
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2jumpzd2exzd2it1356za2zzintegrate_nodez00(obj_t, obj_t,
		obj_t);
	static BgL_makezd2boxzd2_bglt
		BGl_azd2makezd2cellz00zzintegrate_nodez00(BgL_nodez00_bglt,
		BgL_variablez00_bglt);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	static BgL_nodez00_bglt
		BGl_z62gloz12zd2sequence1327za2zzintegrate_nodez00(obj_t, obj_t, obj_t);
	static obj_t __cnst[5];


	   
		 
		DEFINE_STATIC_BGL_GENERIC(BGl_gloz12zd2envzc0zzintegrate_nodez00,
		BgL_bgl_za762gloza712za770za7za7in1986za7,
		BGl_z62gloz12z70zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1949z00zzintegrate_nodez00,
		BgL_bgl_string1949za700za7za7i1987za7, "glo!1316", 8);
	      DEFINE_STRING(BGl_string1950z00zzintegrate_nodez00,
		BgL_bgl_string1950za700za7za7i1988za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1952z00zzintegrate_nodez00,
		BgL_bgl_string1952za700za7za7i1989za7, "glo!", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1948z00zzintegrate_nodez00,
		BgL_bgl_za762gloza7121316za7701990za7,
		BGl_z62gloz121316z70zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1951z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2atom1991za7,
		BGl_z62gloz12zd2atom1319za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1953z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2kwot1992za7,
		BGl_z62gloz12zd2kwote1321za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1954z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2var11993za7,
		BGl_z62gloz12zd2var1323za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1955z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2clos1994za7,
		BGl_z62gloz12zd2closure1325za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1956z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2sequ1995za7,
		BGl_z62gloz12zd2sequence1327za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1957z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2sync1996za7,
		BGl_z62gloz12zd2sync1329za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1958z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2app11997za7,
		BGl_z62gloz12zd2app1331za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1959z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2appza71998z00,
		BGl_z62gloz12zd2appzd2ly1333z70zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1960z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2func1999za7,
		BGl_z62gloz12zd2funcall1335za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1961z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2exte2000za7,
		BGl_z62gloz12zd2extern1337za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1962z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2cast2001za7,
		BGl_z62gloz12zd2cast1340za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1963z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2setq2002za7,
		BGl_z62gloz12zd2setq1342za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1964z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2cond2003za7,
		BGl_z62gloz12zd2conditional1344za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1965z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2fail2004za7,
		BGl_z62gloz12zd2fail1346za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1966z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2swit2005za7,
		BGl_z62gloz12zd2switch1348za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1967z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2letza72006z00,
		BGl_z62gloz12zd2letzd2fun1350z70zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1974z00zzintegrate_nodez00,
		BgL_bgl_string1974za700za7za7i2007za7, "box-ref=", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1968z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2letza72008z00,
		BGl_z62gloz12zd2letzd2var1352z70zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1975z00zzintegrate_nodez00,
		BgL_bgl_string1975za700za7za7i2009za7, ":", 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1969z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2setza72010z00,
		BGl_z62gloz12zd2setzd2exzd2it1354za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1976z00zzintegrate_nodez00,
		BgL_bgl_string1976za700za7za7i2011za7, ",", 1);
	      DEFINE_STRING(BGl_string1977z00zzintegrate_nodez00,
		BgL_bgl_string1977za700za7za7i2012za7, "Integrate/node.scm", 18);
	      DEFINE_STRING(BGl_string1978z00zzintegrate_nodez00,
		BgL_bgl_string1978za700za7za7i2013za7, "access=", 7);
	      DEFINE_STRING(BGl_string1979z00zzintegrate_nodez00,
		BgL_bgl_string1979za700za7za7i2014za7, "integration", 11);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1970z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2jump2015za7,
		BGl_z62gloz12zd2jumpzd2exzd2it1356za2zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1971z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2make2016za7,
		BGl_z62gloz12zd2makezd2box1358z70zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1972z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2boxza72017z00,
		BGl_z62gloz12zd2boxzd2ref1360z70zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1973z00zzintegrate_nodez00,
		BgL_bgl_za762gloza712za7d2boxza72018z00,
		BGl_z62gloz12zd2boxzd2setz121362z62zzintegrate_nodez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1980z00zzintegrate_nodez00,
		BgL_bgl_string1980za700za7za7i2019za7, "box-ref", 7);
	      DEFINE_STRING(BGl_string1981z00zzintegrate_nodez00,
		BgL_bgl_string1981za700za7za7i2020za7, "node-free", 9);
	      DEFINE_STRING(BGl_string1982z00zzintegrate_nodez00,
		BgL_bgl_string1982za700za7za7i2021za7, "Unexepected `closure' node", 26);
	      DEFINE_STRING(BGl_string1983z00zzintegrate_nodez00,
		BgL_bgl_string1983za700za7za7i2022za7, "integrate_node", 14);
	      DEFINE_STRING(BGl_string1984z00zzintegrate_nodez00,
		BgL_bgl_string1984za700za7za7i2023za7,
		"aux glo!1316 done (write cell-integrate) cell-integrate ", 56);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_integratezd2celledzf3zd2envzf3zzintegrate_nodez00,
		BgL_bgl_za762integrateza7d2c2024z00,
		BGl_z62integratezd2celledzf3z43zzintegrate_nodez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_integratezd2globaliza7ez12zd2envzb5zzintegrate_nodez00,
		BgL_bgl_za762integrateza7d2g2025z00,
		BGl_z62integratezd2globaliza7ez12z05zzintegrate_nodez00, 0L, BUNSPEC, 3);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzintegrate_nodez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_nodez00(long
		BgL_checksumz00_3315, char *BgL_fromz00_3316)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzintegrate_nodez00))
				{
					BGl_requirezd2initializa7ationz75zzintegrate_nodez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzintegrate_nodez00();
					BGl_libraryzd2moduleszd2initz00zzintegrate_nodez00();
					BGl_cnstzd2initzd2zzintegrate_nodez00();
					BGl_importedzd2moduleszd2initz00zzintegrate_nodez00();
					BGl_genericzd2initzd2zzintegrate_nodez00();
					BGl_methodzd2initzd2zzintegrate_nodez00();
					return BGl_toplevelzd2initzd2zzintegrate_nodez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_nodez00(void)
	{
		{	/* Integrate/node.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "integrate_node");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"integrate_node");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "integrate_node");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"integrate_node");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "integrate_node");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"integrate_node");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"integrate_node");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "integrate_node");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"integrate_node");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"integrate_node");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"integrate_node");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzintegrate_nodez00(void)
	{
		{	/* Integrate/node.scm 15 */
			{	/* Integrate/node.scm 15 */
				obj_t BgL_cportz00_3036;

				{	/* Integrate/node.scm 15 */
					obj_t BgL_stringz00_3043;

					BgL_stringz00_3043 = BGl_string1984z00zzintegrate_nodez00;
					{	/* Integrate/node.scm 15 */
						obj_t BgL_startz00_3044;

						BgL_startz00_3044 = BINT(0L);
						{	/* Integrate/node.scm 15 */
							obj_t BgL_endz00_3045;

							BgL_endz00_3045 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3043)));
							{	/* Integrate/node.scm 15 */

								BgL_cportz00_3036 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3043, BgL_startz00_3044, BgL_endz00_3045);
				}}}}
				{
					long BgL_iz00_3037;

					BgL_iz00_3037 = 4L;
				BgL_loopz00_3038:
					if ((BgL_iz00_3037 == -1L))
						{	/* Integrate/node.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Integrate/node.scm 15 */
							{	/* Integrate/node.scm 15 */
								obj_t BgL_arg1985z00_3039;

								{	/* Integrate/node.scm 15 */

									{	/* Integrate/node.scm 15 */
										obj_t BgL_locationz00_3041;

										BgL_locationz00_3041 = BBOOL(((bool_t) 0));
										{	/* Integrate/node.scm 15 */

											BgL_arg1985z00_3039 =
												BGl_readz00zz__readerz00(BgL_cportz00_3036,
												BgL_locationz00_3041);
										}
									}
								}
								{	/* Integrate/node.scm 15 */
									int BgL_tmpz00_3347;

									BgL_tmpz00_3347 = (int) (BgL_iz00_3037);
									CNST_TABLE_SET(BgL_tmpz00_3347, BgL_arg1985z00_3039);
							}}
							{	/* Integrate/node.scm 15 */
								int BgL_auxz00_3042;

								BgL_auxz00_3042 = (int) ((BgL_iz00_3037 - 1L));
								{
									long BgL_iz00_3352;

									BgL_iz00_3352 = (long) (BgL_auxz00_3042);
									BgL_iz00_3037 = BgL_iz00_3352;
									goto BgL_loopz00_3038;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzintegrate_nodez00(void)
	{
		{	/* Integrate/node.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzintegrate_nodez00(void)
	{
		{	/* Integrate/node.scm 15 */
			return BUNSPEC;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzintegrate_nodez00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1619;

				BgL_headz00_1619 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1620;
					obj_t BgL_tailz00_1621;

					BgL_prevz00_1620 = BgL_headz00_1619;
					BgL_tailz00_1621 = BgL_l1z00_1;
				BgL_loopz00_1622:
					if (PAIRP(BgL_tailz00_1621))
						{
							obj_t BgL_newzd2prevzd2_1624;

							BgL_newzd2prevzd2_1624 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1621), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1620, BgL_newzd2prevzd2_1624);
							{
								obj_t BgL_tailz00_3362;
								obj_t BgL_prevz00_3361;

								BgL_prevz00_3361 = BgL_newzd2prevzd2_1624;
								BgL_tailz00_3362 = CDR(BgL_tailz00_1621);
								BgL_tailz00_1621 = BgL_tailz00_3362;
								BgL_prevz00_1620 = BgL_prevz00_3361;
								goto BgL_loopz00_1622;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1619);
				}
			}
		}

	}



/* integrate-globalize! */
	BGL_EXPORTED_DEF obj_t
		BGl_integratezd2globaliza7ez12z67zzintegrate_nodez00(BgL_nodez00_bglt
		BgL_astz00_3, BgL_variablez00_bglt BgL_integratorz00_4,
		obj_t BgL_whatzf2byza2z50_5)
	{
		{	/* Integrate/node.scm 36 */
			{	/* Integrate/node.scm 42 */
				BgL_valuez00_bglt BgL_funz00_1627;

				BgL_funz00_1627 =
					(((BgL_variablez00_bglt) COBJECT(BgL_integratorz00_4))->BgL_valuez00);
				{	/* Integrate/node.scm 42 */
					obj_t BgL_celledz00_1628;

					{	/* Integrate/node.scm 43 */
						obj_t BgL_arg1422z00_1649;

						BgL_arg1422z00_1649 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_1627)))->BgL_argsz00);
						BgL_celledz00_1628 =
							BGl_celledzd2bindingszd2zzintegrate_nodez00(BgL_arg1422z00_1649);
					}
					{	/* Integrate/node.scm 43 */
						obj_t BgL_whatzf2byza2z50_1629;

						BgL_whatzf2byza2z50_1629 =
							BGl_appendzd221011zd2zzintegrate_nodez00(BgL_celledz00_1628,
							BgL_whatzf2byza2z50_5);
						{	/* Integrate/node.scm 44 */

							{
								obj_t BgL_l1291z00_1631;

								BgL_l1291z00_1631 = BgL_whatzf2byza2z50_1629;
							BgL_zc3z04anonymousza31368ze3z87_1632:
								if (PAIRP(BgL_l1291z00_1631))
									{	/* Integrate/node.scm 46 */
										{	/* Integrate/node.scm 47 */
											obj_t BgL_wzd2bzd2_1634;

											BgL_wzd2bzd2_1634 = CAR(BgL_l1291z00_1631);
											{	/* Integrate/node.scm 47 */
												obj_t BgL_arg1370z00_1635;
												obj_t BgL_arg1371z00_1636;

												BgL_arg1370z00_1635 = CAR(((obj_t) BgL_wzd2bzd2_1634));
												BgL_arg1371z00_1636 = CDR(((obj_t) BgL_wzd2bzd2_1634));
												((((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_arg1370z00_1635))))->
														BgL_fastzd2alphazd2) =
													((obj_t) BgL_arg1371z00_1636), BUNSPEC);
											}
										}
										{
											obj_t BgL_l1291z00_3380;

											BgL_l1291z00_3380 = CDR(BgL_l1291z00_1631);
											BgL_l1291z00_1631 = BgL_l1291z00_3380;
											goto BgL_zc3z04anonymousza31368ze3z87_1632;
										}
									}
								else
									{	/* Integrate/node.scm 46 */
										((bool_t) 1);
									}
							}
							{	/* Integrate/node.scm 49 */
								BgL_nodez00_bglt BgL_resz00_1639;

								BgL_resz00_1639 =
									BGl_cellzd2formalszd2zzintegrate_nodez00(BgL_celledz00_1628,
									BGl_gloz12z12zzintegrate_nodez00(BgL_astz00_3,
										BgL_integratorz00_4));
								{
									obj_t BgL_l1293z00_1641;

									BgL_l1293z00_1641 = BgL_whatzf2byza2z50_1629;
								BgL_zc3z04anonymousza31376ze3z87_1642:
									if (PAIRP(BgL_l1293z00_1641))
										{	/* Integrate/node.scm 51 */
											{	/* Integrate/node.scm 52 */
												obj_t BgL_wzd2bzd2_1644;

												BgL_wzd2bzd2_1644 = CAR(BgL_l1293z00_1641);
												{	/* Integrate/node.scm 52 */
													obj_t BgL_arg1408z00_1645;

													BgL_arg1408z00_1645 =
														CAR(((obj_t) BgL_wzd2bzd2_1644));
													((((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_localz00_bglt)
																			BgL_arg1408z00_1645))))->
															BgL_fastzd2alphazd2) =
														((obj_t) BUNSPEC), BUNSPEC);
												}
											}
											{
												obj_t BgL_l1293z00_3392;

												BgL_l1293z00_3392 = CDR(BgL_l1293z00_1641);
												BgL_l1293z00_1641 = BgL_l1293z00_3392;
												goto BgL_zc3z04anonymousza31376ze3z87_1642;
											}
										}
									else
										{	/* Integrate/node.scm 51 */
											((bool_t) 1);
										}
								}
								return ((obj_t) BgL_resz00_1639);
							}
						}
					}
				}
			}
		}

	}



/* &integrate-globalize! */
	obj_t BGl_z62integratezd2globaliza7ez12z05zzintegrate_nodez00(obj_t
		BgL_envz00_2935, obj_t BgL_astz00_2936, obj_t BgL_integratorz00_2937,
		obj_t BgL_whatzf2byza2z50_2938)
	{
		{	/* Integrate/node.scm 36 */
			return
				BGl_integratezd2globaliza7ez12z67zzintegrate_nodez00(
				((BgL_nodez00_bglt) BgL_astz00_2936),
				((BgL_variablez00_bglt) BgL_integratorz00_2937),
				BgL_whatzf2byza2z50_2938);
		}

	}



/* celled-bindings */
	obj_t BGl_celledzd2bindingszd2zzintegrate_nodez00(obj_t BgL_formalsz00_6)
	{
		{	/* Integrate/node.scm 59 */
			{
				obj_t BgL_celledz00_1652;
				obj_t BgL_formalsz00_1653;

				BgL_celledz00_1652 = BNIL;
				BgL_formalsz00_1653 = BgL_formalsz00_6;
			BgL_zc3z04anonymousza31423ze3z87_1654:
				if (NULLP(BgL_formalsz00_1653))
					{	/* Integrate/node.scm 63 */
						return BgL_celledz00_1652;
					}
				else
					{	/* Integrate/node.scm 65 */
						bool_t BgL_test2032z00_3400;

						{	/* Integrate/node.scm 65 */
							obj_t BgL_arg1472z00_1664;

							BgL_arg1472z00_1664 = CAR(((obj_t) BgL_formalsz00_1653));
							BgL_test2032z00_3400 =
								BGl_integratezd2celledzf3z21zzintegrate_nodez00(
								((BgL_localz00_bglt) BgL_arg1472z00_1664));
						}
						if (BgL_test2032z00_3400)
							{	/* Integrate/node.scm 68 */
								obj_t BgL_ovarz00_1658;

								BgL_ovarz00_1658 = CAR(((obj_t) BgL_formalsz00_1653));
								{	/* Integrate/node.scm 68 */
									BgL_localz00_bglt BgL_nvarz00_1659;

									BgL_nvarz00_1659 =
										BGl_cellzd2variablezd2zzintegrate_nodez00(
										((BgL_localz00_bglt) BgL_ovarz00_1658));
									{	/* Integrate/node.scm 69 */

										{	/* Integrate/node.scm 70 */
											obj_t BgL_arg1437z00_1660;
											obj_t BgL_arg1448z00_1661;

											{	/* Integrate/node.scm 70 */
												obj_t BgL_arg1453z00_1662;

												BgL_arg1453z00_1662 =
													MAKE_YOUNG_PAIR(BgL_ovarz00_1658,
													((obj_t) BgL_nvarz00_1659));
												BgL_arg1437z00_1660 =
													MAKE_YOUNG_PAIR(BgL_arg1453z00_1662,
													BgL_celledz00_1652);
											}
											BgL_arg1448z00_1661 = CDR(((obj_t) BgL_formalsz00_1653));
											{
												obj_t BgL_formalsz00_3415;
												obj_t BgL_celledz00_3414;

												BgL_celledz00_3414 = BgL_arg1437z00_1660;
												BgL_formalsz00_3415 = BgL_arg1448z00_1661;
												BgL_formalsz00_1653 = BgL_formalsz00_3415;
												BgL_celledz00_1652 = BgL_celledz00_3414;
												goto BgL_zc3z04anonymousza31423ze3z87_1654;
											}
										}
									}
								}
							}
						else
							{	/* Integrate/node.scm 66 */
								obj_t BgL_arg1454z00_1663;

								BgL_arg1454z00_1663 = CDR(((obj_t) BgL_formalsz00_1653));
								{
									obj_t BgL_formalsz00_3418;

									BgL_formalsz00_3418 = BgL_arg1454z00_1663;
									BgL_formalsz00_1653 = BgL_formalsz00_3418;
									goto BgL_zc3z04anonymousza31423ze3z87_1654;
								}
							}
					}
			}
		}

	}



/* cell-variable */
	BgL_localz00_bglt BGl_cellzd2variablezd2zzintegrate_nodez00(BgL_localz00_bglt
		BgL_localz00_7)
	{
		{	/* Integrate/node.scm 75 */
			{	/* Integrate/node.scm 76 */
				BgL_localz00_bglt BgL_varz00_1666;

				{	/* Integrate/node.scm 76 */
					obj_t BgL_arg1485z00_1672;

					BgL_arg1485z00_1672 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_localz00_7)))->BgL_idz00);
					BgL_varz00_1666 =
						BGl_makezd2localzd2svarz00zzast_localz00(BgL_arg1485z00_1672,
						((BgL_typez00_bglt) BGl_za2cellza2z00zztype_cachez00));
				}
				{	/* Integrate/node.scm 77 */
					obj_t BgL_vz00_2272;

					BgL_vz00_2272 = CNST_TABLE_REF(0);
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_varz00_1666)))->BgL_accessz00) =
						((obj_t) BgL_vz00_2272), BUNSPEC);
				}
				{	/* Integrate/node.scm 78 */
					BgL_svarz00_bglt BgL_tmp1110z00_1667;

					BgL_tmp1110z00_1667 =
						((BgL_svarz00_bglt)
						(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_varz00_1666)))->BgL_valuez00));
					{	/* Integrate/node.scm 78 */
						BgL_svarzf2iinfozf2_bglt BgL_wide1112z00_1669;

						BgL_wide1112z00_1669 =
							((BgL_svarzf2iinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_svarzf2iinfozf2_bgl))));
						{	/* Integrate/node.scm 78 */
							obj_t BgL_auxz00_3433;
							BgL_objectz00_bglt BgL_tmpz00_3430;

							BgL_auxz00_3433 = ((obj_t) BgL_wide1112z00_1669);
							BgL_tmpz00_3430 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_tmp1110z00_1667));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3430, BgL_auxz00_3433);
						}
						((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_tmp1110z00_1667));
						{	/* Integrate/node.scm 78 */
							long BgL_arg1473z00_1670;

							BgL_arg1473z00_1670 =
								BGL_CLASS_NUM(BGl_svarzf2Iinfozf2zzintegrate_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_svarz00_bglt) BgL_tmp1110z00_1667)),
								BgL_arg1473z00_1670);
						}
						((BgL_svarz00_bglt) ((BgL_svarz00_bglt) BgL_tmp1110z00_1667));
					}
					{
						BgL_svarzf2iinfozf2_bglt BgL_auxz00_3444;

						{
							obj_t BgL_auxz00_3445;

							{	/* Integrate/node.scm 79 */
								BgL_objectz00_bglt BgL_tmpz00_3446;

								BgL_tmpz00_3446 =
									((BgL_objectz00_bglt)
									((BgL_svarz00_bglt) BgL_tmp1110z00_1667));
								BgL_auxz00_3445 = BGL_OBJECT_WIDENING(BgL_tmpz00_3446);
							}
							BgL_auxz00_3444 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3445);
						}
						((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3444))->
								BgL_fzd2markzd2) = ((obj_t) BUNSPEC), BUNSPEC);
					}
					{
						BgL_svarzf2iinfozf2_bglt BgL_auxz00_3452;

						{
							obj_t BgL_auxz00_3453;

							{	/* Integrate/node.scm 79 */
								BgL_objectz00_bglt BgL_tmpz00_3454;

								BgL_tmpz00_3454 =
									((BgL_objectz00_bglt)
									((BgL_svarz00_bglt) BgL_tmp1110z00_1667));
								BgL_auxz00_3453 = BGL_OBJECT_WIDENING(BgL_tmpz00_3454);
							}
							BgL_auxz00_3452 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3453);
						}
						((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3452))->
								BgL_uzd2markzd2) = ((obj_t) BUNSPEC), BUNSPEC);
					}
					{
						BgL_svarzf2iinfozf2_bglt BgL_auxz00_3460;

						{
							obj_t BgL_auxz00_3461;

							{	/* Integrate/node.scm 80 */
								BgL_objectz00_bglt BgL_tmpz00_3462;

								BgL_tmpz00_3462 =
									((BgL_objectz00_bglt)
									((BgL_svarz00_bglt) BgL_tmp1110z00_1667));
								BgL_auxz00_3461 = BGL_OBJECT_WIDENING(BgL_tmpz00_3462);
							}
							BgL_auxz00_3460 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3461);
						}
						((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3460))->
								BgL_kapturedzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
					}
					{
						BgL_svarzf2iinfozf2_bglt BgL_auxz00_3468;

						{
							obj_t BgL_auxz00_3469;

							{	/* Integrate/node.scm 79 */
								BgL_objectz00_bglt BgL_tmpz00_3470;

								BgL_tmpz00_3470 =
									((BgL_objectz00_bglt)
									((BgL_svarz00_bglt) BgL_tmp1110z00_1667));
								BgL_auxz00_3469 = BGL_OBJECT_WIDENING(BgL_tmpz00_3470);
							}
							BgL_auxz00_3468 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3469);
						}
						((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3468))->
								BgL_celledzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
					}
					{
						BgL_svarzf2iinfozf2_bglt BgL_auxz00_3476;

						{
							obj_t BgL_auxz00_3477;

							{	/* Integrate/node.scm 79 */
								BgL_objectz00_bglt BgL_tmpz00_3478;

								BgL_tmpz00_3478 =
									((BgL_objectz00_bglt)
									((BgL_svarz00_bglt) BgL_tmp1110z00_1667));
								BgL_auxz00_3477 = BGL_OBJECT_WIDENING(BgL_tmpz00_3478);
							}
							BgL_auxz00_3476 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3477);
						}
						((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3476))->
								BgL_xhdlz00) = ((obj_t) BFALSE), BUNSPEC);
					}
					((BgL_svarz00_bglt) BgL_tmp1110z00_1667);
				}
				return BgL_varz00_1666;
			}
		}

	}



/* cell-formals */
	BgL_nodez00_bglt BGl_cellzd2formalszd2zzintegrate_nodez00(obj_t
		BgL_celledz00_8, BgL_nodez00_bglt BgL_bodyz00_9)
	{
		{	/* Integrate/node.scm 86 */
			if (NULLP(BgL_celledz00_8))
				{	/* Integrate/node.scm 87 */
					return BgL_bodyz00_9;
				}
			else
				{	/* Integrate/node.scm 89 */
					obj_t BgL_locz00_1674;

					BgL_locz00_1674 =
						(((BgL_nodez00_bglt) COBJECT(BgL_bodyz00_9))->BgL_locz00);
					{	/* Integrate/node.scm 90 */
						BgL_letzd2varzd2_bglt BgL_new1115z00_1675;

						{	/* Integrate/node.scm 91 */
							BgL_letzd2varzd2_bglt BgL_new1114z00_1697;

							BgL_new1114z00_1697 =
								((BgL_letzd2varzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_letzd2varzd2_bgl))));
							{	/* Integrate/node.scm 91 */
								long BgL_arg1546z00_1698;

								BgL_arg1546z00_1698 =
									BGL_CLASS_NUM(BGl_letzd2varzd2zzast_nodez00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_new1114z00_1697),
									BgL_arg1546z00_1698);
							}
							{	/* Integrate/node.scm 91 */
								BgL_objectz00_bglt BgL_tmpz00_3492;

								BgL_tmpz00_3492 = ((BgL_objectz00_bglt) BgL_new1114z00_1697);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3492, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_new1114z00_1697);
							BgL_new1115z00_1675 = BgL_new1114z00_1697;
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_new1115z00_1675)))->BgL_locz00) =
							((obj_t) BgL_locz00_1674), BUNSPEC);
						((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
											BgL_new1115z00_1675)))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_nodez00_bglt) COBJECT(BgL_bodyz00_9))->
									BgL_typez00)), BUNSPEC);
						((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
											BgL_new1115z00_1675)))->BgL_sidezd2effectzd2) =
							((obj_t) BUNSPEC), BUNSPEC);
						((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
											BgL_new1115z00_1675)))->BgL_keyz00) =
							((obj_t) BINT(-1L)), BUNSPEC);
						{
							obj_t BgL_auxz00_3506;

							{	/* Integrate/node.scm 94 */
								obj_t BgL_head1297z00_1678;

								BgL_head1297z00_1678 = MAKE_YOUNG_PAIR(BNIL, BNIL);
								{
									obj_t BgL_l1295z00_1680;
									obj_t BgL_tail1298z00_1681;

									BgL_l1295z00_1680 = BgL_celledz00_8;
									BgL_tail1298z00_1681 = BgL_head1297z00_1678;
								BgL_zc3z04anonymousza31488ze3z87_1682:
									if (NULLP(BgL_l1295z00_1680))
										{	/* Integrate/node.scm 94 */
											BgL_auxz00_3506 = CDR(BgL_head1297z00_1678);
										}
									else
										{	/* Integrate/node.scm 94 */
											obj_t BgL_newtail1299z00_1684;

											{	/* Integrate/node.scm 94 */
												obj_t BgL_arg1509z00_1686;

												{	/* Integrate/node.scm 94 */
													obj_t BgL_ozd2nzd2_1687;

													BgL_ozd2nzd2_1687 = CAR(((obj_t) BgL_l1295z00_1680));
													{	/* Integrate/node.scm 95 */
														obj_t BgL_arg1513z00_1688;
														BgL_makezd2boxzd2_bglt BgL_arg1514z00_1689;

														BgL_arg1513z00_1688 =
															CDR(((obj_t) BgL_ozd2nzd2_1687));
														{	/* Integrate/node.scm 96 */
															BgL_refz00_bglt BgL_arg1516z00_1690;
															obj_t BgL_arg1535z00_1691;

															{	/* Integrate/node.scm 96 */
																BgL_refz00_bglt BgL_new1118z00_1692;

																{	/* Integrate/node.scm 99 */
																	BgL_refz00_bglt BgL_new1116z00_1694;

																	BgL_new1116z00_1694 =
																		((BgL_refz00_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_refz00_bgl))));
																	{	/* Integrate/node.scm 99 */
																		long BgL_arg1544z00_1695;

																		{	/* Integrate/node.scm 99 */
																			obj_t BgL_classz00_2292;

																			BgL_classz00_2292 =
																				BGl_refz00zzast_nodez00;
																			BgL_arg1544z00_1695 =
																				BGL_CLASS_NUM(BgL_classz00_2292);
																		}
																		BGL_OBJECT_CLASS_NUM_SET(
																			((BgL_objectz00_bglt)
																				BgL_new1116z00_1694),
																			BgL_arg1544z00_1695);
																	}
																	{	/* Integrate/node.scm 99 */
																		BgL_objectz00_bglt BgL_tmpz00_3519;

																		BgL_tmpz00_3519 =
																			((BgL_objectz00_bglt)
																			BgL_new1116z00_1694);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3519,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1116z00_1694);
																	BgL_new1118z00_1692 = BgL_new1116z00_1694;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1118z00_1692)))->BgL_locz00) =
																	((obj_t) BgL_locz00_1674), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1118z00_1692)))->BgL_typez00) =
																	((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																				COBJECT(((BgL_variablez00_bglt)
																						CAR(((obj_t)
																								BgL_ozd2nzd2_1687)))))->
																			BgL_typez00)), BUNSPEC);
																((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																					BgL_new1118z00_1692)))->
																		BgL_variablez00) =
																	((BgL_variablez00_bglt) (
																			(BgL_variablez00_bglt) CAR(((obj_t)
																					BgL_ozd2nzd2_1687)))), BUNSPEC);
																BgL_arg1516z00_1690 = BgL_new1118z00_1692;
															}
															BgL_arg1535z00_1691 =
																CAR(((obj_t) BgL_ozd2nzd2_1687));
															BgL_arg1514z00_1689 =
																BGl_azd2makezd2cellz00zzintegrate_nodez00(
																((BgL_nodez00_bglt) BgL_arg1516z00_1690),
																((BgL_variablez00_bglt) BgL_arg1535z00_1691));
														}
														BgL_arg1509z00_1686 =
															MAKE_YOUNG_PAIR(BgL_arg1513z00_1688,
															((obj_t) BgL_arg1514z00_1689));
												}}
												BgL_newtail1299z00_1684 =
													MAKE_YOUNG_PAIR(BgL_arg1509z00_1686, BNIL);
											}
											SET_CDR(BgL_tail1298z00_1681, BgL_newtail1299z00_1684);
											{	/* Integrate/node.scm 94 */
												obj_t BgL_arg1502z00_1685;

												BgL_arg1502z00_1685 = CDR(((obj_t) BgL_l1295z00_1680));
												{
													obj_t BgL_tail1298z00_3548;
													obj_t BgL_l1295z00_3547;

													BgL_l1295z00_3547 = BgL_arg1502z00_1685;
													BgL_tail1298z00_3548 = BgL_newtail1299z00_1684;
													BgL_tail1298z00_1681 = BgL_tail1298z00_3548;
													BgL_l1295z00_1680 = BgL_l1295z00_3547;
													goto BgL_zc3z04anonymousza31488ze3z87_1682;
												}
											}
										}
								}
							}
							((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1115z00_1675))->
									BgL_bindingsz00) = ((obj_t) BgL_auxz00_3506), BUNSPEC);
						}
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1115z00_1675))->
								BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_bodyz00_9), BUNSPEC);
						((((BgL_letzd2varzd2_bglt) COBJECT(BgL_new1115z00_1675))->
								BgL_removablezf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
						return ((BgL_nodez00_bglt) BgL_new1115z00_1675);
					}
				}
		}

	}



/* a-make-cell */
	BgL_makezd2boxzd2_bglt
		BGl_azd2makezd2cellz00zzintegrate_nodez00(BgL_nodez00_bglt BgL_nodez00_10,
		BgL_variablez00_bglt BgL_variablez00_11)
	{
		{	/* Integrate/node.scm 106 */
			{	/* Integrate/node.scm 110 */
				obj_t BgL_vz00_2303;

				BgL_vz00_2303 = CNST_TABLE_REF(0);
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_variablez00_11))))->BgL_accessz00) =
					((obj_t) BgL_vz00_2303), BUNSPEC);
			}
			{	/* Integrate/node.scm 111 */
				BgL_valuez00_bglt BgL_arg1552z00_1700;

				BgL_arg1552z00_1700 =
					(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_11))->BgL_valuez00);
				{
					BgL_svarzf2iinfozf2_bglt BgL_auxz00_3558;

					{
						obj_t BgL_auxz00_3559;

						{	/* Integrate/node.scm 111 */
							BgL_objectz00_bglt BgL_tmpz00_3560;

							BgL_tmpz00_3560 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_arg1552z00_1700));
							BgL_auxz00_3559 = BGL_OBJECT_WIDENING(BgL_tmpz00_3560);
						}
						BgL_auxz00_3558 = ((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3559);
					}
					((((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3558))->
							BgL_celledzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
				}
			}
			{	/* Integrate/node.scm 112 */
				BgL_makezd2boxzd2_bglt BgL_new1122z00_1701;

				{	/* Integrate/node.scm 115 */
					BgL_makezd2boxzd2_bglt BgL_new1120z00_1703;

					BgL_new1120z00_1703 =
						((BgL_makezd2boxzd2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_makezd2boxzd2_bgl))));
					{	/* Integrate/node.scm 115 */
						long BgL_arg1559z00_1704;

						BgL_arg1559z00_1704 = BGL_CLASS_NUM(BGl_makezd2boxzd2zzast_nodez00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt) BgL_new1120z00_1703), BgL_arg1559z00_1704);
					}
					{	/* Integrate/node.scm 115 */
						BgL_objectz00_bglt BgL_tmpz00_3570;

						BgL_tmpz00_3570 = ((BgL_objectz00_bglt) BgL_new1120z00_1703);
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3570, BFALSE);
					}
					((BgL_objectz00_bglt) BgL_new1120z00_1703);
					BgL_new1122z00_1701 = BgL_new1120z00_1703;
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt) BgL_new1122z00_1701)))->BgL_locz00) =
					((obj_t) (((BgL_nodez00_bglt) COBJECT(BgL_nodez00_10))->BgL_locz00)),
					BUNSPEC);
				((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
									BgL_new1122z00_1701)))->BgL_typez00) =
					((BgL_typez00_bglt) ((BgL_typez00_bglt)
							BGl_za2cellza2z00zztype_cachez00)), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1122z00_1701)))->BgL_sidezd2effectzd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_nodezf2effectzf2_bglt) COBJECT(((BgL_nodezf2effectzf2_bglt)
									BgL_new1122z00_1701)))->BgL_keyz00) =
					((obj_t) BINT(-1L)), BUNSPEC);
				((((BgL_makezd2boxzd2_bglt) COBJECT(BgL_new1122z00_1701))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_nodez00_10), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_3586;

					{	/* Integrate/node.scm 114 */
						BgL_typez00_bglt BgL_arg1553z00_1702;

						BgL_arg1553z00_1702 =
							(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_11))->
							BgL_typez00);
						BgL_auxz00_3586 =
							BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_arg1553z00_1702);
					}
					((((BgL_makezd2boxzd2_bglt) COBJECT(BgL_new1122z00_1701))->
							BgL_vtypez00) = ((BgL_typez00_bglt) BgL_auxz00_3586), BUNSPEC);
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(BgL_new1122z00_1701))->
						BgL_stackablez00) = ((obj_t) BTRUE), BUNSPEC);
				return BgL_new1122z00_1701;
			}
		}

	}



/* integrate-celled? */
	BGL_EXPORTED_DEF bool_t
		BGl_integratezd2celledzf3z21zzintegrate_nodez00(BgL_localz00_bglt
		BgL_variablez00_12)
	{
		{	/* Integrate/node.scm 122 */
			{	/* Integrate/node.scm 133 */
				bool_t BgL_test2035z00_3591;

				{	/* Integrate/node.scm 133 */
					BgL_valuez00_bglt BgL_arg1571z00_1711;

					BgL_arg1571z00_1711 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_variablez00_12)))->BgL_valuez00);
					{	/* Integrate/node.scm 133 */
						obj_t BgL_classz00_2314;

						BgL_classz00_2314 = BGl_svarzf2Iinfozf2zzintegrate_infoz00;
						{	/* Integrate/node.scm 133 */
							BgL_objectz00_bglt BgL_arg1807z00_2316;

							{	/* Integrate/node.scm 133 */
								obj_t BgL_tmpz00_3594;

								BgL_tmpz00_3594 =
									((obj_t) ((BgL_objectz00_bglt) BgL_arg1571z00_1711));
								BgL_arg1807z00_2316 = (BgL_objectz00_bglt) (BgL_tmpz00_3594);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Integrate/node.scm 133 */
									long BgL_idxz00_2322;

									BgL_idxz00_2322 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2316);
									BgL_test2035z00_3591 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_2322 + 3L)) == BgL_classz00_2314);
								}
							else
								{	/* Integrate/node.scm 133 */
									bool_t BgL_res1929z00_2347;

									{	/* Integrate/node.scm 133 */
										obj_t BgL_oclassz00_2330;

										{	/* Integrate/node.scm 133 */
											obj_t BgL_arg1815z00_2338;
											long BgL_arg1816z00_2339;

											BgL_arg1815z00_2338 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Integrate/node.scm 133 */
												long BgL_arg1817z00_2340;

												BgL_arg1817z00_2340 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2316);
												BgL_arg1816z00_2339 =
													(BgL_arg1817z00_2340 - OBJECT_TYPE);
											}
											BgL_oclassz00_2330 =
												VECTOR_REF(BgL_arg1815z00_2338, BgL_arg1816z00_2339);
										}
										{	/* Integrate/node.scm 133 */
											bool_t BgL__ortest_1115z00_2331;

											BgL__ortest_1115z00_2331 =
												(BgL_classz00_2314 == BgL_oclassz00_2330);
											if (BgL__ortest_1115z00_2331)
												{	/* Integrate/node.scm 133 */
													BgL_res1929z00_2347 = BgL__ortest_1115z00_2331;
												}
											else
												{	/* Integrate/node.scm 133 */
													long BgL_odepthz00_2332;

													{	/* Integrate/node.scm 133 */
														obj_t BgL_arg1804z00_2333;

														BgL_arg1804z00_2333 = (BgL_oclassz00_2330);
														BgL_odepthz00_2332 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_2333);
													}
													if ((3L < BgL_odepthz00_2332))
														{	/* Integrate/node.scm 133 */
															obj_t BgL_arg1802z00_2335;

															{	/* Integrate/node.scm 133 */
																obj_t BgL_arg1803z00_2336;

																BgL_arg1803z00_2336 = (BgL_oclassz00_2330);
																BgL_arg1802z00_2335 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_2336,
																	3L);
															}
															BgL_res1929z00_2347 =
																(BgL_arg1802z00_2335 == BgL_classz00_2314);
														}
													else
														{	/* Integrate/node.scm 133 */
															BgL_res1929z00_2347 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2035z00_3591 = BgL_res1929z00_2347;
								}
						}
					}
				}
				if (BgL_test2035z00_3591)
					{	/* Integrate/node.scm 134 */
						bool_t BgL__ortest_1124z00_1706;

						{	/* Integrate/node.scm 134 */
							BgL_svarz00_bglt BgL_oz00_2349;

							BgL_oz00_2349 =
								((BgL_svarz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_variablez00_12)))->
									BgL_valuez00));
							{
								BgL_svarzf2iinfozf2_bglt BgL_auxz00_3620;

								{
									obj_t BgL_auxz00_3621;

									{	/* Integrate/node.scm 134 */
										BgL_objectz00_bglt BgL_tmpz00_3622;

										BgL_tmpz00_3622 = ((BgL_objectz00_bglt) BgL_oz00_2349);
										BgL_auxz00_3621 = BGL_OBJECT_WIDENING(BgL_tmpz00_3622);
									}
									BgL_auxz00_3620 =
										((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3621);
								}
								BgL__ortest_1124z00_1706 =
									(((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3620))->
									BgL_celledzf3zf3);
							}
						}
						if (BgL__ortest_1124z00_1706)
							{	/* Integrate/node.scm 134 */
								return BgL__ortest_1124z00_1706;
							}
						else
							{	/* Integrate/node.scm 134 */
								if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_variablez00_12)))->
												BgL_accessz00), CNST_TABLE_REF(1))))
									{	/* Integrate/node.scm 136 */
										BgL_svarz00_bglt BgL_oz00_2353;

										BgL_oz00_2353 =
											((BgL_svarz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_variablez00_12)))->
												BgL_valuez00));
										{
											BgL_svarzf2iinfozf2_bglt BgL_auxz00_3637;

											{
												obj_t BgL_auxz00_3638;

												{	/* Integrate/node.scm 136 */
													BgL_objectz00_bglt BgL_tmpz00_3639;

													BgL_tmpz00_3639 =
														((BgL_objectz00_bglt) BgL_oz00_2353);
													BgL_auxz00_3638 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3639);
												}
												BgL_auxz00_3637 =
													((BgL_svarzf2iinfozf2_bglt) BgL_auxz00_3638);
											}
											return
												(((BgL_svarzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3637))->
												BgL_kapturedzf3zf3);
										}
									}
								else
									{	/* Integrate/node.scm 135 */
										return ((bool_t) 0);
									}
							}
					}
				else
					{	/* Integrate/node.scm 133 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &integrate-celled? */
	obj_t BGl_z62integratezd2celledzf3z43zzintegrate_nodez00(obj_t
		BgL_envz00_2939, obj_t BgL_variablez00_2940)
	{
		{	/* Integrate/node.scm 122 */
			return
				BBOOL(BGl_integratezd2celledzf3z21zzintegrate_nodez00(
					((BgL_localz00_bglt) BgL_variablez00_2940)));
		}

	}



/* glo*! */
	obj_t BGl_gloza2z12zb0zzintegrate_nodez00(obj_t BgL_nodeza2za2_59,
		obj_t BgL_integratorz00_60)
	{
		{	/* Integrate/node.scm 439 */
			{
				obj_t BgL_nodeza2za2_1713;

				BgL_nodeza2za2_1713 = BgL_nodeza2za2_59;
			BgL_zc3z04anonymousza31572ze3z87_1714:
				if (NULLP(BgL_nodeza2za2_1713))
					{	/* Integrate/node.scm 441 */
						return CNST_TABLE_REF(2);
					}
				else
					{	/* Integrate/node.scm 441 */
						{	/* Integrate/node.scm 444 */
							BgL_nodez00_bglt BgL_arg1575z00_1716;

							{	/* Integrate/node.scm 444 */
								obj_t BgL_arg1576z00_1717;

								BgL_arg1576z00_1717 = CAR(((obj_t) BgL_nodeza2za2_1713));
								BgL_arg1575z00_1716 =
									BGl_gloz12z12zzintegrate_nodez00(
									((BgL_nodez00_bglt) BgL_arg1576z00_1717),
									((BgL_variablez00_bglt) BgL_integratorz00_60));
							}
							{	/* Integrate/node.scm 444 */
								obj_t BgL_auxz00_3657;
								obj_t BgL_tmpz00_3655;

								BgL_auxz00_3657 = ((obj_t) BgL_arg1575z00_1716);
								BgL_tmpz00_3655 = ((obj_t) BgL_nodeza2za2_1713);
								SET_CAR(BgL_tmpz00_3655, BgL_auxz00_3657);
							}
						}
						{	/* Integrate/node.scm 445 */
							obj_t BgL_arg1584z00_1718;

							BgL_arg1584z00_1718 = CDR(((obj_t) BgL_nodeza2za2_1713));
							{
								obj_t BgL_nodeza2za2_3662;

								BgL_nodeza2za2_3662 = BgL_arg1584z00_1718;
								BgL_nodeza2za2_1713 = BgL_nodeza2za2_3662;
								goto BgL_zc3z04anonymousza31572ze3z87_1714;
							}
						}
					}
			}
		}

	}



/* globalize-local-fun! */
	obj_t BGl_globaliza7ezd2localzd2funz12zb5zzintegrate_nodez00(BgL_localz00_bglt
		BgL_localz00_61, BgL_variablez00_bglt BgL_integratorz00_62)
	{
		{	/* Integrate/node.scm 450 */
			{	/* Integrate/node.scm 453 */
				BgL_valuez00_bglt BgL_funz00_1720;

				BgL_funz00_1720 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_localz00_61)))->BgL_valuez00);
				{	/* Integrate/node.scm 453 */
					obj_t BgL_obodyz00_1721;

					BgL_obodyz00_1721 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1720)))->BgL_bodyz00);
					{	/* Integrate/node.scm 454 */

						if ((((obj_t) BgL_localz00_61) == ((obj_t) BgL_integratorz00_62)))
							{	/* Integrate/node.scm 457 */
								BgL_nodez00_bglt BgL_arg1585z00_1722;

								BgL_arg1585z00_1722 =
									BGl_gloz12z12zzintegrate_nodez00(
									((BgL_nodez00_bglt) BgL_obodyz00_1721), BgL_integratorz00_62);
								return
									((((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_funz00_1720)))->BgL_bodyz00) =
									((obj_t) ((obj_t) BgL_arg1585z00_1722)), BUNSPEC);
							}
						else
							{	/* Integrate/node.scm 458 */
								bool_t BgL_test2043z00_3676;

								{	/* Integrate/node.scm 458 */
									bool_t BgL_test2044z00_3677;

									{	/* Integrate/node.scm 458 */
										obj_t BgL_classz00_2361;

										BgL_classz00_2361 = BGl_sfunzf2Iinfozf2zzintegrate_infoz00;
										{	/* Integrate/node.scm 458 */
											BgL_objectz00_bglt BgL_arg1807z00_2363;

											{	/* Integrate/node.scm 458 */
												obj_t BgL_tmpz00_3678;

												BgL_tmpz00_3678 =
													((obj_t) ((BgL_objectz00_bglt) BgL_funz00_1720));
												BgL_arg1807z00_2363 =
													(BgL_objectz00_bglt) (BgL_tmpz00_3678);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Integrate/node.scm 458 */
													long BgL_idxz00_2369;

													BgL_idxz00_2369 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2363);
													BgL_test2044z00_3677 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_2369 + 4L)) == BgL_classz00_2361);
												}
											else
												{	/* Integrate/node.scm 458 */
													bool_t BgL_res1930z00_2394;

													{	/* Integrate/node.scm 458 */
														obj_t BgL_oclassz00_2377;

														{	/* Integrate/node.scm 458 */
															obj_t BgL_arg1815z00_2385;
															long BgL_arg1816z00_2386;

															BgL_arg1815z00_2385 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Integrate/node.scm 458 */
																long BgL_arg1817z00_2387;

																BgL_arg1817z00_2387 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2363);
																BgL_arg1816z00_2386 =
																	(BgL_arg1817z00_2387 - OBJECT_TYPE);
															}
															BgL_oclassz00_2377 =
																VECTOR_REF(BgL_arg1815z00_2385,
																BgL_arg1816z00_2386);
														}
														{	/* Integrate/node.scm 458 */
															bool_t BgL__ortest_1115z00_2378;

															BgL__ortest_1115z00_2378 =
																(BgL_classz00_2361 == BgL_oclassz00_2377);
															if (BgL__ortest_1115z00_2378)
																{	/* Integrate/node.scm 458 */
																	BgL_res1930z00_2394 =
																		BgL__ortest_1115z00_2378;
																}
															else
																{	/* Integrate/node.scm 458 */
																	long BgL_odepthz00_2379;

																	{	/* Integrate/node.scm 458 */
																		obj_t BgL_arg1804z00_2380;

																		BgL_arg1804z00_2380 = (BgL_oclassz00_2377);
																		BgL_odepthz00_2379 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_2380);
																	}
																	if ((4L < BgL_odepthz00_2379))
																		{	/* Integrate/node.scm 458 */
																			obj_t BgL_arg1802z00_2382;

																			{	/* Integrate/node.scm 458 */
																				obj_t BgL_arg1803z00_2383;

																				BgL_arg1803z00_2383 =
																					(BgL_oclassz00_2377);
																				BgL_arg1802z00_2382 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_2383, 4L);
																			}
																			BgL_res1930z00_2394 =
																				(BgL_arg1802z00_2382 ==
																				BgL_classz00_2361);
																		}
																	else
																		{	/* Integrate/node.scm 458 */
																			BgL_res1930z00_2394 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2044z00_3677 = BgL_res1930z00_2394;
												}
										}
									}
									if (BgL_test2044z00_3677)
										{
											BgL_sfunzf2iinfozf2_bglt BgL_auxz00_3701;

											{
												obj_t BgL_auxz00_3702;

												{	/* Integrate/node.scm 458 */
													BgL_objectz00_bglt BgL_tmpz00_3703;

													BgL_tmpz00_3703 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_1720));
													BgL_auxz00_3702 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_3703);
												}
												BgL_auxz00_3701 =
													((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_3702);
											}
											BgL_test2043z00_3676 =
												(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_3701))->
												BgL_gzf3zf3);
										}
									else
										{	/* Integrate/node.scm 458 */
											BgL_test2043z00_3676 = ((bool_t) 0);
										}
								}
								if (BgL_test2043z00_3676)
									{	/* Integrate/node.scm 458 */
										return BUNSPEC;
									}
								else
									{	/* Integrate/node.scm 461 */
										obj_t BgL_celledz00_1725;

										{	/* Integrate/node.scm 461 */
											obj_t BgL_arg1606z00_1745;

											BgL_arg1606z00_1745 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_1720)))->
												BgL_argsz00);
											BgL_celledz00_1725 =
												BGl_celledzd2bindingszd2zzintegrate_nodez00
												(BgL_arg1606z00_1745);
										}
										{
											obj_t BgL_l1312z00_1727;

											BgL_l1312z00_1727 = BgL_celledz00_1725;
										BgL_zc3z04anonymousza31588ze3z87_1728:
											if (PAIRP(BgL_l1312z00_1727))
												{	/* Integrate/node.scm 462 */
													{	/* Integrate/node.scm 463 */
														obj_t BgL_wzd2bzd2_1730;

														BgL_wzd2bzd2_1730 = CAR(BgL_l1312z00_1727);
														{	/* Integrate/node.scm 463 */
															obj_t BgL_arg1591z00_1731;
															obj_t BgL_arg1593z00_1732;

															BgL_arg1591z00_1731 =
																CAR(((obj_t) BgL_wzd2bzd2_1730));
															BgL_arg1593z00_1732 =
																CDR(((obj_t) BgL_wzd2bzd2_1730));
															((((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_arg1591z00_1731)))->
																	BgL_fastzd2alphazd2) =
																((obj_t) BgL_arg1593z00_1732), BUNSPEC);
														}
													}
													{
														obj_t BgL_l1312z00_3721;

														BgL_l1312z00_3721 = CDR(BgL_l1312z00_1727);
														BgL_l1312z00_1727 = BgL_l1312z00_3721;
														goto BgL_zc3z04anonymousza31588ze3z87_1728;
													}
												}
											else
												{	/* Integrate/node.scm 462 */
													((bool_t) 1);
												}
										}
										{	/* Integrate/node.scm 465 */
											BgL_nodez00_bglt BgL_nbody1z00_1735;

											BgL_nbody1z00_1735 =
												BGl_gloz12z12zzintegrate_nodez00(
												((BgL_nodez00_bglt) BgL_obodyz00_1721),
												BgL_integratorz00_62);
											{	/* Integrate/node.scm 465 */
												BgL_nodez00_bglt BgL_nbody2z00_1736;

												BgL_nbody2z00_1736 =
													BGl_cellzd2formalszd2zzintegrate_nodez00
													(BgL_celledz00_1725, BgL_nbody1z00_1735);
												{	/* Integrate/node.scm 466 */

													{
														obj_t BgL_l1314z00_1738;

														BgL_l1314z00_1738 = BgL_celledz00_1725;
													BgL_zc3z04anonymousza31595ze3z87_1739:
														if (PAIRP(BgL_l1314z00_1738))
															{	/* Integrate/node.scm 467 */
																{	/* Integrate/node.scm 468 */
																	obj_t BgL_wzd2bzd2_1741;

																	BgL_wzd2bzd2_1741 = CAR(BgL_l1314z00_1738);
																	{	/* Integrate/node.scm 468 */
																		obj_t BgL_arg1602z00_1742;

																		BgL_arg1602z00_1742 =
																			CAR(((obj_t) BgL_wzd2bzd2_1741));
																		((((BgL_variablez00_bglt) COBJECT(
																						((BgL_variablez00_bglt)
																							BgL_arg1602z00_1742)))->
																				BgL_fastzd2alphazd2) =
																			((obj_t) BUNSPEC), BUNSPEC);
																	}
																}
																{
																	obj_t BgL_l1314z00_3733;

																	BgL_l1314z00_3733 = CDR(BgL_l1314z00_1738);
																	BgL_l1314z00_1738 = BgL_l1314z00_3733;
																	goto BgL_zc3z04anonymousza31595ze3z87_1739;
																}
															}
														else
															{	/* Integrate/node.scm 467 */
																((bool_t) 1);
															}
													}
													return
														((((BgL_sfunz00_bglt) COBJECT(
																	((BgL_sfunz00_bglt) BgL_funz00_1720)))->
															BgL_bodyz00) =
														((obj_t) ((obj_t) BgL_nbody2z00_1736)), BUNSPEC);
												}
											}
										}
									}
							}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzintegrate_nodez00(void)
	{
		{	/* Integrate/node.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzintegrate_nodez00(void)
	{
		{	/* Integrate/node.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00,
				BGl_proc1948z00zzintegrate_nodez00, BGl_nodez00zzast_nodez00,
				BGl_string1949z00zzintegrate_nodez00);
		}

	}



/* &glo!1316 */
	obj_t BGl_z62gloz121316z70zzintegrate_nodez00(obj_t BgL_envz00_2942,
		obj_t BgL_nodez00_2943, obj_t BgL_integratorz00_2944)
	{
		{	/* Integrate/node.scm 141 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(3),
				BGl_string1950z00zzintegrate_nodez00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_2943)));
		}

	}



/* glo! */
	BgL_nodez00_bglt BGl_gloz12z12zzintegrate_nodez00(BgL_nodez00_bglt
		BgL_nodez00_13, BgL_variablez00_bglt BgL_integratorz00_14)
	{
		{	/* Integrate/node.scm 141 */
			{	/* Integrate/node.scm 141 */
				obj_t BgL_method1317z00_1752;

				{	/* Integrate/node.scm 141 */
					obj_t BgL_res1935z00_2438;

					{	/* Integrate/node.scm 141 */
						long BgL_objzd2classzd2numz00_2409;

						BgL_objzd2classzd2numz00_2409 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_13));
						{	/* Integrate/node.scm 141 */
							obj_t BgL_arg1811z00_2410;

							BgL_arg1811z00_2410 =
								PROCEDURE_REF(BGl_gloz12zd2envzc0zzintegrate_nodez00,
								(int) (1L));
							{	/* Integrate/node.scm 141 */
								int BgL_offsetz00_2413;

								BgL_offsetz00_2413 = (int) (BgL_objzd2classzd2numz00_2409);
								{	/* Integrate/node.scm 141 */
									long BgL_offsetz00_2414;

									BgL_offsetz00_2414 =
										((long) (BgL_offsetz00_2413) - OBJECT_TYPE);
									{	/* Integrate/node.scm 141 */
										long BgL_modz00_2415;

										BgL_modz00_2415 =
											(BgL_offsetz00_2414 >> (int) ((long) ((int) (4L))));
										{	/* Integrate/node.scm 141 */
											long BgL_restz00_2417;

											BgL_restz00_2417 =
												(BgL_offsetz00_2414 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Integrate/node.scm 141 */

												{	/* Integrate/node.scm 141 */
													obj_t BgL_bucketz00_2419;

													BgL_bucketz00_2419 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2410), BgL_modz00_2415);
													BgL_res1935z00_2438 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2419), BgL_restz00_2417);
					}}}}}}}}
					BgL_method1317z00_1752 = BgL_res1935z00_2438;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL2(BgL_method1317z00_1752,
						((obj_t) BgL_nodez00_13), ((obj_t) BgL_integratorz00_14)));
			}
		}

	}



/* &glo! */
	BgL_nodez00_bglt BGl_z62gloz12z70zzintegrate_nodez00(obj_t BgL_envz00_2945,
		obj_t BgL_nodez00_2946, obj_t BgL_integratorz00_2947)
	{
		{	/* Integrate/node.scm 141 */
			return
				BGl_gloz12z12zzintegrate_nodez00(
				((BgL_nodez00_bglt) BgL_nodez00_2946),
				((BgL_variablez00_bglt) BgL_integratorz00_2947));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzintegrate_nodez00(void)
	{
		{	/* Integrate/node.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_atomz00zzast_nodez00,
				BGl_proc1951z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_kwotez00zzast_nodez00,
				BGl_proc1953z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_varz00zzast_nodez00,
				BGl_proc1954z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_closurez00zzast_nodez00,
				BGl_proc1955z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_sequencez00zzast_nodez00,
				BGl_proc1956z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_syncz00zzast_nodez00,
				BGl_proc1957z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_appz00zzast_nodez00,
				BGl_proc1958z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc1959z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_funcallz00zzast_nodez00,
				BGl_proc1960z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_externz00zzast_nodez00,
				BGl_proc1961z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_castz00zzast_nodez00,
				BGl_proc1962z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_setqz00zzast_nodez00,
				BGl_proc1963z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00,
				BGl_conditionalz00zzast_nodez00, BGl_proc1964z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_failz00zzast_nodez00,
				BGl_proc1965z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_switchz00zzast_nodez00,
				BGl_proc1966z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc1967z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc1968z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1969z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc1970z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc1971z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc1972z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_gloz12zd2envzc0zzintegrate_nodez00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1973z00zzintegrate_nodez00,
				BGl_string1952z00zzintegrate_nodez00);
		}

	}



/* &glo!-box-set!1362 */
	BgL_nodez00_bglt BGl_z62gloz12zd2boxzd2setz121362z62zzintegrate_nodez00(obj_t
		BgL_envz00_2970, obj_t BgL_nodez00_2971, obj_t BgL_integratorz00_2972)
	{
		{	/* Integrate/node.scm 429 */
			{
				BgL_varz00_bglt BgL_auxz00_3801;

				{	/* Tools/trace.sch 53 */
					BgL_varz00_bglt BgL_arg1852z00_3050;

					BgL_arg1852z00_3050 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2971)))->BgL_varz00);
					BgL_auxz00_3801 =
						((BgL_varz00_bglt)
						BGl_gloz12z12zzintegrate_nodez00(
							((BgL_nodez00_bglt) BgL_arg1852z00_3050),
							((BgL_variablez00_bglt) BgL_integratorz00_2972)));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2971)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3801), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3810;

				{	/* Tools/trace.sch 53 */
					BgL_nodez00_bglt BgL_arg1853z00_3051;

					BgL_arg1853z00_3051 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2971)))->BgL_valuez00);
					BgL_auxz00_3810 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1853z00_3051,
						((BgL_variablez00_bglt) BgL_integratorz00_2972));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2971)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3810), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2971));
		}

	}



/* &glo!-box-ref1360 */
	BgL_nodez00_bglt BGl_z62gloz12zd2boxzd2ref1360z70zzintegrate_nodez00(obj_t
		BgL_envz00_2973, obj_t BgL_nodez00_2974, obj_t BgL_integratorz00_2975)
	{
		{	/* Integrate/node.scm 415 */
			{	/* Tools/trace.sch 53 */
				bool_t BgL_test2050z00_3819;

				{	/* Tools/trace.sch 53 */
					BgL_variablez00_bglt BgL_arg1849z00_3053;

					BgL_arg1849z00_3053 =
						(((BgL_varz00_bglt) COBJECT(
								(((BgL_boxzd2refzd2_bglt) COBJECT(
											((BgL_boxzd2refzd2_bglt) BgL_nodez00_2974)))->
									BgL_varz00)))->BgL_variablez00);
					BgL_test2050z00_3819 =
						BGl_integratezd2celledzf3z21zzintegrate_nodez00(((BgL_localz00_bglt)
							BgL_arg1849z00_3053));
				}
				if (BgL_test2050z00_3819)
					{	/* Tools/trace.sch 53 */
						BgL_variablez00_bglt BgL_vz00_3054;

						BgL_vz00_3054 =
							(((BgL_varz00_bglt) COBJECT(
									(((BgL_boxzd2refzd2_bglt) COBJECT(
												((BgL_boxzd2refzd2_bglt) BgL_nodez00_2974)))->
										BgL_varz00)))->BgL_variablez00);
						{	/* Tools/trace.sch 53 */
							obj_t BgL_arg1831z00_3055;
							obj_t BgL_arg1832z00_3056;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_tmpz00_3828;

								BgL_tmpz00_3828 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_arg1831z00_3055 =
									BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_3828);
							}
							BgL_arg1832z00_3056 =
								BGl_shapez00zztools_shapez00(
								((obj_t) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2974)));
							{	/* Tools/trace.sch 53 */
								obj_t BgL_list1833z00_3057;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_arg1834z00_3058;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg1835z00_3059;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1836z00_3060;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_arg1837z00_3061;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_arg1838z00_3062;

													BgL_arg1838z00_3062 =
														MAKE_YOUNG_PAIR(BgL_arg1832z00_3056, BNIL);
													BgL_arg1837z00_3061 =
														MAKE_YOUNG_PAIR
														(BGl_string1974z00zzintegrate_nodez00,
														BgL_arg1838z00_3062);
												}
												BgL_arg1836z00_3060 =
													MAKE_YOUNG_PAIR(BGl_string1975z00zzintegrate_nodez00,
													BgL_arg1837z00_3061);
											}
											BgL_arg1835z00_3059 =
												MAKE_YOUNG_PAIR(BINT(420L), BgL_arg1836z00_3060);
										}
										BgL_arg1834z00_3058 =
											MAKE_YOUNG_PAIR(BGl_string1976z00zzintegrate_nodez00,
											BgL_arg1835z00_3059);
									}
									BgL_list1833z00_3057 =
										MAKE_YOUNG_PAIR(BGl_string1977z00zzintegrate_nodez00,
										BgL_arg1834z00_3058);
								}
								BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1831z00_3055,
									BgL_list1833z00_3057);
							}
						}
						{	/* Tools/trace.sch 53 */
							obj_t BgL_arg1839z00_3063;
							obj_t BgL_arg1840z00_3064;

							{	/* Tools/trace.sch 53 */
								obj_t BgL_tmpz00_3842;

								BgL_tmpz00_3842 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_arg1839z00_3063 =
									BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_3842);
							}
							BgL_arg1840z00_3064 =
								(((BgL_variablez00_bglt) COBJECT(BgL_vz00_3054))->
								BgL_accessz00);
							{	/* Tools/trace.sch 53 */
								obj_t BgL_list1841z00_3065;

								{	/* Tools/trace.sch 53 */
									obj_t BgL_arg1842z00_3066;

									{	/* Tools/trace.sch 53 */
										obj_t BgL_arg1843z00_3067;

										{	/* Tools/trace.sch 53 */
											obj_t BgL_arg1844z00_3068;

											{	/* Tools/trace.sch 53 */
												obj_t BgL_arg1845z00_3069;

												{	/* Tools/trace.sch 53 */
													obj_t BgL_arg1846z00_3070;

													BgL_arg1846z00_3070 =
														MAKE_YOUNG_PAIR(BgL_arg1840z00_3064, BNIL);
													BgL_arg1845z00_3069 =
														MAKE_YOUNG_PAIR
														(BGl_string1978z00zzintegrate_nodez00,
														BgL_arg1846z00_3070);
												}
												BgL_arg1844z00_3068 =
													MAKE_YOUNG_PAIR(BGl_string1975z00zzintegrate_nodez00,
													BgL_arg1845z00_3069);
											}
											BgL_arg1843z00_3067 =
												MAKE_YOUNG_PAIR(BINT(421L), BgL_arg1844z00_3068);
										}
										BgL_arg1842z00_3066 =
											MAKE_YOUNG_PAIR(BGl_string1976z00zzintegrate_nodez00,
											BgL_arg1843z00_3067);
									}
									BgL_list1841z00_3065 =
										MAKE_YOUNG_PAIR(BGl_string1977z00zzintegrate_nodez00,
										BgL_arg1842z00_3066);
								}
								BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1839z00_3063,
									BgL_list1841z00_3065);
							}
						}
						{	/* Tools/trace.sch 53 */
							obj_t BgL_arg1847z00_3071;

							BgL_arg1847z00_3071 =
								BGl_shapez00zztools_shapez00(
								((obj_t) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2974)));
							BGl_errorz00zz__errorz00(BGl_string1979z00zzintegrate_nodez00,
								BGl_string1980z00zzintegrate_nodez00, BgL_arg1847z00_3071);
						}
					}
				else
					{	/* Tools/trace.sch 53 */
						BFALSE;
					}
			}
			{
				BgL_varz00_bglt BgL_auxz00_3858;

				{	/* Tools/trace.sch 53 */
					BgL_varz00_bglt BgL_arg1851z00_3072;

					BgL_arg1851z00_3072 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2974)))->BgL_varz00);
					BgL_auxz00_3858 =
						((BgL_varz00_bglt)
						BGl_gloz12z12zzintegrate_nodez00(
							((BgL_nodez00_bglt) BgL_arg1851z00_3072),
							((BgL_variablez00_bglt) BgL_integratorz00_2975)));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_2974)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_3858), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_2974));
		}

	}



/* &glo!-make-box1358 */
	BgL_nodez00_bglt BGl_z62gloz12zd2makezd2box1358z70zzintegrate_nodez00(obj_t
		BgL_envz00_2976, obj_t BgL_nodez00_2977, obj_t BgL_integratorz00_2978)
	{
		{	/* Integrate/node.scm 407 */
			{
				BgL_nodez00_bglt BgL_auxz00_3869;

				{	/* Integrate/node.scm 409 */
					BgL_nodez00_bglt BgL_arg1820z00_3074;

					BgL_arg1820z00_3074 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2977)))->BgL_valuez00);
					BgL_auxz00_3869 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1820z00_3074,
						((BgL_variablez00_bglt) BgL_integratorz00_2978));
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_2977)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_3869), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_2977));
		}

	}



/* &glo!-jump-ex-it1356 */
	BgL_nodez00_bglt
		BGl_z62gloz12zd2jumpzd2exzd2it1356za2zzintegrate_nodez00(obj_t
		BgL_envz00_2979, obj_t BgL_nodez00_2980, obj_t BgL_integratorz00_2981)
	{
		{	/* Integrate/node.scm 398 */
			{
				BgL_nodez00_bglt BgL_auxz00_3878;

				{	/* Integrate/node.scm 400 */
					BgL_nodez00_bglt BgL_arg1808z00_3076;

					BgL_arg1808z00_3076 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2980)))->BgL_exitz00);
					BgL_auxz00_3878 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1808z00_3076,
						((BgL_variablez00_bglt) BgL_integratorz00_2981));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2980)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_3878), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3885;

				{	/* Integrate/node.scm 401 */
					BgL_nodez00_bglt BgL_arg1812z00_3077;

					BgL_arg1812z00_3077 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2980)))->
						BgL_valuez00);
					BgL_auxz00_3885 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1812z00_3077,
						((BgL_variablez00_bglt) BgL_integratorz00_2981));
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2980)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_3885), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2980));
		}

	}



/* &glo!-set-ex-it1354 */
	BgL_nodez00_bglt BGl_z62gloz12zd2setzd2exzd2it1354za2zzintegrate_nodez00(obj_t
		BgL_envz00_2982, obj_t BgL_nodez00_2983, obj_t BgL_integratorz00_2984)
	{
		{	/* Integrate/node.scm 389 */
			{
				BgL_nodez00_bglt BgL_auxz00_3894;

				{	/* Integrate/node.scm 391 */
					BgL_nodez00_bglt BgL_arg1805z00_3079;

					BgL_arg1805z00_3079 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2983)))->
						BgL_onexitz00);
					BgL_auxz00_3894 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1805z00_3079,
						((BgL_variablez00_bglt) BgL_integratorz00_2984));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2983)))->
						BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_auxz00_3894), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3901;

				{	/* Integrate/node.scm 392 */
					BgL_nodez00_bglt BgL_arg1806z00_3080;

					BgL_arg1806z00_3080 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2983)))->BgL_bodyz00);
					BgL_auxz00_3901 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1806z00_3080,
						((BgL_variablez00_bglt) BgL_integratorz00_2984));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2983)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3901), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2983));
		}

	}



/* &glo!-let-var1352 */
	BgL_nodez00_bglt BGl_z62gloz12zd2letzd2var1352z70zzintegrate_nodez00(obj_t
		BgL_envz00_2985, obj_t BgL_nodez00_2986, obj_t BgL_integratorz00_2987)
	{
		{	/* Integrate/node.scm 364 */
			{	/* Integrate/node.scm 366 */
				obj_t BgL_g1308z00_3082;

				BgL_g1308z00_3082 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2986)))->BgL_bindingsz00);
				{
					obj_t BgL_l1306z00_3084;

					BgL_l1306z00_3084 = BgL_g1308z00_3082;
				BgL_zc3z04anonymousza31762ze3z87_3083:
					if (PAIRP(BgL_l1306z00_3084))
						{	/* Integrate/node.scm 366 */
							{	/* Integrate/node.scm 367 */
								obj_t BgL_bindingz00_3085;

								BgL_bindingz00_3085 = CAR(BgL_l1306z00_3084);
								{	/* Integrate/node.scm 367 */
									obj_t BgL_varz00_3086;
									obj_t BgL_valz00_3087;

									BgL_varz00_3086 = CAR(((obj_t) BgL_bindingz00_3085));
									BgL_valz00_3087 = CDR(((obj_t) BgL_bindingz00_3085));
									{	/* Integrate/node.scm 369 */
										BgL_nodez00_bglt BgL_arg1765z00_3088;

										BgL_arg1765z00_3088 =
											BGl_gloz12z12zzintegrate_nodez00(
											((BgL_nodez00_bglt) BgL_valz00_3087),
											((BgL_variablez00_bglt) BgL_integratorz00_2987));
										{	/* Integrate/node.scm 369 */
											obj_t BgL_auxz00_3924;
											obj_t BgL_tmpz00_3922;

											BgL_auxz00_3924 = ((obj_t) BgL_arg1765z00_3088);
											BgL_tmpz00_3922 = ((obj_t) BgL_bindingz00_3085);
											SET_CDR(BgL_tmpz00_3922, BgL_auxz00_3924);
										}
									}
									if (BGl_integratezd2celledzf3z21zzintegrate_nodez00(
											((BgL_localz00_bglt) BgL_varz00_3086)))
										{	/* Integrate/node.scm 371 */
											BgL_localz00_bglt BgL_nvarz00_3089;

											BgL_nvarz00_3089 =
												BGl_cellzd2variablezd2zzintegrate_nodez00(
												((BgL_localz00_bglt) BgL_varz00_3086));
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt) BgL_varz00_3086))))->
													BgL_fastzd2alphazd2) =
												((obj_t) ((obj_t) BgL_nvarz00_3089)), BUNSPEC);
											{	/* Integrate/node.scm 374 */
												BgL_makezd2boxzd2_bglt BgL_arg1767z00_3090;

												{	/* Integrate/node.scm 374 */
													obj_t BgL_arg1770z00_3091;

													BgL_arg1770z00_3091 =
														CDR(((obj_t) BgL_bindingz00_3085));
													BgL_arg1767z00_3090 =
														BGl_azd2makezd2cellz00zzintegrate_nodez00(
														((BgL_nodez00_bglt) BgL_arg1770z00_3091),
														((BgL_variablez00_bglt) BgL_varz00_3086));
												}
												{	/* Integrate/node.scm 373 */
													obj_t BgL_auxz00_3943;
													obj_t BgL_tmpz00_3941;

													BgL_auxz00_3943 = ((obj_t) BgL_arg1767z00_3090);
													BgL_tmpz00_3941 = ((obj_t) BgL_bindingz00_3085);
													SET_CDR(BgL_tmpz00_3941, BgL_auxz00_3943);
												}
											}
										}
									else
										{	/* Integrate/node.scm 370 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1306z00_3946;

								BgL_l1306z00_3946 = CDR(BgL_l1306z00_3084);
								BgL_l1306z00_3084 = BgL_l1306z00_3946;
								goto BgL_zc3z04anonymousza31762ze3z87_3083;
							}
						}
					else
						{	/* Integrate/node.scm 366 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_3948;

				{	/* Integrate/node.scm 376 */
					BgL_nodez00_bglt BgL_arg1773z00_3092;

					BgL_arg1773z00_3092 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2986)))->BgL_bodyz00);
					BgL_auxz00_3948 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1773z00_3092,
						((BgL_variablez00_bglt) BgL_integratorz00_2987));
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_2986)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3948), BUNSPEC);
			}
			{	/* Integrate/node.scm 377 */
				obj_t BgL_g1311z00_3093;

				BgL_g1311z00_3093 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2986)))->BgL_bindingsz00);
				{
					obj_t BgL_l1309z00_3095;

					BgL_l1309z00_3095 = BgL_g1311z00_3093;
				BgL_zc3z04anonymousza31774ze3z87_3094:
					if (PAIRP(BgL_l1309z00_3095))
						{	/* Integrate/node.scm 377 */
							{	/* Integrate/node.scm 378 */
								obj_t BgL_bindingz00_3096;

								BgL_bindingz00_3096 = CAR(BgL_l1309z00_3095);
								{	/* Integrate/node.scm 378 */
									obj_t BgL_varz00_3097;

									BgL_varz00_3097 = CAR(((obj_t) BgL_bindingz00_3096));
									if (BGl_integratezd2celledzf3z21zzintegrate_nodez00(
											((BgL_localz00_bglt) BgL_varz00_3097)))
										{	/* Integrate/node.scm 380 */
											BgL_localz00_bglt BgL_nvarz00_3098;

											BgL_nvarz00_3098 =
												BGl_cellzd2variablezd2zzintegrate_nodez00(
												((BgL_localz00_bglt) BgL_varz00_3097));
											{	/* Integrate/node.scm 381 */
												obj_t BgL_arg1798z00_3099;

												BgL_arg1798z00_3099 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_varz00_3097)))->
													BgL_fastzd2alphazd2);
												{	/* Integrate/node.scm 381 */
													obj_t BgL_tmpz00_3969;

													BgL_tmpz00_3969 = ((obj_t) BgL_bindingz00_3096);
													SET_CAR(BgL_tmpz00_3969, BgL_arg1798z00_3099);
												}
											}
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt) BgL_varz00_3097))))->
													BgL_fastzd2alphazd2) = ((obj_t) BUNSPEC), BUNSPEC);
										}
									else
										{	/* Integrate/node.scm 379 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1309z00_3975;

								BgL_l1309z00_3975 = CDR(BgL_l1309z00_3095);
								BgL_l1309z00_3095 = BgL_l1309z00_3975;
								goto BgL_zc3z04anonymousza31774ze3z87_3094;
							}
						}
					else
						{	/* Integrate/node.scm 377 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_2986));
		}

	}



/* &glo!-let-fun1350 */
	BgL_nodez00_bglt BGl_z62gloz12zd2letzd2fun1350z70zzintegrate_nodez00(obj_t
		BgL_envz00_2988, obj_t BgL_nodez00_2989, obj_t BgL_integratorz00_2990)
	{
		{	/* Integrate/node.scm 353 */
			{
				BgL_nodez00_bglt BgL_auxz00_3979;

				{	/* Integrate/node.scm 355 */
					BgL_nodez00_bglt BgL_arg1754z00_3101;

					BgL_arg1754z00_3101 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2989)))->BgL_bodyz00);
					BgL_auxz00_3979 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1754z00_3101,
						((BgL_variablez00_bglt) BgL_integratorz00_2990));
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_2989)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3979), BUNSPEC);
			}
			{	/* Integrate/node.scm 356 */
				obj_t BgL_g1305z00_3102;

				BgL_g1305z00_3102 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2989)))->BgL_localsz00);
				{
					obj_t BgL_l1303z00_3104;

					BgL_l1303z00_3104 = BgL_g1305z00_3102;
				BgL_zc3z04anonymousza31755ze3z87_3103:
					if (PAIRP(BgL_l1303z00_3104))
						{	/* Integrate/node.scm 356 */
							{	/* Integrate/node.scm 357 */
								obj_t BgL_localz00_3105;

								BgL_localz00_3105 = CAR(BgL_l1303z00_3104);
								BGl_globaliza7ezd2localzd2funz12zb5zzintegrate_nodez00(
									((BgL_localz00_bglt) BgL_localz00_3105),
									((BgL_variablez00_bglt) BgL_integratorz00_2990));
							}
							{
								obj_t BgL_l1303z00_3994;

								BgL_l1303z00_3994 = CDR(BgL_l1303z00_3104);
								BgL_l1303z00_3104 = BgL_l1303z00_3994;
								goto BgL_zc3z04anonymousza31755ze3z87_3103;
							}
						}
					else
						{	/* Integrate/node.scm 356 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_2989));
		}

	}



/* &glo!-switch1348 */
	BgL_nodez00_bglt BGl_z62gloz12zd2switch1348za2zzintegrate_nodez00(obj_t
		BgL_envz00_2991, obj_t BgL_nodez00_2992, obj_t BgL_integratorz00_2993)
	{
		{	/* Integrate/node.scm 342 */
			{
				BgL_nodez00_bglt BgL_auxz00_3998;

				{	/* Integrate/node.scm 344 */
					BgL_nodez00_bglt BgL_arg1747z00_3107;

					BgL_arg1747z00_3107 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2992)))->BgL_testz00);
					BgL_auxz00_3998 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1747z00_3107,
						((BgL_variablez00_bglt) BgL_integratorz00_2993));
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2992)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_3998), BUNSPEC);
			}
			{	/* Integrate/node.scm 345 */
				obj_t BgL_g1302z00_3108;

				BgL_g1302z00_3108 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_2992)))->BgL_clausesz00);
				{
					obj_t BgL_l1300z00_3110;

					BgL_l1300z00_3110 = BgL_g1302z00_3108;
				BgL_zc3z04anonymousza31748ze3z87_3109:
					if (PAIRP(BgL_l1300z00_3110))
						{	/* Integrate/node.scm 345 */
							{	/* Integrate/node.scm 346 */
								obj_t BgL_clausez00_3111;

								BgL_clausez00_3111 = CAR(BgL_l1300z00_3110);
								{	/* Integrate/node.scm 346 */
									BgL_nodez00_bglt BgL_arg1751z00_3112;

									{	/* Integrate/node.scm 346 */
										obj_t BgL_arg1752z00_3113;

										BgL_arg1752z00_3113 = CDR(((obj_t) BgL_clausez00_3111));
										BgL_arg1751z00_3112 =
											BGl_gloz12z12zzintegrate_nodez00(
											((BgL_nodez00_bglt) BgL_arg1752z00_3113),
											((BgL_variablez00_bglt) BgL_integratorz00_2993));
									}
									{	/* Integrate/node.scm 346 */
										obj_t BgL_auxz00_4017;
										obj_t BgL_tmpz00_4015;

										BgL_auxz00_4017 = ((obj_t) BgL_arg1751z00_3112);
										BgL_tmpz00_4015 = ((obj_t) BgL_clausez00_3111);
										SET_CDR(BgL_tmpz00_4015, BgL_auxz00_4017);
									}
								}
							}
							{
								obj_t BgL_l1300z00_4020;

								BgL_l1300z00_4020 = CDR(BgL_l1300z00_3110);
								BgL_l1300z00_3110 = BgL_l1300z00_4020;
								goto BgL_zc3z04anonymousza31748ze3z87_3109;
							}
						}
					else
						{	/* Integrate/node.scm 345 */
							((bool_t) 1);
						}
				}
			}
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_2992));
		}

	}



/* &glo!-fail1346 */
	BgL_nodez00_bglt BGl_z62gloz12zd2fail1346za2zzintegrate_nodez00(obj_t
		BgL_envz00_2994, obj_t BgL_nodez00_2995, obj_t BgL_integratorz00_2996)
	{
		{	/* Integrate/node.scm 332 */
			{
				BgL_nodez00_bglt BgL_auxz00_4024;

				{	/* Integrate/node.scm 334 */
					BgL_nodez00_bglt BgL_arg1739z00_3115;

					BgL_arg1739z00_3115 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2995)))->BgL_procz00);
					BgL_auxz00_4024 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1739z00_3115,
						((BgL_variablez00_bglt) BgL_integratorz00_2996));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2995)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4024), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4031;

				{	/* Integrate/node.scm 335 */
					BgL_nodez00_bglt BgL_arg1740z00_3116;

					BgL_arg1740z00_3116 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2995)))->BgL_msgz00);
					BgL_auxz00_4031 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1740z00_3116,
						((BgL_variablez00_bglt) BgL_integratorz00_2996));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2995)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4031), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4038;

				{	/* Integrate/node.scm 336 */
					BgL_nodez00_bglt BgL_arg1746z00_3117;

					BgL_arg1746z00_3117 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2995)))->BgL_objz00);
					BgL_auxz00_4038 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1746z00_3117,
						((BgL_variablez00_bglt) BgL_integratorz00_2996));
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_2995)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4038), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_2995));
		}

	}



/* &glo!-conditional1344 */
	BgL_nodez00_bglt BGl_z62gloz12zd2conditional1344za2zzintegrate_nodez00(obj_t
		BgL_envz00_2997, obj_t BgL_nodez00_2998, obj_t BgL_integratorz00_2999)
	{
		{	/* Integrate/node.scm 322 */
			{
				BgL_nodez00_bglt BgL_auxz00_4047;

				{	/* Integrate/node.scm 324 */
					BgL_nodez00_bglt BgL_arg1736z00_3119;

					BgL_arg1736z00_3119 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2998)))->BgL_testz00);
					BgL_auxz00_4047 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1736z00_3119,
						((BgL_variablez00_bglt) BgL_integratorz00_2999));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2998)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4047), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4054;

				{	/* Integrate/node.scm 325 */
					BgL_nodez00_bglt BgL_arg1737z00_3120;

					BgL_arg1737z00_3120 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2998)))->BgL_truez00);
					BgL_auxz00_4054 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1737z00_3120,
						((BgL_variablez00_bglt) BgL_integratorz00_2999));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2998)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_4054), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4061;

				{	/* Integrate/node.scm 326 */
					BgL_nodez00_bglt BgL_arg1738z00_3121;

					BgL_arg1738z00_3121 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2998)))->BgL_falsez00);
					BgL_auxz00_4061 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1738z00_3121,
						((BgL_variablez00_bglt) BgL_integratorz00_2999));
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_2998)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_4061), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_2998));
		}

	}



/* &glo!-setq1342 */
	BgL_nodez00_bglt BGl_z62gloz12zd2setq1342za2zzintegrate_nodez00(obj_t
		BgL_envz00_3000, obj_t BgL_nodez00_3001, obj_t BgL_integratorz00_3002)
	{
		{	/* Integrate/node.scm 285 */
			{
				BgL_nodez00_bglt BgL_auxz00_4070;

				{	/* Integrate/node.scm 287 */
					BgL_nodez00_bglt BgL_arg1705z00_3123;

					BgL_arg1705z00_3123 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_3001)))->BgL_valuez00);
					BgL_auxz00_4070 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1705z00_3123,
						((BgL_variablez00_bglt) BgL_integratorz00_3002));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_3001)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_4070), BUNSPEC);
			}
			{	/* Integrate/node.scm 288 */
				BgL_variablez00_bglt BgL_varz00_3124;

				BgL_varz00_3124 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_setqz00_bglt) COBJECT(
										((BgL_setqz00_bglt) BgL_nodez00_3001)))->BgL_varz00)))->
					BgL_variablez00);
				{	/* Integrate/node.scm 288 */
					BgL_typez00_bglt BgL_vtypez00_3125;

					BgL_vtypez00_3125 =
						(((BgL_variablez00_bglt) COBJECT(BgL_varz00_3124))->BgL_typez00);
					{	/* Integrate/node.scm 289 */

						{	/* Integrate/node.scm 290 */
							obj_t BgL_g1143z00_3126;

							BgL_g1143z00_3126 =
								(((BgL_variablez00_bglt) COBJECT(BgL_varz00_3124))->
								BgL_fastzd2alphazd2);
							{
								obj_t BgL_varz00_3128;
								obj_t BgL_alphaz00_3129;

								BgL_varz00_3128 = ((obj_t) BgL_varz00_3124);
								BgL_alphaz00_3129 = BgL_g1143z00_3126;
							BgL_loopz00_3127:
								{	/* Integrate/node.scm 292 */
									bool_t BgL_test2057z00_4082;

									{	/* Integrate/node.scm 292 */
										obj_t BgL_classz00_3130;

										BgL_classz00_3130 = BGl_localz00zzast_varz00;
										if (BGL_OBJECTP(BgL_alphaz00_3129))
											{	/* Integrate/node.scm 292 */
												BgL_objectz00_bglt BgL_arg1807z00_3131;

												BgL_arg1807z00_3131 =
													(BgL_objectz00_bglt) (BgL_alphaz00_3129);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Integrate/node.scm 292 */
														long BgL_idxz00_3132;

														BgL_idxz00_3132 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3131);
														BgL_test2057z00_4082 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3132 + 2L)) == BgL_classz00_3130);
													}
												else
													{	/* Integrate/node.scm 292 */
														bool_t BgL_res1941z00_3135;

														{	/* Integrate/node.scm 292 */
															obj_t BgL_oclassz00_3136;

															{	/* Integrate/node.scm 292 */
																obj_t BgL_arg1815z00_3137;
																long BgL_arg1816z00_3138;

																BgL_arg1815z00_3137 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Integrate/node.scm 292 */
																	long BgL_arg1817z00_3139;

																	BgL_arg1817z00_3139 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3131);
																	BgL_arg1816z00_3138 =
																		(BgL_arg1817z00_3139 - OBJECT_TYPE);
																}
																BgL_oclassz00_3136 =
																	VECTOR_REF(BgL_arg1815z00_3137,
																	BgL_arg1816z00_3138);
															}
															{	/* Integrate/node.scm 292 */
																bool_t BgL__ortest_1115z00_3140;

																BgL__ortest_1115z00_3140 =
																	(BgL_classz00_3130 == BgL_oclassz00_3136);
																if (BgL__ortest_1115z00_3140)
																	{	/* Integrate/node.scm 292 */
																		BgL_res1941z00_3135 =
																			BgL__ortest_1115z00_3140;
																	}
																else
																	{	/* Integrate/node.scm 292 */
																		long BgL_odepthz00_3141;

																		{	/* Integrate/node.scm 292 */
																			obj_t BgL_arg1804z00_3142;

																			BgL_arg1804z00_3142 =
																				(BgL_oclassz00_3136);
																			BgL_odepthz00_3141 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3142);
																		}
																		if ((2L < BgL_odepthz00_3141))
																			{	/* Integrate/node.scm 292 */
																				obj_t BgL_arg1802z00_3143;

																				{	/* Integrate/node.scm 292 */
																					obj_t BgL_arg1803z00_3144;

																					BgL_arg1803z00_3144 =
																						(BgL_oclassz00_3136);
																					BgL_arg1802z00_3143 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3144, 2L);
																				}
																				BgL_res1941z00_3135 =
																					(BgL_arg1802z00_3143 ==
																					BgL_classz00_3130);
																			}
																		else
																			{	/* Integrate/node.scm 292 */
																				BgL_res1941z00_3135 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2057z00_4082 = BgL_res1941z00_3135;
													}
											}
										else
											{	/* Integrate/node.scm 292 */
												BgL_test2057z00_4082 = ((bool_t) 0);
											}
									}
									if (BgL_test2057z00_4082)
										{	/* Integrate/node.scm 292 */
											{	/* Integrate/node.scm 294 */
												BgL_varz00_bglt BgL_arg1708z00_3145;

												BgL_arg1708z00_3145 =
													(((BgL_setqz00_bglt) COBJECT(
															((BgL_setqz00_bglt) BgL_nodez00_3001)))->
													BgL_varz00);
												((((BgL_varz00_bglt) COBJECT(BgL_arg1708z00_3145))->
														BgL_variablez00) =
													((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
															BgL_alphaz00_3129)), BUNSPEC);
											}
											{	/* Integrate/node.scm 295 */
												obj_t BgL_arg1709z00_3146;

												BgL_arg1709z00_3146 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_alphaz00_3129)))->
													BgL_fastzd2alphazd2);
												{
													obj_t BgL_alphaz00_4112;
													obj_t BgL_varz00_4111;

													BgL_varz00_4111 = BgL_alphaz00_3129;
													BgL_alphaz00_4112 = BgL_arg1709z00_3146;
													BgL_alphaz00_3129 = BgL_alphaz00_4112;
													BgL_varz00_3128 = BgL_varz00_4111;
													goto BgL_loopz00_3127;
												}
											}
										}
									else
										{	/* Integrate/node.scm 296 */
											BgL_variablez00_bglt BgL_varz00_3147;

											BgL_varz00_3147 =
												(((BgL_varz00_bglt) COBJECT(
														(((BgL_setqz00_bglt) COBJECT(
																	((BgL_setqz00_bglt) BgL_nodez00_3001)))->
															BgL_varz00)))->BgL_variablez00);
											{	/* Integrate/node.scm 297 */
												bool_t BgL_test2062z00_4116;

												{	/* Integrate/node.scm 297 */
													bool_t BgL_test2063z00_4117;

													{	/* Integrate/node.scm 297 */
														obj_t BgL_classz00_3148;

														BgL_classz00_3148 = BGl_localz00zzast_varz00;
														{	/* Integrate/node.scm 297 */
															BgL_objectz00_bglt BgL_arg1807z00_3149;

															{	/* Integrate/node.scm 297 */
																obj_t BgL_tmpz00_4118;

																BgL_tmpz00_4118 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_varz00_3147));
																BgL_arg1807z00_3149 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_4118);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Integrate/node.scm 297 */
																	long BgL_idxz00_3150;

																	BgL_idxz00_3150 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3149);
																	BgL_test2063z00_4117 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3150 + 2L)) ==
																		BgL_classz00_3148);
																}
															else
																{	/* Integrate/node.scm 297 */
																	bool_t BgL_res1942z00_3153;

																	{	/* Integrate/node.scm 297 */
																		obj_t BgL_oclassz00_3154;

																		{	/* Integrate/node.scm 297 */
																			obj_t BgL_arg1815z00_3155;
																			long BgL_arg1816z00_3156;

																			BgL_arg1815z00_3155 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Integrate/node.scm 297 */
																				long BgL_arg1817z00_3157;

																				BgL_arg1817z00_3157 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3149);
																				BgL_arg1816z00_3156 =
																					(BgL_arg1817z00_3157 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3154 =
																				VECTOR_REF(BgL_arg1815z00_3155,
																				BgL_arg1816z00_3156);
																		}
																		{	/* Integrate/node.scm 297 */
																			bool_t BgL__ortest_1115z00_3158;

																			BgL__ortest_1115z00_3158 =
																				(BgL_classz00_3148 ==
																				BgL_oclassz00_3154);
																			if (BgL__ortest_1115z00_3158)
																				{	/* Integrate/node.scm 297 */
																					BgL_res1942z00_3153 =
																						BgL__ortest_1115z00_3158;
																				}
																			else
																				{	/* Integrate/node.scm 297 */
																					long BgL_odepthz00_3159;

																					{	/* Integrate/node.scm 297 */
																						obj_t BgL_arg1804z00_3160;

																						BgL_arg1804z00_3160 =
																							(BgL_oclassz00_3154);
																						BgL_odepthz00_3159 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3160);
																					}
																					if ((2L < BgL_odepthz00_3159))
																						{	/* Integrate/node.scm 297 */
																							obj_t BgL_arg1802z00_3161;

																							{	/* Integrate/node.scm 297 */
																								obj_t BgL_arg1803z00_3162;

																								BgL_arg1803z00_3162 =
																									(BgL_oclassz00_3154);
																								BgL_arg1802z00_3161 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3162, 2L);
																							}
																							BgL_res1942z00_3153 =
																								(BgL_arg1802z00_3161 ==
																								BgL_classz00_3148);
																						}
																					else
																						{	/* Integrate/node.scm 297 */
																							BgL_res1942z00_3153 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2063z00_4117 = BgL_res1942z00_3153;
																}
														}
													}
													if (BgL_test2063z00_4117)
														{	/* Integrate/node.scm 297 */
															BgL_test2062z00_4116 =
																BGl_integratezd2celledzf3z21zzintegrate_nodez00(
																((BgL_localz00_bglt) BgL_varz00_3147));
														}
													else
														{	/* Integrate/node.scm 297 */
															BgL_test2062z00_4116 = ((bool_t) 0);
														}
												}
												if (BgL_test2062z00_4116)
													{	/* Integrate/node.scm 298 */
														BgL_localz00_bglt BgL_azd2varzd2_3163;
														obj_t BgL_locz00_3164;

														BgL_azd2varzd2_3163 =
															BGl_makezd2localzd2svarz00zzast_localz00
															(CNST_TABLE_REF(4),
															((BgL_typez00_bglt)
																BGl_za2objza2z00zztype_cachez00));
														BgL_locz00_3164 =
															(((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt) ((BgL_setqz00_bglt)
																			BgL_nodez00_3001))))->BgL_locz00);
														{	/* Integrate/node.scm 300 */
															obj_t BgL_vz00_3165;

															BgL_vz00_3165 = CNST_TABLE_REF(0);
															((((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					BgL_varz00_3147))))->BgL_accessz00) =
																((obj_t) BgL_vz00_3165), BUNSPEC);
														}
														{	/* Integrate/node.scm 301 */
															BgL_varz00_bglt BgL_arg1714z00_3166;

															BgL_arg1714z00_3166 =
																(((BgL_setqz00_bglt) COBJECT(
																		((BgL_setqz00_bglt) BgL_nodez00_3001)))->
																BgL_varz00);
															{	/* Integrate/node.scm 301 */
																BgL_typez00_bglt BgL_vz00_3167;

																BgL_vz00_3167 =
																	((BgL_typez00_bglt)
																	BGl_za2objza2z00zztype_cachez00);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_arg1714z00_3166)))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_vz00_3167), BUNSPEC);
															}
														}
														{	/* Integrate/node.scm 302 */
															BgL_svarz00_bglt BgL_tmp1144z00_3168;

															BgL_tmp1144z00_3168 =
																((BgL_svarz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				BgL_azd2varzd2_3163)))->BgL_valuez00));
															{	/* Integrate/node.scm 302 */
																BgL_svarzf2iinfozf2_bglt BgL_wide1146z00_3169;

																BgL_wide1146z00_3169 =
																	((BgL_svarzf2iinfozf2_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_svarzf2iinfozf2_bgl))));
																{	/* Integrate/node.scm 302 */
																	obj_t BgL_auxz00_4165;
																	BgL_objectz00_bglt BgL_tmpz00_4162;

																	BgL_auxz00_4165 =
																		((obj_t) BgL_wide1146z00_3169);
																	BgL_tmpz00_4162 =
																		((BgL_objectz00_bglt)
																		((BgL_svarz00_bglt) BgL_tmp1144z00_3168));
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4162,
																		BgL_auxz00_4165);
																}
																((BgL_objectz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1144z00_3168));
																{	/* Integrate/node.scm 302 */
																	long BgL_arg1717z00_3170;

																	BgL_arg1717z00_3170 =
																		BGL_CLASS_NUM
																		(BGl_svarzf2Iinfozf2zzintegrate_infoz00);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1144z00_3168)),
																		BgL_arg1717z00_3170);
																}
																((BgL_svarz00_bglt)
																	((BgL_svarz00_bglt) BgL_tmp1144z00_3168));
															}
															{
																BgL_svarzf2iinfozf2_bglt BgL_auxz00_4176;

																{
																	obj_t BgL_auxz00_4177;

																	{	/* Integrate/node.scm 303 */
																		BgL_objectz00_bglt BgL_tmpz00_4178;

																		BgL_tmpz00_4178 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1144z00_3168));
																		BgL_auxz00_4177 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_4178);
																	}
																	BgL_auxz00_4176 =
																		((BgL_svarzf2iinfozf2_bglt)
																		BgL_auxz00_4177);
																}
																((((BgL_svarzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_4176))->
																		BgL_fzd2markzd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
															}
															{
																BgL_svarzf2iinfozf2_bglt BgL_auxz00_4184;

																{
																	obj_t BgL_auxz00_4185;

																	{	/* Integrate/node.scm 303 */
																		BgL_objectz00_bglt BgL_tmpz00_4186;

																		BgL_tmpz00_4186 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1144z00_3168));
																		BgL_auxz00_4185 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_4186);
																	}
																	BgL_auxz00_4184 =
																		((BgL_svarzf2iinfozf2_bglt)
																		BgL_auxz00_4185);
																}
																((((BgL_svarzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_4184))->
																		BgL_uzd2markzd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
															}
															{
																BgL_svarzf2iinfozf2_bglt BgL_auxz00_4192;

																{
																	obj_t BgL_auxz00_4193;

																	{	/* Integrate/node.scm 303 */
																		BgL_objectz00_bglt BgL_tmpz00_4194;

																		BgL_tmpz00_4194 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1144z00_3168));
																		BgL_auxz00_4193 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_4194);
																	}
																	BgL_auxz00_4192 =
																		((BgL_svarzf2iinfozf2_bglt)
																		BgL_auxz00_4193);
																}
																((((BgL_svarzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_4192))->
																		BgL_kapturedzf3zf3) =
																	((bool_t) ((bool_t) 0)), BUNSPEC);
															}
															{
																BgL_svarzf2iinfozf2_bglt BgL_auxz00_4200;

																{
																	obj_t BgL_auxz00_4201;

																	{	/* Integrate/node.scm 303 */
																		BgL_objectz00_bglt BgL_tmpz00_4202;

																		BgL_tmpz00_4202 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1144z00_3168));
																		BgL_auxz00_4201 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_4202);
																	}
																	BgL_auxz00_4200 =
																		((BgL_svarzf2iinfozf2_bglt)
																		BgL_auxz00_4201);
																}
																((((BgL_svarzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_4200))->
																		BgL_celledzf3zf3) =
																	((bool_t) ((bool_t) 0)), BUNSPEC);
															}
															{
																BgL_svarzf2iinfozf2_bglt BgL_auxz00_4208;

																{
																	obj_t BgL_auxz00_4209;

																	{	/* Integrate/node.scm 303 */
																		BgL_objectz00_bglt BgL_tmpz00_4210;

																		BgL_tmpz00_4210 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_tmp1144z00_3168));
																		BgL_auxz00_4209 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_4210);
																	}
																	BgL_auxz00_4208 =
																		((BgL_svarzf2iinfozf2_bglt)
																		BgL_auxz00_4209);
																}
																((((BgL_svarzf2iinfozf2_bglt)
																			COBJECT(BgL_auxz00_4208))->BgL_xhdlz00) =
																	((obj_t) BFALSE), BUNSPEC);
															}
															((BgL_svarz00_bglt) BgL_tmp1144z00_3168);
														}
														{	/* Integrate/node.scm 304 */
															BgL_letzd2varzd2_bglt BgL_new1149z00_3171;

															{	/* Integrate/node.scm 305 */
																BgL_letzd2varzd2_bglt BgL_new1148z00_3172;

																BgL_new1148z00_3172 =
																	((BgL_letzd2varzd2_bglt)
																	BOBJECT(GC_MALLOC(sizeof(struct
																				BgL_letzd2varzd2_bgl))));
																{	/* Integrate/node.scm 305 */
																	long BgL_arg1733z00_3173;

																	BgL_arg1733z00_3173 =
																		BGL_CLASS_NUM
																		(BGl_letzd2varzd2zzast_nodez00);
																	BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
																			BgL_new1148z00_3172),
																		BgL_arg1733z00_3173);
																}
																{	/* Integrate/node.scm 305 */
																	BgL_objectz00_bglt BgL_tmpz00_4221;

																	BgL_tmpz00_4221 =
																		((BgL_objectz00_bglt) BgL_new1148z00_3172);
																	BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4221,
																		BFALSE);
																}
																((BgL_objectz00_bglt) BgL_new1148z00_3172);
																BgL_new1149z00_3171 = BgL_new1148z00_3172;
															}
															((((BgL_nodez00_bglt) COBJECT(
																			((BgL_nodez00_bglt)
																				BgL_new1149z00_3171)))->BgL_locz00) =
																((obj_t) BgL_locz00_3164), BUNSPEC);
															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																				BgL_new1149z00_3171)))->BgL_typez00) =
																((BgL_typez00_bglt) ((BgL_typez00_bglt)
																		BGl_za2unspecza2z00zztype_cachez00)),
																BUNSPEC);
															((((BgL_nodezf2effectzf2_bglt)
																		COBJECT(((BgL_nodezf2effectzf2_bglt)
																				BgL_new1149z00_3171)))->
																	BgL_sidezd2effectzd2) =
																((obj_t) BUNSPEC), BUNSPEC);
															((((BgL_nodezf2effectzf2_bglt)
																		COBJECT(((BgL_nodezf2effectzf2_bglt)
																				BgL_new1149z00_3171)))->BgL_keyz00) =
																((obj_t) BINT(-1L)), BUNSPEC);
															{
																obj_t BgL_auxz00_4235;

																{	/* Integrate/node.scm 307 */
																	obj_t BgL_arg1718z00_3174;

																	{	/* Integrate/node.scm 307 */
																		BgL_nodez00_bglt BgL_arg1720z00_3175;

																		BgL_arg1720z00_3175 =
																			(((BgL_setqz00_bglt) COBJECT(
																					((BgL_setqz00_bglt)
																						BgL_nodez00_3001)))->BgL_valuez00);
																		BgL_arg1718z00_3174 =
																			MAKE_YOUNG_PAIR(((obj_t)
																				BgL_azd2varzd2_3163),
																			((obj_t) BgL_arg1720z00_3175));
																	}
																	{	/* Integrate/node.scm 307 */
																		obj_t BgL_list1719z00_3176;

																		BgL_list1719z00_3176 =
																			MAKE_YOUNG_PAIR(BgL_arg1718z00_3174,
																			BNIL);
																		BgL_auxz00_4235 = BgL_list1719z00_3176;
																}}
																((((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_new1149z00_3171))->
																		BgL_bindingsz00) =
																	((obj_t) BgL_auxz00_4235), BUNSPEC);
															}
															{
																BgL_nodez00_bglt BgL_auxz00_4243;

																{	/* Integrate/node.scm 308 */
																	BgL_boxzd2setz12zc0_bglt BgL_new1151z00_3177;

																	{	/* Integrate/node.scm 309 */
																		BgL_boxzd2setz12zc0_bglt
																			BgL_new1150z00_3178;
																		BgL_new1150z00_3178 =
																			((BgL_boxzd2setz12zc0_bglt)
																			BOBJECT(GC_MALLOC(sizeof(struct
																						BgL_boxzd2setz12zc0_bgl))));
																		{	/* Integrate/node.scm 309 */
																			long BgL_arg1724z00_3179;

																			{	/* Integrate/node.scm 309 */
																				obj_t BgL_classz00_3180;

																				BgL_classz00_3180 =
																					BGl_boxzd2setz12zc0zzast_nodez00;
																				BgL_arg1724z00_3179 =
																					BGL_CLASS_NUM(BgL_classz00_3180);
																			}
																			BGL_OBJECT_CLASS_NUM_SET(
																				((BgL_objectz00_bglt)
																					BgL_new1150z00_3178),
																				BgL_arg1724z00_3179);
																		}
																		{	/* Integrate/node.scm 309 */
																			BgL_objectz00_bglt BgL_tmpz00_4248;

																			BgL_tmpz00_4248 =
																				((BgL_objectz00_bglt)
																				BgL_new1150z00_3178);
																			BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4248,
																				BFALSE);
																		}
																		((BgL_objectz00_bglt) BgL_new1150z00_3178);
																		BgL_new1151z00_3177 = BgL_new1150z00_3178;
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						BgL_new1151z00_3177)))->
																			BgL_locz00) =
																		((obj_t) BgL_locz00_3164), BUNSPEC);
																	((((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt)
																						BgL_new1151z00_3177)))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) ((BgL_typez00_bglt)
																				BGl_za2unspecza2z00zztype_cachez00)),
																		BUNSPEC);
																	((((BgL_boxzd2setz12zc0_bglt)
																				COBJECT(BgL_new1151z00_3177))->
																			BgL_varz00) =
																		((BgL_varz00_bglt) (((BgL_setqz00_bglt)
																					COBJECT(((BgL_setqz00_bglt)
																							BgL_nodez00_3001)))->BgL_varz00)),
																		BUNSPEC);
																	{
																		BgL_nodez00_bglt BgL_auxz00_4260;

																		{	/* Integrate/node.scm 313 */
																			BgL_refz00_bglt BgL_new1153z00_3181;

																			{	/* Integrate/node.scm 314 */
																				BgL_refz00_bglt BgL_new1152z00_3182;

																				BgL_new1152z00_3182 =
																					((BgL_refz00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_refz00_bgl))));
																				{	/* Integrate/node.scm 314 */
																					long BgL_arg1722z00_3183;

																					{	/* Integrate/node.scm 314 */
																						obj_t BgL_classz00_3184;

																						BgL_classz00_3184 =
																							BGl_refz00zzast_nodez00;
																						BgL_arg1722z00_3183 =
																							BGL_CLASS_NUM(BgL_classz00_3184);
																					}
																					BGL_OBJECT_CLASS_NUM_SET(
																						((BgL_objectz00_bglt)
																							BgL_new1152z00_3182),
																						BgL_arg1722z00_3183);
																				}
																				{	/* Integrate/node.scm 314 */
																					BgL_objectz00_bglt BgL_tmpz00_4265;

																					BgL_tmpz00_4265 =
																						((BgL_objectz00_bglt)
																						BgL_new1152z00_3182);
																					BGL_OBJECT_WIDENING_SET
																						(BgL_tmpz00_4265, BFALSE);
																				}
																				((BgL_objectz00_bglt)
																					BgL_new1152z00_3182);
																				BgL_new1153z00_3181 =
																					BgL_new1152z00_3182;
																			}
																			((((BgL_nodez00_bglt) COBJECT(
																							((BgL_nodez00_bglt)
																								BgL_new1153z00_3181)))->
																					BgL_locz00) =
																				((obj_t) BgL_locz00_3164), BUNSPEC);
																			((((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_new1153z00_3181)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) ((
																							(BgL_variablez00_bglt)
																							COBJECT(((BgL_variablez00_bglt)
																									BgL_azd2varzd2_3163)))->
																						BgL_typez00)), BUNSPEC);
																			((((BgL_varz00_bglt)
																						COBJECT(((BgL_varz00_bglt)
																								BgL_new1153z00_3181)))->
																					BgL_variablez00) =
																				((BgL_variablez00_bglt) (
																						(BgL_variablez00_bglt)
																						BgL_azd2varzd2_3163)), BUNSPEC);
																			BgL_auxz00_4260 =
																				((BgL_nodez00_bglt)
																				BgL_new1153z00_3181);
																		}
																		((((BgL_boxzd2setz12zc0_bglt)
																					COBJECT(BgL_new1151z00_3177))->
																				BgL_valuez00) =
																			((BgL_nodez00_bglt) BgL_auxz00_4260),
																			BUNSPEC);
																	}
																	((((BgL_boxzd2setz12zc0_bglt)
																				COBJECT(BgL_new1151z00_3177))->
																			BgL_vtypez00) =
																		((BgL_typez00_bglt)
																			BGl_getzd2bigloozd2typez00zztype_cachez00
																			(BgL_vtypez00_3125)), BUNSPEC);
																	BgL_auxz00_4243 =
																		((BgL_nodez00_bglt) BgL_new1151z00_3177);
																}
																((((BgL_letzd2varzd2_bglt)
																			COBJECT(BgL_new1149z00_3171))->
																		BgL_bodyz00) =
																	((BgL_nodez00_bglt) BgL_auxz00_4243),
																	BUNSPEC);
															}
															((((BgL_letzd2varzd2_bglt)
																		COBJECT(BgL_new1149z00_3171))->
																	BgL_removablezf3zf3) =
																((bool_t) ((bool_t) 1)), BUNSPEC);
															return ((BgL_nodez00_bglt) BgL_new1149z00_3171);
														}
													}
												else
													{	/* Integrate/node.scm 297 */
														return
															((BgL_nodez00_bglt)
															((BgL_setqz00_bglt) BgL_nodez00_3001));
													}
											}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &glo!-cast1340 */
	BgL_nodez00_bglt BGl_z62gloz12zd2cast1340za2zzintegrate_nodez00(obj_t
		BgL_envz00_3003, obj_t BgL_nodez00_3004, obj_t BgL_integratorz00_3005)
	{
		{	/* Integrate/node.scm 277 */
			{	/* Integrate/node.scm 279 */
				BgL_nodez00_bglt BgL_arg1703z00_3186;

				BgL_arg1703z00_3186 =
					(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_3004)))->BgL_argz00);
				BGl_gloz12z12zzintegrate_nodez00(BgL_arg1703z00_3186,
					((BgL_variablez00_bglt) BgL_integratorz00_3005));
			}
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_3004));
		}

	}



/* &glo!-extern1337 */
	BgL_nodez00_bglt BGl_z62gloz12zd2extern1337za2zzintegrate_nodez00(obj_t
		BgL_envz00_3006, obj_t BgL_nodez00_3007, obj_t BgL_integratorz00_3008)
	{
		{	/* Integrate/node.scm 269 */
			BGl_gloza2z12zb0zzintegrate_nodez00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_3007)))->BgL_exprza2za2),
				BgL_integratorz00_3008);
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_3007));
		}

	}



/* &glo!-funcall1335 */
	BgL_nodez00_bglt BGl_z62gloz12zd2funcall1335za2zzintegrate_nodez00(obj_t
		BgL_envz00_3009, obj_t BgL_nodez00_3010, obj_t BgL_integratorz00_3011)
	{
		{	/* Integrate/node.scm 260 */
			{
				BgL_nodez00_bglt BgL_auxz00_4300;

				{	/* Integrate/node.scm 262 */
					BgL_nodez00_bglt BgL_arg1700z00_3189;

					BgL_arg1700z00_3189 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_3010)))->BgL_funz00);
					BgL_auxz00_4300 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1700z00_3189,
						((BgL_variablez00_bglt) BgL_integratorz00_3011));
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_3010)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4300), BUNSPEC);
			}
			BGl_gloza2z12zb0zzintegrate_nodez00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_3010)))->BgL_argsz00),
				BgL_integratorz00_3011);
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_3010));
		}

	}



/* &glo!-app-ly1333 */
	BgL_nodez00_bglt BGl_z62gloz12zd2appzd2ly1333z70zzintegrate_nodez00(obj_t
		BgL_envz00_3012, obj_t BgL_nodez00_3013, obj_t BgL_integratorz00_3014)
	{
		{	/* Integrate/node.scm 251 */
			{
				BgL_nodez00_bglt BgL_auxz00_4312;

				{	/* Integrate/node.scm 253 */
					BgL_nodez00_bglt BgL_arg1692z00_3191;

					BgL_arg1692z00_3191 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3013)))->BgL_funz00);
					BgL_auxz00_4312 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1692z00_3191,
						((BgL_variablez00_bglt) BgL_integratorz00_3014));
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3013)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4312), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4319;

				{	/* Integrate/node.scm 254 */
					BgL_nodez00_bglt BgL_arg1699z00_3192;

					BgL_arg1699z00_3192 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3013)))->BgL_argz00);
					BgL_auxz00_4319 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1699z00_3192,
						((BgL_variablez00_bglt) BgL_integratorz00_3014));
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt) BgL_nodez00_3013)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4319), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_3013));
		}

	}



/* &glo!-app1331 */
	BgL_nodez00_bglt BGl_z62gloz12zd2app1331za2zzintegrate_nodez00(obj_t
		BgL_envz00_3015, obj_t BgL_nodez00_3016, obj_t BgL_integratorz00_3017)
	{
		{	/* Integrate/node.scm 211 */
			{	/* Integrate/node.scm 213 */
				BgL_variablez00_bglt BgL_funz00_3194;

				BgL_funz00_3194 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_3016)))->BgL_funz00)))->
					BgL_variablez00);
				{	/* Integrate/node.scm 213 */
					BgL_valuez00_bglt BgL_infoz00_3195;

					BgL_infoz00_3195 =
						(((BgL_variablez00_bglt) COBJECT(BgL_funz00_3194))->BgL_valuez00);
					{	/* Integrate/node.scm 214 */

						{	/* Integrate/node.scm 216 */
							bool_t BgL_test2067z00_4332;

							{	/* Integrate/node.scm 216 */
								bool_t BgL_test2068z00_4333;

								{	/* Integrate/node.scm 216 */
									obj_t BgL_classz00_3196;

									BgL_classz00_3196 = BGl_localz00zzast_varz00;
									{	/* Integrate/node.scm 216 */
										BgL_objectz00_bglt BgL_arg1807z00_3197;

										{	/* Integrate/node.scm 216 */
											obj_t BgL_tmpz00_4334;

											BgL_tmpz00_4334 =
												((obj_t) ((BgL_objectz00_bglt) BgL_funz00_3194));
											BgL_arg1807z00_3197 =
												(BgL_objectz00_bglt) (BgL_tmpz00_4334);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Integrate/node.scm 216 */
												long BgL_idxz00_3198;

												BgL_idxz00_3198 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3197);
												BgL_test2068z00_4333 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3198 + 2L)) == BgL_classz00_3196);
											}
										else
											{	/* Integrate/node.scm 216 */
												bool_t BgL_res1938z00_3201;

												{	/* Integrate/node.scm 216 */
													obj_t BgL_oclassz00_3202;

													{	/* Integrate/node.scm 216 */
														obj_t BgL_arg1815z00_3203;
														long BgL_arg1816z00_3204;

														BgL_arg1815z00_3203 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Integrate/node.scm 216 */
															long BgL_arg1817z00_3205;

															BgL_arg1817z00_3205 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3197);
															BgL_arg1816z00_3204 =
																(BgL_arg1817z00_3205 - OBJECT_TYPE);
														}
														BgL_oclassz00_3202 =
															VECTOR_REF(BgL_arg1815z00_3203,
															BgL_arg1816z00_3204);
													}
													{	/* Integrate/node.scm 216 */
														bool_t BgL__ortest_1115z00_3206;

														BgL__ortest_1115z00_3206 =
															(BgL_classz00_3196 == BgL_oclassz00_3202);
														if (BgL__ortest_1115z00_3206)
															{	/* Integrate/node.scm 216 */
																BgL_res1938z00_3201 = BgL__ortest_1115z00_3206;
															}
														else
															{	/* Integrate/node.scm 216 */
																long BgL_odepthz00_3207;

																{	/* Integrate/node.scm 216 */
																	obj_t BgL_arg1804z00_3208;

																	BgL_arg1804z00_3208 = (BgL_oclassz00_3202);
																	BgL_odepthz00_3207 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3208);
																}
																if ((2L < BgL_odepthz00_3207))
																	{	/* Integrate/node.scm 216 */
																		obj_t BgL_arg1802z00_3209;

																		{	/* Integrate/node.scm 216 */
																			obj_t BgL_arg1803z00_3210;

																			BgL_arg1803z00_3210 =
																				(BgL_oclassz00_3202);
																			BgL_arg1802z00_3209 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3210, 2L);
																		}
																		BgL_res1938z00_3201 =
																			(BgL_arg1802z00_3209 ==
																			BgL_classz00_3196);
																	}
																else
																	{	/* Integrate/node.scm 216 */
																		BgL_res1938z00_3201 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2068z00_4333 = BgL_res1938z00_3201;
											}
									}
								}
								if (BgL_test2068z00_4333)
									{
										BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4357;

										{
											obj_t BgL_auxz00_4358;

											{	/* Integrate/node.scm 216 */
												BgL_objectz00_bglt BgL_tmpz00_4359;

												BgL_tmpz00_4359 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_infoz00_3195));
												BgL_auxz00_4358 = BGL_OBJECT_WIDENING(BgL_tmpz00_4359);
											}
											BgL_auxz00_4357 =
												((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4358);
										}
										BgL_test2067z00_4332 =
											(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4357))->
											BgL_gzf3zf3);
									}
								else
									{	/* Integrate/node.scm 216 */
										BgL_test2067z00_4332 = ((bool_t) 0);
									}
							}
							if (BgL_test2067z00_4332)
								{	/* Integrate/node.scm 218 */
									BgL_refz00_bglt BgL_arg1642z00_3211;

									{	/* Integrate/node.scm 218 */
										BgL_refz00_bglt BgL_new1134z00_3212;

										{	/* Integrate/node.scm 219 */
											BgL_refz00_bglt BgL_new1133z00_3213;

											BgL_new1133z00_3213 =
												((BgL_refz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_refz00_bgl))));
											{	/* Integrate/node.scm 219 */
												long BgL_arg1650z00_3214;

												BgL_arg1650z00_3214 =
													BGL_CLASS_NUM(BGl_refz00zzast_nodez00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1133z00_3213),
													BgL_arg1650z00_3214);
											}
											{	/* Integrate/node.scm 219 */
												BgL_objectz00_bglt BgL_tmpz00_4369;

												BgL_tmpz00_4369 =
													((BgL_objectz00_bglt) BgL_new1133z00_3213);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4369, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1133z00_3213);
											BgL_new1134z00_3212 = BgL_new1133z00_3213;
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1134z00_3212)))->
												BgL_locz00) =
											((obj_t) (((BgL_nodez00_bglt)
														COBJECT(((BgL_nodez00_bglt) ((BgL_appz00_bglt)
																	BgL_nodez00_3016))))->BgL_locz00)), BUNSPEC);
										{
											BgL_typez00_bglt BgL_auxz00_4378;

											{	/* Integrate/node.scm 220 */
												obj_t BgL_arg1646z00_3215;

												BgL_arg1646z00_3215 =
													BGl_thezd2globalzd2zzintegrate_localzd2ze3globalz31(
													((BgL_localz00_bglt) BgL_funz00_3194));
												BgL_auxz00_4378 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_arg1646z00_3215)))->
													BgL_typez00);
											}
											((((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_new1134z00_3212)))->
													BgL_typez00) =
												((BgL_typez00_bglt) BgL_auxz00_4378), BUNSPEC);
										}
										((((BgL_varz00_bglt) COBJECT(
														((BgL_varz00_bglt) BgL_new1134z00_3212)))->
												BgL_variablez00) =
											((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
													BGl_thezd2globalzd2zzintegrate_localzd2ze3globalz31((
															(BgL_localz00_bglt) BgL_funz00_3194)))), BUNSPEC);
										BgL_arg1642z00_3211 = BgL_new1134z00_3212;
									}
									((((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt) BgL_nodez00_3016)))->BgL_funz00) =
										((BgL_varz00_bglt) ((BgL_varz00_bglt) BgL_arg1642z00_3211)),
										BUNSPEC);
								}
							else
								{	/* Integrate/node.scm 216 */
									BFALSE;
								}
						}
						{
							obj_t BgL_nodesz00_3217;

							BgL_nodesz00_3217 =
								(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt) BgL_nodez00_3016)))->BgL_argsz00);
						BgL_liipz00_3216:
							if (NULLP(BgL_nodesz00_3217))
								{	/* Integrate/node.scm 225 */
									CNST_TABLE_REF(2);
								}
							else
								{	/* Integrate/node.scm 225 */
									{	/* Integrate/node.scm 228 */
										BgL_nodez00_bglt BgL_arg1654z00_3218;

										{	/* Integrate/node.scm 228 */
											obj_t BgL_arg1661z00_3219;

											BgL_arg1661z00_3219 = CAR(((obj_t) BgL_nodesz00_3217));
											BgL_arg1654z00_3218 =
												BGl_gloz12z12zzintegrate_nodez00(
												((BgL_nodez00_bglt) BgL_arg1661z00_3219),
												((BgL_variablez00_bglt) BgL_integratorz00_3017));
										}
										{	/* Integrate/node.scm 228 */
											obj_t BgL_auxz00_4403;
											obj_t BgL_tmpz00_4401;

											BgL_auxz00_4403 = ((obj_t) BgL_arg1654z00_3218);
											BgL_tmpz00_4401 = ((obj_t) BgL_nodesz00_3217);
											SET_CAR(BgL_tmpz00_4401, BgL_auxz00_4403);
										}
									}
									{	/* Integrate/node.scm 229 */
										obj_t BgL_arg1663z00_3220;

										BgL_arg1663z00_3220 = CDR(((obj_t) BgL_nodesz00_3217));
										{
											obj_t BgL_nodesz00_4408;

											BgL_nodesz00_4408 = BgL_arg1663z00_3220;
											BgL_nodesz00_3217 = BgL_nodesz00_4408;
											goto BgL_liipz00_3216;
										}
									}
								}
						}
						{	/* Integrate/node.scm 230 */
							bool_t BgL_test2073z00_4411;

							{	/* Integrate/node.scm 230 */
								bool_t BgL_test2074z00_4412;

								{	/* Integrate/node.scm 230 */
									obj_t BgL_classz00_3221;

									BgL_classz00_3221 = BGl_globalz00zzast_varz00;
									{	/* Integrate/node.scm 230 */
										BgL_objectz00_bglt BgL_arg1807z00_3222;

										{	/* Integrate/node.scm 230 */
											obj_t BgL_tmpz00_4413;

											BgL_tmpz00_4413 =
												((obj_t) ((BgL_objectz00_bglt) BgL_funz00_3194));
											BgL_arg1807z00_3222 =
												(BgL_objectz00_bglt) (BgL_tmpz00_4413);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Integrate/node.scm 230 */
												long BgL_idxz00_3223;

												BgL_idxz00_3223 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3222);
												BgL_test2074z00_4412 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3223 + 2L)) == BgL_classz00_3221);
											}
										else
											{	/* Integrate/node.scm 230 */
												bool_t BgL_res1939z00_3226;

												{	/* Integrate/node.scm 230 */
													obj_t BgL_oclassz00_3227;

													{	/* Integrate/node.scm 230 */
														obj_t BgL_arg1815z00_3228;
														long BgL_arg1816z00_3229;

														BgL_arg1815z00_3228 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Integrate/node.scm 230 */
															long BgL_arg1817z00_3230;

															BgL_arg1817z00_3230 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3222);
															BgL_arg1816z00_3229 =
																(BgL_arg1817z00_3230 - OBJECT_TYPE);
														}
														BgL_oclassz00_3227 =
															VECTOR_REF(BgL_arg1815z00_3228,
															BgL_arg1816z00_3229);
													}
													{	/* Integrate/node.scm 230 */
														bool_t BgL__ortest_1115z00_3231;

														BgL__ortest_1115z00_3231 =
															(BgL_classz00_3221 == BgL_oclassz00_3227);
														if (BgL__ortest_1115z00_3231)
															{	/* Integrate/node.scm 230 */
																BgL_res1939z00_3226 = BgL__ortest_1115z00_3231;
															}
														else
															{	/* Integrate/node.scm 230 */
																long BgL_odepthz00_3232;

																{	/* Integrate/node.scm 230 */
																	obj_t BgL_arg1804z00_3233;

																	BgL_arg1804z00_3233 = (BgL_oclassz00_3227);
																	BgL_odepthz00_3232 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3233);
																}
																if ((2L < BgL_odepthz00_3232))
																	{	/* Integrate/node.scm 230 */
																		obj_t BgL_arg1802z00_3234;

																		{	/* Integrate/node.scm 230 */
																			obj_t BgL_arg1803z00_3235;

																			BgL_arg1803z00_3235 =
																				(BgL_oclassz00_3227);
																			BgL_arg1802z00_3234 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3235, 2L);
																		}
																		BgL_res1939z00_3226 =
																			(BgL_arg1802z00_3234 ==
																			BgL_classz00_3221);
																	}
																else
																	{	/* Integrate/node.scm 230 */
																		BgL_res1939z00_3226 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2074z00_4412 = BgL_res1939z00_3226;
											}
									}
								}
								if (BgL_test2074z00_4412)
									{	/* Integrate/node.scm 230 */
										BgL_test2073z00_4411 = ((bool_t) 1);
									}
								else
									{	/* Integrate/node.scm 230 */
										bool_t BgL_test2078z00_4436;

										{
											BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4437;

											{
												obj_t BgL_auxz00_4438;

												{	/* Integrate/node.scm 230 */
													BgL_objectz00_bglt BgL_tmpz00_4439;

													BgL_tmpz00_4439 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_infoz00_3195));
													BgL_auxz00_4438 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4439);
												}
												BgL_auxz00_4437 =
													((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4438);
											}
											BgL_test2078z00_4436 =
												(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4437))->
												BgL_gzf3zf3);
										}
										if (BgL_test2078z00_4436)
											{	/* Integrate/node.scm 230 */
												BgL_test2073z00_4411 = ((bool_t) 0);
											}
										else
											{	/* Integrate/node.scm 230 */
												BgL_test2073z00_4411 = ((bool_t) 1);
											}
									}
							}
							if (BgL_test2073z00_4411)
								{	/* Integrate/node.scm 230 */
									BFALSE;
								}
							else
								{	/* Integrate/node.scm 233 */
									obj_t BgL_g1135z00_3236;

									{
										BgL_sfunzf2iinfozf2_bglt BgL_auxz00_4445;

										{
											obj_t BgL_auxz00_4446;

											{	/* Integrate/node.scm 234 */
												BgL_objectz00_bglt BgL_tmpz00_4447;

												BgL_tmpz00_4447 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_infoz00_3195));
												BgL_auxz00_4446 = BGL_OBJECT_WIDENING(BgL_tmpz00_4447);
											}
											BgL_auxz00_4445 =
												((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_4446);
										}
										BgL_g1135z00_3236 =
											(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_4445))->
											BgL_kapturedz00);
									}
									{
										obj_t BgL_newzd2actualszd2_3238;
										obj_t BgL_kapturedz00_3239;

										BgL_newzd2actualszd2_3238 =
											(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt) BgL_nodez00_3016)))->BgL_argsz00);
										BgL_kapturedz00_3239 = BgL_g1135z00_3236;
									BgL_loopz00_3237:
										if (NULLP(BgL_kapturedz00_3239))
											{	/* Integrate/node.scm 235 */
												((((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_nodez00_3016)))->
														BgL_argsz00) =
													((obj_t) BgL_newzd2actualszd2_3238), BUNSPEC);
											}
										else
											{	/* Integrate/node.scm 237 */
												obj_t BgL_kapz00_3240;

												BgL_kapz00_3240 = CAR(((obj_t) BgL_kapturedz00_3239));
												{	/* Integrate/node.scm 237 */
													obj_t BgL_alphaz00_3241;

													BgL_alphaz00_3241 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_kapz00_3240))))->
														BgL_fastzd2alphazd2);
													{	/* Integrate/node.scm 238 */
														obj_t BgL_varz00_3242;

														{	/* Integrate/node.scm 239 */
															bool_t BgL_test2080z00_4462;

															{	/* Integrate/node.scm 239 */
																obj_t BgL_classz00_3243;

																BgL_classz00_3243 = BGl_localz00zzast_varz00;
																if (BGL_OBJECTP(BgL_alphaz00_3241))
																	{	/* Integrate/node.scm 239 */
																		BgL_objectz00_bglt BgL_arg1807z00_3244;

																		BgL_arg1807z00_3244 =
																			(BgL_objectz00_bglt) (BgL_alphaz00_3241);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Integrate/node.scm 239 */
																				long BgL_idxz00_3245;

																				BgL_idxz00_3245 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_3244);
																				BgL_test2080z00_4462 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_3245 + 2L)) ==
																					BgL_classz00_3243);
																			}
																		else
																			{	/* Integrate/node.scm 239 */
																				bool_t BgL_res1940z00_3248;

																				{	/* Integrate/node.scm 239 */
																					obj_t BgL_oclassz00_3249;

																					{	/* Integrate/node.scm 239 */
																						obj_t BgL_arg1815z00_3250;
																						long BgL_arg1816z00_3251;

																						BgL_arg1815z00_3250 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Integrate/node.scm 239 */
																							long BgL_arg1817z00_3252;

																							BgL_arg1817z00_3252 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_3244);
																							BgL_arg1816z00_3251 =
																								(BgL_arg1817z00_3252 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_3249 =
																							VECTOR_REF(BgL_arg1815z00_3250,
																							BgL_arg1816z00_3251);
																					}
																					{	/* Integrate/node.scm 239 */
																						bool_t BgL__ortest_1115z00_3253;

																						BgL__ortest_1115z00_3253 =
																							(BgL_classz00_3243 ==
																							BgL_oclassz00_3249);
																						if (BgL__ortest_1115z00_3253)
																							{	/* Integrate/node.scm 239 */
																								BgL_res1940z00_3248 =
																									BgL__ortest_1115z00_3253;
																							}
																						else
																							{	/* Integrate/node.scm 239 */
																								long BgL_odepthz00_3254;

																								{	/* Integrate/node.scm 239 */
																									obj_t BgL_arg1804z00_3255;

																									BgL_arg1804z00_3255 =
																										(BgL_oclassz00_3249);
																									BgL_odepthz00_3254 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_3255);
																								}
																								if ((2L < BgL_odepthz00_3254))
																									{	/* Integrate/node.scm 239 */
																										obj_t BgL_arg1802z00_3256;

																										{	/* Integrate/node.scm 239 */
																											obj_t BgL_arg1803z00_3257;

																											BgL_arg1803z00_3257 =
																												(BgL_oclassz00_3249);
																											BgL_arg1802z00_3256 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_3257,
																												2L);
																										}
																										BgL_res1940z00_3248 =
																											(BgL_arg1802z00_3256 ==
																											BgL_classz00_3243);
																									}
																								else
																									{	/* Integrate/node.scm 239 */
																										BgL_res1940z00_3248 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2080z00_4462 =
																					BgL_res1940z00_3248;
																			}
																	}
																else
																	{	/* Integrate/node.scm 239 */
																		BgL_test2080z00_4462 = ((bool_t) 0);
																	}
															}
															if (BgL_test2080z00_4462)
																{	/* Integrate/node.scm 239 */
																	BgL_varz00_3242 = BgL_alphaz00_3241;
																}
															else
																{	/* Integrate/node.scm 239 */
																	BgL_varz00_3242 = BgL_kapz00_3240;
																}
														}
														{	/* Integrate/node.scm 239 */

															{	/* Integrate/node.scm 240 */
																obj_t BgL_arg1678z00_3258;
																obj_t BgL_arg1681z00_3259;

																{	/* Integrate/node.scm 240 */
																	BgL_refz00_bglt BgL_arg1688z00_3260;

																	{	/* Integrate/node.scm 240 */
																		BgL_refz00_bglt BgL_new1137z00_3261;

																		{	/* Integrate/node.scm 243 */
																			BgL_refz00_bglt BgL_new1136z00_3262;

																			BgL_new1136z00_3262 =
																				((BgL_refz00_bglt)
																				BOBJECT(GC_MALLOC(sizeof(struct
																							BgL_refz00_bgl))));
																			{	/* Integrate/node.scm 243 */
																				long BgL_arg1689z00_3263;

																				BgL_arg1689z00_3263 =
																					BGL_CLASS_NUM
																					(BGl_refz00zzast_nodez00);
																				BGL_OBJECT_CLASS_NUM_SET((
																						(BgL_objectz00_bglt)
																						BgL_new1136z00_3262),
																					BgL_arg1689z00_3263);
																			}
																			{	/* Integrate/node.scm 243 */
																				BgL_objectz00_bglt BgL_tmpz00_4489;

																				BgL_tmpz00_4489 =
																					((BgL_objectz00_bglt)
																					BgL_new1136z00_3262);
																				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4489,
																					BFALSE);
																			}
																			((BgL_objectz00_bglt)
																				BgL_new1136z00_3262);
																			BgL_new1137z00_3261 = BgL_new1136z00_3262;
																		}
																		((((BgL_nodez00_bglt) COBJECT(
																						((BgL_nodez00_bglt)
																							BgL_new1137z00_3261)))->
																				BgL_locz00) =
																			((obj_t) (((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt) (
																									(BgL_appz00_bglt)
																									BgL_nodez00_3016))))->
																					BgL_locz00)), BUNSPEC);
																		((((BgL_nodez00_bglt)
																					COBJECT(((BgL_nodez00_bglt)
																							BgL_new1137z00_3261)))->
																				BgL_typez00) =
																			((BgL_typez00_bglt) ((
																						(BgL_variablez00_bglt)
																						COBJECT(((BgL_variablez00_bglt)
																								BgL_varz00_3242)))->
																					BgL_typez00)), BUNSPEC);
																		((((BgL_varz00_bglt)
																					COBJECT(((BgL_varz00_bglt)
																							BgL_new1137z00_3261)))->
																				BgL_variablez00) =
																			((BgL_variablez00_bglt) (
																					(BgL_variablez00_bglt)
																					BgL_varz00_3242)), BUNSPEC);
																		BgL_arg1688z00_3260 = BgL_new1137z00_3261;
																	}
																	BgL_arg1678z00_3258 =
																		MAKE_YOUNG_PAIR(
																		((obj_t) BgL_arg1688z00_3260),
																		BgL_newzd2actualszd2_3238);
																}
																BgL_arg1681z00_3259 =
																	CDR(((obj_t) BgL_kapturedz00_3239));
																{
																	obj_t BgL_kapturedz00_4510;
																	obj_t BgL_newzd2actualszd2_4509;

																	BgL_newzd2actualszd2_4509 =
																		BgL_arg1678z00_3258;
																	BgL_kapturedz00_4510 = BgL_arg1681z00_3259;
																	BgL_kapturedz00_3239 = BgL_kapturedz00_4510;
																	BgL_newzd2actualszd2_3238 =
																		BgL_newzd2actualszd2_4509;
																	goto BgL_loopz00_3237;
																}
															}
														}
													}
												}
											}
									}
								}
						}
						return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3016));
					}
				}
			}
		}

	}



/* &glo!-sync1329 */
	BgL_nodez00_bglt BGl_z62gloz12zd2sync1329za2zzintegrate_nodez00(obj_t
		BgL_envz00_3018, obj_t BgL_nodez00_3019, obj_t BgL_integratorz00_3020)
	{
		{	/* Integrate/node.scm 201 */
			{
				BgL_nodez00_bglt BgL_auxz00_4515;

				{	/* Integrate/node.scm 203 */
					BgL_nodez00_bglt BgL_arg1627z00_3265;

					BgL_arg1627z00_3265 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3019)))->BgL_mutexz00);
					BgL_auxz00_4515 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1627z00_3265,
						((BgL_variablez00_bglt) BgL_integratorz00_3020));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3019)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4515), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4522;

				{	/* Integrate/node.scm 204 */
					BgL_nodez00_bglt BgL_arg1629z00_3266;

					BgL_arg1629z00_3266 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3019)))->BgL_prelockz00);
					BgL_auxz00_4522 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1629z00_3266,
						((BgL_variablez00_bglt) BgL_integratorz00_3020));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3019)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4522), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_4529;

				{	/* Integrate/node.scm 205 */
					BgL_nodez00_bglt BgL_arg1630z00_3267;

					BgL_arg1630z00_3267 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3019)))->BgL_bodyz00);
					BgL_auxz00_4529 =
						BGl_gloz12z12zzintegrate_nodez00(BgL_arg1630z00_3267,
						((BgL_variablez00_bglt) BgL_integratorz00_3020));
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_3019)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_4529), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_3019));
		}

	}



/* &glo!-sequence1327 */
	BgL_nodez00_bglt BGl_z62gloz12zd2sequence1327za2zzintegrate_nodez00(obj_t
		BgL_envz00_3021, obj_t BgL_nodez00_3022, obj_t BgL_integratorz00_3023)
	{
		{	/* Integrate/node.scm 193 */
			BGl_gloza2z12zb0zzintegrate_nodez00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_3022)))->BgL_nodesz00),
				BgL_integratorz00_3023);
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_3022));
		}

	}



/* &glo!-closure1325 */
	BgL_nodez00_bglt BGl_z62gloz12zd2closure1325za2zzintegrate_nodez00(obj_t
		BgL_envz00_3024, obj_t BgL_nodez00_3025, obj_t BgL_integratorz00_3026)
	{
		{	/* Integrate/node.scm 187 */
			{	/* Integrate/node.scm 188 */
				obj_t BgL_arg1625z00_3270;

				BgL_arg1625z00_3270 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_3025)));
				return
					((BgL_nodez00_bglt)
					BGl_internalzd2errorzd2zztools_errorz00
					(BGl_string1981z00zzintegrate_nodez00,
						BGl_string1982z00zzintegrate_nodez00, BgL_arg1625z00_3270));
			}
		}

	}



/* &glo!-var1323 */
	BgL_nodez00_bglt BGl_z62gloz12zd2var1323za2zzintegrate_nodez00(obj_t
		BgL_envz00_3027, obj_t BgL_nodez00_3028, obj_t BgL_integratorz00_3029)
	{
		{	/* Integrate/node.scm 158 */
			{	/* Integrate/node.scm 159 */
				BgL_variablez00_bglt BgL_variablez00_3272;

				BgL_variablez00_3272 =
					(((BgL_varz00_bglt) COBJECT(
							((BgL_varz00_bglt) BgL_nodez00_3028)))->BgL_variablez00);
				{	/* Integrate/node.scm 159 */
					BgL_typez00_bglt BgL_vtypez00_3273;

					BgL_vtypez00_3273 =
						(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_3272))->
						BgL_typez00);
					{	/* Integrate/node.scm 160 */

						{
							obj_t BgL_variablez00_3275;

							BgL_variablez00_3275 = ((obj_t) BgL_variablez00_3272);
						BgL_loopz00_3274:
							{	/* Integrate/node.scm 162 */
								obj_t BgL_alphaz00_3276;

								BgL_alphaz00_3276 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_variablez00_3275)))->
									BgL_fastzd2alphazd2);
								{	/* Integrate/node.scm 164 */
									bool_t BgL_test2085z00_4553;

									{	/* Integrate/node.scm 164 */
										obj_t BgL_classz00_3277;

										BgL_classz00_3277 = BGl_localz00zzast_varz00;
										if (BGL_OBJECTP(BgL_alphaz00_3276))
											{	/* Integrate/node.scm 164 */
												BgL_objectz00_bglt BgL_arg1807z00_3278;

												BgL_arg1807z00_3278 =
													(BgL_objectz00_bglt) (BgL_alphaz00_3276);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Integrate/node.scm 164 */
														long BgL_idxz00_3279;

														BgL_idxz00_3279 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3278);
														BgL_test2085z00_4553 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3279 + 2L)) == BgL_classz00_3277);
													}
												else
													{	/* Integrate/node.scm 164 */
														bool_t BgL_res1936z00_3282;

														{	/* Integrate/node.scm 164 */
															obj_t BgL_oclassz00_3283;

															{	/* Integrate/node.scm 164 */
																obj_t BgL_arg1815z00_3284;
																long BgL_arg1816z00_3285;

																BgL_arg1815z00_3284 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Integrate/node.scm 164 */
																	long BgL_arg1817z00_3286;

																	BgL_arg1817z00_3286 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3278);
																	BgL_arg1816z00_3285 =
																		(BgL_arg1817z00_3286 - OBJECT_TYPE);
																}
																BgL_oclassz00_3283 =
																	VECTOR_REF(BgL_arg1815z00_3284,
																	BgL_arg1816z00_3285);
															}
															{	/* Integrate/node.scm 164 */
																bool_t BgL__ortest_1115z00_3287;

																BgL__ortest_1115z00_3287 =
																	(BgL_classz00_3277 == BgL_oclassz00_3283);
																if (BgL__ortest_1115z00_3287)
																	{	/* Integrate/node.scm 164 */
																		BgL_res1936z00_3282 =
																			BgL__ortest_1115z00_3287;
																	}
																else
																	{	/* Integrate/node.scm 164 */
																		long BgL_odepthz00_3288;

																		{	/* Integrate/node.scm 164 */
																			obj_t BgL_arg1804z00_3289;

																			BgL_arg1804z00_3289 =
																				(BgL_oclassz00_3283);
																			BgL_odepthz00_3288 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3289);
																		}
																		if ((2L < BgL_odepthz00_3288))
																			{	/* Integrate/node.scm 164 */
																				obj_t BgL_arg1802z00_3290;

																				{	/* Integrate/node.scm 164 */
																					obj_t BgL_arg1803z00_3291;

																					BgL_arg1803z00_3291 =
																						(BgL_oclassz00_3283);
																					BgL_arg1802z00_3290 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3291, 2L);
																				}
																				BgL_res1936z00_3282 =
																					(BgL_arg1802z00_3290 ==
																					BgL_classz00_3277);
																			}
																		else
																			{	/* Integrate/node.scm 164 */
																				BgL_res1936z00_3282 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2085z00_4553 = BgL_res1936z00_3282;
													}
											}
										else
											{	/* Integrate/node.scm 164 */
												BgL_test2085z00_4553 = ((bool_t) 0);
											}
									}
									if (BgL_test2085z00_4553)
										{	/* Integrate/node.scm 164 */
											((((BgL_varz00_bglt) COBJECT(
															((BgL_varz00_bglt) BgL_nodez00_3028)))->
													BgL_variablez00) =
												((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
														BgL_alphaz00_3276)), BUNSPEC);
											{	/* Integrate/node.scm 166 */
												BgL_typez00_bglt BgL_arg1613z00_3292;

												BgL_arg1613z00_3292 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_alphaz00_3276)))->
													BgL_typez00);
												((((BgL_nodez00_bglt)
															COBJECT(((BgL_nodez00_bglt) ((BgL_varz00_bglt)
																		BgL_nodez00_3028))))->BgL_typez00) =
													((BgL_typez00_bglt) BgL_arg1613z00_3292), BUNSPEC);
											}
											{
												obj_t BgL_variablez00_4584;

												BgL_variablez00_4584 = BgL_alphaz00_3276;
												BgL_variablez00_3275 = BgL_variablez00_4584;
												goto BgL_loopz00_3274;
											}
										}
									else
										{	/* Integrate/node.scm 168 */
											bool_t BgL_test2090z00_4585;

											{	/* Integrate/node.scm 168 */
												obj_t BgL_classz00_3293;

												BgL_classz00_3293 = BGl_globalz00zzast_varz00;
												if (BGL_OBJECTP(BgL_variablez00_3275))
													{	/* Integrate/node.scm 168 */
														BgL_objectz00_bglt BgL_arg1807z00_3294;

														BgL_arg1807z00_3294 =
															(BgL_objectz00_bglt) (BgL_variablez00_3275);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Integrate/node.scm 168 */
																long BgL_idxz00_3295;

																BgL_idxz00_3295 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_3294);
																BgL_test2090z00_4585 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_3295 + 2L)) ==
																	BgL_classz00_3293);
															}
														else
															{	/* Integrate/node.scm 168 */
																bool_t BgL_res1937z00_3298;

																{	/* Integrate/node.scm 168 */
																	obj_t BgL_oclassz00_3299;

																	{	/* Integrate/node.scm 168 */
																		obj_t BgL_arg1815z00_3300;
																		long BgL_arg1816z00_3301;

																		BgL_arg1815z00_3300 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Integrate/node.scm 168 */
																			long BgL_arg1817z00_3302;

																			BgL_arg1817z00_3302 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_3294);
																			BgL_arg1816z00_3301 =
																				(BgL_arg1817z00_3302 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_3299 =
																			VECTOR_REF(BgL_arg1815z00_3300,
																			BgL_arg1816z00_3301);
																	}
																	{	/* Integrate/node.scm 168 */
																		bool_t BgL__ortest_1115z00_3303;

																		BgL__ortest_1115z00_3303 =
																			(BgL_classz00_3293 == BgL_oclassz00_3299);
																		if (BgL__ortest_1115z00_3303)
																			{	/* Integrate/node.scm 168 */
																				BgL_res1937z00_3298 =
																					BgL__ortest_1115z00_3303;
																			}
																		else
																			{	/* Integrate/node.scm 168 */
																				long BgL_odepthz00_3304;

																				{	/* Integrate/node.scm 168 */
																					obj_t BgL_arg1804z00_3305;

																					BgL_arg1804z00_3305 =
																						(BgL_oclassz00_3299);
																					BgL_odepthz00_3304 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_3305);
																				}
																				if ((2L < BgL_odepthz00_3304))
																					{	/* Integrate/node.scm 168 */
																						obj_t BgL_arg1802z00_3306;

																						{	/* Integrate/node.scm 168 */
																							obj_t BgL_arg1803z00_3307;

																							BgL_arg1803z00_3307 =
																								(BgL_oclassz00_3299);
																							BgL_arg1802z00_3306 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_3307, 2L);
																						}
																						BgL_res1937z00_3298 =
																							(BgL_arg1802z00_3306 ==
																							BgL_classz00_3293);
																					}
																				else
																					{	/* Integrate/node.scm 168 */
																						BgL_res1937z00_3298 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test2090z00_4585 = BgL_res1937z00_3298;
															}
													}
												else
													{	/* Integrate/node.scm 168 */
														BgL_test2090z00_4585 = ((bool_t) 0);
													}
											}
											if (BgL_test2090z00_4585)
												{	/* Integrate/node.scm 168 */
													return
														((BgL_nodez00_bglt)
														((BgL_varz00_bglt) BgL_nodez00_3028));
												}
											else
												{	/* Integrate/node.scm 168 */
													if (BGl_integratezd2celledzf3z21zzintegrate_nodez00(
															((BgL_localz00_bglt) BgL_variablez00_3275)))
														{	/* Integrate/node.scm 172 */
															BgL_typez00_bglt BgL_vtypez00_3308;

															BgL_vtypez00_3308 =
																BGl_getzd2bigloozd2typez00zztype_cachez00
																(BgL_vtypez00_3273);
															{	/* Integrate/node.scm 174 */
																BgL_typez00_bglt BgL_vz00_3309;

																BgL_vz00_3309 =
																	((BgL_typez00_bglt)
																	BGl_za2cellza2z00zztype_cachez00);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt) (
																						(BgL_varz00_bglt)
																						BgL_nodez00_3028))))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_vz00_3309), BUNSPEC);
															}
															{	/* Integrate/node.scm 175 */
																BgL_boxzd2refzd2_bglt BgL_new1129z00_3310;

																{	/* Integrate/node.scm 176 */
																	BgL_boxzd2refzd2_bglt BgL_new1127z00_3311;

																	BgL_new1127z00_3311 =
																		((BgL_boxzd2refzd2_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_boxzd2refzd2_bgl))));
																	{	/* Integrate/node.scm 176 */
																		long BgL_arg1616z00_3312;

																		BgL_arg1616z00_3312 =
																			BGL_CLASS_NUM
																			(BGl_boxzd2refzd2zzast_nodez00);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt)
																				BgL_new1127z00_3311),
																			BgL_arg1616z00_3312);
																	}
																	{	/* Integrate/node.scm 176 */
																		BgL_objectz00_bglt BgL_tmpz00_4622;

																		BgL_tmpz00_4622 =
																			((BgL_objectz00_bglt)
																			BgL_new1127z00_3311);
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4622,
																			BFALSE);
																	}
																	((BgL_objectz00_bglt) BgL_new1127z00_3311);
																	BgL_new1129z00_3310 = BgL_new1127z00_3311;
																}
																((((BgL_nodez00_bglt) COBJECT(
																				((BgL_nodez00_bglt)
																					BgL_new1129z00_3310)))->BgL_locz00) =
																	((obj_t) (((BgL_nodez00_bglt)
																				COBJECT(((BgL_nodez00_bglt) (
																							(BgL_varz00_bglt)
																							BgL_nodez00_3028))))->
																			BgL_locz00)), BUNSPEC);
																((((BgL_nodez00_bglt)
																			COBJECT(((BgL_nodez00_bglt)
																					BgL_new1129z00_3310)))->BgL_typez00) =
																	((BgL_typez00_bglt) BgL_vtypez00_3308),
																	BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1129z00_3310)))->
																		BgL_sidezd2effectzd2) =
																	((obj_t) BUNSPEC), BUNSPEC);
																((((BgL_nodezf2effectzf2_bglt)
																			COBJECT(((BgL_nodezf2effectzf2_bglt)
																					BgL_new1129z00_3310)))->BgL_keyz00) =
																	((obj_t) BINT(-1L)), BUNSPEC);
																((((BgL_boxzd2refzd2_bglt)
																			COBJECT(BgL_new1129z00_3310))->
																		BgL_varz00) =
																	((BgL_varz00_bglt) ((BgL_varz00_bglt)
																			BgL_nodez00_3028)), BUNSPEC);
																((((BgL_boxzd2refzd2_bglt)
																			COBJECT(BgL_new1129z00_3310))->
																		BgL_vtypez00) =
																	((BgL_typez00_bglt) BgL_vtypez00_3308),
																	BUNSPEC);
																return ((BgL_nodez00_bglt) BgL_new1129z00_3310);
															}
														}
													else
														{	/* Integrate/node.scm 170 */
															return
																((BgL_nodez00_bglt)
																((BgL_varz00_bglt) BgL_nodez00_3028));
														}
												}
										}
								}
							}
						}
					}
				}
			}
		}

	}



/* &glo!-kwote1321 */
	BgL_nodez00_bglt BGl_z62gloz12zd2kwote1321za2zzintegrate_nodez00(obj_t
		BgL_envz00_3030, obj_t BgL_nodez00_3031, obj_t BgL_integratorz00_3032)
	{
		{	/* Integrate/node.scm 152 */
			return ((BgL_nodez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_3031));
		}

	}



/* &glo!-atom1319 */
	BgL_nodez00_bglt BGl_z62gloz12zd2atom1319za2zzintegrate_nodez00(obj_t
		BgL_envz00_3033, obj_t BgL_nodez00_3034, obj_t BgL_integratorz00_3035)
	{
		{	/* Integrate/node.scm 146 */
			return ((BgL_nodez00_bglt) ((BgL_atomz00_bglt) BgL_nodez00_3034));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzintegrate_nodez00(void)
	{
		{	/* Integrate/node.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1983z00zzintegrate_nodez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1983z00zzintegrate_nodez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1983z00zzintegrate_nodez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1983z00zzintegrate_nodez00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string1983z00zzintegrate_nodez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1983z00zzintegrate_nodez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1983z00zzintegrate_nodez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1983z00zzintegrate_nodez00));
			BGl_modulezd2initializa7ationz75zzast_localz00(315247917L,
				BSTRING_TO_STRING(BGl_string1983z00zzintegrate_nodez00));
			BGl_modulezd2initializa7ationz75zzintegrate_infoz00(0L,
				BSTRING_TO_STRING(BGl_string1983z00zzintegrate_nodez00));
			return
				BGl_modulezd2initializa7ationz75zzintegrate_localzd2ze3globalz31
				(143977554L, BSTRING_TO_STRING(BGl_string1983z00zzintegrate_nodez00));
		}

	}

#ifdef __cplusplus
}
#endif
