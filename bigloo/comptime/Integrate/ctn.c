/*===========================================================================*/
/*   (Integrate/ctn.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Integrate/ctn.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INTEGRATE_CTN_TYPE_DEFINITIONS
#define BGL_INTEGRATE_CTN_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_sfunzf2iinfozf2_bgl
	{
		obj_t BgL_ownerz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
		obj_t BgL_cfromz00;
		obj_t BgL_ctoz00;
		obj_t BgL_kz00;
		obj_t BgL_kza2za2;
		obj_t BgL_uz00;
		obj_t BgL_cnz00;
		obj_t BgL_ctz00;
		obj_t BgL_kontz00;
		bool_t BgL_gzf3zf3;
		bool_t BgL_forcegzf3zf3;
		obj_t BgL_lz00;
		obj_t BgL_ledz00;
		obj_t BgL_istampz00;
		obj_t BgL_globalz00;
		obj_t BgL_kapturedz00;
		obj_t BgL_tailzd2coercionzd2;
		bool_t BgL_xhdlzf3zf3;
		obj_t BgL_xhdlsz00;
	}                      *BgL_sfunzf2iinfozf2_bglt;


#endif													// BGL_INTEGRATE_CTN_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzintegrate_ctnz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_genericzd2initzd2zzintegrate_ctnz00(void);
	BGL_EXPORTED_DECL obj_t BGl_Cnz62Ctz12z70zzintegrate_ctnz00(obj_t);
	static obj_t BGl_objectzd2initzd2zzintegrate_ctnz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzintegrate_ctnz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzintegrate_ctnz00(void);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62Cnz62Ctz12z12zzintegrate_ctnz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_append2(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzintegrate_ctnz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_az00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_infoz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzintegrate_ctnz00(void);
	static obj_t BGl_globaliza7ez12zb5zzintegrate_ctnz00(BgL_localz00_bglt);
	static obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_ctnz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzintegrate_ctnz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzintegrate_ctnz00(void);
	static obj_t BGl_zc3z04anonymousza31333ze3ze70z60zzintegrate_ctnz00(obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t __cnst[2];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_Cnz62Ctz12zd2envza2zzintegrate_ctnz00,
		BgL_bgl_za762cnza762ctza712za7121626z00,
		BGl_z62Cnz62Ctz12z12zzintegrate_ctnz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1613z00zzintegrate_ctnz00,
		BgL_bgl_string1613za700za7za7i1627za7, "Cn&Ct!", 6);
	      DEFINE_STRING(BGl_string1614z00zzintegrate_ctnz00,
		BgL_bgl_string1614za700za7za7i1628za7, "Should not be here", 18);
	      DEFINE_STRING(BGl_string1615z00zzintegrate_ctnz00,
		BgL_bgl_string1615za700za7za7i1629za7, "integrate_ctn", 13);
	      DEFINE_STRING(BGl_string1616z00zzintegrate_ctnz00,
		BgL_bgl_string1616za700za7za7i1630za7, "escape tail ", 12);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzintegrate_ctnz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzintegrate_ctnz00(long
		BgL_checksumz00_1973, char *BgL_fromz00_1974)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzintegrate_ctnz00))
				{
					BGl_requirezd2initializa7ationz75zzintegrate_ctnz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzintegrate_ctnz00();
					BGl_libraryzd2moduleszd2initz00zzintegrate_ctnz00();
					BGl_cnstzd2initzd2zzintegrate_ctnz00();
					BGl_importedzd2moduleszd2initz00zzintegrate_ctnz00();
					return BGl_methodzd2initzd2zzintegrate_ctnz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_ctnz00(void)
	{
		{	/* Integrate/ctn.scm 17 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "integrate_ctn");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"integrate_ctn");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "integrate_ctn");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"integrate_ctn");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "integrate_ctn");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"integrate_ctn");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"integrate_ctn");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"integrate_ctn");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"integrate_ctn");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzintegrate_ctnz00(void)
	{
		{	/* Integrate/ctn.scm 17 */
			{	/* Integrate/ctn.scm 17 */
				obj_t BgL_cportz00_1962;

				{	/* Integrate/ctn.scm 17 */
					obj_t BgL_stringz00_1969;

					BgL_stringz00_1969 = BGl_string1616z00zzintegrate_ctnz00;
					{	/* Integrate/ctn.scm 17 */
						obj_t BgL_startz00_1970;

						BgL_startz00_1970 = BINT(0L);
						{	/* Integrate/ctn.scm 17 */
							obj_t BgL_endz00_1971;

							BgL_endz00_1971 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1969)));
							{	/* Integrate/ctn.scm 17 */

								BgL_cportz00_1962 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1969, BgL_startz00_1970, BgL_endz00_1971);
				}}}}
				{
					long BgL_iz00_1963;

					BgL_iz00_1963 = 1L;
				BgL_loopz00_1964:
					if ((BgL_iz00_1963 == -1L))
						{	/* Integrate/ctn.scm 17 */
							return BUNSPEC;
						}
					else
						{	/* Integrate/ctn.scm 17 */
							{	/* Integrate/ctn.scm 17 */
								obj_t BgL_arg1625z00_1965;

								{	/* Integrate/ctn.scm 17 */

									{	/* Integrate/ctn.scm 17 */
										obj_t BgL_locationz00_1967;

										BgL_locationz00_1967 = BBOOL(((bool_t) 0));
										{	/* Integrate/ctn.scm 17 */

											BgL_arg1625z00_1965 =
												BGl_readz00zz__readerz00(BgL_cportz00_1962,
												BgL_locationz00_1967);
										}
									}
								}
								{	/* Integrate/ctn.scm 17 */
									int BgL_tmpz00_2001;

									BgL_tmpz00_2001 = (int) (BgL_iz00_1963);
									CNST_TABLE_SET(BgL_tmpz00_2001, BgL_arg1625z00_1965);
							}}
							{	/* Integrate/ctn.scm 17 */
								int BgL_auxz00_1968;

								BgL_auxz00_1968 = (int) ((BgL_iz00_1963 - 1L));
								{
									long BgL_iz00_2006;

									BgL_iz00_2006 = (long) (BgL_auxz00_1968);
									BgL_iz00_1963 = BgL_iz00_2006;
									goto BgL_loopz00_1964;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzintegrate_ctnz00(void)
	{
		{	/* Integrate/ctn.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzintegrate_ctnz00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1561;

				BgL_headz00_1561 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1562;
					obj_t BgL_tailz00_1563;

					BgL_prevz00_1562 = BgL_headz00_1561;
					BgL_tailz00_1563 = BgL_l1z00_1;
				BgL_loopz00_1564:
					if (PAIRP(BgL_tailz00_1563))
						{
							obj_t BgL_newzd2prevzd2_1566;

							BgL_newzd2prevzd2_1566 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1563), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1562, BgL_newzd2prevzd2_1566);
							{
								obj_t BgL_tailz00_2016;
								obj_t BgL_prevz00_2015;

								BgL_prevz00_2015 = BgL_newzd2prevzd2_1566;
								BgL_tailz00_2016 = CDR(BgL_tailz00_1563);
								BgL_tailz00_1563 = BgL_tailz00_2016;
								BgL_prevz00_1562 = BgL_prevz00_2015;
								goto BgL_loopz00_1564;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1561);
				}
			}
		}

	}



/* Cn&Ct! */
	BGL_EXPORTED_DEF obj_t BGl_Cnz62Ctz12z70zzintegrate_ctnz00(obj_t BgL_az00_3)
	{
		{	/* Integrate/ctn.scm 33 */
			{
				obj_t BgL_asz00_1571;
				obj_t BgL_gzf2cnzf2_1572;

				BgL_asz00_1571 = BgL_az00_3;
				BgL_gzf2cnzf2_1572 = BNIL;
			BgL_zc3z04anonymousza31245ze3z87_1573:
				if (NULLP(BgL_asz00_1571))
					{	/* Integrate/ctn.scm 36 */
						return BgL_gzf2cnzf2_1572;
					}
				else
					{	/* Integrate/ctn.scm 40 */
						obj_t BgL_az00_1575;

						BgL_az00_1575 = CAR(((obj_t) BgL_asz00_1571));
						{	/* Integrate/ctn.scm 40 */
							obj_t BgL_fz00_1576;

							BgL_fz00_1576 = CAR(((obj_t) BgL_az00_1575));
							{	/* Integrate/ctn.scm 41 */
								obj_t BgL_gz00_1577;

								{	/* Integrate/ctn.scm 42 */
									obj_t BgL_pairz00_1818;

									BgL_pairz00_1818 = CDR(((obj_t) BgL_az00_1575));
									BgL_gz00_1577 = CAR(BgL_pairz00_1818);
								}
								{	/* Integrate/ctn.scm 42 */
									obj_t BgL_kz00_1578;

									{	/* Integrate/ctn.scm 43 */
										obj_t BgL_pairz00_1824;

										{	/* Integrate/ctn.scm 43 */
											obj_t BgL_pairz00_1823;

											BgL_pairz00_1823 = CDR(((obj_t) BgL_az00_1575));
											BgL_pairz00_1824 = CDR(BgL_pairz00_1823);
										}
										BgL_kz00_1578 = CAR(BgL_pairz00_1824);
									}
									{	/* Integrate/ctn.scm 43 */
										BgL_valuez00_bglt BgL_fiz00_1579;

										BgL_fiz00_1579 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt) BgL_fz00_1576)))->
											BgL_valuez00);
										{	/* Integrate/ctn.scm 44 */
											BgL_valuez00_bglt BgL_giz00_1580;

											BgL_giz00_1580 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_gz00_1577)))->
												BgL_valuez00);
											{	/* Integrate/ctn.scm 45 */

												{	/* Integrate/ctn.scm 47 */
													bool_t BgL_test1635z00_2036;

													{	/* Integrate/ctn.scm 47 */
														obj_t BgL_classz00_1827;

														BgL_classz00_1827 = BGl_globalz00zzast_varz00;
														if (BGL_OBJECTP(BgL_gz00_1577))
															{	/* Integrate/ctn.scm 47 */
																BgL_objectz00_bglt BgL_arg1807z00_1829;

																BgL_arg1807z00_1829 =
																	(BgL_objectz00_bglt) (BgL_gz00_1577);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Integrate/ctn.scm 47 */
																		long BgL_idxz00_1835;

																		BgL_idxz00_1835 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_1829);
																		BgL_test1635z00_2036 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_1835 + 2L)) ==
																			BgL_classz00_1827);
																	}
																else
																	{	/* Integrate/ctn.scm 47 */
																		bool_t BgL_res1610z00_1860;

																		{	/* Integrate/ctn.scm 47 */
																			obj_t BgL_oclassz00_1843;

																			{	/* Integrate/ctn.scm 47 */
																				obj_t BgL_arg1815z00_1851;
																				long BgL_arg1816z00_1852;

																				BgL_arg1815z00_1851 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Integrate/ctn.scm 47 */
																					long BgL_arg1817z00_1853;

																					BgL_arg1817z00_1853 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_1829);
																					BgL_arg1816z00_1852 =
																						(BgL_arg1817z00_1853 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_1843 =
																					VECTOR_REF(BgL_arg1815z00_1851,
																					BgL_arg1816z00_1852);
																			}
																			{	/* Integrate/ctn.scm 47 */
																				bool_t BgL__ortest_1115z00_1844;

																				BgL__ortest_1115z00_1844 =
																					(BgL_classz00_1827 ==
																					BgL_oclassz00_1843);
																				if (BgL__ortest_1115z00_1844)
																					{	/* Integrate/ctn.scm 47 */
																						BgL_res1610z00_1860 =
																							BgL__ortest_1115z00_1844;
																					}
																				else
																					{	/* Integrate/ctn.scm 47 */
																						long BgL_odepthz00_1845;

																						{	/* Integrate/ctn.scm 47 */
																							obj_t BgL_arg1804z00_1846;

																							BgL_arg1804z00_1846 =
																								(BgL_oclassz00_1843);
																							BgL_odepthz00_1845 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_1846);
																						}
																						if ((2L < BgL_odepthz00_1845))
																							{	/* Integrate/ctn.scm 47 */
																								obj_t BgL_arg1802z00_1848;

																								{	/* Integrate/ctn.scm 47 */
																									obj_t BgL_arg1803z00_1849;

																									BgL_arg1803z00_1849 =
																										(BgL_oclassz00_1843);
																									BgL_arg1802z00_1848 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_1849, 2L);
																								}
																								BgL_res1610z00_1860 =
																									(BgL_arg1802z00_1848 ==
																									BgL_classz00_1827);
																							}
																						else
																							{	/* Integrate/ctn.scm 47 */
																								BgL_res1610z00_1860 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test1635z00_2036 = BgL_res1610z00_1860;
																	}
															}
														else
															{	/* Integrate/ctn.scm 47 */
																BgL_test1635z00_2036 = ((bool_t) 0);
															}
													}
													if (BgL_test1635z00_2036)
														{	/* Integrate/ctn.scm 48 */
															obj_t BgL_arg1248z00_1582;

															BgL_arg1248z00_1582 =
																CDR(((obj_t) BgL_asz00_1571));
															{
																obj_t BgL_asz00_2061;

																BgL_asz00_2061 = BgL_arg1248z00_1582;
																BgL_asz00_1571 = BgL_asz00_2061;
																goto BgL_zc3z04anonymousza31245ze3z87_1573;
															}
														}
													else
														{	/* Integrate/ctn.scm 49 */
															bool_t BgL_test1640z00_2062;

															{
																BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2063;

																{
																	obj_t BgL_auxz00_2064;

																	{	/* Integrate/ctn.scm 49 */
																		BgL_objectz00_bglt BgL_tmpz00_2065;

																		BgL_tmpz00_2065 =
																			((BgL_objectz00_bglt)
																			((BgL_sfunz00_bglt) BgL_giz00_1580));
																		BgL_auxz00_2064 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2065);
																	}
																	BgL_auxz00_2063 =
																		((BgL_sfunzf2iinfozf2_bglt)
																		BgL_auxz00_2064);
																}
																BgL_test1640z00_2062 =
																	(((BgL_sfunzf2iinfozf2_bglt)
																		COBJECT(BgL_auxz00_2063))->
																	BgL_forcegzf3zf3);
															}
															if (BgL_test1640z00_2062)
																{	/* Integrate/ctn.scm 50 */
																	bool_t BgL_test1641z00_2071;

																	{
																		BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2072;

																		{
																			obj_t BgL_auxz00_2073;

																			{	/* Integrate/ctn.scm 50 */
																				BgL_objectz00_bglt BgL_tmpz00_2074;

																				BgL_tmpz00_2074 =
																					((BgL_objectz00_bglt)
																					((BgL_sfunz00_bglt) BgL_giz00_1580));
																				BgL_auxz00_2073 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_2074);
																			}
																			BgL_auxz00_2072 =
																				((BgL_sfunzf2iinfozf2_bglt)
																				BgL_auxz00_2073);
																		}
																		BgL_test1641z00_2071 =
																			(((BgL_sfunzf2iinfozf2_bglt)
																				COBJECT(BgL_auxz00_2072))->BgL_gzf3zf3);
																	}
																	if (BgL_test1641z00_2071)
																		{	/* Integrate/ctn.scm 52 */
																			obj_t BgL_arg1252z00_1585;

																			BgL_arg1252z00_1585 =
																				CDR(((obj_t) BgL_asz00_1571));
																			{
																				obj_t BgL_asz00_2082;

																				BgL_asz00_2082 = BgL_arg1252z00_1585;
																				BgL_asz00_1571 = BgL_asz00_2082;
																				goto
																					BgL_zc3z04anonymousza31245ze3z87_1573;
																			}
																		}
																	else
																		{	/* Integrate/ctn.scm 51 */
																			obj_t BgL_arg1268z00_1586;
																			obj_t BgL_arg1272z00_1587;

																			BgL_arg1268z00_1586 =
																				CDR(((obj_t) BgL_asz00_1571));
																			{	/* Integrate/ctn.scm 51 */
																				obj_t BgL_arg1284z00_1588;

																				BgL_arg1284z00_1588 =
																					BGl_globaliza7ez12zb5zzintegrate_ctnz00
																					(((BgL_localz00_bglt) BgL_gz00_1577));
																				BgL_arg1272z00_1587 =
																					BGl_appendzd221011zd2zzintegrate_ctnz00
																					(BgL_arg1284z00_1588,
																					BgL_gzf2cnzf2_1572);
																			}
																			{
																				obj_t BgL_gzf2cnzf2_2089;
																				obj_t BgL_asz00_2088;

																				BgL_asz00_2088 = BgL_arg1268z00_1586;
																				BgL_gzf2cnzf2_2089 =
																					BgL_arg1272z00_1587;
																				BgL_gzf2cnzf2_1572 = BgL_gzf2cnzf2_2089;
																				BgL_asz00_1571 = BgL_asz00_2088;
																				goto
																					BgL_zc3z04anonymousza31245ze3z87_1573;
																			}
																		}
																}
															else
																{	/* Integrate/ctn.scm 49 */
																	if ((BgL_kz00_1578 == CNST_TABLE_REF(0)))
																		{	/* Integrate/ctn.scm 53 */
																			{	/* Integrate/ctn.scm 54 */
																				obj_t BgL_arg1304z00_1589;

																				{	/* Integrate/ctn.scm 54 */
																					obj_t BgL_arg1305z00_1590;

																					{
																						BgL_sfunzf2iinfozf2_bglt
																							BgL_auxz00_2093;
																						{
																							obj_t BgL_auxz00_2094;

																							{	/* Integrate/ctn.scm 54 */
																								BgL_objectz00_bglt
																									BgL_tmpz00_2095;
																								BgL_tmpz00_2095 =
																									((BgL_objectz00_bglt) (
																										(BgL_sfunz00_bglt)
																										BgL_fiz00_1579));
																								BgL_auxz00_2094 =
																									BGL_OBJECT_WIDENING
																									(BgL_tmpz00_2095);
																							}
																							BgL_auxz00_2093 =
																								((BgL_sfunzf2iinfozf2_bglt)
																								BgL_auxz00_2094);
																						}
																						BgL_arg1305z00_1590 =
																							(((BgL_sfunzf2iinfozf2_bglt)
																								COBJECT(BgL_auxz00_2093))->
																							BgL_ctz00);
																					}
																					BgL_arg1304z00_1589 =
																						MAKE_YOUNG_PAIR(BgL_gz00_1577,
																						BgL_arg1305z00_1590);
																				}
																				{
																					BgL_sfunzf2iinfozf2_bglt
																						BgL_auxz00_2102;
																					{
																						obj_t BgL_auxz00_2103;

																						{	/* Integrate/ctn.scm 54 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_2104;
																							BgL_tmpz00_2104 =
																								((BgL_objectz00_bglt) (
																									(BgL_sfunz00_bglt)
																									BgL_fiz00_1579));
																							BgL_auxz00_2103 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_2104);
																						}
																						BgL_auxz00_2102 =
																							((BgL_sfunzf2iinfozf2_bglt)
																							BgL_auxz00_2103);
																					}
																					((((BgL_sfunzf2iinfozf2_bglt)
																								COBJECT(BgL_auxz00_2102))->
																							BgL_ctz00) =
																						((obj_t) BgL_arg1304z00_1589),
																						BUNSPEC);
																				}
																			}
																			{	/* Integrate/ctn.scm 55 */
																				bool_t BgL_test1643z00_2110;

																				if ((BgL_fz00_1576 == BgL_gz00_1577))
																					{	/* Integrate/ctn.scm 55 */
																						BgL_test1643z00_2110 = ((bool_t) 0);
																					}
																				else
																					{	/* Integrate/ctn.scm 56 */
																						bool_t BgL_test1645z00_2113;

																						{	/* Integrate/ctn.scm 56 */
																							obj_t BgL_arg1312z00_1598;

																							{
																								BgL_sfunzf2iinfozf2_bglt
																									BgL_auxz00_2114;
																								{
																									obj_t BgL_auxz00_2115;

																									{	/* Integrate/ctn.scm 56 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_2116;
																										BgL_tmpz00_2116 =
																											((BgL_objectz00_bglt) (
																												(BgL_sfunz00_bglt)
																												BgL_fiz00_1579));
																										BgL_auxz00_2115 =
																											BGL_OBJECT_WIDENING
																											(BgL_tmpz00_2116);
																									}
																									BgL_auxz00_2114 =
																										((BgL_sfunzf2iinfozf2_bglt)
																										BgL_auxz00_2115);
																								}
																								BgL_arg1312z00_1598 =
																									(((BgL_sfunzf2iinfozf2_bglt)
																										COBJECT(BgL_auxz00_2114))->
																									BgL_kontz00);
																							}
																							BgL_test1645z00_2113 =
																								CBOOL
																								(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																								(BgL_gz00_1577,
																									BgL_arg1312z00_1598));
																						}
																						if (BgL_test1645z00_2113)
																							{	/* Integrate/ctn.scm 56 */
																								BgL_test1643z00_2110 =
																									((bool_t) 0);
																							}
																						else
																							{	/* Integrate/ctn.scm 56 */
																								BgL_test1643z00_2110 =
																									((bool_t) 1);
																							}
																					}
																				if (BgL_test1643z00_2110)
																					{	/* Integrate/ctn.scm 57 */
																						obj_t BgL_arg1310z00_1595;

																						{	/* Integrate/ctn.scm 57 */
																							obj_t BgL_arg1311z00_1596;

																							{
																								BgL_sfunzf2iinfozf2_bglt
																									BgL_auxz00_2124;
																								{
																									obj_t BgL_auxz00_2125;

																									{	/* Integrate/ctn.scm 57 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_2126;
																										BgL_tmpz00_2126 =
																											((BgL_objectz00_bglt) (
																												(BgL_sfunz00_bglt)
																												BgL_fiz00_1579));
																										BgL_auxz00_2125 =
																											BGL_OBJECT_WIDENING
																											(BgL_tmpz00_2126);
																									}
																									BgL_auxz00_2124 =
																										((BgL_sfunzf2iinfozf2_bglt)
																										BgL_auxz00_2125);
																								}
																								BgL_arg1311z00_1596 =
																									(((BgL_sfunzf2iinfozf2_bglt)
																										COBJECT(BgL_auxz00_2124))->
																									BgL_kontz00);
																							}
																							BgL_arg1310z00_1595 =
																								MAKE_YOUNG_PAIR(BgL_gz00_1577,
																								BgL_arg1311z00_1596);
																						}
																						{
																							BgL_sfunzf2iinfozf2_bglt
																								BgL_auxz00_2133;
																							{
																								obj_t BgL_auxz00_2134;

																								{	/* Integrate/ctn.scm 57 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_2135;
																									BgL_tmpz00_2135 =
																										((BgL_objectz00_bglt) (
																											(BgL_sfunz00_bglt)
																											BgL_fiz00_1579));
																									BgL_auxz00_2134 =
																										BGL_OBJECT_WIDENING
																										(BgL_tmpz00_2135);
																								}
																								BgL_auxz00_2133 =
																									((BgL_sfunzf2iinfozf2_bglt)
																									BgL_auxz00_2134);
																							}
																							((((BgL_sfunzf2iinfozf2_bglt)
																										COBJECT(BgL_auxz00_2133))->
																									BgL_kontz00) =
																								((obj_t) BgL_arg1310z00_1595),
																								BUNSPEC);
																						}
																					}
																				else
																					{	/* Integrate/ctn.scm 55 */
																						BFALSE;
																					}
																			}
																			{	/* Integrate/ctn.scm 58 */
																				obj_t BgL_arg1314z00_1599;

																				BgL_arg1314z00_1599 =
																					CDR(((obj_t) BgL_asz00_1571));
																				{
																					obj_t BgL_asz00_2143;

																					BgL_asz00_2143 = BgL_arg1314z00_1599;
																					BgL_asz00_1571 = BgL_asz00_2143;
																					goto
																						BgL_zc3z04anonymousza31245ze3z87_1573;
																				}
																			}
																		}
																	else
																		{	/* Integrate/ctn.scm 53 */
																			if ((BgL_kz00_1578 == CNST_TABLE_REF(1)))
																				{	/* Integrate/ctn.scm 59 */
																					return
																						BGl_errorz00zz__errorz00
																						(BGl_string1613z00zzintegrate_ctnz00,
																						BGl_string1614z00zzintegrate_ctnz00,
																						BgL_az00_1575);
																				}
																			else
																				{	/* Integrate/ctn.scm 61 */
																					bool_t BgL_test1647z00_2148;

																					{	/* Integrate/ctn.scm 61 */
																						obj_t BgL_tmpz00_2149;

																						{
																							BgL_sfunzf2iinfozf2_bglt
																								BgL_auxz00_2150;
																							{
																								obj_t BgL_auxz00_2151;

																								{	/* Integrate/ctn.scm 61 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_2152;
																									BgL_tmpz00_2152 =
																										((BgL_objectz00_bglt) (
																											(BgL_sfunz00_bglt)
																											BgL_giz00_1580));
																									BgL_auxz00_2151 =
																										BGL_OBJECT_WIDENING
																										(BgL_tmpz00_2152);
																								}
																								BgL_auxz00_2150 =
																									((BgL_sfunzf2iinfozf2_bglt)
																									BgL_auxz00_2151);
																							}
																							BgL_tmpz00_2149 =
																								(((BgL_sfunzf2iinfozf2_bglt)
																									COBJECT(BgL_auxz00_2150))->
																								BgL_uz00);
																						}
																						BgL_test1647z00_2148 =
																							CBOOL(BgL_tmpz00_2149);
																					}
																					if (BgL_test1647z00_2148)
																						{	/* Integrate/ctn.scm 61 */
																							{	/* Integrate/ctn.scm 62 */
																								obj_t BgL_arg1316z00_1601;

																								{	/* Integrate/ctn.scm 62 */
																									obj_t BgL_arg1317z00_1602;

																									{
																										BgL_sfunzf2iinfozf2_bglt
																											BgL_auxz00_2159;
																										{
																											obj_t BgL_auxz00_2160;

																											{	/* Integrate/ctn.scm 62 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_2161;
																												BgL_tmpz00_2161 =
																													((BgL_objectz00_bglt)
																													((BgL_sfunz00_bglt)
																														BgL_fiz00_1579));
																												BgL_auxz00_2160 =
																													BGL_OBJECT_WIDENING
																													(BgL_tmpz00_2161);
																											}
																											BgL_auxz00_2159 =
																												(
																												(BgL_sfunzf2iinfozf2_bglt)
																												BgL_auxz00_2160);
																										}
																										BgL_arg1317z00_1602 =
																											(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2159))->BgL_ctz00);
																									}
																									BgL_arg1316z00_1601 =
																										MAKE_YOUNG_PAIR
																										(BgL_gz00_1577,
																										BgL_arg1317z00_1602);
																								}
																								{
																									BgL_sfunzf2iinfozf2_bglt
																										BgL_auxz00_2168;
																									{
																										obj_t BgL_auxz00_2169;

																										{	/* Integrate/ctn.scm 62 */
																											BgL_objectz00_bglt
																												BgL_tmpz00_2170;
																											BgL_tmpz00_2170 =
																												((BgL_objectz00_bglt) (
																													(BgL_sfunz00_bglt)
																													BgL_fiz00_1579));
																											BgL_auxz00_2169 =
																												BGL_OBJECT_WIDENING
																												(BgL_tmpz00_2170);
																										}
																										BgL_auxz00_2168 =
																											(
																											(BgL_sfunzf2iinfozf2_bglt)
																											BgL_auxz00_2169);
																									}
																									((((BgL_sfunzf2iinfozf2_bglt)
																												COBJECT
																												(BgL_auxz00_2168))->
																											BgL_ctz00) =
																										((obj_t)
																											BgL_arg1316z00_1601),
																										BUNSPEC);
																								}
																							}
																							{	/* Integrate/ctn.scm 63 */
																								bool_t BgL_test1648z00_2176;

																								{	/* Integrate/ctn.scm 63 */
																									obj_t BgL_arg1322z00_1607;

																									{
																										BgL_sfunzf2iinfozf2_bglt
																											BgL_auxz00_2177;
																										{
																											obj_t BgL_auxz00_2178;

																											{	/* Integrate/ctn.scm 63 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_2179;
																												BgL_tmpz00_2179 =
																													((BgL_objectz00_bglt)
																													((BgL_sfunz00_bglt)
																														BgL_fiz00_1579));
																												BgL_auxz00_2178 =
																													BGL_OBJECT_WIDENING
																													(BgL_tmpz00_2179);
																											}
																											BgL_auxz00_2177 =
																												(
																												(BgL_sfunzf2iinfozf2_bglt)
																												BgL_auxz00_2178);
																										}
																										BgL_arg1322z00_1607 =
																											(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2177))->BgL_kontz00);
																									}
																									BgL_test1648z00_2176 =
																										CBOOL
																										(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																										(BgL_gz00_1577,
																											BgL_arg1322z00_1607));
																								}
																								if (BgL_test1648z00_2176)
																									{	/* Integrate/ctn.scm 63 */
																										BFALSE;
																									}
																								else
																									{	/* Integrate/ctn.scm 64 */
																										obj_t BgL_arg1320z00_1605;

																										{	/* Integrate/ctn.scm 64 */
																											obj_t BgL_arg1321z00_1606;

																											{
																												BgL_sfunzf2iinfozf2_bglt
																													BgL_auxz00_2187;
																												{
																													obj_t BgL_auxz00_2188;

																													{	/* Integrate/ctn.scm 64 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_2189;
																														BgL_tmpz00_2189 =
																															(
																															(BgL_objectz00_bglt)
																															((BgL_sfunz00_bglt) BgL_fiz00_1579));
																														BgL_auxz00_2188 =
																															BGL_OBJECT_WIDENING
																															(BgL_tmpz00_2189);
																													}
																													BgL_auxz00_2187 =
																														(
																														(BgL_sfunzf2iinfozf2_bglt)
																														BgL_auxz00_2188);
																												}
																												BgL_arg1321z00_1606 =
																													(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2187))->BgL_kontz00);
																											}
																											BgL_arg1320z00_1605 =
																												MAKE_YOUNG_PAIR
																												(BgL_gz00_1577,
																												BgL_arg1321z00_1606);
																										}
																										{
																											BgL_sfunzf2iinfozf2_bglt
																												BgL_auxz00_2196;
																											{
																												obj_t BgL_auxz00_2197;

																												{	/* Integrate/ctn.scm 64 */
																													BgL_objectz00_bglt
																														BgL_tmpz00_2198;
																													BgL_tmpz00_2198 =
																														(
																														(BgL_objectz00_bglt)
																														((BgL_sfunz00_bglt)
																															BgL_fiz00_1579));
																													BgL_auxz00_2197 =
																														BGL_OBJECT_WIDENING
																														(BgL_tmpz00_2198);
																												}
																												BgL_auxz00_2196 =
																													(
																													(BgL_sfunzf2iinfozf2_bglt)
																													BgL_auxz00_2197);
																											}
																											((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2196))->BgL_kontz00) = ((obj_t) BgL_arg1320z00_1605), BUNSPEC);
																										}
																									}
																							}
																							{	/* Integrate/ctn.scm 65 */
																								obj_t BgL_arg1323z00_1608;

																								BgL_arg1323z00_1608 =
																									CDR(((obj_t) BgL_asz00_1571));
																								{
																									obj_t BgL_asz00_2206;

																									BgL_asz00_2206 =
																										BgL_arg1323z00_1608;
																									BgL_asz00_1571 =
																										BgL_asz00_2206;
																									goto
																										BgL_zc3z04anonymousza31245ze3z87_1573;
																								}
																							}
																						}
																					else
																						{	/* Integrate/ctn.scm 61 */
																							{	/* Integrate/ctn.scm 67 */
																								obj_t BgL_arg1325z00_1609;

																								{	/* Integrate/ctn.scm 67 */
																									obj_t BgL_arg1326z00_1610;

																									{
																										BgL_sfunzf2iinfozf2_bglt
																											BgL_auxz00_2207;
																										{
																											obj_t BgL_auxz00_2208;

																											{	/* Integrate/ctn.scm 67 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_2209;
																												BgL_tmpz00_2209 =
																													((BgL_objectz00_bglt)
																													((BgL_sfunz00_bglt)
																														BgL_fiz00_1579));
																												BgL_auxz00_2208 =
																													BGL_OBJECT_WIDENING
																													(BgL_tmpz00_2209);
																											}
																											BgL_auxz00_2207 =
																												(
																												(BgL_sfunzf2iinfozf2_bglt)
																												BgL_auxz00_2208);
																										}
																										BgL_arg1326z00_1610 =
																											(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2207))->BgL_cnz00);
																									}
																									BgL_arg1325z00_1609 =
																										MAKE_YOUNG_PAIR
																										(BgL_gz00_1577,
																										BgL_arg1326z00_1610);
																								}
																								{
																									BgL_sfunzf2iinfozf2_bglt
																										BgL_auxz00_2216;
																									{
																										obj_t BgL_auxz00_2217;

																										{	/* Integrate/ctn.scm 67 */
																											BgL_objectz00_bglt
																												BgL_tmpz00_2218;
																											BgL_tmpz00_2218 =
																												((BgL_objectz00_bglt) (
																													(BgL_sfunz00_bglt)
																													BgL_fiz00_1579));
																											BgL_auxz00_2217 =
																												BGL_OBJECT_WIDENING
																												(BgL_tmpz00_2218);
																										}
																										BgL_auxz00_2216 =
																											(
																											(BgL_sfunzf2iinfozf2_bglt)
																											BgL_auxz00_2217);
																									}
																									((((BgL_sfunzf2iinfozf2_bglt)
																												COBJECT
																												(BgL_auxz00_2216))->
																											BgL_cnz00) =
																										((obj_t)
																											BgL_arg1325z00_1609),
																										BUNSPEC);
																								}
																							}
																							{	/* Integrate/ctn.scm 68 */
																								bool_t BgL_test1649z00_2224;

																								{
																									BgL_sfunzf2iinfozf2_bglt
																										BgL_auxz00_2225;
																									{
																										obj_t BgL_auxz00_2226;

																										{	/* Integrate/ctn.scm 68 */
																											BgL_objectz00_bglt
																												BgL_tmpz00_2227;
																											BgL_tmpz00_2227 =
																												((BgL_objectz00_bglt) (
																													(BgL_sfunz00_bglt)
																													BgL_giz00_1580));
																											BgL_auxz00_2226 =
																												BGL_OBJECT_WIDENING
																												(BgL_tmpz00_2227);
																										}
																										BgL_auxz00_2225 =
																											(
																											(BgL_sfunzf2iinfozf2_bglt)
																											BgL_auxz00_2226);
																									}
																									BgL_test1649z00_2224 =
																										(((BgL_sfunzf2iinfozf2_bglt)
																											COBJECT
																											(BgL_auxz00_2225))->
																										BgL_gzf3zf3);
																								}
																								if (BgL_test1649z00_2224)
																									{	/* Integrate/ctn.scm 70 */
																										obj_t BgL_arg1328z00_1612;

																										BgL_arg1328z00_1612 =
																											CDR(
																											((obj_t) BgL_asz00_1571));
																										{
																											obj_t BgL_asz00_2235;

																											BgL_asz00_2235 =
																												BgL_arg1328z00_1612;
																											BgL_asz00_1571 =
																												BgL_asz00_2235;
																											goto
																												BgL_zc3z04anonymousza31245ze3z87_1573;
																										}
																									}
																								else
																									{	/* Integrate/ctn.scm 69 */
																										obj_t BgL_arg1329z00_1613;
																										obj_t BgL_arg1331z00_1614;

																										BgL_arg1329z00_1613 =
																											CDR(
																											((obj_t) BgL_asz00_1571));
																										{	/* Integrate/ctn.scm 69 */
																											obj_t BgL_arg1332z00_1615;

																											BgL_arg1332z00_1615 =
																												BGl_globaliza7ez12zb5zzintegrate_ctnz00
																												(((BgL_localz00_bglt)
																													BgL_gz00_1577));
																											BgL_arg1331z00_1614 =
																												BGl_appendzd221011zd2zzintegrate_ctnz00
																												(BgL_arg1332z00_1615,
																												BgL_gzf2cnzf2_1572);
																										}
																										{
																											obj_t BgL_gzf2cnzf2_2242;
																											obj_t BgL_asz00_2241;

																											BgL_asz00_2241 =
																												BgL_arg1329z00_1613;
																											BgL_gzf2cnzf2_2242 =
																												BgL_arg1331z00_1614;
																											BgL_gzf2cnzf2_1572 =
																												BgL_gzf2cnzf2_2242;
																											BgL_asz00_1571 =
																												BgL_asz00_2241;
																											goto
																												BgL_zc3z04anonymousza31245ze3z87_1573;
																										}
																									}
																							}
																						}
																				}
																		}
																}
														}
												}
											}
										}
									}
								}
							}
						}
					}
			}
		}

	}



/* &Cn&Ct! */
	obj_t BGl_z62Cnz62Ctz12z12zzintegrate_ctnz00(obj_t BgL_envz00_1960,
		obj_t BgL_az00_1961)
	{
		{	/* Integrate/ctn.scm 33 */
			return BGl_Cnz62Ctz12z70zzintegrate_ctnz00(BgL_az00_1961);
		}

	}



/* globalize! */
	obj_t BGl_globaliza7ez12zb5zzintegrate_ctnz00(BgL_localz00_bglt BgL_lz00_4)
	{
		{	/* Integrate/ctn.scm 75 */
			{	/* Integrate/ctn.scm 76 */
				BgL_valuez00_bglt BgL_iz00_1617;

				BgL_iz00_1617 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_lz00_4)))->BgL_valuez00);
				{
					BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2246;

					{
						obj_t BgL_auxz00_2247;

						{	/* Integrate/ctn.scm 77 */
							BgL_objectz00_bglt BgL_tmpz00_2248;

							BgL_tmpz00_2248 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_iz00_1617));
							BgL_auxz00_2247 = BGL_OBJECT_WIDENING(BgL_tmpz00_2248);
						}
						BgL_auxz00_2246 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2247);
					}
					((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2246))->
							BgL_gzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
				}
				{	/* Integrate/ctn.scm 78 */
					obj_t BgL_xgz00_1618;

					{	/* Integrate/ctn.scm 78 */
						obj_t BgL_g1240z00_1619;

						{
							BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2254;

							{
								obj_t BgL_auxz00_2255;

								{	/* Integrate/ctn.scm 85 */
									BgL_objectz00_bglt BgL_tmpz00_2256;

									BgL_tmpz00_2256 =
										((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_iz00_1617));
									BgL_auxz00_2255 = BGL_OBJECT_WIDENING(BgL_tmpz00_2256);
								}
								BgL_auxz00_2254 = ((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2255);
							}
							BgL_g1240z00_1619 =
								(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2254))->
								BgL_xhdlsz00);
						}
						BgL_xgz00_1618 =
							BGl_zc3z04anonymousza31333ze3ze70z60zzintegrate_ctnz00
							(BgL_g1240z00_1619);
					}
					return MAKE_YOUNG_PAIR(((obj_t) BgL_lz00_4), BgL_xgz00_1618);
				}
			}
		}

	}



/* <@anonymous:1333>~0 */
	obj_t BGl_zc3z04anonymousza31333ze3ze70z60zzintegrate_ctnz00(obj_t
		BgL_l1239z00_1621)
	{
		{	/* Integrate/ctn.scm 85 */
			if (NULLP(BgL_l1239z00_1621))
				{	/* Integrate/ctn.scm 85 */
					return BNIL;
				}
			else
				{	/* Integrate/ctn.scm 79 */
					obj_t BgL_arg1335z00_1624;
					obj_t BgL_arg1339z00_1625;

					{	/* Integrate/ctn.scm 79 */
						obj_t BgL_xz00_1626;

						BgL_xz00_1626 = CAR(((obj_t) BgL_l1239z00_1621));
						{	/* Integrate/ctn.scm 79 */
							BgL_valuez00_bglt BgL_xiz00_1627;

							BgL_xiz00_1627 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_localz00_bglt) BgL_xz00_1626))))->BgL_valuez00);
							{	/* Integrate/ctn.scm 80 */
								bool_t BgL_test1652z00_2272;

								{
									BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2273;

									{
										obj_t BgL_auxz00_2274;

										{	/* Integrate/ctn.scm 80 */
											BgL_objectz00_bglt BgL_tmpz00_2275;

											BgL_tmpz00_2275 =
												((BgL_objectz00_bglt)
												((BgL_sfunz00_bglt) BgL_xiz00_1627));
											BgL_auxz00_2274 = BGL_OBJECT_WIDENING(BgL_tmpz00_2275);
										}
										BgL_auxz00_2273 =
											((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2274);
									}
									BgL_test1652z00_2272 =
										(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2273))->
										BgL_gzf3zf3);
								}
								if (BgL_test1652z00_2272)
									{	/* Integrate/ctn.scm 80 */
										BgL_arg1335z00_1624 = BNIL;
									}
								else
									{	/* Integrate/ctn.scm 80 */
										{
											BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2281;

											{
												obj_t BgL_auxz00_2282;

												{	/* Integrate/ctn.scm 83 */
													BgL_objectz00_bglt BgL_tmpz00_2283;

													BgL_tmpz00_2283 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_xiz00_1627));
													BgL_auxz00_2282 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_2283);
												}
												BgL_auxz00_2281 =
													((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2282);
											}
											((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2281))->
													BgL_gzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
										}
										{	/* Integrate/ctn.scm 84 */
											obj_t BgL_list1342z00_1629;

											BgL_list1342z00_1629 =
												MAKE_YOUNG_PAIR(BgL_xz00_1626, BNIL);
											BgL_arg1335z00_1624 = BgL_list1342z00_1629;
										}
									}
							}
						}
					}
					{	/* Integrate/ctn.scm 85 */
						obj_t BgL_arg1343z00_1630;

						BgL_arg1343z00_1630 = CDR(((obj_t) BgL_l1239z00_1621));
						BgL_arg1339z00_1625 =
							BGl_zc3z04anonymousza31333ze3ze70z60zzintegrate_ctnz00
							(BgL_arg1343z00_1630);
					}
					return bgl_append2(BgL_arg1335z00_1624, BgL_arg1339z00_1625);
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzintegrate_ctnz00(void)
	{
		{	/* Integrate/ctn.scm 17 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzintegrate_ctnz00(void)
	{
		{	/* Integrate/ctn.scm 17 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzintegrate_ctnz00(void)
	{
		{	/* Integrate/ctn.scm 17 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzintegrate_ctnz00(void)
	{
		{	/* Integrate/ctn.scm 17 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1615z00zzintegrate_ctnz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1615z00zzintegrate_ctnz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1615z00zzintegrate_ctnz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1615z00zzintegrate_ctnz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1615z00zzintegrate_ctnz00));
			BGl_modulezd2initializa7ationz75zzintegrate_infoz00(0L,
				BSTRING_TO_STRING(BGl_string1615z00zzintegrate_ctnz00));
			return
				BGl_modulezd2initializa7ationz75zzintegrate_az00(523633197L,
				BSTRING_TO_STRING(BGl_string1615z00zzintegrate_ctnz00));
		}

	}

#ifdef __cplusplus
}
#endif
