/*===========================================================================*/
/*   (Integrate/g.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Integrate/g.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_INTEGRATE_G_TYPE_DEFINITIONS
#define BGL_INTEGRATE_G_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_sfunzf2iinfozf2_bgl
	{
		obj_t BgL_ownerz00;
		obj_t BgL_freez00;
		obj_t BgL_boundz00;
		obj_t BgL_cfromz00;
		obj_t BgL_ctoz00;
		obj_t BgL_kz00;
		obj_t BgL_kza2za2;
		obj_t BgL_uz00;
		obj_t BgL_cnz00;
		obj_t BgL_ctz00;
		obj_t BgL_kontz00;
		bool_t BgL_gzf3zf3;
		bool_t BgL_forcegzf3zf3;
		obj_t BgL_lz00;
		obj_t BgL_ledz00;
		obj_t BgL_istampz00;
		obj_t BgL_globalz00;
		obj_t BgL_kapturedz00;
		obj_t BgL_tailzd2coercionzd2;
		bool_t BgL_xhdlzf3zf3;
		obj_t BgL_xhdlsz00;
	}                      *BgL_sfunzf2iinfozf2_bglt;


#endif													// BGL_INTEGRATE_G_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzintegrate_gz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static bool_t
		BGl_integratezd2remainingzd2localzd2functionsz12zc0zzintegrate_gz00(void);
	static obj_t BGl_genericzd2initzd2zzintegrate_gz00(void);
	static obj_t BGl_objectzd2initzd2zzintegrate_gz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_methodzd2initzd2zzintegrate_gz00(void);
	static obj_t BGl_z62Gz12z70zzintegrate_gz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzintegrate_gz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_az00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzintegrate_infoz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_gz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzintegrate_gz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzintegrate_gz00(void);
	extern obj_t BGl_variablez00zzast_varz00;
	BGL_EXPORTED_DECL obj_t BGl_Gz12z12zzintegrate_gz00(obj_t);
	extern obj_t BGl_za2phiza2z00zzintegrate_az00;
	static obj_t *__cnst;


	   
		 
		DEFINE_STRING(BGl_string1712z00zzintegrate_gz00,
		BgL_bgl_string1712za700za7za7i1713za7, "integrate_g", 11);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_Gz12zd2envzc0zzintegrate_gz00,
		BgL_bgl_za762gza712za770za7za7inte1714za7, BGl_z62Gz12z70zzintegrate_gz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzintegrate_gz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzintegrate_gz00(long
		BgL_checksumz00_2243, char *BgL_fromz00_2244)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzintegrate_gz00))
				{
					BGl_requirezd2initializa7ationz75zzintegrate_gz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzintegrate_gz00();
					BGl_libraryzd2moduleszd2initz00zzintegrate_gz00();
					BGl_importedzd2moduleszd2initz00zzintegrate_gz00();
					return BGl_methodzd2initzd2zzintegrate_gz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzintegrate_gz00(void)
	{
		{	/* Integrate/g.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "integrate_g");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"integrate_g");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "integrate_g");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"integrate_g");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"integrate_g");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "integrate_g");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzintegrate_gz00(void)
	{
		{	/* Integrate/g.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* G! */
	BGL_EXPORTED_DEF obj_t BGl_Gz12z12zzintegrate_gz00(obj_t BgL_gzf2cnzf2_3)
	{
		{	/* Integrate/g.scm 32 */
			{
				bool_t BgL_stopzf3zf3_1569;
				long BgL_stampz00_1570;
				obj_t BgL_gsz00_1571;

				BgL_stopzf3zf3_1569 = ((bool_t) 0);
				BgL_stampz00_1570 = 0L;
				BgL_gsz00_1571 = BgL_gzf2cnzf2_3;
			BgL_zc3z04anonymousza31250ze3z87_1572:
				if (BgL_stopzf3zf3_1569)
					{	/* Integrate/g.scm 37 */
						{
							obj_t BgL_l1243z00_1574;

							BgL_l1243z00_1574 = BGl_za2phiza2z00zzintegrate_az00;
						BgL_zc3z04anonymousza31251ze3z87_1575:
							if (PAIRP(BgL_l1243z00_1574))
								{	/* Integrate/g.scm 41 */
									{	/* Integrate/g.scm 42 */
										obj_t BgL_fz00_1577;

										BgL_fz00_1577 = CAR(BgL_l1243z00_1574);
										{	/* Integrate/g.scm 42 */
											bool_t BgL_test1719z00_2263;

											{	/* Integrate/g.scm 42 */
												obj_t BgL_classz00_1849;

												BgL_classz00_1849 = BGl_localz00zzast_varz00;
												if (BGL_OBJECTP(BgL_fz00_1577))
													{	/* Integrate/g.scm 42 */
														BgL_objectz00_bglt BgL_arg1807z00_1851;

														BgL_arg1807z00_1851 =
															(BgL_objectz00_bglt) (BgL_fz00_1577);
														if (BGL_CONDEXPAND_ISA_ARCH64())
															{	/* Integrate/g.scm 42 */
																long BgL_idxz00_1857;

																BgL_idxz00_1857 =
																	BGL_OBJECT_INHERITANCE_NUM
																	(BgL_arg1807z00_1851);
																BgL_test1719z00_2263 =
																	(VECTOR_REF
																	(BGl_za2inheritancesza2z00zz__objectz00,
																		(BgL_idxz00_1857 + 2L)) ==
																	BgL_classz00_1849);
															}
														else
															{	/* Integrate/g.scm 42 */
																bool_t BgL_res1703z00_1882;

																{	/* Integrate/g.scm 42 */
																	obj_t BgL_oclassz00_1865;

																	{	/* Integrate/g.scm 42 */
																		obj_t BgL_arg1815z00_1873;
																		long BgL_arg1816z00_1874;

																		BgL_arg1815z00_1873 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Integrate/g.scm 42 */
																			long BgL_arg1817z00_1875;

																			BgL_arg1817z00_1875 =
																				BGL_OBJECT_CLASS_NUM
																				(BgL_arg1807z00_1851);
																			BgL_arg1816z00_1874 =
																				(BgL_arg1817z00_1875 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_1865 =
																			VECTOR_REF(BgL_arg1815z00_1873,
																			BgL_arg1816z00_1874);
																	}
																	{	/* Integrate/g.scm 42 */
																		bool_t BgL__ortest_1115z00_1866;

																		BgL__ortest_1115z00_1866 =
																			(BgL_classz00_1849 == BgL_oclassz00_1865);
																		if (BgL__ortest_1115z00_1866)
																			{	/* Integrate/g.scm 42 */
																				BgL_res1703z00_1882 =
																					BgL__ortest_1115z00_1866;
																			}
																		else
																			{	/* Integrate/g.scm 42 */
																				long BgL_odepthz00_1867;

																				{	/* Integrate/g.scm 42 */
																					obj_t BgL_arg1804z00_1868;

																					BgL_arg1804z00_1868 =
																						(BgL_oclassz00_1865);
																					BgL_odepthz00_1867 =
																						BGL_CLASS_DEPTH
																						(BgL_arg1804z00_1868);
																				}
																				if ((2L < BgL_odepthz00_1867))
																					{	/* Integrate/g.scm 42 */
																						obj_t BgL_arg1802z00_1870;

																						{	/* Integrate/g.scm 42 */
																							obj_t BgL_arg1803z00_1871;

																							BgL_arg1803z00_1871 =
																								(BgL_oclassz00_1865);
																							BgL_arg1802z00_1870 =
																								BGL_CLASS_ANCESTORS_REF
																								(BgL_arg1803z00_1871, 2L);
																						}
																						BgL_res1703z00_1882 =
																							(BgL_arg1802z00_1870 ==
																							BgL_classz00_1849);
																					}
																				else
																					{	/* Integrate/g.scm 42 */
																						BgL_res1703z00_1882 = ((bool_t) 0);
																					}
																			}
																	}
																}
																BgL_test1719z00_2263 = BgL_res1703z00_1882;
															}
													}
												else
													{	/* Integrate/g.scm 42 */
														BgL_test1719z00_2263 = ((bool_t) 0);
													}
											}
											if (BgL_test1719z00_2263)
												{	/* Integrate/g.scm 43 */
													BgL_valuez00_bglt BgL_arg1268z00_1579;

													BgL_arg1268z00_1579 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_fz00_1577))))->
														BgL_valuez00);
													{
														BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2289;

														{
															obj_t BgL_auxz00_2290;

															{	/* Integrate/g.scm 43 */
																BgL_objectz00_bglt BgL_tmpz00_2291;

																BgL_tmpz00_2291 =
																	((BgL_objectz00_bglt)
																	((BgL_sfunz00_bglt) BgL_arg1268z00_1579));
																BgL_auxz00_2290 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_2291);
															}
															BgL_auxz00_2289 =
																((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2290);
														}
														((((BgL_sfunzf2iinfozf2_bglt)
																	COBJECT(BgL_auxz00_2289))->BgL_istampz00) =
															((obj_t) BINT(-1L)), BUNSPEC);
													}
												}
											else
												{	/* Integrate/g.scm 42 */
													BFALSE;
												}
										}
									}
									{
										obj_t BgL_l1243z00_2298;

										BgL_l1243z00_2298 = CDR(BgL_l1243z00_1574);
										BgL_l1243z00_1574 = BgL_l1243z00_2298;
										goto BgL_zc3z04anonymousza31251ze3z87_1575;
									}
								}
							else
								{	/* Integrate/g.scm 41 */
									((bool_t) 1);
								}
						}
						BGl_integratezd2remainingzd2localzd2functionsz12zc0zzintegrate_gz00
							();
						return BgL_gsz00_1571;
					}
				else
					{
						obj_t BgL_phiz00_1583;
						bool_t BgL_stopzf3zf3_1584;
						obj_t BgL_gsz00_1585;

						BgL_phiz00_1583 = BGl_za2phiza2z00zzintegrate_az00;
						BgL_stopzf3zf3_1584 = ((bool_t) 1);
						BgL_gsz00_1585 = BgL_gsz00_1571;
					BgL_zc3z04anonymousza31273ze3z87_1586:
						if (NULLP(BgL_phiz00_1583))
							{
								obj_t BgL_gsz00_2306;
								long BgL_stampz00_2304;
								bool_t BgL_stopzf3zf3_2303;

								BgL_stopzf3zf3_2303 = BgL_stopzf3zf3_1584;
								BgL_stampz00_2304 = (BgL_stampz00_1570 + 1L);
								BgL_gsz00_2306 = BgL_gsz00_1585;
								BgL_gsz00_1571 = BgL_gsz00_2306;
								BgL_stampz00_1570 = BgL_stampz00_2304;
								BgL_stopzf3zf3_1569 = BgL_stopzf3zf3_2303;
								goto BgL_zc3z04anonymousza31250ze3z87_1572;
							}
						else
							{	/* Integrate/g.scm 54 */
								obj_t BgL_fz00_1589;

								BgL_fz00_1589 = CAR(((obj_t) BgL_phiz00_1583));
								{	/* Integrate/g.scm 54 */
									BgL_valuez00_bglt BgL_fifz00_1590;

									BgL_fifz00_1590 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_fz00_1589)))->BgL_valuez00);
									{	/* Integrate/g.scm 55 */

										{	/* Integrate/g.scm 56 */
											obj_t BgL_g1109z00_1591;

											{
												BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2311;

												{
													obj_t BgL_auxz00_2312;

													{	/* Integrate/g.scm 56 */
														BgL_objectz00_bglt BgL_tmpz00_2313;

														BgL_tmpz00_2313 =
															((BgL_objectz00_bglt)
															((BgL_sfunz00_bglt) BgL_fifz00_1590));
														BgL_auxz00_2312 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_2313);
													}
													BgL_auxz00_2311 =
														((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2312);
												}
												BgL_g1109z00_1591 =
													(((BgL_sfunzf2iinfozf2_bglt)
														COBJECT(BgL_auxz00_2311))->BgL_ctz00);
											}
											{
												obj_t BgL_ctz00_1593;
												bool_t BgL_stopzf3zf3_1594;
												obj_t BgL_gsz00_1595;

												BgL_ctz00_1593 = BgL_g1109z00_1591;
												BgL_stopzf3zf3_1594 = BgL_stopzf3zf3_1584;
												BgL_gsz00_1595 = BgL_gsz00_1585;
											BgL_zc3z04anonymousza31285ze3z87_1596:
												if (NULLP(BgL_ctz00_1593))
													{	/* Integrate/g.scm 60 */
														obj_t BgL_arg1304z00_1598;

														BgL_arg1304z00_1598 =
															CDR(((obj_t) BgL_phiz00_1583));
														{
															obj_t BgL_gsz00_2325;
															bool_t BgL_stopzf3zf3_2324;
															obj_t BgL_phiz00_2323;

															BgL_phiz00_2323 = BgL_arg1304z00_1598;
															BgL_stopzf3zf3_2324 = BgL_stopzf3zf3_1594;
															BgL_gsz00_2325 = BgL_gsz00_1595;
															BgL_gsz00_1585 = BgL_gsz00_2325;
															BgL_stopzf3zf3_1584 = BgL_stopzf3zf3_2324;
															BgL_phiz00_1583 = BgL_phiz00_2323;
															goto BgL_zc3z04anonymousza31273ze3z87_1586;
														}
													}
												else
													{	/* Integrate/g.scm 61 */
														obj_t BgL_gz00_1599;

														BgL_gz00_1599 = CAR(((obj_t) BgL_ctz00_1593));
														{	/* Integrate/g.scm 61 */
															BgL_valuez00_bglt BgL_gifz00_1600;

															BgL_gifz00_1600 =
																(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_localz00_bglt) BgL_gz00_1599))))->
																BgL_valuez00);
															{	/* Integrate/g.scm 62 */

																if ((BgL_fz00_1589 == BgL_gz00_1599))
																	{	/* Integrate/g.scm 72 */
																		obj_t BgL_arg1305z00_1601;

																		BgL_arg1305z00_1601 =
																			CDR(((obj_t) BgL_ctz00_1593));
																		{
																			obj_t BgL_ctz00_2335;

																			BgL_ctz00_2335 = BgL_arg1305z00_1601;
																			BgL_ctz00_1593 = BgL_ctz00_2335;
																			goto
																				BgL_zc3z04anonymousza31285ze3z87_1596;
																		}
																	}
																else
																	{	/* Integrate/g.scm 73 */
																		bool_t BgL_test1728z00_2336;

																		{
																			BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2337;

																			{
																				obj_t BgL_auxz00_2338;

																				{	/* Integrate/g.scm 73 */
																					BgL_objectz00_bglt BgL_tmpz00_2339;

																					BgL_tmpz00_2339 =
																						((BgL_objectz00_bglt)
																						((BgL_sfunz00_bglt)
																							BgL_gifz00_1600));
																					BgL_auxz00_2338 =
																						BGL_OBJECT_WIDENING
																						(BgL_tmpz00_2339);
																				}
																				BgL_auxz00_2337 =
																					((BgL_sfunzf2iinfozf2_bglt)
																					BgL_auxz00_2338);
																			}
																			BgL_test1728z00_2336 =
																				(((BgL_sfunzf2iinfozf2_bglt)
																					COBJECT(BgL_auxz00_2337))->
																				BgL_gzf3zf3);
																		}
																		if (BgL_test1728z00_2336)
																			{	/* Integrate/g.scm 74 */
																				obj_t BgL_arg1308z00_1603;

																				BgL_arg1308z00_1603 =
																					CDR(((obj_t) BgL_ctz00_1593));
																				{
																					obj_t BgL_ctz00_2347;

																					BgL_ctz00_2347 = BgL_arg1308z00_1603;
																					BgL_ctz00_1593 = BgL_ctz00_2347;
																					goto
																						BgL_zc3z04anonymousza31285ze3z87_1596;
																				}
																			}
																		else
																			{	/* Integrate/g.scm 75 */
																				bool_t BgL_test1729z00_2348;

																				{
																					BgL_sfunzf2iinfozf2_bglt
																						BgL_auxz00_2349;
																					{
																						obj_t BgL_auxz00_2350;

																						{	/* Integrate/g.scm 75 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_2351;
																							BgL_tmpz00_2351 =
																								((BgL_objectz00_bglt) (
																									(BgL_sfunz00_bglt)
																									BgL_fifz00_1590));
																							BgL_auxz00_2350 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_2351);
																						}
																						BgL_auxz00_2349 =
																							((BgL_sfunzf2iinfozf2_bglt)
																							BgL_auxz00_2350);
																					}
																					BgL_test1729z00_2348 =
																						(((BgL_sfunzf2iinfozf2_bglt)
																							COBJECT(BgL_auxz00_2349))->
																						BgL_gzf3zf3);
																				}
																				if (BgL_test1729z00_2348)
																					{	/* Integrate/g.scm 77 */
																						bool_t BgL_test1730z00_2357;

																						{	/* Integrate/g.scm 77 */
																							obj_t BgL_arg1319z00_1614;

																							{
																								BgL_sfunzf2iinfozf2_bglt
																									BgL_auxz00_2358;
																								{
																									obj_t BgL_auxz00_2359;

																									{	/* Integrate/g.scm 77 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_2360;
																										BgL_tmpz00_2360 =
																											((BgL_objectz00_bglt) (
																												(BgL_sfunz00_bglt)
																												BgL_gifz00_1600));
																										BgL_auxz00_2359 =
																											BGL_OBJECT_WIDENING
																											(BgL_tmpz00_2360);
																									}
																									BgL_auxz00_2358 =
																										((BgL_sfunzf2iinfozf2_bglt)
																										BgL_auxz00_2359);
																								}
																								BgL_arg1319z00_1614 =
																									(((BgL_sfunzf2iinfozf2_bglt)
																										COBJECT(BgL_auxz00_2358))->
																									BgL_lz00);
																							}
																							{	/* Integrate/g.scm 77 */
																								obj_t BgL_classz00_1903;

																								BgL_classz00_1903 =
																									BGl_variablez00zzast_varz00;
																								if (BGL_OBJECTP
																									(BgL_arg1319z00_1614))
																									{	/* Integrate/g.scm 77 */
																										BgL_objectz00_bglt
																											BgL_arg1807z00_1905;
																										BgL_arg1807z00_1905 =
																											(BgL_objectz00_bglt)
																											(BgL_arg1319z00_1614);
																										if (BGL_CONDEXPAND_ISA_ARCH64())
																											{	/* Integrate/g.scm 77 */
																												long BgL_idxz00_1911;

																												BgL_idxz00_1911 =
																													BGL_OBJECT_INHERITANCE_NUM
																													(BgL_arg1807z00_1905);
																												BgL_test1730z00_2357 =
																													(VECTOR_REF
																													(BGl_za2inheritancesza2z00zz__objectz00,
																														(BgL_idxz00_1911 +
																															1L)) ==
																													BgL_classz00_1903);
																											}
																										else
																											{	/* Integrate/g.scm 77 */
																												bool_t
																													BgL_res1704z00_1936;
																												{	/* Integrate/g.scm 77 */
																													obj_t
																														BgL_oclassz00_1919;
																													{	/* Integrate/g.scm 77 */
																														obj_t
																															BgL_arg1815z00_1927;
																														long
																															BgL_arg1816z00_1928;
																														BgL_arg1815z00_1927
																															=
																															(BGl_za2classesza2z00zz__objectz00);
																														{	/* Integrate/g.scm 77 */
																															long
																																BgL_arg1817z00_1929;
																															BgL_arg1817z00_1929
																																=
																																BGL_OBJECT_CLASS_NUM
																																(BgL_arg1807z00_1905);
																															BgL_arg1816z00_1928
																																=
																																(BgL_arg1817z00_1929
																																- OBJECT_TYPE);
																														}
																														BgL_oclassz00_1919 =
																															VECTOR_REF
																															(BgL_arg1815z00_1927,
																															BgL_arg1816z00_1928);
																													}
																													{	/* Integrate/g.scm 77 */
																														bool_t
																															BgL__ortest_1115z00_1920;
																														BgL__ortest_1115z00_1920
																															=
																															(BgL_classz00_1903
																															==
																															BgL_oclassz00_1919);
																														if (BgL__ortest_1115z00_1920)
																															{	/* Integrate/g.scm 77 */
																																BgL_res1704z00_1936
																																	=
																																	BgL__ortest_1115z00_1920;
																															}
																														else
																															{	/* Integrate/g.scm 77 */
																																long
																																	BgL_odepthz00_1921;
																																{	/* Integrate/g.scm 77 */
																																	obj_t
																																		BgL_arg1804z00_1922;
																																	BgL_arg1804z00_1922
																																		=
																																		(BgL_oclassz00_1919);
																																	BgL_odepthz00_1921
																																		=
																																		BGL_CLASS_DEPTH
																																		(BgL_arg1804z00_1922);
																																}
																																if (
																																	(1L <
																																		BgL_odepthz00_1921))
																																	{	/* Integrate/g.scm 77 */
																																		obj_t
																																			BgL_arg1802z00_1924;
																																		{	/* Integrate/g.scm 77 */
																																			obj_t
																																				BgL_arg1803z00_1925;
																																			BgL_arg1803z00_1925
																																				=
																																				(BgL_oclassz00_1919);
																																			BgL_arg1802z00_1924
																																				=
																																				BGL_CLASS_ANCESTORS_REF
																																				(BgL_arg1803z00_1925,
																																				1L);
																																		}
																																		BgL_res1704z00_1936
																																			=
																																			(BgL_arg1802z00_1924
																																			==
																																			BgL_classz00_1903);
																																	}
																																else
																																	{	/* Integrate/g.scm 77 */
																																		BgL_res1704z00_1936
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																													}
																												}
																												BgL_test1730z00_2357 =
																													BgL_res1704z00_1936;
																											}
																									}
																								else
																									{	/* Integrate/g.scm 77 */
																										BgL_test1730z00_2357 =
																											((bool_t) 0);
																									}
																							}
																						}
																						if (BgL_test1730z00_2357)
																							{	/* Integrate/g.scm 84 */
																								bool_t BgL_test1737z00_2388;

																								{	/* Integrate/g.scm 84 */
																									obj_t BgL_arg1317z00_1612;

																									{
																										BgL_sfunzf2iinfozf2_bglt
																											BgL_auxz00_2389;
																										{
																											obj_t BgL_auxz00_2390;

																											{	/* Integrate/g.scm 84 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_2391;
																												BgL_tmpz00_2391 =
																													((BgL_objectz00_bglt)
																													((BgL_sfunz00_bglt)
																														BgL_gifz00_1600));
																												BgL_auxz00_2390 =
																													BGL_OBJECT_WIDENING
																													(BgL_tmpz00_2391);
																											}
																											BgL_auxz00_2389 =
																												(
																												(BgL_sfunzf2iinfozf2_bglt)
																												BgL_auxz00_2390);
																										}
																										BgL_arg1317z00_1612 =
																											(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2389))->BgL_lz00);
																									}
																									BgL_test1737z00_2388 =
																										(BgL_arg1317z00_1612 ==
																										BgL_fz00_1589);
																								}
																								if (BgL_test1737z00_2388)
																									{	/* Integrate/g.scm 85 */
																										obj_t BgL_arg1314z00_1609;

																										BgL_arg1314z00_1609 =
																											CDR(
																											((obj_t) BgL_ctz00_1593));
																										{
																											obj_t BgL_ctz00_2400;

																											BgL_ctz00_2400 =
																												BgL_arg1314z00_1609;
																											BgL_ctz00_1593 =
																												BgL_ctz00_2400;
																											goto
																												BgL_zc3z04anonymousza31285ze3z87_1596;
																										}
																									}
																								else
																									{	/* Integrate/g.scm 84 */
																										{
																											BgL_sfunzf2iinfozf2_bglt
																												BgL_auxz00_2401;
																											{
																												obj_t BgL_auxz00_2402;

																												{	/* Integrate/g.scm 87 */
																													BgL_objectz00_bglt
																														BgL_tmpz00_2403;
																													BgL_tmpz00_2403 =
																														(
																														(BgL_objectz00_bglt)
																														((BgL_sfunz00_bglt)
																															BgL_gifz00_1600));
																													BgL_auxz00_2402 =
																														BGL_OBJECT_WIDENING
																														(BgL_tmpz00_2403);
																												}
																												BgL_auxz00_2401 =
																													(
																													(BgL_sfunzf2iinfozf2_bglt)
																													BgL_auxz00_2402);
																											}
																											((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2401))->BgL_gzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
																										}
																										{	/* Integrate/g.scm 91 */
																											obj_t BgL_arg1315z00_1610;
																											obj_t BgL_arg1316z00_1611;

																											BgL_arg1315z00_1610 =
																												CDR(
																												((obj_t)
																													BgL_ctz00_1593));
																											BgL_arg1316z00_1611 =
																												MAKE_YOUNG_PAIR
																												(BgL_gz00_1599,
																												BgL_gsz00_1595);
																											{
																												obj_t BgL_gsz00_2414;
																												bool_t
																													BgL_stopzf3zf3_2413;
																												obj_t BgL_ctz00_2412;

																												BgL_ctz00_2412 =
																													BgL_arg1315z00_1610;
																												BgL_stopzf3zf3_2413 =
																													((bool_t) 0);
																												BgL_gsz00_2414 =
																													BgL_arg1316z00_1611;
																												BgL_gsz00_1595 =
																													BgL_gsz00_2414;
																												BgL_stopzf3zf3_1594 =
																													BgL_stopzf3zf3_2413;
																												BgL_ctz00_1593 =
																													BgL_ctz00_2412;
																												goto
																													BgL_zc3z04anonymousza31285ze3z87_1596;
																											}
																										}
																									}
																							}
																						else
																							{	/* Integrate/g.scm 77 */
																								{
																									BgL_sfunzf2iinfozf2_bglt
																										BgL_auxz00_2415;
																									{
																										obj_t BgL_auxz00_2416;

																										{	/* Integrate/g.scm 82 */
																											BgL_objectz00_bglt
																												BgL_tmpz00_2417;
																											BgL_tmpz00_2417 =
																												((BgL_objectz00_bglt) (
																													(BgL_sfunz00_bglt)
																													BgL_gifz00_1600));
																											BgL_auxz00_2416 =
																												BGL_OBJECT_WIDENING
																												(BgL_tmpz00_2417);
																										}
																										BgL_auxz00_2415 =
																											(
																											(BgL_sfunzf2iinfozf2_bglt)
																											BgL_auxz00_2416);
																									}
																									((((BgL_sfunzf2iinfozf2_bglt)
																												COBJECT
																												(BgL_auxz00_2415))->
																											BgL_lz00) =
																										((obj_t) BgL_fz00_1589),
																										BUNSPEC);
																								}
																								{	/* Integrate/g.scm 83 */
																									obj_t BgL_arg1318z00_1613;

																									BgL_arg1318z00_1613 =
																										CDR(
																										((obj_t) BgL_ctz00_1593));
																									{
																										bool_t BgL_stopzf3zf3_2426;
																										obj_t BgL_ctz00_2425;

																										BgL_ctz00_2425 =
																											BgL_arg1318z00_1613;
																										BgL_stopzf3zf3_2426 =
																											((bool_t) 0);
																										BgL_stopzf3zf3_1594 =
																											BgL_stopzf3zf3_2426;
																										BgL_ctz00_1593 =
																											BgL_ctz00_2425;
																										goto
																											BgL_zc3z04anonymousza31285ze3z87_1596;
																									}
																								}
																							}
																					}
																				else
																					{	/* Integrate/g.scm 92 */
																						bool_t BgL_test1738z00_2427;

																						{	/* Integrate/g.scm 92 */
																							obj_t BgL_arg1364z00_1641;

																							{
																								BgL_sfunzf2iinfozf2_bglt
																									BgL_auxz00_2428;
																								{
																									obj_t BgL_auxz00_2429;

																									{	/* Integrate/g.scm 92 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_2430;
																										BgL_tmpz00_2430 =
																											((BgL_objectz00_bglt) (
																												(BgL_sfunz00_bglt)
																												BgL_gifz00_1600));
																										BgL_auxz00_2429 =
																											BGL_OBJECT_WIDENING
																											(BgL_tmpz00_2430);
																									}
																									BgL_auxz00_2428 =
																										((BgL_sfunzf2iinfozf2_bglt)
																										BgL_auxz00_2429);
																								}
																								BgL_arg1364z00_1641 =
																									(((BgL_sfunzf2iinfozf2_bglt)
																										COBJECT(BgL_auxz00_2428))->
																									BgL_lz00);
																							}
																							{	/* Integrate/g.scm 92 */
																								obj_t BgL_classz00_1949;

																								BgL_classz00_1949 =
																									BGl_variablez00zzast_varz00;
																								if (BGL_OBJECTP
																									(BgL_arg1364z00_1641))
																									{	/* Integrate/g.scm 92 */
																										BgL_objectz00_bglt
																											BgL_arg1807z00_1951;
																										BgL_arg1807z00_1951 =
																											(BgL_objectz00_bglt)
																											(BgL_arg1364z00_1641);
																										if (BGL_CONDEXPAND_ISA_ARCH64())
																											{	/* Integrate/g.scm 92 */
																												long BgL_idxz00_1957;

																												BgL_idxz00_1957 =
																													BGL_OBJECT_INHERITANCE_NUM
																													(BgL_arg1807z00_1951);
																												BgL_test1738z00_2427 =
																													(VECTOR_REF
																													(BGl_za2inheritancesza2z00zz__objectz00,
																														(BgL_idxz00_1957 +
																															1L)) ==
																													BgL_classz00_1949);
																											}
																										else
																											{	/* Integrate/g.scm 92 */
																												bool_t
																													BgL_res1705z00_1982;
																												{	/* Integrate/g.scm 92 */
																													obj_t
																														BgL_oclassz00_1965;
																													{	/* Integrate/g.scm 92 */
																														obj_t
																															BgL_arg1815z00_1973;
																														long
																															BgL_arg1816z00_1974;
																														BgL_arg1815z00_1973
																															=
																															(BGl_za2classesza2z00zz__objectz00);
																														{	/* Integrate/g.scm 92 */
																															long
																																BgL_arg1817z00_1975;
																															BgL_arg1817z00_1975
																																=
																																BGL_OBJECT_CLASS_NUM
																																(BgL_arg1807z00_1951);
																															BgL_arg1816z00_1974
																																=
																																(BgL_arg1817z00_1975
																																- OBJECT_TYPE);
																														}
																														BgL_oclassz00_1965 =
																															VECTOR_REF
																															(BgL_arg1815z00_1973,
																															BgL_arg1816z00_1974);
																													}
																													{	/* Integrate/g.scm 92 */
																														bool_t
																															BgL__ortest_1115z00_1966;
																														BgL__ortest_1115z00_1966
																															=
																															(BgL_classz00_1949
																															==
																															BgL_oclassz00_1965);
																														if (BgL__ortest_1115z00_1966)
																															{	/* Integrate/g.scm 92 */
																																BgL_res1705z00_1982
																																	=
																																	BgL__ortest_1115z00_1966;
																															}
																														else
																															{	/* Integrate/g.scm 92 */
																																long
																																	BgL_odepthz00_1967;
																																{	/* Integrate/g.scm 92 */
																																	obj_t
																																		BgL_arg1804z00_1968;
																																	BgL_arg1804z00_1968
																																		=
																																		(BgL_oclassz00_1965);
																																	BgL_odepthz00_1967
																																		=
																																		BGL_CLASS_DEPTH
																																		(BgL_arg1804z00_1968);
																																}
																																if (
																																	(1L <
																																		BgL_odepthz00_1967))
																																	{	/* Integrate/g.scm 92 */
																																		obj_t
																																			BgL_arg1802z00_1970;
																																		{	/* Integrate/g.scm 92 */
																																			obj_t
																																				BgL_arg1803z00_1971;
																																			BgL_arg1803z00_1971
																																				=
																																				(BgL_oclassz00_1965);
																																			BgL_arg1802z00_1970
																																				=
																																				BGL_CLASS_ANCESTORS_REF
																																				(BgL_arg1803z00_1971,
																																				1L);
																																		}
																																		BgL_res1705z00_1982
																																			=
																																			(BgL_arg1802z00_1970
																																			==
																																			BgL_classz00_1949);
																																	}
																																else
																																	{	/* Integrate/g.scm 92 */
																																		BgL_res1705z00_1982
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																													}
																												}
																												BgL_test1738z00_2427 =
																													BgL_res1705z00_1982;
																											}
																									}
																								else
																									{	/* Integrate/g.scm 92 */
																										BgL_test1738z00_2427 =
																											((bool_t) 0);
																									}
																							}
																						}
																						if (BgL_test1738z00_2427)
																							{	/* Integrate/g.scm 116 */
																								bool_t BgL_test1744z00_2458;

																								{	/* Integrate/g.scm 116 */
																									obj_t BgL_arg1340z00_1629;

																									{
																										BgL_sfunzf2iinfozf2_bglt
																											BgL_auxz00_2459;
																										{
																											obj_t BgL_auxz00_2460;

																											{	/* Integrate/g.scm 116 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_2461;
																												BgL_tmpz00_2461 =
																													((BgL_objectz00_bglt)
																													((BgL_sfunz00_bglt)
																														BgL_fifz00_1590));
																												BgL_auxz00_2460 =
																													BGL_OBJECT_WIDENING
																													(BgL_tmpz00_2461);
																											}
																											BgL_auxz00_2459 =
																												(
																												(BgL_sfunzf2iinfozf2_bglt)
																												BgL_auxz00_2460);
																										}
																										BgL_arg1340z00_1629 =
																											(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2459))->BgL_lz00);
																									}
																									{	/* Integrate/g.scm 116 */
																										obj_t BgL_classz00_1985;

																										BgL_classz00_1985 =
																											BGl_variablez00zzast_varz00;
																										if (BGL_OBJECTP
																											(BgL_arg1340z00_1629))
																											{	/* Integrate/g.scm 116 */
																												BgL_objectz00_bglt
																													BgL_arg1807z00_1987;
																												BgL_arg1807z00_1987 =
																													(BgL_objectz00_bglt)
																													(BgL_arg1340z00_1629);
																												if (BGL_CONDEXPAND_ISA_ARCH64())
																													{	/* Integrate/g.scm 116 */
																														long
																															BgL_idxz00_1993;
																														BgL_idxz00_1993 =
																															BGL_OBJECT_INHERITANCE_NUM
																															(BgL_arg1807z00_1987);
																														BgL_test1744z00_2458
																															=
																															(VECTOR_REF
																															(BGl_za2inheritancesza2z00zz__objectz00,
																																(BgL_idxz00_1993
																																	+ 1L)) ==
																															BgL_classz00_1985);
																													}
																												else
																													{	/* Integrate/g.scm 116 */
																														bool_t
																															BgL_res1706z00_2018;
																														{	/* Integrate/g.scm 116 */
																															obj_t
																																BgL_oclassz00_2001;
																															{	/* Integrate/g.scm 116 */
																																obj_t
																																	BgL_arg1815z00_2009;
																																long
																																	BgL_arg1816z00_2010;
																																BgL_arg1815z00_2009
																																	=
																																	(BGl_za2classesza2z00zz__objectz00);
																																{	/* Integrate/g.scm 116 */
																																	long
																																		BgL_arg1817z00_2011;
																																	BgL_arg1817z00_2011
																																		=
																																		BGL_OBJECT_CLASS_NUM
																																		(BgL_arg1807z00_1987);
																																	BgL_arg1816z00_2010
																																		=
																																		(BgL_arg1817z00_2011
																																		-
																																		OBJECT_TYPE);
																																}
																																BgL_oclassz00_2001
																																	=
																																	VECTOR_REF
																																	(BgL_arg1815z00_2009,
																																	BgL_arg1816z00_2010);
																															}
																															{	/* Integrate/g.scm 116 */
																																bool_t
																																	BgL__ortest_1115z00_2002;
																																BgL__ortest_1115z00_2002
																																	=
																																	(BgL_classz00_1985
																																	==
																																	BgL_oclassz00_2001);
																																if (BgL__ortest_1115z00_2002)
																																	{	/* Integrate/g.scm 116 */
																																		BgL_res1706z00_2018
																																			=
																																			BgL__ortest_1115z00_2002;
																																	}
																																else
																																	{	/* Integrate/g.scm 116 */
																																		long
																																			BgL_odepthz00_2003;
																																		{	/* Integrate/g.scm 116 */
																																			obj_t
																																				BgL_arg1804z00_2004;
																																			BgL_arg1804z00_2004
																																				=
																																				(BgL_oclassz00_2001);
																																			BgL_odepthz00_2003
																																				=
																																				BGL_CLASS_DEPTH
																																				(BgL_arg1804z00_2004);
																																		}
																																		if (
																																			(1L <
																																				BgL_odepthz00_2003))
																																			{	/* Integrate/g.scm 116 */
																																				obj_t
																																					BgL_arg1802z00_2006;
																																				{	/* Integrate/g.scm 116 */
																																					obj_t
																																						BgL_arg1803z00_2007;
																																					BgL_arg1803z00_2007
																																						=
																																						(BgL_oclassz00_2001);
																																					BgL_arg1802z00_2006
																																						=
																																						BGL_CLASS_ANCESTORS_REF
																																						(BgL_arg1803z00_2007,
																																						1L);
																																				}
																																				BgL_res1706z00_2018
																																					=
																																					(BgL_arg1802z00_2006
																																					==
																																					BgL_classz00_1985);
																																			}
																																		else
																																			{	/* Integrate/g.scm 116 */
																																				BgL_res1706z00_2018
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																															}
																														}
																														BgL_test1744z00_2458
																															=
																															BgL_res1706z00_2018;
																													}
																											}
																										else
																											{	/* Integrate/g.scm 116 */
																												BgL_test1744z00_2458 =
																													((bool_t) 0);
																											}
																									}
																								}
																								if (BgL_test1744z00_2458)
																									{	/* Integrate/g.scm 124 */
																										bool_t BgL_test1751z00_2489;

																										{	/* Integrate/g.scm 124 */
																											obj_t BgL_arg1332z00_1625;
																											obj_t BgL_arg1333z00_1626;

																											{
																												BgL_sfunzf2iinfozf2_bglt
																													BgL_auxz00_2490;
																												{
																													obj_t BgL_auxz00_2491;

																													{	/* Integrate/g.scm 124 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_2492;
																														BgL_tmpz00_2492 =
																															(
																															(BgL_objectz00_bglt)
																															((BgL_sfunz00_bglt) BgL_fifz00_1590));
																														BgL_auxz00_2491 =
																															BGL_OBJECT_WIDENING
																															(BgL_tmpz00_2492);
																													}
																													BgL_auxz00_2490 =
																														(
																														(BgL_sfunzf2iinfozf2_bglt)
																														BgL_auxz00_2491);
																												}
																												BgL_arg1332z00_1625 =
																													(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2490))->BgL_lz00);
																											}
																											{
																												BgL_sfunzf2iinfozf2_bglt
																													BgL_auxz00_2498;
																												{
																													obj_t BgL_auxz00_2499;

																													{	/* Integrate/g.scm 124 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_2500;
																														BgL_tmpz00_2500 =
																															(
																															(BgL_objectz00_bglt)
																															((BgL_sfunz00_bglt) BgL_gifz00_1600));
																														BgL_auxz00_2499 =
																															BGL_OBJECT_WIDENING
																															(BgL_tmpz00_2500);
																													}
																													BgL_auxz00_2498 =
																														(
																														(BgL_sfunzf2iinfozf2_bglt)
																														BgL_auxz00_2499);
																												}
																												BgL_arg1333z00_1626 =
																													(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2498))->BgL_lz00);
																											}
																											BgL_test1751z00_2489 =
																												(BgL_arg1332z00_1625 ==
																												BgL_arg1333z00_1626);
																										}
																										if (BgL_test1751z00_2489)
																											{	/* Integrate/g.scm 124 */
																												{	/* Integrate/g.scm 128 */
																													obj_t
																														BgL_arg1328z00_1622;
																													BgL_arg1328z00_1622 =
																														CDR(((obj_t)
																															BgL_ctz00_1593));
																													{
																														obj_t
																															BgL_ctz00_2509;
																														BgL_ctz00_2509 =
																															BgL_arg1328z00_1622;
																														BgL_ctz00_1593 =
																															BgL_ctz00_2509;
																														goto
																															BgL_zc3z04anonymousza31285ze3z87_1596;
																													}
																												}
																											}
																										else
																											{	/* Integrate/g.scm 124 */
																												{
																													BgL_sfunzf2iinfozf2_bglt
																														BgL_auxz00_2510;
																													{
																														obj_t
																															BgL_auxz00_2511;
																														{	/* Integrate/g.scm 130 */
																															BgL_objectz00_bglt
																																BgL_tmpz00_2512;
																															BgL_tmpz00_2512 =
																																(
																																(BgL_objectz00_bglt)
																																((BgL_sfunz00_bglt) BgL_gifz00_1600));
																															BgL_auxz00_2511 =
																																BGL_OBJECT_WIDENING
																																(BgL_tmpz00_2512);
																														}
																														BgL_auxz00_2510 =
																															(
																															(BgL_sfunzf2iinfozf2_bglt)
																															BgL_auxz00_2511);
																													}
																													((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2510))->BgL_gzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
																												}
																												{	/* Integrate/g.scm 133 */
																													obj_t
																														BgL_arg1329z00_1623;
																													obj_t
																														BgL_arg1331z00_1624;
																													BgL_arg1329z00_1623 =
																														CDR(((obj_t)
																															BgL_ctz00_1593));
																													BgL_arg1331z00_1624 =
																														MAKE_YOUNG_PAIR
																														(BgL_gz00_1599,
																														BgL_gsz00_1595);
																													{
																														obj_t
																															BgL_gsz00_2523;
																														bool_t
																															BgL_stopzf3zf3_2522;
																														obj_t
																															BgL_ctz00_2521;
																														BgL_ctz00_2521 =
																															BgL_arg1329z00_1623;
																														BgL_stopzf3zf3_2522
																															= ((bool_t) 0);
																														BgL_gsz00_2523 =
																															BgL_arg1331z00_1624;
																														BgL_gsz00_1595 =
																															BgL_gsz00_2523;
																														BgL_stopzf3zf3_1594
																															=
																															BgL_stopzf3zf3_2522;
																														BgL_ctz00_1593 =
																															BgL_ctz00_2521;
																														goto
																															BgL_zc3z04anonymousza31285ze3z87_1596;
																													}
																												}
																											}
																									}
																								else
																									{	/* Integrate/g.scm 116 */
																										{	/* Integrate/g.scm 122 */
																											obj_t BgL_arg1335z00_1627;

																											{
																												BgL_sfunzf2iinfozf2_bglt
																													BgL_auxz00_2524;
																												{
																													obj_t BgL_auxz00_2525;

																													{	/* Integrate/g.scm 122 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_2526;
																														BgL_tmpz00_2526 =
																															(
																															(BgL_objectz00_bglt)
																															((BgL_sfunz00_bglt) BgL_gifz00_1600));
																														BgL_auxz00_2525 =
																															BGL_OBJECT_WIDENING
																															(BgL_tmpz00_2526);
																													}
																													BgL_auxz00_2524 =
																														(
																														(BgL_sfunzf2iinfozf2_bglt)
																														BgL_auxz00_2525);
																												}
																												BgL_arg1335z00_1627 =
																													(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2524))->BgL_lz00);
																											}
																											{
																												BgL_sfunzf2iinfozf2_bglt
																													BgL_auxz00_2532;
																												{
																													obj_t BgL_auxz00_2533;

																													{	/* Integrate/g.scm 122 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_2534;
																														BgL_tmpz00_2534 =
																															(
																															(BgL_objectz00_bglt)
																															((BgL_sfunz00_bglt) BgL_fifz00_1590));
																														BgL_auxz00_2533 =
																															BGL_OBJECT_WIDENING
																															(BgL_tmpz00_2534);
																													}
																													BgL_auxz00_2532 =
																														(
																														(BgL_sfunzf2iinfozf2_bglt)
																														BgL_auxz00_2533);
																												}
																												((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2532))->BgL_lz00) = ((obj_t) BgL_arg1335z00_1627), BUNSPEC);
																											}
																										}
																										{	/* Integrate/g.scm 123 */
																											obj_t BgL_arg1339z00_1628;

																											BgL_arg1339z00_1628 =
																												CDR(
																												((obj_t)
																													BgL_ctz00_1593));
																											{
																												bool_t
																													BgL_stopzf3zf3_2543;
																												obj_t BgL_ctz00_2542;

																												BgL_ctz00_2542 =
																													BgL_arg1339z00_1628;
																												BgL_stopzf3zf3_2543 =
																													((bool_t) 0);
																												BgL_stopzf3zf3_1594 =
																													BgL_stopzf3zf3_2543;
																												BgL_ctz00_1593 =
																													BgL_ctz00_2542;
																												goto
																													BgL_zc3z04anonymousza31285ze3z87_1596;
																											}
																										}
																									}
																							}
																						else
																							{	/* Integrate/g.scm 94 */
																								bool_t BgL_test1752z00_2544;

																								{	/* Integrate/g.scm 94 */
																									obj_t BgL_arg1361z00_1640;

																									{
																										BgL_sfunzf2iinfozf2_bglt
																											BgL_auxz00_2545;
																										{
																											obj_t BgL_auxz00_2546;

																											{	/* Integrate/g.scm 94 */
																												BgL_objectz00_bglt
																													BgL_tmpz00_2547;
																												BgL_tmpz00_2547 =
																													((BgL_objectz00_bglt)
																													((BgL_sfunz00_bglt)
																														BgL_fifz00_1590));
																												BgL_auxz00_2546 =
																													BGL_OBJECT_WIDENING
																													(BgL_tmpz00_2547);
																											}
																											BgL_auxz00_2545 =
																												(
																												(BgL_sfunzf2iinfozf2_bglt)
																												BgL_auxz00_2546);
																										}
																										BgL_arg1361z00_1640 =
																											(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2545))->BgL_lz00);
																									}
																									{	/* Integrate/g.scm 94 */
																										obj_t BgL_classz00_2035;

																										BgL_classz00_2035 =
																											BGl_variablez00zzast_varz00;
																										if (BGL_OBJECTP
																											(BgL_arg1361z00_1640))
																											{	/* Integrate/g.scm 94 */
																												BgL_objectz00_bglt
																													BgL_arg1807z00_2037;
																												BgL_arg1807z00_2037 =
																													(BgL_objectz00_bglt)
																													(BgL_arg1361z00_1640);
																												if (BGL_CONDEXPAND_ISA_ARCH64())
																													{	/* Integrate/g.scm 94 */
																														long
																															BgL_idxz00_2043;
																														BgL_idxz00_2043 =
																															BGL_OBJECT_INHERITANCE_NUM
																															(BgL_arg1807z00_2037);
																														BgL_test1752z00_2544
																															=
																															(VECTOR_REF
																															(BGl_za2inheritancesza2z00zz__objectz00,
																																(BgL_idxz00_2043
																																	+ 1L)) ==
																															BgL_classz00_2035);
																													}
																												else
																													{	/* Integrate/g.scm 94 */
																														bool_t
																															BgL_res1707z00_2068;
																														{	/* Integrate/g.scm 94 */
																															obj_t
																																BgL_oclassz00_2051;
																															{	/* Integrate/g.scm 94 */
																																obj_t
																																	BgL_arg1815z00_2059;
																																long
																																	BgL_arg1816z00_2060;
																																BgL_arg1815z00_2059
																																	=
																																	(BGl_za2classesza2z00zz__objectz00);
																																{	/* Integrate/g.scm 94 */
																																	long
																																		BgL_arg1817z00_2061;
																																	BgL_arg1817z00_2061
																																		=
																																		BGL_OBJECT_CLASS_NUM
																																		(BgL_arg1807z00_2037);
																																	BgL_arg1816z00_2060
																																		=
																																		(BgL_arg1817z00_2061
																																		-
																																		OBJECT_TYPE);
																																}
																																BgL_oclassz00_2051
																																	=
																																	VECTOR_REF
																																	(BgL_arg1815z00_2059,
																																	BgL_arg1816z00_2060);
																															}
																															{	/* Integrate/g.scm 94 */
																																bool_t
																																	BgL__ortest_1115z00_2052;
																																BgL__ortest_1115z00_2052
																																	=
																																	(BgL_classz00_2035
																																	==
																																	BgL_oclassz00_2051);
																																if (BgL__ortest_1115z00_2052)
																																	{	/* Integrate/g.scm 94 */
																																		BgL_res1707z00_2068
																																			=
																																			BgL__ortest_1115z00_2052;
																																	}
																																else
																																	{	/* Integrate/g.scm 94 */
																																		long
																																			BgL_odepthz00_2053;
																																		{	/* Integrate/g.scm 94 */
																																			obj_t
																																				BgL_arg1804z00_2054;
																																			BgL_arg1804z00_2054
																																				=
																																				(BgL_oclassz00_2051);
																																			BgL_odepthz00_2053
																																				=
																																				BGL_CLASS_DEPTH
																																				(BgL_arg1804z00_2054);
																																		}
																																		if (
																																			(1L <
																																				BgL_odepthz00_2053))
																																			{	/* Integrate/g.scm 94 */
																																				obj_t
																																					BgL_arg1802z00_2056;
																																				{	/* Integrate/g.scm 94 */
																																					obj_t
																																						BgL_arg1803z00_2057;
																																					BgL_arg1803z00_2057
																																						=
																																						(BgL_oclassz00_2051);
																																					BgL_arg1802z00_2056
																																						=
																																						BGL_CLASS_ANCESTORS_REF
																																						(BgL_arg1803z00_2057,
																																						1L);
																																				}
																																				BgL_res1707z00_2068
																																					=
																																					(BgL_arg1802z00_2056
																																					==
																																					BgL_classz00_2035);
																																			}
																																		else
																																			{	/* Integrate/g.scm 94 */
																																				BgL_res1707z00_2068
																																					=
																																					(
																																					(bool_t)
																																					0);
																																			}
																																	}
																															}
																														}
																														BgL_test1752z00_2544
																															=
																															BgL_res1707z00_2068;
																													}
																											}
																										else
																											{	/* Integrate/g.scm 94 */
																												BgL_test1752z00_2544 =
																													((bool_t) 0);
																											}
																									}
																								}
																								if (BgL_test1752z00_2544)
																									{	/* Integrate/g.scm 94 */
																										{	/* Integrate/g.scm 95 */
																											obj_t BgL_arg1346z00_1632;

																											{
																												BgL_sfunzf2iinfozf2_bglt
																													BgL_auxz00_2575;
																												{
																													obj_t BgL_auxz00_2576;

																													{	/* Integrate/g.scm 95 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_2577;
																														BgL_tmpz00_2577 =
																															(
																															(BgL_objectz00_bglt)
																															((BgL_sfunz00_bglt) BgL_fifz00_1590));
																														BgL_auxz00_2576 =
																															BGL_OBJECT_WIDENING
																															(BgL_tmpz00_2577);
																													}
																													BgL_auxz00_2575 =
																														(
																														(BgL_sfunzf2iinfozf2_bglt)
																														BgL_auxz00_2576);
																												}
																												BgL_arg1346z00_1632 =
																													(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2575))->BgL_lz00);
																											}
																											{
																												BgL_sfunzf2iinfozf2_bglt
																													BgL_auxz00_2583;
																												{
																													obj_t BgL_auxz00_2584;

																													{	/* Integrate/g.scm 95 */
																														BgL_objectz00_bglt
																															BgL_tmpz00_2585;
																														BgL_tmpz00_2585 =
																															(
																															(BgL_objectz00_bglt)
																															((BgL_sfunz00_bglt) BgL_gifz00_1600));
																														BgL_auxz00_2584 =
																															BGL_OBJECT_WIDENING
																															(BgL_tmpz00_2585);
																													}
																													BgL_auxz00_2583 =
																														(
																														(BgL_sfunzf2iinfozf2_bglt)
																														BgL_auxz00_2584);
																												}
																												((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2583))->BgL_lz00) = ((obj_t) BgL_arg1346z00_1632), BUNSPEC);
																											}
																										}
																										{	/* Integrate/g.scm 101 */
																											obj_t BgL_arg1348z00_1633;

																											BgL_arg1348z00_1633 =
																												CDR(
																												((obj_t)
																													BgL_ctz00_1593));
																											{
																												bool_t
																													BgL_stopzf3zf3_2594;
																												obj_t BgL_ctz00_2593;

																												BgL_ctz00_2593 =
																													BgL_arg1348z00_1633;
																												BgL_stopzf3zf3_2594 =
																													((bool_t) 0);
																												BgL_stopzf3zf3_1594 =
																													BgL_stopzf3zf3_2594;
																												BgL_ctz00_1593 =
																													BgL_ctz00_2593;
																												goto
																													BgL_zc3z04anonymousza31285ze3z87_1596;
																											}
																										}
																									}
																								else
																									{	/* Integrate/g.scm 103 */
																										bool_t BgL_stopzf3zf3_1634;

																										if (BgL_stopzf3zf3_1594)
																											{	/* Integrate/g.scm 105 */
																												bool_t
																													BgL_test1759z00_2596;
																												{	/* Integrate/g.scm 105 */
																													obj_t
																														BgL_arg1352z00_1639;
																													{
																														BgL_sfunzf2iinfozf2_bglt
																															BgL_auxz00_2597;
																														{
																															obj_t
																																BgL_auxz00_2598;
																															{	/* Integrate/g.scm 105 */
																																BgL_objectz00_bglt
																																	BgL_tmpz00_2599;
																																BgL_tmpz00_2599
																																	=
																																	(
																																	(BgL_objectz00_bglt)
																																	((BgL_sfunz00_bglt) BgL_fifz00_1590));
																																BgL_auxz00_2598
																																	=
																																	BGL_OBJECT_WIDENING
																																	(BgL_tmpz00_2599);
																															}
																															BgL_auxz00_2597 =
																																(
																																(BgL_sfunzf2iinfozf2_bglt)
																																BgL_auxz00_2598);
																														}
																														BgL_arg1352z00_1639
																															=
																															(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2597))->BgL_istampz00);
																													}
																													BgL_test1759z00_2596 =
																														INTEGERP
																														(BgL_arg1352z00_1639);
																												}
																												if (BgL_test1759z00_2596)
																													{	/* Integrate/g.scm 107 */
																														obj_t
																															BgL_arg1351z00_1638;
																														{
																															BgL_sfunzf2iinfozf2_bglt
																																BgL_auxz00_2606;
																															{
																																obj_t
																																	BgL_auxz00_2607;
																																{	/* Integrate/g.scm 107 */
																																	BgL_objectz00_bglt
																																		BgL_tmpz00_2608;
																																	BgL_tmpz00_2608
																																		=
																																		(
																																		(BgL_objectz00_bglt)
																																		((BgL_sfunz00_bglt) BgL_fifz00_1590));
																																	BgL_auxz00_2607
																																		=
																																		BGL_OBJECT_WIDENING
																																		(BgL_tmpz00_2608);
																																}
																																BgL_auxz00_2606
																																	=
																																	(
																																	(BgL_sfunzf2iinfozf2_bglt)
																																	BgL_auxz00_2607);
																															}
																															BgL_arg1351z00_1638
																																=
																																(((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2606))->BgL_istampz00);
																														}
																														BgL_stopzf3zf3_1634
																															=
																															((long)
																															CINT
																															(BgL_arg1351z00_1638)
																															<=
																															BgL_stampz00_1570);
																													}
																												else
																													{	/* Integrate/g.scm 105 */
																														BgL_stopzf3zf3_1634
																															= ((bool_t) 0);
																													}
																											}
																										else
																											{	/* Integrate/g.scm 103 */
																												BgL_stopzf3zf3_1634 =
																													((bool_t) 0);
																											}
																										{
																											BgL_sfunzf2iinfozf2_bglt
																												BgL_auxz00_2616;
																											{
																												obj_t BgL_auxz00_2617;

																												{	/* Integrate/g.scm 114 */
																													BgL_objectz00_bglt
																														BgL_tmpz00_2618;
																													BgL_tmpz00_2618 =
																														(
																														(BgL_objectz00_bglt)
																														((BgL_sfunz00_bglt)
																															BgL_fifz00_1590));
																													BgL_auxz00_2617 =
																														BGL_OBJECT_WIDENING
																														(BgL_tmpz00_2618);
																												}
																												BgL_auxz00_2616 =
																													(
																													(BgL_sfunzf2iinfozf2_bglt)
																													BgL_auxz00_2617);
																											}
																											((((BgL_sfunzf2iinfozf2_bglt) COBJECT(BgL_auxz00_2616))->BgL_istampz00) = ((obj_t) BINT(BgL_stampz00_1570)), BUNSPEC);
																										}
																										{	/* Integrate/g.scm 115 */
																											obj_t BgL_arg1349z00_1635;

																											BgL_arg1349z00_1635 =
																												CDR(
																												((obj_t)
																													BgL_ctz00_1593));
																											{
																												bool_t
																													BgL_stopzf3zf3_2628;
																												obj_t BgL_ctz00_2627;

																												BgL_ctz00_2627 =
																													BgL_arg1349z00_1635;
																												BgL_stopzf3zf3_2628 =
																													BgL_stopzf3zf3_1634;
																												BgL_stopzf3zf3_1594 =
																													BgL_stopzf3zf3_2628;
																												BgL_ctz00_1593 =
																													BgL_ctz00_2627;
																												goto
																													BgL_zc3z04anonymousza31285ze3z87_1596;
																											}
																										}
																									}
																							}
																					}
																			}
																	}
															}
														}
													}
											}
										}
									}
								}
							}
					}
			}
		}

	}



/* &G! */
	obj_t BGl_z62Gz12z70zzintegrate_gz00(obj_t BgL_envz00_2241,
		obj_t BgL_gzf2cnzf2_2242)
	{
		{	/* Integrate/g.scm 32 */
			return BGl_Gz12z12zzintegrate_gz00(BgL_gzf2cnzf2_2242);
		}

	}



/* integrate-remaining-local-functions! */
	bool_t
		BGl_integratezd2remainingzd2localzd2functionsz12zc0zzintegrate_gz00(void)
	{
		{	/* Integrate/g.scm 143 */
			{
				obj_t BgL_l1245z00_1646;

				BgL_l1245z00_1646 = BGl_za2phiza2z00zzintegrate_az00;
			BgL_zc3z04anonymousza31365ze3z87_1647:
				if (PAIRP(BgL_l1245z00_1646))
					{	/* Integrate/g.scm 144 */
						{	/* Integrate/g.scm 145 */
							obj_t BgL_fz00_1649;

							BgL_fz00_1649 = CAR(BgL_l1245z00_1646);
							{	/* Integrate/g.scm 145 */
								bool_t BgL_test1761z00_2633;

								{	/* Integrate/g.scm 145 */
									bool_t BgL_test1763z00_2634;

									{	/* Integrate/g.scm 145 */
										obj_t BgL_classz00_2084;

										BgL_classz00_2084 = BGl_localz00zzast_varz00;
										if (BGL_OBJECTP(BgL_fz00_1649))
											{	/* Integrate/g.scm 145 */
												BgL_objectz00_bglt BgL_arg1807z00_2086;

												BgL_arg1807z00_2086 =
													(BgL_objectz00_bglt) (BgL_fz00_1649);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Integrate/g.scm 145 */
														long BgL_idxz00_2092;

														BgL_idxz00_2092 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2086);
														BgL_test1763z00_2634 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_2092 + 2L)) == BgL_classz00_2084);
													}
												else
													{	/* Integrate/g.scm 145 */
														bool_t BgL_res1708z00_2117;

														{	/* Integrate/g.scm 145 */
															obj_t BgL_oclassz00_2100;

															{	/* Integrate/g.scm 145 */
																obj_t BgL_arg1815z00_2108;
																long BgL_arg1816z00_2109;

																BgL_arg1815z00_2108 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Integrate/g.scm 145 */
																	long BgL_arg1817z00_2110;

																	BgL_arg1817z00_2110 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2086);
																	BgL_arg1816z00_2109 =
																		(BgL_arg1817z00_2110 - OBJECT_TYPE);
																}
																BgL_oclassz00_2100 =
																	VECTOR_REF(BgL_arg1815z00_2108,
																	BgL_arg1816z00_2109);
															}
															{	/* Integrate/g.scm 145 */
																bool_t BgL__ortest_1115z00_2101;

																BgL__ortest_1115z00_2101 =
																	(BgL_classz00_2084 == BgL_oclassz00_2100);
																if (BgL__ortest_1115z00_2101)
																	{	/* Integrate/g.scm 145 */
																		BgL_res1708z00_2117 =
																			BgL__ortest_1115z00_2101;
																	}
																else
																	{	/* Integrate/g.scm 145 */
																		long BgL_odepthz00_2102;

																		{	/* Integrate/g.scm 145 */
																			obj_t BgL_arg1804z00_2103;

																			BgL_arg1804z00_2103 =
																				(BgL_oclassz00_2100);
																			BgL_odepthz00_2102 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_2103);
																		}
																		if ((2L < BgL_odepthz00_2102))
																			{	/* Integrate/g.scm 145 */
																				obj_t BgL_arg1802z00_2105;

																				{	/* Integrate/g.scm 145 */
																					obj_t BgL_arg1803z00_2106;

																					BgL_arg1803z00_2106 =
																						(BgL_oclassz00_2100);
																					BgL_arg1802z00_2105 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_2106, 2L);
																				}
																				BgL_res1708z00_2117 =
																					(BgL_arg1802z00_2105 ==
																					BgL_classz00_2084);
																			}
																		else
																			{	/* Integrate/g.scm 145 */
																				BgL_res1708z00_2117 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test1763z00_2634 = BgL_res1708z00_2117;
													}
											}
										else
											{	/* Integrate/g.scm 145 */
												BgL_test1763z00_2634 = ((bool_t) 0);
											}
									}
									if (BgL_test1763z00_2634)
										{	/* Integrate/g.scm 146 */
											bool_t BgL_test1768z00_2657;

											{	/* Integrate/g.scm 146 */
												BgL_sfunz00_bglt BgL_oz00_2119;

												BgL_oz00_2119 =
													((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_fz00_1649))))->
														BgL_valuez00));
												{
													BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2662;

													{
														obj_t BgL_auxz00_2663;

														{	/* Integrate/g.scm 146 */
															BgL_objectz00_bglt BgL_tmpz00_2664;

															BgL_tmpz00_2664 =
																((BgL_objectz00_bglt) BgL_oz00_2119);
															BgL_auxz00_2663 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2664);
														}
														BgL_auxz00_2662 =
															((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2663);
													}
													BgL_test1768z00_2657 =
														(((BgL_sfunzf2iinfozf2_bglt)
															COBJECT(BgL_auxz00_2662))->BgL_gzf3zf3);
												}
											}
											if (BgL_test1768z00_2657)
												{	/* Integrate/g.scm 146 */
													BgL_test1761z00_2633 = ((bool_t) 0);
												}
											else
												{	/* Integrate/g.scm 147 */
													bool_t BgL_test1769z00_2669;

													{	/* Integrate/g.scm 147 */
														obj_t BgL_arg1453z00_1666;

														{	/* Integrate/g.scm 147 */
															BgL_sfunz00_bglt BgL_oz00_2122;

															BgL_oz00_2122 =
																((BgL_sfunz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt) BgL_fz00_1649))))->
																	BgL_valuez00));
															{
																BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2674;

																{
																	obj_t BgL_auxz00_2675;

																	{	/* Integrate/g.scm 147 */
																		BgL_objectz00_bglt BgL_tmpz00_2676;

																		BgL_tmpz00_2676 =
																			((BgL_objectz00_bglt) BgL_oz00_2122);
																		BgL_auxz00_2675 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_2676);
																	}
																	BgL_auxz00_2674 =
																		((BgL_sfunzf2iinfozf2_bglt)
																		BgL_auxz00_2675);
																}
																BgL_arg1453z00_1666 =
																	(((BgL_sfunzf2iinfozf2_bglt)
																		COBJECT(BgL_auxz00_2674))->BgL_lz00);
															}
														}
														{	/* Integrate/g.scm 147 */
															obj_t BgL_classz00_2124;

															BgL_classz00_2124 = BGl_variablez00zzast_varz00;
															if (BGL_OBJECTP(BgL_arg1453z00_1666))
																{	/* Integrate/g.scm 147 */
																	BgL_objectz00_bglt BgL_arg1807z00_2126;

																	BgL_arg1807z00_2126 =
																		(BgL_objectz00_bglt) (BgL_arg1453z00_1666);
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Integrate/g.scm 147 */
																			long BgL_idxz00_2132;

																			BgL_idxz00_2132 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_2126);
																			BgL_test1769z00_2669 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_2132 + 1L)) ==
																				BgL_classz00_2124);
																		}
																	else
																		{	/* Integrate/g.scm 147 */
																			bool_t BgL_res1709z00_2157;

																			{	/* Integrate/g.scm 147 */
																				obj_t BgL_oclassz00_2140;

																				{	/* Integrate/g.scm 147 */
																					obj_t BgL_arg1815z00_2148;
																					long BgL_arg1816z00_2149;

																					BgL_arg1815z00_2148 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Integrate/g.scm 147 */
																						long BgL_arg1817z00_2150;

																						BgL_arg1817z00_2150 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_2126);
																						BgL_arg1816z00_2149 =
																							(BgL_arg1817z00_2150 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_2140 =
																						VECTOR_REF(BgL_arg1815z00_2148,
																						BgL_arg1816z00_2149);
																				}
																				{	/* Integrate/g.scm 147 */
																					bool_t BgL__ortest_1115z00_2141;

																					BgL__ortest_1115z00_2141 =
																						(BgL_classz00_2124 ==
																						BgL_oclassz00_2140);
																					if (BgL__ortest_1115z00_2141)
																						{	/* Integrate/g.scm 147 */
																							BgL_res1709z00_2157 =
																								BgL__ortest_1115z00_2141;
																						}
																					else
																						{	/* Integrate/g.scm 147 */
																							long BgL_odepthz00_2142;

																							{	/* Integrate/g.scm 147 */
																								obj_t BgL_arg1804z00_2143;

																								BgL_arg1804z00_2143 =
																									(BgL_oclassz00_2140);
																								BgL_odepthz00_2142 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_2143);
																							}
																							if ((1L < BgL_odepthz00_2142))
																								{	/* Integrate/g.scm 147 */
																									obj_t BgL_arg1802z00_2145;

																									{	/* Integrate/g.scm 147 */
																										obj_t BgL_arg1803z00_2146;

																										BgL_arg1803z00_2146 =
																											(BgL_oclassz00_2140);
																										BgL_arg1802z00_2145 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_2146, 1L);
																									}
																									BgL_res1709z00_2157 =
																										(BgL_arg1802z00_2145 ==
																										BgL_classz00_2124);
																								}
																							else
																								{	/* Integrate/g.scm 147 */
																									BgL_res1709z00_2157 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test1769z00_2669 =
																				BgL_res1709z00_2157;
																		}
																}
															else
																{	/* Integrate/g.scm 147 */
																	BgL_test1769z00_2669 = ((bool_t) 0);
																}
														}
													}
													if (BgL_test1769z00_2669)
														{	/* Integrate/g.scm 147 */
															BgL_test1761z00_2633 = ((bool_t) 0);
														}
													else
														{	/* Integrate/g.scm 147 */
															BgL_test1761z00_2633 = ((bool_t) 1);
														}
												}
										}
									else
										{	/* Integrate/g.scm 145 */
											BgL_test1761z00_2633 = ((bool_t) 0);
										}
								}
								if (BgL_test1761z00_2633)
									{	/* Integrate/g.scm 145 */
										{	/* Integrate/g.scm 153 */
											BgL_valuez00_bglt BgL_arg1434z00_1660;
											obj_t BgL_arg1437z00_1661;

											BgL_arg1434z00_1660 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt) BgL_fz00_1649))))->
												BgL_valuez00);
											{	/* Integrate/g.scm 154 */
												BgL_sfunz00_bglt BgL_oz00_2160;

												BgL_oz00_2160 =
													((BgL_sfunz00_bglt)
													(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_fz00_1649))))->
														BgL_valuez00));
												{
													BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2710;

													{
														obj_t BgL_auxz00_2711;

														{	/* Integrate/g.scm 154 */
															BgL_objectz00_bglt BgL_tmpz00_2712;

															BgL_tmpz00_2712 =
																((BgL_objectz00_bglt) BgL_oz00_2160);
															BgL_auxz00_2711 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_2712);
														}
														BgL_auxz00_2710 =
															((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2711);
													}
													BgL_arg1437z00_1661 =
														(((BgL_sfunzf2iinfozf2_bglt)
															COBJECT(BgL_auxz00_2710))->BgL_ownerz00);
												}
											}
											{
												BgL_sfunzf2iinfozf2_bglt BgL_auxz00_2717;

												{
													obj_t BgL_auxz00_2718;

													{	/* Integrate/g.scm 153 */
														BgL_objectz00_bglt BgL_tmpz00_2719;

														BgL_tmpz00_2719 =
															((BgL_objectz00_bglt)
															((BgL_sfunz00_bglt) BgL_arg1434z00_1660));
														BgL_auxz00_2718 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_2719);
													}
													BgL_auxz00_2717 =
														((BgL_sfunzf2iinfozf2_bglt) BgL_auxz00_2718);
												}
												((((BgL_sfunzf2iinfozf2_bglt)
															COBJECT(BgL_auxz00_2717))->BgL_lz00) =
													((obj_t) BgL_arg1437z00_1661), BUNSPEC);
											}
										}
									}
								else
									{	/* Integrate/g.scm 145 */
										BFALSE;
									}
							}
						}
						{
							obj_t BgL_l1245z00_2725;

							BgL_l1245z00_2725 = CDR(BgL_l1245z00_1646);
							BgL_l1245z00_1646 = BgL_l1245z00_2725;
							goto BgL_zc3z04anonymousza31365ze3z87_1647;
						}
					}
				else
					{	/* Integrate/g.scm 144 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzintegrate_gz00(void)
	{
		{	/* Integrate/g.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzintegrate_gz00(void)
	{
		{	/* Integrate/g.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzintegrate_gz00(void)
	{
		{	/* Integrate/g.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzintegrate_gz00(void)
	{
		{	/* Integrate/g.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1712z00zzintegrate_gz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1712z00zzintegrate_gz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1712z00zzintegrate_gz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1712z00zzintegrate_gz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1712z00zzintegrate_gz00));
			BGl_modulezd2initializa7ationz75zzintegrate_infoz00(0L,
				BSTRING_TO_STRING(BGl_string1712z00zzintegrate_gz00));
			return
				BGl_modulezd2initializa7ationz75zzintegrate_az00(523633197L,
				BSTRING_TO_STRING(BGl_string1712z00zzintegrate_gz00));
		}

	}

#ifdef __cplusplus
}
#endif
