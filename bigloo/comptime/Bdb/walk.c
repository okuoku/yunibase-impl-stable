/*===========================================================================*/
/*   (Bdb/walk.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Bdb/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BDB_WALK_TYPE_DEFINITIONS
#define BGL_BDB_WALK_TYPE_DEFINITIONS
#endif													// BGL_BDB_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzbdb_walkz00 = BUNSPEC;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static obj_t BGl_z62bdbzd2walkz12za2zzbdb_walkz00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzbdb_walkz00(void);
	static obj_t BGl_objectzd2initzd2zzbdb_walkz00(void);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_bdbzd2walkz12zc0zzbdb_walkz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzbdb_walkz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzbdb_walkz00(void);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzbdb_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbdb_initializa7eza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzbdb_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzbdb_walkz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzbdb_walkz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzbdb_walkz00(void);
	extern obj_t BGl_initializa7ezd2astz75zzbdb_initializa7eza7(void);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t __cnst[1];


	   
		 
		DEFINE_STRING(BGl_string1451z00zzbdb_walkz00,
		BgL_bgl_string1451za700za7za7b1473za7, "Bdb", 3);
	      DEFINE_STRING(BGl_string1452z00zzbdb_walkz00,
		BgL_bgl_string1452za700za7za7b1474za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1453z00zzbdb_walkz00,
		BgL_bgl_string1453za700za7za7b1475za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1454z00zzbdb_walkz00,
		BgL_bgl_string1454za700za7za7b1476za7, " error", 6);
	      DEFINE_STRING(BGl_string1455z00zzbdb_walkz00,
		BgL_bgl_string1455za700za7za7b1477za7, "s", 1);
	      DEFINE_STRING(BGl_string1456z00zzbdb_walkz00,
		BgL_bgl_string1456za700za7za7b1478za7, "", 0);
	      DEFINE_STRING(BGl_string1457z00zzbdb_walkz00,
		BgL_bgl_string1457za700za7za7b1479za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1458z00zzbdb_walkz00,
		BgL_bgl_string1458za700za7za7b1480za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1459z00zzbdb_walkz00,
		BgL_bgl_string1459za700za7za7b1481za7, "bdb_walk", 8);
	      DEFINE_STRING(BGl_string1460z00zzbdb_walkz00,
		BgL_bgl_string1460za700za7za7b1482za7, "pass-started ", 13);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_bdbzd2walkz12zd2envz12zzbdb_walkz00,
		BgL_bgl_za762bdbza7d2walkza7121483za7, BGl_z62bdbzd2walkz12za2zzbdb_walkz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzbdb_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzbdb_walkz00(long
		BgL_checksumz00_1589, char *BgL_fromz00_1590)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzbdb_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzbdb_walkz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzbdb_walkz00();
					BGl_libraryzd2moduleszd2initz00zzbdb_walkz00();
					BGl_cnstzd2initzd2zzbdb_walkz00();
					BGl_importedzd2moduleszd2initz00zzbdb_walkz00();
					return BGl_methodzd2initzd2zzbdb_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzbdb_walkz00(void)
	{
		{	/* Bdb/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "bdb_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"bdb_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "bdb_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "bdb_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "bdb_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "bdb_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "bdb_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"bdb_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzbdb_walkz00(void)
	{
		{	/* Bdb/walk.scm 15 */
			{	/* Bdb/walk.scm 15 */
				obj_t BgL_cportz00_1578;

				{	/* Bdb/walk.scm 15 */
					obj_t BgL_stringz00_1585;

					BgL_stringz00_1585 = BGl_string1460z00zzbdb_walkz00;
					{	/* Bdb/walk.scm 15 */
						obj_t BgL_startz00_1586;

						BgL_startz00_1586 = BINT(0L);
						{	/* Bdb/walk.scm 15 */
							obj_t BgL_endz00_1587;

							BgL_endz00_1587 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1585)));
							{	/* Bdb/walk.scm 15 */

								BgL_cportz00_1578 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1585, BgL_startz00_1586, BgL_endz00_1587);
				}}}}
				{
					long BgL_iz00_1579;

					BgL_iz00_1579 = 0L;
				BgL_loopz00_1580:
					if ((BgL_iz00_1579 == -1L))
						{	/* Bdb/walk.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Bdb/walk.scm 15 */
							{	/* Bdb/walk.scm 15 */
								obj_t BgL_arg1472z00_1581;

								{	/* Bdb/walk.scm 15 */

									{	/* Bdb/walk.scm 15 */
										obj_t BgL_locationz00_1583;

										BgL_locationz00_1583 = BBOOL(((bool_t) 0));
										{	/* Bdb/walk.scm 15 */

											BgL_arg1472z00_1581 =
												BGl_readz00zz__readerz00(BgL_cportz00_1578,
												BgL_locationz00_1583);
										}
									}
								}
								{	/* Bdb/walk.scm 15 */
									int BgL_tmpz00_1616;

									BgL_tmpz00_1616 = (int) (BgL_iz00_1579);
									CNST_TABLE_SET(BgL_tmpz00_1616, BgL_arg1472z00_1581);
							}}
							{	/* Bdb/walk.scm 15 */
								int BgL_auxz00_1584;

								BgL_auxz00_1584 = (int) ((BgL_iz00_1579 - 1L));
								{
									long BgL_iz00_1621;

									BgL_iz00_1621 = (long) (BgL_auxz00_1584);
									BgL_iz00_1579 = BgL_iz00_1621;
									goto BgL_loopz00_1580;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzbdb_walkz00(void)
	{
		{	/* Bdb/walk.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzbdb_walkz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1345;

				BgL_headz00_1345 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1346;
					obj_t BgL_tailz00_1347;

					BgL_prevz00_1346 = BgL_headz00_1345;
					BgL_tailz00_1347 = BgL_l1z00_1;
				BgL_loopz00_1348:
					if (PAIRP(BgL_tailz00_1347))
						{
							obj_t BgL_newzd2prevzd2_1350;

							BgL_newzd2prevzd2_1350 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1347), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1346, BgL_newzd2prevzd2_1350);
							{
								obj_t BgL_tailz00_1631;
								obj_t BgL_prevz00_1630;

								BgL_prevz00_1630 = BgL_newzd2prevzd2_1350;
								BgL_tailz00_1631 = CDR(BgL_tailz00_1347);
								BgL_tailz00_1347 = BgL_tailz00_1631;
								BgL_prevz00_1346 = BgL_prevz00_1630;
								goto BgL_loopz00_1348;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1345);
				}
			}
		}

	}



/* bdb-walk! */
	BGL_EXPORTED_DEF obj_t BGl_bdbzd2walkz12zc0zzbdb_walkz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Bdb/walk.scm 30 */
			{	/* Bdb/walk.scm 31 */
				obj_t BgL_list1231z00_1353;

				{	/* Bdb/walk.scm 31 */
					obj_t BgL_arg1232z00_1354;

					{	/* Bdb/walk.scm 31 */
						obj_t BgL_arg1233z00_1355;

						BgL_arg1233z00_1355 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1232z00_1354 =
							MAKE_YOUNG_PAIR(BGl_string1451z00zzbdb_walkz00,
							BgL_arg1233z00_1355);
					}
					BgL_list1231z00_1353 =
						MAKE_YOUNG_PAIR(BGl_string1452z00zzbdb_walkz00,
						BgL_arg1232z00_1354);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1231z00_1353);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1451z00zzbdb_walkz00;
			{	/* Bdb/walk.scm 31 */
				obj_t BgL_g1106z00_1356;

				BgL_g1106z00_1356 = BNIL;
				{
					obj_t BgL_hooksz00_1359;
					obj_t BgL_hnamesz00_1360;

					BgL_hooksz00_1359 = BgL_g1106z00_1356;
					BgL_hnamesz00_1360 = BNIL;
				BgL_zc3z04anonymousza31234ze3z87_1361:
					if (NULLP(BgL_hooksz00_1359))
						{	/* Bdb/walk.scm 31 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Bdb/walk.scm 31 */
							bool_t BgL_test1488z00_1644;

							{	/* Bdb/walk.scm 31 */
								obj_t BgL_fun1243z00_1368;

								BgL_fun1243z00_1368 = CAR(((obj_t) BgL_hooksz00_1359));
								BgL_test1488z00_1644 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1243z00_1368));
							}
							if (BgL_test1488z00_1644)
								{	/* Bdb/walk.scm 31 */
									obj_t BgL_arg1238z00_1365;
									obj_t BgL_arg1239z00_1366;

									BgL_arg1238z00_1365 = CDR(((obj_t) BgL_hooksz00_1359));
									BgL_arg1239z00_1366 = CDR(((obj_t) BgL_hnamesz00_1360));
									{
										obj_t BgL_hnamesz00_1656;
										obj_t BgL_hooksz00_1655;

										BgL_hooksz00_1655 = BgL_arg1238z00_1365;
										BgL_hnamesz00_1656 = BgL_arg1239z00_1366;
										BgL_hnamesz00_1360 = BgL_hnamesz00_1656;
										BgL_hooksz00_1359 = BgL_hooksz00_1655;
										goto BgL_zc3z04anonymousza31234ze3z87_1361;
									}
								}
							else
								{	/* Bdb/walk.scm 31 */
									obj_t BgL_arg1242z00_1367;

									BgL_arg1242z00_1367 = CAR(((obj_t) BgL_hnamesz00_1360));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1451z00zzbdb_walkz00,
										BGl_string1453z00zzbdb_walkz00, BgL_arg1242z00_1367);
								}
						}
				}
			}
			{	/* Bdb/walk.scm 32 */
				obj_t BgL_valuez00_1371;

				BgL_valuez00_1371 =
					BGl_appendzd221011zd2zzbdb_walkz00
					(BGl_initializa7ezd2astz75zzbdb_initializa7eza7(), BgL_globalsz00_3);
				if (((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) >
						0L))
					{	/* Bdb/walk.scm 32 */
						{	/* Bdb/walk.scm 32 */
							obj_t BgL_port1227z00_1373;

							{	/* Bdb/walk.scm 32 */
								obj_t BgL_tmpz00_1665;

								BgL_tmpz00_1665 = BGL_CURRENT_DYNAMIC_ENV();
								BgL_port1227z00_1373 =
									BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_1665);
							}
							bgl_display_obj
								(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
								BgL_port1227z00_1373);
							bgl_display_string(BGl_string1454z00zzbdb_walkz00,
								BgL_port1227z00_1373);
							{	/* Bdb/walk.scm 32 */
								obj_t BgL_arg1248z00_1374;

								{	/* Bdb/walk.scm 32 */
									bool_t BgL_test1490z00_1670;

									if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
										(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
										{	/* Bdb/walk.scm 32 */
											if (INTEGERP
												(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
												{	/* Bdb/walk.scm 32 */
													BgL_test1490z00_1670 =
														(
														(long)
														CINT
														(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
														> 1L);
												}
											else
												{	/* Bdb/walk.scm 32 */
													BgL_test1490z00_1670 =
														BGl_2ze3ze3zz__r4_numbers_6_5z00
														(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
														BINT(1L));
												}
										}
									else
										{	/* Bdb/walk.scm 32 */
											BgL_test1490z00_1670 = ((bool_t) 0);
										}
									if (BgL_test1490z00_1670)
										{	/* Bdb/walk.scm 32 */
											BgL_arg1248z00_1374 = BGl_string1455z00zzbdb_walkz00;
										}
									else
										{	/* Bdb/walk.scm 32 */
											BgL_arg1248z00_1374 = BGl_string1456z00zzbdb_walkz00;
										}
								}
								bgl_display_obj(BgL_arg1248z00_1374, BgL_port1227z00_1373);
							}
							bgl_display_string(BGl_string1457z00zzbdb_walkz00,
								BgL_port1227z00_1373);
							bgl_display_char(((unsigned char) 10), BgL_port1227z00_1373);
						}
						{	/* Bdb/walk.scm 32 */
							obj_t BgL_list1251z00_1378;

							BgL_list1251z00_1378 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
							BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1251z00_1378);
						}
					}
				else
					{	/* Bdb/walk.scm 32 */
						obj_t BgL_g1108z00_1379;

						BgL_g1108z00_1379 = BNIL;
						{
							obj_t BgL_hooksz00_1382;
							obj_t BgL_hnamesz00_1383;

							BgL_hooksz00_1382 = BgL_g1108z00_1379;
							BgL_hnamesz00_1383 = BNIL;
						BgL_zc3z04anonymousza31252ze3z87_1384:
							if (NULLP(BgL_hooksz00_1382))
								{	/* Bdb/walk.scm 32 */
									return BgL_valuez00_1371;
								}
							else
								{	/* Bdb/walk.scm 32 */
									bool_t BgL_test1494z00_1687;

									{	/* Bdb/walk.scm 32 */
										obj_t BgL_fun1285z00_1391;

										BgL_fun1285z00_1391 = CAR(((obj_t) BgL_hooksz00_1382));
										BgL_test1494z00_1687 =
											CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1285z00_1391));
									}
									if (BgL_test1494z00_1687)
										{	/* Bdb/walk.scm 32 */
											obj_t BgL_arg1268z00_1388;
											obj_t BgL_arg1272z00_1389;

											BgL_arg1268z00_1388 = CDR(((obj_t) BgL_hooksz00_1382));
											BgL_arg1272z00_1389 = CDR(((obj_t) BgL_hnamesz00_1383));
											{
												obj_t BgL_hnamesz00_1699;
												obj_t BgL_hooksz00_1698;

												BgL_hooksz00_1698 = BgL_arg1268z00_1388;
												BgL_hnamesz00_1699 = BgL_arg1272z00_1389;
												BgL_hnamesz00_1383 = BgL_hnamesz00_1699;
												BgL_hooksz00_1382 = BgL_hooksz00_1698;
												goto BgL_zc3z04anonymousza31252ze3z87_1384;
											}
										}
									else
										{	/* Bdb/walk.scm 32 */
											obj_t BgL_arg1284z00_1390;

											BgL_arg1284z00_1390 = CAR(((obj_t) BgL_hnamesz00_1383));
											return
												BGl_internalzd2errorzd2zztools_errorz00
												(BGl_za2currentzd2passza2zd2zzengine_passz00,
												BGl_string1458z00zzbdb_walkz00, BgL_arg1284z00_1390);
										}
								}
						}
					}
			}
		}

	}



/* &bdb-walk! */
	obj_t BGl_z62bdbzd2walkz12za2zzbdb_walkz00(obj_t BgL_envz00_1576,
		obj_t BgL_globalsz00_1577)
	{
		{	/* Bdb/walk.scm 30 */
			return BGl_bdbzd2walkz12zc0zzbdb_walkz00(BgL_globalsz00_1577);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzbdb_walkz00(void)
	{
		{	/* Bdb/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzbdb_walkz00(void)
	{
		{	/* Bdb/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzbdb_walkz00(void)
	{
		{	/* Bdb/walk.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzbdb_walkz00(void)
	{
		{	/* Bdb/walk.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1459z00zzbdb_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1459z00zzbdb_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1459z00zzbdb_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1459z00zzbdb_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1459z00zzbdb_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1459z00zzbdb_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1459z00zzbdb_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1459z00zzbdb_walkz00));
			BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1459z00zzbdb_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzbdb_initializa7eza7(527197336L,
				BSTRING_TO_STRING(BGl_string1459z00zzbdb_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
