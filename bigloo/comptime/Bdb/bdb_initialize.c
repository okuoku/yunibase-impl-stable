/*===========================================================================*/
/*   (Bdb/bdb_initialize.scm)                                                */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Bdb/bdb_initialize.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BDB_INITIALIZE_TYPE_DEFINITIONS
#define BGL_BDB_INITIALIZE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;


#endif													// BGL_BDB_INITIALIZE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_requirezd2initializa7ationz75zzbdb_initializa7eza7 = BUNSPEC;
	static obj_t BGl_toplevelzd2initzd2zzbdb_initializa7eza7(void);
	static obj_t BGl_genericzd2initzd2zzbdb_initializa7eza7(void);
	static obj_t BGl_objectzd2initzd2zzbdb_initializa7eza7(void);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzbdb_initializa7eza7(void);
	extern BgL_nodez00_bglt BGl_coercez12z12zzcoerce_coercez00(BgL_nodez00_bglt,
		obj_t, BgL_typez00_bglt, bool_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	extern obj_t BGl_buildzd2astzd2zzast_buildz00(obj_t);
	static obj_t BGl_z62initializa7ezd2astz17zzbdb_initializa7eza7(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzbdb_initializa7eza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_allocz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcoerce_coercez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_buildz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_cnstzd2initzd2zzbdb_initializa7eza7(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzbdb_initializa7eza7(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzbdb_initializa7eza7(void);
	static obj_t BGl_gczd2rootszd2initz00zzbdb_initializa7eza7(void);
	BGL_EXPORTED_DECL obj_t BGl_initializa7ezd2astz75zzbdb_initializa7eza7(void);
	static obj_t __cnst[5];


	   
		 
		DEFINE_STRING(BGl_string1492z00zzbdb_initializa7eza7,
		BgL_bgl_string1492za700za7za7b1503za7, "((obj_t)__bdb_info)", 19);
	      DEFINE_STRING(BGl_string1493z00zzbdb_initializa7eza7,
		BgL_bgl_string1493za700za7za7b1504za7, "bdb_initialize", 14);
	      DEFINE_STRING(BGl_string1494z00zzbdb_initializa7eza7,
		BgL_bgl_string1494za700za7za7b1505za7,
		"unit bdb bdb-set-module-info! pragma::obj quote ", 48);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_initializa7ezd2astzd2envza7zzbdb_initializa7eza7,
		BgL_bgl_za762initializa7a7eza71506za7,
		BGl_z62initializa7ezd2astz17zzbdb_initializa7eza7, 0L, BUNSPEC, 0);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzbdb_initializa7eza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzbdb_initializa7eza7(long
		BgL_checksumz00_1619, char *BgL_fromz00_1620)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzbdb_initializa7eza7))
				{
					BGl_requirezd2initializa7ationz75zzbdb_initializa7eza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzbdb_initializa7eza7();
					BGl_libraryzd2moduleszd2initz00zzbdb_initializa7eza7();
					BGl_cnstzd2initzd2zzbdb_initializa7eza7();
					BGl_importedzd2moduleszd2initz00zzbdb_initializa7eza7();
					return BGl_toplevelzd2initzd2zzbdb_initializa7eza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzbdb_initializa7eza7(void)
	{
		{	/* Bdb/bdb_initialize.scm 15 */
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "bdb_initialize");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"bdb_initialize");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"bdb_initialize");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"bdb_initialize");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "bdb_initialize");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "bdb_initialize");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzbdb_initializa7eza7(void)
	{
		{	/* Bdb/bdb_initialize.scm 15 */
			{	/* Bdb/bdb_initialize.scm 15 */
				obj_t BgL_cportz00_1608;

				{	/* Bdb/bdb_initialize.scm 15 */
					obj_t BgL_stringz00_1615;

					BgL_stringz00_1615 = BGl_string1494z00zzbdb_initializa7eza7;
					{	/* Bdb/bdb_initialize.scm 15 */
						obj_t BgL_startz00_1616;

						BgL_startz00_1616 = BINT(0L);
						{	/* Bdb/bdb_initialize.scm 15 */
							obj_t BgL_endz00_1617;

							BgL_endz00_1617 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1615)));
							{	/* Bdb/bdb_initialize.scm 15 */

								BgL_cportz00_1608 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1615, BgL_startz00_1616, BgL_endz00_1617);
				}}}}
				{
					long BgL_iz00_1609;

					BgL_iz00_1609 = 4L;
				BgL_loopz00_1610:
					if ((BgL_iz00_1609 == -1L))
						{	/* Bdb/bdb_initialize.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Bdb/bdb_initialize.scm 15 */
							{	/* Bdb/bdb_initialize.scm 15 */
								obj_t BgL_arg1502z00_1611;

								{	/* Bdb/bdb_initialize.scm 15 */

									{	/* Bdb/bdb_initialize.scm 15 */
										obj_t BgL_locationz00_1613;

										BgL_locationz00_1613 = BBOOL(((bool_t) 0));
										{	/* Bdb/bdb_initialize.scm 15 */

											BgL_arg1502z00_1611 =
												BGl_readz00zz__readerz00(BgL_cportz00_1608,
												BgL_locationz00_1613);
										}
									}
								}
								{	/* Bdb/bdb_initialize.scm 15 */
									int BgL_tmpz00_1644;

									BgL_tmpz00_1644 = (int) (BgL_iz00_1609);
									CNST_TABLE_SET(BgL_tmpz00_1644, BgL_arg1502z00_1611);
							}}
							{	/* Bdb/bdb_initialize.scm 15 */
								int BgL_auxz00_1614;

								BgL_auxz00_1614 = (int) ((BgL_iz00_1609 - 1L));
								{
									long BgL_iz00_1649;

									BgL_iz00_1649 = (long) (BgL_auxz00_1614);
									BgL_iz00_1609 = BgL_iz00_1649;
									goto BgL_loopz00_1610;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzbdb_initializa7eza7(void)
	{
		{	/* Bdb/bdb_initialize.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzbdb_initializa7eza7(void)
	{
		{	/* Bdb/bdb_initialize.scm 15 */
			return BUNSPEC;
		}

	}



/* initialize-ast */
	BGL_EXPORTED_DEF obj_t BGl_initializa7ezd2astz75zzbdb_initializa7eza7(void)
	{
		{	/* Bdb/bdb_initialize.scm 42 */
			{	/* Bdb/bdb_initialize.scm 44 */
				obj_t BgL_bodyz00_1397;

				{	/* Bdb/bdb_initialize.scm 44 */
					obj_t BgL_arg1284z00_1412;

					{	/* Bdb/bdb_initialize.scm 44 */
						obj_t BgL_arg1304z00_1413;

						{	/* Bdb/bdb_initialize.scm 44 */
							obj_t BgL_arg1305z00_1414;
							obj_t BgL_arg1306z00_1415;

							{	/* Bdb/bdb_initialize.scm 44 */
								obj_t BgL_arg1307z00_1416;

								BgL_arg1307z00_1416 =
									MAKE_YOUNG_PAIR(BGl_za2moduleza2z00zzmodule_modulez00, BNIL);
								BgL_arg1305z00_1414 =
									MAKE_YOUNG_PAIR(CNST_TABLE_REF(0), BgL_arg1307z00_1416);
							}
							{	/* Bdb/bdb_initialize.scm 45 */
								obj_t BgL_arg1308z00_1417;

								{	/* Bdb/bdb_initialize.scm 45 */
									obj_t BgL_arg1310z00_1418;

									BgL_arg1310z00_1418 =
										MAKE_YOUNG_PAIR(BGl_string1492z00zzbdb_initializa7eza7,
										BNIL);
									BgL_arg1308z00_1417 =
										MAKE_YOUNG_PAIR(CNST_TABLE_REF(1), BgL_arg1310z00_1418);
								}
								BgL_arg1306z00_1415 =
									MAKE_YOUNG_PAIR(BgL_arg1308z00_1417, BNIL);
							}
							BgL_arg1304z00_1413 =
								MAKE_YOUNG_PAIR(BgL_arg1305z00_1414, BgL_arg1306z00_1415);
						}
						BgL_arg1284z00_1412 =
							MAKE_YOUNG_PAIR(CNST_TABLE_REF(2), BgL_arg1304z00_1413);
					}
					BgL_bodyz00_1397 = MAKE_YOUNG_PAIR(BgL_arg1284z00_1412, BNIL);
				}
				{	/* Bdb/bdb_initialize.scm 46 */
					obj_t BgL_unitz00_1398;

					{	/* Bdb/bdb_initialize.scm 46 */
						obj_t BgL_idz00_1593;

						BgL_idz00_1593 = CNST_TABLE_REF(3);
						{	/* Bdb/bdb_initialize.scm 46 */
							obj_t BgL_newz00_1594;

							BgL_newz00_1594 = create_struct(CNST_TABLE_REF(4), (int) (5L));
							{	/* Bdb/bdb_initialize.scm 46 */
								int BgL_tmpz00_1667;

								BgL_tmpz00_1667 = (int) (4L);
								STRUCT_SET(BgL_newz00_1594, BgL_tmpz00_1667, BFALSE);
							}
							{	/* Bdb/bdb_initialize.scm 46 */
								int BgL_tmpz00_1670;

								BgL_tmpz00_1670 = (int) (3L);
								STRUCT_SET(BgL_newz00_1594, BgL_tmpz00_1670, BTRUE);
							}
							{	/* Bdb/bdb_initialize.scm 46 */
								int BgL_tmpz00_1673;

								BgL_tmpz00_1673 = (int) (2L);
								STRUCT_SET(BgL_newz00_1594, BgL_tmpz00_1673, BgL_bodyz00_1397);
							}
							{	/* Bdb/bdb_initialize.scm 46 */
								obj_t BgL_auxz00_1678;
								int BgL_tmpz00_1676;

								BgL_auxz00_1678 = BINT(9L);
								BgL_tmpz00_1676 = (int) (1L);
								STRUCT_SET(BgL_newz00_1594, BgL_tmpz00_1676, BgL_auxz00_1678);
							}
							{	/* Bdb/bdb_initialize.scm 46 */
								int BgL_tmpz00_1681;

								BgL_tmpz00_1681 = (int) (0L);
								STRUCT_SET(BgL_newz00_1594, BgL_tmpz00_1681, BgL_idz00_1593);
							}
							BgL_unitz00_1398 = BgL_newz00_1594;
					}}
					{	/* Bdb/bdb_initialize.scm 47 */
						obj_t BgL_astz00_1399;

						{	/* Bdb/bdb_initialize.scm 47 */
							obj_t BgL_arg1272z00_1410;

							{	/* Bdb/bdb_initialize.scm 47 */
								obj_t BgL_list1273z00_1411;

								BgL_list1273z00_1411 = MAKE_YOUNG_PAIR(BgL_unitz00_1398, BNIL);
								BgL_arg1272z00_1410 = BgL_list1273z00_1411;
							}
							BgL_astz00_1399 =
								BGl_buildzd2astzd2zzast_buildz00(BgL_arg1272z00_1410);
						}
						{
							obj_t BgL_l1226z00_1401;

							BgL_l1226z00_1401 = BgL_astz00_1399;
						BgL_zc3z04anonymousza31245ze3z87_1402:
							if (PAIRP(BgL_l1226z00_1401))
								{	/* Bdb/bdb_initialize.scm 48 */
									{	/* Bdb/bdb_initialize.scm 49 */
										obj_t BgL_globalz00_1404;

										BgL_globalz00_1404 = CAR(BgL_l1226z00_1401);
										{	/* Bdb/bdb_initialize.scm 49 */
											obj_t BgL_arg1248z00_1405;
											BgL_typez00_bglt BgL_arg1249z00_1406;

											BgL_arg1248z00_1405 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt)
															(((BgL_variablez00_bglt) COBJECT(
																		((BgL_variablez00_bglt)
																			((BgL_globalz00_bglt)
																				BgL_globalz00_1404))))->
																BgL_valuez00))))->BgL_bodyz00);
											BgL_arg1249z00_1406 =
												(((BgL_variablez00_bglt)
													COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
																BgL_globalz00_1404))))->BgL_typez00);
											BGl_coercez12z12zzcoerce_coercez00(((BgL_nodez00_bglt)
													BgL_arg1248z00_1405), BgL_globalz00_1404,
												BgL_arg1249z00_1406, ((bool_t) 0));
										}
									}
									{
										obj_t BgL_l1226z00_1699;

										BgL_l1226z00_1699 = CDR(BgL_l1226z00_1401);
										BgL_l1226z00_1401 = BgL_l1226z00_1699;
										goto BgL_zc3z04anonymousza31245ze3z87_1402;
									}
								}
							else
								{	/* Bdb/bdb_initialize.scm 48 */
									((bool_t) 1);
								}
						}
						return BgL_astz00_1399;
					}
				}
			}
		}

	}



/* &initialize-ast */
	obj_t BGl_z62initializa7ezd2astz17zzbdb_initializa7eza7(obj_t BgL_envz00_1606)
	{
		{	/* Bdb/bdb_initialize.scm 42 */
			return BGl_initializa7ezd2astz75zzbdb_initializa7eza7();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzbdb_initializa7eza7(void)
	{
		{	/* Bdb/bdb_initialize.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzbdb_initializa7eza7(void)
	{
		{	/* Bdb/bdb_initialize.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzbdb_initializa7eza7(void)
	{
		{	/* Bdb/bdb_initialize.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzbdb_initializa7eza7(void)
	{
		{	/* Bdb/bdb_initialize.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzast_buildz00(428035925L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzast_glozd2defzd2(44601789L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzcoerce_coercez00(361167184L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			BGl_modulezd2initializa7ationz75zzcnst_allocz00(192699986L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
			return
				BGl_modulezd2initializa7ationz75zzcnst_nodez00(89013043L,
				BSTRING_TO_STRING(BGl_string1493z00zzbdb_initializa7eza7));
		}

	}

#ifdef __cplusplus
}
#endif
