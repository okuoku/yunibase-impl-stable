/*===========================================================================*/
/*   (Bdb/spread_obj.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Bdb/spread_obj.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BgL_BDB_SPREADzd2OBJzd2_TYPE_DEFINITIONS
#define BGL_BgL_BDB_SPREADzd2OBJzd2_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;


#endif													// BGL_BgL_BDB_SPREADzd2OBJzd2_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2swi1273za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzbdb_spreadzd2objzd2 = BUNSPEC;
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	static obj_t BGl_spreadzd2objzd2funz12z12zzbdb_spreadzd2objzd2(obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2fai1271za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzbdb_spreadzd2objzd2(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2seq1253za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzbdb_spreadzd2objzd2(void);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static obj_t BGl_objectzd2initzd2zzbdb_spreadzd2objzd2(void);
	static obj_t
		BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(BgL_nodez00_bglt);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2let1275za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2let1277za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzbdb_spreadzd2objzd2(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2app1257za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2app1259za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2con1269za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	static obj_t BGl_z62spreadzd2objzd2nodez12z70zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	extern obj_t BGl_za2bdbzd2debugza2zd2zzengine_paramz00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2box1285za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2box1287za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t BGl_spreadzd2objzd2nodeza2z12zb0zzbdb_spreadzd2objzd2(obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2jum1281za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2set1267za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzbdb_spreadzd2objzd2(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2set1279za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzbdb_spreadzd2objzd2(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzbdb_spreadzd2objzd2(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzbdb_spreadzd2objzd2(void);
	static obj_t BGl_gczd2rootszd2initz00zzbdb_spreadzd2objzd2(void);
	static obj_t BGl_z62spreadzd2objzd2nodez121250z70zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2mak1283za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2cas1265za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2fun1261za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2ext1263za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	static obj_t
		BGl_z62spreadzd2objzd2nodez12zd2syn1255za2zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_bdbzd2spreadzd2objz12z12zzbdb_spreadzd2objzd2(obj_t);
	static obj_t BGl_z62bdbzd2spreadzd2objz12z70zzbdb_spreadzd2objzd2(obj_t,
		obj_t);
	static obj_t __cnst[2];


	   
		 
		DEFINE_STRING(BGl_string1742z00zzbdb_spreadzd2objzd2,
		BgL_bgl_string1742za700za7za7b1774za7, "Bdb (obj spreading)", 19);
	      DEFINE_STRING(BGl_string1743z00zzbdb_spreadzd2objzd2,
		BgL_bgl_string1743za700za7za7b1775za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1744z00zzbdb_spreadzd2objzd2,
		BgL_bgl_string1744za700za7za7b1776za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1745z00zzbdb_spreadzd2objzd2,
		BgL_bgl_string1745za700za7za7b1777za7, " error", 6);
	      DEFINE_STRING(BGl_string1746z00zzbdb_spreadzd2objzd2,
		BgL_bgl_string1746za700za7za7b1778za7, "s", 1);
	      DEFINE_STRING(BGl_string1747z00zzbdb_spreadzd2objzd2,
		BgL_bgl_string1747za700za7za7b1779za7, "", 0);
	      DEFINE_STRING(BGl_string1748z00zzbdb_spreadzd2objzd2,
		BgL_bgl_string1748za700za7za7b1780za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1749z00zzbdb_spreadzd2objzd2,
		BgL_bgl_string1749za700za7za7b1781za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1751z00zzbdb_spreadzd2objzd2,
		BgL_bgl_string1751za700za7za7b1782za7, "spread-obj-node!1250", 20);
	      DEFINE_STRING(BGl_string1753z00zzbdb_spreadzd2objzd2,
		BgL_bgl_string1753za700za7za7b1783za7, "spread-obj-node!", 16);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1750z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71784za7,
		BGl_z62spreadzd2objzd2nodez121250z70zzbdb_spreadzd2objzd2, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1752z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71785za7,
		BGl_z62spreadzd2objzd2nodez12zd2seq1253za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1754z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71786za7,
		BGl_z62spreadzd2objzd2nodez12zd2syn1255za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1755z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71787za7,
		BGl_z62spreadzd2objzd2nodez12zd2app1257za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1756z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71788za7,
		BGl_z62spreadzd2objzd2nodez12zd2app1259za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1757z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71789za7,
		BGl_z62spreadzd2objzd2nodez12zd2fun1261za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1758z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71790za7,
		BGl_z62spreadzd2objzd2nodez12zd2ext1263za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1759z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71791za7,
		BGl_z62spreadzd2objzd2nodez12zd2cas1265za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1760z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71792za7,
		BGl_z62spreadzd2objzd2nodez12zd2set1267za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1761z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71793za7,
		BGl_z62spreadzd2objzd2nodez12zd2con1269za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1762z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71794za7,
		BGl_z62spreadzd2objzd2nodez12zd2fai1271za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1763z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71795za7,
		BGl_z62spreadzd2objzd2nodez12zd2swi1273za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1764z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71796za7,
		BGl_z62spreadzd2objzd2nodez12zd2let1275za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1771z00zzbdb_spreadzd2objzd2,
		BgL_bgl_string1771za700za7za7b1797za7, "bdb_spread-obj", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1765z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71798za7,
		BGl_z62spreadzd2objzd2nodez12zd2let1277za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1772z00zzbdb_spreadzd2objzd2,
		BgL_bgl_string1772za700za7za7b1799za7, "done pass-started ", 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1766z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71800za7,
		BGl_z62spreadzd2objzd2nodez12zd2set1279za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1767z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71801za7,
		BGl_z62spreadzd2objzd2nodez12zd2jum1281za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1768z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71802za7,
		BGl_z62spreadzd2objzd2nodez12zd2mak1283za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1769z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71803za7,
		BGl_z62spreadzd2objzd2nodez12zd2box1285za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1770z00zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71804za7,
		BGl_z62spreadzd2objzd2nodez12zd2box1287za2zzbdb_spreadzd2objzd2, 0L,
		BUNSPEC, 1);
	     
		DEFINE_STATIC_BGL_GENERIC
		(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
		BgL_bgl_za762spreadza7d2objza71805za7,
		BGl_z62spreadzd2objzd2nodez12z70zzbdb_spreadzd2objzd2, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_bdbzd2spreadzd2objz12zd2envzc0zzbdb_spreadzd2objzd2,
		BgL_bgl_za762bdbza7d2spreadza71806za7,
		BGl_z62bdbzd2spreadzd2objz12z70zzbdb_spreadzd2objzd2, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzbdb_spreadzd2objzd2));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzbdb_spreadzd2objzd2(long
		BgL_checksumz00_1953, char *BgL_fromz00_1954)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzbdb_spreadzd2objzd2))
				{
					BGl_requirezd2initializa7ationz75zzbdb_spreadzd2objzd2 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzbdb_spreadzd2objzd2();
					BGl_libraryzd2moduleszd2initz00zzbdb_spreadzd2objzd2();
					BGl_cnstzd2initzd2zzbdb_spreadzd2objzd2();
					BGl_importedzd2moduleszd2initz00zzbdb_spreadzd2objzd2();
					BGl_genericzd2initzd2zzbdb_spreadzd2objzd2();
					BGl_methodzd2initzd2zzbdb_spreadzd2objzd2();
					return BGl_toplevelzd2initzd2zzbdb_spreadzd2objzd2();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzbdb_spreadzd2objzd2(void)
	{
		{	/* Bdb/spread_obj.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"bdb_spread-obj");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "bdb_spread-obj");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"bdb_spread-obj");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"bdb_spread-obj");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "bdb_spread-obj");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"bdb_spread-obj");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "bdb_spread-obj");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"bdb_spread-obj");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"bdb_spread-obj");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "bdb_spread-obj");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"bdb_spread-obj");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzbdb_spreadzd2objzd2(void)
	{
		{	/* Bdb/spread_obj.scm 15 */
			{	/* Bdb/spread_obj.scm 15 */
				obj_t BgL_cportz00_1905;

				{	/* Bdb/spread_obj.scm 15 */
					obj_t BgL_stringz00_1912;

					BgL_stringz00_1912 = BGl_string1772z00zzbdb_spreadzd2objzd2;
					{	/* Bdb/spread_obj.scm 15 */
						obj_t BgL_startz00_1913;

						BgL_startz00_1913 = BINT(0L);
						{	/* Bdb/spread_obj.scm 15 */
							obj_t BgL_endz00_1914;

							BgL_endz00_1914 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_1912)));
							{	/* Bdb/spread_obj.scm 15 */

								BgL_cportz00_1905 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_1912, BgL_startz00_1913, BgL_endz00_1914);
				}}}}
				{
					long BgL_iz00_1906;

					BgL_iz00_1906 = 1L;
				BgL_loopz00_1907:
					if ((BgL_iz00_1906 == -1L))
						{	/* Bdb/spread_obj.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Bdb/spread_obj.scm 15 */
							{	/* Bdb/spread_obj.scm 15 */
								obj_t BgL_arg1773z00_1908;

								{	/* Bdb/spread_obj.scm 15 */

									{	/* Bdb/spread_obj.scm 15 */
										obj_t BgL_locationz00_1910;

										BgL_locationz00_1910 = BBOOL(((bool_t) 0));
										{	/* Bdb/spread_obj.scm 15 */

											BgL_arg1773z00_1908 =
												BGl_readz00zz__readerz00(BgL_cportz00_1905,
												BgL_locationz00_1910);
										}
									}
								}
								{	/* Bdb/spread_obj.scm 15 */
									int BgL_tmpz00_1985;

									BgL_tmpz00_1985 = (int) (BgL_iz00_1906);
									CNST_TABLE_SET(BgL_tmpz00_1985, BgL_arg1773z00_1908);
							}}
							{	/* Bdb/spread_obj.scm 15 */
								int BgL_auxz00_1911;

								BgL_auxz00_1911 = (int) ((BgL_iz00_1906 - 1L));
								{
									long BgL_iz00_1990;

									BgL_iz00_1990 = (long) (BgL_auxz00_1911);
									BgL_iz00_1906 = BgL_iz00_1990;
									goto BgL_loopz00_1907;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzbdb_spreadzd2objzd2(void)
	{
		{	/* Bdb/spread_obj.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzbdb_spreadzd2objzd2(void)
	{
		{	/* Bdb/spread_obj.scm 15 */
			return BUNSPEC;
		}

	}



/* bdb-spread-obj! */
	BGL_EXPORTED_DEF obj_t BGl_bdbzd2spreadzd2objz12z12zzbdb_spreadzd2objzd2(obj_t
		BgL_globalsz00_3)
	{
		{	/* Bdb/spread_obj.scm 30 */
			{	/* Bdb/spread_obj.scm 31 */
				obj_t BgL_list1306z00_1374;

				{	/* Bdb/spread_obj.scm 31 */
					obj_t BgL_arg1307z00_1375;

					{	/* Bdb/spread_obj.scm 31 */
						obj_t BgL_arg1308z00_1376;

						BgL_arg1308z00_1376 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1307z00_1375 =
							MAKE_YOUNG_PAIR(BGl_string1742z00zzbdb_spreadzd2objzd2,
							BgL_arg1308z00_1376);
					}
					BgL_list1306z00_1374 =
						MAKE_YOUNG_PAIR(BGl_string1743z00zzbdb_spreadzd2objzd2,
						BgL_arg1307z00_1375);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1306z00_1374);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1742z00zzbdb_spreadzd2objzd2;
			{	/* Bdb/spread_obj.scm 31 */
				obj_t BgL_g1106z00_1377;

				BgL_g1106z00_1377 = BNIL;
				{
					obj_t BgL_hooksz00_1380;
					obj_t BgL_hnamesz00_1381;

					BgL_hooksz00_1380 = BgL_g1106z00_1377;
					BgL_hnamesz00_1381 = BNIL;
				BgL_zc3z04anonymousza31309ze3z87_1382:
					if (NULLP(BgL_hooksz00_1380))
						{	/* Bdb/spread_obj.scm 31 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Bdb/spread_obj.scm 31 */
							bool_t BgL_test1811z00_2003;

							{	/* Bdb/spread_obj.scm 31 */
								obj_t BgL_fun1317z00_1389;

								BgL_fun1317z00_1389 = CAR(((obj_t) BgL_hooksz00_1380));
								BgL_test1811z00_2003 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1317z00_1389));
							}
							if (BgL_test1811z00_2003)
								{	/* Bdb/spread_obj.scm 31 */
									obj_t BgL_arg1314z00_1386;
									obj_t BgL_arg1315z00_1387;

									BgL_arg1314z00_1386 = CDR(((obj_t) BgL_hooksz00_1380));
									BgL_arg1315z00_1387 = CDR(((obj_t) BgL_hnamesz00_1381));
									{
										obj_t BgL_hnamesz00_2015;
										obj_t BgL_hooksz00_2014;

										BgL_hooksz00_2014 = BgL_arg1314z00_1386;
										BgL_hnamesz00_2015 = BgL_arg1315z00_1387;
										BgL_hnamesz00_1381 = BgL_hnamesz00_2015;
										BgL_hooksz00_1380 = BgL_hooksz00_2014;
										goto BgL_zc3z04anonymousza31309ze3z87_1382;
									}
								}
							else
								{	/* Bdb/spread_obj.scm 31 */
									obj_t BgL_arg1316z00_1388;

									BgL_arg1316z00_1388 = CAR(((obj_t) BgL_hnamesz00_1381));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1742z00zzbdb_spreadzd2objzd2,
										BGl_string1744z00zzbdb_spreadzd2objzd2,
										BgL_arg1316z00_1388);
								}
						}
				}
			}
			{
				obj_t BgL_l1236z00_1393;

				BgL_l1236z00_1393 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31319ze3z87_1394:
				if (PAIRP(BgL_l1236z00_1393))
					{	/* Bdb/spread_obj.scm 32 */
						BGl_spreadzd2objzd2funz12z12zzbdb_spreadzd2objzd2(CAR
							(BgL_l1236z00_1393));
						{
							obj_t BgL_l1236z00_2023;

							BgL_l1236z00_2023 = CDR(BgL_l1236z00_1393);
							BgL_l1236z00_1393 = BgL_l1236z00_2023;
							goto BgL_zc3z04anonymousza31319ze3z87_1394;
						}
					}
				else
					{	/* Bdb/spread_obj.scm 32 */
						((bool_t) 1);
					}
			}
			if (
				((long) CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) > 0L))
				{	/* Bdb/spread_obj.scm 33 */
					{	/* Bdb/spread_obj.scm 33 */
						obj_t BgL_port1238z00_1401;

						{	/* Bdb/spread_obj.scm 33 */
							obj_t BgL_tmpz00_2028;

							BgL_tmpz00_2028 = BGL_CURRENT_DYNAMIC_ENV();
							BgL_port1238z00_1401 =
								BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_2028);
						}
						bgl_display_obj(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
							BgL_port1238z00_1401);
						bgl_display_string(BGl_string1745z00zzbdb_spreadzd2objzd2,
							BgL_port1238z00_1401);
						{	/* Bdb/spread_obj.scm 33 */
							obj_t BgL_arg1325z00_1402;

							{	/* Bdb/spread_obj.scm 33 */
								bool_t BgL_test1814z00_2033;

								if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
									(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
									{	/* Bdb/spread_obj.scm 33 */
										if (INTEGERP
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
											{	/* Bdb/spread_obj.scm 33 */
												BgL_test1814z00_2033 =
													(
													(long)
													CINT
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
													> 1L);
											}
										else
											{	/* Bdb/spread_obj.scm 33 */
												BgL_test1814z00_2033 =
													BGl_2ze3ze3zz__r4_numbers_6_5z00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
													BINT(1L));
											}
									}
								else
									{	/* Bdb/spread_obj.scm 33 */
										BgL_test1814z00_2033 = ((bool_t) 0);
									}
								if (BgL_test1814z00_2033)
									{	/* Bdb/spread_obj.scm 33 */
										BgL_arg1325z00_1402 =
											BGl_string1746z00zzbdb_spreadzd2objzd2;
									}
								else
									{	/* Bdb/spread_obj.scm 33 */
										BgL_arg1325z00_1402 =
											BGl_string1747z00zzbdb_spreadzd2objzd2;
									}
							}
							bgl_display_obj(BgL_arg1325z00_1402, BgL_port1238z00_1401);
						}
						bgl_display_string(BGl_string1748z00zzbdb_spreadzd2objzd2,
							BgL_port1238z00_1401);
						bgl_display_char(((unsigned char) 10), BgL_port1238z00_1401);
					}
					{	/* Bdb/spread_obj.scm 33 */
						obj_t BgL_list1328z00_1406;

						BgL_list1328z00_1406 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
						BGL_TAIL return BGl_exitz00zz__errorz00(BgL_list1328z00_1406);
					}
				}
			else
				{	/* Bdb/spread_obj.scm 33 */
					obj_t BgL_g1108z00_1407;

					BgL_g1108z00_1407 = BNIL;
					{
						obj_t BgL_hooksz00_1410;
						obj_t BgL_hnamesz00_1411;

						BgL_hooksz00_1410 = BgL_g1108z00_1407;
						BgL_hnamesz00_1411 = BNIL;
					BgL_zc3z04anonymousza31329ze3z87_1412:
						if (NULLP(BgL_hooksz00_1410))
							{	/* Bdb/spread_obj.scm 33 */
								return BgL_globalsz00_3;
							}
						else
							{	/* Bdb/spread_obj.scm 33 */
								bool_t BgL_test1818z00_2050;

								{	/* Bdb/spread_obj.scm 33 */
									obj_t BgL_fun1340z00_1419;

									BgL_fun1340z00_1419 = CAR(((obj_t) BgL_hooksz00_1410));
									BgL_test1818z00_2050 =
										CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1340z00_1419));
								}
								if (BgL_test1818z00_2050)
									{	/* Bdb/spread_obj.scm 33 */
										obj_t BgL_arg1333z00_1416;
										obj_t BgL_arg1335z00_1417;

										BgL_arg1333z00_1416 = CDR(((obj_t) BgL_hooksz00_1410));
										BgL_arg1335z00_1417 = CDR(((obj_t) BgL_hnamesz00_1411));
										{
											obj_t BgL_hnamesz00_2062;
											obj_t BgL_hooksz00_2061;

											BgL_hooksz00_2061 = BgL_arg1333z00_1416;
											BgL_hnamesz00_2062 = BgL_arg1335z00_1417;
											BgL_hnamesz00_1411 = BgL_hnamesz00_2062;
											BgL_hooksz00_1410 = BgL_hooksz00_2061;
											goto BgL_zc3z04anonymousza31329ze3z87_1412;
										}
									}
								else
									{	/* Bdb/spread_obj.scm 33 */
										obj_t BgL_arg1339z00_1418;

										BgL_arg1339z00_1418 = CAR(((obj_t) BgL_hnamesz00_1411));
										return
											BGl_internalzd2errorzd2zztools_errorz00
											(BGl_za2currentzd2passza2zd2zzengine_passz00,
											BGl_string1749z00zzbdb_spreadzd2objzd2,
											BgL_arg1339z00_1418);
									}
							}
					}
				}
		}

	}



/* &bdb-spread-obj! */
	obj_t BGl_z62bdbzd2spreadzd2objz12z70zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1844, obj_t BgL_globalsz00_1845)
	{
		{	/* Bdb/spread_obj.scm 30 */
			return
				BGl_bdbzd2spreadzd2objz12z12zzbdb_spreadzd2objzd2(BgL_globalsz00_1845);
		}

	}



/* spread-obj-fun! */
	obj_t BGl_spreadzd2objzd2funz12z12zzbdb_spreadzd2objzd2(obj_t BgL_varz00_4)
	{
		{	/* Bdb/spread_obj.scm 38 */
			{	/* Bdb/spread_obj.scm 39 */
				obj_t BgL_arg1342z00_1422;

				BgL_arg1342z00_1422 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_4)))->BgL_idz00);
				BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1342z00_1422);
			}
			{	/* Bdb/spread_obj.scm 40 */
				BgL_valuez00_bglt BgL_funz00_1423;

				BgL_funz00_1423 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_varz00_4)))->BgL_valuez00);
				{	/* Bdb/spread_obj.scm 40 */
					obj_t BgL_bodyz00_1424;

					BgL_bodyz00_1424 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_funz00_1423)))->BgL_bodyz00);
					{	/* Bdb/spread_obj.scm 41 */
						BgL_typez00_bglt BgL_typez00_1425;

						BgL_typez00_1425 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_varz00_4)))->BgL_typez00);
						{	/* Bdb/spread_obj.scm 42 */
							obj_t BgL_argsz00_1426;

							BgL_argsz00_1426 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt) BgL_funz00_1423)))->BgL_argsz00);
							{	/* Bdb/spread_obj.scm 43 */

								if (
									(((obj_t) BgL_typez00_1425) == BGl_za2_za2z00zztype_cachez00))
									{	/* Bdb/spread_obj.scm 45 */
										BgL_typez00_bglt BgL_vz00_1775;

										BgL_vz00_1775 =
											((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
										((((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt) BgL_varz00_4)))->
												BgL_typez00) =
											((BgL_typez00_bglt) BgL_vz00_1775), BUNSPEC);
									}
								else
									{	/* Bdb/spread_obj.scm 44 */
										BFALSE;
									}
								{
									obj_t BgL_l1239z00_1428;

									BgL_l1239z00_1428 = BgL_argsz00_1426;
								BgL_zc3z04anonymousza31343ze3z87_1429:
									if (PAIRP(BgL_l1239z00_1428))
										{	/* Bdb/spread_obj.scm 46 */
											{	/* Bdb/spread_obj.scm 47 */
												obj_t BgL_argsz00_1431;

												BgL_argsz00_1431 = CAR(BgL_l1239z00_1428);
												{	/* Bdb/spread_obj.scm 47 */
													bool_t BgL_test1822z00_2087;

													{	/* Bdb/spread_obj.scm 47 */
														BgL_typez00_bglt BgL_arg1348z00_1434;

														BgL_arg1348z00_1434 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt) BgL_argsz00_1431)))->
															BgL_typez00);
														BgL_test1822z00_2087 =
															(((obj_t) BgL_arg1348z00_1434) ==
															BGl_za2_za2z00zztype_cachez00);
													}
													if (BgL_test1822z00_2087)
														{	/* Bdb/spread_obj.scm 48 */
															BgL_typez00_bglt BgL_vz00_1779;

															BgL_vz00_1779 =
																((BgL_typez00_bglt)
																BGl_za2objza2z00zztype_cachez00);
															((((BgL_variablez00_bglt)
																		COBJECT(((BgL_variablez00_bglt)
																				BgL_argsz00_1431)))->BgL_typez00) =
																((BgL_typez00_bglt) BgL_vz00_1779), BUNSPEC);
														}
													else
														{	/* Bdb/spread_obj.scm 47 */
															BFALSE;
														}
												}
											}
											{
												obj_t BgL_l1239z00_2095;

												BgL_l1239z00_2095 = CDR(BgL_l1239z00_1428);
												BgL_l1239z00_1428 = BgL_l1239z00_2095;
												goto BgL_zc3z04anonymousza31343ze3z87_1429;
											}
										}
									else
										{	/* Bdb/spread_obj.scm 46 */
											((bool_t) 1);
										}
								}
								if (
									((long) CINT(BGl_za2bdbzd2debugza2zd2zzengine_paramz00) >=
										3L))
									{	/* Bdb/spread_obj.scm 50 */
										BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
											((BgL_nodez00_bglt) BgL_bodyz00_1424));
									}
								else
									{	/* Bdb/spread_obj.scm 50 */
										BFALSE;
									}
								return BGl_leavezd2functionzd2zztools_errorz00();
							}
						}
					}
				}
			}
		}

	}



/* spread-obj-node*! */
	obj_t BGl_spreadzd2objzd2nodeza2z12zb0zzbdb_spreadzd2objzd2(obj_t
		BgL_nodeza2za2_24)
	{
		{	/* Bdb/spread_obj.scm 204 */
		BGl_spreadzd2objzd2nodeza2z12zb0zzbdb_spreadzd2objzd2:
			if (NULLP(BgL_nodeza2za2_24))
				{	/* Bdb/spread_obj.scm 205 */
					return CNST_TABLE_REF(1);
				}
			else
				{	/* Bdb/spread_obj.scm 205 */
					{	/* Bdb/spread_obj.scm 208 */
						obj_t BgL_arg1361z00_1439;

						BgL_arg1361z00_1439 = CAR(((obj_t) BgL_nodeza2za2_24));
						BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
							((BgL_nodez00_bglt) BgL_arg1361z00_1439));
					}
					{	/* Bdb/spread_obj.scm 209 */
						obj_t BgL_arg1364z00_1440;

						BgL_arg1364z00_1440 = CDR(((obj_t) BgL_nodeza2za2_24));
						{
							obj_t BgL_nodeza2za2_2112;

							BgL_nodeza2za2_2112 = BgL_arg1364z00_1440;
							BgL_nodeza2za2_24 = BgL_nodeza2za2_2112;
							goto BGl_spreadzd2objzd2nodeza2z12zb0zzbdb_spreadzd2objzd2;
						}
					}
				}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzbdb_spreadzd2objzd2(void)
	{
		{	/* Bdb/spread_obj.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzbdb_spreadzd2objzd2(void)
	{
		{	/* Bdb/spread_obj.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_proc1750z00zzbdb_spreadzd2objzd2, BGl_nodez00zzast_nodez00,
				BGl_string1751z00zzbdb_spreadzd2objzd2);
		}

	}



/* &spread-obj-node!1250 */
	obj_t BGl_z62spreadzd2objzd2nodez121250z70zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1847, obj_t BgL_nodez00_1848)
	{
		{	/* Bdb/spread_obj.scm 57 */
			return ((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_1848));
		}

	}



/* spread-obj-node! */
	obj_t BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(BgL_nodez00_bglt
		BgL_nodez00_5)
	{
		{	/* Bdb/spread_obj.scm 57 */
			{	/* Bdb/spread_obj.scm 57 */
				obj_t BgL_method1251z00_1445;

				{	/* Bdb/spread_obj.scm 57 */
					obj_t BgL_res1741z00_1814;

					{	/* Bdb/spread_obj.scm 57 */
						long BgL_objzd2classzd2numz00_1785;

						BgL_objzd2classzd2numz00_1785 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_5));
						{	/* Bdb/spread_obj.scm 57 */
							obj_t BgL_arg1811z00_1786;

							BgL_arg1811z00_1786 =
								PROCEDURE_REF
								(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
								(int) (1L));
							{	/* Bdb/spread_obj.scm 57 */
								int BgL_offsetz00_1789;

								BgL_offsetz00_1789 = (int) (BgL_objzd2classzd2numz00_1785);
								{	/* Bdb/spread_obj.scm 57 */
									long BgL_offsetz00_1790;

									BgL_offsetz00_1790 =
										((long) (BgL_offsetz00_1789) - OBJECT_TYPE);
									{	/* Bdb/spread_obj.scm 57 */
										long BgL_modz00_1791;

										BgL_modz00_1791 =
											(BgL_offsetz00_1790 >> (int) ((long) ((int) (4L))));
										{	/* Bdb/spread_obj.scm 57 */
											long BgL_restz00_1793;

											BgL_restz00_1793 =
												(BgL_offsetz00_1790 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Bdb/spread_obj.scm 57 */

												{	/* Bdb/spread_obj.scm 57 */
													obj_t BgL_bucketz00_1795;

													BgL_bucketz00_1795 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_1786), BgL_modz00_1791);
													BgL_res1741z00_1814 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_1795), BgL_restz00_1793);
					}}}}}}}}
					BgL_method1251z00_1445 = BgL_res1741z00_1814;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1251z00_1445, ((obj_t) BgL_nodez00_5));
			}
		}

	}



/* &spread-obj-node! */
	obj_t BGl_z62spreadzd2objzd2nodez12z70zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1849, obj_t BgL_nodez00_1850)
	{
		{	/* Bdb/spread_obj.scm 57 */
			return
				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				((BgL_nodez00_bglt) BgL_nodez00_1850));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzbdb_spreadzd2objzd2(void)
	{
		{	/* Bdb/spread_obj.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_sequencez00zzast_nodez00, BGl_proc1752z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_syncz00zzast_nodez00, BGl_proc1754z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_appz00zzast_nodez00, BGl_proc1755z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc1756z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_funcallz00zzast_nodez00, BGl_proc1757z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_externz00zzast_nodez00, BGl_proc1758z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_castz00zzast_nodez00, BGl_proc1759z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_setqz00zzast_nodez00, BGl_proc1760z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_conditionalz00zzast_nodez00, BGl_proc1761z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_failz00zzast_nodez00, BGl_proc1762z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_switchz00zzast_nodez00, BGl_proc1763z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc1764z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc1765z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc1766z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc1767z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1768z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1769z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_spreadzd2objzd2nodez12zd2envzc0zzbdb_spreadzd2objzd2,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1770z00zzbdb_spreadzd2objzd2,
				BGl_string1753z00zzbdb_spreadzd2objzd2);
		}

	}



/* &spread-obj-node!-box1287 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2box1287za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1869, obj_t BgL_nodez00_1870)
	{
		{	/* Bdb/spread_obj.scm 196 */
			{	/* Bdb/spread_obj.scm 198 */
				BgL_varz00_bglt BgL_arg1573z00_1918;

				BgL_arg1573z00_1918 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_1870)))->BgL_varz00);
				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
					((BgL_nodez00_bglt) BgL_arg1573z00_1918));
			}
			return
				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_1870)))->BgL_valuez00));
		}

	}



/* &spread-obj-node!-box1285 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2box1285za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1871, obj_t BgL_nodez00_1872)
	{
		{	/* Bdb/spread_obj.scm 190 */
			{	/* Bdb/spread_obj.scm 191 */
				BgL_varz00_bglt BgL_arg1571z00_1920;

				BgL_arg1571z00_1920 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_1872)))->BgL_varz00);
				return
					BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
					((BgL_nodez00_bglt) BgL_arg1571z00_1920));
			}
		}

	}



/* &spread-obj-node!-mak1283 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2mak1283za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1873, obj_t BgL_nodez00_1874)
	{
		{	/* Bdb/spread_obj.scm 184 */
			return
				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_1874)))->BgL_valuez00));
		}

	}



/* &spread-obj-node!-jum1281 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2jum1281za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1875, obj_t BgL_nodez00_1876)
	{
		{	/* Bdb/spread_obj.scm 175 */
			{
				BgL_jumpzd2exzd2itz00_bglt BgL_auxz00_2180;

				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_1876)))->
						BgL_exitz00));
				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(((
							(BgL_jumpzd2exzd2itz00_bglt) COBJECT(((BgL_jumpzd2exzd2itz00_bglt)
									BgL_nodez00_1876)))->BgL_valuez00));
				BgL_auxz00_2180 = ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_1876);
				return ((obj_t) BgL_auxz00_2180);
			}
		}

	}



/* &spread-obj-node!-set1279 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2set1279za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1877, obj_t BgL_nodez00_1878)
	{
		{	/* Bdb/spread_obj.scm 168 */
			BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_1878)))->BgL_onexitz00));
			return
				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_1878)))->BgL_bodyz00));
		}

	}



/* &spread-obj-node!-let1277 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2let1277za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1879, obj_t BgL_nodez00_1880)
	{
		{	/* Bdb/spread_obj.scm 154 */
			{	/* Bdb/spread_obj.scm 156 */
				obj_t BgL_g1249z00_1925;

				BgL_g1249z00_1925 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_1880)))->BgL_bindingsz00);
				{
					obj_t BgL_l1247z00_1927;

					BgL_l1247z00_1927 = BgL_g1249z00_1925;
				BgL_zc3z04anonymousza31514ze3z87_1926:
					if (PAIRP(BgL_l1247z00_1927))
						{	/* Bdb/spread_obj.scm 156 */
							{	/* Bdb/spread_obj.scm 160 */
								obj_t BgL_bindingz00_1928;

								BgL_bindingz00_1928 = CAR(BgL_l1247z00_1927);
								{	/* Bdb/spread_obj.scm 157 */
									obj_t BgL_variablez00_1929;

									BgL_variablez00_1929 = CAR(((obj_t) BgL_bindingz00_1928));
									{	/* Bdb/spread_obj.scm 158 */
										bool_t BgL_test1827z00_2202;

										{	/* Bdb/spread_obj.scm 158 */
											bool_t BgL_test1828z00_2203;

											{	/* Bdb/spread_obj.scm 158 */
												BgL_typez00_bglt BgL_arg1540z00_1930;

												BgL_arg1540z00_1930 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_variablez00_1929)))->
													BgL_typez00);
												BgL_test1828z00_2203 =
													(((obj_t) BgL_arg1540z00_1930) ==
													BGl_za2_za2z00zztype_cachez00);
											}
											if (BgL_test1828z00_2203)
												{	/* Bdb/spread_obj.scm 158 */
													BgL_test1827z00_2202 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt)
																		BgL_variablez00_1929))))->BgL_userzf3zf3);
												}
											else
												{	/* Bdb/spread_obj.scm 158 */
													BgL_test1827z00_2202 = ((bool_t) 0);
												}
										}
										if (BgL_test1827z00_2202)
											{	/* Bdb/spread_obj.scm 160 */
												BgL_typez00_bglt BgL_vz00_1931;

												BgL_vz00_1931 =
													((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
												((((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	BgL_variablez00_1929)))->BgL_typez00) =
													((BgL_typez00_bglt) BgL_vz00_1931), BUNSPEC);
											}
										else
											{	/* Bdb/spread_obj.scm 158 */
												BFALSE;
											}
									}
								}
								{	/* Bdb/spread_obj.scm 161 */
									obj_t BgL_arg1544z00_1932;

									BgL_arg1544z00_1932 = CDR(((obj_t) BgL_bindingz00_1928));
									BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
										((BgL_nodez00_bglt) BgL_arg1544z00_1932));
								}
							}
							{
								obj_t BgL_l1247z00_2218;

								BgL_l1247z00_2218 = CDR(BgL_l1247z00_1927);
								BgL_l1247z00_1927 = BgL_l1247z00_2218;
								goto BgL_zc3z04anonymousza31514ze3z87_1926;
							}
						}
					else
						{	/* Bdb/spread_obj.scm 156 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_1880)))->BgL_bodyz00));
		}

	}



/* &spread-obj-node!-let1275 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2let1275za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1881, obj_t BgL_nodez00_1882)
	{
		{	/* Bdb/spread_obj.scm 146 */
			{	/* Bdb/spread_obj.scm 148 */
				obj_t BgL_g1246z00_1934;

				BgL_g1246z00_1934 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_1882)))->BgL_localsz00);
				{
					obj_t BgL_l1244z00_1936;

					BgL_l1244z00_1936 = BgL_g1246z00_1934;
				BgL_zc3z04anonymousza31490ze3z87_1935:
					if (PAIRP(BgL_l1244z00_1936))
						{	/* Bdb/spread_obj.scm 148 */
							BGl_spreadzd2objzd2funz12z12zzbdb_spreadzd2objzd2(CAR
								(BgL_l1244z00_1936));
							{
								obj_t BgL_l1244z00_2229;

								BgL_l1244z00_2229 = CDR(BgL_l1244z00_1936);
								BgL_l1244z00_1936 = BgL_l1244z00_2229;
								goto BgL_zc3z04anonymousza31490ze3z87_1935;
							}
						}
					else
						{	/* Bdb/spread_obj.scm 148 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_1882)))->BgL_bodyz00));
		}

	}



/* &spread-obj-node!-swi1273 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2swi1273za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1883, obj_t BgL_nodez00_1884)
	{
		{	/* Bdb/spread_obj.scm 136 */
			{	/* Bdb/spread_obj.scm 137 */
				bool_t BgL_tmpz00_2234;

				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
					(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_1884)))->BgL_testz00));
				{	/* Bdb/spread_obj.scm 139 */
					obj_t BgL_g1243z00_1938;

					BgL_g1243z00_1938 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_1884)))->BgL_clausesz00);
					{
						obj_t BgL_l1241z00_1940;

						BgL_l1241z00_1940 = BgL_g1243z00_1938;
					BgL_zc3z04anonymousza31474ze3z87_1939:
						if (PAIRP(BgL_l1241z00_1940))
							{	/* Bdb/spread_obj.scm 139 */
								{	/* Bdb/spread_obj.scm 140 */
									obj_t BgL_clausez00_1941;

									BgL_clausez00_1941 = CAR(BgL_l1241z00_1940);
									{	/* Bdb/spread_obj.scm 140 */
										obj_t BgL_arg1485z00_1942;

										BgL_arg1485z00_1942 = CDR(((obj_t) BgL_clausez00_1941));
										BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
											((BgL_nodez00_bglt) BgL_arg1485z00_1942));
									}
								}
								{
									obj_t BgL_l1241z00_2247;

									BgL_l1241z00_2247 = CDR(BgL_l1241z00_1940);
									BgL_l1241z00_1940 = BgL_l1241z00_2247;
									goto BgL_zc3z04anonymousza31474ze3z87_1939;
								}
							}
						else
							{	/* Bdb/spread_obj.scm 139 */
								BgL_tmpz00_2234 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_2234);
			}
		}

	}



/* &spread-obj-node!-fai1271 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2fai1271za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1885, obj_t BgL_nodez00_1886)
	{
		{	/* Bdb/spread_obj.scm 127 */
			BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_1886)))->BgL_procz00));
			BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_1886)))->BgL_msgz00));
			return
				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_1886)))->BgL_objz00));
		}

	}



/* &spread-obj-node!-con1269 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2con1269za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1887, obj_t BgL_nodez00_1888)
	{
		{	/* Bdb/spread_obj.scm 118 */
			BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_1888)))->BgL_testz00));
			BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_1888)))->BgL_truez00));
			return
				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_1888)))->BgL_falsez00));
		}

	}



/* &spread-obj-node!-set1267 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2set1267za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1889, obj_t BgL_nodez00_1890)
	{
		{	/* Bdb/spread_obj.scm 112 */
			return
				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_1890)))->BgL_valuez00));
		}

	}



/* &spread-obj-node!-cas1265 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2cas1265za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1891, obj_t BgL_nodez00_1892)
	{
		{	/* Bdb/spread_obj.scm 106 */
			return
				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_1892)))->BgL_argz00));
		}

	}



/* &spread-obj-node!-ext1263 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2ext1263za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1893, obj_t BgL_nodez00_1894)
	{
		{	/* Bdb/spread_obj.scm 100 */
			return
				BGl_spreadzd2objzd2nodeza2z12zb0zzbdb_spreadzd2objzd2(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_1894)))->BgL_exprza2za2));
		}

	}



/* &spread-obj-node!-fun1261 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2fun1261za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1895, obj_t BgL_nodez00_1896)
	{
		{	/* Bdb/spread_obj.scm 92 */
			BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_1896)))->BgL_funz00));
			return
				BGl_spreadzd2objzd2nodeza2z12zb0zzbdb_spreadzd2objzd2(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_1896)))->BgL_argsz00));
		}

	}



/* &spread-obj-node!-app1259 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2app1259za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1897, obj_t BgL_nodez00_1898)
	{
		{	/* Bdb/spread_obj.scm 84 */
			BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_1898)))->BgL_funz00));
			return
				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_1898)))->BgL_argz00));
		}

	}



/* &spread-obj-node!-app1257 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2app1257za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1899, obj_t BgL_nodez00_1900)
	{
		{	/* Bdb/spread_obj.scm 78 */
			return
				BGl_spreadzd2objzd2nodeza2z12zb0zzbdb_spreadzd2objzd2(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_1900)))->BgL_argsz00));
		}

	}



/* &spread-obj-node!-syn1255 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2syn1255za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1901, obj_t BgL_nodez00_1902)
	{
		{	/* Bdb/spread_obj.scm 69 */
			BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_1902)))->BgL_mutexz00));
			BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_1902)))->BgL_prelockz00));
			return
				BGl_spreadzd2objzd2nodez12z12zzbdb_spreadzd2objzd2(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_1902)))->BgL_bodyz00));
		}

	}



/* &spread-obj-node!-seq1253 */
	obj_t BGl_z62spreadzd2objzd2nodez12zd2seq1253za2zzbdb_spreadzd2objzd2(obj_t
		BgL_envz00_1903, obj_t BgL_nodez00_1904)
	{
		{	/* Bdb/spread_obj.scm 63 */
			return
				BGl_spreadzd2objzd2nodeza2z12zb0zzbdb_spreadzd2objzd2(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_1904)))->BgL_nodesz00));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzbdb_spreadzd2objzd2(void)
	{
		{	/* Bdb/spread_obj.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1771z00zzbdb_spreadzd2objzd2));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1771z00zzbdb_spreadzd2objzd2));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1771z00zzbdb_spreadzd2objzd2));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1771z00zzbdb_spreadzd2objzd2));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1771z00zzbdb_spreadzd2objzd2));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1771z00zzbdb_spreadzd2objzd2));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1771z00zzbdb_spreadzd2objzd2));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1771z00zzbdb_spreadzd2objzd2));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1771z00zzbdb_spreadzd2objzd2));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1771z00zzbdb_spreadzd2objzd2));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1771z00zzbdb_spreadzd2objzd2));
			return
				BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1771z00zzbdb_spreadzd2objzd2));
		}

	}

#ifdef __cplusplus
}
#endif
