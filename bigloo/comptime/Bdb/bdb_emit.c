/*===========================================================================*/
/*   (Bdb/bdb_emit.scm)                                                      */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Bdb/bdb_emit.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_BDB_EMIT_TYPE_DEFINITIONS
#define BGL_BDB_EMIT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_tclassz00_bgl
	{
		obj_t BgL_itszd2superzd2;
		obj_t BgL_slotsz00;
		struct BgL_globalz00_bgl *BgL_holderz00;
		obj_t BgL_wideningz00;
		long BgL_depthz00;
		bool_t BgL_finalzf3zf3;
		obj_t BgL_constructorz00;
		obj_t BgL_virtualzd2slotszd2numberz00;
		bool_t BgL_abstractzf3zf3;
		obj_t BgL_widezd2typezd2;
		obj_t BgL_subclassesz00;
	}                *BgL_tclassz00_bglt;


#endif													// BGL_BDB_EMIT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62bdbz12zd2appzd2ly1338z70zzbdb_emitz00(obj_t, obj_t);
	static obj_t BGl_bdbzd2globalzd2svarz12z12zzbdb_emitz00(obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_za2intza2z00zztype_cachez00;
	static obj_t BGl_z62bdbz12zd2letzd2fun1326z70zzbdb_emitz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzbdb_emitz00 = BUNSPEC;
	static obj_t BGl_z62bdbz12zd2extern1314za2zzbdb_emitz00(obj_t, obj_t);
	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static obj_t BGl_z62bdbz12zd2switch1324za2zzbdb_emitz00(obj_t, obj_t);
	extern obj_t BGl_za2srczd2filesza2zd2zzengine_paramz00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_za2czd2portza2zd2zzbdb_emitz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_getzd2toplevelzd2unitz00zzmodule_includez00(void);
	static obj_t BGl_z62bdbz12zd2app1342za2zzbdb_emitz00(obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzbdb_emitz00(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_forzd2eachzd2globalz12z12zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t bigloo_module_mangle(obj_t, obj_t);
	static obj_t BGl_z62bdbz12zd2jumpzd2exzd2it1332za2zzbdb_emitz00(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzbdb_emitz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern obj_t BGl_leavezd2functionzd2zztools_errorz00(void);
	static obj_t BGl_z62emitzd2bdbzd2infoz62zzbdb_emitz00(obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzbdb_emitz00(void);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	static bool_t BGl_bdbzd2localzd2variablezf3zf3zzbdb_emitz00(obj_t);
	extern obj_t BGl_za2stringza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_bdbzd2emitzd2localzd2infoz12zc0zzbdb_emitz00(obj_t);
	static obj_t BGl_methodzd2initzd2zzbdb_emitz00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	extern obj_t BGl_za2realza2z00zztype_cachez00;
	static obj_t BGl_z62bdbz12zd2letzd2var1328z70zzbdb_emitz00(obj_t, obj_t);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	static obj_t BGl_bdbza2z12zb0zzbdb_emitz00(obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62bdbz12zd2funcall1340za2zzbdb_emitz00(obj_t, obj_t);
	static bool_t BGl_emitzd2bdbzd2classzd2typeszd2zzbdb_emitz00(obj_t);
	extern obj_t BGl_svarz00zzast_varz00;
	extern obj_t BGl_za2modulezd2clauseza2zd2zzmodule_modulez00;
	static obj_t BGl_z62zc3z04anonymousza31516ze3ze5zzbdb_emitz00(obj_t, obj_t);
	extern obj_t BGl_za2moduleza2z00zzmodule_modulez00;
	BGL_EXPORTED_DECL obj_t BGl_emitzd2bdbzd2infoz00zzbdb_emitz00(obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzbdb_emitz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_cplibz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_configurez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_includez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_modulez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_unitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_identz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_slotsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_locationz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_miscz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	extern obj_t BGl_findzd2locationzd2zztools_locationz00(obj_t);
	extern obj_t BGl_enterzd2functionzd2zztools_errorz00(obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62bdbz121307z70zzbdb_emitz00(obj_t, obj_t);
	BGL_IMPORT bool_t BGl_numberzf3zf3zz__r4_numbers_6_5z00(obj_t);
	static obj_t BGl_z62bdbz12z70zzbdb_emitz00(obj_t, obj_t);
	static obj_t BGl_bdbzd2globalzd2sfunz12z12zzbdb_emitz00(obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzbdb_emitz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzbdb_emitz00(void);
	static obj_t BGl_bdbz12z12zzbdb_emitz00(BgL_nodez00_bglt);
	static obj_t BGl_importedzd2moduleszd2initz00zzbdb_emitz00(void);
	static obj_t BGl_z62bdbz12zd2boxzd2setz121336z62zzbdb_emitz00(obj_t, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzbdb_emitz00(void);
	static obj_t BGl_z62bdbz12zd2makezd2box1334z70zzbdb_emitz00(obj_t, obj_t);
	static obj_t BGl_z62bdbz12zd2setzd2exzd2it1330za2zzbdb_emitz00(obj_t, obj_t);
	extern obj_t BGl_unitzd2initializa7erzd2idza7zzast_unitz00(obj_t);
	static obj_t BGl_z62bdbz12zd2conditional1320za2zzbdb_emitz00(obj_t, obj_t);
	static obj_t BGl_z62bdbz12zd2cast1316za2zzbdb_emitz00(obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62bdbz12zd2setq1318za2zzbdb_emitz00(obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t
		BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(BgL_variablez00_bglt);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_z62bdbz12zd2sync1312za2zzbdb_emitz00(obj_t, obj_t);
	static obj_t BGl_z62bdbz12zd2fail1322za2zzbdb_emitz00(obj_t, obj_t);
	static obj_t BGl_bdbzd2sfunz12zc0zzbdb_emitz00(BgL_valuez00_bglt);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_getzd2classzd2listz00zzobject_classz00(void);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_z62bdbz12zd2sequence1310za2zzbdb_emitz00(obj_t, obj_t);
	extern obj_t BGl_za2charza2z00zztype_cachez00;
	static obj_t __cnst[6];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_emitzd2bdbzd2infozd2envzd2zzbdb_emitz00,
		BgL_bgl_za762emitza7d2bdbza7d21948za7,
		BGl_z62emitzd2bdbzd2infoz62zzbdb_emitz00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string1900z00zzbdb_emitz00,
		BgL_bgl_string1900za700za7za7b1949za7, "   {(char *)", 12);
	      DEFINE_STRING(BGl_string1901z00zzbdb_emitz00,
		BgL_bgl_string1901za700za7za7b1950za7, ", (char *)", 10);
	      DEFINE_STRING(BGl_string1902z00zzbdb_emitz00,
		BgL_bgl_string1902za700za7za7b1951za7, "},", 2);
	      DEFINE_STRING(BGl_string1903z00zzbdb_emitz00,
		BgL_bgl_string1903za700za7za7b1952za7,
		"   /* Module and source file identification */", 46);
	      DEFINE_STRING(BGl_string1904z00zzbdb_emitz00,
		BgL_bgl_string1904za700za7za7b1953za7, "   {\"", 5);
	      DEFINE_STRING(BGl_string1905z00zzbdb_emitz00,
		BgL_bgl_string1905za700za7za7b1954za7, "\", \"", 4);
	      DEFINE_STRING(BGl_string1906z00zzbdb_emitz00,
		BgL_bgl_string1906za700za7za7b1955za7, "\" },", 4);
	      DEFINE_STRING(BGl_string1907z00zzbdb_emitz00,
		BgL_bgl_string1907za700za7za7b1956za7, "\", 0 },", 7);
	      DEFINE_STRING(BGl_string1908z00zzbdb_emitz00,
		BgL_bgl_string1908za700za7za7b1957za7, "   { 0, (char *)", 16);
	      DEFINE_STRING(BGl_string1909z00zzbdb_emitz00,
		BgL_bgl_string1909za700za7za7b1958za7, "0", 1);
	      DEFINE_STRING(BGl_string1910z00zzbdb_emitz00,
		BgL_bgl_string1910za700za7za7b1959za7, " },", 3);
	      DEFINE_STRING(BGl_string1911z00zzbdb_emitz00,
		BgL_bgl_string1911za700za7za7b1960za7, "   /* Global functions */", 25);
	      DEFINE_STRING(BGl_string1912z00zzbdb_emitz00,
		BgL_bgl_string1912za700za7za7b1961za7, "   /* Global variables */", 25);
	      DEFINE_STRING(BGl_string1914z00zzbdb_emitz00,
		BgL_bgl_string1914za700za7za7b1962za7, "   {0, 0},", 10);
	      DEFINE_STRING(BGl_string1915z00zzbdb_emitz00,
		BgL_bgl_string1915za700za7za7b1963za7, "   0};\n", 7);
	      DEFINE_STRING(BGl_string1916z00zzbdb_emitz00,
		BgL_bgl_string1916za700za7za7b1964za7, "   /* Bigloo classes */", 23);
	      DEFINE_STRING(BGl_string1917z00zzbdb_emitz00,
		BgL_bgl_string1917za700za7za7b1965za7, " *\", 0 },", 9);
	      DEFINE_STRING(BGl_string1918z00zzbdb_emitz00,
		BgL_bgl_string1918za700za7za7b1966za7, "\", (char *)", 11);
	      DEFINE_STRING(BGl_string1919z00zzbdb_emitz00,
		BgL_bgl_string1919za700za7za7b1967za7, "\", 0},", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1913z00zzbdb_emitz00,
		BgL_bgl_za762za7c3za704anonymo1968za7,
		BGl_z62zc3z04anonymousza31516ze3ze5zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1920z00zzbdb_emitz00,
		BgL_bgl_string1920za700za7za7b1969za7, "   {", 4);
	      DEFINE_STRING(BGl_string1921z00zzbdb_emitz00,
		BgL_bgl_string1921za700za7za7b1970za7, ", \"", 3);
	      DEFINE_STRING(BGl_string1922z00zzbdb_emitz00,
		BgL_bgl_string1922za700za7za7b1971za7, "\"},", 3);
	      DEFINE_STRING(BGl_string1923z00zzbdb_emitz00,
		BgL_bgl_string1923za700za7za7b1972za7, "     {0, 0},", 12);
	      DEFINE_STRING(BGl_string1924z00zzbdb_emitz00,
		BgL_bgl_string1924za700za7za7b1973za7, "     {\"", 7);
	      DEFINE_STRING(BGl_string1926z00zzbdb_emitz00,
		BgL_bgl_string1926za700za7za7b1974za7, "bdb!1307", 8);
	      DEFINE_STRING(BGl_string1928z00zzbdb_emitz00,
		BgL_bgl_string1928za700za7za7b1975za7, "bdb!", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1925z00zzbdb_emitz00,
		BgL_bgl_za762bdbza7121307za7701976za7, BGl_z62bdbz121307z70zzbdb_emitz00,
		0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1927z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2sequ1977za7,
		BGl_z62bdbz12zd2sequence1310za2zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1929z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2sync1978za7,
		BGl_z62bdbz12zd2sync1312za2zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1930z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2exte1979za7,
		BGl_z62bdbz12zd2extern1314za2zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1931z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2cast1980za7,
		BGl_z62bdbz12zd2cast1316za2zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1932z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2setq1981za7,
		BGl_z62bdbz12zd2setq1318za2zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1933z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2cond1982za7,
		BGl_z62bdbz12zd2conditional1320za2zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1934z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2fail1983za7,
		BGl_z62bdbz12zd2fail1322za2zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1935z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2swit1984za7,
		BGl_z62bdbz12zd2switch1324za2zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1936z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2letza71985z00,
		BGl_z62bdbz12zd2letzd2fun1326z70zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1937z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2letza71986z00,
		BGl_z62bdbz12zd2letzd2var1328z70zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1938z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2setza71987z00,
		BGl_z62bdbz12zd2setzd2exzd2it1330za2zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1945z00zzbdb_emitz00,
		BgL_bgl_string1945za700za7za7b1988za7, "bdb_emit", 8);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1939z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2jump1989za7,
		BGl_z62bdbz12zd2jumpzd2exzd2it1332za2zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1946z00zzbdb_emitz00,
		BgL_bgl_string1946za700za7za7b1990za7,
		"done bigloo @ TOPLEVEL-INIT export location ", 44);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1940z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2make1991za7,
		BGl_z62bdbz12zd2makezd2box1334z70zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1941z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2boxza71992z00,
		BGl_z62bdbz12zd2boxzd2setz121336z62zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1942z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2appza71993z00,
		BGl_z62bdbz12zd2appzd2ly1338z70zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1943z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2func1994za7,
		BGl_z62bdbz12zd2funcall1340za2zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1944z00zzbdb_emitz00,
		BgL_bgl_za762bdbza712za7d2app11995za7,
		BGl_z62bdbz12zd2app1342za2zzbdb_emitz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1893z00zzbdb_emitz00,
		BgL_bgl_string1893za700za7za7b1996za7, "/* bdb association table */", 27);
	      DEFINE_STRING(BGl_string1894z00zzbdb_emitz00,
		BgL_bgl_string1894za700za7za7b1997za7, "static struct bdb_fun_info {\n",
		29);
	      DEFINE_STRING(BGl_string1895z00zzbdb_emitz00,
		BgL_bgl_string1895za700za7za7b1998za7, "   char *sname, *cname;\n", 24);
	      DEFINE_STRING(BGl_string1896z00zzbdb_emitz00,
		BgL_bgl_string1896za700za7za7b1999za7, "} ", 2);
	      DEFINE_STRING(BGl_string1897z00zzbdb_emitz00,
		BgL_bgl_string1897za700za7za7b2000za7, "__bdb_info", 10);
	      DEFINE_STRING(BGl_string1898z00zzbdb_emitz00,
		BgL_bgl_string1898za700za7za7b2001za7, "[] = { ", 7);
	      DEFINE_STRING(BGl_string1899z00zzbdb_emitz00,
		BgL_bgl_string1899za700za7za7b2002za7,
		"   /* Magic number to ensure comp/dbg compatibility */", 54);
	      DEFINE_STATIC_BGL_GENERIC(BGl_bdbz12zd2envzc0zzbdb_emitz00,
		BgL_bgl_za762bdbza712za770za7za7bd2003za7, BGl_z62bdbz12z70zzbdb_emitz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzbdb_emitz00));
		     ADD_ROOT((void *) (&BGl_za2czd2portza2zd2zzbdb_emitz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzbdb_emitz00(long
		BgL_checksumz00_2693, char *BgL_fromz00_2694)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzbdb_emitz00))
				{
					BGl_requirezd2initializa7ationz75zzbdb_emitz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzbdb_emitz00();
					BGl_libraryzd2moduleszd2initz00zzbdb_emitz00();
					BGl_cnstzd2initzd2zzbdb_emitz00();
					BGl_importedzd2moduleszd2initz00zzbdb_emitz00();
					BGl_genericzd2initzd2zzbdb_emitz00();
					BGl_methodzd2initzd2zzbdb_emitz00();
					return BGl_toplevelzd2initzd2zzbdb_emitz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzbdb_emitz00(void)
	{
		{	/* Bdb/bdb_emit.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "bdb_emit");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "bdb_emit");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "bdb_emit");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "bdb_emit");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "bdb_emit");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "bdb_emit");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "bdb_emit");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "bdb_emit");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "bdb_emit");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "bdb_emit");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"bdb_emit");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"bdb_emit");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "bdb_emit");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "bdb_emit");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzbdb_emitz00(void)
	{
		{	/* Bdb/bdb_emit.scm 15 */
			{	/* Bdb/bdb_emit.scm 15 */
				obj_t BgL_cportz00_2631;

				{	/* Bdb/bdb_emit.scm 15 */
					obj_t BgL_stringz00_2638;

					BgL_stringz00_2638 = BGl_string1946z00zzbdb_emitz00;
					{	/* Bdb/bdb_emit.scm 15 */
						obj_t BgL_startz00_2639;

						BgL_startz00_2639 = BINT(0L);
						{	/* Bdb/bdb_emit.scm 15 */
							obj_t BgL_endz00_2640;

							BgL_endz00_2640 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_2638)));
							{	/* Bdb/bdb_emit.scm 15 */

								BgL_cportz00_2631 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_2638, BgL_startz00_2639, BgL_endz00_2640);
				}}}}
				{
					long BgL_iz00_2632;

					BgL_iz00_2632 = 5L;
				BgL_loopz00_2633:
					if ((BgL_iz00_2632 == -1L))
						{	/* Bdb/bdb_emit.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Bdb/bdb_emit.scm 15 */
							{	/* Bdb/bdb_emit.scm 15 */
								obj_t BgL_arg1947z00_2634;

								{	/* Bdb/bdb_emit.scm 15 */

									{	/* Bdb/bdb_emit.scm 15 */
										obj_t BgL_locationz00_2636;

										BgL_locationz00_2636 = BBOOL(((bool_t) 0));
										{	/* Bdb/bdb_emit.scm 15 */

											BgL_arg1947z00_2634 =
												BGl_readz00zz__readerz00(BgL_cportz00_2631,
												BgL_locationz00_2636);
										}
									}
								}
								{	/* Bdb/bdb_emit.scm 15 */
									int BgL_tmpz00_2728;

									BgL_tmpz00_2728 = (int) (BgL_iz00_2632);
									CNST_TABLE_SET(BgL_tmpz00_2728, BgL_arg1947z00_2634);
							}}
							{	/* Bdb/bdb_emit.scm 15 */
								int BgL_auxz00_2637;

								BgL_auxz00_2637 = (int) ((BgL_iz00_2632 - 1L));
								{
									long BgL_iz00_2733;

									BgL_iz00_2733 = (long) (BgL_auxz00_2637);
									BgL_iz00_2632 = BgL_iz00_2733;
									goto BgL_loopz00_2633;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzbdb_emitz00(void)
	{
		{	/* Bdb/bdb_emit.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzbdb_emitz00(void)
	{
		{	/* Bdb/bdb_emit.scm 15 */
			BGl_za2czd2portza2zd2zzbdb_emitz00 = BUNSPEC;
			return BUNSPEC;
		}

	}



/* emit-bdb-info */
	BGL_EXPORTED_DEF obj_t BGl_emitzd2bdbzd2infoz00zzbdb_emitz00(obj_t
		BgL_globalsz00_39, obj_t BgL_portz00_40)
	{
		{	/* Bdb/bdb_emit.scm 43 */
			BGl_za2czd2portza2zd2zzbdb_emitz00 = BgL_portz00_40;
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_string(BGl_string1893z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_string(BGl_string1894z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_string(BGl_string1895z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_string(BGl_string1896z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_obj(BGl_string1897z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_string(BGl_string1898z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_string(BGl_string1899z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_string(BGl_string1900z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_obj(BINT(BDB_LIBRARY_MAGIC_NUMBER), BgL_portz00_40);
			bgl_display_string(BGl_string1901z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_obj(BINT(BDB_LIBRARY_MAGIC_NUMBER), BgL_portz00_40);
			bgl_display_string(BGl_string1902z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_string(BGl_string1903z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_string(BGl_string1904z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_obj(BGl_za2moduleza2z00zzmodule_modulez00, BgL_portz00_40);
			bgl_display_string(BGl_string1905z00zzbdb_emitz00, BgL_portz00_40);
			{	/* Bdb/bdb_emit.scm 66 */
				obj_t BgL_arg1448z00_1715;

				{	/* Bdb/bdb_emit.scm 66 */
					obj_t BgL_arg1453z00_1716;
					obj_t BgL_arg1454z00_1717;

					{	/* Bdb/bdb_emit.scm 66 */
						obj_t BgL_arg1472z00_1718;

						{	/* Bdb/bdb_emit.scm 66 */
							obj_t BgL_arg1473z00_1719;

							{	/* Bdb/bdb_emit.scm 66 */
								obj_t BgL_arg1485z00_1720;

								BgL_arg1485z00_1720 =
									BGl_getzd2toplevelzd2unitz00zzmodule_includez00();
								BgL_arg1473z00_1719 =
									STRUCT_REF(((obj_t) BgL_arg1485z00_1720), (int) (0L));
							}
							BgL_arg1472z00_1718 =
								BGl_unitzd2initializa7erzd2idza7zzast_unitz00
								(BgL_arg1473z00_1719);
						}
						{	/* Bdb/bdb_emit.scm 64 */
							obj_t BgL_arg1455z00_2231;

							BgL_arg1455z00_2231 =
								SYMBOL_TO_STRING(((obj_t) BgL_arg1472z00_1718));
							BgL_arg1453z00_1716 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_2231);
					}}
					{	/* Bdb/bdb_emit.scm 67 */
						obj_t BgL_symbolz00_2232;

						BgL_symbolz00_2232 = BGl_za2moduleza2z00zzmodule_modulez00;
						{	/* Bdb/bdb_emit.scm 67 */
							obj_t BgL_arg1455z00_2233;

							BgL_arg1455z00_2233 = SYMBOL_TO_STRING(BgL_symbolz00_2232);
							BgL_arg1454z00_1717 =
								BGl_stringzd2copyzd2zz__r4_strings_6_7z00(BgL_arg1455z00_2233);
					}}
					BgL_arg1448z00_1715 =
						bigloo_module_mangle(BgL_arg1453z00_1716, BgL_arg1454z00_1717);
				}
				bgl_display_obj(BgL_arg1448z00_1715, BgL_portz00_40);
			}
			bgl_display_string(BGl_string1906z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			{
				obj_t BgL_l1273z00_1722;

				BgL_l1273z00_1722 = BGl_za2srczd2filesza2zd2zzengine_paramz00;
			BgL_zc3z04anonymousza31486ze3z87_1723:
				if (PAIRP(BgL_l1273z00_1722))
					{	/* Bdb/bdb_emit.scm 69 */
						{	/* Bdb/bdb_emit.scm 70 */
							obj_t BgL_srcz00_1725;

							BgL_srcz00_1725 = CAR(BgL_l1273z00_1722);
							bgl_display_string(BGl_string1904z00zzbdb_emitz00,
								BgL_portz00_40);
							bgl_display_obj(BgL_srcz00_1725, BgL_portz00_40);
							bgl_display_string(BGl_string1907z00zzbdb_emitz00,
								BgL_portz00_40);
							bgl_display_char(((unsigned char) 10), BgL_portz00_40);
						}
						{
							obj_t BgL_l1273z00_2782;

							BgL_l1273z00_2782 = CDR(BgL_l1273z00_1722);
							BgL_l1273z00_1722 = BgL_l1273z00_2782;
							goto BgL_zc3z04anonymousza31486ze3z87_1723;
						}
					}
				else
					{	/* Bdb/bdb_emit.scm 69 */
						((bool_t) 1);
					}
			}
			bgl_display_string(BGl_string1908z00zzbdb_emitz00, BgL_portz00_40);
			{	/* Bdb/bdb_emit.scm 74 */
				obj_t BgL_arg1502z00_1730;

				{	/* Bdb/bdb_emit.scm 74 */
					obj_t BgL_locz00_1731;

					BgL_locz00_1731 =
						BGl_findzd2locationzd2zztools_locationz00
						(BGl_za2modulezd2clauseza2zd2zzmodule_modulez00);
					{	/* Bdb/bdb_emit.scm 75 */
						bool_t BgL_test2007z00_2786;

						if (STRUCTP(BgL_locz00_1731))
							{	/* Bdb/bdb_emit.scm 75 */
								BgL_test2007z00_2786 =
									(STRUCT_KEY(BgL_locz00_1731) == CNST_TABLE_REF(0));
							}
						else
							{	/* Bdb/bdb_emit.scm 75 */
								BgL_test2007z00_2786 = ((bool_t) 0);
							}
						if (BgL_test2007z00_2786)
							{	/* Bdb/bdb_emit.scm 75 */
								BgL_arg1502z00_1730 = STRUCT_REF(BgL_locz00_1731, (int) (2L));
							}
						else
							{	/* Bdb/bdb_emit.scm 75 */
								BgL_arg1502z00_1730 = BGl_string1909z00zzbdb_emitz00;
							}
					}
				}
				bgl_display_obj(BgL_arg1502z00_1730, BgL_portz00_40);
			}
			bgl_display_string(BGl_string1910z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			bgl_display_string(BGl_string1911z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			{
				obj_t BgL_l1277z00_1735;

				BgL_l1277z00_1735 = BgL_globalsz00_39;
			BgL_zc3z04anonymousza31504ze3z87_1736:
				if (PAIRP(BgL_l1277z00_1735))
					{	/* Bdb/bdb_emit.scm 81 */
						{	/* Bdb/bdb_emit.scm 82 */
							obj_t BgL_globalz00_1738;

							BgL_globalz00_1738 = CAR(BgL_l1277z00_1735);
							{	/* Bdb/bdb_emit.scm 82 */
								obj_t BgL_arg1509z00_1739;

								BgL_arg1509z00_1739 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_globalz00_1738))))->
									BgL_idz00);
								BGl_enterzd2functionzd2zztools_errorz00(BgL_arg1509z00_1739);
							}
							BGl_bdbzd2globalzd2sfunz12z12zzbdb_emitz00(BgL_globalz00_1738);
							BGl_leavezd2functionzd2zztools_errorz00();
						}
						{
							obj_t BgL_l1277z00_2808;

							BgL_l1277z00_2808 = CDR(BgL_l1277z00_1735);
							BgL_l1277z00_1735 = BgL_l1277z00_2808;
							goto BgL_zc3z04anonymousza31504ze3z87_1736;
						}
					}
				else
					{	/* Bdb/bdb_emit.scm 81 */
						((bool_t) 1);
					}
			}
			bgl_display_string(BGl_string1912z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			BGl_forzd2eachzd2globalz12z12zzast_envz00(BGl_proc1913z00zzbdb_emitz00,
				BNIL);
			bgl_display_string(BGl_string1914z00zzbdb_emitz00, BgL_portz00_40);
			bgl_display_char(((unsigned char) 10), BgL_portz00_40);
			BGl_emitzd2bdbzd2classzd2typeszd2zzbdb_emitz00(BgL_portz00_40);
			bgl_display_string(BGl_string1915z00zzbdb_emitz00, BgL_portz00_40);
			return bgl_display_char(((unsigned char) 10), BgL_portz00_40);
		}

	}



/* &emit-bdb-info */
	obj_t BGl_z62emitzd2bdbzd2infoz62zzbdb_emitz00(obj_t BgL_envz00_2568,
		obj_t BgL_globalsz00_2569, obj_t BgL_portz00_2570)
	{
		{	/* Bdb/bdb_emit.scm 43 */
			return
				BGl_emitzd2bdbzd2infoz00zzbdb_emitz00(BgL_globalsz00_2569,
				BgL_portz00_2570);
		}

	}



/* &<@anonymous:1516> */
	obj_t BGl_z62zc3z04anonymousza31516ze3ze5zzbdb_emitz00(obj_t BgL_envz00_2571,
		obj_t BgL_globalz00_2572)
	{
		{	/* Bdb/bdb_emit.scm 88 */
			{	/* Bdb/bdb_emit.scm 89 */
				bool_t BgL_test2010z00_2819;

				{	/* Bdb/bdb_emit.scm 89 */
					bool_t BgL_test2011z00_2820;

					{	/* Bdb/bdb_emit.scm 89 */
						bool_t BgL_test2012z00_2821;

						{	/* Bdb/bdb_emit.scm 89 */
							bool_t BgL_test2013z00_2822;

							{	/* Bdb/bdb_emit.scm 89 */
								obj_t BgL_arg1585z00_2642;

								BgL_arg1585z00_2642 =
									(((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt) BgL_globalz00_2572)))->
									BgL_modulez00);
								BgL_test2013z00_2822 =
									(BgL_arg1585z00_2642 ==
									BGl_za2moduleza2z00zzmodule_modulez00);
							}
							if (BgL_test2013z00_2822)
								{	/* Bdb/bdb_emit.scm 89 */
									BgL_test2012z00_2821 =
										(
										(((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt) BgL_globalz00_2572)))->
											BgL_importz00) == CNST_TABLE_REF(1));
								}
							else
								{	/* Bdb/bdb_emit.scm 89 */
									BgL_test2012z00_2821 = ((bool_t) 0);
								}
						}
						if (BgL_test2012z00_2821)
							{	/* Bdb/bdb_emit.scm 89 */
								BgL_test2011z00_2820 = ((bool_t) 1);
							}
						else
							{	/* Bdb/bdb_emit.scm 89 */
								BgL_test2011z00_2820 =
									(
									(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_globalz00_2572))))->
										BgL_occurrencez00) > 0L);
							}
					}
					if (BgL_test2011z00_2820)
						{	/* Bdb/bdb_emit.scm 92 */
							BgL_valuez00_bglt BgL_arg1575z00_2643;

							BgL_arg1575z00_2643 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_globalz00_bglt) BgL_globalz00_2572))))->
								BgL_valuez00);
							{	/* Bdb/bdb_emit.scm 92 */
								obj_t BgL_classz00_2644;

								BgL_classz00_2644 = BGl_svarz00zzast_varz00;
								{	/* Bdb/bdb_emit.scm 92 */
									BgL_objectz00_bglt BgL_arg1807z00_2645;

									{	/* Bdb/bdb_emit.scm 92 */
										obj_t BgL_tmpz00_2837;

										BgL_tmpz00_2837 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1575z00_2643));
										BgL_arg1807z00_2645 =
											(BgL_objectz00_bglt) (BgL_tmpz00_2837);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Bdb/bdb_emit.scm 92 */
											long BgL_idxz00_2646;

											BgL_idxz00_2646 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2645);
											BgL_test2010z00_2819 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_2646 + 2L)) == BgL_classz00_2644);
										}
									else
										{	/* Bdb/bdb_emit.scm 92 */
											bool_t BgL_res1882z00_2649;

											{	/* Bdb/bdb_emit.scm 92 */
												obj_t BgL_oclassz00_2650;

												{	/* Bdb/bdb_emit.scm 92 */
													obj_t BgL_arg1815z00_2651;
													long BgL_arg1816z00_2652;

													BgL_arg1815z00_2651 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Bdb/bdb_emit.scm 92 */
														long BgL_arg1817z00_2653;

														BgL_arg1817z00_2653 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2645);
														BgL_arg1816z00_2652 =
															(BgL_arg1817z00_2653 - OBJECT_TYPE);
													}
													BgL_oclassz00_2650 =
														VECTOR_REF(BgL_arg1815z00_2651,
														BgL_arg1816z00_2652);
												}
												{	/* Bdb/bdb_emit.scm 92 */
													bool_t BgL__ortest_1115z00_2654;

													BgL__ortest_1115z00_2654 =
														(BgL_classz00_2644 == BgL_oclassz00_2650);
													if (BgL__ortest_1115z00_2654)
														{	/* Bdb/bdb_emit.scm 92 */
															BgL_res1882z00_2649 = BgL__ortest_1115z00_2654;
														}
													else
														{	/* Bdb/bdb_emit.scm 92 */
															long BgL_odepthz00_2655;

															{	/* Bdb/bdb_emit.scm 92 */
																obj_t BgL_arg1804z00_2656;

																BgL_arg1804z00_2656 = (BgL_oclassz00_2650);
																BgL_odepthz00_2655 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_2656);
															}
															if ((2L < BgL_odepthz00_2655))
																{	/* Bdb/bdb_emit.scm 92 */
																	obj_t BgL_arg1802z00_2657;

																	{	/* Bdb/bdb_emit.scm 92 */
																		obj_t BgL_arg1803z00_2658;

																		BgL_arg1803z00_2658 = (BgL_oclassz00_2650);
																		BgL_arg1802z00_2657 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_2658, 2L);
																	}
																	BgL_res1882z00_2649 =
																		(BgL_arg1802z00_2657 == BgL_classz00_2644);
																}
															else
																{	/* Bdb/bdb_emit.scm 92 */
																	BgL_res1882z00_2649 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2010z00_2819 = BgL_res1882z00_2649;
										}
								}
							}
						}
					else
						{	/* Bdb/bdb_emit.scm 89 */
							BgL_test2010z00_2819 = ((bool_t) 0);
						}
				}
				if (BgL_test2010z00_2819)
					{	/* Bdb/bdb_emit.scm 89 */
						return
							BGl_bdbzd2globalzd2svarz12z12zzbdb_emitz00(BgL_globalz00_2572);
					}
				else
					{	/* Bdb/bdb_emit.scm 89 */
						return BFALSE;
					}
			}
		}

	}



/* emit-bdb-class-types */
	bool_t BGl_emitzd2bdbzd2classzd2typeszd2zzbdb_emitz00(obj_t BgL_oportz00_41)
	{
		{	/* Bdb/bdb_emit.scm 102 */
			if (NULLP(BGl_getzd2classzd2listz00zzobject_classz00()))
				{	/* Bdb/bdb_emit.scm 103 */
					BFALSE;
				}
			else
				{	/* Bdb/bdb_emit.scm 103 */
					bgl_display_string(BGl_string1916z00zzbdb_emitz00, BgL_oportz00_41);
					bgl_display_char(((unsigned char) 10), BgL_oportz00_41);
				}
			{	/* Bdb/bdb_emit.scm 105 */
				obj_t BgL_g1286z00_1778;

				BgL_g1286z00_1778 = BGl_getzd2classzd2listz00zzobject_classz00();
				{
					obj_t BgL_l1284z00_1780;

					BgL_l1284z00_1780 = BgL_g1286z00_1778;
				BgL_zc3z04anonymousza31594ze3z87_1781:
					if (PAIRP(BgL_l1284z00_1780))
						{	/* Bdb/bdb_emit.scm 111 */
							{	/* Bdb/bdb_emit.scm 106 */
								obj_t BgL_classz00_1783;

								BgL_classz00_1783 = CAR(BgL_l1284z00_1780);
								{	/* Bdb/bdb_emit.scm 107 */
									bool_t BgL_test2019z00_2870;

									{	/* Bdb/bdb_emit.scm 107 */
										obj_t BgL_arg1611z00_1790;

										{	/* Bdb/bdb_emit.scm 107 */
											BgL_globalz00_bglt BgL_arg1613z00_1791;

											{
												BgL_tclassz00_bglt BgL_auxz00_2871;

												{
													obj_t BgL_auxz00_2872;

													{	/* Bdb/bdb_emit.scm 107 */
														BgL_objectz00_bglt BgL_tmpz00_2873;

														BgL_tmpz00_2873 =
															((BgL_objectz00_bglt)
															((BgL_typez00_bglt) BgL_classz00_1783));
														BgL_auxz00_2872 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_2873);
													}
													BgL_auxz00_2871 =
														((BgL_tclassz00_bglt) BgL_auxz00_2872);
												}
												BgL_arg1613z00_1791 =
													(((BgL_tclassz00_bglt) COBJECT(BgL_auxz00_2871))->
													BgL_holderz00);
											}
											BgL_arg1611z00_1790 =
												(((BgL_globalz00_bglt) COBJECT(BgL_arg1613z00_1791))->
												BgL_modulez00);
										}
										BgL_test2019z00_2870 =
											(BgL_arg1611z00_1790 ==
											BGl_za2moduleza2z00zzmodule_modulez00);
									}
									if (BgL_test2019z00_2870)
										{	/* Bdb/bdb_emit.scm 107 */
											bgl_display_string(BGl_string1904z00zzbdb_emitz00,
												BgL_oportz00_41);
											{	/* Bdb/bdb_emit.scm 109 */
												obj_t BgL_arg1609z00_1789;

												BgL_arg1609z00_1789 =
													(((BgL_typez00_bglt) COBJECT(
															((BgL_typez00_bglt) BgL_classz00_1783)))->
													BgL_siza7eza7);
												bgl_display_obj(BgL_arg1609z00_1789, BgL_oportz00_41);
											}
											bgl_display_string(BGl_string1917z00zzbdb_emitz00,
												BgL_oportz00_41);
											bgl_display_char(((unsigned char) 10), BgL_oportz00_41);
										}
									else
										{	/* Bdb/bdb_emit.scm 107 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1284z00_2887;

								BgL_l1284z00_2887 = CDR(BgL_l1284z00_1780);
								BgL_l1284z00_1780 = BgL_l1284z00_2887;
								goto BgL_zc3z04anonymousza31594ze3z87_1781;
							}
						}
					else
						{	/* Bdb/bdb_emit.scm 111 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* bdb-global-sfun! */
	obj_t BGl_bdbzd2globalzd2sfunz12z12zzbdb_emitz00(obj_t BgL_globalz00_42)
	{
		{	/* Bdb/bdb_emit.scm 116 */
			{	/* Bdb/bdb_emit.scm 117 */
				BgL_valuez00_bglt BgL_sfunz00_1794;

				BgL_sfunz00_1794 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_globalz00_42))))->BgL_valuez00);
				{	/* Bdb/bdb_emit.scm 117 */
					obj_t BgL_sfunzd2loczd2_1795;

					BgL_sfunzd2loczd2_1795 =
						(((BgL_sfunz00_bglt) COBJECT(
								((BgL_sfunz00_bglt) BgL_sfunz00_1794)))->BgL_locz00);
					{	/* Bdb/bdb_emit.scm 118 */
						obj_t BgL_cloz00_1796;

						BgL_cloz00_1796 =
							(((BgL_funz00_bglt) COBJECT(
									((BgL_funz00_bglt)
										((BgL_sfunz00_bglt) BgL_sfunz00_1794))))->
							BgL_thezd2closurezd2);
						{	/* Bdb/bdb_emit.scm 119 */

							{	/* Bdb/bdb_emit.scm 120 */
								bool_t BgL_test2020z00_2897;

								{	/* Bdb/bdb_emit.scm 120 */
									bool_t BgL_test2021z00_2898;

									if (STRUCTP(BgL_sfunzd2loczd2_1795))
										{	/* Bdb/bdb_emit.scm 120 */
											BgL_test2021z00_2898 =
												(STRUCT_KEY(BgL_sfunzd2loczd2_1795) ==
												CNST_TABLE_REF(0));
										}
									else
										{	/* Bdb/bdb_emit.scm 120 */
											BgL_test2021z00_2898 = ((bool_t) 0);
										}
									if (BgL_test2021z00_2898)
										{	/* Bdb/bdb_emit.scm 120 */
											BgL_test2020z00_2897 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_globalz00_bglt) BgL_globalz00_42))))->
												BgL_userzf3zf3);
										}
									else
										{	/* Bdb/bdb_emit.scm 120 */
											BgL_test2020z00_2897 = ((bool_t) 0);
										}
								}
								if (BgL_test2020z00_2897)
									{	/* Bdb/bdb_emit.scm 120 */
										BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
											((BgL_variablez00_bglt) BgL_globalz00_42));
										{	/* Bdb/bdb_emit.scm 124 */
											obj_t BgL_fnamez00_1799;
											obj_t BgL_lnumz00_1800;
											obj_t BgL_idz00_1801;
											obj_t BgL_valzd2namezd2_1802;
											obj_t BgL_bpzd2namezd2_1803;

											BgL_fnamez00_1799 =
												STRUCT_REF(BgL_sfunzd2loczd2_1795, (int) (0L));
											BgL_lnumz00_1800 =
												STRUCT_REF(BgL_sfunzd2loczd2_1795, (int) (2L));
											{	/* Bdb/bdb_emit.scm 126 */
												obj_t BgL_idz00_1810;

												BgL_idz00_1810 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_globalz00_42))))->
													BgL_idz00);
												if ((BgL_idz00_1810 == CNST_TABLE_REF(2)))
													{	/* Bdb/bdb_emit.scm 128 */
														obj_t BgL_arg1625z00_1811;

														{	/* Bdb/bdb_emit.scm 128 */
															obj_t BgL_arg1626z00_1812;
															obj_t BgL_arg1627z00_1813;

															{	/* Bdb/bdb_emit.scm 128 */
																obj_t BgL_symbolz00_2332;

																BgL_symbolz00_2332 = CNST_TABLE_REF(3);
																{	/* Bdb/bdb_emit.scm 128 */
																	obj_t BgL_arg1455z00_2333;

																	BgL_arg1455z00_2333 =
																		SYMBOL_TO_STRING(BgL_symbolz00_2332);
																	BgL_arg1626z00_1812 =
																		BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																		(BgL_arg1455z00_2333);
																}
															}
															{	/* Bdb/bdb_emit.scm 128 */
																obj_t BgL_arg1629z00_1814;

																BgL_arg1629z00_1814 =
																	(((BgL_globalz00_bglt) COBJECT(
																			((BgL_globalz00_bglt)
																				BgL_globalz00_42)))->BgL_modulez00);
																{	/* Bdb/bdb_emit.scm 128 */
																	obj_t BgL_arg1455z00_2336;

																	BgL_arg1455z00_2336 =
																		SYMBOL_TO_STRING(BgL_arg1629z00_1814);
																	BgL_arg1627z00_1813 =
																		BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																		(BgL_arg1455z00_2336);
																}
															}
															BgL_arg1625z00_1811 =
																string_append(BgL_arg1626z00_1812,
																BgL_arg1627z00_1813);
														}
														BgL_idz00_1801 =
															bstring_to_symbol(BgL_arg1625z00_1811);
													}
												else
													{	/* Bdb/bdb_emit.scm 127 */
														BgL_idz00_1801 = BgL_idz00_1810;
													}
											}
											{	/* Bdb/bdb_emit.scm 130 */
												bool_t BgL_test2024z00_2928;

												{	/* Bdb/bdb_emit.scm 130 */
													obj_t BgL_classz00_2338;

													BgL_classz00_2338 = BGl_globalz00zzast_varz00;
													if (BGL_OBJECTP(BgL_cloz00_1796))
														{	/* Bdb/bdb_emit.scm 130 */
															BgL_objectz00_bglt BgL_arg1807z00_2340;

															BgL_arg1807z00_2340 =
																(BgL_objectz00_bglt) (BgL_cloz00_1796);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Bdb/bdb_emit.scm 130 */
																	long BgL_idxz00_2346;

																	BgL_idxz00_2346 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_2340);
																	BgL_test2024z00_2928 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_2346 + 2L)) ==
																		BgL_classz00_2338);
																}
															else
																{	/* Bdb/bdb_emit.scm 130 */
																	bool_t BgL_res1884z00_2371;

																	{	/* Bdb/bdb_emit.scm 130 */
																		obj_t BgL_oclassz00_2354;

																		{	/* Bdb/bdb_emit.scm 130 */
																			obj_t BgL_arg1815z00_2362;
																			long BgL_arg1816z00_2363;

																			BgL_arg1815z00_2362 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Bdb/bdb_emit.scm 130 */
																				long BgL_arg1817z00_2364;

																				BgL_arg1817z00_2364 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_2340);
																				BgL_arg1816z00_2363 =
																					(BgL_arg1817z00_2364 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_2354 =
																				VECTOR_REF(BgL_arg1815z00_2362,
																				BgL_arg1816z00_2363);
																		}
																		{	/* Bdb/bdb_emit.scm 130 */
																			bool_t BgL__ortest_1115z00_2355;

																			BgL__ortest_1115z00_2355 =
																				(BgL_classz00_2338 ==
																				BgL_oclassz00_2354);
																			if (BgL__ortest_1115z00_2355)
																				{	/* Bdb/bdb_emit.scm 130 */
																					BgL_res1884z00_2371 =
																						BgL__ortest_1115z00_2355;
																				}
																			else
																				{	/* Bdb/bdb_emit.scm 130 */
																					long BgL_odepthz00_2356;

																					{	/* Bdb/bdb_emit.scm 130 */
																						obj_t BgL_arg1804z00_2357;

																						BgL_arg1804z00_2357 =
																							(BgL_oclassz00_2354);
																						BgL_odepthz00_2356 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_2357);
																					}
																					if ((2L < BgL_odepthz00_2356))
																						{	/* Bdb/bdb_emit.scm 130 */
																							obj_t BgL_arg1802z00_2359;

																							{	/* Bdb/bdb_emit.scm 130 */
																								obj_t BgL_arg1803z00_2360;

																								BgL_arg1803z00_2360 =
																									(BgL_oclassz00_2354);
																								BgL_arg1802z00_2359 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_2360, 2L);
																							}
																							BgL_res1884z00_2371 =
																								(BgL_arg1802z00_2359 ==
																								BgL_classz00_2338);
																						}
																					else
																						{	/* Bdb/bdb_emit.scm 130 */
																							BgL_res1884z00_2371 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2024z00_2928 = BgL_res1884z00_2371;
																}
														}
													else
														{	/* Bdb/bdb_emit.scm 130 */
															BgL_test2024z00_2928 = ((bool_t) 0);
														}
												}
												if (BgL_test2024z00_2928)
													{	/* Bdb/bdb_emit.scm 130 */
														BgL_valzd2namezd2_1802 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt) BgL_cloz00_1796))))->
															BgL_namez00);
													}
												else
													{	/* Bdb/bdb_emit.scm 130 */
														BgL_valzd2namezd2_1802 = BINT(0L);
													}
											}
											BgL_bpzd2namezd2_1803 =
												(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_globalz00_bglt) BgL_globalz00_42))))->
												BgL_namez00);
											{	/* Bdb/bdb_emit.scm 134 */
												obj_t BgL_port1287z00_1804;

												BgL_port1287z00_1804 =
													BGl_za2czd2portza2zd2zzbdb_emitz00;
												{	/* Bdb/bdb_emit.scm 134 */
													obj_t BgL_tmpz00_2958;

													BgL_tmpz00_2958 = ((obj_t) BgL_port1287z00_1804);
													bgl_display_string(BGl_string1904z00zzbdb_emitz00,
														BgL_tmpz00_2958);
												}
												bgl_display_obj(BgL_fnamez00_1799,
													BgL_port1287z00_1804);
												{	/* Bdb/bdb_emit.scm 134 */
													obj_t BgL_tmpz00_2962;

													BgL_tmpz00_2962 = ((obj_t) BgL_port1287z00_1804);
													bgl_display_string(BGl_string1918z00zzbdb_emitz00,
														BgL_tmpz00_2962);
												}
												bgl_display_obj(BgL_lnumz00_1800, BgL_port1287z00_1804);
												{	/* Bdb/bdb_emit.scm 134 */
													obj_t BgL_tmpz00_2966;

													BgL_tmpz00_2966 = ((obj_t) BgL_port1287z00_1804);
													bgl_display_string(BGl_string1910z00zzbdb_emitz00,
														BgL_tmpz00_2966);
												}
												{	/* Bdb/bdb_emit.scm 134 */
													obj_t BgL_tmpz00_2969;

													BgL_tmpz00_2969 = ((obj_t) BgL_port1287z00_1804);
													bgl_display_char(((unsigned char) 10),
														BgL_tmpz00_2969);
											}}
											{	/* Bdb/bdb_emit.scm 135 */
												obj_t BgL_port1288z00_1805;

												BgL_port1288z00_1805 =
													BGl_za2czd2portza2zd2zzbdb_emitz00;
												{	/* Bdb/bdb_emit.scm 135 */
													obj_t BgL_tmpz00_2972;

													BgL_tmpz00_2972 = ((obj_t) BgL_port1288z00_1805);
													bgl_display_string(BGl_string1904z00zzbdb_emitz00,
														BgL_tmpz00_2972);
												}
												bgl_display_obj(BgL_idz00_1801, BgL_port1288z00_1805);
												{	/* Bdb/bdb_emit.scm 135 */
													obj_t BgL_tmpz00_2976;

													BgL_tmpz00_2976 = ((obj_t) BgL_port1288z00_1805);
													bgl_display_string(BGl_string1919z00zzbdb_emitz00,
														BgL_tmpz00_2976);
												}
												{	/* Bdb/bdb_emit.scm 135 */
													obj_t BgL_tmpz00_2979;

													BgL_tmpz00_2979 = ((obj_t) BgL_port1288z00_1805);
													bgl_display_char(((unsigned char) 10),
														BgL_tmpz00_2979);
											}}
											if (BGl_numberzf3zf3zz__r4_numbers_6_5z00
												(BgL_valzd2namezd2_1802))
												{	/* Bdb/bdb_emit.scm 138 */
													obj_t BgL_port1290z00_1807;

													BgL_port1290z00_1807 =
														BGl_za2czd2portza2zd2zzbdb_emitz00;
													{	/* Bdb/bdb_emit.scm 138 */
														obj_t BgL_tmpz00_2984;

														BgL_tmpz00_2984 = ((obj_t) BgL_port1290z00_1807);
														bgl_display_string(BGl_string1920z00zzbdb_emitz00,
															BgL_tmpz00_2984);
													}
													bgl_display_obj(BgL_valzd2namezd2_1802,
														BgL_port1290z00_1807);
													{	/* Bdb/bdb_emit.scm 138 */
														obj_t BgL_tmpz00_2988;

														BgL_tmpz00_2988 = ((obj_t) BgL_port1290z00_1807);
														bgl_display_string(BGl_string1921z00zzbdb_emitz00,
															BgL_tmpz00_2988);
													}
													bgl_display_obj(BgL_bpzd2namezd2_1803,
														BgL_port1290z00_1807);
													{	/* Bdb/bdb_emit.scm 138 */
														obj_t BgL_tmpz00_2992;

														BgL_tmpz00_2992 = ((obj_t) BgL_port1290z00_1807);
														bgl_display_string(BGl_string1922z00zzbdb_emitz00,
															BgL_tmpz00_2992);
													}
													{	/* Bdb/bdb_emit.scm 138 */
														obj_t BgL_tmpz00_2995;

														BgL_tmpz00_2995 = ((obj_t) BgL_port1290z00_1807);
														bgl_display_char(((unsigned char) 10),
															BgL_tmpz00_2995);
												}}
											else
												{	/* Bdb/bdb_emit.scm 137 */
													obj_t BgL_port1289z00_1808;

													BgL_port1289z00_1808 =
														BGl_za2czd2portza2zd2zzbdb_emitz00;
													{	/* Bdb/bdb_emit.scm 137 */
														obj_t BgL_tmpz00_2998;

														BgL_tmpz00_2998 = ((obj_t) BgL_port1289z00_1808);
														bgl_display_string(BGl_string1904z00zzbdb_emitz00,
															BgL_tmpz00_2998);
													}
													bgl_display_obj(BgL_valzd2namezd2_1802,
														BgL_port1289z00_1808);
													{	/* Bdb/bdb_emit.scm 137 */
														obj_t BgL_tmpz00_3002;

														BgL_tmpz00_3002 = ((obj_t) BgL_port1289z00_1808);
														bgl_display_string(BGl_string1905z00zzbdb_emitz00,
															BgL_tmpz00_3002);
													}
													bgl_display_obj(BgL_bpzd2namezd2_1803,
														BgL_port1289z00_1808);
													{	/* Bdb/bdb_emit.scm 137 */
														obj_t BgL_tmpz00_3006;

														BgL_tmpz00_3006 = ((obj_t) BgL_port1289z00_1808);
														bgl_display_string(BGl_string1922z00zzbdb_emitz00,
															BgL_tmpz00_3006);
													}
													{	/* Bdb/bdb_emit.scm 137 */
														obj_t BgL_tmpz00_3009;

														BgL_tmpz00_3009 = ((obj_t) BgL_port1289z00_1808);
														bgl_display_char(((unsigned char) 10),
															BgL_tmpz00_3009);
												}}
											BGl_bdbzd2sfunz12zc0zzbdb_emitz00(BgL_sfunz00_1794);
											{	/* Bdb/bdb_emit.scm 140 */
												obj_t BgL_port1291z00_1809;

												BgL_port1291z00_1809 =
													BGl_za2czd2portza2zd2zzbdb_emitz00;
												{	/* Bdb/bdb_emit.scm 140 */
													obj_t BgL_tmpz00_3013;

													BgL_tmpz00_3013 = ((obj_t) BgL_port1291z00_1809);
													bgl_display_string(BGl_string1923z00zzbdb_emitz00,
														BgL_tmpz00_3013);
												}
												{	/* Bdb/bdb_emit.scm 140 */
													obj_t BgL_tmpz00_3016;

													BgL_tmpz00_3016 = ((obj_t) BgL_port1291z00_1809);
													return
														bgl_display_char(((unsigned char) 10),
														BgL_tmpz00_3016);
									}}}}
								else
									{	/* Bdb/bdb_emit.scm 120 */
										return BFALSE;
									}
							}
						}
					}
				}
			}
		}

	}



/* bdb-global-svar! */
	obj_t BGl_bdbzd2globalzd2svarz12z12zzbdb_emitz00(obj_t BgL_globalz00_43)
	{
		{	/* Bdb/bdb_emit.scm 145 */
			{	/* Bdb/bdb_emit.scm 146 */
				BgL_valuez00_bglt BgL_svarz00_1817;

				BgL_svarz00_1817 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt)
								((BgL_globalz00_bglt) BgL_globalz00_43))))->BgL_valuez00);
				{	/* Bdb/bdb_emit.scm 146 */
					obj_t BgL_svarzd2loczd2_1818;

					BgL_svarzd2loczd2_1818 =
						(((BgL_svarz00_bglt) COBJECT(
								((BgL_svarz00_bglt) BgL_svarz00_1817)))->BgL_locz00);
					{	/* Bdb/bdb_emit.scm 147 */

						{	/* Bdb/bdb_emit.scm 148 */
							bool_t BgL_test2030z00_3024;

							if (STRUCTP(BgL_svarzd2loczd2_1818))
								{	/* Bdb/bdb_emit.scm 148 */
									BgL_test2030z00_3024 =
										(STRUCT_KEY(BgL_svarzd2loczd2_1818) == CNST_TABLE_REF(0));
								}
							else
								{	/* Bdb/bdb_emit.scm 148 */
									BgL_test2030z00_3024 = ((bool_t) 0);
								}
							if (BgL_test2030z00_3024)
								{	/* Bdb/bdb_emit.scm 148 */
									BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
										((BgL_variablez00_bglt) BgL_globalz00_43));
									{	/* Bdb/bdb_emit.scm 151 */
										obj_t BgL_fnamez00_1820;
										obj_t BgL_lnumz00_1821;
										obj_t BgL_idz00_1822;
										obj_t BgL_namez00_1823;

										BgL_fnamez00_1820 =
											STRUCT_REF(BgL_svarzd2loczd2_1818, (int) (0L));
										BgL_lnumz00_1821 =
											STRUCT_REF(BgL_svarzd2loczd2_1818, (int) (2L));
										BgL_idz00_1822 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_globalz00_43))))->
											BgL_idz00);
										BgL_namez00_1823 =
											(((BgL_variablez00_bglt)
												COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
															BgL_globalz00_43))))->BgL_namez00);
										{	/* Bdb/bdb_emit.scm 155 */
											obj_t BgL_port1292z00_1824;

											BgL_port1292z00_1824 = BGl_za2czd2portza2zd2zzbdb_emitz00;
											{	/* Bdb/bdb_emit.scm 155 */
												obj_t BgL_tmpz00_3042;

												BgL_tmpz00_3042 = ((obj_t) BgL_port1292z00_1824);
												bgl_display_string(BGl_string1904z00zzbdb_emitz00,
													BgL_tmpz00_3042);
											}
											bgl_display_obj(BgL_fnamez00_1820, BgL_port1292z00_1824);
											{	/* Bdb/bdb_emit.scm 155 */
												obj_t BgL_tmpz00_3046;

												BgL_tmpz00_3046 = ((obj_t) BgL_port1292z00_1824);
												bgl_display_string(BGl_string1918z00zzbdb_emitz00,
													BgL_tmpz00_3046);
											}
											bgl_display_obj(BgL_lnumz00_1821, BgL_port1292z00_1824);
											{	/* Bdb/bdb_emit.scm 155 */
												obj_t BgL_tmpz00_3050;

												BgL_tmpz00_3050 = ((obj_t) BgL_port1292z00_1824);
												bgl_display_string(BGl_string1910z00zzbdb_emitz00,
													BgL_tmpz00_3050);
											}
											{	/* Bdb/bdb_emit.scm 155 */
												obj_t BgL_tmpz00_3053;

												BgL_tmpz00_3053 = ((obj_t) BgL_port1292z00_1824);
												bgl_display_char(((unsigned char) 10), BgL_tmpz00_3053);
										}}
										{	/* Bdb/bdb_emit.scm 156 */
											obj_t BgL_port1293z00_1825;

											BgL_port1293z00_1825 = BGl_za2czd2portza2zd2zzbdb_emitz00;
											{	/* Bdb/bdb_emit.scm 156 */
												obj_t BgL_tmpz00_3056;

												BgL_tmpz00_3056 = ((obj_t) BgL_port1293z00_1825);
												bgl_display_string(BGl_string1904z00zzbdb_emitz00,
													BgL_tmpz00_3056);
											}
											bgl_display_obj(BgL_idz00_1822, BgL_port1293z00_1825);
											{	/* Bdb/bdb_emit.scm 156 */
												obj_t BgL_tmpz00_3060;

												BgL_tmpz00_3060 = ((obj_t) BgL_port1293z00_1825);
												bgl_display_string(BGl_string1905z00zzbdb_emitz00,
													BgL_tmpz00_3060);
											}
											bgl_display_obj(BgL_namez00_1823, BgL_port1293z00_1825);
											{	/* Bdb/bdb_emit.scm 156 */
												obj_t BgL_tmpz00_3064;

												BgL_tmpz00_3064 = ((obj_t) BgL_port1293z00_1825);
												bgl_display_string(BGl_string1922z00zzbdb_emitz00,
													BgL_tmpz00_3064);
											}
											{	/* Bdb/bdb_emit.scm 156 */
												obj_t BgL_tmpz00_3067;

												BgL_tmpz00_3067 = ((obj_t) BgL_port1293z00_1825);
												return
													bgl_display_char(((unsigned char) 10),
													BgL_tmpz00_3067);
								}}}}
							else
								{	/* Bdb/bdb_emit.scm 148 */
									return BFALSE;
								}
						}
					}
				}
			}
		}

	}



/* bdb-sfun! */
	obj_t BGl_bdbzd2sfunz12zc0zzbdb_emitz00(BgL_valuez00_bglt BgL_sfunz00_44)
	{
		{	/* Bdb/bdb_emit.scm 161 */
			{	/* Bdb/bdb_emit.scm 163 */
				obj_t BgL_g1296z00_1827;

				BgL_g1296z00_1827 =
					(((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_sfunz00_44)))->BgL_argsz00);
				{
					obj_t BgL_l1294z00_1829;

					BgL_l1294z00_1829 = BgL_g1296z00_1827;
				BgL_zc3z04anonymousza31632ze3z87_1830:
					if (PAIRP(BgL_l1294z00_1829))
						{	/* Bdb/bdb_emit.scm 163 */
							BGl_bdbzd2emitzd2localzd2infoz12zc0zzbdb_emitz00(CAR
								(BgL_l1294z00_1829));
							{
								obj_t BgL_l1294z00_3076;

								BgL_l1294z00_3076 = CDR(BgL_l1294z00_1829);
								BgL_l1294z00_1829 = BgL_l1294z00_3076;
								goto BgL_zc3z04anonymousza31632ze3z87_1830;
							}
						}
					else
						{	/* Bdb/bdb_emit.scm 163 */
							((bool_t) 1);
						}
				}
			}
			{	/* Bdb/bdb_emit.scm 164 */
				obj_t BgL_arg1650z00_1835;

				BgL_arg1650z00_1835 =
					(((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_sfunz00_44)))->BgL_bodyz00);
				return
					BGl_bdbz12z12zzbdb_emitz00(((BgL_nodez00_bglt) BgL_arg1650z00_1835));
			}
		}

	}



/* bdb-local-variable? */
	bool_t BGl_bdbzd2localzd2variablezf3zf3zzbdb_emitz00(obj_t BgL_localz00_45)
	{
		{	/* Bdb/bdb_emit.scm 176 */
			{
				obj_t BgL_localz00_1838;

				if (
					(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_localz00_45))))->BgL_userzf3zf3))
					{	/* Bdb/bdb_emit.scm 190 */
						BgL_localz00_1838 = BgL_localz00_45;
						{	/* Bdb/bdb_emit.scm 180 */
							BgL_typez00_bglt BgL_tz00_1840;

							BgL_tz00_1840 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_localz00_bglt) BgL_localz00_1838))))->BgL_typez00);
							{	/* Bdb/bdb_emit.scm 181 */
								bool_t BgL__ortest_1109z00_1841;

								BgL__ortest_1109z00_1841 =
									(
									(((BgL_typez00_bglt) COBJECT(BgL_tz00_1840))->BgL_classz00) ==
									CNST_TABLE_REF(4));
								if (BgL__ortest_1109z00_1841)
									{	/* Bdb/bdb_emit.scm 181 */
										return BgL__ortest_1109z00_1841;
									}
								else
									{	/* Bdb/bdb_emit.scm 182 */
										bool_t BgL__ortest_1111z00_1842;

										BgL__ortest_1111z00_1842 =
											(
											((obj_t) BgL_tz00_1840) ==
											BGl_za2intza2z00zztype_cachez00);
										if (BgL__ortest_1111z00_1842)
											{	/* Bdb/bdb_emit.scm 182 */
												return BgL__ortest_1111z00_1842;
											}
										else
											{	/* Bdb/bdb_emit.scm 183 */
												bool_t BgL__ortest_1112z00_1843;

												BgL__ortest_1112z00_1843 =
													(
													((obj_t) BgL_tz00_1840) ==
													BGl_za2longza2z00zztype_cachez00);
												if (BgL__ortest_1112z00_1843)
													{	/* Bdb/bdb_emit.scm 183 */
														return BgL__ortest_1112z00_1843;
													}
												else
													{	/* Bdb/bdb_emit.scm 184 */
														bool_t BgL__ortest_1113z00_1844;

														BgL__ortest_1113z00_1844 =
															(
															((obj_t) BgL_tz00_1840) ==
															BGl_za2boolza2z00zztype_cachez00);
														if (BgL__ortest_1113z00_1844)
															{	/* Bdb/bdb_emit.scm 184 */
																return BgL__ortest_1113z00_1844;
															}
														else
															{	/* Bdb/bdb_emit.scm 185 */
																bool_t BgL__ortest_1114z00_1845;

																BgL__ortest_1114z00_1845 =
																	(
																	((obj_t) BgL_tz00_1840) ==
																	BGl_za2realza2z00zztype_cachez00);
																if (BgL__ortest_1114z00_1845)
																	{	/* Bdb/bdb_emit.scm 185 */
																		return BgL__ortest_1114z00_1845;
																	}
																else
																	{	/* Bdb/bdb_emit.scm 186 */
																		bool_t BgL__ortest_1116z00_1846;

																		BgL__ortest_1116z00_1846 =
																			(
																			((obj_t) BgL_tz00_1840) ==
																			BGl_za2charza2z00zztype_cachez00);
																		if (BgL__ortest_1116z00_1846)
																			{	/* Bdb/bdb_emit.scm 186 */
																				return BgL__ortest_1116z00_1846;
																			}
																		else
																			{	/* Bdb/bdb_emit.scm 186 */
																				return
																					(
																					((obj_t) BgL_tz00_1840) ==
																					BGl_za2stringza2z00zztype_cachez00);
																			}
																	}
															}
													}
											}
									}
							}
						}
					}
				else
					{	/* Bdb/bdb_emit.scm 190 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* bdb-emit-local-info! */
	obj_t BGl_bdbzd2emitzd2localzd2infoz12zc0zzbdb_emitz00(obj_t BgL_localz00_46)
	{
		{	/* Bdb/bdb_emit.scm 195 */
			if (BGl_bdbzd2localzd2variablezf3zf3zzbdb_emitz00(BgL_localz00_46))
				{	/* Bdb/bdb_emit.scm 196 */
					BGl_setzd2variablezd2namez12z12zzbackend_cplibz00(
						((BgL_variablez00_bglt) BgL_localz00_46));
					{	/* Bdb/bdb_emit.scm 199 */
						obj_t BgL_idz00_1850;
						obj_t BgL_namez00_1851;

						BgL_idz00_1850 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_localz00_46))))->BgL_idz00);
						BgL_namez00_1851 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_localz00_bglt) BgL_localz00_46))))->BgL_namez00);
						{	/* Bdb/bdb_emit.scm 201 */
							obj_t BgL_port1297z00_1852;

							BgL_port1297z00_1852 = BGl_za2czd2portza2zd2zzbdb_emitz00;
							{	/* Bdb/bdb_emit.scm 201 */
								obj_t BgL_tmpz00_3120;

								BgL_tmpz00_3120 = ((obj_t) BgL_port1297z00_1852);
								bgl_display_string(BGl_string1924z00zzbdb_emitz00,
									BgL_tmpz00_3120);
							}
							bgl_display_obj(BgL_idz00_1850, BgL_port1297z00_1852);
							{	/* Bdb/bdb_emit.scm 201 */
								obj_t BgL_tmpz00_3124;

								BgL_tmpz00_3124 = ((obj_t) BgL_port1297z00_1852);
								bgl_display_string(BGl_string1905z00zzbdb_emitz00,
									BgL_tmpz00_3124);
							}
							bgl_display_obj(BgL_namez00_1851, BgL_port1297z00_1852);
							{	/* Bdb/bdb_emit.scm 201 */
								obj_t BgL_tmpz00_3128;

								BgL_tmpz00_3128 = ((obj_t) BgL_port1297z00_1852);
								bgl_display_string(BGl_string1922z00zzbdb_emitz00,
									BgL_tmpz00_3128);
							}
							{	/* Bdb/bdb_emit.scm 201 */
								obj_t BgL_tmpz00_3131;

								BgL_tmpz00_3131 = ((obj_t) BgL_port1297z00_1852);
								return bgl_display_char(((unsigned char) 10), BgL_tmpz00_3131);
				}}}}
			else
				{	/* Bdb/bdb_emit.scm 196 */
					return BFALSE;
				}
		}

	}



/* bdb*! */
	obj_t BGl_bdbza2z12zb0zzbdb_emitz00(obj_t BgL_nodesz00_64)
	{
		{	/* Bdb/bdb_emit.scm 343 */
			{
				obj_t BgL_hookz00_1854;

				BgL_hookz00_1854 = BgL_nodesz00_64;
			BgL_zc3z04anonymousza31656ze3z87_1855:
				if (NULLP(BgL_hookz00_1854))
					{	/* Bdb/bdb_emit.scm 345 */
						return CNST_TABLE_REF(5);
					}
				else
					{	/* Bdb/bdb_emit.scm 345 */
						{	/* Bdb/bdb_emit.scm 348 */
							obj_t BgL_arg1661z00_1857;

							BgL_arg1661z00_1857 = CAR(((obj_t) BgL_hookz00_1854));
							BGl_bdbz12z12zzbdb_emitz00(
								((BgL_nodez00_bglt) BgL_arg1661z00_1857));
						}
						{	/* Bdb/bdb_emit.scm 349 */
							obj_t BgL_arg1663z00_1858;

							BgL_arg1663z00_1858 = CDR(((obj_t) BgL_hookz00_1854));
							{
								obj_t BgL_hookz00_3143;

								BgL_hookz00_3143 = BgL_arg1663z00_1858;
								BgL_hookz00_1854 = BgL_hookz00_3143;
								goto BgL_zc3z04anonymousza31656ze3z87_1855;
							}
						}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzbdb_emitz00(void)
	{
		{	/* Bdb/bdb_emit.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzbdb_emitz00(void)
	{
		{	/* Bdb/bdb_emit.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_proc1925z00zzbdb_emitz00,
				BGl_nodez00zzast_nodez00, BGl_string1926z00zzbdb_emitz00);
		}

	}



/* &bdb!1307 */
	obj_t BGl_z62bdbz121307z70zzbdb_emitz00(obj_t BgL_envz00_2574,
		obj_t BgL_nodez00_2575)
	{
		{	/* Bdb/bdb_emit.scm 206 */
			return BUNSPEC;
		}

	}



/* bdb! */
	obj_t BGl_bdbz12z12zzbdb_emitz00(BgL_nodez00_bglt BgL_nodez00_47)
	{
		{	/* Bdb/bdb_emit.scm 206 */
			{	/* Bdb/bdb_emit.scm 206 */
				obj_t BgL_method1308z00_1864;

				{	/* Bdb/bdb_emit.scm 206 */
					obj_t BgL_res1890z00_2473;

					{	/* Bdb/bdb_emit.scm 206 */
						long BgL_objzd2classzd2numz00_2444;

						BgL_objzd2classzd2numz00_2444 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_47));
						{	/* Bdb/bdb_emit.scm 206 */
							obj_t BgL_arg1811z00_2445;

							BgL_arg1811z00_2445 =
								PROCEDURE_REF(BGl_bdbz12zd2envzc0zzbdb_emitz00, (int) (1L));
							{	/* Bdb/bdb_emit.scm 206 */
								int BgL_offsetz00_2448;

								BgL_offsetz00_2448 = (int) (BgL_objzd2classzd2numz00_2444);
								{	/* Bdb/bdb_emit.scm 206 */
									long BgL_offsetz00_2449;

									BgL_offsetz00_2449 =
										((long) (BgL_offsetz00_2448) - OBJECT_TYPE);
									{	/* Bdb/bdb_emit.scm 206 */
										long BgL_modz00_2450;

										BgL_modz00_2450 =
											(BgL_offsetz00_2449 >> (int) ((long) ((int) (4L))));
										{	/* Bdb/bdb_emit.scm 206 */
											long BgL_restz00_2452;

											BgL_restz00_2452 =
												(BgL_offsetz00_2449 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Bdb/bdb_emit.scm 206 */

												{	/* Bdb/bdb_emit.scm 206 */
													obj_t BgL_bucketz00_2454;

													BgL_bucketz00_2454 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2445), BgL_modz00_2450);
													BgL_res1890z00_2473 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2454), BgL_restz00_2452);
					}}}}}}}}
					BgL_method1308z00_1864 = BgL_res1890z00_2473;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1308z00_1864, ((obj_t) BgL_nodez00_47));
			}
		}

	}



/* &bdb! */
	obj_t BGl_z62bdbz12z70zzbdb_emitz00(obj_t BgL_envz00_2576,
		obj_t BgL_nodez00_2577)
	{
		{	/* Bdb/bdb_emit.scm 206 */
			return BGl_bdbz12z12zzbdb_emitz00(((BgL_nodez00_bglt) BgL_nodez00_2577));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzbdb_emitz00(void)
	{
		{	/* Bdb/bdb_emit.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_sequencez00zzast_nodez00,
				BGl_proc1927z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_syncz00zzast_nodez00,
				BGl_proc1929z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_externz00zzast_nodez00,
				BGl_proc1930z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_castz00zzast_nodez00,
				BGl_proc1931z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_setqz00zzast_nodez00,
				BGl_proc1932z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc1933z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_failz00zzast_nodez00,
				BGl_proc1934z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_switchz00zzast_nodez00,
				BGl_proc1935z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc1936z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc1937z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_setzd2exzd2itz00zzast_nodez00,
				BGl_proc1938z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc1939z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc1940z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_boxzd2setz12zc0zzast_nodez00,
				BGl_proc1941z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc1942z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_funcallz00zzast_nodez00,
				BGl_proc1943z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_bdbz12zd2envzc0zzbdb_emitz00, BGl_appz00zzast_nodez00,
				BGl_proc1944z00zzbdb_emitz00, BGl_string1928z00zzbdb_emitz00);
		}

	}



/* &bdb!-app1342 */
	obj_t BGl_z62bdbz12zd2app1342za2zzbdb_emitz00(obj_t BgL_envz00_2595,
		obj_t BgL_nodez00_2596)
	{
		{	/* Bdb/bdb_emit.scm 354 */
			return
				BGl_bdbza2z12zb0zzbdb_emitz00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_2596)))->BgL_argsz00));
		}

	}



/* &bdb!-funcall1340 */
	obj_t BGl_z62bdbz12zd2funcall1340za2zzbdb_emitz00(obj_t BgL_envz00_2597,
		obj_t BgL_nodez00_2598)
	{
		{	/* Bdb/bdb_emit.scm 335 */
			BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2598)))->BgL_funz00));
			return
				BGl_bdbza2z12zb0zzbdb_emitz00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_2598)))->BgL_argsz00));
		}

	}



/* &bdb!-app-ly1338 */
	obj_t BGl_z62bdbz12zd2appzd2ly1338z70zzbdb_emitz00(obj_t BgL_envz00_2599,
		obj_t BgL_nodez00_2600)
	{
		{	/* Bdb/bdb_emit.scm 327 */
			BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2600)))->BgL_funz00));
			return
				BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_2600)))->BgL_argz00));
		}

	}



/* &bdb!-box-set!1336 */
	obj_t BGl_z62bdbz12zd2boxzd2setz121336z62zzbdb_emitz00(obj_t BgL_envz00_2601,
		obj_t BgL_nodez00_2602)
	{
		{	/* Bdb/bdb_emit.scm 320 */
			return
				BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_2602)))->BgL_valuez00));
		}

	}



/* &bdb!-make-box1334 */
	obj_t BGl_z62bdbz12zd2makezd2box1334z70zzbdb_emitz00(obj_t BgL_envz00_2603,
		obj_t BgL_nodez00_2604)
	{
		{	/* Bdb/bdb_emit.scm 313 */
			return
				BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_2604)))->BgL_valuez00));
		}

	}



/* &bdb!-jump-ex-it1332 */
	obj_t BGl_z62bdbz12zd2jumpzd2exzd2it1332za2zzbdb_emitz00(obj_t
		BgL_envz00_2605, obj_t BgL_nodez00_2606)
	{
		{	/* Bdb/bdb_emit.scm 305 */
			BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2606)))->BgL_exitz00));
			return
				BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_2606)))->BgL_valuez00));
		}

	}



/* &bdb!-set-ex-it1330 */
	obj_t BGl_z62bdbz12zd2setzd2exzd2it1330za2zzbdb_emitz00(obj_t BgL_envz00_2607,
		obj_t BgL_nodez00_2608)
	{
		{	/* Bdb/bdb_emit.scm 297 */
			BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2608)))->BgL_onexitz00));
			return
				BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_2608)))->BgL_bodyz00));
		}

	}



/* &bdb!-let-var1328 */
	obj_t BGl_z62bdbz12zd2letzd2var1328z70zzbdb_emitz00(obj_t BgL_envz00_2609,
		obj_t BgL_nodez00_2610)
	{
		{	/* Bdb/bdb_emit.scm 285 */
			{	/* Bdb/bdb_emit.scm 287 */
				obj_t BgL_g1306z00_2668;

				BgL_g1306z00_2668 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2610)))->BgL_bindingsz00);
				{
					obj_t BgL_l1304z00_2670;

					BgL_l1304z00_2670 = BgL_g1306z00_2668;
				BgL_zc3z04anonymousza31725ze3z87_2669:
					if (PAIRP(BgL_l1304z00_2670))
						{	/* Bdb/bdb_emit.scm 287 */
							{	/* Bdb/bdb_emit.scm 288 */
								obj_t BgL_bindingz00_2671;

								BgL_bindingz00_2671 = CAR(BgL_l1304z00_2670);
								{	/* Bdb/bdb_emit.scm 289 */
									obj_t BgL_arg1733z00_2672;

									BgL_arg1733z00_2672 = CDR(((obj_t) BgL_bindingz00_2671));
									BGl_bdbz12z12zzbdb_emitz00(
										((BgL_nodez00_bglt) BgL_arg1733z00_2672));
								}
								{	/* Bdb/bdb_emit.scm 290 */
									obj_t BgL_arg1734z00_2673;

									BgL_arg1734z00_2673 = CAR(((obj_t) BgL_bindingz00_2671));
									BGl_bdbzd2emitzd2localzd2infoz12zc0zzbdb_emitz00
										(BgL_arg1734z00_2673);
								}
							}
							{
								obj_t BgL_l1304z00_3239;

								BgL_l1304z00_3239 = CDR(BgL_l1304z00_2670);
								BgL_l1304z00_2670 = BgL_l1304z00_3239;
								goto BgL_zc3z04anonymousza31725ze3z87_2669;
							}
						}
					else
						{	/* Bdb/bdb_emit.scm 287 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_2610)))->BgL_bodyz00));
		}

	}



/* &bdb!-let-fun1326 */
	obj_t BGl_z62bdbz12zd2letzd2fun1326z70zzbdb_emitz00(obj_t BgL_envz00_2611,
		obj_t BgL_nodez00_2612)
	{
		{	/* Bdb/bdb_emit.scm 275 */
			{	/* Bdb/bdb_emit.scm 277 */
				obj_t BgL_g1303z00_2675;

				BgL_g1303z00_2675 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2612)))->BgL_localsz00);
				{
					obj_t BgL_l1301z00_2677;

					BgL_l1301z00_2677 = BgL_g1303z00_2675;
				BgL_zc3z04anonymousza31718ze3z87_2676:
					if (PAIRP(BgL_l1301z00_2677))
						{	/* Bdb/bdb_emit.scm 277 */
							{	/* Bdb/bdb_emit.scm 278 */
								obj_t BgL_localz00_2678;

								BgL_localz00_2678 = CAR(BgL_l1301z00_2677);
								{	/* Bdb/bdb_emit.scm 278 */
									BgL_valuez00_bglt BgL_arg1720z00_2679;

									BgL_arg1720z00_2679 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_localz00_bglt) BgL_localz00_2678))))->
										BgL_valuez00);
									BGl_bdbzd2sfunz12zc0zzbdb_emitz00(BgL_arg1720z00_2679);
								}
							}
							{
								obj_t BgL_l1301z00_3253;

								BgL_l1301z00_3253 = CDR(BgL_l1301z00_2677);
								BgL_l1301z00_2677 = BgL_l1301z00_3253;
								goto BgL_zc3z04anonymousza31718ze3z87_2676;
							}
						}
					else
						{	/* Bdb/bdb_emit.scm 277 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_2612)))->BgL_bodyz00));
		}

	}



/* &bdb!-switch1324 */
	obj_t BGl_z62bdbz12zd2switch1324za2zzbdb_emitz00(obj_t BgL_envz00_2613,
		obj_t BgL_nodez00_2614)
	{
		{	/* Bdb/bdb_emit.scm 267 */
			{	/* Bdb/bdb_emit.scm 268 */
				bool_t BgL_tmpz00_3258;

				BGl_bdbz12z12zzbdb_emitz00(
					(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2614)))->BgL_testz00));
				{	/* Bdb/bdb_emit.scm 270 */
					obj_t BgL_g1300z00_2681;

					BgL_g1300z00_2681 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_2614)))->BgL_clausesz00);
					{
						obj_t BgL_l1298z00_2683;

						BgL_l1298z00_2683 = BgL_g1300z00_2681;
					BgL_zc3z04anonymousza31710ze3z87_2682:
						if (PAIRP(BgL_l1298z00_2683))
							{	/* Bdb/bdb_emit.scm 270 */
								{	/* Bdb/bdb_emit.scm 270 */
									obj_t BgL_clausez00_2684;

									BgL_clausez00_2684 = CAR(BgL_l1298z00_2683);
									{	/* Bdb/bdb_emit.scm 270 */
										obj_t BgL_arg1714z00_2685;

										BgL_arg1714z00_2685 = CDR(((obj_t) BgL_clausez00_2684));
										BGl_bdbz12z12zzbdb_emitz00(
											((BgL_nodez00_bglt) BgL_arg1714z00_2685));
									}
								}
								{
									obj_t BgL_l1298z00_3271;

									BgL_l1298z00_3271 = CDR(BgL_l1298z00_2683);
									BgL_l1298z00_2683 = BgL_l1298z00_3271;
									goto BgL_zc3z04anonymousza31710ze3z87_2682;
								}
							}
						else
							{	/* Bdb/bdb_emit.scm 270 */
								BgL_tmpz00_3258 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_3258);
			}
		}

	}



/* &bdb!-fail1322 */
	obj_t BGl_z62bdbz12zd2fail1322za2zzbdb_emitz00(obj_t BgL_envz00_2615,
		obj_t BgL_nodez00_2616)
	{
		{	/* Bdb/bdb_emit.scm 258 */
			BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2616)))->BgL_procz00));
			BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2616)))->BgL_msgz00));
			return
				BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_2616)))->BgL_objz00));
		}

	}



/* &bdb!-conditional1320 */
	obj_t BGl_z62bdbz12zd2conditional1320za2zzbdb_emitz00(obj_t BgL_envz00_2617,
		obj_t BgL_nodez00_2618)
	{
		{	/* Bdb/bdb_emit.scm 249 */
			BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2618)))->BgL_testz00));
			BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2618)))->BgL_truez00));
			return
				BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_2618)))->BgL_falsez00));
		}

	}



/* &bdb!-setq1318 */
	obj_t BGl_z62bdbz12zd2setq1318za2zzbdb_emitz00(obj_t BgL_envz00_2619,
		obj_t BgL_nodez00_2620)
	{
		{	/* Bdb/bdb_emit.scm 242 */
			return
				BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_2620)))->BgL_valuez00));
		}

	}



/* &bdb!-cast1316 */
	obj_t BGl_z62bdbz12zd2cast1316za2zzbdb_emitz00(obj_t BgL_envz00_2621,
		obj_t BgL_nodez00_2622)
	{
		{	/* Bdb/bdb_emit.scm 235 */
			return
				BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_2622)))->BgL_argz00));
		}

	}



/* &bdb!-extern1314 */
	obj_t BGl_z62bdbz12zd2extern1314za2zzbdb_emitz00(obj_t BgL_envz00_2623,
		obj_t BgL_nodez00_2624)
	{
		{	/* Bdb/bdb_emit.scm 228 */
			return
				BGl_bdbza2z12zb0zzbdb_emitz00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_2624)))->BgL_exprza2za2));
		}

	}



/* &bdb!-sync1312 */
	obj_t BGl_z62bdbz12zd2sync1312za2zzbdb_emitz00(obj_t BgL_envz00_2625,
		obj_t BgL_nodez00_2626)
	{
		{	/* Bdb/bdb_emit.scm 219 */
			BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2626)))->BgL_mutexz00));
			BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2626)))->BgL_prelockz00));
			return
				BGl_bdbz12z12zzbdb_emitz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_2626)))->BgL_bodyz00));
		}

	}



/* &bdb!-sequence1310 */
	obj_t BGl_z62bdbz12zd2sequence1310za2zzbdb_emitz00(obj_t BgL_envz00_2627,
		obj_t BgL_nodez00_2628)
	{
		{	/* Bdb/bdb_emit.scm 212 */
			return
				BGl_bdbza2z12zb0zzbdb_emitz00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_2628)))->BgL_nodesz00));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzbdb_emitz00(void)
	{
		{	/* Bdb/bdb_emit.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zztools_miscz00(9470071L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zztools_locationz00(462333240L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zzobject_slotsz00(151271251L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zzast_identz00(174885617L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zzast_unitz00(234044111L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zzmodule_modulez00(158397907L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zzmodule_includez00(184800657L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zzengine_configurez00(272817175L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			BGl_modulezd2initializa7ationz75zzbackend_c_prototypez00(364917963L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
			return
				BGl_modulezd2initializa7ationz75zzbackend_cplibz00(395792377L,
				BSTRING_TO_STRING(BGl_string1945z00zzbdb_emitz00));
		}

	}

#ifdef __cplusplus
}
#endif
