/*===========================================================================*/
/*   (Cfa/closure.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/closure.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_CLOSURE_TYPE_DEFINITIONS
#define BGL_CFA_CLOSURE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_scnstz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_nodez00;
		obj_t BgL_classz00;
		obj_t BgL_locz00;
	}               *BgL_scnstz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_refz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_refz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_internzd2sfunzf2cinfoz20_bgl
	{
		bool_t BgL_polymorphiczf3zf3;
		struct BgL_approxz00_bgl *BgL_approxz00;
		long BgL_stampz00;
	}                               *BgL_internzd2sfunzf2cinfoz20_bglt;

	typedef struct BgL_svarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_clozd2envzf3z21;
		long BgL_stampz00;
	}                      *BgL_svarzf2cinfozf2_bglt;

	typedef struct BgL_makezd2procedurezd2appz00_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		obj_t BgL_valueszd2approxzd2;
		long BgL_lostzd2stampzd2;
		bool_t BgL_xzd2tzf3z21;
		bool_t BgL_xz00;
		bool_t BgL_tz00;
		struct BgL_variablez00_bgl *BgL_ownerz00;
		obj_t BgL_stackzd2stampzd2;
	}                                *BgL_makezd2procedurezd2appz00_bglt;


#endif													// BGL_CFA_CLOSURE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_getzd2approxzd2typez00zzcfa_typez00(BgL_approxz00_bglt,
		obj_t);
	static bool_t BGl_lightzd2makezd2procedurez12z12zzcfa_closurez00(void);
	extern obj_t BGl_setzd2lengthzd2zzcfa_setz00(obj_t);
	static obj_t BGl_z62getzd2procedurezd2listz62zzcfa_closurez00(obj_t);
	static bool_t BGl_mergezd2appzd2typesz12z12zzcfa_closurez00(obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_addzd2makezd2procedurez12z12zzcfa_closurez00(BgL_nodez00_bglt);
	static obj_t BGl_z62stopzd2closurezd2cachez62zzcfa_closurez00(obj_t);
	static obj_t BGl_objectzd2initzd2zzcfa_closurez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_addzd2funcallz12zc0zzcfa_closurez00(BgL_nodez00_bglt);
	extern obj_t BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00(obj_t,
		BgL_approxz00_bglt);
	static obj_t BGl_methodzd2initzd2zzcfa_closurez00(void);
	extern BgL_typez00_bglt
		BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_typez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_typezd2closuresz12zc0zzcfa_closurez00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_closurezd2optimiza7ationz12z67zzcfa_closurez00(obj_t);
	static obj_t BGl_z62zc3z04anonymousza31700ze3ze5zzcfa_closurez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_closurezd2optimiza7ationzf3z86zzcfa_closurez00(void);
	extern obj_t
		BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00;
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t
		BGl_getzd2nodezd2atomzd2valuezd2zzcfa_approxz00(BgL_nodez00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_getzd2procedurezd2listz00zzcfa_closurez00(void);
	static bool_t BGl_showze70ze7zzcfa_closurez00(obj_t, obj_t);
	static obj_t BGl_za2procedurezd2lzd2setz12za2z12zzcfa_closurez00 = BUNSPEC;
	extern obj_t BGl_lightzd2typez12zc0zzcfa_ltypez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_stopzd2closurezd2cachez00zzcfa_closurez00(void);
	static obj_t BGl_z62closurezd2optimiza7ationz12z05zzcfa_closurez00(obj_t,
		obj_t);
	static obj_t BGl_z62closurezd2optimiza7ationzf3ze4zzcfa_closurez00(obj_t);
	static obj_t BGl_z62addzd2funcallz12za2zzcfa_closurez00(obj_t, obj_t);
	extern obj_t BGl_za2unsafezd2typeza2zd2zzengine_paramz00;
	static obj_t BGl_gczd2rootszd2initz00zzcfa_closurez00(void);
	static obj_t BGl_z62addzd2procedurezd2refz12z70zzcfa_closurez00(obj_t, obj_t);
	static bool_t BGl_lightzd2funcallz12zc0zzcfa_closurez00(void);
	extern obj_t BGl_makezd2procedurezd2appz00zzcfa_info2z00;
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_za2procedurezd2elzd2setz12za2z12zzcfa_closurez00 = BUNSPEC;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	extern BgL_typez00_bglt
		BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00(BgL_typez00_bglt);
	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	static obj_t BGl_za2makezd2elzd2procedureza2z00zzcfa_closurez00 = BUNSPEC;
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_closurez00 = BUNSPEC;
	extern obj_t BGl_getzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_EXPORTED_DECL obj_t
		BGl_addzd2procedurezd2refz12z12zzcfa_closurez00(BgL_nodez00_bglt);
	static obj_t BGl_toplevelzd2initzd2zzcfa_closurez00(void);
	extern obj_t BGl_setzd2ze3listz31zzcfa_setz00(obj_t);
	static obj_t BGl_genericzd2initzd2zzcfa_closurez00(void);
	static obj_t BGl_showzd2Xzd2Tz00zzcfa_closurez00(obj_t);
	extern obj_t BGl_setzd2headzd2zzcfa_setz00(obj_t, obj_t);
	static obj_t BGl_z62typezd2closuresz12za2zzcfa_closurez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	static obj_t BGl_za2makezd2lzd2procedureza2z00zzcfa_closurez00 = BUNSPEC;
	static obj_t BGl_z62approxzd2procedurezd2elzf3z91zzcfa_closurez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	extern obj_t BGl_refz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t
		BGl_approxzd2procedurezd2elzf3zf3zzcfa_closurez00(BgL_approxz00_bglt);
	static obj_t BGl_za2makezd2procedurezd2listza2z00zzcfa_closurez00 = BUNSPEC;
	static bool_t BGl_Xz12z12zzcfa_closurez00(obj_t);
	static obj_t BGl_Tzd2fixzd2pointz12z12zzcfa_closurez00(obj_t);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_closurez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_ltypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_setz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_cfaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_za2procedurezd2lzd2refza2z00zzcfa_closurez00 = BUNSPEC;
	static obj_t BGl_za2procedurezd2refzd2listza2z00zzcfa_closurez00 = BUNSPEC;
	static bool_t BGl_lightzd2accessz12zc0zzcfa_closurez00(void);
	static obj_t BGl_za2procedurezd2refza2zd2zzcfa_closurez00 = BUNSPEC;
	static obj_t BGl_cnstzd2initzd2zzcfa_closurez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_closurez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_closurez00(void);
	extern obj_t BGl_scnstz00zzast_varz00;
	static obj_t BGl_z62zc3z04anonymousza31715ze3ze5zzcfa_closurez00(obj_t,
		obj_t);
	static obj_t BGl_startzd2closurezd2cachez00zzcfa_closurez00(void);
	static obj_t BGl_za2funcallzd2listza2zd2zzcfa_closurez00 = BUNSPEC;
	static obj_t BGl_za2procedurezd2setz12za2zc0zzcfa_closurez00 = BUNSPEC;
	static obj_t BGl_za2procedurezd2elzd2refza2z00zzcfa_closurez00 = BUNSPEC;
	extern BgL_approxz00_bglt BGl_cfaz12z12zzcfa_cfaz00(BgL_nodez00_bglt);
	static obj_t BGl_z62addzd2makezd2procedurez12z70zzcfa_closurez00(obj_t,
		obj_t);
	extern obj_t BGl_za2procedurezd2elza2zd2zztype_cachez00;
	static bool_t BGl_mergezd2appzd2returnzd2typesz12zc0zzcfa_closurez00(obj_t,
		BgL_approxz00_bglt);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t __cnst[20];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2229z00zzcfa_closurez00,
		BgL_bgl_za762za7c3za704anonymo2237za7,
		BGl_z62zc3z04anonymousza31700ze3ze5zzcfa_closurez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2230z00zzcfa_closurez00,
		BgL_bgl_za762za7c3za704anonymo2238za7,
		BGl_z62zc3z04anonymousza31715ze3ze5zzcfa_closurez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_addzd2makezd2procedurez12zd2envzc0zzcfa_closurez00,
		BgL_bgl_za762addza7d2makeza7d22239za7,
		BGl_z62addzd2makezd2procedurez12z70zzcfa_closurez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_addzd2funcallz12zd2envz12zzcfa_closurez00,
		BgL_bgl_za762addza7d2funcall2240z00,
		BGl_z62addzd2funcallz12za2zzcfa_closurez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2228z00zzcfa_closurez00,
		BgL_bgl_string2228za700za7za7c2241za7, "   . Light closures", 19);
	      DEFINE_STRING(BGl_string2231z00zzcfa_closurez00,
		BgL_bgl_string2231za700za7za7c2242za7, ": ", 2);
	      DEFINE_STRING(BGl_string2232z00zzcfa_closurez00,
		BgL_bgl_string2232za700za7za7c2243za7, "      ", 6);
	      DEFINE_STRING(BGl_string2233z00zzcfa_closurez00,
		BgL_bgl_string2233za700za7za7c2244za7, "        ", 8);
	      DEFINE_STRING(BGl_string2234z00zzcfa_closurez00,
		BgL_bgl_string2234za700za7za7c2245za7, "cfa_closure", 11);
	      DEFINE_STRING(BGl_string2235z00zzcfa_closurez00,
		BgL_bgl_string2235za700za7za7c2246za7,
		"make-l-procedure make-el-procedure procedure-el-set! procedure-el-ref procedure-l-set! procedure-l-ref procedure-set! foreign procedure-ref T X nothing-to-do slfun selfun done ok \077\077? light elight static ",
		203);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_addzd2procedurezd2refz12zd2envzc0zzcfa_closurez00,
		BgL_bgl_za762addza7d2procedu2247z00,
		BGl_z62addzd2procedurezd2refz12z70zzcfa_closurez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_getzd2procedurezd2listzd2envzd2zzcfa_closurez00,
		BgL_bgl_za762getza7d2procedu2248z00,
		BGl_z62getzd2procedurezd2listz62zzcfa_closurez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2closuresz12zd2envz12zzcfa_closurez00,
		BgL_bgl_za762typeza7d2closur2249z00,
		BGl_z62typezd2closuresz12za2zzcfa_closurez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_closurezd2optimiza7ationzf3zd2envz54zzcfa_closurez00,
		BgL_bgl_za762closureza7d2opt2250z00,
		BGl_z62closurezd2optimiza7ationzf3ze4zzcfa_closurez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_stopzd2closurezd2cachezd2envzd2zzcfa_closurez00,
		BgL_bgl_za762stopza7d2closur2251z00,
		BGl_z62stopzd2closurezd2cachez62zzcfa_closurez00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_closurezd2optimiza7ationz12zd2envzb5zzcfa_closurez00,
		BgL_bgl_za762closureza7d2opt2252z00,
		BGl_z62closurezd2optimiza7ationz12z05zzcfa_closurez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_approxzd2procedurezd2elzf3zd2envz21zzcfa_closurez00,
		BgL_bgl_za762approxza7d2proc2253z00,
		BGl_z62approxzd2procedurezd2elzf3z91zzcfa_closurez00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_za2procedurezd2lzd2setz12za2z12zzcfa_closurez00));
		     ADD_ROOT((void
				*) (&BGl_za2procedurezd2elzd2setz12za2z12zzcfa_closurez00));
		     ADD_ROOT((void
				*) (&BGl_za2makezd2elzd2procedureza2z00zzcfa_closurez00));
		     ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzcfa_closurez00));
		     ADD_ROOT((void
				*) (&BGl_za2makezd2lzd2procedureza2z00zzcfa_closurez00));
		     ADD_ROOT((void
				*) (&BGl_za2makezd2procedurezd2listza2z00zzcfa_closurez00));
		     ADD_ROOT((void *) (&BGl_za2procedurezd2lzd2refza2z00zzcfa_closurez00));
		   
			 ADD_ROOT((void
				*) (&BGl_za2procedurezd2refzd2listza2z00zzcfa_closurez00));
		     ADD_ROOT((void *) (&BGl_za2procedurezd2refza2zd2zzcfa_closurez00));
		     ADD_ROOT((void *) (&BGl_za2funcallzd2listza2zd2zzcfa_closurez00));
		     ADD_ROOT((void *) (&BGl_za2procedurezd2setz12za2zc0zzcfa_closurez00));
		   
			 ADD_ROOT((void *) (&BGl_za2procedurezd2elzd2refza2z00zzcfa_closurez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_closurez00(long
		BgL_checksumz00_4973, char *BgL_fromz00_4974)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_closurez00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_closurez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_closurez00();
					BGl_libraryzd2moduleszd2initz00zzcfa_closurez00();
					BGl_cnstzd2initzd2zzcfa_closurez00();
					BGl_importedzd2moduleszd2initz00zzcfa_closurez00();
					return BGl_toplevelzd2initzd2zzcfa_closurez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 25 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_closure");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_closure");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_closure");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_closure");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cfa_closure");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_closure");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_closure");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_closure");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cfa_closure");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 25 */
			{	/* Cfa/closure.scm 25 */
				obj_t BgL_cportz00_4932;

				{	/* Cfa/closure.scm 25 */
					obj_t BgL_stringz00_4939;

					BgL_stringz00_4939 = BGl_string2235z00zzcfa_closurez00;
					{	/* Cfa/closure.scm 25 */
						obj_t BgL_startz00_4940;

						BgL_startz00_4940 = BINT(0L);
						{	/* Cfa/closure.scm 25 */
							obj_t BgL_endz00_4941;

							BgL_endz00_4941 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4939)));
							{	/* Cfa/closure.scm 25 */

								BgL_cportz00_4932 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4939, BgL_startz00_4940, BgL_endz00_4941);
				}}}}
				{
					long BgL_iz00_4933;

					BgL_iz00_4933 = 19L;
				BgL_loopz00_4934:
					if ((BgL_iz00_4933 == -1L))
						{	/* Cfa/closure.scm 25 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/closure.scm 25 */
							{	/* Cfa/closure.scm 25 */
								obj_t BgL_arg2236z00_4935;

								{	/* Cfa/closure.scm 25 */

									{	/* Cfa/closure.scm 25 */
										obj_t BgL_locationz00_4937;

										BgL_locationz00_4937 = BBOOL(((bool_t) 0));
										{	/* Cfa/closure.scm 25 */

											BgL_arg2236z00_4935 =
												BGl_readz00zz__readerz00(BgL_cportz00_4932,
												BgL_locationz00_4937);
										}
									}
								}
								{	/* Cfa/closure.scm 25 */
									int BgL_tmpz00_5001;

									BgL_tmpz00_5001 = (int) (BgL_iz00_4933);
									CNST_TABLE_SET(BgL_tmpz00_5001, BgL_arg2236z00_4935);
							}}
							{	/* Cfa/closure.scm 25 */
								int BgL_auxz00_4938;

								BgL_auxz00_4938 = (int) ((BgL_iz00_4933 - 1L));
								{
									long BgL_iz00_5006;

									BgL_iz00_5006 = (long) (BgL_auxz00_4938);
									BgL_iz00_4933 = BgL_iz00_5006;
									goto BgL_loopz00_4934;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 25 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 25 */
			BGl_za2funcallzd2listza2zd2zzcfa_closurez00 = BNIL;
			BGl_za2makezd2procedurezd2listza2z00zzcfa_closurez00 = BNIL;
			BGl_za2procedurezd2refzd2listza2z00zzcfa_closurez00 = BNIL;
			BGl_za2procedurezd2refza2zd2zzcfa_closurez00 = BFALSE;
			BGl_za2procedurezd2setz12za2zc0zzcfa_closurez00 = BFALSE;
			BGl_za2procedurezd2lzd2refza2z00zzcfa_closurez00 = BFALSE;
			BGl_za2procedurezd2lzd2setz12za2z12zzcfa_closurez00 = BFALSE;
			BGl_za2procedurezd2elzd2refza2z00zzcfa_closurez00 = BFALSE;
			BGl_za2procedurezd2elzd2setz12za2z12zzcfa_closurez00 = BFALSE;
			BGl_za2makezd2elzd2procedureza2z00zzcfa_closurez00 = BFALSE;
			return (BGl_za2makezd2lzd2procedureza2z00zzcfa_closurez00 =
				BFALSE, BUNSPEC);
		}

	}



/* closure-optimization? */
	BGL_EXPORTED_DEF obj_t
		BGl_closurezd2optimiza7ationzf3z86zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 56 */
			{	/* Cfa/closure.scm 57 */
				long BgL_n1z00_4025;

				BgL_n1z00_4025 = (long) CINT(BGl_za2optimza2z00zzengine_paramz00);
				return BBOOL((BgL_n1z00_4025 >= 2L));
			}
		}

	}



/* &closure-optimization? */
	obj_t BGl_z62closurezd2optimiza7ationzf3ze4zzcfa_closurez00(obj_t
		BgL_envz00_4910)
	{
		{	/* Cfa/closure.scm 56 */
			return BGl_closurezd2optimiza7ationzf3z86zzcfa_closurez00();
		}

	}



/* closure-optimization! */
	BGL_EXPORTED_DEF obj_t
		BGl_closurezd2optimiza7ationz12z67zzcfa_closurez00(obj_t BgL_globalsz00_23)
	{
		{	/* Cfa/closure.scm 62 */
			if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 2L))
				{	/* Cfa/closure.scm 63 */
					{	/* Cfa/closure.scm 64 */
						obj_t BgL_list1607z00_3026;

						{	/* Cfa/closure.scm 64 */
							obj_t BgL_arg1609z00_3027;

							BgL_arg1609z00_3027 =
								MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
							BgL_list1607z00_3026 =
								MAKE_YOUNG_PAIR(BGl_string2228z00zzcfa_closurez00,
								BgL_arg1609z00_3027);
						}
						BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1607z00_3026);
					}
					BGl_startzd2closurezd2cachez00zzcfa_closurez00();
					{
						obj_t BgL_l1518z00_3029;

						BgL_l1518z00_3029 =
							BGl_za2makezd2procedurezd2listza2z00zzcfa_closurez00;
					BgL_zc3z04anonymousza31610ze3z87_3030:
						if (PAIRP(BgL_l1518z00_3029))
							{	/* Cfa/closure.scm 73 */
								{	/* Cfa/closure.scm 75 */
									obj_t BgL_appz00_3032;

									BgL_appz00_3032 = CAR(BgL_l1518z00_3029);
									{	/* Cfa/closure.scm 76 */
										BgL_variablez00_bglt BgL_funz00_3034;

										{
											BgL_varz00_bglt BgL_auxz00_5025;

											{	/* Cfa/closure.scm 76 */
												obj_t BgL_pairz00_4028;

												BgL_pairz00_4028 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt)
																((BgL_appz00_bglt) BgL_appz00_3032))))->
													BgL_argsz00);
												BgL_auxz00_5025 =
													((BgL_varz00_bglt) CAR(BgL_pairz00_4028));
											}
											BgL_funz00_3034 =
												(((BgL_varz00_bglt) COBJECT(BgL_auxz00_5025))->
												BgL_variablez00);
										}
										{	/* Cfa/closure.scm 76 */
											obj_t BgL_cloz00_3035;

											BgL_cloz00_3035 =
												(((BgL_funz00_bglt) COBJECT(
														((BgL_funz00_bglt)
															((BgL_sfunz00_bglt)
																(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_globalz00_bglt)
																					BgL_funz00_3034))))->
																	BgL_valuez00)))))->BgL_thezd2closurezd2);
											{	/* Cfa/closure.scm 77 */
												bool_t BgL_lostzf3zf3_3036;

												{	/* Cfa/closure.scm 78 */
													long BgL_arg1661z00_3060;

													{
														BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5038;

														{
															obj_t BgL_auxz00_5039;

															{	/* Cfa/closure.scm 78 */
																BgL_objectz00_bglt BgL_tmpz00_5040;

																BgL_tmpz00_5040 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt) BgL_appz00_3032));
																BgL_auxz00_5039 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5040);
															}
															BgL_auxz00_5038 =
																((BgL_makezd2procedurezd2appz00_bglt)
																BgL_auxz00_5039);
														}
														BgL_arg1661z00_3060 =
															(((BgL_makezd2procedurezd2appz00_bglt)
																COBJECT(BgL_auxz00_5038))->BgL_lostzd2stampzd2);
													}
													BgL_lostzf3zf3_3036 = (BgL_arg1661z00_3060 > -1L);
												}
												{	/* Cfa/closure.scm 78 */
													obj_t BgL_siza7eza7_3037;

													{	/* Cfa/closure.scm 79 */
														obj_t BgL_arg1651z00_3058;

														{	/* Cfa/closure.scm 79 */
															obj_t BgL_pairz00_4034;

															BgL_pairz00_4034 =
																(((BgL_appz00_bglt) COBJECT(
																		((BgL_appz00_bglt)
																			((BgL_appz00_bglt) BgL_appz00_3032))))->
																BgL_argsz00);
															BgL_arg1651z00_3058 =
																CAR(CDR(CDR(BgL_pairz00_4034)));
														}
														BgL_siza7eza7_3037 =
															BGl_getzd2nodezd2atomzd2valuezd2zzcfa_approxz00(
															((BgL_nodez00_bglt) BgL_arg1651z00_3058));
													}
													{	/* Cfa/closure.scm 79 */

														{	/* Cfa/closure.scm 81 */
															bool_t BgL_test2258z00_5055;

															if (BgL_lostzf3zf3_3036)
																{	/* Cfa/closure.scm 81 */
																	BgL_test2258z00_5055 = ((bool_t) 1);
																}
															else
																{	/* Cfa/closure.scm 82 */
																	bool_t BgL_test2260z00_5057;

																	{
																		BgL_makezd2procedurezd2appz00_bglt
																			BgL_auxz00_5058;
																		{
																			obj_t BgL_auxz00_5059;

																			{	/* Cfa/closure.scm 82 */
																				BgL_objectz00_bglt BgL_tmpz00_5060;

																				BgL_tmpz00_5060 =
																					((BgL_objectz00_bglt)
																					((BgL_appz00_bglt) BgL_appz00_3032));
																				BgL_auxz00_5059 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_5060);
																			}
																			BgL_auxz00_5058 =
																				((BgL_makezd2procedurezd2appz00_bglt)
																				BgL_auxz00_5059);
																		}
																		BgL_test2260z00_5057 =
																			(((BgL_makezd2procedurezd2appz00_bglt)
																				COBJECT(BgL_auxz00_5058))->
																			BgL_xzd2tzf3z21);
																	}
																	if (BgL_test2260z00_5057)
																		{	/* Cfa/closure.scm 82 */
																			if (
																				((((BgL_funz00_bglt) COBJECT(
																								((BgL_funz00_bglt)
																									((BgL_sfunz00_bglt)
																										(((BgL_variablez00_bglt)
																												COBJECT((
																														(BgL_variablez00_bglt)
																														((BgL_globalz00_bglt) BgL_funz00_3034))))->BgL_valuez00)))))->BgL_arityz00) < 0L))
																				{	/* Cfa/closure.scm 83 */
																					BgL_test2258z00_5055 = ((bool_t) 1);
																				}
																			else
																				{	/* Cfa/closure.scm 83 */
																					if (INTEGERP(BgL_siza7eza7_3037))
																						{	/* Cfa/closure.scm 85 */
																							bool_t BgL_test2263z00_5076;

																							{	/* Cfa/closure.scm 85 */
																								obj_t BgL_classz00_4044;

																								BgL_classz00_4044 =
																									BGl_globalz00zzast_varz00;
																								if (BGL_OBJECTP
																									(BgL_cloz00_3035))
																									{	/* Cfa/closure.scm 85 */
																										BgL_objectz00_bglt
																											BgL_arg1807z00_4046;
																										BgL_arg1807z00_4046 =
																											(BgL_objectz00_bglt)
																											(BgL_cloz00_3035);
																										if (BGL_CONDEXPAND_ISA_ARCH64())
																											{	/* Cfa/closure.scm 85 */
																												long BgL_idxz00_4052;

																												BgL_idxz00_4052 =
																													BGL_OBJECT_INHERITANCE_NUM
																													(BgL_arg1807z00_4046);
																												BgL_test2263z00_5076 =
																													(VECTOR_REF
																													(BGl_za2inheritancesza2z00zz__objectz00,
																														(BgL_idxz00_4052 +
																															2L)) ==
																													BgL_classz00_4044);
																											}
																										else
																											{	/* Cfa/closure.scm 85 */
																												bool_t
																													BgL_res2209z00_4077;
																												{	/* Cfa/closure.scm 85 */
																													obj_t
																														BgL_oclassz00_4060;
																													{	/* Cfa/closure.scm 85 */
																														obj_t
																															BgL_arg1815z00_4068;
																														long
																															BgL_arg1816z00_4069;
																														BgL_arg1815z00_4068
																															=
																															(BGl_za2classesza2z00zz__objectz00);
																														{	/* Cfa/closure.scm 85 */
																															long
																																BgL_arg1817z00_4070;
																															BgL_arg1817z00_4070
																																=
																																BGL_OBJECT_CLASS_NUM
																																(BgL_arg1807z00_4046);
																															BgL_arg1816z00_4069
																																=
																																(BgL_arg1817z00_4070
																																- OBJECT_TYPE);
																														}
																														BgL_oclassz00_4060 =
																															VECTOR_REF
																															(BgL_arg1815z00_4068,
																															BgL_arg1816z00_4069);
																													}
																													{	/* Cfa/closure.scm 85 */
																														bool_t
																															BgL__ortest_1115z00_4061;
																														BgL__ortest_1115z00_4061
																															=
																															(BgL_classz00_4044
																															==
																															BgL_oclassz00_4060);
																														if (BgL__ortest_1115z00_4061)
																															{	/* Cfa/closure.scm 85 */
																																BgL_res2209z00_4077
																																	=
																																	BgL__ortest_1115z00_4061;
																															}
																														else
																															{	/* Cfa/closure.scm 85 */
																																long
																																	BgL_odepthz00_4062;
																																{	/* Cfa/closure.scm 85 */
																																	obj_t
																																		BgL_arg1804z00_4063;
																																	BgL_arg1804z00_4063
																																		=
																																		(BgL_oclassz00_4060);
																																	BgL_odepthz00_4062
																																		=
																																		BGL_CLASS_DEPTH
																																		(BgL_arg1804z00_4063);
																																}
																																if (
																																	(2L <
																																		BgL_odepthz00_4062))
																																	{	/* Cfa/closure.scm 85 */
																																		obj_t
																																			BgL_arg1802z00_4065;
																																		{	/* Cfa/closure.scm 85 */
																																			obj_t
																																				BgL_arg1803z00_4066;
																																			BgL_arg1803z00_4066
																																				=
																																				(BgL_oclassz00_4060);
																																			BgL_arg1802z00_4065
																																				=
																																				BGL_CLASS_ANCESTORS_REF
																																				(BgL_arg1803z00_4066,
																																				2L);
																																		}
																																		BgL_res2209z00_4077
																																			=
																																			(BgL_arg1802z00_4065
																																			==
																																			BgL_classz00_4044);
																																	}
																																else
																																	{	/* Cfa/closure.scm 85 */
																																		BgL_res2209z00_4077
																																			=
																																			((bool_t)
																																			0);
																																	}
																															}
																													}
																												}
																												BgL_test2263z00_5076 =
																													BgL_res2209z00_4077;
																											}
																									}
																								else
																									{	/* Cfa/closure.scm 85 */
																										BgL_test2263z00_5076 =
																											((bool_t) 0);
																									}
																							}
																							if (BgL_test2263z00_5076)
																								{	/* Cfa/closure.scm 85 */
																									if (
																										((((BgL_globalz00_bglt)
																													COBJECT((
																															(BgL_globalz00_bglt)
																															BgL_cloz00_3035)))->
																												BgL_importz00) ==
																											CNST_TABLE_REF(0)))
																										{	/* Cfa/closure.scm 86 */
																											BgL_test2258z00_5055 =
																												((bool_t) 0);
																										}
																									else
																										{	/* Cfa/closure.scm 86 */
																											BgL_test2258z00_5055 =
																												((bool_t) 1);
																										}
																								}
																							else
																								{	/* Cfa/closure.scm 85 */
																									BgL_test2258z00_5055 =
																										((bool_t) 0);
																								}
																						}
																					else
																						{	/* Cfa/closure.scm 84 */
																							BgL_test2258z00_5055 =
																								((bool_t) 1);
																						}
																				}
																		}
																	else
																		{	/* Cfa/closure.scm 82 */
																			BgL_test2258z00_5055 = ((bool_t) 1);
																		}
																}
															if (BgL_test2258z00_5055)
																{	/* Cfa/closure.scm 81 */
																	BNIL;
																}
															else
																{	/* Cfa/closure.scm 81 */
																	{
																		BgL_makezd2procedurezd2appz00_bglt
																			BgL_auxz00_5104;
																		{
																			obj_t BgL_auxz00_5105;

																			{	/* Cfa/closure.scm 103 */
																				BgL_objectz00_bglt BgL_tmpz00_5106;

																				BgL_tmpz00_5106 =
																					((BgL_objectz00_bglt)
																					((BgL_appz00_bglt) BgL_appz00_3032));
																				BgL_auxz00_5105 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_5106);
																			}
																			BgL_auxz00_5104 =
																				((BgL_makezd2procedurezd2appz00_bglt)
																				BgL_auxz00_5105);
																		}
																		((((BgL_makezd2procedurezd2appz00_bglt)
																					COBJECT(BgL_auxz00_5104))->BgL_xz00) =
																			((bool_t) ((bool_t) 1)), BUNSPEC);
																	}
																	{
																		BgL_makezd2procedurezd2appz00_bglt
																			BgL_auxz00_5112;
																		{
																			obj_t BgL_auxz00_5113;

																			{	/* Cfa/closure.scm 104 */
																				BgL_objectz00_bglt BgL_tmpz00_5114;

																				BgL_tmpz00_5114 =
																					((BgL_objectz00_bglt)
																					((BgL_appz00_bglt) BgL_appz00_3032));
																				BgL_auxz00_5113 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_5114);
																			}
																			BgL_auxz00_5112 =
																				((BgL_makezd2procedurezd2appz00_bglt)
																				BgL_auxz00_5113);
																		}
																		((((BgL_makezd2procedurezd2appz00_bglt)
																					COBJECT(BgL_auxz00_5112))->BgL_tz00) =
																			((bool_t) ((bool_t) 1)), BUNSPEC);
																	}
																}
														}
													}
												}
											}
										}
									}
								}
								{
									obj_t BgL_l1518z00_5120;

									BgL_l1518z00_5120 = CDR(BgL_l1518z00_3029);
									BgL_l1518z00_3029 = BgL_l1518z00_5120;
									goto BgL_zc3z04anonymousza31610ze3z87_3030;
								}
							}
						else
							{	/* Cfa/closure.scm 73 */
								((bool_t) 1);
							}
					}
					BGl_Xz12z12zzcfa_closurez00
						(BGl_za2funcallzd2listza2zd2zzcfa_closurez00);
					BGl_Tzd2fixzd2pointz12z12zzcfa_closurez00
						(BGl_za2funcallzd2listza2zd2zzcfa_closurez00);
					{
						obj_t BgL_l1520z00_3067;

						BgL_l1520z00_3067 =
							BGl_za2makezd2procedurezd2listza2z00zzcfa_closurez00;
					BgL_zc3z04anonymousza31682ze3z87_3068:
						if (PAIRP(BgL_l1520z00_3067))
							{	/* Cfa/closure.scm 111 */
								{	/* Cfa/closure.scm 112 */
									obj_t BgL_allocz00_3070;

									BgL_allocz00_3070 = CAR(BgL_l1520z00_3067);
									{	/* Cfa/closure.scm 113 */
										BgL_valuez00_bglt BgL_fz00_3072;

										{
											BgL_variablez00_bglt BgL_auxz00_5127;

											{
												BgL_varz00_bglt BgL_auxz00_5128;

												{	/* Cfa/closure.scm 113 */
													obj_t BgL_pairz00_4083;

													BgL_pairz00_4083 =
														(((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt)
																	((BgL_appz00_bglt) BgL_allocz00_3070))))->
														BgL_argsz00);
													BgL_auxz00_5128 =
														((BgL_varz00_bglt) CAR(BgL_pairz00_4083));
												}
												BgL_auxz00_5127 =
													(((BgL_varz00_bglt) COBJECT(BgL_auxz00_5128))->
													BgL_variablez00);
											}
											BgL_fz00_3072 =
												(((BgL_variablez00_bglt) COBJECT(BgL_auxz00_5127))->
												BgL_valuez00);
										}
										{	/* Cfa/closure.scm 115 */
											bool_t BgL_test2270z00_5136;

											{
												BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5137;

												{
													obj_t BgL_auxz00_5138;

													{	/* Cfa/closure.scm 115 */
														BgL_objectz00_bglt BgL_tmpz00_5139;

														BgL_tmpz00_5139 =
															((BgL_objectz00_bglt)
															((BgL_appz00_bglt) BgL_allocz00_3070));
														BgL_auxz00_5138 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5139);
													}
													BgL_auxz00_5137 =
														((BgL_makezd2procedurezd2appz00_bglt)
														BgL_auxz00_5138);
												}
												BgL_test2270z00_5136 =
													(((BgL_makezd2procedurezd2appz00_bglt)
														COBJECT(BgL_auxz00_5137))->BgL_xz00);
											}
											if (BgL_test2270z00_5136)
												{	/* Cfa/closure.scm 115 */
													obj_t BgL_vz00_4088;

													BgL_vz00_4088 = CNST_TABLE_REF(1);
													((((BgL_sfunz00_bglt) COBJECT(
																	((BgL_sfunz00_bglt) BgL_fz00_3072)))->
															BgL_strengthz00) =
														((obj_t) BgL_vz00_4088), BUNSPEC);
												}
											else
												{	/* Cfa/closure.scm 116 */
													bool_t BgL_test2271z00_5148;

													{
														BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5149;

														{
															obj_t BgL_auxz00_5150;

															{	/* Cfa/closure.scm 116 */
																BgL_objectz00_bglt BgL_tmpz00_5151;

																BgL_tmpz00_5151 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt) BgL_allocz00_3070));
																BgL_auxz00_5150 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5151);
															}
															BgL_auxz00_5149 =
																((BgL_makezd2procedurezd2appz00_bglt)
																BgL_auxz00_5150);
														}
														BgL_test2271z00_5148 =
															(((BgL_makezd2procedurezd2appz00_bglt)
																COBJECT(BgL_auxz00_5149))->BgL_tz00);
													}
													if (BgL_test2271z00_5148)
														{	/* Cfa/closure.scm 116 */
															obj_t BgL_vz00_4091;

															BgL_vz00_4091 = CNST_TABLE_REF(2);
															((((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt) BgL_fz00_3072)))->
																	BgL_strengthz00) =
																((obj_t) BgL_vz00_4091), BUNSPEC);
														}
													else
														{	/* Cfa/closure.scm 117 */
															obj_t BgL_vz00_4093;

															BgL_vz00_4093 = CNST_TABLE_REF(3);
															((((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt) BgL_fz00_3072)))->
																	BgL_strengthz00) =
																((obj_t) BgL_vz00_4093), BUNSPEC);
														}
												}
										}
									}
								}
								{
									obj_t BgL_l1520z00_5163;

									BgL_l1520z00_5163 = CDR(BgL_l1520z00_3067);
									BgL_l1520z00_3067 = BgL_l1520z00_5163;
									goto BgL_zc3z04anonymousza31682ze3z87_3068;
								}
							}
						else
							{	/* Cfa/closure.scm 111 */
								((bool_t) 1);
							}
					}
					BGl_showzd2Xzd2Tz00zzcfa_closurez00
						(BGl_za2makezd2procedurezd2listza2z00zzcfa_closurez00);
					BGl_lightzd2funcallz12zc0zzcfa_closurez00();
					BGl_lightzd2accessz12zc0zzcfa_closurez00();
					BGl_lightzd2makezd2procedurez12z12zzcfa_closurez00();
					return BGl_lightzd2typez12zc0zzcfa_ltypez00(BgL_globalsz00_23);
				}
			else
				{	/* Cfa/closure.scm 63 */
					return BFALSE;
				}
		}

	}



/* &closure-optimization! */
	obj_t BGl_z62closurezd2optimiza7ationz12z05zzcfa_closurez00(obj_t
		BgL_envz00_4911, obj_t BgL_globalsz00_4912)
	{
		{	/* Cfa/closure.scm 62 */
			return
				BGl_closurezd2optimiza7ationz12z67zzcfa_closurez00(BgL_globalsz00_4912);
		}

	}



/* X! */
	bool_t BGl_Xz12z12zzcfa_closurez00(obj_t BgL_funcallzd2listzd2_24)
	{
		{	/* Cfa/closure.scm 133 */
			{
				obj_t BgL_l1522z00_3081;

				BgL_l1522z00_3081 = BgL_funcallzd2listzd2_24;
			BgL_zc3z04anonymousza31693ze3z87_3082:
				if (PAIRP(BgL_l1522z00_3081))
					{	/* Cfa/closure.scm 135 */
						{	/* Cfa/closure.scm 136 */
							BgL_funcallz00_bglt BgL_appz00_3084;

							BgL_appz00_3084 = ((BgL_funcallz00_bglt) CAR(BgL_l1522z00_3081));
							{	/* Cfa/closure.scm 137 */
								BgL_nodez00_bglt BgL_funz00_3085;

								BgL_funz00_3085 =
									(((BgL_funcallz00_bglt) COBJECT(BgL_appz00_3084))->
									BgL_funz00);
								{	/* Cfa/closure.scm 137 */
									BgL_approxz00_bglt BgL_approxz00_3086;

									BgL_approxz00_3086 =
										BGl_cfaz12z12zzcfa_cfaz00(BgL_funz00_3085);
									{	/* Cfa/closure.scm 138 */
										obj_t BgL_allocz00_3087;

										BgL_allocz00_3087 =
											(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_3086))->
											BgL_allocsz00);
										{	/* Cfa/closure.scm 139 */
											BgL_typez00_bglt BgL_typez00_3088;

											BgL_typez00_3088 =
												(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_3086))->
												BgL_typez00);
											{	/* Cfa/closure.scm 140 */
												bool_t BgL_topzf3zf3_3089;

												BgL_topzf3zf3_3089 =
													(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_3086))->
													BgL_topzf3zf3);
												{	/* Cfa/closure.scm 141 */

													if (BgL_topzf3zf3_3089)
														{	/* Cfa/closure.scm 143 */
															if (
																(((BgL_approxz00_bglt)
																		COBJECT(BgL_approxz00_3086))->
																	BgL_haszd2procedurezf3z21))
																{	/* Cfa/closure.scm 146 */
																	BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
																		(BGl_proc2229z00zzcfa_closurez00,
																		BgL_approxz00_3086);
																}
															else
																{	/* Cfa/closure.scm 146 */
																	BFALSE;
																}
														}
													else
														{	/* Cfa/closure.scm 156 */
															bool_t BgL_test2275z00_5184;

															{	/* Cfa/closure.scm 156 */
																obj_t BgL_arg1718z00_3112;

																BgL_arg1718z00_3112 =
																	BGl_setzd2lengthzd2zzcfa_setz00
																	(BgL_allocz00_3087);
																BgL_test2275z00_5184 =
																	((long) CINT(BgL_arg1718z00_3112) == 0L);
															}
															if (BgL_test2275z00_5184)
																{	/* Cfa/closure.scm 156 */
																	CNST_TABLE_REF(4);
																}
															else
																{	/* Cfa/closure.scm 159 */
																	bool_t BgL_test2276z00_5189;

																	{	/* Cfa/closure.scm 159 */
																		bool_t BgL_test2277z00_5190;

																		{	/* Cfa/closure.scm 159 */
																			obj_t BgL_arg1717z00_3111;

																			BgL_arg1717z00_3111 =
																				BGl_setzd2lengthzd2zzcfa_setz00
																				(BgL_allocz00_3087);
																			BgL_test2277z00_5190 =
																				((long) CINT(BgL_arg1717z00_3111) ==
																				1L);
																		}
																		if (BgL_test2277z00_5190)
																			{	/* Cfa/closure.scm 160 */
																				obj_t BgL__ortest_1165z00_3110;

																				BgL__ortest_1165z00_3110 =
																					BGl_za2unsafezd2typeza2zd2zzengine_paramz00;
																				if (CBOOL(BgL__ortest_1165z00_3110))
																					{	/* Cfa/closure.scm 160 */
																						BgL_test2276z00_5189 =
																							CBOOL(BgL__ortest_1165z00_3110);
																					}
																				else
																					{	/* Cfa/closure.scm 160 */
																						BgL_test2276z00_5189 =
																							(
																							((obj_t) BgL_typez00_3088) ==
																							BGl_za2procedureza2z00zztype_cachez00);
																					}
																			}
																		else
																			{	/* Cfa/closure.scm 159 */
																				BgL_test2276z00_5189 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test2276z00_5189)
																		{	/* Cfa/closure.scm 159 */
																			CNST_TABLE_REF(4);
																		}
																	else
																		{	/* Cfa/closure.scm 159 */
																			if (
																				(((BgL_approxz00_bglt)
																						COBJECT(BgL_approxz00_3086))->
																					BgL_haszd2procedurezf3z21))
																				{	/* Cfa/closure.scm 167 */
																					BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
																						(BGl_proc2230z00zzcfa_closurez00,
																						BgL_approxz00_3086);
																				}
																			else
																				{	/* Cfa/closure.scm 167 */
																					BFALSE;
																				}
																		}
																}
														}
												}
											}
										}
									}
								}
							}
						}
						{
							obj_t BgL_l1522z00_5203;

							BgL_l1522z00_5203 = CDR(BgL_l1522z00_3081);
							BgL_l1522z00_3081 = BgL_l1522z00_5203;
							goto BgL_zc3z04anonymousza31693ze3z87_3082;
						}
					}
				else
					{	/* Cfa/closure.scm 135 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* &<@anonymous:1700> */
	obj_t BGl_z62zc3z04anonymousza31700ze3ze5zzcfa_closurez00(obj_t
		BgL_envz00_4915, obj_t BgL_allocz00_4916)
	{
		{	/* Cfa/closure.scm 148 */
			{	/* Cfa/closure.scm 149 */
				bool_t BgL_test2280z00_5205;

				{	/* Cfa/closure.scm 149 */
					obj_t BgL_classz00_4943;

					BgL_classz00_4943 = BGl_makezd2procedurezd2appz00zzcfa_info2z00;
					if (BGL_OBJECTP(BgL_allocz00_4916))
						{	/* Cfa/closure.scm 149 */
							BgL_objectz00_bglt BgL_arg1807z00_4944;

							BgL_arg1807z00_4944 = (BgL_objectz00_bglt) (BgL_allocz00_4916);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/closure.scm 149 */
									long BgL_idxz00_4945;

									BgL_idxz00_4945 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4944);
									BgL_test2280z00_5205 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4945 + 4L)) == BgL_classz00_4943);
								}
							else
								{	/* Cfa/closure.scm 149 */
									bool_t BgL_res2210z00_4948;

									{	/* Cfa/closure.scm 149 */
										obj_t BgL_oclassz00_4949;

										{	/* Cfa/closure.scm 149 */
											obj_t BgL_arg1815z00_4950;
											long BgL_arg1816z00_4951;

											BgL_arg1815z00_4950 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/closure.scm 149 */
												long BgL_arg1817z00_4952;

												BgL_arg1817z00_4952 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4944);
												BgL_arg1816z00_4951 =
													(BgL_arg1817z00_4952 - OBJECT_TYPE);
											}
											BgL_oclassz00_4949 =
												VECTOR_REF(BgL_arg1815z00_4950, BgL_arg1816z00_4951);
										}
										{	/* Cfa/closure.scm 149 */
											bool_t BgL__ortest_1115z00_4953;

											BgL__ortest_1115z00_4953 =
												(BgL_classz00_4943 == BgL_oclassz00_4949);
											if (BgL__ortest_1115z00_4953)
												{	/* Cfa/closure.scm 149 */
													BgL_res2210z00_4948 = BgL__ortest_1115z00_4953;
												}
											else
												{	/* Cfa/closure.scm 149 */
													long BgL_odepthz00_4954;

													{	/* Cfa/closure.scm 149 */
														obj_t BgL_arg1804z00_4955;

														BgL_arg1804z00_4955 = (BgL_oclassz00_4949);
														BgL_odepthz00_4954 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4955);
													}
													if ((4L < BgL_odepthz00_4954))
														{	/* Cfa/closure.scm 149 */
															obj_t BgL_arg1802z00_4956;

															{	/* Cfa/closure.scm 149 */
																obj_t BgL_arg1803z00_4957;

																BgL_arg1803z00_4957 = (BgL_oclassz00_4949);
																BgL_arg1802z00_4956 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4957,
																	4L);
															}
															BgL_res2210z00_4948 =
																(BgL_arg1802z00_4956 == BgL_classz00_4943);
														}
													else
														{	/* Cfa/closure.scm 149 */
															BgL_res2210z00_4948 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2280z00_5205 = BgL_res2210z00_4948;
								}
						}
					else
						{	/* Cfa/closure.scm 149 */
							BgL_test2280z00_5205 = ((bool_t) 0);
						}
				}
				if (BgL_test2280z00_5205)
					{	/* Cfa/closure.scm 149 */
						{
							BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5228;

							{
								obj_t BgL_auxz00_5229;

								{	/* Cfa/closure.scm 153 */
									BgL_objectz00_bglt BgL_tmpz00_5230;

									BgL_tmpz00_5230 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) BgL_allocz00_4916));
									BgL_auxz00_5229 = BGL_OBJECT_WIDENING(BgL_tmpz00_5230);
								}
								BgL_auxz00_5228 =
									((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_5229);
							}
							((((BgL_makezd2procedurezd2appz00_bglt)
										COBJECT(BgL_auxz00_5228))->BgL_tz00) =
								((bool_t) ((bool_t) 0)), BUNSPEC);
						}
						{
							BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5236;

							{
								obj_t BgL_auxz00_5237;

								{	/* Cfa/closure.scm 154 */
									BgL_objectz00_bglt BgL_tmpz00_5238;

									BgL_tmpz00_5238 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) BgL_allocz00_4916));
									BgL_auxz00_5237 = BGL_OBJECT_WIDENING(BgL_tmpz00_5238);
								}
								BgL_auxz00_5236 =
									((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_5237);
							}
							return
								((((BgL_makezd2procedurezd2appz00_bglt)
										COBJECT(BgL_auxz00_5236))->BgL_xz00) =
								((bool_t) ((bool_t) 0)), BUNSPEC);
						}
					}
				else
					{	/* Cfa/closure.scm 149 */
						return BFALSE;
					}
			}
		}

	}



/* &<@anonymous:1715> */
	obj_t BGl_z62zc3z04anonymousza31715ze3ze5zzcfa_closurez00(obj_t
		BgL_envz00_4917, obj_t BgL_allocz00_4918)
	{
		{	/* Cfa/closure.scm 169 */
			{	/* Cfa/closure.scm 170 */
				bool_t BgL_test2285z00_5244;

				{	/* Cfa/closure.scm 170 */
					obj_t BgL_classz00_4958;

					BgL_classz00_4958 = BGl_makezd2procedurezd2appz00zzcfa_info2z00;
					if (BGL_OBJECTP(BgL_allocz00_4918))
						{	/* Cfa/closure.scm 170 */
							BgL_objectz00_bglt BgL_arg1807z00_4959;

							BgL_arg1807z00_4959 = (BgL_objectz00_bglt) (BgL_allocz00_4918);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/closure.scm 170 */
									long BgL_idxz00_4960;

									BgL_idxz00_4960 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4959);
									BgL_test2285z00_5244 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4960 + 4L)) == BgL_classz00_4958);
								}
							else
								{	/* Cfa/closure.scm 170 */
									bool_t BgL_res2211z00_4963;

									{	/* Cfa/closure.scm 170 */
										obj_t BgL_oclassz00_4964;

										{	/* Cfa/closure.scm 170 */
											obj_t BgL_arg1815z00_4965;
											long BgL_arg1816z00_4966;

											BgL_arg1815z00_4965 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/closure.scm 170 */
												long BgL_arg1817z00_4967;

												BgL_arg1817z00_4967 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4959);
												BgL_arg1816z00_4966 =
													(BgL_arg1817z00_4967 - OBJECT_TYPE);
											}
											BgL_oclassz00_4964 =
												VECTOR_REF(BgL_arg1815z00_4965, BgL_arg1816z00_4966);
										}
										{	/* Cfa/closure.scm 170 */
											bool_t BgL__ortest_1115z00_4968;

											BgL__ortest_1115z00_4968 =
												(BgL_classz00_4958 == BgL_oclassz00_4964);
											if (BgL__ortest_1115z00_4968)
												{	/* Cfa/closure.scm 170 */
													BgL_res2211z00_4963 = BgL__ortest_1115z00_4968;
												}
											else
												{	/* Cfa/closure.scm 170 */
													long BgL_odepthz00_4969;

													{	/* Cfa/closure.scm 170 */
														obj_t BgL_arg1804z00_4970;

														BgL_arg1804z00_4970 = (BgL_oclassz00_4964);
														BgL_odepthz00_4969 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4970);
													}
													if ((4L < BgL_odepthz00_4969))
														{	/* Cfa/closure.scm 170 */
															obj_t BgL_arg1802z00_4971;

															{	/* Cfa/closure.scm 170 */
																obj_t BgL_arg1803z00_4972;

																BgL_arg1803z00_4972 = (BgL_oclassz00_4964);
																BgL_arg1802z00_4971 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4972,
																	4L);
															}
															BgL_res2211z00_4963 =
																(BgL_arg1802z00_4971 == BgL_classz00_4958);
														}
													else
														{	/* Cfa/closure.scm 170 */
															BgL_res2211z00_4963 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2285z00_5244 = BgL_res2211z00_4963;
								}
						}
					else
						{	/* Cfa/closure.scm 170 */
							BgL_test2285z00_5244 = ((bool_t) 0);
						}
				}
				if (BgL_test2285z00_5244)
					{	/* Cfa/closure.scm 170 */
						{
							BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5267;

							{
								obj_t BgL_auxz00_5268;

								{	/* Cfa/closure.scm 174 */
									BgL_objectz00_bglt BgL_tmpz00_5269;

									BgL_tmpz00_5269 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) BgL_allocz00_4918));
									BgL_auxz00_5268 = BGL_OBJECT_WIDENING(BgL_tmpz00_5269);
								}
								BgL_auxz00_5267 =
									((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_5268);
							}
							return
								((((BgL_makezd2procedurezd2appz00_bglt)
										COBJECT(BgL_auxz00_5267))->BgL_xz00) =
								((bool_t) ((bool_t) 0)), BUNSPEC);
						}
					}
				else
					{	/* Cfa/closure.scm 170 */
						return BFALSE;
					}
			}
		}

	}



/* T-fix-point! */
	obj_t BGl_Tzd2fixzd2pointz12z12zzcfa_closurez00(obj_t
		BgL_funcallzd2listzd2_25)
	{
		{	/* Cfa/closure.scm 187 */
			{
				bool_t BgL_continuezf3zf3_3116;

				BgL_continuezf3zf3_3116 = ((bool_t) 1);
			BgL_zc3z04anonymousza31721ze3z87_3117:
				if (BgL_continuezf3zf3_3116)
					{	/* Cfa/closure.scm 192 */
						bool_t BgL_continuezf3zf3_3118;

						BgL_continuezf3zf3_3118 = ((bool_t) 0);
						{
							obj_t BgL_l1526z00_3120;

							BgL_l1526z00_3120 = BgL_funcallzd2listzd2_25;
						BgL_zc3z04anonymousza31722ze3z87_3121:
							if (PAIRP(BgL_l1526z00_3120))
								{	/* Cfa/closure.scm 193 */
									{	/* Cfa/closure.scm 195 */
										obj_t BgL_appz00_3123;

										BgL_appz00_3123 = CAR(BgL_l1526z00_3120);
										{	/* Cfa/closure.scm 196 */
											BgL_nodez00_bglt BgL_funz00_3124;

											BgL_funz00_3124 =
												(((BgL_funcallz00_bglt) COBJECT(
														((BgL_funcallz00_bglt) BgL_appz00_3123)))->
												BgL_funz00);
											{	/* Cfa/closure.scm 196 */
												BgL_approxz00_bglt BgL_approxz00_3125;

												BgL_approxz00_3125 =
													BGl_cfaz12z12zzcfa_cfaz00(BgL_funz00_3124);
												{	/* Cfa/closure.scm 197 */
													obj_t BgL_allocz00_3126;

													BgL_allocz00_3126 =
														BGl_setzd2ze3listz31zzcfa_setz00(
														(((BgL_approxz00_bglt)
																COBJECT(BgL_approxz00_3125))->BgL_allocsz00));
													{	/* Cfa/closure.scm 198 */
														BgL_typez00_bglt BgL_typez00_3127;

														BgL_typez00_3127 =
															(((BgL_approxz00_bglt)
																COBJECT(BgL_approxz00_3125))->BgL_typez00);
														{	/* Cfa/closure.scm 199 */
															bool_t BgL_tzd2initzf3z21_3128;

															{	/* Cfa/closure.scm 200 */
																bool_t BgL__ortest_1166z00_3152;

																BgL__ortest_1166z00_3152 =
																	(((BgL_approxz00_bglt)
																		COBJECT(BgL_approxz00_3125))->
																	BgL_topzf3zf3);
																if (BgL__ortest_1166z00_3152)
																	{	/* Cfa/closure.scm 200 */
																		BgL_tzd2initzf3z21_3128 =
																			BgL__ortest_1166z00_3152;
																	}
																else
																	{	/* Cfa/closure.scm 201 */
																		bool_t BgL_test2293z00_5287;

																		if (
																			(((obj_t) BgL_typez00_3127) ==
																				BGl_za2procedureza2z00zztype_cachez00))
																			{	/* Cfa/closure.scm 201 */
																				BgL_test2293z00_5287 = ((bool_t) 1);
																			}
																		else
																			{	/* Cfa/closure.scm 201 */
																				BgL_test2293z00_5287 =
																					CBOOL
																					(BGl_za2unsafezd2typeza2zd2zzengine_paramz00);
																			}
																		if (BgL_test2293z00_5287)
																			{	/* Cfa/closure.scm 201 */
																				BgL_tzd2initzf3z21_3128 = ((bool_t) 0);
																			}
																		else
																			{	/* Cfa/closure.scm 201 */
																				BgL_tzd2initzf3z21_3128 = ((bool_t) 1);
																			}
																	}
															}
															{	/* Cfa/closure.scm 200 */

																{
																	bool_t BgL_onezd2nonzd2tzf3zf3_3130;
																	obj_t BgL_allocsz00_3131;

																	BgL_onezd2nonzd2tzf3zf3_3130 =
																		BgL_tzd2initzf3z21_3128;
																	BgL_allocsz00_3131 = BgL_allocz00_3126;
																BgL_zc3z04anonymousza31724ze3z87_3132:
																	if (NULLP(BgL_allocsz00_3131))
																		{	/* Cfa/closure.scm 209 */
																			CNST_TABLE_REF(5);
																		}
																	else
																		{	/* Cfa/closure.scm 209 */
																			if (BgL_onezd2nonzd2tzf3zf3_3130)
																				{
																					obj_t BgL_l1524z00_3135;

																					{	/* Cfa/closure.scm 212 */
																						bool_t BgL_tmpz00_5296;

																						BgL_l1524z00_3135 =
																							BgL_allocz00_3126;
																					BgL_zc3z04anonymousza31726ze3z87_3136:
																						if (PAIRP
																							(BgL_l1524z00_3135))
																							{	/* Cfa/closure.scm 212 */
																								{	/* Cfa/closure.scm 214 */
																									obj_t BgL_allocz00_3138;

																									BgL_allocz00_3138 =
																										CAR(BgL_l1524z00_3135);
																									{	/* Cfa/closure.scm 214 */
																										bool_t BgL_test2298z00_5300;

																										{	/* Cfa/closure.scm 214 */
																											obj_t BgL_classz00_4188;

																											BgL_classz00_4188 =
																												BGl_makezd2procedurezd2appz00zzcfa_info2z00;
																											if (BGL_OBJECTP
																												(BgL_allocz00_3138))
																												{	/* Cfa/closure.scm 214 */
																													BgL_objectz00_bglt
																														BgL_arg1807z00_4190;
																													BgL_arg1807z00_4190 =
																														(BgL_objectz00_bglt)
																														(BgL_allocz00_3138);
																													if (BGL_CONDEXPAND_ISA_ARCH64())
																														{	/* Cfa/closure.scm 214 */
																															long
																																BgL_idxz00_4196;
																															BgL_idxz00_4196 =
																																BGL_OBJECT_INHERITANCE_NUM
																																(BgL_arg1807z00_4190);
																															BgL_test2298z00_5300
																																=
																																(VECTOR_REF
																																(BGl_za2inheritancesza2z00zz__objectz00,
																																	(BgL_idxz00_4196
																																		+ 4L)) ==
																																BgL_classz00_4188);
																														}
																													else
																														{	/* Cfa/closure.scm 214 */
																															bool_t
																																BgL_res2212z00_4221;
																															{	/* Cfa/closure.scm 214 */
																																obj_t
																																	BgL_oclassz00_4204;
																																{	/* Cfa/closure.scm 214 */
																																	obj_t
																																		BgL_arg1815z00_4212;
																																	long
																																		BgL_arg1816z00_4213;
																																	BgL_arg1815z00_4212
																																		=
																																		(BGl_za2classesza2z00zz__objectz00);
																																	{	/* Cfa/closure.scm 214 */
																																		long
																																			BgL_arg1817z00_4214;
																																		BgL_arg1817z00_4214
																																			=
																																			BGL_OBJECT_CLASS_NUM
																																			(BgL_arg1807z00_4190);
																																		BgL_arg1816z00_4213
																																			=
																																			(BgL_arg1817z00_4214
																																			-
																																			OBJECT_TYPE);
																																	}
																																	BgL_oclassz00_4204
																																		=
																																		VECTOR_REF
																																		(BgL_arg1815z00_4212,
																																		BgL_arg1816z00_4213);
																																}
																																{	/* Cfa/closure.scm 214 */
																																	bool_t
																																		BgL__ortest_1115z00_4205;
																																	BgL__ortest_1115z00_4205
																																		=
																																		(BgL_classz00_4188
																																		==
																																		BgL_oclassz00_4204);
																																	if (BgL__ortest_1115z00_4205)
																																		{	/* Cfa/closure.scm 214 */
																																			BgL_res2212z00_4221
																																				=
																																				BgL__ortest_1115z00_4205;
																																		}
																																	else
																																		{	/* Cfa/closure.scm 214 */
																																			long
																																				BgL_odepthz00_4206;
																																			{	/* Cfa/closure.scm 214 */
																																				obj_t
																																					BgL_arg1804z00_4207;
																																				BgL_arg1804z00_4207
																																					=
																																					(BgL_oclassz00_4204);
																																				BgL_odepthz00_4206
																																					=
																																					BGL_CLASS_DEPTH
																																					(BgL_arg1804z00_4207);
																																			}
																																			if (
																																				(4L <
																																					BgL_odepthz00_4206))
																																				{	/* Cfa/closure.scm 214 */
																																					obj_t
																																						BgL_arg1802z00_4209;
																																					{	/* Cfa/closure.scm 214 */
																																						obj_t
																																							BgL_arg1803z00_4210;
																																						BgL_arg1803z00_4210
																																							=
																																							(BgL_oclassz00_4204);
																																						BgL_arg1802z00_4209
																																							=
																																							BGL_CLASS_ANCESTORS_REF
																																							(BgL_arg1803z00_4210,
																																							4L);
																																					}
																																					BgL_res2212z00_4221
																																						=
																																						(BgL_arg1802z00_4209
																																						==
																																						BgL_classz00_4188);
																																				}
																																			else
																																				{	/* Cfa/closure.scm 214 */
																																					BgL_res2212z00_4221
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																															}
																															BgL_test2298z00_5300
																																=
																																BgL_res2212z00_4221;
																														}
																												}
																											else
																												{	/* Cfa/closure.scm 214 */
																													BgL_test2298z00_5300 =
																														((bool_t) 0);
																												}
																										}
																										if (BgL_test2298z00_5300)
																											{	/* Cfa/closure.scm 216 */
																												bool_t
																													BgL_test2303z00_5323;
																												{
																													BgL_makezd2procedurezd2appz00_bglt
																														BgL_auxz00_5324;
																													{
																														obj_t
																															BgL_auxz00_5325;
																														{	/* Cfa/closure.scm 216 */
																															BgL_objectz00_bglt
																																BgL_tmpz00_5326;
																															BgL_tmpz00_5326 =
																																(
																																(BgL_objectz00_bglt)
																																((BgL_appz00_bglt) BgL_allocz00_3138));
																															BgL_auxz00_5325 =
																																BGL_OBJECT_WIDENING
																																(BgL_tmpz00_5326);
																														}
																														BgL_auxz00_5324 =
																															(
																															(BgL_makezd2procedurezd2appz00_bglt)
																															BgL_auxz00_5325);
																													}
																													BgL_test2303z00_5323 =
																														(((BgL_makezd2procedurezd2appz00_bglt) COBJECT(BgL_auxz00_5324))->BgL_tz00);
																												}
																												if (BgL_test2303z00_5323)
																													{	/* Cfa/closure.scm 216 */
																														{
																															BgL_makezd2procedurezd2appz00_bglt
																																BgL_auxz00_5332;
																															{
																																obj_t
																																	BgL_auxz00_5333;
																																{	/* Cfa/closure.scm 217 */
																																	BgL_objectz00_bglt
																																		BgL_tmpz00_5334;
																																	BgL_tmpz00_5334
																																		=
																																		(
																																		(BgL_objectz00_bglt)
																																		((BgL_appz00_bglt) BgL_allocz00_3138));
																																	BgL_auxz00_5333
																																		=
																																		BGL_OBJECT_WIDENING
																																		(BgL_tmpz00_5334);
																																}
																																BgL_auxz00_5332
																																	=
																																	(
																																	(BgL_makezd2procedurezd2appz00_bglt)
																																	BgL_auxz00_5333);
																															}
																															((((BgL_makezd2procedurezd2appz00_bglt) COBJECT(BgL_auxz00_5332))->BgL_tz00) = ((bool_t) ((bool_t) 0)), BUNSPEC);
																														}
																														BgL_continuezf3zf3_3118
																															= ((bool_t) 1);
																													}
																												else
																													{	/* Cfa/closure.scm 216 */
																														BFALSE;
																													}
																											}
																										else
																											{	/* Cfa/closure.scm 214 */
																												BFALSE;
																											}
																									}
																								}
																								{
																									obj_t BgL_l1524z00_5340;

																									BgL_l1524z00_5340 =
																										CDR(BgL_l1524z00_3135);
																									BgL_l1524z00_3135 =
																										BgL_l1524z00_5340;
																									goto
																										BgL_zc3z04anonymousza31726ze3z87_3136;
																								}
																							}
																						else
																							{	/* Cfa/closure.scm 212 */
																								BgL_tmpz00_5296 = ((bool_t) 1);
																							}
																						BBOOL(BgL_tmpz00_5296);
																					}
																				}
																			else
																				{	/* Cfa/closure.scm 220 */
																					bool_t BgL_test2304z00_5343;

																					{	/* Cfa/closure.scm 220 */
																						obj_t BgL_arg1740z00_3150;

																						BgL_arg1740z00_3150 =
																							CAR(((obj_t) BgL_allocsz00_3131));
																						{	/* Cfa/closure.scm 220 */
																							obj_t BgL_classz00_4226;

																							BgL_classz00_4226 =
																								BGl_makezd2procedurezd2appz00zzcfa_info2z00;
																							if (BGL_OBJECTP
																								(BgL_arg1740z00_3150))
																								{	/* Cfa/closure.scm 220 */
																									BgL_objectz00_bglt
																										BgL_arg1807z00_4228;
																									BgL_arg1807z00_4228 =
																										(BgL_objectz00_bglt)
																										(BgL_arg1740z00_3150);
																									if (BGL_CONDEXPAND_ISA_ARCH64
																										())
																										{	/* Cfa/closure.scm 220 */
																											long BgL_idxz00_4234;

																											BgL_idxz00_4234 =
																												BGL_OBJECT_INHERITANCE_NUM
																												(BgL_arg1807z00_4228);
																											BgL_test2304z00_5343 =
																												(VECTOR_REF
																												(BGl_za2inheritancesza2z00zz__objectz00,
																													(BgL_idxz00_4234 +
																														4L)) ==
																												BgL_classz00_4226);
																										}
																									else
																										{	/* Cfa/closure.scm 220 */
																											bool_t
																												BgL_res2213z00_4259;
																											{	/* Cfa/closure.scm 220 */
																												obj_t
																													BgL_oclassz00_4242;
																												{	/* Cfa/closure.scm 220 */
																													obj_t
																														BgL_arg1815z00_4250;
																													long
																														BgL_arg1816z00_4251;
																													BgL_arg1815z00_4250 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* Cfa/closure.scm 220 */
																														long
																															BgL_arg1817z00_4252;
																														BgL_arg1817z00_4252
																															=
																															BGL_OBJECT_CLASS_NUM
																															(BgL_arg1807z00_4228);
																														BgL_arg1816z00_4251
																															=
																															(BgL_arg1817z00_4252
																															- OBJECT_TYPE);
																													}
																													BgL_oclassz00_4242 =
																														VECTOR_REF
																														(BgL_arg1815z00_4250,
																														BgL_arg1816z00_4251);
																												}
																												{	/* Cfa/closure.scm 220 */
																													bool_t
																														BgL__ortest_1115z00_4243;
																													BgL__ortest_1115z00_4243
																														=
																														(BgL_classz00_4226
																														==
																														BgL_oclassz00_4242);
																													if (BgL__ortest_1115z00_4243)
																														{	/* Cfa/closure.scm 220 */
																															BgL_res2213z00_4259
																																=
																																BgL__ortest_1115z00_4243;
																														}
																													else
																														{	/* Cfa/closure.scm 220 */
																															long
																																BgL_odepthz00_4244;
																															{	/* Cfa/closure.scm 220 */
																																obj_t
																																	BgL_arg1804z00_4245;
																																BgL_arg1804z00_4245
																																	=
																																	(BgL_oclassz00_4242);
																																BgL_odepthz00_4244
																																	=
																																	BGL_CLASS_DEPTH
																																	(BgL_arg1804z00_4245);
																															}
																															if (
																																(4L <
																																	BgL_odepthz00_4244))
																																{	/* Cfa/closure.scm 220 */
																																	obj_t
																																		BgL_arg1802z00_4247;
																																	{	/* Cfa/closure.scm 220 */
																																		obj_t
																																			BgL_arg1803z00_4248;
																																		BgL_arg1803z00_4248
																																			=
																																			(BgL_oclassz00_4242);
																																		BgL_arg1802z00_4247
																																			=
																																			BGL_CLASS_ANCESTORS_REF
																																			(BgL_arg1803z00_4248,
																																			4L);
																																	}
																																	BgL_res2213z00_4259
																																		=
																																		(BgL_arg1802z00_4247
																																		==
																																		BgL_classz00_4226);
																																}
																															else
																																{	/* Cfa/closure.scm 220 */
																																	BgL_res2213z00_4259
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																											}
																											BgL_test2304z00_5343 =
																												BgL_res2213z00_4259;
																										}
																								}
																							else
																								{	/* Cfa/closure.scm 220 */
																									BgL_test2304z00_5343 =
																										((bool_t) 0);
																								}
																						}
																					}
																					if (BgL_test2304z00_5343)
																						{	/* Cfa/closure.scm 221 */
																							BgL_appz00_bglt BgL_i1168z00_3146;

																							BgL_i1168z00_3146 =
																								((BgL_appz00_bglt)
																								CAR(
																									((obj_t)
																										BgL_allocsz00_3131)));
																							{	/* Cfa/closure.scm 222 */
																								bool_t BgL_test2309z00_5371;

																								{
																									BgL_makezd2procedurezd2appz00_bglt
																										BgL_auxz00_5372;
																									{
																										obj_t BgL_auxz00_5373;

																										{	/* Cfa/closure.scm 222 */
																											BgL_objectz00_bglt
																												BgL_tmpz00_5374;
																											BgL_tmpz00_5374 =
																												((BgL_objectz00_bglt)
																												BgL_i1168z00_3146);
																											BgL_auxz00_5373 =
																												BGL_OBJECT_WIDENING
																												(BgL_tmpz00_5374);
																										}
																										BgL_auxz00_5372 =
																											(
																											(BgL_makezd2procedurezd2appz00_bglt)
																											BgL_auxz00_5373);
																									}
																									BgL_test2309z00_5371 =
																										(((BgL_makezd2procedurezd2appz00_bglt) COBJECT(BgL_auxz00_5372))->BgL_tz00);
																								}
																								if (BgL_test2309z00_5371)
																									{	/* Cfa/closure.scm 223 */
																										obj_t BgL_arg1738z00_3148;

																										BgL_arg1738z00_3148 =
																											CDR(
																											((obj_t)
																												BgL_allocsz00_3131));
																										{
																											obj_t BgL_allocsz00_5381;

																											BgL_allocsz00_5381 =
																												BgL_arg1738z00_3148;
																											BgL_allocsz00_3131 =
																												BgL_allocsz00_5381;
																											goto
																												BgL_zc3z04anonymousza31724ze3z87_3132;
																										}
																									}
																								else
																									{
																										bool_t
																											BgL_onezd2nonzd2tzf3zf3_5382;
																										BgL_onezd2nonzd2tzf3zf3_5382
																											= ((bool_t) 1);
																										BgL_onezd2nonzd2tzf3zf3_3130
																											=
																											BgL_onezd2nonzd2tzf3zf3_5382;
																										goto
																											BgL_zc3z04anonymousza31724ze3z87_3132;
																									}
																							}
																						}
																					else
																						{	/* Cfa/closure.scm 220 */
																							if (CBOOL
																								(BGl_za2unsafezd2typeza2zd2zzengine_paramz00))
																								{	/* Cfa/closure.scm 227 */
																									obj_t BgL_arg1739z00_3149;

																									BgL_arg1739z00_3149 =
																										CDR(
																										((obj_t)
																											BgL_allocsz00_3131));
																									{
																										obj_t BgL_allocsz00_5387;

																										BgL_allocsz00_5387 =
																											BgL_arg1739z00_3149;
																										BgL_allocsz00_3131 =
																											BgL_allocsz00_5387;
																										goto
																											BgL_zc3z04anonymousza31724ze3z87_3132;
																									}
																								}
																							else
																								{
																									bool_t
																										BgL_onezd2nonzd2tzf3zf3_5388;
																									BgL_onezd2nonzd2tzf3zf3_5388 =
																										((bool_t) 1);
																									BgL_onezd2nonzd2tzf3zf3_3130 =
																										BgL_onezd2nonzd2tzf3zf3_5388;
																									goto
																										BgL_zc3z04anonymousza31724ze3z87_3132;
																								}
																						}
																				}
																		}
																}
															}
														}
													}
												}
											}
										}
									}
									{
										obj_t BgL_l1526z00_5389;

										BgL_l1526z00_5389 = CDR(BgL_l1526z00_3120);
										BgL_l1526z00_3120 = BgL_l1526z00_5389;
										goto BgL_zc3z04anonymousza31722ze3z87_3121;
									}
								}
							else
								{	/* Cfa/closure.scm 193 */
									((bool_t) 1);
								}
						}
						{
							bool_t BgL_continuezf3zf3_5391;

							BgL_continuezf3zf3_5391 = BgL_continuezf3zf3_3118;
							BgL_continuezf3zf3_3116 = BgL_continuezf3zf3_5391;
							goto BgL_zc3z04anonymousza31721ze3z87_3117;
						}
					}
				else
					{	/* Cfa/closure.scm 190 */
						return BUNSPEC;
					}
			}
		}

	}



/* light-make-procedure! */
	bool_t BGl_lightzd2makezd2procedurez12z12zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 256 */
			{
				obj_t BgL_appz00_3203;
				obj_t BgL_appz00_3173;

				{
					obj_t BgL_l1528z00_3164;

					BgL_l1528z00_3164 =
						BGl_za2makezd2procedurezd2listza2z00zzcfa_closurez00;
				BgL_zc3z04anonymousza31750ze3z87_3165:
					if (PAIRP(BgL_l1528z00_3164))
						{	/* Cfa/closure.scm 298 */
							{	/* Cfa/closure.scm 299 */
								obj_t BgL_appz00_3167;

								BgL_appz00_3167 = CAR(BgL_l1528z00_3164);
								{	/* Cfa/closure.scm 301 */
									bool_t BgL_test2312z00_5395;

									{
										BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5396;

										{
											obj_t BgL_auxz00_5397;

											{	/* Cfa/closure.scm 301 */
												BgL_objectz00_bglt BgL_tmpz00_5398;

												BgL_tmpz00_5398 =
													((BgL_objectz00_bglt)
													((BgL_appz00_bglt) BgL_appz00_3167));
												BgL_auxz00_5397 = BGL_OBJECT_WIDENING(BgL_tmpz00_5398);
											}
											BgL_auxz00_5396 =
												((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_5397);
										}
										BgL_test2312z00_5395 =
											(((BgL_makezd2procedurezd2appz00_bglt)
												COBJECT(BgL_auxz00_5396))->BgL_xz00);
									}
									if (BgL_test2312z00_5395)
										{	/* Cfa/closure.scm 301 */
											BgL_appz00_3173 = BgL_appz00_3167;
											{	/* Cfa/closure.scm 260 */
												obj_t BgL_siza7eza7_3176;

												{	/* Cfa/closure.scm 260 */
													obj_t BgL_arg1831z00_3200;

													{	/* Cfa/closure.scm 260 */
														obj_t BgL_pairz00_4265;

														BgL_pairz00_4265 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_appz00_3173))))->
															BgL_argsz00);
														BgL_arg1831z00_3200 =
															CAR(CDR(CDR(BgL_pairz00_4265)));
													}
													BgL_siza7eza7_3176 =
														BGl_getzd2nodezd2atomzd2valuezd2zzcfa_approxz00(
														((BgL_nodez00_bglt) BgL_arg1831z00_3200));
												}
												{	/* Cfa/closure.scm 260 */
													BgL_variablez00_bglt BgL_ffunz00_3177;

													{
														BgL_varz00_bglt BgL_auxz00_5412;

														{	/* Cfa/closure.scm 261 */
															obj_t BgL_pairz00_4271;

															BgL_pairz00_4271 =
																(((BgL_appz00_bglt) COBJECT(
																		((BgL_appz00_bglt)
																			((BgL_appz00_bglt) BgL_appz00_3173))))->
																BgL_argsz00);
															BgL_auxz00_5412 =
																((BgL_varz00_bglt) CAR(BgL_pairz00_4271));
														}
														BgL_ffunz00_3177 =
															(((BgL_varz00_bglt) COBJECT(BgL_auxz00_5412))->
															BgL_variablez00);
													}
													{	/* Cfa/closure.scm 261 */
														BgL_valuez00_bglt BgL_sfunz00_3178;

														BgL_sfunz00_3178 =
															(((BgL_variablez00_bglt)
																COBJECT(BgL_ffunz00_3177))->BgL_valuez00);
														{	/* Cfa/closure.scm 262 */

															if (((long) CINT(BgL_siza7eza7_3176) < 1L))
																{	/* Cfa/closure.scm 264 */
																	{	/* Cfa/closure.scm 265 */
																		bool_t BgL_test2314z00_5423;

																		{	/* Cfa/closure.scm 265 */
																			bool_t BgL_test2315z00_5424;

																			{	/* Cfa/closure.scm 265 */
																				obj_t BgL_classz00_4275;

																				BgL_classz00_4275 =
																					BGl_globalz00zzast_varz00;
																				{	/* Cfa/closure.scm 265 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_4277;
																					{	/* Cfa/closure.scm 265 */
																						obj_t BgL_tmpz00_5425;

																						BgL_tmpz00_5425 =
																							((obj_t)
																							((BgL_objectz00_bglt)
																								BgL_ffunz00_3177));
																						BgL_arg1807z00_4277 =
																							(BgL_objectz00_bglt)
																							(BgL_tmpz00_5425);
																					}
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Cfa/closure.scm 265 */
																							long BgL_idxz00_4283;

																							BgL_idxz00_4283 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_4277);
																							BgL_test2315z00_5424 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_4283 + 2L)) ==
																								BgL_classz00_4275);
																						}
																					else
																						{	/* Cfa/closure.scm 265 */
																							bool_t BgL_res2214z00_4308;

																							{	/* Cfa/closure.scm 265 */
																								obj_t BgL_oclassz00_4291;

																								{	/* Cfa/closure.scm 265 */
																									obj_t BgL_arg1815z00_4299;
																									long BgL_arg1816z00_4300;

																									BgL_arg1815z00_4299 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Cfa/closure.scm 265 */
																										long BgL_arg1817z00_4301;

																										BgL_arg1817z00_4301 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_4277);
																										BgL_arg1816z00_4300 =
																											(BgL_arg1817z00_4301 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_4291 =
																										VECTOR_REF
																										(BgL_arg1815z00_4299,
																										BgL_arg1816z00_4300);
																								}
																								{	/* Cfa/closure.scm 265 */
																									bool_t
																										BgL__ortest_1115z00_4292;
																									BgL__ortest_1115z00_4292 =
																										(BgL_classz00_4275 ==
																										BgL_oclassz00_4291);
																									if (BgL__ortest_1115z00_4292)
																										{	/* Cfa/closure.scm 265 */
																											BgL_res2214z00_4308 =
																												BgL__ortest_1115z00_4292;
																										}
																									else
																										{	/* Cfa/closure.scm 265 */
																											long BgL_odepthz00_4293;

																											{	/* Cfa/closure.scm 265 */
																												obj_t
																													BgL_arg1804z00_4294;
																												BgL_arg1804z00_4294 =
																													(BgL_oclassz00_4291);
																												BgL_odepthz00_4293 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_4294);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_4293))
																												{	/* Cfa/closure.scm 265 */
																													obj_t
																														BgL_arg1802z00_4296;
																													{	/* Cfa/closure.scm 265 */
																														obj_t
																															BgL_arg1803z00_4297;
																														BgL_arg1803z00_4297
																															=
																															(BgL_oclassz00_4291);
																														BgL_arg1802z00_4296
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_4297,
																															2L);
																													}
																													BgL_res2214z00_4308 =
																														(BgL_arg1802z00_4296
																														==
																														BgL_classz00_4275);
																												}
																											else
																												{	/* Cfa/closure.scm 265 */
																													BgL_res2214z00_4308 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test2315z00_5424 =
																								BgL_res2214z00_4308;
																						}
																				}
																			}
																			if (BgL_test2315z00_5424)
																				{	/* Cfa/closure.scm 266 */
																					bool_t BgL_test2319z00_5448;

																					{	/* Cfa/closure.scm 266 */
																						obj_t BgL_arg1805z00_3193;

																						BgL_arg1805z00_3193 =
																							(((BgL_funz00_bglt) COBJECT(
																									((BgL_funz00_bglt)
																										((BgL_sfunz00_bglt)
																											BgL_sfunz00_3178))))->
																							BgL_thezd2closurezd2);
																						{	/* Cfa/closure.scm 266 */
																							obj_t BgL_classz00_4310;

																							BgL_classz00_4310 =
																								BGl_globalz00zzast_varz00;
																							if (BGL_OBJECTP
																								(BgL_arg1805z00_3193))
																								{	/* Cfa/closure.scm 266 */
																									BgL_objectz00_bglt
																										BgL_arg1807z00_4312;
																									BgL_arg1807z00_4312 =
																										(BgL_objectz00_bglt)
																										(BgL_arg1805z00_3193);
																									if (BGL_CONDEXPAND_ISA_ARCH64
																										())
																										{	/* Cfa/closure.scm 266 */
																											long BgL_idxz00_4318;

																											BgL_idxz00_4318 =
																												BGL_OBJECT_INHERITANCE_NUM
																												(BgL_arg1807z00_4312);
																											BgL_test2319z00_5448 =
																												(VECTOR_REF
																												(BGl_za2inheritancesza2z00zz__objectz00,
																													(BgL_idxz00_4318 +
																														2L)) ==
																												BgL_classz00_4310);
																										}
																									else
																										{	/* Cfa/closure.scm 266 */
																											bool_t
																												BgL_res2215z00_4343;
																											{	/* Cfa/closure.scm 266 */
																												obj_t
																													BgL_oclassz00_4326;
																												{	/* Cfa/closure.scm 266 */
																													obj_t
																														BgL_arg1815z00_4334;
																													long
																														BgL_arg1816z00_4335;
																													BgL_arg1815z00_4334 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* Cfa/closure.scm 266 */
																														long
																															BgL_arg1817z00_4336;
																														BgL_arg1817z00_4336
																															=
																															BGL_OBJECT_CLASS_NUM
																															(BgL_arg1807z00_4312);
																														BgL_arg1816z00_4335
																															=
																															(BgL_arg1817z00_4336
																															- OBJECT_TYPE);
																													}
																													BgL_oclassz00_4326 =
																														VECTOR_REF
																														(BgL_arg1815z00_4334,
																														BgL_arg1816z00_4335);
																												}
																												{	/* Cfa/closure.scm 266 */
																													bool_t
																														BgL__ortest_1115z00_4327;
																													BgL__ortest_1115z00_4327
																														=
																														(BgL_classz00_4310
																														==
																														BgL_oclassz00_4326);
																													if (BgL__ortest_1115z00_4327)
																														{	/* Cfa/closure.scm 266 */
																															BgL_res2215z00_4343
																																=
																																BgL__ortest_1115z00_4327;
																														}
																													else
																														{	/* Cfa/closure.scm 266 */
																															long
																																BgL_odepthz00_4328;
																															{	/* Cfa/closure.scm 266 */
																																obj_t
																																	BgL_arg1804z00_4329;
																																BgL_arg1804z00_4329
																																	=
																																	(BgL_oclassz00_4326);
																																BgL_odepthz00_4328
																																	=
																																	BGL_CLASS_DEPTH
																																	(BgL_arg1804z00_4329);
																															}
																															if (
																																(2L <
																																	BgL_odepthz00_4328))
																																{	/* Cfa/closure.scm 266 */
																																	obj_t
																																		BgL_arg1802z00_4331;
																																	{	/* Cfa/closure.scm 266 */
																																		obj_t
																																			BgL_arg1803z00_4332;
																																		BgL_arg1803z00_4332
																																			=
																																			(BgL_oclassz00_4326);
																																		BgL_arg1802z00_4331
																																			=
																																			BGL_CLASS_ANCESTORS_REF
																																			(BgL_arg1803z00_4332,
																																			2L);
																																	}
																																	BgL_res2215z00_4343
																																		=
																																		(BgL_arg1802z00_4331
																																		==
																																		BgL_classz00_4310);
																																}
																															else
																																{	/* Cfa/closure.scm 266 */
																																	BgL_res2215z00_4343
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																											}
																											BgL_test2319z00_5448 =
																												BgL_res2215z00_4343;
																										}
																								}
																							else
																								{	/* Cfa/closure.scm 266 */
																									BgL_test2319z00_5448 =
																										((bool_t) 0);
																								}
																						}
																					}
																					if (BgL_test2319z00_5448)
																						{	/* Cfa/closure.scm 267 */
																							BgL_valuez00_bglt
																								BgL_arg1798z00_3191;
																							BgL_arg1798z00_3191 =
																								(((BgL_variablez00_bglt)
																									COBJECT((
																											(BgL_variablez00_bglt) (
																												(BgL_globalz00_bglt) ((
																														(BgL_funz00_bglt)
																														COBJECT((
																																(BgL_funz00_bglt)
																																((BgL_sfunz00_bglt) BgL_sfunz00_3178))))->BgL_thezd2closurezd2)))))->BgL_valuez00);
																							{	/* Cfa/closure.scm 267 */
																								obj_t BgL_classz00_4346;

																								BgL_classz00_4346 =
																									BGl_scnstz00zzast_varz00;
																								{	/* Cfa/closure.scm 267 */
																									BgL_objectz00_bglt
																										BgL_arg1807z00_4348;
																									{	/* Cfa/closure.scm 267 */
																										obj_t BgL_tmpz00_5480;

																										BgL_tmpz00_5480 =
																											((obj_t)
																											((BgL_objectz00_bglt)
																												BgL_arg1798z00_3191));
																										BgL_arg1807z00_4348 =
																											(BgL_objectz00_bglt)
																											(BgL_tmpz00_5480);
																									}
																									if (BGL_CONDEXPAND_ISA_ARCH64
																										())
																										{	/* Cfa/closure.scm 267 */
																											long BgL_idxz00_4354;

																											BgL_idxz00_4354 =
																												BGL_OBJECT_INHERITANCE_NUM
																												(BgL_arg1807z00_4348);
																											BgL_test2314z00_5423 =
																												(VECTOR_REF
																												(BGl_za2inheritancesza2z00zz__objectz00,
																													(BgL_idxz00_4354 +
																														2L)) ==
																												BgL_classz00_4346);
																										}
																									else
																										{	/* Cfa/closure.scm 267 */
																											bool_t
																												BgL_res2216z00_4379;
																											{	/* Cfa/closure.scm 267 */
																												obj_t
																													BgL_oclassz00_4362;
																												{	/* Cfa/closure.scm 267 */
																													obj_t
																														BgL_arg1815z00_4370;
																													long
																														BgL_arg1816z00_4371;
																													BgL_arg1815z00_4370 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* Cfa/closure.scm 267 */
																														long
																															BgL_arg1817z00_4372;
																														BgL_arg1817z00_4372
																															=
																															BGL_OBJECT_CLASS_NUM
																															(BgL_arg1807z00_4348);
																														BgL_arg1816z00_4371
																															=
																															(BgL_arg1817z00_4372
																															- OBJECT_TYPE);
																													}
																													BgL_oclassz00_4362 =
																														VECTOR_REF
																														(BgL_arg1815z00_4370,
																														BgL_arg1816z00_4371);
																												}
																												{	/* Cfa/closure.scm 267 */
																													bool_t
																														BgL__ortest_1115z00_4363;
																													BgL__ortest_1115z00_4363
																														=
																														(BgL_classz00_4346
																														==
																														BgL_oclassz00_4362);
																													if (BgL__ortest_1115z00_4363)
																														{	/* Cfa/closure.scm 267 */
																															BgL_res2216z00_4379
																																=
																																BgL__ortest_1115z00_4363;
																														}
																													else
																														{	/* Cfa/closure.scm 267 */
																															long
																																BgL_odepthz00_4364;
																															{	/* Cfa/closure.scm 267 */
																																obj_t
																																	BgL_arg1804z00_4365;
																																BgL_arg1804z00_4365
																																	=
																																	(BgL_oclassz00_4362);
																																BgL_odepthz00_4364
																																	=
																																	BGL_CLASS_DEPTH
																																	(BgL_arg1804z00_4365);
																															}
																															if (
																																(2L <
																																	BgL_odepthz00_4364))
																																{	/* Cfa/closure.scm 267 */
																																	obj_t
																																		BgL_arg1802z00_4367;
																																	{	/* Cfa/closure.scm 267 */
																																		obj_t
																																			BgL_arg1803z00_4368;
																																		BgL_arg1803z00_4368
																																			=
																																			(BgL_oclassz00_4362);
																																		BgL_arg1802z00_4367
																																			=
																																			BGL_CLASS_ANCESTORS_REF
																																			(BgL_arg1803z00_4368,
																																			2L);
																																	}
																																	BgL_res2216z00_4379
																																		=
																																		(BgL_arg1802z00_4367
																																		==
																																		BgL_classz00_4346);
																																}
																															else
																																{	/* Cfa/closure.scm 267 */
																																	BgL_res2216z00_4379
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																											}
																											BgL_test2314z00_5423 =
																												BgL_res2216z00_4379;
																										}
																								}
																							}
																						}
																					else
																						{	/* Cfa/closure.scm 266 */
																							BgL_test2314z00_5423 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* Cfa/closure.scm 265 */
																					BgL_test2314z00_5423 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test2314z00_5423)
																			{	/* Cfa/closure.scm 268 */
																				BgL_valuez00_bglt BgL_arg1773z00_3187;

																				BgL_arg1773z00_3187 =
																					(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_globalz00_bglt)
																									(((BgL_funz00_bglt) COBJECT(
																												((BgL_funz00_bglt)
																													((BgL_sfunz00_bglt)
																														BgL_sfunz00_3178))))->
																										BgL_thezd2closurezd2)))))->
																					BgL_valuez00);
																				{	/* Cfa/closure.scm 268 */
																					obj_t BgL_vz00_4383;

																					BgL_vz00_4383 = CNST_TABLE_REF(6);
																					((((BgL_scnstz00_bglt) COBJECT(
																									((BgL_scnstz00_bglt)
																										BgL_arg1773z00_3187)))->
																							BgL_classz00) =
																						((obj_t) BgL_vz00_4383), BUNSPEC);
																				}
																			}
																		else
																			{	/* Cfa/closure.scm 265 */
																				BFALSE;
																			}
																	}
																	{	/* Cfa/closure.scm 270 */
																		BgL_varz00_bglt BgL_arg1806z00_3194;

																		BgL_arg1806z00_3194 =
																			(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						((BgL_appz00_bglt)
																							BgL_appz00_3173))))->BgL_funz00);
																		{	/* Cfa/closure.scm 270 */
																			BgL_variablez00_bglt BgL_vz00_4385;

																			BgL_vz00_4385 =
																				((BgL_variablez00_bglt)
																				BGl_za2makezd2elzd2procedureza2z00zzcfa_closurez00);
																			((((BgL_varz00_bglt)
																						COBJECT(BgL_arg1806z00_3194))->
																					BgL_variablez00) =
																				((BgL_variablez00_bglt) BgL_vz00_4385),
																				BUNSPEC);
																		}
																	}
																	{	/* Cfa/closure.scm 271 */
																		BgL_varz00_bglt BgL_arg1808z00_3195;

																		BgL_arg1808z00_3195 =
																			(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						((BgL_appz00_bglt)
																							BgL_appz00_3173))))->BgL_funz00);
																		{	/* Cfa/closure.scm 271 */
																			BgL_typez00_bglt BgL_vz00_4387;

																			BgL_vz00_4387 =
																				((BgL_typez00_bglt)
																				BGl_za2procedurezd2elza2zd2zztype_cachez00);
																			((((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_arg1808z00_3195)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) BgL_vz00_4387),
																				BUNSPEC);
																		}
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						((BgL_appz00_bglt)
																							BgL_appz00_3173))))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) ((BgL_typez00_bglt)
																				BGl_za2procedurezd2elza2zd2zztype_cachez00)),
																		BUNSPEC);
																}
															else
																{	/* Cfa/closure.scm 264 */
																	{	/* Cfa/closure.scm 274 */
																		BgL_varz00_bglt BgL_arg1812z00_3196;

																		BgL_arg1812z00_3196 =
																			(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						((BgL_appz00_bglt)
																							BgL_appz00_3173))))->BgL_funz00);
																		{	/* Cfa/closure.scm 274 */
																			BgL_variablez00_bglt BgL_vz00_4389;

																			BgL_vz00_4389 =
																				((BgL_variablez00_bglt)
																				BGl_za2makezd2elzd2procedureza2z00zzcfa_closurez00);
																			((((BgL_varz00_bglt)
																						COBJECT(BgL_arg1812z00_3196))->
																					BgL_variablez00) =
																				((BgL_variablez00_bglt) BgL_vz00_4389),
																				BUNSPEC);
																		}
																	}
																	{	/* Cfa/closure.scm 275 */
																		BgL_varz00_bglt BgL_arg1820z00_3197;

																		BgL_arg1820z00_3197 =
																			(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						((BgL_appz00_bglt)
																							BgL_appz00_3173))))->BgL_funz00);
																		{	/* Cfa/closure.scm 275 */
																			BgL_typez00_bglt BgL_vz00_4391;

																			BgL_vz00_4391 =
																				((BgL_typez00_bglt)
																				BGl_za2procedurezd2elza2zd2zztype_cachez00);
																			((((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_arg1820z00_3197)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) BgL_vz00_4391),
																				BUNSPEC);
																		}
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						((BgL_appz00_bglt)
																							BgL_appz00_3173))))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) ((BgL_typez00_bglt)
																				BGl_za2procedurezd2elza2zd2zztype_cachez00)),
																		BUNSPEC);
																}
														}
													}
												}
											}
											{
												obj_t BgL_auxz00_5542;

												{	/* Cfa/closure.scm 277 */
													obj_t BgL_pairz00_4392;

													BgL_pairz00_4392 =
														(((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt)
																	((BgL_appz00_bglt) BgL_appz00_3173))))->
														BgL_argsz00);
													{	/* Cfa/closure.scm 277 */
														obj_t BgL_pairz00_4395;

														BgL_pairz00_4395 = CDR(BgL_pairz00_4392);
														BgL_auxz00_5542 = CDR(BgL_pairz00_4395);
													}
												}
												((((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt)
																	((BgL_appz00_bglt) BgL_appz00_3173))))->
														BgL_argsz00) = ((obj_t) BgL_auxz00_5542), BUNSPEC);
											}
											BgL_appz00_3173;
										}
									else
										{	/* Cfa/closure.scm 302 */
											bool_t BgL_test2327z00_5551;

											{
												BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5552;

												{
													obj_t BgL_auxz00_5553;

													{	/* Cfa/closure.scm 302 */
														BgL_objectz00_bglt BgL_tmpz00_5554;

														BgL_tmpz00_5554 =
															((BgL_objectz00_bglt)
															((BgL_appz00_bglt) BgL_appz00_3167));
														BgL_auxz00_5553 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5554);
													}
													BgL_auxz00_5552 =
														((BgL_makezd2procedurezd2appz00_bglt)
														BgL_auxz00_5553);
												}
												BgL_test2327z00_5551 =
													(((BgL_makezd2procedurezd2appz00_bglt)
														COBJECT(BgL_auxz00_5552))->BgL_tz00);
											}
											if (BgL_test2327z00_5551)
												{	/* Cfa/closure.scm 302 */
													BgL_appz00_3203 = BgL_appz00_3167;
													{	/* Cfa/closure.scm 282 */
														obj_t BgL_siza7eza7_3206;

														{	/* Cfa/closure.scm 282 */
															obj_t BgL_arg1854z00_3230;

															{	/* Cfa/closure.scm 282 */
																obj_t BgL_pairz00_4396;

																BgL_pairz00_4396 =
																	(((BgL_appz00_bglt) COBJECT(
																			((BgL_appz00_bglt)
																				((BgL_appz00_bglt) BgL_appz00_3203))))->
																	BgL_argsz00);
																BgL_arg1854z00_3230 =
																	CAR(CDR(CDR(BgL_pairz00_4396)));
															}
															BgL_siza7eza7_3206 =
																BGl_getzd2nodezd2atomzd2valuezd2zzcfa_approxz00(
																((BgL_nodez00_bglt) BgL_arg1854z00_3230));
														}
														{	/* Cfa/closure.scm 282 */
															BgL_variablez00_bglt BgL_ffunz00_3207;

															{
																BgL_varz00_bglt BgL_auxz00_5568;

																{	/* Cfa/closure.scm 283 */
																	obj_t BgL_pairz00_4402;

																	BgL_pairz00_4402 =
																		(((BgL_appz00_bglt) COBJECT(
																				((BgL_appz00_bglt)
																					((BgL_appz00_bglt)
																						BgL_appz00_3203))))->BgL_argsz00);
																	BgL_auxz00_5568 =
																		((BgL_varz00_bglt) CAR(BgL_pairz00_4402));
																}
																BgL_ffunz00_3207 =
																	(((BgL_varz00_bglt)
																		COBJECT(BgL_auxz00_5568))->BgL_variablez00);
															}
															{	/* Cfa/closure.scm 283 */
																BgL_valuez00_bglt BgL_sfunz00_3208;

																BgL_sfunz00_3208 =
																	(((BgL_variablez00_bglt)
																		COBJECT(BgL_ffunz00_3207))->BgL_valuez00);
																{	/* Cfa/closure.scm 284 */

																	{	/* Cfa/closure.scm 285 */
																		bool_t BgL_test2328z00_5576;

																		{	/* Cfa/closure.scm 285 */
																			bool_t BgL_test2329z00_5577;

																			{	/* Cfa/closure.scm 285 */
																				obj_t BgL_classz00_4405;

																				BgL_classz00_4405 =
																					BGl_globalz00zzast_varz00;
																				{	/* Cfa/closure.scm 285 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_4407;
																					{	/* Cfa/closure.scm 285 */
																						obj_t BgL_tmpz00_5578;

																						BgL_tmpz00_5578 =
																							((obj_t)
																							((BgL_objectz00_bglt)
																								BgL_ffunz00_3207));
																						BgL_arg1807z00_4407 =
																							(BgL_objectz00_bglt)
																							(BgL_tmpz00_5578);
																					}
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Cfa/closure.scm 285 */
																							long BgL_idxz00_4413;

																							BgL_idxz00_4413 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_4407);
																							BgL_test2329z00_5577 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_4413 + 2L)) ==
																								BgL_classz00_4405);
																						}
																					else
																						{	/* Cfa/closure.scm 285 */
																							bool_t BgL_res2217z00_4438;

																							{	/* Cfa/closure.scm 285 */
																								obj_t BgL_oclassz00_4421;

																								{	/* Cfa/closure.scm 285 */
																									obj_t BgL_arg1815z00_4429;
																									long BgL_arg1816z00_4430;

																									BgL_arg1815z00_4429 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Cfa/closure.scm 285 */
																										long BgL_arg1817z00_4431;

																										BgL_arg1817z00_4431 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_4407);
																										BgL_arg1816z00_4430 =
																											(BgL_arg1817z00_4431 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_4421 =
																										VECTOR_REF
																										(BgL_arg1815z00_4429,
																										BgL_arg1816z00_4430);
																								}
																								{	/* Cfa/closure.scm 285 */
																									bool_t
																										BgL__ortest_1115z00_4422;
																									BgL__ortest_1115z00_4422 =
																										(BgL_classz00_4405 ==
																										BgL_oclassz00_4421);
																									if (BgL__ortest_1115z00_4422)
																										{	/* Cfa/closure.scm 285 */
																											BgL_res2217z00_4438 =
																												BgL__ortest_1115z00_4422;
																										}
																									else
																										{	/* Cfa/closure.scm 285 */
																											long BgL_odepthz00_4423;

																											{	/* Cfa/closure.scm 285 */
																												obj_t
																													BgL_arg1804z00_4424;
																												BgL_arg1804z00_4424 =
																													(BgL_oclassz00_4421);
																												BgL_odepthz00_4423 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_4424);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_4423))
																												{	/* Cfa/closure.scm 285 */
																													obj_t
																														BgL_arg1802z00_4426;
																													{	/* Cfa/closure.scm 285 */
																														obj_t
																															BgL_arg1803z00_4427;
																														BgL_arg1803z00_4427
																															=
																															(BgL_oclassz00_4421);
																														BgL_arg1802z00_4426
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_4427,
																															2L);
																													}
																													BgL_res2217z00_4438 =
																														(BgL_arg1802z00_4426
																														==
																														BgL_classz00_4405);
																												}
																											else
																												{	/* Cfa/closure.scm 285 */
																													BgL_res2217z00_4438 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test2329z00_5577 =
																								BgL_res2217z00_4438;
																						}
																				}
																			}
																			if (BgL_test2329z00_5577)
																				{	/* Cfa/closure.scm 286 */
																					bool_t BgL_test2333z00_5601;

																					{	/* Cfa/closure.scm 286 */
																						obj_t BgL_arg1846z00_3222;

																						BgL_arg1846z00_3222 =
																							(((BgL_funz00_bglt) COBJECT(
																									((BgL_funz00_bglt)
																										((BgL_sfunz00_bglt)
																											BgL_sfunz00_3208))))->
																							BgL_thezd2closurezd2);
																						{	/* Cfa/closure.scm 286 */
																							obj_t BgL_classz00_4440;

																							BgL_classz00_4440 =
																								BGl_globalz00zzast_varz00;
																							if (BGL_OBJECTP
																								(BgL_arg1846z00_3222))
																								{	/* Cfa/closure.scm 286 */
																									BgL_objectz00_bglt
																										BgL_arg1807z00_4442;
																									BgL_arg1807z00_4442 =
																										(BgL_objectz00_bglt)
																										(BgL_arg1846z00_3222);
																									if (BGL_CONDEXPAND_ISA_ARCH64
																										())
																										{	/* Cfa/closure.scm 286 */
																											long BgL_idxz00_4448;

																											BgL_idxz00_4448 =
																												BGL_OBJECT_INHERITANCE_NUM
																												(BgL_arg1807z00_4442);
																											BgL_test2333z00_5601 =
																												(VECTOR_REF
																												(BGl_za2inheritancesza2z00zz__objectz00,
																													(BgL_idxz00_4448 +
																														2L)) ==
																												BgL_classz00_4440);
																										}
																									else
																										{	/* Cfa/closure.scm 286 */
																											bool_t
																												BgL_res2218z00_4473;
																											{	/* Cfa/closure.scm 286 */
																												obj_t
																													BgL_oclassz00_4456;
																												{	/* Cfa/closure.scm 286 */
																													obj_t
																														BgL_arg1815z00_4464;
																													long
																														BgL_arg1816z00_4465;
																													BgL_arg1815z00_4464 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* Cfa/closure.scm 286 */
																														long
																															BgL_arg1817z00_4466;
																														BgL_arg1817z00_4466
																															=
																															BGL_OBJECT_CLASS_NUM
																															(BgL_arg1807z00_4442);
																														BgL_arg1816z00_4465
																															=
																															(BgL_arg1817z00_4466
																															- OBJECT_TYPE);
																													}
																													BgL_oclassz00_4456 =
																														VECTOR_REF
																														(BgL_arg1815z00_4464,
																														BgL_arg1816z00_4465);
																												}
																												{	/* Cfa/closure.scm 286 */
																													bool_t
																														BgL__ortest_1115z00_4457;
																													BgL__ortest_1115z00_4457
																														=
																														(BgL_classz00_4440
																														==
																														BgL_oclassz00_4456);
																													if (BgL__ortest_1115z00_4457)
																														{	/* Cfa/closure.scm 286 */
																															BgL_res2218z00_4473
																																=
																																BgL__ortest_1115z00_4457;
																														}
																													else
																														{	/* Cfa/closure.scm 286 */
																															long
																																BgL_odepthz00_4458;
																															{	/* Cfa/closure.scm 286 */
																																obj_t
																																	BgL_arg1804z00_4459;
																																BgL_arg1804z00_4459
																																	=
																																	(BgL_oclassz00_4456);
																																BgL_odepthz00_4458
																																	=
																																	BGL_CLASS_DEPTH
																																	(BgL_arg1804z00_4459);
																															}
																															if (
																																(2L <
																																	BgL_odepthz00_4458))
																																{	/* Cfa/closure.scm 286 */
																																	obj_t
																																		BgL_arg1802z00_4461;
																																	{	/* Cfa/closure.scm 286 */
																																		obj_t
																																			BgL_arg1803z00_4462;
																																		BgL_arg1803z00_4462
																																			=
																																			(BgL_oclassz00_4456);
																																		BgL_arg1802z00_4461
																																			=
																																			BGL_CLASS_ANCESTORS_REF
																																			(BgL_arg1803z00_4462,
																																			2L);
																																	}
																																	BgL_res2218z00_4473
																																		=
																																		(BgL_arg1802z00_4461
																																		==
																																		BgL_classz00_4440);
																																}
																															else
																																{	/* Cfa/closure.scm 286 */
																																	BgL_res2218z00_4473
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																											}
																											BgL_test2333z00_5601 =
																												BgL_res2218z00_4473;
																										}
																								}
																							else
																								{	/* Cfa/closure.scm 286 */
																									BgL_test2333z00_5601 =
																										((bool_t) 0);
																								}
																						}
																					}
																					if (BgL_test2333z00_5601)
																						{	/* Cfa/closure.scm 287 */
																							BgL_valuez00_bglt
																								BgL_arg1844z00_3220;
																							BgL_arg1844z00_3220 =
																								(((BgL_variablez00_bglt)
																									COBJECT((
																											(BgL_variablez00_bglt) (
																												(BgL_globalz00_bglt) ((
																														(BgL_funz00_bglt)
																														COBJECT((
																																(BgL_funz00_bglt)
																																((BgL_sfunz00_bglt) BgL_sfunz00_3208))))->BgL_thezd2closurezd2)))))->BgL_valuez00);
																							{	/* Cfa/closure.scm 287 */
																								obj_t BgL_classz00_4476;

																								BgL_classz00_4476 =
																									BGl_scnstz00zzast_varz00;
																								{	/* Cfa/closure.scm 287 */
																									BgL_objectz00_bglt
																										BgL_arg1807z00_4478;
																									{	/* Cfa/closure.scm 287 */
																										obj_t BgL_tmpz00_5633;

																										BgL_tmpz00_5633 =
																											((obj_t)
																											((BgL_objectz00_bglt)
																												BgL_arg1844z00_3220));
																										BgL_arg1807z00_4478 =
																											(BgL_objectz00_bglt)
																											(BgL_tmpz00_5633);
																									}
																									if (BGL_CONDEXPAND_ISA_ARCH64
																										())
																										{	/* Cfa/closure.scm 287 */
																											long BgL_idxz00_4484;

																											BgL_idxz00_4484 =
																												BGL_OBJECT_INHERITANCE_NUM
																												(BgL_arg1807z00_4478);
																											BgL_test2328z00_5576 =
																												(VECTOR_REF
																												(BGl_za2inheritancesza2z00zz__objectz00,
																													(BgL_idxz00_4484 +
																														2L)) ==
																												BgL_classz00_4476);
																										}
																									else
																										{	/* Cfa/closure.scm 287 */
																											bool_t
																												BgL_res2219z00_4509;
																											{	/* Cfa/closure.scm 287 */
																												obj_t
																													BgL_oclassz00_4492;
																												{	/* Cfa/closure.scm 287 */
																													obj_t
																														BgL_arg1815z00_4500;
																													long
																														BgL_arg1816z00_4501;
																													BgL_arg1815z00_4500 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* Cfa/closure.scm 287 */
																														long
																															BgL_arg1817z00_4502;
																														BgL_arg1817z00_4502
																															=
																															BGL_OBJECT_CLASS_NUM
																															(BgL_arg1807z00_4478);
																														BgL_arg1816z00_4501
																															=
																															(BgL_arg1817z00_4502
																															- OBJECT_TYPE);
																													}
																													BgL_oclassz00_4492 =
																														VECTOR_REF
																														(BgL_arg1815z00_4500,
																														BgL_arg1816z00_4501);
																												}
																												{	/* Cfa/closure.scm 287 */
																													bool_t
																														BgL__ortest_1115z00_4493;
																													BgL__ortest_1115z00_4493
																														=
																														(BgL_classz00_4476
																														==
																														BgL_oclassz00_4492);
																													if (BgL__ortest_1115z00_4493)
																														{	/* Cfa/closure.scm 287 */
																															BgL_res2219z00_4509
																																=
																																BgL__ortest_1115z00_4493;
																														}
																													else
																														{	/* Cfa/closure.scm 287 */
																															long
																																BgL_odepthz00_4494;
																															{	/* Cfa/closure.scm 287 */
																																obj_t
																																	BgL_arg1804z00_4495;
																																BgL_arg1804z00_4495
																																	=
																																	(BgL_oclassz00_4492);
																																BgL_odepthz00_4494
																																	=
																																	BGL_CLASS_DEPTH
																																	(BgL_arg1804z00_4495);
																															}
																															if (
																																(2L <
																																	BgL_odepthz00_4494))
																																{	/* Cfa/closure.scm 287 */
																																	obj_t
																																		BgL_arg1802z00_4497;
																																	{	/* Cfa/closure.scm 287 */
																																		obj_t
																																			BgL_arg1803z00_4498;
																																		BgL_arg1803z00_4498
																																			=
																																			(BgL_oclassz00_4492);
																																		BgL_arg1802z00_4497
																																			=
																																			BGL_CLASS_ANCESTORS_REF
																																			(BgL_arg1803z00_4498,
																																			2L);
																																	}
																																	BgL_res2219z00_4509
																																		=
																																		(BgL_arg1802z00_4497
																																		==
																																		BgL_classz00_4476);
																																}
																															else
																																{	/* Cfa/closure.scm 287 */
																																	BgL_res2219z00_4509
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																											}
																											BgL_test2328z00_5576 =
																												BgL_res2219z00_4509;
																										}
																								}
																							}
																						}
																					else
																						{	/* Cfa/closure.scm 286 */
																							BgL_test2328z00_5576 =
																								((bool_t) 0);
																						}
																				}
																			else
																				{	/* Cfa/closure.scm 285 */
																					BgL_test2328z00_5576 = ((bool_t) 0);
																				}
																		}
																		if (BgL_test2328z00_5576)
																			{	/* Cfa/closure.scm 288 */
																				BgL_valuez00_bglt BgL_arg1842z00_3216;

																				BgL_arg1842z00_3216 =
																					(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								((BgL_globalz00_bglt)
																									(((BgL_funz00_bglt) COBJECT(
																												((BgL_funz00_bglt)
																													((BgL_sfunz00_bglt)
																														BgL_sfunz00_3208))))->
																										BgL_thezd2closurezd2)))))->
																					BgL_valuez00);
																				{	/* Cfa/closure.scm 288 */
																					obj_t BgL_vz00_4513;

																					BgL_vz00_4513 = CNST_TABLE_REF(7);
																					((((BgL_scnstz00_bglt) COBJECT(
																									((BgL_scnstz00_bglt)
																										BgL_arg1842z00_3216)))->
																							BgL_classz00) =
																						((obj_t) BgL_vz00_4513), BUNSPEC);
																				}
																			}
																		else
																			{	/* Cfa/closure.scm 285 */
																				BFALSE;
																			}
																	}
																	{	/* Cfa/closure.scm 290 */
																		BgL_varz00_bglt BgL_arg1847z00_3223;

																		BgL_arg1847z00_3223 =
																			(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						((BgL_appz00_bglt)
																							BgL_appz00_3203))))->BgL_funz00);
																		{	/* Cfa/closure.scm 290 */
																			BgL_variablez00_bglt BgL_vz00_4515;

																			BgL_vz00_4515 =
																				((BgL_variablez00_bglt)
																				BGl_za2makezd2lzd2procedureza2z00zzcfa_closurez00);
																			((((BgL_varz00_bglt)
																						COBJECT(BgL_arg1847z00_3223))->
																					BgL_variablez00) =
																				((BgL_variablez00_bglt) BgL_vz00_4515),
																				BUNSPEC);
																		}
																	}
																	{	/* Cfa/closure.scm 292 */
																		BgL_varz00_bglt BgL_arg1848z00_3224;

																		BgL_arg1848z00_3224 =
																			(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						((BgL_appz00_bglt)
																							BgL_appz00_3203))))->BgL_funz00);
																		{	/* Cfa/closure.scm 292 */
																			BgL_typez00_bglt BgL_vz00_4517;

																			BgL_vz00_4517 =
																				((BgL_typez00_bglt)
																				BGl_za2procedureza2z00zztype_cachez00);
																			((((BgL_nodez00_bglt)
																						COBJECT(((BgL_nodez00_bglt)
																								BgL_arg1848z00_3224)))->
																					BgL_typez00) =
																				((BgL_typez00_bglt) BgL_vz00_4517),
																				BUNSPEC);
																		}
																	}
																	((((BgL_nodez00_bglt) COBJECT(
																					((BgL_nodez00_bglt)
																						((BgL_appz00_bglt)
																							BgL_appz00_3203))))->
																			BgL_typez00) =
																		((BgL_typez00_bglt) ((BgL_typez00_bglt)
																				BGl_za2procedureza2z00zztype_cachez00)),
																		BUNSPEC);
																	{	/* Cfa/closure.scm 294 */
																		obj_t BgL_arg1849z00_3225;
																		obj_t BgL_arg1850z00_3226;

																		BgL_arg1849z00_3225 =
																			(((BgL_appz00_bglt) COBJECT(
																					((BgL_appz00_bglt)
																						((BgL_appz00_bglt)
																							BgL_appz00_3203))))->BgL_argsz00);
																		{	/* Cfa/closure.scm 294 */
																			obj_t BgL_pairz00_4518;

																			BgL_pairz00_4518 =
																				(((BgL_appz00_bglt) COBJECT(
																						((BgL_appz00_bglt)
																							((BgL_appz00_bglt)
																								BgL_appz00_3203))))->
																				BgL_argsz00);
																			BgL_arg1850z00_3226 =
																				CDR(CDR(BgL_pairz00_4518));
																		}
																		{	/* Cfa/closure.scm 294 */
																			obj_t BgL_tmpz00_5688;

																			BgL_tmpz00_5688 =
																				((obj_t) BgL_arg1849z00_3225);
																			SET_CDR(BgL_tmpz00_5688,
																				BgL_arg1850z00_3226);
																		}
																	}
																	BgL_appz00_3203;
																}
															}
														}
													}
												}
											else
												{	/* Cfa/closure.scm 302 */
													BFALSE;
												}
										}
								}
							}
							{
								obj_t BgL_l1528z00_5691;

								BgL_l1528z00_5691 = CDR(BgL_l1528z00_3164);
								BgL_l1528z00_3164 = BgL_l1528z00_5691;
								goto BgL_zc3z04anonymousza31750ze3z87_3165;
							}
						}
					else
						{	/* Cfa/closure.scm 298 */
							return ((bool_t) 1);
						}
				}
			}
		}

	}



/* light-funcall! */
	bool_t BGl_lightzd2funcallz12zc0zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 308 */
			{
				obj_t BgL_l1535z00_3235;

				BgL_l1535z00_3235 = BGl_za2funcallzd2listza2zd2zzcfa_closurez00;
			BgL_zc3z04anonymousza31857ze3z87_3236:
				if (PAIRP(BgL_l1535z00_3235))
					{	/* Cfa/closure.scm 312 */
						{	/* Cfa/closure.scm 313 */
							BgL_funcallz00_bglt BgL_appz00_3238;

							BgL_appz00_3238 = ((BgL_funcallz00_bglt) CAR(BgL_l1535z00_3235));
							{	/* Cfa/closure.scm 314 */
								BgL_nodez00_bglt BgL_funz00_3239;

								BgL_funz00_3239 =
									(((BgL_funcallz00_bglt) COBJECT(BgL_appz00_3238))->
									BgL_funz00);
								{	/* Cfa/closure.scm 314 */
									BgL_approxz00_bglt BgL_approxz00_3240;

									BgL_approxz00_3240 =
										BGl_cfaz12z12zzcfa_cfaz00(BgL_funz00_3239);
									{	/* Cfa/closure.scm 315 */
										obj_t BgL_alloczd2listzd2_3241;

										BgL_alloczd2listzd2_3241 =
											BGl_setzd2ze3listz31zzcfa_setz00(
											(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_3240))->
												BgL_allocsz00));
										{	/* Cfa/closure.scm 316 */

											{	/* Cfa/closure.scm 317 */
												bool_t BgL_test2342z00_5701;

												if (PAIRP(BgL_alloczd2listzd2_3241))
													{	/* Cfa/closure.scm 318 */
														bool_t BgL_test2344z00_5704;

														{	/* Cfa/closure.scm 318 */
															obj_t BgL_arg1890z00_3286;

															BgL_arg1890z00_3286 =
																CAR(BgL_alloczd2listzd2_3241);
															{	/* Cfa/closure.scm 318 */
																obj_t BgL_classz00_4531;

																BgL_classz00_4531 =
																	BGl_makezd2procedurezd2appz00zzcfa_info2z00;
																if (BGL_OBJECTP(BgL_arg1890z00_3286))
																	{	/* Cfa/closure.scm 318 */
																		BgL_objectz00_bglt BgL_arg1807z00_4533;

																		BgL_arg1807z00_4533 =
																			(BgL_objectz00_bglt)
																			(BgL_arg1890z00_3286);
																		if (BGL_CONDEXPAND_ISA_ARCH64())
																			{	/* Cfa/closure.scm 318 */
																				long BgL_idxz00_4539;

																				BgL_idxz00_4539 =
																					BGL_OBJECT_INHERITANCE_NUM
																					(BgL_arg1807z00_4533);
																				BgL_test2344z00_5704 =
																					(VECTOR_REF
																					(BGl_za2inheritancesza2z00zz__objectz00,
																						(BgL_idxz00_4539 + 4L)) ==
																					BgL_classz00_4531);
																			}
																		else
																			{	/* Cfa/closure.scm 318 */
																				bool_t BgL_res2220z00_4564;

																				{	/* Cfa/closure.scm 318 */
																					obj_t BgL_oclassz00_4547;

																					{	/* Cfa/closure.scm 318 */
																						obj_t BgL_arg1815z00_4555;
																						long BgL_arg1816z00_4556;

																						BgL_arg1815z00_4555 =
																							(BGl_za2classesza2z00zz__objectz00);
																						{	/* Cfa/closure.scm 318 */
																							long BgL_arg1817z00_4557;

																							BgL_arg1817z00_4557 =
																								BGL_OBJECT_CLASS_NUM
																								(BgL_arg1807z00_4533);
																							BgL_arg1816z00_4556 =
																								(BgL_arg1817z00_4557 -
																								OBJECT_TYPE);
																						}
																						BgL_oclassz00_4547 =
																							VECTOR_REF(BgL_arg1815z00_4555,
																							BgL_arg1816z00_4556);
																					}
																					{	/* Cfa/closure.scm 318 */
																						bool_t BgL__ortest_1115z00_4548;

																						BgL__ortest_1115z00_4548 =
																							(BgL_classz00_4531 ==
																							BgL_oclassz00_4547);
																						if (BgL__ortest_1115z00_4548)
																							{	/* Cfa/closure.scm 318 */
																								BgL_res2220z00_4564 =
																									BgL__ortest_1115z00_4548;
																							}
																						else
																							{	/* Cfa/closure.scm 318 */
																								long BgL_odepthz00_4549;

																								{	/* Cfa/closure.scm 318 */
																									obj_t BgL_arg1804z00_4550;

																									BgL_arg1804z00_4550 =
																										(BgL_oclassz00_4547);
																									BgL_odepthz00_4549 =
																										BGL_CLASS_DEPTH
																										(BgL_arg1804z00_4550);
																								}
																								if ((4L < BgL_odepthz00_4549))
																									{	/* Cfa/closure.scm 318 */
																										obj_t BgL_arg1802z00_4552;

																										{	/* Cfa/closure.scm 318 */
																											obj_t BgL_arg1803z00_4553;

																											BgL_arg1803z00_4553 =
																												(BgL_oclassz00_4547);
																											BgL_arg1802z00_4552 =
																												BGL_CLASS_ANCESTORS_REF
																												(BgL_arg1803z00_4553,
																												4L);
																										}
																										BgL_res2220z00_4564 =
																											(BgL_arg1802z00_4552 ==
																											BgL_classz00_4531);
																									}
																								else
																									{	/* Cfa/closure.scm 318 */
																										BgL_res2220z00_4564 =
																											((bool_t) 0);
																									}
																							}
																					}
																				}
																				BgL_test2344z00_5704 =
																					BgL_res2220z00_4564;
																			}
																	}
																else
																	{	/* Cfa/closure.scm 318 */
																		BgL_test2344z00_5704 = ((bool_t) 0);
																	}
															}
														}
														if (BgL_test2344z00_5704)
															{	/* Cfa/closure.scm 318 */
																BgL_test2342z00_5701 = ((bool_t) 0);
															}
														else
															{	/* Cfa/closure.scm 318 */
																BgL_test2342z00_5701 = ((bool_t) 1);
															}
													}
												else
													{	/* Cfa/closure.scm 317 */
														BgL_test2342z00_5701 = ((bool_t) 1);
													}
												if (BgL_test2342z00_5701)
													{	/* Cfa/closure.scm 317 */
														CNST_TABLE_REF(8);
													}
												else
													{	/* Cfa/closure.scm 322 */
														BgL_appz00_bglt BgL_allocz00_3247;

														BgL_allocz00_3247 =
															((BgL_appz00_bglt)
															CAR(((obj_t) BgL_alloczd2listzd2_3241)));
														{	/* Cfa/closure.scm 325 */
															bool_t BgL_test2349z00_5732;

															{
																BgL_makezd2procedurezd2appz00_bglt
																	BgL_auxz00_5733;
																{
																	obj_t BgL_auxz00_5734;

																	{	/* Cfa/closure.scm 325 */
																		BgL_objectz00_bglt BgL_tmpz00_5735;

																		BgL_tmpz00_5735 =
																			((BgL_objectz00_bglt)
																			((BgL_appz00_bglt) BgL_allocz00_3247));
																		BgL_auxz00_5734 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_5735);
																	}
																	BgL_auxz00_5733 =
																		((BgL_makezd2procedurezd2appz00_bglt)
																		BgL_auxz00_5734);
																}
																BgL_test2349z00_5732 =
																	(((BgL_makezd2procedurezd2appz00_bglt)
																		COBJECT(BgL_auxz00_5733))->BgL_xz00);
															}
															if (BgL_test2349z00_5732)
																{	/* Cfa/closure.scm 325 */
																	{	/* Cfa/closure.scm 330 */
																		BgL_refz00_bglt BgL_arg1866z00_3250;

																		{	/* Cfa/closure.scm 330 */
																			BgL_nodez00_bglt
																				BgL_duplicated1176z00_3251;
																			BgL_refz00_bglt BgL_new1174z00_3252;

																			{	/* Cfa/closure.scm 330 */
																				obj_t BgL_pairz00_4567;

																				BgL_pairz00_4567 =
																					(((BgL_appz00_bglt) COBJECT(
																							((BgL_appz00_bglt)
																								((BgL_appz00_bglt)
																									BgL_allocz00_3247))))->
																					BgL_argsz00);
																				BgL_duplicated1176z00_3251 =
																					((BgL_nodez00_bglt)
																					CAR(BgL_pairz00_4567));
																			}
																			{	/* Cfa/closure.scm 331 */
																				BgL_refz00_bglt BgL_new1178z00_3262;

																				BgL_new1178z00_3262 =
																					((BgL_refz00_bglt)
																					BOBJECT(GC_MALLOC(sizeof(struct
																								BgL_refz00_bgl))));
																				{	/* Cfa/closure.scm 331 */
																					long BgL_arg1877z00_3263;

																					BgL_arg1877z00_3263 =
																						BGL_CLASS_NUM
																						(BGl_refz00zzast_nodez00);
																					BGL_OBJECT_CLASS_NUM_SET((
																							(BgL_objectz00_bglt)
																							BgL_new1178z00_3262),
																						BgL_arg1877z00_3263);
																				}
																				{	/* Cfa/closure.scm 331 */
																					BgL_objectz00_bglt BgL_tmpz00_5750;

																					BgL_tmpz00_5750 =
																						((BgL_objectz00_bglt)
																						BgL_new1178z00_3262);
																					BGL_OBJECT_WIDENING_SET
																						(BgL_tmpz00_5750, BFALSE);
																				}
																				((BgL_objectz00_bglt)
																					BgL_new1178z00_3262);
																				BgL_new1174z00_3252 =
																					BgL_new1178z00_3262;
																			}
																			((((BgL_nodez00_bglt) COBJECT(
																							((BgL_nodez00_bglt)
																								BgL_new1174z00_3252)))->
																					BgL_locz00) =
																				((obj_t) (((BgL_nodez00_bglt)
																							COBJECT
																							(BgL_duplicated1176z00_3251))->
																						BgL_locz00)), BUNSPEC);
																			{
																				BgL_typez00_bglt BgL_auxz00_5757;

																				{	/* Cfa/closure.scm 332 */
																					obj_t BgL_arg1868z00_3253;
																					BgL_typez00_bglt BgL_arg1869z00_3254;

																					{	/* Cfa/closure.scm 332 */
																						BgL_approxz00_bglt
																							BgL_arg1870z00_3255;
																						obj_t BgL_arg1872z00_3256;

																						{
																							BgL_makezd2procedurezd2appz00_bglt
																								BgL_auxz00_5759;
																							{
																								obj_t BgL_auxz00_5760;

																								{	/* Cfa/closure.scm 332 */
																									BgL_objectz00_bglt
																										BgL_tmpz00_5761;
																									BgL_tmpz00_5761 =
																										((BgL_objectz00_bglt) (
																											(BgL_appz00_bglt)
																											BgL_allocz00_3247));
																									BgL_auxz00_5760 =
																										BGL_OBJECT_WIDENING
																										(BgL_tmpz00_5761);
																								}
																								BgL_auxz00_5759 =
																									(
																									(BgL_makezd2procedurezd2appz00_bglt)
																									BgL_auxz00_5760);
																							}
																							BgL_arg1870z00_3255 =
																								(((BgL_makezd2procedurezd2appz00_bglt) COBJECT(BgL_auxz00_5759))->BgL_approxz00);
																						}
																						{	/* Cfa/closure.scm 332 */
																							obj_t BgL_pairz00_4573;

																							BgL_pairz00_4573 =
																								(((BgL_appz00_bglt) COBJECT(
																										((BgL_appz00_bglt)
																											((BgL_appz00_bglt)
																												BgL_allocz00_3247))))->
																								BgL_argsz00);
																							BgL_arg1872z00_3256 =
																								CAR(BgL_pairz00_4573);
																						}
																						BgL_arg1868z00_3253 =
																							BGl_getzd2approxzd2typez00zzcfa_typez00
																							(BgL_arg1870z00_3255,
																							BgL_arg1872z00_3256);
																					}
																					{
																						BgL_nodez00_bglt BgL_auxz00_5772;

																						{
																							BgL_varz00_bglt BgL_auxz00_5773;

																							{	/* Cfa/closure.scm 333 */
																								obj_t BgL_pairz00_4574;

																								BgL_pairz00_4574 =
																									(((BgL_appz00_bglt) COBJECT(
																											((BgL_appz00_bglt)
																												((BgL_appz00_bglt)
																													BgL_allocz00_3247))))->
																									BgL_argsz00);
																								BgL_auxz00_5773 =
																									((BgL_varz00_bglt)
																									CAR(BgL_pairz00_4574));
																							}
																							BgL_auxz00_5772 =
																								((BgL_nodez00_bglt)
																								BgL_auxz00_5773);
																						}
																						BgL_arg1869z00_3254 =
																							(((BgL_nodez00_bglt)
																								COBJECT(BgL_auxz00_5772))->
																							BgL_typez00);
																					}
																					BgL_auxz00_5757 =
																						BGl_strictzd2nodezd2typez00zzast_nodez00
																						(((BgL_typez00_bglt)
																							BgL_arg1868z00_3253),
																						BgL_arg1869z00_3254);
																				}
																				((((BgL_nodez00_bglt) COBJECT(
																								((BgL_nodez00_bglt)
																									BgL_new1174z00_3252)))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) BgL_auxz00_5757),
																					BUNSPEC);
																			}
																			((((BgL_varz00_bglt) COBJECT(
																							((BgL_varz00_bglt)
																								BgL_new1174z00_3252)))->
																					BgL_variablez00) =
																				((BgL_variablez00_bglt) ((
																							(BgL_varz00_bglt)
																							COBJECT(((BgL_varz00_bglt)
																									BgL_duplicated1176z00_3251)))->
																						BgL_variablez00)), BUNSPEC);
																			BgL_arg1866z00_3250 = BgL_new1174z00_3252;
																		}
																		((((BgL_funcallz00_bglt)
																					COBJECT(BgL_appz00_3238))->
																				BgL_funz00) =
																			((BgL_nodez00_bglt) ((BgL_nodez00_bglt)
																					BgL_arg1866z00_3250)), BUNSPEC);
																	}
																	{	/* Cfa/closure.scm 334 */
																		obj_t BgL_arg1878z00_3264;

																		{	/* Cfa/closure.scm 334 */
																			obj_t BgL_arg1879z00_3265;

																			{	/* Cfa/closure.scm 334 */
																				obj_t BgL_pairz00_4578;

																				BgL_pairz00_4578 =
																					(((BgL_appz00_bglt) COBJECT(
																							((BgL_appz00_bglt)
																								((BgL_appz00_bglt)
																									BgL_allocz00_3247))))->
																					BgL_argsz00);
																				BgL_arg1879z00_3265 =
																					CAR(BgL_pairz00_4578);
																			}
																			{	/* Cfa/closure.scm 334 */
																				obj_t BgL_list1880z00_3266;

																				BgL_list1880z00_3266 =
																					MAKE_YOUNG_PAIR(BgL_arg1879z00_3265,
																					BNIL);
																				BgL_arg1878z00_3264 =
																					BgL_list1880z00_3266;
																		}}
																		((((BgL_funcallz00_bglt)
																					COBJECT(BgL_appz00_3238))->
																				BgL_functionsz00) =
																			((obj_t) BgL_arg1878z00_3264), BUNSPEC);
																	}
																	{	/* Cfa/closure.scm 335 */
																		obj_t BgL_vz00_4582;

																		BgL_vz00_4582 = CNST_TABLE_REF(1);
																		((((BgL_funcallz00_bglt)
																					COBJECT(BgL_appz00_3238))->
																				BgL_strengthz00) =
																			((obj_t) BgL_vz00_4582), BUNSPEC);
																}}
															else
																{	/* Cfa/closure.scm 336 */
																	bool_t BgL_test2350z00_5798;

																	{
																		BgL_makezd2procedurezd2appz00_bglt
																			BgL_auxz00_5799;
																		{
																			obj_t BgL_auxz00_5800;

																			{	/* Cfa/closure.scm 336 */
																				BgL_objectz00_bglt BgL_tmpz00_5801;

																				BgL_tmpz00_5801 =
																					((BgL_objectz00_bglt)
																					((BgL_appz00_bglt)
																						BgL_allocz00_3247));
																				BgL_auxz00_5800 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_5801);
																			}
																			BgL_auxz00_5799 =
																				((BgL_makezd2procedurezd2appz00_bglt)
																				BgL_auxz00_5800);
																		}
																		BgL_test2350z00_5798 =
																			(((BgL_makezd2procedurezd2appz00_bglt)
																				COBJECT(BgL_auxz00_5799))->BgL_tz00);
																	}
																	if (BgL_test2350z00_5798)
																		{	/* Cfa/closure.scm 336 */
																			{	/* Cfa/closure.scm 339 */
																				obj_t BgL_fz00_3269;

																				if (NULLP(BgL_alloczd2listzd2_3241))
																					{	/* Cfa/closure.scm 339 */
																						BgL_fz00_3269 = BNIL;
																					}
																				else
																					{	/* Cfa/closure.scm 339 */
																						obj_t BgL_head1532z00_3272;

																						BgL_head1532z00_3272 =
																							MAKE_YOUNG_PAIR(BNIL, BNIL);
																						{
																							obj_t BgL_l1530z00_3274;
																							obj_t BgL_tail1533z00_3275;

																							BgL_l1530z00_3274 =
																								BgL_alloczd2listzd2_3241;
																							BgL_tail1533z00_3275 =
																								BgL_head1532z00_3272;
																						BgL_zc3z04anonymousza31885ze3z87_3276:
																							if (NULLP
																								(BgL_l1530z00_3274))
																								{	/* Cfa/closure.scm 339 */
																									BgL_fz00_3269 =
																										CDR(BgL_head1532z00_3272);
																								}
																							else
																								{	/* Cfa/closure.scm 339 */
																									obj_t BgL_newtail1534z00_3278;

																									{	/* Cfa/closure.scm 339 */
																										obj_t BgL_arg1888z00_3280;

																										{	/* Cfa/closure.scm 340 */
																											obj_t BgL_pairz00_4587;

																											BgL_pairz00_4587 =
																												(((BgL_appz00_bglt)
																													COBJECT((
																															(BgL_appz00_bglt)
																															((BgL_appz00_bglt)
																																CAR(((obj_t)
																																		BgL_l1530z00_3274))))))->
																												BgL_argsz00);
																											BgL_arg1888z00_3280 =
																												CAR(BgL_pairz00_4587);
																										}
																										BgL_newtail1534z00_3278 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1888z00_3280,
																											BNIL);
																									}
																									SET_CDR(BgL_tail1533z00_3275,
																										BgL_newtail1534z00_3278);
																									{	/* Cfa/closure.scm 339 */
																										obj_t BgL_arg1887z00_3279;

																										BgL_arg1887z00_3279 =
																											CDR(
																											((obj_t)
																												BgL_l1530z00_3274));
																										{
																											obj_t
																												BgL_tail1533z00_5824;
																											obj_t BgL_l1530z00_5823;

																											BgL_l1530z00_5823 =
																												BgL_arg1887z00_3279;
																											BgL_tail1533z00_5824 =
																												BgL_newtail1534z00_3278;
																											BgL_tail1533z00_3275 =
																												BgL_tail1533z00_5824;
																											BgL_l1530z00_3274 =
																												BgL_l1530z00_5823;
																											goto
																												BgL_zc3z04anonymousza31885ze3z87_3276;
																										}
																									}
																								}
																						}
																					}
																				((((BgL_funcallz00_bglt)
																							COBJECT(BgL_appz00_3238))->
																						BgL_functionsz00) =
																					((obj_t) BgL_fz00_3269), BUNSPEC);
																				{	/* Cfa/closure.scm 343 */
																					obj_t BgL_vz00_4592;

																					BgL_vz00_4592 = CNST_TABLE_REF(2);
																					((((BgL_funcallz00_bglt)
																								COBJECT(BgL_appz00_3238))->
																							BgL_strengthz00) =
																						((obj_t) BgL_vz00_4592), BUNSPEC);
																				}
																			}
																		}
																	else
																		{	/* Cfa/closure.scm 336 */
																			CNST_TABLE_REF(8);
																		}
																}
														}
													}
											}
										}
									}
								}
							}
						}
						{
							obj_t BgL_l1535z00_5829;

							BgL_l1535z00_5829 = CDR(BgL_l1535z00_3235);
							BgL_l1535z00_3235 = BgL_l1535z00_5829;
							goto BgL_zc3z04anonymousza31857ze3z87_3236;
						}
					}
				else
					{	/* Cfa/closure.scm 312 */
						((bool_t) 1);
					}
			}
			if (CBOOL
				(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00))
				{	/* Cfa/closure.scm 348 */
					{	/* Cfa/closure.scm 354 */
						obj_t BgL_funcallzd2lzd2_3290;

						{	/* Cfa/closure.scm 354 */
							obj_t BgL_hook1541z00_3314;

							BgL_hook1541z00_3314 = MAKE_YOUNG_PAIR(BFALSE, BNIL);
							{
								obj_t BgL_l1538z00_3316;
								obj_t BgL_h1539z00_3317;

								BgL_l1538z00_3316 = BGl_za2funcallzd2listza2zd2zzcfa_closurez00;
								BgL_h1539z00_3317 = BgL_hook1541z00_3314;
							BgL_zc3z04anonymousza31904ze3z87_3318:
								if (NULLP(BgL_l1538z00_3316))
									{	/* Cfa/closure.scm 354 */
										BgL_funcallzd2lzd2_3290 = CDR(BgL_hook1541z00_3314);
									}
								else
									{	/* Cfa/closure.scm 354 */
										if (
											((((BgL_funcallz00_bglt) COBJECT(
															((BgL_funcallz00_bglt)
																CAR(
																	((obj_t) BgL_l1538z00_3316)))))->
													BgL_strengthz00) == CNST_TABLE_REF(2)))
											{	/* Cfa/closure.scm 354 */
												obj_t BgL_nh1540z00_3323;

												{	/* Cfa/closure.scm 354 */
													obj_t BgL_arg1911z00_3325;

													BgL_arg1911z00_3325 =
														CAR(((obj_t) BgL_l1538z00_3316));
													BgL_nh1540z00_3323 =
														MAKE_YOUNG_PAIR(BgL_arg1911z00_3325, BNIL);
												}
												SET_CDR(BgL_h1539z00_3317, BgL_nh1540z00_3323);
												{	/* Cfa/closure.scm 354 */
													obj_t BgL_arg1910z00_3324;

													BgL_arg1910z00_3324 =
														CDR(((obj_t) BgL_l1538z00_3316));
													{
														obj_t BgL_h1539z00_5851;
														obj_t BgL_l1538z00_5850;

														BgL_l1538z00_5850 = BgL_arg1910z00_3324;
														BgL_h1539z00_5851 = BgL_nh1540z00_3323;
														BgL_h1539z00_3317 = BgL_h1539z00_5851;
														BgL_l1538z00_3316 = BgL_l1538z00_5850;
														goto BgL_zc3z04anonymousza31904ze3z87_3318;
													}
												}
											}
										else
											{	/* Cfa/closure.scm 354 */
												obj_t BgL_arg1912z00_3326;

												BgL_arg1912z00_3326 = CDR(((obj_t) BgL_l1538z00_3316));
												{
													obj_t BgL_l1538z00_5854;

													BgL_l1538z00_5854 = BgL_arg1912z00_3326;
													BgL_l1538z00_3316 = BgL_l1538z00_5854;
													goto BgL_zc3z04anonymousza31904ze3z87_3318;
												}
											}
									}
							}
						}
						{

						BgL_zc3z04anonymousza31893ze3z87_3292:
							{	/* Cfa/closure.scm 358 */
								bool_t BgL_contz00_3293;

								BgL_contz00_3293 = ((bool_t) 0);
								{
									obj_t BgL_l1542z00_3295;

									BgL_l1542z00_3295 = BgL_funcallzd2lzd2_3290;
								BgL_zc3z04anonymousza31894ze3z87_3296:
									if (PAIRP(BgL_l1542z00_3295))
										{	/* Cfa/closure.scm 359 */
											{	/* Cfa/closure.scm 360 */
												BgL_funcallz00_bglt BgL_appz00_3298;

												BgL_appz00_3298 =
													((BgL_funcallz00_bglt) CAR(BgL_l1542z00_3295));
												{	/* Cfa/closure.scm 360 */
													BgL_nodez00_bglt BgL_funz00_3299;

													BgL_funz00_3299 =
														(((BgL_funcallz00_bglt) COBJECT(BgL_appz00_3298))->
														BgL_funz00);
													{	/* Cfa/closure.scm 360 */
														BgL_approxz00_bglt BgL_approxz00_3300;

														BgL_approxz00_3300 =
															BGl_cfaz12z12zzcfa_cfaz00(BgL_funz00_3299);
														{	/* Cfa/closure.scm 361 */
															obj_t BgL_appsz00_3301;

															BgL_appsz00_3301 =
																BGl_setzd2ze3listz31zzcfa_setz00(
																(((BgL_approxz00_bglt)
																		COBJECT(BgL_approxz00_3300))->
																	BgL_allocsz00));
															{	/* Cfa/closure.scm 362 */

																if (PAIRP(BgL_appsz00_3301))
																	{	/* Cfa/closure.scm 363 */
																		{	/* Cfa/closure.scm 364 */
																			bool_t BgL_test2358z00_5865;

																			{	/* Cfa/closure.scm 364 */
																				obj_t BgL_tmpz00_5866;

																				BgL_tmpz00_5866 = CDR(BgL_appsz00_3301);
																				BgL_test2358z00_5865 =
																					PAIRP(BgL_tmpz00_5866);
																			}
																			if (BgL_test2358z00_5865)
																				{	/* Cfa/closure.scm 366 */
																					bool_t BgL__ortest_1179z00_3305;

																					BgL__ortest_1179z00_3305 =
																						BGl_mergezd2appzd2typesz12z12zzcfa_closurez00
																						(BgL_appsz00_3301);
																					if (BgL__ortest_1179z00_3305)
																						{	/* Cfa/closure.scm 366 */
																							BgL_contz00_3293 =
																								BgL__ortest_1179z00_3305;
																						}
																					else
																						{	/* Cfa/closure.scm 366 */
																							BgL_contz00_3293 =
																								BgL_contz00_3293;
																						}
																				}
																			else
																				{	/* Cfa/closure.scm 364 */
																					BFALSE;
																				}
																		}
																		{	/* Cfa/closure.scm 367 */
																			bool_t BgL_mz00_3307;

																			{	/* Cfa/closure.scm 368 */
																				BgL_approxz00_bglt BgL_arg1901z00_3309;

																				BgL_arg1901z00_3309 =
																					BGl_cfaz12z12zzcfa_cfaz00(
																					((BgL_nodez00_bglt) BgL_appz00_3298));
																				BgL_mz00_3307 =
																					BGl_mergezd2appzd2returnzd2typesz12zc0zzcfa_closurez00
																					(BgL_appsz00_3301,
																					BgL_arg1901z00_3309);
																			}
																			{	/* Cfa/closure.scm 369 */
																				bool_t BgL__ortest_1180z00_3308;

																				BgL__ortest_1180z00_3308 =
																					BgL_contz00_3293;
																				if (BgL__ortest_1180z00_3308)
																					{	/* Cfa/closure.scm 369 */
																						BgL_contz00_3293 =
																							BgL__ortest_1180z00_3308;
																					}
																				else
																					{	/* Cfa/closure.scm 369 */
																						BgL_contz00_3293 = BgL_mz00_3307;
																					}
																			}
																		}
																	}
																else
																	{	/* Cfa/closure.scm 363 */
																		BFALSE;
																	}
															}
														}
													}
												}
											}
											{
												obj_t BgL_l1542z00_5875;

												BgL_l1542z00_5875 = CDR(BgL_l1542z00_3295);
												BgL_l1542z00_3295 = BgL_l1542z00_5875;
												goto BgL_zc3z04anonymousza31894ze3z87_3296;
											}
										}
									else
										{	/* Cfa/closure.scm 359 */
											((bool_t) 1);
										}
								}
								if (BgL_contz00_3293)
									{	/* Cfa/closure.scm 371 */
										goto BgL_zc3z04anonymousza31893ze3z87_3292;
									}
								else
									{	/* Cfa/closure.scm 371 */
										return ((bool_t) 0);
									}
							}
						}
					}
				}
			else
				{	/* Cfa/closure.scm 348 */
					return ((bool_t) 0);
				}
		}

	}



/* merge-app-return-types! */
	bool_t BGl_mergezd2appzd2returnzd2typesz12zc0zzcfa_closurez00(obj_t
		BgL_appsz00_28, BgL_approxz00_bglt BgL_approxz00_29)
	{
		{	/* Cfa/closure.scm 377 */
			{	/* Cfa/closure.scm 378 */
				obj_t BgL_app0z00_3330;

				BgL_app0z00_3330 = CAR(BgL_appsz00_28);
				{	/* Cfa/closure.scm 378 */
					BgL_variablez00_bglt BgL_f0z00_3331;

					{
						BgL_varz00_bglt BgL_auxz00_5879;

						{	/* Cfa/closure.scm 379 */
							obj_t BgL_pairz00_4608;

							BgL_pairz00_4608 =
								(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt)
											((BgL_appz00_bglt) BgL_app0z00_3330))))->BgL_argsz00);
							BgL_auxz00_5879 = ((BgL_varz00_bglt) CAR(BgL_pairz00_4608));
						}
						BgL_f0z00_3331 =
							(((BgL_varz00_bglt) COBJECT(BgL_auxz00_5879))->BgL_variablez00);
					}
					{	/* Cfa/closure.scm 379 */
						BgL_approxz00_bglt BgL_r0z00_3332;

						{	/* Cfa/closure.scm 380 */
							BgL_sfunz00_bglt BgL_oz00_4611;

							BgL_oz00_4611 =
								((BgL_sfunz00_bglt)
								(((BgL_variablez00_bglt) COBJECT(BgL_f0z00_3331))->
									BgL_valuez00));
							{
								BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_5888;

								{
									obj_t BgL_auxz00_5889;

									{	/* Cfa/closure.scm 380 */
										BgL_objectz00_bglt BgL_tmpz00_5890;

										BgL_tmpz00_5890 = ((BgL_objectz00_bglt) BgL_oz00_4611);
										BgL_auxz00_5889 = BGL_OBJECT_WIDENING(BgL_tmpz00_5890);
									}
									BgL_auxz00_5888 =
										((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_5889);
								}
								BgL_r0z00_3332 =
									(((BgL_internzd2sfunzf2cinfoz20_bglt)
										COBJECT(BgL_auxz00_5888))->BgL_approxz00);
							}
						}
						{	/* Cfa/closure.scm 380 */
							BgL_typez00_bglt BgL_rt0z00_3333;

							BgL_rt0z00_3333 =
								(((BgL_approxz00_bglt) COBJECT(BgL_r0z00_3332))->BgL_typez00);
							{	/* Cfa/closure.scm 381 */
								BgL_typez00_bglt BgL_brt0z00_3334;

								BgL_brt0z00_3334 =
									BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_rt0z00_3333);
								{	/* Cfa/closure.scm 382 */
									BgL_typez00_bglt BgL_atz00_3335;

									BgL_atz00_3335 =
										(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_29))->
										BgL_typez00);
									{	/* Cfa/closure.scm 383 */

										{	/* Cfa/closure.scm 384 */
											bool_t BgL_test2362z00_5898;

											if (
												(((obj_t) BgL_rt0z00_3333) == ((obj_t) BgL_atz00_3335)))
												{	/* Cfa/closure.scm 384 */
													BgL_test2362z00_5898 = ((bool_t) 1);
												}
											else
												{	/* Cfa/closure.scm 384 */
													if (BGl_bigloozd2typezf3z21zztype_typez00
														(BgL_rt0z00_3333))
														{	/* Cfa/closure.scm 384 */
															BgL_test2362z00_5898 =
																BGl_bigloozd2typezf3z21zztype_typez00
																(BgL_atz00_3335);
														}
													else
														{	/* Cfa/closure.scm 384 */
															BgL_test2362z00_5898 = ((bool_t) 0);
														}
												}
											if (BgL_test2362z00_5898)
												{	/* Cfa/closure.scm 384 */
													return ((bool_t) 0);
												}
											else
												{	/* Cfa/closure.scm 384 */
													((((BgL_approxz00_bglt) COBJECT(BgL_approxz00_29))->
															BgL_typez00) =
														((BgL_typez00_bglt) (((BgL_approxz00_bglt)
																	COBJECT(BgL_r0z00_3332))->BgL_typez00)),
														BUNSPEC);
													return ((bool_t) 1);
												}
										}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* merge-app-types! */
	bool_t BGl_mergezd2appzd2typesz12z12zzcfa_closurez00(obj_t BgL_apps0z00_30)
	{
		{	/* Cfa/closure.scm 391 */
			{
				BgL_appz00_bglt BgL_app0z00_3356;
				BgL_appz00_bglt BgL_app1z00_3357;

				{	/* Cfa/closure.scm 460 */
					obj_t BgL_g1186z00_3344;

					BgL_g1186z00_3344 = CDR(((obj_t) BgL_apps0z00_30));
					{
						obj_t BgL_appsz00_3346;
						bool_t BgL_contz00_3347;

						BgL_appsz00_3346 = BgL_g1186z00_3344;
						BgL_contz00_3347 = ((bool_t) 0);
					BgL_zc3z04anonymousza31920ze3z87_3348:
						if (NULLP(BgL_appsz00_3346))
							{	/* Cfa/closure.scm 462 */
								return BgL_contz00_3347;
							}
						else
							{	/* Cfa/closure.scm 464 */
								obj_t BgL_arg1923z00_3350;
								bool_t BgL_arg1924z00_3351;

								BgL_arg1923z00_3350 = CDR(((obj_t) BgL_appsz00_3346));
								{	/* Cfa/closure.scm 464 */
									bool_t BgL__ortest_1187z00_3352;

									{	/* Cfa/closure.scm 464 */
										obj_t BgL_arg1925z00_3353;
										obj_t BgL_arg1926z00_3354;

										BgL_arg1925z00_3353 = CAR(((obj_t) BgL_apps0z00_30));
										BgL_arg1926z00_3354 = CAR(((obj_t) BgL_appsz00_3346));
										BgL_app0z00_3356 = ((BgL_appz00_bglt) BgL_arg1925z00_3353);
										BgL_app1z00_3357 = ((BgL_appz00_bglt) BgL_arg1926z00_3354);
										{	/* Cfa/closure.scm 394 */
											BgL_variablez00_bglt BgL_f0z00_3359;

											{
												BgL_varz00_bglt BgL_auxz00_5918;

												{	/* Cfa/closure.scm 394 */
													obj_t BgL_pairz00_4619;

													BgL_pairz00_4619 =
														(((BgL_appz00_bglt) COBJECT(
																((BgL_appz00_bglt) BgL_app0z00_3356)))->
														BgL_argsz00);
													BgL_auxz00_5918 =
														((BgL_varz00_bglt) CAR(BgL_pairz00_4619));
												}
												BgL_f0z00_3359 =
													(((BgL_varz00_bglt) COBJECT(BgL_auxz00_5918))->
													BgL_variablez00);
											}
											{	/* Cfa/closure.scm 394 */
												BgL_variablez00_bglt BgL_f1z00_3360;

												{
													BgL_varz00_bglt BgL_auxz00_5924;

													{	/* Cfa/closure.scm 395 */
														obj_t BgL_pairz00_4622;

														BgL_pairz00_4622 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_app1z00_3357)))->
															BgL_argsz00);
														BgL_auxz00_5924 =
															((BgL_varz00_bglt) CAR(BgL_pairz00_4622));
													}
													BgL_f1z00_3360 =
														(((BgL_varz00_bglt) COBJECT(BgL_auxz00_5924))->
														BgL_variablez00);
												}
												{	/* Cfa/closure.scm 395 */

													{	/* Cfa/closure.scm 397 */
														BgL_approxz00_bglt BgL_r0z00_3361;

														{	/* Cfa/closure.scm 397 */
															BgL_sfunz00_bglt BgL_oz00_4625;

															BgL_oz00_4625 =
																((BgL_sfunz00_bglt)
																(((BgL_variablez00_bglt)
																		COBJECT(BgL_f0z00_3359))->BgL_valuez00));
															{
																BgL_internzd2sfunzf2cinfoz20_bglt
																	BgL_auxz00_5932;
																{
																	obj_t BgL_auxz00_5933;

																	{	/* Cfa/closure.scm 397 */
																		BgL_objectz00_bglt BgL_tmpz00_5934;

																		BgL_tmpz00_5934 =
																			((BgL_objectz00_bglt) BgL_oz00_4625);
																		BgL_auxz00_5933 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_5934);
																	}
																	BgL_auxz00_5932 =
																		((BgL_internzd2sfunzf2cinfoz20_bglt)
																		BgL_auxz00_5933);
																}
																BgL_r0z00_3361 =
																	(((BgL_internzd2sfunzf2cinfoz20_bglt)
																		COBJECT(BgL_auxz00_5932))->BgL_approxz00);
															}
														}
														{	/* Cfa/closure.scm 397 */
															BgL_approxz00_bglt BgL_r1z00_3362;

															{	/* Cfa/closure.scm 398 */
																BgL_sfunz00_bglt BgL_oz00_4628;

																BgL_oz00_4628 =
																	((BgL_sfunz00_bglt)
																	(((BgL_variablez00_bglt)
																			COBJECT(BgL_f1z00_3360))->BgL_valuez00));
																{
																	BgL_internzd2sfunzf2cinfoz20_bglt
																		BgL_auxz00_5941;
																	{
																		obj_t BgL_auxz00_5942;

																		{	/* Cfa/closure.scm 398 */
																			BgL_objectz00_bglt BgL_tmpz00_5943;

																			BgL_tmpz00_5943 =
																				((BgL_objectz00_bglt) BgL_oz00_4628);
																			BgL_auxz00_5942 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_5943);
																		}
																		BgL_auxz00_5941 =
																			((BgL_internzd2sfunzf2cinfoz20_bglt)
																			BgL_auxz00_5942);
																	}
																	BgL_r1z00_3362 =
																		(((BgL_internzd2sfunzf2cinfoz20_bglt)
																			COBJECT(BgL_auxz00_5941))->BgL_approxz00);
																}
															}
															{	/* Cfa/closure.scm 398 */
																BgL_typez00_bglt BgL_rt0z00_3363;

																BgL_rt0z00_3363 =
																	(((BgL_approxz00_bglt)
																		COBJECT(BgL_r0z00_3361))->BgL_typez00);
																{	/* Cfa/closure.scm 399 */
																	BgL_typez00_bglt BgL_rt1z00_3364;

																	BgL_rt1z00_3364 =
																		(((BgL_approxz00_bglt)
																			COBJECT(BgL_r1z00_3362))->BgL_typez00);
																	{	/* Cfa/closure.scm 400 */
																		BgL_typez00_bglt BgL_brt0z00_3365;

																		BgL_brt0z00_3365 =
																			BGl_getzd2bigloozd2typez00zztype_cachez00
																			(BgL_rt0z00_3363);
																		{	/* Cfa/closure.scm 401 */
																			BgL_typez00_bglt BgL_brt1z00_3366;

																			BgL_brt1z00_3366 =
																				BGl_getzd2bigloozd2typez00zztype_cachez00
																				(BgL_rt1z00_3364);
																			{	/* Cfa/closure.scm 402 */

																				{	/* Cfa/closure.scm 403 */
																					bool_t BgL_test2366z00_5952;

																					if (
																						(((obj_t) BgL_rt0z00_3363) ==
																							((obj_t) BgL_rt1z00_3364)))
																						{	/* Cfa/closure.scm 403 */
																							BgL_test2366z00_5952 =
																								((bool_t) 1);
																						}
																					else
																						{	/* Cfa/closure.scm 403 */
																							if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_rt0z00_3363))
																								{	/* Cfa/closure.scm 404 */
																									BgL_test2366z00_5952 =
																										BGl_bigloozd2typezf3z21zztype_typez00
																										(BgL_rt1z00_3364);
																								}
																							else
																								{	/* Cfa/closure.scm 404 */
																									BgL_test2366z00_5952 =
																										((bool_t) 0);
																								}
																						}
																					if (BgL_test2366z00_5952)
																						{	/* Cfa/closure.scm 403 */
																							BFALSE;
																						}
																					else
																						{	/* Cfa/closure.scm 403 */
																							if (
																								(((obj_t) BgL_rt0z00_3363) ==
																									((obj_t) BgL_brt0z00_3365)))
																								{	/* Cfa/closure.scm 405 */
																									BFALSE;
																								}
																							else
																								{	/* Cfa/closure.scm 405 */
																									((((BgL_approxz00_bglt)
																												COBJECT
																												(BgL_r0z00_3361))->
																											BgL_typez00) =
																										((BgL_typez00_bglt)
																											BgL_brt0z00_3365),
																										BUNSPEC);
																								}
																							if (
																								(((obj_t) BgL_rt1z00_3364) ==
																									((obj_t) BgL_brt1z00_3366)))
																								{	/* Cfa/closure.scm 407 */
																									BFALSE;
																								}
																							else
																								{	/* Cfa/closure.scm 407 */
																									((((BgL_approxz00_bglt)
																												COBJECT
																												(BgL_r1z00_3362))->
																											BgL_typez00) =
																										((BgL_typez00_bglt)
																											BgL_brt1z00_3366),
																										BUNSPEC);
																								}
																						}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
													{	/* Cfa/closure.scm 409 */
														obj_t BgL_g1183z00_3372;
														obj_t BgL_g1184z00_3373;

														{	/* Cfa/closure.scm 409 */
															obj_t BgL_pairz00_4638;

															BgL_pairz00_4638 =
																(((BgL_sfunz00_bglt) COBJECT(
																		((BgL_sfunz00_bglt)
																			(((BgL_variablez00_bglt)
																					COBJECT(BgL_f0z00_3359))->
																				BgL_valuez00))))->BgL_argsz00);
															BgL_g1183z00_3372 = CDR(BgL_pairz00_4638);
														}
														{	/* Cfa/closure.scm 410 */
															obj_t BgL_pairz00_4641;

															BgL_pairz00_4641 =
																(((BgL_sfunz00_bglt) COBJECT(
																		((BgL_sfunz00_bglt)
																			(((BgL_variablez00_bglt)
																					COBJECT(BgL_f1z00_3360))->
																				BgL_valuez00))))->BgL_argsz00);
															BgL_g1184z00_3373 = CDR(BgL_pairz00_4641);
														}
														{
															obj_t BgL_a0z00_3375;
															obj_t BgL_a1z00_3376;
															bool_t BgL_contz00_3377;

															BgL_a0z00_3375 = BgL_g1183z00_3372;
															BgL_a1z00_3376 = BgL_g1184z00_3373;
															BgL_contz00_3377 = ((bool_t) 0);
														BgL_zc3z04anonymousza31931ze3z87_3378:
															{	/* Cfa/closure.scm 412 */
																bool_t BgL_test2371z00_5978;

																if (PAIRP(BgL_a0z00_3375))
																	{	/* Cfa/closure.scm 412 */
																		BgL_test2371z00_5978 =
																			PAIRP(BgL_a1z00_3376);
																	}
																else
																	{	/* Cfa/closure.scm 412 */
																		BgL_test2371z00_5978 = ((bool_t) 0);
																	}
																if (BgL_test2371z00_5978)
																	{	/* Cfa/closure.scm 413 */
																		BgL_approxz00_bglt BgL_p0z00_3381;

																		{	/* Cfa/closure.scm 413 */
																			BgL_svarz00_bglt BgL_oz00_4644;

																			BgL_oz00_4644 =
																				((BgL_svarz00_bglt)
																				(((BgL_variablez00_bglt) COBJECT(
																							((BgL_variablez00_bglt)
																								CAR(BgL_a0z00_3375))))->
																					BgL_valuez00));
																			{
																				BgL_svarzf2cinfozf2_bglt
																					BgL_auxz00_5986;
																				{
																					obj_t BgL_auxz00_5987;

																					{	/* Cfa/closure.scm 413 */
																						BgL_objectz00_bglt BgL_tmpz00_5988;

																						BgL_tmpz00_5988 =
																							((BgL_objectz00_bglt)
																							BgL_oz00_4644);
																						BgL_auxz00_5987 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_5988);
																					}
																					BgL_auxz00_5986 =
																						((BgL_svarzf2cinfozf2_bglt)
																						BgL_auxz00_5987);
																				}
																				BgL_p0z00_3381 =
																					(((BgL_svarzf2cinfozf2_bglt)
																						COBJECT(BgL_auxz00_5986))->
																					BgL_approxz00);
																			}
																		}
																		{	/* Cfa/closure.scm 413 */
																			BgL_approxz00_bglt BgL_p1z00_3382;

																			{	/* Cfa/closure.scm 414 */
																				BgL_svarz00_bglt BgL_oz00_4648;

																				BgL_oz00_4648 =
																					((BgL_svarz00_bglt)
																					(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									CAR(BgL_a1z00_3376))))->
																						BgL_valuez00));
																				{
																					BgL_svarzf2cinfozf2_bglt
																						BgL_auxz00_5997;
																					{
																						obj_t BgL_auxz00_5998;

																						{	/* Cfa/closure.scm 414 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_5999;
																							BgL_tmpz00_5999 =
																								((BgL_objectz00_bglt)
																								BgL_oz00_4648);
																							BgL_auxz00_5998 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_5999);
																						}
																						BgL_auxz00_5997 =
																							((BgL_svarzf2cinfozf2_bglt)
																							BgL_auxz00_5998);
																					}
																					BgL_p1z00_3382 =
																						(((BgL_svarzf2cinfozf2_bglt)
																							COBJECT(BgL_auxz00_5997))->
																						BgL_approxz00);
																				}
																			}
																			{	/* Cfa/closure.scm 414 */
																				BgL_typez00_bglt BgL_t0z00_3383;

																				BgL_t0z00_3383 =
																					(((BgL_approxz00_bglt)
																						COBJECT(BgL_p0z00_3381))->
																					BgL_typez00);
																				{	/* Cfa/closure.scm 415 */
																					BgL_typez00_bglt BgL_t1z00_3384;

																					BgL_t1z00_3384 =
																						(((BgL_approxz00_bglt)
																							COBJECT(BgL_p1z00_3382))->
																						BgL_typez00);
																					{	/* Cfa/closure.scm 416 */

																						{	/* Cfa/closure.scm 417 */
																							bool_t BgL_test2373z00_6006;

																							if (
																								(((obj_t) BgL_t0z00_3383) ==
																									((obj_t) BgL_t1z00_3384)))
																								{	/* Cfa/closure.scm 417 */
																									BgL_test2373z00_6006 =
																										((bool_t) 1);
																								}
																							else
																								{	/* Cfa/closure.scm 417 */
																									if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_t0z00_3383))
																										{	/* Cfa/closure.scm 417 */
																											BgL_test2373z00_6006 =
																												BGl_bigloozd2typezf3z21zztype_typez00
																												(BgL_t1z00_3384);
																										}
																									else
																										{	/* Cfa/closure.scm 417 */
																											BgL_test2373z00_6006 =
																												((bool_t) 0);
																										}
																								}
																							if (BgL_test2373z00_6006)
																								{
																									obj_t BgL_a1z00_6016;
																									obj_t BgL_a0z00_6014;

																									BgL_a0z00_6014 =
																										CDR(BgL_a0z00_3375);
																									BgL_a1z00_6016 =
																										CDR(BgL_a1z00_3376);
																									BgL_a1z00_3376 =
																										BgL_a1z00_6016;
																									BgL_a0z00_3375 =
																										BgL_a0z00_6014;
																									goto
																										BgL_zc3z04anonymousza31931ze3z87_3378;
																								}
																							else
																								{	/* Cfa/closure.scm 440 */
																									BgL_typez00_bglt
																										BgL_bt0z00_3389;
																									BgL_typez00_bglt
																										BgL_bt1z00_3390;
																									bool_t BgL_contz00_3391;

																									BgL_bt0z00_3389 =
																										BGl_getzd2bigloozd2typez00zztype_cachez00
																										(BgL_t0z00_3383);
																									BgL_bt1z00_3390 =
																										BGl_getzd2bigloozd2typez00zztype_cachez00
																										(BgL_t1z00_3384);
																									BgL_contz00_3391 =
																										((bool_t) 0);
																									if ((((obj_t) ((
																														(BgL_variablez00_bglt)
																														COBJECT((
																																(BgL_variablez00_bglt)
																																CAR
																																(BgL_a0z00_3375))))->
																													BgL_typez00)) ==
																											((obj_t)
																												BgL_bt0z00_3389)))
																										{	/* Cfa/closure.scm 443 */
																											BFALSE;
																										}
																									else
																										{	/* Cfa/closure.scm 443 */
																											{	/* Cfa/closure.scm 445 */
																												obj_t
																													BgL_arg1940z00_3395;
																												BgL_arg1940z00_3395 =
																													CAR(BgL_a0z00_3375);
																												((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_arg1940z00_3395)))->BgL_typez00) = ((BgL_typez00_bglt) BgL_bt0z00_3389), BUNSPEC);
																											}
																											((((BgL_approxz00_bglt)
																														COBJECT
																														(BgL_p0z00_3381))->
																													BgL_typez00) =
																												((BgL_typez00_bglt)
																													BgL_bt0z00_3389),
																												BUNSPEC);
																											BgL_contz00_3391 =
																												((bool_t) 1);
																										}
																									if (
																										(((obj_t)
																												(((BgL_variablez00_bglt)
																														COBJECT((
																																(BgL_variablez00_bglt)
																																CAR
																																(BgL_a1z00_3376))))->
																													BgL_typez00)) ==
																											((obj_t)
																												BgL_bt1z00_3390)))
																										{	/* Cfa/closure.scm 449 */
																											BFALSE;
																										}
																									else
																										{	/* Cfa/closure.scm 449 */
																											{	/* Cfa/closure.scm 451 */
																												obj_t
																													BgL_arg1946z00_3401;
																												BgL_arg1946z00_3401 =
																													CAR(BgL_a1z00_3376);
																												((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) BgL_arg1946z00_3401)))->BgL_typez00) = ((BgL_typez00_bglt) BgL_bt1z00_3390), BUNSPEC);
																											}
																											((((BgL_approxz00_bglt)
																														COBJECT
																														(BgL_p1z00_3382))->
																													BgL_typez00) =
																												((BgL_typez00_bglt)
																													BgL_bt1z00_3390),
																												BUNSPEC);
																											BgL_contz00_3391 =
																												((bool_t) 1);
																										}
																									{	/* Cfa/closure.scm 457 */
																										obj_t BgL_arg1949z00_3404;
																										obj_t BgL_arg1950z00_3405;

																										BgL_arg1949z00_3404 =
																											CDR(BgL_a0z00_3375);
																										BgL_arg1950z00_3405 =
																											CDR(BgL_a1z00_3376);
																										{
																											bool_t BgL_contz00_6046;
																											obj_t BgL_a1z00_6045;
																											obj_t BgL_a0z00_6044;

																											BgL_a0z00_6044 =
																												BgL_arg1949z00_3404;
																											BgL_a1z00_6045 =
																												BgL_arg1950z00_3405;
																											BgL_contz00_6046 =
																												BgL_contz00_3391;
																											BgL_contz00_3377 =
																												BgL_contz00_6046;
																											BgL_a1z00_3376 =
																												BgL_a1z00_6045;
																											BgL_a0z00_3375 =
																												BgL_a0z00_6044;
																											goto
																												BgL_zc3z04anonymousza31931ze3z87_3378;
																										}
																									}
																								}
																						}
																					}
																				}
																			}
																		}
																	}
																else
																	{	/* Cfa/closure.scm 412 */
																		BgL__ortest_1187z00_3352 = BgL_contz00_3377;
																	}
															}
														}
													}
												}
											}
										}
									}
									if (BgL__ortest_1187z00_3352)
										{	/* Cfa/closure.scm 464 */
											BgL_arg1924z00_3351 = BgL__ortest_1187z00_3352;
										}
									else
										{	/* Cfa/closure.scm 464 */
											BgL_arg1924z00_3351 = BgL_contz00_3347;
										}
								}
								{
									bool_t BgL_contz00_6051;
									obj_t BgL_appsz00_6050;

									BgL_appsz00_6050 = BgL_arg1923z00_3350;
									BgL_contz00_6051 = BgL_arg1924z00_3351;
									BgL_contz00_3347 = BgL_contz00_6051;
									BgL_appsz00_3346 = BgL_appsz00_6050;
									goto BgL_zc3z04anonymousza31920ze3z87_3348;
								}
							}
					}
				}
			}
		}

	}



/* type-closures! */
	BGL_EXPORTED_DEF obj_t BGl_typezd2closuresz12zc0zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 469 */
			{
				BgL_appz00_bglt BgL_appz00_3431;

				if (CBOOL
					(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00))
					{	/* Cfa/closure.scm 496 */
						obj_t BgL_g1549z00_3423;

						BgL_g1549z00_3423 =
							BGl_za2makezd2procedurezd2listza2z00zzcfa_closurez00;
						{
							obj_t BgL_l1547z00_3425;

							{	/* Cfa/closure.scm 496 */
								bool_t BgL_tmpz00_6054;

								BgL_l1547z00_3425 = BgL_g1549z00_3423;
							BgL_zc3z04anonymousza31963ze3z87_3426:
								if (PAIRP(BgL_l1547z00_3425))
									{	/* Cfa/closure.scm 496 */
										{	/* Cfa/closure.scm 496 */
											obj_t BgL_arg1965z00_3428;

											BgL_arg1965z00_3428 = CAR(BgL_l1547z00_3425);
											BgL_appz00_3431 = ((BgL_appz00_bglt) BgL_arg1965z00_3428);
											{	/* Cfa/closure.scm 480 */
												bool_t BgL_test2381z00_6058;

												{	/* Cfa/closure.scm 480 */
													bool_t BgL_test2382z00_6059;

													{
														BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_6060;

														{
															obj_t BgL_auxz00_6061;

															{	/* Cfa/closure.scm 480 */
																BgL_objectz00_bglt BgL_tmpz00_6062;

																BgL_tmpz00_6062 =
																	((BgL_objectz00_bglt) BgL_appz00_3431);
																BgL_auxz00_6061 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_6062);
															}
															BgL_auxz00_6060 =
																((BgL_makezd2procedurezd2appz00_bglt)
																BgL_auxz00_6061);
														}
														BgL_test2382z00_6059 =
															(((BgL_makezd2procedurezd2appz00_bglt)
																COBJECT(BgL_auxz00_6060))->BgL_xz00);
													}
													if (BgL_test2382z00_6059)
														{	/* Cfa/closure.scm 480 */
															BgL_test2381z00_6058 = ((bool_t) 1);
														}
													else
														{
															BgL_makezd2procedurezd2appz00_bglt
																BgL_auxz00_6067;
															{
																obj_t BgL_auxz00_6068;

																{	/* Cfa/closure.scm 480 */
																	BgL_objectz00_bglt BgL_tmpz00_6069;

																	BgL_tmpz00_6069 =
																		((BgL_objectz00_bglt) BgL_appz00_3431);
																	BgL_auxz00_6068 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_6069);
																}
																BgL_auxz00_6067 =
																	((BgL_makezd2procedurezd2appz00_bglt)
																	BgL_auxz00_6068);
															}
															BgL_test2381z00_6058 =
																(((BgL_makezd2procedurezd2appz00_bglt)
																	COBJECT(BgL_auxz00_6067))->BgL_tz00);
														}
												}
												if (BgL_test2381z00_6058)
													{	/* Cfa/closure.scm 480 */
														((bool_t) 0);
													}
												else
													{	/* Cfa/closure.scm 482 */
														BgL_variablez00_bglt BgL_varz00_3436;

														{
															BgL_varz00_bglt BgL_auxz00_6074;

															{	/* Cfa/closure.scm 482 */
																obj_t BgL_pairz00_4676;

																BgL_pairz00_4676 =
																	(((BgL_appz00_bglt) COBJECT(
																			((BgL_appz00_bglt) BgL_appz00_3431)))->
																	BgL_argsz00);
																BgL_auxz00_6074 =
																	((BgL_varz00_bglt) CAR(BgL_pairz00_4676));
															}
															BgL_varz00_3436 =
																(((BgL_varz00_bglt) COBJECT(BgL_auxz00_6074))->
																BgL_variablez00);
														}
														{	/* Cfa/closure.scm 482 */
															BgL_valuez00_bglt BgL_sfunz00_3437;

															BgL_sfunz00_3437 =
																(((BgL_variablez00_bglt)
																	COBJECT(BgL_varz00_3436))->BgL_valuez00);
															{	/* Cfa/closure.scm 483 */

																((((BgL_variablez00_bglt)
																			COBJECT(BgL_varz00_3436))->BgL_typez00) =
																	((BgL_typez00_bglt)
																		BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00
																		((((BgL_variablez00_bglt)
																					COBJECT(BgL_varz00_3436))->
																				BgL_typez00))), BUNSPEC);
																{	/* Cfa/closure.scm 486 */
																	obj_t BgL_g1546z00_3440;

																	{	/* Cfa/closure.scm 493 */
																		obj_t BgL_pairz00_4683;

																		BgL_pairz00_4683 =
																			(((BgL_sfunz00_bglt) COBJECT(
																					((BgL_sfunz00_bglt)
																						BgL_sfunz00_3437)))->BgL_argsz00);
																		BgL_g1546z00_3440 = CDR(BgL_pairz00_4683);
																	}
																	{
																		obj_t BgL_l1544z00_3442;

																		BgL_l1544z00_3442 = BgL_g1546z00_3440;
																	BgL_zc3z04anonymousza31972ze3z87_3443:
																		if (PAIRP(BgL_l1544z00_3442))
																			{	/* Cfa/closure.scm 493 */
																				{	/* Cfa/closure.scm 487 */
																					obj_t BgL_az00_3445;

																					BgL_az00_3445 =
																						CAR(BgL_l1544z00_3442);
																					{	/* Cfa/closure.scm 487 */
																						BgL_approxz00_bglt BgL_pz00_3446;

																						{	/* Cfa/closure.scm 487 */
																							BgL_svarz00_bglt BgL_oz00_4686;

																							BgL_oz00_4686 =
																								((BgL_svarz00_bglt)
																								(((BgL_variablez00_bglt)
																										COBJECT((
																												(BgL_variablez00_bglt)
																												BgL_az00_3445)))->
																									BgL_valuez00));
																							{
																								BgL_svarzf2cinfozf2_bglt
																									BgL_auxz00_6093;
																								{
																									obj_t BgL_auxz00_6094;

																									{	/* Cfa/closure.scm 487 */
																										BgL_objectz00_bglt
																											BgL_tmpz00_6095;
																										BgL_tmpz00_6095 =
																											((BgL_objectz00_bglt)
																											BgL_oz00_4686);
																										BgL_auxz00_6094 =
																											BGL_OBJECT_WIDENING
																											(BgL_tmpz00_6095);
																									}
																									BgL_auxz00_6093 =
																										((BgL_svarzf2cinfozf2_bglt)
																										BgL_auxz00_6094);
																								}
																								BgL_pz00_3446 =
																									(((BgL_svarzf2cinfozf2_bglt)
																										COBJECT(BgL_auxz00_6093))->
																									BgL_approxz00);
																							}
																						}
																						{	/* Cfa/closure.scm 487 */
																							BgL_typez00_bglt BgL_tz00_3447;

																							BgL_tz00_3447 =
																								(((BgL_approxz00_bglt)
																									COBJECT(BgL_pz00_3446))->
																								BgL_typez00);
																							{	/* Cfa/closure.scm 488 */

																								{	/* Cfa/closure.scm 492 */
																									BgL_typez00_bglt
																										BgL_arg1974z00_3448;
																									BgL_arg1974z00_3448 =
																										BGl_getzd2bigloozd2definedzd2typezd2zztype_cachez00
																										(BgL_tz00_3447);
																									((((BgL_variablez00_bglt)
																												COBJECT((
																														(BgL_variablez00_bglt)
																														BgL_az00_3445)))->
																											BgL_typez00) =
																										((BgL_typez00_bglt)
																											BgL_arg1974z00_3448),
																										BUNSPEC);
																								}
																							}
																						}
																					}
																				}
																				{
																					obj_t BgL_l1544z00_6104;

																					BgL_l1544z00_6104 =
																						CDR(BgL_l1544z00_3442);
																					BgL_l1544z00_3442 = BgL_l1544z00_6104;
																					goto
																						BgL_zc3z04anonymousza31972ze3z87_3443;
																				}
																			}
																		else
																			{	/* Cfa/closure.scm 493 */
																				((bool_t) 1);
																			}
																	}
																}
															}
														}
													}
											}
										}
										{
											obj_t BgL_l1547z00_6107;

											BgL_l1547z00_6107 = CDR(BgL_l1547z00_3425);
											BgL_l1547z00_3425 = BgL_l1547z00_6107;
											goto BgL_zc3z04anonymousza31963ze3z87_3426;
										}
									}
								else
									{	/* Cfa/closure.scm 496 */
										BgL_tmpz00_6054 = ((bool_t) 1);
									}
								return BBOOL(BgL_tmpz00_6054);
							}
						}
					}
				else
					{	/* Cfa/closure.scm 495 */
						return BFALSE;
					}
			}
		}

	}



/* &type-closures! */
	obj_t BGl_z62typezd2closuresz12za2zzcfa_closurez00(obj_t BgL_envz00_4919)
	{
		{	/* Cfa/closure.scm 469 */
			return BGl_typezd2closuresz12zc0zzcfa_closurez00();
		}

	}



/* light-access! */
	bool_t BGl_lightzd2accessz12zc0zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 501 */
			{
				obj_t BgL_l1550z00_3458;

				BgL_l1550z00_3458 = BGl_za2procedurezd2refzd2listza2z00zzcfa_closurez00;
			BgL_zc3z04anonymousza31980ze3z87_3459:
				if (PAIRP(BgL_l1550z00_3458))
					{	/* Cfa/closure.scm 502 */
						{	/* Cfa/closure.scm 504 */
							obj_t BgL_appz00_3461;

							BgL_appz00_3461 = CAR(BgL_l1550z00_3458);
							{	/* Cfa/closure.scm 504 */
								BgL_approxz00_bglt BgL_approxz00_3463;

								{	/* Cfa/closure.scm 505 */
									obj_t BgL_arg1986z00_3475;

									{	/* Cfa/closure.scm 505 */
										obj_t BgL_pairz00_4698;

										BgL_pairz00_4698 =
											(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt) BgL_appz00_3461)))->BgL_argsz00);
										BgL_arg1986z00_3475 = CAR(BgL_pairz00_4698);
									}
									BgL_approxz00_3463 =
										BGl_cfaz12z12zzcfa_cfaz00(
										((BgL_nodez00_bglt) BgL_arg1986z00_3475));
								}
								{	/* Cfa/closure.scm 505 */
									BgL_varz00_bglt BgL_funz00_3464;

									BgL_funz00_3464 =
										(((BgL_appz00_bglt) COBJECT(
												((BgL_appz00_bglt) BgL_appz00_3461)))->BgL_funz00);
									{	/* Cfa/closure.scm 506 */
										BgL_variablez00_bglt BgL_vfunz00_3465;

										BgL_vfunz00_3465 =
											(((BgL_varz00_bglt) COBJECT(BgL_funz00_3464))->
											BgL_variablez00);
										{	/* Cfa/closure.scm 507 */
											obj_t BgL_allocz00_3466;

											{	/* Cfa/closure.scm 508 */
												obj_t BgL_arg1985z00_3472;

												BgL_arg1985z00_3472 =
													(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_3463))->
													BgL_allocsz00);
												{	/* Cfa/closure.scm 508 */

													BgL_allocz00_3466 =
														BGl_setzd2headzd2zzcfa_setz00(BgL_arg1985z00_3472,
														BFALSE);
												}
											}
											{	/* Cfa/closure.scm 508 */

												{	/* Cfa/closure.scm 509 */
													bool_t BgL_test2385z00_6124;

													{	/* Cfa/closure.scm 509 */
														obj_t BgL_classz00_4702;

														BgL_classz00_4702 =
															BGl_makezd2procedurezd2appz00zzcfa_info2z00;
														if (BGL_OBJECTP(BgL_allocz00_3466))
															{	/* Cfa/closure.scm 509 */
																BgL_objectz00_bglt BgL_arg1807z00_4704;

																BgL_arg1807z00_4704 =
																	(BgL_objectz00_bglt) (BgL_allocz00_3466);
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Cfa/closure.scm 509 */
																		long BgL_idxz00_4710;

																		BgL_idxz00_4710 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_4704);
																		BgL_test2385z00_6124 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_4710 + 4L)) ==
																			BgL_classz00_4702);
																	}
																else
																	{	/* Cfa/closure.scm 509 */
																		bool_t BgL_res2223z00_4735;

																		{	/* Cfa/closure.scm 509 */
																			obj_t BgL_oclassz00_4718;

																			{	/* Cfa/closure.scm 509 */
																				obj_t BgL_arg1815z00_4726;
																				long BgL_arg1816z00_4727;

																				BgL_arg1815z00_4726 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Cfa/closure.scm 509 */
																					long BgL_arg1817z00_4728;

																					BgL_arg1817z00_4728 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_4704);
																					BgL_arg1816z00_4727 =
																						(BgL_arg1817z00_4728 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_4718 =
																					VECTOR_REF(BgL_arg1815z00_4726,
																					BgL_arg1816z00_4727);
																			}
																			{	/* Cfa/closure.scm 509 */
																				bool_t BgL__ortest_1115z00_4719;

																				BgL__ortest_1115z00_4719 =
																					(BgL_classz00_4702 ==
																					BgL_oclassz00_4718);
																				if (BgL__ortest_1115z00_4719)
																					{	/* Cfa/closure.scm 509 */
																						BgL_res2223z00_4735 =
																							BgL__ortest_1115z00_4719;
																					}
																				else
																					{	/* Cfa/closure.scm 509 */
																						long BgL_odepthz00_4720;

																						{	/* Cfa/closure.scm 509 */
																							obj_t BgL_arg1804z00_4721;

																							BgL_arg1804z00_4721 =
																								(BgL_oclassz00_4718);
																							BgL_odepthz00_4720 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_4721);
																						}
																						if ((4L < BgL_odepthz00_4720))
																							{	/* Cfa/closure.scm 509 */
																								obj_t BgL_arg1802z00_4723;

																								{	/* Cfa/closure.scm 509 */
																									obj_t BgL_arg1803z00_4724;

																									BgL_arg1803z00_4724 =
																										(BgL_oclassz00_4718);
																									BgL_arg1802z00_4723 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_4724, 4L);
																								}
																								BgL_res2223z00_4735 =
																									(BgL_arg1802z00_4723 ==
																									BgL_classz00_4702);
																							}
																						else
																							{	/* Cfa/closure.scm 509 */
																								BgL_res2223z00_4735 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2385z00_6124 = BgL_res2223z00_4735;
																	}
															}
														else
															{	/* Cfa/closure.scm 509 */
																BgL_test2385z00_6124 = ((bool_t) 0);
															}
													}
													if (BgL_test2385z00_6124)
														{	/* Cfa/closure.scm 513 */
															bool_t BgL_test2390z00_6147;

															{
																BgL_makezd2procedurezd2appz00_bglt
																	BgL_auxz00_6148;
																{
																	obj_t BgL_auxz00_6149;

																	{	/* Cfa/closure.scm 513 */
																		BgL_objectz00_bglt BgL_tmpz00_6150;

																		BgL_tmpz00_6150 =
																			((BgL_objectz00_bglt)
																			((BgL_appz00_bglt)
																				((BgL_appz00_bglt) BgL_allocz00_3466)));
																		BgL_auxz00_6149 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_6150);
																	}
																	BgL_auxz00_6148 =
																		((BgL_makezd2procedurezd2appz00_bglt)
																		BgL_auxz00_6149);
																}
																BgL_test2390z00_6147 =
																	(((BgL_makezd2procedurezd2appz00_bglt)
																		COBJECT(BgL_auxz00_6148))->BgL_xz00);
															}
															if (BgL_test2390z00_6147)
																{	/* Cfa/closure.scm 513 */
																	if (
																		(((obj_t) BgL_vfunz00_3465) ==
																			BGl_za2procedurezd2refza2zd2zzcfa_closurez00))
																		{	/* Cfa/closure.scm 515 */
																			BgL_variablez00_bglt BgL_vz00_4738;

																			BgL_vz00_4738 =
																				((BgL_variablez00_bglt)
																				BGl_za2procedurezd2elzd2refza2z00zzcfa_closurez00);
																			((((BgL_varz00_bglt)
																						COBJECT(BgL_funz00_3464))->
																					BgL_variablez00) =
																				((BgL_variablez00_bglt) BgL_vz00_4738),
																				BUNSPEC);
																		}
																	else
																		{	/* Cfa/closure.scm 516 */
																			BgL_variablez00_bglt BgL_vz00_4740;

																			BgL_vz00_4740 =
																				((BgL_variablez00_bglt)
																				BGl_za2procedurezd2elzd2setz12za2z12zzcfa_closurez00);
																			((((BgL_varz00_bglt)
																						COBJECT(BgL_funz00_3464))->
																					BgL_variablez00) =
																				((BgL_variablez00_bglt) BgL_vz00_4740),
																				BUNSPEC);
																		}
																}
															else
																{	/* Cfa/closure.scm 517 */
																	bool_t BgL_test2392z00_6164;

																	{
																		BgL_makezd2procedurezd2appz00_bglt
																			BgL_auxz00_6165;
																		{
																			obj_t BgL_auxz00_6166;

																			{	/* Cfa/closure.scm 517 */
																				BgL_objectz00_bglt BgL_tmpz00_6167;

																				BgL_tmpz00_6167 =
																					((BgL_objectz00_bglt)
																					((BgL_appz00_bglt)
																						((BgL_appz00_bglt)
																							BgL_allocz00_3466)));
																				BgL_auxz00_6166 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_6167);
																			}
																			BgL_auxz00_6165 =
																				((BgL_makezd2procedurezd2appz00_bglt)
																				BgL_auxz00_6166);
																		}
																		BgL_test2392z00_6164 =
																			(((BgL_makezd2procedurezd2appz00_bglt)
																				COBJECT(BgL_auxz00_6165))->BgL_tz00);
																	}
																	if (BgL_test2392z00_6164)
																		{	/* Cfa/closure.scm 517 */
																			if (
																				(((obj_t) BgL_vfunz00_3465) ==
																					BGl_za2procedurezd2refza2zd2zzcfa_closurez00))
																				{	/* Cfa/closure.scm 519 */
																					BgL_variablez00_bglt BgL_vz00_4743;

																					BgL_vz00_4743 =
																						((BgL_variablez00_bglt)
																						BGl_za2procedurezd2lzd2refza2z00zzcfa_closurez00);
																					((((BgL_varz00_bglt)
																								COBJECT(BgL_funz00_3464))->
																							BgL_variablez00) =
																						((BgL_variablez00_bglt)
																							BgL_vz00_4743), BUNSPEC);
																				}
																			else
																				{	/* Cfa/closure.scm 520 */
																					BgL_variablez00_bglt BgL_vz00_4745;

																					BgL_vz00_4745 =
																						((BgL_variablez00_bglt)
																						BGl_za2procedurezd2lzd2setz12za2z12zzcfa_closurez00);
																					((((BgL_varz00_bglt)
																								COBJECT(BgL_funz00_3464))->
																							BgL_variablez00) =
																						((BgL_variablez00_bglt)
																							BgL_vz00_4745), BUNSPEC);
																				}
																		}
																	else
																		{	/* Cfa/closure.scm 517 */
																			BFALSE;
																		}
																}
														}
													else
														{	/* Cfa/closure.scm 509 */
															BFALSE;
														}
												}
											}
										}
									}
								}
							}
						}
						{
							obj_t BgL_l1550z00_6181;

							BgL_l1550z00_6181 = CDR(BgL_l1550z00_3458);
							BgL_l1550z00_3458 = BgL_l1550z00_6181;
							goto BgL_zc3z04anonymousza31980ze3z87_3459;
						}
					}
				else
					{	/* Cfa/closure.scm 502 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* show-X-T */
	obj_t BGl_showzd2Xzd2Tz00zzcfa_closurez00(obj_t BgL_allocsz00_31)
	{
		{	/* Cfa/closure.scm 526 */
			{
				obj_t BgL_xpz00_3483;
				obj_t BgL_tpz00_3484;
				obj_t BgL_allocsz00_3485;

				BgL_xpz00_3483 = BNIL;
				BgL_tpz00_3484 = BNIL;
				BgL_allocsz00_3485 = BgL_allocsz00_31;
			BgL_zc3z04anonymousza31989ze3z87_3486:
				if (NULLP(BgL_allocsz00_3485))
					{	/* Cfa/closure.scm 537 */
						BGl_showze70ze7zzcfa_closurez00(CNST_TABLE_REF(9), BgL_xpz00_3483);
						BGl_showze70ze7zzcfa_closurez00(CNST_TABLE_REF(10), BgL_tpz00_3484);
						return BUNSPEC;
					}
				else
					{	/* Cfa/closure.scm 542 */
						BgL_appz00_bglt BgL_i1192z00_3488;

						BgL_i1192z00_3488 =
							((BgL_appz00_bglt) CAR(((obj_t) BgL_allocsz00_3485)));
						{	/* Cfa/closure.scm 544 */
							bool_t BgL_test2395z00_6192;

							{
								BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_6193;

								{
									obj_t BgL_auxz00_6194;

									{	/* Cfa/closure.scm 544 */
										BgL_objectz00_bglt BgL_tmpz00_6195;

										BgL_tmpz00_6195 = ((BgL_objectz00_bglt) BgL_i1192z00_3488);
										BgL_auxz00_6194 = BGL_OBJECT_WIDENING(BgL_tmpz00_6195);
									}
									BgL_auxz00_6193 =
										((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_6194);
								}
								BgL_test2395z00_6192 =
									(((BgL_makezd2procedurezd2appz00_bglt)
										COBJECT(BgL_auxz00_6193))->BgL_xz00);
							}
							if (BgL_test2395z00_6192)
								{	/* Cfa/closure.scm 545 */
									obj_t BgL_arg1992z00_3490;
									obj_t BgL_arg1993z00_3491;

									{	/* Cfa/closure.scm 545 */
										BgL_variablez00_bglt BgL_arg1994z00_3492;

										{
											BgL_varz00_bglt BgL_auxz00_6200;

											{	/* Cfa/closure.scm 545 */
												obj_t BgL_pairz00_4753;

												BgL_pairz00_4753 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt) BgL_i1192z00_3488)))->
													BgL_argsz00);
												BgL_auxz00_6200 =
													((BgL_varz00_bglt) CAR(BgL_pairz00_4753));
											}
											BgL_arg1994z00_3492 =
												(((BgL_varz00_bglt) COBJECT(BgL_auxz00_6200))->
												BgL_variablez00);
										}
										BgL_arg1992z00_3490 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_arg1994z00_3492), BgL_xpz00_3483);
									}
									BgL_arg1993z00_3491 = CDR(((obj_t) BgL_allocsz00_3485));
									{
										obj_t BgL_allocsz00_6211;
										obj_t BgL_xpz00_6210;

										BgL_xpz00_6210 = BgL_arg1992z00_3490;
										BgL_allocsz00_6211 = BgL_arg1993z00_3491;
										BgL_allocsz00_3485 = BgL_allocsz00_6211;
										BgL_xpz00_3483 = BgL_xpz00_6210;
										goto BgL_zc3z04anonymousza31989ze3z87_3486;
									}
								}
							else
								{	/* Cfa/closure.scm 546 */
									bool_t BgL_test2396z00_6212;

									{
										BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_6213;

										{
											obj_t BgL_auxz00_6214;

											{	/* Cfa/closure.scm 546 */
												BgL_objectz00_bglt BgL_tmpz00_6215;

												BgL_tmpz00_6215 =
													((BgL_objectz00_bglt) BgL_i1192z00_3488);
												BgL_auxz00_6214 = BGL_OBJECT_WIDENING(BgL_tmpz00_6215);
											}
											BgL_auxz00_6213 =
												((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_6214);
										}
										BgL_test2396z00_6212 =
											(((BgL_makezd2procedurezd2appz00_bglt)
												COBJECT(BgL_auxz00_6213))->BgL_tz00);
									}
									if (BgL_test2396z00_6212)
										{	/* Cfa/closure.scm 547 */
											obj_t BgL_arg1998z00_3496;
											obj_t BgL_arg1999z00_3497;

											{	/* Cfa/closure.scm 547 */
												BgL_variablez00_bglt BgL_arg2000z00_3498;

												{
													BgL_varz00_bglt BgL_auxz00_6220;

													{	/* Cfa/closure.scm 547 */
														obj_t BgL_pairz00_4757;

														BgL_pairz00_4757 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt) BgL_i1192z00_3488)))->
															BgL_argsz00);
														BgL_auxz00_6220 =
															((BgL_varz00_bglt) CAR(BgL_pairz00_4757));
													}
													BgL_arg2000z00_3498 =
														(((BgL_varz00_bglt) COBJECT(BgL_auxz00_6220))->
														BgL_variablez00);
												}
												BgL_arg1998z00_3496 =
													MAKE_YOUNG_PAIR(
													((obj_t) BgL_arg2000z00_3498), BgL_tpz00_3484);
											}
											BgL_arg1999z00_3497 = CDR(((obj_t) BgL_allocsz00_3485));
											{
												obj_t BgL_allocsz00_6231;
												obj_t BgL_tpz00_6230;

												BgL_tpz00_6230 = BgL_arg1998z00_3496;
												BgL_allocsz00_6231 = BgL_arg1999z00_3497;
												BgL_allocsz00_3485 = BgL_allocsz00_6231;
												BgL_tpz00_3484 = BgL_tpz00_6230;
												goto BgL_zc3z04anonymousza31989ze3z87_3486;
											}
										}
									else
										{	/* Cfa/closure.scm 549 */
											obj_t BgL_arg2003z00_3501;

											BgL_arg2003z00_3501 = CDR(((obj_t) BgL_allocsz00_3485));
											{
												obj_t BgL_allocsz00_6234;

												BgL_allocsz00_6234 = BgL_arg2003z00_3501;
												BgL_allocsz00_3485 = BgL_allocsz00_6234;
												goto BgL_zc3z04anonymousza31989ze3z87_3486;
											}
										}
								}
						}
					}
			}
		}

	}



/* show~0 */
	bool_t BGl_showze70ze7zzcfa_closurez00(obj_t BgL_propz00_3503,
		obj_t BgL_lz00_3504)
	{
		{	/* Cfa/closure.scm 533 */
			if (NULLP(BgL_lz00_3504))
				{	/* Cfa/closure.scm 528 */
					return ((bool_t) 0);
				}
			else
				{	/* Cfa/closure.scm 528 */
					{	/* Cfa/closure.scm 530 */
						obj_t BgL_arg2006z00_3507;

						BgL_arg2006z00_3507 =
							BGl_shapez00zztools_shapez00(CAR(BgL_lz00_3504));
						{	/* Cfa/closure.scm 530 */
							obj_t BgL_list2007z00_3508;

							{	/* Cfa/closure.scm 530 */
								obj_t BgL_arg2008z00_3509;

								{	/* Cfa/closure.scm 530 */
									obj_t BgL_arg2009z00_3510;

									{	/* Cfa/closure.scm 530 */
										obj_t BgL_arg2010z00_3511;

										{	/* Cfa/closure.scm 530 */
											obj_t BgL_arg2011z00_3512;

											BgL_arg2011z00_3512 =
												MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
											BgL_arg2010z00_3511 =
												MAKE_YOUNG_PAIR(BgL_arg2006z00_3507,
												BgL_arg2011z00_3512);
										}
										BgL_arg2009z00_3510 =
											MAKE_YOUNG_PAIR(BGl_string2231z00zzcfa_closurez00,
											BgL_arg2010z00_3511);
									}
									BgL_arg2008z00_3509 =
										MAKE_YOUNG_PAIR(BgL_propz00_3503, BgL_arg2009z00_3510);
								}
								BgL_list2007z00_3508 =
									MAKE_YOUNG_PAIR(BGl_string2232z00zzcfa_closurez00,
									BgL_arg2008z00_3509);
							}
							BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list2007z00_3508);
					}}
					{	/* Cfa/closure.scm 531 */
						obj_t BgL_g1554z00_3514;

						BgL_g1554z00_3514 = CDR(BgL_lz00_3504);
						{
							obj_t BgL_l1552z00_3516;

							BgL_l1552z00_3516 = BgL_g1554z00_3514;
						BgL_zc3z04anonymousza32013ze3z87_3517:
							if (PAIRP(BgL_l1552z00_3516))
								{	/* Cfa/closure.scm 533 */
									{	/* Cfa/closure.scm 532 */
										obj_t BgL_xz00_3519;

										BgL_xz00_3519 = CAR(BgL_l1552z00_3516);
										{	/* Cfa/closure.scm 532 */
											obj_t BgL_arg2015z00_3520;

											BgL_arg2015z00_3520 =
												BGl_shapez00zztools_shapez00(BgL_xz00_3519);
											{	/* Cfa/closure.scm 532 */
												obj_t BgL_list2016z00_3521;

												{	/* Cfa/closure.scm 532 */
													obj_t BgL_arg2017z00_3522;

													{	/* Cfa/closure.scm 532 */
														obj_t BgL_arg2018z00_3523;

														BgL_arg2018z00_3523 =
															MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)),
															BNIL);
														BgL_arg2017z00_3522 =
															MAKE_YOUNG_PAIR(BgL_arg2015z00_3520,
															BgL_arg2018z00_3523);
													}
													BgL_list2016z00_3521 =
														MAKE_YOUNG_PAIR(BGl_string2233z00zzcfa_closurez00,
														BgL_arg2017z00_3522);
												}
												BGl_verbosez00zztools_speekz00(BINT(2L),
													BgL_list2016z00_3521);
									}}}
									{
										obj_t BgL_l1552z00_6258;

										BgL_l1552z00_6258 = CDR(BgL_l1552z00_3516);
										BgL_l1552z00_3516 = BgL_l1552z00_6258;
										goto BgL_zc3z04anonymousza32013ze3z87_3517;
									}
								}
							else
								{	/* Cfa/closure.scm 533 */
									return ((bool_t) 1);
								}
						}
					}
				}
		}

	}



/* get-procedure-list */
	BGL_EXPORTED_DEF obj_t BGl_getzd2procedurezd2listz00zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 561 */
			return BGl_za2makezd2procedurezd2listza2z00zzcfa_closurez00;
		}

	}



/* &get-procedure-list */
	obj_t BGl_z62getzd2procedurezd2listz62zzcfa_closurez00(obj_t BgL_envz00_4920)
	{
		{	/* Cfa/closure.scm 561 */
			return BGl_getzd2procedurezd2listz00zzcfa_closurez00();
		}

	}



/* add-funcall! */
	BGL_EXPORTED_DEF obj_t
		BGl_addzd2funcallz12zc0zzcfa_closurez00(BgL_nodez00_bglt BgL_astz00_32)
	{
		{	/* Cfa/closure.scm 567 */
			if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 2L))
				{	/* Cfa/closure.scm 568 */
					return (BGl_za2funcallzd2listza2zd2zzcfa_closurez00 =
						MAKE_YOUNG_PAIR(
							((obj_t) BgL_astz00_32),
							BGl_za2funcallzd2listza2zd2zzcfa_closurez00), BUNSPEC);
				}
			else
				{	/* Cfa/closure.scm 568 */
					return BFALSE;
				}
		}

	}



/* &add-funcall! */
	obj_t BGl_z62addzd2funcallz12za2zzcfa_closurez00(obj_t BgL_envz00_4921,
		obj_t BgL_astz00_4922)
	{
		{	/* Cfa/closure.scm 567 */
			return
				BGl_addzd2funcallz12zc0zzcfa_closurez00(
				((BgL_nodez00_bglt) BgL_astz00_4922));
		}

	}



/* add-make-procedure! */
	BGL_EXPORTED_DEF obj_t
		BGl_addzd2makezd2procedurez12z12zzcfa_closurez00(BgL_nodez00_bglt
		BgL_astz00_33)
	{
		{	/* Cfa/closure.scm 574 */
			return (BGl_za2makezd2procedurezd2listza2z00zzcfa_closurez00 =
				MAKE_YOUNG_PAIR(
					((obj_t) BgL_astz00_33),
					BGl_za2makezd2procedurezd2listza2z00zzcfa_closurez00), BUNSPEC);
		}

	}



/* &add-make-procedure! */
	obj_t BGl_z62addzd2makezd2procedurez12z70zzcfa_closurez00(obj_t
		BgL_envz00_4923, obj_t BgL_astz00_4924)
	{
		{	/* Cfa/closure.scm 574 */
			return
				BGl_addzd2makezd2procedurez12z12zzcfa_closurez00(
				((BgL_nodez00_bglt) BgL_astz00_4924));
		}

	}



/* add-procedure-ref! */
	BGL_EXPORTED_DEF obj_t
		BGl_addzd2procedurezd2refz12z12zzcfa_closurez00(BgL_nodez00_bglt
		BgL_astz00_34)
	{
		{	/* Cfa/closure.scm 580 */
			if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 2L))
				{	/* Cfa/closure.scm 581 */
					return (BGl_za2procedurezd2refzd2listza2z00zzcfa_closurez00 =
						MAKE_YOUNG_PAIR(
							((obj_t) BgL_astz00_34),
							BGl_za2procedurezd2refzd2listza2z00zzcfa_closurez00), BUNSPEC);
				}
			else
				{	/* Cfa/closure.scm 581 */
					return BFALSE;
				}
		}

	}



/* &add-procedure-ref! */
	obj_t BGl_z62addzd2procedurezd2refz12z70zzcfa_closurez00(obj_t
		BgL_envz00_4925, obj_t BgL_astz00_4926)
	{
		{	/* Cfa/closure.scm 580 */
			return
				BGl_addzd2procedurezd2refz12z12zzcfa_closurez00(
				((BgL_nodez00_bglt) BgL_astz00_4926));
		}

	}



/* approx-procedure-el? */
	BGL_EXPORTED_DEF bool_t
		BGl_approxzd2procedurezd2elzf3zf3zzcfa_closurez00(BgL_approxz00_bglt
		BgL_approxz00_35)
	{
		{	/* Cfa/closure.scm 587 */
			{	/* Cfa/closure.scm 588 */
				bool_t BgL_test2401z00_6279;

				{	/* Cfa/closure.scm 588 */
					BgL_typez00_bglt BgL_arg2031z00_3541;

					BgL_arg2031z00_3541 =
						(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_35))->BgL_typez00);
					BgL_test2401z00_6279 =
						(
						((obj_t) BgL_arg2031z00_3541) ==
						BGl_za2procedureza2z00zztype_cachez00);
				}
				if (BgL_test2401z00_6279)
					{	/* Cfa/closure.scm 589 */
						bool_t BgL_test2402z00_6283;

						{	/* Cfa/closure.scm 589 */
							obj_t BgL_arg2029z00_3539;

							BgL_arg2029z00_3539 =
								BGl_setzd2lengthzd2zzcfa_setz00(
								(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_35))->
									BgL_allocsz00));
							BgL_test2402z00_6283 = ((long) CINT(BgL_arg2029z00_3539) == 1L);
						}
						if (BgL_test2402z00_6283)
							{	/* Cfa/closure.scm 590 */
								obj_t BgL_allocz00_3534;

								{	/* Cfa/closure.scm 590 */
									obj_t BgL_arg2027z00_3536;

									BgL_arg2027z00_3536 =
										(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_35))->
										BgL_allocsz00);
									{	/* Cfa/closure.scm 590 */

										BgL_allocz00_3534 =
											BGl_setzd2headzd2zzcfa_setz00(BgL_arg2027z00_3536,
											BFALSE);
									}
								}
								{	/* Cfa/closure.scm 591 */
									bool_t BgL_test2403z00_6290;

									{	/* Cfa/closure.scm 591 */
										obj_t BgL_classz00_4767;

										BgL_classz00_4767 =
											BGl_makezd2procedurezd2appz00zzcfa_info2z00;
										if (BGL_OBJECTP(BgL_allocz00_3534))
											{	/* Cfa/closure.scm 591 */
												BgL_objectz00_bglt BgL_arg1807z00_4769;

												BgL_arg1807z00_4769 =
													(BgL_objectz00_bglt) (BgL_allocz00_3534);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Cfa/closure.scm 591 */
														long BgL_idxz00_4775;

														BgL_idxz00_4775 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4769);
														BgL_test2403z00_6290 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_4775 + 4L)) == BgL_classz00_4767);
													}
												else
													{	/* Cfa/closure.scm 591 */
														bool_t BgL_res2224z00_4800;

														{	/* Cfa/closure.scm 591 */
															obj_t BgL_oclassz00_4783;

															{	/* Cfa/closure.scm 591 */
																obj_t BgL_arg1815z00_4791;
																long BgL_arg1816z00_4792;

																BgL_arg1815z00_4791 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Cfa/closure.scm 591 */
																	long BgL_arg1817z00_4793;

																	BgL_arg1817z00_4793 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4769);
																	BgL_arg1816z00_4792 =
																		(BgL_arg1817z00_4793 - OBJECT_TYPE);
																}
																BgL_oclassz00_4783 =
																	VECTOR_REF(BgL_arg1815z00_4791,
																	BgL_arg1816z00_4792);
															}
															{	/* Cfa/closure.scm 591 */
																bool_t BgL__ortest_1115z00_4784;

																BgL__ortest_1115z00_4784 =
																	(BgL_classz00_4767 == BgL_oclassz00_4783);
																if (BgL__ortest_1115z00_4784)
																	{	/* Cfa/closure.scm 591 */
																		BgL_res2224z00_4800 =
																			BgL__ortest_1115z00_4784;
																	}
																else
																	{	/* Cfa/closure.scm 591 */
																		long BgL_odepthz00_4785;

																		{	/* Cfa/closure.scm 591 */
																			obj_t BgL_arg1804z00_4786;

																			BgL_arg1804z00_4786 =
																				(BgL_oclassz00_4783);
																			BgL_odepthz00_4785 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_4786);
																		}
																		if ((4L < BgL_odepthz00_4785))
																			{	/* Cfa/closure.scm 591 */
																				obj_t BgL_arg1802z00_4788;

																				{	/* Cfa/closure.scm 591 */
																					obj_t BgL_arg1803z00_4789;

																					BgL_arg1803z00_4789 =
																						(BgL_oclassz00_4783);
																					BgL_arg1802z00_4788 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_4789, 4L);
																				}
																				BgL_res2224z00_4800 =
																					(BgL_arg1802z00_4788 ==
																					BgL_classz00_4767);
																			}
																		else
																			{	/* Cfa/closure.scm 591 */
																				BgL_res2224z00_4800 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2403z00_6290 = BgL_res2224z00_4800;
													}
											}
										else
											{	/* Cfa/closure.scm 591 */
												BgL_test2403z00_6290 = ((bool_t) 0);
											}
									}
									if (BgL_test2403z00_6290)
										{
											BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_6313;

											{
												obj_t BgL_auxz00_6314;

												{	/* Cfa/closure.scm 592 */
													BgL_objectz00_bglt BgL_tmpz00_6315;

													BgL_tmpz00_6315 =
														((BgL_objectz00_bglt)
														((BgL_appz00_bglt) BgL_allocz00_3534));
													BgL_auxz00_6314 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_6315);
												}
												BgL_auxz00_6313 =
													((BgL_makezd2procedurezd2appz00_bglt)
													BgL_auxz00_6314);
											}
											return
												(((BgL_makezd2procedurezd2appz00_bglt)
													COBJECT(BgL_auxz00_6313))->BgL_xz00);
										}
									else
										{	/* Cfa/closure.scm 591 */
											return ((bool_t) 0);
										}
								}
							}
						else
							{	/* Cfa/closure.scm 589 */
								return ((bool_t) 0);
							}
					}
				else
					{	/* Cfa/closure.scm 588 */
						return ((bool_t) 0);
					}
			}
		}

	}



/* &approx-procedure-el? */
	obj_t BGl_z62approxzd2procedurezd2elzf3z91zzcfa_closurez00(obj_t
		BgL_envz00_4927, obj_t BgL_approxz00_4928)
	{
		{	/* Cfa/closure.scm 587 */
			return
				BBOOL(BGl_approxzd2procedurezd2elzf3zf3zzcfa_closurez00(
					((BgL_approxz00_bglt) BgL_approxz00_4928)));
		}

	}



/* start-closure-cache */
	obj_t BGl_startzd2closurezd2cachez00zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 609 */
			BGl_za2procedurezd2refza2zd2zzcfa_closurez00 =
				BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(11),
				CNST_TABLE_REF(12));
			BGl_za2procedurezd2setz12za2zc0zzcfa_closurez00 =
				BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(13),
				CNST_TABLE_REF(12));
			BGl_za2procedurezd2lzd2refza2z00zzcfa_closurez00 =
				BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(14),
				CNST_TABLE_REF(12));
			BGl_za2procedurezd2lzd2setz12za2z12zzcfa_closurez00 =
				BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(15),
				CNST_TABLE_REF(12));
			BGl_za2procedurezd2elzd2refza2z00zzcfa_closurez00 =
				BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(16),
				CNST_TABLE_REF(12));
			BGl_za2procedurezd2elzd2setz12za2z12zzcfa_closurez00 =
				BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(17),
				CNST_TABLE_REF(12));
			BGl_za2makezd2elzd2procedureza2z00zzcfa_closurez00 =
				BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(18),
				CNST_TABLE_REF(12));
			return (BGl_za2makezd2lzd2procedureza2z00zzcfa_closurez00 =
				BGl_getzd2globalzf2modulez20zzast_envz00(CNST_TABLE_REF(19),
					CNST_TABLE_REF(12)), BUNSPEC);
		}

	}



/* stop-closure-cache */
	BGL_EXPORTED_DEF obj_t BGl_stopzd2closurezd2cachez00zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 622 */
			BGl_za2procedurezd2refza2zd2zzcfa_closurez00 = BFALSE;
			BGl_za2procedurezd2setz12za2zc0zzcfa_closurez00 = BFALSE;
			BGl_za2procedurezd2lzd2refza2z00zzcfa_closurez00 = BFALSE;
			BGl_za2procedurezd2lzd2setz12za2z12zzcfa_closurez00 = BFALSE;
			BGl_za2procedurezd2elzd2refza2z00zzcfa_closurez00 = BFALSE;
			BGl_za2procedurezd2elzd2setz12za2z12zzcfa_closurez00 = BFALSE;
			BGl_za2makezd2elzd2procedureza2z00zzcfa_closurez00 = BFALSE;
			return (BGl_za2makezd2lzd2procedureza2z00zzcfa_closurez00 =
				BFALSE, BUNSPEC);
		}

	}



/* &stop-closure-cache */
	obj_t BGl_z62stopzd2closurezd2cachez62zzcfa_closurez00(obj_t BgL_envz00_4929)
	{
		{	/* Cfa/closure.scm 622 */
			return BGl_stopzd2closurezd2cachez00zzcfa_closurez00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 25 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 25 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 25 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_closurez00(void)
	{
		{	/* Cfa/closure.scm 25 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
			BGl_modulezd2initializa7ationz75zzcfa_cfaz00(400853817L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
			BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
			BGl_modulezd2initializa7ationz75zzcfa_setz00(5932721L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
			BGl_modulezd2initializa7ationz75zzcfa_ltypez00(315872512L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_typez00(93933605L,
				BSTRING_TO_STRING(BGl_string2234z00zzcfa_closurez00));
		}

	}

#ifdef __cplusplus
}
#endif
