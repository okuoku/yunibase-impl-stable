/*===========================================================================*/
/*   (Cfa/iterate.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/iterate.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_ITERATE_TYPE_DEFINITIONS
#define BGL_CFA_ITERATE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_internzd2sfunzf2cinfoz20_bgl
	{
		bool_t BgL_polymorphiczf3zf3;
		struct BgL_approxz00_bgl *BgL_approxz00;
		long BgL_stampz00;
	}                               *BgL_internzd2sfunzf2cinfoz20_bglt;

	typedef struct BgL_svarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_clozd2envzf3z21;
		long BgL_stampz00;
	}                      *BgL_svarzf2cinfozf2_bglt;


#endif													// BGL_CFA_ITERATE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62zc3z04anonymousza31595ze3ze5zzcfa_iteratez00(obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	BGL_EXPORTED_DECL obj_t BGl_continuezd2cfaz12zc0zzcfa_iteratez00(obj_t);
	extern obj_t BGl_unitzd2initializa7ersz75zzast_unitz00(void);
	BGL_IMPORT obj_t BGl_timez00zz__biglooz00(obj_t);
	static obj_t BGl_objectzd2initzd2zzcfa_iteratez00(void);
	static obj_t BGl_za2cfazd2currentza2zd2zzcfa_iteratez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_cfazd2currentzd2zzcfa_iteratez00(void);
	static obj_t BGl_appendzd221011zd2zzcfa_iteratez00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcfa_iteratez00(void);
	static obj_t BGl_z62cfazd2iteratez12za2zzcfa_iteratez00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfazd2internzd2sfunz12z70zzcfa_iteratez00(obj_t, obj_t, obj_t);
	extern BgL_typez00_bglt
		BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_typez00_bglt);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t
		BGl_cfazd2variablezd2valuezd2approxzd2zzcfa_cfaz00(BgL_valuez00_bglt);
	static obj_t BGl_z62cfazd2currentzb0zzcfa_iteratez00(obj_t);
	extern BgL_approxz00_bglt
		BGl_unionzd2approxzd2filterz12z12zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_cfazd2iteratezd2tozd2fixpointz12zc0zzcfa_iteratez00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_iteratez00(void);
	extern obj_t BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
	static obj_t BGl_z62continuezd2cfaz12za2zzcfa_iteratez00(obj_t, obj_t);
	extern obj_t BGl_svarzf2Cinfozf2zzcfa_infoz00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_EXPORTED_DEF obj_t BGl_za2cfazd2stampza2zd2zzcfa_iteratez00 = BUNSPEC;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t BGl_z62cfazd2exportzd2varz121503z70zzcfa_iteratez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_za2cfazd2continuezf3za2z21zzcfa_iteratez00 = BUNSPEC;
	BGL_EXPORTED_DECL obj_t
		BGl_cfazd2exportzd2varz12z12zzcfa_iteratez00(BgL_valuez00_bglt, obj_t);
	extern obj_t BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_approxz00_bglt);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_iteratez00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzcfa_iteratez00(void);
	BGL_IMPORT obj_t BGl_formatz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_cfazd2internzd2sfunz12z12zzcfa_iteratez00(BgL_sfunz00_bglt, obj_t);
	static obj_t BGl_genericzd2initzd2zzcfa_iteratez00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	static obj_t BGl_z62cfazd2exportzd2varz12zd2inte1508za2zzcfa_iteratez00(obj_t,
		obj_t, obj_t);
	extern BgL_approxz00_bglt BGl_loosez12z12zzcfa_loosez00(BgL_approxz00_bglt,
		obj_t);
	static obj_t BGl_z62cfazd2iteratezd2tozd2fixpointz12za2zzcfa_iteratez00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62polymorphicz62zzcfa_iteratez00(BgL_sfunz00_bglt, BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_iteratez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_loosez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_cfaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_unitz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_z62cfazd2exportzd2varz12zd2svar1506za2zzcfa_iteratez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzcfa_iteratez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_iteratez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_iteratez00(void);
	extern obj_t BGl_valuez00zzast_varz00;
	extern BgL_approxz00_bglt BGl_cfaz12z12zzcfa_cfaz00(BgL_nodez00_bglt);
	static bool_t BGl_exportedzd2closurezf3z21zzcfa_iteratez00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_cfazd2iteratez12zc0zzcfa_iteratez00(obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_z62cfazd2exportzd2varz12z70zzcfa_iteratez00(obj_t, obj_t,
		obj_t);
	static obj_t __cnst[4];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfazd2iteratezd2tozd2fixpointz12zd2envz12zzcfa_iteratez00,
		BgL_bgl_za762cfaza7d2iterate1980z00,
		BGl_z62cfazd2iteratezd2tozd2fixpointz12za2zzcfa_iteratez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cfazd2internzd2sfunz12zd2envzc0zzcfa_iteratez00,
		BgL_bgl_za762cfaza7d2internza71981za7,
		BGl_z62cfazd2internzd2sfunz12z70zzcfa_iteratez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfazd2currentzd2envz00zzcfa_iteratez00,
		BgL_bgl_za762cfaza7d2current1982z00,
		BGl_z62cfazd2currentzb0zzcfa_iteratez00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1971z00zzcfa_iteratez00,
		BgL_bgl_za762cfaza7d2exportza71983za7,
		BGl_z62cfazd2exportzd2varz121503z70zzcfa_iteratez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1974z00zzcfa_iteratez00,
		BgL_bgl_za762cfaza7d2exportza71984za7,
		BGl_z62cfazd2exportzd2varz12zd2svar1506za2zzcfa_iteratez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1976z00zzcfa_iteratez00,
		BgL_bgl_za762cfaza7d2exportza71985za7,
		BGl_z62cfazd2exportzd2varz12zd2inte1508za2zzcfa_iteratez00, 0L, BUNSPEC, 2);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_continuezd2cfaz12zd2envz12zzcfa_iteratez00,
		BgL_bgl_za762continueza7d2cf1986z00,
		BGl_z62continuezd2cfaz12za2zzcfa_iteratez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1962z00zzcfa_iteratez00,
		BgL_bgl_string1962za700za7za7c1987za7, "      ", 6);
	      DEFINE_STRING(BGl_string1963z00zzcfa_iteratez00,
		BgL_bgl_string1963za700za7za7c1988za7, ".", 1);
	      DEFINE_STRING(BGl_string1964z00zzcfa_iteratez00,
		BgL_bgl_string1964za700za7za7c1989za7, "\n", 1);
	      DEFINE_STRING(BGl_string1965z00zzcfa_iteratez00,
		BgL_bgl_string1965za700za7za7c1990za7, " [", 2);
	      DEFINE_STRING(BGl_string1966z00zzcfa_iteratez00,
		BgL_bgl_string1966za700za7za7c1991za7, "]", 1);
	      DEFINE_STRING(BGl_string1967z00zzcfa_iteratez00,
		BgL_bgl_string1967za700za7za7c1992za7, "> ", 2);
	      DEFINE_STRING(BGl_string1968z00zzcfa_iteratez00,
		BgL_bgl_string1968za700za7za7c1993za7, "<", 1);
	      DEFINE_STRING(BGl_string1969z00zzcfa_iteratez00,
		BgL_bgl_string1969za700za7za7c1994za7, " ", 1);
	      DEFINE_STRING(BGl_string1970z00zzcfa_iteratez00,
		BgL_bgl_string1970za700za7za7c1995za7, "~a[~a]", 6);
	      DEFINE_STRING(BGl_string1972z00zzcfa_iteratez00,
		BgL_bgl_string1972za700za7za7c1996za7, "cfa-export-var!1503", 19);
	      DEFINE_STRING(BGl_string1973z00zzcfa_iteratez00,
		BgL_bgl_string1973za700za7za7c1997za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string1975z00zzcfa_iteratez00,
		BgL_bgl_string1975za700za7za7c1998za7, "cfa-export-var!", 15);
	      DEFINE_STRING(BGl_string1977z00zzcfa_iteratez00,
		BgL_bgl_string1977za700za7za7c1999za7, "cfa_iterate", 11);
	      DEFINE_STRING(BGl_string1978z00zzcfa_iteratez00,
		BgL_bgl_string1978za700za7za7c2000za7,
		"all cfa-export-var!1503 never export ", 37);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfazd2iteratez12zd2envz12zzcfa_iteratez00,
		BgL_bgl_za762cfaza7d2iterate2001z00,
		BGl_z62cfazd2iteratez12za2zzcfa_iteratez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_cfazd2exportzd2varz12zd2envzc0zzcfa_iteratez00,
		BgL_bgl_za762cfaza7d2exportza72002za7,
		BGl_z62cfazd2exportzd2varz12z70zzcfa_iteratez00, 0L, BUNSPEC, 2);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2cfazd2currentza2zd2zzcfa_iteratez00));
		     ADD_ROOT((void *) (&BGl_za2cfazd2stampza2zd2zzcfa_iteratez00));
		     ADD_ROOT((void *) (&BGl_za2cfazd2continuezf3za2z21zzcfa_iteratez00));
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_iteratez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_iteratez00(long
		BgL_checksumz00_3910, char *BgL_fromz00_3911)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_iteratez00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_iteratez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_iteratez00();
					BGl_libraryzd2moduleszd2initz00zzcfa_iteratez00();
					BGl_cnstzd2initzd2zzcfa_iteratez00();
					BGl_importedzd2moduleszd2initz00zzcfa_iteratez00();
					BGl_genericzd2initzd2zzcfa_iteratez00();
					BGl_methodzd2initzd2zzcfa_iteratez00();
					return BGl_toplevelzd2initzd2zzcfa_iteratez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_iteratez00(void)
	{
		{	/* Cfa/iterate.scm 15 */
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cfa_iterate");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cfa_iterate");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_iterate");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"cfa_iterate");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_iterate");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_iterate");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_iterate");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cfa_iterate");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"cfa_iterate");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cfa_iterate");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_iterate");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_iterate");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_iterate");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_iteratez00(void)
	{
		{	/* Cfa/iterate.scm 15 */
			{	/* Cfa/iterate.scm 15 */
				obj_t BgL_cportz00_3855;

				{	/* Cfa/iterate.scm 15 */
					obj_t BgL_stringz00_3862;

					BgL_stringz00_3862 = BGl_string1978z00zzcfa_iteratez00;
					{	/* Cfa/iterate.scm 15 */
						obj_t BgL_startz00_3863;

						BgL_startz00_3863 = BINT(0L);
						{	/* Cfa/iterate.scm 15 */
							obj_t BgL_endz00_3864;

							BgL_endz00_3864 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3862)));
							{	/* Cfa/iterate.scm 15 */

								BgL_cportz00_3855 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3862, BgL_startz00_3863, BgL_endz00_3864);
				}}}}
				{
					long BgL_iz00_3856;

					BgL_iz00_3856 = 3L;
				BgL_loopz00_3857:
					if ((BgL_iz00_3856 == -1L))
						{	/* Cfa/iterate.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/iterate.scm 15 */
							{	/* Cfa/iterate.scm 15 */
								obj_t BgL_arg1979z00_3858;

								{	/* Cfa/iterate.scm 15 */

									{	/* Cfa/iterate.scm 15 */
										obj_t BgL_locationz00_3860;

										BgL_locationz00_3860 = BBOOL(((bool_t) 0));
										{	/* Cfa/iterate.scm 15 */

											BgL_arg1979z00_3858 =
												BGl_readz00zz__readerz00(BgL_cportz00_3855,
												BgL_locationz00_3860);
										}
									}
								}
								{	/* Cfa/iterate.scm 15 */
									int BgL_tmpz00_3944;

									BgL_tmpz00_3944 = (int) (BgL_iz00_3856);
									CNST_TABLE_SET(BgL_tmpz00_3944, BgL_arg1979z00_3858);
							}}
							{	/* Cfa/iterate.scm 15 */
								int BgL_auxz00_3861;

								BgL_auxz00_3861 = (int) ((BgL_iz00_3856 - 1L));
								{
									long BgL_iz00_3949;

									BgL_iz00_3949 = (long) (BgL_auxz00_3861);
									BgL_iz00_3856 = BgL_iz00_3949;
									goto BgL_loopz00_3857;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_iteratez00(void)
	{
		{	/* Cfa/iterate.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_iteratez00(void)
	{
		{	/* Cfa/iterate.scm 15 */
			BGl_za2cfazd2continuezf3za2z21zzcfa_iteratez00 = BUNSPEC;
			BGl_za2cfazd2stampza2zd2zzcfa_iteratez00 = BINT(-1L);
			return (BGl_za2cfazd2currentza2zd2zzcfa_iteratez00 = BUNSPEC, BUNSPEC);
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzcfa_iteratez00(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_2972;

				BgL_headz00_2972 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_2973;
					obj_t BgL_tailz00_2974;

					BgL_prevz00_2973 = BgL_headz00_2972;
					BgL_tailz00_2974 = BgL_l1z00_1;
				BgL_loopz00_2975:
					if (PAIRP(BgL_tailz00_2974))
						{
							obj_t BgL_newzd2prevzd2_2977;

							BgL_newzd2prevzd2_2977 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_2974), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_2973, BgL_newzd2prevzd2_2977);
							{
								obj_t BgL_tailz00_3960;
								obj_t BgL_prevz00_3959;

								BgL_prevz00_3959 = BgL_newzd2prevzd2_2977;
								BgL_tailz00_3960 = CDR(BgL_tailz00_2974);
								BgL_tailz00_2974 = BgL_tailz00_3960;
								BgL_prevz00_2973 = BgL_prevz00_3959;
								goto BgL_loopz00_2975;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_2972);
				}
			}
		}

	}



/* cfa-iterate-to-fixpoint! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfazd2iteratezd2tozd2fixpointz12zc0zzcfa_iteratez00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Cfa/iterate.scm 40 */
			BGl_za2cfazd2stampza2zd2zzcfa_iteratez00 = BINT(-1L);
			{	/* Cfa/iterate.scm 46 */
				obj_t BgL_glodefsz00_2980;

				BgL_glodefsz00_2980 = BNIL;
				{
					obj_t BgL_l1496z00_2982;

					BgL_l1496z00_2982 = BgL_globalsz00_3;
				BgL_zc3z04anonymousza31515ze3z87_2983:
					if (PAIRP(BgL_l1496z00_2982))
						{	/* Cfa/iterate.scm 47 */
							{	/* Cfa/iterate.scm 48 */
								obj_t BgL_gz00_2985;

								BgL_gz00_2985 = CAR(BgL_l1496z00_2982);
								{	/* Cfa/iterate.scm 48 */
									bool_t BgL_test2007z00_3967;

									if (
										((((BgL_globalz00_bglt) COBJECT(
														((BgL_globalz00_bglt) BgL_gz00_2985)))->
												BgL_importz00) == CNST_TABLE_REF(0)))
										{	/* Cfa/iterate.scm 48 */
											BgL_test2007z00_3967 = ((bool_t) 1);
										}
									else
										{	/* Cfa/iterate.scm 48 */
											BgL_test2007z00_3967 =
												BGl_exportedzd2closurezf3z21zzcfa_iteratez00
												(BgL_gz00_2985);
										}
									if (BgL_test2007z00_3967)
										{	/* Cfa/iterate.scm 48 */
											BgL_glodefsz00_2980 =
												MAKE_YOUNG_PAIR(BgL_gz00_2985, BgL_glodefsz00_2980);
										}
									else
										{	/* Cfa/iterate.scm 48 */
											BFALSE;
										}
								}
							}
							{
								obj_t BgL_l1496z00_3975;

								BgL_l1496z00_3975 = CDR(BgL_l1496z00_2982);
								BgL_l1496z00_2982 = BgL_l1496z00_3975;
								goto BgL_zc3z04anonymousza31515ze3z87_2983;
							}
						}
					else
						{	/* Cfa/iterate.scm 47 */
							((bool_t) 1);
						}
				}
				{	/* Cfa/iterate.scm 53 */
					obj_t BgL_arg1552z00_2994;

					BgL_arg1552z00_2994 = BGl_unitzd2initializa7ersz75zzast_unitz00();
					BgL_glodefsz00_2980 =
						BGl_appendzd221011zd2zzcfa_iteratez00(BgL_arg1552z00_2994,
						BgL_glodefsz00_2980);
				}
				if (CBOOL(BGl_za2cfazd2continuezf3za2z21zzcfa_iteratez00))
					{	/* Cfa/iterate.scm 199 */
						BFALSE;
					}
				else
					{	/* Cfa/iterate.scm 199 */
						BNIL;
					}
				BGl_za2cfazd2continuezf3za2z21zzcfa_iteratez00 = BTRUE;
				{	/* Cfa/iterate.scm 57 */
					obj_t BgL_list1553z00_2995;

					BgL_list1553z00_2995 =
						MAKE_YOUNG_PAIR(BGl_string1962z00zzcfa_iteratez00, BNIL);
					BGl_verbosez00zztools_speekz00(BINT(3L), BgL_list1553z00_2995);
				}
				{
					long BgL_iz00_2997;

					BgL_iz00_2997 = 0L;
				BgL_zc3z04anonymousza31554ze3z87_2998:
					{	/* Cfa/iterate.scm 59 */
						obj_t BgL_list1555z00_2999;

						{	/* Cfa/iterate.scm 59 */
							obj_t BgL_arg1559z00_3000;

							BgL_arg1559z00_3000 = MAKE_YOUNG_PAIR(BINT(BgL_iz00_2997), BNIL);
							BgL_list1555z00_2999 =
								MAKE_YOUNG_PAIR(BGl_string1963z00zzcfa_iteratez00,
								BgL_arg1559z00_3000);
						}
						BGl_verbosez00zztools_speekz00(BINT(3L), BgL_list1555z00_2999);
					}
					if (CBOOL(BGl_za2cfazd2continuezf3za2z21zzcfa_iteratez00))
						{	/* Cfa/iterate.scm 60 */
							BGl_cfazd2iteratez12zc0zzcfa_iteratez00(BgL_glodefsz00_2980);
							{
								long BgL_iz00_3992;

								BgL_iz00_3992 = (BgL_iz00_2997 + 1L);
								BgL_iz00_2997 = BgL_iz00_3992;
								goto BgL_zc3z04anonymousza31554ze3z87_2998;
							}
						}
					else
						{	/* Cfa/iterate.scm 60 */
							BgL_glodefsz00_2980;
						}
				}
				{	/* Cfa/iterate.scm 65 */
					obj_t BgL_list1565z00_3004;

					BgL_list1565z00_3004 =
						MAKE_YOUNG_PAIR(BGl_string1964z00zzcfa_iteratez00, BNIL);
					return BGl_verbosez00zztools_speekz00(BINT(3L), BgL_list1565z00_3004);
				}
			}
		}

	}



/* &cfa-iterate-to-fixpoint! */
	obj_t BGl_z62cfazd2iteratezd2tozd2fixpointz12za2zzcfa_iteratez00(obj_t
		BgL_envz00_3823, obj_t BgL_globalsz00_3824)
	{
		{	/* Cfa/iterate.scm 40 */
			return
				BGl_cfazd2iteratezd2tozd2fixpointz12zc0zzcfa_iteratez00
				(BgL_globalsz00_3824);
		}

	}



/* exported-closure? */
	bool_t BGl_exportedzd2closurezf3z21zzcfa_iteratez00(obj_t BgL_gz00_4)
	{
		{	/* Cfa/iterate.scm 70 */
			if (
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_gz00_4))))->BgL_removablez00) ==
					CNST_TABLE_REF(1)))
				{	/* Cfa/iterate.scm 72 */
					BgL_valuez00_bglt BgL_valz00_3006;

					BgL_valz00_3006 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_gz00_4))))->BgL_valuez00);
					{	/* Cfa/iterate.scm 73 */
						bool_t BgL_test2012z00_4007;

						{	/* Cfa/iterate.scm 73 */
							obj_t BgL_classz00_3582;

							BgL_classz00_3582 = BGl_sfunz00zzast_varz00;
							{	/* Cfa/iterate.scm 73 */
								BgL_objectz00_bglt BgL_arg1807z00_3584;

								{	/* Cfa/iterate.scm 73 */
									obj_t BgL_tmpz00_4008;

									BgL_tmpz00_4008 =
										((obj_t) ((BgL_objectz00_bglt) BgL_valz00_3006));
									BgL_arg1807z00_3584 = (BgL_objectz00_bglt) (BgL_tmpz00_4008);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/iterate.scm 73 */
										long BgL_idxz00_3590;

										BgL_idxz00_3590 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3584);
										BgL_test2012z00_4007 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3590 + 3L)) == BgL_classz00_3582);
									}
								else
									{	/* Cfa/iterate.scm 73 */
										bool_t BgL_res1952z00_3615;

										{	/* Cfa/iterate.scm 73 */
											obj_t BgL_oclassz00_3598;

											{	/* Cfa/iterate.scm 73 */
												obj_t BgL_arg1815z00_3606;
												long BgL_arg1816z00_3607;

												BgL_arg1815z00_3606 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/iterate.scm 73 */
													long BgL_arg1817z00_3608;

													BgL_arg1817z00_3608 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3584);
													BgL_arg1816z00_3607 =
														(BgL_arg1817z00_3608 - OBJECT_TYPE);
												}
												BgL_oclassz00_3598 =
													VECTOR_REF(BgL_arg1815z00_3606, BgL_arg1816z00_3607);
											}
											{	/* Cfa/iterate.scm 73 */
												bool_t BgL__ortest_1115z00_3599;

												BgL__ortest_1115z00_3599 =
													(BgL_classz00_3582 == BgL_oclassz00_3598);
												if (BgL__ortest_1115z00_3599)
													{	/* Cfa/iterate.scm 73 */
														BgL_res1952z00_3615 = BgL__ortest_1115z00_3599;
													}
												else
													{	/* Cfa/iterate.scm 73 */
														long BgL_odepthz00_3600;

														{	/* Cfa/iterate.scm 73 */
															obj_t BgL_arg1804z00_3601;

															BgL_arg1804z00_3601 = (BgL_oclassz00_3598);
															BgL_odepthz00_3600 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3601);
														}
														if ((3L < BgL_odepthz00_3600))
															{	/* Cfa/iterate.scm 73 */
																obj_t BgL_arg1802z00_3603;

																{	/* Cfa/iterate.scm 73 */
																	obj_t BgL_arg1803z00_3604;

																	BgL_arg1803z00_3604 = (BgL_oclassz00_3598);
																	BgL_arg1802z00_3603 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3604,
																		3L);
																}
																BgL_res1952z00_3615 =
																	(BgL_arg1802z00_3603 == BgL_classz00_3582);
															}
														else
															{	/* Cfa/iterate.scm 73 */
																BgL_res1952z00_3615 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2012z00_4007 = BgL_res1952z00_3615;
									}
							}
						}
						if (BgL_test2012z00_4007)
							{	/* Cfa/iterate.scm 74 */
								bool_t BgL_test2016z00_4031;

								{	/* Cfa/iterate.scm 74 */
									obj_t BgL_arg1575z00_3011;

									BgL_arg1575z00_3011 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_valz00_3006)))->
										BgL_thezd2closurezd2globalz00);
									{	/* Cfa/iterate.scm 74 */
										obj_t BgL_classz00_3617;

										BgL_classz00_3617 = BGl_globalz00zzast_varz00;
										if (BGL_OBJECTP(BgL_arg1575z00_3011))
											{	/* Cfa/iterate.scm 74 */
												BgL_objectz00_bglt BgL_arg1807z00_3619;

												BgL_arg1807z00_3619 =
													(BgL_objectz00_bglt) (BgL_arg1575z00_3011);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Cfa/iterate.scm 74 */
														long BgL_idxz00_3625;

														BgL_idxz00_3625 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3619);
														BgL_test2016z00_4031 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_3625 + 2L)) == BgL_classz00_3617);
													}
												else
													{	/* Cfa/iterate.scm 74 */
														bool_t BgL_res1953z00_3650;

														{	/* Cfa/iterate.scm 74 */
															obj_t BgL_oclassz00_3633;

															{	/* Cfa/iterate.scm 74 */
																obj_t BgL_arg1815z00_3641;
																long BgL_arg1816z00_3642;

																BgL_arg1815z00_3641 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Cfa/iterate.scm 74 */
																	long BgL_arg1817z00_3643;

																	BgL_arg1817z00_3643 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3619);
																	BgL_arg1816z00_3642 =
																		(BgL_arg1817z00_3643 - OBJECT_TYPE);
																}
																BgL_oclassz00_3633 =
																	VECTOR_REF(BgL_arg1815z00_3641,
																	BgL_arg1816z00_3642);
															}
															{	/* Cfa/iterate.scm 74 */
																bool_t BgL__ortest_1115z00_3634;

																BgL__ortest_1115z00_3634 =
																	(BgL_classz00_3617 == BgL_oclassz00_3633);
																if (BgL__ortest_1115z00_3634)
																	{	/* Cfa/iterate.scm 74 */
																		BgL_res1953z00_3650 =
																			BgL__ortest_1115z00_3634;
																	}
																else
																	{	/* Cfa/iterate.scm 74 */
																		long BgL_odepthz00_3635;

																		{	/* Cfa/iterate.scm 74 */
																			obj_t BgL_arg1804z00_3636;

																			BgL_arg1804z00_3636 =
																				(BgL_oclassz00_3633);
																			BgL_odepthz00_3635 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_3636);
																		}
																		if ((2L < BgL_odepthz00_3635))
																			{	/* Cfa/iterate.scm 74 */
																				obj_t BgL_arg1802z00_3638;

																				{	/* Cfa/iterate.scm 74 */
																					obj_t BgL_arg1803z00_3639;

																					BgL_arg1803z00_3639 =
																						(BgL_oclassz00_3633);
																					BgL_arg1802z00_3638 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_3639, 2L);
																				}
																				BgL_res1953z00_3650 =
																					(BgL_arg1802z00_3638 ==
																					BgL_classz00_3617);
																			}
																		else
																			{	/* Cfa/iterate.scm 74 */
																				BgL_res1953z00_3650 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2016z00_4031 = BgL_res1953z00_3650;
													}
											}
										else
											{	/* Cfa/iterate.scm 74 */
												BgL_test2016z00_4031 = ((bool_t) 0);
											}
									}
								}
								if (BgL_test2016z00_4031)
									{	/* Cfa/iterate.scm 74 */
										return
											(
											(((BgL_globalz00_bglt) COBJECT(
														((BgL_globalz00_bglt)
															(((BgL_sfunz00_bglt) COBJECT(
																		((BgL_sfunz00_bglt) BgL_valz00_3006)))->
																BgL_thezd2closurezd2globalz00))))->
												BgL_importz00) == CNST_TABLE_REF(0));
									}
								else
									{	/* Cfa/iterate.scm 74 */
										return ((bool_t) 0);
									}
							}
						else
							{	/* Cfa/iterate.scm 73 */
								return ((bool_t) 0);
							}
					}
				}
			else
				{	/* Cfa/iterate.scm 71 */
					return ((bool_t) 0);
				}
		}

	}



/* cfa-iterate! */
	BGL_EXPORTED_DEF obj_t BGl_cfazd2iteratez12zc0zzcfa_iteratez00(obj_t
		BgL_globalsz00_5)
	{
		{	/* Cfa/iterate.scm 80 */
			BGl_za2cfazd2continuezf3za2z21zzcfa_iteratez00 = BFALSE;
			BGl_za2cfazd2stampza2zd2zzcfa_iteratez00 =
				ADDFX(BINT(1L), BGl_za2cfazd2stampza2zd2zzcfa_iteratez00);
			{	/* Cfa/iterate.scm 83 */
				obj_t BgL_list1577z00_3013;

				BgL_list1577z00_3013 =
					MAKE_YOUNG_PAIR(BGl_string1965z00zzcfa_iteratez00, BNIL);
				BGl_verbosez00zztools_speekz00(BINT(4L), BgL_list1577z00_3013);
			}
			{
				obj_t BgL_l1498z00_3015;

				BgL_l1498z00_3015 = BgL_globalsz00_5;
			BgL_zc3z04anonymousza31578ze3z87_3016:
				if (PAIRP(BgL_l1498z00_3015))
					{	/* Cfa/iterate.scm 85 */
						{	/* Cfa/iterate.scm 86 */
							obj_t BgL_gz00_3018;

							BgL_gz00_3018 = CAR(BgL_l1498z00_3015);
							{	/* Cfa/iterate.scm 89 */
								BgL_valuez00_bglt BgL_arg1584z00_3019;

								BgL_arg1584z00_3019 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_gz00_3018))))->BgL_valuez00);
								BGl_cfazd2exportzd2varz12z12zzcfa_iteratez00
									(BgL_arg1584z00_3019, BgL_gz00_3018);
							}
							BNIL;
						}
						{
							obj_t BgL_l1498z00_4074;

							BgL_l1498z00_4074 = CDR(BgL_l1498z00_3015);
							BgL_l1498z00_3015 = BgL_l1498z00_4074;
							goto BgL_zc3z04anonymousza31578ze3z87_3016;
						}
					}
				else
					{	/* Cfa/iterate.scm 85 */
						((bool_t) 1);
					}
			}
			{	/* Cfa/iterate.scm 92 */
				obj_t BgL_list1586z00_3022;

				BgL_list1586z00_3022 =
					MAKE_YOUNG_PAIR(BGl_string1966z00zzcfa_iteratez00, BNIL);
				BGl_verbosez00zztools_speekz00(BINT(4L), BgL_list1586z00_3022);
			}
			return BNIL;
		}

	}



/* &cfa-iterate! */
	obj_t BGl_z62cfazd2iteratez12za2zzcfa_iteratez00(obj_t BgL_envz00_3825,
		obj_t BgL_globalsz00_3826)
	{
		{	/* Cfa/iterate.scm 80 */
			return BGl_cfazd2iteratez12zc0zzcfa_iteratez00(BgL_globalsz00_3826);
		}

	}



/* cfa-intern-sfun! */
	BGL_EXPORTED_DEF BgL_approxz00_bglt
		BGl_cfazd2internzd2sfunz12z12zzcfa_iteratez00(BgL_sfunz00_bglt
		BgL_sfunz00_12, obj_t BgL_ownerz00_13)
	{
		{	/* Cfa/iterate.scm 146 */
			{	/* Cfa/iterate.scm 157 */
				bool_t BgL_test2022z00_4080;

				{	/* Cfa/iterate.scm 157 */
					long BgL_arg1642z00_3052;

					{
						BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_4081;

						{
							obj_t BgL_auxz00_4082;

							{	/* Cfa/iterate.scm 157 */
								BgL_objectz00_bglt BgL_tmpz00_4083;

								BgL_tmpz00_4083 = ((BgL_objectz00_bglt) BgL_sfunz00_12);
								BgL_auxz00_4082 = BGL_OBJECT_WIDENING(BgL_tmpz00_4083);
							}
							BgL_auxz00_4081 =
								((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_4082);
						}
						BgL_arg1642z00_3052 =
							(((BgL_internzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_4081))->
							BgL_stampz00);
					}
					BgL_test2022z00_4080 =
						(BgL_arg1642z00_3052 ==
						(long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00));
				}
				if (BgL_test2022z00_4080)
					{	/* Cfa/iterate.scm 157 */
						{	/* Cfa/iterate.scm 161 */
							BgL_approxz00_bglt BgL_arg1593z00_3027;

							{
								BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_4090;

								{
									obj_t BgL_auxz00_4091;

									{	/* Cfa/iterate.scm 161 */
										BgL_objectz00_bglt BgL_tmpz00_4092;

										BgL_tmpz00_4092 = ((BgL_objectz00_bglt) BgL_sfunz00_12);
										BgL_auxz00_4091 = BGL_OBJECT_WIDENING(BgL_tmpz00_4092);
									}
									BgL_auxz00_4090 =
										((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_4091);
								}
								BgL_arg1593z00_3027 =
									(((BgL_internzd2sfunzf2cinfoz20_bglt)
										COBJECT(BgL_auxz00_4090))->BgL_approxz00);
							}
							return
								BGl_z62polymorphicz62zzcfa_iteratez00(BgL_sfunz00_12,
								BgL_arg1593z00_3027);
						}
					}
				else
					{	/* Cfa/iterate.scm 162 */
						obj_t BgL_resz00_3028;

						{	/* Cfa/iterate.scm 165 */
							obj_t BgL_zc3z04anonymousza31595ze3z87_3828;

							BgL_zc3z04anonymousza31595ze3z87_3828 =
								MAKE_FX_PROCEDURE
								(BGl_z62zc3z04anonymousza31595ze3ze5zzcfa_iteratez00,
								(int) (0L), (int) (3L));
							PROCEDURE_SET(BgL_zc3z04anonymousza31595ze3z87_3828, (int) (0L),
								((obj_t) BgL_sfunz00_12));
							PROCEDURE_SET(BgL_zc3z04anonymousza31595ze3z87_3828, (int) (1L),
								BgL_ownerz00_13);
							PROCEDURE_SET(BgL_zc3z04anonymousza31595ze3z87_3828, (int) (2L),
								((obj_t) BgL_sfunz00_12));
							BgL_resz00_3028 =
								BGl_timez00zz__biglooz00(BgL_zc3z04anonymousza31595ze3z87_3828);
						}
						{	/* Cfa/iterate.scm 163 */
							obj_t BgL_rtimez00_3029;
							obj_t BgL_stimez00_3030;
							obj_t BgL_utimez00_3031;

							{	/* Cfa/iterate.scm 163 */
								obj_t BgL_tmpz00_3703;

								{	/* Cfa/iterate.scm 163 */
									int BgL_tmpz00_4110;

									BgL_tmpz00_4110 = (int) (1L);
									BgL_tmpz00_3703 = BGL_MVALUES_VAL(BgL_tmpz00_4110);
								}
								{	/* Cfa/iterate.scm 163 */
									int BgL_tmpz00_4113;

									BgL_tmpz00_4113 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4113, BUNSPEC);
								}
								BgL_rtimez00_3029 = BgL_tmpz00_3703;
							}
							{	/* Cfa/iterate.scm 163 */
								obj_t BgL_tmpz00_3704;

								{	/* Cfa/iterate.scm 163 */
									int BgL_tmpz00_4116;

									BgL_tmpz00_4116 = (int) (2L);
									BgL_tmpz00_3704 = BGL_MVALUES_VAL(BgL_tmpz00_4116);
								}
								{	/* Cfa/iterate.scm 163 */
									int BgL_tmpz00_4119;

									BgL_tmpz00_4119 = (int) (2L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4119, BUNSPEC);
								}
								BgL_stimez00_3030 = BgL_tmpz00_3704;
							}
							{	/* Cfa/iterate.scm 163 */
								obj_t BgL_tmpz00_3705;

								{	/* Cfa/iterate.scm 163 */
									int BgL_tmpz00_4122;

									BgL_tmpz00_4122 = (int) (3L);
									BgL_tmpz00_3705 = BGL_MVALUES_VAL(BgL_tmpz00_4122);
								}
								{	/* Cfa/iterate.scm 163 */
									int BgL_tmpz00_4125;

									BgL_tmpz00_4125 = (int) (3L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_4125, BUNSPEC);
								}
								BgL_utimez00_3031 = BgL_tmpz00_3705;
							}
							return ((BgL_approxz00_bglt) BgL_resz00_3028);
						}
					}
			}
		}

	}



/* &cfa-intern-sfun! */
	BgL_approxz00_bglt BGl_z62cfazd2internzd2sfunz12z70zzcfa_iteratez00(obj_t
		BgL_envz00_3829, obj_t BgL_sfunz00_3830, obj_t BgL_ownerz00_3831)
	{
		{	/* Cfa/iterate.scm 146 */
			return
				BGl_cfazd2internzd2sfunz12z12zzcfa_iteratez00(
				((BgL_sfunz00_bglt) BgL_sfunz00_3830), BgL_ownerz00_3831);
		}

	}



/* &polymorphic */
	BgL_approxz00_bglt BGl_z62polymorphicz62zzcfa_iteratez00(BgL_sfunz00_bglt
		BgL_sfunz00_3832, BgL_approxz00_bglt BgL_approxz00_3053)
	{
		{	/* Cfa/iterate.scm 153 */
			{	/* Cfa/iterate.scm 150 */
				bool_t BgL_test2023z00_4131;

				{
					BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_4132;

					{
						obj_t BgL_auxz00_4133;

						{	/* Cfa/iterate.scm 150 */
							BgL_objectz00_bglt BgL_tmpz00_4134;

							BgL_tmpz00_4134 = ((BgL_objectz00_bglt) BgL_sfunz00_3832);
							BgL_auxz00_4133 = BGL_OBJECT_WIDENING(BgL_tmpz00_4134);
						}
						BgL_auxz00_4132 =
							((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_4133);
					}
					BgL_test2023z00_4131 =
						(((BgL_internzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_4132))->
						BgL_polymorphiczf3zf3);
				}
				if (BgL_test2023z00_4131)
					{
						BgL_typez00_bglt BgL_auxz00_4139;

						{	/* Cfa/iterate.scm 152 */
							BgL_typez00_bglt BgL_arg1646z00_3058;

							BgL_arg1646z00_3058 =
								(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_3053))->
								BgL_typez00);
							BgL_auxz00_4139 =
								BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_arg1646z00_3058);
						}
						((((BgL_approxz00_bglt) COBJECT(BgL_approxz00_3053))->BgL_typez00) =
							((BgL_typez00_bglt) BgL_auxz00_4139), BUNSPEC);
					}
				else
					{	/* Cfa/iterate.scm 150 */
						BFALSE;
					}
			}
			return BgL_approxz00_3053;
		}

	}



/* &<@anonymous:1595> */
	obj_t BGl_z62zc3z04anonymousza31595ze3ze5zzcfa_iteratez00(obj_t
		BgL_envz00_3833)
	{
		{	/* Cfa/iterate.scm 164 */
			{	/* Cfa/iterate.scm 165 */
				BgL_sfunz00_bglt BgL_sfunz00_3834;
				obj_t BgL_ownerz00_3835;
				BgL_sfunz00_bglt BgL_i1169z00_3836;

				BgL_sfunz00_3834 =
					((BgL_sfunz00_bglt) PROCEDURE_REF(BgL_envz00_3833, (int) (0L)));
				BgL_ownerz00_3835 = PROCEDURE_REF(BgL_envz00_3833, (int) (1L));
				BgL_i1169z00_3836 =
					((BgL_sfunz00_bglt) PROCEDURE_REF(BgL_envz00_3833, (int) (2L)));
				{
					BgL_approxz00_bglt BgL_auxz00_4151;

					{	/* Cfa/iterate.scm 165 */
						bool_t BgL_test2024z00_4152;

						{	/* Cfa/iterate.scm 165 */
							obj_t BgL_classz00_3866;

							BgL_classz00_3866 = BGl_globalz00zzast_varz00;
							if (BGL_OBJECTP(BgL_ownerz00_3835))
								{	/* Cfa/iterate.scm 165 */
									BgL_objectz00_bglt BgL_arg1807z00_3867;

									BgL_arg1807z00_3867 =
										(BgL_objectz00_bglt) (BgL_ownerz00_3835);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/iterate.scm 165 */
											long BgL_idxz00_3868;

											BgL_idxz00_3868 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3867);
											BgL_test2024z00_4152 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_3868 + 2L)) == BgL_classz00_3866);
										}
									else
										{	/* Cfa/iterate.scm 165 */
											bool_t BgL_res1954z00_3871;

											{	/* Cfa/iterate.scm 165 */
												obj_t BgL_oclassz00_3872;

												{	/* Cfa/iterate.scm 165 */
													obj_t BgL_arg1815z00_3873;
													long BgL_arg1816z00_3874;

													BgL_arg1815z00_3873 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/iterate.scm 165 */
														long BgL_arg1817z00_3875;

														BgL_arg1817z00_3875 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3867);
														BgL_arg1816z00_3874 =
															(BgL_arg1817z00_3875 - OBJECT_TYPE);
													}
													BgL_oclassz00_3872 =
														VECTOR_REF(BgL_arg1815z00_3873,
														BgL_arg1816z00_3874);
												}
												{	/* Cfa/iterate.scm 165 */
													bool_t BgL__ortest_1115z00_3876;

													BgL__ortest_1115z00_3876 =
														(BgL_classz00_3866 == BgL_oclassz00_3872);
													if (BgL__ortest_1115z00_3876)
														{	/* Cfa/iterate.scm 165 */
															BgL_res1954z00_3871 = BgL__ortest_1115z00_3876;
														}
													else
														{	/* Cfa/iterate.scm 165 */
															long BgL_odepthz00_3877;

															{	/* Cfa/iterate.scm 165 */
																obj_t BgL_arg1804z00_3878;

																BgL_arg1804z00_3878 = (BgL_oclassz00_3872);
																BgL_odepthz00_3877 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_3878);
															}
															if ((2L < BgL_odepthz00_3877))
																{	/* Cfa/iterate.scm 165 */
																	obj_t BgL_arg1802z00_3879;

																	{	/* Cfa/iterate.scm 165 */
																		obj_t BgL_arg1803z00_3880;

																		BgL_arg1803z00_3880 = (BgL_oclassz00_3872);
																		BgL_arg1802z00_3879 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_3880, 2L);
																	}
																	BgL_res1954z00_3871 =
																		(BgL_arg1802z00_3879 == BgL_classz00_3866);
																}
															else
																{	/* Cfa/iterate.scm 165 */
																	BgL_res1954z00_3871 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2024z00_4152 = BgL_res1954z00_3871;
										}
								}
							else
								{	/* Cfa/iterate.scm 165 */
									BgL_test2024z00_4152 = ((bool_t) 0);
								}
						}
						if (BgL_test2024z00_4152)
							{	/* Cfa/iterate.scm 166 */
								obj_t BgL_arg1602z00_3881;

								BgL_arg1602z00_3881 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_ownerz00_3835)))->BgL_idz00);
								{	/* Cfa/iterate.scm 166 */
									obj_t BgL_list1603z00_3882;

									{	/* Cfa/iterate.scm 166 */
										obj_t BgL_arg1605z00_3883;

										{	/* Cfa/iterate.scm 166 */
											obj_t BgL_arg1606z00_3884;

											BgL_arg1606z00_3884 =
												MAKE_YOUNG_PAIR(BGl_string1967z00zzcfa_iteratez00,
												BNIL);
											BgL_arg1605z00_3883 =
												MAKE_YOUNG_PAIR(BgL_arg1602z00_3881,
												BgL_arg1606z00_3884);
										}
										BgL_list1603z00_3882 =
											MAKE_YOUNG_PAIR(BGl_string1968z00zzcfa_iteratez00,
											BgL_arg1605z00_3883);
									}
									BGl_verbosez00zztools_speekz00(BINT(4L),
										BgL_list1603z00_3882);
								}
							}
						else
							{	/* Cfa/iterate.scm 167 */
								obj_t BgL_arg1609z00_3885;

								BgL_arg1609z00_3885 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_ownerz00_3835)))->BgL_idz00);
								{	/* Cfa/iterate.scm 167 */
									obj_t BgL_list1610z00_3886;

									{	/* Cfa/iterate.scm 167 */
										obj_t BgL_arg1611z00_3887;

										BgL_arg1611z00_3887 =
											MAKE_YOUNG_PAIR(BGl_string1969z00zzcfa_iteratez00, BNIL);
										BgL_list1610z00_3886 =
											MAKE_YOUNG_PAIR(BgL_arg1609z00_3885, BgL_arg1611z00_3887);
									}
									BGl_verbosez00zztools_speekz00(BINT(4L),
										BgL_list1610z00_3886);
								}
							}
					}
					{
						BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_4188;

						{
							obj_t BgL_auxz00_4189;

							{	/* Cfa/iterate.scm 171 */
								BgL_objectz00_bglt BgL_tmpz00_4190;

								BgL_tmpz00_4190 = ((BgL_objectz00_bglt) BgL_i1169z00_3836);
								BgL_auxz00_4189 = BGL_OBJECT_WIDENING(BgL_tmpz00_4190);
							}
							BgL_auxz00_4188 =
								((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_4189);
						}
						((((BgL_internzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_4188))->
								BgL_stampz00) =
							((long) (long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00)),
							BUNSPEC);
					}
					{	/* Cfa/iterate.scm 172 */
						obj_t BgL_curz00_3888;

						BgL_curz00_3888 = BGl_za2cfazd2currentza2zd2zzcfa_iteratez00;
						{	/* Cfa/iterate.scm 173 */
							obj_t BgL_arg1613z00_3889;
							long BgL_arg1615z00_3890;

							BgL_arg1613z00_3889 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_ownerz00_3835)))->BgL_idz00);
							{
								BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_4198;

								{
									obj_t BgL_auxz00_4199;

									{	/* Cfa/iterate.scm 173 */
										BgL_objectz00_bglt BgL_tmpz00_4200;

										BgL_tmpz00_4200 = ((BgL_objectz00_bglt) BgL_i1169z00_3836);
										BgL_auxz00_4199 = BGL_OBJECT_WIDENING(BgL_tmpz00_4200);
									}
									BgL_auxz00_4198 =
										((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_4199);
								}
								BgL_arg1615z00_3890 =
									(((BgL_internzd2sfunzf2cinfoz20_bglt)
										COBJECT(BgL_auxz00_4198))->BgL_stampz00);
							}
							{	/* Cfa/iterate.scm 173 */
								obj_t BgL_list1616z00_3891;

								{	/* Cfa/iterate.scm 173 */
									obj_t BgL_arg1625z00_3892;

									BgL_arg1625z00_3892 =
										MAKE_YOUNG_PAIR(BINT(BgL_arg1615z00_3890), BNIL);
									BgL_list1616z00_3891 =
										MAKE_YOUNG_PAIR(BgL_arg1613z00_3889, BgL_arg1625z00_3892);
								}
								BGl_za2cfazd2currentza2zd2zzcfa_iteratez00 =
									BGl_formatz00zz__r4_output_6_10_3z00
									(BGl_string1970z00zzcfa_iteratez00, BgL_list1616z00_3891);
						}}
						{	/* Cfa/iterate.scm 174 */
							BgL_approxz00_bglt BgL_arg1626z00_3893;
							BgL_approxz00_bglt BgL_arg1627z00_3894;

							{
								BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_4209;

								{
									obj_t BgL_auxz00_4210;

									{	/* Cfa/iterate.scm 174 */
										BgL_objectz00_bglt BgL_tmpz00_4211;

										BgL_tmpz00_4211 = ((BgL_objectz00_bglt) BgL_i1169z00_3836);
										BgL_auxz00_4210 = BGL_OBJECT_WIDENING(BgL_tmpz00_4211);
									}
									BgL_auxz00_4209 =
										((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_4210);
								}
								BgL_arg1626z00_3893 =
									(((BgL_internzd2sfunzf2cinfoz20_bglt)
										COBJECT(BgL_auxz00_4209))->BgL_approxz00);
							}
							{	/* Cfa/iterate.scm 174 */
								obj_t BgL_arg1629z00_3895;

								BgL_arg1629z00_3895 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt) BgL_i1169z00_3836)))->BgL_bodyz00);
								BgL_arg1627z00_3894 =
									BGl_cfaz12z12zzcfa_cfaz00(
									((BgL_nodez00_bglt) BgL_arg1629z00_3895));
							}
							BGl_unionzd2approxzd2filterz12z12zzcfa_approxz00
								(BgL_arg1626z00_3893, BgL_arg1627z00_3894);
						}
						BGl_za2cfazd2currentza2zd2zzcfa_iteratez00 = BgL_curz00_3888;
					}
					{	/* Cfa/iterate.scm 178 */
						BgL_approxz00_bglt BgL_arg1630z00_3896;

						{
							BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_4221;

							{
								obj_t BgL_auxz00_4222;

								{	/* Cfa/iterate.scm 178 */
									BgL_objectz00_bglt BgL_tmpz00_4223;

									BgL_tmpz00_4223 = ((BgL_objectz00_bglt) BgL_i1169z00_3836);
									BgL_auxz00_4222 = BGL_OBJECT_WIDENING(BgL_tmpz00_4223);
								}
								BgL_auxz00_4221 =
									((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_4222);
							}
							BgL_arg1630z00_3896 =
								(((BgL_internzd2sfunzf2cinfoz20_bglt)
									COBJECT(BgL_auxz00_4221))->BgL_approxz00);
						}
						BgL_auxz00_4151 =
							BGl_z62polymorphicz62zzcfa_iteratez00(BgL_sfunz00_3834,
							BgL_arg1630z00_3896);
					}
					return ((obj_t) BgL_auxz00_4151);
				}
			}
		}

	}



/* cfa-current */
	BGL_EXPORTED_DEF obj_t BGl_cfazd2currentzd2zzcfa_iteratez00(void)
	{
		{	/* Cfa/iterate.scm 192 */
			return BGl_za2cfazd2currentza2zd2zzcfa_iteratez00;
		}

	}



/* &cfa-current */
	obj_t BGl_z62cfazd2currentzb0zzcfa_iteratez00(obj_t BgL_envz00_3837)
	{
		{	/* Cfa/iterate.scm 192 */
			return BGl_cfazd2currentzd2zzcfa_iteratez00();
		}

	}



/* continue-cfa! */
	BGL_EXPORTED_DEF obj_t BGl_continuezd2cfaz12zc0zzcfa_iteratez00(obj_t
		BgL_reasonz00_14)
	{
		{	/* Cfa/iterate.scm 198 */
			if (CBOOL(BGl_za2cfazd2continuezf3za2z21zzcfa_iteratez00))
				{	/* Cfa/iterate.scm 199 */
					BFALSE;
				}
			else
				{	/* Cfa/iterate.scm 199 */
					BNIL;
				}
			return (BGl_za2cfazd2continuezf3za2z21zzcfa_iteratez00 = BTRUE, BUNSPEC);
		}

	}



/* &continue-cfa! */
	obj_t BGl_z62continuezd2cfaz12za2zzcfa_iteratez00(obj_t BgL_envz00_3838,
		obj_t BgL_reasonz00_3839)
	{
		{	/* Cfa/iterate.scm 198 */
			return BGl_continuezd2cfaz12zc0zzcfa_iteratez00(BgL_reasonz00_3839);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_iteratez00(void)
	{
		{	/* Cfa/iterate.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_iteratez00(void)
	{
		{	/* Cfa/iterate.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_cfazd2exportzd2varz12zd2envzc0zzcfa_iteratez00,
				BGl_proc1971z00zzcfa_iteratez00, BGl_valuez00zzast_varz00,
				BGl_string1972z00zzcfa_iteratez00);
		}

	}



/* &cfa-export-var!1503 */
	obj_t BGl_z62cfazd2exportzd2varz121503z70zzcfa_iteratez00(obj_t
		BgL_envz00_3841, obj_t BgL_valuez00_3842, obj_t BgL_ownerz00_3843)
	{
		{	/* Cfa/iterate.scm 98 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(2),
				BGl_string1973z00zzcfa_iteratez00,
				((obj_t) ((BgL_valuez00_bglt) BgL_valuez00_3842)));
		}

	}



/* cfa-export-var! */
	BGL_EXPORTED_DEF obj_t
		BGl_cfazd2exportzd2varz12z12zzcfa_iteratez00(BgL_valuez00_bglt
		BgL_valuez00_6, obj_t BgL_ownerz00_7)
	{
		{	/* Cfa/iterate.scm 98 */
			{	/* Cfa/iterate.scm 98 */
				obj_t BgL_method1504z00_3065;

				{	/* Cfa/iterate.scm 98 */
					obj_t BgL_res1959z00_3736;

					{	/* Cfa/iterate.scm 98 */
						long BgL_objzd2classzd2numz00_3707;

						BgL_objzd2classzd2numz00_3707 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_valuez00_6));
						{	/* Cfa/iterate.scm 98 */
							obj_t BgL_arg1811z00_3708;

							BgL_arg1811z00_3708 =
								PROCEDURE_REF
								(BGl_cfazd2exportzd2varz12zd2envzc0zzcfa_iteratez00,
								(int) (1L));
							{	/* Cfa/iterate.scm 98 */
								int BgL_offsetz00_3711;

								BgL_offsetz00_3711 = (int) (BgL_objzd2classzd2numz00_3707);
								{	/* Cfa/iterate.scm 98 */
									long BgL_offsetz00_3712;

									BgL_offsetz00_3712 =
										((long) (BgL_offsetz00_3711) - OBJECT_TYPE);
									{	/* Cfa/iterate.scm 98 */
										long BgL_modz00_3713;

										BgL_modz00_3713 =
											(BgL_offsetz00_3712 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/iterate.scm 98 */
											long BgL_restz00_3715;

											BgL_restz00_3715 =
												(BgL_offsetz00_3712 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/iterate.scm 98 */

												{	/* Cfa/iterate.scm 98 */
													obj_t BgL_bucketz00_3717;

													BgL_bucketz00_3717 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3708), BgL_modz00_3713);
													BgL_res1959z00_3736 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3717), BgL_restz00_3715);
					}}}}}}}}
					BgL_method1504z00_3065 = BgL_res1959z00_3736;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1504z00_3065,
					((obj_t) BgL_valuez00_6), BgL_ownerz00_7);
			}
		}

	}



/* &cfa-export-var! */
	obj_t BGl_z62cfazd2exportzd2varz12z70zzcfa_iteratez00(obj_t BgL_envz00_3844,
		obj_t BgL_valuez00_3845, obj_t BgL_ownerz00_3846)
	{
		{	/* Cfa/iterate.scm 98 */
			return
				BGl_cfazd2exportzd2varz12z12zzcfa_iteratez00(
				((BgL_valuez00_bglt) BgL_valuez00_3845), BgL_ownerz00_3846);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_iteratez00(void)
	{
		{	/* Cfa/iterate.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfazd2exportzd2varz12zd2envzc0zzcfa_iteratez00,
				BGl_svarzf2Cinfozf2zzcfa_infoz00, BGl_proc1974z00zzcfa_iteratez00,
				BGl_string1975z00zzcfa_iteratez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfazd2exportzd2varz12zd2envzc0zzcfa_iteratez00,
				BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00,
				BGl_proc1976z00zzcfa_iteratez00, BGl_string1975z00zzcfa_iteratez00);
		}

	}



/* &cfa-export-var!-inte1508 */
	obj_t BGl_z62cfazd2exportzd2varz12zd2inte1508za2zzcfa_iteratez00(obj_t
		BgL_envz00_3849, obj_t BgL_valuez00_3850, obj_t BgL_ownerz00_3851)
	{
		{	/* Cfa/iterate.scm 117 */
			{	/* Cfa/iterate.scm 122 */
				bool_t BgL_test2030z00_4274;

				{	/* Cfa/iterate.scm 122 */
					long BgL_arg1688z00_3899;

					{
						BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_4275;

						{
							obj_t BgL_auxz00_4276;

							{	/* Cfa/iterate.scm 122 */
								BgL_objectz00_bglt BgL_tmpz00_4277;

								BgL_tmpz00_4277 =
									((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_valuez00_3850));
								BgL_auxz00_4276 = BGL_OBJECT_WIDENING(BgL_tmpz00_4277);
							}
							BgL_auxz00_4275 =
								((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_4276);
						}
						BgL_arg1688z00_3899 =
							(((BgL_internzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_4275))->
							BgL_stampz00);
					}
					BgL_test2030z00_4274 =
						(BgL_arg1688z00_3899 ==
						(long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00));
				}
				if (BgL_test2030z00_4274)
					{	/* Cfa/iterate.scm 122 */
						{
							BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_4285;

							{
								obj_t BgL_auxz00_4286;

								{	/* Cfa/iterate.scm 124 */
									BgL_objectz00_bglt BgL_tmpz00_4287;

									BgL_tmpz00_4287 =
										((BgL_objectz00_bglt)
										((BgL_sfunz00_bglt) BgL_valuez00_3850));
									BgL_auxz00_4286 = BGL_OBJECT_WIDENING(BgL_tmpz00_4287);
								}
								BgL_auxz00_4285 =
									((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_4286);
							}
							((((BgL_internzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_4285))->
									BgL_stampz00) =
								((long) (long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00)),
								BUNSPEC);
						}
						{
							BgL_approxz00_bglt BgL_auxz00_4294;

							{
								BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_4295;

								{
									obj_t BgL_auxz00_4296;

									{	/* Cfa/iterate.scm 124 */
										BgL_objectz00_bglt BgL_tmpz00_4297;

										BgL_tmpz00_4297 =
											((BgL_objectz00_bglt)
											((BgL_sfunz00_bglt) BgL_valuez00_3850));
										BgL_auxz00_4296 = BGL_OBJECT_WIDENING(BgL_tmpz00_4297);
									}
									BgL_auxz00_4295 =
										((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_4296);
								}
								BgL_auxz00_4294 =
									(((BgL_internzd2sfunzf2cinfoz20_bglt)
										COBJECT(BgL_auxz00_4295))->BgL_approxz00);
							}
							return ((obj_t) BgL_auxz00_4294);
						}
					}
				else
					{	/* Cfa/iterate.scm 122 */
						{	/* Cfa/iterate.scm 131 */
							obj_t BgL_g1502z00_3900;

							BgL_g1502z00_3900 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt)
											((BgL_sfunz00_bglt) BgL_valuez00_3850))))->BgL_argsz00);
							{
								obj_t BgL_l1500z00_3902;

								BgL_l1500z00_3902 = BgL_g1502z00_3900;
							BgL_zc3z04anonymousza31665ze3z87_3901:
								if (PAIRP(BgL_l1500z00_3902))
									{	/* Cfa/iterate.scm 131 */
										{	/* Cfa/iterate.scm 132 */
											obj_t BgL_localz00_3903;

											BgL_localz00_3903 = CAR(BgL_l1500z00_3902);
											{	/* Cfa/iterate.scm 132 */
												BgL_valuez00_bglt BgL_valz00_3904;

												BgL_valz00_3904 =
													(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt) BgL_localz00_3903))))->
													BgL_valuez00);
												{	/* Cfa/iterate.scm 137 */
													bool_t BgL_test2032z00_4313;

													{
														BgL_svarzf2cinfozf2_bglt BgL_auxz00_4314;

														{
															obj_t BgL_auxz00_4315;

															{	/* Cfa/iterate.scm 137 */
																BgL_objectz00_bglt BgL_tmpz00_4316;

																BgL_tmpz00_4316 =
																	((BgL_objectz00_bglt)
																	((BgL_svarz00_bglt) BgL_valz00_3904));
																BgL_auxz00_4315 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_4316);
															}
															BgL_auxz00_4314 =
																((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_4315);
														}
														BgL_test2032z00_4313 =
															(((BgL_svarzf2cinfozf2_bglt)
																COBJECT(BgL_auxz00_4314))->BgL_clozd2envzf3z21);
													}
													if (BgL_test2032z00_4313)
														{	/* Cfa/iterate.scm 137 */
															BFALSE;
														}
													else
														{	/* Cfa/iterate.scm 138 */
															BgL_approxz00_bglt BgL_arg1675z00_3905;

															{
																BgL_svarzf2cinfozf2_bglt BgL_auxz00_4322;

																{
																	obj_t BgL_auxz00_4323;

																	{	/* Cfa/iterate.scm 138 */
																		BgL_objectz00_bglt BgL_tmpz00_4324;

																		BgL_tmpz00_4324 =
																			((BgL_objectz00_bglt)
																			((BgL_svarz00_bglt) BgL_valz00_3904));
																		BgL_auxz00_4323 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_4324);
																	}
																	BgL_auxz00_4322 =
																		((BgL_svarzf2cinfozf2_bglt)
																		BgL_auxz00_4323);
																}
																BgL_arg1675z00_3905 =
																	(((BgL_svarzf2cinfozf2_bglt)
																		COBJECT(BgL_auxz00_4322))->BgL_approxz00);
															}
															BGl_approxzd2setzd2topz12z12zzcfa_approxz00
																(BgL_arg1675z00_3905);
														}
												}
											}
										}
										{
											obj_t BgL_l1500z00_4331;

											BgL_l1500z00_4331 = CDR(BgL_l1500z00_3902);
											BgL_l1500z00_3902 = BgL_l1500z00_4331;
											goto BgL_zc3z04anonymousza31665ze3z87_3901;
										}
									}
								else
									{	/* Cfa/iterate.scm 131 */
										((bool_t) 1);
									}
							}
						}
						{	/* Cfa/iterate.scm 141 */
							BgL_approxz00_bglt BgL_arg1681z00_3906;

							BgL_arg1681z00_3906 =
								BGl_cfazd2internzd2sfunz12z12zzcfa_iteratez00(
								((BgL_sfunz00_bglt) BgL_valuez00_3850), BgL_ownerz00_3851);
							return
								((obj_t)
								BGl_loosez12z12zzcfa_loosez00(BgL_arg1681z00_3906,
									CNST_TABLE_REF(3)));
						}
					}
			}
		}

	}



/* &cfa-export-var!-svar1506 */
	obj_t BGl_z62cfazd2exportzd2varz12zd2svar1506za2zzcfa_iteratez00(obj_t
		BgL_envz00_3852, obj_t BgL_valuez00_3853, obj_t BgL_ownerz00_3854)
	{
		{	/* Cfa/iterate.scm 103 */
			{	/* Cfa/iterate.scm 108 */
				bool_t BgL_test2033z00_4338;

				{	/* Cfa/iterate.scm 108 */
					long BgL_arg1661z00_3908;

					{
						BgL_svarzf2cinfozf2_bglt BgL_auxz00_4339;

						{
							obj_t BgL_auxz00_4340;

							{	/* Cfa/iterate.scm 108 */
								BgL_objectz00_bglt BgL_tmpz00_4341;

								BgL_tmpz00_4341 =
									((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_valuez00_3853));
								BgL_auxz00_4340 = BGL_OBJECT_WIDENING(BgL_tmpz00_4341);
							}
							BgL_auxz00_4339 = ((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_4340);
						}
						BgL_arg1661z00_3908 =
							(((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_4339))->
							BgL_stampz00);
					}
					BgL_test2033z00_4338 =
						(BgL_arg1661z00_3908 ==
						(long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00));
				}
				if (BgL_test2033z00_4338)
					{	/* Cfa/iterate.scm 108 */
						return
							BGl_cfazd2variablezd2valuezd2approxzd2zzcfa_cfaz00(
							((BgL_valuez00_bglt) ((BgL_svarz00_bglt) BgL_valuez00_3853)));
					}
				else
					{	/* Cfa/iterate.scm 108 */
						{
							BgL_svarzf2cinfozf2_bglt BgL_auxz00_4352;

							{
								obj_t BgL_auxz00_4353;

								{	/* Cfa/iterate.scm 111 */
									BgL_objectz00_bglt BgL_tmpz00_4354;

									BgL_tmpz00_4354 =
										((BgL_objectz00_bglt)
										((BgL_svarz00_bglt) BgL_valuez00_3853));
									BgL_auxz00_4353 = BGL_OBJECT_WIDENING(BgL_tmpz00_4354);
								}
								BgL_auxz00_4352 = ((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_4353);
							}
							((((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_4352))->
									BgL_stampz00) =
								((long) (long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00)),
								BUNSPEC);
						}
						{	/* Cfa/iterate.scm 112 */
							obj_t BgL_arg1654z00_3909;

							BgL_arg1654z00_3909 =
								BGl_cfazd2variablezd2valuezd2approxzd2zzcfa_cfaz00(
								((BgL_valuez00_bglt) ((BgL_svarz00_bglt) BgL_valuez00_3853)));
							return
								((obj_t)
								BGl_loosez12z12zzcfa_loosez00(
									((BgL_approxz00_bglt) BgL_arg1654z00_3909),
									CNST_TABLE_REF(3)));
						}
					}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_iteratez00(void)
	{
		{	/* Cfa/iterate.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1977z00zzcfa_iteratez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1977z00zzcfa_iteratez00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1977z00zzcfa_iteratez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1977z00zzcfa_iteratez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1977z00zzcfa_iteratez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1977z00zzcfa_iteratez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1977z00zzcfa_iteratez00));
			BGl_modulezd2initializa7ationz75zzast_unitz00(234044111L,
				BSTRING_TO_STRING(BGl_string1977z00zzcfa_iteratez00));
			BGl_modulezd2initializa7ationz75zzcfa_cfaz00(400853817L,
				BSTRING_TO_STRING(BGl_string1977z00zzcfa_iteratez00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string1977z00zzcfa_iteratez00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string1977z00zzcfa_iteratez00));
			BGl_modulezd2initializa7ationz75zzcfa_loosez00(471177480L,
				BSTRING_TO_STRING(BGl_string1977z00zzcfa_iteratez00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string1977z00zzcfa_iteratez00));
		}

	}

#ifdef __cplusplus
}
#endif
