/*===========================================================================*/
/*   (Cfa/struct.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/struct.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_STRUCT_TYPE_DEFINITIONS
#define BGL_CFA_STRUCT_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_prezd2makezd2structzd2appzd2_bgl
	{
		struct BgL_variablez00_bgl *BgL_ownerz00;
	}                                   *BgL_prezd2makezd2structzd2appzd2_bglt;

	typedef struct BgL_prezd2structzd2refzd2appzd2_bgl
	{
	}                                  *BgL_prezd2structzd2refzd2appzd2_bglt;

	typedef struct BgL_prezd2structzd2setz12zd2appzc0_bgl
	{
	}                                    
		*BgL_prezd2structzd2setz12zd2appzc0_bglt;

	typedef struct BgL_makezd2structzd2appz00_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		struct BgL_approxz00_bgl *BgL_valuezd2approxzd2;
		long BgL_lostzd2stampzd2;
		struct BgL_variablez00_bgl *BgL_ownerz00;
		obj_t BgL_stackzd2stampzd2;
	}                             *BgL_makezd2structzd2appz00_bglt;

	typedef struct BgL_structzd2refzd2appz00_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                            *BgL_structzd2refzd2appz00_bglt;

	typedef struct BgL_structzd2setz12zd2appz12_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                               *BgL_structzd2setz12zd2appz12_bglt;


#endif													// BGL_CFA_STRUCT_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_makezd2structzd2appz00zzcfa_info2z00;
	extern obj_t BGl_za2structza2z00zztype_cachez00;
	extern obj_t BGl_structzd2refzd2appz00zzcfa_info2z00;
	static obj_t BGl_objectzd2initzd2zzcfa_structz00(void);
	extern obj_t BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00(obj_t,
		BgL_approxz00_bglt);
	static obj_t BGl_methodzd2initzd2zzcfa_structz00(void);
	extern obj_t BGl_structzd2setz12zd2appz12zzcfa_info2z00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t BGl_z62zc3z04anonymousza31614ze3ze5zzcfa_structz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2prezd2stru1512za2zzcfa_structz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2prezd2stru1514za2zzcfa_structz00(obj_t,
		obj_t);
	extern obj_t BGl_prezd2structzd2refzd2appzd2zzcfa_info2z00;
	extern BgL_approxz00_bglt
		BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_approxz00_bglt);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_structz00(void);
	extern BgL_approxz00_bglt BGl_makezd2emptyzd2approxz00zzcfa_approxz00(void);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31655ze3ze5zzcfa_structz00(obj_t, obj_t);
	extern obj_t BGl_za2cfazd2stampza2zd2zzcfa_iteratez00;
	extern obj_t BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_approxz00_bglt);
	static obj_t BGl_z62loosezd2allocz12zd2makezd2st1522za2zzcfa_structz00(obj_t,
		obj_t);
	extern obj_t BGl_nodezd2setupza2z12z62zzcfa_setupz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_structz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_toplevelzd2initzd2zzcfa_structz00(void);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2makezd2structzd2app1516za2zzcfa_structz00(obj_t, obj_t);
	extern obj_t BGl_prezd2makezd2structzd2appzd2zzcfa_info2z00;
	static obj_t BGl_genericzd2initzd2zzcfa_structz00(void);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2structzd2refzd2app1518za2zzcfa_structz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern BgL_approxz00_bglt BGl_loosez12z12zzcfa_loosez00(BgL_approxz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2structzd2setz12zd2app1520zb0zzcfa_structz00(obj_t, obj_t);
	extern BgL_approxz00_bglt
		BGl_makezd2typezd2alloczd2approxzd2zzcfa_approxz00(BgL_typez00_bglt,
		BgL_nodez00_bglt);
	extern obj_t BGl_prezd2structzd2setz12zd2appzc0zzcfa_info2z00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_structz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_setupz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_cfaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_iteratez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_loosez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_z62nodezd2setupz12zd2prezd2make1510za2zzcfa_structz00(obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzcfa_structz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_structz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_structz00(void);
	extern BgL_approxz00_bglt
		BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_typez00_bglt);
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	extern BgL_approxz00_bglt BGl_cfaz12z12zzcfa_cfaz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_approxzd2setzd2typez12z12zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_typez00_bglt);
	static obj_t __cnst[1];


	extern obj_t BGl_cfaz12zd2envzc0zzcfa_cfaz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1963z00zzcfa_structz00,
		BgL_bgl_za762nodeza7d2setupza71976za7,
		BGl_z62nodezd2setupz12zd2prezd2make1510za2zzcfa_structz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1965z00zzcfa_structz00,
		BgL_bgl_za762nodeza7d2setupza71977za7,
		BGl_z62nodezd2setupz12zd2prezd2stru1512za2zzcfa_structz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1966z00zzcfa_structz00,
		BgL_bgl_za762nodeza7d2setupza71978za7,
		BGl_z62nodezd2setupz12zd2prezd2stru1514za2zzcfa_structz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1967z00zzcfa_structz00,
		BgL_bgl_za762cfaza712za7d2make1979za7,
		BGl_z62cfaz12zd2makezd2structzd2app1516za2zzcfa_structz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1969z00zzcfa_structz00,
		BgL_bgl_za762cfaza712za7d2stru1980za7,
		BGl_z62cfaz12zd2structzd2refzd2app1518za2zzcfa_structz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1970z00zzcfa_structz00,
		BgL_bgl_za762cfaza712za7d2stru1981za7,
		BGl_z62cfaz12zd2structzd2setz12zd2app1520zb0zzcfa_structz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1971z00zzcfa_structz00,
		BgL_bgl_za762looseza7d2alloc1982z00,
		BGl_z62loosezd2allocz12zd2makezd2st1522za2zzcfa_structz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_nodezd2setupz12zd2envz12zzcfa_setupz00;
	   
		 
		DEFINE_STRING(BGl_string1964z00zzcfa_structz00,
		BgL_bgl_string1964za700za7za7c1983za7, "node-setup!", 11);
	      DEFINE_STRING(BGl_string1968z00zzcfa_structz00,
		BgL_bgl_string1968za700za7za7c1984za7, "cfa!::approx", 12);
	      DEFINE_STRING(BGl_string1972z00zzcfa_structz00,
		BgL_bgl_string1972za700za7za7c1985za7, "loose-alloc!", 12);
	      DEFINE_STRING(BGl_string1973z00zzcfa_structz00,
		BgL_bgl_string1973za700za7za7c1986za7, "cfa_struct", 10);
	      DEFINE_STRING(BGl_string1974z00zzcfa_structz00,
		BgL_bgl_string1974za700za7za7c1987za7, "all ", 4);
	extern obj_t BGl_loosezd2allocz12zd2envz12zzcfa_loosez00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_structz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_structz00(long
		BgL_checksumz00_3922, char *BgL_fromz00_3923)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_structz00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_structz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_structz00();
					BGl_libraryzd2moduleszd2initz00zzcfa_structz00();
					BGl_cnstzd2initzd2zzcfa_structz00();
					BGl_importedzd2moduleszd2initz00zzcfa_structz00();
					BGl_methodzd2initzd2zzcfa_structz00();
					return BGl_toplevelzd2initzd2zzcfa_structz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_structz00(void)
	{
		{	/* Cfa/struct.scm 17 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_struct");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_struct");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_struct");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_struct");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cfa_struct");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_struct");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_struct");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_struct");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_structz00(void)
	{
		{	/* Cfa/struct.scm 17 */
			{	/* Cfa/struct.scm 17 */
				obj_t BgL_cportz00_3809;

				{	/* Cfa/struct.scm 17 */
					obj_t BgL_stringz00_3816;

					BgL_stringz00_3816 = BGl_string1974z00zzcfa_structz00;
					{	/* Cfa/struct.scm 17 */
						obj_t BgL_startz00_3817;

						BgL_startz00_3817 = BINT(0L);
						{	/* Cfa/struct.scm 17 */
							obj_t BgL_endz00_3818;

							BgL_endz00_3818 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3816)));
							{	/* Cfa/struct.scm 17 */

								BgL_cportz00_3809 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3816, BgL_startz00_3817, BgL_endz00_3818);
				}}}}
				{
					long BgL_iz00_3810;

					BgL_iz00_3810 = 0L;
				BgL_loopz00_3811:
					if ((BgL_iz00_3810 == -1L))
						{	/* Cfa/struct.scm 17 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/struct.scm 17 */
							{	/* Cfa/struct.scm 17 */
								obj_t BgL_arg1975z00_3812;

								{	/* Cfa/struct.scm 17 */

									{	/* Cfa/struct.scm 17 */
										obj_t BgL_locationz00_3814;

										BgL_locationz00_3814 = BBOOL(((bool_t) 0));
										{	/* Cfa/struct.scm 17 */

											BgL_arg1975z00_3812 =
												BGl_readz00zz__readerz00(BgL_cportz00_3809,
												BgL_locationz00_3814);
										}
									}
								}
								{	/* Cfa/struct.scm 17 */
									int BgL_tmpz00_3950;

									BgL_tmpz00_3950 = (int) (BgL_iz00_3810);
									CNST_TABLE_SET(BgL_tmpz00_3950, BgL_arg1975z00_3812);
							}}
							{	/* Cfa/struct.scm 17 */
								int BgL_auxz00_3815;

								BgL_auxz00_3815 = (int) ((BgL_iz00_3810 - 1L));
								{
									long BgL_iz00_3955;

									BgL_iz00_3955 = (long) (BgL_auxz00_3815);
									BgL_iz00_3810 = BgL_iz00_3955;
									goto BgL_loopz00_3811;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_structz00(void)
	{
		{	/* Cfa/struct.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_structz00(void)
	{
		{	/* Cfa/struct.scm 17 */
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_structz00(void)
	{
		{	/* Cfa/struct.scm 17 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_structz00(void)
	{
		{	/* Cfa/struct.scm 17 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_structz00(void)
	{
		{	/* Cfa/struct.scm 17 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_prezd2makezd2structzd2appzd2zzcfa_info2z00,
				BGl_proc1963z00zzcfa_structz00, BGl_string1964z00zzcfa_structz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_prezd2structzd2refzd2appzd2zzcfa_info2z00,
				BGl_proc1965z00zzcfa_structz00, BGl_string1964z00zzcfa_structz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_prezd2structzd2setz12zd2appzc0zzcfa_info2z00,
				BGl_proc1966z00zzcfa_structz00, BGl_string1964z00zzcfa_structz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_makezd2structzd2appz00zzcfa_info2z00,
				BGl_proc1967z00zzcfa_structz00, BGl_string1968z00zzcfa_structz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_structzd2refzd2appz00zzcfa_info2z00, BGl_proc1969z00zzcfa_structz00,
				BGl_string1968z00zzcfa_structz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_structzd2setz12zd2appz12zzcfa_info2z00,
				BGl_proc1970z00zzcfa_structz00, BGl_string1968z00zzcfa_structz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_loosezd2allocz12zd2envz12zzcfa_loosez00,
				BGl_makezd2structzd2appz00zzcfa_info2z00,
				BGl_proc1971z00zzcfa_structz00, BGl_string1972z00zzcfa_structz00);
		}

	}



/* &loose-alloc!-make-st1522 */
	obj_t BGl_z62loosezd2allocz12zd2makezd2st1522za2zzcfa_structz00(obj_t
		BgL_envz00_3785, obj_t BgL_allocz00_3786)
	{
		{	/* Cfa/struct.scm 136 */
			{	/* Cfa/struct.scm 138 */
				bool_t BgL_test1990z00_3965;

				{	/* Cfa/struct.scm 138 */
					long BgL_arg1700z00_3821;

					{
						BgL_makezd2structzd2appz00_bglt BgL_auxz00_3966;

						{
							obj_t BgL_auxz00_3967;

							{	/* Cfa/struct.scm 138 */
								BgL_objectz00_bglt BgL_tmpz00_3968;

								BgL_tmpz00_3968 =
									((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_allocz00_3786));
								BgL_auxz00_3967 = BGL_OBJECT_WIDENING(BgL_tmpz00_3968);
							}
							BgL_auxz00_3966 =
								((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_3967);
						}
						BgL_arg1700z00_3821 =
							(((BgL_makezd2structzd2appz00_bglt) COBJECT(BgL_auxz00_3966))->
							BgL_lostzd2stampzd2);
					}
					BgL_test1990z00_3965 =
						(BgL_arg1700z00_3821 ==
						(long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00));
				}
				if (BgL_test1990z00_3965)
					{	/* Cfa/struct.scm 138 */
						return BUNSPEC;
					}
				else
					{	/* Cfa/struct.scm 138 */
						{
							BgL_makezd2structzd2appz00_bglt BgL_auxz00_3976;

							{
								obj_t BgL_auxz00_3977;

								{	/* Cfa/struct.scm 142 */
									BgL_objectz00_bglt BgL_tmpz00_3978;

									BgL_tmpz00_3978 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) BgL_allocz00_3786));
									BgL_auxz00_3977 = BGL_OBJECT_WIDENING(BgL_tmpz00_3978);
								}
								BgL_auxz00_3976 =
									((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_3977);
							}
							((((BgL_makezd2structzd2appz00_bglt) COBJECT(BgL_auxz00_3976))->
									BgL_lostzd2stampzd2) =
								((long) (long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00)),
								BUNSPEC);
						}
						{	/* Cfa/struct.scm 143 */
							BgL_approxz00_bglt BgL_arg1691z00_3822;

							{
								BgL_makezd2structzd2appz00_bglt BgL_auxz00_3985;

								{
									obj_t BgL_auxz00_3986;

									{	/* Cfa/struct.scm 143 */
										BgL_objectz00_bglt BgL_tmpz00_3987;

										BgL_tmpz00_3987 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_allocz00_3786));
										BgL_auxz00_3986 = BGL_OBJECT_WIDENING(BgL_tmpz00_3987);
									}
									BgL_auxz00_3985 =
										((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_3986);
								}
								BgL_arg1691z00_3822 =
									(((BgL_makezd2structzd2appz00_bglt)
										COBJECT(BgL_auxz00_3985))->BgL_valuezd2approxzd2);
							}
							BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
								(BGl_loosezd2allocz12zd2envz12zzcfa_loosez00,
								BgL_arg1691z00_3822);
						}
						{	/* Cfa/struct.scm 144 */
							BgL_approxz00_bglt BgL_arg1692z00_3823;

							{
								BgL_makezd2structzd2appz00_bglt BgL_auxz00_3994;

								{
									obj_t BgL_auxz00_3995;

									{	/* Cfa/struct.scm 144 */
										BgL_objectz00_bglt BgL_tmpz00_3996;

										BgL_tmpz00_3996 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_allocz00_3786));
										BgL_auxz00_3995 = BGL_OBJECT_WIDENING(BgL_tmpz00_3996);
									}
									BgL_auxz00_3994 =
										((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_3995);
								}
								BgL_arg1692z00_3823 =
									(((BgL_makezd2structzd2appz00_bglt)
										COBJECT(BgL_auxz00_3994))->BgL_valuezd2approxzd2);
							}
							BGl_approxzd2setzd2typez12z12zzcfa_approxz00(BgL_arg1692z00_3823,
								((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
						}
						{	/* Cfa/struct.scm 145 */
							BgL_approxz00_bglt BgL_arg1699z00_3824;

							{
								BgL_makezd2structzd2appz00_bglt BgL_auxz00_4004;

								{
									obj_t BgL_auxz00_4005;

									{	/* Cfa/struct.scm 145 */
										BgL_objectz00_bglt BgL_tmpz00_4006;

										BgL_tmpz00_4006 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_allocz00_3786));
										BgL_auxz00_4005 = BGL_OBJECT_WIDENING(BgL_tmpz00_4006);
									}
									BgL_auxz00_4004 =
										((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_4005);
								}
								BgL_arg1699z00_3824 =
									(((BgL_makezd2structzd2appz00_bglt)
										COBJECT(BgL_auxz00_4004))->BgL_valuezd2approxzd2);
							}
							return
								BGl_approxzd2setzd2topz12z12zzcfa_approxz00
								(BgL_arg1699z00_3824);
						}
					}
			}
		}

	}



/* &cfa!-struct-set!-app1520 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2structzd2setz12zd2app1520zb0zzcfa_structz00(obj_t
		BgL_envz00_3787, obj_t BgL_nodez00_3788)
	{
		{	/* Cfa/struct.scm 109 */
			{	/* Cfa/struct.scm 113 */
				obj_t BgL_arg1646z00_3826;

				{	/* Cfa/struct.scm 113 */
					obj_t BgL_pairz00_3827;

					BgL_pairz00_3827 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_3788))))->BgL_argsz00);
					BgL_arg1646z00_3826 = CAR(CDR(BgL_pairz00_3827));
				}
				BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1646z00_3826));
			}
			{	/* Cfa/struct.scm 114 */
				BgL_approxz00_bglt BgL_structzd2approxzd2_3828;
				BgL_approxz00_bglt BgL_valzd2approxzd2_3829;

				{	/* Cfa/struct.scm 114 */
					obj_t BgL_arg1663z00_3830;

					{	/* Cfa/struct.scm 114 */
						obj_t BgL_pairz00_3831;

						BgL_pairz00_3831 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_3788))))->BgL_argsz00);
						BgL_arg1663z00_3830 = CAR(BgL_pairz00_3831);
					}
					BgL_structzd2approxzd2_3828 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1663z00_3830));
				}
				{	/* Cfa/struct.scm 115 */
					obj_t BgL_arg1678z00_3832;

					{	/* Cfa/struct.scm 115 */
						obj_t BgL_pairz00_3833;

						BgL_pairz00_3833 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_3788))))->BgL_argsz00);
						BgL_arg1678z00_3832 = CAR(CDR(CDR(BgL_pairz00_3833)));
					}
					BgL_valzd2approxzd2_3829 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1678z00_3832));
				}
				if (
					(((BgL_approxz00_bglt) COBJECT(BgL_structzd2approxzd2_3828))->
						BgL_topzf3zf3))
					{	/* Cfa/struct.scm 118 */
						((obj_t)
							BGl_loosez12z12zzcfa_loosez00(BgL_valzd2approxzd2_3829,
								CNST_TABLE_REF(0)));
					}
				else
					{	/* Cfa/struct.scm 123 */
						obj_t BgL_zc3z04anonymousza31655ze3z87_3834;

						BgL_zc3z04anonymousza31655ze3z87_3834 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31655ze3ze5zzcfa_structz00, (int) (1L),
							(int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31655ze3z87_3834, (int) (0L),
							((obj_t) BgL_valzd2approxzd2_3829));
						BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
							(BgL_zc3z04anonymousza31655ze3z87_3834,
							BgL_structzd2approxzd2_3828);
			}}
			{
				BgL_structzd2setz12zd2appz12_bglt BgL_auxz00_4046;

				{
					obj_t BgL_auxz00_4047;

					{	/* Cfa/struct.scm 127 */
						BgL_objectz00_bglt BgL_tmpz00_4048;

						BgL_tmpz00_4048 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3788));
						BgL_auxz00_4047 = BGL_OBJECT_WIDENING(BgL_tmpz00_4048);
					}
					BgL_auxz00_4046 =
						((BgL_structzd2setz12zd2appz12_bglt) BgL_auxz00_4047);
				}
				return
					(((BgL_structzd2setz12zd2appz12_bglt) COBJECT(BgL_auxz00_4046))->
					BgL_approxz00);
			}
		}

	}



/* &<@anonymous:1655> */
	obj_t BGl_z62zc3z04anonymousza31655ze3ze5zzcfa_structz00(obj_t
		BgL_envz00_3789, obj_t BgL_appz00_3791)
	{
		{	/* Cfa/struct.scm 122 */
			{	/* Cfa/struct.scm 123 */
				BgL_approxz00_bglt BgL_valzd2approxzd2_3790;

				BgL_valzd2approxzd2_3790 =
					((BgL_approxz00_bglt) PROCEDURE_REF(BgL_envz00_3789, (int) (0L)));
				{	/* Cfa/struct.scm 123 */
					bool_t BgL_test1992z00_4057;

					{	/* Cfa/struct.scm 123 */
						obj_t BgL_classz00_3835;

						BgL_classz00_3835 = BGl_makezd2structzd2appz00zzcfa_info2z00;
						if (BGL_OBJECTP(BgL_appz00_3791))
							{	/* Cfa/struct.scm 123 */
								BgL_objectz00_bglt BgL_arg1807z00_3836;

								BgL_arg1807z00_3836 = (BgL_objectz00_bglt) (BgL_appz00_3791);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/struct.scm 123 */
										long BgL_idxz00_3837;

										BgL_idxz00_3837 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3836);
										BgL_test1992z00_4057 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3837 + 4L)) == BgL_classz00_3835);
									}
								else
									{	/* Cfa/struct.scm 123 */
										bool_t BgL_res1958z00_3840;

										{	/* Cfa/struct.scm 123 */
											obj_t BgL_oclassz00_3841;

											{	/* Cfa/struct.scm 123 */
												obj_t BgL_arg1815z00_3842;
												long BgL_arg1816z00_3843;

												BgL_arg1815z00_3842 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/struct.scm 123 */
													long BgL_arg1817z00_3844;

													BgL_arg1817z00_3844 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3836);
													BgL_arg1816z00_3843 =
														(BgL_arg1817z00_3844 - OBJECT_TYPE);
												}
												BgL_oclassz00_3841 =
													VECTOR_REF(BgL_arg1815z00_3842, BgL_arg1816z00_3843);
											}
											{	/* Cfa/struct.scm 123 */
												bool_t BgL__ortest_1115z00_3845;

												BgL__ortest_1115z00_3845 =
													(BgL_classz00_3835 == BgL_oclassz00_3841);
												if (BgL__ortest_1115z00_3845)
													{	/* Cfa/struct.scm 123 */
														BgL_res1958z00_3840 = BgL__ortest_1115z00_3845;
													}
												else
													{	/* Cfa/struct.scm 123 */
														long BgL_odepthz00_3846;

														{	/* Cfa/struct.scm 123 */
															obj_t BgL_arg1804z00_3847;

															BgL_arg1804z00_3847 = (BgL_oclassz00_3841);
															BgL_odepthz00_3846 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3847);
														}
														if ((4L < BgL_odepthz00_3846))
															{	/* Cfa/struct.scm 123 */
																obj_t BgL_arg1802z00_3848;

																{	/* Cfa/struct.scm 123 */
																	obj_t BgL_arg1803z00_3849;

																	BgL_arg1803z00_3849 = (BgL_oclassz00_3841);
																	BgL_arg1802z00_3848 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3849,
																		4L);
																}
																BgL_res1958z00_3840 =
																	(BgL_arg1802z00_3848 == BgL_classz00_3835);
															}
														else
															{	/* Cfa/struct.scm 123 */
																BgL_res1958z00_3840 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1992z00_4057 = BgL_res1958z00_3840;
									}
							}
						else
							{	/* Cfa/struct.scm 123 */
								BgL_test1992z00_4057 = ((bool_t) 0);
							}
					}
					if (BgL_test1992z00_4057)
						{	/* Cfa/struct.scm 125 */
							BgL_approxz00_bglt BgL_arg1661z00_3850;

							{
								BgL_makezd2structzd2appz00_bglt BgL_auxz00_4080;

								{
									obj_t BgL_auxz00_4081;

									{	/* Cfa/struct.scm 125 */
										BgL_objectz00_bglt BgL_tmpz00_4082;

										BgL_tmpz00_4082 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_appz00_3791));
										BgL_auxz00_4081 = BGL_OBJECT_WIDENING(BgL_tmpz00_4082);
									}
									BgL_auxz00_4080 =
										((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_4081);
								}
								BgL_arg1661z00_3850 =
									(((BgL_makezd2structzd2appz00_bglt)
										COBJECT(BgL_auxz00_4080))->BgL_valuezd2approxzd2);
							}
							return
								((obj_t)
								BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1661z00_3850,
									BgL_valzd2approxzd2_3790));
						}
					else
						{	/* Cfa/struct.scm 123 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &cfa!-struct-ref-app1518 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2structzd2refzd2app1518za2zzcfa_structz00(obj_t
		BgL_envz00_3792, obj_t BgL_nodez00_3793)
	{
		{	/* Cfa/struct.scm 87 */
			{	/* Cfa/struct.scm 91 */
				obj_t BgL_arg1606z00_3852;

				{	/* Cfa/struct.scm 91 */
					obj_t BgL_pairz00_3853;

					BgL_pairz00_3853 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_3793))))->BgL_argsz00);
					BgL_arg1606z00_3852 = CAR(CDR(BgL_pairz00_3853));
				}
				BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1606z00_3852));
			}
			{	/* Cfa/struct.scm 92 */
				BgL_approxz00_bglt BgL_structzd2approxzd2_3854;

				{	/* Cfa/struct.scm 92 */
					obj_t BgL_arg1630z00_3855;

					{	/* Cfa/struct.scm 92 */
						obj_t BgL_pairz00_3856;

						BgL_pairz00_3856 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_3793))))->BgL_argsz00);
						BgL_arg1630z00_3855 = CAR(BgL_pairz00_3856);
					}
					BgL_structzd2approxzd2_3854 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1630z00_3855));
				}
				if (
					(((BgL_approxz00_bglt) COBJECT(BgL_structzd2approxzd2_3854))->
						BgL_topzf3zf3))
					{	/* Cfa/struct.scm 96 */
						BgL_approxz00_bglt BgL_arg1611z00_3857;

						{
							BgL_structzd2refzd2appz00_bglt BgL_auxz00_4105;

							{
								obj_t BgL_auxz00_4106;

								{	/* Cfa/struct.scm 96 */
									BgL_objectz00_bglt BgL_tmpz00_4107;

									BgL_tmpz00_4107 =
										((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3793));
									BgL_auxz00_4106 = BGL_OBJECT_WIDENING(BgL_tmpz00_4107);
								}
								BgL_auxz00_4105 =
									((BgL_structzd2refzd2appz00_bglt) BgL_auxz00_4106);
							}
							BgL_arg1611z00_3857 =
								(((BgL_structzd2refzd2appz00_bglt) COBJECT(BgL_auxz00_4105))->
								BgL_approxz00);
						}
						BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_arg1611z00_3857);
					}
				else
					{	/* Cfa/struct.scm 95 */
						BFALSE;
					}
				{	/* Cfa/struct.scm 99 */
					obj_t BgL_zc3z04anonymousza31614ze3z87_3858;

					BgL_zc3z04anonymousza31614ze3z87_3858 =
						MAKE_FX_PROCEDURE
						(BGl_z62zc3z04anonymousza31614ze3ze5zzcfa_structz00, (int) (1L),
						(int) (1L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31614ze3z87_3858, (int) (0L),
						((obj_t) ((BgL_appz00_bglt) BgL_nodez00_3793)));
					BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
						(BgL_zc3z04anonymousza31614ze3z87_3858,
						BgL_structzd2approxzd2_3854);
			}}
			{
				BgL_structzd2refzd2appz00_bglt BgL_auxz00_4122;

				{
					obj_t BgL_auxz00_4123;

					{	/* Cfa/struct.scm 104 */
						BgL_objectz00_bglt BgL_tmpz00_4124;

						BgL_tmpz00_4124 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3793));
						BgL_auxz00_4123 = BGL_OBJECT_WIDENING(BgL_tmpz00_4124);
					}
					BgL_auxz00_4122 = ((BgL_structzd2refzd2appz00_bglt) BgL_auxz00_4123);
				}
				return
					(((BgL_structzd2refzd2appz00_bglt) COBJECT(BgL_auxz00_4122))->
					BgL_approxz00);
			}
		}

	}



/* &<@anonymous:1614> */
	obj_t BGl_z62zc3z04anonymousza31614ze3ze5zzcfa_structz00(obj_t
		BgL_envz00_3794, obj_t BgL_appz00_3796)
	{
		{	/* Cfa/struct.scm 98 */
			{	/* Cfa/struct.scm 99 */
				BgL_appz00_bglt BgL_i1181z00_3795;

				BgL_i1181z00_3795 =
					((BgL_appz00_bglt) PROCEDURE_REF(BgL_envz00_3794, (int) (0L)));
				{	/* Cfa/struct.scm 99 */
					bool_t BgL_test1998z00_4133;

					{	/* Cfa/struct.scm 99 */
						obj_t BgL_classz00_3859;

						BgL_classz00_3859 = BGl_makezd2structzd2appz00zzcfa_info2z00;
						if (BGL_OBJECTP(BgL_appz00_3796))
							{	/* Cfa/struct.scm 99 */
								BgL_objectz00_bglt BgL_arg1807z00_3860;

								BgL_arg1807z00_3860 = (BgL_objectz00_bglt) (BgL_appz00_3796);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/struct.scm 99 */
										long BgL_idxz00_3861;

										BgL_idxz00_3861 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3860);
										BgL_test1998z00_4133 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3861 + 4L)) == BgL_classz00_3859);
									}
								else
									{	/* Cfa/struct.scm 99 */
										bool_t BgL_res1957z00_3864;

										{	/* Cfa/struct.scm 99 */
											obj_t BgL_oclassz00_3865;

											{	/* Cfa/struct.scm 99 */
												obj_t BgL_arg1815z00_3866;
												long BgL_arg1816z00_3867;

												BgL_arg1815z00_3866 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/struct.scm 99 */
													long BgL_arg1817z00_3868;

													BgL_arg1817z00_3868 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3860);
													BgL_arg1816z00_3867 =
														(BgL_arg1817z00_3868 - OBJECT_TYPE);
												}
												BgL_oclassz00_3865 =
													VECTOR_REF(BgL_arg1815z00_3866, BgL_arg1816z00_3867);
											}
											{	/* Cfa/struct.scm 99 */
												bool_t BgL__ortest_1115z00_3869;

												BgL__ortest_1115z00_3869 =
													(BgL_classz00_3859 == BgL_oclassz00_3865);
												if (BgL__ortest_1115z00_3869)
													{	/* Cfa/struct.scm 99 */
														BgL_res1957z00_3864 = BgL__ortest_1115z00_3869;
													}
												else
													{	/* Cfa/struct.scm 99 */
														long BgL_odepthz00_3870;

														{	/* Cfa/struct.scm 99 */
															obj_t BgL_arg1804z00_3871;

															BgL_arg1804z00_3871 = (BgL_oclassz00_3865);
															BgL_odepthz00_3870 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3871);
														}
														if ((4L < BgL_odepthz00_3870))
															{	/* Cfa/struct.scm 99 */
																obj_t BgL_arg1802z00_3872;

																{	/* Cfa/struct.scm 99 */
																	obj_t BgL_arg1803z00_3873;

																	BgL_arg1803z00_3873 = (BgL_oclassz00_3865);
																	BgL_arg1802z00_3872 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3873,
																		4L);
																}
																BgL_res1957z00_3864 =
																	(BgL_arg1802z00_3872 == BgL_classz00_3859);
															}
														else
															{	/* Cfa/struct.scm 99 */
																BgL_res1957z00_3864 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1998z00_4133 = BgL_res1957z00_3864;
									}
							}
						else
							{	/* Cfa/struct.scm 99 */
								BgL_test1998z00_4133 = ((bool_t) 0);
							}
					}
					if (BgL_test1998z00_4133)
						{	/* Cfa/struct.scm 99 */
							{	/* Cfa/struct.scm 101 */
								BgL_approxz00_bglt BgL_arg1616z00_3874;
								BgL_approxz00_bglt BgL_arg1625z00_3875;

								{
									BgL_structzd2refzd2appz00_bglt BgL_auxz00_4156;

									{
										obj_t BgL_auxz00_4157;

										{	/* Cfa/struct.scm 101 */
											BgL_objectz00_bglt BgL_tmpz00_4158;

											BgL_tmpz00_4158 =
												((BgL_objectz00_bglt) BgL_i1181z00_3795);
											BgL_auxz00_4157 = BGL_OBJECT_WIDENING(BgL_tmpz00_4158);
										}
										BgL_auxz00_4156 =
											((BgL_structzd2refzd2appz00_bglt) BgL_auxz00_4157);
									}
									BgL_arg1616z00_3874 =
										(((BgL_structzd2refzd2appz00_bglt)
											COBJECT(BgL_auxz00_4156))->BgL_approxz00);
								}
								{
									BgL_makezd2structzd2appz00_bglt BgL_auxz00_4163;

									{
										obj_t BgL_auxz00_4164;

										{	/* Cfa/struct.scm 101 */
											BgL_objectz00_bglt BgL_tmpz00_4165;

											BgL_tmpz00_4165 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_appz00_3796));
											BgL_auxz00_4164 = BGL_OBJECT_WIDENING(BgL_tmpz00_4165);
										}
										BgL_auxz00_4163 =
											((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_4164);
									}
									BgL_arg1625z00_3875 =
										(((BgL_makezd2structzd2appz00_bglt)
											COBJECT(BgL_auxz00_4163))->BgL_valuezd2approxzd2);
								}
								BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1616z00_3874,
									BgL_arg1625z00_3875);
							}
							{	/* Cfa/struct.scm 102 */
								BgL_approxz00_bglt BgL_arg1626z00_3876;
								BgL_typez00_bglt BgL_arg1627z00_3877;

								{
									BgL_makezd2structzd2appz00_bglt BgL_auxz00_4172;

									{
										obj_t BgL_auxz00_4173;

										{	/* Cfa/struct.scm 102 */
											BgL_objectz00_bglt BgL_tmpz00_4174;

											BgL_tmpz00_4174 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_appz00_3796));
											BgL_auxz00_4173 = BGL_OBJECT_WIDENING(BgL_tmpz00_4174);
										}
										BgL_auxz00_4172 =
											((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_4173);
									}
									BgL_arg1626z00_3876 =
										(((BgL_makezd2structzd2appz00_bglt)
											COBJECT(BgL_auxz00_4172))->BgL_valuezd2approxzd2);
								}
								{	/* Cfa/struct.scm 102 */
									BgL_approxz00_bglt BgL_arg1629z00_3878;

									{
										BgL_structzd2refzd2appz00_bglt BgL_auxz00_4180;

										{
											obj_t BgL_auxz00_4181;

											{	/* Cfa/struct.scm 102 */
												BgL_objectz00_bglt BgL_tmpz00_4182;

												BgL_tmpz00_4182 =
													((BgL_objectz00_bglt) BgL_i1181z00_3795);
												BgL_auxz00_4181 = BGL_OBJECT_WIDENING(BgL_tmpz00_4182);
											}
											BgL_auxz00_4180 =
												((BgL_structzd2refzd2appz00_bglt) BgL_auxz00_4181);
										}
										BgL_arg1629z00_3878 =
											(((BgL_structzd2refzd2appz00_bglt)
												COBJECT(BgL_auxz00_4180))->BgL_approxz00);
									}
									BgL_arg1627z00_3877 =
										(((BgL_approxz00_bglt) COBJECT(BgL_arg1629z00_3878))->
										BgL_typez00);
								}
								return
									BGl_approxzd2setzd2typez12z12zzcfa_approxz00
									(BgL_arg1626z00_3876, BgL_arg1627z00_3877);
							}
						}
					else
						{	/* Cfa/struct.scm 99 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &cfa!-make-struct-app1516 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2makezd2structzd2app1516za2zzcfa_structz00(obj_t
		BgL_envz00_3797, obj_t BgL_nodez00_3798)
	{
		{	/* Cfa/struct.scm 71 */
			{	/* Cfa/struct.scm 74 */
				obj_t BgL_arg1593z00_3880;

				{	/* Cfa/struct.scm 74 */
					obj_t BgL_pairz00_3881;

					BgL_pairz00_3881 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_3798))))->BgL_argsz00);
					BgL_arg1593z00_3880 = CAR(BgL_pairz00_3881);
				}
				BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1593z00_3880));
			}
			{	/* Cfa/struct.scm 75 */
				BgL_approxz00_bglt BgL_initzd2valuezd2approxz00_3882;

				{	/* Cfa/struct.scm 75 */
					obj_t BgL_arg1602z00_3883;

					{	/* Cfa/struct.scm 75 */
						obj_t BgL_pairz00_3884;

						BgL_pairz00_3884 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_3798))))->BgL_argsz00);
						BgL_arg1602z00_3883 = CAR(CDR(BgL_pairz00_3884));
					}
					BgL_initzd2valuezd2approxz00_3882 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1602z00_3883));
				}
				{	/* Cfa/struct.scm 76 */
					BgL_approxz00_bglt BgL_arg1595z00_3885;

					{
						BgL_makezd2structzd2appz00_bglt BgL_auxz00_4202;

						{
							obj_t BgL_auxz00_4203;

							{	/* Cfa/struct.scm 76 */
								BgL_objectz00_bglt BgL_tmpz00_4204;

								BgL_tmpz00_4204 =
									((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3798));
								BgL_auxz00_4203 = BGL_OBJECT_WIDENING(BgL_tmpz00_4204);
							}
							BgL_auxz00_4202 =
								((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_4203);
						}
						BgL_arg1595z00_3885 =
							(((BgL_makezd2structzd2appz00_bglt) COBJECT(BgL_auxz00_4202))->
							BgL_valuezd2approxzd2);
					}
					BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1595z00_3885,
						BgL_initzd2valuezd2approxz00_3882);
				}
				{
					BgL_makezd2structzd2appz00_bglt BgL_auxz00_4211;

					{
						obj_t BgL_auxz00_4212;

						{	/* Cfa/struct.scm 77 */
							BgL_objectz00_bglt BgL_tmpz00_4213;

							BgL_tmpz00_4213 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3798));
							BgL_auxz00_4212 = BGL_OBJECT_WIDENING(BgL_tmpz00_4213);
						}
						BgL_auxz00_4211 =
							((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_4212);
					}
					return
						(((BgL_makezd2structzd2appz00_bglt) COBJECT(BgL_auxz00_4211))->
						BgL_approxz00);
				}
			}
		}

	}



/* &node-setup!-pre-stru1514 */
	obj_t BGl_z62nodezd2setupz12zd2prezd2stru1514za2zzcfa_structz00(obj_t
		BgL_envz00_3799, obj_t BgL_nodez00_3800)
	{
		{	/* Cfa/struct.scm 62 */
			{	/* Cfa/struct.scm 64 */
				obj_t BgL_arg1576z00_3887;

				BgL_arg1576z00_3887 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_3800))))->BgL_argsz00);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1576z00_3887);
			}
			{	/* Cfa/struct.scm 65 */
				BgL_appz00_bglt BgL_nodez00_3888;

				{	/* Cfa/struct.scm 65 */
					long BgL_arg1585z00_3889;

					{	/* Cfa/struct.scm 65 */
						obj_t BgL_arg1589z00_3890;

						{	/* Cfa/struct.scm 65 */
							obj_t BgL_arg1591z00_3891;

							{	/* Cfa/struct.scm 65 */
								obj_t BgL_arg1815z00_3892;
								long BgL_arg1816z00_3893;

								BgL_arg1815z00_3892 = (BGl_za2classesza2z00zz__objectz00);
								{	/* Cfa/struct.scm 65 */
									long BgL_arg1817z00_3894;

									BgL_arg1817z00_3894 =
										BGL_OBJECT_CLASS_NUM(
										((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_3800)));
									BgL_arg1816z00_3893 = (BgL_arg1817z00_3894 - OBJECT_TYPE);
								}
								BgL_arg1591z00_3891 =
									VECTOR_REF(BgL_arg1815z00_3892, BgL_arg1816z00_3893);
							}
							BgL_arg1589z00_3890 =
								BGl_classzd2superzd2zz__objectz00(BgL_arg1591z00_3891);
						}
						{	/* Cfa/struct.scm 65 */
							obj_t BgL_tmpz00_4230;

							BgL_tmpz00_4230 = ((obj_t) BgL_arg1589z00_3890);
							BgL_arg1585z00_3889 = BGL_CLASS_NUM(BgL_tmpz00_4230);
					}}
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_appz00_bglt) BgL_nodez00_3800)), BgL_arg1585z00_3889);
				}
				{	/* Cfa/struct.scm 65 */
					BgL_objectz00_bglt BgL_tmpz00_4236;

					BgL_tmpz00_4236 =
						((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3800));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4236, BFALSE);
				}
				((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3800));
				BgL_nodez00_3888 = ((BgL_appz00_bglt) BgL_nodez00_3800);
				{	/* Cfa/struct.scm 66 */
					BgL_structzd2setz12zd2appz12_bglt BgL_wide1178z00_3895;

					BgL_wide1178z00_3895 =
						((BgL_structzd2setz12zd2appz12_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_structzd2setz12zd2appz12_bgl))));
					{	/* Cfa/struct.scm 66 */
						obj_t BgL_auxz00_4248;
						BgL_objectz00_bglt BgL_tmpz00_4244;

						BgL_auxz00_4248 = ((obj_t) BgL_wide1178z00_3895);
						BgL_tmpz00_4244 =
							((BgL_objectz00_bglt)
							((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3888)));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4244, BgL_auxz00_4248);
					}
					((BgL_objectz00_bglt)
						((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3888)));
					{	/* Cfa/struct.scm 66 */
						long BgL_arg1584z00_3896;

						BgL_arg1584z00_3896 =
							BGL_CLASS_NUM(BGl_structzd2setz12zd2appz12zzcfa_info2z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_appz00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_3888))), BgL_arg1584z00_3896);
					}
					((BgL_appz00_bglt)
						((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3888)));
				}
				{
					BgL_structzd2setz12zd2appz12_bglt BgL_auxz00_4262;

					{
						obj_t BgL_auxz00_4263;

						{	/* Cfa/struct.scm 66 */
							BgL_objectz00_bglt BgL_tmpz00_4264;

							BgL_tmpz00_4264 =
								((BgL_objectz00_bglt)
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3888)));
							BgL_auxz00_4263 = BGL_OBJECT_WIDENING(BgL_tmpz00_4264);
						}
						BgL_auxz00_4262 =
							((BgL_structzd2setz12zd2appz12_bglt) BgL_auxz00_4263);
					}
					((((BgL_structzd2setz12zd2appz12_bglt) COBJECT(BgL_auxz00_4262))->
							BgL_approxz00) =
						((BgL_approxz00_bglt)
							BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
									BGl_za2unspecza2z00zztype_cachez00))), BUNSPEC);
				}
				return
					((obj_t) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3888)));
			}
		}

	}



/* &node-setup!-pre-stru1512 */
	obj_t BGl_z62nodezd2setupz12zd2prezd2stru1512za2zzcfa_structz00(obj_t
		BgL_envz00_3801, obj_t BgL_nodez00_3802)
	{
		{	/* Cfa/struct.scm 53 */
			{	/* Cfa/struct.scm 55 */
				obj_t BgL_arg1564z00_3898;

				BgL_arg1564z00_3898 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_3802))))->BgL_argsz00);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1564z00_3898);
			}
			{	/* Cfa/struct.scm 56 */
				BgL_appz00_bglt BgL_nodez00_3899;

				{	/* Cfa/struct.scm 56 */
					long BgL_arg1571z00_3900;

					{	/* Cfa/struct.scm 56 */
						obj_t BgL_arg1573z00_3901;

						{	/* Cfa/struct.scm 56 */
							obj_t BgL_arg1575z00_3902;

							{	/* Cfa/struct.scm 56 */
								obj_t BgL_arg1815z00_3903;
								long BgL_arg1816z00_3904;

								BgL_arg1815z00_3903 = (BGl_za2classesza2z00zz__objectz00);
								{	/* Cfa/struct.scm 56 */
									long BgL_arg1817z00_3905;

									BgL_arg1817z00_3905 =
										BGL_OBJECT_CLASS_NUM(
										((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_3802)));
									BgL_arg1816z00_3904 = (BgL_arg1817z00_3905 - OBJECT_TYPE);
								}
								BgL_arg1575z00_3902 =
									VECTOR_REF(BgL_arg1815z00_3903, BgL_arg1816z00_3904);
							}
							BgL_arg1573z00_3901 =
								BGl_classzd2superzd2zz__objectz00(BgL_arg1575z00_3902);
						}
						{	/* Cfa/struct.scm 56 */
							obj_t BgL_tmpz00_4287;

							BgL_tmpz00_4287 = ((obj_t) BgL_arg1573z00_3901);
							BgL_arg1571z00_3900 = BGL_CLASS_NUM(BgL_tmpz00_4287);
					}}
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_appz00_bglt) BgL_nodez00_3802)), BgL_arg1571z00_3900);
				}
				{	/* Cfa/struct.scm 56 */
					BgL_objectz00_bglt BgL_tmpz00_4293;

					BgL_tmpz00_4293 =
						((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3802));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4293, BFALSE);
				}
				((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3802));
				BgL_nodez00_3899 = ((BgL_appz00_bglt) BgL_nodez00_3802);
				{	/* Cfa/struct.scm 57 */
					BgL_structzd2refzd2appz00_bglt BgL_wide1172z00_3906;

					BgL_wide1172z00_3906 =
						((BgL_structzd2refzd2appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_structzd2refzd2appz00_bgl))));
					{	/* Cfa/struct.scm 57 */
						obj_t BgL_auxz00_4305;
						BgL_objectz00_bglt BgL_tmpz00_4301;

						BgL_auxz00_4305 = ((obj_t) BgL_wide1172z00_3906);
						BgL_tmpz00_4301 =
							((BgL_objectz00_bglt)
							((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3899)));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4301, BgL_auxz00_4305);
					}
					((BgL_objectz00_bglt)
						((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3899)));
					{	/* Cfa/struct.scm 57 */
						long BgL_arg1565z00_3907;

						BgL_arg1565z00_3907 =
							BGL_CLASS_NUM(BGl_structzd2refzd2appz00zzcfa_info2z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_appz00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_3899))), BgL_arg1565z00_3907);
					}
					((BgL_appz00_bglt)
						((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3899)));
				}
				{
					BgL_structzd2refzd2appz00_bglt BgL_auxz00_4319;

					{
						obj_t BgL_auxz00_4320;

						{	/* Cfa/struct.scm 57 */
							BgL_objectz00_bglt BgL_tmpz00_4321;

							BgL_tmpz00_4321 =
								((BgL_objectz00_bglt)
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3899)));
							BgL_auxz00_4320 = BGL_OBJECT_WIDENING(BgL_tmpz00_4321);
						}
						BgL_auxz00_4319 =
							((BgL_structzd2refzd2appz00_bglt) BgL_auxz00_4320);
					}
					((((BgL_structzd2refzd2appz00_bglt) COBJECT(BgL_auxz00_4319))->
							BgL_approxz00) =
						((BgL_approxz00_bglt)
							BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
									BGl_za2objza2z00zztype_cachez00))), BUNSPEC);
				}
				return
					((obj_t) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3899)));
			}
		}

	}



/* &node-setup!-pre-make1510 */
	obj_t BGl_z62nodezd2setupz12zd2prezd2make1510za2zzcfa_structz00(obj_t
		BgL_envz00_3803, obj_t BgL_nodez00_3804)
	{
		{	/* Cfa/struct.scm 36 */
			{	/* Cfa/struct.scm 38 */
				obj_t BgL_arg1544z00_3909;

				BgL_arg1544z00_3909 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_3804))))->BgL_argsz00);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1544z00_3909);
			}
			{	/* Cfa/struct.scm 39 */
				BgL_variablez00_bglt BgL_ownerz00_3910;

				{
					BgL_prezd2makezd2structzd2appzd2_bglt BgL_auxz00_4337;

					{
						obj_t BgL_auxz00_4338;

						{	/* Cfa/struct.scm 39 */
							BgL_objectz00_bglt BgL_tmpz00_4339;

							BgL_tmpz00_4339 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3804));
							BgL_auxz00_4338 = BGL_OBJECT_WIDENING(BgL_tmpz00_4339);
						}
						BgL_auxz00_4337 =
							((BgL_prezd2makezd2structzd2appzd2_bglt) BgL_auxz00_4338);
					}
					BgL_ownerz00_3910 =
						(((BgL_prezd2makezd2structzd2appzd2_bglt)
							COBJECT(BgL_auxz00_4337))->BgL_ownerz00);
				}
				{	/* Cfa/struct.scm 39 */
					BgL_appz00_bglt BgL_nodez00_3911;

					{	/* Cfa/struct.scm 40 */
						long BgL_arg1553z00_3912;

						{	/* Cfa/struct.scm 40 */
							obj_t BgL_arg1559z00_3913;

							{	/* Cfa/struct.scm 40 */
								obj_t BgL_arg1561z00_3914;

								{	/* Cfa/struct.scm 40 */
									obj_t BgL_arg1815z00_3915;
									long BgL_arg1816z00_3916;

									BgL_arg1815z00_3915 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Cfa/struct.scm 40 */
										long BgL_arg1817z00_3917;

										BgL_arg1817z00_3917 =
											BGL_OBJECT_CLASS_NUM(
											((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_3804)));
										BgL_arg1816z00_3916 = (BgL_arg1817z00_3917 - OBJECT_TYPE);
									}
									BgL_arg1561z00_3914 =
										VECTOR_REF(BgL_arg1815z00_3915, BgL_arg1816z00_3916);
								}
								BgL_arg1559z00_3913 =
									BGl_classzd2superzd2zz__objectz00(BgL_arg1561z00_3914);
							}
							{	/* Cfa/struct.scm 40 */
								obj_t BgL_tmpz00_4352;

								BgL_tmpz00_4352 = ((obj_t) BgL_arg1559z00_3913);
								BgL_arg1553z00_3912 = BGL_CLASS_NUM(BgL_tmpz00_4352);
						}}
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_3804)), BgL_arg1553z00_3912);
					}
					{	/* Cfa/struct.scm 40 */
						BgL_objectz00_bglt BgL_tmpz00_4358;

						BgL_tmpz00_4358 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3804));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4358, BFALSE);
					}
					((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3804));
					BgL_nodez00_3911 = ((BgL_appz00_bglt) BgL_nodez00_3804);
					{	/* Cfa/struct.scm 40 */

						{	/* Cfa/struct.scm 41 */
							BgL_appz00_bglt BgL_wnodez00_3918;

							{	/* Cfa/struct.scm 41 */
								BgL_makezd2structzd2appz00_bglt BgL_wide1166z00_3919;

								BgL_wide1166z00_3919 =
									((BgL_makezd2structzd2appz00_bglt)
									BOBJECT(GC_MALLOC(sizeof(struct
												BgL_makezd2structzd2appz00_bgl))));
								{	/* Cfa/struct.scm 41 */
									obj_t BgL_auxz00_4370;
									BgL_objectz00_bglt BgL_tmpz00_4366;

									BgL_auxz00_4370 = ((obj_t) BgL_wide1166z00_3919);
									BgL_tmpz00_4366 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3911)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4366, BgL_auxz00_4370);
								}
								((BgL_objectz00_bglt)
									((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3911)));
								{	/* Cfa/struct.scm 41 */
									long BgL_arg1552z00_3920;

									BgL_arg1552z00_3920 =
										BGL_CLASS_NUM(BGl_makezd2structzd2appz00zzcfa_info2z00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_appz00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_3911))),
										BgL_arg1552z00_3920);
								}
								((BgL_appz00_bglt)
									((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3911)));
							}
							{
								BgL_makezd2structzd2appz00_bglt BgL_auxz00_4384;

								{
									obj_t BgL_auxz00_4385;

									{	/* Cfa/struct.scm 43 */
										BgL_objectz00_bglt BgL_tmpz00_4386;

										BgL_tmpz00_4386 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3911)));
										BgL_auxz00_4385 = BGL_OBJECT_WIDENING(BgL_tmpz00_4386);
									}
									BgL_auxz00_4384 =
										((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_4385);
								}
								((((BgL_makezd2structzd2appz00_bglt) COBJECT(BgL_auxz00_4384))->
										BgL_approxz00) =
									((BgL_approxz00_bglt)
										BGl_makezd2emptyzd2approxz00zzcfa_approxz00()), BUNSPEC);
							}
							{
								BgL_makezd2structzd2appz00_bglt BgL_auxz00_4394;

								{
									obj_t BgL_auxz00_4395;

									{	/* Cfa/struct.scm 44 */
										BgL_objectz00_bglt BgL_tmpz00_4396;

										BgL_tmpz00_4396 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3911)));
										BgL_auxz00_4395 = BGL_OBJECT_WIDENING(BgL_tmpz00_4396);
									}
									BgL_auxz00_4394 =
										((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_4395);
								}
								((((BgL_makezd2structzd2appz00_bglt) COBJECT(BgL_auxz00_4394))->
										BgL_valuezd2approxzd2) =
									((BgL_approxz00_bglt)
										BGl_makezd2emptyzd2approxz00zzcfa_approxz00()), BUNSPEC);
							}
							{
								BgL_makezd2structzd2appz00_bglt BgL_auxz00_4404;

								{
									obj_t BgL_auxz00_4405;

									{	/* Cfa/struct.scm 40 */
										BgL_objectz00_bglt BgL_tmpz00_4406;

										BgL_tmpz00_4406 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3911)));
										BgL_auxz00_4405 = BGL_OBJECT_WIDENING(BgL_tmpz00_4406);
									}
									BgL_auxz00_4404 =
										((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_4405);
								}
								((((BgL_makezd2structzd2appz00_bglt) COBJECT(BgL_auxz00_4404))->
										BgL_lostzd2stampzd2) = ((long) -1L), BUNSPEC);
							}
							{
								BgL_makezd2structzd2appz00_bglt BgL_auxz00_4413;

								{
									obj_t BgL_auxz00_4414;

									{	/* Cfa/struct.scm 42 */
										BgL_objectz00_bglt BgL_tmpz00_4415;

										BgL_tmpz00_4415 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3911)));
										BgL_auxz00_4414 = BGL_OBJECT_WIDENING(BgL_tmpz00_4415);
									}
									BgL_auxz00_4413 =
										((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_4414);
								}
								((((BgL_makezd2structzd2appz00_bglt) COBJECT(BgL_auxz00_4413))->
										BgL_ownerz00) =
									((BgL_variablez00_bglt) BgL_ownerz00_3910), BUNSPEC);
							}
							{
								BgL_makezd2structzd2appz00_bglt BgL_auxz00_4422;

								{
									obj_t BgL_auxz00_4423;

									{	/* Cfa/struct.scm 40 */
										BgL_objectz00_bglt BgL_tmpz00_4424;

										BgL_tmpz00_4424 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3911)));
										BgL_auxz00_4423 = BGL_OBJECT_WIDENING(BgL_tmpz00_4424);
									}
									BgL_auxz00_4422 =
										((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_4423);
								}
								((((BgL_makezd2structzd2appz00_bglt) COBJECT(BgL_auxz00_4422))->
										BgL_stackzd2stampzd2) = ((obj_t) BNIL), BUNSPEC);
							}
							BgL_wnodez00_3918 =
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_3911));
							{	/* Cfa/struct.scm 47 */
								BgL_approxz00_bglt BgL_arg1546z00_3921;

								BgL_arg1546z00_3921 =
									BGl_makezd2typezd2alloczd2approxzd2zzcfa_approxz00(
									((BgL_typez00_bglt) BGl_za2structza2z00zztype_cachez00),
									((BgL_nodez00_bglt) BgL_nodez00_3911));
								{
									BgL_makezd2structzd2appz00_bglt BgL_auxz00_4436;

									{
										obj_t BgL_auxz00_4437;

										{	/* Cfa/struct.scm 46 */
											BgL_objectz00_bglt BgL_tmpz00_4438;

											BgL_tmpz00_4438 =
												((BgL_objectz00_bglt) BgL_wnodez00_3918);
											BgL_auxz00_4437 = BGL_OBJECT_WIDENING(BgL_tmpz00_4438);
										}
										BgL_auxz00_4436 =
											((BgL_makezd2structzd2appz00_bglt) BgL_auxz00_4437);
									}
									return
										((((BgL_makezd2structzd2appz00_bglt)
												COBJECT(BgL_auxz00_4436))->BgL_approxz00) =
										((BgL_approxz00_bglt) BgL_arg1546z00_3921), BUNSPEC);
								}
							}
						}
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_structz00(void)
	{
		{	/* Cfa/struct.scm 17 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1973z00zzcfa_structz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1973z00zzcfa_structz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1973z00zzcfa_structz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1973z00zzcfa_structz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1973z00zzcfa_structz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1973z00zzcfa_structz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1973z00zzcfa_structz00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string1973z00zzcfa_structz00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string1973z00zzcfa_structz00));
			BGl_modulezd2initializa7ationz75zzcfa_loosez00(471177480L,
				BSTRING_TO_STRING(BGl_string1973z00zzcfa_structz00));
			BGl_modulezd2initializa7ationz75zzcfa_iteratez00(131412063L,
				BSTRING_TO_STRING(BGl_string1973z00zzcfa_structz00));
			BGl_modulezd2initializa7ationz75zzcfa_cfaz00(400853817L,
				BSTRING_TO_STRING(BGl_string1973z00zzcfa_structz00));
			BGl_modulezd2initializa7ationz75zzcfa_setupz00(168272122L,
				BSTRING_TO_STRING(BGl_string1973z00zzcfa_structz00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string1973z00zzcfa_structz00));
		}

	}

#ifdef __cplusplus
}
#endif
