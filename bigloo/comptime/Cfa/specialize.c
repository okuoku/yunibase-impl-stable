/*===========================================================================*/
/*   (Cfa/specialize.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/specialize.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_SPECIALIZE_TYPE_DEFINITIONS
#define BGL_CFA_SPECIALIZE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_backendz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_languagez00;
		obj_t BgL_srfi0z00;
		obj_t BgL_namez00;
		obj_t BgL_externzd2variableszd2;
		obj_t BgL_externzd2functionszd2;
		obj_t BgL_externzd2typeszd2;
		obj_t BgL_variablesz00;
		obj_t BgL_functionsz00;
		obj_t BgL_typesz00;
		bool_t BgL_typedz00;
		obj_t BgL_heapzd2suffixzd2;
		obj_t BgL_heapzd2compatiblezd2;
		bool_t BgL_callccz00;
		bool_t BgL_qualifiedzd2typeszd2;
		bool_t BgL_effectzb2zb2;
		bool_t BgL_removezd2emptyzd2letz00;
		bool_t BgL_foreignzd2closurezd2;
		bool_t BgL_typedzd2eqzd2;
		bool_t BgL_tracezd2supportzd2;
		obj_t BgL_foreignzd2clausezd2supportz00;
		obj_t BgL_debugzd2supportzd2;
		bool_t BgL_pragmazd2supportzd2;
		bool_t BgL_tvectorzd2descrzd2supportz00;
		bool_t BgL_requirezd2tailczd2;
		obj_t BgL_registersz00;
		obj_t BgL_pregistersz00;
		bool_t BgL_boundzd2checkzd2;
		bool_t BgL_typezd2checkzd2;
		bool_t BgL_typedzd2funcallzd2;
		bool_t BgL_strictzd2typezd2castz00;
		bool_t BgL_forcezd2registerzd2gczd2rootszd2;
		bool_t BgL_stringzd2literalzd2supportz00;
	}                 *BgL_backendz00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_specializa7edzd2globalz75_bgl
	{
		obj_t BgL_fixz00;
	}                                *BgL_specializa7edzd2globalz75_bglt;


#endif													// BGL_CFA_SPECIALIZE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_za2specializa7eza2za7zzcfa_specializa7eza7 = BUNSPEC;
	static obj_t BGl_z62patchz121319z70zzcfa_specializa7eza7(obj_t, obj_t);
	static BgL_appz00_bglt
		BGl_specializa7ezd2eqzf3z86zzcfa_specializa7eza7(BgL_appz00_bglt);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	extern obj_t BGl_za2intza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_arithmeticzd2operatorzf3z21zzcfa_specializa7eza7(BgL_globalz00_bglt);
	BGL_IMPORT obj_t BGl_classzd2nilzd2initz12z12zz__objectz00(obj_t);
	static obj_t BGl_installzd2specializa7ez12z67zzcfa_specializa7eza7(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_specializa7eza7 = BUNSPEC;
	static obj_t BGl_z62patchz12zd2atom1322za2zzcfa_specializa7eza7(obj_t, obj_t);
	extern obj_t BGl_closurez00zzast_nodez00;
	static BgL_globalz00_bglt BGl_z62lambda1869z62zzcfa_specializa7eza7(obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t,
		obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static BgL_globalz00_bglt BGl_z62lambda1873z62zzcfa_specializa7eza7(obj_t,
		obj_t);
	static obj_t BGl_normaliza7ezd2getzd2typeze70z40zzcfa_specializa7eza7(obj_t);
	static BgL_globalz00_bglt BGl_z62lambda1876z62zzcfa_specializa7eza7(obj_t,
		obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_z62patchz12zd2appzd2ly1334z70zzcfa_specializa7eza7(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static bool_t BGl_patchzd2treez12zc0zzcfa_specializa7eza7(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_warningz00zz__errorz00(obj_t);
	static obj_t BGl_z62lambda1884z62zzcfa_specializa7eza7(obj_t, obj_t);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_z62lambda1885z62zzcfa_specializa7eza7(obj_t, obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzcfa_specializa7eza7(void);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t BGl_z62patchz12zd2conditional1344za2zzcfa_specializa7eza7(obj_t,
		obj_t);
	extern obj_t
		BGl_za2optimzd2cfazd2fixnumzd2arithmeticzf3za2z21zzengine_paramz00;
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31875ze3ze5zzcfa_specializa7eza7(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_assqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_genericzd2initzd2zzcfa_specializa7eza7(void);
	BGL_IMPORT obj_t BGl_makezd2classzd2fieldz00zz__objectz00(obj_t, obj_t, obj_t,
		bool_t, bool_t, obj_t, obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzcfa_specializa7eza7(void);
	static obj_t BGl_z62patchz12zd2extern1338za2zzcfa_specializa7eza7(obj_t,
		obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	extern obj_t BGl_typez00zztype_typez00;
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	static obj_t BGl_z62patchz12zd2switch1348za2zzcfa_specializa7eza7(obj_t,
		obj_t);
	static obj_t BGl_za2boxedzd2eqzf3za2z21zzcfa_specializa7eza7 = BUNSPEC;
	extern obj_t
		BGl_za2optimzd2cfazd2flonumzd2arithmeticzf3za2z21zzengine_paramz00;
	static obj_t BGl_z62patchz12zd2var1326za2zzcfa_specializa7eza7(obj_t, obj_t);
	extern obj_t BGl_za2brealza2z00zztype_cachez00;
	extern obj_t BGl_za2libzd2modeza2zd2zzengine_paramz00;
	extern obj_t BGl_thezd2backendzd2zzbackend_backendz00(void);
	static bool_t BGl_showzd2specializa7ez75zzcfa_specializa7eza7(void);
	static obj_t BGl_za2czd2eqzf3za2z21zzcfa_specializa7eza7 = BUNSPEC;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_appendzd221011zd2zzcfa_specializa7eza7(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcfa_specializa7eza7(void);
	static obj_t
		BGl_z62patchz12zd2jumpzd2exzd2it1356za2zzcfa_specializa7eza7(obj_t, obj_t);
	static obj_t BGl_za2specializa7ationszd2overflowza2z75zzcfa_specializa7eza7 =
		BUNSPEC;
	BGL_EXPORTED_DECL obj_t BGl_specializa7ez12zb5zzcfa_specializa7eza7(obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_getzd2specializa7ationsz75zzcfa_specializa7eza7(void);
	static obj_t BGl_uninstallzd2specializa7esz12z67zzcfa_specializa7eza7(void);
	static obj_t BGl_z62patchz12zd2makezd2box1358z70zzcfa_specializa7eza7(obj_t,
		obj_t);
	static obj_t BGl_z62patchz12zd2boxzd2setz121360z62zzcfa_specializa7eza7(obj_t,
		obj_t);
	extern obj_t BGl_za2realza2z00zztype_cachez00;
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static obj_t BGl_za2specializa7edzd2eqzd2typesza2za7zzcfa_specializa7eza7 =
		BUNSPEC;
	extern obj_t BGl_varz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	static obj_t BGl_addzd2specializa7edzd2typez12zb5zzcfa_specializa7eza7(obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62patchz12zd2letzd2fun1350z70zzcfa_specializa7eza7(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	static obj_t BGl_z62patchz12zd2cast1340za2zzcfa_specializa7eza7(obj_t, obj_t);
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static obj_t
		BGl_za2specializa7ationszd2sanszd2overflowza2za7zzcfa_specializa7eza7 =
		BUNSPEC;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_arithmeticzd2speczd2typesz00zzcfa_specializa7eza7(BgL_globalz00_bglt);
	static obj_t BGl_z62patchz12zd2setq1342za2zzcfa_specializa7eza7(obj_t, obj_t);
	static obj_t BGl_z62patchz12zd2app1364za2zzcfa_specializa7eza7(obj_t, obj_t);
	static bool_t
		BGl_specializa7ezd2optimiza7ationzf3z21zzcfa_specializa7eza7(void);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t BGl_z62patchz12zd2sync1332za2zzcfa_specializa7eza7(obj_t, obj_t);
	static obj_t BGl_z62patchz12zd2fail1346za2zzcfa_specializa7eza7(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzcfa_specializa7eza7(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_inlinez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzbackend_backendz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_patchzd2funz12zc0zzcfa_specializa7eza7(obj_t);
	static obj_t BGl_specializa7edzd2globalz75zzcfa_specializa7eza7 = BUNSPEC;
	extern obj_t BGl_cfunz00zzast_varz00;
	static obj_t BGl_z62specializa7ez12zd7zzcfa_specializa7eza7(obj_t, obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62patchz12zd2sequence1330za2zzcfa_specializa7eza7(obj_t,
		obj_t);
	static obj_t
		BGl_z62patchz12zd2setzd2exzd2it1354za2zzcfa_specializa7eza7(obj_t, obj_t);
	static obj_t BGl_za2specializa7ationsza2za7zzcfa_specializa7eza7 = BUNSPEC;
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t BGl_z62arithmeticzd2speczd2typesz62zzcfa_specializa7eza7(obj_t,
		obj_t);
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_patchz12z12zzcfa_specializa7eza7(BgL_nodez00_bglt);
	static BgL_appz00_bglt
		BGl_specializa7ezd2appz12z67zzcfa_specializa7eza7(BgL_appz00_bglt);
	static obj_t BGl_cnstzd2initzd2zzcfa_specializa7eza7(void);
	static obj_t BGl_patchza2z12zb0zzcfa_specializa7eza7(obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_specializa7eza7(void);
	BGL_IMPORT obj_t BGl_registerzd2classz12zc0zz__objectz00(obj_t, obj_t, obj_t,
		long, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_specializa7eza7(void);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_specializa7eza7(void);
	extern obj_t BGl_findzd2globalzf2modulez20zzast_envz00(obj_t, obj_t);
	static obj_t
		BGl_installzd2specializa7edzd2typez12zb5zzcfa_specializa7eza7(obj_t);
	static obj_t BGl_z62patchz12zd2funcall1336za2zzcfa_specializa7eza7(obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_valuez00zzast_varz00;
	extern obj_t BGl_za2arithmeticzd2overflowza2zd2zzengine_paramz00;
	extern obj_t BGl_za2bintza2z00zztype_cachez00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62patchz12zd2letzd2var1352z70zzcfa_specializa7eza7(obj_t,
		obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_za2bcharza2z00zztype_cachez00;
	static obj_t BGl_z62patchz12zd2kwote1324za2zzcfa_specializa7eza7(obj_t,
		obj_t);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	static obj_t BGl_z62patchz12zd2closure1328za2zzcfa_specializa7eza7(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t BGl_za2specializa7edzd2typesza2z75zzcfa_specializa7eza7 =
		BUNSPEC;
	static obj_t BGl_z62patchz12zd2boxzd2ref1362z70zzcfa_specializa7eza7(obj_t,
		obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t BGl_z62arithmeticzd2operatorzf3z43zzcfa_specializa7eza7(obj_t,
		obj_t);
	static obj_t
		BGl_addzd2specializa7edzd2eqzd2typez12z67zzcfa_specializa7eza7(obj_t);
	static obj_t BGl_z62patchz12z70zzcfa_specializa7eza7(obj_t, obj_t);
	static obj_t __cnst[14];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_arithmeticzd2speczd2typeszd2envzd2zzcfa_specializa7eza7,
		BgL_bgl_za762arithmeticza7d22085z00,
		BGl_z62arithmeticzd2speczd2typesz62zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2040z00zzcfa_specializa7eza7,
		BgL_bgl_string2040za700za7za7c2086za7, "        (eq? ", 13);
	      DEFINE_STRING(BGl_string2041z00zzcfa_specializa7eza7,
		BgL_bgl_string2041za700za7za7c2087za7, "add-specialized-type!", 21);
	      DEFINE_STRING(BGl_string2042z00zzcfa_specializa7eza7,
		BgL_bgl_string2042za700za7za7c2088za7, "Unspecialized type", 18);
	      DEFINE_STRING(BGl_string2043z00zzcfa_specializa7eza7,
		BgL_bgl_string2043za700za7za7c2089za7, "install-specialize", 18);
	      DEFINE_STRING(BGl_string2044z00zzcfa_specializa7eza7,
		BgL_bgl_string2044za700za7za7c2090za7, "Illegal argument", 16);
	      DEFINE_STRING(BGl_string2045z00zzcfa_specializa7eza7,
		BgL_bgl_string2045za700za7za7c2091za7, " -- ", 4);
	      DEFINE_STRING(BGl_string2046z00zzcfa_specializa7eza7,
		BgL_bgl_string2046za700za7za7c2092za7, "Can't find global", 17);
	      DEFINE_STRING(BGl_string2047z00zzcfa_specializa7eza7,
		BgL_bgl_string2047za700za7za7c2093za7, "install-specialize!", 19);
	      DEFINE_STRING(BGl_string2054z00zzcfa_specializa7eza7,
		BgL_bgl_string2054za700za7za7c2094za7, "", 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2048z00zzcfa_specializa7eza7,
		BgL_bgl_za762lambda1885za7622095z00,
		BGl_z62lambda1885z62zzcfa_specializa7eza7, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2049z00zzcfa_specializa7eza7,
		BgL_bgl_za762lambda1884za7622096z00,
		BGl_z62lambda1884z62zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2056z00zzcfa_specializa7eza7,
		BgL_bgl_string2056za700za7za7c2097za7, "patch!1319", 10);
	      DEFINE_STRING(BGl_string2057z00zzcfa_specializa7eza7,
		BgL_bgl_string2057za700za7za7c2098za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2059z00zzcfa_specializa7eza7,
		BgL_bgl_string2059za700za7za7c2099za7, "patch!", 6);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2050z00zzcfa_specializa7eza7,
		BgL_bgl_za762lambda1876za7622100z00,
		BGl_z62lambda1876z62zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2051z00zzcfa_specializa7eza7,
		BgL_bgl_za762za7c3za704anonymo2101za7,
		BGl_z62zc3z04anonymousza31875ze3ze5zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2052z00zzcfa_specializa7eza7,
		BgL_bgl_za762lambda1873za7622102z00,
		BGl_z62lambda1873z62zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2053z00zzcfa_specializa7eza7,
		BgL_bgl_za762lambda1869za7622103z00,
		BGl_z62lambda1869z62zzcfa_specializa7eza7, 0L, BUNSPEC, 21);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2055z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza7121319za72104za7,
		BGl_z62patchz121319z70zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2058z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2at2105za7,
		BGl_z62patchz12zd2atom1322za2zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2060z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2kw2106za7,
		BGl_z62patchz12zd2kwote1324za2zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2061z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2va2107za7,
		BGl_z62patchz12zd2var1326za2zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2062z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2cl2108za7,
		BGl_z62patchz12zd2closure1328za2zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2063z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2se2109za7,
		BGl_z62patchz12zd2sequence1330za2zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2064z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2sy2110za7,
		BGl_z62patchz12zd2sync1332za2zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2065z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2ap2111za7,
		BGl_z62patchz12zd2appzd2ly1334z70zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2066z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2fu2112za7,
		BGl_z62patchz12zd2funcall1336za2zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2067z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2ex2113za7,
		BGl_z62patchz12zd2extern1338za2zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2068z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2ca2114za7,
		BGl_z62patchz12zd2cast1340za2zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2069z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2se2115za7,
		BGl_z62patchz12zd2setq1342za2zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2070z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2co2116za7,
		BGl_z62patchz12zd2conditional1344za2zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2071z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2fa2117za7,
		BGl_z62patchz12zd2fail1346za2zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2072z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2sw2118za7,
		BGl_z62patchz12zd2switch1348za2zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2073z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2le2119za7,
		BGl_z62patchz12zd2letzd2fun1350z70zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2074z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2le2120za7,
		BGl_z62patchz12zd2letzd2var1352z70zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2081z00zzcfa_specializa7eza7,
		BgL_bgl_string2081za700za7za7c2121za7, "Unexpected closure", 18);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2075z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2se2122za7,
		BGl_z62patchz12zd2setzd2exzd2it1354za2zzcfa_specializa7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2082z00zzcfa_specializa7eza7,
		BgL_bgl_string2082za700za7za7c2123za7, "cfa_specialize", 14);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2076z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2ju2124za7,
		BGl_z62patchz12zd2jumpzd2exzd2it1356za2zzcfa_specializa7eza7, 0L, BUNSPEC,
		1);
	      DEFINE_STRING(BGl_string2083z00zzcfa_specializa7eza7,
		BgL_bgl_string2083za700za7za7c2125za7,
		"patch!1319 _ cfa_specialize specialized-global obj fix done c-eq? c-boxed-eq? foreign __r4_numbers_6_5_flonum __r4_numbers_6_5_fixnum ((|2+| (|2+| __r4_numbers_6_5) (+fl __r4_numbers_6_5_flonum) (+fx __r4_numbers_6_5_fixnum)) (|2-| (|2-| __r4_numbers_6_5) (-fl __r4_numbers_6_5_flonum) (-fx __r4_numbers_6_5_fixnum)) (2* (2* __r4_numbers_6_5) (*fl __r4_numbers_6_5_flonum) (*fx __r4_numbers_6_5_fixnum)) (abs (abs __r4_numbers_6_5) (absfl __r4_numbers_6_5_flonum) (absfx __r4_numbers_6_5_fixnum))) ((|2+| (|2+| __r4_numbers_6_5) (+fl __r4_numbers_6_5_flonum)) (|2-| (|2-| __r4_numbers_6_5) (-fl __r4_numbers_6_5_flonum)) (2* (2* __r4_numbers_6_5) (*fl __r4_numbers_6_5_flonum)) (2/ (2/ __r4_numbers_6_5) (/fl __r4_numbers_6_5_flonum)) (abs (abs __r4_numbers_6_5) (absfl __r4_numbers_6_5_flonum)) (c-eq? (c-eq? foreign) (=fx __r4_numbers_6_5_fixnum) (=fl __r4_numbers_6_5_flonum)) (2= (2= __r4_numbers_6_5) (=fx __r4_numbers_6_5_fixnum) (=fl __r4_numbers_6_5_flonum)) (2< (2< __r4_numbers_6_5) (<fx __r4_numbers_6_5_fixnum) ("
		"<fl __r4_numbers_6_5_flonum)) (2> (2> __r4_numbers_6_5) (>fx __r4_numbers_6_5_fixnum) (>fl __r4_numbers_6_5_flonum)) (2<= (2<= __r4_numbers_6_5) (<=fx __r4_numbers_6_5_fixnum) (<=fl __r4_numbers_6_5_flonum)) (2>= (2>= __r4_numbers_6_5) (>=fx __r4_numbers_6_5_fixnum) (>=fl __r4_numbers_6_5_flonum)) (zero? (zero? __r4_numbers_6_5) (zerofx? __r4_numbers_6_5_fixnum) (zerofl? __r4_numbers_6_5_flonum)) (positive? (positive? __r4_numbers_6_5) (positivefx? __r4_numbers_6_5_fixnum) (positivefl? __r4_numbers_6_5_flonum)) (negative? (negative? __r4_numbers_6_5) (negativefx? __r4_numbers_6_5_fixnum) (negativefl? __r4_numbers_6_5_flonum)) (log (log __r4_numbers_6_5) (logfl __r4_numbers_6_5_flonum)) (exp (exp __r4_numbers_6_5) (expfl __r4_numbers_6_5_flonum)) (sin (sin __r4_numbers_6_5) (sinfl __r4_numbers_6_5_flonum)) (cos (cos __r4_numbers_6_5) (cosfl __r4_numbers_6_5_flonum)) (tan (tan __r4_numbers_6_5) (tanfl __r4_numbers_6_5_flonum)) (atan (atan __r4_numbers_6_5) (atanfl __r4_numbers_6_5_flonum)) (asin (asin __r4_numb"
		"ers_6_5) (asinfl __r4_numbers_6_5_flonum)) (acos (acos __r4_numbers_6_5) (acosfl __r4_numbers_6_5_flonum)) (sqrt (sqrt __r4_numbers_6_5) (sqrtfl __r4_numbers_6_5_flonum))) ",
		2220);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2077z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2ma2126za7,
		BGl_z62patchz12zd2makezd2box1358z70zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2078z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2bo2127za7,
		BGl_z62patchz12zd2boxzd2setz121360z62zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2079z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2bo2128za7,
		BGl_z62patchz12zd2boxzd2ref1362z70zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2080z00zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za7d2ap2129za7,
		BGl_z62patchz12zd2app1364za2zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_arithmeticzd2operatorzf3zd2envzf3zzcfa_specializa7eza7,
		BgL_bgl_za762arithmeticza7d22130z00,
		BGl_z62arithmeticzd2operatorzf3z43zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_specializa7ez12zd2envz67zzcfa_specializa7eza7,
		BgL_bgl_za762specializa7a7eza72131za7,
		BGl_z62specializa7ez12zd7zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_patchz12zd2envzc0zzcfa_specializa7eza7,
		BgL_bgl_za762patchza712za770za7za72132za7,
		BGl_z62patchz12z70zzcfa_specializa7eza7, 0L, BUNSPEC, 1);
	BGL_IMPORT obj_t BGl_zc3zd2envz11zz__r4_numbers_6_5z00;
	   
		 
		DEFINE_STRING(BGl_string2036z00zzcfa_specializa7eza7,
		BgL_bgl_string2036za700za7za7c2133za7, "   . Specializing", 17);
	      DEFINE_STRING(BGl_string2037z00zzcfa_specializa7eza7,
		BgL_bgl_string2037za700za7za7c2134za7, ")\n", 2);
	      DEFINE_STRING(BGl_string2038z00zzcfa_specializa7eza7,
		BgL_bgl_string2038za700za7za7c2135za7, ": ", 2);
	      DEFINE_STRING(BGl_string2039z00zzcfa_specializa7eza7,
		BgL_bgl_string2039za700za7za7c2136za7, "        (-> ", 12);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_za2specializa7eza2za7zzcfa_specializa7eza7));
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzcfa_specializa7eza7));
		     ADD_ROOT((void *) (&BGl_za2boxedzd2eqzf3za2z21zzcfa_specializa7eza7));
		     ADD_ROOT((void *) (&BGl_za2czd2eqzf3za2z21zzcfa_specializa7eza7));
		   
			 ADD_ROOT((void
				*) (&BGl_za2specializa7ationszd2overflowza2z75zzcfa_specializa7eza7));
		     ADD_ROOT((void
				*) (&BGl_za2specializa7edzd2eqzd2typesza2za7zzcfa_specializa7eza7));
		     ADD_ROOT((void
				*)
			(&BGl_za2specializa7ationszd2sanszd2overflowza2za7zzcfa_specializa7eza7));
		     ADD_ROOT((void
				*) (&BGl_specializa7edzd2globalz75zzcfa_specializa7eza7));
		     ADD_ROOT((void
				*) (&BGl_za2specializa7ationsza2za7zzcfa_specializa7eza7));
		     ADD_ROOT((void
				*) (&BGl_za2specializa7edzd2typesza2z75zzcfa_specializa7eza7));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzcfa_specializa7eza7(long
		BgL_checksumz00_3237, char *BgL_fromz00_3238)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_specializa7eza7))
				{
					BGl_requirezd2initializa7ationz75zzcfa_specializa7eza7 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_specializa7eza7();
					BGl_libraryzd2moduleszd2initz00zzcfa_specializa7eza7();
					BGl_cnstzd2initzd2zzcfa_specializa7eza7();
					BGl_importedzd2moduleszd2initz00zzcfa_specializa7eza7();
					BGl_objectzd2initzd2zzcfa_specializa7eza7();
					BGl_genericzd2initzd2zzcfa_specializa7eza7();
					BGl_methodzd2initzd2zzcfa_specializa7eza7();
					return BGl_toplevelzd2initzd2zzcfa_specializa7eza7();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_specializa7eza7(void)
	{
		{	/* Cfa/specialize.scm 19 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_specialize");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cfa_specialize");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_specialize");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_specialize");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"cfa_specialize");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L,
				"cfa_specialize");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"cfa_specialize");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cfa_specialize");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cfa_specialize");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"cfa_specialize");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_specialize");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_specializa7eza7(void)
	{
		{	/* Cfa/specialize.scm 19 */
			{	/* Cfa/specialize.scm 19 */
				obj_t BgL_cportz00_3116;

				{	/* Cfa/specialize.scm 19 */
					obj_t BgL_stringz00_3123;

					BgL_stringz00_3123 = BGl_string2083z00zzcfa_specializa7eza7;
					{	/* Cfa/specialize.scm 19 */
						obj_t BgL_startz00_3124;

						BgL_startz00_3124 = BINT(0L);
						{	/* Cfa/specialize.scm 19 */
							obj_t BgL_endz00_3125;

							BgL_endz00_3125 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3123)));
							{	/* Cfa/specialize.scm 19 */

								BgL_cportz00_3116 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3123, BgL_startz00_3124, BgL_endz00_3125);
				}}}}
				{
					long BgL_iz00_3117;

					BgL_iz00_3117 = 13L;
				BgL_loopz00_3118:
					if ((BgL_iz00_3117 == -1L))
						{	/* Cfa/specialize.scm 19 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/specialize.scm 19 */
							{	/* Cfa/specialize.scm 19 */
								obj_t BgL_arg2084z00_3119;

								{	/* Cfa/specialize.scm 19 */

									{	/* Cfa/specialize.scm 19 */
										obj_t BgL_locationz00_3121;

										BgL_locationz00_3121 = BBOOL(((bool_t) 0));
										{	/* Cfa/specialize.scm 19 */

											BgL_arg2084z00_3119 =
												BGl_readz00zz__readerz00(BgL_cportz00_3116,
												BgL_locationz00_3121);
										}
									}
								}
								{	/* Cfa/specialize.scm 19 */
									int BgL_tmpz00_3270;

									BgL_tmpz00_3270 = (int) (BgL_iz00_3117);
									CNST_TABLE_SET(BgL_tmpz00_3270, BgL_arg2084z00_3119);
							}}
							{	/* Cfa/specialize.scm 19 */
								int BgL_auxz00_3122;

								BgL_auxz00_3122 = (int) ((BgL_iz00_3117 - 1L));
								{
									long BgL_iz00_3275;

									BgL_iz00_3275 = (long) (BgL_auxz00_3122);
									BgL_iz00_3117 = BgL_iz00_3275;
									goto BgL_loopz00_3118;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_specializa7eza7(void)
	{
		{	/* Cfa/specialize.scm 19 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_specializa7eza7(void)
	{
		{	/* Cfa/specialize.scm 19 */
			BGl_za2specializa7ationsza2za7zzcfa_specializa7eza7 = BFALSE;
			BGl_za2specializa7ationszd2sanszd2overflowza2za7zzcfa_specializa7eza7 =
				CNST_TABLE_REF(0);
			BGl_za2specializa7ationszd2overflowza2z75zzcfa_specializa7eza7 =
				CNST_TABLE_REF(1);
			BGl_za2czd2eqzf3za2z21zzcfa_specializa7eza7 = BUNSPEC;
			BGl_za2specializa7eza2za7zzcfa_specializa7eza7 = BNIL;
			BGl_za2specializa7edzd2typesza2z75zzcfa_specializa7eza7 = BNIL;
			BGl_za2specializa7edzd2eqzd2typesza2za7zzcfa_specializa7eza7 = BNIL;
			BGl_za2boxedzd2eqzf3za2z21zzcfa_specializa7eza7 = BUNSPEC;
			return BGl_zc3zd2envz11zz__r4_numbers_6_5z00;
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzcfa_specializa7eza7(obj_t BgL_l1z00_1,
		obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_1640;

				BgL_headz00_1640 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_1641;
					obj_t BgL_tailz00_1642;

					BgL_prevz00_1641 = BgL_headz00_1640;
					BgL_tailz00_1642 = BgL_l1z00_1;
				BgL_loopz00_1643:
					if (PAIRP(BgL_tailz00_1642))
						{
							obj_t BgL_newzd2prevzd2_1645;

							BgL_newzd2prevzd2_1645 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_1642), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_1641, BgL_newzd2prevzd2_1645);
							{
								obj_t BgL_tailz00_3287;
								obj_t BgL_prevz00_3286;

								BgL_prevz00_3286 = BgL_newzd2prevzd2_1645;
								BgL_tailz00_3287 = CDR(BgL_tailz00_1642);
								BgL_tailz00_1642 = BgL_tailz00_3287;
								BgL_prevz00_1641 = BgL_prevz00_3286;
								goto BgL_loopz00_1643;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_1640);
				}
			}
		}

	}



/* specialize! */
	BGL_EXPORTED_DEF obj_t BGl_specializa7ez12zb5zzcfa_specializa7eza7(obj_t
		BgL_globalsz00_88)
	{
		{	/* Cfa/specialize.scm 44 */
			if (BGl_specializa7ezd2optimiza7ationzf3z21zzcfa_specializa7eza7())
				{	/* Cfa/specialize.scm 45 */
					{	/* Cfa/specialize.scm 47 */
						obj_t BgL_g1299z00_1655;

						BgL_g1299z00_1655 =
							BGl_getzd2specializa7ationsz75zzcfa_specializa7eza7();
						{
							obj_t BgL_l1297z00_1657;

							BgL_l1297z00_1657 = BgL_g1299z00_1655;
						BgL_zc3z04anonymousza31383ze3z87_1658:
							if (PAIRP(BgL_l1297z00_1657))
								{	/* Cfa/specialize.scm 47 */
									BGl_installzd2specializa7ez12z67zzcfa_specializa7eza7(CAR
										(BgL_l1297z00_1657));
									{
										obj_t BgL_l1297z00_3297;

										BgL_l1297z00_3297 = CDR(BgL_l1297z00_1657);
										BgL_l1297z00_1657 = BgL_l1297z00_3297;
										goto BgL_zc3z04anonymousza31383ze3z87_1658;
									}
								}
							else
								{	/* Cfa/specialize.scm 47 */
									((bool_t) 1);
								}
						}
					}
					BGl_patchzd2treez12zc0zzcfa_specializa7eza7(BgL_globalsz00_88);
					BGl_showzd2specializa7ez75zzcfa_specializa7eza7();
					BGl_uninstallzd2specializa7esz12z67zzcfa_specializa7eza7();
				}
			else
				{	/* Cfa/specialize.scm 45 */
					BFALSE;
				}
			return BgL_globalsz00_88;
		}

	}



/* &specialize! */
	obj_t BGl_z62specializa7ez12zd7zzcfa_specializa7eza7(obj_t BgL_envz00_3000,
		obj_t BgL_globalsz00_3001)
	{
		{	/* Cfa/specialize.scm 44 */
			return BGl_specializa7ez12zb5zzcfa_specializa7eza7(BgL_globalsz00_3001);
		}

	}



/* arithmetic-operator? */
	BGL_EXPORTED_DEF obj_t
		BGl_arithmeticzd2operatorzf3z21zzcfa_specializa7eza7(BgL_globalz00_bglt
		BgL_globalz00_89)
	{
		{	/* Cfa/specialize.scm 56 */
			{	/* Cfa/specialize.scm 58 */
				obj_t BgL_cellz00_1664;

				{	/* Cfa/specialize.scm 58 */
					obj_t BgL_arg1437z00_1669;
					obj_t BgL_arg1448z00_1670;

					BgL_arg1437z00_1669 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_globalz00_89)))->BgL_idz00);
					BgL_arg1448z00_1670 =
						BGl_getzd2specializa7ationsz75zzcfa_specializa7eza7();
					BgL_cellz00_1664 =
						BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg1437z00_1669,
						BgL_arg1448z00_1670);
				}
				if (PAIRP(BgL_cellz00_1664))
					{	/* Cfa/specialize.scm 59 */
						bool_t BgL_tmpz00_3309;

						{	/* Cfa/specialize.scm 59 */
							obj_t BgL_tmpz00_3310;

							{	/* Cfa/specialize.scm 59 */
								obj_t BgL_pairz00_2352;

								BgL_pairz00_2352 = CAR(CDR(BgL_cellz00_1664));
								BgL_tmpz00_3310 = CAR(CDR(BgL_pairz00_2352));
							}
							BgL_tmpz00_3309 =
								(BgL_tmpz00_3310 ==
								(((BgL_globalz00_bglt) COBJECT(BgL_globalz00_89))->
									BgL_modulez00));
						}
						return BBOOL(BgL_tmpz00_3309);
					}
				else
					{	/* Cfa/specialize.scm 59 */
						return BFALSE;
					}
			}
		}

	}



/* &arithmetic-operator? */
	obj_t BGl_z62arithmeticzd2operatorzf3z43zzcfa_specializa7eza7(obj_t
		BgL_envz00_3002, obj_t BgL_globalz00_3003)
	{
		{	/* Cfa/specialize.scm 56 */
			return
				BGl_arithmeticzd2operatorzf3z21zzcfa_specializa7eza7(
				((BgL_globalz00_bglt) BgL_globalz00_3003));
		}

	}



/* arithmetic-spec-types */
	BGL_EXPORTED_DEF obj_t
		BGl_arithmeticzd2speczd2typesz00zzcfa_specializa7eza7(BgL_globalz00_bglt
		BgL_globalz00_90)
	{
		{	/* Cfa/specialize.scm 67 */
			{	/* Cfa/specialize.scm 69 */
				obj_t BgL_cellz00_1672;

				{	/* Cfa/specialize.scm 69 */
					obj_t BgL_arg1544z00_1697;
					obj_t BgL_arg1546z00_1698;

					BgL_arg1544z00_1697 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt) BgL_globalz00_90)))->BgL_idz00);
					BgL_arg1546z00_1698 =
						BGl_getzd2specializa7ationsz75zzcfa_specializa7eza7();
					BgL_cellz00_1672 =
						BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_arg1544z00_1697,
						BgL_arg1546z00_1698);
				}
				if (PAIRP(BgL_cellz00_1672))
					{	/* Cfa/specialize.scm 72 */
						obj_t BgL_g1142z00_1674;

						BgL_g1142z00_1674 = CDR(CDR(BgL_cellz00_1672));
						{
							obj_t BgL_specz00_1677;
							obj_t BgL_typesz00_1678;

							BgL_specz00_1677 = BgL_g1142z00_1674;
							BgL_typesz00_1678 = BNIL;
						BgL_zc3z04anonymousza31450ze3z87_1679:
							if (NULLP(BgL_specz00_1677))
								{	/* Cfa/specialize.scm 75 */
									return BgL_typesz00_1678;
								}
							else
								{	/* Cfa/specialize.scm 77 */
									bool_t BgL_test2145z00_3330;

									if (CBOOL
										(BGl_za2optimzd2cfazd2fixnumzd2arithmeticzf3za2z21zzengine_paramz00))
										{	/* Cfa/specialize.scm 78 */
											obj_t BgL_tmpz00_3333;

											{	/* Cfa/specialize.scm 78 */
												obj_t BgL_pairz00_2361;

												BgL_pairz00_2361 = CAR(((obj_t) BgL_specz00_1677));
												BgL_tmpz00_3333 = CAR(CDR(BgL_pairz00_2361));
											}
											BgL_test2145z00_3330 =
												(BgL_tmpz00_3333 == CNST_TABLE_REF(2));
										}
									else
										{	/* Cfa/specialize.scm 77 */
											BgL_test2145z00_3330 = ((bool_t) 0);
										}
									if (BgL_test2145z00_3330)
										{	/* Cfa/specialize.scm 79 */
											obj_t BgL_arg1472z00_1684;
											obj_t BgL_arg1473z00_1685;

											BgL_arg1472z00_1684 = CDR(((obj_t) BgL_specz00_1677));
											BgL_arg1473z00_1685 =
												MAKE_YOUNG_PAIR(BGl_za2longza2z00zztype_cachez00,
												BgL_typesz00_1678);
											{
												obj_t BgL_typesz00_3344;
												obj_t BgL_specz00_3343;

												BgL_specz00_3343 = BgL_arg1472z00_1684;
												BgL_typesz00_3344 = BgL_arg1473z00_1685;
												BgL_typesz00_1678 = BgL_typesz00_3344;
												BgL_specz00_1677 = BgL_specz00_3343;
												goto BgL_zc3z04anonymousza31450ze3z87_1679;
											}
										}
									else
										{	/* Cfa/specialize.scm 80 */
											bool_t BgL_test2147z00_3345;

											if (CBOOL
												(BGl_za2optimzd2cfazd2flonumzd2arithmeticzf3za2z21zzengine_paramz00))
												{	/* Cfa/specialize.scm 81 */
													obj_t BgL_tmpz00_3348;

													{	/* Cfa/specialize.scm 81 */
														obj_t BgL_pairz00_2367;

														BgL_pairz00_2367 = CAR(((obj_t) BgL_specz00_1677));
														BgL_tmpz00_3348 = CAR(CDR(BgL_pairz00_2367));
													}
													BgL_test2147z00_3345 =
														(BgL_tmpz00_3348 == CNST_TABLE_REF(3));
												}
											else
												{	/* Cfa/specialize.scm 80 */
													BgL_test2147z00_3345 = ((bool_t) 0);
												}
											if (BgL_test2147z00_3345)
												{	/* Cfa/specialize.scm 82 */
													obj_t BgL_arg1502z00_1689;
													obj_t BgL_arg1509z00_1690;

													BgL_arg1502z00_1689 = CDR(((obj_t) BgL_specz00_1677));
													BgL_arg1509z00_1690 =
														MAKE_YOUNG_PAIR(BGl_za2realza2z00zztype_cachez00,
														BgL_typesz00_1678);
													{
														obj_t BgL_typesz00_3359;
														obj_t BgL_specz00_3358;

														BgL_specz00_3358 = BgL_arg1502z00_1689;
														BgL_typesz00_3359 = BgL_arg1509z00_1690;
														BgL_typesz00_1678 = BgL_typesz00_3359;
														BgL_specz00_1677 = BgL_specz00_3358;
														goto BgL_zc3z04anonymousza31450ze3z87_1679;
													}
												}
											else
												{	/* Cfa/specialize.scm 84 */
													obj_t BgL_arg1513z00_1691;

													BgL_arg1513z00_1691 = CDR(((obj_t) BgL_specz00_1677));
													{
														obj_t BgL_specz00_3362;

														BgL_specz00_3362 = BgL_arg1513z00_1691;
														BgL_specz00_1677 = BgL_specz00_3362;
														goto BgL_zc3z04anonymousza31450ze3z87_1679;
													}
												}
										}
								}
						}
					}
				else
					{	/* Cfa/specialize.scm 70 */
						return BNIL;
					}
			}
		}

	}



/* &arithmetic-spec-types */
	obj_t BGl_z62arithmeticzd2speczd2typesz62zzcfa_specializa7eza7(obj_t
		BgL_envz00_3004, obj_t BgL_globalz00_3005)
	{
		{	/* Cfa/specialize.scm 67 */
			return
				BGl_arithmeticzd2speczd2typesz00zzcfa_specializa7eza7(
				((BgL_globalz00_bglt) BgL_globalz00_3005));
		}

	}



/* get-specializations */
	obj_t BGl_getzd2specializa7ationsz75zzcfa_specializa7eza7(void)
	{
		{	/* Cfa/specialize.scm 89 */
			if (CBOOL(BGl_za2specializa7ationsza2za7zzcfa_specializa7eza7))
				{	/* Cfa/specialize.scm 91 */
					return BGl_za2specializa7ationsza2za7zzcfa_specializa7eza7;
				}
			else
				{	/* Cfa/specialize.scm 91 */
					if (CBOOL(BGl_za2arithmeticzd2overflowza2zd2zzengine_paramz00))
						{	/* Cfa/specialize.scm 94 */
							BGl_za2specializa7ationsza2za7zzcfa_specializa7eza7 =
								CNST_TABLE_REF(0);
							return BGl_za2specializa7ationsza2za7zzcfa_specializa7eza7;
						}
					else
						{	/* Cfa/specialize.scm 94 */
							BGl_za2specializa7ationsza2za7zzcfa_specializa7eza7 =
								BGl_appendzd221011zd2zzcfa_specializa7eza7(CNST_TABLE_REF(1),
								CNST_TABLE_REF(0));
							return BGl_za2specializa7ationsza2za7zzcfa_specializa7eza7;
						}
				}
		}

	}



/* specialize-optimization? */
	bool_t BGl_specializa7ezd2optimiza7ationzf3z21zzcfa_specializa7eza7(void)
	{
		{	/* Cfa/specialize.scm 188 */
			if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 1L))
				{	/* Cfa/specialize.scm 189 */
					if (CBOOL(BGl_za2libzd2modeza2zd2zzengine_paramz00))
						{	/* Cfa/specialize.scm 189 */
							return ((bool_t) 0);
						}
					else
						{	/* Cfa/specialize.scm 189 */
							return ((bool_t) 1);
						}
				}
			else
				{	/* Cfa/specialize.scm 189 */
					return ((bool_t) 0);
				}
		}

	}



/* show-specialize */
	bool_t BGl_showzd2specializa7ez75zzcfa_specializa7eza7(void)
	{
		{	/* Cfa/specialize.scm 194 */
			{	/* Cfa/specialize.scm 195 */
				obj_t BgL_list1547z00_1700;

				{	/* Cfa/specialize.scm 195 */
					obj_t BgL_arg1552z00_1701;

					BgL_arg1552z00_1701 =
						MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
					BgL_list1547z00_1700 =
						MAKE_YOUNG_PAIR(BGl_string2036z00zzcfa_specializa7eza7,
						BgL_arg1552z00_1701);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1547z00_1700);
			}
			{
				obj_t BgL_l1300z00_1703;

				BgL_l1300z00_1703 =
					BGl_za2specializa7edzd2typesza2z75zzcfa_specializa7eza7;
			BgL_zc3z04anonymousza31553ze3z87_1704:
				if (PAIRP(BgL_l1300z00_1703))
					{	/* Cfa/specialize.scm 196 */
						{	/* Cfa/specialize.scm 197 */
							obj_t BgL_typezd2numzd2_1706;

							BgL_typezd2numzd2_1706 = CAR(BgL_l1300z00_1703);
							if (((long) CINT(CDR(((obj_t) BgL_typezd2numzd2_1706))) > 0L))
								{	/* Cfa/specialize.scm 198 */
									obj_t BgL_arg1564z00_1709;
									obj_t BgL_arg1565z00_1710;

									{	/* Cfa/specialize.scm 198 */
										obj_t BgL_arg1584z00_1716;

										BgL_arg1584z00_1716 = CAR(((obj_t) BgL_typezd2numzd2_1706));
										BgL_arg1564z00_1709 =
											BGl_shapez00zztools_shapez00(BgL_arg1584z00_1716);
									}
									BgL_arg1565z00_1710 = CDR(((obj_t) BgL_typezd2numzd2_1706));
									{	/* Cfa/specialize.scm 198 */
										obj_t BgL_list1566z00_1711;

										{	/* Cfa/specialize.scm 198 */
											obj_t BgL_arg1571z00_1712;

											{	/* Cfa/specialize.scm 198 */
												obj_t BgL_arg1573z00_1713;

												{	/* Cfa/specialize.scm 198 */
													obj_t BgL_arg1575z00_1714;

													{	/* Cfa/specialize.scm 198 */
														obj_t BgL_arg1576z00_1715;

														BgL_arg1576z00_1715 =
															MAKE_YOUNG_PAIR
															(BGl_string2037z00zzcfa_specializa7eza7, BNIL);
														BgL_arg1575z00_1714 =
															MAKE_YOUNG_PAIR(BgL_arg1565z00_1710,
															BgL_arg1576z00_1715);
													}
													BgL_arg1573z00_1713 =
														MAKE_YOUNG_PAIR
														(BGl_string2038z00zzcfa_specializa7eza7,
														BgL_arg1575z00_1714);
												}
												BgL_arg1571z00_1712 =
													MAKE_YOUNG_PAIR(BgL_arg1564z00_1709,
													BgL_arg1573z00_1713);
											}
											BgL_list1566z00_1711 =
												MAKE_YOUNG_PAIR(BGl_string2039z00zzcfa_specializa7eza7,
												BgL_arg1571z00_1712);
										}
										BGl_verbosez00zztools_speekz00(BINT(2L),
											BgL_list1566z00_1711);
								}}
							else
								{	/* Cfa/specialize.scm 197 */
									BFALSE;
								}
						}
						{
							obj_t BgL_l1300z00_3403;

							BgL_l1300z00_3403 = CDR(BgL_l1300z00_1703);
							BgL_l1300z00_1703 = BgL_l1300z00_3403;
							goto BgL_zc3z04anonymousza31553ze3z87_1704;
						}
					}
				else
					{	/* Cfa/specialize.scm 196 */
						((bool_t) 1);
					}
			}
			{
				obj_t BgL_l1302z00_1721;

				BgL_l1302z00_1721 =
					BGl_za2specializa7edzd2eqzd2typesza2za7zzcfa_specializa7eza7;
			BgL_zc3z04anonymousza31590ze3z87_1722:
				if (PAIRP(BgL_l1302z00_1721))
					{	/* Cfa/specialize.scm 201 */
						{	/* Cfa/specialize.scm 202 */
							obj_t BgL_typezd2numzd2_1724;

							BgL_typezd2numzd2_1724 = CAR(BgL_l1302z00_1721);
							if (((long) CINT(CDR(((obj_t) BgL_typezd2numzd2_1724))) > 0L))
								{	/* Cfa/specialize.scm 203 */
									obj_t BgL_arg1595z00_1727;
									obj_t BgL_arg1602z00_1728;

									{	/* Cfa/specialize.scm 203 */
										obj_t BgL_arg1613z00_1734;

										BgL_arg1613z00_1734 = CAR(((obj_t) BgL_typezd2numzd2_1724));
										BgL_arg1595z00_1727 =
											BGl_shapez00zztools_shapez00(BgL_arg1613z00_1734);
									}
									BgL_arg1602z00_1728 = CDR(((obj_t) BgL_typezd2numzd2_1724));
									{	/* Cfa/specialize.scm 203 */
										obj_t BgL_list1603z00_1729;

										{	/* Cfa/specialize.scm 203 */
											obj_t BgL_arg1605z00_1730;

											{	/* Cfa/specialize.scm 203 */
												obj_t BgL_arg1606z00_1731;

												{	/* Cfa/specialize.scm 203 */
													obj_t BgL_arg1609z00_1732;

													{	/* Cfa/specialize.scm 203 */
														obj_t BgL_arg1611z00_1733;

														BgL_arg1611z00_1733 =
															MAKE_YOUNG_PAIR
															(BGl_string2037z00zzcfa_specializa7eza7, BNIL);
														BgL_arg1609z00_1732 =
															MAKE_YOUNG_PAIR(BgL_arg1602z00_1728,
															BgL_arg1611z00_1733);
													}
													BgL_arg1606z00_1731 =
														MAKE_YOUNG_PAIR
														(BGl_string2038z00zzcfa_specializa7eza7,
														BgL_arg1609z00_1732);
												}
												BgL_arg1605z00_1730 =
													MAKE_YOUNG_PAIR(BgL_arg1595z00_1727,
													BgL_arg1606z00_1731);
											}
											BgL_list1603z00_1729 =
												MAKE_YOUNG_PAIR(BGl_string2040z00zzcfa_specializa7eza7,
												BgL_arg1605z00_1730);
										}
										BGl_verbosez00zztools_speekz00(BINT(2L),
											BgL_list1603z00_1729);
								}}
							else
								{	/* Cfa/specialize.scm 202 */
									BFALSE;
								}
						}
						{
							obj_t BgL_l1302z00_3425;

							BgL_l1302z00_3425 = CDR(BgL_l1302z00_1721);
							BgL_l1302z00_1721 = BgL_l1302z00_3425;
							goto BgL_zc3z04anonymousza31590ze3z87_1722;
						}
					}
				else
					{	/* Cfa/specialize.scm 201 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* install-specialized-type! */
	obj_t BGl_installzd2specializa7edzd2typez12zb5zzcfa_specializa7eza7(obj_t
		BgL_typez00_91)
	{
		{	/* Cfa/specialize.scm 218 */
			{	/* Cfa/specialize.scm 219 */
				bool_t BgL_test2157z00_3427;

				{	/* Cfa/specialize.scm 219 */
					obj_t BgL_tmpz00_3428;

					BgL_tmpz00_3428 =
						BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_typez00_91,
						BGl_za2specializa7edzd2typesza2z75zzcfa_specializa7eza7);
					BgL_test2157z00_3427 = PAIRP(BgL_tmpz00_3428);
				}
				if (BgL_test2157z00_3427)
					{	/* Cfa/specialize.scm 219 */
						return BFALSE;
					}
				else
					{	/* Cfa/specialize.scm 219 */
						{	/* Cfa/specialize.scm 220 */
							obj_t BgL_arg1627z00_1740;

							BgL_arg1627z00_1740 = MAKE_YOUNG_PAIR(BgL_typez00_91, BINT(0L));
							BGl_za2specializa7edzd2typesza2z75zzcfa_specializa7eza7 =
								MAKE_YOUNG_PAIR(BgL_arg1627z00_1740,
								BGl_za2specializa7edzd2typesza2z75zzcfa_specializa7eza7);
						}
						{	/* Cfa/specialize.scm 221 */
							obj_t BgL_list1628z00_1741;

							BgL_list1628z00_1741 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BNIL);
							BGl_za2boxedzd2eqzf3za2z21zzcfa_specializa7eza7 =
								BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(5),
								BgL_list1628z00_1741);
						}
						{	/* Cfa/specialize.scm 222 */
							obj_t BgL_list1629z00_1742;

							BgL_list1629z00_1742 = MAKE_YOUNG_PAIR(CNST_TABLE_REF(4), BNIL);
							return (BGl_za2czd2eqzf3za2z21zzcfa_specializa7eza7 =
								BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(6),
									BgL_list1629z00_1742), BUNSPEC);
						}
					}
			}
		}

	}



/* add-specialized-type! */
	obj_t BGl_addzd2specializa7edzd2typez12zb5zzcfa_specializa7eza7(obj_t
		BgL_typez00_92)
	{
		{	/* Cfa/specialize.scm 227 */
			{	/* Cfa/specialize.scm 228 */
				obj_t BgL_cellz00_1744;

				BgL_cellz00_1744 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_typez00_92,
					BGl_za2specializa7edzd2typesza2z75zzcfa_specializa7eza7);
				if (PAIRP(BgL_cellz00_1744))
					{	/* Cfa/specialize.scm 232 */
						long BgL_arg1642z00_1746;

						BgL_arg1642z00_1746 = (1L + (long) CINT(CDR(BgL_cellz00_1744)));
						{	/* Cfa/specialize.scm 232 */
							obj_t BgL_tmpz00_3448;

							BgL_tmpz00_3448 = BINT(BgL_arg1642z00_1746);
							return SET_CDR(BgL_cellz00_1744, BgL_tmpz00_3448);
						}
					}
				else
					{	/* Cfa/specialize.scm 229 */
						return
							BGl_internalzd2errorzd2zztools_errorz00
							(BGl_string2041z00zzcfa_specializa7eza7,
							BGl_string2042z00zzcfa_specializa7eza7,
							BGl_shapez00zztools_shapez00(BgL_typez00_92));
					}
			}
		}

	}



/* add-specialized-eq-type! */
	obj_t BGl_addzd2specializa7edzd2eqzd2typez12z67zzcfa_specializa7eza7(obj_t
		BgL_typez00_93)
	{
		{	/* Cfa/specialize.scm 237 */
			{	/* Cfa/specialize.scm 238 */
				obj_t BgL_cellz00_1749;

				BgL_cellz00_1749 =
					BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_typez00_93,
					BGl_za2specializa7edzd2eqzd2typesza2za7zzcfa_specializa7eza7);
				if (PAIRP(BgL_cellz00_1749))
					{	/* Cfa/specialize.scm 242 */
						long BgL_arg1654z00_1751;

						BgL_arg1654z00_1751 = (1L + (long) CINT(CDR(BgL_cellz00_1749)));
						{	/* Cfa/specialize.scm 242 */
							obj_t BgL_tmpz00_3459;

							BgL_tmpz00_3459 = BINT(BgL_arg1654z00_1751);
							return SET_CDR(BgL_cellz00_1749, BgL_tmpz00_3459);
						}
					}
				else
					{	/* Cfa/specialize.scm 241 */
						obj_t BgL_arg1663z00_1753;

						BgL_arg1663z00_1753 = MAKE_YOUNG_PAIR(BgL_typez00_93, BINT(1L));
						return (BGl_za2specializa7edzd2eqzd2typesza2za7zzcfa_specializa7eza7
							=
							MAKE_YOUNG_PAIR(BgL_arg1663z00_1753,
								BGl_za2specializa7edzd2eqzd2typesza2za7zzcfa_specializa7eza7),
							BUNSPEC);
					}
			}
		}

	}



/* install-specialize! */
	obj_t BGl_installzd2specializa7ez12z67zzcfa_specializa7eza7(obj_t
		BgL_specz00_94)
	{
		{	/* Cfa/specialize.scm 247 */
			{	/* Cfa/specialize.scm 248 */
				obj_t BgL_genz00_1754;

				{	/* Cfa/specialize.scm 248 */
					obj_t BgL_pairz00_2395;

					BgL_pairz00_2395 = CDR(((obj_t) BgL_specz00_94));
					BgL_genz00_1754 = CAR(BgL_pairz00_2395);
				}
				{	/* Cfa/specialize.scm 248 */
					obj_t BgL_fixesz00_1755;

					{	/* Cfa/specialize.scm 249 */
						obj_t BgL_pairz00_2399;

						BgL_pairz00_2399 = CDR(((obj_t) BgL_specz00_94));
						BgL_fixesz00_1755 = CDR(BgL_pairz00_2399);
					}
					{	/* Cfa/specialize.scm 249 */
						obj_t BgL_genzd2idzd2_1756;

						BgL_genzd2idzd2_1756 = CAR(((obj_t) BgL_genz00_1754));
						{	/* Cfa/specialize.scm 250 */
							obj_t BgL_genzd2modzd2_1757;

							{	/* Cfa/specialize.scm 251 */
								obj_t BgL_pairz00_2404;

								BgL_pairz00_2404 = CDR(((obj_t) BgL_genz00_1754));
								BgL_genzd2modzd2_1757 = CAR(BgL_pairz00_2404);
							}
							{	/* Cfa/specialize.scm 251 */
								obj_t BgL_genericz00_1758;

								BgL_genericz00_1758 =
									BGl_findzd2globalzf2modulez20zzast_envz00
									(BgL_genzd2idzd2_1756, BgL_genzd2modzd2_1757);
								{	/* Cfa/specialize.scm 252 */

									{	/* Cfa/specialize.scm 253 */
										bool_t BgL_test2160z00_3477;

										{	/* Cfa/specialize.scm 253 */
											obj_t BgL_classz00_2405;

											BgL_classz00_2405 = BGl_globalz00zzast_varz00;
											if (BGL_OBJECTP(BgL_genericz00_1758))
												{	/* Cfa/specialize.scm 253 */
													BgL_objectz00_bglt BgL_arg1807z00_2407;

													BgL_arg1807z00_2407 =
														(BgL_objectz00_bglt) (BgL_genericz00_1758);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Cfa/specialize.scm 253 */
															long BgL_idxz00_2413;

															BgL_idxz00_2413 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_2407);
															BgL_test2160z00_3477 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_2413 + 2L)) == BgL_classz00_2405);
														}
													else
														{	/* Cfa/specialize.scm 253 */
															bool_t BgL_res2016z00_2438;

															{	/* Cfa/specialize.scm 253 */
																obj_t BgL_oclassz00_2421;

																{	/* Cfa/specialize.scm 253 */
																	obj_t BgL_arg1815z00_2429;
																	long BgL_arg1816z00_2430;

																	BgL_arg1815z00_2429 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Cfa/specialize.scm 253 */
																		long BgL_arg1817z00_2431;

																		BgL_arg1817z00_2431 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_2407);
																		BgL_arg1816z00_2430 =
																			(BgL_arg1817z00_2431 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_2421 =
																		VECTOR_REF(BgL_arg1815z00_2429,
																		BgL_arg1816z00_2430);
																}
																{	/* Cfa/specialize.scm 253 */
																	bool_t BgL__ortest_1115z00_2422;

																	BgL__ortest_1115z00_2422 =
																		(BgL_classz00_2405 == BgL_oclassz00_2421);
																	if (BgL__ortest_1115z00_2422)
																		{	/* Cfa/specialize.scm 253 */
																			BgL_res2016z00_2438 =
																				BgL__ortest_1115z00_2422;
																		}
																	else
																		{	/* Cfa/specialize.scm 253 */
																			long BgL_odepthz00_2423;

																			{	/* Cfa/specialize.scm 253 */
																				obj_t BgL_arg1804z00_2424;

																				BgL_arg1804z00_2424 =
																					(BgL_oclassz00_2421);
																				BgL_odepthz00_2423 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_2424);
																			}
																			if ((2L < BgL_odepthz00_2423))
																				{	/* Cfa/specialize.scm 253 */
																					obj_t BgL_arg1802z00_2426;

																					{	/* Cfa/specialize.scm 253 */
																						obj_t BgL_arg1803z00_2427;

																						BgL_arg1803z00_2427 =
																							(BgL_oclassz00_2421);
																						BgL_arg1802z00_2426 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_2427, 2L);
																					}
																					BgL_res2016z00_2438 =
																						(BgL_arg1802z00_2426 ==
																						BgL_classz00_2405);
																				}
																			else
																				{	/* Cfa/specialize.scm 253 */
																					BgL_res2016z00_2438 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2160z00_3477 = BgL_res2016z00_2438;
														}
												}
											else
												{	/* Cfa/specialize.scm 253 */
													BgL_test2160z00_3477 = ((bool_t) 0);
												}
										}
										if (BgL_test2160z00_3477)
											{
												obj_t BgL_fixesz00_1762;
												obj_t BgL_resz00_1763;

												BgL_fixesz00_1762 = BgL_fixesz00_1755;
												BgL_resz00_1763 = BNIL;
											BgL_zc3z04anonymousza31665ze3z87_1764:
												if (NULLP(BgL_fixesz00_1762))
													{	/* Cfa/specialize.scm 257 */
														bool_t BgL_test2166z00_3502;

														{	/* Cfa/specialize.scm 257 */
															obj_t BgL_classz00_2439;

															BgL_classz00_2439 =
																BGl_specializa7edzd2globalz75zzcfa_specializa7eza7;
															if (BGL_OBJECTP(BgL_genericz00_1758))
																{	/* Cfa/specialize.scm 257 */
																	obj_t BgL_oclassz00_2441;

																	{	/* Cfa/specialize.scm 257 */
																		obj_t BgL_arg1815z00_2443;
																		long BgL_arg1816z00_2444;

																		BgL_arg1815z00_2443 =
																			(BGl_za2classesza2z00zz__objectz00);
																		{	/* Cfa/specialize.scm 257 */
																			long BgL_arg1817z00_2445;

																			BgL_arg1817z00_2445 =
																				BGL_OBJECT_CLASS_NUM(
																				((BgL_objectz00_bglt)
																					BgL_genericz00_1758));
																			BgL_arg1816z00_2444 =
																				(BgL_arg1817z00_2445 - OBJECT_TYPE);
																		}
																		BgL_oclassz00_2441 =
																			VECTOR_REF(BgL_arg1815z00_2443,
																			BgL_arg1816z00_2444);
																	}
																	BgL_test2166z00_3502 =
																		(BgL_oclassz00_2441 == BgL_classz00_2439);
																}
															else
																{	/* Cfa/specialize.scm 257 */
																	BgL_test2166z00_3502 = ((bool_t) 0);
																}
														}
														if (BgL_test2166z00_3502)
															{
																obj_t BgL_auxz00_3518;
																BgL_specializa7edzd2globalz75_bglt
																	BgL_auxz00_3511;
																{	/* Cfa/specialize.scm 259 */
																	obj_t BgL_arg1675z00_1768;

																	{
																		BgL_specializa7edzd2globalz75_bglt
																			BgL_auxz00_3519;
																		{
																			obj_t BgL_auxz00_3520;

																			{	/* Cfa/specialize.scm 259 */
																				BgL_objectz00_bglt BgL_tmpz00_3521;

																				BgL_tmpz00_3521 =
																					((BgL_objectz00_bglt)
																					((BgL_globalz00_bglt)
																						BgL_genericz00_1758));
																				BgL_auxz00_3520 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_3521);
																			}
																			BgL_auxz00_3519 =
																				((BgL_specializa7edzd2globalz75_bglt)
																				BgL_auxz00_3520);
																		}
																		BgL_arg1675z00_1768 =
																			(((BgL_specializa7edzd2globalz75_bglt)
																				COBJECT(BgL_auxz00_3519))->BgL_fixz00);
																	}
																	BgL_auxz00_3518 =
																		BGl_appendzd221011zd2zzcfa_specializa7eza7
																		(BgL_arg1675z00_1768, BgL_resz00_1763);
																}
																{
																	obj_t BgL_auxz00_3512;

																	{	/* Cfa/specialize.scm 259 */
																		BgL_objectz00_bglt BgL_tmpz00_3513;

																		BgL_tmpz00_3513 =
																			((BgL_objectz00_bglt)
																			((BgL_globalz00_bglt)
																				BgL_genericz00_1758));
																		BgL_auxz00_3512 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_3513);
																	}
																	BgL_auxz00_3511 =
																		((BgL_specializa7edzd2globalz75_bglt)
																		BgL_auxz00_3512);
																}
																return
																	((((BgL_specializa7edzd2globalz75_bglt)
																			COBJECT(BgL_auxz00_3511))->BgL_fixz00) =
																	((obj_t) BgL_auxz00_3518), BUNSPEC);
															}
														else
															{	/* Cfa/specialize.scm 257 */
																{	/* Cfa/specialize.scm 261 */
																	BgL_specializa7edzd2globalz75_bglt
																		BgL_wide1149z00_1771;
																	BgL_wide1149z00_1771 =
																		((BgL_specializa7edzd2globalz75_bglt)
																		BOBJECT(GC_MALLOC(sizeof(struct
																					BgL_specializa7edzd2globalz75_bgl))));
																	{	/* Cfa/specialize.scm 261 */
																		obj_t BgL_auxz00_3534;
																		BgL_objectz00_bglt BgL_tmpz00_3530;

																		BgL_auxz00_3534 =
																			((obj_t) BgL_wide1149z00_1771);
																		BgL_tmpz00_3530 =
																			((BgL_objectz00_bglt)
																			((BgL_globalz00_bglt)
																				((BgL_globalz00_bglt)
																					BgL_genericz00_1758)));
																		BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3530,
																			BgL_auxz00_3534);
																	}
																	((BgL_objectz00_bglt)
																		((BgL_globalz00_bglt)
																			((BgL_globalz00_bglt)
																				BgL_genericz00_1758)));
																	{	/* Cfa/specialize.scm 261 */
																		long BgL_arg1678z00_1772;

																		BgL_arg1678z00_1772 =
																			BGL_CLASS_NUM
																			(BGl_specializa7edzd2globalz75zzcfa_specializa7eza7);
																		BGL_OBJECT_CLASS_NUM_SET((
																				(BgL_objectz00_bglt) (
																					(BgL_globalz00_bglt) (
																						(BgL_globalz00_bglt)
																						BgL_genericz00_1758))),
																			BgL_arg1678z00_1772);
																	}
																	((BgL_globalz00_bglt)
																		((BgL_globalz00_bglt)
																			((BgL_globalz00_bglt)
																				BgL_genericz00_1758)));
																}
																{
																	BgL_specializa7edzd2globalz75_bglt
																		BgL_auxz00_3548;
																	{
																		obj_t BgL_auxz00_3549;

																		{	/* Cfa/specialize.scm 262 */
																			BgL_objectz00_bglt BgL_tmpz00_3550;

																			BgL_tmpz00_3550 =
																				((BgL_objectz00_bglt)
																				((BgL_globalz00_bglt)
																					((BgL_globalz00_bglt)
																						BgL_genericz00_1758)));
																			BgL_auxz00_3549 =
																				BGL_OBJECT_WIDENING(BgL_tmpz00_3550);
																		}
																		BgL_auxz00_3548 =
																			((BgL_specializa7edzd2globalz75_bglt)
																			BgL_auxz00_3549);
																	}
																	((((BgL_specializa7edzd2globalz75_bglt)
																				COBJECT(BgL_auxz00_3548))->BgL_fixz00) =
																		((obj_t) BgL_resz00_1763), BUNSPEC);
																}
																((BgL_globalz00_bglt)
																	((BgL_globalz00_bglt) BgL_genericz00_1758));
																return
																	(BGl_za2specializa7eza2za7zzcfa_specializa7eza7
																	=
																	MAKE_YOUNG_PAIR(BgL_genericz00_1758,
																		BGl_za2specializa7eza2za7zzcfa_specializa7eza7),
																	BUNSPEC);
															}
													}
												else
													{	/* Cfa/specialize.scm 264 */
														obj_t BgL_fixz00_1774;

														BgL_fixz00_1774 = CAR(((obj_t) BgL_fixesz00_1762));
														{	/* Cfa/specialize.scm 264 */
															obj_t BgL_idz00_1775;

															BgL_idz00_1775 = CAR(((obj_t) BgL_fixz00_1774));
															{	/* Cfa/specialize.scm 265 */
																obj_t BgL_modz00_1776;

																{	/* Cfa/specialize.scm 266 */
																	obj_t BgL_pairz00_2463;

																	BgL_pairz00_2463 =
																		CDR(((obj_t) BgL_fixz00_1774));
																	BgL_modz00_1776 = CAR(BgL_pairz00_2463);
																}
																{	/* Cfa/specialize.scm 266 */
																	obj_t BgL_globalz00_1777;

																	BgL_globalz00_1777 =
																		BGl_findzd2globalzf2modulez20zzast_envz00
																		(BgL_idz00_1775, BgL_modz00_1776);
																	{	/* Cfa/specialize.scm 267 */

																		{	/* Cfa/specialize.scm 268 */
																			bool_t BgL_test2168z00_3568;

																			{	/* Cfa/specialize.scm 268 */
																				obj_t BgL_classz00_2464;

																				BgL_classz00_2464 =
																					BGl_globalz00zzast_varz00;
																				if (BGL_OBJECTP(BgL_globalz00_1777))
																					{	/* Cfa/specialize.scm 268 */
																						BgL_objectz00_bglt
																							BgL_arg1807z00_2466;
																						BgL_arg1807z00_2466 =
																							(BgL_objectz00_bglt)
																							(BgL_globalz00_1777);
																						if (BGL_CONDEXPAND_ISA_ARCH64())
																							{	/* Cfa/specialize.scm 268 */
																								long BgL_idxz00_2472;

																								BgL_idxz00_2472 =
																									BGL_OBJECT_INHERITANCE_NUM
																									(BgL_arg1807z00_2466);
																								BgL_test2168z00_3568 =
																									(VECTOR_REF
																									(BGl_za2inheritancesza2z00zz__objectz00,
																										(BgL_idxz00_2472 + 2L)) ==
																									BgL_classz00_2464);
																							}
																						else
																							{	/* Cfa/specialize.scm 268 */
																								bool_t BgL_res2017z00_2497;

																								{	/* Cfa/specialize.scm 268 */
																									obj_t BgL_oclassz00_2480;

																									{	/* Cfa/specialize.scm 268 */
																										obj_t BgL_arg1815z00_2488;
																										long BgL_arg1816z00_2489;

																										BgL_arg1815z00_2488 =
																											(BGl_za2classesza2z00zz__objectz00);
																										{	/* Cfa/specialize.scm 268 */
																											long BgL_arg1817z00_2490;

																											BgL_arg1817z00_2490 =
																												BGL_OBJECT_CLASS_NUM
																												(BgL_arg1807z00_2466);
																											BgL_arg1816z00_2489 =
																												(BgL_arg1817z00_2490 -
																												OBJECT_TYPE);
																										}
																										BgL_oclassz00_2480 =
																											VECTOR_REF
																											(BgL_arg1815z00_2488,
																											BgL_arg1816z00_2489);
																									}
																									{	/* Cfa/specialize.scm 268 */
																										bool_t
																											BgL__ortest_1115z00_2481;
																										BgL__ortest_1115z00_2481 =
																											(BgL_classz00_2464 ==
																											BgL_oclassz00_2480);
																										if (BgL__ortest_1115z00_2481)
																											{	/* Cfa/specialize.scm 268 */
																												BgL_res2017z00_2497 =
																													BgL__ortest_1115z00_2481;
																											}
																										else
																											{	/* Cfa/specialize.scm 268 */
																												long BgL_odepthz00_2482;

																												{	/* Cfa/specialize.scm 268 */
																													obj_t
																														BgL_arg1804z00_2483;
																													BgL_arg1804z00_2483 =
																														(BgL_oclassz00_2480);
																													BgL_odepthz00_2482 =
																														BGL_CLASS_DEPTH
																														(BgL_arg1804z00_2483);
																												}
																												if (
																													(2L <
																														BgL_odepthz00_2482))
																													{	/* Cfa/specialize.scm 268 */
																														obj_t
																															BgL_arg1802z00_2485;
																														{	/* Cfa/specialize.scm 268 */
																															obj_t
																																BgL_arg1803z00_2486;
																															BgL_arg1803z00_2486
																																=
																																(BgL_oclassz00_2480);
																															BgL_arg1802z00_2485
																																=
																																BGL_CLASS_ANCESTORS_REF
																																(BgL_arg1803z00_2486,
																																2L);
																														}
																														BgL_res2017z00_2497
																															=
																															(BgL_arg1802z00_2485
																															==
																															BgL_classz00_2464);
																													}
																												else
																													{	/* Cfa/specialize.scm 268 */
																														BgL_res2017z00_2497
																															= ((bool_t) 0);
																													}
																											}
																									}
																								}
																								BgL_test2168z00_3568 =
																									BgL_res2017z00_2497;
																							}
																					}
																				else
																					{	/* Cfa/specialize.scm 268 */
																						BgL_test2168z00_3568 = ((bool_t) 0);
																					}
																			}
																			if (BgL_test2168z00_3568)
																				{	/* Cfa/specialize.scm 269 */
																					BgL_valuez00_bglt BgL_valz00_1779;

																					BgL_valz00_1779 =
																						(((BgL_variablez00_bglt) COBJECT(
																								((BgL_variablez00_bglt)
																									((BgL_globalz00_bglt)
																										BgL_globalz00_1777))))->
																						BgL_valuez00);
																					{	/* Cfa/specialize.scm 271 */
																						bool_t BgL_test2173z00_3594;

																						{	/* Cfa/specialize.scm 271 */
																							bool_t BgL_test2174z00_3595;

																							{	/* Cfa/specialize.scm 271 */
																								obj_t BgL_classz00_2499;

																								BgL_classz00_2499 =
																									BGl_sfunz00zzast_varz00;
																								{	/* Cfa/specialize.scm 271 */
																									BgL_objectz00_bglt
																										BgL_arg1807z00_2501;
																									{	/* Cfa/specialize.scm 271 */
																										obj_t BgL_tmpz00_3596;

																										BgL_tmpz00_3596 =
																											((obj_t)
																											((BgL_objectz00_bglt)
																												BgL_valz00_1779));
																										BgL_arg1807z00_2501 =
																											(BgL_objectz00_bglt)
																											(BgL_tmpz00_3596);
																									}
																									if (BGL_CONDEXPAND_ISA_ARCH64
																										())
																										{	/* Cfa/specialize.scm 271 */
																											long BgL_idxz00_2507;

																											BgL_idxz00_2507 =
																												BGL_OBJECT_INHERITANCE_NUM
																												(BgL_arg1807z00_2501);
																											BgL_test2174z00_3595 =
																												(VECTOR_REF
																												(BGl_za2inheritancesza2z00zz__objectz00,
																													(BgL_idxz00_2507 +
																														3L)) ==
																												BgL_classz00_2499);
																										}
																									else
																										{	/* Cfa/specialize.scm 271 */
																											bool_t
																												BgL_res2018z00_2532;
																											{	/* Cfa/specialize.scm 271 */
																												obj_t
																													BgL_oclassz00_2515;
																												{	/* Cfa/specialize.scm 271 */
																													obj_t
																														BgL_arg1815z00_2523;
																													long
																														BgL_arg1816z00_2524;
																													BgL_arg1815z00_2523 =
																														(BGl_za2classesza2z00zz__objectz00);
																													{	/* Cfa/specialize.scm 271 */
																														long
																															BgL_arg1817z00_2525;
																														BgL_arg1817z00_2525
																															=
																															BGL_OBJECT_CLASS_NUM
																															(BgL_arg1807z00_2501);
																														BgL_arg1816z00_2524
																															=
																															(BgL_arg1817z00_2525
																															- OBJECT_TYPE);
																													}
																													BgL_oclassz00_2515 =
																														VECTOR_REF
																														(BgL_arg1815z00_2523,
																														BgL_arg1816z00_2524);
																												}
																												{	/* Cfa/specialize.scm 271 */
																													bool_t
																														BgL__ortest_1115z00_2516;
																													BgL__ortest_1115z00_2516
																														=
																														(BgL_classz00_2499
																														==
																														BgL_oclassz00_2515);
																													if (BgL__ortest_1115z00_2516)
																														{	/* Cfa/specialize.scm 271 */
																															BgL_res2018z00_2532
																																=
																																BgL__ortest_1115z00_2516;
																														}
																													else
																														{	/* Cfa/specialize.scm 271 */
																															long
																																BgL_odepthz00_2517;
																															{	/* Cfa/specialize.scm 271 */
																																obj_t
																																	BgL_arg1804z00_2518;
																																BgL_arg1804z00_2518
																																	=
																																	(BgL_oclassz00_2515);
																																BgL_odepthz00_2517
																																	=
																																	BGL_CLASS_DEPTH
																																	(BgL_arg1804z00_2518);
																															}
																															if (
																																(3L <
																																	BgL_odepthz00_2517))
																																{	/* Cfa/specialize.scm 271 */
																																	obj_t
																																		BgL_arg1802z00_2520;
																																	{	/* Cfa/specialize.scm 271 */
																																		obj_t
																																			BgL_arg1803z00_2521;
																																		BgL_arg1803z00_2521
																																			=
																																			(BgL_oclassz00_2515);
																																		BgL_arg1802z00_2520
																																			=
																																			BGL_CLASS_ANCESTORS_REF
																																			(BgL_arg1803z00_2521,
																																			3L);
																																	}
																																	BgL_res2018z00_2532
																																		=
																																		(BgL_arg1802z00_2520
																																		==
																																		BgL_classz00_2499);
																																}
																															else
																																{	/* Cfa/specialize.scm 271 */
																																	BgL_res2018z00_2532
																																		=
																																		((bool_t)
																																		0);
																																}
																														}
																												}
																											}
																											BgL_test2174z00_3595 =
																												BgL_res2018z00_2532;
																										}
																								}
																							}
																							if (BgL_test2174z00_3595)
																								{	/* Cfa/specialize.scm 271 */
																									obj_t BgL_tmpz00_3619;

																									BgL_tmpz00_3619 =
																										(((BgL_sfunz00_bglt)
																											COBJECT((
																													(BgL_sfunz00_bglt)
																													BgL_valz00_1779)))->
																										BgL_argsz00);
																									BgL_test2173z00_3594 =
																										PAIRP(BgL_tmpz00_3619);
																								}
																							else
																								{	/* Cfa/specialize.scm 271 */
																									BgL_test2173z00_3594 =
																										((bool_t) 0);
																								}
																						}
																						if (BgL_test2173z00_3594)
																							{	/* Cfa/specialize.scm 272 */
																								obj_t BgL_typez00_1783;

																								{	/* Cfa/specialize.scm 272 */
																									obj_t BgL_vz00_1787;

																									{	/* Cfa/specialize.scm 272 */
																										obj_t BgL_pairz00_2535;

																										BgL_pairz00_2535 =
																											(((BgL_sfunz00_bglt)
																												COBJECT((
																														(BgL_sfunz00_bglt)
																														BgL_valz00_1779)))->
																											BgL_argsz00);
																										BgL_vz00_1787 =
																											CAR(BgL_pairz00_2535);
																									}
																									{	/* Cfa/specialize.scm 274 */
																										bool_t BgL_test2178z00_3626;

																										{	/* Cfa/specialize.scm 274 */
																											obj_t BgL_classz00_2536;

																											BgL_classz00_2536 =
																												BGl_typez00zztype_typez00;
																											if (BGL_OBJECTP
																												(BgL_vz00_1787))
																												{	/* Cfa/specialize.scm 274 */
																													BgL_objectz00_bglt
																														BgL_arg1807z00_2538;
																													BgL_arg1807z00_2538 =
																														(BgL_objectz00_bglt)
																														(BgL_vz00_1787);
																													if (BGL_CONDEXPAND_ISA_ARCH64())
																														{	/* Cfa/specialize.scm 274 */
																															long
																																BgL_idxz00_2544;
																															BgL_idxz00_2544 =
																																BGL_OBJECT_INHERITANCE_NUM
																																(BgL_arg1807z00_2538);
																															BgL_test2178z00_3626
																																=
																																(VECTOR_REF
																																(BGl_za2inheritancesza2z00zz__objectz00,
																																	(BgL_idxz00_2544
																																		+ 1L)) ==
																																BgL_classz00_2536);
																														}
																													else
																														{	/* Cfa/specialize.scm 274 */
																															bool_t
																																BgL_res2019z00_2569;
																															{	/* Cfa/specialize.scm 274 */
																																obj_t
																																	BgL_oclassz00_2552;
																																{	/* Cfa/specialize.scm 274 */
																																	obj_t
																																		BgL_arg1815z00_2560;
																																	long
																																		BgL_arg1816z00_2561;
																																	BgL_arg1815z00_2560
																																		=
																																		(BGl_za2classesza2z00zz__objectz00);
																																	{	/* Cfa/specialize.scm 274 */
																																		long
																																			BgL_arg1817z00_2562;
																																		BgL_arg1817z00_2562
																																			=
																																			BGL_OBJECT_CLASS_NUM
																																			(BgL_arg1807z00_2538);
																																		BgL_arg1816z00_2561
																																			=
																																			(BgL_arg1817z00_2562
																																			-
																																			OBJECT_TYPE);
																																	}
																																	BgL_oclassz00_2552
																																		=
																																		VECTOR_REF
																																		(BgL_arg1815z00_2560,
																																		BgL_arg1816z00_2561);
																																}
																																{	/* Cfa/specialize.scm 274 */
																																	bool_t
																																		BgL__ortest_1115z00_2553;
																																	BgL__ortest_1115z00_2553
																																		=
																																		(BgL_classz00_2536
																																		==
																																		BgL_oclassz00_2552);
																																	if (BgL__ortest_1115z00_2553)
																																		{	/* Cfa/specialize.scm 274 */
																																			BgL_res2019z00_2569
																																				=
																																				BgL__ortest_1115z00_2553;
																																		}
																																	else
																																		{	/* Cfa/specialize.scm 274 */
																																			long
																																				BgL_odepthz00_2554;
																																			{	/* Cfa/specialize.scm 274 */
																																				obj_t
																																					BgL_arg1804z00_2555;
																																				BgL_arg1804z00_2555
																																					=
																																					(BgL_oclassz00_2552);
																																				BgL_odepthz00_2554
																																					=
																																					BGL_CLASS_DEPTH
																																					(BgL_arg1804z00_2555);
																																			}
																																			if (
																																				(1L <
																																					BgL_odepthz00_2554))
																																				{	/* Cfa/specialize.scm 274 */
																																					obj_t
																																						BgL_arg1802z00_2557;
																																					{	/* Cfa/specialize.scm 274 */
																																						obj_t
																																							BgL_arg1803z00_2558;
																																						BgL_arg1803z00_2558
																																							=
																																							(BgL_oclassz00_2552);
																																						BgL_arg1802z00_2557
																																							=
																																							BGL_CLASS_ANCESTORS_REF
																																							(BgL_arg1803z00_2558,
																																							1L);
																																					}
																																					BgL_res2019z00_2569
																																						=
																																						(BgL_arg1802z00_2557
																																						==
																																						BgL_classz00_2536);
																																				}
																																			else
																																				{	/* Cfa/specialize.scm 274 */
																																					BgL_res2019z00_2569
																																						=
																																						(
																																						(bool_t)
																																						0);
																																				}
																																		}
																																}
																															}
																															BgL_test2178z00_3626
																																=
																																BgL_res2019z00_2569;
																														}
																												}
																											else
																												{	/* Cfa/specialize.scm 274 */
																													BgL_test2178z00_3626 =
																														((bool_t) 0);
																												}
																										}
																										if (BgL_test2178z00_3626)
																											{	/* Cfa/specialize.scm 274 */
																												BgL_typez00_1783 =
																													BgL_vz00_1787;
																											}
																										else
																											{	/* Cfa/specialize.scm 276 */
																												bool_t
																													BgL_test2183z00_3649;
																												{	/* Cfa/specialize.scm 276 */
																													obj_t
																														BgL_classz00_2570;
																													BgL_classz00_2570 =
																														BGl_localz00zzast_varz00;
																													if (BGL_OBJECTP
																														(BgL_vz00_1787))
																														{	/* Cfa/specialize.scm 276 */
																															BgL_objectz00_bglt
																																BgL_arg1807z00_2572;
																															BgL_arg1807z00_2572
																																=
																																(BgL_objectz00_bglt)
																																(BgL_vz00_1787);
																															if (BGL_CONDEXPAND_ISA_ARCH64())
																																{	/* Cfa/specialize.scm 276 */
																																	long
																																		BgL_idxz00_2578;
																																	BgL_idxz00_2578
																																		=
																																		BGL_OBJECT_INHERITANCE_NUM
																																		(BgL_arg1807z00_2572);
																																	BgL_test2183z00_3649
																																		=
																																		(VECTOR_REF
																																		(BGl_za2inheritancesza2z00zz__objectz00,
																																			(BgL_idxz00_2578
																																				+
																																				2L)) ==
																																		BgL_classz00_2570);
																																}
																															else
																																{	/* Cfa/specialize.scm 276 */
																																	bool_t
																																		BgL_res2020z00_2603;
																																	{	/* Cfa/specialize.scm 276 */
																																		obj_t
																																			BgL_oclassz00_2586;
																																		{	/* Cfa/specialize.scm 276 */
																																			obj_t
																																				BgL_arg1815z00_2594;
																																			long
																																				BgL_arg1816z00_2595;
																																			BgL_arg1815z00_2594
																																				=
																																				(BGl_za2classesza2z00zz__objectz00);
																																			{	/* Cfa/specialize.scm 276 */
																																				long
																																					BgL_arg1817z00_2596;
																																				BgL_arg1817z00_2596
																																					=
																																					BGL_OBJECT_CLASS_NUM
																																					(BgL_arg1807z00_2572);
																																				BgL_arg1816z00_2595
																																					=
																																					(BgL_arg1817z00_2596
																																					-
																																					OBJECT_TYPE);
																																			}
																																			BgL_oclassz00_2586
																																				=
																																				VECTOR_REF
																																				(BgL_arg1815z00_2594,
																																				BgL_arg1816z00_2595);
																																		}
																																		{	/* Cfa/specialize.scm 276 */
																																			bool_t
																																				BgL__ortest_1115z00_2587;
																																			BgL__ortest_1115z00_2587
																																				=
																																				(BgL_classz00_2570
																																				==
																																				BgL_oclassz00_2586);
																																			if (BgL__ortest_1115z00_2587)
																																				{	/* Cfa/specialize.scm 276 */
																																					BgL_res2020z00_2603
																																						=
																																						BgL__ortest_1115z00_2587;
																																				}
																																			else
																																				{	/* Cfa/specialize.scm 276 */
																																					long
																																						BgL_odepthz00_2588;
																																					{	/* Cfa/specialize.scm 276 */
																																						obj_t
																																							BgL_arg1804z00_2589;
																																						BgL_arg1804z00_2589
																																							=
																																							(BgL_oclassz00_2586);
																																						BgL_odepthz00_2588
																																							=
																																							BGL_CLASS_DEPTH
																																							(BgL_arg1804z00_2589);
																																					}
																																					if (
																																						(2L
																																							<
																																							BgL_odepthz00_2588))
																																						{	/* Cfa/specialize.scm 276 */
																																							obj_t
																																								BgL_arg1802z00_2591;
																																							{	/* Cfa/specialize.scm 276 */
																																								obj_t
																																									BgL_arg1803z00_2592;
																																								BgL_arg1803z00_2592
																																									=
																																									(BgL_oclassz00_2586);
																																								BgL_arg1802z00_2591
																																									=
																																									BGL_CLASS_ANCESTORS_REF
																																									(BgL_arg1803z00_2592,
																																									2L);
																																							}
																																							BgL_res2020z00_2603
																																								=
																																								(BgL_arg1802z00_2591
																																								==
																																								BgL_classz00_2570);
																																						}
																																					else
																																						{	/* Cfa/specialize.scm 276 */
																																							BgL_res2020z00_2603
																																								=
																																								(
																																								(bool_t)
																																								0);
																																						}
																																				}
																																		}
																																	}
																																	BgL_test2183z00_3649
																																		=
																																		BgL_res2020z00_2603;
																																}
																														}
																													else
																														{	/* Cfa/specialize.scm 276 */
																															BgL_test2183z00_3649
																																= ((bool_t) 0);
																														}
																												}
																												if (BgL_test2183z00_3649)
																													{	/* Cfa/specialize.scm 276 */
																														BgL_typez00_1783 =
																															((obj_t)
																															(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_localz00_bglt) BgL_vz00_1787))))->BgL_typez00));
																													}
																												else
																													{	/* Cfa/specialize.scm 282 */
																														obj_t
																															BgL_arg1702z00_1790;
																														BgL_arg1702z00_1790
																															=
																															(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt) BgL_globalz00_1777))))->BgL_idz00);
																														BgL_typez00_1783 =
																															BGl_internalzd2errorzd2zztools_errorz00
																															(BGl_string2043z00zzcfa_specializa7eza7,
																															BGl_string2044z00zzcfa_specializa7eza7,
																															BgL_arg1702z00_1790);
																													}
																											}
																									}
																								}
																								BGl_installzd2specializa7edzd2typez12zb5zzcfa_specializa7eza7
																									(BgL_typez00_1783);
																								{	/* Cfa/specialize.scm 284 */
																									obj_t BgL_arg1691z00_1784;
																									obj_t BgL_arg1692z00_1785;

																									BgL_arg1691z00_1784 =
																										CDR(
																										((obj_t)
																											BgL_fixesz00_1762));
																									{	/* Cfa/specialize.scm 285 */
																										obj_t BgL_arg1699z00_1786;

																										BgL_arg1699z00_1786 =
																											MAKE_YOUNG_PAIR
																											(BgL_typez00_1783,
																											BgL_globalz00_1777);
																										BgL_arg1692z00_1785 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1699z00_1786,
																											BgL_resz00_1763);
																									}
																									{
																										obj_t BgL_resz00_3686;
																										obj_t BgL_fixesz00_3685;

																										BgL_fixesz00_3685 =
																											BgL_arg1691z00_1784;
																										BgL_resz00_3686 =
																											BgL_arg1692z00_1785;
																										BgL_resz00_1763 =
																											BgL_resz00_3686;
																										BgL_fixesz00_1762 =
																											BgL_fixesz00_3685;
																										goto
																											BgL_zc3z04anonymousza31665ze3z87_1764;
																									}
																								}
																							}
																						else
																							{	/* Cfa/specialize.scm 286 */
																								bool_t BgL_test2188z00_3687;

																								{	/* Cfa/specialize.scm 286 */
																									bool_t BgL_test2189z00_3688;

																									{	/* Cfa/specialize.scm 286 */
																										obj_t BgL_classz00_2607;

																										BgL_classz00_2607 =
																											BGl_cfunz00zzast_varz00;
																										{	/* Cfa/specialize.scm 286 */
																											BgL_objectz00_bglt
																												BgL_arg1807z00_2609;
																											{	/* Cfa/specialize.scm 286 */
																												obj_t BgL_tmpz00_3689;

																												BgL_tmpz00_3689 =
																													((obj_t)
																													((BgL_objectz00_bglt)
																														BgL_valz00_1779));
																												BgL_arg1807z00_2609 =
																													(BgL_objectz00_bglt)
																													(BgL_tmpz00_3689);
																											}
																											if (BGL_CONDEXPAND_ISA_ARCH64())
																												{	/* Cfa/specialize.scm 286 */
																													long BgL_idxz00_2615;

																													BgL_idxz00_2615 =
																														BGL_OBJECT_INHERITANCE_NUM
																														(BgL_arg1807z00_2609);
																													BgL_test2189z00_3688 =
																														(VECTOR_REF
																														(BGl_za2inheritancesza2z00zz__objectz00,
																															(BgL_idxz00_2615 +
																																3L)) ==
																														BgL_classz00_2607);
																												}
																											else
																												{	/* Cfa/specialize.scm 286 */
																													bool_t
																														BgL_res2021z00_2640;
																													{	/* Cfa/specialize.scm 286 */
																														obj_t
																															BgL_oclassz00_2623;
																														{	/* Cfa/specialize.scm 286 */
																															obj_t
																																BgL_arg1815z00_2631;
																															long
																																BgL_arg1816z00_2632;
																															BgL_arg1815z00_2631
																																=
																																(BGl_za2classesza2z00zz__objectz00);
																															{	/* Cfa/specialize.scm 286 */
																																long
																																	BgL_arg1817z00_2633;
																																BgL_arg1817z00_2633
																																	=
																																	BGL_OBJECT_CLASS_NUM
																																	(BgL_arg1807z00_2609);
																																BgL_arg1816z00_2632
																																	=
																																	(BgL_arg1817z00_2633
																																	-
																																	OBJECT_TYPE);
																															}
																															BgL_oclassz00_2623
																																=
																																VECTOR_REF
																																(BgL_arg1815z00_2631,
																																BgL_arg1816z00_2632);
																														}
																														{	/* Cfa/specialize.scm 286 */
																															bool_t
																																BgL__ortest_1115z00_2624;
																															BgL__ortest_1115z00_2624
																																=
																																(BgL_classz00_2607
																																==
																																BgL_oclassz00_2623);
																															if (BgL__ortest_1115z00_2624)
																																{	/* Cfa/specialize.scm 286 */
																																	BgL_res2021z00_2640
																																		=
																																		BgL__ortest_1115z00_2624;
																																}
																															else
																																{	/* Cfa/specialize.scm 286 */
																																	long
																																		BgL_odepthz00_2625;
																																	{	/* Cfa/specialize.scm 286 */
																																		obj_t
																																			BgL_arg1804z00_2626;
																																		BgL_arg1804z00_2626
																																			=
																																			(BgL_oclassz00_2623);
																																		BgL_odepthz00_2625
																																			=
																																			BGL_CLASS_DEPTH
																																			(BgL_arg1804z00_2626);
																																	}
																																	if (
																																		(3L <
																																			BgL_odepthz00_2625))
																																		{	/* Cfa/specialize.scm 286 */
																																			obj_t
																																				BgL_arg1802z00_2628;
																																			{	/* Cfa/specialize.scm 286 */
																																				obj_t
																																					BgL_arg1803z00_2629;
																																				BgL_arg1803z00_2629
																																					=
																																					(BgL_oclassz00_2623);
																																				BgL_arg1802z00_2628
																																					=
																																					BGL_CLASS_ANCESTORS_REF
																																					(BgL_arg1803z00_2629,
																																					3L);
																																			}
																																			BgL_res2021z00_2640
																																				=
																																				(BgL_arg1802z00_2628
																																				==
																																				BgL_classz00_2607);
																																		}
																																	else
																																		{	/* Cfa/specialize.scm 286 */
																																			BgL_res2021z00_2640
																																				=
																																				(
																																				(bool_t)
																																				0);
																																		}
																																}
																														}
																													}
																													BgL_test2189z00_3688 =
																														BgL_res2021z00_2640;
																												}
																										}
																									}
																									if (BgL_test2189z00_3688)
																										{	/* Cfa/specialize.scm 286 */
																											obj_t BgL_tmpz00_3712;

																											BgL_tmpz00_3712 =
																												(((BgL_cfunz00_bglt)
																													COBJECT((
																															(BgL_cfunz00_bglt)
																															BgL_valz00_1779)))->
																												BgL_argszd2typezd2);
																											BgL_test2188z00_3687 =
																												PAIRP(BgL_tmpz00_3712);
																										}
																									else
																										{	/* Cfa/specialize.scm 286 */
																											BgL_test2188z00_3687 =
																												((bool_t) 0);
																										}
																								}
																								if (BgL_test2188z00_3687)
																									{	/* Cfa/specialize.scm 287 */
																										obj_t BgL_typez00_1795;

																										{	/* Cfa/specialize.scm 287 */
																											obj_t BgL_pairz00_2643;

																											BgL_pairz00_2643 =
																												(((BgL_cfunz00_bglt)
																													COBJECT((
																															(BgL_cfunz00_bglt)
																															BgL_valz00_1779)))->
																												BgL_argszd2typezd2);
																											BgL_typez00_1795 =
																												CAR(BgL_pairz00_2643);
																										}
																										BGl_installzd2specializa7edzd2typez12zb5zzcfa_specializa7eza7
																											(BgL_typez00_1795);
																										{	/* Cfa/specialize.scm 289 */
																											obj_t BgL_arg1708z00_1796;
																											obj_t BgL_arg1709z00_1797;

																											BgL_arg1708z00_1796 =
																												CDR(
																												((obj_t)
																													BgL_fixesz00_1762));
																											{	/* Cfa/specialize.scm 290 */
																												obj_t
																													BgL_arg1710z00_1798;
																												BgL_arg1710z00_1798 =
																													MAKE_YOUNG_PAIR
																													(BgL_typez00_1795,
																													BgL_globalz00_1777);
																												BgL_arg1709z00_1797 =
																													MAKE_YOUNG_PAIR
																													(BgL_arg1710z00_1798,
																													BgL_resz00_1763);
																											}
																											{
																												obj_t BgL_resz00_3725;
																												obj_t BgL_fixesz00_3724;

																												BgL_fixesz00_3724 =
																													BgL_arg1708z00_1796;
																												BgL_resz00_3725 =
																													BgL_arg1709z00_1797;
																												BgL_resz00_1763 =
																													BgL_resz00_3725;
																												BgL_fixesz00_1762 =
																													BgL_fixesz00_3724;
																												goto
																													BgL_zc3z04anonymousza31665ze3z87_1764;
																											}
																										}
																									}
																								else
																									{	/* Cfa/specialize.scm 286 */
																										return BFALSE;
																									}
																							}
																					}
																				}
																			else
																				{	/* Cfa/specialize.scm 268 */
																					{	/* Cfa/specialize.scm 295 */
																						obj_t BgL_arg1718z00_1804;

																						BgL_arg1718z00_1804 =
																							MAKE_YOUNG_PAIR(BgL_idz00_1775,
																							BgL_modz00_1776);
																						{	/* Cfa/specialize.scm 292 */
																							obj_t BgL_list1719z00_1805;

																							{	/* Cfa/specialize.scm 292 */
																								obj_t BgL_arg1720z00_1806;

																								{	/* Cfa/specialize.scm 292 */
																									obj_t BgL_arg1722z00_1807;

																									{	/* Cfa/specialize.scm 292 */
																										obj_t BgL_arg1724z00_1808;

																										BgL_arg1724z00_1808 =
																											MAKE_YOUNG_PAIR
																											(BgL_arg1718z00_1804,
																											BNIL);
																										BgL_arg1722z00_1807 =
																											MAKE_YOUNG_PAIR
																											(BGl_string2045z00zzcfa_specializa7eza7,
																											BgL_arg1724z00_1808);
																									}
																									BgL_arg1720z00_1806 =
																										MAKE_YOUNG_PAIR
																										(BGl_string2046z00zzcfa_specializa7eza7,
																										BgL_arg1722z00_1807);
																								}
																								BgL_list1719z00_1805 =
																									MAKE_YOUNG_PAIR
																									(BGl_string2047z00zzcfa_specializa7eza7,
																									BgL_arg1720z00_1806);
																							}
																							BGl_warningz00zz__errorz00
																								(BgL_list1719z00_1805);
																						}
																					}
																					{	/* Cfa/specialize.scm 296 */
																						obj_t BgL_arg1733z00_1809;

																						BgL_arg1733z00_1809 =
																							CDR(((obj_t) BgL_fixesz00_1762));
																						{
																							obj_t BgL_fixesz00_3734;

																							BgL_fixesz00_3734 =
																								BgL_arg1733z00_1809;
																							BgL_fixesz00_1762 =
																								BgL_fixesz00_3734;
																							goto
																								BgL_zc3z04anonymousza31665ze3z87_1764;
																						}
																					}
																				}
																		}
																	}
																}
															}
														}
													}
											}
										else
											{	/* Cfa/specialize.scm 300 */
												obj_t BgL_arg1734z00_1811;

												BgL_arg1734z00_1811 =
													MAKE_YOUNG_PAIR(BgL_genzd2idzd2_1756,
													BgL_genzd2modzd2_1757);
												{	/* Cfa/specialize.scm 297 */
													obj_t BgL_list1735z00_1812;

													{	/* Cfa/specialize.scm 297 */
														obj_t BgL_arg1736z00_1813;

														{	/* Cfa/specialize.scm 297 */
															obj_t BgL_arg1737z00_1814;

															{	/* Cfa/specialize.scm 297 */
																obj_t BgL_arg1738z00_1815;

																BgL_arg1738z00_1815 =
																	MAKE_YOUNG_PAIR(BgL_arg1734z00_1811, BNIL);
																BgL_arg1737z00_1814 =
																	MAKE_YOUNG_PAIR
																	(BGl_string2045z00zzcfa_specializa7eza7,
																	BgL_arg1738z00_1815);
															}
															BgL_arg1736z00_1813 =
																MAKE_YOUNG_PAIR
																(BGl_string2046z00zzcfa_specializa7eza7,
																BgL_arg1737z00_1814);
														}
														BgL_list1735z00_1812 =
															MAKE_YOUNG_PAIR
															(BGl_string2047z00zzcfa_specializa7eza7,
															BgL_arg1736z00_1813);
													}
													BGL_TAIL return
														BGl_warningz00zz__errorz00(BgL_list1735z00_1812);
												}
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* uninstall-specializes! */
	obj_t BGl_uninstallzd2specializa7esz12z67zzcfa_specializa7eza7(void)
	{
		{	/* Cfa/specialize.scm 305 */
			BGl_za2specializa7edzd2typesza2z75zzcfa_specializa7eza7 = BNIL;
			{
				obj_t BgL_l1304z00_1817;

				BgL_l1304z00_1817 = BGl_za2specializa7eza2za7zzcfa_specializa7eza7;
			BgL_zc3z04anonymousza31739ze3z87_1818:
				if (PAIRP(BgL_l1304z00_1817))
					{	/* Cfa/specialize.scm 307 */
						{	/* Cfa/specialize.scm 307 */
							obj_t BgL_oz00_1820;

							BgL_oz00_1820 = CAR(BgL_l1304z00_1817);
							{	/* Cfa/specialize.scm 307 */
								long BgL_arg1746z00_1822;

								{	/* Cfa/specialize.scm 307 */
									obj_t BgL_arg1747z00_1823;

									{	/* Cfa/specialize.scm 307 */
										obj_t BgL_arg1748z00_1824;

										{	/* Cfa/specialize.scm 307 */
											obj_t BgL_arg1815z00_2648;
											long BgL_arg1816z00_2649;

											BgL_arg1815z00_2648 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/specialize.scm 307 */
												long BgL_arg1817z00_2650;

												BgL_arg1817z00_2650 =
													BGL_OBJECT_CLASS_NUM(
													((BgL_objectz00_bglt) BgL_oz00_1820));
												BgL_arg1816z00_2649 =
													(BgL_arg1817z00_2650 - OBJECT_TYPE);
											}
											BgL_arg1748z00_1824 =
												VECTOR_REF(BgL_arg1815z00_2648, BgL_arg1816z00_2649);
										}
										BgL_arg1747z00_1823 =
											BGl_classzd2superzd2zz__objectz00(BgL_arg1748z00_1824);
									}
									{	/* Cfa/specialize.scm 307 */
										obj_t BgL_tmpz00_3750;

										BgL_tmpz00_3750 = ((obj_t) BgL_arg1747z00_1823);
										BgL_arg1746z00_1822 = BGL_CLASS_NUM(BgL_tmpz00_3750);
								}}
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_oz00_1820), BgL_arg1746z00_1822);
							}
							{	/* Cfa/specialize.scm 307 */
								BgL_objectz00_bglt BgL_tmpz00_3755;

								BgL_tmpz00_3755 = ((BgL_objectz00_bglt) BgL_oz00_1820);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3755, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_oz00_1820);
							BgL_oz00_1820;
						}
						{
							obj_t BgL_l1304z00_3759;

							BgL_l1304z00_3759 = CDR(BgL_l1304z00_1817);
							BgL_l1304z00_1817 = BgL_l1304z00_3759;
							goto BgL_zc3z04anonymousza31739ze3z87_1818;
						}
					}
				else
					{	/* Cfa/specialize.scm 307 */
						((bool_t) 1);
					}
			}
			return (BGl_za2specializa7eza2za7zzcfa_specializa7eza7 = BNIL, BUNSPEC);
		}

	}



/* patch-tree! */
	bool_t BGl_patchzd2treez12zc0zzcfa_specializa7eza7(obj_t BgL_globalsz00_95)
	{
		{	/* Cfa/specialize.scm 313 */
			{
				obj_t BgL_l1306z00_1828;

				BgL_l1306z00_1828 = BgL_globalsz00_95;
			BgL_zc3z04anonymousza31750ze3z87_1829:
				if (PAIRP(BgL_l1306z00_1828))
					{	/* Cfa/specialize.scm 314 */
						BGl_patchzd2funz12zc0zzcfa_specializa7eza7(CAR(BgL_l1306z00_1828));
						{
							obj_t BgL_l1306z00_3765;

							BgL_l1306z00_3765 = CDR(BgL_l1306z00_1828);
							BgL_l1306z00_1828 = BgL_l1306z00_3765;
							goto BgL_zc3z04anonymousza31750ze3z87_1829;
						}
					}
				else
					{	/* Cfa/specialize.scm 314 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* patch-fun! */
	obj_t BGl_patchzd2funz12zc0zzcfa_specializa7eza7(obj_t BgL_variablez00_96)
	{
		{	/* Cfa/specialize.scm 319 */
			{	/* Cfa/specialize.scm 320 */
				BgL_valuez00_bglt BgL_funz00_1834;

				BgL_funz00_1834 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_variablez00_96)))->BgL_valuez00);
				{	/* Cfa/specialize.scm 321 */
					obj_t BgL_arg1754z00_1835;

					{	/* Cfa/specialize.scm 321 */
						obj_t BgL_arg1755z00_1836;

						BgL_arg1755z00_1836 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_1834)))->BgL_bodyz00);
						BgL_arg1754z00_1835 =
							BGl_patchz12z12zzcfa_specializa7eza7(
							((BgL_nodez00_bglt) BgL_arg1755z00_1836));
					}
					return
						((((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_1834)))->BgL_bodyz00) =
						((obj_t) BgL_arg1754z00_1835), BUNSPEC);
				}
			}
		}

	}



/* patch*! */
	obj_t BGl_patchza2z12zb0zzcfa_specializa7eza7(obj_t BgL_nodeza2za2_119)
	{
		{	/* Cfa/specialize.scm 527 */
			{
				obj_t BgL_nodeza2za2_1838;

				BgL_nodeza2za2_1838 = BgL_nodeza2za2_119;
			BgL_zc3z04anonymousza31756ze3z87_1839:
				if (NULLP(BgL_nodeza2za2_1838))
					{	/* Cfa/specialize.scm 529 */
						return CNST_TABLE_REF(7);
					}
				else
					{	/* Cfa/specialize.scm 529 */
						{	/* Cfa/specialize.scm 532 */
							obj_t BgL_arg1761z00_1841;

							{	/* Cfa/specialize.scm 532 */
								obj_t BgL_arg1762z00_1842;

								BgL_arg1762z00_1842 = CAR(((obj_t) BgL_nodeza2za2_1838));
								BgL_arg1761z00_1841 =
									BGl_patchz12z12zzcfa_specializa7eza7(
									((BgL_nodez00_bglt) BgL_arg1762z00_1842));
							}
							{	/* Cfa/specialize.scm 532 */
								obj_t BgL_tmpz00_3782;

								BgL_tmpz00_3782 = ((obj_t) BgL_nodeza2za2_1838);
								SET_CAR(BgL_tmpz00_3782, BgL_arg1761z00_1841);
							}
						}
						{	/* Cfa/specialize.scm 533 */
							obj_t BgL_arg1765z00_1843;

							BgL_arg1765z00_1843 = CDR(((obj_t) BgL_nodeza2za2_1838));
							{
								obj_t BgL_nodeza2za2_3787;

								BgL_nodeza2za2_3787 = BgL_arg1765z00_1843;
								BgL_nodeza2za2_1838 = BgL_nodeza2za2_3787;
								goto BgL_zc3z04anonymousza31756ze3z87_1839;
							}
						}
					}
			}
		}

	}



/* specialize-app! */
	BgL_appz00_bglt
		BGl_specializa7ezd2appz12z67zzcfa_specializa7eza7(BgL_appz00_bglt
		BgL_nodez00_121)
	{
		{	/* Cfa/specialize.scm 552 */
			if (NULLP((((BgL_appz00_bglt) COBJECT(BgL_nodez00_121))->BgL_argsz00)))
				{	/* Cfa/specialize.scm 561 */
					return BgL_nodez00_121;
				}
			else
				{	/* Cfa/specialize.scm 565 */
					obj_t BgL_typez00_1849;

					BgL_typez00_1849 =
						BGl_normaliza7ezd2getzd2typeze70z40zzcfa_specializa7eza7(CAR(
							(((BgL_appz00_bglt) COBJECT(BgL_nodez00_121))->BgL_argsz00)));
					{	/* Cfa/specialize.scm 565 */
						BgL_variablez00_bglt BgL_gloz00_1850;

						BgL_gloz00_1850 =
							(((BgL_varz00_bglt) COBJECT(
									(((BgL_appz00_bglt) COBJECT(BgL_nodez00_121))->BgL_funz00)))->
							BgL_variablez00);
						{	/* Cfa/specialize.scm 566 */
							obj_t BgL_specz00_1851;

							{	/* Cfa/specialize.scm 567 */
								obj_t BgL_arg1831z00_1872;

								{
									BgL_specializa7edzd2globalz75_bglt BgL_auxz00_3796;

									{
										obj_t BgL_auxz00_3797;

										{	/* Cfa/specialize.scm 567 */
											BgL_objectz00_bglt BgL_tmpz00_3798;

											BgL_tmpz00_3798 =
												((BgL_objectz00_bglt)
												((BgL_globalz00_bglt) BgL_gloz00_1850));
											BgL_auxz00_3797 = BGL_OBJECT_WIDENING(BgL_tmpz00_3798);
										}
										BgL_auxz00_3796 =
											((BgL_specializa7edzd2globalz75_bglt) BgL_auxz00_3797);
									}
									BgL_arg1831z00_1872 =
										(((BgL_specializa7edzd2globalz75_bglt)
											COBJECT(BgL_auxz00_3796))->BgL_fixz00);
								}
								BgL_specz00_1851 =
									BGl_assqz00zz__r4_pairs_and_lists_6_3z00(BgL_typez00_1849,
									BgL_arg1831z00_1872);
							}
							{	/* Cfa/specialize.scm 567 */

								if (PAIRP(BgL_specz00_1851))
									{	/* Cfa/specialize.scm 569 */
										obj_t BgL_g1171z00_1853;

										BgL_g1171z00_1853 =
											CDR(
											(((BgL_appz00_bglt) COBJECT(BgL_nodez00_121))->
												BgL_argsz00));
										{
											obj_t BgL_argsz00_1855;

											BgL_argsz00_1855 = BgL_g1171z00_1853;
										BgL_zc3z04anonymousza31770ze3z87_1856:
											if (NULLP(BgL_argsz00_1855))
												{	/* Cfa/specialize.scm 571 */
													BGl_addzd2specializa7edzd2typez12zb5zzcfa_specializa7eza7
														(BgL_typez00_1849);
													{	/* Cfa/specialize.scm 573 */
														BgL_varz00_bglt BgL_arg1773z00_1858;
														obj_t BgL_arg1775z00_1859;

														BgL_arg1773z00_1858 =
															(((BgL_appz00_bglt) COBJECT(BgL_nodez00_121))->
															BgL_funz00);
														BgL_arg1775z00_1859 =
															CDR(((obj_t) BgL_specz00_1851));
														((((BgL_varz00_bglt) COBJECT(BgL_arg1773z00_1858))->
																BgL_variablez00) =
															((BgL_variablez00_bglt) ((BgL_variablez00_bglt)
																	BgL_arg1775z00_1859)), BUNSPEC);
													}
													{	/* Cfa/specialize.scm 574 */
														BgL_typez00_bglt BgL_ntz00_1860;

														BgL_ntz00_1860 =
															(((BgL_variablez00_bglt) COBJECT(
																	(((BgL_varz00_bglt) COBJECT(
																				(((BgL_appz00_bglt)
																						COBJECT(BgL_nodez00_121))->
																					BgL_funz00)))->BgL_variablez00)))->
															BgL_typez00);
														{	/* Cfa/specialize.scm 575 */
															BgL_typez00_bglt BgL_arg1798z00_1861;

															BgL_arg1798z00_1861 =
																BGl_strictzd2nodezd2typez00zzast_nodez00
																(BgL_ntz00_1860,
																((BgL_typez00_bglt) BgL_typez00_1849));
															((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																				BgL_nodez00_121)))->BgL_typez00) =
																((BgL_typez00_bglt) BgL_arg1798z00_1861),
																BUNSPEC);
														}
													}
													return BgL_nodez00_121;
												}
											else
												{	/* Cfa/specialize.scm 577 */
													bool_t BgL_test2199z00_3824;

													{	/* Cfa/specialize.scm 577 */
														obj_t BgL_arg1820z00_1868;

														{	/* Cfa/specialize.scm 577 */
															obj_t BgL_arg1822z00_1869;

															BgL_arg1822z00_1869 =
																CAR(((obj_t) BgL_argsz00_1855));
															BgL_arg1820z00_1868 =
																BGl_normaliza7ezd2getzd2typeze70z40zzcfa_specializa7eza7
																(BgL_arg1822z00_1869);
														}
														BgL_test2199z00_3824 =
															(BgL_arg1820z00_1868 == BgL_typez00_1849);
													}
													if (BgL_test2199z00_3824)
														{	/* Cfa/specialize.scm 578 */
															obj_t BgL_arg1812z00_1867;

															BgL_arg1812z00_1867 =
																CDR(((obj_t) BgL_argsz00_1855));
															{
																obj_t BgL_argsz00_3831;

																BgL_argsz00_3831 = BgL_arg1812z00_1867;
																BgL_argsz00_1855 = BgL_argsz00_3831;
																goto BgL_zc3z04anonymousza31770ze3z87_1856;
															}
														}
													else
														{	/* Cfa/specialize.scm 577 */
															BGL_TAIL return
																BGl_specializa7ezd2eqzf3z86zzcfa_specializa7eza7
																(BgL_nodez00_121);
														}
												}
										}
									}
								else
									{	/* Cfa/specialize.scm 568 */
										BGL_TAIL return
											BGl_specializa7ezd2eqzf3z86zzcfa_specializa7eza7
											(BgL_nodez00_121);
									}
							}
						}
					}
				}
		}

	}



/* normalize-get-type~0 */
	obj_t BGl_normaliza7ezd2getzd2typeze70z40zzcfa_specializa7eza7(obj_t
		BgL_valz00_1877)
	{
		{	/* Cfa/specialize.scm 558 */
			{	/* Cfa/specialize.scm 555 */
				BgL_typez00_bglt BgL_tyz00_1879;

				BgL_tyz00_1879 =
					BGl_getzd2typezd2zztype_typeofz00(
					((BgL_nodez00_bglt) BgL_valz00_1877), ((bool_t) 0));
				if ((((obj_t) BgL_tyz00_1879) == BGl_za2intza2z00zztype_cachez00))
					{	/* Cfa/specialize.scm 556 */
						return BGl_za2longza2z00zztype_cachez00;
					}
				else
					{	/* Cfa/specialize.scm 556 */
						return ((obj_t) BgL_tyz00_1879);
					}
			}
		}

	}



/* specialize-eq? */
	BgL_appz00_bglt
		BGl_specializa7ezd2eqzf3z86zzcfa_specializa7eza7(BgL_appz00_bglt
		BgL_nodez00_122)
	{
		{	/* Cfa/specialize.scm 591 */
			{
				BgL_appz00_bglt BgL_nodez00_1905;
				obj_t BgL_typez00_1906;

				{	/* Cfa/specialize.scm 598 */
					bool_t BgL_test2201z00_3840;

					{	/* Cfa/specialize.scm 598 */
						bool_t BgL_test2202z00_3841;

						{	/* Cfa/specialize.scm 598 */
							BgL_variablez00_bglt BgL_arg1854z00_1903;

							BgL_arg1854z00_1903 =
								(((BgL_varz00_bglt) COBJECT(
										(((BgL_appz00_bglt) COBJECT(BgL_nodez00_122))->
											BgL_funz00)))->BgL_variablez00);
							BgL_test2202z00_3841 =
								(((obj_t) BgL_arg1854z00_1903) ==
								BGl_za2czd2eqzf3za2z21zzcfa_specializa7eza7);
						}
						if (BgL_test2202z00_3841)
							{	/* Cfa/specialize.scm 599 */
								obj_t BgL_arg1853z00_1902;

								BgL_arg1853z00_1902 =
									BGl_thezd2backendzd2zzbackend_backendz00();
								BgL_test2201z00_3840 =
									(((BgL_backendz00_bglt) COBJECT(
											((BgL_backendz00_bglt) BgL_arg1853z00_1902)))->
									BgL_typedzd2eqzd2);
							}
						else
							{	/* Cfa/specialize.scm 598 */
								BgL_test2201z00_3840 = ((bool_t) 0);
							}
					}
					if (BgL_test2201z00_3840)
						{	/* Cfa/specialize.scm 601 */
							BgL_typez00_bglt BgL_t1z00_1890;
							BgL_typez00_bglt BgL_t2z00_1891;

							{	/* Cfa/specialize.scm 601 */
								obj_t BgL_arg1849z00_1897;

								BgL_arg1849z00_1897 =
									CAR(
									(((BgL_appz00_bglt) COBJECT(BgL_nodez00_122))->BgL_argsz00));
								BgL_t1z00_1890 =
									BGl_getzd2typezd2zztype_typeofz00(
									((BgL_nodez00_bglt) BgL_arg1849z00_1897), ((bool_t) 0));
							}
							{	/* Cfa/specialize.scm 602 */
								obj_t BgL_arg1851z00_1899;

								{	/* Cfa/specialize.scm 602 */
									obj_t BgL_pairz00_2723;

									BgL_pairz00_2723 =
										(((BgL_appz00_bglt) COBJECT(BgL_nodez00_122))->BgL_argsz00);
									BgL_arg1851z00_1899 = CAR(CDR(BgL_pairz00_2723));
								}
								BgL_t2z00_1891 =
									BGl_getzd2typezd2zztype_typeofz00(
									((BgL_nodez00_bglt) BgL_arg1851z00_1899), ((bool_t) 0));
							}
							{	/* Cfa/specialize.scm 604 */
								bool_t BgL_test2203z00_3858;

								if (
									(((obj_t) BgL_t1z00_1890) == BGl_za2objza2z00zztype_cachez00))
									{	/* Cfa/specialize.scm 604 */
										BgL_test2203z00_3858 =
											(
											((obj_t) BgL_t2z00_1891) ==
											BGl_za2objza2z00zztype_cachez00);
									}
								else
									{	/* Cfa/specialize.scm 604 */
										BgL_test2203z00_3858 = ((bool_t) 0);
									}
								if (BgL_test2203z00_3858)
									{	/* Cfa/specialize.scm 604 */
										BUNSPEC;
									}
								else
									{	/* Cfa/specialize.scm 604 */
										if (
											(((obj_t) BgL_t1z00_1890) ==
												BGl_za2bintza2z00zztype_cachez00))
											{	/* Cfa/specialize.scm 606 */
												if (
													(((obj_t) BgL_t2z00_1891) ==
														BGl_za2bintza2z00zztype_cachez00))
													{	/* Cfa/specialize.scm 607 */
														BgL_nodez00_1905 = BgL_nodez00_122;
														BgL_typez00_1906 = BGl_za2bintza2z00zztype_cachez00;
													BgL_zc3z04anonymousza31857ze3z87_1907:
														{	/* Cfa/specialize.scm 594 */
															bool_t BgL_test2207z00_3870;

															{	/* Cfa/specialize.scm 594 */
																obj_t BgL_objz00_2683;

																BgL_objz00_2683 =
																	BGl_za2boxedzd2eqzf3za2z21zzcfa_specializa7eza7;
																{	/* Cfa/specialize.scm 594 */
																	obj_t BgL_classz00_2684;

																	BgL_classz00_2684 = BGl_globalz00zzast_varz00;
																	if (BGL_OBJECTP(BgL_objz00_2683))
																		{	/* Cfa/specialize.scm 594 */
																			BgL_objectz00_bglt BgL_arg1807z00_2686;

																			BgL_arg1807z00_2686 =
																				(BgL_objectz00_bglt) (BgL_objz00_2683);
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Cfa/specialize.scm 594 */
																					long BgL_idxz00_2692;

																					BgL_idxz00_2692 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_2686);
																					BgL_test2207z00_3870 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_2692 + 2L)) ==
																						BgL_classz00_2684);
																				}
																			else
																				{	/* Cfa/specialize.scm 594 */
																					bool_t BgL_res2022z00_2717;

																					{	/* Cfa/specialize.scm 594 */
																						obj_t BgL_oclassz00_2700;

																						{	/* Cfa/specialize.scm 594 */
																							obj_t BgL_arg1815z00_2708;
																							long BgL_arg1816z00_2709;

																							BgL_arg1815z00_2708 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Cfa/specialize.scm 594 */
																								long BgL_arg1817z00_2710;

																								BgL_arg1817z00_2710 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_2686);
																								BgL_arg1816z00_2709 =
																									(BgL_arg1817z00_2710 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_2700 =
																								VECTOR_REF(BgL_arg1815z00_2708,
																								BgL_arg1816z00_2709);
																						}
																						{	/* Cfa/specialize.scm 594 */
																							bool_t BgL__ortest_1115z00_2701;

																							BgL__ortest_1115z00_2701 =
																								(BgL_classz00_2684 ==
																								BgL_oclassz00_2700);
																							if (BgL__ortest_1115z00_2701)
																								{	/* Cfa/specialize.scm 594 */
																									BgL_res2022z00_2717 =
																										BgL__ortest_1115z00_2701;
																								}
																							else
																								{	/* Cfa/specialize.scm 594 */
																									long BgL_odepthz00_2702;

																									{	/* Cfa/specialize.scm 594 */
																										obj_t BgL_arg1804z00_2703;

																										BgL_arg1804z00_2703 =
																											(BgL_oclassz00_2700);
																										BgL_odepthz00_2702 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_2703);
																									}
																									if ((2L < BgL_odepthz00_2702))
																										{	/* Cfa/specialize.scm 594 */
																											obj_t BgL_arg1802z00_2705;

																											{	/* Cfa/specialize.scm 594 */
																												obj_t
																													BgL_arg1803z00_2706;
																												BgL_arg1803z00_2706 =
																													(BgL_oclassz00_2700);
																												BgL_arg1802z00_2705 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_2706,
																													2L);
																											}
																											BgL_res2022z00_2717 =
																												(BgL_arg1802z00_2705 ==
																												BgL_classz00_2684);
																										}
																									else
																										{	/* Cfa/specialize.scm 594 */
																											BgL_res2022z00_2717 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2207z00_3870 =
																						BgL_res2022z00_2717;
																				}
																		}
																	else
																		{	/* Cfa/specialize.scm 594 */
																			BgL_test2207z00_3870 = ((bool_t) 0);
																		}
																}
															}
															if (BgL_test2207z00_3870)
																{	/* Cfa/specialize.scm 594 */
																	{	/* Cfa/specialize.scm 595 */
																		BgL_varz00_bglt BgL_arg1859z00_1909;

																		BgL_arg1859z00_1909 =
																			(((BgL_appz00_bglt)
																				COBJECT(BgL_nodez00_122))->BgL_funz00);
																		{	/* Cfa/specialize.scm 595 */
																			BgL_variablez00_bglt BgL_vz00_2719;

																			BgL_vz00_2719 =
																				((BgL_variablez00_bglt)
																				BGl_za2boxedzd2eqzf3za2z21zzcfa_specializa7eza7);
																			((((BgL_varz00_bglt)
																						COBJECT(BgL_arg1859z00_1909))->
																					BgL_variablez00) =
																				((BgL_variablez00_bglt) BgL_vz00_2719),
																				BUNSPEC);
																		}
																	}
																	BGl_addzd2specializa7edzd2eqzd2typez12z67zzcfa_specializa7eza7
																		(BgL_typez00_1906);
																}
															else
																{	/* Cfa/specialize.scm 594 */
																	BFALSE;
																}
														}
													}
												else
													{	/* Cfa/specialize.scm 607 */
														BFALSE;
													}
											}
										else
											{	/* Cfa/specialize.scm 606 */
												if (
													(((obj_t) BgL_t2z00_1891) ==
														BGl_za2bintza2z00zztype_cachez00))
													{	/* Cfa/specialize.scm 610 */
														BUNSPEC;
													}
												else
													{	/* Cfa/specialize.scm 610 */
														if (
															(((obj_t) BgL_t1z00_1890) ==
																BGl_za2brealza2z00zztype_cachez00))
															{	/* Cfa/specialize.scm 612 */
																if (
																	(((obj_t) BgL_t2z00_1891) ==
																		BGl_za2brealza2z00zztype_cachez00))
																	{
																		obj_t BgL_typez00_3907;
																		BgL_appz00_bglt BgL_nodez00_3906;

																		BgL_nodez00_3906 = BgL_nodez00_122;
																		BgL_typez00_3907 =
																			BGl_za2brealza2z00zztype_cachez00;
																		BgL_typez00_1906 = BgL_typez00_3907;
																		BgL_nodez00_1905 = BgL_nodez00_3906;
																		goto BgL_zc3z04anonymousza31857ze3z87_1907;
																	}
																else
																	{	/* Cfa/specialize.scm 613 */
																		BFALSE;
																	}
															}
														else
															{	/* Cfa/specialize.scm 612 */
																if (
																	(((obj_t) BgL_t2z00_1891) ==
																		BGl_za2brealza2z00zztype_cachez00))
																	{	/* Cfa/specialize.scm 616 */
																		BUNSPEC;
																	}
																else
																	{	/* Cfa/specialize.scm 616 */
																		if (
																			(((obj_t) BgL_t2z00_1891) ==
																				BGl_za2bintza2z00zztype_cachez00))
																			{	/* Cfa/specialize.scm 618 */
																				BUNSPEC;
																			}
																		else
																			{	/* Cfa/specialize.scm 618 */
																				if (
																					(((obj_t) BgL_t1z00_1890) ==
																						BGl_za2bcharza2z00zztype_cachez00))
																					{	/* Cfa/specialize.scm 620 */
																						if (
																							(((obj_t) BgL_t2z00_1891) ==
																								BGl_za2bcharza2z00zztype_cachez00))
																							{
																								obj_t BgL_typez00_3921;
																								BgL_appz00_bglt
																									BgL_nodez00_3920;
																								BgL_nodez00_3920 =
																									BgL_nodez00_122;
																								BgL_typez00_3921 =
																									BGl_za2bcharza2z00zztype_cachez00;
																								BgL_typez00_1906 =
																									BgL_typez00_3921;
																								BgL_nodez00_1905 =
																									BgL_nodez00_3920;
																								goto
																									BgL_zc3z04anonymousza31857ze3z87_1907;
																							}
																						else
																							{	/* Cfa/specialize.scm 621 */
																								BFALSE;
																							}
																					}
																				else
																					{	/* Cfa/specialize.scm 620 */
																						if (
																							(((obj_t) BgL_t2z00_1891) ==
																								BGl_za2bcharza2z00zztype_cachez00))
																							{	/* Cfa/specialize.scm 624 */
																								BUNSPEC;
																							}
																						else
																							{	/* Cfa/specialize.scm 626 */
																								bool_t BgL_test2220z00_3925;

																								if (BGl_bigloozd2typezf3z21zztype_typez00(BgL_t1z00_1890))
																									{	/* Cfa/specialize.scm 626 */
																										BgL_test2220z00_3925 =
																											BGl_bigloozd2typezf3z21zztype_typez00
																											(BgL_t2z00_1891);
																									}
																								else
																									{	/* Cfa/specialize.scm 626 */
																										BgL_test2220z00_3925 =
																											((bool_t) 0);
																									}
																								if (BgL_test2220z00_3925)
																									{	/* Cfa/specialize.scm 628 */
																										BgL_typez00_bglt
																											BgL_arg1848z00_1895;
																										if ((((obj_t)
																													BgL_t1z00_1890) ==
																												BGl_za2objza2z00zztype_cachez00))
																											{	/* Cfa/specialize.scm 628 */
																												BgL_arg1848z00_1895 =
																													BgL_t2z00_1891;
																											}
																										else
																											{	/* Cfa/specialize.scm 628 */
																												BgL_arg1848z00_1895 =
																													BgL_t1z00_1890;
																											}
																										{
																											obj_t BgL_typez00_3933;
																											BgL_appz00_bglt
																												BgL_nodez00_3932;
																											BgL_nodez00_3932 =
																												BgL_nodez00_122;
																											BgL_typez00_3933 =
																												((obj_t)
																												BgL_arg1848z00_1895);
																											BgL_typez00_1906 =
																												BgL_typez00_3933;
																											BgL_nodez00_1905 =
																												BgL_nodez00_3932;
																											goto
																												BgL_zc3z04anonymousza31857ze3z87_1907;
																										}
																									}
																								else
																									{	/* Cfa/specialize.scm 626 */
																										BUNSPEC;
																									}
																							}
																					}
																			}
																	}
															}
													}
											}
									}
							}
							return BgL_nodez00_122;
						}
					else
						{	/* Cfa/specialize.scm 598 */
							return BgL_nodez00_122;
						}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_specializa7eza7(void)
	{
		{	/* Cfa/specialize.scm 19 */
			{	/* Cfa/specialize.scm 35 */
				obj_t BgL_arg1866z00_1915;
				obj_t BgL_arg1868z00_1916;

				{	/* Cfa/specialize.scm 35 */
					obj_t BgL_v1317z00_1958;

					BgL_v1317z00_1958 = create_vector(1L);
					{	/* Cfa/specialize.scm 35 */
						obj_t BgL_arg1880z00_1959;

						BgL_arg1880z00_1959 =
							BGl_makezd2classzd2fieldz00zz__objectz00(CNST_TABLE_REF(8),
							BGl_proc2049z00zzcfa_specializa7eza7,
							BGl_proc2048z00zzcfa_specializa7eza7, ((bool_t) 0), ((bool_t) 0),
							BFALSE, BFALSE, CNST_TABLE_REF(9));
						VECTOR_SET(BgL_v1317z00_1958, 0L, BgL_arg1880z00_1959);
					}
					BgL_arg1866z00_1915 = BgL_v1317z00_1958;
				}
				{	/* Cfa/specialize.scm 35 */
					obj_t BgL_v1318z00_1969;

					BgL_v1318z00_1969 = create_vector(0L);
					BgL_arg1868z00_1916 = BgL_v1318z00_1969;
				}
				return (BGl_specializa7edzd2globalz75zzcfa_specializa7eza7 =
					BGl_registerzd2classz12zc0zz__objectz00(CNST_TABLE_REF(10),
						CNST_TABLE_REF(11), BGl_globalz00zzast_varz00, 16878L,
						BGl_proc2053z00zzcfa_specializa7eza7,
						BGl_proc2052z00zzcfa_specializa7eza7, BFALSE,
						BGl_proc2051z00zzcfa_specializa7eza7,
						BGl_proc2050z00zzcfa_specializa7eza7, BgL_arg1866z00_1915,
						BgL_arg1868z00_1916), BUNSPEC);
			}
		}

	}



/* &lambda1876 */
	BgL_globalz00_bglt BGl_z62lambda1876z62zzcfa_specializa7eza7(obj_t
		BgL_envz00_3012, obj_t BgL_o1130z00_3013)
	{
		{	/* Cfa/specialize.scm 35 */
			{	/* Cfa/specialize.scm 35 */
				long BgL_arg1877z00_3128;

				{	/* Cfa/specialize.scm 35 */
					obj_t BgL_arg1878z00_3129;

					{	/* Cfa/specialize.scm 35 */
						obj_t BgL_arg1879z00_3130;

						{	/* Cfa/specialize.scm 35 */
							obj_t BgL_arg1815z00_3131;
							long BgL_arg1816z00_3132;

							BgL_arg1815z00_3131 = (BGl_za2classesza2z00zz__objectz00);
							{	/* Cfa/specialize.scm 35 */
								long BgL_arg1817z00_3133;

								BgL_arg1817z00_3133 =
									BGL_OBJECT_CLASS_NUM(
									((BgL_objectz00_bglt)
										((BgL_globalz00_bglt) BgL_o1130z00_3013)));
								BgL_arg1816z00_3132 = (BgL_arg1817z00_3133 - OBJECT_TYPE);
							}
							BgL_arg1879z00_3130 =
								VECTOR_REF(BgL_arg1815z00_3131, BgL_arg1816z00_3132);
						}
						BgL_arg1878z00_3129 =
							BGl_classzd2superzd2zz__objectz00(BgL_arg1879z00_3130);
					}
					{	/* Cfa/specialize.scm 35 */
						obj_t BgL_tmpz00_3951;

						BgL_tmpz00_3951 = ((obj_t) BgL_arg1878z00_3129);
						BgL_arg1877z00_3128 = BGL_CLASS_NUM(BgL_tmpz00_3951);
				}}
				BGL_OBJECT_CLASS_NUM_SET(
					((BgL_objectz00_bglt)
						((BgL_globalz00_bglt) BgL_o1130z00_3013)), BgL_arg1877z00_3128);
			}
			{	/* Cfa/specialize.scm 35 */
				BgL_objectz00_bglt BgL_tmpz00_3957;

				BgL_tmpz00_3957 =
					((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_o1130z00_3013));
				BGL_OBJECT_WIDENING_SET(BgL_tmpz00_3957, BFALSE);
			}
			((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_o1130z00_3013));
			return ((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1130z00_3013));
		}

	}



/* &<@anonymous:1875> */
	obj_t BGl_z62zc3z04anonymousza31875ze3ze5zzcfa_specializa7eza7(obj_t
		BgL_envz00_3014, obj_t BgL_new1129z00_3015)
	{
		{	/* Cfa/specialize.scm 35 */
			{
				BgL_globalz00_bglt BgL_auxz00_3965;

				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_new1129z00_3015))))->BgL_idz00) =
					((obj_t) CNST_TABLE_REF(12)), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_namez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_typez00_bglt BgL_auxz00_3973;

					{	/* Cfa/specialize.scm 35 */
						obj_t BgL_classz00_3135;

						BgL_classz00_3135 = BGl_typez00zztype_typez00;
						{	/* Cfa/specialize.scm 35 */
							obj_t BgL__ortest_1117z00_3136;

							BgL__ortest_1117z00_3136 = BGL_CLASS_NIL(BgL_classz00_3135);
							if (CBOOL(BgL__ortest_1117z00_3136))
								{	/* Cfa/specialize.scm 35 */
									BgL_auxz00_3973 =
										((BgL_typez00_bglt) BgL__ortest_1117z00_3136);
								}
							else
								{	/* Cfa/specialize.scm 35 */
									BgL_auxz00_3973 =
										((BgL_typez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3135));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_new1129z00_3015))))->
							BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_3973), BUNSPEC);
				}
				{
					BgL_valuez00_bglt BgL_auxz00_3983;

					{	/* Cfa/specialize.scm 35 */
						obj_t BgL_classz00_3137;

						BgL_classz00_3137 = BGl_valuez00zzast_varz00;
						{	/* Cfa/specialize.scm 35 */
							obj_t BgL__ortest_1117z00_3138;

							BgL__ortest_1117z00_3138 = BGL_CLASS_NIL(BgL_classz00_3137);
							if (CBOOL(BgL__ortest_1117z00_3138))
								{	/* Cfa/specialize.scm 35 */
									BgL_auxz00_3983 =
										((BgL_valuez00_bglt) BgL__ortest_1117z00_3138);
								}
							else
								{	/* Cfa/specialize.scm 35 */
									BgL_auxz00_3983 =
										((BgL_valuez00_bglt)
										BGl_classzd2nilzd2initz12z12zz__objectz00
										(BgL_classz00_3137));
								}
						}
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_new1129z00_3015))))->
							BgL_valuez00) = ((BgL_valuez00_bglt) BgL_auxz00_3983), BUNSPEC);
				}
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_globalz00_bglt) BgL_new1129z00_3015))))->
						BgL_accessz00) = ((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_fastzd2alphazd2) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_removablez00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_occurrencez00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_occurrencewz00) =
					((long) 0L), BUNSPEC);
				((((BgL_variablez00_bglt)
							COBJECT(((BgL_variablez00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_userzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_modulez00) =
					((obj_t) CNST_TABLE_REF(12)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_importz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_evaluablezf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_evalzf3zf3) =
					((bool_t) ((bool_t) 0)), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_libraryz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_pragmaz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_srcz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_jvmzd2typezd2namez00) =
					((obj_t) BGl_string2054z00zzcfa_specializa7eza7), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_initz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				((((BgL_globalz00_bglt)
							COBJECT(((BgL_globalz00_bglt) ((BgL_globalz00_bglt)
										BgL_new1129z00_3015))))->BgL_aliasz00) =
					((obj_t) BUNSPEC), BUNSPEC);
				{
					BgL_specializa7edzd2globalz75_bglt BgL_auxz00_4042;

					{
						obj_t BgL_auxz00_4043;

						{	/* Cfa/specialize.scm 35 */
							BgL_objectz00_bglt BgL_tmpz00_4044;

							BgL_tmpz00_4044 =
								((BgL_objectz00_bglt)
								((BgL_globalz00_bglt) BgL_new1129z00_3015));
							BgL_auxz00_4043 = BGL_OBJECT_WIDENING(BgL_tmpz00_4044);
						}
						BgL_auxz00_4042 =
							((BgL_specializa7edzd2globalz75_bglt) BgL_auxz00_4043);
					}
					((((BgL_specializa7edzd2globalz75_bglt) COBJECT(BgL_auxz00_4042))->
							BgL_fixz00) = ((obj_t) BUNSPEC), BUNSPEC);
				}
				BgL_auxz00_3965 = ((BgL_globalz00_bglt) BgL_new1129z00_3015);
				return ((obj_t) BgL_auxz00_3965);
			}
		}

	}



/* &lambda1873 */
	BgL_globalz00_bglt BGl_z62lambda1873z62zzcfa_specializa7eza7(obj_t
		BgL_envz00_3016, obj_t BgL_o1126z00_3017)
	{
		{	/* Cfa/specialize.scm 35 */
			{	/* Cfa/specialize.scm 35 */
				BgL_specializa7edzd2globalz75_bglt BgL_wide1128z00_3140;

				BgL_wide1128z00_3140 =
					((BgL_specializa7edzd2globalz75_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_specializa7edzd2globalz75_bgl))));
				{	/* Cfa/specialize.scm 35 */
					obj_t BgL_auxz00_4057;
					BgL_objectz00_bglt BgL_tmpz00_4053;

					BgL_auxz00_4057 = ((obj_t) BgL_wide1128z00_3140);
					BgL_tmpz00_4053 =
						((BgL_objectz00_bglt)
						((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1126z00_3017)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4053, BgL_auxz00_4057);
				}
				((BgL_objectz00_bglt)
					((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1126z00_3017)));
				{	/* Cfa/specialize.scm 35 */
					long BgL_arg1874z00_3141;

					BgL_arg1874z00_3141 =
						BGL_CLASS_NUM(BGl_specializa7edzd2globalz75zzcfa_specializa7eza7);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_globalz00_bglt)
								((BgL_globalz00_bglt) BgL_o1126z00_3017))),
						BgL_arg1874z00_3141);
				}
				return
					((BgL_globalz00_bglt)
					((BgL_globalz00_bglt) ((BgL_globalz00_bglt) BgL_o1126z00_3017)));
			}
		}

	}



/* &lambda1869 */
	BgL_globalz00_bglt BGl_z62lambda1869z62zzcfa_specializa7eza7(obj_t
		BgL_envz00_3018, obj_t BgL_id1105z00_3019, obj_t BgL_name1106z00_3020,
		obj_t BgL_type1107z00_3021, obj_t BgL_value1108z00_3022,
		obj_t BgL_access1109z00_3023, obj_t BgL_fastzd2alpha1110zd2_3024,
		obj_t BgL_removable1111z00_3025, obj_t BgL_occurrence1112z00_3026,
		obj_t BgL_occurrencew1113z00_3027, obj_t BgL_userzf31114zf3_3028,
		obj_t BgL_module1115z00_3029, obj_t BgL_import1116z00_3030,
		obj_t BgL_evaluablezf31117zf3_3031, obj_t BgL_evalzf31118zf3_3032,
		obj_t BgL_library1119z00_3033, obj_t BgL_pragma1120z00_3034,
		obj_t BgL_src1121z00_3035, obj_t BgL_jvmzd2typezd2name1122z00_3036,
		obj_t BgL_init1123z00_3037, obj_t BgL_alias1124z00_3038,
		obj_t BgL_fix1125z00_3039)
	{
		{	/* Cfa/specialize.scm 35 */
			{	/* Cfa/specialize.scm 35 */
				long BgL_occurrence1112z00_3145;
				long BgL_occurrencew1113z00_3146;
				bool_t BgL_userzf31114zf3_3147;
				bool_t BgL_evaluablezf31117zf3_3149;
				bool_t BgL_evalzf31118zf3_3150;

				BgL_occurrence1112z00_3145 = (long) CINT(BgL_occurrence1112z00_3026);
				BgL_occurrencew1113z00_3146 = (long) CINT(BgL_occurrencew1113z00_3027);
				BgL_userzf31114zf3_3147 = CBOOL(BgL_userzf31114zf3_3028);
				BgL_evaluablezf31117zf3_3149 = CBOOL(BgL_evaluablezf31117zf3_3031);
				BgL_evalzf31118zf3_3150 = CBOOL(BgL_evalzf31118zf3_3032);
				{	/* Cfa/specialize.scm 35 */
					BgL_globalz00_bglt BgL_new1176z00_3152;

					{	/* Cfa/specialize.scm 35 */
						BgL_globalz00_bglt BgL_tmp1174z00_3153;
						BgL_specializa7edzd2globalz75_bglt BgL_wide1175z00_3154;

						{
							BgL_globalz00_bglt BgL_auxz00_4076;

							{	/* Cfa/specialize.scm 35 */
								BgL_globalz00_bglt BgL_new1173z00_3155;

								BgL_new1173z00_3155 =
									((BgL_globalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_globalz00_bgl))));
								{	/* Cfa/specialize.scm 35 */
									long BgL_arg1872z00_3156;

									BgL_arg1872z00_3156 =
										BGL_CLASS_NUM(BGl_globalz00zzast_varz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1173z00_3155),
										BgL_arg1872z00_3156);
								}
								{	/* Cfa/specialize.scm 35 */
									BgL_objectz00_bglt BgL_tmpz00_4081;

									BgL_tmpz00_4081 = ((BgL_objectz00_bglt) BgL_new1173z00_3155);
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4081, BFALSE);
								}
								((BgL_objectz00_bglt) BgL_new1173z00_3155);
								BgL_auxz00_4076 = BgL_new1173z00_3155;
							}
							BgL_tmp1174z00_3153 = ((BgL_globalz00_bglt) BgL_auxz00_4076);
						}
						BgL_wide1175z00_3154 =
							((BgL_specializa7edzd2globalz75_bglt)
							BOBJECT(GC_MALLOC(sizeof(struct
										BgL_specializa7edzd2globalz75_bgl))));
						{	/* Cfa/specialize.scm 35 */
							obj_t BgL_auxz00_4089;
							BgL_objectz00_bglt BgL_tmpz00_4087;

							BgL_auxz00_4089 = ((obj_t) BgL_wide1175z00_3154);
							BgL_tmpz00_4087 = ((BgL_objectz00_bglt) BgL_tmp1174z00_3153);
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4087, BgL_auxz00_4089);
						}
						((BgL_objectz00_bglt) BgL_tmp1174z00_3153);
						{	/* Cfa/specialize.scm 35 */
							long BgL_arg1870z00_3157;

							BgL_arg1870z00_3157 =
								BGL_CLASS_NUM
								(BGl_specializa7edzd2globalz75zzcfa_specializa7eza7);
							BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt)
									BgL_tmp1174z00_3153), BgL_arg1870z00_3157);
						}
						BgL_new1176z00_3152 = ((BgL_globalz00_bglt) BgL_tmp1174z00_3153);
					}
					((((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_new1176z00_3152)))->BgL_idz00) =
						((obj_t) ((obj_t) BgL_id1105z00_3019)), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1176z00_3152)))->BgL_namez00) =
						((obj_t) BgL_name1106z00_3020), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1176z00_3152)))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_type1107z00_3021)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1176z00_3152)))->BgL_valuez00) =
						((BgL_valuez00_bglt) ((BgL_valuez00_bglt) BgL_value1108z00_3022)),
						BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1176z00_3152)))->BgL_accessz00) =
						((obj_t) BgL_access1109z00_3023), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1176z00_3152)))->BgL_fastzd2alphazd2) =
						((obj_t) BgL_fastzd2alpha1110zd2_3024), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1176z00_3152)))->BgL_removablez00) =
						((obj_t) BgL_removable1111z00_3025), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1176z00_3152)))->BgL_occurrencez00) =
						((long) BgL_occurrence1112z00_3145), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1176z00_3152)))->BgL_occurrencewz00) =
						((long) BgL_occurrencew1113z00_3146), BUNSPEC);
					((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
										BgL_new1176z00_3152)))->BgL_userzf3zf3) =
						((bool_t) BgL_userzf31114zf3_3147), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1176z00_3152)))->BgL_modulez00) =
						((obj_t) ((obj_t) BgL_module1115z00_3029)), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1176z00_3152)))->BgL_importz00) =
						((obj_t) BgL_import1116z00_3030), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1176z00_3152)))->BgL_evaluablezf3zf3) =
						((bool_t) BgL_evaluablezf31117zf3_3149), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1176z00_3152)))->BgL_evalzf3zf3) =
						((bool_t) BgL_evalzf31118zf3_3150), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1176z00_3152)))->BgL_libraryz00) =
						((obj_t) BgL_library1119z00_3033), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1176z00_3152)))->BgL_pragmaz00) =
						((obj_t) BgL_pragma1120z00_3034), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1176z00_3152)))->BgL_srcz00) =
						((obj_t) BgL_src1121z00_3035), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1176z00_3152)))->BgL_jvmzd2typezd2namez00) =
						((obj_t) ((obj_t) BgL_jvmzd2typezd2name1122z00_3036)), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1176z00_3152)))->BgL_initz00) =
						((obj_t) BgL_init1123z00_3037), BUNSPEC);
					((((BgL_globalz00_bglt) COBJECT(((BgL_globalz00_bglt)
										BgL_new1176z00_3152)))->BgL_aliasz00) =
						((obj_t) BgL_alias1124z00_3038), BUNSPEC);
					{
						BgL_specializa7edzd2globalz75_bglt BgL_auxz00_4142;

						{
							obj_t BgL_auxz00_4143;

							{	/* Cfa/specialize.scm 35 */
								BgL_objectz00_bglt BgL_tmpz00_4144;

								BgL_tmpz00_4144 = ((BgL_objectz00_bglt) BgL_new1176z00_3152);
								BgL_auxz00_4143 = BGL_OBJECT_WIDENING(BgL_tmpz00_4144);
							}
							BgL_auxz00_4142 =
								((BgL_specializa7edzd2globalz75_bglt) BgL_auxz00_4143);
						}
						((((BgL_specializa7edzd2globalz75_bglt) COBJECT(BgL_auxz00_4142))->
								BgL_fixz00) = ((obj_t) BgL_fix1125z00_3039), BUNSPEC);
					}
					return BgL_new1176z00_3152;
				}
			}
		}

	}



/* &lambda1885 */
	obj_t BGl_z62lambda1885z62zzcfa_specializa7eza7(obj_t BgL_envz00_3040,
		obj_t BgL_oz00_3041, obj_t BgL_vz00_3042)
	{
		{	/* Cfa/specialize.scm 35 */
			{
				BgL_specializa7edzd2globalz75_bglt BgL_auxz00_4149;

				{
					obj_t BgL_auxz00_4150;

					{	/* Cfa/specialize.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_4151;

						BgL_tmpz00_4151 =
							((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_oz00_3041));
						BgL_auxz00_4150 = BGL_OBJECT_WIDENING(BgL_tmpz00_4151);
					}
					BgL_auxz00_4149 =
						((BgL_specializa7edzd2globalz75_bglt) BgL_auxz00_4150);
				}
				return
					((((BgL_specializa7edzd2globalz75_bglt) COBJECT(BgL_auxz00_4149))->
						BgL_fixz00) = ((obj_t) BgL_vz00_3042), BUNSPEC);
			}
		}

	}



/* &lambda1884 */
	obj_t BGl_z62lambda1884z62zzcfa_specializa7eza7(obj_t BgL_envz00_3043,
		obj_t BgL_oz00_3044)
	{
		{	/* Cfa/specialize.scm 35 */
			{
				BgL_specializa7edzd2globalz75_bglt BgL_auxz00_4157;

				{
					obj_t BgL_auxz00_4158;

					{	/* Cfa/specialize.scm 35 */
						BgL_objectz00_bglt BgL_tmpz00_4159;

						BgL_tmpz00_4159 =
							((BgL_objectz00_bglt) ((BgL_globalz00_bglt) BgL_oz00_3044));
						BgL_auxz00_4158 = BGL_OBJECT_WIDENING(BgL_tmpz00_4159);
					}
					BgL_auxz00_4157 =
						((BgL_specializa7edzd2globalz75_bglt) BgL_auxz00_4158);
				}
				return
					(((BgL_specializa7edzd2globalz75_bglt) COBJECT(BgL_auxz00_4157))->
					BgL_fixz00);
			}
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_specializa7eza7(void)
	{
		{	/* Cfa/specialize.scm 19 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7,
				BGl_proc2055z00zzcfa_specializa7eza7, BGl_nodez00zzast_nodez00,
				BGl_string2056z00zzcfa_specializa7eza7);
		}

	}



/* &patch!1319 */
	obj_t BGl_z62patchz121319z70zzcfa_specializa7eza7(obj_t BgL_envz00_3046,
		obj_t BgL_nodez00_3047)
	{
		{	/* Cfa/specialize.scm 326 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(13),
				BGl_string2057z00zzcfa_specializa7eza7,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_3047)));
		}

	}



/* patch! */
	obj_t BGl_patchz12z12zzcfa_specializa7eza7(BgL_nodez00_bglt BgL_nodez00_97)
	{
		{	/* Cfa/specialize.scm 326 */
			{	/* Cfa/specialize.scm 326 */
				obj_t BgL_method1320z00_1978;

				{	/* Cfa/specialize.scm 326 */
					obj_t BgL_res2027z00_2797;

					{	/* Cfa/specialize.scm 326 */
						long BgL_objzd2classzd2numz00_2768;

						BgL_objzd2classzd2numz00_2768 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_97));
						{	/* Cfa/specialize.scm 326 */
							obj_t BgL_arg1811z00_2769;

							BgL_arg1811z00_2769 =
								PROCEDURE_REF(BGl_patchz12zd2envzc0zzcfa_specializa7eza7,
								(int) (1L));
							{	/* Cfa/specialize.scm 326 */
								int BgL_offsetz00_2772;

								BgL_offsetz00_2772 = (int) (BgL_objzd2classzd2numz00_2768);
								{	/* Cfa/specialize.scm 326 */
									long BgL_offsetz00_2773;

									BgL_offsetz00_2773 =
										((long) (BgL_offsetz00_2772) - OBJECT_TYPE);
									{	/* Cfa/specialize.scm 326 */
										long BgL_modz00_2774;

										BgL_modz00_2774 =
											(BgL_offsetz00_2773 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/specialize.scm 326 */
											long BgL_restz00_2776;

											BgL_restz00_2776 =
												(BgL_offsetz00_2773 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/specialize.scm 326 */

												{	/* Cfa/specialize.scm 326 */
													obj_t BgL_bucketz00_2778;

													BgL_bucketz00_2778 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_2769), BgL_modz00_2774);
													BgL_res2027z00_2797 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_2778), BgL_restz00_2776);
					}}}}}}}}
					BgL_method1320z00_1978 = BgL_res2027z00_2797;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1320z00_1978, ((obj_t) BgL_nodez00_97));
			}
		}

	}



/* &patch! */
	obj_t BGl_z62patchz12z70zzcfa_specializa7eza7(obj_t BgL_envz00_3048,
		obj_t BgL_nodez00_3049)
	{
		{	/* Cfa/specialize.scm 326 */
			return
				BGl_patchz12z12zzcfa_specializa7eza7(
				((BgL_nodez00_bglt) BgL_nodez00_3049));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_specializa7eza7(void)
	{
		{	/* Cfa/specialize.scm 19 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7, BGl_atomz00zzast_nodez00,
				BGl_proc2058z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7, BGl_kwotez00zzast_nodez00,
				BGl_proc2060z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7, BGl_varz00zzast_nodez00,
				BGl_proc2061z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7,
				BGl_closurez00zzast_nodez00, BGl_proc2062z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7,
				BGl_sequencez00zzast_nodez00, BGl_proc2063z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7, BGl_syncz00zzast_nodez00,
				BGl_proc2064z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2065z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7,
				BGl_funcallz00zzast_nodez00, BGl_proc2066z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7, BGl_externz00zzast_nodez00,
				BGl_proc2067z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7, BGl_castz00zzast_nodez00,
				BGl_proc2068z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7, BGl_setqz00zzast_nodez00,
				BGl_proc2069z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7,
				BGl_conditionalz00zzast_nodez00, BGl_proc2070z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7, BGl_failz00zzast_nodez00,
				BGl_proc2071z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7, BGl_switchz00zzast_nodez00,
				BGl_proc2072z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2073z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2074z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2075z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7,
				BGl_jumpzd2exzd2itz00zzast_nodez00,
				BGl_proc2076z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2077z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2078z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2079z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_specializa7eza7, BGl_appz00zzast_nodez00,
				BGl_proc2080z00zzcfa_specializa7eza7,
				BGl_string2059z00zzcfa_specializa7eza7);
		}

	}



/* &patch!-app1364 */
	obj_t BGl_z62patchz12zd2app1364za2zzcfa_specializa7eza7(obj_t BgL_envz00_3072,
		obj_t BgL_nodez00_3073)
	{
		{	/* Cfa/specialize.scm 538 */
			{
				BgL_appz00_bglt BgL_auxz00_4224;

				BGl_patchza2z12zb0zzcfa_specializa7eza7(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_3073)))->BgL_argsz00));
				{
					BgL_varz00_bglt BgL_auxz00_4228;

					{	/* Cfa/specialize.scm 541 */
						BgL_varz00_bglt BgL_arg1957z00_3162;

						BgL_arg1957z00_3162 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nodez00_3073)))->BgL_funz00);
						BgL_auxz00_4228 =
							((BgL_varz00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(
								((BgL_nodez00_bglt) BgL_arg1957z00_3162)));
					}
					((((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nodez00_3073)))->BgL_funz00) =
						((BgL_varz00_bglt) BgL_auxz00_4228), BUNSPEC);
				}
				{	/* Cfa/specialize.scm 542 */
					BgL_variablez00_bglt BgL_vz00_3163;

					BgL_vz00_3163 =
						(((BgL_varz00_bglt) COBJECT(
								(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_3073)))->BgL_funz00)))->
						BgL_variablez00);
					{	/* Cfa/specialize.scm 544 */
						bool_t BgL_test2225z00_4239;

						{	/* Cfa/specialize.scm 544 */
							obj_t BgL_classz00_3164;

							BgL_classz00_3164 =
								BGl_specializa7edzd2globalz75zzcfa_specializa7eza7;
							{	/* Cfa/specialize.scm 544 */
								obj_t BgL_oclassz00_3165;

								{	/* Cfa/specialize.scm 544 */
									obj_t BgL_arg1815z00_3166;
									long BgL_arg1816z00_3167;

									BgL_arg1815z00_3166 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Cfa/specialize.scm 544 */
										long BgL_arg1817z00_3168;

										BgL_arg1817z00_3168 =
											BGL_OBJECT_CLASS_NUM(
											((BgL_objectz00_bglt) BgL_vz00_3163));
										BgL_arg1816z00_3167 = (BgL_arg1817z00_3168 - OBJECT_TYPE);
									}
									BgL_oclassz00_3165 =
										VECTOR_REF(BgL_arg1815z00_3166, BgL_arg1816z00_3167);
								}
								BgL_test2225z00_4239 =
									(BgL_oclassz00_3165 == BgL_classz00_3164);
						}}
						if (BgL_test2225z00_4239)
							{	/* Cfa/specialize.scm 544 */
								BgL_auxz00_4224 =
									BGl_specializa7ezd2appz12z67zzcfa_specializa7eza7(
									((BgL_appz00_bglt) BgL_nodez00_3073));
							}
						else
							{	/* Cfa/specialize.scm 544 */
								BgL_auxz00_4224 = ((BgL_appz00_bglt) BgL_nodez00_3073);
							}
					}
				}
				return ((obj_t) BgL_auxz00_4224);
			}
		}

	}



/* &patch!-box-ref1362 */
	obj_t BGl_z62patchz12zd2boxzd2ref1362z70zzcfa_specializa7eza7(obj_t
		BgL_envz00_3074, obj_t BgL_nodez00_3075)
	{
		{	/* Cfa/specialize.scm 519 */
			{
				BgL_boxzd2refzd2_bglt BgL_auxz00_4250;

				{
					BgL_varz00_bglt BgL_auxz00_4251;

					{	/* Cfa/specialize.scm 521 */
						BgL_varz00_bglt BgL_arg1955z00_3170;

						BgL_arg1955z00_3170 =
							(((BgL_boxzd2refzd2_bglt) COBJECT(
									((BgL_boxzd2refzd2_bglt) BgL_nodez00_3075)))->BgL_varz00);
						BgL_auxz00_4251 =
							((BgL_varz00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(
								((BgL_nodez00_bglt) BgL_arg1955z00_3170)));
					}
					((((BgL_boxzd2refzd2_bglt) COBJECT(
									((BgL_boxzd2refzd2_bglt) BgL_nodez00_3075)))->BgL_varz00) =
						((BgL_varz00_bglt) BgL_auxz00_4251), BUNSPEC);
				}
				BgL_auxz00_4250 = ((BgL_boxzd2refzd2_bglt) BgL_nodez00_3075);
				return ((obj_t) BgL_auxz00_4250);
			}
		}

	}



/* &patch!-box-set!1360 */
	obj_t BGl_z62patchz12zd2boxzd2setz121360z62zzcfa_specializa7eza7(obj_t
		BgL_envz00_3076, obj_t BgL_nodez00_3077)
	{
		{	/* Cfa/specialize.scm 510 */
			{
				BgL_boxzd2setz12zc0_bglt BgL_auxz00_4261;

				{
					BgL_varz00_bglt BgL_auxz00_4262;

					{	/* Cfa/specialize.scm 512 */
						BgL_varz00_bglt BgL_arg1953z00_3172;

						BgL_arg1953z00_3172 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3077)))->BgL_varz00);
						BgL_auxz00_4262 =
							((BgL_varz00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(
								((BgL_nodez00_bglt) BgL_arg1953z00_3172)));
					}
					((((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3077)))->BgL_varz00) =
						((BgL_varz00_bglt) BgL_auxz00_4262), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4270;

					{	/* Cfa/specialize.scm 513 */
						BgL_nodez00_bglt BgL_arg1954z00_3173;

						BgL_arg1954z00_3173 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3077)))->
							BgL_valuez00);
						BgL_auxz00_4270 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1954z00_3173));
					}
					((((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3077)))->
							BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_4270), BUNSPEC);
				}
				BgL_auxz00_4261 = ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3077);
				return ((obj_t) BgL_auxz00_4261);
			}
		}

	}



/* &patch!-make-box1358 */
	obj_t BGl_z62patchz12zd2makezd2box1358z70zzcfa_specializa7eza7(obj_t
		BgL_envz00_3078, obj_t BgL_nodez00_3079)
	{
		{	/* Cfa/specialize.scm 502 */
			{
				BgL_makezd2boxzd2_bglt BgL_auxz00_4279;

				{
					BgL_nodez00_bglt BgL_auxz00_4280;

					{	/* Cfa/specialize.scm 504 */
						BgL_nodez00_bglt BgL_arg1952z00_3175;

						BgL_arg1952z00_3175 =
							(((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_3079)))->BgL_valuez00);
						BgL_auxz00_4280 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1952z00_3175));
					}
					((((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_3079)))->BgL_valuez00) =
						((BgL_nodez00_bglt) BgL_auxz00_4280), BUNSPEC);
				}
				BgL_auxz00_4279 = ((BgL_makezd2boxzd2_bglt) BgL_nodez00_3079);
				return ((obj_t) BgL_auxz00_4279);
			}
		}

	}



/* &patch!-jump-ex-it1356 */
	obj_t BGl_z62patchz12zd2jumpzd2exzd2it1356za2zzcfa_specializa7eza7(obj_t
		BgL_envz00_3080, obj_t BgL_nodez00_3081)
	{
		{	/* Cfa/specialize.scm 493 */
			{
				BgL_jumpzd2exzd2itz00_bglt BgL_auxz00_4289;

				{
					BgL_nodez00_bglt BgL_auxz00_4290;

					{	/* Cfa/specialize.scm 495 */
						BgL_nodez00_bglt BgL_arg1950z00_3177;

						BgL_arg1950z00_3177 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3081)))->
							BgL_exitz00);
						BgL_auxz00_4290 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1950z00_3177));
					}
					((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3081)))->
							BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_4290), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4297;

					{	/* Cfa/specialize.scm 496 */
						BgL_nodez00_bglt BgL_arg1951z00_3178;

						BgL_arg1951z00_3178 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3081)))->
							BgL_valuez00);
						BgL_auxz00_4297 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1951z00_3178));
					}
					((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3081)))->
							BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_4297), BUNSPEC);
				}
				BgL_auxz00_4289 = ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_3081);
				return ((obj_t) BgL_auxz00_4289);
			}
		}

	}



/* &patch!-set-ex-it1354 */
	obj_t BGl_z62patchz12zd2setzd2exzd2it1354za2zzcfa_specializa7eza7(obj_t
		BgL_envz00_3082, obj_t BgL_nodez00_3083)
	{
		{	/* Cfa/specialize.scm 483 */
			{
				BgL_setzd2exzd2itz00_bglt BgL_auxz00_4306;

				{
					BgL_nodez00_bglt BgL_auxz00_4307;

					{	/* Cfa/specialize.scm 485 */
						BgL_nodez00_bglt BgL_arg1947z00_3180;

						BgL_arg1947z00_3180 =
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3083)))->
							BgL_bodyz00);
						BgL_auxz00_4307 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1947z00_3180));
					}
					((((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3083)))->
							BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_4307), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4314;

					{	/* Cfa/specialize.scm 486 */
						BgL_nodez00_bglt BgL_arg1948z00_3181;

						BgL_arg1948z00_3181 =
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3083)))->
							BgL_onexitz00);
						BgL_auxz00_4314 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1948z00_3181));
					}
					((((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3083)))->
							BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_auxz00_4314), BUNSPEC);
				}
				{	/* Cfa/specialize.scm 487 */
					BgL_varz00_bglt BgL_arg1949z00_3182;

					BgL_arg1949z00_3182 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3083)))->BgL_varz00);
					BGl_patchz12z12zzcfa_specializa7eza7(
						((BgL_nodez00_bglt) BgL_arg1949z00_3182));
				}
				BgL_auxz00_4306 = ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_3083);
				return ((obj_t) BgL_auxz00_4306);
			}
		}

	}



/* &patch!-let-var1352 */
	obj_t BGl_z62patchz12zd2letzd2var1352z70zzcfa_specializa7eza7(obj_t
		BgL_envz00_3084, obj_t BgL_nodez00_3085)
	{
		{	/* Cfa/specialize.scm 468 */
			{
				BgL_letzd2varzd2_bglt BgL_auxz00_4327;

				{	/* Cfa/specialize.scm 470 */
					obj_t BgL_g1316z00_3184;

					BgL_g1316z00_3184 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_3085)))->BgL_bindingsz00);
					{
						obj_t BgL_l1314z00_3186;

						BgL_l1314z00_3186 = BgL_g1316z00_3184;
					BgL_zc3z04anonymousza31939ze3z87_3185:
						if (PAIRP(BgL_l1314z00_3186))
							{	/* Cfa/specialize.scm 470 */
								{	/* Cfa/specialize.scm 471 */
									obj_t BgL_bindingz00_3187;

									BgL_bindingz00_3187 = CAR(BgL_l1314z00_3186);
									{	/* Cfa/specialize.scm 471 */
										obj_t BgL_valz00_3188;

										BgL_valz00_3188 = CDR(((obj_t) BgL_bindingz00_3187));
										{	/* Cfa/specialize.scm 472 */
											obj_t BgL_arg1941z00_3189;

											BgL_arg1941z00_3189 =
												BGl_patchz12z12zzcfa_specializa7eza7(
												((BgL_nodez00_bglt) BgL_valz00_3188));
											{	/* Cfa/specialize.scm 472 */
												obj_t BgL_tmpz00_4337;

												BgL_tmpz00_4337 = ((obj_t) BgL_bindingz00_3187);
												SET_CDR(BgL_tmpz00_4337, BgL_arg1941z00_3189);
											}
										}
									}
								}
								{
									obj_t BgL_l1314z00_4340;

									BgL_l1314z00_4340 = CDR(BgL_l1314z00_3186);
									BgL_l1314z00_3186 = BgL_l1314z00_4340;
									goto BgL_zc3z04anonymousza31939ze3z87_3185;
								}
							}
						else
							{	/* Cfa/specialize.scm 470 */
								((bool_t) 1);
							}
					}
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4342;

					{	/* Cfa/specialize.scm 474 */
						BgL_nodez00_bglt BgL_arg1943z00_3190;

						BgL_arg1943z00_3190 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_3085)))->BgL_bodyz00);
						BgL_auxz00_4342 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1943z00_3190));
					}
					((((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_3085)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) BgL_auxz00_4342), BUNSPEC);
				}
				{	/* Cfa/specialize.scm 475 */
					bool_t BgL_test2227z00_4349;

					{	/* Cfa/specialize.scm 475 */
						BgL_typez00_bglt BgL_arg1946z00_3191;

						BgL_arg1946z00_3191 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_letzd2varzd2_bglt) BgL_nodez00_3085))))->BgL_typez00);
						BgL_test2227z00_4349 =
							(
							((obj_t) BgL_arg1946z00_3191) == BGl_za2objza2z00zztype_cachez00);
					}
					if (BgL_test2227z00_4349)
						{	/* Cfa/specialize.scm 475 */
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_letzd2varzd2_bglt) BgL_nodez00_3085))))->
									BgL_typez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
							((((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt)
													BgL_nodez00_3085))))->BgL_typez00) =
								((BgL_typez00_bglt)
									BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt) (
												(BgL_letzd2varzd2_bglt) BgL_nodez00_3085)),
										((bool_t) 0))), BUNSPEC);
						}
					else
						{	/* Cfa/specialize.scm 475 */
							BFALSE;
						}
				}
				BgL_auxz00_4327 = ((BgL_letzd2varzd2_bglt) BgL_nodez00_3085);
				return ((obj_t) BgL_auxz00_4327);
			}
		}

	}



/* &patch!-let-fun1350 */
	obj_t BGl_z62patchz12zd2letzd2fun1350z70zzcfa_specializa7eza7(obj_t
		BgL_envz00_3086, obj_t BgL_nodez00_3087)
	{
		{	/* Cfa/specialize.scm 456 */
			{
				BgL_letzd2funzd2_bglt BgL_auxz00_4367;

				{	/* Cfa/specialize.scm 458 */
					obj_t BgL_g1313z00_3193;

					BgL_g1313z00_3193 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_3087)))->BgL_localsz00);
					{
						obj_t BgL_l1311z00_3195;

						BgL_l1311z00_3195 = BgL_g1313z00_3193;
					BgL_zc3z04anonymousza31931ze3z87_3194:
						if (PAIRP(BgL_l1311z00_3195))
							{	/* Cfa/specialize.scm 458 */
								BGl_patchzd2funz12zc0zzcfa_specializa7eza7(CAR
									(BgL_l1311z00_3195));
								{
									obj_t BgL_l1311z00_4374;

									BgL_l1311z00_4374 = CDR(BgL_l1311z00_3195);
									BgL_l1311z00_3195 = BgL_l1311z00_4374;
									goto BgL_zc3z04anonymousza31931ze3z87_3194;
								}
							}
						else
							{	/* Cfa/specialize.scm 458 */
								((bool_t) 1);
							}
					}
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4376;

					{	/* Cfa/specialize.scm 459 */
						BgL_nodez00_bglt BgL_arg1935z00_3196;

						BgL_arg1935z00_3196 =
							(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nodez00_3087)))->BgL_bodyz00);
						BgL_auxz00_4376 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1935z00_3196));
					}
					((((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nodez00_3087)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) BgL_auxz00_4376), BUNSPEC);
				}
				{	/* Cfa/specialize.scm 460 */
					bool_t BgL_test2229z00_4383;

					{	/* Cfa/specialize.scm 460 */
						BgL_typez00_bglt BgL_arg1938z00_3197;

						BgL_arg1938z00_3197 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_letzd2funzd2_bglt) BgL_nodez00_3087))))->BgL_typez00);
						BgL_test2229z00_4383 =
							(
							((obj_t) BgL_arg1938z00_3197) == BGl_za2objza2z00zztype_cachez00);
					}
					if (BgL_test2229z00_4383)
						{	/* Cfa/specialize.scm 460 */
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_letzd2funzd2_bglt) BgL_nodez00_3087))))->
									BgL_typez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
							((((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt)
													BgL_nodez00_3087))))->BgL_typez00) =
								((BgL_typez00_bglt)
									BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt) (
												(BgL_letzd2funzd2_bglt) BgL_nodez00_3087)),
										((bool_t) 0))), BUNSPEC);
						}
					else
						{	/* Cfa/specialize.scm 460 */
							BFALSE;
						}
				}
				BgL_auxz00_4367 = ((BgL_letzd2funzd2_bglt) BgL_nodez00_3087);
				return ((obj_t) BgL_auxz00_4367);
			}
		}

	}



/* &patch!-switch1348 */
	obj_t BGl_z62patchz12zd2switch1348za2zzcfa_specializa7eza7(obj_t
		BgL_envz00_3088, obj_t BgL_nodez00_3089)
	{
		{	/* Cfa/specialize.scm 445 */
			{
				BgL_switchz00_bglt BgL_auxz00_4401;

				{
					BgL_nodez00_bglt BgL_auxz00_4402;

					{	/* Cfa/specialize.scm 447 */
						BgL_nodez00_bglt BgL_arg1925z00_3199;

						BgL_arg1925z00_3199 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_3089)))->BgL_testz00);
						BgL_auxz00_4402 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1925z00_3199));
					}
					((((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_3089)))->BgL_testz00) =
						((BgL_nodez00_bglt) BgL_auxz00_4402), BUNSPEC);
				}
				{	/* Cfa/specialize.scm 448 */
					obj_t BgL_g1310z00_3200;

					BgL_g1310z00_3200 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_3089)))->BgL_clausesz00);
					{
						obj_t BgL_l1308z00_3202;

						BgL_l1308z00_3202 = BgL_g1310z00_3200;
					BgL_zc3z04anonymousza31926ze3z87_3201:
						if (PAIRP(BgL_l1308z00_3202))
							{	/* Cfa/specialize.scm 448 */
								{	/* Cfa/specialize.scm 449 */
									obj_t BgL_clausez00_3203;

									BgL_clausez00_3203 = CAR(BgL_l1308z00_3202);
									{	/* Cfa/specialize.scm 449 */
										obj_t BgL_arg1928z00_3204;

										{	/* Cfa/specialize.scm 449 */
											obj_t BgL_arg1929z00_3205;

											BgL_arg1929z00_3205 = CDR(((obj_t) BgL_clausez00_3203));
											BgL_arg1928z00_3204 =
												BGl_patchz12z12zzcfa_specializa7eza7(
												((BgL_nodez00_bglt) BgL_arg1929z00_3205));
										}
										{	/* Cfa/specialize.scm 449 */
											obj_t BgL_tmpz00_4418;

											BgL_tmpz00_4418 = ((obj_t) BgL_clausez00_3203);
											SET_CDR(BgL_tmpz00_4418, BgL_arg1928z00_3204);
										}
									}
								}
								{
									obj_t BgL_l1308z00_4421;

									BgL_l1308z00_4421 = CDR(BgL_l1308z00_3202);
									BgL_l1308z00_3202 = BgL_l1308z00_4421;
									goto BgL_zc3z04anonymousza31926ze3z87_3201;
								}
							}
						else
							{	/* Cfa/specialize.scm 448 */
								((bool_t) 1);
							}
					}
				}
				BgL_auxz00_4401 = ((BgL_switchz00_bglt) BgL_nodez00_3089);
				return ((obj_t) BgL_auxz00_4401);
			}
		}

	}



/* &patch!-fail1346 */
	obj_t BGl_z62patchz12zd2fail1346za2zzcfa_specializa7eza7(obj_t
		BgL_envz00_3090, obj_t BgL_nodez00_3091)
	{
		{	/* Cfa/specialize.scm 435 */
			{
				BgL_failz00_bglt BgL_auxz00_4425;

				{
					BgL_nodez00_bglt BgL_auxz00_4426;

					{	/* Cfa/specialize.scm 437 */
						BgL_nodez00_bglt BgL_arg1920z00_3207;

						BgL_arg1920z00_3207 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_3091)))->BgL_procz00);
						BgL_auxz00_4426 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1920z00_3207));
					}
					((((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_3091)))->BgL_procz00) =
						((BgL_nodez00_bglt) BgL_auxz00_4426), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4433;

					{	/* Cfa/specialize.scm 438 */
						BgL_nodez00_bglt BgL_arg1923z00_3208;

						BgL_arg1923z00_3208 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_3091)))->BgL_msgz00);
						BgL_auxz00_4433 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1923z00_3208));
					}
					((((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_3091)))->BgL_msgz00) =
						((BgL_nodez00_bglt) BgL_auxz00_4433), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4440;

					{	/* Cfa/specialize.scm 439 */
						BgL_nodez00_bglt BgL_arg1924z00_3209;

						BgL_arg1924z00_3209 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_3091)))->BgL_objz00);
						BgL_auxz00_4440 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1924z00_3209));
					}
					((((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_3091)))->BgL_objz00) =
						((BgL_nodez00_bglt) BgL_auxz00_4440), BUNSPEC);
				}
				BgL_auxz00_4425 = ((BgL_failz00_bglt) BgL_nodez00_3091);
				return ((obj_t) BgL_auxz00_4425);
			}
		}

	}



/* &patch!-conditional1344 */
	obj_t BGl_z62patchz12zd2conditional1344za2zzcfa_specializa7eza7(obj_t
		BgL_envz00_3092, obj_t BgL_nodez00_3093)
	{
		{	/* Cfa/specialize.scm 422 */
			{
				BgL_conditionalz00_bglt BgL_auxz00_4449;

				{
					BgL_nodez00_bglt BgL_auxz00_4450;

					{	/* Cfa/specialize.scm 424 */
						BgL_nodez00_bglt BgL_arg1913z00_3211;

						BgL_arg1913z00_3211 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_3093)))->BgL_testz00);
						BgL_auxz00_4450 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1913z00_3211));
					}
					((((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_3093)))->BgL_testz00) =
						((BgL_nodez00_bglt) BgL_auxz00_4450), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4457;

					{	/* Cfa/specialize.scm 425 */
						BgL_nodez00_bglt BgL_arg1914z00_3212;

						BgL_arg1914z00_3212 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_3093)))->BgL_truez00);
						BgL_auxz00_4457 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1914z00_3212));
					}
					((((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_3093)))->BgL_truez00) =
						((BgL_nodez00_bglt) BgL_auxz00_4457), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4464;

					{	/* Cfa/specialize.scm 426 */
						BgL_nodez00_bglt BgL_arg1916z00_3213;

						BgL_arg1916z00_3213 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_3093)))->BgL_falsez00);
						BgL_auxz00_4464 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1916z00_3213));
					}
					((((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_3093)))->
							BgL_falsez00) = ((BgL_nodez00_bglt) BgL_auxz00_4464), BUNSPEC);
				}
				{	/* Cfa/specialize.scm 427 */
					bool_t BgL_test2231z00_4471;

					{	/* Cfa/specialize.scm 427 */
						BgL_typez00_bglt BgL_arg1919z00_3214;

						BgL_arg1919z00_3214 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_conditionalz00_bglt) BgL_nodez00_3093))))->
							BgL_typez00);
						BgL_test2231z00_4471 =
							(((obj_t) BgL_arg1919z00_3214) ==
							BGl_za2objza2z00zztype_cachez00);
					}
					if (BgL_test2231z00_4471)
						{	/* Cfa/specialize.scm 427 */
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_conditionalz00_bglt) BgL_nodez00_3093))))->
									BgL_typez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
							((((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt)
													BgL_nodez00_3093))))->BgL_typez00) =
								((BgL_typez00_bglt)
									BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt) (
												(BgL_conditionalz00_bglt) BgL_nodez00_3093)),
										((bool_t) 0))), BUNSPEC);
						}
					else
						{	/* Cfa/specialize.scm 427 */
							BFALSE;
						}
				}
				BgL_auxz00_4449 = ((BgL_conditionalz00_bglt) BgL_nodez00_3093);
				return ((obj_t) BgL_auxz00_4449);
			}
		}

	}



/* &patch!-setq1342 */
	obj_t BGl_z62patchz12zd2setq1342za2zzcfa_specializa7eza7(obj_t
		BgL_envz00_3094, obj_t BgL_nodez00_3095)
	{
		{	/* Cfa/specialize.scm 413 */
			{
				BgL_setqz00_bglt BgL_auxz00_4489;

				{
					BgL_nodez00_bglt BgL_auxz00_4490;

					{	/* Cfa/specialize.scm 415 */
						BgL_nodez00_bglt BgL_arg1911z00_3216;

						BgL_arg1911z00_3216 =
							(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nodez00_3095)))->BgL_valuez00);
						BgL_auxz00_4490 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1911z00_3216));
					}
					((((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nodez00_3095)))->BgL_valuez00) =
						((BgL_nodez00_bglt) BgL_auxz00_4490), BUNSPEC);
				}
				{
					BgL_varz00_bglt BgL_auxz00_4497;

					{	/* Cfa/specialize.scm 416 */
						BgL_varz00_bglt BgL_arg1912z00_3217;

						BgL_arg1912z00_3217 =
							(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nodez00_3095)))->BgL_varz00);
						BgL_auxz00_4497 =
							((BgL_varz00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(
								((BgL_nodez00_bglt) BgL_arg1912z00_3217)));
					}
					((((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nodez00_3095)))->BgL_varz00) =
						((BgL_varz00_bglt) BgL_auxz00_4497), BUNSPEC);
				}
				BgL_auxz00_4489 = ((BgL_setqz00_bglt) BgL_nodez00_3095);
				return ((obj_t) BgL_auxz00_4489);
			}
		}

	}



/* &patch!-cast1340 */
	obj_t BGl_z62patchz12zd2cast1340za2zzcfa_specializa7eza7(obj_t
		BgL_envz00_3096, obj_t BgL_nodez00_3097)
	{
		{	/* Cfa/specialize.scm 405 */
			{
				BgL_castz00_bglt BgL_auxz00_4507;

				BGl_patchz12z12zzcfa_specializa7eza7(
					(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_3097)))->BgL_argz00));
				BgL_auxz00_4507 = ((BgL_castz00_bglt) BgL_nodez00_3097);
				return ((obj_t) BgL_auxz00_4507);
			}
		}

	}



/* &patch!-extern1338 */
	obj_t BGl_z62patchz12zd2extern1338za2zzcfa_specializa7eza7(obj_t
		BgL_envz00_3098, obj_t BgL_nodez00_3099)
	{
		{	/* Cfa/specialize.scm 397 */
			{
				BgL_externz00_bglt BgL_auxz00_4513;

				BGl_patchza2z12zb0zzcfa_specializa7eza7(
					(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_3099)))->BgL_exprza2za2));
				BgL_auxz00_4513 = ((BgL_externz00_bglt) BgL_nodez00_3099);
				return ((obj_t) BgL_auxz00_4513);
			}
		}

	}



/* &patch!-funcall1336 */
	obj_t BGl_z62patchz12zd2funcall1336za2zzcfa_specializa7eza7(obj_t
		BgL_envz00_3100, obj_t BgL_nodez00_3101)
	{
		{	/* Cfa/specialize.scm 388 */
			{
				BgL_funcallz00_bglt BgL_auxz00_4519;

				{
					BgL_nodez00_bglt BgL_auxz00_4520;

					{	/* Cfa/specialize.scm 390 */
						BgL_nodez00_bglt BgL_arg1903z00_3221;

						BgL_arg1903z00_3221 =
							(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nodez00_3101)))->BgL_funz00);
						BgL_auxz00_4520 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1903z00_3221));
					}
					((((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nodez00_3101)))->BgL_funz00) =
						((BgL_nodez00_bglt) BgL_auxz00_4520), BUNSPEC);
				}
				BGl_patchza2z12zb0zzcfa_specializa7eza7(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_3101)))->BgL_argsz00));
				BgL_auxz00_4519 = ((BgL_funcallz00_bglt) BgL_nodez00_3101);
				return ((obj_t) BgL_auxz00_4519);
			}
		}

	}



/* &patch!-app-ly1334 */
	obj_t BGl_z62patchz12zd2appzd2ly1334z70zzcfa_specializa7eza7(obj_t
		BgL_envz00_3102, obj_t BgL_nodez00_3103)
	{
		{	/* Cfa/specialize.scm 379 */
			{
				BgL_appzd2lyzd2_bglt BgL_auxz00_4532;

				{
					BgL_nodez00_bglt BgL_auxz00_4533;

					{	/* Cfa/specialize.scm 381 */
						BgL_nodez00_bglt BgL_arg1901z00_3223;

						BgL_arg1901z00_3223 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_3103)))->BgL_funz00);
						BgL_auxz00_4533 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1901z00_3223));
					}
					((((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_3103)))->BgL_funz00) =
						((BgL_nodez00_bglt) BgL_auxz00_4533), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4540;

					{	/* Cfa/specialize.scm 382 */
						BgL_nodez00_bglt BgL_arg1902z00_3224;

						BgL_arg1902z00_3224 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_3103)))->BgL_argz00);
						BgL_auxz00_4540 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1902z00_3224));
					}
					((((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_3103)))->BgL_argz00) =
						((BgL_nodez00_bglt) BgL_auxz00_4540), BUNSPEC);
				}
				BgL_auxz00_4532 = ((BgL_appzd2lyzd2_bglt) BgL_nodez00_3103);
				return ((obj_t) BgL_auxz00_4532);
			}
		}

	}



/* &patch!-sync1332 */
	obj_t BGl_z62patchz12zd2sync1332za2zzcfa_specializa7eza7(obj_t
		BgL_envz00_3104, obj_t BgL_nodez00_3105)
	{
		{	/* Cfa/specialize.scm 366 */
			{
				BgL_syncz00_bglt BgL_auxz00_4549;

				{
					BgL_nodez00_bglt BgL_auxz00_4550;

					{	/* Cfa/specialize.scm 368 */
						BgL_nodez00_bglt BgL_arg1893z00_3226;

						BgL_arg1893z00_3226 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_3105)))->BgL_mutexz00);
						BgL_auxz00_4550 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1893z00_3226));
					}
					((((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_3105)))->BgL_mutexz00) =
						((BgL_nodez00_bglt) BgL_auxz00_4550), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4557;

					{	/* Cfa/specialize.scm 369 */
						BgL_nodez00_bglt BgL_arg1894z00_3227;

						BgL_arg1894z00_3227 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_3105)))->BgL_prelockz00);
						BgL_auxz00_4557 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1894z00_3227));
					}
					((((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_3105)))->BgL_prelockz00) =
						((BgL_nodez00_bglt) BgL_auxz00_4557), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_4564;

					{	/* Cfa/specialize.scm 370 */
						BgL_nodez00_bglt BgL_arg1896z00_3228;

						BgL_arg1896z00_3228 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_3105)))->BgL_bodyz00);
						BgL_auxz00_4564 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_specializa7eza7(BgL_arg1896z00_3228));
					}
					((((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_3105)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) BgL_auxz00_4564), BUNSPEC);
				}
				{	/* Cfa/specialize.scm 371 */
					bool_t BgL_test2232z00_4571;

					{	/* Cfa/specialize.scm 371 */
						BgL_typez00_bglt BgL_arg1899z00_3229;

						BgL_arg1899z00_3229 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_syncz00_bglt) BgL_nodez00_3105))))->BgL_typez00);
						BgL_test2232z00_4571 =
							(
							((obj_t) BgL_arg1899z00_3229) == BGl_za2objza2z00zztype_cachez00);
					}
					if (BgL_test2232z00_4571)
						{	/* Cfa/specialize.scm 371 */
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_syncz00_bglt) BgL_nodez00_3105))))->BgL_typez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
							((((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) ((BgL_syncz00_bglt)
													BgL_nodez00_3105))))->BgL_typez00) =
								((BgL_typez00_bglt)
									BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt) (
												(BgL_syncz00_bglt) BgL_nodez00_3105)), ((bool_t) 0))),
								BUNSPEC);
						}
					else
						{	/* Cfa/specialize.scm 371 */
							BFALSE;
						}
				}
				BgL_auxz00_4549 = ((BgL_syncz00_bglt) BgL_nodez00_3105);
				return ((obj_t) BgL_auxz00_4549);
			}
		}

	}



/* &patch!-sequence1330 */
	obj_t BGl_z62patchz12zd2sequence1330za2zzcfa_specializa7eza7(obj_t
		BgL_envz00_3106, obj_t BgL_nodez00_3107)
	{
		{	/* Cfa/specialize.scm 355 */
			{
				BgL_sequencez00_bglt BgL_auxz00_4589;

				BGl_patchza2z12zb0zzcfa_specializa7eza7(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_3107)))->BgL_nodesz00));
				{	/* Cfa/specialize.scm 358 */
					bool_t BgL_test2233z00_4593;

					{	/* Cfa/specialize.scm 358 */
						BgL_typez00_bglt BgL_arg1892z00_3231;

						BgL_arg1892z00_3231 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_sequencez00_bglt) BgL_nodez00_3107))))->BgL_typez00);
						BgL_test2233z00_4593 =
							(
							((obj_t) BgL_arg1892z00_3231) == BGl_za2objza2z00zztype_cachez00);
					}
					if (BgL_test2233z00_4593)
						{	/* Cfa/specialize.scm 358 */
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_sequencez00_bglt) BgL_nodez00_3107))))->
									BgL_typez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2_za2z00zztype_cachez00)), BUNSPEC);
							((((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) ((BgL_sequencez00_bglt)
													BgL_nodez00_3107))))->BgL_typez00) =
								((BgL_typez00_bglt)
									BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt) (
												(BgL_sequencez00_bglt) BgL_nodez00_3107)),
										((bool_t) 0))), BUNSPEC);
						}
					else
						{	/* Cfa/specialize.scm 358 */
							BFALSE;
						}
				}
				BgL_auxz00_4589 = ((BgL_sequencez00_bglt) BgL_nodez00_3107);
				return ((obj_t) BgL_auxz00_4589);
			}
		}

	}



/* &patch!-closure1328 */
	obj_t BGl_z62patchz12zd2closure1328za2zzcfa_specializa7eza7(obj_t
		BgL_envz00_3108, obj_t BgL_nodez00_3109)
	{
		{	/* Cfa/specialize.scm 349 */
			{	/* Cfa/specialize.scm 350 */
				obj_t BgL_arg1888z00_3233;

				BgL_arg1888z00_3233 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_3109)));
				return
					BGl_internalzd2errorzd2zztools_errorz00
					(BGl_string2059z00zzcfa_specializa7eza7,
					BGl_string2081z00zzcfa_specializa7eza7, BgL_arg1888z00_3233);
			}
		}

	}



/* &patch!-var1326 */
	obj_t BGl_z62patchz12zd2var1326za2zzcfa_specializa7eza7(obj_t BgL_envz00_3110,
		obj_t BgL_nodez00_3111)
	{
		{	/* Cfa/specialize.scm 343 */
			return ((obj_t) ((BgL_varz00_bglt) BgL_nodez00_3111));
		}

	}



/* &patch!-kwote1324 */
	obj_t BGl_z62patchz12zd2kwote1324za2zzcfa_specializa7eza7(obj_t
		BgL_envz00_3112, obj_t BgL_nodez00_3113)
	{
		{	/* Cfa/specialize.scm 337 */
			return ((obj_t) ((BgL_kwotez00_bglt) BgL_nodez00_3113));
		}

	}



/* &patch!-atom1322 */
	obj_t BGl_z62patchz12zd2atom1322za2zzcfa_specializa7eza7(obj_t
		BgL_envz00_3114, obj_t BgL_nodez00_3115)
	{
		{	/* Cfa/specialize.scm 331 */
			return ((obj_t) ((BgL_atomz00_bglt) BgL_nodez00_3115));
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_specializa7eza7(void)
	{
		{	/* Cfa/specialize.scm 19 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2082z00zzcfa_specializa7eza7));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2082z00zzcfa_specializa7eza7));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2082z00zzcfa_specializa7eza7));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2082z00zzcfa_specializa7eza7));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2082z00zzcfa_specializa7eza7));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2082z00zzcfa_specializa7eza7));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2082z00zzcfa_specializa7eza7));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2082z00zzcfa_specializa7eza7));
			BGl_modulezd2initializa7ationz75zzbackend_backendz00(216206620L,
				BSTRING_TO_STRING(BGl_string2082z00zzcfa_specializa7eza7));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2082z00zzcfa_specializa7eza7));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2082z00zzcfa_specializa7eza7));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2082z00zzcfa_specializa7eza7));
			BGl_modulezd2initializa7ationz75zzinline_inlinez00(20504962L,
				BSTRING_TO_STRING(BGl_string2082z00zzcfa_specializa7eza7));
			return
				BGl_modulezd2initializa7ationz75zzinline_walkz00(385476819L,
				BSTRING_TO_STRING(BGl_string2082z00zzcfa_specializa7eza7));
		}

	}

#ifdef __cplusplus
}
#endif
