/*===========================================================================*/
/*   (Cfa/vector.scm)                                                        */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/vector.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_VECTOR_TYPE_DEFINITIONS
#define BGL_CFA_VECTOR_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_ftypez00;
	}                 *BgL_vlengthz00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_prezd2makezd2vectorzd2appzd2_bgl
	{
		struct BgL_variablez00_bgl *BgL_ownerz00;
	}                                   *BgL_prezd2makezd2vectorzd2appzd2_bglt;

	typedef struct BgL_makezd2vectorzd2appz00_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		struct BgL_approxz00_bgl *BgL_valuezd2approxzd2;
		long BgL_lostzd2stampzd2;
		struct BgL_variablez00_bgl *BgL_ownerz00;
		obj_t BgL_stackzd2stampzd2;
		bool_t BgL_seenzf3zf3;
		bool_t BgL_tvectorzf3zf3;
	}                             *BgL_makezd2vectorzd2appz00_bglt;

	typedef struct BgL_vrefzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_tvectorzf3zf3;
	}                      *BgL_vrefzf2cinfozf2_bglt;

	typedef struct BgL_vsetz12zf2cinfoze0_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_tvectorzf3zf3;
	}                         *BgL_vsetz12zf2cinfoze0_bglt;

	typedef struct BgL_vlengthzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_tvectorzf3zf3;
	}                         *BgL_vlengthzf2cinfozf2_bglt;

	typedef struct BgL_prezd2valloczf2cinfoz20_bgl
	{
		struct BgL_variablez00_bgl *BgL_ownerz00;
	}                              *BgL_prezd2valloczf2cinfoz20_bglt;

	typedef struct BgL_valloczf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                        *BgL_valloczf2cinfozf2_bglt;

	typedef struct BgL_valloczf2cinfozb2optimz40_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		struct BgL_approxz00_bgl *BgL_valuezd2approxzd2;
		long BgL_lostzd2stampzd2;
		struct BgL_variablez00_bgl *BgL_ownerz00;
		bool_t BgL_stackablezf3zf3;
		obj_t BgL_stackzd2stampzd2;
		bool_t BgL_seenzf3zf3;
	}                                *BgL_valloczf2cinfozb2optimz40_bglt;


#endif													// BGL_CFA_VECTOR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2vlengthzf2Cinfo1615z50zzcfa_vectorz00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2vrefzf2Cinfo1617z50zzcfa_vectorz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzcfa_vectorz00(void);
	extern obj_t BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00(obj_t,
		BgL_approxz00_bglt);
	extern obj_t BGl_prezd2valloczf2Cinfoz20zzcfa_info3z00;
	static obj_t BGl_methodzd2initzd2zzcfa_vectorz00(void);
	static obj_t BGl_z62nodezd2setupz12zd2vsetz121607z62zzcfa_vectorz00(obj_t,
		obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2valloczf2Cinfozb2op1611ze2zzcfa_vectorz00(obj_t, obj_t);
	extern obj_t BGl_prezd2makezd2vectorzd2appzd2zzcfa_info2z00;
	BGL_EXPORTED_DECL bool_t BGl_vectorzd2optimzf3z21zzcfa_vectorz00(void);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_vsetz12zf2Cinfoze0zzcfa_info3z00;
	static obj_t BGl_z62loosezd2allocz12zd2makezd2ve1621za2zzcfa_vectorz00(obj_t,
		obj_t);
	extern BgL_approxz00_bglt
		BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_approxz00_bglt);
	extern obj_t BGl_vlengthzf2Cinfozf2zzcfa_info3z00;
	static obj_t BGl_gczd2rootszd2initz00zzcfa_vectorz00(void);
	extern obj_t BGl_vallocz00zzast_nodez00;
	extern BgL_approxz00_bglt BGl_makezd2emptyzd2approxz00zzcfa_approxz00(void);
	static obj_t BGl_z62zc3z04anonymousza31813ze3ze5zzcfa_vectorz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_za2cfazd2stampza2zd2zzcfa_iteratez00;
	static obj_t BGl_z62zc3z04anonymousza31851ze3ze5zzcfa_vectorz00(obj_t, obj_t);
	extern obj_t BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_approxz00_bglt);
	extern obj_t BGl_valloczf2Cinfozf2zzcfa_info3z00;
	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	static obj_t BGl_z62vectorzd2optimzf3z43zzcfa_vectorz00(obj_t);
	extern obj_t BGl_nodezd2setupza2z12z62zzcfa_setupz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_vectorz00 = BUNSPEC;
	extern obj_t BGl_za2vectorza2z00zztype_cachez00;
	extern obj_t BGl_vlengthz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_toplevelzd2initzd2zzcfa_vectorz00(void);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2makezd2vectorzd2app1609za2zzcfa_vectorz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcfa_vectorz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern BgL_approxz00_bglt BGl_loosez12z12zzcfa_loosez00(BgL_approxz00_bglt,
		obj_t);
	extern obj_t BGl_makezd2vectorzd2appz00zzcfa_info2z00;
	static obj_t BGl_z62nodezd2setupz12zd2vref1605z70zzcfa_vectorz00(obj_t,
		obj_t);
	extern obj_t BGl_vrefzf2Cinfozf2zzcfa_info3z00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_nodezd2setupz12zc0zzcfa_setupz00(BgL_nodez00_bglt);
	extern BgL_approxz00_bglt
		BGl_makezd2typezd2alloczd2approxzd2zzcfa_approxz00(BgL_typez00_bglt,
		BgL_nodez00_bglt);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2vsetz12zf2Cinfo1619z42zzcfa_vectorz00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2valloczf2Cinfo1613z50zzcfa_vectorz00(obj_t, obj_t);
	extern obj_t BGl_addzd2makezd2vectorz12z12zzcfa_tvectorz00(BgL_nodez00_bglt);
	static obj_t BGl_z62nodezd2setupz12zd2vlength1603z70zzcfa_vectorz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_vectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_setupz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_cfaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_iteratez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_loosez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__tvectorz00(long, char *);
	static obj_t BGl_z62nodezd2setupz12zd2valloc1601z70zzcfa_vectorz00(obj_t,
		obj_t);
	extern obj_t BGl_vrefz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzcfa_vectorz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_vectorz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_vectorz00(void);
	extern BgL_approxz00_bglt
		BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_typez00_bglt);
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	extern BgL_approxz00_bglt BGl_cfaz12z12zzcfa_cfaz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2prezd2make1597za2zzcfa_vectorz00(obj_t,
		obj_t);
	static obj_t BGl_z62loosezd2allocz12zd2valloczf21623z82zzcfa_vectorz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2prezd2vall1599za2zzcfa_vectorz00(obj_t,
		obj_t);
	extern obj_t BGl_approxzd2setzd2typez12z12zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00;
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	static obj_t __cnst[2];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2089z00zzcfa_vectorz00,
		BgL_bgl_za762nodeza7d2setupza72109za7,
		BGl_z62nodezd2setupz12zd2prezd2make1597za2zzcfa_vectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2091z00zzcfa_vectorz00,
		BgL_bgl_za762nodeza7d2setupza72110za7,
		BGl_z62nodezd2setupz12zd2prezd2vall1599za2zzcfa_vectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2092z00zzcfa_vectorz00,
		BgL_bgl_za762nodeza7d2setupza72111za7,
		BGl_z62nodezd2setupz12zd2valloc1601z70zzcfa_vectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2093z00zzcfa_vectorz00,
		BgL_bgl_za762nodeza7d2setupza72112za7,
		BGl_z62nodezd2setupz12zd2vlength1603z70zzcfa_vectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2094z00zzcfa_vectorz00,
		BgL_bgl_za762nodeza7d2setupza72113za7,
		BGl_z62nodezd2setupz12zd2vref1605z70zzcfa_vectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2095z00zzcfa_vectorz00,
		BgL_bgl_za762nodeza7d2setupza72114za7,
		BGl_z62nodezd2setupz12zd2vsetz121607z62zzcfa_vectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2096z00zzcfa_vectorz00,
		BgL_bgl_za762cfaza712za7d2make2115za7,
		BGl_z62cfaz12zd2makezd2vectorzd2app1609za2zzcfa_vectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2098z00zzcfa_vectorz00,
		BgL_bgl_za762cfaza712za7d2vall2116za7,
		BGl_z62cfaz12zd2valloczf2Cinfozb2op1611ze2zzcfa_vectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2099z00zzcfa_vectorz00,
		BgL_bgl_za762cfaza712za7d2vall2117za7,
		BGl_z62cfaz12zd2valloczf2Cinfo1613z50zzcfa_vectorz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_cfaz12zd2envzc0zzcfa_cfaz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_vectorzd2optimzf3zd2envzf3zzcfa_vectorz00,
		BgL_bgl_za762vectorza7d2opti2118z00,
		BGl_z62vectorzd2optimzf3z43zzcfa_vectorz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2100z00zzcfa_vectorz00,
		BgL_bgl_za762cfaza712za7d2vlen2119za7,
		BGl_z62cfaz12zd2vlengthzf2Cinfo1615z50zzcfa_vectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2101z00zzcfa_vectorz00,
		BgL_bgl_za762cfaza712za7d2vref2120za7,
		BGl_z62cfaz12zd2vrefzf2Cinfo1617z50zzcfa_vectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2102z00zzcfa_vectorz00,
		BgL_bgl_za762cfaza712za7d2vset2121za7,
		BGl_z62cfaz12zd2vsetz12zf2Cinfo1619z42zzcfa_vectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2103z00zzcfa_vectorz00,
		BgL_bgl_za762looseza7d2alloc2122z00,
		BGl_z62loosezd2allocz12zd2makezd2ve1621za2zzcfa_vectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2105z00zzcfa_vectorz00,
		BgL_bgl_za762looseza7d2alloc2123z00,
		BGl_z62loosezd2allocz12zd2valloczf21623z82zzcfa_vectorz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_nodezd2setupz12zd2envz12zzcfa_setupz00;
	   
		 
		DEFINE_STRING(BGl_string2090z00zzcfa_vectorz00,
		BgL_bgl_string2090za700za7za7c2124za7, "node-setup!", 11);
	      DEFINE_STRING(BGl_string2097z00zzcfa_vectorz00,
		BgL_bgl_string2097za700za7za7c2125za7, "cfa!::approx", 12);
	extern obj_t BGl_loosezd2allocz12zd2envz12zzcfa_loosez00;
	   
		 
		DEFINE_STRING(BGl_string2104z00zzcfa_vectorz00,
		BgL_bgl_string2104za700za7za7c2126za7, "loose-alloc!", 12);
	      DEFINE_STRING(BGl_string2106z00zzcfa_vectorz00,
		BgL_bgl_string2106za700za7za7c2127za7, "cfa_vector", 10);
	      DEFINE_STRING(BGl_string2107z00zzcfa_vectorz00,
		BgL_bgl_string2107za700za7za7c2128za7, "all nothing ", 12);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_vectorz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_vectorz00(long
		BgL_checksumz00_4861, char *BgL_fromz00_4862)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_vectorz00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_vectorz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_vectorz00();
					BGl_libraryzd2moduleszd2initz00zzcfa_vectorz00();
					BGl_cnstzd2initzd2zzcfa_vectorz00();
					BGl_importedzd2moduleszd2initz00zzcfa_vectorz00();
					BGl_methodzd2initzd2zzcfa_vectorz00();
					return BGl_toplevelzd2initzd2zzcfa_vectorz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_vectorz00(void)
	{
		{	/* Cfa/vector.scm 17 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_vector");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_vector");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_vector");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_vector");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cfa_vector");
			BGl_modulezd2initializa7ationz75zz__tvectorz00(0L, "cfa_vector");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_vector");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_vector");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_vector");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_vectorz00(void)
	{
		{	/* Cfa/vector.scm 17 */
			{	/* Cfa/vector.scm 17 */
				obj_t BgL_cportz00_4664;

				{	/* Cfa/vector.scm 17 */
					obj_t BgL_stringz00_4671;

					BgL_stringz00_4671 = BGl_string2107z00zzcfa_vectorz00;
					{	/* Cfa/vector.scm 17 */
						obj_t BgL_startz00_4672;

						BgL_startz00_4672 = BINT(0L);
						{	/* Cfa/vector.scm 17 */
							obj_t BgL_endz00_4673;

							BgL_endz00_4673 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4671)));
							{	/* Cfa/vector.scm 17 */

								BgL_cportz00_4664 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4671, BgL_startz00_4672, BgL_endz00_4673);
				}}}}
				{
					long BgL_iz00_4665;

					BgL_iz00_4665 = 1L;
				BgL_loopz00_4666:
					if ((BgL_iz00_4665 == -1L))
						{	/* Cfa/vector.scm 17 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/vector.scm 17 */
							{	/* Cfa/vector.scm 17 */
								obj_t BgL_arg2108z00_4667;

								{	/* Cfa/vector.scm 17 */

									{	/* Cfa/vector.scm 17 */
										obj_t BgL_locationz00_4669;

										BgL_locationz00_4669 = BBOOL(((bool_t) 0));
										{	/* Cfa/vector.scm 17 */

											BgL_arg2108z00_4667 =
												BGl_readz00zz__readerz00(BgL_cportz00_4664,
												BgL_locationz00_4669);
										}
									}
								}
								{	/* Cfa/vector.scm 17 */
									int BgL_tmpz00_4890;

									BgL_tmpz00_4890 = (int) (BgL_iz00_4665);
									CNST_TABLE_SET(BgL_tmpz00_4890, BgL_arg2108z00_4667);
							}}
							{	/* Cfa/vector.scm 17 */
								int BgL_auxz00_4670;

								BgL_auxz00_4670 = (int) ((BgL_iz00_4665 - 1L));
								{
									long BgL_iz00_4895;

									BgL_iz00_4895 = (long) (BgL_auxz00_4670);
									BgL_iz00_4665 = BgL_iz00_4895;
									goto BgL_loopz00_4666;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_vectorz00(void)
	{
		{	/* Cfa/vector.scm 17 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_vectorz00(void)
	{
		{	/* Cfa/vector.scm 17 */
			return BUNSPEC;
		}

	}



/* vector-optim? */
	BGL_EXPORTED_DEF bool_t BGl_vectorzd2optimzf3z21zzcfa_vectorz00(void)
	{
		{	/* Cfa/vector.scm 40 */
			return ((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 2L);
		}

	}



/* &vector-optim? */
	obj_t BGl_z62vectorzd2optimzf3z43zzcfa_vectorz00(obj_t BgL_envz00_4607)
	{
		{	/* Cfa/vector.scm 40 */
			return BBOOL(BGl_vectorzd2optimzf3z21zzcfa_vectorz00());
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_vectorz00(void)
	{
		{	/* Cfa/vector.scm 17 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_vectorz00(void)
	{
		{	/* Cfa/vector.scm 17 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_vectorz00(void)
	{
		{	/* Cfa/vector.scm 17 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_prezd2makezd2vectorzd2appzd2zzcfa_info2z00,
				BGl_proc2089z00zzcfa_vectorz00, BGl_string2090z00zzcfa_vectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_prezd2valloczf2Cinfoz20zzcfa_info3z00,
				BGl_proc2091z00zzcfa_vectorz00, BGl_string2090z00zzcfa_vectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00, BGl_vallocz00zzast_nodez00,
				BGl_proc2092z00zzcfa_vectorz00, BGl_string2090z00zzcfa_vectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_vlengthz00zzast_nodez00, BGl_proc2093z00zzcfa_vectorz00,
				BGl_string2090z00zzcfa_vectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00, BGl_vrefz00zzast_nodez00,
				BGl_proc2094z00zzcfa_vectorz00, BGl_string2090z00zzcfa_vectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_vsetz12z12zzast_nodez00, BGl_proc2095z00zzcfa_vectorz00,
				BGl_string2090z00zzcfa_vectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_makezd2vectorzd2appz00zzcfa_info2z00,
				BGl_proc2096z00zzcfa_vectorz00, BGl_string2097z00zzcfa_vectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00,
				BGl_proc2098z00zzcfa_vectorz00, BGl_string2097z00zzcfa_vectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_valloczf2Cinfozf2zzcfa_info3z00,
				BGl_proc2099z00zzcfa_vectorz00, BGl_string2097z00zzcfa_vectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_vlengthzf2Cinfozf2zzcfa_info3z00,
				BGl_proc2100z00zzcfa_vectorz00, BGl_string2097z00zzcfa_vectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_vrefzf2Cinfozf2zzcfa_info3z00,
				BGl_proc2101z00zzcfa_vectorz00, BGl_string2097z00zzcfa_vectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_vsetz12zf2Cinfoze0zzcfa_info3z00,
				BGl_proc2102z00zzcfa_vectorz00, BGl_string2097z00zzcfa_vectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_loosezd2allocz12zd2envz12zzcfa_loosez00,
				BGl_makezd2vectorzd2appz00zzcfa_info2z00,
				BGl_proc2103z00zzcfa_vectorz00, BGl_string2104z00zzcfa_vectorz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_loosezd2allocz12zd2envz12zzcfa_loosez00,
				BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00,
				BGl_proc2105z00zzcfa_vectorz00, BGl_string2104z00zzcfa_vectorz00);
		}

	}



/* &loose-alloc!-valloc/1623 */
	obj_t BGl_z62loosezd2allocz12zd2valloczf21623z82zzcfa_vectorz00(obj_t
		BgL_envz00_4626, obj_t BgL_allocz00_4627)
	{
		{	/* Cfa/vector.scm 281 */
			{	/* Cfa/vector.scm 283 */
				bool_t BgL_test2131z00_4916;

				{	/* Cfa/vector.scm 283 */
					long BgL_arg1875z00_4676;

					{
						BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_4917;

						{
							obj_t BgL_auxz00_4918;

							{	/* Cfa/vector.scm 283 */
								BgL_objectz00_bglt BgL_tmpz00_4919;

								BgL_tmpz00_4919 =
									((BgL_objectz00_bglt)
									((BgL_vallocz00_bglt) BgL_allocz00_4627));
								BgL_auxz00_4918 = BGL_OBJECT_WIDENING(BgL_tmpz00_4919);
							}
							BgL_auxz00_4917 =
								((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_4918);
						}
						BgL_arg1875z00_4676 =
							(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_4917))->
							BgL_lostzd2stampzd2);
					}
					BgL_test2131z00_4916 =
						(BgL_arg1875z00_4676 ==
						(long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00));
				}
				if (BgL_test2131z00_4916)
					{	/* Cfa/vector.scm 283 */
						return BUNSPEC;
					}
				else
					{	/* Cfa/vector.scm 283 */
						{
							BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_4927;

							{
								obj_t BgL_auxz00_4928;

								{	/* Cfa/vector.scm 288 */
									BgL_objectz00_bglt BgL_tmpz00_4929;

									BgL_tmpz00_4929 =
										((BgL_objectz00_bglt)
										((BgL_vallocz00_bglt) BgL_allocz00_4627));
									BgL_auxz00_4928 = BGL_OBJECT_WIDENING(BgL_tmpz00_4929);
								}
								BgL_auxz00_4927 =
									((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_4928);
							}
							((((BgL_valloczf2cinfozb2optimz40_bglt)
										COBJECT(BgL_auxz00_4927))->BgL_lostzd2stampzd2) =
								((long) (long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00)),
								BUNSPEC);
						}
						{	/* Cfa/vector.scm 289 */
							BgL_approxz00_bglt BgL_arg1872z00_4677;

							{
								BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_4936;

								{
									obj_t BgL_auxz00_4937;

									{	/* Cfa/vector.scm 289 */
										BgL_objectz00_bglt BgL_tmpz00_4938;

										BgL_tmpz00_4938 =
											((BgL_objectz00_bglt)
											((BgL_vallocz00_bglt) BgL_allocz00_4627));
										BgL_auxz00_4937 = BGL_OBJECT_WIDENING(BgL_tmpz00_4938);
									}
									BgL_auxz00_4936 =
										((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_4937);
								}
								BgL_arg1872z00_4677 =
									(((BgL_valloczf2cinfozb2optimz40_bglt)
										COBJECT(BgL_auxz00_4936))->BgL_valuezd2approxzd2);
							}
							BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
								(BGl_loosezd2allocz12zd2envz12zzcfa_loosez00,
								BgL_arg1872z00_4677);
						}
						{	/* Cfa/vector.scm 290 */
							BgL_approxz00_bglt BgL_arg1873z00_4678;

							{
								BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_4945;

								{
									obj_t BgL_auxz00_4946;

									{	/* Cfa/vector.scm 290 */
										BgL_objectz00_bglt BgL_tmpz00_4947;

										BgL_tmpz00_4947 =
											((BgL_objectz00_bglt)
											((BgL_vallocz00_bglt) BgL_allocz00_4627));
										BgL_auxz00_4946 = BGL_OBJECT_WIDENING(BgL_tmpz00_4947);
									}
									BgL_auxz00_4945 =
										((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_4946);
								}
								BgL_arg1873z00_4678 =
									(((BgL_valloczf2cinfozb2optimz40_bglt)
										COBJECT(BgL_auxz00_4945))->BgL_valuezd2approxzd2);
							}
							BGl_approxzd2setzd2typez12z12zzcfa_approxz00(BgL_arg1873z00_4678,
								((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
						}
						{	/* Cfa/vector.scm 291 */
							BgL_approxz00_bglt BgL_arg1874z00_4679;

							{
								BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_4955;

								{
									obj_t BgL_auxz00_4956;

									{	/* Cfa/vector.scm 291 */
										BgL_objectz00_bglt BgL_tmpz00_4957;

										BgL_tmpz00_4957 =
											((BgL_objectz00_bglt)
											((BgL_vallocz00_bglt) BgL_allocz00_4627));
										BgL_auxz00_4956 = BGL_OBJECT_WIDENING(BgL_tmpz00_4957);
									}
									BgL_auxz00_4955 =
										((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_4956);
								}
								BgL_arg1874z00_4679 =
									(((BgL_valloczf2cinfozb2optimz40_bglt)
										COBJECT(BgL_auxz00_4955))->BgL_valuezd2approxzd2);
							}
							return
								BGl_approxzd2setzd2topz12z12zzcfa_approxz00
								(BgL_arg1874z00_4679);
						}
					}
			}
		}

	}



/* &loose-alloc!-make-ve1621 */
	obj_t BGl_z62loosezd2allocz12zd2makezd2ve1621za2zzcfa_vectorz00(obj_t
		BgL_envz00_4628, obj_t BgL_allocz00_4629)
	{
		{	/* Cfa/vector.scm 263 */
			{	/* Cfa/vector.scm 265 */
				bool_t BgL_test2132z00_4964;

				{	/* Cfa/vector.scm 265 */
					long BgL_arg1869z00_4681;

					{
						BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_4965;

						{
							obj_t BgL_auxz00_4966;

							{	/* Cfa/vector.scm 265 */
								BgL_objectz00_bglt BgL_tmpz00_4967;

								BgL_tmpz00_4967 =
									((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_allocz00_4629));
								BgL_auxz00_4966 = BGL_OBJECT_WIDENING(BgL_tmpz00_4967);
							}
							BgL_auxz00_4965 =
								((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_4966);
						}
						BgL_arg1869z00_4681 =
							(((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_4965))->
							BgL_lostzd2stampzd2);
					}
					BgL_test2132z00_4964 =
						(BgL_arg1869z00_4681 ==
						(long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00));
				}
				if (BgL_test2132z00_4964)
					{	/* Cfa/vector.scm 265 */
						return BUNSPEC;
					}
				else
					{	/* Cfa/vector.scm 265 */
						{
							BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_4975;

							{
								obj_t BgL_auxz00_4976;

								{	/* Cfa/vector.scm 269 */
									BgL_objectz00_bglt BgL_tmpz00_4977;

									BgL_tmpz00_4977 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) BgL_allocz00_4629));
									BgL_auxz00_4976 = BGL_OBJECT_WIDENING(BgL_tmpz00_4977);
								}
								BgL_auxz00_4975 =
									((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_4976);
							}
							((((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_4975))->
									BgL_lostzd2stampzd2) =
								((long) (long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00)),
								BUNSPEC);
						}
						{	/* Cfa/vector.scm 270 */
							BgL_approxz00_bglt BgL_arg1864z00_4682;

							{
								BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_4984;

								{
									obj_t BgL_auxz00_4985;

									{	/* Cfa/vector.scm 270 */
										BgL_objectz00_bglt BgL_tmpz00_4986;

										BgL_tmpz00_4986 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_allocz00_4629));
										BgL_auxz00_4985 = BGL_OBJECT_WIDENING(BgL_tmpz00_4986);
									}
									BgL_auxz00_4984 =
										((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_4985);
								}
								BgL_arg1864z00_4682 =
									(((BgL_makezd2vectorzd2appz00_bglt)
										COBJECT(BgL_auxz00_4984))->BgL_valuezd2approxzd2);
							}
							BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
								(BGl_loosezd2allocz12zd2envz12zzcfa_loosez00,
								BgL_arg1864z00_4682);
						}
						{	/* Cfa/vector.scm 271 */
							BgL_approxz00_bglt BgL_arg1866z00_4683;

							{
								BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_4993;

								{
									obj_t BgL_auxz00_4994;

									{	/* Cfa/vector.scm 271 */
										BgL_objectz00_bglt BgL_tmpz00_4995;

										BgL_tmpz00_4995 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_allocz00_4629));
										BgL_auxz00_4994 = BGL_OBJECT_WIDENING(BgL_tmpz00_4995);
									}
									BgL_auxz00_4993 =
										((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_4994);
								}
								BgL_arg1866z00_4683 =
									(((BgL_makezd2vectorzd2appz00_bglt)
										COBJECT(BgL_auxz00_4993))->BgL_valuezd2approxzd2);
							}
							BGl_approxzd2setzd2typez12z12zzcfa_approxz00(BgL_arg1866z00_4683,
								((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
						}
						{	/* Cfa/vector.scm 272 */
							BgL_approxz00_bglt BgL_arg1868z00_4684;

							{
								BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5003;

								{
									obj_t BgL_auxz00_5004;

									{	/* Cfa/vector.scm 272 */
										BgL_objectz00_bglt BgL_tmpz00_5005;

										BgL_tmpz00_5005 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_allocz00_4629));
										BgL_auxz00_5004 = BGL_OBJECT_WIDENING(BgL_tmpz00_5005);
									}
									BgL_auxz00_5003 =
										((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5004);
								}
								BgL_arg1868z00_4684 =
									(((BgL_makezd2vectorzd2appz00_bglt)
										COBJECT(BgL_auxz00_5003))->BgL_valuezd2approxzd2);
							}
							return
								BGl_approxzd2setzd2topz12z12zzcfa_approxz00
								(BgL_arg1868z00_4684);
						}
					}
			}
		}

	}



/* &cfa!-vset!/Cinfo1619 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2vsetz12zf2Cinfo1619z42zzcfa_vectorz00(obj_t
		BgL_envz00_4630, obj_t BgL_nodez00_4631)
	{
		{	/* Cfa/vector.scm 218 */
			{	/* Cfa/vector.scm 222 */
				obj_t BgL_arg1842z00_4686;

				{	/* Cfa/vector.scm 222 */
					obj_t BgL_pairz00_4687;

					BgL_pairz00_4687 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vsetz12z12_bglt) BgL_nodez00_4631))))->BgL_exprza2za2);
					BgL_arg1842z00_4686 = CAR(CDR(BgL_pairz00_4687));
				}
				BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1842z00_4686));
			}
			{	/* Cfa/vector.scm 223 */
				BgL_approxz00_bglt BgL_veczd2approxzd2_4688;
				BgL_approxz00_bglt BgL_valzd2approxzd2_4689;

				{	/* Cfa/vector.scm 223 */
					obj_t BgL_arg1857z00_4690;

					{	/* Cfa/vector.scm 223 */
						obj_t BgL_pairz00_4691;

						BgL_pairz00_4691 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_vsetz12z12_bglt) BgL_nodez00_4631))))->
							BgL_exprza2za2);
						BgL_arg1857z00_4690 = CAR(BgL_pairz00_4691);
					}
					BgL_veczd2approxzd2_4688 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1857z00_4690));
				}
				{	/* Cfa/vector.scm 224 */
					obj_t BgL_arg1859z00_4692;

					{	/* Cfa/vector.scm 224 */
						obj_t BgL_pairz00_4693;

						BgL_pairz00_4693 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_vsetz12z12_bglt) BgL_nodez00_4631))))->
							BgL_exprza2za2);
						BgL_arg1859z00_4692 = CAR(CDR(CDR(BgL_pairz00_4693)));
					}
					BgL_valzd2approxzd2_4689 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1859z00_4692));
				}
				{	/* Cfa/vector.scm 226 */
					bool_t BgL_test2133z00_5033;

					{
						BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_5034;

						{
							obj_t BgL_auxz00_5035;

							{	/* Cfa/vector.scm 226 */
								BgL_objectz00_bglt BgL_tmpz00_5036;

								BgL_tmpz00_5036 =
									((BgL_objectz00_bglt)
									((BgL_vsetz12z12_bglt) BgL_nodez00_4631));
								BgL_auxz00_5035 = BGL_OBJECT_WIDENING(BgL_tmpz00_5036);
							}
							BgL_auxz00_5034 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_5035);
						}
						BgL_test2133z00_5033 =
							(((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_5034))->
							BgL_tvectorzf3zf3);
					}
					if (BgL_test2133z00_5033)
						{	/* Cfa/vector.scm 226 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Cfa/vector.scm 226 */
							if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 2L))
								{	/* Cfa/vector.scm 228 */
									{	/* Cfa/vector.scm 232 */
										bool_t BgL_test2135z00_5046;

										{	/* Cfa/vector.scm 232 */
											BgL_typez00_bglt BgL_arg1848z00_4694;

											BgL_arg1848z00_4694 =
												(((BgL_approxz00_bglt)
													COBJECT(BgL_veczd2approxzd2_4688))->BgL_typez00);
											BgL_test2135z00_5046 =
												(((obj_t) BgL_arg1848z00_4694) ==
												BGl_za2vectorza2z00zztype_cachez00);
										}
										if (BgL_test2135z00_5046)
											{	/* Cfa/vector.scm 232 */
												BFALSE;
											}
										else
											{	/* Cfa/vector.scm 232 */
												BGl_approxzd2setzd2typez12z12zzcfa_approxz00
													(BgL_valzd2approxzd2_4689,
													((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
											}
									}
									if (
										(((BgL_approxz00_bglt) COBJECT(BgL_veczd2approxzd2_4688))->
											BgL_topzf3zf3))
										{	/* Cfa/vector.scm 235 */
											((obj_t)
												BGl_loosez12z12zzcfa_loosez00(BgL_valzd2approxzd2_4689,
													CNST_TABLE_REF(1)));
										}
									else
										{	/* Cfa/vector.scm 242 */
											obj_t BgL_zc3z04anonymousza31851ze3z87_4695;

											BgL_zc3z04anonymousza31851ze3z87_4695 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31851ze3ze5zzcfa_vectorz00,
												(int) (1L), (int) (1L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31851ze3z87_4695,
												(int) (0L), ((obj_t) BgL_valzd2approxzd2_4689));
											BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
												(BgL_zc3z04anonymousza31851ze3z87_4695,
												BgL_veczd2approxzd2_4688);
								}}
							else
								{	/* Cfa/vector.scm 228 */
									BGl_loosez12z12zzcfa_loosez00(BgL_veczd2approxzd2_4688,
										CNST_TABLE_REF(1));
									((obj_t)
										BGl_loosez12z12zzcfa_loosez00(BgL_valzd2approxzd2_4689,
											CNST_TABLE_REF(1)));
								}
						}
				}
			}
			{
				BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_5069;

				{
					obj_t BgL_auxz00_5070;

					{	/* Cfa/vector.scm 254 */
						BgL_objectz00_bglt BgL_tmpz00_5071;

						BgL_tmpz00_5071 =
							((BgL_objectz00_bglt) ((BgL_vsetz12z12_bglt) BgL_nodez00_4631));
						BgL_auxz00_5070 = BGL_OBJECT_WIDENING(BgL_tmpz00_5071);
					}
					BgL_auxz00_5069 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_5070);
				}
				return
					(((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_5069))->
					BgL_approxz00);
			}
		}

	}



/* &<@anonymous:1851> */
	obj_t BGl_z62zc3z04anonymousza31851ze3ze5zzcfa_vectorz00(obj_t
		BgL_envz00_4632, obj_t BgL_appz00_4634)
	{
		{	/* Cfa/vector.scm 240 */
			{	/* Cfa/vector.scm 242 */
				BgL_approxz00_bglt BgL_valzd2approxzd2_4633;

				BgL_valzd2approxzd2_4633 =
					((BgL_approxz00_bglt) PROCEDURE_REF(BgL_envz00_4632, (int) (0L)));
				{	/* Cfa/vector.scm 242 */
					bool_t BgL_test2137z00_5080;

					{	/* Cfa/vector.scm 242 */
						obj_t BgL_classz00_4696;

						BgL_classz00_4696 = BGl_makezd2vectorzd2appz00zzcfa_info2z00;
						if (BGL_OBJECTP(BgL_appz00_4634))
							{	/* Cfa/vector.scm 242 */
								BgL_objectz00_bglt BgL_arg1807z00_4697;

								BgL_arg1807z00_4697 = (BgL_objectz00_bglt) (BgL_appz00_4634);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/vector.scm 242 */
										long BgL_idxz00_4698;

										BgL_idxz00_4698 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4697);
										BgL_test2137z00_5080 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4698 + 4L)) == BgL_classz00_4696);
									}
								else
									{	/* Cfa/vector.scm 242 */
										bool_t BgL_res2082z00_4701;

										{	/* Cfa/vector.scm 242 */
											obj_t BgL_oclassz00_4702;

											{	/* Cfa/vector.scm 242 */
												obj_t BgL_arg1815z00_4703;
												long BgL_arg1816z00_4704;

												BgL_arg1815z00_4703 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/vector.scm 242 */
													long BgL_arg1817z00_4705;

													BgL_arg1817z00_4705 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4697);
													BgL_arg1816z00_4704 =
														(BgL_arg1817z00_4705 - OBJECT_TYPE);
												}
												BgL_oclassz00_4702 =
													VECTOR_REF(BgL_arg1815z00_4703, BgL_arg1816z00_4704);
											}
											{	/* Cfa/vector.scm 242 */
												bool_t BgL__ortest_1115z00_4706;

												BgL__ortest_1115z00_4706 =
													(BgL_classz00_4696 == BgL_oclassz00_4702);
												if (BgL__ortest_1115z00_4706)
													{	/* Cfa/vector.scm 242 */
														BgL_res2082z00_4701 = BgL__ortest_1115z00_4706;
													}
												else
													{	/* Cfa/vector.scm 242 */
														long BgL_odepthz00_4707;

														{	/* Cfa/vector.scm 242 */
															obj_t BgL_arg1804z00_4708;

															BgL_arg1804z00_4708 = (BgL_oclassz00_4702);
															BgL_odepthz00_4707 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4708);
														}
														if ((4L < BgL_odepthz00_4707))
															{	/* Cfa/vector.scm 242 */
																obj_t BgL_arg1802z00_4709;

																{	/* Cfa/vector.scm 242 */
																	obj_t BgL_arg1803z00_4710;

																	BgL_arg1803z00_4710 = (BgL_oclassz00_4702);
																	BgL_arg1802z00_4709 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4710,
																		4L);
																}
																BgL_res2082z00_4701 =
																	(BgL_arg1802z00_4709 == BgL_classz00_4696);
															}
														else
															{	/* Cfa/vector.scm 242 */
																BgL_res2082z00_4701 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2137z00_5080 = BgL_res2082z00_4701;
									}
							}
						else
							{	/* Cfa/vector.scm 242 */
								BgL_test2137z00_5080 = ((bool_t) 0);
							}
					}
					if (BgL_test2137z00_5080)
						{	/* Cfa/vector.scm 242 */
							{
								BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5103;

								{
									obj_t BgL_auxz00_5104;

									{	/* Cfa/vector.scm 244 */
										BgL_objectz00_bglt BgL_tmpz00_5105;

										BgL_tmpz00_5105 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_appz00_4634));
										BgL_auxz00_5104 = BGL_OBJECT_WIDENING(BgL_tmpz00_5105);
									}
									BgL_auxz00_5103 =
										((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5104);
								}
								((((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_5103))->
										BgL_seenzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
							}
							{	/* Cfa/vector.scm 245 */
								BgL_approxz00_bglt BgL_arg1853z00_4711;

								{
									BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5111;

									{
										obj_t BgL_auxz00_5112;

										{	/* Cfa/vector.scm 245 */
											BgL_objectz00_bglt BgL_tmpz00_5113;

											BgL_tmpz00_5113 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_appz00_4634));
											BgL_auxz00_5112 = BGL_OBJECT_WIDENING(BgL_tmpz00_5113);
										}
										BgL_auxz00_5111 =
											((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5112);
									}
									BgL_arg1853z00_4711 =
										(((BgL_makezd2vectorzd2appz00_bglt)
											COBJECT(BgL_auxz00_5111))->BgL_valuezd2approxzd2);
								}
								return
									((obj_t)
									BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1853z00_4711,
										BgL_valzd2approxzd2_4633));
							}
						}
					else
						{	/* Cfa/vector.scm 246 */
							bool_t BgL_test2142z00_5121;

							{	/* Cfa/vector.scm 246 */
								obj_t BgL_classz00_4712;

								BgL_classz00_4712 = BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00;
								if (BGL_OBJECTP(BgL_appz00_4634))
									{	/* Cfa/vector.scm 246 */
										BgL_objectz00_bglt BgL_arg1809z00_4713;
										long BgL_arg1810z00_4714;

										BgL_arg1809z00_4713 =
											(BgL_objectz00_bglt) (BgL_appz00_4634);
										BgL_arg1810z00_4714 = BGL_CLASS_DEPTH(BgL_classz00_4712);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/vector.scm 246 */
												long BgL_idxz00_4715;

												BgL_idxz00_4715 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1809z00_4713);
												BgL_test2142z00_5121 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4715 + BgL_arg1810z00_4714)) ==
													BgL_classz00_4712);
											}
										else
											{	/* Cfa/vector.scm 246 */
												bool_t BgL_res2083z00_4718;

												{	/* Cfa/vector.scm 246 */
													obj_t BgL_oclassz00_4719;

													{	/* Cfa/vector.scm 246 */
														obj_t BgL_arg1815z00_4720;
														long BgL_arg1816z00_4721;

														BgL_arg1815z00_4720 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/vector.scm 246 */
															long BgL_arg1817z00_4722;

															BgL_arg1817z00_4722 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1809z00_4713);
															BgL_arg1816z00_4721 =
																(BgL_arg1817z00_4722 - OBJECT_TYPE);
														}
														BgL_oclassz00_4719 =
															VECTOR_REF(BgL_arg1815z00_4720,
															BgL_arg1816z00_4721);
													}
													{	/* Cfa/vector.scm 246 */
														bool_t BgL__ortest_1115z00_4723;

														BgL__ortest_1115z00_4723 =
															(BgL_classz00_4712 == BgL_oclassz00_4719);
														if (BgL__ortest_1115z00_4723)
															{	/* Cfa/vector.scm 246 */
																BgL_res2083z00_4718 = BgL__ortest_1115z00_4723;
															}
														else
															{	/* Cfa/vector.scm 246 */
																long BgL_odepthz00_4724;

																{	/* Cfa/vector.scm 246 */
																	obj_t BgL_arg1804z00_4725;

																	BgL_arg1804z00_4725 = (BgL_oclassz00_4719);
																	BgL_odepthz00_4724 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4725);
																}
																if ((BgL_arg1810z00_4714 < BgL_odepthz00_4724))
																	{	/* Cfa/vector.scm 246 */
																		obj_t BgL_arg1802z00_4726;

																		{	/* Cfa/vector.scm 246 */
																			obj_t BgL_arg1803z00_4727;

																			BgL_arg1803z00_4727 =
																				(BgL_oclassz00_4719);
																			BgL_arg1802z00_4726 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4727,
																				BgL_arg1810z00_4714);
																		}
																		BgL_res2083z00_4718 =
																			(BgL_arg1802z00_4726 ==
																			BgL_classz00_4712);
																	}
																else
																	{	/* Cfa/vector.scm 246 */
																		BgL_res2083z00_4718 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2142z00_5121 = BgL_res2083z00_4718;
											}
									}
								else
									{	/* Cfa/vector.scm 246 */
										BgL_test2142z00_5121 = ((bool_t) 0);
									}
							}
							if (BgL_test2142z00_5121)
								{	/* Cfa/vector.scm 246 */
									{
										BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_5145;

										{
											obj_t BgL_auxz00_5146;

											{	/* Cfa/vector.scm 248 */
												BgL_objectz00_bglt BgL_tmpz00_5147;

												BgL_tmpz00_5147 =
													((BgL_objectz00_bglt)
													((BgL_vallocz00_bglt) BgL_appz00_4634));
												BgL_auxz00_5146 = BGL_OBJECT_WIDENING(BgL_tmpz00_5147);
											}
											BgL_auxz00_5145 =
												((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_5146);
										}
										((((BgL_valloczf2cinfozb2optimz40_bglt)
													COBJECT(BgL_auxz00_5145))->BgL_seenzf3zf3) =
											((bool_t) ((bool_t) 1)), BUNSPEC);
									}
									{	/* Cfa/vector.scm 249 */
										BgL_approxz00_bglt BgL_arg1856z00_4728;

										{
											BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_5153;

											{
												obj_t BgL_auxz00_5154;

												{	/* Cfa/vector.scm 249 */
													BgL_objectz00_bglt BgL_tmpz00_5155;

													BgL_tmpz00_5155 =
														((BgL_objectz00_bglt)
														((BgL_vallocz00_bglt) BgL_appz00_4634));
													BgL_auxz00_5154 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5155);
												}
												BgL_auxz00_5153 =
													((BgL_valloczf2cinfozb2optimz40_bglt)
													BgL_auxz00_5154);
											}
											BgL_arg1856z00_4728 =
												(((BgL_valloczf2cinfozb2optimz40_bglt)
													COBJECT(BgL_auxz00_5153))->BgL_valuezd2approxzd2);
										}
										return
											((obj_t)
											BGl_unionzd2approxz12zc0zzcfa_approxz00
											(BgL_arg1856z00_4728, BgL_valzd2approxzd2_4633));
									}
								}
							else
								{	/* Cfa/vector.scm 246 */
									return BFALSE;
								}
						}
				}
			}
		}

	}



/* &cfa!-vref/Cinfo1617 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2vrefzf2Cinfo1617z50zzcfa_vectorz00(obj_t
		BgL_envz00_4635, obj_t BgL_nodez00_4636)
	{
		{	/* Cfa/vector.scm 176 */
			{	/* Cfa/vector.scm 180 */
				obj_t BgL_arg1770z00_4730;

				{	/* Cfa/vector.scm 180 */
					obj_t BgL_pairz00_4731;

					BgL_pairz00_4731 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vrefz00_bglt) BgL_nodez00_4636))))->BgL_exprza2za2);
					BgL_arg1770z00_4730 = CAR(CDR(BgL_pairz00_4731));
				}
				BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1770z00_4730));
			}
			{	/* Cfa/vector.scm 181 */
				BgL_approxz00_bglt BgL_veczd2approxzd2_4732;

				{	/* Cfa/vector.scm 181 */
					obj_t BgL_arg1839z00_4733;

					{	/* Cfa/vector.scm 181 */
						obj_t BgL_pairz00_4734;

						BgL_pairz00_4734 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_vrefz00_bglt) BgL_nodez00_4636))))->BgL_exprza2za2);
						BgL_arg1839z00_4733 = CAR(BgL_pairz00_4734);
					}
					BgL_veczd2approxzd2_4732 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1839z00_4733));
				}
				{	/* Cfa/vector.scm 183 */
					bool_t BgL_test2147z00_5176;

					{
						BgL_vrefzf2cinfozf2_bglt BgL_auxz00_5177;

						{
							obj_t BgL_auxz00_5178;

							{	/* Cfa/vector.scm 183 */
								BgL_objectz00_bglt BgL_tmpz00_5179;

								BgL_tmpz00_5179 =
									((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_nodez00_4636));
								BgL_auxz00_5178 = BGL_OBJECT_WIDENING(BgL_tmpz00_5179);
							}
							BgL_auxz00_5177 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_5178);
						}
						BgL_test2147z00_5176 =
							(((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5177))->
							BgL_tvectorzf3zf3);
					}
					if (BgL_test2147z00_5176)
						{
							BgL_approxz00_bglt BgL_auxz00_5185;

							{
								BgL_vrefzf2cinfozf2_bglt BgL_auxz00_5186;

								{
									obj_t BgL_auxz00_5187;

									{	/* Cfa/vector.scm 184 */
										BgL_objectz00_bglt BgL_tmpz00_5188;

										BgL_tmpz00_5188 =
											((BgL_objectz00_bglt)
											((BgL_vrefz00_bglt) BgL_nodez00_4636));
										BgL_auxz00_5187 = BGL_OBJECT_WIDENING(BgL_tmpz00_5188);
									}
									BgL_auxz00_5186 =
										((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_5187);
								}
								BgL_auxz00_5185 =
									(((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5186))->
									BgL_approxz00);
							}
							((obj_t) BgL_auxz00_5185);
						}
					else
						{	/* Cfa/vector.scm 183 */
							if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 2L))
								{	/* Cfa/vector.scm 185 */
									{	/* Cfa/vector.scm 190 */
										bool_t BgL_test2149z00_5198;

										{	/* Cfa/vector.scm 190 */
											bool_t BgL_test2150z00_5199;

											{	/* Cfa/vector.scm 190 */
												BgL_typez00_bglt BgL_arg1806z00_4735;

												BgL_arg1806z00_4735 =
													(((BgL_approxz00_bglt)
														COBJECT(BgL_veczd2approxzd2_4732))->BgL_typez00);
												BgL_test2150z00_5199 =
													(((obj_t) BgL_arg1806z00_4735) ==
													BGl_za2vectorza2z00zztype_cachez00);
											}
											if (BgL_test2150z00_5199)
												{	/* Cfa/vector.scm 190 */
													BgL_test2149z00_5198 =
														(((BgL_approxz00_bglt)
															COBJECT(BgL_veczd2approxzd2_4732))->
														BgL_topzf3zf3);
												}
											else
												{	/* Cfa/vector.scm 190 */
													BgL_test2149z00_5198 = ((bool_t) 1);
												}
										}
										if (BgL_test2149z00_5198)
											{	/* Cfa/vector.scm 192 */
												BgL_approxz00_bglt BgL_arg1805z00_4736;

												{
													BgL_vrefzf2cinfozf2_bglt BgL_auxz00_5204;

													{
														obj_t BgL_auxz00_5205;

														{	/* Cfa/vector.scm 192 */
															BgL_objectz00_bglt BgL_tmpz00_5206;

															BgL_tmpz00_5206 =
																((BgL_objectz00_bglt)
																((BgL_vrefz00_bglt) BgL_nodez00_4636));
															BgL_auxz00_5205 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_5206);
														}
														BgL_auxz00_5204 =
															((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_5205);
													}
													BgL_arg1805z00_4736 =
														(((BgL_vrefzf2cinfozf2_bglt)
															COBJECT(BgL_auxz00_5204))->BgL_approxz00);
												}
												BGl_approxzd2setzd2typez12z12zzcfa_approxz00
													(BgL_arg1805z00_4736,
													((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
											}
										else
											{	/* Cfa/vector.scm 190 */
												BFALSE;
											}
									}
									if (
										(((BgL_approxz00_bglt) COBJECT(BgL_veczd2approxzd2_4732))->
											BgL_topzf3zf3))
										{	/* Cfa/vector.scm 195 */
											BgL_approxz00_bglt BgL_arg1808z00_4737;

											{
												BgL_vrefzf2cinfozf2_bglt BgL_auxz00_5216;

												{
													obj_t BgL_auxz00_5217;

													{	/* Cfa/vector.scm 195 */
														BgL_objectz00_bglt BgL_tmpz00_5218;

														BgL_tmpz00_5218 =
															((BgL_objectz00_bglt)
															((BgL_vrefz00_bglt) BgL_nodez00_4636));
														BgL_auxz00_5217 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5218);
													}
													BgL_auxz00_5216 =
														((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_5217);
												}
												BgL_arg1808z00_4737 =
													(((BgL_vrefzf2cinfozf2_bglt)
														COBJECT(BgL_auxz00_5216))->BgL_approxz00);
											}
											BGl_approxzd2setzd2topz12z12zzcfa_approxz00
												(BgL_arg1808z00_4737);
										}
									else
										{	/* Cfa/vector.scm 194 */
											BFALSE;
										}
									{	/* Cfa/vector.scm 200 */
										obj_t BgL_zc3z04anonymousza31813ze3z87_4738;

										BgL_zc3z04anonymousza31813ze3z87_4738 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31813ze3ze5zzcfa_vectorz00,
											(int) (1L), (int) (1L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31813ze3z87_4738,
											(int) (0L),
											((obj_t) ((BgL_vrefz00_bglt) BgL_nodez00_4636)));
										BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
											(BgL_zc3z04anonymousza31813ze3z87_4738,
											BgL_veczd2approxzd2_4732);
								}}
							else
								{	/* Cfa/vector.scm 185 */
									((obj_t)
										BGl_loosez12z12zzcfa_loosez00(BgL_veczd2approxzd2_4732,
											CNST_TABLE_REF(1)));
								}
						}
				}
				{
					BgL_vrefzf2cinfozf2_bglt BgL_auxz00_5236;

					{
						obj_t BgL_auxz00_5237;

						{	/* Cfa/vector.scm 213 */
							BgL_objectz00_bglt BgL_tmpz00_5238;

							BgL_tmpz00_5238 =
								((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_nodez00_4636));
							BgL_auxz00_5237 = BGL_OBJECT_WIDENING(BgL_tmpz00_5238);
						}
						BgL_auxz00_5236 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_5237);
					}
					return
						(((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5236))->
						BgL_approxz00);
				}
			}
		}

	}



/* &<@anonymous:1813> */
	obj_t BGl_z62zc3z04anonymousza31813ze3ze5zzcfa_vectorz00(obj_t
		BgL_envz00_4637, obj_t BgL_appz00_4639)
	{
		{	/* Cfa/vector.scm 198 */
			{	/* Cfa/vector.scm 200 */
				BgL_vrefz00_bglt BgL_i1210z00_4638;

				BgL_i1210z00_4638 =
					((BgL_vrefz00_bglt) PROCEDURE_REF(BgL_envz00_4637, (int) (0L)));
				{	/* Cfa/vector.scm 200 */
					bool_t BgL_test2152z00_5247;

					{	/* Cfa/vector.scm 200 */
						obj_t BgL_classz00_4739;

						BgL_classz00_4739 = BGl_makezd2vectorzd2appz00zzcfa_info2z00;
						if (BGL_OBJECTP(BgL_appz00_4639))
							{	/* Cfa/vector.scm 200 */
								BgL_objectz00_bglt BgL_arg1807z00_4740;

								BgL_arg1807z00_4740 = (BgL_objectz00_bglt) (BgL_appz00_4639);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/vector.scm 200 */
										long BgL_idxz00_4741;

										BgL_idxz00_4741 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4740);
										BgL_test2152z00_5247 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4741 + 4L)) == BgL_classz00_4739);
									}
								else
									{	/* Cfa/vector.scm 200 */
										bool_t BgL_res2080z00_4744;

										{	/* Cfa/vector.scm 200 */
											obj_t BgL_oclassz00_4745;

											{	/* Cfa/vector.scm 200 */
												obj_t BgL_arg1815z00_4746;
												long BgL_arg1816z00_4747;

												BgL_arg1815z00_4746 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/vector.scm 200 */
													long BgL_arg1817z00_4748;

													BgL_arg1817z00_4748 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4740);
													BgL_arg1816z00_4747 =
														(BgL_arg1817z00_4748 - OBJECT_TYPE);
												}
												BgL_oclassz00_4745 =
													VECTOR_REF(BgL_arg1815z00_4746, BgL_arg1816z00_4747);
											}
											{	/* Cfa/vector.scm 200 */
												bool_t BgL__ortest_1115z00_4749;

												BgL__ortest_1115z00_4749 =
													(BgL_classz00_4739 == BgL_oclassz00_4745);
												if (BgL__ortest_1115z00_4749)
													{	/* Cfa/vector.scm 200 */
														BgL_res2080z00_4744 = BgL__ortest_1115z00_4749;
													}
												else
													{	/* Cfa/vector.scm 200 */
														long BgL_odepthz00_4750;

														{	/* Cfa/vector.scm 200 */
															obj_t BgL_arg1804z00_4751;

															BgL_arg1804z00_4751 = (BgL_oclassz00_4745);
															BgL_odepthz00_4750 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4751);
														}
														if ((4L < BgL_odepthz00_4750))
															{	/* Cfa/vector.scm 200 */
																obj_t BgL_arg1802z00_4752;

																{	/* Cfa/vector.scm 200 */
																	obj_t BgL_arg1803z00_4753;

																	BgL_arg1803z00_4753 = (BgL_oclassz00_4745);
																	BgL_arg1802z00_4752 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4753,
																		4L);
																}
																BgL_res2080z00_4744 =
																	(BgL_arg1802z00_4752 == BgL_classz00_4739);
															}
														else
															{	/* Cfa/vector.scm 200 */
																BgL_res2080z00_4744 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2152z00_5247 = BgL_res2080z00_4744;
									}
							}
						else
							{	/* Cfa/vector.scm 200 */
								BgL_test2152z00_5247 = ((bool_t) 0);
							}
					}
					if (BgL_test2152z00_5247)
						{	/* Cfa/vector.scm 200 */
							{
								BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5270;

								{
									obj_t BgL_auxz00_5271;

									{	/* Cfa/vector.scm 202 */
										BgL_objectz00_bglt BgL_tmpz00_5272;

										BgL_tmpz00_5272 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_appz00_4639));
										BgL_auxz00_5271 = BGL_OBJECT_WIDENING(BgL_tmpz00_5272);
									}
									BgL_auxz00_5270 =
										((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5271);
								}
								((((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_5270))->
										BgL_seenzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
							}
							{	/* Cfa/vector.scm 203 */
								BgL_approxz00_bglt BgL_arg1820z00_4754;
								BgL_approxz00_bglt BgL_arg1822z00_4755;

								{
									BgL_vrefzf2cinfozf2_bglt BgL_auxz00_5278;

									{
										obj_t BgL_auxz00_5279;

										{	/* Cfa/vector.scm 203 */
											BgL_objectz00_bglt BgL_tmpz00_5280;

											BgL_tmpz00_5280 =
												((BgL_objectz00_bglt) BgL_i1210z00_4638);
											BgL_auxz00_5279 = BGL_OBJECT_WIDENING(BgL_tmpz00_5280);
										}
										BgL_auxz00_5278 =
											((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_5279);
									}
									BgL_arg1820z00_4754 =
										(((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5278))->
										BgL_approxz00);
								}
								{
									BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5285;

									{
										obj_t BgL_auxz00_5286;

										{	/* Cfa/vector.scm 203 */
											BgL_objectz00_bglt BgL_tmpz00_5287;

											BgL_tmpz00_5287 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_appz00_4639));
											BgL_auxz00_5286 = BGL_OBJECT_WIDENING(BgL_tmpz00_5287);
										}
										BgL_auxz00_5285 =
											((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5286);
									}
									BgL_arg1822z00_4755 =
										(((BgL_makezd2vectorzd2appz00_bglt)
											COBJECT(BgL_auxz00_5285))->BgL_valuezd2approxzd2);
								}
								BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1820z00_4754,
									BgL_arg1822z00_4755);
							}
							{	/* Cfa/vector.scm 204 */
								BgL_approxz00_bglt BgL_arg1823z00_4756;
								BgL_typez00_bglt BgL_arg1831z00_4757;

								{
									BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5294;

									{
										obj_t BgL_auxz00_5295;

										{	/* Cfa/vector.scm 204 */
											BgL_objectz00_bglt BgL_tmpz00_5296;

											BgL_tmpz00_5296 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_appz00_4639));
											BgL_auxz00_5295 = BGL_OBJECT_WIDENING(BgL_tmpz00_5296);
										}
										BgL_auxz00_5294 =
											((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5295);
									}
									BgL_arg1823z00_4756 =
										(((BgL_makezd2vectorzd2appz00_bglt)
											COBJECT(BgL_auxz00_5294))->BgL_valuezd2approxzd2);
								}
								{	/* Cfa/vector.scm 204 */
									BgL_approxz00_bglt BgL_arg1832z00_4758;

									{
										BgL_vrefzf2cinfozf2_bglt BgL_auxz00_5302;

										{
											obj_t BgL_auxz00_5303;

											{	/* Cfa/vector.scm 204 */
												BgL_objectz00_bglt BgL_tmpz00_5304;

												BgL_tmpz00_5304 =
													((BgL_objectz00_bglt) BgL_i1210z00_4638);
												BgL_auxz00_5303 = BGL_OBJECT_WIDENING(BgL_tmpz00_5304);
											}
											BgL_auxz00_5302 =
												((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_5303);
										}
										BgL_arg1832z00_4758 =
											(((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5302))->
											BgL_approxz00);
									}
									BgL_arg1831z00_4757 =
										(((BgL_approxz00_bglt) COBJECT(BgL_arg1832z00_4758))->
										BgL_typez00);
								}
								return
									BGl_approxzd2setzd2typez12z12zzcfa_approxz00
									(BgL_arg1823z00_4756, BgL_arg1831z00_4757);
							}
						}
					else
						{	/* Cfa/vector.scm 205 */
							bool_t BgL_test2157z00_5311;

							{	/* Cfa/vector.scm 205 */
								obj_t BgL_classz00_4759;

								BgL_classz00_4759 = BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00;
								if (BGL_OBJECTP(BgL_appz00_4639))
									{	/* Cfa/vector.scm 205 */
										BgL_objectz00_bglt BgL_arg1809z00_4760;
										long BgL_arg1810z00_4761;

										BgL_arg1809z00_4760 =
											(BgL_objectz00_bglt) (BgL_appz00_4639);
										BgL_arg1810z00_4761 = BGL_CLASS_DEPTH(BgL_classz00_4759);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/vector.scm 205 */
												long BgL_idxz00_4762;

												BgL_idxz00_4762 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1809z00_4760);
												BgL_test2157z00_5311 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4762 + BgL_arg1810z00_4761)) ==
													BgL_classz00_4759);
											}
										else
											{	/* Cfa/vector.scm 205 */
												bool_t BgL_res2081z00_4765;

												{	/* Cfa/vector.scm 205 */
													obj_t BgL_oclassz00_4766;

													{	/* Cfa/vector.scm 205 */
														obj_t BgL_arg1815z00_4767;
														long BgL_arg1816z00_4768;

														BgL_arg1815z00_4767 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/vector.scm 205 */
															long BgL_arg1817z00_4769;

															BgL_arg1817z00_4769 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1809z00_4760);
															BgL_arg1816z00_4768 =
																(BgL_arg1817z00_4769 - OBJECT_TYPE);
														}
														BgL_oclassz00_4766 =
															VECTOR_REF(BgL_arg1815z00_4767,
															BgL_arg1816z00_4768);
													}
													{	/* Cfa/vector.scm 205 */
														bool_t BgL__ortest_1115z00_4770;

														BgL__ortest_1115z00_4770 =
															(BgL_classz00_4759 == BgL_oclassz00_4766);
														if (BgL__ortest_1115z00_4770)
															{	/* Cfa/vector.scm 205 */
																BgL_res2081z00_4765 = BgL__ortest_1115z00_4770;
															}
														else
															{	/* Cfa/vector.scm 205 */
																long BgL_odepthz00_4771;

																{	/* Cfa/vector.scm 205 */
																	obj_t BgL_arg1804z00_4772;

																	BgL_arg1804z00_4772 = (BgL_oclassz00_4766);
																	BgL_odepthz00_4771 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4772);
																}
																if ((BgL_arg1810z00_4761 < BgL_odepthz00_4771))
																	{	/* Cfa/vector.scm 205 */
																		obj_t BgL_arg1802z00_4773;

																		{	/* Cfa/vector.scm 205 */
																			obj_t BgL_arg1803z00_4774;

																			BgL_arg1803z00_4774 =
																				(BgL_oclassz00_4766);
																			BgL_arg1802z00_4773 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4774,
																				BgL_arg1810z00_4761);
																		}
																		BgL_res2081z00_4765 =
																			(BgL_arg1802z00_4773 ==
																			BgL_classz00_4759);
																	}
																else
																	{	/* Cfa/vector.scm 205 */
																		BgL_res2081z00_4765 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2157z00_5311 = BgL_res2081z00_4765;
											}
									}
								else
									{	/* Cfa/vector.scm 205 */
										BgL_test2157z00_5311 = ((bool_t) 0);
									}
							}
							if (BgL_test2157z00_5311)
								{	/* Cfa/vector.scm 205 */
									{
										BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_5335;

										{
											obj_t BgL_auxz00_5336;

											{	/* Cfa/vector.scm 207 */
												BgL_objectz00_bglt BgL_tmpz00_5337;

												BgL_tmpz00_5337 =
													((BgL_objectz00_bglt)
													((BgL_vallocz00_bglt) BgL_appz00_4639));
												BgL_auxz00_5336 = BGL_OBJECT_WIDENING(BgL_tmpz00_5337);
											}
											BgL_auxz00_5335 =
												((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_5336);
										}
										((((BgL_valloczf2cinfozb2optimz40_bglt)
													COBJECT(BgL_auxz00_5335))->BgL_seenzf3zf3) =
											((bool_t) ((bool_t) 1)), BUNSPEC);
									}
									{	/* Cfa/vector.scm 208 */
										BgL_approxz00_bglt BgL_arg1834z00_4775;
										BgL_approxz00_bglt BgL_arg1835z00_4776;

										{
											BgL_vrefzf2cinfozf2_bglt BgL_auxz00_5343;

											{
												obj_t BgL_auxz00_5344;

												{	/* Cfa/vector.scm 208 */
													BgL_objectz00_bglt BgL_tmpz00_5345;

													BgL_tmpz00_5345 =
														((BgL_objectz00_bglt) BgL_i1210z00_4638);
													BgL_auxz00_5344 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5345);
												}
												BgL_auxz00_5343 =
													((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_5344);
											}
											BgL_arg1834z00_4775 =
												(((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5343))->
												BgL_approxz00);
										}
										{
											BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_5350;

											{
												obj_t BgL_auxz00_5351;

												{	/* Cfa/vector.scm 208 */
													BgL_objectz00_bglt BgL_tmpz00_5352;

													BgL_tmpz00_5352 =
														((BgL_objectz00_bglt)
														((BgL_vallocz00_bglt) BgL_appz00_4639));
													BgL_auxz00_5351 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5352);
												}
												BgL_auxz00_5350 =
													((BgL_valloczf2cinfozb2optimz40_bglt)
													BgL_auxz00_5351);
											}
											BgL_arg1835z00_4776 =
												(((BgL_valloczf2cinfozb2optimz40_bglt)
													COBJECT(BgL_auxz00_5350))->BgL_valuezd2approxzd2);
										}
										BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1834z00_4775,
											BgL_arg1835z00_4776);
									}
									{	/* Cfa/vector.scm 209 */
										BgL_approxz00_bglt BgL_arg1836z00_4777;
										BgL_typez00_bglt BgL_arg1837z00_4778;

										{
											BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_5359;

											{
												obj_t BgL_auxz00_5360;

												{	/* Cfa/vector.scm 209 */
													BgL_objectz00_bglt BgL_tmpz00_5361;

													BgL_tmpz00_5361 =
														((BgL_objectz00_bglt)
														((BgL_vallocz00_bglt) BgL_appz00_4639));
													BgL_auxz00_5360 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5361);
												}
												BgL_auxz00_5359 =
													((BgL_valloczf2cinfozb2optimz40_bglt)
													BgL_auxz00_5360);
											}
											BgL_arg1836z00_4777 =
												(((BgL_valloczf2cinfozb2optimz40_bglt)
													COBJECT(BgL_auxz00_5359))->BgL_valuezd2approxzd2);
										}
										{	/* Cfa/vector.scm 209 */
											BgL_approxz00_bglt BgL_arg1838z00_4779;

											{
												BgL_vrefzf2cinfozf2_bglt BgL_auxz00_5367;

												{
													obj_t BgL_auxz00_5368;

													{	/* Cfa/vector.scm 209 */
														BgL_objectz00_bglt BgL_tmpz00_5369;

														BgL_tmpz00_5369 =
															((BgL_objectz00_bglt) BgL_i1210z00_4638);
														BgL_auxz00_5368 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5369);
													}
													BgL_auxz00_5367 =
														((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_5368);
												}
												BgL_arg1838z00_4779 =
													(((BgL_vrefzf2cinfozf2_bglt)
														COBJECT(BgL_auxz00_5367))->BgL_approxz00);
											}
											BgL_arg1837z00_4778 =
												(((BgL_approxz00_bglt) COBJECT(BgL_arg1838z00_4779))->
												BgL_typez00);
										}
										return
											BGl_approxzd2setzd2typez12z12zzcfa_approxz00
											(BgL_arg1836z00_4777, BgL_arg1837z00_4778);
									}
								}
							else
								{	/* Cfa/vector.scm 205 */
									return BFALSE;
								}
						}
				}
			}
		}

	}



/* &cfa!-vlength/Cinfo1615 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2vlengthzf2Cinfo1615z50zzcfa_vectorz00(obj_t
		BgL_envz00_4640, obj_t BgL_nodez00_4641)
	{
		{	/* Cfa/vector.scm 165 */
			{	/* Cfa/vector.scm 168 */
				BgL_approxz00_bglt BgL_veczd2approxzd2_4781;

				{	/* Cfa/vector.scm 168 */
					obj_t BgL_arg1765z00_4782;

					{	/* Cfa/vector.scm 168 */
						obj_t BgL_pairz00_4783;

						BgL_pairz00_4783 =
							(((BgL_externz00_bglt) COBJECT(
									((BgL_externz00_bglt)
										((BgL_vlengthz00_bglt) BgL_nodez00_4641))))->
							BgL_exprza2za2);
						BgL_arg1765z00_4782 = CAR(BgL_pairz00_4783);
					}
					BgL_veczd2approxzd2_4781 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1765z00_4782));
				}
				if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 2L))
					{	/* Cfa/vector.scm 169 */
						BFALSE;
					}
				else
					{	/* Cfa/vector.scm 169 */
						((obj_t)
							BGl_loosez12z12zzcfa_loosez00(BgL_veczd2approxzd2_4781,
								CNST_TABLE_REF(1)));
					}
			}
			{
				BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_5388;

				{
					obj_t BgL_auxz00_5389;

					{	/* Cfa/vector.scm 171 */
						BgL_objectz00_bglt BgL_tmpz00_5390;

						BgL_tmpz00_5390 =
							((BgL_objectz00_bglt) ((BgL_vlengthz00_bglt) BgL_nodez00_4641));
						BgL_auxz00_5389 = BGL_OBJECT_WIDENING(BgL_tmpz00_5390);
					}
					BgL_auxz00_5388 = ((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_5389);
				}
				return
					(((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5388))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-valloc/Cinfo1613 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2valloczf2Cinfo1613z50zzcfa_vectorz00(obj_t
		BgL_envz00_4642, obj_t BgL_nodez00_4643)
	{
		{	/* Cfa/vector.scm 156 */
			{	/* Cfa/vector.scm 159 */
				obj_t BgL_g1595z00_4785;

				BgL_g1595z00_4785 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vallocz00_bglt) BgL_nodez00_4643))))->BgL_exprza2za2);
				{
					obj_t BgL_l1593z00_4787;

					BgL_l1593z00_4787 = BgL_g1595z00_4785;
				BgL_zc3z04anonymousza31756ze3z87_4786:
					if (PAIRP(BgL_l1593z00_4787))
						{	/* Cfa/vector.scm 159 */
							{	/* Cfa/vector.scm 159 */
								obj_t BgL_xz00_4788;

								BgL_xz00_4788 = CAR(BgL_l1593z00_4787);
								{	/* Cfa/vector.scm 159 */
									BgL_approxz00_bglt BgL_arg1761z00_4789;

									BgL_arg1761z00_4789 =
										BGl_cfaz12z12zzcfa_cfaz00(
										((BgL_nodez00_bglt) BgL_xz00_4788));
									BGl_loosez12z12zzcfa_loosez00(BgL_arg1761z00_4789,
										CNST_TABLE_REF(1));
								}
							}
							{
								obj_t BgL_l1593z00_5406;

								BgL_l1593z00_5406 = CDR(BgL_l1593z00_4787);
								BgL_l1593z00_4787 = BgL_l1593z00_5406;
								goto BgL_zc3z04anonymousza31756ze3z87_4786;
							}
						}
					else
						{	/* Cfa/vector.scm 159 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_valloczf2cinfozf2_bglt BgL_auxz00_5408;

				{
					obj_t BgL_auxz00_5409;

					{	/* Cfa/vector.scm 160 */
						BgL_objectz00_bglt BgL_tmpz00_5410;

						BgL_tmpz00_5410 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_4643));
						BgL_auxz00_5409 = BGL_OBJECT_WIDENING(BgL_tmpz00_5410);
					}
					BgL_auxz00_5408 = ((BgL_valloczf2cinfozf2_bglt) BgL_auxz00_5409);
				}
				return
					(((BgL_valloczf2cinfozf2_bglt) COBJECT(BgL_auxz00_5408))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-valloc/Cinfo+op1611 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2valloczf2Cinfozb2op1611ze2zzcfa_vectorz00(obj_t
		BgL_envz00_4644, obj_t BgL_nodez00_4645)
	{
		{	/* Cfa/vector.scm 147 */
			{	/* Cfa/vector.scm 150 */
				obj_t BgL_g1592z00_4791;

				BgL_g1592z00_4791 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vallocz00_bglt) BgL_nodez00_4645))))->BgL_exprza2za2);
				{
					obj_t BgL_l1590z00_4793;

					BgL_l1590z00_4793 = BgL_g1592z00_4791;
				BgL_zc3z04anonymousza31752ze3z87_4792:
					if (PAIRP(BgL_l1590z00_4793))
						{	/* Cfa/vector.scm 150 */
							{	/* Cfa/vector.scm 150 */
								obj_t BgL_arg1754z00_4794;

								BgL_arg1754z00_4794 = CAR(BgL_l1590z00_4793);
								BGl_cfaz12z12zzcfa_cfaz00(
									((BgL_nodez00_bglt) BgL_arg1754z00_4794));
							}
							{
								obj_t BgL_l1590z00_5424;

								BgL_l1590z00_5424 = CDR(BgL_l1590z00_4793);
								BgL_l1590z00_4793 = BgL_l1590z00_5424;
								goto BgL_zc3z04anonymousza31752ze3z87_4792;
							}
						}
					else
						{	/* Cfa/vector.scm 150 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_5426;

				{
					obj_t BgL_auxz00_5427;

					{	/* Cfa/vector.scm 151 */
						BgL_objectz00_bglt BgL_tmpz00_5428;

						BgL_tmpz00_5428 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_4645));
						BgL_auxz00_5427 = BGL_OBJECT_WIDENING(BgL_tmpz00_5428);
					}
					BgL_auxz00_5426 =
						((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_5427);
				}
				return
					(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_5426))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-make-vector-app1609 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2makezd2vectorzd2app1609za2zzcfa_vectorz00(obj_t
		BgL_envz00_4646, obj_t BgL_nodez00_4647)
	{
		{	/* Cfa/vector.scm 136 */
			{	/* Cfa/vector.scm 139 */
				obj_t BgL_arg1747z00_4796;

				{	/* Cfa/vector.scm 139 */
					obj_t BgL_pairz00_4797;

					BgL_pairz00_4797 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_4647))))->BgL_argsz00);
					BgL_arg1747z00_4796 = CAR(BgL_pairz00_4797);
				}
				BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1747z00_4796));
			}
			{	/* Cfa/vector.scm 140 */
				BgL_approxz00_bglt BgL_initzd2valuezd2approxz00_4798;

				{	/* Cfa/vector.scm 140 */
					obj_t BgL_arg1750z00_4799;

					{	/* Cfa/vector.scm 140 */
						obj_t BgL_pairz00_4800;

						BgL_pairz00_4800 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4647))))->BgL_argsz00);
						BgL_arg1750z00_4799 = CAR(CDR(BgL_pairz00_4800));
					}
					BgL_initzd2valuezd2approxz00_4798 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1750z00_4799));
				}
				{	/* Cfa/vector.scm 141 */
					BgL_approxz00_bglt BgL_arg1749z00_4801;

					{
						BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5447;

						{
							obj_t BgL_auxz00_5448;

							{	/* Cfa/vector.scm 141 */
								BgL_objectz00_bglt BgL_tmpz00_5449;

								BgL_tmpz00_5449 =
									((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4647));
								BgL_auxz00_5448 = BGL_OBJECT_WIDENING(BgL_tmpz00_5449);
							}
							BgL_auxz00_5447 =
								((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5448);
						}
						BgL_arg1749z00_4801 =
							(((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_5447))->
							BgL_valuezd2approxzd2);
					}
					BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1749z00_4801,
						BgL_initzd2valuezd2approxz00_4798);
				}
				{
					BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5456;

					{
						obj_t BgL_auxz00_5457;

						{	/* Cfa/vector.scm 142 */
							BgL_objectz00_bglt BgL_tmpz00_5458;

							BgL_tmpz00_5458 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4647));
							BgL_auxz00_5457 = BGL_OBJECT_WIDENING(BgL_tmpz00_5458);
						}
						BgL_auxz00_5456 =
							((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5457);
					}
					return
						(((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_5456))->
						BgL_approxz00);
				}
			}
		}

	}



/* &node-setup!-vset!1607 */
	obj_t BGl_z62nodezd2setupz12zd2vsetz121607z62zzcfa_vectorz00(obj_t
		BgL_envz00_4648, obj_t BgL_nodez00_4649)
	{
		{	/* Cfa/vector.scm 126 */
			{	/* Cfa/vector.scm 128 */
				obj_t BgL_arg1724z00_4803;

				BgL_arg1724z00_4803 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vsetz12z12_bglt) BgL_nodez00_4649))))->BgL_exprza2za2);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1724z00_4803);
			}
			{	/* Cfa/vector.scm 129 */
				BgL_vsetz12zf2cinfoze0_bglt BgL_wide1204z00_4804;

				BgL_wide1204z00_4804 =
					((BgL_vsetz12zf2cinfoze0_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_vsetz12zf2cinfoze0_bgl))));
				{	/* Cfa/vector.scm 129 */
					obj_t BgL_auxz00_5473;
					BgL_objectz00_bglt BgL_tmpz00_5469;

					BgL_auxz00_5473 = ((obj_t) BgL_wide1204z00_4804);
					BgL_tmpz00_5469 =
						((BgL_objectz00_bglt)
						((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_nodez00_4649)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5469, BgL_auxz00_5473);
				}
				((BgL_objectz00_bglt)
					((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_nodez00_4649)));
				{	/* Cfa/vector.scm 129 */
					long BgL_arg1733z00_4805;

					BgL_arg1733z00_4805 =
						BGL_CLASS_NUM(BGl_vsetz12zf2Cinfoze0zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_vsetz12z12_bglt)
								((BgL_vsetz12z12_bglt) BgL_nodez00_4649))),
						BgL_arg1733z00_4805);
				}
				((BgL_vsetz12z12_bglt)
					((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_nodez00_4649)));
			}
			{
				BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_5487;

				{
					obj_t BgL_auxz00_5488;

					{	/* Cfa/vector.scm 130 */
						BgL_objectz00_bglt BgL_tmpz00_5489;

						BgL_tmpz00_5489 =
							((BgL_objectz00_bglt)
							((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_nodez00_4649)));
						BgL_auxz00_5488 = BGL_OBJECT_WIDENING(BgL_tmpz00_5489);
					}
					BgL_auxz00_5487 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_5488);
				}
				((((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_5487))->
						BgL_approxz00) =
					((BgL_approxz00_bglt)
						BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
								BGl_za2unspecza2z00zztype_cachez00))), BUNSPEC);
			}
			{
				bool_t BgL_auxz00_5506;
				BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_5498;

				{	/* Cfa/vector.scm 131 */
					bool_t BgL_test2165z00_5507;

					{	/* Cfa/vector.scm 131 */
						bool_t BgL_test2166z00_5508;

						{	/* Cfa/vector.scm 131 */
							BgL_typez00_bglt BgL_arg1746z00_4806;

							BgL_arg1746z00_4806 =
								(((BgL_vsetz12z12_bglt) COBJECT(
										((BgL_vsetz12z12_bglt) BgL_nodez00_4649)))->BgL_ftypez00);
							BgL_test2166z00_5508 =
								(
								((obj_t) BgL_arg1746z00_4806) == BGl_za2_za2z00zztype_cachez00);
						}
						if (BgL_test2166z00_5508)
							{	/* Cfa/vector.scm 131 */
								BgL_test2165z00_5507 = ((bool_t) 1);
							}
						else
							{	/* Cfa/vector.scm 131 */
								BgL_typez00_bglt BgL_arg1740z00_4807;

								BgL_arg1740z00_4807 =
									(((BgL_vsetz12z12_bglt) COBJECT(
											((BgL_vsetz12z12_bglt) BgL_nodez00_4649)))->BgL_ftypez00);
								BgL_test2165z00_5507 =
									(
									((obj_t) BgL_arg1740z00_4807) ==
									BGl_za2objza2z00zztype_cachez00);
							}
					}
					if (BgL_test2165z00_5507)
						{	/* Cfa/vector.scm 131 */
							BgL_auxz00_5506 = ((bool_t) 0);
						}
					else
						{	/* Cfa/vector.scm 131 */
							BgL_auxz00_5506 = ((bool_t) 1);
						}
				}
				{
					obj_t BgL_auxz00_5499;

					{	/* Cfa/vector.scm 131 */
						BgL_objectz00_bglt BgL_tmpz00_5500;

						BgL_tmpz00_5500 =
							((BgL_objectz00_bglt)
							((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_nodez00_4649)));
						BgL_auxz00_5499 = BGL_OBJECT_WIDENING(BgL_tmpz00_5500);
					}
					BgL_auxz00_5498 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_5499);
				}
				((((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_5498))->
						BgL_tvectorzf3zf3) = ((bool_t) BgL_auxz00_5506), BUNSPEC);
			}
			return
				((obj_t)
				((BgL_vsetz12z12_bglt) ((BgL_vsetz12z12_bglt) BgL_nodez00_4649)));
		}

	}



/* &node-setup!-vref1605 */
	obj_t BGl_z62nodezd2setupz12zd2vref1605z70zzcfa_vectorz00(obj_t
		BgL_envz00_4650, obj_t BgL_nodez00_4651)
	{
		{	/* Cfa/vector.scm 106 */
			{	/* Cfa/vector.scm 108 */
				obj_t BgL_arg1709z00_4809;

				BgL_arg1709z00_4809 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vrefz00_bglt) BgL_nodez00_4651))))->BgL_exprza2za2);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1709z00_4809);
			}
			{	/* Cfa/vector.scm 109 */
				bool_t BgL_tvectorzf3zf3_4810;

				{	/* Cfa/vector.scm 109 */
					bool_t BgL_test2167z00_5525;

					{	/* Cfa/vector.scm 109 */
						bool_t BgL_test2168z00_5526;

						{	/* Cfa/vector.scm 109 */
							BgL_typez00_bglt BgL_arg1722z00_4811;

							BgL_arg1722z00_4811 =
								(((BgL_vrefz00_bglt) COBJECT(
										((BgL_vrefz00_bglt) BgL_nodez00_4651)))->BgL_ftypez00);
							BgL_test2168z00_5526 =
								(
								((obj_t) BgL_arg1722z00_4811) == BGl_za2_za2z00zztype_cachez00);
						}
						if (BgL_test2168z00_5526)
							{	/* Cfa/vector.scm 109 */
								BgL_test2167z00_5525 = ((bool_t) 1);
							}
						else
							{	/* Cfa/vector.scm 109 */
								BgL_typez00_bglt BgL_arg1720z00_4812;

								BgL_arg1720z00_4812 =
									(((BgL_vrefz00_bglt) COBJECT(
											((BgL_vrefz00_bglt) BgL_nodez00_4651)))->BgL_ftypez00);
								BgL_test2167z00_5525 =
									(
									((obj_t) BgL_arg1720z00_4812) ==
									BGl_za2objza2z00zztype_cachez00);
							}
					}
					if (BgL_test2167z00_5525)
						{	/* Cfa/vector.scm 109 */
							BgL_tvectorzf3zf3_4810 = ((bool_t) 0);
						}
					else
						{	/* Cfa/vector.scm 109 */
							BgL_tvectorzf3zf3_4810 = ((bool_t) 1);
						}
				}
				{	/* Cfa/vector.scm 109 */
					BgL_approxz00_bglt BgL_approxz00_4813;

					if (BgL_tvectorzf3zf3_4810)
						{	/* Cfa/vector.scm 111 */
							BgL_approxz00_4813 =
								BGl_makezd2typezd2approxz00zzcfa_approxz00(
								(((BgL_vrefz00_bglt) COBJECT(
											((BgL_vrefz00_bglt) BgL_nodez00_4651)))->BgL_ftypez00));
						}
					else
						{	/* Cfa/vector.scm 111 */
							if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 2L))
								{	/* Cfa/vector.scm 113 */
									BgL_approxz00_4813 =
										BGl_makezd2emptyzd2approxz00zzcfa_approxz00();
								}
							else
								{	/* Cfa/vector.scm 116 */
									BgL_approxz00_bglt BgL_approxz00_4814;

									BgL_approxz00_4814 =
										BGl_makezd2typezd2approxz00zzcfa_approxz00(
										((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
									BGl_approxzd2setzd2topz12z12zzcfa_approxz00
										(BgL_approxz00_4814);
									BgL_approxz00_4813 = BgL_approxz00_4814;
								}
						}
					{	/* Cfa/vector.scm 110 */

						{	/* Cfa/vector.scm 119 */
							BgL_vrefzf2cinfozf2_bglt BgL_wide1198z00_4815;

							BgL_wide1198z00_4815 =
								((BgL_vrefzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
											BgL_vrefzf2cinfozf2_bgl))));
							{	/* Cfa/vector.scm 119 */
								obj_t BgL_auxz00_5551;
								BgL_objectz00_bglt BgL_tmpz00_5547;

								BgL_auxz00_5551 = ((obj_t) BgL_wide1198z00_4815);
								BgL_tmpz00_5547 =
									((BgL_objectz00_bglt)
									((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_nodez00_4651)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5547, BgL_auxz00_5551);
							}
							((BgL_objectz00_bglt)
								((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_nodez00_4651)));
							{	/* Cfa/vector.scm 119 */
								long BgL_arg1710z00_4816;

								BgL_arg1710z00_4816 =
									BGL_CLASS_NUM(BGl_vrefzf2Cinfozf2zzcfa_info3z00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_vrefz00_bglt)
											((BgL_vrefz00_bglt) BgL_nodez00_4651))),
									BgL_arg1710z00_4816);
							}
							((BgL_vrefz00_bglt)
								((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_nodez00_4651)));
						}
						{
							BgL_vrefzf2cinfozf2_bglt BgL_auxz00_5565;

							{
								obj_t BgL_auxz00_5566;

								{	/* Cfa/vector.scm 120 */
									BgL_objectz00_bglt BgL_tmpz00_5567;

									BgL_tmpz00_5567 =
										((BgL_objectz00_bglt)
										((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_nodez00_4651)));
									BgL_auxz00_5566 = BGL_OBJECT_WIDENING(BgL_tmpz00_5567);
								}
								BgL_auxz00_5565 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_5566);
							}
							((((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5565))->
									BgL_approxz00) =
								((BgL_approxz00_bglt) BgL_approxz00_4813), BUNSPEC);
						}
						{
							BgL_vrefzf2cinfozf2_bglt BgL_auxz00_5574;

							{
								obj_t BgL_auxz00_5575;

								{	/* Cfa/vector.scm 121 */
									BgL_objectz00_bglt BgL_tmpz00_5576;

									BgL_tmpz00_5576 =
										((BgL_objectz00_bglt)
										((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_nodez00_4651)));
									BgL_auxz00_5575 = BGL_OBJECT_WIDENING(BgL_tmpz00_5576);
								}
								BgL_auxz00_5574 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_5575);
							}
							((((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5574))->
									BgL_tvectorzf3zf3) =
								((bool_t) BgL_tvectorzf3zf3_4810), BUNSPEC);
						}
						return
							((obj_t)
							((BgL_vrefz00_bglt) ((BgL_vrefz00_bglt) BgL_nodez00_4651)));
					}
				}
			}
		}

	}



/* &node-setup!-vlength1603 */
	obj_t BGl_z62nodezd2setupz12zd2vlength1603z70zzcfa_vectorz00(obj_t
		BgL_envz00_4652, obj_t BgL_nodez00_4653)
	{
		{	/* Cfa/vector.scm 96 */
			{	/* Cfa/vector.scm 98 */
				obj_t BgL_arg1701z00_4818;

				{	/* Cfa/vector.scm 98 */
					obj_t BgL_pairz00_4819;

					BgL_pairz00_4819 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vlengthz00_bglt) BgL_nodez00_4653))))->BgL_exprza2za2);
					BgL_arg1701z00_4818 = CAR(BgL_pairz00_4819);
				}
				BGl_nodezd2setupz12zc0zzcfa_setupz00(
					((BgL_nodez00_bglt) BgL_arg1701z00_4818));
			}
			{	/* Cfa/vector.scm 99 */
				BgL_vlengthzf2cinfozf2_bglt BgL_wide1193z00_4820;

				BgL_wide1193z00_4820 =
					((BgL_vlengthzf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_vlengthzf2cinfozf2_bgl))));
				{	/* Cfa/vector.scm 99 */
					obj_t BgL_auxz00_5597;
					BgL_objectz00_bglt BgL_tmpz00_5593;

					BgL_auxz00_5597 = ((obj_t) BgL_wide1193z00_4820);
					BgL_tmpz00_5593 =
						((BgL_objectz00_bglt)
						((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_nodez00_4653)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5593, BgL_auxz00_5597);
				}
				((BgL_objectz00_bglt)
					((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_nodez00_4653)));
				{	/* Cfa/vector.scm 99 */
					long BgL_arg1703z00_4821;

					BgL_arg1703z00_4821 =
						BGL_CLASS_NUM(BGl_vlengthzf2Cinfozf2zzcfa_info3z00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_vlengthz00_bglt)
								((BgL_vlengthz00_bglt) BgL_nodez00_4653))),
						BgL_arg1703z00_4821);
				}
				((BgL_vlengthz00_bglt)
					((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_nodez00_4653)));
			}
			{
				BgL_approxz00_bglt BgL_auxz00_5619;
				BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_5611;

				{	/* Cfa/vector.scm 100 */
					BgL_typez00_bglt BgL_arg1705z00_4822;

					BgL_arg1705z00_4822 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vlengthz00_bglt) BgL_nodez00_4653))))->BgL_typez00);
					BgL_auxz00_5619 =
						BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_arg1705z00_4822);
				}
				{
					obj_t BgL_auxz00_5612;

					{	/* Cfa/vector.scm 100 */
						BgL_objectz00_bglt BgL_tmpz00_5613;

						BgL_tmpz00_5613 =
							((BgL_objectz00_bglt)
							((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_nodez00_4653)));
						BgL_auxz00_5612 = BGL_OBJECT_WIDENING(BgL_tmpz00_5613);
					}
					BgL_auxz00_5611 = ((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_5612);
				}
				((((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5611))->
						BgL_approxz00) = ((BgL_approxz00_bglt) BgL_auxz00_5619), BUNSPEC);
			}
			{
				BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_5625;

				{
					obj_t BgL_auxz00_5626;

					{	/* Cfa/vector.scm 101 */
						BgL_objectz00_bglt BgL_tmpz00_5627;

						BgL_tmpz00_5627 =
							((BgL_objectz00_bglt)
							((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_nodez00_4653)));
						BgL_auxz00_5626 = BGL_OBJECT_WIDENING(BgL_tmpz00_5627);
					}
					BgL_auxz00_5625 = ((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_5626);
				}
				((((BgL_vlengthzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5625))->
						BgL_tvectorzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
			}
			return
				((obj_t)
				((BgL_vlengthz00_bglt) ((BgL_vlengthz00_bglt) BgL_nodez00_4653)));
		}

	}



/* &node-setup!-valloc1601 */
	obj_t BGl_z62nodezd2setupz12zd2valloc1601z70zzcfa_vectorz00(obj_t
		BgL_envz00_4654, obj_t BgL_nodez00_4655)
	{
		{	/* Cfa/vector.scm 83 */
			{	/* Cfa/vector.scm 85 */
				obj_t BgL_arg1692z00_4824;

				BgL_arg1692z00_4824 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vallocz00_bglt) BgL_nodez00_4655))))->BgL_exprza2za2);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1692z00_4824);
			}
			{	/* Cfa/vector.scm 87 */
				BgL_approxz00_bglt BgL_approxz00_4825;

				{	/* Cfa/vector.scm 87 */
					BgL_typez00_bglt BgL_arg1700z00_4826;

					BgL_arg1700z00_4826 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vallocz00_bglt) BgL_nodez00_4655))))->BgL_typez00);
					BgL_approxz00_4825 =
						BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_arg1700z00_4826);
				}
				{	/* Cfa/vector.scm 87 */
					BgL_vallocz00_bglt BgL_wnodez00_4827;

					{	/* Cfa/vector.scm 88 */
						BgL_valloczf2cinfozf2_bglt BgL_wide1187z00_4828;

						BgL_wide1187z00_4828 =
							((BgL_valloczf2cinfozf2_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_valloczf2cinfozf2_bgl))));
						{	/* Cfa/vector.scm 88 */
							obj_t BgL_auxz00_5650;
							BgL_objectz00_bglt BgL_tmpz00_5646;

							BgL_auxz00_5650 = ((obj_t) BgL_wide1187z00_4828);
							BgL_tmpz00_5646 =
								((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_4655)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5646, BgL_auxz00_5650);
						}
						((BgL_objectz00_bglt)
							((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_4655)));
						{	/* Cfa/vector.scm 88 */
							long BgL_arg1699z00_4829;

							BgL_arg1699z00_4829 =
								BGL_CLASS_NUM(BGl_valloczf2Cinfozf2zzcfa_info3z00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_vallocz00_bglt)
										((BgL_vallocz00_bglt) BgL_nodez00_4655))),
								BgL_arg1699z00_4829);
						}
						((BgL_vallocz00_bglt)
							((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_4655)));
					}
					{
						BgL_valloczf2cinfozf2_bglt BgL_auxz00_5664;

						{
							obj_t BgL_auxz00_5665;

							{	/* Cfa/vector.scm 89 */
								BgL_objectz00_bglt BgL_tmpz00_5666;

								BgL_tmpz00_5666 =
									((BgL_objectz00_bglt)
									((BgL_vallocz00_bglt)
										((BgL_vallocz00_bglt) BgL_nodez00_4655)));
								BgL_auxz00_5665 = BGL_OBJECT_WIDENING(BgL_tmpz00_5666);
							}
							BgL_auxz00_5664 = ((BgL_valloczf2cinfozf2_bglt) BgL_auxz00_5665);
						}
						((((BgL_valloczf2cinfozf2_bglt) COBJECT(BgL_auxz00_5664))->
								BgL_approxz00) =
							((BgL_approxz00_bglt) BgL_approxz00_4825), BUNSPEC);
					}
					BgL_wnodez00_4827 =
						((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_4655));
					{	/* Cfa/vector.scm 88 */

						BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_approxz00_4825);
						return ((obj_t) BgL_wnodez00_4827);
					}
				}
			}
		}

	}



/* &node-setup!-pre-vall1599 */
	obj_t BGl_z62nodezd2setupz12zd2prezd2vall1599za2zzcfa_vectorz00(obj_t
		BgL_envz00_4656, obj_t BgL_nodez00_4657)
	{
		{	/* Cfa/vector.scm 63 */
			BGl_addzd2makezd2vectorz12z12zzcfa_tvectorz00(
				((BgL_nodez00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_4657)));
			{	/* Cfa/vector.scm 66 */
				obj_t BgL_arg1651z00_4831;

				BgL_arg1651z00_4831 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_vallocz00_bglt) BgL_nodez00_4657))))->BgL_exprza2za2);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1651z00_4831);
			}
			{	/* Cfa/vector.scm 67 */
				BgL_variablez00_bglt BgL_ownerz00_4832;

				{
					BgL_prezd2valloczf2cinfoz20_bglt BgL_auxz00_5684;

					{
						obj_t BgL_auxz00_5685;

						{	/* Cfa/vector.scm 67 */
							BgL_objectz00_bglt BgL_tmpz00_5686;

							BgL_tmpz00_5686 =
								((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_4657));
							BgL_auxz00_5685 = BGL_OBJECT_WIDENING(BgL_tmpz00_5686);
						}
						BgL_auxz00_5684 =
							((BgL_prezd2valloczf2cinfoz20_bglt) BgL_auxz00_5685);
					}
					BgL_ownerz00_4832 =
						(((BgL_prezd2valloczf2cinfoz20_bglt) COBJECT(BgL_auxz00_5684))->
						BgL_ownerz00);
				}
				{	/* Cfa/vector.scm 67 */
					BgL_vallocz00_bglt BgL_nodez00_4833;

					{	/* Cfa/vector.scm 68 */
						long BgL_arg1688z00_4834;

						{	/* Cfa/vector.scm 68 */
							obj_t BgL_arg1689z00_4835;

							{	/* Cfa/vector.scm 68 */
								obj_t BgL_arg1691z00_4836;

								{	/* Cfa/vector.scm 68 */
									obj_t BgL_arg1815z00_4837;
									long BgL_arg1816z00_4838;

									BgL_arg1815z00_4837 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Cfa/vector.scm 68 */
										long BgL_arg1817z00_4839;

										BgL_arg1817z00_4839 =
											BGL_OBJECT_CLASS_NUM(
											((BgL_objectz00_bglt)
												((BgL_vallocz00_bglt) BgL_nodez00_4657)));
										BgL_arg1816z00_4838 = (BgL_arg1817z00_4839 - OBJECT_TYPE);
									}
									BgL_arg1691z00_4836 =
										VECTOR_REF(BgL_arg1815z00_4837, BgL_arg1816z00_4838);
								}
								BgL_arg1689z00_4835 =
									BGl_classzd2superzd2zz__objectz00(BgL_arg1691z00_4836);
							}
							{	/* Cfa/vector.scm 68 */
								obj_t BgL_tmpz00_5699;

								BgL_tmpz00_5699 = ((obj_t) BgL_arg1689z00_4835);
								BgL_arg1688z00_4834 = BGL_CLASS_NUM(BgL_tmpz00_5699);
						}}
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_vallocz00_bglt) BgL_nodez00_4657)), BgL_arg1688z00_4834);
					}
					{	/* Cfa/vector.scm 68 */
						BgL_objectz00_bglt BgL_tmpz00_5705;

						BgL_tmpz00_5705 =
							((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_4657));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5705, BFALSE);
					}
					((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_4657));
					BgL_nodez00_4833 = ((BgL_vallocz00_bglt) BgL_nodez00_4657);
					{	/* Cfa/vector.scm 68 */

						{	/* Cfa/vector.scm 69 */
							BgL_vallocz00_bglt BgL_wnodez00_4840;

							{	/* Cfa/vector.scm 69 */
								BgL_valloczf2cinfozb2optimz40_bglt BgL_wide1182z00_4841;

								BgL_wide1182z00_4841 =
									((BgL_valloczf2cinfozb2optimz40_bglt)
									BOBJECT(GC_MALLOC(sizeof(struct
												BgL_valloczf2cinfozb2optimz40_bgl))));
								{	/* Cfa/vector.scm 69 */
									obj_t BgL_auxz00_5717;
									BgL_objectz00_bglt BgL_tmpz00_5713;

									BgL_auxz00_5717 = ((obj_t) BgL_wide1182z00_4841);
									BgL_tmpz00_5713 =
										((BgL_objectz00_bglt)
										((BgL_vallocz00_bglt)
											((BgL_vallocz00_bglt) BgL_nodez00_4833)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5713, BgL_auxz00_5717);
								}
								((BgL_objectz00_bglt)
									((BgL_vallocz00_bglt)
										((BgL_vallocz00_bglt) BgL_nodez00_4833)));
								{	/* Cfa/vector.scm 69 */
									long BgL_arg1663z00_4842;

									BgL_arg1663z00_4842 =
										BGL_CLASS_NUM(BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_vallocz00_bglt)
												((BgL_vallocz00_bglt) BgL_nodez00_4833))),
										BgL_arg1663z00_4842);
								}
								((BgL_vallocz00_bglt)
									((BgL_vallocz00_bglt)
										((BgL_vallocz00_bglt) BgL_nodez00_4833)));
							}
							{
								BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_5731;

								{
									obj_t BgL_auxz00_5732;

									{	/* Cfa/vector.scm 71 */
										BgL_objectz00_bglt BgL_tmpz00_5733;

										BgL_tmpz00_5733 =
											((BgL_objectz00_bglt)
											((BgL_vallocz00_bglt)
												((BgL_vallocz00_bglt) BgL_nodez00_4833)));
										BgL_auxz00_5732 = BGL_OBJECT_WIDENING(BgL_tmpz00_5733);
									}
									BgL_auxz00_5731 =
										((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_5732);
								}
								((((BgL_valloczf2cinfozb2optimz40_bglt)
											COBJECT(BgL_auxz00_5731))->BgL_approxz00) =
									((BgL_approxz00_bglt)
										BGl_makezd2emptyzd2approxz00zzcfa_approxz00()), BUNSPEC);
							}
							{
								BgL_approxz00_bglt BgL_auxz00_5749;
								BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_5741;

								{	/* Cfa/vector.scm 72 */
									bool_t BgL_test2171z00_5750;

									{	/* Cfa/vector.scm 72 */
										BgL_typez00_bglt BgL_arg1681z00_4843;

										BgL_arg1681z00_4843 =
											(((BgL_vallocz00_bglt) COBJECT(
													((BgL_vallocz00_bglt)
														((BgL_vallocz00_bglt) BgL_nodez00_4657))))->
											BgL_ftypez00);
										BgL_test2171z00_5750 =
											(((obj_t) BgL_arg1681z00_4843) ==
											BGl_za2_za2z00zztype_cachez00);
									}
									if (BgL_test2171z00_5750)
										{	/* Cfa/vector.scm 72 */
											BgL_auxz00_5749 =
												BGl_makezd2emptyzd2approxz00zzcfa_approxz00();
										}
									else
										{	/* Cfa/vector.scm 74 */
											BgL_typez00_bglt BgL_arg1678z00_4844;

											BgL_arg1678z00_4844 =
												(((BgL_vallocz00_bglt) COBJECT(
														((BgL_vallocz00_bglt)
															((BgL_vallocz00_bglt) BgL_nodez00_4657))))->
												BgL_ftypez00);
											BgL_auxz00_5749 =
												BGl_makezd2typezd2approxz00zzcfa_approxz00
												(BgL_arg1678z00_4844);
										}
								}
								{
									obj_t BgL_auxz00_5742;

									{	/* Cfa/vector.scm 72 */
										BgL_objectz00_bglt BgL_tmpz00_5743;

										BgL_tmpz00_5743 =
											((BgL_objectz00_bglt)
											((BgL_vallocz00_bglt)
												((BgL_vallocz00_bglt) BgL_nodez00_4833)));
										BgL_auxz00_5742 = BGL_OBJECT_WIDENING(BgL_tmpz00_5743);
									}
									BgL_auxz00_5741 =
										((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_5742);
								}
								((((BgL_valloczf2cinfozb2optimz40_bglt)
											COBJECT(BgL_auxz00_5741))->BgL_valuezd2approxzd2) =
									((BgL_approxz00_bglt) BgL_auxz00_5749), BUNSPEC);
							}
							{
								BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_5762;

								{
									obj_t BgL_auxz00_5763;

									{	/* Cfa/vector.scm 68 */
										BgL_objectz00_bglt BgL_tmpz00_5764;

										BgL_tmpz00_5764 =
											((BgL_objectz00_bglt)
											((BgL_vallocz00_bglt)
												((BgL_vallocz00_bglt) BgL_nodez00_4833)));
										BgL_auxz00_5763 = BGL_OBJECT_WIDENING(BgL_tmpz00_5764);
									}
									BgL_auxz00_5762 =
										((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_5763);
								}
								((((BgL_valloczf2cinfozb2optimz40_bglt)
											COBJECT(BgL_auxz00_5762))->BgL_lostzd2stampzd2) =
									((long) -1L), BUNSPEC);
							}
							{
								BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_5771;

								{
									obj_t BgL_auxz00_5772;

									{	/* Cfa/vector.scm 70 */
										BgL_objectz00_bglt BgL_tmpz00_5773;

										BgL_tmpz00_5773 =
											((BgL_objectz00_bglt)
											((BgL_vallocz00_bglt)
												((BgL_vallocz00_bglt) BgL_nodez00_4833)));
										BgL_auxz00_5772 = BGL_OBJECT_WIDENING(BgL_tmpz00_5773);
									}
									BgL_auxz00_5771 =
										((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_5772);
								}
								((((BgL_valloczf2cinfozb2optimz40_bglt)
											COBJECT(BgL_auxz00_5771))->BgL_ownerz00) =
									((BgL_variablez00_bglt) BgL_ownerz00_4832), BUNSPEC);
							}
							{
								BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_5780;

								{
									obj_t BgL_auxz00_5781;

									{	/* Cfa/vector.scm 68 */
										BgL_objectz00_bglt BgL_tmpz00_5782;

										BgL_tmpz00_5782 =
											((BgL_objectz00_bglt)
											((BgL_vallocz00_bglt)
												((BgL_vallocz00_bglt) BgL_nodez00_4833)));
										BgL_auxz00_5781 = BGL_OBJECT_WIDENING(BgL_tmpz00_5782);
									}
									BgL_auxz00_5780 =
										((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_5781);
								}
								((((BgL_valloczf2cinfozb2optimz40_bglt)
											COBJECT(BgL_auxz00_5780))->BgL_stackablezf3zf3) =
									((bool_t) ((bool_t) 1)), BUNSPEC);
							}
							{
								BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_5789;

								{
									obj_t BgL_auxz00_5790;

									{	/* Cfa/vector.scm 68 */
										BgL_objectz00_bglt BgL_tmpz00_5791;

										BgL_tmpz00_5791 =
											((BgL_objectz00_bglt)
											((BgL_vallocz00_bglt)
												((BgL_vallocz00_bglt) BgL_nodez00_4833)));
										BgL_auxz00_5790 = BGL_OBJECT_WIDENING(BgL_tmpz00_5791);
									}
									BgL_auxz00_5789 =
										((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_5790);
								}
								((((BgL_valloczf2cinfozb2optimz40_bglt)
											COBJECT(BgL_auxz00_5789))->BgL_stackzd2stampzd2) =
									((obj_t) BNIL), BUNSPEC);
							}
							{
								BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_5798;

								{
									obj_t BgL_auxz00_5799;

									{	/* Cfa/vector.scm 68 */
										BgL_objectz00_bglt BgL_tmpz00_5800;

										BgL_tmpz00_5800 =
											((BgL_objectz00_bglt)
											((BgL_vallocz00_bglt)
												((BgL_vallocz00_bglt) BgL_nodez00_4833)));
										BgL_auxz00_5799 = BGL_OBJECT_WIDENING(BgL_tmpz00_5800);
									}
									BgL_auxz00_5798 =
										((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_5799);
								}
								((((BgL_valloczf2cinfozb2optimz40_bglt)
											COBJECT(BgL_auxz00_5798))->BgL_seenzf3zf3) =
									((bool_t) ((bool_t) 0)), BUNSPEC);
							}
							BgL_wnodez00_4840 =
								((BgL_vallocz00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_4833));
							{	/* Cfa/vector.scm 78 */
								BgL_approxz00_bglt BgL_arg1654z00_4845;

								{	/* Cfa/vector.scm 78 */
									BgL_typez00_bglt BgL_arg1661z00_4846;

									BgL_arg1661z00_4846 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_vallocz00_bglt) BgL_nodez00_4657))))->
										BgL_typez00);
									BgL_arg1654z00_4845 =
										BGl_makezd2typezd2alloczd2approxzd2zzcfa_approxz00
										(BgL_arg1661z00_4846,
										((BgL_nodez00_bglt) BgL_nodez00_4833));
								}
								{
									BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_5814;

									{
										obj_t BgL_auxz00_5815;

										{	/* Cfa/vector.scm 76 */
											BgL_objectz00_bglt BgL_tmpz00_5816;

											BgL_tmpz00_5816 =
												((BgL_objectz00_bglt) BgL_wnodez00_4840);
											BgL_auxz00_5815 = BGL_OBJECT_WIDENING(BgL_tmpz00_5816);
										}
										BgL_auxz00_5814 =
											((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_5815);
									}
									return
										((((BgL_valloczf2cinfozb2optimz40_bglt)
												COBJECT(BgL_auxz00_5814))->BgL_approxz00) =
										((BgL_approxz00_bglt) BgL_arg1654z00_4845), BUNSPEC);
								}
							}
						}
					}
				}
			}
		}

	}



/* &node-setup!-pre-make1597 */
	obj_t BGl_z62nodezd2setupz12zd2prezd2make1597za2zzcfa_vectorz00(obj_t
		BgL_envz00_4658, obj_t BgL_nodez00_4659)
	{
		{	/* Cfa/vector.scm 46 */
			BGl_addzd2makezd2vectorz12z12zzcfa_tvectorz00(
				((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4659)));
			{	/* Cfa/vector.scm 49 */
				obj_t BgL_arg1627z00_4848;

				BgL_arg1627z00_4848 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_4659))))->BgL_argsz00);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1627z00_4848);
			}
			{	/* Cfa/vector.scm 50 */
				BgL_variablez00_bglt BgL_ownerz00_4849;

				{
					BgL_prezd2makezd2vectorzd2appzd2_bglt BgL_auxz00_5828;

					{
						obj_t BgL_auxz00_5829;

						{	/* Cfa/vector.scm 50 */
							BgL_objectz00_bglt BgL_tmpz00_5830;

							BgL_tmpz00_5830 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4659));
							BgL_auxz00_5829 = BGL_OBJECT_WIDENING(BgL_tmpz00_5830);
						}
						BgL_auxz00_5828 =
							((BgL_prezd2makezd2vectorzd2appzd2_bglt) BgL_auxz00_5829);
					}
					BgL_ownerz00_4849 =
						(((BgL_prezd2makezd2vectorzd2appzd2_bglt)
							COBJECT(BgL_auxz00_5828))->BgL_ownerz00);
				}
				{	/* Cfa/vector.scm 50 */
					BgL_appz00_bglt BgL_nodez00_4850;

					{	/* Cfa/vector.scm 51 */
						long BgL_arg1642z00_4851;

						{	/* Cfa/vector.scm 51 */
							obj_t BgL_arg1646z00_4852;

							{	/* Cfa/vector.scm 51 */
								obj_t BgL_arg1650z00_4853;

								{	/* Cfa/vector.scm 51 */
									obj_t BgL_arg1815z00_4854;
									long BgL_arg1816z00_4855;

									BgL_arg1815z00_4854 = (BGl_za2classesza2z00zz__objectz00);
									{	/* Cfa/vector.scm 51 */
										long BgL_arg1817z00_4856;

										BgL_arg1817z00_4856 =
											BGL_OBJECT_CLASS_NUM(
											((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_4659)));
										BgL_arg1816z00_4855 = (BgL_arg1817z00_4856 - OBJECT_TYPE);
									}
									BgL_arg1650z00_4853 =
										VECTOR_REF(BgL_arg1815z00_4854, BgL_arg1816z00_4855);
								}
								BgL_arg1646z00_4852 =
									BGl_classzd2superzd2zz__objectz00(BgL_arg1650z00_4853);
							}
							{	/* Cfa/vector.scm 51 */
								obj_t BgL_tmpz00_5843;

								BgL_tmpz00_5843 = ((obj_t) BgL_arg1646z00_4852);
								BgL_arg1642z00_4851 = BGL_CLASS_NUM(BgL_tmpz00_5843);
						}}
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_4659)), BgL_arg1642z00_4851);
					}
					{	/* Cfa/vector.scm 51 */
						BgL_objectz00_bglt BgL_tmpz00_5849;

						BgL_tmpz00_5849 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4659));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5849, BFALSE);
					}
					((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4659));
					BgL_nodez00_4850 = ((BgL_appz00_bglt) BgL_nodez00_4659);
					{	/* Cfa/vector.scm 51 */

						{	/* Cfa/vector.scm 52 */
							BgL_appz00_bglt BgL_wnodez00_4857;

							{	/* Cfa/vector.scm 52 */
								BgL_makezd2vectorzd2appz00_bglt BgL_wide1176z00_4858;

								BgL_wide1176z00_4858 =
									((BgL_makezd2vectorzd2appz00_bglt)
									BOBJECT(GC_MALLOC(sizeof(struct
												BgL_makezd2vectorzd2appz00_bgl))));
								{	/* Cfa/vector.scm 52 */
									obj_t BgL_auxz00_5861;
									BgL_objectz00_bglt BgL_tmpz00_5857;

									BgL_auxz00_5861 = ((obj_t) BgL_wide1176z00_4858);
									BgL_tmpz00_5857 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4850)));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5857, BgL_auxz00_5861);
								}
								((BgL_objectz00_bglt)
									((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4850)));
								{	/* Cfa/vector.scm 52 */
									long BgL_arg1630z00_4859;

									BgL_arg1630z00_4859 =
										BGL_CLASS_NUM(BGl_makezd2vectorzd2appz00zzcfa_info2z00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_appz00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_4850))),
										BgL_arg1630z00_4859);
								}
								((BgL_appz00_bglt)
									((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4850)));
							}
							{
								BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5875;

								{
									obj_t BgL_auxz00_5876;

									{	/* Cfa/vector.scm 54 */
										BgL_objectz00_bglt BgL_tmpz00_5877;

										BgL_tmpz00_5877 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4850)));
										BgL_auxz00_5876 = BGL_OBJECT_WIDENING(BgL_tmpz00_5877);
									}
									BgL_auxz00_5875 =
										((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5876);
								}
								((((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_5875))->
										BgL_approxz00) =
									((BgL_approxz00_bglt)
										BGl_makezd2emptyzd2approxz00zzcfa_approxz00()), BUNSPEC);
							}
							{
								BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5885;

								{
									obj_t BgL_auxz00_5886;

									{	/* Cfa/vector.scm 55 */
										BgL_objectz00_bglt BgL_tmpz00_5887;

										BgL_tmpz00_5887 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4850)));
										BgL_auxz00_5886 = BGL_OBJECT_WIDENING(BgL_tmpz00_5887);
									}
									BgL_auxz00_5885 =
										((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5886);
								}
								((((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_5885))->
										BgL_valuezd2approxzd2) =
									((BgL_approxz00_bglt)
										BGl_makezd2emptyzd2approxz00zzcfa_approxz00()), BUNSPEC);
							}
							{
								BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5895;

								{
									obj_t BgL_auxz00_5896;

									{	/* Cfa/vector.scm 51 */
										BgL_objectz00_bglt BgL_tmpz00_5897;

										BgL_tmpz00_5897 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4850)));
										BgL_auxz00_5896 = BGL_OBJECT_WIDENING(BgL_tmpz00_5897);
									}
									BgL_auxz00_5895 =
										((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5896);
								}
								((((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_5895))->
										BgL_lostzd2stampzd2) = ((long) -1L), BUNSPEC);
							}
							{
								BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5904;

								{
									obj_t BgL_auxz00_5905;

									{	/* Cfa/vector.scm 53 */
										BgL_objectz00_bglt BgL_tmpz00_5906;

										BgL_tmpz00_5906 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4850)));
										BgL_auxz00_5905 = BGL_OBJECT_WIDENING(BgL_tmpz00_5906);
									}
									BgL_auxz00_5904 =
										((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5905);
								}
								((((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_5904))->
										BgL_ownerz00) =
									((BgL_variablez00_bglt) BgL_ownerz00_4849), BUNSPEC);
							}
							{
								BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5913;

								{
									obj_t BgL_auxz00_5914;

									{	/* Cfa/vector.scm 51 */
										BgL_objectz00_bglt BgL_tmpz00_5915;

										BgL_tmpz00_5915 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4850)));
										BgL_auxz00_5914 = BGL_OBJECT_WIDENING(BgL_tmpz00_5915);
									}
									BgL_auxz00_5913 =
										((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5914);
								}
								((((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_5913))->
										BgL_stackzd2stampzd2) = ((obj_t) BNIL), BUNSPEC);
							}
							{
								BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5922;

								{
									obj_t BgL_auxz00_5923;

									{	/* Cfa/vector.scm 51 */
										BgL_objectz00_bglt BgL_tmpz00_5924;

										BgL_tmpz00_5924 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4850)));
										BgL_auxz00_5923 = BGL_OBJECT_WIDENING(BgL_tmpz00_5924);
									}
									BgL_auxz00_5922 =
										((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5923);
								}
								((((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_5922))->
										BgL_seenzf3zf3) = ((bool_t) ((bool_t) 0)), BUNSPEC);
							}
							{
								BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5931;

								{
									obj_t BgL_auxz00_5932;

									{	/* Cfa/vector.scm 51 */
										BgL_objectz00_bglt BgL_tmpz00_5933;

										BgL_tmpz00_5933 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4850)));
										BgL_auxz00_5932 = BGL_OBJECT_WIDENING(BgL_tmpz00_5933);
									}
									BgL_auxz00_5931 =
										((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5932);
								}
								((((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_5931))->
										BgL_tvectorzf3zf3) = ((bool_t) ((bool_t) 1)), BUNSPEC);
							}
							BgL_wnodez00_4857 =
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4850));
							{	/* Cfa/vector.scm 58 */
								BgL_approxz00_bglt BgL_arg1629z00_4860;

								BgL_arg1629z00_4860 =
									BGl_makezd2typezd2alloczd2approxzd2zzcfa_approxz00(
									((BgL_typez00_bglt) BGl_za2vectorza2z00zztype_cachez00),
									((BgL_nodez00_bglt) BgL_nodez00_4850));
								{
									BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_5945;

									{
										obj_t BgL_auxz00_5946;

										{	/* Cfa/vector.scm 57 */
											BgL_objectz00_bglt BgL_tmpz00_5947;

											BgL_tmpz00_5947 =
												((BgL_objectz00_bglt) BgL_wnodez00_4857);
											BgL_auxz00_5946 = BGL_OBJECT_WIDENING(BgL_tmpz00_5947);
										}
										BgL_auxz00_5945 =
											((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_5946);
									}
									return
										((((BgL_makezd2vectorzd2appz00_bglt)
												COBJECT(BgL_auxz00_5945))->BgL_approxz00) =
										((BgL_approxz00_bglt) BgL_arg1629z00_4860), BUNSPEC);
								}
							}
						}
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_vectorz00(void)
	{
		{	/* Cfa/vector.scm 17 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			BGl_modulezd2initializa7ationz75zzcfa_info3z00(0L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			BGl_modulezd2initializa7ationz75zzcfa_loosez00(471177480L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			BGl_modulezd2initializa7ationz75zzcfa_iteratez00(131412063L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			BGl_modulezd2initializa7ationz75zzcfa_cfaz00(400853817L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			BGl_modulezd2initializa7ationz75zzcfa_setupz00(168272122L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(324810626L,
				BSTRING_TO_STRING(BGl_string2106z00zzcfa_vectorz00));
		}

	}

#ifdef __cplusplus
}
#endif
