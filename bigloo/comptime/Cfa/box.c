/*===========================================================================*/
/*   (Cfa/box.scm)                                                           */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/box.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_BOX_TYPE_DEFINITIONS
#define BGL_CFA_BOX_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_prezd2makezd2boxz00_bgl
	{
	}                          *BgL_prezd2makezd2boxz00_bglt;

	typedef struct BgL_makezd2boxzf2cinfoz20_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                            *BgL_makezd2boxzf2cinfoz20_bglt;

	typedef struct BgL_makezd2boxzf2ozd2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		struct BgL_approxz00_bgl *BgL_valuezd2approxzd2;
	}                                *BgL_makezd2boxzf2ozd2cinfozf2_bglt;

	typedef struct BgL_boxzd2setz12zf2cinfoz32_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                              *BgL_boxzd2setz12zf2cinfoz32_bglt;

	typedef struct BgL_boxzd2refzf2cinfoz20_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                           *BgL_boxzd2refzf2cinfoz20_bglt;

	typedef struct BgL_boxzd2setz12zf2ozd2cinfoze0_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                                  *BgL_boxzd2setz12zf2ozd2cinfoze0_bglt;

	typedef struct BgL_boxzd2refzf2ozd2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                               *BgL_boxzd2refzf2ozd2cinfozf2_bglt;


#endif													// BGL_CFA_BOX_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_objectzd2initzd2zzcfa_boxz00(void);
	extern obj_t BGl_boxzd2setz12zf2Cinfoz32zzcfa_infoz00;
	extern obj_t BGl_prezd2makezd2boxz00zzcfa_infoz00;
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	extern obj_t BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00(obj_t,
		BgL_approxz00_bglt);
	extern obj_t BGl_boxzd2refzf2Ozd2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_makezd2boxzf2Cinfoz20zzcfa_infoz00;
	static obj_t BGl_methodzd2initzd2zzcfa_boxz00(void);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern BgL_approxz00_bglt
		BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_approxz00_bglt);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_boxz00(void);
	extern BgL_approxz00_bglt BGl_makezd2emptyzd2approxz00zzcfa_approxz00(void);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_approxz00_bglt);
	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_boxz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2boxzd2setz12zf2Ozd2Cinf1534z42zzcfa_boxz00(obj_t, obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2boxzd2ref1528za2zzcfa_boxz00(obj_t,
		obj_t);
	static obj_t BGl_toplevelzd2initzd2zzcfa_boxz00(void);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcfa_boxz00(void);
	static obj_t BGl_z62nodezd2setupz12zd2boxzd2setz121526zb0zzcfa_boxz00(obj_t,
		obj_t);
	static obj_t BGl_z62nodezd2setupz12zd2makezd2box1522za2zzcfa_boxz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	extern BgL_approxz00_bglt BGl_loosez12z12zzcfa_loosez00(BgL_approxz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2boxzd2refzf2Ozd2Cinfo1532z50zzcfa_boxz00(obj_t, obj_t);
	extern obj_t BGl_nodezd2setupz12zc0zzcfa_setupz00(BgL_nodez00_bglt);
	extern BgL_approxz00_bglt
		BGl_makezd2typezd2alloczd2approxzd2zzcfa_approxz00(BgL_typez00_bglt,
		BgL_nodez00_bglt);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_boxz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_closurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_iteratez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_cfaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_setupz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_loosez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	static obj_t BGl_z62zc3z04anonymousza31607ze3ze5zzcfa_boxz00(obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zf2Ozd2Cinfoze0zzcfa_infoz00;
	static obj_t BGl_z62nodezd2setupz12zd2prezd2make1524za2zzcfa_boxz00(obj_t,
		obj_t);
	static obj_t BGl_cnstzd2initzd2zzcfa_boxz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_boxz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_boxz00(void);
	static obj_t BGl_z62zc3z04anonymousza31626ze3ze5zzcfa_boxz00(obj_t, obj_t);
	extern BgL_approxz00_bglt
		BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_typez00_bglt);
	static obj_t BGl_z62loosezd2allocz12zd2makezd2bo1536za2zzcfa_boxz00(obj_t,
		obj_t);
	extern obj_t BGl_makezd2boxzf2Ozd2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2makezd2boxzf2Ozd2Cinf1530z50zzcfa_boxz00(obj_t, obj_t);
	extern BgL_approxz00_bglt BGl_cfaz12z12zzcfa_cfaz00(BgL_nodez00_bglt);
	extern obj_t BGl_boxzd2refzf2Cinfoz20zzcfa_infoz00;
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t __cnst[1];


	extern obj_t BGl_cfaz12zd2envzc0zzcfa_cfaz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1963z00zzcfa_boxz00,
		BgL_bgl_za762nodeza7d2setupza71979za7,
		BGl_z62nodezd2setupz12zd2makezd2box1522za2zzcfa_boxz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1965z00zzcfa_boxz00,
		BgL_bgl_za762nodeza7d2setupza71980za7,
		BGl_z62nodezd2setupz12zd2prezd2make1524za2zzcfa_boxz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1966z00zzcfa_boxz00,
		BgL_bgl_za762nodeza7d2setupza71981za7,
		BGl_z62nodezd2setupz12zd2boxzd2setz121526zb0zzcfa_boxz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1967z00zzcfa_boxz00,
		BgL_bgl_za762nodeza7d2setupza71982za7,
		BGl_z62nodezd2setupz12zd2boxzd2ref1528za2zzcfa_boxz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1968z00zzcfa_boxz00,
		BgL_bgl_za762cfaza712za7d2make1983za7,
		BGl_z62cfaz12zd2makezd2boxzf2Ozd2Cinf1530z50zzcfa_boxz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1970z00zzcfa_boxz00,
		BgL_bgl_za762cfaza712za7d2boxza71984z00,
		BGl_z62cfaz12zd2boxzd2refzf2Ozd2Cinfo1532z50zzcfa_boxz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1971z00zzcfa_boxz00,
		BgL_bgl_za762cfaza712za7d2boxza71985z00,
		BGl_z62cfaz12zd2boxzd2setz12zf2Ozd2Cinf1534z42zzcfa_boxz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1972z00zzcfa_boxz00,
		BgL_bgl_za762looseza7d2alloc1986z00,
		BGl_z62loosezd2allocz12zd2makezd2bo1536za2zzcfa_boxz00, 0L, BUNSPEC, 1);
	extern obj_t BGl_nodezd2setupz12zd2envz12zzcfa_setupz00;
	   
		 
		DEFINE_STRING(BGl_string1964z00zzcfa_boxz00,
		BgL_bgl_string1964za700za7za7c1987za7, "node-setup!", 11);
	      DEFINE_STRING(BGl_string1969z00zzcfa_boxz00,
		BgL_bgl_string1969za700za7za7c1988za7, "cfa!::approx", 12);
	      DEFINE_STRING(BGl_string1973z00zzcfa_boxz00,
		BgL_bgl_string1973za700za7za7c1989za7, "loose-alloc!", 12);
	      DEFINE_STRING(BGl_string1974z00zzcfa_boxz00,
		BgL_bgl_string1974za700za7za7c1990za7, "box-ref", 7);
	      DEFINE_STRING(BGl_string1975z00zzcfa_boxz00,
		BgL_bgl_string1975za700za7za7c1991za7,
		"Illegal mixed of optimized and unoptimize `make-box'", 52);
	      DEFINE_STRING(BGl_string1976z00zzcfa_boxz00,
		BgL_bgl_string1976za700za7za7c1992za7, "cfa_box", 7);
	      DEFINE_STRING(BGl_string1977z00zzcfa_boxz00,
		BgL_bgl_string1977za700za7za7c1993za7, "all ", 4);
	extern obj_t BGl_loosezd2allocz12zd2envz12zzcfa_loosez00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_boxz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_boxz00(long
		BgL_checksumz00_3993, char *BgL_fromz00_3994)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_boxz00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_boxz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_boxz00();
					BGl_libraryzd2moduleszd2initz00zzcfa_boxz00();
					BGl_cnstzd2initzd2zzcfa_boxz00();
					BGl_importedzd2moduleszd2initz00zzcfa_boxz00();
					BGl_methodzd2initzd2zzcfa_boxz00();
					return BGl_toplevelzd2initzd2zzcfa_boxz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_boxz00(void)
	{
		{	/* Cfa/box.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_box");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_box");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_box");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_box");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "cfa_box");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_box");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_box");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_boxz00(void)
	{
		{	/* Cfa/box.scm 15 */
			{	/* Cfa/box.scm 15 */
				obj_t BgL_cportz00_3872;

				{	/* Cfa/box.scm 15 */
					obj_t BgL_stringz00_3879;

					BgL_stringz00_3879 = BGl_string1977z00zzcfa_boxz00;
					{	/* Cfa/box.scm 15 */
						obj_t BgL_startz00_3880;

						BgL_startz00_3880 = BINT(0L);
						{	/* Cfa/box.scm 15 */
							obj_t BgL_endz00_3881;

							BgL_endz00_3881 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3879)));
							{	/* Cfa/box.scm 15 */

								BgL_cportz00_3872 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3879, BgL_startz00_3880, BgL_endz00_3881);
				}}}}
				{
					long BgL_iz00_3873;

					BgL_iz00_3873 = 0L;
				BgL_loopz00_3874:
					if ((BgL_iz00_3873 == -1L))
						{	/* Cfa/box.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/box.scm 15 */
							{	/* Cfa/box.scm 15 */
								obj_t BgL_arg1978z00_3875;

								{	/* Cfa/box.scm 15 */

									{	/* Cfa/box.scm 15 */
										obj_t BgL_locationz00_3877;

										BgL_locationz00_3877 = BBOOL(((bool_t) 0));
										{	/* Cfa/box.scm 15 */

											BgL_arg1978z00_3875 =
												BGl_readz00zz__readerz00(BgL_cportz00_3872,
												BgL_locationz00_3877);
										}
									}
								}
								{	/* Cfa/box.scm 15 */
									int BgL_tmpz00_4020;

									BgL_tmpz00_4020 = (int) (BgL_iz00_3873);
									CNST_TABLE_SET(BgL_tmpz00_4020, BgL_arg1978z00_3875);
							}}
							{	/* Cfa/box.scm 15 */
								int BgL_auxz00_3878;

								BgL_auxz00_3878 = (int) ((BgL_iz00_3873 - 1L));
								{
									long BgL_iz00_4025;

									BgL_iz00_4025 = (long) (BgL_auxz00_3878);
									BgL_iz00_3873 = BgL_iz00_4025;
									goto BgL_loopz00_3874;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_boxz00(void)
	{
		{	/* Cfa/box.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_boxz00(void)
	{
		{	/* Cfa/box.scm 15 */
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_boxz00(void)
	{
		{	/* Cfa/box.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_boxz00(void)
	{
		{	/* Cfa/box.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_boxz00(void)
	{
		{	/* Cfa/box.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1963z00zzcfa_boxz00,
				BGl_string1964z00zzcfa_boxz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_prezd2makezd2boxz00zzcfa_infoz00, BGl_proc1965z00zzcfa_boxz00,
				BGl_string1964z00zzcfa_boxz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc1966z00zzcfa_boxz00,
				BGl_string1964z00zzcfa_boxz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc1967z00zzcfa_boxz00,
				BGl_string1964z00zzcfa_boxz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_makezd2boxzf2Ozd2Cinfozf2zzcfa_infoz00, BGl_proc1968z00zzcfa_boxz00,
				BGl_string1969z00zzcfa_boxz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_boxzd2refzf2Ozd2Cinfozf2zzcfa_infoz00, BGl_proc1970z00zzcfa_boxz00,
				BGl_string1969z00zzcfa_boxz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_boxzd2setz12zf2Ozd2Cinfoze0zzcfa_infoz00,
				BGl_proc1971z00zzcfa_boxz00, BGl_string1969z00zzcfa_boxz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_loosezd2allocz12zd2envz12zzcfa_loosez00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc1972z00zzcfa_boxz00,
				BGl_string1973z00zzcfa_boxz00);
		}

	}



/* &loose-alloc!-make-bo1536 */
	obj_t BGl_z62loosezd2allocz12zd2makezd2bo1536za2zzcfa_boxz00(obj_t
		BgL_envz00_3842, obj_t BgL_allocz00_3843)
	{
		{	/* Cfa/box.scm 145 */
			return BUNSPEC;
		}

	}



/* &cfa!-box-set!/O-Cinf1534 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2boxzd2setz12zf2Ozd2Cinf1534z42zzcfa_boxz00(obj_t
		BgL_envz00_3844, obj_t BgL_nodez00_3845)
	{
		{	/* Cfa/box.scm 117 */
			{	/* Cfa/box.scm 119 */
				BgL_approxz00_bglt BgL_boxzd2approxzd2_3885;
				BgL_approxz00_bglt BgL_valzd2approxzd2_3886;

				{	/* Cfa/box.scm 119 */
					BgL_varz00_bglt BgL_arg1646z00_3887;

					BgL_arg1646z00_3887 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt)
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3845))))->BgL_varz00);
					BgL_boxzd2approxzd2_3885 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1646z00_3887));
				}
				{	/* Cfa/box.scm 120 */
					BgL_nodez00_bglt BgL_arg1650z00_3888;

					BgL_arg1650z00_3888 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt)
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3845))))->
						BgL_valuez00);
					BgL_valzd2approxzd2_3886 =
						BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1650z00_3888);
				}
				if (
					(((BgL_approxz00_bglt) COBJECT(BgL_boxzd2approxzd2_3885))->
						BgL_topzf3zf3))
					{	/* Cfa/box.scm 122 */
						((obj_t)
							BGl_loosez12z12zzcfa_loosez00(BgL_valzd2approxzd2_3886,
								CNST_TABLE_REF(0)));
					}
				else
					{	/* Cfa/box.scm 128 */
						obj_t BgL_zc3z04anonymousza31626ze3z87_3889;

						BgL_zc3z04anonymousza31626ze3z87_3889 =
							MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31626ze3ze5zzcfa_boxz00,
							(int) (1L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31626ze3z87_3889,
							(int) (0L),
							((obj_t) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3845)));
						PROCEDURE_SET(BgL_zc3z04anonymousza31626ze3z87_3889,
							(int) (1L), ((obj_t) BgL_valzd2approxzd2_3886));
						BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
							(BgL_zc3z04anonymousza31626ze3z87_3889, BgL_boxzd2approxzd2_3885);
			}}
			{
				BgL_boxzd2setz12zf2ozd2cinfoze0_bglt BgL_auxz00_4061;

				{
					obj_t BgL_auxz00_4062;

					{	/* Cfa/box.scm 137 */
						BgL_objectz00_bglt BgL_tmpz00_4063;

						BgL_tmpz00_4063 =
							((BgL_objectz00_bglt)
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3845));
						BgL_auxz00_4062 = BGL_OBJECT_WIDENING(BgL_tmpz00_4063);
					}
					BgL_auxz00_4061 =
						((BgL_boxzd2setz12zf2ozd2cinfoze0_bglt) BgL_auxz00_4062);
				}
				return
					(((BgL_boxzd2setz12zf2ozd2cinfoze0_bglt) COBJECT(BgL_auxz00_4061))->
					BgL_approxz00);
			}
		}

	}



/* &<@anonymous:1626> */
	obj_t BGl_z62zc3z04anonymousza31626ze3ze5zzcfa_boxz00(obj_t BgL_envz00_3846,
		obj_t BgL_boxz00_3849)
	{
		{	/* Cfa/box.scm 126 */
			{	/* Cfa/box.scm 128 */
				BgL_boxzd2setz12zc0_bglt BgL_nodez00_3847;
				BgL_approxz00_bglt BgL_valzd2approxzd2_3848;

				BgL_nodez00_3847 =
					((BgL_boxzd2setz12zc0_bglt)
					PROCEDURE_REF(BgL_envz00_3846, (int) (0L)));
				BgL_valzd2approxzd2_3848 =
					((BgL_approxz00_bglt) PROCEDURE_REF(BgL_envz00_3846, (int) (1L)));
				{	/* Cfa/box.scm 128 */
					bool_t BgL_test1997z00_4075;

					{	/* Cfa/box.scm 128 */
						obj_t BgL_classz00_3890;

						BgL_classz00_3890 = BGl_makezd2boxzf2Ozd2Cinfozf2zzcfa_infoz00;
						if (BGL_OBJECTP(BgL_boxz00_3849))
							{	/* Cfa/box.scm 128 */
								BgL_objectz00_bglt BgL_arg1807z00_3891;

								BgL_arg1807z00_3891 = (BgL_objectz00_bglt) (BgL_boxz00_3849);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/box.scm 128 */
										long BgL_idxz00_3892;

										BgL_idxz00_3892 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3891);
										BgL_test1997z00_4075 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3892 + 4L)) == BgL_classz00_3890);
									}
								else
									{	/* Cfa/box.scm 128 */
										bool_t BgL_res1946z00_3895;

										{	/* Cfa/box.scm 128 */
											obj_t BgL_oclassz00_3896;

											{	/* Cfa/box.scm 128 */
												obj_t BgL_arg1815z00_3897;
												long BgL_arg1816z00_3898;

												BgL_arg1815z00_3897 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/box.scm 128 */
													long BgL_arg1817z00_3899;

													BgL_arg1817z00_3899 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3891);
													BgL_arg1816z00_3898 =
														(BgL_arg1817z00_3899 - OBJECT_TYPE);
												}
												BgL_oclassz00_3896 =
													VECTOR_REF(BgL_arg1815z00_3897, BgL_arg1816z00_3898);
											}
											{	/* Cfa/box.scm 128 */
												bool_t BgL__ortest_1115z00_3900;

												BgL__ortest_1115z00_3900 =
													(BgL_classz00_3890 == BgL_oclassz00_3896);
												if (BgL__ortest_1115z00_3900)
													{	/* Cfa/box.scm 128 */
														BgL_res1946z00_3895 = BgL__ortest_1115z00_3900;
													}
												else
													{	/* Cfa/box.scm 128 */
														long BgL_odepthz00_3901;

														{	/* Cfa/box.scm 128 */
															obj_t BgL_arg1804z00_3902;

															BgL_arg1804z00_3902 = (BgL_oclassz00_3896);
															BgL_odepthz00_3901 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3902);
														}
														if ((4L < BgL_odepthz00_3901))
															{	/* Cfa/box.scm 128 */
																obj_t BgL_arg1802z00_3903;

																{	/* Cfa/box.scm 128 */
																	obj_t BgL_arg1803z00_3904;

																	BgL_arg1803z00_3904 = (BgL_oclassz00_3896);
																	BgL_arg1802z00_3903 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3904,
																		4L);
																}
																BgL_res1946z00_3895 =
																	(BgL_arg1802z00_3903 == BgL_classz00_3890);
															}
														else
															{	/* Cfa/box.scm 128 */
																BgL_res1946z00_3895 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1997z00_4075 = BgL_res1946z00_3895;
									}
							}
						else
							{	/* Cfa/box.scm 128 */
								BgL_test1997z00_4075 = ((bool_t) 0);
							}
					}
					if (BgL_test1997z00_4075)
						{	/* Cfa/box.scm 130 */
							BgL_approxz00_bglt BgL_arg1629z00_3905;

							{
								BgL_makezd2boxzf2ozd2cinfozf2_bglt BgL_auxz00_4098;

								{
									obj_t BgL_auxz00_4099;

									{	/* Cfa/box.scm 130 */
										BgL_objectz00_bglt BgL_tmpz00_4100;

										BgL_tmpz00_4100 =
											((BgL_objectz00_bglt)
											((BgL_makezd2boxzd2_bglt) BgL_boxz00_3849));
										BgL_auxz00_4099 = BGL_OBJECT_WIDENING(BgL_tmpz00_4100);
									}
									BgL_auxz00_4098 =
										((BgL_makezd2boxzf2ozd2cinfozf2_bglt) BgL_auxz00_4099);
								}
								BgL_arg1629z00_3905 =
									(((BgL_makezd2boxzf2ozd2cinfozf2_bglt)
										COBJECT(BgL_auxz00_4098))->BgL_valuezd2approxzd2);
							}
							return
								((obj_t)
								BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1629z00_3905,
									BgL_valzd2approxzd2_3848));
						}
					else
						{	/* Cfa/box.scm 131 */
							bool_t BgL_test2002z00_4108;

							{	/* Cfa/box.scm 131 */
								obj_t BgL_classz00_3906;

								BgL_classz00_3906 = BGl_makezd2boxzf2Cinfoz20zzcfa_infoz00;
								if (BGL_OBJECTP(BgL_boxz00_3849))
									{	/* Cfa/box.scm 131 */
										BgL_objectz00_bglt BgL_arg1807z00_3907;

										BgL_arg1807z00_3907 =
											(BgL_objectz00_bglt) (BgL_boxz00_3849);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/box.scm 131 */
												long BgL_idxz00_3908;

												BgL_idxz00_3908 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3907);
												BgL_test2002z00_4108 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3908 + 4L)) == BgL_classz00_3906);
											}
										else
											{	/* Cfa/box.scm 131 */
												bool_t BgL_res1947z00_3911;

												{	/* Cfa/box.scm 131 */
													obj_t BgL_oclassz00_3912;

													{	/* Cfa/box.scm 131 */
														obj_t BgL_arg1815z00_3913;
														long BgL_arg1816z00_3914;

														BgL_arg1815z00_3913 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/box.scm 131 */
															long BgL_arg1817z00_3915;

															BgL_arg1817z00_3915 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3907);
															BgL_arg1816z00_3914 =
																(BgL_arg1817z00_3915 - OBJECT_TYPE);
														}
														BgL_oclassz00_3912 =
															VECTOR_REF(BgL_arg1815z00_3913,
															BgL_arg1816z00_3914);
													}
													{	/* Cfa/box.scm 131 */
														bool_t BgL__ortest_1115z00_3916;

														BgL__ortest_1115z00_3916 =
															(BgL_classz00_3906 == BgL_oclassz00_3912);
														if (BgL__ortest_1115z00_3916)
															{	/* Cfa/box.scm 131 */
																BgL_res1947z00_3911 = BgL__ortest_1115z00_3916;
															}
														else
															{	/* Cfa/box.scm 131 */
																long BgL_odepthz00_3917;

																{	/* Cfa/box.scm 131 */
																	obj_t BgL_arg1804z00_3918;

																	BgL_arg1804z00_3918 = (BgL_oclassz00_3912);
																	BgL_odepthz00_3917 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3918);
																}
																if ((4L < BgL_odepthz00_3917))
																	{	/* Cfa/box.scm 131 */
																		obj_t BgL_arg1802z00_3919;

																		{	/* Cfa/box.scm 131 */
																			obj_t BgL_arg1803z00_3920;

																			BgL_arg1803z00_3920 =
																				(BgL_oclassz00_3912);
																			BgL_arg1802z00_3919 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3920, 4L);
																		}
																		BgL_res1947z00_3911 =
																			(BgL_arg1802z00_3919 ==
																			BgL_classz00_3906);
																	}
																else
																	{	/* Cfa/box.scm 131 */
																		BgL_res1947z00_3911 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2002z00_4108 = BgL_res1947z00_3911;
											}
									}
								else
									{	/* Cfa/box.scm 131 */
										BgL_test2002z00_4108 = ((bool_t) 0);
									}
							}
							if (BgL_test2002z00_4108)
								{	/* Cfa/box.scm 135 */
									obj_t BgL_arg1642z00_3921;

									BgL_arg1642z00_3921 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_nodez00_3847));
									return
										BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1974z00zzcfa_boxz00,
										BGl_string1975z00zzcfa_boxz00, BgL_arg1642z00_3921);
								}
							else
								{	/* Cfa/box.scm 131 */
									return BFALSE;
								}
						}
				}
			}
		}

	}



/* &cfa!-box-ref/O-Cinfo1532 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2boxzd2refzf2Ozd2Cinfo1532z50zzcfa_boxz00(obj_t
		BgL_envz00_3850, obj_t BgL_nodez00_3851)
	{
		{	/* Cfa/box.scm 97 */
			{	/* Cfa/box.scm 99 */
				BgL_approxz00_bglt BgL_boxzd2approxzd2_3923;

				{	/* Cfa/box.scm 99 */
					BgL_varz00_bglt BgL_arg1615z00_3924;

					BgL_arg1615z00_3924 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt)
									((BgL_boxzd2refzd2_bglt) BgL_nodez00_3851))))->BgL_varz00);
					BgL_boxzd2approxzd2_3923 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1615z00_3924));
				}
				{	/* Cfa/box.scm 103 */
					obj_t BgL_zc3z04anonymousza31607ze3z87_3925;

					BgL_zc3z04anonymousza31607ze3z87_3925 =
						MAKE_FX_PROCEDURE(BGl_z62zc3z04anonymousza31607ze3ze5zzcfa_boxz00,
						(int) (1L), (int) (2L));
					PROCEDURE_SET(BgL_zc3z04anonymousza31607ze3z87_3925,
						(int) (0L), ((obj_t) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_3851)));
					PROCEDURE_SET(BgL_zc3z04anonymousza31607ze3z87_3925,
						(int) (1L), ((obj_t) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_3851)));
					BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
						(BgL_zc3z04anonymousza31607ze3z87_3925, BgL_boxzd2approxzd2_3923);
			}}
			{
				BgL_boxzd2refzf2ozd2cinfozf2_bglt BgL_auxz00_4151;

				{
					obj_t BgL_auxz00_4152;

					{	/* Cfa/box.scm 112 */
						BgL_objectz00_bglt BgL_tmpz00_4153;

						BgL_tmpz00_4153 =
							((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_3851));
						BgL_auxz00_4152 = BGL_OBJECT_WIDENING(BgL_tmpz00_4153);
					}
					BgL_auxz00_4151 =
						((BgL_boxzd2refzf2ozd2cinfozf2_bglt) BgL_auxz00_4152);
				}
				return
					(((BgL_boxzd2refzf2ozd2cinfozf2_bglt) COBJECT(BgL_auxz00_4151))->
					BgL_approxz00);
			}
		}

	}



/* &<@anonymous:1607> */
	obj_t BGl_z62zc3z04anonymousza31607ze3ze5zzcfa_boxz00(obj_t BgL_envz00_3852,
		obj_t BgL_boxz00_3855)
	{
		{	/* Cfa/box.scm 101 */
			{	/* Cfa/box.scm 103 */
				BgL_boxzd2refzd2_bglt BgL_nodez00_3853;
				BgL_boxzd2refzd2_bglt BgL_i1193z00_3854;

				BgL_nodez00_3853 =
					((BgL_boxzd2refzd2_bglt) PROCEDURE_REF(BgL_envz00_3852, (int) (0L)));
				BgL_i1193z00_3854 =
					((BgL_boxzd2refzd2_bglt) PROCEDURE_REF(BgL_envz00_3852, (int) (1L)));
				{	/* Cfa/box.scm 103 */
					bool_t BgL_test2007z00_4165;

					{	/* Cfa/box.scm 103 */
						obj_t BgL_classz00_3926;

						BgL_classz00_3926 = BGl_makezd2boxzf2Ozd2Cinfozf2zzcfa_infoz00;
						if (BGL_OBJECTP(BgL_boxz00_3855))
							{	/* Cfa/box.scm 103 */
								BgL_objectz00_bglt BgL_arg1807z00_3927;

								BgL_arg1807z00_3927 = (BgL_objectz00_bglt) (BgL_boxz00_3855);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/box.scm 103 */
										long BgL_idxz00_3928;

										BgL_idxz00_3928 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3927);
										BgL_test2007z00_4165 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3928 + 4L)) == BgL_classz00_3926);
									}
								else
									{	/* Cfa/box.scm 103 */
										bool_t BgL_res1944z00_3931;

										{	/* Cfa/box.scm 103 */
											obj_t BgL_oclassz00_3932;

											{	/* Cfa/box.scm 103 */
												obj_t BgL_arg1815z00_3933;
												long BgL_arg1816z00_3934;

												BgL_arg1815z00_3933 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/box.scm 103 */
													long BgL_arg1817z00_3935;

													BgL_arg1817z00_3935 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3927);
													BgL_arg1816z00_3934 =
														(BgL_arg1817z00_3935 - OBJECT_TYPE);
												}
												BgL_oclassz00_3932 =
													VECTOR_REF(BgL_arg1815z00_3933, BgL_arg1816z00_3934);
											}
											{	/* Cfa/box.scm 103 */
												bool_t BgL__ortest_1115z00_3936;

												BgL__ortest_1115z00_3936 =
													(BgL_classz00_3926 == BgL_oclassz00_3932);
												if (BgL__ortest_1115z00_3936)
													{	/* Cfa/box.scm 103 */
														BgL_res1944z00_3931 = BgL__ortest_1115z00_3936;
													}
												else
													{	/* Cfa/box.scm 103 */
														long BgL_odepthz00_3937;

														{	/* Cfa/box.scm 103 */
															obj_t BgL_arg1804z00_3938;

															BgL_arg1804z00_3938 = (BgL_oclassz00_3932);
															BgL_odepthz00_3937 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3938);
														}
														if ((4L < BgL_odepthz00_3937))
															{	/* Cfa/box.scm 103 */
																obj_t BgL_arg1802z00_3939;

																{	/* Cfa/box.scm 103 */
																	obj_t BgL_arg1803z00_3940;

																	BgL_arg1803z00_3940 = (BgL_oclassz00_3932);
																	BgL_arg1802z00_3939 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3940,
																		4L);
																}
																BgL_res1944z00_3931 =
																	(BgL_arg1802z00_3939 == BgL_classz00_3926);
															}
														else
															{	/* Cfa/box.scm 103 */
																BgL_res1944z00_3931 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2007z00_4165 = BgL_res1944z00_3931;
									}
							}
						else
							{	/* Cfa/box.scm 103 */
								BgL_test2007z00_4165 = ((bool_t) 0);
							}
					}
					if (BgL_test2007z00_4165)
						{	/* Cfa/box.scm 105 */
							BgL_approxz00_bglt BgL_arg1609z00_3941;
							BgL_approxz00_bglt BgL_arg1611z00_3942;

							{
								BgL_boxzd2refzf2ozd2cinfozf2_bglt BgL_auxz00_4188;

								{
									obj_t BgL_auxz00_4189;

									{	/* Cfa/box.scm 105 */
										BgL_objectz00_bglt BgL_tmpz00_4190;

										BgL_tmpz00_4190 = ((BgL_objectz00_bglt) BgL_i1193z00_3854);
										BgL_auxz00_4189 = BGL_OBJECT_WIDENING(BgL_tmpz00_4190);
									}
									BgL_auxz00_4188 =
										((BgL_boxzd2refzf2ozd2cinfozf2_bglt) BgL_auxz00_4189);
								}
								BgL_arg1609z00_3941 =
									(((BgL_boxzd2refzf2ozd2cinfozf2_bglt)
										COBJECT(BgL_auxz00_4188))->BgL_approxz00);
							}
							{
								BgL_makezd2boxzf2ozd2cinfozf2_bglt BgL_auxz00_4195;

								{
									obj_t BgL_auxz00_4196;

									{	/* Cfa/box.scm 105 */
										BgL_objectz00_bglt BgL_tmpz00_4197;

										BgL_tmpz00_4197 =
											((BgL_objectz00_bglt)
											((BgL_makezd2boxzd2_bglt) BgL_boxz00_3855));
										BgL_auxz00_4196 = BGL_OBJECT_WIDENING(BgL_tmpz00_4197);
									}
									BgL_auxz00_4195 =
										((BgL_makezd2boxzf2ozd2cinfozf2_bglt) BgL_auxz00_4196);
								}
								BgL_arg1611z00_3942 =
									(((BgL_makezd2boxzf2ozd2cinfozf2_bglt)
										COBJECT(BgL_auxz00_4195))->BgL_valuezd2approxzd2);
							}
							return
								((obj_t)
								BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1609z00_3941,
									BgL_arg1611z00_3942));
						}
					else
						{	/* Cfa/box.scm 106 */
							bool_t BgL_test2012z00_4205;

							{	/* Cfa/box.scm 106 */
								obj_t BgL_classz00_3943;

								BgL_classz00_3943 = BGl_makezd2boxzf2Cinfoz20zzcfa_infoz00;
								if (BGL_OBJECTP(BgL_boxz00_3855))
									{	/* Cfa/box.scm 106 */
										BgL_objectz00_bglt BgL_arg1807z00_3944;

										BgL_arg1807z00_3944 =
											(BgL_objectz00_bglt) (BgL_boxz00_3855);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/box.scm 106 */
												long BgL_idxz00_3945;

												BgL_idxz00_3945 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3944);
												BgL_test2012z00_4205 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3945 + 4L)) == BgL_classz00_3943);
											}
										else
											{	/* Cfa/box.scm 106 */
												bool_t BgL_res1945z00_3948;

												{	/* Cfa/box.scm 106 */
													obj_t BgL_oclassz00_3949;

													{	/* Cfa/box.scm 106 */
														obj_t BgL_arg1815z00_3950;
														long BgL_arg1816z00_3951;

														BgL_arg1815z00_3950 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/box.scm 106 */
															long BgL_arg1817z00_3952;

															BgL_arg1817z00_3952 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3944);
															BgL_arg1816z00_3951 =
																(BgL_arg1817z00_3952 - OBJECT_TYPE);
														}
														BgL_oclassz00_3949 =
															VECTOR_REF(BgL_arg1815z00_3950,
															BgL_arg1816z00_3951);
													}
													{	/* Cfa/box.scm 106 */
														bool_t BgL__ortest_1115z00_3953;

														BgL__ortest_1115z00_3953 =
															(BgL_classz00_3943 == BgL_oclassz00_3949);
														if (BgL__ortest_1115z00_3953)
															{	/* Cfa/box.scm 106 */
																BgL_res1945z00_3948 = BgL__ortest_1115z00_3953;
															}
														else
															{	/* Cfa/box.scm 106 */
																long BgL_odepthz00_3954;

																{	/* Cfa/box.scm 106 */
																	obj_t BgL_arg1804z00_3955;

																	BgL_arg1804z00_3955 = (BgL_oclassz00_3949);
																	BgL_odepthz00_3954 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3955);
																}
																if ((4L < BgL_odepthz00_3954))
																	{	/* Cfa/box.scm 106 */
																		obj_t BgL_arg1802z00_3956;

																		{	/* Cfa/box.scm 106 */
																			obj_t BgL_arg1803z00_3957;

																			BgL_arg1803z00_3957 =
																				(BgL_oclassz00_3949);
																			BgL_arg1802z00_3956 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3957, 4L);
																		}
																		BgL_res1945z00_3948 =
																			(BgL_arg1802z00_3956 ==
																			BgL_classz00_3943);
																	}
																else
																	{	/* Cfa/box.scm 106 */
																		BgL_res1945z00_3948 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2012z00_4205 = BgL_res1945z00_3948;
											}
									}
								else
									{	/* Cfa/box.scm 106 */
										BgL_test2012z00_4205 = ((bool_t) 0);
									}
							}
							if (BgL_test2012z00_4205)
								{	/* Cfa/box.scm 110 */
									obj_t BgL_arg1613z00_3958;

									BgL_arg1613z00_3958 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_nodez00_3853));
									return
										BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1974z00zzcfa_boxz00,
										BGl_string1975z00zzcfa_boxz00, BgL_arg1613z00_3958);
								}
							else
								{	/* Cfa/box.scm 106 */
									return BFALSE;
								}
						}
				}
			}
		}

	}



/* &cfa!-make-box/O-Cinf1530 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2makezd2boxzf2Ozd2Cinf1530z50zzcfa_boxz00(obj_t
		BgL_envz00_3856, obj_t BgL_nodez00_3857)
	{
		{	/* Cfa/box.scm 87 */
			{	/* Cfa/box.scm 90 */
				BgL_approxz00_bglt BgL_initzd2valuezd2approxz00_3960;

				{	/* Cfa/box.scm 90 */
					BgL_nodez00_bglt BgL_arg1605z00_3961;

					BgL_arg1605z00_3961 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_3857))))->BgL_valuez00);
					BgL_initzd2valuezd2approxz00_3960 =
						BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1605z00_3961);
				}
				{	/* Cfa/box.scm 91 */
					BgL_approxz00_bglt BgL_arg1602z00_3962;

					{
						BgL_makezd2boxzf2ozd2cinfozf2_bglt BgL_auxz00_4235;

						{
							obj_t BgL_auxz00_4236;

							{	/* Cfa/box.scm 91 */
								BgL_objectz00_bglt BgL_tmpz00_4237;

								BgL_tmpz00_4237 =
									((BgL_objectz00_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_3857));
								BgL_auxz00_4236 = BGL_OBJECT_WIDENING(BgL_tmpz00_4237);
							}
							BgL_auxz00_4235 =
								((BgL_makezd2boxzf2ozd2cinfozf2_bglt) BgL_auxz00_4236);
						}
						BgL_arg1602z00_3962 =
							(((BgL_makezd2boxzf2ozd2cinfozf2_bglt) COBJECT(BgL_auxz00_4235))->
							BgL_valuezd2approxzd2);
					}
					BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1602z00_3962,
						BgL_initzd2valuezd2approxz00_3960);
				}
				{
					BgL_makezd2boxzf2ozd2cinfozf2_bglt BgL_auxz00_4244;

					{
						obj_t BgL_auxz00_4245;

						{	/* Cfa/box.scm 92 */
							BgL_objectz00_bglt BgL_tmpz00_4246;

							BgL_tmpz00_4246 =
								((BgL_objectz00_bglt)
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_3857));
							BgL_auxz00_4245 = BGL_OBJECT_WIDENING(BgL_tmpz00_4246);
						}
						BgL_auxz00_4244 =
							((BgL_makezd2boxzf2ozd2cinfozf2_bglt) BgL_auxz00_4245);
					}
					return
						(((BgL_makezd2boxzf2ozd2cinfozf2_bglt) COBJECT(BgL_auxz00_4244))->
						BgL_approxz00);
				}
			}
		}

	}



/* &node-setup!-box-ref1528 */
	obj_t BGl_z62nodezd2setupz12zd2boxzd2ref1528za2zzcfa_boxz00(obj_t
		BgL_envz00_3858, obj_t BgL_nodez00_3859)
	{
		{	/* Cfa/box.scm 73 */
			{	/* Cfa/box.scm 75 */
				BgL_varz00_bglt BgL_arg1589z00_3964;

				BgL_arg1589z00_3964 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_3859)))->BgL_varz00);
				BGl_nodezd2setupz12zc0zzcfa_setupz00(
					((BgL_nodez00_bglt) BgL_arg1589z00_3964));
			}
			if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 1L))
				{	/* Cfa/box.scm 76 */
					{	/* Cfa/box.scm 77 */
						BgL_boxzd2refzf2ozd2cinfozf2_bglt BgL_wide1186z00_3965;

						BgL_wide1186z00_3965 =
							((BgL_boxzd2refzf2ozd2cinfozf2_bglt)
							BOBJECT(GC_MALLOC(sizeof(struct
										BgL_boxzd2refzf2ozd2cinfozf2_bgl))));
						{	/* Cfa/box.scm 77 */
							obj_t BgL_auxz00_4264;
							BgL_objectz00_bglt BgL_tmpz00_4260;

							BgL_auxz00_4264 = ((obj_t) BgL_wide1186z00_3965);
							BgL_tmpz00_4260 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2refzd2_bglt)
									((BgL_boxzd2refzd2_bglt) BgL_nodez00_3859)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4260, BgL_auxz00_4264);
						}
						((BgL_objectz00_bglt)
							((BgL_boxzd2refzd2_bglt)
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_3859)));
						{	/* Cfa/box.scm 77 */
							long BgL_arg1593z00_3966;

							BgL_arg1593z00_3966 =
								BGL_CLASS_NUM(BGl_boxzd2refzf2Ozd2Cinfozf2zzcfa_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_boxzd2refzd2_bglt)
										((BgL_boxzd2refzd2_bglt) BgL_nodez00_3859))),
								BgL_arg1593z00_3966);
						}
						((BgL_boxzd2refzd2_bglt)
							((BgL_boxzd2refzd2_bglt)
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_3859)));
					}
					{
						BgL_boxzd2refzf2ozd2cinfozf2_bglt BgL_auxz00_4278;

						{
							obj_t BgL_auxz00_4279;

							{	/* Cfa/box.scm 78 */
								BgL_objectz00_bglt BgL_tmpz00_4280;

								BgL_tmpz00_4280 =
									((BgL_objectz00_bglt)
									((BgL_boxzd2refzd2_bglt)
										((BgL_boxzd2refzd2_bglt) BgL_nodez00_3859)));
								BgL_auxz00_4279 = BGL_OBJECT_WIDENING(BgL_tmpz00_4280);
							}
							BgL_auxz00_4278 =
								((BgL_boxzd2refzf2ozd2cinfozf2_bglt) BgL_auxz00_4279);
						}
						((((BgL_boxzd2refzf2ozd2cinfozf2_bglt) COBJECT(BgL_auxz00_4278))->
								BgL_approxz00) =
							((BgL_approxz00_bglt)
								BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
										BGl_za2objza2z00zztype_cachez00))), BUNSPEC);
					}
					return
						((obj_t)
						((BgL_boxzd2refzd2_bglt)
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_3859)));
				}
			else
				{	/* Cfa/box.scm 76 */
					{	/* Cfa/box.scm 80 */
						BgL_boxzd2refzf2cinfoz20_bglt BgL_wide1190z00_3967;

						BgL_wide1190z00_3967 =
							((BgL_boxzd2refzf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
										BgL_boxzd2refzf2cinfoz20_bgl))));
						{	/* Cfa/box.scm 80 */
							obj_t BgL_auxz00_4297;
							BgL_objectz00_bglt BgL_tmpz00_4293;

							BgL_auxz00_4297 = ((obj_t) BgL_wide1190z00_3967);
							BgL_tmpz00_4293 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2refzd2_bglt)
									((BgL_boxzd2refzd2_bglt) BgL_nodez00_3859)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4293, BgL_auxz00_4297);
						}
						((BgL_objectz00_bglt)
							((BgL_boxzd2refzd2_bglt)
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_3859)));
						{	/* Cfa/box.scm 80 */
							long BgL_arg1594z00_3968;

							BgL_arg1594z00_3968 =
								BGL_CLASS_NUM(BGl_boxzd2refzf2Cinfoz20zzcfa_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_boxzd2refzd2_bglt)
										((BgL_boxzd2refzd2_bglt) BgL_nodez00_3859))),
								BgL_arg1594z00_3968);
						}
						((BgL_boxzd2refzd2_bglt)
							((BgL_boxzd2refzd2_bglt)
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_3859)));
					}
					{
						BgL_boxzd2refzf2cinfoz20_bglt BgL_auxz00_4311;

						{
							obj_t BgL_auxz00_4312;

							{	/* Cfa/box.scm 81 */
								BgL_objectz00_bglt BgL_tmpz00_4313;

								BgL_tmpz00_4313 =
									((BgL_objectz00_bglt)
									((BgL_boxzd2refzd2_bglt)
										((BgL_boxzd2refzd2_bglt) BgL_nodez00_3859)));
								BgL_auxz00_4312 = BGL_OBJECT_WIDENING(BgL_tmpz00_4313);
							}
							BgL_auxz00_4311 =
								((BgL_boxzd2refzf2cinfoz20_bglt) BgL_auxz00_4312);
						}
						((((BgL_boxzd2refzf2cinfoz20_bglt) COBJECT(BgL_auxz00_4311))->
								BgL_approxz00) =
							((BgL_approxz00_bglt)
								BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
										BGl_za2objza2z00zztype_cachez00))), BUNSPEC);
					}
					((BgL_boxzd2refzd2_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_3859));
					{	/* Cfa/box.scm 82 */
						BgL_approxz00_bglt BgL_arg1595z00_3969;

						{
							BgL_boxzd2refzf2cinfoz20_bglt BgL_auxz00_4324;

							{
								obj_t BgL_auxz00_4325;

								{	/* Cfa/box.scm 82 */
									BgL_objectz00_bglt BgL_tmpz00_4326;

									BgL_tmpz00_4326 =
										((BgL_objectz00_bglt)
										((BgL_boxzd2refzd2_bglt)
											((BgL_boxzd2refzd2_bglt) BgL_nodez00_3859)));
									BgL_auxz00_4325 = BGL_OBJECT_WIDENING(BgL_tmpz00_4326);
								}
								BgL_auxz00_4324 =
									((BgL_boxzd2refzf2cinfoz20_bglt) BgL_auxz00_4325);
							}
							BgL_arg1595z00_3969 =
								(((BgL_boxzd2refzf2cinfoz20_bglt) COBJECT(BgL_auxz00_4324))->
								BgL_approxz00);
						}
						return
							BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_arg1595z00_3969);
					}
				}
		}

	}



/* &node-setup!-box-set!1526 */
	obj_t BGl_z62nodezd2setupz12zd2boxzd2setz121526zb0zzcfa_boxz00(obj_t
		BgL_envz00_3860, obj_t BgL_nodez00_3861)
	{
		{	/* Cfa/box.scm 60 */
			{	/* Cfa/box.scm 62 */
				BgL_varz00_bglt BgL_arg1575z00_3971;

				BgL_arg1575z00_3971 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3861)))->BgL_varz00);
				BGl_nodezd2setupz12zc0zzcfa_setupz00(
					((BgL_nodez00_bglt) BgL_arg1575z00_3971));
			}
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3861)))->BgL_valuez00));
			if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 1L))
				{	/* Cfa/box.scm 64 */
					{	/* Cfa/box.scm 65 */
						BgL_boxzd2setz12zf2ozd2cinfoze0_bglt BgL_wide1176z00_3972;

						BgL_wide1176z00_3972 =
							((BgL_boxzd2setz12zf2ozd2cinfoze0_bglt)
							BOBJECT(GC_MALLOC(sizeof(struct
										BgL_boxzd2setz12zf2ozd2cinfoze0_bgl))));
						{	/* Cfa/box.scm 65 */
							obj_t BgL_auxz00_4349;
							BgL_objectz00_bglt BgL_tmpz00_4345;

							BgL_auxz00_4349 = ((obj_t) BgL_wide1176z00_3972);
							BgL_tmpz00_4345 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2setz12zc0_bglt)
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3861)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4345, BgL_auxz00_4349);
						}
						((BgL_objectz00_bglt)
							((BgL_boxzd2setz12zc0_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3861)));
						{	/* Cfa/box.scm 65 */
							long BgL_arg1584z00_3973;

							BgL_arg1584z00_3973 =
								BGL_CLASS_NUM(BGl_boxzd2setz12zf2Ozd2Cinfoze0zzcfa_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_boxzd2setz12zc0_bglt)
										((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3861))),
								BgL_arg1584z00_3973);
						}
						((BgL_boxzd2setz12zc0_bglt)
							((BgL_boxzd2setz12zc0_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3861)));
					}
					{
						BgL_boxzd2setz12zf2ozd2cinfoze0_bglt BgL_auxz00_4363;

						{
							obj_t BgL_auxz00_4364;

							{	/* Cfa/box.scm 66 */
								BgL_objectz00_bglt BgL_tmpz00_4365;

								BgL_tmpz00_4365 =
									((BgL_objectz00_bglt)
									((BgL_boxzd2setz12zc0_bglt)
										((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3861)));
								BgL_auxz00_4364 = BGL_OBJECT_WIDENING(BgL_tmpz00_4365);
							}
							BgL_auxz00_4363 =
								((BgL_boxzd2setz12zf2ozd2cinfoze0_bglt) BgL_auxz00_4364);
						}
						((((BgL_boxzd2setz12zf2ozd2cinfoze0_bglt)
									COBJECT(BgL_auxz00_4363))->BgL_approxz00) =
							((BgL_approxz00_bglt)
								BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
										BGl_za2unspecza2z00zztype_cachez00))), BUNSPEC);
					}
					return
						((obj_t)
						((BgL_boxzd2setz12zc0_bglt)
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3861)));
				}
			else
				{	/* Cfa/box.scm 64 */
					{	/* Cfa/box.scm 67 */
						BgL_boxzd2setz12zf2cinfoz32_bglt BgL_wide1180z00_3974;

						BgL_wide1180z00_3974 =
							((BgL_boxzd2setz12zf2cinfoz32_bglt)
							BOBJECT(GC_MALLOC(sizeof(struct
										BgL_boxzd2setz12zf2cinfoz32_bgl))));
						{	/* Cfa/box.scm 67 */
							obj_t BgL_auxz00_4382;
							BgL_objectz00_bglt BgL_tmpz00_4378;

							BgL_auxz00_4382 = ((obj_t) BgL_wide1180z00_3974);
							BgL_tmpz00_4378 =
								((BgL_objectz00_bglt)
								((BgL_boxzd2setz12zc0_bglt)
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3861)));
							BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4378, BgL_auxz00_4382);
						}
						((BgL_objectz00_bglt)
							((BgL_boxzd2setz12zc0_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3861)));
						{	/* Cfa/box.scm 67 */
							long BgL_arg1585z00_3975;

							BgL_arg1585z00_3975 =
								BGL_CLASS_NUM(BGl_boxzd2setz12zf2Cinfoz32zzcfa_infoz00);
							BGL_OBJECT_CLASS_NUM_SET(
								((BgL_objectz00_bglt)
									((BgL_boxzd2setz12zc0_bglt)
										((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3861))),
								BgL_arg1585z00_3975);
						}
						((BgL_boxzd2setz12zc0_bglt)
							((BgL_boxzd2setz12zc0_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3861)));
					}
					{
						BgL_boxzd2setz12zf2cinfoz32_bglt BgL_auxz00_4396;

						{
							obj_t BgL_auxz00_4397;

							{	/* Cfa/box.scm 68 */
								BgL_objectz00_bglt BgL_tmpz00_4398;

								BgL_tmpz00_4398 =
									((BgL_objectz00_bglt)
									((BgL_boxzd2setz12zc0_bglt)
										((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3861)));
								BgL_auxz00_4397 = BGL_OBJECT_WIDENING(BgL_tmpz00_4398);
							}
							BgL_auxz00_4396 =
								((BgL_boxzd2setz12zf2cinfoz32_bglt) BgL_auxz00_4397);
						}
						((((BgL_boxzd2setz12zf2cinfoz32_bglt) COBJECT(BgL_auxz00_4396))->
								BgL_approxz00) =
							((BgL_approxz00_bglt)
								BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
										BGl_za2unspecza2z00zztype_cachez00))), BUNSPEC);
					}
					return
						((obj_t)
						((BgL_boxzd2setz12zc0_bglt)
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_3861)));
				}
		}

	}



/* &node-setup!-pre-make1524 */
	obj_t BGl_z62nodezd2setupz12zd2prezd2make1524za2zzcfa_boxz00(obj_t
		BgL_envz00_3862, obj_t BgL_nodez00_3863)
	{
		{	/* Cfa/box.scm 46 */
			{	/* Cfa/box.scm 48 */
				BgL_nodez00_bglt BgL_arg1559z00_3977;

				BgL_arg1559z00_3977 =
					(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt)
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_3863))))->BgL_valuez00);
				BGl_nodezd2setupz12zc0zzcfa_setupz00(BgL_arg1559z00_3977);
			}
			{	/* Cfa/box.scm 49 */
				BgL_makezd2boxzd2_bglt BgL_nodez00_3978;

				{	/* Cfa/box.scm 49 */
					long BgL_arg1565z00_3979;

					{	/* Cfa/box.scm 49 */
						obj_t BgL_arg1571z00_3980;

						{	/* Cfa/box.scm 49 */
							obj_t BgL_arg1573z00_3981;

							{	/* Cfa/box.scm 49 */
								obj_t BgL_arg1815z00_3982;
								long BgL_arg1816z00_3983;

								BgL_arg1815z00_3982 = (BGl_za2classesza2z00zz__objectz00);
								{	/* Cfa/box.scm 49 */
									long BgL_arg1817z00_3984;

									BgL_arg1817z00_3984 =
										BGL_OBJECT_CLASS_NUM(
										((BgL_objectz00_bglt)
											((BgL_makezd2boxzd2_bglt) BgL_nodez00_3863)));
									BgL_arg1816z00_3983 = (BgL_arg1817z00_3984 - OBJECT_TYPE);
								}
								BgL_arg1573z00_3981 =
									VECTOR_REF(BgL_arg1815z00_3982, BgL_arg1816z00_3983);
							}
							BgL_arg1571z00_3980 =
								BGl_classzd2superzd2zz__objectz00(BgL_arg1573z00_3981);
						}
						{	/* Cfa/box.scm 49 */
							obj_t BgL_tmpz00_4421;

							BgL_tmpz00_4421 = ((obj_t) BgL_arg1571z00_3980);
							BgL_arg1565z00_3979 = BGL_CLASS_NUM(BgL_tmpz00_4421);
					}}
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_3863)),
						BgL_arg1565z00_3979);
				}
				{	/* Cfa/box.scm 49 */
					BgL_objectz00_bglt BgL_tmpz00_4427;

					BgL_tmpz00_4427 =
						((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_3863));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4427, BFALSE);
				}
				((BgL_objectz00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_3863));
				BgL_nodez00_3978 = ((BgL_makezd2boxzd2_bglt) BgL_nodez00_3863);
				{	/* Cfa/box.scm 49 */

					{	/* Cfa/box.scm 50 */
						BgL_makezd2boxzd2_bglt BgL_nodez00_3985;

						{	/* Cfa/box.scm 50 */
							BgL_makezd2boxzf2ozd2cinfozf2_bglt BgL_wide1171z00_3986;

							BgL_wide1171z00_3986 =
								((BgL_makezd2boxzf2ozd2cinfozf2_bglt)
								BOBJECT(GC_MALLOC(sizeof(struct
											BgL_makezd2boxzf2ozd2cinfozf2_bgl))));
							{	/* Cfa/box.scm 50 */
								obj_t BgL_auxz00_4439;
								BgL_objectz00_bglt BgL_tmpz00_4435;

								BgL_auxz00_4439 = ((obj_t) BgL_wide1171z00_3986);
								BgL_tmpz00_4435 =
									((BgL_objectz00_bglt)
									((BgL_makezd2boxzd2_bglt)
										((BgL_makezd2boxzd2_bglt) BgL_nodez00_3978)));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4435, BgL_auxz00_4439);
							}
							((BgL_objectz00_bglt)
								((BgL_makezd2boxzd2_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_3978)));
							{	/* Cfa/box.scm 50 */
								long BgL_arg1564z00_3987;

								BgL_arg1564z00_3987 =
									BGL_CLASS_NUM(BGl_makezd2boxzf2Ozd2Cinfozf2zzcfa_infoz00);
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_makezd2boxzd2_bglt)
											((BgL_makezd2boxzd2_bglt) BgL_nodez00_3978))),
									BgL_arg1564z00_3987);
							}
							((BgL_makezd2boxzd2_bglt)
								((BgL_makezd2boxzd2_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_3978)));
						}
						{
							BgL_makezd2boxzf2ozd2cinfozf2_bglt BgL_auxz00_4453;

							{
								obj_t BgL_auxz00_4454;

								{	/* Cfa/box.scm 51 */
									BgL_objectz00_bglt BgL_tmpz00_4455;

									BgL_tmpz00_4455 =
										((BgL_objectz00_bglt)
										((BgL_makezd2boxzd2_bglt)
											((BgL_makezd2boxzd2_bglt) BgL_nodez00_3978)));
									BgL_auxz00_4454 = BGL_OBJECT_WIDENING(BgL_tmpz00_4455);
								}
								BgL_auxz00_4453 =
									((BgL_makezd2boxzf2ozd2cinfozf2_bglt) BgL_auxz00_4454);
							}
							((((BgL_makezd2boxzf2ozd2cinfozf2_bglt)
										COBJECT(BgL_auxz00_4453))->BgL_approxz00) =
								((BgL_approxz00_bglt)
									BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
											BGl_za2objza2z00zztype_cachez00))), BUNSPEC);
						}
						{
							BgL_makezd2boxzf2ozd2cinfozf2_bglt BgL_auxz00_4464;

							{
								obj_t BgL_auxz00_4465;

								{	/* Cfa/box.scm 52 */
									BgL_objectz00_bglt BgL_tmpz00_4466;

									BgL_tmpz00_4466 =
										((BgL_objectz00_bglt)
										((BgL_makezd2boxzd2_bglt)
											((BgL_makezd2boxzd2_bglt) BgL_nodez00_3978)));
									BgL_auxz00_4465 = BGL_OBJECT_WIDENING(BgL_tmpz00_4466);
								}
								BgL_auxz00_4464 =
									((BgL_makezd2boxzf2ozd2cinfozf2_bglt) BgL_auxz00_4465);
							}
							((((BgL_makezd2boxzf2ozd2cinfozf2_bglt)
										COBJECT(BgL_auxz00_4464))->BgL_valuezd2approxzd2) =
								((BgL_approxz00_bglt)
									BGl_makezd2emptyzd2approxz00zzcfa_approxz00()), BUNSPEC);
						}
						BgL_nodez00_3985 =
							((BgL_makezd2boxzd2_bglt)
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_3978));
						{	/* Cfa/box.scm 55 */
							BgL_approxz00_bglt BgL_arg1561z00_3988;

							BgL_arg1561z00_3988 =
								BGl_makezd2typezd2alloczd2approxzd2zzcfa_approxz00(
								((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00),
								((BgL_nodez00_bglt) BgL_nodez00_3985));
							{
								BgL_makezd2boxzf2ozd2cinfozf2_bglt BgL_auxz00_4479;

								{
									obj_t BgL_auxz00_4480;

									{	/* Cfa/box.scm 53 */
										BgL_objectz00_bglt BgL_tmpz00_4481;

										BgL_tmpz00_4481 = ((BgL_objectz00_bglt) BgL_nodez00_3985);
										BgL_auxz00_4480 = BGL_OBJECT_WIDENING(BgL_tmpz00_4481);
									}
									BgL_auxz00_4479 =
										((BgL_makezd2boxzf2ozd2cinfozf2_bglt) BgL_auxz00_4480);
								}
								return
									((((BgL_makezd2boxzf2ozd2cinfozf2_bglt)
											COBJECT(BgL_auxz00_4479))->BgL_approxz00) =
									((BgL_approxz00_bglt) BgL_arg1561z00_3988), BUNSPEC);
							}
						}
					}
				}
			}
		}

	}



/* &node-setup!-make-box1522 */
	obj_t BGl_z62nodezd2setupz12zd2makezd2box1522za2zzcfa_boxz00(obj_t
		BgL_envz00_3864, obj_t BgL_nodez00_3865)
	{
		{	/* Cfa/box.scm 36 */
			BGl_nodezd2setupz12zc0zzcfa_setupz00(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_3865)))->BgL_valuez00));
			{	/* Cfa/box.scm 39 */
				BgL_makezd2boxzf2cinfoz20_bglt BgL_wide1165z00_3990;

				BgL_wide1165z00_3990 =
					((BgL_makezd2boxzf2cinfoz20_bglt) BOBJECT(GC_MALLOC(sizeof(struct
								BgL_makezd2boxzf2cinfoz20_bgl))));
				{	/* Cfa/box.scm 39 */
					obj_t BgL_auxz00_4494;
					BgL_objectz00_bglt BgL_tmpz00_4490;

					BgL_auxz00_4494 = ((obj_t) BgL_wide1165z00_3990);
					BgL_tmpz00_4490 =
						((BgL_objectz00_bglt)
						((BgL_makezd2boxzd2_bglt)
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_3865)));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4490, BgL_auxz00_4494);
				}
				((BgL_objectz00_bglt)
					((BgL_makezd2boxzd2_bglt)
						((BgL_makezd2boxzd2_bglt) BgL_nodez00_3865)));
				{	/* Cfa/box.scm 39 */
					long BgL_arg1552z00_3991;

					BgL_arg1552z00_3991 =
						BGL_CLASS_NUM(BGl_makezd2boxzf2Cinfoz20zzcfa_infoz00);
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_makezd2boxzd2_bglt)
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_3865))),
						BgL_arg1552z00_3991);
				}
				((BgL_makezd2boxzd2_bglt)
					((BgL_makezd2boxzd2_bglt)
						((BgL_makezd2boxzd2_bglt) BgL_nodez00_3865)));
			}
			{
				BgL_makezd2boxzf2cinfoz20_bglt BgL_auxz00_4508;

				{
					obj_t BgL_auxz00_4509;

					{	/* Cfa/box.scm 40 */
						BgL_objectz00_bglt BgL_tmpz00_4510;

						BgL_tmpz00_4510 =
							((BgL_objectz00_bglt)
							((BgL_makezd2boxzd2_bglt)
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_3865)));
						BgL_auxz00_4509 = BGL_OBJECT_WIDENING(BgL_tmpz00_4510);
					}
					BgL_auxz00_4508 = ((BgL_makezd2boxzf2cinfoz20_bglt) BgL_auxz00_4509);
				}
				((((BgL_makezd2boxzf2cinfoz20_bglt) COBJECT(BgL_auxz00_4508))->
						BgL_approxz00) =
					((BgL_approxz00_bglt)
						BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
								BGl_za2objza2z00zztype_cachez00))), BUNSPEC);
			}
			((BgL_makezd2boxzd2_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_3865));
			{	/* Cfa/box.scm 41 */
				BgL_approxz00_bglt BgL_arg1553z00_3992;

				{
					BgL_makezd2boxzf2cinfoz20_bglt BgL_auxz00_4521;

					{
						obj_t BgL_auxz00_4522;

						{	/* Cfa/box.scm 41 */
							BgL_objectz00_bglt BgL_tmpz00_4523;

							BgL_tmpz00_4523 =
								((BgL_objectz00_bglt)
								((BgL_makezd2boxzd2_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_3865)));
							BgL_auxz00_4522 = BGL_OBJECT_WIDENING(BgL_tmpz00_4523);
						}
						BgL_auxz00_4521 =
							((BgL_makezd2boxzf2cinfoz20_bglt) BgL_auxz00_4522);
					}
					BgL_arg1553z00_3992 =
						(((BgL_makezd2boxzf2cinfoz20_bglt) COBJECT(BgL_auxz00_4521))->
						BgL_approxz00);
				}
				return BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_arg1553z00_3992);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_boxz00(void)
	{
		{	/* Cfa/box.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
			BGl_modulezd2initializa7ationz75zzcfa_loosez00(471177480L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
			BGl_modulezd2initializa7ationz75zzcfa_setupz00(168272122L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
			BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
			BGl_modulezd2initializa7ationz75zzcfa_cfaz00(400853817L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
			BGl_modulezd2initializa7ationz75zzcfa_iteratez00(131412063L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_closurez00(189402713L,
				BSTRING_TO_STRING(BGl_string1976z00zzcfa_boxz00));
		}

	}

#ifdef __cplusplus
}
#endif
