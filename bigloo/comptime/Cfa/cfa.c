/*===========================================================================*/
/*   (Cfa/cfa.scm)                                                           */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/cfa.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_CFA_TYPE_DEFINITIONS
#define BGL_CFA_CFA_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_scnstz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_nodez00;
		obj_t BgL_classz00;
		obj_t BgL_locz00;
	}               *BgL_scnstz00_bglt;

	typedef struct BgL_cvarz00_bgl
	{
		header_t header;
		obj_t widening;
		bool_t BgL_macrozf3zf3;
	}              *BgL_cvarz00_bglt;

	typedef struct BgL_sexitz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_handlerz00;
		bool_t BgL_detachedzf3zf3;
	}               *BgL_sexitz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_patchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
		struct BgL_varz00_bgl *BgL_refz00;
		long BgL_indexz00;
		obj_t BgL_patchidz00;
	}               *BgL_patchz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_pragmaz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_formatz00;
		obj_t BgL_srfi0z00;
	}                *BgL_pragmaz00_bglt;

	typedef struct BgL_genpatchidz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		long BgL_indexz00;
		long BgL_rindexz00;
	}                    *BgL_genpatchidz00_bglt;

	typedef struct BgL_getfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_getfieldz00_bglt;

	typedef struct BgL_setfieldz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_fnamez00;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                  *BgL_setfieldz00_bglt;

	typedef struct BgL_newz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		obj_t BgL_argszd2typezd2;
	}             *BgL_newz00_bglt;

	typedef struct BgL_instanceofz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_classz00;
	}                    *BgL_instanceofz00_bglt;

	typedef struct BgL_castzd2nullzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
	}                     *BgL_castzd2nullzd2_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_internzd2sfunzf2cinfoz20_bgl
	{
		bool_t BgL_polymorphiczf3zf3;
		struct BgL_approxz00_bgl *BgL_approxz00;
		long BgL_stampz00;
	}                               *BgL_internzd2sfunzf2cinfoz20_bglt;

	typedef struct BgL_scnstzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                       *BgL_scnstzf2cinfozf2_bglt;

	typedef struct BgL_svarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_clozd2envzf3z21;
		long BgL_stampz00;
	}                      *BgL_svarzf2cinfozf2_bglt;

	typedef struct BgL_cvarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_cvarzf2cinfozf2_bglt;

	typedef struct BgL_sexitzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                       *BgL_sexitzf2cinfozf2_bglt;

	typedef struct BgL_literalzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                         *BgL_literalzf2cinfozf2_bglt;

	typedef struct BgL_genpatchidzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                            *BgL_genpatchidzf2cinfozf2_bglt;

	typedef struct BgL_patchzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                       *BgL_patchzf2cinfozf2_bglt;

	typedef struct BgL_kwotezf2nodezf2_bgl
	{
		struct BgL_nodez00_bgl *BgL_nodez00;
	}                      *BgL_kwotezf2nodezf2_bglt;

	typedef struct BgL_kwotezf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                       *BgL_kwotezf2cinfozf2_bglt;

	typedef struct BgL_appzd2lyzf2cinfoz20_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                          *BgL_appzd2lyzf2cinfoz20_bglt;

	typedef struct BgL_setqzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_setqzf2cinfozf2_bglt;

	typedef struct BgL_conditionalzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                             *BgL_conditionalzf2cinfozf2_bglt;

	typedef struct BgL_failzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_failzf2cinfozf2_bglt;

	typedef struct BgL_switchzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                        *BgL_switchzf2cinfozf2_bglt;

	typedef struct BgL_setzd2exzd2itzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                               *BgL_setzd2exzd2itzf2cinfozf2_bglt;

	typedef struct BgL_jumpzd2exzd2itzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                                *BgL_jumpzd2exzd2itzf2cinfozf2_bglt;

	typedef struct BgL_makezd2boxzf2cinfoz20_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                            *BgL_makezd2boxzf2cinfoz20_bglt;

	typedef struct BgL_boxzd2setz12zf2cinfoz32_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                              *BgL_boxzd2setz12zf2cinfoz32_bglt;

	typedef struct BgL_boxzd2refzf2cinfoz20_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                           *BgL_boxzd2refzf2cinfoz20_bglt;

	typedef struct BgL_pragmazf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                        *BgL_pragmazf2cinfozf2_bglt;

	typedef struct BgL_getfieldzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                          *BgL_getfieldzf2cinfozf2_bglt;

	typedef struct BgL_setfieldzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                          *BgL_setfieldzf2cinfozf2_bglt;

	typedef struct BgL_newzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                     *BgL_newzf2cinfozf2_bglt;

	typedef struct BgL_instanceofzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                            *BgL_instanceofzf2cinfozf2_bglt;

	typedef struct BgL_castzd2nullzf2cinfoz20_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                             *BgL_castzd2nullzf2cinfoz20_bglt;


#endif													// BGL_CFA_CFA_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_newzf2Cinfozf2zzcfa_info3z00;
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2genpatchidzf2Cinf1635z50zzcfa_cfaz00(obj_t, obj_t);
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_pragmazf2Cinfozf2zzcfa_info3z00;
	extern obj_t BGl_approxz00zzcfa_infoz00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_setzd2exzd2itzf2Cinfozf2zzcfa_infoz00;
	static BgL_approxz00_bglt BGl_z62cfaz12zd2sequence1626za2zzcfa_cfaz00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzcfa_cfaz00(void);
	extern obj_t BGl_boxzd2setz12zf2Cinfoz32zzcfa_infoz00;
	extern obj_t BGl_genpatchidzf2Cinfozf2zzcfa_infoz00;
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2setfieldzf2Cinfo1639z50zzcfa_cfaz00(obj_t, obj_t);
	static BgL_approxz00_bglt BGl_z62cfaz12zd2letzd2fun1657z70zzcfa_cfaz00(obj_t,
		obj_t);
	extern obj_t BGl_appzd2lyzf2Cinfoz20zzcfa_infoz00;
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2literalzf2Cinfo1602z50zzcfa_cfaz00(obj_t, obj_t);
	extern obj_t BGl_makezd2boxzf2Cinfoz20zzcfa_infoz00;
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2kwotezf2Cinfo1606z50zzcfa_cfaz00(obj_t, obj_t);
	extern obj_t BGl_globalzd2loosez12zc0zzcfa_loosez00(BgL_globalz00_bglt,
		BgL_approxz00_bglt);
	extern obj_t BGl_cvarzf2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_switchzf2Cinfozf2zzcfa_infoz00;
	static obj_t BGl_methodzd2initzd2zzcfa_cfaz00(void);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2instanceofzf2Cinf1643z50zzcfa_cfaz00(obj_t, obj_t);
	extern obj_t BGl_literalzf2Cinfozf2zzcfa_infoz00;
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2conditionalzf2Cin1651z50zzcfa_cfaz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_cfazd2variablezd2valuezd2approxzd2zzcfa_cfaz00(BgL_valuez00_bglt);
	extern obj_t BGl_castzd2nullzf2Cinfoz20zzcfa_info3z00;
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_failzf2Cinfozf2zzcfa_infoz00;
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2setzd2exzd2itzf2Cinfo1661z50zzcfa_cfaz00(obj_t, obj_t);
	extern obj_t BGl_setqzf2Cinfozf2zzcfa_infoz00;
	extern BgL_approxz00_bglt
		BGl_unionzd2approxzd2filterz12z12zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_approxz00_bglt);
	static BgL_approxz00_bglt BGl_z62cfaz12zd2closure1612za2zzcfa_cfaz00(obj_t,
		obj_t);
	extern obj_t BGl_kwotezf2Cinfozf2zzcfa_infoz00;
	static BgL_approxz00_bglt BGl_z62cfaz12zd2letzd2var1659z70zzcfa_cfaz00(obj_t,
		obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2failzf2Cinfo1653z50zzcfa_cfaz00(obj_t, obj_t);
	extern BgL_approxz00_bglt
		BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_approxz00_bglt);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_cfaz00(void);
	extern obj_t BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
	extern obj_t BGl_kwotezf2nodezf2zzcfa_infoz00;
	extern obj_t BGl_instanceofzf2Cinfozf2zzcfa_info3z00;
	extern obj_t BGl_sexitzf2Cinfozf2zzcfa_infoz00;
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2boxzd2refzf2Cinfo1669z82zzcfa_cfaz00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2setqzf2Cinfo1649z50zzcfa_cfaz00(obj_t, obj_t);
	static BgL_approxz00_bglt BGl_z62cfaz12z70zzcfa_cfaz00(obj_t, obj_t);
	extern obj_t BGl_svarzf2Cinfozf2zzcfa_infoz00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static BgL_approxz00_bglt BGl_z62cfaz12zd2sync1628za2zzcfa_cfaz00(obj_t,
		obj_t);
	extern obj_t BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_approxz00_bglt);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2jumpzd2exzd2itzf2Cinf1663z50zzcfa_cfaz00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2boxzd2setz12zf2Cinfo1667z90zzcfa_cfaz00(obj_t, obj_t);
	extern obj_t BGl_getfieldzf2Cinfozf2zzcfa_info3z00;
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_cfaz00 = BUNSPEC;
	extern obj_t BGl_patchzf2Cinfozf2zzcfa_infoz00;
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2appzd2lyzf2Cinfo1631z82zzcfa_cfaz00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2switchzf2Cinfo1655z50zzcfa_cfaz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzcfa_cfaz00(void);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2castzd2nullzf2Cinfo1645z82zzcfa_cfaz00(obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcfa_cfaz00(void);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2makezd2boxzf2Cinfo1665z82zzcfa_cfaz00(obj_t, obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	extern BgL_approxz00_bglt BGl_loosez12z12zzcfa_loosez00(BgL_approxz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2kwotezf2node1608z50zzcfa_cfaz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62cfazd2variablezd2valuezd2approxzb0zzcfa_cfaz00(obj_t,
		obj_t);
	static obj_t BGl_z62cfazd2variablezd2valuezd2a1613zb0zzcfa_cfaz00(obj_t,
		obj_t);
	static obj_t BGl_z62cfazd2variablezd2valuezd2a1616zb0zzcfa_cfaz00(obj_t,
		obj_t);
	static obj_t BGl_z62cfazd2variablezd2valuezd2a1618zb0zzcfa_cfaz00(obj_t,
		obj_t);
	static obj_t BGl_z62cfazd2variablezd2valuezd2a1620zb0zzcfa_cfaz00(obj_t,
		obj_t);
	static obj_t BGl_z62cfazd2variablezd2valuezd2a1622zb0zzcfa_cfaz00(obj_t,
		obj_t);
	static obj_t BGl_z62cfazd2variablezd2valuezd2a1624zb0zzcfa_cfaz00(obj_t,
		obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	static obj_t BGl_z62cfaz121599z70zzcfa_cfaz00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2patchzf2Cinfo1604z50zzcfa_cfaz00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2pragmazf2Cinfo1633z50zzcfa_cfaz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_cfaz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_funcallz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_appz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_iteratez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_loosez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzcfa_cfaz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_cfaz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_cfaz00(void);
	extern obj_t BGl_conditionalzf2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_jumpzd2exzd2itzf2Cinfozf2zzcfa_infoz00;
	static BgL_approxz00_bglt BGl_z62cfaz12zd2var1610za2zzcfa_cfaz00(obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_valuez00zzast_varz00;
	static BgL_approxz00_bglt BGl_z62cfaz12zd2cast1647za2zzcfa_cfaz00(obj_t,
		obj_t);
	extern obj_t BGl_scnstzf2Cinfozf2zzcfa_infoz00;
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2getfieldzf2Cinfo1637z50zzcfa_cfaz00(obj_t, obj_t);
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_cfaz12z12zzcfa_cfaz00(BgL_nodez00_bglt);
	extern obj_t BGl_boxzd2refzf2Cinfoz20zzcfa_infoz00;
	extern obj_t BGl_setfieldzf2Cinfozf2zzcfa_info3z00;
	extern obj_t BGl_globalz00zzast_varz00;
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2newzf2Cinfo1641z50zzcfa_cfaz00(obj_t, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_STRING(BGl_string2136z00zzcfa_cfaz00,
		BgL_bgl_string2136za700za7za7c2140za7, "Unexpected closure", 18);
	      DEFINE_STRING(BGl_string2137z00zzcfa_cfaz00,
		BgL_bgl_string2137za700za7za7c2141za7, "cfa_cfa", 7);
	      DEFINE_STRING(BGl_string2138z00zzcfa_cfaz00,
		BgL_bgl_string2138za700za7za7c2142za7,
		"static all cfa-variable-value-a1613 ", 36);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2095z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza7121599za7702143za7, BGl_z62cfaz121599z70zzcfa_cfaz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2097z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza7d2variabl2144z00,
		BGl_z62cfazd2variablezd2valuezd2a1613zb0zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_GENERIC(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za770za7za7cf2145za7, BGl_z62cfaz12z70zzcfa_cfaz00, 0L,
		BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2101z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2lite2146za7,
		BGl_z62cfaz12zd2literalzf2Cinfo1602z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2103z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2patc2147za7,
		BGl_z62cfaz12zd2patchzf2Cinfo1604z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2104z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2kwot2148za7,
		BGl_z62cfaz12zd2kwotezf2Cinfo1606z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2105z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2kwot2149za7,
		BGl_z62cfaz12zd2kwotezf2node1608z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2106z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2var12150za7,
		BGl_z62cfaz12zd2var1610za2zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2107z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2clos2151za7,
		BGl_z62cfaz12zd2closure1612za2zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2108z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza7d2variabl2152z00,
		BGl_z62cfazd2variablezd2valuezd2a1616zb0zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2110z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza7d2variabl2153z00,
		BGl_z62cfazd2variablezd2valuezd2a1618zb0zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2111z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza7d2variabl2154z00,
		BGl_z62cfazd2variablezd2valuezd2a1620zb0zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2112z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza7d2variabl2155z00,
		BGl_z62cfazd2variablezd2valuezd2a1622zb0zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2113z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza7d2variabl2156z00,
		BGl_z62cfazd2variablezd2valuezd2a1624zb0zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2114z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2sequ2157za7,
		BGl_z62cfaz12zd2sequence1626za2zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2115z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2sync2158za7,
		BGl_z62cfaz12zd2sync1628za2zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2116z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2appza72159z00,
		BGl_z62cfaz12zd2appzd2lyzf2Cinfo1631z82zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2117z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2prag2160za7,
		BGl_z62cfaz12zd2pragmazf2Cinfo1633z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2118z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2genp2161za7,
		BGl_z62cfaz12zd2genpatchidzf2Cinf1635z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2119z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2getf2162za7,
		BGl_z62cfaz12zd2getfieldzf2Cinfo1637z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2120z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2setf2163za7,
		BGl_z62cfaz12zd2setfieldzf2Cinfo1639z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2121z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2newza72164z00,
		BGl_z62cfaz12zd2newzf2Cinfo1641z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2122z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2inst2165za7,
		BGl_z62cfaz12zd2instanceofzf2Cinf1643z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2123z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2cast2166za7,
		BGl_z62cfaz12zd2castzd2nullzf2Cinfo1645z82zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2124z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2cast2167za7,
		BGl_z62cfaz12zd2cast1647za2zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2125z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2setq2168za7,
		BGl_z62cfaz12zd2setqzf2Cinfo1649z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2126z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2cond2169za7,
		BGl_z62cfaz12zd2conditionalzf2Cin1651z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2127z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2fail2170za7,
		BGl_z62cfaz12zd2failzf2Cinfo1653z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2128z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2swit2171za7,
		BGl_z62cfaz12zd2switchzf2Cinfo1655z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2129z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2letza72172z00,
		BGl_z62cfaz12zd2letzd2fun1657z70zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2130z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2letza72173z00,
		BGl_z62cfaz12zd2letzd2var1659z70zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2131z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2setza72174z00,
		BGl_z62cfaz12zd2setzd2exzd2itzf2Cinfo1661z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2132z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2jump2175za7,
		BGl_z62cfaz12zd2jumpzd2exzd2itzf2Cinf1663z50zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2133z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2make2176za7,
		BGl_z62cfaz12zd2makezd2boxzf2Cinfo1665z82zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2134z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2boxza72177z00,
		BGl_z62cfaz12zd2boxzd2setz12zf2Cinfo1667z90zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2135z00zzcfa_cfaz00,
		BgL_bgl_za762cfaza712za7d2boxza72178z00,
		BGl_z62cfaz12zd2boxzd2refzf2Cinfo1669z82zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2096z00zzcfa_cfaz00,
		BgL_bgl_string2096za700za7za7c2179za7, "cfa!1599", 8);
	      DEFINE_STRING(BGl_string2098z00zzcfa_cfaz00,
		BgL_bgl_string2098za700za7za7c2180za7, "cfa-variable-value-a1613", 24);
	      DEFINE_STRING(BGl_string2099z00zzcfa_cfaz00,
		BgL_bgl_string2099za700za7za7c2181za7, "No method for this object", 25);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_cfazd2variablezd2valuezd2approxzd2envz00zzcfa_cfaz00,
		BgL_bgl_za762cfaza7d2variabl2182z00,
		BGl_z62cfazd2variablezd2valuezd2approxzb0zzcfa_cfaz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2100z00zzcfa_cfaz00,
		BgL_bgl_string2100za700za7za7c2183za7, "cfa!:no method for this ast", 27);
	      DEFINE_STRING(BGl_string2102z00zzcfa_cfaz00,
		BgL_bgl_string2102za700za7za7c2184za7, "cfa!", 4);
	      DEFINE_STRING(BGl_string2109z00zzcfa_cfaz00,
		BgL_bgl_string2109za700za7za7c2185za7, "cfa-variable-value-approx", 25);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_cfaz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_cfaz00(long
		BgL_checksumz00_4821, char *BgL_fromz00_4822)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_cfaz00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_cfaz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_cfaz00();
					BGl_libraryzd2moduleszd2initz00zzcfa_cfaz00();
					BGl_cnstzd2initzd2zzcfa_cfaz00();
					BGl_importedzd2moduleszd2initz00zzcfa_cfaz00();
					BGl_genericzd2initzd2zzcfa_cfaz00();
					BGl_methodzd2initzd2zzcfa_cfaz00();
					return BGl_toplevelzd2initzd2zzcfa_cfaz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_cfaz00(void)
	{
		{	/* Cfa/cfa.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cfa_cfa");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_cfa");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_cfa");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_cfa");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_cfa");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "cfa_cfa");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_cfa");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_cfa");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cfa_cfa");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_cfa");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_cfaz00(void)
	{
		{	/* Cfa/cfa.scm 15 */
			{	/* Cfa/cfa.scm 15 */
				obj_t BgL_cportz00_4660;

				{	/* Cfa/cfa.scm 15 */
					obj_t BgL_stringz00_4667;

					BgL_stringz00_4667 = BGl_string2138z00zzcfa_cfaz00;
					{	/* Cfa/cfa.scm 15 */
						obj_t BgL_startz00_4668;

						BgL_startz00_4668 = BINT(0L);
						{	/* Cfa/cfa.scm 15 */
							obj_t BgL_endz00_4669;

							BgL_endz00_4669 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4667)));
							{	/* Cfa/cfa.scm 15 */

								BgL_cportz00_4660 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4667, BgL_startz00_4668, BgL_endz00_4669);
				}}}}
				{
					long BgL_iz00_4661;

					BgL_iz00_4661 = 2L;
				BgL_loopz00_4662:
					if ((BgL_iz00_4661 == -1L))
						{	/* Cfa/cfa.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/cfa.scm 15 */
							{	/* Cfa/cfa.scm 15 */
								obj_t BgL_arg2139z00_4663;

								{	/* Cfa/cfa.scm 15 */

									{	/* Cfa/cfa.scm 15 */
										obj_t BgL_locationz00_4665;

										BgL_locationz00_4665 = BBOOL(((bool_t) 0));
										{	/* Cfa/cfa.scm 15 */

											BgL_arg2139z00_4663 =
												BGl_readz00zz__readerz00(BgL_cportz00_4660,
												BgL_locationz00_4665);
										}
									}
								}
								{	/* Cfa/cfa.scm 15 */
									int BgL_tmpz00_4852;

									BgL_tmpz00_4852 = (int) (BgL_iz00_4661);
									CNST_TABLE_SET(BgL_tmpz00_4852, BgL_arg2139z00_4663);
							}}
							{	/* Cfa/cfa.scm 15 */
								int BgL_auxz00_4666;

								BgL_auxz00_4666 = (int) ((BgL_iz00_4661 - 1L));
								{
									long BgL_iz00_4857;

									BgL_iz00_4857 = (long) (BgL_auxz00_4666);
									BgL_iz00_4661 = BgL_iz00_4857;
									goto BgL_loopz00_4662;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_cfaz00(void)
	{
		{	/* Cfa/cfa.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_cfaz00(void)
	{
		{	/* Cfa/cfa.scm 15 */
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_cfaz00(void)
	{
		{	/* Cfa/cfa.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_cfaz00(void)
	{
		{	/* Cfa/cfa.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_proc2095z00zzcfa_cfaz00, BGl_nodez00zzast_nodez00,
				BGl_string2096z00zzcfa_cfaz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_cfazd2variablezd2valuezd2approxzd2envz00zzcfa_cfaz00,
				BGl_proc2097z00zzcfa_cfaz00, BGl_valuez00zzast_varz00,
				BGl_string2098z00zzcfa_cfaz00);
		}

	}



/* &cfa-variable-value-a1613 */
	obj_t BGl_z62cfazd2variablezd2valuezd2a1613zb0zzcfa_cfaz00(obj_t
		BgL_envz00_4553, obj_t BgL_valuez00_4554)
	{
		{	/* Cfa/cfa.scm 93 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
				BGl_string2099z00zzcfa_cfaz00,
				((obj_t) ((BgL_valuez00_bglt) BgL_valuez00_4554)));
		}

	}



/* &cfa!1599 */
	obj_t BGl_z62cfaz121599z70zzcfa_cfaz00(obj_t BgL_envz00_4555,
		obj_t BgL_nodez00_4556)
	{
		{	/* Cfa/cfa.scm 38 */
			{	/* Cfa/cfa.scm 39 */
				obj_t BgL_arg1688z00_4673;

				BgL_arg1688z00_4673 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_4556)));
				return
					BGl_internalzd2errorzd2zztools_errorz00(BGl_string2100z00zzcfa_cfaz00,
					((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_4556)), BgL_arg1688z00_4673);
			}
		}

	}



/* cfa! */
	BGL_EXPORTED_DEF BgL_approxz00_bglt BGl_cfaz12z12zzcfa_cfaz00(BgL_nodez00_bglt
		BgL_nodez00_3)
	{
		{	/* Cfa/cfa.scm 38 */
			{	/* Cfa/cfa.scm 38 */
				obj_t BgL_method1600z00_3456;

				{	/* Cfa/cfa.scm 38 */
					obj_t BgL_res2087z00_4363;

					{	/* Cfa/cfa.scm 38 */
						long BgL_objzd2classzd2numz00_4334;

						BgL_objzd2classzd2numz00_4334 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_3));
						{	/* Cfa/cfa.scm 38 */
							obj_t BgL_arg1811z00_4335;

							BgL_arg1811z00_4335 =
								PROCEDURE_REF(BGl_cfaz12zd2envzc0zzcfa_cfaz00, (int) (1L));
							{	/* Cfa/cfa.scm 38 */
								int BgL_offsetz00_4338;

								BgL_offsetz00_4338 = (int) (BgL_objzd2classzd2numz00_4334);
								{	/* Cfa/cfa.scm 38 */
									long BgL_offsetz00_4339;

									BgL_offsetz00_4339 =
										((long) (BgL_offsetz00_4338) - OBJECT_TYPE);
									{	/* Cfa/cfa.scm 38 */
										long BgL_modz00_4340;

										BgL_modz00_4340 =
											(BgL_offsetz00_4339 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/cfa.scm 38 */
											long BgL_restz00_4342;

											BgL_restz00_4342 =
												(BgL_offsetz00_4339 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/cfa.scm 38 */

												{	/* Cfa/cfa.scm 38 */
													obj_t BgL_bucketz00_4344;

													BgL_bucketz00_4344 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4335), BgL_modz00_4340);
													BgL_res2087z00_4363 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4344), BgL_restz00_4342);
					}}}}}}}}
					BgL_method1600z00_3456 = BgL_res2087z00_4363;
				}
				return
					((BgL_approxz00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1600z00_3456, ((obj_t) BgL_nodez00_3)));
			}
		}

	}



/* &cfa! */
	BgL_approxz00_bglt BGl_z62cfaz12z70zzcfa_cfaz00(obj_t BgL_envz00_4557,
		obj_t BgL_nodez00_4558)
	{
		{	/* Cfa/cfa.scm 38 */
			return BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_nodez00_4558));
		}

	}



/* cfa-variable-value-approx */
	BGL_EXPORTED_DEF obj_t
		BGl_cfazd2variablezd2valuezd2approxzd2zzcfa_cfaz00(BgL_valuez00_bglt
		BgL_valuez00_10)
	{
		{	/* Cfa/cfa.scm 93 */
			{	/* Cfa/cfa.scm 93 */
				obj_t BgL_method1614z00_3457;

				{	/* Cfa/cfa.scm 93 */
					obj_t BgL_res2092z00_4394;

					{	/* Cfa/cfa.scm 93 */
						long BgL_objzd2classzd2numz00_4365;

						BgL_objzd2classzd2numz00_4365 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_valuez00_10));
						{	/* Cfa/cfa.scm 93 */
							obj_t BgL_arg1811z00_4366;

							BgL_arg1811z00_4366 =
								PROCEDURE_REF
								(BGl_cfazd2variablezd2valuezd2approxzd2envz00zzcfa_cfaz00,
								(int) (1L));
							{	/* Cfa/cfa.scm 93 */
								int BgL_offsetz00_4369;

								BgL_offsetz00_4369 = (int) (BgL_objzd2classzd2numz00_4365);
								{	/* Cfa/cfa.scm 93 */
									long BgL_offsetz00_4370;

									BgL_offsetz00_4370 =
										((long) (BgL_offsetz00_4369) - OBJECT_TYPE);
									{	/* Cfa/cfa.scm 93 */
										long BgL_modz00_4371;

										BgL_modz00_4371 =
											(BgL_offsetz00_4370 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/cfa.scm 93 */
											long BgL_restz00_4373;

											BgL_restz00_4373 =
												(BgL_offsetz00_4370 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/cfa.scm 93 */

												{	/* Cfa/cfa.scm 93 */
													obj_t BgL_bucketz00_4375;

													BgL_bucketz00_4375 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4366), BgL_modz00_4371);
													BgL_res2092z00_4394 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4375), BgL_restz00_4373);
					}}}}}}}}
					BgL_method1614z00_3457 = BgL_res2092z00_4394;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1614z00_3457,
					((obj_t) BgL_valuez00_10));
			}
		}

	}



/* &cfa-variable-value-approx */
	obj_t BGl_z62cfazd2variablezd2valuezd2approxzb0zzcfa_cfaz00(obj_t
		BgL_envz00_4559, obj_t BgL_valuez00_4560)
	{
		{	/* Cfa/cfa.scm 93 */
			return
				BGl_cfazd2variablezd2valuezd2approxzd2zzcfa_cfaz00(
				((BgL_valuez00_bglt) BgL_valuez00_4560));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_cfaz00(void)
	{
		{	/* Cfa/cfa.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_literalzf2Cinfozf2zzcfa_infoz00,
				BGl_proc2101z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_patchzf2Cinfozf2zzcfa_infoz00,
				BGl_proc2103z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_kwotezf2Cinfozf2zzcfa_infoz00,
				BGl_proc2104z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_kwotezf2nodezf2zzcfa_infoz00,
				BGl_proc2105z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_varz00zzast_nodez00,
				BGl_proc2106z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_closurez00zzast_nodez00,
				BGl_proc2107z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfazd2variablezd2valuezd2approxzd2envz00zzcfa_cfaz00,
				BGl_svarzf2Cinfozf2zzcfa_infoz00, BGl_proc2108z00zzcfa_cfaz00,
				BGl_string2109z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfazd2variablezd2valuezd2approxzd2envz00zzcfa_cfaz00,
				BGl_scnstzf2Cinfozf2zzcfa_infoz00, BGl_proc2110z00zzcfa_cfaz00,
				BGl_string2109z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfazd2variablezd2valuezd2approxzd2envz00zzcfa_cfaz00,
				BGl_cvarzf2Cinfozf2zzcfa_infoz00, BGl_proc2111z00zzcfa_cfaz00,
				BGl_string2109z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfazd2variablezd2valuezd2approxzd2envz00zzcfa_cfaz00,
				BGl_sexitzf2Cinfozf2zzcfa_infoz00, BGl_proc2112z00zzcfa_cfaz00,
				BGl_string2109z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfazd2variablezd2valuezd2approxzd2envz00zzcfa_cfaz00,
				BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00, BGl_proc2113z00zzcfa_cfaz00,
				BGl_string2109z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_sequencez00zzast_nodez00,
				BGl_proc2114z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_syncz00zzast_nodez00,
				BGl_proc2115z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_appzd2lyzf2Cinfoz20zzcfa_infoz00,
				BGl_proc2116z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_pragmazf2Cinfozf2zzcfa_info3z00,
				BGl_proc2117z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_genpatchidzf2Cinfozf2zzcfa_infoz00, BGl_proc2118z00zzcfa_cfaz00,
				BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_getfieldzf2Cinfozf2zzcfa_info3z00,
				BGl_proc2119z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_setfieldzf2Cinfozf2zzcfa_info3z00,
				BGl_proc2120z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_newzf2Cinfozf2zzcfa_info3z00,
				BGl_proc2121z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_instanceofzf2Cinfozf2zzcfa_info3z00, BGl_proc2122z00zzcfa_cfaz00,
				BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_castzd2nullzf2Cinfoz20zzcfa_info3z00, BGl_proc2123z00zzcfa_cfaz00,
				BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_castz00zzast_nodez00,
				BGl_proc2124z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_setqzf2Cinfozf2zzcfa_infoz00,
				BGl_proc2125z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_conditionalzf2Cinfozf2zzcfa_infoz00, BGl_proc2126z00zzcfa_cfaz00,
				BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_failzf2Cinfozf2zzcfa_infoz00,
				BGl_proc2127z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_switchzf2Cinfozf2zzcfa_infoz00,
				BGl_proc2128z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc2129z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc2130z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_setzd2exzd2itzf2Cinfozf2zzcfa_infoz00, BGl_proc2131z00zzcfa_cfaz00,
				BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_jumpzd2exzd2itzf2Cinfozf2zzcfa_infoz00, BGl_proc2132z00zzcfa_cfaz00,
				BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_makezd2boxzf2Cinfoz20zzcfa_infoz00, BGl_proc2133z00zzcfa_cfaz00,
				BGl_string2102z00zzcfa_cfaz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_boxzd2setz12zf2Cinfoz32zzcfa_infoz00, BGl_proc2134z00zzcfa_cfaz00,
				BGl_string2102z00zzcfa_cfaz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_boxzd2refzf2Cinfoz20zzcfa_infoz00,
				BGl_proc2135z00zzcfa_cfaz00, BGl_string2102z00zzcfa_cfaz00);
		}

	}



/* &cfa!-box-ref/Cinfo1669 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2boxzd2refzf2Cinfo1669z82zzcfa_cfaz00(obj_t
		BgL_envz00_4594, obj_t BgL_nodez00_4595)
	{
		{	/* Cfa/cfa.scm 369 */
			{	/* Cfa/cfa.scm 371 */
				BgL_varz00_bglt BgL_arg1878z00_4675;

				BgL_arg1878z00_4675 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt)
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_4595))))->BgL_varz00);
				BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1878z00_4675));
			}
			{
				BgL_boxzd2refzf2cinfoz20_bglt BgL_auxz00_4975;

				{
					obj_t BgL_auxz00_4976;

					{	/* Cfa/cfa.scm 372 */
						BgL_objectz00_bglt BgL_tmpz00_4977;

						BgL_tmpz00_4977 =
							((BgL_objectz00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_4595));
						BgL_auxz00_4976 = BGL_OBJECT_WIDENING(BgL_tmpz00_4977);
					}
					BgL_auxz00_4975 = ((BgL_boxzd2refzf2cinfoz20_bglt) BgL_auxz00_4976);
				}
				return
					(((BgL_boxzd2refzf2cinfoz20_bglt) COBJECT(BgL_auxz00_4975))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-box-set!/Cinfo1667 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2boxzd2setz12zf2Cinfo1667z90zzcfa_cfaz00(obj_t
		BgL_envz00_4596, obj_t BgL_nodez00_4597)
	{
		{	/* Cfa/cfa.scm 358 */
			{	/* Cfa/cfa.scm 360 */
				BgL_varz00_bglt BgL_arg1873z00_4677;

				BgL_arg1873z00_4677 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt)
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_4597))))->BgL_varz00);
				BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1873z00_4677));
			}
			{	/* Cfa/cfa.scm 361 */
				BgL_approxz00_bglt BgL_valzd2approxzd2_4678;

				{	/* Cfa/cfa.scm 361 */
					BgL_nodez00_bglt BgL_arg1875z00_4679;

					BgL_arg1875z00_4679 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt)
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_4597))))->
						BgL_valuez00);
					BgL_valzd2approxzd2_4678 =
						BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1875z00_4679);
				}
				{	/* Cfa/cfa.scm 362 */
					BgL_approxz00_bglt BgL_arg1874z00_4680;

					{
						BgL_boxzd2setz12zf2cinfoz32_bglt BgL_auxz00_4992;

						{
							obj_t BgL_auxz00_4993;

							{	/* Cfa/cfa.scm 362 */
								BgL_objectz00_bglt BgL_tmpz00_4994;

								BgL_tmpz00_4994 =
									((BgL_objectz00_bglt)
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_4597));
								BgL_auxz00_4993 = BGL_OBJECT_WIDENING(BgL_tmpz00_4994);
							}
							BgL_auxz00_4992 =
								((BgL_boxzd2setz12zf2cinfoz32_bglt) BgL_auxz00_4993);
						}
						BgL_arg1874z00_4680 =
							(((BgL_boxzd2setz12zf2cinfoz32_bglt) COBJECT(BgL_auxz00_4992))->
							BgL_approxz00);
					}
					BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1874z00_4680,
						BgL_valzd2approxzd2_4678);
				}
			}
			{	/* Cfa/cfa.scm 363 */
				BgL_approxz00_bglt BgL_arg1876z00_4681;

				{	/* Cfa/cfa.scm 363 */
					BgL_nodez00_bglt BgL_arg1877z00_4682;

					BgL_arg1877z00_4682 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt)
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_4597))))->
						BgL_valuez00);
					BgL_arg1876z00_4681 = BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1877z00_4682);
				}
				BGl_loosez12z12zzcfa_loosez00(BgL_arg1876z00_4681, CNST_TABLE_REF(1));
			}
			{
				BgL_boxzd2setz12zf2cinfoz32_bglt BgL_auxz00_5007;

				{
					obj_t BgL_auxz00_5008;

					{	/* Cfa/cfa.scm 364 */
						BgL_objectz00_bglt BgL_tmpz00_5009;

						BgL_tmpz00_5009 =
							((BgL_objectz00_bglt)
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_4597));
						BgL_auxz00_5008 = BGL_OBJECT_WIDENING(BgL_tmpz00_5009);
					}
					BgL_auxz00_5007 =
						((BgL_boxzd2setz12zf2cinfoz32_bglt) BgL_auxz00_5008);
				}
				return
					(((BgL_boxzd2setz12zf2cinfoz32_bglt) COBJECT(BgL_auxz00_5007))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-make-box/Cinfo1665 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2makezd2boxzf2Cinfo1665z82zzcfa_cfaz00(obj_t
		BgL_envz00_4598, obj_t BgL_nodez00_4599)
	{
		{	/* Cfa/cfa.scm 348 */
			{	/* Cfa/cfa.scm 352 */
				BgL_approxz00_bglt BgL_arg1870z00_4684;

				{	/* Cfa/cfa.scm 352 */
					BgL_nodez00_bglt BgL_arg1872z00_4685;

					BgL_arg1872z00_4685 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt)
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_4599))))->BgL_valuez00);
					BgL_arg1870z00_4684 = BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1872z00_4685);
				}
				BGl_loosez12z12zzcfa_loosez00(BgL_arg1870z00_4684, CNST_TABLE_REF(1));
			}
			{
				BgL_makezd2boxzf2cinfoz20_bglt BgL_auxz00_5021;

				{
					obj_t BgL_auxz00_5022;

					{	/* Cfa/cfa.scm 353 */
						BgL_objectz00_bglt BgL_tmpz00_5023;

						BgL_tmpz00_5023 =
							((BgL_objectz00_bglt)
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_4599));
						BgL_auxz00_5022 = BGL_OBJECT_WIDENING(BgL_tmpz00_5023);
					}
					BgL_auxz00_5021 = ((BgL_makezd2boxzf2cinfoz20_bglt) BgL_auxz00_5022);
				}
				return
					(((BgL_makezd2boxzf2cinfoz20_bglt) COBJECT(BgL_auxz00_5021))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-jump-ex-it/Cinf1663 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2jumpzd2exzd2itzf2Cinf1663z50zzcfa_cfaz00(obj_t
		BgL_envz00_4600, obj_t BgL_nodez00_4601)
	{
		{	/* Cfa/cfa.scm 338 */
			{	/* Cfa/cfa.scm 340 */
				BgL_nodez00_bglt BgL_arg1868z00_4687;

				BgL_arg1868z00_4687 =
					(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_4601))))->
					BgL_exitz00);
				BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1868z00_4687);
			}
			{	/* Cfa/cfa.scm 341 */
				BgL_approxz00_bglt BgL_valzd2approxzd2_4688;

				{	/* Cfa/cfa.scm 341 */
					BgL_nodez00_bglt BgL_arg1869z00_4689;

					BgL_arg1869z00_4689 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt)
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_4601))))->
						BgL_valuez00);
					BgL_valzd2approxzd2_4688 =
						BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1869z00_4689);
				}
				BGl_loosez12z12zzcfa_loosez00(BgL_valzd2approxzd2_4688,
					CNST_TABLE_REF(1));
				{
					BgL_jumpzd2exzd2itzf2cinfozf2_bglt BgL_auxz00_5039;

					{
						obj_t BgL_auxz00_5040;

						{	/* Cfa/cfa.scm 343 */
							BgL_objectz00_bglt BgL_tmpz00_5041;

							BgL_tmpz00_5041 =
								((BgL_objectz00_bglt)
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_4601));
							BgL_auxz00_5040 = BGL_OBJECT_WIDENING(BgL_tmpz00_5041);
						}
						BgL_auxz00_5039 =
							((BgL_jumpzd2exzd2itzf2cinfozf2_bglt) BgL_auxz00_5040);
					}
					return
						(((BgL_jumpzd2exzd2itzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5039))->
						BgL_approxz00);
				}
			}
		}

	}



/* &cfa!-set-ex-it/Cinfo1661 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2setzd2exzd2itzf2Cinfo1661z50zzcfa_cfaz00(obj_t
		BgL_envz00_4602, obj_t BgL_nodez00_4603)
	{
		{	/* Cfa/cfa.scm 329 */
			{	/* Cfa/cfa.scm 331 */
				BgL_approxz00_bglt BgL_arg1862z00_4691;

				{	/* Cfa/cfa.scm 331 */
					BgL_nodez00_bglt BgL_arg1863z00_4692;

					BgL_arg1863z00_4692 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt)
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4603))))->
						BgL_bodyz00);
					BgL_arg1862z00_4691 = BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1863z00_4692);
				}
				BGl_loosez12z12zzcfa_loosez00(BgL_arg1862z00_4691, CNST_TABLE_REF(1));
			}
			{	/* Cfa/cfa.scm 332 */
				BgL_approxz00_bglt BgL_arg1864z00_4693;

				{	/* Cfa/cfa.scm 332 */
					BgL_nodez00_bglt BgL_arg1866z00_4694;

					BgL_arg1866z00_4694 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt)
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4603))))->
						BgL_onexitz00);
					BgL_arg1864z00_4693 = BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1866z00_4694);
				}
				BGl_loosez12z12zzcfa_loosez00(BgL_arg1864z00_4693, CNST_TABLE_REF(1));
			}
			{
				BgL_setzd2exzd2itzf2cinfozf2_bglt BgL_auxz00_5059;

				{
					obj_t BgL_auxz00_5060;

					{	/* Cfa/cfa.scm 333 */
						BgL_objectz00_bglt BgL_tmpz00_5061;

						BgL_tmpz00_5061 =
							((BgL_objectz00_bglt)
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4603));
						BgL_auxz00_5060 = BGL_OBJECT_WIDENING(BgL_tmpz00_5061);
					}
					BgL_auxz00_5059 =
						((BgL_setzd2exzd2itzf2cinfozf2_bglt) BgL_auxz00_5060);
				}
				return
					(((BgL_setzd2exzd2itzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5059))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-let-var1659 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2letzd2var1659z70zzcfa_cfaz00(obj_t
		BgL_envz00_4604, obj_t BgL_nodez00_4605)
	{
		{	/* Cfa/cfa.scm 288 */
			{	/* Cfa/cfa.scm 291 */
				obj_t BgL_g1598z00_4696;

				BgL_g1598z00_4696 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_4605)))->BgL_bindingsz00);
				{
					obj_t BgL_l1596z00_4698;

					BgL_l1596z00_4698 = BgL_g1598z00_4696;
				BgL_zc3z04anonymousza31854ze3z87_4697:
					if (PAIRP(BgL_l1596z00_4698))
						{	/* Cfa/cfa.scm 291 */
							{	/* Cfa/cfa.scm 292 */
								obj_t BgL_bindingz00_4699;

								BgL_bindingz00_4699 = CAR(BgL_l1596z00_4698);
								{	/* Cfa/cfa.scm 292 */
									obj_t BgL_varz00_4700;

									BgL_varz00_4700 = CAR(((obj_t) BgL_bindingz00_4699));
									{	/* Cfa/cfa.scm 292 */
										BgL_approxz00_bglt BgL_varzd2approxzd2_4701;

										{	/* Cfa/cfa.scm 293 */
											BgL_svarz00_bglt BgL_oz00_4702;

											BgL_oz00_4702 =
												((BgL_svarz00_bglt)
												(((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_varz00_4700)))->
													BgL_valuez00));
											{
												BgL_svarzf2cinfozf2_bglt BgL_auxz00_5077;

												{
													obj_t BgL_auxz00_5078;

													{	/* Cfa/cfa.scm 293 */
														BgL_objectz00_bglt BgL_tmpz00_5079;

														BgL_tmpz00_5079 =
															((BgL_objectz00_bglt) BgL_oz00_4702);
														BgL_auxz00_5078 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5079);
													}
													BgL_auxz00_5077 =
														((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_5078);
												}
												BgL_varzd2approxzd2_4701 =
													(((BgL_svarzf2cinfozf2_bglt)
														COBJECT(BgL_auxz00_5077))->BgL_approxz00);
											}
										}
										{	/* Cfa/cfa.scm 293 */
											BgL_approxz00_bglt BgL_valzd2approxzd2_4703;

											{	/* Cfa/cfa.scm 294 */
												obj_t BgL_arg1857z00_4704;

												BgL_arg1857z00_4704 =
													CDR(((obj_t) BgL_bindingz00_4699));
												BgL_valzd2approxzd2_4703 =
													BGl_cfaz12z12zzcfa_cfaz00(
													((BgL_nodez00_bglt) BgL_arg1857z00_4704));
											}
											{	/* Cfa/cfa.scm 294 */

												{	/* Cfa/cfa.scm 295 */
													BgL_typez00_bglt BgL_vtypez00_4705;
													BgL_typez00_bglt BgL_atypez00_4706;

													BgL_vtypez00_4705 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt) BgL_varz00_4700)))->
														BgL_typez00);
													BgL_atypez00_4706 =
														(((BgL_approxz00_bglt)
															COBJECT(BgL_valzd2approxzd2_4703))->BgL_typez00);
													BGl_unionzd2approxzd2filterz12z12zzcfa_approxz00
														(BgL_varzd2approxzd2_4701,
														BgL_valzd2approxzd2_4703);
													{	/* Cfa/cfa.scm 311 */
														bool_t BgL_test2189z00_5092;

														if (
															(((obj_t) BgL_vtypez00_4705) ==
																BGl_za2_za2z00zztype_cachez00))
															{	/* Cfa/cfa.scm 311 */
																BgL_test2189z00_5092 = ((bool_t) 0);
															}
														else
															{	/* Cfa/cfa.scm 311 */
																if (
																	(((obj_t) BgL_vtypez00_4705) ==
																		BGl_za2objza2z00zztype_cachez00))
																	{	/* Cfa/cfa.scm 312 */
																		BgL_test2189z00_5092 = ((bool_t) 0);
																	}
																else
																	{	/* Cfa/cfa.scm 312 */
																		if (
																			(((obj_t) BgL_atypez00_4706) ==
																				BGl_za2_za2z00zztype_cachez00))
																			{	/* Cfa/cfa.scm 313 */
																				BgL_test2189z00_5092 = ((bool_t) 0);
																			}
																		else
																			{	/* Cfa/cfa.scm 313 */
																				BgL_test2189z00_5092 =
																					(
																					((obj_t) BgL_atypez00_4706) ==
																					BGl_za2objza2z00zztype_cachez00);
																			}
																	}
															}
														if (BgL_test2189z00_5092)
															{	/* Cfa/cfa.scm 311 */
																BGl_approxzd2setzd2topz12z12zzcfa_approxz00
																	(BgL_valzd2approxzd2_4703);
																((obj_t)
																	BGl_loosez12z12zzcfa_loosez00
																	(BgL_valzd2approxzd2_4703,
																		CNST_TABLE_REF(1)));
															}
														else
															{	/* Cfa/cfa.scm 311 */
																BFALSE;
															}
													}
												}
											}
										}
									}
								}
							}
							{
								obj_t BgL_l1596z00_5108;

								BgL_l1596z00_5108 = CDR(BgL_l1596z00_4698);
								BgL_l1596z00_4698 = BgL_l1596z00_5108;
								goto BgL_zc3z04anonymousza31854ze3z87_4697;
							}
						}
					else
						{	/* Cfa/cfa.scm 291 */
							((bool_t) 1);
						}
				}
			}
			{	/* Cfa/cfa.scm 321 */
				BgL_approxz00_bglt BgL_approxz00_4707;

				BgL_approxz00_4707 =
					BGl_cfaz12z12zzcfa_cfaz00(
					(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_4605)))->BgL_bodyz00));
				return BgL_approxz00_4707;
			}
		}

	}



/* &cfa!-let-fun1657 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2letzd2fun1657z70zzcfa_cfaz00(obj_t
		BgL_envz00_4606, obj_t BgL_nodez00_4607)
	{
		{	/* Cfa/cfa.scm 281 */
			return
				BGl_cfaz12z12zzcfa_cfaz00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_4607)))->BgL_bodyz00));
		}

	}



/* &cfa!-switch/Cinfo1655 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2switchzf2Cinfo1655z50zzcfa_cfaz00(obj_t
		BgL_envz00_4608, obj_t BgL_nodez00_4609)
	{
		{	/* Cfa/cfa.scm 267 */
			{	/* Cfa/cfa.scm 269 */
				BgL_nodez00_bglt BgL_arg1846z00_4710;

				BgL_arg1846z00_4710 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt)
								((BgL_switchz00_bglt) BgL_nodez00_4609))))->BgL_testz00);
				BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1846z00_4710);
			}
			{	/* Cfa/cfa.scm 270 */
				BgL_approxz00_bglt BgL_reszd2approxzd2_4711;

				{
					BgL_switchzf2cinfozf2_bglt BgL_auxz00_5120;

					{
						obj_t BgL_auxz00_5121;

						{	/* Cfa/cfa.scm 270 */
							BgL_objectz00_bglt BgL_tmpz00_5122;

							BgL_tmpz00_5122 =
								((BgL_objectz00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_4609));
							BgL_auxz00_5121 = BGL_OBJECT_WIDENING(BgL_tmpz00_5122);
						}
						BgL_auxz00_5120 = ((BgL_switchzf2cinfozf2_bglt) BgL_auxz00_5121);
					}
					BgL_reszd2approxzd2_4711 =
						(((BgL_switchzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5120))->
						BgL_approxz00);
				}
				{
					obj_t BgL_clsz00_4713;

					{	/* Cfa/cfa.scm 271 */
						obj_t BgL_arg1847z00_4718;

						BgL_arg1847z00_4718 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt)
										((BgL_switchz00_bglt) BgL_nodez00_4609))))->BgL_clausesz00);
						BgL_clsz00_4713 = BgL_arg1847z00_4718;
					BgL_loopz00_4712:
						if (NULLP(BgL_clsz00_4713))
							{	/* Cfa/cfa.scm 272 */
								return BgL_reszd2approxzd2_4711;
							}
						else
							{	/* Cfa/cfa.scm 274 */
								BgL_approxz00_bglt BgL_newzd2approxzd2_4714;

								{	/* Cfa/cfa.scm 274 */
									obj_t BgL_arg1851z00_4715;

									{	/* Cfa/cfa.scm 274 */
										obj_t BgL_pairz00_4716;

										BgL_pairz00_4716 = CAR(((obj_t) BgL_clsz00_4713));
										BgL_arg1851z00_4715 = CDR(BgL_pairz00_4716);
									}
									BgL_newzd2approxzd2_4714 =
										BGl_cfaz12z12zzcfa_cfaz00(
										((BgL_nodez00_bglt) BgL_arg1851z00_4715));
								}
								BGl_unionzd2approxz12zc0zzcfa_approxz00
									(BgL_reszd2approxzd2_4711, BgL_newzd2approxzd2_4714);
								{	/* Cfa/cfa.scm 276 */
									obj_t BgL_arg1850z00_4717;

									BgL_arg1850z00_4717 = CDR(((obj_t) BgL_clsz00_4713));
									{
										obj_t BgL_clsz00_5141;

										BgL_clsz00_5141 = BgL_arg1850z00_4717;
										BgL_clsz00_4713 = BgL_clsz00_5141;
										goto BgL_loopz00_4712;
									}
								}
							}
					}
				}
			}
		}

	}



/* &cfa!-fail/Cinfo1653 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2failzf2Cinfo1653z50zzcfa_cfaz00(obj_t
		BgL_envz00_4610, obj_t BgL_nodez00_4611)
	{
		{	/* Cfa/cfa.scm 257 */
			{	/* Cfa/cfa.scm 259 */
				BgL_approxz00_bglt BgL_arg1839z00_4720;

				{	/* Cfa/cfa.scm 259 */
					BgL_nodez00_bglt BgL_arg1840z00_4721;

					BgL_arg1840z00_4721 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt)
									((BgL_failz00_bglt) BgL_nodez00_4611))))->BgL_procz00);
					BgL_arg1839z00_4720 = BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1840z00_4721);
				}
				BGl_loosez12z12zzcfa_loosez00(BgL_arg1839z00_4720, CNST_TABLE_REF(1));
			}
			{	/* Cfa/cfa.scm 260 */
				BgL_approxz00_bglt BgL_arg1842z00_4722;

				{	/* Cfa/cfa.scm 260 */
					BgL_nodez00_bglt BgL_arg1843z00_4723;

					BgL_arg1843z00_4723 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt)
									((BgL_failz00_bglt) BgL_nodez00_4611))))->BgL_msgz00);
					BgL_arg1842z00_4722 = BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1843z00_4723);
				}
				BGl_loosez12z12zzcfa_loosez00(BgL_arg1842z00_4722, CNST_TABLE_REF(1));
			}
			{	/* Cfa/cfa.scm 261 */
				BgL_approxz00_bglt BgL_arg1844z00_4724;

				{	/* Cfa/cfa.scm 261 */
					BgL_nodez00_bglt BgL_arg1845z00_4725;

					BgL_arg1845z00_4725 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt)
									((BgL_failz00_bglt) BgL_nodez00_4611))))->BgL_objz00);
					BgL_arg1844z00_4724 = BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1845z00_4725);
				}
				BGl_loosez12z12zzcfa_loosez00(BgL_arg1844z00_4724, CNST_TABLE_REF(1));
			}
			{
				BgL_failzf2cinfozf2_bglt BgL_auxz00_5160;

				{
					obj_t BgL_auxz00_5161;

					{	/* Cfa/cfa.scm 262 */
						BgL_objectz00_bglt BgL_tmpz00_5162;

						BgL_tmpz00_5162 =
							((BgL_objectz00_bglt) ((BgL_failz00_bglt) BgL_nodez00_4611));
						BgL_auxz00_5161 = BGL_OBJECT_WIDENING(BgL_tmpz00_5162);
					}
					BgL_auxz00_5160 = ((BgL_failzf2cinfozf2_bglt) BgL_auxz00_5161);
				}
				return
					(((BgL_failzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5160))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-conditional/Cin1651 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2conditionalzf2Cin1651z50zzcfa_cfaz00(obj_t
		BgL_envz00_4612, obj_t BgL_nodez00_4613)
	{
		{	/* Cfa/cfa.scm 245 */
			{	/* Cfa/cfa.scm 247 */
				BgL_nodez00_bglt BgL_arg1834z00_4727;

				BgL_arg1834z00_4727 =
					(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt)
								((BgL_conditionalz00_bglt) BgL_nodez00_4613))))->BgL_testz00);
				BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1834z00_4727);
			}
			{	/* Cfa/cfa.scm 248 */
				BgL_approxz00_bglt BgL_thenzd2approxzd2_4728;
				BgL_approxz00_bglt BgL_elsezd2approxzd2_4729;

				{	/* Cfa/cfa.scm 248 */
					BgL_nodez00_bglt BgL_arg1837z00_4730;

					BgL_arg1837z00_4730 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt)
									((BgL_conditionalz00_bglt) BgL_nodez00_4613))))->BgL_truez00);
					BgL_thenzd2approxzd2_4728 =
						BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1837z00_4730);
				}
				{	/* Cfa/cfa.scm 249 */
					BgL_nodez00_bglt BgL_arg1838z00_4731;

					BgL_arg1838z00_4731 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt)
									((BgL_conditionalz00_bglt) BgL_nodez00_4613))))->
						BgL_falsez00);
					BgL_elsezd2approxzd2_4729 =
						BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1838z00_4731);
				}
				{	/* Cfa/cfa.scm 250 */
					BgL_approxz00_bglt BgL_arg1835z00_4732;

					{
						BgL_conditionalzf2cinfozf2_bglt BgL_auxz00_5180;

						{
							obj_t BgL_auxz00_5181;

							{	/* Cfa/cfa.scm 250 */
								BgL_objectz00_bglt BgL_tmpz00_5182;

								BgL_tmpz00_5182 =
									((BgL_objectz00_bglt)
									((BgL_conditionalz00_bglt) BgL_nodez00_4613));
								BgL_auxz00_5181 = BGL_OBJECT_WIDENING(BgL_tmpz00_5182);
							}
							BgL_auxz00_5180 =
								((BgL_conditionalzf2cinfozf2_bglt) BgL_auxz00_5181);
						}
						BgL_arg1835z00_4732 =
							(((BgL_conditionalzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5180))->
							BgL_approxz00);
					}
					BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1835z00_4732,
						BgL_thenzd2approxzd2_4728);
				}
				{	/* Cfa/cfa.scm 251 */
					BgL_approxz00_bglt BgL_arg1836z00_4733;

					{
						BgL_conditionalzf2cinfozf2_bglt BgL_auxz00_5189;

						{
							obj_t BgL_auxz00_5190;

							{	/* Cfa/cfa.scm 251 */
								BgL_objectz00_bglt BgL_tmpz00_5191;

								BgL_tmpz00_5191 =
									((BgL_objectz00_bglt)
									((BgL_conditionalz00_bglt) BgL_nodez00_4613));
								BgL_auxz00_5190 = BGL_OBJECT_WIDENING(BgL_tmpz00_5191);
							}
							BgL_auxz00_5189 =
								((BgL_conditionalzf2cinfozf2_bglt) BgL_auxz00_5190);
						}
						BgL_arg1836z00_4733 =
							(((BgL_conditionalzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5189))->
							BgL_approxz00);
					}
					BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1836z00_4733,
						BgL_elsezd2approxzd2_4729);
				}
				{
					BgL_conditionalzf2cinfozf2_bglt BgL_auxz00_5198;

					{
						obj_t BgL_auxz00_5199;

						{	/* Cfa/cfa.scm 252 */
							BgL_objectz00_bglt BgL_tmpz00_5200;

							BgL_tmpz00_5200 =
								((BgL_objectz00_bglt)
								((BgL_conditionalz00_bglt) BgL_nodez00_4613));
							BgL_auxz00_5199 = BGL_OBJECT_WIDENING(BgL_tmpz00_5200);
						}
						BgL_auxz00_5198 =
							((BgL_conditionalzf2cinfozf2_bglt) BgL_auxz00_5199);
					}
					return
						(((BgL_conditionalzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5198))->
						BgL_approxz00);
				}
			}
		}

	}



/* &cfa!-setq/Cinfo1649 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2setqzf2Cinfo1649z50zzcfa_cfaz00(obj_t
		BgL_envz00_4614, obj_t BgL_nodez00_4615)
	{
		{	/* Cfa/cfa.scm 227 */
			{	/* Cfa/cfa.scm 229 */
				BgL_approxz00_bglt BgL_varzd2approxzd2_4735;

				{	/* Cfa/cfa.scm 229 */
					BgL_varz00_bglt BgL_arg1833z00_4736;

					BgL_arg1833z00_4736 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt)
									((BgL_setqz00_bglt) BgL_nodez00_4615))))->BgL_varz00);
					BgL_varzd2approxzd2_4735 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1833z00_4736));
				}
				{	/* Cfa/cfa.scm 229 */
					BgL_approxz00_bglt BgL_valzd2approxzd2_4737;

					{	/* Cfa/cfa.scm 230 */
						BgL_nodez00_bglt BgL_arg1832z00_4738;

						BgL_arg1832z00_4738 =
							(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt)
										((BgL_setqz00_bglt) BgL_nodez00_4615))))->BgL_valuez00);
						BgL_valzd2approxzd2_4737 =
							BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1832z00_4738);
					}
					{	/* Cfa/cfa.scm 230 */
						BgL_variablez00_bglt BgL_vz00_4739;

						BgL_vz00_4739 =
							(((BgL_varz00_bglt) COBJECT(
									(((BgL_setqz00_bglt) COBJECT(
												((BgL_setqz00_bglt)
													((BgL_setqz00_bglt) BgL_nodez00_4615))))->
										BgL_varz00)))->BgL_variablez00);
						{	/* Cfa/cfa.scm 231 */

							BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_varzd2approxzd2_4735,
								BgL_valzd2approxzd2_4737);
							{	/* Cfa/cfa.scm 236 */
								bool_t BgL_test2194z00_5220;

								{	/* Cfa/cfa.scm 236 */
									bool_t BgL_test2195z00_5221;

									{	/* Cfa/cfa.scm 236 */
										obj_t BgL_classz00_4740;

										BgL_classz00_4740 = BGl_globalz00zzast_varz00;
										{	/* Cfa/cfa.scm 236 */
											BgL_objectz00_bglt BgL_arg1807z00_4741;

											{	/* Cfa/cfa.scm 236 */
												obj_t BgL_tmpz00_5222;

												BgL_tmpz00_5222 =
													((obj_t) ((BgL_objectz00_bglt) BgL_vz00_4739));
												BgL_arg1807z00_4741 =
													(BgL_objectz00_bglt) (BgL_tmpz00_5222);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Cfa/cfa.scm 236 */
													long BgL_idxz00_4742;

													BgL_idxz00_4742 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4741);
													BgL_test2195z00_5221 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_4742 + 2L)) == BgL_classz00_4740);
												}
											else
												{	/* Cfa/cfa.scm 236 */
													bool_t BgL_res2093z00_4745;

													{	/* Cfa/cfa.scm 236 */
														obj_t BgL_oclassz00_4746;

														{	/* Cfa/cfa.scm 236 */
															obj_t BgL_arg1815z00_4747;
															long BgL_arg1816z00_4748;

															BgL_arg1815z00_4747 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Cfa/cfa.scm 236 */
																long BgL_arg1817z00_4749;

																BgL_arg1817z00_4749 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4741);
																BgL_arg1816z00_4748 =
																	(BgL_arg1817z00_4749 - OBJECT_TYPE);
															}
															BgL_oclassz00_4746 =
																VECTOR_REF(BgL_arg1815z00_4747,
																BgL_arg1816z00_4748);
														}
														{	/* Cfa/cfa.scm 236 */
															bool_t BgL__ortest_1115z00_4750;

															BgL__ortest_1115z00_4750 =
																(BgL_classz00_4740 == BgL_oclassz00_4746);
															if (BgL__ortest_1115z00_4750)
																{	/* Cfa/cfa.scm 236 */
																	BgL_res2093z00_4745 =
																		BgL__ortest_1115z00_4750;
																}
															else
																{	/* Cfa/cfa.scm 236 */
																	long BgL_odepthz00_4751;

																	{	/* Cfa/cfa.scm 236 */
																		obj_t BgL_arg1804z00_4752;

																		BgL_arg1804z00_4752 = (BgL_oclassz00_4746);
																		BgL_odepthz00_4751 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_4752);
																	}
																	if ((2L < BgL_odepthz00_4751))
																		{	/* Cfa/cfa.scm 236 */
																			obj_t BgL_arg1802z00_4753;

																			{	/* Cfa/cfa.scm 236 */
																				obj_t BgL_arg1803z00_4754;

																				BgL_arg1803z00_4754 =
																					(BgL_oclassz00_4746);
																				BgL_arg1802z00_4753 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_4754, 2L);
																			}
																			BgL_res2093z00_4745 =
																				(BgL_arg1802z00_4753 ==
																				BgL_classz00_4740);
																		}
																	else
																		{	/* Cfa/cfa.scm 236 */
																			BgL_res2093z00_4745 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2195z00_5221 = BgL_res2093z00_4745;
												}
										}
									}
									if (BgL_test2195z00_5221)
										{	/* Cfa/cfa.scm 237 */
											bool_t BgL__ortest_1198z00_4755;

											if (CBOOL(
													(((BgL_globalz00_bglt) COBJECT(
																((BgL_globalz00_bglt) BgL_vz00_4739)))->
														BgL_initz00)))
												{	/* Cfa/cfa.scm 237 */
													BgL__ortest_1198z00_4755 = ((bool_t) 0);
												}
											else
												{	/* Cfa/cfa.scm 237 */
													BgL__ortest_1198z00_4755 = ((bool_t) 1);
												}
											if (BgL__ortest_1198z00_4755)
												{	/* Cfa/cfa.scm 237 */
													BgL_test2194z00_5220 = BgL__ortest_1198z00_4755;
												}
											else
												{	/* Cfa/cfa.scm 237 */
													if (
														((((BgL_globalz00_bglt) COBJECT(
																		((BgL_globalz00_bglt) BgL_vz00_4739)))->
																BgL_importz00) == CNST_TABLE_REF(2)))
														{	/* Cfa/cfa.scm 238 */
															BgL_test2194z00_5220 = ((bool_t) 0);
														}
													else
														{	/* Cfa/cfa.scm 238 */
															BgL_test2194z00_5220 = ((bool_t) 1);
														}
												}
										}
									else
										{	/* Cfa/cfa.scm 236 */
											BgL_test2194z00_5220 = ((bool_t) 0);
										}
								}
								if (BgL_test2194z00_5220)
									{	/* Cfa/cfa.scm 236 */
										BGl_globalzd2loosez12zc0zzcfa_loosez00(
											((BgL_globalz00_bglt) BgL_vz00_4739),
											BgL_varzd2approxzd2_4735);
									}
								else
									{	/* Cfa/cfa.scm 236 */
										BFALSE;
									}
							}
							{
								BgL_setqzf2cinfozf2_bglt BgL_auxz00_5257;

								{
									obj_t BgL_auxz00_5258;

									{	/* Cfa/cfa.scm 240 */
										BgL_objectz00_bglt BgL_tmpz00_5259;

										BgL_tmpz00_5259 =
											((BgL_objectz00_bglt)
											((BgL_setqz00_bglt) BgL_nodez00_4615));
										BgL_auxz00_5258 = BGL_OBJECT_WIDENING(BgL_tmpz00_5259);
									}
									BgL_auxz00_5257 =
										((BgL_setqzf2cinfozf2_bglt) BgL_auxz00_5258);
								}
								return
									(((BgL_setqzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5257))->
									BgL_approxz00);
							}
						}
					}
				}
			}
		}

	}



/* &cfa!-cast1647 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2cast1647za2zzcfa_cfaz00(obj_t
		BgL_envz00_4616, obj_t BgL_nodez00_4617)
	{
		{	/* Cfa/cfa.scm 220 */
			return
				BGl_cfaz12z12zzcfa_cfaz00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_4617)))->BgL_argz00));
		}

	}



/* &cfa!-cast-null/Cinfo1645 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2castzd2nullzf2Cinfo1645z82zzcfa_cfaz00(obj_t
		BgL_envz00_4618, obj_t BgL_nodez00_4619)
	{
		{	/* Cfa/cfa.scm 213 */
			{
				BgL_castzd2nullzf2cinfoz20_bglt BgL_auxz00_5268;

				{
					obj_t BgL_auxz00_5269;

					{	/* Cfa/cfa.scm 214 */
						BgL_objectz00_bglt BgL_tmpz00_5270;

						BgL_tmpz00_5270 =
							((BgL_objectz00_bglt)
							((BgL_castzd2nullzd2_bglt) BgL_nodez00_4619));
						BgL_auxz00_5269 = BGL_OBJECT_WIDENING(BgL_tmpz00_5270);
					}
					BgL_auxz00_5268 = ((BgL_castzd2nullzf2cinfoz20_bglt) BgL_auxz00_5269);
				}
				return
					(((BgL_castzd2nullzf2cinfoz20_bglt) COBJECT(BgL_auxz00_5268))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-instanceof/Cinf1643 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2instanceofzf2Cinf1643z50zzcfa_cfaz00(obj_t
		BgL_envz00_4620, obj_t BgL_nodez00_4621)
	{
		{	/* Cfa/cfa.scm 206 */
			{
				BgL_instanceofzf2cinfozf2_bglt BgL_auxz00_5276;

				{
					obj_t BgL_auxz00_5277;

					{	/* Cfa/cfa.scm 207 */
						BgL_objectz00_bglt BgL_tmpz00_5278;

						BgL_tmpz00_5278 =
							((BgL_objectz00_bglt)
							((BgL_instanceofz00_bglt) BgL_nodez00_4621));
						BgL_auxz00_5277 = BGL_OBJECT_WIDENING(BgL_tmpz00_5278);
					}
					BgL_auxz00_5276 = ((BgL_instanceofzf2cinfozf2_bglt) BgL_auxz00_5277);
				}
				return
					(((BgL_instanceofzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5276))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-new/Cinfo1641 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2newzf2Cinfo1641z50zzcfa_cfaz00(obj_t
		BgL_envz00_4622, obj_t BgL_nodez00_4623)
	{
		{	/* Cfa/cfa.scm 198 */
			{	/* Cfa/cfa.scm 200 */
				obj_t BgL_g1595z00_4760;

				BgL_g1595z00_4760 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_newz00_bglt) BgL_nodez00_4623))))->BgL_exprza2za2);
				{
					obj_t BgL_l1593z00_4762;

					BgL_l1593z00_4762 = BgL_g1595z00_4760;
				BgL_zc3z04anonymousza31799ze3z87_4761:
					if (PAIRP(BgL_l1593z00_4762))
						{	/* Cfa/cfa.scm 200 */
							{	/* Cfa/cfa.scm 200 */
								obj_t BgL_az00_4763;

								BgL_az00_4763 = CAR(BgL_l1593z00_4762);
								{	/* Cfa/cfa.scm 200 */
									BgL_approxz00_bglt BgL_arg1805z00_4764;

									BgL_arg1805z00_4764 =
										BGl_cfaz12z12zzcfa_cfaz00(
										((BgL_nodez00_bglt) BgL_az00_4763));
									BGl_loosez12z12zzcfa_loosez00(BgL_arg1805z00_4764,
										CNST_TABLE_REF(1));
								}
							}
							{
								obj_t BgL_l1593z00_5294;

								BgL_l1593z00_5294 = CDR(BgL_l1593z00_4762);
								BgL_l1593z00_4762 = BgL_l1593z00_5294;
								goto BgL_zc3z04anonymousza31799ze3z87_4761;
							}
						}
					else
						{	/* Cfa/cfa.scm 200 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_newzf2cinfozf2_bglt BgL_auxz00_5296;

				{
					obj_t BgL_auxz00_5297;

					{	/* Cfa/cfa.scm 201 */
						BgL_objectz00_bglt BgL_tmpz00_5298;

						BgL_tmpz00_5298 =
							((BgL_objectz00_bglt) ((BgL_newz00_bglt) BgL_nodez00_4623));
						BgL_auxz00_5297 = BGL_OBJECT_WIDENING(BgL_tmpz00_5298);
					}
					BgL_auxz00_5296 = ((BgL_newzf2cinfozf2_bglt) BgL_auxz00_5297);
				}
				return
					(((BgL_newzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5296))->BgL_approxz00);
			}
		}

	}



/* &cfa!-setfield/Cinfo1639 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2setfieldzf2Cinfo1639z50zzcfa_cfaz00(obj_t
		BgL_envz00_4624, obj_t BgL_nodez00_4625)
	{
		{	/* Cfa/cfa.scm 190 */
			{	/* Cfa/cfa.scm 192 */
				obj_t BgL_g1592z00_4766;

				BgL_g1592z00_4766 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_setfieldz00_bglt) BgL_nodez00_4625))))->BgL_exprza2za2);
				{
					obj_t BgL_l1590z00_4768;

					BgL_l1590z00_4768 = BgL_g1592z00_4766;
				BgL_zc3z04anonymousza31772ze3z87_4767:
					if (PAIRP(BgL_l1590z00_4768))
						{	/* Cfa/cfa.scm 192 */
							{	/* Cfa/cfa.scm 192 */
								obj_t BgL_az00_4769;

								BgL_az00_4769 = CAR(BgL_l1590z00_4768);
								{	/* Cfa/cfa.scm 192 */
									BgL_approxz00_bglt BgL_arg1775z00_4770;

									BgL_arg1775z00_4770 =
										BGl_cfaz12z12zzcfa_cfaz00(
										((BgL_nodez00_bglt) BgL_az00_4769));
									BGl_loosez12z12zzcfa_loosez00(BgL_arg1775z00_4770,
										CNST_TABLE_REF(1));
								}
							}
							{
								obj_t BgL_l1590z00_5314;

								BgL_l1590z00_5314 = CDR(BgL_l1590z00_4768);
								BgL_l1590z00_4768 = BgL_l1590z00_5314;
								goto BgL_zc3z04anonymousza31772ze3z87_4767;
							}
						}
					else
						{	/* Cfa/cfa.scm 192 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_setfieldzf2cinfozf2_bglt BgL_auxz00_5316;

				{
					obj_t BgL_auxz00_5317;

					{	/* Cfa/cfa.scm 193 */
						BgL_objectz00_bglt BgL_tmpz00_5318;

						BgL_tmpz00_5318 =
							((BgL_objectz00_bglt) ((BgL_setfieldz00_bglt) BgL_nodez00_4625));
						BgL_auxz00_5317 = BGL_OBJECT_WIDENING(BgL_tmpz00_5318);
					}
					BgL_auxz00_5316 = ((BgL_setfieldzf2cinfozf2_bglt) BgL_auxz00_5317);
				}
				return
					(((BgL_setfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5316))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-getfield/Cinfo1637 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2getfieldzf2Cinfo1637z50zzcfa_cfaz00(obj_t
		BgL_envz00_4626, obj_t BgL_nodez00_4627)
	{
		{	/* Cfa/cfa.scm 182 */
			{	/* Cfa/cfa.scm 184 */
				obj_t BgL_g1588z00_4772;

				BgL_g1588z00_4772 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_getfieldz00_bglt) BgL_nodez00_4627))))->BgL_exprza2za2);
				{
					obj_t BgL_l1586z00_4774;

					BgL_l1586z00_4774 = BgL_g1588z00_4772;
				BgL_zc3z04anonymousza31768ze3z87_4773:
					if (PAIRP(BgL_l1586z00_4774))
						{	/* Cfa/cfa.scm 184 */
							{	/* Cfa/cfa.scm 184 */
								obj_t BgL_az00_4775;

								BgL_az00_4775 = CAR(BgL_l1586z00_4774);
								{	/* Cfa/cfa.scm 184 */
									BgL_approxz00_bglt BgL_arg1770z00_4776;

									BgL_arg1770z00_4776 =
										BGl_cfaz12z12zzcfa_cfaz00(
										((BgL_nodez00_bglt) BgL_az00_4775));
									BGl_loosez12z12zzcfa_loosez00(BgL_arg1770z00_4776,
										CNST_TABLE_REF(1));
								}
							}
							{
								obj_t BgL_l1586z00_5334;

								BgL_l1586z00_5334 = CDR(BgL_l1586z00_4774);
								BgL_l1586z00_4774 = BgL_l1586z00_5334;
								goto BgL_zc3z04anonymousza31768ze3z87_4773;
							}
						}
					else
						{	/* Cfa/cfa.scm 184 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_getfieldzf2cinfozf2_bglt BgL_auxz00_5336;

				{
					obj_t BgL_auxz00_5337;

					{	/* Cfa/cfa.scm 185 */
						BgL_objectz00_bglt BgL_tmpz00_5338;

						BgL_tmpz00_5338 =
							((BgL_objectz00_bglt) ((BgL_getfieldz00_bglt) BgL_nodez00_4627));
						BgL_auxz00_5337 = BGL_OBJECT_WIDENING(BgL_tmpz00_5338);
					}
					BgL_auxz00_5336 = ((BgL_getfieldzf2cinfozf2_bglt) BgL_auxz00_5337);
				}
				return
					(((BgL_getfieldzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5336))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-genpatchid/Cinf1635 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2genpatchidzf2Cinf1635z50zzcfa_cfaz00(obj_t
		BgL_envz00_4628, obj_t BgL_nodez00_4629)
	{
		{	/* Cfa/cfa.scm 174 */
			{	/* Cfa/cfa.scm 176 */
				obj_t BgL_g1585z00_4778;

				BgL_g1585z00_4778 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_genpatchidz00_bglt) BgL_nodez00_4629))))->BgL_exprza2za2);
				{
					obj_t BgL_l1583z00_4780;

					BgL_l1583z00_4780 = BgL_g1585z00_4778;
				BgL_zc3z04anonymousza31762ze3z87_4779:
					if (PAIRP(BgL_l1583z00_4780))
						{	/* Cfa/cfa.scm 176 */
							{	/* Cfa/cfa.scm 176 */
								obj_t BgL_az00_4781;

								BgL_az00_4781 = CAR(BgL_l1583z00_4780);
								{	/* Cfa/cfa.scm 176 */
									BgL_approxz00_bglt BgL_arg1765z00_4782;

									BgL_arg1765z00_4782 =
										BGl_cfaz12z12zzcfa_cfaz00(
										((BgL_nodez00_bglt) BgL_az00_4781));
									BGl_loosez12z12zzcfa_loosez00(BgL_arg1765z00_4782,
										CNST_TABLE_REF(1));
								}
							}
							{
								obj_t BgL_l1583z00_5354;

								BgL_l1583z00_5354 = CDR(BgL_l1583z00_4780);
								BgL_l1583z00_4780 = BgL_l1583z00_5354;
								goto BgL_zc3z04anonymousza31762ze3z87_4779;
							}
						}
					else
						{	/* Cfa/cfa.scm 176 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_genpatchidzf2cinfozf2_bglt BgL_auxz00_5356;

				{
					obj_t BgL_auxz00_5357;

					{	/* Cfa/cfa.scm 177 */
						BgL_objectz00_bglt BgL_tmpz00_5358;

						BgL_tmpz00_5358 =
							((BgL_objectz00_bglt)
							((BgL_genpatchidz00_bglt) BgL_nodez00_4629));
						BgL_auxz00_5357 = BGL_OBJECT_WIDENING(BgL_tmpz00_5358);
					}
					BgL_auxz00_5356 = ((BgL_genpatchidzf2cinfozf2_bglt) BgL_auxz00_5357);
				}
				return
					(((BgL_genpatchidzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5356))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-pragma/Cinfo1633 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2pragmazf2Cinfo1633z50zzcfa_cfaz00(obj_t
		BgL_envz00_4630, obj_t BgL_nodez00_4631)
	{
		{	/* Cfa/cfa.scm 166 */
			{	/* Cfa/cfa.scm 168 */
				obj_t BgL_g1582z00_4784;

				BgL_g1582z00_4784 =
					(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt)
								((BgL_pragmaz00_bglt) BgL_nodez00_4631))))->BgL_exprza2za2);
				{
					obj_t BgL_l1580z00_4786;

					BgL_l1580z00_4786 = BgL_g1582z00_4784;
				BgL_zc3z04anonymousza31753ze3z87_4785:
					if (PAIRP(BgL_l1580z00_4786))
						{	/* Cfa/cfa.scm 168 */
							{	/* Cfa/cfa.scm 168 */
								obj_t BgL_az00_4787;

								BgL_az00_4787 = CAR(BgL_l1580z00_4786);
								{	/* Cfa/cfa.scm 168 */
									BgL_approxz00_bglt BgL_arg1755z00_4788;

									BgL_arg1755z00_4788 =
										BGl_cfaz12z12zzcfa_cfaz00(
										((BgL_nodez00_bglt) BgL_az00_4787));
									BGl_loosez12z12zzcfa_loosez00(BgL_arg1755z00_4788,
										CNST_TABLE_REF(1));
								}
							}
							{
								obj_t BgL_l1580z00_5374;

								BgL_l1580z00_5374 = CDR(BgL_l1580z00_4786);
								BgL_l1580z00_4786 = BgL_l1580z00_5374;
								goto BgL_zc3z04anonymousza31753ze3z87_4785;
							}
						}
					else
						{	/* Cfa/cfa.scm 168 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_pragmazf2cinfozf2_bglt BgL_auxz00_5376;

				{
					obj_t BgL_auxz00_5377;

					{	/* Cfa/cfa.scm 169 */
						BgL_objectz00_bglt BgL_tmpz00_5378;

						BgL_tmpz00_5378 =
							((BgL_objectz00_bglt) ((BgL_pragmaz00_bglt) BgL_nodez00_4631));
						BgL_auxz00_5377 = BGL_OBJECT_WIDENING(BgL_tmpz00_5378);
					}
					BgL_auxz00_5376 = ((BgL_pragmazf2cinfozf2_bglt) BgL_auxz00_5377);
				}
				return
					(((BgL_pragmazf2cinfozf2_bglt) COBJECT(BgL_auxz00_5376))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-app-ly/Cinfo1631 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2appzd2lyzf2Cinfo1631z82zzcfa_cfaz00(obj_t
		BgL_envz00_4632, obj_t BgL_nodez00_4633)
	{
		{	/* Cfa/cfa.scm 157 */
			{	/* Cfa/cfa.scm 159 */
				BgL_approxz00_bglt BgL_arg1749z00_4790;

				{	/* Cfa/cfa.scm 159 */
					BgL_nodez00_bglt BgL_arg1750z00_4791;

					BgL_arg1750z00_4791 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt)
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_4633))))->BgL_argz00);
					BgL_arg1749z00_4790 = BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1750z00_4791);
				}
				BGl_loosez12z12zzcfa_loosez00(BgL_arg1749z00_4790, CNST_TABLE_REF(1));
			}
			{	/* Cfa/cfa.scm 160 */
				BgL_approxz00_bglt BgL_arg1751z00_4792;

				{	/* Cfa/cfa.scm 160 */
					BgL_nodez00_bglt BgL_arg1752z00_4793;

					BgL_arg1752z00_4793 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt)
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_4633))))->BgL_funz00);
					BgL_arg1751z00_4792 = BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1752z00_4793);
				}
				BGl_loosez12z12zzcfa_loosez00(BgL_arg1751z00_4792, CNST_TABLE_REF(1));
			}
			{
				BgL_appzd2lyzf2cinfoz20_bglt BgL_auxz00_5396;

				{
					obj_t BgL_auxz00_5397;

					{	/* Cfa/cfa.scm 161 */
						BgL_objectz00_bglt BgL_tmpz00_5398;

						BgL_tmpz00_5398 =
							((BgL_objectz00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_4633));
						BgL_auxz00_5397 = BGL_OBJECT_WIDENING(BgL_tmpz00_5398);
					}
					BgL_auxz00_5396 = ((BgL_appzd2lyzf2cinfoz20_bglt) BgL_auxz00_5397);
				}
				return
					(((BgL_appzd2lyzf2cinfoz20_bglt) COBJECT(BgL_auxz00_5396))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-sync1628 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2sync1628za2zzcfa_cfaz00(obj_t
		BgL_envz00_4634, obj_t BgL_nodez00_4635)
	{
		{	/* Cfa/cfa.scm 148 */
			BGl_cfaz12z12zzcfa_cfaz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_4635)))->BgL_prelockz00));
			BGl_cfaz12z12zzcfa_cfaz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_4635)))->BgL_mutexz00));
			return
				BGl_cfaz12z12zzcfa_cfaz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_4635)))->BgL_bodyz00));
		}

	}



/* &cfa!-sequence1626 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2sequence1626za2zzcfa_cfaz00(obj_t
		BgL_envz00_4636, obj_t BgL_nodez00_4637)
	{
		{	/* Cfa/cfa.scm 137 */
			{	/* Cfa/cfa.scm 139 */
				obj_t BgL_arg1735z00_4796;

				BgL_arg1735z00_4796 =
					(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_4637)))->BgL_nodesz00);
				{
					obj_t BgL_nz00_4798;
					obj_t BgL_approxz00_4799;

					{
						obj_t BgL_auxz00_5415;

						BgL_nz00_4798 = BgL_arg1735z00_4796;
						BgL_approxz00_4799 = BUNSPEC;
					BgL_loopz00_4797:
						if (NULLP(BgL_nz00_4798))
							{	/* Cfa/cfa.scm 141 */
								BgL_auxz00_5415 = BgL_approxz00_4799;
							}
						else
							{	/* Cfa/cfa.scm 143 */
								obj_t BgL_arg1738z00_4800;
								BgL_approxz00_bglt BgL_arg1739z00_4801;

								BgL_arg1738z00_4800 = CDR(((obj_t) BgL_nz00_4798));
								{	/* Cfa/cfa.scm 143 */
									obj_t BgL_arg1740z00_4802;

									BgL_arg1740z00_4802 = CAR(((obj_t) BgL_nz00_4798));
									BgL_arg1739z00_4801 =
										BGl_cfaz12z12zzcfa_cfaz00(
										((BgL_nodez00_bglt) BgL_arg1740z00_4802));
								}
								{
									obj_t BgL_approxz00_5425;
									obj_t BgL_nz00_5424;

									BgL_nz00_5424 = BgL_arg1738z00_4800;
									BgL_approxz00_5425 = ((obj_t) BgL_arg1739z00_4801);
									BgL_approxz00_4799 = BgL_approxz00_5425;
									BgL_nz00_4798 = BgL_nz00_5424;
									goto BgL_loopz00_4797;
								}
							}
						return ((BgL_approxz00_bglt) BgL_auxz00_5415);
					}
				}
			}
		}

	}



/* &cfa-variable-value-a1624 */
	obj_t BGl_z62cfazd2variablezd2valuezd2a1624zb0zzcfa_cfaz00(obj_t
		BgL_envz00_4638, obj_t BgL_valuez00_4639)
	{
		{	/* Cfa/cfa.scm 130 */
			{
				BgL_approxz00_bglt BgL_auxz00_5428;

				{
					BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_5429;

					{
						obj_t BgL_auxz00_5430;

						{	/* Cfa/cfa.scm 131 */
							BgL_objectz00_bglt BgL_tmpz00_5431;

							BgL_tmpz00_5431 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_valuez00_4639));
							BgL_auxz00_5430 = BGL_OBJECT_WIDENING(BgL_tmpz00_5431);
						}
						BgL_auxz00_5429 =
							((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_5430);
					}
					BgL_auxz00_5428 =
						(((BgL_internzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_5429))->
						BgL_approxz00);
				}
				return ((obj_t) BgL_auxz00_5428);
			}
		}

	}



/* &cfa-variable-value-a1622 */
	obj_t BGl_z62cfazd2variablezd2valuezd2a1622zb0zzcfa_cfaz00(obj_t
		BgL_envz00_4640, obj_t BgL_valuez00_4641)
	{
		{	/* Cfa/cfa.scm 119 */
			{
				BgL_approxz00_bglt BgL_auxz00_5438;

				{
					BgL_sexitzf2cinfozf2_bglt BgL_auxz00_5439;

					{
						obj_t BgL_auxz00_5440;

						{	/* Cfa/cfa.scm 120 */
							BgL_objectz00_bglt BgL_tmpz00_5441;

							BgL_tmpz00_5441 =
								((BgL_objectz00_bglt) ((BgL_sexitz00_bglt) BgL_valuez00_4641));
							BgL_auxz00_5440 = BGL_OBJECT_WIDENING(BgL_tmpz00_5441);
						}
						BgL_auxz00_5439 = ((BgL_sexitzf2cinfozf2_bglt) BgL_auxz00_5440);
					}
					BgL_auxz00_5438 =
						(((BgL_sexitzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5439))->
						BgL_approxz00);
				}
				return ((obj_t) BgL_auxz00_5438);
			}
		}

	}



/* &cfa-variable-value-a1620 */
	obj_t BGl_z62cfazd2variablezd2valuezd2a1620zb0zzcfa_cfaz00(obj_t
		BgL_envz00_4642, obj_t BgL_valuez00_4643)
	{
		{	/* Cfa/cfa.scm 112 */
			{
				BgL_approxz00_bglt BgL_auxz00_5448;

				{
					BgL_cvarzf2cinfozf2_bglt BgL_auxz00_5449;

					{
						obj_t BgL_auxz00_5450;

						{	/* Cfa/cfa.scm 113 */
							BgL_objectz00_bglt BgL_tmpz00_5451;

							BgL_tmpz00_5451 =
								((BgL_objectz00_bglt) ((BgL_cvarz00_bglt) BgL_valuez00_4643));
							BgL_auxz00_5450 = BGL_OBJECT_WIDENING(BgL_tmpz00_5451);
						}
						BgL_auxz00_5449 = ((BgL_cvarzf2cinfozf2_bglt) BgL_auxz00_5450);
					}
					BgL_auxz00_5448 =
						(((BgL_cvarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5449))->
						BgL_approxz00);
				}
				return ((obj_t) BgL_auxz00_5448);
			}
		}

	}



/* &cfa-variable-value-a1618 */
	obj_t BGl_z62cfazd2variablezd2valuezd2a1618zb0zzcfa_cfaz00(obj_t
		BgL_envz00_4644, obj_t BgL_valuez00_4645)
	{
		{	/* Cfa/cfa.scm 105 */
			{
				BgL_approxz00_bglt BgL_auxz00_5458;

				{
					BgL_scnstzf2cinfozf2_bglt BgL_auxz00_5459;

					{
						obj_t BgL_auxz00_5460;

						{	/* Cfa/cfa.scm 106 */
							BgL_objectz00_bglt BgL_tmpz00_5461;

							BgL_tmpz00_5461 =
								((BgL_objectz00_bglt) ((BgL_scnstz00_bglt) BgL_valuez00_4645));
							BgL_auxz00_5460 = BGL_OBJECT_WIDENING(BgL_tmpz00_5461);
						}
						BgL_auxz00_5459 = ((BgL_scnstzf2cinfozf2_bglt) BgL_auxz00_5460);
					}
					BgL_auxz00_5458 =
						(((BgL_scnstzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5459))->
						BgL_approxz00);
				}
				return ((obj_t) BgL_auxz00_5458);
			}
		}

	}



/* &cfa-variable-value-a1616 */
	obj_t BGl_z62cfazd2variablezd2valuezd2a1616zb0zzcfa_cfaz00(obj_t
		BgL_envz00_4646, obj_t BgL_valuez00_4647)
	{
		{	/* Cfa/cfa.scm 98 */
			{
				BgL_approxz00_bglt BgL_auxz00_5468;

				{
					BgL_svarzf2cinfozf2_bglt BgL_auxz00_5469;

					{
						obj_t BgL_auxz00_5470;

						{	/* Cfa/cfa.scm 99 */
							BgL_objectz00_bglt BgL_tmpz00_5471;

							BgL_tmpz00_5471 =
								((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_valuez00_4647));
							BgL_auxz00_5470 = BGL_OBJECT_WIDENING(BgL_tmpz00_5471);
						}
						BgL_auxz00_5469 = ((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_5470);
					}
					BgL_auxz00_5468 =
						(((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5469))->
						BgL_approxz00);
				}
				return ((obj_t) BgL_auxz00_5468);
			}
		}

	}



/* &cfa!-closure1612 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2closure1612za2zzcfa_cfaz00(obj_t
		BgL_envz00_4648, obj_t BgL_nodez00_4649)
	{
		{	/* Cfa/cfa.scm 87 */
			{	/* Cfa/cfa.scm 88 */
				obj_t BgL_arg1734z00_4809;

				BgL_arg1734z00_4809 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_4649)));
				return
					((BgL_approxz00_bglt)
					BGl_internalzd2errorzd2zztools_errorz00(BGl_string2102z00zzcfa_cfaz00,
						BGl_string2136z00zzcfa_cfaz00, BgL_arg1734z00_4809));
			}
		}

	}



/* &cfa!-var1610 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2var1610za2zzcfa_cfaz00(obj_t
		BgL_envz00_4650, obj_t BgL_nodez00_4651)
	{
		{	/* Cfa/cfa.scm 75 */
			{	/* Cfa/cfa.scm 77 */
				obj_t BgL_approxz00_4811;

				BgL_approxz00_4811 =
					BGl_cfazd2variablezd2valuezd2approxzd2zzcfa_cfaz00(
					(((BgL_variablez00_bglt) COBJECT(
								(((BgL_varz00_bglt) COBJECT(
											((BgL_varz00_bglt) BgL_nodez00_4651)))->
									BgL_variablez00)))->BgL_valuez00));
				{	/* Cfa/cfa.scm 78 */
					bool_t BgL_test2208z00_5487;

					if (
						(((obj_t)
								(((BgL_variablez00_bglt) COBJECT(
											(((BgL_varz00_bglt) COBJECT(
														((BgL_varz00_bglt) BgL_nodez00_4651)))->
												BgL_variablez00)))->BgL_typez00)) ==
							((obj_t) (((BgL_nodez00_bglt)
										COBJECT(((BgL_nodez00_bglt) ((BgL_varz00_bglt)
													BgL_nodez00_4651))))->BgL_typez00))))
						{	/* Cfa/cfa.scm 78 */
							BgL_test2208z00_5487 = ((bool_t) 1);
						}
					else
						{	/* Cfa/cfa.scm 78 */
							BgL_typez00_bglt BgL_arg1717z00_4812;

							BgL_arg1717z00_4812 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_varz00_bglt) BgL_nodez00_4651))))->BgL_typez00);
							BgL_test2208z00_5487 =
								(
								((obj_t) BgL_arg1717z00_4812) == BGl_za2_za2z00zztype_cachez00);
						}
					if (BgL_test2208z00_5487)
						{	/* Cfa/cfa.scm 78 */
							return ((BgL_approxz00_bglt) BgL_approxz00_4811);
						}
					else
						{	/* Cfa/cfa.scm 80 */
							BgL_approxz00_bglt BgL_new1177z00_4813;

							{	/* Cfa/cfa.scm 82 */
								BgL_approxz00_bglt BgL_new1180z00_4814;

								BgL_new1180z00_4814 =
									((BgL_approxz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
												BgL_approxz00_bgl))));
								{	/* Cfa/cfa.scm 82 */
									long BgL_arg1714z00_4815;

									BgL_arg1714z00_4815 =
										BGL_CLASS_NUM(BGl_approxz00zzcfa_infoz00);
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt) BgL_new1180z00_4814),
										BgL_arg1714z00_4815);
								}
								BgL_new1177z00_4813 = BgL_new1180z00_4814;
							}
							((((BgL_approxz00_bglt) COBJECT(BgL_new1177z00_4813))->
									BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_nodez00_bglt)
											COBJECT(((BgL_nodez00_bglt) ((BgL_varz00_bglt)
														BgL_nodez00_4651))))->BgL_typez00)), BUNSPEC);
							((((BgL_approxz00_bglt) COBJECT(BgL_new1177z00_4813))->
									BgL_typezd2lockedzf3z21) =
								((bool_t) (((BgL_approxz00_bglt) COBJECT(((BgL_approxz00_bglt)
													BgL_approxz00_4811)))->BgL_typezd2lockedzf3z21)),
								BUNSPEC);
							((((BgL_approxz00_bglt) COBJECT(BgL_new1177z00_4813))->
									BgL_allocsz00) =
								((obj_t) (((BgL_approxz00_bglt) COBJECT(((BgL_approxz00_bglt)
													BgL_approxz00_4811)))->BgL_allocsz00)), BUNSPEC);
							((((BgL_approxz00_bglt) COBJECT(BgL_new1177z00_4813))->
									BgL_topzf3zf3) =
								((bool_t) (((BgL_approxz00_bglt) COBJECT(((BgL_approxz00_bglt)
													BgL_approxz00_4811)))->BgL_topzf3zf3)), BUNSPEC);
							((((BgL_approxz00_bglt) COBJECT(BgL_new1177z00_4813))->
									BgL_lostzd2stampzd2) =
								((long) (((BgL_approxz00_bglt) COBJECT(((BgL_approxz00_bglt)
													BgL_approxz00_4811)))->BgL_lostzd2stampzd2)),
								BUNSPEC);
							((((BgL_approxz00_bglt) COBJECT(BgL_new1177z00_4813))->
									BgL_dupz00) = ((obj_t) BgL_approxz00_4811), BUNSPEC);
							((((BgL_approxz00_bglt) COBJECT(BgL_new1177z00_4813))->
									BgL_haszd2procedurezf3z21) =
								((bool_t) (((BgL_approxz00_bglt) COBJECT(((BgL_approxz00_bglt)
													BgL_approxz00_4811)))->BgL_haszd2procedurezf3z21)),
								BUNSPEC);
							return BgL_new1177z00_4813;
						}
				}
			}
		}

	}



/* &cfa!-kwote/node1608 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2kwotezf2node1608z50zzcfa_cfaz00(obj_t
		BgL_envz00_4652, obj_t BgL_knodez00_4653)
	{
		{	/* Cfa/cfa.scm 65 */
			{	/* Cfa/cfa.scm 70 */
				BgL_nodez00_bglt BgL_arg1691z00_4817;

				{
					BgL_kwotezf2nodezf2_bglt BgL_auxz00_5528;

					{
						obj_t BgL_auxz00_5529;

						{	/* Cfa/cfa.scm 70 */
							BgL_objectz00_bglt BgL_tmpz00_5530;

							BgL_tmpz00_5530 =
								((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_knodez00_4653));
							BgL_auxz00_5529 = BGL_OBJECT_WIDENING(BgL_tmpz00_5530);
						}
						BgL_auxz00_5528 = ((BgL_kwotezf2nodezf2_bglt) BgL_auxz00_5529);
					}
					BgL_arg1691z00_4817 =
						(((BgL_kwotezf2nodezf2_bglt) COBJECT(BgL_auxz00_5528))->
						BgL_nodez00);
				}
				return BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1691z00_4817);
			}
		}

	}



/* &cfa!-kwote/Cinfo1606 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2kwotezf2Cinfo1606z50zzcfa_cfaz00(obj_t
		BgL_envz00_4654, obj_t BgL_nodez00_4655)
	{
		{	/* Cfa/cfa.scm 58 */
			{
				BgL_kwotezf2cinfozf2_bglt BgL_auxz00_5537;

				{
					obj_t BgL_auxz00_5538;

					{	/* Cfa/cfa.scm 59 */
						BgL_objectz00_bglt BgL_tmpz00_5539;

						BgL_tmpz00_5539 =
							((BgL_objectz00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_4655));
						BgL_auxz00_5538 = BGL_OBJECT_WIDENING(BgL_tmpz00_5539);
					}
					BgL_auxz00_5537 = ((BgL_kwotezf2cinfozf2_bglt) BgL_auxz00_5538);
				}
				return
					(((BgL_kwotezf2cinfozf2_bglt) COBJECT(BgL_auxz00_5537))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-patch/Cinfo1604 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2patchzf2Cinfo1604z50zzcfa_cfaz00(obj_t
		BgL_envz00_4656, obj_t BgL_nodez00_4657)
	{
		{	/* Cfa/cfa.scm 51 */
			{
				BgL_patchzf2cinfozf2_bglt BgL_auxz00_5545;

				{
					obj_t BgL_auxz00_5546;

					{	/* Cfa/cfa.scm 52 */
						BgL_objectz00_bglt BgL_tmpz00_5547;

						BgL_tmpz00_5547 =
							((BgL_objectz00_bglt) ((BgL_patchz00_bglt) BgL_nodez00_4657));
						BgL_auxz00_5546 = BGL_OBJECT_WIDENING(BgL_tmpz00_5547);
					}
					BgL_auxz00_5545 = ((BgL_patchzf2cinfozf2_bglt) BgL_auxz00_5546);
				}
				return
					(((BgL_patchzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5545))->
					BgL_approxz00);
			}
		}

	}



/* &cfa!-literal/Cinfo1602 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2literalzf2Cinfo1602z50zzcfa_cfaz00(obj_t
		BgL_envz00_4658, obj_t BgL_nodez00_4659)
	{
		{	/* Cfa/cfa.scm 44 */
			{
				BgL_literalzf2cinfozf2_bglt BgL_auxz00_5553;

				{
					obj_t BgL_auxz00_5554;

					{	/* Cfa/cfa.scm 45 */
						BgL_objectz00_bglt BgL_tmpz00_5555;

						BgL_tmpz00_5555 =
							((BgL_objectz00_bglt) ((BgL_literalz00_bglt) BgL_nodez00_4659));
						BgL_auxz00_5554 = BGL_OBJECT_WIDENING(BgL_tmpz00_5555);
					}
					BgL_auxz00_5553 = ((BgL_literalzf2cinfozf2_bglt) BgL_auxz00_5554);
				}
				return
					(((BgL_literalzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5553))->
					BgL_approxz00);
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_cfaz00(void)
	{
		{	/* Cfa/cfa.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
			BGl_modulezd2initializa7ationz75zzcfa_info3z00(0L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
			BGl_modulezd2initializa7ationz75zzcfa_loosez00(471177480L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
			BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
			BGl_modulezd2initializa7ationz75zzcfa_iteratez00(131412063L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
			BGl_modulezd2initializa7ationz75zzcfa_appz00(102053843L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
			BGl_modulezd2initializa7ationz75zzcfa_funcallz00(87791639L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_typez00(93933605L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_cfaz00));
		}

	}

#ifdef __cplusplus
}
#endif
