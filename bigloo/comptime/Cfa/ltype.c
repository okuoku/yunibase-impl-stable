/*===========================================================================*/
/*   (Cfa/ltype.scm)                                                         */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/ltype.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_LTYPE_TYPE_DEFINITIONS
#define BGL_CFA_LTYPE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_cvarz00_bgl
	{
		header_t header;
		obj_t widening;
		bool_t BgL_macrozf3zf3;
	}              *BgL_cvarz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_internzd2sfunzf2cinfoz20_bgl
	{
		bool_t BgL_polymorphiczf3zf3;
		struct BgL_approxz00_bgl *BgL_approxz00;
		long BgL_stampz00;
	}                               *BgL_internzd2sfunzf2cinfoz20_bglt;

	typedef struct BgL_svarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_clozd2envzf3z21;
		long BgL_stampz00;
	}                      *BgL_svarzf2cinfozf2_bglt;

	typedef struct BgL_cvarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_cvarzf2cinfozf2_bglt;


#endif													// BGL_CFA_LTYPE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_closurez00zzast_nodez00;
	static obj_t BGl_z62typezd2variablez12zd2inter1596z70zzcfa_ltypez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_objectzd2initzd2zzcfa_ltypez00(void);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62typezd2nodez12zd2boxzd2setz121645zb0zzcfa_ltypez00(obj_t,
		obj_t);
	static obj_t BGl_z62typezd2nodez12zd2makezd2box1643za2zzcfa_ltypez00(obj_t,
		obj_t);
	static obj_t BGl_z62typezd2nodez12zd2atom1600z70zzcfa_ltypez00(obj_t, obj_t);
	extern obj_t BGl_cvarzf2Cinfozf2zzcfa_infoz00;
	static obj_t BGl_methodzd2initzd2zzcfa_ltypez00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62typezd2variablez12zd2scnst1590z70zzcfa_ltypez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t BGl_z62typezd2nodez12zd2funcall1616z70zzcfa_ltypez00(obj_t,
		obj_t);
	static obj_t BGl_z62typezd2nodez12zd2app1612z70zzcfa_ltypez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_lightzd2typez12zc0zzcfa_ltypez00(obj_t);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62typezd2nodez12zd2boxzd2ref1647za2zzcfa_ltypez00(obj_t,
		obj_t);
	static obj_t BGl_z62typezd2nodez12zd2setzd2exzd2it1639z70zzcfa_ltypez00(obj_t,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_ltypez00(void);
	static obj_t BGl_typezd2variablez12zc0zzcfa_ltypez00(BgL_valuez00_bglt,
		BgL_variablez00_bglt);
	extern obj_t BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
	extern obj_t BGl_sexitzf2Cinfozf2zzcfa_infoz00;
	static obj_t BGl_z62typezd2nodez12zd2vsetz121622z62zzcfa_ltypez00(obj_t,
		obj_t);
	static bool_t BGl_typezd2nodeza2z12z62zzcfa_ltypez00(obj_t);
	static obj_t BGl_z62typezd2nodez12zd2sync1610z70zzcfa_ltypez00(obj_t, obj_t);
	static obj_t BGl_z62lightzd2typez12za2zzcfa_ltypez00(obj_t, obj_t);
	extern obj_t BGl_svarzf2Cinfozf2zzcfa_infoz00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62typezd2nodez12zd2switch1633z70zzcfa_ltypez00(obj_t,
		obj_t);
	static obj_t BGl_z62typezd2nodez12zd2fail1631z70zzcfa_ltypez00(obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_ltypez00 = BUNSPEC;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzcfa_ltypez00(void);
	static obj_t BGl_z62typezd2nodez12zd2vref1620z70zzcfa_ltypez00(obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcfa_ltypez00(void);
	static obj_t BGl_z62typezd2nodez12zd2letzd2fun1635za2zzcfa_ltypez00(obj_t,
		obj_t);
	static obj_t BGl_z62typezd2variablez12zd2sexit1594z70zzcfa_ltypez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	static obj_t BGl_z62typezd2nodez12zd2kwote1602z70zzcfa_ltypez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static obj_t BGl_z62typezd2variablez12zd2svarzf21588z82zzcfa_ltypez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62typezd2variablez12zd2cvarzf21592z82zzcfa_ltypez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62typezd2nodez12zd2condition1628z70zzcfa_ltypez00(obj_t,
		obj_t);
	extern bool_t
		BGl_approxzd2procedurezd2elzf3zf3zzcfa_closurez00(BgL_approxz00_bglt);
	static obj_t BGl_z62typezd2nodez12zd2var1604z70zzcfa_ltypez00(obj_t, obj_t);
	static obj_t BGl_lightzd2typezd2funz12z12zzcfa_ltypez00(BgL_variablez00_bglt);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	static obj_t BGl_z62typezd2nodez12zd2sequence1608z70zzcfa_ltypez00(obj_t,
		obj_t);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	static obj_t BGl_z62typezd2variablez121585za2zzcfa_ltypez00(obj_t, obj_t,
		obj_t);
	static obj_t BGl_z62typezd2variablez12za2zzcfa_ltypez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static obj_t BGl_z62typezd2nodez12zd2letzd2var1637za2zzcfa_ltypez00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_ltypez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcnst_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_closurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_setz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_z62typezd2nodez12zd2closure1606z70zzcfa_ltypez00(obj_t,
		obj_t);
	static obj_t BGl_z62typezd2nodez12zd2jumpzd2exzd2i1641z70zzcfa_ltypez00(obj_t,
		obj_t);
	extern obj_t BGl_vrefz00zzast_nodez00;
	static obj_t BGl_typezd2nodez12zc0zzcfa_ltypez00(BgL_nodez00_bglt);
	extern obj_t BGl_nodez00zzast_nodez00;
	static obj_t BGl_z62typezd2nodez12zd2appzd2ly1614za2zzcfa_ltypez00(obj_t,
		obj_t);
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzcfa_ltypez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_ltypez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_ltypez00(void);
	static obj_t BGl_z62typezd2nodez12za2zzcfa_ltypez00(obj_t, obj_t);
	static obj_t BGl_z62typezd2nodez121597za2zzcfa_ltypez00(obj_t, obj_t);
	static obj_t BGl_z62typezd2nodez12zd2cast1624z70zzcfa_ltypez00(obj_t, obj_t);
	static obj_t BGl_z62typezd2nodez12zd2extern1618z70zzcfa_ltypez00(obj_t,
		obj_t);
	static obj_t BGl_z62typezd2nodez12zd2setq1626z70zzcfa_ltypez00(obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_valuez00zzast_varz00;
	extern obj_t BGl_scnstzf2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_switchz00zzast_nodez00;
	extern obj_t BGl_za2procedurezd2elza2zd2zztype_cachez00;
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	static obj_t __cnst[3];


	   
		 
		DEFINE_STRING(BGl_string2136z00zzcfa_ltypez00,
		BgL_bgl_string2136za700za7za7c2140za7, "Unexpected closure", 18);
	      DEFINE_STRING(BGl_string2137z00zzcfa_ltypez00,
		BgL_bgl_string2137za700za7za7c2141za7, "cfa_ltype", 9);
	      DEFINE_STRING(BGl_string2138z00zzcfa_ltypez00,
		BgL_bgl_string2138za700za7za7c2142za7, "static nothing type-node!1597 ",
		30);
	      DEFINE_STATIC_BGL_GENERIC(BGl_typezd2variablez12zd2envz12zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2variab2143z00,
		BGl_z62typezd2variablez12za2zzcfa_ltypez00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_lightzd2typez12zd2envz12zzcfa_ltypez00,
		BgL_bgl_za762lightza7d2typeza72144za7,
		BGl_z62lightzd2typez12za2zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2100z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2variab2145z00,
		BGl_z62typezd2variablez121585za2zzcfa_ltypez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2102z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712146za7,
		BGl_z62typezd2nodez121597za2zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2105z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2variab2147z00,
		BGl_z62typezd2variablez12zd2svarzf21588z82zzcfa_ltypez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2107z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2variab2148z00,
		BGl_z62typezd2variablez12zd2scnst1590z70zzcfa_ltypez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2108z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2variab2149z00,
		BGl_z62typezd2variablez12zd2cvarzf21592z82zzcfa_ltypez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2109z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2variab2150z00,
		BGl_z62typezd2variablez12zd2sexit1594z70zzcfa_ltypez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2110z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2variab2151z00,
		BGl_z62typezd2variablez12zd2inter1596z70zzcfa_ltypez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2111z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712152za7,
		BGl_z62typezd2nodez12zd2atom1600z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2113z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712153za7,
		BGl_z62typezd2nodez12zd2kwote1602z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2114z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712154za7,
		BGl_z62typezd2nodez12zd2var1604z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2115z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712155za7,
		BGl_z62typezd2nodez12zd2closure1606z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2116z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712156za7,
		BGl_z62typezd2nodez12zd2sequence1608z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2117z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712157za7,
		BGl_z62typezd2nodez12zd2sync1610z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2118z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712158za7,
		BGl_z62typezd2nodez12zd2app1612z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2119z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712159za7,
		BGl_z62typezd2nodez12zd2appzd2ly1614za2zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2120z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712160za7,
		BGl_z62typezd2nodez12zd2funcall1616z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2121z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712161za7,
		BGl_z62typezd2nodez12zd2extern1618z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2122z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712162za7,
		BGl_z62typezd2nodez12zd2vref1620z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2123z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712163za7,
		BGl_z62typezd2nodez12zd2vsetz121622z62zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2124z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712164za7,
		BGl_z62typezd2nodez12zd2cast1624z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2125z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712165za7,
		BGl_z62typezd2nodez12zd2setq1626z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2126z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712166za7,
		BGl_z62typezd2nodez12zd2condition1628z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2127z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712167za7,
		BGl_z62typezd2nodez12zd2fail1631z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2128z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712168za7,
		BGl_z62typezd2nodez12zd2switch1633z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2129z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712169za7,
		BGl_z62typezd2nodez12zd2letzd2fun1635za2zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2130z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712170za7,
		BGl_z62typezd2nodez12zd2letzd2var1637za2zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2131z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712171za7,
		BGl_z62typezd2nodez12zd2setzd2exzd2it1639z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2132z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712172za7,
		BGl_z62typezd2nodez12zd2jumpzd2exzd2i1641z70zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2133z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712173za7,
		BGl_z62typezd2nodez12zd2makezd2box1643za2zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2134z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712174za7,
		BGl_z62typezd2nodez12zd2boxzd2setz121645zb0zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2135z00zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712175za7,
		BGl_z62typezd2nodez12zd2boxzd2ref1647za2zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00,
		BgL_bgl_za762typeza7d2nodeza712176za7,
		BGl_z62typezd2nodez12za2zzcfa_ltypez00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2101z00zzcfa_ltypez00,
		BgL_bgl_string2101za700za7za7c2177za7, "type-variable!1585", 18);
	      DEFINE_STRING(BGl_string2103z00zzcfa_ltypez00,
		BgL_bgl_string2103za700za7za7c2178za7, "type-node!1597", 14);
	      DEFINE_STRING(BGl_string2104z00zzcfa_ltypez00,
		BgL_bgl_string2104za700za7za7c2179za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2106z00zzcfa_ltypez00,
		BgL_bgl_string2106za700za7za7c2180za7, "type-variable!", 14);
	      DEFINE_STRING(BGl_string2112z00zzcfa_ltypez00,
		BgL_bgl_string2112za700za7za7c2181za7, "type-node!", 10);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_ltypez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_ltypez00(long
		BgL_checksumz00_4869, char *BgL_fromz00_4870)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_ltypez00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_ltypez00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_ltypez00();
					BGl_libraryzd2moduleszd2initz00zzcfa_ltypez00();
					BGl_cnstzd2initzd2zzcfa_ltypez00();
					BGl_importedzd2moduleszd2initz00zzcfa_ltypez00();
					BGl_genericzd2initzd2zzcfa_ltypez00();
					BGl_methodzd2initzd2zzcfa_ltypez00();
					return BGl_toplevelzd2initzd2zzcfa_ltypez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_ltypez00(void)
	{
		{	/* Cfa/ltype.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cfa_ltype");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_ltype");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_ltype");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_ltype");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_ltype");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cfa_ltype");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cfa_ltype");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_ltype");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_ltype");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_ltype");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cfa_ltype");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_ltypez00(void)
	{
		{	/* Cfa/ltype.scm 15 */
			{	/* Cfa/ltype.scm 15 */
				obj_t BgL_cportz00_4773;

				{	/* Cfa/ltype.scm 15 */
					obj_t BgL_stringz00_4780;

					BgL_stringz00_4780 = BGl_string2138z00zzcfa_ltypez00;
					{	/* Cfa/ltype.scm 15 */
						obj_t BgL_startz00_4781;

						BgL_startz00_4781 = BINT(0L);
						{	/* Cfa/ltype.scm 15 */
							obj_t BgL_endz00_4782;

							BgL_endz00_4782 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4780)));
							{	/* Cfa/ltype.scm 15 */

								BgL_cportz00_4773 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4780, BgL_startz00_4781, BgL_endz00_4782);
				}}}}
				{
					long BgL_iz00_4774;

					BgL_iz00_4774 = 2L;
				BgL_loopz00_4775:
					if ((BgL_iz00_4774 == -1L))
						{	/* Cfa/ltype.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/ltype.scm 15 */
							{	/* Cfa/ltype.scm 15 */
								obj_t BgL_arg2139z00_4776;

								{	/* Cfa/ltype.scm 15 */

									{	/* Cfa/ltype.scm 15 */
										obj_t BgL_locationz00_4778;

										BgL_locationz00_4778 = BBOOL(((bool_t) 0));
										{	/* Cfa/ltype.scm 15 */

											BgL_arg2139z00_4776 =
												BGl_readz00zz__readerz00(BgL_cportz00_4773,
												BgL_locationz00_4778);
										}
									}
								}
								{	/* Cfa/ltype.scm 15 */
									int BgL_tmpz00_4901;

									BgL_tmpz00_4901 = (int) (BgL_iz00_4774);
									CNST_TABLE_SET(BgL_tmpz00_4901, BgL_arg2139z00_4776);
							}}
							{	/* Cfa/ltype.scm 15 */
								int BgL_auxz00_4779;

								BgL_auxz00_4779 = (int) ((BgL_iz00_4774 - 1L));
								{
									long BgL_iz00_4906;

									BgL_iz00_4906 = (long) (BgL_auxz00_4779);
									BgL_iz00_4774 = BgL_iz00_4906;
									goto BgL_loopz00_4775;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_ltypez00(void)
	{
		{	/* Cfa/ltype.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_ltypez00(void)
	{
		{	/* Cfa/ltype.scm 15 */
			return BUNSPEC;
		}

	}



/* light-type! */
	BGL_EXPORTED_DEF obj_t BGl_lightzd2typez12zc0zzcfa_ltypez00(obj_t
		BgL_globalsz00_23)
	{
		{	/* Cfa/ltype.scm 40 */
			{
				obj_t BgL_l1569z00_3561;

				{	/* Cfa/ltype.scm 41 */
					bool_t BgL_tmpz00_4909;

					BgL_l1569z00_3561 = BgL_globalsz00_23;
				BgL_zc3z04anonymousza31700ze3z87_3562:
					if (PAIRP(BgL_l1569z00_3561))
						{	/* Cfa/ltype.scm 41 */
							{	/* Cfa/ltype.scm 41 */
								obj_t BgL_arg1702z00_3564;

								BgL_arg1702z00_3564 = CAR(BgL_l1569z00_3561);
								BGl_lightzd2typezd2funz12z12zzcfa_ltypez00(
									((BgL_variablez00_bglt) BgL_arg1702z00_3564));
							}
							{
								obj_t BgL_l1569z00_4915;

								BgL_l1569z00_4915 = CDR(BgL_l1569z00_3561);
								BgL_l1569z00_3561 = BgL_l1569z00_4915;
								goto BgL_zc3z04anonymousza31700ze3z87_3562;
							}
						}
					else
						{	/* Cfa/ltype.scm 41 */
							BgL_tmpz00_4909 = ((bool_t) 1);
						}
					return BBOOL(BgL_tmpz00_4909);
				}
			}
		}

	}



/* &light-type! */
	obj_t BGl_z62lightzd2typez12za2zzcfa_ltypez00(obj_t BgL_envz00_4665,
		obj_t BgL_globalsz00_4666)
	{
		{	/* Cfa/ltype.scm 40 */
			return BGl_lightzd2typez12zc0zzcfa_ltypez00(BgL_globalsz00_4666);
		}

	}



/* light-type-fun! */
	obj_t BGl_lightzd2typezd2funz12z12zzcfa_ltypez00(BgL_variablez00_bglt
		BgL_varz00_24)
	{
		{	/* Cfa/ltype.scm 46 */
			{	/* Cfa/ltype.scm 47 */
				BgL_valuez00_bglt BgL_funz00_3567;

				BgL_funz00_3567 =
					(((BgL_variablez00_bglt) COBJECT(BgL_varz00_24))->BgL_valuez00);
				{	/* Cfa/ltype.scm 48 */
					bool_t BgL_test2185z00_4920;

					{	/* Cfa/ltype.scm 48 */
						obj_t BgL_classz00_4401;

						BgL_classz00_4401 = BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
						{	/* Cfa/ltype.scm 48 */
							BgL_objectz00_bglt BgL_arg1807z00_4403;

							{	/* Cfa/ltype.scm 48 */
								obj_t BgL_tmpz00_4921;

								BgL_tmpz00_4921 =
									((obj_t) ((BgL_objectz00_bglt) BgL_funz00_3567));
								BgL_arg1807z00_4403 = (BgL_objectz00_bglt) (BgL_tmpz00_4921);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/ltype.scm 48 */
									long BgL_idxz00_4409;

									BgL_idxz00_4409 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4403);
									BgL_test2185z00_4920 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4409 + 4L)) == BgL_classz00_4401);
								}
							else
								{	/* Cfa/ltype.scm 48 */
									bool_t BgL_res2086z00_4434;

									{	/* Cfa/ltype.scm 48 */
										obj_t BgL_oclassz00_4417;

										{	/* Cfa/ltype.scm 48 */
											obj_t BgL_arg1815z00_4425;
											long BgL_arg1816z00_4426;

											BgL_arg1815z00_4425 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/ltype.scm 48 */
												long BgL_arg1817z00_4427;

												BgL_arg1817z00_4427 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4403);
												BgL_arg1816z00_4426 =
													(BgL_arg1817z00_4427 - OBJECT_TYPE);
											}
											BgL_oclassz00_4417 =
												VECTOR_REF(BgL_arg1815z00_4425, BgL_arg1816z00_4426);
										}
										{	/* Cfa/ltype.scm 48 */
											bool_t BgL__ortest_1115z00_4418;

											BgL__ortest_1115z00_4418 =
												(BgL_classz00_4401 == BgL_oclassz00_4417);
											if (BgL__ortest_1115z00_4418)
												{	/* Cfa/ltype.scm 48 */
													BgL_res2086z00_4434 = BgL__ortest_1115z00_4418;
												}
											else
												{	/* Cfa/ltype.scm 48 */
													long BgL_odepthz00_4419;

													{	/* Cfa/ltype.scm 48 */
														obj_t BgL_arg1804z00_4420;

														BgL_arg1804z00_4420 = (BgL_oclassz00_4417);
														BgL_odepthz00_4419 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4420);
													}
													if ((4L < BgL_odepthz00_4419))
														{	/* Cfa/ltype.scm 48 */
															obj_t BgL_arg1802z00_4422;

															{	/* Cfa/ltype.scm 48 */
																obj_t BgL_arg1803z00_4423;

																BgL_arg1803z00_4423 = (BgL_oclassz00_4417);
																BgL_arg1802z00_4422 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4423,
																	4L);
															}
															BgL_res2086z00_4434 =
																(BgL_arg1802z00_4422 == BgL_classz00_4401);
														}
													else
														{	/* Cfa/ltype.scm 48 */
															BgL_res2086z00_4434 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2185z00_4920 = BgL_res2086z00_4434;
								}
						}
					}
					if (BgL_test2185z00_4920)
						{	/* Cfa/ltype.scm 48 */
							{	/* Cfa/ltype.scm 53 */
								obj_t BgL_g1573z00_3570;

								BgL_g1573z00_3570 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt)
												((BgL_sfunz00_bglt) BgL_funz00_3567))))->BgL_argsz00);
								{
									obj_t BgL_l1571z00_3572;

									BgL_l1571z00_3572 = BgL_g1573z00_3570;
								BgL_zc3z04anonymousza31705ze3z87_3573:
									if (PAIRP(BgL_l1571z00_3572))
										{	/* Cfa/ltype.scm 53 */
											{	/* Cfa/ltype.scm 54 */
												obj_t BgL_varz00_3575;

												BgL_varz00_3575 = CAR(BgL_l1571z00_3572);
												{	/* Cfa/ltype.scm 54 */
													BgL_valuez00_bglt BgL_arg1708z00_3576;

													BgL_arg1708z00_3576 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_varz00_3575))))->
														BgL_valuez00);
													BGl_typezd2variablez12zc0zzcfa_ltypez00
														(BgL_arg1708z00_3576,
														((BgL_variablez00_bglt) BgL_varz00_3575));
												}
											}
											{
												obj_t BgL_l1571z00_4955;

												BgL_l1571z00_4955 = CDR(BgL_l1571z00_3572);
												BgL_l1571z00_3572 = BgL_l1571z00_4955;
												goto BgL_zc3z04anonymousza31705ze3z87_3573;
											}
										}
									else
										{	/* Cfa/ltype.scm 53 */
											((bool_t) 1);
										}
								}
							}
							{	/* Cfa/ltype.scm 57 */
								obj_t BgL_arg1710z00_3579;

								BgL_arg1710z00_3579 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt)
												((BgL_sfunz00_bglt) BgL_funz00_3567))))->BgL_bodyz00);
								BGl_typezd2nodez12zc0zzcfa_ltypez00(
									((BgL_nodez00_bglt) BgL_arg1710z00_3579));
							}
							{	/* Cfa/ltype.scm 59 */
								obj_t BgL_arg1711z00_3580;

								{	/* Cfa/ltype.scm 59 */
									BgL_approxz00_bglt BgL_arg1714z00_3581;

									{
										BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_4962;

										{
											obj_t BgL_auxz00_4963;

											{	/* Cfa/ltype.scm 59 */
												BgL_objectz00_bglt BgL_tmpz00_4964;

												BgL_tmpz00_4964 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_funz00_3567));
												BgL_auxz00_4963 = BGL_OBJECT_WIDENING(BgL_tmpz00_4964);
											}
											BgL_auxz00_4962 =
												((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_4963);
										}
										BgL_arg1714z00_3581 =
											(((BgL_internzd2sfunzf2cinfoz20_bglt)
												COBJECT(BgL_auxz00_4962))->BgL_approxz00);
									}
									if (BGl_approxzd2procedurezd2elzf3zf3zzcfa_closurez00
										(BgL_arg1714z00_3581))
										{	/* Cfa/ltype.scm 65 */
											BgL_arg1711z00_3580 =
												BGl_za2procedurezd2elza2zd2zztype_cachez00;
										}
									else
										{	/* Cfa/ltype.scm 65 */
											BgL_arg1711z00_3580 = BGl_za2_za2z00zztype_cachez00;
										}
								}
								if (
									(((obj_t)
											((BgL_typez00_bglt) BgL_arg1711z00_3580)) ==
										BGl_za2_za2z00zztype_cachez00))
									{	/* Cfa/ltype.scm 118 */
										return BFALSE;
									}
								else
									{	/* Cfa/ltype.scm 118 */
										return
											((((BgL_variablez00_bglt) COBJECT(BgL_varz00_24))->
												BgL_typez00) =
											((BgL_typez00_bglt) ((BgL_typez00_bglt)
													BgL_arg1711z00_3580)), BUNSPEC);
									}
							}
						}
					else
						{	/* Cfa/ltype.scm 48 */
							return BFALSE;
						}
				}
			}
		}

	}



/* type-node*! */
	bool_t BGl_typezd2nodeza2z12z62zzcfa_ltypez00(obj_t BgL_nodeza2za2_65)
	{
		{	/* Cfa/ltype.scm 327 */
			{
				obj_t BgL_l1583z00_3584;

				BgL_l1583z00_3584 = BgL_nodeza2za2_65;
			BgL_zc3z04anonymousza31716ze3z87_3585:
				if (PAIRP(BgL_l1583z00_3584))
					{	/* Cfa/ltype.scm 328 */
						{	/* Cfa/ltype.scm 328 */
							obj_t BgL_arg1720z00_3587;

							BgL_arg1720z00_3587 = CAR(BgL_l1583z00_3584);
							BGl_typezd2nodez12zc0zzcfa_ltypez00(
								((BgL_nodez00_bglt) BgL_arg1720z00_3587));
						}
						{
							obj_t BgL_l1583z00_4983;

							BgL_l1583z00_4983 = CDR(BgL_l1583z00_3584);
							BgL_l1583z00_3584 = BgL_l1583z00_4983;
							goto BgL_zc3z04anonymousza31716ze3z87_3585;
						}
					}
				else
					{	/* Cfa/ltype.scm 328 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_ltypez00(void)
	{
		{	/* Cfa/ltype.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_ltypez00(void)
	{
		{	/* Cfa/ltype.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_typezd2variablez12zd2envz12zzcfa_ltypez00,
				BGl_proc2100z00zzcfa_ltypez00, BGl_valuez00zzast_varz00,
				BGl_string2101z00zzcfa_ltypez00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00,
				BGl_proc2102z00zzcfa_ltypez00, BGl_nodez00zzast_nodez00,
				BGl_string2103z00zzcfa_ltypez00);
		}

	}



/* &type-node!1597 */
	obj_t BGl_z62typezd2nodez121597za2zzcfa_ltypez00(obj_t BgL_envz00_4669,
		obj_t BgL_nodez00_4670)
	{
		{	/* Cfa/ltype.scm 125 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(0),
				BGl_string2104z00zzcfa_ltypez00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_4670)));
		}

	}



/* &type-variable!1585 */
	obj_t BGl_z62typezd2variablez121585za2zzcfa_ltypez00(obj_t BgL_envz00_4671,
		obj_t BgL_valuez00_4672, obj_t BgL_variablez00_4673)
	{
		{	/* Cfa/ltype.scm 72 */
			{	/* Cfa/ltype.scm 73 */
				BgL_typez00_bglt BgL_typez00_4787;

				BgL_typez00_4787 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_variablez00_4673)))->BgL_typez00);
				if (
					(((obj_t) BgL_typez00_4787) == BGl_za2procedureza2z00zztype_cachez00))
					{	/* Cfa/ltype.scm 74 */
						if ((((obj_t) BgL_typez00_4787) == BGl_za2_za2z00zztype_cachez00))
							{	/* Cfa/ltype.scm 118 */
								return BFALSE;
							}
						else
							{	/* Cfa/ltype.scm 118 */
								return
									((((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_variablez00_4673)))->
										BgL_typez00) =
									((BgL_typez00_bglt) BgL_typez00_4787), BUNSPEC);
							}
					}
				else
					{	/* Cfa/ltype.scm 74 */
						return CNST_TABLE_REF(1);
					}
			}
		}

	}



/* type-variable! */
	obj_t BGl_typezd2variablez12zc0zzcfa_ltypez00(BgL_valuez00_bglt
		BgL_valuez00_26, BgL_variablez00_bglt BgL_variablez00_27)
	{
		{	/* Cfa/ltype.scm 72 */
			{	/* Cfa/ltype.scm 72 */
				obj_t BgL_method1586z00_3600;

				{	/* Cfa/ltype.scm 72 */
					obj_t BgL_res2091z00_4485;

					{	/* Cfa/ltype.scm 72 */
						long BgL_objzd2classzd2numz00_4456;

						BgL_objzd2classzd2numz00_4456 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_valuez00_26));
						{	/* Cfa/ltype.scm 72 */
							obj_t BgL_arg1811z00_4457;

							BgL_arg1811z00_4457 =
								PROCEDURE_REF(BGl_typezd2variablez12zd2envz12zzcfa_ltypez00,
								(int) (1L));
							{	/* Cfa/ltype.scm 72 */
								int BgL_offsetz00_4460;

								BgL_offsetz00_4460 = (int) (BgL_objzd2classzd2numz00_4456);
								{	/* Cfa/ltype.scm 72 */
									long BgL_offsetz00_4461;

									BgL_offsetz00_4461 =
										((long) (BgL_offsetz00_4460) - OBJECT_TYPE);
									{	/* Cfa/ltype.scm 72 */
										long BgL_modz00_4462;

										BgL_modz00_4462 =
											(BgL_offsetz00_4461 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/ltype.scm 72 */
											long BgL_restz00_4464;

											BgL_restz00_4464 =
												(BgL_offsetz00_4461 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/ltype.scm 72 */

												{	/* Cfa/ltype.scm 72 */
													obj_t BgL_bucketz00_4466;

													BgL_bucketz00_4466 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4457), BgL_modz00_4462);
													BgL_res2091z00_4485 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4466), BgL_restz00_4464);
					}}}}}}}}
					BgL_method1586z00_3600 = BgL_res2091z00_4485;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1586z00_3600,
					((obj_t) BgL_valuez00_26), ((obj_t) BgL_variablez00_27));
			}
		}

	}



/* &type-variable! */
	obj_t BGl_z62typezd2variablez12za2zzcfa_ltypez00(obj_t BgL_envz00_4674,
		obj_t BgL_valuez00_4675, obj_t BgL_variablez00_4676)
	{
		{	/* Cfa/ltype.scm 72 */
			return
				BGl_typezd2variablez12zc0zzcfa_ltypez00(
				((BgL_valuez00_bglt) BgL_valuez00_4675),
				((BgL_variablez00_bglt) BgL_variablez00_4676));
		}

	}



/* type-node! */
	obj_t BGl_typezd2nodez12zc0zzcfa_ltypez00(BgL_nodez00_bglt BgL_nodez00_40)
	{
		{	/* Cfa/ltype.scm 125 */
			{	/* Cfa/ltype.scm 125 */
				obj_t BgL_method1598z00_3601;

				{	/* Cfa/ltype.scm 125 */
					obj_t BgL_res2096z00_4516;

					{	/* Cfa/ltype.scm 125 */
						long BgL_objzd2classzd2numz00_4487;

						BgL_objzd2classzd2numz00_4487 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_40));
						{	/* Cfa/ltype.scm 125 */
							obj_t BgL_arg1811z00_4488;

							BgL_arg1811z00_4488 =
								PROCEDURE_REF(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00,
								(int) (1L));
							{	/* Cfa/ltype.scm 125 */
								int BgL_offsetz00_4491;

								BgL_offsetz00_4491 = (int) (BgL_objzd2classzd2numz00_4487);
								{	/* Cfa/ltype.scm 125 */
									long BgL_offsetz00_4492;

									BgL_offsetz00_4492 =
										((long) (BgL_offsetz00_4491) - OBJECT_TYPE);
									{	/* Cfa/ltype.scm 125 */
										long BgL_modz00_4493;

										BgL_modz00_4493 =
											(BgL_offsetz00_4492 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/ltype.scm 125 */
											long BgL_restz00_4495;

											BgL_restz00_4495 =
												(BgL_offsetz00_4492 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/ltype.scm 125 */

												{	/* Cfa/ltype.scm 125 */
													obj_t BgL_bucketz00_4497;

													BgL_bucketz00_4497 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4488), BgL_modz00_4493);
													BgL_res2096z00_4516 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4497), BgL_restz00_4495);
					}}}}}}}}
					BgL_method1598z00_3601 = BgL_res2096z00_4516;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1598z00_3601, ((obj_t) BgL_nodez00_40));
			}
		}

	}



/* &type-node! */
	obj_t BGl_z62typezd2nodez12za2zzcfa_ltypez00(obj_t BgL_envz00_4677,
		obj_t BgL_nodez00_4678)
	{
		{	/* Cfa/ltype.scm 125 */
			return
				BGl_typezd2nodez12zc0zzcfa_ltypez00(
				((BgL_nodez00_bglt) BgL_nodez00_4678));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_ltypez00(void)
	{
		{	/* Cfa/ltype.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2variablez12zd2envz12zzcfa_ltypez00,
				BGl_svarzf2Cinfozf2zzcfa_infoz00, BGl_proc2105z00zzcfa_ltypez00,
				BGl_string2106z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2variablez12zd2envz12zzcfa_ltypez00,
				BGl_scnstzf2Cinfozf2zzcfa_infoz00, BGl_proc2107z00zzcfa_ltypez00,
				BGl_string2106z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2variablez12zd2envz12zzcfa_ltypez00,
				BGl_cvarzf2Cinfozf2zzcfa_infoz00, BGl_proc2108z00zzcfa_ltypez00,
				BGl_string2106z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2variablez12zd2envz12zzcfa_ltypez00,
				BGl_sexitzf2Cinfozf2zzcfa_infoz00, BGl_proc2109z00zzcfa_ltypez00,
				BGl_string2106z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2variablez12zd2envz12zzcfa_ltypez00,
				BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00,
				BGl_proc2110z00zzcfa_ltypez00, BGl_string2106z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00, BGl_atomz00zzast_nodez00,
				BGl_proc2111z00zzcfa_ltypez00, BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00, BGl_kwotez00zzast_nodez00,
				BGl_proc2113z00zzcfa_ltypez00, BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00, BGl_varz00zzast_nodez00,
				BGl_proc2114z00zzcfa_ltypez00, BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00, BGl_closurez00zzast_nodez00,
				BGl_proc2115z00zzcfa_ltypez00, BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00,
				BGl_sequencez00zzast_nodez00, BGl_proc2116z00zzcfa_ltypez00,
				BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00, BGl_syncz00zzast_nodez00,
				BGl_proc2117z00zzcfa_ltypez00, BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00, BGl_appz00zzast_nodez00,
				BGl_proc2118z00zzcfa_ltypez00, BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2119z00zzcfa_ltypez00,
				BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00, BGl_funcallz00zzast_nodez00,
				BGl_proc2120z00zzcfa_ltypez00, BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00, BGl_externz00zzast_nodez00,
				BGl_proc2121z00zzcfa_ltypez00, BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00, BGl_vrefz00zzast_nodez00,
				BGl_proc2122z00zzcfa_ltypez00, BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00, BGl_vsetz12z12zzast_nodez00,
				BGl_proc2123z00zzcfa_ltypez00, BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00, BGl_castz00zzast_nodez00,
				BGl_proc2124z00zzcfa_ltypez00, BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00, BGl_setqz00zzast_nodez00,
				BGl_proc2125z00zzcfa_ltypez00, BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2126z00zzcfa_ltypez00,
				BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00, BGl_failz00zzast_nodez00,
				BGl_proc2127z00zzcfa_ltypez00, BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00, BGl_switchz00zzast_nodez00,
				BGl_proc2128z00zzcfa_ltypez00, BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2129z00zzcfa_ltypez00,
				BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2130z00zzcfa_ltypez00,
				BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2131z00zzcfa_ltypez00,
				BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2132z00zzcfa_ltypez00,
				BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2133z00zzcfa_ltypez00,
				BGl_string2112z00zzcfa_ltypez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2134z00zzcfa_ltypez00,
				BGl_string2112z00zzcfa_ltypez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_ltypez00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2135z00zzcfa_ltypez00,
				BGl_string2112z00zzcfa_ltypez00);
		}

	}



/* &type-node!-box-ref1647 */
	obj_t BGl_z62typezd2nodez12zd2boxzd2ref1647za2zzcfa_ltypez00(obj_t
		BgL_envz00_4708, obj_t BgL_nodez00_4709)
	{
		{	/* Cfa/ltype.scm 320 */
			{	/* Cfa/ltype.scm 322 */
				BgL_varz00_bglt BgL_arg1874z00_4789;

				BgL_arg1874z00_4789 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_4709)))->BgL_varz00);
				return
					BGl_typezd2nodez12zc0zzcfa_ltypez00(
					((BgL_nodez00_bglt) BgL_arg1874z00_4789));
			}
		}

	}



/* &type-node!-box-set!1645 */
	obj_t BGl_z62typezd2nodez12zd2boxzd2setz121645zb0zzcfa_ltypez00(obj_t
		BgL_envz00_4710, obj_t BgL_nodez00_4711)
	{
		{	/* Cfa/ltype.scm 312 */
			{	/* Cfa/ltype.scm 314 */
				BgL_varz00_bglt BgL_arg1872z00_4791;

				BgL_arg1872z00_4791 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_4711)))->BgL_varz00);
				BGl_typezd2nodez12zc0zzcfa_ltypez00(
					((BgL_nodez00_bglt) BgL_arg1872z00_4791));
			}
			return
				BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_4711)))->BgL_valuez00));
		}

	}



/* &type-node!-make-box1643 */
	obj_t BGl_z62typezd2nodez12zd2makezd2box1643za2zzcfa_ltypez00(obj_t
		BgL_envz00_4712, obj_t BgL_nodez00_4713)
	{
		{	/* Cfa/ltype.scm 305 */
			return
				BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_4713)))->BgL_valuez00));
		}

	}



/* &type-node!-jump-ex-i1641 */
	obj_t BGl_z62typezd2nodez12zd2jumpzd2exzd2i1641z70zzcfa_ltypez00(obj_t
		BgL_envz00_4714, obj_t BgL_nodez00_4715)
	{
		{	/* Cfa/ltype.scm 297 */
			BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_4715)))->BgL_exitz00));
			return
				BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_4715)))->BgL_valuez00));
		}

	}



/* &type-node!-set-ex-it1639 */
	obj_t BGl_z62typezd2nodez12zd2setzd2exzd2it1639z70zzcfa_ltypez00(obj_t
		BgL_envz00_4716, obj_t BgL_nodez00_4717)
	{
		{	/* Cfa/ltype.scm 286 */
			{	/* Cfa/ltype.scm 288 */
				BgL_variablez00_bglt BgL_vz00_4795;

				BgL_vz00_4795 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
										((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4717)))->
								BgL_varz00)))->BgL_variablez00);
				{	/* Cfa/ltype.scm 289 */
					BgL_valuez00_bglt BgL_arg1860z00_4796;

					BgL_arg1860z00_4796 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_vz00_4795))))->BgL_valuez00);
					BGl_typezd2variablez12zc0zzcfa_ltypez00(BgL_arg1860z00_4796,
						BgL_vz00_4795);
				}
			}
			BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4717)))->BgL_bodyz00));
			BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4717)))->BgL_onexitz00));
			{	/* Cfa/ltype.scm 292 */
				BgL_varz00_bglt BgL_arg1866z00_4797;

				BgL_arg1866z00_4797 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4717)))->BgL_varz00);
				return
					BGl_typezd2nodez12zc0zzcfa_ltypez00(
					((BgL_nodez00_bglt) BgL_arg1866z00_4797));
			}
		}

	}



/* &type-node!-let-var1637 */
	obj_t BGl_z62typezd2nodez12zd2letzd2var1637za2zzcfa_ltypez00(obj_t
		BgL_envz00_4718, obj_t BgL_nodez00_4719)
	{
		{	/* Cfa/ltype.scm 273 */
			{	/* Cfa/ltype.scm 275 */
				obj_t BgL_g1582z00_4799;

				BgL_g1582z00_4799 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_4719)))->BgL_bindingsz00);
				{
					obj_t BgL_l1580z00_4801;

					BgL_l1580z00_4801 = BgL_g1582z00_4799;
				BgL_zc3z04anonymousza31855ze3z87_4800:
					if (PAIRP(BgL_l1580z00_4801))
						{	/* Cfa/ltype.scm 275 */
							{	/* Cfa/ltype.scm 276 */
								obj_t BgL_bindingz00_4802;

								BgL_bindingz00_4802 = CAR(BgL_l1580z00_4801);
								{	/* Cfa/ltype.scm 276 */
									obj_t BgL_varz00_4803;
									obj_t BgL_valz00_4804;

									BgL_varz00_4803 = CAR(((obj_t) BgL_bindingz00_4802));
									BgL_valz00_4804 = CDR(((obj_t) BgL_bindingz00_4802));
									BGl_typezd2nodez12zc0zzcfa_ltypez00(
										((BgL_nodez00_bglt) BgL_valz00_4804));
									{	/* Cfa/ltype.scm 279 */
										BgL_valuez00_bglt BgL_arg1857z00_4805;

										BgL_arg1857z00_4805 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_varz00_4803))))->
											BgL_valuez00);
										BGl_typezd2variablez12zc0zzcfa_ltypez00(BgL_arg1857z00_4805,
											((BgL_variablez00_bglt) BgL_varz00_4803));
									}
								}
							}
							{
								obj_t BgL_l1580z00_5151;

								BgL_l1580z00_5151 = CDR(BgL_l1580z00_4801);
								BgL_l1580z00_4801 = BgL_l1580z00_5151;
								goto BgL_zc3z04anonymousza31855ze3z87_4800;
							}
						}
					else
						{	/* Cfa/ltype.scm 275 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_4719)))->BgL_bodyz00));
		}

	}



/* &type-node!-let-fun1635 */
	obj_t BGl_z62typezd2nodez12zd2letzd2fun1635za2zzcfa_ltypez00(obj_t
		BgL_envz00_4720, obj_t BgL_nodez00_4721)
	{
		{	/* Cfa/ltype.scm 265 */
			{	/* Cfa/ltype.scm 267 */
				obj_t BgL_g1579z00_4807;

				BgL_g1579z00_4807 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_4721)))->BgL_localsz00);
				{
					obj_t BgL_l1577z00_4809;

					BgL_l1577z00_4809 = BgL_g1579z00_4807;
				BgL_zc3z04anonymousza31850ze3z87_4808:
					if (PAIRP(BgL_l1577z00_4809))
						{	/* Cfa/ltype.scm 267 */
							{	/* Cfa/ltype.scm 267 */
								obj_t BgL_arg1852z00_4810;

								BgL_arg1852z00_4810 = CAR(BgL_l1577z00_4809);
								BGl_lightzd2typezd2funz12z12zzcfa_ltypez00(
									((BgL_variablez00_bglt) BgL_arg1852z00_4810));
							}
							{
								obj_t BgL_l1577z00_5163;

								BgL_l1577z00_5163 = CDR(BgL_l1577z00_4809);
								BgL_l1577z00_4809 = BgL_l1577z00_5163;
								goto BgL_zc3z04anonymousza31850ze3z87_4808;
							}
						}
					else
						{	/* Cfa/ltype.scm 267 */
							((bool_t) 1);
						}
				}
			}
			return
				BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_4721)))->BgL_bodyz00));
		}

	}



/* &type-node!-switch1633 */
	obj_t BGl_z62typezd2nodez12zd2switch1633z70zzcfa_ltypez00(obj_t
		BgL_envz00_4722, obj_t BgL_nodez00_4723)
	{
		{	/* Cfa/ltype.scm 255 */
			{	/* Cfa/ltype.scm 256 */
				bool_t BgL_tmpz00_5168;

				BGl_typezd2nodez12zc0zzcfa_ltypez00(
					(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_4723)))->BgL_testz00));
				{	/* Cfa/ltype.scm 258 */
					obj_t BgL_g1576z00_4812;

					BgL_g1576z00_4812 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_4723)))->BgL_clausesz00);
					{
						obj_t BgL_l1574z00_4814;

						BgL_l1574z00_4814 = BgL_g1576z00_4812;
					BgL_zc3z04anonymousza31846ze3z87_4813:
						if (PAIRP(BgL_l1574z00_4814))
							{	/* Cfa/ltype.scm 258 */
								{	/* Cfa/ltype.scm 259 */
									obj_t BgL_clausez00_4815;

									BgL_clausez00_4815 = CAR(BgL_l1574z00_4814);
									{	/* Cfa/ltype.scm 259 */
										obj_t BgL_arg1848z00_4816;

										BgL_arg1848z00_4816 = CDR(((obj_t) BgL_clausez00_4815));
										BGl_typezd2nodez12zc0zzcfa_ltypez00(
											((BgL_nodez00_bglt) BgL_arg1848z00_4816));
									}
								}
								{
									obj_t BgL_l1574z00_5181;

									BgL_l1574z00_5181 = CDR(BgL_l1574z00_4814);
									BgL_l1574z00_4814 = BgL_l1574z00_5181;
									goto BgL_zc3z04anonymousza31846ze3z87_4813;
								}
							}
						else
							{	/* Cfa/ltype.scm 258 */
								BgL_tmpz00_5168 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_5168);
			}
		}

	}



/* &type-node!-fail1631 */
	obj_t BGl_z62typezd2nodez12zd2fail1631z70zzcfa_ltypez00(obj_t BgL_envz00_4724,
		obj_t BgL_nodez00_4725)
	{
		{	/* Cfa/ltype.scm 246 */
			BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_4725)))->BgL_procz00));
			BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_4725)))->BgL_msgz00));
			return
				BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_4725)))->BgL_objz00));
		}

	}



/* &type-node!-condition1628 */
	obj_t BGl_z62typezd2nodez12zd2condition1628z70zzcfa_ltypez00(obj_t
		BgL_envz00_4726, obj_t BgL_nodez00_4727)
	{
		{	/* Cfa/ltype.scm 237 */
			BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_4727)))->BgL_testz00));
			BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_4727)))->BgL_truez00));
			return
				BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_4727)))->BgL_falsez00));
		}

	}



/* &type-node!-setq1626 */
	obj_t BGl_z62typezd2nodez12zd2setq1626z70zzcfa_ltypez00(obj_t BgL_envz00_4728,
		obj_t BgL_nodez00_4729)
	{
		{	/* Cfa/ltype.scm 229 */
			BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_4729)))->BgL_valuez00));
			{	/* Cfa/ltype.scm 232 */
				BgL_varz00_bglt BgL_arg1837z00_4820;

				BgL_arg1837z00_4820 =
					(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_4729)))->BgL_varz00);
				return
					BGl_typezd2nodez12zc0zzcfa_ltypez00(
					((BgL_nodez00_bglt) BgL_arg1837z00_4820));
			}
		}

	}



/* &type-node!-cast1624 */
	obj_t BGl_z62typezd2nodez12zd2cast1624z70zzcfa_ltypez00(obj_t BgL_envz00_4730,
		obj_t BgL_nodez00_4731)
	{
		{	/* Cfa/ltype.scm 222 */
			return
				BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_castz00_bglt) COBJECT(
							((BgL_castz00_bglt) BgL_nodez00_4731)))->BgL_argz00));
		}

	}



/* &type-node!-vset!1622 */
	obj_t BGl_z62typezd2nodez12zd2vsetz121622z62zzcfa_ltypez00(obj_t
		BgL_envz00_4732, obj_t BgL_nodez00_4733)
	{
		{	/* Cfa/ltype.scm 213 */
			{

				{	/* Cfa/ltype.scm 213 */
					obj_t BgL_nextzd2method1621zd2_4824;

					BgL_nextzd2method1621zd2_4824 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_vsetz12z12_bglt) BgL_nodez00_4733)),
						BGl_typezd2nodez12zd2envz12zzcfa_ltypez00,
						BGl_vsetz12z12zzast_nodez00);
					BGL_PROCEDURE_CALL1(BgL_nextzd2method1621zd2_4824,
						((obj_t) ((BgL_vsetz12z12_bglt) BgL_nodez00_4733)));
				}
				{	/* Cfa/ltype.scm 216 */
					bool_t BgL_test2198z00_5221;

					{	/* Cfa/ltype.scm 216 */
						BgL_typez00_bglt BgL_arg1834z00_4825;

						BgL_arg1834z00_4825 =
							(((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt) BgL_nodez00_4733)))->BgL_ftypez00);
						BgL_test2198z00_5221 =
							(((obj_t) BgL_arg1834z00_4825) == BGl_za2_za2z00zztype_cachez00);
					}
					if (BgL_test2198z00_5221)
						{	/* Cfa/ltype.scm 216 */
							return
								((((BgL_vsetz12z12_bglt) COBJECT(
											((BgL_vsetz12z12_bglt) BgL_nodez00_4733)))->
									BgL_ftypez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
						}
					else
						{	/* Cfa/ltype.scm 216 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &type-node!-vref1620 */
	obj_t BGl_z62typezd2nodez12zd2vref1620z70zzcfa_ltypez00(obj_t BgL_envz00_4734,
		obj_t BgL_nodez00_4735)
	{
		{	/* Cfa/ltype.scm 204 */
			{

				{	/* Cfa/ltype.scm 204 */
					obj_t BgL_nextzd2method1619zd2_4828;

					BgL_nextzd2method1619zd2_4828 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_vrefz00_bglt) BgL_nodez00_4735)),
						BGl_typezd2nodez12zd2envz12zzcfa_ltypez00,
						BGl_vrefz00zzast_nodez00);
					BGL_PROCEDURE_CALL1(BgL_nextzd2method1619zd2_4828,
						((obj_t) ((BgL_vrefz00_bglt) BgL_nodez00_4735)));
				}
				{	/* Cfa/ltype.scm 207 */
					bool_t BgL_test2199z00_5238;

					{	/* Cfa/ltype.scm 207 */
						BgL_typez00_bglt BgL_arg1831z00_4829;

						BgL_arg1831z00_4829 =
							(((BgL_vrefz00_bglt) COBJECT(
									((BgL_vrefz00_bglt) BgL_nodez00_4735)))->BgL_ftypez00);
						BgL_test2199z00_5238 =
							(((obj_t) BgL_arg1831z00_4829) == BGl_za2_za2z00zztype_cachez00);
					}
					if (BgL_test2199z00_5238)
						{	/* Cfa/ltype.scm 207 */
							return
								((((BgL_vrefz00_bglt) COBJECT(
											((BgL_vrefz00_bglt) BgL_nodez00_4735)))->BgL_ftypez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
						}
					else
						{	/* Cfa/ltype.scm 207 */
							return BFALSE;
						}
				}
			}
		}

	}



/* &type-node!-extern1618 */
	obj_t BGl_z62typezd2nodez12zd2extern1618z70zzcfa_ltypez00(obj_t
		BgL_envz00_4736, obj_t BgL_nodez00_4737)
	{
		{	/* Cfa/ltype.scm 197 */
			return
				BBOOL(BGl_typezd2nodeza2z12z62zzcfa_ltypez00(
					(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_4737)))->BgL_exprza2za2)));
		}

	}



/* &type-node!-funcall1616 */
	obj_t BGl_z62typezd2nodez12zd2funcall1616z70zzcfa_ltypez00(obj_t
		BgL_envz00_4738, obj_t BgL_nodez00_4739)
	{
		{	/* Cfa/ltype.scm 189 */
			{	/* Cfa/ltype.scm 190 */
				bool_t BgL_tmpz00_5250;

				BGl_typezd2nodez12zc0zzcfa_ltypez00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_4739)))->BgL_funz00));
				BgL_tmpz00_5250 =
					BGl_typezd2nodeza2z12z62zzcfa_ltypez00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_4739)))->BgL_argsz00));
				return BBOOL(BgL_tmpz00_5250);
			}
		}

	}



/* &type-node!-app-ly1614 */
	obj_t BGl_z62typezd2nodez12zd2appzd2ly1614za2zzcfa_ltypez00(obj_t
		BgL_envz00_4740, obj_t BgL_nodez00_4741)
	{
		{	/* Cfa/ltype.scm 181 */
			BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_4741)))->BgL_funz00));
			return
				BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_4741)))->BgL_argz00));
		}

	}



/* &type-node!-app1612 */
	obj_t BGl_z62typezd2nodez12zd2app1612z70zzcfa_ltypez00(obj_t BgL_envz00_4742,
		obj_t BgL_nodez00_4743)
	{
		{	/* Cfa/ltype.scm 174 */
			return
				BBOOL(BGl_typezd2nodeza2z12z62zzcfa_ltypez00(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_4743)))->BgL_argsz00)));
		}

	}



/* &type-node!-sync1610 */
	obj_t BGl_z62typezd2nodez12zd2sync1610z70zzcfa_ltypez00(obj_t BgL_envz00_4744,
		obj_t BgL_nodez00_4745)
	{
		{	/* Cfa/ltype.scm 165 */
			BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_4745)))->BgL_mutexz00));
			BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_4745)))->BgL_prelockz00));
			return
				BGl_typezd2nodez12zc0zzcfa_ltypez00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_4745)))->BgL_bodyz00));
		}

	}



/* &type-node!-sequence1608 */
	obj_t BGl_z62typezd2nodez12zd2sequence1608z70zzcfa_ltypez00(obj_t
		BgL_envz00_4746, obj_t BgL_nodez00_4747)
	{
		{	/* Cfa/ltype.scm 158 */
			return
				BBOOL(BGl_typezd2nodeza2z12z62zzcfa_ltypez00(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_4747)))->BgL_nodesz00)));
		}

	}



/* &type-node!-closure1606 */
	obj_t BGl_z62typezd2nodez12zd2closure1606z70zzcfa_ltypez00(obj_t
		BgL_envz00_4748, obj_t BgL_nodez00_4749)
	{
		{	/* Cfa/ltype.scm 152 */
			{	/* Cfa/ltype.scm 153 */
				obj_t BgL_arg1770z00_4837;

				BgL_arg1770z00_4837 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_4749)));
				return
					BGl_internalzd2errorzd2zztools_errorz00
					(BGl_string2112z00zzcfa_ltypez00, BGl_string2136z00zzcfa_ltypez00,
					BgL_arg1770z00_4837);
			}
		}

	}



/* &type-node!-var1604 */
	obj_t BGl_z62typezd2nodez12zd2var1604z70zzcfa_ltypez00(obj_t BgL_envz00_4750,
		obj_t BgL_nodez00_4751)
	{
		{	/* Cfa/ltype.scm 142 */
			{	/* Cfa/ltype.scm 144 */
				bool_t BgL_test2200z00_5285;

				{	/* Cfa/ltype.scm 144 */
					bool_t BgL_test2201z00_5286;

					{	/* Cfa/ltype.scm 144 */
						BgL_variablez00_bglt BgL_arg1754z00_4839;

						BgL_arg1754z00_4839 =
							(((BgL_varz00_bglt) COBJECT(
									((BgL_varz00_bglt) BgL_nodez00_4751)))->BgL_variablez00);
						{	/* Cfa/ltype.scm 144 */
							obj_t BgL_classz00_4840;

							BgL_classz00_4840 = BGl_globalz00zzast_varz00;
							{	/* Cfa/ltype.scm 144 */
								BgL_objectz00_bglt BgL_arg1807z00_4841;

								{	/* Cfa/ltype.scm 144 */
									obj_t BgL_tmpz00_5289;

									BgL_tmpz00_5289 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg1754z00_4839));
									BgL_arg1807z00_4841 = (BgL_objectz00_bglt) (BgL_tmpz00_5289);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/ltype.scm 144 */
										long BgL_idxz00_4842;

										BgL_idxz00_4842 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4841);
										BgL_test2201z00_5286 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4842 + 2L)) == BgL_classz00_4840);
									}
								else
									{	/* Cfa/ltype.scm 144 */
										bool_t BgL_res2097z00_4845;

										{	/* Cfa/ltype.scm 144 */
											obj_t BgL_oclassz00_4846;

											{	/* Cfa/ltype.scm 144 */
												obj_t BgL_arg1815z00_4847;
												long BgL_arg1816z00_4848;

												BgL_arg1815z00_4847 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/ltype.scm 144 */
													long BgL_arg1817z00_4849;

													BgL_arg1817z00_4849 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4841);
													BgL_arg1816z00_4848 =
														(BgL_arg1817z00_4849 - OBJECT_TYPE);
												}
												BgL_oclassz00_4846 =
													VECTOR_REF(BgL_arg1815z00_4847, BgL_arg1816z00_4848);
											}
											{	/* Cfa/ltype.scm 144 */
												bool_t BgL__ortest_1115z00_4850;

												BgL__ortest_1115z00_4850 =
													(BgL_classz00_4840 == BgL_oclassz00_4846);
												if (BgL__ortest_1115z00_4850)
													{	/* Cfa/ltype.scm 144 */
														BgL_res2097z00_4845 = BgL__ortest_1115z00_4850;
													}
												else
													{	/* Cfa/ltype.scm 144 */
														long BgL_odepthz00_4851;

														{	/* Cfa/ltype.scm 144 */
															obj_t BgL_arg1804z00_4852;

															BgL_arg1804z00_4852 = (BgL_oclassz00_4846);
															BgL_odepthz00_4851 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4852);
														}
														if ((2L < BgL_odepthz00_4851))
															{	/* Cfa/ltype.scm 144 */
																obj_t BgL_arg1802z00_4853;

																{	/* Cfa/ltype.scm 144 */
																	obj_t BgL_arg1803z00_4854;

																	BgL_arg1803z00_4854 = (BgL_oclassz00_4846);
																	BgL_arg1802z00_4853 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4854,
																		2L);
																}
																BgL_res2097z00_4845 =
																	(BgL_arg1802z00_4853 == BgL_classz00_4840);
															}
														else
															{	/* Cfa/ltype.scm 144 */
																BgL_res2097z00_4845 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2201z00_5286 = BgL_res2097z00_4845;
									}
							}
						}
					}
					if (BgL_test2201z00_5286)
						{	/* Cfa/ltype.scm 144 */
							BgL_test2200z00_5285 =
								(
								(((BgL_globalz00_bglt) COBJECT(
											((BgL_globalz00_bglt)
												(((BgL_varz00_bglt) COBJECT(
															((BgL_varz00_bglt) BgL_nodez00_4751)))->
													BgL_variablez00))))->BgL_importz00) ==
								CNST_TABLE_REF(2));
						}
					else
						{	/* Cfa/ltype.scm 144 */
							BgL_test2200z00_5285 = ((bool_t) 0);
						}
				}
				if (BgL_test2200z00_5285)
					{	/* Cfa/ltype.scm 145 */
						BgL_valuez00_bglt BgL_arg1749z00_4855;
						BgL_variablez00_bglt BgL_arg1750z00_4856;

						BgL_arg1749z00_4855 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt)
											(((BgL_varz00_bglt) COBJECT(
														((BgL_varz00_bglt) BgL_nodez00_4751)))->
												BgL_variablez00)))))->BgL_valuez00);
						BgL_arg1750z00_4856 =
							(((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
										BgL_nodez00_4751)))->BgL_variablez00);
						BGl_typezd2variablez12zc0zzcfa_ltypez00(BgL_arg1749z00_4855,
							BgL_arg1750z00_4856);
					}
				else
					{	/* Cfa/ltype.scm 144 */
						BFALSE;
					}
			}
			{	/* Cfa/ltype.scm 146 */
				bool_t BgL_test2205z00_5326;

				{	/* Cfa/ltype.scm 146 */
					BgL_typez00_bglt BgL_arg1765z00_4857;

					BgL_arg1765z00_4857 =
						(((BgL_variablez00_bglt) COBJECT(
								(((BgL_varz00_bglt) COBJECT(
											((BgL_varz00_bglt) BgL_nodez00_4751)))->
									BgL_variablez00)))->BgL_typez00);
					BgL_test2205z00_5326 =
						(((obj_t) BgL_arg1765z00_4857) ==
						BGl_za2procedurezd2elza2zd2zztype_cachez00);
				}
				if (BgL_test2205z00_5326)
					{	/* Cfa/ltype.scm 146 */
						return
							((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_varz00_bglt) BgL_nodez00_4751))))->BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt)
									BGl_za2procedurezd2elza2zd2zztype_cachez00)), BUNSPEC);
					}
				else
					{	/* Cfa/ltype.scm 146 */
						return BFALSE;
					}
			}
		}

	}



/* &type-node!-kwote1602 */
	obj_t BGl_z62typezd2nodez12zd2kwote1602z70zzcfa_ltypez00(obj_t
		BgL_envz00_4752, obj_t BgL_nodez00_4753)
	{
		{	/* Cfa/ltype.scm 136 */
			return BUNSPEC;
		}

	}



/* &type-node!-atom1600 */
	obj_t BGl_z62typezd2nodez12zd2atom1600z70zzcfa_ltypez00(obj_t BgL_envz00_4754,
		obj_t BgL_nodez00_4755)
	{
		{	/* Cfa/ltype.scm 130 */
			return BUNSPEC;
		}

	}



/* &type-variable!-inter1596 */
	obj_t BGl_z62typezd2variablez12zd2inter1596z70zzcfa_ltypez00(obj_t
		BgL_envz00_4756, obj_t BgL_valuez00_4757, obj_t BgL_variablez00_4758)
	{
		{	/* Cfa/ltype.scm 111 */
			return CNST_TABLE_REF(1);
		}

	}



/* &type-variable!-sexit1594 */
	obj_t BGl_z62typezd2variablez12zd2sexit1594z70zzcfa_ltypez00(obj_t
		BgL_envz00_4759, obj_t BgL_valuez00_4760, obj_t BgL_variablez00_4761)
	{
		{	/* Cfa/ltype.scm 101 */
			return CNST_TABLE_REF(1);
		}

	}



/* &type-variable!-cvar/1592 */
	obj_t BGl_z62typezd2variablez12zd2cvarzf21592z82zzcfa_ltypez00(obj_t
		BgL_envz00_4762, obj_t BgL_valuez00_4763, obj_t BgL_variablez00_4764)
	{
		{	/* Cfa/ltype.scm 94 */
			{	/* Cfa/ltype.scm 96 */
				obj_t BgL_arg1736z00_4863;

				{	/* Cfa/ltype.scm 96 */
					BgL_approxz00_bglt BgL_arg1737z00_4864;

					{
						BgL_cvarzf2cinfozf2_bglt BgL_auxz00_5338;

						{
							obj_t BgL_auxz00_5339;

							{	/* Cfa/ltype.scm 96 */
								BgL_objectz00_bglt BgL_tmpz00_5340;

								BgL_tmpz00_5340 =
									((BgL_objectz00_bglt) ((BgL_cvarz00_bglt) BgL_valuez00_4763));
								BgL_auxz00_5339 = BGL_OBJECT_WIDENING(BgL_tmpz00_5340);
							}
							BgL_auxz00_5338 = ((BgL_cvarzf2cinfozf2_bglt) BgL_auxz00_5339);
						}
						BgL_arg1737z00_4864 =
							(((BgL_cvarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5338))->
							BgL_approxz00);
					}
					if (BGl_approxzd2procedurezd2elzf3zf3zzcfa_closurez00
						(BgL_arg1737z00_4864))
						{	/* Cfa/ltype.scm 65 */
							BgL_arg1736z00_4863 = BGl_za2procedurezd2elza2zd2zztype_cachez00;
						}
					else
						{	/* Cfa/ltype.scm 65 */
							BgL_arg1736z00_4863 = BGl_za2_za2z00zztype_cachez00;
						}
				}
				if (
					(((obj_t)
							((BgL_typez00_bglt) BgL_arg1736z00_4863)) ==
						BGl_za2_za2z00zztype_cachez00))
					{	/* Cfa/ltype.scm 118 */
						return BFALSE;
					}
				else
					{	/* Cfa/ltype.scm 118 */
						return
							((((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_variablez00_4764)))->
								BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_arg1736z00_4863)),
							BUNSPEC);
					}
			}
		}

	}



/* &type-variable!-scnst1590 */
	obj_t BGl_z62typezd2variablez12zd2scnst1590z70zzcfa_ltypez00(obj_t
		BgL_envz00_4765, obj_t BgL_valuez00_4766, obj_t BgL_variablez00_4767)
	{
		{	/* Cfa/ltype.scm 88 */
			return CNST_TABLE_REF(1);
		}

	}



/* &type-variable!-svar/1588 */
	obj_t BGl_z62typezd2variablez12zd2svarzf21588z82zzcfa_ltypez00(obj_t
		BgL_envz00_4768, obj_t BgL_valuez00_4769, obj_t BgL_variablez00_4770)
	{
		{	/* Cfa/ltype.scm 81 */
			{	/* Cfa/ltype.scm 83 */
				obj_t BgL_arg1734z00_4867;

				{	/* Cfa/ltype.scm 83 */
					BgL_approxz00_bglt BgL_arg1735z00_4868;

					{
						BgL_svarzf2cinfozf2_bglt BgL_auxz00_5356;

						{
							obj_t BgL_auxz00_5357;

							{	/* Cfa/ltype.scm 83 */
								BgL_objectz00_bglt BgL_tmpz00_5358;

								BgL_tmpz00_5358 =
									((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_valuez00_4769));
								BgL_auxz00_5357 = BGL_OBJECT_WIDENING(BgL_tmpz00_5358);
							}
							BgL_auxz00_5356 = ((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_5357);
						}
						BgL_arg1735z00_4868 =
							(((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5356))->
							BgL_approxz00);
					}
					if (BGl_approxzd2procedurezd2elzf3zf3zzcfa_closurez00
						(BgL_arg1735z00_4868))
						{	/* Cfa/ltype.scm 65 */
							BgL_arg1734z00_4867 = BGl_za2procedurezd2elza2zd2zztype_cachez00;
						}
					else
						{	/* Cfa/ltype.scm 65 */
							BgL_arg1734z00_4867 = BGl_za2_za2z00zztype_cachez00;
						}
				}
				if (
					(((obj_t)
							((BgL_typez00_bglt) BgL_arg1734z00_4867)) ==
						BGl_za2_za2z00zztype_cachez00))
					{	/* Cfa/ltype.scm 118 */
						return BFALSE;
					}
				else
					{	/* Cfa/ltype.scm 118 */
						return
							((((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_variablez00_4770)))->
								BgL_typez00) =
							((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_arg1734z00_4867)),
							BUNSPEC);
					}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_ltypez00(void)
	{
		{	/* Cfa/ltype.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
			BGl_modulezd2initializa7ationz75zzcfa_info3z00(0L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
			BGl_modulezd2initializa7ationz75zzcfa_setz00(5932721L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
			BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(324810626L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
			BGl_modulezd2initializa7ationz75zzcfa_closurez00(189402713L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
			BGl_modulezd2initializa7ationz75zztvector_tvectorz00(501518119L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
			BGl_modulezd2initializa7ationz75zzcnst_nodez00(89013043L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string2137z00zzcfa_ltypez00));
		}

	}

#ifdef __cplusplus
}
#endif
