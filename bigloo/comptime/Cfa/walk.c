/*===========================================================================*/
/*   (Cfa/walk.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/walk.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_WALK_TYPE_DEFINITIONS
#define BGL_CFA_WALK_TYPE_DEFINITIONS
#endif													// BGL_CFA_WALK_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	extern obj_t BGl_declarezd2approxzd2setsz12z12zzcfa_approxz00(void);
	BGL_IMPORT bool_t BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00(obj_t);
	static obj_t BGl_objectzd2initzd2zzcfa_walkz00(void);
	extern obj_t BGl_typezd2settingsz12zc0zzcfa_typez00(obj_t);
	BGL_IMPORT bool_t BGl_2ze3ze3zz__r4_numbers_6_5z00(obj_t, obj_t);
	extern obj_t BGl_shrinkifyz12z12zzast_shrinkifyz00(obj_t);
	static obj_t BGl_appendzd221011zd2zzcfa_walkz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcfa_walkz00(void);
	extern obj_t BGl_specializa7ez12zb5zzcfa_specializa7eza7(obj_t);
	extern obj_t BGl_closurezd2optimiza7ationz12z67zzcfa_closurez00(obj_t);
	extern obj_t BGl_vectorzd2ze3tvectorz12z23zzcfa_tvectorz00(obj_t);
	extern obj_t BGl_cfazd2iteratezd2tozd2fixpointz12zc0zzcfa_iteratez00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_walkz00(void);
	extern obj_t BGl_za2currentzd2passza2zd2zzengine_passz00;
	extern obj_t BGl_showzd2cfazd2nbzd2iterationszd2zzcfa_showz00(void);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_walkz00 = BUNSPEC;
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcfa_walkz00(void);
	extern obj_t BGl_collectzd2allzd2approxz12z12zzcfa_collectz00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_setzd2initialzd2approxz12z12zzcfa_setupz00(obj_t);
	extern obj_t BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00;
	extern obj_t BGl_removezd2varzd2zzast_removez00(obj_t, obj_t);
	extern obj_t BGl_showzd2cfazd2resultsz00zzcfa_showz00(obj_t);
	BGL_IMPORT obj_t BGl_exitz00zz__errorz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_walkz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_arithmeticz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_pairz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_specializa7eza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_closurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_showz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_iteratez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_setupz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_collectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzwrite_astz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_shrinkifyz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_removez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_passz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	static obj_t BGl_z62cfazd2walkz12za2zzcfa_walkz00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzcfa_walkz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_walkz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_walkz00(void);
	BGL_EXPORTED_DECL obj_t BGl_cfazd2walkz12zc0zzcfa_walkz00(obj_t);
	static obj_t __cnst[3];


	extern obj_t BGl_unpatchzd2pairzd2setz12zd2envzc0zzcfa_pairz00;
	extern obj_t BGl_unpatchzd2vectorzd2setz12zd2envzc0zzcfa_tvectorz00;
	   
		 
		DEFINE_STRING(BGl_string1957z00zzcfa_walkz00,
		BgL_bgl_string1957za700za7za7c1968za7, "Cfa", 3);
	      DEFINE_STRING(BGl_string1958z00zzcfa_walkz00,
		BgL_bgl_string1958za700za7za7c1969za7, "   . ", 5);
	      DEFINE_STRING(BGl_string1959z00zzcfa_walkz00,
		BgL_bgl_string1959za700za7za7c1970za7, "failure during prelude hook", 27);
	      DEFINE_STRING(BGl_string1960z00zzcfa_walkz00,
		BgL_bgl_string1960za700za7za7c1971za7, " error", 6);
	      DEFINE_STRING(BGl_string1961z00zzcfa_walkz00,
		BgL_bgl_string1961za700za7za7c1972za7, "s", 1);
	      DEFINE_STRING(BGl_string1962z00zzcfa_walkz00,
		BgL_bgl_string1962za700za7za7c1973za7, "", 0);
	      DEFINE_STRING(BGl_string1963z00zzcfa_walkz00,
		BgL_bgl_string1963za700za7za7c1974za7, " occured, ending ...", 20);
	      DEFINE_STRING(BGl_string1964z00zzcfa_walkz00,
		BgL_bgl_string1964za700za7za7c1975za7, "failure during postlude hook", 28);
	      DEFINE_STRING(BGl_string1965z00zzcfa_walkz00,
		BgL_bgl_string1965za700za7za7c1976za7, "cfa_walk", 8);
	      DEFINE_STRING(BGl_string1966z00zzcfa_walkz00,
		BgL_bgl_string1966za700za7za7c1977za7,
		"(stop-closure-cache unpatch-vector-set! unpatch-pair-set!) (cfa inline) pass-started ",
		85);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_cfazd2walkz12zd2envz12zzcfa_walkz00,
		BgL_bgl_za762cfaza7d2walkza7121978za7, BGl_z62cfazd2walkz12za2zzcfa_walkz00,
		0L, BUNSPEC, 1);
	extern obj_t BGl_stopzd2closurezd2cachezd2envzd2zzcfa_closurez00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_walkz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_walkz00(long
		BgL_checksumz00_4044, char *BgL_fromz00_4045)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_walkz00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_walkz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_walkz00();
					BGl_libraryzd2moduleszd2initz00zzcfa_walkz00();
					BGl_cnstzd2initzd2zzcfa_walkz00();
					BGl_importedzd2moduleszd2initz00zzcfa_walkz00();
					return BGl_methodzd2initzd2zzcfa_walkz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_walkz00(void)
	{
		{	/* Cfa/walk.scm 24 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "cfa_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_walk");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5z00(0L, "cfa_walk");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_walk");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_walk");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cfa_walk");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_walk");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_walk");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_walkz00(void)
	{
		{	/* Cfa/walk.scm 24 */
			{	/* Cfa/walk.scm 24 */
				obj_t BgL_cportz00_4033;

				{	/* Cfa/walk.scm 24 */
					obj_t BgL_stringz00_4040;

					BgL_stringz00_4040 = BGl_string1966z00zzcfa_walkz00;
					{	/* Cfa/walk.scm 24 */
						obj_t BgL_startz00_4041;

						BgL_startz00_4041 = BINT(0L);
						{	/* Cfa/walk.scm 24 */
							obj_t BgL_endz00_4042;

							BgL_endz00_4042 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4040)));
							{	/* Cfa/walk.scm 24 */

								BgL_cportz00_4033 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4040, BgL_startz00_4041, BgL_endz00_4042);
				}}}}
				{
					long BgL_iz00_4034;

					BgL_iz00_4034 = 2L;
				BgL_loopz00_4035:
					if ((BgL_iz00_4034 == -1L))
						{	/* Cfa/walk.scm 24 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/walk.scm 24 */
							{	/* Cfa/walk.scm 24 */
								obj_t BgL_arg1967z00_4036;

								{	/* Cfa/walk.scm 24 */

									{	/* Cfa/walk.scm 24 */
										obj_t BgL_locationz00_4038;

										BgL_locationz00_4038 = BBOOL(((bool_t) 0));
										{	/* Cfa/walk.scm 24 */

											BgL_arg1967z00_4036 =
												BGl_readz00zz__readerz00(BgL_cportz00_4033,
												BgL_locationz00_4038);
										}
									}
								}
								{	/* Cfa/walk.scm 24 */
									int BgL_tmpz00_4071;

									BgL_tmpz00_4071 = (int) (BgL_iz00_4034);
									CNST_TABLE_SET(BgL_tmpz00_4071, BgL_arg1967z00_4036);
							}}
							{	/* Cfa/walk.scm 24 */
								int BgL_auxz00_4039;

								BgL_auxz00_4039 = (int) ((BgL_iz00_4034 - 1L));
								{
									long BgL_iz00_4076;

									BgL_iz00_4076 = (long) (BgL_auxz00_4039);
									BgL_iz00_4034 = BgL_iz00_4076;
									goto BgL_loopz00_4035;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_walkz00(void)
	{
		{	/* Cfa/walk.scm 24 */
			return bgl_gc_roots_register();
		}

	}



/* append-21011 */
	obj_t BGl_appendzd221011zd2zzcfa_walkz00(obj_t BgL_l1z00_1, obj_t BgL_l2z00_2)
	{
		{
			{
				obj_t BgL_headz00_3405;

				BgL_headz00_3405 = MAKE_YOUNG_PAIR(BNIL, BgL_l2z00_2);
				{
					obj_t BgL_prevz00_3406;
					obj_t BgL_tailz00_3407;

					BgL_prevz00_3406 = BgL_headz00_3405;
					BgL_tailz00_3407 = BgL_l1z00_1;
				BgL_loopz00_3408:
					if (PAIRP(BgL_tailz00_3407))
						{
							obj_t BgL_newzd2prevzd2_3410;

							BgL_newzd2prevzd2_3410 =
								MAKE_YOUNG_PAIR(CAR(BgL_tailz00_3407), BgL_l2z00_2);
							SET_CDR(BgL_prevz00_3406, BgL_newzd2prevzd2_3410);
							{
								obj_t BgL_tailz00_4086;
								obj_t BgL_prevz00_4085;

								BgL_prevz00_4085 = BgL_newzd2prevzd2_3410;
								BgL_tailz00_4086 = CDR(BgL_tailz00_3407);
								BgL_tailz00_3407 = BgL_tailz00_4086;
								BgL_prevz00_3406 = BgL_prevz00_4085;
								goto BgL_loopz00_3408;
							}
						}
					else
						{
							BNIL;
						}
					return CDR(BgL_headz00_3405);
				}
			}
		}

	}



/* cfa-walk! */
	BGL_EXPORTED_DEF obj_t BGl_cfazd2walkz12zc0zzcfa_walkz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Cfa/walk.scm 62 */
			{	/* Cfa/walk.scm 63 */
				obj_t BgL_list1562z00_3413;

				{	/* Cfa/walk.scm 63 */
					obj_t BgL_arg1564z00_3414;

					{	/* Cfa/walk.scm 63 */
						obj_t BgL_arg1565z00_3415;

						BgL_arg1565z00_3415 =
							MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
						BgL_arg1564z00_3414 =
							MAKE_YOUNG_PAIR(BGl_string1957z00zzcfa_walkz00,
							BgL_arg1565z00_3415);
					}
					BgL_list1562z00_3413 =
						MAKE_YOUNG_PAIR(BGl_string1958z00zzcfa_walkz00,
						BgL_arg1564z00_3414);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1562z00_3413);
			}
			BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00 = BINT(0L);
			BGl_za2currentzd2passza2zd2zzengine_passz00 =
				BGl_string1957z00zzcfa_walkz00;
			{	/* Cfa/walk.scm 63 */
				obj_t BgL_g1178z00_3416;

				BgL_g1178z00_3416 = BNIL;
				{
					obj_t BgL_hooksz00_3419;
					obj_t BgL_hnamesz00_3420;

					BgL_hooksz00_3419 = BgL_g1178z00_3416;
					BgL_hnamesz00_3420 = BNIL;
				BgL_zc3z04anonymousza31566ze3z87_3421:
					if (NULLP(BgL_hooksz00_3419))
						{	/* Cfa/walk.scm 63 */
							CNST_TABLE_REF(0);
						}
					else
						{	/* Cfa/walk.scm 63 */
							bool_t BgL_test1983z00_4099;

							{	/* Cfa/walk.scm 63 */
								obj_t BgL_fun1576z00_3428;

								BgL_fun1576z00_3428 = CAR(((obj_t) BgL_hooksz00_3419));
								BgL_test1983z00_4099 =
									CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1576z00_3428));
							}
							if (BgL_test1983z00_4099)
								{	/* Cfa/walk.scm 63 */
									obj_t BgL_arg1571z00_3425;
									obj_t BgL_arg1573z00_3426;

									BgL_arg1571z00_3425 = CDR(((obj_t) BgL_hooksz00_3419));
									BgL_arg1573z00_3426 = CDR(((obj_t) BgL_hnamesz00_3420));
									{
										obj_t BgL_hnamesz00_4111;
										obj_t BgL_hooksz00_4110;

										BgL_hooksz00_4110 = BgL_arg1571z00_3425;
										BgL_hnamesz00_4111 = BgL_arg1573z00_3426;
										BgL_hnamesz00_3420 = BgL_hnamesz00_4111;
										BgL_hooksz00_3419 = BgL_hooksz00_4110;
										goto BgL_zc3z04anonymousza31566ze3z87_3421;
									}
								}
							else
								{	/* Cfa/walk.scm 63 */
									obj_t BgL_arg1575z00_3427;

									BgL_arg1575z00_3427 = CAR(((obj_t) BgL_hnamesz00_3420));
									BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string1957z00zzcfa_walkz00,
										BGl_string1959z00zzcfa_walkz00, BgL_arg1575z00_3427);
								}
						}
				}
			}
			BGl_collectzd2allzd2approxz12z12zzcfa_collectz00(BgL_globalsz00_3);
			BGl_declarezd2approxzd2setsz12z12zzcfa_approxz00();
			BGl_setzd2initialzd2approxz12z12zzcfa_setupz00(BgL_globalsz00_3);
			{	/* Cfa/walk.scm 73 */
				obj_t BgL_iterationzd2rootszd2_3431;

				BgL_iterationzd2rootszd2_3431 =
					BGl_cfazd2iteratezd2tozd2fixpointz12zc0zzcfa_iteratez00
					(BgL_globalsz00_3);
				BGl_showzd2cfazd2nbzd2iterationszd2zzcfa_showz00();
				{	/* Cfa/walk.scm 77 */
					obj_t BgL_globalsz00_3432;

					BgL_globalsz00_3432 =
						BGl_removezd2varzd2zzast_removez00(CNST_TABLE_REF(1),
						BgL_globalsz00_3);
					BGl_showzd2cfazd2resultsz00zzcfa_showz00(BgL_globalsz00_3432);
					{	/* Cfa/walk.scm 81 */
						obj_t BgL_additionalz00_3433;

						BgL_additionalz00_3433 =
							BGl_vectorzd2ze3tvectorz12z23zzcfa_tvectorz00
							(BgL_globalsz00_3432);
						BGl_closurezd2optimiza7ationz12z67zzcfa_closurez00
							(BgL_globalsz00_3432);
						BGl_typezd2settingsz12zc0zzcfa_typez00(BgL_globalsz00_3432);
						BGl_specializa7ez12zb5zzcfa_specializa7eza7(BgL_globalsz00_3432);
						{	/* Cfa/walk.scm 89 */
							obj_t BgL_valuez00_3434;

							BgL_valuez00_3434 =
								BGl_shrinkifyz12z12zzast_shrinkifyz00
								(BGl_appendzd221011zd2zzcfa_walkz00(BgL_additionalz00_3433,
									BgL_globalsz00_3432));
							if (((long)
									CINT(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00) >
									0L))
								{	/* Cfa/walk.scm 89 */
									{	/* Cfa/walk.scm 89 */
										obj_t BgL_port1554z00_3436;

										{	/* Cfa/walk.scm 89 */
											obj_t BgL_tmpz00_4132;

											BgL_tmpz00_4132 = BGL_CURRENT_DYNAMIC_ENV();
											BgL_port1554z00_3436 =
												BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_4132);
										}
										bgl_display_obj
											(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
											BgL_port1554z00_3436);
										bgl_display_string(BGl_string1960z00zzcfa_walkz00,
											BgL_port1554z00_3436);
										{	/* Cfa/walk.scm 89 */
											obj_t BgL_arg1584z00_3437;

											{	/* Cfa/walk.scm 89 */
												bool_t BgL_test1985z00_4137;

												if (BGl_integerzf3zf3zz__r4_numbers_6_5_fixnumz00
													(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
													{	/* Cfa/walk.scm 89 */
														if (INTEGERP
															(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00))
															{	/* Cfa/walk.scm 89 */
																BgL_test1985z00_4137 =
																	(
																	(long)
																	CINT
																	(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00)
																	> 1L);
															}
														else
															{	/* Cfa/walk.scm 89 */
																BgL_test1985z00_4137 =
																	BGl_2ze3ze3zz__r4_numbers_6_5z00
																	(BGl_za2nbzd2errorzd2onzd2passza2zd2zztools_errorz00,
																	BINT(1L));
															}
													}
												else
													{	/* Cfa/walk.scm 89 */
														BgL_test1985z00_4137 = ((bool_t) 0);
													}
												if (BgL_test1985z00_4137)
													{	/* Cfa/walk.scm 89 */
														BgL_arg1584z00_3437 =
															BGl_string1961z00zzcfa_walkz00;
													}
												else
													{	/* Cfa/walk.scm 89 */
														BgL_arg1584z00_3437 =
															BGl_string1962z00zzcfa_walkz00;
													}
											}
											bgl_display_obj(BgL_arg1584z00_3437,
												BgL_port1554z00_3436);
										}
										bgl_display_string(BGl_string1963z00zzcfa_walkz00,
											BgL_port1554z00_3436);
										bgl_display_char(((unsigned char) 10),
											BgL_port1554z00_3436);
									}
									{	/* Cfa/walk.scm 89 */
										obj_t BgL_list1587z00_3441;

										BgL_list1587z00_3441 = MAKE_YOUNG_PAIR(BINT(-1L), BNIL);
										BGL_TAIL return
											BGl_exitz00zz__errorz00(BgL_list1587z00_3441);
									}
								}
							else
								{	/* Cfa/walk.scm 89 */
									obj_t BgL_g1180z00_3442;
									obj_t BgL_g1181z00_3443;

									{	/* Cfa/walk.scm 89 */
										obj_t BgL_list1599z00_3456;

										{	/* Cfa/walk.scm 89 */
											obj_t BgL_arg1602z00_3457;

											{	/* Cfa/walk.scm 89 */
												obj_t BgL_arg1605z00_3458;

												BgL_arg1605z00_3458 =
													MAKE_YOUNG_PAIR
													(BGl_unpatchzd2pairzd2setz12zd2envzc0zzcfa_pairz00,
													BNIL);
												BgL_arg1602z00_3457 =
													MAKE_YOUNG_PAIR
													(BGl_unpatchzd2vectorzd2setz12zd2envzc0zzcfa_tvectorz00,
													BgL_arg1605z00_3458);
											}
											BgL_list1599z00_3456 =
												MAKE_YOUNG_PAIR
												(BGl_stopzd2closurezd2cachezd2envzd2zzcfa_closurez00,
												BgL_arg1602z00_3457);
										}
										BgL_g1180z00_3442 = BgL_list1599z00_3456;
									}
									BgL_g1181z00_3443 = CNST_TABLE_REF(2);
									{
										obj_t BgL_hooksz00_3445;
										obj_t BgL_hnamesz00_3446;

										BgL_hooksz00_3445 = BgL_g1180z00_3442;
										BgL_hnamesz00_3446 = BgL_g1181z00_3443;
									BgL_zc3z04anonymousza31588ze3z87_3447:
										if (NULLP(BgL_hooksz00_3445))
											{	/* Cfa/walk.scm 89 */
												return BgL_valuez00_3434;
											}
										else
											{	/* Cfa/walk.scm 89 */
												bool_t BgL_test1989z00_4158;

												{	/* Cfa/walk.scm 89 */
													obj_t BgL_fun1597z00_3454;

													BgL_fun1597z00_3454 =
														CAR(((obj_t) BgL_hooksz00_3445));
													BgL_test1989z00_4158 =
														CBOOL(BGL_PROCEDURE_CALL0(BgL_fun1597z00_3454));
												}
												if (BgL_test1989z00_4158)
													{	/* Cfa/walk.scm 89 */
														obj_t BgL_arg1593z00_3451;
														obj_t BgL_arg1594z00_3452;

														BgL_arg1593z00_3451 =
															CDR(((obj_t) BgL_hooksz00_3445));
														BgL_arg1594z00_3452 =
															CDR(((obj_t) BgL_hnamesz00_3446));
														{
															obj_t BgL_hnamesz00_4170;
															obj_t BgL_hooksz00_4169;

															BgL_hooksz00_4169 = BgL_arg1593z00_3451;
															BgL_hnamesz00_4170 = BgL_arg1594z00_3452;
															BgL_hnamesz00_3446 = BgL_hnamesz00_4170;
															BgL_hooksz00_3445 = BgL_hooksz00_4169;
															goto BgL_zc3z04anonymousza31588ze3z87_3447;
														}
													}
												else
													{	/* Cfa/walk.scm 89 */
														obj_t BgL_arg1595z00_3453;

														BgL_arg1595z00_3453 =
															CAR(((obj_t) BgL_hnamesz00_3446));
														return
															BGl_internalzd2errorzd2zztools_errorz00
															(BGl_za2currentzd2passza2zd2zzengine_passz00,
															BGl_string1964z00zzcfa_walkz00,
															BgL_arg1595z00_3453);
													}
											}
									}
								}
						}
					}
				}
			}
		}

	}



/* &cfa-walk! */
	obj_t BGl_z62cfazd2walkz12za2zzcfa_walkz00(obj_t BgL_envz00_4028,
		obj_t BgL_globalsz00_4029)
	{
		{	/* Cfa/walk.scm 62 */
			return BGl_cfazd2walkz12zc0zzcfa_walkz00(BgL_globalsz00_4029);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_walkz00(void)
	{
		{	/* Cfa/walk.scm 24 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_walkz00(void)
	{
		{	/* Cfa/walk.scm 24 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_walkz00(void)
	{
		{	/* Cfa/walk.scm 24 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_walkz00(void)
	{
		{	/* Cfa/walk.scm 24 */
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_passz00(373082237L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzast_removez00(383247627L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzast_shrinkifyz00(45200572L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzwrite_astz00(250486252L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzcfa_collectz00(220306886L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzcfa_setupz00(168272122L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzcfa_iteratez00(131412063L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzcfa_showz00(391833753L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzcfa_info3z00(0L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzcfa_typez00(93933605L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzcfa_closurez00(189402713L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzcfa_specializa7eza7(374743292L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(324810626L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			BGl_modulezd2initializa7ationz75zzcfa_pairz00(37668556L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_arithmeticz00(185547682L,
				BSTRING_TO_STRING(BGl_string1965z00zzcfa_walkz00));
		}

	}

#ifdef __cplusplus
}
#endif
