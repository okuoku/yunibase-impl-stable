/*===========================================================================*/
/*   (Cfa/procedure.scm)                                                     */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/procedure.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_PROCEDURE_TYPE_DEFINITIONS
#define BGL_CFA_PROCEDURE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_externzd2sfunzf2cinfoz20_bgl
	{
		bool_t BgL_polymorphiczf3zf3;
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                               *BgL_externzd2sfunzf2cinfoz20_bglt;

	typedef struct BgL_internzd2sfunzf2cinfoz20_bgl
	{
		bool_t BgL_polymorphiczf3zf3;
		struct BgL_approxz00_bgl *BgL_approxz00;
		long BgL_stampz00;
	}                               *BgL_internzd2sfunzf2cinfoz20_bglt;

	typedef struct BgL_prezd2clozd2envz00_bgl
	{
	}                         *BgL_prezd2clozd2envz00_bglt;

	typedef struct BgL_svarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_clozd2envzf3z21;
		long BgL_stampz00;
	}                      *BgL_svarzf2cinfozf2_bglt;

	typedef struct BgL_prezd2makezd2procedurezd2appzd2_bgl
	{
		struct BgL_variablez00_bgl *BgL_ownerz00;
	}                                     
		*BgL_prezd2makezd2procedurezd2appzd2_bglt;

	typedef struct BgL_prezd2procedurezd2refzd2appzd2_bgl
	{
	}                                    
		*BgL_prezd2procedurezd2refzd2appzd2_bglt;

	typedef struct BgL_prezd2procedurezd2setz12zd2appzc0_bgl
	{
	}                                       
		*BgL_prezd2procedurezd2setz12zd2appzc0_bglt;

	typedef struct BgL_makezd2procedurezd2appz00_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		obj_t BgL_valueszd2approxzd2;
		long BgL_lostzd2stampzd2;
		bool_t BgL_xzd2tzf3z21;
		bool_t BgL_xz00;
		bool_t BgL_tz00;
		struct BgL_variablez00_bgl *BgL_ownerz00;
		obj_t BgL_stackzd2stampzd2;
	}                                *BgL_makezd2procedurezd2appz00_bglt;

	typedef struct BgL_procedurezd2refzd2appz00_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                               *BgL_procedurezd2refzd2appz00_bglt;

	typedef struct BgL_procedurezd2setz12zd2appz12_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		obj_t BgL_vapproxz00;
	}                                  *BgL_procedurezd2setz12zd2appz12_bglt;


#endif													// BGL_CFA_PROCEDURE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_continuezd2cfaz12zc0zzcfa_iteratez00(obj_t);
	extern obj_t
		BGl_addzd2makezd2procedurez12z12zzcfa_closurez00(BgL_nodez00_bglt);
	static obj_t BGl_objectzd2initzd2zzcfa_procedurez00(void);
	static obj_t
		BGl_z62setzd2procedurezd2approxzd2polymorphicz12za2zzcfa_procedurez00(obj_t,
		obj_t);
	extern obj_t BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00(obj_t,
		BgL_approxz00_bglt);
	BGL_IMPORT obj_t make_vector(long, obj_t);
	extern obj_t BGl_reshapedzd2localzd2zzcfa_infoz00;
	static obj_t
		BGl_z62loosezd2allocz12zd2makezd2pr1535za2zzcfa_procedurez00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcfa_procedurez00(void);
	BGL_IMPORT obj_t bgl_typeof(obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2procedurezd2setz12zd21533zb0zzcfa_procedurez00(obj_t,
		obj_t);
	extern obj_t BGl_procedurezd2refzd2appz00zzcfa_info2z00;
	extern obj_t BGl_prezd2clozd2envz00zzcfa_infoz00;
	extern obj_t
		BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00;
	extern obj_t
		BGl_getzd2nodezd2atomzd2valuezd2zzcfa_approxz00(BgL_nodez00_bglt);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t BGl_loosezd2argz12zc0zzcfa_procedurez00(obj_t);
	extern BgL_approxz00_bglt
		BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_approxz00_bglt);
	BGL_EXPORTED_DECL obj_t
		BGl_disablezd2Xzd2Tz12z12zzcfa_procedurez00(BgL_approxz00_bglt, obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_procedurez00(void);
	extern obj_t BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
	extern BgL_approxz00_bglt BGl_makezd2emptyzd2approxz00zzcfa_approxz00(void);
	static obj_t BGl_z62zc3z04anonymousza31740ze3ze5zzcfa_procedurez00(obj_t,
		obj_t);
	extern obj_t BGl_prezd2procedurezd2setz12zd2appzc0zzcfa_info2z00;
	static obj_t BGl_z62zc3z04anonymousza31832ze3ze5zzcfa_procedurez00(obj_t,
		obj_t);
	extern obj_t BGl_prezd2procedurezd2refzd2appzd2zzcfa_info2z00;
	extern obj_t BGl_svarzf2Cinfozf2zzcfa_infoz00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_makezd2procedurezd2appz00zzcfa_info2z00;
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_za2cfazd2stampza2zd2zzcfa_iteratez00;
	extern obj_t BGl_cfazd2exportzd2varz12z12zzcfa_iteratez00(BgL_valuez00_bglt,
		obj_t);
	extern obj_t BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_approxz00_bglt);
	extern obj_t BGl_nodezd2setupza2z12z62zzcfa_setupz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_procedurez00 = BUNSPEC;
	extern obj_t BGl_funz00zzast_varz00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_z62disablezd2Xzd2Tz12z70zzcfa_procedurez00(obj_t, obj_t,
		obj_t);
	extern obj_t
		BGl_addzd2procedurezd2refz12z12zzcfa_closurez00(BgL_nodez00_bglt);
	static obj_t BGl_toplevelzd2initzd2zzcfa_procedurez00(void);
	static obj_t BGl_genericzd2initzd2zzcfa_procedurez00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern BgL_approxz00_bglt BGl_loosez12z12zzcfa_loosez00(BgL_approxz00_bglt,
		obj_t);
	static obj_t BGl_z62disablezd2allocz12za2zzcfa_procedurez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern BgL_approxz00_bglt
		BGl_makezd2typezd2alloczd2approxzd2zzcfa_approxz00(BgL_typez00_bglt,
		BgL_nodez00_bglt);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	extern obj_t
		BGl_za2optimzd2cfazd2freezd2varzd2trackingzf3za2zf3zzengine_paramz00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzcfa_procedurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_closurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_iteratez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_cfaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_setupz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_loosez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	BGL_IMPORT obj_t BGl_tprintz00zz__r4_output_6_10_3z00(obj_t, obj_t);
	static obj_t
		BGl_z62nodezd2setupz12zd2prezd2make1523za2zzcfa_procedurez00(obj_t, obj_t);
	static obj_t BGl_cnstzd2initzd2zzcfa_procedurez00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_procedurez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_procedurez00(void);
	extern BgL_approxz00_bglt
		BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_typez00_bglt);
	extern obj_t BGl_procedurezd2setz12zd2appz12zzcfa_info2z00;
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	BGL_EXPORTED_DECL obj_t
		BGl_setzd2procedurezd2approxzd2polymorphicz12zc0zzcfa_procedurez00
		(BgL_appz00_bglt);
	extern obj_t BGl_prezd2makezd2procedurezd2appzd2zzcfa_info2z00;
	extern BgL_approxz00_bglt BGl_cfaz12z12zzcfa_cfaz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2procedurezd2refzd2a1531za2zzcfa_procedurez00(obj_t, obj_t);
	static obj_t
		BGl_z62nodezd2setupz12zd2prezd2proc1525za2zzcfa_procedurez00(obj_t, obj_t);
	static obj_t
		BGl_z62nodezd2setupz12zd2prezd2proc1527za2zzcfa_procedurez00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2makezd2procedurezd21529za2zzcfa_procedurez00(obj_t, obj_t);
	static obj_t BGl_z62zc3z04anonymousza31845ze3ze5zzcfa_procedurez00(obj_t,
		obj_t);
	static obj_t BGl_z62zc3z04anonymousza31756ze3ze5zzcfa_procedurez00(obj_t,
		obj_t);
	static obj_t __cnst[1];


	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2069z00zzcfa_procedurez00,
		BgL_bgl_za762disableza7d2all2089z00,
		BGl_z62disablezd2allocz12za2zzcfa_procedurez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2076z00zzcfa_procedurez00,
		BgL_bgl_za762nodeza7d2setupza72090za7,
		BGl_z62nodezd2setupz12zd2prezd2make1523za2zzcfa_procedurez00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2078z00zzcfa_procedurez00,
		BgL_bgl_za762nodeza7d2setupza72091za7,
		BGl_z62nodezd2setupz12zd2prezd2proc1525za2zzcfa_procedurez00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2079z00zzcfa_procedurez00,
		BgL_bgl_za762nodeza7d2setupza72092za7,
		BGl_z62nodezd2setupz12zd2prezd2proc1527za2zzcfa_procedurez00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2080z00zzcfa_procedurez00,
		BgL_bgl_za762cfaza712za7d2make2093za7,
		BGl_z62cfaz12zd2makezd2procedurezd21529za2zzcfa_procedurez00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2082z00zzcfa_procedurez00,
		BgL_bgl_za762cfaza712za7d2proc2094za7,
		BGl_z62cfaz12zd2procedurezd2refzd2a1531za2zzcfa_procedurez00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2083z00zzcfa_procedurez00,
		BgL_bgl_za762cfaza712za7d2proc2095za7,
		BGl_z62cfaz12zd2procedurezd2setz12zd21533zb0zzcfa_procedurez00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2084z00zzcfa_procedurez00,
		BgL_bgl_za762looseza7d2alloc2096z00,
		BGl_z62loosezd2allocz12zd2makezd2pr1535za2zzcfa_procedurez00, 0L, BUNSPEC,
		1);
	extern obj_t BGl_cfaz12zd2envzc0zzcfa_cfaz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_setzd2procedurezd2approxzd2polymorphicz12zd2envz12zzcfa_procedurez00,
		BgL_bgl_za762setza7d2procedu2097z00,
		BGl_z62setzd2procedurezd2approxzd2polymorphicz12za2zzcfa_procedurez00, 0L,
		BUNSPEC, 1);
	extern obj_t BGl_nodezd2setupz12zd2envz12zzcfa_setupz00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_disablezd2Xzd2Tz12zd2envzc0zzcfa_procedurez00,
		BgL_bgl_za762disableza7d2xza7d2098za7,
		BGl_z62disablezd2Xzd2Tz12z70zzcfa_procedurez00, 0L, BUNSPEC, 2);
	      DEFINE_STRING(BGl_string2070z00zzcfa_procedurez00,
		BgL_bgl_string2070za700za7za7c2099za7, "polymorphic intern fun", 22);
	      DEFINE_STRING(BGl_string2071z00zzcfa_procedurez00,
		BgL_bgl_string2071za700za7za7c2100za7, "polymorphic extern fun", 22);
	      DEFINE_STRING(BGl_string2072z00zzcfa_procedurez00,
		BgL_bgl_string2072za700za7za7c2101za7, "INTERNAL CFA ERROR: ", 20);
	      DEFINE_STRING(BGl_string2073z00zzcfa_procedurez00,
		BgL_bgl_string2073za700za7za7c2102za7, ":", 1);
	      DEFINE_STRING(BGl_string2074z00zzcfa_procedurez00,
		BgL_bgl_string2074za700za7za7c2103za7, ",", 1);
	      DEFINE_STRING(BGl_string2075z00zzcfa_procedurez00,
		BgL_bgl_string2075za700za7za7c2104za7, "Cfa/procedure.scm", 17);
	      DEFINE_STRING(BGl_string2077z00zzcfa_procedurez00,
		BgL_bgl_string2077za700za7za7c2105za7, "node-setup!", 11);
	      DEFINE_STRING(BGl_string2081z00zzcfa_procedurez00,
		BgL_bgl_string2081za700za7za7c2106za7, "cfa!::approx", 12);
	      DEFINE_STRING(BGl_string2085z00zzcfa_procedurez00,
		BgL_bgl_string2085za700za7za7c2107za7, "loose-alloc!", 12);
	      DEFINE_STRING(BGl_string2086z00zzcfa_procedurez00,
		BgL_bgl_string2086za700za7za7c2108za7, "cfa_procedure", 13);
	      DEFINE_STRING(BGl_string2087z00zzcfa_procedurez00,
		BgL_bgl_string2087za700za7za7c2109za7, "all ", 4);
	extern obj_t BGl_loosezd2allocz12zd2envz12zzcfa_loosez00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzcfa_procedurez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_procedurez00(long
		BgL_checksumz00_4893, char *BgL_fromz00_4894)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_procedurez00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_procedurez00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_procedurez00();
					BGl_libraryzd2moduleszd2initz00zzcfa_procedurez00();
					BGl_cnstzd2initzd2zzcfa_procedurez00();
					BGl_importedzd2moduleszd2initz00zzcfa_procedurez00();
					BGl_methodzd2initzd2zzcfa_procedurez00();
					return BGl_toplevelzd2initzd2zzcfa_procedurez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_procedurez00(void)
	{
		{	/* Cfa/procedure.scm 15 */
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cfa_procedure");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_procedure");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_procedure");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"cfa_procedure");
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L,
				"cfa_procedure");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"cfa_procedure");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cfa_procedure");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_procedure");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cfa_procedure");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"cfa_procedure");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_procedure");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_procedurez00(void)
	{
		{	/* Cfa/procedure.scm 15 */
			{	/* Cfa/procedure.scm 15 */
				obj_t BgL_cportz00_4614;

				{	/* Cfa/procedure.scm 15 */
					obj_t BgL_stringz00_4621;

					BgL_stringz00_4621 = BGl_string2087z00zzcfa_procedurez00;
					{	/* Cfa/procedure.scm 15 */
						obj_t BgL_startz00_4622;

						BgL_startz00_4622 = BINT(0L);
						{	/* Cfa/procedure.scm 15 */
							obj_t BgL_endz00_4623;

							BgL_endz00_4623 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4621)));
							{	/* Cfa/procedure.scm 15 */

								BgL_cportz00_4614 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4621, BgL_startz00_4622, BgL_endz00_4623);
				}}}}
				{
					long BgL_iz00_4615;

					BgL_iz00_4615 = 0L;
				BgL_loopz00_4616:
					if ((BgL_iz00_4615 == -1L))
						{	/* Cfa/procedure.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/procedure.scm 15 */
							{	/* Cfa/procedure.scm 15 */
								obj_t BgL_arg2088z00_4617;

								{	/* Cfa/procedure.scm 15 */

									{	/* Cfa/procedure.scm 15 */
										obj_t BgL_locationz00_4619;

										BgL_locationz00_4619 = BBOOL(((bool_t) 0));
										{	/* Cfa/procedure.scm 15 */

											BgL_arg2088z00_4617 =
												BGl_readz00zz__readerz00(BgL_cportz00_4614,
												BgL_locationz00_4619);
										}
									}
								}
								{	/* Cfa/procedure.scm 15 */
									int BgL_tmpz00_4924;

									BgL_tmpz00_4924 = (int) (BgL_iz00_4615);
									CNST_TABLE_SET(BgL_tmpz00_4924, BgL_arg2088z00_4617);
							}}
							{	/* Cfa/procedure.scm 15 */
								int BgL_auxz00_4620;

								BgL_auxz00_4620 = (int) ((BgL_iz00_4615 - 1L));
								{
									long BgL_iz00_4929;

									BgL_iz00_4929 = (long) (BgL_auxz00_4620);
									BgL_iz00_4615 = BgL_iz00_4929;
									goto BgL_loopz00_4616;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_procedurez00(void)
	{
		{	/* Cfa/procedure.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_procedurez00(void)
	{
		{	/* Cfa/procedure.scm 15 */
			return BUNSPEC;
		}

	}



/* disable-X-T! */
	BGL_EXPORTED_DEF obj_t
		BGl_disablezd2Xzd2Tz12z12zzcfa_procedurez00(BgL_approxz00_bglt
		BgL_approxz00_3, obj_t BgL_reasonz00_4)
	{
		{	/* Cfa/procedure.scm 38 */
			if (
				(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_3))->
					BgL_haszd2procedurezf3z21))
				{	/* Cfa/procedure.scm 56 */
					return
						BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
						(BGl_proc2069z00zzcfa_procedurez00, BgL_approxz00_3);
				}
			else
				{	/* Cfa/procedure.scm 56 */
					return BFALSE;
				}
		}

	}



/* &disable-X-T! */
	obj_t BGl_z62disablezd2Xzd2Tz12z70zzcfa_procedurez00(obj_t BgL_envz00_4559,
		obj_t BgL_approxz00_4560, obj_t BgL_reasonz00_4561)
	{
		{	/* Cfa/procedure.scm 38 */
			return
				BGl_disablezd2Xzd2Tz12z12zzcfa_procedurez00(
				((BgL_approxz00_bglt) BgL_approxz00_4560), BgL_reasonz00_4561);
		}

	}



/* &disable-alloc! */
	obj_t BGl_z62disablezd2allocz12za2zzcfa_procedurez00(obj_t BgL_envz00_4562,
		obj_t BgL_appz00_4563)
	{
		{	/* Cfa/procedure.scm 53 */
			{	/* Cfa/procedure.scm 41 */
				bool_t BgL_tmpz00_4937;

				{	/* Cfa/procedure.scm 41 */
					bool_t BgL_test2113z00_4938;

					{	/* Cfa/procedure.scm 41 */
						obj_t BgL_classz00_4625;

						BgL_classz00_4625 = BGl_makezd2procedurezd2appz00zzcfa_info2z00;
						if (BGL_OBJECTP(BgL_appz00_4563))
							{	/* Cfa/procedure.scm 41 */
								BgL_objectz00_bglt BgL_arg1807z00_4626;

								BgL_arg1807z00_4626 = (BgL_objectz00_bglt) (BgL_appz00_4563);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/procedure.scm 41 */
										long BgL_idxz00_4627;

										BgL_idxz00_4627 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4626);
										BgL_test2113z00_4938 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4627 + 4L)) == BgL_classz00_4625);
									}
								else
									{	/* Cfa/procedure.scm 41 */
										bool_t BgL_res2047z00_4630;

										{	/* Cfa/procedure.scm 41 */
											obj_t BgL_oclassz00_4631;

											{	/* Cfa/procedure.scm 41 */
												obj_t BgL_arg1815z00_4632;
												long BgL_arg1816z00_4633;

												BgL_arg1815z00_4632 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/procedure.scm 41 */
													long BgL_arg1817z00_4634;

													BgL_arg1817z00_4634 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4626);
													BgL_arg1816z00_4633 =
														(BgL_arg1817z00_4634 - OBJECT_TYPE);
												}
												BgL_oclassz00_4631 =
													VECTOR_REF(BgL_arg1815z00_4632, BgL_arg1816z00_4633);
											}
											{	/* Cfa/procedure.scm 41 */
												bool_t BgL__ortest_1115z00_4635;

												BgL__ortest_1115z00_4635 =
													(BgL_classz00_4625 == BgL_oclassz00_4631);
												if (BgL__ortest_1115z00_4635)
													{	/* Cfa/procedure.scm 41 */
														BgL_res2047z00_4630 = BgL__ortest_1115z00_4635;
													}
												else
													{	/* Cfa/procedure.scm 41 */
														long BgL_odepthz00_4636;

														{	/* Cfa/procedure.scm 41 */
															obj_t BgL_arg1804z00_4637;

															BgL_arg1804z00_4637 = (BgL_oclassz00_4631);
															BgL_odepthz00_4636 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4637);
														}
														if ((4L < BgL_odepthz00_4636))
															{	/* Cfa/procedure.scm 41 */
																obj_t BgL_arg1802z00_4638;

																{	/* Cfa/procedure.scm 41 */
																	obj_t BgL_arg1803z00_4639;

																	BgL_arg1803z00_4639 = (BgL_oclassz00_4631);
																	BgL_arg1802z00_4638 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4639,
																		4L);
																}
																BgL_res2047z00_4630 =
																	(BgL_arg1802z00_4638 == BgL_classz00_4625);
															}
														else
															{	/* Cfa/procedure.scm 41 */
																BgL_res2047z00_4630 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2113z00_4938 = BgL_res2047z00_4630;
									}
							}
						else
							{	/* Cfa/procedure.scm 41 */
								BgL_test2113z00_4938 = ((bool_t) 0);
							}
					}
					if (BgL_test2113z00_4938)
						{	/* Cfa/procedure.scm 41 */
							{
								BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_4961;

								{
									obj_t BgL_auxz00_4962;

									{	/* Cfa/procedure.scm 47 */
										BgL_objectz00_bglt BgL_tmpz00_4963;

										BgL_tmpz00_4963 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_appz00_4563));
										BgL_auxz00_4962 = BGL_OBJECT_WIDENING(BgL_tmpz00_4963);
									}
									BgL_auxz00_4961 =
										((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_4962);
								}
								((((BgL_makezd2procedurezd2appz00_bglt)
											COBJECT(BgL_auxz00_4961))->BgL_xzd2tzf3z21) =
									((bool_t) ((bool_t) 0)), BUNSPEC);
							}
							BGl_setzd2procedurezd2approxzd2polymorphicz12zc0zzcfa_procedurez00
								(((BgL_appz00_bglt) BgL_appz00_4563));
							if (CBOOL
								(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00))
								{	/* Cfa/procedure.scm 50 */
									obj_t BgL_calleez00_4640;

									{	/* Cfa/procedure.scm 50 */
										obj_t BgL_pairz00_4641;

										BgL_pairz00_4641 =
											(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt)
														((BgL_appz00_bglt) BgL_appz00_4563))))->
											BgL_argsz00);
										BgL_calleez00_4640 = CAR(BgL_pairz00_4641);
									}
									{	/* Cfa/procedure.scm 50 */
										BgL_variablez00_bglt BgL_vz00_4642;

										BgL_vz00_4642 =
											(((BgL_varz00_bglt) COBJECT(
													((BgL_varz00_bglt) BgL_calleez00_4640)))->
											BgL_variablez00);
										{	/* Cfa/procedure.scm 51 */
											BgL_valuez00_bglt BgL_funz00_4643;

											BgL_funz00_4643 =
												(((BgL_variablez00_bglt) COBJECT(BgL_vz00_4642))->
												BgL_valuez00);
											{	/* Cfa/procedure.scm 52 */

												{	/* Cfa/procedure.scm 53 */
													obj_t BgL_g1515z00_4644;

													{	/* Cfa/procedure.scm 53 */
														obj_t BgL_pairz00_4645;

														BgL_pairz00_4645 =
															(((BgL_sfunz00_bglt) COBJECT(
																	((BgL_sfunz00_bglt) BgL_funz00_4643)))->
															BgL_argsz00);
														BgL_g1515z00_4644 = CDR(BgL_pairz00_4645);
													}
													{
														obj_t BgL_l1513z00_4647;

														BgL_l1513z00_4647 = BgL_g1515z00_4644;
													BgL_zc3z04anonymousza31549ze3z87_4646:
														if (PAIRP(BgL_l1513z00_4647))
															{	/* Cfa/procedure.scm 53 */
																BGl_loosezd2argz12zc0zzcfa_procedurez00(CAR
																	(BgL_l1513z00_4647));
																{
																	obj_t BgL_l1513z00_4987;

																	BgL_l1513z00_4987 = CDR(BgL_l1513z00_4647);
																	BgL_l1513z00_4647 = BgL_l1513z00_4987;
																	goto BgL_zc3z04anonymousza31549ze3z87_4646;
																}
															}
														else
															{	/* Cfa/procedure.scm 53 */
																BgL_tmpz00_4937 = ((bool_t) 1);
															}
													}
												}
											}
										}
									}
								}
							else
								{	/* Cfa/procedure.scm 49 */
									BgL_tmpz00_4937 = ((bool_t) 0);
								}
						}
					else
						{	/* Cfa/procedure.scm 41 */
							BgL_tmpz00_4937 = ((bool_t) 0);
						}
				}
				return BBOOL(BgL_tmpz00_4937);
			}
		}

	}



/* set-procedure-approx-polymorphic! */
	BGL_EXPORTED_DEF obj_t
		BGl_setzd2procedurezd2approxzd2polymorphicz12zc0zzcfa_procedurez00
		(BgL_appz00_bglt BgL_appz00_12)
	{
		{	/* Cfa/procedure.scm 303 */
			{	/* Cfa/procedure.scm 304 */
				obj_t BgL_calleez00_2999;

				{	/* Cfa/procedure.scm 304 */
					obj_t BgL_pairz00_3783;

					BgL_pairz00_3783 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_appz00_12)))->BgL_argsz00);
					BgL_calleez00_2999 = CAR(BgL_pairz00_3783);
				}
				{	/* Cfa/procedure.scm 304 */
					BgL_variablez00_bglt BgL_vz00_3000;

					BgL_vz00_3000 =
						(((BgL_varz00_bglt) COBJECT(
								((BgL_varz00_bglt) BgL_calleez00_2999)))->BgL_variablez00);
					{	/* Cfa/procedure.scm 305 */
						BgL_valuez00_bglt BgL_funz00_3001;

						BgL_funz00_3001 =
							(((BgL_variablez00_bglt) COBJECT(BgL_vz00_3000))->BgL_valuez00);
						{	/* Cfa/procedure.scm 306 */

							{	/* Cfa/procedure.scm 309 */
								bool_t BgL_test2120z00_4996;

								{	/* Cfa/procedure.scm 309 */
									obj_t BgL_classz00_3786;

									BgL_classz00_3786 = BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
									{	/* Cfa/procedure.scm 309 */
										BgL_objectz00_bglt BgL_arg1807z00_3788;

										{	/* Cfa/procedure.scm 309 */
											obj_t BgL_tmpz00_4997;

											BgL_tmpz00_4997 =
												((obj_t) ((BgL_objectz00_bglt) BgL_funz00_3001));
											BgL_arg1807z00_3788 =
												(BgL_objectz00_bglt) (BgL_tmpz00_4997);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/procedure.scm 309 */
												long BgL_idxz00_3794;

												BgL_idxz00_3794 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3788);
												BgL_test2120z00_4996 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3794 + 4L)) == BgL_classz00_3786);
											}
										else
											{	/* Cfa/procedure.scm 309 */
												bool_t BgL_res2048z00_3819;

												{	/* Cfa/procedure.scm 309 */
													obj_t BgL_oclassz00_3802;

													{	/* Cfa/procedure.scm 309 */
														obj_t BgL_arg1815z00_3810;
														long BgL_arg1816z00_3811;

														BgL_arg1815z00_3810 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/procedure.scm 309 */
															long BgL_arg1817z00_3812;

															BgL_arg1817z00_3812 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3788);
															BgL_arg1816z00_3811 =
																(BgL_arg1817z00_3812 - OBJECT_TYPE);
														}
														BgL_oclassz00_3802 =
															VECTOR_REF(BgL_arg1815z00_3810,
															BgL_arg1816z00_3811);
													}
													{	/* Cfa/procedure.scm 309 */
														bool_t BgL__ortest_1115z00_3803;

														BgL__ortest_1115z00_3803 =
															(BgL_classz00_3786 == BgL_oclassz00_3802);
														if (BgL__ortest_1115z00_3803)
															{	/* Cfa/procedure.scm 309 */
																BgL_res2048z00_3819 = BgL__ortest_1115z00_3803;
															}
														else
															{	/* Cfa/procedure.scm 309 */
																long BgL_odepthz00_3804;

																{	/* Cfa/procedure.scm 309 */
																	obj_t BgL_arg1804z00_3805;

																	BgL_arg1804z00_3805 = (BgL_oclassz00_3802);
																	BgL_odepthz00_3804 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3805);
																}
																if ((4L < BgL_odepthz00_3804))
																	{	/* Cfa/procedure.scm 309 */
																		obj_t BgL_arg1802z00_3807;

																		{	/* Cfa/procedure.scm 309 */
																			obj_t BgL_arg1803z00_3808;

																			BgL_arg1803z00_3808 =
																				(BgL_oclassz00_3802);
																			BgL_arg1802z00_3807 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3808, 4L);
																		}
																		BgL_res2048z00_3819 =
																			(BgL_arg1802z00_3807 ==
																			BgL_classz00_3786);
																	}
																else
																	{	/* Cfa/procedure.scm 309 */
																		BgL_res2048z00_3819 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2120z00_4996 = BgL_res2048z00_3819;
											}
									}
								}
								if (BgL_test2120z00_4996)
									{	/* Cfa/procedure.scm 310 */
										bool_t BgL_test2124z00_5020;

										{
											BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_5021;

											{
												obj_t BgL_auxz00_5022;

												{	/* Cfa/procedure.scm 310 */
													BgL_objectz00_bglt BgL_tmpz00_5023;

													BgL_tmpz00_5023 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_3001));
													BgL_auxz00_5022 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_5023);
												}
												BgL_auxz00_5021 =
													((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_5022);
											}
											BgL_test2124z00_5020 =
												(((BgL_internzd2sfunzf2cinfoz20_bglt)
													COBJECT(BgL_auxz00_5021))->BgL_polymorphiczf3zf3);
										}
										if (BgL_test2124z00_5020)
											{	/* Cfa/procedure.scm 310 */
												return BFALSE;
											}
										else
											{	/* Cfa/procedure.scm 310 */
												BGl_continuezd2cfaz12zc0zzcfa_iteratez00
													(BGl_string2070z00zzcfa_procedurez00);
												{
													BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_5030;

													{
														obj_t BgL_auxz00_5031;

														{	/* Cfa/procedure.scm 312 */
															BgL_objectz00_bglt BgL_tmpz00_5032;

															BgL_tmpz00_5032 =
																((BgL_objectz00_bglt)
																((BgL_sfunz00_bglt) BgL_funz00_3001));
															BgL_auxz00_5031 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_5032);
														}
														BgL_auxz00_5030 =
															((BgL_internzd2sfunzf2cinfoz20_bglt)
															BgL_auxz00_5031);
													}
													return
														((((BgL_internzd2sfunzf2cinfoz20_bglt)
																COBJECT(BgL_auxz00_5030))->
															BgL_polymorphiczf3zf3) =
														((bool_t) ((bool_t) 1)), BUNSPEC);
												}
											}
									}
								else
									{	/* Cfa/procedure.scm 309 */
										{	/* Cfa/procedure.scm 313 */
											BgL_approxz00_bglt BgL_xz00_4611;

											{
												BgL_externzd2sfunzf2cinfoz20_bglt BgL_auxz00_5038;

												{
													obj_t BgL_auxz00_5039;

													{	/* Cfa/procedure.scm 313 */
														BgL_objectz00_bglt BgL_tmpz00_5040;

														BgL_tmpz00_5040 =
															((BgL_objectz00_bglt)
															((BgL_sfunz00_bglt) BgL_funz00_3001));
														BgL_auxz00_5039 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5040);
													}
													BgL_auxz00_5038 =
														((BgL_externzd2sfunzf2cinfoz20_bglt)
														BgL_auxz00_5039);
												}
												BgL_xz00_4611 =
													(((BgL_externzd2sfunzf2cinfoz20_bglt)
														COBJECT(BgL_auxz00_5038))->BgL_approxz00);
											}
											((bool_t) 1);
										}
										{	/* Cfa/procedure.scm 314 */
											bool_t BgL_test2125z00_5046;

											{
												BgL_externzd2sfunzf2cinfoz20_bglt BgL_auxz00_5047;

												{
													obj_t BgL_auxz00_5048;

													{	/* Cfa/procedure.scm 314 */
														BgL_objectz00_bglt BgL_tmpz00_5049;

														BgL_tmpz00_5049 =
															((BgL_objectz00_bglt)
															((BgL_sfunz00_bglt) BgL_funz00_3001));
														BgL_auxz00_5048 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_5049);
													}
													BgL_auxz00_5047 =
														((BgL_externzd2sfunzf2cinfoz20_bglt)
														BgL_auxz00_5048);
												}
												BgL_test2125z00_5046 =
													(((BgL_externzd2sfunzf2cinfoz20_bglt)
														COBJECT(BgL_auxz00_5047))->BgL_polymorphiczf3zf3);
											}
											if (BgL_test2125z00_5046)
												{	/* Cfa/procedure.scm 314 */
													return BFALSE;
												}
											else
												{	/* Cfa/procedure.scm 314 */
													BGl_continuezd2cfaz12zc0zzcfa_iteratez00
														(BGl_string2071z00zzcfa_procedurez00);
													{
														BgL_externzd2sfunzf2cinfoz20_bglt BgL_auxz00_5056;

														{
															obj_t BgL_auxz00_5057;

															{	/* Cfa/procedure.scm 316 */
																BgL_objectz00_bglt BgL_tmpz00_5058;

																BgL_tmpz00_5058 =
																	((BgL_objectz00_bglt)
																	((BgL_sfunz00_bglt) BgL_funz00_3001));
																BgL_auxz00_5057 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5058);
															}
															BgL_auxz00_5056 =
																((BgL_externzd2sfunzf2cinfoz20_bglt)
																BgL_auxz00_5057);
														}
														return
															((((BgL_externzd2sfunzf2cinfoz20_bglt)
																	COBJECT(BgL_auxz00_5056))->
																BgL_polymorphiczf3zf3) =
															((bool_t) ((bool_t) 1)), BUNSPEC);
													}
												}
										}
									}
							}
						}
					}
				}
			}
		}

	}



/* &set-procedure-approx-polymorphic! */
	obj_t
		BGl_z62setzd2procedurezd2approxzd2polymorphicz12za2zzcfa_procedurez00(obj_t
		BgL_envz00_4564, obj_t BgL_appz00_4565)
	{
		{	/* Cfa/procedure.scm 303 */
			return
				BGl_setzd2procedurezd2approxzd2polymorphicz12zc0zzcfa_procedurez00(
				((BgL_appz00_bglt) BgL_appz00_4565));
		}

	}



/* loose-arg! */
	obj_t BGl_loosezd2argz12zc0zzcfa_procedurez00(obj_t BgL_az00_13)
	{
		{	/* Cfa/procedure.scm 321 */
			{	/* Cfa/procedure.scm 322 */
				BgL_typez00_bglt BgL_vz00_3833;

				BgL_vz00_3833 = ((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
				((((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_az00_13))))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_vz00_3833), BUNSPEC);
			}
			{	/* Cfa/procedure.scm 323 */
				bool_t BgL_test2126z00_5070;

				{	/* Cfa/procedure.scm 323 */
					obj_t BgL_classz00_3834;

					BgL_classz00_3834 = BGl_reshapedzd2localzd2zzcfa_infoz00;
					if (BGL_OBJECTP(BgL_az00_13))
						{	/* Cfa/procedure.scm 323 */
							BgL_objectz00_bglt BgL_arg1807z00_3836;

							BgL_arg1807z00_3836 = (BgL_objectz00_bglt) (BgL_az00_13);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/procedure.scm 323 */
									long BgL_idxz00_3842;

									BgL_idxz00_3842 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3836);
									BgL_test2126z00_5070 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3842 + 3L)) == BgL_classz00_3834);
								}
							else
								{	/* Cfa/procedure.scm 323 */
									bool_t BgL_res2049z00_3867;

									{	/* Cfa/procedure.scm 323 */
										obj_t BgL_oclassz00_3850;

										{	/* Cfa/procedure.scm 323 */
											obj_t BgL_arg1815z00_3858;
											long BgL_arg1816z00_3859;

											BgL_arg1815z00_3858 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/procedure.scm 323 */
												long BgL_arg1817z00_3860;

												BgL_arg1817z00_3860 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3836);
												BgL_arg1816z00_3859 =
													(BgL_arg1817z00_3860 - OBJECT_TYPE);
											}
											BgL_oclassz00_3850 =
												VECTOR_REF(BgL_arg1815z00_3858, BgL_arg1816z00_3859);
										}
										{	/* Cfa/procedure.scm 323 */
											bool_t BgL__ortest_1115z00_3851;

											BgL__ortest_1115z00_3851 =
												(BgL_classz00_3834 == BgL_oclassz00_3850);
											if (BgL__ortest_1115z00_3851)
												{	/* Cfa/procedure.scm 323 */
													BgL_res2049z00_3867 = BgL__ortest_1115z00_3851;
												}
											else
												{	/* Cfa/procedure.scm 323 */
													long BgL_odepthz00_3852;

													{	/* Cfa/procedure.scm 323 */
														obj_t BgL_arg1804z00_3853;

														BgL_arg1804z00_3853 = (BgL_oclassz00_3850);
														BgL_odepthz00_3852 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3853);
													}
													if ((3L < BgL_odepthz00_3852))
														{	/* Cfa/procedure.scm 323 */
															obj_t BgL_arg1802z00_3855;

															{	/* Cfa/procedure.scm 323 */
																obj_t BgL_arg1803z00_3856;

																BgL_arg1803z00_3856 = (BgL_oclassz00_3850);
																BgL_arg1802z00_3855 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3856,
																	3L);
															}
															BgL_res2049z00_3867 =
																(BgL_arg1802z00_3855 == BgL_classz00_3834);
														}
													else
														{	/* Cfa/procedure.scm 323 */
															BgL_res2049z00_3867 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2126z00_5070 = BgL_res2049z00_3867;
								}
						}
					else
						{	/* Cfa/procedure.scm 323 */
							BgL_test2126z00_5070 = ((bool_t) 0);
						}
				}
				if (BgL_test2126z00_5070)
					{	/* Cfa/procedure.scm 324 */
						BgL_valuez00_bglt BgL_valuez00_3008;

						BgL_valuez00_3008 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt) BgL_az00_13)))->BgL_valuez00);
						{	/* Cfa/procedure.scm 325 */
							bool_t BgL_test2131z00_5095;

							{	/* Cfa/procedure.scm 325 */
								obj_t BgL_classz00_3869;

								BgL_classz00_3869 = BGl_svarzf2Cinfozf2zzcfa_infoz00;
								{	/* Cfa/procedure.scm 325 */
									BgL_objectz00_bglt BgL_arg1807z00_3871;

									{	/* Cfa/procedure.scm 325 */
										obj_t BgL_tmpz00_5096;

										BgL_tmpz00_5096 =
											((obj_t) ((BgL_objectz00_bglt) BgL_valuez00_3008));
										BgL_arg1807z00_3871 =
											(BgL_objectz00_bglt) (BgL_tmpz00_5096);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/procedure.scm 325 */
											long BgL_idxz00_3877;

											BgL_idxz00_3877 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3871);
											BgL_test2131z00_5095 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_3877 + 3L)) == BgL_classz00_3869);
										}
									else
										{	/* Cfa/procedure.scm 325 */
											bool_t BgL_res2050z00_3902;

											{	/* Cfa/procedure.scm 325 */
												obj_t BgL_oclassz00_3885;

												{	/* Cfa/procedure.scm 325 */
													obj_t BgL_arg1815z00_3893;
													long BgL_arg1816z00_3894;

													BgL_arg1815z00_3893 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/procedure.scm 325 */
														long BgL_arg1817z00_3895;

														BgL_arg1817z00_3895 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3871);
														BgL_arg1816z00_3894 =
															(BgL_arg1817z00_3895 - OBJECT_TYPE);
													}
													BgL_oclassz00_3885 =
														VECTOR_REF(BgL_arg1815z00_3893,
														BgL_arg1816z00_3894);
												}
												{	/* Cfa/procedure.scm 325 */
													bool_t BgL__ortest_1115z00_3886;

													BgL__ortest_1115z00_3886 =
														(BgL_classz00_3869 == BgL_oclassz00_3885);
													if (BgL__ortest_1115z00_3886)
														{	/* Cfa/procedure.scm 325 */
															BgL_res2050z00_3902 = BgL__ortest_1115z00_3886;
														}
													else
														{	/* Cfa/procedure.scm 325 */
															long BgL_odepthz00_3887;

															{	/* Cfa/procedure.scm 325 */
																obj_t BgL_arg1804z00_3888;

																BgL_arg1804z00_3888 = (BgL_oclassz00_3885);
																BgL_odepthz00_3887 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_3888);
															}
															if ((3L < BgL_odepthz00_3887))
																{	/* Cfa/procedure.scm 325 */
																	obj_t BgL_arg1802z00_3890;

																	{	/* Cfa/procedure.scm 325 */
																		obj_t BgL_arg1803z00_3891;

																		BgL_arg1803z00_3891 = (BgL_oclassz00_3885);
																		BgL_arg1802z00_3890 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_3891, 3L);
																	}
																	BgL_res2050z00_3902 =
																		(BgL_arg1802z00_3890 == BgL_classz00_3869);
																}
															else
																{	/* Cfa/procedure.scm 325 */
																	BgL_res2050z00_3902 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2131z00_5095 = BgL_res2050z00_3902;
										}
								}
							}
							if (BgL_test2131z00_5095)
								{	/* Cfa/procedure.scm 326 */
									BgL_approxz00_bglt BgL_i1189z00_3010;

									{
										BgL_svarzf2cinfozf2_bglt BgL_auxz00_5119;

										{
											obj_t BgL_auxz00_5120;

											{	/* Cfa/procedure.scm 326 */
												BgL_objectz00_bglt BgL_tmpz00_5121;

												BgL_tmpz00_5121 =
													((BgL_objectz00_bglt)
													((BgL_svarz00_bglt) BgL_valuez00_3008));
												BgL_auxz00_5120 = BGL_OBJECT_WIDENING(BgL_tmpz00_5121);
											}
											BgL_auxz00_5119 =
												((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_5120);
										}
										BgL_i1189z00_3010 =
											(((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5119))->
											BgL_approxz00);
									}
									return
										((((BgL_approxz00_bglt) COBJECT(BgL_i1189z00_3010))->
											BgL_typez00) =
										((BgL_typez00_bglt) ((BgL_typez00_bglt)
												BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
								}
							else
								{	/* Cfa/procedure.scm 328 */
									obj_t BgL_arg1575z00_3011;
									obj_t BgL_arg1576z00_3012;

									{	/* Cfa/procedure.scm 328 */
										obj_t BgL_tmpz00_5129;

										BgL_tmpz00_5129 = BGL_CURRENT_DYNAMIC_ENV();
										BgL_arg1575z00_3011 =
											BGL_ENV_CURRENT_ERROR_PORT(BgL_tmpz00_5129);
									}
									BgL_arg1576z00_3012 = bgl_typeof(((obj_t) BgL_valuez00_3008));
									{	/* Cfa/procedure.scm 328 */
										obj_t BgL_list1577z00_3013;

										{	/* Cfa/procedure.scm 328 */
											obj_t BgL_arg1584z00_3014;

											{	/* Cfa/procedure.scm 328 */
												obj_t BgL_arg1585z00_3015;

												{	/* Cfa/procedure.scm 328 */
													obj_t BgL_arg1589z00_3016;

													{	/* Cfa/procedure.scm 328 */
														obj_t BgL_arg1591z00_3017;

														{	/* Cfa/procedure.scm 328 */
															obj_t BgL_arg1593z00_3018;

															BgL_arg1593z00_3018 =
																MAKE_YOUNG_PAIR(BgL_arg1576z00_3012, BNIL);
															BgL_arg1591z00_3017 =
																MAKE_YOUNG_PAIR
																(BGl_string2072z00zzcfa_procedurez00,
																BgL_arg1593z00_3018);
														}
														BgL_arg1589z00_3016 =
															MAKE_YOUNG_PAIR
															(BGl_string2073z00zzcfa_procedurez00,
															BgL_arg1591z00_3017);
													}
													BgL_arg1585z00_3015 =
														MAKE_YOUNG_PAIR(BINT(328L), BgL_arg1589z00_3016);
												}
												BgL_arg1584z00_3014 =
													MAKE_YOUNG_PAIR(BGl_string2074z00zzcfa_procedurez00,
													BgL_arg1585z00_3015);
											}
											BgL_list1577z00_3013 =
												MAKE_YOUNG_PAIR(BGl_string2075z00zzcfa_procedurez00,
												BgL_arg1584z00_3014);
										}
										return
											BGl_tprintz00zz__r4_output_6_10_3z00(BgL_arg1575z00_3011,
											BgL_list1577z00_3013);
									}
								}
						}
					}
				else
					{	/* Cfa/procedure.scm 323 */
						return BFALSE;
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_procedurez00(void)
	{
		{	/* Cfa/procedure.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_procedurez00(void)
	{
		{	/* Cfa/procedure.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_procedurez00(void)
	{
		{	/* Cfa/procedure.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_prezd2makezd2procedurezd2appzd2zzcfa_info2z00,
				BGl_proc2076z00zzcfa_procedurez00, BGl_string2077z00zzcfa_procedurez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_prezd2procedurezd2refzd2appzd2zzcfa_info2z00,
				BGl_proc2078z00zzcfa_procedurez00, BGl_string2077z00zzcfa_procedurez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_prezd2procedurezd2setz12zd2appzc0zzcfa_info2z00,
				BGl_proc2079z00zzcfa_procedurez00, BGl_string2077z00zzcfa_procedurez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_makezd2procedurezd2appz00zzcfa_info2z00,
				BGl_proc2080z00zzcfa_procedurez00, BGl_string2081z00zzcfa_procedurez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_procedurezd2refzd2appz00zzcfa_info2z00,
				BGl_proc2082z00zzcfa_procedurez00, BGl_string2081z00zzcfa_procedurez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00,
				BGl_procedurezd2setz12zd2appz12zzcfa_info2z00,
				BGl_proc2083z00zzcfa_procedurez00, BGl_string2081z00zzcfa_procedurez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_loosezd2allocz12zd2envz12zzcfa_loosez00,
				BGl_makezd2procedurezd2appz00zzcfa_info2z00,
				BGl_proc2084z00zzcfa_procedurez00, BGl_string2085z00zzcfa_procedurez00);
		}

	}



/* &loose-alloc!-make-pr1535 */
	obj_t BGl_z62loosezd2allocz12zd2makezd2pr1535za2zzcfa_procedurez00(obj_t
		BgL_envz00_4579, obj_t BgL_allocz00_4580)
	{
		{	/* Cfa/procedure.scm 281 */
			{	/* Cfa/procedure.scm 283 */
				bool_t BgL_test2135z00_5149;

				{	/* Cfa/procedure.scm 283 */
					long BgL_arg1870z00_4649;

					{
						BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5150;

						{
							obj_t BgL_auxz00_5151;

							{	/* Cfa/procedure.scm 283 */
								BgL_objectz00_bglt BgL_tmpz00_5152;

								BgL_tmpz00_5152 =
									((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_allocz00_4580));
								BgL_auxz00_5151 = BGL_OBJECT_WIDENING(BgL_tmpz00_5152);
							}
							BgL_auxz00_5150 =
								((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_5151);
						}
						BgL_arg1870z00_4649 =
							(((BgL_makezd2procedurezd2appz00_bglt) COBJECT(BgL_auxz00_5150))->
							BgL_lostzd2stampzd2);
					}
					BgL_test2135z00_5149 =
						(BgL_arg1870z00_4649 ==
						(long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00));
				}
				if (BgL_test2135z00_5149)
					{	/* Cfa/procedure.scm 283 */
						return BFALSE;
					}
				else
					{	/* Cfa/procedure.scm 283 */
						{
							BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5160;

							{
								obj_t BgL_auxz00_5161;

								{	/* Cfa/procedure.scm 285 */
									BgL_objectz00_bglt BgL_tmpz00_5162;

									BgL_tmpz00_5162 =
										((BgL_objectz00_bglt)
										((BgL_appz00_bglt) BgL_allocz00_4580));
									BgL_auxz00_5161 = BGL_OBJECT_WIDENING(BgL_tmpz00_5162);
								}
								BgL_auxz00_5160 =
									((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_5161);
							}
							((((BgL_makezd2procedurezd2appz00_bglt)
										COBJECT(BgL_auxz00_5160))->BgL_lostzd2stampzd2) =
								((long) (long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00)),
								BUNSPEC);
						}
						BGl_setzd2procedurezd2approxzd2polymorphicz12zc0zzcfa_procedurez00(
							((BgL_appz00_bglt) BgL_allocz00_4580));
						{	/* Cfa/procedure.scm 287 */
							obj_t BgL_calleez00_4650;

							{	/* Cfa/procedure.scm 287 */
								obj_t BgL_pairz00_4651;

								BgL_pairz00_4651 =
									(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt)
												((BgL_appz00_bglt) BgL_allocz00_4580))))->BgL_argsz00);
								BgL_calleez00_4650 = CAR(BgL_pairz00_4651);
							}
							{	/* Cfa/procedure.scm 287 */
								BgL_variablez00_bglt BgL_vz00_4652;

								BgL_vz00_4652 =
									(((BgL_varz00_bglt) COBJECT(
											((BgL_varz00_bglt) BgL_calleez00_4650)))->
									BgL_variablez00);
								{	/* Cfa/procedure.scm 288 */
									BgL_valuez00_bglt BgL_funz00_4653;

									BgL_funz00_4653 =
										(((BgL_variablez00_bglt) COBJECT(BgL_vz00_4652))->
										BgL_valuez00);
									{	/* Cfa/procedure.scm 289 */

										BGl_cfazd2exportzd2varz12z12zzcfa_iteratez00
											(BgL_funz00_4653, ((obj_t) BgL_vz00_4652));
										if (CBOOL
											(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00))
											{	/* Cfa/procedure.scm 292 */
												obj_t BgL_g1521z00_4654;

												{	/* Cfa/procedure.scm 292 */
													obj_t BgL_pairz00_4655;

													BgL_pairz00_4655 =
														(((BgL_sfunz00_bglt) COBJECT(
																((BgL_sfunz00_bglt) BgL_funz00_4653)))->
														BgL_argsz00);
													BgL_g1521z00_4654 = CDR(BgL_pairz00_4655);
												}
												{
													obj_t BgL_l1519z00_4657;

													{	/* Cfa/procedure.scm 292 */
														bool_t BgL_tmpz00_5185;

														BgL_l1519z00_4657 = BgL_g1521z00_4654;
													BgL_zc3z04anonymousza31862ze3z87_4656:
														if (PAIRP(BgL_l1519z00_4657))
															{	/* Cfa/procedure.scm 292 */
																BGl_loosezd2argz12zc0zzcfa_procedurez00(CAR
																	(BgL_l1519z00_4657));
																{
																	obj_t BgL_l1519z00_5190;

																	BgL_l1519z00_5190 = CDR(BgL_l1519z00_4657);
																	BgL_l1519z00_4657 = BgL_l1519z00_5190;
																	goto BgL_zc3z04anonymousza31862ze3z87_4656;
																}
															}
														else
															{	/* Cfa/procedure.scm 292 */
																BgL_tmpz00_5185 = ((bool_t) 1);
															}
														return BBOOL(BgL_tmpz00_5185);
													}
												}
											}
										else
											{	/* Cfa/procedure.scm 291 */
												return BFALSE;
											}
									}
								}
							}
						}
					}
			}
		}

	}



/* &cfa!-procedure-set!-1533 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2procedurezd2setz12zd21533zb0zzcfa_procedurez00(obj_t
		BgL_envz00_4581, obj_t BgL_nodez00_4582)
	{
		{	/* Cfa/procedure.scm 217 */
			{	/* Cfa/procedure.scm 219 */
				BgL_approxz00_bglt BgL_proczd2approxzd2_4659;
				obj_t BgL_offsetz00_4660;

				{	/* Cfa/procedure.scm 219 */
					obj_t BgL_arg1856z00_4661;

					{	/* Cfa/procedure.scm 219 */
						obj_t BgL_pairz00_4662;

						BgL_pairz00_4662 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4582))))->BgL_argsz00);
						BgL_arg1856z00_4661 = CAR(BgL_pairz00_4662);
					}
					BgL_proczd2approxzd2_4659 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1856z00_4661));
				}
				{	/* Cfa/procedure.scm 220 */
					obj_t BgL_arg1858z00_4663;

					{	/* Cfa/procedure.scm 220 */
						obj_t BgL_pairz00_4664;

						BgL_pairz00_4664 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4582))))->BgL_argsz00);
						BgL_arg1858z00_4663 = CAR(CDR(BgL_pairz00_4664));
					}
					BgL_offsetz00_4660 =
						BGl_getzd2nodezd2atomzd2valuezd2zzcfa_approxz00(
						((BgL_nodez00_bglt) BgL_arg1858z00_4663));
				}
				{	/* Cfa/procedure.scm 224 */
					obj_t BgL_arg1799z00_4665;

					{	/* Cfa/procedure.scm 224 */
						obj_t BgL_pairz00_4666;

						BgL_pairz00_4666 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4582))))->BgL_argsz00);
						BgL_arg1799z00_4665 = CAR(CDR(BgL_pairz00_4666));
					}
					BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1799z00_4665));
				}
				{	/* Cfa/procedure.scm 225 */
					obj_t BgL_arg1806z00_4667;
					BgL_approxz00_bglt BgL_arg1808z00_4668;

					{
						BgL_procedurezd2setz12zd2appz12_bglt BgL_auxz00_5213;

						{
							obj_t BgL_auxz00_5214;

							{	/* Cfa/procedure.scm 225 */
								BgL_objectz00_bglt BgL_tmpz00_5215;

								BgL_tmpz00_5215 =
									((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4582));
								BgL_auxz00_5214 = BGL_OBJECT_WIDENING(BgL_tmpz00_5215);
							}
							BgL_auxz00_5213 =
								((BgL_procedurezd2setz12zd2appz12_bglt) BgL_auxz00_5214);
						}
						BgL_arg1806z00_4667 =
							(((BgL_procedurezd2setz12zd2appz12_bglt)
								COBJECT(BgL_auxz00_5213))->BgL_vapproxz00);
					}
					{	/* Cfa/procedure.scm 225 */
						obj_t BgL_arg1812z00_4669;

						{	/* Cfa/procedure.scm 225 */
							obj_t BgL_pairz00_4670;

							BgL_pairz00_4670 =
								(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_4582))))->BgL_argsz00);
							BgL_arg1812z00_4669 = CAR(CDR(CDR(BgL_pairz00_4670)));
						}
						BgL_arg1808z00_4668 =
							BGl_cfaz12z12zzcfa_cfaz00(
							((BgL_nodez00_bglt) BgL_arg1812z00_4669));
					}
					BGl_unionzd2approxz12zc0zzcfa_approxz00(
						((BgL_approxz00_bglt) BgL_arg1806z00_4667), BgL_arg1808z00_4668);
				}
				if (
					(((BgL_approxz00_bglt) COBJECT(BgL_proczd2approxzd2_4659))->
						BgL_topzf3zf3))
					{	/* Cfa/procedure.scm 229 */
						obj_t BgL_arg1822z00_4671;

						{
							BgL_procedurezd2setz12zd2appz12_bglt BgL_auxz00_5233;

							{
								obj_t BgL_auxz00_5234;

								{	/* Cfa/procedure.scm 229 */
									BgL_objectz00_bglt BgL_tmpz00_5235;

									BgL_tmpz00_5235 =
										((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4582));
									BgL_auxz00_5234 = BGL_OBJECT_WIDENING(BgL_tmpz00_5235);
								}
								BgL_auxz00_5233 =
									((BgL_procedurezd2setz12zd2appz12_bglt) BgL_auxz00_5234);
							}
							BgL_arg1822z00_4671 =
								(((BgL_procedurezd2setz12zd2appz12_bglt)
									COBJECT(BgL_auxz00_5233))->BgL_vapproxz00);
						}
						((obj_t)
							BGl_loosez12z12zzcfa_loosez00(
								((BgL_approxz00_bglt) BgL_arg1822z00_4671), CNST_TABLE_REF(0)));
					}
				else
					{	/* Cfa/procedure.scm 227 */
						if (INTEGERP(BgL_offsetz00_4660))
							{	/* Cfa/procedure.scm 234 */
								obj_t BgL_zc3z04anonymousza31832ze3z87_4672;

								BgL_zc3z04anonymousza31832ze3z87_4672 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31832ze3ze5zzcfa_procedurez00,
									(int) (1L), (int) (2L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31832ze3z87_4672, (int) (0L),
									((obj_t) ((BgL_appz00_bglt) BgL_nodez00_4582)));
								PROCEDURE_SET(BgL_zc3z04anonymousza31832ze3z87_4672, (int) (1L),
									BgL_offsetz00_4660);
								BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
									(BgL_zc3z04anonymousza31832ze3z87_4672,
									BgL_proczd2approxzd2_4659);
							}
						else
							{	/* Cfa/procedure.scm 256 */
								obj_t BgL_zc3z04anonymousza31845ze3z87_4673;

								BgL_zc3z04anonymousza31845ze3z87_4673 =
									MAKE_FX_PROCEDURE
									(BGl_z62zc3z04anonymousza31845ze3ze5zzcfa_procedurez00,
									(int) (1L), (int) (1L));
								PROCEDURE_SET(BgL_zc3z04anonymousza31845ze3z87_4673, (int) (0L),
									((obj_t) ((BgL_appz00_bglt) BgL_nodez00_4582)));
								BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
									(BgL_zc3z04anonymousza31845ze3z87_4673,
									BgL_proczd2approxzd2_4659);
					}}
				{
					BgL_procedurezd2setz12zd2appz12_bglt BgL_auxz00_5265;

					{
						obj_t BgL_auxz00_5266;

						{	/* Cfa/procedure.scm 271 */
							BgL_objectz00_bglt BgL_tmpz00_5267;

							BgL_tmpz00_5267 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4582));
							BgL_auxz00_5266 = BGL_OBJECT_WIDENING(BgL_tmpz00_5267);
						}
						BgL_auxz00_5265 =
							((BgL_procedurezd2setz12zd2appz12_bglt) BgL_auxz00_5266);
					}
					return
						(((BgL_procedurezd2setz12zd2appz12_bglt) COBJECT(BgL_auxz00_5265))->
						BgL_approxz00);
				}
			}
		}

	}



/* &<@anonymous:1832> */
	obj_t BGl_z62zc3z04anonymousza31832ze3ze5zzcfa_procedurez00(obj_t
		BgL_envz00_4583, obj_t BgL_appz00_4586)
	{
		{	/* Cfa/procedure.scm 233 */
			{	/* Cfa/procedure.scm 234 */
				BgL_appz00_bglt BgL_i1187z00_4584;
				obj_t BgL_offsetz00_4585;

				BgL_i1187z00_4584 =
					((BgL_appz00_bglt) PROCEDURE_REF(BgL_envz00_4583, (int) (0L)));
				BgL_offsetz00_4585 = PROCEDURE_REF(BgL_envz00_4583, (int) (1L));
				{	/* Cfa/procedure.scm 234 */
					bool_t BgL_test2140z00_5278;

					{	/* Cfa/procedure.scm 234 */
						bool_t BgL_test2141z00_5279;

						{	/* Cfa/procedure.scm 234 */
							obj_t BgL_classz00_4674;

							BgL_classz00_4674 = BGl_makezd2procedurezd2appz00zzcfa_info2z00;
							if (BGL_OBJECTP(BgL_appz00_4586))
								{	/* Cfa/procedure.scm 234 */
									BgL_objectz00_bglt BgL_arg1807z00_4675;

									BgL_arg1807z00_4675 = (BgL_objectz00_bglt) (BgL_appz00_4586);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/procedure.scm 234 */
											long BgL_idxz00_4676;

											BgL_idxz00_4676 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4675);
											BgL_test2141z00_5279 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4676 + 4L)) == BgL_classz00_4674);
										}
									else
										{	/* Cfa/procedure.scm 234 */
											bool_t BgL_res2058z00_4679;

											{	/* Cfa/procedure.scm 234 */
												obj_t BgL_oclassz00_4680;

												{	/* Cfa/procedure.scm 234 */
													obj_t BgL_arg1815z00_4681;
													long BgL_arg1816z00_4682;

													BgL_arg1815z00_4681 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/procedure.scm 234 */
														long BgL_arg1817z00_4683;

														BgL_arg1817z00_4683 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4675);
														BgL_arg1816z00_4682 =
															(BgL_arg1817z00_4683 - OBJECT_TYPE);
													}
													BgL_oclassz00_4680 =
														VECTOR_REF(BgL_arg1815z00_4681,
														BgL_arg1816z00_4682);
												}
												{	/* Cfa/procedure.scm 234 */
													bool_t BgL__ortest_1115z00_4684;

													BgL__ortest_1115z00_4684 =
														(BgL_classz00_4674 == BgL_oclassz00_4680);
													if (BgL__ortest_1115z00_4684)
														{	/* Cfa/procedure.scm 234 */
															BgL_res2058z00_4679 = BgL__ortest_1115z00_4684;
														}
													else
														{	/* Cfa/procedure.scm 234 */
															long BgL_odepthz00_4685;

															{	/* Cfa/procedure.scm 234 */
																obj_t BgL_arg1804z00_4686;

																BgL_arg1804z00_4686 = (BgL_oclassz00_4680);
																BgL_odepthz00_4685 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4686);
															}
															if ((4L < BgL_odepthz00_4685))
																{	/* Cfa/procedure.scm 234 */
																	obj_t BgL_arg1802z00_4687;

																	{	/* Cfa/procedure.scm 234 */
																		obj_t BgL_arg1803z00_4688;

																		BgL_arg1803z00_4688 = (BgL_oclassz00_4680);
																		BgL_arg1802z00_4687 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4688, 4L);
																	}
																	BgL_res2058z00_4679 =
																		(BgL_arg1802z00_4687 == BgL_classz00_4674);
																}
															else
																{	/* Cfa/procedure.scm 234 */
																	BgL_res2058z00_4679 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2141z00_5279 = BgL_res2058z00_4679;
										}
								}
							else
								{	/* Cfa/procedure.scm 234 */
									BgL_test2141z00_5279 = ((bool_t) 0);
								}
						}
						if (BgL_test2141z00_5279)
							{	/* Cfa/procedure.scm 237 */
								long BgL_arg1842z00_4689;

								{	/* Cfa/procedure.scm 237 */
									obj_t BgL_arg1843z00_4690;

									{
										BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5302;

										{
											obj_t BgL_auxz00_5303;

											{	/* Cfa/procedure.scm 237 */
												BgL_objectz00_bglt BgL_tmpz00_5304;

												BgL_tmpz00_5304 =
													((BgL_objectz00_bglt)
													((BgL_appz00_bglt) BgL_appz00_4586));
												BgL_auxz00_5303 = BGL_OBJECT_WIDENING(BgL_tmpz00_5304);
											}
											BgL_auxz00_5302 =
												((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_5303);
										}
										BgL_arg1843z00_4690 =
											(((BgL_makezd2procedurezd2appz00_bglt)
												COBJECT(BgL_auxz00_5302))->BgL_valueszd2approxzd2);
									}
									BgL_arg1842z00_4689 = VECTOR_LENGTH(BgL_arg1843z00_4690);
								}
								BgL_test2140z00_5278 =
									((long) CINT(BgL_offsetz00_4585) < BgL_arg1842z00_4689);
							}
						else
							{	/* Cfa/procedure.scm 234 */
								BgL_test2140z00_5278 = ((bool_t) 0);
							}
					}
					if (BgL_test2140z00_5278)
						{	/* Cfa/procedure.scm 234 */
							{	/* Cfa/procedure.scm 248 */
								obj_t BgL_arg1837z00_4691;
								obj_t BgL_arg1838z00_4692;

								{	/* Cfa/procedure.scm 248 */
									obj_t BgL_arg1839z00_4693;

									{
										BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5313;

										{
											obj_t BgL_auxz00_5314;

											{	/* Cfa/procedure.scm 248 */
												BgL_objectz00_bglt BgL_tmpz00_5315;

												BgL_tmpz00_5315 =
													((BgL_objectz00_bglt)
													((BgL_appz00_bglt) BgL_appz00_4586));
												BgL_auxz00_5314 = BGL_OBJECT_WIDENING(BgL_tmpz00_5315);
											}
											BgL_auxz00_5313 =
												((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_5314);
										}
										BgL_arg1839z00_4693 =
											(((BgL_makezd2procedurezd2appz00_bglt)
												COBJECT(BgL_auxz00_5313))->BgL_valueszd2approxzd2);
									}
									BgL_arg1837z00_4691 =
										VECTOR_REF(BgL_arg1839z00_4693,
										(long) CINT(BgL_offsetz00_4585));
								}
								{
									BgL_procedurezd2setz12zd2appz12_bglt BgL_auxz00_5323;

									{
										obj_t BgL_auxz00_5324;

										{	/* Cfa/procedure.scm 247 */
											BgL_objectz00_bglt BgL_tmpz00_5325;

											BgL_tmpz00_5325 =
												((BgL_objectz00_bglt) BgL_i1187z00_4584);
											BgL_auxz00_5324 = BGL_OBJECT_WIDENING(BgL_tmpz00_5325);
										}
										BgL_auxz00_5323 =
											((BgL_procedurezd2setz12zd2appz12_bglt) BgL_auxz00_5324);
									}
									BgL_arg1838z00_4692 =
										(((BgL_procedurezd2setz12zd2appz12_bglt)
											COBJECT(BgL_auxz00_5323))->BgL_vapproxz00);
								}
								return
									((obj_t)
									BGl_unionzd2approxz12zc0zzcfa_approxz00(
										((BgL_approxz00_bglt) BgL_arg1837z00_4691),
										((BgL_approxz00_bglt) BgL_arg1838z00_4692)));
							}
						}
					else
						{	/* Cfa/procedure.scm 251 */
							obj_t BgL_arg1840z00_4694;

							{
								BgL_procedurezd2setz12zd2appz12_bglt BgL_auxz00_5334;

								{
									obj_t BgL_auxz00_5335;

									{	/* Cfa/procedure.scm 251 */
										BgL_objectz00_bglt BgL_tmpz00_5336;

										BgL_tmpz00_5336 = ((BgL_objectz00_bglt) BgL_i1187z00_4584);
										BgL_auxz00_5335 = BGL_OBJECT_WIDENING(BgL_tmpz00_5336);
									}
									BgL_auxz00_5334 =
										((BgL_procedurezd2setz12zd2appz12_bglt) BgL_auxz00_5335);
								}
								BgL_arg1840z00_4694 =
									(((BgL_procedurezd2setz12zd2appz12_bglt)
										COBJECT(BgL_auxz00_5334))->BgL_vapproxz00);
							}
							return
								BGl_approxzd2setzd2topz12z12zzcfa_approxz00(
								((BgL_approxz00_bglt) BgL_arg1840z00_4694));
						}
				}
			}
		}

	}



/* &<@anonymous:1845> */
	obj_t BGl_z62zc3z04anonymousza31845ze3ze5zzcfa_procedurez00(obj_t
		BgL_envz00_4587, obj_t BgL_appz00_4589)
	{
		{	/* Cfa/procedure.scm 255 */
			{	/* Cfa/procedure.scm 256 */
				BgL_appz00_bglt BgL_i1187z00_4588;

				BgL_i1187z00_4588 =
					((BgL_appz00_bglt) PROCEDURE_REF(BgL_envz00_4587, (int) (0L)));
				{	/* Cfa/procedure.scm 256 */
					bool_t BgL_test2146z00_5346;

					{	/* Cfa/procedure.scm 256 */
						obj_t BgL_classz00_4695;

						BgL_classz00_4695 = BGl_makezd2procedurezd2appz00zzcfa_info2z00;
						if (BGL_OBJECTP(BgL_appz00_4589))
							{	/* Cfa/procedure.scm 256 */
								BgL_objectz00_bglt BgL_arg1807z00_4696;

								BgL_arg1807z00_4696 = (BgL_objectz00_bglt) (BgL_appz00_4589);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/procedure.scm 256 */
										long BgL_idxz00_4697;

										BgL_idxz00_4697 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4696);
										BgL_test2146z00_5346 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_4697 + 4L)) == BgL_classz00_4695);
									}
								else
									{	/* Cfa/procedure.scm 256 */
										bool_t BgL_res2059z00_4700;

										{	/* Cfa/procedure.scm 256 */
											obj_t BgL_oclassz00_4701;

											{	/* Cfa/procedure.scm 256 */
												obj_t BgL_arg1815z00_4702;
												long BgL_arg1816z00_4703;

												BgL_arg1815z00_4702 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/procedure.scm 256 */
													long BgL_arg1817z00_4704;

													BgL_arg1817z00_4704 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4696);
													BgL_arg1816z00_4703 =
														(BgL_arg1817z00_4704 - OBJECT_TYPE);
												}
												BgL_oclassz00_4701 =
													VECTOR_REF(BgL_arg1815z00_4702, BgL_arg1816z00_4703);
											}
											{	/* Cfa/procedure.scm 256 */
												bool_t BgL__ortest_1115z00_4705;

												BgL__ortest_1115z00_4705 =
													(BgL_classz00_4695 == BgL_oclassz00_4701);
												if (BgL__ortest_1115z00_4705)
													{	/* Cfa/procedure.scm 256 */
														BgL_res2059z00_4700 = BgL__ortest_1115z00_4705;
													}
												else
													{	/* Cfa/procedure.scm 256 */
														long BgL_odepthz00_4706;

														{	/* Cfa/procedure.scm 256 */
															obj_t BgL_arg1804z00_4707;

															BgL_arg1804z00_4707 = (BgL_oclassz00_4701);
															BgL_odepthz00_4706 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_4707);
														}
														if ((4L < BgL_odepthz00_4706))
															{	/* Cfa/procedure.scm 256 */
																obj_t BgL_arg1802z00_4708;

																{	/* Cfa/procedure.scm 256 */
																	obj_t BgL_arg1803z00_4709;

																	BgL_arg1803z00_4709 = (BgL_oclassz00_4701);
																	BgL_arg1802z00_4708 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4709,
																		4L);
																}
																BgL_res2059z00_4700 =
																	(BgL_arg1802z00_4708 == BgL_classz00_4695);
															}
														else
															{	/* Cfa/procedure.scm 256 */
																BgL_res2059z00_4700 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2146z00_5346 = BgL_res2059z00_4700;
									}
							}
						else
							{	/* Cfa/procedure.scm 256 */
								BgL_test2146z00_5346 = ((bool_t) 0);
							}
					}
					if (BgL_test2146z00_5346)
						{	/* Cfa/procedure.scm 257 */
							long BgL_lenz00_4710;

							{	/* Cfa/procedure.scm 258 */
								obj_t BgL_arg1853z00_4711;

								{
									BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5369;

									{
										obj_t BgL_auxz00_5370;

										{	/* Cfa/procedure.scm 258 */
											BgL_objectz00_bglt BgL_tmpz00_5371;

											BgL_tmpz00_5371 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_appz00_4589));
											BgL_auxz00_5370 = BGL_OBJECT_WIDENING(BgL_tmpz00_5371);
										}
										BgL_auxz00_5369 =
											((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_5370);
									}
									BgL_arg1853z00_4711 =
										(((BgL_makezd2procedurezd2appz00_bglt)
											COBJECT(BgL_auxz00_5369))->BgL_valueszd2approxzd2);
								}
								BgL_lenz00_4710 = VECTOR_LENGTH(BgL_arg1853z00_4711);
							}
							{
								long BgL_iz00_4713;

								{	/* Cfa/procedure.scm 259 */
									bool_t BgL_tmpz00_5378;

									BgL_iz00_4713 = 0L;
								BgL_loopz00_4712:
									if ((BgL_iz00_4713 < BgL_lenz00_4710))
										{	/* Cfa/procedure.scm 260 */
											{	/* Cfa/procedure.scm 264 */
												obj_t BgL_arg1849z00_4714;
												obj_t BgL_arg1850z00_4715;

												{	/* Cfa/procedure.scm 264 */
													obj_t BgL_arg1851z00_4716;

													{
														BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5381;

														{
															obj_t BgL_auxz00_5382;

															{	/* Cfa/procedure.scm 264 */
																BgL_objectz00_bglt BgL_tmpz00_5383;

																BgL_tmpz00_5383 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt) BgL_appz00_4589));
																BgL_auxz00_5382 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5383);
															}
															BgL_auxz00_5381 =
																((BgL_makezd2procedurezd2appz00_bglt)
																BgL_auxz00_5382);
														}
														BgL_arg1851z00_4716 =
															(((BgL_makezd2procedurezd2appz00_bglt)
																COBJECT(BgL_auxz00_5381))->
															BgL_valueszd2approxzd2);
													}
													BgL_arg1849z00_4714 =
														VECTOR_REF(BgL_arg1851z00_4716, BgL_iz00_4713);
												}
												{
													BgL_procedurezd2setz12zd2appz12_bglt BgL_auxz00_5390;

													{
														obj_t BgL_auxz00_5391;

														{	/* Cfa/procedure.scm 262 */
															BgL_objectz00_bglt BgL_tmpz00_5392;

															BgL_tmpz00_5392 =
																((BgL_objectz00_bglt) BgL_i1187z00_4588);
															BgL_auxz00_5391 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_5392);
														}
														BgL_auxz00_5390 =
															((BgL_procedurezd2setz12zd2appz12_bglt)
															BgL_auxz00_5391);
													}
													BgL_arg1850z00_4715 =
														(((BgL_procedurezd2setz12zd2appz12_bglt)
															COBJECT(BgL_auxz00_5390))->BgL_vapproxz00);
												}
												BGl_unionzd2approxz12zc0zzcfa_approxz00(
													((BgL_approxz00_bglt) BgL_arg1849z00_4714),
													((BgL_approxz00_bglt) BgL_arg1850z00_4715));
											}
											{
												long BgL_iz00_5400;

												BgL_iz00_5400 = (BgL_iz00_4713 + 1L);
												BgL_iz00_4713 = BgL_iz00_5400;
												goto BgL_loopz00_4712;
											}
										}
									else
										{	/* Cfa/procedure.scm 260 */
											BgL_tmpz00_5378 = ((bool_t) 0);
										}
									return BBOOL(BgL_tmpz00_5378);
								}
							}
						}
					else
						{	/* Cfa/procedure.scm 269 */
							obj_t BgL_arg1854z00_4717;

							{
								BgL_procedurezd2setz12zd2appz12_bglt BgL_auxz00_5403;

								{
									obj_t BgL_auxz00_5404;

									{	/* Cfa/procedure.scm 269 */
										BgL_objectz00_bglt BgL_tmpz00_5405;

										BgL_tmpz00_5405 = ((BgL_objectz00_bglt) BgL_i1187z00_4588);
										BgL_auxz00_5404 = BGL_OBJECT_WIDENING(BgL_tmpz00_5405);
									}
									BgL_auxz00_5403 =
										((BgL_procedurezd2setz12zd2appz12_bglt) BgL_auxz00_5404);
								}
								BgL_arg1854z00_4717 =
									(((BgL_procedurezd2setz12zd2appz12_bglt)
										COBJECT(BgL_auxz00_5403))->BgL_vapproxz00);
							}
							return
								BGl_approxzd2setzd2topz12z12zzcfa_approxz00(
								((BgL_approxz00_bglt) BgL_arg1854z00_4717));
						}
				}
			}
		}

	}



/* &cfa!-procedure-ref-a1531 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2procedurezd2refzd2a1531za2zzcfa_procedurez00(obj_t
		BgL_envz00_4590, obj_t BgL_nodez00_4591)
	{
		{	/* Cfa/procedure.scm 167 */
			{	/* Cfa/procedure.scm 170 */
				BgL_approxz00_bglt BgL_proczd2approxzd2_4719;
				obj_t BgL_offsetz00_4720;

				{	/* Cfa/procedure.scm 170 */
					obj_t BgL_arg1771z00_4721;

					{	/* Cfa/procedure.scm 170 */
						obj_t BgL_pairz00_4722;

						BgL_pairz00_4722 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4591))))->BgL_argsz00);
						BgL_arg1771z00_4721 = CAR(BgL_pairz00_4722);
					}
					BgL_proczd2approxzd2_4719 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1771z00_4721));
				}
				{	/* Cfa/procedure.scm 171 */
					obj_t BgL_arg1775z00_4723;

					{	/* Cfa/procedure.scm 171 */
						obj_t BgL_pairz00_4724;

						BgL_pairz00_4724 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4591))))->BgL_argsz00);
						BgL_arg1775z00_4723 = CAR(CDR(BgL_pairz00_4724));
					}
					BgL_offsetz00_4720 =
						BGl_getzd2nodezd2atomzd2valuezd2zzcfa_approxz00(
						((BgL_nodez00_bglt) BgL_arg1775z00_4723));
				}
				{	/* Cfa/procedure.scm 172 */
					obj_t BgL_arg1734z00_4725;

					{	/* Cfa/procedure.scm 172 */
						obj_t BgL_pairz00_4726;

						BgL_pairz00_4726 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4591))))->BgL_argsz00);
						BgL_arg1734z00_4725 = CAR(CDR(BgL_pairz00_4726));
					}
					BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1734z00_4725));
				}
				if (
					(((BgL_approxz00_bglt) COBJECT(BgL_proczd2approxzd2_4719))->
						BgL_topzf3zf3))
					{	/* Cfa/procedure.scm 178 */
						BgL_approxz00_bglt BgL_arg1737z00_4727;

						{
							BgL_procedurezd2refzd2appz00_bglt BgL_auxz00_5434;

							{
								obj_t BgL_auxz00_5435;

								{	/* Cfa/procedure.scm 178 */
									BgL_objectz00_bglt BgL_tmpz00_5436;

									BgL_tmpz00_5436 =
										((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4591));
									BgL_auxz00_5435 = BGL_OBJECT_WIDENING(BgL_tmpz00_5436);
								}
								BgL_auxz00_5434 =
									((BgL_procedurezd2refzd2appz00_bglt) BgL_auxz00_5435);
							}
							BgL_arg1737z00_4727 =
								(((BgL_procedurezd2refzd2appz00_bglt)
									COBJECT(BgL_auxz00_5434))->BgL_approxz00);
						}
						BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_arg1737z00_4727);
					}
				else
					{	/* Cfa/procedure.scm 177 */
						BFALSE;
					}
				if (INTEGERP(BgL_offsetz00_4720))
					{	/* Cfa/procedure.scm 183 */
						obj_t BgL_zc3z04anonymousza31740ze3z87_4728;

						BgL_zc3z04anonymousza31740ze3z87_4728 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31740ze3ze5zzcfa_procedurez00,
							(int) (1L), (int) (2L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31740ze3z87_4728, (int) (0L),
							((obj_t) ((BgL_appz00_bglt) BgL_nodez00_4591)));
						PROCEDURE_SET(BgL_zc3z04anonymousza31740ze3z87_4728, (int) (1L),
							BgL_offsetz00_4720);
						BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
							(BgL_zc3z04anonymousza31740ze3z87_4728,
							BgL_proczd2approxzd2_4719);
					}
				else
					{	/* Cfa/procedure.scm 198 */
						obj_t BgL_zc3z04anonymousza31756ze3z87_4729;

						BgL_zc3z04anonymousza31756ze3z87_4729 =
							MAKE_FX_PROCEDURE
							(BGl_z62zc3z04anonymousza31756ze3ze5zzcfa_procedurez00,
							(int) (1L), (int) (1L));
						PROCEDURE_SET(BgL_zc3z04anonymousza31756ze3z87_4729, (int) (0L),
							((obj_t) ((BgL_appz00_bglt) BgL_nodez00_4591)));
						BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
							(BgL_zc3z04anonymousza31756ze3z87_4729,
							BgL_proczd2approxzd2_4719);
					}
				{
					BgL_procedurezd2refzd2appz00_bglt BgL_auxz00_5463;

					{
						obj_t BgL_auxz00_5464;

						{	/* Cfa/procedure.scm 212 */
							BgL_objectz00_bglt BgL_tmpz00_5465;

							BgL_tmpz00_5465 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4591));
							BgL_auxz00_5464 = BGL_OBJECT_WIDENING(BgL_tmpz00_5465);
						}
						BgL_auxz00_5463 =
							((BgL_procedurezd2refzd2appz00_bglt) BgL_auxz00_5464);
					}
					return
						(((BgL_procedurezd2refzd2appz00_bglt) COBJECT(BgL_auxz00_5463))->
						BgL_approxz00);
				}
			}
		}

	}



/* &<@anonymous:1740> */
	obj_t BGl_z62zc3z04anonymousza31740ze3ze5zzcfa_procedurez00(obj_t
		BgL_envz00_4592, obj_t BgL_appz00_4595)
	{
		{	/* Cfa/procedure.scm 182 */
			{	/* Cfa/procedure.scm 183 */
				BgL_appz00_bglt BgL_i1186z00_4593;
				obj_t BgL_offsetz00_4594;

				BgL_i1186z00_4593 =
					((BgL_appz00_bglt) PROCEDURE_REF(BgL_envz00_4592, (int) (0L)));
				BgL_offsetz00_4594 = PROCEDURE_REF(BgL_envz00_4592, (int) (1L));
				{	/* Cfa/procedure.scm 183 */
					bool_t BgL_test2154z00_5476;

					{	/* Cfa/procedure.scm 183 */
						bool_t BgL_test2155z00_5477;

						{	/* Cfa/procedure.scm 183 */
							obj_t BgL_classz00_4730;

							BgL_classz00_4730 = BGl_makezd2procedurezd2appz00zzcfa_info2z00;
							if (BGL_OBJECTP(BgL_appz00_4595))
								{	/* Cfa/procedure.scm 183 */
									BgL_objectz00_bglt BgL_arg1807z00_4731;

									BgL_arg1807z00_4731 = (BgL_objectz00_bglt) (BgL_appz00_4595);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/procedure.scm 183 */
											long BgL_idxz00_4732;

											BgL_idxz00_4732 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4731);
											BgL_test2155z00_5477 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4732 + 4L)) == BgL_classz00_4730);
										}
									else
										{	/* Cfa/procedure.scm 183 */
											bool_t BgL_res2056z00_4735;

											{	/* Cfa/procedure.scm 183 */
												obj_t BgL_oclassz00_4736;

												{	/* Cfa/procedure.scm 183 */
													obj_t BgL_arg1815z00_4737;
													long BgL_arg1816z00_4738;

													BgL_arg1815z00_4737 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/procedure.scm 183 */
														long BgL_arg1817z00_4739;

														BgL_arg1817z00_4739 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4731);
														BgL_arg1816z00_4738 =
															(BgL_arg1817z00_4739 - OBJECT_TYPE);
													}
													BgL_oclassz00_4736 =
														VECTOR_REF(BgL_arg1815z00_4737,
														BgL_arg1816z00_4738);
												}
												{	/* Cfa/procedure.scm 183 */
													bool_t BgL__ortest_1115z00_4740;

													BgL__ortest_1115z00_4740 =
														(BgL_classz00_4730 == BgL_oclassz00_4736);
													if (BgL__ortest_1115z00_4740)
														{	/* Cfa/procedure.scm 183 */
															BgL_res2056z00_4735 = BgL__ortest_1115z00_4740;
														}
													else
														{	/* Cfa/procedure.scm 183 */
															long BgL_odepthz00_4741;

															{	/* Cfa/procedure.scm 183 */
																obj_t BgL_arg1804z00_4742;

																BgL_arg1804z00_4742 = (BgL_oclassz00_4736);
																BgL_odepthz00_4741 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4742);
															}
															if ((4L < BgL_odepthz00_4741))
																{	/* Cfa/procedure.scm 183 */
																	obj_t BgL_arg1802z00_4743;

																	{	/* Cfa/procedure.scm 183 */
																		obj_t BgL_arg1803z00_4744;

																		BgL_arg1803z00_4744 = (BgL_oclassz00_4736);
																		BgL_arg1802z00_4743 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4744, 4L);
																	}
																	BgL_res2056z00_4735 =
																		(BgL_arg1802z00_4743 == BgL_classz00_4730);
																}
															else
																{	/* Cfa/procedure.scm 183 */
																	BgL_res2056z00_4735 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2155z00_5477 = BgL_res2056z00_4735;
										}
								}
							else
								{	/* Cfa/procedure.scm 183 */
									BgL_test2155z00_5477 = ((bool_t) 0);
								}
						}
						if (BgL_test2155z00_5477)
							{	/* Cfa/procedure.scm 186 */
								long BgL_arg1753z00_4745;

								{	/* Cfa/procedure.scm 186 */
									obj_t BgL_arg1754z00_4746;

									{
										BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5500;

										{
											obj_t BgL_auxz00_5501;

											{	/* Cfa/procedure.scm 186 */
												BgL_objectz00_bglt BgL_tmpz00_5502;

												BgL_tmpz00_5502 =
													((BgL_objectz00_bglt)
													((BgL_appz00_bglt) BgL_appz00_4595));
												BgL_auxz00_5501 = BGL_OBJECT_WIDENING(BgL_tmpz00_5502);
											}
											BgL_auxz00_5500 =
												((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_5501);
										}
										BgL_arg1754z00_4746 =
											(((BgL_makezd2procedurezd2appz00_bglt)
												COBJECT(BgL_auxz00_5500))->BgL_valueszd2approxzd2);
									}
									BgL_arg1753z00_4745 = VECTOR_LENGTH(BgL_arg1754z00_4746);
								}
								BgL_test2154z00_5476 =
									((long) CINT(BgL_offsetz00_4594) < BgL_arg1753z00_4745);
							}
						else
							{	/* Cfa/procedure.scm 183 */
								BgL_test2154z00_5476 = ((bool_t) 0);
							}
					}
					if (BgL_test2154z00_5476)
						{	/* Cfa/procedure.scm 187 */
							BgL_approxz00_bglt BgL_arg1749z00_4747;
							obj_t BgL_arg1750z00_4748;

							{
								BgL_procedurezd2refzd2appz00_bglt BgL_auxz00_5511;

								{
									obj_t BgL_auxz00_5512;

									{	/* Cfa/procedure.scm 187 */
										BgL_objectz00_bglt BgL_tmpz00_5513;

										BgL_tmpz00_5513 = ((BgL_objectz00_bglt) BgL_i1186z00_4593);
										BgL_auxz00_5512 = BGL_OBJECT_WIDENING(BgL_tmpz00_5513);
									}
									BgL_auxz00_5511 =
										((BgL_procedurezd2refzd2appz00_bglt) BgL_auxz00_5512);
								}
								BgL_arg1749z00_4747 =
									(((BgL_procedurezd2refzd2appz00_bglt)
										COBJECT(BgL_auxz00_5511))->BgL_approxz00);
							}
							{	/* Cfa/procedure.scm 189 */
								obj_t BgL_arg1751z00_4749;

								{
									BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5518;

									{
										obj_t BgL_auxz00_5519;

										{	/* Cfa/procedure.scm 189 */
											BgL_objectz00_bglt BgL_tmpz00_5520;

											BgL_tmpz00_5520 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_appz00_4595));
											BgL_auxz00_5519 = BGL_OBJECT_WIDENING(BgL_tmpz00_5520);
										}
										BgL_auxz00_5518 =
											((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_5519);
									}
									BgL_arg1751z00_4749 =
										(((BgL_makezd2procedurezd2appz00_bglt)
											COBJECT(BgL_auxz00_5518))->BgL_valueszd2approxzd2);
								}
								BgL_arg1750z00_4748 =
									VECTOR_REF(BgL_arg1751z00_4749,
									(long) CINT(BgL_offsetz00_4594));
							}
							return
								((obj_t)
								BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1749z00_4747,
									((BgL_approxz00_bglt) BgL_arg1750z00_4748)));
						}
					else
						{	/* Cfa/procedure.scm 193 */
							BgL_approxz00_bglt BgL_arg1752z00_4750;

							{
								BgL_procedurezd2refzd2appz00_bglt BgL_auxz00_5531;

								{
									obj_t BgL_auxz00_5532;

									{	/* Cfa/procedure.scm 193 */
										BgL_objectz00_bglt BgL_tmpz00_5533;

										BgL_tmpz00_5533 = ((BgL_objectz00_bglt) BgL_i1186z00_4593);
										BgL_auxz00_5532 = BGL_OBJECT_WIDENING(BgL_tmpz00_5533);
									}
									BgL_auxz00_5531 =
										((BgL_procedurezd2refzd2appz00_bglt) BgL_auxz00_5532);
								}
								BgL_arg1752z00_4750 =
									(((BgL_procedurezd2refzd2appz00_bglt)
										COBJECT(BgL_auxz00_5531))->BgL_approxz00);
							}
							return
								BGl_approxzd2setzd2topz12z12zzcfa_approxz00
								(BgL_arg1752z00_4750);
						}
				}
			}
		}

	}



/* &<@anonymous:1756> */
	obj_t BGl_z62zc3z04anonymousza31756ze3ze5zzcfa_procedurez00(obj_t
		BgL_envz00_4596, obj_t BgL_appz00_4598)
	{
		{	/* Cfa/procedure.scm 197 */
			{	/* Cfa/procedure.scm 198 */
				BgL_appz00_bglt BgL_i1186z00_4597;

				BgL_i1186z00_4597 =
					((BgL_appz00_bglt) PROCEDURE_REF(BgL_envz00_4596, (int) (0L)));
				{	/* Cfa/procedure.scm 198 */
					bool_t BgL_tmpz00_5542;

					{	/* Cfa/procedure.scm 198 */
						bool_t BgL_test2160z00_5543;

						{	/* Cfa/procedure.scm 198 */
							obj_t BgL_classz00_4751;

							BgL_classz00_4751 = BGl_makezd2procedurezd2appz00zzcfa_info2z00;
							if (BGL_OBJECTP(BgL_appz00_4598))
								{	/* Cfa/procedure.scm 198 */
									BgL_objectz00_bglt BgL_arg1807z00_4752;

									BgL_arg1807z00_4752 = (BgL_objectz00_bglt) (BgL_appz00_4598);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/procedure.scm 198 */
											long BgL_idxz00_4753;

											BgL_idxz00_4753 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4752);
											BgL_test2160z00_5543 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4753 + 4L)) == BgL_classz00_4751);
										}
									else
										{	/* Cfa/procedure.scm 198 */
											bool_t BgL_res2057z00_4756;

											{	/* Cfa/procedure.scm 198 */
												obj_t BgL_oclassz00_4757;

												{	/* Cfa/procedure.scm 198 */
													obj_t BgL_arg1815z00_4758;
													long BgL_arg1816z00_4759;

													BgL_arg1815z00_4758 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/procedure.scm 198 */
														long BgL_arg1817z00_4760;

														BgL_arg1817z00_4760 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4752);
														BgL_arg1816z00_4759 =
															(BgL_arg1817z00_4760 - OBJECT_TYPE);
													}
													BgL_oclassz00_4757 =
														VECTOR_REF(BgL_arg1815z00_4758,
														BgL_arg1816z00_4759);
												}
												{	/* Cfa/procedure.scm 198 */
													bool_t BgL__ortest_1115z00_4761;

													BgL__ortest_1115z00_4761 =
														(BgL_classz00_4751 == BgL_oclassz00_4757);
													if (BgL__ortest_1115z00_4761)
														{	/* Cfa/procedure.scm 198 */
															BgL_res2057z00_4756 = BgL__ortest_1115z00_4761;
														}
													else
														{	/* Cfa/procedure.scm 198 */
															long BgL_odepthz00_4762;

															{	/* Cfa/procedure.scm 198 */
																obj_t BgL_arg1804z00_4763;

																BgL_arg1804z00_4763 = (BgL_oclassz00_4757);
																BgL_odepthz00_4762 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4763);
															}
															if ((4L < BgL_odepthz00_4762))
																{	/* Cfa/procedure.scm 198 */
																	obj_t BgL_arg1802z00_4764;

																	{	/* Cfa/procedure.scm 198 */
																		obj_t BgL_arg1803z00_4765;

																		BgL_arg1803z00_4765 = (BgL_oclassz00_4757);
																		BgL_arg1802z00_4764 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4765, 4L);
																	}
																	BgL_res2057z00_4756 =
																		(BgL_arg1802z00_4764 == BgL_classz00_4751);
																}
															else
																{	/* Cfa/procedure.scm 198 */
																	BgL_res2057z00_4756 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2160z00_5543 = BgL_res2057z00_4756;
										}
								}
							else
								{	/* Cfa/procedure.scm 198 */
									BgL_test2160z00_5543 = ((bool_t) 0);
								}
						}
						if (BgL_test2160z00_5543)
							{	/* Cfa/procedure.scm 199 */
								long BgL_lenz00_4766;

								{	/* Cfa/procedure.scm 200 */
									obj_t BgL_arg1770z00_4767;

									{
										BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5566;

										{
											obj_t BgL_auxz00_5567;

											{	/* Cfa/procedure.scm 200 */
												BgL_objectz00_bglt BgL_tmpz00_5568;

												BgL_tmpz00_5568 =
													((BgL_objectz00_bglt)
													((BgL_appz00_bglt) BgL_appz00_4598));
												BgL_auxz00_5567 = BGL_OBJECT_WIDENING(BgL_tmpz00_5568);
											}
											BgL_auxz00_5566 =
												((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_5567);
										}
										BgL_arg1770z00_4767 =
											(((BgL_makezd2procedurezd2appz00_bglt)
												COBJECT(BgL_auxz00_5566))->BgL_valueszd2approxzd2);
									}
									BgL_lenz00_4766 = VECTOR_LENGTH(BgL_arg1770z00_4767);
								}
								{
									long BgL_iz00_4769;

									BgL_iz00_4769 = 0L;
								BgL_loopz00_4768:
									if ((BgL_iz00_4769 < BgL_lenz00_4766))
										{	/* Cfa/procedure.scm 202 */
											{	/* Cfa/procedure.scm 204 */
												BgL_approxz00_bglt BgL_arg1761z00_4770;
												obj_t BgL_arg1762z00_4771;

												{
													BgL_procedurezd2refzd2appz00_bglt BgL_auxz00_5577;

													{
														obj_t BgL_auxz00_5578;

														{	/* Cfa/procedure.scm 204 */
															BgL_objectz00_bglt BgL_tmpz00_5579;

															BgL_tmpz00_5579 =
																((BgL_objectz00_bglt) BgL_i1186z00_4597);
															BgL_auxz00_5578 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_5579);
														}
														BgL_auxz00_5577 =
															((BgL_procedurezd2refzd2appz00_bglt)
															BgL_auxz00_5578);
													}
													BgL_arg1761z00_4770 =
														(((BgL_procedurezd2refzd2appz00_bglt)
															COBJECT(BgL_auxz00_5577))->BgL_approxz00);
												}
												{	/* Cfa/procedure.scm 207 */
													obj_t BgL_arg1765z00_4772;

													{
														BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5584;

														{
															obj_t BgL_auxz00_5585;

															{	/* Cfa/procedure.scm 207 */
																BgL_objectz00_bglt BgL_tmpz00_5586;

																BgL_tmpz00_5586 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt) BgL_appz00_4598));
																BgL_auxz00_5585 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5586);
															}
															BgL_auxz00_5584 =
																((BgL_makezd2procedurezd2appz00_bglt)
																BgL_auxz00_5585);
														}
														BgL_arg1765z00_4772 =
															(((BgL_makezd2procedurezd2appz00_bglt)
																COBJECT(BgL_auxz00_5584))->
															BgL_valueszd2approxzd2);
													}
													BgL_arg1762z00_4771 =
														VECTOR_REF(BgL_arg1765z00_4772, BgL_iz00_4769);
												}
												BGl_unionzd2approxz12zc0zzcfa_approxz00
													(BgL_arg1761z00_4770,
													((BgL_approxz00_bglt) BgL_arg1762z00_4771));
											}
											{
												long BgL_iz00_5595;

												BgL_iz00_5595 = (BgL_iz00_4769 + 1L);
												BgL_iz00_4769 = BgL_iz00_5595;
												goto BgL_loopz00_4768;
											}
										}
									else
										{	/* Cfa/procedure.scm 202 */
											BgL_tmpz00_5542 = ((bool_t) 0);
										}
								}
							}
						else
							{	/* Cfa/procedure.scm 198 */
								BgL_tmpz00_5542 = ((bool_t) 0);
							}
					}
					return BBOOL(BgL_tmpz00_5542);
				}
			}
		}

	}



/* &cfa!-make-procedure-1529 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2makezd2procedurezd21529za2zzcfa_procedurez00(obj_t
		BgL_envz00_4599, obj_t BgL_nodez00_4600)
	{
		{	/* Cfa/procedure.scm 146 */
			{	/* Cfa/procedure.scm 150 */
				obj_t BgL_procz00_4774;

				{	/* Cfa/procedure.scm 150 */
					obj_t BgL_pairz00_4775;

					BgL_pairz00_4775 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_4600))))->BgL_argsz00);
					BgL_procz00_4774 = CAR(BgL_pairz00_4775);
				}
				{	/* Cfa/procedure.scm 150 */
					BgL_valuez00_bglt BgL_funz00_4776;

					BgL_funz00_4776 =
						(((BgL_variablez00_bglt) COBJECT(
								(((BgL_varz00_bglt) COBJECT(
											((BgL_varz00_bglt) BgL_procz00_4774)))->
									BgL_variablez00)))->BgL_valuez00);
					{	/* Cfa/procedure.scm 151 */
						obj_t BgL_envz00_4777;

						{	/* Cfa/procedure.scm 152 */
							obj_t BgL_pairz00_4778;

							BgL_pairz00_4778 =
								(((BgL_sfunz00_bglt) COBJECT(
										((BgL_sfunz00_bglt) BgL_funz00_4776)))->BgL_argsz00);
							BgL_envz00_4777 = CAR(BgL_pairz00_4778);
						}
						{	/* Cfa/procedure.scm 152 */

							{	/* Cfa/procedure.scm 159 */
								BgL_approxz00_bglt BgL_arg1710z00_4779;
								BgL_approxz00_bglt BgL_arg1711z00_4780;

								{	/* Cfa/procedure.scm 159 */
									BgL_svarz00_bglt BgL_oz00_4781;

									BgL_oz00_4781 =
										((BgL_svarz00_bglt)
										(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_envz00_4777))))->
											BgL_valuez00));
									{
										BgL_svarzf2cinfozf2_bglt BgL_auxz00_5612;

										{
											obj_t BgL_auxz00_5613;

											{	/* Cfa/procedure.scm 159 */
												BgL_objectz00_bglt BgL_tmpz00_5614;

												BgL_tmpz00_5614 = ((BgL_objectz00_bglt) BgL_oz00_4781);
												BgL_auxz00_5613 = BGL_OBJECT_WIDENING(BgL_tmpz00_5614);
											}
											BgL_auxz00_5612 =
												((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_5613);
										}
										BgL_arg1710z00_4779 =
											(((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_5612))->
											BgL_approxz00);
									}
								}
								{
									BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5619;

									{
										obj_t BgL_auxz00_5620;

										{	/* Cfa/procedure.scm 159 */
											BgL_objectz00_bglt BgL_tmpz00_5621;

											BgL_tmpz00_5621 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_4600));
											BgL_auxz00_5620 = BGL_OBJECT_WIDENING(BgL_tmpz00_5621);
										}
										BgL_auxz00_5619 =
											((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_5620);
									}
									BgL_arg1711z00_4780 =
										(((BgL_makezd2procedurezd2appz00_bglt)
											COBJECT(BgL_auxz00_5619))->BgL_approxz00);
								}
								BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1710z00_4779,
									BgL_arg1711z00_4780);
							}
						}
					}
				}
			}
			{	/* Cfa/procedure.scm 161 */
				obj_t BgL_g1518z00_4782;

				BgL_g1518z00_4782 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_4600))))->BgL_argsz00);
				{
					obj_t BgL_l1516z00_4784;

					BgL_l1516z00_4784 = BgL_g1518z00_4782;
				BgL_zc3z04anonymousza31721ze3z87_4783:
					if (PAIRP(BgL_l1516z00_4784))
						{	/* Cfa/procedure.scm 161 */
							{	/* Cfa/procedure.scm 161 */
								obj_t BgL_arg1724z00_4785;

								BgL_arg1724z00_4785 = CAR(BgL_l1516z00_4784);
								BGl_cfaz12z12zzcfa_cfaz00(
									((BgL_nodez00_bglt) BgL_arg1724z00_4785));
							}
							{
								obj_t BgL_l1516z00_5636;

								BgL_l1516z00_5636 = CDR(BgL_l1516z00_4784);
								BgL_l1516z00_4784 = BgL_l1516z00_5636;
								goto BgL_zc3z04anonymousza31721ze3z87_4783;
							}
						}
					else
						{	/* Cfa/procedure.scm 161 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5638;

				{
					obj_t BgL_auxz00_5639;

					{	/* Cfa/procedure.scm 162 */
						BgL_objectz00_bglt BgL_tmpz00_5640;

						BgL_tmpz00_5640 =
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4600));
						BgL_auxz00_5639 = BGL_OBJECT_WIDENING(BgL_tmpz00_5640);
					}
					BgL_auxz00_5638 =
						((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_5639);
				}
				return
					(((BgL_makezd2procedurezd2appz00_bglt) COBJECT(BgL_auxz00_5638))->
					BgL_approxz00);
			}
		}

	}



/* &node-setup!-pre-proc1527 */
	obj_t BGl_z62nodezd2setupz12zd2prezd2proc1527za2zzcfa_procedurez00(obj_t
		BgL_envz00_4601, obj_t BgL_nodez00_4602)
	{
		{	/* Cfa/procedure.scm 134 */
			BGl_addzd2procedurezd2refz12z12zzcfa_closurez00(
				((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4602)));
			{	/* Cfa/procedure.scm 137 */
				obj_t BgL_arg1702z00_4787;

				BgL_arg1702z00_4787 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_4602))))->BgL_argsz00);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1702z00_4787);
			}
			{	/* Cfa/procedure.scm 138 */
				BgL_appz00_bglt BgL_nodez00_4788;

				{	/* Cfa/procedure.scm 138 */
					long BgL_arg1705z00_4789;

					{	/* Cfa/procedure.scm 138 */
						obj_t BgL_arg1708z00_4790;

						{	/* Cfa/procedure.scm 138 */
							obj_t BgL_arg1709z00_4791;

							{	/* Cfa/procedure.scm 138 */
								obj_t BgL_arg1815z00_4792;
								long BgL_arg1816z00_4793;

								BgL_arg1815z00_4792 = (BGl_za2classesza2z00zz__objectz00);
								{	/* Cfa/procedure.scm 138 */
									long BgL_arg1817z00_4794;

									BgL_arg1817z00_4794 =
										BGL_OBJECT_CLASS_NUM(
										((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_4602)));
									BgL_arg1816z00_4793 = (BgL_arg1817z00_4794 - OBJECT_TYPE);
								}
								BgL_arg1709z00_4791 =
									VECTOR_REF(BgL_arg1815z00_4792, BgL_arg1816z00_4793);
							}
							BgL_arg1708z00_4790 =
								BGl_classzd2superzd2zz__objectz00(BgL_arg1709z00_4791);
						}
						{	/* Cfa/procedure.scm 138 */
							obj_t BgL_tmpz00_5660;

							BgL_tmpz00_5660 = ((obj_t) BgL_arg1708z00_4790);
							BgL_arg1705z00_4789 = BGL_CLASS_NUM(BgL_tmpz00_5660);
					}}
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_appz00_bglt) BgL_nodez00_4602)), BgL_arg1705z00_4789);
				}
				{	/* Cfa/procedure.scm 138 */
					BgL_objectz00_bglt BgL_tmpz00_5666;

					BgL_tmpz00_5666 =
						((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4602));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5666, BFALSE);
				}
				((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4602));
				BgL_nodez00_4788 = ((BgL_appz00_bglt) BgL_nodez00_4602);
				{	/* Cfa/procedure.scm 139 */
					BgL_procedurezd2setz12zd2appz12_bglt BgL_wide1183z00_4795;

					BgL_wide1183z00_4795 =
						((BgL_procedurezd2setz12zd2appz12_bglt)
						BOBJECT(GC_MALLOC(sizeof(struct
									BgL_procedurezd2setz12zd2appz12_bgl))));
					{	/* Cfa/procedure.scm 139 */
						obj_t BgL_auxz00_5678;
						BgL_objectz00_bglt BgL_tmpz00_5674;

						BgL_auxz00_5678 = ((obj_t) BgL_wide1183z00_4795);
						BgL_tmpz00_5674 =
							((BgL_objectz00_bglt)
							((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4788)));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5674, BgL_auxz00_5678);
					}
					((BgL_objectz00_bglt)
						((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4788)));
					{	/* Cfa/procedure.scm 139 */
						long BgL_arg1703z00_4796;

						BgL_arg1703z00_4796 =
							BGL_CLASS_NUM(BGl_procedurezd2setz12zd2appz12zzcfa_info2z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_appz00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_4788))), BgL_arg1703z00_4796);
					}
					((BgL_appz00_bglt)
						((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4788)));
				}
				{
					BgL_procedurezd2setz12zd2appz12_bglt BgL_auxz00_5692;

					{
						obj_t BgL_auxz00_5693;

						{	/* Cfa/procedure.scm 141 */
							BgL_objectz00_bglt BgL_tmpz00_5694;

							BgL_tmpz00_5694 =
								((BgL_objectz00_bglt)
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4788)));
							BgL_auxz00_5693 = BGL_OBJECT_WIDENING(BgL_tmpz00_5694);
						}
						BgL_auxz00_5692 =
							((BgL_procedurezd2setz12zd2appz12_bglt) BgL_auxz00_5693);
					}
					((((BgL_procedurezd2setz12zd2appz12_bglt) COBJECT(BgL_auxz00_5692))->
							BgL_approxz00) =
						((BgL_approxz00_bglt)
							BGl_makezd2typezd2approxz00zzcfa_approxz00(((BgL_typez00_bglt)
									BGl_za2unspecza2z00zztype_cachez00))), BUNSPEC);
				}
				{
					BgL_procedurezd2setz12zd2appz12_bglt BgL_auxz00_5703;

					{
						obj_t BgL_auxz00_5704;

						{	/* Cfa/procedure.scm 140 */
							BgL_objectz00_bglt BgL_tmpz00_5705;

							BgL_tmpz00_5705 =
								((BgL_objectz00_bglt)
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4788)));
							BgL_auxz00_5704 = BGL_OBJECT_WIDENING(BgL_tmpz00_5705);
						}
						BgL_auxz00_5703 =
							((BgL_procedurezd2setz12zd2appz12_bglt) BgL_auxz00_5704);
					}
					((((BgL_procedurezd2setz12zd2appz12_bglt) COBJECT(BgL_auxz00_5703))->
							BgL_vapproxz00) =
						((obj_t) ((obj_t) BGl_makezd2emptyzd2approxz00zzcfa_approxz00())),
						BUNSPEC);
				}
				return
					((obj_t) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4788)));
			}
		}

	}



/* &node-setup!-pre-proc1525 */
	obj_t BGl_z62nodezd2setupz12zd2prezd2proc1525za2zzcfa_procedurez00(obj_t
		BgL_envz00_4603, obj_t BgL_nodez00_4604)
	{
		{	/* Cfa/procedure.scm 121 */
			BGl_addzd2procedurezd2refz12z12zzcfa_closurez00(
				((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4604)));
			{	/* Cfa/procedure.scm 124 */
				obj_t BgL_arg1691z00_4798;

				BgL_arg1691z00_4798 =
					(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt)
								((BgL_appz00_bglt) BgL_nodez00_4604))))->BgL_argsz00);
				BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1691z00_4798);
			}
			{	/* Cfa/procedure.scm 125 */
				BgL_appz00_bglt BgL_nodez00_4799;

				{	/* Cfa/procedure.scm 125 */
					long BgL_arg1699z00_4800;

					{	/* Cfa/procedure.scm 125 */
						obj_t BgL_arg1700z00_4801;

						{	/* Cfa/procedure.scm 125 */
							obj_t BgL_arg1701z00_4802;

							{	/* Cfa/procedure.scm 125 */
								obj_t BgL_arg1815z00_4803;
								long BgL_arg1816z00_4804;

								BgL_arg1815z00_4803 = (BGl_za2classesza2z00zz__objectz00);
								{	/* Cfa/procedure.scm 125 */
									long BgL_arg1817z00_4805;

									BgL_arg1817z00_4805 =
										BGL_OBJECT_CLASS_NUM(
										((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_4604)));
									BgL_arg1816z00_4804 = (BgL_arg1817z00_4805 - OBJECT_TYPE);
								}
								BgL_arg1701z00_4802 =
									VECTOR_REF(BgL_arg1815z00_4803, BgL_arg1816z00_4804);
							}
							BgL_arg1700z00_4801 =
								BGl_classzd2superzd2zz__objectz00(BgL_arg1701z00_4802);
						}
						{	/* Cfa/procedure.scm 125 */
							obj_t BgL_tmpz00_5731;

							BgL_tmpz00_5731 = ((obj_t) BgL_arg1700z00_4801);
							BgL_arg1699z00_4800 = BGL_CLASS_NUM(BgL_tmpz00_5731);
					}}
					BGL_OBJECT_CLASS_NUM_SET(
						((BgL_objectz00_bglt)
							((BgL_appz00_bglt) BgL_nodez00_4604)), BgL_arg1699z00_4800);
				}
				{	/* Cfa/procedure.scm 125 */
					BgL_objectz00_bglt BgL_tmpz00_5737;

					BgL_tmpz00_5737 =
						((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4604));
					BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5737, BFALSE);
				}
				((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4604));
				BgL_nodez00_4799 = ((BgL_appz00_bglt) BgL_nodez00_4604);
				{	/* Cfa/procedure.scm 126 */
					BgL_procedurezd2refzd2appz00_bglt BgL_wide1177z00_4806;

					BgL_wide1177z00_4806 =
						((BgL_procedurezd2refzd2appz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
									BgL_procedurezd2refzd2appz00_bgl))));
					{	/* Cfa/procedure.scm 126 */
						obj_t BgL_auxz00_5749;
						BgL_objectz00_bglt BgL_tmpz00_5745;

						BgL_auxz00_5749 = ((obj_t) BgL_wide1177z00_4806);
						BgL_tmpz00_5745 =
							((BgL_objectz00_bglt)
							((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4799)));
						BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5745, BgL_auxz00_5749);
					}
					((BgL_objectz00_bglt)
						((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4799)));
					{	/* Cfa/procedure.scm 126 */
						long BgL_arg1692z00_4807;

						BgL_arg1692z00_4807 =
							BGL_CLASS_NUM(BGl_procedurezd2refzd2appz00zzcfa_info2z00);
						BGL_OBJECT_CLASS_NUM_SET(
							((BgL_objectz00_bglt)
								((BgL_appz00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_4799))), BgL_arg1692z00_4807);
					}
					((BgL_appz00_bglt)
						((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4799)));
				}
				{
					BgL_approxz00_bglt BgL_auxz00_5771;
					BgL_procedurezd2refzd2appz00_bglt BgL_auxz00_5763;

					if (CBOOL
						(BGl_za2optimzd2cfazd2freezd2varzd2trackingzf3za2zf3zzengine_paramz00))
						{	/* Cfa/procedure.scm 127 */
							BgL_auxz00_5771 = BGl_makezd2emptyzd2approxz00zzcfa_approxz00();
						}
					else
						{	/* Cfa/procedure.scm 127 */
							BgL_auxz00_5771 =
								BGl_makezd2typezd2approxz00zzcfa_approxz00(
								((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
						}
					{
						obj_t BgL_auxz00_5764;

						{	/* Cfa/procedure.scm 127 */
							BgL_objectz00_bglt BgL_tmpz00_5765;

							BgL_tmpz00_5765 =
								((BgL_objectz00_bglt)
								((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4799)));
							BgL_auxz00_5764 = BGL_OBJECT_WIDENING(BgL_tmpz00_5765);
						}
						BgL_auxz00_5763 =
							((BgL_procedurezd2refzd2appz00_bglt) BgL_auxz00_5764);
					}
					((((BgL_procedurezd2refzd2appz00_bglt) COBJECT(BgL_auxz00_5763))->
							BgL_approxz00) = ((BgL_approxz00_bglt) BgL_auxz00_5771), BUNSPEC);
				}
				return
					((obj_t) ((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4799)));
			}
		}

	}



/* &node-setup!-pre-make1523 */
	obj_t BGl_z62nodezd2setupz12zd2prezd2make1523za2zzcfa_procedurez00(obj_t
		BgL_envz00_4605, obj_t BgL_nodez00_4606)
	{
		{	/* Cfa/procedure.scm 62 */
			{

				{
					obj_t BgL_lenz00_4818;
					obj_t BgL_lenz00_4813;

					BGl_addzd2makezd2procedurez12z12zzcfa_closurez00(
						((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4606)));
					{	/* Cfa/procedure.scm 81 */
						obj_t BgL_arg1594z00_4823;

						BgL_arg1594z00_4823 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4606))))->BgL_argsz00);
						BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1594z00_4823);
					}
					{	/* Cfa/procedure.scm 82 */
						BgL_variablez00_bglt BgL_ownerz00_4824;

						{
							BgL_prezd2makezd2procedurezd2appzd2_bglt BgL_auxz00_5788;

							{
								obj_t BgL_auxz00_5789;

								{	/* Cfa/procedure.scm 82 */
									BgL_objectz00_bglt BgL_tmpz00_5790;

									BgL_tmpz00_5790 =
										((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4606));
									BgL_auxz00_5789 = BGL_OBJECT_WIDENING(BgL_tmpz00_5790);
								}
								BgL_auxz00_5788 =
									((BgL_prezd2makezd2procedurezd2appzd2_bglt) BgL_auxz00_5789);
							}
							BgL_ownerz00_4824 =
								(((BgL_prezd2makezd2procedurezd2appzd2_bglt)
									COBJECT(BgL_auxz00_5788))->BgL_ownerz00);
						}
						{	/* Cfa/procedure.scm 82 */
							BgL_appz00_bglt BgL_nodez00_4825;

							{	/* Cfa/procedure.scm 83 */
								long BgL_arg1646z00_4826;

								{	/* Cfa/procedure.scm 83 */
									obj_t BgL_arg1650z00_4827;

									{	/* Cfa/procedure.scm 83 */
										obj_t BgL_arg1651z00_4828;

										{	/* Cfa/procedure.scm 83 */
											obj_t BgL_arg1815z00_4829;
											long BgL_arg1816z00_4830;

											BgL_arg1815z00_4829 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/procedure.scm 83 */
												long BgL_arg1817z00_4831;

												BgL_arg1817z00_4831 =
													BGL_OBJECT_CLASS_NUM(
													((BgL_objectz00_bglt)
														((BgL_appz00_bglt) BgL_nodez00_4606)));
												BgL_arg1816z00_4830 =
													(BgL_arg1817z00_4831 - OBJECT_TYPE);
											}
											BgL_arg1651z00_4828 =
												VECTOR_REF(BgL_arg1815z00_4829, BgL_arg1816z00_4830);
										}
										BgL_arg1650z00_4827 =
											BGl_classzd2superzd2zz__objectz00(BgL_arg1651z00_4828);
									}
									{	/* Cfa/procedure.scm 83 */
										obj_t BgL_tmpz00_5803;

										BgL_tmpz00_5803 = ((obj_t) BgL_arg1650z00_4827);
										BgL_arg1646z00_4826 = BGL_CLASS_NUM(BgL_tmpz00_5803);
								}}
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4606)), BgL_arg1646z00_4826);
							}
							{	/* Cfa/procedure.scm 83 */
								BgL_objectz00_bglt BgL_tmpz00_5809;

								BgL_tmpz00_5809 =
									((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4606));
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5809, BFALSE);
							}
							((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4606));
							BgL_nodez00_4825 = ((BgL_appz00_bglt) BgL_nodez00_4606);
							{	/* Cfa/procedure.scm 83 */

								{	/* Cfa/procedure.scm 84 */
									obj_t BgL_proczd2siza7ez75_4832;
									obj_t BgL_procz00_4833;

									{	/* Cfa/procedure.scm 84 */
										obj_t BgL_arg1629z00_4834;

										{	/* Cfa/procedure.scm 84 */
											obj_t BgL_pairz00_4835;

											BgL_pairz00_4835 =
												(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt)
															((BgL_appz00_bglt) BgL_nodez00_4606))))->
												BgL_argsz00);
											BgL_arg1629z00_4834 = CAR(CDR(CDR(BgL_pairz00_4835)));
										}
										BgL_proczd2siza7ez75_4832 =
											BGl_getzd2nodezd2atomzd2valuezd2zzcfa_approxz00(
											((BgL_nodez00_bglt) BgL_arg1629z00_4834));
									}
									{	/* Cfa/procedure.scm 85 */
										obj_t BgL_pairz00_4836;

										BgL_pairz00_4836 =
											(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt)
														((BgL_appz00_bglt) BgL_nodez00_4606))))->
											BgL_argsz00);
										BgL_procz00_4833 = CAR(BgL_pairz00_4836);
									}
									{	/* Cfa/procedure.scm 86 */
										bool_t BgL_test2168z00_5828;

										if (INTEGERP(BgL_proczd2siza7ez75_4832))
											{	/* Cfa/procedure.scm 87 */
												bool_t BgL_test2170z00_5831;

												{	/* Cfa/procedure.scm 87 */
													obj_t BgL_classz00_4837;

													BgL_classz00_4837 = BGl_varz00zzast_nodez00;
													if (BGL_OBJECTP(BgL_procz00_4833))
														{	/* Cfa/procedure.scm 87 */
															BgL_objectz00_bglt BgL_arg1807z00_4838;

															BgL_arg1807z00_4838 =
																(BgL_objectz00_bglt) (BgL_procz00_4833);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Cfa/procedure.scm 87 */
																	long BgL_idxz00_4839;

																	BgL_idxz00_4839 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_4838);
																	BgL_test2170z00_5831 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_4839 + 2L)) ==
																		BgL_classz00_4837);
																}
															else
																{	/* Cfa/procedure.scm 87 */
																	bool_t BgL_res2053z00_4842;

																	{	/* Cfa/procedure.scm 87 */
																		obj_t BgL_oclassz00_4843;

																		{	/* Cfa/procedure.scm 87 */
																			obj_t BgL_arg1815z00_4844;
																			long BgL_arg1816z00_4845;

																			BgL_arg1815z00_4844 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Cfa/procedure.scm 87 */
																				long BgL_arg1817z00_4846;

																				BgL_arg1817z00_4846 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_4838);
																				BgL_arg1816z00_4845 =
																					(BgL_arg1817z00_4846 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_4843 =
																				VECTOR_REF(BgL_arg1815z00_4844,
																				BgL_arg1816z00_4845);
																		}
																		{	/* Cfa/procedure.scm 87 */
																			bool_t BgL__ortest_1115z00_4847;

																			BgL__ortest_1115z00_4847 =
																				(BgL_classz00_4837 ==
																				BgL_oclassz00_4843);
																			if (BgL__ortest_1115z00_4847)
																				{	/* Cfa/procedure.scm 87 */
																					BgL_res2053z00_4842 =
																						BgL__ortest_1115z00_4847;
																				}
																			else
																				{	/* Cfa/procedure.scm 87 */
																					long BgL_odepthz00_4848;

																					{	/* Cfa/procedure.scm 87 */
																						obj_t BgL_arg1804z00_4849;

																						BgL_arg1804z00_4849 =
																							(BgL_oclassz00_4843);
																						BgL_odepthz00_4848 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_4849);
																					}
																					if ((2L < BgL_odepthz00_4848))
																						{	/* Cfa/procedure.scm 87 */
																							obj_t BgL_arg1802z00_4850;

																							{	/* Cfa/procedure.scm 87 */
																								obj_t BgL_arg1803z00_4851;

																								BgL_arg1803z00_4851 =
																									(BgL_oclassz00_4843);
																								BgL_arg1802z00_4850 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_4851, 2L);
																							}
																							BgL_res2053z00_4842 =
																								(BgL_arg1802z00_4850 ==
																								BgL_classz00_4837);
																						}
																					else
																						{	/* Cfa/procedure.scm 87 */
																							BgL_res2053z00_4842 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2170z00_5831 = BgL_res2053z00_4842;
																}
														}
													else
														{	/* Cfa/procedure.scm 87 */
															BgL_test2170z00_5831 = ((bool_t) 0);
														}
												}
												if (BgL_test2170z00_5831)
													{	/* Cfa/procedure.scm 88 */
														BgL_valuez00_bglt BgL_arg1626z00_4852;

														BgL_arg1626z00_4852 =
															(((BgL_variablez00_bglt) COBJECT(
																	(((BgL_varz00_bglt) COBJECT(
																				((BgL_varz00_bglt) BgL_procz00_4833)))->
																		BgL_variablez00)))->BgL_valuez00);
														{	/* Cfa/procedure.scm 88 */
															obj_t BgL_classz00_4853;

															BgL_classz00_4853 = BGl_funz00zzast_varz00;
															{	/* Cfa/procedure.scm 88 */
																BgL_objectz00_bglt BgL_arg1807z00_4854;

																{	/* Cfa/procedure.scm 88 */
																	obj_t BgL_tmpz00_5857;

																	BgL_tmpz00_5857 =
																		((obj_t)
																		((BgL_objectz00_bglt) BgL_arg1626z00_4852));
																	BgL_arg1807z00_4854 =
																		(BgL_objectz00_bglt) (BgL_tmpz00_5857);
																}
																if (BGL_CONDEXPAND_ISA_ARCH64())
																	{	/* Cfa/procedure.scm 88 */
																		long BgL_idxz00_4855;

																		BgL_idxz00_4855 =
																			BGL_OBJECT_INHERITANCE_NUM
																			(BgL_arg1807z00_4854);
																		BgL_test2168z00_5828 =
																			(VECTOR_REF
																			(BGl_za2inheritancesza2z00zz__objectz00,
																				(BgL_idxz00_4855 + 2L)) ==
																			BgL_classz00_4853);
																	}
																else
																	{	/* Cfa/procedure.scm 88 */
																		bool_t BgL_res2054z00_4858;

																		{	/* Cfa/procedure.scm 88 */
																			obj_t BgL_oclassz00_4859;

																			{	/* Cfa/procedure.scm 88 */
																				obj_t BgL_arg1815z00_4860;
																				long BgL_arg1816z00_4861;

																				BgL_arg1815z00_4860 =
																					(BGl_za2classesza2z00zz__objectz00);
																				{	/* Cfa/procedure.scm 88 */
																					long BgL_arg1817z00_4862;

																					BgL_arg1817z00_4862 =
																						BGL_OBJECT_CLASS_NUM
																						(BgL_arg1807z00_4854);
																					BgL_arg1816z00_4861 =
																						(BgL_arg1817z00_4862 - OBJECT_TYPE);
																				}
																				BgL_oclassz00_4859 =
																					VECTOR_REF(BgL_arg1815z00_4860,
																					BgL_arg1816z00_4861);
																			}
																			{	/* Cfa/procedure.scm 88 */
																				bool_t BgL__ortest_1115z00_4863;

																				BgL__ortest_1115z00_4863 =
																					(BgL_classz00_4853 ==
																					BgL_oclassz00_4859);
																				if (BgL__ortest_1115z00_4863)
																					{	/* Cfa/procedure.scm 88 */
																						BgL_res2054z00_4858 =
																							BgL__ortest_1115z00_4863;
																					}
																				else
																					{	/* Cfa/procedure.scm 88 */
																						long BgL_odepthz00_4864;

																						{	/* Cfa/procedure.scm 88 */
																							obj_t BgL_arg1804z00_4865;

																							BgL_arg1804z00_4865 =
																								(BgL_oclassz00_4859);
																							BgL_odepthz00_4864 =
																								BGL_CLASS_DEPTH
																								(BgL_arg1804z00_4865);
																						}
																						if ((2L < BgL_odepthz00_4864))
																							{	/* Cfa/procedure.scm 88 */
																								obj_t BgL_arg1802z00_4866;

																								{	/* Cfa/procedure.scm 88 */
																									obj_t BgL_arg1803z00_4867;

																									BgL_arg1803z00_4867 =
																										(BgL_oclassz00_4859);
																									BgL_arg1802z00_4866 =
																										BGL_CLASS_ANCESTORS_REF
																										(BgL_arg1803z00_4867, 2L);
																								}
																								BgL_res2054z00_4858 =
																									(BgL_arg1802z00_4866 ==
																									BgL_classz00_4853);
																							}
																						else
																							{	/* Cfa/procedure.scm 88 */
																								BgL_res2054z00_4858 =
																									((bool_t) 0);
																							}
																					}
																			}
																		}
																		BgL_test2168z00_5828 = BgL_res2054z00_4858;
																	}
															}
														}
													}
												else
													{	/* Cfa/procedure.scm 87 */
														BgL_test2168z00_5828 = ((bool_t) 0);
													}
											}
										else
											{	/* Cfa/procedure.scm 86 */
												BgL_test2168z00_5828 = ((bool_t) 0);
											}
										if (BgL_test2168z00_5828)
											{	/* Cfa/procedure.scm 89 */
												obj_t BgL_vapproxz00_4868;

												if (CBOOL
													(BGl_za2optimzd2cfazd2freezd2varzd2trackingzf3za2zf3zzengine_paramz00))
													{	/* Cfa/procedure.scm 89 */
														BgL_lenz00_4813 = BgL_proczd2siza7ez75_4832;
														{	/* Cfa/procedure.scm 72 */
															obj_t BgL_resz00_4814;

															BgL_resz00_4814 =
																make_vector(
																(long) CINT(BgL_lenz00_4813), BUNSPEC);
															{
																long BgL_iz00_4816;

																BgL_iz00_4816 = 0L;
															BgL_loopz00_4815:
																if (
																	(BgL_iz00_4816 ==
																		(long) CINT(BgL_lenz00_4813)))
																	{	/* Cfa/procedure.scm 74 */
																		BgL_vapproxz00_4868 = BgL_resz00_4814;
																	}
																else
																	{	/* Cfa/procedure.scm 74 */
																		{	/* Cfa/procedure.scm 77 */
																			BgL_approxz00_bglt BgL_arg1681z00_4817;

																			BgL_arg1681z00_4817 =
																				BGl_makezd2emptyzd2approxz00zzcfa_approxz00
																				();
																			VECTOR_SET(BgL_resz00_4814, BgL_iz00_4816,
																				((obj_t) BgL_arg1681z00_4817));
																		}
																		{
																			long BgL_iz00_5890;

																			BgL_iz00_5890 = (BgL_iz00_4816 + 1L);
																			BgL_iz00_4816 = BgL_iz00_5890;
																			goto BgL_loopz00_4815;
																		}
																	}
															}
														}
													}
												else
													{	/* Cfa/procedure.scm 89 */
														BgL_lenz00_4818 = BgL_proczd2siza7ez75_4832;
														{	/* Cfa/procedure.scm 64 */
															obj_t BgL_resz00_4819;

															BgL_resz00_4819 =
																make_vector(
																(long) CINT(BgL_lenz00_4818), BUNSPEC);
															{
																long BgL_iz00_4821;

																BgL_iz00_4821 = 0L;
															BgL_loopz00_4820:
																if (
																	(BgL_iz00_4821 ==
																		(long) CINT(BgL_lenz00_4818)))
																	{	/* Cfa/procedure.scm 66 */
																		BgL_vapproxz00_4868 = BgL_resz00_4819;
																	}
																else
																	{	/* Cfa/procedure.scm 66 */
																		{	/* Cfa/procedure.scm 69 */
																			BgL_approxz00_bglt BgL_arg1661z00_4822;

																			BgL_arg1661z00_4822 =
																				BGl_makezd2typezd2approxz00zzcfa_approxz00
																				(((BgL_typez00_bglt)
																					BGl_za2objza2z00zztype_cachez00));
																			VECTOR_SET(BgL_resz00_4819, BgL_iz00_4821,
																				((obj_t) BgL_arg1661z00_4822));
																		}
																		{
																			long BgL_iz00_5901;

																			BgL_iz00_5901 = (BgL_iz00_4821 + 1L);
																			BgL_iz00_4821 = BgL_iz00_5901;
																			goto BgL_loopz00_4820;
																		}
																	}
															}
														}
													}
												{	/* Cfa/procedure.scm 89 */
													BgL_appz00_bglt BgL_nodez00_4869;

													{	/* Cfa/procedure.scm 92 */
														BgL_makezd2procedurezd2appz00_bglt
															BgL_wide1167z00_4870;
														BgL_wide1167z00_4870 =
															((BgL_makezd2procedurezd2appz00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_makezd2procedurezd2appz00_bgl))));
														{	/* Cfa/procedure.scm 92 */
															obj_t BgL_auxz00_5908;
															BgL_objectz00_bglt BgL_tmpz00_5904;

															BgL_auxz00_5908 = ((obj_t) BgL_wide1167z00_4870);
															BgL_tmpz00_5904 =
																((BgL_objectz00_bglt)
																((BgL_appz00_bglt)
																	((BgL_appz00_bglt) BgL_nodez00_4825)));
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5904,
																BgL_auxz00_5908);
														}
														((BgL_objectz00_bglt)
															((BgL_appz00_bglt)
																((BgL_appz00_bglt) BgL_nodez00_4825)));
														{	/* Cfa/procedure.scm 92 */
															long BgL_arg1625z00_4871;

															BgL_arg1625z00_4871 =
																BGL_CLASS_NUM
																(BGl_makezd2procedurezd2appz00zzcfa_info2z00);
															BGL_OBJECT_CLASS_NUM_SET(((BgL_objectz00_bglt) (
																		(BgL_appz00_bglt) ((BgL_appz00_bglt)
																			BgL_nodez00_4825))), BgL_arg1625z00_4871);
														}
														((BgL_appz00_bglt)
															((BgL_appz00_bglt)
																((BgL_appz00_bglt) BgL_nodez00_4825)));
													}
													{
														BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5922;

														{
															obj_t BgL_auxz00_5923;

															{	/* Cfa/procedure.scm 94 */
																BgL_objectz00_bglt BgL_tmpz00_5924;

																BgL_tmpz00_5924 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_4825)));
																BgL_auxz00_5923 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5924);
															}
															BgL_auxz00_5922 =
																((BgL_makezd2procedurezd2appz00_bglt)
																BgL_auxz00_5923);
														}
														((((BgL_makezd2procedurezd2appz00_bglt)
																	COBJECT(BgL_auxz00_5922))->BgL_approxz00) =
															((BgL_approxz00_bglt)
																BGl_makezd2emptyzd2approxz00zzcfa_approxz00()),
															BUNSPEC);
													}
													{
														BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5932;

														{
															obj_t BgL_auxz00_5933;

															{	/* Cfa/procedure.scm 95 */
																BgL_objectz00_bglt BgL_tmpz00_5934;

																BgL_tmpz00_5934 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_4825)));
																BgL_auxz00_5933 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5934);
															}
															BgL_auxz00_5932 =
																((BgL_makezd2procedurezd2appz00_bglt)
																BgL_auxz00_5933);
														}
														((((BgL_makezd2procedurezd2appz00_bglt)
																	COBJECT(BgL_auxz00_5932))->
																BgL_valueszd2approxzd2) =
															((obj_t) BgL_vapproxz00_4868), BUNSPEC);
													}
													{
														BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5941;

														{
															obj_t BgL_auxz00_5942;

															{	/* Cfa/procedure.scm 89 */
																BgL_objectz00_bglt BgL_tmpz00_5943;

																BgL_tmpz00_5943 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_4825)));
																BgL_auxz00_5942 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5943);
															}
															BgL_auxz00_5941 =
																((BgL_makezd2procedurezd2appz00_bglt)
																BgL_auxz00_5942);
														}
														((((BgL_makezd2procedurezd2appz00_bglt)
																	COBJECT(BgL_auxz00_5941))->
																BgL_lostzd2stampzd2) = ((long) -1L), BUNSPEC);
													}
													{
														BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5950;

														{
															obj_t BgL_auxz00_5951;

															{	/* Cfa/procedure.scm 89 */
																BgL_objectz00_bglt BgL_tmpz00_5952;

																BgL_tmpz00_5952 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_4825)));
																BgL_auxz00_5951 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5952);
															}
															BgL_auxz00_5950 =
																((BgL_makezd2procedurezd2appz00_bglt)
																BgL_auxz00_5951);
														}
														((((BgL_makezd2procedurezd2appz00_bglt)
																	COBJECT(BgL_auxz00_5950))->BgL_xzd2tzf3z21) =
															((bool_t) ((bool_t) 1)), BUNSPEC);
													}
													{
														BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5959;

														{
															obj_t BgL_auxz00_5960;

															{	/* Cfa/procedure.scm 89 */
																BgL_objectz00_bglt BgL_tmpz00_5961;

																BgL_tmpz00_5961 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_4825)));
																BgL_auxz00_5960 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5961);
															}
															BgL_auxz00_5959 =
																((BgL_makezd2procedurezd2appz00_bglt)
																BgL_auxz00_5960);
														}
														((((BgL_makezd2procedurezd2appz00_bglt)
																	COBJECT(BgL_auxz00_5959))->BgL_xz00) =
															((bool_t) ((bool_t) 0)), BUNSPEC);
													}
													{
														BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5968;

														{
															obj_t BgL_auxz00_5969;

															{	/* Cfa/procedure.scm 89 */
																BgL_objectz00_bglt BgL_tmpz00_5970;

																BgL_tmpz00_5970 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_4825)));
																BgL_auxz00_5969 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5970);
															}
															BgL_auxz00_5968 =
																((BgL_makezd2procedurezd2appz00_bglt)
																BgL_auxz00_5969);
														}
														((((BgL_makezd2procedurezd2appz00_bglt)
																	COBJECT(BgL_auxz00_5968))->BgL_tz00) =
															((bool_t) ((bool_t) 0)), BUNSPEC);
													}
													{
														BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5977;

														{
															obj_t BgL_auxz00_5978;

															{	/* Cfa/procedure.scm 93 */
																BgL_objectz00_bglt BgL_tmpz00_5979;

																BgL_tmpz00_5979 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_4825)));
																BgL_auxz00_5978 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5979);
															}
															BgL_auxz00_5977 =
																((BgL_makezd2procedurezd2appz00_bglt)
																BgL_auxz00_5978);
														}
														((((BgL_makezd2procedurezd2appz00_bglt)
																	COBJECT(BgL_auxz00_5977))->BgL_ownerz00) =
															((BgL_variablez00_bglt) BgL_ownerz00_4824),
															BUNSPEC);
													}
													{
														BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_5986;

														{
															obj_t BgL_auxz00_5987;

															{	/* Cfa/procedure.scm 89 */
																BgL_objectz00_bglt BgL_tmpz00_5988;

																BgL_tmpz00_5988 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_4825)));
																BgL_auxz00_5987 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5988);
															}
															BgL_auxz00_5986 =
																((BgL_makezd2procedurezd2appz00_bglt)
																BgL_auxz00_5987);
														}
														((((BgL_makezd2procedurezd2appz00_bglt)
																	COBJECT(BgL_auxz00_5986))->
																BgL_stackzd2stampzd2) =
															((obj_t) BNIL), BUNSPEC);
													}
													BgL_nodez00_4869 =
														((BgL_appz00_bglt)
														((BgL_appz00_bglt) BgL_nodez00_4825));
													{	/* Cfa/procedure.scm 92 */

														{	/* Cfa/procedure.scm 101 */
															BgL_approxz00_bglt BgL_arg1609z00_4872;

															BgL_arg1609z00_4872 =
																BGl_makezd2typezd2alloczd2approxzd2zzcfa_approxz00
																(((BgL_typez00_bglt)
																	BGl_za2procedureza2z00zztype_cachez00),
																((BgL_nodez00_bglt) BgL_nodez00_4869));
															{
																BgL_makezd2procedurezd2appz00_bglt
																	BgL_auxz00_6000;
																{
																	obj_t BgL_auxz00_6001;

																	{	/* Cfa/procedure.scm 99 */
																		BgL_objectz00_bglt BgL_tmpz00_6002;

																		BgL_tmpz00_6002 =
																			((BgL_objectz00_bglt) BgL_nodez00_4869);
																		BgL_auxz00_6001 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_6002);
																	}
																	BgL_auxz00_6000 =
																		((BgL_makezd2procedurezd2appz00_bglt)
																		BgL_auxz00_6001);
																}
																((((BgL_makezd2procedurezd2appz00_bglt)
																			COBJECT(BgL_auxz00_6000))->
																		BgL_approxz00) =
																	((BgL_approxz00_bglt) BgL_arg1609z00_4872),
																	BUNSPEC);
														}}
														{	/* Cfa/procedure.scm 104 */
															obj_t BgL_cloz00_4873;

															{	/* Cfa/procedure.scm 104 */
																obj_t BgL_pairz00_4874;

																BgL_pairz00_4874 =
																	(((BgL_sfunz00_bglt) COBJECT(
																			((BgL_sfunz00_bglt)
																				(((BgL_variablez00_bglt) COBJECT(
																							(((BgL_varz00_bglt) COBJECT(
																										((BgL_varz00_bglt)
																											BgL_procz00_4833)))->
																								BgL_variablez00)))->
																					BgL_valuez00))))->BgL_argsz00);
																BgL_cloz00_4873 = CAR(BgL_pairz00_4874);
															}
															{	/* Cfa/procedure.scm 104 */
																BgL_valuez00_bglt BgL_vcloz00_4875;

																BgL_vcloz00_4875 =
																	(((BgL_variablez00_bglt) COBJECT(
																			((BgL_variablez00_bglt)
																				((BgL_localz00_bglt)
																					BgL_cloz00_4873))))->BgL_valuez00);
																{	/* Cfa/procedure.scm 107 */

																	{	/* Cfa/procedure.scm 108 */
																		bool_t BgL_test2181z00_6016;

																		{	/* Cfa/procedure.scm 108 */
																			obj_t BgL_classz00_4876;

																			BgL_classz00_4876 =
																				BGl_svarzf2Cinfozf2zzcfa_infoz00;
																			{	/* Cfa/procedure.scm 108 */
																				BgL_objectz00_bglt BgL_arg1807z00_4877;

																				{	/* Cfa/procedure.scm 108 */
																					obj_t BgL_tmpz00_6017;

																					BgL_tmpz00_6017 =
																						((obj_t)
																						((BgL_objectz00_bglt)
																							BgL_vcloz00_4875));
																					BgL_arg1807z00_4877 =
																						(BgL_objectz00_bglt)
																						(BgL_tmpz00_6017);
																				}
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Cfa/procedure.scm 108 */
																						long BgL_idxz00_4878;

																						BgL_idxz00_4878 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_4877);
																						BgL_test2181z00_6016 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_4878 + 3L)) ==
																							BgL_classz00_4876);
																					}
																				else
																					{	/* Cfa/procedure.scm 108 */
																						bool_t BgL_res2055z00_4881;

																						{	/* Cfa/procedure.scm 108 */
																							obj_t BgL_oclassz00_4882;

																							{	/* Cfa/procedure.scm 108 */
																								obj_t BgL_arg1815z00_4883;
																								long BgL_arg1816z00_4884;

																								BgL_arg1815z00_4883 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Cfa/procedure.scm 108 */
																									long BgL_arg1817z00_4885;

																									BgL_arg1817z00_4885 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_4877);
																									BgL_arg1816z00_4884 =
																										(BgL_arg1817z00_4885 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_4882 =
																									VECTOR_REF
																									(BgL_arg1815z00_4883,
																									BgL_arg1816z00_4884);
																							}
																							{	/* Cfa/procedure.scm 108 */
																								bool_t BgL__ortest_1115z00_4886;

																								BgL__ortest_1115z00_4886 =
																									(BgL_classz00_4876 ==
																									BgL_oclassz00_4882);
																								if (BgL__ortest_1115z00_4886)
																									{	/* Cfa/procedure.scm 108 */
																										BgL_res2055z00_4881 =
																											BgL__ortest_1115z00_4886;
																									}
																								else
																									{	/* Cfa/procedure.scm 108 */
																										long BgL_odepthz00_4887;

																										{	/* Cfa/procedure.scm 108 */
																											obj_t BgL_arg1804z00_4888;

																											BgL_arg1804z00_4888 =
																												(BgL_oclassz00_4882);
																											BgL_odepthz00_4887 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_4888);
																										}
																										if (
																											(3L < BgL_odepthz00_4887))
																											{	/* Cfa/procedure.scm 108 */
																												obj_t
																													BgL_arg1802z00_4889;
																												{	/* Cfa/procedure.scm 108 */
																													obj_t
																														BgL_arg1803z00_4890;
																													BgL_arg1803z00_4890 =
																														(BgL_oclassz00_4882);
																													BgL_arg1802z00_4889 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_4890,
																														3L);
																												}
																												BgL_res2055z00_4881 =
																													(BgL_arg1802z00_4889
																													== BgL_classz00_4876);
																											}
																										else
																											{	/* Cfa/procedure.scm 108 */
																												BgL_res2055z00_4881 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test2181z00_6016 =
																							BgL_res2055z00_4881;
																					}
																			}
																		}
																		if (BgL_test2181z00_6016)
																			{	/* Cfa/procedure.scm 108 */
																				{
																					BgL_svarzf2cinfozf2_bglt
																						BgL_auxz00_6040;
																					{
																						obj_t BgL_auxz00_6041;

																						{	/* Cfa/procedure.scm 112 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_6042;
																							BgL_tmpz00_6042 =
																								((BgL_objectz00_bglt) (
																									(BgL_svarz00_bglt)
																									BgL_vcloz00_4875));
																							BgL_auxz00_6041 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_6042);
																						}
																						BgL_auxz00_6040 =
																							((BgL_svarzf2cinfozf2_bglt)
																							BgL_auxz00_6041);
																					}
																					return
																						((((BgL_svarzf2cinfozf2_bglt)
																								COBJECT(BgL_auxz00_6040))->
																							BgL_clozd2envzf3z21) =
																						((bool_t) ((bool_t) 1)), BUNSPEC);
																				}
																			}
																		else
																			{	/* Cfa/procedure.scm 108 */
																				{	/* Cfa/procedure.scm 113 */
																					BgL_prezd2clozd2envz00_bglt
																						BgL_wide1171z00_4891;
																					BgL_wide1171z00_4891 =
																						((BgL_prezd2clozd2envz00_bglt)
																						BOBJECT(GC_MALLOC(sizeof(struct
																									BgL_prezd2clozd2envz00_bgl))));
																					{	/* Cfa/procedure.scm 113 */
																						obj_t BgL_auxz00_6053;
																						BgL_objectz00_bglt BgL_tmpz00_6049;

																						BgL_auxz00_6053 =
																							((obj_t) BgL_wide1171z00_4891);
																						BgL_tmpz00_6049 =
																							((BgL_objectz00_bglt)
																							((BgL_svarz00_bglt)
																								((BgL_svarz00_bglt)
																									BgL_vcloz00_4875)));
																						BGL_OBJECT_WIDENING_SET
																							(BgL_tmpz00_6049,
																							BgL_auxz00_6053);
																					}
																					((BgL_objectz00_bglt)
																						((BgL_svarz00_bglt)
																							((BgL_svarz00_bglt)
																								BgL_vcloz00_4875)));
																					{	/* Cfa/procedure.scm 113 */
																						long BgL_arg1611z00_4892;

																						BgL_arg1611z00_4892 =
																							BGL_CLASS_NUM
																							(BGl_prezd2clozd2envz00zzcfa_infoz00);
																						BGL_OBJECT_CLASS_NUM_SET((
																								(BgL_objectz00_bglt) (
																									(BgL_svarz00_bglt) (
																										(BgL_svarz00_bglt)
																										BgL_vcloz00_4875))),
																							BgL_arg1611z00_4892);
																					}
																					((BgL_svarz00_bglt)
																						((BgL_svarz00_bglt)
																							((BgL_svarz00_bglt)
																								BgL_vcloz00_4875)));
																				}
																				return
																					((obj_t)
																					((BgL_svarz00_bglt)
																						((BgL_svarz00_bglt)
																							BgL_vcloz00_4875)));
																			}
																	}
																}
															}
														}
													}
												}
											}
										else
											{	/* Cfa/procedure.scm 86 */
												{	/* Cfa/procedure.scm 62 */
													obj_t BgL_nextzd2method1522zd2_4810;

													BgL_nextzd2method1522zd2_4810 =
														BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
														((BgL_objectz00_bglt)
															((BgL_appz00_bglt) BgL_nodez00_4606)),
														BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
														BGl_prezd2makezd2procedurezd2appzd2zzcfa_info2z00);
													return
														BGL_PROCEDURE_CALL1(BgL_nextzd2method1522zd2_4810,
														((obj_t) ((BgL_appz00_bglt) BgL_nodez00_4606)));
												}
											}
									}
								}
							}
						}
					}
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_procedurez00(void)
	{
		{	/* Cfa/procedure.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
			BGl_modulezd2initializa7ationz75zzcfa_loosez00(471177480L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
			BGl_modulezd2initializa7ationz75zzcfa_setupz00(168272122L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
			BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
			BGl_modulezd2initializa7ationz75zzcfa_cfaz00(400853817L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
			BGl_modulezd2initializa7ationz75zzcfa_iteratez00(131412063L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_closurez00(189402713L,
				BSTRING_TO_STRING(BGl_string2086z00zzcfa_procedurez00));
		}

	}

#ifdef __cplusplus
}
#endif
