/*===========================================================================*/
/*   (Cfa/tvector.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/tvector.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_TVECTOR_TYPE_DEFINITIONS
#define BGL_CFA_TVECTOR_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_literalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}                 *BgL_literalz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_vlengthz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_ftypez00;
	}                 *BgL_vlengthz00_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_kwotezf2nodezf2_bgl
	{
		struct BgL_nodez00_bgl *BgL_nodez00;
	}                      *BgL_kwotezf2nodezf2_bglt;

	typedef struct BgL_kwotezf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                       *BgL_kwotezf2cinfozf2_bglt;

	typedef struct BgL_makezd2vectorzd2appz00_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		struct BgL_approxz00_bgl *BgL_valuezd2approxzd2;
		long BgL_lostzd2stampzd2;
		struct BgL_variablez00_bgl *BgL_ownerz00;
		obj_t BgL_stackzd2stampzd2;
		bool_t BgL_seenzf3zf3;
		bool_t BgL_tvectorzf3zf3;
	}                             *BgL_makezd2vectorzd2appz00_bglt;

	typedef struct BgL_vrefzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_tvectorzf3zf3;
	}                      *BgL_vrefzf2cinfozf2_bglt;

	typedef struct BgL_vsetz12zf2cinfoze0_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_tvectorzf3zf3;
	}                         *BgL_vsetz12zf2cinfoze0_bglt;

	typedef struct BgL_vlengthzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_tvectorzf3zf3;
	}                         *BgL_vlengthzf2cinfozf2_bglt;

	typedef struct BgL_valloczf2cinfozb2optimz40_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		struct BgL_approxz00_bgl *BgL_valuezd2approxzd2;
		long BgL_lostzd2stampzd2;
		struct BgL_variablez00_bgl *BgL_ownerz00;
		bool_t BgL_stackablezf3zf3;
		obj_t BgL_stackzd2stampzd2;
		bool_t BgL_seenzf3zf3;
	}                                *BgL_valloczf2cinfozb2optimz40_bglt;


#endif													// BGL_CFA_TVECTOR_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	extern obj_t BGl_getzd2approxzd2typez00zzcfa_typez00(BgL_approxz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(obj_t,
		obj_t);
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_z62patchz12zd2letzd2var1680z70zzcfa_tvectorz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_patchzd2vectorzf3z12z33zzcfa_tvectorz00(BgL_appz00_bglt);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern BgL_nodez00_bglt
		BGl_inlinezd2nodezd2zzinline_inlinez00(BgL_nodez00_bglt, long, obj_t);
	static obj_t BGl_z62patchz12zd2closure1656za2zzcfa_tvectorz00(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_z62patchz12zd2vrefzf2Cinfo1700z50zzcfa_tvectorz00(obj_t,
		obj_t);
	static obj_t BGl_objectzd2initzd2zzcfa_tvectorz00(void);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_z62patchz121645z70zzcfa_tvectorz00(obj_t, obj_t);
	extern obj_t BGl_inlinezd2setupz12zc0zzinline_walkz00(obj_t);
	static obj_t BGl_z62patchz12zd2atom1648za2zzcfa_tvectorz00(obj_t, obj_t);
	static obj_t BGl_z62unpatchzd2vectorzd2setz12z70zzcfa_tvectorz00(obj_t);
	static obj_t BGl_methodzd2initzd2zzcfa_tvectorz00(void);
	static obj_t BGl_z62patchz12zd2makezd2vectorzd2a1696za2zzcfa_tvectorz00(obj_t,
		obj_t);
	extern obj_t BGl_externz00zzast_nodez00;
	static obj_t BGl_z62patchz12zd2appzd2ly1662z70zzcfa_tvectorz00(obj_t, obj_t);
	static obj_t BGl_z62patchz12zd2conditional1672za2zzcfa_tvectorz00(obj_t,
		obj_t);
	static obj_t BGl_z62patchz12zd2kwotezf2node1652z50zzcfa_tvectorz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_vectorzd2ze3tvectorz12z23zzcfa_tvectorz00(obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static obj_t BGl_z62patchz12zd2extern1666za2zzcfa_tvectorz00(obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t BGl_buildzd2astzd2sanszd2removezd2zzast_buildz00(obj_t);
	static obj_t BGl_z62patchz12zd2var1654za2zzcfa_tvectorz00(obj_t, obj_t);
	extern obj_t BGl_pragmazd2finaliza7erz75zzmodule_pragmaz00(void);
	BGL_EXPORTED_DECL obj_t
		BGl_unpatchzd2vectorzd2setz12z12zzcfa_tvectorz00(void);
	static obj_t BGl_patchzd2funz12zc0zzcfa_tvectorz00(obj_t);
	extern obj_t BGl_cfunz00zzast_varz00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	extern obj_t BGl_vsetz12zf2Cinfoze0zzcfa_info3z00;
	extern obj_t BGl_kwotezf2Cinfozf2zzcfa_infoz00;
	static obj_t BGl_z62patchz12zd2vsetz12zf2Cinfo1702z42zzcfa_tvectorz00(obj_t,
		obj_t);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_patchz12z12zzcfa_tvectorz00(BgL_nodez00_bglt);
	static obj_t BGl_patchza2z12zb0zzcfa_tvectorz00(obj_t);
	static obj_t BGl_z62getzd2vectorzd2itemzd2type1638zb0zzcfa_tvectorz00(obj_t,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_tvectorz00(void);
	static obj_t BGl_z62patchz12zd2vlength1694za2zzcfa_tvectorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_tvectorzd2optimiza7ationzf3z86zzcfa_tvectorz00(void);
	static BgL_typez00_bglt
		BGl_z62getzd2vectorzd2itemzd2type1642zb0zzcfa_tvectorz00(obj_t, obj_t);
	extern obj_t BGl_kwotezf2nodezf2zzcfa_infoz00;
	static BgL_typez00_bglt
		BGl_z62getzd2vectorzd2itemzd2type1644zb0zzcfa_tvectorz00(obj_t, obj_t);
	extern obj_t BGl_lvtypezd2astz12zc0zzast_lvtypez00(obj_t);
	static obj_t BGl_z62addzd2makezd2vectorz12z70zzcfa_tvectorz00(obj_t, obj_t);
	static obj_t BGl_z62tvectorzd2optimiza7ationzf3ze4zzcfa_tvectorz00(obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62patchz12zd2sync1660za2zzcfa_tvectorz00(obj_t, obj_t);
	static obj_t BGl_z62patchz12zd2fail1674za2zzcfa_tvectorz00(obj_t, obj_t);
	static obj_t BGl_z62patchz12zd2sequence1658za2zzcfa_tvectorz00(obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static bool_t BGl_showzd2tvectorzd2zzcfa_tvectorz00(obj_t);
	extern obj_t BGl_za2optimza2z00zzengine_paramz00;
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_za2intza2z00zztype_cachez00;
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_tvectorz00 = BUNSPEC;
	extern obj_t BGl_za2vectorza2z00zztype_cachez00;
	extern obj_t BGl_vlengthz00zzast_nodez00;
	static obj_t BGl_z62patchz12zd2funcall1664za2zzcfa_tvectorz00(obj_t, obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static bool_t BGl_patchzd2treez12zc0zzcfa_tvectorz00(obj_t);
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_toplevelzd2initzd2zzcfa_tvectorz00(void);
	static obj_t BGl_z62patchz12zd2kwote1650za2zzcfa_tvectorz00(obj_t, obj_t);
	static obj_t BGl_z62patchzd2vectorzd2setz12z70zzcfa_tvectorz00(obj_t);
	static BgL_typez00_bglt
		BGl_z62getzd2vectorzd2itemzd2typezb0zzcfa_tvectorz00(obj_t, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nodez00_bglt);
	static obj_t BGl_genericzd2initzd2zzcfa_tvectorz00(void);
	BGL_IMPORT obj_t BGl_stringzd2copyzd2zz__r4_strings_6_7z00(obj_t);
	extern BgL_typez00_bglt BGl_getzd2defaultzd2typez00zztype_cachez00(void);
	static BgL_nodez00_bglt
		BGl_patchzd2vectorzd2ze3listz12zf1zzcfa_tvectorz00(BgL_appz00_bglt);
	extern obj_t BGl_za2boolza2z00zztype_cachez00;
	static obj_t BGl_z62patchz12zd2boxzd2ref1690z70zzcfa_tvectorz00(obj_t, obj_t);
	static obj_t BGl_getzd2tvectorszd2zzcfa_tvectorz00(void);
	BGL_EXPORTED_DECL BgL_typez00_bglt
		BGl_getzd2vectorzd2itemzd2typezd2zzcfa_tvectorz00(BgL_nodez00_bglt);
	extern obj_t BGl_castz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	extern obj_t BGl_typez00zztype_typez00;
	extern obj_t BGl_makezd2vectorzd2appz00zzcfa_info2z00;
	extern bool_t BGl_subzd2typezf3z21zztype_envz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_za2libzd2modeza2zd2zzengine_paramz00;
	extern obj_t BGl_vrefzf2Cinfozf2zzcfa_info3z00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	extern obj_t BGl_setzd2defaultzd2typez12z12zztype_cachez00(BgL_typez00_bglt);
	BGL_IMPORT obj_t string_append(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2atomz00zztype_typeofz00(obj_t);
	static obj_t BGl_za2makezd2vectorzd2listza2z00zzcfa_tvectorz00 = BUNSPEC;
	static obj_t BGl_z62patchz12zd2switch1676za2zzcfa_tvectorz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_addzd2makezd2vectorz12z12zzcfa_tvectorz00(BgL_nodez00_bglt);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzinline_inlinez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzglobaliza7e_walkza7(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_setz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_cfaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_walkz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_lvtypez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_sexpz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_buildz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_pragmaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzmodule_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_z62patchz12zd2jumpzd2exzd2it1684za2zzcfa_tvectorz00(obj_t,
		obj_t);
	static obj_t BGl_z62patchz12zd2boxzd2setz121688z62zzcfa_tvectorz00(obj_t,
		obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	static obj_t BGl_z62vectorzd2ze3tvectorz12z41zzcfa_tvectorz00(obj_t, obj_t);
	static obj_t BGl_z62patchz12zd2makezd2box1686z70zzcfa_tvectorz00(obj_t,
		obj_t);
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	extern obj_t BGl_tvectorzd2finaliza7erz75zzmodule_typez00(void);
	static obj_t BGl_cnstzd2initzd2zzcfa_tvectorz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_tvectorz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_tvectorz00(void);
	extern obj_t BGl_tvecz00zztvector_tvectorz00;
	static obj_t BGl_z62patchz12zd2letzd2fun1678z70zzcfa_tvectorz00(obj_t, obj_t);
	static obj_t BGl_z62patchz12zd2cast1668za2zzcfa_tvectorz00(obj_t, obj_t);
	extern BgL_nodez00_bglt BGl_sexpzd2ze3nodez31zzast_sexpz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62patchz12zd2valloczf2Cinfozb21698ze2zzcfa_tvectorz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_patchzd2vectorzd2setz12z12zzcfa_tvectorz00(void);
	static obj_t BGl_declarezd2tvectorszd2zzcfa_tvectorz00(obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_z62patchz12zd2setq1670za2zzcfa_tvectorz00(obj_t, obj_t);
	static obj_t BGl_z62patchz12zd2app1692za2zzcfa_tvectorz00(obj_t, obj_t);
	extern obj_t BGl_modulezd2tvectorzd2clausez00zzmodule_typez00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_literalz00zzast_nodez00;
	extern obj_t BGl_switchz00zzast_nodez00;
	extern BgL_approxz00_bglt BGl_cfaz12z12zzcfa_cfaz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t BGl_z62patchz12zd2setzd2exzd2it1682za2zzcfa_tvectorz00(obj_t,
		obj_t);
	extern obj_t BGl_globaliza7ezd2walkz12z67zzglobaliza7e_walkza7(obj_t, obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00;
	static obj_t BGl_z62patchz12z70zzcfa_tvectorz00(obj_t, obj_t);
	static obj_t __cnst[23];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_patchzd2vectorzd2setz12zd2envzc0zzcfa_tvectorz00,
		BgL_bgl_za762patchza7d2vecto2400z00,
		BGl_z62patchzd2vectorzd2setz12z70zzcfa_tvectorz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_vectorzd2ze3tvectorz12zd2envzf1zzcfa_tvectorz00,
		BgL_bgl_za762vectorza7d2za7e3t2401za7,
		BGl_z62vectorzd2ze3tvectorz12z41zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string2356z00zzcfa_tvectorz00,
		BgL_bgl_string2356za700za7za7c2402za7, "   . Vector -> Tvector", 22);
	      DEFINE_STRING(BGl_string2357z00zzcfa_tvectorz00,
		BgL_bgl_string2357za700za7za7c2403za7, " -> vector of ", 14);
	      DEFINE_STRING(BGl_string2358z00zzcfa_tvectorz00,
		BgL_bgl_string2358za700za7za7c2404za7, "        vector of ", 18);
	      DEFINE_STRING(BGl_string2360z00zzcfa_tvectorz00,
		BgL_bgl_string2360za700za7za7c2405za7, "get-vector-item-type1638", 24);
	      DEFINE_STRING(BGl_string2362z00zzcfa_tvectorz00,
		BgL_bgl_string2362za700za7za7c2406za7, "patch!1645", 10);
	      DEFINE_STRING(BGl_string2363z00zzcfa_tvectorz00,
		BgL_bgl_string2363za700za7za7c2407za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2365z00zzcfa_tvectorz00,
		BgL_bgl_string2365za700za7za7c2408za7, "get-vector-item-type", 20);
	      DEFINE_STRING(BGl_string2368z00zzcfa_tvectorz00,
		BgL_bgl_string2368za700za7za7c2409za7, "patch!", 6);
	      DEFINE_STRING(BGl_string2396z00zzcfa_tvectorz00,
		BgL_bgl_string2396za700za7za7c2410za7, "Unexpected closure", 18);
	      DEFINE_STRING(BGl_string2397z00zzcfa_tvectorz00,
		BgL_bgl_string2397za700za7za7c2411za7, "cfa_tvector", 11);
	      DEFINE_STRING(BGl_string2398z00zzcfa_tvectorz00,
		BgL_bgl_string2398za700za7za7c2412za7,
		"a-tvector __r4_vectors_6_8 vector->list -length make- allocate- -ref -set! get-vector-item-type1638 patch!1645 value ->list done no-remove unit cfa tv-of- all (vector-set! c-vector-set! vector-set-ur!) vector? $vector? c-vector? (vector-set! vector-set-ur! c-vector-set! $vector-set! $vector-set-ur!) ",
		301);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_tvectorzd2optimiza7ationzf3zd2envz54zzcfa_tvectorz00,
		BgL_bgl_za762tvectorza7d2opt2413z00,
		BGl_z62tvectorzd2optimiza7ationzf3ze4zzcfa_tvectorz00, 0L, BUNSPEC, 0);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_addzd2makezd2vectorz12zd2envzc0zzcfa_tvectorz00,
		BgL_bgl_za762addza7d2makeza7d22414za7,
		BGl_z62addzd2makezd2vectorz12z70zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_unpatchzd2vectorzd2setz12zd2envzc0zzcfa_tvectorz00,
		BgL_bgl_za762unpatchza7d2vec2415z00,
		BGl_z62unpatchzd2vectorzd2setz12z70zzcfa_tvectorz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2359z00zzcfa_tvectorz00,
		BgL_bgl_za762getza7d2vectorza72416za7,
		BGl_z62getzd2vectorzd2itemzd2type1638zb0zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2361z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza7121645za72417za7,
		BGl_z62patchz121645z70zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2364z00zzcfa_tvectorz00,
		BgL_bgl_za762getza7d2vectorza72418za7,
		BGl_z62getzd2vectorzd2itemzd2type1642zb0zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2366z00zzcfa_tvectorz00,
		BgL_bgl_za762getza7d2vectorza72419za7,
		BGl_z62getzd2vectorzd2itemzd2type1644zb0zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2367z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2at2420za7,
		BGl_z62patchz12zd2atom1648za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2369z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2kw2421za7,
		BGl_z62patchz12zd2kwote1650za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2370z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2kw2422za7,
		BGl_z62patchz12zd2kwotezf2node1652z50zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2371z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2va2423za7,
		BGl_z62patchz12zd2var1654za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2372z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2cl2424za7,
		BGl_z62patchz12zd2closure1656za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2373z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2se2425za7,
		BGl_z62patchz12zd2sequence1658za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2374z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2sy2426za7,
		BGl_z62patchz12zd2sync1660za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2375z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2ap2427za7,
		BGl_z62patchz12zd2appzd2ly1662z70zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2376z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2fu2428za7,
		BGl_z62patchz12zd2funcall1664za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2377z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2ex2429za7,
		BGl_z62patchz12zd2extern1666za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2378z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2ca2430za7,
		BGl_z62patchz12zd2cast1668za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2379z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2se2431za7,
		BGl_z62patchz12zd2setq1670za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2380z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2co2432za7,
		BGl_z62patchz12zd2conditional1672za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2381z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2fa2433za7,
		BGl_z62patchz12zd2fail1674za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2382z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2sw2434za7,
		BGl_z62patchz12zd2switch1676za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2383z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2le2435za7,
		BGl_z62patchz12zd2letzd2fun1678z70zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2384z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2le2436za7,
		BGl_z62patchz12zd2letzd2var1680z70zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2385z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2se2437za7,
		BGl_z62patchz12zd2setzd2exzd2it1682za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2386z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2ju2438za7,
		BGl_z62patchz12zd2jumpzd2exzd2it1684za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2387z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2ma2439za7,
		BGl_z62patchz12zd2makezd2box1686z70zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2388z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2bo2440za7,
		BGl_z62patchz12zd2boxzd2setz121688z62zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2389z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2bo2441za7,
		BGl_z62patchz12zd2boxzd2ref1690z70zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2390z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2ap2442za7,
		BGl_z62patchz12zd2app1692za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2391z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2vl2443za7,
		BGl_z62patchz12zd2vlength1694za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2392z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2ma2444za7,
		BGl_z62patchz12zd2makezd2vectorzd2a1696za2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2393z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2va2445za7,
		BGl_z62patchz12zd2valloczf2Cinfozb21698ze2zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2394z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2vr2446za7,
		BGl_z62patchz12zd2vrefzf2Cinfo1700z50zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2395z00zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za7d2vs2447za7,
		BGl_z62patchz12zd2vsetz12zf2Cinfo1702z42zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_GENERIC
		(BGl_getzd2vectorzd2itemzd2typezd2envz00zzcfa_tvectorz00,
		BgL_bgl_za762getza7d2vectorza72448za7,
		BGl_z62getzd2vectorzd2itemzd2typezb0zzcfa_tvectorz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_patchz12zd2envzc0zzcfa_tvectorz00,
		BgL_bgl_za762patchza712za770za7za72449za7,
		BGl_z62patchz12z70zzcfa_tvectorz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_tvectorz00));
		   
			 ADD_ROOT((void *) (&BGl_za2makezd2vectorzd2listza2z00zzcfa_tvectorz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(long
		BgL_checksumz00_6542, char *BgL_fromz00_6543)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_tvectorz00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_tvectorz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_tvectorz00();
					BGl_libraryzd2moduleszd2initz00zzcfa_tvectorz00();
					BGl_cnstzd2initzd2zzcfa_tvectorz00();
					BGl_importedzd2moduleszd2initz00zzcfa_tvectorz00();
					BGl_genericzd2initzd2zzcfa_tvectorz00();
					BGl_methodzd2initzd2zzcfa_tvectorz00();
					return BGl_toplevelzd2initzd2zzcfa_tvectorz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_tvectorz00(void)
	{
		{	/* Cfa/tvector.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_tvector");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cfa_tvector");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_tvector");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_tvector");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_tvector");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_tvector");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cfa_tvector");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "cfa_tvector");
			BGl_modulezd2initializa7ationz75zz__r5_control_features_6_4z00(0L,
				"cfa_tvector");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cfa_tvector");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_tvector");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_tvector");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "cfa_tvector");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cfa_tvector");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_tvectorz00(void)
	{
		{	/* Cfa/tvector.scm 15 */
			{	/* Cfa/tvector.scm 15 */
				obj_t BgL_cportz00_6207;

				{	/* Cfa/tvector.scm 15 */
					obj_t BgL_stringz00_6214;

					BgL_stringz00_6214 = BGl_string2398z00zzcfa_tvectorz00;
					{	/* Cfa/tvector.scm 15 */
						obj_t BgL_startz00_6215;

						BgL_startz00_6215 = BINT(0L);
						{	/* Cfa/tvector.scm 15 */
							obj_t BgL_endz00_6216;

							BgL_endz00_6216 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_6214)));
							{	/* Cfa/tvector.scm 15 */

								BgL_cportz00_6207 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_6214, BgL_startz00_6215, BgL_endz00_6216);
				}}}}
				{
					long BgL_iz00_6208;

					BgL_iz00_6208 = 22L;
				BgL_loopz00_6209:
					if ((BgL_iz00_6208 == -1L))
						{	/* Cfa/tvector.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/tvector.scm 15 */
							{	/* Cfa/tvector.scm 15 */
								obj_t BgL_arg2399z00_6210;

								{	/* Cfa/tvector.scm 15 */

									{	/* Cfa/tvector.scm 15 */
										obj_t BgL_locationz00_6212;

										BgL_locationz00_6212 = BBOOL(((bool_t) 0));
										{	/* Cfa/tvector.scm 15 */

											BgL_arg2399z00_6210 =
												BGl_readz00zz__readerz00(BgL_cportz00_6207,
												BgL_locationz00_6212);
										}
									}
								}
								{	/* Cfa/tvector.scm 15 */
									int BgL_tmpz00_6577;

									BgL_tmpz00_6577 = (int) (BgL_iz00_6208);
									CNST_TABLE_SET(BgL_tmpz00_6577, BgL_arg2399z00_6210);
							}}
							{	/* Cfa/tvector.scm 15 */
								int BgL_auxz00_6213;

								BgL_auxz00_6213 = (int) ((BgL_iz00_6208 - 1L));
								{
									long BgL_iz00_6582;

									BgL_iz00_6582 = (long) (BgL_auxz00_6213);
									BgL_iz00_6208 = BgL_iz00_6582;
									goto BgL_loopz00_6209;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_tvectorz00(void)
	{
		{	/* Cfa/tvector.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_tvectorz00(void)
	{
		{	/* Cfa/tvector.scm 15 */
			BGl_za2makezd2vectorzd2listza2z00zzcfa_tvectorz00 = BNIL;
			return BUNSPEC;
		}

	}



/* tvector-optimization? */
	BGL_EXPORTED_DEF obj_t
		BGl_tvectorzd2optimiza7ationzf3z86zzcfa_tvectorz00(void)
	{
		{	/* Cfa/tvector.scm 63 */
			if (((long) CINT(BGl_za2optimza2z00zzengine_paramz00) >= 3L))
				{	/* Cfa/tvector.scm 64 */
					if (CBOOL(BGl_za2libzd2modeza2zd2zzengine_paramz00))
						{	/* Cfa/tvector.scm 64 */
							return BFALSE;
						}
					else
						{	/* Cfa/tvector.scm 64 */
							return BTRUE;
						}
				}
			else
				{	/* Cfa/tvector.scm 64 */
					return BFALSE;
				}
		}

	}



/* &tvector-optimization? */
	obj_t BGl_z62tvectorzd2optimiza7ationzf3ze4zzcfa_tvectorz00(obj_t
		BgL_envz00_6096)
	{
		{	/* Cfa/tvector.scm 63 */
			return BGl_tvectorzd2optimiza7ationzf3z86zzcfa_tvectorz00();
		}

	}



/* patch-vector-set! */
	BGL_EXPORTED_DEF obj_t BGl_patchzd2vectorzd2setz12z12zzcfa_tvectorz00(void)
	{
		{	/* Cfa/tvector.scm 72 */
			if (CBOOL(BGl_tvectorzd2optimiza7ationzf3z86zzcfa_tvectorz00()))
				{	/* Cfa/tvector.scm 73 */
					{	/* Cfa/tvector.scm 74 */
						obj_t BgL_g1615z00_3696;

						BgL_g1615z00_3696 = CNST_TABLE_REF(0);
						{
							obj_t BgL_l1613z00_3698;

							BgL_l1613z00_3698 = BgL_g1615z00_3696;
						BgL_zc3z04anonymousza31801ze3z87_3699:
							if (PAIRP(BgL_l1613z00_3698))
								{	/* Cfa/tvector.scm 85 */
									{	/* Cfa/tvector.scm 75 */
										obj_t BgL_setz00_3701;

										BgL_setz00_3701 = CAR(BgL_l1613z00_3698);
										{	/* Cfa/tvector.scm 75 */
											obj_t BgL_gz00_3702;

											BgL_gz00_3702 =
												BGl_findzd2globalzd2zzast_envz00(BgL_setz00_3701, BNIL);
											{	/* Cfa/tvector.scm 76 */
												bool_t BgL_test2456z00_6599;

												{	/* Cfa/tvector.scm 76 */
													obj_t BgL_classz00_4845;

													BgL_classz00_4845 = BGl_globalz00zzast_varz00;
													if (BGL_OBJECTP(BgL_gz00_3702))
														{	/* Cfa/tvector.scm 76 */
															BgL_objectz00_bglt BgL_arg1807z00_4847;

															BgL_arg1807z00_4847 =
																(BgL_objectz00_bglt) (BgL_gz00_3702);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Cfa/tvector.scm 76 */
																	long BgL_idxz00_4853;

																	BgL_idxz00_4853 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_4847);
																	BgL_test2456z00_6599 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_4853 + 2L)) ==
																		BgL_classz00_4845);
																}
															else
																{	/* Cfa/tvector.scm 76 */
																	bool_t BgL_res2316z00_4878;

																	{	/* Cfa/tvector.scm 76 */
																		obj_t BgL_oclassz00_4861;

																		{	/* Cfa/tvector.scm 76 */
																			obj_t BgL_arg1815z00_4869;
																			long BgL_arg1816z00_4870;

																			BgL_arg1815z00_4869 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Cfa/tvector.scm 76 */
																				long BgL_arg1817z00_4871;

																				BgL_arg1817z00_4871 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_4847);
																				BgL_arg1816z00_4870 =
																					(BgL_arg1817z00_4871 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_4861 =
																				VECTOR_REF(BgL_arg1815z00_4869,
																				BgL_arg1816z00_4870);
																		}
																		{	/* Cfa/tvector.scm 76 */
																			bool_t BgL__ortest_1115z00_4862;

																			BgL__ortest_1115z00_4862 =
																				(BgL_classz00_4845 ==
																				BgL_oclassz00_4861);
																			if (BgL__ortest_1115z00_4862)
																				{	/* Cfa/tvector.scm 76 */
																					BgL_res2316z00_4878 =
																						BgL__ortest_1115z00_4862;
																				}
																			else
																				{	/* Cfa/tvector.scm 76 */
																					long BgL_odepthz00_4863;

																					{	/* Cfa/tvector.scm 76 */
																						obj_t BgL_arg1804z00_4864;

																						BgL_arg1804z00_4864 =
																							(BgL_oclassz00_4861);
																						BgL_odepthz00_4863 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_4864);
																					}
																					if ((2L < BgL_odepthz00_4863))
																						{	/* Cfa/tvector.scm 76 */
																							obj_t BgL_arg1802z00_4866;

																							{	/* Cfa/tvector.scm 76 */
																								obj_t BgL_arg1803z00_4867;

																								BgL_arg1803z00_4867 =
																									(BgL_oclassz00_4861);
																								BgL_arg1802z00_4866 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_4867, 2L);
																							}
																							BgL_res2316z00_4878 =
																								(BgL_arg1802z00_4866 ==
																								BgL_classz00_4845);
																						}
																					else
																						{	/* Cfa/tvector.scm 76 */
																							BgL_res2316z00_4878 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2456z00_6599 = BgL_res2316z00_4878;
																}
														}
													else
														{	/* Cfa/tvector.scm 76 */
															BgL_test2456z00_6599 = ((bool_t) 0);
														}
												}
												if (BgL_test2456z00_6599)
													{	/* Cfa/tvector.scm 77 */
														BgL_valuez00_bglt BgL_funz00_3704;

														BgL_funz00_3704 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt) BgL_gz00_3702))))->
															BgL_valuez00);
														{	/* Cfa/tvector.scm 79 */
															bool_t BgL_test2461z00_6625;

															{	/* Cfa/tvector.scm 79 */
																obj_t BgL_classz00_4880;

																BgL_classz00_4880 = BGl_cfunz00zzast_varz00;
																{	/* Cfa/tvector.scm 79 */
																	BgL_objectz00_bglt BgL_arg1807z00_4882;

																	{	/* Cfa/tvector.scm 79 */
																		obj_t BgL_tmpz00_6626;

																		BgL_tmpz00_6626 =
																			((obj_t)
																			((BgL_objectz00_bglt) BgL_funz00_3704));
																		BgL_arg1807z00_4882 =
																			(BgL_objectz00_bglt) (BgL_tmpz00_6626);
																	}
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Cfa/tvector.scm 79 */
																			long BgL_idxz00_4888;

																			BgL_idxz00_4888 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_4882);
																			BgL_test2461z00_6625 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_4888 + 3L)) ==
																				BgL_classz00_4880);
																		}
																	else
																		{	/* Cfa/tvector.scm 79 */
																			bool_t BgL_res2317z00_4913;

																			{	/* Cfa/tvector.scm 79 */
																				obj_t BgL_oclassz00_4896;

																				{	/* Cfa/tvector.scm 79 */
																					obj_t BgL_arg1815z00_4904;
																					long BgL_arg1816z00_4905;

																					BgL_arg1815z00_4904 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Cfa/tvector.scm 79 */
																						long BgL_arg1817z00_4906;

																						BgL_arg1817z00_4906 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_4882);
																						BgL_arg1816z00_4905 =
																							(BgL_arg1817z00_4906 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_4896 =
																						VECTOR_REF(BgL_arg1815z00_4904,
																						BgL_arg1816z00_4905);
																				}
																				{	/* Cfa/tvector.scm 79 */
																					bool_t BgL__ortest_1115z00_4897;

																					BgL__ortest_1115z00_4897 =
																						(BgL_classz00_4880 ==
																						BgL_oclassz00_4896);
																					if (BgL__ortest_1115z00_4897)
																						{	/* Cfa/tvector.scm 79 */
																							BgL_res2317z00_4913 =
																								BgL__ortest_1115z00_4897;
																						}
																					else
																						{	/* Cfa/tvector.scm 79 */
																							long BgL_odepthz00_4898;

																							{	/* Cfa/tvector.scm 79 */
																								obj_t BgL_arg1804z00_4899;

																								BgL_arg1804z00_4899 =
																									(BgL_oclassz00_4896);
																								BgL_odepthz00_4898 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_4899);
																							}
																							if ((3L < BgL_odepthz00_4898))
																								{	/* Cfa/tvector.scm 79 */
																									obj_t BgL_arg1802z00_4901;

																									{	/* Cfa/tvector.scm 79 */
																										obj_t BgL_arg1803z00_4902;

																										BgL_arg1803z00_4902 =
																											(BgL_oclassz00_4896);
																										BgL_arg1802z00_4901 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_4902, 3L);
																									}
																									BgL_res2317z00_4913 =
																										(BgL_arg1802z00_4901 ==
																										BgL_classz00_4880);
																								}
																							else
																								{	/* Cfa/tvector.scm 79 */
																									BgL_res2317z00_4913 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2461z00_6625 =
																				BgL_res2317z00_4913;
																		}
																}
															}
															if (BgL_test2461z00_6625)
																{	/* Cfa/tvector.scm 80 */
																	obj_t BgL_arg1805z00_3706;
																	BgL_typez00_bglt BgL_arg1806z00_3707;

																	{	/* Cfa/tvector.scm 80 */
																		obj_t BgL_pairz00_4915;

																		BgL_pairz00_4915 =
																			(((BgL_cfunz00_bglt) COBJECT(
																					((BgL_cfunz00_bglt)
																						BgL_funz00_3704)))->
																			BgL_argszd2typezd2);
																		BgL_arg1805z00_3706 =
																			CDR(CDR(BgL_pairz00_4915));
																	}
																	BgL_arg1806z00_3707 =
																		BGl_getzd2defaultzd2typez00zztype_cachez00
																		();
																	{	/* Cfa/tvector.scm 80 */
																		obj_t BgL_auxz00_6656;
																		obj_t BgL_tmpz00_6654;

																		BgL_auxz00_6656 =
																			((obj_t) BgL_arg1806z00_3707);
																		BgL_tmpz00_6654 =
																			((obj_t) BgL_arg1805z00_3706);
																		SET_CAR(BgL_tmpz00_6654, BgL_auxz00_6656);
																	}
																}
															else
																{	/* Cfa/tvector.scm 82 */
																	bool_t BgL_test2465z00_6659;

																	{	/* Cfa/tvector.scm 82 */
																		obj_t BgL_classz00_4920;

																		BgL_classz00_4920 = BGl_sfunz00zzast_varz00;
																		{	/* Cfa/tvector.scm 82 */
																			BgL_objectz00_bglt BgL_arg1807z00_4922;

																			{	/* Cfa/tvector.scm 82 */
																				obj_t BgL_tmpz00_6660;

																				BgL_tmpz00_6660 =
																					((obj_t)
																					((BgL_objectz00_bglt)
																						BgL_funz00_3704));
																				BgL_arg1807z00_4922 =
																					(BgL_objectz00_bglt)
																					(BgL_tmpz00_6660);
																			}
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Cfa/tvector.scm 82 */
																					long BgL_idxz00_4928;

																					BgL_idxz00_4928 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_4922);
																					BgL_test2465z00_6659 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_4928 + 3L)) ==
																						BgL_classz00_4920);
																				}
																			else
																				{	/* Cfa/tvector.scm 82 */
																					bool_t BgL_res2318z00_4953;

																					{	/* Cfa/tvector.scm 82 */
																						obj_t BgL_oclassz00_4936;

																						{	/* Cfa/tvector.scm 82 */
																							obj_t BgL_arg1815z00_4944;
																							long BgL_arg1816z00_4945;

																							BgL_arg1815z00_4944 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Cfa/tvector.scm 82 */
																								long BgL_arg1817z00_4946;

																								BgL_arg1817z00_4946 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_4922);
																								BgL_arg1816z00_4945 =
																									(BgL_arg1817z00_4946 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_4936 =
																								VECTOR_REF(BgL_arg1815z00_4944,
																								BgL_arg1816z00_4945);
																						}
																						{	/* Cfa/tvector.scm 82 */
																							bool_t BgL__ortest_1115z00_4937;

																							BgL__ortest_1115z00_4937 =
																								(BgL_classz00_4920 ==
																								BgL_oclassz00_4936);
																							if (BgL__ortest_1115z00_4937)
																								{	/* Cfa/tvector.scm 82 */
																									BgL_res2318z00_4953 =
																										BgL__ortest_1115z00_4937;
																								}
																							else
																								{	/* Cfa/tvector.scm 82 */
																									long BgL_odepthz00_4938;

																									{	/* Cfa/tvector.scm 82 */
																										obj_t BgL_arg1804z00_4939;

																										BgL_arg1804z00_4939 =
																											(BgL_oclassz00_4936);
																										BgL_odepthz00_4938 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_4939);
																									}
																									if ((3L < BgL_odepthz00_4938))
																										{	/* Cfa/tvector.scm 82 */
																											obj_t BgL_arg1802z00_4941;

																											{	/* Cfa/tvector.scm 82 */
																												obj_t
																													BgL_arg1803z00_4942;
																												BgL_arg1803z00_4942 =
																													(BgL_oclassz00_4936);
																												BgL_arg1802z00_4941 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_4942,
																													3L);
																											}
																											BgL_res2318z00_4953 =
																												(BgL_arg1802z00_4941 ==
																												BgL_classz00_4920);
																										}
																									else
																										{	/* Cfa/tvector.scm 82 */
																											BgL_res2318z00_4953 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2465z00_6659 =
																						BgL_res2318z00_4953;
																				}
																		}
																	}
																	if (BgL_test2465z00_6659)
																		{	/* Cfa/tvector.scm 83 */
																			obj_t BgL_arg1812z00_3710;
																			BgL_typez00_bglt BgL_arg1820z00_3711;

																			{	/* Cfa/tvector.scm 83 */
																				obj_t BgL_pairz00_4955;

																				BgL_pairz00_4955 =
																					(((BgL_sfunz00_bglt) COBJECT(
																							((BgL_sfunz00_bglt)
																								BgL_funz00_3704)))->
																					BgL_argsz00);
																				BgL_arg1812z00_3710 =
																					CAR(CDR(CDR(BgL_pairz00_4955)));
																			}
																			BgL_arg1820z00_3711 =
																				BGl_getzd2defaultzd2typez00zztype_cachez00
																				();
																			((((BgL_variablez00_bglt)
																						COBJECT(((BgL_variablez00_bglt) (
																									(BgL_localz00_bglt)
																									BgL_arg1812z00_3710))))->
																					BgL_typez00) =
																				((BgL_typez00_bglt)
																					BgL_arg1820z00_3711), BUNSPEC);
																		}
																	else
																		{	/* Cfa/tvector.scm 82 */
																			BFALSE;
																		}
																}
														}
													}
												else
													{	/* Cfa/tvector.scm 76 */
														BFALSE;
													}
											}
										}
									}
									{
										obj_t BgL_l1613z00_6692;

										BgL_l1613z00_6692 = CDR(BgL_l1613z00_3698);
										BgL_l1613z00_3698 = BgL_l1613z00_6692;
										goto BgL_zc3z04anonymousza31801ze3z87_3699;
									}
								}
							else
								{	/* Cfa/tvector.scm 85 */
									((bool_t) 1);
								}
						}
					}
					{	/* Cfa/tvector.scm 87 */
						obj_t BgL_gz00_3716;

						BgL_gz00_3716 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(1), BNIL);
						{	/* Cfa/tvector.scm 88 */
							bool_t BgL_test2469z00_6696;

							{	/* Cfa/tvector.scm 88 */
								obj_t BgL_classz00_4964;

								BgL_classz00_4964 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_gz00_3716))
									{	/* Cfa/tvector.scm 88 */
										BgL_objectz00_bglt BgL_arg1807z00_4966;

										BgL_arg1807z00_4966 = (BgL_objectz00_bglt) (BgL_gz00_3716);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/tvector.scm 88 */
												long BgL_idxz00_4972;

												BgL_idxz00_4972 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4966);
												BgL_test2469z00_6696 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_4972 + 2L)) == BgL_classz00_4964);
											}
										else
											{	/* Cfa/tvector.scm 88 */
												bool_t BgL_res2319z00_4997;

												{	/* Cfa/tvector.scm 88 */
													obj_t BgL_oclassz00_4980;

													{	/* Cfa/tvector.scm 88 */
														obj_t BgL_arg1815z00_4988;
														long BgL_arg1816z00_4989;

														BgL_arg1815z00_4988 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/tvector.scm 88 */
															long BgL_arg1817z00_4990;

															BgL_arg1817z00_4990 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4966);
															BgL_arg1816z00_4989 =
																(BgL_arg1817z00_4990 - OBJECT_TYPE);
														}
														BgL_oclassz00_4980 =
															VECTOR_REF(BgL_arg1815z00_4988,
															BgL_arg1816z00_4989);
													}
													{	/* Cfa/tvector.scm 88 */
														bool_t BgL__ortest_1115z00_4981;

														BgL__ortest_1115z00_4981 =
															(BgL_classz00_4964 == BgL_oclassz00_4980);
														if (BgL__ortest_1115z00_4981)
															{	/* Cfa/tvector.scm 88 */
																BgL_res2319z00_4997 = BgL__ortest_1115z00_4981;
															}
														else
															{	/* Cfa/tvector.scm 88 */
																long BgL_odepthz00_4982;

																{	/* Cfa/tvector.scm 88 */
																	obj_t BgL_arg1804z00_4983;

																	BgL_arg1804z00_4983 = (BgL_oclassz00_4980);
																	BgL_odepthz00_4982 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_4983);
																}
																if ((2L < BgL_odepthz00_4982))
																	{	/* Cfa/tvector.scm 88 */
																		obj_t BgL_arg1802z00_4985;

																		{	/* Cfa/tvector.scm 88 */
																			obj_t BgL_arg1803z00_4986;

																			BgL_arg1803z00_4986 =
																				(BgL_oclassz00_4980);
																			BgL_arg1802z00_4985 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_4986, 2L);
																		}
																		BgL_res2319z00_4997 =
																			(BgL_arg1802z00_4985 ==
																			BgL_classz00_4964);
																	}
																else
																	{	/* Cfa/tvector.scm 88 */
																		BgL_res2319z00_4997 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2469z00_6696 = BgL_res2319z00_4997;
											}
									}
								else
									{	/* Cfa/tvector.scm 88 */
										BgL_test2469z00_6696 = ((bool_t) 0);
									}
							}
							if (BgL_test2469z00_6696)
								{	/* Cfa/tvector.scm 89 */
									BgL_valuez00_bglt BgL_fz00_3718;

									BgL_fz00_3718 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_gz00_3716))))->
										BgL_valuez00);
									{	/* Cfa/tvector.scm 90 */
										obj_t BgL_arg1833z00_3719;
										BgL_typez00_bglt BgL_arg1834z00_3720;

										BgL_arg1833z00_3719 =
											(((BgL_cfunz00_bglt) COBJECT(
													((BgL_cfunz00_bglt) BgL_fz00_3718)))->
											BgL_argszd2typezd2);
										BgL_arg1834z00_3720 =
											BGl_getzd2defaultzd2typez00zztype_cachez00();
										{	/* Cfa/tvector.scm 90 */
											obj_t BgL_auxz00_6727;
											obj_t BgL_tmpz00_6725;

											BgL_auxz00_6727 = ((obj_t) BgL_arg1834z00_3720);
											BgL_tmpz00_6725 = ((obj_t) BgL_arg1833z00_3719);
											SET_CAR(BgL_tmpz00_6725, BgL_auxz00_6727);
										}
									}
								}
							else
								{	/* Cfa/tvector.scm 88 */
									BFALSE;
								}
						}
					}
					{	/* Cfa/tvector.scm 91 */
						obj_t BgL_gz00_3722;

						BgL_gz00_3722 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(2), BNIL);
						{	/* Cfa/tvector.scm 92 */
							bool_t BgL_test2474z00_6732;

							{	/* Cfa/tvector.scm 92 */
								obj_t BgL_classz00_5001;

								BgL_classz00_5001 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_gz00_3722))
									{	/* Cfa/tvector.scm 92 */
										BgL_objectz00_bglt BgL_arg1807z00_5003;

										BgL_arg1807z00_5003 = (BgL_objectz00_bglt) (BgL_gz00_3722);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/tvector.scm 92 */
												long BgL_idxz00_5009;

												BgL_idxz00_5009 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5003);
												BgL_test2474z00_6732 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5009 + 2L)) == BgL_classz00_5001);
											}
										else
											{	/* Cfa/tvector.scm 92 */
												bool_t BgL_res2320z00_5034;

												{	/* Cfa/tvector.scm 92 */
													obj_t BgL_oclassz00_5017;

													{	/* Cfa/tvector.scm 92 */
														obj_t BgL_arg1815z00_5025;
														long BgL_arg1816z00_5026;

														BgL_arg1815z00_5025 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/tvector.scm 92 */
															long BgL_arg1817z00_5027;

															BgL_arg1817z00_5027 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5003);
															BgL_arg1816z00_5026 =
																(BgL_arg1817z00_5027 - OBJECT_TYPE);
														}
														BgL_oclassz00_5017 =
															VECTOR_REF(BgL_arg1815z00_5025,
															BgL_arg1816z00_5026);
													}
													{	/* Cfa/tvector.scm 92 */
														bool_t BgL__ortest_1115z00_5018;

														BgL__ortest_1115z00_5018 =
															(BgL_classz00_5001 == BgL_oclassz00_5017);
														if (BgL__ortest_1115z00_5018)
															{	/* Cfa/tvector.scm 92 */
																BgL_res2320z00_5034 = BgL__ortest_1115z00_5018;
															}
														else
															{	/* Cfa/tvector.scm 92 */
																long BgL_odepthz00_5019;

																{	/* Cfa/tvector.scm 92 */
																	obj_t BgL_arg1804z00_5020;

																	BgL_arg1804z00_5020 = (BgL_oclassz00_5017);
																	BgL_odepthz00_5019 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_5020);
																}
																if ((2L < BgL_odepthz00_5019))
																	{	/* Cfa/tvector.scm 92 */
																		obj_t BgL_arg1802z00_5022;

																		{	/* Cfa/tvector.scm 92 */
																			obj_t BgL_arg1803z00_5023;

																			BgL_arg1803z00_5023 =
																				(BgL_oclassz00_5017);
																			BgL_arg1802z00_5022 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_5023, 2L);
																		}
																		BgL_res2320z00_5034 =
																			(BgL_arg1802z00_5022 ==
																			BgL_classz00_5001);
																	}
																else
																	{	/* Cfa/tvector.scm 92 */
																		BgL_res2320z00_5034 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2474z00_6732 = BgL_res2320z00_5034;
											}
									}
								else
									{	/* Cfa/tvector.scm 92 */
										BgL_test2474z00_6732 = ((bool_t) 0);
									}
							}
							if (BgL_test2474z00_6732)
								{	/* Cfa/tvector.scm 93 */
									BgL_valuez00_bglt BgL_fz00_3724;

									BgL_fz00_3724 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_gz00_3722))))->
										BgL_valuez00);
									{	/* Cfa/tvector.scm 94 */
										obj_t BgL_arg1837z00_3725;
										BgL_typez00_bglt BgL_arg1838z00_3726;

										BgL_arg1837z00_3725 =
											(((BgL_cfunz00_bglt) COBJECT(
													((BgL_cfunz00_bglt) BgL_fz00_3724)))->
											BgL_argszd2typezd2);
										BgL_arg1838z00_3726 =
											BGl_getzd2defaultzd2typez00zztype_cachez00();
										{	/* Cfa/tvector.scm 94 */
											obj_t BgL_auxz00_6763;
											obj_t BgL_tmpz00_6761;

											BgL_auxz00_6763 = ((obj_t) BgL_arg1838z00_3726);
											BgL_tmpz00_6761 = ((obj_t) BgL_arg1837z00_3725);
											SET_CAR(BgL_tmpz00_6761, BgL_auxz00_6763);
										}
									}
								}
							else
								{	/* Cfa/tvector.scm 92 */
									BFALSE;
								}
						}
					}
					{	/* Cfa/tvector.scm 95 */
						obj_t BgL_gz00_3728;

						BgL_gz00_3728 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(3), BNIL);
						{	/* Cfa/tvector.scm 96 */
							bool_t BgL_test2479z00_6768;

							{	/* Cfa/tvector.scm 96 */
								obj_t BgL_classz00_5038;

								BgL_classz00_5038 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_gz00_3728))
									{	/* Cfa/tvector.scm 96 */
										BgL_objectz00_bglt BgL_arg1807z00_5040;

										BgL_arg1807z00_5040 = (BgL_objectz00_bglt) (BgL_gz00_3728);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/tvector.scm 96 */
												long BgL_idxz00_5046;

												BgL_idxz00_5046 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5040);
												BgL_test2479z00_6768 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5046 + 2L)) == BgL_classz00_5038);
											}
										else
											{	/* Cfa/tvector.scm 96 */
												bool_t BgL_res2321z00_5071;

												{	/* Cfa/tvector.scm 96 */
													obj_t BgL_oclassz00_5054;

													{	/* Cfa/tvector.scm 96 */
														obj_t BgL_arg1815z00_5062;
														long BgL_arg1816z00_5063;

														BgL_arg1815z00_5062 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/tvector.scm 96 */
															long BgL_arg1817z00_5064;

															BgL_arg1817z00_5064 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5040);
															BgL_arg1816z00_5063 =
																(BgL_arg1817z00_5064 - OBJECT_TYPE);
														}
														BgL_oclassz00_5054 =
															VECTOR_REF(BgL_arg1815z00_5062,
															BgL_arg1816z00_5063);
													}
													{	/* Cfa/tvector.scm 96 */
														bool_t BgL__ortest_1115z00_5055;

														BgL__ortest_1115z00_5055 =
															(BgL_classz00_5038 == BgL_oclassz00_5054);
														if (BgL__ortest_1115z00_5055)
															{	/* Cfa/tvector.scm 96 */
																BgL_res2321z00_5071 = BgL__ortest_1115z00_5055;
															}
														else
															{	/* Cfa/tvector.scm 96 */
																long BgL_odepthz00_5056;

																{	/* Cfa/tvector.scm 96 */
																	obj_t BgL_arg1804z00_5057;

																	BgL_arg1804z00_5057 = (BgL_oclassz00_5054);
																	BgL_odepthz00_5056 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_5057);
																}
																if ((2L < BgL_odepthz00_5056))
																	{	/* Cfa/tvector.scm 96 */
																		obj_t BgL_arg1802z00_5059;

																		{	/* Cfa/tvector.scm 96 */
																			obj_t BgL_arg1803z00_5060;

																			BgL_arg1803z00_5060 =
																				(BgL_oclassz00_5054);
																			BgL_arg1802z00_5059 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_5060, 2L);
																		}
																		BgL_res2321z00_5071 =
																			(BgL_arg1802z00_5059 ==
																			BgL_classz00_5038);
																	}
																else
																	{	/* Cfa/tvector.scm 96 */
																		BgL_res2321z00_5071 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2479z00_6768 = BgL_res2321z00_5071;
											}
									}
								else
									{	/* Cfa/tvector.scm 96 */
										BgL_test2479z00_6768 = ((bool_t) 0);
									}
							}
							if (BgL_test2479z00_6768)
								{	/* Cfa/tvector.scm 97 */
									BgL_valuez00_bglt BgL_fz00_3730;

									BgL_fz00_3730 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_gz00_3728))))->
										BgL_valuez00);
									{	/* Cfa/tvector.scm 98 */
										obj_t BgL_arg1842z00_3731;
										BgL_typez00_bglt BgL_arg1843z00_3732;

										{	/* Cfa/tvector.scm 98 */
											obj_t BgL_pairz00_5074;

											BgL_pairz00_5074 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_fz00_3730)))->BgL_argsz00);
											BgL_arg1842z00_3731 = CAR(BgL_pairz00_5074);
										}
										BgL_arg1843z00_3732 =
											BGl_getzd2defaultzd2typez00zztype_cachez00();
										return
											((((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt) BgL_arg1842z00_3731))))->
												BgL_typez00) =
											((BgL_typez00_bglt) BgL_arg1843z00_3732), BUNSPEC);
									}
								}
							else
								{	/* Cfa/tvector.scm 96 */
									return BFALSE;
								}
						}
					}
				}
			else
				{	/* Cfa/tvector.scm 73 */
					return BFALSE;
				}
		}

	}



/* &patch-vector-set! */
	obj_t BGl_z62patchzd2vectorzd2setz12z70zzcfa_tvectorz00(obj_t BgL_envz00_6097)
	{
		{	/* Cfa/tvector.scm 72 */
			return BGl_patchzd2vectorzd2setz12z12zzcfa_tvectorz00();
		}

	}



/* unpatch-vector-set! */
	BGL_EXPORTED_DEF obj_t BGl_unpatchzd2vectorzd2setz12z12zzcfa_tvectorz00(void)
	{
		{	/* Cfa/tvector.scm 103 */
			if (CBOOL(BGl_tvectorzd2optimiza7ationzf3z86zzcfa_tvectorz00()))
				{	/* Cfa/tvector.scm 104 */
					{	/* Cfa/tvector.scm 105 */
						obj_t BgL_g1618z00_3736;

						BgL_g1618z00_3736 = CNST_TABLE_REF(4);
						{
							obj_t BgL_l1616z00_3738;

							BgL_l1616z00_3738 = BgL_g1618z00_3736;
						BgL_zc3z04anonymousza31847ze3z87_3739:
							if (PAIRP(BgL_l1616z00_3738))
								{	/* Cfa/tvector.scm 116 */
									{	/* Cfa/tvector.scm 106 */
										obj_t BgL_setz00_3741;

										BgL_setz00_3741 = CAR(BgL_l1616z00_3738);
										{	/* Cfa/tvector.scm 106 */
											obj_t BgL_gz00_3742;

											BgL_gz00_3742 =
												BGl_findzd2globalzd2zzast_envz00(BgL_setz00_3741, BNIL);
											{	/* Cfa/tvector.scm 107 */
												bool_t BgL_test2486z00_6810;

												{	/* Cfa/tvector.scm 107 */
													obj_t BgL_classz00_5078;

													BgL_classz00_5078 = BGl_globalz00zzast_varz00;
													if (BGL_OBJECTP(BgL_gz00_3742))
														{	/* Cfa/tvector.scm 107 */
															BgL_objectz00_bglt BgL_arg1807z00_5080;

															BgL_arg1807z00_5080 =
																(BgL_objectz00_bglt) (BgL_gz00_3742);
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Cfa/tvector.scm 107 */
																	long BgL_idxz00_5086;

																	BgL_idxz00_5086 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_5080);
																	BgL_test2486z00_6810 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_5086 + 2L)) ==
																		BgL_classz00_5078);
																}
															else
																{	/* Cfa/tvector.scm 107 */
																	bool_t BgL_res2322z00_5111;

																	{	/* Cfa/tvector.scm 107 */
																		obj_t BgL_oclassz00_5094;

																		{	/* Cfa/tvector.scm 107 */
																			obj_t BgL_arg1815z00_5102;
																			long BgL_arg1816z00_5103;

																			BgL_arg1815z00_5102 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Cfa/tvector.scm 107 */
																				long BgL_arg1817z00_5104;

																				BgL_arg1817z00_5104 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_5080);
																				BgL_arg1816z00_5103 =
																					(BgL_arg1817z00_5104 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_5094 =
																				VECTOR_REF(BgL_arg1815z00_5102,
																				BgL_arg1816z00_5103);
																		}
																		{	/* Cfa/tvector.scm 107 */
																			bool_t BgL__ortest_1115z00_5095;

																			BgL__ortest_1115z00_5095 =
																				(BgL_classz00_5078 ==
																				BgL_oclassz00_5094);
																			if (BgL__ortest_1115z00_5095)
																				{	/* Cfa/tvector.scm 107 */
																					BgL_res2322z00_5111 =
																						BgL__ortest_1115z00_5095;
																				}
																			else
																				{	/* Cfa/tvector.scm 107 */
																					long BgL_odepthz00_5096;

																					{	/* Cfa/tvector.scm 107 */
																						obj_t BgL_arg1804z00_5097;

																						BgL_arg1804z00_5097 =
																							(BgL_oclassz00_5094);
																						BgL_odepthz00_5096 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_5097);
																					}
																					if ((2L < BgL_odepthz00_5096))
																						{	/* Cfa/tvector.scm 107 */
																							obj_t BgL_arg1802z00_5099;

																							{	/* Cfa/tvector.scm 107 */
																								obj_t BgL_arg1803z00_5100;

																								BgL_arg1803z00_5100 =
																									(BgL_oclassz00_5094);
																								BgL_arg1802z00_5099 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_5100, 2L);
																							}
																							BgL_res2322z00_5111 =
																								(BgL_arg1802z00_5099 ==
																								BgL_classz00_5078);
																						}
																					else
																						{	/* Cfa/tvector.scm 107 */
																							BgL_res2322z00_5111 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2486z00_6810 = BgL_res2322z00_5111;
																}
														}
													else
														{	/* Cfa/tvector.scm 107 */
															BgL_test2486z00_6810 = ((bool_t) 0);
														}
												}
												if (BgL_test2486z00_6810)
													{	/* Cfa/tvector.scm 108 */
														BgL_valuez00_bglt BgL_funz00_3744;

														BgL_funz00_3744 =
															(((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		((BgL_globalz00_bglt) BgL_gz00_3742))))->
															BgL_valuez00);
														{	/* Cfa/tvector.scm 110 */
															bool_t BgL_test2491z00_6836;

															{	/* Cfa/tvector.scm 110 */
																obj_t BgL_classz00_5113;

																BgL_classz00_5113 = BGl_cfunz00zzast_varz00;
																{	/* Cfa/tvector.scm 110 */
																	BgL_objectz00_bglt BgL_arg1807z00_5115;

																	{	/* Cfa/tvector.scm 110 */
																		obj_t BgL_tmpz00_6837;

																		BgL_tmpz00_6837 =
																			((obj_t)
																			((BgL_objectz00_bglt) BgL_funz00_3744));
																		BgL_arg1807z00_5115 =
																			(BgL_objectz00_bglt) (BgL_tmpz00_6837);
																	}
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Cfa/tvector.scm 110 */
																			long BgL_idxz00_5121;

																			BgL_idxz00_5121 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_5115);
																			BgL_test2491z00_6836 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_5121 + 3L)) ==
																				BgL_classz00_5113);
																		}
																	else
																		{	/* Cfa/tvector.scm 110 */
																			bool_t BgL_res2323z00_5146;

																			{	/* Cfa/tvector.scm 110 */
																				obj_t BgL_oclassz00_5129;

																				{	/* Cfa/tvector.scm 110 */
																					obj_t BgL_arg1815z00_5137;
																					long BgL_arg1816z00_5138;

																					BgL_arg1815z00_5137 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Cfa/tvector.scm 110 */
																						long BgL_arg1817z00_5139;

																						BgL_arg1817z00_5139 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_5115);
																						BgL_arg1816z00_5138 =
																							(BgL_arg1817z00_5139 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_5129 =
																						VECTOR_REF(BgL_arg1815z00_5137,
																						BgL_arg1816z00_5138);
																				}
																				{	/* Cfa/tvector.scm 110 */
																					bool_t BgL__ortest_1115z00_5130;

																					BgL__ortest_1115z00_5130 =
																						(BgL_classz00_5113 ==
																						BgL_oclassz00_5129);
																					if (BgL__ortest_1115z00_5130)
																						{	/* Cfa/tvector.scm 110 */
																							BgL_res2323z00_5146 =
																								BgL__ortest_1115z00_5130;
																						}
																					else
																						{	/* Cfa/tvector.scm 110 */
																							long BgL_odepthz00_5131;

																							{	/* Cfa/tvector.scm 110 */
																								obj_t BgL_arg1804z00_5132;

																								BgL_arg1804z00_5132 =
																									(BgL_oclassz00_5129);
																								BgL_odepthz00_5131 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_5132);
																							}
																							if ((3L < BgL_odepthz00_5131))
																								{	/* Cfa/tvector.scm 110 */
																									obj_t BgL_arg1802z00_5134;

																									{	/* Cfa/tvector.scm 110 */
																										obj_t BgL_arg1803z00_5135;

																										BgL_arg1803z00_5135 =
																											(BgL_oclassz00_5129);
																										BgL_arg1802z00_5134 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_5135, 3L);
																									}
																									BgL_res2323z00_5146 =
																										(BgL_arg1802z00_5134 ==
																										BgL_classz00_5113);
																								}
																							else
																								{	/* Cfa/tvector.scm 110 */
																									BgL_res2323z00_5146 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2491z00_6836 =
																				BgL_res2323z00_5146;
																		}
																}
															}
															if (BgL_test2491z00_6836)
																{	/* Cfa/tvector.scm 111 */
																	obj_t BgL_arg1851z00_3746;

																	{	/* Cfa/tvector.scm 111 */
																		obj_t BgL_pairz00_5148;

																		BgL_pairz00_5148 =
																			(((BgL_cfunz00_bglt) COBJECT(
																					((BgL_cfunz00_bglt)
																						BgL_funz00_3744)))->
																			BgL_argszd2typezd2);
																		BgL_arg1851z00_3746 =
																			CDR(CDR(BgL_pairz00_5148));
																	}
																	{	/* Cfa/tvector.scm 111 */
																		obj_t BgL_objz00_5153;

																		BgL_objz00_5153 =
																			BGl_za2objza2z00zztype_cachez00;
																		{	/* Cfa/tvector.scm 111 */
																			obj_t BgL_tmpz00_6864;

																			BgL_tmpz00_6864 =
																				((obj_t) BgL_arg1851z00_3746);
																			SET_CAR(BgL_tmpz00_6864, BgL_objz00_5153);
																		}
																	}
																}
															else
																{	/* Cfa/tvector.scm 113 */
																	bool_t BgL_test2495z00_6867;

																	{	/* Cfa/tvector.scm 113 */
																		obj_t BgL_classz00_5154;

																		BgL_classz00_5154 = BGl_sfunz00zzast_varz00;
																		{	/* Cfa/tvector.scm 113 */
																			BgL_objectz00_bglt BgL_arg1807z00_5156;

																			{	/* Cfa/tvector.scm 113 */
																				obj_t BgL_tmpz00_6868;

																				BgL_tmpz00_6868 =
																					((obj_t)
																					((BgL_objectz00_bglt)
																						BgL_funz00_3744));
																				BgL_arg1807z00_5156 =
																					(BgL_objectz00_bglt)
																					(BgL_tmpz00_6868);
																			}
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Cfa/tvector.scm 113 */
																					long BgL_idxz00_5162;

																					BgL_idxz00_5162 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_5156);
																					BgL_test2495z00_6867 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_5162 + 3L)) ==
																						BgL_classz00_5154);
																				}
																			else
																				{	/* Cfa/tvector.scm 113 */
																					bool_t BgL_res2324z00_5187;

																					{	/* Cfa/tvector.scm 113 */
																						obj_t BgL_oclassz00_5170;

																						{	/* Cfa/tvector.scm 113 */
																							obj_t BgL_arg1815z00_5178;
																							long BgL_arg1816z00_5179;

																							BgL_arg1815z00_5178 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Cfa/tvector.scm 113 */
																								long BgL_arg1817z00_5180;

																								BgL_arg1817z00_5180 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_5156);
																								BgL_arg1816z00_5179 =
																									(BgL_arg1817z00_5180 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_5170 =
																								VECTOR_REF(BgL_arg1815z00_5178,
																								BgL_arg1816z00_5179);
																						}
																						{	/* Cfa/tvector.scm 113 */
																							bool_t BgL__ortest_1115z00_5171;

																							BgL__ortest_1115z00_5171 =
																								(BgL_classz00_5154 ==
																								BgL_oclassz00_5170);
																							if (BgL__ortest_1115z00_5171)
																								{	/* Cfa/tvector.scm 113 */
																									BgL_res2324z00_5187 =
																										BgL__ortest_1115z00_5171;
																								}
																							else
																								{	/* Cfa/tvector.scm 113 */
																									long BgL_odepthz00_5172;

																									{	/* Cfa/tvector.scm 113 */
																										obj_t BgL_arg1804z00_5173;

																										BgL_arg1804z00_5173 =
																											(BgL_oclassz00_5170);
																										BgL_odepthz00_5172 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_5173);
																									}
																									if ((3L < BgL_odepthz00_5172))
																										{	/* Cfa/tvector.scm 113 */
																											obj_t BgL_arg1802z00_5175;

																											{	/* Cfa/tvector.scm 113 */
																												obj_t
																													BgL_arg1803z00_5176;
																												BgL_arg1803z00_5176 =
																													(BgL_oclassz00_5170);
																												BgL_arg1802z00_5175 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_5176,
																													3L);
																											}
																											BgL_res2324z00_5187 =
																												(BgL_arg1802z00_5175 ==
																												BgL_classz00_5154);
																										}
																									else
																										{	/* Cfa/tvector.scm 113 */
																											BgL_res2324z00_5187 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2495z00_6867 =
																						BgL_res2324z00_5187;
																				}
																		}
																	}
																	if (BgL_test2495z00_6867)
																		{	/* Cfa/tvector.scm 114 */
																			obj_t BgL_arg1854z00_3749;

																			{	/* Cfa/tvector.scm 114 */
																				obj_t BgL_pairz00_5189;

																				BgL_pairz00_5189 =
																					(((BgL_sfunz00_bglt) COBJECT(
																							((BgL_sfunz00_bglt)
																								BgL_funz00_3744)))->
																					BgL_argsz00);
																				BgL_arg1854z00_3749 =
																					CAR(CDR(CDR(BgL_pairz00_5189)));
																			}
																			{	/* Cfa/tvector.scm 114 */
																				BgL_typez00_bglt BgL_vz00_5196;

																				BgL_vz00_5196 =
																					((BgL_typez00_bglt)
																					BGl_za2objza2z00zztype_cachez00);
																				((((BgL_variablez00_bglt)
																							COBJECT(((BgL_variablez00_bglt) (
																										(BgL_localz00_bglt)
																										BgL_arg1854z00_3749))))->
																						BgL_typez00) =
																					((BgL_typez00_bglt) BgL_vz00_5196),
																					BUNSPEC);
																			}
																		}
																	else
																		{	/* Cfa/tvector.scm 113 */
																			BFALSE;
																		}
																}
														}
													}
												else
													{	/* Cfa/tvector.scm 107 */
														BFALSE;
													}
											}
										}
									}
									{
										obj_t BgL_l1616z00_6900;

										BgL_l1616z00_6900 = CDR(BgL_l1616z00_3738);
										BgL_l1616z00_3738 = BgL_l1616z00_6900;
										goto BgL_zc3z04anonymousza31847ze3z87_3739;
									}
								}
							else
								{	/* Cfa/tvector.scm 116 */
									((bool_t) 1);
								}
						}
					}
					{	/* Cfa/tvector.scm 117 */
						obj_t BgL_gz00_3754;

						BgL_gz00_3754 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(1), BNIL);
						{	/* Cfa/tvector.scm 118 */
							bool_t BgL_test2499z00_6904;

							{	/* Cfa/tvector.scm 118 */
								obj_t BgL_classz00_5198;

								BgL_classz00_5198 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_gz00_3754))
									{	/* Cfa/tvector.scm 118 */
										BgL_objectz00_bglt BgL_arg1807z00_5200;

										BgL_arg1807z00_5200 = (BgL_objectz00_bglt) (BgL_gz00_3754);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/tvector.scm 118 */
												long BgL_idxz00_5206;

												BgL_idxz00_5206 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5200);
												BgL_test2499z00_6904 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5206 + 2L)) == BgL_classz00_5198);
											}
										else
											{	/* Cfa/tvector.scm 118 */
												bool_t BgL_res2325z00_5231;

												{	/* Cfa/tvector.scm 118 */
													obj_t BgL_oclassz00_5214;

													{	/* Cfa/tvector.scm 118 */
														obj_t BgL_arg1815z00_5222;
														long BgL_arg1816z00_5223;

														BgL_arg1815z00_5222 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/tvector.scm 118 */
															long BgL_arg1817z00_5224;

															BgL_arg1817z00_5224 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5200);
															BgL_arg1816z00_5223 =
																(BgL_arg1817z00_5224 - OBJECT_TYPE);
														}
														BgL_oclassz00_5214 =
															VECTOR_REF(BgL_arg1815z00_5222,
															BgL_arg1816z00_5223);
													}
													{	/* Cfa/tvector.scm 118 */
														bool_t BgL__ortest_1115z00_5215;

														BgL__ortest_1115z00_5215 =
															(BgL_classz00_5198 == BgL_oclassz00_5214);
														if (BgL__ortest_1115z00_5215)
															{	/* Cfa/tvector.scm 118 */
																BgL_res2325z00_5231 = BgL__ortest_1115z00_5215;
															}
														else
															{	/* Cfa/tvector.scm 118 */
																long BgL_odepthz00_5216;

																{	/* Cfa/tvector.scm 118 */
																	obj_t BgL_arg1804z00_5217;

																	BgL_arg1804z00_5217 = (BgL_oclassz00_5214);
																	BgL_odepthz00_5216 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_5217);
																}
																if ((2L < BgL_odepthz00_5216))
																	{	/* Cfa/tvector.scm 118 */
																		obj_t BgL_arg1802z00_5219;

																		{	/* Cfa/tvector.scm 118 */
																			obj_t BgL_arg1803z00_5220;

																			BgL_arg1803z00_5220 =
																				(BgL_oclassz00_5214);
																			BgL_arg1802z00_5219 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_5220, 2L);
																		}
																		BgL_res2325z00_5231 =
																			(BgL_arg1802z00_5219 ==
																			BgL_classz00_5198);
																	}
																else
																	{	/* Cfa/tvector.scm 118 */
																		BgL_res2325z00_5231 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2499z00_6904 = BgL_res2325z00_5231;
											}
									}
								else
									{	/* Cfa/tvector.scm 118 */
										BgL_test2499z00_6904 = ((bool_t) 0);
									}
							}
							if (BgL_test2499z00_6904)
								{	/* Cfa/tvector.scm 119 */
									BgL_valuez00_bglt BgL_fz00_3756;

									BgL_fz00_3756 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_gz00_3754))))->
										BgL_valuez00);
									{	/* Cfa/tvector.scm 120 */
										obj_t BgL_arg1860z00_3757;

										BgL_arg1860z00_3757 =
											(((BgL_cfunz00_bglt) COBJECT(
													((BgL_cfunz00_bglt) BgL_fz00_3756)))->
											BgL_argszd2typezd2);
										{	/* Cfa/tvector.scm 120 */
											obj_t BgL_objz00_5235;

											BgL_objz00_5235 = BGl_za2objza2z00zztype_cachez00;
											{	/* Cfa/tvector.scm 120 */
												obj_t BgL_tmpz00_6932;

												BgL_tmpz00_6932 = ((obj_t) BgL_arg1860z00_3757);
												SET_CAR(BgL_tmpz00_6932, BgL_objz00_5235);
											}
										}
									}
								}
							else
								{	/* Cfa/tvector.scm 118 */
									BFALSE;
								}
						}
					}
					{	/* Cfa/tvector.scm 121 */
						obj_t BgL_gz00_3759;

						BgL_gz00_3759 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(2), BNIL);
						{	/* Cfa/tvector.scm 122 */
							bool_t BgL_test2504z00_6937;

							{	/* Cfa/tvector.scm 122 */
								obj_t BgL_classz00_5236;

								BgL_classz00_5236 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_gz00_3759))
									{	/* Cfa/tvector.scm 122 */
										BgL_objectz00_bglt BgL_arg1807z00_5238;

										BgL_arg1807z00_5238 = (BgL_objectz00_bglt) (BgL_gz00_3759);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/tvector.scm 122 */
												long BgL_idxz00_5244;

												BgL_idxz00_5244 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5238);
												BgL_test2504z00_6937 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5244 + 2L)) == BgL_classz00_5236);
											}
										else
											{	/* Cfa/tvector.scm 122 */
												bool_t BgL_res2326z00_5269;

												{	/* Cfa/tvector.scm 122 */
													obj_t BgL_oclassz00_5252;

													{	/* Cfa/tvector.scm 122 */
														obj_t BgL_arg1815z00_5260;
														long BgL_arg1816z00_5261;

														BgL_arg1815z00_5260 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/tvector.scm 122 */
															long BgL_arg1817z00_5262;

															BgL_arg1817z00_5262 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5238);
															BgL_arg1816z00_5261 =
																(BgL_arg1817z00_5262 - OBJECT_TYPE);
														}
														BgL_oclassz00_5252 =
															VECTOR_REF(BgL_arg1815z00_5260,
															BgL_arg1816z00_5261);
													}
													{	/* Cfa/tvector.scm 122 */
														bool_t BgL__ortest_1115z00_5253;

														BgL__ortest_1115z00_5253 =
															(BgL_classz00_5236 == BgL_oclassz00_5252);
														if (BgL__ortest_1115z00_5253)
															{	/* Cfa/tvector.scm 122 */
																BgL_res2326z00_5269 = BgL__ortest_1115z00_5253;
															}
														else
															{	/* Cfa/tvector.scm 122 */
																long BgL_odepthz00_5254;

																{	/* Cfa/tvector.scm 122 */
																	obj_t BgL_arg1804z00_5255;

																	BgL_arg1804z00_5255 = (BgL_oclassz00_5252);
																	BgL_odepthz00_5254 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_5255);
																}
																if ((2L < BgL_odepthz00_5254))
																	{	/* Cfa/tvector.scm 122 */
																		obj_t BgL_arg1802z00_5257;

																		{	/* Cfa/tvector.scm 122 */
																			obj_t BgL_arg1803z00_5258;

																			BgL_arg1803z00_5258 =
																				(BgL_oclassz00_5252);
																			BgL_arg1802z00_5257 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_5258, 2L);
																		}
																		BgL_res2326z00_5269 =
																			(BgL_arg1802z00_5257 ==
																			BgL_classz00_5236);
																	}
																else
																	{	/* Cfa/tvector.scm 122 */
																		BgL_res2326z00_5269 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2504z00_6937 = BgL_res2326z00_5269;
											}
									}
								else
									{	/* Cfa/tvector.scm 122 */
										BgL_test2504z00_6937 = ((bool_t) 0);
									}
							}
							if (BgL_test2504z00_6937)
								{	/* Cfa/tvector.scm 123 */
									BgL_valuez00_bglt BgL_fz00_3761;

									BgL_fz00_3761 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_gz00_3759))))->
										BgL_valuez00);
									{	/* Cfa/tvector.scm 124 */
										obj_t BgL_arg1863z00_3762;

										BgL_arg1863z00_3762 =
											(((BgL_cfunz00_bglt) COBJECT(
													((BgL_cfunz00_bglt) BgL_fz00_3761)))->
											BgL_argszd2typezd2);
										{	/* Cfa/tvector.scm 124 */
											obj_t BgL_objz00_5273;

											BgL_objz00_5273 = BGl_za2objza2z00zztype_cachez00;
											{	/* Cfa/tvector.scm 124 */
												obj_t BgL_tmpz00_6965;

												BgL_tmpz00_6965 = ((obj_t) BgL_arg1863z00_3762);
												SET_CAR(BgL_tmpz00_6965, BgL_objz00_5273);
											}
										}
									}
								}
							else
								{	/* Cfa/tvector.scm 122 */
									BFALSE;
								}
						}
					}
					{	/* Cfa/tvector.scm 125 */
						obj_t BgL_gz00_3764;

						BgL_gz00_3764 =
							BGl_findzd2globalzd2zzast_envz00(CNST_TABLE_REF(3), BNIL);
						{	/* Cfa/tvector.scm 126 */
							bool_t BgL_test2509z00_6970;

							{	/* Cfa/tvector.scm 126 */
								obj_t BgL_classz00_5274;

								BgL_classz00_5274 = BGl_globalz00zzast_varz00;
								if (BGL_OBJECTP(BgL_gz00_3764))
									{	/* Cfa/tvector.scm 126 */
										BgL_objectz00_bglt BgL_arg1807z00_5276;

										BgL_arg1807z00_5276 = (BgL_objectz00_bglt) (BgL_gz00_3764);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/tvector.scm 126 */
												long BgL_idxz00_5282;

												BgL_idxz00_5282 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5276);
												BgL_test2509z00_6970 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5282 + 2L)) == BgL_classz00_5274);
											}
										else
											{	/* Cfa/tvector.scm 126 */
												bool_t BgL_res2327z00_5307;

												{	/* Cfa/tvector.scm 126 */
													obj_t BgL_oclassz00_5290;

													{	/* Cfa/tvector.scm 126 */
														obj_t BgL_arg1815z00_5298;
														long BgL_arg1816z00_5299;

														BgL_arg1815z00_5298 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/tvector.scm 126 */
															long BgL_arg1817z00_5300;

															BgL_arg1817z00_5300 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5276);
															BgL_arg1816z00_5299 =
																(BgL_arg1817z00_5300 - OBJECT_TYPE);
														}
														BgL_oclassz00_5290 =
															VECTOR_REF(BgL_arg1815z00_5298,
															BgL_arg1816z00_5299);
													}
													{	/* Cfa/tvector.scm 126 */
														bool_t BgL__ortest_1115z00_5291;

														BgL__ortest_1115z00_5291 =
															(BgL_classz00_5274 == BgL_oclassz00_5290);
														if (BgL__ortest_1115z00_5291)
															{	/* Cfa/tvector.scm 126 */
																BgL_res2327z00_5307 = BgL__ortest_1115z00_5291;
															}
														else
															{	/* Cfa/tvector.scm 126 */
																long BgL_odepthz00_5292;

																{	/* Cfa/tvector.scm 126 */
																	obj_t BgL_arg1804z00_5293;

																	BgL_arg1804z00_5293 = (BgL_oclassz00_5290);
																	BgL_odepthz00_5292 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_5293);
																}
																if ((2L < BgL_odepthz00_5292))
																	{	/* Cfa/tvector.scm 126 */
																		obj_t BgL_arg1802z00_5295;

																		{	/* Cfa/tvector.scm 126 */
																			obj_t BgL_arg1803z00_5296;

																			BgL_arg1803z00_5296 =
																				(BgL_oclassz00_5290);
																			BgL_arg1802z00_5295 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_5296, 2L);
																		}
																		BgL_res2327z00_5307 =
																			(BgL_arg1802z00_5295 ==
																			BgL_classz00_5274);
																	}
																else
																	{	/* Cfa/tvector.scm 126 */
																		BgL_res2327z00_5307 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2509z00_6970 = BgL_res2327z00_5307;
											}
									}
								else
									{	/* Cfa/tvector.scm 126 */
										BgL_test2509z00_6970 = ((bool_t) 0);
									}
							}
							if (BgL_test2509z00_6970)
								{	/* Cfa/tvector.scm 127 */
									BgL_valuez00_bglt BgL_fz00_3766;

									BgL_fz00_3766 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt)
													((BgL_globalz00_bglt) BgL_gz00_3764))))->
										BgL_valuez00);
									{	/* Cfa/tvector.scm 128 */
										obj_t BgL_arg1866z00_3767;

										{	/* Cfa/tvector.scm 128 */
											obj_t BgL_pairz00_5310;

											BgL_pairz00_5310 =
												(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_fz00_3766)))->BgL_argsz00);
											BgL_arg1866z00_3767 = CAR(BgL_pairz00_5310);
										}
										{	/* Cfa/tvector.scm 128 */
											BgL_typez00_bglt BgL_vz00_5312;

											BgL_vz00_5312 =
												((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt) BgL_arg1866z00_3767))))->
													BgL_typez00) =
												((BgL_typez00_bglt) BgL_vz00_5312), BUNSPEC);
										}
									}
								}
							else
								{	/* Cfa/tvector.scm 126 */
									BFALSE;
								}
						}
					}
				}
			else
				{	/* Cfa/tvector.scm 104 */
					BFALSE;
				}
			return BUNSPEC;
		}

	}



/* &unpatch-vector-set! */
	obj_t BGl_z62unpatchzd2vectorzd2setz12z70zzcfa_tvectorz00(obj_t
		BgL_envz00_6098)
	{
		{	/* Cfa/tvector.scm 103 */
			return BGl_unpatchzd2vectorzd2setz12z12zzcfa_tvectorz00();
		}

	}



/* vector->tvector! */
	BGL_EXPORTED_DEF obj_t BGl_vectorzd2ze3tvectorz12z23zzcfa_tvectorz00(obj_t
		BgL_globalsz00_55)
	{
		{	/* Cfa/tvector.scm 134 */
			if (CBOOL(BGl_tvectorzd2optimiza7ationzf3z86zzcfa_tvectorz00()))
				{	/* Cfa/tvector.scm 135 */
					BGl_inlinezd2setupz12zc0zzinline_walkz00(CNST_TABLE_REF(5));
					{	/* Cfa/tvector.scm 144 */
						obj_t BgL_vectorsz00_3771;

						BgL_vectorsz00_3771 = BGl_getzd2tvectorszd2zzcfa_tvectorz00();
						{	/* Cfa/tvector.scm 145 */
							obj_t BgL_tvectorsz00_3772;

							{	/* Cfa/tvector.scm 149 */
								obj_t BgL_tmpz00_5313;

								{	/* Cfa/tvector.scm 149 */
									int BgL_tmpz00_7010;

									BgL_tmpz00_7010 = (int) (1L);
									BgL_tmpz00_5313 = BGL_MVALUES_VAL(BgL_tmpz00_7010);
								}
								{	/* Cfa/tvector.scm 149 */
									int BgL_tmpz00_7013;

									BgL_tmpz00_7013 = (int) (1L);
									BGL_MVALUES_VAL_SET(BgL_tmpz00_7013, BUNSPEC);
								}
								BgL_tvectorsz00_3772 = BgL_tmpz00_5313;
							}
							{
								obj_t BgL_l1619z00_3774;

								BgL_l1619z00_3774 = BgL_vectorsz00_3771;
							BgL_zc3z04anonymousza31871ze3z87_3775:
								if (PAIRP(BgL_l1619z00_3774))
									{	/* Cfa/tvector.scm 146 */
										{	/* Cfa/tvector.scm 147 */
											obj_t BgL_vz00_3777;

											BgL_vz00_3777 = CAR(BgL_l1619z00_3774);
											{	/* Cfa/tvector.scm 147 */
												bool_t BgL_test2516z00_7019;

												{	/* Cfa/tvector.scm 147 */
													BgL_typez00_bglt BgL_arg1875z00_3780;

													BgL_arg1875z00_3780 =
														(((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_vz00_3777)))->
														BgL_typez00);
													BgL_test2516z00_7019 =
														(((obj_t) BgL_arg1875z00_3780) ==
														BGl_za2_za2z00zztype_cachez00);
												}
												if (BgL_test2516z00_7019)
													{	/* Cfa/tvector.scm 148 */
														BgL_typez00_bglt BgL_vz00_5317;

														BgL_vz00_5317 =
															((BgL_typez00_bglt)
															BGl_za2vectorza2z00zztype_cachez00);
														((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																			BgL_vz00_3777)))->BgL_typez00) =
															((BgL_typez00_bglt) BgL_vz00_5317), BUNSPEC);
													}
												else
													{	/* Cfa/tvector.scm 147 */
														BFALSE;
													}
											}
										}
										{
											obj_t BgL_l1619z00_7027;

											BgL_l1619z00_7027 = CDR(BgL_l1619z00_3774);
											BgL_l1619z00_3774 = BgL_l1619z00_7027;
											goto BgL_zc3z04anonymousza31871ze3z87_3775;
										}
									}
								else
									{	/* Cfa/tvector.scm 146 */
										((bool_t) 1);
									}
							}
							BGl_showzd2tvectorzd2zzcfa_tvectorz00(BgL_tvectorsz00_3772);
							if (PAIRP(BgL_tvectorsz00_3772))
								{	/* Cfa/tvector.scm 153 */
									obj_t BgL_addzd2treezd2_3784;

									BgL_addzd2treezd2_3784 =
										BGl_declarezd2tvectorszd2zzcfa_tvectorz00
										(BgL_tvectorsz00_3772);
									BGl_patchzd2treez12zc0zzcfa_tvectorz00(BgL_globalsz00_55);
									BGl_lvtypezd2astz12zc0zzast_lvtypez00(BgL_addzd2treezd2_3784);
									return BgL_addzd2treezd2_3784;
								}
							else
								{	/* Cfa/tvector.scm 152 */
									BGl_patchzd2treez12zc0zzcfa_tvectorz00(BgL_globalsz00_55);
									return BNIL;
								}
						}
					}
				}
			else
				{	/* Cfa/tvector.scm 135 */
					return BNIL;
				}
		}

	}



/* &vector->tvector! */
	obj_t BGl_z62vectorzd2ze3tvectorz12z41zzcfa_tvectorz00(obj_t BgL_envz00_6099,
		obj_t BgL_globalsz00_6100)
	{
		{	/* Cfa/tvector.scm 134 */
			return BGl_vectorzd2ze3tvectorz12z23zzcfa_tvectorz00(BgL_globalsz00_6100);
		}

	}



/* add-make-vector! */
	BGL_EXPORTED_DEF obj_t
		BGl_addzd2makezd2vectorz12z12zzcfa_tvectorz00(BgL_nodez00_bglt
		BgL_nodez00_56)
	{
		{	/* Cfa/tvector.scm 172 */
			if (CBOOL(BGl_tvectorzd2optimiza7ationzf3z86zzcfa_tvectorz00()))
				{	/* Cfa/tvector.scm 173 */
					return (BGl_za2makezd2vectorzd2listza2z00zzcfa_tvectorz00 =
						MAKE_YOUNG_PAIR(
							((obj_t) BgL_nodez00_56),
							BGl_za2makezd2vectorzd2listza2z00zzcfa_tvectorz00), BUNSPEC);
				}
			else
				{	/* Cfa/tvector.scm 173 */
					return BFALSE;
				}
		}

	}



/* &add-make-vector! */
	obj_t BGl_z62addzd2makezd2vectorz12z70zzcfa_tvectorz00(obj_t BgL_envz00_6101,
		obj_t BgL_nodez00_6102)
	{
		{	/* Cfa/tvector.scm 172 */
			return
				BGl_addzd2makezd2vectorz12z12zzcfa_tvectorz00(
				((BgL_nodez00_bglt) BgL_nodez00_6102));
		}

	}



/* get-tvectors */
	obj_t BGl_getzd2tvectorszd2zzcfa_tvectorz00(void)
	{
		{	/* Cfa/tvector.scm 182 */
			{
				obj_t BgL_appsz00_3789;
				obj_t BgL_vectorsz00_3790;
				obj_t BgL_tvectorsz00_3791;

				BgL_appsz00_3789 = BGl_za2makezd2vectorzd2listza2z00zzcfa_tvectorz00;
				BgL_vectorsz00_3790 = BNIL;
				BgL_tvectorsz00_3791 = BNIL;
			BgL_zc3z04anonymousza31879ze3z87_3792:
				if (NULLP(BgL_appsz00_3789))
					{	/* Cfa/tvector.scm 186 */
						{	/* Cfa/tvector.scm 187 */
							int BgL_tmpz00_7046;

							BgL_tmpz00_7046 = (int) (2L);
							BGL_MVALUES_NUMBER_SET(BgL_tmpz00_7046);
						}
						{	/* Cfa/tvector.scm 187 */
							int BgL_tmpz00_7049;

							BgL_tmpz00_7049 = (int) (1L);
							BGL_MVALUES_VAL_SET(BgL_tmpz00_7049, BgL_tvectorsz00_3791);
						}
						return BgL_vectorsz00_3790;
					}
				else
					{	/* Cfa/tvector.scm 188 */
						obj_t BgL_appz00_3796;

						BgL_appz00_3796 = CAR(((obj_t) BgL_appsz00_3789));
						{	/* Cfa/tvector.scm 188 */
							BgL_typez00_bglt BgL_typez00_3797;

							BgL_typez00_3797 =
								BGl_getzd2vectorzd2itemzd2typezd2zzcfa_tvectorz00(
								((BgL_nodez00_bglt) BgL_appz00_3796));
							{	/* Cfa/tvector.scm 189 */

								{	/* Cfa/tvector.scm 193 */
									bool_t BgL_test2520z00_7056;

									if (
										(((obj_t) BgL_typez00_3797) ==
											BGl_za2_za2z00zztype_cachez00))
										{	/* Cfa/tvector.scm 193 */
											BgL_test2520z00_7056 = ((bool_t) 0);
										}
									else
										{	/* Cfa/tvector.scm 193 */
											if (BGl_subzd2typezf3z21zztype_envz00(BgL_typez00_3797,
													((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00)))
												{	/* Cfa/tvector.scm 194 */
													BgL_test2520z00_7056 = ((bool_t) 0);
												}
											else
												{	/* Cfa/tvector.scm 194 */
													BgL_test2520z00_7056 = ((bool_t) 1);
												}
										}
									if (BgL_test2520z00_7056)
										{	/* Cfa/tvector.scm 195 */
											obj_t BgL_arg1883z00_3800;
											obj_t BgL_arg1884z00_3801;

											BgL_arg1883z00_3800 = CDR(((obj_t) BgL_appsz00_3789));
											BgL_arg1884z00_3801 =
												MAKE_YOUNG_PAIR(BgL_appz00_3796, BgL_tvectorsz00_3791);
											{
												obj_t BgL_tvectorsz00_7067;
												obj_t BgL_appsz00_7066;

												BgL_appsz00_7066 = BgL_arg1883z00_3800;
												BgL_tvectorsz00_7067 = BgL_arg1884z00_3801;
												BgL_tvectorsz00_3791 = BgL_tvectorsz00_7067;
												BgL_appsz00_3789 = BgL_appsz00_7066;
												goto BgL_zc3z04anonymousza31879ze3z87_3792;
											}
										}
									else
										{	/* Cfa/tvector.scm 196 */
											obj_t BgL_arg1885z00_3802;
											obj_t BgL_arg1887z00_3803;

											BgL_arg1885z00_3802 = CDR(((obj_t) BgL_appsz00_3789));
											BgL_arg1887z00_3803 =
												MAKE_YOUNG_PAIR(BgL_appz00_3796, BgL_vectorsz00_3790);
											{
												obj_t BgL_vectorsz00_7072;
												obj_t BgL_appsz00_7071;

												BgL_appsz00_7071 = BgL_arg1885z00_3802;
												BgL_vectorsz00_7072 = BgL_arg1887z00_3803;
												BgL_vectorsz00_3790 = BgL_vectorsz00_7072;
												BgL_appsz00_3789 = BgL_appsz00_7071;
												goto BgL_zc3z04anonymousza31879ze3z87_3792;
											}
										}
								}
							}
						}
					}
			}
		}

	}



/* show-tvector */
	bool_t BGl_showzd2tvectorzd2zzcfa_tvectorz00(obj_t BgL_tvectorz00_60)
	{
		{	/* Cfa/tvector.scm 224 */
			{	/* Cfa/tvector.scm 225 */
				obj_t BgL_list1888z00_3806;

				{	/* Cfa/tvector.scm 225 */
					obj_t BgL_arg1889z00_3807;

					BgL_arg1889z00_3807 =
						MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
					BgL_list1888z00_3806 =
						MAKE_YOUNG_PAIR(BGl_string2356z00zzcfa_tvectorz00,
						BgL_arg1889z00_3807);
				}
				BGl_verbosez00zztools_speekz00(BINT(1L), BgL_list1888z00_3806);
			}
			{
				obj_t BgL_l1623z00_3809;

				BgL_l1623z00_3809 = BgL_tvectorz00_60;
			BgL_zc3z04anonymousza31890ze3z87_3810:
				if (PAIRP(BgL_l1623z00_3809))
					{	/* Cfa/tvector.scm 226 */
						{	/* Cfa/tvector.scm 227 */
							obj_t BgL_appz00_3812;

							BgL_appz00_3812 = CAR(BgL_l1623z00_3809);
							{	/* Cfa/tvector.scm 228 */
								obj_t BgL_arg1892z00_3813;
								obj_t BgL_arg1893z00_3814;

								BgL_arg1892z00_3813 =
									BGl_shapez00zztools_shapez00(BGl_za2objza2z00zztype_cachez00);
								{	/* Cfa/tvector.scm 230 */
									BgL_typez00_bglt BgL_arg1901z00_3820;

									BgL_arg1901z00_3820 =
										BGl_getzd2vectorzd2itemzd2typezd2zzcfa_tvectorz00(
										((BgL_nodez00_bglt) BgL_appz00_3812));
									BgL_arg1893z00_3814 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_arg1901z00_3820));
								}
								{	/* Cfa/tvector.scm 227 */
									obj_t BgL_list1894z00_3815;

									{	/* Cfa/tvector.scm 227 */
										obj_t BgL_arg1896z00_3816;

										{	/* Cfa/tvector.scm 227 */
											obj_t BgL_arg1897z00_3817;

											{	/* Cfa/tvector.scm 227 */
												obj_t BgL_arg1898z00_3818;

												{	/* Cfa/tvector.scm 227 */
													obj_t BgL_arg1899z00_3819;

													BgL_arg1899z00_3819 =
														MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
													BgL_arg1898z00_3818 =
														MAKE_YOUNG_PAIR(BgL_arg1893z00_3814,
														BgL_arg1899z00_3819);
												}
												BgL_arg1897z00_3817 =
													MAKE_YOUNG_PAIR(BGl_string2357z00zzcfa_tvectorz00,
													BgL_arg1898z00_3818);
											}
											BgL_arg1896z00_3816 =
												MAKE_YOUNG_PAIR(BgL_arg1892z00_3813,
												BgL_arg1897z00_3817);
										}
										BgL_list1894z00_3815 =
											MAKE_YOUNG_PAIR(BGl_string2358z00zzcfa_tvectorz00,
											BgL_arg1896z00_3816);
									}
									BGl_verbosez00zztools_speekz00(BINT(2L),
										BgL_list1894z00_3815);
						}}}
						{
							obj_t BgL_l1623z00_7094;

							BgL_l1623z00_7094 = CDR(BgL_l1623z00_3809);
							BgL_l1623z00_3809 = BgL_l1623z00_7094;
							goto BgL_zc3z04anonymousza31890ze3z87_3810;
						}
					}
				else
					{	/* Cfa/tvector.scm 226 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* declare-tvectors */
	obj_t BGl_declarezd2tvectorszd2zzcfa_tvectorz00(obj_t BgL_tvectorz00_61)
	{
		{	/* Cfa/tvector.scm 237 */
			{
				obj_t BgL_l1625z00_3824;

				BgL_l1625z00_3824 = BgL_tvectorz00_61;
			BgL_zc3z04anonymousza31903ze3z87_3825:
				if (PAIRP(BgL_l1625z00_3824))
					{	/* Cfa/tvector.scm 239 */
						{	/* Cfa/tvector.scm 240 */
							obj_t BgL_appz00_3827;

							BgL_appz00_3827 = CAR(BgL_l1625z00_3824);
							{	/* Cfa/tvector.scm 240 */
								BgL_typez00_bglt BgL_typez00_3828;

								BgL_typez00_3828 =
									BGl_getzd2vectorzd2itemzd2typezd2zzcfa_tvectorz00(
									((BgL_nodez00_bglt) BgL_appz00_3827));
								{	/* Cfa/tvector.scm 242 */
									bool_t BgL_test2525z00_7101;

									{	/* Cfa/tvector.scm 242 */
										obj_t BgL_arg1916z00_3838;

										BgL_arg1916z00_3838 =
											(((BgL_typez00_bglt) COBJECT(BgL_typez00_3828))->
											BgL_tvectorz00);
										{	/* Cfa/tvector.scm 242 */
											obj_t BgL_classz00_5325;

											BgL_classz00_5325 = BGl_typez00zztype_typez00;
											if (BGL_OBJECTP(BgL_arg1916z00_3838))
												{	/* Cfa/tvector.scm 242 */
													BgL_objectz00_bglt BgL_arg1807z00_5327;

													BgL_arg1807z00_5327 =
														(BgL_objectz00_bglt) (BgL_arg1916z00_3838);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Cfa/tvector.scm 242 */
															long BgL_idxz00_5333;

															BgL_idxz00_5333 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5327);
															BgL_test2525z00_7101 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_5333 + 1L)) == BgL_classz00_5325);
														}
													else
														{	/* Cfa/tvector.scm 242 */
															bool_t BgL_res2328z00_5358;

															{	/* Cfa/tvector.scm 242 */
																obj_t BgL_oclassz00_5341;

																{	/* Cfa/tvector.scm 242 */
																	obj_t BgL_arg1815z00_5349;
																	long BgL_arg1816z00_5350;

																	BgL_arg1815z00_5349 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Cfa/tvector.scm 242 */
																		long BgL_arg1817z00_5351;

																		BgL_arg1817z00_5351 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5327);
																		BgL_arg1816z00_5350 =
																			(BgL_arg1817z00_5351 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_5341 =
																		VECTOR_REF(BgL_arg1815z00_5349,
																		BgL_arg1816z00_5350);
																}
																{	/* Cfa/tvector.scm 242 */
																	bool_t BgL__ortest_1115z00_5342;

																	BgL__ortest_1115z00_5342 =
																		(BgL_classz00_5325 == BgL_oclassz00_5341);
																	if (BgL__ortest_1115z00_5342)
																		{	/* Cfa/tvector.scm 242 */
																			BgL_res2328z00_5358 =
																				BgL__ortest_1115z00_5342;
																		}
																	else
																		{	/* Cfa/tvector.scm 242 */
																			long BgL_odepthz00_5343;

																			{	/* Cfa/tvector.scm 242 */
																				obj_t BgL_arg1804z00_5344;

																				BgL_arg1804z00_5344 =
																					(BgL_oclassz00_5341);
																				BgL_odepthz00_5343 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_5344);
																			}
																			if ((1L < BgL_odepthz00_5343))
																				{	/* Cfa/tvector.scm 242 */
																					obj_t BgL_arg1802z00_5346;

																					{	/* Cfa/tvector.scm 242 */
																						obj_t BgL_arg1803z00_5347;

																						BgL_arg1803z00_5347 =
																							(BgL_oclassz00_5341);
																						BgL_arg1802z00_5346 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_5347, 1L);
																					}
																					BgL_res2328z00_5358 =
																						(BgL_arg1802z00_5346 ==
																						BgL_classz00_5325);
																				}
																			else
																				{	/* Cfa/tvector.scm 242 */
																					BgL_res2328z00_5358 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2525z00_7101 = BgL_res2328z00_5358;
														}
												}
											else
												{	/* Cfa/tvector.scm 242 */
													BgL_test2525z00_7101 = ((bool_t) 0);
												}
										}
									}
									if (BgL_test2525z00_7101)
										{	/* Cfa/tvector.scm 242 */
											BFALSE;
										}
									else
										{	/* Cfa/tvector.scm 243 */
											obj_t BgL_tvzd2idzd2_3832;

											{	/* Cfa/tvector.scm 243 */
												obj_t BgL_arg1911z00_3834;

												{	/* Cfa/tvector.scm 243 */
													obj_t BgL_arg1912z00_3835;
													obj_t BgL_arg1913z00_3836;

													{	/* Cfa/tvector.scm 243 */
														obj_t BgL_symbolz00_5359;

														BgL_symbolz00_5359 = CNST_TABLE_REF(6);
														{	/* Cfa/tvector.scm 243 */
															obj_t BgL_arg1455z00_5360;

															BgL_arg1455z00_5360 =
																SYMBOL_TO_STRING(BgL_symbolz00_5359);
															BgL_arg1912z00_3835 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg1455z00_5360);
														}
													}
													{	/* Cfa/tvector.scm 243 */
														obj_t BgL_arg1914z00_3837;

														BgL_arg1914z00_3837 =
															(((BgL_typez00_bglt) COBJECT(BgL_typez00_3828))->
															BgL_idz00);
														{	/* Cfa/tvector.scm 243 */
															obj_t BgL_arg1455z00_5362;

															BgL_arg1455z00_5362 =
																SYMBOL_TO_STRING(BgL_arg1914z00_3837);
															BgL_arg1913z00_3836 =
																BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																(BgL_arg1455z00_5362);
														}
													}
													BgL_arg1911z00_3834 =
														string_append(BgL_arg1912z00_3835,
														BgL_arg1913z00_3836);
												}
												BgL_tvzd2idzd2_3832 =
													bstring_to_symbol(BgL_arg1911z00_3834);
											}
											{
												obj_t BgL_auxz00_7133;

												{	/* Cfa/tvector.scm 245 */
													obj_t BgL_arg1910z00_3833;

													BgL_arg1910z00_3833 =
														(((BgL_typez00_bglt) COBJECT(BgL_typez00_3828))->
														BgL_idz00);
													BgL_auxz00_7133 =
														BGl_modulezd2tvectorzd2clausez00zzmodule_typez00
														(BgL_tvzd2idzd2_3832, BgL_arg1910z00_3833,
														CNST_TABLE_REF(7), BFALSE);
												}
												((((BgL_typez00_bglt) COBJECT(BgL_typez00_3828))->
														BgL_tvectorz00) =
													((obj_t) BgL_auxz00_7133), BUNSPEC);
											}
										}
								}
							}
						}
						{
							obj_t BgL_l1625z00_7138;

							BgL_l1625z00_7138 = CDR(BgL_l1625z00_3824);
							BgL_l1625z00_3824 = BgL_l1625z00_7138;
							goto BgL_zc3z04anonymousza31903ze3z87_3825;
						}
					}
				else
					{	/* Cfa/tvector.scm 239 */
						((bool_t) 1);
					}
			}
			{	/* Cfa/tvector.scm 248 */
				BgL_typez00_bglt BgL_oldzd2defaultzd2typez00_3841;

				BgL_oldzd2defaultzd2typez00_3841 =
					BGl_getzd2defaultzd2typez00zztype_cachez00();
				BGl_setzd2defaultzd2typez12z12zztype_cachez00(
					((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
				{	/* Cfa/tvector.scm 250 */
					obj_t BgL_tvectorzd2unitzd2_3842;

					BgL_tvectorzd2unitzd2_3842 =
						BGl_tvectorzd2finaliza7erz75zzmodule_typez00();
					BGl_pragmazd2finaliza7erz75zzmodule_pragmaz00();
					{	/* Cfa/tvector.scm 252 */
						obj_t BgL_resz00_3843;

						{	/* Cfa/tvector.scm 252 */
							bool_t BgL_test2530z00_7145;

							if (STRUCTP(BgL_tvectorzd2unitzd2_3842))
								{	/* Cfa/tvector.scm 252 */
									BgL_test2530z00_7145 =
										(STRUCT_KEY(BgL_tvectorzd2unitzd2_3842) ==
										CNST_TABLE_REF(8));
								}
							else
								{	/* Cfa/tvector.scm 252 */
									BgL_test2530z00_7145 = ((bool_t) 0);
								}
							if (BgL_test2530z00_7145)
								{	/* Cfa/tvector.scm 253 */
									obj_t BgL_astz00_3845;

									{	/* Cfa/tvector.scm 253 */
										obj_t BgL_arg1919z00_3846;

										{	/* Cfa/tvector.scm 253 */
											obj_t BgL_list1920z00_3847;

											BgL_list1920z00_3847 =
												MAKE_YOUNG_PAIR(BgL_tvectorzd2unitzd2_3842, BNIL);
											BgL_arg1919z00_3846 = BgL_list1920z00_3847;
										}
										BgL_astz00_3845 =
											BGl_buildzd2astzd2sanszd2removezd2zzast_buildz00
											(BgL_arg1919z00_3846);
									}
									BgL_resz00_3843 =
										BGl_globaliza7ezd2walkz12z67zzglobaliza7e_walkza7
										(BgL_astz00_3845, CNST_TABLE_REF(9));
								}
							else
								{	/* Cfa/tvector.scm 252 */
									BgL_resz00_3843 = BNIL;
								}
						}
						BGl_lvtypezd2astz12zc0zzast_lvtypez00(BgL_resz00_3843);
						BGl_setzd2defaultzd2typez12z12zztype_cachez00
							(BgL_oldzd2defaultzd2typez00_3841);
						return BgL_resz00_3843;
					}
				}
			}
		}

	}



/* patch-tree! */
	bool_t BGl_patchzd2treez12zc0zzcfa_tvectorz00(obj_t BgL_globalsz00_62)
	{
		{	/* Cfa/tvector.scm 267 */
			{
				obj_t BgL_l1627z00_3849;

				BgL_l1627z00_3849 = BgL_globalsz00_62;
			BgL_zc3z04anonymousza31921ze3z87_3850:
				if (PAIRP(BgL_l1627z00_3849))
					{	/* Cfa/tvector.scm 268 */
						BGl_patchzd2funz12zc0zzcfa_tvectorz00(CAR(BgL_l1627z00_3849));
						{
							obj_t BgL_l1627z00_7161;

							BgL_l1627z00_7161 = CDR(BgL_l1627z00_3849);
							BgL_l1627z00_3849 = BgL_l1627z00_7161;
							goto BgL_zc3z04anonymousza31921ze3z87_3850;
						}
					}
				else
					{	/* Cfa/tvector.scm 268 */
						return ((bool_t) 1);
					}
			}
		}

	}



/* patch-fun! */
	obj_t BGl_patchzd2funz12zc0zzcfa_tvectorz00(obj_t BgL_variablez00_63)
	{
		{	/* Cfa/tvector.scm 273 */
			{	/* Cfa/tvector.scm 274 */
				BgL_valuez00_bglt BgL_funz00_3855;

				BgL_funz00_3855 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_variablez00_63)))->BgL_valuez00);
				{	/* Cfa/tvector.scm 277 */
					obj_t BgL_arg1925z00_3856;

					{	/* Cfa/tvector.scm 277 */
						obj_t BgL_arg1926z00_3857;

						BgL_arg1926z00_3857 =
							(((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_3855)))->BgL_bodyz00);
						BgL_arg1925z00_3856 =
							BGl_patchz12z12zzcfa_tvectorz00(
							((BgL_nodez00_bglt) BgL_arg1926z00_3857));
					}
					return
						((((BgL_sfunz00_bglt) COBJECT(
									((BgL_sfunz00_bglt) BgL_funz00_3855)))->BgL_bodyz00) =
						((obj_t) BgL_arg1925z00_3856), BUNSPEC);
				}
			}
		}

	}



/* patch*! */
	obj_t BGl_patchza2z12zb0zzcfa_tvectorz00(obj_t BgL_nodeza2za2_87)
	{
		{	/* Cfa/tvector.scm 485 */
			{
				obj_t BgL_nodeza2za2_3859;

				BgL_nodeza2za2_3859 = BgL_nodeza2za2_87;
			BgL_zc3z04anonymousza31927ze3z87_3860:
				if (NULLP(BgL_nodeza2za2_3859))
					{	/* Cfa/tvector.scm 487 */
						return CNST_TABLE_REF(10);
					}
				else
					{	/* Cfa/tvector.scm 487 */
						{	/* Cfa/tvector.scm 490 */
							obj_t BgL_arg1929z00_3862;

							{	/* Cfa/tvector.scm 490 */
								obj_t BgL_arg1930z00_3863;

								BgL_arg1930z00_3863 = CAR(((obj_t) BgL_nodeza2za2_3859));
								BgL_arg1929z00_3862 =
									BGl_patchz12z12zzcfa_tvectorz00(
									((BgL_nodez00_bglt) BgL_arg1930z00_3863));
							}
							{	/* Cfa/tvector.scm 490 */
								obj_t BgL_tmpz00_7178;

								BgL_tmpz00_7178 = ((obj_t) BgL_nodeza2za2_3859);
								SET_CAR(BgL_tmpz00_7178, BgL_arg1929z00_3862);
							}
						}
						{	/* Cfa/tvector.scm 491 */
							obj_t BgL_arg1931z00_3864;

							BgL_arg1931z00_3864 = CDR(((obj_t) BgL_nodeza2za2_3859));
							{
								obj_t BgL_nodeza2za2_7183;

								BgL_nodeza2za2_7183 = BgL_arg1931z00_3864;
								BgL_nodeza2za2_3859 = BgL_nodeza2za2_7183;
								goto BgL_zc3z04anonymousza31927ze3z87_3860;
							}
						}
					}
			}
		}

	}



/* patch-vector?! */
	BgL_nodez00_bglt BGl_patchzd2vectorzf3z12z33zzcfa_tvectorz00(BgL_appz00_bglt
		BgL_nodez00_90)
	{
		{	/* Cfa/tvector.scm 534 */
			BGl_patchza2z12zb0zzcfa_tvectorz00(
				(((BgL_appz00_bglt) COBJECT(BgL_nodez00_90))->BgL_argsz00));
			{	/* Cfa/tvector.scm 537 */
				BgL_approxz00_bglt BgL_approxz00_3868;

				{	/* Cfa/tvector.scm 537 */
					obj_t BgL_arg1938z00_3877;

					BgL_arg1938z00_3877 =
						CAR((((BgL_appz00_bglt) COBJECT(BgL_nodez00_90))->BgL_argsz00));
					BgL_approxz00_3868 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1938z00_3877));
				}
				{	/* Cfa/tvector.scm 537 */
					obj_t BgL_typez00_3869;

					BgL_typez00_3869 =
						BGl_getzd2approxzd2typez00zzcfa_typez00(BgL_approxz00_3868,
						CAR((((BgL_appz00_bglt) COBJECT(BgL_nodez00_90))->BgL_argsz00)));
					{	/* Cfa/tvector.scm 538 */

						{	/* Cfa/tvector.scm 539 */
							bool_t BgL_test2534z00_7193;

							if ((BgL_typez00_3869 == BGl_za2vectorza2z00zztype_cachez00))
								{	/* Cfa/tvector.scm 539 */
									BgL_test2534z00_7193 = ((bool_t) 1);
								}
							else
								{	/* Cfa/tvector.scm 539 */
									obj_t BgL_classz00_5380;

									BgL_classz00_5380 = BGl_tvecz00zztvector_tvectorz00;
									if (BGL_OBJECTP(BgL_typez00_3869))
										{	/* Cfa/tvector.scm 539 */
											BgL_objectz00_bglt BgL_arg1807z00_5382;

											BgL_arg1807z00_5382 =
												(BgL_objectz00_bglt) (BgL_typez00_3869);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Cfa/tvector.scm 539 */
													long BgL_idxz00_5388;

													BgL_idxz00_5388 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5382);
													BgL_test2534z00_7193 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_5388 + 2L)) == BgL_classz00_5380);
												}
											else
												{	/* Cfa/tvector.scm 539 */
													bool_t BgL_res2331z00_5413;

													{	/* Cfa/tvector.scm 539 */
														obj_t BgL_oclassz00_5396;

														{	/* Cfa/tvector.scm 539 */
															obj_t BgL_arg1815z00_5404;
															long BgL_arg1816z00_5405;

															BgL_arg1815z00_5404 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Cfa/tvector.scm 539 */
																long BgL_arg1817z00_5406;

																BgL_arg1817z00_5406 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5382);
																BgL_arg1816z00_5405 =
																	(BgL_arg1817z00_5406 - OBJECT_TYPE);
															}
															BgL_oclassz00_5396 =
																VECTOR_REF(BgL_arg1815z00_5404,
																BgL_arg1816z00_5405);
														}
														{	/* Cfa/tvector.scm 539 */
															bool_t BgL__ortest_1115z00_5397;

															BgL__ortest_1115z00_5397 =
																(BgL_classz00_5380 == BgL_oclassz00_5396);
															if (BgL__ortest_1115z00_5397)
																{	/* Cfa/tvector.scm 539 */
																	BgL_res2331z00_5413 =
																		BgL__ortest_1115z00_5397;
																}
															else
																{	/* Cfa/tvector.scm 539 */
																	long BgL_odepthz00_5398;

																	{	/* Cfa/tvector.scm 539 */
																		obj_t BgL_arg1804z00_5399;

																		BgL_arg1804z00_5399 = (BgL_oclassz00_5396);
																		BgL_odepthz00_5398 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_5399);
																	}
																	if ((2L < BgL_odepthz00_5398))
																		{	/* Cfa/tvector.scm 539 */
																			obj_t BgL_arg1802z00_5401;

																			{	/* Cfa/tvector.scm 539 */
																				obj_t BgL_arg1803z00_5402;

																				BgL_arg1803z00_5402 =
																					(BgL_oclassz00_5396);
																				BgL_arg1802z00_5401 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_5402, 2L);
																			}
																			BgL_res2331z00_5413 =
																				(BgL_arg1802z00_5401 ==
																				BgL_classz00_5380);
																		}
																	else
																		{	/* Cfa/tvector.scm 539 */
																			BgL_res2331z00_5413 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2534z00_7193 = BgL_res2331z00_5413;
												}
										}
									else
										{	/* Cfa/tvector.scm 539 */
											BgL_test2534z00_7193 = ((bool_t) 0);
										}
								}
							if (BgL_test2534z00_7193)
								{	/* Cfa/tvector.scm 540 */
									BgL_literalz00_bglt BgL_new1228z00_3871;

									{	/* Cfa/tvector.scm 541 */
										BgL_literalz00_bglt BgL_new1226z00_3873;

										BgL_new1226z00_3873 =
											((BgL_literalz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_literalz00_bgl))));
										{	/* Cfa/tvector.scm 541 */
											long BgL_arg1935z00_3874;

											BgL_arg1935z00_3874 =
												BGL_CLASS_NUM(BGl_literalz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1226z00_3873),
												BgL_arg1935z00_3874);
										}
										{	/* Cfa/tvector.scm 541 */
											BgL_objectz00_bglt BgL_tmpz00_7222;

											BgL_tmpz00_7222 =
												((BgL_objectz00_bglt) BgL_new1226z00_3873);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7222, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1226z00_3873);
										BgL_new1228z00_3871 = BgL_new1226z00_3873;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1228z00_3871)))->
											BgL_locz00) =
										((obj_t) (((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
															BgL_nodez00_90)))->BgL_locz00)), BUNSPEC);
									{
										BgL_typez00_bglt BgL_auxz00_7230;

										{	/* Cfa/tvector.scm 542 */
											BgL_typez00_bglt BgL_arg1934z00_3872;

											BgL_arg1934z00_3872 =
												BGl_getzd2typezd2atomz00zztype_typeofz00(BTRUE);
											BgL_auxz00_7230 =
												BGl_strictzd2nodezd2typez00zzast_nodez00
												(BgL_arg1934z00_3872,
												((BgL_typez00_bglt) BGl_za2boolza2z00zztype_cachez00));
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1228z00_3871)))->
												BgL_typez00) =
											((BgL_typez00_bglt) BgL_auxz00_7230), BUNSPEC);
									}
									((((BgL_atomz00_bglt) COBJECT(
													((BgL_atomz00_bglt) BgL_new1228z00_3871)))->
											BgL_valuez00) = ((obj_t) BTRUE), BUNSPEC);
									return ((BgL_nodez00_bglt) BgL_new1228z00_3871);
								}
							else
								{	/* Cfa/tvector.scm 539 */
									return ((BgL_nodez00_bglt) BgL_nodez00_90);
								}
						}
					}
				}
			}
		}

	}



/* patch-vector->list! */
	BgL_nodez00_bglt
		BGl_patchzd2vectorzd2ze3listz12zf1zzcfa_tvectorz00(BgL_appz00_bglt
		BgL_nodez00_91)
	{
		{	/* Cfa/tvector.scm 549 */
			BGl_patchza2z12zb0zzcfa_tvectorz00(
				(((BgL_appz00_bglt) COBJECT(BgL_nodez00_91))->BgL_argsz00));
			{	/* Cfa/tvector.scm 552 */
				BgL_approxz00_bglt BgL_approxz00_3881;

				{	/* Cfa/tvector.scm 552 */
					obj_t BgL_arg1951z00_3895;

					BgL_arg1951z00_3895 =
						CAR((((BgL_appz00_bglt) COBJECT(BgL_nodez00_91))->BgL_argsz00));
					BgL_approxz00_3881 =
						BGl_cfaz12z12zzcfa_cfaz00(((BgL_nodez00_bglt) BgL_arg1951z00_3895));
				}
				{	/* Cfa/tvector.scm 552 */
					obj_t BgL_tvz00_3882;

					BgL_tvz00_3882 =
						BGl_getzd2approxzd2typez00zzcfa_typez00(BgL_approxz00_3881,
						((obj_t) BgL_nodez00_91));
					{	/* Cfa/tvector.scm 553 */

						{	/* Cfa/tvector.scm 554 */
							bool_t BgL_test2540z00_7248;

							{	/* Cfa/tvector.scm 554 */
								obj_t BgL_classz00_5419;

								BgL_classz00_5419 = BGl_tvecz00zztvector_tvectorz00;
								if (BGL_OBJECTP(BgL_tvz00_3882))
									{	/* Cfa/tvector.scm 554 */
										BgL_objectz00_bglt BgL_arg1807z00_5421;

										BgL_arg1807z00_5421 = (BgL_objectz00_bglt) (BgL_tvz00_3882);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/tvector.scm 554 */
												long BgL_idxz00_5427;

												BgL_idxz00_5427 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5421);
												BgL_test2540z00_7248 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5427 + 2L)) == BgL_classz00_5419);
											}
										else
											{	/* Cfa/tvector.scm 554 */
												bool_t BgL_res2332z00_5452;

												{	/* Cfa/tvector.scm 554 */
													obj_t BgL_oclassz00_5435;

													{	/* Cfa/tvector.scm 554 */
														obj_t BgL_arg1815z00_5443;
														long BgL_arg1816z00_5444;

														BgL_arg1815z00_5443 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/tvector.scm 554 */
															long BgL_arg1817z00_5445;

															BgL_arg1817z00_5445 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5421);
															BgL_arg1816z00_5444 =
																(BgL_arg1817z00_5445 - OBJECT_TYPE);
														}
														BgL_oclassz00_5435 =
															VECTOR_REF(BgL_arg1815z00_5443,
															BgL_arg1816z00_5444);
													}
													{	/* Cfa/tvector.scm 554 */
														bool_t BgL__ortest_1115z00_5436;

														BgL__ortest_1115z00_5436 =
															(BgL_classz00_5419 == BgL_oclassz00_5435);
														if (BgL__ortest_1115z00_5436)
															{	/* Cfa/tvector.scm 554 */
																BgL_res2332z00_5452 = BgL__ortest_1115z00_5436;
															}
														else
															{	/* Cfa/tvector.scm 554 */
																long BgL_odepthz00_5437;

																{	/* Cfa/tvector.scm 554 */
																	obj_t BgL_arg1804z00_5438;

																	BgL_arg1804z00_5438 = (BgL_oclassz00_5435);
																	BgL_odepthz00_5437 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_5438);
																}
																if ((2L < BgL_odepthz00_5437))
																	{	/* Cfa/tvector.scm 554 */
																		obj_t BgL_arg1802z00_5440;

																		{	/* Cfa/tvector.scm 554 */
																			obj_t BgL_arg1803z00_5441;

																			BgL_arg1803z00_5441 =
																				(BgL_oclassz00_5435);
																			BgL_arg1802z00_5440 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_5441, 2L);
																		}
																		BgL_res2332z00_5452 =
																			(BgL_arg1802z00_5440 ==
																			BgL_classz00_5419);
																	}
																else
																	{	/* Cfa/tvector.scm 554 */
																		BgL_res2332z00_5452 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2540z00_7248 = BgL_res2332z00_5452;
											}
									}
								else
									{	/* Cfa/tvector.scm 554 */
										BgL_test2540z00_7248 = ((bool_t) 0);
									}
							}
							if (BgL_test2540z00_7248)
								{	/* Cfa/tvector.scm 555 */
									obj_t BgL_tvzd2ze3listz31_3884;

									{	/* Cfa/tvector.scm 555 */
										obj_t BgL_arg1947z00_3891;

										{	/* Cfa/tvector.scm 555 */
											obj_t BgL_arg1948z00_3892;
											obj_t BgL_arg1949z00_3893;

											{	/* Cfa/tvector.scm 555 */
												obj_t BgL_arg1950z00_3894;

												BgL_arg1950z00_3894 =
													(((BgL_typez00_bglt) COBJECT(
															((BgL_typez00_bglt) BgL_tvz00_3882)))->BgL_idz00);
												{	/* Cfa/tvector.scm 555 */
													obj_t BgL_arg1455z00_5455;

													BgL_arg1455z00_5455 =
														SYMBOL_TO_STRING(BgL_arg1950z00_3894);
													BgL_arg1948z00_3892 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_5455);
												}
											}
											{	/* Cfa/tvector.scm 555 */
												obj_t BgL_symbolz00_5456;

												BgL_symbolz00_5456 = CNST_TABLE_REF(11);
												{	/* Cfa/tvector.scm 555 */
													obj_t BgL_arg1455z00_5457;

													BgL_arg1455z00_5457 =
														SYMBOL_TO_STRING(BgL_symbolz00_5456);
													BgL_arg1949z00_3893 =
														BGl_stringzd2copyzd2zz__r4_strings_6_7z00
														(BgL_arg1455z00_5457);
												}
											}
											BgL_arg1947z00_3891 =
												string_append(BgL_arg1948z00_3892, BgL_arg1949z00_3893);
										}
										BgL_tvzd2ze3listz31_3884 =
											bstring_to_symbol(BgL_arg1947z00_3891);
									}
									{	/* Cfa/tvector.scm 555 */
										BgL_nodez00_bglt BgL_newzd2nodezd2_3885;

										{	/* Cfa/tvector.scm 556 */
											obj_t BgL_arg1943z00_3887;
											obj_t BgL_arg1944z00_3888;

											BgL_arg1943z00_3887 =
												MAKE_YOUNG_PAIR(BgL_tvzd2ze3listz31_3884,
												BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(
													(((BgL_appz00_bglt) COBJECT(BgL_nodez00_91))->
														BgL_argsz00), BNIL));
											BgL_arg1944z00_3888 =
												(((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
															BgL_nodez00_91)))->BgL_locz00);
											BgL_newzd2nodezd2_3885 =
												BGl_sexpzd2ze3nodez31zzast_sexpz00(BgL_arg1943z00_3887,
												BNIL, BgL_arg1944z00_3888, CNST_TABLE_REF(12));
										}
										{	/* Cfa/tvector.scm 556 */

											{	/* Cfa/tvector.scm 560 */
												BgL_typez00_bglt BgL_arg1942z00_3886;

												BgL_arg1942z00_3886 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_nodez00_91)))->
													BgL_typez00);
												((((BgL_nodez00_bglt) COBJECT(BgL_newzd2nodezd2_3885))->
														BgL_typez00) =
													((BgL_typez00_bglt) BgL_arg1942z00_3886), BUNSPEC);
											}
											return BgL_newzd2nodezd2_3885;
										}
									}
								}
							else
								{	/* Cfa/tvector.scm 554 */
									return ((BgL_nodez00_bglt) BgL_nodez00_91);
								}
						}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_tvectorz00(void)
	{
		{	/* Cfa/tvector.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_tvectorz00(void)
	{
		{	/* Cfa/tvector.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_getzd2vectorzd2itemzd2typezd2envz00zzcfa_tvectorz00,
				BGl_proc2359z00zzcfa_tvectorz00, BGl_nodez00zzast_nodez00,
				BGl_string2360z00zzcfa_tvectorz00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_proc2361z00zzcfa_tvectorz00,
				BGl_nodez00zzast_nodez00, BGl_string2362z00zzcfa_tvectorz00);
		}

	}



/* &patch!1645 */
	obj_t BGl_z62patchz121645z70zzcfa_tvectorz00(obj_t BgL_envz00_6105,
		obj_t BgL_nodez00_6106)
	{
		{	/* Cfa/tvector.scm 282 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(13),
				BGl_string2363z00zzcfa_tvectorz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_6106)));
		}

	}



/* &get-vector-item-type1638 */
	obj_t BGl_z62getzd2vectorzd2itemzd2type1638zb0zzcfa_tvectorz00(obj_t
		BgL_envz00_6107, obj_t BgL_nodez00_6108)
	{
		{	/* Cfa/tvector.scm 201 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(14),
				BGl_string2363z00zzcfa_tvectorz00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_6108)));
		}

	}



/* get-vector-item-type */
	BGL_EXPORTED_DEF BgL_typez00_bglt
		BGl_getzd2vectorzd2itemzd2typezd2zzcfa_tvectorz00(BgL_nodez00_bglt
		BgL_nodez00_57)
	{
		{	/* Cfa/tvector.scm 201 */
			{	/* Cfa/tvector.scm 201 */
				obj_t BgL_method1640z00_3905;

				{	/* Cfa/tvector.scm 201 */
					obj_t BgL_res2337z00_5492;

					{	/* Cfa/tvector.scm 201 */
						long BgL_objzd2classzd2numz00_5463;

						BgL_objzd2classzd2numz00_5463 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_57));
						{	/* Cfa/tvector.scm 201 */
							obj_t BgL_arg1811z00_5464;

							BgL_arg1811z00_5464 =
								PROCEDURE_REF
								(BGl_getzd2vectorzd2itemzd2typezd2envz00zzcfa_tvectorz00,
								(int) (1L));
							{	/* Cfa/tvector.scm 201 */
								int BgL_offsetz00_5467;

								BgL_offsetz00_5467 = (int) (BgL_objzd2classzd2numz00_5463);
								{	/* Cfa/tvector.scm 201 */
									long BgL_offsetz00_5468;

									BgL_offsetz00_5468 =
										((long) (BgL_offsetz00_5467) - OBJECT_TYPE);
									{	/* Cfa/tvector.scm 201 */
										long BgL_modz00_5469;

										BgL_modz00_5469 =
											(BgL_offsetz00_5468 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/tvector.scm 201 */
											long BgL_restz00_5471;

											BgL_restz00_5471 =
												(BgL_offsetz00_5468 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/tvector.scm 201 */

												{	/* Cfa/tvector.scm 201 */
													obj_t BgL_bucketz00_5473;

													BgL_bucketz00_5473 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_5464), BgL_modz00_5469);
													BgL_res2337z00_5492 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_5473), BgL_restz00_5471);
					}}}}}}}}
					BgL_method1640z00_3905 = BgL_res2337z00_5492;
				}
				return
					((BgL_typez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1640z00_3905,
						((obj_t) BgL_nodez00_57)));
			}
		}

	}



/* &get-vector-item-type */
	BgL_typez00_bglt BGl_z62getzd2vectorzd2itemzd2typezb0zzcfa_tvectorz00(obj_t
		BgL_envz00_6109, obj_t BgL_nodez00_6110)
	{
		{	/* Cfa/tvector.scm 201 */
			return
				BGl_getzd2vectorzd2itemzd2typezd2zzcfa_tvectorz00(
				((BgL_nodez00_bglt) BgL_nodez00_6110));
		}

	}



/* patch! */
	obj_t BGl_patchz12z12zzcfa_tvectorz00(BgL_nodez00_bglt BgL_nodez00_64)
	{
		{	/* Cfa/tvector.scm 282 */
			{	/* Cfa/tvector.scm 282 */
				obj_t BgL_method1646z00_3906;

				{	/* Cfa/tvector.scm 282 */
					obj_t BgL_res2342z00_5523;

					{	/* Cfa/tvector.scm 282 */
						long BgL_objzd2classzd2numz00_5494;

						BgL_objzd2classzd2numz00_5494 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_64));
						{	/* Cfa/tvector.scm 282 */
							obj_t BgL_arg1811z00_5495;

							BgL_arg1811z00_5495 =
								PROCEDURE_REF(BGl_patchz12zd2envzc0zzcfa_tvectorz00,
								(int) (1L));
							{	/* Cfa/tvector.scm 282 */
								int BgL_offsetz00_5498;

								BgL_offsetz00_5498 = (int) (BgL_objzd2classzd2numz00_5494);
								{	/* Cfa/tvector.scm 282 */
									long BgL_offsetz00_5499;

									BgL_offsetz00_5499 =
										((long) (BgL_offsetz00_5498) - OBJECT_TYPE);
									{	/* Cfa/tvector.scm 282 */
										long BgL_modz00_5500;

										BgL_modz00_5500 =
											(BgL_offsetz00_5499 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/tvector.scm 282 */
											long BgL_restz00_5502;

											BgL_restz00_5502 =
												(BgL_offsetz00_5499 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/tvector.scm 282 */

												{	/* Cfa/tvector.scm 282 */
													obj_t BgL_bucketz00_5504;

													BgL_bucketz00_5504 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_5495), BgL_modz00_5500);
													BgL_res2342z00_5523 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_5504), BgL_restz00_5502);
					}}}}}}}}
					BgL_method1646z00_3906 = BgL_res2342z00_5523;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1646z00_3906, ((obj_t) BgL_nodez00_64));
			}
		}

	}



/* &patch! */
	obj_t BGl_z62patchz12z70zzcfa_tvectorz00(obj_t BgL_envz00_6111,
		obj_t BgL_nodez00_6112)
	{
		{	/* Cfa/tvector.scm 282 */
			return
				BGl_patchz12z12zzcfa_tvectorz00(((BgL_nodez00_bglt) BgL_nodez00_6112));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_tvectorz00(void)
	{
		{	/* Cfa/tvector.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2vectorzd2itemzd2typezd2envz00zzcfa_tvectorz00,
				BGl_makezd2vectorzd2appz00zzcfa_info2z00,
				BGl_proc2364z00zzcfa_tvectorz00, BGl_string2365z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_getzd2vectorzd2itemzd2typezd2envz00zzcfa_tvectorz00,
				BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00,
				BGl_proc2366z00zzcfa_tvectorz00, BGl_string2365z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_atomz00zzast_nodez00,
				BGl_proc2367z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_kwotez00zzast_nodez00,
				BGl_proc2369z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00,
				BGl_kwotezf2nodezf2zzcfa_infoz00, BGl_proc2370z00zzcfa_tvectorz00,
				BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_varz00zzast_nodez00,
				BGl_proc2371z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_closurez00zzast_nodez00,
				BGl_proc2372z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_sequencez00zzast_nodez00,
				BGl_proc2373z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_syncz00zzast_nodez00,
				BGl_proc2374z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc2375z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_funcallz00zzast_nodez00,
				BGl_proc2376z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_externz00zzast_nodez00,
				BGl_proc2377z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_castz00zzast_nodez00,
				BGl_proc2378z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_setqz00zzast_nodez00,
				BGl_proc2379z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_conditionalz00zzast_nodez00,
				BGl_proc2380z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_failz00zzast_nodez00,
				BGl_proc2381z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_switchz00zzast_nodez00,
				BGl_proc2382z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_letzd2funzd2zzast_nodez00,
				BGl_proc2383z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_letzd2varzd2zzast_nodez00,
				BGl_proc2384z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2385z00zzcfa_tvectorz00,
				BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2386z00zzcfa_tvectorz00,
				BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_makezd2boxzd2zzast_nodez00,
				BGl_proc2387z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2388z00zzcfa_tvectorz00,
				BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_boxzd2refzd2zzast_nodez00,
				BGl_proc2389z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_appz00zzast_nodez00,
				BGl_proc2390z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00, BGl_vlengthz00zzast_nodez00,
				BGl_proc2391z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00,
				BGl_makezd2vectorzd2appz00zzcfa_info2z00,
				BGl_proc2392z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00,
				BGl_valloczf2Cinfozb2optimz40zzcfa_info3z00,
				BGl_proc2393z00zzcfa_tvectorz00, BGl_string2368z00zzcfa_tvectorz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00,
				BGl_vrefzf2Cinfozf2zzcfa_info3z00, BGl_proc2394z00zzcfa_tvectorz00,
				BGl_string2368z00zzcfa_tvectorz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_patchz12zd2envzc0zzcfa_tvectorz00,
				BGl_vsetz12zf2Cinfoze0zzcfa_info3z00, BGl_proc2395z00zzcfa_tvectorz00,
				BGl_string2368z00zzcfa_tvectorz00);
		}

	}



/* &patch!-vset!/Cinfo1702 */
	obj_t BGl_z62patchz12zd2vsetz12zf2Cinfo1702z42zzcfa_tvectorz00(obj_t
		BgL_envz00_6143, obj_t BgL_nodez00_6144)
	{
		{	/* Cfa/tvector.scm 629 */
			{
				BgL_nodez00_bglt BgL_auxz00_7396;

				{	/* Cfa/tvector.scm 631 */
					obj_t BgL_arg2101z00_6221;

					BgL_arg2101z00_6221 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vsetz12z12_bglt) BgL_nodez00_6144))))->BgL_exprza2za2);
					BGl_patchza2z12zb0zzcfa_tvectorz00(BgL_arg2101z00_6221);
				}
				{	/* Cfa/tvector.scm 632 */
					bool_t BgL_test2545z00_7401;

					{
						BgL_vsetz12zf2cinfoze0_bglt BgL_auxz00_7402;

						{
							obj_t BgL_auxz00_7403;

							{	/* Cfa/tvector.scm 632 */
								BgL_objectz00_bglt BgL_tmpz00_7404;

								BgL_tmpz00_7404 =
									((BgL_objectz00_bglt)
									((BgL_vsetz12z12_bglt) BgL_nodez00_6144));
								BgL_auxz00_7403 = BGL_OBJECT_WIDENING(BgL_tmpz00_7404);
							}
							BgL_auxz00_7402 = ((BgL_vsetz12zf2cinfoze0_bglt) BgL_auxz00_7403);
						}
						BgL_test2545z00_7401 =
							(((BgL_vsetz12zf2cinfoze0_bglt) COBJECT(BgL_auxz00_7402))->
							BgL_tvectorzf3zf3);
					}
					if (BgL_test2545z00_7401)
						{	/* Cfa/tvector.scm 632 */
							BgL_auxz00_7396 =
								((BgL_nodez00_bglt) ((BgL_vsetz12z12_bglt) BgL_nodez00_6144));
						}
					else
						{	/* Cfa/tvector.scm 634 */
							BgL_approxz00_bglt BgL_veczd2approxzd2_6222;

							{	/* Cfa/tvector.scm 634 */
								obj_t BgL_arg2112z00_6223;

								{	/* Cfa/tvector.scm 634 */
									obj_t BgL_pairz00_6224;

									BgL_pairz00_6224 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt)
													((BgL_vsetz12z12_bglt) BgL_nodez00_6144))))->
										BgL_exprza2za2);
									BgL_arg2112z00_6223 = CAR(BgL_pairz00_6224);
								}
								BgL_veczd2approxzd2_6222 =
									BGl_cfaz12z12zzcfa_cfaz00(
									((BgL_nodez00_bglt) BgL_arg2112z00_6223));
							}
							{	/* Cfa/tvector.scm 634 */
								obj_t BgL_tvz00_6225;

								BgL_tvz00_6225 =
									BGl_getzd2approxzd2typez00zzcfa_typez00
									(BgL_veczd2approxzd2_6222,
									((obj_t) ((BgL_vsetz12z12_bglt) BgL_nodez00_6144)));
								{	/* Cfa/tvector.scm 635 */

									{	/* Cfa/tvector.scm 636 */
										bool_t BgL_test2546z00_7421;

										{	/* Cfa/tvector.scm 636 */
											obj_t BgL_classz00_6226;

											BgL_classz00_6226 = BGl_tvecz00zztvector_tvectorz00;
											if (BGL_OBJECTP(BgL_tvz00_6225))
												{	/* Cfa/tvector.scm 636 */
													BgL_objectz00_bglt BgL_arg1807z00_6227;

													BgL_arg1807z00_6227 =
														(BgL_objectz00_bglt) (BgL_tvz00_6225);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Cfa/tvector.scm 636 */
															long BgL_idxz00_6228;

															BgL_idxz00_6228 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6227);
															BgL_test2546z00_7421 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_6228 + 2L)) == BgL_classz00_6226);
														}
													else
														{	/* Cfa/tvector.scm 636 */
															bool_t BgL_res2350z00_6231;

															{	/* Cfa/tvector.scm 636 */
																obj_t BgL_oclassz00_6232;

																{	/* Cfa/tvector.scm 636 */
																	obj_t BgL_arg1815z00_6233;
																	long BgL_arg1816z00_6234;

																	BgL_arg1815z00_6233 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Cfa/tvector.scm 636 */
																		long BgL_arg1817z00_6235;

																		BgL_arg1817z00_6235 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6227);
																		BgL_arg1816z00_6234 =
																			(BgL_arg1817z00_6235 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_6232 =
																		VECTOR_REF(BgL_arg1815z00_6233,
																		BgL_arg1816z00_6234);
																}
																{	/* Cfa/tvector.scm 636 */
																	bool_t BgL__ortest_1115z00_6236;

																	BgL__ortest_1115z00_6236 =
																		(BgL_classz00_6226 == BgL_oclassz00_6232);
																	if (BgL__ortest_1115z00_6236)
																		{	/* Cfa/tvector.scm 636 */
																			BgL_res2350z00_6231 =
																				BgL__ortest_1115z00_6236;
																		}
																	else
																		{	/* Cfa/tvector.scm 636 */
																			long BgL_odepthz00_6237;

																			{	/* Cfa/tvector.scm 636 */
																				obj_t BgL_arg1804z00_6238;

																				BgL_arg1804z00_6238 =
																					(BgL_oclassz00_6232);
																				BgL_odepthz00_6237 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_6238);
																			}
																			if ((2L < BgL_odepthz00_6237))
																				{	/* Cfa/tvector.scm 636 */
																					obj_t BgL_arg1802z00_6239;

																					{	/* Cfa/tvector.scm 636 */
																						obj_t BgL_arg1803z00_6240;

																						BgL_arg1803z00_6240 =
																							(BgL_oclassz00_6232);
																						BgL_arg1802z00_6239 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_6240, 2L);
																					}
																					BgL_res2350z00_6231 =
																						(BgL_arg1802z00_6239 ==
																						BgL_classz00_6226);
																				}
																			else
																				{	/* Cfa/tvector.scm 636 */
																					BgL_res2350z00_6231 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2546z00_7421 = BgL_res2350z00_6231;
														}
												}
											else
												{	/* Cfa/tvector.scm 636 */
													BgL_test2546z00_7421 = ((bool_t) 0);
												}
										}
										if (BgL_test2546z00_7421)
											{	/* Cfa/tvector.scm 638 */
												obj_t BgL_tvzd2setz12zc0_6241;

												{	/* Cfa/tvector.scm 638 */
													obj_t BgL_arg2108z00_6242;

													{	/* Cfa/tvector.scm 638 */
														obj_t BgL_arg2109z00_6243;
														obj_t BgL_arg2110z00_6244;

														{	/* Cfa/tvector.scm 638 */
															obj_t BgL_arg2111z00_6245;

															BgL_arg2111z00_6245 =
																(((BgL_typez00_bglt) COBJECT(
																		((BgL_typez00_bglt) BgL_tvz00_6225)))->
																BgL_idz00);
															{	/* Cfa/tvector.scm 638 */
																obj_t BgL_arg1455z00_6246;

																BgL_arg1455z00_6246 =
																	SYMBOL_TO_STRING(BgL_arg2111z00_6245);
																BgL_arg2109z00_6243 =
																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																	(BgL_arg1455z00_6246);
															}
														}
														{	/* Cfa/tvector.scm 638 */
															obj_t BgL_symbolz00_6247;

															BgL_symbolz00_6247 = CNST_TABLE_REF(15);
															{	/* Cfa/tvector.scm 638 */
																obj_t BgL_arg1455z00_6248;

																BgL_arg1455z00_6248 =
																	SYMBOL_TO_STRING(BgL_symbolz00_6247);
																BgL_arg2110z00_6244 =
																	BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																	(BgL_arg1455z00_6248);
															}
														}
														BgL_arg2108z00_6242 =
															string_append(BgL_arg2109z00_6243,
															BgL_arg2110z00_6244);
													}
													BgL_tvzd2setz12zc0_6241 =
														bstring_to_symbol(BgL_arg2108z00_6242);
												}
												{	/* Cfa/tvector.scm 638 */
													BgL_nodez00_bglt BgL_newzd2nodezd2_6249;

													{	/* Cfa/tvector.scm 639 */
														obj_t BgL_arg2104z00_6250;
														obj_t BgL_arg2105z00_6251;

														{	/* Cfa/tvector.scm 639 */
															obj_t BgL_arg2106z00_6252;

															BgL_arg2106z00_6252 =
																BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(
																(((BgL_externz00_bglt) COBJECT(
																			((BgL_externz00_bglt)
																				((BgL_vsetz12z12_bglt)
																					BgL_nodez00_6144))))->BgL_exprza2za2),
																BNIL);
															BgL_arg2104z00_6250 =
																MAKE_YOUNG_PAIR(BgL_tvzd2setz12zc0_6241,
																BgL_arg2106z00_6252);
														}
														BgL_arg2105z00_6251 =
															(((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt)
																		((BgL_vsetz12z12_bglt)
																			BgL_nodez00_6144))))->BgL_locz00);
														BgL_newzd2nodezd2_6249 =
															BGl_sexpzd2ze3nodez31zzast_sexpz00
															(BgL_arg2104z00_6250, BNIL, BgL_arg2105z00_6251,
															CNST_TABLE_REF(12));
													}
													{	/* Cfa/tvector.scm 639 */

														BgL_auxz00_7396 =
															BGl_inlinezd2nodezd2zzinline_inlinez00
															(BgL_newzd2nodezd2_6249, 1L, BNIL);
													}
												}
											}
										else
											{	/* Cfa/tvector.scm 636 */
												BgL_auxz00_7396 =
													((BgL_nodez00_bglt)
													((BgL_vsetz12z12_bglt) BgL_nodez00_6144));
											}
									}
								}
							}
						}
				}
				return ((obj_t) BgL_auxz00_7396);
			}
		}

	}



/* &patch!-vref/Cinfo1700 */
	obj_t BGl_z62patchz12zd2vrefzf2Cinfo1700z50zzcfa_tvectorz00(obj_t
		BgL_envz00_6145, obj_t BgL_nodez00_6146)
	{
		{	/* Cfa/tvector.scm 606 */
			{
				BgL_nodez00_bglt BgL_auxz00_7467;

				{	/* Cfa/tvector.scm 608 */
					obj_t BgL_arg2084z00_6254;

					BgL_arg2084z00_6254 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vrefz00_bglt) BgL_nodez00_6146))))->BgL_exprza2za2);
					BGl_patchza2z12zb0zzcfa_tvectorz00(BgL_arg2084z00_6254);
				}
				{	/* Cfa/tvector.scm 609 */
					bool_t BgL_test2551z00_7472;

					{
						BgL_vrefzf2cinfozf2_bglt BgL_auxz00_7473;

						{
							obj_t BgL_auxz00_7474;

							{	/* Cfa/tvector.scm 609 */
								BgL_objectz00_bglt BgL_tmpz00_7475;

								BgL_tmpz00_7475 =
									((BgL_objectz00_bglt) ((BgL_vrefz00_bglt) BgL_nodez00_6146));
								BgL_auxz00_7474 = BGL_OBJECT_WIDENING(BgL_tmpz00_7475);
							}
							BgL_auxz00_7473 = ((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_7474);
						}
						BgL_test2551z00_7472 =
							(((BgL_vrefzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7473))->
							BgL_tvectorzf3zf3);
					}
					if (BgL_test2551z00_7472)
						{	/* Cfa/tvector.scm 610 */
							BgL_typez00_bglt BgL_tyz00_6255;

							BgL_tyz00_6255 =
								(((BgL_vrefz00_bglt) COBJECT(
										((BgL_vrefz00_bglt)
											((BgL_vrefz00_bglt) BgL_nodez00_6146))))->BgL_ftypez00);
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_vrefz00_bglt) BgL_nodez00_6146))))->BgL_typez00) =
								((BgL_typez00_bglt) BgL_tyz00_6255), BUNSPEC);
							BgL_auxz00_7467 =
								((BgL_nodez00_bglt) ((BgL_vrefz00_bglt) BgL_nodez00_6146));
						}
					else
						{	/* Cfa/tvector.scm 613 */
							BgL_approxz00_bglt BgL_veczd2approxzd2_6256;

							{	/* Cfa/tvector.scm 613 */
								obj_t BgL_arg2099z00_6257;

								{	/* Cfa/tvector.scm 613 */
									obj_t BgL_pairz00_6258;

									BgL_pairz00_6258 =
										(((BgL_externz00_bglt) COBJECT(
												((BgL_externz00_bglt)
													((BgL_vrefz00_bglt) BgL_nodez00_6146))))->
										BgL_exprza2za2);
									BgL_arg2099z00_6257 = CAR(BgL_pairz00_6258);
								}
								BgL_veczd2approxzd2_6256 =
									BGl_cfaz12z12zzcfa_cfaz00(
									((BgL_nodez00_bglt) BgL_arg2099z00_6257));
							}
							{	/* Cfa/tvector.scm 613 */
								obj_t BgL_tvz00_6259;

								BgL_tvz00_6259 =
									BGl_getzd2approxzd2typez00zzcfa_typez00
									(BgL_veczd2approxzd2_6256,
									((obj_t) ((BgL_vrefz00_bglt) BgL_nodez00_6146)));
								{	/* Cfa/tvector.scm 614 */

									{	/* Cfa/tvector.scm 615 */
										bool_t BgL_test2552z00_7498;

										{	/* Cfa/tvector.scm 615 */
											obj_t BgL_classz00_6260;

											BgL_classz00_6260 = BGl_tvecz00zztvector_tvectorz00;
											if (BGL_OBJECTP(BgL_tvz00_6259))
												{	/* Cfa/tvector.scm 615 */
													BgL_objectz00_bglt BgL_arg1807z00_6261;

													BgL_arg1807z00_6261 =
														(BgL_objectz00_bglt) (BgL_tvz00_6259);
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Cfa/tvector.scm 615 */
															long BgL_idxz00_6262;

															BgL_idxz00_6262 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6261);
															BgL_test2552z00_7498 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_6262 + 2L)) == BgL_classz00_6260);
														}
													else
														{	/* Cfa/tvector.scm 615 */
															bool_t BgL_res2349z00_6265;

															{	/* Cfa/tvector.scm 615 */
																obj_t BgL_oclassz00_6266;

																{	/* Cfa/tvector.scm 615 */
																	obj_t BgL_arg1815z00_6267;
																	long BgL_arg1816z00_6268;

																	BgL_arg1815z00_6267 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Cfa/tvector.scm 615 */
																		long BgL_arg1817z00_6269;

																		BgL_arg1817z00_6269 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6261);
																		BgL_arg1816z00_6268 =
																			(BgL_arg1817z00_6269 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_6266 =
																		VECTOR_REF(BgL_arg1815z00_6267,
																		BgL_arg1816z00_6268);
																}
																{	/* Cfa/tvector.scm 615 */
																	bool_t BgL__ortest_1115z00_6270;

																	BgL__ortest_1115z00_6270 =
																		(BgL_classz00_6260 == BgL_oclassz00_6266);
																	if (BgL__ortest_1115z00_6270)
																		{	/* Cfa/tvector.scm 615 */
																			BgL_res2349z00_6265 =
																				BgL__ortest_1115z00_6270;
																		}
																	else
																		{	/* Cfa/tvector.scm 615 */
																			long BgL_odepthz00_6271;

																			{	/* Cfa/tvector.scm 615 */
																				obj_t BgL_arg1804z00_6272;

																				BgL_arg1804z00_6272 =
																					(BgL_oclassz00_6266);
																				BgL_odepthz00_6271 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_6272);
																			}
																			if ((2L < BgL_odepthz00_6271))
																				{	/* Cfa/tvector.scm 615 */
																					obj_t BgL_arg1802z00_6273;

																					{	/* Cfa/tvector.scm 615 */
																						obj_t BgL_arg1803z00_6274;

																						BgL_arg1803z00_6274 =
																							(BgL_oclassz00_6266);
																						BgL_arg1802z00_6273 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_6274, 2L);
																					}
																					BgL_res2349z00_6265 =
																						(BgL_arg1802z00_6273 ==
																						BgL_classz00_6260);
																				}
																			else
																				{	/* Cfa/tvector.scm 615 */
																					BgL_res2349z00_6265 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2552z00_7498 = BgL_res2349z00_6265;
														}
												}
											else
												{	/* Cfa/tvector.scm 615 */
													BgL_test2552z00_7498 = ((bool_t) 0);
												}
										}
										if (BgL_test2552z00_7498)
											{	/* Cfa/tvector.scm 620 */
												obj_t BgL_tyz00_6275;

												{	/* Cfa/tvector.scm 620 */
													BgL_approxz00_bglt BgL_arg2097z00_6276;

													{
														BgL_vrefzf2cinfozf2_bglt BgL_auxz00_7521;

														{
															obj_t BgL_auxz00_7522;

															{	/* Cfa/tvector.scm 620 */
																BgL_objectz00_bglt BgL_tmpz00_7523;

																BgL_tmpz00_7523 =
																	((BgL_objectz00_bglt)
																	((BgL_vrefz00_bglt) BgL_nodez00_6146));
																BgL_auxz00_7522 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_7523);
															}
															BgL_auxz00_7521 =
																((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_7522);
														}
														BgL_arg2097z00_6276 =
															(((BgL_vrefzf2cinfozf2_bglt)
																COBJECT(BgL_auxz00_7521))->BgL_approxz00);
													}
													BgL_tyz00_6275 =
														BGl_getzd2approxzd2typez00zzcfa_typez00
														(BgL_arg2097z00_6276,
														((obj_t) ((BgL_vrefz00_bglt) BgL_nodez00_6146)));
												}
												{	/* Cfa/tvector.scm 620 */
													obj_t BgL_tvzd2refzd2_6277;

													{	/* Cfa/tvector.scm 621 */
														obj_t BgL_arg2093z00_6278;

														{	/* Cfa/tvector.scm 621 */
															obj_t BgL_arg2094z00_6279;
															obj_t BgL_arg2095z00_6280;

															{	/* Cfa/tvector.scm 621 */
																obj_t BgL_arg2096z00_6281;

																BgL_arg2096z00_6281 =
																	(((BgL_typez00_bglt) COBJECT(
																			((BgL_typez00_bglt) BgL_tvz00_6259)))->
																	BgL_idz00);
																{	/* Cfa/tvector.scm 621 */
																	obj_t BgL_arg1455z00_6282;

																	BgL_arg1455z00_6282 =
																		SYMBOL_TO_STRING(BgL_arg2096z00_6281);
																	BgL_arg2094z00_6279 =
																		BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																		(BgL_arg1455z00_6282);
																}
															}
															{	/* Cfa/tvector.scm 621 */
																obj_t BgL_symbolz00_6283;

																BgL_symbolz00_6283 = CNST_TABLE_REF(16);
																{	/* Cfa/tvector.scm 621 */
																	obj_t BgL_arg1455z00_6284;

																	BgL_arg1455z00_6284 =
																		SYMBOL_TO_STRING(BgL_symbolz00_6283);
																	BgL_arg2095z00_6280 =
																		BGl_stringzd2copyzd2zz__r4_strings_6_7z00
																		(BgL_arg1455z00_6284);
																}
															}
															BgL_arg2093z00_6278 =
																string_append(BgL_arg2094z00_6279,
																BgL_arg2095z00_6280);
														}
														BgL_tvzd2refzd2_6277 =
															bstring_to_symbol(BgL_arg2093z00_6278);
													}
													{	/* Cfa/tvector.scm 621 */
														BgL_nodez00_bglt BgL_newzd2nodezd2_6285;

														{	/* Cfa/tvector.scm 622 */
															obj_t BgL_arg2088z00_6286;
															obj_t BgL_arg2089z00_6287;

															{	/* Cfa/tvector.scm 622 */
																obj_t BgL_arg2090z00_6288;

																BgL_arg2090z00_6288 =
																	BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00
																	((((BgL_externz00_bglt)
																			COBJECT(((BgL_externz00_bglt) (
																						(BgL_vrefz00_bglt)
																						BgL_nodez00_6146))))->
																		BgL_exprza2za2), BNIL);
																BgL_arg2088z00_6286 =
																	MAKE_YOUNG_PAIR(BgL_tvzd2refzd2_6277,
																	BgL_arg2090z00_6288);
															}
															BgL_arg2089z00_6287 =
																(((BgL_nodez00_bglt) COBJECT(
																		((BgL_nodez00_bglt)
																			((BgL_vrefz00_bglt) BgL_nodez00_6146))))->
																BgL_locz00);
															BgL_newzd2nodezd2_6285 =
																BGl_sexpzd2ze3nodez31zzast_sexpz00
																(BgL_arg2088z00_6286, BNIL, BgL_arg2089z00_6287,
																CNST_TABLE_REF(12));
														}
														{	/* Cfa/tvector.scm 622 */

															{	/* Cfa/tvector.scm 623 */
																BgL_typez00_bglt BgL_arg2087z00_6289;

																BgL_arg2087z00_6289 =
																	BGl_strictzd2nodezd2typez00zzast_nodez00(
																	((BgL_typez00_bglt) BgL_tvz00_6259),
																	((BgL_typez00_bglt) BgL_tyz00_6275));
																((((BgL_nodez00_bglt)
																			COBJECT(BgL_newzd2nodezd2_6285))->
																		BgL_typez00) =
																	((BgL_typez00_bglt) BgL_arg2087z00_6289),
																	BUNSPEC);
															}
															BgL_auxz00_7467 =
																BGl_inlinezd2nodezd2zzinline_inlinez00
																(BgL_newzd2nodezd2_6285, 1L, BNIL);
														}
													}
												}
											}
										else
											{	/* Cfa/tvector.scm 616 */
												obj_t BgL_tyz00_6290;

												{	/* Cfa/tvector.scm 616 */
													BgL_approxz00_bglt BgL_arg2098z00_6291;

													{
														BgL_vrefzf2cinfozf2_bglt BgL_auxz00_7556;

														{
															obj_t BgL_auxz00_7557;

															{	/* Cfa/tvector.scm 616 */
																BgL_objectz00_bglt BgL_tmpz00_7558;

																BgL_tmpz00_7558 =
																	((BgL_objectz00_bglt)
																	((BgL_vrefz00_bglt) BgL_nodez00_6146));
																BgL_auxz00_7557 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_7558);
															}
															BgL_auxz00_7556 =
																((BgL_vrefzf2cinfozf2_bglt) BgL_auxz00_7557);
														}
														BgL_arg2098z00_6291 =
															(((BgL_vrefzf2cinfozf2_bglt)
																COBJECT(BgL_auxz00_7556))->BgL_approxz00);
													}
													BgL_tyz00_6290 =
														BGl_getzd2approxzd2typez00zzcfa_typez00
														(BgL_arg2098z00_6291,
														((obj_t) ((BgL_vrefz00_bglt) BgL_nodez00_6146)));
												}
												((((BgL_vrefz00_bglt) COBJECT(
																((BgL_vrefz00_bglt)
																	((BgL_vrefz00_bglt) BgL_nodez00_6146))))->
														BgL_ftypez00) =
													((BgL_typez00_bglt) ((BgL_typez00_bglt)
															BgL_tyz00_6290)), BUNSPEC);
												((((BgL_nodez00_bglt)
															COBJECT(((BgL_nodez00_bglt) ((BgL_vrefz00_bglt)
																		BgL_nodez00_6146))))->BgL_typez00) =
													((BgL_typez00_bglt) ((BgL_typez00_bglt)
															BgL_tyz00_6290)), BUNSPEC);
												BgL_auxz00_7467 =
													((BgL_nodez00_bglt) ((BgL_vrefz00_bglt)
														BgL_nodez00_6146));
											}
									}
								}
							}
						}
				}
				return ((obj_t) BgL_auxz00_7467);
			}
		}

	}



/* &patch!-valloc/Cinfo+1698 */
	obj_t BGl_z62patchz12zd2valloczf2Cinfozb21698ze2zzcfa_tvectorz00(obj_t
		BgL_envz00_6147, obj_t BgL_nodez00_6148)
	{
		{	/* Cfa/tvector.scm 585 */
			{
				BgL_nodez00_bglt BgL_auxz00_7578;

				{	/* Cfa/tvector.scm 587 */
					obj_t BgL_arg2068z00_6293;

					BgL_arg2068z00_6293 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vallocz00_bglt) BgL_nodez00_6148))))->BgL_exprza2za2);
					BGl_patchza2z12zb0zzcfa_tvectorz00(BgL_arg2068z00_6293);
				}
				{	/* Cfa/tvector.scm 588 */
					BgL_typez00_bglt BgL_typez00_6294;

					{	/* Cfa/tvector.scm 588 */
						BgL_approxz00_bglt BgL_arg2083z00_6295;

						{
							BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_7583;

							{
								obj_t BgL_auxz00_7584;

								{	/* Cfa/tvector.scm 588 */
									BgL_objectz00_bglt BgL_tmpz00_7585;

									BgL_tmpz00_7585 =
										((BgL_objectz00_bglt)
										((BgL_vallocz00_bglt) BgL_nodez00_6148));
									BgL_auxz00_7584 = BGL_OBJECT_WIDENING(BgL_tmpz00_7585);
								}
								BgL_auxz00_7583 =
									((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_7584);
							}
							BgL_arg2083z00_6295 =
								(((BgL_valloczf2cinfozb2optimz40_bglt)
									COBJECT(BgL_auxz00_7583))->BgL_valuezd2approxzd2);
						}
						BgL_typez00_6294 =
							(((BgL_approxz00_bglt) COBJECT(BgL_arg2083z00_6295))->
							BgL_typez00);
					}
					{	/* Cfa/tvector.scm 588 */
						obj_t BgL_tvz00_6296;

						BgL_tvz00_6296 =
							(((BgL_typez00_bglt) COBJECT(BgL_typez00_6294))->BgL_tvectorz00);
						{	/* Cfa/tvector.scm 589 */

							{	/* Cfa/tvector.scm 590 */
								bool_t BgL_test2557z00_7593;

								{	/* Cfa/tvector.scm 590 */
									bool_t BgL_test2558z00_7594;

									{	/* Cfa/tvector.scm 590 */
										obj_t BgL_classz00_6297;

										BgL_classz00_6297 = BGl_typez00zztype_typez00;
										if (BGL_OBJECTP(BgL_tvz00_6296))
											{	/* Cfa/tvector.scm 590 */
												BgL_objectz00_bglt BgL_arg1807z00_6298;

												BgL_arg1807z00_6298 =
													(BgL_objectz00_bglt) (BgL_tvz00_6296);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Cfa/tvector.scm 590 */
														long BgL_idxz00_6299;

														BgL_idxz00_6299 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6298);
														BgL_test2558z00_7594 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_6299 + 1L)) == BgL_classz00_6297);
													}
												else
													{	/* Cfa/tvector.scm 590 */
														bool_t BgL_res2348z00_6302;

														{	/* Cfa/tvector.scm 590 */
															obj_t BgL_oclassz00_6303;

															{	/* Cfa/tvector.scm 590 */
																obj_t BgL_arg1815z00_6304;
																long BgL_arg1816z00_6305;

																BgL_arg1815z00_6304 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Cfa/tvector.scm 590 */
																	long BgL_arg1817z00_6306;

																	BgL_arg1817z00_6306 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6298);
																	BgL_arg1816z00_6305 =
																		(BgL_arg1817z00_6306 - OBJECT_TYPE);
																}
																BgL_oclassz00_6303 =
																	VECTOR_REF(BgL_arg1815z00_6304,
																	BgL_arg1816z00_6305);
															}
															{	/* Cfa/tvector.scm 590 */
																bool_t BgL__ortest_1115z00_6307;

																BgL__ortest_1115z00_6307 =
																	(BgL_classz00_6297 == BgL_oclassz00_6303);
																if (BgL__ortest_1115z00_6307)
																	{	/* Cfa/tvector.scm 590 */
																		BgL_res2348z00_6302 =
																			BgL__ortest_1115z00_6307;
																	}
																else
																	{	/* Cfa/tvector.scm 590 */
																		long BgL_odepthz00_6308;

																		{	/* Cfa/tvector.scm 590 */
																			obj_t BgL_arg1804z00_6309;

																			BgL_arg1804z00_6309 =
																				(BgL_oclassz00_6303);
																			BgL_odepthz00_6308 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_6309);
																		}
																		if ((1L < BgL_odepthz00_6308))
																			{	/* Cfa/tvector.scm 590 */
																				obj_t BgL_arg1802z00_6310;

																				{	/* Cfa/tvector.scm 590 */
																					obj_t BgL_arg1803z00_6311;

																					BgL_arg1803z00_6311 =
																						(BgL_oclassz00_6303);
																					BgL_arg1802z00_6310 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_6311, 1L);
																				}
																				BgL_res2348z00_6302 =
																					(BgL_arg1802z00_6310 ==
																					BgL_classz00_6297);
																			}
																		else
																			{	/* Cfa/tvector.scm 590 */
																				BgL_res2348z00_6302 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2558z00_7594 = BgL_res2348z00_6302;
													}
											}
										else
											{	/* Cfa/tvector.scm 590 */
												BgL_test2558z00_7594 = ((bool_t) 0);
											}
									}
									if (BgL_test2558z00_7594)
										{	/* Cfa/tvector.scm 590 */
											BgL_typez00_bglt BgL_arg2082z00_6312;

											BgL_arg2082z00_6312 =
												(((BgL_vallocz00_bglt) COBJECT(
														((BgL_vallocz00_bglt)
															((BgL_vallocz00_bglt) BgL_nodez00_6148))))->
												BgL_ftypez00);
											BgL_test2557z00_7593 =
												(((obj_t) BgL_arg2082z00_6312) ==
												BGl_za2_za2z00zztype_cachez00);
										}
									else
										{	/* Cfa/tvector.scm 590 */
											BgL_test2557z00_7593 = ((bool_t) 0);
										}
								}
								if (BgL_test2557z00_7593)
									{	/* Cfa/tvector.scm 591 */
										obj_t BgL_createzd2tvzd2_6313;

										{	/* Cfa/tvector.scm 591 */
											obj_t BgL_arg2077z00_6314;

											{	/* Cfa/tvector.scm 591 */
												obj_t BgL_arg2078z00_6315;
												obj_t BgL_arg2079z00_6316;

												{	/* Cfa/tvector.scm 591 */
													obj_t BgL_symbolz00_6317;

													BgL_symbolz00_6317 = CNST_TABLE_REF(17);
													{	/* Cfa/tvector.scm 591 */
														obj_t BgL_arg1455z00_6318;

														BgL_arg1455z00_6318 =
															SYMBOL_TO_STRING(BgL_symbolz00_6317);
														BgL_arg2078z00_6315 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_6318);
													}
												}
												{	/* Cfa/tvector.scm 591 */
													obj_t BgL_arg2080z00_6319;

													BgL_arg2080z00_6319 =
														(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt) BgL_tvz00_6296)))->
														BgL_idz00);
													{	/* Cfa/tvector.scm 591 */
														obj_t BgL_arg1455z00_6320;

														BgL_arg1455z00_6320 =
															SYMBOL_TO_STRING(BgL_arg2080z00_6319);
														BgL_arg2079z00_6316 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_6320);
													}
												}
												BgL_arg2077z00_6314 =
													string_append(BgL_arg2078z00_6315,
													BgL_arg2079z00_6316);
											}
											BgL_createzd2tvzd2_6313 =
												bstring_to_symbol(BgL_arg2077z00_6314);
										}
										{	/* Cfa/tvector.scm 591 */
											BgL_nodez00_bglt BgL_newzd2nodezd2_6321;

											{	/* Cfa/tvector.scm 592 */
												obj_t BgL_arg2072z00_6322;
												obj_t BgL_arg2074z00_6323;

												{	/* Cfa/tvector.scm 592 */
													obj_t BgL_arg2075z00_6324;

													BgL_arg2075z00_6324 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(
														(((BgL_externz00_bglt) COBJECT(
																	((BgL_externz00_bglt)
																		((BgL_vallocz00_bglt) BgL_nodez00_6148))))->
															BgL_exprza2za2), BNIL);
													BgL_arg2072z00_6322 =
														MAKE_YOUNG_PAIR(BgL_createzd2tvzd2_6313,
														BgL_arg2075z00_6324);
												}
												BgL_arg2074z00_6323 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																((BgL_vallocz00_bglt) BgL_nodez00_6148))))->
													BgL_locz00);
												BgL_newzd2nodezd2_6321 =
													BGl_sexpzd2ze3nodez31zzast_sexpz00
													(BgL_arg2072z00_6322, BNIL, BgL_arg2074z00_6323,
													CNST_TABLE_REF(12));
											}
											{	/* Cfa/tvector.scm 592 */

												{	/* Cfa/tvector.scm 596 */
													BgL_nodez00_bglt BgL_nz00_6325;

													BgL_nz00_6325 =
														BGl_inlinezd2nodezd2zzinline_inlinez00
														(BgL_newzd2nodezd2_6321, 1L, BNIL);
													BGl_lvtypezd2nodez12zc0zzast_lvtypez00(BgL_nz00_6325);
													BgL_auxz00_7578 = BgL_nz00_6325;
												}
											}
										}
									}
								else
									{	/* Cfa/tvector.scm 590 */
										{
											BgL_typez00_bglt BgL_auxz00_7643;

											{	/* Cfa/tvector.scm 600 */
												BgL_approxz00_bglt BgL_arg2081z00_6326;

												{
													BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_7646;

													{
														obj_t BgL_auxz00_7647;

														{	/* Cfa/tvector.scm 600 */
															BgL_objectz00_bglt BgL_tmpz00_7648;

															BgL_tmpz00_7648 =
																((BgL_objectz00_bglt)
																((BgL_vallocz00_bglt) BgL_nodez00_6148));
															BgL_auxz00_7647 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_7648);
														}
														BgL_auxz00_7646 =
															((BgL_valloczf2cinfozb2optimz40_bglt)
															BgL_auxz00_7647);
													}
													BgL_arg2081z00_6326 =
														(((BgL_valloczf2cinfozb2optimz40_bglt)
															COBJECT(BgL_auxz00_7646))->BgL_valuezd2approxzd2);
												}
												BgL_auxz00_7643 =
													((BgL_typez00_bglt)
													BGl_getzd2approxzd2typez00zzcfa_typez00
													(BgL_arg2081z00_6326,
														((obj_t) ((BgL_vallocz00_bglt) BgL_nodez00_6148))));
											}
											((((BgL_vallocz00_bglt) COBJECT(
															((BgL_vallocz00_bglt)
																((BgL_vallocz00_bglt) BgL_nodez00_6148))))->
													BgL_ftypez00) =
												((BgL_typez00_bglt) BgL_auxz00_7643), BUNSPEC);
										}
										BgL_auxz00_7578 =
											((BgL_nodez00_bglt)
											((BgL_vallocz00_bglt) BgL_nodez00_6148));
									}
							}
						}
					}
				}
				return ((obj_t) BgL_auxz00_7578);
			}
		}

	}



/* &patch!-make-vector-a1696 */
	obj_t BGl_z62patchz12zd2makezd2vectorzd2a1696za2zzcfa_tvectorz00(obj_t
		BgL_envz00_6149, obj_t BgL_nodez00_6150)
	{
		{	/* Cfa/tvector.scm 567 */
			{
				BgL_nodez00_bglt BgL_auxz00_7662;

				{	/* Cfa/tvector.scm 569 */
					obj_t BgL_arg2055z00_6328;

					BgL_arg2055z00_6328 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_6150))))->BgL_argsz00);
					BGl_patchza2z12zb0zzcfa_tvectorz00(BgL_arg2055z00_6328);
				}
				{	/* Cfa/tvector.scm 570 */
					BgL_typez00_bglt BgL_typez00_6329;

					{	/* Cfa/tvector.scm 570 */
						BgL_approxz00_bglt BgL_arg2067z00_6330;

						{
							BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_7667;

							{
								obj_t BgL_auxz00_7668;

								{	/* Cfa/tvector.scm 570 */
									BgL_objectz00_bglt BgL_tmpz00_7669;

									BgL_tmpz00_7669 =
										((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_6150));
									BgL_auxz00_7668 = BGL_OBJECT_WIDENING(BgL_tmpz00_7669);
								}
								BgL_auxz00_7667 =
									((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_7668);
							}
							BgL_arg2067z00_6330 =
								(((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_7667))->
								BgL_valuezd2approxzd2);
						}
						BgL_typez00_6329 =
							(((BgL_approxz00_bglt) COBJECT(BgL_arg2067z00_6330))->
							BgL_typez00);
					}
					{	/* Cfa/tvector.scm 570 */
						obj_t BgL_tvz00_6331;

						BgL_tvz00_6331 =
							(((BgL_typez00_bglt) COBJECT(BgL_typez00_6329))->BgL_tvectorz00);
						{	/* Cfa/tvector.scm 571 */

							{	/* Cfa/tvector.scm 572 */
								bool_t BgL_test2563z00_7677;

								{	/* Cfa/tvector.scm 572 */
									bool_t BgL_test2564z00_7678;

									{	/* Cfa/tvector.scm 572 */
										obj_t BgL_classz00_6332;

										BgL_classz00_6332 = BGl_typez00zztype_typez00;
										if (BGL_OBJECTP(BgL_tvz00_6331))
											{	/* Cfa/tvector.scm 572 */
												BgL_objectz00_bglt BgL_arg1807z00_6333;

												BgL_arg1807z00_6333 =
													(BgL_objectz00_bglt) (BgL_tvz00_6331);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Cfa/tvector.scm 572 */
														long BgL_idxz00_6334;

														BgL_idxz00_6334 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6333);
														BgL_test2564z00_7678 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_6334 + 1L)) == BgL_classz00_6332);
													}
												else
													{	/* Cfa/tvector.scm 572 */
														bool_t BgL_res2347z00_6337;

														{	/* Cfa/tvector.scm 572 */
															obj_t BgL_oclassz00_6338;

															{	/* Cfa/tvector.scm 572 */
																obj_t BgL_arg1815z00_6339;
																long BgL_arg1816z00_6340;

																BgL_arg1815z00_6339 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Cfa/tvector.scm 572 */
																	long BgL_arg1817z00_6341;

																	BgL_arg1817z00_6341 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6333);
																	BgL_arg1816z00_6340 =
																		(BgL_arg1817z00_6341 - OBJECT_TYPE);
																}
																BgL_oclassz00_6338 =
																	VECTOR_REF(BgL_arg1815z00_6339,
																	BgL_arg1816z00_6340);
															}
															{	/* Cfa/tvector.scm 572 */
																bool_t BgL__ortest_1115z00_6342;

																BgL__ortest_1115z00_6342 =
																	(BgL_classz00_6332 == BgL_oclassz00_6338);
																if (BgL__ortest_1115z00_6342)
																	{	/* Cfa/tvector.scm 572 */
																		BgL_res2347z00_6337 =
																			BgL__ortest_1115z00_6342;
																	}
																else
																	{	/* Cfa/tvector.scm 572 */
																		long BgL_odepthz00_6343;

																		{	/* Cfa/tvector.scm 572 */
																			obj_t BgL_arg1804z00_6344;

																			BgL_arg1804z00_6344 =
																				(BgL_oclassz00_6338);
																			BgL_odepthz00_6343 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_6344);
																		}
																		if ((1L < BgL_odepthz00_6343))
																			{	/* Cfa/tvector.scm 572 */
																				obj_t BgL_arg1802z00_6345;

																				{	/* Cfa/tvector.scm 572 */
																					obj_t BgL_arg1803z00_6346;

																					BgL_arg1803z00_6346 =
																						(BgL_oclassz00_6338);
																					BgL_arg1802z00_6345 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_6346, 1L);
																				}
																				BgL_res2347z00_6337 =
																					(BgL_arg1802z00_6345 ==
																					BgL_classz00_6332);
																			}
																		else
																			{	/* Cfa/tvector.scm 572 */
																				BgL_res2347z00_6337 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2564z00_7678 = BgL_res2347z00_6337;
													}
											}
										else
											{	/* Cfa/tvector.scm 572 */
												BgL_test2564z00_7678 = ((bool_t) 0);
											}
									}
									if (BgL_test2564z00_7678)
										{
											BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_7701;

											{
												obj_t BgL_auxz00_7702;

												{	/* Cfa/tvector.scm 572 */
													BgL_objectz00_bglt BgL_tmpz00_7703;

													BgL_tmpz00_7703 =
														((BgL_objectz00_bglt)
														((BgL_appz00_bglt) BgL_nodez00_6150));
													BgL_auxz00_7702 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_7703);
												}
												BgL_auxz00_7701 =
													((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_7702);
											}
											BgL_test2563z00_7677 =
												(((BgL_makezd2vectorzd2appz00_bglt)
													COBJECT(BgL_auxz00_7701))->BgL_tvectorzf3zf3);
										}
									else
										{	/* Cfa/tvector.scm 572 */
											BgL_test2563z00_7677 = ((bool_t) 0);
										}
								}
								if (BgL_test2563z00_7677)
									{	/* Cfa/tvector.scm 573 */
										obj_t BgL_makezd2tvzd2_6347;

										{	/* Cfa/tvector.scm 573 */
											obj_t BgL_arg2062z00_6348;

											{	/* Cfa/tvector.scm 573 */
												obj_t BgL_arg2063z00_6349;
												obj_t BgL_arg2064z00_6350;

												{	/* Cfa/tvector.scm 573 */
													obj_t BgL_symbolz00_6351;

													BgL_symbolz00_6351 = CNST_TABLE_REF(18);
													{	/* Cfa/tvector.scm 573 */
														obj_t BgL_arg1455z00_6352;

														BgL_arg1455z00_6352 =
															SYMBOL_TO_STRING(BgL_symbolz00_6351);
														BgL_arg2063z00_6349 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_6352);
													}
												}
												{	/* Cfa/tvector.scm 573 */
													obj_t BgL_arg2065z00_6353;

													BgL_arg2065z00_6353 =
														(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt) BgL_tvz00_6331)))->
														BgL_idz00);
													{	/* Cfa/tvector.scm 573 */
														obj_t BgL_arg1455z00_6354;

														BgL_arg1455z00_6354 =
															SYMBOL_TO_STRING(BgL_arg2065z00_6353);
														BgL_arg2064z00_6350 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_6354);
													}
												}
												BgL_arg2062z00_6348 =
													string_append(BgL_arg2063z00_6349,
													BgL_arg2064z00_6350);
											}
											BgL_makezd2tvzd2_6347 =
												bstring_to_symbol(BgL_arg2062z00_6348);
										}
										{	/* Cfa/tvector.scm 573 */
											BgL_nodez00_bglt BgL_newzd2nodezd2_6355;

											{	/* Cfa/tvector.scm 574 */
												obj_t BgL_arg2058z00_6356;
												obj_t BgL_arg2059z00_6357;

												{	/* Cfa/tvector.scm 574 */
													obj_t BgL_arg2060z00_6358;

													BgL_arg2060z00_6358 =
														BGl_eappendzd22zd2zz__r4_pairs_and_lists_6_3z00(
														(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_6150))))->
															BgL_argsz00), BNIL);
													BgL_arg2058z00_6356 =
														MAKE_YOUNG_PAIR(BgL_makezd2tvzd2_6347,
														BgL_arg2060z00_6358);
												}
												BgL_arg2059z00_6357 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																((BgL_appz00_bglt) BgL_nodez00_6150))))->
													BgL_locz00);
												BgL_newzd2nodezd2_6355 =
													BGl_sexpzd2ze3nodez31zzast_sexpz00
													(BgL_arg2058z00_6356, BNIL, BgL_arg2059z00_6357,
													CNST_TABLE_REF(12));
											}
											{	/* Cfa/tvector.scm 574 */

												((((BgL_nodez00_bglt) COBJECT(BgL_newzd2nodezd2_6355))->
														BgL_typez00) =
													((BgL_typez00_bglt) ((BgL_typez00_bglt)
															BgL_tvz00_6331)), BUNSPEC);
												BgL_auxz00_7662 =
													BGl_inlinezd2nodezd2zzinline_inlinez00
													(BgL_newzd2nodezd2_6355, 1L, BNIL);
											}
										}
									}
								else
									{	/* Cfa/tvector.scm 572 */
										BgL_auxz00_7662 =
											((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_6150));
									}
							}
						}
					}
				}
				return ((obj_t) BgL_auxz00_7662);
			}
		}

	}



/* &patch!-vlength1694 */
	obj_t BGl_z62patchz12zd2vlength1694za2zzcfa_tvectorz00(obj_t BgL_envz00_6151,
		obj_t BgL_nodez00_6152)
	{
		{	/* Cfa/tvector.scm 516 */
			{
				BgL_nodez00_bglt BgL_auxz00_7734;

				{	/* Cfa/tvector.scm 518 */
					obj_t BgL_arg2037z00_6360;

					BgL_arg2037z00_6360 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vlengthz00_bglt)
										((BgL_vlengthz00_bglt) BgL_nodez00_6152)))))->
						BgL_exprza2za2);
					BGl_patchza2z12zb0zzcfa_tvectorz00(BgL_arg2037z00_6360);
				}
				{	/* Cfa/tvector.scm 519 */
					BgL_approxz00_bglt BgL_approxz00_6361;

					{	/* Cfa/tvector.scm 519 */
						obj_t BgL_arg2051z00_6362;

						{	/* Cfa/tvector.scm 519 */
							obj_t BgL_pairz00_6363;

							BgL_pairz00_6363 =
								(((BgL_externz00_bglt) COBJECT(
										((BgL_externz00_bglt)
											((BgL_vlengthz00_bglt)
												((BgL_vlengthz00_bglt) BgL_nodez00_6152)))))->
								BgL_exprza2za2);
							BgL_arg2051z00_6362 = CAR(BgL_pairz00_6363);
						}
						BgL_approxz00_6361 =
							BGl_cfaz12z12zzcfa_cfaz00(
							((BgL_nodez00_bglt) BgL_arg2051z00_6362));
					}
					{	/* Cfa/tvector.scm 519 */
						obj_t BgL_tvz00_6364;

						BgL_tvz00_6364 =
							BGl_getzd2approxzd2typez00zzcfa_typez00(BgL_approxz00_6361,
							((obj_t) ((BgL_vlengthz00_bglt) BgL_nodez00_6152)));
						{	/* Cfa/tvector.scm 520 */

							{	/* Cfa/tvector.scm 521 */
								bool_t BgL_test2569z00_7750;

								{	/* Cfa/tvector.scm 521 */
									bool_t BgL_test2570z00_7751;

									{	/* Cfa/tvector.scm 521 */
										obj_t BgL_classz00_6365;

										BgL_classz00_6365 = BGl_tvecz00zztvector_tvectorz00;
										if (BGL_OBJECTP(BgL_tvz00_6364))
											{	/* Cfa/tvector.scm 521 */
												BgL_objectz00_bglt BgL_arg1807z00_6366;

												BgL_arg1807z00_6366 =
													(BgL_objectz00_bglt) (BgL_tvz00_6364);
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Cfa/tvector.scm 521 */
														long BgL_idxz00_6367;

														BgL_idxz00_6367 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6366);
														BgL_test2570z00_7751 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_6367 + 2L)) == BgL_classz00_6365);
													}
												else
													{	/* Cfa/tvector.scm 521 */
														bool_t BgL_res2346z00_6370;

														{	/* Cfa/tvector.scm 521 */
															obj_t BgL_oclassz00_6371;

															{	/* Cfa/tvector.scm 521 */
																obj_t BgL_arg1815z00_6372;
																long BgL_arg1816z00_6373;

																BgL_arg1815z00_6372 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Cfa/tvector.scm 521 */
																	long BgL_arg1817z00_6374;

																	BgL_arg1817z00_6374 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6366);
																	BgL_arg1816z00_6373 =
																		(BgL_arg1817z00_6374 - OBJECT_TYPE);
																}
																BgL_oclassz00_6371 =
																	VECTOR_REF(BgL_arg1815z00_6372,
																	BgL_arg1816z00_6373);
															}
															{	/* Cfa/tvector.scm 521 */
																bool_t BgL__ortest_1115z00_6375;

																BgL__ortest_1115z00_6375 =
																	(BgL_classz00_6365 == BgL_oclassz00_6371);
																if (BgL__ortest_1115z00_6375)
																	{	/* Cfa/tvector.scm 521 */
																		BgL_res2346z00_6370 =
																			BgL__ortest_1115z00_6375;
																	}
																else
																	{	/* Cfa/tvector.scm 521 */
																		long BgL_odepthz00_6376;

																		{	/* Cfa/tvector.scm 521 */
																			obj_t BgL_arg1804z00_6377;

																			BgL_arg1804z00_6377 =
																				(BgL_oclassz00_6371);
																			BgL_odepthz00_6376 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_6377);
																		}
																		if ((2L < BgL_odepthz00_6376))
																			{	/* Cfa/tvector.scm 521 */
																				obj_t BgL_arg1802z00_6378;

																				{	/* Cfa/tvector.scm 521 */
																					obj_t BgL_arg1803z00_6379;

																					BgL_arg1803z00_6379 =
																						(BgL_oclassz00_6371);
																					BgL_arg1802z00_6378 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_6379, 2L);
																				}
																				BgL_res2346z00_6370 =
																					(BgL_arg1802z00_6378 ==
																					BgL_classz00_6365);
																			}
																		else
																			{	/* Cfa/tvector.scm 521 */
																				BgL_res2346z00_6370 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2570z00_7751 = BgL_res2346z00_6370;
													}
											}
										else
											{	/* Cfa/tvector.scm 521 */
												BgL_test2570z00_7751 = ((bool_t) 0);
											}
									}
									if (BgL_test2570z00_7751)
										{	/* Cfa/tvector.scm 521 */
											bool_t BgL_test2575z00_7774;

											{
												BgL_vlengthzf2cinfozf2_bglt BgL_auxz00_7775;

												{
													obj_t BgL_auxz00_7776;

													{	/* Cfa/tvector.scm 521 */
														BgL_objectz00_bglt BgL_tmpz00_7777;

														BgL_tmpz00_7777 =
															((BgL_objectz00_bglt)
															((BgL_vlengthz00_bglt)
																((BgL_vlengthz00_bglt) BgL_nodez00_6152)));
														BgL_auxz00_7776 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_7777);
													}
													BgL_auxz00_7775 =
														((BgL_vlengthzf2cinfozf2_bglt) BgL_auxz00_7776);
												}
												BgL_test2575z00_7774 =
													(((BgL_vlengthzf2cinfozf2_bglt)
														COBJECT(BgL_auxz00_7775))->BgL_tvectorzf3zf3);
											}
											if (BgL_test2575z00_7774)
												{	/* Cfa/tvector.scm 521 */
													BgL_test2569z00_7750 = ((bool_t) 0);
												}
											else
												{	/* Cfa/tvector.scm 521 */
													BgL_test2569z00_7750 = ((bool_t) 1);
												}
										}
									else
										{	/* Cfa/tvector.scm 521 */
											BgL_test2569z00_7750 = ((bool_t) 0);
										}
								}
								if (BgL_test2569z00_7750)
									{	/* Cfa/tvector.scm 522 */
										obj_t BgL_lengthzd2tvzd2_6380;

										{	/* Cfa/tvector.scm 522 */
											obj_t BgL_arg2047z00_6381;

											{	/* Cfa/tvector.scm 522 */
												obj_t BgL_arg2048z00_6382;
												obj_t BgL_arg2049z00_6383;

												{	/* Cfa/tvector.scm 522 */
													obj_t BgL_arg2050z00_6384;

													BgL_arg2050z00_6384 =
														(((BgL_typez00_bglt) COBJECT(
																((BgL_typez00_bglt) BgL_tvz00_6364)))->
														BgL_idz00);
													{	/* Cfa/tvector.scm 522 */
														obj_t BgL_arg1455z00_6385;

														BgL_arg1455z00_6385 =
															SYMBOL_TO_STRING(BgL_arg2050z00_6384);
														BgL_arg2048z00_6382 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_6385);
													}
												}
												{	/* Cfa/tvector.scm 522 */
													obj_t BgL_symbolz00_6386;

													BgL_symbolz00_6386 = CNST_TABLE_REF(19);
													{	/* Cfa/tvector.scm 522 */
														obj_t BgL_arg1455z00_6387;

														BgL_arg1455z00_6387 =
															SYMBOL_TO_STRING(BgL_symbolz00_6386);
														BgL_arg2049z00_6383 =
															BGl_stringzd2copyzd2zz__r4_strings_6_7z00
															(BgL_arg1455z00_6387);
													}
												}
												BgL_arg2047z00_6381 =
													string_append(BgL_arg2048z00_6382,
													BgL_arg2049z00_6383);
											}
											BgL_lengthzd2tvzd2_6380 =
												bstring_to_symbol(BgL_arg2047z00_6381);
										}
										{	/* Cfa/tvector.scm 522 */
											BgL_nodez00_bglt BgL_newzd2nodezd2_6388;

											{	/* Cfa/tvector.scm 523 */
												obj_t BgL_arg2041z00_6389;
												obj_t BgL_arg2042z00_6390;

												{	/* Cfa/tvector.scm 523 */
													obj_t BgL_arg2044z00_6391;

													{	/* Cfa/tvector.scm 523 */
														obj_t BgL_arg2045z00_6392;

														{	/* Cfa/tvector.scm 523 */
															obj_t BgL_pairz00_6393;

															BgL_pairz00_6393 =
																(((BgL_externz00_bglt) COBJECT(
																		((BgL_externz00_bglt)
																			((BgL_vlengthz00_bglt)
																				((BgL_vlengthz00_bglt)
																					BgL_nodez00_6152)))))->
																BgL_exprza2za2);
															BgL_arg2045z00_6392 = CAR(BgL_pairz00_6393);
														}
														BgL_arg2044z00_6391 =
															MAKE_YOUNG_PAIR(BgL_arg2045z00_6392, BNIL);
													}
													BgL_arg2041z00_6389 =
														MAKE_YOUNG_PAIR(BgL_lengthzd2tvzd2_6380,
														BgL_arg2044z00_6391);
												}
												BgL_arg2042z00_6390 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt)
																((BgL_vlengthz00_bglt)
																	((BgL_vlengthz00_bglt) BgL_nodez00_6152)))))->
													BgL_locz00);
												BgL_newzd2nodezd2_6388 =
													BGl_sexpzd2ze3nodez31zzast_sexpz00
													(BgL_arg2041z00_6389, BNIL, BgL_arg2042z00_6390,
													CNST_TABLE_REF(12));
											}
											{	/* Cfa/tvector.scm 523 */

												{	/* Cfa/tvector.scm 527 */
													BgL_typez00_bglt BgL_vz00_6394;

													BgL_vz00_6394 =
														((BgL_typez00_bglt)
														BGl_za2intza2z00zztype_cachez00);
													((((BgL_nodez00_bglt)
																COBJECT(BgL_newzd2nodezd2_6388))->BgL_typez00) =
														((BgL_typez00_bglt) BgL_vz00_6394), BUNSPEC);
												}
												BgL_auxz00_7734 =
													BGl_inlinezd2nodezd2zzinline_inlinez00
													(BgL_newzd2nodezd2_6388, 1L, BNIL);
											}
										}
									}
								else
									{	/* Cfa/tvector.scm 521 */
										BgL_auxz00_7734 =
											((BgL_nodez00_bglt)
											((BgL_vlengthz00_bglt) BgL_nodez00_6152));
									}
							}
						}
					}
				}
				return ((obj_t) BgL_auxz00_7734);
			}
		}

	}



/* &patch!-app1692 */
	obj_t BGl_z62patchz12zd2app1692za2zzcfa_tvectorz00(obj_t BgL_envz00_6153,
		obj_t BgL_nodez00_6154)
	{
		{	/* Cfa/tvector.scm 496 */
			{
				BgL_nodez00_bglt BgL_auxz00_7812;

				BGl_patchza2z12zb0zzcfa_tvectorz00(
					(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_6154)))->BgL_argsz00));
				{
					BgL_varz00_bglt BgL_auxz00_7816;

					{	/* Cfa/tvector.scm 499 */
						BgL_varz00_bglt BgL_arg2019z00_6396;

						BgL_arg2019z00_6396 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nodez00_6154)))->BgL_funz00);
						BgL_auxz00_7816 =
							((BgL_varz00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(
								((BgL_nodez00_bglt) BgL_arg2019z00_6396)));
					}
					((((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt) BgL_nodez00_6154)))->BgL_funz00) =
						((BgL_varz00_bglt) BgL_auxz00_7816), BUNSPEC);
				}
				{	/* Cfa/tvector.scm 500 */
					BgL_variablez00_bglt BgL_vz00_6397;

					BgL_vz00_6397 =
						(((BgL_varz00_bglt) COBJECT(
								(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt) BgL_nodez00_6154)))->BgL_funz00)))->
						BgL_variablez00);
					{	/* Cfa/tvector.scm 501 */
						bool_t BgL_test2576z00_7827;

						{	/* Cfa/tvector.scm 501 */
							obj_t BgL_classz00_6398;

							BgL_classz00_6398 = BGl_globalz00zzast_varz00;
							{	/* Cfa/tvector.scm 501 */
								BgL_objectz00_bglt BgL_arg1807z00_6399;

								{	/* Cfa/tvector.scm 501 */
									obj_t BgL_tmpz00_7828;

									BgL_tmpz00_7828 =
										((obj_t) ((BgL_objectz00_bglt) BgL_vz00_6397));
									BgL_arg1807z00_6399 = (BgL_objectz00_bglt) (BgL_tmpz00_7828);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/tvector.scm 501 */
										long BgL_idxz00_6400;

										BgL_idxz00_6400 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6399);
										BgL_test2576z00_7827 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_6400 + 2L)) == BgL_classz00_6398);
									}
								else
									{	/* Cfa/tvector.scm 501 */
										bool_t BgL_res2344z00_6403;

										{	/* Cfa/tvector.scm 501 */
											obj_t BgL_oclassz00_6404;

											{	/* Cfa/tvector.scm 501 */
												obj_t BgL_arg1815z00_6405;
												long BgL_arg1816z00_6406;

												BgL_arg1815z00_6405 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/tvector.scm 501 */
													long BgL_arg1817z00_6407;

													BgL_arg1817z00_6407 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6399);
													BgL_arg1816z00_6406 =
														(BgL_arg1817z00_6407 - OBJECT_TYPE);
												}
												BgL_oclassz00_6404 =
													VECTOR_REF(BgL_arg1815z00_6405, BgL_arg1816z00_6406);
											}
											{	/* Cfa/tvector.scm 501 */
												bool_t BgL__ortest_1115z00_6408;

												BgL__ortest_1115z00_6408 =
													(BgL_classz00_6398 == BgL_oclassz00_6404);
												if (BgL__ortest_1115z00_6408)
													{	/* Cfa/tvector.scm 501 */
														BgL_res2344z00_6403 = BgL__ortest_1115z00_6408;
													}
												else
													{	/* Cfa/tvector.scm 501 */
														long BgL_odepthz00_6409;

														{	/* Cfa/tvector.scm 501 */
															obj_t BgL_arg1804z00_6410;

															BgL_arg1804z00_6410 = (BgL_oclassz00_6404);
															BgL_odepthz00_6409 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_6410);
														}
														if ((2L < BgL_odepthz00_6409))
															{	/* Cfa/tvector.scm 501 */
																obj_t BgL_arg1802z00_6411;

																{	/* Cfa/tvector.scm 501 */
																	obj_t BgL_arg1803z00_6412;

																	BgL_arg1803z00_6412 = (BgL_oclassz00_6404);
																	BgL_arg1802z00_6411 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6412,
																		2L);
																}
																BgL_res2344z00_6403 =
																	(BgL_arg1802z00_6411 == BgL_classz00_6398);
															}
														else
															{	/* Cfa/tvector.scm 501 */
																BgL_res2344z00_6403 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2576z00_7827 = BgL_res2344z00_6403;
									}
							}
						}
						if (BgL_test2576z00_7827)
							{	/* Cfa/tvector.scm 502 */
								bool_t BgL_test2580z00_7851;

								{	/* Cfa/tvector.scm 502 */
									BgL_valuez00_bglt BgL_arg2034z00_6413;

									BgL_arg2034z00_6413 =
										(((BgL_variablez00_bglt) COBJECT(BgL_vz00_6397))->
										BgL_valuez00);
									{	/* Cfa/tvector.scm 502 */
										obj_t BgL_classz00_6414;

										BgL_classz00_6414 = BGl_cfunz00zzast_varz00;
										{	/* Cfa/tvector.scm 502 */
											BgL_objectz00_bglt BgL_arg1807z00_6415;

											{	/* Cfa/tvector.scm 502 */
												obj_t BgL_tmpz00_7853;

												BgL_tmpz00_7853 =
													((obj_t) ((BgL_objectz00_bglt) BgL_arg2034z00_6413));
												BgL_arg1807z00_6415 =
													(BgL_objectz00_bglt) (BgL_tmpz00_7853);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Cfa/tvector.scm 502 */
													long BgL_idxz00_6416;

													BgL_idxz00_6416 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6415);
													BgL_test2580z00_7851 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_6416 + 3L)) == BgL_classz00_6414);
												}
											else
												{	/* Cfa/tvector.scm 502 */
													bool_t BgL_res2345z00_6419;

													{	/* Cfa/tvector.scm 502 */
														obj_t BgL_oclassz00_6420;

														{	/* Cfa/tvector.scm 502 */
															obj_t BgL_arg1815z00_6421;
															long BgL_arg1816z00_6422;

															BgL_arg1815z00_6421 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Cfa/tvector.scm 502 */
																long BgL_arg1817z00_6423;

																BgL_arg1817z00_6423 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6415);
																BgL_arg1816z00_6422 =
																	(BgL_arg1817z00_6423 - OBJECT_TYPE);
															}
															BgL_oclassz00_6420 =
																VECTOR_REF(BgL_arg1815z00_6421,
																BgL_arg1816z00_6422);
														}
														{	/* Cfa/tvector.scm 502 */
															bool_t BgL__ortest_1115z00_6424;

															BgL__ortest_1115z00_6424 =
																(BgL_classz00_6414 == BgL_oclassz00_6420);
															if (BgL__ortest_1115z00_6424)
																{	/* Cfa/tvector.scm 502 */
																	BgL_res2345z00_6419 =
																		BgL__ortest_1115z00_6424;
																}
															else
																{	/* Cfa/tvector.scm 502 */
																	long BgL_odepthz00_6425;

																	{	/* Cfa/tvector.scm 502 */
																		obj_t BgL_arg1804z00_6426;

																		BgL_arg1804z00_6426 = (BgL_oclassz00_6420);
																		BgL_odepthz00_6425 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_6426);
																	}
																	if ((3L < BgL_odepthz00_6425))
																		{	/* Cfa/tvector.scm 502 */
																			obj_t BgL_arg1802z00_6427;

																			{	/* Cfa/tvector.scm 502 */
																				obj_t BgL_arg1803z00_6428;

																				BgL_arg1803z00_6428 =
																					(BgL_oclassz00_6420);
																				BgL_arg1802z00_6427 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_6428, 3L);
																			}
																			BgL_res2345z00_6419 =
																				(BgL_arg1802z00_6427 ==
																				BgL_classz00_6414);
																		}
																	else
																		{	/* Cfa/tvector.scm 502 */
																			BgL_res2345z00_6419 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2580z00_7851 = BgL_res2345z00_6419;
												}
										}
									}
								}
								if (BgL_test2580z00_7851)
									{	/* Cfa/tvector.scm 503 */
										obj_t BgL_casezd2valuezd2_6429;

										BgL_casezd2valuezd2_6429 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_vz00_6397))))->BgL_idz00);
										if ((BgL_casezd2valuezd2_6429 == CNST_TABLE_REF(1)))
											{	/* Cfa/tvector.scm 503 */
												BgL_auxz00_7812 =
													BGl_patchzd2vectorzf3z12z33zzcfa_tvectorz00(
													((BgL_appz00_bglt) BgL_nodez00_6154));
											}
										else
											{	/* Cfa/tvector.scm 503 */
												if ((BgL_casezd2valuezd2_6429 == CNST_TABLE_REF(2)))
													{	/* Cfa/tvector.scm 503 */
														BgL_auxz00_7812 =
															BGl_patchzd2vectorzf3z12z33zzcfa_tvectorz00(
															((BgL_appz00_bglt) BgL_nodez00_6154));
													}
												else
													{	/* Cfa/tvector.scm 503 */
														BgL_auxz00_7812 =
															((BgL_nodez00_bglt)
															((BgL_appz00_bglt) BgL_nodez00_6154));
													}
											}
									}
								else
									{	/* Cfa/tvector.scm 507 */
										bool_t BgL_test2586z00_7891;

										if (
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_globalz00_bglt) BgL_vz00_6397))))->
													BgL_idz00) == CNST_TABLE_REF(20)))
											{	/* Cfa/tvector.scm 507 */
												BgL_test2586z00_7891 =
													(
													(((BgL_globalz00_bglt) COBJECT(
																((BgL_globalz00_bglt) BgL_vz00_6397)))->
														BgL_modulez00) == CNST_TABLE_REF(21));
											}
										else
											{	/* Cfa/tvector.scm 507 */
												BgL_test2586z00_7891 = ((bool_t) 0);
											}
										if (BgL_test2586z00_7891)
											{	/* Cfa/tvector.scm 507 */
												BgL_auxz00_7812 =
													BGl_patchzd2vectorzd2ze3listz12zf1zzcfa_tvectorz00(
													((BgL_appz00_bglt) BgL_nodez00_6154));
											}
										else
											{	/* Cfa/tvector.scm 507 */
												BgL_auxz00_7812 =
													((BgL_nodez00_bglt)
													((BgL_appz00_bglt) BgL_nodez00_6154));
											}
									}
							}
						else
							{	/* Cfa/tvector.scm 501 */
								BgL_auxz00_7812 =
									((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_6154));
							}
					}
				}
				return ((obj_t) BgL_auxz00_7812);
			}
		}

	}



/* &patch!-box-ref1690 */
	obj_t BGl_z62patchz12zd2boxzd2ref1690z70zzcfa_tvectorz00(obj_t
		BgL_envz00_6155, obj_t BgL_nodez00_6156)
	{
		{	/* Cfa/tvector.scm 477 */
			{
				BgL_boxzd2refzd2_bglt BgL_auxz00_7909;

				{
					BgL_varz00_bglt BgL_auxz00_7910;

					{	/* Cfa/tvector.scm 479 */
						BgL_varz00_bglt BgL_arg2017z00_6431;

						BgL_arg2017z00_6431 =
							(((BgL_boxzd2refzd2_bglt) COBJECT(
									((BgL_boxzd2refzd2_bglt) BgL_nodez00_6156)))->BgL_varz00);
						BgL_auxz00_7910 =
							((BgL_varz00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(
								((BgL_nodez00_bglt) BgL_arg2017z00_6431)));
					}
					((((BgL_boxzd2refzd2_bglt) COBJECT(
									((BgL_boxzd2refzd2_bglt) BgL_nodez00_6156)))->BgL_varz00) =
						((BgL_varz00_bglt) BgL_auxz00_7910), BUNSPEC);
				}
				BgL_auxz00_7909 = ((BgL_boxzd2refzd2_bglt) BgL_nodez00_6156);
				return ((obj_t) BgL_auxz00_7909);
			}
		}

	}



/* &patch!-box-set!1688 */
	obj_t BGl_z62patchz12zd2boxzd2setz121688z62zzcfa_tvectorz00(obj_t
		BgL_envz00_6157, obj_t BgL_nodez00_6158)
	{
		{	/* Cfa/tvector.scm 468 */
			{
				BgL_boxzd2setz12zc0_bglt BgL_auxz00_7920;

				{
					BgL_varz00_bglt BgL_auxz00_7921;

					{	/* Cfa/tvector.scm 470 */
						BgL_varz00_bglt BgL_arg2015z00_6433;

						BgL_arg2015z00_6433 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_6158)))->BgL_varz00);
						BgL_auxz00_7921 =
							((BgL_varz00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(
								((BgL_nodez00_bglt) BgL_arg2015z00_6433)));
					}
					((((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_6158)))->BgL_varz00) =
						((BgL_varz00_bglt) BgL_auxz00_7921), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_7929;

					{	/* Cfa/tvector.scm 471 */
						BgL_nodez00_bglt BgL_arg2016z00_6434;

						BgL_arg2016z00_6434 =
							(((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_6158)))->
							BgL_valuez00);
						BgL_auxz00_7929 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg2016z00_6434));
					}
					((((BgL_boxzd2setz12zc0_bglt) COBJECT(
									((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_6158)))->
							BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_7929), BUNSPEC);
				}
				BgL_auxz00_7920 = ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_6158);
				return ((obj_t) BgL_auxz00_7920);
			}
		}

	}



/* &patch!-make-box1686 */
	obj_t BGl_z62patchz12zd2makezd2box1686z70zzcfa_tvectorz00(obj_t
		BgL_envz00_6159, obj_t BgL_nodez00_6160)
	{
		{	/* Cfa/tvector.scm 460 */
			{
				BgL_makezd2boxzd2_bglt BgL_auxz00_7938;

				{
					BgL_nodez00_bglt BgL_auxz00_7939;

					{	/* Cfa/tvector.scm 462 */
						BgL_nodez00_bglt BgL_arg2014z00_6436;

						BgL_arg2014z00_6436 =
							(((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_6160)))->BgL_valuez00);
						BgL_auxz00_7939 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg2014z00_6436));
					}
					((((BgL_makezd2boxzd2_bglt) COBJECT(
									((BgL_makezd2boxzd2_bglt) BgL_nodez00_6160)))->BgL_valuez00) =
						((BgL_nodez00_bglt) BgL_auxz00_7939), BUNSPEC);
				}
				BgL_auxz00_7938 = ((BgL_makezd2boxzd2_bglt) BgL_nodez00_6160);
				return ((obj_t) BgL_auxz00_7938);
			}
		}

	}



/* &patch!-jump-ex-it1684 */
	obj_t BGl_z62patchz12zd2jumpzd2exzd2it1684za2zzcfa_tvectorz00(obj_t
		BgL_envz00_6161, obj_t BgL_nodez00_6162)
	{
		{	/* Cfa/tvector.scm 451 */
			{
				BgL_jumpzd2exzd2itz00_bglt BgL_auxz00_7948;

				{
					BgL_nodez00_bglt BgL_auxz00_7949;

					{	/* Cfa/tvector.scm 453 */
						BgL_nodez00_bglt BgL_arg2012z00_6438;

						BgL_arg2012z00_6438 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6162)))->
							BgL_exitz00);
						BgL_auxz00_7949 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg2012z00_6438));
					}
					((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6162)))->
							BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_7949), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_7956;

					{	/* Cfa/tvector.scm 454 */
						BgL_nodez00_bglt BgL_arg2013z00_6439;

						BgL_arg2013z00_6439 =
							(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6162)))->
							BgL_valuez00);
						BgL_auxz00_7956 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg2013z00_6439));
					}
					((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
									((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6162)))->
							BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_7956), BUNSPEC);
				}
				BgL_auxz00_7948 = ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6162);
				return ((obj_t) BgL_auxz00_7948);
			}
		}

	}



/* &patch!-set-ex-it1682 */
	obj_t BGl_z62patchz12zd2setzd2exzd2it1682za2zzcfa_tvectorz00(obj_t
		BgL_envz00_6163, obj_t BgL_nodez00_6164)
	{
		{	/* Cfa/tvector.scm 441 */
			{
				BgL_setzd2exzd2itz00_bglt BgL_auxz00_7965;

				{
					BgL_nodez00_bglt BgL_auxz00_7966;

					{	/* Cfa/tvector.scm 443 */
						BgL_nodez00_bglt BgL_arg2009z00_6441;

						BgL_arg2009z00_6441 =
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6164)))->
							BgL_bodyz00);
						BgL_auxz00_7966 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg2009z00_6441));
					}
					((((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6164)))->
							BgL_bodyz00) = ((BgL_nodez00_bglt) BgL_auxz00_7966), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_7973;

					{	/* Cfa/tvector.scm 444 */
						BgL_nodez00_bglt BgL_arg2010z00_6442;

						BgL_arg2010z00_6442 =
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6164)))->
							BgL_onexitz00);
						BgL_auxz00_7973 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg2010z00_6442));
					}
					((((BgL_setzd2exzd2itz00_bglt) COBJECT(
									((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6164)))->
							BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_auxz00_7973), BUNSPEC);
				}
				{	/* Cfa/tvector.scm 445 */
					BgL_varz00_bglt BgL_arg2011z00_6443;

					BgL_arg2011z00_6443 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6164)))->BgL_varz00);
					BGl_patchz12z12zzcfa_tvectorz00(
						((BgL_nodez00_bglt) BgL_arg2011z00_6443));
				}
				BgL_auxz00_7965 = ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6164);
				return ((obj_t) BgL_auxz00_7965);
			}
		}

	}



/* &patch!-let-var1680 */
	obj_t BGl_z62patchz12zd2letzd2var1680z70zzcfa_tvectorz00(obj_t
		BgL_envz00_6165, obj_t BgL_nodez00_6166)
	{
		{	/* Cfa/tvector.scm 429 */
			{
				BgL_letzd2varzd2_bglt BgL_auxz00_7986;

				{	/* Cfa/tvector.scm 431 */
					obj_t BgL_g1637z00_6445;

					BgL_g1637z00_6445 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_6166)))->BgL_bindingsz00);
					{
						obj_t BgL_l1635z00_6447;

						BgL_l1635z00_6447 = BgL_g1637z00_6445;
					BgL_zc3z04anonymousza32003ze3z87_6446:
						if (PAIRP(BgL_l1635z00_6447))
							{	/* Cfa/tvector.scm 431 */
								{	/* Cfa/tvector.scm 432 */
									obj_t BgL_bindingz00_6448;

									BgL_bindingz00_6448 = CAR(BgL_l1635z00_6447);
									{	/* Cfa/tvector.scm 432 */
										obj_t BgL_valz00_6449;

										BgL_valz00_6449 = CDR(((obj_t) BgL_bindingz00_6448));
										{	/* Cfa/tvector.scm 433 */
											obj_t BgL_arg2006z00_6450;

											BgL_arg2006z00_6450 =
												BGl_patchz12z12zzcfa_tvectorz00(
												((BgL_nodez00_bglt) BgL_valz00_6449));
											{	/* Cfa/tvector.scm 433 */
												obj_t BgL_tmpz00_7996;

												BgL_tmpz00_7996 = ((obj_t) BgL_bindingz00_6448);
												SET_CDR(BgL_tmpz00_7996, BgL_arg2006z00_6450);
											}
										}
									}
								}
								{
									obj_t BgL_l1635z00_7999;

									BgL_l1635z00_7999 = CDR(BgL_l1635z00_6447);
									BgL_l1635z00_6447 = BgL_l1635z00_7999;
									goto BgL_zc3z04anonymousza32003ze3z87_6446;
								}
							}
						else
							{	/* Cfa/tvector.scm 431 */
								((bool_t) 1);
							}
					}
				}
				{
					BgL_nodez00_bglt BgL_auxz00_8001;

					{	/* Cfa/tvector.scm 435 */
						BgL_nodez00_bglt BgL_arg2008z00_6451;

						BgL_arg2008z00_6451 =
							(((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_6166)))->BgL_bodyz00);
						BgL_auxz00_8001 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg2008z00_6451));
					}
					((((BgL_letzd2varzd2_bglt) COBJECT(
									((BgL_letzd2varzd2_bglt) BgL_nodez00_6166)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) BgL_auxz00_8001), BUNSPEC);
				}
				BgL_auxz00_7986 = ((BgL_letzd2varzd2_bglt) BgL_nodez00_6166);
				return ((obj_t) BgL_auxz00_7986);
			}
		}

	}



/* &patch!-let-fun1678 */
	obj_t BGl_z62patchz12zd2letzd2fun1678z70zzcfa_tvectorz00(obj_t
		BgL_envz00_6167, obj_t BgL_nodez00_6168)
	{
		{	/* Cfa/tvector.scm 420 */
			{
				BgL_letzd2funzd2_bglt BgL_auxz00_8010;

				{	/* Cfa/tvector.scm 422 */
					obj_t BgL_g1634z00_6453;

					BgL_g1634z00_6453 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_6168)))->BgL_localsz00);
					{
						obj_t BgL_l1632z00_6455;

						BgL_l1632z00_6455 = BgL_g1634z00_6453;
					BgL_zc3z04anonymousza31998ze3z87_6454:
						if (PAIRP(BgL_l1632z00_6455))
							{	/* Cfa/tvector.scm 422 */
								BGl_patchzd2funz12zc0zzcfa_tvectorz00(CAR(BgL_l1632z00_6455));
								{
									obj_t BgL_l1632z00_8017;

									BgL_l1632z00_8017 = CDR(BgL_l1632z00_6455);
									BgL_l1632z00_6455 = BgL_l1632z00_8017;
									goto BgL_zc3z04anonymousza31998ze3z87_6454;
								}
							}
						else
							{	/* Cfa/tvector.scm 422 */
								((bool_t) 1);
							}
					}
				}
				{
					BgL_nodez00_bglt BgL_auxz00_8019;

					{	/* Cfa/tvector.scm 423 */
						BgL_nodez00_bglt BgL_arg2002z00_6456;

						BgL_arg2002z00_6456 =
							(((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nodez00_6168)))->BgL_bodyz00);
						BgL_auxz00_8019 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg2002z00_6456));
					}
					((((BgL_letzd2funzd2_bglt) COBJECT(
									((BgL_letzd2funzd2_bglt) BgL_nodez00_6168)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) BgL_auxz00_8019), BUNSPEC);
				}
				BgL_auxz00_8010 = ((BgL_letzd2funzd2_bglt) BgL_nodez00_6168);
				return ((obj_t) BgL_auxz00_8010);
			}
		}

	}



/* &patch!-switch1676 */
	obj_t BGl_z62patchz12zd2switch1676za2zzcfa_tvectorz00(obj_t BgL_envz00_6169,
		obj_t BgL_nodez00_6170)
	{
		{	/* Cfa/tvector.scm 409 */
			{
				BgL_switchz00_bglt BgL_auxz00_8028;

				{
					BgL_nodez00_bglt BgL_auxz00_8029;

					{	/* Cfa/tvector.scm 411 */
						BgL_nodez00_bglt BgL_arg1992z00_6458;

						BgL_arg1992z00_6458 =
							(((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_6170)))->BgL_testz00);
						BgL_auxz00_8029 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg1992z00_6458));
					}
					((((BgL_switchz00_bglt) COBJECT(
									((BgL_switchz00_bglt) BgL_nodez00_6170)))->BgL_testz00) =
						((BgL_nodez00_bglt) BgL_auxz00_8029), BUNSPEC);
				}
				{	/* Cfa/tvector.scm 412 */
					obj_t BgL_g1631z00_6459;

					BgL_g1631z00_6459 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_6170)))->BgL_clausesz00);
					{
						obj_t BgL_l1629z00_6461;

						BgL_l1629z00_6461 = BgL_g1631z00_6459;
					BgL_zc3z04anonymousza31993ze3z87_6460:
						if (PAIRP(BgL_l1629z00_6461))
							{	/* Cfa/tvector.scm 412 */
								{	/* Cfa/tvector.scm 413 */
									obj_t BgL_clausez00_6462;

									BgL_clausez00_6462 = CAR(BgL_l1629z00_6461);
									{	/* Cfa/tvector.scm 413 */
										obj_t BgL_arg1995z00_6463;

										{	/* Cfa/tvector.scm 413 */
											obj_t BgL_arg1996z00_6464;

											BgL_arg1996z00_6464 = CDR(((obj_t) BgL_clausez00_6462));
											BgL_arg1995z00_6463 =
												BGl_patchz12z12zzcfa_tvectorz00(
												((BgL_nodez00_bglt) BgL_arg1996z00_6464));
										}
										{	/* Cfa/tvector.scm 413 */
											obj_t BgL_tmpz00_8045;

											BgL_tmpz00_8045 = ((obj_t) BgL_clausez00_6462);
											SET_CDR(BgL_tmpz00_8045, BgL_arg1995z00_6463);
										}
									}
								}
								{
									obj_t BgL_l1629z00_8048;

									BgL_l1629z00_8048 = CDR(BgL_l1629z00_6461);
									BgL_l1629z00_6461 = BgL_l1629z00_8048;
									goto BgL_zc3z04anonymousza31993ze3z87_6460;
								}
							}
						else
							{	/* Cfa/tvector.scm 412 */
								((bool_t) 1);
							}
					}
				}
				BgL_auxz00_8028 = ((BgL_switchz00_bglt) BgL_nodez00_6170);
				return ((obj_t) BgL_auxz00_8028);
			}
		}

	}



/* &patch!-fail1674 */
	obj_t BGl_z62patchz12zd2fail1674za2zzcfa_tvectorz00(obj_t BgL_envz00_6171,
		obj_t BgL_nodez00_6172)
	{
		{	/* Cfa/tvector.scm 399 */
			{
				BgL_failz00_bglt BgL_auxz00_8052;

				{
					BgL_nodez00_bglt BgL_auxz00_8053;

					{	/* Cfa/tvector.scm 401 */
						BgL_nodez00_bglt BgL_arg1989z00_6466;

						BgL_arg1989z00_6466 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_6172)))->BgL_procz00);
						BgL_auxz00_8053 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg1989z00_6466));
					}
					((((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_6172)))->BgL_procz00) =
						((BgL_nodez00_bglt) BgL_auxz00_8053), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_8060;

					{	/* Cfa/tvector.scm 402 */
						BgL_nodez00_bglt BgL_arg1990z00_6467;

						BgL_arg1990z00_6467 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_6172)))->BgL_msgz00);
						BgL_auxz00_8060 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg1990z00_6467));
					}
					((((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_6172)))->BgL_msgz00) =
						((BgL_nodez00_bglt) BgL_auxz00_8060), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_8067;

					{	/* Cfa/tvector.scm 403 */
						BgL_nodez00_bglt BgL_arg1991z00_6468;

						BgL_arg1991z00_6468 =
							(((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_6172)))->BgL_objz00);
						BgL_auxz00_8067 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg1991z00_6468));
					}
					((((BgL_failz00_bglt) COBJECT(
									((BgL_failz00_bglt) BgL_nodez00_6172)))->BgL_objz00) =
						((BgL_nodez00_bglt) BgL_auxz00_8067), BUNSPEC);
				}
				BgL_auxz00_8052 = ((BgL_failz00_bglt) BgL_nodez00_6172);
				return ((obj_t) BgL_auxz00_8052);
			}
		}

	}



/* &patch!-conditional1672 */
	obj_t BGl_z62patchz12zd2conditional1672za2zzcfa_tvectorz00(obj_t
		BgL_envz00_6173, obj_t BgL_nodez00_6174)
	{
		{	/* Cfa/tvector.scm 389 */
			{
				BgL_conditionalz00_bglt BgL_auxz00_8076;

				{
					BgL_nodez00_bglt BgL_auxz00_8077;

					{	/* Cfa/tvector.scm 391 */
						BgL_nodez00_bglt BgL_arg1986z00_6470;

						BgL_arg1986z00_6470 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_6174)))->BgL_testz00);
						BgL_auxz00_8077 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg1986z00_6470));
					}
					((((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_6174)))->BgL_testz00) =
						((BgL_nodez00_bglt) BgL_auxz00_8077), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_8084;

					{	/* Cfa/tvector.scm 392 */
						BgL_nodez00_bglt BgL_arg1987z00_6471;

						BgL_arg1987z00_6471 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_6174)))->BgL_truez00);
						BgL_auxz00_8084 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg1987z00_6471));
					}
					((((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_6174)))->BgL_truez00) =
						((BgL_nodez00_bglt) BgL_auxz00_8084), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_8091;

					{	/* Cfa/tvector.scm 393 */
						BgL_nodez00_bglt BgL_arg1988z00_6472;

						BgL_arg1988z00_6472 =
							(((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_6174)))->BgL_falsez00);
						BgL_auxz00_8091 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg1988z00_6472));
					}
					((((BgL_conditionalz00_bglt) COBJECT(
									((BgL_conditionalz00_bglt) BgL_nodez00_6174)))->
							BgL_falsez00) = ((BgL_nodez00_bglt) BgL_auxz00_8091), BUNSPEC);
				}
				BgL_auxz00_8076 = ((BgL_conditionalz00_bglt) BgL_nodez00_6174);
				return ((obj_t) BgL_auxz00_8076);
			}
		}

	}



/* &patch!-setq1670 */
	obj_t BGl_z62patchz12zd2setq1670za2zzcfa_tvectorz00(obj_t BgL_envz00_6175,
		obj_t BgL_nodez00_6176)
	{
		{	/* Cfa/tvector.scm 380 */
			{
				BgL_setqz00_bglt BgL_auxz00_8100;

				{
					BgL_nodez00_bglt BgL_auxz00_8101;

					{	/* Cfa/tvector.scm 382 */
						BgL_nodez00_bglt BgL_arg1984z00_6474;

						BgL_arg1984z00_6474 =
							(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nodez00_6176)))->BgL_valuez00);
						BgL_auxz00_8101 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg1984z00_6474));
					}
					((((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nodez00_6176)))->BgL_valuez00) =
						((BgL_nodez00_bglt) BgL_auxz00_8101), BUNSPEC);
				}
				{
					BgL_varz00_bglt BgL_auxz00_8108;

					{	/* Cfa/tvector.scm 383 */
						BgL_varz00_bglt BgL_arg1985z00_6475;

						BgL_arg1985z00_6475 =
							(((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nodez00_6176)))->BgL_varz00);
						BgL_auxz00_8108 =
							((BgL_varz00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(
								((BgL_nodez00_bglt) BgL_arg1985z00_6475)));
					}
					((((BgL_setqz00_bglt) COBJECT(
									((BgL_setqz00_bglt) BgL_nodez00_6176)))->BgL_varz00) =
						((BgL_varz00_bglt) BgL_auxz00_8108), BUNSPEC);
				}
				BgL_auxz00_8100 = ((BgL_setqz00_bglt) BgL_nodez00_6176);
				return ((obj_t) BgL_auxz00_8100);
			}
		}

	}



/* &patch!-cast1668 */
	obj_t BGl_z62patchz12zd2cast1668za2zzcfa_tvectorz00(obj_t BgL_envz00_6177,
		obj_t BgL_nodez00_6178)
	{
		{	/* Cfa/tvector.scm 372 */
			{
				BgL_castz00_bglt BgL_auxz00_8118;

				BGl_patchz12z12zzcfa_tvectorz00(
					(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_6178)))->BgL_argz00));
				BgL_auxz00_8118 = ((BgL_castz00_bglt) BgL_nodez00_6178);
				return ((obj_t) BgL_auxz00_8118);
			}
		}

	}



/* &patch!-extern1666 */
	obj_t BGl_z62patchz12zd2extern1666za2zzcfa_tvectorz00(obj_t BgL_envz00_6179,
		obj_t BgL_nodez00_6180)
	{
		{	/* Cfa/tvector.scm 364 */
			{
				BgL_externz00_bglt BgL_auxz00_8124;

				BGl_patchza2z12zb0zzcfa_tvectorz00(
					(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt) BgL_nodez00_6180)))->BgL_exprza2za2));
				BgL_auxz00_8124 = ((BgL_externz00_bglt) BgL_nodez00_6180);
				return ((obj_t) BgL_auxz00_8124);
			}
		}

	}



/* &patch!-funcall1664 */
	obj_t BGl_z62patchz12zd2funcall1664za2zzcfa_tvectorz00(obj_t BgL_envz00_6181,
		obj_t BgL_nodez00_6182)
	{
		{	/* Cfa/tvector.scm 355 */
			{
				BgL_funcallz00_bglt BgL_auxz00_8130;

				{
					BgL_nodez00_bglt BgL_auxz00_8131;

					{	/* Cfa/tvector.scm 357 */
						BgL_nodez00_bglt BgL_arg1980z00_6479;

						BgL_arg1980z00_6479 =
							(((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nodez00_6182)))->BgL_funz00);
						BgL_auxz00_8131 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg1980z00_6479));
					}
					((((BgL_funcallz00_bglt) COBJECT(
									((BgL_funcallz00_bglt) BgL_nodez00_6182)))->BgL_funz00) =
						((BgL_nodez00_bglt) BgL_auxz00_8131), BUNSPEC);
				}
				BGl_patchza2z12zb0zzcfa_tvectorz00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_6182)))->BgL_argsz00));
				BgL_auxz00_8130 = ((BgL_funcallz00_bglt) BgL_nodez00_6182);
				return ((obj_t) BgL_auxz00_8130);
			}
		}

	}



/* &patch!-app-ly1662 */
	obj_t BGl_z62patchz12zd2appzd2ly1662z70zzcfa_tvectorz00(obj_t BgL_envz00_6183,
		obj_t BgL_nodez00_6184)
	{
		{	/* Cfa/tvector.scm 346 */
			{
				BgL_appzd2lyzd2_bglt BgL_auxz00_8143;

				{
					BgL_nodez00_bglt BgL_auxz00_8144;

					{	/* Cfa/tvector.scm 348 */
						BgL_nodez00_bglt BgL_arg1978z00_6481;

						BgL_arg1978z00_6481 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_6184)))->BgL_funz00);
						BgL_auxz00_8144 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg1978z00_6481));
					}
					((((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_6184)))->BgL_funz00) =
						((BgL_nodez00_bglt) BgL_auxz00_8144), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_8151;

					{	/* Cfa/tvector.scm 349 */
						BgL_nodez00_bglt BgL_arg1979z00_6482;

						BgL_arg1979z00_6482 =
							(((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_6184)))->BgL_argz00);
						BgL_auxz00_8151 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg1979z00_6482));
					}
					((((BgL_appzd2lyzd2_bglt) COBJECT(
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_6184)))->BgL_argz00) =
						((BgL_nodez00_bglt) BgL_auxz00_8151), BUNSPEC);
				}
				BgL_auxz00_8143 = ((BgL_appzd2lyzd2_bglt) BgL_nodez00_6184);
				return ((obj_t) BgL_auxz00_8143);
			}
		}

	}



/* &patch!-sync1660 */
	obj_t BGl_z62patchz12zd2sync1660za2zzcfa_tvectorz00(obj_t BgL_envz00_6185,
		obj_t BgL_nodez00_6186)
	{
		{	/* Cfa/tvector.scm 336 */
			{
				BgL_syncz00_bglt BgL_auxz00_8160;

				{
					BgL_nodez00_bglt BgL_auxz00_8161;

					{	/* Cfa/tvector.scm 338 */
						BgL_nodez00_bglt BgL_arg1975z00_6484;

						BgL_arg1975z00_6484 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_6186)))->BgL_mutexz00);
						BgL_auxz00_8161 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg1975z00_6484));
					}
					((((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_6186)))->BgL_mutexz00) =
						((BgL_nodez00_bglt) BgL_auxz00_8161), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_8168;

					{	/* Cfa/tvector.scm 339 */
						BgL_nodez00_bglt BgL_arg1976z00_6485;

						BgL_arg1976z00_6485 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_6186)))->BgL_prelockz00);
						BgL_auxz00_8168 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg1976z00_6485));
					}
					((((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_6186)))->BgL_prelockz00) =
						((BgL_nodez00_bglt) BgL_auxz00_8168), BUNSPEC);
				}
				{
					BgL_nodez00_bglt BgL_auxz00_8175;

					{	/* Cfa/tvector.scm 340 */
						BgL_nodez00_bglt BgL_arg1977z00_6486;

						BgL_arg1977z00_6486 =
							(((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_6186)))->BgL_bodyz00);
						BgL_auxz00_8175 =
							((BgL_nodez00_bglt)
							BGl_patchz12z12zzcfa_tvectorz00(BgL_arg1977z00_6486));
					}
					((((BgL_syncz00_bglt) COBJECT(
									((BgL_syncz00_bglt) BgL_nodez00_6186)))->BgL_bodyz00) =
						((BgL_nodez00_bglt) BgL_auxz00_8175), BUNSPEC);
				}
				BgL_auxz00_8160 = ((BgL_syncz00_bglt) BgL_nodez00_6186);
				return ((obj_t) BgL_auxz00_8160);
			}
		}

	}



/* &patch!-sequence1658 */
	obj_t BGl_z62patchz12zd2sequence1658za2zzcfa_tvectorz00(obj_t BgL_envz00_6187,
		obj_t BgL_nodez00_6188)
	{
		{	/* Cfa/tvector.scm 328 */
			{
				BgL_sequencez00_bglt BgL_auxz00_8184;

				BGl_patchza2z12zb0zzcfa_tvectorz00(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_6188)))->BgL_nodesz00));
				BgL_auxz00_8184 = ((BgL_sequencez00_bglt) BgL_nodez00_6188);
				return ((obj_t) BgL_auxz00_8184);
			}
		}

	}



/* &patch!-closure1656 */
	obj_t BGl_z62patchz12zd2closure1656za2zzcfa_tvectorz00(obj_t BgL_envz00_6189,
		obj_t BgL_nodez00_6190)
	{
		{	/* Cfa/tvector.scm 322 */
			{	/* Cfa/tvector.scm 323 */
				obj_t BgL_arg1973z00_6489;

				BgL_arg1973z00_6489 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_6190)));
				return
					BGl_internalzd2errorzd2zztools_errorz00
					(BGl_string2368z00zzcfa_tvectorz00, BGl_string2396z00zzcfa_tvectorz00,
					BgL_arg1973z00_6489);
			}
		}

	}



/* &patch!-var1654 */
	obj_t BGl_z62patchz12zd2var1654za2zzcfa_tvectorz00(obj_t BgL_envz00_6191,
		obj_t BgL_nodez00_6192)
	{
		{	/* Cfa/tvector.scm 316 */
			return ((obj_t) ((BgL_varz00_bglt) BgL_nodez00_6192));
		}

	}



/* &patch!-kwote/node1652 */
	obj_t BGl_z62patchz12zd2kwotezf2node1652z50zzcfa_tvectorz00(obj_t
		BgL_envz00_6193, obj_t BgL_knodez00_6194)
	{
		{	/* Cfa/tvector.scm 299 */
			{
				BgL_kwotez00_bglt BgL_auxz00_8196;

				{	/* Cfa/tvector.scm 301 */
					BgL_approxz00_bglt BgL_approxz00_6492;

					{	/* Cfa/tvector.scm 301 */
						BgL_nodez00_bglt BgL_arg1972z00_6493;

						{
							BgL_kwotezf2nodezf2_bglt BgL_auxz00_8197;

							{
								obj_t BgL_auxz00_8198;

								{	/* Cfa/tvector.scm 301 */
									BgL_objectz00_bglt BgL_tmpz00_8199;

									BgL_tmpz00_8199 =
										((BgL_objectz00_bglt)
										((BgL_kwotez00_bglt) BgL_knodez00_6194));
									BgL_auxz00_8198 = BGL_OBJECT_WIDENING(BgL_tmpz00_8199);
								}
								BgL_auxz00_8197 = ((BgL_kwotezf2nodezf2_bglt) BgL_auxz00_8198);
							}
							BgL_arg1972z00_6493 =
								(((BgL_kwotezf2nodezf2_bglt) COBJECT(BgL_auxz00_8197))->
								BgL_nodez00);
						}
						BgL_approxz00_6492 = BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1972z00_6493);
					}
					{	/* Cfa/tvector.scm 301 */
						obj_t BgL_tvz00_6494;

						{	/* Cfa/tvector.scm 302 */
							BgL_nodez00_bglt BgL_arg1971z00_6495;

							{
								BgL_kwotezf2nodezf2_bglt BgL_auxz00_8206;

								{
									obj_t BgL_auxz00_8207;

									{	/* Cfa/tvector.scm 302 */
										BgL_objectz00_bglt BgL_tmpz00_8208;

										BgL_tmpz00_8208 =
											((BgL_objectz00_bglt)
											((BgL_kwotez00_bglt) BgL_knodez00_6194));
										BgL_auxz00_8207 = BGL_OBJECT_WIDENING(BgL_tmpz00_8208);
									}
									BgL_auxz00_8206 =
										((BgL_kwotezf2nodezf2_bglt) BgL_auxz00_8207);
								}
								BgL_arg1971z00_6495 =
									(((BgL_kwotezf2nodezf2_bglt) COBJECT(BgL_auxz00_8206))->
									BgL_nodez00);
							}
							BgL_tvz00_6494 =
								BGl_getzd2approxzd2typez00zzcfa_typez00(BgL_approxz00_6492,
								((obj_t) BgL_arg1971z00_6495));
						}
						{	/* Cfa/tvector.scm 302 */

							{	/* Cfa/tvector.scm 303 */
								bool_t BgL_test2591z00_8216;

								{	/* Cfa/tvector.scm 303 */
									obj_t BgL_classz00_6496;

									BgL_classz00_6496 = BGl_tvecz00zztvector_tvectorz00;
									if (BGL_OBJECTP(BgL_tvz00_6494))
										{	/* Cfa/tvector.scm 303 */
											BgL_objectz00_bglt BgL_arg1807z00_6497;

											BgL_arg1807z00_6497 =
												(BgL_objectz00_bglt) (BgL_tvz00_6494);
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Cfa/tvector.scm 303 */
													long BgL_idxz00_6498;

													BgL_idxz00_6498 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6497);
													BgL_test2591z00_8216 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_6498 + 2L)) == BgL_classz00_6496);
												}
											else
												{	/* Cfa/tvector.scm 303 */
													bool_t BgL_res2343z00_6501;

													{	/* Cfa/tvector.scm 303 */
														obj_t BgL_oclassz00_6502;

														{	/* Cfa/tvector.scm 303 */
															obj_t BgL_arg1815z00_6503;
															long BgL_arg1816z00_6504;

															BgL_arg1815z00_6503 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Cfa/tvector.scm 303 */
																long BgL_arg1817z00_6505;

																BgL_arg1817z00_6505 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6497);
																BgL_arg1816z00_6504 =
																	(BgL_arg1817z00_6505 - OBJECT_TYPE);
															}
															BgL_oclassz00_6502 =
																VECTOR_REF(BgL_arg1815z00_6503,
																BgL_arg1816z00_6504);
														}
														{	/* Cfa/tvector.scm 303 */
															bool_t BgL__ortest_1115z00_6506;

															BgL__ortest_1115z00_6506 =
																(BgL_classz00_6496 == BgL_oclassz00_6502);
															if (BgL__ortest_1115z00_6506)
																{	/* Cfa/tvector.scm 303 */
																	BgL_res2343z00_6501 =
																		BgL__ortest_1115z00_6506;
																}
															else
																{	/* Cfa/tvector.scm 303 */
																	long BgL_odepthz00_6507;

																	{	/* Cfa/tvector.scm 303 */
																		obj_t BgL_arg1804z00_6508;

																		BgL_arg1804z00_6508 = (BgL_oclassz00_6502);
																		BgL_odepthz00_6507 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_6508);
																	}
																	if ((2L < BgL_odepthz00_6507))
																		{	/* Cfa/tvector.scm 303 */
																			obj_t BgL_arg1802z00_6509;

																			{	/* Cfa/tvector.scm 303 */
																				obj_t BgL_arg1803z00_6510;

																				BgL_arg1803z00_6510 =
																					(BgL_oclassz00_6502);
																				BgL_arg1802z00_6509 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_6510, 2L);
																			}
																			BgL_res2343z00_6501 =
																				(BgL_arg1802z00_6509 ==
																				BgL_classz00_6496);
																		}
																	else
																		{	/* Cfa/tvector.scm 303 */
																			BgL_res2343z00_6501 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2591z00_8216 = BgL_res2343z00_6501;
												}
										}
									else
										{	/* Cfa/tvector.scm 303 */
											BgL_test2591z00_8216 = ((bool_t) 0);
										}
								}
								if (BgL_test2591z00_8216)
									{	/* Cfa/tvector.scm 304 */
										BgL_kwotez00_bglt BgL_knodez00_6511;

										{	/* Cfa/tvector.scm 304 */
											long BgL_arg1964z00_6512;

											{	/* Cfa/tvector.scm 304 */
												obj_t BgL_arg1965z00_6513;

												{	/* Cfa/tvector.scm 304 */
													obj_t BgL_arg1966z00_6514;

													{	/* Cfa/tvector.scm 304 */
														obj_t BgL_arg1815z00_6515;
														long BgL_arg1816z00_6516;

														BgL_arg1815z00_6515 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/tvector.scm 304 */
															long BgL_arg1817z00_6517;

															BgL_arg1817z00_6517 =
																BGL_OBJECT_CLASS_NUM(
																((BgL_objectz00_bglt)
																	((BgL_kwotez00_bglt) BgL_knodez00_6194)));
															BgL_arg1816z00_6516 =
																(BgL_arg1817z00_6517 - OBJECT_TYPE);
														}
														BgL_arg1966z00_6514 =
															VECTOR_REF(BgL_arg1815z00_6515,
															BgL_arg1816z00_6516);
													}
													BgL_arg1965z00_6513 =
														BGl_classzd2superzd2zz__objectz00
														(BgL_arg1966z00_6514);
												}
												{	/* Cfa/tvector.scm 304 */
													obj_t BgL_tmpz00_8246;

													BgL_tmpz00_8246 = ((obj_t) BgL_arg1965z00_6513);
													BgL_arg1964z00_6512 = BGL_CLASS_NUM(BgL_tmpz00_8246);
											}}
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt)
													((BgL_kwotez00_bglt) BgL_knodez00_6194)),
												BgL_arg1964z00_6512);
										}
										{	/* Cfa/tvector.scm 304 */
											BgL_objectz00_bglt BgL_tmpz00_8252;

											BgL_tmpz00_8252 =
												((BgL_objectz00_bglt)
												((BgL_kwotez00_bglt) BgL_knodez00_6194));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8252, BFALSE);
										}
										((BgL_objectz00_bglt)
											((BgL_kwotez00_bglt) BgL_knodez00_6194));
										BgL_knodez00_6511 = ((BgL_kwotez00_bglt) BgL_knodez00_6194);
										{	/* Cfa/tvector.scm 304 */
											BgL_kwotez00_bglt BgL_nz00_6518;

											{	/* Cfa/tvector.scm 305 */
												BgL_kwotez00_bglt BgL_new1193z00_6519;

												{	/* Cfa/tvector.scm 305 */
													BgL_kwotez00_bglt BgL_new1196z00_6520;

													BgL_new1196z00_6520 =
														((BgL_kwotez00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_kwotez00_bgl))));
													{	/* Cfa/tvector.scm 305 */
														long BgL_arg1963z00_6521;

														BgL_arg1963z00_6521 =
															BGL_CLASS_NUM(BGl_kwotez00zzast_nodez00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt) BgL_new1196z00_6520),
															BgL_arg1963z00_6521);
													}
													{	/* Cfa/tvector.scm 305 */
														BgL_objectz00_bglt BgL_tmpz00_8263;

														BgL_tmpz00_8263 =
															((BgL_objectz00_bglt) BgL_new1196z00_6520);
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8263, BFALSE);
													}
													((BgL_objectz00_bglt) BgL_new1196z00_6520);
													BgL_new1193z00_6519 = BgL_new1196z00_6520;
												}
												((((BgL_nodez00_bglt) COBJECT(
																((BgL_nodez00_bglt) BgL_new1193z00_6519)))->
														BgL_locz00) =
													((obj_t) (((BgL_nodez00_bglt)
																COBJECT(((BgL_nodez00_bglt)
																		BgL_knodez00_6511)))->BgL_locz00)),
													BUNSPEC);
												{
													BgL_typez00_bglt BgL_auxz00_8271;

													{	/* Cfa/tvector.scm 306 */
														BgL_typez00_bglt BgL_arg1961z00_6522;

														BgL_arg1961z00_6522 =
															(((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_knodez00_6511)))->
															BgL_typez00);
														BgL_auxz00_8271 =
															BGl_strictzd2nodezd2typez00zzast_nodez00((
																(BgL_typez00_bglt) BgL_tvz00_6494),
															BgL_arg1961z00_6522);
													}
													((((BgL_nodez00_bglt) COBJECT(
																	((BgL_nodez00_bglt) BgL_new1193z00_6519)))->
															BgL_typez00) =
														((BgL_typez00_bglt) BgL_auxz00_8271), BUNSPEC);
												}
												{
													obj_t BgL_auxz00_8278;

													{	/* Cfa/tvector.scm 307 */
														obj_t BgL_arg1962z00_6523;

														BgL_arg1962z00_6523 =
															(((BgL_kwotez00_bglt) COBJECT(
																	((BgL_kwotez00_bglt)
																		((BgL_kwotez00_bglt) BgL_knodez00_6194))))->
															BgL_valuez00);
														{	/* Cfa/tvector.scm 307 */
															obj_t BgL_newz00_6524;

															BgL_newz00_6524 =
																create_struct(CNST_TABLE_REF(22), (int) (2L));
															{	/* Cfa/tvector.scm 307 */
																int BgL_tmpz00_8285;

																BgL_tmpz00_8285 = (int) (1L);
																STRUCT_SET(BgL_newz00_6524, BgL_tmpz00_8285,
																	BgL_arg1962z00_6523);
															}
															{	/* Cfa/tvector.scm 307 */
																int BgL_tmpz00_8288;

																BgL_tmpz00_8288 = (int) (0L);
																STRUCT_SET(BgL_newz00_6524, BgL_tmpz00_8288,
																	BgL_tvz00_6494);
															}
															BgL_auxz00_8278 = BgL_newz00_6524;
													}}
													((((BgL_kwotez00_bglt) COBJECT(BgL_new1193z00_6519))->
															BgL_valuez00) =
														((obj_t) BgL_auxz00_8278), BUNSPEC);
												}
												BgL_nz00_6518 = BgL_new1193z00_6519;
											}
											{	/* Cfa/tvector.scm 305 */

												{	/* Cfa/tvector.scm 308 */
													BgL_kwotezf2cinfozf2_bglt BgL_wide1199z00_6525;

													BgL_wide1199z00_6525 =
														((BgL_kwotezf2cinfozf2_bglt)
														BOBJECT(GC_MALLOC(sizeof(struct
																	BgL_kwotezf2cinfozf2_bgl))));
													{	/* Cfa/tvector.scm 308 */
														obj_t BgL_auxz00_8296;
														BgL_objectz00_bglt BgL_tmpz00_8293;

														BgL_auxz00_8296 = ((obj_t) BgL_wide1199z00_6525);
														BgL_tmpz00_8293 =
															((BgL_objectz00_bglt)
															((BgL_kwotez00_bglt) BgL_nz00_6518));
														BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8293,
															BgL_auxz00_8296);
													}
													((BgL_objectz00_bglt)
														((BgL_kwotez00_bglt) BgL_nz00_6518));
													{	/* Cfa/tvector.scm 308 */
														long BgL_arg1960z00_6526;

														BgL_arg1960z00_6526 =
															BGL_CLASS_NUM(BGl_kwotezf2Cinfozf2zzcfa_infoz00);
														BGL_OBJECT_CLASS_NUM_SET(
															((BgL_objectz00_bglt)
																((BgL_kwotez00_bglt) BgL_nz00_6518)),
															BgL_arg1960z00_6526);
													}
													((BgL_kwotez00_bglt)
														((BgL_kwotez00_bglt) BgL_nz00_6518));
												}
												{
													BgL_kwotezf2cinfozf2_bglt BgL_auxz00_8307;

													{
														obj_t BgL_auxz00_8308;

														{	/* Cfa/tvector.scm 309 */
															BgL_objectz00_bglt BgL_tmpz00_8309;

															BgL_tmpz00_8309 =
																((BgL_objectz00_bglt)
																((BgL_kwotez00_bglt) BgL_nz00_6518));
															BgL_auxz00_8308 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_8309);
														}
														BgL_auxz00_8307 =
															((BgL_kwotezf2cinfozf2_bglt) BgL_auxz00_8308);
													}
													((((BgL_kwotezf2cinfozf2_bglt)
																COBJECT(BgL_auxz00_8307))->BgL_approxz00) =
														((BgL_approxz00_bglt) BgL_approxz00_6492), BUNSPEC);
												}
												BgL_auxz00_8196 = ((BgL_kwotez00_bglt) BgL_nz00_6518);
									}}}
								else
									{	/* Cfa/tvector.scm 310 */
										BgL_kwotez00_bglt BgL_tmp1201z00_6527;

										{	/* Cfa/tvector.scm 310 */
											long BgL_arg1968z00_6528;

											{	/* Cfa/tvector.scm 310 */
												obj_t BgL_arg1969z00_6529;

												{	/* Cfa/tvector.scm 310 */
													obj_t BgL_arg1970z00_6530;

													{	/* Cfa/tvector.scm 310 */
														obj_t BgL_arg1815z00_6531;
														long BgL_arg1816z00_6532;

														BgL_arg1815z00_6531 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/tvector.scm 310 */
															long BgL_arg1817z00_6533;

															BgL_arg1817z00_6533 =
																BGL_OBJECT_CLASS_NUM(
																((BgL_objectz00_bglt)
																	((BgL_kwotez00_bglt) BgL_knodez00_6194)));
															BgL_arg1816z00_6532 =
																(BgL_arg1817z00_6533 - OBJECT_TYPE);
														}
														BgL_arg1970z00_6530 =
															VECTOR_REF(BgL_arg1815z00_6531,
															BgL_arg1816z00_6532);
													}
													BgL_arg1969z00_6529 =
														BGl_classzd2superzd2zz__objectz00
														(BgL_arg1970z00_6530);
												}
												{	/* Cfa/tvector.scm 310 */
													obj_t BgL_tmpz00_8323;

													BgL_tmpz00_8323 = ((obj_t) BgL_arg1969z00_6529);
													BgL_arg1968z00_6528 = BGL_CLASS_NUM(BgL_tmpz00_8323);
											}}
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt)
													((BgL_kwotez00_bglt) BgL_knodez00_6194)),
												BgL_arg1968z00_6528);
										}
										{	/* Cfa/tvector.scm 310 */
											BgL_objectz00_bglt BgL_tmpz00_8329;

											BgL_tmpz00_8329 =
												((BgL_objectz00_bglt)
												((BgL_kwotez00_bglt) BgL_knodez00_6194));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8329, BFALSE);
										}
										((BgL_objectz00_bglt)
											((BgL_kwotez00_bglt) BgL_knodez00_6194));
										BgL_tmp1201z00_6527 =
											((BgL_kwotez00_bglt)
											((BgL_kwotez00_bglt) BgL_knodez00_6194));
										{	/* Cfa/tvector.scm 310 */
											BgL_kwotezf2cinfozf2_bglt BgL_wide1203z00_6534;

											BgL_wide1203z00_6534 =
												((BgL_kwotezf2cinfozf2_bglt)
												BOBJECT(GC_MALLOC(sizeof(struct
															BgL_kwotezf2cinfozf2_bgl))));
											{	/* Cfa/tvector.scm 310 */
												obj_t BgL_auxz00_8341;
												BgL_objectz00_bglt BgL_tmpz00_8338;

												BgL_auxz00_8341 = ((obj_t) BgL_wide1203z00_6534);
												BgL_tmpz00_8338 =
													((BgL_objectz00_bglt)
													((BgL_kwotez00_bglt) BgL_tmp1201z00_6527));
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_8338,
													BgL_auxz00_8341);
											}
											((BgL_objectz00_bglt)
												((BgL_kwotez00_bglt) BgL_tmp1201z00_6527));
											{	/* Cfa/tvector.scm 310 */
												long BgL_arg1967z00_6535;

												BgL_arg1967z00_6535 =
													BGL_CLASS_NUM(BGl_kwotezf2Cinfozf2zzcfa_infoz00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt)
														((BgL_kwotez00_bglt) BgL_tmp1201z00_6527)),
													BgL_arg1967z00_6535);
											}
											((BgL_kwotez00_bglt)
												((BgL_kwotez00_bglt) BgL_tmp1201z00_6527));
										}
										{
											BgL_kwotezf2cinfozf2_bglt BgL_auxz00_8352;

											{
												obj_t BgL_auxz00_8353;

												{	/* Cfa/tvector.scm 311 */
													BgL_objectz00_bglt BgL_tmpz00_8354;

													BgL_tmpz00_8354 =
														((BgL_objectz00_bglt)
														((BgL_kwotez00_bglt) BgL_tmp1201z00_6527));
													BgL_auxz00_8353 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_8354);
												}
												BgL_auxz00_8352 =
													((BgL_kwotezf2cinfozf2_bglt) BgL_auxz00_8353);
											}
											((((BgL_kwotezf2cinfozf2_bglt) COBJECT(BgL_auxz00_8352))->
													BgL_approxz00) =
												((BgL_approxz00_bglt) BgL_approxz00_6492), BUNSPEC);
										}
										BgL_auxz00_8196 = ((BgL_kwotez00_bglt) BgL_tmp1201z00_6527);
				}}}}}
				return ((obj_t) BgL_auxz00_8196);
			}
		}

	}



/* &patch!-kwote1650 */
	obj_t BGl_z62patchz12zd2kwote1650za2zzcfa_tvectorz00(obj_t BgL_envz00_6195,
		obj_t BgL_nodez00_6196)
	{
		{	/* Cfa/tvector.scm 293 */
			return ((obj_t) ((BgL_kwotez00_bglt) BgL_nodez00_6196));
		}

	}



/* &patch!-atom1648 */
	obj_t BGl_z62patchz12zd2atom1648za2zzcfa_tvectorz00(obj_t BgL_envz00_6197,
		obj_t BgL_nodez00_6198)
	{
		{	/* Cfa/tvector.scm 287 */
			return ((obj_t) ((BgL_atomz00_bglt) BgL_nodez00_6198));
		}

	}



/* &get-vector-item-type1644 */
	BgL_typez00_bglt
		BGl_z62getzd2vectorzd2itemzd2type1644zb0zzcfa_tvectorz00(obj_t
		BgL_envz00_6199, obj_t BgL_nodez00_6200)
	{
		{	/* Cfa/tvector.scm 215 */
			{	/* Cfa/tvector.scm 217 */
				bool_t BgL_test2596z00_8366;

				{
					BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8367;

					{
						obj_t BgL_auxz00_8368;

						{	/* Cfa/tvector.scm 217 */
							BgL_objectz00_bglt BgL_tmpz00_8369;

							BgL_tmpz00_8369 =
								((BgL_objectz00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_6200));
							BgL_auxz00_8368 = BGL_OBJECT_WIDENING(BgL_tmpz00_8369);
						}
						BgL_auxz00_8367 =
							((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8368);
					}
					BgL_test2596z00_8366 =
						(((BgL_valloczf2cinfozb2optimz40_bglt) COBJECT(BgL_auxz00_8367))->
						BgL_seenzf3zf3);
				}
				if (BgL_test2596z00_8366)
					{	/* Cfa/tvector.scm 219 */
						BgL_approxz00_bglt BgL_arg1958z00_6539;

						{
							BgL_valloczf2cinfozb2optimz40_bglt BgL_auxz00_8375;

							{
								obj_t BgL_auxz00_8376;

								{	/* Cfa/tvector.scm 219 */
									BgL_objectz00_bglt BgL_tmpz00_8377;

									BgL_tmpz00_8377 =
										((BgL_objectz00_bglt)
										((BgL_vallocz00_bglt) BgL_nodez00_6200));
									BgL_auxz00_8376 = BGL_OBJECT_WIDENING(BgL_tmpz00_8377);
								}
								BgL_auxz00_8375 =
									((BgL_valloczf2cinfozb2optimz40_bglt) BgL_auxz00_8376);
							}
							BgL_arg1958z00_6539 =
								(((BgL_valloczf2cinfozb2optimz40_bglt)
									COBJECT(BgL_auxz00_8375))->BgL_valuezd2approxzd2);
						}
						return
							(((BgL_approxz00_bglt) COBJECT(BgL_arg1958z00_6539))->
							BgL_typez00);
					}
				else
					{	/* Cfa/tvector.scm 217 */
						return ((BgL_typez00_bglt) BGl_za2vectorza2z00zztype_cachez00);
					}
			}
		}

	}



/* &get-vector-item-type1642 */
	BgL_typez00_bglt
		BGl_z62getzd2vectorzd2itemzd2type1642zb0zzcfa_tvectorz00(obj_t
		BgL_envz00_6201, obj_t BgL_appz00_6202)
	{
		{	/* Cfa/tvector.scm 206 */
			{	/* Cfa/tvector.scm 208 */
				bool_t BgL_test2597z00_8385;

				{
					BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_8386;

					{
						obj_t BgL_auxz00_8387;

						{	/* Cfa/tvector.scm 208 */
							BgL_objectz00_bglt BgL_tmpz00_8388;

							BgL_tmpz00_8388 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_appz00_6202));
							BgL_auxz00_8387 = BGL_OBJECT_WIDENING(BgL_tmpz00_8388);
						}
						BgL_auxz00_8386 =
							((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_8387);
					}
					BgL_test2597z00_8385 =
						(((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_8386))->
						BgL_seenzf3zf3);
				}
				if (BgL_test2597z00_8385)
					{	/* Cfa/tvector.scm 210 */
						BgL_approxz00_bglt BgL_arg1956z00_6541;

						{
							BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_8394;

							{
								obj_t BgL_auxz00_8395;

								{	/* Cfa/tvector.scm 210 */
									BgL_objectz00_bglt BgL_tmpz00_8396;

									BgL_tmpz00_8396 =
										((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_appz00_6202));
									BgL_auxz00_8395 = BGL_OBJECT_WIDENING(BgL_tmpz00_8396);
								}
								BgL_auxz00_8394 =
									((BgL_makezd2vectorzd2appz00_bglt) BgL_auxz00_8395);
							}
							BgL_arg1956z00_6541 =
								(((BgL_makezd2vectorzd2appz00_bglt) COBJECT(BgL_auxz00_8394))->
								BgL_valuezd2approxzd2);
						}
						return
							(((BgL_approxz00_bglt) COBJECT(BgL_arg1956z00_6541))->
							BgL_typez00);
					}
				else
					{	/* Cfa/tvector.scm 208 */
						return ((BgL_typez00_bglt) BGl_za2vectorza2z00zztype_cachez00);
					}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_tvectorz00(void)
	{
		{	/* Cfa/tvector.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzmodule_typez00(410405073L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzmodule_pragmaz00(433305536L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zztype_envz00(296457443L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zztvector_tvectorz00(501518119L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzast_buildz00(428035925L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzast_sexpz00(163122759L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzast_lvtypez00(189769752L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzast_walkz00(343174225L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzcfa_info3z00(0L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzcfa_cfaz00(400853817L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzcfa_setz00(5932721L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzcfa_typez00(93933605L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzglobaliza7e_walkza7(69163963L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			BGl_modulezd2initializa7ationz75zzinline_inlinez00(20504962L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
			return
				BGl_modulezd2initializa7ationz75zzinline_walkz00(385476819L,
				BSTRING_TO_STRING(BGl_string2397z00zzcfa_tvectorz00));
		}

	}

#ifdef __cplusplus
}
#endif
