/*===========================================================================*/
/*   (Cfa/set.scm)                                                           */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/set.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_SET_TYPE_DEFINITIONS
#define BGL_CFA_SET_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_nodezf2effectzf2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
	}                       *BgL_nodezf2effectzf2_bglt;


#endif													// BGL_CFA_SET_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_z62makezd2setz12za2zzcfa_setz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_setzd2lengthzd2zzcfa_setz00(obj_t);
	static obj_t BGl_z62setzd2anyzb0zzcfa_setz00(obj_t, obj_t, obj_t);
	static obj_t BGl_z62setzd2extendz12za2zzcfa_setz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_makezd2setz12zc0zzcfa_setz00(obj_t);
	static obj_t BGl_z62setzd2lengthzb0zzcfa_setz00(obj_t, obj_t);
	static obj_t BGl_z62setzd2ze3vectorz53zzcfa_setz00(obj_t, obj_t);
	static obj_t BGl_z62declarezd2setz12za2zzcfa_setz00(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzcfa_setz00(void);
	static obj_t BGl_setzd2unionzd22z12z12zzcfa_setz00(obj_t, obj_t);
	static obj_t BGl_methodzd2initzd2zzcfa_setz00(void);
	BGL_IMPORT obj_t make_string(long, unsigned char);
	BGL_EXPORTED_DECL obj_t BGl_setzd2anyzd2zzcfa_setz00(obj_t, obj_t);
	static obj_t BGl_z62setzd2everyzb0zzcfa_setz00(obj_t, obj_t, obj_t);
	BGL_IMPORT obj_t create_struct(obj_t, int);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_setz00(void);
	BGL_EXPORTED_DECL obj_t BGl_setzd2everyzd2zzcfa_setz00(obj_t, obj_t);
	BGL_EXPORTED_DECL bool_t BGl_setzf3zf3zzcfa_setz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_setz00 = BUNSPEC;
	static obj_t BGl_z62setzd2ze3listz53zzcfa_setz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_setzd2memberzf3z21zzcfa_setz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzcfa_setz00(void);
	BGL_EXPORTED_DECL obj_t BGl_setzd2ze3listz31zzcfa_setz00(obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcfa_setz00(void);
	static obj_t BGl__setzd2headzd2zzcfa_setz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_setzd2headzd2zzcfa_setz00(obj_t, obj_t);
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62setzd2memberzf3z43zzcfa_setz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_setzd2forzd2eachz00zzcfa_setz00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_setzd2unionz12zc0zzcfa_setz00(obj_t, obj_t);
	static obj_t BGl_z62setzd2unionz12za2zzcfa_setz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_setz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__structurez00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	static obj_t BGl_cnstzd2initzd2zzcfa_setz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_setz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_setz00(void);
	BGL_EXPORTED_DECL obj_t BGl_declarezd2setz12zc0zzcfa_setz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_setzd2extendz12zc0zzcfa_setz00(obj_t, obj_t);
	static obj_t BGl_z62setzd2forzd2eachz62zzcfa_setz00(obj_t, obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_setzd2ze3vectorz31zzcfa_setz00(obj_t);
	static obj_t BGl_z62setzf3z91zzcfa_setz00(obj_t, obj_t);
	static obj_t __cnst[2];


	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzd2memberzf3zd2envzf3zzcfa_setz00,
		BgL_bgl_za762setza7d2memberza72015za7,
		BGl_z62setzd2memberzf3z43zzcfa_setz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_makezd2setz12zd2envz12zzcfa_setz00,
		BgL_bgl_za762makeza7d2setza7122016za7, BGl_z62makezd2setz12za2zzcfa_setz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzd2forzd2eachzd2envzd2zzcfa_setz00,
		BgL_bgl_za762setza7d2forza7d2e2017za7,
		BGl_z62setzd2forzd2eachz62zzcfa_setz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzd2ze3vectorzd2envze3zzcfa_setz00,
		BgL_bgl_za762setza7d2za7e3vect2018za7,
		BGl_z62setzd2ze3vectorz53zzcfa_setz00, 0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzd2everyzd2envz00zzcfa_setz00,
		BgL_bgl_za762setza7d2everyza7b2019za7, BGl_z62setzd2everyzb0zzcfa_setz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzd2lengthzd2envz00zzcfa_setz00,
		BgL_bgl_za762setza7d2lengthza72020za7, BGl_z62setzd2lengthzb0zzcfa_setz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzd2ze3listzd2envze3zzcfa_setz00,
		BgL_bgl_za762setza7d2za7e3list2021za7, BGl_z62setzd2ze3listz53zzcfa_setz00,
		0L, BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzd2unionz12zd2envz12zzcfa_setz00,
		BgL_bgl_za762setza7d2unionza712022za7, va_generic_entry,
		BGl_z62setzd2unionz12za2zzcfa_setz00, BUNSPEC, -2);
	      DEFINE_STRING(BGl_string2008z00zzcfa_setz00,
		BgL_bgl_string2008za700za7za7c2023za7, "make-set", 8);
	      DEFINE_STRING(BGl_string2009z00zzcfa_setz00,
		BgL_bgl_string2009za700za7za7c2024za7, "Not a meta-set", 14);
	      DEFINE_STRING(BGl_string2010z00zzcfa_setz00,
		BgL_bgl_string2010za700za7za7c2025za7, "set-union!", 10);
	      DEFINE_STRING(BGl_string2011z00zzcfa_setz00,
		BgL_bgl_string2011za700za7za7c2026za7, "Incompatible sets", 17);
	      DEFINE_STRING(BGl_string2012z00zzcfa_setz00,
		BgL_bgl_string2012za700za7za7c2027za7, "cfa_set", 7);
	      DEFINE_STRING(BGl_string2013z00zzcfa_setz00,
		BgL_bgl_string2013za700za7za7c2028za7, "large-set meta-set ", 19);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzd2extendz12zd2envz12zzcfa_setz00,
		BgL_bgl_za762setza7d2extendza72029za7,
		BGl_z62setzd2extendz12za2zzcfa_setz00, 0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzf3zd2envz21zzcfa_setz00,
		BgL_bgl_za762setza7f3za791za7za7cf2030za7, BGl_z62setzf3z91zzcfa_setz00, 0L,
		BUNSPEC, 1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzd2headzd2envz00zzcfa_setz00,
		BgL_bgl__setza7d2headza7d2za7za72031z00, opt_generic_entry,
		BGl__setzd2headzd2zzcfa_setz00, BFALSE, -1);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_setzd2anyzd2envz00zzcfa_setz00,
		BgL_bgl_za762setza7d2anyza7b0za72032z00, BGl_z62setzd2anyzb0zzcfa_setz00,
		0L, BUNSPEC, 2);
	      DEFINE_EXPORT_BGL_PROCEDURE(BGl_declarezd2setz12zd2envz12zzcfa_setz00,
		BgL_bgl_za762declareza7d2set2033z00, BGl_z62declarezd2setz12za2zzcfa_setz00,
		0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_setz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_setz00(long
		BgL_checksumz00_4034, char *BgL_fromz00_4035)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_setz00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_setz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_setz00();
					BGl_libraryzd2moduleszd2initz00zzcfa_setz00();
					BGl_cnstzd2initzd2zzcfa_setz00();
					BGl_importedzd2moduleszd2initz00zzcfa_setz00();
					return BGl_toplevelzd2initzd2zzcfa_setz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_setz00(void)
	{
		{	/* Cfa/set.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_set");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_set");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_set");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cfa_set");
			BGl_modulezd2initializa7ationz75zz__r4_characters_6_6z00(0L, "cfa_set");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_set");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_set");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_set");
			BGl_modulezd2initializa7ationz75zz__structurez00(0L, "cfa_set");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cfa_set");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_setz00(void)
	{
		{	/* Cfa/set.scm 15 */
			{	/* Cfa/set.scm 15 */
				obj_t BgL_cportz00_4021;

				{	/* Cfa/set.scm 15 */
					obj_t BgL_stringz00_4028;

					BgL_stringz00_4028 = BGl_string2013z00zzcfa_setz00;
					{	/* Cfa/set.scm 15 */
						obj_t BgL_startz00_4029;

						BgL_startz00_4029 = BINT(0L);
						{	/* Cfa/set.scm 15 */
							obj_t BgL_endz00_4030;

							BgL_endz00_4030 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4028)));
							{	/* Cfa/set.scm 15 */

								BgL_cportz00_4021 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4028, BgL_startz00_4029, BgL_endz00_4030);
				}}}}
				{
					long BgL_iz00_4022;

					BgL_iz00_4022 = 1L;
				BgL_loopz00_4023:
					if ((BgL_iz00_4022 == -1L))
						{	/* Cfa/set.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/set.scm 15 */
							{	/* Cfa/set.scm 15 */
								obj_t BgL_arg2014z00_4024;

								{	/* Cfa/set.scm 15 */

									{	/* Cfa/set.scm 15 */
										obj_t BgL_locationz00_4026;

										BgL_locationz00_4026 = BBOOL(((bool_t) 0));
										{	/* Cfa/set.scm 15 */

											BgL_arg2014z00_4024 =
												BGl_readz00zz__readerz00(BgL_cportz00_4021,
												BgL_locationz00_4026);
										}
									}
								}
								{	/* Cfa/set.scm 15 */
									int BgL_tmpz00_4063;

									BgL_tmpz00_4063 = (int) (BgL_iz00_4022);
									CNST_TABLE_SET(BgL_tmpz00_4063, BgL_arg2014z00_4024);
							}}
							{	/* Cfa/set.scm 15 */
								int BgL_auxz00_4027;

								BgL_auxz00_4027 = (int) ((BgL_iz00_4022 - 1L));
								{
									long BgL_iz00_4068;

									BgL_iz00_4068 = (long) (BgL_auxz00_4027);
									BgL_iz00_4022 = BgL_iz00_4068;
									goto BgL_loopz00_4023;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_setz00(void)
	{
		{	/* Cfa/set.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_setz00(void)
	{
		{	/* Cfa/set.scm 15 */
			return BUNSPEC;
		}

	}



/* declare-set! */
	BGL_EXPORTED_DEF obj_t BGl_declarezd2setz12zc0zzcfa_setz00(obj_t
		BgL_tablez00_23)
	{
		{	/* Cfa/set.scm 43 */
			{	/* Cfa/set.scm 44 */
				long BgL_quotientz00_3035;

				{	/* Cfa/set.scm 45 */
					long BgL_tmpz00_4071;

					BgL_tmpz00_4071 = VECTOR_LENGTH(BgL_tablez00_23);
					BgL_quotientz00_3035 = (BgL_tmpz00_4071 / 8L);
				}
				{	/* Cfa/set.scm 45 */
					long BgL_remainderz00_3036;

					{	/* Cfa/set.scm 46 */
						long BgL_n1z00_3721;
						long BgL_n2z00_3722;

						BgL_n1z00_3721 = VECTOR_LENGTH(BgL_tablez00_23);
						BgL_n2z00_3722 = 8L;
						{	/* Cfa/set.scm 46 */
							bool_t BgL_test2036z00_4075;

							{	/* Cfa/set.scm 46 */
								long BgL_arg1338z00_3724;

								BgL_arg1338z00_3724 =
									(((BgL_n1z00_3721) | (BgL_n2z00_3722)) & -2147483648);
								BgL_test2036z00_4075 = (BgL_arg1338z00_3724 == 0L);
							}
							if (BgL_test2036z00_4075)
								{	/* Cfa/set.scm 46 */
									int32_t BgL_arg1334z00_3725;

									{	/* Cfa/set.scm 46 */
										int32_t BgL_arg1336z00_3726;
										int32_t BgL_arg1337z00_3727;

										BgL_arg1336z00_3726 = (int32_t) (BgL_n1z00_3721);
										BgL_arg1337z00_3727 = (int32_t) (BgL_n2z00_3722);
										BgL_arg1334z00_3725 =
											(BgL_arg1336z00_3726 % BgL_arg1337z00_3727);
									}
									{	/* Cfa/set.scm 46 */
										long BgL_arg1446z00_3732;

										BgL_arg1446z00_3732 = (long) (BgL_arg1334z00_3725);
										BgL_remainderz00_3036 = (long) (BgL_arg1446z00_3732);
								}}
							else
								{	/* Cfa/set.scm 46 */
									BgL_remainderz00_3036 = (BgL_n1z00_3721 % BgL_n2z00_3722);
								}
						}
					}
					{	/* Cfa/set.scm 46 */
						long BgL_siza7eza7_3037;

						if ((BgL_remainderz00_3036 == 0L))
							{	/* Cfa/set.scm 47 */
								BgL_siza7eza7_3037 = (BgL_quotientz00_3035 + 1L);
							}
						else
							{	/* Cfa/set.scm 47 */
								BgL_siza7eza7_3037 = (BgL_quotientz00_3035 + 2L);
							}
						{	/* Cfa/set.scm 47 */

							{
								long BgL_iz00_3039;
								long BgL_quotientz00_3040;
								long BgL_maskz00_3041;

								BgL_iz00_3039 = 0L;
								BgL_quotientz00_3040 = 0L;
								BgL_maskz00_3041 = 1L;
							BgL_zc3z04anonymousza31565ze3z87_3042:
								if ((BgL_iz00_3039 == VECTOR_LENGTH(BgL_tablez00_23)))
									{	/* Cfa/set.scm 53 */
										obj_t BgL_newz00_3739;

										BgL_newz00_3739 =
											create_struct(CNST_TABLE_REF(0), (int) (2L));
										{	/* Cfa/set.scm 53 */
											obj_t BgL_auxz00_4096;
											int BgL_tmpz00_4094;

											BgL_auxz00_4096 = BINT(BgL_siza7eza7_3037);
											BgL_tmpz00_4094 = (int) (1L);
											STRUCT_SET(BgL_newz00_3739, BgL_tmpz00_4094,
												BgL_auxz00_4096);
										}
										{	/* Cfa/set.scm 53 */
											int BgL_tmpz00_4099;

											BgL_tmpz00_4099 = (int) (0L);
											STRUCT_SET(BgL_newz00_3739, BgL_tmpz00_4099,
												BgL_tablez00_23);
										}
										return BgL_newz00_3739;
									}
								else
									{	/* Cfa/set.scm 52 */
										if ((BgL_maskz00_3041 == 256L))
											{
												long BgL_maskz00_4106;
												long BgL_quotientz00_4104;

												BgL_quotientz00_4104 = (BgL_quotientz00_3040 + 1L);
												BgL_maskz00_4106 = 1L;
												BgL_maskz00_3041 = BgL_maskz00_4106;
												BgL_quotientz00_3040 = BgL_quotientz00_4104;
												goto BgL_zc3z04anonymousza31565ze3z87_3042;
											}
										else
											{	/* Cfa/set.scm 54 */
												{	/* Cfa/set.scm 57 */
													obj_t BgL_arg1573z00_3046;
													obj_t BgL_arg1575z00_3047;

													BgL_arg1573z00_3046 =
														VECTOR_REF(BgL_tablez00_23, BgL_iz00_3039);
													BgL_arg1575z00_3047 =
														MAKE_YOUNG_PAIR(BINT(BgL_quotientz00_3040),
														BINT(BgL_maskz00_3041));
													((((BgL_nodezf2effectzf2_bglt) COBJECT(
																	((BgL_nodezf2effectzf2_bglt)
																		BgL_arg1573z00_3046)))->BgL_keyz00) =
														((obj_t) BgL_arg1575z00_3047), BUNSPEC);
												}
												{
													long BgL_maskz00_4115;
													long BgL_iz00_4113;

													BgL_iz00_4113 = (BgL_iz00_3039 + 1L);
													BgL_maskz00_4115 = (BgL_maskz00_3041 * 2L);
													BgL_maskz00_3041 = BgL_maskz00_4115;
													BgL_iz00_3039 = BgL_iz00_4113;
													goto BgL_zc3z04anonymousza31565ze3z87_3042;
												}
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* &declare-set! */
	obj_t BGl_z62declarezd2setz12za2zzcfa_setz00(obj_t BgL_envz00_3989,
		obj_t BgL_tablez00_3990)
	{
		{	/* Cfa/set.scm 43 */
			return BGl_declarezd2setz12zc0zzcfa_setz00(BgL_tablez00_3990);
		}

	}



/* make-set! */
	BGL_EXPORTED_DEF obj_t BGl_makezd2setz12zc0zzcfa_setz00(obj_t
		BgL_metazd2setzd2_24)
	{
		{	/* Cfa/set.scm 63 */
			{	/* Cfa/set.scm 65 */
				bool_t BgL_test2040z00_4118;

				if (STRUCTP(BgL_metazd2setzd2_24))
					{	/* Cfa/set.scm 65 */
						BgL_test2040z00_4118 =
							(STRUCT_KEY(BgL_metazd2setzd2_24) == CNST_TABLE_REF(0));
					}
				else
					{	/* Cfa/set.scm 65 */
						BgL_test2040z00_4118 = ((bool_t) 0);
					}
				if (BgL_test2040z00_4118)
					{	/* Cfa/set.scm 68 */
						obj_t BgL_arg1589z00_3053;

						{	/* Cfa/set.scm 68 */
							obj_t BgL_arg1591z00_3054;

							BgL_arg1591z00_3054 =
								STRUCT_REF(BgL_metazd2setzd2_24, (int) (1L));
							BgL_arg1589z00_3053 =
								make_string(
								(long) CINT(BgL_arg1591z00_3054), ((unsigned char) '\000'));
						}
						{	/* Cfa/set.scm 68 */
							obj_t BgL_newz00_3756;

							BgL_newz00_3756 = create_struct(CNST_TABLE_REF(1), (int) (2L));
							{	/* Cfa/set.scm 68 */
								int BgL_tmpz00_4131;

								BgL_tmpz00_4131 = (int) (1L);
								STRUCT_SET(BgL_newz00_3756, BgL_tmpz00_4131,
									BgL_metazd2setzd2_24);
							}
							{	/* Cfa/set.scm 68 */
								int BgL_tmpz00_4134;

								BgL_tmpz00_4134 = (int) (0L);
								STRUCT_SET(BgL_newz00_3756, BgL_tmpz00_4134,
									BgL_arg1589z00_3053);
							}
							return BgL_newz00_3756;
						}
					}
				else
					{	/* Cfa/set.scm 65 */
						return
							BGl_internalzd2errorzd2zztools_errorz00
							(BGl_string2008z00zzcfa_setz00, BGl_string2009z00zzcfa_setz00,
							BGl_shapez00zztools_shapez00(BgL_metazd2setzd2_24));
					}
			}
		}

	}



/* &make-set! */
	obj_t BGl_z62makezd2setz12za2zzcfa_setz00(obj_t BgL_envz00_3991,
		obj_t BgL_metazd2setzd2_3992)
	{
		{	/* Cfa/set.scm 63 */
			return BGl_makezd2setz12zc0zzcfa_setz00(BgL_metazd2setzd2_3992);
		}

	}



/* set? */
	BGL_EXPORTED_DEF bool_t BGl_setzf3zf3zzcfa_setz00(obj_t BgL_objz00_25)
	{
		{	/* Cfa/set.scm 74 */
			if (STRUCTP(BgL_objz00_25))
				{	/* Cfa/set.scm 75 */
					return (STRUCT_KEY(BgL_objz00_25) == CNST_TABLE_REF(1));
				}
			else
				{	/* Cfa/set.scm 75 */
					return ((bool_t) 0);
				}
		}

	}



/* &set? */
	obj_t BGl_z62setzf3z91zzcfa_setz00(obj_t BgL_envz00_3993,
		obj_t BgL_objz00_3994)
	{
		{	/* Cfa/set.scm 74 */
			return BBOOL(BGl_setzf3zf3zzcfa_setz00(BgL_objz00_3994));
		}

	}



/* set-extend! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2extendz12zc0zzcfa_setz00(obj_t BgL_setz00_26,
		obj_t BgL_objz00_27)
	{
		{	/* Cfa/set.scm 80 */
			{	/* Cfa/set.scm 81 */
				obj_t BgL_keyz00_3056;

				BgL_keyz00_3056 =
					(((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_objz00_27)))->BgL_keyz00);
				{	/* Cfa/set.scm 81 */
					obj_t BgL_thezd2setzd2_3057;

					BgL_thezd2setzd2_3057 =
						STRUCT_REF(((obj_t) BgL_setz00_26), (int) (0L));
					{	/* Cfa/set.scm 82 */
						obj_t BgL_quotientz00_3058;

						BgL_quotientz00_3058 = CAR(((obj_t) BgL_keyz00_3056));
						{	/* Cfa/set.scm 83 */
							obj_t BgL_maskz00_3059;

							BgL_maskz00_3059 = CDR(((obj_t) BgL_keyz00_3056));
							{	/* Cfa/set.scm 84 */

								{	/* Cfa/set.scm 86 */
									unsigned char BgL_arg1594z00_3060;

									BgL_arg1594z00_3060 =
										(
										((long) CINT(BgL_maskz00_3059)) |
										STRING_REF(
											((obj_t) BgL_thezd2setzd2_3057),
											(long) CINT(BgL_quotientz00_3058)));
									{	/* Cfa/set.scm 85 */
										long BgL_kz00_3774;

										BgL_kz00_3774 = (long) CINT(BgL_quotientz00_3058);
										{	/* Cfa/set.scm 85 */
											obj_t BgL_tmpz00_4163;

											BgL_tmpz00_4163 = ((obj_t) BgL_thezd2setzd2_3057);
											STRING_SET(BgL_tmpz00_4163, BgL_kz00_3774,
												BgL_arg1594z00_3060);
								}}}
								return BUNSPEC;
							}
						}
					}
				}
			}
		}

	}



/* &set-extend! */
	obj_t BGl_z62setzd2extendz12za2zzcfa_setz00(obj_t BgL_envz00_3995,
		obj_t BgL_setz00_3996, obj_t BgL_objz00_3997)
	{
		{	/* Cfa/set.scm 80 */
			return
				BGl_setzd2extendz12zc0zzcfa_setz00(BgL_setz00_3996, BgL_objz00_3997);
		}

	}



/* set-member? */
	BGL_EXPORTED_DEF obj_t BGl_setzd2memberzf3z21zzcfa_setz00(obj_t BgL_setz00_28,
		obj_t BgL_objz00_29)
	{
		{	/* Cfa/set.scm 92 */
			{	/* Cfa/set.scm 93 */
				obj_t BgL_keyz00_4032;

				BgL_keyz00_4032 =
					(((BgL_nodezf2effectzf2_bglt) COBJECT(
							((BgL_nodezf2effectzf2_bglt) BgL_objz00_29)))->BgL_keyz00);
				{	/* Cfa/set.scm 96 */

					{	/* Cfa/set.scm 97 */
						bool_t BgL_tmpz00_4169;

						{	/* Cfa/set.scm 97 */
							long BgL_tmpz00_4170;

							{	/* Cfa/set.scm 97 */
								long BgL_tmpz00_4171;

								{	/* Cfa/set.scm 97 */
									unsigned char BgL_tmpz00_4175;

									{	/* Cfa/set.scm 97 */
										obj_t BgL_stringz00_4033;

										BgL_stringz00_4033 =
											STRUCT_REF(((obj_t) BgL_setz00_28), (int) (0L));
										BgL_tmpz00_4175 =
											STRING_REF(BgL_stringz00_4033,
											(long) CINT(CAR(((obj_t) BgL_keyz00_4032))));
									}
									BgL_tmpz00_4171 = (BgL_tmpz00_4175);
								}
								BgL_tmpz00_4170 =
									(
									(long) CINT(CDR(
											((obj_t) BgL_keyz00_4032))) & BgL_tmpz00_4171);
							}
							BgL_tmpz00_4169 = (BgL_tmpz00_4170 > 0L);
						}
						return BBOOL(BgL_tmpz00_4169);
					}
				}
			}
		}

	}



/* &set-member? */
	obj_t BGl_z62setzd2memberzf3z43zzcfa_setz00(obj_t BgL_envz00_3998,
		obj_t BgL_setz00_3999, obj_t BgL_objz00_4000)
	{
		{	/* Cfa/set.scm 92 */
			return
				BGl_setzd2memberzf3z21zzcfa_setz00(BgL_setz00_3999, BgL_objz00_4000);
		}

	}



/* set-union-2! */
	obj_t BGl_setzd2unionzd22z12z12zzcfa_setz00(obj_t BgL_dstz00_30,
		obj_t BgL_srcz00_31)
	{
		{	/* Cfa/set.scm 105 */
			{

				{	/* Cfa/set.scm 122 */
					bool_t BgL_test2043z00_4188;

					if (STRUCTP(BgL_srcz00_31))
						{	/* Cfa/set.scm 122 */
							BgL_test2043z00_4188 =
								(STRUCT_KEY(BgL_srcz00_31) == CNST_TABLE_REF(1));
						}
					else
						{	/* Cfa/set.scm 122 */
							BgL_test2043z00_4188 = ((bool_t) 0);
						}
					if (BgL_test2043z00_4188)
						{	/* Cfa/set.scm 124 */
							bool_t BgL_test2045z00_4194;

							{	/* Cfa/set.scm 124 */
								long BgL_tmpz00_4195;

								{	/* Cfa/set.scm 124 */
									obj_t BgL_stringz00_3820;

									BgL_stringz00_3820 =
										STRUCT_REF(((obj_t) BgL_dstz00_30), (int) (0L));
									BgL_tmpz00_4195 = STRING_LENGTH(BgL_stringz00_3820);
								}
								BgL_test2045z00_4194 =
									(BgL_tmpz00_4195 ==
									STRING_LENGTH(STRUCT_REF(BgL_srcz00_31, (int) (0L))));
							}
							if (BgL_test2045z00_4194)
								{	/* Cfa/set.scm 127 */
									bool_t BgL_tmpz00_4204;

									{	/* Cfa/set.scm 107 */
										obj_t BgL_thezd2dstzd2_3084;
										obj_t BgL_thezd2srczd2_3085;

										BgL_thezd2dstzd2_3084 =
											STRUCT_REF(((obj_t) BgL_dstz00_30), (int) (0L));
										BgL_thezd2srczd2_3085 =
											STRUCT_REF(((obj_t) BgL_srcz00_31), (int) (0L));
										{	/* Cfa/set.scm 109 */
											long BgL_g1155z00_3086;

											{	/* Cfa/set.scm 109 */
												long BgL_tmpz00_4211;

												{	/* Cfa/set.scm 109 */
													obj_t BgL_sz00_3797;

													BgL_sz00_3797 =
														STRUCT_REF(((obj_t) BgL_dstz00_30), (int) (1L));
													BgL_tmpz00_4211 =
														(long) CINT(STRUCT_REF(BgL_sz00_3797, (int) (1L)));
												}
												BgL_g1155z00_3086 = (BgL_tmpz00_4211 - 1L);
											}
											{
												long BgL_iz00_3088;
												bool_t BgL_resz00_3089;

												BgL_iz00_3088 = BgL_g1155z00_3086;
												BgL_resz00_3089 = ((bool_t) 0);
											BgL_zc3z04anonymousza31644ze3z87_3090:
												if ((BgL_iz00_3088 == -1L))
													{	/* Cfa/set.scm 112 */
														BgL_tmpz00_4204 = BgL_resz00_3089;
													}
												else
													{	/* Cfa/set.scm 114 */
														unsigned char BgL_oldz00_3092;
														unsigned char BgL_newz00_3093;

														BgL_oldz00_3092 =
															STRING_REF(
															((obj_t) BgL_thezd2dstzd2_3084), BgL_iz00_3088);
														BgL_newz00_3093 =
															(STRING_REF(
																((obj_t) BgL_thezd2dstzd2_3084),
																BgL_iz00_3088) | STRING_REF(((obj_t)
																	BgL_thezd2srczd2_3085), BgL_iz00_3088));
														if ((BgL_newz00_3093 == BgL_oldz00_3092))
															{
																long BgL_iz00_4230;

																BgL_iz00_4230 = (BgL_iz00_3088 - 1L);
																BgL_iz00_3088 = BgL_iz00_4230;
																goto BgL_zc3z04anonymousza31644ze3z87_3090;
															}
														else
															{	/* Cfa/set.scm 117 */
																{	/* Cfa/set.scm 120 */
																	obj_t BgL_tmpz00_4232;

																	BgL_tmpz00_4232 =
																		((obj_t) BgL_thezd2dstzd2_3084);
																	STRING_SET(BgL_tmpz00_4232, BgL_iz00_3088,
																		BgL_newz00_3093);
																}
																{
																	bool_t BgL_resz00_4237;
																	long BgL_iz00_4235;

																	BgL_iz00_4235 = (BgL_iz00_3088 - 1L);
																	BgL_resz00_4237 = ((bool_t) 1);
																	BgL_resz00_3089 = BgL_resz00_4237;
																	BgL_iz00_3088 = BgL_iz00_4235;
																	goto BgL_zc3z04anonymousza31644ze3z87_3090;
																}
															}
													}
											}
										}
									}
									return BBOOL(BgL_tmpz00_4204);
								}
							else
								{	/* Cfa/set.scm 124 */
									return
										BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string2010z00zzcfa_setz00,
										BGl_string2011z00zzcfa_setz00,
										BGl_shapez00zztools_shapez00(BgL_srcz00_31));
								}
						}
					else
						{	/* Cfa/set.scm 122 */
							return
								BGl_internalzd2errorzd2zztools_errorz00
								(BGl_string2010z00zzcfa_setz00, BGl_string2011z00zzcfa_setz00,
								BGl_shapez00zztools_shapez00(BgL_srcz00_31));
						}
				}
			}
		}

	}



/* set-union! */
	BGL_EXPORTED_DEF obj_t BGl_setzd2unionz12zc0zzcfa_setz00(obj_t BgL_dstz00_32,
		obj_t BgL_srcza2za2_33)
	{
		{	/* Cfa/set.scm 135 */
			if (NULLP(BgL_srcza2za2_33))
				{	/* Cfa/set.scm 137 */
					return BgL_dstz00_32;
				}
			else
				{	/* Cfa/set.scm 137 */
					if (NULLP(CDR(((obj_t) BgL_srcza2za2_33))))
						{	/* Cfa/set.scm 140 */
							obj_t BgL_arg1681z00_3106;

							BgL_arg1681z00_3106 = CAR(((obj_t) BgL_srcza2za2_33));
							BGL_TAIL return
								BGl_setzd2unionzd22z12z12zzcfa_setz00(BgL_dstz00_32,
								BgL_arg1681z00_3106);
						}
					else
						{
							obj_t BgL_srcza2za2_3108;
							obj_t BgL_resz00_3109;

							BgL_srcza2za2_3108 = BgL_srcza2za2_33;
							BgL_resz00_3109 = BFALSE;
						BgL_zc3z04anonymousza31682ze3z87_3110:
							if (NULLP(BgL_srcza2za2_3108))
								{	/* Cfa/set.scm 144 */
									return BgL_resz00_3109;
								}
							else
								{	/* Cfa/set.scm 146 */
									obj_t BgL_arg1688z00_3112;
									obj_t BgL_arg1689z00_3113;

									BgL_arg1688z00_3112 = CDR(((obj_t) BgL_srcza2za2_3108));
									{	/* Cfa/set.scm 147 */
										obj_t BgL__ortest_1156z00_3114;

										{	/* Cfa/set.scm 147 */
											obj_t BgL_arg1691z00_3115;

											BgL_arg1691z00_3115 = CAR(((obj_t) BgL_srcza2za2_3108));
											BgL__ortest_1156z00_3114 =
												BGl_setzd2unionzd22z12z12zzcfa_setz00(BgL_dstz00_32,
												BgL_arg1691z00_3115);
										}
										if (CBOOL(BgL__ortest_1156z00_3114))
											{	/* Cfa/set.scm 147 */
												BgL_arg1689z00_3113 = BgL__ortest_1156z00_3114;
											}
										else
											{	/* Cfa/set.scm 147 */
												BgL_arg1689z00_3113 = BgL_resz00_3109;
											}
									}
									{
										obj_t BgL_resz00_4262;
										obj_t BgL_srcza2za2_4261;

										BgL_srcza2za2_4261 = BgL_arg1688z00_3112;
										BgL_resz00_4262 = BgL_arg1689z00_3113;
										BgL_resz00_3109 = BgL_resz00_4262;
										BgL_srcza2za2_3108 = BgL_srcza2za2_4261;
										goto BgL_zc3z04anonymousza31682ze3z87_3110;
									}
								}
						}
				}
		}

	}



/* &set-union! */
	obj_t BGl_z62setzd2unionz12za2zzcfa_setz00(obj_t BgL_envz00_4001,
		obj_t BgL_dstz00_4002, obj_t BgL_srcza2za2_4003)
	{
		{	/* Cfa/set.scm 135 */
			return
				BGl_setzd2unionz12zc0zzcfa_setz00(BgL_dstz00_4002, BgL_srcza2za2_4003);
		}

	}



/* set-for-each */
	BGL_EXPORTED_DEF obj_t BGl_setzd2forzd2eachz00zzcfa_setz00(obj_t
		BgL_procz00_34, obj_t BgL_setz00_35)
	{
		{	/* Cfa/set.scm 152 */
			{	/* Cfa/set.scm 153 */
				obj_t BgL_metaz00_3118;

				BgL_metaz00_3118 = STRUCT_REF(((obj_t) BgL_setz00_35), (int) (1L));
				{	/* Cfa/set.scm 153 */
					obj_t BgL_tablez00_3119;

					BgL_tablez00_3119 =
						STRUCT_REF(((obj_t) BgL_metaz00_3118), (int) (0L));
					{	/* Cfa/set.scm 154 */

						{	/* Cfa/set.scm 155 */
							long BgL_g1159z00_3120;

							BgL_g1159z00_3120 =
								(VECTOR_LENGTH(((obj_t) BgL_tablez00_3119)) - 1L);
							{
								long BgL_iz00_3122;

								BgL_iz00_3122 = BgL_g1159z00_3120;
							BgL_zc3z04anonymousza31693ze3z87_3123:
								if ((BgL_iz00_3122 == -1L))
									{	/* Cfa/set.scm 157 */
										return BUNSPEC;
									}
								else
									{	/* Cfa/set.scm 159 */
										bool_t BgL_test2053z00_4275;

										{	/* Cfa/set.scm 159 */
											obj_t BgL_arg1705z00_3130;

											BgL_arg1705z00_3130 =
												VECTOR_REF(((obj_t) BgL_tablez00_3119), BgL_iz00_3122);
											{	/* Cfa/set.scm 93 */
												obj_t BgL_keyz00_3836;

												BgL_keyz00_3836 =
													(((BgL_nodezf2effectzf2_bglt) COBJECT(
															((BgL_nodezf2effectzf2_bglt)
																BgL_arg1705z00_3130)))->BgL_keyz00);
												{	/* Cfa/set.scm 96 */

													{	/* Cfa/set.scm 97 */
														long BgL_tmpz00_4280;

														{	/* Cfa/set.scm 97 */
															long BgL_tmpz00_4281;

															{	/* Cfa/set.scm 97 */
																unsigned char BgL_tmpz00_4285;

																{	/* Cfa/set.scm 97 */
																	obj_t BgL_stringz00_3848;

																	BgL_stringz00_3848 =
																		STRUCT_REF(
																		((obj_t) BgL_setz00_35), (int) (0L));
																	BgL_tmpz00_4285 =
																		STRING_REF(BgL_stringz00_3848,
																		(long) CINT(CAR(
																				((obj_t) BgL_keyz00_3836))));
																}
																BgL_tmpz00_4281 = (BgL_tmpz00_4285);
															}
															BgL_tmpz00_4280 =
																(
																(long) CINT(CDR(
																		((obj_t) BgL_keyz00_3836))) &
																BgL_tmpz00_4281);
														}
														BgL_test2053z00_4275 = (BgL_tmpz00_4280 > 0L);
										}}}}
										if (BgL_test2053z00_4275)
											{	/* Cfa/set.scm 159 */
												{	/* Cfa/set.scm 160 */
													obj_t BgL_arg1701z00_3127;

													BgL_arg1701z00_3127 =
														VECTOR_REF(
														((obj_t) BgL_tablez00_3119), BgL_iz00_3122);
													BGL_PROCEDURE_CALL1(BgL_procz00_34,
														BgL_arg1701z00_3127);
												}
												{
													long BgL_iz00_4302;

													BgL_iz00_4302 = (BgL_iz00_3122 - 1L);
													BgL_iz00_3122 = BgL_iz00_4302;
													goto BgL_zc3z04anonymousza31693ze3z87_3123;
												}
											}
										else
											{
												long BgL_iz00_4304;

												BgL_iz00_4304 = (BgL_iz00_3122 - 1L);
												BgL_iz00_3122 = BgL_iz00_4304;
												goto BgL_zc3z04anonymousza31693ze3z87_3123;
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* &set-for-each */
	obj_t BGl_z62setzd2forzd2eachz62zzcfa_setz00(obj_t BgL_envz00_4004,
		obj_t BgL_procz00_4005, obj_t BgL_setz00_4006)
	{
		{	/* Cfa/set.scm 152 */
			return
				BGl_setzd2forzd2eachz00zzcfa_setz00(BgL_procz00_4005, BgL_setz00_4006);
		}

	}



/* set-any */
	BGL_EXPORTED_DEF obj_t BGl_setzd2anyzd2zzcfa_setz00(obj_t BgL_procz00_36,
		obj_t BgL_setz00_37)
	{
		{	/* Cfa/set.scm 168 */
			{	/* Cfa/set.scm 169 */
				obj_t BgL_metaz00_3133;

				BgL_metaz00_3133 = STRUCT_REF(((obj_t) BgL_setz00_37), (int) (1L));
				{	/* Cfa/set.scm 169 */
					obj_t BgL_tablez00_3134;

					BgL_tablez00_3134 =
						STRUCT_REF(((obj_t) BgL_metaz00_3133), (int) (0L));
					{	/* Cfa/set.scm 170 */

						{	/* Cfa/set.scm 171 */
							long BgL_g1160z00_3135;

							BgL_g1160z00_3135 =
								(VECTOR_LENGTH(((obj_t) BgL_tablez00_3134)) - 1L);
							{
								long BgL_iz00_3137;

								BgL_iz00_3137 = BgL_g1160z00_3135;
							BgL_zc3z04anonymousza31709ze3z87_3138:
								if ((BgL_iz00_3137 == -1L))
									{	/* Cfa/set.scm 173 */
										return BFALSE;
									}
								else
									{	/* Cfa/set.scm 175 */
										bool_t BgL_test2055z00_4318;

										{	/* Cfa/set.scm 175 */
											obj_t BgL_arg1722z00_3146;

											BgL_arg1722z00_3146 =
												VECTOR_REF(((obj_t) BgL_tablez00_3134), BgL_iz00_3137);
											{	/* Cfa/set.scm 93 */
												obj_t BgL_keyz00_3865;

												BgL_keyz00_3865 =
													(((BgL_nodezf2effectzf2_bglt) COBJECT(
															((BgL_nodezf2effectzf2_bglt)
																BgL_arg1722z00_3146)))->BgL_keyz00);
												{	/* Cfa/set.scm 96 */

													{	/* Cfa/set.scm 97 */
														long BgL_tmpz00_4323;

														{	/* Cfa/set.scm 97 */
															long BgL_tmpz00_4324;

															{	/* Cfa/set.scm 97 */
																unsigned char BgL_tmpz00_4328;

																{	/* Cfa/set.scm 97 */
																	obj_t BgL_stringz00_3877;

																	BgL_stringz00_3877 =
																		STRUCT_REF(
																		((obj_t) BgL_setz00_37), (int) (0L));
																	BgL_tmpz00_4328 =
																		STRING_REF(BgL_stringz00_3877,
																		(long) CINT(CAR(
																				((obj_t) BgL_keyz00_3865))));
																}
																BgL_tmpz00_4324 = (BgL_tmpz00_4328);
															}
															BgL_tmpz00_4323 =
																(
																(long) CINT(CDR(
																		((obj_t) BgL_keyz00_3865))) &
																BgL_tmpz00_4324);
														}
														BgL_test2055z00_4318 = (BgL_tmpz00_4323 > 0L);
										}}}}
										if (BgL_test2055z00_4318)
											{	/* Cfa/set.scm 176 */
												obj_t BgL__ortest_1161z00_3142;

												{	/* Cfa/set.scm 176 */
													obj_t BgL_arg1718z00_3144;

													BgL_arg1718z00_3144 =
														VECTOR_REF(
														((obj_t) BgL_tablez00_3134), BgL_iz00_3137);
													BgL__ortest_1161z00_3142 =
														BGL_PROCEDURE_CALL1(BgL_procz00_36,
														BgL_arg1718z00_3144);
												}
												if (CBOOL(BgL__ortest_1161z00_3142))
													{	/* Cfa/set.scm 176 */
														return BgL__ortest_1161z00_3142;
													}
												else
													{
														long BgL_iz00_4347;

														BgL_iz00_4347 = (BgL_iz00_3137 - 1L);
														BgL_iz00_3137 = BgL_iz00_4347;
														goto BgL_zc3z04anonymousza31709ze3z87_3138;
													}
											}
										else
											{
												long BgL_iz00_4349;

												BgL_iz00_4349 = (BgL_iz00_3137 - 1L);
												BgL_iz00_3137 = BgL_iz00_4349;
												goto BgL_zc3z04anonymousza31709ze3z87_3138;
											}
									}
							}
						}
					}
				}
			}
		}

	}



/* &set-any */
	obj_t BGl_z62setzd2anyzb0zzcfa_setz00(obj_t BgL_envz00_4007,
		obj_t BgL_procz00_4008, obj_t BgL_setz00_4009)
	{
		{	/* Cfa/set.scm 168 */
			return BGl_setzd2anyzd2zzcfa_setz00(BgL_procz00_4008, BgL_setz00_4009);
		}

	}



/* set-every */
	BGL_EXPORTED_DEF obj_t BGl_setzd2everyzd2zzcfa_setz00(obj_t BgL_procz00_38,
		obj_t BgL_setz00_39)
	{
		{	/* Cfa/set.scm 184 */
			{	/* Cfa/set.scm 185 */
				obj_t BgL_metaz00_3149;

				BgL_metaz00_3149 = STRUCT_REF(((obj_t) BgL_setz00_39), (int) (1L));
				{	/* Cfa/set.scm 185 */
					obj_t BgL_tablez00_3150;

					BgL_tablez00_3150 =
						STRUCT_REF(((obj_t) BgL_metaz00_3149), (int) (0L));
					{	/* Cfa/set.scm 186 */

						{	/* Cfa/set.scm 187 */
							long BgL_g1162z00_3151;

							BgL_g1162z00_3151 =
								(VECTOR_LENGTH(((obj_t) BgL_tablez00_3150)) - 1L);
							{
								long BgL_iz00_3153;

								{	/* Cfa/set.scm 187 */
									bool_t BgL_tmpz00_4361;

									BgL_iz00_3153 = BgL_g1162z00_3151;
								BgL_zc3z04anonymousza31725ze3z87_3154:
									if ((BgL_iz00_3153 == -1L))
										{	/* Cfa/set.scm 189 */
											BgL_tmpz00_4361 = ((bool_t) 1);
										}
									else
										{	/* Cfa/set.scm 191 */
											bool_t BgL_test2058z00_4364;

											{	/* Cfa/set.scm 191 */
												obj_t BgL_arg1740z00_3163;

												BgL_arg1740z00_3163 =
													VECTOR_REF(
													((obj_t) BgL_tablez00_3150), BgL_iz00_3153);
												{	/* Cfa/set.scm 93 */
													obj_t BgL_keyz00_3894;

													BgL_keyz00_3894 =
														(((BgL_nodezf2effectzf2_bglt) COBJECT(
																((BgL_nodezf2effectzf2_bglt)
																	BgL_arg1740z00_3163)))->BgL_keyz00);
													{	/* Cfa/set.scm 96 */

														{	/* Cfa/set.scm 97 */
															long BgL_tmpz00_4369;

															{	/* Cfa/set.scm 97 */
																long BgL_tmpz00_4370;

																{	/* Cfa/set.scm 97 */
																	unsigned char BgL_tmpz00_4374;

																	{	/* Cfa/set.scm 97 */
																		obj_t BgL_stringz00_3906;

																		BgL_stringz00_3906 =
																			STRUCT_REF(
																			((obj_t) BgL_setz00_39), (int) (0L));
																		BgL_tmpz00_4374 =
																			STRING_REF(BgL_stringz00_3906,
																			(long) CINT(CAR(
																					((obj_t) BgL_keyz00_3894))));
																	}
																	BgL_tmpz00_4370 = (BgL_tmpz00_4374);
																}
																BgL_tmpz00_4369 =
																	(
																	(long) CINT(CDR(
																			((obj_t) BgL_keyz00_3894))) &
																	BgL_tmpz00_4370);
															}
															BgL_test2058z00_4364 = (BgL_tmpz00_4369 > 0L);
											}}}}
											if (BgL_test2058z00_4364)
												{	/* Cfa/set.scm 192 */
													bool_t BgL_test2059z00_4385;

													{	/* Cfa/set.scm 192 */
														obj_t BgL_arg1738z00_3161;

														BgL_arg1738z00_3161 =
															VECTOR_REF(
															((obj_t) BgL_tablez00_3150), BgL_iz00_3153);
														BgL_test2059z00_4385 =
															CBOOL(BGL_PROCEDURE_CALL1(BgL_procz00_38,
																BgL_arg1738z00_3161));
													}
													if (BgL_test2059z00_4385)
														{
															long BgL_iz00_4393;

															BgL_iz00_4393 = (BgL_iz00_3153 - 1L);
															BgL_iz00_3153 = BgL_iz00_4393;
															goto BgL_zc3z04anonymousza31725ze3z87_3154;
														}
													else
														{	/* Cfa/set.scm 192 */
															BgL_tmpz00_4361 = ((bool_t) 0);
														}
												}
											else
												{
													long BgL_iz00_4395;

													BgL_iz00_4395 = (BgL_iz00_3153 - 1L);
													BgL_iz00_3153 = BgL_iz00_4395;
													goto BgL_zc3z04anonymousza31725ze3z87_3154;
												}
										}
									return BBOOL(BgL_tmpz00_4361);
								}
							}
						}
					}
				}
			}
		}

	}



/* &set-every */
	obj_t BGl_z62setzd2everyzb0zzcfa_setz00(obj_t BgL_envz00_4010,
		obj_t BgL_procz00_4011, obj_t BgL_setz00_4012)
	{
		{	/* Cfa/set.scm 184 */
			return BGl_setzd2everyzd2zzcfa_setz00(BgL_procz00_4011, BgL_setz00_4012);
		}

	}



/* set-length */
	BGL_EXPORTED_DEF obj_t BGl_setzd2lengthzd2zzcfa_setz00(obj_t BgL_setz00_40)
	{
		{	/* Cfa/set.scm 200 */
			{	/* Cfa/set.scm 201 */
				obj_t BgL_thezd2setzd2_3166;

				BgL_thezd2setzd2_3166 = STRUCT_REF(((obj_t) BgL_setz00_40), (int) (0L));
				{	/* Cfa/set.scm 201 */
					long BgL_thezd2lenzd2_3167;

					BgL_thezd2lenzd2_3167 =
						STRING_LENGTH(((obj_t) BgL_thezd2setzd2_3166));
					{	/* Cfa/set.scm 202 */

						{
							long BgL_offsetz00_3169;
							long BgL_numz00_3170;

							{	/* Cfa/set.scm 203 */
								long BgL_tmpz00_4404;

								BgL_offsetz00_3169 = 0L;
								BgL_numz00_3170 = 0L;
							BgL_zc3z04anonymousza31747ze3z87_3171:
								if ((BgL_offsetz00_3169 == BgL_thezd2lenzd2_3167))
									{	/* Cfa/set.scm 205 */
										BgL_tmpz00_4404 = BgL_numz00_3170;
									}
								else
									{
										long BgL_charz00_3175;
										long BgL_numz00_3176;

										BgL_charz00_3175 =
											(STRING_REF(
												((obj_t) BgL_thezd2setzd2_3166), BgL_offsetz00_3169));
										BgL_numz00_3176 = BgL_numz00_3170;
									BgL_zc3z04anonymousza31749ze3z87_3177:
										if ((BgL_charz00_3175 == 0L))
											{
												long BgL_numz00_4411;
												long BgL_offsetz00_4409;

												BgL_offsetz00_4409 = (1L + BgL_offsetz00_3169);
												BgL_numz00_4411 = BgL_numz00_3176;
												BgL_numz00_3170 = BgL_numz00_4411;
												BgL_offsetz00_3169 = BgL_offsetz00_4409;
												goto BgL_zc3z04anonymousza31747ze3z87_3171;
											}
										else
											{
												long BgL_numz00_4415;
												long BgL_charz00_4412;

												BgL_charz00_4412 = (BgL_charz00_3175 >> (int) (1L));
												BgL_numz00_4415 =
													(BgL_numz00_3176 + (BgL_charz00_3175 & 1L));
												BgL_numz00_3176 = BgL_numz00_4415;
												BgL_charz00_3175 = BgL_charz00_4412;
												goto BgL_zc3z04anonymousza31749ze3z87_3177;
											}
									}
								return BINT(BgL_tmpz00_4404);
							}
						}
					}
				}
			}
		}

	}



/* &set-length */
	obj_t BGl_z62setzd2lengthzb0zzcfa_setz00(obj_t BgL_envz00_4013,
		obj_t BgL_setz00_4014)
	{
		{	/* Cfa/set.scm 200 */
			return BGl_setzd2lengthzd2zzcfa_setz00(BgL_setz00_4014);
		}

	}



/* set->list */
	BGL_EXPORTED_DEF obj_t BGl_setzd2ze3listz31zzcfa_setz00(obj_t BgL_setz00_41)
	{
		{	/* Cfa/set.scm 219 */
			{	/* Cfa/set.scm 220 */
				obj_t BgL_metaz00_3186;

				BgL_metaz00_3186 = STRUCT_REF(((obj_t) BgL_setz00_41), (int) (1L));
				{	/* Cfa/set.scm 221 */
					obj_t BgL_tablez00_3187;

					BgL_tablez00_3187 =
						STRUCT_REF(((obj_t) BgL_metaz00_3186), (int) (0L));
					{	/* Cfa/set.scm 222 */

						{
							long BgL_iz00_3191;
							obj_t BgL_lz00_3192;

							BgL_iz00_3191 = 0L;
							BgL_lz00_3192 = BNIL;
						BgL_zc3z04anonymousza31756ze3z87_3193:
							if ((BgL_iz00_3191 == VECTOR_LENGTH(((obj_t) BgL_tablez00_3187))))
								{	/* Cfa/set.scm 226 */
									return BgL_lz00_3192;
								}
							else
								{	/* Cfa/set.scm 228 */
									bool_t BgL_test2063z00_4433;

									{	/* Cfa/set.scm 228 */
										obj_t BgL_arg1773z00_3201;

										BgL_arg1773z00_3201 =
											VECTOR_REF(((obj_t) BgL_tablez00_3187), BgL_iz00_3191);
										{	/* Cfa/set.scm 93 */
											obj_t BgL_keyz00_3936;

											BgL_keyz00_3936 =
												(((BgL_nodezf2effectzf2_bglt) COBJECT(
														((BgL_nodezf2effectzf2_bglt)
															BgL_arg1773z00_3201)))->BgL_keyz00);
											{	/* Cfa/set.scm 96 */

												{	/* Cfa/set.scm 97 */
													long BgL_tmpz00_4438;

													{	/* Cfa/set.scm 97 */
														long BgL_tmpz00_4439;

														{	/* Cfa/set.scm 97 */
															unsigned char BgL_tmpz00_4443;

															{	/* Cfa/set.scm 97 */
																obj_t BgL_stringz00_3948;

																BgL_stringz00_3948 =
																	STRUCT_REF(
																	((obj_t) BgL_setz00_41), (int) (0L));
																BgL_tmpz00_4443 =
																	STRING_REF(BgL_stringz00_3948,
																	(long) CINT(CAR(((obj_t) BgL_keyz00_3936))));
															}
															BgL_tmpz00_4439 = (BgL_tmpz00_4443);
														}
														BgL_tmpz00_4438 =
															(
															(long) CINT(CDR(
																	((obj_t) BgL_keyz00_3936))) &
															BgL_tmpz00_4439);
													}
													BgL_test2063z00_4433 = (BgL_tmpz00_4438 > 0L);
									}}}}
									if (BgL_test2063z00_4433)
										{	/* Cfa/set.scm 229 */
											long BgL_arg1765z00_3197;
											obj_t BgL_arg1767z00_3198;

											BgL_arg1765z00_3197 = (BgL_iz00_3191 + 1L);
											{	/* Cfa/set.scm 229 */
												obj_t BgL_arg1770z00_3199;

												BgL_arg1770z00_3199 =
													VECTOR_REF(
													((obj_t) BgL_tablez00_3187), BgL_iz00_3191);
												BgL_arg1767z00_3198 =
													MAKE_YOUNG_PAIR(BgL_arg1770z00_3199, BgL_lz00_3192);
											}
											{
												obj_t BgL_lz00_4459;
												long BgL_iz00_4458;

												BgL_iz00_4458 = BgL_arg1765z00_3197;
												BgL_lz00_4459 = BgL_arg1767z00_3198;
												BgL_lz00_3192 = BgL_lz00_4459;
												BgL_iz00_3191 = BgL_iz00_4458;
												goto BgL_zc3z04anonymousza31756ze3z87_3193;
											}
										}
									else
										{
											long BgL_iz00_4460;

											BgL_iz00_4460 = (BgL_iz00_3191 + 1L);
											BgL_iz00_3191 = BgL_iz00_4460;
											goto BgL_zc3z04anonymousza31756ze3z87_3193;
										}
								}
						}
					}
				}
			}
		}

	}



/* &set->list */
	obj_t BGl_z62setzd2ze3listz53zzcfa_setz00(obj_t BgL_envz00_4015,
		obj_t BgL_setz00_4016)
	{
		{	/* Cfa/set.scm 219 */
			return BGl_setzd2ze3listz31zzcfa_setz00(BgL_setz00_4016);
		}

	}



/* set->vector */
	BGL_EXPORTED_DEF obj_t BGl_setzd2ze3vectorz31zzcfa_setz00(obj_t BgL_setz00_42)
	{
		{	/* Cfa/set.scm 236 */
			return
				BGl_listzd2ze3vectorz31zz__r4_vectors_6_8z00
				(BGl_setzd2ze3listz31zzcfa_setz00(BgL_setz00_42));
		}

	}



/* &set->vector */
	obj_t BGl_z62setzd2ze3vectorz53zzcfa_setz00(obj_t BgL_envz00_4017,
		obj_t BgL_setz00_4018)
	{
		{	/* Cfa/set.scm 236 */
			return BGl_setzd2ze3vectorz31zzcfa_setz00(BgL_setz00_4018);
		}

	}



/* _set-head */
	obj_t BGl__setzd2headzd2zzcfa_setz00(obj_t BgL_env1495z00_46,
		obj_t BgL_opt1494z00_45)
	{
		{	/* Cfa/set.scm 242 */
			{	/* Cfa/set.scm 242 */
				obj_t BgL_zc3setze3z20_3204;

				BgL_zc3setze3z20_3204 = VECTOR_REF(BgL_opt1494z00_45, 0L);
				switch (VECTOR_LENGTH(BgL_opt1494z00_45))
					{
					case 1L:

						{	/* Cfa/set.scm 242 */

							return
								BGl_setzd2headzd2zzcfa_setz00(BgL_zc3setze3z20_3204, BFALSE);
						}
						break;
					case 2L:

						{	/* Cfa/set.scm 242 */
							obj_t BgL_emptyvalz00_3208;

							BgL_emptyvalz00_3208 = VECTOR_REF(BgL_opt1494z00_45, 1L);
							{	/* Cfa/set.scm 242 */

								return
									BGl_setzd2headzd2zzcfa_setz00(BgL_zc3setze3z20_3204,
									BgL_emptyvalz00_3208);
							}
						}
						break;
					default:
						return BUNSPEC;
					}
			}
		}

	}



/* set-head */
	BGL_EXPORTED_DEF obj_t BGl_setzd2headzd2zzcfa_setz00(obj_t BgL_setz00_43,
		obj_t BgL_emptyvalz00_44)
	{
		{	/* Cfa/set.scm 242 */
			{	/* Cfa/set.scm 243 */
				obj_t BgL_metaz00_3209;

				BgL_metaz00_3209 = STRUCT_REF(((obj_t) BgL_setz00_43), (int) (1L));
				{	/* Cfa/set.scm 244 */
					obj_t BgL_tablez00_3210;

					BgL_tablez00_3210 =
						STRUCT_REF(((obj_t) BgL_metaz00_3209), (int) (0L));
					{	/* Cfa/set.scm 245 */

						{
							long BgL_iz00_3213;

							BgL_iz00_3213 = 0L;
						BgL_zc3z04anonymousza31776ze3z87_3214:
							if ((BgL_iz00_3213 == VECTOR_LENGTH(((obj_t) BgL_tablez00_3210))))
								{	/* Cfa/set.scm 248 */
									return BgL_emptyvalz00_44;
								}
							else
								{	/* Cfa/set.scm 250 */
									bool_t BgL_test2065z00_4482;

									{	/* Cfa/set.scm 250 */
										obj_t BgL_arg1806z00_3219;

										BgL_arg1806z00_3219 =
											VECTOR_REF(((obj_t) BgL_tablez00_3210), BgL_iz00_3213);
										{	/* Cfa/set.scm 93 */
											obj_t BgL_keyz00_3966;

											BgL_keyz00_3966 =
												(((BgL_nodezf2effectzf2_bglt) COBJECT(
														((BgL_nodezf2effectzf2_bglt)
															BgL_arg1806z00_3219)))->BgL_keyz00);
											{	/* Cfa/set.scm 96 */

												{	/* Cfa/set.scm 97 */
													long BgL_tmpz00_4487;

													{	/* Cfa/set.scm 97 */
														long BgL_tmpz00_4488;

														{	/* Cfa/set.scm 97 */
															unsigned char BgL_tmpz00_4492;

															{	/* Cfa/set.scm 97 */
																obj_t BgL_stringz00_3978;

																BgL_stringz00_3978 =
																	STRUCT_REF(
																	((obj_t) BgL_setz00_43), (int) (0L));
																BgL_tmpz00_4492 =
																	STRING_REF(BgL_stringz00_3978,
																	(long) CINT(CAR(((obj_t) BgL_keyz00_3966))));
															}
															BgL_tmpz00_4488 = (BgL_tmpz00_4492);
														}
														BgL_tmpz00_4487 =
															(
															(long) CINT(CDR(
																	((obj_t) BgL_keyz00_3966))) &
															BgL_tmpz00_4488);
													}
													BgL_test2065z00_4482 = (BgL_tmpz00_4487 > 0L);
									}}}}
									if (BgL_test2065z00_4482)
										{	/* Cfa/set.scm 250 */
											return
												VECTOR_REF(((obj_t) BgL_tablez00_3210), BgL_iz00_3213);
										}
									else
										{
											long BgL_iz00_4505;

											BgL_iz00_4505 = (BgL_iz00_3213 + 1L);
											BgL_iz00_3213 = BgL_iz00_4505;
											goto BgL_zc3z04anonymousza31776ze3z87_3214;
										}
								}
						}
					}
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_setz00(void)
	{
		{	/* Cfa/set.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_setz00(void)
	{
		{	/* Cfa/set.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_setz00(void)
	{
		{	/* Cfa/set.scm 15 */
			return BUNSPEC;
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_setz00(void)
	{
		{	/* Cfa/set.scm 15 */
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2012z00zzcfa_setz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2012z00zzcfa_setz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2012z00zzcfa_setz00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string2012z00zzcfa_setz00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string2012z00zzcfa_setz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2012z00zzcfa_setz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2012z00zzcfa_setz00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string2012z00zzcfa_setz00));
		}

	}

#ifdef __cplusplus
}
#endif
