/*===========================================================================*/
/*   (Cfa/show.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/show.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_SHOW_TYPE_DEFINITIONS
#define BGL_CFA_SHOW_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_scnstz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_nodez00;
		obj_t BgL_classz00;
		obj_t BgL_locz00;
	}               *BgL_scnstz00_bglt;

	typedef struct BgL_cvarz00_bgl
	{
		header_t header;
		obj_t widening;
		bool_t BgL_macrozf3zf3;
	}              *BgL_cvarz00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_cfunzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_cfunzf2cinfozf2_bglt;

	typedef struct BgL_externzd2sfunzf2cinfoz20_bgl
	{
		bool_t BgL_polymorphiczf3zf3;
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                               *BgL_externzd2sfunzf2cinfoz20_bglt;

	typedef struct BgL_internzd2sfunzf2cinfoz20_bgl
	{
		bool_t BgL_polymorphiczf3zf3;
		struct BgL_approxz00_bgl *BgL_approxz00;
		long BgL_stampz00;
	}                               *BgL_internzd2sfunzf2cinfoz20_bglt;

	typedef struct BgL_scnstzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                       *BgL_scnstzf2cinfozf2_bglt;

	typedef struct BgL_svarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_clozd2envzf3z21;
		long BgL_stampz00;
	}                      *BgL_svarzf2cinfozf2_bglt;

	typedef struct BgL_cvarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_cvarzf2cinfozf2_bglt;

	typedef struct BgL_reshapedzd2localzd2_bgl
	{
		obj_t BgL_bindingzd2valuezd2;
	}                          *BgL_reshapedzd2localzd2_bglt;

	typedef struct BgL_reshapedzd2globalzd2_bgl
	{
	}                           *BgL_reshapedzd2globalzd2_bglt;


#endif													// BGL_CFA_SHOW_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_IMPORT obj_t bgl_display_obj(obj_t, obj_t);
	static obj_t BGl_objectzd2initzd2zzcfa_showz00(void);
	extern obj_t BGl_reshapedzd2localzd2zzcfa_infoz00;
	extern obj_t BGl_cvarzf2Cinfozf2zzcfa_infoz00;
	static obj_t BGl_methodzd2initzd2zzcfa_showz00(void);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_showz00(void);
	extern obj_t BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
	static obj_t BGl_z62showzd2cfazd2resultsz62zzcfa_showz00(obj_t, obj_t);
	extern obj_t BGl_svarzf2Cinfozf2zzcfa_infoz00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	extern obj_t BGl_za2cfazd2stampza2zd2zzcfa_iteratez00;
	BGL_EXPORTED_DECL obj_t
		BGl_showzd2cfazd2nbzd2iterationszd2zzcfa_showz00(void);
	extern obj_t BGl_verbosez00zztools_speekz00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_showz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_toplevelzd2initzd2zzcfa_showz00(void);
	static obj_t BGl_genericzd2initzd2zzcfa_showz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	static obj_t BGl_z62showzd2cfazd2nbzd2iterationszb0zzcfa_showz00(obj_t);
	BGL_IMPORT obj_t bstring_to_symbol(obj_t);
	extern obj_t BGl_reshapedzd2globalzd2zzcfa_infoz00;
	extern obj_t BGl_externzd2sfunzf2Cinfoz20zzcfa_infoz00;
	BGL_IMPORT obj_t BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(obj_t);
	BGL_IMPORT obj_t bgl_close_output_port(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_showzd2cfazd2resultsz00zzcfa_showz00(obj_t);
	static obj_t BGl_z62shapezd2reshapedzd2local1492z62zzcfa_showz00(obj_t,
		obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_showz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_iteratez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_collectz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzwrite_schemez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_speekz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	extern obj_t BGl_getzd2allocszd2zzcfa_collectz00(void);
	static obj_t BGl_z62shapezd2reshapedzd2globa1494z62zzcfa_showz00(obj_t,
		obj_t);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_showz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_showz00(void);
	extern obj_t BGl_scnstzf2Cinfozf2zzcfa_infoz00;
	static obj_t BGl_cfazd2variablezd2shapez00zzcfa_showz00(BgL_variablez00_bglt,
		obj_t);
	extern obj_t BGl_cfunzf2Cinfozf2zzcfa_infoz00;
	static obj_t *__cnst;


	extern obj_t BGl_shapezd2envzd2zztools_shapez00;
	   
		 
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_showzd2cfazd2nbzd2iterationszd2envz00zzcfa_showz00,
		BgL_bgl_za762showza7d2cfaza7d21968za7,
		BGl_z62showzd2cfazd2nbzd2iterationszb0zzcfa_showz00, 0L, BUNSPEC, 0);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1964z00zzcfa_showz00,
		BgL_bgl_za762shapeza7d2resha1969z00,
		BGl_z62shapezd2reshapedzd2local1492z62zzcfa_showz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1966z00zzcfa_showz00,
		BgL_bgl_za762shapeza7d2resha1970z00,
		BGl_z62shapezd2reshapedzd2globa1494z62zzcfa_showz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_showzd2cfazd2resultszd2envzd2zzcfa_showz00,
		BgL_bgl_za762showza7d2cfaza7d21971za7,
		BGl_z62showzd2cfazd2resultsz62zzcfa_showz00, 0L, BUNSPEC, 1);
	      DEFINE_STRING(BGl_string1961z00zzcfa_showz00,
		BgL_bgl_string1961za700za7za7c1972za7, " Iterations)", 12);
	      DEFINE_STRING(BGl_string1962z00zzcfa_showz00,
		BgL_bgl_string1962za700za7za7c1973za7, "      (", 7);
	      DEFINE_STRING(BGl_string1963z00zzcfa_showz00,
		BgL_bgl_string1963za700za7za7c1974za7, ",", 1);
	      DEFINE_STRING(BGl_string1965z00zzcfa_showz00,
		BgL_bgl_string1965za700za7za7c1975za7, "shape", 5);
	      DEFINE_STRING(BGl_string1967z00zzcfa_showz00,
		BgL_bgl_string1967za700za7za7c1976za7, "cfa_show", 8);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_showz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_showz00(long
		BgL_checksumz00_3960, char *BgL_fromz00_3961)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_showz00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_showz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_showz00();
					BGl_libraryzd2moduleszd2initz00zzcfa_showz00();
					BGl_importedzd2moduleszd2initz00zzcfa_showz00();
					BGl_methodzd2initzd2zzcfa_showz00();
					return BGl_toplevelzd2initzd2zzcfa_showz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_showz00(void)
	{
		{	/* Cfa/show.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_output_6_10_3z00(0L, "cfa_show");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_show");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_show");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "cfa_show");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "cfa_show");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_show");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_show");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_show");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_show");
			return BUNSPEC;
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_showz00(void)
	{
		{	/* Cfa/show.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_showz00(void)
	{
		{	/* Cfa/show.scm 15 */
			return BUNSPEC;
		}

	}



/* show-cfa-nb-iterations */
	BGL_EXPORTED_DEF obj_t BGl_showzd2cfazd2nbzd2iterationszd2zzcfa_showz00(void)
	{
		{	/* Cfa/show.scm 35 */
			{	/* Cfa/show.scm 36 */
				long BgL_arg1513z00_2973;

				BgL_arg1513z00_2973 =
					(1L + (long) CINT(BGl_za2cfazd2stampza2zd2zzcfa_iteratez00));
				{	/* Cfa/show.scm 36 */
					obj_t BgL_list1514z00_2974;

					{	/* Cfa/show.scm 36 */
						obj_t BgL_arg1516z00_2975;

						{	/* Cfa/show.scm 36 */
							obj_t BgL_arg1535z00_2976;

							{	/* Cfa/show.scm 36 */
								obj_t BgL_arg1540z00_2977;

								BgL_arg1540z00_2977 =
									MAKE_YOUNG_PAIR(BCHAR(((unsigned char) 10)), BNIL);
								BgL_arg1535z00_2976 =
									MAKE_YOUNG_PAIR(BGl_string1961z00zzcfa_showz00,
									BgL_arg1540z00_2977);
							}
							BgL_arg1516z00_2975 =
								MAKE_YOUNG_PAIR(BINT(BgL_arg1513z00_2973), BgL_arg1535z00_2976);
						}
						BgL_list1514z00_2974 =
							MAKE_YOUNG_PAIR(BGl_string1962z00zzcfa_showz00,
							BgL_arg1516z00_2975);
					}
					return BGl_verbosez00zztools_speekz00(BINT(2L), BgL_list1514z00_2974);
				}
			}
		}

	}



/* &show-cfa-nb-iterations */
	obj_t BGl_z62showzd2cfazd2nbzd2iterationszb0zzcfa_showz00(obj_t
		BgL_envz00_3941)
	{
		{	/* Cfa/show.scm 35 */
			return BGl_showzd2cfazd2nbzd2iterationszd2zzcfa_showz00();
		}

	}



/* show-cfa-results */
	BGL_EXPORTED_DEF obj_t BGl_showzd2cfazd2resultsz00zzcfa_showz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Cfa/show.scm 41 */
			{	/* Cfa/show.scm 46 */
				bool_t BgL_test1978z00_3991;

				{	/* Cfa/show.scm 46 */
					obj_t BgL_tmpz00_3992;

					BgL_tmpz00_3992 = BGl_getzd2allocszd2zzcfa_collectz00();
					BgL_test1978z00_3991 = PAIRP(BgL_tmpz00_3992);
				}
				if (BgL_test1978z00_3991)
					{	/* Cfa/show.scm 46 */
						BNIL;
					}
				else
					{	/* Cfa/show.scm 46 */
						BFALSE;
					}
			}
			{	/* Cfa/show.scm 47 */
				obj_t BgL_g1490z00_2981;

				BgL_g1490z00_2981 = BGl_getzd2allocszd2zzcfa_collectz00();
				{
					obj_t BgL_l1488z00_2983;

					BgL_l1488z00_2983 = BgL_g1490z00_2981;
				BgL_zc3z04anonymousza31553ze3z87_2984:
					if (PAIRP(BgL_l1488z00_2983))
						{	/* Cfa/show.scm 49 */
							{
								obj_t BgL_l1488z00_3998;

								BgL_l1488z00_3998 = CDR(BgL_l1488z00_2983);
								BgL_l1488z00_2983 = BgL_l1488z00_3998;
								goto BgL_zc3z04anonymousza31553ze3z87_2984;
							}
						}
					else
						{	/* Cfa/show.scm 49 */
							((bool_t) 1);
						}
				}
			}
			return BNIL;
		}

	}



/* &show-cfa-results */
	obj_t BGl_z62showzd2cfazd2resultsz62zzcfa_showz00(obj_t BgL_envz00_3942,
		obj_t BgL_globalsz00_3943)
	{
		{	/* Cfa/show.scm 41 */
			return BGl_showzd2cfazd2resultsz00zzcfa_showz00(BgL_globalsz00_3943);
		}

	}



/* cfa-variable-shape */
	obj_t BGl_cfazd2variablezd2shapez00zzcfa_showz00(BgL_variablez00_bglt
		BgL_variablez00_6, obj_t BgL_portz00_7)
	{
		{	/* Cfa/show.scm 93 */
			{	/* Cfa/show.scm 94 */
				BgL_valuez00_bglt BgL_valuez00_2989;

				BgL_valuez00_2989 =
					(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_6))->BgL_valuez00);
				{	/* Cfa/show.scm 96 */
					bool_t BgL_test1980z00_4002;

					{	/* Cfa/show.scm 96 */
						obj_t BgL_classz00_3501;

						BgL_classz00_3501 = BGl_svarzf2Cinfozf2zzcfa_infoz00;
						{	/* Cfa/show.scm 96 */
							BgL_objectz00_bglt BgL_arg1807z00_3503;

							{	/* Cfa/show.scm 96 */
								obj_t BgL_tmpz00_4003;

								BgL_tmpz00_4003 =
									((obj_t) ((BgL_objectz00_bglt) BgL_valuez00_2989));
								BgL_arg1807z00_3503 = (BgL_objectz00_bglt) (BgL_tmpz00_4003);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/show.scm 96 */
									long BgL_idxz00_3509;

									BgL_idxz00_3509 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3503);
									BgL_test1980z00_4002 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3509 + 3L)) == BgL_classz00_3501);
								}
							else
								{	/* Cfa/show.scm 96 */
									bool_t BgL_res1930z00_3534;

									{	/* Cfa/show.scm 96 */
										obj_t BgL_oclassz00_3517;

										{	/* Cfa/show.scm 96 */
											obj_t BgL_arg1815z00_3525;
											long BgL_arg1816z00_3526;

											BgL_arg1815z00_3525 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/show.scm 96 */
												long BgL_arg1817z00_3527;

												BgL_arg1817z00_3527 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3503);
												BgL_arg1816z00_3526 =
													(BgL_arg1817z00_3527 - OBJECT_TYPE);
											}
											BgL_oclassz00_3517 =
												VECTOR_REF(BgL_arg1815z00_3525, BgL_arg1816z00_3526);
										}
										{	/* Cfa/show.scm 96 */
											bool_t BgL__ortest_1115z00_3518;

											BgL__ortest_1115z00_3518 =
												(BgL_classz00_3501 == BgL_oclassz00_3517);
											if (BgL__ortest_1115z00_3518)
												{	/* Cfa/show.scm 96 */
													BgL_res1930z00_3534 = BgL__ortest_1115z00_3518;
												}
											else
												{	/* Cfa/show.scm 96 */
													long BgL_odepthz00_3519;

													{	/* Cfa/show.scm 96 */
														obj_t BgL_arg1804z00_3520;

														BgL_arg1804z00_3520 = (BgL_oclassz00_3517);
														BgL_odepthz00_3519 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3520);
													}
													if ((3L < BgL_odepthz00_3519))
														{	/* Cfa/show.scm 96 */
															obj_t BgL_arg1802z00_3522;

															{	/* Cfa/show.scm 96 */
																obj_t BgL_arg1803z00_3523;

																BgL_arg1803z00_3523 = (BgL_oclassz00_3517);
																BgL_arg1802z00_3522 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3523,
																	3L);
															}
															BgL_res1930z00_3534 =
																(BgL_arg1802z00_3522 == BgL_classz00_3501);
														}
													else
														{	/* Cfa/show.scm 96 */
															BgL_res1930z00_3534 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test1980z00_4002 = BgL_res1930z00_3534;
								}
						}
					}
					if (BgL_test1980z00_4002)
						{	/* Cfa/show.scm 96 */
							bgl_display_string(BGl_string1963z00zzcfa_showz00, BgL_portz00_7);
							{	/* Cfa/show.scm 98 */
								obj_t BgL_arg1564z00_2991;

								{	/* Cfa/show.scm 98 */
									BgL_approxz00_bglt BgL_arg1565z00_2992;

									{
										BgL_svarzf2cinfozf2_bglt BgL_auxz00_4027;

										{
											obj_t BgL_auxz00_4028;

											{	/* Cfa/show.scm 98 */
												BgL_objectz00_bglt BgL_tmpz00_4029;

												BgL_tmpz00_4029 =
													((BgL_objectz00_bglt)
													((BgL_svarz00_bglt) BgL_valuez00_2989));
												BgL_auxz00_4028 = BGL_OBJECT_WIDENING(BgL_tmpz00_4029);
											}
											BgL_auxz00_4027 =
												((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_4028);
										}
										BgL_arg1565z00_2992 =
											(((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_4027))->
											BgL_approxz00);
									}
									BgL_arg1564z00_2991 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_arg1565z00_2992));
								}
								bgl_display_obj(BgL_arg1564z00_2991, BgL_portz00_7);
							}
						}
					else
						{	/* Cfa/show.scm 99 */
							bool_t BgL_test1984z00_4038;

							{	/* Cfa/show.scm 99 */
								obj_t BgL_classz00_3539;

								BgL_classz00_3539 = BGl_cvarzf2Cinfozf2zzcfa_infoz00;
								{	/* Cfa/show.scm 99 */
									BgL_objectz00_bglt BgL_arg1807z00_3541;

									{	/* Cfa/show.scm 99 */
										obj_t BgL_tmpz00_4039;

										BgL_tmpz00_4039 =
											((obj_t) ((BgL_objectz00_bglt) BgL_valuez00_2989));
										BgL_arg1807z00_3541 =
											(BgL_objectz00_bglt) (BgL_tmpz00_4039);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/show.scm 99 */
											long BgL_idxz00_3547;

											BgL_idxz00_3547 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3541);
											BgL_test1984z00_4038 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_3547 + 3L)) == BgL_classz00_3539);
										}
									else
										{	/* Cfa/show.scm 99 */
											bool_t BgL_res1931z00_3572;

											{	/* Cfa/show.scm 99 */
												obj_t BgL_oclassz00_3555;

												{	/* Cfa/show.scm 99 */
													obj_t BgL_arg1815z00_3563;
													long BgL_arg1816z00_3564;

													BgL_arg1815z00_3563 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/show.scm 99 */
														long BgL_arg1817z00_3565;

														BgL_arg1817z00_3565 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3541);
														BgL_arg1816z00_3564 =
															(BgL_arg1817z00_3565 - OBJECT_TYPE);
													}
													BgL_oclassz00_3555 =
														VECTOR_REF(BgL_arg1815z00_3563,
														BgL_arg1816z00_3564);
												}
												{	/* Cfa/show.scm 99 */
													bool_t BgL__ortest_1115z00_3556;

													BgL__ortest_1115z00_3556 =
														(BgL_classz00_3539 == BgL_oclassz00_3555);
													if (BgL__ortest_1115z00_3556)
														{	/* Cfa/show.scm 99 */
															BgL_res1931z00_3572 = BgL__ortest_1115z00_3556;
														}
													else
														{	/* Cfa/show.scm 99 */
															long BgL_odepthz00_3557;

															{	/* Cfa/show.scm 99 */
																obj_t BgL_arg1804z00_3558;

																BgL_arg1804z00_3558 = (BgL_oclassz00_3555);
																BgL_odepthz00_3557 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_3558);
															}
															if ((3L < BgL_odepthz00_3557))
																{	/* Cfa/show.scm 99 */
																	obj_t BgL_arg1802z00_3560;

																	{	/* Cfa/show.scm 99 */
																		obj_t BgL_arg1803z00_3561;

																		BgL_arg1803z00_3561 = (BgL_oclassz00_3555);
																		BgL_arg1802z00_3560 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_3561, 3L);
																	}
																	BgL_res1931z00_3572 =
																		(BgL_arg1802z00_3560 == BgL_classz00_3539);
																}
															else
																{	/* Cfa/show.scm 99 */
																	BgL_res1931z00_3572 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test1984z00_4038 = BgL_res1931z00_3572;
										}
								}
							}
							if (BgL_test1984z00_4038)
								{	/* Cfa/show.scm 99 */
									bgl_display_string(BGl_string1963z00zzcfa_showz00,
										BgL_portz00_7);
									{	/* Cfa/show.scm 101 */
										obj_t BgL_arg1571z00_2994;

										{	/* Cfa/show.scm 101 */
											BgL_approxz00_bglt BgL_arg1573z00_2995;

											{
												BgL_cvarzf2cinfozf2_bglt BgL_auxz00_4063;

												{
													obj_t BgL_auxz00_4064;

													{	/* Cfa/show.scm 101 */
														BgL_objectz00_bglt BgL_tmpz00_4065;

														BgL_tmpz00_4065 =
															((BgL_objectz00_bglt)
															((BgL_cvarz00_bglt) BgL_valuez00_2989));
														BgL_auxz00_4064 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4065);
													}
													BgL_auxz00_4063 =
														((BgL_cvarzf2cinfozf2_bglt) BgL_auxz00_4064);
												}
												BgL_arg1573z00_2995 =
													(((BgL_cvarzf2cinfozf2_bglt)
														COBJECT(BgL_auxz00_4063))->BgL_approxz00);
											}
											BgL_arg1571z00_2994 =
												BGl_shapez00zztools_shapez00(
												((obj_t) BgL_arg1573z00_2995));
										}
										bgl_display_obj(BgL_arg1571z00_2994, BgL_portz00_7);
									}
								}
							else
								{	/* Cfa/show.scm 102 */
									bool_t BgL_test1988z00_4074;

									{	/* Cfa/show.scm 102 */
										obj_t BgL_classz00_3577;

										BgL_classz00_3577 = BGl_scnstzf2Cinfozf2zzcfa_infoz00;
										{	/* Cfa/show.scm 102 */
											BgL_objectz00_bglt BgL_arg1807z00_3579;

											{	/* Cfa/show.scm 102 */
												obj_t BgL_tmpz00_4075;

												BgL_tmpz00_4075 =
													((obj_t) ((BgL_objectz00_bglt) BgL_valuez00_2989));
												BgL_arg1807z00_3579 =
													(BgL_objectz00_bglt) (BgL_tmpz00_4075);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Cfa/show.scm 102 */
													long BgL_idxz00_3585;

													BgL_idxz00_3585 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3579);
													BgL_test1988z00_4074 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_3585 + 3L)) == BgL_classz00_3577);
												}
											else
												{	/* Cfa/show.scm 102 */
													bool_t BgL_res1932z00_3610;

													{	/* Cfa/show.scm 102 */
														obj_t BgL_oclassz00_3593;

														{	/* Cfa/show.scm 102 */
															obj_t BgL_arg1815z00_3601;
															long BgL_arg1816z00_3602;

															BgL_arg1815z00_3601 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Cfa/show.scm 102 */
																long BgL_arg1817z00_3603;

																BgL_arg1817z00_3603 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3579);
																BgL_arg1816z00_3602 =
																	(BgL_arg1817z00_3603 - OBJECT_TYPE);
															}
															BgL_oclassz00_3593 =
																VECTOR_REF(BgL_arg1815z00_3601,
																BgL_arg1816z00_3602);
														}
														{	/* Cfa/show.scm 102 */
															bool_t BgL__ortest_1115z00_3594;

															BgL__ortest_1115z00_3594 =
																(BgL_classz00_3577 == BgL_oclassz00_3593);
															if (BgL__ortest_1115z00_3594)
																{	/* Cfa/show.scm 102 */
																	BgL_res1932z00_3610 =
																		BgL__ortest_1115z00_3594;
																}
															else
																{	/* Cfa/show.scm 102 */
																	long BgL_odepthz00_3595;

																	{	/* Cfa/show.scm 102 */
																		obj_t BgL_arg1804z00_3596;

																		BgL_arg1804z00_3596 = (BgL_oclassz00_3593);
																		BgL_odepthz00_3595 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_3596);
																	}
																	if ((3L < BgL_odepthz00_3595))
																		{	/* Cfa/show.scm 102 */
																			obj_t BgL_arg1802z00_3598;

																			{	/* Cfa/show.scm 102 */
																				obj_t BgL_arg1803z00_3599;

																				BgL_arg1803z00_3599 =
																					(BgL_oclassz00_3593);
																				BgL_arg1802z00_3598 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_3599, 3L);
																			}
																			BgL_res1932z00_3610 =
																				(BgL_arg1802z00_3598 ==
																				BgL_classz00_3577);
																		}
																	else
																		{	/* Cfa/show.scm 102 */
																			BgL_res1932z00_3610 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test1988z00_4074 = BgL_res1932z00_3610;
												}
										}
									}
									if (BgL_test1988z00_4074)
										{	/* Cfa/show.scm 102 */
											bgl_display_string(BGl_string1963z00zzcfa_showz00,
												BgL_portz00_7);
											{	/* Cfa/show.scm 104 */
												obj_t BgL_arg1575z00_2997;

												{	/* Cfa/show.scm 104 */
													BgL_approxz00_bglt BgL_arg1576z00_2998;

													{
														BgL_scnstzf2cinfozf2_bglt BgL_auxz00_4099;

														{
															obj_t BgL_auxz00_4100;

															{	/* Cfa/show.scm 104 */
																BgL_objectz00_bglt BgL_tmpz00_4101;

																BgL_tmpz00_4101 =
																	((BgL_objectz00_bglt)
																	((BgL_scnstz00_bglt) BgL_valuez00_2989));
																BgL_auxz00_4100 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_4101);
															}
															BgL_auxz00_4099 =
																((BgL_scnstzf2cinfozf2_bglt) BgL_auxz00_4100);
														}
														BgL_arg1576z00_2998 =
															(((BgL_scnstzf2cinfozf2_bglt)
																COBJECT(BgL_auxz00_4099))->BgL_approxz00);
													}
													BgL_arg1575z00_2997 =
														BGl_shapez00zztools_shapez00(
														((obj_t) BgL_arg1576z00_2998));
												}
												bgl_display_obj(BgL_arg1575z00_2997, BgL_portz00_7);
											}
										}
									else
										{	/* Cfa/show.scm 105 */
											bool_t BgL_test1992z00_4110;

											{	/* Cfa/show.scm 105 */
												obj_t BgL_classz00_3615;

												BgL_classz00_3615 =
													BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
												{	/* Cfa/show.scm 105 */
													BgL_objectz00_bglt BgL_arg1807z00_3617;

													{	/* Cfa/show.scm 105 */
														obj_t BgL_tmpz00_4111;

														BgL_tmpz00_4111 =
															((obj_t)
															((BgL_objectz00_bglt) BgL_valuez00_2989));
														BgL_arg1807z00_3617 =
															(BgL_objectz00_bglt) (BgL_tmpz00_4111);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Cfa/show.scm 105 */
															long BgL_idxz00_3623;

															BgL_idxz00_3623 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3617);
															BgL_test1992z00_4110 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_3623 + 4L)) == BgL_classz00_3615);
														}
													else
														{	/* Cfa/show.scm 105 */
															bool_t BgL_res1933z00_3648;

															{	/* Cfa/show.scm 105 */
																obj_t BgL_oclassz00_3631;

																{	/* Cfa/show.scm 105 */
																	obj_t BgL_arg1815z00_3639;
																	long BgL_arg1816z00_3640;

																	BgL_arg1815z00_3639 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Cfa/show.scm 105 */
																		long BgL_arg1817z00_3641;

																		BgL_arg1817z00_3641 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3617);
																		BgL_arg1816z00_3640 =
																			(BgL_arg1817z00_3641 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_3631 =
																		VECTOR_REF(BgL_arg1815z00_3639,
																		BgL_arg1816z00_3640);
																}
																{	/* Cfa/show.scm 105 */
																	bool_t BgL__ortest_1115z00_3632;

																	BgL__ortest_1115z00_3632 =
																		(BgL_classz00_3615 == BgL_oclassz00_3631);
																	if (BgL__ortest_1115z00_3632)
																		{	/* Cfa/show.scm 105 */
																			BgL_res1933z00_3648 =
																				BgL__ortest_1115z00_3632;
																		}
																	else
																		{	/* Cfa/show.scm 105 */
																			long BgL_odepthz00_3633;

																			{	/* Cfa/show.scm 105 */
																				obj_t BgL_arg1804z00_3634;

																				BgL_arg1804z00_3634 =
																					(BgL_oclassz00_3631);
																				BgL_odepthz00_3633 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_3634);
																			}
																			if ((4L < BgL_odepthz00_3633))
																				{	/* Cfa/show.scm 105 */
																					obj_t BgL_arg1802z00_3636;

																					{	/* Cfa/show.scm 105 */
																						obj_t BgL_arg1803z00_3637;

																						BgL_arg1803z00_3637 =
																							(BgL_oclassz00_3631);
																						BgL_arg1802z00_3636 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_3637, 4L);
																					}
																					BgL_res1933z00_3648 =
																						(BgL_arg1802z00_3636 ==
																						BgL_classz00_3615);
																				}
																			else
																				{	/* Cfa/show.scm 105 */
																					BgL_res1933z00_3648 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test1992z00_4110 = BgL_res1933z00_3648;
														}
												}
											}
											if (BgL_test1992z00_4110)
												{	/* Cfa/show.scm 105 */
													bgl_display_string(BGl_string1963z00zzcfa_showz00,
														BgL_portz00_7);
													{	/* Cfa/show.scm 107 */
														obj_t BgL_arg1584z00_3000;

														{	/* Cfa/show.scm 107 */
															BgL_approxz00_bglt BgL_arg1585z00_3001;

															{
																BgL_internzd2sfunzf2cinfoz20_bglt
																	BgL_auxz00_4135;
																{
																	obj_t BgL_auxz00_4136;

																	{	/* Cfa/show.scm 107 */
																		BgL_objectz00_bglt BgL_tmpz00_4137;

																		BgL_tmpz00_4137 =
																			((BgL_objectz00_bglt)
																			((BgL_sfunz00_bglt) BgL_valuez00_2989));
																		BgL_auxz00_4136 =
																			BGL_OBJECT_WIDENING(BgL_tmpz00_4137);
																	}
																	BgL_auxz00_4135 =
																		((BgL_internzd2sfunzf2cinfoz20_bglt)
																		BgL_auxz00_4136);
																}
																BgL_arg1585z00_3001 =
																	(((BgL_internzd2sfunzf2cinfoz20_bglt)
																		COBJECT(BgL_auxz00_4135))->BgL_approxz00);
															}
															BgL_arg1584z00_3000 =
																BGl_shapez00zztools_shapez00(
																((obj_t) BgL_arg1585z00_3001));
														}
														bgl_display_obj(BgL_arg1584z00_3000, BgL_portz00_7);
													}
												}
											else
												{	/* Cfa/show.scm 108 */
													bool_t BgL_test1996z00_4146;

													{	/* Cfa/show.scm 108 */
														obj_t BgL_classz00_3653;

														BgL_classz00_3653 =
															BGl_externzd2sfunzf2Cinfoz20zzcfa_infoz00;
														{	/* Cfa/show.scm 108 */
															BgL_objectz00_bglt BgL_arg1807z00_3655;

															{	/* Cfa/show.scm 108 */
																obj_t BgL_tmpz00_4147;

																BgL_tmpz00_4147 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_valuez00_2989));
																BgL_arg1807z00_3655 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_4147);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Cfa/show.scm 108 */
																	long BgL_idxz00_3661;

																	BgL_idxz00_3661 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_3655);
																	BgL_test1996z00_4146 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_3661 + 4L)) ==
																		BgL_classz00_3653);
																}
															else
																{	/* Cfa/show.scm 108 */
																	bool_t BgL_res1934z00_3686;

																	{	/* Cfa/show.scm 108 */
																		obj_t BgL_oclassz00_3669;

																		{	/* Cfa/show.scm 108 */
																			obj_t BgL_arg1815z00_3677;
																			long BgL_arg1816z00_3678;

																			BgL_arg1815z00_3677 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Cfa/show.scm 108 */
																				long BgL_arg1817z00_3679;

																				BgL_arg1817z00_3679 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_3655);
																				BgL_arg1816z00_3678 =
																					(BgL_arg1817z00_3679 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_3669 =
																				VECTOR_REF(BgL_arg1815z00_3677,
																				BgL_arg1816z00_3678);
																		}
																		{	/* Cfa/show.scm 108 */
																			bool_t BgL__ortest_1115z00_3670;

																			BgL__ortest_1115z00_3670 =
																				(BgL_classz00_3653 ==
																				BgL_oclassz00_3669);
																			if (BgL__ortest_1115z00_3670)
																				{	/* Cfa/show.scm 108 */
																					BgL_res1934z00_3686 =
																						BgL__ortest_1115z00_3670;
																				}
																			else
																				{	/* Cfa/show.scm 108 */
																					long BgL_odepthz00_3671;

																					{	/* Cfa/show.scm 108 */
																						obj_t BgL_arg1804z00_3672;

																						BgL_arg1804z00_3672 =
																							(BgL_oclassz00_3669);
																						BgL_odepthz00_3671 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_3672);
																					}
																					if ((4L < BgL_odepthz00_3671))
																						{	/* Cfa/show.scm 108 */
																							obj_t BgL_arg1802z00_3674;

																							{	/* Cfa/show.scm 108 */
																								obj_t BgL_arg1803z00_3675;

																								BgL_arg1803z00_3675 =
																									(BgL_oclassz00_3669);
																								BgL_arg1802z00_3674 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_3675, 4L);
																							}
																							BgL_res1934z00_3686 =
																								(BgL_arg1802z00_3674 ==
																								BgL_classz00_3653);
																						}
																					else
																						{	/* Cfa/show.scm 108 */
																							BgL_res1934z00_3686 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test1996z00_4146 = BgL_res1934z00_3686;
																}
														}
													}
													if (BgL_test1996z00_4146)
														{	/* Cfa/show.scm 108 */
															bgl_display_string(BGl_string1963z00zzcfa_showz00,
																BgL_portz00_7);
															{	/* Cfa/show.scm 110 */
																obj_t BgL_arg1589z00_3003;

																{	/* Cfa/show.scm 110 */
																	BgL_approxz00_bglt BgL_arg1591z00_3004;

																	{
																		BgL_externzd2sfunzf2cinfoz20_bglt
																			BgL_auxz00_4171;
																		{
																			obj_t BgL_auxz00_4172;

																			{	/* Cfa/show.scm 110 */
																				BgL_objectz00_bglt BgL_tmpz00_4173;

																				BgL_tmpz00_4173 =
																					((BgL_objectz00_bglt)
																					((BgL_sfunz00_bglt)
																						BgL_valuez00_2989));
																				BgL_auxz00_4172 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_4173);
																			}
																			BgL_auxz00_4171 =
																				((BgL_externzd2sfunzf2cinfoz20_bglt)
																				BgL_auxz00_4172);
																		}
																		BgL_arg1591z00_3004 =
																			(((BgL_externzd2sfunzf2cinfoz20_bglt)
																				COBJECT(BgL_auxz00_4171))->
																			BgL_approxz00);
																	}
																	BgL_arg1589z00_3003 =
																		BGl_shapez00zztools_shapez00(
																		((obj_t) BgL_arg1591z00_3004));
																}
																bgl_display_obj(BgL_arg1589z00_3003,
																	BgL_portz00_7);
															}
														}
													else
														{	/* Cfa/show.scm 111 */
															bool_t BgL_test2000z00_4182;

															{	/* Cfa/show.scm 111 */
																obj_t BgL_classz00_3691;

																BgL_classz00_3691 =
																	BGl_cfunzf2Cinfozf2zzcfa_infoz00;
																{	/* Cfa/show.scm 111 */
																	BgL_objectz00_bglt BgL_arg1807z00_3693;

																	{	/* Cfa/show.scm 111 */
																		obj_t BgL_tmpz00_4183;

																		BgL_tmpz00_4183 =
																			((obj_t)
																			((BgL_objectz00_bglt) BgL_valuez00_2989));
																		BgL_arg1807z00_3693 =
																			(BgL_objectz00_bglt) (BgL_tmpz00_4183);
																	}
																	if (BGL_CONDEXPAND_ISA_ARCH64())
																		{	/* Cfa/show.scm 111 */
																			long BgL_idxz00_3699;

																			BgL_idxz00_3699 =
																				BGL_OBJECT_INHERITANCE_NUM
																				(BgL_arg1807z00_3693);
																			BgL_test2000z00_4182 =
																				(VECTOR_REF
																				(BGl_za2inheritancesza2z00zz__objectz00,
																					(BgL_idxz00_3699 + 4L)) ==
																				BgL_classz00_3691);
																		}
																	else
																		{	/* Cfa/show.scm 111 */
																			bool_t BgL_res1935z00_3724;

																			{	/* Cfa/show.scm 111 */
																				obj_t BgL_oclassz00_3707;

																				{	/* Cfa/show.scm 111 */
																					obj_t BgL_arg1815z00_3715;
																					long BgL_arg1816z00_3716;

																					BgL_arg1815z00_3715 =
																						(BGl_za2classesza2z00zz__objectz00);
																					{	/* Cfa/show.scm 111 */
																						long BgL_arg1817z00_3717;

																						BgL_arg1817z00_3717 =
																							BGL_OBJECT_CLASS_NUM
																							(BgL_arg1807z00_3693);
																						BgL_arg1816z00_3716 =
																							(BgL_arg1817z00_3717 -
																							OBJECT_TYPE);
																					}
																					BgL_oclassz00_3707 =
																						VECTOR_REF(BgL_arg1815z00_3715,
																						BgL_arg1816z00_3716);
																				}
																				{	/* Cfa/show.scm 111 */
																					bool_t BgL__ortest_1115z00_3708;

																					BgL__ortest_1115z00_3708 =
																						(BgL_classz00_3691 ==
																						BgL_oclassz00_3707);
																					if (BgL__ortest_1115z00_3708)
																						{	/* Cfa/show.scm 111 */
																							BgL_res1935z00_3724 =
																								BgL__ortest_1115z00_3708;
																						}
																					else
																						{	/* Cfa/show.scm 111 */
																							long BgL_odepthz00_3709;

																							{	/* Cfa/show.scm 111 */
																								obj_t BgL_arg1804z00_3710;

																								BgL_arg1804z00_3710 =
																									(BgL_oclassz00_3707);
																								BgL_odepthz00_3709 =
																									BGL_CLASS_DEPTH
																									(BgL_arg1804z00_3710);
																							}
																							if ((4L < BgL_odepthz00_3709))
																								{	/* Cfa/show.scm 111 */
																									obj_t BgL_arg1802z00_3712;

																									{	/* Cfa/show.scm 111 */
																										obj_t BgL_arg1803z00_3713;

																										BgL_arg1803z00_3713 =
																											(BgL_oclassz00_3707);
																										BgL_arg1802z00_3712 =
																											BGL_CLASS_ANCESTORS_REF
																											(BgL_arg1803z00_3713, 4L);
																									}
																									BgL_res1935z00_3724 =
																										(BgL_arg1802z00_3712 ==
																										BgL_classz00_3691);
																								}
																							else
																								{	/* Cfa/show.scm 111 */
																									BgL_res1935z00_3724 =
																										((bool_t) 0);
																								}
																						}
																				}
																			}
																			BgL_test2000z00_4182 =
																				BgL_res1935z00_3724;
																		}
																}
															}
															if (BgL_test2000z00_4182)
																{	/* Cfa/show.scm 111 */
																	bgl_display_string
																		(BGl_string1963z00zzcfa_showz00,
																		BgL_portz00_7);
																	{	/* Cfa/show.scm 113 */
																		obj_t BgL_arg1593z00_3006;

																		{	/* Cfa/show.scm 113 */
																			BgL_approxz00_bglt BgL_arg1594z00_3007;

																			{
																				BgL_cfunzf2cinfozf2_bglt
																					BgL_auxz00_4207;
																				{
																					obj_t BgL_auxz00_4208;

																					{	/* Cfa/show.scm 113 */
																						BgL_objectz00_bglt BgL_tmpz00_4209;

																						BgL_tmpz00_4209 =
																							((BgL_objectz00_bglt)
																							((BgL_cfunz00_bglt)
																								BgL_valuez00_2989));
																						BgL_auxz00_4208 =
																							BGL_OBJECT_WIDENING
																							(BgL_tmpz00_4209);
																					}
																					BgL_auxz00_4207 =
																						((BgL_cfunzf2cinfozf2_bglt)
																						BgL_auxz00_4208);
																				}
																				BgL_arg1594z00_3007 =
																					(((BgL_cfunzf2cinfozf2_bglt)
																						COBJECT(BgL_auxz00_4207))->
																					BgL_approxz00);
																			}
																			BgL_arg1593z00_3006 =
																				BGl_shapez00zztools_shapez00(
																				((obj_t) BgL_arg1594z00_3007));
																		}
																		bgl_display_obj(BgL_arg1593z00_3006,
																			BgL_portz00_7);
																	}
																}
															else
																{	/* Cfa/show.scm 111 */
																	BFALSE;
																}
														}
												}
										}
								}
						}
				}
				{	/* Cfa/show.scm 114 */
					obj_t BgL_arg1595z00_3008;

					BgL_arg1595z00_3008 = bgl_close_output_port(BgL_portz00_7);
					return bstring_to_symbol(((obj_t) BgL_arg1595z00_3008));
				}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_showz00(void)
	{
		{	/* Cfa/show.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_showz00(void)
	{
		{	/* Cfa/show.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_showz00(void)
	{
		{	/* Cfa/show.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00,
				BGl_reshapedzd2localzd2zzcfa_infoz00, BGl_proc1964z00zzcfa_showz00,
				BGl_string1965z00zzcfa_showz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_shapezd2envzd2zztools_shapez00,
				BGl_reshapedzd2globalzd2zzcfa_infoz00, BGl_proc1966z00zzcfa_showz00,
				BGl_string1965z00zzcfa_showz00);
		}

	}



/* &shape-reshaped-globa1494 */
	obj_t BGl_z62shapezd2reshapedzd2globa1494z62zzcfa_showz00(obj_t
		BgL_envz00_3948, obj_t BgL_globalz00_3949)
	{
		{	/* Cfa/show.scm 85 */
			{

				{	/* Cfa/show.scm 86 */
					obj_t BgL_portz00_3955;

					{	/* Cfa/show.scm 86 */

						{	/* Cfa/show.scm 86 */

							BgL_portz00_3955 =
								BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(BTRUE);
						}
					}
					{	/* Cfa/show.scm 87 */
						obj_t BgL_auxz00_4224;

						{	/* Cfa/show.scm 85 */
							obj_t BgL_nextzd2method1493zd2_3954;

							BgL_nextzd2method1493zd2_3954 =
								BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
								((BgL_objectz00_bglt)
									((BgL_globalz00_bglt) BgL_globalz00_3949)),
								BGl_shapezd2envzd2zztools_shapez00,
								BGl_reshapedzd2globalzd2zzcfa_infoz00);
							BgL_auxz00_4224 =
								BGL_PROCEDURE_CALL1(BgL_nextzd2method1493zd2_3954,
								((obj_t) ((BgL_globalz00_bglt) BgL_globalz00_3949)));
						}
						bgl_display_obj(BgL_auxz00_4224, BgL_portz00_3955);
					}
					return
						BGl_cfazd2variablezd2shapez00zzcfa_showz00(
						((BgL_variablez00_bglt)
							((BgL_globalz00_bglt) BgL_globalz00_3949)), BgL_portz00_3955);
				}
			}
		}

	}



/* &shape-reshaped-local1492 */
	obj_t BGl_z62shapezd2reshapedzd2local1492z62zzcfa_showz00(obj_t
		BgL_envz00_3950, obj_t BgL_localz00_3951)
	{
		{	/* Cfa/show.scm 77 */
			{

				{	/* Cfa/show.scm 78 */
					obj_t BgL_portz00_3959;

					{	/* Cfa/show.scm 78 */

						{	/* Cfa/show.scm 78 */

							BgL_portz00_3959 =
								BGl_openzd2outputzd2stringz00zz__r4_ports_6_10_1z00(BTRUE);
						}
					}
					{	/* Cfa/show.scm 79 */
						obj_t BgL_auxz00_4239;

						{	/* Cfa/show.scm 77 */
							obj_t BgL_nextzd2method1491zd2_3958;

							BgL_nextzd2method1491zd2_3958 =
								BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
								((BgL_objectz00_bglt)
									((BgL_localz00_bglt) BgL_localz00_3951)),
								BGl_shapezd2envzd2zztools_shapez00,
								BGl_reshapedzd2localzd2zzcfa_infoz00);
							BgL_auxz00_4239 =
								BGL_PROCEDURE_CALL1(BgL_nextzd2method1491zd2_3958,
								((obj_t) ((BgL_localz00_bglt) BgL_localz00_3951)));
						}
						bgl_display_obj(BgL_auxz00_4239, BgL_portz00_3959);
					}
					return
						BGl_cfazd2variablezd2shapez00zzcfa_showz00(
						((BgL_variablez00_bglt)
							((BgL_localz00_bglt) BgL_localz00_3951)), BgL_portz00_3959);
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_showz00(void)
	{
		{	/* Cfa/show.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1967z00zzcfa_showz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1967z00zzcfa_showz00));
			BGl_modulezd2initializa7ationz75zztools_speekz00(61247943L,
				BSTRING_TO_STRING(BGl_string1967z00zzcfa_showz00));
			BGl_modulezd2initializa7ationz75zzwrite_schemez00(305499406L,
				BSTRING_TO_STRING(BGl_string1967z00zzcfa_showz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1967z00zzcfa_showz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1967z00zzcfa_showz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1967z00zzcfa_showz00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string1967z00zzcfa_showz00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string1967z00zzcfa_showz00));
			BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string1967z00zzcfa_showz00));
			BGl_modulezd2initializa7ationz75zzcfa_collectz00(220306886L,
				BSTRING_TO_STRING(BGl_string1967z00zzcfa_showz00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_iteratez00(131412063L,
				BSTRING_TO_STRING(BGl_string1967z00zzcfa_showz00));
		}

	}

#ifdef __cplusplus
}
#endif
