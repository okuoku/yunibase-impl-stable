/*===========================================================================*/
/*   (Cfa/arithmetic.scm)                                                    */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/arithmetic.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_ARITHMETIC_TYPE_DEFINITIONS
#define BGL_CFA_ARITHMETIC_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_prezd2arithmeticzd2appz00_bgl
	{
		obj_t BgL_speczd2typeszd2;
	}                                *BgL_prezd2arithmeticzd2appz00_bglt;

	typedef struct BgL_arithmeticzd2appzd2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		obj_t BgL_speczd2typeszd2;
	}                          *BgL_arithmeticzd2appzd2_bglt;


#endif													// BGL_CFA_ARITHMETIC_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	BGL_EXPORTED_DECL obj_t
		BGl_cleanupzd2arithmeticzd2nodesz12z12zzcfa_arithmeticz00(obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	extern obj_t BGl_sfunz00zzast_varz00;
	extern obj_t BGl_continuezd2cfaz12zc0zzcfa_iteratez00(obj_t);
	static obj_t BGl_unspecifiedzd2typeze70z35zzcfa_arithmeticz00(obj_t);
	static obj_t
		BGl_z62cleanupzd2nodez12zd2funcal1590z70zzcfa_arithmeticz00(obj_t, obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	static obj_t
		BGl_z62cleanupzd2nodez12zd2switch1572z70zzcfa_arithmeticz00(obj_t, obj_t);
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_objectzd2initzd2zzcfa_arithmeticz00(void);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_methodzd2initzd2zzcfa_arithmeticz00(void);
	static obj_t
		BGl_z62cleanupzd2nodez12zd2makezd2b1582za2zzcfa_arithmeticz00(obj_t, obj_t);
	static obj_t
		BGl_z62cleanupzd2nodez12zd2jumpzd2e1580za2zzcfa_arithmeticz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static obj_t BGl_z62cleanupzd2nodez121559za2zzcfa_arithmeticz00(obj_t, obj_t);
	extern obj_t BGl_cfunz00zzast_varz00;
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_cleanupzd2typeze70z35zzcfa_arithmeticz00(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_arithmeticz00(void);
	extern BgL_approxz00_bglt BGl_makezd2emptyzd2approxz00zzcfa_approxz00(void);
	static obj_t BGl_z62cleanupzd2nodez12zd2setq1566z70zzcfa_arithmeticz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62cleanupzd2nodez12zd2letzd2va1576za2zzcfa_arithmeticz00(obj_t, obj_t);
	static obj_t
		BGl_z62cleanupzd2nodez12zd2letzd2fu1574za2zzcfa_arithmeticz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t
		BGl_z62cleanupzd2arithmeticzd2nodesz12z70zzcfa_arithmeticz00(obj_t, obj_t);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	static obj_t
		BGl_z62cleanupzd2nodez12zd2setzd2ex1578za2zzcfa_arithmeticz00(obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	extern obj_t BGl_za2intza2z00zztype_cachez00;
	static obj_t
		BGl_z62nodezd2setupz12zd2prezd2arit1594za2zzcfa_arithmeticz00(obj_t, obj_t);
	extern obj_t BGl_nodezd2setupza2z12z62zzcfa_setupz00(obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_arithmeticz00 = BUNSPEC;
	static obj_t
		BGl_z62cleanupzd2nodez12zd2appzd2ly1588za2zzcfa_arithmeticz00(obj_t, obj_t);
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzcfa_arithmeticz00(void);
	static obj_t
		BGl_z62cleanupzd2nodez12zd2condit1568z70zzcfa_arithmeticz00(obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcfa_arithmeticz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_arithmeticzd2appzd2zzcfa_info2z00;
	extern obj_t BGl_typez00zztype_typez00;
	extern BgL_approxz00_bglt BGl_loosez12z12zzcfa_loosez00(BgL_approxz00_bglt,
		obj_t);
	extern obj_t BGl_za2longza2z00zztype_cachez00;
	static obj_t BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(BgL_nodez00_bglt);
	static obj_t
		BGl_z62cleanupzd2nodez12zd2boxzd2re1584za2zzcfa_arithmeticz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_za2arithmeticzd2nodesza2zd2zzcfa_arithmeticz00 = BUNSPEC;
	static obj_t BGl_z62cleanupzd2nodez12zd2app1592z70zzcfa_arithmeticz00(obj_t,
		obj_t);
	static obj_t
		BGl_z62cleanupzd2nodez12zd2boxzd2se1586za2zzcfa_arithmeticz00(obj_t, obj_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	BGL_EXPORTED_DECL obj_t
		BGl_modulezd2initializa7ationz75zzcfa_arithmeticz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_setupz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_cfaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_iteratez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_loosez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t
		BGl_z62cleanupzd2nodez12zd2sequen1562z70zzcfa_arithmeticz00(obj_t, obj_t);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzcfa_arithmeticz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_arithmeticz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_arithmeticz00(void);
	extern BgL_approxz00_bglt
		BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_typez00_bglt);
	extern obj_t BGl_prezd2arithmeticzd2appz00zzcfa_info2z00;
	static obj_t BGl_z62cleanupzd2nodez12zd2fail1570z70zzcfa_arithmeticz00(obj_t,
		obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	static obj_t BGl_z62cleanupzd2nodez12zd2sync1564z70zzcfa_arithmeticz00(obj_t,
		obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	extern BgL_approxz00_bglt BGl_cfaz12z12zzcfa_cfaz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	static obj_t BGl_z62cleanupzd2nodez12za2zzcfa_arithmeticz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t BGl_approxzd2setzd2typez12z12zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2arithmeticzd2app1596z70zzcfa_arithmeticz00(obj_t, obj_t);
	static obj_t __cnst[3];


	   
		 
		DEFINE_STRING(BGl_string2143z00zzcfa_arithmeticz00,
		BgL_bgl_string2143za700za7za7c2169za7, "cleanup-node!1559", 17);
	      DEFINE_STRING(BGl_string2145z00zzcfa_arithmeticz00,
		BgL_bgl_string2145za700za7za7c2170za7, "cleanup-node!", 13);
	      DEFINE_STRING(BGl_string2162z00zzcfa_arithmeticz00,
		BgL_bgl_string2162za700za7za7c2171za7, "node-setup!", 11);
	      DEFINE_STRING(BGl_string2164z00zzcfa_arithmeticz00,
		BgL_bgl_string2164za700za7za7c2172za7, "cfa!::approx", 12);
	      DEFINE_STRING(BGl_string2165z00zzcfa_arithmeticz00,
		BgL_bgl_string2165za700za7za7c2173za7, "Illegal arithmetic node", 23);
	      DEFINE_STRING(BGl_string2166z00zzcfa_arithmeticz00,
		BgL_bgl_string2166za700za7za7c2174za7, "cfa_arithmetic", 14);
	      DEFINE_STRING(BGl_string2167z00zzcfa_arithmeticz00,
		BgL_bgl_string2167za700za7za7c2175za7, "all c-eq? arithmetic-app ", 25);
	extern obj_t BGl_cfaz12zd2envzc0zzcfa_cfaz00;
	extern obj_t BGl_nodezd2setupz12zd2envz12zzcfa_setupz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2142z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2176z00,
		BGl_z62cleanupzd2nodez121559za2zzcfa_arithmeticz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2144z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2177z00,
		BGl_z62cleanupzd2nodez12zd2sequen1562z70zzcfa_arithmeticz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2146z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2178z00,
		BGl_z62cleanupzd2nodez12zd2sync1564z70zzcfa_arithmeticz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2147z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2179z00,
		BGl_z62cleanupzd2nodez12zd2setq1566z70zzcfa_arithmeticz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2148z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2180z00,
		BGl_z62cleanupzd2nodez12zd2condit1568z70zzcfa_arithmeticz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2149z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2181z00,
		BGl_z62cleanupzd2nodez12zd2fail1570z70zzcfa_arithmeticz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2150z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2182z00,
		BGl_z62cleanupzd2nodez12zd2switch1572z70zzcfa_arithmeticz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2151z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2183z00,
		BGl_z62cleanupzd2nodez12zd2letzd2fu1574za2zzcfa_arithmeticz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2152z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2184z00,
		BGl_z62cleanupzd2nodez12zd2letzd2va1576za2zzcfa_arithmeticz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2153z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2185z00,
		BGl_z62cleanupzd2nodez12zd2setzd2ex1578za2zzcfa_arithmeticz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2154z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2186z00,
		BGl_z62cleanupzd2nodez12zd2jumpzd2e1580za2zzcfa_arithmeticz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2155z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2187z00,
		BGl_z62cleanupzd2nodez12zd2makezd2b1582za2zzcfa_arithmeticz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2156z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2188z00,
		BGl_z62cleanupzd2nodez12zd2boxzd2re1584za2zzcfa_arithmeticz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2157z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2189z00,
		BGl_z62cleanupzd2nodez12zd2boxzd2se1586za2zzcfa_arithmeticz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2158z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2190z00,
		BGl_z62cleanupzd2nodez12zd2appzd2ly1588za2zzcfa_arithmeticz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2159z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2191z00,
		BGl_z62cleanupzd2nodez12zd2funcal1590z70zzcfa_arithmeticz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2160z00zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2192z00,
		BGl_z62cleanupzd2nodez12zd2app1592z70zzcfa_arithmeticz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2161z00zzcfa_arithmeticz00,
		BgL_bgl_za762nodeza7d2setupza72193za7,
		BGl_z62nodezd2setupz12zd2prezd2arit1594za2zzcfa_arithmeticz00, 0L, BUNSPEC,
		1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2163z00zzcfa_arithmeticz00,
		BgL_bgl_za762cfaza712za7d2arit2194za7,
		BGl_z62cfaz12zd2arithmeticzd2app1596z70zzcfa_arithmeticz00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE
		(BGl_cleanupzd2arithmeticzd2nodesz12zd2envzc0zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2ari2195z00,
		BGl_z62cleanupzd2arithmeticzd2nodesz12z70zzcfa_arithmeticz00, 0L, BUNSPEC,
		1);
	     
		DEFINE_STATIC_BGL_GENERIC(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
		BgL_bgl_za762cleanupza7d2nod2196z00,
		BGl_z62cleanupzd2nodez12za2zzcfa_arithmeticz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void
				*) (&BGl_requirezd2initializa7ationz75zzcfa_arithmeticz00));
		     ADD_ROOT((void
				*) (&BGl_za2arithmeticzd2nodesza2zd2zzcfa_arithmeticz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t
		BGl_modulezd2initializa7ationz75zzcfa_arithmeticz00(long
		BgL_checksumz00_4694, char *BgL_fromz00_4695)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_arithmeticz00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_arithmeticz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_arithmeticz00();
					BGl_libraryzd2moduleszd2initz00zzcfa_arithmeticz00();
					BGl_cnstzd2initzd2zzcfa_arithmeticz00();
					BGl_importedzd2moduleszd2initz00zzcfa_arithmeticz00();
					BGl_genericzd2initzd2zzcfa_arithmeticz00();
					BGl_methodzd2initzd2zzcfa_arithmeticz00();
					return BGl_toplevelzd2initzd2zzcfa_arithmeticz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_arithmeticz00(void)
	{
		{	/* Cfa/arithmetic.scm 28 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_arithmetic");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cfa_arithmetic");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_arithmetic");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_arithmetic");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L,
				"cfa_arithmetic");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L,
				"cfa_arithmetic");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cfa_arithmetic");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cfa_arithmetic");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L,
				"cfa_arithmetic");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_arithmetic");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_arithmeticz00(void)
	{
		{	/* Cfa/arithmetic.scm 28 */
			{	/* Cfa/arithmetic.scm 28 */
				obj_t BgL_cportz00_4514;

				{	/* Cfa/arithmetic.scm 28 */
					obj_t BgL_stringz00_4521;

					BgL_stringz00_4521 = BGl_string2167z00zzcfa_arithmeticz00;
					{	/* Cfa/arithmetic.scm 28 */
						obj_t BgL_startz00_4522;

						BgL_startz00_4522 = BINT(0L);
						{	/* Cfa/arithmetic.scm 28 */
							obj_t BgL_endz00_4523;

							BgL_endz00_4523 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4521)));
							{	/* Cfa/arithmetic.scm 28 */

								BgL_cportz00_4514 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4521, BgL_startz00_4522, BgL_endz00_4523);
				}}}}
				{
					long BgL_iz00_4515;

					BgL_iz00_4515 = 2L;
				BgL_loopz00_4516:
					if ((BgL_iz00_4515 == -1L))
						{	/* Cfa/arithmetic.scm 28 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/arithmetic.scm 28 */
							{	/* Cfa/arithmetic.scm 28 */
								obj_t BgL_arg2168z00_4517;

								{	/* Cfa/arithmetic.scm 28 */

									{	/* Cfa/arithmetic.scm 28 */
										obj_t BgL_locationz00_4519;

										BgL_locationz00_4519 = BBOOL(((bool_t) 0));
										{	/* Cfa/arithmetic.scm 28 */

											BgL_arg2168z00_4517 =
												BGl_readz00zz__readerz00(BgL_cportz00_4514,
												BgL_locationz00_4519);
										}
									}
								}
								{	/* Cfa/arithmetic.scm 28 */
									int BgL_tmpz00_4725;

									BgL_tmpz00_4725 = (int) (BgL_iz00_4515);
									CNST_TABLE_SET(BgL_tmpz00_4725, BgL_arg2168z00_4517);
							}}
							{	/* Cfa/arithmetic.scm 28 */
								int BgL_auxz00_4520;

								BgL_auxz00_4520 = (int) ((BgL_iz00_4515 - 1L));
								{
									long BgL_iz00_4730;

									BgL_iz00_4730 = (long) (BgL_auxz00_4520);
									BgL_iz00_4515 = BgL_iz00_4730;
									goto BgL_loopz00_4516;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_arithmeticz00(void)
	{
		{	/* Cfa/arithmetic.scm 28 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_arithmeticz00(void)
	{
		{	/* Cfa/arithmetic.scm 28 */
			BGl_za2arithmeticzd2nodesza2zd2zzcfa_arithmeticz00 = BNIL;
			return BUNSPEC;
		}

	}



/* cleanup-arithmetic-nodes! */
	BGL_EXPORTED_DEF obj_t
		BGl_cleanupzd2arithmeticzd2nodesz12z12zzcfa_arithmeticz00(obj_t
		BgL_globalsz00_3)
	{
		{	/* Cfa/arithmetic.scm 52 */
			{
				obj_t BgL_l1523z00_2991;

				BgL_l1523z00_2991 = BGl_za2arithmeticzd2nodesza2zd2zzcfa_arithmeticz00;
			BgL_zc3z04anonymousza31606ze3z87_2992:
				if (PAIRP(BgL_l1523z00_2991))
					{	/* Cfa/arithmetic.scm 63 */
						{	/* Cfa/arithmetic.scm 64 */
							obj_t BgL_nodez00_2994;

							BgL_nodez00_2994 = CAR(BgL_l1523z00_2991);
							{	/* Cfa/arithmetic.scm 65 */
								BgL_variablez00_bglt BgL_fz00_2996;

								BgL_fz00_2996 =
									(((BgL_varz00_bglt) COBJECT(
											(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt)
															((BgL_appz00_bglt) BgL_nodez00_2994))))->
												BgL_funz00)))->BgL_variablez00);
								{	/* Cfa/arithmetic.scm 65 */
									BgL_valuez00_bglt BgL_valz00_2997;

									BgL_valz00_2997 =
										(((BgL_variablez00_bglt) COBJECT(BgL_fz00_2996))->
										BgL_valuez00);
									{	/* Cfa/arithmetic.scm 66 */

										{	/* Cfa/arithmetic.scm 68 */
											bool_t BgL_test2200z00_4741;

											{	/* Cfa/arithmetic.scm 68 */
												BgL_typez00_bglt BgL_arg1611z00_3000;

												BgL_arg1611z00_3000 =
													(((BgL_variablez00_bglt) COBJECT(BgL_fz00_2996))->
													BgL_typez00);
												BgL_test2200z00_4741 =
													(((obj_t) BgL_arg1611z00_3000) ==
													BGl_za2_za2z00zztype_cachez00);
											}
											if (BgL_test2200z00_4741)
												{	/* Cfa/arithmetic.scm 69 */
													BgL_typez00_bglt BgL_vz00_3955;

													BgL_vz00_3955 =
														((BgL_typez00_bglt)
														BGl_za2objza2z00zztype_cachez00);
													((((BgL_variablez00_bglt) COBJECT(BgL_fz00_2996))->
															BgL_typez00) =
														((BgL_typez00_bglt) BgL_vz00_3955), BUNSPEC);
												}
											else
												{	/* Cfa/arithmetic.scm 68 */
													BFALSE;
												}
										}
										{	/* Cfa/arithmetic.scm 70 */
											bool_t BgL_test2201z00_4747;

											{	/* Cfa/arithmetic.scm 70 */
												BgL_typez00_bglt BgL_arg1616z00_3003;

												BgL_arg1616z00_3003 =
													(((BgL_nodez00_bglt) COBJECT(
															((BgL_nodez00_bglt) BgL_nodez00_2994)))->
													BgL_typez00);
												BgL_test2201z00_4747 =
													(((obj_t) BgL_arg1616z00_3003) ==
													BGl_za2_za2z00zztype_cachez00);
											}
											if (BgL_test2201z00_4747)
												{	/* Cfa/arithmetic.scm 71 */
													BgL_typez00_bglt BgL_vz00_3958;

													BgL_vz00_3958 =
														((BgL_typez00_bglt)
														BGl_za2objza2z00zztype_cachez00);
													((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
																		BgL_nodez00_2994)))->BgL_typez00) =
														((BgL_typez00_bglt) BgL_vz00_3958), BUNSPEC);
												}
											else
												{	/* Cfa/arithmetic.scm 70 */
													BFALSE;
												}
										}
										{	/* Cfa/arithmetic.scm 73 */
											bool_t BgL_test2202z00_4755;

											{	/* Cfa/arithmetic.scm 73 */
												obj_t BgL_classz00_3959;

												BgL_classz00_3959 = BGl_sfunz00zzast_varz00;
												{	/* Cfa/arithmetic.scm 73 */
													BgL_objectz00_bglt BgL_arg1807z00_3961;

													{	/* Cfa/arithmetic.scm 73 */
														obj_t BgL_tmpz00_4756;

														BgL_tmpz00_4756 =
															((obj_t) ((BgL_objectz00_bglt) BgL_valz00_2997));
														BgL_arg1807z00_3961 =
															(BgL_objectz00_bglt) (BgL_tmpz00_4756);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Cfa/arithmetic.scm 73 */
															long BgL_idxz00_3967;

															BgL_idxz00_3967 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3961);
															BgL_test2202z00_4755 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_3967 + 3L)) == BgL_classz00_3959);
														}
													else
														{	/* Cfa/arithmetic.scm 73 */
															bool_t BgL_res2125z00_3992;

															{	/* Cfa/arithmetic.scm 73 */
																obj_t BgL_oclassz00_3975;

																{	/* Cfa/arithmetic.scm 73 */
																	obj_t BgL_arg1815z00_3983;
																	long BgL_arg1816z00_3984;

																	BgL_arg1815z00_3983 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Cfa/arithmetic.scm 73 */
																		long BgL_arg1817z00_3985;

																		BgL_arg1817z00_3985 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3961);
																		BgL_arg1816z00_3984 =
																			(BgL_arg1817z00_3985 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_3975 =
																		VECTOR_REF(BgL_arg1815z00_3983,
																		BgL_arg1816z00_3984);
																}
																{	/* Cfa/arithmetic.scm 73 */
																	bool_t BgL__ortest_1115z00_3976;

																	BgL__ortest_1115z00_3976 =
																		(BgL_classz00_3959 == BgL_oclassz00_3975);
																	if (BgL__ortest_1115z00_3976)
																		{	/* Cfa/arithmetic.scm 73 */
																			BgL_res2125z00_3992 =
																				BgL__ortest_1115z00_3976;
																		}
																	else
																		{	/* Cfa/arithmetic.scm 73 */
																			long BgL_odepthz00_3977;

																			{	/* Cfa/arithmetic.scm 73 */
																				obj_t BgL_arg1804z00_3978;

																				BgL_arg1804z00_3978 =
																					(BgL_oclassz00_3975);
																				BgL_odepthz00_3977 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_3978);
																			}
																			if ((3L < BgL_odepthz00_3977))
																				{	/* Cfa/arithmetic.scm 73 */
																					obj_t BgL_arg1802z00_3980;

																					{	/* Cfa/arithmetic.scm 73 */
																						obj_t BgL_arg1803z00_3981;

																						BgL_arg1803z00_3981 =
																							(BgL_oclassz00_3975);
																						BgL_arg1802z00_3980 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_3981, 3L);
																					}
																					BgL_res2125z00_3992 =
																						(BgL_arg1802z00_3980 ==
																						BgL_classz00_3959);
																				}
																			else
																				{	/* Cfa/arithmetic.scm 73 */
																					BgL_res2125z00_3992 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2202z00_4755 = BgL_res2125z00_3992;
														}
												}
											}
											if (BgL_test2202z00_4755)
												{	/* Cfa/arithmetic.scm 74 */
													obj_t BgL_arg1625z00_3005;

													{	/* Cfa/arithmetic.scm 74 */
														obj_t BgL_l1517z00_3006;

														BgL_l1517z00_3006 =
															(((BgL_sfunz00_bglt) COBJECT(
																	((BgL_sfunz00_bglt) BgL_valz00_2997)))->
															BgL_argsz00);
														if (NULLP(BgL_l1517z00_3006))
															{	/* Cfa/arithmetic.scm 74 */
																BgL_arg1625z00_3005 = BNIL;
															}
														else
															{	/* Cfa/arithmetic.scm 74 */
																obj_t BgL_head1519z00_3008;

																{	/* Cfa/arithmetic.scm 74 */
																	obj_t BgL_arg1646z00_3020;

																	{	/* Cfa/arithmetic.scm 74 */
																		obj_t BgL_arg1650z00_3021;

																		BgL_arg1650z00_3021 =
																			CAR(((obj_t) BgL_l1517z00_3006));
																		BgL_arg1646z00_3020 =
																			BGl_cleanupzd2typeze70z35zzcfa_arithmeticz00
																			(BgL_arg1650z00_3021);
																	}
																	BgL_head1519z00_3008 =
																		MAKE_YOUNG_PAIR(BgL_arg1646z00_3020, BNIL);
																}
																{	/* Cfa/arithmetic.scm 74 */
																	obj_t BgL_g1522z00_3009;

																	BgL_g1522z00_3009 =
																		CDR(((obj_t) BgL_l1517z00_3006));
																	{
																		obj_t BgL_l1517z00_3011;
																		obj_t BgL_tail1520z00_3012;

																		BgL_l1517z00_3011 = BgL_g1522z00_3009;
																		BgL_tail1520z00_3012 = BgL_head1519z00_3008;
																	BgL_zc3z04anonymousza31627ze3z87_3013:
																		if (NULLP(BgL_l1517z00_3011))
																			{	/* Cfa/arithmetic.scm 74 */
																				BgL_arg1625z00_3005 =
																					BgL_head1519z00_3008;
																			}
																		else
																			{	/* Cfa/arithmetic.scm 74 */
																				obj_t BgL_newtail1521z00_3015;

																				{	/* Cfa/arithmetic.scm 74 */
																					obj_t BgL_arg1630z00_3017;

																					{	/* Cfa/arithmetic.scm 74 */
																						obj_t BgL_arg1642z00_3018;

																						BgL_arg1642z00_3018 =
																							CAR(((obj_t) BgL_l1517z00_3011));
																						BgL_arg1630z00_3017 =
																							BGl_cleanupzd2typeze70z35zzcfa_arithmeticz00
																							(BgL_arg1642z00_3018);
																					}
																					BgL_newtail1521z00_3015 =
																						MAKE_YOUNG_PAIR(BgL_arg1630z00_3017,
																						BNIL);
																				}
																				SET_CDR(BgL_tail1520z00_3012,
																					BgL_newtail1521z00_3015);
																				{	/* Cfa/arithmetic.scm 74 */
																					obj_t BgL_arg1629z00_3016;

																					BgL_arg1629z00_3016 =
																						CDR(((obj_t) BgL_l1517z00_3011));
																					{
																						obj_t BgL_tail1520z00_4799;
																						obj_t BgL_l1517z00_4798;

																						BgL_l1517z00_4798 =
																							BgL_arg1629z00_3016;
																						BgL_tail1520z00_4799 =
																							BgL_newtail1521z00_3015;
																						BgL_tail1520z00_3012 =
																							BgL_tail1520z00_4799;
																						BgL_l1517z00_3011 =
																							BgL_l1517z00_4798;
																						goto
																							BgL_zc3z04anonymousza31627ze3z87_3013;
																					}
																				}
																			}
																	}
																}
															}
													}
													((((BgL_sfunz00_bglt) COBJECT(
																	((BgL_sfunz00_bglt) BgL_valz00_2997)))->
															BgL_argsz00) =
														((obj_t) BgL_arg1625z00_3005), BUNSPEC);
												}
											else
												{	/* Cfa/arithmetic.scm 75 */
													bool_t BgL_test2208z00_4802;

													{	/* Cfa/arithmetic.scm 75 */
														obj_t BgL_classz00_4000;

														BgL_classz00_4000 = BGl_cfunz00zzast_varz00;
														{	/* Cfa/arithmetic.scm 75 */
															BgL_objectz00_bglt BgL_arg1807z00_4002;

															{	/* Cfa/arithmetic.scm 75 */
																obj_t BgL_tmpz00_4803;

																BgL_tmpz00_4803 =
																	((obj_t)
																	((BgL_objectz00_bglt) BgL_valz00_2997));
																BgL_arg1807z00_4002 =
																	(BgL_objectz00_bglt) (BgL_tmpz00_4803);
															}
															if (BGL_CONDEXPAND_ISA_ARCH64())
																{	/* Cfa/arithmetic.scm 75 */
																	long BgL_idxz00_4008;

																	BgL_idxz00_4008 =
																		BGL_OBJECT_INHERITANCE_NUM
																		(BgL_arg1807z00_4002);
																	BgL_test2208z00_4802 =
																		(VECTOR_REF
																		(BGl_za2inheritancesza2z00zz__objectz00,
																			(BgL_idxz00_4008 + 3L)) ==
																		BgL_classz00_4000);
																}
															else
																{	/* Cfa/arithmetic.scm 75 */
																	bool_t BgL_res2126z00_4033;

																	{	/* Cfa/arithmetic.scm 75 */
																		obj_t BgL_oclassz00_4016;

																		{	/* Cfa/arithmetic.scm 75 */
																			obj_t BgL_arg1815z00_4024;
																			long BgL_arg1816z00_4025;

																			BgL_arg1815z00_4024 =
																				(BGl_za2classesza2z00zz__objectz00);
																			{	/* Cfa/arithmetic.scm 75 */
																				long BgL_arg1817z00_4026;

																				BgL_arg1817z00_4026 =
																					BGL_OBJECT_CLASS_NUM
																					(BgL_arg1807z00_4002);
																				BgL_arg1816z00_4025 =
																					(BgL_arg1817z00_4026 - OBJECT_TYPE);
																			}
																			BgL_oclassz00_4016 =
																				VECTOR_REF(BgL_arg1815z00_4024,
																				BgL_arg1816z00_4025);
																		}
																		{	/* Cfa/arithmetic.scm 75 */
																			bool_t BgL__ortest_1115z00_4017;

																			BgL__ortest_1115z00_4017 =
																				(BgL_classz00_4000 ==
																				BgL_oclassz00_4016);
																			if (BgL__ortest_1115z00_4017)
																				{	/* Cfa/arithmetic.scm 75 */
																					BgL_res2126z00_4033 =
																						BgL__ortest_1115z00_4017;
																				}
																			else
																				{	/* Cfa/arithmetic.scm 75 */
																					long BgL_odepthz00_4018;

																					{	/* Cfa/arithmetic.scm 75 */
																						obj_t BgL_arg1804z00_4019;

																						BgL_arg1804z00_4019 =
																							(BgL_oclassz00_4016);
																						BgL_odepthz00_4018 =
																							BGL_CLASS_DEPTH
																							(BgL_arg1804z00_4019);
																					}
																					if ((3L < BgL_odepthz00_4018))
																						{	/* Cfa/arithmetic.scm 75 */
																							obj_t BgL_arg1802z00_4021;

																							{	/* Cfa/arithmetic.scm 75 */
																								obj_t BgL_arg1803z00_4022;

																								BgL_arg1803z00_4022 =
																									(BgL_oclassz00_4016);
																								BgL_arg1802z00_4021 =
																									BGL_CLASS_ANCESTORS_REF
																									(BgL_arg1803z00_4022, 3L);
																							}
																							BgL_res2126z00_4033 =
																								(BgL_arg1802z00_4021 ==
																								BgL_classz00_4000);
																						}
																					else
																						{	/* Cfa/arithmetic.scm 75 */
																							BgL_res2126z00_4033 =
																								((bool_t) 0);
																						}
																				}
																		}
																	}
																	BgL_test2208z00_4802 = BgL_res2126z00_4033;
																}
														}
													}
													if (BgL_test2208z00_4802)
														{	/* Cfa/arithmetic.scm 76 */
															obj_t BgL_g1166z00_3023;

															BgL_g1166z00_3023 =
																(((BgL_cfunz00_bglt) COBJECT(
																		((BgL_cfunz00_bglt) BgL_valz00_2997)))->
																BgL_argszd2typezd2);
															{
																obj_t BgL_lz00_3025;

																{	/* Cfa/arithmetic.scm 76 */
																	bool_t BgL_tmpz00_4828;

																	BgL_lz00_3025 = BgL_g1166z00_3023;
																BgL_zc3z04anonymousza31653ze3z87_3026:
																	if (PAIRP(BgL_lz00_3025))
																		{	/* Cfa/arithmetic.scm 77 */
																			{	/* Cfa/arithmetic.scm 79 */
																				obj_t BgL_tmpz00_4831;

																				BgL_tmpz00_4831 =
																					BGl_cleanupzd2typeze70z35zzcfa_arithmeticz00
																					(CAR(BgL_lz00_3025));
																				SET_CAR(BgL_lz00_3025, BgL_tmpz00_4831);
																			}
																			{
																				obj_t BgL_lz00_4835;

																				BgL_lz00_4835 = CDR(BgL_lz00_3025);
																				BgL_lz00_3025 = BgL_lz00_4835;
																				goto
																					BgL_zc3z04anonymousza31653ze3z87_3026;
																			}
																		}
																	else
																		{	/* Cfa/arithmetic.scm 77 */
																			BgL_tmpz00_4828 = ((bool_t) 0);
																		}
																	BBOOL(BgL_tmpz00_4828);
																}
															}
														}
													else
														{	/* Cfa/arithmetic.scm 75 */
															BFALSE;
														}
												}
										}
									}
								}
							}
						}
						{
							obj_t BgL_l1523z00_4838;

							BgL_l1523z00_4838 = CDR(BgL_l1523z00_2991);
							BgL_l1523z00_2991 = BgL_l1523z00_4838;
							goto BgL_zc3z04anonymousza31606ze3z87_2992;
						}
					}
				else
					{	/* Cfa/arithmetic.scm 63 */
						((bool_t) 1);
					}
			}
			{
				obj_t BgL_l1525z00_3036;

				BgL_l1525z00_3036 = BgL_globalsz00_3;
			BgL_zc3z04anonymousza31682ze3z87_3037:
				if (PAIRP(BgL_l1525z00_3036))
					{	/* Cfa/arithmetic.scm 83 */
						{	/* Cfa/arithmetic.scm 83 */
							obj_t BgL_arg1688z00_3039;

							BgL_arg1688z00_3039 = CAR(BgL_l1525z00_3036);
							{	/* Cfa/arithmetic.scm 90 */
								BgL_valuez00_bglt BgL_funz00_4040;

								BgL_funz00_4040 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt) BgL_arg1688z00_3039)))->
									BgL_valuez00);
								{	/* Cfa/arithmetic.scm 90 */
									obj_t BgL_bodyz00_4041;

									BgL_bodyz00_4041 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt) BgL_funz00_4040)))->BgL_bodyz00);
									{	/* Cfa/arithmetic.scm 91 */

										BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
											((BgL_nodez00_bglt) BgL_bodyz00_4041));
									}
								}
							}
						}
						{
							obj_t BgL_l1525z00_4849;

							BgL_l1525z00_4849 = CDR(BgL_l1525z00_3036);
							BgL_l1525z00_3036 = BgL_l1525z00_4849;
							goto BgL_zc3z04anonymousza31682ze3z87_3037;
						}
					}
				else
					{	/* Cfa/arithmetic.scm 83 */
						((bool_t) 1);
					}
			}
			return BgL_globalsz00_3;
		}

	}



/* cleanup-type~0 */
	obj_t BGl_cleanupzd2typeze70z35zzcfa_arithmeticz00(obj_t BgL_tz00_3042)
	{
		{	/* Cfa/arithmetic.scm 62 */
			{	/* Cfa/arithmetic.scm 55 */
				bool_t BgL_test2214z00_4851;

				{	/* Cfa/arithmetic.scm 55 */
					obj_t BgL_classz00_3879;

					BgL_classz00_3879 = BGl_typez00zztype_typez00;
					if (BGL_OBJECTP(BgL_tz00_3042))
						{	/* Cfa/arithmetic.scm 55 */
							BgL_objectz00_bglt BgL_arg1807z00_3881;

							BgL_arg1807z00_3881 = (BgL_objectz00_bglt) (BgL_tz00_3042);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/arithmetic.scm 55 */
									long BgL_idxz00_3887;

									BgL_idxz00_3887 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3881);
									BgL_test2214z00_4851 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_3887 + 1L)) == BgL_classz00_3879);
								}
							else
								{	/* Cfa/arithmetic.scm 55 */
									bool_t BgL_res2123z00_3912;

									{	/* Cfa/arithmetic.scm 55 */
										obj_t BgL_oclassz00_3895;

										{	/* Cfa/arithmetic.scm 55 */
											obj_t BgL_arg1815z00_3903;
											long BgL_arg1816z00_3904;

											BgL_arg1815z00_3903 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/arithmetic.scm 55 */
												long BgL_arg1817z00_3905;

												BgL_arg1817z00_3905 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3881);
												BgL_arg1816z00_3904 =
													(BgL_arg1817z00_3905 - OBJECT_TYPE);
											}
											BgL_oclassz00_3895 =
												VECTOR_REF(BgL_arg1815z00_3903, BgL_arg1816z00_3904);
										}
										{	/* Cfa/arithmetic.scm 55 */
											bool_t BgL__ortest_1115z00_3896;

											BgL__ortest_1115z00_3896 =
												(BgL_classz00_3879 == BgL_oclassz00_3895);
											if (BgL__ortest_1115z00_3896)
												{	/* Cfa/arithmetic.scm 55 */
													BgL_res2123z00_3912 = BgL__ortest_1115z00_3896;
												}
											else
												{	/* Cfa/arithmetic.scm 55 */
													long BgL_odepthz00_3897;

													{	/* Cfa/arithmetic.scm 55 */
														obj_t BgL_arg1804z00_3898;

														BgL_arg1804z00_3898 = (BgL_oclassz00_3895);
														BgL_odepthz00_3897 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_3898);
													}
													if ((1L < BgL_odepthz00_3897))
														{	/* Cfa/arithmetic.scm 55 */
															obj_t BgL_arg1802z00_3900;

															{	/* Cfa/arithmetic.scm 55 */
																obj_t BgL_arg1803z00_3901;

																BgL_arg1803z00_3901 = (BgL_oclassz00_3895);
																BgL_arg1802z00_3900 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3901,
																	1L);
															}
															BgL_res2123z00_3912 =
																(BgL_arg1802z00_3900 == BgL_classz00_3879);
														}
													else
														{	/* Cfa/arithmetic.scm 55 */
															BgL_res2123z00_3912 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2214z00_4851 = BgL_res2123z00_3912;
								}
						}
					else
						{	/* Cfa/arithmetic.scm 55 */
							BgL_test2214z00_4851 = ((bool_t) 0);
						}
				}
				if (BgL_test2214z00_4851)
					{	/* Cfa/arithmetic.scm 55 */
						if ((BgL_tz00_3042 == BGl_za2_za2z00zztype_cachez00))
							{	/* Cfa/arithmetic.scm 56 */
								return BGl_za2objza2z00zztype_cachez00;
							}
						else
							{	/* Cfa/arithmetic.scm 56 */
								return BgL_tz00_3042;
							}
					}
				else
					{	/* Cfa/arithmetic.scm 57 */
						bool_t BgL_test2220z00_4876;

						{	/* Cfa/arithmetic.scm 57 */
							obj_t BgL_classz00_3913;

							BgL_classz00_3913 = BGl_localz00zzast_varz00;
							if (BGL_OBJECTP(BgL_tz00_3042))
								{	/* Cfa/arithmetic.scm 57 */
									BgL_objectz00_bglt BgL_arg1807z00_3915;

									BgL_arg1807z00_3915 = (BgL_objectz00_bglt) (BgL_tz00_3042);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/arithmetic.scm 57 */
											long BgL_idxz00_3921;

											BgL_idxz00_3921 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3915);
											BgL_test2220z00_4876 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_3921 + 2L)) == BgL_classz00_3913);
										}
									else
										{	/* Cfa/arithmetic.scm 57 */
											bool_t BgL_res2124z00_3946;

											{	/* Cfa/arithmetic.scm 57 */
												obj_t BgL_oclassz00_3929;

												{	/* Cfa/arithmetic.scm 57 */
													obj_t BgL_arg1815z00_3937;
													long BgL_arg1816z00_3938;

													BgL_arg1815z00_3937 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/arithmetic.scm 57 */
														long BgL_arg1817z00_3939;

														BgL_arg1817z00_3939 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3915);
														BgL_arg1816z00_3938 =
															(BgL_arg1817z00_3939 - OBJECT_TYPE);
													}
													BgL_oclassz00_3929 =
														VECTOR_REF(BgL_arg1815z00_3937,
														BgL_arg1816z00_3938);
												}
												{	/* Cfa/arithmetic.scm 57 */
													bool_t BgL__ortest_1115z00_3930;

													BgL__ortest_1115z00_3930 =
														(BgL_classz00_3913 == BgL_oclassz00_3929);
													if (BgL__ortest_1115z00_3930)
														{	/* Cfa/arithmetic.scm 57 */
															BgL_res2124z00_3946 = BgL__ortest_1115z00_3930;
														}
													else
														{	/* Cfa/arithmetic.scm 57 */
															long BgL_odepthz00_3931;

															{	/* Cfa/arithmetic.scm 57 */
																obj_t BgL_arg1804z00_3932;

																BgL_arg1804z00_3932 = (BgL_oclassz00_3929);
																BgL_odepthz00_3931 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_3932);
															}
															if ((2L < BgL_odepthz00_3931))
																{	/* Cfa/arithmetic.scm 57 */
																	obj_t BgL_arg1802z00_3934;

																	{	/* Cfa/arithmetic.scm 57 */
																		obj_t BgL_arg1803z00_3935;

																		BgL_arg1803z00_3935 = (BgL_oclassz00_3929);
																		BgL_arg1802z00_3934 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_3935, 2L);
																	}
																	BgL_res2124z00_3946 =
																		(BgL_arg1802z00_3934 == BgL_classz00_3913);
																}
															else
																{	/* Cfa/arithmetic.scm 57 */
																	BgL_res2124z00_3946 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2220z00_4876 = BgL_res2124z00_3946;
										}
								}
							else
								{	/* Cfa/arithmetic.scm 57 */
									BgL_test2220z00_4876 = ((bool_t) 0);
								}
						}
						if (BgL_test2220z00_4876)
							{	/* Cfa/arithmetic.scm 57 */
								{	/* Cfa/arithmetic.scm 58 */
									bool_t BgL_test2225z00_4899;

									{	/* Cfa/arithmetic.scm 58 */
										BgL_typez00_bglt BgL_arg1701z00_3048;

										BgL_arg1701z00_3048 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_tz00_3042))))->
											BgL_typez00);
										BgL_test2225z00_4899 =
											(((obj_t) BgL_arg1701z00_3048) ==
											BGl_za2_za2z00zztype_cachez00);
									}
									if (BgL_test2225z00_4899)
										{	/* Cfa/arithmetic.scm 59 */
											BgL_typez00_bglt BgL_vz00_3949;

											BgL_vz00_3949 =
												((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt) BgL_tz00_3042))))->
													BgL_typez00) =
												((BgL_typez00_bglt) BgL_vz00_3949), BUNSPEC);
										}
									else
										{	/* Cfa/arithmetic.scm 58 */
											BFALSE;
										}
								}
								return BgL_tz00_3042;
							}
						else
							{	/* Cfa/arithmetic.scm 57 */
								return BgL_tz00_3042;
							}
					}
			}
		}

	}



/* &cleanup-arithmetic-nodes! */
	obj_t BGl_z62cleanupzd2arithmeticzd2nodesz12z70zzcfa_arithmeticz00(obj_t
		BgL_envz00_4448, obj_t BgL_globalsz00_4449)
	{
		{	/* Cfa/arithmetic.scm 52 */
			return
				BGl_cleanupzd2arithmeticzd2nodesz12z12zzcfa_arithmeticz00
				(BgL_globalsz00_4449);
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_arithmeticz00(void)
	{
		{	/* Cfa/arithmetic.scm 28 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_arithmeticz00(void)
	{
		{	/* Cfa/arithmetic.scm 28 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_proc2142z00zzcfa_arithmeticz00, BGl_nodez00zzast_nodez00,
				BGl_string2143z00zzcfa_arithmeticz00);
		}

	}



/* &cleanup-node!1559 */
	obj_t BGl_z62cleanupzd2nodez121559za2zzcfa_arithmeticz00(obj_t
		BgL_envz00_4451, obj_t BgL_nodez00_4452)
	{
		{	/* Cfa/arithmetic.scm 97 */
			return BUNSPEC;
		}

	}



/* cleanup-node! */
	obj_t BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(BgL_nodez00_bglt
		BgL_nodez00_5)
	{
		{	/* Cfa/arithmetic.scm 97 */
			{	/* Cfa/arithmetic.scm 97 */
				obj_t BgL_method1560z00_3056;

				{	/* Cfa/arithmetic.scm 97 */
					obj_t BgL_res2131z00_4079;

					{	/* Cfa/arithmetic.scm 97 */
						long BgL_objzd2classzd2numz00_4050;

						BgL_objzd2classzd2numz00_4050 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_5));
						{	/* Cfa/arithmetic.scm 97 */
							obj_t BgL_arg1811z00_4051;

							BgL_arg1811z00_4051 =
								PROCEDURE_REF(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
								(int) (1L));
							{	/* Cfa/arithmetic.scm 97 */
								int BgL_offsetz00_4054;

								BgL_offsetz00_4054 = (int) (BgL_objzd2classzd2numz00_4050);
								{	/* Cfa/arithmetic.scm 97 */
									long BgL_offsetz00_4055;

									BgL_offsetz00_4055 =
										((long) (BgL_offsetz00_4054) - OBJECT_TYPE);
									{	/* Cfa/arithmetic.scm 97 */
										long BgL_modz00_4056;

										BgL_modz00_4056 =
											(BgL_offsetz00_4055 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/arithmetic.scm 97 */
											long BgL_restz00_4058;

											BgL_restz00_4058 =
												(BgL_offsetz00_4055 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/arithmetic.scm 97 */

												{	/* Cfa/arithmetic.scm 97 */
													obj_t BgL_bucketz00_4060;

													BgL_bucketz00_4060 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_4051), BgL_modz00_4056);
													BgL_res2131z00_4079 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_4060), BgL_restz00_4058);
					}}}}}}}}
					BgL_method1560z00_3056 = BgL_res2131z00_4079;
				}
				return
					BGL_PROCEDURE_CALL1(BgL_method1560z00_3056, ((obj_t) BgL_nodez00_5));
			}
		}

	}



/* &cleanup-node! */
	obj_t BGl_z62cleanupzd2nodez12za2zzcfa_arithmeticz00(obj_t BgL_envz00_4453,
		obj_t BgL_nodez00_4454)
	{
		{	/* Cfa/arithmetic.scm 97 */
			return
				BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				((BgL_nodez00_bglt) BgL_nodez00_4454));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_arithmeticz00(void)
	{
		{	/* Cfa/arithmetic.scm 28 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_sequencez00zzast_nodez00, BGl_proc2144z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_syncz00zzast_nodez00, BGl_proc2146z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_setqz00zzast_nodez00, BGl_proc2147z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2148z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_failz00zzast_nodez00, BGl_proc2149z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_switchz00zzast_nodez00, BGl_proc2150z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2151z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2152z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2153z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2154z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2155z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2156z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2157z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_appzd2lyzd2zzast_nodez00, BGl_proc2158z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_funcallz00zzast_nodez00, BGl_proc2159z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cleanupzd2nodez12zd2envz12zzcfa_arithmeticz00,
				BGl_appz00zzast_nodez00, BGl_proc2160z00zzcfa_arithmeticz00,
				BGl_string2145z00zzcfa_arithmeticz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_nodezd2setupz12zd2envz12zzcfa_setupz00,
				BGl_prezd2arithmeticzd2appz00zzcfa_info2z00,
				BGl_proc2161z00zzcfa_arithmeticz00,
				BGl_string2162z00zzcfa_arithmeticz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_arithmeticzd2appzd2zzcfa_info2z00,
				BGl_proc2163z00zzcfa_arithmeticz00,
				BGl_string2164z00zzcfa_arithmeticz00);
		}

	}



/* &cfa!-arithmetic-app1596 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2arithmeticzd2app1596z70zzcfa_arithmeticz00(obj_t
		BgL_envz00_4473, obj_t BgL_nodez00_4474)
	{
		{	/* Cfa/arithmetic.scm 288 */
			{
				obj_t BgL_argszd2approxzd2_4547;
				obj_t BgL_argszd2approxzd2_4537;
				obj_t BgL_typez00_4538;
				obj_t BgL_speczd2typeszd2_4539;
				obj_t BgL_argszd2approxzd2_4530;
				obj_t BgL_speczd2typeszd2_4531;

				{	/* Cfa/arithmetic.scm 329 */
					obj_t BgL_argszd2approxzd2_4553;

					{	/* Cfa/arithmetic.scm 329 */
						obj_t BgL_l1551z00_4554;

						BgL_l1551z00_4554 =
							(((BgL_appz00_bglt) COBJECT(
									((BgL_appz00_bglt)
										((BgL_appz00_bglt) BgL_nodez00_4474))))->BgL_argsz00);
						if (NULLP(BgL_l1551z00_4554))
							{	/* Cfa/arithmetic.scm 329 */
								BgL_argszd2approxzd2_4553 = BNIL;
							}
						else
							{	/* Cfa/arithmetic.scm 329 */
								obj_t BgL_head1553z00_4555;

								{	/* Cfa/arithmetic.scm 329 */
									BgL_approxz00_bglt BgL_arg1925z00_4556;

									{	/* Cfa/arithmetic.scm 329 */
										obj_t BgL_arg1926z00_4557;

										BgL_arg1926z00_4557 = CAR(((obj_t) BgL_l1551z00_4554));
										BgL_arg1925z00_4556 =
											BGl_cfaz12z12zzcfa_cfaz00(
											((BgL_nodez00_bglt) BgL_arg1926z00_4557));
									}
									BgL_head1553z00_4555 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_arg1925z00_4556), BNIL);
								}
								{	/* Cfa/arithmetic.scm 329 */
									obj_t BgL_g1556z00_4558;

									BgL_g1556z00_4558 = CDR(((obj_t) BgL_l1551z00_4554));
									{
										obj_t BgL_l1551z00_4560;
										obj_t BgL_tail1554z00_4561;

										BgL_l1551z00_4560 = BgL_g1556z00_4558;
										BgL_tail1554z00_4561 = BgL_head1553z00_4555;
									BgL_zc3z04anonymousza31918ze3z87_4559:
										if (NULLP(BgL_l1551z00_4560))
											{	/* Cfa/arithmetic.scm 329 */
												BgL_argszd2approxzd2_4553 = BgL_head1553z00_4555;
											}
										else
											{	/* Cfa/arithmetic.scm 329 */
												obj_t BgL_newtail1555z00_4562;

												{	/* Cfa/arithmetic.scm 329 */
													BgL_approxz00_bglt BgL_arg1923z00_4563;

													{	/* Cfa/arithmetic.scm 329 */
														obj_t BgL_arg1924z00_4564;

														BgL_arg1924z00_4564 =
															CAR(((obj_t) BgL_l1551z00_4560));
														BgL_arg1923z00_4563 =
															BGl_cfaz12z12zzcfa_cfaz00(
															((BgL_nodez00_bglt) BgL_arg1924z00_4564));
													}
													BgL_newtail1555z00_4562 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_arg1923z00_4563), BNIL);
												}
												SET_CDR(BgL_tail1554z00_4561, BgL_newtail1555z00_4562);
												{	/* Cfa/arithmetic.scm 329 */
													obj_t BgL_arg1920z00_4565;

													BgL_arg1920z00_4565 =
														CDR(((obj_t) BgL_l1551z00_4560));
													{
														obj_t BgL_tail1554z00_4986;
														obj_t BgL_l1551z00_4985;

														BgL_l1551z00_4985 = BgL_arg1920z00_4565;
														BgL_tail1554z00_4986 = BgL_newtail1555z00_4562;
														BgL_tail1554z00_4561 = BgL_tail1554z00_4986;
														BgL_l1551z00_4560 = BgL_l1551z00_4985;
														goto BgL_zc3z04anonymousza31918ze3z87_4559;
													}
												}
											}
									}
								}
							}
					}
					{	/* Cfa/arithmetic.scm 331 */
						obj_t BgL_tyz00_4566;

						BgL_argszd2approxzd2_4547 = BgL_argszd2approxzd2_4553;
						{
							obj_t BgL_argsz00_4549;

							BgL_argsz00_4549 = BgL_argszd2approxzd2_4547;
						BgL_loopz00_4548:
							if (NULLP(BgL_argsz00_4549))
								{	/* Cfa/arithmetic.scm 297 */
									BgL_tyz00_4566 = BFALSE;
								}
							else
								{	/* Cfa/arithmetic.scm 299 */
									obj_t BgL_tz00_4550;

									{	/* Cfa/arithmetic.scm 299 */
										BgL_typez00_bglt BgL_arg1932z00_4551;

										BgL_arg1932z00_4551 =
											(((BgL_approxz00_bglt) COBJECT(
													((BgL_approxz00_bglt)
														CAR(((obj_t) BgL_argsz00_4549)))))->BgL_typez00);
										if (
											(((obj_t) BgL_arg1932z00_4551) ==
												BGl_za2intza2z00zztype_cachez00))
											{	/* Cfa/arithmetic.scm 291 */
												BgL_tz00_4550 = BGl_za2longza2z00zztype_cachez00;
											}
										else
											{	/* Cfa/arithmetic.scm 291 */
												BgL_tz00_4550 = ((obj_t) BgL_arg1932z00_4551);
											}
									}
									if ((BgL_tz00_4550 == BGl_za2_za2z00zztype_cachez00))
										{	/* Cfa/arithmetic.scm 301 */
											obj_t BgL_arg1931z00_4552;

											BgL_arg1931z00_4552 = CDR(((obj_t) BgL_argsz00_4549));
											{
												obj_t BgL_argsz00_5001;

												BgL_argsz00_5001 = BgL_arg1931z00_4552;
												BgL_argsz00_4549 = BgL_argsz00_5001;
												goto BgL_loopz00_4548;
											}
										}
									else
										{	/* Cfa/arithmetic.scm 300 */
											BgL_tyz00_4566 = BgL_tz00_4550;
										}
								}
						}
						{	/* Cfa/arithmetic.scm 337 */
							bool_t BgL_test2231z00_5002;

							{	/* Cfa/arithmetic.scm 337 */
								obj_t BgL_arg1916z00_4567;

								{
									BgL_arithmeticzd2appzd2_bglt BgL_auxz00_5003;

									{
										obj_t BgL_auxz00_5004;

										{	/* Cfa/arithmetic.scm 337 */
											BgL_objectz00_bglt BgL_tmpz00_5005;

											BgL_tmpz00_5005 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_4474));
											BgL_auxz00_5004 = BGL_OBJECT_WIDENING(BgL_tmpz00_5005);
										}
										BgL_auxz00_5003 =
											((BgL_arithmeticzd2appzd2_bglt) BgL_auxz00_5004);
									}
									BgL_arg1916z00_4567 =
										(((BgL_arithmeticzd2appzd2_bglt) COBJECT(BgL_auxz00_5003))->
										BgL_speczd2typeszd2);
								}
								BgL_argszd2approxzd2_4537 = BgL_argszd2approxzd2_4553;
								BgL_typez00_4538 = BgL_tyz00_4566;
								BgL_speczd2typeszd2_4539 = BgL_arg1916z00_4567;
								{	/* Cfa/arithmetic.scm 306 */
									bool_t BgL__ortest_1190z00_4540;

									if (CBOOL(BgL_typez00_4538))
										{	/* Cfa/arithmetic.scm 306 */
											BgL__ortest_1190z00_4540 = ((bool_t) 0);
										}
									else
										{	/* Cfa/arithmetic.scm 306 */
											BgL__ortest_1190z00_4540 = ((bool_t) 1);
										}
									if (BgL__ortest_1190z00_4540)
										{	/* Cfa/arithmetic.scm 306 */
											BgL_test2231z00_5002 = BgL__ortest_1190z00_4540;
										}
									else
										{	/* Cfa/arithmetic.scm 307 */
											obj_t BgL__andtest_1191z00_4541;

											BgL__andtest_1191z00_4541 =
												BGl_memqz00zz__r4_pairs_and_lists_6_3z00
												(BgL_typez00_4538, BgL_speczd2typeszd2_4539);
											if (CBOOL(BgL__andtest_1191z00_4541))
												{
													obj_t BgL_argsz00_4543;

													BgL_argsz00_4543 = BgL_argszd2approxzd2_4537;
												BgL_loopz00_4542:
													if (NULLP(BgL_argsz00_4543))
														{	/* Cfa/arithmetic.scm 309 */
															BgL_test2231z00_5002 = ((bool_t) 1);
														}
													else
														{	/* Cfa/arithmetic.scm 311 */
															obj_t BgL_tz00_4544;

															{	/* Cfa/arithmetic.scm 311 */
																BgL_typez00_bglt BgL_arg1939z00_4545;

																BgL_arg1939z00_4545 =
																	(((BgL_approxz00_bglt) COBJECT(
																			((BgL_approxz00_bglt)
																				CAR(
																					((obj_t) BgL_argsz00_4543)))))->
																	BgL_typez00);
																if ((((obj_t) BgL_arg1939z00_4545) ==
																		BGl_za2intza2z00zztype_cachez00))
																	{	/* Cfa/arithmetic.scm 291 */
																		BgL_tz00_4544 =
																			BGl_za2longza2z00zztype_cachez00;
																	}
																else
																	{	/* Cfa/arithmetic.scm 291 */
																		BgL_tz00_4544 =
																			((obj_t) BgL_arg1939z00_4545);
																	}
															}
															{	/* Cfa/arithmetic.scm 312 */
																bool_t BgL_test2237z00_5027;

																if ((BgL_tz00_4544 == BgL_typez00_4538))
																	{	/* Cfa/arithmetic.scm 312 */
																		BgL_test2237z00_5027 = ((bool_t) 1);
																	}
																else
																	{	/* Cfa/arithmetic.scm 312 */
																		BgL_test2237z00_5027 =
																			(BgL_tz00_4544 ==
																			BGl_za2_za2z00zztype_cachez00);
																	}
																if (BgL_test2237z00_5027)
																	{	/* Cfa/arithmetic.scm 313 */
																		obj_t BgL_arg1938z00_4546;

																		BgL_arg1938z00_4546 =
																			CDR(((obj_t) BgL_argsz00_4543));
																		{
																			obj_t BgL_argsz00_5033;

																			BgL_argsz00_5033 = BgL_arg1938z00_4546;
																			BgL_argsz00_4543 = BgL_argsz00_5033;
																			goto BgL_loopz00_4542;
																		}
																	}
																else
																	{	/* Cfa/arithmetic.scm 312 */
																		BgL_test2231z00_5002 = ((bool_t) 0);
																	}
															}
														}
												}
											else
												{	/* Cfa/arithmetic.scm 307 */
													BgL_test2231z00_5002 = ((bool_t) 0);
												}
										}
								}
							}
							if (BgL_test2231z00_5002)
								{	/* Cfa/arithmetic.scm 337 */
									{	/* Cfa/arithmetic.scm 340 */
										bool_t BgL_test2239z00_5034;

										{	/* Cfa/arithmetic.scm 340 */
											bool_t BgL_test2240z00_5035;

											{	/* Cfa/arithmetic.scm 340 */
												obj_t BgL_arg1897z00_4568;

												{
													BgL_arithmeticzd2appzd2_bglt BgL_auxz00_5036;

													{
														obj_t BgL_auxz00_5037;

														{	/* Cfa/arithmetic.scm 340 */
															BgL_objectz00_bglt BgL_tmpz00_5038;

															BgL_tmpz00_5038 =
																((BgL_objectz00_bglt)
																((BgL_appz00_bglt) BgL_nodez00_4474));
															BgL_auxz00_5037 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_5038);
														}
														BgL_auxz00_5036 =
															((BgL_arithmeticzd2appzd2_bglt) BgL_auxz00_5037);
													}
													BgL_arg1897z00_4568 =
														(((BgL_arithmeticzd2appzd2_bglt)
															COBJECT(BgL_auxz00_5036))->BgL_speczd2typeszd2);
												}
												BgL_argszd2approxzd2_4530 = BgL_argszd2approxzd2_4553;
												BgL_speczd2typeszd2_4531 = BgL_arg1897z00_4568;
												{
													obj_t BgL_argsz00_4533;

													BgL_argsz00_4533 = BgL_argszd2approxzd2_4530;
												BgL_loopz00_4532:
													if (NULLP(BgL_argsz00_4533))
														{	/* Cfa/arithmetic.scm 319 */
															BgL_test2240z00_5035 = ((bool_t) 1);
														}
													else
														{	/* Cfa/arithmetic.scm 321 */
															bool_t BgL_test2242z00_5046;

															{	/* Cfa/arithmetic.scm 321 */
																obj_t BgL_arg1949z00_4534;

																{	/* Cfa/arithmetic.scm 321 */
																	BgL_typez00_bglt BgL_arg1950z00_4535;

																	BgL_arg1950z00_4535 =
																		(((BgL_approxz00_bglt) COBJECT(
																				((BgL_approxz00_bglt)
																					CAR(
																						((obj_t) BgL_argsz00_4533)))))->
																		BgL_typez00);
																	if ((((obj_t) BgL_arg1950z00_4535) ==
																			BGl_za2intza2z00zztype_cachez00))
																		{	/* Cfa/arithmetic.scm 291 */
																			BgL_arg1949z00_4534 =
																				BGl_za2longza2z00zztype_cachez00;
																		}
																	else
																		{	/* Cfa/arithmetic.scm 291 */
																			BgL_arg1949z00_4534 =
																				((obj_t) BgL_arg1950z00_4535);
																		}
																}
																BgL_test2242z00_5046 =
																	CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00
																	(BgL_arg1949z00_4534,
																		BgL_speczd2typeszd2_4531));
															}
															if (BgL_test2242z00_5046)
																{	/* Cfa/arithmetic.scm 322 */
																	obj_t BgL_arg1948z00_4536;

																	BgL_arg1948z00_4536 =
																		CDR(((obj_t) BgL_argsz00_4533));
																	{
																		obj_t BgL_argsz00_5059;

																		BgL_argsz00_5059 = BgL_arg1948z00_4536;
																		BgL_argsz00_4533 = BgL_argsz00_5059;
																		goto BgL_loopz00_4532;
																	}
																}
															else
																{	/* Cfa/arithmetic.scm 321 */
																	BgL_test2240z00_5035 = ((bool_t) 0);
																}
														}
												}
											}
											if (BgL_test2240z00_5035)
												{	/* Cfa/arithmetic.scm 341 */
													BgL_typez00_bglt BgL_arg1894z00_4569;

													{	/* Cfa/arithmetic.scm 341 */
														BgL_approxz00_bglt BgL_arg1896z00_4570;

														{
															BgL_arithmeticzd2appzd2_bglt BgL_auxz00_5060;

															{
																obj_t BgL_auxz00_5061;

																{	/* Cfa/arithmetic.scm 341 */
																	BgL_objectz00_bglt BgL_tmpz00_5062;

																	BgL_tmpz00_5062 =
																		((BgL_objectz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_4474));
																	BgL_auxz00_5061 =
																		BGL_OBJECT_WIDENING(BgL_tmpz00_5062);
																}
																BgL_auxz00_5060 =
																	((BgL_arithmeticzd2appzd2_bglt)
																	BgL_auxz00_5061);
															}
															BgL_arg1896z00_4570 =
																(((BgL_arithmeticzd2appzd2_bglt)
																	COBJECT(BgL_auxz00_5060))->BgL_approxz00);
														}
														BgL_arg1894z00_4569 =
															(((BgL_approxz00_bglt)
																COBJECT(BgL_arg1896z00_4570))->BgL_typez00);
													}
													BgL_test2239z00_5034 =
														(
														((obj_t) BgL_arg1894z00_4569) ==
														BGl_za2_za2z00zztype_cachez00);
												}
											else
												{	/* Cfa/arithmetic.scm 340 */
													BgL_test2239z00_5034 = ((bool_t) 0);
												}
										}
										if (BgL_test2239z00_5034)
											{	/* Cfa/arithmetic.scm 340 */
												{	/* Cfa/arithmetic.scm 344 */
													BgL_approxz00_bglt BgL_arg1893z00_4571;

													{
														BgL_arithmeticzd2appzd2_bglt BgL_auxz00_5071;

														{
															obj_t BgL_auxz00_5072;

															{	/* Cfa/arithmetic.scm 344 */
																BgL_objectz00_bglt BgL_tmpz00_5073;

																BgL_tmpz00_5073 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt) BgL_nodez00_4474));
																BgL_auxz00_5072 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_5073);
															}
															BgL_auxz00_5071 =
																((BgL_arithmeticzd2appzd2_bglt)
																BgL_auxz00_5072);
														}
														BgL_arg1893z00_4571 =
															(((BgL_arithmeticzd2appzd2_bglt)
																COBJECT(BgL_auxz00_5071))->BgL_approxz00);
													}
													BGl_approxzd2setzd2typez12z12zzcfa_approxz00
														(BgL_arg1893z00_4571,
														((BgL_typez00_bglt) BgL_tyz00_4566));
												}
												BGl_continuezd2cfaz12zc0zzcfa_iteratez00(CNST_TABLE_REF
													(0));
											}
										else
											{	/* Cfa/arithmetic.scm 340 */
												BFALSE;
											}
									}
								}
							else
								{	/* Cfa/arithmetic.scm 346 */
									bool_t BgL_test2244z00_5083;

									if (
										((((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_globalz00_bglt)
																(((BgL_varz00_bglt) COBJECT(
																			(((BgL_appz00_bglt) COBJECT(
																						((BgL_appz00_bglt)
																							((BgL_appz00_bglt)
																								BgL_nodez00_4474))))->
																				BgL_funz00)))->BgL_variablez00)))))->
												BgL_idz00) == CNST_TABLE_REF(1)))
										{	/* Cfa/arithmetic.scm 346 */
											BgL_test2244z00_5083 = ((bool_t) 0);
										}
									else
										{	/* Cfa/arithmetic.scm 346 */
											BgL_test2244z00_5083 = ((bool_t) 1);
										}
									if (BgL_test2244z00_5083)
										{	/* Cfa/arithmetic.scm 346 */
											{
												obj_t BgL_l1557z00_4573;

												BgL_l1557z00_4573 = BgL_argszd2approxzd2_4553;
											BgL_zc3z04anonymousza31908ze3z87_4572:
												if (PAIRP(BgL_l1557z00_4573))
													{	/* Cfa/arithmetic.scm 347 */
														{	/* Cfa/arithmetic.scm 347 */
															obj_t BgL_az00_4574;

															BgL_az00_4574 = CAR(BgL_l1557z00_4573);
															BGl_loosez12z12zzcfa_loosez00(
																((BgL_approxz00_bglt) BgL_az00_4574),
																CNST_TABLE_REF(2));
														}
														{
															obj_t BgL_l1557z00_5100;

															BgL_l1557z00_5100 = CDR(BgL_l1557z00_4573);
															BgL_l1557z00_4573 = BgL_l1557z00_5100;
															goto BgL_zc3z04anonymousza31908ze3z87_4572;
														}
													}
												else
													{	/* Cfa/arithmetic.scm 347 */
														((bool_t) 1);
													}
											}
											{	/* Cfa/arithmetic.scm 348 */
												BgL_approxz00_bglt BgL_arg1911z00_4575;

												{
													BgL_arithmeticzd2appzd2_bglt BgL_auxz00_5102;

													{
														obj_t BgL_auxz00_5103;

														{	/* Cfa/arithmetic.scm 348 */
															BgL_objectz00_bglt BgL_tmpz00_5104;

															BgL_tmpz00_5104 =
																((BgL_objectz00_bglt)
																((BgL_appz00_bglt) BgL_nodez00_4474));
															BgL_auxz00_5103 =
																BGL_OBJECT_WIDENING(BgL_tmpz00_5104);
														}
														BgL_auxz00_5102 =
															((BgL_arithmeticzd2appzd2_bglt) BgL_auxz00_5103);
													}
													BgL_arg1911z00_4575 =
														(((BgL_arithmeticzd2appzd2_bglt)
															COBJECT(BgL_auxz00_5102))->BgL_approxz00);
												}
												BGl_approxzd2setzd2typez12z12zzcfa_approxz00
													(BgL_arg1911z00_4575,
													((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
											}
										}
									else
										{	/* Cfa/arithmetic.scm 346 */
											BFALSE;
										}
								}
						}
					}
				}
				{
					BgL_arithmeticzd2appzd2_bglt BgL_auxz00_5112;

					{
						obj_t BgL_auxz00_5113;

						{	/* Cfa/arithmetic.scm 351 */
							BgL_objectz00_bglt BgL_tmpz00_5114;

							BgL_tmpz00_5114 =
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4474));
							BgL_auxz00_5113 = BGL_OBJECT_WIDENING(BgL_tmpz00_5114);
						}
						BgL_auxz00_5112 = ((BgL_arithmeticzd2appzd2_bglt) BgL_auxz00_5113);
					}
					return
						(((BgL_arithmeticzd2appzd2_bglt) COBJECT(BgL_auxz00_5112))->
						BgL_approxz00);
				}
			}
		}

	}



/* &node-setup!-pre-arit1594 */
	obj_t BGl_z62nodezd2setupz12zd2prezd2arit1594za2zzcfa_arithmeticz00(obj_t
		BgL_envz00_4475, obj_t BgL_nodez00_4476)
	{
		{	/* Cfa/arithmetic.scm 245 */
			{	/* Cfa/arithmetic.scm 256 */
				BgL_variablez00_bglt BgL_fz00_4577;

				BgL_fz00_4577 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_4476))))->BgL_funz00)))->
					BgL_variablez00);
				{	/* Cfa/arithmetic.scm 256 */
					BgL_valuez00_bglt BgL_valz00_4578;

					BgL_valz00_4578 =
						(((BgL_variablez00_bglt) COBJECT(BgL_fz00_4577))->BgL_valuez00);
					{	/* Cfa/arithmetic.scm 257 */

						{	/* Cfa/arithmetic.scm 259 */
							bool_t BgL_test2247z00_5125;

							{	/* Cfa/arithmetic.scm 259 */
								BgL_typez00_bglt BgL_arg1851z00_4579;

								BgL_arg1851z00_4579 =
									(((BgL_variablez00_bglt) COBJECT(BgL_fz00_4577))->
									BgL_typez00);
								BgL_test2247z00_5125 =
									(((obj_t) BgL_arg1851z00_4579) ==
									BGl_za2objza2z00zztype_cachez00);
							}
							if (BgL_test2247z00_5125)
								{	/* Cfa/arithmetic.scm 259 */
									{	/* Cfa/arithmetic.scm 262 */
										BgL_typez00_bglt BgL_vz00_4580;

										BgL_vz00_4580 =
											((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00);
										((((BgL_variablez00_bglt) COBJECT(BgL_fz00_4577))->
												BgL_typez00) =
											((BgL_typez00_bglt) BgL_vz00_4580), BUNSPEC);
									}
									{	/* Cfa/arithmetic.scm 263 */
										BgL_typez00_bglt BgL_vz00_4581;

										BgL_vz00_4581 =
											((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00);
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt)
															((BgL_appz00_bglt) BgL_nodez00_4476))))->
												BgL_typez00) =
											((BgL_typez00_bglt) BgL_vz00_4581), BUNSPEC);
									}
								}
							else
								{	/* Cfa/arithmetic.scm 259 */
									BFALSE;
								}
						}
						{	/* Cfa/arithmetic.scm 265 */
							bool_t BgL_test2248z00_5135;

							{	/* Cfa/arithmetic.scm 265 */
								obj_t BgL_classz00_4582;

								BgL_classz00_4582 = BGl_sfunz00zzast_varz00;
								{	/* Cfa/arithmetic.scm 265 */
									BgL_objectz00_bglt BgL_arg1807z00_4583;

									{	/* Cfa/arithmetic.scm 265 */
										obj_t BgL_tmpz00_5136;

										BgL_tmpz00_5136 =
											((obj_t) ((BgL_objectz00_bglt) BgL_valz00_4578));
										BgL_arg1807z00_4583 =
											(BgL_objectz00_bglt) (BgL_tmpz00_5136);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/arithmetic.scm 265 */
											long BgL_idxz00_4584;

											BgL_idxz00_4584 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4583);
											BgL_test2248z00_5135 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4584 + 3L)) == BgL_classz00_4582);
										}
									else
										{	/* Cfa/arithmetic.scm 265 */
											bool_t BgL_res2134z00_4587;

											{	/* Cfa/arithmetic.scm 265 */
												obj_t BgL_oclassz00_4588;

												{	/* Cfa/arithmetic.scm 265 */
													obj_t BgL_arg1815z00_4589;
													long BgL_arg1816z00_4590;

													BgL_arg1815z00_4589 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/arithmetic.scm 265 */
														long BgL_arg1817z00_4591;

														BgL_arg1817z00_4591 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4583);
														BgL_arg1816z00_4590 =
															(BgL_arg1817z00_4591 - OBJECT_TYPE);
													}
													BgL_oclassz00_4588 =
														VECTOR_REF(BgL_arg1815z00_4589,
														BgL_arg1816z00_4590);
												}
												{	/* Cfa/arithmetic.scm 265 */
													bool_t BgL__ortest_1115z00_4592;

													BgL__ortest_1115z00_4592 =
														(BgL_classz00_4582 == BgL_oclassz00_4588);
													if (BgL__ortest_1115z00_4592)
														{	/* Cfa/arithmetic.scm 265 */
															BgL_res2134z00_4587 = BgL__ortest_1115z00_4592;
														}
													else
														{	/* Cfa/arithmetic.scm 265 */
															long BgL_odepthz00_4593;

															{	/* Cfa/arithmetic.scm 265 */
																obj_t BgL_arg1804z00_4594;

																BgL_arg1804z00_4594 = (BgL_oclassz00_4588);
																BgL_odepthz00_4593 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4594);
															}
															if ((3L < BgL_odepthz00_4593))
																{	/* Cfa/arithmetic.scm 265 */
																	obj_t BgL_arg1802z00_4595;

																	{	/* Cfa/arithmetic.scm 265 */
																		obj_t BgL_arg1803z00_4596;

																		BgL_arg1803z00_4596 = (BgL_oclassz00_4588);
																		BgL_arg1802z00_4595 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4596, 3L);
																	}
																	BgL_res2134z00_4587 =
																		(BgL_arg1802z00_4595 == BgL_classz00_4582);
																}
															else
																{	/* Cfa/arithmetic.scm 265 */
																	BgL_res2134z00_4587 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2248z00_5135 = BgL_res2134z00_4587;
										}
								}
							}
							if (BgL_test2248z00_5135)
								{	/* Cfa/arithmetic.scm 266 */
									obj_t BgL_arg1853z00_4597;

									{	/* Cfa/arithmetic.scm 266 */
										obj_t BgL_l1545z00_4598;

										BgL_l1545z00_4598 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_valz00_4578)))->BgL_argsz00);
										if (NULLP(BgL_l1545z00_4598))
											{	/* Cfa/arithmetic.scm 266 */
												BgL_arg1853z00_4597 = BNIL;
											}
										else
											{	/* Cfa/arithmetic.scm 266 */
												obj_t BgL_head1547z00_4599;

												{	/* Cfa/arithmetic.scm 266 */
													obj_t BgL_arg1860z00_4600;

													{	/* Cfa/arithmetic.scm 266 */
														obj_t BgL_arg1862z00_4601;

														BgL_arg1862z00_4601 =
															CAR(((obj_t) BgL_l1545z00_4598));
														BgL_arg1860z00_4600 =
															BGl_unspecifiedzd2typeze70z35zzcfa_arithmeticz00
															(BgL_arg1862z00_4601);
													}
													BgL_head1547z00_4599 =
														MAKE_YOUNG_PAIR(BgL_arg1860z00_4600, BNIL);
												}
												{	/* Cfa/arithmetic.scm 266 */
													obj_t BgL_g1550z00_4602;

													BgL_g1550z00_4602 = CDR(((obj_t) BgL_l1545z00_4598));
													{
														obj_t BgL_l1545z00_4604;
														obj_t BgL_tail1548z00_4605;

														BgL_l1545z00_4604 = BgL_g1550z00_4602;
														BgL_tail1548z00_4605 = BgL_head1547z00_4599;
													BgL_zc3z04anonymousza31855ze3z87_4603:
														if (NULLP(BgL_l1545z00_4604))
															{	/* Cfa/arithmetic.scm 266 */
																BgL_arg1853z00_4597 = BgL_head1547z00_4599;
															}
														else
															{	/* Cfa/arithmetic.scm 266 */
																obj_t BgL_newtail1549z00_4606;

																{	/* Cfa/arithmetic.scm 266 */
																	obj_t BgL_arg1858z00_4607;

																	{	/* Cfa/arithmetic.scm 266 */
																		obj_t BgL_arg1859z00_4608;

																		BgL_arg1859z00_4608 =
																			CAR(((obj_t) BgL_l1545z00_4604));
																		BgL_arg1858z00_4607 =
																			BGl_unspecifiedzd2typeze70z35zzcfa_arithmeticz00
																			(BgL_arg1859z00_4608);
																	}
																	BgL_newtail1549z00_4606 =
																		MAKE_YOUNG_PAIR(BgL_arg1858z00_4607, BNIL);
																}
																SET_CDR(BgL_tail1548z00_4605,
																	BgL_newtail1549z00_4606);
																{	/* Cfa/arithmetic.scm 266 */
																	obj_t BgL_arg1857z00_4609;

																	BgL_arg1857z00_4609 =
																		CDR(((obj_t) BgL_l1545z00_4604));
																	{
																		obj_t BgL_tail1548z00_5179;
																		obj_t BgL_l1545z00_5178;

																		BgL_l1545z00_5178 = BgL_arg1857z00_4609;
																		BgL_tail1548z00_5179 =
																			BgL_newtail1549z00_4606;
																		BgL_tail1548z00_4605 = BgL_tail1548z00_5179;
																		BgL_l1545z00_4604 = BgL_l1545z00_5178;
																		goto BgL_zc3z04anonymousza31855ze3z87_4603;
																	}
																}
															}
													}
												}
											}
									}
									((((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_valz00_4578)))->BgL_argsz00) =
										((obj_t) BgL_arg1853z00_4597), BUNSPEC);
								}
							else
								{	/* Cfa/arithmetic.scm 267 */
									bool_t BgL_test2254z00_5182;

									{	/* Cfa/arithmetic.scm 267 */
										obj_t BgL_classz00_4610;

										BgL_classz00_4610 = BGl_cfunz00zzast_varz00;
										{	/* Cfa/arithmetic.scm 267 */
											BgL_objectz00_bglt BgL_arg1807z00_4611;

											{	/* Cfa/arithmetic.scm 267 */
												obj_t BgL_tmpz00_5183;

												BgL_tmpz00_5183 =
													((obj_t) ((BgL_objectz00_bglt) BgL_valz00_4578));
												BgL_arg1807z00_4611 =
													(BgL_objectz00_bglt) (BgL_tmpz00_5183);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Cfa/arithmetic.scm 267 */
													long BgL_idxz00_4612;

													BgL_idxz00_4612 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4611);
													BgL_test2254z00_5182 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_4612 + 3L)) == BgL_classz00_4610);
												}
											else
												{	/* Cfa/arithmetic.scm 267 */
													bool_t BgL_res2135z00_4615;

													{	/* Cfa/arithmetic.scm 267 */
														obj_t BgL_oclassz00_4616;

														{	/* Cfa/arithmetic.scm 267 */
															obj_t BgL_arg1815z00_4617;
															long BgL_arg1816z00_4618;

															BgL_arg1815z00_4617 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Cfa/arithmetic.scm 267 */
																long BgL_arg1817z00_4619;

																BgL_arg1817z00_4619 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4611);
																BgL_arg1816z00_4618 =
																	(BgL_arg1817z00_4619 - OBJECT_TYPE);
															}
															BgL_oclassz00_4616 =
																VECTOR_REF(BgL_arg1815z00_4617,
																BgL_arg1816z00_4618);
														}
														{	/* Cfa/arithmetic.scm 267 */
															bool_t BgL__ortest_1115z00_4620;

															BgL__ortest_1115z00_4620 =
																(BgL_classz00_4610 == BgL_oclassz00_4616);
															if (BgL__ortest_1115z00_4620)
																{	/* Cfa/arithmetic.scm 267 */
																	BgL_res2135z00_4615 =
																		BgL__ortest_1115z00_4620;
																}
															else
																{	/* Cfa/arithmetic.scm 267 */
																	long BgL_odepthz00_4621;

																	{	/* Cfa/arithmetic.scm 267 */
																		obj_t BgL_arg1804z00_4622;

																		BgL_arg1804z00_4622 = (BgL_oclassz00_4616);
																		BgL_odepthz00_4621 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_4622);
																	}
																	if ((3L < BgL_odepthz00_4621))
																		{	/* Cfa/arithmetic.scm 267 */
																			obj_t BgL_arg1802z00_4623;

																			{	/* Cfa/arithmetic.scm 267 */
																				obj_t BgL_arg1803z00_4624;

																				BgL_arg1803z00_4624 =
																					(BgL_oclassz00_4616);
																				BgL_arg1802z00_4623 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_4624, 3L);
																			}
																			BgL_res2135z00_4615 =
																				(BgL_arg1802z00_4623 ==
																				BgL_classz00_4610);
																		}
																	else
																		{	/* Cfa/arithmetic.scm 267 */
																			BgL_res2135z00_4615 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2254z00_5182 = BgL_res2135z00_4615;
												}
										}
									}
									if (BgL_test2254z00_5182)
										{	/* Cfa/arithmetic.scm 268 */
											obj_t BgL_g1184z00_4625;

											BgL_g1184z00_4625 =
												(((BgL_cfunz00_bglt) COBJECT(
														((BgL_cfunz00_bglt) BgL_valz00_4578)))->
												BgL_argszd2typezd2);
											{
												obj_t BgL_lz00_4627;

												BgL_lz00_4627 = BgL_g1184z00_4625;
											BgL_loopz00_4626:
												if (PAIRP(BgL_lz00_4627))
													{	/* Cfa/arithmetic.scm 270 */
														obj_t BgL_tmpz00_5210;

														BgL_tmpz00_5210 =
															BGl_unspecifiedzd2typeze70z35zzcfa_arithmeticz00
															(CAR(BgL_lz00_4627));
														SET_CAR(BgL_lz00_4627, BgL_tmpz00_5210);
													}
												else
													{	/* Cfa/arithmetic.scm 271 */
														obj_t BgL_arg1869z00_4628;

														BgL_arg1869z00_4628 = CDR(((obj_t) BgL_lz00_4627));
														{
															obj_t BgL_lz00_5216;

															BgL_lz00_5216 = BgL_arg1869z00_4628;
															BgL_lz00_4627 = BgL_lz00_5216;
															goto BgL_loopz00_4626;
														}
													}
											}
										}
									else
										{	/* Cfa/arithmetic.scm 267 */
											BGl_errorz00zz__errorz00
												(BGl_string2162z00zzcfa_arithmeticz00,
												BGl_string2165z00zzcfa_arithmeticz00,
												((obj_t) ((BgL_appz00_bglt) BgL_nodez00_4476)));
										}
								}
						}
						BGl_za2arithmeticzd2nodesza2zd2zzcfa_arithmeticz00 =
							MAKE_YOUNG_PAIR(
							((obj_t)
								((BgL_appz00_bglt) BgL_nodez00_4476)),
							BGl_za2arithmeticzd2nodesza2zd2zzcfa_arithmeticz00);
						{	/* Cfa/arithmetic.scm 276 */
							obj_t BgL_arg1870z00_4629;

							BgL_arg1870z00_4629 =
								(((BgL_appz00_bglt) COBJECT(
										((BgL_appz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_4476))))->BgL_argsz00);
							BGl_nodezd2setupza2z12z62zzcfa_setupz00(BgL_arg1870z00_4629);
						}
						{	/* Cfa/arithmetic.scm 277 */
							obj_t BgL_speczd2typeszd2_4630;

							{
								BgL_prezd2arithmeticzd2appz00_bglt BgL_auxz00_5227;

								{
									obj_t BgL_auxz00_5228;

									{	/* Cfa/arithmetic.scm 277 */
										BgL_objectz00_bglt BgL_tmpz00_5229;

										BgL_tmpz00_5229 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_4476));
										BgL_auxz00_5228 = BGL_OBJECT_WIDENING(BgL_tmpz00_5229);
									}
									BgL_auxz00_5227 =
										((BgL_prezd2arithmeticzd2appz00_bglt) BgL_auxz00_5228);
								}
								BgL_speczd2typeszd2_4630 =
									(((BgL_prezd2arithmeticzd2appz00_bglt)
										COBJECT(BgL_auxz00_5227))->BgL_speczd2typeszd2);
							}
							{	/* Cfa/arithmetic.scm 277 */
								BgL_appz00_bglt BgL_nodez00_4631;

								{	/* Cfa/arithmetic.scm 278 */
									long BgL_arg1877z00_4632;

									{	/* Cfa/arithmetic.scm 278 */
										obj_t BgL_arg1878z00_4633;

										{	/* Cfa/arithmetic.scm 278 */
											obj_t BgL_arg1879z00_4634;

											{	/* Cfa/arithmetic.scm 278 */
												obj_t BgL_arg1815z00_4635;
												long BgL_arg1816z00_4636;

												BgL_arg1815z00_4635 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/arithmetic.scm 278 */
													long BgL_arg1817z00_4637;

													BgL_arg1817z00_4637 =
														BGL_OBJECT_CLASS_NUM(
														((BgL_objectz00_bglt)
															((BgL_appz00_bglt) BgL_nodez00_4476)));
													BgL_arg1816z00_4636 =
														(BgL_arg1817z00_4637 - OBJECT_TYPE);
												}
												BgL_arg1879z00_4634 =
													VECTOR_REF(BgL_arg1815z00_4635, BgL_arg1816z00_4636);
											}
											BgL_arg1878z00_4633 =
												BGl_classzd2superzd2zz__objectz00(BgL_arg1879z00_4634);
										}
										{	/* Cfa/arithmetic.scm 278 */
											obj_t BgL_tmpz00_5242;

											BgL_tmpz00_5242 = ((obj_t) BgL_arg1878z00_4633);
											BgL_arg1877z00_4632 = BGL_CLASS_NUM(BgL_tmpz00_5242);
									}}
									BGL_OBJECT_CLASS_NUM_SET(
										((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_4476)),
										BgL_arg1877z00_4632);
								}
								{	/* Cfa/arithmetic.scm 278 */
									BgL_objectz00_bglt BgL_tmpz00_5248;

									BgL_tmpz00_5248 =
										((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4476));
									BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5248, BFALSE);
								}
								((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4476));
								BgL_nodez00_4631 = ((BgL_appz00_bglt) BgL_nodez00_4476);
								{	/* Cfa/arithmetic.scm 278 */

									{	/* Cfa/arithmetic.scm 279 */
										BgL_arithmeticzd2appzd2_bglt BgL_wide1188z00_4638;

										BgL_wide1188z00_4638 =
											((BgL_arithmeticzd2appzd2_bglt)
											BOBJECT(GC_MALLOC(sizeof(struct
														BgL_arithmeticzd2appzd2_bgl))));
										{	/* Cfa/arithmetic.scm 279 */
											obj_t BgL_auxz00_5260;
											BgL_objectz00_bglt BgL_tmpz00_5256;

											BgL_auxz00_5260 = ((obj_t) BgL_wide1188z00_4638);
											BgL_tmpz00_5256 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt)
													((BgL_appz00_bglt) BgL_nodez00_4631)));
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_5256, BgL_auxz00_5260);
										}
										((BgL_objectz00_bglt)
											((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4631)));
										{	/* Cfa/arithmetic.scm 279 */
											long BgL_arg1872z00_4639;

											BgL_arg1872z00_4639 =
												BGL_CLASS_NUM(BGl_arithmeticzd2appzd2zzcfa_info2z00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt)
													((BgL_appz00_bglt)
														((BgL_appz00_bglt) BgL_nodez00_4631))),
												BgL_arg1872z00_4639);
										}
										((BgL_appz00_bglt)
											((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4631)));
									}
									{
										BgL_approxz00_bglt BgL_auxz00_5282;
										BgL_arithmeticzd2appzd2_bglt BgL_auxz00_5274;

										{	/* Cfa/arithmetic.scm 281 */
											bool_t BgL_test2259z00_5283;

											{	/* Cfa/arithmetic.scm 281 */
												BgL_typez00_bglt BgL_arg1876z00_4640;

												BgL_arg1876z00_4640 =
													(((BgL_variablez00_bglt) COBJECT(BgL_fz00_4577))->
													BgL_typez00);
												BgL_test2259z00_5283 =
													(((obj_t) BgL_arg1876z00_4640) ==
													BGl_za2_za2z00zztype_cachez00);
											}
											if (BgL_test2259z00_5283)
												{	/* Cfa/arithmetic.scm 281 */
													BgL_auxz00_5282 =
														BGl_makezd2emptyzd2approxz00zzcfa_approxz00();
												}
											else
												{	/* Cfa/arithmetic.scm 283 */
													BgL_typez00_bglt BgL_arg1875z00_4641;

													BgL_arg1875z00_4641 =
														(((BgL_variablez00_bglt) COBJECT(BgL_fz00_4577))->
														BgL_typez00);
													BgL_auxz00_5282 =
														BGl_makezd2typezd2approxz00zzcfa_approxz00
														(BgL_arg1875z00_4641);
												}
										}
										{
											obj_t BgL_auxz00_5275;

											{	/* Cfa/arithmetic.scm 281 */
												BgL_objectz00_bglt BgL_tmpz00_5276;

												BgL_tmpz00_5276 =
													((BgL_objectz00_bglt)
													((BgL_appz00_bglt)
														((BgL_appz00_bglt) BgL_nodez00_4631)));
												BgL_auxz00_5275 = BGL_OBJECT_WIDENING(BgL_tmpz00_5276);
											}
											BgL_auxz00_5274 =
												((BgL_arithmeticzd2appzd2_bglt) BgL_auxz00_5275);
										}
										((((BgL_arithmeticzd2appzd2_bglt)
													COBJECT(BgL_auxz00_5274))->BgL_approxz00) =
											((BgL_approxz00_bglt) BgL_auxz00_5282), BUNSPEC);
									}
									{
										BgL_arithmeticzd2appzd2_bglt BgL_auxz00_5291;

										{
											obj_t BgL_auxz00_5292;

											{	/* Cfa/arithmetic.scm 280 */
												BgL_objectz00_bglt BgL_tmpz00_5293;

												BgL_tmpz00_5293 =
													((BgL_objectz00_bglt)
													((BgL_appz00_bglt)
														((BgL_appz00_bglt) BgL_nodez00_4631)));
												BgL_auxz00_5292 = BGL_OBJECT_WIDENING(BgL_tmpz00_5293);
											}
											BgL_auxz00_5291 =
												((BgL_arithmeticzd2appzd2_bglt) BgL_auxz00_5292);
										}
										((((BgL_arithmeticzd2appzd2_bglt)
													COBJECT(BgL_auxz00_5291))->BgL_speczd2typeszd2) =
											((obj_t) BgL_speczd2typeszd2_4630), BUNSPEC);
									}
									return
										((obj_t)
										((BgL_appz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_4631)));
								}
							}
						}
					}
				}
			}
		}

	}



/* unspecified-type~0 */
	obj_t BGl_unspecifiedzd2typeze70z35zzcfa_arithmeticz00(obj_t BgL_lz00_3289)
	{
		{	/* Cfa/arithmetic.scm 254 */
			{	/* Cfa/arithmetic.scm 248 */
				bool_t BgL_test2260z00_5303;

				{	/* Cfa/arithmetic.scm 248 */
					obj_t BgL_classz00_4109;

					BgL_classz00_4109 = BGl_typez00zztype_typez00;
					if (BGL_OBJECTP(BgL_lz00_3289))
						{	/* Cfa/arithmetic.scm 248 */
							BgL_objectz00_bglt BgL_arg1807z00_4111;

							BgL_arg1807z00_4111 = (BgL_objectz00_bglt) (BgL_lz00_3289);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/arithmetic.scm 248 */
									long BgL_idxz00_4117;

									BgL_idxz00_4117 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4111);
									BgL_test2260z00_5303 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4117 + 1L)) == BgL_classz00_4109);
								}
							else
								{	/* Cfa/arithmetic.scm 248 */
									bool_t BgL_res2132z00_4142;

									{	/* Cfa/arithmetic.scm 248 */
										obj_t BgL_oclassz00_4125;

										{	/* Cfa/arithmetic.scm 248 */
											obj_t BgL_arg1815z00_4133;
											long BgL_arg1816z00_4134;

											BgL_arg1815z00_4133 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/arithmetic.scm 248 */
												long BgL_arg1817z00_4135;

												BgL_arg1817z00_4135 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4111);
												BgL_arg1816z00_4134 =
													(BgL_arg1817z00_4135 - OBJECT_TYPE);
											}
											BgL_oclassz00_4125 =
												VECTOR_REF(BgL_arg1815z00_4133, BgL_arg1816z00_4134);
										}
										{	/* Cfa/arithmetic.scm 248 */
											bool_t BgL__ortest_1115z00_4126;

											BgL__ortest_1115z00_4126 =
												(BgL_classz00_4109 == BgL_oclassz00_4125);
											if (BgL__ortest_1115z00_4126)
												{	/* Cfa/arithmetic.scm 248 */
													BgL_res2132z00_4142 = BgL__ortest_1115z00_4126;
												}
											else
												{	/* Cfa/arithmetic.scm 248 */
													long BgL_odepthz00_4127;

													{	/* Cfa/arithmetic.scm 248 */
														obj_t BgL_arg1804z00_4128;

														BgL_arg1804z00_4128 = (BgL_oclassz00_4125);
														BgL_odepthz00_4127 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4128);
													}
													if ((1L < BgL_odepthz00_4127))
														{	/* Cfa/arithmetic.scm 248 */
															obj_t BgL_arg1802z00_4130;

															{	/* Cfa/arithmetic.scm 248 */
																obj_t BgL_arg1803z00_4131;

																BgL_arg1803z00_4131 = (BgL_oclassz00_4125);
																BgL_arg1802z00_4130 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4131,
																	1L);
															}
															BgL_res2132z00_4142 =
																(BgL_arg1802z00_4130 == BgL_classz00_4109);
														}
													else
														{	/* Cfa/arithmetic.scm 248 */
															BgL_res2132z00_4142 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2260z00_5303 = BgL_res2132z00_4142;
								}
						}
					else
						{	/* Cfa/arithmetic.scm 248 */
							BgL_test2260z00_5303 = ((bool_t) 0);
						}
				}
				if (BgL_test2260z00_5303)
					{	/* Cfa/arithmetic.scm 248 */
						return BGl_za2_za2z00zztype_cachez00;
					}
				else
					{	/* Cfa/arithmetic.scm 250 */
						bool_t BgL_test2265z00_5326;

						{	/* Cfa/arithmetic.scm 250 */
							obj_t BgL_classz00_4143;

							BgL_classz00_4143 = BGl_localz00zzast_varz00;
							if (BGL_OBJECTP(BgL_lz00_3289))
								{	/* Cfa/arithmetic.scm 250 */
									BgL_objectz00_bglt BgL_arg1807z00_4145;

									BgL_arg1807z00_4145 = (BgL_objectz00_bglt) (BgL_lz00_3289);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/arithmetic.scm 250 */
											long BgL_idxz00_4151;

											BgL_idxz00_4151 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4145);
											BgL_test2265z00_5326 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4151 + 2L)) == BgL_classz00_4143);
										}
									else
										{	/* Cfa/arithmetic.scm 250 */
											bool_t BgL_res2133z00_4176;

											{	/* Cfa/arithmetic.scm 250 */
												obj_t BgL_oclassz00_4159;

												{	/* Cfa/arithmetic.scm 250 */
													obj_t BgL_arg1815z00_4167;
													long BgL_arg1816z00_4168;

													BgL_arg1815z00_4167 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/arithmetic.scm 250 */
														long BgL_arg1817z00_4169;

														BgL_arg1817z00_4169 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4145);
														BgL_arg1816z00_4168 =
															(BgL_arg1817z00_4169 - OBJECT_TYPE);
													}
													BgL_oclassz00_4159 =
														VECTOR_REF(BgL_arg1815z00_4167,
														BgL_arg1816z00_4168);
												}
												{	/* Cfa/arithmetic.scm 250 */
													bool_t BgL__ortest_1115z00_4160;

													BgL__ortest_1115z00_4160 =
														(BgL_classz00_4143 == BgL_oclassz00_4159);
													if (BgL__ortest_1115z00_4160)
														{	/* Cfa/arithmetic.scm 250 */
															BgL_res2133z00_4176 = BgL__ortest_1115z00_4160;
														}
													else
														{	/* Cfa/arithmetic.scm 250 */
															long BgL_odepthz00_4161;

															{	/* Cfa/arithmetic.scm 250 */
																obj_t BgL_arg1804z00_4162;

																BgL_arg1804z00_4162 = (BgL_oclassz00_4159);
																BgL_odepthz00_4161 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4162);
															}
															if ((2L < BgL_odepthz00_4161))
																{	/* Cfa/arithmetic.scm 250 */
																	obj_t BgL_arg1802z00_4164;

																	{	/* Cfa/arithmetic.scm 250 */
																		obj_t BgL_arg1803z00_4165;

																		BgL_arg1803z00_4165 = (BgL_oclassz00_4159);
																		BgL_arg1802z00_4164 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4165, 2L);
																	}
																	BgL_res2133z00_4176 =
																		(BgL_arg1802z00_4164 == BgL_classz00_4143);
																}
															else
																{	/* Cfa/arithmetic.scm 250 */
																	BgL_res2133z00_4176 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2265z00_5326 = BgL_res2133z00_4176;
										}
								}
							else
								{	/* Cfa/arithmetic.scm 250 */
									BgL_test2265z00_5326 = ((bool_t) 0);
								}
						}
						if (BgL_test2265z00_5326)
							{	/* Cfa/arithmetic.scm 250 */
								{	/* Cfa/arithmetic.scm 251 */
									BgL_typez00_bglt BgL_vz00_4178;

									BgL_vz00_4178 =
										((BgL_typez00_bglt) BGl_za2_za2z00zztype_cachez00);
									((((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_lz00_3289))))->
											BgL_typez00) =
										((BgL_typez00_bglt) BgL_vz00_4178), BUNSPEC);
								}
								return BgL_lz00_3289;
							}
						else
							{	/* Cfa/arithmetic.scm 250 */
								return BgL_lz00_3289;
							}
					}
			}
		}

	}



/* &cleanup-node!-app1592 */
	obj_t BGl_z62cleanupzd2nodez12zd2app1592z70zzcfa_arithmeticz00(obj_t
		BgL_envz00_4477, obj_t BgL_nodez00_4478)
	{
		{	/* Cfa/arithmetic.scm 232 */
			{	/* Cfa/arithmetic.scm 233 */
				bool_t BgL_tmpz00_5353;

				{	/* Cfa/arithmetic.scm 234 */
					BgL_varz00_bglt BgL_arg1844z00_4643;

					BgL_arg1844z00_4643 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_4478)))->BgL_funz00);
					BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
						((BgL_nodez00_bglt) BgL_arg1844z00_4643));
				}
				{	/* Cfa/arithmetic.scm 235 */
					obj_t BgL_g1544z00_4644;

					BgL_g1544z00_4644 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_4478)))->BgL_argsz00);
					{
						obj_t BgL_l1542z00_4646;

						BgL_l1542z00_4646 = BgL_g1544z00_4644;
					BgL_zc3z04anonymousza31845ze3z87_4645:
						if (PAIRP(BgL_l1542z00_4646))
							{	/* Cfa/arithmetic.scm 235 */
								{	/* Cfa/arithmetic.scm 235 */
									obj_t BgL_arg1847z00_4647;

									BgL_arg1847z00_4647 = CAR(BgL_l1542z00_4646);
									BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
										((BgL_nodez00_bglt) BgL_arg1847z00_4647));
								}
								{
									obj_t BgL_l1542z00_5365;

									BgL_l1542z00_5365 = CDR(BgL_l1542z00_4646);
									BgL_l1542z00_4646 = BgL_l1542z00_5365;
									goto BgL_zc3z04anonymousza31845ze3z87_4645;
								}
							}
						else
							{	/* Cfa/arithmetic.scm 235 */
								BgL_tmpz00_5353 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_5353);
			}
		}

	}



/* &cleanup-node!-funcal1590 */
	obj_t BGl_z62cleanupzd2nodez12zd2funcal1590z70zzcfa_arithmeticz00(obj_t
		BgL_envz00_4479, obj_t BgL_nodez00_4480)
	{
		{	/* Cfa/arithmetic.scm 224 */
			{	/* Cfa/arithmetic.scm 225 */
				bool_t BgL_tmpz00_5368;

				BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
					(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_4480)))->BgL_funz00));
				{	/* Cfa/arithmetic.scm 227 */
					obj_t BgL_g1541z00_4649;

					BgL_g1541z00_4649 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_4480)))->BgL_argsz00);
					{
						obj_t BgL_l1539z00_4651;

						BgL_l1539z00_4651 = BgL_g1541z00_4649;
					BgL_zc3z04anonymousza31840ze3z87_4650:
						if (PAIRP(BgL_l1539z00_4651))
							{	/* Cfa/arithmetic.scm 227 */
								{	/* Cfa/arithmetic.scm 227 */
									obj_t BgL_arg1842z00_4652;

									BgL_arg1842z00_4652 = CAR(BgL_l1539z00_4651);
									BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
										((BgL_nodez00_bglt) BgL_arg1842z00_4652));
								}
								{
									obj_t BgL_l1539z00_5379;

									BgL_l1539z00_5379 = CDR(BgL_l1539z00_4651);
									BgL_l1539z00_4651 = BgL_l1539z00_5379;
									goto BgL_zc3z04anonymousza31840ze3z87_4650;
								}
							}
						else
							{	/* Cfa/arithmetic.scm 227 */
								BgL_tmpz00_5368 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_5368);
			}
		}

	}



/* &cleanup-node!-app-ly1588 */
	obj_t BGl_z62cleanupzd2nodez12zd2appzd2ly1588za2zzcfa_arithmeticz00(obj_t
		BgL_envz00_4481, obj_t BgL_nodez00_4482)
	{
		{	/* Cfa/arithmetic.scm 216 */
			BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_4482)))->BgL_funz00));
			return
				BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_appzd2lyzd2_bglt) COBJECT(
							((BgL_appzd2lyzd2_bglt) BgL_nodez00_4482)))->BgL_argz00));
		}

	}



/* &cleanup-node!-box-se1586 */
	obj_t BGl_z62cleanupzd2nodez12zd2boxzd2se1586za2zzcfa_arithmeticz00(obj_t
		BgL_envz00_4483, obj_t BgL_nodez00_4484)
	{
		{	/* Cfa/arithmetic.scm 208 */
			{	/* Cfa/arithmetic.scm 210 */
				BgL_varz00_bglt BgL_arg1835z00_4655;

				BgL_arg1835z00_4655 =
					(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_4484)))->BgL_varz00);
				BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
					((BgL_nodez00_bglt) BgL_arg1835z00_4655));
			}
			return
				BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_boxzd2setz12zc0_bglt) COBJECT(
							((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_4484)))->BgL_valuez00));
		}

	}



/* &cleanup-node!-box-re1584 */
	obj_t BGl_z62cleanupzd2nodez12zd2boxzd2re1584za2zzcfa_arithmeticz00(obj_t
		BgL_envz00_4485, obj_t BgL_nodez00_4486)
	{
		{	/* Cfa/arithmetic.scm 201 */
			{	/* Cfa/arithmetic.scm 203 */
				BgL_varz00_bglt BgL_arg1834z00_4657;

				BgL_arg1834z00_4657 =
					(((BgL_boxzd2refzd2_bglt) COBJECT(
							((BgL_boxzd2refzd2_bglt) BgL_nodez00_4486)))->BgL_varz00);
				return
					BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
					((BgL_nodez00_bglt) BgL_arg1834z00_4657));
			}
		}

	}



/* &cleanup-node!-make-b1582 */
	obj_t BGl_z62cleanupzd2nodez12zd2makezd2b1582za2zzcfa_arithmeticz00(obj_t
		BgL_envz00_4487, obj_t BgL_nodez00_4488)
	{
		{	/* Cfa/arithmetic.scm 194 */
			return
				BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_makezd2boxzd2_bglt) COBJECT(
							((BgL_makezd2boxzd2_bglt) BgL_nodez00_4488)))->BgL_valuez00));
		}

	}



/* &cleanup-node!-jump-e1580 */
	obj_t BGl_z62cleanupzd2nodez12zd2jumpzd2e1580za2zzcfa_arithmeticz00(obj_t
		BgL_envz00_4489, obj_t BgL_nodez00_4490)
	{
		{	/* Cfa/arithmetic.scm 186 */
			BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_4490)))->BgL_exitz00));
			return
				BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
							((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_4490)))->BgL_valuez00));
		}

	}



/* &cleanup-node!-set-ex1578 */
	obj_t BGl_z62cleanupzd2nodez12zd2setzd2ex1578za2zzcfa_arithmeticz00(obj_t
		BgL_envz00_4491, obj_t BgL_nodez00_4492)
	{
		{	/* Cfa/arithmetic.scm 177 */
			{	/* Cfa/arithmetic.scm 179 */
				BgL_varz00_bglt BgL_arg1820z00_4661;

				BgL_arg1820z00_4661 =
					(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4492)))->BgL_varz00);
				BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
					((BgL_nodez00_bglt) BgL_arg1820z00_4661));
			}
			BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4492)))->BgL_onexitz00));
			return
				BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_setzd2exzd2itz00_bglt) COBJECT(
							((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_4492)))->BgL_bodyz00));
		}

	}



/* &cleanup-node!-let-va1576 */
	obj_t BGl_z62cleanupzd2nodez12zd2letzd2va1576za2zzcfa_arithmeticz00(obj_t
		BgL_envz00_4493, obj_t BgL_nodez00_4494)
	{
		{	/* Cfa/arithmetic.scm 167 */
			{	/* Cfa/arithmetic.scm 169 */
				obj_t BgL_g1538z00_4663;

				BgL_g1538z00_4663 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_4494)))->BgL_bindingsz00);
				{
					obj_t BgL_l1536z00_4665;

					BgL_l1536z00_4665 = BgL_g1538z00_4663;
				BgL_zc3z04anonymousza31772ze3z87_4664:
					if (PAIRP(BgL_l1536z00_4665))
						{	/* Cfa/arithmetic.scm 169 */
							{	/* Cfa/arithmetic.scm 169 */
								obj_t BgL_bz00_4666;

								BgL_bz00_4666 = CAR(BgL_l1536z00_4665);
								{	/* Cfa/arithmetic.scm 169 */
									obj_t BgL_arg1775z00_4667;

									BgL_arg1775z00_4667 = CDR(((obj_t) BgL_bz00_4666));
									BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
										((BgL_nodez00_bglt) BgL_arg1775z00_4667));
								}
							}
							{
								obj_t BgL_l1536z00_5427;

								BgL_l1536z00_5427 = CDR(BgL_l1536z00_4665);
								BgL_l1536z00_4665 = BgL_l1536z00_5427;
								goto BgL_zc3z04anonymousza31772ze3z87_4664;
							}
						}
					else
						{	/* Cfa/arithmetic.scm 169 */
							((bool_t) 1);
						}
				}
			}
			BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_4494)))->BgL_bodyz00));
			{	/* Cfa/arithmetic.scm 171 */
				bool_t BgL_test2273z00_5432;

				{	/* Cfa/arithmetic.scm 171 */
					BgL_typez00_bglt BgL_arg1812z00_4668;

					BgL_arg1812z00_4668 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2varzd2_bglt) BgL_nodez00_4494))))->BgL_typez00);
					BgL_test2273z00_5432 =
						(((obj_t) BgL_arg1812z00_4668) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test2273z00_5432)
					{	/* Cfa/arithmetic.scm 171 */
						return
							((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_letzd2varzd2_bglt) BgL_nodez00_4494))))->
								BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_nodez00_bglt)
										COBJECT((((BgL_letzd2varzd2_bglt)
													COBJECT(((BgL_letzd2varzd2_bglt) BgL_nodez00_4494)))->
												BgL_bodyz00)))->BgL_typez00)), BUNSPEC);
					}
				else
					{	/* Cfa/arithmetic.scm 171 */
						return BFALSE;
					}
			}
		}

	}



/* &cleanup-node!-let-fu1574 */
	obj_t BGl_z62cleanupzd2nodez12zd2letzd2fu1574za2zzcfa_arithmeticz00(obj_t
		BgL_envz00_4495, obj_t BgL_nodez00_4496)
	{
		{	/* Cfa/arithmetic.scm 157 */
			{	/* Cfa/arithmetic.scm 159 */
				obj_t BgL_g1535z00_4670;

				BgL_g1535z00_4670 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_4496)))->BgL_localsz00);
				{
					obj_t BgL_l1533z00_4672;

					BgL_l1533z00_4672 = BgL_g1535z00_4670;
				BgL_zc3z04anonymousza31756ze3z87_4671:
					if (PAIRP(BgL_l1533z00_4672))
						{	/* Cfa/arithmetic.scm 159 */
							{	/* Cfa/arithmetic.scm 159 */
								obj_t BgL_arg1761z00_4673;

								BgL_arg1761z00_4673 = CAR(BgL_l1533z00_4672);
								{	/* Cfa/arithmetic.scm 90 */
									BgL_valuez00_bglt BgL_funz00_4674;

									BgL_funz00_4674 =
										(((BgL_variablez00_bglt) COBJECT(
												((BgL_variablez00_bglt) BgL_arg1761z00_4673)))->
										BgL_valuez00);
									{	/* Cfa/arithmetic.scm 90 */
										obj_t BgL_bodyz00_4675;

										BgL_bodyz00_4675 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_4674)))->BgL_bodyz00);
										{	/* Cfa/arithmetic.scm 91 */

											BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
												((BgL_nodez00_bglt) BgL_bodyz00_4675));
										}
									}
								}
							}
							{
								obj_t BgL_l1533z00_5455;

								BgL_l1533z00_5455 = CDR(BgL_l1533z00_4672);
								BgL_l1533z00_4672 = BgL_l1533z00_5455;
								goto BgL_zc3z04anonymousza31756ze3z87_4671;
							}
						}
					else
						{	/* Cfa/arithmetic.scm 159 */
							((bool_t) 1);
						}
				}
			}
			BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_4496)))->BgL_bodyz00));
			{	/* Cfa/arithmetic.scm 161 */
				bool_t BgL_test2275z00_5460;

				{	/* Cfa/arithmetic.scm 161 */
					BgL_typez00_bglt BgL_arg1771z00_4676;

					BgL_arg1771z00_4676 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_letzd2funzd2_bglt) BgL_nodez00_4496))))->BgL_typez00);
					BgL_test2275z00_5460 =
						(((obj_t) BgL_arg1771z00_4676) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test2275z00_5460)
					{	/* Cfa/arithmetic.scm 161 */
						return
							((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_letzd2funzd2_bglt) BgL_nodez00_4496))))->
								BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_nodez00_bglt)
										COBJECT((((BgL_letzd2funzd2_bglt)
													COBJECT(((BgL_letzd2funzd2_bglt) BgL_nodez00_4496)))->
												BgL_bodyz00)))->BgL_typez00)), BUNSPEC);
					}
				else
					{	/* Cfa/arithmetic.scm 161 */
						return BFALSE;
					}
			}
		}

	}



/* &cleanup-node!-switch1572 */
	obj_t BGl_z62cleanupzd2nodez12zd2switch1572z70zzcfa_arithmeticz00(obj_t
		BgL_envz00_4497, obj_t BgL_nodez00_4498)
	{
		{	/* Cfa/arithmetic.scm 149 */
			{	/* Cfa/arithmetic.scm 150 */
				bool_t BgL_tmpz00_5472;

				BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
					(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_4498)))->BgL_testz00));
				{	/* Cfa/arithmetic.scm 152 */
					obj_t BgL_g1532z00_4678;

					BgL_g1532z00_4678 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_4498)))->BgL_clausesz00);
					{
						obj_t BgL_l1530z00_4680;

						BgL_l1530z00_4680 = BgL_g1532z00_4678;
					BgL_zc3z04anonymousza31752ze3z87_4679:
						if (PAIRP(BgL_l1530z00_4680))
							{	/* Cfa/arithmetic.scm 152 */
								{	/* Cfa/arithmetic.scm 152 */
									obj_t BgL_clausez00_4681;

									BgL_clausez00_4681 = CAR(BgL_l1530z00_4680);
									{	/* Cfa/arithmetic.scm 152 */
										obj_t BgL_arg1754z00_4682;

										BgL_arg1754z00_4682 = CDR(((obj_t) BgL_clausez00_4681));
										BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
											((BgL_nodez00_bglt) BgL_arg1754z00_4682));
									}
								}
								{
									obj_t BgL_l1530z00_5485;

									BgL_l1530z00_5485 = CDR(BgL_l1530z00_4680);
									BgL_l1530z00_4680 = BgL_l1530z00_5485;
									goto BgL_zc3z04anonymousza31752ze3z87_4679;
								}
							}
						else
							{	/* Cfa/arithmetic.scm 152 */
								BgL_tmpz00_5472 = ((bool_t) 1);
							}
					}
				}
				return BBOOL(BgL_tmpz00_5472);
			}
		}

	}



/* &cleanup-node!-fail1570 */
	obj_t BGl_z62cleanupzd2nodez12zd2fail1570z70zzcfa_arithmeticz00(obj_t
		BgL_envz00_4499, obj_t BgL_nodez00_4500)
	{
		{	/* Cfa/arithmetic.scm 140 */
			BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_4500)))->BgL_procz00));
			BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_4500)))->BgL_msgz00));
			return
				BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_failz00_bglt) COBJECT(
							((BgL_failz00_bglt) BgL_nodez00_4500)))->BgL_objz00));
		}

	}



/* &cleanup-node!-condit1568 */
	obj_t BGl_z62cleanupzd2nodez12zd2condit1568z70zzcfa_arithmeticz00(obj_t
		BgL_envz00_4501, obj_t BgL_nodez00_4502)
	{
		{	/* Cfa/arithmetic.scm 131 */
			BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_4502)))->BgL_testz00));
			BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_4502)))->BgL_truez00));
			return
				BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_conditionalz00_bglt) COBJECT(
							((BgL_conditionalz00_bglt) BgL_nodez00_4502)))->BgL_falsez00));
		}

	}



/* &cleanup-node!-setq1566 */
	obj_t BGl_z62cleanupzd2nodez12zd2setq1566z70zzcfa_arithmeticz00(obj_t
		BgL_envz00_4503, obj_t BgL_nodez00_4504)
	{
		{	/* Cfa/arithmetic.scm 124 */
			return
				BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_setqz00_bglt) COBJECT(
							((BgL_setqz00_bglt) BgL_nodez00_4504)))->BgL_valuez00));
		}

	}



/* &cleanup-node!-sync1564 */
	obj_t BGl_z62cleanupzd2nodez12zd2sync1564z70zzcfa_arithmeticz00(obj_t
		BgL_envz00_4505, obj_t BgL_nodez00_4506)
	{
		{	/* Cfa/arithmetic.scm 113 */
			BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_4506)))->BgL_mutexz00));
			BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_4506)))->BgL_prelockz00));
			BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
				(((BgL_syncz00_bglt) COBJECT(
							((BgL_syncz00_bglt) BgL_nodez00_4506)))->BgL_bodyz00));
			{	/* Cfa/arithmetic.scm 118 */
				bool_t BgL_test2277z00_5518;

				{	/* Cfa/arithmetic.scm 118 */
					BgL_typez00_bglt BgL_arg1738z00_4687;

					BgL_arg1738z00_4687 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_syncz00_bglt) BgL_nodez00_4506))))->BgL_typez00);
					BgL_test2277z00_5518 =
						(((obj_t) BgL_arg1738z00_4687) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test2277z00_5518)
					{	/* Cfa/arithmetic.scm 118 */
						return
							((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_syncz00_bglt) BgL_nodez00_4506))))->BgL_typez00) =
							((BgL_typez00_bglt) (((BgL_nodez00_bglt)
										COBJECT((((BgL_syncz00_bglt) COBJECT(((BgL_syncz00_bglt)
															BgL_nodez00_4506)))->BgL_bodyz00)))->
									BgL_typez00)), BUNSPEC);
					}
				else
					{	/* Cfa/arithmetic.scm 118 */
						return BFALSE;
					}
			}
		}

	}



/* &cleanup-node!-sequen1562 */
	obj_t BGl_z62cleanupzd2nodez12zd2sequen1562z70zzcfa_arithmeticz00(obj_t
		BgL_envz00_4507, obj_t BgL_nodez00_4508)
	{
		{	/* Cfa/arithmetic.scm 103 */
			if (NULLP(
					(((BgL_sequencez00_bglt) COBJECT(
								((BgL_sequencez00_bglt) BgL_nodez00_4508)))->BgL_nodesz00)))
				{	/* Cfa/arithmetic.scm 105 */
					return BFALSE;
				}
			else
				{	/* Cfa/arithmetic.scm 105 */
					{	/* Cfa/arithmetic.scm 106 */
						obj_t BgL_g1529z00_4689;

						BgL_g1529z00_4689 =
							(((BgL_sequencez00_bglt) COBJECT(
									((BgL_sequencez00_bglt) BgL_nodez00_4508)))->BgL_nodesz00);
						{
							obj_t BgL_l1527z00_4691;

							BgL_l1527z00_4691 = BgL_g1529z00_4689;
						BgL_zc3z04anonymousza31705ze3z87_4690:
							if (PAIRP(BgL_l1527z00_4691))
								{	/* Cfa/arithmetic.scm 106 */
									{	/* Cfa/arithmetic.scm 106 */
										obj_t BgL_arg1708z00_4692;

										BgL_arg1708z00_4692 = CAR(BgL_l1527z00_4691);
										BGl_cleanupzd2nodez12zc0zzcfa_arithmeticz00(
											((BgL_nodez00_bglt) BgL_arg1708z00_4692));
									}
									{
										obj_t BgL_l1527z00_5541;

										BgL_l1527z00_5541 = CDR(BgL_l1527z00_4691);
										BgL_l1527z00_4691 = BgL_l1527z00_5541;
										goto BgL_zc3z04anonymousza31705ze3z87_4690;
									}
								}
							else
								{	/* Cfa/arithmetic.scm 106 */
									((bool_t) 1);
								}
						}
					}
					{	/* Cfa/arithmetic.scm 107 */
						bool_t BgL_test2280z00_5543;

						{	/* Cfa/arithmetic.scm 107 */
							BgL_typez00_bglt BgL_arg1720z00_4693;

							BgL_arg1720z00_4693 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_sequencez00_bglt) BgL_nodez00_4508))))->
								BgL_typez00);
							BgL_test2280z00_5543 =
								(((obj_t) BgL_arg1720z00_4693) ==
								BGl_za2_za2z00zztype_cachez00);
						}
						if (BgL_test2280z00_5543)
							{	/* Cfa/arithmetic.scm 107 */
								return
									((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_sequencez00_bglt) BgL_nodez00_4508))))->
										BgL_typez00) =
									((BgL_typez00_bglt) (((BgL_nodez00_bglt)
												COBJECT(((BgL_nodez00_bglt)
														CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
																(((BgL_sequencez00_bglt)
																		COBJECT(((BgL_sequencez00_bglt)
																				BgL_nodez00_4508)))->
																	BgL_nodesz00))))))->BgL_typez00)), BUNSPEC);
							}
						else
							{	/* Cfa/arithmetic.scm 107 */
								return BFALSE;
							}
					}
				}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_arithmeticz00(void)
	{
		{	/* Cfa/arithmetic.scm 28 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2166z00zzcfa_arithmeticz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2166z00zzcfa_arithmeticz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2166z00zzcfa_arithmeticz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2166z00zzcfa_arithmeticz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2166z00zzcfa_arithmeticz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2166z00zzcfa_arithmeticz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2166z00zzcfa_arithmeticz00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string2166z00zzcfa_arithmeticz00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string2166z00zzcfa_arithmeticz00));
			BGl_modulezd2initializa7ationz75zzcfa_loosez00(471177480L,
				BSTRING_TO_STRING(BGl_string2166z00zzcfa_arithmeticz00));
			BGl_modulezd2initializa7ationz75zzcfa_iteratez00(131412063L,
				BSTRING_TO_STRING(BGl_string2166z00zzcfa_arithmeticz00));
			BGl_modulezd2initializa7ationz75zzcfa_cfaz00(400853817L,
				BSTRING_TO_STRING(BGl_string2166z00zzcfa_arithmeticz00));
			BGl_modulezd2initializa7ationz75zzcfa_setupz00(168272122L,
				BSTRING_TO_STRING(BGl_string2166z00zzcfa_arithmeticz00));
			BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string2166z00zzcfa_arithmeticz00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(324810626L,
				BSTRING_TO_STRING(BGl_string2166z00zzcfa_arithmeticz00));
		}

	}

#ifdef __cplusplus
}
#endif
