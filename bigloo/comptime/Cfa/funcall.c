/*===========================================================================*/
/*   (Cfa/funcall.scm)                                                       */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/funcall.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_FUNCALL_TYPE_DEFINITIONS
#define BGL_CFA_FUNCALL_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_funcallzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		struct BgL_approxz00_bgl *BgL_vazd2approxzd2;
		bool_t BgL_arityzd2errorzd2noticedzf3zf3;
		bool_t BgL_typezd2errorzd2noticedzf3zf3;
	}                         *BgL_funcallzf2cinfozf2_bglt;

	typedef struct BgL_makezd2procedurezd2appz00_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		obj_t BgL_valueszd2approxzd2;
		long BgL_lostzd2stampzd2;
		bool_t BgL_xzd2tzf3z21;
		bool_t BgL_xz00;
		bool_t BgL_tz00;
		struct BgL_variablez00_bgl *BgL_ownerz00;
		obj_t BgL_stackzd2stampzd2;
	}                                *BgL_makezd2procedurezd2appz00_bglt;


#endif													// BGL_CFA_FUNCALL_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_objectzd2initzd2zzcfa_funcallz00(void);
	extern obj_t BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00(obj_t,
		BgL_approxz00_bglt);
	static BgL_approxz00_bglt BGl_funcallz12z12zzcfa_funcallz00(BgL_appz00_bglt,
		obj_t, BgL_funcallz00_bglt);
	static obj_t BGl_methodzd2initzd2zzcfa_funcallz00(void);
	static bool_t
		BGl_optionalzd2correctzd2arityzf3zf3zzcfa_funcallz00(BgL_valuez00_bglt,
		obj_t);
	static bool_t
		BGl_keyzd2correctzd2arityzf3zf3zzcfa_funcallz00(BgL_valuez00_bglt, obj_t);
	extern obj_t
		BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00;
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	extern obj_t
		BGl_emptyzd2approxzd2alloczf3zf3zzcfa_approxz00(BgL_approxz00_bglt);
	extern BgL_approxz00_bglt
		BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_approxz00_bglt);
	BGL_IMPORT long bgl_list_length(obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_funcallz00(void);
	static obj_t BGl_z62zc3z04anonymousza31723ze3ze5zzcfa_funcallz00(obj_t,
		obj_t);
	extern BgL_approxz00_bglt BGl_makezd2emptyzd2approxz00zzcfa_approxz00(void);
	static obj_t BGl_z62zc3z04anonymousza31740ze3ze5zzcfa_funcallz00(obj_t,
		obj_t);
	extern BgL_approxz00_bglt BGl_appz12z12zzcfa_appz00(BgL_funz00_bglt,
		BgL_varz00_bglt, obj_t);
	extern obj_t BGl_userzd2warningzf2locationz20zztools_errorz00(obj_t, obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_makezd2procedurezd2appz00zzcfa_info2z00;
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	static BgL_approxz00_bglt
		BGl_z62cfaz12zd2funcallzf2Cinfo1526z50zzcfa_funcallz00(obj_t, obj_t);
	static BgL_approxz00_bglt
		BGl_funcallzd2arityzd2errorz00zzcfa_funcallz00(BgL_funcallz00_bglt,
		BgL_variablez00_bglt, long, obj_t);
	extern obj_t BGl_funcallzf2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_approxzd2setzd2topz12z12zzcfa_approxz00(BgL_approxz00_bglt);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_funcallz00 = BUNSPEC;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	static obj_t BGl_toplevelzd2initzd2zzcfa_funcallz00(void);
	static obj_t BGl_genericzd2initzd2zzcfa_funcallz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	BGL_IMPORT obj_t bgl_reverse_bang(obj_t);
	extern BgL_approxz00_bglt BGl_loosez12z12zzcfa_loosez00(BgL_approxz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static bool_t
		BGl_cfazd2correctzd2arityzf3zf3zzcfa_funcallz00(BgL_valuez00_bglt, obj_t);
	extern bool_t BGl_globalzd2optionalzf3z21zzast_varz00(obj_t);
	extern obj_t BGl_za2procedureza2z00zztype_cachez00;
	extern bool_t BGl_soundzd2arityzf3z21zztools_argsz00(obj_t, obj_t);
	extern bool_t BGl_globalzd2keyzf3z21zzast_varz00(obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_funcallz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_procedurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_appz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_loosez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_cfaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_argsz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_stringzd2appendzd2zz__r4_strings_6_7z00(obj_t);
	BGL_IMPORT obj_t BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(long,
		long);
	extern obj_t BGl_localz00zzast_varz00;
	static obj_t BGl_cnstzd2initzd2zzcfa_funcallz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_funcallz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_funcallz00(void);
	extern BgL_approxz00_bglt
		BGl_makezd2typezd2approxz00zzcfa_approxz00(BgL_typez00_bglt);
	static obj_t
		BGl_funcallzd2typezd2errorz00zzcfa_funcallz00(BgL_funcallz00_bglt,
		BgL_typez00_bglt);
	extern obj_t
		BGl_setzd2procedurezd2approxzd2polymorphicz12zc0zzcfa_procedurez00
		(BgL_appz00_bglt);
	extern BgL_approxz00_bglt BGl_cfaz12z12zzcfa_cfaz00(BgL_nodez00_bglt);
	extern obj_t BGl_approxzd2setzd2typez12z12zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t BGl_z62zc3z04anonymousza31771ze3ze5zzcfa_funcallz00(obj_t,
		obj_t);
	static obj_t __cnst[1];


	extern obj_t BGl_cfaz12zd2envzc0zzcfa_cfaz00;
	   
		 
		DEFINE_STRING(BGl_string2016z00zzcfa_funcallz00,
		BgL_bgl_string2016za700za7za7c2027za7, "cfa", 3);
	      DEFINE_STRING(BGl_string2017z00zzcfa_funcallz00,
		BgL_bgl_string2017za700za7za7c2028za7, "Possible funcall type error", 27);
	      DEFINE_STRING(BGl_string2018z00zzcfa_funcallz00,
		BgL_bgl_string2018za700za7za7c2029za7, " provided", 9);
	      DEFINE_STRING(BGl_string2019z00zzcfa_funcallz00,
		BgL_bgl_string2019za700za7za7c2030za7, " arg(s) expected, ", 18);
	      DEFINE_STRING(BGl_string2020z00zzcfa_funcallz00,
		BgL_bgl_string2020za700za7za7c2031za7, "Possible funcall arity error", 28);
	      DEFINE_STRING(BGl_string2022z00zzcfa_funcallz00,
		BgL_bgl_string2022za700za7za7c2032za7, "cfa!", 4);
	      DEFINE_STRING(BGl_string2024z00zzcfa_funcallz00,
		BgL_bgl_string2024za700za7za7c2033za7, "cfa_funcall", 11);
	      DEFINE_STRING(BGl_string2025z00zzcfa_funcallz00,
		BgL_bgl_string2025za700za7za7c2034za7, "all ", 4);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2021z00zzcfa_funcallz00,
		BgL_bgl_za762cfaza712za7d2func2035za7,
		BGl_z62cfaz12zd2funcallzf2Cinfo1526z50zzcfa_funcallz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2023z00zzcfa_funcallz00,
		BgL_bgl_za762za7c3za704anonymo2036za7,
		BGl_z62zc3z04anonymousza31771ze3ze5zzcfa_funcallz00, 0L, BUNSPEC, 1);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		   
			 ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_funcallz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_funcallz00(long
		BgL_checksumz00_4136, char *BgL_fromz00_4137)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_funcallz00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_funcallz00 =
						BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_funcallz00();
					BGl_libraryzd2moduleszd2initz00zzcfa_funcallz00();
					BGl_cnstzd2initzd2zzcfa_funcallz00();
					BGl_importedzd2moduleszd2initz00zzcfa_funcallz00();
					BGl_methodzd2initzd2zzcfa_funcallz00();
					return BGl_toplevelzd2initzd2zzcfa_funcallz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_funcallz00(void)
	{
		{	/* Cfa/funcall.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_funcall");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_funcall");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L,
				"cfa_funcall");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_funcall");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_funcallz00(void)
	{
		{	/* Cfa/funcall.scm 15 */
			{	/* Cfa/funcall.scm 15 */
				obj_t BgL_cportz00_4040;

				{	/* Cfa/funcall.scm 15 */
					obj_t BgL_stringz00_4047;

					BgL_stringz00_4047 = BGl_string2025z00zzcfa_funcallz00;
					{	/* Cfa/funcall.scm 15 */
						obj_t BgL_startz00_4048;

						BgL_startz00_4048 = BINT(0L);
						{	/* Cfa/funcall.scm 15 */
							obj_t BgL_endz00_4049;

							BgL_endz00_4049 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_4047)));
							{	/* Cfa/funcall.scm 15 */

								BgL_cportz00_4040 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_4047, BgL_startz00_4048, BgL_endz00_4049);
				}}}}
				{
					long BgL_iz00_4041;

					BgL_iz00_4041 = 0L;
				BgL_loopz00_4042:
					if ((BgL_iz00_4041 == -1L))
						{	/* Cfa/funcall.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/funcall.scm 15 */
							{	/* Cfa/funcall.scm 15 */
								obj_t BgL_arg2026z00_4043;

								{	/* Cfa/funcall.scm 15 */

									{	/* Cfa/funcall.scm 15 */
										obj_t BgL_locationz00_4045;

										BgL_locationz00_4045 = BBOOL(((bool_t) 0));
										{	/* Cfa/funcall.scm 15 */

											BgL_arg2026z00_4043 =
												BGl_readz00zz__readerz00(BgL_cportz00_4040,
												BgL_locationz00_4045);
										}
									}
								}
								{	/* Cfa/funcall.scm 15 */
									int BgL_tmpz00_4164;

									BgL_tmpz00_4164 = (int) (BgL_iz00_4041);
									CNST_TABLE_SET(BgL_tmpz00_4164, BgL_arg2026z00_4043);
							}}
							{	/* Cfa/funcall.scm 15 */
								int BgL_auxz00_4046;

								BgL_auxz00_4046 = (int) ((BgL_iz00_4041 - 1L));
								{
									long BgL_iz00_4169;

									BgL_iz00_4169 = (long) (BgL_auxz00_4046);
									BgL_iz00_4041 = BgL_iz00_4169;
									goto BgL_loopz00_4042;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_funcallz00(void)
	{
		{	/* Cfa/funcall.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_funcallz00(void)
	{
		{	/* Cfa/funcall.scm 15 */
			return BUNSPEC;
		}

	}



/* optional-correct-arity? */
	bool_t BGl_optionalzd2correctzd2arityzf3zf3zzcfa_funcallz00(BgL_valuez00_bglt
		BgL_funz00_4, obj_t BgL_argszd2approxzd2_5)
	{
		{	/* Cfa/funcall.scm 111 */
			{	/* Cfa/funcall.scm 113 */
				long BgL_miz00_2984;

				BgL_miz00_2984 =
					(1L +
					(((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_funz00_4)))->BgL_arityz00));
				{	/* Cfa/funcall.scm 114 */
					long BgL_lz00_2986;

					BgL_lz00_2986 = bgl_list_length(BgL_argszd2approxzd2_5);
					{	/* Cfa/funcall.scm 115 */

						if ((BgL_lz00_2986 >= BgL_miz00_2984))
							{	/* Cfa/funcall.scm 116 */
								return
									(BgL_lz00_2986 <=
									(BgL_miz00_2984 +
										bgl_list_length(
											(((BgL_sfunz00_bglt) COBJECT(
														((BgL_sfunz00_bglt) BgL_funz00_4)))->
												BgL_optionalsz00))));
							}
						else
							{	/* Cfa/funcall.scm 116 */
								return ((bool_t) 0);
							}
					}
				}
			}
		}

	}



/* key-correct-arity? */
	bool_t BGl_keyzd2correctzd2arityzf3zf3zzcfa_funcallz00(BgL_valuez00_bglt
		BgL_funz00_6, obj_t BgL_argszd2approxzd2_7)
	{
		{	/* Cfa/funcall.scm 121 */
			{	/* Cfa/funcall.scm 123 */
				long BgL_miz00_2991;

				BgL_miz00_2991 =
					(1L +
					(((BgL_funz00_bglt) COBJECT(
								((BgL_funz00_bglt) BgL_funz00_6)))->BgL_arityz00));
				{	/* Cfa/funcall.scm 124 */
					long BgL_lz00_2993;

					BgL_lz00_2993 = bgl_list_length(BgL_argszd2approxzd2_7);
					{	/* Cfa/funcall.scm 125 */

						if ((BgL_lz00_2993 >= BgL_miz00_2991))
							{	/* Cfa/funcall.scm 126 */
								return
									(BgL_lz00_2993 <=
									(BgL_miz00_2991 +
										(2L *
											bgl_list_length(
												(((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_funz00_6)))->
													BgL_keysz00)))));
							}
						else
							{	/* Cfa/funcall.scm 126 */
								return ((bool_t) 0);
							}
					}
				}
			}
		}

	}



/* cfa-correct-arity? */
	bool_t BGl_cfazd2correctzd2arityzf3zf3zzcfa_funcallz00(BgL_valuez00_bglt
		BgL_funz00_8, obj_t BgL_argszd2approxzd2_9)
	{
		{	/* Cfa/funcall.scm 131 */
			{	/* Cfa/funcall.scm 132 */
				obj_t BgL_gz00_2999;

				BgL_gz00_2999 =
					(((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt) BgL_funz00_8)))->
					BgL_thezd2closurezd2globalz00);
				if (BGl_globalzd2optionalzf3z21zzast_varz00(BgL_gz00_2999))
					{	/* Cfa/funcall.scm 135 */
						BgL_valuez00_bglt BgL_arg1571z00_3001;

						BgL_arg1571z00_3001 =
							(((BgL_variablez00_bglt) COBJECT(
									((BgL_variablez00_bglt)
										((BgL_globalz00_bglt) BgL_gz00_2999))))->BgL_valuez00);
						BGL_TAIL return
							BGl_optionalzd2correctzd2arityzf3zf3zzcfa_funcallz00
							(BgL_arg1571z00_3001, BgL_argszd2approxzd2_9);
					}
				else
					{	/* Cfa/funcall.scm 134 */
						if (BGl_globalzd2keyzf3z21zzast_varz00(BgL_gz00_2999))
							{	/* Cfa/funcall.scm 137 */
								BgL_valuez00_bglt BgL_arg1575z00_3003;

								BgL_arg1575z00_3003 =
									(((BgL_variablez00_bglt) COBJECT(
											((BgL_variablez00_bglt)
												((BgL_globalz00_bglt) BgL_gz00_2999))))->BgL_valuez00);
								BGL_TAIL return
									BGl_keyzd2correctzd2arityzf3zf3zzcfa_funcallz00
									(BgL_arg1575z00_3003, BgL_argszd2approxzd2_9);
							}
						else
							{	/* Cfa/funcall.scm 139 */
								long BgL_arg1576z00_3004;

								BgL_arg1576z00_3004 =
									(((BgL_funz00_bglt) COBJECT(
											((BgL_funz00_bglt) BgL_funz00_8)))->BgL_arityz00);
								return
									BGl_soundzd2arityzf3z21zztools_argsz00(BINT
									(BgL_arg1576z00_3004), BgL_argszd2approxzd2_9);
							}
					}
			}
		}

	}



/* funcall! */
	BgL_approxz00_bglt BGl_funcallz12z12zzcfa_funcallz00(BgL_appz00_bglt
		BgL_allocz00_10, obj_t BgL_argszd2approxzd2_11,
		BgL_funcallz00_bglt BgL_nodez00_12)
	{
		{	/* Cfa/funcall.scm 144 */
			{	/* Cfa/funcall.scm 145 */
				obj_t BgL_calleez00_3005;

				{	/* Cfa/funcall.scm 145 */
					obj_t BgL_pairz00_3686;

					BgL_pairz00_3686 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_allocz00_10)))->BgL_argsz00);
					BgL_calleez00_3005 = CAR(BgL_pairz00_3686);
				}
				{	/* Cfa/funcall.scm 145 */
					BgL_variablez00_bglt BgL_vz00_3006;

					BgL_vz00_3006 =
						(((BgL_varz00_bglt) COBJECT(
								((BgL_varz00_bglt) BgL_calleez00_3005)))->BgL_variablez00);
					{	/* Cfa/funcall.scm 146 */
						BgL_valuez00_bglt BgL_funz00_3007;

						BgL_funz00_3007 =
							(((BgL_variablez00_bglt) COBJECT(BgL_vz00_3006))->BgL_valuez00);
						{	/* Cfa/funcall.scm 147 */
							long BgL_arityz00_3008;

							BgL_arityz00_3008 =
								(((BgL_funz00_bglt) COBJECT(
										((BgL_funz00_bglt) BgL_funz00_3007)))->BgL_arityz00);
							{	/* Cfa/funcall.scm 148 */

								if (BGl_cfazd2correctzd2arityzf3zf3zzcfa_funcallz00
									(BgL_funz00_3007, BgL_argszd2approxzd2_11))
									{	/* Cfa/funcall.scm 159 */
										bool_t BgL_test2044z00_4223;

										{	/* Cfa/funcall.scm 159 */
											bool_t BgL_test2045z00_4224;

											{	/* Cfa/funcall.scm 159 */
												obj_t BgL_arg1626z00_3047;

												BgL_arg1626z00_3047 =
													(((BgL_sfunz00_bglt) COBJECT(
															((BgL_sfunz00_bglt) BgL_funz00_3007)))->
													BgL_thezd2closurezd2globalz00);
												BgL_test2045z00_4224 =
													BGl_globalzd2optionalzf3z21zzast_varz00
													(BgL_arg1626z00_3047);
											}
											if (BgL_test2045z00_4224)
												{	/* Cfa/funcall.scm 159 */
													BgL_test2044z00_4223 = ((bool_t) 1);
												}
											else
												{	/* Cfa/funcall.scm 160 */
													obj_t BgL_arg1625z00_3046;

													BgL_arg1625z00_3046 =
														(((BgL_sfunz00_bglt) COBJECT(
																((BgL_sfunz00_bglt) BgL_funz00_3007)))->
														BgL_thezd2closurezd2globalz00);
													BgL_test2044z00_4223 =
														BGl_globalzd2keyzf3z21zzast_varz00
														(BgL_arg1625z00_3046);
												}
										}
										if (BgL_test2044z00_4223)
											{	/* Cfa/funcall.scm 159 */
												{
													obj_t BgL_l1521z00_3016;

													BgL_l1521z00_3016 = BgL_argszd2approxzd2_11;
												BgL_zc3z04anonymousza31593ze3z87_3017:
													if (PAIRP(BgL_l1521z00_3016))
														{	/* Cfa/funcall.scm 163 */
															{	/* Cfa/funcall.scm 163 */
																obj_t BgL_approxz00_3019;

																BgL_approxz00_3019 = CAR(BgL_l1521z00_3016);
																BGl_loosez12z12zzcfa_loosez00(
																	((BgL_approxz00_bglt) BgL_approxz00_3019),
																	CNST_TABLE_REF(0));
															}
															{
																obj_t BgL_l1521z00_4237;

																BgL_l1521z00_4237 = CDR(BgL_l1521z00_3016);
																BgL_l1521z00_3016 = BgL_l1521z00_4237;
																goto BgL_zc3z04anonymousza31593ze3z87_3017;
															}
														}
													else
														{	/* Cfa/funcall.scm 163 */
															((bool_t) 1);
														}
												}
												return
													BGl_makezd2typezd2approxz00zzcfa_approxz00(
													((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
											}
										else
											{	/* Cfa/funcall.scm 159 */
												if ((BgL_arityz00_3008 >= 0L))
													{	/* Cfa/funcall.scm 165 */
														return
															BGl_appz12z12zzcfa_appz00(
															((BgL_funz00_bglt) BgL_funz00_3007),
															((BgL_varz00_bglt) BgL_calleez00_3005),
															BgL_argszd2approxzd2_11);
													}
												else
													{	/* Cfa/funcall.scm 165 */
														{
															obj_t BgL_oldzd2argszd2approxz00_3025;
															obj_t BgL_newzd2argszd2approxz00_3026;
															long BgL_arityz00_3027;

															BgL_oldzd2argszd2approxz00_3025 =
																BgL_argszd2approxzd2_11;
															BgL_newzd2argszd2approxz00_3026 = BNIL;
															BgL_arityz00_3027 = BgL_arityz00_3008;
														BgL_zc3z04anonymousza31597ze3z87_3028:
															if ((BgL_arityz00_3027 == -1L))
																{	/* Cfa/funcall.scm 175 */
																	{
																		obj_t BgL_l1523z00_3031;

																		BgL_l1523z00_3031 =
																			BgL_oldzd2argszd2approxz00_3025;
																	BgL_zc3z04anonymousza31599ze3z87_3032:
																		if (PAIRP(BgL_l1523z00_3031))
																			{	/* Cfa/funcall.scm 177 */
																				{	/* Cfa/funcall.scm 177 */
																					obj_t BgL_approxz00_3034;

																					BgL_approxz00_3034 =
																						CAR(BgL_l1523z00_3031);
																					BGl_loosez12z12zzcfa_loosez00(
																						((BgL_approxz00_bglt)
																							BgL_approxz00_3034),
																						CNST_TABLE_REF(0));
																				}
																				{
																					obj_t BgL_l1523z00_4254;

																					BgL_l1523z00_4254 =
																						CDR(BgL_l1523z00_3031);
																					BgL_l1523z00_3031 = BgL_l1523z00_4254;
																					goto
																						BgL_zc3z04anonymousza31599ze3z87_3032;
																				}
																			}
																		else
																			{	/* Cfa/funcall.scm 177 */
																				((bool_t) 1);
																			}
																	}
																	{	/* Cfa/funcall.scm 181 */
																		obj_t BgL_arg1605z00_3037;

																		{	/* Cfa/funcall.scm 181 */
																			obj_t BgL_arg1606z00_3038;

																			{	/* Cfa/funcall.scm 181 */
																				BgL_approxz00_bglt BgL_arg1609z00_3039;

																				{
																					BgL_funcallzf2cinfozf2_bglt
																						BgL_auxz00_4256;
																					{
																						obj_t BgL_auxz00_4257;

																						{	/* Cfa/funcall.scm 181 */
																							BgL_objectz00_bglt
																								BgL_tmpz00_4258;
																							BgL_tmpz00_4258 =
																								((BgL_objectz00_bglt)
																								BgL_nodez00_12);
																							BgL_auxz00_4257 =
																								BGL_OBJECT_WIDENING
																								(BgL_tmpz00_4258);
																						}
																						BgL_auxz00_4256 =
																							((BgL_funcallzf2cinfozf2_bglt)
																							BgL_auxz00_4257);
																					}
																					BgL_arg1609z00_3039 =
																						(((BgL_funcallzf2cinfozf2_bglt)
																							COBJECT(BgL_auxz00_4256))->
																						BgL_vazd2approxzd2);
																				}
																				BgL_arg1606z00_3038 =
																					MAKE_YOUNG_PAIR(
																					((obj_t) BgL_arg1609z00_3039),
																					BgL_newzd2argszd2approxz00_3026);
																			}
																			BgL_arg1605z00_3037 =
																				bgl_reverse_bang(BgL_arg1606z00_3038);
																		}
																		return
																			BGl_appz12z12zzcfa_appz00(
																			((BgL_funz00_bglt) BgL_funz00_3007),
																			((BgL_varz00_bglt) BgL_calleez00_3005),
																			BgL_arg1605z00_3037);
																	}
																}
															else
																{	/* Cfa/funcall.scm 183 */
																	obj_t BgL_arg1611z00_3040;
																	obj_t BgL_arg1613z00_3041;
																	long BgL_arg1615z00_3042;

																	BgL_arg1611z00_3040 =
																		CDR(
																		((obj_t) BgL_oldzd2argszd2approxz00_3025));
																	{	/* Cfa/funcall.scm 184 */
																		obj_t BgL_arg1616z00_3043;

																		BgL_arg1616z00_3043 =
																			CAR(
																			((obj_t)
																				BgL_oldzd2argszd2approxz00_3025));
																		BgL_arg1613z00_3041 =
																			MAKE_YOUNG_PAIR(BgL_arg1616z00_3043,
																			BgL_newzd2argszd2approxz00_3026);
																	}
																	BgL_arg1615z00_3042 =
																		(BgL_arityz00_3027 + 1L);
																	{
																		long BgL_arityz00_4277;
																		obj_t BgL_newzd2argszd2approxz00_4276;
																		obj_t BgL_oldzd2argszd2approxz00_4275;

																		BgL_oldzd2argszd2approxz00_4275 =
																			BgL_arg1611z00_3040;
																		BgL_newzd2argszd2approxz00_4276 =
																			BgL_arg1613z00_3041;
																		BgL_arityz00_4277 = BgL_arg1615z00_3042;
																		BgL_arityz00_3027 = BgL_arityz00_4277;
																		BgL_newzd2argszd2approxz00_3026 =
																			BgL_newzd2argszd2approxz00_4276;
																		BgL_oldzd2argszd2approxz00_3025 =
																			BgL_oldzd2argszd2approxz00_4275;
																		goto BgL_zc3z04anonymousza31597ze3z87_3028;
																	}
																}
														}
													}
											}
									}
								else
									{	/* Cfa/funcall.scm 153 */
										{
											BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_4278;

											{
												obj_t BgL_auxz00_4279;

												{	/* Cfa/funcall.scm 157 */
													BgL_objectz00_bglt BgL_tmpz00_4280;

													BgL_tmpz00_4280 =
														((BgL_objectz00_bglt) BgL_allocz00_10);
													BgL_auxz00_4279 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4280);
												}
												BgL_auxz00_4278 =
													((BgL_makezd2procedurezd2appz00_bglt)
													BgL_auxz00_4279);
											}
											((((BgL_makezd2procedurezd2appz00_bglt)
														COBJECT(BgL_auxz00_4278))->BgL_xzd2tzf3z21) =
												((bool_t) ((bool_t) 0)), BUNSPEC);
										}
										return
											BGl_funcallzd2arityzd2errorz00zzcfa_funcallz00
											(BgL_nodez00_12, BgL_vz00_3006, BgL_arityz00_3008,
											BgL_argszd2approxzd2_11);
									}
							}
						}
					}
				}
			}
		}

	}



/* funcall-type-error */
	obj_t BGl_funcallzd2typezd2errorz00zzcfa_funcallz00(BgL_funcallz00_bglt
		BgL_nodez00_13, BgL_typez00_bglt BgL_typez00_14)
	{
		{	/* Cfa/funcall.scm 190 */
			{	/* Cfa/funcall.scm 193 */
				bool_t BgL_test2050z00_4286;

				{
					BgL_funcallzf2cinfozf2_bglt BgL_auxz00_4287;

					{
						obj_t BgL_auxz00_4288;

						{	/* Cfa/funcall.scm 193 */
							BgL_objectz00_bglt BgL_tmpz00_4289;

							BgL_tmpz00_4289 = ((BgL_objectz00_bglt) BgL_nodez00_13);
							BgL_auxz00_4288 = BGL_OBJECT_WIDENING(BgL_tmpz00_4289);
						}
						BgL_auxz00_4287 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_4288);
					}
					BgL_test2050z00_4286 =
						(((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_4287))->
						BgL_typezd2errorzd2noticedzf3zf3);
				}
				if (BgL_test2050z00_4286)
					{	/* Cfa/funcall.scm 193 */
						return BFALSE;
					}
				else
					{	/* Cfa/funcall.scm 193 */
						{
							BgL_funcallzf2cinfozf2_bglt BgL_auxz00_4294;

							{
								obj_t BgL_auxz00_4295;

								{	/* Cfa/funcall.scm 194 */
									BgL_objectz00_bglt BgL_tmpz00_4296;

									BgL_tmpz00_4296 = ((BgL_objectz00_bglt) BgL_nodez00_13);
									BgL_auxz00_4295 = BGL_OBJECT_WIDENING(BgL_tmpz00_4296);
								}
								BgL_auxz00_4294 =
									((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_4295);
							}
							((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_4294))->
									BgL_typezd2errorzd2noticedzf3zf3) =
								((bool_t) ((bool_t) 1)), BUNSPEC);
						}
						{	/* Cfa/funcall.scm 195 */
							obj_t BgL_arg1629z00_3052;
							obj_t BgL_arg1630z00_3053;

							BgL_arg1629z00_3052 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_nodez00_13)))->BgL_locz00);
							BgL_arg1630z00_3053 =
								BGl_shapez00zztools_shapez00(((obj_t) BgL_typez00_14));
							return
								BGl_userzd2warningzf2locationz20zztools_errorz00
								(BgL_arg1629z00_3052, BGl_string2016z00zzcfa_funcallz00,
								BGl_string2017z00zzcfa_funcallz00, BgL_arg1630z00_3053);
						}
					}
			}
		}

	}



/* funcall-arity-error */
	BgL_approxz00_bglt
		BGl_funcallzd2arityzd2errorz00zzcfa_funcallz00(BgL_funcallz00_bglt
		BgL_nodez00_15, BgL_variablez00_bglt BgL_vz00_16, long BgL_arityz00_17,
		obj_t BgL_argszd2approxzd2_18)
	{
		{	/* Cfa/funcall.scm 203 */
			{	/* Cfa/funcall.scm 205 */
				bool_t BgL_test2051z00_4306;

				{
					BgL_funcallzf2cinfozf2_bglt BgL_auxz00_4307;

					{
						obj_t BgL_auxz00_4308;

						{	/* Cfa/funcall.scm 205 */
							BgL_objectz00_bglt BgL_tmpz00_4309;

							BgL_tmpz00_4309 = ((BgL_objectz00_bglt) BgL_nodez00_15);
							BgL_auxz00_4308 = BGL_OBJECT_WIDENING(BgL_tmpz00_4309);
						}
						BgL_auxz00_4307 = ((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_4308);
					}
					BgL_test2051z00_4306 =
						(((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_4307))->
						BgL_arityzd2errorzd2noticedzf3zf3);
				}
				if (BgL_test2051z00_4306)
					{	/* Cfa/funcall.scm 205 */
						BFALSE;
					}
				else
					{	/* Cfa/funcall.scm 206 */
						long BgL_lenzd2provzd2_3056;

						BgL_lenzd2provzd2_3056 =
							(bgl_list_length(BgL_argszd2approxzd2_18) - 1L);
						{
							BgL_funcallzf2cinfozf2_bglt BgL_auxz00_4316;

							{
								obj_t BgL_auxz00_4317;

								{	/* Cfa/funcall.scm 207 */
									BgL_objectz00_bglt BgL_tmpz00_4318;

									BgL_tmpz00_4318 = ((BgL_objectz00_bglt) BgL_nodez00_15);
									BgL_auxz00_4317 = BGL_OBJECT_WIDENING(BgL_tmpz00_4318);
								}
								BgL_auxz00_4316 =
									((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_4317);
							}
							((((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_4316))->
									BgL_arityzd2errorzd2noticedzf3zf3) =
								((bool_t) ((bool_t) 1)), BUNSPEC);
						}
						{	/* Cfa/funcall.scm 208 */
							obj_t BgL_arg1642z00_3057;
							obj_t BgL_arg1646z00_3058;
							obj_t BgL_arg1650z00_3059;

							BgL_arg1642z00_3057 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt) BgL_nodez00_15)))->BgL_locz00);
							{	/* Cfa/funcall.scm 212 */
								BgL_variablez00_bglt BgL_arg1651z00_3060;

								{	/* Cfa/funcall.scm 212 */
									bool_t BgL_test2052z00_4325;

									{	/* Cfa/funcall.scm 212 */
										obj_t BgL_classz00_3709;

										BgL_classz00_3709 = BGl_localz00zzast_varz00;
										{	/* Cfa/funcall.scm 212 */
											BgL_objectz00_bglt BgL_arg1807z00_3711;

											{	/* Cfa/funcall.scm 212 */
												obj_t BgL_tmpz00_4326;

												BgL_tmpz00_4326 =
													((obj_t) ((BgL_objectz00_bglt) BgL_vz00_16));
												BgL_arg1807z00_3711 =
													(BgL_objectz00_bglt) (BgL_tmpz00_4326);
											}
											if (BGL_CONDEXPAND_ISA_ARCH64())
												{	/* Cfa/funcall.scm 212 */
													long BgL_idxz00_3717;

													BgL_idxz00_3717 =
														BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3711);
													BgL_test2052z00_4325 =
														(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
															(BgL_idxz00_3717 + 2L)) == BgL_classz00_3709);
												}
											else
												{	/* Cfa/funcall.scm 212 */
													bool_t BgL_res2007z00_3742;

													{	/* Cfa/funcall.scm 212 */
														obj_t BgL_oclassz00_3725;

														{	/* Cfa/funcall.scm 212 */
															obj_t BgL_arg1815z00_3733;
															long BgL_arg1816z00_3734;

															BgL_arg1815z00_3733 =
																(BGl_za2classesza2z00zz__objectz00);
															{	/* Cfa/funcall.scm 212 */
																long BgL_arg1817z00_3735;

																BgL_arg1817z00_3735 =
																	BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3711);
																BgL_arg1816z00_3734 =
																	(BgL_arg1817z00_3735 - OBJECT_TYPE);
															}
															BgL_oclassz00_3725 =
																VECTOR_REF(BgL_arg1815z00_3733,
																BgL_arg1816z00_3734);
														}
														{	/* Cfa/funcall.scm 212 */
															bool_t BgL__ortest_1115z00_3726;

															BgL__ortest_1115z00_3726 =
																(BgL_classz00_3709 == BgL_oclassz00_3725);
															if (BgL__ortest_1115z00_3726)
																{	/* Cfa/funcall.scm 212 */
																	BgL_res2007z00_3742 =
																		BgL__ortest_1115z00_3726;
																}
															else
																{	/* Cfa/funcall.scm 212 */
																	long BgL_odepthz00_3727;

																	{	/* Cfa/funcall.scm 212 */
																		obj_t BgL_arg1804z00_3728;

																		BgL_arg1804z00_3728 = (BgL_oclassz00_3725);
																		BgL_odepthz00_3727 =
																			BGL_CLASS_DEPTH(BgL_arg1804z00_3728);
																	}
																	if ((2L < BgL_odepthz00_3727))
																		{	/* Cfa/funcall.scm 212 */
																			obj_t BgL_arg1802z00_3730;

																			{	/* Cfa/funcall.scm 212 */
																				obj_t BgL_arg1803z00_3731;

																				BgL_arg1803z00_3731 =
																					(BgL_oclassz00_3725);
																				BgL_arg1802z00_3730 =
																					BGL_CLASS_ANCESTORS_REF
																					(BgL_arg1803z00_3731, 2L);
																			}
																			BgL_res2007z00_3742 =
																				(BgL_arg1802z00_3730 ==
																				BgL_classz00_3709);
																		}
																	else
																		{	/* Cfa/funcall.scm 212 */
																			BgL_res2007z00_3742 = ((bool_t) 0);
																		}
																}
														}
													}
													BgL_test2052z00_4325 = BgL_res2007z00_3742;
												}
										}
									}
									if (BgL_test2052z00_4325)
										{	/* Cfa/funcall.scm 213 */
											BgL_localz00_bglt BgL_new1169z00_3063;

											{	/* Cfa/funcall.scm 213 */
												BgL_localz00_bglt BgL_new1175z00_3067;

												BgL_new1175z00_3067 =
													((BgL_localz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
																BgL_localz00_bgl))));
												{	/* Cfa/funcall.scm 213 */
													long BgL_arg1654z00_3068;

													BgL_arg1654z00_3068 =
														BGL_CLASS_NUM(BGl_localz00zzast_varz00);
													BGL_OBJECT_CLASS_NUM_SET(
														((BgL_objectz00_bglt) BgL_new1175z00_3067),
														BgL_arg1654z00_3068);
												}
												{	/* Cfa/funcall.scm 213 */
													BgL_objectz00_bglt BgL_tmpz00_4353;

													BgL_tmpz00_4353 =
														((BgL_objectz00_bglt) BgL_new1175z00_3067);
													BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4353, BFALSE);
												}
												((BgL_objectz00_bglt) BgL_new1175z00_3067);
												BgL_new1169z00_3063 = BgL_new1175z00_3067;
											}
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt) BgL_new1169z00_3063)))->
													BgL_idz00) =
												((obj_t) (((BgL_variablez00_bglt)
															COBJECT(BgL_vz00_16))->BgL_idz00)), BUNSPEC);
											((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
																BgL_new1169z00_3063)))->BgL_namez00) =
												((obj_t) (((BgL_variablez00_bglt)
															COBJECT(BgL_vz00_16))->BgL_namez00)), BUNSPEC);
											((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
																BgL_new1169z00_3063)))->BgL_typez00) =
												((BgL_typez00_bglt) (((BgL_variablez00_bglt)
															COBJECT(BgL_vz00_16))->BgL_typez00)), BUNSPEC);
											((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
																BgL_new1169z00_3063)))->BgL_valuez00) =
												((BgL_valuez00_bglt) (((BgL_variablez00_bglt)
															COBJECT(BgL_vz00_16))->BgL_valuez00)), BUNSPEC);
											((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
																BgL_new1169z00_3063)))->BgL_accessz00) =
												((obj_t) (((BgL_variablez00_bglt)
															COBJECT(BgL_vz00_16))->BgL_accessz00)), BUNSPEC);
											((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
																BgL_new1169z00_3063)))->BgL_fastzd2alphazd2) =
												((obj_t) (((BgL_variablez00_bglt)
															COBJECT(BgL_vz00_16))->BgL_fastzd2alphazd2)),
												BUNSPEC);
											((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
																BgL_new1169z00_3063)))->BgL_removablez00) =
												((obj_t) (((BgL_variablez00_bglt)
															COBJECT(BgL_vz00_16))->BgL_removablez00)),
												BUNSPEC);
											((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
																BgL_new1169z00_3063)))->BgL_occurrencez00) =
												((long) (((BgL_variablez00_bglt) COBJECT(BgL_vz00_16))->
														BgL_occurrencez00)), BUNSPEC);
											((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
																BgL_new1169z00_3063)))->BgL_occurrencewz00) =
												((long) (((BgL_variablez00_bglt) COBJECT(BgL_vz00_16))->
														BgL_occurrencewz00)), BUNSPEC);
											((((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
																BgL_new1169z00_3063)))->BgL_userzf3zf3) =
												((bool_t) (((BgL_variablez00_bglt)
															COBJECT(BgL_vz00_16))->BgL_userzf3zf3)), BUNSPEC);
											((((BgL_localz00_bglt) COBJECT(BgL_new1169z00_3063))->
													BgL_keyz00) =
												((long) (((BgL_localz00_bglt)
															COBJECT(((BgL_localz00_bglt) BgL_vz00_16)))->
														BgL_keyz00)), BUNSPEC);
											((((BgL_localz00_bglt) COBJECT(BgL_new1169z00_3063))->
													BgL_valzd2noescapezd2) =
												((obj_t) (((BgL_localz00_bglt)
															COBJECT(((BgL_localz00_bglt) BgL_vz00_16)))->
														BgL_valzd2noescapezd2)), BUNSPEC);
											((((BgL_localz00_bglt) COBJECT(BgL_new1169z00_3063))->
													BgL_volatilez00) =
												((bool_t) (((BgL_localz00_bglt)
															COBJECT(((BgL_localz00_bglt) BgL_vz00_16)))->
														BgL_volatilez00)), BUNSPEC);
											BgL_arg1651z00_3060 =
												((BgL_variablez00_bglt) BgL_new1169z00_3063);
										}
									else
										{	/* Cfa/funcall.scm 214 */
											bool_t BgL_test2056z00_4397;

											{	/* Cfa/funcall.scm 214 */
												obj_t BgL_classz00_3747;

												BgL_classz00_3747 = BGl_globalz00zzast_varz00;
												{	/* Cfa/funcall.scm 214 */
													BgL_objectz00_bglt BgL_arg1807z00_3749;

													{	/* Cfa/funcall.scm 214 */
														obj_t BgL_tmpz00_4398;

														BgL_tmpz00_4398 =
															((obj_t) ((BgL_objectz00_bglt) BgL_vz00_16));
														BgL_arg1807z00_3749 =
															(BgL_objectz00_bglt) (BgL_tmpz00_4398);
													}
													if (BGL_CONDEXPAND_ISA_ARCH64())
														{	/* Cfa/funcall.scm 214 */
															long BgL_idxz00_3755;

															BgL_idxz00_3755 =
																BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3749);
															BgL_test2056z00_4397 =
																(VECTOR_REF
																(BGl_za2inheritancesza2z00zz__objectz00,
																	(BgL_idxz00_3755 + 2L)) == BgL_classz00_3747);
														}
													else
														{	/* Cfa/funcall.scm 214 */
															bool_t BgL_res2008z00_3780;

															{	/* Cfa/funcall.scm 214 */
																obj_t BgL_oclassz00_3763;

																{	/* Cfa/funcall.scm 214 */
																	obj_t BgL_arg1815z00_3771;
																	long BgL_arg1816z00_3772;

																	BgL_arg1815z00_3771 =
																		(BGl_za2classesza2z00zz__objectz00);
																	{	/* Cfa/funcall.scm 214 */
																		long BgL_arg1817z00_3773;

																		BgL_arg1817z00_3773 =
																			BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3749);
																		BgL_arg1816z00_3772 =
																			(BgL_arg1817z00_3773 - OBJECT_TYPE);
																	}
																	BgL_oclassz00_3763 =
																		VECTOR_REF(BgL_arg1815z00_3771,
																		BgL_arg1816z00_3772);
																}
																{	/* Cfa/funcall.scm 214 */
																	bool_t BgL__ortest_1115z00_3764;

																	BgL__ortest_1115z00_3764 =
																		(BgL_classz00_3747 == BgL_oclassz00_3763);
																	if (BgL__ortest_1115z00_3764)
																		{	/* Cfa/funcall.scm 214 */
																			BgL_res2008z00_3780 =
																				BgL__ortest_1115z00_3764;
																		}
																	else
																		{	/* Cfa/funcall.scm 214 */
																			long BgL_odepthz00_3765;

																			{	/* Cfa/funcall.scm 214 */
																				obj_t BgL_arg1804z00_3766;

																				BgL_arg1804z00_3766 =
																					(BgL_oclassz00_3763);
																				BgL_odepthz00_3765 =
																					BGL_CLASS_DEPTH(BgL_arg1804z00_3766);
																			}
																			if ((2L < BgL_odepthz00_3765))
																				{	/* Cfa/funcall.scm 214 */
																					obj_t BgL_arg1802z00_3768;

																					{	/* Cfa/funcall.scm 214 */
																						obj_t BgL_arg1803z00_3769;

																						BgL_arg1803z00_3769 =
																							(BgL_oclassz00_3763);
																						BgL_arg1802z00_3768 =
																							BGL_CLASS_ANCESTORS_REF
																							(BgL_arg1803z00_3769, 2L);
																					}
																					BgL_res2008z00_3780 =
																						(BgL_arg1802z00_3768 ==
																						BgL_classz00_3747);
																				}
																			else
																				{	/* Cfa/funcall.scm 214 */
																					BgL_res2008z00_3780 = ((bool_t) 0);
																				}
																		}
																}
															}
															BgL_test2056z00_4397 = BgL_res2008z00_3780;
														}
												}
											}
											if (BgL_test2056z00_4397)
												{	/* Cfa/funcall.scm 215 */
													BgL_globalz00_bglt BgL_new1176z00_3071;

													{	/* Cfa/funcall.scm 215 */
														BgL_globalz00_bglt BgL_new1189z00_3082;

														BgL_new1189z00_3082 =
															((BgL_globalz00_bglt)
															BOBJECT(GC_MALLOC(sizeof(struct
																		BgL_globalz00_bgl))));
														{	/* Cfa/funcall.scm 215 */
															long BgL_arg1661z00_3083;

															BgL_arg1661z00_3083 =
																BGL_CLASS_NUM(BGl_globalz00zzast_varz00);
															BGL_OBJECT_CLASS_NUM_SET(
																((BgL_objectz00_bglt) BgL_new1189z00_3082),
																BgL_arg1661z00_3083);
														}
														{	/* Cfa/funcall.scm 215 */
															BgL_objectz00_bglt BgL_tmpz00_4425;

															BgL_tmpz00_4425 =
																((BgL_objectz00_bglt) BgL_new1189z00_3082);
															BGL_OBJECT_WIDENING_SET(BgL_tmpz00_4425, BFALSE);
														}
														((BgL_objectz00_bglt) BgL_new1189z00_3082);
														BgL_new1176z00_3071 = BgL_new1189z00_3082;
													}
													((((BgL_variablez00_bglt) COBJECT(
																	((BgL_variablez00_bglt)
																		BgL_new1176z00_3071)))->BgL_idz00) =
														((obj_t) (((BgL_variablez00_bglt)
																	COBJECT(BgL_vz00_16))->BgL_idz00)), BUNSPEC);
													((((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt)
																		BgL_new1176z00_3071)))->BgL_namez00) =
														((obj_t) (((BgL_variablez00_bglt)
																	COBJECT(BgL_vz00_16))->BgL_namez00)),
														BUNSPEC);
													((((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt)
																		BgL_new1176z00_3071)))->BgL_typez00) =
														((BgL_typez00_bglt) (((BgL_variablez00_bglt)
																	COBJECT(BgL_vz00_16))->BgL_typez00)),
														BUNSPEC);
													((((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt)
																		BgL_new1176z00_3071)))->BgL_valuez00) =
														((BgL_valuez00_bglt) (((BgL_variablez00_bglt)
																	COBJECT(BgL_vz00_16))->BgL_valuez00)),
														BUNSPEC);
													((((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt)
																		BgL_new1176z00_3071)))->BgL_accessz00) =
														((obj_t) (((BgL_variablez00_bglt)
																	COBJECT(BgL_vz00_16))->BgL_accessz00)),
														BUNSPEC);
													((((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt)
																		BgL_new1176z00_3071)))->
															BgL_fastzd2alphazd2) =
														((obj_t) (((BgL_variablez00_bglt)
																	COBJECT(BgL_vz00_16))->BgL_fastzd2alphazd2)),
														BUNSPEC);
													((((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt)
																		BgL_new1176z00_3071)))->BgL_removablez00) =
														((obj_t) (((BgL_variablez00_bglt)
																	COBJECT(BgL_vz00_16))->BgL_removablez00)),
														BUNSPEC);
													((((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt)
																		BgL_new1176z00_3071)))->BgL_occurrencez00) =
														((long) (((BgL_variablez00_bglt)
																	COBJECT(BgL_vz00_16))->BgL_occurrencez00)),
														BUNSPEC);
													((((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt)
																		BgL_new1176z00_3071)))->
															BgL_occurrencewz00) =
														((long) (((BgL_variablez00_bglt)
																	COBJECT(BgL_vz00_16))->BgL_occurrencewz00)),
														BUNSPEC);
													((((BgL_variablez00_bglt)
																COBJECT(((BgL_variablez00_bglt)
																		BgL_new1176z00_3071)))->BgL_userzf3zf3) =
														((bool_t) (((BgL_variablez00_bglt)
																	COBJECT(BgL_vz00_16))->BgL_userzf3zf3)),
														BUNSPEC);
													((((BgL_globalz00_bglt)
																COBJECT(BgL_new1176z00_3071))->BgL_modulez00) =
														((obj_t) (((BgL_globalz00_bglt)
																	COBJECT(((BgL_globalz00_bglt) BgL_vz00_16)))->
																BgL_modulez00)), BUNSPEC);
													((((BgL_globalz00_bglt)
																COBJECT(BgL_new1176z00_3071))->BgL_importz00) =
														((obj_t) (((BgL_globalz00_bglt)
																	COBJECT(((BgL_globalz00_bglt) BgL_vz00_16)))->
																BgL_importz00)), BUNSPEC);
													((((BgL_globalz00_bglt)
																COBJECT(BgL_new1176z00_3071))->
															BgL_evaluablezf3zf3) =
														((bool_t) (((BgL_globalz00_bglt)
																	COBJECT(((BgL_globalz00_bglt) BgL_vz00_16)))->
																BgL_evaluablezf3zf3)), BUNSPEC);
													((((BgL_globalz00_bglt)
																COBJECT(BgL_new1176z00_3071))->BgL_evalzf3zf3) =
														((bool_t) (((BgL_globalz00_bglt)
																	COBJECT(((BgL_globalz00_bglt) BgL_vz00_16)))->
																BgL_evalzf3zf3)), BUNSPEC);
													((((BgL_globalz00_bglt)
																COBJECT(BgL_new1176z00_3071))->BgL_libraryz00) =
														((obj_t) (((BgL_globalz00_bglt)
																	COBJECT(((BgL_globalz00_bglt) BgL_vz00_16)))->
																BgL_libraryz00)), BUNSPEC);
													((((BgL_globalz00_bglt)
																COBJECT(BgL_new1176z00_3071))->BgL_pragmaz00) =
														((obj_t) (((BgL_globalz00_bglt)
																	COBJECT(((BgL_globalz00_bglt) BgL_vz00_16)))->
																BgL_pragmaz00)), BUNSPEC);
													((((BgL_globalz00_bglt)
																COBJECT(BgL_new1176z00_3071))->BgL_srcz00) =
														((obj_t) (((BgL_globalz00_bglt)
																	COBJECT(((BgL_globalz00_bglt) BgL_vz00_16)))->
																BgL_srcz00)), BUNSPEC);
													((((BgL_globalz00_bglt)
																COBJECT(BgL_new1176z00_3071))->
															BgL_jvmzd2typezd2namez00) =
														((obj_t) (((BgL_globalz00_bglt)
																	COBJECT(((BgL_globalz00_bglt) BgL_vz00_16)))->
																BgL_jvmzd2typezd2namez00)), BUNSPEC);
													((((BgL_globalz00_bglt)
																COBJECT(BgL_new1176z00_3071))->BgL_initz00) =
														((obj_t) (((BgL_globalz00_bglt)
																	COBJECT(((BgL_globalz00_bglt) BgL_vz00_16)))->
																BgL_initz00)), BUNSPEC);
													((((BgL_globalz00_bglt)
																COBJECT(BgL_new1176z00_3071))->BgL_aliasz00) =
														((obj_t) (((BgL_globalz00_bglt)
																	COBJECT(((BgL_globalz00_bglt) BgL_vz00_16)))->
																BgL_aliasz00)), BUNSPEC);
													BgL_arg1651z00_3060 =
														((BgL_variablez00_bglt) BgL_new1176z00_3071);
												}
											else
												{	/* Cfa/funcall.scm 214 */
													BgL_arg1651z00_3060 = BgL_vz00_16;
												}
										}
								}
								BgL_arg1646z00_3058 =
									BGl_shapez00zztools_shapez00(((obj_t) BgL_arg1651z00_3060));
							}
							{	/* Cfa/funcall.scm 220 */
								obj_t BgL_arg1663z00_3084;
								obj_t BgL_arg1675z00_3085;

								{	/* Cfa/funcall.scm 220 */

									BgL_arg1663z00_3084 =
										BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00(
										(BgL_arityz00_17 - 1L), 10L);
								}
								{	/* Cfa/funcall.scm 222 */

									BgL_arg1675z00_3085 =
										BGl_integerzd2ze3stringz31zz__r4_numbers_6_5_fixnumz00
										(BgL_lenzd2provzd2_3056, 10L);
								}
								{	/* Cfa/funcall.scm 219 */
									obj_t BgL_list1676z00_3086;

									{	/* Cfa/funcall.scm 219 */
										obj_t BgL_arg1678z00_3087;

										{	/* Cfa/funcall.scm 219 */
											obj_t BgL_arg1681z00_3088;

											{	/* Cfa/funcall.scm 219 */
												obj_t BgL_arg1688z00_3089;

												BgL_arg1688z00_3089 =
													MAKE_YOUNG_PAIR(BGl_string2018z00zzcfa_funcallz00,
													BNIL);
												BgL_arg1681z00_3088 =
													MAKE_YOUNG_PAIR(BgL_arg1675z00_3085,
													BgL_arg1688z00_3089);
											}
											BgL_arg1678z00_3087 =
												MAKE_YOUNG_PAIR(BGl_string2019z00zzcfa_funcallz00,
												BgL_arg1681z00_3088);
										}
										BgL_list1676z00_3086 =
											MAKE_YOUNG_PAIR(BgL_arg1663z00_3084, BgL_arg1678z00_3087);
									}
									BgL_arg1650z00_3059 =
										BGl_stringzd2appendzd2zz__r4_strings_6_7z00
										(BgL_list1676z00_3086);
								}
							}
							BGl_userzd2warningzf2locationz20zztools_errorz00
								(BgL_arg1642z00_3057, BgL_arg1646z00_3058,
								BGl_string2020z00zzcfa_funcallz00, BgL_arg1650z00_3059);
						}
					}
			}
			return BGl_makezd2emptyzd2approxz00zzcfa_approxz00();
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_funcallz00(void)
	{
		{	/* Cfa/funcall.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_funcallz00(void)
	{
		{	/* Cfa/funcall.scm 15 */
			return BUNSPEC;
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_funcallz00(void)
	{
		{	/* Cfa/funcall.scm 15 */
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_funcallzf2Cinfozf2zzcfa_infoz00,
				BGl_proc2021z00zzcfa_funcallz00, BGl_string2022z00zzcfa_funcallz00);
		}

	}



/* &cfa!-funcall/Cinfo1526 */
	BgL_approxz00_bglt
		BGl_z62cfaz12zd2funcallzf2Cinfo1526z50zzcfa_funcallz00(obj_t
		BgL_envz00_4024, obj_t BgL_nodez00_4025)
	{
		{	/* Cfa/funcall.scm 44 */
			{	/* Cfa/funcall.scm 48 */
				BgL_approxz00_bglt BgL_fapproxz00_4052;

				{	/* Cfa/funcall.scm 48 */
					BgL_nodez00_bglt BgL_arg1823z00_4053;

					BgL_arg1823z00_4053 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt)
									((BgL_funcallz00_bglt) BgL_nodez00_4025))))->BgL_funz00);
					BgL_fapproxz00_4052 = BGl_cfaz12z12zzcfa_cfaz00(BgL_arg1823z00_4053);
				}
				{	/* Cfa/funcall.scm 48 */
					obj_t BgL_aapproxz00_4054;

					{	/* Cfa/funcall.scm 49 */
						obj_t BgL_l1513z00_4055;

						{	/* Cfa/funcall.scm 49 */
							obj_t BgL_pairz00_4056;

							BgL_pairz00_4056 =
								(((BgL_funcallz00_bglt) COBJECT(
										((BgL_funcallz00_bglt)
											((BgL_funcallz00_bglt) BgL_nodez00_4025))))->BgL_argsz00);
							BgL_l1513z00_4055 = CDR(BgL_pairz00_4056);
						}
						if (NULLP(BgL_l1513z00_4055))
							{	/* Cfa/funcall.scm 49 */
								BgL_aapproxz00_4054 = BNIL;
							}
						else
							{	/* Cfa/funcall.scm 49 */
								obj_t BgL_head1515z00_4057;

								{	/* Cfa/funcall.scm 49 */
									BgL_approxz00_bglt BgL_arg1812z00_4058;

									{	/* Cfa/funcall.scm 49 */
										obj_t BgL_arg1820z00_4059;

										BgL_arg1820z00_4059 = CAR(((obj_t) BgL_l1513z00_4055));
										BgL_arg1812z00_4058 =
											BGl_cfaz12z12zzcfa_cfaz00(
											((BgL_nodez00_bglt) BgL_arg1820z00_4059));
									}
									BgL_head1515z00_4057 =
										MAKE_YOUNG_PAIR(((obj_t) BgL_arg1812z00_4058), BNIL);
								}
								{	/* Cfa/funcall.scm 49 */
									obj_t BgL_g1518z00_4060;

									BgL_g1518z00_4060 = CDR(((obj_t) BgL_l1513z00_4055));
									{
										obj_t BgL_l1513z00_4062;
										obj_t BgL_tail1516z00_4063;

										BgL_l1513z00_4062 = BgL_g1518z00_4060;
										BgL_tail1516z00_4063 = BgL_head1515z00_4057;
									BgL_zc3z04anonymousza31801ze3z87_4061:
										if (NULLP(BgL_l1513z00_4062))
											{	/* Cfa/funcall.scm 49 */
												BgL_aapproxz00_4054 = BgL_head1515z00_4057;
											}
										else
											{	/* Cfa/funcall.scm 49 */
												obj_t BgL_newtail1517z00_4064;

												{	/* Cfa/funcall.scm 49 */
													BgL_approxz00_bglt BgL_arg1806z00_4065;

													{	/* Cfa/funcall.scm 49 */
														obj_t BgL_arg1808z00_4066;

														BgL_arg1808z00_4066 =
															CAR(((obj_t) BgL_l1513z00_4062));
														BgL_arg1806z00_4065 =
															BGl_cfaz12z12zzcfa_cfaz00(
															((BgL_nodez00_bglt) BgL_arg1808z00_4066));
													}
													BgL_newtail1517z00_4064 =
														MAKE_YOUNG_PAIR(
														((obj_t) BgL_arg1806z00_4065), BNIL);
												}
												SET_CDR(BgL_tail1516z00_4063, BgL_newtail1517z00_4064);
												{	/* Cfa/funcall.scm 49 */
													obj_t BgL_arg1805z00_4067;

													BgL_arg1805z00_4067 =
														CDR(((obj_t) BgL_l1513z00_4062));
													{
														obj_t BgL_tail1516z00_4533;
														obj_t BgL_l1513z00_4532;

														BgL_l1513z00_4532 = BgL_arg1805z00_4067;
														BgL_tail1516z00_4533 = BgL_newtail1517z00_4064;
														BgL_tail1516z00_4063 = BgL_tail1516z00_4533;
														BgL_l1513z00_4062 = BgL_l1513z00_4532;
														goto BgL_zc3z04anonymousza31801ze3z87_4061;
													}
												}
											}
									}
								}
							}
					}
					{	/* Cfa/funcall.scm 49 */

						{	/* Cfa/funcall.scm 54 */
							BgL_typez00_bglt BgL_funzd2typezd2_4068;

							BgL_funzd2typezd2_4068 =
								(((BgL_approxz00_bglt) COBJECT(BgL_fapproxz00_4052))->
								BgL_typez00);
							{	/* Cfa/funcall.scm 55 */
								bool_t BgL_test2062z00_4535;

								if (
									(((obj_t) BgL_funzd2typezd2_4068) ==
										BGl_za2_za2z00zztype_cachez00))
									{	/* Cfa/funcall.scm 55 */
										BgL_test2062z00_4535 = ((bool_t) 1);
									}
								else
									{	/* Cfa/funcall.scm 55 */
										if (
											(((obj_t) BgL_funzd2typezd2_4068) ==
												BGl_za2objza2z00zztype_cachez00))
											{	/* Cfa/funcall.scm 56 */
												BgL_test2062z00_4535 = ((bool_t) 1);
											}
										else
											{	/* Cfa/funcall.scm 56 */
												BgL_test2062z00_4535 =
													(
													((obj_t) BgL_funzd2typezd2_4068) ==
													BGl_za2procedureza2z00zztype_cachez00);
											}
									}
								if (BgL_test2062z00_4535)
									{	/* Cfa/funcall.scm 55 */
										BFALSE;
									}
								else
									{	/* Cfa/funcall.scm 55 */
										BGl_funcallzd2typezd2errorz00zzcfa_funcallz00(
											((BgL_funcallz00_bglt) BgL_nodez00_4025),
											BgL_funzd2typezd2_4068);
									}
							}
						}
						{	/* Cfa/funcall.scm 60 */
							bool_t BgL_test2065z00_4546;

							{	/* Cfa/funcall.scm 60 */
								bool_t BgL_test2066z00_4547;

								{	/* Cfa/funcall.scm 60 */
									BgL_typez00_bglt BgL_arg1705z00_4069;

									BgL_arg1705z00_4069 =
										(((BgL_approxz00_bglt) COBJECT(BgL_fapproxz00_4052))->
										BgL_typez00);
									BgL_test2066z00_4547 =
										(((obj_t) BgL_arg1705z00_4069) ==
										BGl_za2procedureza2z00zztype_cachez00);
								}
								if (BgL_test2066z00_4547)
									{	/* Cfa/funcall.scm 60 */
										BgL_test2065z00_4546 =
											(((BgL_approxz00_bglt) COBJECT(BgL_fapproxz00_4052))->
											BgL_topzf3zf3);
									}
								else
									{	/* Cfa/funcall.scm 60 */
										BgL_test2065z00_4546 = ((bool_t) 1);
									}
							}
							if (BgL_test2065z00_4546)
								{	/* Cfa/funcall.scm 62 */
									BgL_approxz00_bglt BgL_arg1703z00_4070;

									{
										BgL_funcallzf2cinfozf2_bglt BgL_auxz00_4552;

										{
											obj_t BgL_auxz00_4553;

											{	/* Cfa/funcall.scm 62 */
												BgL_objectz00_bglt BgL_tmpz00_4554;

												BgL_tmpz00_4554 =
													((BgL_objectz00_bglt)
													((BgL_funcallz00_bglt) BgL_nodez00_4025));
												BgL_auxz00_4553 = BGL_OBJECT_WIDENING(BgL_tmpz00_4554);
											}
											BgL_auxz00_4552 =
												((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_4553);
										}
										BgL_arg1703z00_4070 =
											(((BgL_funcallzf2cinfozf2_bglt)
												COBJECT(BgL_auxz00_4552))->BgL_approxz00);
									}
									BGl_approxzd2setzd2typez12z12zzcfa_approxz00
										(BgL_arg1703z00_4070,
										((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
								}
							else
								{	/* Cfa/funcall.scm 60 */
									BFALSE;
								}
						}
						{	/* Cfa/funcall.scm 65 */
							bool_t BgL_test2067z00_4562;

							if (
								(((BgL_approxz00_bglt) COBJECT(BgL_fapproxz00_4052))->
									BgL_topzf3zf3))
								{	/* Cfa/funcall.scm 65 */
									BgL_test2067z00_4562 = ((bool_t) 1);
								}
							else
								{	/* Cfa/funcall.scm 66 */
									bool_t BgL_test2069z00_4565;

									{	/* Cfa/funcall.scm 66 */
										BgL_typez00_bglt BgL_arg1749z00_4071;

										BgL_arg1749z00_4071 =
											(((BgL_approxz00_bglt) COBJECT(BgL_fapproxz00_4052))->
											BgL_typez00);
										BgL_test2069z00_4565 =
											(((obj_t) BgL_arg1749z00_4071) ==
											BGl_za2procedureza2z00zztype_cachez00);
									}
									if (BgL_test2069z00_4565)
										{	/* Cfa/funcall.scm 66 */
											BgL_test2067z00_4562 = ((bool_t) 0);
										}
									else
										{	/* Cfa/funcall.scm 66 */
											BgL_test2067z00_4562 = ((bool_t) 1);
										}
								}
							if (BgL_test2067z00_4562)
								{	/* Cfa/funcall.scm 65 */
									{
										obj_t BgL_l1519z00_4073;

										BgL_l1519z00_4073 = BgL_aapproxz00_4054;
									BgL_zc3z04anonymousza31716ze3z87_4072:
										if (PAIRP(BgL_l1519z00_4073))
											{	/* Cfa/funcall.scm 67 */
												{	/* Cfa/funcall.scm 67 */
													obj_t BgL_approxz00_4074;

													BgL_approxz00_4074 = CAR(BgL_l1519z00_4073);
													BGl_loosez12z12zzcfa_loosez00(
														((BgL_approxz00_bglt) BgL_approxz00_4074),
														CNST_TABLE_REF(0));
												}
												{
													obj_t BgL_l1519z00_4575;

													BgL_l1519z00_4575 = CDR(BgL_l1519z00_4073);
													BgL_l1519z00_4073 = BgL_l1519z00_4575;
													goto BgL_zc3z04anonymousza31716ze3z87_4072;
												}
											}
										else
											{	/* Cfa/funcall.scm 67 */
												((bool_t) 1);
											}
									}
									{	/* Cfa/funcall.scm 70 */
										obj_t BgL_zc3z04anonymousza31723ze3z87_4075;

										BgL_zc3z04anonymousza31723ze3z87_4075 =
											MAKE_FX_PROCEDURE
											(BGl_z62zc3z04anonymousza31723ze3ze5zzcfa_funcallz00,
											(int) (1L), (int) (3L));
										PROCEDURE_SET(BgL_zc3z04anonymousza31723ze3z87_4075,
											(int) (0L),
											((obj_t) ((BgL_funcallz00_bglt) BgL_nodez00_4025)));
										PROCEDURE_SET(BgL_zc3z04anonymousza31723ze3z87_4075,
											(int) (1L), BgL_aapproxz00_4054);
										PROCEDURE_SET(BgL_zc3z04anonymousza31723ze3z87_4075,
											(int) (2L),
											((obj_t) ((BgL_funcallz00_bglt) BgL_nodez00_4025)));
										BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
											(BgL_zc3z04anonymousza31723ze3z87_4075,
											BgL_fapproxz00_4052);
									}
									{	/* Cfa/funcall.scm 77 */
										BgL_approxz00_bglt BgL_arg1736z00_4076;

										{
											BgL_funcallzf2cinfozf2_bglt BgL_auxz00_4591;

											{
												obj_t BgL_auxz00_4592;

												{	/* Cfa/funcall.scm 77 */
													BgL_objectz00_bglt BgL_tmpz00_4593;

													BgL_tmpz00_4593 =
														((BgL_objectz00_bglt)
														((BgL_funcallz00_bglt) BgL_nodez00_4025));
													BgL_auxz00_4592 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4593);
												}
												BgL_auxz00_4591 =
													((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_4592);
											}
											BgL_arg1736z00_4076 =
												(((BgL_funcallzf2cinfozf2_bglt)
													COBJECT(BgL_auxz00_4591))->BgL_approxz00);
										}
										BGl_approxzd2setzd2topz12z12zzcfa_approxz00
											(BgL_arg1736z00_4076);
								}}
							else
								{	/* Cfa/funcall.scm 65 */
									if (CBOOL(BGl_emptyzd2approxzd2alloczf3zf3zzcfa_approxz00
											(BgL_fapproxz00_4052)))
										{	/* Cfa/funcall.scm 82 */
											BgL_approxz00_bglt BgL_arg1738z00_4077;

											{
												BgL_funcallzf2cinfozf2_bglt BgL_auxz00_4603;

												{
													obj_t BgL_auxz00_4604;

													{	/* Cfa/funcall.scm 82 */
														BgL_objectz00_bglt BgL_tmpz00_4605;

														BgL_tmpz00_4605 =
															((BgL_objectz00_bglt)
															((BgL_funcallz00_bglt) BgL_nodez00_4025));
														BgL_auxz00_4604 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4605);
													}
													BgL_auxz00_4603 =
														((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_4604);
												}
												BgL_arg1738z00_4077 =
													(((BgL_funcallzf2cinfozf2_bglt)
														COBJECT(BgL_auxz00_4603))->BgL_approxz00);
											}
											BGl_approxzd2setzd2typez12z12zzcfa_approxz00
												(BgL_arg1738z00_4077,
												((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00));
										}
									else
										{	/* Cfa/funcall.scm 86 */
											obj_t BgL_zc3z04anonymousza31740ze3z87_4078;

											BgL_zc3z04anonymousza31740ze3z87_4078 =
												MAKE_FX_PROCEDURE
												(BGl_z62zc3z04anonymousza31740ze3ze5zzcfa_funcallz00,
												(int) (1L), (int) (3L));
											PROCEDURE_SET(BgL_zc3z04anonymousza31740ze3z87_4078,
												(int) (0L),
												((obj_t) ((BgL_funcallz00_bglt) BgL_nodez00_4025)));
											PROCEDURE_SET(BgL_zc3z04anonymousza31740ze3z87_4078,
												(int) (1L), BgL_aapproxz00_4054);
											PROCEDURE_SET(BgL_zc3z04anonymousza31740ze3z87_4078,
												(int) (2L),
												((obj_t) ((BgL_funcallz00_bglt) BgL_nodez00_4025)));
											BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
												(BgL_zc3z04anonymousza31740ze3z87_4078,
												BgL_fapproxz00_4052);
						}}}
						{	/* Cfa/funcall.scm 96 */
							bool_t BgL_test2072z00_4627;

							if (CBOOL
								(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00))
								{	/* Cfa/funcall.scm 97 */
									bool_t BgL_test2074z00_4630;

									{	/* Cfa/funcall.scm 97 */
										BgL_typez00_bglt BgL_arg1798z00_4079;

										{	/* Cfa/funcall.scm 97 */
											BgL_approxz00_bglt BgL_arg1799z00_4080;

											{
												BgL_funcallzf2cinfozf2_bglt BgL_auxz00_4631;

												{
													obj_t BgL_auxz00_4632;

													{	/* Cfa/funcall.scm 97 */
														BgL_objectz00_bglt BgL_tmpz00_4633;

														BgL_tmpz00_4633 =
															((BgL_objectz00_bglt)
															((BgL_funcallz00_bglt) BgL_nodez00_4025));
														BgL_auxz00_4632 =
															BGL_OBJECT_WIDENING(BgL_tmpz00_4633);
													}
													BgL_auxz00_4631 =
														((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_4632);
												}
												BgL_arg1799z00_4080 =
													(((BgL_funcallzf2cinfozf2_bglt)
														COBJECT(BgL_auxz00_4631))->BgL_approxz00);
											}
											BgL_arg1798z00_4079 =
												(((BgL_approxz00_bglt) COBJECT(BgL_arg1799z00_4080))->
												BgL_typez00);
										}
										BgL_test2074z00_4630 =
											BGl_bigloozd2typezf3z21zztype_typez00
											(BgL_arg1798z00_4079);
									}
									if (BgL_test2074z00_4630)
										{	/* Cfa/funcall.scm 98 */
											bool_t BgL_test2075z00_4641;

											{	/* Cfa/funcall.scm 98 */
												BgL_typez00_bglt BgL_arg1773z00_4081;

												{	/* Cfa/funcall.scm 98 */
													BgL_approxz00_bglt BgL_arg1775z00_4082;

													{
														BgL_funcallzf2cinfozf2_bglt BgL_auxz00_4642;

														{
															obj_t BgL_auxz00_4643;

															{	/* Cfa/funcall.scm 98 */
																BgL_objectz00_bglt BgL_tmpz00_4644;

																BgL_tmpz00_4644 =
																	((BgL_objectz00_bglt)
																	((BgL_funcallz00_bglt) BgL_nodez00_4025));
																BgL_auxz00_4643 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_4644);
															}
															BgL_auxz00_4642 =
																((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_4643);
														}
														BgL_arg1775z00_4082 =
															(((BgL_funcallzf2cinfozf2_bglt)
																COBJECT(BgL_auxz00_4642))->BgL_approxz00);
													}
													BgL_arg1773z00_4081 =
														(((BgL_approxz00_bglt)
															COBJECT(BgL_arg1775z00_4082))->BgL_typez00);
												}
												BgL_test2075z00_4641 =
													(
													((obj_t) BgL_arg1773z00_4081) ==
													BGl_za2_za2z00zztype_cachez00);
											}
											if (BgL_test2075z00_4641)
												{	/* Cfa/funcall.scm 98 */
													BgL_test2072z00_4627 = ((bool_t) 0);
												}
											else
												{	/* Cfa/funcall.scm 98 */
													BgL_test2072z00_4627 = ((bool_t) 1);
												}
										}
									else
										{	/* Cfa/funcall.scm 97 */
											BgL_test2072z00_4627 = ((bool_t) 0);
										}
								}
							else
								{	/* Cfa/funcall.scm 96 */
									BgL_test2072z00_4627 = ((bool_t) 0);
								}
							if (BgL_test2072z00_4627)
								{	/* Cfa/funcall.scm 96 */
									BGl_forzd2eachzd2approxzd2alloczd2zzcfa_approxz00
										(BGl_proc2023z00zzcfa_funcallz00, BgL_fapproxz00_4052);
								}
							else
								{	/* Cfa/funcall.scm 96 */
									BFALSE;
								}
						}
						{
							BgL_funcallzf2cinfozf2_bglt BgL_auxz00_4654;

							{
								obj_t BgL_auxz00_4655;

								{	/* Cfa/funcall.scm 106 */
									BgL_objectz00_bglt BgL_tmpz00_4656;

									BgL_tmpz00_4656 =
										((BgL_objectz00_bglt)
										((BgL_funcallz00_bglt) BgL_nodez00_4025));
									BgL_auxz00_4655 = BGL_OBJECT_WIDENING(BgL_tmpz00_4656);
								}
								BgL_auxz00_4654 =
									((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_4655);
							}
							return
								(((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_4654))->
								BgL_approxz00);
						}
					}
				}
			}
		}

	}



/* &<@anonymous:1771> */
	obj_t BGl_z62zc3z04anonymousza31771ze3ze5zzcfa_funcallz00(obj_t
		BgL_envz00_4026, obj_t BgL_az00_4027)
	{
		{	/* Cfa/funcall.scm 100 */
			{	/* Cfa/funcall.scm 101 */
				bool_t BgL_test2076z00_4662;

				{	/* Cfa/funcall.scm 101 */
					obj_t BgL_classz00_4083;

					BgL_classz00_4083 = BGl_makezd2procedurezd2appz00zzcfa_info2z00;
					if (BGL_OBJECTP(BgL_az00_4027))
						{	/* Cfa/funcall.scm 101 */
							BgL_objectz00_bglt BgL_arg1807z00_4084;

							BgL_arg1807z00_4084 = (BgL_objectz00_bglt) (BgL_az00_4027);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/funcall.scm 101 */
									long BgL_idxz00_4085;

									BgL_idxz00_4085 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4084);
									BgL_test2076z00_4662 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4085 + 4L)) == BgL_classz00_4083);
								}
							else
								{	/* Cfa/funcall.scm 101 */
									bool_t BgL_res2011z00_4088;

									{	/* Cfa/funcall.scm 101 */
										obj_t BgL_oclassz00_4089;

										{	/* Cfa/funcall.scm 101 */
											obj_t BgL_arg1815z00_4090;
											long BgL_arg1816z00_4091;

											BgL_arg1815z00_4090 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/funcall.scm 101 */
												long BgL_arg1817z00_4092;

												BgL_arg1817z00_4092 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4084);
												BgL_arg1816z00_4091 =
													(BgL_arg1817z00_4092 - OBJECT_TYPE);
											}
											BgL_oclassz00_4089 =
												VECTOR_REF(BgL_arg1815z00_4090, BgL_arg1816z00_4091);
										}
										{	/* Cfa/funcall.scm 101 */
											bool_t BgL__ortest_1115z00_4093;

											BgL__ortest_1115z00_4093 =
												(BgL_classz00_4083 == BgL_oclassz00_4089);
											if (BgL__ortest_1115z00_4093)
												{	/* Cfa/funcall.scm 101 */
													BgL_res2011z00_4088 = BgL__ortest_1115z00_4093;
												}
											else
												{	/* Cfa/funcall.scm 101 */
													long BgL_odepthz00_4094;

													{	/* Cfa/funcall.scm 101 */
														obj_t BgL_arg1804z00_4095;

														BgL_arg1804z00_4095 = (BgL_oclassz00_4089);
														BgL_odepthz00_4094 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4095);
													}
													if ((4L < BgL_odepthz00_4094))
														{	/* Cfa/funcall.scm 101 */
															obj_t BgL_arg1802z00_4096;

															{	/* Cfa/funcall.scm 101 */
																obj_t BgL_arg1803z00_4097;

																BgL_arg1803z00_4097 = (BgL_oclassz00_4089);
																BgL_arg1802z00_4096 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4097,
																	4L);
															}
															BgL_res2011z00_4088 =
																(BgL_arg1802z00_4096 == BgL_classz00_4083);
														}
													else
														{	/* Cfa/funcall.scm 101 */
															BgL_res2011z00_4088 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2076z00_4662 = BgL_res2011z00_4088;
								}
						}
					else
						{	/* Cfa/funcall.scm 101 */
							BgL_test2076z00_4662 = ((bool_t) 0);
						}
				}
				if (BgL_test2076z00_4662)
					{	/* Cfa/funcall.scm 101 */
						return
							BGl_setzd2procedurezd2approxzd2polymorphicz12zc0zzcfa_procedurez00
							(((BgL_appz00_bglt) BgL_az00_4027));
					}
				else
					{	/* Cfa/funcall.scm 101 */
						return BFALSE;
					}
			}
		}

	}



/* &<@anonymous:1723> */
	obj_t BGl_z62zc3z04anonymousza31723ze3ze5zzcfa_funcallz00(obj_t
		BgL_envz00_4028, obj_t BgL_allocz00_4032)
	{
		{	/* Cfa/funcall.scm 69 */
			{	/* Cfa/funcall.scm 70 */
				BgL_funcallz00_bglt BgL_i1162z00_4029;
				obj_t BgL_aapproxz00_4030;
				BgL_funcallz00_bglt BgL_nodez00_4031;

				BgL_i1162z00_4029 =
					((BgL_funcallz00_bglt) PROCEDURE_REF(BgL_envz00_4028, (int) (0L)));
				BgL_aapproxz00_4030 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4028, (int) (1L)));
				BgL_nodez00_4031 =
					((BgL_funcallz00_bglt) PROCEDURE_REF(BgL_envz00_4028, (int) (2L)));
				{
					BgL_approxz00_bglt BgL_auxz00_4696;

					{	/* Cfa/funcall.scm 70 */
						bool_t BgL_test2081z00_4697;

						{	/* Cfa/funcall.scm 70 */
							obj_t BgL_classz00_4098;

							BgL_classz00_4098 = BGl_makezd2procedurezd2appz00zzcfa_info2z00;
							if (BGL_OBJECTP(BgL_allocz00_4032))
								{	/* Cfa/funcall.scm 70 */
									BgL_objectz00_bglt BgL_arg1807z00_4099;

									BgL_arg1807z00_4099 =
										(BgL_objectz00_bglt) (BgL_allocz00_4032);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/funcall.scm 70 */
											long BgL_idxz00_4100;

											BgL_idxz00_4100 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4099);
											BgL_test2081z00_4697 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4100 + 4L)) == BgL_classz00_4098);
										}
									else
										{	/* Cfa/funcall.scm 70 */
											bool_t BgL_res2009z00_4103;

											{	/* Cfa/funcall.scm 70 */
												obj_t BgL_oclassz00_4104;

												{	/* Cfa/funcall.scm 70 */
													obj_t BgL_arg1815z00_4105;
													long BgL_arg1816z00_4106;

													BgL_arg1815z00_4105 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/funcall.scm 70 */
														long BgL_arg1817z00_4107;

														BgL_arg1817z00_4107 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4099);
														BgL_arg1816z00_4106 =
															(BgL_arg1817z00_4107 - OBJECT_TYPE);
													}
													BgL_oclassz00_4104 =
														VECTOR_REF(BgL_arg1815z00_4105,
														BgL_arg1816z00_4106);
												}
												{	/* Cfa/funcall.scm 70 */
													bool_t BgL__ortest_1115z00_4108;

													BgL__ortest_1115z00_4108 =
														(BgL_classz00_4098 == BgL_oclassz00_4104);
													if (BgL__ortest_1115z00_4108)
														{	/* Cfa/funcall.scm 70 */
															BgL_res2009z00_4103 = BgL__ortest_1115z00_4108;
														}
													else
														{	/* Cfa/funcall.scm 70 */
															long BgL_odepthz00_4109;

															{	/* Cfa/funcall.scm 70 */
																obj_t BgL_arg1804z00_4110;

																BgL_arg1804z00_4110 = (BgL_oclassz00_4104);
																BgL_odepthz00_4109 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4110);
															}
															if ((4L < BgL_odepthz00_4109))
																{	/* Cfa/funcall.scm 70 */
																	obj_t BgL_arg1802z00_4111;

																	{	/* Cfa/funcall.scm 70 */
																		obj_t BgL_arg1803z00_4112;

																		BgL_arg1803z00_4112 = (BgL_oclassz00_4104);
																		BgL_arg1802z00_4111 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4112, 4L);
																	}
																	BgL_res2009z00_4103 =
																		(BgL_arg1802z00_4111 == BgL_classz00_4098);
																}
															else
																{	/* Cfa/funcall.scm 70 */
																	BgL_res2009z00_4103 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2081z00_4697 = BgL_res2009z00_4103;
										}
								}
							else
								{	/* Cfa/funcall.scm 70 */
									BgL_test2081z00_4697 = ((bool_t) 0);
								}
						}
						if (BgL_test2081z00_4697)
							{	/* Cfa/funcall.scm 71 */
								BgL_approxz00_bglt BgL_eapproxz00_4113;

								{
									BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_4720;

									{
										obj_t BgL_auxz00_4721;

										{	/* Cfa/funcall.scm 71 */
											BgL_objectz00_bglt BgL_tmpz00_4722;

											BgL_tmpz00_4722 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_allocz00_4032));
											BgL_auxz00_4721 = BGL_OBJECT_WIDENING(BgL_tmpz00_4722);
										}
										BgL_auxz00_4720 =
											((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_4721);
									}
									BgL_eapproxz00_4113 =
										(((BgL_makezd2procedurezd2appz00_bglt)
											COBJECT(BgL_auxz00_4720))->BgL_approxz00);
								}
								{	/* Cfa/funcall.scm 72 */
									BgL_approxz00_bglt BgL_arg1733z00_4114;
									BgL_approxz00_bglt BgL_arg1734z00_4115;

									{
										BgL_funcallzf2cinfozf2_bglt BgL_auxz00_4728;

										{
											obj_t BgL_auxz00_4729;

											{	/* Cfa/funcall.scm 72 */
												BgL_objectz00_bglt BgL_tmpz00_4730;

												BgL_tmpz00_4730 =
													((BgL_objectz00_bglt) BgL_i1162z00_4029);
												BgL_auxz00_4729 = BGL_OBJECT_WIDENING(BgL_tmpz00_4730);
											}
											BgL_auxz00_4728 =
												((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_4729);
										}
										BgL_arg1733z00_4114 =
											(((BgL_funcallzf2cinfozf2_bglt)
												COBJECT(BgL_auxz00_4728))->BgL_approxz00);
									}
									{	/* Cfa/funcall.scm 74 */
										obj_t BgL_arg1735z00_4116;

										BgL_arg1735z00_4116 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_eapproxz00_4113), BgL_aapproxz00_4030);
										BgL_arg1734z00_4115 =
											BGl_funcallz12z12zzcfa_funcallz00(
											((BgL_appz00_bglt) BgL_allocz00_4032),
											BgL_arg1735z00_4116, BgL_nodez00_4031);
									}
									BgL_auxz00_4696 =
										BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1733z00_4114,
										BgL_arg1734z00_4115);
								}
							}
						else
							{	/* Cfa/funcall.scm 70 */
								BgL_auxz00_4696 = BGl_makezd2emptyzd2approxz00zzcfa_approxz00();
							}
					}
					return ((obj_t) BgL_auxz00_4696);
				}
			}
		}

	}



/* &<@anonymous:1740> */
	obj_t BGl_z62zc3z04anonymousza31740ze3ze5zzcfa_funcallz00(obj_t
		BgL_envz00_4033, obj_t BgL_allocz00_4037)
	{
		{	/* Cfa/funcall.scm 85 */
			{	/* Cfa/funcall.scm 86 */
				BgL_funcallz00_bglt BgL_i1162z00_4034;
				obj_t BgL_aapproxz00_4035;
				BgL_funcallz00_bglt BgL_nodez00_4036;

				BgL_i1162z00_4034 =
					((BgL_funcallz00_bglt) PROCEDURE_REF(BgL_envz00_4033, (int) (0L)));
				BgL_aapproxz00_4035 =
					((obj_t) PROCEDURE_REF(BgL_envz00_4033, (int) (1L)));
				BgL_nodez00_4036 =
					((BgL_funcallz00_bglt) PROCEDURE_REF(BgL_envz00_4033, (int) (2L)));
				{
					BgL_approxz00_bglt BgL_auxz00_4751;

					{	/* Cfa/funcall.scm 86 */
						bool_t BgL_test2086z00_4752;

						{	/* Cfa/funcall.scm 86 */
							obj_t BgL_classz00_4117;

							BgL_classz00_4117 = BGl_makezd2procedurezd2appz00zzcfa_info2z00;
							if (BGL_OBJECTP(BgL_allocz00_4037))
								{	/* Cfa/funcall.scm 86 */
									BgL_objectz00_bglt BgL_arg1807z00_4118;

									BgL_arg1807z00_4118 =
										(BgL_objectz00_bglt) (BgL_allocz00_4037);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/funcall.scm 86 */
											long BgL_idxz00_4119;

											BgL_idxz00_4119 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4118);
											BgL_test2086z00_4752 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4119 + 4L)) == BgL_classz00_4117);
										}
									else
										{	/* Cfa/funcall.scm 86 */
											bool_t BgL_res2010z00_4122;

											{	/* Cfa/funcall.scm 86 */
												obj_t BgL_oclassz00_4123;

												{	/* Cfa/funcall.scm 86 */
													obj_t BgL_arg1815z00_4124;
													long BgL_arg1816z00_4125;

													BgL_arg1815z00_4124 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/funcall.scm 86 */
														long BgL_arg1817z00_4126;

														BgL_arg1817z00_4126 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4118);
														BgL_arg1816z00_4125 =
															(BgL_arg1817z00_4126 - OBJECT_TYPE);
													}
													BgL_oclassz00_4123 =
														VECTOR_REF(BgL_arg1815z00_4124,
														BgL_arg1816z00_4125);
												}
												{	/* Cfa/funcall.scm 86 */
													bool_t BgL__ortest_1115z00_4127;

													BgL__ortest_1115z00_4127 =
														(BgL_classz00_4117 == BgL_oclassz00_4123);
													if (BgL__ortest_1115z00_4127)
														{	/* Cfa/funcall.scm 86 */
															BgL_res2010z00_4122 = BgL__ortest_1115z00_4127;
														}
													else
														{	/* Cfa/funcall.scm 86 */
															long BgL_odepthz00_4128;

															{	/* Cfa/funcall.scm 86 */
																obj_t BgL_arg1804z00_4129;

																BgL_arg1804z00_4129 = (BgL_oclassz00_4123);
																BgL_odepthz00_4128 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4129);
															}
															if ((4L < BgL_odepthz00_4128))
																{	/* Cfa/funcall.scm 86 */
																	obj_t BgL_arg1802z00_4130;

																	{	/* Cfa/funcall.scm 86 */
																		obj_t BgL_arg1803z00_4131;

																		BgL_arg1803z00_4131 = (BgL_oclassz00_4123);
																		BgL_arg1802z00_4130 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4131, 4L);
																	}
																	BgL_res2010z00_4122 =
																		(BgL_arg1802z00_4130 == BgL_classz00_4117);
																}
															else
																{	/* Cfa/funcall.scm 86 */
																	BgL_res2010z00_4122 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2086z00_4752 = BgL_res2010z00_4122;
										}
								}
							else
								{	/* Cfa/funcall.scm 86 */
									BgL_test2086z00_4752 = ((bool_t) 0);
								}
						}
						if (BgL_test2086z00_4752)
							{	/* Cfa/funcall.scm 87 */
								BgL_approxz00_bglt BgL_eapproxz00_4132;

								{
									BgL_makezd2procedurezd2appz00_bglt BgL_auxz00_4775;

									{
										obj_t BgL_auxz00_4776;

										{	/* Cfa/funcall.scm 87 */
											BgL_objectz00_bglt BgL_tmpz00_4777;

											BgL_tmpz00_4777 =
												((BgL_objectz00_bglt)
												((BgL_appz00_bglt) BgL_allocz00_4037));
											BgL_auxz00_4776 = BGL_OBJECT_WIDENING(BgL_tmpz00_4777);
										}
										BgL_auxz00_4775 =
											((BgL_makezd2procedurezd2appz00_bglt) BgL_auxz00_4776);
									}
									BgL_eapproxz00_4132 =
										(((BgL_makezd2procedurezd2appz00_bglt)
											COBJECT(BgL_auxz00_4775))->BgL_approxz00);
								}
								{	/* Cfa/funcall.scm 88 */
									BgL_approxz00_bglt BgL_arg1746z00_4133;
									BgL_approxz00_bglt BgL_arg1747z00_4134;

									{
										BgL_funcallzf2cinfozf2_bglt BgL_auxz00_4783;

										{
											obj_t BgL_auxz00_4784;

											{	/* Cfa/funcall.scm 88 */
												BgL_objectz00_bglt BgL_tmpz00_4785;

												BgL_tmpz00_4785 =
													((BgL_objectz00_bglt) BgL_i1162z00_4034);
												BgL_auxz00_4784 = BGL_OBJECT_WIDENING(BgL_tmpz00_4785);
											}
											BgL_auxz00_4783 =
												((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_4784);
										}
										BgL_arg1746z00_4133 =
											(((BgL_funcallzf2cinfozf2_bglt)
												COBJECT(BgL_auxz00_4783))->BgL_approxz00);
									}
									{	/* Cfa/funcall.scm 90 */
										obj_t BgL_arg1748z00_4135;

										BgL_arg1748z00_4135 =
											MAKE_YOUNG_PAIR(
											((obj_t) BgL_eapproxz00_4132), BgL_aapproxz00_4035);
										BgL_arg1747z00_4134 =
											BGl_funcallz12z12zzcfa_funcallz00(
											((BgL_appz00_bglt) BgL_allocz00_4037),
											BgL_arg1748z00_4135, BgL_nodez00_4036);
									}
									BgL_auxz00_4751 =
										BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1746z00_4133,
										BgL_arg1747z00_4134);
								}
							}
						else
							{	/* Cfa/funcall.scm 86 */
								BgL_auxz00_4751 = BGl_makezd2emptyzd2approxz00zzcfa_approxz00();
							}
					}
					return ((obj_t) BgL_auxz00_4751);
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_funcallz00(void)
	{
		{	/* Cfa/funcall.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
			BGl_modulezd2initializa7ationz75zztools_argsz00(47102372L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
			BGl_modulezd2initializa7ationz75zzcfa_cfaz00(400853817L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
			BGl_modulezd2initializa7ationz75zzcfa_loosez00(471177480L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
			BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
			BGl_modulezd2initializa7ationz75zzcfa_appz00(102053843L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_procedurez00(227655313L,
				BSTRING_TO_STRING(BGl_string2024z00zzcfa_funcallz00));
		}

	}

#ifdef __cplusplus
}
#endif
