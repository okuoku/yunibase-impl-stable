/*===========================================================================*/
/*   (Cfa/type.scm)                                                          */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/type.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_TYPE_TYPE_DEFINITIONS
#define BGL_CFA_TYPE_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_cvarz00_bgl
	{
		header_t header;
		obj_t widening;
		bool_t BgL_macrozf3zf3;
	}              *BgL_cvarz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_atomz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}              *BgL_atomz00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_closurez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}                 *BgL_closurez00_bglt;

	typedef struct BgL_kwotez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_valuez00;
	}               *BgL_kwotez00_bglt;

	typedef struct BgL_sequencez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_nodesz00;
		bool_t BgL_unsafez00;
		obj_t BgL_metaz00;
	}                  *BgL_sequencez00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_appzd2lyzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}                  *BgL_appzd2lyzd2_bglt;

	typedef struct BgL_funcallz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_strengthz00;
		obj_t BgL_functionsz00;
	}                 *BgL_funcallz00_bglt;

	typedef struct BgL_externz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
	}                *BgL_externz00_bglt;

	typedef struct BgL_vallocz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
	}                *BgL_vallocz00_bglt;

	typedef struct BgL_vrefz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}              *BgL_vrefz00_bglt;

	typedef struct BgL_vsetz12z12_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_exprza2za2;
		obj_t BgL_effectz00;
		obj_t BgL_czd2formatzd2;
		struct BgL_typez00_bgl *BgL_ftypez00;
		struct BgL_typez00_bgl *BgL_otypez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		bool_t BgL_unsafez00;
	}                 *BgL_vsetz12z12_bglt;

	typedef struct BgL_castz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_argz00;
	}              *BgL_castz00_bglt;

	typedef struct BgL_setqz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}              *BgL_setqz00_bglt;

	typedef struct BgL_conditionalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		struct BgL_nodez00_bgl *BgL_truez00;
		struct BgL_nodez00_bgl *BgL_falsez00;
	}                     *BgL_conditionalz00_bglt;

	typedef struct BgL_failz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_procz00;
		struct BgL_nodez00_bgl *BgL_msgz00;
		struct BgL_nodez00_bgl *BgL_objz00;
	}              *BgL_failz00_bglt;

	typedef struct BgL_switchz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_testz00;
		obj_t BgL_clausesz00;
		struct BgL_typez00_bgl *BgL_itemzd2typezd2;
	}                *BgL_switchz00_bglt;

	typedef struct BgL_letzd2funzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_localsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}                   *BgL_letzd2funzd2_bglt;

	typedef struct BgL_letzd2varzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		obj_t BgL_bindingsz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		bool_t BgL_removablezf3zf3;
	}                   *BgL_letzd2varzd2_bglt;

	typedef struct BgL_setzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
		struct BgL_nodez00_bgl *BgL_onexitz00;
	}                       *BgL_setzd2exzd2itz00_bglt;

	typedef struct BgL_jumpzd2exzd2itz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_exitz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
	}                        *BgL_jumpzd2exzd2itz00_bglt;

	typedef struct BgL_makezd2boxzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
		obj_t BgL_stackablez00;
	}                    *BgL_makezd2boxzd2_bglt;

	typedef struct BgL_boxzd2refzd2_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                   *BgL_boxzd2refzd2_bglt;

	typedef struct BgL_boxzd2setz12zc0_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_varz00_bgl *BgL_varz00;
		struct BgL_nodez00_bgl *BgL_valuez00;
		struct BgL_typez00_bgl *BgL_vtypez00;
	}                      *BgL_boxzd2setz12zc0_bglt;

	typedef struct BgL_syncz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_nodez00_bgl *BgL_mutexz00;
		struct BgL_nodez00_bgl *BgL_prelockz00;
		struct BgL_nodez00_bgl *BgL_bodyz00;
	}              *BgL_syncz00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_internzd2sfunzf2cinfoz20_bgl
	{
		bool_t BgL_polymorphiczf3zf3;
		struct BgL_approxz00_bgl *BgL_approxz00;
		long BgL_stampz00;
	}                               *BgL_internzd2sfunzf2cinfoz20_bglt;

	typedef struct BgL_svarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_clozd2envzf3z21;
		long BgL_stampz00;
	}                      *BgL_svarzf2cinfozf2_bglt;

	typedef struct BgL_cvarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_cvarzf2cinfozf2_bglt;

	typedef struct BgL_appzd2lyzf2cinfoz20_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                          *BgL_appzd2lyzf2cinfoz20_bglt;

	typedef struct BgL_funcallzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		struct BgL_approxz00_bgl *BgL_vazd2approxzd2;
		bool_t BgL_arityzd2errorzd2noticedzf3zf3;
		bool_t BgL_typezd2errorzd2noticedzf3zf3;
	}                         *BgL_funcallzf2cinfozf2_bglt;

	typedef struct BgL_conditionalzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                             *BgL_conditionalzf2cinfozf2_bglt;

	typedef struct BgL_arithmeticzd2appzd2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		obj_t BgL_speczd2typeszd2;
	}                          *BgL_arithmeticzd2appzd2_bglt;

	typedef struct BgL_procedurezd2refzd2appz00_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                               *BgL_procedurezd2refzd2appz00_bglt;

	typedef struct BgL_procedurezd2setz12zd2appz12_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		obj_t BgL_vapproxz00;
	}                                  *BgL_procedurezd2setz12zd2appz12_bglt;

	typedef struct BgL_makezd2vectorzd2appz00_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		struct BgL_approxz00_bgl *BgL_valuezd2approxzd2;
		long BgL_lostzd2stampzd2;
		struct BgL_variablez00_bgl *BgL_ownerz00;
		obj_t BgL_stackzd2stampzd2;
		bool_t BgL_seenzf3zf3;
		bool_t BgL_tvectorzf3zf3;
	}                             *BgL_makezd2vectorzd2appz00_bglt;

	typedef struct BgL_conszd2refzd2appz00_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		obj_t BgL_getz00;
	}                          *BgL_conszd2refzd2appz00_bglt;

	typedef struct BgL_valloczf2cinfozb2optimz40_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		struct BgL_approxz00_bgl *BgL_valuezd2approxzd2;
		long BgL_lostzd2stampzd2;
		struct BgL_variablez00_bgl *BgL_ownerz00;
		bool_t BgL_stackablezf3zf3;
		obj_t BgL_stackzd2stampzd2;
		bool_t BgL_seenzf3zf3;
	}                                *BgL_valloczf2cinfozb2optimz40_bglt;


#endif													// BGL_CFA_TYPE_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2setq1688z70zzcfa_typez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t
		BGl_getzd2approxzd2typez00zzcfa_typez00(BgL_approxz00_bglt, obj_t);
	extern obj_t BGl_closurez00zzast_nodez00;
	extern obj_t BGl_setzd2lengthzd2zzcfa_setz00(obj_t);
	extern obj_t BGl_syncz00zzast_nodez00;
	static obj_t
		BGl_setzd2variablezd2typez12z12zzcfa_typez00(BgL_variablez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_sfunz00zzast_varz00;
	static obj_t BGl_z62typezd2variablez12zd2sexit1642z70zzcfa_typez00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_sequencez00zzast_nodez00;
	extern obj_t BGl_za2optimzd2cfazf3za2z21zzengine_paramz00;
	extern obj_t BGl_letzd2varzd2zzast_nodez00;
	extern obj_t BGl_failz00zzast_nodez00;
	static obj_t BGl_objectzd2initzd2zzcfa_typez00(void);
	BGL_EXPORTED_DECL obj_t BGl_typezd2settingsz12zc0zzcfa_typez00(obj_t);
	static obj_t BGl_z62typezd2variablez12zd2svarzf21636z82zzcfa_typez00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62typezd2variablez12zd2cvarzf21640z82zzcfa_typez00(obj_t,
		obj_t, obj_t);
	BGL_IMPORT obj_t BGl_memqz00zz__r4_pairs_and_lists_6_3z00(obj_t, obj_t);
	extern obj_t BGl_boxzd2refzd2zzast_nodez00;
	static obj_t BGl_typezd2funzd2nodez12z12zzcfa_typez00(BgL_varz00_bglt);
	extern obj_t BGl_appzd2lyzf2Cinfoz20zzcfa_infoz00;
	extern obj_t BGl_cvarzf2Cinfozf2zzcfa_infoz00;
	static obj_t BGl_methodzd2initzd2zzcfa_typez00(void);
	extern obj_t BGl_externz00zzast_nodez00;
	extern BgL_typez00_bglt
		BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_typez00_bglt);
	extern obj_t BGl_typezd2closuresz12zc0zzcfa_closurez00(void);
	static obj_t BGl_z62typezd2variablez121633za2zzcfa_typez00(obj_t, obj_t,
		obj_t);
	extern obj_t BGl_procedurezd2refzd2appz00zzcfa_info2z00;
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2atom1648z70zzcfa_typez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_errorz00zz__errorz00(obj_t, obj_t, obj_t);
	extern obj_t
		BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00;
	extern obj_t BGl_za2_za2z00zztype_cachez00;
	extern BgL_typez00_bglt
		BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2procedure1664z70zzcfa_typez00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2procedure1666z70zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_za2objza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2letzd2var1700za2zzcfa_typez00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2jumpzd2exzd2i1704z70zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_cfunz00zzast_varz00;
	extern bool_t BGl_typezd2subclasszf3z21zzobject_classz00(BgL_typez00_bglt,
		BgL_typez00_bglt);
	extern obj_t BGl_appzd2lyzd2zzast_nodez00;
	static obj_t BGl_z62typezd2nodez121645za2zzcfa_typez00(obj_t, obj_t);
	static obj_t BGl_cleanupzd2typeze70z35zzcfa_typez00(obj_t);
	extern obj_t BGl_za2unsafezd2typeza2zd2zzengine_paramz00;
	static obj_t BGl_gczd2rootszd2initz00zzcfa_typez00(void);
	static obj_t BGl_typezd2variablez12zc0zzcfa_typez00(BgL_valuez00_bglt,
		BgL_variablez00_bglt);
	static BgL_nodez00_bglt BGl_z62typezd2nodez12zd2app1660z70zzcfa_typez00(obj_t,
		obj_t);
	extern obj_t BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
	extern obj_t BGl_tvectorzd2optimiza7ationzf3z86zzcfa_tvectorz00(void);
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2funcall1674z70zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_vallocz00zzast_nodez00;
	extern obj_t BGl_sexitzf2Cinfozf2zzcfa_infoz00;
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2funcallzf2C1676z82zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_setzd2everyzd2zzcfa_setz00(obj_t, obj_t);
	static obj_t BGl_typezd2nodeza2z12z62zzcfa_typez00(obj_t);
	extern obj_t BGl_za2epairza2z00zztype_cachez00;
	extern obj_t BGl_svarzf2Cinfozf2zzcfa_infoz00;
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern bool_t BGl_bigloozd2typezf3z21zztype_typez00(BgL_typez00_bglt);
	extern obj_t BGl_conditionalz00zzast_nodez00;
	BGL_IMPORT obj_t
		BGl_findzd2superzd2classzd2methodzd2zz__objectz00(BgL_objectz00_bglt, obj_t,
		obj_t);
	static obj_t BGl_z62typezd2variablez12zd2inter1644z70zzcfa_typez00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2sync1658z70zzcfa_typez00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2vsetz121684z62zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_funcallzf2Cinfozf2zzcfa_infoz00;
	static obj_t BGl_z62getzd2approxzd2typez62zzcfa_typez00(obj_t, obj_t, obj_t);
	extern obj_t BGl_setqz00zzast_nodez00;
	static obj_t BGl_z62typezd2settingsz12za2zzcfa_typez00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2switch1696z70zzcfa_typez00(obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_typez00 = BUNSPEC;
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2fail1694z70zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_za2vectorza2z00zztype_cachez00;
	extern obj_t BGl_funz00zzast_varz00;
	extern obj_t BGl_setzd2exzd2itz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	extern obj_t BGl_tclassz00zzobject_classz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_atomz00zzast_nodez00;
	static obj_t BGl_markzd2nullzd2typezd2ctorz12zc0zzcfa_typez00(obj_t);
	static obj_t BGl_toplevelzd2initzd2zzcfa_typez00(void);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcfa_typez00(void);
	extern BgL_typez00_bglt BGl_getzd2defaultzd2typez00zztype_cachez00(void);
	extern obj_t BGl_conszd2refzd2appz00zzcfa_info2z00;
	extern obj_t BGl_setzd2headzd2zzcfa_setz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2boxzd2setz121708zb0zzcfa_typez00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2makezd2box1706za2zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_castz00zzast_nodez00;
	extern BgL_typez00_bglt
		BGl_getzd2vectorzd2itemzd2typezd2zzcfa_tvectorz00(BgL_nodez00_bglt);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	extern obj_t BGl_arithmeticzd2appzd2zzcfa_info2z00;
	extern obj_t BGl_typez00zztype_typez00;
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2arithmeti1662z70zzcfa_typez00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	static obj_t BGl_z62typezd2variablez12zd2scnst1638z70zzcfa_typez00(obj_t,
		obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2vref1682z70zzcfa_typez00(obj_t, obj_t);
	extern bool_t
		BGl_approxzd2procedurezd2elzf3zf3zzcfa_closurez00(BgL_approxz00_bglt);
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2letzd2fun1698za2zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_findzd2globalzd2zzast_envz00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2kwote1650z70zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_varz00zzast_nodez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2zztype_typeofz00(BgL_nodez00_bglt,
		bool_t);
	extern obj_t BGl_makezd2boxzd2zzast_nodez00;
	extern obj_t BGl_za2pairza2z00zztype_cachez00;
	static obj_t BGl_z62typezd2variablez12za2zzcfa_typez00(obj_t, obj_t, obj_t);
	static BgL_nodez00_bglt BGl_z62typezd2nodez12zd2var1652z70zzcfa_typez00(obj_t,
		obj_t);
	extern obj_t
		BGl_za2optimzd2cfazd2freezd2varzd2trackingzf3za2zf3zzengine_paramz00;
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2condition1690z70zzcfa_typez00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2condition1692z70zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_boxzd2setz12zc0zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2appzd2lyzf2Ci1672z50zzcfa_typez00(obj_t, obj_t);
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_typez00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzobject_classz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztvector_tvectorz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_closurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_setz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info3z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_envz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzengine_paramz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typeofz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__errorz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__biglooz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2boxzd2ref1710za2zzcfa_typez00(obj_t, obj_t);
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2setzd2exzd2it1702z70zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_vrefz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2sequence1656z70zzcfa_typez00(obj_t, obj_t);
	static BgL_nodez00_bglt BGl_typezd2nodez12zc0zzcfa_typez00(BgL_nodez00_bglt);
	extern obj_t BGl_nodez00zzast_nodez00;
	extern obj_t BGl_kwotez00zzast_nodez00;
	extern obj_t BGl_localz00zzast_varz00;
	extern obj_t BGl_jumpzd2exzd2itz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzcfa_typez00(void);
	static obj_t BGl_typezd2funz12zc0zzcfa_typez00(BgL_variablez00_bglt);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_typez00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_typez00(void);
	extern obj_t BGl_tvecz00zztvector_tvectorz00;
	static BgL_nodez00_bglt BGl_z62typezd2nodez12za2zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_conditionalzf2Cinfozf2zzcfa_infoz00;
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2valloc1680z70zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_procedurezd2setz12zd2appz12zzcfa_info2z00;
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2closure1654z70zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_letzd2funzd2zzast_nodez00;
	extern obj_t BGl_valuez00zzast_varz00;
	extern obj_t BGl_za2unspecza2z00zztype_cachez00;
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2conszd2refzd21668z70zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_scnstzf2Cinfozf2zzcfa_infoz00;
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2appzd2ly1670za2zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_switchz00zzast_nodez00;
	BGL_IMPORT obj_t BGl_classzd2superzd2zz__objectz00(obj_t);
	extern obj_t BGl_za2procedurezd2elza2zd2zztype_cachez00;
	extern BgL_typez00_bglt BGl_getzd2typezd2kwotez00zztype_typeofz00(obj_t);
	BGL_IMPORT obj_t BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(obj_t);
	extern obj_t BGl_funcallz00zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2cast1686z70zzcfa_typez00(obj_t, obj_t);
	extern obj_t BGl_globalz00zzast_varz00;
	extern obj_t BGl_vsetz12z12zzast_nodez00;
	static BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2extern1678z70zzcfa_typez00(obj_t, obj_t);
	static obj_t __cnst[5];


	   
		 
		DEFINE_STRING(BGl_string2313z00zzcfa_typez00,
		BgL_bgl_string2313za700za7za7c2366za7, "type-fun!", 9);
	      DEFINE_STRING(BGl_string2314z00zzcfa_typez00,
		BgL_bgl_string2314za700za7za7c2367za7, "Unknown value", 13);
	      DEFINE_STRING(BGl_string2315z00zzcfa_typez00,
		BgL_bgl_string2315za700za7za7c2368za7, "cfg:null-type!", 14);
	      DEFINE_STRING(BGl_string2316z00zzcfa_typez00,
		BgL_bgl_string2316za700za7za7c2369za7, "Cannot find null-type variable",
		30);
	      DEFINE_STRING(BGl_string2318z00zzcfa_typez00,
		BgL_bgl_string2318za700za7za7c2370za7, "type-variable!1633", 18);
	      DEFINE_STRING(BGl_string2320z00zzcfa_typez00,
		BgL_bgl_string2320za700za7za7c2371za7, "type-node!1645", 14);
	      DEFINE_STRING(BGl_string2321z00zzcfa_typez00,
		BgL_bgl_string2321za700za7za7c2372za7, "No method for this object", 25);
	      DEFINE_STRING(BGl_string2323z00zzcfa_typez00,
		BgL_bgl_string2323za700za7za7c2373za7, "type-variable!", 14);
	      DEFINE_STRING(BGl_string2329z00zzcfa_typez00,
		BgL_bgl_string2329za700za7za7c2374za7, "type-node!", 10);
	extern obj_t BGl_makezd2vectorzd2appzf3zd2envz21zzcfa_info2z00;
	   
		 
		DEFINE_STRING(BGl_string2361z00zzcfa_typez00,
		BgL_bgl_string2361za700za7za7c2375za7, "unexpected node", 15);
	      DEFINE_STRING(BGl_string2362z00zzcfa_typez00,
		BgL_bgl_string2362za700za7za7c2376za7, "Unexpected closure", 18);
	      DEFINE_STRING(BGl_string2363z00zzcfa_typez00,
		BgL_bgl_string2363za700za7za7c2377za7, "cfa_type", 8);
	      DEFINE_STRING(BGl_string2364z00zzcfa_typez00,
		BgL_bgl_string2364za700za7za7c2378za7,
		"nothing static (light elight) type-node!1645 bigloo ", 52);
	      DEFINE_STATIC_BGL_GENERIC(BGl_typezd2variablez12zd2envz12zzcfa_typez00,
		BgL_bgl_za762typeza7d2variab2379z00,
		BGl_z62typezd2variablez12za2zzcfa_typez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2317z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2variab2380z00,
		BGl_z62typezd2variablez121633za2zzcfa_typez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2319z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712381za7,
		BGl_z62typezd2nodez121645za2zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2322z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2variab2382z00,
		BGl_z62typezd2variablez12zd2svarzf21636z82zzcfa_typez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2324z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2variab2383z00,
		BGl_z62typezd2variablez12zd2scnst1638z70zzcfa_typez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2325z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2variab2384z00,
		BGl_z62typezd2variablez12zd2cvarzf21640z82zzcfa_typez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2326z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2variab2385z00,
		BGl_z62typezd2variablez12zd2sexit1642z70zzcfa_typez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2327z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2variab2386z00,
		BGl_z62typezd2variablez12zd2inter1644z70zzcfa_typez00, 0L, BUNSPEC, 2);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2328z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712387za7,
		BGl_z62typezd2nodez12zd2atom1648z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2330z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712388za7,
		BGl_z62typezd2nodez12zd2kwote1650z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2331z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712389za7,
		BGl_z62typezd2nodez12zd2var1652z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2332z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712390za7,
		BGl_z62typezd2nodez12zd2closure1654z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2333z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712391za7,
		BGl_z62typezd2nodez12zd2sequence1656z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2334z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712392za7,
		BGl_z62typezd2nodez12zd2sync1658z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2335z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712393za7,
		BGl_z62typezd2nodez12zd2app1660z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2336z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712394za7,
		BGl_z62typezd2nodez12zd2arithmeti1662z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2337z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712395za7,
		BGl_z62typezd2nodez12zd2procedure1664z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2338z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712396za7,
		BGl_z62typezd2nodez12zd2procedure1666z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2339z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712397za7,
		BGl_z62typezd2nodez12zd2conszd2refzd21668z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2340z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712398za7,
		BGl_z62typezd2nodez12zd2appzd2ly1670za2zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2341z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712399za7,
		BGl_z62typezd2nodez12zd2appzd2lyzf2Ci1672z50zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2342z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712400za7,
		BGl_z62typezd2nodez12zd2funcall1674z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2343z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712401za7,
		BGl_z62typezd2nodez12zd2funcallzf2C1676z82zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2344z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712402za7,
		BGl_z62typezd2nodez12zd2extern1678z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2345z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712403za7,
		BGl_z62typezd2nodez12zd2valloc1680z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2346z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712404za7,
		BGl_z62typezd2nodez12zd2vref1682z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2347z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712405za7,
		BGl_z62typezd2nodez12zd2vsetz121684z62zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2348z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712406za7,
		BGl_z62typezd2nodez12zd2cast1686z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2349z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712407za7,
		BGl_z62typezd2nodez12zd2setq1688z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2350z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712408za7,
		BGl_z62typezd2nodez12zd2condition1690z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2351z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712409za7,
		BGl_z62typezd2nodez12zd2condition1692z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2352z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712410za7,
		BGl_z62typezd2nodez12zd2fail1694z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2353z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712411za7,
		BGl_z62typezd2nodez12zd2switch1696z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2354z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712412za7,
		BGl_z62typezd2nodez12zd2letzd2fun1698za2zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2355z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712413za7,
		BGl_z62typezd2nodez12zd2letzd2var1700za2zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2356z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712414za7,
		BGl_z62typezd2nodez12zd2setzd2exzd2it1702z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2357z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712415za7,
		BGl_z62typezd2nodez12zd2jumpzd2exzd2i1704z70zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2358z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712416za7,
		BGl_z62typezd2nodez12zd2makezd2box1706za2zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2359z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712417za7,
		BGl_z62typezd2nodez12zd2boxzd2setz121708zb0zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc2360z00zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712418za7,
		BGl_z62typezd2nodez12zd2boxzd2ref1710za2zzcfa_typez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_typezd2settingsz12zd2envz12zzcfa_typez00,
		BgL_bgl_za762typeza7d2settin2419z00,
		BGl_z62typezd2settingsz12za2zzcfa_typez00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_GENERIC(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
		BgL_bgl_za762typeza7d2nodeza712420za7,
		BGl_z62typezd2nodez12za2zzcfa_typez00, 0L, BUNSPEC, 1);
	     
		DEFINE_EXPORT_BGL_PROCEDURE(BGl_getzd2approxzd2typezd2envzd2zzcfa_typez00,
		BgL_bgl_za762getza7d2approxza72421za7,
		BGl_z62getzd2approxzd2typez62zzcfa_typez00, 0L, BUNSPEC, 2);
	extern obj_t BGl_valloczf2Cinfozb2optimzf3zd2envz61zzcfa_info3z00;

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_typez00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_typez00(long
		BgL_checksumz00_6505, char *BgL_fromz00_6506)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_typez00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_typez00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_typez00();
					BGl_libraryzd2moduleszd2initz00zzcfa_typez00();
					BGl_cnstzd2initzd2zzcfa_typez00();
					BGl_importedzd2moduleszd2initz00zzcfa_typez00();
					BGl_genericzd2initzd2zzcfa_typez00();
					BGl_methodzd2initzd2zzcfa_typez00();
					return BGl_toplevelzd2initzd2zzcfa_typez00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_typez00(void)
	{
		{	/* Cfa/type.scm 15 */
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_type");
			BGl_modulezd2initializa7ationz75zz__errorz00(0L, "cfa_type");
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_type");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_type");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_type");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_type");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cfa_type");
			BGl_modulezd2initializa7ationz75zz__r4_symbols_6_4z00(0L, "cfa_type");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "cfa_type");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_type");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_type");
			BGl_modulezd2initializa7ationz75zz__biglooz00(0L, "cfa_type");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_typez00(void)
	{
		{	/* Cfa/type.scm 15 */
			{	/* Cfa/type.scm 15 */
				obj_t BgL_cportz00_6218;

				{	/* Cfa/type.scm 15 */
					obj_t BgL_stringz00_6225;

					BgL_stringz00_6225 = BGl_string2364z00zzcfa_typez00;
					{	/* Cfa/type.scm 15 */
						obj_t BgL_startz00_6226;

						BgL_startz00_6226 = BINT(0L);
						{	/* Cfa/type.scm 15 */
							obj_t BgL_endz00_6227;

							BgL_endz00_6227 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_6225)));
							{	/* Cfa/type.scm 15 */

								BgL_cportz00_6218 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_6225, BgL_startz00_6226, BgL_endz00_6227);
				}}}}
				{
					long BgL_iz00_6219;

					BgL_iz00_6219 = 4L;
				BgL_loopz00_6220:
					if ((BgL_iz00_6219 == -1L))
						{	/* Cfa/type.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/type.scm 15 */
							{	/* Cfa/type.scm 15 */
								obj_t BgL_arg2365z00_6221;

								{	/* Cfa/type.scm 15 */

									{	/* Cfa/type.scm 15 */
										obj_t BgL_locationz00_6223;

										BgL_locationz00_6223 = BBOOL(((bool_t) 0));
										{	/* Cfa/type.scm 15 */

											BgL_arg2365z00_6221 =
												BGl_readz00zz__readerz00(BgL_cportz00_6218,
												BgL_locationz00_6223);
										}
									}
								}
								{	/* Cfa/type.scm 15 */
									int BgL_tmpz00_6538;

									BgL_tmpz00_6538 = (int) (BgL_iz00_6219);
									CNST_TABLE_SET(BgL_tmpz00_6538, BgL_arg2365z00_6221);
							}}
							{	/* Cfa/type.scm 15 */
								int BgL_auxz00_6224;

								BgL_auxz00_6224 = (int) ((BgL_iz00_6219 - 1L));
								{
									long BgL_iz00_6543;

									BgL_iz00_6543 = (long) (BgL_auxz00_6224);
									BgL_iz00_6219 = BgL_iz00_6543;
									goto BgL_loopz00_6220;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_typez00(void)
	{
		{	/* Cfa/type.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_typez00(void)
	{
		{	/* Cfa/type.scm 15 */
			return BUNSPEC;
		}

	}



/* type-settings! */
	BGL_EXPORTED_DEF obj_t BGl_typezd2settingsz12zc0zzcfa_typez00(obj_t
		BgL_globalsz00_23)
	{
		{	/* Cfa/type.scm 43 */
			BGl_typezd2closuresz12zc0zzcfa_closurez00();
			{
				obj_t BgL_l1609z00_3799;

				{	/* Cfa/type.scm 48 */
					bool_t BgL_tmpz00_6547;

					BgL_l1609z00_3799 = BgL_globalsz00_23;
				BgL_zc3z04anonymousza31749ze3z87_3800:
					if (PAIRP(BgL_l1609z00_3799))
						{	/* Cfa/type.scm 48 */
							{	/* Cfa/type.scm 48 */
								obj_t BgL_arg1751z00_3802;

								BgL_arg1751z00_3802 = CAR(BgL_l1609z00_3799);
								BGl_typezd2funz12zc0zzcfa_typez00(
									((BgL_variablez00_bglt) BgL_arg1751z00_3802));
							}
							{
								obj_t BgL_l1609z00_6553;

								BgL_l1609z00_6553 = CDR(BgL_l1609z00_3799);
								BgL_l1609z00_3799 = BgL_l1609z00_6553;
								goto BgL_zc3z04anonymousza31749ze3z87_3800;
							}
						}
					else
						{	/* Cfa/type.scm 48 */
							BgL_tmpz00_6547 = ((bool_t) 1);
						}
					return BBOOL(BgL_tmpz00_6547);
				}
			}
		}

	}



/* &type-settings! */
	obj_t BGl_z62typezd2settingsz12za2zzcfa_typez00(obj_t BgL_envz00_6079,
		obj_t BgL_globalsz00_6080)
	{
		{	/* Cfa/type.scm 43 */
			return BGl_typezd2settingsz12zc0zzcfa_typez00(BgL_globalsz00_6080);
		}

	}



/* type-fun! */
	obj_t BGl_typezd2funz12zc0zzcfa_typez00(BgL_variablez00_bglt BgL_varz00_24)
	{
		{	/* Cfa/type.scm 53 */
			{	/* Cfa/type.scm 54 */
				BgL_valuez00_bglt BgL_funz00_3805;

				BgL_funz00_3805 =
					(((BgL_variablez00_bglt) COBJECT(BgL_varz00_24))->BgL_valuez00);
				{	/* Cfa/type.scm 58 */
					bool_t BgL_test2425z00_6558;

					{	/* Cfa/type.scm 58 */
						obj_t BgL_classz00_4926;

						BgL_classz00_4926 = BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
						{	/* Cfa/type.scm 58 */
							BgL_objectz00_bglt BgL_arg1807z00_4928;

							{	/* Cfa/type.scm 58 */
								obj_t BgL_tmpz00_6559;

								BgL_tmpz00_6559 =
									((obj_t) ((BgL_objectz00_bglt) BgL_funz00_3805));
								BgL_arg1807z00_4928 = (BgL_objectz00_bglt) (BgL_tmpz00_6559);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/type.scm 58 */
									long BgL_idxz00_4934;

									BgL_idxz00_4934 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4928);
									BgL_test2425z00_6558 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_4934 + 4L)) == BgL_classz00_4926);
								}
							else
								{	/* Cfa/type.scm 58 */
									bool_t BgL_res2275z00_4959;

									{	/* Cfa/type.scm 58 */
										obj_t BgL_oclassz00_4942;

										{	/* Cfa/type.scm 58 */
											obj_t BgL_arg1815z00_4950;
											long BgL_arg1816z00_4951;

											BgL_arg1815z00_4950 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/type.scm 58 */
												long BgL_arg1817z00_4952;

												BgL_arg1817z00_4952 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4928);
												BgL_arg1816z00_4951 =
													(BgL_arg1817z00_4952 - OBJECT_TYPE);
											}
											BgL_oclassz00_4942 =
												VECTOR_REF(BgL_arg1815z00_4950, BgL_arg1816z00_4951);
										}
										{	/* Cfa/type.scm 58 */
											bool_t BgL__ortest_1115z00_4943;

											BgL__ortest_1115z00_4943 =
												(BgL_classz00_4926 == BgL_oclassz00_4942);
											if (BgL__ortest_1115z00_4943)
												{	/* Cfa/type.scm 58 */
													BgL_res2275z00_4959 = BgL__ortest_1115z00_4943;
												}
											else
												{	/* Cfa/type.scm 58 */
													long BgL_odepthz00_4944;

													{	/* Cfa/type.scm 58 */
														obj_t BgL_arg1804z00_4945;

														BgL_arg1804z00_4945 = (BgL_oclassz00_4942);
														BgL_odepthz00_4944 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_4945);
													}
													if ((4L < BgL_odepthz00_4944))
														{	/* Cfa/type.scm 58 */
															obj_t BgL_arg1802z00_4947;

															{	/* Cfa/type.scm 58 */
																obj_t BgL_arg1803z00_4948;

																BgL_arg1803z00_4948 = (BgL_oclassz00_4942);
																BgL_arg1802z00_4947 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_4948,
																	4L);
															}
															BgL_res2275z00_4959 =
																(BgL_arg1802z00_4947 == BgL_classz00_4926);
														}
													else
														{	/* Cfa/type.scm 58 */
															BgL_res2275z00_4959 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2425z00_6558 = BgL_res2275z00_4959;
								}
						}
					}
					if (BgL_test2425z00_6558)
						{	/* Cfa/type.scm 58 */
							{	/* Cfa/type.scm 63 */
								obj_t BgL_g1613z00_3808;

								BgL_g1613z00_3808 =
									(((BgL_sfunz00_bglt) COBJECT(
											((BgL_sfunz00_bglt)
												((BgL_sfunz00_bglt) BgL_funz00_3805))))->BgL_argsz00);
								{
									obj_t BgL_l1611z00_3810;

									BgL_l1611z00_3810 = BgL_g1613z00_3808;
								BgL_zc3z04anonymousza31754ze3z87_3811:
									if (PAIRP(BgL_l1611z00_3810))
										{	/* Cfa/type.scm 63 */
											{	/* Cfa/type.scm 65 */
												obj_t BgL_varz00_3813;

												BgL_varz00_3813 = CAR(BgL_l1611z00_3810);
												{	/* Cfa/type.scm 66 */
													BgL_valuez00_bglt BgL_arg1761z00_3814;

													BgL_arg1761z00_3814 =
														(((BgL_variablez00_bglt) COBJECT(
																((BgL_variablez00_bglt)
																	((BgL_localz00_bglt) BgL_varz00_3813))))->
														BgL_valuez00);
													BGl_typezd2variablez12zc0zzcfa_typez00
														(BgL_arg1761z00_3814,
														((BgL_variablez00_bglt) BgL_varz00_3813));
												}
											}
											{
												obj_t BgL_l1611z00_6593;

												BgL_l1611z00_6593 = CDR(BgL_l1611z00_3810);
												BgL_l1611z00_3810 = BgL_l1611z00_6593;
												goto BgL_zc3z04anonymousza31754ze3z87_3811;
											}
										}
									else
										{	/* Cfa/type.scm 63 */
											((bool_t) 1);
										}
								}
							}
							{	/* Cfa/type.scm 69 */
								obj_t BgL_arg1765z00_3817;

								{	/* Cfa/type.scm 69 */
									BgL_approxz00_bglt BgL_arg1767z00_3818;

									{
										BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_6595;

										{
											obj_t BgL_auxz00_6596;

											{	/* Cfa/type.scm 69 */
												BgL_objectz00_bglt BgL_tmpz00_6597;

												BgL_tmpz00_6597 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_funz00_3805));
												BgL_auxz00_6596 = BGL_OBJECT_WIDENING(BgL_tmpz00_6597);
											}
											BgL_auxz00_6595 =
												((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_6596);
										}
										BgL_arg1767z00_3818 =
											(((BgL_internzd2sfunzf2cinfoz20_bglt)
												COBJECT(BgL_auxz00_6595))->BgL_approxz00);
									}
									BgL_arg1765z00_3817 =
										BGl_getzd2approxzd2typez00zzcfa_typez00(BgL_arg1767z00_3818,
										((obj_t) BgL_varz00_24));
								}
								BGl_setzd2variablezd2typez12z12zzcfa_typez00(BgL_varz00_24,
									((BgL_typez00_bglt) BgL_arg1765z00_3817));
							}
							{	/* Cfa/type.scm 70 */
								long BgL_arg1770z00_3820;

								{	/* Cfa/type.scm 70 */
									obj_t BgL_arg1771z00_3821;

									{	/* Cfa/type.scm 70 */
										obj_t BgL_arg1773z00_3822;

										{	/* Cfa/type.scm 70 */
											obj_t BgL_arg1815z00_4965;
											long BgL_arg1816z00_4966;

											BgL_arg1815z00_4965 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/type.scm 70 */
												long BgL_arg1817z00_4967;

												BgL_arg1817z00_4967 =
													BGL_OBJECT_CLASS_NUM(
													((BgL_objectz00_bglt) BgL_funz00_3805));
												BgL_arg1816z00_4966 =
													(BgL_arg1817z00_4967 - OBJECT_TYPE);
											}
											BgL_arg1773z00_3822 =
												VECTOR_REF(BgL_arg1815z00_4965, BgL_arg1816z00_4966);
										}
										BgL_arg1771z00_3821 =
											BGl_classzd2superzd2zz__objectz00(BgL_arg1773z00_3822);
									}
									{	/* Cfa/type.scm 70 */
										obj_t BgL_tmpz00_6613;

										BgL_tmpz00_6613 = ((obj_t) BgL_arg1771z00_3821);
										BgL_arg1770z00_3820 = BGL_CLASS_NUM(BgL_tmpz00_6613);
								}}
								BGL_OBJECT_CLASS_NUM_SET(
									((BgL_objectz00_bglt) BgL_funz00_3805), BgL_arg1770z00_3820);
							}
							{	/* Cfa/type.scm 70 */
								BgL_objectz00_bglt BgL_tmpz00_6618;

								BgL_tmpz00_6618 = ((BgL_objectz00_bglt) BgL_funz00_3805);
								BGL_OBJECT_WIDENING_SET(BgL_tmpz00_6618, BFALSE);
							}
							((BgL_objectz00_bglt) BgL_funz00_3805);
							BgL_funz00_3805;
							{
								obj_t BgL_auxz00_6622;

								{	/* Cfa/type.scm 72 */
									obj_t BgL_arg1775z00_3823;

									BgL_arg1775z00_3823 =
										(((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													((BgL_sfunz00_bglt) BgL_funz00_3805))))->BgL_bodyz00);
									BgL_auxz00_6622 =
										((obj_t)
										BGl_typezd2nodez12zc0zzcfa_typez00(
											((BgL_nodez00_bglt) BgL_arg1775z00_3823)));
								}
								return
									((((BgL_sfunz00_bglt) COBJECT(
												((BgL_sfunz00_bglt)
													((BgL_sfunz00_bglt) BgL_funz00_3805))))->
										BgL_bodyz00) = ((obj_t) BgL_auxz00_6622), BUNSPEC);
							}
						}
					else
						{	/* Cfa/type.scm 73 */
							bool_t BgL_test2430z00_6632;

							{	/* Cfa/type.scm 73 */
								obj_t BgL_classz00_4977;

								BgL_classz00_4977 = BGl_sfunz00zzast_varz00;
								{	/* Cfa/type.scm 73 */
									BgL_objectz00_bglt BgL_arg1807z00_4979;

									{	/* Cfa/type.scm 73 */
										obj_t BgL_tmpz00_6633;

										BgL_tmpz00_6633 =
											((obj_t) ((BgL_objectz00_bglt) BgL_funz00_3805));
										BgL_arg1807z00_4979 =
											(BgL_objectz00_bglt) (BgL_tmpz00_6633);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/type.scm 73 */
											long BgL_idxz00_4985;

											BgL_idxz00_4985 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_4979);
											BgL_test2430z00_6632 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_4985 + 3L)) == BgL_classz00_4977);
										}
									else
										{	/* Cfa/type.scm 73 */
											bool_t BgL_res2276z00_5010;

											{	/* Cfa/type.scm 73 */
												obj_t BgL_oclassz00_4993;

												{	/* Cfa/type.scm 73 */
													obj_t BgL_arg1815z00_5001;
													long BgL_arg1816z00_5002;

													BgL_arg1815z00_5001 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/type.scm 73 */
														long BgL_arg1817z00_5003;

														BgL_arg1817z00_5003 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_4979);
														BgL_arg1816z00_5002 =
															(BgL_arg1817z00_5003 - OBJECT_TYPE);
													}
													BgL_oclassz00_4993 =
														VECTOR_REF(BgL_arg1815z00_5001,
														BgL_arg1816z00_5002);
												}
												{	/* Cfa/type.scm 73 */
													bool_t BgL__ortest_1115z00_4994;

													BgL__ortest_1115z00_4994 =
														(BgL_classz00_4977 == BgL_oclassz00_4993);
													if (BgL__ortest_1115z00_4994)
														{	/* Cfa/type.scm 73 */
															BgL_res2276z00_5010 = BgL__ortest_1115z00_4994;
														}
													else
														{	/* Cfa/type.scm 73 */
															long BgL_odepthz00_4995;

															{	/* Cfa/type.scm 73 */
																obj_t BgL_arg1804z00_4996;

																BgL_arg1804z00_4996 = (BgL_oclassz00_4993);
																BgL_odepthz00_4995 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_4996);
															}
															if ((3L < BgL_odepthz00_4995))
																{	/* Cfa/type.scm 73 */
																	obj_t BgL_arg1802z00_4998;

																	{	/* Cfa/type.scm 73 */
																		obj_t BgL_arg1803z00_4999;

																		BgL_arg1803z00_4999 = (BgL_oclassz00_4993);
																		BgL_arg1802z00_4998 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_4999, 3L);
																	}
																	BgL_res2276z00_5010 =
																		(BgL_arg1802z00_4998 == BgL_classz00_4977);
																}
															else
																{	/* Cfa/type.scm 73 */
																	BgL_res2276z00_5010 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2430z00_6632 = BgL_res2276z00_5010;
										}
								}
							}
							if (BgL_test2430z00_6632)
								{
									obj_t BgL_auxz00_6656;

									{	/* Cfa/type.scm 75 */
										obj_t BgL_arg1798z00_3826;

										BgL_arg1798z00_3826 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_3805)))->BgL_bodyz00);
										BgL_auxz00_6656 =
											((obj_t)
											BGl_typezd2nodez12zc0zzcfa_typez00(
												((BgL_nodez00_bglt) BgL_arg1798z00_3826)));
									}
									return
										((((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_funz00_3805)))->BgL_bodyz00) =
										((obj_t) BgL_auxz00_6656), BUNSPEC);
								}
							else
								{	/* Cfa/type.scm 77 */
									obj_t BgL_arg1799z00_3827;

									BgL_arg1799z00_3827 =
										BGl_shapez00zztools_shapez00(((obj_t) BgL_varz00_24));
									return
										BGl_internalzd2errorzd2zztools_errorz00
										(BGl_string2313z00zzcfa_typez00,
										BGl_string2314z00zzcfa_typez00, BgL_arg1799z00_3827);
								}
						}
				}
			}
		}

	}



/* type-fun-node! */
	obj_t BGl_typezd2funzd2nodez12z12zzcfa_typez00(BgL_varz00_bglt BgL_vz00_25)
	{
		{	/* Cfa/type.scm 82 */
			{	/* Cfa/type.scm 83 */
				BgL_variablez00_bglt BgL_varz00_3828;

				BgL_varz00_3828 =
					(((BgL_varz00_bglt) COBJECT(BgL_vz00_25))->BgL_variablez00);
				{	/* Cfa/type.scm 83 */
					BgL_valuez00_bglt BgL_funz00_3829;

					BgL_funz00_3829 =
						(((BgL_variablez00_bglt) COBJECT(BgL_varz00_3828))->BgL_valuez00);
					{	/* Cfa/type.scm 84 */

						{	/* Cfa/type.scm 85 */
							bool_t BgL_test2434z00_6669;

							{	/* Cfa/type.scm 85 */
								obj_t BgL_classz00_5013;

								BgL_classz00_5013 = BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
								{	/* Cfa/type.scm 85 */
									BgL_objectz00_bglt BgL_arg1807z00_5015;

									{	/* Cfa/type.scm 85 */
										obj_t BgL_tmpz00_6670;

										BgL_tmpz00_6670 =
											((obj_t) ((BgL_objectz00_bglt) BgL_funz00_3829));
										BgL_arg1807z00_5015 =
											(BgL_objectz00_bglt) (BgL_tmpz00_6670);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/type.scm 85 */
											long BgL_idxz00_5021;

											BgL_idxz00_5021 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5015);
											BgL_test2434z00_6669 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_5021 + 4L)) == BgL_classz00_5013);
										}
									else
										{	/* Cfa/type.scm 85 */
											bool_t BgL_res2277z00_5046;

											{	/* Cfa/type.scm 85 */
												obj_t BgL_oclassz00_5029;

												{	/* Cfa/type.scm 85 */
													obj_t BgL_arg1815z00_5037;
													long BgL_arg1816z00_5038;

													BgL_arg1815z00_5037 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/type.scm 85 */
														long BgL_arg1817z00_5039;

														BgL_arg1817z00_5039 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5015);
														BgL_arg1816z00_5038 =
															(BgL_arg1817z00_5039 - OBJECT_TYPE);
													}
													BgL_oclassz00_5029 =
														VECTOR_REF(BgL_arg1815z00_5037,
														BgL_arg1816z00_5038);
												}
												{	/* Cfa/type.scm 85 */
													bool_t BgL__ortest_1115z00_5030;

													BgL__ortest_1115z00_5030 =
														(BgL_classz00_5013 == BgL_oclassz00_5029);
													if (BgL__ortest_1115z00_5030)
														{	/* Cfa/type.scm 85 */
															BgL_res2277z00_5046 = BgL__ortest_1115z00_5030;
														}
													else
														{	/* Cfa/type.scm 85 */
															long BgL_odepthz00_5031;

															{	/* Cfa/type.scm 85 */
																obj_t BgL_arg1804z00_5032;

																BgL_arg1804z00_5032 = (BgL_oclassz00_5029);
																BgL_odepthz00_5031 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_5032);
															}
															if ((4L < BgL_odepthz00_5031))
																{	/* Cfa/type.scm 85 */
																	obj_t BgL_arg1802z00_5034;

																	{	/* Cfa/type.scm 85 */
																		obj_t BgL_arg1803z00_5035;

																		BgL_arg1803z00_5035 = (BgL_oclassz00_5029);
																		BgL_arg1802z00_5034 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_5035, 4L);
																	}
																	BgL_res2277z00_5046 =
																		(BgL_arg1802z00_5034 == BgL_classz00_5013);
																}
															else
																{	/* Cfa/type.scm 85 */
																	BgL_res2277z00_5046 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2434z00_6669 = BgL_res2277z00_5046;
										}
								}
							}
							if (BgL_test2434z00_6669)
								{	/* Cfa/type.scm 87 */
									obj_t BgL_arg1805z00_3832;

									{	/* Cfa/type.scm 87 */
										BgL_approxz00_bglt BgL_arg1806z00_3833;

										{
											BgL_internzd2sfunzf2cinfoz20_bglt BgL_auxz00_6693;

											{
												obj_t BgL_auxz00_6694;

												{	/* Cfa/type.scm 87 */
													BgL_objectz00_bglt BgL_tmpz00_6695;

													BgL_tmpz00_6695 =
														((BgL_objectz00_bglt)
														((BgL_sfunz00_bglt) BgL_funz00_3829));
													BgL_auxz00_6694 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_6695);
												}
												BgL_auxz00_6693 =
													((BgL_internzd2sfunzf2cinfoz20_bglt) BgL_auxz00_6694);
											}
											BgL_arg1806z00_3833 =
												(((BgL_internzd2sfunzf2cinfoz20_bglt)
													COBJECT(BgL_auxz00_6693))->BgL_approxz00);
										}
										BgL_arg1805z00_3832 =
											BGl_getzd2approxzd2typez00zzcfa_typez00
											(BgL_arg1806z00_3833, ((obj_t) BgL_vz00_25));
									}
									return
										BGl_setzd2variablezd2typez12z12zzcfa_typez00
										(BgL_varz00_3828, ((BgL_typez00_bglt) BgL_arg1805z00_3832));
								}
							else
								{	/* Cfa/type.scm 85 */
									return BFALSE;
								}
						}
					}
				}
			}
		}

	}



/* get-approx-type */
	BGL_EXPORTED_DEF obj_t
		BGl_getzd2approxzd2typez00zzcfa_typez00(BgL_approxz00_bglt BgL_approxz00_26,
		obj_t BgL_nodez00_27)
	{
		{	/* Cfa/type.scm 92 */
			{	/* Cfa/type.scm 93 */
				BgL_typez00_bglt BgL_typez00_3834;
				obj_t BgL_allocsz00_3835;

				BgL_typez00_3834 =
					(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_26))->BgL_typez00);
				BgL_allocsz00_3835 =
					(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_26))->BgL_allocsz00);
				{	/* Cfa/type.scm 96 */
					bool_t BgL_test2438z00_6707;

					{	/* Cfa/type.scm 96 */
						obj_t BgL_arg1820z00_3858;

						BgL_arg1820z00_3858 =
							BGl_setzd2lengthzd2zzcfa_setz00(BgL_allocsz00_3835);
						BgL_test2438z00_6707 = ((long) CINT(BgL_arg1820z00_3858) == 0L);
					}
					if (BgL_test2438z00_6707)
						{	/* Cfa/type.scm 96 */
							return ((obj_t) BgL_typez00_3834);
						}
					else
						{	/* Cfa/type.scm 96 */
							if (
								(((BgL_approxz00_bglt) COBJECT(BgL_approxz00_26))->
									BgL_topzf3zf3))
								{	/* Cfa/type.scm 98 */
									return BGl_za2objza2z00zztype_cachez00;
								}
							else
								{	/* Cfa/type.scm 98 */
									if (CBOOL(BGl_setzd2everyzd2zzcfa_setz00
											(BGl_makezd2vectorzd2appzf3zd2envz21zzcfa_info2z00,
												BgL_allocsz00_3835)))
										{	/* Cfa/type.scm 101 */
											obj_t BgL_appz00_3840;

											{	/* Cfa/type.scm 101 */

												BgL_appz00_3840 =
													BGl_setzd2headzd2zzcfa_setz00(BgL_allocsz00_3835,
													BFALSE);
											}
											{	/* Cfa/type.scm 101 */
												BgL_typez00_bglt BgL_tvzd2typezd2_3841;

												BgL_tvzd2typezd2_3841 =
													BGl_getzd2vectorzd2itemzd2typezd2zzcfa_tvectorz00(
													((BgL_nodez00_bglt) BgL_appz00_3840));
												{	/* Cfa/type.scm 102 */
													BgL_approxz00_bglt BgL_valuezd2approxzd2_3842;

													{
														BgL_makezd2vectorzd2appz00_bglt BgL_auxz00_6720;

														{
															obj_t BgL_auxz00_6721;

															{	/* Cfa/type.scm 103 */
																BgL_objectz00_bglt BgL_tmpz00_6722;

																BgL_tmpz00_6722 =
																	((BgL_objectz00_bglt)
																	((BgL_appz00_bglt) BgL_appz00_3840));
																BgL_auxz00_6721 =
																	BGL_OBJECT_WIDENING(BgL_tmpz00_6722);
															}
															BgL_auxz00_6720 =
																((BgL_makezd2vectorzd2appz00_bglt)
																BgL_auxz00_6721);
														}
														BgL_valuezd2approxzd2_3842 =
															(((BgL_makezd2vectorzd2appz00_bglt)
																COBJECT(BgL_auxz00_6720))->
															BgL_valuezd2approxzd2);
													}
													{	/* Cfa/type.scm 103 */
														BgL_typez00_bglt BgL_itemzd2typezd2_3843;

														BgL_itemzd2typezd2_3843 =
															(((BgL_approxz00_bglt)
																COBJECT(BgL_valuezd2approxzd2_3842))->
															BgL_typez00);
														{	/* Cfa/type.scm 104 */
															obj_t BgL_tvz00_3844;

															BgL_tvz00_3844 =
																(((BgL_typez00_bglt)
																	COBJECT(BgL_itemzd2typezd2_3843))->
																BgL_tvectorz00);
															{	/* Cfa/type.scm 105 */

																{	/* Cfa/type.scm 107 */
																	bool_t BgL_test2441z00_6730;

																	{	/* Cfa/type.scm 107 */
																		obj_t BgL_classz00_5056;

																		BgL_classz00_5056 =
																			BGl_typez00zztype_typez00;
																		if (BGL_OBJECTP(BgL_tvz00_3844))
																			{	/* Cfa/type.scm 107 */
																				BgL_objectz00_bglt BgL_arg1807z00_5058;

																				BgL_arg1807z00_5058 =
																					(BgL_objectz00_bglt) (BgL_tvz00_3844);
																				if (BGL_CONDEXPAND_ISA_ARCH64())
																					{	/* Cfa/type.scm 107 */
																						long BgL_idxz00_5064;

																						BgL_idxz00_5064 =
																							BGL_OBJECT_INHERITANCE_NUM
																							(BgL_arg1807z00_5058);
																						BgL_test2441z00_6730 =
																							(VECTOR_REF
																							(BGl_za2inheritancesza2z00zz__objectz00,
																								(BgL_idxz00_5064 + 1L)) ==
																							BgL_classz00_5056);
																					}
																				else
																					{	/* Cfa/type.scm 107 */
																						bool_t BgL_res2278z00_5089;

																						{	/* Cfa/type.scm 107 */
																							obj_t BgL_oclassz00_5072;

																							{	/* Cfa/type.scm 107 */
																								obj_t BgL_arg1815z00_5080;
																								long BgL_arg1816z00_5081;

																								BgL_arg1815z00_5080 =
																									(BGl_za2classesza2z00zz__objectz00);
																								{	/* Cfa/type.scm 107 */
																									long BgL_arg1817z00_5082;

																									BgL_arg1817z00_5082 =
																										BGL_OBJECT_CLASS_NUM
																										(BgL_arg1807z00_5058);
																									BgL_arg1816z00_5081 =
																										(BgL_arg1817z00_5082 -
																										OBJECT_TYPE);
																								}
																								BgL_oclassz00_5072 =
																									VECTOR_REF
																									(BgL_arg1815z00_5080,
																									BgL_arg1816z00_5081);
																							}
																							{	/* Cfa/type.scm 107 */
																								bool_t BgL__ortest_1115z00_5073;

																								BgL__ortest_1115z00_5073 =
																									(BgL_classz00_5056 ==
																									BgL_oclassz00_5072);
																								if (BgL__ortest_1115z00_5073)
																									{	/* Cfa/type.scm 107 */
																										BgL_res2278z00_5089 =
																											BgL__ortest_1115z00_5073;
																									}
																								else
																									{	/* Cfa/type.scm 107 */
																										long BgL_odepthz00_5074;

																										{	/* Cfa/type.scm 107 */
																											obj_t BgL_arg1804z00_5075;

																											BgL_arg1804z00_5075 =
																												(BgL_oclassz00_5072);
																											BgL_odepthz00_5074 =
																												BGL_CLASS_DEPTH
																												(BgL_arg1804z00_5075);
																										}
																										if (
																											(1L < BgL_odepthz00_5074))
																											{	/* Cfa/type.scm 107 */
																												obj_t
																													BgL_arg1802z00_5077;
																												{	/* Cfa/type.scm 107 */
																													obj_t
																														BgL_arg1803z00_5078;
																													BgL_arg1803z00_5078 =
																														(BgL_oclassz00_5072);
																													BgL_arg1802z00_5077 =
																														BGL_CLASS_ANCESTORS_REF
																														(BgL_arg1803z00_5078,
																														1L);
																												}
																												BgL_res2278z00_5089 =
																													(BgL_arg1802z00_5077
																													== BgL_classz00_5056);
																											}
																										else
																											{	/* Cfa/type.scm 107 */
																												BgL_res2278z00_5089 =
																													((bool_t) 0);
																											}
																									}
																							}
																						}
																						BgL_test2441z00_6730 =
																							BgL_res2278z00_5089;
																					}
																			}
																		else
																			{	/* Cfa/type.scm 107 */
																				BgL_test2441z00_6730 = ((bool_t) 0);
																			}
																	}
																	if (BgL_test2441z00_6730)
																		{	/* Cfa/type.scm 107 */
																			return BgL_tvz00_3844;
																		}
																	else
																		{	/* Cfa/type.scm 107 */
																			if (
																				(((obj_t) BgL_typez00_3834) ==
																					BGl_za2_za2z00zztype_cachez00))
																				{	/* Cfa/type.scm 108 */
																					return
																						BGl_za2vectorza2z00zztype_cachez00;
																				}
																			else
																				{	/* Cfa/type.scm 108 */
																					return ((obj_t) BgL_typez00_3834);
																				}
																		}
																}
															}
														}
													}
												}
											}
										}
									else
										{	/* Cfa/type.scm 100 */
											if (CBOOL(BGl_setzd2everyzd2zzcfa_setz00
													(BGl_valloczf2Cinfozb2optimzf3zd2envz61zzcfa_info3z00,
														BgL_allocsz00_3835)))
												{	/* Cfa/type.scm 110 */
													if (CBOOL
														(BGl_tvectorzd2optimiza7ationzf3z86zzcfa_tvectorz00
															()))
														{	/* Cfa/type.scm 115 */
															obj_t BgL_appz00_3850;

															{	/* Cfa/type.scm 115 */

																BgL_appz00_3850 =
																	BGl_setzd2headzd2zzcfa_setz00
																	(BgL_allocsz00_3835, BFALSE);
															}
															{	/* Cfa/type.scm 115 */
																BgL_typez00_bglt BgL_tvzd2typezd2_3851;

																BgL_tvzd2typezd2_3851 =
																	BGl_getzd2vectorzd2itemzd2typezd2zzcfa_tvectorz00
																	(((BgL_nodez00_bglt) BgL_appz00_3850));
																{	/* Cfa/type.scm 116 */
																	BgL_approxz00_bglt BgL_valuezd2approxzd2_3852;

																	{
																		BgL_valloczf2cinfozb2optimz40_bglt
																			BgL_auxz00_6766;
																		{
																			obj_t BgL_auxz00_6767;

																			{	/* Cfa/type.scm 117 */
																				BgL_objectz00_bglt BgL_tmpz00_6768;

																				BgL_tmpz00_6768 =
																					((BgL_objectz00_bglt)
																					((BgL_vallocz00_bglt)
																						BgL_appz00_3850));
																				BgL_auxz00_6767 =
																					BGL_OBJECT_WIDENING(BgL_tmpz00_6768);
																			}
																			BgL_auxz00_6766 =
																				((BgL_valloczf2cinfozb2optimz40_bglt)
																				BgL_auxz00_6767);
																		}
																		BgL_valuezd2approxzd2_3852 =
																			(((BgL_valloczf2cinfozb2optimz40_bglt)
																				COBJECT(BgL_auxz00_6766))->
																			BgL_valuezd2approxzd2);
																	}
																	{	/* Cfa/type.scm 117 */
																		BgL_typez00_bglt BgL_itemzd2typezd2_3853;

																		BgL_itemzd2typezd2_3853 =
																			(((BgL_approxz00_bglt)
																				COBJECT(BgL_valuezd2approxzd2_3852))->
																			BgL_typez00);
																		{	/* Cfa/type.scm 118 */
																			obj_t BgL_tvz00_3854;

																			BgL_tvz00_3854 =
																				(((BgL_typez00_bglt)
																					COBJECT(BgL_itemzd2typezd2_3853))->
																				BgL_tvectorz00);
																			{	/* Cfa/type.scm 119 */

																				{	/* Cfa/type.scm 121 */
																					bool_t BgL_test2449z00_6776;

																					{	/* Cfa/type.scm 121 */
																						obj_t BgL_classz00_5094;

																						BgL_classz00_5094 =
																							BGl_typez00zztype_typez00;
																						if (BGL_OBJECTP(BgL_tvz00_3854))
																							{	/* Cfa/type.scm 121 */
																								BgL_objectz00_bglt
																									BgL_arg1807z00_5096;
																								BgL_arg1807z00_5096 =
																									(BgL_objectz00_bglt)
																									(BgL_tvz00_3854);
																								if (BGL_CONDEXPAND_ISA_ARCH64())
																									{	/* Cfa/type.scm 121 */
																										long BgL_idxz00_5102;

																										BgL_idxz00_5102 =
																											BGL_OBJECT_INHERITANCE_NUM
																											(BgL_arg1807z00_5096);
																										BgL_test2449z00_6776 =
																											(VECTOR_REF
																											(BGl_za2inheritancesza2z00zz__objectz00,
																												(BgL_idxz00_5102 +
																													1L)) ==
																											BgL_classz00_5094);
																									}
																								else
																									{	/* Cfa/type.scm 121 */
																										bool_t BgL_res2279z00_5127;

																										{	/* Cfa/type.scm 121 */
																											obj_t BgL_oclassz00_5110;

																											{	/* Cfa/type.scm 121 */
																												obj_t
																													BgL_arg1815z00_5118;
																												long
																													BgL_arg1816z00_5119;
																												BgL_arg1815z00_5118 =
																													(BGl_za2classesza2z00zz__objectz00);
																												{	/* Cfa/type.scm 121 */
																													long
																														BgL_arg1817z00_5120;
																													BgL_arg1817z00_5120 =
																														BGL_OBJECT_CLASS_NUM
																														(BgL_arg1807z00_5096);
																													BgL_arg1816z00_5119 =
																														(BgL_arg1817z00_5120
																														- OBJECT_TYPE);
																												}
																												BgL_oclassz00_5110 =
																													VECTOR_REF
																													(BgL_arg1815z00_5118,
																													BgL_arg1816z00_5119);
																											}
																											{	/* Cfa/type.scm 121 */
																												bool_t
																													BgL__ortest_1115z00_5111;
																												BgL__ortest_1115z00_5111
																													=
																													(BgL_classz00_5094 ==
																													BgL_oclassz00_5110);
																												if (BgL__ortest_1115z00_5111)
																													{	/* Cfa/type.scm 121 */
																														BgL_res2279z00_5127
																															=
																															BgL__ortest_1115z00_5111;
																													}
																												else
																													{	/* Cfa/type.scm 121 */
																														long
																															BgL_odepthz00_5112;
																														{	/* Cfa/type.scm 121 */
																															obj_t
																																BgL_arg1804z00_5113;
																															BgL_arg1804z00_5113
																																=
																																(BgL_oclassz00_5110);
																															BgL_odepthz00_5112
																																=
																																BGL_CLASS_DEPTH
																																(BgL_arg1804z00_5113);
																														}
																														if (
																															(1L <
																																BgL_odepthz00_5112))
																															{	/* Cfa/type.scm 121 */
																																obj_t
																																	BgL_arg1802z00_5115;
																																{	/* Cfa/type.scm 121 */
																																	obj_t
																																		BgL_arg1803z00_5116;
																																	BgL_arg1803z00_5116
																																		=
																																		(BgL_oclassz00_5110);
																																	BgL_arg1802z00_5115
																																		=
																																		BGL_CLASS_ANCESTORS_REF
																																		(BgL_arg1803z00_5116,
																																		1L);
																																}
																																BgL_res2279z00_5127
																																	=
																																	(BgL_arg1802z00_5115
																																	==
																																	BgL_classz00_5094);
																															}
																														else
																															{	/* Cfa/type.scm 121 */
																																BgL_res2279z00_5127
																																	=
																																	((bool_t) 0);
																															}
																													}
																											}
																										}
																										BgL_test2449z00_6776 =
																											BgL_res2279z00_5127;
																									}
																							}
																						else
																							{	/* Cfa/type.scm 121 */
																								BgL_test2449z00_6776 =
																									((bool_t) 0);
																							}
																					}
																					if (BgL_test2449z00_6776)
																						{	/* Cfa/type.scm 121 */
																							return BgL_tvz00_3854;
																						}
																					else
																						{	/* Cfa/type.scm 121 */
																							if (
																								(((obj_t) BgL_typez00_3834) ==
																									BGl_za2_za2z00zztype_cachez00))
																								{	/* Cfa/type.scm 122 */
																									return
																										BGl_za2vectorza2z00zztype_cachez00;
																								}
																							else
																								{	/* Cfa/type.scm 122 */
																									return
																										((obj_t) BgL_typez00_3834);
																								}
																						}
																				}
																			}
																		}
																	}
																}
															}
														}
													else
														{	/* Cfa/type.scm 111 */
															if (
																(((obj_t) BgL_typez00_3834) ==
																	BGl_za2_za2z00zztype_cachez00))
																{	/* Cfa/type.scm 112 */
																	return BGl_za2vectorza2z00zztype_cachez00;
																}
															else
																{	/* Cfa/type.scm 112 */
																	return ((obj_t) BgL_typez00_3834);
																}
														}
												}
											else
												{	/* Cfa/type.scm 110 */
													return ((obj_t) BgL_typez00_3834);
												}
										}
								}
						}
				}
			}
		}

	}



/* &get-approx-type */
	obj_t BGl_z62getzd2approxzd2typez62zzcfa_typez00(obj_t BgL_envz00_6081,
		obj_t BgL_approxz00_6082, obj_t BgL_nodez00_6083)
	{
		{	/* Cfa/type.scm 92 */
			return
				BGl_getzd2approxzd2typez00zzcfa_typez00(
				((BgL_approxz00_bglt) BgL_approxz00_6082), BgL_nodez00_6083);
		}

	}



/* set-variable-type! */
	obj_t BGl_setzd2variablezd2typez12z12zzcfa_typez00(BgL_variablez00_bglt
		BgL_variablez00_40, BgL_typez00_bglt BgL_typez00_41)
	{
		{	/* Cfa/type.scm 185 */
			{	/* Cfa/type.scm 186 */
				obj_t BgL_ntypez00_3859;
				BgL_typez00_bglt BgL_otypez00_3860;

				if ((((obj_t) BgL_typez00_41) == BGl_za2_za2z00zztype_cachez00))
					{	/* Cfa/type.scm 186 */
						BgL_ntypez00_3859 = BGl_za2objza2z00zztype_cachez00;
					}
				else
					{	/* Cfa/type.scm 186 */
						BgL_ntypez00_3859 = ((obj_t) BgL_typez00_41);
					}
				BgL_otypez00_3860 =
					(((BgL_variablez00_bglt) COBJECT(BgL_variablez00_40))->BgL_typez00);
				if ((((obj_t) BgL_otypez00_3860) == BGl_za2_za2z00zztype_cachez00))
					{	/* Cfa/type.scm 190 */
						bool_t BgL_test2458z00_6818;

						{	/* Cfa/type.scm 190 */
							bool_t BgL_test2459z00_6819;

							{	/* Cfa/type.scm 190 */
								obj_t BgL_classz00_5129;

								BgL_classz00_5129 = BGl_globalz00zzast_varz00;
								{	/* Cfa/type.scm 190 */
									BgL_objectz00_bglt BgL_arg1807z00_5131;

									{	/* Cfa/type.scm 190 */
										obj_t BgL_tmpz00_6820;

										BgL_tmpz00_6820 = ((obj_t) BgL_variablez00_40);
										BgL_arg1807z00_5131 =
											(BgL_objectz00_bglt) (BgL_tmpz00_6820);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/type.scm 190 */
											long BgL_idxz00_5137;

											BgL_idxz00_5137 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5131);
											BgL_test2459z00_6819 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_5137 + 2L)) == BgL_classz00_5129);
										}
									else
										{	/* Cfa/type.scm 190 */
											bool_t BgL_res2280z00_5162;

											{	/* Cfa/type.scm 190 */
												obj_t BgL_oclassz00_5145;

												{	/* Cfa/type.scm 190 */
													obj_t BgL_arg1815z00_5153;
													long BgL_arg1816z00_5154;

													BgL_arg1815z00_5153 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/type.scm 190 */
														long BgL_arg1817z00_5155;

														BgL_arg1817z00_5155 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5131);
														BgL_arg1816z00_5154 =
															(BgL_arg1817z00_5155 - OBJECT_TYPE);
													}
													BgL_oclassz00_5145 =
														VECTOR_REF(BgL_arg1815z00_5153,
														BgL_arg1816z00_5154);
												}
												{	/* Cfa/type.scm 190 */
													bool_t BgL__ortest_1115z00_5146;

													BgL__ortest_1115z00_5146 =
														(BgL_classz00_5129 == BgL_oclassz00_5145);
													if (BgL__ortest_1115z00_5146)
														{	/* Cfa/type.scm 190 */
															BgL_res2280z00_5162 = BgL__ortest_1115z00_5146;
														}
													else
														{	/* Cfa/type.scm 190 */
															long BgL_odepthz00_5147;

															{	/* Cfa/type.scm 190 */
																obj_t BgL_arg1804z00_5148;

																BgL_arg1804z00_5148 = (BgL_oclassz00_5145);
																BgL_odepthz00_5147 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_5148);
															}
															if ((2L < BgL_odepthz00_5147))
																{	/* Cfa/type.scm 190 */
																	obj_t BgL_arg1802z00_5150;

																	{	/* Cfa/type.scm 190 */
																		obj_t BgL_arg1803z00_5151;

																		BgL_arg1803z00_5151 = (BgL_oclassz00_5145);
																		BgL_arg1802z00_5150 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_5151, 2L);
																	}
																	BgL_res2280z00_5162 =
																		(BgL_arg1802z00_5150 == BgL_classz00_5129);
																}
															else
																{	/* Cfa/type.scm 190 */
																	BgL_res2280z00_5162 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2459z00_6819 = BgL_res2280z00_5162;
										}
								}
							}
							if (BgL_test2459z00_6819)
								{	/* Cfa/type.scm 191 */
									bool_t BgL_test2463z00_6842;

									{	/* Cfa/type.scm 191 */
										BgL_valuez00_bglt BgL_arg1843z00_3879;

										BgL_arg1843z00_3879 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_globalz00_bglt) BgL_variablez00_40))))->
											BgL_valuez00);
										{	/* Cfa/type.scm 191 */
											obj_t BgL_classz00_5164;

											BgL_classz00_5164 = BGl_funz00zzast_varz00;
											{	/* Cfa/type.scm 191 */
												BgL_objectz00_bglt BgL_arg1807z00_5166;

												{	/* Cfa/type.scm 191 */
													obj_t BgL_tmpz00_6846;

													BgL_tmpz00_6846 =
														((obj_t)
														((BgL_objectz00_bglt) BgL_arg1843z00_3879));
													BgL_arg1807z00_5166 =
														(BgL_objectz00_bglt) (BgL_tmpz00_6846);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Cfa/type.scm 191 */
														long BgL_idxz00_5172;

														BgL_idxz00_5172 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5166);
														BgL_test2463z00_6842 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_5172 + 2L)) == BgL_classz00_5164);
													}
												else
													{	/* Cfa/type.scm 191 */
														bool_t BgL_res2281z00_5197;

														{	/* Cfa/type.scm 191 */
															obj_t BgL_oclassz00_5180;

															{	/* Cfa/type.scm 191 */
																obj_t BgL_arg1815z00_5188;
																long BgL_arg1816z00_5189;

																BgL_arg1815z00_5188 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Cfa/type.scm 191 */
																	long BgL_arg1817z00_5190;

																	BgL_arg1817z00_5190 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5166);
																	BgL_arg1816z00_5189 =
																		(BgL_arg1817z00_5190 - OBJECT_TYPE);
																}
																BgL_oclassz00_5180 =
																	VECTOR_REF(BgL_arg1815z00_5188,
																	BgL_arg1816z00_5189);
															}
															{	/* Cfa/type.scm 191 */
																bool_t BgL__ortest_1115z00_5181;

																BgL__ortest_1115z00_5181 =
																	(BgL_classz00_5164 == BgL_oclassz00_5180);
																if (BgL__ortest_1115z00_5181)
																	{	/* Cfa/type.scm 191 */
																		BgL_res2281z00_5197 =
																			BgL__ortest_1115z00_5181;
																	}
																else
																	{	/* Cfa/type.scm 191 */
																		long BgL_odepthz00_5182;

																		{	/* Cfa/type.scm 191 */
																			obj_t BgL_arg1804z00_5183;

																			BgL_arg1804z00_5183 =
																				(BgL_oclassz00_5180);
																			BgL_odepthz00_5182 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_5183);
																		}
																		if ((2L < BgL_odepthz00_5182))
																			{	/* Cfa/type.scm 191 */
																				obj_t BgL_arg1802z00_5185;

																				{	/* Cfa/type.scm 191 */
																					obj_t BgL_arg1803z00_5186;

																					BgL_arg1803z00_5186 =
																						(BgL_oclassz00_5180);
																					BgL_arg1802z00_5185 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_5186, 2L);
																				}
																				BgL_res2281z00_5197 =
																					(BgL_arg1802z00_5185 ==
																					BgL_classz00_5164);
																			}
																		else
																			{	/* Cfa/type.scm 191 */
																				BgL_res2281z00_5197 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2463z00_6842 = BgL_res2281z00_5197;
													}
											}
										}
									}
									if (BgL_test2463z00_6842)
										{	/* Cfa/type.scm 191 */
											BgL_test2458z00_6818 = ((bool_t) 0);
										}
									else
										{	/* Cfa/type.scm 192 */
											bool_t BgL_test2467z00_6869;

											if (CBOOL(BGl_za2unsafezd2typeza2zd2zzengine_paramz00))
												{	/* Cfa/type.scm 192 */
													BgL_test2467z00_6869 = ((bool_t) 1);
												}
											else
												{	/* Cfa/type.scm 192 */
													BgL_test2467z00_6869 =
														(
														(((BgL_globalz00_bglt) COBJECT(
																	((BgL_globalz00_bglt) BgL_variablez00_40)))->
															BgL_initz00) == BTRUE);
												}
											if (BgL_test2467z00_6869)
												{	/* Cfa/type.scm 192 */
													BgL_test2458z00_6818 = ((bool_t) 0);
												}
											else
												{	/* Cfa/type.scm 192 */
													if (
														((((BgL_typez00_bglt) COBJECT(
																		((BgL_typez00_bglt) BgL_ntypez00_3859)))->
																BgL_classz00) == CNST_TABLE_REF(0)))
														{	/* Cfa/type.scm 193 */
															BgL_test2458z00_6818 = ((bool_t) 0);
														}
													else
														{	/* Cfa/type.scm 193 */
															BgL_test2458z00_6818 = ((bool_t) 1);
														}
												}
										}
								}
							else
								{	/* Cfa/type.scm 190 */
									BgL_test2458z00_6818 = ((bool_t) 0);
								}
						}
						if (BgL_test2458z00_6818)
							{	/* Cfa/type.scm 198 */
								BgL_typez00_bglt BgL_vz00_5201;

								BgL_vz00_5201 =
									((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
								return
									((((BgL_variablez00_bglt) COBJECT(BgL_variablez00_40))->
										BgL_typez00) = ((BgL_typez00_bglt) BgL_vz00_5201), BUNSPEC);
							}
						else
							{	/* Cfa/type.scm 199 */
								obj_t BgL_arg1839z00_3872;

								BgL_arg1839z00_3872 =
									BGl_markzd2nullzd2typezd2ctorz12zc0zzcfa_typez00
									(BgL_ntypez00_3859);
								return ((((BgL_variablez00_bglt) COBJECT(BgL_variablez00_40))->
										BgL_typez00) =
									((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_arg1839z00_3872)),
									BUNSPEC);
							}
					}
				else
					{	/* Cfa/type.scm 200 */
						bool_t BgL_test2470z00_6885;

						if (
							(((obj_t) BgL_otypez00_3860) ==
								BGl_za2vectorza2z00zztype_cachez00))
							{	/* Cfa/type.scm 200 */
								obj_t BgL_classz00_5204;

								BgL_classz00_5204 = BGl_tvecz00zztvector_tvectorz00;
								if (BGL_OBJECTP(BgL_ntypez00_3859))
									{	/* Cfa/type.scm 200 */
										BgL_objectz00_bglt BgL_arg1807z00_5206;

										BgL_arg1807z00_5206 =
											(BgL_objectz00_bglt) (BgL_ntypez00_3859);
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/type.scm 200 */
												long BgL_idxz00_5212;

												BgL_idxz00_5212 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5206);
												BgL_test2470z00_6885 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_5212 + 2L)) == BgL_classz00_5204);
											}
										else
											{	/* Cfa/type.scm 200 */
												bool_t BgL_res2282z00_5237;

												{	/* Cfa/type.scm 200 */
													obj_t BgL_oclassz00_5220;

													{	/* Cfa/type.scm 200 */
														obj_t BgL_arg1815z00_5228;
														long BgL_arg1816z00_5229;

														BgL_arg1815z00_5228 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/type.scm 200 */
															long BgL_arg1817z00_5230;

															BgL_arg1817z00_5230 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5206);
															BgL_arg1816z00_5229 =
																(BgL_arg1817z00_5230 - OBJECT_TYPE);
														}
														BgL_oclassz00_5220 =
															VECTOR_REF(BgL_arg1815z00_5228,
															BgL_arg1816z00_5229);
													}
													{	/* Cfa/type.scm 200 */
														bool_t BgL__ortest_1115z00_5221;

														BgL__ortest_1115z00_5221 =
															(BgL_classz00_5204 == BgL_oclassz00_5220);
														if (BgL__ortest_1115z00_5221)
															{	/* Cfa/type.scm 200 */
																BgL_res2282z00_5237 = BgL__ortest_1115z00_5221;
															}
														else
															{	/* Cfa/type.scm 200 */
																long BgL_odepthz00_5222;

																{	/* Cfa/type.scm 200 */
																	obj_t BgL_arg1804z00_5223;

																	BgL_arg1804z00_5223 = (BgL_oclassz00_5220);
																	BgL_odepthz00_5222 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_5223);
																}
																if ((2L < BgL_odepthz00_5222))
																	{	/* Cfa/type.scm 200 */
																		obj_t BgL_arg1802z00_5225;

																		{	/* Cfa/type.scm 200 */
																			obj_t BgL_arg1803z00_5226;

																			BgL_arg1803z00_5226 =
																				(BgL_oclassz00_5220);
																			BgL_arg1802z00_5225 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_5226, 2L);
																		}
																		BgL_res2282z00_5237 =
																			(BgL_arg1802z00_5225 ==
																			BgL_classz00_5204);
																	}
																else
																	{	/* Cfa/type.scm 200 */
																		BgL_res2282z00_5237 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2470z00_6885 = BgL_res2282z00_5237;
											}
									}
								else
									{	/* Cfa/type.scm 200 */
										BgL_test2470z00_6885 = ((bool_t) 0);
									}
							}
						else
							{	/* Cfa/type.scm 200 */
								BgL_test2470z00_6885 = ((bool_t) 0);
							}
						if (BgL_test2470z00_6885)
							{	/* Cfa/type.scm 201 */
								obj_t BgL_arg1845z00_3881;

								BgL_arg1845z00_3881 =
									BGl_markzd2nullzd2typezd2ctorz12zc0zzcfa_typez00
									(BgL_ntypez00_3859);
								return ((((BgL_variablez00_bglt) COBJECT(BgL_variablez00_40))->
										BgL_typez00) =
									((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_arg1845z00_3881)),
									BUNSPEC);
							}
						else
							{	/* Cfa/type.scm 200 */
								return BFALSE;
							}
					}
			}
		}

	}



/* mark-null-type-ctor! */
	obj_t BGl_markzd2nullzd2typezd2ctorz12zc0zzcfa_typez00(obj_t BgL_typez00_42)
	{
		{	/* Cfa/type.scm 216 */
			{	/* Cfa/type.scm 218 */
				bool_t BgL_test2476z00_6914;

				{	/* Cfa/type.scm 218 */
					obj_t BgL_tmpz00_6915;

					BgL_tmpz00_6915 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_typez00_42)))->BgL_nullz00);
					BgL_test2476z00_6914 = SYMBOLP(BgL_tmpz00_6915);
				}
				if (BgL_test2476z00_6914)
					{	/* Cfa/type.scm 218 */
						{
							obj_t BgL_auxz00_6919;

							{	/* Cfa/type.scm 219 */
								obj_t BgL_arg1848z00_3885;

								BgL_arg1848z00_3885 =
									(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt) BgL_typez00_42)))->BgL_nullz00);
								BgL_auxz00_6919 =
									BGl_findzd2globalzd2zzast_envz00(BgL_arg1848z00_3885, BNIL);
							}
							((((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt) BgL_typez00_42)))->BgL_nullz00) =
								((obj_t) BgL_auxz00_6919), BUNSPEC);
						}
						if (CBOOL(
								(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt) BgL_typez00_42)))->BgL_nullz00)))
							{	/* Cfa/type.scm 220 */
								BFALSE;
							}
						else
							{	/* Cfa/type.scm 221 */
								obj_t BgL_arg1851z00_3888;

								BgL_arg1851z00_3888 =
									(((BgL_typez00_bglt) COBJECT(
											((BgL_typez00_bglt) BgL_typez00_42)))->BgL_nullz00);
								BGl_internalzd2errorzd2zztools_errorz00
									(BGl_string2315z00zzcfa_typez00,
									BGl_string2316z00zzcfa_typez00, BgL_arg1851z00_3888);
							}
					}
				else
					{	/* Cfa/type.scm 218 */
						BFALSE;
					}
			}
			{	/* Cfa/type.scm 223 */
				bool_t BgL_test2478z00_6932;

				{	/* Cfa/type.scm 223 */
					obj_t BgL_arg1857z00_3894;

					BgL_arg1857z00_3894 =
						(((BgL_typez00_bglt) COBJECT(
								((BgL_typez00_bglt) BgL_typez00_42)))->BgL_nullz00);
					{	/* Cfa/type.scm 223 */
						obj_t BgL_classz00_5240;

						BgL_classz00_5240 = BGl_globalz00zzast_varz00;
						if (BGL_OBJECTP(BgL_arg1857z00_3894))
							{	/* Cfa/type.scm 223 */
								BgL_objectz00_bglt BgL_arg1807z00_5242;

								BgL_arg1807z00_5242 =
									(BgL_objectz00_bglt) (BgL_arg1857z00_3894);
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/type.scm 223 */
										long BgL_idxz00_5248;

										BgL_idxz00_5248 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5242);
										BgL_test2478z00_6932 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_5248 + 2L)) == BgL_classz00_5240);
									}
								else
									{	/* Cfa/type.scm 223 */
										bool_t BgL_res2283z00_5273;

										{	/* Cfa/type.scm 223 */
											obj_t BgL_oclassz00_5256;

											{	/* Cfa/type.scm 223 */
												obj_t BgL_arg1815z00_5264;
												long BgL_arg1816z00_5265;

												BgL_arg1815z00_5264 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/type.scm 223 */
													long BgL_arg1817z00_5266;

													BgL_arg1817z00_5266 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5242);
													BgL_arg1816z00_5265 =
														(BgL_arg1817z00_5266 - OBJECT_TYPE);
												}
												BgL_oclassz00_5256 =
													VECTOR_REF(BgL_arg1815z00_5264, BgL_arg1816z00_5265);
											}
											{	/* Cfa/type.scm 223 */
												bool_t BgL__ortest_1115z00_5257;

												BgL__ortest_1115z00_5257 =
													(BgL_classz00_5240 == BgL_oclassz00_5256);
												if (BgL__ortest_1115z00_5257)
													{	/* Cfa/type.scm 223 */
														BgL_res2283z00_5273 = BgL__ortest_1115z00_5257;
													}
												else
													{	/* Cfa/type.scm 223 */
														long BgL_odepthz00_5258;

														{	/* Cfa/type.scm 223 */
															obj_t BgL_arg1804z00_5259;

															BgL_arg1804z00_5259 = (BgL_oclassz00_5256);
															BgL_odepthz00_5258 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_5259);
														}
														if ((2L < BgL_odepthz00_5258))
															{	/* Cfa/type.scm 223 */
																obj_t BgL_arg1802z00_5261;

																{	/* Cfa/type.scm 223 */
																	obj_t BgL_arg1803z00_5262;

																	BgL_arg1803z00_5262 = (BgL_oclassz00_5256);
																	BgL_arg1802z00_5261 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5262,
																		2L);
																}
																BgL_res2283z00_5273 =
																	(BgL_arg1802z00_5261 == BgL_classz00_5240);
															}
														else
															{	/* Cfa/type.scm 223 */
																BgL_res2283z00_5273 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test2478z00_6932 = BgL_res2283z00_5273;
									}
							}
						else
							{	/* Cfa/type.scm 223 */
								BgL_test2478z00_6932 = ((bool_t) 0);
							}
					}
				}
				if (BgL_test2478z00_6932)
					{	/* Cfa/type.scm 224 */
						BgL_globalz00_bglt BgL_i1182z00_3892;

						BgL_i1182z00_3892 =
							((BgL_globalz00_bglt)
							(((BgL_typez00_bglt) COBJECT(
										((BgL_typez00_bglt) BgL_typez00_42)))->BgL_nullz00));
						((((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt) BgL_i1182z00_3892)))->
								BgL_occurrencez00) =
							((long) (1L +
									(((BgL_variablez00_bglt) COBJECT(((BgL_variablez00_bglt)
													BgL_i1182z00_3892)))->BgL_occurrencez00))), BUNSPEC);
					}
				else
					{	/* Cfa/type.scm 223 */
						BFALSE;
					}
			}
			return BgL_typez00_42;
		}

	}



/* type-node*! */
	obj_t BGl_typezd2nodeza2z12z62zzcfa_typez00(obj_t BgL_nodeza2za2_78)
	{
		{	/* Cfa/type.scm 645 */
			{
				obj_t BgL_l1631z00_3898;

				BgL_l1631z00_3898 = BgL_nodeza2za2_78;
			BgL_zc3z04anonymousza31859ze3z87_3899:
				if (NULLP(BgL_l1631z00_3898))
					{	/* Cfa/type.scm 646 */
						return BgL_nodeza2za2_78;
					}
				else
					{	/* Cfa/type.scm 646 */
						{	/* Cfa/type.scm 646 */
							BgL_nodez00_bglt BgL_arg1862z00_3901;

							{	/* Cfa/type.scm 646 */
								obj_t BgL_arg1863z00_3902;

								BgL_arg1863z00_3902 = CAR(((obj_t) BgL_l1631z00_3898));
								BgL_arg1862z00_3901 =
									BGl_typezd2nodez12zc0zzcfa_typez00(
									((BgL_nodez00_bglt) BgL_arg1863z00_3902));
							}
							{	/* Cfa/type.scm 646 */
								obj_t BgL_auxz00_6973;
								obj_t BgL_tmpz00_6971;

								BgL_auxz00_6973 = ((obj_t) BgL_arg1862z00_3901);
								BgL_tmpz00_6971 = ((obj_t) BgL_l1631z00_3898);
								SET_CAR(BgL_tmpz00_6971, BgL_auxz00_6973);
							}
						}
						{	/* Cfa/type.scm 646 */
							obj_t BgL_arg1864z00_3903;

							BgL_arg1864z00_3903 = CDR(((obj_t) BgL_l1631z00_3898));
							{
								obj_t BgL_l1631z00_6978;

								BgL_l1631z00_6978 = BgL_arg1864z00_3903;
								BgL_l1631z00_3898 = BgL_l1631z00_6978;
								goto BgL_zc3z04anonymousza31859ze3z87_3899;
							}
						}
					}
			}
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_typez00(void)
	{
		{	/* Cfa/type.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_typez00(void)
	{
		{	/* Cfa/type.scm 15 */
			BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_typezd2variablez12zd2envz12zzcfa_typez00,
				BGl_proc2317z00zzcfa_typez00, BGl_valuez00zzast_varz00,
				BGl_string2318z00zzcfa_typez00);
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_proc2319z00zzcfa_typez00,
				BGl_nodez00zzast_nodez00, BGl_string2320z00zzcfa_typez00);
		}

	}



/* &type-node!1645 */
	obj_t BGl_z62typezd2nodez121645za2zzcfa_typez00(obj_t BgL_envz00_6090,
		obj_t BgL_nodez00_6091)
	{
		{	/* Cfa/type.scm 231 */
			return
				BGl_errorz00zz__errorz00(CNST_TABLE_REF(1),
				BGl_string2321z00zzcfa_typez00,
				((obj_t) ((BgL_nodez00_bglt) BgL_nodez00_6091)));
		}

	}



/* &type-variable!1633 */
	obj_t BGl_z62typezd2variablez121633za2zzcfa_typez00(obj_t BgL_envz00_6092,
		obj_t BgL_valuez00_6093, obj_t BgL_variablez00_6094)
	{
		{	/* Cfa/type.scm 130 */
			{	/* Cfa/type.scm 131 */
				BgL_typez00_bglt BgL_typez00_6232;

				BgL_typez00_6232 =
					(((BgL_variablez00_bglt) COBJECT(
							((BgL_variablez00_bglt) BgL_variablez00_6094)))->BgL_typez00);
				{	/* Cfa/type.scm 133 */
					bool_t BgL_test2484z00_6987;

					{	/* Cfa/type.scm 133 */
						obj_t BgL_classz00_6233;

						BgL_classz00_6233 = BGl_typez00zztype_typez00;
						{	/* Cfa/type.scm 133 */
							BgL_objectz00_bglt BgL_arg1807z00_6234;

							{	/* Cfa/type.scm 133 */
								obj_t BgL_tmpz00_6988;

								BgL_tmpz00_6988 =
									((obj_t) ((BgL_objectz00_bglt) BgL_typez00_6232));
								BgL_arg1807z00_6234 = (BgL_objectz00_bglt) (BgL_tmpz00_6988);
							}
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/type.scm 133 */
									long BgL_idxz00_6235;

									BgL_idxz00_6235 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6234);
									BgL_test2484z00_6987 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_6235 + 1L)) == BgL_classz00_6233);
								}
							else
								{	/* Cfa/type.scm 133 */
									bool_t BgL_res2284z00_6238;

									{	/* Cfa/type.scm 133 */
										obj_t BgL_oclassz00_6239;

										{	/* Cfa/type.scm 133 */
											obj_t BgL_arg1815z00_6240;
											long BgL_arg1816z00_6241;

											BgL_arg1815z00_6240 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/type.scm 133 */
												long BgL_arg1817z00_6242;

												BgL_arg1817z00_6242 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6234);
												BgL_arg1816z00_6241 =
													(BgL_arg1817z00_6242 - OBJECT_TYPE);
											}
											BgL_oclassz00_6239 =
												VECTOR_REF(BgL_arg1815z00_6240, BgL_arg1816z00_6241);
										}
										{	/* Cfa/type.scm 133 */
											bool_t BgL__ortest_1115z00_6243;

											BgL__ortest_1115z00_6243 =
												(BgL_classz00_6233 == BgL_oclassz00_6239);
											if (BgL__ortest_1115z00_6243)
												{	/* Cfa/type.scm 133 */
													BgL_res2284z00_6238 = BgL__ortest_1115z00_6243;
												}
											else
												{	/* Cfa/type.scm 133 */
													long BgL_odepthz00_6244;

													{	/* Cfa/type.scm 133 */
														obj_t BgL_arg1804z00_6245;

														BgL_arg1804z00_6245 = (BgL_oclassz00_6239);
														BgL_odepthz00_6244 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_6245);
													}
													if ((1L < BgL_odepthz00_6244))
														{	/* Cfa/type.scm 133 */
															obj_t BgL_arg1802z00_6246;

															{	/* Cfa/type.scm 133 */
																obj_t BgL_arg1803z00_6247;

																BgL_arg1803z00_6247 = (BgL_oclassz00_6239);
																BgL_arg1802z00_6246 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_6247,
																	1L);
															}
															BgL_res2284z00_6238 =
																(BgL_arg1802z00_6246 == BgL_classz00_6233);
														}
													else
														{	/* Cfa/type.scm 133 */
															BgL_res2284z00_6238 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2484z00_6987 = BgL_res2284z00_6238;
								}
						}
					}
					if (BgL_test2484z00_6987)
						{	/* Cfa/type.scm 135 */
							bool_t BgL_test2488z00_7011;

							if ((((obj_t) BgL_typez00_6232) == BGl_za2_za2z00zztype_cachez00))
								{	/* Cfa/type.scm 135 */
									if (CBOOL(BGl_za2optimzd2cfazf3za2z21zzengine_paramz00))
										{	/* Cfa/type.scm 135 */
											BgL_test2488z00_7011 = ((bool_t) 0);
										}
									else
										{	/* Cfa/type.scm 135 */
											BgL_test2488z00_7011 = ((bool_t) 1);
										}
								}
							else
								{	/* Cfa/type.scm 135 */
									BgL_test2488z00_7011 = ((bool_t) 0);
								}
							if (BgL_test2488z00_7011)
								{	/* Cfa/type.scm 135 */
									return
										BGl_setzd2variablezd2typez12z12zzcfa_typez00(
										((BgL_variablez00_bglt) BgL_variablez00_6094),
										BGl_getzd2defaultzd2typez00zztype_cachez00());
								}
							else
								{	/* Cfa/type.scm 135 */
									return BFALSE;
								}
						}
					else
						{	/* Cfa/type.scm 133 */
							return
								BGl_setzd2variablezd2typez12z12zzcfa_typez00(
								((BgL_variablez00_bglt) BgL_variablez00_6094),
								BGl_getzd2defaultzd2typez00zztype_cachez00());
						}
				}
			}
		}

	}



/* type-variable! */
	obj_t BGl_typezd2variablez12zc0zzcfa_typez00(BgL_valuez00_bglt
		BgL_valuez00_28, BgL_variablez00_bglt BgL_variablez00_29)
	{
		{	/* Cfa/type.scm 130 */
			{	/* Cfa/type.scm 130 */
				obj_t BgL_method1634z00_3919;

				{	/* Cfa/type.scm 130 */
					obj_t BgL_res2289z00_5344;

					{	/* Cfa/type.scm 130 */
						long BgL_objzd2classzd2numz00_5315;

						BgL_objzd2classzd2numz00_5315 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_valuez00_28));
						{	/* Cfa/type.scm 130 */
							obj_t BgL_arg1811z00_5316;

							BgL_arg1811z00_5316 =
								PROCEDURE_REF(BGl_typezd2variablez12zd2envz12zzcfa_typez00,
								(int) (1L));
							{	/* Cfa/type.scm 130 */
								int BgL_offsetz00_5319;

								BgL_offsetz00_5319 = (int) (BgL_objzd2classzd2numz00_5315);
								{	/* Cfa/type.scm 130 */
									long BgL_offsetz00_5320;

									BgL_offsetz00_5320 =
										((long) (BgL_offsetz00_5319) - OBJECT_TYPE);
									{	/* Cfa/type.scm 130 */
										long BgL_modz00_5321;

										BgL_modz00_5321 =
											(BgL_offsetz00_5320 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/type.scm 130 */
											long BgL_restz00_5323;

											BgL_restz00_5323 =
												(BgL_offsetz00_5320 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/type.scm 130 */

												{	/* Cfa/type.scm 130 */
													obj_t BgL_bucketz00_5325;

													BgL_bucketz00_5325 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_5316), BgL_modz00_5321);
													BgL_res2289z00_5344 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_5325), BgL_restz00_5323);
					}}}}}}}}
					BgL_method1634z00_3919 = BgL_res2289z00_5344;
				}
				return
					BGL_PROCEDURE_CALL2(BgL_method1634z00_3919,
					((obj_t) BgL_valuez00_28), ((obj_t) BgL_variablez00_29));
			}
		}

	}



/* &type-variable! */
	obj_t BGl_z62typezd2variablez12za2zzcfa_typez00(obj_t BgL_envz00_6095,
		obj_t BgL_valuez00_6096, obj_t BgL_variablez00_6097)
	{
		{	/* Cfa/type.scm 130 */
			return
				BGl_typezd2variablez12zc0zzcfa_typez00(
				((BgL_valuez00_bglt) BgL_valuez00_6096),
				((BgL_variablez00_bglt) BgL_variablez00_6097));
		}

	}



/* type-node! */
	BgL_nodez00_bglt BGl_typezd2nodez12zc0zzcfa_typez00(BgL_nodez00_bglt
		BgL_nodez00_43)
	{
		{	/* Cfa/type.scm 231 */
			{	/* Cfa/type.scm 231 */
				obj_t BgL_method1646z00_3920;

				{	/* Cfa/type.scm 231 */
					obj_t BgL_res2294z00_5375;

					{	/* Cfa/type.scm 231 */
						long BgL_objzd2classzd2numz00_5346;

						BgL_objzd2classzd2numz00_5346 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_nodez00_43));
						{	/* Cfa/type.scm 231 */
							obj_t BgL_arg1811z00_5347;

							BgL_arg1811z00_5347 =
								PROCEDURE_REF(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
								(int) (1L));
							{	/* Cfa/type.scm 231 */
								int BgL_offsetz00_5350;

								BgL_offsetz00_5350 = (int) (BgL_objzd2classzd2numz00_5346);
								{	/* Cfa/type.scm 231 */
									long BgL_offsetz00_5351;

									BgL_offsetz00_5351 =
										((long) (BgL_offsetz00_5350) - OBJECT_TYPE);
									{	/* Cfa/type.scm 231 */
										long BgL_modz00_5352;

										BgL_modz00_5352 =
											(BgL_offsetz00_5351 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/type.scm 231 */
											long BgL_restz00_5354;

											BgL_restz00_5354 =
												(BgL_offsetz00_5351 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/type.scm 231 */

												{	/* Cfa/type.scm 231 */
													obj_t BgL_bucketz00_5356;

													BgL_bucketz00_5356 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_5347), BgL_modz00_5352);
													BgL_res2294z00_5375 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_5356), BgL_restz00_5354);
					}}}}}}}}
					BgL_method1646z00_3920 = BgL_res2294z00_5375;
				}
				return
					((BgL_nodez00_bglt)
					BGL_PROCEDURE_CALL1(BgL_method1646z00_3920,
						((obj_t) BgL_nodez00_43)));
			}
		}

	}



/* &type-node! */
	BgL_nodez00_bglt BGl_z62typezd2nodez12za2zzcfa_typez00(obj_t BgL_envz00_6098,
		obj_t BgL_nodez00_6099)
	{
		{	/* Cfa/type.scm 231 */
			return
				BGl_typezd2nodez12zc0zzcfa_typez00(
				((BgL_nodez00_bglt) BgL_nodez00_6099));
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_typez00(void)
	{
		{	/* Cfa/type.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2variablez12zd2envz12zzcfa_typez00,
				BGl_svarzf2Cinfozf2zzcfa_infoz00, BGl_proc2322z00zzcfa_typez00,
				BGl_string2323z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2variablez12zd2envz12zzcfa_typez00,
				BGl_scnstzf2Cinfozf2zzcfa_infoz00, BGl_proc2324z00zzcfa_typez00,
				BGl_string2323z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2variablez12zd2envz12zzcfa_typez00,
				BGl_cvarzf2Cinfozf2zzcfa_infoz00, BGl_proc2325z00zzcfa_typez00,
				BGl_string2323z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2variablez12zd2envz12zzcfa_typez00,
				BGl_sexitzf2Cinfozf2zzcfa_infoz00, BGl_proc2326z00zzcfa_typez00,
				BGl_string2323z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2variablez12zd2envz12zzcfa_typez00,
				BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00, BGl_proc2327z00zzcfa_typez00,
				BGl_string2323z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_atomz00zzast_nodez00,
				BGl_proc2328z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_kwotez00zzast_nodez00,
				BGl_proc2330z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_varz00zzast_nodez00,
				BGl_proc2331z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_closurez00zzast_nodez00,
				BGl_proc2332z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_sequencez00zzast_nodez00,
				BGl_proc2333z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_syncz00zzast_nodez00,
				BGl_proc2334z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_appz00zzast_nodez00,
				BGl_proc2335z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
				BGl_arithmeticzd2appzd2zzcfa_info2z00, BGl_proc2336z00zzcfa_typez00,
				BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
				BGl_procedurezd2refzd2appz00zzcfa_info2z00,
				BGl_proc2337z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
				BGl_procedurezd2setz12zd2appz12zzcfa_info2z00,
				BGl_proc2338z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
				BGl_conszd2refzd2appz00zzcfa_info2z00, BGl_proc2339z00zzcfa_typez00,
				BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_appzd2lyzd2zzast_nodez00,
				BGl_proc2340z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
				BGl_appzd2lyzf2Cinfoz20zzcfa_infoz00, BGl_proc2341z00zzcfa_typez00,
				BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_funcallz00zzast_nodez00,
				BGl_proc2342z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
				BGl_funcallzf2Cinfozf2zzcfa_infoz00, BGl_proc2343z00zzcfa_typez00,
				BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_externz00zzast_nodez00,
				BGl_proc2344z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_vallocz00zzast_nodez00,
				BGl_proc2345z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_vrefz00zzast_nodez00,
				BGl_proc2346z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_vsetz12z12zzast_nodez00,
				BGl_proc2347z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_castz00zzast_nodez00,
				BGl_proc2348z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_setqz00zzast_nodez00,
				BGl_proc2349z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
				BGl_conditionalz00zzast_nodez00, BGl_proc2350z00zzcfa_typez00,
				BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
				BGl_conditionalzf2Cinfozf2zzcfa_infoz00, BGl_proc2351z00zzcfa_typez00,
				BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_failz00zzast_nodez00,
				BGl_proc2352z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_switchz00zzast_nodez00,
				BGl_proc2353z00zzcfa_typez00, BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
				BGl_letzd2funzd2zzast_nodez00, BGl_proc2354z00zzcfa_typez00,
				BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
				BGl_letzd2varzd2zzast_nodez00, BGl_proc2355z00zzcfa_typez00,
				BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
				BGl_setzd2exzd2itz00zzast_nodez00, BGl_proc2356z00zzcfa_typez00,
				BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
				BGl_jumpzd2exzd2itz00zzast_nodez00, BGl_proc2357z00zzcfa_typez00,
				BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
				BGl_makezd2boxzd2zzast_nodez00, BGl_proc2358z00zzcfa_typez00,
				BGl_string2329z00zzcfa_typez00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
				BGl_boxzd2setz12zc0zzast_nodez00, BGl_proc2359z00zzcfa_typez00,
				BGl_string2329z00zzcfa_typez00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_typezd2nodez12zd2envz12zzcfa_typez00,
				BGl_boxzd2refzd2zzast_nodez00, BGl_proc2360z00zzcfa_typez00,
				BGl_string2329z00zzcfa_typez00);
		}

	}



/* &type-node!-box-ref1710 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2boxzd2ref1710za2zzcfa_typez00(obj_t
		BgL_envz00_6137, obj_t BgL_nodez00_6138)
	{
		{	/* Cfa/type.scm 635 */
			{
				BgL_varz00_bglt BgL_auxz00_7128;

				{	/* Cfa/type.scm 637 */
					BgL_varz00_bglt BgL_arg2072z00_6249;

					BgL_arg2072z00_6249 =
						(((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_6138)))->BgL_varz00);
					BgL_auxz00_7128 =
						((BgL_varz00_bglt)
						BGl_typezd2nodez12zc0zzcfa_typez00(
							((BgL_nodez00_bglt) BgL_arg2072z00_6249)));
				}
				((((BgL_boxzd2refzd2_bglt) COBJECT(
								((BgL_boxzd2refzd2_bglt) BgL_nodez00_6138)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_7128), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2refzd2_bglt) BgL_nodez00_6138));
		}

	}



/* &type-node!-box-set!1708 */
	BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2boxzd2setz121708zb0zzcfa_typez00(obj_t
		BgL_envz00_6139, obj_t BgL_nodez00_6140)
	{
		{	/* Cfa/type.scm 626 */
			{
				BgL_varz00_bglt BgL_auxz00_7138;

				{	/* Cfa/type.scm 628 */
					BgL_varz00_bglt BgL_arg2069z00_6251;

					BgL_arg2069z00_6251 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_6140)))->BgL_varz00);
					BgL_auxz00_7138 =
						((BgL_varz00_bglt)
						BGl_typezd2nodez12zc0zzcfa_typez00(
							((BgL_nodez00_bglt) BgL_arg2069z00_6251)));
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_6140)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_7138), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_7146;

				{	/* Cfa/type.scm 629 */
					BgL_nodez00_bglt BgL_arg2070z00_6252;

					BgL_arg2070z00_6252 =
						(((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_6140)))->BgL_valuez00);
					BgL_auxz00_7146 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2070z00_6252);
				}
				((((BgL_boxzd2setz12zc0_bglt) COBJECT(
								((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_6140)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_7146), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_boxzd2setz12zc0_bglt) BgL_nodez00_6140));
		}

	}



/* &type-node!-make-box1706 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2makezd2box1706za2zzcfa_typez00(obj_t
		BgL_envz00_6141, obj_t BgL_nodez00_6142)
	{
		{	/* Cfa/type.scm 618 */
			{
				BgL_nodez00_bglt BgL_auxz00_7154;

				{	/* Cfa/type.scm 620 */
					BgL_nodez00_bglt BgL_arg2068z00_6254;

					BgL_arg2068z00_6254 =
						(((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_6142)))->BgL_valuez00);
					BgL_auxz00_7154 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2068z00_6254);
				}
				((((BgL_makezd2boxzd2_bglt) COBJECT(
								((BgL_makezd2boxzd2_bglt) BgL_nodez00_6142)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_7154), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_makezd2boxzd2_bglt) BgL_nodez00_6142));
		}

	}



/* &type-node!-jump-ex-i1704 */
	BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2jumpzd2exzd2i1704z70zzcfa_typez00(obj_t
		BgL_envz00_6143, obj_t BgL_nodez00_6144)
	{
		{	/* Cfa/type.scm 609 */
			{
				BgL_nodez00_bglt BgL_auxz00_7162;

				{	/* Cfa/type.scm 611 */
					BgL_nodez00_bglt BgL_arg2065z00_6256;

					BgL_arg2065z00_6256 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6144)))->BgL_exitz00);
					BgL_auxz00_7162 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2065z00_6256);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6144)))->
						BgL_exitz00) = ((BgL_nodez00_bglt) BgL_auxz00_7162), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_7168;

				{	/* Cfa/type.scm 612 */
					BgL_nodez00_bglt BgL_arg2067z00_6257;

					BgL_arg2067z00_6257 =
						(((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6144)))->
						BgL_valuez00);
					BgL_auxz00_7168 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2067z00_6257);
				}
				((((BgL_jumpzd2exzd2itz00_bglt) COBJECT(
								((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6144)))->
						BgL_valuez00) = ((BgL_nodez00_bglt) BgL_auxz00_7168), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_jumpzd2exzd2itz00_bglt) BgL_nodez00_6144));
		}

	}



/* &type-node!-set-ex-it1702 */
	BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2setzd2exzd2it1702z70zzcfa_typez00(obj_t
		BgL_envz00_6145, obj_t BgL_nodez00_6146)
	{
		{	/* Cfa/type.scm 597 */
			{	/* Cfa/type.scm 599 */
				BgL_variablez00_bglt BgL_vz00_6259;

				BgL_vz00_6259 =
					(((BgL_varz00_bglt) COBJECT(
							(((BgL_setzd2exzd2itz00_bglt) COBJECT(
										((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6146)))->
								BgL_varz00)))->BgL_variablez00);
				{	/* Cfa/type.scm 600 */
					BgL_valuez00_bglt BgL_arg2060z00_6260;

					BgL_arg2060z00_6260 =
						(((BgL_variablez00_bglt) COBJECT(
								((BgL_variablez00_bglt)
									((BgL_localz00_bglt) BgL_vz00_6259))))->BgL_valuez00);
					BGl_typezd2variablez12zc0zzcfa_typez00(BgL_arg2060z00_6260,
						BgL_vz00_6259);
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_7183;

				{	/* Cfa/type.scm 601 */
					BgL_nodez00_bglt BgL_arg2062z00_6261;

					BgL_arg2062z00_6261 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6146)))->BgL_bodyz00);
					BgL_auxz00_7183 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2062z00_6261);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6146)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7183), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_7189;

				{	/* Cfa/type.scm 602 */
					BgL_nodez00_bglt BgL_arg2063z00_6262;

					BgL_arg2063z00_6262 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6146)))->
						BgL_onexitz00);
					BgL_auxz00_7189 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2063z00_6262);
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6146)))->
						BgL_onexitz00) = ((BgL_nodez00_bglt) BgL_auxz00_7189), BUNSPEC);
			}
			{
				BgL_varz00_bglt BgL_auxz00_7195;

				{	/* Cfa/type.scm 603 */
					BgL_varz00_bglt BgL_arg2064z00_6263;

					BgL_arg2064z00_6263 =
						(((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6146)))->BgL_varz00);
					BgL_auxz00_7195 =
						((BgL_varz00_bglt)
						BGl_typezd2nodez12zc0zzcfa_typez00(
							((BgL_nodez00_bglt) BgL_arg2064z00_6263)));
				}
				((((BgL_setzd2exzd2itz00_bglt) COBJECT(
								((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6146)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_7195), BUNSPEC);
			}
			return
				((BgL_nodez00_bglt) ((BgL_setzd2exzd2itz00_bglt) BgL_nodez00_6146));
		}

	}



/* &type-node!-let-var1700 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2letzd2var1700za2zzcfa_typez00(obj_t
		BgL_envz00_6147, obj_t BgL_nodez00_6148)
	{
		{	/* Cfa/type.scm 582 */
			{	/* Cfa/type.scm 584 */
				obj_t BgL_g1629z00_6265;

				BgL_g1629z00_6265 =
					(((BgL_letzd2varzd2_bglt) COBJECT(
							((BgL_letzd2varzd2_bglt) BgL_nodez00_6148)))->BgL_bindingsz00);
				{
					obj_t BgL_l1627z00_6267;

					BgL_l1627z00_6267 = BgL_g1629z00_6265;
				BgL_zc3z04anonymousza32053ze3z87_6266:
					if (PAIRP(BgL_l1627z00_6267))
						{	/* Cfa/type.scm 584 */
							{	/* Cfa/type.scm 585 */
								obj_t BgL_bindingz00_6268;

								BgL_bindingz00_6268 = CAR(BgL_l1627z00_6267);
								{	/* Cfa/type.scm 585 */
									obj_t BgL_varz00_6269;
									obj_t BgL_valz00_6270;

									BgL_varz00_6269 = CAR(((obj_t) BgL_bindingz00_6268));
									BgL_valz00_6270 = CDR(((obj_t) BgL_bindingz00_6268));
									{	/* Cfa/type.scm 587 */
										BgL_nodez00_bglt BgL_arg2055z00_6271;

										BgL_arg2055z00_6271 =
											BGl_typezd2nodez12zc0zzcfa_typez00(
											((BgL_nodez00_bglt) BgL_valz00_6270));
										{	/* Cfa/type.scm 587 */
											obj_t BgL_auxz00_7218;
											obj_t BgL_tmpz00_7216;

											BgL_auxz00_7218 = ((obj_t) BgL_arg2055z00_6271);
											BgL_tmpz00_7216 = ((obj_t) BgL_bindingz00_6268);
											SET_CDR(BgL_tmpz00_7216, BgL_auxz00_7218);
										}
									}
									{	/* Cfa/type.scm 588 */
										BgL_valuez00_bglt BgL_arg2056z00_6272;

										BgL_arg2056z00_6272 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_varz00_6269))))->
											BgL_valuez00);
										BGl_typezd2variablez12zc0zzcfa_typez00(BgL_arg2056z00_6272,
											((BgL_variablez00_bglt) BgL_varz00_6269));
									}
								}
							}
							{
								obj_t BgL_l1627z00_7226;

								BgL_l1627z00_7226 = CDR(BgL_l1627z00_6267);
								BgL_l1627z00_6267 = BgL_l1627z00_7226;
								goto BgL_zc3z04anonymousza32053ze3z87_6266;
							}
						}
					else
						{	/* Cfa/type.scm 584 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_7228;

				{	/* Cfa/type.scm 590 */
					BgL_nodez00_bglt BgL_arg2058z00_6273;

					BgL_arg2058z00_6273 =
						(((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_6148)))->BgL_bodyz00);
					BgL_auxz00_7228 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2058z00_6273);
				}
				((((BgL_letzd2varzd2_bglt) COBJECT(
								((BgL_letzd2varzd2_bglt) BgL_nodez00_6148)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7228), BUNSPEC);
			}
			((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_letzd2varzd2_bglt) BgL_nodez00_6148))))->BgL_typez00) =
				((BgL_typez00_bglt) (((BgL_nodez00_bglt)
							COBJECT((((BgL_letzd2varzd2_bglt) COBJECT(((BgL_letzd2varzd2_bglt)
												BgL_nodez00_6148)))->BgL_bodyz00)))->BgL_typez00)),
				BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_letzd2varzd2_bglt) BgL_nodez00_6148));
		}

	}



/* &type-node!-let-fun1698 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2letzd2fun1698za2zzcfa_typez00(obj_t
		BgL_envz00_6149, obj_t BgL_nodez00_6150)
	{
		{	/* Cfa/type.scm 572 */
			{	/* Cfa/type.scm 574 */
				obj_t BgL_g1626z00_6275;

				BgL_g1626z00_6275 =
					(((BgL_letzd2funzd2_bglt) COBJECT(
							((BgL_letzd2funzd2_bglt) BgL_nodez00_6150)))->BgL_localsz00);
				{
					obj_t BgL_l1623z00_6277;

					BgL_l1623z00_6277 = BgL_g1626z00_6275;
				BgL_zc3z04anonymousza32047ze3z87_6276:
					if (PAIRP(BgL_l1623z00_6277))
						{	/* Cfa/type.scm 574 */
							{	/* Cfa/type.scm 574 */
								obj_t BgL_arg2049z00_6278;

								BgL_arg2049z00_6278 = CAR(BgL_l1623z00_6277);
								BGl_typezd2funz12zc0zzcfa_typez00(
									((BgL_variablez00_bglt) BgL_arg2049z00_6278));
							}
							{
								obj_t BgL_l1623z00_7249;

								BgL_l1623z00_7249 = CDR(BgL_l1623z00_6277);
								BgL_l1623z00_6277 = BgL_l1623z00_7249;
								goto BgL_zc3z04anonymousza32047ze3z87_6276;
							}
						}
					else
						{	/* Cfa/type.scm 574 */
							((bool_t) 1);
						}
				}
			}
			{
				BgL_nodez00_bglt BgL_auxz00_7251;

				{	/* Cfa/type.scm 575 */
					BgL_nodez00_bglt BgL_arg2051z00_6279;

					BgL_arg2051z00_6279 =
						(((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_6150)))->BgL_bodyz00);
					BgL_auxz00_7251 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2051z00_6279);
				}
				((((BgL_letzd2funzd2_bglt) COBJECT(
								((BgL_letzd2funzd2_bglt) BgL_nodez00_6150)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7251), BUNSPEC);
			}
			((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_letzd2funzd2_bglt) BgL_nodez00_6150))))->BgL_typez00) =
				((BgL_typez00_bglt) (((BgL_nodez00_bglt)
							COBJECT((((BgL_letzd2funzd2_bglt) COBJECT(((BgL_letzd2funzd2_bglt)
												BgL_nodez00_6150)))->BgL_bodyz00)))->BgL_typez00)),
				BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_letzd2funzd2_bglt) BgL_nodez00_6150));
		}

	}



/* &type-node!-switch1696 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2switch1696z70zzcfa_typez00(obj_t
		BgL_envz00_6151, obj_t BgL_nodez00_6152)
	{
		{	/* Cfa/type.scm 547 */
			{
				BgL_nodez00_bglt BgL_auxz00_7265;

				{	/* Cfa/type.scm 549 */
					BgL_nodez00_bglt BgL_arg2040z00_6281;

					BgL_arg2040z00_6281 =
						(((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_6152)))->BgL_testz00);
					BgL_auxz00_7265 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2040z00_6281);
				}
				((((BgL_switchz00_bglt) COBJECT(
								((BgL_switchz00_bglt) BgL_nodez00_6152)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7265), BUNSPEC);
			}
			{	/* Cfa/type.scm 550 */
				obj_t BgL_g1622z00_6282;

				BgL_g1622z00_6282 =
					(((BgL_switchz00_bglt) COBJECT(
							((BgL_switchz00_bglt) BgL_nodez00_6152)))->BgL_clausesz00);
				{
					obj_t BgL_l1620z00_6284;

					BgL_l1620z00_6284 = BgL_g1622z00_6282;
				BgL_zc3z04anonymousza32041ze3z87_6283:
					if (PAIRP(BgL_l1620z00_6284))
						{	/* Cfa/type.scm 550 */
							{	/* Cfa/type.scm 551 */
								obj_t BgL_clausez00_6285;

								BgL_clausez00_6285 = CAR(BgL_l1620z00_6284);
								{	/* Cfa/type.scm 551 */
									BgL_nodez00_bglt BgL_arg2044z00_6286;

									{	/* Cfa/type.scm 551 */
										obj_t BgL_arg2045z00_6287;

										BgL_arg2045z00_6287 = CDR(((obj_t) BgL_clausez00_6285));
										BgL_arg2044z00_6286 =
											BGl_typezd2nodez12zc0zzcfa_typez00(
											((BgL_nodez00_bglt) BgL_arg2045z00_6287));
									}
									{	/* Cfa/type.scm 551 */
										obj_t BgL_auxz00_7282;
										obj_t BgL_tmpz00_7280;

										BgL_auxz00_7282 = ((obj_t) BgL_arg2044z00_6286);
										BgL_tmpz00_7280 = ((obj_t) BgL_clausez00_6285);
										SET_CDR(BgL_tmpz00_7280, BgL_auxz00_7282);
									}
								}
							}
							{
								obj_t BgL_l1620z00_7285;

								BgL_l1620z00_7285 = CDR(BgL_l1620z00_6284);
								BgL_l1620z00_6284 = BgL_l1620z00_7285;
								goto BgL_zc3z04anonymousza32041ze3z87_6283;
							}
						}
					else
						{	/* Cfa/type.scm 550 */
							((bool_t) 1);
						}
				}
			}
			((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_switchz00_bglt) BgL_nodez00_6152))))->BgL_typez00) =
				((BgL_typez00_bglt)
					BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt) (
								(BgL_switchz00_bglt) BgL_nodez00_6152)), ((bool_t) 0))),
				BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_switchz00_bglt) BgL_nodez00_6152));
		}

	}



/* &type-node!-fail1694 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2fail1694z70zzcfa_typez00(obj_t
		BgL_envz00_6153, obj_t BgL_nodez00_6154)
	{
		{	/* Cfa/type.scm 537 */
			{
				BgL_nodez00_bglt BgL_auxz00_7295;

				{	/* Cfa/type.scm 539 */
					BgL_nodez00_bglt BgL_arg2037z00_6289;

					BgL_arg2037z00_6289 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_6154)))->BgL_procz00);
					BgL_auxz00_7295 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2037z00_6289);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_6154)))->BgL_procz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7295), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_7301;

				{	/* Cfa/type.scm 540 */
					BgL_nodez00_bglt BgL_arg2038z00_6290;

					BgL_arg2038z00_6290 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_6154)))->BgL_msgz00);
					BgL_auxz00_7301 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2038z00_6290);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_6154)))->BgL_msgz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7301), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_7307;

				{	/* Cfa/type.scm 541 */
					BgL_nodez00_bglt BgL_arg2039z00_6291;

					BgL_arg2039z00_6291 =
						(((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_6154)))->BgL_objz00);
					BgL_auxz00_7307 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2039z00_6291);
				}
				((((BgL_failz00_bglt) COBJECT(
								((BgL_failz00_bglt) BgL_nodez00_6154)))->BgL_objz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7307), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_failz00_bglt) BgL_nodez00_6154));
		}

	}



/* &type-node!-condition1692 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2condition1692z70zzcfa_typez00(obj_t
		BgL_envz00_6155, obj_t BgL_nodez00_6156)
	{
		{	/* Cfa/type.scm 530 */
			{

				{	/* Cfa/type.scm 530 */
					obj_t BgL_nextzd2method1691zd2_6294;

					BgL_nextzd2method1691zd2_6294 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_conditionalz00_bglt) BgL_nodez00_6156)),
						BGl_typezd2nodez12zd2envz12zzcfa_typez00,
						BGl_conditionalzf2Cinfozf2zzcfa_infoz00);
					BGL_PROCEDURE_CALL1(BgL_nextzd2method1691zd2_6294,
						((obj_t) ((BgL_conditionalz00_bglt) BgL_nodez00_6156)));
				}
				return
					((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_6156));
			}
		}

	}



/* &type-node!-condition1690 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2condition1690z70zzcfa_typez00(obj_t
		BgL_envz00_6157, obj_t BgL_nodez00_6158)
	{
		{	/* Cfa/type.scm 519 */
			{
				BgL_nodez00_bglt BgL_auxz00_7326;

				{	/* Cfa/type.scm 521 */
					BgL_nodez00_bglt BgL_arg2033z00_6296;

					BgL_arg2033z00_6296 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_6158)))->BgL_testz00);
					BgL_auxz00_7326 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2033z00_6296);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_6158)))->BgL_testz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7326), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_7332;

				{	/* Cfa/type.scm 522 */
					BgL_nodez00_bglt BgL_arg2034z00_6297;

					BgL_arg2034z00_6297 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_6158)))->BgL_truez00);
					BgL_auxz00_7332 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2034z00_6297);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_6158)))->BgL_truez00) =
					((BgL_nodez00_bglt) BgL_auxz00_7332), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_7338;

				{	/* Cfa/type.scm 523 */
					BgL_nodez00_bglt BgL_arg2036z00_6298;

					BgL_arg2036z00_6298 =
						(((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_6158)))->BgL_falsez00);
					BgL_auxz00_7338 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2036z00_6298);
				}
				((((BgL_conditionalz00_bglt) COBJECT(
								((BgL_conditionalz00_bglt) BgL_nodez00_6158)))->BgL_falsez00) =
					((BgL_nodez00_bglt) BgL_auxz00_7338), BUNSPEC);
			}
			((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_conditionalz00_bglt) BgL_nodez00_6158))))->BgL_typez00) =
				((BgL_typez00_bglt)
					BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt) (
								(BgL_conditionalz00_bglt) BgL_nodez00_6158)), ((bool_t) 0))),
				BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_conditionalz00_bglt) BgL_nodez00_6158));
		}

	}



/* &type-node!-setq1688 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2setq1688z70zzcfa_typez00(obj_t
		BgL_envz00_6159, obj_t BgL_nodez00_6160)
	{
		{	/* Cfa/type.scm 508 */
			{
				BgL_nodez00_bglt BgL_auxz00_7352;

				{	/* Cfa/type.scm 510 */
					BgL_nodez00_bglt BgL_arg2030z00_6300;

					BgL_arg2030z00_6300 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_6160)))->BgL_valuez00);
					BgL_auxz00_7352 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2030z00_6300);
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_6160)))->BgL_valuez00) =
					((BgL_nodez00_bglt) BgL_auxz00_7352), BUNSPEC);
			}
			{
				BgL_varz00_bglt BgL_auxz00_7358;

				{	/* Cfa/type.scm 511 */
					BgL_varz00_bglt BgL_arg2031z00_6301;

					BgL_arg2031z00_6301 =
						(((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_6160)))->BgL_varz00);
					BgL_auxz00_7358 =
						((BgL_varz00_bglt)
						BGl_typezd2nodez12zc0zzcfa_typez00(
							((BgL_nodez00_bglt) BgL_arg2031z00_6301)));
				}
				((((BgL_setqz00_bglt) COBJECT(
								((BgL_setqz00_bglt) BgL_nodez00_6160)))->BgL_varz00) =
					((BgL_varz00_bglt) BgL_auxz00_7358), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_setqz00_bglt) BgL_nodez00_6160));
		}

	}



/* &type-node!-cast1686 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2cast1686z70zzcfa_typez00(obj_t
		BgL_envz00_6161, obj_t BgL_nodez00_6162)
	{
		{	/* Cfa/type.scm 500 */
			{
				BgL_nodez00_bglt BgL_auxz00_7368;

				{	/* Cfa/type.scm 502 */
					BgL_nodez00_bglt BgL_arg2029z00_6303;

					BgL_arg2029z00_6303 =
						(((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_6162)))->BgL_argz00);
					BgL_auxz00_7368 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2029z00_6303);
				}
				((((BgL_castz00_bglt) COBJECT(
								((BgL_castz00_bglt) BgL_nodez00_6162)))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7368), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_castz00_bglt) BgL_nodez00_6162));
		}

	}



/* &type-node!-vset!1684 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2vsetz121684z62zzcfa_typez00(obj_t
		BgL_envz00_6163, obj_t BgL_nodez00_6164)
	{
		{	/* Cfa/type.scm 490 */
			{

				{	/* Cfa/type.scm 490 */
					obj_t BgL_nextzd2method1683zd2_6306;

					BgL_nextzd2method1683zd2_6306 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_vsetz12z12_bglt) BgL_nodez00_6164)),
						BGl_typezd2nodez12zd2envz12zzcfa_typez00,
						BGl_vsetz12z12zzast_nodez00);
					BGL_PROCEDURE_CALL1(BgL_nextzd2method1683zd2_6306,
						((obj_t) ((BgL_vsetz12z12_bglt) BgL_nodez00_6164)));
				}
				{	/* Cfa/type.scm 493 */
					bool_t BgL_test2494z00_7385;

					{	/* Cfa/type.scm 493 */
						BgL_typez00_bglt BgL_arg2027z00_6307;

						BgL_arg2027z00_6307 =
							(((BgL_vsetz12z12_bglt) COBJECT(
									((BgL_vsetz12z12_bglt) BgL_nodez00_6164)))->BgL_ftypez00);
						BgL_test2494z00_7385 =
							(((obj_t) BgL_arg2027z00_6307) == BGl_za2_za2z00zztype_cachez00);
					}
					if (BgL_test2494z00_7385)
						{	/* Cfa/type.scm 493 */
							((((BgL_vsetz12z12_bglt) COBJECT(
											((BgL_vsetz12z12_bglt) BgL_nodez00_6164)))->
									BgL_ftypez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
						}
					else
						{	/* Cfa/type.scm 493 */
							BFALSE;
						}
				}
				return ((BgL_nodez00_bglt) ((BgL_vsetz12z12_bglt) BgL_nodez00_6164));
			}
		}

	}



/* &type-node!-vref1682 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2vref1682z70zzcfa_typez00(obj_t
		BgL_envz00_6165, obj_t BgL_nodez00_6166)
	{
		{	/* Cfa/type.scm 479 */
			{

				{	/* Cfa/type.scm 479 */
					obj_t BgL_nextzd2method1681zd2_6310;

					BgL_nextzd2method1681zd2_6310 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_vrefz00_bglt) BgL_nodez00_6166)),
						BGl_typezd2nodez12zd2envz12zzcfa_typez00, BGl_vrefz00zzast_nodez00);
					BGL_PROCEDURE_CALL1(BgL_nextzd2method1681zd2_6310,
						((obj_t) ((BgL_vrefz00_bglt) BgL_nodez00_6166)));
				}
				{	/* Cfa/type.scm 482 */
					bool_t BgL_test2495z00_7404;

					{	/* Cfa/type.scm 482 */
						BgL_typez00_bglt BgL_arg2024z00_6311;

						BgL_arg2024z00_6311 =
							(((BgL_vrefz00_bglt) COBJECT(
									((BgL_vrefz00_bglt) BgL_nodez00_6166)))->BgL_ftypez00);
						BgL_test2495z00_7404 =
							(((obj_t) BgL_arg2024z00_6311) == BGl_za2_za2z00zztype_cachez00);
					}
					if (BgL_test2495z00_7404)
						{	/* Cfa/type.scm 482 */
							((((BgL_vrefz00_bglt) COBJECT(
											((BgL_vrefz00_bglt) BgL_nodez00_6166)))->BgL_ftypez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
						}
					else
						{	/* Cfa/type.scm 482 */
							BFALSE;
						}
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_vrefz00_bglt) BgL_nodez00_6166))))->BgL_typez00) =
					((BgL_typez00_bglt) (((BgL_vrefz00_bglt) COBJECT(((BgL_vrefz00_bglt)
										BgL_nodez00_6166)))->BgL_ftypez00)), BUNSPEC);
				return ((BgL_nodez00_bglt) ((BgL_vrefz00_bglt) BgL_nodez00_6166));
			}
		}

	}



/* &type-node!-valloc1680 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2valloc1680z70zzcfa_typez00(obj_t
		BgL_envz00_6167, obj_t BgL_nodez00_6168)
	{
		{	/* Cfa/type.scm 468 */
			{

				{	/* Cfa/type.scm 468 */
					obj_t BgL_nextzd2method1679zd2_6314;

					BgL_nextzd2method1679zd2_6314 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_vallocz00_bglt) BgL_nodez00_6168)),
						BGl_typezd2nodez12zd2envz12zzcfa_typez00,
						BGl_vallocz00zzast_nodez00);
					BGL_PROCEDURE_CALL1(BgL_nextzd2method1679zd2_6314,
						((obj_t) ((BgL_vallocz00_bglt) BgL_nodez00_6168)));
				}
				{	/* Cfa/type.scm 471 */
					obj_t BgL_arg2012z00_6315;

					BgL_arg2012z00_6315 =
						(((BgL_externz00_bglt) COBJECT(
								((BgL_externz00_bglt)
									((BgL_vallocz00_bglt) BgL_nodez00_6168))))->BgL_exprza2za2);
					BGl_typezd2nodeza2z12z62zzcfa_typez00(BgL_arg2012z00_6315);
				}
				{
					BgL_typez00_bglt BgL_auxz00_7432;

					{	/* Cfa/type.scm 472 */
						obj_t BgL_arg2013z00_6316;
						BgL_typez00_bglt BgL_arg2014z00_6317;

						{	/* Cfa/type.scm 472 */
							bool_t BgL_test2496z00_7435;

							{	/* Cfa/type.scm 472 */
								BgL_typez00_bglt BgL_arg2017z00_6318;

								BgL_arg2017z00_6318 =
									(((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_vallocz00_bglt) BgL_nodez00_6168))))->
									BgL_typez00);
								BgL_test2496z00_7435 =
									(((obj_t) BgL_arg2017z00_6318) ==
									BGl_za2_za2z00zztype_cachez00);
							}
							if (BgL_test2496z00_7435)
								{	/* Cfa/type.scm 472 */
									BgL_arg2013z00_6316 = BGl_za2vectorza2z00zztype_cachez00;
								}
							else
								{	/* Cfa/type.scm 472 */
									BgL_arg2013z00_6316 =
										((obj_t)
										(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_vallocz00_bglt) BgL_nodez00_6168))))->
											BgL_typez00));
								}
						}
						BgL_arg2014z00_6317 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_vallocz00_bglt) BgL_nodez00_6168))))->BgL_typez00);
						BgL_auxz00_7432 =
							BGl_strictzd2nodezd2typez00zzast_nodez00(
							((BgL_typez00_bglt) BgL_arg2013z00_6316), BgL_arg2014z00_6317);
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_vallocz00_bglt) BgL_nodez00_6168))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_7432), BUNSPEC);
				}
				{
					BgL_typez00_bglt BgL_auxz00_7451;

					{	/* Cfa/type.scm 473 */
						bool_t BgL_test2497z00_7453;

						{	/* Cfa/type.scm 473 */
							BgL_typez00_bglt BgL_arg2020z00_6319;

							BgL_arg2020z00_6319 =
								(((BgL_vallocz00_bglt) COBJECT(
										((BgL_vallocz00_bglt) BgL_nodez00_6168)))->BgL_ftypez00);
							BgL_test2497z00_7453 =
								(
								((obj_t) BgL_arg2020z00_6319) == BGl_za2_za2z00zztype_cachez00);
						}
						if (BgL_test2497z00_7453)
							{	/* Cfa/type.scm 473 */
								BgL_auxz00_7451 =
									((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
							}
						else
							{	/* Cfa/type.scm 473 */
								BgL_auxz00_7451 =
									(((BgL_vallocz00_bglt) COBJECT(
											((BgL_vallocz00_bglt) BgL_nodez00_6168)))->BgL_ftypez00);
							}
					}
					((((BgL_vallocz00_bglt) COBJECT(
									((BgL_vallocz00_bglt) BgL_nodez00_6168)))->BgL_ftypez00) =
						((BgL_typez00_bglt) BgL_auxz00_7451), BUNSPEC);
				}
				return ((BgL_nodez00_bglt) ((BgL_vallocz00_bglt) BgL_nodez00_6168));
			}
		}

	}



/* &type-node!-extern1678 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2extern1678z70zzcfa_typez00(obj_t
		BgL_envz00_6169, obj_t BgL_nodez00_6170)
	{
		{	/* Cfa/type.scm 460 */
			BGl_typezd2nodeza2z12z62zzcfa_typez00(
				(((BgL_externz00_bglt) COBJECT(
							((BgL_externz00_bglt) BgL_nodez00_6170)))->BgL_exprza2za2));
			return ((BgL_nodez00_bglt) ((BgL_externz00_bglt) BgL_nodez00_6170));
		}

	}



/* &type-node!-funcall/C1676 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2funcallzf2C1676z82zzcfa_typez00(obj_t
		BgL_envz00_6171, obj_t BgL_nodez00_6172)
	{
		{	/* Cfa/type.scm 434 */
			{
				BgL_nodez00_bglt BgL_auxz00_7469;

				{	/* Cfa/type.scm 436 */
					BgL_nodez00_bglt BgL_arg2004z00_6322;

					BgL_arg2004z00_6322 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt)
									((BgL_funcallz00_bglt) BgL_nodez00_6172))))->BgL_funz00);
					BgL_auxz00_7469 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2004z00_6322);
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt)
									((BgL_funcallz00_bglt) BgL_nodez00_6172))))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7469), BUNSPEC);
			}
			{	/* Cfa/type.scm 437 */
				obj_t BgL_arg2006z00_6323;

				BgL_arg2006z00_6323 =
					(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt)
								((BgL_funcallz00_bglt) BgL_nodez00_6172))))->BgL_argsz00);
				BGl_typezd2nodeza2z12z62zzcfa_typez00(BgL_arg2006z00_6323);
			}
			if (CBOOL
				(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00))
				{	/* Cfa/type.scm 439 */
					obj_t BgL_typz00_6324;

					{	/* Cfa/type.scm 439 */
						BgL_approxz00_bglt BgL_arg2010z00_6325;

						{
							BgL_funcallzf2cinfozf2_bglt BgL_auxz00_7483;

							{
								obj_t BgL_auxz00_7484;

								{	/* Cfa/type.scm 439 */
									BgL_objectz00_bglt BgL_tmpz00_7485;

									BgL_tmpz00_7485 =
										((BgL_objectz00_bglt)
										((BgL_funcallz00_bglt) BgL_nodez00_6172));
									BgL_auxz00_7484 = BGL_OBJECT_WIDENING(BgL_tmpz00_7485);
								}
								BgL_auxz00_7483 =
									((BgL_funcallzf2cinfozf2_bglt) BgL_auxz00_7484);
							}
							BgL_arg2010z00_6325 =
								(((BgL_funcallzf2cinfozf2_bglt) COBJECT(BgL_auxz00_7483))->
								BgL_approxz00);
						}
						BgL_typz00_6324 =
							BGl_getzd2approxzd2typez00zzcfa_typez00(BgL_arg2010z00_6325,
							((obj_t) ((BgL_funcallz00_bglt) BgL_nodez00_6172)));
					}
					if ((BgL_typz00_6324 == BGl_za2_za2z00zztype_cachez00))
						{	/* Cfa/type.scm 440 */
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_funcallz00_bglt) BgL_nodez00_6172))))->
									BgL_typez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
							return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt)
									BgL_nodez00_6172));
						}
					else
						{	/* Cfa/type.scm 440 */
							if (CBOOL(BGl_memqz00zz__r4_pairs_and_lists_6_3z00(
										(((BgL_funcallz00_bglt) COBJECT(
													((BgL_funcallz00_bglt)
														((BgL_funcallz00_bglt) BgL_nodez00_6172))))->
											BgL_strengthz00), CNST_TABLE_REF(2))))
								{	/* Cfa/type.scm 449 */
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_funcallz00_bglt) BgL_nodez00_6172))))->
											BgL_typez00) =
										((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_typz00_6324)),
										BUNSPEC);
								}
							else
								{	/* Cfa/type.scm 449 */
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_funcallz00_bglt) BgL_nodez00_6172))))->
											BgL_typez00) =
										((BgL_typez00_bglt)
											BGl_getzd2bigloozd2typez00zztype_cachez00((
													(BgL_typez00_bglt) BgL_typz00_6324))), BUNSPEC);
								}
							return
								((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_6172));
						}
				}
			else
				{	/* Cfa/type.scm 438 */
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_funcallz00_bglt) BgL_nodez00_6172))))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt)
								BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
					return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_6172));
				}
		}

	}



/* &type-node!-funcall1674 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2funcall1674z70zzcfa_typez00(obj_t
		BgL_envz00_6173, obj_t BgL_nodez00_6174)
	{
		{	/* Cfa/type.scm 423 */
			if (CBOOL(BGl_za2optimzd2cfazf3za2z21zzengine_paramz00))
				{	/* Cfa/type.scm 424 */
					obj_t BgL_arg2001z00_6327;

					BgL_arg2001z00_6327 =
						BGl_shapez00zztools_shapez00(
						((obj_t) ((BgL_funcallz00_bglt) BgL_nodez00_6174)));
					BGl_errorz00zz__errorz00(BGl_string2329z00zzcfa_typez00,
						BGl_string2361z00zzcfa_typez00, BgL_arg2001z00_6327);
				}
			else
				{	/* Cfa/type.scm 424 */
					BFALSE;
				}
			{
				BgL_nodez00_bglt BgL_auxz00_7532;

				{	/* Cfa/type.scm 426 */
					BgL_nodez00_bglt BgL_arg2002z00_6328;

					BgL_arg2002z00_6328 =
						(((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_6174)))->BgL_funz00);
					BgL_auxz00_7532 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg2002z00_6328);
				}
				((((BgL_funcallz00_bglt) COBJECT(
								((BgL_funcallz00_bglt) BgL_nodez00_6174)))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7532), BUNSPEC);
			}
			BGl_typezd2nodeza2z12z62zzcfa_typez00(
				(((BgL_funcallz00_bglt) COBJECT(
							((BgL_funcallz00_bglt) BgL_nodez00_6174)))->BgL_argsz00));
			((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_funcallz00_bglt) BgL_nodez00_6174))))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt)
						BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_funcallz00_bglt) BgL_nodez00_6174));
		}

	}



/* &type-node!-app-ly/Ci1672 */
	BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2appzd2lyzf2Ci1672z50zzcfa_typez00(obj_t
		BgL_envz00_6175, obj_t BgL_nodez00_6176)
	{
		{	/* Cfa/type.scm 411 */
			{
				BgL_nodez00_bglt BgL_auxz00_7547;

				{	/* Cfa/type.scm 413 */
					BgL_nodez00_bglt BgL_arg1998z00_6330;

					BgL_arg1998z00_6330 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt)
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_6176))))->BgL_funz00);
					BgL_auxz00_7547 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg1998z00_6330);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt)
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_6176))))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7547), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_7555;

				{	/* Cfa/type.scm 414 */
					BgL_nodez00_bglt BgL_arg1999z00_6331;

					BgL_arg1999z00_6331 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt)
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_6176))))->BgL_argz00);
					BgL_auxz00_7555 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg1999z00_6331);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt)
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_6176))))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7555), BUNSPEC);
			}
			if (CBOOL
				(BGl_za2optimzd2cfazd2unboxzd2closurezd2argsza2z00zzengine_paramz00))
				{
					BgL_typez00_bglt BgL_auxz00_7565;

					{	/* Cfa/type.scm 416 */
						BgL_approxz00_bglt BgL_arg2000z00_6332;

						{
							BgL_appzd2lyzf2cinfoz20_bglt BgL_auxz00_7568;

							{
								obj_t BgL_auxz00_7569;

								{	/* Cfa/type.scm 416 */
									BgL_objectz00_bglt BgL_tmpz00_7570;

									BgL_tmpz00_7570 =
										((BgL_objectz00_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_6176));
									BgL_auxz00_7569 = BGL_OBJECT_WIDENING(BgL_tmpz00_7570);
								}
								BgL_auxz00_7568 =
									((BgL_appzd2lyzf2cinfoz20_bglt) BgL_auxz00_7569);
							}
							BgL_arg2000z00_6332 =
								(((BgL_appzd2lyzf2cinfoz20_bglt) COBJECT(BgL_auxz00_7568))->
								BgL_approxz00);
						}
						BgL_auxz00_7565 =
							((BgL_typez00_bglt)
							BGl_getzd2approxzd2typez00zzcfa_typez00(BgL_arg2000z00_6332,
								((obj_t) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_6176))));
					}
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_6176))))->BgL_typez00) =
						((BgL_typez00_bglt) BgL_auxz00_7565), BUNSPEC);
				}
			else
				{	/* Cfa/type.scm 415 */
					((((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_6176))))->BgL_typez00) =
						((BgL_typez00_bglt) ((BgL_typez00_bglt)
								BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
				}
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_6176));
		}

	}



/* &type-node!-app-ly1670 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2appzd2ly1670za2zzcfa_typez00(obj_t
		BgL_envz00_6177, obj_t BgL_nodez00_6178)
	{
		{	/* Cfa/type.scm 400 */
			if (CBOOL(BGl_za2optimzd2cfazf3za2z21zzengine_paramz00))
				{	/* Cfa/type.scm 401 */
					obj_t BgL_arg1995z00_6334;

					BgL_arg1995z00_6334 =
						BGl_shapez00zztools_shapez00(
						((obj_t) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_6178)));
					BGl_errorz00zz__errorz00(BGl_string2329z00zzcfa_typez00,
						BGl_string2361z00zzcfa_typez00, BgL_arg1995z00_6334);
				}
			else
				{	/* Cfa/type.scm 401 */
					BFALSE;
				}
			{
				BgL_nodez00_bglt BgL_auxz00_7593;

				{	/* Cfa/type.scm 403 */
					BgL_nodez00_bglt BgL_arg1996z00_6335;

					BgL_arg1996z00_6335 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt)
									((BgL_appzd2lyzd2_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_6178)))))->BgL_funz00);
					BgL_auxz00_7593 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg1996z00_6335);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt)
									((BgL_appzd2lyzd2_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_6178)))))->BgL_funz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7593), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_7603;

				{	/* Cfa/type.scm 404 */
					BgL_nodez00_bglt BgL_arg1997z00_6336;

					BgL_arg1997z00_6336 =
						(((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt)
									((BgL_appzd2lyzd2_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_6178)))))->BgL_argz00);
					BgL_auxz00_7603 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg1997z00_6336);
				}
				((((BgL_appzd2lyzd2_bglt) COBJECT(
								((BgL_appzd2lyzd2_bglt)
									((BgL_appzd2lyzd2_bglt)
										((BgL_appzd2lyzd2_bglt) BgL_nodez00_6178)))))->BgL_argz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7603), BUNSPEC);
			}
			((((BgL_nodez00_bglt) COBJECT(
							((BgL_nodez00_bglt)
								((BgL_appzd2lyzd2_bglt)
									((BgL_appzd2lyzd2_bglt) BgL_nodez00_6178)))))->BgL_typez00) =
				((BgL_typez00_bglt) ((BgL_typez00_bglt)
						BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
			return ((BgL_nodez00_bglt) ((BgL_appzd2lyzd2_bglt) BgL_nodez00_6178));
		}

	}



/* &type-node!-cons-ref-1668 */
	BgL_nodez00_bglt
		BGl_z62typezd2nodez12zd2conszd2refzd21668z70zzcfa_typez00(obj_t
		BgL_envz00_6179, obj_t BgL_nodez00_6180)
	{
		{	/* Cfa/type.scm 389 */
			{

				{	/* Cfa/type.scm 389 */
					obj_t BgL_nextzd2method1667zd2_6339;

					BgL_nextzd2method1667zd2_6339 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_appz00_bglt) BgL_nodez00_6180)),
						BGl_typezd2nodez12zd2envz12zzcfa_typez00,
						BGl_conszd2refzd2appz00zzcfa_info2z00);
					BGL_PROCEDURE_CALL1(BgL_nextzd2method1667zd2_6339,
						((obj_t) ((BgL_appz00_bglt) BgL_nodez00_6180)));
				}
				{	/* Cfa/type.scm 392 */
					obj_t BgL_atypez00_6340;

					{	/* Cfa/type.scm 392 */
						BgL_approxz00_bglt BgL_arg1994z00_6341;

						{
							BgL_conszd2refzd2appz00_bglt BgL_auxz00_7629;

							{
								obj_t BgL_auxz00_7630;

								{	/* Cfa/type.scm 392 */
									BgL_objectz00_bglt BgL_tmpz00_7631;

									BgL_tmpz00_7631 =
										((BgL_objectz00_bglt) ((BgL_appz00_bglt) BgL_nodez00_6180));
									BgL_auxz00_7630 = BGL_OBJECT_WIDENING(BgL_tmpz00_7631);
								}
								BgL_auxz00_7629 =
									((BgL_conszd2refzd2appz00_bglt) BgL_auxz00_7630);
							}
							BgL_arg1994z00_6341 =
								(((BgL_conszd2refzd2appz00_bglt) COBJECT(BgL_auxz00_7629))->
								BgL_approxz00);
						}
						BgL_atypez00_6340 =
							BGl_getzd2approxzd2typez00zzcfa_typez00(BgL_arg1994z00_6341,
							((obj_t) ((BgL_appz00_bglt) BgL_nodez00_6180)));
					}
					if ((BgL_atypez00_6340 == BGl_za2_za2z00zztype_cachez00))
						{	/* Cfa/type.scm 393 */
							BFALSE;
						}
					else
						{	/* Cfa/type.scm 393 */
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_6180))))->BgL_typez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_atypez00_6340)),
								BUNSPEC);
						}
				}
				return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_6180));
			}
		}

	}



/* &type-node!-procedure1666 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2procedure1666z70zzcfa_typez00(obj_t
		BgL_envz00_6181, obj_t BgL_nodez00_6182)
	{
		{	/* Cfa/type.scm 372 */
			{

				{	/* Cfa/type.scm 372 */
					obj_t BgL_nextzd2method1665zd2_6344;

					BgL_nextzd2method1665zd2_6344 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_appz00_bglt) BgL_nodez00_6182)),
						BGl_typezd2nodez12zd2envz12zzcfa_typez00,
						BGl_procedurezd2setz12zd2appz12zzcfa_info2z00);
					BGL_PROCEDURE_CALL1(BgL_nextzd2method1665zd2_6344,
						((obj_t) ((BgL_appz00_bglt) BgL_nodez00_6182)));
				}
				if (CBOOL
					(BGl_za2optimzd2cfazd2freezd2varzd2trackingzf3za2zf3zzengine_paramz00))
					{	/* Cfa/type.scm 376 */
						obj_t BgL_atypez00_6345;

						{	/* Cfa/type.scm 376 */
							obj_t BgL_arg1993z00_6346;

							{
								BgL_procedurezd2setz12zd2appz12_bglt BgL_auxz00_7659;

								{
									obj_t BgL_auxz00_7660;

									{	/* Cfa/type.scm 376 */
										BgL_objectz00_bglt BgL_tmpz00_7661;

										BgL_tmpz00_7661 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_6182));
										BgL_auxz00_7660 = BGL_OBJECT_WIDENING(BgL_tmpz00_7661);
									}
									BgL_auxz00_7659 =
										((BgL_procedurezd2setz12zd2appz12_bglt) BgL_auxz00_7660);
								}
								BgL_arg1993z00_6346 =
									(((BgL_procedurezd2setz12zd2appz12_bglt)
										COBJECT(BgL_auxz00_7659))->BgL_vapproxz00);
							}
							if (BGl_approxzd2procedurezd2elzf3zf3zzcfa_closurez00(
									((BgL_approxz00_bglt) BgL_arg1993z00_6346)))
								{	/* Cfa/type.scm 348 */
									BgL_atypez00_6345 =
										BGl_za2procedurezd2elza2zd2zztype_cachez00;
								}
							else
								{	/* Cfa/type.scm 348 */
									BgL_atypez00_6345 =
										BGl_getzd2approxzd2typez00zzcfa_typez00(
										((BgL_approxz00_bglt) BgL_arg1993z00_6346),
										((obj_t) ((BgL_appz00_bglt) BgL_nodez00_6182)));
								}
						}
						{	/* Cfa/type.scm 377 */
							bool_t BgL_test2507z00_7674;

							if (BGl_bigloozd2typezf3z21zztype_typez00(
									((BgL_typez00_bglt) BgL_atypez00_6345)))
								{	/* Cfa/type.scm 377 */
									if ((BgL_atypez00_6345 == BGl_za2_za2z00zztype_cachez00))
										{	/* Cfa/type.scm 378 */
											BgL_test2507z00_7674 = ((bool_t) 0);
										}
									else
										{	/* Cfa/type.scm 379 */
											bool_t BgL_test2510z00_7680;

											{	/* Cfa/type.scm 379 */
												BgL_typez00_bglt BgL_arg1990z00_6347;

												{	/* Cfa/type.scm 379 */
													obj_t BgL_arg1991z00_6348;

													{	/* Cfa/type.scm 379 */
														obj_t BgL_pairz00_6349;

														BgL_pairz00_6349 =
															(((BgL_appz00_bglt) COBJECT(
																	((BgL_appz00_bglt)
																		((BgL_appz00_bglt) BgL_nodez00_6182))))->
															BgL_argsz00);
														BgL_arg1991z00_6348 =
															CAR(CDR(CDR(BgL_pairz00_6349)));
													}
													BgL_arg1990z00_6347 =
														BGl_getzd2typezd2zztype_typeofz00(
														((BgL_nodez00_bglt) BgL_arg1991z00_6348),
														((bool_t) 0));
												}
												BgL_test2510z00_7680 =
													(BgL_atypez00_6345 == ((obj_t) BgL_arg1990z00_6347));
											}
											if (BgL_test2510z00_7680)
												{	/* Cfa/type.scm 379 */
													BgL_test2507z00_7674 = ((bool_t) 0);
												}
											else
												{	/* Cfa/type.scm 379 */
													BgL_test2507z00_7674 = ((bool_t) 1);
												}
										}
								}
							else
								{	/* Cfa/type.scm 377 */
									BgL_test2507z00_7674 = ((bool_t) 0);
								}
							if (BgL_test2507z00_7674)
								{	/* Cfa/type.scm 380 */
									obj_t BgL_arg1985z00_6350;
									BgL_castz00_bglt BgL_arg1986z00_6351;

									{	/* Cfa/type.scm 380 */
										obj_t BgL_pairz00_6352;

										BgL_pairz00_6352 =
											(((BgL_appz00_bglt) COBJECT(
													((BgL_appz00_bglt)
														((BgL_appz00_bglt) BgL_nodez00_6182))))->
											BgL_argsz00);
										BgL_arg1985z00_6350 = CDR(CDR(BgL_pairz00_6352));
									}
									{	/* Cfa/type.scm 381 */
										BgL_castz00_bglt BgL_new1201z00_6353;

										{	/* Cfa/type.scm 381 */
											BgL_castz00_bglt BgL_new1200z00_6354;

											BgL_new1200z00_6354 =
												((BgL_castz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
															BgL_castz00_bgl))));
											{	/* Cfa/type.scm 381 */
												long BgL_arg1989z00_6355;

												BgL_arg1989z00_6355 =
													BGL_CLASS_NUM(BGl_castz00zzast_nodez00);
												BGL_OBJECT_CLASS_NUM_SET(
													((BgL_objectz00_bglt) BgL_new1200z00_6354),
													BgL_arg1989z00_6355);
											}
											{	/* Cfa/type.scm 381 */
												BgL_objectz00_bglt BgL_tmpz00_7700;

												BgL_tmpz00_7700 =
													((BgL_objectz00_bglt) BgL_new1200z00_6354);
												BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7700, BFALSE);
											}
											((BgL_objectz00_bglt) BgL_new1200z00_6354);
											BgL_new1201z00_6353 = BgL_new1200z00_6354;
										}
										((((BgL_nodez00_bglt) COBJECT(
														((BgL_nodez00_bglt) BgL_new1201z00_6353)))->
												BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
										((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
															BgL_new1201z00_6353)))->BgL_typez00) =
											((BgL_typez00_bglt) ((BgL_typez00_bglt)
													BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
										{
											BgL_nodez00_bglt BgL_auxz00_7709;

											{	/* Cfa/type.scm 383 */
												obj_t BgL_pairz00_6356;

												BgL_pairz00_6356 =
													(((BgL_appz00_bglt) COBJECT(
															((BgL_appz00_bglt)
																((BgL_appz00_bglt) BgL_nodez00_6182))))->
													BgL_argsz00);
												{	/* Cfa/type.scm 383 */
													obj_t BgL_pairz00_6357;

													{	/* Cfa/type.scm 383 */
														obj_t BgL_pairz00_6358;

														BgL_pairz00_6358 = CDR(BgL_pairz00_6356);
														BgL_pairz00_6357 = CDR(BgL_pairz00_6358);
													}
													BgL_auxz00_7709 =
														((BgL_nodez00_bglt) CAR(BgL_pairz00_6357));
											}}
											((((BgL_castz00_bglt) COBJECT(BgL_new1201z00_6353))->
													BgL_argz00) =
												((BgL_nodez00_bglt) BgL_auxz00_7709), BUNSPEC);
										}
										BgL_arg1986z00_6351 = BgL_new1201z00_6353;
									}
									{	/* Cfa/type.scm 380 */
										obj_t BgL_auxz00_7720;
										obj_t BgL_tmpz00_7718;

										BgL_auxz00_7720 = ((obj_t) BgL_arg1986z00_6351);
										BgL_tmpz00_7718 = ((obj_t) BgL_arg1985z00_6350);
										SET_CAR(BgL_tmpz00_7718, BgL_auxz00_7720);
								}}
							else
								{	/* Cfa/type.scm 377 */
									BFALSE;
								}
						}
					}
				else
					{	/* Cfa/type.scm 375 */
						BFALSE;
					}
				return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_6182));
			}
		}

	}



/* &type-node!-procedure1664 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2procedure1664z70zzcfa_typez00(obj_t
		BgL_envz00_6183, obj_t BgL_nodez00_6184)
	{
		{	/* Cfa/type.scm 355 */
			{

				{	/* Cfa/type.scm 355 */
					obj_t BgL_nextzd2method1663zd2_6361;

					BgL_nextzd2method1663zd2_6361 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_appz00_bglt) BgL_nodez00_6184)),
						BGl_typezd2nodez12zd2envz12zzcfa_typez00,
						BGl_procedurezd2refzd2appz00zzcfa_info2z00);
					BGL_PROCEDURE_CALL1(BgL_nextzd2method1663zd2_6361,
						((obj_t) ((BgL_appz00_bglt) BgL_nodez00_6184)));
				}
				if (CBOOL
					(BGl_za2optimzd2cfazd2freezd2varzd2trackingzf3za2zf3zzengine_paramz00))
					{	/* Cfa/type.scm 359 */
						obj_t BgL_atypez00_6362;

						{	/* Cfa/type.scm 359 */
							BgL_approxz00_bglt BgL_arg1975z00_6363;

							{
								BgL_procedurezd2refzd2appz00_bglt BgL_auxz00_7736;

								{
									obj_t BgL_auxz00_7737;

									{	/* Cfa/type.scm 359 */
										BgL_objectz00_bglt BgL_tmpz00_7738;

										BgL_tmpz00_7738 =
											((BgL_objectz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_6184));
										BgL_auxz00_7737 = BGL_OBJECT_WIDENING(BgL_tmpz00_7738);
									}
									BgL_auxz00_7736 =
										((BgL_procedurezd2refzd2appz00_bglt) BgL_auxz00_7737);
								}
								BgL_arg1975z00_6363 =
									(((BgL_procedurezd2refzd2appz00_bglt)
										COBJECT(BgL_auxz00_7736))->BgL_approxz00);
							}
							if (BGl_approxzd2procedurezd2elzf3zf3zzcfa_closurez00
								(BgL_arg1975z00_6363))
								{	/* Cfa/type.scm 348 */
									BgL_atypez00_6362 =
										BGl_za2procedurezd2elza2zd2zztype_cachez00;
								}
							else
								{	/* Cfa/type.scm 348 */
									BgL_atypez00_6362 =
										BGl_getzd2approxzd2typez00zzcfa_typez00(BgL_arg1975z00_6363,
										((obj_t) ((BgL_appz00_bglt) BgL_nodez00_6184)));
								}
						}
						{	/* Cfa/type.scm 360 */
							bool_t BgL_test2513z00_7749;

							if (BGl_bigloozd2typezf3z21zztype_typez00(
									((BgL_typez00_bglt) BgL_atypez00_6362)))
								{	/* Cfa/type.scm 360 */
									if ((BgL_atypez00_6362 == BGl_za2_za2z00zztype_cachez00))
										{	/* Cfa/type.scm 361 */
											BgL_test2513z00_7749 = ((bool_t) 0);
										}
									else
										{	/* Cfa/type.scm 362 */
											bool_t BgL_test2516z00_7755;

											{	/* Cfa/type.scm 362 */
												BgL_typez00_bglt BgL_arg1974z00_6364;

												BgL_arg1974z00_6364 =
													BGl_getzd2typezd2zztype_typeofz00(
													((BgL_nodez00_bglt)
														((BgL_appz00_bglt) BgL_nodez00_6184)),
													((bool_t) 0));
												BgL_test2516z00_7755 =
													(BgL_atypez00_6362 == ((obj_t) BgL_arg1974z00_6364));
											}
											if (BgL_test2516z00_7755)
												{	/* Cfa/type.scm 362 */
													BgL_test2513z00_7749 = ((bool_t) 0);
												}
											else
												{	/* Cfa/type.scm 362 */
													BgL_test2513z00_7749 = ((bool_t) 1);
												}
										}
								}
							else
								{	/* Cfa/type.scm 360 */
									BgL_test2513z00_7749 = ((bool_t) 0);
								}
							if (BgL_test2513z00_7749)
								{	/* Cfa/type.scm 363 */
									BgL_castz00_bglt BgL_new1197z00_6365;

									{	/* Cfa/type.scm 363 */
										BgL_castz00_bglt BgL_new1196z00_6366;

										BgL_new1196z00_6366 =
											((BgL_castz00_bglt) BOBJECT(GC_MALLOC(sizeof(struct
														BgL_castz00_bgl))));
										{	/* Cfa/type.scm 363 */
											long BgL_arg1973z00_6367;

											BgL_arg1973z00_6367 =
												BGL_CLASS_NUM(BGl_castz00zzast_nodez00);
											BGL_OBJECT_CLASS_NUM_SET(
												((BgL_objectz00_bglt) BgL_new1196z00_6366),
												BgL_arg1973z00_6367);
										}
										{	/* Cfa/type.scm 363 */
											BgL_objectz00_bglt BgL_tmpz00_7765;

											BgL_tmpz00_7765 =
												((BgL_objectz00_bglt) BgL_new1196z00_6366);
											BGL_OBJECT_WIDENING_SET(BgL_tmpz00_7765, BFALSE);
										}
										((BgL_objectz00_bglt) BgL_new1196z00_6366);
										BgL_new1197z00_6365 = BgL_new1196z00_6366;
									}
									((((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt) BgL_new1197z00_6365)))->
											BgL_locz00) = ((obj_t) BFALSE), BUNSPEC);
									((((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt)
														BgL_new1197z00_6365)))->BgL_typez00) =
										((BgL_typez00_bglt) ((BgL_typez00_bglt) BgL_atypez00_6362)),
										BUNSPEC);
									((((BgL_castz00_bglt) COBJECT(BgL_new1197z00_6365))->
											BgL_argz00) =
										((BgL_nodez00_bglt) ((BgL_nodez00_bglt) ((BgL_appz00_bglt)
													BgL_nodez00_6184))), BUNSPEC);
									return ((BgL_nodez00_bglt) BgL_new1197z00_6365);
								}
							else
								{	/* Cfa/type.scm 360 */
									return
										((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_6184));
								}
						}
					}
				else
					{	/* Cfa/type.scm 358 */
						return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_6184));
					}
			}
		}

	}



/* &type-node!-arithmeti1662 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2arithmeti1662z70zzcfa_typez00(obj_t
		BgL_envz00_6185, obj_t BgL_nodez00_6186)
	{
		{	/* Cfa/type.scm 311 */
			{

				{	/* Cfa/type.scm 311 */
					obj_t BgL_nextzd2method1661zd2_6370;

					BgL_nextzd2method1661zd2_6370 =
						BGl_findzd2superzd2classzd2methodzd2zz__objectz00(
						((BgL_objectz00_bglt)
							((BgL_appz00_bglt) BgL_nodez00_6186)),
						BGl_typezd2nodez12zd2envz12zzcfa_typez00,
						BGl_arithmeticzd2appzd2zzcfa_info2z00);
					BGL_PROCEDURE_CALL1(BgL_nextzd2method1661zd2_6370,
						((obj_t) ((BgL_appz00_bglt) BgL_nodez00_6186)));
				}
				{	/* Cfa/type.scm 328 */
					BgL_variablez00_bglt BgL_vz00_6371;

					BgL_vz00_6371 =
						(((BgL_varz00_bglt) COBJECT(
								(((BgL_appz00_bglt) COBJECT(
											((BgL_appz00_bglt)
												((BgL_appz00_bglt) BgL_nodez00_6186))))->BgL_funz00)))->
						BgL_variablez00);
					{	/* Cfa/type.scm 328 */
						BgL_valuez00_bglt BgL_valz00_6372;

						BgL_valz00_6372 =
							(((BgL_variablez00_bglt) COBJECT(BgL_vz00_6371))->BgL_valuez00);
						{	/* Cfa/type.scm 329 */

							{	/* Cfa/type.scm 330 */
								bool_t BgL_test2517z00_7796;

								{	/* Cfa/type.scm 330 */
									BgL_typez00_bglt BgL_arg1945z00_6373;

									BgL_arg1945z00_6373 =
										(((BgL_variablez00_bglt) COBJECT(BgL_vz00_6371))->
										BgL_typez00);
									BgL_test2517z00_7796 =
										(((obj_t) BgL_arg1945z00_6373) ==
										BGl_za2_za2z00zztype_cachez00);
								}
								if (BgL_test2517z00_7796)
									{	/* Cfa/type.scm 331 */
										BgL_typez00_bglt BgL_vz00_6374;

										BgL_vz00_6374 =
											((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
										((((BgL_variablez00_bglt) COBJECT(BgL_vz00_6371))->
												BgL_typez00) =
											((BgL_typez00_bglt) BgL_vz00_6374), BUNSPEC);
									}
								else
									{	/* Cfa/type.scm 330 */
										BFALSE;
									}
							}
							{	/* Cfa/type.scm 334 */
								bool_t BgL_test2518z00_7802;

								{	/* Cfa/type.scm 334 */
									obj_t BgL_classz00_6375;

									BgL_classz00_6375 = BGl_sfunz00zzast_varz00;
									{	/* Cfa/type.scm 334 */
										BgL_objectz00_bglt BgL_arg1807z00_6376;

										{	/* Cfa/type.scm 334 */
											obj_t BgL_tmpz00_7803;

											BgL_tmpz00_7803 =
												((obj_t) ((BgL_objectz00_bglt) BgL_valz00_6372));
											BgL_arg1807z00_6376 =
												(BgL_objectz00_bglt) (BgL_tmpz00_7803);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/type.scm 334 */
												long BgL_idxz00_6377;

												BgL_idxz00_6377 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6376);
												BgL_test2518z00_7802 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_6377 + 3L)) == BgL_classz00_6375);
											}
										else
											{	/* Cfa/type.scm 334 */
												bool_t BgL_res2300z00_6380;

												{	/* Cfa/type.scm 334 */
													obj_t BgL_oclassz00_6381;

													{	/* Cfa/type.scm 334 */
														obj_t BgL_arg1815z00_6382;
														long BgL_arg1816z00_6383;

														BgL_arg1815z00_6382 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/type.scm 334 */
															long BgL_arg1817z00_6384;

															BgL_arg1817z00_6384 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6376);
															BgL_arg1816z00_6383 =
																(BgL_arg1817z00_6384 - OBJECT_TYPE);
														}
														BgL_oclassz00_6381 =
															VECTOR_REF(BgL_arg1815z00_6382,
															BgL_arg1816z00_6383);
													}
													{	/* Cfa/type.scm 334 */
														bool_t BgL__ortest_1115z00_6385;

														BgL__ortest_1115z00_6385 =
															(BgL_classz00_6375 == BgL_oclassz00_6381);
														if (BgL__ortest_1115z00_6385)
															{	/* Cfa/type.scm 334 */
																BgL_res2300z00_6380 = BgL__ortest_1115z00_6385;
															}
														else
															{	/* Cfa/type.scm 334 */
																long BgL_odepthz00_6386;

																{	/* Cfa/type.scm 334 */
																	obj_t BgL_arg1804z00_6387;

																	BgL_arg1804z00_6387 = (BgL_oclassz00_6381);
																	BgL_odepthz00_6386 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_6387);
																}
																if ((3L < BgL_odepthz00_6386))
																	{	/* Cfa/type.scm 334 */
																		obj_t BgL_arg1802z00_6388;

																		{	/* Cfa/type.scm 334 */
																			obj_t BgL_arg1803z00_6389;

																			BgL_arg1803z00_6389 =
																				(BgL_oclassz00_6381);
																			BgL_arg1802z00_6388 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_6389, 3L);
																		}
																		BgL_res2300z00_6380 =
																			(BgL_arg1802z00_6388 ==
																			BgL_classz00_6375);
																	}
																else
																	{	/* Cfa/type.scm 334 */
																		BgL_res2300z00_6380 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2518z00_7802 = BgL_res2300z00_6380;
											}
									}
								}
								if (BgL_test2518z00_7802)
									{	/* Cfa/type.scm 335 */
										obj_t BgL_l01616z00_6390;

										BgL_l01616z00_6390 =
											(((BgL_sfunz00_bglt) COBJECT(
													((BgL_sfunz00_bglt) BgL_valz00_6372)))->BgL_argsz00);
										{
											obj_t BgL_l1615z00_6392;

											BgL_l1615z00_6392 = BgL_l01616z00_6390;
										BgL_zc3z04anonymousza31947ze3z87_6391:
											if (NULLP(BgL_l1615z00_6392))
												{	/* Cfa/type.scm 335 */
													BgL_l01616z00_6390;
												}
											else
												{	/* Cfa/type.scm 335 */
													{	/* Cfa/type.scm 335 */
														obj_t BgL_arg1949z00_6393;

														{	/* Cfa/type.scm 335 */
															obj_t BgL_arg1950z00_6394;

															BgL_arg1950z00_6394 =
																CAR(((obj_t) BgL_l1615z00_6392));
															BgL_arg1949z00_6393 =
																BGl_cleanupzd2typeze70z35zzcfa_typez00
																(BgL_arg1950z00_6394);
														}
														{	/* Cfa/type.scm 335 */
															obj_t BgL_tmpz00_7833;

															BgL_tmpz00_7833 = ((obj_t) BgL_l1615z00_6392);
															SET_CAR(BgL_tmpz00_7833, BgL_arg1949z00_6393);
														}
													}
													{	/* Cfa/type.scm 335 */
														obj_t BgL_arg1951z00_6395;

														BgL_arg1951z00_6395 =
															CDR(((obj_t) BgL_l1615z00_6392));
														{
															obj_t BgL_l1615z00_7838;

															BgL_l1615z00_7838 = BgL_arg1951z00_6395;
															BgL_l1615z00_6392 = BgL_l1615z00_7838;
															goto BgL_zc3z04anonymousza31947ze3z87_6391;
														}
													}
												}
										}
									}
								else
									{	/* Cfa/type.scm 336 */
										bool_t BgL_test2523z00_7839;

										{	/* Cfa/type.scm 336 */
											obj_t BgL_classz00_6396;

											BgL_classz00_6396 = BGl_cfunz00zzast_varz00;
											{	/* Cfa/type.scm 336 */
												BgL_objectz00_bglt BgL_arg1807z00_6397;

												{	/* Cfa/type.scm 336 */
													obj_t BgL_tmpz00_7840;

													BgL_tmpz00_7840 =
														((obj_t) ((BgL_objectz00_bglt) BgL_valz00_6372));
													BgL_arg1807z00_6397 =
														(BgL_objectz00_bglt) (BgL_tmpz00_7840);
												}
												if (BGL_CONDEXPAND_ISA_ARCH64())
													{	/* Cfa/type.scm 336 */
														long BgL_idxz00_6398;

														BgL_idxz00_6398 =
															BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6397);
														BgL_test2523z00_7839 =
															(VECTOR_REF
															(BGl_za2inheritancesza2z00zz__objectz00,
																(BgL_idxz00_6398 + 3L)) == BgL_classz00_6396);
													}
												else
													{	/* Cfa/type.scm 336 */
														bool_t BgL_res2301z00_6401;

														{	/* Cfa/type.scm 336 */
															obj_t BgL_oclassz00_6402;

															{	/* Cfa/type.scm 336 */
																obj_t BgL_arg1815z00_6403;
																long BgL_arg1816z00_6404;

																BgL_arg1815z00_6403 =
																	(BGl_za2classesza2z00zz__objectz00);
																{	/* Cfa/type.scm 336 */
																	long BgL_arg1817z00_6405;

																	BgL_arg1817z00_6405 =
																		BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6397);
																	BgL_arg1816z00_6404 =
																		(BgL_arg1817z00_6405 - OBJECT_TYPE);
																}
																BgL_oclassz00_6402 =
																	VECTOR_REF(BgL_arg1815z00_6403,
																	BgL_arg1816z00_6404);
															}
															{	/* Cfa/type.scm 336 */
																bool_t BgL__ortest_1115z00_6406;

																BgL__ortest_1115z00_6406 =
																	(BgL_classz00_6396 == BgL_oclassz00_6402);
																if (BgL__ortest_1115z00_6406)
																	{	/* Cfa/type.scm 336 */
																		BgL_res2301z00_6401 =
																			BgL__ortest_1115z00_6406;
																	}
																else
																	{	/* Cfa/type.scm 336 */
																		long BgL_odepthz00_6407;

																		{	/* Cfa/type.scm 336 */
																			obj_t BgL_arg1804z00_6408;

																			BgL_arg1804z00_6408 =
																				(BgL_oclassz00_6402);
																			BgL_odepthz00_6407 =
																				BGL_CLASS_DEPTH(BgL_arg1804z00_6408);
																		}
																		if ((3L < BgL_odepthz00_6407))
																			{	/* Cfa/type.scm 336 */
																				obj_t BgL_arg1802z00_6409;

																				{	/* Cfa/type.scm 336 */
																					obj_t BgL_arg1803z00_6410;

																					BgL_arg1803z00_6410 =
																						(BgL_oclassz00_6402);
																					BgL_arg1802z00_6409 =
																						BGL_CLASS_ANCESTORS_REF
																						(BgL_arg1803z00_6410, 3L);
																				}
																				BgL_res2301z00_6401 =
																					(BgL_arg1802z00_6409 ==
																					BgL_classz00_6396);
																			}
																		else
																			{	/* Cfa/type.scm 336 */
																				BgL_res2301z00_6401 = ((bool_t) 0);
																			}
																	}
															}
														}
														BgL_test2523z00_7839 = BgL_res2301z00_6401;
													}
											}
										}
										if (BgL_test2523z00_7839)
											{	/* Cfa/type.scm 337 */
												obj_t BgL_l01619z00_6411;

												BgL_l01619z00_6411 =
													(((BgL_cfunz00_bglt) COBJECT(
															((BgL_cfunz00_bglt) BgL_valz00_6372)))->
													BgL_argszd2typezd2);
												{
													obj_t BgL_l1618z00_6413;

													BgL_l1618z00_6413 = BgL_l01619z00_6411;
												BgL_zc3z04anonymousza31953ze3z87_6412:
													if (NULLP(BgL_l1618z00_6413))
														{	/* Cfa/type.scm 337 */
															BgL_l01619z00_6411;
														}
													else
														{	/* Cfa/type.scm 337 */
															{	/* Cfa/type.scm 337 */
																obj_t BgL_arg1955z00_6414;

																{	/* Cfa/type.scm 337 */
																	obj_t BgL_arg1956z00_6415;

																	BgL_arg1956z00_6415 =
																		CAR(((obj_t) BgL_l1618z00_6413));
																	BgL_arg1955z00_6414 =
																		BGl_cleanupzd2typeze70z35zzcfa_typez00
																		(BgL_arg1956z00_6415);
																}
																{	/* Cfa/type.scm 337 */
																	obj_t BgL_tmpz00_7870;

																	BgL_tmpz00_7870 = ((obj_t) BgL_l1618z00_6413);
																	SET_CAR(BgL_tmpz00_7870, BgL_arg1955z00_6414);
																}
															}
															{	/* Cfa/type.scm 337 */
																obj_t BgL_arg1957z00_6416;

																BgL_arg1957z00_6416 =
																	CDR(((obj_t) BgL_l1618z00_6413));
																{
																	obj_t BgL_l1618z00_7875;

																	BgL_l1618z00_7875 = BgL_arg1957z00_6416;
																	BgL_l1618z00_6413 = BgL_l1618z00_7875;
																	goto BgL_zc3z04anonymousza31953ze3z87_6412;
																}
															}
														}
												}
											}
										else
											{	/* Cfa/type.scm 336 */
												BFALSE;
											}
									}
							}
						}
					}
				}
				{	/* Cfa/type.scm 339 */
					bool_t BgL_test2528z00_7876;

					{	/* Cfa/type.scm 339 */
						BgL_typez00_bglt BgL_arg1961z00_6417;

						BgL_arg1961z00_6417 =
							(((BgL_nodez00_bglt) COBJECT(
									((BgL_nodez00_bglt)
										((BgL_appz00_bglt)
											((BgL_appz00_bglt) BgL_nodez00_6186)))))->BgL_typez00);
						BgL_test2528z00_7876 =
							(((obj_t) BgL_arg1961z00_6417) == BGl_za2_za2z00zztype_cachez00);
					}
					if (BgL_test2528z00_7876)
						{	/* Cfa/type.scm 339 */
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_appz00_bglt)
													((BgL_appz00_bglt) BgL_nodez00_6186)))))->
									BgL_typez00) =
								((BgL_typez00_bglt) ((BgL_typez00_bglt)
										BGl_za2objza2z00zztype_cachez00)), BUNSPEC);
						}
					else
						{	/* Cfa/type.scm 339 */
							BFALSE;
						}
				}
				return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_6186));
			}
		}

	}



/* cleanup-type~0 */
	obj_t BGl_cleanupzd2typeze70z35zzcfa_typez00(obj_t BgL_tz00_4090)
	{
		{	/* Cfa/type.scm 322 */
			{	/* Cfa/type.scm 315 */
				bool_t BgL_test2529z00_7890;

				{	/* Cfa/type.scm 315 */
					obj_t BgL_classz00_5494;

					BgL_classz00_5494 = BGl_typez00zztype_typez00;
					if (BGL_OBJECTP(BgL_tz00_4090))
						{	/* Cfa/type.scm 315 */
							BgL_objectz00_bglt BgL_arg1807z00_5496;

							BgL_arg1807z00_5496 = (BgL_objectz00_bglt) (BgL_tz00_4090);
							if (BGL_CONDEXPAND_ISA_ARCH64())
								{	/* Cfa/type.scm 315 */
									long BgL_idxz00_5502;

									BgL_idxz00_5502 =
										BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5496);
									BgL_test2529z00_7890 =
										(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
											(BgL_idxz00_5502 + 1L)) == BgL_classz00_5494);
								}
							else
								{	/* Cfa/type.scm 315 */
									bool_t BgL_res2298z00_5527;

									{	/* Cfa/type.scm 315 */
										obj_t BgL_oclassz00_5510;

										{	/* Cfa/type.scm 315 */
											obj_t BgL_arg1815z00_5518;
											long BgL_arg1816z00_5519;

											BgL_arg1815z00_5518 = (BGl_za2classesza2z00zz__objectz00);
											{	/* Cfa/type.scm 315 */
												long BgL_arg1817z00_5520;

												BgL_arg1817z00_5520 =
													BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5496);
												BgL_arg1816z00_5519 =
													(BgL_arg1817z00_5520 - OBJECT_TYPE);
											}
											BgL_oclassz00_5510 =
												VECTOR_REF(BgL_arg1815z00_5518, BgL_arg1816z00_5519);
										}
										{	/* Cfa/type.scm 315 */
											bool_t BgL__ortest_1115z00_5511;

											BgL__ortest_1115z00_5511 =
												(BgL_classz00_5494 == BgL_oclassz00_5510);
											if (BgL__ortest_1115z00_5511)
												{	/* Cfa/type.scm 315 */
													BgL_res2298z00_5527 = BgL__ortest_1115z00_5511;
												}
											else
												{	/* Cfa/type.scm 315 */
													long BgL_odepthz00_5512;

													{	/* Cfa/type.scm 315 */
														obj_t BgL_arg1804z00_5513;

														BgL_arg1804z00_5513 = (BgL_oclassz00_5510);
														BgL_odepthz00_5512 =
															BGL_CLASS_DEPTH(BgL_arg1804z00_5513);
													}
													if ((1L < BgL_odepthz00_5512))
														{	/* Cfa/type.scm 315 */
															obj_t BgL_arg1802z00_5515;

															{	/* Cfa/type.scm 315 */
																obj_t BgL_arg1803z00_5516;

																BgL_arg1803z00_5516 = (BgL_oclassz00_5510);
																BgL_arg1802z00_5515 =
																	BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_5516,
																	1L);
															}
															BgL_res2298z00_5527 =
																(BgL_arg1802z00_5515 == BgL_classz00_5494);
														}
													else
														{	/* Cfa/type.scm 315 */
															BgL_res2298z00_5527 = ((bool_t) 0);
														}
												}
										}
									}
									BgL_test2529z00_7890 = BgL_res2298z00_5527;
								}
						}
					else
						{	/* Cfa/type.scm 315 */
							BgL_test2529z00_7890 = ((bool_t) 0);
						}
				}
				if (BgL_test2529z00_7890)
					{	/* Cfa/type.scm 315 */
						if ((BgL_tz00_4090 == BGl_za2_za2z00zztype_cachez00))
							{	/* Cfa/type.scm 316 */
								return BGl_za2objza2z00zztype_cachez00;
							}
						else
							{	/* Cfa/type.scm 316 */
								return BgL_tz00_4090;
							}
					}
				else
					{	/* Cfa/type.scm 317 */
						bool_t BgL_test2535z00_7915;

						{	/* Cfa/type.scm 317 */
							obj_t BgL_classz00_5528;

							BgL_classz00_5528 = BGl_localz00zzast_varz00;
							if (BGL_OBJECTP(BgL_tz00_4090))
								{	/* Cfa/type.scm 317 */
									BgL_objectz00_bglt BgL_arg1807z00_5530;

									BgL_arg1807z00_5530 = (BgL_objectz00_bglt) (BgL_tz00_4090);
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/type.scm 317 */
											long BgL_idxz00_5536;

											BgL_idxz00_5536 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_5530);
											BgL_test2535z00_7915 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_5536 + 2L)) == BgL_classz00_5528);
										}
									else
										{	/* Cfa/type.scm 317 */
											bool_t BgL_res2299z00_5561;

											{	/* Cfa/type.scm 317 */
												obj_t BgL_oclassz00_5544;

												{	/* Cfa/type.scm 317 */
													obj_t BgL_arg1815z00_5552;
													long BgL_arg1816z00_5553;

													BgL_arg1815z00_5552 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/type.scm 317 */
														long BgL_arg1817z00_5554;

														BgL_arg1817z00_5554 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_5530);
														BgL_arg1816z00_5553 =
															(BgL_arg1817z00_5554 - OBJECT_TYPE);
													}
													BgL_oclassz00_5544 =
														VECTOR_REF(BgL_arg1815z00_5552,
														BgL_arg1816z00_5553);
												}
												{	/* Cfa/type.scm 317 */
													bool_t BgL__ortest_1115z00_5545;

													BgL__ortest_1115z00_5545 =
														(BgL_classz00_5528 == BgL_oclassz00_5544);
													if (BgL__ortest_1115z00_5545)
														{	/* Cfa/type.scm 317 */
															BgL_res2299z00_5561 = BgL__ortest_1115z00_5545;
														}
													else
														{	/* Cfa/type.scm 317 */
															long BgL_odepthz00_5546;

															{	/* Cfa/type.scm 317 */
																obj_t BgL_arg1804z00_5547;

																BgL_arg1804z00_5547 = (BgL_oclassz00_5544);
																BgL_odepthz00_5546 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_5547);
															}
															if ((2L < BgL_odepthz00_5546))
																{	/* Cfa/type.scm 317 */
																	obj_t BgL_arg1802z00_5549;

																	{	/* Cfa/type.scm 317 */
																		obj_t BgL_arg1803z00_5550;

																		BgL_arg1803z00_5550 = (BgL_oclassz00_5544);
																		BgL_arg1802z00_5549 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_5550, 2L);
																	}
																	BgL_res2299z00_5561 =
																		(BgL_arg1802z00_5549 == BgL_classz00_5528);
																}
															else
																{	/* Cfa/type.scm 317 */
																	BgL_res2299z00_5561 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2535z00_7915 = BgL_res2299z00_5561;
										}
								}
							else
								{	/* Cfa/type.scm 317 */
									BgL_test2535z00_7915 = ((bool_t) 0);
								}
						}
						if (BgL_test2535z00_7915)
							{	/* Cfa/type.scm 317 */
								{	/* Cfa/type.scm 318 */
									bool_t BgL_test2540z00_7938;

									{	/* Cfa/type.scm 318 */
										BgL_typez00_bglt BgL_arg1967z00_4096;

										BgL_arg1967z00_4096 =
											(((BgL_variablez00_bglt) COBJECT(
													((BgL_variablez00_bglt)
														((BgL_localz00_bglt) BgL_tz00_4090))))->
											BgL_typez00);
										BgL_test2540z00_7938 =
											(((obj_t) BgL_arg1967z00_4096) ==
											BGl_za2_za2z00zztype_cachez00);
									}
									if (BgL_test2540z00_7938)
										{	/* Cfa/type.scm 319 */
											BgL_typez00_bglt BgL_vz00_5564;

											BgL_vz00_5564 =
												((BgL_typez00_bglt) BGl_za2objza2z00zztype_cachez00);
											((((BgL_variablez00_bglt) COBJECT(
															((BgL_variablez00_bglt)
																((BgL_localz00_bglt) BgL_tz00_4090))))->
													BgL_typez00) =
												((BgL_typez00_bglt) BgL_vz00_5564), BUNSPEC);
										}
									else
										{	/* Cfa/type.scm 318 */
											BFALSE;
										}
								}
								return BgL_tz00_4090;
							}
						else
							{	/* Cfa/type.scm 317 */
								return BgL_tz00_4090;
							}
					}
			}
		}

	}



/* &type-node!-app1660 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2app1660z70zzcfa_typez00(obj_t
		BgL_envz00_6187, obj_t BgL_nodez00_6188)
	{
		{	/* Cfa/type.scm 301 */
			BGl_typezd2nodeza2z12z62zzcfa_typez00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_6188)))->BgL_argsz00));
			BGl_typezd2funzd2nodez12z12zzcfa_typez00(
				(((BgL_appz00_bglt) COBJECT(
							((BgL_appz00_bglt) BgL_nodez00_6188)))->BgL_funz00));
			{
				BgL_typez00_bglt BgL_auxz00_7954;

				{	/* Cfa/type.scm 305 */
					BgL_typez00_bglt BgL_arg1939z00_6419;
					BgL_typez00_bglt BgL_arg1940z00_6420;

					BgL_arg1939z00_6419 =
						(((BgL_variablez00_bglt) COBJECT(
								(((BgL_varz00_bglt) COBJECT(
											(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_6188)))->
												BgL_funz00)))->BgL_variablez00)))->BgL_typez00);
					BgL_arg1940z00_6420 =
						(((BgL_nodez00_bglt) COBJECT(((BgL_nodez00_bglt) ((BgL_appz00_bglt)
										BgL_nodez00_6188))))->BgL_typez00);
					BgL_auxz00_7954 =
						BGl_strictzd2nodezd2typez00zzast_nodez00(BgL_arg1939z00_6419,
						BgL_arg1940z00_6420);
				}
				((((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_appz00_bglt) BgL_nodez00_6188))))->BgL_typez00) =
					((BgL_typez00_bglt) BgL_auxz00_7954), BUNSPEC);
			}
			return ((BgL_nodez00_bglt) ((BgL_appz00_bglt) BgL_nodez00_6188));
		}

	}



/* &type-node!-sync1658 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2sync1658z70zzcfa_typez00(obj_t
		BgL_envz00_6189, obj_t BgL_nodez00_6190)
	{
		{	/* Cfa/type.scm 289 */
			{
				BgL_nodez00_bglt BgL_auxz00_7968;

				{	/* Cfa/type.scm 291 */
					BgL_nodez00_bglt BgL_arg1930z00_6422;

					BgL_arg1930z00_6422 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_6190)))->BgL_mutexz00);
					BgL_auxz00_7968 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg1930z00_6422);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_6190)))->BgL_mutexz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7968), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_7974;

				{	/* Cfa/type.scm 292 */
					BgL_nodez00_bglt BgL_arg1931z00_6423;

					BgL_arg1931z00_6423 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_6190)))->BgL_prelockz00);
					BgL_auxz00_7974 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg1931z00_6423);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_6190)))->BgL_prelockz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7974), BUNSPEC);
			}
			{
				BgL_nodez00_bglt BgL_auxz00_7980;

				{	/* Cfa/type.scm 293 */
					BgL_nodez00_bglt BgL_arg1932z00_6424;

					BgL_arg1932z00_6424 =
						(((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_6190)))->BgL_bodyz00);
					BgL_auxz00_7980 =
						BGl_typezd2nodez12zc0zzcfa_typez00(BgL_arg1932z00_6424);
				}
				((((BgL_syncz00_bglt) COBJECT(
								((BgL_syncz00_bglt) BgL_nodez00_6190)))->BgL_bodyz00) =
					((BgL_nodez00_bglt) BgL_auxz00_7980), BUNSPEC);
			}
			{	/* Cfa/type.scm 294 */
				bool_t BgL_test2541z00_7986;

				{	/* Cfa/type.scm 294 */
					BgL_typez00_bglt BgL_arg1936z00_6425;

					BgL_arg1936z00_6425 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_syncz00_bglt) BgL_nodez00_6190))))->BgL_typez00);
					BgL_test2541z00_7986 =
						(((obj_t) BgL_arg1936z00_6425) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test2541z00_7986)
					{
						BgL_typez00_bglt BgL_auxz00_7992;

						{	/* Cfa/type.scm 295 */
							BgL_nodez00_bglt BgL_arg1935z00_6426;

							BgL_arg1935z00_6426 =
								(((BgL_syncz00_bglt) COBJECT(
										((BgL_syncz00_bglt) BgL_nodez00_6190)))->BgL_bodyz00);
							BgL_auxz00_7992 =
								BGl_getzd2typezd2zztype_typeofz00(BgL_arg1935z00_6426,
								((bool_t) 0));
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_syncz00_bglt) BgL_nodez00_6190))))->BgL_typez00) =
							((BgL_typez00_bglt) BgL_auxz00_7992), BUNSPEC);
					}
				else
					{	/* Cfa/type.scm 294 */
						BFALSE;
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_syncz00_bglt) BgL_nodez00_6190));
		}

	}



/* &type-node!-sequence1656 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2sequence1656z70zzcfa_typez00(obj_t
		BgL_envz00_6191, obj_t BgL_nodez00_6192)
	{
		{	/* Cfa/type.scm 277 */
			BGl_typezd2nodeza2z12z62zzcfa_typez00(
				(((BgL_sequencez00_bglt) COBJECT(
							((BgL_sequencez00_bglt) BgL_nodez00_6192)))->BgL_nodesz00));
			{	/* Cfa/type.scm 280 */
				bool_t BgL_test2542z00_8004;

				{	/* Cfa/type.scm 280 */
					BgL_typez00_bglt BgL_arg1929z00_6428;

					BgL_arg1929z00_6428 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_sequencez00_bglt) BgL_nodez00_6192))))->BgL_typez00);
					BgL_test2542z00_8004 =
						(((obj_t) BgL_arg1929z00_6428) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test2542z00_8004)
					{	/* Cfa/type.scm 280 */
						if (NULLP(
								(((BgL_sequencez00_bglt) COBJECT(
											((BgL_sequencez00_bglt) BgL_nodez00_6192)))->
									BgL_nodesz00)))
							{	/* Cfa/type.scm 281 */
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_sequencez00_bglt) BgL_nodez00_6192))))->
										BgL_typez00) =
									((BgL_typez00_bglt) ((BgL_typez00_bglt)
											BGl_za2unspecza2z00zztype_cachez00)), BUNSPEC);
							}
						else
							{
								BgL_typez00_bglt BgL_auxz00_8018;

								{	/* Cfa/type.scm 282 */
									obj_t BgL_arg1925z00_6429;

									BgL_arg1925z00_6429 =
										CAR(BGl_lastzd2pairzd2zz__r4_pairs_and_lists_6_3z00(
											(((BgL_sequencez00_bglt) COBJECT(
														((BgL_sequencez00_bglt) BgL_nodez00_6192)))->
												BgL_nodesz00)));
									BgL_auxz00_8018 =
										BGl_getzd2typezd2zztype_typeofz00(((BgL_nodez00_bglt)
											BgL_arg1925z00_6429), ((bool_t) 0));
								}
								((((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_sequencez00_bglt) BgL_nodez00_6192))))->
										BgL_typez00) =
									((BgL_typez00_bglt) BgL_auxz00_8018), BUNSPEC);
							}
					}
				else
					{	/* Cfa/type.scm 280 */
						BFALSE;
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_sequencez00_bglt) BgL_nodez00_6192));
		}

	}



/* &type-node!-closure1654 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2closure1654z70zzcfa_typez00(obj_t
		BgL_envz00_6193, obj_t BgL_nodez00_6194)
	{
		{	/* Cfa/type.scm 271 */
			{	/* Cfa/type.scm 272 */
				obj_t BgL_arg1917z00_6431;

				BgL_arg1917z00_6431 =
					BGl_shapez00zztools_shapez00(
					((obj_t) ((BgL_closurez00_bglt) BgL_nodez00_6194)));
				return
					((BgL_nodez00_bglt)
					BGl_internalzd2errorzd2zztools_errorz00
					(BGl_string2329z00zzcfa_typez00, BGl_string2362z00zzcfa_typez00,
						BgL_arg1917z00_6431));
			}
		}

	}



/* &type-node!-var1652 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2var1652z70zzcfa_typez00(obj_t
		BgL_envz00_6195, obj_t BgL_nodez00_6196)
	{
		{	/* Cfa/type.scm 251 */
			{
				BgL_typez00_bglt BgL_ntypez00_6434;
				BgL_typez00_bglt BgL_vtypez00_6435;

				{	/* Cfa/type.scm 260 */
					bool_t BgL_test2544z00_8035;

					{	/* Cfa/type.scm 260 */
						bool_t BgL_test2545z00_8036;

						{	/* Cfa/type.scm 260 */
							BgL_variablez00_bglt BgL_arg1892z00_6470;

							BgL_arg1892z00_6470 =
								(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_nodez00_6196)))->BgL_variablez00);
							{	/* Cfa/type.scm 260 */
								obj_t BgL_classz00_6471;

								BgL_classz00_6471 = BGl_globalz00zzast_varz00;
								{	/* Cfa/type.scm 260 */
									BgL_objectz00_bglt BgL_arg1807z00_6472;

									{	/* Cfa/type.scm 260 */
										obj_t BgL_tmpz00_8039;

										BgL_tmpz00_8039 =
											((obj_t) ((BgL_objectz00_bglt) BgL_arg1892z00_6470));
										BgL_arg1807z00_6472 =
											(BgL_objectz00_bglt) (BgL_tmpz00_8039);
									}
									if (BGL_CONDEXPAND_ISA_ARCH64())
										{	/* Cfa/type.scm 260 */
											long BgL_idxz00_6473;

											BgL_idxz00_6473 =
												BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_6472);
											BgL_test2545z00_8036 =
												(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
													(BgL_idxz00_6473 + 2L)) == BgL_classz00_6471);
										}
									else
										{	/* Cfa/type.scm 260 */
											bool_t BgL_res2297z00_6476;

											{	/* Cfa/type.scm 260 */
												obj_t BgL_oclassz00_6477;

												{	/* Cfa/type.scm 260 */
													obj_t BgL_arg1815z00_6478;
													long BgL_arg1816z00_6479;

													BgL_arg1815z00_6478 =
														(BGl_za2classesza2z00zz__objectz00);
													{	/* Cfa/type.scm 260 */
														long BgL_arg1817z00_6480;

														BgL_arg1817z00_6480 =
															BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_6472);
														BgL_arg1816z00_6479 =
															(BgL_arg1817z00_6480 - OBJECT_TYPE);
													}
													BgL_oclassz00_6477 =
														VECTOR_REF(BgL_arg1815z00_6478,
														BgL_arg1816z00_6479);
												}
												{	/* Cfa/type.scm 260 */
													bool_t BgL__ortest_1115z00_6481;

													BgL__ortest_1115z00_6481 =
														(BgL_classz00_6471 == BgL_oclassz00_6477);
													if (BgL__ortest_1115z00_6481)
														{	/* Cfa/type.scm 260 */
															BgL_res2297z00_6476 = BgL__ortest_1115z00_6481;
														}
													else
														{	/* Cfa/type.scm 260 */
															long BgL_odepthz00_6482;

															{	/* Cfa/type.scm 260 */
																obj_t BgL_arg1804z00_6483;

																BgL_arg1804z00_6483 = (BgL_oclassz00_6477);
																BgL_odepthz00_6482 =
																	BGL_CLASS_DEPTH(BgL_arg1804z00_6483);
															}
															if ((2L < BgL_odepthz00_6482))
																{	/* Cfa/type.scm 260 */
																	obj_t BgL_arg1802z00_6484;

																	{	/* Cfa/type.scm 260 */
																		obj_t BgL_arg1803z00_6485;

																		BgL_arg1803z00_6485 = (BgL_oclassz00_6477);
																		BgL_arg1802z00_6484 =
																			BGL_CLASS_ANCESTORS_REF
																			(BgL_arg1803z00_6485, 2L);
																	}
																	BgL_res2297z00_6476 =
																		(BgL_arg1802z00_6484 == BgL_classz00_6471);
																}
															else
																{	/* Cfa/type.scm 260 */
																	BgL_res2297z00_6476 = ((bool_t) 0);
																}
														}
												}
											}
											BgL_test2545z00_8036 = BgL_res2297z00_6476;
										}
								}
							}
						}
						if (BgL_test2545z00_8036)
							{	/* Cfa/type.scm 260 */
								BgL_test2544z00_8035 =
									(
									(((BgL_globalz00_bglt) COBJECT(
												((BgL_globalz00_bglt)
													(((BgL_varz00_bglt) COBJECT(
																((BgL_varz00_bglt) BgL_nodez00_6196)))->
														BgL_variablez00))))->BgL_importz00) ==
									CNST_TABLE_REF(3));
							}
						else
							{	/* Cfa/type.scm 260 */
								BgL_test2544z00_8035 = ((bool_t) 0);
							}
					}
					if (BgL_test2544z00_8035)
						{	/* Cfa/type.scm 261 */
							BgL_valuez00_bglt BgL_arg1887z00_6486;
							BgL_variablez00_bglt BgL_arg1888z00_6487;

							BgL_arg1887z00_6486 =
								(((BgL_variablez00_bglt) COBJECT(
										((BgL_variablez00_bglt)
											((BgL_globalz00_bglt)
												(((BgL_varz00_bglt) COBJECT(
															((BgL_varz00_bglt) BgL_nodez00_6196)))->
													BgL_variablez00)))))->BgL_valuez00);
							BgL_arg1888z00_6487 =
								(((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
											BgL_nodez00_6196)))->BgL_variablez00);
							BGl_typezd2variablez12zc0zzcfa_typez00(BgL_arg1887z00_6486,
								BgL_arg1888z00_6487);
						}
					else
						{	/* Cfa/type.scm 260 */
							BFALSE;
						}
				}
				{	/* Cfa/type.scm 262 */
					bool_t BgL_test2549z00_8076;

					{	/* Cfa/type.scm 262 */
						bool_t BgL_test2550z00_8077;

						{	/* Cfa/type.scm 262 */
							BgL_typez00_bglt BgL_arg1914z00_6488;

							BgL_arg1914z00_6488 =
								(((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_varz00_bglt) BgL_nodez00_6196))))->BgL_typez00);
							BgL_test2550z00_8077 =
								(
								((obj_t) BgL_arg1914z00_6488) == BGl_za2_za2z00zztype_cachez00);
						}
						if (BgL_test2550z00_8077)
							{	/* Cfa/type.scm 262 */
								BgL_test2549z00_8076 = ((bool_t) 1);
							}
						else
							{	/* Cfa/type.scm 263 */
								bool_t BgL_test2551z00_8083;

								{	/* Cfa/type.scm 263 */
									BgL_typez00_bglt BgL_arg1913z00_6489;

									BgL_arg1913z00_6489 =
										(((BgL_nodez00_bglt) COBJECT(
												((BgL_nodez00_bglt)
													((BgL_varz00_bglt) BgL_nodez00_6196))))->BgL_typez00);
									BgL_test2551z00_8083 =
										(
										((obj_t) BgL_arg1913z00_6489) ==
										BGl_za2vectorza2z00zztype_cachez00);
								}
								if (BgL_test2551z00_8083)
									{	/* Cfa/type.scm 263 */
										BgL_test2549z00_8076 = ((bool_t) 1);
									}
								else
									{	/* Cfa/type.scm 264 */
										BgL_typez00_bglt BgL_arg1910z00_6490;
										BgL_typez00_bglt BgL_arg1911z00_6491;

										BgL_arg1910z00_6490 =
											(((BgL_nodez00_bglt) COBJECT(
													((BgL_nodez00_bglt)
														((BgL_varz00_bglt) BgL_nodez00_6196))))->
											BgL_typez00);
										BgL_arg1911z00_6491 =
											(((BgL_variablez00_bglt) COBJECT((((BgL_varz00_bglt)
															COBJECT(((BgL_varz00_bglt) BgL_nodez00_6196)))->
														BgL_variablez00)))->BgL_typez00);
										BgL_ntypez00_6434 = BgL_arg1910z00_6490;
										BgL_vtypez00_6435 = BgL_arg1911z00_6491;
										{	/* Cfa/type.scm 254 */
											bool_t BgL__ortest_1184z00_6436;

											if (BGl_bigloozd2typezf3z21zztype_typez00
												(BgL_vtypez00_6435))
												{	/* Cfa/type.scm 254 */
													BgL__ortest_1184z00_6436 = ((bool_t) 0);
												}
											else
												{	/* Cfa/type.scm 254 */
													BgL__ortest_1184z00_6436 = ((bool_t) 1);
												}
											if (BgL__ortest_1184z00_6436)
												{	/* Cfa/type.scm 254 */
													BgL_test2549z00_8076 = BgL__ortest_1184z00_6436;
												}
											else
												{	/* Cfa/type.scm 255 */
													bool_t BgL__ortest_1185z00_6437;

													BgL__ortest_1185z00_6437 =
														(
														((obj_t) BgL_ntypez00_6434) ==
														BGl_za2objza2z00zztype_cachez00);
													if (BgL__ortest_1185z00_6437)
														{	/* Cfa/type.scm 255 */
															BgL_test2549z00_8076 = BgL__ortest_1185z00_6437;
														}
													else
														{	/* Cfa/type.scm 256 */
															bool_t BgL__ortest_1186z00_6438;

															{	/* Cfa/type.scm 256 */
																bool_t BgL__ortest_1187z00_6439;

																BgL__ortest_1187z00_6439 =
																	(
																	((obj_t) BgL_vtypez00_6435) ==
																	BGl_za2pairza2z00zztype_cachez00);
																if (BgL__ortest_1187z00_6439)
																	{	/* Cfa/type.scm 256 */
																		BgL__ortest_1186z00_6438 =
																			BgL__ortest_1187z00_6439;
																	}
																else
																	{	/* Cfa/type.scm 256 */
																		BgL__ortest_1186z00_6438 =
																			(
																			((obj_t) BgL_vtypez00_6435) ==
																			BGl_za2epairza2z00zztype_cachez00);
																	}
															}
															if (BgL__ortest_1186z00_6438)
																{	/* Cfa/type.scm 256 */
																	BgL_test2549z00_8076 =
																		BgL__ortest_1186z00_6438;
																}
															else
																{	/* Cfa/type.scm 257 */
																	bool_t BgL_test2557z00_8107;

																	{	/* Cfa/type.scm 257 */
																		obj_t BgL_classz00_6440;

																		BgL_classz00_6440 =
																			BGl_tclassz00zzobject_classz00;
																		{	/* Cfa/type.scm 257 */
																			BgL_objectz00_bglt BgL_arg1807z00_6441;

																			{	/* Cfa/type.scm 257 */
																				obj_t BgL_tmpz00_8108;

																				BgL_tmpz00_8108 =
																					((obj_t)
																					((BgL_objectz00_bglt)
																						BgL_vtypez00_6435));
																				BgL_arg1807z00_6441 =
																					(BgL_objectz00_bglt)
																					(BgL_tmpz00_8108);
																			}
																			if (BGL_CONDEXPAND_ISA_ARCH64())
																				{	/* Cfa/type.scm 257 */
																					long BgL_idxz00_6442;

																					BgL_idxz00_6442 =
																						BGL_OBJECT_INHERITANCE_NUM
																						(BgL_arg1807z00_6441);
																					BgL_test2557z00_8107 =
																						(VECTOR_REF
																						(BGl_za2inheritancesza2z00zz__objectz00,
																							(BgL_idxz00_6442 + 2L)) ==
																						BgL_classz00_6440);
																				}
																			else
																				{	/* Cfa/type.scm 257 */
																					bool_t BgL_res2295z00_6445;

																					{	/* Cfa/type.scm 257 */
																						obj_t BgL_oclassz00_6446;

																						{	/* Cfa/type.scm 257 */
																							obj_t BgL_arg1815z00_6447;
																							long BgL_arg1816z00_6448;

																							BgL_arg1815z00_6447 =
																								(BGl_za2classesza2z00zz__objectz00);
																							{	/* Cfa/type.scm 257 */
																								long BgL_arg1817z00_6449;

																								BgL_arg1817z00_6449 =
																									BGL_OBJECT_CLASS_NUM
																									(BgL_arg1807z00_6441);
																								BgL_arg1816z00_6448 =
																									(BgL_arg1817z00_6449 -
																									OBJECT_TYPE);
																							}
																							BgL_oclassz00_6446 =
																								VECTOR_REF(BgL_arg1815z00_6447,
																								BgL_arg1816z00_6448);
																						}
																						{	/* Cfa/type.scm 257 */
																							bool_t BgL__ortest_1115z00_6450;

																							BgL__ortest_1115z00_6450 =
																								(BgL_classz00_6440 ==
																								BgL_oclassz00_6446);
																							if (BgL__ortest_1115z00_6450)
																								{	/* Cfa/type.scm 257 */
																									BgL_res2295z00_6445 =
																										BgL__ortest_1115z00_6450;
																								}
																							else
																								{	/* Cfa/type.scm 257 */
																									long BgL_odepthz00_6451;

																									{	/* Cfa/type.scm 257 */
																										obj_t BgL_arg1804z00_6452;

																										BgL_arg1804z00_6452 =
																											(BgL_oclassz00_6446);
																										BgL_odepthz00_6451 =
																											BGL_CLASS_DEPTH
																											(BgL_arg1804z00_6452);
																									}
																									if ((2L < BgL_odepthz00_6451))
																										{	/* Cfa/type.scm 257 */
																											obj_t BgL_arg1802z00_6453;

																											{	/* Cfa/type.scm 257 */
																												obj_t
																													BgL_arg1803z00_6454;
																												BgL_arg1803z00_6454 =
																													(BgL_oclassz00_6446);
																												BgL_arg1802z00_6453 =
																													BGL_CLASS_ANCESTORS_REF
																													(BgL_arg1803z00_6454,
																													2L);
																											}
																											BgL_res2295z00_6445 =
																												(BgL_arg1802z00_6453 ==
																												BgL_classz00_6440);
																										}
																									else
																										{	/* Cfa/type.scm 257 */
																											BgL_res2295z00_6445 =
																												((bool_t) 0);
																										}
																								}
																						}
																					}
																					BgL_test2557z00_8107 =
																						BgL_res2295z00_6445;
																				}
																		}
																	}
																	if (BgL_test2557z00_8107)
																		{	/* Cfa/type.scm 257 */
																			bool_t BgL_test2561z00_8131;

																			{	/* Cfa/type.scm 257 */
																				obj_t BgL_classz00_6455;

																				BgL_classz00_6455 =
																					BGl_tclassz00zzobject_classz00;
																				{	/* Cfa/type.scm 257 */
																					BgL_objectz00_bglt
																						BgL_arg1807z00_6456;
																					{	/* Cfa/type.scm 257 */
																						obj_t BgL_tmpz00_8132;

																						BgL_tmpz00_8132 =
																							((obj_t)
																							((BgL_objectz00_bglt)
																								BgL_ntypez00_6434));
																						BgL_arg1807z00_6456 =
																							(BgL_objectz00_bglt)
																							(BgL_tmpz00_8132);
																					}
																					if (BGL_CONDEXPAND_ISA_ARCH64())
																						{	/* Cfa/type.scm 257 */
																							long BgL_idxz00_6457;

																							BgL_idxz00_6457 =
																								BGL_OBJECT_INHERITANCE_NUM
																								(BgL_arg1807z00_6456);
																							BgL_test2561z00_8131 =
																								(VECTOR_REF
																								(BGl_za2inheritancesza2z00zz__objectz00,
																									(BgL_idxz00_6457 + 2L)) ==
																								BgL_classz00_6455);
																						}
																					else
																						{	/* Cfa/type.scm 257 */
																							bool_t BgL_res2296z00_6460;

																							{	/* Cfa/type.scm 257 */
																								obj_t BgL_oclassz00_6461;

																								{	/* Cfa/type.scm 257 */
																									obj_t BgL_arg1815z00_6462;
																									long BgL_arg1816z00_6463;

																									BgL_arg1815z00_6462 =
																										(BGl_za2classesza2z00zz__objectz00);
																									{	/* Cfa/type.scm 257 */
																										long BgL_arg1817z00_6464;

																										BgL_arg1817z00_6464 =
																											BGL_OBJECT_CLASS_NUM
																											(BgL_arg1807z00_6456);
																										BgL_arg1816z00_6463 =
																											(BgL_arg1817z00_6464 -
																											OBJECT_TYPE);
																									}
																									BgL_oclassz00_6461 =
																										VECTOR_REF
																										(BgL_arg1815z00_6462,
																										BgL_arg1816z00_6463);
																								}
																								{	/* Cfa/type.scm 257 */
																									bool_t
																										BgL__ortest_1115z00_6465;
																									BgL__ortest_1115z00_6465 =
																										(BgL_classz00_6455 ==
																										BgL_oclassz00_6461);
																									if (BgL__ortest_1115z00_6465)
																										{	/* Cfa/type.scm 257 */
																											BgL_res2296z00_6460 =
																												BgL__ortest_1115z00_6465;
																										}
																									else
																										{	/* Cfa/type.scm 257 */
																											long BgL_odepthz00_6466;

																											{	/* Cfa/type.scm 257 */
																												obj_t
																													BgL_arg1804z00_6467;
																												BgL_arg1804z00_6467 =
																													(BgL_oclassz00_6461);
																												BgL_odepthz00_6466 =
																													BGL_CLASS_DEPTH
																													(BgL_arg1804z00_6467);
																											}
																											if (
																												(2L <
																													BgL_odepthz00_6466))
																												{	/* Cfa/type.scm 257 */
																													obj_t
																														BgL_arg1802z00_6468;
																													{	/* Cfa/type.scm 257 */
																														obj_t
																															BgL_arg1803z00_6469;
																														BgL_arg1803z00_6469
																															=
																															(BgL_oclassz00_6461);
																														BgL_arg1802z00_6468
																															=
																															BGL_CLASS_ANCESTORS_REF
																															(BgL_arg1803z00_6469,
																															2L);
																													}
																													BgL_res2296z00_6460 =
																														(BgL_arg1802z00_6468
																														==
																														BgL_classz00_6455);
																												}
																											else
																												{	/* Cfa/type.scm 257 */
																													BgL_res2296z00_6460 =
																														((bool_t) 0);
																												}
																										}
																								}
																							}
																							BgL_test2561z00_8131 =
																								BgL_res2296z00_6460;
																						}
																				}
																			}
																			if (BgL_test2561z00_8131)
																				{	/* Cfa/type.scm 257 */
																					BgL_test2549z00_8076 =
																						BGl_typezd2subclasszf3z21zzobject_classz00
																						(BgL_vtypez00_6435,
																						BgL_ntypez00_6434);
																				}
																			else
																				{	/* Cfa/type.scm 257 */
																					BgL_test2549z00_8076 = ((bool_t) 0);
																				}
																		}
																	else
																		{	/* Cfa/type.scm 257 */
																			BgL_test2549z00_8076 = ((bool_t) 0);
																		}
																}
														}
												}
										}
									}
							}
					}
					if (BgL_test2549z00_8076)
						{	/* Cfa/type.scm 262 */
							((((BgL_nodez00_bglt) COBJECT(
											((BgL_nodez00_bglt)
												((BgL_varz00_bglt) BgL_nodez00_6196))))->BgL_typez00) =
								((BgL_typez00_bglt) (((BgL_variablez00_bglt)
											COBJECT((((BgL_varz00_bglt) COBJECT(((BgL_varz00_bglt)
																BgL_nodez00_6196)))->BgL_variablez00)))->
										BgL_typez00)), BUNSPEC);
						}
					else
						{	/* Cfa/type.scm 262 */
							BFALSE;
						}
				}
				return ((BgL_nodez00_bglt) ((BgL_varz00_bglt) BgL_nodez00_6196));
			}
		}

	}



/* &type-node!-kwote1650 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2kwote1650z70zzcfa_typez00(obj_t
		BgL_envz00_6197, obj_t BgL_nodez00_6198)
	{
		{	/* Cfa/type.scm 242 */
			{	/* Cfa/type.scm 244 */
				bool_t BgL_test2565z00_8164;

				{	/* Cfa/type.scm 244 */
					BgL_typez00_bglt BgL_arg1879z00_6493;

					BgL_arg1879z00_6493 =
						(((BgL_nodez00_bglt) COBJECT(
								((BgL_nodez00_bglt)
									((BgL_kwotez00_bglt) BgL_nodez00_6198))))->BgL_typez00);
					BgL_test2565z00_8164 =
						(((obj_t) BgL_arg1879z00_6493) == BGl_za2_za2z00zztype_cachez00);
				}
				if (BgL_test2565z00_8164)
					{
						BgL_typez00_bglt BgL_auxz00_8170;

						{	/* Cfa/type.scm 245 */
							obj_t BgL_arg1878z00_6494;

							BgL_arg1878z00_6494 =
								(((BgL_kwotez00_bglt) COBJECT(
										((BgL_kwotez00_bglt) BgL_nodez00_6198)))->BgL_valuez00);
							BgL_auxz00_8170 =
								BGl_getzd2typezd2kwotez00zztype_typeofz00(BgL_arg1878z00_6494);
						}
						((((BgL_nodez00_bglt) COBJECT(
										((BgL_nodez00_bglt)
											((BgL_kwotez00_bglt) BgL_nodez00_6198))))->BgL_typez00) =
							((BgL_typez00_bglt) BgL_auxz00_8170), BUNSPEC);
					}
				else
					{	/* Cfa/type.scm 244 */
						BFALSE;
					}
			}
			return ((BgL_nodez00_bglt) ((BgL_kwotez00_bglt) BgL_nodez00_6198));
		}

	}



/* &type-node!-atom1648 */
	BgL_nodez00_bglt BGl_z62typezd2nodez12zd2atom1648z70zzcfa_typez00(obj_t
		BgL_envz00_6199, obj_t BgL_nodez00_6200)
	{
		{	/* Cfa/type.scm 236 */
			return ((BgL_nodez00_bglt) ((BgL_atomz00_bglt) BgL_nodez00_6200));
		}

	}



/* &type-variable!-inter1644 */
	obj_t BGl_z62typezd2variablez12zd2inter1644z70zzcfa_typez00(obj_t
		BgL_envz00_6201, obj_t BgL_valuez00_6202, obj_t BgL_variablez00_6203)
	{
		{	/* Cfa/type.scm 174 */
			return CNST_TABLE_REF(4);
		}

	}



/* &type-variable!-sexit1642 */
	obj_t BGl_z62typezd2variablez12zd2sexit1642z70zzcfa_typez00(obj_t
		BgL_envz00_6204, obj_t BgL_valuez00_6205, obj_t BgL_variablez00_6206)
	{
		{	/* Cfa/type.scm 164 */
			return CNST_TABLE_REF(4);
		}

	}



/* &type-variable!-cvar/1640 */
	obj_t BGl_z62typezd2variablez12zd2cvarzf21640z82zzcfa_typez00(obj_t
		BgL_envz00_6207, obj_t BgL_valuez00_6208, obj_t BgL_variablez00_6209)
	{
		{	/* Cfa/type.scm 157 */
			{	/* Cfa/type.scm 159 */
				obj_t BgL_arg1874z00_6499;

				{	/* Cfa/type.scm 159 */
					BgL_approxz00_bglt BgL_arg1875z00_6500;

					{
						BgL_cvarzf2cinfozf2_bglt BgL_auxz00_8183;

						{
							obj_t BgL_auxz00_8184;

							{	/* Cfa/type.scm 159 */
								BgL_objectz00_bglt BgL_tmpz00_8185;

								BgL_tmpz00_8185 =
									((BgL_objectz00_bglt) ((BgL_cvarz00_bglt) BgL_valuez00_6208));
								BgL_auxz00_8184 = BGL_OBJECT_WIDENING(BgL_tmpz00_8185);
							}
							BgL_auxz00_8183 = ((BgL_cvarzf2cinfozf2_bglt) BgL_auxz00_8184);
						}
						BgL_arg1875z00_6500 =
							(((BgL_cvarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8183))->
							BgL_approxz00);
					}
					BgL_arg1874z00_6499 =
						BGl_getzd2approxzd2typez00zzcfa_typez00(BgL_arg1875z00_6500,
						((obj_t) ((BgL_cvarz00_bglt) BgL_valuez00_6208)));
				}
				return
					BGl_setzd2variablezd2typez12z12zzcfa_typez00(
					((BgL_variablez00_bglt) BgL_variablez00_6209),
					((BgL_typez00_bglt) BgL_arg1874z00_6499));
			}
		}

	}



/* &type-variable!-scnst1638 */
	obj_t BGl_z62typezd2variablez12zd2scnst1638z70zzcfa_typez00(obj_t
		BgL_envz00_6210, obj_t BgL_valuez00_6211, obj_t BgL_variablez00_6212)
	{
		{	/* Cfa/type.scm 151 */
			return CNST_TABLE_REF(4);
		}

	}



/* &type-variable!-svar/1636 */
	obj_t BGl_z62typezd2variablez12zd2svarzf21636z82zzcfa_typez00(obj_t
		BgL_envz00_6213, obj_t BgL_valuez00_6214, obj_t BgL_variablez00_6215)
	{
		{	/* Cfa/type.scm 141 */
			{	/* Cfa/type.scm 143 */
				obj_t BgL_typz00_6503;

				{	/* Cfa/type.scm 143 */
					BgL_approxz00_bglt BgL_arg1873z00_6504;

					{
						BgL_svarzf2cinfozf2_bglt BgL_auxz00_8198;

						{
							obj_t BgL_auxz00_8199;

							{	/* Cfa/type.scm 143 */
								BgL_objectz00_bglt BgL_tmpz00_8200;

								BgL_tmpz00_8200 =
									((BgL_objectz00_bglt) ((BgL_svarz00_bglt) BgL_valuez00_6214));
								BgL_auxz00_8199 = BGL_OBJECT_WIDENING(BgL_tmpz00_8200);
							}
							BgL_auxz00_8198 = ((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_8199);
						}
						BgL_arg1873z00_6504 =
							(((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_8198))->
							BgL_approxz00);
					}
					BgL_typz00_6503 =
						BGl_getzd2approxzd2typez00zzcfa_typez00(BgL_arg1873z00_6504,
						((obj_t) ((BgL_svarz00_bglt) BgL_valuez00_6214)));
				}
				return
					BGl_setzd2variablezd2typez12z12zzcfa_typez00(
					((BgL_variablez00_bglt) BgL_variablez00_6215),
					((BgL_typez00_bglt) BgL_typz00_6503));
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_typez00(void)
	{
		{	/* Cfa/type.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zztype_typeofz00(398780265L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zzengine_paramz00(18364471L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zzast_envz00(146730773L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zzcfa_info3z00(0L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zzcfa_setz00(5932721L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zzcfa_tvectorz00(324810626L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zzcfa_closurez00(189402713L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zztvector_tvectorz00(501518119L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			BGl_modulezd2initializa7ationz75zzobject_classz00(502007119L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string2363z00zzcfa_typez00));
		}

	}

#ifdef __cplusplus
}
#endif
