/*===========================================================================*/
/*   (Cfa/app.scm)                                                           */
/*   Bigloo (4.6a)                                                           */
/*   Inria -- Sophia Antipolis (c)       Thu Jul 18 04:37:14 PM CEST 2024    */
/*===========================================================================*/
/* COMPILATION: (/home/serrano/prgm/project/bigloo/bigloo/bin/bigloo -ldopt -L/home/serrano/prgm/project/bigloo/bigloo/libbacktrace/home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a  -q -afile .afile -fsharing -fno-reflection -cc gcc -lib-dir /home/serrano/prgm/project/bigloo/bigloo/lib/bigloo/4.6a -unsafev -gno-error-localization -no-hello -O2 -rm -unsafe -s -stdc -indent Cfa/app.scm) */
/* GC selection */
#define THE_GC BOEHM_GC

/* unsafe mode */
#define BIGLOO_UNSAFE 1

/* traces mode */
#define BIGLOO_TRACE 0

/* standard Bigloo include */
#include <bigloo.h>

#ifdef __cplusplus
extern "C"
{
#endif
#ifndef BGL_MODULE_TYPE_DEFINITIONS
#define BGL_MODULE_TYPE_DEFINITIONS
#ifndef BGL_CFA_APP_TYPE_DEFINITIONS
#define BGL_CFA_APP_TYPE_DEFINITIONS

/* object type definitions */
	typedef struct BgL_typez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		obj_t BgL_siza7eza7;
		obj_t BgL_classz00;
		obj_t BgL_coercezd2tozd2;
		obj_t BgL_parentsz00;
		bool_t BgL_initzf3zf3;
		bool_t BgL_magiczf3zf3;
		obj_t BgL_nullz00;
		obj_t BgL_z42z42;
		obj_t BgL_aliasz00;
		obj_t BgL_pointedzd2tozd2byz00;
		obj_t BgL_tvectorz00;
		obj_t BgL_locationz00;
		obj_t BgL_importzd2locationzd2;
		int BgL_occurrencez00;
	}              *BgL_typez00_bglt;

	typedef struct BgL_valuez00_bgl
	{
		header_t header;
		obj_t widening;
	}               *BgL_valuez00_bglt;

	typedef struct BgL_variablez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
	}                  *BgL_variablez00_bglt;

	typedef struct BgL_globalz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		obj_t BgL_modulez00;
		obj_t BgL_importz00;
		bool_t BgL_evaluablezf3zf3;
		bool_t BgL_evalzf3zf3;
		obj_t BgL_libraryz00;
		obj_t BgL_pragmaz00;
		obj_t BgL_srcz00;
		obj_t BgL_jvmzd2typezd2namez00;
		obj_t BgL_initz00;
		obj_t BgL_aliasz00;
	}                *BgL_globalz00_bglt;

	typedef struct BgL_localz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_idz00;
		obj_t BgL_namez00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_valuez00_bgl *BgL_valuez00;
		obj_t BgL_accessz00;
		obj_t BgL_fastzd2alphazd2;
		obj_t BgL_removablez00;
		long BgL_occurrencez00;
		long BgL_occurrencewz00;
		bool_t BgL_userzf3zf3;
		long BgL_keyz00;
		obj_t BgL_valzd2noescapezd2;
		bool_t BgL_volatilez00;
	}               *BgL_localz00_bglt;

	typedef struct BgL_funz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
	}             *BgL_funz00_bglt;

	typedef struct BgL_sfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_propertyz00;
		obj_t BgL_argsz00;
		obj_t BgL_argszd2namezd2;
		obj_t BgL_bodyz00;
		obj_t BgL_classz00;
		obj_t BgL_dssslzd2keywordszd2;
		obj_t BgL_locz00;
		obj_t BgL_optionalsz00;
		obj_t BgL_keysz00;
		obj_t BgL_thezd2closurezd2globalz00;
		obj_t BgL_strengthz00;
		obj_t BgL_stackablez00;
	}              *BgL_sfunz00_bglt;

	typedef struct BgL_cfunz00_bgl
	{
		header_t header;
		obj_t widening;
		long BgL_arityz00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_predicatezd2ofzd2;
		obj_t BgL_stackzd2allocatorzd2;
		bool_t BgL_topzf3zf3;
		obj_t BgL_thezd2closurezd2;
		obj_t BgL_effectz00;
		obj_t BgL_failsafez00;
		obj_t BgL_argszd2noescapezd2;
		obj_t BgL_argszd2retescapezd2;
		obj_t BgL_argszd2typezd2;
		bool_t BgL_macrozf3zf3;
		bool_t BgL_infixzf3zf3;
		obj_t BgL_methodz00;
	}              *BgL_cfunz00_bglt;

	typedef struct BgL_svarz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
	}              *BgL_svarz00_bglt;

	typedef struct BgL_nodez00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
	}              *BgL_nodez00_bglt;

	typedef struct BgL_varz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		struct BgL_variablez00_bgl *BgL_variablez00;
	}             *BgL_varz00_bglt;

	typedef struct BgL_appz00_bgl
	{
		header_t header;
		obj_t widening;
		obj_t BgL_locz00;
		struct BgL_typez00_bgl *BgL_typez00;
		obj_t BgL_sidezd2effectzd2;
		obj_t BgL_keyz00;
		struct BgL_varz00_bgl *BgL_funz00;
		obj_t BgL_argsz00;
		obj_t BgL_stackablez00;
	}             *BgL_appz00_bglt;

	typedef struct BgL_approxz00_bgl
	{
		header_t header;
		obj_t widening;
		struct BgL_typez00_bgl *BgL_typez00;
		bool_t BgL_typezd2lockedzf3z21;
		obj_t BgL_allocsz00;
		bool_t BgL_topzf3zf3;
		long BgL_lostzd2stampzd2;
		obj_t BgL_dupz00;
		bool_t BgL_haszd2procedurezf3z21;
	}                *BgL_approxz00_bglt;

	typedef struct BgL_cfunzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                      *BgL_cfunzf2cinfozf2_bglt;

	typedef struct BgL_externzd2sfunzf2cinfoz20_bgl
	{
		bool_t BgL_polymorphiczf3zf3;
		struct BgL_approxz00_bgl *BgL_approxz00;
	}                               *BgL_externzd2sfunzf2cinfoz20_bglt;

	typedef struct BgL_internzd2sfunzf2cinfoz20_bgl
	{
		bool_t BgL_polymorphiczf3zf3;
		struct BgL_approxz00_bgl *BgL_approxz00;
		long BgL_stampz00;
	}                               *BgL_internzd2sfunzf2cinfoz20_bglt;

	typedef struct BgL_svarzf2cinfozf2_bgl
	{
		struct BgL_approxz00_bgl *BgL_approxz00;
		bool_t BgL_clozd2envzf3z21;
		long BgL_stampz00;
	}                      *BgL_svarzf2cinfozf2_bglt;


#endif													// BGL_CFA_APP_TYPE_DEFINITIONS
#endif													// BGL_MODULE_TYPE_DEFINITIONS

	static obj_t BGl_objectzd2initzd2zzcfa_appz00(void);
	static obj_t BGl_methodzd2initzd2zzcfa_appz00(void);
	extern BgL_typez00_bglt
		BGl_getzd2bigloozd2typez00zztype_cachez00(BgL_typez00_bglt);
	static BgL_approxz00_bglt
		BGl_z62appz12zd2cfunzf2Cinfo1519z50zzcfa_appz00(obj_t, obj_t, obj_t, obj_t);
	extern BgL_approxz00_bglt
		BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_approxz00_bglt,
		BgL_approxz00_bglt);
	extern obj_t BGl_disablezd2Xzd2Tz12z12zzcfa_procedurez00(BgL_approxz00_bglt,
		obj_t);
	static obj_t BGl_gczd2rootszd2initz00zzcfa_appz00(void);
	extern obj_t BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00;
	BGL_EXPORTED_DECL BgL_approxz00_bglt
		BGl_appz12z12zzcfa_appz00(BgL_funz00_bglt, BgL_varz00_bglt, obj_t);
	static BgL_approxz00_bglt
		BGl_z62appz12zd2internzd2sfunzf2Cin1515z82zzcfa_appz00(obj_t, obj_t, obj_t,
		obj_t);
	BGL_IMPORT obj_t BGl_genericzd2addzd2methodz12z12zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	extern obj_t BGl_cfazd2exportzd2varz12z12zzcfa_iteratez00(BgL_valuez00_bglt,
		obj_t);
	static obj_t BGl_z62appz121512z70zzcfa_appz00(obj_t, obj_t, obj_t, obj_t);
	static obj_t BGl_requirezd2initializa7ationz75zzcfa_appz00 = BUNSPEC;
	extern obj_t BGl_funz00zzast_varz00;
	BGL_IMPORT obj_t BGl_za2classesza2z00zz__objectz00;
	BGL_IMPORT obj_t BGl_registerzd2genericz12zc0zz__objectz00(obj_t, obj_t,
		obj_t, obj_t);
	static obj_t BGl_toplevelzd2initzd2zzcfa_appz00(void);
	extern BgL_approxz00_bglt
		BGl_cfazd2internzd2sfunz12z12zzcfa_iteratez00(BgL_sfunz00_bglt, obj_t);
	extern obj_t BGl_internalzd2errorzd2zztools_errorz00(obj_t, obj_t, obj_t);
	static obj_t BGl_genericzd2initzd2zzcfa_appz00(void);
	BGL_IMPORT obj_t BGl_za2inheritancesza2z00zz__objectz00;
	extern obj_t BGl_shapez00zztools_shapez00(obj_t);
	extern BgL_approxz00_bglt BGl_loosez12z12zzcfa_loosez00(BgL_approxz00_bglt,
		obj_t);
	BGL_IMPORT obj_t BGl_readz00zz__readerz00(obj_t, obj_t);
	BGL_IMPORT obj_t BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00(obj_t,
		obj_t, obj_t);
	extern obj_t BGl_externzd2sfunzf2Cinfoz20zzcfa_infoz00;
	BGL_EXPORTED_DECL obj_t BGl_modulezd2initializa7ationz75zzcfa_appz00(long,
		char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_procedurez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_approxz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_loosez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_iteratez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_cfaz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_info2z00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzcfa_infoz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_nodez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zzast_varz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_cachez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztype_typez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_errorz00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_shapez00(long, char *);
	extern obj_t BGl_modulezd2initializa7ationz75zztools_tracez00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(long,
		char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(long,
		char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(long, char *);
	BGL_IMPORT obj_t
		BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__readerz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__objectz00(long, char *);
	BGL_IMPORT obj_t BGl_modulezd2initializa7ationz75zz__bitz00(long, char *);
	extern obj_t BGl_appz00zzast_nodez00;
	static obj_t BGl_cnstzd2initzd2zzcfa_appz00(void);
	static obj_t BGl_libraryzd2moduleszd2initz00zzcfa_appz00(void);
	static obj_t BGl_importedzd2moduleszd2initz00zzcfa_appz00(void);
	static BgL_approxz00_bglt BGl_z62appz12z70zzcfa_appz00(obj_t, obj_t, obj_t,
		obj_t);
	static BgL_approxz00_bglt BGl_z62cfaz12zd2app1511za2zzcfa_appz00(obj_t,
		obj_t);
	static BgL_approxz00_bglt
		BGl_z62appz12zd2externzd2sfunzf2Cin1517z82zzcfa_appz00(obj_t, obj_t, obj_t,
		obj_t);
	extern BgL_approxz00_bglt BGl_cfaz12z12zzcfa_cfaz00(BgL_nodez00_bglt);
	extern obj_t BGl_cfunzf2Cinfozf2zzcfa_infoz00;
	extern obj_t BGl_globalz00zzast_varz00;
	static obj_t __cnst[2];


	   
		 
		DEFINE_EXPORT_BGL_GENERIC(BGl_appz12zd2envzc0zzcfa_appz00,
		BgL_bgl_za762appza712za770za7za7cf1974za7, BGl_z62appz12z70zzcfa_appz00, 0L,
		BUNSPEC, 3);
	extern obj_t BGl_cfaz12zd2envzc0zzcfa_cfaz00;
	   
		 
		DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1961z00zzcfa_appz00,
		BgL_bgl_za762appza7121512za7701975za7, BGl_z62appz121512z70zzcfa_appz00, 0L,
		BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1965z00zzcfa_appz00,
		BgL_bgl_za762cfaza712za7d2app11976za7,
		BGl_z62cfaz12zd2app1511za2zzcfa_appz00, 0L, BUNSPEC, 1);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1967z00zzcfa_appz00,
		BgL_bgl_za762appza712za7d2inte1977za7,
		BGl_z62appz12zd2internzd2sfunzf2Cin1515z82zzcfa_appz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1968z00zzcfa_appz00,
		BgL_bgl_za762appza712za7d2exte1978za7,
		BGl_z62appz12zd2externzd2sfunzf2Cin1517z82zzcfa_appz00, 0L, BUNSPEC, 3);
	      DEFINE_STATIC_BGL_PROCEDURE(BGl_proc1969z00zzcfa_appz00,
		BgL_bgl_za762appza712za7d2cfun1979za7,
		BGl_z62appz12zd2cfunzf2Cinfo1519z50zzcfa_appz00, 0L, BUNSPEC, 3);
	      DEFINE_STRING(BGl_string1962z00zzcfa_appz00,
		BgL_bgl_string1962za700za7za7c1980za7, "app!1512", 8);
	      DEFINE_STRING(BGl_string1963z00zzcfa_appz00,
		BgL_bgl_string1963za700za7za7c1981za7, "app!", 4);
	      DEFINE_STRING(BGl_string1964z00zzcfa_appz00,
		BgL_bgl_string1964za700za7za7c1982za7, "No method for this function", 27);
	      DEFINE_STRING(BGl_string1966z00zzcfa_appz00,
		BgL_bgl_string1966za700za7za7c1983za7, "cfa!::approx", 12);
	      DEFINE_STRING(BGl_string1970z00zzcfa_appz00,
		BgL_bgl_string1970za700za7za7c1984za7, "extern call", 11);
	      DEFINE_STRING(BGl_string1971z00zzcfa_appz00,
		BgL_bgl_string1971za700za7za7c1985za7, "cfa_app", 7);
	      DEFINE_STRING(BGl_string1972z00zzcfa_appz00,
		BgL_bgl_string1972za700za7za7c1986za7, "static all ", 11);

/* GC roots registration */
	static obj_t bgl_gc_roots_register()
	{
#if defined( BGL_GC_ROOTS )
#define ADD_ROOT( addr ) (addr > roots_max ? roots_max = addr : (addr < roots_min ? roots_min = addr : 0))
		void *roots_min = (void *) ULONG_MAX, *roots_max = 0;
		     ADD_ROOT((void *) (&BGl_requirezd2initializa7ationz75zzcfa_appz00));
#undef ADD_ROOT
		if   (roots_max > 0)
			     GC_add_roots(roots_min, ((void **) roots_max) + 1);
#endif
		     return BUNSPEC;
	}



/* module-initialization */
	BGL_EXPORTED_DEF obj_t BGl_modulezd2initializa7ationz75zzcfa_appz00(long
		BgL_checksumz00_3873, char *BgL_fromz00_3874)
	{
		{
			if (CBOOL(BGl_requirezd2initializa7ationz75zzcfa_appz00))
				{
					BGl_requirezd2initializa7ationz75zzcfa_appz00 = BBOOL(((bool_t) 0));
					BGl_gczd2rootszd2initz00zzcfa_appz00();
					BGl_libraryzd2moduleszd2initz00zzcfa_appz00();
					BGl_cnstzd2initzd2zzcfa_appz00();
					BGl_importedzd2moduleszd2initz00zzcfa_appz00();
					BGl_genericzd2initzd2zzcfa_appz00();
					BGl_methodzd2initzd2zzcfa_appz00();
					return BGl_toplevelzd2initzd2zzcfa_appz00();
				}
			else
				{
					return BUNSPEC;
				}
		}

	}



/* library-modules-init */
	obj_t BGl_libraryzd2moduleszd2initz00zzcfa_appz00(void)
	{
		{	/* Cfa/app.scm 15 */
			BGl_modulezd2initializa7ationz75zz__objectz00(0L, "cfa_app");
			BGl_modulezd2initializa7ationz75zz__readerz00(0L, "cfa_app");
			BGl_modulezd2initializa7ationz75zz__r4_ports_6_10_1z00(0L, "cfa_app");
			BGl_modulezd2initializa7ationz75zz__r4_strings_6_7z00(0L, "cfa_app");
			BGl_modulezd2initializa7ationz75zz__r4_equivalence_6_2z00(0L, "cfa_app");
			BGl_modulezd2initializa7ationz75zz__r4_pairs_and_lists_6_3z00(0L,
				"cfa_app");
			BGl_modulezd2initializa7ationz75zz__r4_vectors_6_8z00(0L, "cfa_app");
			BGl_modulezd2initializa7ationz75zz__bitz00(0L, "cfa_app");
			BGl_modulezd2initializa7ationz75zz__r4_numbers_6_5_fixnumz00(0L,
				"cfa_app");
			return BUNSPEC;
		}

	}



/* cnst-init */
	obj_t BGl_cnstzd2initzd2zzcfa_appz00(void)
	{
		{	/* Cfa/app.scm 15 */
			{	/* Cfa/app.scm 15 */
				obj_t BgL_cportz00_3775;

				{	/* Cfa/app.scm 15 */
					obj_t BgL_stringz00_3782;

					BgL_stringz00_3782 = BGl_string1972z00zzcfa_appz00;
					{	/* Cfa/app.scm 15 */
						obj_t BgL_startz00_3783;

						BgL_startz00_3783 = BINT(0L);
						{	/* Cfa/app.scm 15 */
							obj_t BgL_endz00_3784;

							BgL_endz00_3784 =
								BINT(STRING_LENGTH(((obj_t) BgL_stringz00_3782)));
							{	/* Cfa/app.scm 15 */

								BgL_cportz00_3775 =
									BGl_openzd2inputzd2stringz12z12zz__r4_ports_6_10_1z00
									(BgL_stringz00_3782, BgL_startz00_3783, BgL_endz00_3784);
				}}}}
				{
					long BgL_iz00_3776;

					BgL_iz00_3776 = 1L;
				BgL_loopz00_3777:
					if ((BgL_iz00_3776 == -1L))
						{	/* Cfa/app.scm 15 */
							return BUNSPEC;
						}
					else
						{	/* Cfa/app.scm 15 */
							{	/* Cfa/app.scm 15 */
								obj_t BgL_arg1973z00_3778;

								{	/* Cfa/app.scm 15 */

									{	/* Cfa/app.scm 15 */
										obj_t BgL_locationz00_3780;

										BgL_locationz00_3780 = BBOOL(((bool_t) 0));
										{	/* Cfa/app.scm 15 */

											BgL_arg1973z00_3778 =
												BGl_readz00zz__readerz00(BgL_cportz00_3775,
												BgL_locationz00_3780);
										}
									}
								}
								{	/* Cfa/app.scm 15 */
									int BgL_tmpz00_3903;

									BgL_tmpz00_3903 = (int) (BgL_iz00_3776);
									CNST_TABLE_SET(BgL_tmpz00_3903, BgL_arg1973z00_3778);
							}}
							{	/* Cfa/app.scm 15 */
								int BgL_auxz00_3781;

								BgL_auxz00_3781 = (int) ((BgL_iz00_3776 - 1L));
								{
									long BgL_iz00_3908;

									BgL_iz00_3908 = (long) (BgL_auxz00_3781);
									BgL_iz00_3776 = BgL_iz00_3908;
									goto BgL_loopz00_3777;
								}
							}
						}
				}
			}
		}

	}



/* gc-roots-init */
	obj_t BGl_gczd2rootszd2initz00zzcfa_appz00(void)
	{
		{	/* Cfa/app.scm 15 */
			return bgl_gc_roots_register();
		}

	}



/* toplevel-init */
	obj_t BGl_toplevelzd2initzd2zzcfa_appz00(void)
	{
		{	/* Cfa/app.scm 15 */
			return BUNSPEC;
		}

	}



/* object-init */
	obj_t BGl_objectzd2initzd2zzcfa_appz00(void)
	{
		{	/* Cfa/app.scm 15 */
			return BUNSPEC;
		}

	}



/* generic-init */
	obj_t BGl_genericzd2initzd2zzcfa_appz00(void)
	{
		{	/* Cfa/app.scm 15 */
			return
				BGl_registerzd2genericz12zc0zz__objectz00
				(BGl_appz12zd2envzc0zzcfa_appz00, BGl_proc1961z00zzcfa_appz00,
				BGl_funz00zzast_varz00, BGl_string1962z00zzcfa_appz00);
		}

	}



/* &app!1512 */
	obj_t BGl_z62appz121512z70zzcfa_appz00(obj_t BgL_envz00_3747,
		obj_t BgL_funz00_3748, obj_t BgL_varz00_3749,
		obj_t BgL_argszd2approxzd2_3750)
	{
		{	/* Cfa/app.scm 47 */
			{	/* Cfa/app.scm 48 */
				obj_t BgL_arg1546z00_3788;

				{	/* Cfa/app.scm 48 */
					obj_t BgL_arg1552z00_3789;

					BgL_arg1552z00_3789 =
						BGl_shapez00zztools_shapez00(
						((obj_t) ((BgL_varz00_bglt) BgL_varz00_3749)));
					BgL_arg1546z00_3788 =
						MAKE_YOUNG_PAIR(
						((obj_t) ((BgL_funz00_bglt) BgL_funz00_3748)), BgL_arg1552z00_3789);
				}
				return
					BGl_internalzd2errorzd2zztools_errorz00(BGl_string1963z00zzcfa_appz00,
					BGl_string1964z00zzcfa_appz00, BgL_arg1546z00_3788);
			}
		}

	}



/* app! */
	BGL_EXPORTED_DEF BgL_approxz00_bglt BGl_appz12z12zzcfa_appz00(BgL_funz00_bglt
		BgL_funz00_4, BgL_varz00_bglt BgL_varz00_5, obj_t BgL_argszd2approxzd2_6)
	{
		{	/* Cfa/app.scm 47 */
			{	/* Cfa/app.scm 47 */
				obj_t BgL_method1513z00_2989;

				{	/* Cfa/app.scm 47 */
					obj_t BgL_res1953z00_3608;

					{	/* Cfa/app.scm 47 */
						long BgL_objzd2classzd2numz00_3579;

						BgL_objzd2classzd2numz00_3579 =
							BGL_OBJECT_CLASS_NUM(((BgL_objectz00_bglt) BgL_funz00_4));
						{	/* Cfa/app.scm 47 */
							obj_t BgL_arg1811z00_3580;

							BgL_arg1811z00_3580 =
								PROCEDURE_REF(BGl_appz12zd2envzc0zzcfa_appz00, (int) (1L));
							{	/* Cfa/app.scm 47 */
								int BgL_offsetz00_3583;

								BgL_offsetz00_3583 = (int) (BgL_objzd2classzd2numz00_3579);
								{	/* Cfa/app.scm 47 */
									long BgL_offsetz00_3584;

									BgL_offsetz00_3584 =
										((long) (BgL_offsetz00_3583) - OBJECT_TYPE);
									{	/* Cfa/app.scm 47 */
										long BgL_modz00_3585;

										BgL_modz00_3585 =
											(BgL_offsetz00_3584 >> (int) ((long) ((int) (4L))));
										{	/* Cfa/app.scm 47 */
											long BgL_restz00_3587;

											BgL_restz00_3587 =
												(BgL_offsetz00_3584 &
												(long) (
													(int) (
														((long) (
																(int) (
																	(1L <<
																		(int) ((long) ((int) (4L)))))) - 1L))));
											{	/* Cfa/app.scm 47 */

												{	/* Cfa/app.scm 47 */
													obj_t BgL_bucketz00_3589;

													BgL_bucketz00_3589 =
														VECTOR_REF(
														((obj_t) BgL_arg1811z00_3580), BgL_modz00_3585);
													BgL_res1953z00_3608 =
														VECTOR_REF(
														((obj_t) BgL_bucketz00_3589), BgL_restz00_3587);
					}}}}}}}}
					BgL_method1513z00_2989 = BgL_res1953z00_3608;
				}
				return
					((BgL_approxz00_bglt)
					BGL_PROCEDURE_CALL3(BgL_method1513z00_2989,
						((obj_t) BgL_funz00_4),
						((obj_t) BgL_varz00_5), BgL_argszd2approxzd2_6));
			}
		}

	}



/* &app! */
	BgL_approxz00_bglt BGl_z62appz12z70zzcfa_appz00(obj_t BgL_envz00_3751,
		obj_t BgL_funz00_3752, obj_t BgL_varz00_3753,
		obj_t BgL_argszd2approxzd2_3754)
	{
		{	/* Cfa/app.scm 47 */
			return
				BGl_appz12z12zzcfa_appz00(
				((BgL_funz00_bglt) BgL_funz00_3752),
				((BgL_varz00_bglt) BgL_varz00_3753), BgL_argszd2approxzd2_3754);
		}

	}



/* method-init */
	obj_t BGl_methodzd2initzd2zzcfa_appz00(void)
	{
		{	/* Cfa/app.scm 15 */
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_cfaz12zd2envzc0zzcfa_cfaz00, BGl_appz00zzast_nodez00,
				BGl_proc1965z00zzcfa_appz00, BGl_string1966z00zzcfa_appz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_appz12zd2envzc0zzcfa_appz00,
				BGl_internzd2sfunzf2Cinfoz20zzcfa_infoz00, BGl_proc1967z00zzcfa_appz00,
				BGl_string1963z00zzcfa_appz00);
			BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_appz12zd2envzc0zzcfa_appz00,
				BGl_externzd2sfunzf2Cinfoz20zzcfa_infoz00, BGl_proc1968z00zzcfa_appz00,
				BGl_string1963z00zzcfa_appz00);
			return
				BGl_genericzd2addzd2methodz12z12zz__objectz00
				(BGl_appz12zd2envzc0zzcfa_appz00, BGl_cfunzf2Cinfozf2zzcfa_infoz00,
				BGl_proc1969z00zzcfa_appz00, BGl_string1963z00zzcfa_appz00);
		}

	}



/* &app!-cfun/Cinfo1519 */
	BgL_approxz00_bglt BGl_z62appz12zd2cfunzf2Cinfo1519z50zzcfa_appz00(obj_t
		BgL_envz00_3759, obj_t BgL_funz00_3760, obj_t BgL_varz00_3761,
		obj_t BgL_argszd2approxzd2_3762)
	{
		{	/* Cfa/app.scm 115 */
			if (
				(((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt)
								((BgL_cfunz00_bglt) BgL_funz00_3760))))->BgL_topzf3zf3))
				{
					obj_t BgL_l1506z00_3793;

					BgL_l1506z00_3793 = BgL_argszd2approxzd2_3762;
				BgL_zc3z04anonymousza31653ze3z87_3792:
					if (PAIRP(BgL_l1506z00_3793))
						{	/* Tools/trace.sch 53 */
							{	/* Tools/trace.sch 53 */
								obj_t BgL_az00_3794;

								BgL_az00_3794 = CAR(BgL_l1506z00_3793);
								BGl_loosez12z12zzcfa_loosez00(
									((BgL_approxz00_bglt) BgL_az00_3794), CNST_TABLE_REF(0));
							}
							{
								obj_t BgL_l1506z00_3970;

								BgL_l1506z00_3970 = CDR(BgL_l1506z00_3793);
								BgL_l1506z00_3793 = BgL_l1506z00_3970;
								goto BgL_zc3z04anonymousza31653ze3z87_3792;
							}
						}
					else
						{	/* Tools/trace.sch 53 */
							((bool_t) 1);
						}
				}
			else
				{
					obj_t BgL_l1508z00_3796;

					BgL_l1508z00_3796 = BgL_argszd2approxzd2_3762;
				BgL_zc3z04anonymousza31662ze3z87_3795:
					if (PAIRP(BgL_l1508z00_3796))
						{	/* Tools/trace.sch 53 */
							{	/* Tools/trace.sch 53 */
								obj_t BgL_az00_3797;

								BgL_az00_3797 = CAR(BgL_l1508z00_3796);
								BGl_disablezd2Xzd2Tz12z12zzcfa_procedurez00(
									((BgL_approxz00_bglt) BgL_az00_3797),
									BGl_string1970z00zzcfa_appz00);
							}
							{
								obj_t BgL_l1508z00_3977;

								BgL_l1508z00_3977 = CDR(BgL_l1508z00_3796);
								BgL_l1508z00_3796 = BgL_l1508z00_3977;
								goto BgL_zc3z04anonymousza31662ze3z87_3795;
							}
						}
					else
						{	/* Tools/trace.sch 53 */
							((bool_t) 1);
						}
				}
			{
				BgL_cfunzf2cinfozf2_bglt BgL_auxz00_3979;

				{
					obj_t BgL_auxz00_3980;

					{	/* Tools/trace.sch 53 */
						BgL_objectz00_bglt BgL_tmpz00_3981;

						BgL_tmpz00_3981 =
							((BgL_objectz00_bglt) ((BgL_cfunz00_bglt) BgL_funz00_3760));
						BgL_auxz00_3980 = BGL_OBJECT_WIDENING(BgL_tmpz00_3981);
					}
					BgL_auxz00_3979 = ((BgL_cfunzf2cinfozf2_bglt) BgL_auxz00_3980);
				}
				return
					(((BgL_cfunzf2cinfozf2_bglt) COBJECT(BgL_auxz00_3979))->
					BgL_approxz00);
			}
		}

	}



/* &app!-extern-sfun/Cin1517 */
	BgL_approxz00_bglt
		BGl_z62appz12zd2externzd2sfunzf2Cin1517z82zzcfa_appz00(obj_t
		BgL_envz00_3763, obj_t BgL_funz00_3764, obj_t BgL_varz00_3765,
		obj_t BgL_argszd2approxzd2_3766)
	{
		{	/* Cfa/app.scm 93 */
			if (
				(((BgL_funz00_bglt) COBJECT(
							((BgL_funz00_bglt)
								((BgL_sfunz00_bglt) BgL_funz00_3764))))->BgL_topzf3zf3))
				{
					obj_t BgL_l1502z00_3801;

					BgL_l1502z00_3801 = BgL_argszd2approxzd2_3766;
				BgL_zc3z04anonymousza31629ze3z87_3800:
					if (PAIRP(BgL_l1502z00_3801))
						{	/* Cfa/app.scm 100 */
							{	/* Cfa/app.scm 100 */
								obj_t BgL_az00_3802;

								BgL_az00_3802 = CAR(BgL_l1502z00_3801);
								BGl_loosez12z12zzcfa_loosez00(
									((BgL_approxz00_bglt) BgL_az00_3802), CNST_TABLE_REF(0));
							}
							{
								obj_t BgL_l1502z00_3997;

								BgL_l1502z00_3997 = CDR(BgL_l1502z00_3801);
								BgL_l1502z00_3801 = BgL_l1502z00_3997;
								goto BgL_zc3z04anonymousza31629ze3z87_3800;
							}
						}
					else
						{	/* Cfa/app.scm 100 */
							((bool_t) 1);
						}
				}
			else
				{
					obj_t BgL_l1504z00_3804;

					BgL_l1504z00_3804 = BgL_argszd2approxzd2_3766;
				BgL_zc3z04anonymousza31643ze3z87_3803:
					if (PAIRP(BgL_l1504z00_3804))
						{	/* Cfa/app.scm 102 */
							{	/* Cfa/app.scm 102 */
								obj_t BgL_az00_3805;

								BgL_az00_3805 = CAR(BgL_l1504z00_3804);
								BGl_disablezd2Xzd2Tz12z12zzcfa_procedurez00(
									((BgL_approxz00_bglt) BgL_az00_3805),
									BGl_string1970z00zzcfa_appz00);
							}
							{
								obj_t BgL_l1504z00_4004;

								BgL_l1504z00_4004 = CDR(BgL_l1504z00_3804);
								BgL_l1504z00_3804 = BgL_l1504z00_4004;
								goto BgL_zc3z04anonymousza31643ze3z87_3803;
							}
						}
					else
						{	/* Cfa/app.scm 102 */
							((bool_t) 1);
						}
				}
			{	/* Cfa/app.scm 103 */
				bool_t BgL_test1995z00_4006;

				{
					BgL_externzd2sfunzf2cinfoz20_bglt BgL_auxz00_4007;

					{
						obj_t BgL_auxz00_4008;

						{	/* Cfa/app.scm 103 */
							BgL_objectz00_bglt BgL_tmpz00_4009;

							BgL_tmpz00_4009 =
								((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_3764));
							BgL_auxz00_4008 = BGL_OBJECT_WIDENING(BgL_tmpz00_4009);
						}
						BgL_auxz00_4007 =
							((BgL_externzd2sfunzf2cinfoz20_bglt) BgL_auxz00_4008);
					}
					BgL_test1995z00_4006 =
						(((BgL_externzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_4007))->
						BgL_polymorphiczf3zf3);
				}
				if (BgL_test1995z00_4006)
					{	/* Cfa/app.scm 104 */
						BgL_approxz00_bglt BgL_i1166z00_3806;

						{
							BgL_externzd2sfunzf2cinfoz20_bglt BgL_auxz00_4015;

							{
								obj_t BgL_auxz00_4016;

								{	/* Cfa/app.scm 105 */
									BgL_objectz00_bglt BgL_tmpz00_4017;

									BgL_tmpz00_4017 =
										((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_3764));
									BgL_auxz00_4016 = BGL_OBJECT_WIDENING(BgL_tmpz00_4017);
								}
								BgL_auxz00_4015 =
									((BgL_externzd2sfunzf2cinfoz20_bglt) BgL_auxz00_4016);
							}
							BgL_i1166z00_3806 =
								(((BgL_externzd2sfunzf2cinfoz20_bglt)
									COBJECT(BgL_auxz00_4015))->BgL_approxz00);
						}
						{
							BgL_typez00_bglt BgL_auxz00_4023;

							{	/* Cfa/app.scm 105 */
								BgL_typez00_bglt BgL_arg1650z00_3807;

								{	/* Cfa/app.scm 105 */
									BgL_approxz00_bglt BgL_arg1651z00_3808;

									{
										BgL_externzd2sfunzf2cinfoz20_bglt BgL_auxz00_4024;

										{
											obj_t BgL_auxz00_4025;

											{	/* Cfa/app.scm 105 */
												BgL_objectz00_bglt BgL_tmpz00_4026;

												BgL_tmpz00_4026 =
													((BgL_objectz00_bglt)
													((BgL_sfunz00_bglt) BgL_funz00_3764));
												BgL_auxz00_4025 = BGL_OBJECT_WIDENING(BgL_tmpz00_4026);
											}
											BgL_auxz00_4024 =
												((BgL_externzd2sfunzf2cinfoz20_bglt) BgL_auxz00_4025);
										}
										BgL_arg1651z00_3808 =
											(((BgL_externzd2sfunzf2cinfoz20_bglt)
												COBJECT(BgL_auxz00_4024))->BgL_approxz00);
									}
									BgL_arg1650z00_3807 =
										(((BgL_approxz00_bglt) COBJECT(BgL_arg1651z00_3808))->
										BgL_typez00);
								}
								BgL_auxz00_4023 =
									BGl_getzd2bigloozd2typez00zztype_cachez00
									(BgL_arg1650z00_3807);
							}
							((((BgL_approxz00_bglt) COBJECT(BgL_i1166z00_3806))->
									BgL_typez00) = ((BgL_typez00_bglt) BgL_auxz00_4023), BUNSPEC);
						}
					}
				else
					{	/* Cfa/app.scm 103 */
						BFALSE;
					}
			}
			{
				BgL_externzd2sfunzf2cinfoz20_bglt BgL_auxz00_4035;

				{
					obj_t BgL_auxz00_4036;

					{	/* Cfa/app.scm 110 */
						BgL_objectz00_bglt BgL_tmpz00_4037;

						BgL_tmpz00_4037 =
							((BgL_objectz00_bglt) ((BgL_sfunz00_bglt) BgL_funz00_3764));
						BgL_auxz00_4036 = BGL_OBJECT_WIDENING(BgL_tmpz00_4037);
					}
					BgL_auxz00_4035 =
						((BgL_externzd2sfunzf2cinfoz20_bglt) BgL_auxz00_4036);
				}
				return
					(((BgL_externzd2sfunzf2cinfoz20_bglt) COBJECT(BgL_auxz00_4035))->
					BgL_approxz00);
			}
		}

	}



/* &app!-intern-sfun/Cin1515 */
	BgL_approxz00_bglt
		BGl_z62appz12zd2internzd2sfunzf2Cin1515z82zzcfa_appz00(obj_t
		BgL_envz00_3767, obj_t BgL_funz00_3768, obj_t BgL_varz00_3769,
		obj_t BgL_argszd2approxzd2_3770)
	{
		{	/* Cfa/app.scm 53 */
			{	/* Cfa/app.scm 64 */
				obj_t BgL_g1501z00_3811;

				BgL_g1501z00_3811 =
					(((BgL_sfunz00_bglt) COBJECT(
							((BgL_sfunz00_bglt)
								((BgL_sfunz00_bglt) BgL_funz00_3768))))->BgL_argsz00);
				{
					obj_t BgL_ll1498z00_3813;
					obj_t BgL_ll1499z00_3814;

					BgL_ll1498z00_3813 = BgL_g1501z00_3811;
					BgL_ll1499z00_3814 = BgL_argszd2approxzd2_3770;
				BgL_zc3z04anonymousza31585ze3z87_3812:
					if (NULLP(BgL_ll1498z00_3813))
						{	/* Cfa/app.scm 64 */
							((bool_t) 1);
						}
					else
						{	/* Cfa/app.scm 64 */
							{	/* Cfa/app.scm 65 */
								obj_t BgL_formalz00_3815;
								obj_t BgL_approxz00_3816;

								BgL_formalz00_3815 = CAR(((obj_t) BgL_ll1498z00_3813));
								BgL_approxz00_3816 = CAR(((obj_t) BgL_ll1499z00_3814));
								{	/* Cfa/app.scm 65 */
									BgL_approxz00_bglt BgL_arg1589z00_3817;

									{	/* Cfa/app.scm 65 */
										BgL_svarz00_bglt BgL_oz00_3818;

										BgL_oz00_3818 =
											((BgL_svarz00_bglt)
											(((BgL_variablez00_bglt) COBJECT(
														((BgL_variablez00_bglt)
															((BgL_localz00_bglt) BgL_formalz00_3815))))->
												BgL_valuez00));
										{
											BgL_svarzf2cinfozf2_bglt BgL_auxz00_4056;

											{
												obj_t BgL_auxz00_4057;

												{	/* Cfa/app.scm 65 */
													BgL_objectz00_bglt BgL_tmpz00_4058;

													BgL_tmpz00_4058 =
														((BgL_objectz00_bglt) BgL_oz00_3818);
													BgL_auxz00_4057 =
														BGL_OBJECT_WIDENING(BgL_tmpz00_4058);
												}
												BgL_auxz00_4056 =
													((BgL_svarzf2cinfozf2_bglt) BgL_auxz00_4057);
											}
											BgL_arg1589z00_3817 =
												(((BgL_svarzf2cinfozf2_bglt) COBJECT(BgL_auxz00_4056))->
												BgL_approxz00);
										}
									}
									BGl_unionzd2approxz12zc0zzcfa_approxz00(BgL_arg1589z00_3817,
										((BgL_approxz00_bglt) BgL_approxz00_3816));
								}
							}
							{	/* Cfa/app.scm 64 */
								obj_t BgL_arg1593z00_3819;
								obj_t BgL_arg1594z00_3820;

								BgL_arg1593z00_3819 = CDR(((obj_t) BgL_ll1498z00_3813));
								BgL_arg1594z00_3820 = CDR(((obj_t) BgL_ll1499z00_3814));
								{
									obj_t BgL_ll1499z00_4070;
									obj_t BgL_ll1498z00_4069;

									BgL_ll1498z00_4069 = BgL_arg1593z00_3819;
									BgL_ll1499z00_4070 = BgL_arg1594z00_3820;
									BgL_ll1499z00_3814 = BgL_ll1499z00_4070;
									BgL_ll1498z00_3813 = BgL_ll1498z00_4069;
									goto BgL_zc3z04anonymousza31585ze3z87_3812;
								}
							}
						}
				}
			}
			{	/* Cfa/app.scm 76 */
				bool_t BgL_test1997z00_4071;

				{	/* Cfa/app.scm 76 */
					bool_t BgL_test1998z00_4072;

					{	/* Cfa/app.scm 76 */
						BgL_variablez00_bglt BgL_arg1626z00_3821;

						BgL_arg1626z00_3821 =
							(((BgL_varz00_bglt) COBJECT(
									((BgL_varz00_bglt) BgL_varz00_3769)))->BgL_variablez00);
						{	/* Cfa/app.scm 76 */
							obj_t BgL_classz00_3822;

							BgL_classz00_3822 = BGl_globalz00zzast_varz00;
							{	/* Cfa/app.scm 76 */
								BgL_objectz00_bglt BgL_arg1807z00_3823;

								{	/* Cfa/app.scm 76 */
									obj_t BgL_tmpz00_4075;

									BgL_tmpz00_4075 =
										((obj_t) ((BgL_objectz00_bglt) BgL_arg1626z00_3821));
									BgL_arg1807z00_3823 = (BgL_objectz00_bglt) (BgL_tmpz00_4075);
								}
								if (BGL_CONDEXPAND_ISA_ARCH64())
									{	/* Cfa/app.scm 76 */
										long BgL_idxz00_3824;

										BgL_idxz00_3824 =
											BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3823);
										BgL_test1998z00_4072 =
											(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
												(BgL_idxz00_3824 + 2L)) == BgL_classz00_3822);
									}
								else
									{	/* Cfa/app.scm 76 */
										bool_t BgL_res1954z00_3827;

										{	/* Cfa/app.scm 76 */
											obj_t BgL_oclassz00_3828;

											{	/* Cfa/app.scm 76 */
												obj_t BgL_arg1815z00_3829;
												long BgL_arg1816z00_3830;

												BgL_arg1815z00_3829 =
													(BGl_za2classesza2z00zz__objectz00);
												{	/* Cfa/app.scm 76 */
													long BgL_arg1817z00_3831;

													BgL_arg1817z00_3831 =
														BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3823);
													BgL_arg1816z00_3830 =
														(BgL_arg1817z00_3831 - OBJECT_TYPE);
												}
												BgL_oclassz00_3828 =
													VECTOR_REF(BgL_arg1815z00_3829, BgL_arg1816z00_3830);
											}
											{	/* Cfa/app.scm 76 */
												bool_t BgL__ortest_1115z00_3832;

												BgL__ortest_1115z00_3832 =
													(BgL_classz00_3822 == BgL_oclassz00_3828);
												if (BgL__ortest_1115z00_3832)
													{	/* Cfa/app.scm 76 */
														BgL_res1954z00_3827 = BgL__ortest_1115z00_3832;
													}
												else
													{	/* Cfa/app.scm 76 */
														long BgL_odepthz00_3833;

														{	/* Cfa/app.scm 76 */
															obj_t BgL_arg1804z00_3834;

															BgL_arg1804z00_3834 = (BgL_oclassz00_3828);
															BgL_odepthz00_3833 =
																BGL_CLASS_DEPTH(BgL_arg1804z00_3834);
														}
														if ((2L < BgL_odepthz00_3833))
															{	/* Cfa/app.scm 76 */
																obj_t BgL_arg1802z00_3835;

																{	/* Cfa/app.scm 76 */
																	obj_t BgL_arg1803z00_3836;

																	BgL_arg1803z00_3836 = (BgL_oclassz00_3828);
																	BgL_arg1802z00_3835 =
																		BGL_CLASS_ANCESTORS_REF(BgL_arg1803z00_3836,
																		2L);
																}
																BgL_res1954z00_3827 =
																	(BgL_arg1802z00_3835 == BgL_classz00_3822);
															}
														else
															{	/* Cfa/app.scm 76 */
																BgL_res1954z00_3827 = ((bool_t) 0);
															}
													}
											}
										}
										BgL_test1998z00_4072 = BgL_res1954z00_3827;
									}
							}
						}
					}
					if (BgL_test1998z00_4072)
						{	/* Cfa/app.scm 77 */
							bool_t BgL_test2002z00_4098;

							{	/* Cfa/app.scm 77 */
								BgL_variablez00_bglt BgL_arg1625z00_3837;

								BgL_arg1625z00_3837 =
									(((BgL_varz00_bglt) COBJECT(
											((BgL_varz00_bglt) BgL_varz00_3769)))->BgL_variablez00);
								{	/* Cfa/app.scm 77 */
									obj_t BgL_classz00_3838;

									BgL_classz00_3838 = BGl_globalz00zzast_varz00;
									{	/* Cfa/app.scm 77 */
										BgL_objectz00_bglt BgL_arg1807z00_3839;

										{	/* Cfa/app.scm 77 */
											obj_t BgL_tmpz00_4101;

											BgL_tmpz00_4101 =
												((obj_t) ((BgL_objectz00_bglt) BgL_arg1625z00_3837));
											BgL_arg1807z00_3839 =
												(BgL_objectz00_bglt) (BgL_tmpz00_4101);
										}
										if (BGL_CONDEXPAND_ISA_ARCH64())
											{	/* Cfa/app.scm 77 */
												long BgL_idxz00_3840;

												BgL_idxz00_3840 =
													BGL_OBJECT_INHERITANCE_NUM(BgL_arg1807z00_3839);
												BgL_test2002z00_4098 =
													(VECTOR_REF(BGl_za2inheritancesza2z00zz__objectz00,
														(BgL_idxz00_3840 + 2L)) == BgL_classz00_3838);
											}
										else
											{	/* Cfa/app.scm 77 */
												bool_t BgL_res1955z00_3843;

												{	/* Cfa/app.scm 77 */
													obj_t BgL_oclassz00_3844;

													{	/* Cfa/app.scm 77 */
														obj_t BgL_arg1815z00_3845;
														long BgL_arg1816z00_3846;

														BgL_arg1815z00_3845 =
															(BGl_za2classesza2z00zz__objectz00);
														{	/* Cfa/app.scm 77 */
															long BgL_arg1817z00_3847;

															BgL_arg1817z00_3847 =
																BGL_OBJECT_CLASS_NUM(BgL_arg1807z00_3839);
															BgL_arg1816z00_3846 =
																(BgL_arg1817z00_3847 - OBJECT_TYPE);
														}
														BgL_oclassz00_3844 =
															VECTOR_REF(BgL_arg1815z00_3845,
															BgL_arg1816z00_3846);
													}
													{	/* Cfa/app.scm 77 */
														bool_t BgL__ortest_1115z00_3848;

														BgL__ortest_1115z00_3848 =
															(BgL_classz00_3838 == BgL_oclassz00_3844);
														if (BgL__ortest_1115z00_3848)
															{	/* Cfa/app.scm 77 */
																BgL_res1955z00_3843 = BgL__ortest_1115z00_3848;
															}
														else
															{	/* Cfa/app.scm 77 */
																long BgL_odepthz00_3849;

																{	/* Cfa/app.scm 77 */
																	obj_t BgL_arg1804z00_3850;

																	BgL_arg1804z00_3850 = (BgL_oclassz00_3844);
																	BgL_odepthz00_3849 =
																		BGL_CLASS_DEPTH(BgL_arg1804z00_3850);
																}
																if ((2L < BgL_odepthz00_3849))
																	{	/* Cfa/app.scm 77 */
																		obj_t BgL_arg1802z00_3851;

																		{	/* Cfa/app.scm 77 */
																			obj_t BgL_arg1803z00_3852;

																			BgL_arg1803z00_3852 =
																				(BgL_oclassz00_3844);
																			BgL_arg1802z00_3851 =
																				BGL_CLASS_ANCESTORS_REF
																				(BgL_arg1803z00_3852, 2L);
																		}
																		BgL_res1955z00_3843 =
																			(BgL_arg1802z00_3851 ==
																			BgL_classz00_3838);
																	}
																else
																	{	/* Cfa/app.scm 77 */
																		BgL_res1955z00_3843 = ((bool_t) 0);
																	}
															}
													}
												}
												BgL_test2002z00_4098 = BgL_res1955z00_3843;
											}
									}
								}
							}
							if (BgL_test2002z00_4098)
								{	/* Cfa/app.scm 77 */
									BgL_test1997z00_4071 =
										(
										(((BgL_globalz00_bglt) COBJECT(
													((BgL_globalz00_bglt)
														(((BgL_varz00_bglt) COBJECT(
																	((BgL_varz00_bglt) BgL_varz00_3769)))->
															BgL_variablez00))))->BgL_importz00) ==
										CNST_TABLE_REF(1));
								}
							else
								{	/* Cfa/app.scm 77 */
									BgL_test1997z00_4071 = ((bool_t) 0);
								}
						}
					else
						{	/* Cfa/app.scm 76 */
							BgL_test1997z00_4071 = ((bool_t) 1);
						}
				}
				if (BgL_test1997z00_4071)
					{	/* Cfa/app.scm 80 */
						BgL_approxz00_bglt BgL_az00_3853;

						{	/* Cfa/app.scm 80 */
							BgL_variablez00_bglt BgL_arg1611z00_3854;

							BgL_arg1611z00_3854 =
								(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_varz00_3769)))->BgL_variablez00);
							BgL_az00_3853 =
								BGl_cfazd2internzd2sfunz12z12zzcfa_iteratez00(
								((BgL_sfunz00_bglt) BgL_funz00_3768),
								((obj_t) BgL_arg1611z00_3854));
						}
						return BgL_az00_3853;
					}
				else
					{	/* Cfa/app.scm 85 */
						obj_t BgL_az00_3855;

						{	/* Cfa/app.scm 85 */
							BgL_variablez00_bglt BgL_arg1613z00_3856;

							BgL_arg1613z00_3856 =
								(((BgL_varz00_bglt) COBJECT(
										((BgL_varz00_bglt) BgL_varz00_3769)))->BgL_variablez00);
							BgL_az00_3855 =
								BGl_cfazd2exportzd2varz12z12zzcfa_iteratez00(
								((BgL_valuez00_bglt)
									((BgL_sfunz00_bglt) BgL_funz00_3768)),
								((obj_t) BgL_arg1613z00_3856));
						}
						return ((BgL_approxz00_bglt) BgL_az00_3855);
					}
			}
		}

	}



/* &cfa!-app1511 */
	BgL_approxz00_bglt BGl_z62cfaz12zd2app1511za2zzcfa_appz00(obj_t
		BgL_envz00_3771, obj_t BgL_nodez00_3772)
	{
		{	/* Cfa/app.scm 35 */
			{	/* Cfa/app.scm 39 */
				obj_t BgL_argszd2approxzd2_3858;

				{	/* Cfa/app.scm 39 */
					obj_t BgL_l1492z00_3859;

					BgL_l1492z00_3859 =
						(((BgL_appz00_bglt) COBJECT(
								((BgL_appz00_bglt) BgL_nodez00_3772)))->BgL_argsz00);
					if (NULLP(BgL_l1492z00_3859))
						{	/* Cfa/app.scm 39 */
							BgL_argszd2approxzd2_3858 = BNIL;
						}
					else
						{	/* Cfa/app.scm 39 */
							obj_t BgL_head1494z00_3860;

							{	/* Cfa/app.scm 39 */
								BgL_approxz00_bglt BgL_arg1576z00_3861;

								{	/* Cfa/app.scm 39 */
									obj_t BgL_arg1584z00_3862;

									BgL_arg1584z00_3862 = CAR(((obj_t) BgL_l1492z00_3859));
									BgL_arg1576z00_3861 =
										BGl_cfaz12z12zzcfa_cfaz00(
										((BgL_nodez00_bglt) BgL_arg1584z00_3862));
								}
								BgL_head1494z00_3860 =
									MAKE_YOUNG_PAIR(((obj_t) BgL_arg1576z00_3861), BNIL);
							}
							{	/* Cfa/app.scm 39 */
								obj_t BgL_g1497z00_3863;

								BgL_g1497z00_3863 = CDR(((obj_t) BgL_l1492z00_3859));
								{
									obj_t BgL_l1492z00_3865;
									obj_t BgL_tail1495z00_3866;

									BgL_l1492z00_3865 = BgL_g1497z00_3863;
									BgL_tail1495z00_3866 = BgL_head1494z00_3860;
								BgL_zc3z04anonymousza31566ze3z87_3864:
									if (NULLP(BgL_l1492z00_3865))
										{	/* Cfa/app.scm 39 */
											BgL_argszd2approxzd2_3858 = BgL_head1494z00_3860;
										}
									else
										{	/* Cfa/app.scm 39 */
											obj_t BgL_newtail1496z00_3867;

											{	/* Cfa/app.scm 39 */
												BgL_approxz00_bglt BgL_arg1573z00_3868;

												{	/* Cfa/app.scm 39 */
													obj_t BgL_arg1575z00_3869;

													BgL_arg1575z00_3869 =
														CAR(((obj_t) BgL_l1492z00_3865));
													BgL_arg1573z00_3868 =
														BGl_cfaz12z12zzcfa_cfaz00(
														((BgL_nodez00_bglt) BgL_arg1575z00_3869));
												}
												BgL_newtail1496z00_3867 =
													MAKE_YOUNG_PAIR(((obj_t) BgL_arg1573z00_3868), BNIL);
											}
											SET_CDR(BgL_tail1495z00_3866, BgL_newtail1496z00_3867);
											{	/* Cfa/app.scm 39 */
												obj_t BgL_arg1571z00_3870;

												BgL_arg1571z00_3870 = CDR(((obj_t) BgL_l1492z00_3865));
												{
													obj_t BgL_tail1495z00_4166;
													obj_t BgL_l1492z00_4165;

													BgL_l1492z00_4165 = BgL_arg1571z00_3870;
													BgL_tail1495z00_4166 = BgL_newtail1496z00_3867;
													BgL_tail1495z00_3866 = BgL_tail1495z00_4166;
													BgL_l1492z00_3865 = BgL_l1492z00_4165;
													goto BgL_zc3z04anonymousza31566ze3z87_3864;
												}
											}
										}
								}
							}
						}
				}
				{	/* Cfa/app.scm 42 */
					BgL_valuez00_bglt BgL_arg1553z00_3871;
					BgL_varz00_bglt BgL_arg1559z00_3872;

					BgL_arg1553z00_3871 =
						(((BgL_variablez00_bglt) COBJECT(
								(((BgL_varz00_bglt) COBJECT(
											(((BgL_appz00_bglt) COBJECT(
														((BgL_appz00_bglt) BgL_nodez00_3772)))->
												BgL_funz00)))->BgL_variablez00)))->BgL_valuez00);
					BgL_arg1559z00_3872 =
						(((BgL_appz00_bglt) COBJECT(((BgL_appz00_bglt) BgL_nodez00_3772)))->
						BgL_funz00);
					return BGl_appz12z12zzcfa_appz00(((BgL_funz00_bglt)
							BgL_arg1553z00_3871), BgL_arg1559z00_3872,
						BgL_argszd2approxzd2_3858);
				}
			}
		}

	}



/* imported-modules-init */
	obj_t BGl_importedzd2moduleszd2initz00zzcfa_appz00(void)
	{
		{	/* Cfa/app.scm 15 */
			BGl_modulezd2initializa7ationz75zztools_tracez00(54713316L,
				BSTRING_TO_STRING(BGl_string1971z00zzcfa_appz00));
			BGl_modulezd2initializa7ationz75zztools_shapez00(365925558L,
				BSTRING_TO_STRING(BGl_string1971z00zzcfa_appz00));
			BGl_modulezd2initializa7ationz75zztools_errorz00(300504031L,
				BSTRING_TO_STRING(BGl_string1971z00zzcfa_appz00));
			BGl_modulezd2initializa7ationz75zztype_typez00(515343395L,
				BSTRING_TO_STRING(BGl_string1971z00zzcfa_appz00));
			BGl_modulezd2initializa7ationz75zztype_cachez00(281500181L,
				BSTRING_TO_STRING(BGl_string1971z00zzcfa_appz00));
			BGl_modulezd2initializa7ationz75zzast_varz00(90839984L,
				BSTRING_TO_STRING(BGl_string1971z00zzcfa_appz00));
			BGl_modulezd2initializa7ationz75zzast_nodez00(469732712L,
				BSTRING_TO_STRING(BGl_string1971z00zzcfa_appz00));
			BGl_modulezd2initializa7ationz75zzcfa_infoz00(3788352L,
				BSTRING_TO_STRING(BGl_string1971z00zzcfa_appz00));
			BGl_modulezd2initializa7ationz75zzcfa_info2z00(0L,
				BSTRING_TO_STRING(BGl_string1971z00zzcfa_appz00));
			BGl_modulezd2initializa7ationz75zzcfa_cfaz00(400853817L,
				BSTRING_TO_STRING(BGl_string1971z00zzcfa_appz00));
			BGl_modulezd2initializa7ationz75zzcfa_iteratez00(131412063L,
				BSTRING_TO_STRING(BGl_string1971z00zzcfa_appz00));
			BGl_modulezd2initializa7ationz75zzcfa_loosez00(471177480L,
				BSTRING_TO_STRING(BGl_string1971z00zzcfa_appz00));
			BGl_modulezd2initializa7ationz75zzcfa_approxz00(285971837L,
				BSTRING_TO_STRING(BGl_string1971z00zzcfa_appz00));
			return
				BGl_modulezd2initializa7ationz75zzcfa_procedurez00(227655313L,
				BSTRING_TO_STRING(BGl_string1971z00zzcfa_appz00));
		}

	}

#ifdef __cplusplus
}
#endif
